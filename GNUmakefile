SHELL = /bin/bash
.SUFFIXES: .C .o .h .so 
VPATH = lib:src:include:
vpath %.C src:
DESTDIR = bin 
basedir := 
incdir := include
srcdir := src
builddir := lib

LIBNAME	  := InSANE
SOLIBNAMES := InSANEGeo InSANECluster InSANEEvent  InSANECut InSANEGeneral InSANEMath InSANEPhysics
LIBRARY	  = lib$(LIBNAME).so
LIBMAJOR  = 1
LIBOBJS	  = 1
LIBMINOR	  = 1
LIBREVIS	  = 0

lib_base := lib/base
lib_geo := lib/geo
lib_cut := lib/cut
lib_det := lib/det
lib_monte := lib/monte
libraries := $(lib_base) $(lib_geo) $(lib_cut) $(lib_det) $(lib_monte)

SOLIBS := $(SOLIBNAMES:%=lib/lib%.so.$(LIBMAJOR).$(LIBMINOR) )

################################################################################

CXX = g++
FC  = gfortran 

# Environment variables
PDF_GRID_CXX_FLAG = -D"InSANE_PDF_GRID_DIR=\"${InSANE_PDF_GRID_DIR}\""
PDF_GRID_F77_FLAG = -D"InSANE_PDF_GRID_DIR='${InSANE_PDF_GRID_DIR}'"


ROOTCINTFAGS += -c 

CPPFLAGS += $(shell root-config --cflags )
CPPFLAGS += -I$(ROOTSYS)/include
CPPFLAGS += -Iinclude -I.
CPPFLAGS += -I$(shell lhapdf-config --incdir ) 
CPPFLAGS += $(shell mysql_config --include ) 
CPPFLAGS +=  -g -pipe 
CPPFLAGS += $(shell clhep-config --include ) 
#CPPFLAGS += $(shell pkg-config sqlite3 --cflags ) 
CPPFLAGS += -I$(HOME)/include
#-D_LARGEFILE_SOURCE -fno-strict-aliasing

#LDLIBS +=  -lGeomPainter -lGeom -lSpectrum -lSpectrumPainter
LDLIBS += $(shell lhapdf-config --ldflags)
LDLIBS += $(shell root-config --ldflags)
LDLIBS += $(shell root-config --libs)
LDLIBS += $(shell root-config --evelibs)
LDLIBS += $(shell root-config --glibs)
#LDLIBS += $(shell mysql_config --libs)
LDLIBS += -lSpectrum
#LDLIBS += -lgfortran

#LDLIBS += -lsqlite3
LDLIBS += $(shell pkg-config sqlite3 --libs)

LDLIBS += $(shell clhep-config --libs)

CXXFLAGS  += -O0 -Wall -fPIC 
CXXFLAGS  += $(CPPFLAGS)
CXXFLAGS  += $(PDF_GRID_CXX_FLAG)

F77FLAGS += -g -O0 -Wall -fPIC -Iinclude -ffixed-line-length-none
F77FLAGS += -x f77-cpp-input 
F77FLAGS += $(PDF_GRID_F77_FLAG)

##############################################################################/
default : newbuild 

newbuild :  
	@echo "*** In-source build no longer supported ***"
	@echo "Use cmake with out-of-source build. For example, "
	@echo "cd .."
	@echo "mkdir insane_build "
	@echo "cd insane_build"
	@echo "cmake ../InSANE/. -DCMAKE_INSTALL_PREFIX=/usr/local"
	@echo "make "
	@echo "make install"

# Shared Libraries
##############################################################################/

convert : $(SOLIBNAMES)
	g++ $(CXXFLAGS) -shared  -Wl,-soname,$(LIBRARY).$(LIBMAJOR).$(LIBMINOR)\
	 -o lib/$(LIBRARY).$(LIBMAJOR).$(LIBMINOR)  \
	  $(SOLIBS) $(LDLIBS)


# Shared Libraries
##############################################################################/

shared_libs : $(SOLIBNAMES)
	@echo "  ====== Creating InSANE Shared Library ====== "
	g++ $(CXXFLAGS) -shared  -Wl,-soname,$(LIBRARY).$(LIBMAJOR).$(LIBMINOR)\
	 -o lib/$(LIBRARY).$(LIBMAJOR).$(LIBMINOR)  \
	  $(SOLIBS) $(LDLIBS)
#	ln -s $(builddir)/libInSANE.so.1.1 $(builddir)/libInSANE.so
#	ln -s $(builddir)/libInSANE.so.1.1 $(builddir)/libInSANE.so.1
#	ln -s $(builddir)/libInSANE.so.1.1 $(builddir)/libInSANE.so
#_____________________________________________________________________________/
# Utilitiy
##############################################################################/

doc: 
	doxygen doc/InSANE_Doxyfile #	doxygen doc/Doxyfile_insaneweb
#	doxygen doc/BETAG4_Doxyfile #	doxygen doc/Doxyfile_Analyzer
	cd ..
	@echo "HTML Documentation created"

.PHONY : clean printstuff doc snapshot

snapshot:
	git archive HEAD --format=tar | gzip >InSANE.tar.gz
	mv InSANE.tar.gz public_html/snapshots/.

#git archive HEAD --format=tar | gzip >InSANE-`date +%m_%d_%Y_`.tar.gz

clean:
	rm src/*Dict.cxx
	rm -rf lib/libInSANE*
	rm -rf lib/InSANE*Dict.o
	rm -rf lib/F1F209.o
	rm include/*Dict.h 
	rm -rf lib/*.o

printstuff : 
	@echo $(SOLIBS)
#	@ls lib/*.o

#makefiledir := $(InSANEDIR)/build
#include  $(makefiledir)/Cluster.mk  $(makefiledir)/Event.mk  $(makefiledir)/Geo.mk \
#	 $(makefiledir)/General.mk $(makefiledir)/Cut.mk \
#	 $(makefiledir)/Math.mk $(makefiledir)/Physics.mk 

link:
	echo $(CXXFLAGS) > lib/flags.txt
	echo $(LDLIBS) >> lib/flags.txt
	cd lib/ ; ln -sf libInSANEGeneral.so.$(LIBMAJOR).$(LIBMINOR) libInSANEGeneral.so
	cd lib/ ; ln -sf libInSANECut.so.$(LIBMAJOR).$(LIBMINOR) libInSANECut.so
	cd lib/ ; ln -sf libInSANEPhysics.so.$(LIBMAJOR).$(LIBMINOR) libInSANEPhysics.so
	cd lib/ ; ln -sf libInSANEGeo.so.$(LIBMAJOR).$(LIBMINOR) libInSANEGeo.so
	cd lib/ ; ln -sf libInSANECluster.so.$(LIBMAJOR).$(LIBMINOR) libInSANECluster.so
	cd lib/ ; ln -sf libInSANEMath.so.$(LIBMAJOR).$(LIBMINOR) libInSANEMath.so
	cd lib/ ; ln -sf libInSANEEvent.so.$(LIBMAJOR).$(LIBMINOR) libInSANEEvent.so
	cd lib/ ; ln -sf libInSANE.so.$(LIBMAJOR).$(LIBMINOR) libInSANE.so

