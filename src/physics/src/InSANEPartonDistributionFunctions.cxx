#include "InSANEPartonDistributionFunctions.h"
#include "TStyle.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TLatex.h"
#include "TF1.h"
#include "TF2.h"
#include "TROOT.h"

ClassImp(InSANEPDFBase)
//_____________________________________________________________________________
InSANEPDFBase::InSANEPDFBase() : fLabel("PDFbase"), fQsquared(0.0),fXbjorken(0.0),
   fxPlotMin(0.01),fxPlotMax(1.0),
   fModelMin_x(0.00001),fModelMax_x(0.99999),
   fModelMin_W(1.0),fModelMax_W(1.0e9),
   fModelMin_Q2(1.0),fModelMax_Q2(1.0e9)
{
   for (int i = 0; i < NPARTONDISTS; i++) {
      fFunctions[i] = nullptr;
      fPDFValues[i] = 0.0;
      fPDFErrors[i] = 0.0;
   }
   SetLineWidth(2);
   //ClearValues();
   //fDefaultLineColor = 1;
   //fDefaultLineStyle = 1;
}
//_____________________________________________________________________________

InSANEPDFBase::~InSANEPDFBase() {
}
//_____________________________________________________________________________

void InSANEPDFBase::ClearValues() {
   fXbjorken = 0.0;
   fQsquared = 0.0;
   for (int i = 0; i < NPARTONDISTS; i++) {
      fPDFValues[i] = 0.0;
      fPDFErrors[i] = 0.0;
   }
}
//_____________________________________________________________________________

ClassImp(InSANEPartonDistributionFunctions)
//_____________________________________________________________________________

TF1 * InSANEPartonDistributionFunctions::GetFunction(InSANEPDFBase::InSANEPartonFlavor q ) {
   if (fFunctions[q]){ 
      //fFunctions[q]->SetLineColor(fDefaultLineColor);
      fFunctions[q]->TAttLine::operator=(*this);
      return(fFunctions[q]);
   } else {
      Int_t npar = 1;
      switch (q) {
         case InSANEPDFBase::kUP :
            fFunctions[q] = new TF1(Form("xu %s", GetLabel()), this, &InSANEPartonDistributionFunctions::Evaluatexu,
                  fxPlotMin, fxPlotMax, npar, "InSANEPartonDistributionFunctions", "Evaluate_u");
            break;
         case InSANEPDFBase::kDOWN :
            fFunctions[q] = new TF1(Form("xd %s", GetLabel()), this, &InSANEPartonDistributionFunctions::Evaluatexd,
                  fxPlotMin, fxPlotMax, npar, "InSANEPartonDistributionFunctions", "Evaluate_d");
            break;

         case InSANEPDFBase::kSTRANGE :
            fFunctions[q] = new TF1("xs", this, &InSANEPartonDistributionFunctions::Evaluatexs,
                  fxPlotMin, fxPlotMax, npar, "InSANEPartonDistributionFunctions", "Evaluatexs");
            break;

         case InSANEPDFBase::kGLUON :
            fFunctions[q] = new TF1("xg", this, &InSANEPartonDistributionFunctions::Evaluatexg,
                  fxPlotMin, fxPlotMax, npar, "InSANEPartonDistributionFunctions", "Evaluatexg");
            break;

         case InSANEPDFBase::kANTIUP :
            fFunctions[q] = new TF1("xubar", this, &InSANEPartonDistributionFunctions::Evaluatexubar,
                  fxPlotMin, fxPlotMax, npar, "InSANEPartonDistributionFunctions", "Evaluatexubar");
            break;

         case InSANEPDFBase::kANTIDOWN :
            fFunctions[q] = new TF1("xdbar", this, &InSANEPartonDistributionFunctions::Evaluatexdbar,
                  fxPlotMin, fxPlotMax, npar, "InSANEPartonDistributionFunctions", "Evaluatexdbar");
            break;

         case InSANEPDFBase::kANTISTRANGE :
            fFunctions[q] = new TF1("xsbar", this, &InSANEPartonDistributionFunctions::Evaluatexsbar,
                  fxPlotMin, fxPlotMax, npar, "InSANEPartonDistributionFunctions", "Evaluatexsbar");
            break;
         default :
            fFunctions[q] = new TF1(Form("xu %s", GetLabel()), this, &InSANEPartonDistributionFunctions::Evaluatexu,
                  fxPlotMin, fxPlotMax, npar, "InSANEPartonDistributionFunctions", "Evaluatexu");
            break;

      }
   }
   //fFunctions[q]->SetLineColor(fDefaultLineColor);
   fFunctions[q]->TAttLine::operator=(*this);
   return(fFunctions[q]);
}
//_____________________________________________________________________________
void InSANEPartonDistributionFunctions::GetValues(TObject *obj, Double_t Q2, InSANEPDFBase::InSANEPartonFlavor q){
   // Fills histogram with values.
   //  For error band use GetErrorBand
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   Double_t x[3];

   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
            x[0]=xaxis->GetBinCenter(binx);

   GetPDFs(x[0], Q2);
            hfit->SetBinContent(binx, biny, binz, x[0]*fPDFValues[q]);
   //GetPDFErrors(x[0], Q2);
   //         hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
         }
      }
   }

}
//_____________________________________________________________________________
void InSANEPartonDistributionFunctions::GetErrorBand(TObject *obj, Double_t Q2, InSANEPDFBase::InSANEPartonFlavor q){
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   Double_t x[3];

   
   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
            x[0]=xaxis->GetBinCenter(binx);

   GetPDFs(x[0], Q2);
            hfit->SetBinContent(binx, biny, binz, x[0]*fPDFValues[q]);
   GetPDFErrors(x[0], Q2);
            hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
         }
      }
   }

}
//______________________________________________________________________________

void InSANEPartonDistributionFunctions::PlotPDFs(Double_t Qsq, Int_t log)
{

   const Int_t N = 30;
   Double_t xmin = 0.0001;
   Double_t xmax = 0.9999;
   //Double_t Qsqmin=1.0;
   //Double_t Qsqmax=12.0;
   //Double_t QMean=5.4;
   Double_t dx = (xmax - xmin) / ((Double_t)N);
   //Double_t dQsq=(Qsqmax-Qsqmin)/((Double_t)N);


   Double_t x = 0;
   //, val1,val2;
   Int_t pointNumber = 0;

   auto *mg = new TMultiGraph();

   auto * gUpVsX = new TGraph();
   gUpVsX->SetTitle(Form("xu(x) [%s]", GetLabel()));
   gUpVsX->SetMarkerColor(1);
   gUpVsX->SetMarkerStyle(21);
   gUpVsX->SetMarkerSize(1.3);
   gUpVsX->SetLineColor(1);
   gUpVsX->SetLineWidth(3);

   auto * gDownVsX = new TGraph();
   gDownVsX->SetMarkerColor(2);
   gDownVsX->SetMarkerStyle(22);
   gDownVsX->SetMarkerSize(1.3);
   gDownVsX->SetLineColor(2);
   gDownVsX->SetLineWidth(3);
   gDownVsX->SetTitle(Form("xd(x) [%s]", GetLabel()));

   auto * gStrangeVsX = new TGraph();
   gStrangeVsX->SetMarkerColor(3);
   gStrangeVsX->SetMarkerStyle(23);
   gStrangeVsX->SetMarkerSize(1.3);
   gStrangeVsX->SetLineColor(3);
   gStrangeVsX->SetLineWidth(3);
   gStrangeVsX->SetTitle(Form("xs(x) [%s]", GetLabel()));

   auto * gUpbarVsX = new TGraph();
   gUpbarVsX->SetMarkerColor(4);
   gUpbarVsX->SetMarkerStyle(24);
   gUpbarVsX->SetMarkerSize(1.3);
   gUpbarVsX->SetLineColor(4);
   gUpbarVsX->SetLineWidth(3);
   gUpbarVsX->SetTitle(Form("x#bar{u}(x) [%s]", GetLabel()));

   auto * gDownbarVsX = new TGraph();
   gDownbarVsX->SetMarkerColor(5);
   gDownbarVsX->SetMarkerStyle(25);
   gDownbarVsX->SetMarkerSize(1.3);
   gDownbarVsX->SetLineColor(5);
   gDownbarVsX->SetLineWidth(3);
   gDownbarVsX->SetTitle(Form("x#bar{d}(x) [%s]", GetLabel()));

   auto * gStrangebarVsX = new TGraph();
   gStrangebarVsX->SetMarkerColor(6);
   gStrangebarVsX->SetMarkerStyle(26);
   gStrangebarVsX->SetMarkerSize(1.3);
   gStrangebarVsX->SetLineColor(6);
   gStrangebarVsX->SetLineWidth(3);
   gStrangebarVsX->SetTitle(Form("x#bar{s}(x) [%s]", GetLabel()));

   Double_t * pdfvals;
   pointNumber = 0;
   for (int i = 0; i < N; i++) {
      x = xmin + (Double_t)i * dx;
      pdfvals =  GetPDFs(x, Qsq);
      gUpVsX->SetPoint(pointNumber, x, u());
      gUpbarVsX->SetPoint(pointNumber, x, ubar());
      gDownVsX->SetPoint(pointNumber, x, d());
      gDownbarVsX->SetPoint(pointNumber, x, dbar());
      gStrangeVsX->SetPoint(pointNumber, x, s());
      gStrangebarVsX->SetPoint(pointNumber, x, sbar());
      pointNumber++;
   }

   /*  TCanvas * fCanvas = new TCanvas("pdfs","Parton Dist Functions",0,0,600,400);*/
   /*  fCanvas->Divide(3,2);*/
   auto *fCanvas = new TCanvas(Form("cUnPolPdfs%d", (Int_t)Qsq), "Unpolarized PDFs", 0, 0, 600, 400);
   fCanvas->cd(0)->SetLogx(log);

   mg->Add(gUpVsX, "LP");
   mg->Add(gDownVsX, "LP");
   mg->Add(gStrangeVsX, "LP");
   mg->Add(gUpbarVsX, "LP");
   mg->Add(gDownbarVsX, "LP");
   mg->Add(gStrangebarVsX, "LP");
   mg->Draw("A");

   auto * leg = new TLegend(0.7, 0.7, 0.95, 0.95);
   leg->SetHeader(Form("%s Polarized PDFs", GetLabel()));
   leg->AddEntry(gUpVsX, gUpVsX->GetTitle(), "lp");
   leg->AddEntry(gDownVsX, gDownVsX->GetTitle(), "lp");
   leg->AddEntry(gStrangeVsX, gStrangeVsX->GetTitle(), "lp");
   leg->Draw();

   auto * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(36);
   t->SetTextSize(0.06);
   t->SetTextAlign(12);
   t->DrawLatex(0.05, 0.95, Form("Q^{2} = %d GeV^{2}", (Int_t)Qsq));


//   fCanvas->cd(1)->SetLogx(log);
//   gUpVsX->Draw("ALP");
//   fCanvas->cd(2)->SetLogx(log);
//   gDownVsX->Draw("ALP");
//   fCanvas->cd(3)->SetLogx(log);
//   gStrangeVsX->Draw("ALP");
//   fCanvas->cd(4)->SetLogx(log);
//   gUpbarVsX->Draw("ALP");
//   fCanvas->cd(5)->SetLogx(log);
//   gDownbarVsX->Draw("ALP");
//   fCanvas->cd(6)->SetLogx(log);
//   gStrangebarVsX->Draw("ALP");
}
//_____________________________________________________________________________

TCanvas *  InSANEPartonDistributionFunctions::PlotPDF(Double_t Qsq, Int_t log)
{

   return(new TCanvas());
}
//_____________________________________________________________________________


