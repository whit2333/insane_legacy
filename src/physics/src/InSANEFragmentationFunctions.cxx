#include "InSANEFragmentationFunctions.h"


//______________________________________________________________________________
InSANEFragmentationFunctions::InSANEFragmentationFunctions(){
}
//______________________________________________________________________________
InSANEFragmentationFunctions::~InSANEFragmentationFunctions(){
}
//______________________________________________________________________________
Double_t InSANEFragmentationFunctions::D_u_pi0( Double_t z, Double_t Q2)    {
   Double_t Dplus  = D_u_piplus( z,Q2);
   Double_t Dminus = D_u_piminus(z,Q2);
   return( (Dplus+Dminus)/2.0 );
}
//______________________________________________________________________________
Double_t InSANEFragmentationFunctions::D_ubar_pi0( Double_t z, Double_t Q2) {
   Double_t Dplus  = D_ubar_piplus( z,Q2);
   Double_t Dminus = D_ubar_piminus(z,Q2);
   return( (Dplus+Dminus)/2.0 );
}
//______________________________________________________________________________
Double_t InSANEFragmentationFunctions::D_d_pi0( Double_t z, Double_t Q2)    {
   Double_t Dplus  = D_d_piplus( z,Q2);
   Double_t Dminus = D_d_piminus(z,Q2);
   return( (Dplus+Dminus)/2.0 );
}
//______________________________________________________________________________
Double_t InSANEFragmentationFunctions::D_dbar_pi0( Double_t z, Double_t Q2) {
   Double_t Dplus  = D_dbar_piplus( z,Q2);
   Double_t Dminus = D_dbar_piminus(z,Q2);
   return( (Dplus+Dminus)/2.0 );
}
//______________________________________________________________________________
Double_t InSANEFragmentationFunctions::D_s_pi0( Double_t z, Double_t Q2)    {
   Double_t Dplus  = D_s_piplus( z,Q2);
   Double_t Dminus = D_s_piminus(z,Q2);
   return( (Dplus+Dminus)/2.0 );
}
//______________________________________________________________________________
Double_t InSANEFragmentationFunctions::D_sbar_pi0( Double_t z, Double_t Q2) {
   Double_t Dplus  = D_sbar_piplus( z,Q2);
   Double_t Dminus = D_sbar_piminus(z,Q2);
   return( (Dplus+Dminus)/2.0 );
}
//______________________________________________________________________________

