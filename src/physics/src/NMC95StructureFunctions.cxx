#include "NMC95StructureFunctions.h"

ClassImp(NMC95StructureFunctions) 
//______________________________________________________________________________
NMC95StructureFunctions::NMC95StructureFunctions(){
   SetLabel("NMC95");
   SetNameTitle("NMC95StructureFunctions","NMC95");
   fF1 = 0;
   fF2 = 0;
   fM  = 0;
   fR  = 0; 
   fA  = 0;
   fZ  = 0; 
   fN  = 0; 
}
//______________________________________________________________________________
NMC95StructureFunctions::~NMC95StructureFunctions(){

}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F2Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A){
   Double_t res = (A/2.0)*2.0*F2d(x,Qsq)*EMC_Effect(&x,&A);
   return res; 
}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F1Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A){
   Double_t res = (A/2.0)*2.0*F1d(x,Qsq)*EMC_Effect(&x,&A);
   return res; 
}
//______________________________________________________________________________
void NMC95StructureFunctions::GetSFs(Int_t part,Double_t x, Double_t Qsq) {

	fN          = fA;         
	auto XBj  = (double)x; 
	auto Q2   = (double)Qsq;

        switch(part){
		case 0: // proton 
			fF1 = f1psfun_(&XBj,&Q2); 
			fF2 = f2psfun_(&XBj,&Q2); 
                        break; 
		case 1: // neutron
			fF1 = f1nsfun_(&XBj,&Q2); 
			fF2 = f2nsfun_(&XBj,&Q2); 
			break; 
		case 2: // deuteron (per nucelon) 
			fF1 = f1dsfun_(&XBj,&Q2); 
			fF2 = f2dsfun_(&XBj,&Q2); 
			break;             
		case 3: // 3He (per nucleon)  
			fF1 = f1hesfun_(&XBj,&Q2); 
			fF2 = f2hesfun_(&XBj,&Q2); 
			break;  
		default:
			std::cout << "[NMC95StructureFunctions::GetSFs]: "; 
                        std::cout << "Invalid target!  Exiting..." << std::endl;
			exit(1); 
        }

}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F1p(Double_t x,Double_t Qsq) {
	fA = 1.0;
	fZ = 1.0;
	GetSFs(0,x,Qsq);
	return(fF1);
}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F2p(Double_t x, Double_t Qsq) {
	fA = 1.0;
	fZ = 1.0;
	GetSFs(0,x,Qsq);
	return(fF2);
}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F1n(Double_t x, Double_t Qsq) {
	fA = 1.0;
	fZ = 0.0;
	GetSFs(1,x,Qsq);
	return(fF1);
}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F2n(Double_t x, Double_t Qsq) {
	fA = 1.0;
	fZ = 0.0;
	GetSFs(1,x,Qsq);
	return(fF2);
}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F1d(Double_t x, Double_t Qsq) {
	fA = 2.0;
	fZ = 1.0;
	GetSFs(2,x,Qsq);
	return fF1; // WARNING: returning F1 NOT PER NUCLEON 
}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F2d(Double_t x, Double_t Qsq) {
	fA = 2.0;
	fZ = 1.0;
	GetSFs(2,x,Qsq);
	return fF2; // WARNING: returning F2 NOT PER NUCLEON 
}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F1He3(Double_t x, Double_t Qsq) {
	fA = 3.0;
	fZ = 2.0;
	GetSFs(3,x,Qsq);
	return fF1; // WARNING: returning F1 NOT PER NUCLEON 
}
//______________________________________________________________________________
Double_t NMC95StructureFunctions::F2He3(Double_t x, Double_t Qsq) {
	fA = 3.0;
	fZ = 2.0;
	GetSFs(3,x,Qsq);
	return fF2; // WARNING: returning F2 NOT PER NUCLEON 
}
