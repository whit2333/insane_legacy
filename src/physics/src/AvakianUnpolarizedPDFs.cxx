#include "AvakianUnpolarizedPDFs.h"
ClassImp(AvakianUnpolarizedPDFs) 
//______________________________________________________________________________
AvakianUnpolarizedPDFs::AvakianUnpolarizedPDFs(){
   SetNameTitle("AvakianUnpolarizedPDFs","Avakian PDFs");
   SetLabel("Avakian");
   SetLineColor(4);
}
//______________________________________________________________________________
AvakianUnpolarizedPDFs::~AvakianUnpolarizedPDFs(){

}
//______________________________________________________________________________
Double_t *AvakianUnpolarizedPDFs::GetPDFs(Double_t x,Double_t Q2){

   Double_t up = fqhd.uPlus(x); 
   Double_t um = fqhd.uMinus(x); 
   Double_t dp = fqhd.dPlus(x); 
   Double_t dm = fqhd.dMinus(x); 
   Double_t sp = fqhd.sPlus(x); 
   Double_t sm = fqhd.sMinus(x); 
   Double_t gp = fqhd.gPlus(x); 
   Double_t gm = fqhd.gMinus(x); 

   Double_t u = up + um; 
   Double_t d = dp + dm; 
   Double_t s = sp + sm; 
   Double_t g = gp + gm; 

   for(int i=0;i<13;i++) fPDFValues[i] = 0;

   fPDFValues[0] = u;
   fPDFValues[1] = d;
   fPDFValues[2] = s;
   fPDFValues[6] = g;

   return fPDFValues;  

}

