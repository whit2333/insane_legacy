#include "InSANECompositeDiffXSec.h"

ClassImp(InSANECompositeDiffXSec)
//______________________________________________________________________________

InSANECompositeDiffXSec::InSANECompositeDiffXSec()
{
   fID          = 100100000;
   fProtonXSec  = nullptr;
   fNeutronXSec = nullptr;
   fTitle = "InSANECompositeDiffXSec";
   fPlotTitle = "#frac{d#sigma}{dE d#Omega} nb/GeV-Sr";

   fProtonXSec  = new InSANEInclusiveDiffXSec();
   fProtonXSec->SetTargetNucleus(InSANENucleus::Proton());

   fNeutronXSec = new InSANEInclusiveDiffXSec();
   fNeutronXSec->SetTargetNucleus(InSANENucleus::Neutron());

   fNeutronXSec->UsePhaseSpace(false);
   fProtonXSec->UsePhaseSpace(false);
}
//______________________________________________________________________________
InSANECompositeDiffXSec::~InSANECompositeDiffXSec()
{
}
//_____________________________________________________________________________

InSANECompositeDiffXSec::InSANECompositeDiffXSec(const InSANECompositeDiffXSec& rhs) : 
   InSANEInclusiveDiffXSec(rhs)
{
   (*this) = rhs;
}
//______________________________________________________________________________

InSANECompositeDiffXSec& InSANECompositeDiffXSec::operator=(const InSANECompositeDiffXSec& rhs) 
{
   if (this != &rhs) {  // make sure not same object
      InSANEInclusiveDiffXSec::operator=(rhs);
      fProtonXSec     = rhs.fProtonXSec->Clone();
      fNeutronXSec     = rhs.fNeutronXSec->Clone();
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

InSANECompositeDiffXSec*  InSANECompositeDiffXSec::Clone(const char * newname) const 
{
   std::cout << "InSANECompositeDiffXSec::Clone()\n";
   auto * copy = new InSANECompositeDiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

InSANECompositeDiffXSec*  InSANECompositeDiffXSec::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________
Double_t  InSANECompositeDiffXSec::EvaluateXSec(const Double_t * x) const{

   //std::cout << " composite eval " << std::endl;
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   if( !fProtonXSec )  return 0.0;
   if( !fNeutronXSec ) return 0.0;
   Double_t z    = GetZ();
   //if( z>0.0 ) z = 1.0; // hack for wiser... overridden in wiser codes
   Double_t n    = GetN();
   //if( n>0.0 ) n = 1.0;
   Double_t a =  z+n;

   Double_t xbj = InSANE::Kine::xBjorken_EEprimeTheta(GetBeamEnergy(),x[0],x[1]);
   Double_t emc = EMC_Effect(xbj,a);

   Double_t sigP = fProtonXSec->EvaluateXSec(x);
   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
   Double_t xsec = (sigP + sigN)*(a/2.0);
   //std::cout << " Z  = " << GetZ() << std::endl;
   //std::cout << "sp  = " << sigP << std::endl;
   //std::cout << " N  = " << GetN() << std::endl;
   //std::cout << "sn  = " << sigN << std::endl;

   //std::cout << " composite eval " <<  xsec << std::endl;
   //if ( IncludeJacobian() ) return xsec*TMath::Sin(th);
   if(TMath::IsNaN(xsec)) xsec = 0.0;
   if(xsec < 0.0) xsec = 0.0; 
   return xsec;
	
}
//______________________________________________________________________________
void       InSANECompositeDiffXSec::SetTargetMaterial(const InSANETargetMaterial& mat){
   if( fProtonXSec ) {
      fProtonXSec->SetTargetMaterial(mat);
      fProtonXSec->SetTargetNucleus(InSANENucleus::Proton());
   }
   if( fNeutronXSec ) {
      fNeutronXSec->SetTargetMaterial(mat);
      fNeutronXSec->SetTargetNucleus(InSANENucleus::Neutron());
   }
   fTargetMaterial = mat; fTargetNucleus = fTargetMaterial.GetNucleus();
}
//______________________________________________________________________________
void InSANECompositeDiffXSec::SetTargetNucleus(const InSANENucleus & targ){
   InSANEInclusiveDiffXSec::SetTargetNucleus(targ);
   //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
   //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
}
//______________________________________________________________________________
void       InSANECompositeDiffXSec::SetTargetMaterialIndex(Int_t i){
   if(fProtonXSec)   fProtonXSec->SetTargetMaterialIndex(i);
   if(fNeutronXSec) fNeutronXSec->SetTargetMaterialIndex(i);
   fMaterialIndex = i ;
}
//______________________________________________________________________________
void  InSANECompositeDiffXSec::SetUnits(const char * t){
   fUnits = t;
   if(fProtonXSec)   fProtonXSec->SetUnits(t);
   if(fNeutronXSec) fNeutronXSec->SetUnits(t);
}
//______________________________________________________________________________
void  InSANECompositeDiffXSec::SetBeamEnergy(Double_t en){
   InSANEInclusiveDiffXSec::SetBeamEnergy(en);
   if(fProtonXSec) fProtonXSec->SetBeamEnergy(en);
   if(fNeutronXSec) fNeutronXSec->SetBeamEnergy(en);
}
//______________________________________________________________________________
void  InSANECompositeDiffXSec::SetPhaseSpace(InSANEPhaseSpace * ps){
   if(fProtonXSec)  fProtonXSec->SetPhaseSpace(ps);
   if(fProtonXSec)  InSANEInclusiveDiffXSec::SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   if(fNeutronXSec) fNeutronXSec->SetPhaseSpace(ps);
}
//______________________________________________________________________________
void   InSANECompositeDiffXSec::InitializePhaseSpaceVariables(){
   if(fProtonXSec)  fProtonXSec->InitializePhaseSpaceVariables();
   if(fNeutronXSec) fNeutronXSec->InitializePhaseSpaceVariables();
   if(fProtonXSec)  InSANEInclusiveDiffXSec::SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   if(fNeutronXSec)  fNeutronXSec->SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   //InSANEInclusiveDiffXSec::InitializePhaseSpaceVariables();
}
//______________________________________________________________________________
void InSANECompositeDiffXSec::InitializeFinalStateParticles(){
   if(fProtonXSec)  fProtonXSec->InitializeFinalStateParticles();
   if(fNeutronXSec) fNeutronXSec->InitializeFinalStateParticles();
   InSANEInclusiveDiffXSec::InitializeFinalStateParticles();
}
//_____________________________________________________________________________
void InSANECompositeDiffXSec::SetParticleType(Int_t pdgcode, Int_t part) {
   InSANEDiffXSec::SetParticleType(pdgcode, part);
   if(fProtonXSec)  fProtonXSec->SetParticleType(pdgcode,part);
   if(fNeutronXSec) fNeutronXSec->SetParticleType(pdgcode,part);
}
//______________________________________________________________________________


