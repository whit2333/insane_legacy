#include "InSANEPhotoAbsorptionCrossSections.h"

//______________________________________________________________________________
InSANEPhotoAbsorptionCrossSections::InSANEPhotoAbsorptionCrossSections(
      const char * n, const char * t ) : 
   TNamed(n,t),
   fSig_T( 0.0), fSig_L(  0.0),
   fSig_LT(0.0), fSig_LTp(0.0),
   fSig_TT(0.0), fSig_TTp(0.0),
   fSig_L0(0.0), fSig_LT0(0.0), fSig_LT0p(0.0)
{
   fSigs.resize(9);
}
//______________________________________________________________________________
InSANEPhotoAbsorptionCrossSections::~InSANEPhotoAbsorptionCrossSections()
{ }
//______________________________________________________________________________
//void InSANEPhotoAbsorptionCrossSections::Calculate(Double_t x, Double_t Q2)
//{
//}
//______________________________________________________________________________
