#include "CTEQ10_UPDFs.h"
#include "InSANEFortranWrappers.h"
#include "TMath.h"

namespace insane {
  namespace physics {

    CTEQ10_UPDFs::CTEQ10_UPDFs()
    {
      int iSet = 100;     // default
      setct10_(&iSet);  
      SetLabel("CTEQ10");
      SetNameTitle("CTEQ10UnpolarizedPDFs","CTEQ10 PDFs");
      SetLineColor(kCyan+2);
    }
    //______________________________________________________________________________

    CTEQ10_UPDFs::~CTEQ10_UPDFs()
    { }
    //______________________________________________________________________________
    
    std::array<double,NPartons> CTEQ10_UPDFs::Calculate(double x, double Q2) const
    {
      /** Implementation of pure virtual method.
       *
       *  The function getct10 (Iparton, X, Q)
       *  returns the parton distribution inside the proton for parton [Iparton]
       *  at [X] Bjorken_X and scale [Q] (GeV) in PDF set [Iset].
       *  Iparton  is the parton label (5, 4, 3, 2, 1, 0, -1, ......, -5)
       *                           for (b, c, s, d, u, g, u_bar, ..., b_bar),
       */
      //for(int i=0;i<13;i++) fValues[i] = 0.; 
      

      auto values = fValues;

      //fXbjorken = x;
      //fQsquared = Q2;
      double Q = TMath::Sqrt(Q2);
      double res = 0.0;
      int i = 0;
      /// up quark 
      i = 1;
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kUP] = res; 
      /// down quark 
      i = 2;
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kDOWN] = res; 
      /// strange 
      i = 3;
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kSTRANGE] = res; 
      /// charm 
      i = 4; 
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kCHARM] = res; 
      /// bottom
      i = 5;  
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kBOTTOM] = res; 
      /// top 
      values[PartonFlavor::kTOP] = res; 
      /// gluon 
      i = 0;
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kGLUON] = res; 
      /// u-bar 
      i = -1;
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kANTIUP] = res; 
      /// d-bar 
      i = -2;
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kANTIDOWN] = res; 
      /// s-bar 
      i = -3;
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kANTISTRANGE] = res; 
      /// c-bar 
      i = -4; 
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kANTICHARM] = res; 
      /// b-bar  
      i = -5; 
      getct10_(&i,&x,&Q,&res);
      values[PartonFlavor::kANTIBOTTOM] = res; 
      /// t-bar 
      values[PartonFlavor::kANTITOP] = res; 


      return values;
    }
    //______________________________________________________________________________

    std::array<double,NPartons> CTEQ10_UPDFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }


  }
}
