#include "InSANEInclusiveDISXSec.h"
//#include "InSANEInelasticRadiativeTail.h"

InSANEInclusiveDISXSec::InSANEInclusiveDISXSec()
{
   fID    = 100100001;
   fTitle = "InSANEInclusiveDISXSec";
   fPlotTitle = "#frac{d#sigma}{dE d#Omega} nb/GeV-Sr";

   //fProtonXSec  = new InSANEInclusiveDiffXSec();
   fProtonXSec  = new InSANEInelasticRadiativeTail2();
   //((InSANEInelasticRadiativeTail*)fProtonXSec)->GetBornXSec()->SetTargetNucleus(InSANENucleus::Proton());
   fProtonXSec->SetTargetNucleus(InSANENucleus::Proton());
   //((InSANEInelasticRadiativeTail*)fProtonXSec)->SetTargetThickness(0.046);

   //fNeutronXSec = new InSANEInclusiveDiffXSec();
   fNeutronXSec  = new InSANEInelasticRadiativeTail2();
   //((InSANEInelasticRadiativeTail*)fNeutronXSec)->GetBornXSec()->SetTargetNucleus(InSANENucleus::Neutron());
   fNeutronXSec->SetTargetNucleus(InSANENucleus::Neutron());
   //((InSANEInelasticRadiativeTail*)fNeutronXSec)->SetTargetThickness(0.046);
}
//______________________________________________________________________________

InSANEInclusiveDISXSec::~InSANEInclusiveDISXSec()
{ }
//______________________________________________________________________________

InSANEInclusiveDISXSec*  InSANEInclusiveDISXSec::Clone(const char * newname) const 
{
   std::cout << "InSANEInclusiveDISXSec::Clone()\n";
   auto * copy = new InSANEInclusiveDISXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

void InSANEInclusiveDISXSec::SetTargetThickness(Double_t t)
{
   ((InSANEInelasticRadiativeTail2*)fProtonXSec)->SetTargetThickness(t);
   ((InSANEInelasticRadiativeTail2*)fNeutronXSec)->SetTargetThickness(t);
}
//______________________________________________________________________________

