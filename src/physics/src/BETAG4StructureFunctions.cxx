#include "BETAG4StructureFunctions.h"

ClassImp(BETAG4StructureFunctions)

BETAG4StructureFunctions::BETAG4StructureFunctions()
{


}

BETAG4StructureFunctions::~BETAG4StructureFunctions()
{



}

Double_t BETAG4StructureFunctions::F1p(Double_t x, Double_t Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

Double_t BETAG4StructureFunctions::F2p(Double_t x, Double_t Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

Double_t BETAG4StructureFunctions::F1n(Double_t x, Double_t Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

Double_t BETAG4StructureFunctions::F2n(Double_t x, Double_t Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

Double_t BETAG4StructureFunctions::F1d(Double_t x, Double_t Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

Double_t BETAG4StructureFunctions::F2d(Double_t x, Double_t Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

Double_t BETAG4StructureFunctions::g1p(Double_t x, Double_t Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

Double_t BETAG4StructureFunctions::g2p(Double_t x, Double_t Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}
