#include "Stat2015_PPDFs.h"
#include <iostream>

namespace insane {
  namespace physics {

    Stat2015_PPDFs::Stat2015_PPDFs(){

      SetNameTitle("Stat2015_PPDFs","Stat2015 pol. PDFs");
      SetLabel("Stat2015");
      SetLineColor(2);
      SetLineStyle(1);

      fiPol    = 2;
      fiSingle = 0;
      fiNum    = 0;

      // These values are always zero.
      fValues[4] = 0.0;//Delta b
      fValues[5] = 0.0;//Delta t
      fValues[11] = 0.0;//Delta bbar
      fValues[12] = 0.0;//Delta tbar

      for(Int_t i=0;i<12;i++) fUncertainties[i] = 0.; 
    }
    //______________________________________________________________________________
    Stat2015_PPDFs::~Stat2015_PPDFs(){

    }
    //______________________________________________________________________________
    std::array<double,NPartons> Stat2015_PPDFs::Calculate    (double x, double Q2) const
    {

      auto Xbjorken = x;
      auto Qsquared = Q2;

      int    iPol    = fiPol;
      int    iSingle = fiSingle;
      int    iNum    = fiNum;
      std::array<double,13> pdfs;

      auto res = fValues;

      //* PDF NOTATION FOR UNUnpolarized AND Unpolarized PDF
      //*  -6   -5   -4   -3   -2   -1   0    1  2  3  4  5  6
      //* TBAR BBAR CBAR SBAR DBAR UBAR GLUON U  D  S  C  B  T
      //   0     1    2   3    4    5     6   7  8  9  10 11 12
      partonevol_(&Xbjorken, &Qsquared,&iPol, pdfs.data(),&iSingle,&iNum);

      res[0]  = pdfs[7]/x;   // up
      res[1]  = pdfs[8]/x;   // down
      res[2]  = pdfs[9]/x;   // s
      res[3]  = pdfs[10]/x;  // c
      res[6]  = pdfs[6]/x;   // g
      res[7]  = pdfs[5]/x;   // ubar
      res[8]  = pdfs[4]/x;   // dbar
      res[9]  = pdfs[3]/x;   // sbar
      res[10] = pdfs[2]/x;   // cbar

      //fUncertainties[0] = 0.0;
      //fUncertainties[1] = 0.0;
      //fUncertainties[2] = 0.0;
      //fUncertainties[3] = 0.0;
      //fUncertainties[6] = 0.0;
      //fUncertainties[7] = 0.0;
      //fUncertainties[8] = 0.0;
      //fUncertainties[9] = 0.0;
      //fUncertainties[10] = 0.0;

      //std::cout << " u check: " << Get(kUP) << " vs " << pdfs[7]/x << std::endl;;
      return(res);

    }
    //______________________________________________________________________________

    std::array<double,NPartons> Stat2015_PPDFs::Uncertainties(double x, double Q2) const
    {

      //fUncertainties[0] = 0.0;
      //fUncertainties[1] = 0.0;
      //fUncertainties[2] = 0.0;
      //fUncertainties[3] = 0.0;
      //fUncertainties[6] = 0.0;
      //fUncertainties[7] = 0.0;
      //fUncertainties[8] = 0.0;
      //fUncertainties[9] = 0.0;
      //fUncertainties[10] = 0.0;

      return(fUncertainties);

    }
    //______________________________________________________________________________

  }
}
