#include "InSANETargetMaterial.h"

#include <iomanip>

#include "InSANEInclusiveDiffXSec.h"
#include "InSANECompositeDiffXSec.h"
#include "InSANEFunctionManager.h"
#include "InSANERadiator.h"
#include "InSANEElasticRadiativeTail.h"
#include "WiserXSection.h"
#include "WiserInclusiveElectroXSec.h"
#include "EPCVXSection.h"

#include "TGeoTube.h"

//______________________________________________________________________________
ClassImp(InSANETargetMaterial)

//______________________________________________________________________________
InSANETargetMaterial::InSANETargetMaterial(const char * name, const char * title, Int_t Z, Int_t A) 
   : TNamed(name, title), fMatID(-1), fPackingFraction(1.0)
{
   fZ = 1.0; 
   fA = 1.0;
   fN = 0.0;
   fDensity         = 0.072;
   fLength          = 1.0;
   fZposition       = 0.0;
   fIsPolarized     = false;
   fDFCoeff         = 0.0;
   fAtomicMass      = 1.0;
   fgEMC            = 1.0;
   SetNucleus(Z, A);

   fDISXSec         = nullptr;
   fElasticTail     = nullptr;
   fPi0DiffXSec    = nullptr;
   fPionDiffXSec    = nullptr;

   fTGeoMixture = new TGeoMixture(name,1,fDensity);
   fTGeoMixture->DefineElement(0,fA,fZ,1.0);

   fTGeoMedium   = new TGeoMedium(name,0,fTGeoMixture);

   fTGeoShape    = new TGeoTube( Form("%s-tube",name),0.0,1.5,fLength/2.0 ) ;

   fTGeoVolume   = new TGeoVolume(Form("%s-vol",name),fTGeoShape, fTGeoMedium);
   fTGeoMatrix   = new TGeoTranslation(0.0,0.0,fZposition);
   //fTGeoMatrix   = new TGeoIdentity();
   
   fBremRadiator = nullptr;
}
//______________________________________________________________________________
InSANETargetMaterial::~InSANETargetMaterial(){
}
//______________________________________________________________________________
InSANETargetMaterial::InSANETargetMaterial(const InSANETargetMaterial& v)
   : TNamed(v) {
   fMatID       = v.fMatID ;
   fZ           = v.fZ ;
   fA           = v.fA ;
   fN           = v.fN ;
   fDensity     = v.fDensity ;
   fLength      = v.fLength ;
   fZposition   = v.fZposition ;
   fIsPolarized = v.fIsPolarized ;
   fDFCoeff     = v.fDFCoeff ;
   fAtomicMass  = v.fAtomicMass ;
   fgEMC        = v.fgEMC ;
   fBremRadiator = v.fBremRadiator;
   fNucleus     = v.fNucleus;
}
//______________________________________________________________________________
InSANETargetMaterial& InSANETargetMaterial::operator=(const InSANETargetMaterial& v){
   if (this == &v)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   // Deallocate, allocate new space, copy values...
   TNamed::operator=(v);
   fMatID       = v.fMatID ;
   fZ           = v.fZ ;
   fA           = v.fA ;
   fN           = v.fN ;
   fDensity     = v.fDensity ;
   fLength      = v.fLength ;
   fZposition   = v.fZposition ;
   fIsPolarized = v.fIsPolarized ;
   fDFCoeff     = v.fDFCoeff ;
   fAtomicMass  = v.fAtomicMass ;
   fgEMC        = v.fgEMC ;
   fBremRadiator = v.fBremRadiator;
   fNucleus     = v.fNucleus;
   return *this;
//______________________________________________________________________________
}
void InSANETargetMaterial::InitGeo(){
}
//______________________________________________________________________________
void InSANETargetMaterial::Update()
{
   fTGeoMixture = new TGeoMixture(GetName(),1,fDensity);
   fTGeoMixture->DefineElement(0,fA,fZ,1.0);

   fTGeoMedium   = new TGeoMedium(GetName(),0,fTGeoMixture);

   fTGeoShape    = new TGeoTube( Form("%s-tube",GetName()),0.0,1.5,fLength/2.0 ) ;

   fTGeoVolume   = new TGeoVolume(Form("%s-vol",GetName()),fTGeoShape, fTGeoMedium);
   fTGeoMatrix   = new TGeoTranslation(0.0,0.0,fZposition);
}
//______________________________________________________________________________
void InSANETargetMaterial::SetNucleus(Int_t Z, Int_t A, Bool_t pol) {
   fZ = Z;
   fA = A;
   fN = A - Z;
   fIsPolarized = pol;
   fAtomicMass = (Double_t)fA; // not very accurate... but you can set it afterwards explicitly
   fNucleus = InSANENucleus(fZ,fA);
}
//______________________________________________________________________________
void InSANETargetMaterial::Print(const Option_t * opt ) const {
   std::cout << " - " << std::setw(20) << std::left << GetTitle() << "  (" <<  GetName() << ")  ID = " << fMatID << "\n"
      << std::setw(6)<< std::right << "  L="        << std::setw(8) << std::left << fLength
      << std::setw(5)<< std::right << ", rho="     << std::setw(8) << std::left << fDensity
      << std::setw(5)<< std::right << ", A="       << std::setw(3) << std::left << fA
      << std::setw(5)<< std::right << ", Z="       << std::setw(3) << std::left << fZ
      << std::setw(5)<< std::right << ", z_p="     << std::setw(4) << std::left << fZposition
      << std::setw(5)<< std::right << ", X0="      << std::setw(4) << std::left << GetRadiationLength() << " (g/cm^2)"
      << std::setw(5)<< std::right << ", t="      << std::setw(4) << std::left << GetNumberOfRadiationLengths() 
      << std::setw(7)<< std::right << ", rho*L*NA/ma=" << std::setw(7) << std::left << GetLengthDensity()
      << std::endl;
}
//______________________________________________________________________________

void InSANETargetMaterial::Print(std::ostream &stream) const {
   stream << " - " << std::setw(20) << std::left << GetTitle() << "  (" <<  GetName() << ")  ID = " << fMatID << "\n"
      << std::setw(6)<< std::right << "  L ="         << std::setw(8) << std::left << fLength
      << std::setw(5)<< std::right << ", rho ="     << std::setw(8) << std::left << fDensity
      << std::setw(5)<< std::right << ", A ="       << std::setw(3) << std::left << fA
      << std::setw(5)<< std::right << ", Z ="       << std::setw(3) << std::left << fZ
      << std::setw(5)<< std::right << ", z_p ="     << std::setw(4) << std::left << fZposition
      << std::setw(5)<< std::right << ", X0 ="      << std::setw(4) << std::left << GetRadiationLength() << " (g/cm^2)"
      << std::setw(5)<< std::right << ", t ="      << std::setw(4) << std::left << GetNumberOfRadiationLengths() 
      << std::setw(7)<< std::right << ", rho*L*NA/ma =" << std::setw(7) << std::left << GetLengthDensity()
      << std::endl;
}
//______________________________________________________________________________

Double_t InSANETargetMaterial::GetRate(const TParticle * beam, const TParticle * scat) {
   if(!beam) {
      return 0.0;
   }
   if(!scat) {
      return 0.0;
   }
   Double_t xsec_res = 0.0;

   double   xsec_args[3] = {scat->Energy(),scat->Theta(),scat->Phi()};
   Double_t beamEnergy   = beam->Energy();

   // ----------------------------------
   // Pi0 cross section 
   if( scat->GetPdgCode() == 111 ) {
      if(!fPi0DiffXSec ) {
         //InSANEInclusiveEPCVXSec * pixsec =  new InSANEInclusiveEPCVXSec();
         auto * pixsec =  new ElectroWiserDiffXSec();
         //fPionDiffXSec->SetBeamEnergy(beamEnergy);
         //fPionDiffXSec->SetTargetType(TargetType);
         pixsec->SetTargetNucleus(InSANENucleus(fZ,fA));
         pixsec->SetProductionParticleType(111);
         pixsec->InitializePhaseSpaceVariables();
         pixsec->InitializeFinalStateParticles();
         pixsec->UsePhaseSpace(false);
         fPi0DiffXSec = pixsec;
      }
      fPi0DiffXSec->SetBeamEnergy(beamEnergy);
      xsec_res = fPi0DiffXSec->EvaluateXSec(fPi0DiffXSec->GetDependentVariables(xsec_args));
      //scat->Print();
      //std::cout << " res " << res << std::endl;
   }

   return xsec_res*GetLengthDensity();
}
//______________________________________________________________________________
Double_t InSANETargetMaterial::GetRate(const Double_t x, const Double_t Q2) {

   InSANEStructureFunctions * funcs = InSANEFunctionManager::GetInstance()->GetStructureFunctions();
   Double_t F2p = funcs->F2p(x, Q2);
   Double_t F2n = funcs->F2n(x, Q2);
   Double_t res = GetLengthDensity() * (fZ * F2p + fN * F2n) * fgEMC;
   //Print();
   //std::cout << "   (x,Q2) = (" << x << "," << Q2 << ")" << std::endl;
   //std::cout << "      F2p = " << F2p << std::endl;
   //std::cout << "      F2n = " << F2n << std::endl;
   //std::cout << "      res = " << res << std::endl;

   double xarg[] = {x};
   double yarg[] = {Q2,0.0};

   // -----------------------------------
   if(!fDISXSec) {
      //fDISXSec = new InSANEInclusiveBornDISXSec();
      fDISXSec = new InSANERadiator<InSANEInclusiveBornDISXSec>();
      ((InSANERadiator<InSANEInclusiveBornDISXSec>*)fDISXSec)->SetRadiationLength(GetNumberOfRadiationLengths());
      fDISXSec->SetTargetNucleus(InSANENucleus(fZ,fA));
      fDISXSec->InitializePhaseSpaceVariables();
      fDISXSec->GetPhaseSpace()->GetVariableWithName("energy")->SetMinimum(0.01);
      fDISXSec->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(6.0);
      fDISXSec->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(0.017);
      fDISXSec->GetPhaseSpace()->GetVariableWithName("theta")->SetMaximum(TMath::Pi()-0.017);
      fDISXSec->InitializeFinalStateParticles();
      //fDISXSec->Dump();
   }
   fDISXSec->SetBeamEnergy(InSANEFunctionManager::GetInstance()->GetBeamEnergy());
   fDISXSec->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(InSANEFunctionManager::GetInstance()->GetBeamEnergy());
   fDISXSec->UsePhaseSpace(false);
   Double_t dis_xsec = fDISXSec->xDependentXSec(xarg,yarg);

   // only consider the proton's elastic tail now
   Double_t el_tail = 0.0;
   if( fZ == 1 && fA ==1 ) {
      if(!fElasticTail) {
         fElasticTail = new  InSANEElasticRadiativeTail();
         fElasticTail->SetPolarizations(0.0,0.0);
         fElasticTail->SetTargetNucleus(InSANENucleus::Proton());
         fElasticTail->InitializePhaseSpaceVariables();
         fElasticTail->GetPhaseSpace()->GetVariableWithName("energy")->SetMinimum(0.01);
         fElasticTail->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(6.0);
         fElasticTail->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(0.017);
         fElasticTail->GetPhaseSpace()->GetVariableWithName("theta")->SetMaximum(TMath::Pi()-0.017);
         fElasticTail->InitializeFinalStateParticles();
      }
      fElasticTail->SetTargetThickness(GetNumberOfRadiationLengths());
      fElasticTail->SetBeamEnergy(InSANEFunctionManager::GetInstance()->GetBeamEnergy());
      fElasticTail->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(InSANEFunctionManager::GetInstance()->GetBeamEnergy());
      fElasticTail->UsePhaseSpace(false);
      
      el_tail = fElasticTail->xDependentXSec(xarg,yarg);

   }
   //std::cout << x << " " << dis_xsec << " " << el_tail << std::endl;

   Double_t res2 = (dis_xsec + el_tail)*GetLengthDensity();

   //std::cout << "old f: " << res << std::endl;
   //std::cout << "new f: " << res2 << std::endl;

   return(res2);
}
