#include "InSANE_EG_Event.h"
#include "TList.h"

InSANE_EG_Event::InSANE_EG_Event() : 
   //fEventNumber(0),
   //fRunNumber(0),
   fXS_id(0), 
   fBeamPol(0.0), 
   fTargetPol(0.0),
   fXSec(nullptr)
{
   fParticles = new TClonesArray("InSANEParticle",3);
   fParticles->SetOwner(true);
}
//______________________________________________________________________________

InSANE_EG_Event::~InSANE_EG_Event()
{
   delete fParticles;
}
//______________________________________________________________________________

void InSANE_EG_Event::Clear()
{
   fArray.clear();
   fParticles->Clear();
   fPSVariables.clear();
}
//______________________________________________________________________________

InSANEParticle * InSANE_EG_Event::GetParticle(int i)
{
   if( i<fParticles->GetEntries() ) { 
      return( (InSANEParticle*)((*fParticles)[i]) );
   }
   return nullptr;
}
//______________________________________________________________________________

void InSANE_EG_Event::SetParticles(TList * l)
{
   Clear();
   if(l){
      int n = l->GetEntries();
      for(int i = 0; i<n ; i++){
         fArray.push_back((InSANEParticle*)l->At(i));
         new ((*fParticles)[i]) InSANEParticle(*((InSANEParticle*)l->At(i)));
      }
   }
}
//______________________________________________________________________________
void InSANE_EG_Event::SetPSVariables(double * vars, int n)
{
   fPSVariables.resize(n);
   for(int i = 0; i<n; i++){
      fPSVariables[i] = vars[i];
   }
}
//______________________________________________________________________________

InSANEParticle* InSANE_EG_Event::GetParticle(int i) const
{
   int n = fParticles->GetEntries();
   if( (i>=0) && (i<n) ){
      return (InSANEParticle*)fParticles->At(i);
   }
   return nullptr;
}
//______________________________________________________________________________

int InSANE_EG_Event::GetNParticles() const
{
   return fParticles->GetEntries();
}
//______________________________________________________________________________
