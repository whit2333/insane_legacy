#include "PDFBase.h"

namespace insane {
  namespace physics {

    PDFBase::PDFBase()
    { }
    //__________________________________________________________________________

    PDFBase::~PDFBase()
    { }
    //__________________________________________________________________________

    void PDFBase::Reset()
    {
      for(int i = 0; i<NPartons ; i++) {
        fValues[i]        = 0.0;
        fUncertainties[i] = 0.0;
      }
      fXbjorken = -1.0;
      fQsquared = 0.0;
    }
    //______________________________________________________________________________

    bool PDFBase::IsComputed(double x, double Q2) const
    {
      //if( x  != fXbjorken ) return false;
      //if( Q2 != fQsquared ) return false;
      //return true;
      return false;
    }
    //______________________________________________________________________________

    double  PDFBase::Get(const PartonFlavor& f, double x, double Q2) const 
    {
      //if( !IsComputed(x, Q2) ) {
      auto val = Calculate(x,Q2);
      auto unc = Uncertainties(x,Q2);
      //}
      return( val[int(f)] );
    }
    //______________________________________________________________________________
    
    //double  PDFBase::Get(const PartonFlavor& f) const 
    //{
    //  return( fValues[int(f)] );
    //}
    //______________________________________________________________________________
    
    std::array<double,NPartons> PDFBase::Calculate(double x, double Q2) const
    {
      // do calculations
      return fValues;
    }
    //______________________________________________________________________________

    std::array<double,NPartons> PDFBase::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }

  }
}


