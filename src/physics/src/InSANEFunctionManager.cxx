#include "InSANEFunctionManager.h"

#include "InSANEPhysics.h"

ClassImp(InSANEFunctionManager)

//________________________________________________________________________________
InSANEFunctionManager::InSANEFunctionManager() {
   //std::cout << "InSANEFunctionManager::InSANEFunctionManager" << std::endl;
   fPolarizedStructureFunctions = nullptr;
   fStructureFunctions = nullptr;
   fFormFactors = nullptr;
   fBeamEnergy = 5.9;

   // Unpolarized SFs
   fUnpolSFNames.Add(new TObjString(" 0 - F1F209"));
   fUnpolSFNames.Add(new TObjString(" 1 - CTEQ6"));
   fUnpolSFNames.Add(new TObjString(" 2 - NMC95"));
   fUnpolSFNames.Add(new TObjString(" 5 - BBS"));
   fUnpolSFNames.Add(new TObjString(" 6 - Statistical"));
   fUnpolSFNames.Add(new TObjString(" 7 - LCWF"));
   fUnpolSFNames.Add(new TObjString(" 9 - MAID07"));
   //fUnpolSFNames.Add(new TObjString("10 - F1F209-Quasielastic"));
   fUnpolSFNames.Add(new TObjString("11 - Composite (F1F209 + NMC)"));
   fUnpolSFNames.Add(new TObjString("12 - CJ12"));
   fUnpolSFNames.Add(new TObjString("13 - Stat2015"));
   fUnpolSFNames.Add(new TObjString("20 - F1F209-Inelastic"));
   fUnpolSFNames.Add(new TObjString("21 - F1F209-Quasielastic"));

   // Polarized SFs
   fPolSFNames.Add(new TObjString(" 0 - DSSV"));
   fPolSFNames.Add(new TObjString(" 1 - LSS2006"));
   fPolSFNames.Add(new TObjString(" 2 - AAC08"));
   fPolSFNames.Add(new TObjString(" 3 - BB"));
   fPolSFNames.Add(new TObjString(" 4 - DNS2005"));
   fPolSFNames.Add(new TObjString(" 5 - BBS"));
   fPolSFNames.Add(new TObjString(" 6 - Statistical"));
   fPolSFNames.Add(new TObjString(" 7 - LCWF"));
   fPolSFNames.Add(new TObjString(" 8 - LSS2010"));
   fPolSFNames.Add(new TObjString(" 9 - MAID07"));
   fPolSFNames.Add(new TObjString("11 - Composite (MAID + AAC)"));
   fPolSFNames.Add(new TObjString("12 - JAM"));
   fPolSFNames.Add(new TObjString("13 - Stat2015"));
   fPolSFNames.Add(new TObjString("14 - JAM15"));

   fFFNames.Add(new TObjString("0 Dipole     - default for all"));
   fFFNames.Add(new TObjString("1 AMT        - p Only"));
   fFFNames.Add(new TObjString("2 Bilenkaya  - p only"));
   fFFNames.Add(new TObjString("3 Galster    - n only"));
   fFFNames.Add(new TObjString("4 Kelly      - p and n"));
   fFFNames.Add(new TObjString("5 Riordan    - n (Kelly's p) "));
   fFFNames.Add(new TObjString("6 Amroun     - He3 only "));
   fFFNames.Add(new TObjString("7 MSW        - He3 only"));
   fFFNames.Add(new TObjString("8 Bosted     - p and n (and QE)"));
   fFFNames.Add(new TObjString("9 F1F209QE   - QE only"));

   // Call these so that they are defined.
   GetStructureFunctions();
   GetPolarizedStructureFunctions();
   GetFormFactors();
}
//________________________________________________________________________________
InSANEFunctionManager::~InSANEFunctionManager() {
   if (!fFormFactors) delete fFormFactors;
   if (!fStructureFunctions) delete fStructureFunctions ;
   if (!fPolarizedStructureFunctions) delete fPolarizedStructureFunctions ;
   fgInSANEFunctionManager = nullptr;
}
//________________________________________________________________________________
InSANEFunctionManager * InSANEFunctionManager::fgInSANEFunctionManager = nullptr;

//________________________________________________________________________________
InSANEStructureFunctions * InSANEFunctionManager::CreateSFs(Int_t iSF){
   InSANEStructureFunctions * sf = nullptr;

   switch(iSF) {

      case 0:
         sf = new F1F209StructureFunctions();
         break;

      case 1:
         sf = new InSANEStructureFunctionsFromPDFs();
         ((InSANEStructureFunctionsFromPDFs*)sf)->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
         break;

      case 2:
         sf = new NMC95StructureFunctions();
         break;

      case 5:
         sf = new InSANEStructureFunctionsFromPDFs();
         ((InSANEStructureFunctionsFromPDFs*)sf)->SetUnpolarizedPDFs(new BBSUnpolarizedPDFs());
         break;

      case 6:
         sf = new InSANEStructureFunctionsFromPDFs();
         ((InSANEStructureFunctionsFromPDFs*)sf)->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());
         ((InSANEStructureFunctionsFromPDFs*)sf)->SetUseR(true);
         break;

      case 7:
         sf = new InSANEStructureFunctionsFromPDFs();
         ((InSANEStructureFunctionsFromPDFs*)sf)->SetUnpolarizedPDFs(new LCWFPartonDistributionFunctions());
         break;

      case 9:
         sf = new InSANEStructureFunctionsFromVPCSs();
         break;

      //case 10:
      //   sf = new F1F209QuasiElasticStructureFunctions();
      //   break;

      case 11:
         sf = new LowQ2StructureFunctions();
         break;

      case 12:
         sf = new InSANEStructureFunctionsFromPDFs();
         ((InSANEStructureFunctionsFromPDFs*)sf)->SetUnpolarizedPDFs(new CJ12UnpolarizedPDFs());
         break;

      case 13:
         sf = new InSANEStructureFunctionsFromPDFs();
         ((InSANEStructureFunctionsFromPDFs*)sf)->SetUnpolarizedPDFs(new Stat2015UnpolarizedPDFs());
         break;

      case 20:
         sf = new F1F209StructureFunctions();
         ((F1F209StructureFunctions*)sf)->DoInelasticOnly(true);
         break;

      case 21:
         sf = new F1F209QuasiElasticStructureFunctions();
         break;

      default:
         sf = new InSANEStructureFunctionsFromPDFs();
         ((InSANEStructureFunctionsFromPDFs*)sf)->SetUnpolarizedPDFs(new Stat2015UnpolarizedPDFs());
         break;
   }

   if(sf) SetStructureFunctions(sf);
   return GetStructureFunctions();
}
//________________________________________________________________________________
InSANEPolarizedStructureFunctions * InSANEFunctionManager::CreatePolSFs(Int_t iSF){
   InSANEPolarizedStructureFunctions * sf = nullptr;

   switch(iSF) {
      case 0:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new DSSVPolarizedPDFs());
         break;

      case 1:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new LSS2006PolarizedPDFs());
         break;

      case 2:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new AAC08PolarizedPDFs());
         break;

      case 3:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new BBPolarizedPDFs());
         break;

      case 4:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new DNS2005PolarizedPDFs());
         break;

      case 5:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new BBSPolarizedPDFs());
         break;

      case 6:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new StatisticalPolarizedPDFs());
         break;

      case 7:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new LCWFPolarizedPartonDistributionFunctions());
         break;

      case 8:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new LSS2010PolarizedPDFs());
         break;

      case 9:
         sf = new InSANEPolarizedStructureFunctionsFromVCSAs();
         ((InSANEPolarizedStructureFunctionsFromVCSAs*)sf)->SetVCSAs(new MAIDVirtualComptonAsymmetries());
         break;

      case 11:
         sf = new LowQ2PolarizedStructureFunctions();
         break;

      case 12:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new JAMPolarizedPDFs());
         break;

      case 13:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new Stat2015PolarizedPDFs());
         break;

      case 14:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new JAM15PolarizedPDFs());
         break;

      default:
         sf = new InSANEPolarizedStructureFunctionsFromPDFs();
         ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(new Stat2015PolarizedPDFs());
         break;
   }

   if(sf) SetPolarizedStructureFunctions(sf);
   return GetPolarizedStructureFunctions();
}

//________________________________________________________________________________
InSANEFormFactors *  InSANEFunctionManager::CreateFFs(Int_t iSF){
   // Add all form factors here!!!!
   InSANEFormFactors * ff = nullptr;
   //fFFNames.Add(new TObjString("0 Dipole     - default for all"));
   //fFFNames.Add(new TObjString("1 AMT        - p Only"));
   //fFFNames.Add(new TObjString("2 Bilenkaya  - p only"));
   //fFFNames.Add(new TObjString("3 Galster    - n only"));
   //fFFNames.Add(new TObjString("4 Kelly      - p and n"));
   //fFFNames.Add(new TObjString("5 Riordan    - n (Kelly's p) "));
   //fFFNames.Add(new TObjString("6 Amroun     - He3 only "));
   //fFFNames.Add(new TObjString("7 MSW        - He3 only"));
   //fFFNames.Add(new TObjString("8 Bosted     - p and n (and QE)"));
   //fFFNames.Add(new TObjString("9 F1F209QE   - QE only"));
   switch(iSF) {
      case 0:
         ff = new InSANEDipoleFormFactors();
         break;

      case 1:
         ff = new AMTFormFactors();
         break;

      case 2:
         ff = new BilenkayaFormFactors();
         break;

      case 3:
         ff = new GalsterFormFactors();
         break;

         
      case 4:
         ff = new KellyFormFactors();
         break;

      case 5:
         ff = new RiordanFormFactors();
         break;

      case 6:
         ff = new AmrounFormFactors();
         break;

      case 7:
         ff = new MSWFormFactors();
         break;

      case 8:
         ff = new BostedFormFactors();
         break;

      case 9:
         ff = new F1F209QuasiElasticFormFactors();
         break;


      default:
         ff = new KellyFormFactors();
         break;


   }
   if(ff) SetFormFactors(ff);
   return GetFormFactors();
}

//________________________________________________________________________________
InSANEStructureFunctions * InSANEFunctionManager::GetStructureFunctions() {
   if (!fStructureFunctions){
      //fStructureFunctions = (InSANEStructureFunctions *) new F1F209StructureFunctions();
      fStructureFunctions = (InSANEStructureFunctions*) new LowQ2StructureFunctions();
      fUnpolSFList.AddFirst(fStructureFunctions);
   }
   return fStructureFunctions;
}

//________________________________________________________________________________
void InSANEFunctionManager::SetStructureFunctions(InSANEStructureFunctions * SFs) {
   if (SFs) {
      if(SFs != fStructureFunctions){
         fStructureFunctions = SFs;
         fUnpolSFList.AddFirst(fStructureFunctions);
      }
   } else {
      std::cout << " x SetStructureFunctions argument is null pointer!\n";
   }
}

//________________________________________________________________________________
InSANEPolarizedStructureFunctions * InSANEFunctionManager::GetPolarizedStructureFunctions() {
   if (!fPolarizedStructureFunctions) {
      fPolarizedStructureFunctions = (InSANEPolarizedStructureFunctions *) new InSANEPolarizedStructureFunctionsFromPDFs();
      ((InSANEPolarizedStructureFunctionsFromPDFs*)fPolarizedStructureFunctions)->SetPolarizedPDFs(new BBPolarizedPDFs());
      fPolSFList.AddFirst(fPolarizedStructureFunctions);
      //((InSANEPolarizedStructureFunctionsFromPDFs*)fPolarizedStructureFunctions)->SetPolarizedPDFs(new BBSPolarizedPDFs);
   }
   return fPolarizedStructureFunctions;
}

//________________________________________________________________________________
void InSANEFunctionManager::SetPolarizedStructureFunctions(InSANEPolarizedStructureFunctions * SFs) {
   if (SFs) {
      if( SFs != fPolarizedStructureFunctions ) {
         fPolarizedStructureFunctions = SFs;
         fPolSFList.AddFirst(fPolarizedStructureFunctions);
      }
   } else {
      std::cout << " x SetPolarizedStructureFunctions argument is null pointer!\n";
   }
}

//________________________________________________________________________________
InSANEFormFactors * InSANEFunctionManager::GetFormFactors() {
   if (!fFormFactors){
      fFormFactors = new InSANEDipoleFormFactors();
      fFFList.AddFirst(fFormFactors);
   }
   return fFormFactors;
}

//________________________________________________________________________________
void InSANEFunctionManager::SetFormFactors(InSANEFormFactors * FFs) {
   if (FFs) {
      if( FFs != fFormFactors ) {
         fFormFactors = FFs;
         fFFList.AddFirst(fFormFactors);
      } else {
         std::cout << " x SetFormFactors argument is null pointer!\n";
      }
   }
}

//________________________________________________________________________________
void InSANEFunctionManager::PrintStatus() const {
   std::cout << "---------------------------------------------" << std::endl;
   std::cout << "Current structure functions and form factors " << std::endl;
   if(fStructureFunctions) std::cout << " unpol. SFs : " << fStructureFunctions->GetLabel() << std::endl;
   if(fPolarizedStructureFunctions) std::cout << " pol.   SFs : " << fPolarizedStructureFunctions->GetLabel() << std::endl;
   if(fFormFactors) std::cout << "        FFs : " << fFormFactors->GetLabel() << std::endl;
   fUnpolSFList.Print();
   fPolSFList.Print();
   fFFList.Print();
}

//________________________________________________________________________________
void InSANEFunctionManager::PrintNameLists() const {
   TObjString * opt = nullptr;
   std::cout << "Unpolarized structure functions:" << std::endl;
   for(int i = 0; i<fUnpolSFNames.GetEntries(); i++) {
      opt = (TObjString*)fUnpolSFNames.At(i);
      std::cout << opt->GetString() << std::endl;
   }

   std::cout << "Polarized structure functions:" << std::endl;
   for(int i = 0; i<fPolSFNames.GetEntries(); i++) {
      opt = (TObjString*)fPolSFNames.At(i);
      std::cout << opt->GetString() << std::endl;
   }

   std::cout << "Form Factors:" << std::endl;
   for(int i = 0; i<fFFNames.GetEntries(); i++) {
      opt = (TObjString*)fFFNames.At(i);
      std::cout << opt->GetString() << std::endl;
   }
}
//______________________________________________________________________________
void InSANEFunctionManager::Print(Option_t * opt) const {
   PrintNameLists();
   PrintStatus();
}

//________________________________________________________________________________

