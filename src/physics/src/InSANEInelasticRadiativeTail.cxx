#include "InSANEInelasticRadiativeTail.h"


InSANEInelasticRadiativeTail::InSANEInelasticRadiativeTail()
{
   fID = 100010012;
   SetTitle("InSANEInelasticRadiativeTail");//,"POLRAD Born cross-section");
   SetPlotTitle("Inelastic Radiative Tail cross-section");

   fLabel = "#frac{d#sigma}{dEd#Omega}";
   fUnits = "nb/GeV/sr";
   fAddRegion4 = false;
   fInternalOnly = false;

   fRadLen[0] = 0.05; 
   fRadLen[1] = 0.05; 

   //fDiffXSec0 = new InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec0 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec0->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec0->InitializePhaseSpaceVariables();
   fDiffXSec0->InitializeFinalStateParticles();

   GetRADCOR()->SetUnpolarizedCrossSection(fDiffXSec0); 
   GetRADCOR()->SetRadiationLengths(fRadLen);

}
//________________________________________________________________________________
InSANEInelasticRadiativeTail::~InSANEInelasticRadiativeTail(){


}
//________________________________________________________________________________

ClassImp(InSANEFullInelasticRadiativeTail)
//________________________________________________________________________________
InSANEFullInelasticRadiativeTail::InSANEFullInelasticRadiativeTail(){
   
   SetTitle("InSANEFullInelasticRadiativeTail");//,"POLRAD Born cross-section");
   SetPlotTitle("Full Inelastic Radiative Tail cross-section");

   fLabel = "#frac{d#sigma}{dEd#Omega}";
   fUnits = "nb/GeV/sr";

   fRadLen[0] = 0.025; 
   fRadLen[1] = 0.025; 

   fDiffXSec = new InSANEInelasticRadiativeTail();
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();

   fDiffXSec0 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec0->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec0->InitializePhaseSpaceVariables();
   fDiffXSec0->InitializeFinalStateParticles();

   fRADCOR->SetUnpolarizedCrossSection(fDiffXSec0); 

   fRADCOR->SetRadiationLengths(fRadLen);


}
//________________________________________________________________________________
InSANEFullInelasticRadiativeTail::~InSANEFullInelasticRadiativeTail(){


}
//________________________________________________________________________________




