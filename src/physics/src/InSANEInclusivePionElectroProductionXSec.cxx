#include "InSANEInclusivePionElectroProductionXSec.h"
#include "InSANEFortranWrappers.h"
#include "InSANEMathFunc.h"
#include "WiserXSection.h"
#include "WiserInclusivePhotoXSec.h"


//==============================================================================

InSANEInclusivePionElectroProductionXSec::InSANEInclusivePionElectroProductionXSec()
{ 
   fID         = 1200;
   fPIDs.clear();
   fPIDs.push_back(111);   // pi0

   fSig_0     = new InSANEPhotonDiffXSec();
   fSig_plus  = new InSANEPhotonDiffXSec();
   fSig_minus = new InSANEPhotonDiffXSec();

   fSig_0->SetProductionParticleType(111);
   fSig_plus->SetProductionParticleType(211);
   fSig_minus->SetProductionParticleType(-211);
}
//______________________________________________________________________________

InSANEInclusivePionElectroProductionXSec::~InSANEInclusivePionElectroProductionXSec()
{
   delete fSig_0     ;
   delete fSig_plus  ;
   delete fSig_minus ;
}
//______________________________________________________________________________

InSANEInclusivePionElectroProductionXSec::InSANEInclusivePionElectroProductionXSec(const InSANEInclusivePionElectroProductionXSec& rhs) : 
   InSANEPhotonDiffXSec(rhs)
{
   (*this) = rhs;
}
//______________________________________________________________________________

InSANEInclusivePionElectroProductionXSec& InSANEInclusivePionElectroProductionXSec::operator=(const InSANEInclusivePionElectroProductionXSec& rhs) 
{
   if (this != &rhs) {  // make sure not same object
      InSANEPhotonDiffXSec::operator=(rhs);
      fSig_0     = rhs.fSig_0->Clone();
      fSig_plus  = rhs.fSig_plus->Clone();
      fSig_minus = rhs.fSig_minus->Clone();
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

InSANEInclusivePionElectroProductionXSec*  InSANEInclusivePionElectroProductionXSec::Clone(const char * newname) const 
{
   std::cout << "InSANEInclusivePionElectroProductionXSec::Clone()\n";
   auto * copy = new InSANEInclusivePionElectroProductionXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

InSANEInclusivePionElectroProductionXSec*  InSANEInclusivePionElectroProductionXSec::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________

void  InSANEInclusivePionElectroProductionXSec::SetParameters( int i, const std::vector<double>& pars )
{
   if(i == 0 ) {      fParameters0 = pars; fSig_0->fParameters0 = pars;     fSig_0->SetParameters(pars);     fSig_0->SetProductionParticleType(fSig_0->GetParticleType());}  
   else if(i == 1 ) { fParameters1 = pars; fSig_plus->fParameters1 = pars;  fSig_plus->SetParameters(pars);  fSig_plus->SetProductionParticleType(fSig_plus->GetParticleType());}  
   else if(i == 2 ) { fParameters2 = pars; fSig_minus->fParameters2 = pars; fSig_minus->SetParameters(pars); fSig_minus->SetProductionParticleType(fSig_minus->GetParticleType());}  
   else  { 
      fParameters0 = pars; fSig_0->fParameters0 = pars;     fSig_0->SetParameters(pars); fSig_0->SetProductionParticleType(fSig_0->GetParticleType());
      fParameters1 = pars; fSig_plus->fParameters1 = pars;  fSig_plus->SetParameters(pars); fSig_plus->SetProductionParticleType(fSig_plus->GetParticleType());
      fParameters2 = pars; fSig_minus->fParameters2 = pars; fSig_minus->SetParameters(pars); fSig_minus->SetProductionParticleType(fSig_minus->GetParticleType()); 
   }  
   // this is needed so that fParameters is set to the correct vector above.
   SetProductionParticleType(GetParticleType());
   //std::cout << "p0 = " << pars[0] << std::endl;
}
//______________________________________________________________________________

const std::vector<double>& InSANEInclusivePionElectroProductionXSec::GetParameters() const 
{
   return fParameters;
}
//______________________________________________________________________________

void  InSANEInclusivePionElectroProductionXSec::SetPhaseSpace(InSANEPhaseSpace * ps)
{
   fSig_0->SetPhaseSpace(ps);
   fSig_plus->SetPhaseSpace(ps);
   fSig_minus->SetPhaseSpace(ps);
   InSANEInclusiveDiffXSec::SetPhaseSpace(ps);
}
//______________________________________________________________________________

void InSANEInclusivePionElectroProductionXSec::InitializeFinalStateParticles()
{
   if(fSig_0)  fSig_0->InitializeFinalStateParticles();
   if(fSig_plus) fSig_plus->InitializeFinalStateParticles();
   if(fSig_minus) fSig_minus->InitializeFinalStateParticles();
   InSANEInclusiveDiffXSec::InitializeFinalStateParticles();
}
//_____________________________________________________________________________

void InSANEInclusivePionElectroProductionXSec::InitializePhaseSpaceVariables() 
{
   fSig_0->InitializePhaseSpaceVariables()    ;
   InSANEPhaseSpace * ps = fSig_0->GetPhaseSpace();
   fSig_plus->SetPhaseSpace(ps);
   fSig_minus->SetPhaseSpace(ps);
   InSANEInclusiveDiffXSec::SetPhaseSpace(ps);
}
//______________________________________________________________________________

Double_t InSANEInclusivePionElectroProductionXSec::EvaluateXSec(const Double_t * x) const 
{

   double RES            = 0.0;
   int    PART           = fParticle->PdgCode();
   if( GetTargetNucleus() == InSANENucleus::Neutron() ) {
      // if target is a neutron use isospin to get the result
      if(fParticle->PdgCode() != 111) {
         // make sure it is not set to pi0
         PART = -1*fParticle->PdgCode();
      }
   } else if( !(GetTargetNucleus() == InSANENucleus::Proton()) ) {
      std::cout << " NOT Proton or neutron" << std::endl;
   }

   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];

   // Solution to Tiator-wright Eqn.5 with theta_e=0
   double num = TMath::Power(Ppart,2.0) + TMath::Power(M_p/GeV,2.0) - TMath::Power(M_p/GeV - Epart, 2.0);
   double denom = 2.0*(Ppart*TMath::Cos(THETA) + M_p/GeV - Epart);

   double AMT = M_p/MeV;//is target mass?
   double AM1 = M_pion/MeV;//is produced particle mass (MeV/c2)
   double EI  = EBEAM*1000.0;//is the electron beam energy (MeV)
   double W0  = (num/denom)*1000.0;//is the theta=0 photon energy (MeV)
   double TP  = (Epart - M_pion/GeV)*1000.0;//is the kinetic energy of the produced particle (MeV)
   double TH  = THETA;//is the angle of the produced particle (radians)
   double GN  = 0.0;//the result of Ne*R/w0 of eqn.12 in Nuc.Phys.A379

   if( W0<0.0 || W0>EI ) {
      GN=0.0;
   } else {
      vtp_(&AMT,&AM1,&EI,&W0,&TP,&TH,&GN);
      GN *= 1000.0; // because GN has units 1/MeV
   }

   //std::cout << "W0 = " << W0 << std::endl;
   //std::cout << "GN = " << GN << std::endl;
   //wiser_all_sig_(&RES,&PART,&EBEAM,&PscatteredPart,&THETA);
   double tot = 0.0;
   if(PART==111){
      // pi0 = (pi+ + pi-)/2.0
      fSig_plus->SetBeamEnergy(EBEAM);
      RES = fSig_plus->EvaluateXSec(x);
      if(RES<0.0) RES = 0.0;
      tot  += RES;

      fSig_minus->SetBeamEnergy(EBEAM);
      RES = fSig_minus->EvaluateXSec(x);

      if(RES<0.0) RES = 0.0;
      tot  += RES;
      tot = tot/2.0;

   } else if( PART==211 ){

      fSig_plus->SetBeamEnergy(EBEAM);
      RES = fSig_plus->EvaluateXSec(x);

      if(RES<0.0) RES = 0.0;
      tot  += RES;

   } else if( PART==-211 ){

      fSig_minus->SetBeamEnergy(EBEAM);
      RES = fSig_minus->EvaluateXSec(x);

      if(RES<0.0) RES = 0.0;
      tot  += RES;

   }

   // RES is Ex*dsig/dp^3 with units of GeV-ub/(GeV/c)^2
   if(tot<0.0) tot = 0.0;
   //tot *= (Ppart); // Takes dp^3 to dEdOmega 
   tot *= GN*1000; // conv
   if( TMath::IsNaN(tot) ) tot = 0.0;
   //std::cout << " wiser result is " << RES << " nb/GeV*str "
   //<< " for p,theta " << PscatteredPart << "," << THETA << "\n";
   if ( IncludeJacobian() ) return(tot * TMath::Sin(x[1]) ); 
   return(tot); // converts nb to mb
}
//______________________________________________________________________________

