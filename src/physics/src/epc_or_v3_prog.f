      !>  EPCV subroutine to get single differential cross section.
      !!  Used for linking with C programs. Not used in original program.  
      !!
      !!  -Added 1/10/2014 by Whitney Armstrong (whit@temple.edu)
      !!
      !!  @param PART Particle maybe either 'P','N','PI+','PI-','PI0'
      !!  @param Z    Number of protons in nuclei
      !!  @param N    Number of neturons in nuclei
      !!  @param E1   Beam Energy in MeV
      !!  @param PTP  Scattered particle momentum in MeV/c
      !!  @param THP  Scattered particle angle in degrees
      !!  @param RES  Returned cross section result in ub/Mev-Sr
      !!
      !!  @ingroup EPCV
      !!   
      !!  vax version
      !!  electroproduction yields of nucleons and pions 
      !!  written by j.s. oconnell and j.w. lightbody, jr. 
      !!  national bureau of standards 
      !!  april 1988 
      !!  transverse scaling region added
      !! 	modified by oara to plot like older epc's
      !!       modified slightly by glen warren to compile under linux (g77) sept. 02
      SUBROUTINE EPCV_SINGLE_V3(string,len_string, ZZ,NN,E1,PTP,THP,RES)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DOUBLE PRECISION NN
      DOUBLE PRECISION N
      CHARACTER*3 PART
      COMMON/QD/QDF 
      COMMON/DEL/IP 
      COMMON/M/Z,N
      COMMON/SP/IA
      COMMON/SG/IA3
      !common/sg/ia3
      common/fer/fermi,scal
      common/kf/e11
C      DIMENSION C(0:4,8) 
C      DIMENSION A(0:4) 
C      DATA PI/3.1416/,AM/938.28/,AMD/1875.63/,AMP/139.6/
      BYTE          string(4)
      INTEGER       len_string
      real*8 mp,mpi0,mn
      character*1 topa,ans,mpi,scal,fermi,mort,repe,units
      logical*1 mom
      dimension c(0:4,8),x_plt(125),s_qd(125),s_qf(125),s_del(125) 
      dimension a(0:4),s_tot(125),s_dsc(125)
      data am/938.28/,amp/139.6/,mpi0/135.9/,mn/939.56/
      WRITE (PART,'(3a)') (string(i),i=1,len_string)
!       WRITE(*,*)' PART is ',PART
      e11=e1
      Z=ZZ
      N=NN 
      IA=Z+N
      pi=acos(-1.0d0)
      ia3=IA
c      program epc
      p=PTP


C  'AN' IS EFFECTIVE NUMBER OF NUCLEONS FOR PION PRODUCTION 
      IF(PART.EQ.'P')THEN 
         AN=N/3.+2.*Z/3.
	 mp=am
         IP=1 
      ELSEIF(PART.EQ.'N')THEN 
         AN=Z/3.+2.*N/3.
	 mp=mn
         IP=-1
      ELSEIF(PART.EQ.'PI+')THEN 
         AN=Z/3.
	 mp=amp
         IP=2 
      ELSEIF(PART.EQ.'PI-')THEN 
         AN=N/3.
	 mp=amp
         IP=2 
      ELSEIF(PART.EQ.'PI0')THEN 
         AN=2.*(N+Z)/3. 
	 mp=mpi0
         IP=2 
      ELSE
         write(*,*)' ERROR IN PART ',PART
         write(*,*)' ASSUMING PI0 '
         PART = 'PI0'
         AN=2.*(N+Z)/3. 
         IP=2 
C         STOP 
      ENDIF 


c      if(ia.eq.2.or.ia.eq.3.or.ia.eq.4.or.ia.eq.12.or.ia.eq.16)then
c	print *
c        print 106,ia 
c106	format(' (using a =',i3,' spectral function)')
c      elseif(ia.eq.1)then 
c        print *, ' nucleon case' 
c      else
c	print * 
c        print 107
c107    	format(' no specific spectral function for this nucleus')
c	if(ia.gt.12) print 108
c108    	format(' a = 16 spectral function  will be used') 
c      endif 
c	loop over particles
c2	continue
        ! FIX THIS LATER ADD ARGUMENT
	!print *
	!print *,'target nucleons at rest (return) or fermi motion ("y")?'
	!read(*,101) fermi
        fermi=''
	!print *
	!print *,'virtual (return) or real photons ("y")?'
	!read(*,101) scal
        scal=''
!	mpi = 'y'
!	if(scal.eq.'y') then
	!print *
	!print *,'include multi-pion production ("y") or not (return)?'
	!read(*,101) mpi
!	end if
        mpi='y'
c	print * 
c	part = '   '
c	print 100 
c100	format('$enter particle type [p,n,pi+,pi-,pi0] '//
c     1         '(return to exit) > ')
c	read(5,201) part
c101 	format(a1)
c 201	format(a3)
c      print *,'part=',part
c      print *,'Z=',Z
c      print *,'N=',N
      if(abs(ip).eq.1)then
         if(ia.eq.1)then 
            dlf = 0
         else
            if(ia.gt.1.and.ia.lt.5)then
               dlf=ia
            else 
               dlf=7.
            endif
            al=dlf 
            qdf=al*n*z/float(ia) 
c	  print * 
c          print 102
c102	  format(' enter levinger factor or 0 for default value')
c          read *,al
            al=0
            if(al.eq.0.) al=dlf 
            qdf=al*n*z/float(ia) 
         endif
      endif 
c      print *,'qdf=',qdf
c	select one p/theta or multiple values (only plot output)
	!print *
	!print *,' momentum ("m") or kinetic energy (return)?'
	!read(*,101) mort
        mort='m'
c      print *,'mp=',mp
c	if(mort.eq.'m') then
c11	continue
c	print *
c	print 104 
c104 	format(' enter particle momentum [mev/c], angle [deg] ') 
c	read *,p,thp
C        if(p.le.0.)go to 11
c	else
c
c12	continue
c	print *
c	print 1040 
c1040 	format(' enter particle kinetic energy [mev], angle [deg] ') 
c	read *,tke,thp
c	p = sqrt(tke**2 + 2*mp*tke)
c	print *,' particle momentum: ',p,' [mev/c]'
c        if(tke.le.0.)go to 12
c
c	endif

c	print * 
c	if(scal.eq.'y') then
c	write(6,1090) part 
c1090 	format(' (e,',a3,') cross sections in ub.c/(q.(gev/c)^2.sr)') 
c	else
c	write(6,109) part 
c109 	format(' (e,',a3,') cross sections in ub/((mev/c).sr)') 
c	end if
	th=thp*pi/180.
c	print * 

c      print *,'th=',th
c      print *,'thp=',thp
c      print *,'e1=',e1
c      print *,'e12=',e11
c      print *,'ip=',ip
      if(abs(ip).eq.1)then
         e=sqrt(p**2+am**2) 
         tp=p**2/(e+am) 
         aj=p/e		! converts cross section from 1/mev to 1/mev/c
         if(ia.eq.1)then
            d2qd=0. 
            d2qf=0. 
         elseif(ia.gt.1)then
            call dep_3(e1,tp,th,ip,d2qd)
c            print *,'d2qd=',d2qd
	 if(scal.ne.'y') then
            d2qd=d2qd*aj
	 endif
C            print 110,d2qd
C110     format('      quasi-deuteron =  ',e10.3)
	  if(scal.ne.'y') then
            call ep_3(e1,tp,th,d2qf)
            d2qf=d2qf*aj
c            print *,'d2qf=',d2qf
c112     format('          quasi-free =  ',e10.3)
	  endif
         endif
      elseif(abs(ip).eq.2.or.ip.eq.0)then 
         e=sqrt(p**2+amp**2)
         tp=p**2/(e+amp)
         aj=p/e 
         d2qd=0.
         d2qf=0.
      else
	!call exit
      endif 
      if(p.lt.500.)then 
         d2sc=0.
         call delta_3(e1,tp,th,d2del) 
c         print *,'tp=',tp
c         print *,'d2del=',d2del
	 if(scal.eq.'y') then
         d2del=an*d2del
	 else
         d2del=an*d2del*aj
	 endif
C         print 111,d2del
C111	format('               delta =  ',e10.3) 
C         print *
      else
         d2del=0. 
	if(mpi.eq.'y') then
         if(abs(ip).eq.2.or.ip.eq.0)then
c            print *,'made it'
            call s2pi(2,e1,tp,th,d2sc1) 
            call s2pi(-2,e1,tp,th,d2sc2) 
c            print *,d2sc1
            if(part.eq.'PI+')then 
               d2sc=z*d2sc1+n*d2sc2 
            elseif(part.eq.'PI-')then 
               d2sc=n*d2sc1+z*d2sc2 
            elseif(part.eq.'PI0')then 
               d2sc=ia*d2sc1
            else
            !   call exit
            endif 
c            print *,'made it2'
         elseif(abs(ip).eq.1)then 
            call s2pi(1,e1,tp,th,d2sc1) 
            d2sc=ia*d2sc1 
         endif
	endif
      endif 
c            print *,'made it3'
C      print 117,d2sc
C  117 format('                d2sc =  ',e10.3)
      total=d2qd+d2qf+d2del+d2sc
c      print * 
c      print 113,total 
c  113 format(' total cross section =  ',e10.3)
c      print * 
c      print 115,tp,p
C  115 format(' kinetic energy = ',f10.2,' mev.  momentum = ',f10.2,
C	1 ' mev/c') 
!	1	' mev/c') 
	if(scal.eq.'y') then
c      totale=total*p*1.0d-6
c	print 1160,totale
c1160	format(' total in  ub/(q.mev.sr) = ',g16.3)
	else
c      print *,'total=',total
      totale=total/aj 
c      print *,'totale=',totale
c	print 116,totale
c  116 format(' total in  ub/(mev.sr) = ',g16.3)
	endif
C        print * 
C	print *
C	print *,'repeat same a, e_e, particle ("y") or not (return)?'
C	read(*,101) repe
C	if(repe.eq.'y') then
C        go to 1
C	else
C        go to 2
C	endif
c	plot output
      res=total
      end 

      !>  EPCV subroutine to get single differential cross section.
      !!  Used for linking with C programs. Not used in original program.  
      !!
      !!  -Added 1/10/2014 by Whitney Armstrong (whit@temple.edu)
      !!
      !!  @param PART Particle maybe either 'P','N','PI+','PI-','PI0'
      !!  @param Z    Number of protons in nuclei
      !!  @param N    Number of neturons in nuclei
      !!  @param E1   Beam Energy in MeV
      !!  @param PTP  Scattered particle momentum in MeV/c
      !!  @param THP  Scattered particle angle in degrees
      !!  @param RES  Returned cross section result in ub/Mev-Sr
      !!
      !!  @ingroup EPCV
      !!   
      !!  vax version
      !!  electroproduction yields of nucleons and pions 
      !!  written by j.s. oconnell and j.w. lightbody, jr. 
      !!  national bureau of standards 
      !!  april 1988 
      !!  transverse scaling region added
      !! 	modified by oara to plot like older epc's
      !!       modified slightly by glen warren to compile under linux (g77) sept. 02
      SUBROUTINE GPC_SINGLE_V3(string,len_string, ZZ,NN,E1,PTP,THP,RES)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DOUBLE PRECISION NN
      DOUBLE PRECISION N
      CHARACTER*3 PART
      COMMON/QD/QDF 
      COMMON/DEL/IP 
      COMMON/M/Z,N
      COMMON/SP/IA
      COMMON/SG/IA3
      !common/sg/ia3
      common/fer/fermi,scal
      common/kf/e11
C      DIMENSION C(0:4,8) 
C      DIMENSION A(0:4) 
C      DATA PI/3.1416/,AM/938.28/,AMD/1875.63/,AMP/139.6/
      BYTE          string(4)
      INTEGER       len_string
      real*8 mp,mpi0,mn
      character*1 topa,ans,mpi,scal,fermi,mort,repe,units
      logical*1 mom
      dimension c(0:4,8),x_plt(125),s_qd(125),s_qf(125),s_del(125) 
      dimension a(0:4),s_tot(125),s_dsc(125)
      data am/938.28/,amp/139.6/,mpi0/135.9/,mn/939.56/
      WRITE (PART,'(3a)') (string(i),i=1,len_string)
!       WRITE(*,*)' PART is ',PART
      e11=e1
      Z=ZZ
      N=NN 
      IA=Z+N
      pi=acos(-1.0d0)
      ia3=IA
c      program epc
      p=PTP


C  'AN' IS EFFECTIVE NUMBER OF NUCLEONS FOR PION PRODUCTION 
      IF(PART.EQ.'P')THEN 
         AN=N/3.+2.*Z/3.
	 mp=am
         IP=1 
      ELSEIF(PART.EQ.'N')THEN 
         AN=Z/3.+2.*N/3.
	 mp=mn
         IP=-1
      ELSEIF(PART.EQ.'PI+')THEN 
         AN=Z/3.
	 mp=amp
         IP=2 
      ELSEIF(PART.EQ.'PI-')THEN 
         AN=N/3.
	 mp=amp
         IP=2 
      ELSEIF(PART.EQ.'PI0')THEN 
         AN=2.*(N+Z)/3. 
	 mp=mpi0
         IP=2 
      ELSE
         write(*,*)' ERROR IN PART ',PART
         write(*,*)' ASSUMING PI0 '
         PART = 'PI0'
         AN=2.*(N+Z)/3. 
         IP=2 
C         STOP 
      ENDIF 


c      if(ia.eq.2.or.ia.eq.3.or.ia.eq.4.or.ia.eq.12.or.ia.eq.16)then
c	print *
c        print 106,ia 
c106	format(' (using a =',i3,' spectral function)')
c      elseif(ia.eq.1)then 
c        print *, ' nucleon case' 
c      else
c	print * 
c        print 107
c107    	format(' no specific spectral function for this nucleus')
c	if(ia.gt.12) print 108
c108    	format(' a = 16 spectral function  will be used') 
c      endif 
c	loop over particles
c2	continue
        ! FIX THIS LATER ADD ARGUMENT
	!print *
	!print *,'target nucleons at rest (return) or fermi motion ("y")?'
	!read(*,101) fermi
        fermi=''
	!print *
	!print *,'virtual (return) or real photons ("y")?'
	!read(*,101) scal
        scal='y'
!	mpi = 'y'
!	if(scal.eq.'y') then
	!print *
	!print *,'include multi-pion production ("y") or not (return)?'
	!read(*,101) mpi
!	end if
        mpi='y'
c	print * 
c	part = '   '
c	print 100 
c100	format('$enter particle type [p,n,pi+,pi-,pi0] '//
c     1         '(return to exit) > ')
c	read(5,201) part
c101 	format(a1)
c 201	format(a3)
      print *,'part=',part
      print *,'Z=',Z
      print *,'N=',N
      if(abs(ip).eq.1)then
         if(ia.eq.1)then 
            dlf = 0
         else
            if(ia.gt.1.and.ia.lt.5)then
               dlf=ia
            else 
               dlf=7.
            endif
            al=dlf 
            qdf=al*n*z/float(ia) 
c	  print * 
c          print 102
c102	  format(' enter levinger factor or 0 for default value')
c          read *,al
            al=0
            if(al.eq.0.) al=dlf 
            qdf=al*n*z/float(ia) 
         endif
      endif 
      print *,'qdf=',qdf
c	select one p/theta or multiple values (only plot output)
	!print *
	!print *,' momentum ("m") or kinetic energy (return)?'
	!read(*,101) mort
        mort='m'
      print *,'mp=',mp
c	if(mort.eq.'m') then
c11	continue
c	print *
c	print 104 
c104 	format(' enter particle momentum [mev/c], angle [deg] ') 
c	read *,p,thp
C        if(p.le.0.)go to 11
c	else
c
c12	continue
c	print *
c	print 1040 
c1040 	format(' enter particle kinetic energy [mev], angle [deg] ') 
c	read *,tke,thp
c	p = sqrt(tke**2 + 2*mp*tke)
c	print *,' particle momentum: ',p,' [mev/c]'
c        if(tke.le.0.)go to 12
c
c	endif

c	print * 
c	if(scal.eq.'y') then
c	write(6,1090) part 
c1090 	format(' (e,',a3,') cross sections in ub.c/(q.(gev/c)^2.sr)') 
c	else
c	write(6,109) part 
c109 	format(' (e,',a3,') cross sections in ub/((mev/c).sr)') 
c	end if
	th=thp*pi/180.
c	print * 

      print *,'th=',th
      print *,'thp=',thp
      print *,'e1=',e1
      print *,'e11=',e11
      print *,'ip=',ip
      if(abs(ip).eq.1)then
         e=sqrt(p**2+am**2) 
         tp=p**2/(e+am) 
         aj=p/e		! converts cross section from 1/mev to 1/mev/c
         if(ia.eq.1)then
            d2qd=0. 
            d2qf=0. 
         elseif(ia.gt.1)then
            call dep_3(e1,tp,th,ip,d2qd)
            print *,'d2qd=',d2qd
	 if(scal.ne.'y') then
            d2qd=d2qd*aj
	 endif
C            print 110,d2qd
C110     format('      quasi-deuteron =  ',e10.3)
	  if(scal.ne.'y') then
            call ep_3(e1,tp,th,d2qf)
            d2qf=d2qf*aj
            print *,'d2qf=',d2qf
c112     format('          quasi-free =  ',e10.3)
	  endif
         endif
      elseif(abs(ip).eq.2.or.ip.eq.0)then 
         e=sqrt(p**2+amp**2)
         tp=p**2/(e+amp)
         aj=p/e 
         d2qd=0.
         d2qf=0.
      else
	!call exit
      endif 
      if(p.lt.500.)then 
         d2sc=0.
         call delta_3(e1,tp,th,d2del) 
         print *,'tp=',tp
         print *,'d2del=',d2del
	 if(scal.eq.'y') then
         d2del=an*d2del
	 else
         d2del=an*d2del*aj
	 endif
C         print 111,d2del
C111	format('               delta =  ',e10.3) 
C         print *
      else
         d2del=0. 
	if(mpi.eq.'y') then
         if(abs(ip).eq.2.or.ip.eq.0)then
c            print *,'made it'
            call s2pi(2,e1,tp,th,d2sc1) 
            call s2pi(-2,e1,tp,th,d2sc2) 
            print *,d2sc1
            if(part.eq.'PI+')then 
               d2sc=z*d2sc1+n*d2sc2 
            elseif(part.eq.'PI-')then 
               d2sc=n*d2sc1+z*d2sc2 
            elseif(part.eq.'PI0')then 
               d2sc=ia*d2sc1
            else
            !   call exit
            endif 
c            print *,'made it2'
         elseif(abs(ip).eq.1)then 
            call s2pi(1,e1,tp,th,d2sc1) 
            d2sc=ia*d2sc1 
         endif
	endif
      endif 
c            print *,'made it3'
C      print 117,d2sc
C  117 format('                d2sc =  ',e10.3)
      total=d2qd+d2qf+d2del+d2sc
c      print * 
c      print 113,total 
c  113 format(' total cross section =  ',e10.3)
c      print * 
c      print 115,tp,p
C  115 format(' kinetic energy = ',f10.2,' mev.  momentum = ',f10.2,
C	1 ' mev/c') 
!	1	' mev/c') 
	if(scal.eq.'y') then
        totale=total*p*1.0d-6
        print *,'totale=',totale
c	print 1160,totale
c1160	format(' total in  ub/(q.mev.sr) = ',g16.3)
	else
      print *,'total=',total
      totale=total/aj 
c	print 116,totale
c  116 format(' total in  ub/(mev.sr) = ',g16.3)
	endif
C        print * 
C	print *
C	print *,'repeat same a, e_e, particle ("y") or not (return)?'
C	read(*,101) repe
      res=totale
      end 
