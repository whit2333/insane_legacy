      !> WISER_FIT
      !!
      !! Calculates  pi, k, p  cross section for gamma + p -> k 
      !! for a mono energetic photon beam.   
      !! It is NOT divided by E_GAMMA so that the simple 1/E_GAMMA 
      !! bremstrulung spectra can be substituted with a better one.
      !!
      !! Fit is from David Wiser's Thesis, eq. IV-A-2 and Table III.
      !! Here X denotes either a pion,kaon or proton
      !! 
      !!    @param TYPE   1=pi+;  1=pi-, 3=k+, 4=k-, 5=p, 6=p-bar
      !!    @param P momentum of x in GeV/c
      !!    @param THETA is the lab scattering angle in radians 
      !!    @param E_GAMMA photon energy in GeV 
      !!    @param SIG is the result of Ex*dsig/dp^3 with units of GeV-ub/(GeV/c)^2
      !!
      !! Fit parameter units: 
      !!   A1(ub/GeV^2), A2(ub/GeV), A3(GeV), A4(none), A5 and A6 (1/(GeV/c^2)^-1), A7(GeV^-2)
      !!
      !! REVISION HISTORY 
      !!     - Steve Rock 2/21/96
      !!     - Added documentation - Whitney Armstrong 1/8/2014 
      !!     - Copied from WISER_ALL_FIT to a subroutine which has no common blocks
      !!       and with complete arguments for linking to c programs
      !!              Whitney Armstrong 1/14/2014 
      !! 
      !! @ingroup wiser
      !!
      !!
      SUBROUTINE WISER_FIT(TYPE,P,THETA,E_GAMMA,SIG)
      IMPLICIT NONE       
      INTEGER  TYPE  !  1=pi+;  1=pi-, 3=k+, 4=k-, 5=p, 6=p-bar
      INTEGER PARTICLE   ! 1= pi, 2= K, 3 =P
      DOUBLE PRECISION P, THETA, E_GAMMA, SIG
      DOUBLE PRECISION COST, P_T, M_X,U_MAN
      !  fit pars   pi+     pi-    k+     k-     p+       p- 
      DOUBLE PRECISION A1(6)/566.,  486.,  368., 18.2,  1.33E5, 1.63E3/ 
      DOUBLE PRECISION A2(6)/829.,  115.,  1.91, 307.,  5.69E4,-4.30E3/ 
      DOUBLE PRECISION A3(6)/1.79,  1.77,  1.91, 0.98,  1.41,   1.79 / 
      DOUBLE PRECISION A4(6)/2.10,  2.18,  1.15, 1.83,   .72,   2.24 /
      DOUBLE PRECISION A5(6)/-5.49,-5.23,-5.91, -4.45, -6.77, -6.53/
      DOUBLE PRECISION A6(6)/-1.73,-1.82,-1.74, -3.23,  1.90, -2.45/
      DOUBLE PRECISION A7/-.0117/ !proton only
      DOUBLE PRECISION MASS2(3)/.019488, .2437, .8804/
      DOUBLE PRECISION MP2/.8804/,MP/.9383/
      DOUBLE PRECISION X_R, S, B_CM, GAM_CM, P_CM
      DOUBLE PRECISION P_CM_MAX, P_CM_L, M_L
      DOUBLE PRECISION E_GAM_CM,E

      IF(TYPE.EQ.1)     THEN 
         PARTICLE=1
      ELSEIF(TYPE.EQ.2) THEN  
         PARTICLE=1
      ELSEIF(TYPE.EQ.3) THEN  
         PARTICLE=2
      ELSEIF(TYPE.EQ.4) THEN  
         PARTICLE=2
      ELSEIF(TYPE.EQ.5) THEN  
         PARTICLE=3
      ELSEIF(TYPE.EQ.6) THEN  
         PARTICLE=3
      ELSE
         TYPE=1
         PARTICLE=1
         PRINT *,'WISER_FIT TYPE ERROR'
         PRINT *,'TYPE=',TYPE
         PRINT *,'PARTICLE=',PARTICLE
      ENDIF


      E    = SQRT(P**2 + MASS2(PARTICLE))            !  
      COST = COS(THETA)
      P_T  = P * SIN(THETA)
      M_L  = SQRT(P_T**2 + MASS2(PARTICLE))    
      S = MP2 + 2.* E_GAMMA * MP                     ! Mandlestam variable
                                                     ! Go to Center of Mass to get X_R
      B_CM = E_GAMMA/(E_GAMMA+MP)                    ! beta (lorentz trans)
      GAM_CM = 1./SQRT(1.-B_CM**2)                   ! gamma (lorentz trans)
      P_CM_L = -GAM_CM *B_CM *E + GAM_CM * P * COST  ! x cm longitudinal momentum
      P_CM  = SQRT(P_CM_L**2 + P_T**2)               ! X CM momentum
      E_GAM_CM=GAM_CM*E_GAMMA - GAM_CM *B_CM*E_GAMMA ! Photon CM momentum
c Not sure what the point of the following was
c      P_CM_MAX =SQRT (S +(M_X**2-MASS2(PARTICLE))**2/S 
c     >    -2.*(M_X**2 +MASS2(PARTICLE)) )/2.
      ! X_R is the ratio of the CM particle momentum to CM photon momentum
      X_R =  P_CM/E_GAM_CM 
       IF(X_R.GT.1.) THEN  ! Out of kinematic range
        SIG = 0.
       ELSEIF(TYPE.NE.5) THEN  ! not the proton
        SIG = ((A1(TYPE)+A2(TYPE)/SQRT(S))*
     >               ((1.0-X_R+A3(TYPE)**2/S)**A4(TYPE))*
     >               (EXP(A5(TYPE)*M_L)*EXP(A6(TYPE)*P_T**2/E)))
       ELSE ! special formula for proton
        U_MAN = ABS(2.0*MP2 -2.*MP*E)
        SIG = ((A1(TYPE) + A2(TYPE)/SQRT(S)) *
     >               (1.0-X_R + A3(TYPE)**2/S)**A4(TYPE)*
     >               (1.0+U_MAN)**(-1.0*(A6(TYPE)+A7*S)) )
       ENDIF
      RETURN
      END
