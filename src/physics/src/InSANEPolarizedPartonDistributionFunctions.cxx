#include "InSANEPolarizedPartonDistributionFunctions.h"
#include "InSANEMathFunc.h"
#include "Math.h"


InSANEPolarizedPartonDistributionFunctions::InSANEPolarizedPartonDistributionFunctions()
{
   fNintegrate = 100;
}
//______________________________________________________________________________

InSANEPolarizedPartonDistributionFunctions::~InSANEPolarizedPartonDistributionFunctions()
{ }
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1p_Twist2(Double_t x, Double_t Qsq)
{
   // Leading twist g1
   Double_t result = 0.0;
   GetPDFs(x, Qsq);
   result += 0.5 * (4.0 / 9.0) * Deltau();
   result += 0.5 * (1.0 / 9.0) * Deltad();
   result += 0.5 * (1.0 / 9.0) * Deltas();
   result += 0.5 * (4.0 / 9.0) * Deltaubar();
   result += 0.5 * (1.0 / 9.0) * Deltadbar();
   result += 0.5 * (1.0 / 9.0) * Deltasbar();
   return(result);
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1n_Twist2(Double_t x, Double_t Qsq)
{
   // Leading twist g1
   Double_t result = 0.0;
   GetPDFs(x, Qsq);
   result += 0.5 * (4.0 / 9.0) * Deltad();
   result += 0.5 * (1.0 / 9.0) * Deltau();
   result += 0.5 * (1.0 / 9.0) * Deltas();
   result += 0.5 * (4.0 / 9.0) * Deltadbar();
   result += 0.5 * (1.0 / 9.0) * Deltaubar();
   result += 0.5 * (1.0 / 9.0) * Deltasbar();
   return(result);
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1d_Twist2(Double_t x, Double_t Qsq)
{
   // Leading twist g1
   Double_t wD      = 0.058;         // D-wave state probability  
   Double_t g1n_val = g1n_Twist2(x,Qsq); 
   Double_t g1p_val = g1p_Twist2(x,Qsq); 
   Double_t result  = 0.5*(1.-1.5*wD)*(g1p_val + g1n_val);
   return(result);
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1He3_Twist2(Double_t x, Double_t Qsq)
{
   // Leading twist g1
   Double_t Pn      = 0.879;         // neutron polarization in 3He 
   Double_t Pp      = -0.021;        // proton polarization in 3He 
   Double_t g1n_val = g1n_Twist2(x,Qsq); 
   Double_t g1p_val = g1p_Twist2(x,Qsq); 
   Double_t result  = (Pn + 0.056)*g1n_val + (2.*Pp - 0.014)*g1p_val; 
   return(result);
}
//______________________________________________________________________________
Double_t InSANEPolarizedPartonDistributionFunctions::g1p_Twist2_TMC  ( Double_t x, Double_t Q2)
{
  double xi        = InSANE::Kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = InSANE::Kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M      = (CLHEP::M_p/CLHEP::GeV);
  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = (x/(xi*rho*rho*rho))*g1p_Twist2(xi,Q2);
  double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::simple(
      [&,this](double z){
      return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1p_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (xi_thresh - xi) / ((Double_t)N);
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((Double_t)i);
  //  integral_result += integrand(y)*g1p_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________
Double_t InSANEPolarizedPartonDistributionFunctions::g2p_Twist2_TMC  ( Double_t x, Double_t Q2)
{
  double xi        = InSANE::Kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = InSANE::Kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M         = (CLHEP::M_p/CLHEP::GeV);
  double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho       = TMath::Sqrt(1.0+gamma2);
  double res       = -1.0*(x/(xi*rho*rho*rho))*g1p_Twist2(xi,Q2);
  double t1        = (1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::simple(
      [&,this](double z){
      return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1p_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand   = [&](double z){
  //  return( ((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z );
  //};
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (xi_thresh - xi) / ((Double_t)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((Double_t)i);
  //  integral_result += integrand(y)*g1p_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1n_Twist2_TMC  ( Double_t x, Double_t Q2)
{
  double xi        = InSANE::Kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = InSANE::Kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M         = (CLHEP::M_p/CLHEP::GeV);
  double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho       = TMath::Sqrt(1.0+gamma2);
  double res       = (x/(xi*rho*rho*rho))*g1n_Twist2(xi,Q2);
  double t1        = (rho*rho-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::simple(
      [&,this](double z){
      return( (((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1n_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand   = [&](double z){
  //  return( ((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z );
  //};
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (xi_thresh - xi) / ((Double_t)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((Double_t)i);
  //  integral_result += integrand(y)*g1n_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________
Double_t InSANEPolarizedPartonDistributionFunctions::g1d_Twist2_TMC  ( Double_t x, Double_t Q2)
{
  double xi        = InSANE::Kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = InSANE::Kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double gamma2 = TMath::Power(2.0*CLHEP::M_p*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = (x/(xi*rho*rho*rho))*g1d_Twist2(xi,Q2);
  double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( (((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1d_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand = [&](double z){
  //  return( ((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z );
  //};
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (xi_thresh - xi) / ((Double_t)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((Double_t)i);
  //  integral_result += integrand(y)*g1d_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1He3_Twist2_TMC( Double_t x, Double_t Q2)
{
  double xi        = InSANE::Kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = InSANE::Kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double gamma2 = TMath::Power(2.0*CLHEP::M_p*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = (x/(xi*rho*rho*rho))*g1He3_Twist2(xi,Q2);
  double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( (((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*g1He3_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand = [&](double z){
  //  return( ((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z );
  //};
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (xi_thresh - xi) / ((Double_t)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((Double_t)i);
  //  integral_result += integrand(y)*g1He3_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g2pWW(Double_t x, Double_t Qsq)
{
  // g2WW
  double result          = -1.0*g1p_Twist2(x, Qsq);
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( g1p_Twist2(z,Qsq)/z );
      }, x,1.0);
  //Int_t N         = fNintegrate;
  //Double_t dy     = (1.0 - x)/(double(N));
  //for (int i = 0; i < N; i++) {
  //  double y = x + dy*double(i);
  //  result += g1p_Twist2(y, Qsq)*(dy/y);
  //}
  return( result + integral_result );
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1p_BT_Twist3(Double_t x, Double_t Qsq)
{
  // BT Relation - effectively a TMC only to O(M^2/Q^2)
  // g1p_Twist3_TMC - has the TMC treatment
  // g1 twist-3
  Double_t g2p    =  g2p_Twist3_TMC(x, Qsq);
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( g2p_Twist3_TMC(z,Qsq)/z );
      }, x,1.0);
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (1.0 - x) / ((Double_t)N);
  //Double_t y = 0.0;
  //Double_t integral = 0.0;
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  y = x0 + dy*((Double_t)i);
  //  integral += (g2p_Twist3_TMC(y,Qsq0)*dy/(y));
  ////  result += (-2.0*g2p_Twist3_TMC(y,Qsq0)*dy/(y));
  //}
  Double_t M = (M_p/GeV);
  Double_t coeff = (4.0*M*M*x*x)/Qsq;
  Double_t result = coeff*(g2p - 2.0*integral_result );
  return(result);
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::Dp_BT(Double_t x, Double_t Q2)
{
  // BT Relation - effectively a TMC only to O(M^2/Q^2)
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( (3.0-2.0*TMath::Log(z/x))*Dp_Twist3(z,Q2)/z );
      }, x,1.0);
  //Double_t result = 0.0;
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (1.0 - x) / ((Double_t)N);
  //Double_t y = 0.0;
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  y = x + dy*double(i);
  //  double Dp = Dp_Twist3(y, Q2);
  //  result += ((dy/y)*(3.0-2.0*TMath::Log(y/x))*Dp);
  //}
  return(integral_result);
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1p_Twist3_TMC(Double_t x, Double_t Q2)
{
  // Twist three part of g1p with full TMC treatment
  double xi        = InSANE::Kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = InSANE::Kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M      = (CLHEP::M_p/CLHEP::GeV);
  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = (rho*rho-1.0)/(rho*rho*rho)*Dp_Twist3(xi,Q2);
  double t1     = (1.0-rho*rho)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand = [&](double z){
  //return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
  //};
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (1.0 - xi) / ((Double_t)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((Double_t)i);
  //  integral_result += integrand(y)*Dp_Twist3(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g2p_Twist3_TMC(Double_t x, Double_t Q2)
{
  // Twist three part of g1p with full TMC treatment
  double xi        = InSANE::Kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = InSANE::Kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M      = (CLHEP::M_p/CLHEP::GeV);
  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = 1.0/(rho*rho*rho)*Dp_Twist3(xi,Q2);
  double t1     = (-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return(((3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand = [&](double z){
  //  return( (3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z );
  //};
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (xi_thresh - xi) / ((Double_t)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((Double_t)i);
  //  integral_result += integrand(y)*Dp_Twist3(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g2nWW(Double_t x, Double_t Qsq)
{
  Double_t result = - g1n_Twist2(x, Qsq);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( g1n_Twist2(z,Qsq)/z );
      }, x,1.0);
  //Double_t x0 = x;
  //Double_t Qsq0 = Qsq;
  //Int_t N = fNintegrate;
  //Double_t dx = (1.0 - x) / ((Double_t)N);
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  result += g1n_Twist2(x0 + dx * ((Double_t)i), Qsq0) * dx / (x0 + dx * ((Double_t)i));
  // }
  // // return pdfs to initial x,Qsq
  return(result + integral_result );
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1n_BT_Twist3(Double_t x, Double_t Qsq)
{
  // BT Relation
  // g1 twist-3
  double result          =  g2n_Twist3(x, Qsq);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( g2n_Twist3(z,Qsq)/z );
      }, x,1.0);
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (1.0 - x) / ((Double_t)N);
  //Double_t y = 0.0;
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  y = x0 + dy*((Double_t)i);
  //  result += (-2.0*g2n_Twist3(y,Qsq0)*dy/(y));
  //}
  Double_t M = (M_p/GeV);
  Double_t coeff = (4.0*M*M*x*x)/Qsq;
  return( coeff*(result - 2.0*integral_result) );
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g2dWW(Double_t x, Double_t Qsq)
{
  double result          = -1.0*g1d_Twist2(x, Qsq);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( g1d_Twist2(z,Qsq)/z );
      }, x,1.0);
  //Double_t x0 = x;
  //Double_t Qsq0 = Qsq;
  //Int_t N = fNintegrate;
  //Double_t dx = (1.0 - x) / ((Double_t)N);
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  result += g1d_Twist2(x0 + dx * ((Double_t)i), Qsq0) * dx / (x0 + dx * ((Double_t)i));
  //}
  // return pdfs to initial x,Qsq
  /*      fPolarizedPDFs->GetPDFs(x0,Qsq0);*/
  return(result + integral_result);
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1d_BT_Twist3(Double_t x, Double_t Qsq)
{
  // BT Relation
  // g1 twist-3
  double result          =  g2d_Twist3(x, Qsq);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( g2d_Twist3(z,Qsq)/z );
      }, x,1.0);
  //Int_t    N      = fNintegrate;
  //Double_t dy     = (1.0 - x) / ((Double_t)N);
  //Double_t y = 0.0;
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  y = x0 + dy*((Double_t)i);
  //  result += (-2.0*g2d_Twist3(y,Qsq0)*dy/(y));
  //}
  //result *= (4.0*(M_p/GeV)*(M_p/GeV)*x*x);
  double M     = (M_p/GeV);
  double coeff = (4.0*M*M*x*x)/Qsq;
  return( coeff*(result - 2.0*integral_result) );
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g2He3WW(Double_t x, Double_t Qsq)
{
  double result          = -1.0*g1He3_Twist2(x, Qsq);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( g1He3_Twist2(z,Qsq)/z );
      }, x,1.0);
  return( result + integral_result );
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g1He3_BT_Twist3(Double_t x, Double_t Qsq)
{
  // BT Relation g1 twist-3
  double result          =  g2He3_Twist3(x, Qsq);
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( g2He3_Twist3(z,Qsq)/z );
      }, x,1.0);
  Double_t M = (M_p/GeV);
  Double_t coeff = (4.0*M*M*x*x)/Qsq;
  return( coeff*(result - 2.0*integral_result) );
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::Dp_Twist3(Double_t x, Double_t Q2)
{ 
   Double_t result = 0.0;
   GetPDFs(x, Q2);
   result += (4.0 / 9.0) * fxDu_Twist3/x;
   result += (1.0 / 9.0) * fxDd_Twist3/x;
   //result += 0.5 * (1.0 / 9.0) * Deltas();
   //result += 0.5 * (4.0 / 9.0) * Deltaubar();
   //result += 0.5 * (1.0 / 9.0) * Deltadbar();
   //result += 0.5 * (1.0 / 9.0) * Deltasbar();
   //std::cout << "Dp_Twist3 " <<  result << '\n';
   return(result);
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::Dn_Twist3(Double_t x, Double_t Q2)
{ 
   Double_t result = 0.0;
   GetPDFs(x, Q2);
   result += (4.0 / 9.0) * fxDd_Twist3;
   result += (1.0 / 9.0) * fxDu_Twist3;
   //result += 0.5 * (1.0 / 9.0) * Deltas();
   //result += 0.5 * (4.0 / 9.0) * Deltaubar();
   //result += 0.5 * (1.0 / 9.0) * Deltadbar();
   //result += 0.5 * (1.0 / 9.0) * Deltasbar();
   //std::cout << "Dp_Twist3 " <<  result << '\n';
   return(result);
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::Moment_Dp_Twist3(int n, Double_t Q2, Double_t x1, Double_t x2) 
{
  double integral_result =  insane::integrate::simple(
      [&](double z){
      return( TMath::Power(z,double(n-1.0))*Dp_Twist3(z,Q2) );
      }, x1, x2);
  return(integral_result);
}
//_____________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g2p_Twist3(Double_t x, Double_t Q2)
{ 
  double result          = Dp_Twist3(x, Q2);
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( Dp_Twist3(z,Q2)/z );
      }, x,1.0);
  return( result - integral_result );
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g2n_Twist3(Double_t x, Double_t Q2)
{ 
  double result          = Dn_Twist3(x, Q2);
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( Dn_Twist3(z,Q2)/z );
      }, x,1.0);
  return( result - integral_result );
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::g2d_Twist3(Double_t,Double_t)   { return 0; }
Double_t InSANEPolarizedPartonDistributionFunctions::g2He3_Twist3(Double_t,Double_t) { return 0; }

//______________________________________________________________________________
Double_t InSANEPolarizedPartonDistributionFunctions::Deltau()    const { return(fPDFValues[0]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltad()    const { return(fPDFValues[1]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltas()    const { return(fPDFValues[2]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltac()    const { return(fPDFValues[3]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltab()    const { return(fPDFValues[4]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltat()    const { return(fPDFValues[5]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltag()    const { return(fPDFValues[6]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltaubar() const { return(fPDFValues[7]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltadbar() const { return(fPDFValues[8]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltasbar() const { return(fPDFValues[9]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltacbar() const { return(fPDFValues[10]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltabbar() const { return(fPDFValues[11]); }
Double_t InSANEPolarizedPartonDistributionFunctions::Deltatbar() const { return(fPDFValues[12]); }
//______________________________________________________________________________
Double_t InSANEPolarizedPartonDistributionFunctions::DeltauError()    const { return(fPDFErrors[0]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltadError()    const { return(fPDFErrors[1]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltasError()    const { return(fPDFErrors[2]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltacError()    const { return(fPDFErrors[3]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltabError()    const { return(fPDFErrors[4]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltatError()    const { return(fPDFErrors[5]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltagError()    const { return(fPDFErrors[6]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltaubarError() const { return(fPDFErrors[7]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltadbarError() const { return(fPDFErrors[8]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltasbarError() const { return(fPDFErrors[9]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltacbarError() const { return(fPDFErrors[10]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltabbarError() const { return(fPDFErrors[11]); }
Double_t InSANEPolarizedPartonDistributionFunctions::DeltatbarError() const { return(fPDFErrors[12]); }
//______________________________________________________________________________
Double_t InSANEPolarizedPartonDistributionFunctions::Deltau(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[0]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltauBar(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[7]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::Deltad(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[1]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltadBar(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[8]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::Deltas(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[2]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltasBar(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[9]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::Deltag(Double_t x,Double_t Qsq){
   GetPDFs(x,Qsq);
   return fPDFValues[6]; 
}
Double_t InSANEPolarizedPartonDistributionFunctions::Deltac(Double_t x, Double_t Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFValues[3]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltacBar(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[10]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::Deltab(Double_t x, Double_t Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFValues[4]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltabBar(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[11]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::Deltat(Double_t x, Double_t Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFValues[5]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltatBar(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[12]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltauError(Double_t x, Double_t Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[0]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltauBarError(Double_t x, Double_t Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[7]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltadError(Double_t x, Double_t Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[1]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltadBarError(Double_t x, Double_t Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[8]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltasError(Double_t x, Double_t Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[2]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltasBarError(Double_t x, Double_t Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[9]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltacError(Double_t x, Double_t Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFErrors[3]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltacBarError(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFErrors[10]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltabError(Double_t x, Double_t Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFErrors[4]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltabBarError(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFErrors[11]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltatError(Double_t x, Double_t Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFErrors[5]);
}
Double_t InSANEPolarizedPartonDistributionFunctions::DeltatBarError(Double_t x, Double_t Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFErrors[12]);
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::D_u(Double_t x, Double_t Q2)
{ 
   GetPDFs(x, Q2);
   return fxDu_Twist3/x;
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::D_d(Double_t x, Double_t Q2)
{ 
   GetPDFs(x, Q2);
   return fxDd_Twist3/x;
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::gT_u_WW(Double_t x, Double_t Q2)
{
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( Deltau(z,Q2)/z );
      }, x,1.0);
  return( integral_result );
  //// quick simple integration
  //Int_t N         = 100;
  //Double_t dy     = (1.0 - x) / ((Double_t)N);
  //Double_t result = 0.0;
  //Double_t x0     = x;
  //for (int i = 0; i < N; i++) {
  //  Double_t y = x0 + dy*((Double_t)i);
  //  result    += (Deltau(y,Q2)*dy/(y));
  //}
  //return(result);
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::gT_d_WW(Double_t x, Double_t Q2)
{
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( Deltad(z,Q2)/z );
      }, x,1.0);
  return( integral_result );
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::gT_ubar_WW(Double_t x, Double_t Q2){
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( DeltauBar(z,Q2)/z );
      }, x,1.0);
  return( integral_result );
}
//______________________________________________________________________________

Double_t InSANEPolarizedPartonDistributionFunctions::gT_dbar_WW(Double_t x, Double_t Q2){
  double integral_result = insane::integrate::simple(
      [&](double z){
      return( DeltadBar(z,Q2)/z );
      }, x,1.0);
  return( integral_result );
}

////______________________________________________________________________________
//Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1p(Double_t x, Double_t Qsq) {
//   //Double_t result = xg1p(x, Qsq) / x;
//   Double_t result = 0.0;
//   GetPDFs(x, Qsq);
//   result += 0.5 * (4.0 / 9.0) * Deltau();
//   result += 0.5 * (1.0 / 9.0) * Deltad();
//   result += 0.5 * (1.0 / 9.0) * Deltas();
//   result += 0.5 * (4.0 / 9.0) * Deltaubar();
//   result += 0.5 * (1.0 / 9.0) * Deltadbar();
//   result += 0.5 * (1.0 / 9.0) * Deltasbar();
//   return(result);
//}
////_____________________________________________________________________________
//Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1n(Double_t x, Double_t Qsq) {
//   //Double_t result = xg1n(x, Qsq) / x;
//   Double_t result = 0.0;
//   if(!fPolarizedPDFs) {
//      Error("xg1n(x,Qsq)","No polarized PDFs set");
//      return(result);
//   }
//   fPolarizedPDFs->GetPDFs(x, Qsq);
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltad();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltau();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltas();
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltadbar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltaubar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltasbar();
//   return(result);
//}
////_____________________________________________________________________________
//Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1d(Double_t x, Double_t Qsq) {
//   Double_t wD      = 0.058;         // D-wave state probability  
//   Double_t g1n_val = g1n(x,Qsq); 
//   Double_t g1p_val = g1p(x,Qsq); 
//   Double_t result  = 0.5*(1.-1.5*wD)*(g1p_val + g1n_val);
//   return(result);
//}
////_____________________________________________________________________________
//Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1He3(Double_t x, Double_t Qsq) {
//   Double_t Pn      = 0.879;         // neutron polarization in 3He 
//   Double_t Pp      = -0.021;        // proton polarization in 3He 
//   Double_t g1n_val = g1n(x,Qsq); 
//   Double_t g1p_val = g1p(x,Qsq); 
//   Double_t result  = (Pn + 0.056)*g1n_val + (2.*Pp - 0.014)*g1p_val; 
//   return(result);
//}
//_____________________________________________________________________________
//Double_t InSANEPolarizedStructureFunctionsFromPDFs::xg1p(Double_t x, Double_t Qsq) {
//   Double_t result = 0.0;
//   fPolarizedPDFs->GetPDFs(x, Qsq);
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltau();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltad();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltas();
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltaubar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltadbar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltasbar();
//   return(result*x);
//}
////_____________________________________________________________________________
//Double_t InSANEPolarizedStructureFunctionsFromPDFs::xg1n(Double_t x, Double_t Qsq) {
//   Double_t result = 0.0;
//   if(!fPolarizedPDFs) {
//      Error("xg1n(x,Qsq)","No polarized PDFs set");
//      return(result);
//   }
//   fPolarizedPDFs->GetPDFs(x, Qsq);
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltad();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltau();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltas();
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltadbar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltaubar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltasbar();
//   return(result*x);
//}
////_____________________________________________________________________________
TF1 * InSANEPolarizedPartonDistributionFunctions::GetFunction(InSANEPDFBase::InSANEPartonFlavor q) {
   if (fFunctions[q]){
      fFunctions[q]->TAttLine::operator=(*this);
      return(fFunctions[q]);
   } else {
      Int_t npar = 1;
      switch (q) {
         case InSANEPDFBase::kUP :
            fFunctions[q] = new TF1(Form("delta xu %s", GetLabel()), this, &InSANEPolarizedPartonDistributionFunctions::EvaluatexDeltau,
                  fxPlotMin, fxPlotMax, npar, "InSANEPolarizedPartonDistributionFunctions", "EvaluatexDeltau");
            break;
         case InSANEPDFBase::kDOWN :
            fFunctions[q] = new TF1(Form("delta xd %s", GetLabel()), this, &InSANEPolarizedPartonDistributionFunctions::EvaluatexDeltad,
                  fxPlotMin, fxPlotMax, npar, "InSANEPolarizedPartonDistributionFunctions", "EvaluatexDeltad");
            break;

         case InSANEPDFBase::kSTRANGE :
            fFunctions[q] = new TF1("delta xs", this, &InSANEPolarizedPartonDistributionFunctions::EvaluatexDeltas,
                  fxPlotMin, fxPlotMax, npar, "InSANEPolarizedPartonDistributionFunctions", "EvaluatexDeltas");
            break;

         case InSANEPDFBase::kGLUON :
            fFunctions[q] = new TF1("delta xg", this, &InSANEPolarizedPartonDistributionFunctions::EvaluatexDeltag,
                  fxPlotMin, fxPlotMax, npar, "InSANEPolarizedPartonDistributionFunctions", "EvaluatexDeltag");
            break;

         case InSANEPDFBase::kANTIUP :
            fFunctions[q] = new TF1("delta xubar", this, &InSANEPolarizedPartonDistributionFunctions::EvaluatexDeltaubar,
                  fxPlotMin, fxPlotMax, npar, "InSANEPolarizedPartonDistributionFunctions", "EvaluatexDeltaubar");
            break;

         case InSANEPDFBase::kANTIDOWN :
            fFunctions[q] = new TF1("delta xdbar", this, &InSANEPolarizedPartonDistributionFunctions::EvaluatexDeltadbar,
                  fxPlotMin, fxPlotMax, npar, "InSANEPolarizedPartonDistributionFunctions", "EvaluatexDeltadbar");
            break;

         case InSANEPDFBase::kANTISTRANGE :
            fFunctions[q] = new TF1("delta xsbar", this, &InSANEPolarizedPartonDistributionFunctions::EvaluatexDeltasbar,
                  fxPlotMin, fxPlotMax, npar, "InSANEPolarizedPartonDistributionFunctions", "EvaluatexDeltasbar");
            break;
         default :
            fFunctions[q] = new TF1(Form("delta xu %s", GetLabel()), this, &InSANEPolarizedPartonDistributionFunctions::EvaluatexDeltau,
                  fxPlotMin, fxPlotMax, npar, "InSANEPolarizedPartonDistributionFunctions", "EvaluatexDeltau");
            break;

      }
   }
   //fFunctions[q]->SetLineColor(fDefaultLineColor);
   //fFunctions[q]->SetLineStyle(fDefaultLineStyle);
   fFunctions[q]->TAttLine::operator=(*this);
   return(fFunctions[q]);
}
//_____________________________________________________________________________
void InSANEPolarizedPartonDistributionFunctions::GetValues(TObject *obj, Double_t Q2, InSANEPDFBase::InSANEPartonFlavor q){
   // Fills histogram with values.
   //  For error band use GetErrorBand
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   Double_t x[3];

   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
            x[0]=xaxis->GetBinCenter(binx);

            GetPDFs(x[0], Q2);
            hfit->SetBinContent(binx, biny, binz, x[0]*fPDFValues[q]);
   //GetPDFErrors(x[0], Q2);
   //         hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
         }
      }
   }

}
//_____________________________________________________________________________
void InSANEPolarizedPartonDistributionFunctions::GetErrorBand(TObject *obj, Double_t Q2, InSANEPDFBase::InSANEPartonFlavor q){
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   Double_t x[3];
   Double_t err = 0.0;

   
   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {

            x[0] = xaxis->GetBinCenter(binx);

            GetPDFs(x[0], Q2);
            hfit->SetBinContent(binx, biny, binz, x[0]*fPDFValues[q]);

            GetPDFErrors(x[0], Q2);
            err = fPDFErrors[q];
            if( err == 0.0 ) err = 0.000001;
            hfit->SetBinError(binx, biny, binz, x[0]*err);
         }
      }
   }

}

//void InSANEPolarizedPartonDistributionFunctions::PlotPDFs(Double_t Qsq, Int_t log)
//{
//
//   const Int_t N = 30;
//   Double_t xmin = 0.0001;
//   Double_t xmax = 0.9999;
//   //Double_t Qsqmin=1.0;
//   //Double_t Qsqmax=12.0;
//   //Double_t QMean=5.4;
//   Double_t dx = (xmax - xmin) / ((Double_t)N);
//   //Double_t dQsq=(Qsqmax-Qsqmin)/((Double_t)N);
//
//   Double_t x = 0; //, val1,val2;
//   Int_t pointNumber = 0;
//
//   TMultiGraph *mg = new TMultiGraph();
//
//   TGraph * gUpVsX = new TGraph();
//   gUpVsX->SetTitle(Form("x#Deltau(x) [%s]", GetLabel()));
//   gUpVsX->SetMarkerColor(1);
//   gUpVsX->SetMarkerStyle(21);
//   gUpVsX->SetMarkerSize(1.3);
//   gUpVsX->SetLineColor(1);
//   gUpVsX->SetLineWidth(3);
//
//   TGraph * gDownVsX = new TGraph();
//   gDownVsX->SetMarkerColor(2);
//   gDownVsX->SetMarkerStyle(22);
//   gDownVsX->SetMarkerSize(1.3);
//   gDownVsX->SetLineColor(2);
//   gDownVsX->SetLineWidth(3);
//   gDownVsX->SetTitle(Form("x#Deltad(x) [%s]", GetLabel()));
//
//   TGraph * gStrangeVsX = new TGraph();
//   gStrangeVsX->SetMarkerColor(3);
//   gStrangeVsX->SetMarkerStyle(23);
//   gStrangeVsX->SetMarkerSize(1.3);
//   gStrangeVsX->SetLineColor(3);
//   gStrangeVsX->SetLineWidth(3);
//   gStrangeVsX->SetTitle(Form("x#Deltas(x) [%s]", GetLabel()));
//
//   TGraph * gUpbarVsX = new TGraph();
//   gUpbarVsX->SetMarkerColor(4);
//   gUpbarVsX->SetMarkerStyle(24);
//   gUpbarVsX->SetMarkerSize(1.3);
//   gUpbarVsX->SetLineColor(4);
//   gUpbarVsX->SetLineWidth(3);
//   gUpbarVsX->SetTitle(Form("x #Delta#bar{u}(x) [%s]", GetLabel()));
//
//   TGraph * gDownbarVsX = new TGraph();
//   gDownbarVsX->SetMarkerColor(5);
//   gDownbarVsX->SetMarkerStyle(25);
//   gDownbarVsX->SetMarkerSize(1.3);
//   gDownbarVsX->SetLineColor(5);
//   gDownbarVsX->SetLineWidth(3);
//   gDownbarVsX->SetTitle(Form("x#Delta#bar{d}(x) [%s]", GetLabel()));
//
//   TGraph * gStrangebarVsX = new TGraph();
//   gStrangebarVsX->SetMarkerColor(6);
//   gStrangebarVsX->SetMarkerStyle(26);
//   gStrangebarVsX->SetMarkerSize(1.3);
//   gStrangebarVsX->SetLineColor(6);
//   gStrangebarVsX->SetLineWidth(3);
//   gStrangebarVsX->SetTitle(Form("x#Delta#bar{s}(x) [%s]", GetLabel()));
//
//   //       u->SetMarkerColor(1);
//   //       aGraph->SetLineColor(2);
//   //       aGraph->SetLineWidth(3);
//   //       aGraph->SetMarkerSize(1.3);
//   //       aGraph->SetMarkerStyle(23);
//   //       aGraph->SetTitle(" ");
//   //       aGraph->GetXaxis()->SetTitle("Run Number");
//   //       aGraph->GetYaxis()->SetTitle(" ");
//   Double_t * pdfvals;
//   pointNumber = 0;
//   for (int i = 0; i < N; i++) {
//      x = xmin + (Double_t)i * dx;
//      pdfvals =  GetPDFs(x, Qsq);
//      gUpVsX->SetPoint(pointNumber, x, Deltau());
//      gUpbarVsX->SetPoint(pointNumber, x, Deltaubar());
//      gDownVsX->SetPoint(pointNumber, x, Deltad());
//      gDownbarVsX->SetPoint(pointNumber, x, Deltadbar());
//      gStrangeVsX->SetPoint(pointNumber, x, Deltas());
//      gStrangebarVsX->SetPoint(pointNumber, x, Deltasbar());
//      pointNumber++;
//   }
//
//
//
//   TCanvas *fCanvas = (TCanvas *) gROOT->FindObject(Form("cPolPdfs%d", (Int_t)Qsq));
//   if (!fCanvas) fCanvas = new TCanvas(Form("cPolPdfs%d", (Int_t)Qsq), "Polarzied PDFs", 0, 0, 600, 400);
//   fCanvas->cd(0)->SetLogx(log);
//
//   mg->Add(gUpVsX, "LP");
//   mg->Add(gDownVsX, "LP");
//   mg->Add(gStrangeVsX, "LP");
//   mg->Add(gUpbarVsX, "LP");
//   mg->Add(gDownbarVsX, "LP");
//   mg->Add(gStrangebarVsX, "LP");
//   mg->Draw("A");
//
//   TLegend * leg = new TLegend(0.7, 0.7, 0.95, 0.95);
//   leg->SetHeader(Form("%s Polarized PDFs", GetLabel()));
//   leg->AddEntry(gUpVsX, gUpVsX->GetTitle(), "lp");
//   leg->AddEntry(gDownVsX, gDownVsX->GetTitle(), "lp");
//   leg->AddEntry(gStrangeVsX, gStrangeVsX->GetTitle(), "lp");
//   leg->Draw();
//
//   TLatex * t = new TLatex();
//   t->SetNDC();
//   t->SetTextFont(62);
//   t->SetTextColor(36);
//   t->SetTextSize(0.06);
//   t->SetTextAlign(12);
//   t->DrawLatex(0.05, 0.95, Form("Q^{2} = %d GeV^{2}", (Int_t)Qsq));
//
//   //    fQsqText->AddText();
//   //   fQsqText->Draw();
//
//   //   fCanvas->Divide(3,2);
//   //   fCanvas->cd(1)->SetLogx(log);
//   //   gUpVsX->Draw("ALP");
//   //   fCanvas->cd(2)->SetLogx(log);
//   //   gDownVsX->Draw("ALP");
//   //   fCanvas->cd(3)->SetLogx(log);
//   //   gStrangeVsX->Draw("ALP");
//   //   fCanvas->cd(4)->SetLogx(log);
//   //   gUpbarVsX->Draw("ALP");
//   //   fCanvas->cd(5)->SetLogx(log);
//   //   gDownbarVsX->Draw("ALP");
//   //   fCanvas->cd(6)->SetLogx(log);
//   //   gStrangebarVsX->Draw("ALP");
//}
//_____________________________________________________________________________
