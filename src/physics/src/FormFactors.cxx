#include "FormFactors.h"

namespace insane {
  namespace physics {
    FormFactors::FormFactors()
    { }
    //______________________________________________________________________________

    FormFactors::~FormFactors()
    { }
    //______________________________________________________________________________
 
    double FormFactors::Get(double Q2, std::tuple<SF,Nuclei> ff) const {
      auto fftype = std::get<FormFactor>(sf);
      auto target = std::get<Nuclei>(sf);

      double result = 0.0;
      switch(fftype) {

        case FormFactor::F1 :
            result = F1(Q2, target); 
          }
          break;

        case FormFactor::F2 :
            result = F2(Q2, target); 
          }
          break;
      }
      return result;
    }
    //______________________________________________________________________________

    double FormFactors::F1(double Q2, Nuclei target) const
    {
      switch(target) {

        case Nuclei::p : 
          return F1p(x,Q2);
          break;

        case Nuclei::n : 
          return F1n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double FormFactors::F2(double Q2, Nuclei target) const
    {
      switch(target) {

        case Nuclei::p : 
          return F2p(x,Q2);
          break;

        case Nuclei::n : 
          return F2n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

  }
}
