#include "InSANEPolarizedStructureFunctionsFromPDFs.h"

InSANEPolarizedStructureFunctionsFromPDFs::InSANEPolarizedStructureFunctionsFromPDFs()
{
   SetNameTitle("InSANEPolarizedStructureFunctionsFromPDFs","Pol.SFs from PDFsF");
   SetLabel("pol.SFs from PDFs");
   fPolarizedPDFs = nullptr; 
}
//_____________________________________________________________________________

InSANEPolarizedStructureFunctionsFromPDFs::InSANEPolarizedStructureFunctionsFromPDFs(
    InSANEPolarizedPartonDistributionFunctions * ppdfs) : fPolarizedPDFs(ppdfs)
{
   SetLabel(Form("%s",ppdfs->GetLabel()));
   SetNameTitle(Form("Spin SFs from %s",ppdfs->GetName()),Form("%s",ppdfs->GetName()));
}
//______________________________________________________________________________

InSANEPolarizedStructureFunctionsFromPDFs::~InSANEPolarizedStructureFunctionsFromPDFs()
{ }
//_____________________________________________________________________________

void InSANEPolarizedStructureFunctionsFromPDFs::SetPolarizedPDFs(InSANEPolarizedPartonDistributionFunctions * ppdfs)
{
   fPolarizedPDFs = ppdfs;
   SetLabel(Form("%s",ppdfs->GetLabel()));
   SetNameTitle(Form("Spin SFs from %s",ppdfs->GetName()),Form("%s",ppdfs->GetName()));
}
//______________________________________________________________________________

InSANEPolarizedPartonDistributionFunctions * InSANEPolarizedStructureFunctionsFromPDFs::GetPolarizedPDFs()
{
   return(fPolarizedPDFs);
}
//______________________________________________________________________________

Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1pError(Double_t x,Double_t Q2)
{
   if(!fPolarizedPDFs) {
      Error("g1pErr(x,Q2)","No polarized PDFs set");
      return(0.);
   }
   fPolarizedPDFs->GetPDFErrors(x,Q2); 
   Double_t Qu = Q_up;
   Double_t Qd = Q_down;  
   Double_t Qs = Q_strange;  

   Double_t DeltaUErr    = fPolarizedPDFs->DeltauError(); 
   Double_t DeltaDErr    = fPolarizedPDFs->DeltadError(); 
   Double_t DeltaSErr    = fPolarizedPDFs->DeltasError(); 
   Double_t DeltaUBarErr = fPolarizedPDFs->DeltaubarError(); 
   Double_t DeltaDBarErr = fPolarizedPDFs->DeltadbarError(); 
   Double_t DeltaSBarErr = fPolarizedPDFs->DeltasbarError(); 

   Double_t du_sq  = TMath::Power(Qu*Qu,2.)*(DeltaUErr*DeltaUErr + DeltaUBarErr*DeltaUBarErr); 
   Double_t dd_sq  = TMath::Power(Qd*Qd,2.)*(DeltaDErr*DeltaDErr + DeltaDBarErr*DeltaDBarErr); 
   Double_t ds_sq  = TMath::Power(Qs*Qs,2.)*(DeltaSErr*DeltaSErr + DeltaSBarErr*DeltaSBarErr); 

   Double_t sum_sq = du_sq + dd_sq + ds_sq; 
   Double_t result = 0.5*TMath::Sqrt(sum_sq);  

   if(result < 1E-10) result = 0.; 
   return result;  

}
//_____________________________________________________________________________

Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1nError(Double_t x,Double_t Q2)
{

   if(!fPolarizedPDFs) {
      Error("g1nErr(x,Q2)","No polarized PDFs set");
      return(0.);
   }
   fPolarizedPDFs->GetPDFErrors(x,Q2); 
   Double_t Qu = Q_up;
   Double_t Qd = Q_down;  
   Double_t Qs = Q_strange;  
   Double_t DeltaUErr=0,DeltaUBarErr=0; 
   Double_t DeltaDErr=0,DeltaDBarErr=0; 
   Double_t DeltaSErr=0,DeltaSBarErr=0; 

   DeltaUErr    = fPolarizedPDFs->DeltauError(); 
   DeltaDErr    = fPolarizedPDFs->DeltadError(); 
   DeltaSErr    = fPolarizedPDFs->DeltasError(); 
   DeltaUBarErr = fPolarizedPDFs->DeltaubarError(); 
   DeltaDBarErr = fPolarizedPDFs->DeltadbarError(); 
   DeltaSBarErr = fPolarizedPDFs->DeltasbarError(); 

   Double_t du_sq  = TMath::Power(Qd*Qd,2.)*(DeltaUErr*DeltaUErr + DeltaUBarErr*DeltaUBarErr); 
   Double_t dd_sq  = TMath::Power(Qu*Qu,2.)*(DeltaDErr*DeltaDErr + DeltaDBarErr*DeltaDBarErr); 
   Double_t ds_sq  = TMath::Power(Qs*Qs,2.)*(DeltaSErr*DeltaSErr + DeltaSBarErr*DeltaSBarErr); 

   Double_t sum_sq = du_sq + dd_sq + ds_sq; 
   Double_t result = 0.5*TMath::Sqrt(sum_sq);  

   if(result < 1E-10) result = 0.; 
   return result;  

}
//_____________________________________________________________________________

Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1He3Error(Double_t x,Double_t Q2)
{
   Double_t Pn      = 0.879;         // neutron polarization in 3He 
   Double_t Pp      = -0.021;        // proton polarization in 3He 
   Double_t g1n_err = g1nError(x,Q2); 
   Double_t g1p_err = g1pError(x,Q2); 
   Double_t sum_sq  = TMath::Power( (Pn + 0.056)*g1n_err , 2. ) 
      + TMath::Power( (2.*Pp - 0.014)*g1p_err, 2.); 
   Double_t result  = TMath::Sqrt(sum_sq); 
   return result;  
}
//_____________________________________________________________________________

Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2pWW_TMC(   Double_t x, Double_t Q2)
{
  // Wrong way to do; but this is used as a check
   // g2WW using the Twist-2 TMC corrected g1
   Double_t x0     = x;
   Double_t Q20    = Q2;
   Double_t result = - g1p_Twist2_TMC(x0, Q20);
   Int_t N         = fNintegrate;
   Double_t dx     = (1.0 - x) / ((Double_t)N);
   // quick simple integration
   for (int i = 0; i < N; i++) {
     double y = x0+dx*double(i);
     result += g1p_Twist2_TMC(y, Q20)*dx/y;
   }
   return(result);
}
//______________________________________________________________________________

Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2pWW_TMC_t3(Double_t x, Double_t Q2)
{
  // Wrong way to do; but this is used as a check
  // g2WW using the Twist-2 TMC corrected g1
  Double_t x0     = x;
  Double_t Q20    = Q2;
  Double_t result = - g1p_Twist3_TMC(x0, Q20);
  Int_t N         = fNintegrate;
  Double_t dx     = (1.0 - x) / ((Double_t)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double y = x0+dx*double(i);
    result += g1p_Twist3_TMC(y, Q20)*dx/y;
  }
  return(result);
}
//______________________________________________________________________________

Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1p(Double_t x, Double_t Q2) {
  Double_t result = g1p_Twist2(x,Q2) 
    + g1p_Twist3(x,Q2)
    + g1p_Twist4(x,Q2);
  return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1n(Double_t x, Double_t Q2) {
  Double_t result = g1n_Twist2(x,Q2) 
                   + g1n_Twist3(x,Q2)
                   + g1n_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1d(Double_t x, Double_t Q2) {
   Double_t result = g1d_Twist2(x,Q2) 
                   + g1d_Twist3(x,Q2)
                   + g1d_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1He3(Double_t x, Double_t Q2) {
   Double_t result = g1He3_Twist2(x,Q2) 
                   + g1He3_Twist3(x,Q2)
                   + g1He3_Twist4(x,Q2);
   return(result);
}
//______________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2p(Double_t x, Double_t Q2) {
   Double_t result = g2p_Twist2(x,Q2) 
                   + g2p_Twist3(x,Q2)
                   + g2p_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2n(Double_t x, Double_t Q2) {
   Double_t result = g2n_Twist2(x,Q2) 
                   + g2n_Twist3(x,Q2)
                   + g2n_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2d(Double_t x, Double_t Q2) {
   Double_t result = g2d_Twist2(x,Q2) 
                   + g2d_Twist3(x,Q2)
                   + g2d_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2He3(Double_t x, Double_t Q2) {
   Double_t result = g2He3_Twist2(x,Q2) 
                   + g2He3_Twist3(x,Q2)
                   + g2He3_Twist4(x,Q2);
   return(result);
}
//______________________________________________________________________________

Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1p_TMC(Double_t x, Double_t Q2) {
   Double_t result = g1p_Twist2_TMC(x,Q2) 
                   + g1p_Twist3_TMC(x,Q2)
                   + g1p_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1n_TMC(Double_t x, Double_t Q2) {
   Double_t result = g1n_Twist2_TMC(x,Q2) 
                   + g1n_Twist3_TMC(x,Q2)
                   + g1n_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1d_TMC(Double_t x, Double_t Q2) {
   Double_t result = g1d_Twist2_TMC(x,Q2) 
                   + g1d_Twist3_TMC(x,Q2)
                   + g1d_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g1He3_TMC(Double_t x, Double_t Q2) {
   Double_t result = g1He3_Twist2_TMC(x,Q2) 
                   + g1He3_Twist3_TMC(x,Q2)
                   + g1He3_Twist4_TMC(x,Q2);
   return(result);
}
//______________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2p_TMC(Double_t x, Double_t Q2) {
   Double_t result = g2p_Twist2_TMC(x,Q2) 
                   + g2p_Twist3_TMC(x,Q2)
                   + g2p_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2n_TMC(Double_t x, Double_t Q2) {
   Double_t result = g2n_Twist2_TMC(x,Q2) 
                   + g2n_Twist3_TMC(x,Q2)
                   + g2n_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2d_TMC(Double_t x, Double_t Q2) {
   Double_t result = g2d_Twist2_TMC(x,Q2) 
                   + g2d_Twist3_TMC(x,Q2)
                   + g2d_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromPDFs::g2He3_TMC(Double_t x, Double_t Q2) {
   Double_t result = g2He3_Twist2_TMC(x,Q2) 
                   + g2He3_Twist3_TMC(x,Q2)
                   + g2He3_Twist4_TMC(x,Q2);
   return(result);
}
//______________________________________________________________________________
