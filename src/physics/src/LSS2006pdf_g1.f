!> lss
!! Revision History:
!!  - 1/15/2012  Added build dependence with environment variable InSANE_PDF_GRID_DIR
!!               which points to the directory with the grid files. - Whitney Armstrong (whit@temple.edu)
!!
!!  - 6/2013     Fixed a bug that disallowed repeated calls to the main function.  - David Flay (flay@temple.edu) 
!!
!!  - 10/11/2013 Renamed INTINI common block to INTINI1 so that it
!!               doesn't step on the toes of other routines common
!!               blocks with the same name.
!!               - Whitney Armstrong (whit@temple.edu) 
!!
      !> Initializing subroutine for LSS2006INIT
      !!
      !! Call when changing ISET
      !!
      SUBROUTINE  LSS2006INIT
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      integer IINI,ISET
      COMMON / INTINI1 / IINI
      
c      WRITE(*,*)' o LSS2006 - Setting IINI and ISET to zero.'
      IINI=0
      ISET=0 
      
      END
**********************************************************************
*                                                                    *
*         POLARIZED NLO QCD PARTON DENSITIES                         *
*                                                                    *
*         E. LEADER, A.V. SIDOROV AND D.B. STAMENOV                  *
*                                                                    *
*         Phys. Rev. D75 (2007) 074027 [hep-ph/0612360]              *
*                                                                    *
*         PROBLEMS/QUESTIONS TO sidorov@thsun1.jinr.ru               *
*         OR TO stamenov@inrne.bas.bg                                *
*                                                                    *
*   Three sets of polarized NLO parton densities corresponding to    *
*   positive, negative and changing sign gluon densities are         *
*   presented in MS-bar scheme. The corresponding values of g1 are   *
*   also given. Comparing to the LSS'05 PDFs the new sets are        *
*   determined using the world data set in which the CLAS'06 and     *
*   COMPASS'06 data are included.                                    *
*                                                                    *
*   The sets of PDFs are obtained in the presence of higher twist(HT)*
*   corrections to the spin structure function g1 (5 bins in x have  *
*   been used to extract the HT values) and correspond to the Set2   *
*   LSS'05 PDFs.                                                     *
*                                                                    *
*   The values of g1 are given only in the experimental x region     *
*   (0.0048 < x < 0.751) of the present data because it is not known *
*   how to approximate the higher twist corrections outside this     *
*   region. For more details see the paper.                          *
*                                                                    *
*   Heavy quark thresholds Q(H)=M(H) in the BETA function:           *
*              M(c) = 1.5 GeV,   M(b) = 4.5 GeV.                     *
*                                                                    *
*      NLO:  LAMBDA(3) = 0.353,     LAMBDA(4) = 0.300,               *
*            LAMBDA(5) = 0.206                                       *
*   in the BETA function.                                            *
*                                                                    *
*   INPUT:   ISET = number of the parton set                         *
*             (TO BE DEFINED BY THE USER ):                          *
*            ISET = 1   NEXT-TO-LEADING ORDER (xDelta G > 0)         *
*                      (DATA FILE 'NLO_MS_delGpos.grid' UNIT=11)     *
*                                                                    *
*            ISET = 2   NEXT-TO-LEADING ORDER (xDelta G < 0)         *
*                      (DATA FILE 'NLO_MS_delGneg.grid' UNIT=22)     *
*                                                                    *
*            ISET = 3   NEXT-TO-LEADING ORDER (changing sign         *
*                       xDelta G)                                    *
*                      (DATA FILE 'NLO_MS_chsign_delG.grid' UNIT=33) *
*                                                                    *
*            X  = Bjorken-x       (between  1.E-5  and  1)           *
*            Q2 = scale in GeV**2 (between  1.0 and 0.58E6)          *
*                                                                    *
*   OUTPUT:  UUB = x *(DELTA u + DELTA ubar)                         *
*            DDB = x *(DELTA d + DELTA dbar)                         *
*            SSB = x *(DELTA s + DELTA sbar)                         *
*            GL  = x * DELTA GLUON                                   *
*            UV  = x * DELTA uv                                      *
*            DV  = x * DELTA dv                                      *
*            UB  = x * DELTA ubar                                    *
*            DB  = x * DELTA dbar                                    *
*            ST  = x * DELTA sbar                                    *
*                                                                    *
*            g1p = x*g_1^proton = x*g1p_LT + x*g1p_HT                *
*            g1pLT = x*g1p_{NLO+TMC}                                 *
*                                                                    *
*            g1n = x*g_1^neutron = x*g1n_LT + x*g1n_HT               *
*            g1nLT = x*g1n_{NLO+TMC}                                 *
*                                                                    *
*          (  For the parton distributions and g1(p,n) always        *
*             x times the distribution and g1 is returned   )        *
*                                                                    *
*                                                                    *
*          NOTE: The valence parts DELTA uv, DELTA dv                *
*                                                                    *
*             DELTA uv = (DELTA u + DELTA ubar) - 2*DELTA ubar       *
*             DELTA dv = (DELTA d + DELTA dbar) - 2*DELTA dbar       *
*                                                                    *
*          as well as DELTA ubar, DELTA dbar, DELTA sbar have        *
*          been extracted using the convention of the flavour        *
*          symmetric sea at Q**2 = 1 GeV**2                          *
*               DELTA ubar = DELTA dbar = DELTA sbar                 *
*          and                                                       *
*               DELTA sbar = DELTA s.                                *
*                                                                    *
*   COMMON:  The main program or the calling routine has to have     *
*            a common block  COMMON / INTINI1 / IINI , and  IINI      *
*            has always to be zero when LSS2006 is called for the    *
*            first time or when 'ISET' has been changed.             *
*                                                                    *
**********************************************************************
      SUBROUTINE
     1  LSS2006(ISET,X,Q2,UUB,DDB,SSB,GL,UV,DV,UB,DB,ST
     1          ,g1pLT,g1p,g1nLT,g1n
     1 )

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (NPART=13, NX=48, NQ=28, NARG=2)
      DIMENSION XUUBF(NX,NQ), XDDBF(NX,NQ),XSSBF(NX,NQ),
     1 XUVF(NX,NQ), XDVF(NX,NQ), XUBF(NX,NQ), XDBF(NX,NQ),
     1 XSF(NX,NQ), XGF(NX,NQ),
     1 Xg1pLTf(nX,nQ),Xg1pf(nX,nQ),Xg1nLTf(nX,nQ),Xg1nf(nX,nQ),
     1 PARTON (NPART,NQ,NX),
     2 QS(NQ), XB(NX), XT(NARG), NA(NARG), ARRF(NX+NQ)
      COMMON / INTINI1 / IINI
C      CHARACTER*2  STAR
      CHARACTER*69  STAR
      SAVE XUUBF,XDDBF,XSSBF, XUVF, XDVF, XUBF, XDBF, XSF, XGF,
     1  Xg1pf,Xg1nf,NA, ARRF


*...BJORKEN-X AND Q**2 VALUES OF THE GRID :
       DATA QS / 1.0D0, 1.25D0, 1.5D0, 2.D0, 2.5D0,
     1           4.0D0, 6.4D0, 1.0D1, 1.5D1, 2.5D1, 4.0D1, 6.4D1,
     2           1.0D2, 1.8D2, 3.2D2, 5.8D2, 1.0D3, 1.8D3,
     3           3.2D3, 5.8D3, 1.0D4, 1.8D4, 3.2D4, 5.8D4,
     4           1.0D5, 1.8D5, 3.2D5, 5.8D5 /
       DATA XB /
     1           1.D-5, 1.5D-5, 2.2D-5, 3.2D-5, 4.8D-5, 7.D-5,
     2           1.D-4, 1.5D-4, 2.2D-4, 3.2D-4, 4.8D-4, 7.D-4,
     3           1.D-3, 1.5D-3, 2.2D-3, 3.2D-3, 4.8D-3, 7.D-3,
     4           1.D-2, 1.5D-2, 2.2D-2, 3.2D-2, 5.0D-2, 7.5D-2,
     5           0.1, 0.125, 0.15, 0.175, 0.2, 0.225, 0.25, 0.275,
     6           0.3, 0.325, 0.35, 0.375, 0.4, 0.45,  0.5, 0.55,
     7           0.6, 0.65,  0.7,  0.75,  0.8, 0.85,  0.9, 1.0 /



*...CHECK OF X AND Q2 VALUES :

       IF ( (X.LT.1.0D-5) .OR. (X.GT.1.0D0) ) THEN
           WRITE(6,91)
  91       FORMAT (2X,'PARTON INTERPOLATION: X OUT OF RANGE')
           STOP
       ENDIF
       IF ( (Q2.LT.1.D0) .OR. (Q2.GT.5.8D5) ) THEN
           WRITE(6,92)
  92       FORMAT (2X,'PARTON INTERPOLATION: Q2 OUT OF RANGE')
           WRITE(*,*) 'Q2 = ',Q2 
           STOP
       ENDIF
*...INITIALIZATION :
*    SELECTION AND READING OF THE GRID :
*                                  ( third NUMBER IN THE grid)
*   INPUT:   ISET = number of the parton set
*            ISET = 1 NEXT-TO-LEADING ORDER (xDelta G > 0)
*   FILE - NO. = 11                                     -1.8646E-04
*
*            ISET = 2 NEXT-TO-LEADING ORDER (xDelta G < 0)
*   FILE - NO. = 22:                                     3.4396E-05
*            ISET = 3 NEXT-TO-LEADING ORDER (changing sign xDelta G)
*   FILE - NO. = 33                                     -3.7687E-05


      IF (IINI.NE.0) GOTO 16
      IF (ISET.EQ.1) THEN
       IIREAD=11
       OPEN(UNIT=11,FILE=InSANE_PDF_GRID_DIR//'/NLO_MS_delGpos.grid',STATUS='OLD')
      ELSE IF (ISET.EQ.2) THEN
       IIREAD=22
       OPEN(UNIT=22,FILE=InSANE_PDF_GRID_DIR//'/NLO_MS_delGneg.grid',STATUS='OLD')
      ELSE IF (ISET.EQ.3) THEN
       IIREAD=33
c       WRITE(*,*) 'OPENING THE FILE FOR ISET = 3'
       OPEN(UNIT=33,FILE=InSANE_PDF_GRID_DIR//'/NLO_MS_chsign_delG.grid'
     &     ,STATUS='OLD')
      ELSE
        WRITE(6,93)
  93    FORMAT (2X,'PARTON INTERPOLATION: ISET OUT OF RANGE')
        GOTO 60
      END IF
C

      READ(IIREAD,2004) STAR
! 2004 FORMAT (A2)
 2004 FORMAT (A69)

c      WRITE(*,*) 'First line: ',STAR 

       DO 15 N = 1, NQ

       DO 15 M = 1, NX

      if(Iset.eq.1.or.Iset.eq.2.or.Iset.eq.3) then
       READ(IIREAD,190) q2gri, xgri,
     1                 PARTON( 1,N,M), PARTON( 2,N,M), PARTON( 3,N,M),
     1                 PARTON( 4,N,M), PARTON( 5,N,M), PARTON( 6,N,M),
     1                 PARTON( 7,N,M), PARTON( 8,N,M), PARTON( 9,N,M),
     1                 PARTON(10,N,M), PARTON(11,N,M),
     1                 PARTON(12,N,M), PARTON(13,N,M)

 190   FORMAT (2d9.3,13(1pd12.4))

            endif
  15   CONTINUE
C
      IINI = 1
*....ARRAYS FOR THE INTERPOLATION SUBROUTINE :
      DO 10 IQ = 1, NQ
      DO 20 IX = 1, NX-1


       XB0 = XB(IX)
       XB1 = 1.D0-XB(IX)

       XUUBF(iX,iQ) = PARTON(1,IQ,IX) / (XB1**3 * XB0**0.5)
       XDDBF(iX,iQ) = PARTON(2,IQ,IX) / (XB1**3 * XB0**0.5)
       XSSBF(iX,iQ) = PARTON(3,IQ,IX) / (XB1**7 * XB0**0.5)

       XGF(IX,IQ)  = PARTON(4,IQ,IX) / (XB1**6 * XB0**3.)
       XUVF(IX,IQ) = PARTON(5,IQ,IX) / (XB1**3 * XB0**0.5)
       XDVF(IX,IQ) = PARTON(6,IQ,IX) / (XB1**3 * XB0**0.5)
       XUBF(IX,IQ) = PARTON(7,IQ,IX) / (XB1**7 * XB0**0.5)
       XDBF(IX,IQ) = PARTON(8,IQ,IX) / (XB1**7 * XB0**0.5)
       XSF(IX,IQ)  = PARTON(9,IQ,IX) / (XB1**7 * XB0**0.5)


       Xg1pLTf(IX,IQ) = PARTON(10,IQ,IX) / (XB1**3 * XB0**0.5)
       Xg1pf  (IX,IQ) = PARTON(11,IQ,IX) / (XB1**3 * XB0**0.5)
       Xg1nLTf(IX,IQ) = PARTON(12,IQ,IX) / (XB1**3 * XB0**0.5)
       Xg1nf  (IX,IQ) = PARTON(13,IQ,IX) / (XB1**3 * XB0**0.5)

  20  CONTINUE
        XUUBF(nX,iQ) =0.d0
        XDDBF(nX,iQ) =0.d0
        XSSBF(nX,iQ) =0.d0
        XUVF(NX,IQ) = 0.D0
        XDVF(NX,IQ) = 0.D0
        XUBF(NX,IQ) = 0.D0
        XDBF(NX,IQ) = 0.D0
        XSF(NX,IQ)  = 0.D0
        XGF(NX,IQ)  = 0.D0

        Xg1pLTf(NX,IQ) = 0.d0
        Xg1pf  (NX,IQ) = 0.d0
        Xg1nLTf(NX,IQ) = 0.d0
        Xg1nf  (NX,IQ) = 0.d0

  10  CONTINUE
      NA(1) = NX
      NA(2) = NQ
      DO 30 IX = 1, NX
        ARRF(IX) = DLOG(XB(IX))
  30  CONTINUE
      DO 40 IQ = 1, NQ
        ARRF(NX+IQ) = DLOG(QS(IQ))
  40  CONTINUE
  16  CONTINUE
*...INTERPOLATION :
      XT(1) = DLOG(X)
      XT(2) = DLOG(Q2)
      UUB = DFINT(NARG,XT,NA,ARRF,XUUBF)  * (1.D0-X)**3 * X**0.5
      DDB = DFINT(NARG,XT,NA,ARRF,XDDBF)  * (1.D0-X)**3 * X**0.5
      SSB = DFINT(NARG,XT,NA,ARRF,XSSBF)  * (1.D0-X)**7 * X**0.5
      GL  = DFINT(NARG,XT,NA,ARRF,XGF)  * (1.D0-X)**6 * X**3.
      UV  = DFINT(NARG,XT,NA,ARRF,XUVF) * (1.D0-X)**3 * X**0.5
      DV  = DFINT(NARG,XT,NA,ARRF,XDVF) * (1.D0-X)**3 * X**0.5
      UB  = DFINT(NARG,XT,NA,ARRF,XUBF) * (1.D0-X)**7 * X**0.5
      DB  = DFINT(NARG,XT,NA,ARRF,XDBF) * (1.D0-X)**7 * X**0.5
      ST  = DFINT(NARG,XT,NA,ARRF,XSF)  * (1.D0-X)**7 * X**0.5


      g1pLT = DFINT(NARG,XT,NA,ARRF,Xg1pLTf) * (1.D0-X)**3 * X**0.5
      g1p   = DFINT(NARG,XT,NA,ARRF,Xg1pf  ) * (1.D0-X)**3 * X**0.5
      g1nLT = DFINT(NARG,XT,NA,ARRF,Xg1nLTf) * (1.D0-X)**3 * X**0.5
      g1n   = DFINT(NARG,XT,NA,ARRF,Xg1nf  ) * (1.D0-X)**3 * X**0.5

      IF (ISET.EQ.1) THEN
       CLOSE(11) 
      ELSE IF (ISET.EQ.2) THEN
       CLOSE(22) 
      ELSE IF (ISET.EQ.3) THEN
       CLOSE(33) 
      END IF
C


 60   RETURN
      END
*
*...CERN LIBRARY ROUTINE E104 (INTERPOLATION) :
*
      FUNCTION DFINT(NARG,ARG,NENT,ENT,TABLE)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION ARG(5),NENT(5),ENT(73),TABLE(1200)
      DIMENSION D(5),NCOMB(5),IENT(5)
      DFINT=0.D0
      KD=1
      M=1
      JA=1
         DO 5 I=1,NARG
      NCOMB(I)=1
      JB=JA-1+NENT(I)
         DO 2 J=JA,JB
      IF (ARG(I).LE.ENT(J)) GO TO 3
    2 CONTINUE
      J=JB
    3 IF (J.NE.JA) GO TO 4
      J=J+1
    4 JR=J-1
      D(I)=(ENT(J)-ARG(I))/(ENT(J)-ENT(JR))
      IENT(I)=J-JA
      KD=KD+IENT(I)*M
      M=M*NENT(I)
    5 JA=JB+1
      DFINT=0.D0
   10 FAC=1.D0
      IADR=KD
      IFADR=1
         DO 15 I=1,NARG
      IF (NCOMB(I).EQ.0) GO TO 12
      FAC=FAC*(1.D0-D(I))
      GO TO 15
   12 FAC=FAC*D(I)
      IADR=IADR-IFADR
   15 IFADR=IFADR*NENT(I)
      DFINT=DFINT+FAC*TABLE(IADR)
      IL=NARG
   40 IF (NCOMB(IL).EQ.0) GO TO 80
      NCOMB(IL)=0
      IF (IL.EQ.NARG) GO TO 10
      IL=IL+1
         DO 50  K=IL,NARG
   50 NCOMB(K)=1
      GO TO 10
   80 IL=IL-1
      IF(IL.NE.0) GO TO 40
      RETURN
      END

! CERNLIB E104 modified to be used with (G)JR GRIDS:
! Name changed from fint to dfint.
! Real variables changed to double precision.
! External references to CERNLIB (error handling) routines removed.
!           DOUBLE PRECISION FUNCTION DFINT(NARG,ARG,NENT,ENT,TABLE)
!           INTEGER   NENT(9), INDEX(32)
!           DOUBLE PRECISION ARG(9),   ENT(9),   TABLE(9), WEIGHT(32)
!           DFINT  =  0d0
!           IF(NARG .LT. 1  .OR.  NARG .GT. 5)  GOTO 300
!           LMAX      =  0
!           ISTEP     =  1
!           KNOTS     =  1
!           INDEX(1)  =  1
!           WEIGHT(1) =  1d0
!           DO 100    N  =  1, NARG
!              X     =  ARG(N)
!              NDIM  =  NENT(N)
!              LOCA  =  LMAX
!              LMIN  =  LMAX + 1
!              LMAX  =  LMAX + NDIM
!              IF(NDIM .GT. 2)  GOTO 10
!              IF(NDIM .EQ. 1)  GOTO 100
!              H  =  X - ENT(LMIN)
!              IF(H .EQ. 0.)  GOTO 90
!              ISHIFT  =  ISTEP
!              IF(X-ENT(LMIN+1) .EQ. 0d0)  GOTO 21
!              ISHIFT  =  0
!              ETA     =  H / (ENT(LMIN+1) - ENT(LMIN))
!              GOTO 30
!   10         LOCB  =  LMAX + 1
!   11         LOCC  =  (LOCA+LOCB) / 2
!              IF(X-ENT(LOCC))  12, 20, 13
!   12         LOCB  =  LOCC
!              GOTO 14
!   13         LOCA  =  LOCC
!   14         IF(LOCB-LOCA .GT. 1)  GOTO 11
!              LOCA    =  MIN0( MAX0(LOCA,LMIN), LMAX-1 )
!              ISHIFT  =  (LOCA - LMIN) * ISTEP
!              ETA     =  (X - ENT(LOCA)) / (ENT(LOCA+1) - ENT(LOCA))
!              GOTO 30
!   20         ISHIFT  =  (LOCC - LMIN) * ISTEP
!   21         DO 22  K  =  1, KNOTS
!                 INDEX(K)  =  INDEX(K) + ISHIFT
!   22            CONTINUE
!              GOTO 90
!   30         DO 31  K  =  1, KNOTS
!                 INDEX(K)         =  INDEX(K) + ISHIFT
!                 INDEX(K+KNOTS)   =  INDEX(K) + ISTEP
!                 WEIGHT(K+KNOTS)  =  WEIGHT(K) * ETA
!                 WEIGHT(K)        =  WEIGHT(K) - WEIGHT(K+KNOTS)
!   31            CONTINUE
!              KNOTS  =  2*KNOTS
!   90         ISTEP  =  ISTEP * NDIM
!  100         CONTINUE
!           DO 200    K  =  1, KNOTS
!              I  =  INDEX(K)
!              DFINT  =  DFINT + WEIGHT(K) * TABLE(I)
!  200         CONTINUE
!           RETURN
!  300      WRITE(*,1000) NARG
!           STOP
! 1000      FORMAT( 7X, 24HFUNCTION DFINT... NARG =,I6,
!      +              17H NOT WITHIN RANGE)
!           END


