#include "InSANEPOLRADUltraRelInelasticTailDiffXSec.h"

InSANEPOLRADUltraRelInelasticTailDiffXSec::InSANEPOLRADUltraRelInelasticTailDiffXSec()
{
   SetTitle("InSANEPOLRADInelasticTailDiffXSec");
   SetPlotTitle("POLRAD IRT cross-section");

   fPOLRADCalc = new InSANEPOLRADUltraRelativistic();
   fPOLRADCalc->SetVerbosity(1);
   //fPOLRADCalc->DoQEFullCalc(false); 
   fPOLRADCalc->SetTargetNucleus(InSANENucleus::Proton());
   fPOLRADCalc->fErr   = 1E-7;   // integration error tolerance 
   fPOLRADCalc->fDepth = 6;     // number of iterations for integration 
   fPOLRADCalc->SetMultiPhoton(true); 
   //fPOLRADCalc->SetUltraRel(true); 

   InSANEFunctionManager * funcMan = InSANEFunctionManager::GetInstance();

   // Structure functions F1,F2
   InSANEStructureFunctions * sf   = funcMan->GetStructureFunctions();
   SetUnpolarizedStructureFunctions(sf);

   // Structure functions g1,g2
   InSANEPolarizedStructureFunctions * psf   = funcMan->GetPolarizedStructureFunctions();
   SetPolarizedStructureFunctions(psf);

   // Quasi Elastic structure functions
   auto * F1F209QESFs = new F1F209QuasiElasticStructureFunctions();
   SetQEStructureFunctions(F1F209QESFs);

   // Nucleon form factors 
   InSANEFormFactors * FFs = funcMan->GetFormFactors(); 
   SetFormFactors(FFs);

   // Nuclei form factors 
   //InSANEFormFactors * NFFs = new AmrounFormFactors();
   InSANEFormFactors * NFFs = new MSWFormFactors();
   SetTargetFormFactors(NFFs);

}
//______________________________________________________________________________

InSANEPOLRADUltraRelInelasticTailDiffXSec::~InSANEPOLRADUltraRelInelasticTailDiffXSec()
{ }
//______________________________________________________________________________

InSANEPOLRADUltraRelInelasticTailDiffXSec *  
InSANEPOLRADUltraRelInelasticTailDiffXSec::Clone(const char * newname) const 
{
   std::cout << "InSANEPOLRADUltraRelInelasticTailDiffXSec::Clone()\n";
   auto * copy = new InSANEPOLRADUltraRelInelasticTailDiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

Double_t InSANEPOLRADUltraRelInelasticTailDiffXSec::EvaluateXSec(
      const Double_t *x) const
{
   if (!VariablesInPhaseSpace(fnDim, x)){
      std::cout << "[InSANEPOLRADInelasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }

   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t Ebeam   = GetBeamEnergy();
   Double_t nu      = Ebeam-Eprime;
   Double_t Mtarg   = M_p/GeV;// 0.938

   fPOLRADCalc->SetKinematics(Ebeam,Eprime,theta);

   Double_t sig_rad  = ((InSANEPOLRADUltraRelativistic*)fPOLRADCalc)->sig_in();

   sig_rad = sig_rad * (Eprime/(2.0*TMath::Pi()*Mtarg*nu));
   if(IncludeJacobian()){
      sig_rad *= TMath::Sin(theta);
   }
   return sig_rad*hbarc2_gev_nb;
} 
//______________________________________________________________________________

