#include "InSANEDISTrack.h"

ClassImp(InSANEDISTrack)
//______________________________________________________________________________
InSANEDISTrack::InSANEDISTrack() {
   fHelicity    = 0;
   fTheta       = 0.0;
   fPhi         = 0.0;
   fDeltaTheta  = 0.0;
   fDeltaPhi    = 0.0;
   fDeltaEnergy = 0.0;
   fSubDetector = 0;
   fBeamEnergy  = 0.0;
   Clear();
}
//______________________________________________________________________________
InSANEDISTrack::~InSANEDISTrack() {
}
//______________________________________________________________________________
void InSANEDISTrack::Clear(Option_t * opt) {
   InSANETrack::Clear(opt);
   fHelicity          = 0;
   fEnergy            = 0.0;
   fTheta             = 0.0;
   fPhi               = 0.0;
   fNTrackerNeighbors = 0;
   fDeltaTheta        = 0.0;
   fDeltaPhi          = 0.0;
   fDeltaEnergy       = 0.0;
   fSubDetector       = 0;
   fTrackerDeltaPhi   = -9.9;
   fIsGood            = false;
}
//______________________________________________________________________________

