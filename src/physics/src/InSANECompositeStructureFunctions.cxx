#include "InSANECompositeStructureFunctions.h"


ClassImp(InSANECompositeStructureFunctions)
//______________________________________________________________________________
InSANECompositeStructureFunctions::InSANECompositeStructureFunctions(){
   fNSFs = 0;
   fSFList.Clear();
   SetLabel("F1F209+NMC");
}
//______________________________________________________________________________
InSANECompositeStructureFunctions::~InSANECompositeStructureFunctions()
{ }
//______________________________________________________________________________
void   InSANECompositeStructureFunctions::Add(InSANEStructureFunctions * sf)
{
   fSFList.Add(sf); fNSFs = fSFList.GetEntries();
}
//______________________________________________________________________________
Double_t InSANECompositeStructureFunctions::F2p(Double_t x, Double_t Qsq){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = 0.0;

   Double_t w = GetWeight(0,x,Qsq);
   if( w > 0.0 ) {
      res        +=  w*asf->F2p(x,Qsq);
   }
   w =  GetWeight(1,x,Qsq);
   if( w > 0.0 ) {
      res        +=  w*bsf->F2p(x,Qsq);
   }
   return( res );
}
//______________________________________________________________________________
Double_t InSANECompositeStructureFunctions::F1p(Double_t x, Double_t Qsq){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = 0.0;

   Double_t w = GetWeight(0,x,Qsq);
   if( w > 0.0 ) {
      res        +=  w*asf->F1p(x,Qsq);
   }
   w =  GetWeight(1,x,Qsq);
   if( w > 0.0 ) {
      res        +=  w*bsf->F1p(x,Qsq);
   }
   return( res );
   //InSANEStructureFunctions * asf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->F1p(x,Qsq) );
}
//______________________________________________________________________________
// Proton
Double_t InSANECompositeStructureFunctions::F2n(Double_t x, Double_t Qsq){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = GetWeight(0,x,Qsq)*asf->F2n(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->F2n(x,Qsq);
   return( res );
   //InSANEStructureFunctions * asf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->F2n(x,Qsq) );
}
//______________________________________________________________________________
Double_t InSANECompositeStructureFunctions::F1n(Double_t x, Double_t Qsq){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = GetWeight(0,x,Qsq)*asf->F1n(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->F1n(x,Qsq);
   return( res );
   //InSANEStructureFunctions * asf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->F1n(x,Qsq) );
}
//______________________________________________________________________________
// Deuteron
Double_t InSANECompositeStructureFunctions::F2d(Double_t x, Double_t Qsq){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = GetWeight(0,x,Qsq)*asf->F2d(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->F2d(x,Qsq);
   return( res );
   //InSANEStructureFunctions * asf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->F2d(x,Qsq) );
}
//______________________________________________________________________________
Double_t InSANECompositeStructureFunctions::F1d(Double_t x, Double_t Qsq){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = GetWeight(0,x,Qsq)*asf->F1d(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->F1d(x,Qsq);
   return( res );
   //InSANEStructureFunctions * asf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->F1d(x,Qsq) );
}
//______________________________________________________________________________
// He3 
Double_t InSANECompositeStructureFunctions::F2He3(Double_t x, Double_t Qsq){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = GetWeight(0,x,Qsq)*asf->F2He3(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->F2He3(x,Qsq);
   return( res );
   //InSANEStructureFunctions * asf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->F2He3(x,Qsq) );
}
//______________________________________________________________________________
Double_t InSANECompositeStructureFunctions::F1He3(Double_t x, Double_t Qsq){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = GetWeight(0,x,Qsq)*asf->F1He3(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->F1He3(x,Qsq);
   return( res );
   //InSANEStructureFunctions * asf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->F1He3(x,Qsq) );
}
//______________________________________________________________________________
Double_t InSANECompositeStructureFunctions::F2Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = GetWeight(0,x,Qsq)*asf->F2Nuclear(x,Qsq,Z,A);
   res        +=  GetWeight(1,x,Qsq)*bsf->F2Nuclear(x,Qsq,Z,A);
   return( res );
}
//______________________________________________________________________________
Double_t InSANECompositeStructureFunctions::F1Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A){
   auto * asf = (InSANEStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (InSANEStructureFunctions *)fSFList.At( 1 );
   Double_t res = GetWeight(0,x,Qsq)*asf->F1Nuclear(x,Qsq,Z,A);
   res        +=  GetWeight(1,x,Qsq)*bsf->F1Nuclear(x,Qsq,Z,A);
   return( res );
}


ClassImp(LowQ2StructureFunctions)

//______________________________________________________________________________
LowQ2StructureFunctions::LowQ2StructureFunctions(){

   SetNameTitle("LowQ2StructureFunctions","Low Q2 SFs F1F209+NMC fit");
   //xrange[0] = 0.0;
   //xrange[1] = 0.368;
   //xrange[2] = 0.40;
   //xrange[3] = 1.0;
   //xrange[4] = 1.0;

   /// initialize pdfs and sfs if needed


   /// \todo rename this method and polarized partner to just SetPDFs
   //fCTEQSFs.SetUnpolarizedPDFs(&fPDFs);   

   Add(&fNMCSFs);    // = 0
   Add(&fF1F209SFs); // = 1
}
//______________________________________________________________________________
LowQ2StructureFunctions::~LowQ2StructureFunctions(){
}
//______________________________________________________________________________
Double_t LowQ2StructureFunctions::GetWeight(Int_t iSF, Double_t x, Double_t Q2){

   // F1F209 is good for W<3 GeV and Q2<10 GeV^2
   // There are 3 overlap regions where the weights change for a smooth transfer
   // The width in W is dW and for Q2 dQ2.
   
   Double_t W   = InSANE::Kine::W_xQsq(x,Q2);
   Double_t dQ2 = 1.0;
   Double_t dW  = 0.7;
   Double_t W0  = 2.5;
   Double_t Q20 = 10.0;

   if( (W < W0 - dW) && (Q2 < Q20 - dQ2) ) {
      // F1F209 
      if( iSF == 0 ) { // DIS
         return 0.0;
      } else if( iSF == 1 ) { // F1F209
         return 1.0;
      }
   }

   if( (W >= W0 ) || (Q2 >= Q20 ) ) {
      // not F1F209 
      if( iSF == 0 ) { // DIS
         return 1.0;
      } else if( iSF == 1 ) { // F1F209
         return 0.0;
      }
   }
   
   // Over lap in W
   if( ( TMath::Abs(W - W0 + dW/2.0) <= dW/2.0 ) && (Q2 < Q20 - dQ2) ) {
      Double_t W1 = (W - W0 + dW)/dW;
      if( iSF == 0 ) {
         return(W1);
      } else if( iSF == 1 ) {
         return(1.0 - W1);
      }

   }
   // Overlap in Q2 
   if( ( TMath::Abs(Q2 - Q20 + dQ2/2.0) <= dQ2/2.0 ) && (W < W0 - dW) ) {
      Double_t W1 = (Q2 - Q20 + dQ2)/dQ2;
      if( iSF == 0 ) {
         return(W1);
      } else if( iSF == 1 ) {
         return(1.0 - W1);
      }

   }
   // Overlap in Q2 and W  
   if( ( TMath::Abs(W - W0 + dW/2.0) <= dW/2.0 ) && ( TMath::Abs(Q2 - Q20 + dQ2/2.0) <= dQ2/2.0 ) ) {
      Double_t x1 = (Q2 - Q20 + dQ2)/dQ2;
      Double_t y1 = (W - W0 + dW)/dW;
      Double_t W1 = x1 + y1 - x1*y1;
      if( iSF == 0 ) {
         return(W1);
      } else if( iSF == 1 ) {
         return(1.0 - W1);
      }

   }

   std::cout << "W  " << W <<std::endl;
   std::cout << "Q2 " << Q2 <<std::endl;
   Error("GetWeight","Should not have made it here");
   return 0.0;

      
   //if(Q2 > 8.0 ) return 1.0;
   //   if( x > xrange[1] && x < xrange[2] ) {
   //      double weight = 1.0 - (x - xrange[1])/(xrange[2] - xrange[1]);
   //      return weight;
   //   } else if ( x <= xrange[1] ){
   //      return(1.0);
   //   } else {
   //      return(0.0);
   //   }
   //} else if( iSF == 1 ) {
   //   if(Q2 > 8.0 ) return 0.0;
   //   // F1F209
   //   if( x > xrange[1] && x < xrange[2] ) {
   //      double weight = (x - xrange[1])/(xrange[2] - xrange[1]);
   //      return weight;
   //   } else if ( x <= xrange[1] ){
   //      return(0.0);
   //   } else {
   //      return(1.0);
   //   }
   //}
}
//______________________________________________________________________________

