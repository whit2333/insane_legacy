#include "StructureFunctions.h"
#include "Physics.h"

namespace insane {
  namespace physics {
    StructureFunctions::StructureFunctions()
    { }
    //______________________________________________________________________________

    StructureFunctions::~StructureFunctions()
    { }
    //______________________________________________________________________________
 
    double StructureFunctions::Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const {
    //double StructureFunctions::Get(std::tuple<SF,Nuclei> sf, double x, double Q2) const {
      auto sftype = std::get<SF>(sf);
      auto target = std::get<Nuclei>(sf);

      double result = 0.0;
      switch(sftype) {

        case SF::F1 :
          if( l == OPELimit::Massless) {
            result = F1(x, Q2, target,t); 
          } else {
            result = F1_TMC(x, Q2, target,t); 
          }
          break;

        case SF::F2 :
          if( l == OPELimit::Massless) {
            result = F2(x, Q2, target,t); 
          } else {
            result = F2_TMC(x, Q2, target,t); 
          }
          break;
      }
      return result;
    }
    //______________________________________________________________________________

    double StructureFunctions::F1(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return F1p(x,Q2);
          break;

        case Nuclei::n : 
          return F1n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double StructureFunctions::F2(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return F2p(x,Q2);
          break;

        case Nuclei::n : 
          return F2n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
    
    double StructureFunctions::F1_TMC(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return F1p_TMC(x,Q2);
          break;

        case Nuclei::n : 
          return F1n_TMC(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double StructureFunctions::F2_TMC(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return F2p_TMC(x,Q2);
          break;

        case Nuclei::n : 
          return F2n_TMC(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double StructureFunctions::FL(double x, double Q2, Nuclei target, Twist t) const
    {
      // F_L = \rho^2 F_2 - 2x F_1) = 2x F_1 R 
      double r2  = insane::kinematics::rho2(x,Q2);
      double res = r2*F2_TMC(x,Q2,target,t)-2.0*x*F1_TMC(x,Q2,target,t);
      return res;
    }
    //______________________________________________________________________________

    double StructureFunctions::R(double x, double Q2, Nuclei target, Twist t) const
    {
      double r2  = insane::kinematics::rho2(x,Q2);
      double F_1 = F1_TMC(x,Q2,target,t);
      double F_L = r2*F2_TMC(x,Q2,target,t)-2.0*x*F_1;
      double res = F_L/(2.0*x*F_1);
      return res;
    }
    //______________________________________________________________________________

  }
}
