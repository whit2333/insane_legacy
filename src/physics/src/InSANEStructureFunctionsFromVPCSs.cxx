#include "InSANEStructureFunctionsFromVPCSs.h"
#include "TMath.h"
#include "InSANEPhysicalConstants.h"
#include "InSANEMathFunc.h"
#include "MAIDPhotoAbsorptionCrossSections.h"

//______________________________________________________________________________
InSANEStructureFunctionsFromVPCSs::InSANEStructureFunctionsFromVPCSs()
{
   SetLabel("MAID07");
   SetNameTitle("SFs from MAID07","SFs from MAID07");
   fVPCSs = new MAIDPhotoAbsorptionCrossSections();   // Virtual Photoabsorption Cross Sections
   f4piAlphaOverM = 4.0*TMath::Pi()*TMath::Pi()*fine_structure_const/(M_p/GeV);
}
//______________________________________________________________________________
InSANEStructureFunctionsFromVPCSs::~InSANEStructureFunctionsFromVPCSs()
{ }
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromVPCSs::F1p(   Double_t x, Double_t Q2){
   if(!fVPCSs) {
      return 0.0;
   }
   fVPCSs->CalculateProton(x,Q2);
   Double_t M     = M_p/GeV;
   Double_t K     = InSANE::Kine::K_Hand(x,Q2);
   Double_t sig_T = fVPCSs->Sig_T();
   Double_t res   = sig_T*K/f4piAlphaOverM;
   return res;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromVPCSs::F2p(   Double_t x, Double_t Q2){
   if(!fVPCSs) {
      return 0.0;
   }
   fVPCSs->CalculateProton(x,Q2);
   Double_t M     = M_p/GeV;
   Double_t nu    = Q2/(2.0*M*x);
   Double_t K     = InSANE::Kine::K_Hand(x,Q2);
   Double_t B1    = (f4piAlphaOverM/(K*nu))*(1.0+nu*nu/Q2) ;
   Double_t sig_T = fVPCSs->Sig_T();
   Double_t sig_L = fVPCSs->Sig_L();
   Double_t res   = (sig_L + sig_T)/B1;
   return res;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromVPCSs::F1n(   Double_t x, Double_t Q2){
   return 0.0;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromVPCSs::F2n(   Double_t x, Double_t Q2){
   return 0.0;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromVPCSs::F1d(   Double_t x, Double_t Q2){
   return 0.0;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromVPCSs::F2d(   Double_t x, Double_t Q2){
   return 0.0;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromVPCSs::F1He3( Double_t x, Double_t Q2){
   return 0.0;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromVPCSs::F2He3( Double_t x, Double_t Q2){
   return 0.0;
}
//______________________________________________________________________________
