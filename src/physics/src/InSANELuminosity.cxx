#include "InSANELuminosity.h"

ClassImp(InSANELuminosity)

//______________________________________________________________________________
InSANELuminosity::InSANELuminosity(InSANETarget * targ, Double_t current) {
   fTarget      = targ;
   fBeamCurrent = current;
}
//______________________________________________________________________________
InSANELuminosity::~InSANELuminosity() {
}
//______________________________________________________________________________
Double_t InSANELuminosity::CalculateLuminosity() const {
   fLuminosity = 0.0;
   for (int i = 0; i < fTarget->GetNMaterials(); i++) {
      fLuminosity += GetMaterialLuminosity(i);
   }
   return(fLuminosity);
}
//______________________________________________________________________________
Double_t InSANELuminosity::CalculateLuminosityPerNucleon() const {
   Double_t res = 0.0;
   for (int i = 0; i < fTarget->GetNMaterials(); i++) {
      res += ( GetMaterialLuminosity(i)*fTarget->GetMaterial(i)->fA);
   }
   return(res);
}
//______________________________________________________________________________
Double_t InSANELuminosity::GetMaterialLuminosity(int i) const {
   if (!fTarget) {
      Error("GetMaterialLuminosity", "No Target Defined");
      return(0.0);
   }
   auto* aMat = (InSANETargetMaterial*) fTarget->GetMaterial(i);
   if (!aMat) return(0.0);
   return(GetElectronFlux() * aMat->GetLengthDensity());
}
//______________________________________________________________________________

void InSANELuminosity::Print(Option_t * opt) const {
   Print(std::cout);
   //fTarget->Print();
   //std::cout << " Luminosity   = " << CalculateLuminosity() << " cm^-2 s^-1\n";
   //std::cout << " fBeamCurrent = " << fBeamCurrent << " A\n";
   //std::cout << " e charge     = " << ELECTRONCHARGE << " C\n";
   //std::cout << " e flux       = " << GetElectronFlux() << " 1/s\n";
}
//______________________________________________________________________________


void InSANELuminosity::Print(std::ostream& s) const {
   //fTarget->Print();
   s << "Luminosity for target: " << fTarget->GetTitle() << "  (" << fTarget->GetName() << ")" << std::endl;
   Double_t rl_tot=0.0;
   for (int i = 0; i < fTarget->GetNMaterials(); i++) {
      InSANETargetMaterial* mat = fTarget->GetMaterial(i);
      mat->Print(s);
      double lum = GetMaterialLuminosity(i);
      s << "   Luminosity = " <<  lum << " x " << mat->fA << " = " <<  lum * mat->fA << " cm^-2 s^-1 per nucleon" << std::endl;
      rl_tot += mat->GetNumberOfRadiationLengths();
   }
   s << " Total thickness = " << rl_tot << " (radiation lengths)" << std::endl;
   s << " Luminosity   = " << CalculateLuminosityPerNucleon() << " cm^-2 s^-1 per nucleon\n";
   s << " fBeamCurrent = " << fBeamCurrent << " A\n";
   s << " e flux       = " << GetElectronFlux() << " 1/s\n";
   s << " e charge     = " << ELECTRONCHARGE << " C\n";
}
//______________________________________________________________________________

