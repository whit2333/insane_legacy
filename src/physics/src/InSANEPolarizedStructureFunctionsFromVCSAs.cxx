#include "InSANEPolarizedStructureFunctionsFromVCSAs.h"
#include "InSANEFunctionManager.h"

//______________________________________________________________________________
InSANEPolarizedStructureFunctionsFromVCSAs::InSANEPolarizedStructureFunctionsFromVCSAs()
{
   SetLabel("MAID07");
   SetNameTitle("Spin SFs from MAID07", "Spin SFs from MAID07");
   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   fSFs   = fman->CreateSFs(9);
   fVCSAs = nullptr;
}
//______________________________________________________________________________
InSANEPolarizedStructureFunctionsFromVCSAs::~InSANEPolarizedStructureFunctionsFromVCSAs()
{
}
//______________________________________________________________________________

Double_t InSANEPolarizedStructureFunctionsFromVCSAs::g1p(   Double_t x, Double_t Q2)
{
   if(!fVCSAs) return 0.0;
   double M = (M_p/GeV);
   double gamma2 = (4.0*M*M*x*x)/Q2;
   double F1 =  fSFs->F1p(x,Q2);

   double g1 = (F1/(1.0+gamma2))*(fVCSAs->A1p(x,Q2) + TMath::Sqrt(gamma2)*fVCSAs->A2p(x,Q2));
   return g1;
}
//______________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromVCSAs::g1n(   Double_t x, Double_t Q2)
{
   if(!fVCSAs) return 0.0;
   double M = (M_p/GeV);
   double gamma2 = (4.0*M*M*x*x)/Q2;
   double F1 =  fSFs->F1n(x,Q2);

   double g1 = (F1/(1.0+gamma2))*(fVCSAs->A1n(x,Q2) + TMath::Sqrt(gamma2)*fVCSAs->A2n(x,Q2));
   return g1;
}
//______________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromVCSAs::g1d(   Double_t x, Double_t Q2)
{
   return 0.0;
}
//______________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromVCSAs::g1He3( Double_t x, Double_t Q2)
{
   return 0.0;
}
//______________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromVCSAs::g2p(   Double_t x, Double_t Q2)
{
   if(!fVCSAs) return 0.0;
   double M = (M_p/GeV);
   double gamma2 = (4.0*M*M*x*x)/Q2;
   double F1 =  fSFs->F1p(x,Q2);
   double g2 = (F1/(1.0+gamma2))*(fVCSAs->A2p(x,Q2)/TMath::Sqrt(gamma2) - fVCSAs->A1p(x,Q2));
   return g2;
}
//______________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromVCSAs::g2n(   Double_t x, Double_t Q2)
{
   return 0.0;
}
//______________________________________________________________________________
Double_t InSANEPolarizedStructureFunctionsFromVCSAs::g2d(   Double_t x, Double_t Q2)
{
   return 0.0;
}
Double_t InSANEPolarizedStructureFunctionsFromVCSAs::g2He3( Double_t x, Double_t Q2)
{
   return 0.0;
}

