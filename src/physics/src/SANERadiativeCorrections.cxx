#include "SANERadiativeCorrections.h"
#include "InSANERadiator.h"
#include "InSANEFunctionManager.h"

//______________________________________________________________________________
SANE_RCs_Model0::SANE_RCs_Model0(const char * n, const char * t) 
   : InSANERadiativeCorrections1D(n,t)
{
}
//______________________________________________________________________________
SANE_RCs_Model0::~SANE_RCs_Model0() {
} 
//______________________________________________________________________________
Int_t SANE_RCs_Model0::InitCrossSections() { 
   // This changes the SFs used SANE_RCs_Model1 except
   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   //fman->CreateSFs(11);   // NMC+F1F209
   //fman->CreatePolSFs(0); // DSSV
   //fman->CreateFFs(4);    //  Kelly FF

   TVector3 P_target(0,0,1);
   P_target.SetMagThetaPhi(1.0,fThetaTarget,0.0);

   fPolarizedPhaseSpace = new InSANEPhaseSpace();
   fPhaseSpace          = new InSANEPhaseSpace();

   auto * varEnergy = new InSANEPhaseSpaceVariable("Energy","E#prime"); 
   varEnergy->SetMinimum(0.001);         //GeV
   varEnergy->SetMaximum(6.0); //GeV
   varEnergy->SetVariableUnits("GeV");        //GeV
   fPolarizedPhaseSpace->AddVariable(varEnergy);
   fPhaseSpace->AddVariable(varEnergy);

   auto *   varTheta = new InSANEPhaseSpaceVariable("theta","#theta");
   varTheta->SetMinimum(2.0*TMath::Pi()/180.0); //
   varTheta->SetMaximum(180.0*TMath::Pi()/180.0); //
   varTheta->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varTheta);
   fPhaseSpace->AddVariable(varTheta);

   auto *   varPhi = new InSANEPhaseSpaceVariable("phi","#phi");
   varPhi->SetMinimum(-180.0*TMath::Pi()/180.0); //
   varPhi->SetMaximum(180.0*TMath::Pi()/180.0); //
   varPhi->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varPhi);
   fPhaseSpace->AddVariable(varPhi);

   auto *   varHelicity = new InSANEDiscretePhaseSpaceVariable("helicity","#lambda");
   varHelicity->SetNumberOfValues(3); // ROOT string latex
   fPolarizedPhaseSpace->AddVariable(varHelicity);

   fPolarizedPhaseSpace->Refresh();
   //varEnergy->Print();
   //varTheta->Print();
   //varPhi->Print();
   //varHelicity->Print();

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();

   //-----------------------
   // Born Cross sections
   // born unpolarized 
   auto * fDiffXSec00 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec00->SetBeamEnergy(fBeamEnergy);
   //fDiffXSec00->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec00->SetPhaseSpace(fPhaseSpace);
   fDiffXSec00->GetPOLRAD()->SetTargetPolarization(0.0);
   fDiffXSec00->GetPOLRAD()->SetPolarizationVectors(P_target,0.0);
   fDiffXSec00->Refresh();
   fBorn0 = fDiffXSec00;
   // born polarized + 
   auto * fDiffXSec10 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec10->SetBeamEnergy(fBeamEnergy);
   fDiffXSec10->SetPhaseSpace(fPhaseSpace);
   //fDiffXSec10->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec10->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec10->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec10->Refresh();
   fBorn1 = fDiffXSec10;
   // born polarized - 
   auto * fDiffXSec20 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec20->SetBeamEnergy(fBeamEnergy);
   fDiffXSec20->SetPhaseSpace(fPhaseSpace);
   //fDiffXSec20->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec20->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec20->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec20->Refresh();
   fBorn2 = fDiffXSec20;

   //------------------------
   // Elastic Radiatve Tail 
   // internal and external radiated elastic unpolarized 
   auto * fDiffXSec02 = new  InSANEElasticRadiativeTail();
   fDiffXSec02->SetBeamEnergy(fBeamEnergy);
   fDiffXSec02->SetPolarizations(0.0,0.0);
   fDiffXSec02->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec02->SetPhaseSpace(fPhaseSpace);
   fDiffXSec02->Refresh();
   fERT0_rt = fDiffXSec02;
   // internal and external radiated elastic polarized + 
   auto * fDiffXSec12 = new  InSANEElasticRadiativeTail();
   fDiffXSec12->SetBeamEnergy(fBeamEnergy);
   fDiffXSec12->SetPolarizations(1.0,1.0);
   fDiffXSec12->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec12->GetBornXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec12->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec12->SetPhaseSpace(fPhaseSpace);
   fDiffXSec12->Refresh();
   fERT1_rt = fDiffXSec12;
   // internal and external radiated polarized -
   auto * fDiffXSec22 = new InSANEElasticRadiativeTail();
   fDiffXSec22->SetBeamEnergy(fBeamEnergy);
   fDiffXSec22->SetPolarizations(1.0,1.0);
   fDiffXSec22->GetBornXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec22->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec22->SetPhaseSpace(fPhaseSpace);
   fDiffXSec22->Refresh();
   fERT2_rt = fDiffXSec22;


   //-------------------------
   //// IRT unpolarized 
   auto * fDiffXSec01 = new  InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>();
   fDiffXSec01->SetBeamEnergy(fBeamEnergy);
   fDiffXSec01->SetExternalOnly(true);
   fDiffXSec01->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec01->InSANEPOLRADBornDiffXSec::SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec01->InSANEPOLRADBornDiffXSec::SetPolarizations(0.0,0.0);
   fDiffXSec01->SetPolarizations(0.0,0.0);
   fDiffXSec01->SetPhaseSpace(fPhaseSpace);
   fDiffXSec01->Refresh();
   fInelasticXSec0_rt = fDiffXSec01;

   // internal radiated polarized + 
   auto * fDiffXSec11 = new InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>();
   fDiffXSec11->SetBeamEnergy(fBeamEnergy);
   //fDiffXSec11->InSANEPOLRADBornDiffXSec::GetPOLRAD()->SetIRTMethod(InSANEPOLRAD::kFull);
   fDiffXSec11->SetExternalOnly(true);
   fDiffXSec11->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec11->InSANEPOLRADBornDiffXSec::SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec11->SetPolarizations(1.0,1.0);
   fDiffXSec11->InSANEPOLRADBornDiffXSec::SetPolarizations(1.0,1.0);
   fDiffXSec11->SetTargetPolarization(P_target);
   fDiffXSec11->InSANEPOLRADBornDiffXSec::SetTargetPolarization(P_target);
   fDiffXSec11->SetPhaseSpace(fPhaseSpace);
   fDiffXSec11->Refresh();
   fInelasticXSec1_rt = fDiffXSec11;

   // internal radiated polarized -
   auto * fDiffXSec21 = new  InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>();
   fDiffXSec21->SetBeamEnergy(fBeamEnergy);
   //fDiffXSec21->InSANEPOLRADBornDiffXSec::GetPOLRAD()->SetIRTMethod(InSANEPOLRAD::kFull);
   fDiffXSec21->SetExternalOnly(true);
   fDiffXSec21->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec21->InSANEPOLRADBornDiffXSec::SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec21->SetPolarizations(1.0,-1.0);
   fDiffXSec21->InSANEPOLRADBornDiffXSec::SetPolarizations(1.0,-1.0);
   fDiffXSec21->SetTargetPolarization(P_target);
   fDiffXSec21->InSANEPOLRADBornDiffXSec::SetTargetPolarization(P_target);
   fDiffXSec21->SetPhaseSpace(fPhaseSpace);
   fDiffXSec21->Refresh();
   fInelasticXSec2_rt = fDiffXSec21;

   fDiffXSec01->SetRadiationLength(0.0275,0.0236);
   fDiffXSec11->SetRadiationLength(0.0275,0.0236);
   fDiffXSec21->SetRadiationLength(0.0275,0.0236);

   //PrintConfiguration();

   return 0;
}
//______________________________________________________________________________



SANE_RCs_Model1::SANE_RCs_Model1(const char * n, const char * t) 
   : InSANERadiativeCorrections1D(n,t)
{
}
//______________________________________________________________________________
SANE_RCs_Model1::~SANE_RCs_Model1() {
} 
//______________________________________________________________________________
Int_t SANE_RCs_Model1::InitCrossSections() { 

   // This changes the SFs used SANE_RCs_Model1 except
   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   fman->CreateSFs(6);
   fman->CreatePolSFs(6);
   fman->CreateFFs(0);    //Dipole Kelly FF

   TVector3 P_target(0,0,1);
   P_target.SetMagThetaPhi(1.0,fThetaTarget,0.0);

   fPolarizedPhaseSpace = new InSANEPhaseSpace();
   fPhaseSpace          = new InSANEPhaseSpace();

   auto * varEnergy = new InSANEPhaseSpaceVariable("Energy","E#prime"); 
   varEnergy->SetMinimum(0.001);         //GeV
   varEnergy->SetMaximum(6.0); //GeV
   varEnergy->SetVariableUnits("GeV");        //GeV
   fPolarizedPhaseSpace->AddVariable(varEnergy);
   fPhaseSpace->AddVariable(varEnergy);

   auto *   varTheta = new InSANEPhaseSpaceVariable("theta","#theta");
   varTheta->SetMinimum(2.0*TMath::Pi()/180.0); //
   varTheta->SetMaximum(180.0*TMath::Pi()/180.0); //
   varTheta->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varTheta);
   fPhaseSpace->AddVariable(varTheta);

   auto *   varPhi = new InSANEPhaseSpaceVariable("phi","#phi");
   varPhi->SetMinimum(-180.0*TMath::Pi()/180.0); //
   varPhi->SetMaximum(180.0*TMath::Pi()/180.0); //
   varPhi->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varPhi);
   fPhaseSpace->AddVariable(varPhi);

   auto *   varHelicity = new InSANEDiscretePhaseSpaceVariable("helicity","#lambda");
   varHelicity->SetNumberOfValues(3); // ROOT string latex
   fPolarizedPhaseSpace->AddVariable(varHelicity);

   fPolarizedPhaseSpace->Refresh();
   //varEnergy->Print();
   //varTheta->Print();
   //varPhi->Print();
   //varHelicity->Print();

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();

   //-----------------------
   // Born Cross sections
   // born unpolarized 
   auto * fDiffXSec00 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec00->SetBeamEnergy(fBeamEnergy);
   //fDiffXSec00->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec00->SetPhaseSpace(fPhaseSpace);
   fDiffXSec00->GetPOLRAD()->SetTargetPolarization(0.0);
   fDiffXSec00->GetPOLRAD()->SetPolarizationVectors(P_target,0.0);
   fDiffXSec00->Refresh();
   fBorn0 = fDiffXSec00;
   // born polarized + 
   auto * fDiffXSec10 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec10->SetBeamEnergy(fBeamEnergy);
   fDiffXSec10->SetPhaseSpace(fPhaseSpace);
   //fDiffXSec10->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec10->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec10->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec10->Refresh();
   fBorn1 = fDiffXSec10;
   // born polarized - 
   auto * fDiffXSec20 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec20->SetBeamEnergy(fBeamEnergy);
   fDiffXSec20->SetPhaseSpace(fPhaseSpace);
   //fDiffXSec20->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec20->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec20->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec20->Refresh();
   fBorn2 = fDiffXSec20;

   //------------------------
   // Elastic Radiatve Tail 
   // internal and external radiated elastic unpolarized 
   auto * fDiffXSec02 = new  InSANEElasticRadiativeTail();
   fDiffXSec02->SetBeamEnergy(fBeamEnergy);
   fDiffXSec02->SetPolarizations(0.0,0.0);
   fDiffXSec02->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec02->SetPhaseSpace(fPhaseSpace);
   fDiffXSec02->Refresh();
   fERT0_rt = fDiffXSec02;
   // internal and external radiated elastic polarized + 
   auto * fDiffXSec12 = new  InSANEElasticRadiativeTail();
   fDiffXSec12->SetBeamEnergy(fBeamEnergy);
   fDiffXSec12->SetPolarizations(1.0,1.0);
   fDiffXSec12->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec12->GetBornXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec12->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec12->SetPhaseSpace(fPhaseSpace);
   fDiffXSec12->Refresh();
   fERT1_rt = fDiffXSec12;
   // internal and external radiated polarized -
   auto * fDiffXSec22 = new InSANEElasticRadiativeTail();
   fDiffXSec22->SetBeamEnergy(fBeamEnergy);
   fDiffXSec22->SetPolarizations(1.0,1.0);
   fDiffXSec22->GetBornXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec22->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec22->SetPhaseSpace(fPhaseSpace);
   fDiffXSec22->Refresh();
   fERT2_rt = fDiffXSec22;


   //-------------------------
   //// IRT unpolarized 
   auto * fDiffXSec01 = new  InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>();
   fDiffXSec01->SetBeamEnergy(fBeamEnergy);
   fDiffXSec01->SetExternalOnly(true);
   fDiffXSec01->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec01->InSANEPOLRADBornDiffXSec::SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec01->InSANEPOLRADBornDiffXSec::SetPolarizations(0.0,0.0);
   fDiffXSec01->SetPolarizations(0.0,0.0);
   fDiffXSec01->SetPhaseSpace(fPhaseSpace);
   fDiffXSec01->Refresh();
   fInelasticXSec0_rt = fDiffXSec01;

   // internal radiated polarized + 
   auto * fDiffXSec11 = new InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>();
   fDiffXSec11->SetBeamEnergy(fBeamEnergy);
   fDiffXSec11->SetExternalOnly(true);
   fDiffXSec11->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec11->InSANEPOLRADBornDiffXSec::SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec11->SetPolarizations(1.0,1.0);
   fDiffXSec11->InSANEPOLRADBornDiffXSec::SetPolarizations(1.0,1.0);
   fDiffXSec11->SetTargetPolarization(P_target);
   fDiffXSec11->InSANEPOLRADBornDiffXSec::SetTargetPolarization(P_target);
   fDiffXSec11->SetPhaseSpace(fPhaseSpace);
   fDiffXSec11->Refresh();
   fInelasticXSec1_rt = fDiffXSec11;

   // internal radiated polarized -
   auto * fDiffXSec21 = new  InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>();
   fDiffXSec21->SetBeamEnergy(fBeamEnergy);
   fDiffXSec21->SetExternalOnly(true);
   fDiffXSec21->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec21->InSANEPOLRADBornDiffXSec::SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec21->SetPolarizations(1.0,-1.0);
   fDiffXSec21->InSANEPOLRADBornDiffXSec::SetPolarizations(1.0,-1.0);
   fDiffXSec21->SetTargetPolarization(P_target);
   fDiffXSec21->InSANEPOLRADBornDiffXSec::SetTargetPolarization(P_target);
   fDiffXSec21->SetPhaseSpace(fPhaseSpace);
   fDiffXSec21->Refresh();
   fInelasticXSec2_rt = fDiffXSec21;

   fDiffXSec01->SetRadiationLength(0.0275,0.0236);
   fDiffXSec11->SetRadiationLength(0.0275,0.0236);
   fDiffXSec21->SetRadiationLength(0.0275,0.0236);

   //PrintConfiguration();
   return 0;
}
//______________________________________________________________________________

