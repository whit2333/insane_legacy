#include "JAM_PPDFs.h"
#include "InSANEFortranWrappers.h"

namespace insane {
  namespace physics {

    JAM_PPDFs::JAM_PPDFs()
    {
      char * lib_string  = "JAM15        ";
      char * dist_string = "PPDF         ";
      char * path_string = "             ";
      int    ipos = 0;
      // lib (character*10): library (JAM15,JAM16,etc.)
      // dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
      // ipos (integer): posterior number (0 to 199) from MC analysis
      grid_init_( path_string, lib_string, dist_string, &ipos );
      Reset();
    }
    //______________________________________________________________________________

    JAM_PPDFs::~JAM_PPDFs()
    { }
    //______________________________________________________________________________
    
    std::array<double,NPartons> JAM_PPDFs::Calculate(double x, double Q2) const
    {
      auto Xbjorken      = x;
      auto Qsquared      = Q2;
      double  f = 0.0;
      char *  flav;

      auto values = fValues;

      flav =  "up"; double u_plus = 0.0;
      jam_xf_(&u_plus, &Xbjorken, &Qsquared, flav );

      flav =  "dp"; double d_plus = 0.0;
      jam_xf_(&d_plus,&Xbjorken, &Qsquared, flav );

      flav =  "sp"; double s_plus = 0.0;
      jam_xf_(&s_plus, &Xbjorken, &Qsquared, flav );

      flav =  "u"; double u      = 0.0;
      jam_xf_(&u, &Xbjorken, &Qsquared, flav );

      flav =  "d"; double d      = 0.0;
      jam_xf_(&d, &Xbjorken, &Qsquared, flav );

      flav =  "s"; double s      = 0.0;
      jam_xf_(&s, &Xbjorken, &Qsquared, flav );

      flav =  "gl"; double gl     = 0.0;
      jam_xf_(&gl, &Xbjorken, &Qsquared, flav );

      values[PartonFlavor::kUP]          = u/x; //Delta up
      values[PartonFlavor::kDOWN]        = d/x; //Delta down
      values[PartonFlavor::kSTRANGE]     = s/x;      //Delta s
      values[PartonFlavor::kGLUON]       = gl/x;       //Delta g
      values[PartonFlavor::kANTIUP]      = (u_plus-u)/x;     //Delta ubar
      values[PartonFlavor::kANTIDOWN]    = (d_plus-d)/x;     //Delta dbar
      values[PartonFlavor::kANTISTRANGE] = (s_plus-s)/x;     //Delta sbar

      return values;
    }
    //______________________________________________________________________________

    std::array<double,NPartons> JAM_PPDFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }


  }
}
