#include "JAM_T3DFs.h"
#include "InSANEFortranWrappers.h"
//#include "TLockGuard.h"

namespace insane {
  namespace physics {

    TMutex* JAM_T3DFs::fgInit_mutex = new TMutex();

    //std::mutex* fgInit_mutex = new std::mutex();
    JAM_T3DFs::JAM_T3DFs()
    {
      char * lib_string  = "JAM15        ";
      char * dist_string = "PPDF         ";
      char * path_string = "             ";
      int    ipos = 0;
      // lib (character*10): library (JAM15,JAM16,etc.)
      // dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
      // ipos (integer): posterior number (0 to 199) from MC analysis
      
      // simulate a long page fetch
      //std::this_thread::sleep_for(std::chrono::seconds(2));

      TLockGuard guard(fgInit_mutex);
      grid_init_( path_string, lib_string, dist_string, &ipos );
      Reset();
    }
    //______________________________________________________________________________

    JAM_T3DFs::~JAM_T3DFs()
    { }
    //______________________________________________________________________________

    std::array<double,NPartons> JAM_T3DFs::Calculate(double x, double Q2) const
    {
      auto Xbjorken      = x;
      auto Qsquared      = Q2;
      double  t3_val = 0.0;
      char*  flav;

      auto values = fValues;

      TLockGuard guard(fgInit_mutex);
      flav =  "u3";
      jam_xf_(&t3_val, &Xbjorken, &Qsquared, flav );
      values[PartonFlavor::kUP] = t3_val;

      flav =  "d3";
      jam_xf_(&t3_val, &Xbjorken, &Qsquared, flav );
      values[PartonFlavor::kDOWN] = t3_val;

      //fxDd_Twist3 = t3d;
      // do calculations
      return values;
    }
    //______________________________________________________________________________

    std::array<double,NPartons> JAM_T3DFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }


  }
}
