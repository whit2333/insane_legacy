#include "InSANEStructureFunctionsFromPDFs.h"

InSANEStructureFunctionsFromPDFs::InSANEStructureFunctionsFromPDFs() 
   : fUsesR(true), fTargetMassCorrections(true), fNintegrate(50)
{ }
//______________________________________________________________________________

InSANEStructureFunctionsFromPDFs::InSANEStructureFunctionsFromPDFs(
    InSANEPartonDistributionFunctions * pdfs) : 
  fUsesR(true), fTargetMassCorrections(true), fNintegrate(50), fUnpolarizedPDFs(pdfs)
{
   SetLabel(Form("%s",pdfs->GetLabel()));
   SetNameTitle(Form("SFs from %s",pdfs->GetName()),Form("%s",pdfs->GetName()));
}
//______________________________________________________________________________

InSANEStructureFunctionsFromPDFs::~InSANEStructureFunctionsFromPDFs(){
}
//______________________________________________________________________________
void InSANEStructureFunctionsFromPDFs::SetUnpolarizedPDFs(InSANEPartonDistributionFunctions * pdfs) {
   fUnpolarizedPDFs = pdfs;
   SetLabel(Form("%s",pdfs->GetLabel()));
   SetNameTitle(Form("SFs from %s",pdfs->GetName()),Form("%s",pdfs->GetName()));
}
//______________________________________________________________________________
InSANEPartonDistributionFunctions *  InSANEStructureFunctionsFromPDFs::GetUnpolarizedPDFs() { 
   return(fUnpolarizedPDFs);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::h2_TMC(Double_t xi, Double_t Q2){
   // J. Phys. G 35, 053101 (2008)
   Int_t N         = fNintegrate;
   Double_t du     = (1.0 - xi) / ((Double_t)N);
   Double_t result = 0.0;
   for (int i = 0; i < N; i++) {
      double u =  xi + du*double(i);
      result += (F2pNoTMC(u,Q2)*du/(u*u));
   }
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::g2_TMC(Double_t xi, Double_t Q2){
   // J. Phys. G 35, 053101 (2008)
   Int_t N         = fNintegrate;
   Double_t du     = (1.0 - xi) / ((Double_t)N);
   Double_t result = 0.0;
   for (int i = 0; i < N; i++) {
      double u =  xi + du*double(i);
      result += ((u-xi)*F2pNoTMC(u,Q2)*du/(u*u));
   }
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::R(Double_t x, Double_t Qsq)
{
   if(fUsesR){
      return InSANE::Kine::R1998(x,Qsq);
   }
   //if(fTargetMassCorrections) {
   //   return( F2p(x,Qsq)/(2.0*x*F1p(x,Qsq))*(1.0+(4.0*(M_p/GeV)*(M_p/GeV)*x*x)/(Qsq))-1.0 );
   //}
   /*else*/return 0.0;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F2p(Double_t x, Double_t Qsq) {
   // including target mass effects
   // references: 
   //    Phys Rev D 84, 074008 (2011), Eq 9a  
   //    J. Phys. G 35, 053101 (2008)
   Double_t result = 0.0;
   if( fTargetMassCorrections ) {
      // leading order in 1/Q2 
      Double_t M         = M_p/GeV; 
      Double_t rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Qsq); 
      Double_t xi        = 2.*x/(1. + rho); 
      Double_t F2_0      = F2pNoTMC(xi,Qsq); 
      Double_t h2        = h2_TMC(x,Qsq);
      Double_t g2        = g2_TMC(x,Qsq);
      result    = (F2_0*x*x)/(xi*xi*rho*rho*rho) + 6.0*M*M*x*x*x*h2/(Qsq*rho*rho*rho*rho) + 12.0*M*M*M*M*x*x*x*x*g2/(Qsq*Qsq*rho*rho*rho*rho*rho);
      return(result);

      // Approximate form   Phys Rev D 84, 074008 (2011), Eq 9a  
      //Double_t T1        = TMath::Power(1.+rho,2.)/( 4.*TMath::Power(rho,3.) ); 
      //Double_t T2        = 1. + 3.*(rho*rho - 1.)*TMath::Power(1.-xi,2.)/( rho*(1.+rho) );
      //result             = T1*T2*f2_no_tmc; 
      //return(result);
   }
   if(!fTargetMassCorrections) {
      result = F2pNoTMC(x,Qsq); 
   }
   if(fUsesR) {
      //result *= (1.0+4.0*TMath::Power(x*M_p/GeV,2.0)/Qsq)/(R(x,Qsq)+1.0);
   }
   return result;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1p( Double_t x, Double_t Qsq ){
   Double_t result = 0.0;
   if( fTargetMassCorrections ) {
      // leading order in 1/Q2 
      Double_t M         = M_p/GeV; 
      Double_t rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Qsq); 
      Double_t xi        = 2.*x/(1. + rho); 
      Double_t F1_0      = F1pNoTMC(xi,Qsq); 
      Double_t h2        = h2_TMC(x,Qsq);
      Double_t g2        = g2_TMC(x,Qsq);
      result    = (F1_0*x)/(xi*rho) + M*M*x*x*h2/(Qsq*rho*rho) + 2.0*M*M*M*M*x*x*x*g2/(Qsq*Qsq*rho*rho*rho);
      return(result);
   }
   result = F1pNoTMC(x, Qsq);
   if(fUsesR) {
      result *= (1.0+4.0*TMath::Power(x*M_p/GeV,2.0)/Qsq)/(R(x,Qsq)+1.0);
      return result;
   }
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F2pNoTMC(Double_t x, Double_t Qsq ) {
   Double_t result = 0.0;
   fUnpolarizedPDFs->GetPDFs(x, Qsq);
   result += (4.0 / 9.0) * fUnpolarizedPDFs->u();
   result += (1.0 / 9.0) * fUnpolarizedPDFs->d();
   result += (1.0 / 9.0) * fUnpolarizedPDFs->s();
   result += (4.0 / 9.0) * fUnpolarizedPDFs->ubar();
   result += (1.0 / 9.0) * fUnpolarizedPDFs->dbar();
   result += (1.0 / 9.0) * fUnpolarizedPDFs->sbar();
   return(result*x);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1pNoTMC( Double_t x, Double_t Qsq ){
   Double_t result = F2pNoTMC(x, Qsq) / (2.0*x);
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F2n(Double_t x, Double_t Qsq) {
   // including target mass effects
   // reference: Phys Rev D 84, 074008 (2011), Eq 9a  
   if(!fTargetMassCorrections) {
      return F2nNoTMC(x,Qsq);
   }
   Double_t M         = M_p/GeV;             // nucleon mass => proton mass 
   Double_t rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Qsq); 
   Double_t xi        = 2.*x/(1. + rho); 
   Double_t f2_no_tmc = F2nNoTMC(xi,Qsq); 
   Double_t T1        = TMath::Power(1.+rho,2.)/( 4.*TMath::Power(rho,3.) ); 
   Double_t T2        = 1. + 3.*(rho*rho - 1.)*TMath::Power(1.-xi,2.)/( rho*(1.+rho) );
   Double_t result    = T1*T2*f2_no_tmc; 
   return result;
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F2nNoTMC(Double_t x, Double_t Qsq) {
   // for the neutron we switch the charges on the up and down quarks in the sum.
   Double_t result = 0.0;
   fUnpolarizedPDFs->GetPDFs(x, Qsq);
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->u();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->d();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->s();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->ubar();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dbar();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sbar();
   return(result*x);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1n(Double_t x,Double_t Qsq){
   Double_t result = F2n(x, Qsq) / (2.0*x);
   if(!fTargetMassCorrections) {
      return result;
   }
   result *= (1.0+4.0*TMath::Power(x*M_p/GeV,2.0)/Qsq)/(R(x,Qsq)+1.0);
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1nNoTMC(Double_t x,Double_t Qsq){
   Double_t result = F2nNoTMC(x, Qsq) / (2.0*x);
   result *= (1.0+4.0*TMath::Power(x*M_p/GeV,2.0)/Qsq)/(R(x,Qsq)+1.0);
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1d(Double_t x,Double_t Qsq){
   // F1d PER NUCLEON
   Double_t wD     = 0.058;         // D-wave state probability 
   Double_t result = 0.5*(1.-1.5*wD)*( F1n(x,Qsq) + F1p(x,Qsq) ); 
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F2d(Double_t x,Double_t Qsq){
   // F2d PER NUCLEON 
   Double_t wD     = 0.058;         // D-wave state probability 
   Double_t result = 0.5*(1.-1.5*wD)*( F2n(x,Qsq) + F2p(x,Qsq) ); 
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1He3(Double_t x,Double_t Qsq){
   // F1He3 PER NUCLEUS
   Double_t result = ( F1n(x,Qsq) + 2.*F1p(x,Qsq) ); 
   return(result);
} 
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F2He3(Double_t x,Double_t Qsq){
   // F2He3 PER NUCLEUS
   Double_t result = ( F2n(x,Qsq) + 2.*F2p(x,Qsq) ); 
   return(result);
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1p_Error(   Double_t x, Double_t Q2){
   return 0.0;
}
Double_t InSANEStructureFunctionsFromPDFs::F2p_Error(   Double_t x, Double_t Q2){
   // including target mass effects
   // reference: Phys Rev D 84, 074008 (2011), Eq 9a  
   Double_t M         = M_p/GeV; 
   Double_t rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Q2); 
   Double_t xi        = 2.*x/(1. + rho); 
   Double_t f2_no_tmc = F2pNoTMC_Error(xi,Q2); 
   Double_t T1        = TMath::Power(1.+rho,2.)/( 4.*TMath::Power(rho,3.) ); 
   Double_t T2        = 1. + 3.*(rho*rho - 1.)*TMath::Power(1.-xi,2.)/( rho*(1.+rho) );
   Double_t result    = T1*T2*f2_no_tmc; 
   return result;
}
Double_t InSANEStructureFunctionsFromPDFs::F1n_Error(   Double_t x, Double_t Q2){return 0.0;}
Double_t InSANEStructureFunctionsFromPDFs::F2n_Error(   Double_t x, Double_t Q2){
   // including target mass effects
   // reference: Phys Rev D 84, 074008 (2011), Eq 9a  
   Double_t M         = M_p/GeV;             // nucleon mass => proton mass 
   Double_t rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Q2); 
   Double_t xi        = 2.*x/(1. + rho); 
   Double_t f2_no_tmc = F2nNoTMC_Error(xi,Q2); 
   Double_t T1        = TMath::Power(1.+rho,2.)/( 4.*TMath::Power(rho,3.) ); 
   Double_t T2        = 1. + 3.*(rho*rho - 1.)*TMath::Power(1.-xi,2.)/( rho*(1.+rho) );
   Double_t result    = T1*T2*f2_no_tmc; 
   return result;
}
Double_t InSANEStructureFunctionsFromPDFs::F1d_Error(   Double_t x, Double_t Q2){return 0.0;}
Double_t InSANEStructureFunctionsFromPDFs::F2d_Error(   Double_t x, Double_t Q2){return 0.0;}
Double_t InSANEStructureFunctionsFromPDFs::F1He3_Error( Double_t x, Double_t Q2){return 0.0;}
Double_t InSANEStructureFunctionsFromPDFs::F2He3_Error( Double_t x, Double_t Q2){return 0.0;}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1pNoTMC_Error(   Double_t x, Double_t Q2){
   Double_t result = 0.0;
   fUnpolarizedPDFs->GetPDFErrors(x, Q2);
   result += TMath::Power((4.0 / 9.0) * fUnpolarizedPDFs->uError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->dError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->sError(),2.0);
   result += TMath::Power((4.0 / 9.0) * fUnpolarizedPDFs->ubarError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->dbarError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->sbarError(),2.0);
   return( TMath::Sqrt(result)/2.0 );
}
Double_t InSANEStructureFunctionsFromPDFs::F2pNoTMC_Error(   Double_t x, Double_t Q2){
   Double_t result = 0.0;
   fUnpolarizedPDFs->GetPDFErrors(x, Q2);
   result += TMath::Power((4.0 / 9.0) * fUnpolarizedPDFs->uError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->dError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->sError(),2.0);
   result += TMath::Power((4.0 / 9.0) * fUnpolarizedPDFs->ubarError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->dbarError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->sbarError(),2.0);
   return( TMath::Sqrt(result)*x );
}
Double_t InSANEStructureFunctionsFromPDFs::F1nNoTMC_Error(   Double_t x, Double_t Q2){
   // for the neutron we switch the charges on the up and down quarks in the sum.
   Double_t result = 0.0;
   fUnpolarizedPDFs->GetPDFErrors(x, Q2);
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->uError();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->ubarError();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dbarError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sbarError();
   return(result/2.0);
}
Double_t InSANEStructureFunctionsFromPDFs::F2nNoTMC_Error(   Double_t x, Double_t Q2){
   // for the neutron we switch the charges on the up and down quarks in the sum.
   Double_t result = 0.0;
   fUnpolarizedPDFs->GetPDFErrors(x, Q2);
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->uError();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->ubarError();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dbarError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sbarError();
   return(result*x);
}
Double_t InSANEStructureFunctionsFromPDFs::F1dNoTMC_Error(   Double_t x, Double_t Q2){return 0.0;}
Double_t InSANEStructureFunctionsFromPDFs::F2dNoTMC_Error(   Double_t x, Double_t Q2){return 0.0;}
Double_t InSANEStructureFunctionsFromPDFs::F1He3NoTMC_Error( Double_t x, Double_t Q2){return 0.0;}
Double_t InSANEStructureFunctionsFromPDFs::F2He3NoTMC_Error( Double_t x, Double_t Q2){return 0.0;}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F2Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A){
   Double_t res = (A/2.0)*2.0*F2d(x,Qsq)*EMC_Effect(&x,&A);
   return res; 
}
//______________________________________________________________________________
Double_t InSANEStructureFunctionsFromPDFs::F1Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A){
   Double_t res = (A/2.0)*2.0*F1d(x,Qsq)*EMC_Effect(&x,&A);
   return res; 
}
//______________________________________________________________________________

