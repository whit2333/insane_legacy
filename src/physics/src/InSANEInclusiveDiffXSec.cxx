#include "InSANEInclusiveDiffXSec.h"
#include "TMath.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"
//#include "InSANEStructureFunctions.h"
#include "TCanvas.h"
#include "TGraph2D.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TVirtualPad.h"
#include "TAxis.h"

ClassImp(InSANEInclusiveDiffXSec)

InSANEInclusiveDiffXSec::InSANEInclusiveDiffXSec() : fPolType(0)
{
   fID         = 100000000;
   SetIncludeJacobian(false);
}
//______________________________________________________________________________

InSANEInclusiveDiffXSec::InSANEInclusiveDiffXSec(const InSANEInclusiveDiffXSec& old) : 
   InSANEDiffXSec(old)
{ 
   (*this) = old; 
}
//______________________________________________________________________________

InSANEInclusiveDiffXSec::~InSANEInclusiveDiffXSec()
{ 
}
//______________________________________________________________________________

InSANEInclusiveDiffXSec& InSANEInclusiveDiffXSec::operator=(const InSANEInclusiveDiffXSec& old)
{
   if (this != &old) {  // make sure not same object
      InSANEDiffXSec::operator=(old);
      fPolType = old.fPolType;
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

InSANEInclusiveDiffXSec*  InSANEInclusiveDiffXSec::Clone(const char * newname) const 
{
   std::cout << "InSANEInclusiveDiffXSec::Clone()\n";
   auto * copy = new InSANEInclusiveDiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

InSANEInclusiveDiffXSec*  InSANEInclusiveDiffXSec::Clone() const 
{ 
   return( Clone("") );
}
//______________________________________________________________________________

double InSANEInclusiveDiffXSec::DoEval(const double * x) const 
{
   return (double)EvaluateXSec((Double_t*) x);
}
//______________________________________________________________________________

Double_t  InSANEInclusiveDiffXSec::EvaluateXSec(const Double_t * x) const
{

   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   // for inelastic scattering 
   Double_t M    = M_p/GeV;            // in GeV 
   Double_t Ep   = x[0];               // scattered electron energy in GeV 
   Double_t th   = x[1];               // electron scattering angle in radians 
   Double_t Nu   = fBeamEnergy - Ep;
   Double_t SIN  = TMath::Sin(th/2.);
   Double_t SIN2 = SIN*SIN;
   Double_t COS2 = 1. - SIN2;
   Double_t TAN2 = SIN2/COS2;
   Double_t Q2   = InSANE::Kine::Qsquared(fBeamEnergy,Ep,th);
   Double_t xBj  = InSANE::Kine::xBjorken_EEprimeTheta(fBeamEnergy,Ep,th);;

   // Calculate structure functions 
   Double_t F1,F2;

   //InSANENucleus::NucleusType Target = fTargetNucleus.GetType();
   //fTargetNucleus.Dump();

   if( fTargetNucleus ==  InSANENucleus::Proton() ){
      F1 = fStructureFunctions->F1p(xBj,Q2);
      F2 = fStructureFunctions->F2p(xBj,Q2);
   } else if(fTargetNucleus ==  InSANENucleus::Neutron() ) {
      F1 = fStructureFunctions->F1n(xBj,Q2);
      F2 = fStructureFunctions->F2n(xBj,Q2);
   } else {
      Error("EvaluateXSec","Invalid target (or not functional yet). ");
      std::cout << " Use InSANECompositeDiffXSec Instead ... using proton for now " << std::endl;
      F1 = fStructureFunctions->F1p(xBj,Q2);
      F2 = fStructureFunctions->F2p(xBj,Q2);
   }
   Double_t W1 = (1./M)*F1;
   Double_t W2 = (1./Nu)*F2;
   // compute the Mott cross section (units = nb): 
   Double_t alpha    = fine_structure_const;
   Double_t num      = alpha*alpha*COS2;
   Double_t den      = 4.*fBeamEnergy*fBeamEnergy*SIN2*SIN2;
   Double_t MottXS   = num/den;
   // compute the full cross section (units = nb/GeV/sr) 
   Double_t fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*hbarc2_gev_nb;

   if ( IncludeJacobian() ) return fullXsec*TMath::Sin(th);
   return fullXsec;
}
//______________________________________________________________________________

Double_t  InSANEInclusiveDiffXSec::Density(Int_t ndim, Double_t * x)
{
   //  std::cout << "Density  " << x[0] << " " << x[1] << " " << x[2] << " \n";
   //Double_t y[3]={x[0]*fScaleFactors[0]+fOffsets[0],x[1]*fScaleFactors[1]+fOffsets[1],x[2]*fScaleFactors[2]+fOffsets[2]};
   return(EvaluateXSec(GetDependentVariables(GetUnnormalizedVariables(x))));
}
//______________________________________________________________________________

void InSANEInclusiveDiffXSec::DefineEvent(Double_t * vars) 
{

   Int_t totvars = 0;
   for (int i = 0; i < fParticles.GetEntries(); i++) {
      /// \todo fix this hard coding of 3 variables per event.
      /// here we are assuming the order E,theta,phi,then others
      /// \todo figure out how to handle vertex.
      InSANE::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
      totvars += GetNParticleVars(i);
   }

}
//______________________________________________________________________________

void InSANEInclusiveDiffXSec::InitializePhaseSpaceVariables()
{
   //std::cout << " o InSANEInclusiveDiffXSec::InitializePhaseSpaceVariables() \n";

   InSANEPhaseSpace * ps = GetPhaseSpace();
   if (ps) delete ps;
   ps = nullptr;
   if (!ps) {
      //       std::cout << " o creating new InSANEPhaseSpace\n";

      ps = new InSANEPhaseSpace();

      auto * varEnergy = new InSANEPhaseSpaceVariable("energy_e", "E_{e'}", 0.5, 5.0);
      ps->AddVariable(varEnergy);

      auto *   varTheta = new InSANEPhaseSpaceVariable("theta_e", "#theta_{e'}", 25.0*degree, 60.0*degree );
      ps->AddVariable(varTheta);

      auto *   varPhi = new InSANEPhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree,180.0*degree );
      varPhi->SetUniform(true);
      ps->AddVariable(varPhi);

      SetPhaseSpace(ps);

   }
}
//______________________________________________________________________________

double InSANEInclusiveDiffXSec::Vs_W(double *x, double *p) 
{
   Double_t E0 = GetBeamEnergy();
   Double_t W  = x[0];
   Double_t th = p[0]; 
   fFuncArgs[0] = InSANE::Kine::Eprime_W2theta(W*W,th,E0);
   fFuncArgs[1] = p[0];
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________

double InSANEInclusiveDiffXSec::EnergyDepXSec(double *x, double *p) 
{
   fFuncArgs[0] = x[0];
   fFuncArgs[1] = p[0];
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________

double InSANEInclusiveDiffXSec::EnergyFractionDepXSec(double *x, double *p) 
{
   fFuncArgs[0] = (1.0 - x[0])*GetBeamEnergy();
   fFuncArgs[1] = p[0];
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________

double InSANEInclusiveDiffXSec::EnergyDependentXSec(double *x, double *p) 
{
   fFuncArgs[0] = x[0];
   fFuncArgs[1] = p[0];
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
double InSANEInclusiveDiffXSec::W2DependentXSec(double *x, double *p) {
   Double_t W2 = x[0];
   Double_t Eprime = InSANE::Kine::Eprime_W2theta(W2,p[0],GetBeamEnergy());
   fFuncArgs[0] = Eprime;
   fFuncArgs[1] = p[0];
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
double InSANEInclusiveDiffXSec::WDependentXSec(double *x, double *p) {
   Double_t W      = x[0];
   Double_t Q2     = p[0];
   Double_t xbj    = InSANE::Kine::xBjorken_WQsq(W, Q2);
   Double_t E0     = GetBeamEnergy();
   Double_t y_frac = Q2/(2.0*(M_p/GeV)*xbj*E0);
   if( y_frac >1.0 ) return 0.0;
   Double_t Eprime = InSANE::Kine::Eprime_xQ2y(xbj,Q2,y_frac);
   Double_t theta  = InSANE::Kine::Theta_xQ2y(xbj,Q2,y_frac);
   if( TMath::IsNaN(theta) )  return 0.0;
   if( TMath::IsNaN(Eprime) )  return 0.0;
   //std::cout << "W: " << W << ", Q2: " << Q2 << std::endl;
   //std::cout << "theta: " << theta << ", Eprime: " << Eprime ;
   //std::cout << ", y_frac: " << y_frac << ", x: " << xbj << std::endl;
   fFuncArgs[0]            = Eprime;
   fFuncArgs[1]            = theta;
   fFuncArgs[2]            = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
double InSANEInclusiveDiffXSec::xDependentXSec(double *x, double *p) {
   Double_t x_bj = x[0];
   Double_t Q2   = p[0];
   Double_t E0   = GetBeamEnergy();
   //Double_t phi  = p[1];
   Double_t xmin = Q2/(2.0*(M_p/GeV)*E0);
   if(x_bj <= xmin) return 0.0;
   Double_t y_frac = Q2/(2.0*(M_p/GeV)*x_bj*E0);
   Double_t Eprime = InSANE::Kine::Eprime_xQ2y(x_bj,Q2,y_frac); 
   Double_t theta   = InSANE::Kine::Theta_xQ2y(x_bj,Q2,y_frac);
   //std::cout << "theta: " << theta << ", Eprime: " << Eprime << std::endl;
   //std::cout << "y_frac: " << y_frac << ", x: " << x_bj << std::endl;
   if(Eprime < 0.0) return 0.0;
   if(y_frac < 0.0) return 0.0;
   if(y_frac >= 1.0) return 0.0;
   fFuncArgs[0] = Eprime;
   fFuncArgs[1] = theta;
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
double InSANEInclusiveDiffXSec::PhotonEnergyDependentXSec(double *x, double *p) {
   Double_t pEnergy = GetBeamEnergy() - x[0];
   fFuncArgs[0] = pEnergy;
   fFuncArgs[1] = p[0];
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
double InSANEInclusiveDiffXSec::MomentumDependentXSec(double *x, double *p) {
   TParticle * aParticle = nullptr;
   if(fParticles.GetEntries() > 0) aParticle = (TParticle*)fParticles.At(0);
   Double_t Eprime = x[0];
   if(aParticle) Eprime = TMath::Sqrt( x[0]*x[0] + TMath::Power(aParticle->GetMass(),2.0)); 
   fFuncArgs[0] = Eprime;
   fFuncArgs[1] = p[0];
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
double InSANEInclusiveDiffXSec::PolarAngleDependentXSec(double *x, double *p) {
   fFuncArgs[0] = p[0];
   fFuncArgs[1] = x[0];
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
double InSANEInclusiveDiffXSec::PolarAngleDependentXSec_deg(double *x, double *p) {
   fFuncArgs[0] = p[0];
   fFuncArgs[1] = x[0]*degree;
   fFuncArgs[2] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
double InSANEInclusiveDiffXSec::AzimuthalAngleDependentXSec(double *x, double *p) {
   fFuncArgs[0] = p[0];
   fFuncArgs[1] = p[1];
   fFuncArgs[2] = x[0];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________




ClassImp(InSANEFlatInclusiveDiffXSec)

//______________________________________________________________________________

InSANEFlatInclusiveDiffXSec::InSANEFlatInclusiveDiffXSec()
{
   fID = 100000001;
}
//______________________________________________________________________________

InSANEFlatInclusiveDiffXSec::~InSANEFlatInclusiveDiffXSec()
{ }
//______________________________________________________________________________

Double_t  InSANEFlatInclusiveDiffXSec::EvaluateXSec(const Double_t * x) const {

   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   Double_t th       = x[1];

   Double_t fullXsec = 1.0*hbarc2_gev_nb;

   if ( IncludeJacobian() ) {
      //std::cout << " using jacobian\n";
      return fullXsec*TMath::Sin(th);
   }

   return(fullXsec);
}
//______________________________________________________________________________

