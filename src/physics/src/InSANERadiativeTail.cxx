#include "InSANERadiativeTail.h"
#include "InSANEPhysics.h"

InSANERadiativeTail::InSANERadiativeTail()
{
   fID = 100010011;

   SetTitle("InSANERadiativeTail");//,"POLRAD Born cross-section");
   SetPlotTitle("Radiative Tail cross-section");

   fLabel = "#frac{d#sigma}{dEd#Omega}";
   fUnits = "nb/GeV/sr";

   fRadLen[0] = 0.025; 
   fRadLen[1] = 0.025; 

   fPOLRAD = nullptr;
   fRADCOR = nullptr;

   //InSANEPOLRADElasticDiffXSec *fDiffXSec = new  InSANEPOLRADElasticDiffXSec();
   //fDiffXSec->SetA(1);
   //fDiffXSec->SetZ(1);
   //fDiffXSec->InitializePhaseSpaceVariables();
   //fDiffXSec->InitializeFinalStateParticles();

   InSANENucleus::NucleusType Target = InSANENucleus::kProton; 

   InSANEFunctionManager * funcMan = InSANEFunctionManager::GetInstance();

   // Structure functions F1,F2
   InSANEStructureFunctions * sf   = funcMan->GetStructureFunctions();
   SetUnpolarizedStructureFunctions(sf);

   // Structure functions g1,g2
   InSANEPolarizedStructureFunctions * psf   = funcMan->GetPolarizedStructureFunctions();
   SetPolarizedStructureFunctions(psf);

   // Quasi  structure functions
   auto * F1F209QESFs = new F1F209QuasiElasticStructureFunctions();
   SetQEStructureFunctions(F1F209QESFs);

   // Nucleon form factors 
   InSANEFormFactors * FFs = funcMan->GetFormFactors(); 
   SetFormFactors(FFs);

   // Nuclei form factors 
   //InSANEFormFactors * NFFs = new AmrounFormFactors();
   InSANEFormFactors * NFFs = new MSWFormFactors();
   SetTargetFormFactors(NFFs);
}
//________________________________________________________________________________
InSANERadiativeTail::~InSANERadiativeTail(){
}
//________________________________________________________________________________
void InSANERadiativeTail::CreateRADCOR() const {
   fRADCOR = new InSANERADCOR();
   //fRADCOR->Do(false); 
   fRADCOR->SetThreshold(1); 
   fRADCOR->UseMultiplePhoton();
   fRADCOR->UseInternal(true);  
   fRADCOR->UseExternal(true);  
   fRADCOR->SetPolarization(0); 
   fRADCOR->SetVerbosity(0); 
   //fRADCOR->SetCrossSection(fDiffXSec); 
   fRADCOR->SetTargetNucleus(InSANENucleus::Proton()); 
   fRADCOR->SetRadiationLengths(fRadLen);
}
//________________________________________________________________________________
void InSANERadiativeTail::CreatePOLRAD() const {
   fPOLRAD = new InSANEPOLRAD();
   fPOLRAD->SetVerbosity(1);
   //fPOLRAD->DoQEFullCalc(false); 
   fPOLRAD->SetTargetNucleus(InSANENucleus::Proton());
   fPOLRAD->fErr   = 1E-1;   // integration error tolerance 
   fPOLRAD->fDepth = 3;     // number of iterations for integration 
   fPOLRAD->SetMultiPhoton(true); 
   //fPOLRAD->SetUltraRel   (false);
}

//________________________________________________________________________________
//________________________________________________________________________________
//________________________________________________________________________________
