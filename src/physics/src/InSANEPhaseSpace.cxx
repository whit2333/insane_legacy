#include "InSANEPhaseSpace.h"

#include "TString.h"
#include "TClonesArray.h"
#include <iostream>
#include <vector>


ClassImp(InSANEPhaseSpace)

//______________________________________________________________________________
void InSANEPhaseSpace::Print(Option_t * opt ) const {
   //std::cout << "    Phase Space Address   : " << this << "\n";
   std::cout << std::setw(25) << std::right <<  "Phase Space Dimension : " << fnDim << "\n";
   const int N  = fVariables.GetEntries();
   if (N != fnDim) std::cout << " ARRAY SIZE AND DIMENSION DO NOT MATCH!!!!\n";
   for (int i = 0; i < N; i++)((InSANEPhaseSpaceVariable*)fVariables.At(i))->Print();
}
//______________________________________________________________________________
void InSANEPhaseSpace::Print(std::ostream& stream) const {
   //std::cout << "    Phase Space Address   : " << this << "\n";
   stream << std::setw(25) << std::right <<  "Phase Space Dimension : " << fnDim << "\n";
   const int N  = fVariables.GetEntries();
   if (N != fnDim) stream << " ARRAY SIZE AND DIMENSION DO NOT MATCH!!!!\n";
   for (int i = 0; i < N; i++)((InSANEPhaseSpaceVariable*)fVariables.At(i))->Print(stream);
}
//______________________________________________________________________________
