#include "InSANEPhaseSpaceSampler.h"

#include "InSANEPhaseSpace.h"
#include "TH2D.h"
#include "TRandom3.h"
#include "TFoam.h"
#include "TCanvas.h"
#include "InSANEPhaseSpace.h"
#include "InSANEInclusiveDiffXSec.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"
#include "time.h"


InSANEPhaseSpaceSampler::InSANEPhaseSpaceSampler(InSANEDiffXSec * xsec) 
{
   fWeight                = 1.0;
   fIsModified = true;
   fTotalXSection         = 0.0;
   for(int i = 0;i<30;i++) fMCvect[i] = 0;
   fDiffXSec              = nullptr;
   fFoam                  = nullptr;
   fRandomNumberGenerator = nullptr;

   fFoamCells    = 2000; // default(1000) No of allocated number of cells,
   fFoamSample   = 500;  // default(200)  No. of MC events in the cell MC exploration
   fFoamBins     = 8;    // default(8)    No. of bins in edge-histogram in cell exploration
   
   fFoamOptRej   = 1;    // default(1)    OptRej = 0, weighted; OptRej=1, wt=1 MC events
   fFoamOptDrive = 2;    // default(2)    Maximum weight reduction, =1 for variance reduction
   fFoamEvPerBin = 50;   // default(25)   Maximum number of the effective wt=1 events/bin,
   fFoamMaxWtRej = 1.1;  // 
   fFoamChat     = 0;    // default(1)    0,1,2 is the ``chat level'' in the standard output
   if (xsec) {
      SetXSec(xsec);
   }
   if (!fRandomNumberGenerator) {
      fRandomNumberGenerator   = new TRandom3(0);//gRandom;//InSANERunManager::GetRunManager()->GetRandom();  // Create random number generator
   }
}
//________________________________________________________________________________

InSANEPhaseSpaceSampler::InSANEPhaseSpaceSampler(const InSANEPhaseSpaceSampler& rhs) :
   TObject(rhs),
   fWeight(rhs.fWeight)               ,
   fIsModified(rhs.fIsModified)       ,
   fTotalXSection(rhs.fTotalXSection) ,
   fDiffXSec(rhs.fDiffXSec->Clone())  ,
   fFoam(nullptr)                           ,
   fRandomNumberGenerator(gRandom)    ,
   fFoamCells(rhs.fFoamCells)         ,
   fFoamSample(rhs.fFoamSample)       ,
   fFoamBins(rhs.fFoamBins)           ,
   fFoamOptRej(rhs.fFoamOptRej)       ,
   fFoamOptDrive(rhs.fFoamOptDrive)   ,
   fFoamEvPerBin(rhs.fFoamEvPerBin)   ,
   fFoamChat(rhs.fFoamChat)
{
   fCrossSection.Clear();
   fXSectionList.AddAll(&(rhs.fXSectionList));
   fCrossSection.Clear();
   fCrossSection.AddAll(&(rhs.fCrossSection));
}
//______________________________________________________________________________

InSANEPhaseSpaceSampler& InSANEPhaseSpaceSampler::operator=(const InSANEPhaseSpaceSampler& rhs)
{
   if (this != &rhs) {  // make sure not same object
      TObject::operator=(rhs);
      fWeight                = rhs.fWeight         ;    
      fIsModified            = rhs.fIsModified     ;
      fTotalXSection         = rhs.fTotalXSection  ;
      fDiffXSec              = rhs.fDiffXSec->Clone();
      fFoam                  = nullptr; 
      fRandomNumberGenerator = gRandom;
      fFoamCells             = rhs.fFoamCells    ; 
      fFoamSample            = rhs.fFoamSample   ; 
      fFoamBins              = rhs.fFoamBins     ; 
      fFoamOptRej            = rhs.fFoamOptRej   ; 
      fFoamOptDrive          = rhs.fFoamOptDrive ; 
      fFoamEvPerBin          = rhs.fFoamEvPerBin ; 
      fFoamChat              = rhs.fFoamChat     ; 
      fFoamMaxWtRej          = rhs.fFoamMaxWtRej ; 

      fXSectionList.Clear();
      fXSectionList.AddAll(&(rhs.fXSectionList));

      fCrossSection.Clear();
      fCrossSection.AddAll(&(rhs.fCrossSection));
      //fCrossSection;  // This exists to store the cross section which doesn't stream by itself
      //                // but does when it is in a container. When the sampler is read back from
      //                // a file it should initialized with this cross section by calling InitFromDisk
   }
   return *this;
}
//______________________________________________________________________________

InSANEPhaseSpaceSampler::~InSANEPhaseSpaceSampler()
{
   if(fDiffXSec) delete fDiffXSec;
   fDiffXSec = nullptr;
   //if (fFoam) delete fFoam;
   //fFoam = 0;
   //delete fMCvect;
   //fMCvect = 0;
}
//______________________________________________________________________________
void InSANEPhaseSpaceSampler::InitFromDisk() {
   if(gRandom){
      fRandomNumberGenerator   = gRandom;//InSANERunManager::GetRunManager()->GetRandom();  // Create random number generator
   }
   fFoam->SetPseRan(fRandomNumberGenerator);   // Set random number generator
   if(fCrossSection.GetEntries()>0){ 
      SetXSec( (InSANEDiffXSec*)fCrossSection.At(0) , false);
   } else {
      Error("InitFromDisk","No Cross Section stored in list.");
   }
   std::cout << "going to init cross seciton...\n";
   GetXSec()->InitFromDisk();
}
//________________________________________________________________________________
void InSANEPhaseSpaceSampler::SetXSec(InSANEDiffXSec * xsec, Bool_t mod) {
   SetModified(mod);
   if(xsec)xsec->SetIncludeJacobian(true);
   fDiffXSec = xsec;
   fCrossSection.Clear("nodelete");
   fCrossSection.Add(fDiffXSec);
   if(fFoam) fFoam->SetRho(xsec);
}
//________________________________________________________________________________
void InSANEPhaseSpaceSampler::Print(const Option_t* option) const {
   // option s for short print
   std::cout <<  std::setw(25) << std::right << "                Weight : " << fWeight << std::endl; 
   std::cout <<  std::setw(25) << std::right << "   Total cross section : " << fTotalXSection << " nb. " << std::endl; 
   // don't print xsec if short
   if(strcmp("S",option) || strcmp("s",option))if (fDiffXSec) fDiffXSec->Print();
}
//________________________________________________________________________________
void InSANEPhaseSpaceSampler::Print(std::ostream& stream) const {
   // option s for short print
   stream <<  std::setw(25) << std::right << "                Weight : " << fWeight << std::endl; 
   stream <<  std::setw(25) << std::right << "   Total cross section : " << fTotalXSection << " nb. " << std::endl; 
   // don't print xsec if short
   if(fDiffXSec) fDiffXSec->Print(stream);
}
//________________________________________________________________________________
void InSANEPhaseSpaceSampler::PrintFoamConfig(std::ostream& c) const {
}
//________________________________________________________________________________
void InSANEPhaseSpaceSampler::Refresh(InSANEDiffXSec * xsec ) {
   //std::cout << "InSANEPhaseSpaceSampler::Refresh" << std::cout ;
   if (xsec) {
      if (fDiffXSec) delete fDiffXSec;
      fDiffXSec = nullptr;
      SetXSec(xsec);
   }
   if (fDiffXSec) {
      fDiffXSec->Refresh();
      if(!fDiffXSec->GetPhaseSpace() ) Error("Refresh()","XSec has Null P.S.");

      //fDiffXSec->SetTotalXSec(NormalizePDF());

      if (fFoam) delete fFoam;
      fFoam = nullptr;
      fFoam    = new TFoam("FoamX");   // Create Simulators
      fFoam->SetkDim(      fDiffXSec->NDim());            // No. of dimensions, obligatory!
      fFoam->SetnCells(    fFoamCells);
      fFoam->SetnSampl(    fFoamSample);   // optional
      fFoam->SetChat(      fFoamChat);     // optional
      fFoam->SetnBin(      fFoamBins);     // optional
      fFoam->SetOptRej(    fFoamOptRej);   // optional
      fFoam->SetOptDrive(  fFoamOptDrive); // optional
      fFoam->SetMaxWtRej(  fFoamMaxWtRej); // optional
      fFoam->SetEvPerBin(  fFoamEvPerBin); // optional
      fFoam->SetRho(       fDiffXSec);     // Set 2-dim distribution,
      //fDiffXSec->Print();
      //std::cout << fDiffXSec << std::endl;
      for(int i = 0; i<fDiffXSec->GetPhaseSpace()->GetNIndependentVariables() ;i++ ) {
         bool is_uniform = fDiffXSec->GetPhaseSpace()->GetIndependentVariable(i)->IsUniform();
         if( is_uniform ){
            fFoam->SetInhiDiv(i,1);
         }
      }
      fFoam->SetPseRan(fRandomNumberGenerator);   // Set random number generator
      fFoam->Initialize();        // Initialize simulator, takes a few seconds...

      // Note here we are using the weight in calculating the total cross section
      // This could be passed in as luminosity which means that totalXSection is 
      // actually a rate.
      fTotalXSection = fWeight*fFoam->GetPrimary()*fDiffXSec->GetPhaseSpace()->GetScaledIntegralJacobian();

      fDiffXSec->SetTotalXSec(fTotalXSection);
   } else {
      Error("Refresh()", "Null fDiffXSec!");
   }
   SetModified(false);
}
//________________________________________________________________________________
Double_t InSANEPhaseSpaceSampler::NormalizePDF() {
   /// Deprecated
   //fTotalXSection = fFoam->GetPrimary();
   return(fTotalXSection);
}
//________________________________________________________________________________
Double_t InSANEPhaseSpaceSampler::CalculateTotalXSection() {
   /// Deprecated
   return(NormalizePDF());
}
//________________________________________________________________________________
Double_t * InSANEPhaseSpaceSampler::GenerateEvent() {
   fFoam->MakeEvent();          // generate MC event
   fFoam->GetMCvect(fMCvect);    // get generated vector (x,y)
   // for(int i = 0; i<fDiffXSec->GetPhaseSpace()->GetDimension() ;i++ )
   Double_t * vars    = fDiffXSec->GetUnnormalizedVariables(fMCvect);
   Double_t * allVars = fDiffXSec->GetDependentVariables(vars);
   // is this needed?
   fDiffXSec->GetPhaseSpace()->SetEventValues(allVars);
   fDiffXSec->DefineEvent(allVars);  // Defines all the particle's values
   return(allVars);
}
//________________________________________________________________________________

