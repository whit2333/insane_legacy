#include "InSANEExclusiveDiffXSec.h"
#include "TMath.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"
#include "InSANEStructureFunctions.h"
#include "TCanvas.h"
#include "TGraph2D.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TVirtualPad.h"
#include "TAxis.h"

//________________________________________________________________________

ClassImp(InSANEExclusiveDiffXSec)

//________________________________________________________________________
InSANEExclusiveDiffXSec::InSANEExclusiveDiffXSec()
{
   fID         = 2000;
   fnDim = 2;
   fnParticles = 2;
   fPIDs.push_back(2212);//proton
   //InitializePhaseSpaceVariables();
}

//________________________________________________________________________
InSANEExclusiveDiffXSec::~InSANEExclusiveDiffXSec() {
}
//______________________________________________________________________________
void InSANEExclusiveDiffXSec::DefineEvent(Double_t * vars) {

   Int_t totvars = 0;
   for (int i = 0; i < fParticles.GetEntries(); i++) {
      /// \todo fix this hard coding of 3 variables per event.
      /// here we are assuming the order E,theta,phi,then others
      /// \todo figure out how to handle vertex.
      InSANE::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
      totvars += GetNParticleVars(i);
   }

}

//________________________________________________________________________
Double_t  InSANEExclusiveDiffXSec::EvaluateXSec(const Double_t * x) const
{

   /// Return zero if the variables are not in the phase space
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   Double_t Eprime = x[0];
   Double_t theta = x[1];
   //Double_t phi = x[2];
   Double_t Qsquared = 4.0 * GetBeamEnergy() * Eprime * TMath::Power(TMath::Sin(theta / 2.0), 2);
   Double_t xbjorken = Qsquared / (2.0 * 0.938 * (GetBeamEnergy() - Eprime));
   // MeV and degrees
   /*Double_t hbarcSquaredperbarn = 6.5822e-22*6.5822e-22*9.0e16/(1.0e-28); // GeV^2 ubarn */
   Double_t hbarc2 = 0.38939129; /*(hbar*c)^2 = 0.38939129 GeV^2 mbarn */
   Double_t mottXSec = hbarc2 * (1. / 137.) * (1. / 137.) *
                       TMath::Power(TMath::Cos(theta / 2.0), 2) /
                       (4.0 * GetBeamEnergy() * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 4))
                       * (1.0 / 0.938);
   return(mottXSec);
}
//______________________________________________________________________________

void InSANEExclusiveDiffXSec::InitializePhaseSpaceVariables() {

   InSANEPhaseSpace * ps = GetPhaseSpace();
   if(ps) delete ps;
   ps = nullptr;

   if (!ps) {
      std::cout << " Creating NEW InSANEPhaseSpace for InSANEExclusiveDiffXSec\n";
      ps = new InSANEPhaseSpace();

      // ------------------------------
      // Electron
      auto * varEnergy = new InSANEPhaseSpaceVariable();
      varEnergy = new InSANEPhaseSpaceVariable();
      varEnergy->SetNameTitle("energy_e", "E_{e'}");
      varEnergy->SetMinimum(0.50);
      varEnergy->SetMaximum(GetBeamEnergy());
      varEnergy->SetDependent(true);
      ps->AddVariable(varEnergy);

      auto *   varTheta = new InSANEPhaseSpaceVariable();
      varTheta->SetNameTitle("theta_e", "#theta_{e'}");
      varTheta->SetMinimum(20.0 * TMath::Pi() / 180.0);
      varTheta->SetMaximum(160.0 * TMath::Pi() / 180.0);
      ps->AddVariable(varTheta);

      auto *   varPhi = new InSANEPhaseSpaceVariable();
      varPhi->SetNameTitle("phi_e", "#phi_{e'}");
      varPhi->SetMinimum(-10.0 * TMath::Pi()/180.0 );
      varPhi->SetMaximum( 10.0 * TMath::Pi()/180.0 );
      varPhi->SetUniform(true);
      ps->AddVariable(varPhi);


      // ------------------------------
      // Proton
      auto * varEnergy_p = new InSANEPhaseSpaceVariable();
      varEnergy_p = new InSANEPhaseSpaceVariable();
      varEnergy_p->SetNameTitle("energy_p", "E_{p}");
      varEnergy_p->SetMinimum(0.9);
      varEnergy_p->SetMaximum(5.0);
      varEnergy_p->SetParticleIndex(1);
      varEnergy_p->SetDependent(true);
      ps->AddVariable(varEnergy_p);

      auto *   varTheta_p = new InSANEPhaseSpaceVariable();
      varTheta_p->SetNameTitle("theta_p", "#theta_{p}");
      varTheta_p->SetMinimum(0.5*degree);
      varTheta_p->SetMaximum(170*degree);
      varTheta_p->SetParticleIndex(1);
      varTheta_p->SetDependent(true);
      ps->AddVariable(varTheta_p);

      auto *   varPhi_p = new InSANEPhaseSpaceVariable();
      varPhi_p->SetNameTitle("phi_p", "#phi_{p}");
      varPhi_p->SetMinimum(- 1.0*TMath::Pi() );
      varPhi_p->SetMaximum(  2.0*TMath::Pi() );
      varPhi_p->SetParticleIndex(1);
      varPhi_p->SetDependent(true);
      varPhi_p->SetInverted(true);
      ps->AddVariable(varPhi_p);

      SetPhaseSpace(ps);
   } else {
      std::cout << " Using existing phase space variables !\n";
   }
}
//______________________________________________________________________________



ClassImp(InSANEFlatExclusiveDiffXSec)

//______________________________________________________________________________
Double_t  InSANEFlatExclusiveDiffXSec::EvaluateXSec(const Double_t * x) const
{
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   /// Returns a constant
   return(1.0);
}
//______________________________________________________________________________



ClassImp(InSANEExclusiveMottXSec)

//______________________________________________________________________________
Double_t  InSANEExclusiveMottXSec::EvaluateXSec(const Double_t * x) const
{
   /// Get the recoiling proton momentum
   if (GetBeamEnergy() < GetEPrime(x[1])) return(0.0);

//    TVector3 k1(0,0,GetBeamEnergy());  // incident electron
//    TVector3 k2(0,0,0);            // scattered electron
//    k2.SetMagThetaPhi(GetEPrime(x[1]),x[1],x[2]);
//    TVector3 p2 = k1-k2;  // recoil proton
//    Double_t y[6] = {x[0],x[1],x[2],x[3],x[4],x[5]};
//
//    Double_t  * z = GetDependentVariables(y);

   ///
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   Double_t Eprime = x[0];
   Double_t theta = x[1];
   Double_t hbarc2 = 0.38939129; /*(hbar*c)^2 = 0.38939129 GeV^2 mbarn */
   Double_t mottXSec = hbarc2 * (1. / 137.) * (1. / 137.) *
                       TMath::Power(TMath::Cos(theta / 2.0), 2) /
                       (4.0 * GetBeamEnergy() * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 4))
                       * (Eprime / GetBeamEnergy());
   return(mottXSec);
}



