#include "SpinStructureFunctions.h"
#include "TMath.h"
#include "InSANEMathFunc.h"

namespace insane {
  namespace physics {

    SpinStructureFunctions::SpinStructureFunctions()
    { }
    //______________________________________________________________________________

    SpinStructureFunctions::~SpinStructureFunctions()
    { }
    //______________________________________________________________________________
 
    double SpinStructureFunctions::Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const
    {
      auto sftype = std::get<SF>(sf);
      auto target = std::get<Nuclei>(sf);

      switch(sftype) {

        case SF::g1 : 
          return g1(x,Q2,target);
          break;

        case SF::g2 : 
          return g2(x,Q2,target);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::g1(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return g1p(x,Q2);
          break;

        case Nuclei::n : 
          return g1n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::g2(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return g2p(x,Q2);
          break;

        case Nuclei::n : 
          return g2n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
    
    double SpinStructureFunctions::g1_TMC(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return g1p_TMC(x,Q2);
          break;

        case Nuclei::n : 
          return g1n_TMC(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::g2_TMC(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return g2p_TMC(x,Q2);
          break;

        case Nuclei::n : 
          return g2n_TMC(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
    double SpinStructureFunctions::g2_WW(double x, double Q2, Nuclei target) const
    {
      return 0.0; 
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::g1_BT(double x, double Q2, Nuclei target) const
    {
      return 0.0; 
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::Mellin_g1p(Int_t n, double Q2,double x1, double x2)
    {
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        double xn = TMath::Power(x, double(n)-1.0);
        result += (dx*xn*g1p(x,Q2));
      }
      return result;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::Mellin_g2p(Int_t n, double Q2,double x1,double x2)
    {
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        double xn = TMath::Power(x, double(n)-1.0);
        result += (dx*xn*g2p(x,Q2));
      }
      return result;
    }
    //______________________________________________________________________________
    //double SpinStructureFunctions::d2p_WW(double Q2,double x1,double x2)
    //{
    //  // d2 tilde (no elastic contribution) 
    //  // Calculated using only the WW g2 and leading twist g1
    //  //double x1     = 0.1;
    //  //double x2     = 0.9;
    //  double result = 0.0;
    //  Int_t N         = 100;
    //  double dx     = (x2 - x1) / ((double)N);
    //  // quick simple integration
    //  for (int i = 0; i < N; i++) {
    //    double x = x1 + dx*double(i);
    //    result += (dx*(x*x)*( 2.0*g1p_Twist2(x,Q2) + 3.0*g2pWW(x,Q2) ));
    //  }
    //  return(result);
    //}
    ////_____________________________________________________________________________

    //double SpinStructureFunctions::d2p_Twist2_TMC(double Q2,double x1,double x2)
    //{
    //  // d2 tilde (no elastic contribution) 
    //  //double x1     = 0.1;
    //  //double x2     = 0.9;
    //  double result = 0.0;
    //  Int_t N         = 100;
    //  double dx     = (x2 - x1) / ((double)N);
    //  // quick simple integration
    //  for (int i = 0; i < N; i++) {
    //    double x = x1 + dx*double(i);
    //    result += (dx*(x*x)*( 2.0*g1p_Twist2_TMC(x,Q2) + 3.0*g2p_Twist2_TMC(x,Q2) ));
    //  }
    //  return(result);
    //}
    ////_____________________________________________________________________________

    //double SpinStructureFunctions::d2p_Twist3(double Q2,double x1,double x2)
    //{
    //  // d2 tilde (no elastic contribution) 
    //  // Calculated using only the WW g2 and leading twist g1
    //  //double x1     = 0.1;
    //  //double x2     = 0.9;
    //  double result = 0.0;
    //  Int_t N         = 100;
    //  double dx     = (x2 - x1) / ((double)N);
    //  // quick simple integration
    //  for (int i = 0; i < N; i++) {
    //    double x = x1 + dx*double(i);
    //    result += (dx*(x*x)*( 2.0*g1p_Twist3(x,Q2) + 3.0*g2p_Twist3(x,Q2) ));
    //  }
    //  return(result);
    //}
    ////_____________________________________________________________________________

    //double SpinStructureFunctions::d2p_Twist3_TMC(double Q2,double x1,double x2)
    //{
    //  // d2 tilde (no elastic contribution) 
    //  // Calculated using only the WW g2 and leading twist g1
    //  //double x1     = 0.1;
    //  //double x2     = 0.9;
    //  double result = 0.0;
    //  Int_t N         = 100;
    //  double dx     = (x2 - x1) / ((double)N);
    //  // quick simple integration
    //  for (int i = 0; i < N; i++) {
    //    double x = x1 + dx*double(i);
    //    result += (dx*(x*x)*( 2.0*g1p_Twist3_TMC(x,Q2) + 3.0*g2p_Twist3_TMC(x,Q2) ));
    //  }
    //  return(result);
    //}
    ////_____________________________________________________________________________

    double SpinStructureFunctions::d2p_tilde(double Q2,double x1,double x2)
    {
      // d2 tilde (no elastic contribution) 
      //double x1     = 0.1;
      //double x2     = 0.9;
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        result += (dx*(x*x)*( 2.0*g1p(x,Q2) + 3.0*g2p(x,Q2) ));
      }
      return(result);
    }
    //_____________________________________________________________________________

    double SpinStructureFunctions::d2p_tilde_TMC(double Q2,double x1,double x2)
    {
      // d2 tilde (no elastic contribution) 
      //double x1     = 0.1;
      //double x2     = 0.9;
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for(int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        result += (dx*(x*x)*( 2.0*g1p_TMC(x,Q2) + 3.0*g2p_TMC(x,Q2) ));
      }
      return(result);
    }
    //_____________________________________________________________________________

    double SpinStructureFunctions::M1n_p(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<1 ) {
        Error("M1n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        result += (dx*M1nIntegrand_p(n,x,Q2));
      }
      return result;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M1nIntegrand_p(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = InSANE::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1p(x,Q2) + c2*g2p(x,Q2)) );
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2n_p(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<3 ) { 
        Error("M2n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        result += (dx*M2nIntegrand_p(n,x,Q2));
      }
      return result;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2nIntegrand_p(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = InSANE::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/double(n-1) - y2*x*x*double(n)/double(n+1);
      return( c0*(c1*g1p(x,Q2) + c2*g2p(x,Q2)) );
    }
    //______________________________________________________________________________
    double SpinStructureFunctions::M1n_TMC_p(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<1 ) {
        Error("M1n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        result += (dx*M1nIntegrand_TMC_p(n,x,Q2));
      }
      return result;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M1nIntegrand_TMC_p(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = InSANE::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1p_TMC(x,Q2) + c2*g2p_TMC(x,Q2)) );
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2n_TMC_p(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<3 ) { 
        Error("M2n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        result += (dx*M2nIntegrand_TMC_p(n,x,Q2));
      }
      return result;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2nIntegrand_TMC_p(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = InSANE::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n)+1.0)/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/(double(n)-1.0) - y2*x*x*double(n)/(double(n)+1.0);
      return( c0*(c1*g1p_TMC(x,Q2) + c2*g2p_TMC(x,Q2)) );
    }
    //______________________________________________________________________________
  }
}

