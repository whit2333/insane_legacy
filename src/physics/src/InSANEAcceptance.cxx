#include "InSANEAcceptance.h"
#include "InSANEMathFunc.h"
#include "TRandom3.h"

ClassImp(InSANEAcceptance)
//___________________________________________________________________________

InSANEAcceptance::InSANEAcceptance(const char * n , const char * t) : TNamed(n,t) {
   fFunctions.Clear();

   fBeamEnergy = 5.9;
   fEnergyMin  = 0.5;
   fEnergyMax  = 5.0;
   fThetaMin   = 30.0*TMath::Pi()/180.0;
   fThetaMax   = 50.0*TMath::Pi()/180.0;

}
//___________________________________________________________________________


InSANEAcceptance::~InSANEAcceptance(){

}

//___________________________________________________________________________

void InSANEAcceptance::Initialize(){
    //TF1 * f = 0;
    Double_t x,Q2,W,E,Eprime,theta;

    fEnergy = (fEnergyMax+fEnergyMin)/2.0;
    fTheta  = (fThetaMax+fThetaMin)/2.0;

    fxMin =  999999.0;
    fxMax = -999999.0;
    fWMin =  999999.0;
    fWMax = -999999.0;
    fQ2Min =  999999.0;
    fQ2Max = -999999.0;
    
    //1 
    E      = fBeamEnergy;
    Eprime = fEnergyMin;
    theta  = fThetaMin;
 
    Q2     = InSANE::Kine::Qsquared(E,Eprime,theta);
    x      = InSANE::Kine::xBjorken_EEprimeTheta(E,Eprime,theta);
    W      = InSANE::Kine::W_EEprimeTheta(E,Eprime,theta); 
    if( x < fxMin ) fxMin = x;
    if( x > fxMax ) fxMax = x;
    if( Q2 < fQ2Min ) fQ2Min = Q2;
    if( Q2 > fQ2Max ) fQ2Max = Q2;
    if( W < fWMin ) fWMin = W;
    if( W > fWMax ) fWMax = W;
    
    //2
    E      = fBeamEnergy;
    Eprime = fEnergyMin;
    theta  = fThetaMax;

    Q2     = InSANE::Kine::Qsquared(E,Eprime,theta);
    x      = InSANE::Kine::xBjorken_EEprimeTheta(E,Eprime,theta);
    W      = InSANE::Kine::W_EEprimeTheta(E,Eprime,theta); 
    if( x < fxMin ) fxMin = x;
    if( x > fxMax ) fxMax = x;
    if( Q2 < fQ2Min ) fQ2Min = Q2;
    if( Q2 > fQ2Max ) fQ2Max = Q2;
    if( W < fWMin ) fWMin = W;
    if( W > fWMax ) fWMax = W;

    //3
    E      = fBeamEnergy;
    Eprime = fEnergyMax;
    theta  = fThetaMax;

    Q2     = InSANE::Kine::Qsquared(E,Eprime,theta);
    x      = InSANE::Kine::xBjorken_EEprimeTheta(E,Eprime,theta);
    W      = InSANE::Kine::W_EEprimeTheta(E,Eprime,theta); 
    if( x < fxMin ) fxMin = x;
    if( x > fxMax ) fxMax = x;
    if( Q2 < fQ2Min ) fQ2Min = Q2;
    if( Q2 > fQ2Max ) fQ2Max = Q2;
    if( W < fWMin ) fWMin = W;
    if( W > fWMax ) fWMax = W;

    //4
    E      = fBeamEnergy;
    Eprime = fEnergyMax;
    theta  = fThetaMin;

    Q2     = InSANE::Kine::Qsquared(E,Eprime,theta);
    x      = InSANE::Kine::xBjorken_EEprimeTheta(E,Eprime,theta);
    W      = InSANE::Kine::W_EEprimeTheta(E,Eprime,theta); 
    if( x < fxMin ) fxMin = x;
    if( x > fxMax ) fxMax = x;
    if( Q2 < fQ2Min ) fQ2Min = Q2;
    if( Q2 > fQ2Max ) fQ2Max = Q2;
    if( W < fWMin ) fWMin = W;
    if( W > fWMax ) fWMax = W;


}
//___________________________________________________________________________
TH2F * InSANEAcceptance::KinematicCoverage_xQ2(Int_t N) {
   TRandom3 rand;

   auto * h2 = new TH2F(Form("%s-Q2Vsx",GetName()),Form("%s - Q2 Vs x",GetTitle() ), 100,fxMin,fxMax,100,fQ2Min,fQ2Max);

   Double_t x,W,Q2,e,eprime,theta;

   for(int i = 0; i <N ;i++) {

      e = fBeamEnergy;
      eprime = rand.Uniform(fEnergyMin,fEnergyMax);
      theta = rand.Uniform(fThetaMin,fThetaMax);
      Q2     = InSANE::Kine::Qsquared(e,eprime,theta);
      x      = InSANE::Kine::xBjorken_EEprimeTheta(e,eprime,theta);
      W      = InSANE::Kine::W_EEprimeTheta(e,eprime,theta); 

      h2->Fill(x,Q2); 

   }

   return(h2);
}
//___________________________________________________________________________

