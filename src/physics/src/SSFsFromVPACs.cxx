#include "SSFsFromVPACs.h"
//#include "InSANEFunctionManager.h"

//namespace insane {
//  namespace physics {
//    //______________________________________________________________________________
//    SSFsFromVPACs::SSFsFromVPACs()
//    {
//      SetLabel("MAID07");
//      SetNameTitle("Spin SFs from MAID07", "Spin SFs from MAID07");
//      InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
//      fSFs   = fman->CreateSFs(9);
//      fVCSAs = 0;
//    }
//    //______________________________________________________________________________
//    SSFsFromVPACs::~SSFsFromVPACs()
//    {
//    }
//    //______________________________________________________________________________
//
//    Double_t SSFsFromVPACs::g1p(   Double_t x, Double_t Q2)
//    {
//      if(!fVCSAs) return 0.0;
//      double M = (M_p/GeV);
//      double gamma2 = (4.0*M*M*x*x)/Q2;
//      double F1 =  fSFs->F1p(x,Q2);
//
//      double g1 = (F1/(1.0+gamma2))*(fVCSAs->A1p(x,Q2) + TMath::Sqrt(gamma2)*fVCSAs->A2p(x,Q2));
//      return g1;
//    }
//    //______________________________________________________________________________
//    Double_t SSFsFromVPACs::g1n(   Double_t x, Double_t Q2)
//    {
//      if(!fVCSAs) return 0.0;
//      double M = (M_p/GeV);
//      double gamma2 = (4.0*M*M*x*x)/Q2;
//      double F1 =  fSFs->F1n(x,Q2);
//
//      double g1 = (F1/(1.0+gamma2))*(fVCSAs->A1n(x,Q2) + TMath::Sqrt(gamma2)*fVCSAs->A2n(x,Q2));
//      return g1;
//    }
//    //______________________________________________________________________________
//    Double_t SSFsFromVPACs::g1d(   Double_t x, Double_t Q2)
//    {
//      return 0.0;
//    }
//    //______________________________________________________________________________
//    Double_t SSFsFromVPACs::g1He3( Double_t x, Double_t Q2)
//    {
//      return 0.0;
//    }
//    //______________________________________________________________________________
//    Double_t SSFsFromVPACs::g2p(   Double_t x, Double_t Q2)
//    {
//      if(!fVCSAs) return 0.0;
//      double M = (M_p/GeV);
//      double gamma2 = (4.0*M*M*x*x)/Q2;
//      double F1 =  fSFs->F1p(x,Q2);
//      double g2 = (F1/(1.0+gamma2))*(fVCSAs->A2p(x,Q2)/TMath::Sqrt(gamma2) - fVCSAs->A1p(x,Q2));
//      return g2;
//    }
//    //______________________________________________________________________________
//    Double_t SSFsFromVPACs::g2n(   Double_t x, Double_t Q2)
//    {
//      return 0.0;
//    }
//    //______________________________________________________________________________
//    Double_t SSFsFromVPACs::g2d(   Double_t x, Double_t Q2)
//    {
//      return 0.0;
//    }
//    Double_t SSFsFromVPACs::g2He3( Double_t x, Double_t Q2)
//    {
//      return 0.0;
//    }
//
//  }
//}
