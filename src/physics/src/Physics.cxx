#include "Physics.h"
#include "Math.h"
#include "TMath.h"
#include "InSANEMathFunc.h"

using namespace TMath;

namespace insane {


  namespace physics {

    double F1(double Q2, double GE, double GM)
    {
      double t  = Q2/(4.0*0.938*0.938);
      return (GE + GM*t)/(1.0+t);
    }

    double F2(double Q2, double GE, double GM)
    {
      double t  = Q2/(4.0*0.938*0.938);
      return (GM - GE)/(1.0+t);
    }

    double WW(std::function<double(double)> func, double x, double max)
    {
      return( -1.0*func(x) + insane::integrate::gaus(func,x,max) );
    }

    namespace TMCs {

      double h2(std::function<double(double)> F1,double xi, double Q2)
      {
        // Target Mass correction function
        // J. Phys. G 35, 053101 (2008)
        return(insane::integrate::gaus(
              [&](double z){ return F1(z)/(z*z);}, xi, 1.0) );
      }

      double g2(std::function<double(double)> F1,double xi, double Q2)
      {
        // J. Phys. G 35, 053101 (2008)
        return(insane::integrate::gaus(
              [&](double z){ return F1(z)*(z-xi)/(z*z);}, xi, 1.0) );
      }

    }
  }

  namespace kinematics {

    double rho2(double x, double Q2)
    {
      return( 1.0+4.0*x*x*((M_p/GeV)*(M_p/GeV))/Q2 );
    }

    double rho(double x, double Q2)
    {
      return( Sqrt(rho(x,Q2)) );
    }

    double xi_Nachtmann(double x, double Q2)
    {
      // Nachtmann scaling variable, xi
      return( 2.0*x/(1.0+Sqrt(1.0+4.0*x*x*((M_p/GeV)*(M_p/GeV))/Q2)) );
    }
  }

}
