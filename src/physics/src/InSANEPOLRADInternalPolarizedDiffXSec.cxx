#include "InSANEPOLRADInternalPolarizedDiffXSec.h"

ClassImp(InSANEPOLRADInternalPolarizedDiffXSec) 

//______________________________________________________________________________

InSANEPOLRADInternalPolarizedDiffXSec::InSANEPOLRADInternalPolarizedDiffXSec()
{
   fID         = 100010004;
   SetTitle("InSANEPOLRADInternalPolarizedDiffXSec");//,"POLRAD Born cross-section");
   SetPlotTitle("POLRAD Internal Polarized cross-section");

   fLabel = "#frac{d#sigma}{dxdy}";
   fUnits = "nb";

   fPOLRADCalc = nullptr;
   fPOLRADCalc = new InSANEPOLRAD();
   fPOLRADCalc->SetVerbosity(1);
   //fPOLRADCalc->DoQEFullCalc(false); 
   fPOLRADCalc->SetTargetNucleus(InSANENucleus::Proton());
   fPOLRADCalc->fErr   = 1E-1;   // integration error tolerance 
   fPOLRADCalc->fDepth = 3;     // number of iterations for integration 
   fPOLRADCalc->SetMultiPhoton(true); 
   //fPOLRADCalc->SetUltraRel   (false);

   InSANEFunctionManager * funcMan = InSANEFunctionManager::GetInstance();

   // Structure functions F1,F2
   InSANEStructureFunctions * sf   = funcMan->GetStructureFunctions();
   SetUnpolarizedStructureFunctions(sf);

   // Structure functions g1,g2
   InSANEPolarizedStructureFunctions * psf   = funcMan->GetPolarizedStructureFunctions();
   SetPolarizedStructureFunctions(psf);

   // Quasi Elastic structure functions
   auto * F1F209QESFs = new F1F209QuasiElasticStructureFunctions();
   SetQEStructureFunctions(F1F209QESFs);

   // Nucleon form factors 
   InSANEFormFactors * FFs = funcMan->GetFormFactors(); 
   SetFormFactors(FFs);

   // Nuclei form factors 
   //InSANEFormFactors * NFFs = new AmrounFormFactors();
   InSANEFormFactors * NFFs = new MSWFormFactors();
   SetTargetFormFactors(NFFs);

   fPOLRADCalc = GetPOLRAD();


}
//______________________________________________________________________________
InSANEPOLRADInternalPolarizedDiffXSec::~InSANEPOLRADInternalPolarizedDiffXSec(){
}
//______________________________________________________________________________
void InSANEPOLRADInternalPolarizedDiffXSec::InitializePhaseSpaceVariables(){

   if(fPOLRADCalc->GetVerbosity() > 0) std::cout << " o InSANEPOLRADInternalPolarizedDiffXSec::InitializePhaseSpaceVariables() \n";

   InSANEPhaseSpace * ps = GetPhaseSpace();
   if (ps) delete ps;
   ps = nullptr;
   if (!ps) {
      //       std::cout << " o creating new InSANEPhaseSpace\n";

      ps = new InSANEPhaseSpace();

      auto * varx = new InSANEPhaseSpaceVariable();
      varx = new InSANEPhaseSpaceVariable();
      varx->SetNameTitle("x", "x");
      varx->SetMinimum(0.01); //GeV
      varx->SetMaximum(0.999); //GeV
      //fxBjorken_var = varx->GetCurrentValueAddress();
      ps->AddVariable(varx);

      auto *   vary = new InSANEPhaseSpaceVariable();
      vary->SetNameTitle("y", "y"); // ROOT string latex
      vary->SetMinimum(0.01); //
      vary->SetMaximum(0.9999); //
      //fy_var = vary->GetCurrentValueAddress();
      ps->AddVariable(vary);

      auto *   varPhi = new InSANEPhaseSpaceVariable();
      varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
      varPhi->SetMinimum(-60.0 * TMath::Pi() / 180.0); //
      varPhi->SetMaximum(60.0 * TMath::Pi() / 180.0); //
      //fPhi_var = varPhi->GetCurrentValueAddress();
      ps->AddVariable(varPhi);

      SetPhaseSpace(ps);

   }else{
      std::cout << " Using existing phase space variables !\n";
   }
}

//______________________________________________________________________________

