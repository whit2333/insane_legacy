#include "InSANEPartonDistributionFunctionsFromPHDs.h"


//______________________________________________________________________________
InSANEUnpolarizedPDFsFromPHDs::InSANEUnpolarizedPDFsFromPHDs(){
}
//______________________________________________________________________________
InSANEUnpolarizedPDFsFromPHDs::~InSANEUnpolarizedPDFsFromPHDs(){
}
//______________________________________________________________________________
Double_t * InSANEUnpolarizedPDFsFromPHDs::GetPDFs(Double_t x, Double_t Q2){
   fXbjorken      = x;
   fQsquared      = Q2;

   fPHDSet = fPHDs->GetU(x,Q2);
   fPDFValues[0]  = fPHDSet.fQ_Plus    + fPHDSet.fQ_Minus    ; //up
   fPDFValues[7]  = fPHDSet.fQbar_Plus + fPHDSet.fQbar_Minus ; //ubar

   fPHDSet = fPHDs->GetD(x,Q2);
   fPDFValues[1]  = fPHDSet.fQ_Plus    + fPHDSet.fQ_Minus    ; //d
   fPDFValues[8]  = fPHDSet.fQbar_Plus + fPHDSet.fQbar_Minus ; //dbar

   fPHDSet = fPHDs->GetS(x,Q2);
   fPDFValues[2]  = fPHDSet.fQ_Plus    + fPHDSet.fQ_Minus    ; //s
   fPDFValues[9]  = fPHDSet.fQbar_Plus + fPHDSet.fQbar_Minus ; //sbar

   fPDFValues[3]  = 0;//c
   fPDFValues[4]  = 0;//b
   fPDFValues[5]  = 0;//t

   fPHDSet = fPHDs->GetG(x,Q2);
   fPDFValues[6]  = fPHDSet.fQ_Plus    + fPHDSet.fQ_Minus    ; //gluon

   fPDFValues[10] = 0;//cbar
   fPDFValues[11] = 0;//bbar
   fPDFValues[12] = 0;//tbar
   return fPDFValues;
}
//______________________________________________________________________________
Double_t * InSANEUnpolarizedPDFsFromPHDs::GetPDFErrors(Double_t x, Double_t Q2){
   fXbjorken = x;
   fQsquared = Q2;
   // No Errors yet. Need to implement!!!!!
   for(int i = 0;i< NPARTONDISTS; i++) fPDFErrors[i] = 0.0;
   return fPDFErrors;
}
//______________________________________________________________________________



//______________________________________________________________________________
InSANEPolarizedPDFsFromPHDs::InSANEPolarizedPDFsFromPHDs(){
}
//______________________________________________________________________________
InSANEPolarizedPDFsFromPHDs::~InSANEPolarizedPDFsFromPHDs(){
}
//______________________________________________________________________________
Double_t * InSANEPolarizedPDFsFromPHDs::GetPDFs(Double_t x, Double_t Q2){
   fXbjorken      = x;
   fQsquared      = Q2;

   fPHDSet = fPHDs->GetU(x,Q2);
   fPDFValues[0]  = fPHDSet.fQ_Plus    - fPHDSet.fQ_Minus    ; //up
   fPDFValues[7]  = fPHDSet.fQbar_Plus - fPHDSet.fQbar_Minus ; //ubar

   fPHDSet = fPHDs->GetD(x,Q2);
   fPDFValues[1]  = fPHDSet.fQ_Plus    - fPHDSet.fQ_Minus    ; //d
   fPDFValues[8]  = fPHDSet.fQbar_Plus - fPHDSet.fQbar_Minus ; //dbar

   fPHDSet = fPHDs->GetS(x,Q2);
   fPDFValues[2]  = fPHDSet.fQ_Plus    - fPHDSet.fQ_Minus    ; //s
   fPDFValues[9]  = fPHDSet.fQbar_Plus - fPHDSet.fQbar_Minus ; //sbar

   fPDFValues[3]  = 0;//c
   fPDFValues[4]  = 0;//b
   fPDFValues[5]  = 0;//t

   fPHDSet = fPHDs->GetG(x,Q2);
   fPDFValues[6]  = fPHDSet.fQ_Plus    - fPHDSet.fQ_Minus    ; //gluon

   fPDFValues[10] = 0;//cbar
   fPDFValues[11] = 0;//bbar
   fPDFValues[12] = 0;//tbar

   return fPDFValues;
}
//______________________________________________________________________________
Double_t * InSANEPolarizedPDFsFromPHDs::GetPDFErrors(Double_t x, Double_t Q2){
   fXbjorken = x;
   fQsquared = Q2;
   // No Errors yet. Need to implement!!!!!
   for(int i = 0;i< NPARTONDISTS; i++) fPDFErrors[i] = 0.0;
   return fPDFErrors;
}
//______________________________________________________________________________

