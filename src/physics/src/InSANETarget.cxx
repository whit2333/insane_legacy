#include "InSANETarget.h"
#include "TGeoBBox.h"
#include "TGeoManager.h"


ClassImp(InSANETarget)

//_______________________________________________________________________________
InSANETarget::InSANETarget(const char * name, const char * title) : TNamed(name, title) {
   fMaterials.Clear();
   fMaterials.SetOwner(true);
   fNMaterials = 0;
   fPackingFraction = 1.0;

   auto * vacuum_mat  = new TGeoMaterial("vacuum_mat",0,0,0);
   auto   * vacuum_med  = new TGeoMedium("vacuum_med",0,vacuum_mat);
   //TGeoBBox     * box         = new TGeoBBox(100,100,100); // 2x2x2 m^3
   fTopVolume                 =  gGeoManager->MakeBox("Top",vacuum_med,100.,100.,100.);//new TGeoVolume("topVolume",box, vacuum_med);
   gGeoManager->SetTopVolume(fTopVolume);
   //fBremRadiator = new InSANEBremsstrahlungRadiator();
}
//_______________________________________________________________________________
InSANETarget::~InSANETarget(){
}
//______________________________________________________________________________
InSANETarget::InSANETarget(const InSANETarget & tg) : TNamed(tg) {
   fMaterials.AddAll( &(tg.fMaterials));
   fMaterials.SetOwner(true);
   fNMaterials  = tg.fNMaterials;
}
//______________________________________________________________________________
InSANETarget::InSANETarget(const InSANETarget * tg) : TNamed(*tg) {
   // should do a deep copy?
   fMaterials.AddAll( &(tg->fMaterials));
   fMaterials.SetOwner(true);
   fNMaterials  = tg->fNMaterials;
}
//________________________________________________________________________________
InSANETarget& InSANETarget::operator=(const InSANETarget& tg){
   if (this != &tg) {
      fMaterials.AddAll( &(tg.fMaterials));
      fMaterials.SetOwner(true);
      fNMaterials  = tg.fNMaterials;
   }
   return *this;
}
//_______________________________________________________________________________
void InSANETarget::InitFromFile(){
   //fBremRadiator = new InSANEBremsstrahlungRadiator();
   //for (int i = 0; i < fMaterials.GetEntries(); i++) {
   //   InSANETargetMaterial* mat = GetMaterial(i);
   //   fBremRadiator->AddRadiator(mat);
   //}
}
//_______________________________________________________________________________
void InSANETarget::Print(const Option_t * opt ) const {
   std::cout << "Target : " << GetTitle() << "  (" << GetName() << ")" << std::endl;
   Double_t rl_tot=0.0;
   for (int i = 0; i < fMaterials.GetEntries(); i++) {
      InSANETargetMaterial* mat = GetMaterial(i);
      mat->Print();
      rl_tot += mat->GetNumberOfRadiationLengths();
   }
   std::cout << " Total thickness = " << rl_tot << " (radiation lengths)" << std::endl;
   fBremRadiator.Print();

}
//_______________________________________________________________________________

void InSANETarget::Print(std::ostream &stream) const 
{
   stream << "Target : " << GetTitle() << "  (" << GetName() << ")" << std::endl;
   Double_t rl_tot=0.0;
   for (int i = 0; i < fMaterials.GetEntries(); i++) {
      InSANETargetMaterial* mat = GetMaterial(i);
      mat->Print(stream);
      rl_tot += mat->GetNumberOfRadiationLengths();
   }
   stream << " Total thickness = " << rl_tot << " (radiation lengths)" << std::endl;
   fBremRadiator.Print(stream);

}
//_______________________________________________________________________________
void InSANETarget::AddMaterial(InSANETargetMaterial * mat) {
   mat->Update();
   mat->SetMatID(fNMaterials);
   fMaterials.Add(mat);
   fNMaterials++;
   fBremRadiator.AddRadiator(mat);
}
//______________________________________________________________________________
InSANETargetMaterial * InSANETarget::GetMaterial(const char * name) const {
   return (InSANETargetMaterial*)(fMaterials.FindObject(name)) ;
}
//______________________________________________________________________________
InSANETargetMaterial * InSANETarget::GetMaterial(Int_t i) const {
   if (i >= GetNMaterials())  {
      Error("GetMaterial(Int_t i)","Material number i=%d too large. There are %d materials for this target.",i,GetNMaterials());
      return(nullptr);
   }
   return (InSANETargetMaterial*)(fMaterials.At(i)) ;
}
//______________________________________________________________________________
Double_t InSANETarget::GetAttenuationLength() const {
   Double_t res = 0;
   for(int i = 0; i<GetNMaterials() ; i++) {
      res += GetMaterial(i)->GetAttenuationLength() ; 
   }
   return(res);
}
//______________________________________________________________________________
Double_t InSANETarget::GetRadiationLength() const {
   //Double_t l0         = 0.0
   Double_t lrho0      = 0.0; // sum L_i*rho_i
   Double_t lrhoOverX0 = 0.0; // sum L_i*rho_i/X_i = L_0*rho_0/X_0, where X_0 is the total target radiation length
   for(int i = 0; i<GetNMaterials() ; i++) {
      Double_t li    = GetMaterial(i)->GetLength();
      Double_t rhoi  = GetMaterial(i)->GetDensity();
      Double_t lrhoi = li*rhoi;
      Double_t Xi    = GetMaterial(i)->GetRadiationLength();
      lrho0         += lrhoi;
      lrhoOverX0    += lrhoi/Xi;
      //Double_t l = GetMaterial(i)->GetAttenuationLength();
      //res += (1.0 - TMath::Exp(-1.0*GetMaterial(i)->GetLength()/l)) ; 
   }
   return lrho0/lrhoOverX0;
}
//______________________________________________________________________________
Double_t InSANETarget::GetLengthDensity() const {
   Double_t tot = 0.0; 
   for(int i = 0; i<GetNMaterials() ; i++) {
      tot += GetMaterial(i)->GetLengthDensity();
   }
   return(tot);
}
//______________________________________________________________________________
Double_t InSANETarget::GetNumberOfRadiationLengths() const { //Double_t l0         = 0.0
   //Double_t lrho0      = 0.0; // sum L_i*rho_i
   Double_t lrhoOverX0 = 0.0; // sum L_i*rho_i/X_i = L_0*rho_0/X_0, where X_0 is the total target radiation length
   for(int i = 0; i<GetNMaterials() ; i++) {
      //Double_t l0   += GetMaterial(i)->GetLength();
      //Double_t lrhoi = GetMaterial(i)->GetLengthDensity();
      Double_t li    = GetMaterial(i)->GetLength();
      Double_t rhoi  = GetMaterial(i)->GetDensity();
      Double_t lrhoi = li*rhoi;
      Double_t Xi    = GetMaterial(i)->GetRadiationLength();
      //lrho0         += lrhoi;
      lrhoOverX0    += lrhoi/Xi;
      //Double_t l = GetMaterial(i)->GetAttenuationLength();
      //res += (1.0 - TMath::Exp(-1.0*GetMaterial(i)->GetLength()/l)) ; 
   }
   //Double_t res = lrho0/lrhoOverX0;
   return(lrhoOverX0);
}
//______________________________________________________________________________
void     InSANETarget::DrawTarget(Option_t * opt) {

   for(int i = 0; i<GetNMaterials() ; i++) {
      InSANETargetMaterial * mat = GetMaterial(i);
      fTopVolume->AddNodeOverlap(mat->fTGeoVolume,1,mat->fTGeoMatrix);
   }

   gGeoManager->CloseGeometry();
   gGeoManager->SetTopVisible(0);
   fTopVolume->Draw(opt);

}
//______________________________________________________________________________



ClassImp(InSANESimpleTarget)

//_______________________________________________________________________________
InSANESimpleTarget::InSANESimpleTarget(const char * name, const char * title) : InSANETarget(name, title) {
   DefineMaterials();
}
//_______________________________________________________________________________
InSANESimpleTarget::~InSANESimpleTarget() {
}
//_______________________________________________________________________________
void InSANESimpleTarget::DefineMaterials() {
   auto * matLH2  = new InSANETargetMaterial("LH2", "LH2", 1, 1);
   matLH2->fLength                = 1.0;      //cm
   matLH2->fZposition             = 0.0;      //cm
   matLH2->fDensity               = 0.07085;  // g/cm3
   matLH2->fIsPolarized           = false;

   this->AddMaterial(matLH2);
}
//_______________________________________________________________________________


ClassImp(InSANESimpleTargetWithWindows)

//_______________________________________________________________________________
InSANESimpleTargetWithWindows::InSANESimpleTargetWithWindows(const char * name, const char * title) : InSANETarget(name, title) {
   DefineMaterials();
}
//_______________________________________________________________________________
InSANESimpleTargetWithWindows::~InSANESimpleTargetWithWindows() {
}
//_______________________________________________________________________________
void InSANESimpleTargetWithWindows::DefineMaterials() {
   auto * matAl1 = new InSANETargetMaterial("window1", "Aluminum  upstream window", 13, 27);
   matAl1->fLength               = 0.05  ;  //cm
   matAl1->fDensity              = 2.7;              // g/cm3
   matAl1->fZposition            =-0.5-0.05/2.0 ;     //cm
   this->AddMaterial(matAl1);

   auto * matLH2  = new InSANETargetMaterial("LH2", "LH2", 1, 1);
   matLH2->fLength                = 1.0;      //cm
   matLH2->fZposition             = 0.0;      //cm
   matLH2->fDensity               = 0.07085;  // g/cm3
   matLH2->fIsPolarized           = false;
   this->AddMaterial(matLH2);

   auto * matAl2 = new InSANETargetMaterial("window2", "Aluminum  downstream window", 13, 27);
   matAl2->fLength               = 0.05  ;  //cm
   matAl2->fDensity              = 2.7;              // g/cm3
   matAl2->fZposition            = 0.5+0.05/2.0 ;     //cm
   this->AddMaterial(matAl2);

   matAl1->fTGeoVolume->SetFillColor(4);
   matAl1->fTGeoVolume->SetLineColor(4);
   matAl2->fTGeoVolume->SetFillColor(2);
   matAl2->fTGeoVolume->SetLineColor(2);

   fBremRadiator.Print();

}
//_______________________________________________________________________________
