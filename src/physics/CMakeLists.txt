set(aname physics)
set(needs_libs InSANEbase)
set(libname "InSANE${aname}")
set(dictname "${libname}Dict")
set(lib_LINKDEF "${PROJECT_SOURCE_DIR}/src/${aname}/include/${libname}_LinkDef.h")
set(lib_DICTIONARY_SRC "${libname}Dict.cxx")

# ROOT dictionary files
if(INSANE_USE_ROOT6)
   # ROOT6 generates pcm files that should install with into the lib directory
   # todo: fix the install of this
   set(lib_PCM_FILE "${PROJECT_BINARY_DIR}/src/${aname}/lib${libname}Dict_rdict.pcm")
endif(INSANE_USE_ROOT6)
if(NOT INSANE_USE_ROOT6)
   set(lib_DICTIONARY_HEADER "${libname}Dict.h")
endif(NOT INSANE_USE_ROOT6)

include_directories(${PROJECT_SOURCE_DIR}/src/${aname}/include)

# add src file names here (without the extention or src/ prefix)
set(lib_files
   #InSANEPMTResponse 
   #InSANESystemOfUnits 
   #InSANEPhysicalConstants 
   #InSANEMaterial 
   #InSANENucleus 
   #InSANEParticle
   #InSANEMathFunc 

   InSANEFortranWrappers 
   InSANELuminosity 
   InSANEYield 
   InSANEDilutionFactor 
   InSANEAcceptance 
   InSANETargetMaterial 

   InSANERadiativeCorrections 
   SANERadiativeCorrections

   #InSANEMaterial 
   #InSANENucleus 
   #InSANELuminosity 
   #InSANEDilutionFactor 
   #InSANEAcceptance 
   InSANEEventGenerator 
   InSANE_EG_Event 
   InSANETargetEventGenerator 
   #InSANETargetMaterial 
   #InSANETarget 

   InSANEBremsstrahlungRadiator
   InSANETarget 
   SANETargets 

   CrossSectionInfo
   InSANEFunctionManager 
   InSANEDiffXSec 
   InSANECompositeDiffXSec 
   InSANEInclusiveDISXSec 
   InSANEDiffXSecKinematicKey 
   InSANEGridDiffXSec 
   InSANEElectroProductionXSec 
   InSANEepElasticDiffXSec 
   InSANEMollerDiffXSec 
   InSANEInclusiveMollerDiffXSec 
   InSANEeInclusiveElasticDiffXSec
   InSANEpInclusiveElasticDiffXSec
   InSANEXSections 
   InSANEInclusiveDiffXSec 
   InSANEInclusiveBornDISXSec 
   InSANEExclusiveDiffXSec 
   MAIDKinematicKey   
   MAIDInclusiveDiffXSec   
   MAIDNucleusInclusiveDiffXSec   
   MAIDPolarizedKinematicKey   
   MAIDPolarizedTargetDiffXSec   
   MAIDExclusivePionDiffXSec   
   MAIDExclusivePionDiffXSec2   
   MAIDInclusiveElectronDiffXSec   
   MAIDInclusivePionDiffXSec   
   InSANEVirtualComptonAsymmetries   
   MAIDVirtualComptonAsymmetries   
   F1F209eInclusiveDiffXSec 
   F1F209StructureFunctions 
   F1F209QuasiElasticFormFactors 
   CTEQ6eInclusiveDiffXSec 
   NMC95StructureFunctions 
   #LHAPDFStructureFunctions 
   PolarizedDISXSec 
   QFSInclusiveDiffXSec 
   WiserXSection 
   WiserInclusivePhotoXSec
   WiserInclusiveElectroXSec
   InSANEPhotonDiffXSec
   InSANEInclusivePhotoProductionXSec
   InSANEInclusiveElectroProductionXSec
   InSANEInclusivePionPhotoProductionXSec
   InSANEInclusivePionElectroProductionXSec
   InclusiveHadronProductionXSec
   EPCVXSection 
   OARPionDiffXSec 
   BETAG4StructureFunctions 
   InSANEStructureFunctions 
   InSANECompositeStructureFunctions 
   InSANECompositePolarizedStructureFunctions 
   InSANEPolarizedStructureFunctions 
   InSANEStructureFunctionsFromPDFs 
   InSANEPolSFsFromComptonAsymmetries 
   InSANEPolarizedStructureFunctionsFromPDFs 
   InSANEPolarizedStructureFunctionsFromVCSAs 
   InSANEFormFactors 
   InSANEDipoleFormFactors 
   GalsterFormFactors 
   KellyFormFactors 
   RiordanFormFactors 
   RiordanFormFactors 
   BostedFormFactors 
   AMTFormFactors
   MSWFormFactors 
   AmrounFormFactors 
   BilenkayaFormFactors 
   InSANEBeamSpinAsymmetry 
   InSANEPhaseSpaceSampler 
   InSANEPhaseSpaceVariable 
   InSANEPhaseSpace 
   InSANEPartonDistributionFunctions 
   InSANEPolarizedPartonDistributionFunctions 
   StatisticalQuarkFits     
   StatisticalUnpolarizedPDFs 
   StatisticalPolarizedPDFs 
   Stat2015UnpolarizedPDFs 
   Stat2015PolarizedPDFs 
   BBSQuarkHelicityDistributions     
   BBSUnpolarizedPDFs 
   BBSPolarizedPDFs
   AvakianQuarkHelicityDistributions     
   AvakianUnpolarizedPDFs 
   AvakianPolarizedPDFs 
   DSSVPolarizedPDFs 
   AAC08PolarizedPDFs 
   JAM15PolarizedPDFs 
   BBPolarizedPDFs 
   JAMPolarizedPDFs 
   MHKPolarizedPDFs 
   GSPolarizedPDFs 
   DNS2005PolarizedPDFs 
   LSS2006PolarizedPDFs 
   LSS2010PolarizedPDFs 
   #LHAPDFUnpolarizedPDFs 
   #LHAPDFPolarizedPDFs 
   CTEQ6UnpolarizedPDFs 
   CJ12UnpolarizedPDFs 
   LCWFPartonDistributionFunctions 
   InSANEPolarizedCrossSectionDifference 
   InSANEAsymmetryBase 
   InSANEAsymmetriesFromStructureFunctions 
   InSANECrossSectionDifference 
   InSANEPOLRAD    
   InSANEPOLRADKinematics    
   InSANEPOLRADBornDiffXSec 
   InSANEPOLRADRadiatedDiffXSec
   InSANEPOLRADUltraRelInelasticTailDiffXSec
   InSANEPOLRADInternalPolarizedDiffXSec 
   InSANEPOLRADQuasiElasticTailDiffXSec 
   InSANEPOLRADInelasticTailDiffXSec 
   InSANEPOLRADElasticTailDiffXSec 
   InSANERADCOR 
   InSANERADCORKinematics 
   InSANERADCORRadiatedDiffXSec 
   InSANERADCORRadiatedUnpolarizedDiffXSec 
   InSANERADCORInternalUnpolarizedDiffXSec 
   InSANERADCOR2 
   InSANERadiativeTail 
   InSANEElasticRadiativeTail 
   #InSANEQuasielasticRadiativeTail 
   InSANEInelasticRadiativeTail 
   InSANEInelasticRadiativeTail2 
   InSANEPOLRADUltraRelativistic 
   #QuasiElasticInclusiveDiffXSec 
   #QEIntegral  
   LSS98QuarkHelicityDistributions
   LSS98UnpolarizedPDFs
   LSS98PolarizedPDFs
   InSANEPartonHelicityDistributions
   InSANEPartonDistributionFunctionsFromPHDs
   ABKM09UnpolarizedPDFs
   CTEQ10UnpolarizedPDFs
   MSTW08UnpolarizedPDFs
   MRST2001UnpolarizedPDFs
   MRST2002UnpolarizedPDFs
   MRST2006UnpolarizedPDFs
   InSANERadiatorBase
   InSANERadiator
   InSANEExternalRadiator
   #InSANEFermiMomentumDist 
   #InSANEWaveFunctions
   #BonnWaveFunctions
   InSANEFragmentationFunctions
   DSSFragmentationFunctions
   InSANETransverseMomentumDistributions
   MAIDExclusivePionDiffXSec3
   InSANEStrongCouplingConstant
   InSANEPhotoAbsorptionCrossSections
   MAIDPhotoAbsorptionCrossSections
   InSANEStructureFunctionsFromVPCSs
   RCeInclusiveElasticDiffXSec
   Physics
   PDFBase
   PolarizedPDFs
   UnpolarizedPDFs
   CTEQ10_UPDFs
   PPDFs
   JAM_PPDFs
   Stat2015_UPDFs
   Stat2015_PPDFs
   Twist3DistributionFunctions
   Twist4DistributionFunctions
   T3DFsFromTwist3SSFs
   JAM_T3DFs
   LCWF_T3DFs
   JAM_T4DFs
   #SFBase
   SpinStructureFunctions
   StructureFunctions
   VirtualComptonScatteringAsymmetries
   VirtualPhotoAbsorptionCrossSections
   SFsFromPDFs
   SSFsFromPDFs
   SFsFromVPACs
   SSFsFromVPACs
   F1F209_SFs
   NMC95_SFs
   #VCSABase
   VCSAsFromPDFs
   VCSAsFromSFs
   MAID_VCSAs
   MAID_VPACs
   CompositeSFs
   CompositeSSFs
)

set(header_only_files
  #VCSABase
  #VirtualComptonScatteringAsymmetries
  #VCSAsFromPDFs
  #VCSAsFromSFs
  #MAID_VCSAs
  )
set(lib_Fortran_files
   F1F209_FAST
   sane_pol 
   wiser_func 
   wiser_fit 
   qfs_func 
   qfs_sigs 
   qfs_targs 
   ppdf 
   Cteq6Pdf2010 
   CJ12pdf 
   aac08 
   LSS2006pdf_g1 
   LSS2010_pdfs 
   polnlo 
   JAMLIB
   DSSV-2008 
   mrst2001 
   mrst2002 
   mrst2006 
   partondf 
   nmc_org 
   r1998 
   epcvnew 
   epc_or_v3_funcs 
   epc_or_v3_prog 
   readgrid 
   Amroun 
   nqfs
   abkm09
   cteq10
   mstwpdf
   evolparton
   target_polarization_maid07_version2
   maid07_target_pol
   maid07_total
   maid07_total_subs
   )

set(lib_SRCS)
set(lib_HEADERS)
foreach(infileName ${lib_files})
   SET(lib_SRCS ${lib_SRCS} ${PROJECT_SOURCE_DIR}/src/${aname}/src/${infileName}.cxx)
   SET(lib_HEADERS ${lib_HEADERS} ${PROJECT_SOURCE_DIR}/src/${aname}/include/${infileName}.h)
endforeach(infileName)

set(lib_HEADER_ONLY)
foreach(infileName ${header_only_files})
  SET(lib_HEADER_ONLY ${lib_HEADERS} ${PROJECT_SOURCE_DIR}/src/${aname}/include/${infileName}.h)
endforeach(infileName)

set(lib_Fortran_SRCs)
foreach(infileName ${lib_Fortran_files})
   SET(lib_Fortran_SRCs ${lib_Fortran_SRCs} "${PROJECT_SOURCE_DIR}/src/${aname}/src/${infileName}.f")
endforeach(infileName)


#MESSAGE("(${dictname} ${lib_HEADERS} LINKDEF ${lib_LINKDEF}) ")
ROOT_GENERATE_DICTIONARY(${dictname} ${lib_HEADERS} LINKDEF ${lib_LINKDEF} OPTIONS -p)

SET(lib_HEADERS ${lib_HEADERS})
SET(lib_SRCS ${lib_Fortran_SRCs} ${lib_SRCS} ${lib_DICTIONARY_SRC})

SET(lib_VERSION "${${PROJECT_NAME}_VERSION}")
SET(lib_MAJOR_VERSION "${${PROJECT_NAME}_MAJOR_VERSION}")
SET(lib_LIBRARY_PROPERTIES 
    VERSION "${lib_VERSION}"
    SOVERSION "${lib_MAJOR_VERSION}"
    SUFFIX ".so")

ADD_CUSTOM_TARGET(${aname}_ROOTDICTS DEPENDS ${lib_SRCS} ${lib_HEADERS} ${lib_DICTIONARY_SRC} ${lib_DICTIONARY_HEADER})

add_library(${libname} SHARED ${lib_SRCS})
target_link_libraries(${libname} ${LINK_LIBRARIES} ${needs_libs})
set_target_properties(${libname} PROPERTIES ${lib_LIBRARY_PROPERTIES})
add_dependencies(${libname} ${needs_libs} ${aname}_ROOTDICTS)

install(
   TARGETS ${libname} 
   #   EXPORT ${PROJECT_NAME}Targets
   DESTINATION lib )

if(INSANE_USE_ROOT6)
   install(
   FILES ${lib_PCM_FILE} 
   DESTINATION lib )
endif(INSANE_USE_ROOT6)

file(GLOB template_headers ${PROJECT_SOURCE_DIR}/src/${aname}/include/*.hxx)
SET(lib_HEADERS ${lib_HEADERS} ${template_headers} ${lib_HEADER_ONLY})

install(
   FILES ${lib_HEADERS} 
   DESTINATION include/${PROJECT_NAME} )

#install(TARGETS foo
#  # IMPORTANT: Add the foo library to the "export-set"
#  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
#  LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib
#  PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}/foo"
#    COMPONENT dev)
