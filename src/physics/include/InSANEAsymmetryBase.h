#ifndef INSANEASYMMETRYBASE_HH
#define INSANEASYMMETRYBASE_HH 1 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include "TString.h"
#include "InSANENucleus.h"
#include "TNamed.h"

/** ABC for Asymmetries calculated from structure functions.  
 *  Contains mainly functions useful for ROOT TF1 objects. 
 */  
class InSANEAsymmetryBase : public TNamed {

   protected:
      InSANENucleus fTargetNucleus;  
      TString       fLabel;

   public: 
      InSANEAsymmetryBase(){
         fLabel         = "";
         fTargetNucleus = InSANENucleus::Proton();
      } 
      virtual ~InSANEAsymmetryBase(){} 

      void SetLabel(const char * l){ fLabel = l;}
      const char* GetLabel(){ return fLabel; }

      void SetTargetNucleus(const InSANENucleus& t){ fTargetNucleus = t;}

      virtual Double_t A1(Double_t x,Double_t Q2) = 0;
      virtual Double_t A2(Double_t x,Double_t Q2) = 0;

      double EvaluateA1n(double *x,double *p){
         return A1(x[0],p[0]);
      }
      double EvaluateA1p(double *x,double *p){
         return A1(x[0],p[0]);
      }
      double EvaluateA1d(double *x,double *p){
         return A1(x[0],p[0]);
      }
      double EvaluateA1He3(double *x,double *p){
         return A1(x[0],p[0]);
      }
      double EvaluateA2n(double *x,double *p){
         return A2(x[0],p[0]);
      }
      double EvaluateA2p(double *x,double *p){
         return A2(x[0],p[0]);
      }
      double EvaluateA2d(double *x,double *p){
         return A2(x[0],p[0]);
      }
      double EvaluateA2He3(double *x,double *p){
         return A2(x[0],p[0]);
      }

      ClassDef(InSANEAsymmetryBase,3)

};

#endif 
