#ifndef InSANEElasticRadiativeTail_HH 
#define InSANEElasticRadiativeTail_HH 1

#include "InSANERadiativeTail.h"
#include "InSANEPOLRADInternalPolarizedDiffXSec.h"

/** Class for pulling together all the pieces and calculate the full elastic radiative
 *  tail.
 *  
 */
class InSANEElasticRadiativeTail : public InSANERadiativeTail {

   protected:
      InSANEPOLRADElasticDiffXSec * fElasticDiffXSec; //-> born level cross section 

   public:
      InSANEElasticRadiativeTail();
      virtual ~InSANEElasticRadiativeTail();

      InSANEPOLRADElasticDiffXSec * GetBornXSec() { return fElasticDiffXSec; }

      void SetPolarizations(Double_t pe, Double_t pt){
         GetPOLRAD()->SetPolarizations(pe,pt,0.0);
         GetBornXSec()->GetPOLRAD()->SetPolarizations(pe,pt,0.0);
      } 


      virtual Double_t EvaluateXSec(const Double_t *x) const ;

      ClassDef(InSANEElasticRadiativeTail,1)
};

#endif

