#ifndef insane_physics_FormFactors_HH 
#define insane_physics_FormFactors_HH 

#include "InSANEMathFunc.h"
#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include "Physics.h"

namespace insane {
  namespace physics {

    /** Form Factors interface.
     */
    class FormFactors : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {

      protected:
        std::string      fLabel;

        // Storage of the Q2 values last used to compute the structure function
        // indexed by enum class insane::physics::FormFactor
        mutable std::array<std::array<double, NFormFactors>, NNuclei> fQ2_values;
        mutable std::array<std::array<double, NFormFactors>, NNuclei> fValues;
        mutable std::array<std::array<double, NFormFactors>, NNuclei> fUncertainties;

      public:
        FormFactors();
        virtual ~FormFactors();

        void        SetLabel(const char * l){ fLabel = l;}
        const char* GetLabel() const { return fLabel.c_str(); }

        double GetQSquared(std::tuple<FormFactor, Nuclei> ff) const;
        bool   IsComputed( std::tuple<FormFactor, Nuclei> ff, double Q2) const;

        void Reset();

        /** Calculate and return distribution value.
         * Returns the current value for flavor f but checks that the 
         * distributions have already been calculated at (Q2). 
         * It uses IsComputed(Q2) to do this check.
         */
        virtual double Get(double Q2, std::tuple<FormFactor, Nuclei> ff) const ;

        /** Get current distribution value.
         * Note: this method should only be used to get the stored values 
         * after Calculate(Q2) or Get(f,Q2) has been used at the desired (Q2).
         */ 
        //double  Get(std::tuple<FormFactor, Nuclei> ff) const ;

        /** Virtual method should get all values of form factors. 
         * This also sets internal values of fQsquared for use by IsComputed()
         */
        virtual double  Calculate    (double Q2, std::tuple<FormFactor, Nuclei> ff) const {return 0.0;}
        virtual double  Uncertainties(double Q2, std::tuple<FormFactor, Nuclei> ff) const {return 0.0;}

        virtual double G_E(double Q2, Nuclei target) const = 0;
        virtual double G_M(double Q2, Nuclei target) const = 0;
        virtual double G_Q(double Q2, Nuclei target) const = 0;
        virtual double G_M(double Q2, Nuclei target) const = 0;
       
        virtual double F1p(double Q2) const {return 0.0;}
        virtual double F2p(double Q2) const {return 0.0;}
        virtual double F1n(double Q2) const {return 0.0;}
        virtual double F2n(double Q2) const {return 0.0;}
        virtual double F1p_TMC(double Q2) const {return 0.0;}
        virtual double F2p_TMC(double Q2) const {return 0.0;}
        virtual double F1n_TMC(double Q2) const {return 0.0;}
        virtual double F2n_TMC(double Q2) const {return 0.0;}

        /**
         * \f$ \rho = \sqrt{1+ \frac{4M^2x^2}{Q^2}} \f$
         */
        //virtual double FL(double Q2, Nuclei target, Twist t = Twist::All) const;

        /** 
         * \f$ R = \sigma_L/\sigma_T \f$
         */
        //virtual double R(double Q2, Nuclei target, Twist t = Twist::All) const;
        //

        ClassDef(FormFactors,1)
    };
  }
}

#endif
