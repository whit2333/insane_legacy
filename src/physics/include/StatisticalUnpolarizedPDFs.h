#ifndef StatisticalUNPOLARIZEDPDFS_H
#define StatisticalUNPOLARIZEDPDFS_H 1  

#include "InSANEPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#include "TMath.h"
#include "StatisticalQuarkFits.h"

/** Statistical parton distribution functions.
 *
 * \ingroup updfs
 */
class StatisticalUnpolarizedPDFs : public InSANEPartonDistributionFunctions {

   private: 
      StatisticalQuarkFits fStatisticalFits; 

   public:

      StatisticalUnpolarizedPDFs(); 
      virtual ~StatisticalUnpolarizedPDFs(); 

      void UseQ2Interpolation(Bool_t ans=true){fStatisticalFits.UseQ2Interpolation(ans);} 

      Double_t *GetPDFs(Double_t,Double_t); 
      Double_t *GetPDFErrors(Double_t /*x*/,Double_t /*Q2*/){return fPDFErrors;}  

      ClassDef(StatisticalUnpolarizedPDFs,1)

}; 

#endif 
