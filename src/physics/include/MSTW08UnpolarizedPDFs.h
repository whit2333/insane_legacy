#ifndef MSTW08UNPOLARIZEDPDFS_H 
#define MSTW08UNPOLARIZEDPDFS_H

#include "InSANEPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#include "TMath.h"

/** MSTW08 unpolarized parton distributions. 
  * 
  * Reference: A. D. Martin, W. J. Stirling, R. S. Thorne and G. Watt,
  *            "Parton distributions for the LHC", Eur. Phys. J. C63 (2009) 189-285
  * e-print:   arXiv:0901.0002 [hep-ph]
  *
  * \ingroup updfs
  */
class MSTW08UnpolarizedPDFs: public InSANEPartonDistributionFunctions{

   private: 
      int fPDFSet; 

      void Fit(Double_t,Double_t); 

   public: 
      MSTW08UnpolarizedPDFs(int iset=0);
      virtual ~MSTW08UnpolarizedPDFs();  

      Double_t *GetPDFs(Double_t,Double_t); 
      Double_t *GetPDFErrors(Double_t,Double_t); 

      void SetGrid(int i){fPDFSet = i;} 

      ClassDef(MSTW08UnpolarizedPDFs,1)

};

#endif  
