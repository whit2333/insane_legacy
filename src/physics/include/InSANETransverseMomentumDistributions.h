#ifndef InSANETransverseMomentumDistributions_HH
#define InSANETransverseMomentumDistributions_HH

#include "TNamed.h"

/** Base class for TMDs 
 *
 * @ingroup tmds
 */
class InSANETransverseMomentumDistributions : public TNamed {

   public:
      InSANETransverseMomentumDistributions();
      virtual ~InSANETransverseMomentumDistributions();

      virtual Double_t f_u(   Double_t x, Double_t k_perp, Double_t Q2) = 0;
      virtual Double_t f_d(   Double_t x, Double_t k_perp, Double_t Q2) = 0;
      virtual Double_t f_ubar(Double_t x, Double_t k_perp, Double_t Q2) = 0;
      virtual Double_t f_dbar(Double_t x, Double_t k_perp, Double_t Q2) = 0;

   ClassDef(InSANETransverseMomentumDistributions,1)
};

typedef InSANETransverseMomentumDistributions InSANETMDs;

#endif

