#ifndef MRST2006UnpolarizedPDFs_H 
#define MRST2006UnpolarizedPDFs_H 

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include "InSANEPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"

/** MRST2006 unpolarized parton distruction functions.
 *  reference: Eur. Phys. J. C 28, 455 (2003); arXiv: 0211080 [hep-ph] 
 *
 * \ingroup updfs
 */
class MRST2006UnpolarizedPDFs: public InSANEPartonDistributionFunctions{

   private:

      int fPDFSet; 
      double fuv,fubar;
      double fdv,fdbar;
      double fstr,fsbar;
      double fchm,fbot;
      double fglu; 
 
      void Fit(Double_t,Double_t); 
      Double_t Extrapolate(Double_t,Double_t,Double_t,Double_t,Double_t);

   public:
      MRST2006UnpolarizedPDFs(int iset=0);
      ~MRST2006UnpolarizedPDFs();

      /// inputs are x and Q2. 
      Double_t * GetPDFs(Double_t,Double_t);
      Double_t *GetPDFErrors(Double_t,Double_t);

      /// for switching between PDF sets (to calculate errors on observables that are combos of the PDFs). 
      void SetGrid(int i){fPDFSet = i;}

      ClassDef(MRST2006UnpolarizedPDFs,1)
};



#endif  
