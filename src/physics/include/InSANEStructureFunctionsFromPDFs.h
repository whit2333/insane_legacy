#ifndef InSANEStructureFunctionsFromPDFs_H
#define InSANEStructureFunctionsFromPDFs_H 2 

#include "TNamed.h"
#include "TMath.h"
#include "TString.h"
#include "InSANEPhysicalConstants.h"
#include "InSANEStructureFunctions.h"
#include "InSANEPolarizedStructureFunctions.h"
#include "InSANEPartonDistributionFunctions.h"
#include "InSANEMathFunc.h"

/** ABC class for unpolarized structure functions using unpolarized parton distributions.
 *  Makes use of the naive parton model interpretation of PDFs following the convention:
 * 
 *  \f$ F_2(x) = \sum_q e_q^2 x q(x)  \f$
 *
 *  \f$ F_1(x) = \frac{F_2(x)(1+\gamma^2)}{2x(R+1)} \f$
 *
 * where \f$ R = \sigma_L/\sigma_T \f$
 * 
 * Target mass effects are included by default when calling the functions F2(x,Q2) or F1(x,Q2). 
 * They are calculated from the approximate form (Phys Rev D 84, 074008 (2011) eqn.9). If 
 * you want to just calculate the leading twist piece, use F1XNoTMC(x,Q2) instead.
 *
 * \ingroup structurefunctions
 */

class InSANEStructureFunctionsFromPDFs : public InSANEStructureFunctions {

   private:
      Bool_t fUsesR;
      Bool_t fTargetMassCorrections;
      Int_t  fNintegrate;

   protected:
      Double_t h2_TMC(Double_t xi, Double_t Q2);
      Double_t g2_TMC(Double_t xi, Double_t Q2);

   public:
      InSANEPartonDistributionFunctions * fUnpolarizedPDFs; //->

   public:
      InSANEStructureFunctionsFromPDFs(); 
      InSANEStructureFunctionsFromPDFs(InSANEPartonDistributionFunctions * pdfs);  
      virtual ~InSANEStructureFunctionsFromPDFs(); 

      /** Set whether or not to use R fit incalculating F1.
       *  By default it is false. Turning it off sets R=0.
       */
      void    SetUseR(Bool_t v = true){fUsesR=v; }//if(v) fTargetMassCorrections = (!v);}
      Bool_t  GetUseR() const {return fUsesR;}

      void    CalculateTMCs(Bool_t v = true){fTargetMassCorrections=v;}// if(v) fUsesR = (!v);}
      Bool_t  TargetMassCorrected() const {return fTargetMassCorrections;}

      virtual Double_t R(Double_t x, Double_t Qsq);

      void SetUnpolarizedPDFs(InSANEPartonDistributionFunctions * pdfs);
      InSANEPartonDistributionFunctions *  GetUnpolarizedPDFs();

      virtual Double_t F2p(      Double_t x, Double_t Qsq);
      virtual Double_t F2pNoTMC( Double_t x, Double_t Qsq);
      virtual Double_t F1p(      Double_t x, Double_t Qsq);
      virtual Double_t F1pNoTMC( Double_t x, Double_t Qsq);

      virtual Double_t F2n(      Double_t x, Double_t Qsq);
      virtual Double_t F2nNoTMC( Double_t x, Double_t Qsq);
      virtual Double_t F1n(      Double_t x, Double_t Qsq);
      virtual Double_t F1nNoTMC( Double_t x, Double_t Qsq);

      virtual Double_t F1d(   Double_t x, Double_t Qsq);
      virtual Double_t F2d(   Double_t x, Double_t Qsq);

      virtual Double_t F1He3( Double_t x, Double_t Qsq);
      virtual Double_t F2He3( Double_t x, Double_t Qsq);

      virtual Double_t F1p_Error(   Double_t x, Double_t Q2);
      virtual Double_t F2p_Error(   Double_t x, Double_t Q2);
      virtual Double_t F1n_Error(   Double_t x, Double_t Q2);
      virtual Double_t F2n_Error(   Double_t x, Double_t Q2);
      virtual Double_t F1d_Error(   Double_t x, Double_t Q2);
      virtual Double_t F2d_Error(   Double_t x, Double_t Q2);
      virtual Double_t F1He3_Error( Double_t x, Double_t Q2);
      virtual Double_t F2He3_Error( Double_t x, Double_t Q2);

      virtual Double_t F1pNoTMC_Error(   Double_t x, Double_t Q2);
      virtual Double_t F2pNoTMC_Error(   Double_t x, Double_t Q2);
      virtual Double_t F1nNoTMC_Error(   Double_t x, Double_t Q2);
      virtual Double_t F2nNoTMC_Error(   Double_t x, Double_t Q2);
      virtual Double_t F1dNoTMC_Error(   Double_t x, Double_t Q2);
      virtual Double_t F2dNoTMC_Error(   Double_t x, Double_t Q2);
      virtual Double_t F1He3NoTMC_Error( Double_t x, Double_t Q2);
      virtual Double_t F2He3NoTMC_Error( Double_t x, Double_t Q2);

      virtual Double_t xF1p(Double_t x, Double_t Qsq){ return( x*F1p(x,Qsq)); } 
      virtual Double_t xF2p(Double_t x, Double_t Qsq){ return( x*F2p(x,Qsq)); }
      virtual Double_t xF1n(Double_t x, Double_t Qsq) { return(x*F1n(x, Qsq)); }
      virtual Double_t xF2n(Double_t x, Double_t Qsq) { return(x*F2n(x, Qsq)); }
      virtual Double_t xF1d(Double_t x, Double_t Qsq) { return(x * F1d(x, Qsq)); }
      virtual Double_t xF2d(Double_t x, Double_t Qsq) { return(x * F2d(x, Qsq)); }
      virtual Double_t xF1He3(Double_t x, Double_t Qsq) { return(x * F1He3(x, Qsq)); }
      virtual Double_t xF2He3(Double_t x, Double_t Qsq) { return(x * F2He3(x, Qsq)); }

      double  EvaluateF1pNoTMC(double *x, double *p) {  return(F1pNoTMC(x[0], p[0])); }
      double  EvaluatexF1pNoTMC(double *x, double *p) { return(x[0]*F1pNoTMC(x[0], p[0])); }

      virtual Double_t F2Nuclear(  Double_t x, Double_t Qsq, Double_t Z, Double_t A);
      virtual Double_t F1Nuclear(  Double_t x, Double_t Qsq, Double_t Z, Double_t A);
      
      ClassDef(InSANEStructureFunctionsFromPDFs,2)
};


#endif
