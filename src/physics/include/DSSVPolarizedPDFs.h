#ifndef DSSVPOLARIZEDPDFS_H 
#define DSSVPOLARIZEDPDFS_H 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <cmath> 
#include "InSANEPolarizedPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"



/** DSSV Polarized parton distruction functions.
 *
 * \ingroup ppdfs
 */
class DSSVPolarizedPDFs: public InSANEPolarizedPartonDistributionFunctions{

   private:
      // quark distributions  
      // NOTE: In the class InSANEPolarizedPDFs, the labeling in the array fPDFValues is: 
      //  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$
      double fduv;
      double fdubar;  // delta u, u-bar (valence) 
      double fddv;
      double fddbar;  // delta d, d-bar (valence)  
      double fdstr;
      double fdglu;  // delta s = s-bar, delta g 

      //void Fit(Double_t x,Double_t Q2);
      Double_t Extrapolate(Double_t,Double_t,Double_t,Double_t,Double_t); 

   protected:

      int fiSet;

   public: 
      DSSVPolarizedPDFs(int iset=0);
      virtual ~DSSVPolarizedPDFs();

      virtual Double_t * GetPDFs(     Double_t x, Double_t Q2);  
      virtual Double_t * GetPDFErrors(Double_t x, Double_t Q2);

      /// for switching between PDF sets (to calculate errors on observables that are combos of the PDFs). 
      void SetGrid(int i){fiSet=i;dssvini_(&fiSet);}

      ClassDef(DSSVPolarizedPDFs,1)
};

#endif 
