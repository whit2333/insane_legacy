#ifndef AvakianPolarizedPDFs_H
#define AvakianPolarizedPDFs_H

#include "AvakianQuarkHelicityDistributions.h"
#include "InSANEPolarizedPartonDistributionFunctions.h"

/** Avakian polarized parton distribution functions.  
  * A fit from H. Avakian, S. Brodsky, A. Deur and F. Yuan
  *
  * Paper reference: Phys. Rev. Lett. 99, 082001 (2007)  
  * 
  * \ingroup ppdfs
  */
class AvakianPolarizedPDFs: public InSANEPolarizedPartonDistributionFunctions{

   private: 
      AvakianQuarkHelicityDistributions fqhd; 

   public: 
      AvakianPolarizedPDFs();
      virtual ~AvakianPolarizedPDFs();

      Double_t *GetPDFs(Double_t,Double_t); 
      Double_t *GetPDFErrors(Double_t /*x*/,Double_t /*Q2*/){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

      ClassDef(AvakianPolarizedPDFs,1) 

};

#endif 
