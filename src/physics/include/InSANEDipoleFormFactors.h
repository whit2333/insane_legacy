#ifndef InSANEDipoleFormFactors_HH
#define InSANEDipoleFormFactors_HH 1

#include "InSANEFormFactors.h"

/** Simple Dipole Form Factors
 *
 * \ingroup formfactors
 */
class InSANEDipoleFormFactors : public InSANEFormFactors {
   protected:
      Double_t fDipoleFitParameter;

   public:
      InSANEDipoleFormFactors() {
         SetNameTitle("InSANEDipoleFormFactors","Dipole Form Factors");
         SetLabel("Dipole FFs");
         fDipoleFitParameter = 0.71; //GeV^2
      }

      virtual ~InSANEDipoleFormFactors() { }


      /** \f$ G_D(Q^2) = \frac{1}{(1+ \frac{Q^2}{0.71 GeV^2} )^2} \f$ */
      Double_t DipoleFormFactor(Double_t Qsquared) {
         return(1.0 / TMath::Power(1.0 + Qsquared / fDipoleFitParameter , 2));
      }

      // proton
      /** \f$ G_{Ep}(Q^2) = G_D(Q^2) \f$ */
      virtual Double_t GEp(Double_t Qsq) {
         return(DipoleFormFactor(Qsq));
      }

      /** \f$ \frac{G_{Mp}(Q^2)}{\mu_p} = G_D(Q^2) \f$ */
      virtual Double_t GMp(Double_t Qsq) {
         return(Mu_p * DipoleFormFactor(Qsq));
      }

      // neutron
      virtual Double_t GEn(Double_t /*Qsq*/) {
         return(0.0);
      }
      virtual Double_t GMn(Double_t Qsq) {
         return(Mu_n * DipoleFormFactor(Qsq));
      }

      // Deuteron
      virtual Double_t GEd(Double_t Qsq) {
         return(DipoleFormFactor(Qsq));
      }
      virtual Double_t GMd(Double_t Qsq) {
         return(Mu_d * DipoleFormFactor(Qsq));
      }

      // Helium-3 
      virtual Double_t GEHe3(Double_t /*Qsq*/) {
         return(0.0);
      }
      virtual Double_t GMHe3(Double_t /*Qsq*/) {
         return(0.0);
      }

      // Helium-4 
      virtual Double_t FCHe4(Double_t Qsq) {
         double a = 0.316;
         double b = 0.681;
         double q2 = Qsq/(hbarc_gev_fm*hbarc_gev_fm);
         double res = (1.0 - TMath::Power(a*a*q2,6.0))*TMath::Exp(-b*b*q2);
         return res;
      }
      ClassDef(InSANEDipoleFormFactors, 1)
};

#endif

