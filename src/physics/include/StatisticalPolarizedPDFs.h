#ifndef StatisticalPOLARIZEDPDFS_H 
#define StatisticalPOLARIZEDPDFS_H 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <cmath> 
#include "InSANEPolarizedPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#include "StatisticalQuarkFits.h"

/** Statistical Polarized parton distruction functions.
 *
 * \ingroup ppdfs
 */
class StatisticalPolarizedPDFs: public InSANEPolarizedPartonDistributionFunctions{

   private:
      /// quark distributions  
      /// NOTE: In the class InSANEPolarizedPDFs, the labeling in the array fPDFValues is: 
      ///  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$
      Double_t fdu,fdubar;        ///< delta u, u-bar (valence) 
      Double_t fdd,fddbar;        ///< delta d, d-bar (valence)  
      Double_t fds,fdsbar,fdglu;  ///< delta s, s-bar, delta g 

      StatisticalQuarkFits fStatisticalFits; 

      void Init();
      void FormPDFs(Double_t,Double_t);           // input is x, Q2 ONLY 

   public: 
      StatisticalPolarizedPDFs();
      ~StatisticalPolarizedPDFs();

      void UseQ2Interpolation(Bool_t ans=true){fStatisticalFits.UseQ2Interpolation(ans);} 

      Double_t * GetPDFs(Double_t x,Double_t Qsq){ FormPDFs(x,Qsq); return (fPDFValues); } 
      Double_t *GetPDFErrors(Double_t /*x*/,Double_t /*Q2*/){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

      ClassDef(StatisticalPolarizedPDFs,1)
};

#endif 
