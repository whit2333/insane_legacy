#ifndef InSANEPartonHelicityDistribtuions_HH
#define InSANEPartonHelicityDistribtuions_HH 1

#include "TObject.h"

/** Parton Helicity Distribution Pair.*/
struct InSANEPHDSet {
   Double_t fQ_Plus;
   Double_t fQ_Minus;
   Double_t fQbar_Plus;
   Double_t fQbar_Minus;
};

/** Parton (or quark) Helicity Distributions of the nucleon. 
 *
 */
class InSANEPartonHelicityDistributions {

   private:

   protected:
      InSANEPHDSet fU;
      InSANEPHDSet fD;
      InSANEPHDSet fS;
      //InSANEPHDSet fC;
      //InSANEPHDSet fB;
      //InSANEPHDSet fT;
      InSANEPHDSet fG;


   public:
      InSANEPartonHelicityDistributions();
      virtual ~InSANEPartonHelicityDistributions();

      const InSANEPHDSet& GetU(Double_t x, Double_t Q2) { CalculateDistributions(x,Q2); return fU;}
      const InSANEPHDSet& GetD(Double_t x, Double_t Q2) { CalculateDistributions(x,Q2); return fD;}
      const InSANEPHDSet& GetS(Double_t x, Double_t Q2) { CalculateDistributions(x,Q2); return fS;} 
      const InSANEPHDSet& GetG(Double_t x, Double_t Q2) { CalculateDistributions(x,Q2); return fG;} 

      /** Pure virtual method that must be implemented. 
       *  It should calculate all of the distributions.
       */
      virtual Int_t CalculateDistributions(Double_t x, Double_t Q2) = 0;


   ClassDef(InSANEPartonHelicityDistributions,1)
}; 

#endif

