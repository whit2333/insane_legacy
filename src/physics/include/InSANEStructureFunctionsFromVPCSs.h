#ifndef InSANEStructureFunctionsFromVPCSs_HH
#define InSANEStructureFunctionsFromVPCSs_HH

#include "InSANEStructureFunctions.h"
#include "InSANEPhotoAbsorptionCrossSections.h"

class InSANEStructureFunctionsFromVPCSs : public InSANEStructureFunctions {

   protected:

      InSANEPhotoAbsorptionCrossSections * fVPCSs;

      Double_t   f4piAlphaOverM;

   public:
      InSANEStructureFunctionsFromVPCSs();
      virtual ~InSANEStructureFunctionsFromVPCSs();

      virtual Double_t F1p(   Double_t x, Double_t Q2);
      virtual Double_t F2p(   Double_t x, Double_t Q2);
      virtual Double_t F1n(   Double_t x, Double_t Q2);
      virtual Double_t F2n(   Double_t x, Double_t Q2);
      virtual Double_t F1d(   Double_t x, Double_t Q2);
      virtual Double_t F2d(   Double_t x, Double_t Q2);
      virtual Double_t F1He3( Double_t x, Double_t Q2);
      virtual Double_t F2He3( Double_t x, Double_t Q2);

      void SetVPCSs(InSANEPhotoAbsorptionCrossSections * v) { fVPCSs = v; }
      InSANEPhotoAbsorptionCrossSections * GetVPCSs() { return fVPCSs; }

   ClassDef(InSANEStructureFunctionsFromVPCSs,1)
};


#endif

