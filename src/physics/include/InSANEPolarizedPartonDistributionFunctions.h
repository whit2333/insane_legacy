#ifndef InSANEPolarizedPartonDistributionFunctions_HH
#define InSANEPolarizedPartonDistributionFunctions_HH

#include "InSANEPartonDistributionFunctions.h"
#include "TH1F.h"


/**  ABC for Polarized Parton Distributions
 *
 *  By default polarized charm quark distributions return zero.
 *
 * \ingroup partondistributions
 */
class InSANEPolarizedPartonDistributionFunctions : public InSANEPDFBase {

   protected:
      Int_t fNintegrate;  // number of divisions for doing WW integrals

      //double   fTwist3PDFValues[NPARTONDISTS];  /// f(x), PDF not multiplied by x!
      double fxDu_Twist3 = 0.0;
      double fxDd_Twist3 = 0.0;

      double fxHu_Twist4 = 0.0;
      double fxHd_Twist4 = 0.0;

      double fxHp_Twist4 = 0.0;
      double fxHn_Twist4 = 0.0;

   public:
      InSANEPolarizedPartonDistributionFunctions();
      virtual ~InSANEPolarizedPartonDistributionFunctions();

      /** @name Simple PDF access
       *  After calling GetPDFs(x,Qsq), the following return the values of the PDFs for
       *  those values of x and Qsq.
       *  The quark labeling for fPDFValues is
       *  \f$ (0,1,2,3,4,5,6,7,,8,9,10,11) = 
       *  (\Delta u ,\Delta d,\Delta s,\Delta c,\Delta b,\Delta t,\Delta g,\Delta \bar{u},\Delta \bar{d},\Delta \bar{s},\Delta \bar{c},\Delta \bar{b},\Delta \bar{t}) \f$
       *  @{
       */
      Double_t Deltau()    const ;
      Double_t Deltad()    const ;
      Double_t Deltas()    const ;
      Double_t Deltac()    const ;
      Double_t Deltab()    const ;
      Double_t Deltat()    const ;
      Double_t Deltag()    const ;
      Double_t Deltaubar() const ;
      Double_t Deltadbar() const ;
      Double_t Deltasbar() const ;
      Double_t Deltacbar() const ;
      Double_t Deltabbar() const ;
      Double_t Deltatbar() const ;
      // Errors 
      Double_t DeltauError()    const ;
      Double_t DeltadError()    const ;
      Double_t DeltasError()    const ;
      Double_t DeltacError()    const ;
      Double_t DeltabError()    const ;
      Double_t DeltatError()    const ;
      Double_t DeltagError()    const ;
      Double_t DeltaubarError() const ;
      Double_t DeltadbarError() const ;
      Double_t DeltasbarError() const ;
      Double_t DeltacbarError() const ;
      Double_t DeltabbarError() const ;
      Double_t DeltatbarError() const ;

      //@}

      /** \f$ \Delta u(x) \f$
       *
       *  Note that calling GetPDFs calculates all pdfs at the same time
       *  so using many times in a row for the same x and Qsq is
       *  redundant and wastes time.
       */
      virtual Double_t Deltau(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltauBar( Double_t x, Double_t Qsq) ;
      virtual Double_t Deltad(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltadBar( Double_t x, Double_t Qsq) ;
      virtual Double_t Deltas(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltasBar( Double_t x, Double_t Qsq) ;
      virtual Double_t Deltag(    Double_t x, Double_t Qsq) ;
      virtual Double_t Deltac(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltacBar( Double_t x, Double_t Qsq) ;
      virtual Double_t Deltab(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltabBar( Double_t x, Double_t Qsq) ;
      virtual Double_t Deltat(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltatBar( Double_t x, Double_t Qsq) ;

      virtual Double_t DeltauError(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltauBarError( Double_t x, Double_t Qsq) ;
      virtual Double_t DeltadError(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltadBarError( Double_t x, Double_t Qsq) ;
      virtual Double_t DeltasError(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltasBarError( Double_t x, Double_t Qsq) ;
      virtual Double_t DeltacError(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltacBarError( Double_t x, Double_t Qsq) ;
      virtual Double_t DeltabError(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltabBarError( Double_t x, Double_t Qsq) ;
      virtual Double_t DeltatError(    Double_t x, Double_t Qsq) ;
      virtual Double_t DeltatBarError( Double_t x, Double_t Qsq) ;

      // Twist 3 quark distribution functions
      virtual Double_t D_u(Double_t,Double_t)   ;
      virtual Double_t D_d(Double_t,Double_t)   ;
      virtual Double_t D_s(Double_t,Double_t)   { return 0.0; }
      virtual Double_t D_ubar(Double_t,Double_t){ return 0.0; }
      virtual Double_t D_dbar(Double_t,Double_t){ return 0.0; }
      virtual Double_t D_sbar(Double_t,Double_t){ return 0.0; }

      // Twist 3 quark-gluon distribution functions
      virtual Double_t Dp_Twist3(Double_t,Double_t)   ;
      virtual Double_t Dn_Twist3(Double_t,Double_t)   ;
      virtual Double_t Dd_Twist3(Double_t,Double_t)   { return 0.0; }
      virtual Double_t DHe3_Twist3(Double_t,Double_t) { return 0.0; }

      //  x^2 moment ofTwist 3 quark-gluon distribution functions
      virtual Double_t Moment_Dp_Twist3(int n, Double_t Q2, Double_t x1 = 0.1, Double_t x2 = 0.99)   ;

      // Twist 4 quark-gluon distribution functions
      virtual Double_t Hp_Twist4(Double_t,Double_t)   { return 0.0; }
      virtual Double_t Hn_Twist4(Double_t,Double_t)   { return 0.0; }
      virtual Double_t Hd_Twist4(Double_t,Double_t)   { return 0.0; }
      virtual Double_t HHe3_Twist4(Double_t,Double_t) { return 0.0; }

   public:

      // ------------------------------------
      // Wandzura Wilczek Relation (Twist-2)
      //
      virtual Double_t g2pWW(   Double_t , Double_t );
      virtual Double_t g2nWW(   Double_t , Double_t );
      virtual Double_t g2dWW(   Double_t , Double_t );
      virtual Double_t g2He3WW( Double_t , Double_t );

      // ------------------------------------
      // Blumlein Tkabladze Relation (Twist-3)
      //
      virtual Double_t g1p_BT_Twist3(   Double_t , Double_t );
      virtual Double_t g1n_BT_Twist3(   Double_t , Double_t );
      virtual Double_t g1d_BT_Twist3(   Double_t , Double_t );
      virtual Double_t g1He3_BT_Twist3( Double_t , Double_t );

      virtual Double_t Dp_BT(Double_t x, Double_t Q2);

      // ------------------------------------
      // Twist expansion with target mass M=0
      // ------------------------------------
      // Twist 2
      // g1 twist-2  M=0 (no TMC)
      virtual Double_t g1p_Twist2(Double_t   x, Double_t Q2) ;
      virtual Double_t g1n_Twist2(Double_t   x, Double_t Q2) ;
      virtual Double_t g1d_Twist2(Double_t   x, Double_t Q2) ;
      virtual Double_t g1He3_Twist2(Double_t x, Double_t Q2) ;

      // g2 twist 2 : g2WW
      virtual Double_t g2p_Twist2(Double_t x,Double_t Q2)  { return g2pWW(x,Q2); }
      virtual Double_t g2n_Twist2(Double_t x,Double_t Q2)  { return g2nWW(x,Q2); }
      virtual Double_t g2d_Twist2(Double_t x,Double_t Q2)  { return g2dWW(x,Q2); }
      virtual Double_t g2He3_Twist2(Double_t x,Double_t Q2){ return g2He3WW(x,Q2); }

      // ------------------------------------
      // Twist 3
      // g1 twist 3 - 
      virtual Double_t g1p_Twist3(  Double_t, Double_t){ return 0.0;}
      virtual Double_t g1n_Twist3(  Double_t, Double_t){ return 0.0;}
      virtual Double_t g1d_Twist3(  Double_t, Double_t){ return 0.0;}
      virtual Double_t g1He3_Twist3(Double_t, Double_t){ return 0.0;}

      // g2 twist 3 
      virtual Double_t g2p_Twist3(  Double_t, Double_t);
      virtual Double_t g2n_Twist3(  Double_t, Double_t);
      virtual Double_t g2d_Twist3(  Double_t, Double_t);
      virtual Double_t g2He3_Twist3(Double_t, Double_t);

      // ------------------------------------
      // Twist 4
      // g1 twist 4 - typically takes the form h(x)/Q^2
      virtual Double_t g1p_Twist4(  Double_t x, Double_t Q2) {return Hp_Twist4(x,Q2)/Q2;}
      virtual Double_t g1n_Twist4(  Double_t x, Double_t Q2) {return Hn_Twist4(x,Q2)/Q2;}
      virtual Double_t g1d_Twist4(  Double_t x, Double_t Q2) {return Hd_Twist4(x,Q2)/Q2;}
      virtual Double_t g1He3_Twist4(Double_t x, Double_t Q2) {return HHe3_Twist4(x,Q2)/Q2;}

      // g2 twist 4 - typically takes the form h(x)/Q^2
      virtual Double_t g2p_Twist4(Double_t,Double_t)  {return 0.0;}
      virtual Double_t g2n_Twist4(Double_t,Double_t)  {return 0.0;}
      virtual Double_t g2d_Twist4(Double_t,Double_t)  {return 0.0;}
      virtual Double_t g2He3_Twist4(Double_t,Double_t){return 0.0;}


      // ----------------------------------------------------------
      // SSFs with finite Target Mass Corrections (TMCs)
      // ----------------------------------------------------------
      // Twist 2
      // g1 twist2 + TMC
      virtual Double_t g1p_Twist2_TMC  ( Double_t x, Double_t Q2);
      virtual Double_t g1n_Twist2_TMC  ( Double_t x, Double_t Q2);
      virtual Double_t g1d_Twist2_TMC  ( Double_t x, Double_t Q2);
      virtual Double_t g1He3_Twist2_TMC( Double_t x, Double_t Q2);

      // g2 twist2 + TMC
      virtual Double_t g2p_Twist2_TMC  ( Double_t x, Double_t Q2);
      virtual Double_t g2n_Twist2_TMC  ( Double_t x, Double_t Q2) {return 0.0;}
      virtual Double_t g2d_Twist2_TMC  ( Double_t x, Double_t Q2) {return 0.0;}
      virtual Double_t g2He3_Twist2_TMC( Double_t x, Double_t Q2) {return 0.0;}

      // ------------------------------------
      // Twist 3
      // g1 twist3 + TMC
      virtual Double_t g1p_Twist3_TMC(Double_t x, Double_t Q2);
      virtual Double_t g1n_Twist3_TMC(Double_t x, Double_t Q2)   { return 0.0; }
      virtual Double_t g1d_Twist3_TMC(Double_t x, Double_t Q2)   { return 0.0; }
      virtual Double_t g1He3_Twist3_TMC(Double_t x, Double_t Q2) { return 0.0; }

      // g2 twist3 + TMC
      virtual Double_t g2p_Twist3_TMC(Double_t x, Double_t Q2);
      virtual Double_t g2n_Twist3_TMC(Double_t x, Double_t Q2)  { return 0.0; }
      virtual Double_t g2d_Twist3_TMC(Double_t x, Double_t Q2)  { return 0.0; }
      virtual Double_t g2He3_Twist3_TMC(Double_t x, Double_t Q2){ return 0.0; }

      // ------------------------------------
      // Twist 4
      // g1 twist 4 - typically takes the form h(x)/Q^2
      virtual Double_t g1p_Twist4_TMC(  Double_t x, Double_t Q2)   {return Hp_Twist4(x,Q2)/Q2;}
      virtual Double_t g1n_Twist4_TMC(  Double_t x, Double_t Q2)   {return Hn_Twist4(x,Q2)/Q2;}
      virtual Double_t g1d_Twist4_TMC(  Double_t x, Double_t Q2)   {return Hd_Twist4(x,Q2)/Q2;}
      virtual Double_t g1He3_Twist4_TMC(Double_t x, Double_t Q2) {return HHe3_Twist4(x,Q2)/Q2;}

      // g2 twist 4 - typically takes the form h(x)/Q^2
      virtual Double_t g2p_Twist4_TMC(Double_t,Double_t)  {return 0.0;}
      virtual Double_t g2n_Twist4_TMC(Double_t,Double_t)  {return 0.0;}
      virtual Double_t g2d_Twist4_TMC(Double_t,Double_t)  {return 0.0;}
      virtual Double_t g2He3_Twist4_TMC(Double_t,Double_t){return 0.0;}

      // ------------------------------------
      // Input scale functions
      // ------------------------------------
      //g1 twist 4 - typically takes the form h(x)/Q^2
      virtual Double_t g1p_Twist4_Q20(Double_t   x) {return 0;}
      virtual Double_t g1n_Twist4_Q20(Double_t   x) {return 0;}
      virtual Double_t g1d_Twist4_Q20(Double_t   x) {return 0;}
      virtual Double_t g1He3_Twist4_Q20(Double_t x) {return 0;}

      //g2 twist 3 
      virtual Double_t g2p_Twist3_Q20(Double_t   x) {return 0;}
      virtual Double_t g2n_Twist3_Q20(Double_t   x) {return 0;}
      virtual Double_t g2d_Twist3_Q20(Double_t   x) {return 0;}
      virtual Double_t g2He3_Twist3_Q20(Double_t x) {return 0;}

      //g2 twist 4 
      virtual Double_t g2p_Twist4_Q20(Double_t   x) {return 0;}
      virtual Double_t g2n_Twist4_Q20(Double_t   x) {return 0;}
      virtual Double_t g2d_Twist4_Q20(Double_t   x) {return 0;}
      virtual Double_t g2He3_Twist4_Q20(Double_t x) {return 0;}


      // useful for TMD type calculations
      Double_t gT_u_WW(    Double_t x, Double_t Q2);
      Double_t gT_d_WW(    Double_t x, Double_t Q2);
      Double_t gT_ubar_WW( Double_t x, Double_t Q2);
      Double_t gT_dbar_WW( Double_t x, Double_t Q2);

   public:

      /** @name Useful for using as ROOT functions, TF1 etc...
       *
       * @{
       */
      /** Use with TF1. The arguments are x=x[0], Q^2=p[0] */
      double EvaluatexDeltau(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltau());
      }
      double EvaluatexDeltad(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltad());
      }
      double EvaluatexDeltas(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltas());
      }
      double EvaluatexDeltag(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltag());
      }
      double EvaluatexDeltaubar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltaubar());
      }
      double EvaluatexDeltadbar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltadbar());
      }
      double EvaluatexDeltasbar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltasbar());
      }
      //@}


      /** Returns a TF1 functions for easy plotting and manipulation
       *  Argument q is the quark u=1,d=2,... gluon=6,anti-u=7, anti-d=8 ...
       *
       */
      TF1 * GetFunction(InSANEPDFBase::InSANEPartonFlavor q = InSANEPDFBase::kUP);

      void GetValues(TObject *obj, Double_t Q2, InSANEPDFBase::InSANEPartonFlavor = InSANEPDFBase::kUP );
      /** Get error band.
       *  Argument obj is assumed to be a TH1. 
       */ 
      void GetErrorBand(TObject *obj, Double_t Q2, InSANEPDFBase::InSANEPartonFlavor = InSANEPDFBase::kUP );

      /** Makes a simple plot of each parton distribution function */
      //virtual void PlotPDFs(Double_t Qsq = 5.0, Int_t log = 0);

      ClassDef(InSANEPolarizedPartonDistributionFunctions, 2)
};



#endif

