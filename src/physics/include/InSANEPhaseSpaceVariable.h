#ifndef InSANEPhaseSpaceVariable_HH
#define InSANEPhaseSpaceVariable_HH 1

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

#include "TMath.h"
#include "TNamed.h"
#include "TString.h"
#include "TList.h"

/** Base class for a phase space variable
 *
 *  The variable normalization is for FOAM. Each variable is scaled using the
 *  minima and maxima such that its scaled value falls between 0 and 1 (as required
 *  for FOAM random variables)
 *
 *  @param name The name given to refer to the variable.
 *  @param varexp The displayed expression for the varaible.
 *  This can include the ROOT style latex, eg #theta
 *
 *  \ingroup EventGen
 *  \ingroup xsections
 */
class InSANEPhaseSpaceVariable : public TNamed {
   protected:
      Bool_t            fIsModified     = true;
      Bool_t            fIsDependent    = false;
      Bool_t            fInverted       = false;
      Bool_t            fUniform        = false;
      Int_t             fiParticle      = -1;      // associated particle number (for events with multiple particles).
      Double_t          fVariableMinima = 0.0;
      Double_t          fVariableMaxima = 1.0;
      TString           fVariableUnits  = "";
      mutable Double_t  fCurrentValue   = 0.1;

   public:
      InSANEPhaseSpaceVariable(const char * name = "" , const char * varexp = "" , Double_t min = 0.0, Double_t max = 1.0);
      InSANEPhaseSpaceVariable(const InSANEPhaseSpaceVariable& rhs);

      //InSANEPhaseSpaceVariable(const InSANEPhaseSpaceVariable&) = default;               // Copy constructor
      InSANEPhaseSpaceVariable(InSANEPhaseSpaceVariable&&) = default;                    // Move constructor
      InSANEPhaseSpaceVariable& operator=(const InSANEPhaseSpaceVariable&) & = default;  // Copy assignment operator
      InSANEPhaseSpaceVariable& operator=(InSANEPhaseSpaceVariable&&) & = default;       // Move assignment operato
      virtual ~InSANEPhaseSpaceVariable();

      Double_t GetCurrentValue(){ return fCurrentValue; }
      void     SetCurrentValue(Double_t v){ fCurrentValue = v;}
 
      Double_t * GetCurrentValueAddress() { return &fCurrentValue ; }

      void  SetParticleIndex(Int_t i) { fiParticle = i; }
      Int_t GetParticleIndex() const { return fiParticle; }

      Double_t GetCentralValue() const { return((fVariableMinima + fVariableMaxima) / 2.0); }

      /** Use for azimuthal angles near the branch cut at -pi/pi */
      void SetInverted(Bool_t inv = true) { fInverted = inv; fIsModified = true; }

      void   SetUniform(Bool_t u) { fUniform = u; fIsModified = true; }
      Bool_t IsUniform() const { return fUniform; }

      void   SetDependent(Bool_t val = true) { fIsDependent = val; fIsModified = true; }
      Bool_t IsDependent() { return fIsDependent; }

      Bool_t IsModified() const { return fIsModified; }
      void   SetModified(bool val = false) { fIsModified = val; }

      /** Note that we invert the cut for azimuthal angles centered around the
       * branch cut near phi ~ -pi or pi .
       *
       * Here we use 0 < phi < 2pi
       */
      Bool_t IsInVariableRange() const {
         //Double_t min = GetMinimum();  
         //Double_t max = GetMaximum();  
         //if( !( (fCurrentValue >= min)&&(fCurrentValue <= max) ) ){
         //   std::cout << "[InSANEPhaseSpace::IsInVariableRange]: Current value = " << fCurrentValue << std::endl;
         //   std::cout << "min: " << min << std::endl;
         //   std::cout << "max: " << max << std::endl;
         //}
         if (!fInverted) {
            return((fCurrentValue <= GetMaximum()) &&
                  (fCurrentValue >= GetMinimum()));
         } /* else it is inverted (e.g. azimuthal angle near -pi/pi cut!) */
         if (fCurrentValue > 0.0) {
            return((fCurrentValue <= GetMaximum()) &&
                  (fCurrentValue >= GetMinimum()));
         }
         if (fCurrentValue < 0.0) {
            return((fCurrentValue + 2.0 * TMath::Pi() <= GetMaximum()) &&
                  (fCurrentValue + 2.0 * TMath::Pi()  >= GetMinimum())) ;
         }
         return false;
      }

      /** Returns offset needed such that 0 < (variable-offset)/scale < 1
      */
      Double_t GetNormScale() const {
         return(fVariableMaxima - GetNormOffset());
      }

      /** Returns offset needed such that 0 < (variable-offset)/scale < 1
      */
      Double_t GetNormOffset() const {
         return(fVariableMinima);
      }

      void   SetRange(double min, double max){
         if(min>max) Warning("SetRange","min is greater than max");
         SetMinimum(min);
         SetMaximum(max);
      }

      void   SetMaximum(double val) {
         fVariableMaxima = val;
         fIsModified = true;
      }
      void   SetMinimum(double val) {
         fVariableMinima = val;
         fIsModified = true;
      }
      double GetMaximum() const {
         return(fVariableMaxima);
      }
      double GetMinimum() const {
         return(fVariableMinima);
      }
      void   SetVariableUnits(const char * unit) {
         fVariableUnits = unit;
         fIsModified = true;
      }

      const char * GetVariableUnits() const {
         return(fVariableUnits.Data());
      }

      void Print(const Option_t * opt = "") const ;

      void Print(std::ostream& stream) const ;

      void PrintTable() const {
         std::cout << " " <<  GetName() << " = " << fCurrentValue << "\n";
      }


      ClassDef(InSANEPhaseSpaceVariable, 3)
};


/** A discrete phase space variable such as helicity.
 *
 *  \ingroup EventGen
 *  \ingroup xsections
 */
class InSANEDiscretePhaseSpaceVariable : public InSANEPhaseSpaceVariable {
   private:
      Int_t fNDivisions;

   public:
      InSANEDiscretePhaseSpaceVariable(const char * n = "", const char * t = "") : InSANEPhaseSpaceVariable(n,t) {
         fNDivisions = 2;
      }
      InSANEDiscretePhaseSpaceVariable(const InSANEDiscretePhaseSpaceVariable& rhs) : InSANEPhaseSpaceVariable(rhs),
         fNDivisions(rhs.fNDivisions) {}

      //InSANEPhaseSpaceVariable(const InSANEPhaseSpaceVariable&) = default;               // Copy constructor
      InSANEDiscretePhaseSpaceVariable(InSANEDiscretePhaseSpaceVariable&&) = default;                    // Move constructor
      InSANEDiscretePhaseSpaceVariable& operator=(const InSANEDiscretePhaseSpaceVariable&) & = default;  // Copy assignment operator
      InSANEDiscretePhaseSpaceVariable& operator=(InSANEDiscretePhaseSpaceVariable&&) & = default;       // Move assignment operato
      virtual ~InSANEDiscretePhaseSpaceVariable() { }

      void  SetNumberOfValues(int i) {
         fNDivisions = i;
         fIsModified = true;
      }
      Int_t GetNumberOfValues() const {
         return(fNDivisions);
      }

      /** Returns an integer value less than the set number of possible values
       *  where the range 0 to 1 is divided equally.
       *
       *  @param randomValue should be a value between 0 and 1
       */
      Int_t GetDiscreteVariable(Double_t randomValue) const {
         Double_t val = randomValue * ((Double_t)fNDivisions);
         return((Int_t)val);
      }

      ClassDef(InSANEDiscretePhaseSpaceVariable, 1)
};

#endif

