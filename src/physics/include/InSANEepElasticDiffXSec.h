#ifndef InSANEepElasticDiffXSec_H
#define InSANEepElasticDiffXSec_H 1

#include "InSANEExclusiveDiffXSec.h"
#include "InSANEInclusiveDiffXSec.h"
#include "TMath.h"
#include "InSANEFormFactors.h"
#include "TVector3.h"


/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class InSANEepElasticDiffXSec : public InSANEExclusiveDiffXSec {

   public:
      InSANEepElasticDiffXSec();
      virtual ~InSANEepElasticDiffXSec();
      virtual InSANEepElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANEepElasticDiffXSec::Clone()\n";
         auto * copy = new InSANEepElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANEepElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Virtual method that returns the calculated values of dependent variables. This method
       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
       *
       *  For example, in mott scattering (elastic) there is really only two random variables,
       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
       *  (assuming the beam energy is known).
       *
       */
      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      /**  Returns the scattered electron energy using the angle. */
      Double_t GetEPrime(const Double_t theta) const ;

      /** Evaluate Cross Section (mbarn/sr) */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      ClassDef(InSANEepElasticDiffXSec, 1)
};


#endif

