#ifndef InSANEFunctionManager_HH
#define InSANEFunctionManager_HH 1

#include <iostream>
#include "TNamed.h"
#include "TList.h"
#include "TObjString.h"

#include "InSANEFormFactors.h"
#include "InSANEStructureFunctions.h"
#include "InSANEStructureFunctionsFromPDFs.h"
#include "InSANEPolarizedStructureFunctions.h"
#include "InSANEPolarizedStructureFunctionsFromPDFs.h"


/** Singleton class which manages all fuctions
 *
 *  To get the manager you would do this:
 *  \code
 *  InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();
 *  \endcode
 *
 *  It is important that all connected functions and objects
 *  are INDEPENDENT OF RUN MANAGER state.
 *
 *  This means for certain functions which require some configuration
 *  dependent variable, e.g. beam energy, this must be set manually
 *  (maybe this would be a good analysis manager task?)
 *
 * \ingroup physics
 */
class InSANEFunctionManager : public TNamed {

   protected: 
      Double_t fBeamEnergy;

      TList   fNeuralNetworks;
      TList   fUnpolSFNames;   // Names of available SFs
      TList   fPolSFNames;     // Names of available polarized SFs
      TList   fFFNames;        // Names of FFs

      TList   fUnpolSFList;    // List of instances of SFs where the first is always returned by the GetSFs method
      TList   fPolSFList;      // List of instances of polarized SFs
      TList   fFFList;         // List of instances of FFs

      InSANEStructureFunctions          * fStructureFunctions;
      InSANEPolarizedStructureFunctions * fPolarizedStructureFunctions;
      InSANEFormFactors                 * fFormFactors;

   public :

      static InSANEFunctionManager * GetManager() {
         if (!fgInSANEFunctionManager) fgInSANEFunctionManager = new InSANEFunctionManager();
         return(fgInSANEFunctionManager);
      }
      static InSANEFunctionManager * GetInstance() {
         if (!fgInSANEFunctionManager) fgInSANEFunctionManager = new InSANEFunctionManager();
         return(fgInSANEFunctionManager);
      }


      void     SetBeamEnergy(Double_t e ) { fBeamEnergy = e; }
      Double_t GetBeamEnergy() const { return fBeamEnergy; } 

      void PrintStatus() const ;
      void PrintNameLists() const ;
      void Print(Option_t * opt = "") const ;

      // do not use the following anymore. Try to explicitly initialize functions.
      // Ideally this could go into a function or script .... 
      InSANEStructureFunctions          * CreateSFs(Int_t iSF);    /// \deprecated
      InSANEPolarizedStructureFunctions * CreatePolSFs(Int_t iSF); /// \deprecated
      InSANEFormFactors                 * CreateFFs(Int_t iSF);    /// \deprecated

      /** Get the .unpolarized structure functions.
       *  Default structure functions are F1F209.
       */
      InSANEStructureFunctions * GetStructureFunctions(); 

      /** Set the structure functions to be currently used.*/
      void SetStructureFunctions(InSANEStructureFunctions * SFs) ;

      /** Get the polarized structure functions.
       *  Default polarized structure functions are DNS2005PolarizedPDFs
       */
      InSANEPolarizedStructureFunctions * GetPolarizedStructureFunctions() ;

      /** Set the polarized structure functions to be currently used.*/
      void SetPolarizedStructureFunctions(InSANEPolarizedStructureFunctions * SFs) ;

      /** Get the object representing collectively all form factors */
      InSANEFormFactors * GetFormFactors() ;

      /** Set the form factor functions to be currently used.*/
      void SetFormFactors(InSANEFormFactors * FFs) ;

   protected:

      static InSANEFunctionManager * fgInSANEFunctionManager;

      InSANEFunctionManager();
      ~InSANEFunctionManager() ;


      ClassDef(InSANEFunctionManager,4)
};

#endif


