#ifndef InSANEInclusiveDISXSec_HH
#define InSANEInclusiveDISXSec_HH 1

#include "InSANECompositeDiffXSec.h"

/** Generic DIS cross section which uses whatever structure functions
 *  are set by the function manager.
 *
 * \ingroup inclusiveXSec
 */
class InSANEInclusiveDISXSec : public InSANECompositeDiffXSec {

   public:
      InSANEInclusiveDISXSec();
      virtual ~InSANEInclusiveDISXSec();
      virtual InSANEInclusiveDISXSec*  Clone(const char * newname) const ;
      virtual InSANEInclusiveDISXSec*  Clone() const { return( Clone("") ); } 
      void SetTargetThickness(Double_t t);

   ClassDef(InSANEInclusiveDISXSec,1)
};

#endif

