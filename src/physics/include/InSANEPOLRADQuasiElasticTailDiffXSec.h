#ifndef InSANEPOLRADQuasiElasticTailDiffXSec_HH
#define InSANEPOLRADQuasiElasticTailDiffXSec_HH 1

#include "InSANEPOLRADBornDiffXSec.h"

/**  Quasi-Elastic Radiative Taili cross section. 
 *  
 *
 * \ingroup inclusiveXSec
 */
class InSANEPOLRADQuasiElasticTailDiffXSec: public InSANEPOLRADBornDiffXSec {

   public:

      InSANEPOLRADQuasiElasticTailDiffXSec();
      virtual ~InSANEPOLRADQuasiElasticTailDiffXSec();
      virtual InSANEPOLRADQuasiElasticTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANEPOLRADQuasiElasticTailDiffXSec::Clone()\n";
         auto * copy = new InSANEPOLRADQuasiElasticTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANEPOLRADQuasiElasticTailDiffXSec*  Clone() const { return( Clone("") ); } 
      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(InSANEPOLRADQuasiElasticTailDiffXSec,1)
};
//____________________________________________________________________________________

#endif

