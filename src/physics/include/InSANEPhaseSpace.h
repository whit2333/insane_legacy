#ifndef InSANEPhaseSpace_H
#define InSANEPhaseSpace_H 1

#include "InSANEPhaseSpaceVariable.h"

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>

#include "TMath.h"
#include "TObject.h"
#include "TNamed.h"
#include "TString.h"
#include "TFoamIntegrand.h"
#include "TList.h"

/** Base for phase space of a differential cross section.
 * 
 *  \f$ \frac{d^N\sigma}{dx_1 dx_2 ... dx_N} \f$
 *
 *  \ingroup xsections
 *  \ingroup EventGen
 */
class InSANEPhaseSpace : public TObject {

   protected:
      TList             fVariables;
      Int_t             fnDim;
      Int_t             fnIndependentVars;
      Bool_t            fIsModified;
      Double_t          fScaledIntegralJacobian; // For multiplying the foam integral result to get the actual integral
                                                 // this is because foam uses variables between 0 and 1 

   public:
      InSANEPhaseSpace() {
         fVariables.SetOwner(true);
         fVariables.Clear();
         fnDim             = 0;
         fnIndependentVars = 0;
         fIsModified       = false;
         fScaledIntegralJacobian = 1.0;
      }
      InSANEPhaseSpace(const InSANEPhaseSpace& rhs) : 
         TObject(rhs), fnDim(rhs.fnDim), fnIndependentVars(rhs.fnIndependentVars), fIsModified(rhs.fIsModified)
      {
         fVariables.AddAll(&(rhs.fVariables));
      }

      virtual ~InSANEPhaseSpace() {
         // Delete the PS variables. The TList fVariables is the owner so they are deleted when cleared.
         fVariables.Clear();
      }

      //InSANEPhaseSpace(const InSANEPhaseSpace&) = default;               // Copy constructor
      InSANEPhaseSpace(InSANEPhaseSpace&&) = default;                    // Move constructor
      InSANEPhaseSpace& operator=(const InSANEPhaseSpace&) & = default;  // Copy assignment operator
      InSANEPhaseSpace& operator=(InSANEPhaseSpace&&) & = default;       // Move assignment operato
      //virtual ~InSANEPhaseSpace() { }

      void ClearVariables() {
         fVariables.Clear();
         SetModified(true);
      }

      Double_t GetScaledIntegralJacobian() const { return fScaledIntegralJacobian; }

      Int_t GetNIndependentVariables() const { return fnIndependentVars; }

      Int_t GetDimension() const {
         return(fnDim) ;
      }
      void  SetDimension(Int_t dim) {
         fnDim = dim;
         SetModified(true);
      }

      void AddVariable(InSANEPhaseSpaceVariable * var) {
         fVariables.Add(var);
         fnDim++;
         if(!var->IsDependent()) fnIndependentVars++;
         SetModified(true);
      }


      void Print(Option_t * opt = "") const ;
      void Print(std::ostream& stream) const ;

      /** Returns a list of InSANEPhaseSpaceVariable pointers.
       *  Use pIndex to select a particle's variables.
       *  By default it returns a list with all phase space variables
       */
      TList * GetVariables(Int_t pIndex = -1) {
         TList * thelist = nullptr;
         const int N  = fVariables.GetEntries();
         if (pIndex == -1) thelist = &fVariables;
         else {
            thelist = new TList();
            for (int i = 0; i < N; i++) {
               if (GetVariable(i)->GetParticleIndex() == pIndex) thelist->Add(GetVariable(i));
            }
         }
         return(thelist);
      }

      /** get the ith variable added */
      InSANEPhaseSpaceVariable* GetVariable(Int_t i) const {
         if (i < fnDim) return((InSANEPhaseSpaceVariable*)fVariables.At(i));
         else {
            std::cout << " Index " << i << " is larger than phase space dimension, " << fnDim << ". \n";
            return(nullptr);
         }
      }

      /** Get the InSANEPhaseSpaceVariable by name
       *  \note You should use theta,phi,energy,momentum,... etc when naming Phase space variables.
       *  Usually there is one particle so if it has a PS variable named "theta_e", you can
       *  just ask for "theta" and it will return the first variable to have "theta" in it.
       *  If there are two thetas then it returns the first one,
       *  and therefore you should be more explicit in your search.
       *
       */
      InSANEPhaseSpaceVariable* GetVariableWithName(const char * name) {
         const int N  = fVariables.GetEntries();
         for (int i = 0; i < N; i++) {
            TString aVarName = GetVariable(i)->GetName();
            if (aVarName.Contains(name)) return(GetVariable(i));
            /*         if( !strcmp(name,GetVariable(i)->GetName())  ) return(GetVariable(i));*/
         }
         std::cout << "  InSANEPhaseSpaceVariable named " << name << " was not found!\n";
         return(nullptr);
      }

      InSANEPhaseSpaceVariable* GetIndependentVariable(int i_ind) {
         const int N  = fVariables.GetEntries();
         int n_ind = 0;
         for (int i = 0; i < N; i++) {
            auto  var = GetVariable(i);
            if( !(var->IsDependent()) ) {
               if( n_ind == i_ind ){
                  return var;
               } 
               n_ind++;
            }
         }
         return(nullptr);
      }

      /** Called by InSANEDiffXSec::Refresh() */
      void Refresh() {
         fScaledIntegralJacobian = 1.0;
         const int N  = fVariables.GetEntries();
         for (int i = 0; i < N; i++) {
            auto * aVar  = (InSANEPhaseSpaceVariable*) GetVariable(i);
            fScaledIntegralJacobian *= aVar->GetNormScale();
            aVar->SetCurrentValue( aVar->GetCentralValue() );
            aVar->SetModified(false);
         }
      }

      Double_t GetSolidAngle(Int_t ipart = 0){
         InSANEPhaseSpaceVariable * phi   = GetVariableWithName("phi");
         InSANEPhaseSpaceVariable * theta = GetVariableWithName("theta");
         if(!phi)   return 0.0;
         if(!theta) return 0.0;
         Double_t deltaPhi   = phi->GetMaximum() - phi->GetMinimum();
         Double_t deltaTheta = -1.0*(TMath::Cos(theta->GetMaximum()) - TMath::Cos(theta->GetMinimum()));
         return deltaPhi*deltaTheta;
      }
      Double_t GetEnergyBite(Int_t ipart = 0){
         InSANEPhaseSpaceVariable * energy = GetVariableWithName("energy");
         Double_t deltaE   = energy->GetMaximum() - energy->GetMinimum();
         return deltaE;
      }

      void ListVariables()  {
         std::cout << " Variables in phase space are:\n";
         const int N = fVariables.GetEntries(); 
         for (int i = 0; i < N; i++) {
            std::cout << GetVariable(i)->GetName() << "\n";
         }
         std::cout << "\n";
      }

      /** Sets the values of each PS variable. Is called from InSANEDiffXSec::VariablesInPhaseSpace().
       *  The dimension vals should be of dimension fNParticles*3
       */
      void SetEventValues(const Double_t * vals, int nVars=0) const {
         int N = fVariables.GetEntries(); 
         if(nVars > 0) {
            N= nVars;
         }
         for (int i = 0; i < N; i++) {
            GetVariable(i)->SetCurrentValue(vals[i]);
         }
      }

      /** Returns the array filled with the current values of each phase space
       *  variable.
       */
      void GetEventValues(Double_t * vals) {
         const int N = fVariables.GetEntries();
         if(N>0){ 
            for (int i=0;i<N;i++) {
               vals[i] = GetVariable(i)->GetCurrentValue();
            }
         }else{
            std::cout << "[InSANEPhaseSpace::GetEventValues]: WARNING!  ";
            std::cout << "There seems to be no phase space variables..." << std::endl;
         }

      }

      void PrintTableVariables() {
         const int N = fVariables.GetEntries();
         for (int i = 0; i < N; i++) {
            GetVariable(i)->PrintTable();
         }
      }

      bool IsModified() const {
         return fIsModified;
      }
      void SetModified(bool val = true) {
         fIsModified = val;
      }



      ClassDef(InSANEPhaseSpace,3)
};


#endif


