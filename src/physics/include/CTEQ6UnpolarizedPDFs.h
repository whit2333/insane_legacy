#ifndef CTEQ6UNPOLARIZEDPDFS_H
#define CTEQ6UNPOLARIZEDPDFS_H 2  

#include "InSANEPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#include "TMath.h"

/** CTEQ6 parton distribution functions.
 *
 * \ingroup updfs
 */
class CTEQ6UnpolarizedPDFs : public InSANEPartonDistributionFunctions {

	public:

		CTEQ6UnpolarizedPDFs(); 
		virtual ~CTEQ6UnpolarizedPDFs(); 

		Double_t *GetPDFs(Double_t,Double_t); 
		Double_t *GetPDFErrors(Double_t /*x*/,Double_t /*Q2*/){return fPDFErrors;}  

		ClassDef(CTEQ6UnpolarizedPDFs,2)

}; 

#endif 
