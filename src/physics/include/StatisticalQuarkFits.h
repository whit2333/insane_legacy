#ifndef StatisticalQuarkFITS_H 
#define StatisticalQuarkFITS_H 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <cmath> 
#include "TMath.h"
#include "TString.h"
#include "InSANEFortranWrappers.h"

/** StatisticalQuarkFits.
 * StatisticalQuarkFits define helicity dependent quark distributions based on:
 * C. Bourrely, F. Buccella and J. Soffer, Eur. Phys. J. C 23 (2002) 487 (hep-ph/0109160)
 * C. Bourrely, F. Buccella and J. Soffer, Phys. Lett. B 648 (2007) 39 (hep-ph/0702221)
 *
 * \ingroup helicitydependentpartondistributions 
 * \ingroup StatisticalQuark
 */
class StatisticalQuarkFits {
   private:
      Bool_t fIsInterp; 
      /// Fit parameters
      /// xbar = temprature 
      /// X0ph = thermodynamical potential (p = parton, h = helicity)
      Double_t fxbar, fX0uplus, fX0uminus, fX0dplus, fX0dminus;   ///< Fermi-Dirac fit pars
      Double_t fdxbar,fdX0uplus, fdX0uminus,fdX0dplus,fdX0dminus; ///< errors
      Double_t fb, fbtilda, fAtilda;                              ///< fit parameters
      Double_t fdb, fdbtilda, fdAtilda;                           ///< errors
      Double_t fA,fAbar,fAg;                                      ///< Normalization constants
      Double_t fbbar, fk, fbg;
      Double_t fdAbar, fdbbar, fdbg;                              ///< error
      Double_t fX0splus,fX0sminus,fbs,fAtildas;                   ///< Strange quark parameters

      /// flavor = u,ubar,d,dbar,s,sbar
      /// helicity = -1 (anti-parallel) +1 (parallel) quark helicity
      /// x = x Bjorken
      /// Qsq = 4-momentum transfer Note: set to 4GeV^2 until DGLAP is ready
      /// Note quark and gluon distributions fitted as x*parton, so return x*parton/x
      Double_t fQ0sq;                                             ///< Q2 value used for quark fits
      Double_t FormQuark(TString ,Int_t , Double_t , Double_t);   ///< takes flavor,helicity,x,Q2 arguments 
      Double_t FormGluon(Double_t , Double_t );                   ///< takes helicity,x,Q2 arguments
      Double_t FormPolarizedGluon( Int_t, Double_t , Double_t ){return 0.0;} //takes helicity,x,Q2 arguments
      Double_t CheckX(Double_t );                                 ///< takes x as argument
      void FixWarning(Double_t );                                 ///< takes Q2 value

   public: 
      StatisticalQuarkFits();
      virtual ~StatisticalQuarkFits();

      void UseQ2Interpolation(Bool_t ans=true){ 
         //if(ans==true)  std::cout << "[StatisticalQuarkFits]: Will do Q2 interpolation." << std::endl; 
         //if(ans==false) std::cout << "[StatisticalQuarkFits]: Will NOT do Q2 interpolation." << std::endl; 
         fIsInterp=ans;
      }
      Bool_t IsInterpolated() const {return fIsInterp;}
      Double_t  GetUQuark( Int_t helicity, Double_t x, Double_t Qsq ){return FormQuark("u",helicity,x,Qsq);} 
      Double_t  GetAntiUQuark( Int_t helicity, Double_t x, Double_t Qsq ){return FormQuark("ubar",helicity,x,Qsq);}
      Double_t  GetDQuark( Int_t helicity, Double_t x, Double_t Qsq ){return FormQuark("d",helicity,x,Qsq);}
      Double_t  GetAntiDQuark( Int_t helicity, Double_t x, Double_t Qsq ){return FormQuark("dbar",helicity,x,Qsq);}
      Double_t  GetSQuark( Int_t helicity, Double_t x, Double_t Qsq ){return FormQuark("s",helicity,x,Qsq);}
      Double_t  GetAntiSQuark( Int_t helicity, Double_t x, Double_t Qsq ){return FormQuark("sbar",helicity,x,Qsq);} 
      Double_t  GetGluon( Double_t x, Double_t Qsq ){return FormGluon(x,Qsq);}
      Double_t  GetPolarizedGluon( Int_t helicity, Double_t x, Double_t Qsq ){return FormPolarizedGluon(helicity,x,Qsq);}
      Double_t  GetFermiDiracFunc(TString flavor, Int_t helicity, Double_t x, Double_t Qsq);

      /// These functions use the updated code from Soffer that utilizes Q2 dependence
      /// NOTE: x and Q2 are reversed in the fortran!
      ///       partondf_() returns x*p, where p is the parton distribution  
      Double_t  GetParton(double x,double Q2,int part)           {return partondf_(&Q2,&x,&part);}   
      Double_t  GetUQuarkInterp(Double_t x,Double_t Qsq)         {return (1./x)*GetParton(x,Qsq,1);} 
      Double_t  GetDQuarkInterp(Double_t x,Double_t Qsq)         {return (1./x)*GetParton(x,Qsq,2);} 
      Double_t  GetSQuarkInterp(Double_t x,Double_t Qsq)         {return (1./x)*GetParton(x,Qsq,3);} 
      Double_t  GetAntiUQuarkInterp(Double_t x,Double_t Qsq)     {return (1./x)*GetParton(x,Qsq,4);} 
      Double_t  GetAntiDQuarkInterp(Double_t x,Double_t Qsq)     {return (1./x)*GetParton(x,Qsq,5);} 
      Double_t  GetAntiSQuarkInterp(Double_t x,Double_t Qsq)     {return (1./x)*GetParton(x,Qsq,6);} 
      Double_t  GetGluonInterp(Double_t x,Double_t Qsq)          {return (1./x)*GetParton(x,Qsq,7);} 
      Double_t  GetDeltaUQuarkInterp(Double_t x,Double_t Qsq)    {return (1./x)*GetParton(x,Qsq,8);} 
      Double_t  GetDeltaDQuarkInterp(Double_t x,Double_t Qsq)    {return (1./x)*GetParton(x,Qsq,9);} 
      Double_t  GetDeltaSQuarkInterp(Double_t x,Double_t Qsq)    {return (1./x)*GetParton(x,Qsq,10);} 
      Double_t  GetDeltaAntiUQuarkInterp(Double_t x,Double_t Qsq){return (1./x)*GetParton(x,Qsq,11);} 
      Double_t  GetDeltaAntiDQuarkInterp(Double_t x,Double_t Qsq){return (1./x)*GetParton(x,Qsq,12);} 
      Double_t  GetDeltaAntiSQuarkInterp(Double_t x,Double_t Qsq){return (1./x)*GetParton(x,Qsq,13);} 
      Double_t  GetDeltaGluonInterp(Double_t x,Double_t Qsq)     {return (1./x)*GetParton(x,Qsq,14);} 
      Double_t  GetCQuarkInterp(Double_t x,Double_t Qsq)         {return (1./x)*GetParton(x,Qsq,15);} 
      Double_t  GetDeltaCQuarkInterp(Double_t x,Double_t Qsq)    {return (1./x)*GetParton(x,Qsq,16);} 

      ClassDef(StatisticalQuarkFits,1)
};

#endif 
