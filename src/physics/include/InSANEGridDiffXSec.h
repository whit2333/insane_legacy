#ifndef InSANEGridDiffXSec_hh
#define InSANEGridDiffXSec_hh

#include "InSANEInclusiveDiffXSec.h"
#include "InSANEDiffXSecKinematicKey.h"
#include "TOrdCollection.h"
#include <vector>
#include <algorithm>
#include <iostream>

class InSANEGridXSecValue : public TObject {
   public:
      Double_t fSigma;
      Double_t GetSigma(){return fSigma;}
      void     SetSigma(Double_t s) {fSigma = s;}
      InSANEGridXSecValue(Double_t sig = 0.0){fSigma = sig;}
      ~InSANEGridXSecValue(){}
   ClassDef(InSANEGridXSecValue,1);
};

class InSANEGridDiffXSec : public InSANEInclusiveDiffXSec {
   protected:
      TString                     fFileName;
      mutable TOrdCollection              fGridData;
      mutable InSANEDiffXSecKinematicKey *fKineKey;

      mutable std::vector<Double_t>    fEprime;
      mutable std::vector<Double_t>    fTheta;
      mutable std::vector<Double_t>    fPhi;

   public:
      InSANEGridDiffXSec(const char * file = "xsec_test.txt");
      virtual ~InSANEGridDiffXSec();
      //virtual InSANEGridDiffXSec*  Clone(const char * newname) const {
      //   std::cout << "InSANEGridDiffXSec::Clone()\n";
      //   InSANEGridDiffXSec * copy = new InSANEGridDiffXSec();
      //   (*copy) = (*this);
      //   return copy;
      //}
      //virtual InSANEGridDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual Double_t EvaluateXSec(const Double_t *x) const; 

      void BinarySearch(std::vector<Double_t> *array,Double_t key,Int_t &lowerbound,Int_t &upperbound) const; 
      ClassDef(InSANEGridDiffXSec,1)
};

#endif

