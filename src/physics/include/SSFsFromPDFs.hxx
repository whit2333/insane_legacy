// ------------------------------------------
namespace insane {
  namespace physics {

    template <class PDF, class...T>
    double   SSFsFromPDFs<PDF,T...>::Calculate(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const
    {
      auto sftype = std::get<SF>(sf);
      auto target = std::get<Nuclei>(sf);
      double        result = 0.0;

      switch(sftype) {

        case SF::g1 :
          if( l == OPELimit::Massless ) {
            result = insane::physics::g1(fDFs, target, x, Q2); 
          } else {
            result = insane::physics::g1_TMC(fDFs, target, x, Q2); 
          }
          break;

        case SF::g2 :
          if( l == OPELimit::Massless ) {
            result = insane::physics::g2(fDFs, target, x, Q2); 
          } else {
            result = insane::physics::g2_TMC(fDFs, target, x, Q2); 
          }
          break;

        default:
          result = 0.0;
          break;
      }
      return result;
    }
    //______________________________________________________________________________

    template <class PDF, class...T>
    double  SSFsFromPDFs<PDF,T...>::Uncertainties(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const
    {
      return 0.0;
    }
    //______________________________________________________________________________


  }
}

