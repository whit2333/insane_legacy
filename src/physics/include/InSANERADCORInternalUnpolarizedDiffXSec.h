#ifndef InSANERADCORInternalUnpolarizedDiffXSec_HH 
#define InSANERADCORInternalUnpolarizedDiffXSec_HH 1 

#include "TMath.h"
#include "InSANEInclusiveDiffXSec.h"
#include "F1F209eInclusiveDiffXSec.h"
#include "InSANERADCOR.h"
#include "InSANEFormFactors.h"
#include "MSWFormFactors.h"

/** RADCOR cross section.
 *
 *  Current Status: unknown
 *
 * \ingroup inclusiveXSec
 */
class InSANERADCORInternalUnpolarizedDiffXSec: public InSANEInclusiveDiffXSec{

	public: 
		InSANERADCORInternalUnpolarizedDiffXSec();
		virtual ~InSANERADCORInternalUnpolarizedDiffXSec();
      virtual InSANERADCORInternalUnpolarizedDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANERADCORInternalUnpolarizedDiffXSec::Clone()\n";
         auto * copy = new InSANERADCORInternalUnpolarizedDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANERADCORInternalUnpolarizedDiffXSec*  Clone() const { return( Clone("") ); } 

                Double_t EvaluateXSec(const Double_t *) const;

		InSANERADCOR *fRADCOR;

                void SetVerbosity(int v){fRADCOR->SetVerbosity(v);} 
                void SetMultiplePhoton(Int_t c){fRADCOR->SetMultiplePhoton(c);}

                // Double_t *fEprime_var; //!
                // Double_t *fTh_var;     //!
                // Double_t *fPhi_var;    //!

                // void InitializePhaseSpaceVariables(); 

                ClassDef(InSANERADCORInternalUnpolarizedDiffXSec,1) 

}; 

#endif 
