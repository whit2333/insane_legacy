#ifndef InSANE_EG_Event_HH
#define InSANE_EG_Event_HH 1

#include "InSANEParticle.h"
#include "TClonesArray.h"
#include <vector>

class InSANEDiffXSec;

/** Transitional class for varous EG output formats.
 */ 
class InSANE_EG_Event {

   public:
      //Int_t                  fEventNumber;
      //Int_t                  fRunNumber;
      Int_t                  fXS_id;      // unique number
      Double_t               fBeamPol; 
      Double_t               fTargetPol; 
      InSANEDiffXSec       * fXSec;        //!
      TClonesArray         * fParticles;  //->
      std::vector<InSANEParticle*> fArray;
      std::vector<double>    fPSVariables;

   public:
      InSANE_EG_Event();
      virtual ~InSANE_EG_Event();

      void Clear();

      InSANEParticle * GetParticle(int i = 0) ;
      void SetParticles(TList * l) ;
      void SetPSVariables(double * , int);

      InSANEParticle * GetParticle(int i) const ;
      int              GetNParticles() const ;

      ClassDef(InSANE_EG_Event,6)
};

#endif

