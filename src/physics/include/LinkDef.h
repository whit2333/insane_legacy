#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ class BETAEvent+;
#pragma link C++ class BIGCALGeometryCalculator+;
#pragma link C++ class BIGCALCluster+;
#pragma link C++ class BIGCALClusterProcessor+;
#pragma link C++ class BigcalEvent+;
#pragma link C++ class GasCherenkovEvent+;
#pragma link C++ class ForwardTrackerEvent+;
#pragma link C++ class LuciteHodoscopeEvent+;
#pragma link C++ class rawBETAEvent+;
#endif
