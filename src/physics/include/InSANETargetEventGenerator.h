#ifndef InSANETargetEventGenerator_HH
#define InSANETargetEventGenerator_HH 1

#include "InSANEEventGenerator.h"
#include "InSANETarget.h"
#include "InSANENucleus.h"
#include "InSANECompositeDiffXSec.h"
#include "WiserXSection.h"
#include "WiserInclusiveElectroXSec.h"
#include "InSANELuminosity.h"

/** Event generator which uses the defined InSANETarget to
 *  to construct material weighted cross sections and phase space
 *  samplers. 
 */
class InSANETargetEventGenerator : public InSANEEventGenerator {

   private: 

      int fNCrossSections;


   protected:
      InSANETarget     * fTarget;  //->
      TList              fXSecs;   // List of composite cross sections to create for each material
      InSANELuminosity * fLumin;   //->

   public:
      InSANETargetEventGenerator(const char * n = "eventGen", const char * t = "Event Gen");
      InSANETargetEventGenerator(const InSANETargetEventGenerator & eg);
      virtual ~InSANETargetEventGenerator();
      InSANETargetEventGenerator& operator=(const InSANETargetEventGenerator& eg);

      /** Override this instead of Initialize.  */
      virtual void InitializeMaterialXSec(const Int_t i, const Double_t weight, const InSANETargetMaterial * mat, const InSANENucleus * targ);

      virtual void Initialize();

      void Print(const Option_t * opt = "") const ;
      void Print(std::ostream& stream) const ;

      void            SetTarget(InSANETarget * t);
      InSANETarget *  GetTarget();

      /** Get the simulated time from the total number of events and beam current */
      Double_t  GetSimulatedTime(Double_t N, Double_t I) const {
         Double_t Lrho = GetLengthDensity();
         Double_t xsec = GetTotalCrossSection()*1.0e-9*1.0e-24;
         if(xsec !=0.0 && Lrho != 0.0) {
            return( (N/xsec)*(e_SI/(I*Lrho)) );
         } 
         return 0.0;
      }

      /** returns nA-seconds */
      Double_t GetSimulatedCharge(Double_t N,Double_t I) const {
         return( GetSimulatedTime(N,I)*I );
      }

      Double_t  GetLengthDensity() const { 
         if(fTarget) return fTarget->GetLengthDensity();
         /*else*/ return 0.0;
      }



   ClassDef(InSANETargetEventGenerator,1)
};


/** This class is a wrapper for the geant4 event generators.
 *
 */
class BETAG4SavedEventGenerator : public InSANETargetEventGenerator {

   protected:
      double fThetaTarget;

   public:
      int   fNumberOfGeneratedParticles;
      bool  fIsInitialized;


      double fEnergyMax;
      double fEnergyMin;
      double fMomentumMax;
      double fMomentumMin;
      double fDeltaEnergy;
      double fCentralEnergy;

      double fThetaMax;
      double fThetaMin;
      double fDeltaTheta;
      double fCentralTheta;

      double fPhiMax;
      double fPhiMin;
      double fDeltaPhi;
      double fCentralPhi;

      //G4ThreeVector * fInitialPosition ;
      //G4ThreeVector * fInitialDirection ;
      //G4ThreeVector * fMomentumVector ;

   public:

      BETAG4SavedEventGenerator(const char * n = "eventGen", const char * t = "Event Gen");
      BETAG4SavedEventGenerator(const BETAG4SavedEventGenerator & eg);
      BETAG4SavedEventGenerator(const char * name, TFile * f);
      virtual ~BETAG4SavedEventGenerator();
      BETAG4SavedEventGenerator& operator=(const BETAG4SavedEventGenerator& eg);

      Int_t SaveToFile(const char * fName = "event_generators.root", const char * n = nullptr);
      Int_t LoadFromFile(const char * fName, const char * n);

   ClassDef(BETAG4SavedEventGenerator,1)
};



#endif

