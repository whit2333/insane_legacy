#ifndef LHAPDFSTRUCTUREFUNCTIONS_HH
#define LHAPDFSTRUCTUREFUNCTIONS_HH 3 

#include "TNamed.h"
#include "TMath.h"
#include "TString.h"
#include "InSANEFortranWrappers.h"
#include "InSANEStructureFunctions.h"
#include "InSANEPartonDistributionFunctions.h"

#ifndef __CINT__
#include "LHAPDF/LHAPDF.h"
#else
namespace LHAPDF {
   enum SetType { EVOLVE = 0, LHPDF = 0, INTERPOLATE = 1, LHGRID = 1};
}
#endif

/** Concrete class for structure functions using LHAPDF
 *
 *  Default data set is MRST2004nlo
 *
 * \ingroup structurefunctions
 */
class LHAPDFStructureFunctions : public InSANEStructureFunctions {

	public:
		LHAPDFStructureFunctions(); 
		virtual ~LHAPDFStructureFunctions();

		/** wrapper to set type = 0 = LHAPDF::LHGRID
		 *   type = 1 (or nonzero) = LHAPDF::EVOLVE
		 */
		void SetPDFType(const char * pdfset, Int_t type = 0) ;

		/** Change LHAPDF data set */
		void SetPDFDataSet(const char * pdfset, LHAPDF::SetType type = LHAPDF::LHGRID, Int_t subset = 0) ;

		virtual Double_t F1p(Double_t x, Double_t Qsq);
		virtual Double_t F2p(Double_t x, Double_t Qsq);
		virtual Double_t F1n(Double_t x, Double_t Qsq);
		virtual Double_t F2n(Double_t x, Double_t Qsq);
		virtual Double_t F1d(Double_t x, Double_t Qsq);
		virtual Double_t F2d(Double_t x, Double_t Qsq);
		virtual Double_t F1He3(Double_t x, Double_t Qsq);
		virtual Double_t F2He3(Double_t x, Double_t Qsq);

		Int_t fSubset;

		ClassDef(LHAPDFStructureFunctions,3)
};

#endif
