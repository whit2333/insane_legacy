#ifndef InSANERadiativeTail_HH 
#define InSANERadiativeTail_HH 1

#include "InSANERADCOR.h"
#include "InSANEPOLRAD.h"
#include "InSANEInclusiveDiffXSec.h"

/** Base class for pulling together all the pieces and calculate a full radiative tail.
 * \deprecated
 */
class InSANERadiativeTail : public InSANEInclusiveDiffXSec {

   protected:
      mutable InSANEPOLRAD * fPOLRAD;  //->  Used to get the (internal) polarized 
      mutable InSANERADCOR * fRADCOR;  //->  Used to get the (internal and external)
      Double_t       fRadLen[2];       ///< [0] before and [1] after scattering

      void CreatePOLRAD() const ;
      void CreateRADCOR() const ;

   public:

      InSANERadiativeTail();
      virtual ~InSANERadiativeTail();

      InSANEPOLRAD * GetPOLRAD() const { if(!fPOLRAD) CreatePOLRAD(); return fPOLRAD; }
      InSANERADCOR * GetRADCOR() const { if(!fRADCOR) CreateRADCOR(); return fRADCOR; }

      /** If only one argument is given, we assume it is the total target thickness.
       * Note: tb = "target before"
       *       ta = "target after"
       */
      virtual void SetTargetThickness(Double_t tb, Double_t ta = 0.0){
         if(ta == 0.0) {
            fRadLen[0] = tb/2.0;
            fRadLen[1] = tb/2.0;
         } else {
            fRadLen[0] = tb;
            fRadLen[1] = ta;
         }
      }

      /** Set the unpolarized structure functions to be used to calculate W1,W2,F1,F2, etc. */
      void SetUnpolarizedStructureFunctions(InSANEStructureFunctions * sf)  {
         GetPOLRAD()->SetUSFs(sf);
         GetRADCOR()->SetUnpolSFs(sf);
         InSANEInclusiveDiffXSec::SetUnpolarizedStructureFunctions(sf);
      }

      /** Set the QE structure functions to be used to calculate G1,G2,g1,g2, etc.  */
      void SetQEStructureFunctions(InSANEStructureFunctions * sf) {
         GetPOLRAD()->SetUQESFs(sf);
      }
      /** Set the polarized structure functions to be used to calculate G1,G2,g1,g2, etc.  */
      void SetPolarizedStructureFunctions(InSANEPolarizedStructureFunctions * sf) {
         GetPOLRAD()->SetPSFs(sf);
         GetRADCOR()->SetPolarizedStructureFunctions(sf);
         InSANEInclusiveDiffXSec::SetPolarizedStructureFunctions(sf);
      }

      /** Set the form factors. */
      void SetFormFactors(InSANEFormFactors * ff) {
         GetPOLRAD()->SetFFs(ff);
         GetRADCOR()->SetFormFactors(ff);
         InSANEInclusiveDiffXSec::SetFormFactors(ff);
      }

      /** Set the form factors used for nuclei. */
      void SetTargetFormFactors(InSANEFormFactors * ff) {
         GetPOLRAD()->SetPOLRADTargetFormFactors(ff);
         GetRADCOR()->SetTargetFormFactors(ff);
      }

      void SetTargetNucleus(const InSANENucleus& t){
         InSANEInclusiveDiffXSec::SetTargetNucleus(t);
         GetPOLRAD()->SetTargetNucleus(t); 
         GetRADCOR()->SetTargetNucleus(t);
      }
      void SetVerbosity(int v){
         GetPOLRAD()->SetVerbosity(v);
         GetRADCOR()->SetVerbosity(v);
      } 

      ClassDef(InSANERadiativeTail,1)
};

#endif

