#ifndef InSANECompositePolarizedStructureFunctions_HH
#define InSANECompositePolarizedStructureFunctions_HH 1

#include "InSANEPolarizedStructureFunctions.h"
#include "InSANEPolarizedStructureFunctionsFromPDFs.h"
#include "DSSVPolarizedPDFs.h"
#include "AAC08PolarizedPDFs.h"
#include "BBPolarizedPDFs.h"
#include "GSPolarizedPDFs.h"
#include "DNS2005PolarizedPDFs.h"
#include "LSS2006PolarizedPDFs.h"
#include "TList.h"
#include "DSSVPolarizedPDFs.h"

/** ABC for a composite structure function which calls different structure functions depending on
 *  the kinematic variables, x and Q2.
 *  Potential problem: 
 *  You must implement GetSFIndex(x,Q2)
 *
 * \ingroup structurefunctions
 */ 
class InSANECompositePolarizedStructureFunctions : public InSANEPolarizedStructureFunctions {

   protected:
      TList fSFList;
      Int_t fNSFs;

   public:
      InSANECompositePolarizedStructureFunctions();
      virtual ~InSANECompositePolarizedStructureFunctions();

      void   Add(InSANEPolarizedStructureFunctions * sf);

      //virtual Int_t GetIndex(Double_t x, Double_t Q2) = 0; 
      virtual Double_t GetWeight(Int_t iSF,Double_t x, Double_t Q2) = 0; 

      /// Here is where you implement the specifics
      virtual Double_t g2p(Double_t x, Double_t Qsq);
      virtual Double_t g1p(Double_t x, Double_t Qsq);
      virtual Double_t g2n(Double_t x, Double_t Qsq);
      virtual Double_t g1n(Double_t x, Double_t Qsq);
      virtual Double_t g2d(Double_t x, Double_t Qsq);
      virtual Double_t g1d(Double_t x, Double_t Qsq);
      virtual Double_t g2He3(Double_t x, Double_t Qsq);
      virtual Double_t g1He3(Double_t x, Double_t Qsq);
      



ClassDef(InSANECompositePolarizedStructureFunctions,1)
};


/** Best structure function implementation at Low \f$ Q^2 \f$ over all of x.
 *  - CTEQ6 for low X 
 *  - F1F209 for high x
 */
class LowQ2PolarizedStructureFunctions : public InSANECompositePolarizedStructureFunctions {

   private:
      DSSVPolarizedPDFs fDSSVPolarizedPDFs;
      InSANEPolarizedStructureFunctionsFromPDFs fDSSVSFs;
      AAC08PolarizedPDFs fAACPolarizedPDFs;
      InSANEPolarizedStructureFunctionsFromPDFs fAACSFs;
      
      Double_t xrange[5]; 

   public:
      LowQ2PolarizedStructureFunctions();
      virtual ~LowQ2PolarizedStructureFunctions();

      virtual Double_t GetWeight(Int_t iSF, Double_t x, Double_t Q2);

      ClassDef(LowQ2PolarizedStructureFunctions,1)
};

#endif

