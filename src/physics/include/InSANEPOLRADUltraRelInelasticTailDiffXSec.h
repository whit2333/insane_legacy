#ifndef InSANEPOLRADUltraRelInelasticTailDiffXSec_HH
#define InSANEPOLRADUltraRelInelasticTailDiffXSec_HH 1

#include "InSANEPOLRADBornDiffXSec.h"

/** IRT in the Ultra relativistic approximation.
 *
 * \ingroup inclusiveXSec
 */
class InSANEPOLRADUltraRelInelasticTailDiffXSec: public InSANEPOLRADBornDiffXSec {
   public:
      InSANEPOLRADUltraRelInelasticTailDiffXSec();
      virtual ~InSANEPOLRADUltraRelInelasticTailDiffXSec();
      virtual InSANEPOLRADUltraRelInelasticTailDiffXSec*  Clone(const char * 
            newname) const ;
      virtual InSANEPOLRADUltraRelInelasticTailDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(InSANEPOLRADUltraRelInelasticTailDiffXSec,1)
};

#endif

