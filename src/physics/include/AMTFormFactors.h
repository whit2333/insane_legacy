#ifndef AMTFormFactors_HH
#define AMTFormFactors_HH 1

#include "InSANEDipoleFormFactors.h"

/** AMT fit to electric and magnetic form factors for the proton.
 *  Phys. Rev. C 76, 035205 (2007)
 *  This model first corrects the measurements with two photon exchange corrections
 *  then fits the data.
 *
 *  \ingroup formfactors
 */
class AMTFormFactors : public InSANEDipoleFormFactors {

   protected:
      Double_t a_GE[3];
      Double_t b_GE[5];
      Double_t a_GM_over_Mu[3];
      Double_t b_GM_over_Mu[5];

   public:
      AMTFormFactors() ;
      virtual ~AMTFormFactors();

      /** \f$ G_{Ep} = \frac{1+\sum_{i=1}^{n} a_i \tau^i }{1+\sum_{i=1}^{n+2} b_i \tau^i} \f$
       *   where \f$ \tau= Q^2/4M^2 \f$
       */
      virtual Double_t GEp(Double_t Qsq) ;

      /** \f$ G_{Mp}/\mu_{p} = \frac{1+\sum_{i=1}^{n} a_i \tau^i }{1+\sum_{i=1}^{n+2} b_i \tau^i} \f$ */
      virtual Double_t GMp(Double_t Qsq) ;

      ClassDef(AMTFormFactors, 1)
};

#endif

