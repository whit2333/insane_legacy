#ifndef InSANETSpectrumSeed_HH
#define InSANETSpectrumSeed_HH 1

#include "InSANEClusteringSeed.h"

/** Concrete implementation of InSANEClusteringSeed
 *
 *  Use TSpectrum to find seeds
 *
 *  \ingroup Clustering
 */
class InSANETSpectrumSeed : public InSANEClusteringSeed {
public:
   InSANETSpectrumSeed() {  };

   ~InSANETSpectrumSeed() { };

///Concrete classes should imp clear
   void Clear() {
      for (int k = 0; k < fNSeeds; k++) {
         fiSeed[k] = 0;
         fjSeed[k] = 0;
         fxSeed[k] = -99999.0;
         fySeed[k] = -99999.0;
      }
      fNSeeds = 0;
   }

   ClassDef(InSANETSpectrumSeed, 1)
};


#endif

