#ifndef InSANEInclusiveBornDISXSec_HH
#define InSANEInclusiveBornDISXSec_HH 1

#include "InSANEInclusiveDiffXSec.h"
#include "InSANEInelasticRadiativeTail2.h"
#include "InSANEFortranWrappers.h"
#include "InSANEMathFunc.h"

/** Simply uses any structure function.
 *
 * \ingroup inclusiveXSec
 */
class InSANEInclusiveBornDISXSec : public InSANEInclusiveDiffXSec {

   public:
      InSANEInclusiveBornDISXSec();
      virtual ~InSANEInclusiveBornDISXSec();
      InSANEInclusiveBornDISXSec(const InSANEInclusiveBornDISXSec& rhs);
      InSANEInclusiveBornDISXSec& operator=(const InSANEInclusiveBornDISXSec& rhs);
      virtual InSANEInclusiveBornDISXSec*  Clone(const char * newname) const;
      virtual InSANEInclusiveBornDISXSec*  Clone() const;

      virtual Double_t EvaluateXSec(const Double_t *x) const;

      ClassDef(InSANEInclusiveBornDISXSec,1)
};

#endif

