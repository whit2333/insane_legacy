#ifndef insane_physics_HH
#define insane_physics_HH

#include <functional>

namespace insane {

  namespace physics {

    /** Dirac Form factor from Sachs form factors.
     * 
     * \f$  F_1 = \frac{G_E + \tau G_M}{1+\tau} \f$
     *
     * where
     *
     * \f$ \tau = \frac{Q^2}{4M^2} \f$
     */
    double F1(double Q2, double GE, double GM);

    /** Pauli Form factor from Sachs form factors.
     * 
     * \f$  F_2 = \frac{G_M - G_E}{1+\tau} \f$
     *
     * where
     *
     * \f$ \tau = \frac{Q^2}{4M^2} \f$
     */
    double F2(double Q2, double GE, double GM);



    /** Wandzura-Wilczek function form.
     *
     *  \f$ \displaystyle g_2 = -g_1(x) + \int g_1(y)\frac{dy}{y} \f$
     *
     */
    double WW(std::function<double(double)> func, double x, double max=1.0);

    namespace TMCs {
      // Target Mass correction function
      // J. Phys. G 35, 053101 (2008)
      double h2(std::function<double(double)> F1, double xi, double Q2);
      double g2(std::function<double(double)> F1, double xi, double Q2);
    }

    const size_t NPartons     = 13;
    const size_t NLightQuarks = 6;

    // deprecated
    enum PartonFlavor { 
      kUP          = 0,
      kDOWN        = 1,
      kSTRANGE     = 2,
      kCHARM       = 3,
      kBOTTOM      = 4,
      kTOP         = 5,
      kGLUON       = 6,
      kANTIUP      = 7,
      kANTIDOWN    = 8,
      kANTISTRANGE = 9,
      kANTICHARM   = 10,
      kANTIBOTTOM  = 11,
      kANTITOP     = 12
    };
    enum class Parton { 
      u    = 0,
      d    = 1,
      s    = 2,
      c    = 3,
      b    = 4,
      t    = 5,
      g    = 6,
      ubar = 7,
      dbar = 8,
      sbar = 9,
      cbar = 10,
      bbar = 11,
      tbar = 12
    };
    constexpr std::array<PartonFlavor,NLightQuarks> LightQuarks = {
      kUP    , kDOWN    , kSTRANGE     ,
      kANTIUP, kANTIDOWN, kANTISTRANGE
    };
    constexpr std::array<double,NPartons> PartonCharge2 = { 
      4.0/9.0, 1.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0,
      0.0,
      4.0/9.0, 1.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0
    };

    constexpr std::array<double,NPartons> PartonCharge = { 
      2.0/3.0, -1.0/3.0, -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0,
      0.0,
      -2.0/3.0, 1.0/3.0, 1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0
    };

    constexpr std::array<double,NPartons> IsoSpinConjugatePartonCharge2 = { 
      1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0,
      0.0,
      1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0
    };
    constexpr std::array<double,NPartons> IsoSpinConjugatePartonCharge = { 
      -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0,
      0.0,
      1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0
    };

    enum class SFType {
      Unpolarized,
      Polarzed,
      Spin,
      FormFactor,
      Compton,
    };

    const size_t NStructureFunctions = 15;
    enum class SF {
      F1,
      F2,
      FL,
      R,
      W1,
      W2,
      g1,
      g2,
      gT,
      g2_WW,
      g1_BT,
    };

    const size_t NComptonAsymmetries = 16;
    enum class ComptonAsymmetry {
      A1,
      A2,
      AT,
      AL,
    };

    const size_t NCFormFactors = 10;
    enum class FormFactor {
      GE,
      GM,
      F1,
      F2,
      GC,
      GQ,
      A,
      B,
    };

    enum class FragmentationType {
      D1,
      H1,
    };

    enum class Chirality {
      Even,
      Odd,
    };

    const size_t NNuclei = 7;
    enum class Nuclei {
      p,
      n,
      d,
      t,
      _3He,
      _4He,
      H     = p, 
      D     = d,
      _2H   = d,
      _3H   = t,
      alpha = _4He,
      Other,
    };

    enum class TargetSpin {
      Zero,
      OneHalf,
      One,
      ThreeHalves,
      Two,
      FiveHalves,
      ThreeHalf = ThreeHalves,
      FiveHalf  = ThreeHalf,
    };

    enum class OPELimit {
      Bjorken,
      MasslessTarget,
      MasslessQuark,
      MassiveTarget,
      MassiveQuark,
      MassiveTargetAndQuark,
      TMC                    = MassiveTarget,
      Massless               = Bjorken,
      Massive                = MassiveTargetAndQuark,
    };
    enum class Twist {
      Two,
      Three,
      Four,
      TwoAndThree,
      TwoThreeAndFour,
      Leading             = Two,
      NextToLeading       = TwoAndThree,
      NextToNextToLeading = TwoThreeAndFour,
      All                 = TwoThreeAndFour,
    };

    enum class GPDType {
      H,
      Htilde,
      E,
      Etilde,
      H_T,
      Htilde_T,
      E_T,
      Etilde_T,
    };

  }

  namespace kinematics {

    /** Kinematic variables and other useful functions.
     *
     * \f$ F_L = \rho^2 F_2 - 2x F_1) = 2x F_1 R \f$
     * \f$ \rho = \sqrt{1+ \frac{4M^2x^2}{Q^2}} \f$
     *
     */

    double xi_Nachtmann(double x, double Q2);

    double rho2(double x, double Q2);
    double rho( double x, double Q2);

  }
}

#endif

