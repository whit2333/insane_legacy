#ifndef INSANEPOLARIZEDSTRUCTUREFUNCTIONSFROMPDFS_h
#define INSANEPOLARIZEDSTRUCTUREFUNCTIONSFROMPDFS_h 1

#include <cstdlib> 
#include <iostream>
#include <iomanip>
#include "InSANEPartonDistributionFunctions.h"
#include "InSANEStructureFunctions.h"
#include "InSANEPolarizedStructureFunctions.h"
#include "InSANEPolarizedPartonDistributionFunctions.h"

/** Base class for Polarized Structure Functions using Polarized Parton Distributions.
 *
 *  Only \f$ g_1 \f$ and \f$ g2_{WW} \f$ can be calculated from polarized parton distributions.
 *  
 *  \f$ g_1(x) = (1/2)\sum_q \Delta q(x) \f$
 *
 *  Note that because the the nuclear corrections, target mass and higher twist corrections
 *  effect the extraction of and evolve with the pdfs they are expected to be defined from the
 *  PDF class. In the future they should be decomposed into some type of quark distributions.
 * 
 * \ingroup partondistributions
 * \ingroup structurefunctions
 */
class InSANEPolarizedStructureFunctionsFromPDFs: public InSANEPolarizedStructureFunctions {

   protected:
      InSANEPolarizedPartonDistributionFunctions *fPolarizedPDFs;

   public: 
      InSANEPolarizedStructureFunctionsFromPDFs();  
      InSANEPolarizedStructureFunctionsFromPDFs(InSANEPolarizedPartonDistributionFunctions * ppdfs);  
      virtual ~InSANEPolarizedStructureFunctionsFromPDFs(); 

      void SetPolarizedPDFs(InSANEPolarizedPartonDistributionFunctions * ppdfs) ;
      InSANEPolarizedPartonDistributionFunctions * GetPolarizedPDFs() ;

      /**  g_1^{p}
       *  \f$ \lim_{Bjorken} M^2 \nu G_1(p.q,Q^2) = g_1(x)   \f$
       *  \f$ \nu = \frac{Q^2}{2Mx} \f$
       *
       *  \f$ g_1^{n} \f$ using isospin invariance of pdfs p->n (u->d,d->u) 
       *  \f$ g_1^{d}       \f$  
       *  \f$ g_1^{^{3}He}  \f$ 
       *
       * \f$ \lim_{Bjorken} M \nu^2 G_2(p.q,Q^2) = g_2(x)   \f$
       *
       *
       *  \f$ g_2^{WW} = \int_x^1 g_1(y)/y dy - g_1(x)\f$
       * 
       */        

      // -----------------------------
      // g1 Spin Structure Function
      virtual Double_t g1p(   Double_t x, Double_t Q2);
      virtual Double_t g1n(   Double_t x, Double_t Q2);
      virtual Double_t g1d(   Double_t x, Double_t Q2);
      virtual Double_t g1He3( Double_t x, Double_t Q2);

      virtual Double_t g1p_Error(   Double_t x, Double_t Q2){ return g1pError(x,Q2);}
      virtual Double_t g1n_Error(   Double_t x, Double_t Q2){ return g1nError(x,Q2);}
      virtual Double_t g1d_Error(   Double_t x, Double_t Q2){ return 0.0;}
      virtual Double_t g1He3_Error( Double_t x, Double_t Q2){ return g1He3Error(x,Q2);}
      // Deprecated:
      virtual Double_t g1pError(   Double_t x, Double_t Q2);
      virtual Double_t g1nError(   Double_t x, Double_t Q2);
      virtual Double_t g1He3Error( Double_t x, Double_t Q2);

      // -----------------------------
      // g2 Spin Structure Function
      virtual Double_t g2p(   Double_t x, Double_t Q2);
      virtual Double_t g2n(   Double_t x, Double_t Q2);
      virtual Double_t g2d(   Double_t x, Double_t Q2);
      virtual Double_t g2He3( Double_t x, Double_t Q2);

      /// \todo Need to handle g2 errors 
      virtual Double_t g2p_Error(   Double_t x, Double_t Q2){ return 0.0;}
      virtual Double_t g2n_Error(   Double_t x, Double_t Q2){ return 0.0;}
      virtual Double_t g2d_Error(   Double_t x, Double_t Q2){ return 0.0;}
      virtual Double_t g2He3_Error( Double_t x, Double_t Q2){ return 0.0;}

      // -----------------------------
      //virtual Double_t g2pWW_Error(   Double_t x, Double_t Q2){ return 0.0;}
      //virtual Double_t g2nWW_Error(   Double_t x, Double_t Q2){ return 0.0;}
      //virtual Double_t g2dWW_Error(   Double_t x, Double_t Q2){ return 0.0;}
      //virtual Double_t g2He3WW_Error( Double_t x, Double_t Q2){ return 0.0;}

      // ------------------------------------
      // Twist expansion with target mass M=0
      // ------------------------------------
      // g1 twist-2  M=0 (no TMC)
      virtual Double_t g1p_Twist2(   Double_t x, Double_t Q2){ return fPolarizedPDFs->g1p_Twist2(x , Q2); }
      virtual Double_t g1n_Twist2(   Double_t x, Double_t Q2){ return fPolarizedPDFs->g1n_Twist2(x , Q2); }
      virtual Double_t g1d_Twist2(   Double_t x, Double_t Q2){ return fPolarizedPDFs->g1d_Twist2(x , Q2); }
      virtual Double_t g1He3_Twist2( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1He3_Twist2(x   , Q2); }

      // g2 twist-2  M=0 (no TMC), i.e. g2_WW
      virtual Double_t g2p_Twist2(   Double_t x, Double_t Q2){ return fPolarizedPDFs->g2p_Twist2(x , Q2); }
      virtual Double_t g2n_Twist2(   Double_t x, Double_t Q2){ return fPolarizedPDFs->g2n_Twist2(x , Q2); }
      virtual Double_t g2d_Twist2(   Double_t x, Double_t Q2){ return fPolarizedPDFs->g2d_Twist2(x , Q2); }
      virtual Double_t g2He3_Twist2( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2He3_Twist2(x   , Q2); }

      // -----------------------------
      // g2 twist-3  M=0 (no TMC)
      virtual Double_t g1p_Twist3(Double_t   x, Double_t Q2) { return 0.0;}//fPolarizedPDFs->g1p_Twist3(x , Q2); }
      virtual Double_t g1n_Twist3(Double_t   x, Double_t Q2) { return 0.0;}//fPolarizedPDFs->g1n_Twist3(x , Q2); }
      virtual Double_t g1d_Twist3(Double_t   x, Double_t Q2) { return 0.0;}//fPolarizedPDFs->g1d_Twist3(x , Q2); }
      virtual Double_t g1He3_Twist3(Double_t x, Double_t Q2) { return 0.0;}//fPolarizedPDFs->g1He3_Twist3(x   , Q2); }

      // g2 twist-3  M=0 (no TMC)
      virtual Double_t g2p_Twist3(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2p_Twist3(x , Q2); }
      virtual Double_t g2n_Twist3(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2n_Twist3(x , Q2); }
      virtual Double_t g2d_Twist3(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2d_Twist3(x , Q2); }
      virtual Double_t g2He3_Twist3(Double_t x, Double_t Q2) { return fPolarizedPDFs->g2He3_Twist3(x   , Q2); }

      // -----------------------------
      // g1 twist 4
      virtual Double_t g1p_Twist4(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g1p_Twist4(x , Q2); }
      virtual Double_t g1n_Twist4(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g1n_Twist4(x , Q2); }
      virtual Double_t g1d_Twist4(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g1d_Twist4(x , Q2); }
      virtual Double_t g1He3_Twist4(Double_t x, Double_t Q2) { return fPolarizedPDFs->g1He3_Twist4(x   , Q2); }

      // g2 twist 4
      virtual Double_t g2p_Twist4(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2p_Twist4(x , Q2); }
      virtual Double_t g2n_Twist4(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2n_Twist4(x , Q2); }
      virtual Double_t g2d_Twist4(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2d_Twist4(x , Q2); }
      virtual Double_t g2He3_Twist4(Double_t x, Double_t Q2) { return fPolarizedPDFs->g2He3_Twist4(x   , Q2); }

      // ---------------------------------
      // SSFs with Target Mass Corrections
      // ---------------------------------
      //
      // g1 Spin Structure Function with TMC and all Twists <=4
      virtual Double_t g1p_TMC(   Double_t x, Double_t Q2);
      virtual Double_t g1n_TMC(   Double_t x, Double_t Q2);
      virtual Double_t g1d_TMC(   Double_t x, Double_t Q2);
      virtual Double_t g1He3_TMC( Double_t x, Double_t Q2);

      // g1 Spin Structure Function with TMC and all Twists <=4
      virtual Double_t g2p_TMC(   Double_t x, Double_t Q2);
      virtual Double_t g2n_TMC(   Double_t x, Double_t Q2);
      virtual Double_t g2d_TMC(   Double_t x, Double_t Q2);
      virtual Double_t g2He3_TMC( Double_t x, Double_t Q2);

      // g1_twist2 plus the twist-2 TMC
      virtual Double_t g1p_Twist2_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1p_Twist2_TMC(  x, Q2); }
      virtual Double_t g1n_Twist2_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1n_Twist2_TMC(  x, Q2); }
      virtual Double_t g1d_Twist2_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1d_Twist2_TMC(  x, Q2); }
      virtual Double_t g1He3_Twist2_TMC( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1He3_Twist2_TMC(x, Q2); }

      // g1_twist3 plus the twist-3 TMC
      virtual Double_t g1p_Twist3_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1p_Twist3_TMC(  x, Q2); }
      virtual Double_t g1n_Twist3_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1n_Twist3_TMC(  x, Q2); }
      virtual Double_t g1d_Twist3_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1d_Twist3_TMC(  x, Q2); }
      virtual Double_t g1He3_Twist3_TMC( Double_t x, Double_t Q2){ return fPolarizedPDFs->g1He3_Twist3_TMC(x, Q2); }

      // -----------------------------
      // g2_twist2 plus the twist-2 TMC
      virtual Double_t g2p_Twist2_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2p_Twist2_TMC(  x, Q2); }
      virtual Double_t g2n_Twist2_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2n_Twist2_TMC(  x, Q2); }
      virtual Double_t g2d_Twist2_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2d_Twist2_TMC(  x, Q2); }
      virtual Double_t g2He3_Twist2_TMC( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2He3_Twist2_TMC(x, Q2); }

      // g2_twist3 plus the twist-3 TMC
      virtual Double_t g2p_Twist3_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2p_Twist3_TMC(  x, Q2); }
      virtual Double_t g2n_Twist3_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2n_Twist3_TMC(  x, Q2); }
      virtual Double_t g2d_Twist3_TMC  ( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2d_Twist3_TMC(  x, Q2); }
      virtual Double_t g2He3_Twist3_TMC( Double_t x, Double_t Q2){ return fPolarizedPDFs->g2He3_Twist3_TMC(x, Q2); }

      // -----------------------------
      // g1 twist 4
      virtual Double_t g1p_Twist4_TMC(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g1p_Twist4_TMC(x , Q2); }
      virtual Double_t g1n_Twist4_TMC(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g1n_Twist4_TMC(x , Q2); }
      virtual Double_t g1d_Twist4_TMC(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g1d_Twist4_TMC(x , Q2); }
      virtual Double_t g1He3_Twist4_TMC(Double_t x, Double_t Q2) { return fPolarizedPDFs->g1He3_Twist4_TMC(x   , Q2); }

      // g2 twist 4
      virtual Double_t g2p_Twist4_TMC(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2p_Twist4_TMC(x , Q2); }
      virtual Double_t g2n_Twist4_TMC(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2n_Twist4_TMC(x , Q2); }
      virtual Double_t g2d_Twist4_TMC(Double_t   x, Double_t Q2) { return fPolarizedPDFs->g2d_Twist4_TMC(x , Q2); }
      virtual Double_t g2He3_Twist4_TMC(Double_t x, Double_t Q2) { return fPolarizedPDFs->g2He3_Twist4_TMC(x   , Q2); }

      // -----------------------------
      // The wrong way of doing things (as a check):
      virtual Double_t g2pWW_TMC(   Double_t x, Double_t Q2);
      virtual Double_t g2pWW_TMC_t3(   Double_t x, Double_t Q2);


      ClassDef(InSANEPolarizedStructureFunctionsFromPDFs, 1)

}; 

#endif 
