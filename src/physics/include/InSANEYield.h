#ifndef InSANEYield_HH
#define InSANEYield_HH

#include "InSANELuminosity.h"
#include "InSANETarget.h"
#include "InSANETargetMaterial.h"
#include "InSANEDiffXSec.h"

/** Calculte the Yield from a luminosity and cross section.
 *
 *  Note: units are A, seconds, g, cm, and g/cm^3
 *  The cross section units are nb
 */
class InSANEYield : public InSANELuminosity {

   protected:
      TList              fXSections;
      InSANEDiffXSec   * fXsec;
      //InSANELuminosity * fLumi;

   public : 
      InSANEYield(InSANETarget * targ = nullptr, Double_t current = 0.0);
      virtual ~InSANEYield();

      void AddCrossSection(InSANEDiffXSec * xs);
      void SetCrossSection(InSANEDiffXSec * xs) { fXsec = xs;};
      void SetLuminosity(InSANEDiffXSec * xs) { fXsec = xs;};

      // Calculate the rate for the ith material 
      Double_t CalculateRate(Int_t i, Double_t * x , int pdgcode = 0);

      // Calculate the total rate for particle type 
      Double_t CalculateRate(Double_t * x, int pdgcode = 0);

      Double_t CalculateTotalRate(Int_t i, Double_t * x , int pdgcode = 0);
      Double_t CalculateTotalRate(Double_t * x, int pdgcode = 0);

      ClassDef(InSANEYield,2)
};

#endif

