#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;

#pragma link C++ nestedclass;

#pragma link C++ namespace CLHEP;
#pragma link C++ class std::vector<TString*>+;

#pragma link C++ class std::map<int, double >+;
//#pragma link C++ class std::map<int, double >::iterator+;
//#pragma link C++ class std::pair<int, double >+;

//#pragma link C++ class std::map<int, std::vector<double> >+;
//#pragma link C++ class std::pair<int, std::vector<double> >+;
//#pragma link C++ class std::map<int, std::vector<double> >::iterator+;

#pragma link C++ class std::vector<TGraph*>+;

#pragma link C++ class InSANEPMTResponse+;
#pragma link C++ class InSANEPMTResponse2+;
#pragma link C++ class InSANEPMTResponse3+;
#pragma link C++ class InSANEPMTResponse32+;
#pragma link C++ class InSANEPMTResponse33+;
#pragma link C++ class InSANEPMTResponse4+;
#pragma link C++ class InSANEPMTResponse42+;
#pragma link C++ class InSANEPMTResponse5+;

#pragma link C++ class InSANENucleus+; 
#pragma link C++ global fgProton+;
#pragma link C++ global fgNeutron+;
#pragma link C++ global fgDeuteron+;
#pragma link C++ global fg3He+;
#pragma link C++ global fgTriton+;
// #pragma link C++ global fgIron+;
// #pragma link C++ global fgOther+;

//#pragma link C++ class InSANEAsymmetryBase+;
//#pragma link C++ class InSANEAsymmetriesFromStructureFunctions+;

//#pragma link C++ class InSANEFunctionManager+;
//#pragma link C++ global fgInSANEFunctionManager+;

#pragma link C++ class InSANEDetector+;
#pragma link C++ class InSANEAnalysis+;
#pragma link C++ class BETAG4Analysis+;
#pragma link C++ class BETAG4DetectorsAnalysis+;
#pragma link C++ class InSANEDetectorAnalysis+;
#pragma link C++ class InSANEScalerAnalysis+;
#pragma link C++ class SANEElasticAnalysis+;


#pragma link C++ class InSANEDetectorPackage+;
#pragma link C++ class InSANEDetectorPedestal+;
#pragma link C++ class InSANETDCSpectrum+;
#pragma link C++ class InSANEDetectorTiming+;
#pragma link C++ class std::vector<InSANEDetectorTiming*>+;
#pragma link C++ class std::vector<InSANEDetectorPedestal*>+;
#pragma link C++ class InSANEAsymmetryDetector+;
#pragma link C++ class InSANEAsymmetryDetectorEvent+;

#pragma link C++ class InSANECalorimeterDetector+;
#pragma link C++ class GasCherenkovDetector+;
#pragma link C++ class LuciteHodoscopeDetector+;
#pragma link C++ class ForwardTrackerDetector+;
#pragma link C++ class ForwardTrackerScintillator+;
#pragma link C++ class BigcalDetector+;
#pragma link C++ class BETADetectorPackage+;
#pragma link C++ class BETASubDetector+;
#pragma link C++ class BETAPseudoSpectrometer+;
#pragma link C++ class BETAPseudoSpectrometersCalc+;
#pragma link C++ class std::vector<BETAPseudoSpectrometer*>+;
#pragma link C++ class BETACherenkovMirrorArray+;

#pragma link C++ class BigcalPi0Calibrator+;
#pragma link C++ class Pi0PhotonEnergy+;
#pragma link C++ class Pi0MassSquared+;
#pragma link C++ class BigcalPi0Calibrator+;

#pragma link C++ class AnalyzerToInSANE+;
#pragma link C++ class AnalyzerScalersToInSANE+;

//#pragma link C++ class InSANEPhysics+;
//#pragma link C++ class InSANEQSquared+;

#pragma link C++ class InSANEAnalysis+;
#pragma link C++ class InSANERunNum+;
#pragma link C++ class InSANERunSummary+;
#pragma link C++ class InSANERun+;

#pragma link C++ enum InSANERun::SANETargets;
#pragma link C++ enum InSANERun::SANETargetSign;
#pragma link C++ enum InSANERun::SANETargetCup;

#pragma link C++ global kgSANETargets+;
#pragma link C++ global kgSANETargetNames+;
#pragma link C++ global kgSANETargetCupNames+;
#pragma link C++ global kgSANETargetSignName+;
#pragma link C++ global kgSANETargetCups+;
#pragma link C++ global kgSANETargetSign+;

#pragma link C++ class InSANERunReport+;
#pragma link C++ class InSANERunReportDatum+;
#pragma link C++ class std::vector<InSANERunReportDatum*>+;
#pragma link C++ class InSANERunTiming+;
#pragma link C++ class SANEProductionRun+;
#pragma link C++ class BETAG4SimulationRun+;

#pragma link C++ class std::map<Int_t,TFile*>+;
#pragma link C++ class std::pair<Int_t,TFile*>+;

#pragma link C++ class SANERunManager+;
#pragma link C++ class InSANERunManager+;
#pragma link C++ global fgSANERunManager+;

#pragma link C++ class BigcalElasticClusterAnalysis+;
#pragma link C++ class BETAPedestalAnalysis+;
#pragma link C++ class BETATimingAnalysis+;
#pragma link C++ class SANEGasCherenkovAnalysis+;
#pragma link C++ class SANELuciteHodoscopeAnalysis+;

#pragma link C++ class InSANEClusterAnalysis+;

#pragma link C++ class InSANEMaterial+; 

//#pragma link C++ class InSANEBeamSpinAsymmetry+;
//#pragma link C++ class InSANEBeamTargetAsymmetry+;
//#pragma link C++ class InSANEPolarizedDISAsymmetry+;

#pragma link C++ class InSANELuminosity+;
#pragma link C++ class InSANEDilutionFactor+;
#pragma link C++ class InSANEDilutionFunction+;
#pragma link C++ class InSANEDilutionFromTarget+;

//#pragma link C++ class InSANEInclusiveWiserXSec+;
//#pragma link C++ class InSANEPhaseSpaceSampler+;
//
//#pragma link C++ function formc_(double *)+; 
//#pragma link C++ function formm_(double *)+; 
//#pragma link C++ function cross_tot_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function f1f2in09_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function cross_tot_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function f1f2in09_mod_(double * ,double * ,double *,double * ,double * ,double * ,double * ,double * )+;
//
//#pragma link C++ function qfs_(double*,double*,double*,double*,double*, double*,double*, double*)+;
//#pragma link C++ function qfs_born_(double*,double*,double*,double*,double*, double*,double*, double*)+;
//#pragma link C++ function qfs_radiated_(double*,double*,double*,double*,double*, double*, double*, double*)+;
//#pragma link C++ function wiser_sub_(double * ,int * ,double * ,double * , double * )+;
//#pragma link C++ function wiser_all_sig_(double * ,double * ,double * ,double * , int *  ,double * )+;
//#pragma link C++ function wiser_all_sig0_(double * ,double * ,double * ,double * , int *  ,double * )+;
//#pragma link C++ function aac08pdf_(double*,double*,int*,double*,double**)+;
//#pragma link C++ function dssvini_()+;
//#pragma link C++ function dssvfit_(double *,double *,double *,double *,double *,double *,double *,double *)+;
//#pragma link C++ function setctq6_(int*)+;
//#pragma link C++ function getctq6_(int* ,double*, double*, double*)+;
//#pragma link C++ function ppdf_( int*, double*,double*,double*, double*,double*,double*, double*,double*,double*,double*,double*,double*,double*,double*)+;
//#pragma link C++ function ini_()+;
//#pragma link C++ function polfit_(int*,double*,double*,double*, double*, double*,double*,double*, double*,double*,double*)+;
//#pragma link C++ function lss2006init_()+;
//#pragma link C++ function lss2006_(int*,double*,double*, double*,double*,double*, double*,double*,double*,double*, double*, double*,double*,double*, double*,double*)+;
//#pragma link C++ function nloini_()+;
//#pragma link C++ function polnlo_(int* ,double*, double*, double*, double*, double*, double*, double*, double*)+;
//#pragma link C++ function epcv_single_(char* , int *, double *, double *,double*, double*, double*, double*)+;
//
//#pragma link C++ function sane_pol_(double * ,double * ,double * ,int * ,int * ,double * )+;
//
//
//#pragma link C++ class GSPolarizedPDFs+;
//#pragma link C++ class CTEQ6UnpolarizedPDFs+;
//
//#pragma link C++ class CTEQ6eInclusiveDiffXSec+;
//#pragma link C++ class F1F209eInclusiveDiffXSec+;
//#pragma link C++ class InSANEPolIncDiffXSec+;
//
//#pragma link C++ class QFSInclusiveDiffXSec+;
//#pragma link C++ class QFSXSecConfiguration+;
//
//#pragma link C++ class InSANEPolarizedDiffXSec+;
//#pragma link C++ class PolarizedDISXSec+;
//#pragma link C++ class PolarizedDISXSecParallelHelicity+;
//#pragma link C++ class PolarizedDISXSecAntiParallelHelicity+;
//
//#pragma link C++ class InSANEInclusiveWiserXSec+;
//#pragma link C++ class InSANEInclusiveEPCVXSec+;

#pragma link C++ class InSANEReconstruction+;

#pragma link C++ class SANERunOverview+;
#pragma link C++ class SANEParallelRunOverview+;
#pragma link C++ class SANEPerpendicularRunOverview+;
#pragma link C++ class SANECarbonRunOverview+;

#pragma link C++ class InSANECalibration+;
#pragma link C++ class InSANEDetectorCalibration+;
#pragma link C++ class std::vector<InSANECalibration*>+;
#pragma link C++ class std::vector<TParameter<double>*>+;

#pragma link C++ class InSANEAnalysisManager+;
#pragma link C++ class SANEAnalysisManager+;
#pragma link C++ global fgInSANEAnalysisManager+;
#pragma link C++ global fgSANEAnalysisManager+;

#pragma link C++ class InSANETrajectoryAnalysis+;
#pragma link C++ class SANETrajectoryAnalyzer+;

#pragma link C++ class InSANERunQuantity+;
#pragma link C++ class InSANERunSeriesObject+;
#pragma link C++ class InSANERunSeries+;
#pragma link C++ class InSANEResult+;

#pragma link C++ class InSANERunPolarization+;
#pragma link C++ class InSANERunTargOfflinePol+;
#pragma link C++ class InSANERunTargOnlinePol+;
#pragma link C++ class InSANERunBeamPol+;
#pragma link C++ class InSANERunTargTopPosPol+;
#pragma link C++ class InSANERunTargBotPosPol+;
#pragma link C++ class InSANERunTargTopNegPol+;
#pragma link C++ class InSANERunTargBotNegPol+;
#pragma link C++ class InSANERunAsymmetry+;
#pragma link C++ class InSANERawRunAsymmetry+;
#pragma link C++ class InSANERawRunAsymmetryDilution+;
#pragma link C++ class InSANERawRunAsymmetryCounts+;
#pragma link C++ class InSANERunAsymmetryGroup+;

#pragma link C++ class InSANEAcceptance+;

#pragma link C++ class std::map<int,InSANERun*>+;


#pragma link C++ class InSANEAsymmetry+;
#pragma link C++ class InSANEExpParameter+;
#pragma link C++ class InSANERawAsymmetry+;

#pragma link C++ class InSANEAveragedKinematics1D+;
#pragma link C++ class InSANEAveragedKinematics2D+;
#pragma link C++ class InSANEAveragedKinematics3D+;
#pragma link C++ class InSANEAveragedPi0Kinematics1D+;

#pragma link C++ class InSANERadiativeCorrections1D+;

#pragma link C++ class InSANEMeasuredAsymmetry+;
#pragma link C++ class InSANEAveragedMeasuredAsymmetry+;
#pragma link C++ class SANEMeasuredAsymmetry+;

#pragma link C++ class InSANEPi0MeasuredAsymmetry+;
#pragma link C++ class InSANEAveragedPi0MeasuredAsymmetry+;

#pragma link C++ class InSANEKinematicCoefficient+;
#pragma link C++ class InSANEComptonAsymmetry+;

#pragma link C++ class InSANEDatabaseManager+;
#pragma link C++ global fgInSANEDatabaseManager+;


#endif

