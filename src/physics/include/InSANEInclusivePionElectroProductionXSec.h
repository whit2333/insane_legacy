#ifndef InSANEInclusivePionElectroProductionXSec_HH
#define InSANEInclusivePionElectroProductionXSec_HH 1

#include "InSANECompositeDiffXSec.h"
#include "InSANEPhotonDiffXSec.h"

/**  Inclusive electro production of pions, kaons, and nucleons.
 *   Uses the fit from wiser and virtual photon spectrum from Traitor-Wright Nuc.Phys. A379.
 *   The recoil factor (eq13) is also included.
 *
 * \ingroup inclusiveXSec
 */
class InSANEInclusivePionElectroProductionXSec : public InSANEPhotonDiffXSec {

   protected:
      InSANEPhotonDiffXSec * fSig_0;       //->
      InSANEPhotonDiffXSec * fSig_plus;    //->
      InSANEPhotonDiffXSec * fSig_minus;   //->

   public:
      InSANEInclusivePionElectroProductionXSec();
      InSANEInclusivePionElectroProductionXSec(const InSANEInclusivePionElectroProductionXSec& rhs) ;
      virtual ~InSANEInclusivePionElectroProductionXSec();
      InSANEInclusivePionElectroProductionXSec& operator=(const InSANEInclusivePionElectroProductionXSec& rhs) ;
      virtual InSANEInclusivePionElectroProductionXSec*  Clone(const char * newname) const ;
      virtual InSANEInclusivePionElectroProductionXSec*  Clone() const ; 

      void  SetParameters(int i, const std::vector<double>& pars);
      const std::vector<double>& GetParameters() const ;

      virtual void InitializePhaseSpaceVariables();
      Double_t EvaluateXSec(const Double_t * x) const ;

      // Here we copied a bunch of setters from InSANEDiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void       SetTargetMaterial(const InSANETargetMaterial& mat){
         fSig_0->SetTargetMaterial(mat);
         fSig_plus->SetTargetMaterial(mat);
         fSig_minus->SetTargetMaterial(mat);
         fTargetMaterial = mat;
      }
      void SetTargetNucleus(const InSANENucleus & targ){
         InSANEInclusiveDiffXSec::SetTargetNucleus(targ);
         //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
         //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
      }

      void       SetTargetMaterialIndex(Int_t i){
         fSig_0->SetTargetMaterialIndex(i);
         fSig_plus->SetTargetMaterialIndex(i);
         fSig_minus->SetTargetMaterialIndex(i);
         fMaterialIndex = i ;
      }
      virtual void  SetBeamEnergy(Double_t en){
         InSANEInclusiveDiffXSec::SetBeamEnergy(en);
         fSig_0->SetBeamEnergy(en);
         fSig_plus->SetBeamEnergy(en);
         fSig_minus->SetBeamEnergy(en);
      }

      virtual void  SetPhaseSpace(InSANEPhaseSpace * ps);
      virtual void  InitializeFinalStateParticles();

   ClassDef(InSANEInclusivePionElectroProductionXSec,1)
};



#endif

