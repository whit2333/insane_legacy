#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedefs;

#pragma link C++ nestedclass;

#pragma link C++ class InSANEWaveFunction+;
#pragma link C++ class BonnDeuteronWaveFunction+;

#pragma link C++ class InSANEFragmentationFunctions+;
#pragma link C++ class DSSFragmentationFunctions+;

#pragma link C++ class InSANETransverseMomentumDistributions+;
#pragma link C++ class MAIDExclusivePionDiffXSec3+;

#pragma link C++ class InSANEStrongCouplingConstant+;

#pragma link C++ class SANE_RCs_Model0+;
#pragma link C++ class SANE_RCs_Model1+;

#pragma link C++ class InSANEPhotoAbsorptionCrossSections+;
#pragma link C++ class MAIDPhotoAbsorptionCrossSections+;
#pragma link C++ class InSANEStructureFunctionsFromVPCSs+;

#pragma link C++ function maid07tp_(double*,double*,double*,double*,double*,double*)+; 
#pragma link C++ function maid07tot_(int *, double*,double*,  double*,double*,double*,double*,double*,double*,double*,double*)+; 

#endif

