#ifndef InSANEInelasticRadiativeTail2_HH
#define InSANEInelasticRadiativeTail2_HH 1

#include "InSANERADCOR.h"
#include "InSANEPOLRAD.h"
#include "InSANEInclusiveDiffXSec.h"
#include "InSANEPOLRADInternalPolarizedDiffXSec.h"

/** Inelastic Radiative tail with corrections.
 *  
 * \ingroup xsections
 */
class InSANEInelasticRadiativeTail2 : public InSANEInclusiveDiffXSec {

   protected:
      mutable InSANEPOLRAD * fPOLRAD;          ///< Used to get the (internal) polarized 
      mutable InSANERADCOR * fRADCOR;          ///< Used to get the (internal and external)
      Double_t       fRadLen[2];               ///< [0] before and [1] after scattering

      void CreatePOLRAD() const ;
      void CreateRADCOR() const ;

      Bool_t   fAddRegion4;                                // adds 2d integral over region IV (see Mt 1969)
      Bool_t   fInternalOnly;                              //

   public:
      InSANEInelasticRadiativeTail2();
      virtual ~InSANEInelasticRadiativeTail2();

      void SetRegion4(bool v = true) { fAddRegion4 = v; }
      void SetInternalOnly(bool v = true) { fInternalOnly = v; }

      void SetPolarizations(Double_t pe, Double_t pt){
         GetPOLRAD()->SetPolarizations(pe,pt,0.0);
         TVector3 p = GetTargetPolarization();
         p.SetMag(pt);
         SetTargetPolarization(p);
      } 

      InSANEPOLRAD * GetPOLRAD() const { if(!fPOLRAD) CreatePOLRAD(); return fPOLRAD; }
      InSANERADCOR * GetRADCOR() const { if(!fRADCOR) CreateRADCOR(); return fRADCOR; }

      /** If only one argument is given, we assume it is the total target thickness.
       * Note: tb = "target before"
       *       ta = "target after"
       */
      virtual void SetTargetThickness(Double_t tb, Double_t ta = 0.0){
         if(ta == 0.0) {
            fRadLen[0] = tb/2.0;
            fRadLen[1] = tb/2.0;
         } else {
            fRadLen[0] = tb;
            fRadLen[1] = ta;
         }
      }

      virtual Double_t EvaluateXSec(const Double_t *x) const ;

      virtual Double_t EvaluateBaseXSec(const Double_t *x) const {
         return InSANEInclusiveDiffXSec::EvaluateXSec(x);
      }

      ClassDef(InSANEInelasticRadiativeTail2,1)
};

#endif

