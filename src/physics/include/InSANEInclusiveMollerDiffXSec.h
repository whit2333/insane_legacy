#ifndef InSANEInclusiveMollerDiffXSec_H
#define InSANEInclusiveMollerDiffXSec_H 1

#include "InSANEInclusiveDiffXSec.h"
#include "TMath.h"
#include "InSANEFormFactors.h"
#include "TVector3.h"


/** Moller 
 * This cross section is differential in the beam energy (not scattered)
 * \ingroup inclusiveXSec
 */
class InSANEInclusiveMollerDiffXSec : public InSANEInclusiveDiffXSec {

   public:
      InSANEInclusiveMollerDiffXSec();
      virtual ~InSANEInclusiveMollerDiffXSec();

      virtual InSANEInclusiveMollerDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANEInclusiveMollerDiffXSec::Clone()\n";
         auto * copy = new InSANEInclusiveMollerDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANEInclusiveMollerDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Virtual method that returns the calculated values of dependent variables. This method
       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
       *
       *  For example, in mott scattering (elastic) there is really only two random variables,
       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
       *  (assuming the beam energy is known).
       *
       */
      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual void InitializePhaseSpaceVariables();

      double  MollerCrossSection_CM(double E0, double sin2th) const ;
      double  MollerCrossSection_CM_dE(double E0, double sin2th) const ;
      double  MollerCrossSection_LAB(double E0, double theta) const;

      /** Evaluate Cross Section (nb/sr) */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      double CosTheta2(double Ebeam, double E2);

      ClassDef(InSANEInclusiveMollerDiffXSec, 1)
};


#endif

