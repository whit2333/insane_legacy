// -------------------------------------------------------
//
//namespace insane {
//  namespace physics {
// 
//    double VirtualComptonScatteringAsymmetries::Get(double x, double Q2, std::tuple<ComptonAsymmetry,Nuclei> sf, Twist t, OPELimit l) const
//    {
//      auto sftype = std::get<ComptonAsymmetry>(sf);
//      auto target = std::get<Nuclei>(sf);
//
//      switch(sftype) {
//
//        case ComptonAsymmetry::A1 : 
//          return A1(target,x,Q2);
//          break;
//
//        case ComptonAsymmetry::A2 : 
//          return A2(target,x,Q2);
//          break;
//      }
//      return 0.0;
//    }
//    //______________________________________________________________________________
//
//    double VirtualComptonScatteringAsymmetries::A1(   Nuclei target, double x, double Q2, Twist t) const
//    {
//      switch(target) {
//
//        case Nuclei::p : 
//          return A1p(x,Q2);
//          break;
//
//        case Nuclei::n : 
//          return A1n(x,Q2);
//          break;
//      }
//      return 0.0;
//    }
//    //______________________________________________________________________________
//
//    double VirtualComptonScatteringAsymmetries::A2(   Nuclei target, double x, double Q2, Twist t) const
//    {
//      switch(target) {
//
//        case Nuclei::p : 
//          return A2p(x,Q2);
//          break;
//
//        case Nuclei::n : 
//          return A2n(x,Q2);
//          break;
//      }
//      return 0.0;
//    }
//    //______________________________________________________________________________
//    
//    double VirtualComptonScatteringAsymmetries::A1_NoTMC(   Nuclei target, double x, double Q2, Twist t) const
//    {
//      switch(target) {
//
//        case Nuclei::p : 
//          return A1p_NoTMC(x,Q2);
//          break;
//
//        case Nuclei::n : 
//          return A1n_NoTMC(x,Q2);
//          break;
//      }
//      return 0.0;
//    }
//    //______________________________________________________________________________
//
//    double VirtualComptonScatteringAsymmetries::A2_NoTMC(   Nuclei target, double x, double Q2, Twist t) const
//    {
//      switch(target) {
//
//        case Nuclei::p : 
//          return A2p_NoTMC(x,Q2);
//          break;
//
//        case Nuclei::n : 
//          return A2n_NoTMC(x,Q2);
//          break;
//      }
//      return 0.0;
//    }
//    //______________________________________________________________________________
//
//    double VirtualComptonScatteringAsymmetries::R(Nuclei target, double x, double Q2) const
//    {
//      return InSANE::Kine::R1998(x,Q2);
//    }
//    //______________________________________________________________________________
//
//    double VirtualComptonScatteringAsymmetries::A2_PositivityBound(Nuclei target, double x, double Q2) const
//    {
//      // Soffer-Teryaev  Positivity bound.
//      // |A_2| < \sqrt{R(1+A_1)/2}
//      using namespace TMath;
//      double Rover2 = R(target, x,Q2)/2.0;
//      double result = Sqrt(Abs(Rover2*(1.0+A1(target, x,Q2))));
//      return result;
//    }
//    //______________________________________________________________________________
//
//  }
//}



