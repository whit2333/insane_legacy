#ifndef InSANEStrongCouplingConstant_HH
#define InSANEStrongCouplingConstant_HH 1

#include "TMath.h"

/** Strong coupling constant in the MSbar scheme.
 */
class InSANEStrongCouplingConstant {

   protected:
      double n_f;
      double fLambda;

   public: 
      InSANEStrongCouplingConstant();
      virtual ~InSANEStrongCouplingConstant();
      double operator()(double mu2);

      double b0(double mu2) const ;
      double b1(double mu2) const ;
      double b2(double mu2) const ;
      double b3(double mu2) const ;

      ClassDef(InSANEStrongCouplingConstant,1)
};

#endif
