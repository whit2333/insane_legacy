#ifndef SANETargets_HH
#define SANETargets_HH 1

#include "InSANETarget.h"

/** UVA's polarized Ammonia target.
 *
 * \ingroup Apparatus
 */
class UVAPolarizedAmmoniaTarget : public InSANETarget {

   public:
      UVAPolarizedAmmoniaTarget(const char * name = "UVA-polarized-ammonia-target", 
            const char * title = "UVA Polarized Ammonia Target",
            Double_t pf = 0.6);
      virtual ~UVAPolarizedAmmoniaTarget();

      virtual void DefineMaterials();
      

      ClassDef(UVAPolarizedAmmoniaTarget,7)
};

/** UVA's Carbon target.
 *
 * \ingroup Apparatus
 */
class UVACarbonTarget : public InSANETarget {

   public:
      UVACarbonTarget(const char * name = "UVA-Carbon-target", const char * title = "UVA Carbon Target");
      virtual ~UVACarbonTarget();

      virtual void DefineMaterials();

      ClassDef(UVACarbonTarget,7)
};

/** Cross Hair.
 *
 * \ingroup Apparatus
 */
class UVACrossHairTarget : public InSANETarget {

   public:
      UVACrossHairTarget(const char * name = "UVA-Cross-Hair-target", const char * title = "UVA Cross-Hair Target");
      virtual ~UVACrossHairTarget();

      virtual void DefineMaterials();

      ClassDef(UVACrossHairTarget,7)
};

/** UVA's Carbon target.
 *
 * \ingroup Apparatus
 */
class UVAPureHeliumTarget : public InSANETarget {

   public:
      UVAPureHeliumTarget(const char * name = "UVA-PureHelium-target", const char * title = "UVA PureHelium Target");
      virtual ~UVAPureHeliumTarget();

      virtual void DefineMaterials();

      ClassDef(UVAPureHeliumTarget,7)
};



#endif

