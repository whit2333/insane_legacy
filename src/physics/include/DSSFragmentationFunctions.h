#ifndef DSSFragmentationFunctions_HH
#define DSSFragmentationFunctions_HH

#include "InSANEFragmentationFunctions.h"

/** DSS Fragmentation Functions.
 * http://inspirehep.net/record/746992
 *
 * @ingroup fragfuncs
 */
class DSSFragmentationFunctions : public InSANEFragmentationFunctions {

   protected:

      Double_t fPar_piplus_u_plus_ubar[5];
      Double_t fPar_piplus_d_plus_dbar[5];
      Double_t fPar_piplus_ubar_eq_d[5];
      Double_t fPar_piplus_s_plus_sbar[5];
      Double_t fPar_piplus_c_plus_cbar[5];
      Double_t fPar_piplus_b_plus_bbar[5];
      Double_t fPar_piplus_g[5];

      Double_t fPar_Kplus_u_plus_ubar[5];
      Double_t fPar_Kplus_d_plus_dbar[5];
      Double_t fPar_Kplus_s_plus_sbar[5];
      Double_t fPar_Kplus_ubar_eq_s[5];
      Double_t fPar_Kplus_c_plus_cbar[5];
      Double_t fPar_Kplus_b_plus_bbar[5];
      Double_t fPar_Kplus_g[5];

      Double_t fNprime;

      // Model function (at input scale)
      Double_t D_model(Double_t z,Double_t *pars);

   public:

      DSSFragmentationFunctions();
      virtual ~DSSFragmentationFunctions();

      virtual Double_t D_u_piplus( Double_t z, Double_t Q2)   ;
      virtual Double_t D_u_piminus(Double_t z, Double_t Q2)   ;

      virtual Double_t D_ubar_piplus( Double_t z, Double_t Q2);
      virtual Double_t D_ubar_piminus(Double_t z, Double_t Q2);

      virtual Double_t D_d_piplus( Double_t z, Double_t Q2)   ;
      virtual Double_t D_d_piminus(Double_t z, Double_t Q2)   ;

      virtual Double_t D_dbar_piplus( Double_t z, Double_t Q2);
      virtual Double_t D_dbar_piminus(Double_t z, Double_t Q2);

      virtual Double_t D_s_piplus( Double_t z, Double_t Q2)   ;
      virtual Double_t D_s_piminus(Double_t z, Double_t Q2)   ;

      virtual Double_t D_sbar_piplus( Double_t z, Double_t Q2);
      virtual Double_t D_sbar_piminus(Double_t z, Double_t Q2);

      // kaon FFs
      virtual Double_t D_u_Kplus( Double_t z, Double_t Q2)    ;
      virtual Double_t D_u_Kminus(Double_t z, Double_t Q2)    ;

      virtual Double_t D_ubar_Kplus( Double_t z, Double_t Q2) ;
      virtual Double_t D_ubar_Kminus(Double_t z, Double_t Q2) ;

      virtual Double_t D_d_Kplus( Double_t z, Double_t Q2)    ;
      virtual Double_t D_d_Kminus(Double_t z, Double_t Q2)    ;

      virtual Double_t D_dbar_Kplus( Double_t z, Double_t Q2) ;
      virtual Double_t D_dbar_Kminus(Double_t z, Double_t Q2) ;

      virtual Double_t D_s_Kplus( Double_t z, Double_t Q2)    ;
      virtual Double_t D_s_Kminus(Double_t z, Double_t Q2)    ;

      virtual Double_t D_sbar_Kplus( Double_t z, Double_t Q2) ;
      virtual Double_t D_sbar_Kminus(Double_t z, Double_t Q2) ;


   ClassDef(DSSFragmentationFunctions,1)
};
#endif

