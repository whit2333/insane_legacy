#ifndef InSANEPOLRADBornDiffXSec_HH
#define InSANEPOLRADBornDiffXSec_HH

#include "InSANEPOLRADInternalPolarizedDiffXSec.h"

/** Born Cross Section.
 *  Used as the base class for all POLRAD cross-sections which use 
 *  the phase space variables E',theta, and phi.
 *
 * \ingroup inclusiveXSec
 */
class InSANEPOLRADBornDiffXSec: public InSANEPOLRADInternalPolarizedDiffXSec {

   public:
      InSANEPOLRADBornDiffXSec();
      virtual ~InSANEPOLRADBornDiffXSec();
      virtual InSANEPOLRADBornDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANEPOLRADBornDiffXSec::Clone()\n";
         auto * copy = new InSANEPOLRADBornDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANEPOLRADBornDiffXSec*  Clone() const { return( Clone("") ); } 
      virtual void InitializePhaseSpaceVariables() ;
      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      virtual void SetTargetPolarization(const TVector3 &P){
         // make sure to add call to this in override
         GetPOLRAD()->SetPolarizationVectors(fTargetPol,GetHelicity());
         InSANEDiffXSec::SetTargetPolarization(P);
      }
      virtual void       SetHelicity(Double_t h) { 
         // make sure to add call to this in override
         GetPOLRAD()->SetHelicity(h);
         InSANEDiffXSec::SetHelicity(h);
      }
      virtual void SetPolarizations(Double_t pe, Double_t pt){
         // make sure to add call to this in override
         GetPOLRAD()->SetPolarizations(pe,pt,0.0);
         InSANEDiffXSec::SetPolarizations(pe,pt);
      } 

      ClassDef(InSANEPOLRADBornDiffXSec,1)
};

#endif

