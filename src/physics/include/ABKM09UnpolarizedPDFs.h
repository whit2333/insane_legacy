#ifndef ABKM09UnpolarizedPDFs_H 
#define ABKM09UnpolarizedPDFs_H 

#include "InSANEPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#include "TMath.h"

/** ABKM09 unpolarized PDFs.  
  * 
  * Reference:  Phys. Rev. D 81, 014032 (2010) 
  * 
  * \ingroup updfs
  */ 
class ABKM09UnpolarizedPDFs: public InSANEPartonDistributionFunctions{

   private: 
      int fN_f;   // number of flavors 
      int fPar;   // used for error calculations 
      int fOrder; // NLO (1) or NNLO (2); default is NLO 

   public: 
      ABKM09UnpolarizedPDFs(int Nf=5,int par=0,int order=1);
      virtual ~ABKM09UnpolarizedPDFs();

      Double_t *GetPDFs(Double_t,Double_t);
      Double_t *GetPDFErrors(Double_t,Double_t){return fPDFErrors;}

      ClassDef(ABKM09UnpolarizedPDFs,1)

}; 

#endif 
