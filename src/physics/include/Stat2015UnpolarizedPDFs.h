#ifndef Stat2015UnpolarizedPDFs_HH
#define Stat2015UnpolarizedPDFs_HH 2 

#include "InSANEPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"

/** Statistical 2015 PDFs.
 * C. BOURRELY and J. SOFFER
 * arXiv:1502.02517[hep-ph] submitted to EPJC
 * http://inspirehep.net/record/1343508
 *
 * \ingroup ppdfs
 */
class Stat2015UnpolarizedPDFs : public InSANEPartonDistributionFunctions {

   private:
      int    fiPol;
      int    fiSingle;
      int    fiNum;
      double pdfs[13];

   public:

      Stat2015UnpolarizedPDFs(); 
      virtual ~Stat2015UnpolarizedPDFs(); 

      virtual Double_t *GetPDFs(Double_t x, Double_t Q2); 
      virtual Double_t *GetPDFErrors(Double_t x, Double_t Q2); 

      ClassDef(Stat2015UnpolarizedPDFs,2)
};

#endif

