#ifndef InSANERADCORRadiatedDiffXSec_HH 
#define InSANERADCORRadiatedDiffXSec_HH 1 

#include "TMath.h"
#include "InSANEInclusiveDiffXSec.h"
#include "InSANEPolarizedStructureFunctionsFromPDFs.h"
#include "DSSVPolarizedPDFs.h"
#include "DNS2005PolarizedPDFs.h"
#include "InSANEPolarizedCrossSectionDifference.h"
#include "InSANERADCOR.h"
#include "InSANEFormFactors.h"
#include "MSWFormFactors.h"
#include "AmrounFormFactors.h"
#include "F1F209eInclusiveDiffXSec.h"

/** Base class for using RADCOR to get a cross section.
 */
class InSANERADCORRadiatedDiffXSec: public InSANEInclusiveDiffXSec{

   private: 
      Int_t fApprox;        /// 0 = Exact, 1 = Energy peaking approximation 
      Int_t fMultiPhoton;   /// Multiple photon correction: 0 = off, 1 = on 

   protected:

      InSANERADCOR *fRADCOR;

   public: 
      InSANERADCORRadiatedDiffXSec();
      virtual ~InSANERADCORRadiatedDiffXSec();
      virtual InSANERADCORRadiatedDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANERADCORRadiatedDiffXSec::Clone()\n";
         auto * copy = new InSANERADCORRadiatedDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANERADCORRadiatedDiffXSec*  Clone() const { return( Clone("") ); } 

      InSANERADCOR * GetRADCOR(){ return fRADCOR; }

      // This should be done by direct acces to RADCOR via GetRADCOR()
      void UseApprox(char ans){
         if(ans=='y'){
            std::cout << "[InSANERADCORRadiatedDiffXSec]: Using peaking approximation." << std::endl;
            fApprox = 1;
         }else if(ans=='n'){
            std::cout << "[InSANERADCORRadiatedDiffXSec]: Using exact integral." << std::endl;
            fApprox = 0;
         }else{
            std::cout << "[InSANERADCORRadiatedDiffXSec]: Invalid choice.  Exiting..." << std::endl;
            exit(1);
         }
      }          

      void UseInternal(bool ans=true){ fRADCOR->UseInternal(ans); }
      void UseExternal(bool ans=true){ fRADCOR->UseExternal(ans); }

      void SetTargetNucleus(const InSANENucleus& t){InSANEDiffXSec::SetTargetNucleus(t); fRADCOR->SetTargetNucleus(t); }

      void SetPolarizedCrossSectionDifference(InSANEInclusiveDiffXSec *f){fRADCOR->SetPolDiffXS(f);}
      void SetUnpolarizedCrossSection(InSANEInclusiveDiffXSec *f){fRADCOR->SetUnpolXS(f);}
      void SetPolarizationType(int p){fPolType = p; fRADCOR->SetPolarization(p);} 
      void SetVerbosity(int v){fRADCOR->SetVerbosity(v);} 
      void SetThreshold(Int_t t){fRADCOR->SetThreshold(t);}  
      void SetDeltaM(Double_t dM){fRADCOR->SetDeltaM(dM);}
      void SetMultiplePhoton(Int_t c){fRADCOR->SetMultiplePhoton(c);}            
      void SetRadiationLengths(Double_t *T){fRADCOR->SetRadiationLengths(T);}

      Double_t EvaluateXSec(const Double_t *) const;

      ClassDef(InSANERADCORRadiatedDiffXSec,1) 

}; 

#endif 

