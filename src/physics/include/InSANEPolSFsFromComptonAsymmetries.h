#ifndef InSANEPolSFsFromComptonAsymmetries_HH
#define InSANEPolSFsFromComptonAsymmetries_HH

#include "InSANEPolarizedStructureFunctions.h"
#include "InSANEFunctionManager.h"
#include "InSANEVirtualComptonAsymmetries.h"

/** Uses A1 and A2 along with an instance of InSANEStructureFunctions (F1) to get g1 and g2.
 *
 * \f$ g1 = F1/(1+gamma^2)(A1 + gamma A2) \f$ 
 * \f$ g2 = F1/(1+gamma^2)(A2/gamma - A1) \f$ 
 * \f$ gamma^2  = 4M^2x^2/Q^2 \f$
 */
class InSANEPolSFsFromComptonAsymmetries : public InSANEPolarizedStructureFunctions {
   protected:
      InSANEStructureFunctions          * fSFs;
      InSANEVirtualComptonAsymmetries   * fA1A2;

   public:
      InSANEPolSFsFromComptonAsymmetries(){
         InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
         fSFs = fman->GetStructureFunctions();
      }
      virtual ~InSANEPolSFsFromComptonAsymmetries(){}

      void  SetVirtualComptonAsymmetries(InSANEVirtualComptonAsymmetries * v){ fA1A2 = v; }
      InSANEVirtualComptonAsymmetries * GetVirtualComptonAsymmetries(){ return fA1A2; }

      virtual Double_t A1p(Double_t x,Double_t Q2){
         if(fA1A2)
            return fA1A2->A1p(x,Q2);
         /*else*/
         Error("A1p","No InSANEVirtualComptonAsymmetries defined");
         return 0.0;
      }
      virtual Double_t A2p(Double_t x,Double_t Q2){
         if( fA1A2) 
            return fA1A2->A2p(x,Q2); 
         /*else*/
         Error("A1p","No InSANEVirtualComptonAsymmetries defined");
         return 0.0;
      }

      virtual Double_t g1p(Double_t x, Double_t Q2){
         Double_t  gamma   = 2.0*(M_p/GeV)*x/TMath::Sqrt(Q2);
         Double_t  gamma2  = gamma*gamma;
         Double_t  F1      = fSFs->F1p(x,Q2);
         return( (F1/(1.0+gamma2))*(A1p(x,Q2)+gamma*A2p(x,Q2)) );
      }
      virtual Double_t g2p(Double_t x, Double_t Q2){
         Double_t  gamma   = 2.0*(M_p/GeV)*x/TMath::Sqrt(Q2);
         Double_t  gamma2  = gamma*gamma;
         Double_t  F1      = fSFs->F1p(x,Q2);
         return( (F1/(1.0+gamma2))*(A2p(x,Q2)/gamma - A1p(x,Q2)) );
      }

      // neutron 
      virtual Double_t g1n(Double_t x,Double_t Q2) {return 0 ; }
      // deuteron 
      virtual Double_t g1d(Double_t x,Double_t Q2) {return 0 ; }
      // He3 
      virtual Double_t g1He3(Double_t x,Double_t Q2) {return 0 ; }

      // neutron 
      virtual Double_t g2n(Double_t x,Double_t Qsq) {return 0 ; } 
      // deuteron 
      virtual Double_t g2d(Double_t x,Double_t Qsq) {return 0 ; }
      // 3He  
      virtual Double_t g2He3(Double_t x,Double_t Qsq) {return 0 ; } 

ClassDef(InSANEPolSFsFromComptonAsymmetries,1)
};


#endif

