#ifndef InSANEPhysics_h
#define InSANEPhysics_h 1

// Convenience header that includes all the physics headers.
// Perhaps this is better split up?

// Cross sections
#include "InSANEDiffXSec.h"
// inclusive
#include "InSANEInclusiveDiffXSec.h"
#include "InSANECompositeDiffXSec.h"
#include "InSANEGridDiffXSec.h"
//#include "InSANEInclusiveDISXSec.h"
#include "InSANEInclusiveBornDISXSec.h"
#include "InSANEeInclusiveElasticDiffXSec.h"
#include "InSANEpInclusiveElasticDiffXSec.h"
#include "MAIDNucleusInclusiveDiffXSec.h" 
#include "MAIDInclusiveDiffXSec.h" 
#include "MAIDInclusiveElectronDiffXSec.h" 
#include "MAIDInclusivePionDiffXSec.h" 
//#include "MAIDInclusivePi0DiffXSec.h" 

//exclusive
#include "InSANEExclusiveDiffXSec.h"
#include "InSANEepElasticDiffXSec.h"
#include "MAIDPolarizedTargetDiffXSec.h" 
#include "MAIDExclusivePionDiffXSec.h" 
#include "MAIDExclusivePionDiffXSec2.h" 

// ummm....
#include "InSANEPOLRADInternalPolarizedDiffXSec.h"
//#include "InSANEPOLRADElasticDiffXSec.h"
//#include "InSANEPOLRADBornDiffXSec.h"
//#include "InSANEPOLRADRadiatedDiffXSec.h"
//#include "InSANEPOLRADElasticTailDiffXSec.h"
//#include "InSANEPOLRADQuasiElasticTailDiffXSec.h"
//#include "InSANEPOLRADInelasticTailDiffXSec.h"
//#include "InSANEPOLRADUltraRelInelasticTailDiffXSec.h"
#include "InSANERadiativeTail.h"
#include "InSANEElasticRadiativeTail.h"
#include "InSANEInelasticRadiativeTail.h"
#include "InSANEInelasticRadiativeTail2.h"
//#include "InSANEFullInelasticRadiativeTail.h"
//#include "InSANEFlatInclusiveDiffXSec.h"
//#include "InSANEFlatExclusiveDiffXSec.h"
#include "InSANEElectroProductionXSec.h"
//#include "InSANEInclusiveMottXSec.h"
//#include "InSANEExclusiveMottXSec.h"

#include "OARPionDiffXSec.h"
//#include "OARPionElectroDiffXSec.h"
//#include "OARPionPhotoDiffXSec.h"
//#include "ElectroOARPionDiffXSec.h"
//#include "PhotoOARPionDiffXSec.h"

#include "WiserXSection.h"
//#include "ElectroWiserDiffXSec.h"
//#include "InSANEInclusiveWiserXSec.h"
//#include "WiserInclusivePhotoXSec.h"
//#include "WiserInclusiveElectroXSec.h"
//#include "ElectroWiserDiffXSec.h"
//#include "PhotoWiserDiffXSec.h"

#include "CTEQ6eInclusiveDiffXSec.h"
#include "F1F209eInclusiveDiffXSec.h"
//#include "F1F209QuasiElasticDiffXSec.h"
//#include "InSANEPolIncDiffXSec.h"
#include "QuasiElasticInclusiveDiffXSec.h" 
#include "QFSInclusiveDiffXSec.h"
#include "InSANECrossSectionDifference.h"

//#include "InSANEPolarizedDiffXSec.h"
#include "PolarizedDISXSec.h"
//#include "PolarizedDISXSecParallelHelicity.h"
//#include "PolarizedDISXSecAntiParallelHelicity.h"

#include "EPCVXSection.h"
//#include "InSANEInclusiveEPCVXSec.h"
//#include "InSANEInclusiveEPCVXSec2.h"

// --------------------------------------------
// PDFs
#include "InSANEPartonDistributionFunctions.h"

// unpolarized :
#include "CTEQ6UnpolarizedPDFs.h"
#include "CTEQ10UnpolarizedPDFs.h"
#include "CJ12UnpolarizedPDFs.h" 
#include "ABKM09UnpolarizedPDFs.h" 
#include "MSTW08UnpolarizedPDFs.h" 
#include "MRST2001UnpolarizedPDFs.h" 
#include "MRST2002UnpolarizedPDFs.h" 
#include "MRST2006UnpolarizedPDFs.h" 
#include "StatisticalUnpolarizedPDFs.h"
#include "Stat2015UnpolarizedPDFs.h"
#include "BBSUnpolarizedPDFs.h"


// polarized :
#include "GSPolarizedPDFs.h"
#include "JAMPolarizedPDFs.h"
#include "JAM15PolarizedPDFs.h"
#include "DNS2005PolarizedPDFs.h"
#include "DSSVPolarizedPDFs.h"
#include "AAC08PolarizedPDFs.h"
#include "BBSPolarizedPDFs.h"
#include "BBPolarizedPDFs.h"
#include "LSS2006PolarizedPDFs.h"
#include "LSS2010PolarizedPDFs.h"
#include "StatisticalPolarizedPDFs.h"
#include "Stat2015PolarizedPDFs.h"
#include "LCWFPartonDistributionFunctions.h"

#include "MAIDVirtualComptonAsymmetries.h"
#include "InSANEPhotoAbsorptionCrossSections.h"
#include "MAIDPhotoAbsorptionCrossSections.h"

// Unpolarized Structure Functions
#include "InSANEStructureFunctions.h"
#include "InSANEStructureFunctionsFromPDFs.h"
#include "InSANEStructureFunctionsFromVPCSs.h"
#include "InSANECompositeStructureFunctions.h"
//#include "LHAPDFStructureFunctions.h"
#include "F1F209StructureFunctions.h"
#include "NMC95StructureFunctions.h"

// Polarized Structure Functions
#include "InSANEPolarizedStructureFunctions.h"
#include "InSANEPolarizedStructureFunctionsFromPDFs.h"
#include "InSANEPolarizedStructureFunctionsFromVCSAs.h"
#include "InSANECompositePolarizedStructureFunctions.h"


// Form Factors
#include "InSANEFormFactors.h"
#include "InSANEDipoleFormFactors.h"
#include "AMTFormFactors.h"
#include "KellyFormFactors.h"
#include "GalsterFormFactors.h"
#include "RiordanFormFactors.h"
#include "BostedFormFactors.h"
#include "MSWFormFactors.h"
#include "BilenkayaFormFactors.h"
#include "AmrounFormFactors.h"
#include "F1F209QuasiElasticFormFactors.h"

#endif

