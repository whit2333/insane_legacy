#ifndef InSANEPolarizedCrossSectionDifference_H 
#define InSANEPolarizedCrossSectionDifference_H 1 

#include <iostream>
#include "InSANENucleus.h"
#include "InSANEFormFactors.h"
#include "InSANEPhysicalConstants.h"
#include "InSANEInclusiveDiffXSec.h"
#include "InSANEFortranWrappers.h"
#include "InSANEPolarizedStructureFunctions.h"

class InSANEPolarizedCrossSectionDifference: public InSANEInclusiveDiffXSec {

   private:
      bool fUseScalingFunction; 
      Int_t fPhysType;      ///< 0 => elastic, 1 => QE; 2 => inelastic (default)  
      //InSANENucleus fNucleus; 
      //InSANEPolarizedStructureFunctions *fPolSFs;
      //InSANEFormFactors *fFF;

      void ProcessPolarizedStructureFunctions(Double_t,Double_t,Double_t &,Double_t &) const;

      Double_t ScalingFunction(Double_t) const; 

   public:
      InSANEPolarizedCrossSectionDifference();
      virtual ~InSANEPolarizedCrossSectionDifference(); 

      void InitializePhaseSpaceVariables() {
         InSANEInclusiveDiffXSec::InitializePhaseSpaceVariables();
      }

      void UseScalingFunction(bool ans=true){
         fUseScalingFunction = ans; 
         if(fUseScalingFunction){
            std::cout << "[InSANEPolarizedCrossSectionDifference]: Will use scaling function. " << std::endl;
         }else{
            std::cout << "[InSANEPolarizedCrossSectionDifference]: Will not use scaling function. " << std::endl;
         }
      }

      void SetPhysicsType(Int_t t){
          fPhysType = t; 
          switch(fPhysType){
             case 0: 
                std::cout << "[InSANEPolarizedCrossSectionDifference]: ";
                std::cout << "Will do ELASTIC calculation.  Be sure to set the appropriate form factors! " << std::endl; 
                break;
             case 1: 
                std::cout << "[InSANEPolarizedCrossSectionDifference]: ";
                std::cout << "Will do QE calculation.  Be sure to set the appropriate form factors! " << std::endl; 
                break;
             case 2:  
                std::cout << "[InSANEPolarizedCrossSectionDifference]: ";
                std::cout << "Will do INELASTIC calculation.  Be sure to set the appropriate polarized structure functions! " << std::endl; 
                break;
          } 
      }  

      //void SetTargetType(InSANENucleus::NucleusType i){fNucleus.SetType(i);} 
      //void SetPolarizedStructureFunctions(InSANEPolarizedStructureFunctions *psfs){fPolSFs = psfs;}
      //void SetFormFactors(InSANEFormFactors *f){ fFF = f; }

      Double_t EvaluateXSec(const Double_t *) const; 
      Double_t EvaluateAsymmetry(const Double_t *) const { return(0.0);} 

      ClassDef(InSANEPolarizedCrossSectionDifference,1) 

};

#endif 
