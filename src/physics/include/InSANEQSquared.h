//#ifndef InSANEQSquared_h
//#define InSANEQSquared_h
//#include <TROOT.h>
//
//#include "InSANEPhysics.h"
//#include <TMath.h>
//#include <iostream>
//#include <TNamed.h>
//
///** Concrete functor for the virtual photon 4-momentum squared and positive
// *
// * \f[ Q^2( \theta ,E^\prime ) = 4E^E \prime sin^2(\theta/2) \f]
// *
// * Note that this implementation is essentially a constant.
// *
// * \ingroup Physics
// */
//class InSANEQSquared : public InSANEPhysics  {
//public :
//   InSANEQSquared();
//   ~InSANEQSquared();
//
//   void PrintFormula();
//
///// return the calculated value after setting variables and parameters
//
//   virtual Double_t GetValue();
//
//   /**
//    * Evaluate the function, Q^2(\theta,E\prime) = 4EE\prime sin^2(\theta/2) , explicitly giving variables (useful for TF1)
//    *
//    * Variables are x[0]= \theta  and x[1]= E\prime , while the only parameter is p[0]= E .
//    */
//   double operator()(double *x, double *p) ;
//
//
//private :
//
///// Calculate the value if it can, returns true if calculated properly
//   Bool_t CalculateValue();
//
//
//
//   ClassDef(InSANEQSquared, 1)
//};
//
//#endif

