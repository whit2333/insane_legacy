#ifndef KellyFormFactors_HH
#define KellyFormFactors_HH 1

#include "InSANEDipoleFormFactors.h"

/** Kelly form factors. 
  *
  * paper reference: Phys. Rev. C 70, 068202 (2004) 
  *
  * \ingroup formfactors 
  */ 
class KellyFormFactors: public InSANEDipoleFormFactors{ 

   private: 
      Double_t fa_gep[2],fb_gep[4];
      Double_t fa_gmp[2],fb_gmp[4];
      Double_t fa_gmn[2],fb_gmn[4];
      Double_t fA,fB; 

   protected:
      Double_t G(int type,Double_t Q2);

   public: 
      KellyFormFactors();
 
      virtual ~KellyFormFactors();

      virtual Double_t GEp(Double_t Q2);
      virtual Double_t GMp(Double_t Q2);
      virtual Double_t GEn(Double_t Q2); 
      virtual Double_t GMn(Double_t Q2); 

      ClassDef(KellyFormFactors,1) 

}; 
#endif

