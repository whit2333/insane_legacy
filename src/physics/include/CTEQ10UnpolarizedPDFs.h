#ifndef CTEQ10UnpolarizedPDFs_H  
#define CTEQ10UnpolarizedPDFs_H 1 

#include "InSANEPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#include "TMath.h"

/** CTEQ10 parton distribution functions.
 *
 * H.-L. Lai, M. Guzzi, J. Huston, Z. Li, P.M. Nadolsky, J. Pumplin and C.-P. Yuan
 * Reference: Phys. Rev. D 82, 074024 (2010) 
 *
 * \ingroup updfs
 */
class CTEQ10UnpolarizedPDFs : public InSANEPartonDistributionFunctions {

   public:

      CTEQ10UnpolarizedPDFs();
      virtual ~CTEQ10UnpolarizedPDFs();

      Double_t *GetPDFs(Double_t,Double_t);
      Double_t *GetPDFErrors(Double_t x,Double_t Q2){return fPDFErrors;}

      ClassDef(CTEQ10UnpolarizedPDFs,1)

};

#endif
