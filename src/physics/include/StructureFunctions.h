#ifndef insane_physics_StructureFunctions_HH 
#define insane_physics_StructureFunctions_HH 

#include "InSANEMathFunc.h"
#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include "Physics.h"

namespace insane {
  namespace physics {

    /**  Structure Functions interface.
     *  At this level an implementation not involving the pdfs,
     *  e.g., an empircal fit, will ignore the twist argument.
     *  The implementation SSFsFromPDFs will not ignore the twist argument. 
     */
    class StructureFunctions : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {

      protected:
        std::string      fLabel;

        // Storage of the x and Q2 values last used to compute the structure function
        // indexed by enum class insane::physics::SF
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fx_values;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fQ2_values;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fValues;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fUncertainties;

      public:
        StructureFunctions();
        virtual ~StructureFunctions();

        void        SetLabel(const char * l){ fLabel = l;}
        const char* GetLabel() const { return fLabel.c_str(); }

        double GetXBjorken(std::tuple<SF,Nuclei> sf) const;
        double GetQSquared(std::tuple<SF,Nuclei> sf) const;

        bool   IsComputed( std::tuple<SF,Nuclei> sf, double x, double Q2) const;

        void Reset();

        /** Calculate and return distribution value.
         * Returns the current value for flavor f but checks that the 
         * distributions have already been calculated at (x,Q2). 
         * It uses IsComputed(x,Q2) to do this check.
         */
        virtual double Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const ;
        //virtual double  Get(std::tuple<SF,Nuclei> sf, double x, double Q2) const ;

        /** Get current distribution value.
         * Note: this method should only be used to get the stored values 
         * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
         */ 
        double  Get(std::tuple<SF,Nuclei> sf) const ;

        /** Virtual method should get all values of SFs. 
         * This also sets internal values of fx and fQsquared for use by IsComputed()
         */
        virtual double  Calculate    (double x, double Q2, 
            std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}
        virtual double  Uncertainties(double x, double Q2, 
            std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}

        virtual double F1(    double x, double Q2, Nuclei target, Twist t = Twist::All) const;
        virtual double F2(    double x, double Q2, Nuclei target, Twist t = Twist::All) const;
        virtual double F1_TMC(double x, double Q2, Nuclei target, Twist t = Twist::All) const;
        virtual double F2_TMC(double x, double Q2, Nuclei target, Twist t = Twist::All) const;
       
        virtual double F1p(double x, double Q2) const = 0;
        virtual double F2p(double x, double Q2) const = 0;
        virtual double F1n(double x, double Q2) const = 0;
        virtual double F2n(double x, double Q2) const = 0;
        virtual double F1p_TMC(double x, double Q2) const = 0;
        virtual double F2p_TMC(double x, double Q2) const = 0;
        virtual double F1n_TMC(double x, double Q2) const = 0;
        virtual double F2n_TMC(double x, double Q2) const = 0;

        /** Longiutdinal structure function.
         *
         * \f$ F_L = \rho^2 F_2 - 2x F_1) = 2x F_1 R \f$
         * \f$ \rho = \sqrt{1+ \frac{4M^2x^2}{Q^2}} \f$
         */
        virtual double FL(double x, double Q2, Nuclei target, Twist t = Twist::All) const;

        /** Ratio of longitudinal to transverse cross sections.
         * \f$ R = \sigma_L/\sigma_T \f$
         */
        virtual double R(double x, double Q2, Nuclei target, Twist t = Twist::All) const;


        // Rnp = F2n/F2p
        //virtual Double_t Rnp(Double_t x, Double_t Q2);
        //virtual Double_t Rnp_Error(Double_t x, Double_t Q2);

        ClassDef(StructureFunctions,1)
    };
  }
}

#endif
