#ifndef InSANEPOLRADElasticTailDiffXSec_HH
#define InSANEPOLRADElasticTailDiffXSec_HH

#include "InSANEPOLRADBornDiffXSec.h"

/** Elastic Raditaive Tail. 
 *
 *
 * \ingroup inclusiveXSec
 */
class InSANEPOLRADElasticTailDiffXSec: public InSANEPOLRADBornDiffXSec {
   public:
      InSANEPOLRADElasticTailDiffXSec();
      virtual ~InSANEPOLRADElasticTailDiffXSec();
      virtual InSANEPOLRADElasticTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANEPOLRADElasticTailDiffXSec::Clone()\n";
         auto * copy = new InSANEPOLRADElasticTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANEPOLRADElasticTailDiffXSec*  Clone() const { return( Clone("") ); } 
      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(InSANEPOLRADElasticTailDiffXSec,1)
};

#endif

