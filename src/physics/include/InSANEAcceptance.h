#ifndef InSANEAcceptance_HH
#define InSANEAcceptance_HH 1

#include "TNamed.h"
#include "TList.h"
#include "TMath.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"

/**  Experimental, spectrometer, or detector acceptance.
 * 
 */ 
class InSANEAcceptance : public TNamed {
   protected:
      TList fFunctions; 

      Double_t fBeamEnergy;
      Double_t fEnergyMin;
      Double_t fEnergyMax;
      Double_t fThetaMin;
      Double_t fThetaMax;

      Double_t fEnergy;
      Double_t fTheta;
      Double_t fxMin;
      Double_t fxMax;
      Double_t fQ2Min;
      Double_t fQ2Max;
      Double_t fWMin;
      Double_t fWMax;

   public:
      InSANEAcceptance(const char * n = "", const char * t = "");
      ~InSANEAcceptance();

      void SetBeamEnergy(Double_t e) {fBeamEnergy = e;}
      void SetEnergyMin(Double_t e) {fEnergyMin = e; }
      void SetEnergyMax(Double_t e) {fEnergyMax = e; }
      void SetThetaMin(Double_t t) {fThetaMin = t; }
      void SetThetaMax(Double_t t) {fThetaMax = t; }

      /** Using the defined variables, functions are generated 
       */	
      void Initialize();

      TH2F * KinematicCoverage_xQ2(Int_t N = 100);


   ClassDef(InSANEAcceptance,1)
};


#endif

