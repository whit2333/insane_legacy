#include "InSANEFortranWrappers.h"
#include "InSANEMathFunc.h"
#include "WiserXSection.h"
#include "WiserInclusivePhotoXSec.h"

//==============================================================================

template <class T>
InclusiveHadronProductionXSec<T>::InclusiveHadronProductionXSec()
{
   fTitle = "InclusiveHadronProductionXSec";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";
   fPIDs.clear();
   fPIDs.push_back(111);//
   fProtonXSec  = new T();
   fProtonXSec->SetTargetNucleus(InSANENucleus::Proton());
   fNeutronXSec = new T();
   fNeutronXSec->SetTargetNucleus(InSANENucleus::Neutron());
   fProtonXSec->UsePhaseSpace(false);
   fNeutronXSec->UsePhaseSpace(false);
   fAlphaCT  = 0.85;
   fID = 10000000 + fProtonXSec->GetID();
}
//_____________________________________________________________________________

template <class T>
InclusiveHadronProductionXSec<T>::~InclusiveHadronProductionXSec()
{
}
//______________________________________________________________________________

template <class T>
InclusiveHadronProductionXSec<T>::InclusiveHadronProductionXSec(const InclusiveHadronProductionXSec<T>& rhs) : 
   InSANECompositeDiffXSec(rhs)
{
   (*this) = rhs;
}
//______________________________________________________________________________

template <class T>
InclusiveHadronProductionXSec<T>& InclusiveHadronProductionXSec<T>::operator=(const InclusiveHadronProductionXSec<T>& rhs) 
{
   if (this != &rhs) {  // make sure not same object
      InSANECompositeDiffXSec::operator=(rhs);
      fProtonXSec  = rhs.fProtonXSec->Clone();
      fNeutronXSec = rhs.fNeutronXSec->Clone();
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

template <class T>
InclusiveHadronProductionXSec<T>*  InclusiveHadronProductionXSec<T>::Clone(const char * newname) const 
{
   std::cout << "InclusiveHadronProductionXSec<T>::Clone()\n";
   auto * copy = new InclusiveHadronProductionXSec<T>();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

template <class T>
InclusiveHadronProductionXSec<T>*  InclusiveHadronProductionXSec<T>::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________

template <class T>
Double_t  InclusiveHadronProductionXSec<T>::EvaluateXSec(const Double_t * x) const
{
   //std::cout << " composite eval " << std::endl;
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   if( !fProtonXSec )  return 0.0;
   if( !fNeutronXSec ) return 0.0;

   Double_t z    = GetZ();
   //if( z>0.0 ) z = 1.0;
   Double_t n    = GetN();
   //if( n>0.0 ) n = 1.0;
   Double_t a    = GetA();

   Double_t sigP = fProtonXSec->EvaluateXSec(x);
   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
   //std::cout << " sig_p = " << sigP << std::endl;
   //std::cout << " sig_n = " << sigN << std::endl;
   Double_t xsec = z*sigP + n*sigN;

   // Take into account nuclear transparency sigma_A = A^alpha sigma_p
   //if( a > 2.0 ) xsec = (xsec/a)*TMath::Power(a, 0.9);//fAlphaCT);

   if( IncludeJacobian() ) return xsec*TMath::Sin(x[1]);
   return xsec;
}
//______________________________________________________________________________

