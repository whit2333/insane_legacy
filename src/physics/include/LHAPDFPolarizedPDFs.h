#ifndef LHAPDFPolarizedPDFs_HH
#define LHAPDFPolarizedPDFs_HH 2 

#include "InSANEPolarizedPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#ifndef __CINT__
#include "LHAPDF/LHAPDF.h"
#else
namespace LHAPDF {
   enum SetType { EVOLVE = 0, LHPDF = 0, INTERPOLATE = 1, LHGRID = 1};
}
#endif
#include "TMath.h"

/** LHAPDF interface for concrete implementation of parton distributions
 *
 *  From the <a href="http://projects.hepforge.org/lhapdf/manual">LHAPDF Manual</a>:
 *  \code
 *  std::vector< double > xfx(const double &x;, const double &Q;);
 *  // returns a vector xf(x, Q) with index 0 < i < 12.
 *  // 0..5 = tbar, ..., ubar, dbar;
 *  // 6 = g;
 *  // 7..12 = d, u, ..., t
 *  double xfx(const double &x;, const double &Q;, int fl);
 *  // returns xf(x, Q) for flavour fl - this time the flavour encoding
 *  // is as in the LHAPDF manual...
 *  // -6..-1 = tbar,...,ubar, dbar
 *  // 1..6 = duscbt
 *  // 0 = g
 * \endcode
 *
 * \ingroup ppdfs
 */
class LHAPDFPolarizedPDFs : public InSANEPolarizedPartonDistributionFunctions {
	public:
		Int_t fSubset;
	public:
		LHAPDFPolarizedPDFs();
		virtual ~LHAPDFPolarizedPDFs(); 


		virtual Double_t *GetPDFs(Double_t x,Double_t Qsq);
                virtual Double_t *GetPDFErrors(Double_t x,Double_t Q2){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}


		/** wrapper to set type = 0 = LHAPDF::LHGRID
		 *   type = 1 (or nonzero) = LHAPDF::EVOLVE
		 */
		void SetPDFType(const char * pdfset, Int_t type = 0) ;

		/** Change LHAPDF data set */
		void SetPDFDataSet(const char * pdfset, LHAPDF::SetType type = LHAPDF::LHGRID, Int_t subset = 0) ;

		/** up quark distribution, \f$ u(x) \f$ */
		//    virtual Double_t u(Double_t x, Double_t Qsq) {
		//       return(LHAPDF::xfx(x, TMath::Sqrt(Qsq), 2)/x);
		//    }

		/** down quark distribution, \f$ d(x) \f$  */
		//    virtual Double_t d(Double_t x, Double_t Qsq) {
		//       return(LHAPDF::xfx(x, TMath::Sqrt(Qsq), 1)/x);
		//    }

		/** up anti-quark distribution, \f$ \bar{u}(x) \f$  */
		//    virtual Double_t uBar(Double_t x, Double_t Qsq) {
		//       return(LHAPDF::xfx(x, TMath::Sqrt(Qsq), -2)/x);
		//    }

		/** down anti-quark distribution, \f$ \bar{d}(x) \f$  */
		//    virtual Double_t dBar(Double_t x, Double_t Qsq) {
		//       return(LHAPDF::xfx(x, TMath::Sqrt(Qsq), -1)/x);
		//    }

		ClassDef(LHAPDFPolarizedPDFs,2)
};


#endif
