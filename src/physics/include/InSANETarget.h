#ifndef InSANETarget_HH
#define InSANETarget_HH 1

//#include "InSANEApparatus.h"
#include "InSANEMathFunc.h"
#include "InSANETargetMaterial.h"
#include "TList.h"
#include "TBrowser.h"
#include "TNamed.h"
#include <iostream>
#include "InSANEBremsstrahlungRadiator.h"

//#include "InSANEFunctionManager.h"

/** Base class for a fixed target.
 *
 * \ingroup Apparatus
 */
class InSANETarget : public TNamed {

   protected:

      TList    fMaterials;
      Int_t    fNMaterials;
      Double_t fPackingFraction;

      InSANEBremsstrahlungRadiator  fBremRadiator; 

   public:

      TGeoVolume * fTopVolume; //!

   public:

      InSANETarget(const char * name = "aTarget", const char * title = "A Target");
      virtual ~InSANETarget();
      InSANETarget(const InSANETarget & tg);
      InSANETarget(const InSANETarget * tg);
      InSANETarget& operator=(const InSANETarget& tg); 

      virtual void DefineMaterials() {}

      virtual void InitFromFile();

      Bool_t IsFolder() const { return kTRUE; }
      void Browse(TBrowser* b) {
         b->Add(&fMaterials, "Materials");
      }

      /** Add a new material to target */
      void AddMaterial(InSANETargetMaterial * mat) ;

      /** Get material by name. Returns 0 if it is not found. */
      InSANETargetMaterial * GetMaterial(const char * name) const ;

      /** Get material by name. Returns 0 if it is not found. */
      InSANETargetMaterial * GetMaterial(Int_t i) const ;

      /** Print the target and materials */
      void Print(const Option_t * opt = "") const ; // *MENU*
      void Print(std::ostream &stream) const ;

      Double_t GetAttenuationLength() const ;

      /** Return the radiation length in g/cm^2. This doesn't make much
       *  sense for a composite target so GetNumberOfRadiationLengths is
       *  a more useful quantity.   
       */
      Double_t GetRadiationLength() const ;

      /** returns the length density in units of nuclei per cm^2*/
      Double_t GetLengthDensity() const ;

      /** Return the radiation length as a fraction of the energy lost. */
      Double_t GetNumberOfRadiationLengths() const ;

      Int_t GetNMaterials() const { return fNMaterials; }

      Double_t GetPackingFraction(){ return fPackingFraction; }
      void     SetPackingFraction(Double_t pf) { fPackingFraction = pf; }

      void     DrawTarget(Option_t * opt = "ogl"); 

      ClassDef(InSANETarget,7)
};

/** Simple, single nucleus target. 
 *
 * \ingroup Apparatus
 */
class InSANESimpleTarget : public InSANETarget {
   public:
      InSANESimpleTarget(const char * name = "InSANESimpleTarget", const char * title = "Simple Target");
      virtual ~InSANESimpleTarget();
      virtual void DefineMaterials();
      ClassDef(InSANESimpleTarget,1)
};

/** A target with windows. 
 *
 * \ingroup Apparatus
 */
class InSANESimpleTargetWithWindows : public InSANETarget {
   public:
      InSANESimpleTargetWithWindows(const char * name = "InSANESimpleTarget", const char * title = "Simple Target");
      virtual ~InSANESimpleTargetWithWindows();
      virtual void DefineMaterials();
      ClassDef(InSANESimpleTargetWithWindows,1)
};


#endif
