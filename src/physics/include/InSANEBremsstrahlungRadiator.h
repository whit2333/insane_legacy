#ifndef InSANEBremsstrahlungRadiator_HH
#define InSANEBremsstrahlungRadiator_HH

#include <vector>
#include <map>
#include <utility>
#include <algorithm>
#include "InSANETargetMaterial.h"
#include "TObject.h"
#include "InSANEMathFunc.h"

/** Bremsstrahlung photon spectrum for an arbitrary radiator.
 *  Useful for calculating the photo-production cross section
 *  on targets that have more than one material. That is it is used to accumulate
 *  the bremsstralung photons as the beam moves through the target. This provides the correct 
 *  bremsstrahlung spectrum for the photo production cross section to use.
 *
 *  <h4>How this should be used:</h4>
 *     - Construct a "target" (InSANETarget). This holds the instance of the bremsstrahlung radiator.
 *     - Each "material" (InSANETargetMaterial) of the  should be assigned a unique number
 *     - the materials are (arbitrarily constructed) and added to the target. 
 *     - When added the 
 */
class InSANEBremsstrahlungRadiator : public TObject {

   public:

      std::multimap<double,int>             fZPositions_map;
      std::vector< std::pair<int,double> >  fZPositions;
      std::map<int,double>                  fRadiatorLengths;
      Double_t                              fTotalRL;
      std::map<int,double>                  fCumulativeRL;  // raditor length for all up stream materials (not including the indexed material)

   public :
      InSANEBremsstrahlungRadiator();
      virtual ~InSANEBremsstrahlungRadiator();
      InSANEBremsstrahlungRadiator(const InSANEBremsstrahlungRadiator&) = default;               // Copy constructor
      InSANEBremsstrahlungRadiator(InSANEBremsstrahlungRadiator&&) = default;                    // Move constructor
      InSANEBremsstrahlungRadiator& operator=(const InSANEBremsstrahlungRadiator&) & = default;  // Copy assignment operator
      InSANEBremsstrahlungRadiator& operator=(InSANEBremsstrahlungRadiator&&) & = default;       // Move assignment operato

      //InSANEBremsstrahlungRadiator(const InSANEBremsstrahlungRadiator& v); 
      //InSANEBremsstrahlungRadiator& operator=(const InSANEBremsstrahlungRadiator& v); 

      void     AddRadiator(InSANETargetMaterial * mat);
      Double_t GetRadiatorLength( Int_t i );

      // I_gamma returns the bremsstrahlung intensity at the
      // target material, id, including all upstream radiators
      // at the photo energy k_gamma, 
      // for an initial electron beam energy E0
      Double_t    I_gamma( Int_t id, Double_t k_gamma, Double_t E0 ) const ;

      void Print(Option_t * opt = "") const;
      void Print(std::ostream &stream) const ;

      ClassDef(InSANEBremsstrahlungRadiator,1)
};

#endif

