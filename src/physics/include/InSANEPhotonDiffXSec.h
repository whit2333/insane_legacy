#ifndef InSANEPhotonDiffXSec_HH
#define InSANEPhotonDiffXSec_HH 1

#include "InSANEInclusiveDiffXSec.h"
#include "InSANEFortranWrappers.h"
#include "InSANEMathFunc.h"
#include "InSANECompositeDiffXSec.h"
#include <vector>


/** Mono energetic photon beam cross section for inclusive particle production.
 *
 * \ingroup inclusiveXSec
 */
class InSANEPhotonDiffXSec : public InSANEInclusiveDiffXSec {

   private:
      mutable double      fArgCopy[6];

   protected:
      TParticlePDG *      fParticle; //->
      Double_t            fRadiationLength;
      std::vector<double> fParameters;

   public:
      std::vector<double> fParameters0;
      std::vector<double> fParameters1;
      std::vector<double> fParameters2;

   public:
      InSANEPhotonDiffXSec();
      InSANEPhotonDiffXSec(const InSANEPhotonDiffXSec& rhs);
      virtual ~InSANEPhotonDiffXSec();
      InSANEPhotonDiffXSec& operator=(const InSANEPhotonDiffXSec& rhs);
      virtual InSANEPhotonDiffXSec*  Clone(const char * newname) const ;
      virtual InSANEPhotonDiffXSec*  Clone() const ; 

      Double_t     GetRadiationLength() {            return fRadiationLength; }
      virtual void SetRadiationLength(Double_t r) { fRadiationLength = r; }

      //TParticlePDG * GetParticlePDG()  { return fParticle; }
      //Int_t          GetParticleType() { return fParticle->PdgCode(); }
      void           SetProductionParticleType(Int_t PDGcode, Int_t i=0);
      void           SetParticlePDGEncoding(Int_t PDGcode) { SetProductionParticleType(PDGcode); }

      void                       SetParameters(const std::vector<double>& pars) { fParameters = pars; }
      const std::vector<double>& GetParameters() const { return fParameters; }
      
      virtual void InitializePhaseSpaceVariables();

      double       FitFunction(double * x, const std::vector<double>& p) const ;
      double       FitFunction2(double * x, const std::vector<double>& p) const ;
      double       FitFunction3(double * x, const std::vector<double>& p) const ;
      double       FitFunction4(double * x, const std::vector<double>& p) const ;
      double       FitFunction5(double * x, const std::vector<double>& p) const ;

      Double_t EvaluateXSec(const Double_t * x) const ;

      ClassDef(InSANEPhotonDiffXSec, 1)
};


#endif

