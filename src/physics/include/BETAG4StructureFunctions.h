#ifndef BETAG4StructureFunctions_H
#define BETAG4StructureFunctions_H 1

#include "TMath.h"
#include "InSANEStructureFunctions.h"

/**
 */
class BETAG4StructureFunctions : public InSANEStructureFunctions {
public:

   BETAG4StructureFunctions();

   virtual ~BETAG4StructureFunctions();

   virtual Double_t F1p(Double_t x, Double_t Qsq);

   virtual Double_t F2p(Double_t x, Double_t Qsq);

   virtual Double_t F1n(Double_t x, Double_t Qsq);

   virtual Double_t F2n(Double_t x, Double_t Qsq);

   virtual Double_t F1d(Double_t x, Double_t Qsq);

   virtual Double_t F2d(Double_t x, Double_t Qsq);

   virtual Double_t g1p(Double_t x, Double_t Qsq);

   virtual Double_t g2p(Double_t x, Double_t Qsq);


   ClassDef(BETAG4StructureFunctions, 1)
};



#endif



