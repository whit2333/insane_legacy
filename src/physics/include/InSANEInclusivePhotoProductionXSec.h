#ifndef InSANEInclusivePhotoProductionXSec_HH
#define InSANEInclusivePhotoProductionXSec_HH

#include "InSANEPhotonDiffXSec.h"


/**  Inclusive photo production of pions, kaons, and nucleons from a nucleon target.
 *   For all targets use InclusivePhotoProductionXSec.
 * \ingroup inclusiveXSec
 */
class InSANEInclusivePhotoProductionXSec : public InSANEPhotonDiffXSec {

   protected:
      InSANEPhotonDiffXSec * fSig_0;       //->
      InSANEPhotonDiffXSec * fSig_plus;    //->
      InSANEPhotonDiffXSec * fSig_minus;   //->

   public:
      InSANEInclusivePhotoProductionXSec();
      InSANEInclusivePhotoProductionXSec(const InSANEInclusivePhotoProductionXSec& rhs) ;
      virtual ~InSANEInclusivePhotoProductionXSec();
      InSANEInclusivePhotoProductionXSec& operator=(const InSANEInclusivePhotoProductionXSec& rhs) ;
      virtual InSANEInclusivePhotoProductionXSec*  Clone(const char * newname) const ;
      virtual InSANEInclusivePhotoProductionXSec*  Clone() const ; 

      Double_t EvaluateXSec(const Double_t * x) const ;

      void  SetParameters(int i, const std::vector<double>& pars);
      const std::vector<double>& GetParameters() const ;

      virtual void   SetRadiationLength(Double_t r) { 
         ((InSANEPhotonDiffXSec*)fSig_0)->SetRadiationLength(r);
         ((InSANEPhotonDiffXSec*)fSig_plus)->SetRadiationLength(r);
         ((InSANEPhotonDiffXSec*)fSig_minus)->SetRadiationLength(r);
      }
      // Here we copied a bunch of setters from InSANEDiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void       SetTargetMaterial(const InSANETargetMaterial& mat){
         fSig_0->SetTargetMaterial(mat);
         fSig_plus->SetTargetMaterial(mat);
         fSig_minus->SetTargetMaterial(mat);
         fTargetMaterial = mat;
      }
      void SetTargetNucleus(const InSANENucleus & targ){
         InSANEInclusiveDiffXSec::SetTargetNucleus(targ);
         //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
         //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
      }

      virtual void InitializePhaseSpaceVariables();

      void       SetTargetMaterialIndex(Int_t i){
         fSig_0->SetTargetMaterialIndex(i);
         fSig_plus->SetTargetMaterialIndex(i);
         fSig_minus->SetTargetMaterialIndex(i);
         fMaterialIndex = i ;
      }
      virtual void  SetBeamEnergy(Double_t en){
         InSANEInclusiveDiffXSec::SetBeamEnergy(en);
         fSig_0->SetBeamEnergy(en);
         fSig_plus->SetBeamEnergy(en);
         fSig_minus->SetBeamEnergy(en);
      }

      virtual void  SetPhaseSpace(InSANEPhaseSpace * ps);
      virtual void  InitializeFinalStateParticles();

   ClassDef(InSANEInclusivePhotoProductionXSec,1)
};



/** Wiser cross section for arbitrary nuclear target. 
 *
 * \ingroup inclusiveXSec
 */
class InclusivePhotoProductionXSec : public InSANECompositeDiffXSec {

   public:
      InclusivePhotoProductionXSec();
      InclusivePhotoProductionXSec(const InclusivePhotoProductionXSec& rhs) ;
      virtual ~InclusivePhotoProductionXSec();
      InclusivePhotoProductionXSec& operator=(const InclusivePhotoProductionXSec& rhs) ;
      virtual InclusivePhotoProductionXSec*  Clone(const char * newname) const ;
      virtual InclusivePhotoProductionXSec*  Clone() const ; 

      void  SetParameters(int i, const std::vector<double>& pars) {
         ((InSANEInclusivePhotoProductionXSec*)fProtonXSec)->SetParameters(i,pars);
         ((InSANEInclusivePhotoProductionXSec*)fNeutronXSec)->SetParameters(i,pars);
      }
      const std::vector<double>& GetParameters() const  {
         return( ((InSANEInclusivePhotoProductionXSec*)fProtonXSec)->GetParameters() );
      }

      TParticlePDG * GetParticlePDG() { return ((InSANEInclusivePhotoProductionXSec*)fProtonXSec)->GetParticlePDG(); } 
      Int_t          GetParticleType() { return ((InSANEInclusivePhotoProductionXSec*)fProtonXSec)->GetParticlePDG()->PdgCode(); }

      Double_t GetRadiationLength(){ 
         return( ((InSANEInclusivePhotoProductionXSec*)fProtonXSec)->GetRadiationLength());
      }
      void   SetRadiationLength(Double_t r) { 
         ((InSANEInclusivePhotoProductionXSec*)fProtonXSec )->SetRadiationLength(r);
         ((InSANEInclusivePhotoProductionXSec*)fNeutronXSec)->SetRadiationLength(r);
      }

      void SetProductionParticleType(Int_t PDGcode, Int_t part = 0) {
         SetParticleType(PDGcode);
         ((InSANEInclusivePhotoProductionXSec*)fProtonXSec)->SetProductionParticleType(PDGcode);
         ((InSANEInclusivePhotoProductionXSec*)fNeutronXSec)->SetProductionParticleType(PDGcode);
         switch(PDGcode) {
            case  111 : fID = fBaseID + 0;   break;
            case -211 : fID = fBaseID + 100; break;
            case  211 : fID = fBaseID + 200; break;
            default   : fID = fBaseID + 300; break;
         }
      }

      void SetParticlePDGEncoding(Int_t PDGcode) { SetProductionParticleType(PDGcode); }

      virtual Double_t  EvaluateXSec(const Double_t * x) const ;

   ClassDef(InclusivePhotoProductionXSec,1)
};


#endif

