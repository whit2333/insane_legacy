#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;

#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

//#pragma link C++ namespace CLHEP;
//#pragma link C++ namespace InSANE;
//#pragma link C++ namespace InSANE::Kine;
//#pragma link C++ namespace InSANE::Math;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;
#pragma link C++ namespace insane::physics::TMCs;
#pragma link C++ namespace insane::kinematics;

#pragma link C++ enum  insane::physics::SF;
#pragma link C++ enum  insane::physics::FormFactor;
#pragma link C++ enum  insane::physics::Nuclei;
#pragma link C++ enum  insane::physics::Parton;
#pragma link C++ enum  insane::physics::ComptonAsymmetry;

#pragma link C++ class std::tuple<insane::physics::FormFactor,insane::physics::Nuclei>+;
#pragma link C++ class std::tuple<insane::physics::SF,insane::physics::Nuclei>+;
#pragma link C++ class std::tuple<insane::physics::ComptonAsymmetry,insane::physics::Nuclei>+;

//#pragma link C++ class std::mutex+;

#pragma link C++ class insane::physics::CrossSectionInfo+;
#pragma link C++ global insane::physics::fgInSANEFunctionManager+;

#pragma link C++ class insane::physics::PDFBase+;
#pragma link C++ enum  insane::physics::PDFBase::PartonFlavor;

#pragma link C++ class insane::physics::UnpolarizedPDFs+;
#pragma link C++ class insane::physics::PolarizedPDFs+;
#pragma link C++ class insane::physics::Twist3DistributionFunctions+;
#pragma link C++ class insane::physics::Twist4DistributionFunctions+;
#pragma link C++ class insane::physics::T3DFsFromTwist3SSFs+;

#pragma link C++ class insane::physics::SpinStructureFunctions+;
#pragma link C++ class insane::physics::StructureFunctions+;

#pragma link C++ class insane::physics::CTEQ10_UPDFs+;
#pragma link C++ class std::tuple<insane::physics::CTEQ10_PDFs>+;

#pragma link C++ class insane::physics::JAM_PPDFs+;
#pragma link C++ class insane::physics::JAM_T3DFs+;
#pragma link C++ global insane::physics::fgInit_mutex+;
#pragma link C++ class insane::physics::JAM_T4DFs+;
#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs>+;
#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>+;
#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>+;

#pragma link C++ class insane::physics::Stat2015_UPDFs+;
#pragma link C++ class insane::physics::Stat2015_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::Stat2015_UPDFs>+;
#pragma link C++ class std::tuple<insane::physics::Stat2015_PPDFs>+;

#pragma link C++ class insane::physics::LCWF_T3DFs+;

#pragma link C++ class insane::physics::F1F209_SFs+;
#pragma link C++ class insane::physics::NMC95_SFs+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>+;

#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::CTEQ10_PDFs>+;

#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::Stat2015_UPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>+;

//#pragma link C++ class insane::physics::SFsFromPDFs< std::tuple<insane::physics::Stat2015_UPDFs>>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<std::tuple<insane::physics::Stat2015_PPDFs>>+;

//#pragma link C++ class insane::physics::VCSABase+;
#pragma link C++ class insane::physics::VirtualComptonScatteringAsymmetries+;
#pragma link C++ class insane::physics::VirtualPhotoAbsorptionCrossSections+;
//#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::F1F209_SFs,insane::physics::SpinStructureFunctions>+;
#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::SFsFromPDFs<Stat2015_UPDFs>, insane::physics::SSFsFromPDFs<Stat2015_PPDFs>>+;
#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::F1F209_SFs, insane::physics::SSFsFromPDFs<Stat2015_PPDFs>>+;
#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::NMC95_SFs, insane::physics::SSFsFromPDFs<Stat2015_PPDFs>>+;
//#pragma link C++ class insane::physics::VCSAsFromPDFs<insane::physics::Stat2015_UPDFs, insane::physics::Stat2015_PPDFs>+;
#pragma link C++ class insane::physics::MAID_VCSAs+;
#pragma link C++ class insane::physics::MAID_VPACs+;
#pragma link C++ class insane::physics::SFsFromVPACs<insane::physics::MAID_VPACs>+;
#pragma link C++ class insane::physics::SSFsFromVPACs<insane::physics::MAID_VPACs>+;
#pragma link C++ class insane::physics::CompositeSFs+;
#pragma link C++ class insane::physics::CompositeSSFs+;

//#pragma link C++ class std::map<int, double >+;
//#pragma link C++ class std::pair<int, double >+;
//#pragma link C++ class std::vector<int, std::pair<int, double > >+;
//-----------------------------------------------------------------------
//
#pragma link C++ class InSANETargetMaterial+;
#pragma link C++ class InSANETarget+;
#pragma link C++ class InSANESimpleTarget+;
#pragma link C++ class InSANESimpleTargetWithWindows+;
#pragma link C++ class UVAPolarizedAmmoniaTarget+;
#pragma link C++ class UVACarbonTarget+;
#pragma link C++ class UVACrossHairTarget+;
#pragma link C++ class UVAPureHeliumTarget+;

#pragma link C++ class InSANE_EG_Event+;
#pragma link C++ class InSANEEventGenerator+;
#pragma link C++ class InSANETargetEventGenerator+;
#pragma link C++ class BETAG4SavedEventGenerator+;

#pragma link C++ class InSANEBremsstrahlungRadiator+;
//#pragma link C++ class UVAPolarizedAmmoniaTarget+;
//#pragma link C++ class UVACarbonTarget+;
//#pragma link C++ class UVACrossHairTarget+;
//#pragma link C++ class UVAPureHeliumTarget+;

#pragma link C++ class std::vector<TString*>+;
#pragma link C++ class std::map<int, double >+;
#pragma link C++ class std::vector<TGraph*>+;

#pragma link C++ class std::map<Int_t,TFile*>+;
#pragma link C++ class std::pair<Int_t,TFile*>+;

#pragma link C++ class InSANELuminosity+;
#pragma link C++ class InSANEYield+;
#pragma link C++ class InSANEDilutionFactor+;
#pragma link C++ class InSANEDilutionFunction+;
#pragma link C++ class InSANEDilutionFromTarget+;
#pragma link C++ class InSANEAcceptance+;


//#pragma link C++ class InSANEWaveFunction+;
//#pragma link C++ class BonnDeuteronWaveFunction+;

#pragma link C++ class InSANEFragmentationFunctions+;
#pragma link C++ class DSSFragmentationFunctions+;

#pragma link C++ class InSANETransverseMomentumDistributions+;
#pragma link C++ class MAIDExclusivePionDiffXSec3+;

#pragma link C++ class InSANEStrongCouplingConstant+;

#pragma link C++ class SANE_RCs_Model0+;
#pragma link C++ class SANE_RCs_Model1+;

#pragma link C++ class InSANEPhotoAbsorptionCrossSections+;
#pragma link C++ class MAIDPhotoAbsorptionCrossSections+;
#pragma link C++ class InSANEStructureFunctionsFromVPCSs+;

#pragma link C++ function maid07tp_(double*,double*,double*,double*,double*,double*)+; 
#pragma link C++ function maid07tot_(int *, double*,double*,  double*,double*,double*,double*,double*,double*,double*,double*)+; 


#pragma link C++ class InSANEAsymmetryBase+;
#pragma link C++ class InSANEAsymmetriesFromStructureFunctions+;

#pragma link C++ class InSANEStructureFunctionBase+;
#pragma link C++ class InSANEStructureFunctions+;
#pragma link C++ class InSANECompositeStructureFunctions+;
#pragma link C++ class InSANECompositePolarizedStructureFunctions+;
#pragma link C++ class LowQ2StructureFunctions+;
#pragma link C++ class LowQ2PolarizedStructureFunctions+;

#pragma link C++ class InSANEPolarizedStructureFunctions+;
#pragma link C++ class F1F209StructureFunctions+;
#pragma link C++ class F1F209QuasiElasticStructureFunctions+;
#pragma link C++ class F1F209QuasiElasticFormFactors+; 
#pragma link C++ class NMC95StructureFunctions+;

#pragma link C++ class std::vector<InSANEStructureFunctions*>+;
#pragma link C++ class std::vector<InSANEPolarizedStructureFunctions*>+;
#pragma link C++ class std::vector<InSANEStructureFunctionsFromPDFs*>+;
#pragma link C++ class std::vector<InSANEPolarizedStructureFunctionsFromPDFs*>+;

#pragma link C++ class InSANEFormFactors+;
#pragma link C++ class InSANEDipoleFormFactors+;
#pragma link C++ class AMTFormFactors+;
#pragma link C++ class MSWFormFactors+;
#pragma link C++ class GalsterFormFactors+;
#pragma link C++ class KellyFormFactors+;
#pragma link C++ class RiordanFormFactors+;
#pragma link C++ class AmrounFormFactors+;
#pragma link C++ class BilenkayaFormFactors+;
#pragma link C++ class BostedFormFactors+; 

#pragma link C++ class InSANEPDFBase+;
#pragma link C++ enum  InSANEPDFBase::InSANEPartonFlavor;
#pragma link C++ class InSANEPartonDistributionFunctions+;
#pragma link C++ class InSANEPolarizedPartonDistributionFunctions+;
#pragma link C++ class InSANEPolarizedStructureFunctionsFromPDFs+;
#pragma link C++ class InSANEPolarizedStructureFunctionsFromVCSAs+;
#pragma link C++ class InSANEPolSFsFromComptonAsymmetries+;

#pragma link C++ class LCWFPartonDistributionFunctions+;
#pragma link C++ class LCWFPolarizedPartonDistributionFunctions+;

#pragma link C++ class InSANEPartonHelicityDistributions+;
#pragma link C++ class InSANEUnpolarizedPDFsFromPHDs+;
#pragma link C++ class InSANEPolarizedPDFsFromPHDs+;

//#pragma link C++ class LHAPDFStructureFunctions+;
#pragma link C++ class InSANEStructureFunctionsFromPDFs+;
#pragma link C++ class BETAG4StructureFunctions+;


#pragma link C++ class BBSQuarkHelicityDistributions+; 
#pragma link C++ class BBSUnpolarizedPDFs+; 
#pragma link C++ class BBSPolarizedPDFs+;
#pragma link C++ class AvakianQuarkHelicityDistributions+; 
#pragma link C++ class AvakianUnpolarizedPDFs+; 
#pragma link C++ class AvakianPolarizedPDFs+; 
#pragma link C++ class LSS98QuarkHelicityDistributions+; 
#pragma link C++ class LSS98UnpolarizedPDFs+; 
#pragma link C++ class LSS98PolarizedPDFs+; 
#pragma link C++ class StatisticalQuarkFits+;
#pragma link C++ class StatisticalUnpolarizedPDFs+;
#pragma link C++ class StatisticalPolarizedPDFs+;
#pragma link C++ class Stat2015UnpolarizedPDFs+;
#pragma link C++ class Stat2015PolarizedPDFs+;
//#pragma link C++ class LHAPDFUnpolarizedPDFs+;
//#pragma link C++ class LHAPDFPolarizedPDFs+;
#pragma link C++ class AAC08PolarizedPDFs+;
#pragma link C++ class BBPolarizedPDFs+;
#pragma link C++ class JAMPolarizedPDFs+;
#pragma link C++ class JAM15PolarizedPDFs+;
#pragma link C++ class MHKPolarizedPDFs+;
#pragma link C++ class DNS2005PolarizedPDFs+;
#pragma link C++ class LSS2006PolarizedPDFs+;
#pragma link C++ class LSS2010PolarizedPDFs+;
#pragma link C++ class DSSVPolarizedPDFs+;

#pragma link C++ class InSANEPhaseSpace+;
#pragma link C++ class InSANEPhaseSpaceVariable+;
#pragma link C++ class InSANEDiscretePhaseSpaceVariable+;
#pragma link C++ class std::vector<InSANEPhaseSpaceVariable*>+;

#pragma link C++ class InSANEDiffXSec+;
#pragma link C++ class InSANECompositeDiffXSec+;
#pragma link C++ class InSANEDiffXSecKinematicKey+;
#pragma link C++ class InSANEGridDiffXSec+;
#pragma link C++ class InSANEGridXSecValue+;
#pragma link C++ class InSANEInclusiveDiffXSec+;
#pragma link C++ class InSANEInclusiveDISXSec+;
#pragma link C++ class InSANEInclusiveBornDISXSec+;
#pragma link C++ class InSANEExclusiveDiffXSec+;

#pragma link C++ class MAIDInclusiveDiffXSec+; 
#pragma link C++ class MAIDNucleusInclusiveDiffXSec+; 
#pragma link C++ class MAIDKinematicKey+; 
#pragma link C++ class MAIDPolarizedTargetDiffXSec+; 
#pragma link C++ class MAIDPolarizedKinematicKey+; 

#pragma link C++ class InSANE_VCSABase+; 
#pragma link C++ class InSANEVirtualComptonAsymmetries+; 
#pragma link C++ class InSANEVirtualComptonAsymmetriesModel1+; 
#pragma link C++ class MAIDVirtualComptonAsymmetries+; 

#pragma link C++ class MAIDExclusivePionDiffXSec+; 
#pragma link C++ class MAIDInclusiveElectronDiffXSec+; 
#pragma link C++ class MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_2D_integrand+; 
#pragma link C++ class MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_theta+; 
#pragma link C++ class MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_phi+; 

#pragma link C++ class MAIDExclusivePionDiffXSec2+; 

#pragma link C++ class MAIDInclusivePionDiffXSec+; 
#pragma link C++ class MAIDInclusivePionDiffXSec::MAIDXSec_5Fold_2D_integrand+; 
#pragma link C++ class MAIDInclusivePi0DiffXSec+; 

#pragma link C++ class InSANEPolarizedCrossSectionDifference+; 

#pragma link C++ class InSANERADCOR+;
#pragma link C++ class InSANERADCORVariables+;
#pragma link C++ class InSANERADCORKinematics+;

#pragma link C++ class InSANERADCOR::StripApproxIntegrand1Wrap+;
#pragma link C++ class InSANERADCOR::StripApproxIntegrand1_withDWrap+;
#pragma link C++ class InSANERADCOR::StripApproxIntegrand2Wrap+;
#pragma link C++ class InSANERADCOR::ExternalOnly_ExactInelasticIntegrandWrap+;
#pragma link C++ class InSANERADCOR::External2DEnergyIntegral_IntegrandWrap+;

#pragma link C++ class EsFuncWrap+;
#pragma link C++ class EpFuncWrap+;
// exact forms 
// internal 
#pragma link C++ class IntRadFuncWrap+;
#pragma link C++ class IntRadOmegaFuncWrap+;
#pragma link C++ class IntRadCosThkFuncWrap+;
// external 
#pragma link C++ class TExactFuncWrap+;
#pragma link C++ class EsExactFuncWrap+;
#pragma link C++ class EpExactFuncWrap+;
#pragma link C++ class EsExactFunc2Wrap+;
#pragma link C++ class EpExactFunc2Wrap+;

// Monte Carlo integration methods 
//#pragma link C++ function InSANERADCOR::MCAcceptReject(const int &,double (*)(double *),double *,double *,int,int,double &,double &); 
//#pragma link C++ function InSANERADCOR::MCSampleMean(const int &,double (*)(double *),double *,double *,int,double &,double &); 
//#pragma link C++ function InSANERADCOR::MCSampleMean(const int &,double (*)(double),double *,double *,int,double &,double &); 
//#pragma link C++ function InSANERADCOR::MCImportanceSampling(const int,double (*)(double *),double (*)(double *,double *,double *),double (*)(int,double *,double *),double *,double *,int,double &,double &); 
////// Adaptive Simpson integration method 
//#pragma link C++ function InSANERADCOR::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link C++ function InSANERADCOR::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 

// #pragma link C++ class InSANERADCOR::Full_Elastic_integrand+;

#pragma link C++ class InSANERADCORInternalUnpolarizedDiffXSec+;

#pragma link C++ class InSANERADCORRadiatedDiffXSec+;
#pragma link C++ class InSANERADCORRadiatedUnpolarizedDiffXSec+;

#pragma link C++ class InSANERADCOR2+;
#pragma link C++ class InSANERADCOR2::StripApproxIntegrand1Wrap+;
#pragma link C++ class InSANERADCOR2::StripApproxIntegrand1_withDWrap+;
#pragma link C++ class InSANERADCOR2::StripApproxIntegrand2Wrap+;
#pragma link C++ class InSANERADCOR2::External2DEnergyIntegral_IntegrandWrap+;
#pragma link C++ class InSANERADCOR2::External3DIntegral_IntegrandWrap+;


#pragma link C++ class InSANEPOLRADVariables+;
#pragma link C++ class InSANEPOLRADKinematics+;

#pragma link C++ class InSANEPOLRAD+;
//#pragma link C++ class INSANEPOLRAD::IRT_R_3+;
#pragma link C++ class InSANEPOLRAD::IRT_2D_integrand+;
#pragma link C++ class InSANEPOLRAD::ERTFuncWrap+;
#pragma link C++ class InSANEPOLRAD::QRTTauFuncWrap+;
#pragma link C++ class InSANEPOLRAD::QRTRFuncWrap+;
#pragma link C++ class InSANEPOLRAD::IRT_TauFuncWrap_delta+;
#pragma link C++ class InSANEPOLRAD::IRT_TauFuncWrap_2+;
#pragma link C++ class InSANEPOLRAD::IRT_RFuncWrap_3+;
#pragma link C++ class InSANEPOLRAD::QRT_TauFuncWrap_QE_Full+;
#pragma link C++ class InSANEPOLRAD::QRT_RFuncWrap_QE_Full+;
//#pragma link C++ function InSANEPOLRAD::SimpleIntegration(double (*)(double &), double,double,double,int); 
//#pragma link C++ function InSANEPOLRAD::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link C++ function InSANEPOLRAD::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 

#pragma link C++ class InSANEPOLRADUltraRelativistic+;
#pragma link C++ class InSANEPOLRADUltraRelativistic::SIG_r_2D_integrand+;
//#pragma link off function InSANEPOLRADUltraRelativistic::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link off function InSANEPOLRADUltraRelativistic::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 

#pragma link C++ class InSANEPOLRADInternalPolarizedDiffXSec+;
#pragma link C++ typedef InSANEPolradDiffXSec;
#pragma link C++ class InSANEPOLRADElasticDiffXSec+;
#pragma link C++ class InSANEPOLRADBornDiffXSec+;
#pragma link C++ class InSANEPOLRADRadiatedDiffXSec+;
#pragma link C++ class InSANEPOLRADElasticTailDiffXSec+;
#pragma link C++ class InSANEPOLRADQuasiElasticTailDiffXSec+;
#pragma link C++ class InSANEPOLRADInelasticTailDiffXSec+;
#pragma link C++ class InSANEPOLRADUltraRelInelasticTailDiffXSec+;
//#pragma link off function InSANEPOLRADInternalPolarizedDiffXSec::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link off function InSANEPOLRADInternalPolarizedDiffXSec::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 

#pragma link C++ class InSANERadiativeCorrections1D+;
#pragma link C++ class InSANERadiativeTail+;
//#pragma link C++ class InSANERadiativeTail2+;
#pragma link C++ class InSANEElasticRadiativeTail+;
//#pragma link C++ class InSANEQuasielasticRadiativeTail+;
#pragma link C++ class InSANEInelasticRadiativeTail+;
#pragma link C++ class InSANEInelasticRadiativeTail2+;
#pragma link C++ class InSANEFullInelasticRadiativeTail+;

#pragma link C++ class InSANEFlatInclusiveDiffXSec+;
#pragma link C++ class InSANEFlatExclusiveDiffXSec+;

#pragma link C++ class InSANEElectroProductionXSec+;

#pragma link C++ class InSANEInclusiveMottXSec+;
#pragma link C++ class InSANEExclusiveMottXSec+;
#pragma link C++ class InSANEepElasticDiffXSec+;
#pragma link C++ class InSANEMollerDiffXSec+;
#pragma link C++ class InSANEInclusiveMollerDiffXSec+;
#pragma link C++ class InSANEeInclusiveElasticDiffXSec+;
#pragma link C++ class InSANEpInclusiveElasticDiffXSec+;
#pragma link C++ class RCeInclusiveElasticDiffXSec+;

#pragma link C++ class InSANEBeamSpinAsymmetry+;
#pragma link C++ class InSANEBeamTargetAsymmetry+;
#pragma link C++ class InSANEPolarizedDISAsymmetry+;

#pragma link C++ class OARPionDiffXSec+;
#pragma link C++ class OARPionElectroDiffXSec+;
#pragma link C++ class OARPionPhotoDiffXSec+;
#pragma link C++ class ElectroOARPionDiffXSec+;
#pragma link C++ class PhotoOARPionDiffXSec+;

#pragma link C++ class InSANEInclusiveWiserXSec+;
#pragma link C++ class InSANEPhaseSpaceSampler+;

#pragma link C++ function formc_(double *)+; 
#pragma link C++ function formm_(double *)+; 

#pragma link C++ function inif1f209_()+;
#pragma link C++ function emc_09_(float * ,float * ,int *,float *)+;
#pragma link C++ function cross_tot_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_tot_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_mod_(double * ,double * ,double *,double * ,double * ,double * ,double * ,double * )+;

#pragma link C++ function EMC_Effect(double * ,double *)+;

#pragma link C++ function qfs_(double*,double*,double*,double*,double*, double*,double*, double*)+;
#pragma link C++ function qfs_born_(double*,double*,double*,double*,double*,double*,double*,double*, double*,double*, double*,int*)+;
#pragma link C++ function qfs_radiated_(double*,double*,double*,double*,double*,double*,double*,double*)+;
#pragma link C++ function wiser_sub_(double * ,int * ,double * ,double * , double * )+;
#pragma link C++ function wiser_all_sig_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_all_sig0_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_fit_(int * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function vtp_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function aac08pdf_(double*,double*,int*,double*,double**)+;
#pragma link C++ function dssvini_(int *)+;
#pragma link C++ function dssvfit_(double *,double *,double *,double *,double *,double *,double *,double *)+;
#pragma link C++ function partondf_(double *,double *,int *)+;
#pragma link C++ function partonevol_(double *x, double *Q2, int *ipol, double * pdf, int * isingle, int *num)+;
#pragma link C++ function mrst2002_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function mrst2001e_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function mrstpdfs_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function setctq6_(int*)+;
#pragma link C++ function getctq6_(int* ,double*, double*, double*)+;
#pragma link C++ function setct10_(int*)+; 
#pragma link C++ function getct10_(int*,double*,double*,double*)+; 
#pragma link C++ function getabkm09_(int*,double*,double*,int*,int*,int*,double*)+; 
#pragma link C++ function getmstw08_(int*,int*,double*,double*,double*)+; 
#pragma link C++ function ppdf_( int*, double*,double*,double*, double*,double*,double*, double*,double*,double*,double*,double*,double*,double*,double*)+;
#pragma link C++ function ini_()+;
#pragma link C++ function jam_xF_(double* res,double* x,double* Q2, char* flav)+;
#pragma link C++ function grid_init_(char*, char*, char*, int*)+;
#pragma link C++ function polfit_(int*,double*,double*,double*, double*, double*,double*,double*, double*,double*,double*)+;
#pragma link C++ function lss2006init_()+;
#pragma link C++ function lss2006_(int*,double*,double*, double*,double*,double*, double*,double*,double*,double*, double*, double*,double*,double*, double*,double*)+;
#pragma link C++ function nloini_()+;
#pragma link C++ function polnlo_(int* ,double*, double*, double*, double*, double*, double*, double*, double*)+;
#pragma link C++ function epcv_single_(char* , int *, double *, double *,double*, double*, double*, double*)+;
#pragma link C++ function epcv_single_v3_(char* , int *, double *, double *,double*, double*, double*, double*)+;
#pragma link C++ function gpc_single_v3_(char* , int *, double *, double *,double*, double*, double*, double*)+;
#pragma link C++ function fermi3_(double* , int *, double *)+;

//#pragma link C++ function sane_pol_(double * ,double * ,double * ,int * ,int * ,double * )+;

#pragma link C++ class GSPolarizedPDFs+;
#pragma link C++ class CTEQ6UnpolarizedPDFs+;
#pragma link C++ class CTEQ10UnpolarizedPDFs+;
#pragma link C++ class CJ12UnpolarizedPDFs+; 
#pragma link C++ class ABKM09UnpolarizedPDFs+; 
#pragma link C++ class MSTW08UnpolarizedPDFs+; 
#pragma link C++ class MRST2001UnpolarizedPDFs+; 
#pragma link C++ class MRST2002UnpolarizedPDFs+; 
#pragma link C++ class MRST2006UnpolarizedPDFs+; 

#pragma link C++ class CTEQ6eInclusiveDiffXSec+;
#pragma link C++ class F1F209eInclusiveDiffXSec+;
#pragma link C++ class F1F209QuasiElasticDiffXSec+;
#pragma link C++ class InSANEPolIncDiffXSec+;
//// quasi elastic 
//#pragma link C++ class QuasiElasticInclusiveDiffXSec+; 
//#pragma link C++ class QEIntegral+; 
//#pragma link C++ class QEFuncWrap+; 

#pragma link C++ class QFSInclusiveDiffXSec+;
#pragma link C++ class QFSXSecConfiguration+;

#pragma link C++ class InSANECrossSectionDifference+;

#pragma link C++ class InSANEPolarizedDiffXSec+;
#pragma link C++ class PolarizedDISXSec+;
#pragma link C++ class PolarizedDISXSecParallelHelicity+;
#pragma link C++ class PolarizedDISXSecAntiParallelHelicity+;

#pragma link C++ class InSANEBremsstrahlungRadiator+;
#pragma link C++ class InSANEPhotonDiffXSec+;
#pragma link C++ class InSANEInclusiveElectroProductionXSec+;
#pragma link C++ class InclusiveElectroProductionXSec+;
#pragma link C++ class InSANEInclusivePhotoProductionXSec+;
#pragma link C++ class InclusivePhotoProductionXSec+;
#pragma link C++ class InSANEInclusivePionElectroProductionXSec+;
#pragma link C++ class InSANEInclusivePionPhotoProductionXSec+;
#pragma link C++ class InclusiveHadronProductionXSec<InSANEInclusivePionElectroProductionXSec>+;
#pragma link C++ class InclusiveHadronProductionXSec<InSANEInclusivePionPhotoProductionXSec>+;

#pragma link C++ class InSANEInclusiveWiserXSec+;

#pragma link C++ class WiserInclusivePhotoXSec+;
#pragma link C++ class WiserInclusivePhotoXSec2+;
#pragma link C++ class PhotoWiserDiffXSec+;
#pragma link C++ class PhotoWiserDiffXSec2+;

#pragma link C++ class WiserInclusiveElectroXSec+;
#pragma link C++ class ElectroWiserDiffXSec+;

#pragma link C++ class InSANEPhotonDiffXSec+;

#pragma link C++ class InSANEInclusivePhotoProductionXSec+;
#pragma link C++ class InclusivePhotoProductionXSec+;

#pragma link C++ class InSANEInclusiveElectroProductionXSec+;
#pragma link C++ class InclusiveElectroProductionXSec+;

#pragma link C++ class InSANEInclusiveEPCVXSec+;
#pragma link C++ class InSANEInclusiveEPCVXSec2+;


//#pragma link C++ class InSANEKinematicCoefficient+;
//#pragma link C++ class InSANEComptonAsymmetry+;

#pragma link C++ class InSANEFunctionManager+;
#pragma link C++ global fgInSANEFunctionManager+;

// Base is not used.
//#pragma link C++ class InSANERadiatorBase<InSANEInclusiveDiffXSec>+;
//#pragma link C++ class InSANERadiatorBase<F1F209eInclusiveDiffXSec>+;

// Add all cross sections that can be radiated
#pragma link C++ class InSANERadiator<InSANEInclusiveDiffXSec>+;
#pragma link C++ class InSANERadiator<InSANEInclusiveBornDISXSec>+;
#pragma link C++ class InSANERadiator<InSANECompositeDiffXSec>+;
#pragma link C++ class InSANERadiator<F1F209eInclusiveDiffXSec>+;
#pragma link C++ class InSANERadiator<F1F209QuasiElasticDiffXSec>+;
#pragma link C++ class InSANERadiator<CTEQ6eInclusiveDiffXSec>+;
#pragma link C++ class InSANERadiator<InSANEPOLRADBornDiffXSec>+;
#pragma link C++ class InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>+;
#pragma link C++ class InSANERadiator<F1F209QuasiElasticDiffXSec>+;
#pragma link C++ class InSANERadiator<QFSInclusiveDiffXSec>+;

#pragma link C++ class InSANEExternalRadiator<InSANEInclusiveMollerDiffXSec>+;

//#pragma link C++ class InSANEFermiMomentumDist+;

#endif

