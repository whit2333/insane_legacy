#ifndef InSANEFragmentationFunctions_HH
#define InSANEFragmentationFunctions_HH

#include "TNamed.h"

/** Abstract base class for Fragmentation Functions.
 *
 * @ingroup fragfuncs
 */
class InSANEFragmentationFunctions : public TNamed {
   public:
      InSANEFragmentationFunctions();
      virtual ~InSANEFragmentationFunctions();

      // pion FFs
      virtual Double_t D_u_piplus( Double_t z, Double_t Q2)    = 0;
      virtual Double_t D_u_piminus(Double_t z, Double_t Q2)    = 0;

      virtual Double_t D_ubar_piplus( Double_t z, Double_t Q2) = 0;
      virtual Double_t D_ubar_piminus(Double_t z, Double_t Q2) = 0;

      virtual Double_t D_d_piplus( Double_t z, Double_t Q2)    = 0;
      virtual Double_t D_d_piminus(Double_t z, Double_t Q2)    = 0;

      virtual Double_t D_dbar_piplus( Double_t z, Double_t Q2) = 0;
      virtual Double_t D_dbar_piminus(Double_t z, Double_t Q2) = 0;

      virtual Double_t D_s_piplus( Double_t z, Double_t Q2)    = 0;
      virtual Double_t D_s_piminus(Double_t z, Double_t Q2)    = 0;

      virtual Double_t D_sbar_piplus( Double_t z, Double_t Q2) = 0;
      virtual Double_t D_sbar_piminus(Double_t z, Double_t Q2) = 0;


      // kaon FFs
      virtual Double_t D_u_Kplus( Double_t z, Double_t Q2)    = 0;
      virtual Double_t D_u_Kminus(Double_t z, Double_t Q2)    = 0;

      virtual Double_t D_ubar_Kplus( Double_t z, Double_t Q2) = 0;
      virtual Double_t D_ubar_Kminus(Double_t z, Double_t Q2) = 0;

      virtual Double_t D_d_Kplus( Double_t z, Double_t Q2)    = 0;
      virtual Double_t D_d_Kminus(Double_t z, Double_t Q2)    = 0;

      virtual Double_t D_dbar_Kplus( Double_t z, Double_t Q2) = 0;
      virtual Double_t D_dbar_Kminus(Double_t z, Double_t Q2) = 0;

      virtual Double_t D_s_Kplus( Double_t z, Double_t Q2)    = 0;
      virtual Double_t D_s_Kminus(Double_t z, Double_t Q2)    = 0;

      virtual Double_t D_sbar_Kplus( Double_t z, Double_t Q2) = 0;
      virtual Double_t D_sbar_Kminus(Double_t z, Double_t Q2) = 0;

      // pi0 
      // D_q^{pi0} = (D_q^{pi+} + D_q^{pi-})/2
      virtual Double_t D_u_pi0( Double_t z, Double_t Q2)    ;
      virtual Double_t D_ubar_pi0( Double_t z, Double_t Q2) ;
      virtual Double_t D_d_pi0( Double_t z, Double_t Q2)    ;
      virtual Double_t D_dbar_pi0( Double_t z, Double_t Q2) ;
      virtual Double_t D_s_pi0( Double_t z, Double_t Q2)    ;
      virtual Double_t D_sbar_pi0( Double_t z, Double_t Q2) ;

   ClassDef(InSANEFragmentationFunctions,1)
};
#endif

