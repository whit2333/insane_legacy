#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class InSANEGeometryCalculator+;

#pragma link C++ class InSANEExperimentGeometry+;
#pragma link C++ class InSANEDetectorGeometry+;
#pragma link C++ class InSANECoordinateSystem+;
#pragma link C++ class HMSCoordinateSystem+;
#pragma link C++ class BigcalCoordinateSystem+;
#pragma link C++ class HumanCoordinateSystem+;
#pragma link C++ class BPM1CoordinateSystem+;
#pragma link C++ class BPM2CoordinateSystem+;

#pragma link C++ class InSANEMagneticField+;
#pragma link C++ class UVAOxfordMagneticField+;
#pragma link C++ class UVAEveMagField+;
#pragma link C++ class InSANETrackPropagator2+;

#pragma link C++ class InSANEGeometryCalculator+;
#pragma link C++ class BIGCALGeometryCalculator+;
#pragma link C++ class LuciteGeometryCalculator+;
#pragma link C++ class GasCherenkovGeometryCalculator+;
#pragma link C++ class ForwardTrackerGeometryCalculator+;

#pragma link C++ global fgInSANEGeometryCalculator+;
#pragma link C++ global fgLuciteGeometryCalculator+;
#pragma link C++ global fgBIGCALGeometryCalculator+;
#pragma link C++ global fgGasCherenkovGeometryCalculator+;
#pragma link C++ global fgForwardTrackerGeometryCalculator+;

#pragma link C++ class InSANEDetectorComponent+;
#pragma link C++ class InSANELeadGlassBlock+;
#pragma link C++ class BigcalLeadGlassBlock+;
#pragma link C++ class RCSLeadGlassBlock+;
#pragma link C++ class ProtvinoLeadGlassBlock+;

#pragma link C++ class InSANEApparatus+;
#pragma link C++ class InSANEElectronBeam+;
#pragma link C++ class HallCPolarizedElectronBeam+;
#pragma link C++ class HallCRasteredBeam+;
#pragma link C++ function sane_pol_(double * ,double * ,double * ,int * ,int * ,double * )+;
#pragma link C++ class InSANETargetMaterial+;
#pragma link C++ class InSANETarget+;
#pragma link C++ class InSANESimpleTarget+;
#pragma link C++ class UVAPolarizedAmmoniaTarget+;
#pragma link C++ class UVACarbonTarget+;
#pragma link C++ class UVACrossHairTarget+;
#pragma link C++ class UVAPureHeliumTarget+;

#pragma link C++ class InSANECherenkovMirror+;
#pragma link C++ class InSANEToroidalMirror+;
#pragma link C++ class InSANESphericalMirror+;

#pragma link C++ class LuciteHodoscopeBar+;

//#pragma link C++ class InSANETrackPropagator+;

#endif
