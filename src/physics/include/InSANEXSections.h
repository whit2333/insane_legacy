#ifndef InSANEXSections_HH
#define InSANEXSections_HH 1

#include "InSANEInclusiveDiffXSec.h"
#include "InSANEElectroProductionXSec.h"
#include "InSANEPhysicalConstants.h"

/** Inclusive Mott Cross Section scattering off a particle with proton mass.
 *  The mott cross section describes a point-like spinless particle
 *
 * \ingroup inclusiveXSec
 */
class InSANEInclusiveMottXSec : public InSANEInclusiveDiffXSec {
public:
   InSANEInclusiveMottXSec() { };

   virtual ~InSANEInclusiveMottXSec() { };

   /**  Needed by ROOT::Math::IBaseFunctionMultiDim */
   unsigned int NDim() const {
      return fnDim;
   }

   Double_t GetEPrime(const Double_t theta)const  {
      return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / (M_p/GeV) * TMath::Power(TMath::Sin(theta / 2.0), 2)));
   }

   /** Evaluate Cross Section (mbarn/sr) */
   virtual Double_t EvaluateXSec(const Double_t * x) const;

   virtual void InitializePhaseSpaceVariables();


   ClassDef(InSANEInclusiveMottXSec, 1)
};


/// \deprecated
class InSANEPolIncDiffXSec : public InSANEInclusiveDiffXSec {
public:
   InSANEPolIncDiffXSec() {}
   ~InSANEPolIncDiffXSec() {}

   ClassDef(InSANEPolIncDiffXSec, 1)
};


#endif

