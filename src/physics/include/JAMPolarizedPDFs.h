#ifndef JAMPolarizedPDFs_HH
#define JAMPolarizedPDFs_HH 2 

#include "InSANEPolarizedPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#include "TSpline.h"
#include "TGraph.h"

/** JAM Polarized PDFs.
 *
 *  https://www.jlab.org/theory/jam/
 *  http://inspirehep.net/search?p=find+eprint+1310.3734
 *
 * Note that q^+ = q + qbar
 *
 * \ingroup ppdfs
 */
class JAMPolarizedPDFs : public InSANEPolarizedPartonDistributionFunctions {

   private:
      Double_t fPars_uplus[5];
      Double_t fPars_dplus[5];
      Double_t fPars_g[5];
      Double_t fPars_ubar[5];
      Double_t fPars_dbar[5];
      Double_t fPars_sbar[5];

      Double_t fParsErr_uplus[5];
      Double_t fParsErr_dplus[5];
      Double_t fParsErr_g[5];
      Double_t fParsErr_ubar[5];
      Double_t fParsErr_dbar[5];
      Double_t fParsErr_sbar[5];

      Double_t fPars_Twist3_p[5];
      Double_t fPars_Twist3_n[5];

      Double_t fKnots_x_p[6];
      Double_t fKnots_y_p[6];

      Double_t fKnots_x_n[6];
      Double_t fKnots_y_n[6];


      Double_t fQ20;

   public:
      TGraph   * fh_Twist4_p_gr;
      TSpline3 * fh_Twist4_p_spline;
      TGraph   * fh_Twist4_n_gr;
      TSpline3 * fh_Twist4_n_spline;


   protected:
      Double_t xDeltaf_model(Double_t x,Double_t *p){
         // Model has 5 parameters
         Double_t Nf = p[0];
         Double_t af = p[1];
         Double_t bf = p[2];
         Double_t cf = p[3];
         Double_t df = p[4];
         Double_t res = Nf*TMath::Power(x,af)*TMath::Power(1.0-x,bf)*(1.0+cf*TMath::Power(x,0.5)+df*x);
         return res;
      }
      
      Double_t xDeltaf_model_error(Double_t x, Double_t *p, Double_t *perr) {
         Double_t res = 0.0;
         Double_t Nf  = p[0];
         Double_t af  = p[1];
         Double_t bf  = p[2];
         Double_t cf  = p[3];
         Double_t df  = p[4];
         // dxf/dNf
         Double_t d0 = TMath::Power(x,af)*TMath::Power(1.0-x,bf)*(1.0+cf*TMath::Power(x,0.5)+df*x);
         // dxf/daf
         Double_t d1 = Nf*d0*TMath::Log(x);
         // dxf/dbf
         Double_t d2 = Nf*d0*TMath::Log(1.0-x);
         // dxf/dcf
         Double_t d3 = Nf*TMath::Power(1.0-x,bf)*TMath::Power(x,0.5+af);
         // dxf/ddf
         Double_t d4 = Nf*TMath::Power(1.0-x,bf)*TMath::Power(x,1.0+af);
         res  = d0*d0*perr[0]*perr[0];
         res += d1*d1*perr[1]*perr[1];
         res += d2*d2*perr[2]*perr[2];
         res += d3*d3*perr[3]*perr[3];
         res += d4*d4*perr[4]*perr[4];
         return TMath::Sqrt(res);
      }
      

   public:

      /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
       *  which returned by the subroutine
       */
      JAMPolarizedPDFs(); 
      virtual ~JAMPolarizedPDFs(); 

      virtual Double_t g1p_Twist4_Q20(Double_t x) {
         Double_t res = fh_Twist4_p_spline->Eval(x)/fQ20;
         //std::cout << "h(x) = " << res << std::endl;
         return res;
      }
      virtual Double_t g1p_Twist4(Double_t x, Double_t Q2) {
         Double_t res = fh_Twist4_p_spline->Eval(x)/Q2;
         //std::cout << "h(x) = " << res << std::endl;
         return res;
      }
      virtual Double_t g1n_Twist4_Q20(Double_t x) {
         Double_t res = fh_Twist4_n_spline->Eval(x)/fQ20;
         return res;
      }
      virtual Double_t g1n_Twist4(Double_t x, Double_t Q2) {
         Double_t res = fh_Twist4_n_spline->Eval(x)/Q2;
         return res;
      }

      virtual Double_t g2p_Twist3_Q20(Double_t x) {
         Double_t xbar = 1.0-x; 
         Double_t fA = fPars_Twist3_p[0];
         Double_t fB = fPars_Twist3_p[1];
         Double_t fC = -1.0*fPars_Twist3_p[2]; // minus from difference between JAM and Braun et al
         Double_t fD = fPars_Twist3_p[3];
         Double_t fE = -1.0*fPars_Twist3_p[4];

         Double_t  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         Double_t  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual Double_t g2p_Twist3(Double_t x,Double_t Q2) {
         Double_t xbar = 1.0-x; 
         Double_t fA = fPars_Twist3_p[0];
         Double_t fB = fPars_Twist3_p[1];
         Double_t fC = -1.0*fPars_Twist3_p[2]; // minus from difference between JAM and Braun et al
         Double_t fD = fPars_Twist3_p[3];
         Double_t fE = -1.0*fPars_Twist3_p[4];

         Double_t  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         Double_t  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual Double_t g2n_Twist3_Q20(Double_t x) {
         Double_t xbar = 1.0-x; 
         Double_t fA = fPars_Twist3_n[0];
         Double_t fB = fPars_Twist3_n[1];
         Double_t fC = -1.0*fPars_Twist3_n[2]; // minus from difference between JAM and Braun et al
         Double_t fD = fPars_Twist3_n[3];
         Double_t fE = -1.0*fPars_Twist3_n[4];

         Double_t  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         Double_t  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual Double_t g2n_Twist3(Double_t x,Double_t Q2) {
         Double_t xbar = 1.0-x; 
         Double_t fA = fPars_Twist3_n[0];
         Double_t fB = fPars_Twist3_n[1];
         Double_t fC = -1.0*fPars_Twist3_n[2]; // minus from difference between JAM and Braun et al
         Double_t fD = fPars_Twist3_n[3];
         Double_t fE = -1.0*fPars_Twist3_n[4];

         Double_t  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         Double_t  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       */
      Double_t *GetPDFs(Double_t,Double_t); 
      Double_t *GetPDFErrors(Double_t,Double_t); 

      ClassDef(JAMPolarizedPDFs,2)
};

#endif

