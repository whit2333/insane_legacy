#ifndef INSANEASYMMETRIESFROMSTRUCTUREFUNCTIONS_H 
#define INSANEASYMMETRIESFROMSTRUCTUREFUNCTIONS_H 4 

#include <cstdlib>
#include <iostream> 
#include <iomanip> 
#include <fstream>
#include "InSANEFunctionManager.h"
#include "InSANEPhysicalConstants.h"
#include "InSANEAsymmetryBase.h"
#include "InSANEStructureFunctions.h"
#include "InSANEPolarizedStructureFunctions.h"
#include "InSANEMathFunc.h"
#include "CTEQ6UnpolarizedPDFs.h"
#include "BBPolarizedPDFs.h"
#include "DNS2005PolarizedPDFs.h"
#include "DSSVPolarizedPDFs.h"
#include "F1F209StructureFunctions.h"
#include "InSANEStructureFunctionsFromPDFs.h"
#include "InSANEPolarizedStructureFunctionsFromPDFs.h"

/** Double spin asymmetries and virtual Compton scattering asymmetries from 
 *  polarized and unpolarized structure functions.
 *  
 */
class InSANEAsymmetriesFromStructureFunctions: public InSANEAsymmetryBase {

   private:
      InSANEStructureFunctions            * fUnpolSFs;
      InSANEPolarizedStructureFunctions   * fPolSFs; 

      void SetValues(Double_t,Double_t,Double_t &,Double_t &,Double_t &,Double_t &,Double_t &); 

   public:
      InSANEAsymmetriesFromStructureFunctions();
      virtual ~InSANEAsymmetriesFromStructureFunctions(); 

      virtual Double_t A1(Double_t,Double_t); 
      virtual Double_t A2(Double_t,Double_t); 

      /** Measured asymmetry for an arbitrary target polarization. 
       *  Optional argument of phi is the angle between scattering plane and the plane 
       *  with which the target polarization is rotated.
       */
      virtual Double_t AMeasured(Double_t targ_angle , Double_t x ,Double_t Qsq, Double_t y,Double_t phie = 0);
      virtual Double_t AMeasuredFixedBeamEnergy(Double_t targ_angle , Double_t x ,Double_t Qsq, Double_t E0,Double_t phie = 0);

      /** Measured asymmetry as a function of E,E',theta,phi. */
      Double_t A_Measured(Double_t targ_angle , Double_t E_beam ,Double_t E_prime, Double_t theta,Double_t phie = 0);

      /** \todo Parameters? */
      // double EvaluateAMeasured_p(double *x,double *p){
      //    InSANENucleus::NucleusType t = InSANENucleus::NucleusType(p[0]); 
      //    SetTargetType(t); 
      //    return AMeasured(x[0],p[1],p[1]);
      // }
      // double EvaluateAMeasuredFixedBeamEnergy_p(double *x,double *p){
      //    InSANENucleus::NucleusType t = InSANENucleus::NucleusType(p[0]); 
      //    SetTargetType(t); 
      //    return AMeasuredFixedBeamEnergy(x[0],p[1],p[2]);
      // }

      void SetUnpolarizedSFs(InSANEStructureFunctions *usfs){
         fUnpolSFs = usfs;
         TString l = "";
         if(fUnpolSFs) l += fUnpolSFs->GetLabel();
         l += " " ;
         if(fPolSFs) l+= fPolSFs->GetLabel();
         SetLabel(l);
      } 
      void SetPolarizedSFs(InSANEPolarizedStructureFunctions *psfs){
         fPolSFs = psfs;
         TString l = "";
         if(fUnpolSFs) l+= fUnpolSFs->GetLabel();
         l+= " " ;
         if(fPolSFs) l+= fPolSFs->GetLabel();
         SetLabel(l);
      } 

      ClassDef(InSANEAsymmetriesFromStructureFunctions,4) 
};

#endif 
