#ifndef InSANEPOLRADInelasticTailDiffXSec_HH
#define InSANEPOLRADInelasticTailDiffXSec_HH 1

#include "InSANEPOLRADBornDiffXSec.h"

/** Inelastic Radiative Tail (IRT).
 *
 * \ingroup inclusiveXSec
 */
class InSANEPOLRADInelasticTailDiffXSec: public InSANEPOLRADBornDiffXSec {

   public:
      InSANEPOLRADInelasticTailDiffXSec();
      virtual ~InSANEPOLRADInelasticTailDiffXSec();
      virtual InSANEPOLRADInelasticTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANEPOLRADInelasticTailDiffXSec::Clone()\n";
         auto * copy = new InSANEPOLRADInelasticTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANEPOLRADInelasticTailDiffXSec*  Clone() const { return( Clone("") ); } 
      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(InSANEPOLRADInelasticTailDiffXSec,1)
};

#endif

