#ifndef F1F209STRUCTUREFUNCTIONS_H
#define F1F209STRUCTUREFUNCTIONS_H 2 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <fstream> 
#include "InSANEPhysicalConstants.h"
#include "InSANEFortranWrappers.h"
#include "InSANEStructureFunctions.h"

/** Concrete imp of F1F209 structure functions F1 and F2.
 *  By default it includes the contribution the quasi-elastic 
 *  peak. This can be turned off by SetQEPeak(false). 
 *
 * \ingroup structurefunctions
 */
class F1F209StructureFunctions : public InSANEStructureFunctions {

   private: 
      char     fType;                ///< Deprecated  
      Bool_t   fModifiedVersion;     ///< modified version for 3He...
      Double_t fEs;                  ///< Beam energy (GeV) ? is it really? Why? -whit
      Bool_t   fIsFull;              ///< QE + inelastic: supercedes IncludesQE  
      Bool_t   fIsQEOnly;            ///< QE only                 
      Bool_t   fIsInelasticOnly;     ///< Inelastic only                 

      /** Handy function. Note i=0->R, i=1->F1, i=2->F2.*/
      double GetNuclearSF(Double_t x, Double_t Q2, Double_t z, Double_t a, Int_t i =2); 

   public:
      F1F209StructureFunctions(); 
      virtual ~F1F209StructureFunctions(); 

      // Returns F2 per nucleus (not per nucleon)
      virtual Double_t F2Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A);
      virtual Double_t F1Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A);

      void    SetQEPeak(Bool_t q = true) { fIsFull = true;}
      void    DoQEOnly(Bool_t q = true)  { 
         fIsQEOnly = q;
         if(fIsQEOnly){ 
            fIsFull          = false; 
            fIsInelasticOnly = false; 
         }
         std::cout << "[F1F209StructureFunctions]: Only QE component will be used." << std::endl;
      }  
      void DoInelasticOnly(Bool_t q = true) { 
         fIsInelasticOnly = q; 
         if(fIsInelasticOnly){
            fIsFull          = false; 
            fIsQEOnly        = false; 
         }
         std::cout << "[F1F209StructureFunctions]: Only inelastic component will be used." << std::endl;
      }  
      void DoFullCalculation(Bool_t q = true) { 
         fIsFull = q;
         if(fIsFull){
            fIsQEOnly        = false;
            fIsInelasticOnly = false; 
         } 
         std::cout << "[F1F209StructureFunctions]: QE and inelastic component will be used." << std::endl;
      }  

      virtual Double_t F1p(Double_t,Double_t); 
      virtual Double_t F1n(Double_t,Double_t); 
      virtual Double_t F1d(Double_t,Double_t); 
      virtual Double_t F1He3(Double_t,Double_t); 
      virtual Double_t F2p(Double_t,Double_t); 
      virtual Double_t F2n(Double_t,Double_t); 
      virtual Double_t F2d(Double_t,Double_t); 
      virtual Double_t F2He3(Double_t,Double_t); 

      virtual Double_t R(Double_t, Double_t); 

      void SetBeamEnergy(Double_t E){fEs = E;} 

      /**Use the modified version of F1F209. 
       * Set fType to 'y' to use. 
       * NOTE: You MUST set the beam energy if you want to use this!  
       */
      void UseModifiedModel(char ans){
         fType = ans; 
         std::cout << "[F1F209StructureFunctions]: Now using the modified model." << std::endl; 
      }

      ClassDef(F1F209StructureFunctions,3)
};

/** Concrete imp of F1F209 QUASI-ELASTIC structure functions F1 and F2.
 *
 *
 * \ingroup structurefunctions
 */

class F1F209QuasiElasticStructureFunctions : public InSANEStructureFunctions {

   private: 
      char     fType;                /// Deprecated  
      double   fA,fZ,fN;             /// Deprecated 
      Double_t fEs;                  /// Beam energy (GeV) 
      Double_t fF1,fF2,fR,fM;

      void GetSFs(Double_t,Double_t); 

   public:
      F1F209QuasiElasticStructureFunctions(); 
      virtual ~F1F209QuasiElasticStructureFunctions(); 

      // Returns F2 per nucleus (not per nucleon)
      virtual Double_t F2Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A);
      virtual Double_t F1Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A);

      virtual Double_t F1p(Double_t,Double_t); 
      virtual Double_t F1n(Double_t,Double_t); 
      virtual Double_t F1d(Double_t,Double_t); 
      virtual Double_t F1He3(Double_t,Double_t); 
      virtual Double_t F2p(Double_t,Double_t); 
      virtual Double_t F2n(Double_t,Double_t); 
      virtual Double_t F2d(Double_t,Double_t); 
      virtual Double_t F2He3(Double_t,Double_t); 

      void SetBeamEnergy(Double_t E){fEs = E;} 

      /**Use the modified version of F1F209. 
       * Set fType to 'y' to use. 
       * NOTE: You MUST set the beam energy if you want to use this!  
       */
      void UseModifiedModel(char ans){
         fType = ans; 
         std::cout << "[F1F209QuasiElasticStructureFunctions]: Now using the modified model." << std::endl; 
      }

      ClassDef(F1F209QuasiElasticStructureFunctions,1)
};
#endif 

