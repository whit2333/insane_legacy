#ifndef insane_physics_SSFsFromVPACs_HH
#define insane_physics_SSFsFromVPACs_HH 1

#include "SpinStructureFunctions.h"
#include "VirtualPhotoAbsorptionCrossSections.h"
#include "InSANEPhysicalConstants.h"
#include "InSANEMathFunc.h"

namespace insane {
  namespace physics {

    template<class T>
    class SSFsFromVPACs : public SpinStructureFunctions {

      protected:
        T  fVPACs;
        double   f4piAlphaOverM = 4.0*TMath::Pi()*TMath::Pi()*fine_structure_const/(M_p/GeV);

      public:
        SSFsFromVPACs(){}
        virtual ~SSFsFromVPACs(){}

        virtual Double_t g1p(   Double_t x, Double_t Q2) const
        {
          fVPACs.CalculateProton(x,Q2);
          double M     = M_p/GeV;
          double K     = InSANE::Kine::K_Hand(x,Q2);
          double gam2  = 4.0*x*x*M*M/Q2;
          double gam   = TMath::Sqrt(gam2);
          double k     = f4piAlphaOverM/K;
          double sLTp  = fVPACs.Sig_LTp();
          double sTTp  = fVPACs.Sig_LTp();
          double g1    = (gam*sLTp + 2.0*sTTp)/(k + gam2*k);
          return g1;
        }

        virtual Double_t g1n(   Double_t x, Double_t Q2) const
        {
          fVPACs.CalculateNeutron(x,Q2);
          double M     = M_p/GeV;
          double K     = InSANE::Kine::K_Hand(x,Q2);
          double gam2  = 4.0*x*x*M*M/Q2;
          double gam   = TMath::Sqrt(gam2);
          double k     = f4piAlphaOverM/K;
          double sLTp  = fVPACs.Sig_LTp();
          double sTTp  = fVPACs.Sig_LTp();
          double g1    = (gam*sLTp + 2.0*sTTp)/(k + gam2*k);
            return g1;
        }

        virtual Double_t g2p(   Double_t x, Double_t Q2) const
        {
          fVPACs.CalculateProton(x,Q2);
          double M     = M_p/GeV;
          double K     = InSANE::Kine::K_Hand(x,Q2);
          double gam2  = 4.0*x*x*M*M/Q2;
          double gam   = TMath::Sqrt(gam2);
          double k     = f4piAlphaOverM/K;
          double sLTp  = fVPACs.Sig_LTp();
          double sTTp  = fVPACs.Sig_LTp();
          double g2    = (sLTp - 2.0*gam*sTTp)/(gam*k + gam2*gam*k);
          return g2;
        }

        virtual Double_t g2n(   Double_t x, Double_t Q2) const
        {
          fVPACs.CalculateNeutron(x,Q2);
          double M     = M_p/GeV;
          double K     = InSANE::Kine::K_Hand(x,Q2);
          double gam2  = 4.0*x*x*M*M/Q2;
          double gam   = TMath::Sqrt(gam2);
          double k     = f4piAlphaOverM/K;
          double sLTp  = fVPACs.Sig_LTp();
          double sTTp  = fVPACs.Sig_LTp();
          double g2    = (sLTp - 2.0*gam*sTTp)/(gam*k + gam2*gam*k);
          return g2;
        }
        //virtual Double_t g1d(   Double_t x, Double_t Q2);
        //virtual Double_t g1He3( Double_t x, Double_t Q2);
        //virtual Double_t g2d(   Double_t x, Double_t Q2);
        //virtual Double_t g2He3( Double_t x, Double_t Q2);
        virtual double g1p_TMC(double x, double Q2) const { return g1p(x,Q2); }
        virtual double g2p_TMC(double x, double Q2) const { return g2p(x,Q2); }
        virtual double g1n_TMC(double x, double Q2) const { return g1n(x,Q2); }
        virtual double g2n_TMC(double x, double Q2) const { return g2n(x,Q2); }

        ClassDef(SSFsFromVPACs,1)
    };
  }
}


#endif
