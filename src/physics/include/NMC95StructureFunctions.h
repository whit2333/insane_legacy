#ifndef NMC95STRUCTUREFUNCTIONS_H
#define NMC95STRUCTUREFUNCTIONS_H 1

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <fstream>
#include "TString.h" 
#include "InSANEFortranWrappers.h"
#include "InSANEStructureFunctions.h"
#include "InSANEFortranWrappers.h"

/** Concrete imp of NMC95 structure functions F1 and F2 for n, p, d and higher A nuclei  
 * 
 *  Coverage: 0.5<Q2<75 GeV^2, 0.006<x<0.9
 *
 *
 * \ingroup structurefunctions
 */

class NMC95StructureFunctions : public InSANEStructureFunctions {

   private: 
      double   fA,fZ,fN;
      Double_t fF1,fF2,fR,fM;

      void GetSFs(Int_t,Double_t,Double_t); 

   public:
      NMC95StructureFunctions(); 
      virtual ~NMC95StructureFunctions(); 

      // Returns F2 per nucleus (not per nucleon)
      virtual Double_t F2Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A);
      virtual Double_t F1Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A);

      virtual Double_t F1p(Double_t,Double_t); 
      virtual Double_t F1n(Double_t,Double_t); 
      virtual Double_t F1d(Double_t,Double_t); 
      virtual Double_t F1He3(Double_t,Double_t); 
      virtual Double_t F2p(Double_t,Double_t); 
      virtual Double_t F2n(Double_t,Double_t); 
      virtual Double_t F2d(Double_t,Double_t); 
      virtual Double_t F2He3(Double_t,Double_t); 

      ClassDef(NMC95StructureFunctions,1)
};

#endif 
