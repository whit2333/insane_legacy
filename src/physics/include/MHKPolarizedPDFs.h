#ifndef MHKPolarizedPDFs_HH
#define MHKPolarizedPDFs_HH 2 

#include "InSANEPolarizedPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"
#include "TSpline.h"
#include "TGraph.h"

/** Monfared et al http://inspirehep.net/record/1297075 
 *
 *
 * \ingroup ppdfs
 */
class MHKPolarizedPDFs : public InSANEPolarizedPartonDistributionFunctions {

   private:
      Double_t fPars_uplus[5];
      Double_t fPars_dplus[5];
      Double_t fPars_g[5];
      Double_t fPars_ubar[5];
      Double_t fPars_dbar[5];
      Double_t fPars_sbar[5];

      Double_t fParsErr_uplus[5];
      Double_t fParsErr_dplus[5];
      Double_t fParsErr_g[5];
      Double_t fParsErr_ubar[5];
      Double_t fParsErr_dbar[5];
      Double_t fParsErr_sbar[5];

      Double_t fPars_Twist3_p[5];
      Double_t fPars_Twist3_n[5];

      Double_t fKnots_x_p[6];
      Double_t fKnots_y_p[6];

      Double_t fKnots_x_n[6];
      Double_t fKnots_y_n[6];

      TGraph   * fh_Twist4_p_gr;
      TSpline3 * fh_Twist4_p_spline;
      TGraph   * fh_Twist4_n_gr;
      TSpline3 * fh_Twist4_n_spline;

      Double_t fQ20;


   protected:

      Double_t xDeltaf_model(Double_t x,Double_t *p){
         // Model has 4 parameters
         Double_t etaf = p[0]; // first moment
         Double_t af = p[1];
         Double_t bf = p[2];
         Double_t cf = p[3];
         Double_t Nf = 1.0/( (1.0+cf*(af/(af+bf+1.0)))*TMath::Beta(af,bf+1.0));

         Double_t res = Nf*etaf*TMath::Power(x,af)*TMath::Power(1.0-x,bf)*(1.0+cf*x);
         return res;
      }
      

   public:

      /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
       *  which returned by the subroutine
       */
      MHKPolarizedPDFs(); 
      virtual ~MHKPolarizedPDFs(); 

      virtual Double_t g1p_Twist4_Q20(Double_t x) {
         return 0.0;
         //Double_t res = fh_Twist4_p_spline->Eval(x)/fQ20;
         ////std::cout << "h(x) = " << res << std::endl;
         //return res;
      }
      virtual Double_t g1p_Twist4(Double_t x, Double_t Q2) {
         return 0.0;
         //Double_t res = fh_Twist4_p_spline->Eval(x)/Q2;
         ////std::cout << "h(x) = " << res << std::endl;
         //return res;
      }
      virtual Double_t g1n_Twist4_Q20(Double_t x) {
         return 0.0;
         //Double_t res = fh_Twist4_n_spline->Eval(x)/fQ20;
         //return res;
      }
      virtual Double_t g1n_Twist4(Double_t x, Double_t Q2) {
         return 0.0;
         //Double_t res = fh_Twist4_n_spline->Eval(x)/Q2;
         //return res;
      }

      virtual Double_t g2p_Twist3_Q20(Double_t x) {
         Double_t xbar = 1.0-x; 
         Double_t fA = fPars_Twist3_p[0];
         Double_t fB = fPars_Twist3_p[1];
         Double_t fC = fPars_Twist3_p[2]; 
         Double_t fD = fPars_Twist3_p[3];
         Double_t fE = fPars_Twist3_p[4];

         Double_t  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         Double_t  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual Double_t g2p_Twist3(Double_t x,Double_t Q2) {
         Double_t xbar = 1.0-x; 
         Double_t fA = fPars_Twist3_p[0];
         Double_t fB = fPars_Twist3_p[1];
         Double_t fC = fPars_Twist3_p[2];
         Double_t fD = fPars_Twist3_p[3];
         Double_t fE = fPars_Twist3_p[4];

         Double_t  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         Double_t  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual Double_t g2n_Twist3_Q20(Double_t x) {
         Double_t xbar = 1.0-x; 
         Double_t fA = fPars_Twist3_n[0];
         Double_t fB = fPars_Twist3_n[1];
         Double_t fC = fPars_Twist3_n[2]; 
         Double_t fD = fPars_Twist3_n[3];
         Double_t fE = fPars_Twist3_n[4];

         Double_t  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         Double_t  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual Double_t g2n_Twist3(Double_t x,Double_t Q2) {
         Double_t xbar = 1.0-x; 
         Double_t fA = fPars_Twist3_n[0];
         Double_t fB = fPars_Twist3_n[1];
         Double_t fC = fPars_Twist3_n[2];
         Double_t fD = fPars_Twist3_n[3];
         Double_t fE = fPars_Twist3_n[4];

         Double_t  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         Double_t  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       */
      Double_t *GetPDFs(Double_t,Double_t); 
      Double_t *GetPDFErrors(Double_t,Double_t); 

      ClassDef(MHKPolarizedPDFs,2)
};

#endif

