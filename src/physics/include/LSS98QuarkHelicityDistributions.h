#ifndef LSS98QuarkHelicityDistributions_H
#define LSS98QuarkHelicityDistributions_H

#include <cstdlib> 
#include <iostream> 
#include <iomanip>
#include <cmath>  
#include "TMath.h"

class LSS98QuarkHelicityDistributions{

   private:
      /// polynomial coefficients 
      Double_t fAu,fAd,fAs,fAg;
      Double_t fBu,fBd,fBs,fBg;
      Double_t fCu,fCd,fCs;
      Double_t fDu,fDd,fDs;
      Double_t fAlpha,fAlpha_g; 

   public: 
      LSS98QuarkHelicityDistributions(); 
      virtual ~LSS98QuarkHelicityDistributions();

      Double_t uPlus(Double_t);  
      Double_t uMinus(Double_t);  
      Double_t dPlus(Double_t);  
      Double_t dMinus(Double_t);  
      Double_t sPlus(Double_t);  
      Double_t sMinus(Double_t);  
      Double_t gPlus(Double_t);  
      Double_t gMinus(Double_t);  

      ClassDef(LSS98QuarkHelicityDistributions,1)
 
};

#endif 
