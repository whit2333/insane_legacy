#ifndef InSANEPOLRADRadiatedDiffXSec_HH
#define InSANEPOLRADRadiatedDiffXSec_HH 1

#include "InSANEPOLRADBornDiffXSec.h"

/** Cross Section with radiative corrections.
 *  Full internal calculation. 
 *
 * \ingroup inclusiveXSec
 */
class InSANEPOLRADRadiatedDiffXSec: public InSANEPOLRADBornDiffXSec {
   public:
      InSANEPOLRADRadiatedDiffXSec();
      virtual ~InSANEPOLRADRadiatedDiffXSec();
      virtual InSANEPOLRADRadiatedDiffXSec*  Clone(const char * newname) const ;
      virtual InSANEPOLRADRadiatedDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(InSANEPOLRADRadiatedDiffXSec,1)
};

#endif

