#ifndef InSANERadiativeCorrections_HH
#define InSANERadiativeCorrections_HH 1

#include <iostream>
#include "TNamed.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TVector3.h"
#include "TBrowser.h"
#include "InSANEAveragedKinematics.h"
#include "InSANEPhaseSpace.h"
#include "InSANEInclusiveDiffXSec.h"
#include "InSANEFunctionManager.h"
#include "InSANEPOLRADInternalPolarizedDiffXSec.h"
#include "InSANEPOLRADBornDiffXSec.h"
#include "InSANEPOLRADInelasticTailDiffXSec.h"
#include "InSANEPOLRADQuasiElasticTailDiffXSec.h"
#include "InSANEPOLRADElasticTailDiffXSec.h"
#include "InSANEInelasticRadiativeTail.h"
#include "InSANEElasticRadiativeTail.h"


/** Class to handle radiative corrections to binned data.  
 */ 
class InSANERadiativeCorrections1D : public TNamed {

   protected:
      InSANEPhaseSpace * fPolarizedPhaseSpace; //!
      InSANEPhaseSpace * fPhaseSpace         ; //!

   public:
      InSANEInclusiveDiffXSec * fBorn0 ; //!
      InSANEInclusiveDiffXSec * fBorn1 ; //!
      InSANEInclusiveDiffXSec * fBorn2 ; //!
      InSANEInclusiveDiffXSec * fERT0_rt ; //!
      InSANEInclusiveDiffXSec * fERT1_rt ; //!
      InSANEInclusiveDiffXSec * fERT2_rt ; //!
      InSANEInclusiveDiffXSec * fInelasticXSec0_rt ; //!
      InSANEInclusiveDiffXSec * fInelasticXSec1_rt ; //!
      InSANEInclusiveDiffXSec * fInelasticXSec2_rt ; //!
      InSANEInclusiveDiffXSec * fDiffXSec03 ; //!
      InSANEInclusiveDiffXSec * fDiffXSec13 ; //!
      InSANEInclusiveDiffXSec * fDiffXSec23 ; //!

   public:

      TH1F * fSigmaBorn;       //->
      TH1F * fSigmaBorn_Plus;  //->
      TH1F * fSigmaBorn_Minus; //->
      TH1F * fDeltaBorn;       //->
      TH1F * fAsymBorn;        //->

      TH1F * fSigmaERT;        //->
      TH1F * fSigmaERT_Plus;   //->
      TH1F * fSigmaERT_Minus;  //->
      TH1F * fDeltaERT;        //->
      TH1F * fAsymERT;         //->

      TH1F * fSigmaIRT;        //->
      TH1F * fSigmaIRT_Plus;   //->
      TH1F * fSigmaIRT_Minus;  //->
      TH1F * fDeltaIRT;        //->
      TH1F * fAsymIRT;         //->

      Double_t fThetaTarget;
      Double_t fBeamEnergy;

      InSANEAveragedKinematics1D * fAvgKine; //->

      Int_t InitHist(const TH1F * h0, TH1F ** h1, const char *n = "");

   public:

      InSANERadiativeCorrections1D(const char * n="", const char * t="");
      virtual ~InSANERadiativeCorrections1D();

      Bool_t         IsFolder() const { return kTRUE; }
      void           Browse(TBrowser* b);

      void SetAvgKine(InSANEAveragedKinematics1D * kine){ fAvgKine = kine; }
      InSANEAveragedKinematics1D* GetAvgKine(){ return fAvgKine; }

      void      SetBeamEnergy(Double_t en){ fBeamEnergy = en; }
      Double_t  GetBeamEnergy(){ return fBeamEnergy; }

      void      SetThetaTarget(Double_t en){ fThetaTarget = en; }
      Double_t  GetThetaTarget(){ return fThetaTarget; }

      Int_t CreateHistograms(TH1F * h0);         ///< Uses the supplied histogram to create the binning. 
      virtual Int_t InitCrossSections();
      virtual Int_t Calculate();

      void PrintConfiguration(std::ostream& s = std::cout );
      ClassDef(InSANERadiativeCorrections1D,1)
};

#endif

