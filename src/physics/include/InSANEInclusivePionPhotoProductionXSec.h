#ifndef InSANEInclusivePionPhotoProductionXSec_HH
#define InSANEInclusivePionPhotoProductionXSec_HH

#include "InSANEPhotonDiffXSec.h"


/**  Inclusive photo production of pions, kaons, and nucleons from a nucleon target.
 *   For all targets use InclusivePhotoProductionXSec.
 * \ingroup inclusiveXSec
 */
class InSANEInclusivePionPhotoProductionXSec : public InSANEPhotonDiffXSec {

   protected:
      InSANEPhotonDiffXSec * fSig_0;       //->
      InSANEPhotonDiffXSec * fSig_plus;    //->
      InSANEPhotonDiffXSec * fSig_minus;   //->

   public:
      InSANEInclusivePionPhotoProductionXSec();
      InSANEInclusivePionPhotoProductionXSec(const InSANEInclusivePionPhotoProductionXSec& rhs) ;
      virtual ~InSANEInclusivePionPhotoProductionXSec();
      InSANEInclusivePionPhotoProductionXSec& operator=(const InSANEInclusivePionPhotoProductionXSec& rhs) ;
      virtual InSANEInclusivePionPhotoProductionXSec*  Clone(const char * newname) const ;
      virtual InSANEInclusivePionPhotoProductionXSec*  Clone() const ; 

      Double_t EvaluateXSec(const Double_t * x) const ;

      void  SetParameters(int i, const std::vector<double>& pars);
      const std::vector<double>& GetParameters() const ;

      virtual void   SetRadiationLength(Double_t r) { 
         ((InSANEPhotonDiffXSec*)fSig_0)->SetRadiationLength(r);
         ((InSANEPhotonDiffXSec*)fSig_plus)->SetRadiationLength(r);
         ((InSANEPhotonDiffXSec*)fSig_minus)->SetRadiationLength(r);
      }
      // Here we copied a bunch of setters from InSANEDiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void       SetTargetMaterial(const InSANETargetMaterial& mat){
         fSig_0->SetTargetMaterial(mat);
         fSig_plus->SetTargetMaterial(mat);
         fSig_minus->SetTargetMaterial(mat);
         fTargetMaterial = mat;
      }
      void SetTargetNucleus(const InSANENucleus & targ){
         InSANEInclusiveDiffXSec::SetTargetNucleus(targ);
         //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
         //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
      }

      virtual void InitializePhaseSpaceVariables();

      void       SetTargetMaterialIndex(Int_t i){
         fSig_0->SetTargetMaterialIndex(i);
         fSig_plus->SetTargetMaterialIndex(i);
         fSig_minus->SetTargetMaterialIndex(i);
         fMaterialIndex = i ;
      }
      virtual void  SetBeamEnergy(Double_t en){
         InSANEInclusiveDiffXSec::SetBeamEnergy(en);
         fSig_0->SetBeamEnergy(en);
         fSig_plus->SetBeamEnergy(en);
         fSig_minus->SetBeamEnergy(en);
      }

      virtual void  SetPhaseSpace(InSANEPhaseSpace * ps);
      virtual void  InitializeFinalStateParticles();

   ClassDef(InSANEInclusivePionPhotoProductionXSec,1)
};


#endif

