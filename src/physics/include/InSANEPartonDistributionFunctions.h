#ifndef InSANEPartonDistributionFunctions_HH
#define InSANEPartonDistributionFunctions_HH 1
#include "InSANEPhysicalConstants.h"
#include "TMath.h"
#include "InSANEFortranWrappers.h"
#include "TString.h"
#include "TCanvas.h"
#include "TGraph.h"
#include <vector>
#include "TH1F.h"
#include "TF1.h"
#include "TF2.h"
#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"

#define NPARTONDISTS 13

/** \todo create DGLAP evolution framework here.
 */

/** Base class for PDFs */
class InSANEPDFBase : public TNamed , public TAttLine, public TAttMarker {

   private:
      TString    fLabel;

   protected:
      Double_t   fQsquared;
      Double_t   fXbjorken;

      Double_t   fxPlotMin;
      Double_t   fxPlotMax;

      Double_t   fModelMin_x;     // Minimum value of x for which model is valid
      Double_t   fModelMax_x;     // Maximum value of x for which model is valid
      Double_t   fModelMin_W;     // Min W for model. This should be the cut applied to the data for fitting
      Double_t   fModelMax_W;     // Usually unbound
      Double_t   fModelMin_Q2;    // Min W for model. This should be the cut applied to the data for fitting
      Double_t   fModelMax_Q2;    // Usually unbound

      Double_t   fPDFValues[NPARTONDISTS];  /// f(x), PDF not multiplied by x!
      Double_t   fPDFErrors[NPARTONDISTS];
      TF1 *      fFunctions[NPARTONDISTS]; //! For plotting only

   public :

      InSANEPDFBase();
      virtual ~InSANEPDFBase();

      void        SetLabel(const char * l){ fLabel = l;}
      const char* GetLabel(){ return fLabel; }

      typedef enum  { 
         kUP          = 0,
         kDOWN        = 1,
         kSTRANGE     = 2,
         kCHARM       = 3,
         kBOTTOM      = 4,
         kTOP         = 5,
         kGLUON       = 6,
         kANTIUP      = 7,
         kANTIDOWN    = 8,
         kANTISTRANGE = 9,
         kANTICHARM   = 10,
         kANTIBOTTOM  = 11,
         kANTITOP     = 12
      } InSANEPartonFlavor;


      /** Resets x,Q2,and the pdf values to zero. */
      void ClearValues();

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared.
       */
      virtual Double_t * GetPDFs(Double_t x, Double_t Qsq) {return fPDFValues;}
      virtual Double_t * GetPDFErrors(Double_t x, Double_t Qsq) = 0;

      Double_t GetXBjorken() const {  return(fXbjorken); }
      Double_t GetQSquared() const {  return(fQsquared); }

      ClassDef(InSANEPDFBase, 4)
};



/** ABC for parton distributions
 *
 *  By default the s,sbar,c, and cbar return 0 and
 *  the up and down are pure virtual methods
 *
 * \ingroup partondistributions
 */
class InSANEPartonDistributionFunctions : public InSANEPDFBase {
   public:
      InSANEPartonDistributionFunctions() { }
      virtual ~InSANEPartonDistributionFunctions() { }

      /** @name Simple PDF access
       *  After calling GetPDFs(x,Qsq), the following return the values of the PDFs for
       *  those values of x and Qsq.
       *
       *  The quark labeling for fPDFValues is
       *  \f$ (0,1,2,3,4,5,6,7,,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$
       *  @{
       */
      Double_t u() { return(fPDFValues[0]); }
      Double_t d() { return(fPDFValues[1]); }
      Double_t s() { return(fPDFValues[2]); }
      Double_t c() { return(fPDFValues[3]); }
      Double_t b() { return(fPDFValues[4]); }
      Double_t t() { return(fPDFValues[5]); }
      Double_t g() { return(fPDFValues[6]); }
      Double_t ubar() { return(fPDFValues[7]); }
      Double_t dbar() { return(fPDFValues[8]); }
      Double_t sbar() { return(fPDFValues[9]); }
      Double_t cbar() { return(fPDFValues[10]); }
      Double_t bbar() { return(fPDFValues[11]); }
      Double_t tbar() { return(fPDFValues[12]); }
      // Errors 
      Double_t uError() { return(fPDFErrors[0]); }
      Double_t dError() { return(fPDFErrors[1]); }
      Double_t sError() { return(fPDFErrors[2]); }
      Double_t cError() { return(fPDFErrors[3]); }
      Double_t bError() { return(fPDFErrors[4]); }
      Double_t tError() { return(fPDFErrors[5]); }
      Double_t gError() { return(fPDFErrors[6]); }
      Double_t ubarError() { return(fPDFErrors[7]); }
      Double_t dbarError() { return(fPDFErrors[8]); }
      Double_t sbarError() { return(fPDFErrors[9]); }
      Double_t cbarError() { return(fPDFErrors[10]); }
      Double_t bbarError() { return(fPDFErrors[11]); }
      Double_t tbarError() { return(fPDFErrors[12]); }

      //@}

      /** up quark distribution, \f$ u(x) \f$ */
      virtual Double_t u(Double_t x, Double_t Qsq) {
         GetPDFs(x,Qsq);
         return(u());
      }

      /** down quark distribution, \f$ d(x) \f$  */
      virtual Double_t d(Double_t x, Double_t Qsq) {
         GetPDFs(x,Qsq);
         return(d());
      }

      /** up anti-quark distribution, \f$ \bar{u}(x) \f$  */
      virtual Double_t uBar(Double_t x, Double_t Qsq) {
         GetPDFs(x,Qsq);
         return(ubar());
      }

      /** down anti-quark distribution, \f$ \bar{d}(x) \f$  */
      virtual Double_t dBar(Double_t x, Double_t Qsq) {
         GetPDFs(x,Qsq);
         return(dbar());
      }

      /** Strange quark dist is zero by default. */
      virtual Double_t s(Double_t x, Double_t Qsq) {
         GetPDFs(x,Qsq);
         return(s());
      }
      virtual Double_t sBar(Double_t x, Double_t Qsq) {
         GetPDFs(x,Qsq);
         return(sbar());
      }
      virtual Double_t c(Double_t x, Double_t Qsq)  {
         GetPDFs(x,Qsq);
         return(c());
      }
      virtual Double_t cBar(Double_t x, Double_t Qsq) {
         GetPDFs(x,Qsq);
         return(cbar());
      }

      virtual Double_t g(Double_t x, Double_t Qsq) {
         GetPDFs(x,Qsq);
         return(g());
      }

      /** up quark distribution, \f$ u(x) \f$ */
      virtual Double_t uError(Double_t x, Double_t Qsq) {
         GetPDFErrors(x,Qsq);
         return(uError());
      }

      /** down quark distribution, \f$ d(x) \f$  */
      virtual Double_t dError(Double_t x, Double_t Qsq) {
         GetPDFErrors(x,Qsq);
         return(dError());
      }

      /** up anti-quark distribution, \f$ \bar{u}(x) \f$  */
      virtual Double_t uBarError(Double_t x, Double_t Qsq) {
         GetPDFErrors(x,Qsq);
         return(ubarError());
      }

      /** down anti-quark distribution, \f$ \bar{d}(x) \f$  */
      virtual Double_t dBarError(Double_t x, Double_t Qsq) {
         GetPDFErrors(x,Qsq);
         return(dbarError());
      }

      /** Strange quark dist is zero by default. */
      virtual Double_t sError(Double_t x, Double_t Qsq) {
         GetPDFErrors(x,Qsq);
         return(sError());
      }
      virtual Double_t sBarError(Double_t x, Double_t Qsq) {
         GetPDFErrors(x,Qsq);
         return(sbarError());
      }
      virtual Double_t cError(Double_t x, Double_t Qsq)  {
         GetPDFErrors(x,Qsq);
         return(cError());
      }
      virtual Double_t cBarError(Double_t x, Double_t Qsq) {
         GetPDFErrors(x,Qsq);
         return(cbarError());
      }

      virtual Double_t gError(Double_t x, Double_t Qsq) {
         GetPDFErrors(x,Qsq);
         return(gError());
      }


      /** Makes a simple plot of each parton distribution function */
      virtual void PlotPDFs(Double_t Qsq = 5.0, Int_t log = 0);

      /** Uses TF1 functions to make a plot returns the canvas */
      TCanvas * PlotPDF(Double_t Qsq = 5.0, Int_t log = 0);

      /** @name Useful for using as ROOT functions, TF1 etc...
       *
       * @{
       */
      /** Use with TF1. The arguments are x=x[0], Q^2=p[0] */
      double Evaluate_u(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return( u() );
      }
      double Evaluatexu(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*u());
      }
      double Evaluatexd(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*d());
      }
      double Evaluate_d(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*d());
      }
      double Evaluatexs(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*s());
      }
      double Evaluatexg(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*g());
      }
      double Evaluatexubar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*ubar());
      }
      double Evaluatexdbar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*dbar());
      }
      double Evaluatexsbar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*sbar());
      }
      //@}

      /** Returns a TF1 functions for easy plotting and manipulation
       *  Argument q is the quark u=1,d=2,... gluon=6,anti-u=7, anti-d=8 ...
       *
       */
      TF1 * GetFunction(InSANEPDFBase::InSANEPartonFlavor q = InSANEPDFBase::kUP);

      void GetValues(TObject *obj, Double_t Q2, InSANEPDFBase::InSANEPartonFlavor = InSANEPDFBase::kUP );
      /** Get error band.
       *  Argument obj is assumed to be a TH1. 
       */ 
      void GetErrorBand(TObject *obj, Double_t Q2, InSANEPDFBase::InSANEPartonFlavor = InSANEPDFBase::kUP );

      ClassDef(InSANEPartonDistributionFunctions, 2)
};




#endif

