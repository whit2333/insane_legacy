#ifndef AvakianQuarkHelicityDistributions_H
#define AvakianQuarkHelicityDistributions_H

#include <cstdlib> 
#include <iostream> 
#include <iomanip>
#include <cmath>  
#include "TMath.h"
#include "InSANEPartonHelicityDistributions.h"

/** Quark helicity distribtuions at FIXED Q2 = 4.0 GeV2.
  *
  * Reference: Phys. Rev. Lett. 99, 082001 (2007)   
  *
  */
class AvakianQuarkHelicityDistributions : public InSANEPartonHelicityDistributions {

   private:
      /// polynomial coefficients 
      Double_t fAu,fAd,fAs,fAg;
      Double_t fBu,fBd,fBs,fBg;
      Double_t fCu,fCd,fCs;
      Double_t fDu,fDd,fDs;
      Double_t fAlpha,fAlpha_g;
      Double_t fCup,fCdp;  

   public: 
      AvakianQuarkHelicityDistributions(); 
      virtual ~AvakianQuarkHelicityDistributions();

      Double_t uPlus(Double_t);  
      Double_t uMinus(Double_t);  
      Double_t dPlus(Double_t);  
      Double_t dMinus(Double_t);  
      Double_t sPlus(Double_t);  
      Double_t sMinus(Double_t);  
      Double_t gPlus(Double_t);  
      Double_t gMinus(Double_t);  

      virtual Int_t CalculateDistributions(Double_t x,Double_t Q2);

      ClassDef(AvakianQuarkHelicityDistributions,1)
 
};


#endif 

