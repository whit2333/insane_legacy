#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;

#pragma link C++ namespace InSANE;
#pragma link C++ namespace InSANE::Kine;

#pragma link C++ function InSANE::Kine::SetMomFromEThetaPhi(TParticle*, double *x);

// Old
//#pragma link C++ class NNPerpElectronCorrectionEnergy+;
//#pragma link C++ class NNPerpElectronCorrectionTheta+;
//#pragma link C++ class NNPerpElectronCorrectionPhi+;
//
//#pragma link C++ class NNParaElectronCorrection+;
//#pragma link C++ class NNParaElectronCorrectionEnergy+;
//#pragma link C++ class NNParaElectronCorrectionTheta+;
//#pragma link C++ class NNParaElectronCorrectionPhi+;
//
//#pragma link C++ class NNParaPositronCorrections+;
//#pragma link C++ class NNParaPositronCorrectionEnergy+;
//#pragma link C++ class NNParaPositronCorrectionTheta+;
//#pragma link C++ class NNParaPositronCorrectionPhi+;
//
//#pragma link C++ class NNParaGammaCorrection+;
//#pragma link C++ class NNParaGammaCorrectionEnergy+;
//#pragma link C++ class NNParaGammaCorrectionTheta+;
//#pragma link C++ class NNParaGammaCorrectionPhi+;
//
////#pragma link C++ class NNParaGammaXYCorrectionClusterDeltaX+;
////#pragma link C++ class NNParaGammaXYCorrectionClusterDeltaY+;
////#pragma link C++ class NNParaGammaXYCorrectionEnergy+;
//
//#pragma link C++ class NNPerpGammaXYCorrectionClusterDeltaX+;
//#pragma link C++ class NNPerpGammaXYCorrectionClusterDeltaY+;
//#pragma link C++ class NNPerpGammaXYCorrectionEnergy+;
//
////--------------- Electron
//// Parallel
//#pragma link C++ class NNParaElectronXYCorrectionClusterDeltaX+;
//#pragma link C++ class NNParaElectronXYCorrectionClusterDeltaY+;
//#pragma link C++ class NNParaElectronXYCorrectionEnergy+;
//
//#pragma link C++ class NNParaElectronAngleCorrectionBCPDir+;
//#pragma link C++ class NNParaElectronAngleCorrectionBCPDirTheta+;
//#pragma link C++ class NNParaElectronAngleCorrectionBCPDirPhi+;
//
//#pragma link C++ class NNParaElectronAngleCorrectionDeltaEnergy+;
//#pragma link C++ class NNParaElectronAngleCorrectionDeltaTheta+;
//#pragma link C++ class NNParaElectronAngleCorrectionDeltaPhi+;
//
//// Perpendicular
//#pragma link C++ class NNPerpElectronXYCorrectionClusterDeltaX+;
//#pragma link C++ class NNPerpElectronXYCorrectionClusterDeltaY+;
//#pragma link C++ class NNPerpElectronXYCorrectionEnergy+;
//
//#pragma link C++ class NNPerpPositronXYCorrectionClusterDeltaX+;
//#pragma link C++ class NNPerpPositronXYCorrectionClusterDeltaY+;
//#pragma link C++ class NNPerpPositronXYCorrectionEnergy+;
//
//#pragma link C++ class NNPerpElectronAngleCorrectionDeltaEnergy+;
//#pragma link C++ class NNPerpElectronAngleCorrectionDeltaTheta+;
//#pragma link C++ class NNPerpElectronAngleCorrectionDeltaPhi+;


//---------------------- 

#pragma link C++ namespace InSANE::Math;

#pragma link C++ function InSANE::Math::TestFunction22(double);
#pragma link C++ function InSANE::Math::CGLN_F1(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F2(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F3(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F4(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F5(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F6(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F7(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F8(double,double,double);

#pragma link C++ function InSANE::Math::multipole_E(double,double);
#pragma link C++ function InSANE::Math::multipole_M(double,double);

#pragma link C++ function InSANE::Math::f_rad_length(double);
#pragma link C++ function InSANE::Math::rad_length(int,int);

#pragma link C++ function InSANE::Kine::v0(double,double,double);
#pragma link C++ function InSANE::Kine::vL(double,double);
#pragma link C++ function InSANE::Kine::vT(double,double,double);
#pragma link C++ function InSANE::Kine::vTT(double,double);
#pragma link C++ function InSANE::Kine::vTL(double,double,double);
#pragma link C++ function InSANE::Kine::vTprime(double,double,double);
#pragma link C++ function InSANE::Kine::vTLprime(double,double,double);

#pragma link C++ function InSANE::Kine::Sig_Mott(double,double);
#pragma link C++ function InSANE::Kine::fRecoil(double,double,double);
#pragma link C++ function InSANE::Kine::tau(double,double);
#pragma link C++ function InSANE::Kine::q_abs(double,double);

#pragma link C++ function InSANE::Kine::Qsquared(double,double,double);
#pragma link C++ function InSANE::Kine::xBjorken(double,double,double);
#pragma link C++ function InSANE::Kine::xBjorken_EEprimeTheta(double,double,double);
#pragma link C++ function InSANE::Kine::nu(double,double);

#pragma link C++ function InSANE::Kine::W_xQsq(double,double,double);
#pragma link C++ function InSANE::Kine::W_EEprimeTheta(double,double,double);
#pragma link C++ function InSANE::Kine::xBjorken_WQsq(double,double,double);
#pragma link C++ function InSANE::Kine::Q2_xW(double,double,double);

#pragma link C++ function InSANE::Kine::BeamEnergy_xQ2y(double,double,double,double);
#pragma link C++ function InSANE::Kine::Eprime_xQ2y(double,double,double,double);
#pragma link C++ function InSANE::Kine::Eprime_W2theta(double,double,double,double);
#pragma link C++ function InSANE::Kine::Theta_xQ2y(double,double,double,double);
#pragma link C++ function InSANE::Kine::Theta_epsilonQ2W2(double,double,double,double);

#pragma link C++ function InSANE::Kine::D(double,double,double,double);
#pragma link C++ function InSANE::Kine::d(double,double,double,double);
#pragma link C++ function InSANE::Kine::Eta(double,double,double);
#pragma link C++ function InSANE::Kine::Xi(double,double,double);
#pragma link C++ function InSANE::Kine::Chi(double,double,double,double);

#pragma link C++ namespace InSANE::Phys;
#pragma link C++ function InSANE::Phys::A_pair_corr(double,double,double);
#pragma link C++ function InSANE::Phys::delta_A_pair_corr(double,double,double,double,double,double);
#pragma link C++ function InSANE::Phys::A_elastic_sub_corr(double,double,double,double);
#pragma link C++ function InSANE::Phys::delta_A_elastic_sub_corr(double,double,double,double,double,double,double,double);

#pragma link C++ nestedclass;

#pragma link C++ nestedtypedef;

#endif

