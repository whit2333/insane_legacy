#ifndef InSANECompositeStructureFunctions_HH
#define InSANECompositeStructureFunctions_HH 1

//#include "LHAPDFUnpolarizedPDFs.h"
#include "CTEQ6UnpolarizedPDFs.h"
#include "StatisticalUnpolarizedPDFs.h"
#include "F1F209StructureFunctions.h"
#include "NMC95StructureFunctions.h"
#include "InSANEStructureFunctions.h"
#include "InSANEStructureFunctionsFromPDFs.h"
#include "TList.h"

/** ABC for a composite structure function which calls different structure functions depending on
 *  the kinematic variables, x and Q2.
 *  You must implement GetWeight(i,x,Q2)
 *  which computes the weight for the ith structure function. If it is zero, the function is not evaluate.
 * 
 *  Potential problem might be a jump in values at transition points and discontinuity of the derivative.
 *  Therefore, a smooth transition can be done with the weights.
 *  
 * \ingroup structurefunctions
 */ 
class InSANECompositeStructureFunctions : public InSANEStructureFunctions {

   protected:
      TList fSFList;
      Int_t fNSFs;

   public:
      InSANECompositeStructureFunctions();
      virtual ~InSANECompositeStructureFunctions();

      void   Add(InSANEStructureFunctions * sf);

      // Returns F2 per nucleus (not per nucleon)
      virtual Double_t F2Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A);
      virtual Double_t F1Nuclear(Double_t x,Double_t Qsq,Double_t Z, Double_t A);

      // Pure virtual method which returns the index of the SF to use based on x and Q2. 
      //virtual Int_t GetIndex(Double_t x, Double_t Q2) = 0; 

      /** returns a weight for the ith SF. */
      virtual Double_t GetWeight(Int_t iSF,Double_t x, Double_t Q2) = 0; 

      /// Here is where you implement the specifics
      // proton
      virtual Double_t F2p(Double_t x, Double_t Qsq);
      virtual Double_t F1p(Double_t x, Double_t Qsq);

      // neutron
      virtual Double_t F2n(Double_t x, Double_t Qsq);
      virtual Double_t F1n(Double_t x, Double_t Qsq);

      // Deuteron
      virtual Double_t F2d(Double_t x, Double_t Qsq);
      virtual Double_t F1d(Double_t x, Double_t Qsq);

      // He3 
      virtual Double_t F2He3(Double_t x, Double_t Qsq);
      virtual Double_t F1He3(Double_t x, Double_t Qsq);

      ClassDef(InSANECompositeStructureFunctions,1)
};


/** Best structure function implementation at Low \f$ Q^2 \f$ over all of x.
 *  - CTEQ6 for low X 
 *  - F1F209 for high x
 */
class LowQ2StructureFunctions : public InSANECompositeStructureFunctions {
   private:
      //CTEQ6UnpolarizedPDFs              fPDFs;
      //StatisticalUnpolarizedPDFs          fPDFs;
      //InSANEStructureFunctionsFromPDFs    fCTEQSFs;
      F1F209StructureFunctions            fF1F209SFs;
      NMC95StructureFunctions             fNMCSFs;
      
      //Double_t xrange[5]; 

   public:
      LowQ2StructureFunctions();
      virtual ~LowQ2StructureFunctions();

      virtual Double_t GetWeight(Int_t iSF, Double_t x, Double_t Q2);

      //virtual Int_t GetIndex(Double_t x, Double_t Q2){
      //  if( x>xrange[0] && x<xrange[1]) { // CTEQ
      //     return(0); // CTEQ
      //  } else if( x>=xrange[1] && x<xrange[2] ) {
      //     return(1); // F1F209
      //  } else { /// Error
      //     return(0); // CTEQ
      //  }
      //}


      ClassDef(LowQ2StructureFunctions,1)
};

#endif

