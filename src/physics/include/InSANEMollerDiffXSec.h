#ifndef InSANEMollerDiffXSec_H
#define InSANEMollerDiffXSec_H 1

#include "InSANEExclusiveDiffXSec.h"
#include "InSANEInclusiveDiffXSec.h"
#include "TMath.h"
#include "InSANEFormFactors.h"
#include "TVector3.h"


/** Moller scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class InSANEMollerDiffXSec : public InSANEExclusiveDiffXSec {

   public:
      InSANEMollerDiffXSec();
      virtual ~InSANEMollerDiffXSec();

      virtual InSANEMollerDiffXSec*  Clone(const char * newname) const {
         std::cout << "InSANEMollerDiffXSec::Clone()\n";
         auto * copy = new InSANEMollerDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual InSANEMollerDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Virtual method that returns the calculated values of dependent variables. This method
       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
       *
       *  For example, in mott scattering (elastic) there is really only two random variables,
       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
       *  (assuming the beam energy is known).
       *
       */
      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual void InitializePhaseSpaceVariables();

      double  MollerCrossSection_CM(double E0, double sin2th) const ;

      /** Evaluate Cross Section (mbarn/sr) */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      ClassDef(InSANEMollerDiffXSec, 1)
};


///** Moller scattering.
// * \ingroup inclusiveXSec
// */
//class InSANEInclusiveMollerDiffXSec : public InSANEInclusiveDiffXSec {
//
//   public:
//      InSANEInclusiveMollerDiffXSec();
//      virtual ~InSANEInclusiveMollerDiffXSec();
//
//      virtual InSANEInclusiveMollerDiffXSec*  Clone(const char * newname) const {
//         std::cout << "InSANEInclusiveMollerDiffXSec::Clone()\n";
//         InSANEMollerDiffXSec * copy = new InSANEMollerDiffXSec();
//         (*copy) = (*this);
//         return copy;
//      }
//      virtual InSANEInclusiveMollerDiffXSec*  Clone() const { return( Clone("") ); } 
//
//      /** Virtual method that returns the calculated values of dependent variables. This method
//       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
//       *
//       *  For example, in mott scattering (elastic) there is really only two random variables,
//       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
//       *  (assuming the beam energy is known).
//       *
//       */
//      virtual Double_t * GetDependentVariables(const Double_t * x) const ;
//
//      virtual void InitializePhaseSpaceVariables();
//
//      double  MollerCrossSection_CM(double E0, double sin2th) const ;
//
//      /** Evaluate Cross Section (mbarn/sr) */
//      virtual Double_t EvaluateXSec(const Double_t * x) const;
//
//      ClassDef(InSANEInclusiveMollerDiffXSec, 1)
//};


#endif

