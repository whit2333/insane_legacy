#ifndef InSANEPolarizedStructureFunctionsFromVCSAs_HH
#define InSANEPolarizedStructureFunctionsFromVCSAs_HH 1

#include "InSANEVirtualComptonAsymmetries.h"
#include "InSANEPolarizedStructureFunctions.h"
#include "InSANEStructureFunctions.h"

class InSANEPolarizedStructureFunctionsFromVCSAs: public InSANEPolarizedStructureFunctions {

   protected:

      InSANEVirtualComptonAsymmetries * fVCSAs;
      InSANEStructureFunctions        * fSFs;

   public:

      InSANEPolarizedStructureFunctionsFromVCSAs();
      virtual ~InSANEPolarizedStructureFunctionsFromVCSAs();

      InSANEVirtualComptonAsymmetries * GetVCSAs(){return fVCSAs;}
      void SetVCSAs(InSANEVirtualComptonAsymmetries * v) {fVCSAs = v;}

      InSANEStructureFunctions * GetSFs(){return fSFs;}
      void SetSFs(InSANEStructureFunctions * v) {fSFs = v;}
      

      virtual Double_t g1p(   Double_t x, Double_t Q2);
      virtual Double_t g1n(   Double_t x, Double_t Q2);
      virtual Double_t g1d(   Double_t x, Double_t Q2);
      virtual Double_t g1He3( Double_t x, Double_t Q2);

      virtual Double_t g2p(   Double_t x, Double_t Q2);
      virtual Double_t g2n(   Double_t x, Double_t Q2);
      virtual Double_t g2d(   Double_t x, Double_t Q2);
      virtual Double_t g2He3( Double_t x, Double_t Q2);

   ClassDef(InSANEPolarizedStructureFunctionsFromVCSAs,1)
};


#endif
