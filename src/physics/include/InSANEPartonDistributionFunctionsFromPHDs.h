#ifndef InSANEPartonDistributionFunctionsFromPHDs_HH
#define InSANEPartonDistributionFunctionsFromPHDs_HH 1

#include "InSANEPartonDistributionFunctions.h"
#include "InSANEPolarizedPartonDistributionFunctions.h"
#include "InSANEPartonHelicityDistributions.h"


/** Unpolaried PDFs from the helicity distribution functions. */
class InSANEUnpolarizedPDFsFromPHDs : public InSANEPartonDistributionFunctions {
   private:
      InSANEPHDSet    fPHDSet;

   protected:
      InSANEPartonHelicityDistributions  * fPHDs; //!

   public:
      InSANEUnpolarizedPDFsFromPHDs();
      ~InSANEUnpolarizedPDFsFromPHDs();

      void SetPHDs(InSANEPartonHelicityDistributions * phd){ fPHDs = phd; }
      InSANEPartonHelicityDistributions* GetPHDs() const { return fPHDs ; }

      virtual Double_t * GetPDFs(Double_t x, Double_t Q2);
      virtual Double_t * GetPDFErrors(Double_t x, Double_t Q2);

   ClassDef(InSANEUnpolarizedPDFsFromPHDs,1)
};



/** Polaried PDFs from the helicity distribution functions. */
class InSANEPolarizedPDFsFromPHDs : public InSANEPolarizedPartonDistributionFunctions {
   private:
      InSANEPHDSet    fPHDSet;

   protected:
      InSANEPartonHelicityDistributions  * fPHDs; //!

   public:
      InSANEPolarizedPDFsFromPHDs();
      ~InSANEPolarizedPDFsFromPHDs();

      void SetPHDs(InSANEPartonHelicityDistributions * phd){ fPHDs = phd; }
      InSANEPartonHelicityDistributions* GetPHDs() const { return fPHDs; }

      virtual Double_t * GetPDFs(Double_t x, Double_t Q2);
      virtual Double_t * GetPDFErrors(Double_t x, Double_t Q2);

   ClassDef(InSANEPolarizedPDFsFromPHDs,1)
};

#endif

