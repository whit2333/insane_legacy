#ifndef CJ12UnpolarizedPDFs_H 
#define CJ12UnpolarizedPDFs_H 1 

#include <cstdlib> 
#include <iostream> 
#include "TMath.h"
#include "InSANEFortranWrappers.h"
#include "InSANEPartonDistributionFunctions.h"

/** CTEQ-JLab 12 (CJ12) Parton Distribution Functions. 
 * Paper reference: Phys. Rev. D 87, 094012 (2013)
 * http://inspirehep.net/record/1206325
 *  
 * \ingroup updfs
 */ 
class CJ12UnpolarizedPDFs: public InSANEPartonDistributionFunctions{

   protected:
      int fiSet;

   public: 
      // CJ12UnpolarizedPDFs(); 
      CJ12UnpolarizedPDFs(int iset=200); 
      virtual ~CJ12UnpolarizedPDFs(); 

      virtual Double_t *GetPDFs(Double_t,Double_t); 
      virtual Double_t *GetPDFErrors(Double_t x,Double_t Q2);

      /// for switching between PDF sets (to calculate errors on observables that are combos of the PDFs). 
      void SetGrid(int i){setcj_(&i);};

      ClassDef(CJ12UnpolarizedPDFs,1)  

}; 

#endif 
