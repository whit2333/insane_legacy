#ifndef InSANECompositeDiffXSec_HH
#define InSANECompositeDiffXSec_HH 1

#include "InSANEInclusiveDiffXSec.h"
#include "InSANEInelasticRadiativeTail2.h"
#include "InSANEFortranWrappers.h"
#include "InSANEMathFunc.h"


/** Builds a cross section for a target nucleus from the supplied
 *  proton and neutron cross secitons.
 *  Ideally this should inherit from InSANEDiffXSec not InSANEInclusiveDiffXSec.
 *
 * \ingroup inclusiveXSec
 */
class InSANECompositeDiffXSec : public InSANEInclusiveDiffXSec {

   protected:
      InSANEInclusiveDiffXSec * fProtonXSec;   //->
      InSANEInclusiveDiffXSec * fNeutronXSec;  //->

   public:

      InSANECompositeDiffXSec();
      InSANECompositeDiffXSec(const InSANECompositeDiffXSec& rhs) ;
      virtual ~InSANECompositeDiffXSec();
      InSANECompositeDiffXSec& operator=(const InSANECompositeDiffXSec& rhs) ;
      virtual InSANECompositeDiffXSec*  Clone(const char * newname) const ;
      virtual InSANECompositeDiffXSec*  Clone() const ; 

      virtual Double_t EvaluateXSec(const Double_t * x) const ;

      // Here we copied a bunch of setters from InSANEDiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void       SetTargetMaterial(const InSANETargetMaterial& mat);
      void SetTargetNucleus(const InSANENucleus & targ);

      void       SetTargetMaterialIndex(Int_t i);

      virtual void  SetUnits(const char * t);
      virtual void  SetBeamEnergy(Double_t en);
      virtual void  SetPhaseSpace(InSANEPhaseSpace * ps);
      virtual void  InitializePhaseSpaceVariables();
      virtual void  InitializeFinalStateParticles();
      virtual void  SetParticleType(Int_t pdgcode, Int_t part = 0) ;

      ClassDef(InSANECompositeDiffXSec,1)
};



#endif

