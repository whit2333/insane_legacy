#ifndef MRST2002UnpolarizedPDFs_H 
#define MRST2002UnpolarizedPDFs_H 

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include "InSANEPartonDistributionFunctions.h"
#include "InSANEFortranWrappers.h"

/** MRST2002 unpolarized parton distruction functions.
 *  reference: Eur. Phys. J. C 28, 455 (2003); arXiv: 0211080 [hep-ph] 
 *
 * \ingroup updfs
 */
class MRST2002UnpolarizedPDFs: public InSANEPartonDistributionFunctions{

   private:
      int fPDFSet; 
      double fuv,fubar;
      double fdv,fdbar;
      double fstr,fsbar;
      double fchm,fbot;
      double fglu; 
 
      void Fit(Double_t,Double_t); 
      Double_t Extrapolate(Double_t,Double_t,Double_t,Double_t,Double_t);

   public:
      MRST2002UnpolarizedPDFs(int iset=1);
      ~MRST2002UnpolarizedPDFs();

      /// inputs are x and Q2. 
      Double_t *GetPDFs(Double_t,Double_t);
      Double_t *GetPDFErrors(Double_t,Double_t);

      /// for switching between PDF sets (1 = NLO, 2 = NNLO). 
      void SetGrid(int i){fPDFSet = i;}

      ClassDef(MRST2002UnpolarizedPDFs,1)
};

#endif  
