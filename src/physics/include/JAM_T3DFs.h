#ifndef InSANE_JAM_T3DFs_HH
#define InSANE_JAM_T3DFs_HH

#include "TMutex.h"
#include "Twist3DistributionFunctions.h"

namespace insane {
  namespace physics {

    class JAM_T3DFs : public Twist3DistributionFunctions {

      public:
        JAM_T3DFs();
        virtual ~JAM_T3DFs();

        virtual std::array<double,NPartons> Calculate    (double x, double Q2) const;
        virtual std::array<double,NPartons> Uncertainties(double x, double Q2) const;

        static TMutex* fgInit_mutex;

        ClassDef(JAM_T3DFs,1)
    };
  }
}

#endif

