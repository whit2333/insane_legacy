#ifndef LuciteGeometryCalculator_h
#define LuciteGeometryCalculator_h

#include <TROOT.h>
#include <TFile.h>
#include <TObject.h>
#include "InSANEGeometryCalculator.h"
#include <iostream>
#include "TMath.h"
#include "GasCherenkovGeometryCalculator.h"
#include "TVector3.h"
#include "TRotation.h"


/** Concrete class for SANE Lucite Hodoscope geometry information
 *
 *  Each lucite bar is curved at a radius of  240 cm
 *  with arc length 23 degrees, 3.5 thick and 6 cm tall
 *
 *  There are pmts on both sides of a bar.
 *
 * \ingroup Geometry
 * \ingroup hodoscope
 */
class LuciteGeometryCalculator  : public InSANEGeometryCalculator {
private:
   TRotation  fDetectorRotation; /// Rotation matrix in BETA coordinates
   TVector3   fBBoxOrigin;    /// cm, in BETA coords
   TVector3   fBBoxDim;       /// cm, Bounding box dimensions in cm. 
   TVector3   fDetectorBox;   /// cm, A box that, when placed and rotated, fits entirely in the orthogonal system of the bounding box. 

public :
   Double_t fBarRadius;
   Double_t fBarHeight;
   Double_t fBarThickness;
   Double_t fBarArcLength;
   Double_t fBarWrapThickness;
   Double_t fLuciteZPosition;      /// Z position from survey

   Double_t fDistanceFromTarget;   /// Distandce from target to the face of the detector at (x=0,y=0) in bc coords
   Double_t fVerticalOffset;
   Double_t fHorizontalOffset;

   Double_t fSurveyYaw;   /// Survey report measured yaw radians
   Double_t fSurveyPitch; /// Survey report measured yaw radians
   Double_t fSurveyRoll;  /// Survey report measured yaw radians

   Double_t fLightGuideLength;     /// Length of light guide which is attached to Luictie bar at 45 degree cut
   Double_t fLightGuideEndHeight;  /// Height of rectangular end (should be roughly the same as fBarHeight)
   Double_t fLightGudieEndWidth;   /// Length of side with the 45 degree cut (measured viewing from the top).
   Double_t fLightGuideDiameter;   /// Diameter at PMT end
   Double_t fTotalLightLength;     /// 
public :
   static  LuciteGeometryCalculator * GetCalculator();
   ~LuciteGeometryCalculator();

   Int_t fLuciteBarToMirrorMax[4]; /// There can be overlap from bar to bar
   Int_t fLuciteBarToMirrorMin[4];/// There can be overlap from bar to bar

   /** Get the horizontal bar index number using the PMT number
    *
    *  Lucite bars are numbered from bottom top, starting with 1,
    *  is beam side (negative), then 2 being the bottom (positive) etc
    */
   int iBar(int pmt) {
      if ((pmt) % 2 == 0) return(2);
      else return(1);
   };

   /** Get the vertical bar index number  using the PMT number
    *
    *  Lucite bars are numbered from bottom top, starting with 1,
    *  is beam side (negative), then 2 being the bottom (positive) etc
    */
   int jBar(int pmt) {
      return((pmt - 1) / 2 + 1);
   };

   /** Get the vertical position of lucite bar (units are cm)
    *  Bar number starts at 1
    *
    *  \todo calbirate positions
    */
   Double_t GetLuciteYPosition(int bar) {
      return((((double)(bar - 14)) - 0.5) * fBarHeight);
   }

   /** Get the Luicte row using the BIGCAL row index number
    *  Assuming that there are 1 bars for each block
    *
    *  \todo This needs a better implementation using the calibrations
    */
   int GetLuciteRow(int jBigcalRow) {
      return((jBigcalRow - 1) / 2 + 1);
   }

   /** Returns true if ibar is geometrically correlated with imirror
    */
   bool  IsBarInMirror(const int ibar, const int imirror) const  {
      int barVert = (ibar - 1) / 7 + 1;
      int mirVert = (imirror - 1) / 2 + 1;
      if (barVert == mirVert) return(true);
      else return(false);
   }

   /** Returns the Bigcal row using the Lucite row index, within +-2 blocks */
   int GetBigcalRow(int jLucRow) {
      return(2 * (jLucRow));
   }

   /** rough fit to x positions */
   Double_t GetBigcalXPosition(Double_t timeDif) {
      return((200.0 / 100.0) * timeDif - 50.0) ;
   }

   /** Given a cherenkov mirror number, this returns the maximum possible lucite bar number
    */
   Int_t GetMaxBarNumberForMirror(Int_t cerMirrorNumber) {
      if (cerMirrorNumber < 1 || cerMirrorNumber > 8) {
         std::cout << "Cherenkov Mirror Number does not make sense in GetMaxBarNumber \n";
         return (-9);
      } else return(fLuciteBarToMirrorMax[(cerMirrorNumber-1)/2]);
   }

   /** Given a cherenkov mirror number, this returns the minimum possible lucite bar number
    */
   Int_t GetMinBarNumberForMirror(Int_t cerMirrorNumber) {
      if (cerMirrorNumber < 1 || cerMirrorNumber > 8) {
         std::cout << "Cherenkov Mirror Number does not make sense in GetMaxBarNumber \n";
         return (-9);
      } else return(fLuciteBarToMirrorMin[(cerMirrorNumber-1)/2]);
   }

   /** Returns the distance from the target to the face of the detector. */
   Double_t GetDistanceFromTarget(){ return(fDistanceFromTarget);}


private :


   static LuciteGeometryCalculator * fgLuciteGeometryCalculator;

   LuciteGeometryCalculator();

   ClassDef(LuciteGeometryCalculator, 2)
};


#endif
