#ifndef InSANEDetectorPedestal_H
#define InSANEDetectorPedestal_H 1
#include <iostream>
#include <stdlib.h>

#include "TNamed.h"
#include "TString.h"

#include "InSANEDatabaseManager.h"
#include "TSQLServer.h"
#include "TSQLRow.h"
#include "TSQLResult.h"

/** ABC for an event in a detector pedestals
 *
 * \ingroup Detectors
 */
class InSANEDetectorPedestal : public TNamed {
public :
   InSANEDetectorPedestal();
   InSANEDetectorPedestal(Int_t id, Double_t val, Double_t width);
   ~InSANEDetectorPedestal();

   ULong_t Hash() const { return (ULong_t)fPedestalId ; }
   Bool_t  IsEqual(const TObject *obj) const {
      return fPedestalId == ((InSANEDetectorPedestal*)obj)->GetNumber();
   }
   Bool_t  IsSortable() const { return kTRUE; }
   Int_t   Compare(const TObject *obj) const {
      if (fPedestalId > ((InSANEDetectorPedestal*)obj)->GetNumber())
         return 1;
      else if (fPedestalId < ((InSANEDetectorPedestal*)obj)->GetNumber())
         return -1;
      else return 0;
   }
   Int_t GetNumber() { return fPedestalId; }
   Int_t GetID() { return fPedestalId; }

   /** Set all the values in one shot.
    */
   Bool_t DefinePedestal(Int_t id, Double_t val, Double_t width) {
      fPedestalMean = val;
      fPedestalWidth = width;
      fPedestalId = id;
      return(true);
   }

   void Print(Option_t * opt = "") const ; // *MENU*
   void PrintPedestalHead() const ; // *MENU*
   void PrintPedestal() const ; // *MENU*

   TString * GetSQLInsertString(const char * det, Int_t run_number);
   Int_t     GetPedestal(const char * det, Int_t run_number, Int_t id);

   static int SetMeanWidth_callback(void * thePed, int argc, char **argv, char **azColName);

   void SetMean(Double_t m) { fPedestalMean = m; }
   void SetWidth(Double_t w) { fPedestalWidth = w; }

   Double_t fPedestalMean;
   Double_t fPedestalWidth;
   Int_t fPedestalId;

   ClassDef(InSANEDetectorPedestal, 2)
};



#endif
