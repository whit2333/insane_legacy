#ifndef HumanCoordinateSystem_h
#define HumanCoordinateSystem_h

#include <TROOT.h>
#include <TFile.h>
#include <TObject.h>
#include "InSANECoordinateSystem.h"

/** Human coordinate system.
 *
 * Human Coordinate system has x-z plane horizontal and y vertical.
 * z is along the beam pipe. This is used as the default coordinate
 * system for  concrete InSANECoordinateSystem::GetRotationMatrixTo
 *
 * You need not know about all other coordinate systems, just a path to get to the relevent ones.
 *
 * \ingroup Coordinates
 */
class HumanCoordinateSystem  : public InSANECoordinateSystem {
public :
   HumanCoordinateSystem(const char * name = "humanCoords"):
      InSANECoordinateSystem(name) {
   }

   ~HumanCoordinateSystem() {
   }

   TRotation & GetRotationMatrixTo(const char * system = "humanCoords") {
      if (!strcmp(system, "Human") ||
          !strcmp(system, "HUMAN") ||
          !strcmp(system, "human") ||
          !strcmp(system, "HumanCoords") ||
          !strcmp(system, "humanCoords")) {
         fRotationMatrix.SetToIdentity();
      } else if (!strcmp(system, "Hms") ||
                 !strcmp(system, "HMS") ||
                 !strcmp(system, "hms") ||
                 !strcmp(system, "hmsInSANECoordinateSystem")) {
         fRotationMatrix.SetToIdentity();
         fRotationMatrix.RotateZ(-1.0 * TMath::Pi() / 2.0);
      } else {
         std::cout << " Unknown coordinate system, " << system << "! Returning identity matrix.\n";
         fRotationMatrix.SetToIdentity();
      }
      return(fRotationMatrix);
   }

   TVector3 & GetOriginTranslationTo(const char * system  = "humanCoords") {
      return(fTranslation);
   }

   void DescribeCoordinateSystem() {
      std::cout << "===== HumanCoordinateSystem =====" << ". \n";
      std::cout << "Z is along beam direction, y is \"up\" (vertical), thus, x is beam left\n; " ;

   }

   void DrawCoordinateSystems();


private :

   ClassDef(HumanCoordinateSystem, 1)
};


#endif
