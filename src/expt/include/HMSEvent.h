#ifndef HMSEvent_h
#define HMSEvent_h

 //#include <TChain.h>
#include "TObject.h"
#include "TTree.h"
#include "TFile.h"
#include "InSANEDetectorEvent.h"


/** Concrete class for an event in the High Momentum Spectrometer (HMS) in Hall C.
 *
 * \todo Main analysis of HMS is done in the Hall C Analyzer (fortran/cernlib based)
 *
 * \ingroup Events
 */
class HMSEvent : public InSANEDetectorEvent {
   public :
      HMSEvent() {
      }
      ~HMSEvent() { }

      void ClearEvent(Option_t * opt = ""){
         InSANEDetectorEvent::ClearEvent(opt);
      }

      Double_t         hms_p;
      Double_t         hms_e;
      Double_t         hms_theta;
      Double_t         hms_phi;
      Double_t         hsxfp_s;
      Double_t         hsyfp_s;
      Double_t         hsxpfp_s;
      Double_t         hsypfp_s;
      Double_t         hms_xtar;
      Double_t         hms_ytar;
      Double_t         hms_yptar;
      Double_t         hms_xptar;
      Double_t         hms_delta;
      Double_t         hms_start;
      Double_t         hsshtrk_s;
      Double_t         hsshsum_s;
      Double_t         hsbeta_s;
      Double_t         hms_cer_npe1;
      Double_t         hms_cer_npe2;
      Double_t         hms_cer_adc1;
      Double_t         hms_cer_adc2;

      ClassDef(HMSEvent, 2)
};

#endif

