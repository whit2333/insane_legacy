#ifndef BETATimingCorrection_HH
#define BETATimingCorrection_HH 1
#include "BETACorrection.h"
#include "SANERunManager.h"
#include "TF1.h"
#include "InSANEAnalysis.h"
#include "InSANEDetectorCalibration.h"

/**  BETA Timing Corrections
 *
 *  \todo These include, an aligned TDC peak and timewalk corrections
 *
 *  \ingroup Calculations
 *  \ingroup BETA
 */
class BETATimingCorrection : public BETACorrection {
public:
   BETATimingCorrection(InSANEAnalysis * analysis = nullptr) : BETACorrection(analysis) {
      SetNameTitle("BETATimingCorrection", "BETATimingCorrection");
   }

   ~BETATimingCorrection() {};

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  BETATimingCorrection - \n ";
      std::cout <<  "     Aligns TDC Peaks and values set to fTDCAlign. \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }
   /** Apply the correction
    *   <ul>
    *   <li>Calculates fTDCAlign</li>
    *   </ul>
    */
   virtual Int_t Apply();

   ClassDef(BETATimingCorrection, 1)
};


/**  Time walk correction
 *
 *  This is only for those detectors which had an ADC value associated with it
 */
class BETATimeWalkCorrection : public BETACorrection {
public:
   BETATimeWalkCorrection(InSANEAnalysis * analysis = nullptr) : BETACorrection(analysis) {
      fCherenkovTimewalkCalibration = nullptr;
      SetNameTitle("BETATimeWalkCorrection", "BETATimeWalkCorrection");
   }

   ~BETATimeWalkCorrection() {
      ;
   }


   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  BETATimeWalkCorrection - \n ";
      std::cout <<  "     Performs timewalk correction to TDC values of the SANE Gas Cherenkov.\n";
      std::cout <<  "     It corrects both the un-aligned and aligned values.\n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   virtual void DefineCorrection() {} ;

public:
   TF1 * fCherenkovTimeWalk[12];
   InSANEDetectorCalibration *  fCherenkovTimewalkCalibration;
   InSANECalibration *          fCherenkovTimewalks[12];
   Double_t                     fCherenkovTimewalkZeros[12];

   virtual Int_t Initialize() {
      BETACorrection::Initialize();
      SANERunManager::GetRunManager()->GetCurrentFile()->cd("");
      SANERunManager::GetRunManager()->GetCurrentFile()->cd("timewalk");

      fCherenkovTimewalkCalibration = (InSANEDetectorCalibration*) gROOT->FindObject("cherenkovTimeWalk");
      if (fCherenkovTimewalkCalibration) {
         for (int i = 0; i < fCherenkovTimewalkCalibration->fNChannels; i++) {
            fCherenkovTimewalks[i] = fCherenkovTimewalkCalibration->GetCalibration(i);
            if (fCherenkovTimewalks[i]) fCherenkovTimeWalk[i] = (TF1*)fCherenkovTimewalks[i]->GetFunction(0);
            fCherenkovTimewalkZeros[i] = fCherenkovTimewalks[i]->GetParameter(0)->GetVal();
         }
      } else {
         for (int i = 0; i < 8; i++) {
            fCherenkovTimeWalk[i] = (TF1*)
                                    SANERunManager::GetRunManager()->GetCurrentRun()->fCherenkovTimeWalkFits.FindObject(Form("cer%dtimewalk", i + 1));
            if (! fCherenkovTimeWalk[i]) std::cout << " TIMEWALK FUNCTION NOT FOUND\n";
            fCherenkovTimewalkZeros[i] = 0;
         }
      }
      SANERunManager::GetRunManager()->GetCurrentFile()->cd();
      return(0);
   }

   /** Apply the timewalk correction
    *   <ul>
    *   <li>Corrects fTDC due to timewalk (using same formula)</li>
    *   </ul>
    */
   virtual Int_t Apply();

   ClassDef(BETATimeWalkCorrection, 1)
};

#endif

