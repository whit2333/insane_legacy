#ifndef InSANERunSeries_HH
#define InSANERunSeries_HH

#include "InSANERun.h"
#include "TGraphErrors.h"
#include "TList.h"
#include <vector>
#include "TVectorT.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TF1.h"
#include "TString.h"
#include "TSQLResult.h"
#include "TSQLRow.h"
#include "InSANEDatabaseManager.h"
#include "InSANERunManager.h"
#include "SANEAnalysisManager.h"
#include "TSortedList.h"
#include "TNamed.h"
#include "SANEMeasuredAsymmetry.h"
#include "InSANEDilutionFactor.h"
#include "InSANERawAsymmetry.h"

/**  A wrapper for an InSANERun object, providing sorting by an arbitray number
 *   Useful for plotting runs which are not necessarily in order
 */
class InSANERunSeriesObject : public TObject {
   public:
      /** Constructor of Run object for adding to an InSANERunSeries
       *  i is the x value
       */
      InSANERunSeriesObject(int i = 0 , InSANERun * run = nullptr) : fRun(run) {
         num = i;
         fXValue = num;
      }
      ~InSANERunSeriesObject() {
         Printf("~InSANERunSeriesObject = %d", num);
      }
      void    SetNum(int i) {
         num = i;
      }
      int     GetNum() {
         return num;
      }
      void    Print(Option_t *) const {
         Printf("InSANERunSeriesObject = %d", num);
      }
      ULong_t Hash() const {
         return num;
      }
      Bool_t  IsEqual(const TObject *obj) const {
         return num == ((InSANERunSeriesObject*)obj)->GetNum();
      }
      Bool_t  IsSortable() const {
         return kTRUE;
      }
      Int_t   Compare(const TObject *obj) const {
         if (num > ((InSANERunSeriesObject*)obj)->GetNum())
            return 1;
         else if (num < ((InSANERunSeriesObject*)obj)->GetNum())
            return -1;
         else
            return 0;
      }
      void SetRun(InSANERun * run) {
         fRun = run;
      }
      InSANERun * GetRun() {
         return fRun;
      }
      Int_t fXValue;
   protected:
      InSANERun* fRun; //->
   private:
      int  num;
      ClassDef(InSANERunSeriesObject, 1)
};


/** A Run Quantity accumulated over run
 *
 */
class InSANERunQuantity : public TNamed {
   public:
      InSANERunQuantity(const char * name = "aQuantity", const char * title = "A quantity") ;
      virtual ~InSANERunQuantity() { }

      virtual void FillGraph(InSANERunSeriesObject * runObj = nullptr);

      /** Inherit for a test with greater degree of complexity.
       *  Used in FillGraph()
       *  For example if run is not polarized, you don't want to plot the
       *  polarization
       */
      virtual Bool_t IsValidRun() = 0;

      /** pure virtual method to create fGraph object,
       * ie, TGraph, TGraphErrors, TGraphAsymErrors etc...
       */
      virtual TGraph * CreateGraph();

      virtual void CountRun() {
         if (IsValidRun()) fNDataPoints++;
      }

      /** The quantity associated with the current run being looked at.
       */
      virtual Double_t GetQuantity() {
         return fValue;
      }
      void     SetQuantity(Double_t  val) {
         fValue = val;
      }

      TGraph * GetGraph() {
         return fGraph;
      }
      void SetRun(InSANERun * run) {
         fRun = run;
      }
      InSANERun * GetRun() {
         return fRun;
      }

      Int_t fMarkerStyle;
      Int_t fMarkerColor;
      Double_t fMarkerSize;

   protected:
      InSANERun   * fRun; //!
      TGraph      * fGraph; //!
      Double_t      fValue;
      Int_t         fiPoint;
      Int_t         fNDataPoints;

      ClassDef(InSANERunQuantity, 1)
};



/** Concrete imp of InSANERunQuantity for a polarization.
 *
 */
class InSANERunPolarization : public InSANERunQuantity {
   public:
      InSANERunPolarization(const char * name = "aPol", const char * title = "A pol")
         : InSANERunQuantity(name, title) {   }
      virtual ~InSANERunPolarization() { }

      virtual Bool_t IsValidRun() {
         if (fRun) {
            if (GetQuantity() > -1.0 && GetQuantity() < 1.0)
               return true ;
         }
         return false;
      }

      virtual Double_t GetQuantity() {
         fValue = fRun->fAverageBeamCurrent / 100.0;
         return(fValue);
      }

      void FillGraph(InSANERunSeriesObject * runObj = nullptr) {
         auto xvalue = (Double_t)fiPoint;
         if (runObj) xvalue = runObj->fXValue;
         if (IsValidRun()) {
            fGraph->SetPoint(fiPoint, xvalue, GetQuantity());
            /*         fGraph->GetXaxis()->SetBinLabel(fiPoint,Form("%d",fRun->fRunNumber));*/
            fiPoint++;
         }
      }

      ClassDef(InSANERunPolarization, 1)
};


/** Offline Target Polarization
 */
class InSANERunTargOfflinePol : public InSANERunPolarization {
   public:
      InSANERunTargOfflinePol(const char * name = "aPol", const char * title = "A pol") :
         InSANERunPolarization(name, title) {}
      ~InSANERunTargOfflinePol() {}
      virtual Double_t GetQuantity() {
         if (fRun)fValue = fRun->fTargetOfflinePolarization / 100.0;
         return(fValue);
      }
      ClassDef(InSANERunTargOfflinePol, 1)
};

/** Online Target Polarization
 */
class InSANERunTargOnlinePol : public InSANERunPolarization {
   public:
      InSANERunTargOnlinePol(const char * name = "aPol", const char * title = "A pol") :
         InSANERunPolarization(name, title) {}
      ~InSANERunTargOnlinePol() {}
      virtual Double_t GetQuantity() {
         if (fRun)fValue = fRun->fTargetOnlinePolarization / 100.0;
         return(fValue);
      }
      ClassDef(InSANERunTargOnlinePol, 1)
};

/** Electron Beam Polarization
 */
class InSANERunBeamPol : public InSANERunPolarization {
   public:
      InSANERunBeamPol(const char * name = "aPol", const char * title = "A pol") :
         InSANERunPolarization(name, title) {}
      ~InSANERunBeamPol() {}
      virtual Double_t GetQuantity() {
         if (fRun)fValue = fRun->fBeamPolarization / 100.0;
         return(fValue);
      }
      ClassDef(InSANERunBeamPol, 1)
};

//___________________________________________________________________//

/** Top Target with positive polarization
 */
class InSANERunTargTopPosPol : public InSANERunTargOnlinePol {
   public:
      InSANERunTargTopPosPol(const char * name = "aPol", const char * title = "A pol") :
         InSANERunTargOnlinePol(name, title) {}
      ~InSANERunTargTopPosPol() {}
      virtual Bool_t IsValidRun() {
         if (fRun)
            if (fRun->fTargetCup == InSANERun::kTOP)
               if (fRun->fTargetPolarizationSign == InSANERun::kPOSITIVE)
                  if (GetQuantity() > -1.0 && GetQuantity() < 1.0) {
                     return true ;
                  }
         return false;
      }
      ClassDef(InSANERunTargTopPosPol, 1)
};

/** Bottom Target with positive polarization
 */
class InSANERunTargBotPosPol : public InSANERunTargOnlinePol {
   public:
      InSANERunTargBotPosPol(const char * name = "aPol", const char * title = "A pol") :
         InSANERunTargOnlinePol(name, title) {}
      ~InSANERunTargBotPosPol() {}
      virtual Bool_t IsValidRun() {
         if (fRun)
            if (fRun->fTargetCup == InSANERun::kBOTTOM)
               if (fRun->fTargetPolarizationSign == InSANERun::kPOSITIVE)
                  if (GetQuantity() > -1.0 && GetQuantity() < 1.0) {
                     return true ;
                  }
         return false;
      }
      ClassDef(InSANERunTargBotPosPol, 1)
};

/** Top Target with negative polarization
 */
class InSANERunTargTopNegPol : public InSANERunTargOnlinePol {
   public:
      InSANERunTargTopNegPol(const char * name = "aPol", const char * title = "A pol") :
         InSANERunTargOnlinePol(name, title) {}
      ~InSANERunTargTopNegPol() {}
      virtual Bool_t IsValidRun() {
         if (fRun)
            if (fRun->fTargetCup == InSANERun::kTOP)
               if (fRun->fTargetPolarizationSign == InSANERun::kNEGATIVE)
                  if (GetQuantity() > -1.0 && GetQuantity() < 1.0) {
                     return true ;
                  }
         return false;
      }
      ClassDef(InSANERunTargTopNegPol, 1)
};

/** Bottom Target with negative polarization
 */
class InSANERunTargBotNegPol : public InSANERunTargOnlinePol {
   public:
      InSANERunTargBotNegPol(const char * name = "aPol", const char * title = "A pol") :
         InSANERunTargOnlinePol(name, title) {}
      ~InSANERunTargBotNegPol() {}
      virtual Bool_t IsValidRun() {
         if (fRun)
            if (fRun->fTargetCup == InSANERun::kBOTTOM)
               if (fRun->fTargetPolarizationSign == InSANERun::kNEGATIVE)
                  if (GetQuantity() > -1.0 && GetQuantity() < 1.0) {
                     return true ;
                  }
         return false;
      }
      ClassDef(InSANERunTargBotNegPol, 1)
};

class InSANERawRunAsymmetry : public InSANERunQuantity {
   protected: 
      InSANERawAsymmetry * fAsym; 
      TString fAsymName;

   public:
      InSANERawRunAsymmetry(const char * n = "run-asym1") {
         fAsymName = n;
         fAsym = nullptr;
      }
      virtual ~InSANERawRunAsymmetry() {}
      void SetRun(InSANERun * run) {
         fRun = run;
         fAsym = (InSANERawAsymmetry*)fRun->fResults.FindObject(fAsymName.Data());
         if(!fAsym) {
            std::cout << "Run: " << fRun->fRunNumber << ", " << fAsymName.Data() << " not found\n";
            //fRun->fResults.Print();
         } 
      }
      virtual Bool_t IsValidRun() {
         if(fRun) {
            if(!fAsym) fAsym = (InSANERawAsymmetry*)fRun->fResults.FindObject(fAsymName.Data());
            if(!fAsym) {
               std::cout << "Run: " << fRun->fRunNumber << ", " << fAsymName.Data() << " not found\n";
               //fRun->fResults.Print();
            } 
            if(fAsym) { 
               fAsym->Print();
               return true; 
            }
         }
         return false;
      }
      virtual Double_t GetQuantity(){
         if(fRun) {
            fAsym = (InSANERawAsymmetry*)fRun->fResults.FindObject(fAsymName.Data());
            if(fAsym){
               fAsym->Print();
               fValue = fAsym->GetAsymmetry();
               return(fValue);
            }
            std::cout << fAsymName.Data() << " not found\n";
         }
         fValue = 0.0;
         return(fValue);
      }
      virtual Double_t GetQuantityError() {
         if(!fAsym) return(0.0);
         Double_t N1, N2, Pb, Pt, err;
         Pt = fAsym->GetTargetPol();
         Pb = fAsym->GetBeamPol();
         N1 = (Double_t) fAsym->fCount[0] ;
         N2 = (Double_t) fAsym->fCount[1] ;
         /*      if(fRun)fValue = fRun->fTotalAsymmetry; */
         err = TMath::Sqrt(((0.0025000000000000005 * TMath::Power(N1, 3) -
                     0.0025000000000000005 * TMath::Power(N1, 2) * N2 -
                     0.0025000000000000005 * N1 * TMath::Power(N2, 2) +
                     0.0025000000000000005 * TMath::Power(N2, 3)) * TMath::Power(Pb, 2) +
                  (0.0025000000000000005 * TMath::Power(N1, 3) -
                   0.0025000000000000005 * TMath::Power(N1, 2) * N2 +
                   0.0025000000000000005 * TMath::Power(N2, 3) +
                   N1 * N2 * (-0.0025000000000000005 * N2 + 4.*TMath::Power(Pb, 2))) *
                  TMath::Power(Pt, 2)) / (TMath::Power(N1 + N2, 3) * TMath::Power(Pb, 4) * TMath::Power(Pt, 4))) ;
         return(err/fAsym->GetDilution());
      }
      virtual TGraph * CreateGraph() {
         if (!fNDataPoints) printf(" CreateGraph() called with 0 data points!");
         fGraph = new TGraphErrors(fNDataPoints);
         fGraph->SetMarkerColor(fMarkerColor);
         fGraph->SetLineColor(fMarkerColor);
         fGraph->SetMarkerStyle(fMarkerStyle);
         fGraph->SetMarkerSize(fMarkerSize);
         fGraph->SetNameTitle(GetName(), GetTitle());
         return(fGraph);
      }

      void FillGraph(InSANERunSeriesObject * runObj = nullptr) {
         auto xvalue = (Double_t)fiPoint;
         if (runObj) xvalue = runObj->fXValue;
         if (IsValidRun()) {
            std::cout << " Fill Graph " << std::endl;
            SetRun(runObj->GetRun());
            fGraph->SetPoint(fiPoint, xvalue, GetQuantity());
            ((TGraphErrors*)fGraph)->SetPointError(fiPoint, 0.0, GetQuantityError());
            /*         fGraph->GetXaxis()->SetBinLabel(fiPoint,Form("%d",fRun->fRunNumber));*/
            fiPoint++;
         }
      }

      ClassDef(InSANERawRunAsymmetry,1)
};


class InSANERawRunAsymmetryDilution : public InSANERawRunAsymmetry {
   public:
      InSANERawRunAsymmetryDilution(const char * n="run-asym1"):InSANERawRunAsymmetry(n) {}
      ~InSANERawRunAsymmetryDilution() {}
      virtual Double_t GetQuantity(){
         if(fRun) {
            fAsym = (InSANERawAsymmetry*)fRun->fResults.FindObject(fAsymName.Data());
            if(fAsym){
               fAsym->Print();
               fValue = fAsym->GetDilution();
               return(fValue);
            }
            std::cout << fAsymName.Data() << " not found\n";
         }
         fValue = 0.0;
         return(fValue);
      }
      virtual Double_t GetQuantityError() {
         return(0.0);
      }
      ClassDef(InSANERawRunAsymmetryDilution,1)
};

class InSANERawRunAsymmetryCounts : public InSANERawRunAsymmetry {
   public:
      InSANERawRunAsymmetryCounts(const char * n="run-asym1"):InSANERawRunAsymmetry(n) {}
      ~InSANERawRunAsymmetryCounts() {}
      virtual Double_t GetQuantity(){
         if(fRun) {
            fAsym = (InSANERawAsymmetry*)fRun->fResults.FindObject(fAsymName.Data());
            if(fAsym){
               fAsym->Print();
               fValue = fAsym->GetTotalCount();
               return(fValue);
            }
            std::cout << fAsymName.Data() << " not found\n";
         }
         fValue = 0.0;
         return(fValue);
      }
      virtual Double_t GetQuantityError() {
         return(0.0);
      }
      ClassDef(InSANERawRunAsymmetryCounts,1)
};

/** Run Series Asymmetry
 */
class InSANERunAsymmetry : public InSANERunQuantity {
   protected:
      Double_t fDilutionFactor;
   public:
      InSANERunAsymmetry(): fDilutionFactor(0.12) { }
      virtual ~InSANERunAsymmetry() { }
      void SetRun(InSANERun * run) {
         fRun = run;
         InSANEDilutionFactor * df = nullptr;
         df = (InSANEDilutionFactor*) fRun->fDilution.FindObject("average-df");
         if(df) {
            fDilutionFactor = df->GetDilution(); 
            df->Print();
         } else {
            fDilutionFactor = 0.12;
         }

      }
      virtual Double_t GetQuantity() {
         if (fRun){
            fValue = (fRun->fTotalAsymmetry) / (fDilutionFactor*fRun->fTargetOfflinePolarization / 100.0 * fRun->fBeamPolarization / 100.0);
         }
         return(fValue);
      }
      virtual Double_t GetQuantityError() {
         Double_t N1, N2, Pb, Pt, err;
         Pt = fRun->fTargetOfflinePolarization / 100.0;
         Pb = fRun->fBeamPolarization / 100.0;
         std::cout << "Using dilution factor of " << fDilutionFactor << "\n";
         N1 = (Double_t) fRun->fTotalNPlus ;
         N2 = (Double_t) fRun->fTotalNMinus ;
         /*      if(fRun)fValue = fRun->fTotalAsymmetry; */
         err = TMath::Sqrt(((0.0025000000000000005 * TMath::Power(N1, 3) -
                     0.0025000000000000005 * TMath::Power(N1, 2) * N2 -
                     0.0025000000000000005 * N1 * TMath::Power(N2, 2) +
                     0.0025000000000000005 * TMath::Power(N2, 3)) * TMath::Power(Pb, 2) +
                  (0.0025000000000000005 * TMath::Power(N1, 3) -
                   0.0025000000000000005 * TMath::Power(N1, 2) * N2 +
                   0.0025000000000000005 * TMath::Power(N2, 3) +
                   N1 * N2 * (-0.0025000000000000005 * N2 + 4.*TMath::Power(Pb, 2))) *
                  TMath::Power(Pt, 2)) / (TMath::Power(N1 + N2, 3) * TMath::Power(Pb, 4) * TMath::Power(Pt, 4))) ;
         return(err/fDilutionFactor);
      }

      virtual Bool_t IsValidRun() {
         Double_t N1, N2, Pb, Pt;
         Pt = fRun->fTargetOfflinePolarization / 100.0;
         Pb = fRun->fBeamPolarization / 100.0;
         N1 = (Double_t) fRun->fTotalNPlus ;
         N2 = (Double_t) fRun->fTotalNMinus ;
         if (Pb*Pt == 0.0) return false;
         return true;
      }

      virtual TGraph * CreateGraph() {
         if (!fNDataPoints) printf(" CreateGraph() called with 0 data points!");
         fGraph = new TGraphErrors(fNDataPoints);
         fGraph->SetMarkerColor(fMarkerColor);
         fGraph->SetMarkerStyle(fMarkerStyle);
         fGraph->SetMarkerSize(fMarkerSize);
         fGraph->SetNameTitle(GetName(), GetTitle());
         return(fGraph);
      }

      void FillGraph(InSANERunSeriesObject * runObj = nullptr) {
         auto xvalue = (Double_t)fiPoint;
         if (runObj) xvalue = runObj->fXValue;
         if (IsValidRun()) {
            std::cout << " Fill Graph " << std::endl;
            SetRun(runObj->GetRun());
            fGraph->SetPoint(fiPoint, xvalue, GetQuantity());
            ((TGraphErrors*)fGraph)->SetPointError(fiPoint, 0.0, GetQuantityError());
            /*         fGraph->GetXaxis()->SetBinLabel(fiPoint,Form("%d",fRun->fRunNumber));*/
            fiPoint++;
         }
      }

      ClassDef(InSANERunAsymmetry, 1)
};


/**
 */
class InSANERunAsymmetryGroup : public InSANERunAsymmetry {
   public:
      Int_t fNumber;
      InSANERunAsymmetryGroup() {
         fNumber = 0;
      }
      virtual ~InSANERunAsymmetryGroup() { }

      virtual Double_t GetQuantity() {
         SANEMeasuredAsymmetry* asym = nullptr;
         if (fRun) {
            asym = (SANEMeasuredAsymmetry*)fRun->fAsymmetries.FindObject(Form("asym-%d",fNumber));
            if (asym) {
               fValue = asym->GetAsymmetry(asym->fCount[0],asym->fCount[1],fDilutionFactor)/* / (0.12*fRun->fTargetOfflinePolarization / 100.0 * fRun->fBeamPolarization / 100.0)*/;
            }
         }
         return(fValue);
      }

      virtual Double_t GetQuantityError() {
         SANEMeasuredAsymmetry* asym = nullptr;
         if (fRun) {
            asym = (SANEMeasuredAsymmetry*)fRun->fAsymmetries.FindObject(Form("asym-%d",fNumber));
            if (asym) {
               fValue = asym->GetAsymmetry(asym->fCount[0],asym->fCount[1],fDilutionFactor) / (0.12*fRun->fTargetOfflinePolarization / 100.0 * fRun->fBeamPolarization / 100.0);
            }
         }
         if(!asym) return(0);
         Double_t N1, N2, Pb, Pt, err;
         Pt = fRun->fTargetOfflinePolarization / 100.0;
         Pb = fRun->fBeamPolarization / 100.0;
         N1 = (Double_t) asym->fCount[0] ;
         N2 = (Double_t) asym->fCount[1] ;
         /*      if(fRun)fValue = fRun->fTotalAsymmetry; */
         err = TMath::Sqrt(((0.0025000000000000005 * TMath::Power(N1, 3) -
                     0.0025000000000000005 * TMath::Power(N1, 2) * N2 -
                     0.0025000000000000005 * N1 * TMath::Power(N2, 2) +
                     0.0025000000000000005 * TMath::Power(N2, 3)) * TMath::Power(Pb, 2) +
                  (0.0025000000000000005 * TMath::Power(N1, 3) -
                   0.0025000000000000005 * TMath::Power(N1, 2) * N2 +
                   0.0025000000000000005 * TMath::Power(N2, 3) +
                   N1 * N2 * (-0.0025000000000000005 * N2 + 4.*TMath::Power(Pb, 2))) *
                  TMath::Power(Pt, 2)) / (TMath::Power(N1 + N2, 3) * TMath::Power(Pb, 4) * TMath::Power(Pt, 4))) ;
         return(err/0.12);
      }

      virtual Bool_t IsValidRun() {
         Double_t Pb, Pt;
         Pt = fRun->fTargetOfflinePolarization / 100.0;
         Pb = fRun->fBeamPolarization / 100.0;
         if (Pb*Pt == 0.0) return false;
         return true;
      }

      ClassDef(InSANERunAsymmetryGroup, 1)
};


/** Run Series for Pb*Pt
 */
// class InSANERunTargBotNegPol : public InSANERunTargOnlinePol {
// public:
//    InSANERunTargBotNegPol(){}
//    ~InSANERunTargBotNegPol(){}
//    virtual Bool_t IsValidRun(){
//       if(fRun)
//       if(fRun->fTargetCup == InSANERun::kBOTTOM )
//       if(fRun->fTargetPolarizationSign == InSANERun::kNEGATIVE )
//       if( GetQuantity() > -1.0 && GetQuantity() < 1.0) {
//          return true ;
//       }
//       return false;
//    }
// ClassDef(InSANERunTargBotNegPol,1)
// };

/** Creates the plots vs Run Number.
 * Asymmetry vs run number
 * Adds graphs to TMultiGraph object for plotting
 */
class InSANERunSeries : public TObject {
   protected:
      TSortedList fRunList;
      TList       fRunQuatities;
      Int_t       fNumberOfRuns;
      Int_t       fNumberOfQuantities;

   public:
      InSANERunSeries(const char * legtext = " ");
      ~InSANERunSeries() {  }

      Int_t fMarkerStyle;
      Int_t fMarkerColor;
      TString fLegendName;
      TLegend * fLegend;
      TMultiGraph fMultiGraph;

      /** When all the runs and quantities to be analyzed have been added to
       *  the series, GenerateGraphs() returns a list of graphs of each quantity
       */
      TList * GenerateGraphs();

      TLegend       * GetLegend() const {return fLegend;}
      TMultiGraph   * GetMultiGraph() {return(&fMultiGraph);}

      void AddRunQuantity(InSANERunQuantity * q) {
         if (q) fRunQuatities.Add(q);
      }

      /** Extracts all data from run necessary for plotting.
       */
      Int_t AddRun(InSANERun * aRun);


      /** Draw the run objects.
       */
      Int_t Draw();

      ClassDef(InSANERunSeries, 1)
};
//___________________________________________________________________//



#endif

