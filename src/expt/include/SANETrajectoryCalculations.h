#ifndef SANETrajectoryCalculations_H
#define SANETrajectoryCalculations_H 1

#include "TVector3.h"
#include "TClonesArray.h"
#include "BETACalculation.h"

#include "InSANEHitPosition.h"
#include "InSANEReconstructedEvent.h"
#include "InSANEDISTrajectory.h"
#include "BIGCALCluster.h"
#include "LuciteHodoscopePositionHit.h"
#include "LuciteHodoscopeEvent.h"
#include "GasCherenkovEvent.h"

/** Calculates the trajectories and fills an InSANEReconstructedEvent.
 *
 *  For each cluster it does the following:
 *   - The cluster seeds a new trajectory (so that  numberOfTrajectories = numberOfGoodClusters )
 *   - Set the cherenkov, momentum vector, bigcal position, etc... for the trajectory
 *   - For each LuciteHodoscopePositionHit (in fLucitePositionHits):
 *     - Add a position-hit and position to their arrays if the "miss distance" is not too big
 *
 * \ingroup Calculations
 */
class SANETrajectoryCalculation1 : public BETACalculationWithClusters {

   public:
      InSANEReconstructedEvent   * fReconEvent;

   public:
      SANETrajectoryCalculation1(InSANEAnalysis * analysis); 
      virtual ~SANETrajectoryCalculation1();

      virtual void  Describe();
      virtual Int_t Calculate();
      virtual Int_t Initialize();
      virtual void  MakePlots(){}

      ClassDef(SANETrajectoryCalculation1, 1)
};



/** Calculates the trajectories using tracking only and fills an InSANEReconstructedEvent.
 *  - No Shower
 *
 *   \deprecated Do not use
 *  
 * \ingroup Calculations
 */
class SANETrajectoryCalculation2 : public BETACalculationWithClusters {

   public:
      InSANEReconstructedEvent   * fReconEvent;
   public:
      SANETrajectoryCalculation2(InSANEAnalysis * analysis);
      virtual ~SANETrajectoryCalculation2();

      void Describe();

      Int_t Calculate();

      /** Initialize ... Tree
       * Don't forget to call previous initialize
       */
      virtual Int_t Initialize();

      void MakePlots() {
      }

      ClassDef(SANETrajectoryCalculation2, 1)
};


/** Calculates the trajectories using tracking only and fills an InSANEReconstructedEvent.
 *  - No Shower
 *  - No correlation between tracker and lucite
 *  - Faster and less redundant than SANETrajectoryCalculation2
 *
 * \ingroup Calculations
 */
class SANETrajectoryCalculation3 : public BETACalculationWithClusters {

   protected:
      TVector3    fLucPos;
      TVector3    fSeedPos;
      TVector3    fBigcalPos;
      TVector3    fTrackerPos;
      TVector3    fTrackerSeed;
      TVector3    fTargetPos;

      InSANETrajectory           * aTrajectory;
      LuciteHodoscopePositionHit * aLucPositionHit;
      BIGCALCluster              * aCluster;
      ForwardTrackerPositionHit  * aTrackerPosHit;
      InSANEHitPosition          * aPos;
      InSANEHitPosition          * bPos;
      InSANEHitPosition          * tPos;
      Double_t                     missDist;

   public:
      InSANEReconstructedEvent   * fReconEvent;

      Double_t fMaxLuciteMiss;  ///< Max allowed miss for lucite hits (cm)
      Double_t fMaxTrackerMiss; ///< Max allowed miss for tracker hits (cm)

   public:
      SANETrajectoryCalculation3(InSANEAnalysis * analysis);
      virtual ~SANETrajectoryCalculation3();

      void  Describe();
      Int_t Calculate();

      /** Initialize. Don't forget to call previous initialize */
      virtual Int_t Initialize();

      void MakePlots() { }

      ClassDef(SANETrajectoryCalculation3, 1)
};



/** Calculates the trajectories using tracking only and fills an InSANEReconstructedEvent.
 *  -
 *   \deprecated Do not use
 * 
 * \ingroup Calculations
 */
class SANETrajectoryCalculation4 : public BETACalculationWithClusters {
   public:
      InSANEReconstructedEvent   * fReconEvent;
   public:
      SANETrajectoryCalculation4(InSANEAnalysis * analysis): BETACalculationWithClusters(analysis)  {
         SetNameTitle("SANETrajectoryCalculation4", "SANETrajectoryCalculation4");
         fReconEvent = new InSANEReconstructedEvent();
      }

      ~SANETrajectoryCalculation4() {
         if (fOutTree)fOutTree->FlushBaskets();
         if (fOutTree)fOutTree->Write();
      }

      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  SANETrajectoryCalculation4 - \n ";
         std::cout <<  "       \n";
         std::cout <<  "      \n";
         std::cout <<  "       \n";
         std::cout <<  "       \n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

      Int_t Calculate();

      /** Initialize ... Tree
       * Don't forget to call previous initialize
       */
      virtual Int_t Initialize() {
         BETACalculationWithClusters::Initialize();

         InSANERunManager::GetRunManager()->GetCurrentFile()->cd();

         fOutTree = new TTree("luciteOnlyTracking", "BETA Tracking Detectors - No Shower");
         fOutTree->Branch("luciteBigcalPositions", "InSANEReconstructedEvent", &fReconEvent);
         fOutTree->BranchRef();

         return(0);
      }


      /**
       *
       */
      void MakePlots() {
      }

      ClassDef(SANETrajectoryCalculation4, 1)
};




#endif

