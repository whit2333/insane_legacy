#ifdef __CINT__
/**
 *  @ingroup Cuts
*/

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class InSANECut+;
#pragma link C++ class InSANECutter+;

#pragma link C++ class InSANESlowSkimmer+;
#pragma link C++ class InSANESkimmerFilter+;
#pragma link C++ class InSANEBeamCurrentFilter+;

#pragma link C++ class InSANECorrection+;
#pragma link C++ class InSANECalculation+;

#pragma link C++ class BETACorrection+;
#pragma link C++ class BETACalculation+;
#pragma link C++ class BETAHistogramCalc000+;
#pragma link C++ class BETAHistogramCalc0+;
#pragma link C++ class BETAHistogramCalc1+;
#pragma link C++ class BETAEventCounterCorrection+;

#pragma link C++ class BETAPedestalCalculation+;

#pragma link C++ class BETAPedestalCorrection+;
#pragma link C++ class BETATimingCorrection+;
#pragma link C++ class BETATimeWalkCorrection+;

#pragma link C++ class HallCBeamCalculation0+;


#pragma link C++ class BigcalCalculation1+;
#pragma link C++ class BETACalculationWithClusters+;

#pragma link C++ class GasCherenkovCalculation1+;
#pragma link C++ class GasCherenkovCalculation2+;
#pragma link C++ class GasCherenkovCalculation3+;
#pragma link C++ class GasCherenkovCalculation4+;

#pragma link C++ class LuciteHodoscopeCalculation1+;
#pragma link C++ class LuciteHodoscopeCalculation2+;
#pragma link C++ class LuciteHodoscopeCalculation3+;
#pragma link C++ class LuciteHodoscopeCalculation4+;

#pragma link C++ class ForwardTrackerCalculation1+;
#pragma link C++ class ForwardTrackerCalculation2+;



#pragma link C++ class BigcalClusteringCalculation1+;
#pragma link C++ class BigcalPi0Calculations+;
#pragma link C++ class Bigcal3ClusterPi0Calculation+;
#pragma link C++ class BigcalOverlapCalculation+;

#pragma link C++ class Pi0Calibration+;

#pragma link C++ class SANETrajectoryCalculation1+;
#pragma link C++ class SANETrajectoryCalculation2+;
#pragma link C++ class SANETrajectoryCalculation3+;
#pragma link C++ class SANETrajectoryCalculation4+;

#pragma link C++ class SANEElectronSelector+;
#pragma link C++ class SANEElectronSelector2+;
#pragma link C++ class SANEElasticElectronSelector+;
#pragma link C++ class SANEAsymmetryCalculation1+;
// #pragma link C++ class SANEAsymmetryCalculation2+;

#pragma link C++ class SANETrackCalculation1+;
#pragma link C++ class SANETrackCalculation2+;

//#pragma link C++ class InSANECorrector+;
#pragma link C++ class InSANEAnalyzer+;

#pragma link C++ class SANEZerothPassAnalyzer+;
#pragma link C++ class SANEFirstPassAnalyzer+;
#pragma link C++ class SANESecondPassAnalyzer+;
#pragma link C++ class SANEPi0PassAnalyzer+;

// #pragma link C++ class InSANETrajectoryAnalysis+;
// #pragma link C++ class SANETrajectoryAnalyzer+;

#pragma link C++ class SANEScalerAnalyzer+;
#pragma link C++ class GasCherenkovLEDAnalyzer+;

#pragma link C++ class BigcalClusterAnalyzer+;
#pragma link C++ class BETAGeneralAnalyzer+;

//#pragma link C++ class SANEFinalAnalyzer+;

#pragma link C++ class InSANEScalerCalculation+;
#pragma link C++ class BETADeadTimeCalculation+;
#pragma link C++ class SANEScalerCalculation+;

#pragma link C++ class InSANERunFlag+;




#endif

