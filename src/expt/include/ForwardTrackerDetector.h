#ifndef ForwardTrackerDetector_h
#define ForwardTrackerDetector_h 1

#include <iostream>
#include <vector>
#include "TROOT.h"
#include "TClonesArray.h"
#include "InSANEDetector.h"
#include "ForwardTrackerHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerPositionHit.h"
#include "ForwardTrackerGeometryCalculator.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "InSANEDetectorComponent.h"
#include "InSANERunManager.h"

/** Forward Tracker Scintillator Component.
 *
 */
class ForwardTrackerScintillator : public InSANEDetectorComponent {
   public:
      ForwardTrackerScintillator(Int_t num = 0) {
         SetNumber(num);
         SetNameTitle(Form("trackerScint%d", num), Form("Forward Tracker Scintillator %d", num));
      }
      virtual ~ForwardTrackerScintillator() {}

      ClassDef(ForwardTrackerScintillator, 1)
};


/** Concrete detector class for the forward tracker
 *
 *  \ingroup tracker
 */
class ForwardTrackerDetector : public InSANEDetector {

   protected:
      Int_t fCurrentRunNumber;

   public:

      std::vector<Int_t> * fLayer1PositionCounts[128];
      std::vector<Int_t> * fLayer2PositionCounts[128];

      TH2F * fEventHitsY1 ;
      TH2F * fEventHitsY2 ;
      TH2F * fEventHitsX ;

      Int_t cTDCMin;
      Int_t cTDCMax;
      Int_t cTDCAlignMin;
      Int_t cTDCAlignMax;



   public :
      ForwardTrackerDetector(Int_t runnum = 0);
      virtual ~ForwardTrackerDetector();

      Int_t SetRun(Int_t runnum);

      Int_t CorrectEvent(); /** \deprecated */

      virtual Int_t Build();

      virtual void Initialize() {
         if (!IsBuilt()) Build();
      }

      /**  Prepares the event data holders.
       *   This is called by InSANEAnalyzer and InSANECorrector when it as been
       *   added through their methods AddDetector(InSANEDetector*).
       */
      virtual Int_t ProcessEvent();

      virtual Int_t ClearEvent();

      ForwardTrackerGeometryCalculator * fGeoCalc;//!

      /** \name Geometry Related Memebe Functions
       *
       *  @{
       */

      //   Double_t GetDistanceFromTarget() const {
      //       if(fGeoCalc) return(fGeoCalc->GetDistanceFromTarget());
      //       /* else */
      //       Warning("ForwardTrackerDetectors::GetDistanceFromTarget()","No fGeoCalc found! Using hard coded number.");
      //       return(55.0);
      //    }

      /** Returns the x position in BETA coordinates.
       *  If hit is not an "X1" layer, zero is returned.
       */
      Double_t GetXPosition(ForwardTrackerHit * aHit) const {
         if (aHit->fScintLayer == 0) return(fGeoCalc->GetX1Position(aHit->fRow));
         return(0.0);
         /* else */
         //Warning("ForwardTrackerDetectors::GetYPosition(ForwardTrackerHit * aHit)", "Argumetn aHit is not a Y-plane hit.");
         //return(-30.0);
      }

      /** Returns the y position in BETA coordinates 
       *  If hit is neither a Y1 or Y2 layer, zero is returned.
       */
      Double_t GetYPosition(ForwardTrackerHit * aHit) const {
         if (aHit->fScintLayer == 1) return(fGeoCalc->GetY1Position(aHit->fRow));
         if (aHit->fScintLayer == 2) return(fGeoCalc->GetY2Position(aHit->fRow));
         return(0.0);
         /* else */
         //Warning("ForwardTrackerDetectors::GetYPosition(ForwardTrackerHit * aHit)", "Argumetn aHit is not a Y-plane hit.");
         //return(-30.0);
      }

      /** Returns the z position of the Layer in BETA coords (where z=0 is at target)
       */
      Double_t GetZPosition(ForwardTrackerHit * aHit) const {
         return( fGeoCalc->GetZPosition(aHit->fScintLayer,aHit->fRow) );
      }

      /** Returns the position in human coordinates using the position hit */
      TVector3 GetPosition(ForwardTrackerPositionHit * hit) const {
         TVector3 pos(hit->fPositionVector);
         pos.RotateY(40.0 * TMath::Pi() / 180.0);
         return(pos);
      }

      Double_t GetMissDistance(TVector3 v1, TVector3 v2) const {
         TVector3 res(v1 - v2);
         return(res.Mag());
      }

      /** Returns the point of intersection for a line from the seeding cluster
       *  to the origin in the hodoscope plane.
       */
      TVector3 GetSeedingHitPosition(TVector3 seedvec,Int_t layer = 1) const {
         seedvec.RotateY(-40.0 * TMath::Pi() / 180.0); // Rotate to Bigcal Coords

         //Double_t denom = TMath::Cos(TMath::Pi()*40.0/180.0)*TMath::Cos(seedvec.Phi())*TMath::Sin(seedvec.Theta())
         //                 + TMath::Cos(TMath::Pi()*40.0/180.0)*TMath::Cos(seedvec.Theta());
         //Double_t R = fGeoCalc->GetDistanceFromTarget()/(denom);

         Double_t R = fGeoCalc->GetZPosition(layer) / (TMath::Cos(seedvec.Theta()));
         TVector3 result(0, 0, 1);
         result.SetMagThetaPhi(R, seedvec.Theta(), seedvec.Phi());
         result.RotateY(40.0 * TMath::Pi() / 180.0);
         return(result);
      }

      //@}

      Int_t ProcessPositionHits();

      ClassDef(ForwardTrackerDetector, 1)
};


#endif

