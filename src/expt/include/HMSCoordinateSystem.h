#ifndef HMSCoordinateSystem_h
#define HMSCoordinateSystem_h

#include <TROOT.h>
#include <TFile.h>
#include <TObject.h>
#include "InSANECoordinateSystem.h"


/** HMS coordinate system.
 *
 * HMS Coordinate system has x pointing down and y is pointing towards BETA (away from HMS).
 * z is along the beam pipe
 *
 *  Defining an absolute coordinate system sounds like a bad idea.
 *  so this Abstract Base Class provides a way of describing the coordinate
 *  system (in words printed to screen) and transformation matricies from one relative to another.
 *  Thus by defining two coordinate systems, we gain usefulness.
 *
 * \note you need not know about all other coordinate systems, just a path to get to the relevent ones.
 *
 * \ingroup Coordinates
 */
class HMSCoordinateSystem  : public InSANECoordinateSystem {
public :
   HMSCoordinateSystem(const char * name = "hmsCoords"):
      InSANECoordinateSystem(name) {

   }
   ~HMSCoordinateSystem() {}

   TRotation & GetRotationMatrixTo(const char * system = "humanCoords") {
      if (!strcmp(system, "Human") ||
          !strcmp(system, "HUMAN") ||
          !strcmp(system, "human") ||
          !strcmp(system, "HumanCoords") ||
          !strcmp(system, "humanCoords")) {
         fRotationMatrix.SetToIdentity();
         fRotationMatrix.RotateZ(1.0 * TMath::Pi() / 2.0);
      } else if (!strcmp(system, "Hms") ||
                 !strcmp(system, "HMS") ||
                 !strcmp(system, "hms") ||
                 !strcmp(system, "hmsInSANECoordinateSystem")) {
         fRotationMatrix.SetToIdentity();
      } else {
         std::cout << " Unknown coordinate system, " << system << "! Returning identity matrix.\n";
         fRotationMatrix.SetToIdentity();
      }
      return(fRotationMatrix);
   }

   void DescribeCoordinateSystem() {
      std::cout << " == HMS Coordinate System == \n";
      std::cout << "Origin is at the Hall C nominal target position.\n";
      std::cout << "x is \"down\" and y is towards BETA\n";
   }

   TVector3 & GetOriginTranslationTo(const char * system  = "humanCoords") {
      return(fTranslation);
   }


private :

   ClassDef(HMSCoordinateSystem, 1)
};


#endif
