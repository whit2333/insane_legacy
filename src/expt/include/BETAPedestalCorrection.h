#ifndef BETAPedestalCorrection_HH
#define BETAPedestalCorrection_HH 1
#include "TObject.h"
#include "TNamed.h"
#include "BETACorrection.h"
#include "SANEEvents.h"
#include "InSANEDatabaseManager.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"
#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"

/**  Concrete correction class for pedestal subtraction among other things
 *
 *  \ingroup Calculations
 *  \ingroup BETA
 */
class BETAPedestalCorrection : public BETACorrection {

   public:
      BETAPedestalCorrection(InSANEAnalysis * analysis = nullptr) : BETACorrection(analysis) {
         SetNameTitle("BETAPedestalCorrection", "BETAPedestalCorrection");
      }

      virtual ~BETAPedestalCorrection() {
         ;
      }

      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  BETAPedestalCorrections - \n ";
         std::cout <<
            "     Corrects the the ADC values for the BETA detectors.\n" <<
            "     BETAPedestalCorrection assumes you have run BETAPedestalAnalysis\n" <<
            "     somewhere for the run and that the saved InSANEDetectorPedestals's\n" <<
            "     are correct, i.e., the fits are good and the peaks are appropriately\n" <<
            "     located.\n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

      virtual void DefineCorrection() ;

      virtual Int_t Apply() ;

      virtual  Int_t Initialize() {
         std::cout << " o BETAPedestalCorrection::Initialize() \n";
         this->BETACorrection::Initialize();
         return(0);
      }

      ClassDef(BETAPedestalCorrection, 2)
};


#endif


