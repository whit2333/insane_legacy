#ifndef InSANEEventDisplay_HH
#define InSANEEventDisplay_HH
#include <iostream>
#include <vector>
#include "TCanvas.h"
#include "TObject.h"
#include "TPaveLabel.h"
#include "TEllipse.h"
#include "TBox.h"
#include "TPave.h"
#include "TPolyLine.h"
#include "TLine.h"
#include "TArrow.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TObjArray.h"
#include "InSANERun.h"
#include "TPaveText.h"
#include "InSANEEvent.h"
#include "InSANETriggerEvent.h"


/** Simple event datum class
 *
 *  Used for event display
 */
class InSANEEventDatum : public TObject {
public:
   InSANEEventDatum(const char * name = "none", InSANEEvent * addr = nullptr, Int_t cat = 0) {
      fName = name;
      fEvent = addr;
      fCategory = cat;
   };
   ~InSANEEventDatum() {  };
   TString fName;
   InSANEEvent * fEvent;
   Int_t fCategory;
   ClassDef(InSANEEventDatum, 1)
};



/**  Event display (graf)
 */
class InSANEEventDisplay : public TObject {

public:
   /** default c'tor
     *  \deprecated Don't use this
     */
   InSANEEventDisplay();


   /** C'tor creates a canvas which plots
    *  all the histograms in the list provided
    *  Optionally, nRows sets the number of rows the canvas is divided into
    */
   InSANEEventDisplay(TList * hlist, Int_t nRows = 1) {
      fWaitPrimitive = 0;  //
      fUpdateTriggerBit = 3; // b2trig
      fEventType = 5;      //b2trig,b1trig, and pi0trig
      fTriggerEvent = nullptr;
      fHistograms = nullptr;
      if (hlist) {
         fHistograms = hlist;
         fCanvas = new TCanvas("EventDisplay", "InSANE Event Display");
         fCanvas->Divide(fHistograms->GetEntries(), nRows);
      }
   }

   void UpdateSimpleDisplay() {
      if (fTriggerEvent) if (fTriggerEvent->fTriggerBits[fUpdateTriggerBit] /*&& fTriggerEvent->fCodaType == fEventType*/) {
            if (fCanvas && fHistograms) {
               /*      std::cout << "fCodaType  is " << fTriggerEvent->fCodaType << " \n";*/
               for (int i = 1; i <= fHistograms->GetEntries(); i++) {
                  fCanvas->cd(i);
                  fHistograms->At(i - 1)->Draw();
               }
               fCanvas->Update();
               if (fWaitPrimitive) gPad->WaitPrimitive();
            }
         }
   }


   /** d'tor */
   virtual ~InSANEEventDisplay();

   /** Initilize Event display with an InSANERun object */
   virtual Int_t Initialize(InSANERun * aRun = nullptr);

   /** Called from InSANEAnalyzer */
   virtual void UpdateDisplay();

   /** BETA Display*/
   virtual void BuildBETA();

   /** EVE Display*/
   virtual void OpenEveDisplay();

   /** Add event quantity */
   void AddDisplayDatum(const char * name, InSANEEvent * addr, Int_t cat) {
      fTextDisplay->push_back(new InSANEEventDatum(name, addr, cat));
   };

   void GenerateWebEvents(Int_t startEvent, Int_t numberOfEvents);

   /** Set update trigger bit */
   void SetTriggerBit(Int_t bit) {
      fUpdateTriggerBit = bit ;
   }

   /** Get update trigger bit */
   Int_t GetTriggerBit() {
      return(fUpdateTriggerBit);
   }

   /** Set update trigger bit */
   void SetTriggerEvent(InSANETriggerEvent * ev) {
      fTriggerEvent = ev;
   }

   /** Get update trigger bit */
   InSANETriggerEvent* GetTriggerEvent() {
      return(fTriggerEvent);
   }

/// vector holding InSANEEventDatum for display.
   std::vector<InSANEEventDatum*> * fTextDisplay;

/// InSANERun objects
   InSANERun * fRun;

   TList * fHistograms;

/// Object array containing DATA histograms to be displayed
   TObjArray * fDataHists;

/// Object array containing Simulation histograms to be displayed
   TObjArray * fSimulationHists;

/// Canvas being used.
   TCanvas * fCanvas;

/// If set to non zero, then a wait occurs between updates
   Int_t fWaitPrimitive;

/// If set to non zero, then jpg web display images are saved
   Int_t fWebDisplay;

/// parameters for gui display
   Double_t botx, boty, canvasWidth, canvasHeight;

/// Gui objects

   TPaveLabel * pel;
   TPolyLine *cherenkov ;
   TBox * trackerbox;
   TBox * bigcalbox;
   TBox * lucitebox;
   TEllipse * targ ;
   TLine * beam;
   TArrow * targPara ;
   TArrow * targPerp;
   TPaveText * fEventSummary;



protected :

//   static InSANEEventDisplay * fgInSANEEventDisplay;

/// Trigger bit which determines whether or not to update canvas
   Int_t fUpdateTriggerBit;

/// Pointer to array InSANETriggerEvent::fTriggerBits
   Int_t fEventType;

/// Pointer to InSANETriggerEvent
   InSANETriggerEvent * fTriggerEvent;


//  static InSANEEventDisplay * fgInSANEEventDisplay;


   ClassDef(InSANEEventDisplay, 3)
};



#endif

