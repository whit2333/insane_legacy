#ifndef HMSElasticEvent_HH
#define HMSElasticEvent_HH 1

#include "InSANEPhysicsEvent.h"
#include "TMath.h"

/**
 *
 */
class HMSElasticEvent : public InSANEPhysicsEvent {
   public:
      HMSElasticEvent() {
         fE0 = 4.7;
      }

      /** d'tor */
      ~HMSElasticEvent() {

      }

      /** Calculates the electron energy from the Scattered proton angle
       *  fE0 + M - (M*(Power(fE0 + M,2) +
       (Power(fE0,2) - Power(Me,2))*Power(Cos(theta),2)))/
       (Power(fE0 + M,2) + (-Power(fE0,2) + Power(Me,2))*
       Power(Cos(theta),2)) */
      Double_t CalculateElectronEnergy(Double_t theta) {
         Double_t Me = 0.510998;
         Double_t M = 938.272;
         Double_t result = fE0 + M - (M * (TMath::Power(fE0 + M, 2) +
                  (TMath::Power(fE0, 2) - TMath::Power(Me, 2)) * TMath::Power(TMath::Cos(theta), 2))) /
            (TMath::Power(fE0 + M, 2) + (-TMath::Power(fE0, 2) + TMath::Power(Me, 2)) *
             TMath::Power(TMath::Cos(theta), 2)) ;
         return(result);
      };

      Double_t fE0;

      ClassDef(HMSElasticEvent, 1)
};

#endif
