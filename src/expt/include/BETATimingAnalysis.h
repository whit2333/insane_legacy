#ifndef BETATimingAnalysis_H
#define BETATimingAnalysis_H
#include "TROOT.h"
#include "InSANEAnalysis.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "InSANEDatabaseManager.h"
#include "InSANEDetectorTiming.h"
#include "InSANEDetector.h"
#include "SANEEvents.h"
#include "TH1F.h"
#include "TClonesArray.h"
#include "BIGCALGeometryCalculator.h"
#include "TH2F.h"
#include "TSpectrum.h"
#include "TProfile.h"
#include <vector>


/** Analysis of the raw TDC spectra from the detectors in BETA
 *
 *  Concrete class implementation of InSANEAnalysis
 *  Looks at the first 1000 events, which are pulser generated
 *  pedestal events.
 *
 * \deprecated
 * 
 * \ingroup Analyses
 *  \ingroup BETA
 */
class BETATimingAnalysis : public InSANEAnalysis {
public :

   /** Constructor setting the run number.
    */
   BETATimingAnalysis(const char * sourceTreeName = "betaDetectors");

   ~BETATimingAnalysis();

   /**  Run the pedestal analysis for the run. */
   Int_t AnalyzeRun();

   Int_t Visualize() ;

   Int_t FitHistograms() ;

   Int_t CreatePlots() ;

   Int_t FillDatabase();

   /** temporary ranges */
   Int_t fTDCRangeMin;
   Int_t fTDCRangeMax;
   Int_t fADCRangeMin;
   Int_t fADCRangeMax;

   TFile * fAnalysisFile;
   TChain * fAnalysisTree;
   TFile * fOutputFile;
   TTree * fOutputTree;
   BIGCALGeometryCalculator * bcgeo;

   SANEEvents * fEvents;

   /// Arrays of histograms
   TClonesArray * fCherenkovTdcHists;
   TClonesArray * fLuciteTdcHists;
   TClonesArray * fLuciteTdcDiffHists;
   TClonesArray * fLuciteTdcSumHists;
   TClonesArray * fTrackerTdcHists;

   TClonesArray * fBigcalTdcHists;
   TClonesArray * fBigcalTimewalkHists;

   TClonesArray * fBigcal32TdcHists;
   TClonesArray * fBigcal32TimewalkHists;

   TH1F * cer_tdc_hist[12];
   TH1F * cer_tdc_hist2[12];
   TH1F * cer_adc_hist[12];
   TH2F * cer_tdc_vs_adc_hist[12];

   std::vector<Int_t> * fLuciteTDCHitsPerPMT[56];

   /// Arrays of pedestals
   TClonesArray * fCherenkovTiming;
   TClonesArray * fLuciteTiming;
   TClonesArray * fLuciteDiffTiming;
   TClonesArray * fLuciteSumTiming;

   TClonesArray * fTrackerTiming;
   TClonesArray * fBigcalTiming;
   TClonesArray * fBigcalTriggerTiming;

   InSANEDetectorTiming * fTdcPeaks;

   TSpectrum * fSpectrumAnalyzer;

   ClassDef(BETATimingAnalysis, 1)
};


#endif

