#ifndef InSANEBeamlineCoordinateSystem_h
#define InSANEBeamlineCoordinateSystem_h

#include <TROOT.h>
#include <TFile.h>
#include <TObject.h>
#include "InSANECoordinateSystem.h"

/** Beam Position Monitor 1 coordinate system.
 *
 *  z position of BPM1 is -3.070m ( from Dec 2008 survey )
 *
 * \ingroup Coordinates
 */
class BPM1CoordinateSystem  : public InSANECoordinateSystem {
public :
   BPM1CoordinateSystem(const char * name = "BPM1"):
      InSANECoordinateSystem(name) {
   }

   ~BPM1CoordinateSystem() {
   }

   TRotation & GetRotationMatrixTo(const char * system = "humanCoords") {
      if (!strcmp(system, "Human") ||
          !strcmp(system, "HUMAN") ||
          !strcmp(system, "human") ||
          !strcmp(system, "HumanCoords") ||
          !strcmp(system, "humanCoords")) {
         fRotationMatrix.SetToIdentity();
      } else if (!strcmp(system, "Hms") ||
                 !strcmp(system, "HMS") ||
                 !strcmp(system, "hms") ||
                 !strcmp(system, "hmsInSANECoordinateSystem")) {
         fRotationMatrix.SetToIdentity();
         fRotationMatrix.RotateZ(-1.0 * TMath::Pi() / 2.0);
      } else {
         std::cout << " Unknown coordinate system, " << system << "! Returning identity matrix.\n";
         fRotationMatrix.SetToIdentity();
      }
      return(fRotationMatrix);
   }

   TVector3 & GetOriginTranslationTo(const char * system  = "humanCoords") {
      fTranslation = TVector3(0, 0, -3.070 * 100.0);
      return(fTranslation);
   }

   void DescribeCoordinateSystem() {
      std::cout << "===== BPM1CoordinateSystem =====" <<  ". \n";
      std::cout << "Z is along beam direction, y is \"up\" (vertical), thus, x is beam left\n; " ;

   }

   void DrawCoordinateSystems();


private :

   ClassDef(BPM1CoordinateSystem, 1)
};

/** Beam Position Monitor 1 coordinate system.
 *
 *  z position of BPM1 is -1.308m ( from Dec 2008 survey )
 *
 * \ingroup Coordinates
 */
class BPM2CoordinateSystem  : public InSANECoordinateSystem {
public :
   BPM2CoordinateSystem(const char * name = "BPM2"):
      InSANECoordinateSystem(name) {
   }

   ~BPM2CoordinateSystem() {
   }

   TRotation & GetRotationMatrixTo(const char * system = "humanCoords") {
      if (!strcmp(system, "Human") ||
          !strcmp(system, "HUMAN") ||
          !strcmp(system, "human") ||
          !strcmp(system, "HumanCoords") ||
          !strcmp(system, "humanCoords")) {
         fRotationMatrix.SetToIdentity();
      } else if (!strcmp(system, "Hms") ||
                 !strcmp(system, "HMS") ||
                 !strcmp(system, "hms") ||
                 !strcmp(system, "hmsInSANECoordinateSystem")) {
         fRotationMatrix.SetToIdentity();
         fRotationMatrix.RotateZ(-1.0 * TMath::Pi() / 2.0);
      } else {
         std::cout << " Unknown coordinate system, " << system << "! Returning identity matrix.\n";
         fRotationMatrix.SetToIdentity();
      }
      return(fRotationMatrix);
   }

   TVector3 & GetOriginTranslationTo(const char * system  = "humanCoords") {
      fTranslation = TVector3(0, 0, -1.308 * 100.0);
      return(fTranslation);
   }

   void DescribeCoordinateSystem() {
      std::cout << "===== BPM2CoordinateSystem =====" <<  ". \n";
      std::cout << "Z is along beam direction, y is \"up\" (vertical), thus, x is beam left\n; " ;

   }

   void DrawCoordinateSystems();


private :

   ClassDef(BPM2CoordinateSystem, 1)
};


#endif
