#ifndef GasCherenkovDetector_h
#define GasCherenkovDetector_h
#include <iostream>
#include <vector>
#include <TROOT.h>
 //#include <TChain.h>
#include "InSANEDetector.h"
#include <TClonesArray.h>
#include "GasCherenkovHit.h"
#include "InSANECherenkovMirror.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "GasCherenkovGeometryCalculator.h"
#include "BIGCALCluster.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "TH1F.h"
#include "BIGCALCluster.h"

/** Concrete class for a Detector SANE gas cherenkov built by Temple University.
 *
 *  Use with InSANEAnalyzer  for simplified analysis of detector
 *
 *  \ingroup Detectors
 *  \ingroup Cherenkov
 */
class GasCherenkovDetector : public InSANEDetector {

   private :
      Int_t fCurrentRunNumber;

   public :

      Double_t fCherenkovAlignmentCoef[12];       ///< Coeff for aligning the signal peaks
      Double_t fCherenkovSingleElectronWidth[12]; ///< 
      Double_t fCherenkovDoubleElectronWidth[12]; ///< 
      Double_t fCherenkovPE[12];                  ///< Storage of the ADC to photoelectron coefficients

      Double_t f1PECalibration[12];
      Double_t fMirrorGeoCutX[3];
      Double_t fMirrorGeoCutY[5];

      TH2F * fCerHits ;     //!
      TH2F * fCerHitsGood ; //!

      //InSANECherenkovMirror * fMirrors[12];  //!

      TH1F * cer_tdc_hist[12];        //!
      TH1F * cer_adc_hist[12];        //!
      TH2F * cer_tdc_vs_adc_hist[12]; //!

      TH1F * cer_tdc_hist_luc[12*56]; //!
      TH1F * cer_adc_hist_luc[12*56]; //!
      TH2F * cer_tdc_vs_adc_hist_luc[12*56]; //!

      TH1F * luc_tdc_hist[12*56]; //!
      TH1F * luc_adc_hist[12*56]; //!
      TH2F * luc_tdc_vs_adc_hist[12*56]; //!

      TH1F * hADCWithoutCut[12]; //!
      TH1F * hTDCWithoutCut[12]; //!
      TH1F * hADCAlignedWithoutCut[12]; //!
      TH1F * hTDCAlignedWithoutCut[12]; //!

      TH1F * hADCWithTDCCut[12]; //!
      TH1F * hTDCWithTDCCut[12]; //!
      TH1F * hTDCAlignedWithTDCCut[12]; //!
      TH1F * hADCAlignedWithTDCCut[12]; //!

      TH1F * hADCWithADCCut[12]; //!
      TH1F * hTDCWithADCCut[12]; //!
      TH1F * hTDCAlignedWithADCCut[12]; //!
      TH1F * hADCAlignedWithADCCut[12]; //!

      TH1F * hADCWithADCandTDCCut[12]; //!
      TH1F * hTDCWithADCandTDCCut[12]; //!
      TH1F * hADCAlignedWithADCandTDCCut[12]; //!
      TH1F * hTDCAlignedWithADCandTDCCut[12]; //!

      /** @name Storage of values mapped to the physical mirror
       *  @{
       */
      std::vector<Int_t>    * fCherenkovADCs[12];  //!
      std::vector<Double_t> * fCherenkovNPEs[12]; //!
      std::vector<Double_t> * fCherenkovADCsAligned[12]; //!
      std::vector<Int_t>    * fCherenkovTDCs[12]; //!
      std::vector<Int_t>    * fCherenkovTimedTDCs[12]; //!
      std::vector<Double_t> * fCherenkovTDCsAligned[12]; //!
      std::vector<Double_t> * fCherenkovTimedTDCsAligned[12]; //!
      std::vector<Int_t>    * fTimedTDCHitNumbers;  //!
      bool                    fCherenkovHit;  //!
      bool                    fMirrorTDCHit[12]; //!
      //  Double_t fCherenkovADCsAlign[12];
      //@}

      Double_t cADCMin;
      Double_t cADCMax;
      Bool_t   cPassesADC[12]; //!

      Double_t cADCAlignMin;
      Double_t cADCAlignMax;
      Bool_t   cPassesADCAlign[12]; //!

      Double_t cNPEMin;
      Double_t cNPEMax;
      Bool_t   cPassesNPE[12]; //!

      Double_t cTDCMin;
      Double_t cTDCMax;
      Bool_t   cPassesTDC[12]; //!

      Double_t cTDCAlignMin;
      Double_t cTDCAlignMax;
      Bool_t   cPassesTDCAlign[12]; //!

      Double_t cTDCAlignMin2;
      Double_t cTDCAlignMax2;
      Bool_t   cPassesTDCAlign2[12]; //!

      Double_t fNPEThresholdCut;
      Int_t    fADCThresholdCut;
      Double_t fNPELimitCut;


   public :

      /** C'tor requires run number so that it can look up calibration coefficients, and
       *  various other settings.
       */
      GasCherenkovDetector(Int_t runnum = 0);

      virtual ~GasCherenkovDetector();

      virtual Int_t Build() {
         SetBuilt();
         return(0);
      }
      virtual void Initialize() {
         if (!IsBuilt()) Build();
      }

      /**  Sets the run for the detector. Called from constructor.  */
      Int_t SetRun(Int_t runnum);

      /** Clear event in order to process the next event
       *
       * \note The event no longer needs to be cleared from here.
       * This is done within the implementation of InSANEAnalysis::ClearEvents()
       */
      Int_t ClearEvent();

      /** Prepares the event data holders. This is called by InSANEAnalyzer when it as been
       *  added through their methods AddDetector(InSANEDetector*).
       */
      Int_t ProcessEvent();

      static int GetAlignment_callback(void * det, int argc, char **argv, char **azColName) {
         //       int i;
         //       std::cout << "Number of args= " << argc << std::endl;
         //       for(i=0; i<argc; i++)
         //       {
         //          std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
         //       }
         //       std::cout << std::endl;
         auto * a = (GasCherenkovDetector*) det;
         if (argc == 4) { // should equal 2!
            int j = atoi(argv[0]) - 1;
            if (j >= 0 && j < 8) a->fCherenkovAlignmentCoef[j] = atof(argv[1]);
            if (j >= 0 && j < 8) a->fCherenkovSingleElectronWidth[j] = atof(argv[2]);
            if (j >= 0 && j < 8) a->fCherenkovDoubleElectronWidth[j] = atof(argv[3]);
         } else {
            std::cout << " x argc does not equal 4! it is " << argc << "\n";
         }
         return 0;
      }



      /** uses numbering starting at 1 */
      InSANECherenkovMirror * GetMirror(Int_t i) {
         return((InSANECherenkovMirror*)GetComponent(i - 1));
      }

      /** @name CUTS
       *  @{
       */

      /** Print the cuts used in ProcessEvent() */
      void PrintCuts() const ;

      Bool_t PassesCuts(const BIGCALCluster * clust) const {
         if (clust->fGoodCherenkovTDCHit)
            if (clust->fCherenkovBestADCSum > cADCAlignMin && clust->fCherenkovBestADCSum < cADCAlignMax)
               return(true);
         return(false);
      }

      //@}

      /** Returns the sum of all the surrounding mirrors in addition to the indexed mirror.
       *  Mirror number starts at 1
       *  \todo if i = (9,10,11,12) you will get the sum of (all, lower 4, mid 4, top 4) like
       *  the sums formed for the trigger.
       */
      Double_t GetNPESum(int i) {
         GasCherenkovEvent * gcEvent = ((GasCherenkovEvent*)fEvent);
         if (!gcEvent) {
            Error("GetNPESum", "Detector fEvent not set.");
            return(0);
         }
         if (!(gcEvent->fGasCherenkovADCHits)) Error("GetNPESum", "Null fGasCherenkovADCHits");
         if (gcEvent->fGasCherenkovADCHits->GetEntries() != 12) Error("GetNPESum", "Size of fGasCherenkovADCHits is not 12!");
         auto * aHit = (GasCherenkovHit*)((*gcEvent->fGasCherenkovADCHits)[i-1]);
         Double_t sum = aHit->fNPE; /// Add mirror since IsNeighboringMirror does not include itself
         for (int j = 0; j < 8; j++) {
            aHit = (GasCherenkovHit*)(*gcEvent->fGasCherenkovADCHits)[j];
            if (fGeoCalc->IsNeighboringMirror(i, j + 1)) sum += aHit->fNPE;
         }
         return(sum);
      }

   public:


   public:

      /** Prints some basic event information
       */
      void PrintEvent() const ;

      /** @name Useful methods
       *  @{
       */
   public:

      /** Returns true if there exists a neighboring TDC mirror hit that passes TDC cut */
      Bool_t HasNeighboringHit(Int_t mirror);

      //@}

   public :



      /**  Returns 1 photo-electron calibration in units of channels per photoelectron.
       */
      Double_t Get1PECalibration(int mirrorNumber) {
         return(f1PECalibration[mirrorNumber-1]);
      }

      /**
       *  Sets the calibration coefficients \todo currently values are hard coded to 90 ch/p.e.
       */
      void Set1PECalibrations(Int_t runnumber = 72994);

      /** Set ADC alignment calibrations
       *  Grabs the fit results stored in the table cherenkov_performance
       */
      Int_t GetADCAlignmentCalibrations(Int_t runnumber) ;

      /**
       *  not imp
       */
      Int_t CorrectEvent();

      std::vector<Int_t> * fMirrorsToSum; //!
      GasCherenkovGeometryCalculator * fGeoCalc; //!

      /**
       *  @name Post-process-action
       */
      //@{

      /**  Called at the end of InSANEAnalyzer::Process()
       *
       */
      Int_t PostProcessAction();

      //@}


      void InitHistograms();

      ClassDef(GasCherenkovDetector, 2)
};


#endif

