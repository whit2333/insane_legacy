#ifndef LuciteHodoscopeEvent_h
#define LuciteHodoscopeEvent_h

#include "TObject.h"
#include "TClonesArray.h"
#include "LuciteHodoscopeHit.h"
#include "InSANEDetectorEvent.h"
#include "LuciteHodoscopePositionHit.h"
#include <iostream>

/** Lucite Hodoscope Event
 *
 * \ingroup Events
 *
 * \ingroup hodoscope
 */
class LuciteHodoscopeEvent : public InSANEDetectorEvent {

   public :
      LuciteHodoscopeEvent();
      ~LuciteHodoscopeEvent();
      Int_t AllocateHitArray();

      Int_t fNumberOfPositionHits;
      /// Number of ADC channels to be readout for the event
      Int_t   fNumberOfADCHits;
      /// Sum of the the number of tdc hits on each channel
      Int_t   fNumberOfTDCHits;
      /// Number of ADC channels to be readout for the event AND pass timing cut
      Int_t   fNumberOfTimedADCHits;
      /// Sum of the the number of tdc hits on each channel AND pass timing cut
      Int_t   fNumberOfTimedTDCHits;

      /// This pointer is for the auto streaming... \todo { Maybe there is a better way
      /// ? I am not sure about private members and ROOT auto streamers IO? }
      /// should this be const?
      TClonesArray * fLuciteHits; //->
      TClonesArray * fLucitePositionHits; //->

      void ClearEvent(Option_t * opt = "");

      void Print(Option_t * opt = "") const {
         InSANEDetectorEvent::Print();
         std::cout << " + LuciteHodoscopeEvent object " << this << std::endl;
         std::cout << "  fEventNumber = " << fEventNumber << std::endl;
         std::cout << "  fNumberOfADCHits = " << fNumberOfADCHits << std::endl;
         std::cout << "  fNumberOfTDCHits = " << fNumberOfTDCHits << std::endl;
         std::cout << "  fNumberOfTimedADCHits = " << fNumberOfTimedADCHits << std::endl;
         std::cout << "  fNumberOfTimedTDCHits = " << fNumberOfTimedTDCHits << std::endl;
         std::cout << "  fLuciteHits = " << fLuciteHits << std::endl;
         std::cout << "  fPositionHits = " << fLucitePositionHits << std::endl;
         std::cout << "  fgLuciteHodoscopeHits = " << fgLuciteHodoscopeHits << std::endl;
         std::cout << "  fgLuciteHodoscopePositionHits = " << fgLuciteHodoscopePositionHits << std::endl;
      }

   private :
      static TClonesArray *fgLuciteHodoscopeHits; //!
      static TClonesArray *fgLuciteHodoscopePositionHits; //!

      ClassDef(LuciteHodoscopeEvent, 6)
};


#endif

