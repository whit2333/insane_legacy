#ifndef BETAG4SimulationRun_HH
#define BETAG4SimulationRun_HH
#include "InSANERun.h"

/** Concrete class for a simulation run
 *
 * Adds the configuration parameters of the Primary event generator
 */
class BETAG4SimulationRun : public InSANERun {
public:
   BETAG4SimulationRun(int run = 0): InSANERun(run) {
      fEnergyMin = 1.0;
      fEnergyMax = 5.7;
      fThetaMin  = 35.0;
      fThetaMax  = 45.0;
      fPhiMin    = -15.0;
      fPhiMax    = 15.0;
   }

   ~BETAG4SimulationRun() { }

   Double_t fEnergyMin;
   Double_t fEnergyMax;
   Double_t fThetaMin;
   Double_t fThetaMax;
   Double_t fPhiMin;
   Double_t fPhiMax;

   TString fXSection;

   ClassDef(BETAG4SimulationRun, 1)
};



#endif
