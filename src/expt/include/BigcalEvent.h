#ifndef BigcalEvent_h
#define BigcalEvent_h

#include  "BigcalHit.h"
#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include <TClonesArray.h>
#include <TH1F.h>
#include <TH2F.h>
#include "InSANEDetectorEvent.h"



/** Concrete class for a Bigcal event. Contains all ADC and TDC hit data
 *
 * \ingroup Events
 * \ingroup BIGCAL
 */
class BigcalEvent : public InSANEDetectorEvent {

   public :
      BigcalEvent();
      virtual ~BigcalEvent();

      Int_t AllocateHitArray();

      /// Number of ADC channels to be readout for the event
      Int_t   fNumberOfADCHits;

      /// Sum of the number of tdc hits on each block fLevel=1 events in bigcal (sum of 64)
      Int_t   fNumberOfTDCHits;

      /// Number of ADC channels to be readout for the event AND pass timing cut
      Int_t   fNumberOfTimedADCHits;

      /// Sum of the the number of tdc hits on each channel AND pass timing cut
      Int_t   fNumberOfTimedTDCHits;

      /// Number of fLevel=3 events in bigcal (sum of 64)
      Int_t   fNumberOfTriggerGroupHits;

      /// Number of fLevel=2 events in bigcal (sum of 32)
      Int_t   fNumberOfTDCGroupHits;

      /// Total Energy of event
      Double_t fTotalEnergyDeposited ;

      TClonesArray * fBigcalHits; //->  array with all hits
      TClonesArray * fBigcalTimingGroupHits; //->  array with all hits
      TClonesArray * fBigcalTriggerGroupHits; //->  array with all hits

      void ClearEvent(Option_t * opt = "");

   private:
      static   TClonesArray * fgBigcalHits; //!
      static   TClonesArray * fgBigcalTimingGroupHits; //!
      static   TClonesArray * fgBigcalTriggerGroupHits; //!

      ClassDef(BigcalEvent, 4)
};

#endif

