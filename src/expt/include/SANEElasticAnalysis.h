#ifndef SANEElasticAnalysis_H
#define SANEElasticAnalysis_H
#include "TROOT.h"
#include "InSANEDetectorAnalysis.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "InSANEDatabaseManager.h"

#include "SANEEvents.h"

/**  Concrete class implementation of InSANEAnalysis
 *
 * \ingroup Analyses
 */
class SANEElasticAnalysis : public InSANEDetectorAnalysis {
public :
   SANEElasticAnalysis(const char * sourceTreeName = "correctedBetaDetectors");
   ~SANEElasticAnalysis();

   Int_t fRunNumber ;

   Int_t AnalyzeRun();
   Int_t Visualize() ;

   TFile * fAnalysisFile;
   TChain * fAnalysisTree;
   TFile * fOutputFile;
   TTree * fOutputTree;

   SANEEvents * events;

   //InSANEDatabaseManager * dbManager;

   ClassDef(SANEElasticAnalysis, 2);
};

// class SANEElasticAnalyzer : public SANEElasticAnalysis , public InSANEAnalyzer {
// public:
//    SANEElasticAnalyzer(const char * newTreeName="newTrajectoryTree",
//                          const char * uncorrectedTreeName="oldTrajectoryTree")
//   : SANEElasticAnalysis(uncorrectedTreeName) {
//       if(SANERunManager::GetRunManager()->fVerbosity>1) std::cout<< " o SANEElasticAnalyzer Constructor\n";
//       SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//       fInputTree = 0;
//       if(fAnalysisTree) fInputTree = fAnalysisTree; // anlysis tree set in InSANEDetectorAnalysis
//       if(!fInputTree) fInputTree = (TTree*)gROOT->FindObject(uncorrectedTreeName);
//       if(!fInputTree) printf("x- Tree:%s  was NOT FOUND! (from SANEFirstPassAnalyzer c'tor)  \n",uncorrectedTreeName);
//
//       if(!fScalerTree) fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName);
//       fOutputFile = SANERunManager::GetRunManager()->GetCurrentFile();
//    }
//
// ClassDef(SANEElasticAnalyzer,1)
// };


#endif

