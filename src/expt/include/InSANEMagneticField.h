#ifndef InSANEMagneticField_h
#define InSANEMagneticField_h

#include <iostream>
#include "TVirtualMagField.h"
#include "TObject.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TVector3.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRotation.h"
#include "TH1F.h"
#include "TArrow.h"

/** The default field is a uniform field along the z axis.
 *  Note: Units are cm and Tesla.
 *
 * \ingroup emfields
 *
 * \NOTE A note about target rotations: Since we are mostly interested in longitudinal and transverse
 *       rotations, the rotation occurs in the plane created by the z-axis and x-axis.
 *
 */
class InSANEMagneticField  : public TVirtualMagField {

   public:
      /**
       * The polar angle of the field at the point (0,0,0)
       * This is typically used for a polarized target's magnetic field
       */
      Double_t fPolarizationAngle;

      TCanvas * fCanvas;

   protected:
      /**
       * A multiplying coefficient which can be used to make small adjustments
       * to the field AND/OR apply units (e.g. =Tesla for appropriate GEANT4 units)
       */
      Double_t fScalingCoefficient;

      Double_t fMaximumField;

   private:
      TGraph2D * fBxGraph;
      TGraph2D * fByGraph;
      TGraph2D * fBzGraph;

      TGraph * fBrayGraph;

      TGraph2D * fBrGraph;
      TGraph2D * fBphiGraph;
      TGraph2D * fBAbsGraph;
   protected:
      Bool_t    fIsConst;
      Double_t  fMaxFieldMag;
      Bool_t    fIsInitialized;

   public :
      /**  Default Constructor is a Uniform Field in the Z direction
       *   (with a rotation angle of zero degrees)
       */
      InSANEMagneticField(const char * n = "");

      virtual  ~InSANEMagneticField();

      /** Virtual method from TVirtualMagField 
       */
      virtual void Field(const Double_t* x, Double_t* B) {
         Double_t point[4] = {x[0],x[1],x[2],0};
         GetFieldValue(point,B); 
      }

      Bool_t IsInitialized() const {return fIsInitialized;}
      virtual void Initialize() { fIsInitialized = true; }

      virtual Float_t GetMaxFieldMag() const { return fMaxFieldMag; }

      TVector3 GetField(const TVector3& v);

      TVector3 GetField(Double_t x,Double_t y, Double_t z);

      virtual Bool_t IsConst() const {return fIsConst;}

      virtual void PrintField(Double_t x, Double_t y, Double_t z);

      /**  Integrate Bdl along a ray with the given angles.  */
      virtual Double_t IntegrateBdl(Double_t theta, Double_t phi);

      /** Fills the array Bfield[0-2], (xyz) components, given the spacetime point, Point
       * The time coordinate is index (3).
       * This makes easy a GEANT4 implementation of G4MagneticField
       */
      virtual void GetFieldValue(const  Double_t Point[4], Double_t *Bfield) const;

      /** Get the field using TVector3.  */
      virtual void GetFieldWithVector(const  TVector3& point, TVector3& bfield) const;

      /** Polarization angle is in radians.  */
      void SetPolarizationAngle(Double_t angle) { fPolarizationAngle = angle; }
      Double_t  GetPolarizationAngle() const { return fPolarizationAngle ; }

      /** Changes the sign of the field vector at the origin.
       */
      void SwitchPolarization();

      /** Set the scale factor that changes the field as Bnew = scale*Bold, where
       *  the default value is 1.0
       */
      void SetScaleFactor(Double_t scale) {
         fScalingCoefficient = scale;
      }
      Double_t GetScaleFactor() {
         return(fScalingCoefficient);
      }

   public:

      void PlotFieldAlongRay(Double_t theta, Double_t phi, const Int_t n = 21 , Double_t step = 2.0  /*cm*/);

      TVector3 fTestPoint;

      void PrintTestPoint();

      /** For testing the interpolating function*/
      void TestInterpolation();

      /** Returns the 2-d interpolation given the values on the corners of a rectangle
       *  Z -> i, R -> j
       */
      Double_t BilinearInterpolation(Double_t * cornerDataPoints, Double_t * corners, Double_t x, Double_t y) const ;

      /**
       * Creates a plot of the field's radial component (typically \f$ r \f$  or \f$ \rho \f$ )
       * in cylindrical coords
       */
      void LookAtFieldRadialComponent(const Int_t n = 21 , Double_t step = 2.0  /*cm*/);

      /**
       * Creates a plot of the field's Z component
       * in cylindrical coords
       */
      void LookAtFieldZComponent(const Int_t n = 21 , Double_t step = 2.0  /*cm*/);
      void LookAtFieldXComponent(const Int_t n = 21 , Double_t step = 2.0  /*cm*/);
      void LookAtFieldYComponent(const Int_t n = 21 , Double_t step = 2.0  /*cm*/);
      void LookAtField(const Int_t n = 21 , Double_t step = 2.0  /*cm*/);

      /** returns a pointer to the canvas for viewing magnetic field. Creates a new instance if it is the first
       *  plot looked at.
       */
      TCanvas * GetCanvas() {
         if (!fCanvas) {
            fCanvas = new TCanvas(Form("c_%d_deg", (int)(fPolarizationAngle * 180.0 / TMath::Pi())), "Target Magnetic Field", 500, 500);
            fCanvas->Divide(1, 2);
            fCanvas->cd(1)->Divide(2, 1);
            fCanvas->cd(2)->Divide(3, 1);
         }
         return(fCanvas);
      }

      /**
       * Creates a plot of the field's Z component
       * in cylindrical coords
       */
      void LookAtFieldAbsoluteValue(const Int_t n = 21 , Double_t step = 2.0  /*cm*/);

      ClassDef(InSANEMagneticField, 1)
};


#endif
