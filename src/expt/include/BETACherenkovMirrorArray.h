#ifndef BETACherenkovMirrorArray_HH
#define BETACherenkovMirrorArray_HH 1
#include "InSANEDetector.h"
#include "InSANESphericalMirror.h"
#include "InSANEToroidalMirror.h"
#include "GasCherenkovGeometryCalculator.h"
#include "TList.h"
#include "InSANEClusterEvent.h"

/**  Detector for gas cherenkov mirror array projected on bigcal.
 *
 */
class BETACherenkovMirrorArray : public InSANEDetector {
public:
   BETACherenkovMirrorArray() {
      for (int i = 0; i < 4; i++) {
         fToroidalMirrors[i] = new InSANEToroidalMirror(2 + 2 * i);
         fSphericalMirrors[i] = new InSANESphericalMirror(1 + 2 * i);
         fMirrors.Add(fToroidalMirrors[i]);
         fMirrors.Add(fSphericalMirrors[i]);
      }
      fClusterEvent = nullptr;
   }

   ~BETACherenkovMirrorArray() { }

   virtual Int_t Build() {
      return(0);
   }
   virtual void Initialize() {}

   Int_t ClearEvent() {
      return(0);
   }

   /** Process the event
    */
   Int_t ProcessEvent() {
      Int_t result = 0;
      if (fClusterEvent) result = ProcessEvent(fClusterEvent);
      return(result);
   }

   /** called if a cluster event is set */
   Int_t ProcessEvent(InSANEClusterEvent * ev) {
      return(0);
   }

   /** Called at the end of the processing the events
    */
   Int_t PostProcessAction() {
      return(0);
   }

   InSANEClusterEvent * fClusterEvent;
   void SetClusterEvent(InSANEClusterEvent * ev) {
      fClusterEvent = ev;
   }
   InSANEClusterEvent * GetClusterEvent() {
      return(fClusterEvent);
   }


   TList fMirrors;

private:
   InSANEToroidalMirror * fToroidalMirrors[4];
   InSANESphericalMirror * fSphericalMirrors[4];

   ClassDef(BETACherenkovMirrorArray, 1)
};

#endif
