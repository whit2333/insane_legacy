#ifndef BETAScalerEvent_HH
#define BETAScalerEvent_HH 1
#include "InSANEScalerEvent.h"

/** BETA Scaler Event handles the detector scalers
 *
 *  \ingroup BETA
 */
class BETAScalerEvent : public InSANEScalerEvent {
public:
   BETAScalerEvent() {   }
   BETAScalerEvent(InSANEScalerEvent* insaneScalerEvent) {
   }
   ~BETAScalerEvent() {  }

   Double_t * fHodoscopeScalers; //!
   Double_t * fCherenkovScaler; //!
   Double_t * fCherenkovScalerChange; //!
   Double_t * fTrackerScalers; //!
   Double_t * fBigcalScalers; //!
   Double_t * fLuciteScalers; //!


   ClassDef(BETAScalerEvent, 1)
};

#endif

