#ifndef InSANECluster_h
#define InSANECluster_h

#include "TObject.h"
#include "TH2F.h"
#include "TMath.h"

/** ABC for cluster object.
 *
 *  InSANECluster is an ABC for all cluster objects.
 *  These can range from results of sophisticated algorithms
 *  or simple local maxima finders.
 *
 *  \ingroup Clusters
 */
class InSANECluster : public TObject {

   public:
      InSANECluster(Int_t clustNum = -1) ;
      virtual ~InSANECluster();

      InSANECluster& operator=(const InSANECluster &rhs);

      InSANECluster(const InSANECluster& rhs) : TObject(rhs) {
         (*this) = rhs;
      }

   public:
      Int_t     fRunNumber;
      Int_t     fEventNumber;
      Int_t     fClusterNumber;
      Double_t  fXMoment;
      Double_t  fYMoment;
      Double_t  fX2Moment;
      Double_t  fY2Moment;
      Double_t  fX3Moment;
      Double_t  fY3Moment;
      Double_t  fXStdDeviation;
      Double_t  fYStdDeviation;
      Double_t  fXSkewness;
      Double_t  fYSkewness;

      Double_t  fX4Moment;
      Double_t  fY4Moment;
      Double_t  fXKurtosis;
      Double_t  fYKurtosis;

      /**
       * Returns the first moment about the mean (variance)  of the 5x5 cluster using bigcal
       * coordinates. The second moment is \f$ \sum (x_{i} - x_{0}) E_i/E_{cluster}\f$
       */
      Double_t GetXmoment() const {
         return(fXMoment);
      }

      /**
       * Returns the first moment about the mean of the 5x5 cluster using bigcal
       * coordinates. The second moment is  \f$ \sum (y_{i} - y_{0}) E_i/E_{cluster} \f$
       */
      Double_t GetYmoment() const {
         return(fYMoment);
      }
      /**
       * Returns the second moment about the mean of the 5x5 cluster using bigcal
       * coordinates. The second moment is \f$ \sum (x_{i} - x_{0})^2 E_i/E_{cluster}\f$
       */
      Double_t GetXSquaredMoment() const  {
         return(fX2Moment);
      }
      /**
       * Returns the second moment about the mean of the 5x5 cluster using bigcal
       * coordinates. The second moment is  \f$ \sum (y_{i} - y_{0})^2 E_i/E_{cluster}\f$
       */
      Double_t GetYSquaredMoment()  const {
         return(fY2Moment);
      }
      /**
       * Returns the second moment about the mean (also called the standard deviation or square root of the variance) of the 5x5 cluster using bigcal
       * coordinates. The second moment is  \f$ \sigma = \sqrt{ \sigma^2 } =  \sqrt{ \sum (x_{i} - x_{0})^2  E_i/E_{cluster}}\f$
       */
      Double_t GetXStdDeviation() const {
         return(fXStdDeviation);
      }
      /**
       * Returns the second moment about the mean of the 5x5 cluster using bigcal
       * coordinates. The second moment is  \f$ \sigma = \sqrt{ \sigma^2 } =  \sqrt{ \sum (y_{i} - y_{0})^2  E_i/E_{cluster}}\f$
       */
      Double_t GetYStdDeviation() const {
         return(fYStdDeviation);
      }
      /**
       * Returns the second moment about the mean of the 5x5 cluster using bigcal
       * coordinates. The second moment is
       */
      Double_t GetXSkewness() const  {
         return(fXSkewness);
      }
      /**
       * Returns the second moment about the mean of the 5x5 cluster using bigcal
       * coordinates. The second moment is
       */
      Double_t GetYSkewness() const {
         return(fYSkewness);
      }

      virtual void Clear(Option_t *opt = "");

      void Reset() {
         Clear();
      }


      ClassDef(InSANECluster,5)
};

#endif



