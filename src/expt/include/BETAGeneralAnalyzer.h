#ifndef BETAGeneralAnalyzer_HH
#define BETAGeneralAnalyzer_HH 1

#include "InSANEAnalyzer.h"

/** A General analyzer for BETA.
 *
 */
class BETAGeneralAnalyzer : public InSANEAnalyzer {
public:

   /** Argument should be the name of the new tree to create
    */
   BETAGeneralAnalyzer(const char * newTreeName = "betaDetectors000", const char * inputTreeName = "betaDetectors");

   virtual ~BETAGeneralAnalyzer();

   virtual void Initialize(TTree * t = nullptr);

   virtual void MakePlots();

   SANEScalers *   fScalers;
   SANEEvents  *   fEvents;

   ClassDef(BETAGeneralAnalyzer,1)
};

#endif
