#ifndef LocalMaxClusterProcessor_h
#define LocalMaxClusterProcessor_h

#include <iostream>

#include "TROOT.h"
#include "TObject.h"
#include "TTree.h"
#include "TFile.h"
#include "TClonesArray.h"

#include "InSANECalorimeterCluster.h"
#include "InSANECluster.h"
#include "InSANEEvent.h"
#include "BIGCALClusterProcessor.h"

/**  Concrete class makes clusters around the local maxima.
 *
 *  This class finds a limited number of local maxima and
 *  produces clusters around them.
 *
 * \ingroup ClusteringAlgorithms
 */
class LocalMaxClusterProcessor : public BIGCALClusterProcessor {

   public:
      BIGCALGeometryCalculator * fGeoCalc;

      LocalMaxClusterProcessor();

      virtual ~LocalMaxClusterProcessor();

      /** Searches for peaks above the threshold energy fMinimumEnergy
       *
       *  If a peak is found, its peak position is saved and the histogram is zeroed for that 5x5
       *  square.
       *
       *  \todo Try zeroing a 3x3 square to allow for closer clusters to be separated
       */
      virtual Int_t FindPeaks();

      /** Simply uses the number of peaks found with FindPeaks() and only looks at 5x5 square  */
      virtual Int_t Partition() {
         return(fNPeaks);
      }

      /** Returns true if the surrounding blocks do
       *  not have any adc signal.
       *  (x,y) is the block of interest and (m,n) is the location in the 5x5 matrix of that block.
       */
      Bool_t IsDetached(int x, int y, int m, int n) {
         double tempE = 0.0;
         Bool_t result = false;
         //      Bool_t result = true;
         for (int j = -1 ; j < 2 ; j++)
            for (int i = -1 ; i < 2 ; i++)
               if (!(i == m && j == n))
                  if (m + i > -2 && m + i < 2)
                     if (n + j > -2 && n + j < 2)
                        if (((y + j <= 56 && y + j > 32) && (x + i <= 30 && x + i > 0))  || // RCS
                              ((y + j <= 32 && y + j > 0) && (x + i <= 32 && x + i > 0))) { // PROT
                           //std::cout << "fSourceHistogram = " << fSourceHistogram << "\n";
                           //std::cout << "x= " << x << " y= " << y << "\n";
                           //std::cout << "m= " << m << " n= " << n << "\n";
                           //std::cout << "x+i= " << x+i << " y+j= " << y+j << "\n";
                           tempE = fSourceHistogram->GetBinContent(x + i, y + j);
                           //               std::cout << "Energy= " << tempE << "\n";
                           if (tempE > 5.0) result = false;
                        }
         return(result);
      }

   protected:
      Bool_t fQuit;
      Int_t peaksfound;
      Int_t xpos, ypos, zpos;
      Double_t tempMax;
      Int_t offset;

      ClassDef(LocalMaxClusterProcessor , 1)
};

#endif


