#ifndef SANERunManager_HH
#define SANERunManager_HH 1

#include "TSQLRow.h"
#include "TSQLResult.h"
#include "TStopwatch.h"
//#include "TSQLStatement.h"


#include "InSANERunManager.h"
#include "InSANETarget.h"

/** Concrete implementation of the InSANERunManager  for a run manager
 *
 * \ingroup Runs
 * \ingroup Managers
 */
class SANERunManager : public InSANERunManager {
public:

   ~SANERunManager();

   /** Implements pure virtual method
    * Generates a pre-run report based on info from tables, files,
    * in memory, or even user input. Fills an initial (new) InSANERun
    * Which should actually be a concrete imp of InSANERun.
    * And should be saved to root file.
    */
   virtual Int_t GeneratePreRunReport();

   /** Implements pure virtual method
    * Generates a Run report
    * Returns 0 if all is well
    */
   virtual Int_t GenerateRunReport();

   /**
    * Converts h2root files to raw InSANE data.
    * Assumes that you have ran HallC Analyzer
    * See AnalyzerToInSANE
    * Returns 0 if all is well
    */
   Int_t ConvertRawData();

   /** Runs BETAPedestalAnalysis and saves InSANEDetectorPedestal's
    * TClonesArray inside a tree
    */
   Int_t CalculatePedestals(const char * sourceTreeName = "betaDetectors") ;

   /** Runs BETATimingAnalysis and saves InSANEDetectorTiming's
    *  TClonesArray inside a tree
    */
   Int_t CalculateTiming(const char * sourceTreeName = "betaDetectors") ;

   virtual InSANETarget * BuildTarget(Double_t packingFraction = 0.65);

   TString fResultsFileName;
   TFile * fResultsFile;

   /**  Get main results file. data/SANE.root
    */
   TFile * GetResultsFile() {
      /*      fResultsFileName = TString("data/SANE.root");*/
      if (!fResultsFile) fResultsFile = new TFile("data/SANE.root", "UPDATE");

      return(fResultsFile);
   }
public:
   /**
    *  Creates a timing and pedestal corrected detector tree
    */
//  Int_t CreateCorrectedTree(const char * sourceTreeName = "betaDetectors");


   /**
    *  Exectues clustering of bigcal
    */
//  Int_t Cluster(const char * sourceTreeName = "correctedBetaDetectors") ;

   /**
    *  Exectues clustering of bigcal with and event display
    */
//  Int_t ClusterWithDisplay(const char * sourceTreeName = "correctedBetaDetectors") ;

   /**
    *  Exectues clustering of bigcal for pi0 event triggers
    */
//  Int_t ClusterWithDisplayPi0(const char * sourceTreeName = "correctedBetaDetectors") ;

   /**
    *  Exectues clustering of bigcal for pi0 event triggers
    */
//  Int_t RunPi0Clustering(const char * sourceTreeName = "Clusters") ;

   /**
    *  Run the zeroth pass corrections
    *
    */
//  Int_t RunZerothPass(const char * sourceTreeName = "betaDetectors") ;

//  Int_t RunFirstPass(const char * sourceTreeName = "betaDetectorsZerothPass") ;

//  Int_t RunAlternateFirstPass(const char * sourceTreeName = "betaDetectorsZerothPass") ;

//  Int_t RunScalerAnalysis(const char * sourceTreeName = "saneScalers");

//  sane_run_general_info * sqlRunInfo;

   /** Get the pointer to this instanton class
   */
   static SANERunManager * GetRunManager() {
      if (!fgSANERunManager) fgSANERunManager = new SANERunManager();
      return((SANERunManager*)fgSANERunManager);
   }

protected:
   SANERunManager();

   ClassDef(SANERunManager, 2)
};


#endif
