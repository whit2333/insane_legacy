#ifndef InSANETriggerCounter_HH 
#define InSANETriggerCounter_HH 1

#include "TROOT.h"
#include "TObject.h"

/** Simple trigger counter to determine dead/live times.
 *
 */
class InSANETriggerCounter : public TObject {
   protected:

   public:
      InSANETriggerCounter();
      virtual ~InSANETriggerCounter();

      //   InSANETriggerEvent& operator=(const InSANETriggerEvent &rhs) {
      //     // Check for self-assignment!
      //     if (this == &rhs)      // Same object?
      //       return *this;        // Yes, so skip assignment, and just return *this.
      //     // Deallocate, allocate new space, copy values...
      //     fEventNumber = rhs.fEventNumber;
      //     fRunNumber = rhs.fRunNumber;
      //     fCodaType = rhs.fCodaType;
      //     }

      Long64_t   fScaler;
      Long64_t   fTriggerCount;
      Long64_t   fNegativeHelicityScaler;
      Long64_t   fPositiveHelicityScaler;
      Long64_t   fNegativeHelicityTriggerCount;
      Long64_t   fPositiveHelicityTriggerCount;
      Double_t   fLiveTime;
      Double_t   fDeadTime;
      Double_t   fNegativeHelicityLiveTime;
      Double_t   fNegativeHelicityDeadTime;
      Double_t   fPositiveHelicityLiveTime;
      Double_t   fPositiveHelicityDeadTime;
      Double_t   fLiveTimeAveragePlus;
      Double_t   fLiveTimeAverageMinus;
      Double_t   fLiveTimeAverage;
      Double_t   fDeadTimeAverage;
      Double_t   fDeadTimeAveragePlus;
      Double_t   fDeadTimeAverageMinus;
      Bool_t     fHasGoodDeadTime;

      void ClearCounts(Option_t *opt = ""); 
      void Clear(Option_t *opt = "");

      /** Count the event for plus:hel=1 or minus:hel=-1 */
      void Count(Int_t hel);

      /** Calculates the dead/live times */
      void Calculate();

      /** Updates the average livetimes (and deadtimes).
       *  lt: live time
       *  ltp: positive live time
       *  ltm: negative live time
       *  i:  interval number
       */
      void UpdateLiveTimeAverages(Double_t * lt, Double_t * ltp, Double_t * ltm, Int_t i);

      ClassDef(InSANETriggerCounter, 1)
};

#endif

