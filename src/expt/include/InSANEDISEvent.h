#ifndef InSANEDISEvent_H
#define InSANEDISEvent_H 1

#include "TROOT.h"
#include "InSANEEvent.h"

/** Simple DIS event structure.
 *  Used by InSANEAsymmetry. 
 */
class InSANEDISEvent : public InSANEEvent {
   public:
      Double_t  fW;
      Double_t  fx;
      Double_t  fQ2;
      Double_t  fBeamEnergy;
      Double_t  fEnergy;
      Double_t  fTheta;
      Double_t  fPhi;
      Int_t     fHelicity;
      Double_t  fGroup;

   public:
      InSANEDISEvent() {
         ClearEvent();
      }
      virtual ~InSANEDISEvent() {}

      void ClearEvent(Option_t * opt = ""){
         fW = 0;
         fx = 0;
         fQ2 = 0;
         fBeamEnergy = 0;
         fEnergy = 0;
         fTheta = 0;
         fPhi = 0;
         fHelicity = 0;
         fGroup = 0;
      }

      ClassDef(InSANEDISEvent, 1)
};

#endif

