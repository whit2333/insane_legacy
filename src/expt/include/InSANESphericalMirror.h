#ifndef InSANESphericalMirror_h
#define InSANESphericalMirror_h

#include "InSANECherenkovMirror.h"


/** Concrete class for a spherical mirror
 *
 *  \ingroup detComponents
 */
class InSANESphericalMirror : public InSANECherenkovMirror  {

public :

   InSANESphericalMirror(Int_t num = 0) : InSANECherenkovMirror(num) { }
   virtual ~InSANESphericalMirror() { }

   Double_t GetVerticalSize() {
      return (36.5/*cm*/);
   };
   Double_t GetHorizontalSize() {
      return (35.5/*cm*/);
   };

   ClassDef(InSANESphericalMirror, 1)
};


#endif
