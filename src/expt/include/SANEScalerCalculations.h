#ifndef SANEScalerCalculations_HH
#define SANEScalerCalculations_HH

#include "BETACalculation.h"
#include "InSANEScalerCalculation.h"
#include "SANEScalers.h"

/** Scaler Calculations.
 *
 *  Need to:
 *   - Calculate the Livetime = #BETAEvents/#Beta2scaler
 *     This requires counting the beta2events
 *
 *  \note We are using the virtual method InSANECalculation::CalculateInterval()
 *
 * \ingroup Scalers
 * \ingroup Calculations
 */
class SANEScalerCalculation : public BETACalculation {

   public :
      SANEScalerCalculation(InSANEAnalysis * analysis = nullptr);
      virtual ~SANEScalerCalculation();

      void                   SetScalers(InSANEAnalysisEvents * scalers) {
         if (scalers)fScalers = (SANEScalers*)scalers;
      }
      InSANEAnalysisEvents * GetScalers() {
         return(fScalers);
      }

      virtual Int_t Initialize();

      /** Called for non scaler events and does the accumulations.  */
      virtual Int_t Calculate() ;

      /** Calculates the values at the end of the scaler interval.  */
      virtual Int_t CalculateInterval();

      virtual Int_t Finalize();

      virtual void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  SANEScalerCalculation - \n ";
         std::cout <<  "      Counts all the events BETA1,BETA2,TS,etc., between scaler events. \n";
         std::cout <<  "      Calculates the interval's dead/live times (with and w/o helicity)  \n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

   public:

      Double_t       fBETA2LiveTimeAverage;
      Double_t       fTSLiveTimeAverage;
      Double_t       fPi0LiveTimeAverage;
      Double_t       fPositiveLiveTimeSum;
      Double_t       fNegativeLiveTimeSum;
      Double_t       fLiveTimeSum;
      Int_t          fNumberOfIntervals;
      Bool_t         fClearCounts;           // Signals Calculate to reset the counters
      TTree        * fScalerOutTree;
      Double_t       fTotalCharge;
      SANEScalers  * fScalers;
      InSANERun    * fRun;

      ClassDef(SANEScalerCalculation, 2)
};




/**  Calculates the Helicity dependent dead (live) times.
 *
 *
 */
class BETADeadTimeCalculation :  public SANEScalerCalculation {
   protected:
      Double_t        fTotalCharge;
      Int_t           fNumberOfIntervals;
      TTree         * fScalerOutTree;
      InSANERun     * fRun;
      TF1           * fBETA2LiveTimeFit;

      Double_t        fParaLiveTimeSlope;
      Double_t        fPerpLiveTimeSlope;

      Double_t LiveTimePara(Double_t counts) const {
         return( 1.0 - counts*fParaLiveTimeSlope) ;
      }
      Double_t LiveTimePerp(Double_t counts) const {
         return( 1.0 - counts*fPerpLiveTimeSlope) ;
      }


   public:
      BETADeadTimeCalculation(InSANEAnalysis * analysis = nullptr) : SANEScalerCalculation(analysis) {
         fBETA2LiveTimeFit = nullptr;
      }
      ~BETADeadTimeCalculation() {
      }

      /** Initialize the deadtime calculation.
       */
      Int_t Initialize() ;


      /** Calculates the values at the end of the scaler interval.  */
      Int_t Calculate() {
         return(0);
      }

      /** Calculates the average values.   */
      Int_t CalculateInterval();


      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  BETADeadTimeCalculation - \n ";
         std::cout <<  "      \n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

      ClassDef(BETADeadTimeCalculation, 2)
};


#endif

