#ifndef SANEScalerAnalyzer_H
#define SANEScalerAnalyzer_H 1
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "InSANECalculation.h"

#include "InSANEAnalyzer.h"
#include "InSANEScalerAnalysis.h"
#include "TString.h"
#include "SANERunManager.h"

/** Scaler Analysis class
 *
 *  \ingroup Analyzers
 */
class SANEScalerAnalyzer :  public InSANEScalerAnalysis , public InSANEAnalyzer  {
public:
   /** c'tor
    *  argument should be the name of the new tree to create
    */
   SANEScalerAnalyzer(const char * newTreeName = "Scalers", const char * uncorrectedTreeName = "saneScalers") :
      InSANEScalerAnalysis(uncorrectedTreeName) ,
      InSANEAnalyzer(newTreeName, uncorrectedTreeName) {
      fType = kScalers;
   };

   virtual ~SANEScalerAnalyzer() {

      fCalculatedTree->BuildIndex("Scalers.fRunNumber", "Scalers.fEventNumber");
      fCalculatedTree->Write();
      fCalculatedTree->FlushBaskets();

      SANERunManager::GetRunManager()->WriteRun();
      if (SANERunManager::GetRunManager()->fVerbosity > 0)  printf("\n - SANEScalerAnalyzer destructor \n");

   };

   virtual void Initialize(TTree * t = nullptr);

   /*  virtual void Process();*/

   /**
    *  Fill Trees then clear all event data
    */
   virtual void FillTrees() {
      fCalculatedTree->Fill();
      //fClusterTree->Fill();


   };

   virtual void MakePlots();

   ClassDef(SANEScalerAnalyzer, 1)
};


#endif

