#ifndef InSANEDetectorPackage_H
#define InSANEDetectorPackage_H 1

#include "InSANEAsymmetry.h"
#include "InSANEDetector.h"
#include "TObject.h"
#include "TList.h"

/**  ABC For a Detector Package Event.
 * Is this needed?
 */
class InSANEDetectorPackageEvent : public TObject {
   
   public:
      Long64_t    fEventNumber;    ///< Event number 
      Int_t       fRunNumber;      ///< Run number 
      Int_t       fNumberOfTracks; ///< Number of tracks.
      Int_t       fNumberOfHits;   ///< Number of Hits.  
      Bool_t      fGoodEvent;      ///< Is a good event?

   public:
      InSANEDetectorPackageEvent();
      virtual ~InSANEDetectorPackageEvent();

      /** Implemented for concrete class.
       *  Note: when TClonesArray is cleared with Clear("C") the clear method
       *  of the hit class is also called.
       */
      virtual void ClearEvent(Option_t * opt = "C");

      ClassDef(InSANEDetectorPackageEvent, 2)
};



/**  ABC for a Detector Package.
 *
 *   A "Detector Package" is a collection of InSANEDetector 's.
 *
 *   \ingroup Detectors
 */
class InSANEDetectorPackage : public TNamed {

   protected:
      Int_t                         fReferenceTDC;
      Double_t                      fReferenceTime;
      TList                       * fDetectorList;
      InSANEDetectorPackageEvent  * fEvent;

   public:
      InSANEDetectorPackage(const char * n = "InSANEDetPackage", const char * t = "");
      virtual ~InSANEDetectorPackage();

      virtual Int_t ProcessEvent() = 0;

      virtual void  SetReferenceTDC(Int_t val);

      virtual Int_t ClearEvent(Option_t * opt ="");

      void          SetEventsAddress(InSANEDetectorPackageEvent* event); // needed?

      ClassDef(InSANEDetectorPackage, 2)
};


/** Event object for InSANEAsymmetryDetector
 *
 */
class InSANEAsymmetryDetectorEvent : public InSANEDetectorPackageEvent {

   public:
      InSANEAsymmetry fAsymmetry;

   public:
      InSANEAsymmetryDetectorEvent();
      virtual ~InSANEAsymmetryDetectorEvent();
      virtual void ClearEvent(Option_t * opt = "C");

      ClassDef(InSANEAsymmetryDetectorEvent,2)
};


/**  A detector which measures a generic asymmetry. The asymmetry is between two
 *   detectors.
 *
 *   This class was written to do systemmatic checks of different parts of a
 *   large detector (BETA).
 */
class InSANEAsymmetryDetector : public InSANEDetectorPackage {

   public:
      InSANEAsymmetryDetector(InSANEDetector *  det1 = nullptr , InSANEDetector * det2 = nullptr);
      virtual ~InSANEAsymmetryDetector();

      /**  Process the detector package's event data.
       *   This asymmetry detector calculates the
       */
      virtual Int_t ProcessEvent();

      ClassDef(InSANEAsymmetryDetector, 1)
};

#endif
