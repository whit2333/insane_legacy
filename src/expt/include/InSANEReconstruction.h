#ifndef InSANEReconstruction_HH
#define InSANEReconstruction_HH

#include "TMath.h"
#include "TObject.h"
#include "TClonesArray.h"
#include "InSANETrajectory.h"
#include "TVector3.h"
#include "InSANEPhysicalConstants.h"

/** ABC Reconstruction Class
 *
 *  Use a reconstruction to develop a decay or trajectory reconstruction
 *
 *
 */
class InSANEReconstruction : public TObject {
public:
   InSANEReconstruction() {
      fNTrajectories = 0;
      fPairIndex = 0;
      fClusterIndex = 0;
   }

   ~InSANEReconstruction() {
      fNTrajectories = 0;
   }

   /** Clear */
   void Clear() {
      fNumber = -1;
      fMass = 0.0;
      fEnergy = 0.0;
      fMomentum = 0.0;
      fPairIndex = 0;
      fClusterIndex = 0;
      fMass = 0.0;
      fQ2 = 0.0;
      fEnergy = 0.0;
      fBeamEnergy = 0.0;
      fPhi = 0.0;
      fTheta = 0.0;
      fE1 = 0.0;
      fE2 = 0.0;

   }

   /** Reconstructs the physics quantities from E and theta*/
   virtual Int_t ReconstructEvent() {
      fQ2 = 4.0 * fEnergy * fBeamEnergy * TMath::Sin(fTheta / 2.0) * TMath::Sin(fTheta / 2.0) ;
      fNu = fBeamEnergy - fEnergy;
      fx = fQ2 / (2.0 * (M_p/MeV) * fNu);
      fW2 = 2.0 * fNu * (M_p/MeV) - fQ2 + (M_p/MeV) * (M_p/MeV);
      return(0);
   }

   Double_t fEnergy;
   Double_t fBeamEnergy;

   Double_t fTheta;
   Double_t fPhi;
   Double_t fMomentum;
   TVector3 fPi0Momentum;

   Double_t fQ2;
   Double_t fx;
   Double_t fW2;
   Double_t fNu;


   Int_t    fNumber;
   Double_t fMass;
   Double_t fAngle;

   Double_t fE1;
   Double_t fE2;

   Double_t fClusterDistance;
   Double_t fTime;

   Int_t fPairIndex;
   Int_t fClusterIndex;

   Int_t                      fNTrajectories;        //
//   TRefArray                  fLucitePositions;      // Array of hit positions passing
//   TRefArray                  fTrackerPositions;     // Array of hit positions passing
//   TRefArray                  fTrackerHits;          //
//   TRefArray                  fLuciteHits;           //

   ClassDef(InSANEReconstruction, 3)
};

#endif

