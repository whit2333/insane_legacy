#ifndef InSANEComptonAsymmetry_H
#define InSANEComptonAsymmetry_H 1
#include "InSANEMeasuredAsymmetry.h"

/** Kinematic Coefficient
 *  Used with InSANEMeasuredAsymmetry(s)  to calculate Compton Asymmetries.
 *  NOT YET IMPLEMENTED
 */
class InSANEKinematicCoefficient: public InSANEMeasuredAsymmetry {
public:
   InSANEKinematicCoefficient(Int_t n = 0) : InSANEMeasuredAsymmetry(n) {
   }

   ~InSANEKinematicCoefficient() {}

   ClassDef(InSANEKinematicCoefficient, 1)
};


/** Virtual Compton Scattering Asymmetry.
 *  \ingroup asymmetries
 *
 */
class InSANEComptonAsymmetry : public InSANEMeasuredAsymmetry {
public:
   InSANEComptonAsymmetry(Int_t n = 0) : InSANEMeasuredAsymmetry(n) {
   }
   ~InSANEComptonAsymmetry() {}
   /** Necessary for Browsing */
   Bool_t         IsFolder() const {
      return kTRUE;
   }
   void           Browse(TBrowser* b) {
      if (fParaAsym) b->Add(fParaAsym);
      if (fPerpAsym) b->Add(fPerpAsym);
   }

   Int_t CalculateAsymmetries();

   void SetParaAsym(InSANEMeasuredAsymmetry * a) {
      fParaAsym = a;
   }
   void SetPerpAsym(InSANEMeasuredAsymmetry * a) {
      fPerpAsym = a;
   }

protected:
   InSANEMeasuredAsymmetry * fParaAsym;
   InSANEMeasuredAsymmetry * fPerpAsym;

ClassDef(InSANEComptonAsymmetry, 1)
};


#endif
