#ifndef InSANEEvent_HH
#define InSANEEvent_HH 1

#include "TObject.h"
#include "TROOT.h"
#include <iostream>

/** Base class for an Event.
 *  Note an Event is defined as when the DAQ is triggered
 *
 * \ingroup Events
 */
class InSANEEvent {

   public :
      Int_t fEventNumber;
      Int_t fRunNumber;

   public :
      InSANEEvent();
      virtual ~InSANEEvent();
      virtual void  Print(Option_t * opt = "") const ;
      virtual void  ClearEvent(Option_t * opt = "C"); 

      ClassDef(InSANEEvent,4)
};

#endif


