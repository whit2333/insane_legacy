#ifndef InSANEElectronBeam_H
#define InSANEElectronBeam_H

#include "TObject.h"
//#include "InSANERunManager.h"
#include "InSANEApparatus.h"

/** Base class for a polarized electron beam
 *
 * \ingroup beam
 */
class InSANEElectronBeam : public InSANEApparatus {

public:
   InSANEElectronBeam(const char * name = "e-beam", const char * title = "Electron Beam");
   virtual ~InSANEElectronBeam();

   Double_t GetEnergy() { return(fEnergy); }
   Double_t GetBeamEnergy() { return(fEnergy); }
   void SetEnergy(Double_t en) { fEnergy = en; }
   void SetBeamEnergy(Double_t en) { fEnergy = en; }

   virtual void Print(const Option_t * opt = "") const; // *MENU*

   Double_t fEnergy;
   Double_t fCurrent;
   Int_t    fNPass;

   ClassDef(InSANEElectronBeam, 2)
};


/** Wrapper for subroutine that gets the beam polarization
 *
 *  sane_pol(hallcp,wien,qe,npass,ihwp,polarization)
 *
 *  The arguments are:
 *  - hallcp: beam energy
 *  - wien: wien angle
 *  - qe: Quantum Efficiency
 *  - npass: Number beam passes
 *  - ihwp: half wave plate status (0 or 1)
 *  - polarization: the output polarization\
 *
 *\ingroup beam
 */
extern"C" {
   void sane_pol_(double * hallcp, double * wien, double * qe, int * npass, int * ihwp, double * polarization);
}



/** HallC Polarized Electron Beam
 *
 *  Uses fortran subroutine sane_pol(hallcp,wien,qe,npass,ihwp,polarization) to get beam polarization
 *
 * \ingroup beam
 */
class HallCPolarizedElectronBeam : public InSANEElectronBeam {
   public:
      HallCPolarizedElectronBeam(const char * name = "HallC-beam", const char * title = "Hall C Rastered Beam");
      virtual ~HallCPolarizedElectronBeam();

      Double_t GetPolarization();
      virtual void Print(const Option_t * opt = "") const ; // *MENU*


      Double_t fPolarization;
      Double_t fWienAngle;
      Double_t fQuantumEfficiency;
      Int_t    fHalfwavePlate;

      ClassDef(InSANEElectronBeam, 2)
};


#endif

