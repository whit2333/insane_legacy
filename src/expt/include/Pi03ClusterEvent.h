#ifndef Pi03ClusterEvent_h
#define Pi03ClusterEvent_h
#include <TROOT.h>
#include <TObject.h>
#include <TH2.h>
#include <TClonesArray.h>
#include "InSANEReconstructedEvent.h"
#include "TMath.h"
#include "InSANETrajectory.h"
#include "InSANEReconstruction.h"
#include "Pi0ClusterPair.h"
#include "TVector3.h"

/**  Event class for pi0 mass reconstruction which has 3 clusters.
 *   Here we are primarily concerned with the \f$ \pi_0 \rightarrow \gamma e^+ e^- \f$.
 *
 *   For the decay of the virutal photon we need an e+ and e- cluster, but we do not identify
 *   the charges for each cluster. Thus they are interchangeable. So we need to pair up each cluster
 *   with a pair if they are not in that cluster.
 *
 *  \ingroup pi0
 */
class Pi03ClusterEvent : public InSANEReconstructedEvent {

   public:
      TClonesArray * fReconstruction; //->
      TClonesArray * fClusterPairs; //->
      TClonesArray * fClusters; //->
      Int_t          fNumberOfGroups;
      Int_t          fNumberOfPairs;

   public:
      Pi03ClusterEvent();
      virtual ~Pi03ClusterEvent();

      virtual void Reset();
      virtual void ClearEvent(Option_t * opt = "");
      const char * PrintEvent() const ;

      virtual void Print() const ;

      /**  Calaculate the mass of the pi0 through an assumed decay.
       *   Here we are primarily concerned with the \f$ \pi_0 \rightarrow \gamma e^+ e^- \f$ .
       *   The first vector is assumed to be the photon.
       *
       * \f$ M^2 &=  2 E_1 E_2 (1  - \cos\theta_{\pm}) + 2 \nu_1 (E_1 + E_2)(1  -  \cos\theta_{\gamma \gamma^*} ) \f$
       */
      Double_t CalculateMass(Double_t nu1, Double_t E1, Double_t E2, Double_t thetapm, Double_t thetaGammaGamma);

      ClassDef(Pi03ClusterEvent, 1)
};

#endif



