#ifndef SANESecondPassAnalyzer_H
#define SANESecondPassAnalyzer_H 1
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "InSANECalculation.h"

#include "InSANEAnalyzer.h"
#include "SANEClusteringAnalysis.h"
#include "TString.h"
#include "SANERunManager.h"



/** Second Pass Analyzer class
 *
 *  \ingroup Analyzers
 */
class SANESecondPassAnalyzer :  public SANEClusteringAnalysis , public InSANEAnalyzer  {
public:

   /**
    *  argument should be the name of the new tree to create
    */
   SANESecondPassAnalyzer(const char * newTreeName = "correctedBetaDetectors", const char * uncorrectedTreeName = "betaDetectors") ;

   virtual ~SANESecondPassAnalyzer() ;

   virtual void Initialize(TTree * t = nullptr);


   /**  Needed to clear event from InSANEAnalyzer::Process() or InSANECorrector::Process()
    *
    */
   void ClearEvents() {
//       fClusterEvent->ClearEvent();
//       fClusters->Clear("C");
      /*      std::cout << " last clearing \n";*/
      fEvents->ClearEvent();
   }


   /**
    *  Fill Trees then clear all event data
    */
   virtual void FillTrees() {
      if (fCalculatedTree)fCalculatedTree->Fill();

//     fClusterEvent->ClearEvent("C");
//     fEvents->BETA->ClearEvent("C");
//     fClusters->Clear("C");
   }

   virtual void MakePlots();

   ClassDef(SANESecondPassAnalyzer, 1)
};


#endif

