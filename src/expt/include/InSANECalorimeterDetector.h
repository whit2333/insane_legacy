#ifndef InSANECalorimeterDetector_HH
#define InSANECalorimeterDetector_HH 1

#include "InSANEDetector.h"
#include "TSortedList.h"
#include "InSANELeadGlassBlock.h"

/**  Base Class for a electromagnetic calorimeter.
 */
class InSANECalorimeterDetector : public InSANEDetector {

   protected:
      TSortedList fCalorimeterBlocks;

   public:
      InSANECalorimeterDetector(const char * n="", const char * t="") : InSANEDetector(n,t) {
         fCalorimeterBlocks.Clear();
      }
      virtual ~InSANECalorimeterDetector() {
      }

      virtual Int_t Build() {
         return(0);
      }

      void AddBlock(InSANELeadGlassBlock* block) {
         fCalorimeterBlocks.Add(block);
      }

      /** Get the block using the Block number (= array index + 1) */
      InSANELeadGlassBlock * GetBlock(Int_t num) {
         return (InSANELeadGlassBlock*)fCalorimeterBlocks.At(num - 1);
      }

      TSortedList * GetListOfBlocks() { return &fCalorimeterBlocks; }


      ClassDef(InSANECalorimeterDetector, 1)
};



#endif
