#ifndef BigcalElasticClusterAnalysis_H
#define BigcalElasticClusterAnalysis_H
#include "TROOT.h"
#include "InSANEAnalysis.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "InSANEDatabaseManager.h"
#include "BIGCALClusterProcessor.h"
#include <TClonesArray.h>
#include "SANEEvents.h"

/** Analysis of elastic events in HMS and BETA
 *
 *  Concrete implentation for analysis of the SANE elastic events.
 *  A coincidence was formed between the HMS and bigcal. \todo{ Check
 *  the trigger of bigcal. Was the SANE gas Cherenkov part of the elastic trigger too?}
 *
 * \ingroup Analyses
 * \ingroup BIGCAL
 */
class BigcalElasticClusterAnalysis : public InSANEAnalysis {
public :

   /**
    *
    */
   BigcalElasticClusterAnalysis(Int_t run);

   /**
    *
    */
   ~BigcalElasticClusterAnalysis();

   /**
    *
    */
   Int_t fRunNumber ;

   /**
    *
    */
   Int_t AnalyzeRun();

   /**
    *
    */
   Int_t Visualize() ;

   /**
    *
    */
   TFile * fAnalysisFile;

   /**
    *
    */
   TChain * fAnalysisTree;

   /**
    *
    */
   TFile * fOutputFile;

   TTree * fOutputTree;

   BIGCALClusterProcessor * fClusterProcessor;
   TClonesArray * fClusters;

   SANEEvents * events;

   //InSANEDatabaseManager * dbManager;

   ClassDef(BigcalElasticClusterAnalysis, 2);
};


#endif

