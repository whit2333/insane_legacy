#ifndef BETAG4DetectorsAnalysis_H
#define BETAG4DetectorsAnalysis_H

#include "TROOT.h"
#include "BETAG4Analysis.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "SANEEvents.h"
#include "BigcalEvent.h"

#include "InSANEDatabaseManager.h"

#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"
#include <exception>


/**  Concrete Analysis of simulation data
 *
 * \ingroup Analses
 */
class BETAG4DetectorsAnalysis : public BETAG4Analysis {
public :

   BETAG4DetectorsAnalysis();
   BETAG4DetectorsAnalysis(Int_t run);

   virtual ~BETAG4DetectorsAnalysis();

   /**
    *  Starts the loop over events and fills histograms
    */
   virtual Int_t AnalyzeRun();

   /**
    *
    */
   virtual Int_t Visualize() ;


   /**
    * Allocates and defines histograms
    */
   virtual Int_t  AllocateHistograms() {
      return(0) ;
   }

//   TFile  * fAnalysisFile;
//   TChain * fAnalysisTree;
//   TFile  * fOutputFile;
//   TTree  * fOutputTree;

/// The following are all
/// pointers to the memory allocated by new SANEEvents
   /*
     BETAEvent * aBetaEvent;
       BigcalEvent * bcEvent;
         BigcalHit * aBigcalHit;
       LuciteHodoscopeEvent * lhEvent;
         LuciteHodoscopeHit * aLucHit;
       GasCherenkovEvent * gcEvent;
         GasCherenkovHit * aCerHit;
       ForwardTrackerEvent * ftEvent;
         ForwardTrackerHit * aTrackerHit;*/
   /*
     HallCBeamEvent        * beamEvent;
     HMSEvent              * hmsEvent;
     InSANEMonteCarloEvent * mcEvent;
     InSANETriggerEvent    * trigEvent;*/

//   BIGCALGeometryCalculator * bcgeo;
//   SANEDatabaseManager * dbManager;

////// HISTOGRAMS ///////
   TH1F * cer_tdc_hist[12];
   TH1F * cer_adc_hist[12];
   TH2F * cer_tdc_vs_adc_hist[12];

private:

   ClassDef(BETAG4DetectorsAnalysis, 3);
};


#endif

