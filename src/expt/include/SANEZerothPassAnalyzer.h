#ifndef SANEZerothPassAnalyzer_H
#define SANEZerothPassAnalyzer_H 1

#include "InSANEAnalyzer.h"
#include "InSANEDetectorAnalysis.h"
#include "TString.h"
#include "SANERunManager.h"


/**  Zeroth Pass Corrector class.
 *
 *  \ingroup Analyzers
 */
class SANEZerothPassAnalyzer : public InSANEDetectorAnalysis, public InSANEAnalyzer  {

   public:
      SANEZerothPassAnalyzer(const char * newTreeName = "correctedBetaDetectors",
                             const char * uncorrectedTreeName = "betaDetectors");
      virtual ~SANEZerothPassAnalyzer();

      virtual void Initialize(TTree * t = nullptr);
      virtual void MakePlots();

      /** Method called from ApplyCorrections which fills the trees. */
      virtual void FillTrees() { if (fCalculatedTree) fCalculatedTree->Fill(); }

      /** Needed to clear event from InSANEAnalyzer::Process() or InSANECorrector::Process(). */
      virtual void ClearEvents() { InSANEDetectorAnalysis::ClearEvents(); }

      ClassDef(SANEZerothPassAnalyzer, 2)
};


#endif

