#ifndef BETADetectorPackage_HH
#define BETADetectorPackage_HH 1

#include "InSANEDetectorPackage.h"
#include "BigcalDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "GasCherenkovDetector.h"
#include "ForwardTrackerDetector.h"

/**  Concrete implementation of InSANEDetector for Big Electron Telescope Array (BETA)
 *
 *  BETADetectorPackage has pointers to its various detectors (cherenkov,bigcal,tracker,hodoscope)
 *
 *  \ingroup BETA
 */
class BETADetectorPackage : public InSANEDetectorPackage {

   public:
      ForwardTrackerDetector  * fTrackerDetector;     ///<
      GasCherenkovDetector    * fCherenkovDetector;   ///<
      LuciteHodoscopeDetector * fHodoscopeDetector;   ///<
      BigcalDetector          * fBigcalDetector;      ///<

   public:

      BETADetectorPackage(const char * name = "BETADetector", Int_t runNumber = 0);
      virtual ~BETADetectorPackage();

      virtual Int_t   ClearEvent(Option_t * opt = "C");
      virtual Int_t ProcessEvent();

      TH2F * fEventLuciteTimeSum;
      TH2F * fRunLuciteTimeSum;
      TH2F * fEventLuciteTimeDifference;
      TH2F * fRunLuciteTimeDifference;

      ClassDef(BETADetectorPackage, 1)
};
#endif

