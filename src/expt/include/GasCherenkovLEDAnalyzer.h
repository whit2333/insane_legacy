#ifndef GasCherenkovLEDAnalyzer_H
#define GasCherenkovLEDAnalyzer_H 1
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "InSANECalculation.h"

#include "InSANEAnalyzer.h"
#include "InSANEDetectorAnalysis.h"
#include "TString.h"
#include "SANERunManager.h"

/**  First Pass Analysis class
 *
 *  <ol>
 *  <li> The first goal of this pass is to cluster the calorimeter data.</li>
 *  <li> The second goal of this pass is to analyze the cherenkov in the following way...
 *    <ul><li>independent of all other detectors (of course connected through triggers)</li>
 *    <li>using just the new bigcal clusters calculated, determine geometry parameters</li>
 *    <li>using hodoscope with cherenkov timing cut, determine geometry parameters</li>
 *    </ul>
 *  </li>
 *  </ol>
 *
 *  \ingroup Cherenkov
 */
class GasCherenkovLEDAnalyzer :  public InSANEDetectorAnalysis , public InSANEAnalyzer  {
public:
   /** c'tor
    *  argument should be the name of the new tree to create
    */
   GasCherenkovLEDAnalyzer(const char * newTreeName = "betaLED", const char * uncorrectedTreeName = "betaDetectors") :
      InSANEDetectorAnalysis(uncorrectedTreeName) ,
      InSANEAnalyzer(newTreeName, uncorrectedTreeName) {
      fType = kEvents;

   }

   virtual ~GasCherenkovLEDAnalyzer() {


//   fOutputTree->BuildIndex("bigcalClusterEvent.fRunNumber","bigcalClusterEvent.fEventNumber");
//   fOutputTree->Write();
//   fOutputTree->FlushBaskets();  SANERunManager::GetRunManager()->fCurrentRun->fAnalysisPass = 1;
      SANERunManager::GetRunManager()->WriteRun();
      if (SANERunManager::GetRunManager()->fVerbosity > 0)  printf("\n - GasCherenkovLEDAnalyzer destructor \n");

   }

   virtual void Initialize(TTree * t = nullptr);

   /*  virtual void Process();*/

   /**
    *  Fill Trees then clear all event data
    */
   virtual void FillTrees() {
      //fCorrectedTree->Fill();

      fEvents->BETA->ClearEvent("C");

   };

   virtual void MakePlots();

   ClassDef(GasCherenkovLEDAnalyzer, 1)
};


#endif

