#ifndef BETAEvent_h
#define BETAEvent_h 1

#include "TObject.h"
#include "TTree.h"
#include "TBranch.h"
#include "BigcalEvent.h"
#include "GasCherenkovEvent.h"
#include "LuciteHodoscopeEvent.h"
#include "ForwardTrackerEvent.h"
#include  "InSANEDetectorPackage.h"

/**Concrete Event class for the Big Electron Telescope Array (BETA)
 *
 * The class BETAEvent is used to hold raw data at the event level.
 * Using this
 * class to hold all the detector data for both real and simulated (to look real) data allows
 * one to do easy analysis of both.
 *
 * Using BETAEvent:
 *
 * \ingroup Events
 *  \ingroup BETA
 */
class BETAEvent : public InSANEDetectorPackageEvent {

   public :
      Bool_t fGoodTiming;
      Bool_t fGoodCherenkov;
      Bool_t fGoodGeometry;

      /**
       * Constructor does NOTs allocate memory for Detector Events.
       * Use SANEEvents or similar class for simplified interface.
       */
      BETAEvent();

      virtual ~BETAEvent();

      /**
       *  Allocates the memory and sets the pointers
       *   for each Detector event by calling new for each Event type.
       * Main pointers are fBigcalEvent, fGasCherenkovEvent, fLuciteHodoscopeEvent
       * and fForwardTrackerEvent) *
       * \note { Memory for fHits or fgDETECTOREvent pointers are NOT allocated!}
       */
      Int_t AllocateMemory();

      /**
       *  Allocates the memory and sets the pointers
       *   for each Detector Hit Array
       */
      Int_t AllocateHitsMemory();

      /**
       *  Frees memory of Hit arrays and individual detector objects
       */
      Int_t FreeMemory();

      /// Basic Bigcal Event Class
      BigcalEvent * fBigcalEvent;                      //! bigcal

      /// Basic Cherenkov Event Class
      GasCherenkovEvent * fGasCherenkovEvent;          //! cherenkov

      /// Basic Hodoscope Event Class
      LuciteHodoscopeEvent * fLuciteHodoscopeEvent;    //! lucite

      /// Basic Tracker Event Class
      ForwardTrackerEvent * fForwardTrackerEvent;      //! tracker

      /// This adds to the tree in the argument a branch for each of
      /// BETA detector's HitsArray,
      /// \todo
      /// Ideally we would want to not stream the array from the Event class
      /// but use this instead so that we don't have to stream those data members
      Int_t AddHitsBranch(TTree * t);

      Int_t SetHitsBranch(TTree * t); ///< Sets The Appropriate branch addresses

      void ClearEvent(Option_t * opt = ""); ///< Clears the Events

      Int_t           fAnalysisLevel; ///< Number indicating how much analysis has h appened to detector data

      // SANE
      // Not sure if these data members are needed
      Float_t   half_plate;
      Int_t   bgid;
      Int_t   bgtype;
      Int_t   btrigtype;
      Int_t   ngooda;
      Int_t   ngoodt;
      Int_t   ngoodta;
      Int_t   ngoodtt;
      Int_t irowmax, icolmax;
      Float_t  max_adc;

      Long64_t eventNumber;

   private :

      ClassDef(BETAEvent, 7)
};

#endif



