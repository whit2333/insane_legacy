// Whitney Armstrong's modified version of TEveTrackPropagator
// Authors: Matevz Tadel & Alja Mrak-Tadel: 2006, 2007

/*************************************************************************
 * Copyright (C) 1995-2007, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef InSANETrackPropagator2_HH
#define InSANETrackPropagator2_HH 1

/// The goal is to use this class for analysis, not display. 

#include "TVector3.h"
#include "TLorentzVector.h"
#include <vector>
#include "InSANEMagneticField.h"
#include "UVAOxfordMagneticField.h"
#include "TParticle.h"
#include "TMath.h"
#include <cassert>
#include "TEveTrans.h"
#include "TEveVector.h"

//==============================================================================
// InSANETrackPropagator2
//==============================================================================

class InSANETrackPropagator2 : public TObject /*: public TEveElementList,
                                                public TEveRefBackPtr */
{

   public:
      enum EStepper_e    { kHelix, kRungeKutta };

      enum EProjTrackBreaking_e { kPTB_Break, kPTB_UseFirstPointPos, kPTB_UseLastPointPos };

   protected:
      struct Helix_t
      {
         Int_t    fCharge;   // Charge of tracked particle.
         Double_t fMaxAng;   // Maximum step angle.
         Double_t fMaxStep;  // Maximum allowed step size.
         Double_t fDelta;    // Maximum error in the middle of the step.

         Double_t fPhi;      // Accumulated angle to check fMaxOrbs by propagator.
         Bool_t   fValid;    // Corner case pT~0 or B~0, possible in variable mag field.

         // ----------------------------------------------------------------

         // helix parameters
         Double_t fLam;         // Momentum ratio pT/pZ.
         Double_t fR;           // Helix radius in cm.
         Double_t fPhiStep;     // Caluclated from fMinAng and fDelta.
         Double_t fSin, fCos;   // Current sin/cos(phistep).

         // Runge-Kutta parameters
         Double_t fRKStep;      // Step for Runge-Kutta.

         // cached
         TVector3 fB;        // Current magnetic field, cached.
         TVector3 fE1, fE2, fE3; // Base vectors: E1 -> B dir, E2->pT dir, E3 = E1xE2.
         TVector3 fPt, fPl;  // Transverse and longitudinal momentum.
         Double_t fPtMag;       // Magnitude of pT.
         Double_t fPlMag;       // Momentum parallel to mag field.
         Double_t fLStep;       // Transverse step arc-length in cm.

         // ----------------------------------------------------------------

         Helix_t();

         void UpdateCommon(const TVector3 & p, const TVector3& b);
         void UpdateHelix (const TVector3 & p, const TVector3& b, Bool_t full_update, Bool_t enforce_max_step);
         void UpdateRK    (const TVector3 & p, const TVector3& b);

         void Step(const TLorentzVector& v, const TVector3& p, TLorentzVector& vOut, TVector3& pOut);

         Double_t GetStep()  { return fLStep * TMath::Sqrt(1 + fLam*fLam); }
         Double_t GetStep2() { return fLStep * fLStep * (1 + fLam*fLam);   }
      };


   private:
      InSANETrackPropagator2(const InSANETrackPropagator2&);            // Not implemented
      InSANETrackPropagator2& operator=(const InSANETrackPropagator2&); // Not implemented

      void DistributeOffset(const TVector3& off, Int_t first_point, Int_t np, TVector3& p);

   protected:
      EStepper_e               fStepper;
      InSANEMagneticField *    fMagFieldObj;
      Bool_t                   fOwnMagFiledObj;

      // Track extrapolation limits
      Double_t                  fMaxR;          // Max radius for track extrapolation
      Double_t                  fMaxZ;          // Max z-coordinate for track extrapolation.
      Int_t                     fNMax;          // Max steps
      Double_t                  fMaxPlaneDelta; // Max plane distance  
      // Helix limits
      Double_t                  fMaxOrbs;       // Maximal angular path of tracks' orbits (1 ~ 2Pi).
      // ----------------------------------------------------------------

      // Propagation, state of current track
      std::vector<TLorentzVector>   fPoints;        // Calculated point.
      TLorentzVector          fV;             // Start vertex.
      Helix_t                 fH;             // Helix.

      TParticle *             fParticle;

      TVector3             fMomentum;     /// Current momentum
      TVector3             fVertex;       ///  Current vertex
      TVector3             fOrigin;       ///  Current vertex

      TVector3             fArbPlaneNormal;  /// Normal to intersection plane
      TVector3             fArbPlanePoint;   /// Apoint on the plane
      TVector3             fArbPlaneResult;  /// Resulting point of intersection

      TVector3             fZXPlaneNormal;  /// Normal to intersection plane
      TVector3             fZXPlanePoint;   /// Apoint on the plane
      TVector3             fZXPlaneResult;  /// Resulting point of intersection

      TVector3             fYZPlaneNormal;  /// Normal to intersection plane
      TVector3             fYZPlanePoint;   /// Apoint on the plane
      TVector3             fYZPlaneResult;  /// Resulting point of intersection

      TVector3             fXYPlaneNormal;  /// Normal to intersection plane
      TVector3             fXYPlanePoint;   /// Apoint on the plane
      TVector3             fXYPlaneResult;  /// Resulting point of intersection


      void    RebuildTracks();
      void    Update(const TLorentzVector & v, const TVector3& p, Bool_t full_update=kFALSE, Bool_t enforce_max_step=kFALSE);
      void    Step(const TLorentzVector &v, const TVector3 &p, TLorentzVector &vOut, TVector3 &pOut);

      Bool_t  LoopToPlane(const TVector3& point, const TVector3& normal, TVector3& p);
      Bool_t  LoopToVertex(TVector3& v, TVector3& p);
      Bool_t  LoopToLineSegment(const TVector3& s, const TVector3& r, TVector3& p);
      void    LoopToBounds(TVector3& p);

      Bool_t  LineToVertex (TVector3& v);
      void    LineToBounds (TVector3& p);

      void    StepRungeKutta(Double_t step, Double_t* vect, Double_t* vout);

      Bool_t  HelixIntersectPlane(const TVector3& p, const TVector3& point, const TVector3& normal,
            TVector3&itsect);
      Bool_t  LineIntersectPlane(const TVector3& p, const TVector3& point, const TVector3& normal,
            TVector3& itsect);
      Bool_t  PointOverVertex(const TLorentzVector& v0, const TLorentzVector& v, Double_t* p=nullptr);

      void    LinePlaneIntersectionPoint(const TVector3& l, const TVector3& ldir, const TVector3& point, const TVector3& norm, TVector3& c){
         Double_t d = norm.Dot(point - l);
         Double_t denom = norm.Dot(l);
         if(denom == 0.0) { 
            Warning("LinePlaneIntersectionPoint","ldir is in plane");
            c = l;
         } else { 
            d = d/denom;
            c = l + (d*ldir);
         } 
      }

      void    ClosestPointFromVertexToLineSegment(const TVector3 & v, const TVector3& s, const TVector3& r, Double_t rMagInv, TVector3& c);
      void    ClosestPointFromVertexToPlane(const TVector3 & v, const TVector3& point, const TVector3& norm, TVector3& c);
      Bool_t  ClosestPointBetweenLines(const TVector3&, const TVector3&, const TVector3&, const TVector3&, TVector3& out);

   public:
      InSANETrackPropagator2(const char* n="InSANETrackPropagator", const char* t="",
            InSANEMagneticField* field=nullptr, Bool_t own_field=kTRUE);

      virtual ~InSANETrackPropagator2();

      /** Set the polariztion direction angle in degrees */
      void SetTargetPolAngle(Double_t angle = 180.0){
         fMagFieldObj->SetPolarizationAngle(TMath::Pi()*angle/180.0);
      }

      /** Set the track momentum and position.
       */
      void SetTrackMomentumPosition(TVector3 p, TVector3 v) {
         fMomentum = p;
         fVertex = v;
         fV = TLorentzVector(v,0);
         fParticle->SetMomentum(-1.0*p.X(),-1.0*p.Y(),-1.0*p.Z(),TMath::Sqrt(p.Mag2()+0.000511*0.000511) );
         fParticle->SetProductionVertex(v.X(),v.Y(),v.Z(),0.0);
      }

      void GetPositionOnPlane(TVector3& n, TVector3& p, TVector3& r){
         // Returns the position of intersection  on a plane defined by point p and normal n.
         // Here we assume that SetTrackMomentumPosition was already called.

         // Clear the points vector 
         ResetTrack();

         // Add the vertex as the first point and set the charge used for propagation 
         InitTrack(fV.Vect(),1); /// \todo hard coded charge!! need to use TParticle::Charge() 

         TVector3 p0(fParticle->Px(),fParticle->Py(),fParticle->Pz());
         //fArbPlaneNormal.SetXYZ(n.X(),n.Y(),n.Z());
         //fArbPlanePoint.SetXYZ(p.X(),p.Y(),p.Z());

         // Update the propagation parameters.
         //Update(fV, p0, kTRUE,kTRUE);
         GoToPlane(p,n,p0);

         r = fV.Vect();
         //fArbPlaneResult.SetXYZ(fV.X(),fV.Y(),fV.Z());
         //IntersectPlane(p0,fArbPlanePoint,fArbPlaneNormal,fArbPlaneResult);
         //r.SetXYZ(fArbPlaneResult.X(),fArbPlaneResult.Y(),fArbPlaneResult.Z());
      }

      //void GetMomentumPositionAtTarget(TVector3 * p, TVector3 * v) {
      //   /// Here we are doing something like TEveTrack::MakeTrack ...
      //   TEveVectorD p0 = fTrack->GetMomentum();
      //   fV = fTrack->GetVertex();
      //   ResetTrack();
      //   //       fTrack->MakeTrack();
      //   InitTrack(fTrack->GetVertex(), fTrack->GetCharge());
      //   Update(fV, p0, kTRUE,kTRUE);
      //   LoopToVertex( p0,fOrigin);
      //   FillPointSet(fTrack);
      //   ResetTrack();
      //   p->SetXYZ(fMomentum.fX,fMomentum.fY,fMomentum.fZ);
      //   v->SetXYZ(fVertex.fX,fVertex.fY,fVertex.fZ);
      //}

      // propagation
      void   InitTrack(const TVector3& v, Int_t charge);
      void   ResetTrack();

      Int_t    GetCurrentPoint() const;
      Double_t GetTrackLength(Int_t start_point=0, Int_t end_point=-1) const;

      virtual void   GoToBounds(TVector3& p);
      virtual Bool_t GoToVertex(TVector3& v, TVector3& p);
      virtual Bool_t GoToLineSegment(const TVector3& s, const TVector3& r, TVector3& p);
      virtual Bool_t GoToPlane(const TVector3& norm, const TVector3& point, TVector3& p);

      Bool_t IntersectPlane(const TVector3& p, const TVector3& point, const TVector3& normal,
            TVector3& itsect);

      //void   FillPointSet(TEvePointSet* ps) const;

      void   SetStepper(EStepper_e s) { fStepper = s; }

      void   SetMagField(Double_t bX, Double_t bY, Double_t bZ);
      void   SetMagField(Double_t b) { SetMagField(0, 0, b); }
      void   SetMagFieldObj(InSANEMagneticField* field, Bool_t own_field=kTRUE);

      void   SetMaxR(Double_t x);
      void   SetMaxZ(Double_t x);
      void   SetMaxOrbs(Double_t x);
      void   SetMinAng(Double_t x);
      void   SetMaxAng(Double_t x);
      void   SetMaxStep(Double_t x);
      void   SetDelta(Double_t x);

      TVector3 GetMagField(Double_t x, Double_t y, Double_t z) { return fMagFieldObj->GetField(x, y, z); }
      void PrintMagField(Double_t x, Double_t y, Double_t z) const;

      EStepper_e   GetStepper()  const { return fStepper;}

      Double_t GetMaxR()     const { return fMaxR;     }
      Double_t GetMaxZ()     const { return fMaxZ;     }
      Double_t GetMaxOrbs()  const { return fMaxOrbs;  }
      Double_t GetMinAng()   const;
      Double_t GetMaxAng()   const { return fH.fMaxAng;   }
      Double_t GetMaxStep()  const { return fH.fMaxStep;  }
      Double_t GetDelta()    const { return fH.fDelta;    }

      static Bool_t IsOutsideBounds(const TVector3& point, Double_t maxRsqr, Double_t maxZ);

      static Double_t               fgDefMagField; // Default value for constant solenoid magnetic field.
      static const Double_t         fgkB2C;        // Constant for conversion of momentum to curvature.
      //static InSANETrackPropagator2  fgDefault;     // Default track propagator.

      static Double_t             fgEditorMaxR;  // Max R that can be set in GUI editor.
      static Double_t             fgEditorMaxZ;  // Max Z that can be set in GUI editor.

      ClassDef(InSANETrackPropagator2, 1); // Calculates path of a particle taking into account special path-marks and imposed boundaries.
};

#endif
