#ifndef InSANETriggerEvent_h
#define InSANETriggerEvent_h 1

#include <iostream>
#include <vector>
#include "TROOT.h"
#include "TObject.h"
#include "InSANEDetectorEvent.h"
#include "SANERunManager.h"
#include "TMath.h"

/**  Base class for event trigger
 *
 *   The event loop in InSANEAnalyzer relies on this event being changed every time the
 *   next event is read
 *
 *   There are two trees that need to be looped through, ie scalers and events.
 *
 *   The very first tree reading in Process() should read the first two scalers so that
 *   we know which event number we should read up to in the events tree (and which one to start at).
 *
 *   Skips all events until the first scaler
 *
 *   \note Needs assignement operator
 *
 *  \ingroup Events
 */
class InSANETriggerEvent : public InSANEDetectorEvent {

   public :
      InSANETriggerEvent() {
         ClearEvent();
      }

      virtual ~InSANETriggerEvent() {
         ClearEvent();
      }

      /**
       *  fCodaType =
       *  <ul>
       *  <li> 0 is scalers at 2 sec intervals </li>
       *  <li> 1 is htrig, 3/4 scint and cer single </li>
       *  <li> 4 is pedestal pulser </li>
       *  <li> 5 is b2trig(Bigcal AND Cer sum of 8 mirrors),pi0trig(2/4 of Bigcal quadrants),b1trig(Bigcal and ( or for the 3 sum of 4 mirrors) </li>
       *  <li> 6 is ctrig,  3/4 scint in hms and b1trig </li>
       *  <li> 7 is HMScosmics,  3/4 scint in hms and b1trig </li>
       *  <li> 8 is LED pulser,  either the cherenkov or bigcal LED pulser </li>
       *  <li> 131 is Epics, at 30 sec intervals </li>
       *  <li> 132 is Epics, at 2 sec intervals </li>
       *  </ul>
       */
      Int_t           fCodaType;

      /**
       *  gen_event_trigtype[i] or fTriggerBits[i]:
       *  <ul>
       *  <li> i=0 is HMS</li>
       *  <li> i=1 is b1trig</li>
       *  <li> i=2 is pi0trig</li>
       *  <li> i=3 is b2trig</li>
       *  <li> i=4 is ctrig</li>
       *  <li> i=5 is h2trig-cosmics</li>
       *  <li> i=6 is LED</li>
       *  <li> i=7 is ped</li>
       *  </ul>
       */
      Bool_t           fTriggerBits[12];
      // old
      Int_t           gen_event_type;
      Int_t           gen_event_class;
      Int_t           gen_event_roc_summary;
      Int_t           gen_event_sequence_n;

      Int_t           fTriggerTDC;
      Double_t        fTriggerTime;
      Double_t         T_trghms;
      Double_t         T_trgbig;
      Double_t         T_trgpi0;
      Double_t         T_trgbeta;
      Double_t         T_trgcoin1;
      Double_t         T_trgcoin2;
      Double_t        dtime_p;
      Double_t        dtime_n;
      Double_t        polarea;
      Double_t        polarization;

      InSANETriggerEvent& operator=(const InSANETriggerEvent &rhs) {
         // Check for self-assignment!
         if (this == &rhs)      // Same object?
            return *this;        // Yes, so skip assignment, and just return *this.
         // Deallocate, allocate new space, copy values...
         fEventNumber = rhs.fEventNumber;
         fRunNumber   = rhs.fRunNumber;
         fCodaType    = rhs.fCodaType;
         for (int i = 0; i < 12 ; i++) fTriggerBits[i] = rhs.fTriggerBits[i];
         gen_event_type        = rhs.gen_event_type;
         gen_event_class       = rhs.gen_event_class;
         gen_event_roc_summary = rhs.gen_event_roc_summary;
         gen_event_sequence_n  = rhs.gen_event_sequence_n;
         fTriggerTDC           = rhs.fTriggerTDC;
         fTriggerTime          = rhs.fTriggerTime;
         T_trghms              = rhs.T_trghms;
         T_trgbig              = rhs.T_trgbig;
         T_trgpi0              = rhs.T_trgpi0;
         T_trgbeta             = rhs.T_trgbeta;
         T_trgcoin1            = rhs.T_trgcoin1;
         T_trgcoin2            = rhs.T_trgcoin2;
         dtime_p               = rhs.dtime_p;
         dtime_n               = rhs.dtime_n;
         polarea               = rhs.polarea;
         polarization          = rhs.polarization;
         return *this;
      }

      void ClearEvent(Option_t * opt = ""){
         //      if(InSANERunManager::GetRunManager()->fVerbosity > 2) Print();
         InSANEDetectorEvent::ClearEvent(opt);
         fCodaType = 0;
         for (int i = 0; i < 12 ; i++) fTriggerBits[i] = 0;
         gen_event_type = -99;
         gen_event_class = -99;
         gen_event_roc_summary = -99;
         gen_event_sequence_n = -99;
         fTriggerTDC = 0;
         fTriggerTime = 0;
         T_trghms = 0;
         T_trgbig = 0;
         T_trgpi0 = 0;
         T_trgbeta = 0;
         T_trgcoin1 = 0;
         T_trgcoin2 = 0;
         dtime_p = 0;
         dtime_n = 0;
         polarea = 0;
         polarization = 0;
      }

      /** Returns true for BETA2 , BETA1 and PI0 events */
      Bool_t IsBETAEvent() const {
         return(fCodaType == 5 && (fTriggerBits[2] || fTriggerBits[1] || fTriggerBits[3]) && (!HasPedestalBit()));
      };

      /** Returns true for BETA2  events */
      Bool_t IsBETA2Event() const {
         return(fCodaType == 5 && (fTriggerBits[3]));
      };

      /** Returns true for BETA1 events */
      Bool_t IsBETA1Event() const {
         return(fCodaType == 5 && (fTriggerBits[1]));
      };

      /** Returns true for pedestal events */
      Bool_t IsPedestalEvent()const {
         return(fCodaType == 4 && fTriggerBits[7]);
      };

      /** Returns true for Pi0 eventss */
      Bool_t IsNeutralPionEvent() const {
         return(fCodaType == 5 && fTriggerBits[2]  && !HasPedestalBit());
      };

      /** Returns true for pi0 events */
      Bool_t IsPi0Event() const {
         return(fCodaType == 5 && fTriggerBits[2]);
      };

      /** Returns true for hms events */
      Bool_t IsHMSEvent() const {
         return(fCodaType == 1 && fTriggerBits[0]);
      };

      /** Returns true for hms events */
      Bool_t IsHMS1Event() const {
         return(fCodaType == 1 && fTriggerBits[0]);
      };

      /** Returns true for hms events */
      Bool_t IsHMS2Event() const {
         return(fCodaType == 7 && fTriggerBits[5]);
      };



      /** Since there is no trigger logic for charged pions, charged Pion class should be inherited and defined (through analysis).  */
      //  Bool_t IsChargedPionEvent() {return(fCodaType == 4 && fTriggerBits[7]);  };

      /** Returns true for BETA2 or BETA1 events */
      Bool_t IsLEDPulserEvent() const {
         return(fCodaType == 8 && fTriggerBits[6]);
      }
      Bool_t IsLEDEvent() const {
         return(IsLEDPulserEvent());
      }


      /** Returns true for coincidence triggered events */
      Bool_t IsCoincidenceEvent() const {
         return(fCodaType == 6 && fTriggerBits[4]);
      };
      /** Returns true for coincidence triggered events */
      Bool_t IsCoinEvent() const {
         return(fCodaType == 6 && fTriggerBits[4]);
      };


      /** Returns true for BETA2 or BETA1 triggered eventss */
      Bool_t HasBETABit() const {
         return(fTriggerBits[1] || fTriggerBits[3]);
      };

      /** Returns true for pedestal triggered events */
      Bool_t HasPedestalBit() const {
         return(fTriggerBits[7]);
      };

      /** Returns true for BETA2 or BETA1 eventss */
      Bool_t HasNeutralPionBit() const {
         return(fTriggerBits[2]);
      }

      /** Since there is no trigger logic for charged pions, charged Pion class should be inherited and defined (through analysis).  */
      //  Bool_t IsChargedPionEvent() const {return(fCodaType == 4 && fTriggerBits[7]);  };

      /** Returns true for BETA2 or BETA1 eventss */
      Bool_t HasLEDPulserBit() const {
         return(fTriggerBits[6]);
      }

      /** Returns true for BETA2 or BETA1 eventss */
      Bool_t HasCoincidenceBit() const {
         return(fTriggerBits[4]);
      }

      /** Returns true for EPICS events */
      Bool_t IsEpicsEvent() const {
         return(fCodaType == 131 || fCodaType == 132);
      }

      /** Returns true for Scaler events */
      Bool_t IsScalerEvent() const {
         return(fCodaType == 0);
      }

      /** Returns true for BETA2 or BETA1 eventss */
      Bool_t IsPedestalCorrectable() const {
         if (fCodaType == 5) return(true);
         else if (fCodaType == 1) return(true);
         else if (fCodaType == 6 || fCodaType == 7 || fCodaType == 8) return(true);
         else return(false);
      }

      /** Returns true for Scaler events */
      Bool_t IsClusterable() const {
         if (fCodaType == 5) return(kTRUE);
         else if (fCodaType == 1) return(kTRUE);
         else if (fCodaType == 6) return(kTRUE);
         else return(kFALSE);
      }

      /** print for event display */
      virtual const char * PrintEvent() const {
         TString astr = "";
         astr += " Event:" ;

         astr += fEventNumber;

         astr += " fCodaType=" ;
         astr += fCodaType;
         astr += " pi0=";
         astr += fTriggerBits[2];
         /*    astr+= "\n";*/
         astr += " beta1=";
         astr += fTriggerBits[1];
         /*    astr+= "\n";*/
         astr += " beta2=" ;
         astr += fTriggerBits[3] ;
         /*    astr+= "\n";*/
         astr += " hms=" ;
         astr += fTriggerBits[0] ;
         astr += "\n";
         std::cout << astr.Data();
         return(astr.Data());
      };

      /** print for event display */
      virtual void Print(Option_t * opt = "") const {
         std::cout << "  ++ InSANETriggerEvent " << this << "\n";;
         std::cout << " Run:" ;
         std::cout << fRunNumber;
         std::cout << " Event:" ;
         std::cout << fEventNumber;
         std::cout << " fCodaType=" ;
         std::cout << fCodaType;
         std::cout << " pi0=";
         std::cout << fTriggerBits[2];
         /*    astr+= "\n";*/
         std::cout << " beta1=";
         std::cout << fTriggerBits[1];
         /*    astr+= "\n";*/
         std::cout << " beta2=" ;
         std::cout << fTriggerBits[3] ;
         /*    astr+= "\n";*/
         std::cout << " hms=" ;
         std::cout << fTriggerBits[0] ;
         std::cout << "\n";
         /*    std::cout << astr.Data();*/
         /*    return(astr.Data());*/
      }

      virtual void PrintSummary() {
         std::cout << "  Run:"  << fRunNumber
            << "    Event:" << fEventNumber << std::endl;
         std::cout << "         IsBETAEvent = " << IsBETAEvent() << std::endl;
         std::cout << "        IsBETA2Event = " << IsBETA2Event() << std::endl;
         std::cout << "        IsBETA1Event = " << IsBETA1Event() << std::endl;
         std::cout << "         IsCoinEvent = " << IsCoinEvent() << std::endl;
         std::cout << "          IsHMSEvent = " << IsHMSEvent() << std::endl;
         std::cout << "   HasCoincidenceBit = " << HasCoincidenceBit() << std::endl;
         std::cout << "  IsNeutralPionEvent = " << IsNeutralPionEvent() << std::endl;
         std::cout << "          IsPi0Event = " << IsPi0Event() << std::endl;
         std::cout << "   HasNeutralPionBit = " << HasNeutralPionBit() << std::endl;
         std::cout << "        IsEpicsEvent = " << IsEpicsEvent() << std::endl;
         std::cout << "       IsScalerEvent = " << IsScalerEvent() << std::endl;
         std::cout << "       IsClusterable = " << IsClusterable() << std::endl;
         std::cout << "    IsLEDPulserEvent = " << IsLEDPulserEvent() << std::endl;
         std::cout << "     HasLEDPulserBit = " << HasLEDPulserBit() << std::endl;
      }

   private :
      /*
         void  Clear(Option_t *);*/

      ClassDef(InSANETriggerEvent, 2)
};


#endif

