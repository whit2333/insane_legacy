#ifndef InSANERun_h
#define InSANERun_h
#include <cstdlib>
#include <TROOT.h>
#include "TNamed.h"
#include <iostream>
#include <iomanip>
#include <TSQLServer.h>
#include <TSQLStatement.h>
#include <TString.h>
#include <TDatime.h>
#include "TObjArray.h"
#include "TH2F.h"
#include "TClonesArray.h"
#include "TList.h"
#include "InSANERunSummary.h"
#include "InSANERunFlag.h"
#include "InSANEDilutionFactor.h"
#include "TBrowser.h"
#include <vector>


/** Result to be saved to run.
 * Should be inherited for added SetValue() complexity
 * Inside of an implementation of say, InSANECalculation, although it could be almost anywhere,
 * you would have something like this:
 * \code
 *  InSANERunManager * runMan = InSANERunManager::GetRunManager(); // assuming run is set already
 *  InSANEResult * result = new InSANEResult("number_of_events","Number of Events");
 *  result->SetValue(1234);
 *  result->SetSummary("Count of all events passed through analyzer");
 *  runMan->AddResult(result);
 * \endcode
 *
 * \todo make a separate MySQL table  for results
 */
class InSANEResult : public TNamed {
public:
   InSANEResult(const char * name = "aResult", const char * title = "A Result"):
      TNamed(name, title) {
      fSummary = "";
      fValue = "0.0";
   }
   ~InSANEResult() {}

public:
   void      SetValue(Double_t val) {
      fValue = Form("%f", val);
   }
   void      SetSummary(const char * sum) {
      fSummary = sum;
   }
   Double_t  GetNumericalValue() {
      return atof(fValue.Data());
   }
   /** returns the result in string form.
    *  Note that this is in string form since it is usually printing
    */
   const char * GetResult() {
      return fValue.Data();
   }

protected:
   TString fValue;
   TString fSummary;

   ClassDef(InSANEResult, 1)
};


/** Base class for a Run Object.
 *
 *  The run object is used internally, while the run report is used externally (database
 *  interaction, webpage i/o).
 *  It should be able to completely configure all functions once a run is properly loaded.
 *
 *  \todo In InSANEAnalysisManager add method like ConfigureForRun(InSANERun*)
 *
 * \ingroup Runs
 */
class InSANERun : public TNamed {

   public:
      InSANERun(Int_t runnum = 0);
      InSANERun(const InSANERun& run) : TNamed(run) {
         (*this) = run;
      }
      virtual ~InSANERun();
      InSANERun& operator=(const InSANERun& rhs);

      Bool_t IsFolder() const {
         return kTRUE;
      }

      void Browse(TBrowser* b) {
         b->Add(&fAsymmetries, "Asymmetries");
         b->Add(&fApparatus, "Apparatus");
         b->Add(&fResults, "Results");
         b->Add(&fDilution, "Dilution");
         b->Add(&fRunFlags, "Flags");
         b->Add(&fDetectors, "Detectors");
      }

      /** @name Basic Usage
       *  Basic functions for a run
       *  @{
       */
      void     ResetRun(Int_t pass = 0) {
         fAnalysisPass = pass;
      }

      Int_t    GetRunNumber() {
         return(fRunNumber);
      }

      void     SetRunNumber(Int_t rn) {
         fRunNumber = rn;
      }

      void SetBeamEnergy(Double_t en) { fBeamEnergy = en; }
      Double_t GetBeamEnergy() { return(fBeamEnergy); }

      /** Initializes all data by searching various databases and files */
      void     Clear(Option_t * opt = "");

      /** Clears the various event counters */
      void     ClearCounters();

      void     Print(Option_t * opt = "") const ; // *MENU*
      //@}


      /** @name Run Flags and Quality
       *  Run Flags, quality checks and filtering
       *  @{
       */

      /** Returns true if the run is not a dud */
      Bool_t    IsValid();

      /// should be true if a flag is raised during the run
      Bool_t    fIsFlagged;

      /** returns true if a flag has been raised at some point in the run */
      Bool_t    IsFlagged() {
         return(fIsFlagged);
      }

      /** Only at the begining of an analysis
       */
      void      ClearFlags() {
         fRunFlags.Clear();
      }
      TList *   GetRunFlags() {
         return &fRunFlags;
      }
      void      AddRunFlag(InSANERunFlag * flag) {
         if (flag) fRunFlags.Add(flag);
      }
      TList     fRunFlags;
      //@}

   public:

      /** @name Analysis Results
       *   Analysis Results
       *  @{
       */

      TList fResults;

      void AddResult(InSANEResult* res) {
         fResults.Add(res);
      }

      TList * GetResults() {
         return &fResults;
      }

      TObjArray fAsymmetries;

      Double_t    fTotalAsymmetry;
      Int_t       fTotalNPlus;
      Int_t       fTotalNMinus;
      Double_t    fTotalQPlus;
      Double_t    fTotalQMinus;
      Double_t    fDeltaNPlus;
      Double_t    fDeltaNMinus;
      Double_t    fTotalQ;

      //InSANERunSummary fRunSummary;

      InSANERunSummary GetRunSummary(){
         InSANERunSummary fRunSummary;
         fRunSummary.fRuns.push_back(fRunNumber);
         fRunSummary.fBeamEnergy = fBeamEnergy;
         fRunSummary.fNPlus   = fTotalNPlus;
         fRunSummary.fNMinus  = fTotalNMinus;
         fRunSummary.fLPlus   = fLiveTimePlus;
         fRunSummary.fLMinus  = fLiveTimeMinus;
         fRunSummary.fQPlus   = fTotalQPlus;
         fRunSummary.fQMinus  = fTotalQMinus;
         fRunSummary.fPbSign  = TMath::Sign(1.0,fBeamPolarization);
         fRunSummary.fPtSign  = TMath::Sign(1.0,fTargetOfflinePolarization);
         fRunSummary.fPBeam   = TMath::Abs(fBeamPolarization);
         fRunSummary.fPTarget = TMath::Abs(fTargetOfflinePolarization);
         fRunSummary.fHalfWavePlate   = fHalfWavePlate;
         fRunSummary.fPackingFraction = fPackingFraction;

         // in case there is no offline polarization
         if( fTargetOfflinePolarization == 0.0 || TMath::Abs(fTargetOfflinePolarization) < 0.1 || TMath::Abs(fTargetOfflinePolarization) > 100.0 ) {
            fRunSummary.fPTarget = TMath::Abs(fTargetOnlinePolarization);  
            fRunSummary.fPtSign  = TMath::Sign(1.0,fTargetOnlinePolarization);
         }
         return(fRunSummary);
      }

      //@}


      /** @name Run Meta Data
       *  Meta data
       *  @{
       */
      TDatime   fStartDatetime;
      TDatime   fEndDatetime;
      TDatime   fReplayDatetime;
      Int_t     fRunNumber;
      mutable TDatime   fTimeDuration;
      mutable Double_t  fDuration;
      //@}

      /** @name DAQ Data
       *  DAQ
       *  @{
       */
      Int_t      fTriggers[12];        //-> Trigger Supervisor bits summed over run
      Int_t      fNumberOfScalerReads;
      Double_t   fDeadTime;
      Double_t   fDeadTimePlus;
      Double_t   fDeadTimeMinus;
      Double_t   fLiveTime;
      Double_t   fLiveTimePlus;
      Double_t   fLiveTimeMinus;
      Int_t      fNEvents;
      Int_t      fNCoinEvents;
      Int_t      fNBeta2Events;
      Int_t      fNBeta1Events;
      Int_t      fNPi0Events;
      Int_t      fNLedEvents;
      Int_t      fNHmsEvents;
      Int_t      fNPedEvents;
      //@}


      /** @name Data Analysis Progress
       *  Data Analysis Progress
       *  @{
       */
      Int_t fPedestalsAnalyzed ;
      Int_t fTimingAnalyzed ;
      /// Analysis Pass last competed
      Int_t fAnalysisPass;
      //@}


      /** @name Detector Run Data
       *  Detector run information/results
       *  @{
       */
      TList     fApparatus;
      TList     fDetectors;
      Bool_t    fIronPlate;
      TObjArray fCherenkovTimeWalkFits;
      TObjArray fBigcalTimeWalkFits; 
      TH2F *    hAsymmetries; //! Transient
      Int_t     fBigcalCalibrationSeries;

      void GetHistograms() {
         hAsymmetries = nullptr;
         hAsymmetries = (TH2F*) gROOT->FindObject("Asymmetries");
         if (!hAsymmetries) hAsymmetries = new TH2F("Asymmetries", "Asymmetries", 20, 0.0, 1.0, 6, 1.0, 7.0);
      }
      void ClearAsymmetry() {
         fTotalNPlus = 0;
         fTotalNMinus = 0;
         fTotalQPlus = 0;
         fTotalQMinus = 0;
         fTotalAsymmetry = 0;
         fDeltaNPlus = 0;
         fDeltaNMinus = 0;
         fTotalQ = 0;
      }
      //@}

      /** @name Beam Data
       *  Electron Beam
       *  @{
       */
      Double_t    fBeamEnergy;
      Double_t    fBeamPolarization;
      Double_t    fAverageBeamCurrent;
      Int_t       fHalfWavePlate;
      Int_t       fBeamPassNumber;
      Double_t    fWienAngle;
      Double_t    fQuantumEfficiency;
      Double_t    fAverageBeamPolarization;
      Double_t    fBCM1ChargeAsymmetry;
      Double_t    fBCM1GatedPlusCharge;
      Double_t    fBCM1GatedMinusCharge;
      Double_t    fBCM2ChargeAsymmetry;
      Double_t    fBCM2GatedPlusCharge;
      Double_t    fBCM2GatedMinusCharge;
      Int_t       fBeamTrips;
      //@}

      /** @name Target Data
       *  Target Run data
       *  @{
       */
   public:
      typedef enum  { kNH3, kCARBON, kCROSSHAIR, kCARBON_PLUS_HE, kHOLE, kCROSSHAIR_PLUS_HE , kUNKNOWN } SANETargets;
      typedef enum  { kANTIPARALLEL, kPERPENDICULAR, kNOFIELD } SANETargetOrientation;
      typedef enum  { kTOP, kBOTTOM, kNONE , kNA} SANETargetCup;
      typedef enum  { kPOSITIVE, kNEGATIVE , kUNPOLARIZED} SANETargetSign;

      Double_t        fTargetOfflinePolarization;
      Double_t        fTargetOnlinePolarization;
      Double_t        fAverageTargetPolarization;

      SANETargetSign  fTargetPolarizationSign;
      SANETargets     fTarget;
      SANETargetCup   fTargetCup;
      SANETargetOrientation fTargetOrientation;

      Double_t        fStartingPolarization;
      Double_t        fEndingPolarization;
      Int_t           fPreScale[15];
      Double_t        fTargetAngle;  ///< degrees
      Double_t        fTargetField;
      Double_t        fTargetCurrent;
      Double_t        fPackingFraction;

      void SetTargetAngle(Double_t angle) { fTargetAngle = angle/degree; }
      Double_t GetTargetAngle() {  return( fTargetAngle*degree ); }

      Int_t           fFirstEvent;
      Int_t           fLastEvent;

      SANETargets *   fTargetByNumber; //!

      static const SANETargets kgSANETargets[7];
      static const SANETargetCup kgSANETargetCups[4];
      static const SANETargetSign kgSANETargetSign[3];
      static const SANETargetOrientation kgSANETargetOrientations[3];
      static const char * kgSANETargetNames[];
      static const char * kgSANETargetCupNames[];
      static const char * kgSANETargetSignNames[];
      static const char * kgSANETargetOrientationNames[];
      //@}

      /** @name HMS Data
       *  HMS Run data
       *  @{
       */
      Double_t fHMSMomentum;
      Double_t fHMSAngle;
      Double_t fHMSSign;
      //@}

      /** @name Dilution
       *   Dilution Factors for this run
       *  @{
       */
      TList    fDilution;
      void     AddDilutionFunction(InSANEDilutionFunction * df) {
         fDilution.Add(df);
      }
      TList *  GetDilutionFunctions() {
         return &fDilution;
      }
      //@}


      ClassDef(InSANERun,21)
};
#endif

