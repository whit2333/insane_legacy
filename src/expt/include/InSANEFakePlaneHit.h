#ifndef InSANEFakePlaneHit_HH
#define InSANEFakePlaneHit_HH 1

#include "InSANEDetectorHit.h"
#include "TVector3.h"
#include "TLorentzVector.h"

/** Concrete implementation of InSANEDetectorHit for montecarlo data from an imaginary detector plane
 *
 * \ingroup Hits
 *
 * \ingroup Detectors
 */
class InSANEFakePlaneHit : public InSANEDetectorHit {

   public:
      Int_t             fPID; ///< PID using PDG encoding
      TVector3          fPosition;
      TVector3          fLocalPosition;
      TVector3          fMomentum;
      TLorentzVector    fMomentum4Vector;

      Double_t fTheta, fPhi, fEnergy;

   public :
      InSANEFakePlaneHit();
      virtual ~InSANEFakePlaneHit();
      InSANEFakePlaneHit(const InSANEFakePlaneHit& rhs);
      InSANEFakePlaneHit& operator=(const InSANEFakePlaneHit &rhs);

      Int_t GetPdgCode() const ;
      void  Clear(Option_t * opt = "");

      Bool_t  IsEqual(const TObject *obj) const { return fEnergy == ((InSANEFakePlaneHit*)obj)->fEnergy; }
      Bool_t  IsSortable() const { return kTRUE; }
      Int_t   Compare(const TObject *obj) const { if (fEnergy > ((InSANEFakePlaneHit*)obj)->fEnergy)
         return 1;
         else if (fEnergy < ((InSANEFakePlaneHit*)obj)->fEnergy)
            return -1;
         else
            return 0;
      }

      ClassDef(InSANEFakePlaneHit,4)
};


#endif

