#ifndef SANELuciteHodoscopeAnalysis_H
#define SANELuciteHodoscopeAnalysis_H
#include "TROOT.h"
#include "InSANEDetectorAnalysis.h"

#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "SANEEvents.h"
#include "BigcalEvent.h"
#include "InSANEDatabaseManager.h"
#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"


/**  Analysis for Lucite Hodoscope alone.
 *
 * \ingroup Analyses
 */
class SANELuciteHodoscopeAnalysis : public InSANEDetectorAnalysis {
public :

   SANELuciteHodoscopeAnalysis(const char * sourceTreeName = "correctedBetaDetectors");

   ~SANELuciteHodoscopeAnalysis();

   virtual Int_t AnalyzeRun();

   virtual Int_t Visualize() ;
   /**
    *  Define histograms to fill inside loop
    */
   virtual Int_t  AllocateHistograms() ;

/// /// HISTOGRAMS ///////
   TH1F * luc_tdc_hist_pos[28];
   TH1F * luc_adc_hist_pos[28];
   TH2F * luc_tdc_vs_adc_hist_pos[28];
   TH1F * luc_tdc_hist_neg[28];
   TH1F * luc_adc_hist_neg[28];
   TH2F * luc_tdc_vs_adc_hist_neg[28];
   TH1F * luc_tdc_hist_diff[28];

   TH1F * cer_tdc_hist_luc_pos[28*12];
   TH1F * cer_adc_hist_luc_pos[28*12];
   TH2F * cer_tdc_vs_adc_hist_luc_pos[28*12];

   TH1F * cer_tdc_hist_luc_neg[28*12];
   TH1F * cer_adc_hist_luc_neg[28*12];
   TH2F * cer_tdc_vs_adc_hist_luc_neg[28*12];
   TH2F * luc_bar_hit[28];

   SANEEvents * fEvents;

   ClassDef(SANELuciteHodoscopeAnalysis, 3);
};


#endif

