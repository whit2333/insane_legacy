#ifndef InSANESkimmerFilter_HH
#define InSANESkimmerFilter_HH

#include "TNamed.h"
#include "TBranch.h"
#include "TList.h"
#include "InSANEScalerEvent.h"
#include "InSANERunFlag.h"
#include "InSANERunManager.h"
#include "InSANEDatabaseManager.h"

/** ABC Filtering Primitive for the Skimmer
 */
class InSANESkimmerFilter : public TNamed {

   protected:
      InSANEScalerEvent * fScalerEvent;
      enum InSANEFilterState {
         kFailsFilter,
         kPassesFilter
      } fFilterState;

      Bool_t              fIsFlagRaised;
      InSANERunFlag     * fRunFlag;
      Bool_t              fIsSkipFlag;

   public:

      /** A SkimmerFilter, concrete examples would be...
       *  InSANETrip-> InSANEBeamTrip, InSANEDetectorHVTrip
       */
      InSANESkimmerFilter(const char * n = "", const char * t = "");
      virtual ~InSANESkimmerFilter();

      virtual int  Process()       = 0; ///< Process the filter in its current state
      virtual void CreateRunFlag() = 0;

      /** Initialize this filter by clearing existing database flags (if any). */
      virtual void Initialize();

      /** Deletes from database all run flags with the same title 
       *  (run_flags.event_title) and run number.
       */
      virtual void DeleteExistingFlags();

      void                SetScalerEvent(InSANEScalerEvent * ev);
      InSANEScalerEvent * GetScalerEvent() const ;
      Bool_t              IsSkipFlag() const ;
      Bool_t              IsEventFlaggedSkip() const ;
      InSANERunFlag *     RaiseRunFlag();
      InSANERunFlag *     LowerRunFlag();

      ClassDef(InSANESkimmerFilter,2)
};


/** Beam current filter. 
 *  For detecting beam trips.
 */
class InSANEBeamCurrentFilter : public InSANESkimmerFilter {

   protected:
      Int_t    fFlagNumber;
      Double_t fCurrentThreshold;

   public:
      InSANEBeamCurrentFilter(const char * n = "", const char * t = "");
      virtual ~InSANEBeamCurrentFilter();
      virtual void CreateRunFlag();
      virtual int Process();

      ClassDef(InSANEBeamCurrentFilter, 2)
};

#endif
