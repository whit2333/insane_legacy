#ifndef InSANEClusterAnalysis_H
#define InSANEClusterAnalysis_H

#include "SANEScalers.h"
#include "InSANEAnalysis.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "SANEEvents.h"
#include "BigcalEvent.h"
#include "SANERunManager.h"
#include "InSANEDatabaseManager.h"
#include "InSANEClusterEvent.h"
#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"
#include "BIGCALClusterProcessor.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"
#include "SANERunManager.h"

/** A cluster analysis class
 *
 * \ingroup Analyses
 */
class InSANEClusterAnalysis : virtual public InSANEAnalysis {
   protected:

      BIGCALClusterProcessor * fClusterProcessor;
      InSANEClusterEvent     * fClusterEvent;
      TClonesArray           * fClusters;
      TTree  * fClusterTree;

   public :
      InSANEClusterAnalysis(const char * sourceTreeName = "Clusters");
      virtual ~InSANEClusterAnalysis();

      /** Set the cluster processor to use for analysis */
      void SetClusterProcessor(BIGCALClusterProcessor*cp) {
         fClusterProcessor = cp;
         if (fClusterEvent)fClusterEvent->fClusterSourceHistogram = fClusterProcessor->fSourceHistogram;
      }

      InSANEClusterProcessor * GetClusterProcessor() {
         return fClusterProcessor;
      };

      /** Starts the loop over events and fills histograms
       */
      virtual Int_t AnalyzeRun();

      /**
       */
      virtual Int_t Visualize() ;

      /** Allocates and defines histograms
       */
      virtual Int_t  AllocateHistograms() {
         return(0) ;
      }

      /** Set the cluster output tree. This method also sets the branches.
       *  @deprecated The cluster tree is now the set by either SANEEvents or in an InSANECalculation
       */
      Int_t SetClusterTree(TTree * t);


      ClassDef(InSANEClusterAnalysis, 1);
};


#endif

