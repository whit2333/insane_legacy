#ifndef Pi0Event_h
#define Pi0Event_h

#include "TClonesArray.h"
#include "InSANEPhysicsEvent.h"
#include "TMath.h"
#include "InSANETrajectory.h"
#include "InSANEReconstruction.h"
#include "Pi0ClusterPair.h"
#include "TVector3.h"
#include "Pi0ClusterPair.h"

/**  Event class for pi0 mass reconstruction
 *
 *   mpi0 = Sqrt( 2*E1*E2*(1-Cos(theta12)) )
 *
 *   \ingroup pi0
 */
class Pi0Event : public InSANEPhysicsEvent {

   public:

      Int_t          fNumberOfPairs;
      Bool_t         fCherenkovHit;

      TClonesArray * fClusterPairs;   //->
      TClonesArray * fReconstruction; //->

   public:
      Pi0Event();
      virtual ~Pi0Event();
      virtual void Reset();

      void ClearEvent(Option_t * opt = "");

      /** print for event display */
      const char * PrintEvent() const ;

      virtual void Print(Option_t * opt = "") const ;

      /**  Calaculate the mass of the assumed pi0 */
      Double_t CalculateMass(Double_t E1, Double_t E2, Double_t theta) ;

      ClassDef(Pi0Event,6)
};

#endif



