#ifndef InSANETrackPropagator_HH
#define InSANETrackPropagator_HH 1

#include "TObject.h"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TEveManager.h"
#include "TEveTrack.h"
#include "TEveTrackPropagator.h"
#include "TParticle.h"
#include "UVAOxfordMagneticField.h"
#include "InSANEMagneticField.h"
#include "TEveViewer.h"
#include "TGLUtil.h"
#include "TGLViewer.h"
#include "TEveTrans.h"
/** Used to propagate (backwards) a vector through a magnetic field.
 *  The goal is to send the reconstructed particle backwards and get the
 *  position and scattering angle at the target.
 */
class InSANETrackPropagator : public TEveTrackPropagator {
private:
   
   /** Needed because TEveTrackPropagator::DistributeOffset is private. */
   void DistributeOffset(const TEveVectorD& off, Int_t first_point, Int_t np, TEveVectorD& p);

protected:
   TEveTrack *             fTrack;
   //TEveTrackPropagator*    fProp;
   TParticle *             fParticle;

   TEveMagField*           fEveMagField;
   InSANEMagneticField*    fMagField;

   TEveVectorD             fArbPlaneNormal;  /// Normal to intersection plane
   TEveVectorD             fArbPlanePoint;   /// Apoint on the plane
   TEveVectorD             fArbPlaneResult;  /// Resulting point of intersection

   TEveVectorD             fZXPlaneNormal;  /// Normal to intersection plane
   TEveVectorD             fZXPlanePoint;   /// Apoint on the plane
   TEveVectorD             fZXPlaneResult;  /// Resulting point of intersection

   TEveVectorD             fYZPlaneNormal;  /// Normal to intersection plane
   TEveVectorD             fYZPlanePoint;   /// Apoint on the plane
   TEveVectorD             fYZPlaneResult;  /// Resulting point of intersection

   TEveVectorD             fXYPlaneNormal;  /// Normal to intersection plane
   TEveVectorD             fXYPlanePoint;   /// Apoint on the plane
   TEveVectorD             fXYPlaneResult;  /// Resulting point of intersection

   TEveManager *           fEveManager;     /// Eve manager singleton
   TEveTrackList *         fTrackList;      /// Used for drawing track.

   /** Same as TEveTrackPropagator::LoopToVertex, but with add
    *  
    */
   Bool_t  LoopToVertex(TEveVectorD& v, TEveVectorD& p);

public:

   TEveVectorD             fMomentum;     /// Current momentum
   TEveVectorD             fVertex;       ///  Current vertex
   TEveVectorD             fOrigin;       ///  Current vertex

   InSANETrackPropagator();
   ~InSANETrackPropagator();

   /** Set the polariztion direction angle in degrees */
   void SetTargetPolAngle(Double_t angle = 180.0){
      fMagField->SetPolarizationAngle(TMath::Pi()*angle/180.0);
   }


   /** The track momentum and position.
    */
   void SetTrackMomentumPosition(TVector3 p, TVector3 v/*,bool AddToTrackList = false*/) {
      ResetTrack();
      fParticle->SetMomentum(-1.0*p.X(),-1.0*p.Y(),-1.0*p.Z(),TMath::Sqrt(p.Mag2()+0.000511*0.000511) );
      fParticle->SetProductionVertex(v.X(),v.Y(),v.Z(),0.0);
//      if(fTrack/* && !AddToTrackList*/) delete fTrack;
   /// In order to avoid a memory leak since I cannot call delete on this object without a crash...
   /// I use the new with placement to reuse the memory.
       fTrack = new(fTrack) TEveTrack(fParticle,0,this);
   }

   void GetMomentumPositionAtTarget(TVector3 * p, TVector3 * v) {
      /// Here we are doing something like TEveTrack::MakeTrack ...
      TEveVectorD p0 = fTrack->GetMomentum();
      fV = fTrack->GetVertex();
      ResetTrack();
//       fTrack->MakeTrack();
      InitTrack(fTrack->GetVertex(), fTrack->GetCharge());
      Update(fV, p0, kTRUE,kTRUE);
      LoopToVertex( p0,fOrigin);
      FillPointSet(fTrack);
      ResetTrack();
      p->SetXYZ(fMomentum.fX,fMomentum.fY,fMomentum.fZ);
      v->SetXYZ(fVertex.fX,fVertex.fY,fVertex.fZ);
   }

   void GetPositionOnPlane(TVector3& n, TVector3& p, TVector3& r){
      ResetTrack();
      fTrack->MakeTrack();
      TEveVectorD p0 = fTrack->GetMomentum();
      fArbPlaneNormal.Set(n.X(),n.Y(),n.Z());
      fArbPlanePoint.Set(p.X(),p.Y(),p.Z());
      IntersectPlane(p0,fArbPlanePoint,fArbPlaneNormal,fArbPlaneResult);
      r.SetXYZ(fArbPlaneResult.fX,fArbPlaneResult.fY,fArbPlaneResult.fZ);
      ResetTrack();
   }

   /** Prints the Intersection points.
    */
   void PrintIntersections();

   /** Invokes the TEveManager window and  draws trackd
    */
   void DrawTrack();

ClassDef(InSANETrackPropagator,1)
};


#endif
