/** \file ForwardTrackerHit.h
 *
 * @author Whitney Armstrong (whit@temple.edu)
 *
 */
#ifndef ForwardTrackerHit_h
#define ForwardTrackerHit_h

#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include "InSANEDetectorHit.h"
#include "InSANERunManager.h"

/** Concrete class for a Forward Tracker hit
 *
 *  - fScintLayer=0 ==>  X1
 *  - fScintLayer=1 ==>  Y1
 *  - fScintLayer=2 ==>  Y2
 *
 * \ingroup Hits
 * \ingroup tracker
 */
class ForwardTrackerHit : public InSANEDetectorHit {
   public:
      Int_t     fScintLayer;         // Scintillator layer. This can be 1, 2, or 0 corresponding to Y1, Y2 and X1 respectively
      Int_t     fRow;                // Row number for given scintillator layer, fScintLayer
      Int_t     fTDC;                // TDC value for hit
      Int_t     fTDCAlign;           // TDC value for hit
      Double_t  fPosition;           // The value of the position
      Int_t     fPositionCoordinate; // 1,2,3 ==> "x", "y","z"

   public :

      ForwardTrackerHit() {
         Clear();
      }

      ~ForwardTrackerHit() { };

      ForwardTrackerHit(const ForwardTrackerHit& rhs) : InSANEDetectorHit(rhs) {
         (*this) = rhs;
      }

      void  Clear(Option_t *opt = "") {
         InSANEDetectorHit::Clear(opt);
         fScintLayer = -1;
         fRow = -1;
         fTDC = -9999;
         fTDCAlign = -9999;
         fPosition = -9999.9;
         fPositionCoordinate = -1;
      }

      ForwardTrackerHit& operator=(const ForwardTrackerHit &rhs) {
         // Check for self-assignment!
         if (this == &rhs)      // Same object?
            return *this;        // Yes, so skip assignment, and just return *this.
         // Deallocate, allocate new space, copy values...
         InSANEDetectorHit::operator=(rhs);
         fPosition           = rhs.fPosition;
         fScintLayer         = rhs.fScintLayer;
         fTDC                = rhs.fTDC;
         fTDCAlign           = rhs.fTDCAlign;
         fPositionCoordinate = rhs.fPositionCoordinate;
         fRow                = rhs.fRow;
         return *this;
      }

      /*
         ForwardTrackerHit & operator+=(const ForwardTrackerHit &rhs) {
         fLevel += rhs.fLevel;
         fHitNumber += rhs.fHitNumber;
         fPassesTimingCut += rhs.fPassesTimingCut;

         fPosition += rhs.fPosition;
         fScintLayer += rhs.fScintLayer;
         fTDC += rhs.fTDC;
         fTDCAlign += rhs.fTDCAlign;
         fPositionCoordinate += rhs.fPositionCoordinate;
         fRow += rhs.fRow;
         return *this;
         }
       */
      /*
      // Add this instance's value to other, and return a new instance
      // with the result.
      const ForwardTrackerHit operator+(const ForwardTrackerHit &other) const {
      ForwardTrackerHit result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
      result += other;            // Use += to add other to the copy.
      return result;              // All done!
      }
       */

      ClassDef(ForwardTrackerHit, 5)
};
#endif

