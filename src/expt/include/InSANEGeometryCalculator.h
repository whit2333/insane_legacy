#ifndef InSANEGeometryCalculator_h
#define InSANEGeometryCalculator_h

#include <TROOT.h>
// //#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TNamed.h>


/** ABC for detector calculator instance that provides basic geometry details
 *
 * \ingroup Detectors
 * \ingroup Geometry
 */
class InSANEGeometryCalculator : public TNamed {
public :
   InSANEGeometryCalculator() {  }
   virtual  ~InSANEGeometryCalculator() { };

private :
   ClassDef(InSANEGeometryCalculator, 1)
};


#endif
