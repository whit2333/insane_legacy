#ifndef InSANEScalerAnalysis_H
#define InSANEScalerAnalysis_H
#include "SANEScalers.h"

#include "TROOT.h"
#include "InSANEAnalysis.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "SANEEvents.h"
#include "BigcalEvent.h"
#include "SANERunManager.h"
#include "InSANEDatabaseManager.h"

#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"
#include <exception>
#include "SANERunManager.h"

/**
 *
 * \ingroup Analysis
 */
class InSANEScalerAnalysis : public InSANEAnalysis {
public :
   /**
    *  Constructor. Allocates the memory for each InSANEDetector. As well as event memory (through new SANEEvents())
    */
   InSANEScalerAnalysis(const char * sourceTreeName = "saneScalers");

   virtual ~InSANEScalerAnalysis();

   /**
    *  Starts the loop over events and fills histograms
    */
   virtual Int_t AnalyzeRun();

   /**
    *
    */
   virtual Int_t Visualize() ;

   /**
    * Allocates and defines histograms
    */
   virtual Int_t  AllocateHistograms() {
      return(0) ;
   }

   SANEScalers * fSANEScalers;

   /** Detectors for "Detector Analysis" */
   ForwardTrackerDetector *    fTrackerDetector ;
   GasCherenkovDetector *      fCherenkovDetector;
   LuciteHodoscopeDetector *   fHodoscopeDetector ;
   BigcalDetector *            fBigcalDetector    ;

   SANERunManager * fRunManager;

private:

   ClassDef(InSANEScalerAnalysis, 3);
};


#endif

