#ifndef SANEClusteringAnalysis_HH
#define SANEClusteringAnalysis_HH
#include "BIGCALClusterProcessor.h"
#include "InSANEDetectorAnalysis.h"
#include "InSANEClusterAnalysis.h"
#include "InSANEClusterEvent.h"
#include "TClonesArray.h"

/**  Concrete class which does clustering
 *
 *  This class creates an output tree of new clusters.
 *  It should be noted that this class is different from the class InSANEClusterAnalysis in
 *  this manner. That is InSANEClusterAnalysis is for analyzing existing cluster branches.
 *
 *
 * \ingroup Analyses
 */
class SANEClusteringAnalysis : public InSANEDetectorAnalysis,  public InSANEClusterAnalysis {
public:
   /** Clustering Analysis for SANE.
    *
    *   The constructor does the following:
    *    - Calls InSANEDetectorAnalysis::InSANEDetectorAnalysis() c'tor
    *      which creates the event and detector objects
    *    - Calls SANEvents::AddClusterEvents() which creates the
    *      InSANEClusterEvent and TClonesArray (of BIGCALCluster) objects
    *      which will become branches
    *    - Sets data member pointers...
    *
    */
   SANEClusteringAnalysis(const char * sourceTreeName = "correctedBetaDetectors");

   ~SANEClusteringAnalysis();

public:
   /** Execute Analysis */
   virtual Int_t AnalyzeRun() ;

   /** Visualize */
   virtual Int_t Visualize() {
      return(0);
   };

   ClassDef(SANEClusteringAnalysis, 2)
};


#endif


