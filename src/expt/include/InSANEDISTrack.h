#ifndef InSANEDISTrack_H
#define InSANEDISTrack_H 1

#include "BIGCALCluster.h"
#include "TVector3.h"
#include "InSANETrack.h"
#include "InSANEPhysicalConstants.h"

/** Track for an DIS electron.
 *
 * \ingroup Events
 * \ingroup physics
 */
class InSANEDISTrack : public InSANETrack {

   public:
      BIGCALCluster  fCluster;

      Int_t          fNClusters;

      TVector3       fPBigcal_B;
      TVector3       fXBigcal_B;
      TVector3       fPBigcal_C;
      TVector3       fXBigcal_C;

      TVector3       fPTracker_B;
      TVector3       fXTracker_B;
      TVector3       fPTracker_C;
      TVector3       fXTracker_C;

      TVector3       fPTarget_B;
      TVector3       fXTarget_B;
      TVector3       fPTarget_C;
      TVector3       fXTarget_C;

      TVector3       fTrackerMiss;
      Int_t          fNTrackerNeighbors;
      TVector3       fLuciteMiss;
      TVector3       fTrackerY1Miss;
      TVector3       fTrackerY2Miss;
      TVector3       fTargetPosition;
      TVector3       fReconTargetY1;
      TVector3       fReconTargetY2;
      TVector3       fReconTarget;
      TVector3       fReconTarget1;
      TVector3       fReconTarget2;
      TVector3       fTrackerPosition;
      Double_t       fLuciteMissDistance;
      Double_t       fBeamEnergy;
      Double_t       fEnergy;
      Double_t       fTheta;
      Double_t       fPhi;
      Int_t          fHelicity;
      Double_t       fDeltaTheta;
      Double_t       fDeltaPhi;
      Double_t       fDeltaEnergy;
      Int_t          fSubDetector;
      Bool_t         fIsGood;
      Double_t       fTrackerDeltaPhi;


   public:
      InSANEDISTrack();
      virtual ~InSANEDISTrack();
      void Clear(Option_t * opt = "");

      ClassDef(InSANEDISTrack,1)
};

#endif

