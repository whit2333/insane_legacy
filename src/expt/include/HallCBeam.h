#ifndef HallCBeam_HH
#define HallCBeam_HH 1

#include "InSANEElectronBeam.h"
#include "TVector2.h"

/** HallC's Rastered Electron Beam.
 *
 * From https://hallcweb.jlab.org/experiments/sane/wiki/index.php/Beam_in_engine#Coordinate_systems
 * Coordinate systems
 *  Beam position coordinates (+Z is downstream) : 	Horizontal Postion 	Vertical Position
 *  Epics variables 	+BPMX is beam right (mm) 	+BPMY is up (mm)
 *  Raster calibrated ADC in sem_calc_sr_beampos.f 	+gsrx_calib is beam right (cm) 	+gsry_calib is up (cm)
 *  Beam position variable in h_targ_trans.f 	+y_coord is beam left (m) 	+x_coord is down (m)
 *  Beam position variable in sem_calc_sr_beampos.f 	+gSR_beamy is beam left (cm) 	+gSR_beamx is down (cm)
 *  HMS target positions 	+hsytar is towards small angles (m) 	+hsxtar is down (m) 
 *
 *	
 * \ingroup Apparatus
 * \ingroup beam
 */
class HallCRasteredBeam : public HallCPolarizedElectronBeam {
public:
   HallCRasteredBeam(const char * name = "HallC-beam", const char * title = "Hall C Rastered Beam") ;
   ~HallCRasteredBeam();

   virtual void Print(const Option_t * opt ="") const ; // *MENU*

   Double_t   fSRSize;           /// Slow Raster Size
   Double_t   fSRXSlope;         /// Slow Raster x slope in units of ...
   Double_t   fSRYSlope;         /// Slow Raster y slope in units of ...
   Double_t   fSRXADCZero;       /// Slow Raster ADC value where the x coordinate is zero
   Double_t   fSRYADCZero;       /// Slow Raster ADC value where the y coordinate is zero
   Double_t   fSRXOffset;        /// Slow Raster x position offset in cm
   Double_t   fSRYOffset;        /// Slow Raster y position offset in cm

   Double_t   fFRSize;           /// Fast Raster Size
   Double_t   fFRXSlope;         /// Fast Raster x slope in units of ...
   Double_t   fFRYSlope;         /// Fast Raster y slope in units of ...
   Double_t   fFRXADCZero;       /// Fast Raster ADC value where the x coordinate is zero
   Double_t   fFRYADCZero;       /// Fast Raster ADC value where the y coordinate is zero
   Double_t   fFRXOffset;        /// Fast Raster x position offset in cm
   Double_t   fFRYOffset;        /// Fast Raster y position offset in cm

   /** Calculates the slow raster position from the adcs in Human coordinates.
    *  These are rotated (-90 about the z-axis, ie, x_sr->-x and y_sr->y 
    *  n_sr_size/n_sr_slopex*(adc-n_sr_adcx_zero)
    */
   TVector2 GetSlowRasterPosition(Double_t srx_adc, Double_t sry_adc) {
      TVector2 pos( -1.0*(fSRSize / fSRXSlope)*(srx_adc - fSRXADCZero),(fSRSize / fSRYSlope)*(sry_adc - fSRYADCZero));
      return(pos);
   }
   
   /** Get the ADC from the position. Used in simulating the SR ADC in montecarlo.
    *  Inverts the formula used to determine the position.
    */ 
   TVector2 GetSlowRasterADC(Double_t srx_pos, Double_t sry_pos) {
      TVector2 pos( -1.0*(fSRXSlope/fSRSize)*srx_pos+fSRXADCZero,(fSRYSlope/fSRSize)*sry_pos + fSRYADCZero);
      return(pos);
   }

   ClassDef(HallCRasteredBeam, 2)
};


#endif
