#ifndef InSANEMonteCarloEvent_h
#define InSANEMonteCarloEvent_h

#include  "InSANEDetectorEvent.h"
#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>


/** ABC for Monte Carlo type event.
 *
 *  An Event class for monte carlo "thrown" event information
 *  and other unrealistic data.
 *
 * \ingroup Simulation
 *
 * \ingroup Events
 */
class InSANEMonteCarloEvent : public InSANEDetectorEvent {
public :
   InSANEMonteCarloEvent() {};
   virtual ~InSANEMonteCarloEvent() {};

   Double_t fEnergyThrown;
   Double_t fThetaThrown;
   Double_t fPhiThrown;
   Int_t    fParticleThrown;
   Double_t fThrownXPosition;
   Double_t fThrownYPosition;
   Double_t fThrownZPosition;


   // from PrimaryGeneratorAction
   Int_t           mc_nhit_init;
   Double_t         mc_xpos_init[4];
   Double_t         mc_ypos_init[4];
   Double_t         mc_zpos_init[4];
   Double_t         mc_theta_init[4];
   Double_t         mc_phi_init[4];
   Double_t         mc_e_init[4];
   Double_t         mc_pid_init[4];
   // from DetectorConstruction
   Int_t           mc_nhit_bcplane;
   Double_t         mc_x_bcplane[10];
   Double_t         mc_y_bcplane[10];
   Double_t         mc_pid_bcplane[10];
   Double_t         mc_phi_bcplane[10];
   Double_t         mc_theta_bcplane[10];
   Double_t         mc_e_bcplane[10];
   Double_t         mc_xmom_bcplane[10];
   Double_t         mc_ymom_bcplane[10];
   Double_t         mc_zmom_bcplane[10];

   ClassDef(InSANEMonteCarloEvent, 1)
};

#endif
