#ifndef BIGCALClusterProcessor_h
#define BIGCALClusterProcessor_h 1

#include "TSpectrum2.h"

#include "TFile.h"
#include "TObject.h"
#include "TTree.h"
#include "TFile.h"
#include "BigcalEvent.h"

#include "BIGCALCluster.h"
#include "BIGCALGeometryCalculator.h"
#include "TH2F.h"
#include <iostream>
#include "TMath.h"
#include "TCanvas.h"
#include "TClonesArray.h"
#include "InSANEClusterProcessor.h"



/** Concrete class for Bigcal clustering which uses TSpectrum2 peak finder
 *
 *  BIGCALClusterProcessor simplifies the clustering of the Bigcal electromagnetic
 *  calorimeter. It can be given an already processed histogram or a pointer to a
 *  BigcalEvent from which a histogram is produced.
 *
 *  The source histogram should be in units of "Bigcal Cells" and of TH2F type. Thus it
 *  should go from x=1 to x=32 and y=1 to y=56. Actually it can be any size as long
 *  as it is in units of "Bigcal cells", which could be useful for analysis of Bigcal
 *  piece by piece
 *
 * \ingroup ClusteringAlgorithms
 * \ingroup BIGCAL
 */
class BIGCALClusterProcessor : public InSANEClusterProcessor {
   public:
      Float_t     * fXPeaks;
      Float_t     * fYPeaks;
      BigcalEvent * fBigcalEvent;

   public:
      /** Constructor where an event is set as the address of the event with which
       *         clusters will be extracted.
       */
      BIGCALClusterProcessor(TH2F * hist = nullptr, const char * opt = "nobackground,noMarkov");

      virtual ~BIGCALClusterProcessor();

      /** Does nothing for this implementation
       */
      virtual Int_t  Partition() { return(0); }

      /** Creates a cluster for each peak found in find peaks.
       */
      virtual Int_t Cluster() {
         BIGCALCluster * clust = nullptr;
         for (int kk = 0; kk < fNPeaks; kk++) {
            clust = new((*fClusters)[kk]) BIGCALCluster();
            clust->Clear();
            clust->fClusterNumber = kk;
         }
         return(fNPeaks);
      }

      /**  Then finds peaks in source histogram.
       * Returns the number of peaks found.
       */
      virtual Int_t FindPeaks();

      /** Calculates the various moments, energy, etc... for the ith peak in the source histogram
       */
      virtual Int_t CalculateQuantities(BIGCALCluster * acluster, Int_t k);

      /** Set the event address from which the processed histogram is generated.
       */
      Int_t SetEventAddress(BigcalEvent * bcEvent);

      /** Returns the histogram used in clustering
       */
      virtual TH2F *  GetSourceHistogram();

      virtual Int_t SetFindOptions(char * opt) {
         fOptions = opt;
         return(0);
      }

      virtual Int_t Review();

   protected:
      TSpectrum2 * s;
      BIGCALGeometryCalculator * bcgeo;
      TCanvas * c;

      ClassDef(BIGCALClusterProcessor , 2)
};

#endif


