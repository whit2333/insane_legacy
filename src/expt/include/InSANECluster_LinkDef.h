#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class InSANECluster+;
#pragma link C++ class BIGCALCluster+;
#pragma link C++ class Pi0ClusterPair+;
#pragma link C++ class ANNClusterRecon+;

#pragma link C++ class InSANEClusterProcessor+;
#pragma link C++ class InSANECalorimeterCluster+;
#pragma link C++ class InSANEClusterDatum+;
#pragma link C++ class InSANEPartitionCentroid+;

#pragma link C++ class LocalMaxClusterProcessor+;

#pragma link C++ class KMeansClusterProcessor+;

#pragma link C++ class BIGCALClusterProcessor+;

#pragma link C++ class SANEClusteringAnalysis+;

#pragma link C++ class InSANEClusterPartitioning+;

// #pragma link C++ class InSANEClustering<TObject*>+;
// #pragma link C++ class InSANEClustering<InSANEClusteringSeed*>+;
// #pragma link C++ class InSANEClustering<BIGCALCluster*>+;
//#pragma link C++ class InSANEClustering<TObject*>+;


#endif
