#ifndef BETACorrection_HH
#define BETACorrection_HH 1

#include "InSANECorrection.h"

#include "SANEEvents.h"
#include "InSANEDatabaseManager.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"
#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"
#include "InSANEDetectorAnalysis.h"
#include "InSANERunManager.h"

/** Base clase for corrections to BETA detector data.
 *
 * \ingroup Calculations
 * \ingroup BETA
 */
class BETACorrection: public InSANECorrection {

   protected:
      TClonesArray            * fCerHits;
      TClonesArray            * fLucHits;
      TClonesArray            * fBigcalHits;
      TClonesArray            * fBigcalTimingGroupHits;
      TClonesArray            * fTrackerHits;
      TClonesArray            * fClusters;

      BETAEvent               * aBetaEvent;
      BigcalEvent             * bcEvent;
      BigcalHit               * aBigcalHit;
      LuciteHodoscopeEvent    * lhEvent;
      LuciteHodoscopeHit      * aLucHit;
      GasCherenkovEvent       * gcEvent;
      GasCherenkovHit         * aCerHit;
      ForwardTrackerEvent     * ftEvent;
      ForwardTrackerHit       * aTrackerHit;

      SANEEvents              * fEvents;

      ForwardTrackerDetector  * fTrackerDetector ;
      GasCherenkovDetector    * fCherenkovDetector;
      LuciteHodoscopeDetector * fHodoscopeDetector ;
      BigcalDetector          * fBigcalDetector    ;

   public:
      BETACorrection(InSANEAnalysis * analysis = nullptr);
      virtual ~BETACorrection();

      virtual Int_t SetDetectors(InSANEDetectorAnalysis * anAnalysis);
      virtual void  SetEvents(SANEEvents * e);
      virtual Int_t Initialize();
      virtual Int_t Finalize(){return 0;}


      ClassDef(BETACorrection,2)
};



/** Simply counts events for run object.
 *
 * \ingroup Calculations
 */
class BETAEventCounterCorrection : public BETACorrection {

   protected:
      InSANERun * fRun;

   public:
      BETAEventCounterCorrection(InSANEAnalysis * analysis = nullptr);
      virtual ~BETAEventCounterCorrection();

      virtual void   Describe();
      virtual Int_t  Apply();

      ClassDef(BETAEventCounterCorrection,2)
};

#endif


