#ifndef BigcalDetector_hh
#define BigcalDetector_hh 1

#include "InSANECalorimeterDetector.h"

#include <iostream>
#include <vector>
#include "TH2I.h"
#include "TRotation.h"
#include "TVector3.h"
#include "TBrowser.h"
#include "TClonesArray.h"

#include "InSANEClusterEvent.h"
#include "BIGCALCluster.h"
#include "BigcalHit.h"
#include "BigcalLeadGlassBlock.h"
#include "BIGCALGeometryCalculator.h"
#include "BigcalEvent.h"

#define NBIGCALTDC 224
#define NBIGCALADC 1244


/** Concrete class for a Bigcal Detector
 *
 *  Processing doces the following
 *  - Fills event display histograms
 *  - Calculates BigCal timing group cuts
 *  - Calculates BIGCALCluster energy cuts
 *  - Calculates BIGCALCluster cherenkov cuts
 *
 * \ingroup BIGCAL
 */
class BigcalDetector : public InSANECalorimeterDetector {

   protected :
      TSortedList             fTriggerGroupTimings;    /// list of timings for the smaller timing groups
      TSortedList             fTimingGroupPedestals;   /// list of of pedestals for the smaller timing groups

      TRotation               fRotFromBCcoords;        /// Rotation matrix
      Int_t                   fCurrentRunNumber;       /// is this needed?
      Int_t                   fBlockThreshold;         /// Energy for which block is considered to have a hit

   public:
      Int_t                   fCalibrationSeries;
      Double_t                fBigcalTimingGroupEnergy[NBIGCALTDC];  //!
      Double_t                fBigcalTimingGroupTDC[NBIGCALTDC]; //!
      std::vector<Double_t> * fBigcalTDCs[NBIGCALTDC];  //!
      std::vector<Double_t> * fBigcalTDCAligns[NBIGCALTDC]; //!
      std::vector<Double_t> * fBigcal64SumTDCs[38]; //!
      std::vector<Double_t> * fBigcal64SumTDCAligns[38]; //!

      TH2I                  * fEventADCHist;         //! Event level ADC histogram of bigcal
      InSANEClusterEvent    * fClusterEvent;         //! 

      // Declared in InSANEDetector
      //Int_t                   fTypicalPedestal;
      //Int_t                   fTypicalTDCPeak ;
      //Int_t                   fTypicalPedestalWidth;
      //Int_t                   fTypicalTDCPeakWidth ;

      Double_t                cTimingGroupEnergyMin;
      Double_t                cTimingGroupEnergyMax;
      Bool_t                  cPassesTimingGroupEnergy[NBIGCALTDC];

      Double_t                cClusterEnergyMin;
      Double_t                cClusterEnergyMax;
      Bool_t                  cPassesClusterEnergy[NBIGCALTDC];

      Double_t                cTDCMin;
      Double_t                cTDCMax;
      Bool_t                  cPassesTDC[NBIGCALTDC];

      Double_t                cTDCAlignMin;
      Double_t                cTDCAlignMax;

      BIGCALGeometryCalculator * fGeoCalc; //!
      BigcalLeadGlassBlock     * fBlock;  //!

      /// Event level TDC histogram of bigcal sum of 8 groups (BigcalHit::TDCLevel==1)
      TH2I * fEventTimingHist;    //!
      TH2I * fEventTimingHistGood; //!

      /// Event level TDC histogram of bigcal  sum of 64 groups (BigcalHit::TDCLevel==3)
      TH2I * fEventTrigTimingHist; //!
      TH2I * fEventTrigTimingHistGood; //!

      TH2F * fClustersHist;  //!
      TH2F * fChargedPiClustersHist; //!
      TH2F * fElectronClustersHist; //!

      TH2F * fEventHist;  //!
      TH2F * fEventHistWithThreshold; //!

   public :

      /** Allocates memory needed for the tdcpeaks and pedestals then
       *  calls SetRun(#) which grabs the timing and pedestal information from a database.
       */
      BigcalDetector(Int_t runnum = 0);
      ~BigcalDetector();
      /** Necessary for Browsing */
      Bool_t IsFolder() const {
         return kTRUE;
      }
      /** Needed to make object browsable. */
      void Browse(TBrowser* b) {
      }

      virtual Int_t Build() {
         SetBuilt();
         return(0);
      }
      virtual void Initialize();


      /** Calls Get(Timing/Pedestal) for each DetectorTiming or DetectorPedestal.
       */
      Int_t SetRun(Int_t runnum);

      /** \name Geometry functions
       *  useful for event loops etc....
       * @{
       */

      TVector3 GetMomentumVector(BIGCALCluster * cluster) {
         TVector3 P;
         Double_t p = TMath::Sqrt(TMath::Power(cluster->GetCorrectedEnergy(),2.0) - M_e*M_e);
         P.SetMagThetaPhi(p,cluster->GetTheta() + cluster->fDeltaTheta, cluster->GetPhi() + cluster->fDeltaPhi);
         return(P);
      }

      /** Returns the cluster position in Human Coordinates.
       */
      TVector3 GetPosition(BIGCALCluster * clust) {
         TVector3 aPosition(clust->GetXmoment(), clust->GetYmoment() , fGeoCalc->GetDistanceFromTarget());
         aPosition.Transform(fRotFromBCcoords);
         return(aPosition);
      }

      /** Returns the cluster position using the DeltaXY correction in Human Coordinates.
       */
      TVector3 GetXYCorrectedPosition(BIGCALCluster * clust) {
         TVector3 aPosition(clust->GetXmoment() + clust->fDeltaX, clust->GetYmoment() + clust->fDeltaY, fGeoCalc->GetDistanceFromTarget());
         aPosition.Transform(fRotFromBCcoords);
         return(aPosition);
      }

      /** Returns the cluster position using the DeltaXY correction in Bigcal Coordinates.
       */
      TVector3 GetXYCorrectedPosition_BCCoords(BIGCALCluster * clust) {
         TVector3 aPosition(clust->GetXmoment() + clust->fDeltaX, clust->GetYmoment() + clust->fDeltaY, fGeoCalc->GetDistanceFromTarget());
         //aPosition.Transform(fRotFromBCcoords);
         return(aPosition);
      }

      /** Returns the point on the plane that is the face of bigcal
       *  where a line from the origin to seedvec intersects.
       */
      TVector3 GetSeedingHitPosition(TVector3 seedvec) {
         Double_t denom = TMath::Cos(TMath::Pi() * 40.0 / 180.0) * TMath::Cos(seedvec.Phi()) * TMath::Sin(seedvec.Theta())
            + TMath::Cos(TMath::Pi() * 40.0 / 180.0) * TMath::Cos(seedvec.Theta());
         Double_t R = fGeoCalc->GetDistanceFromTarget() / (denom);
         TVector3 result;
         result.SetMagThetaPhi(R, seedvec.Theta(), seedvec.Phi());
         return(result);
      }

      Double_t GetMissDistance(TVector3 v1, TVector3 v2) {
         TVector3 res(v1 - v2);
         return(res.Mag());
      }

      //@}


      void SetClusterEventAddress(InSANEClusterEvent * c) { fClusterEvent = c; }
      InSANEClusterEvent * GetClusterEventAddress() { return(fClusterEvent); }

      Bool_t HitPassesTDCCut(BigcalHit * hit) {
         if (hit->fTDC > cTDCMin && hit->fTDC < cTDCMax) return(true);
         else return(false);
      }

      /** Calculates the 4 trigger grouping info.
       *  Requires the event address to have been set
       *  using SetEventAddress(InSANEDetectorEvent*)
       */
      Int_t ProcessTriggerLogic() {
         //     BETAG4BigcalHit * bcHit;
         //     G4double energyTemp;
         //     for ( int gg =0;gg<1744;gg++ )
         //     {
         //       bcHit = ( *fBigcalHC)[gg];
         //       energyTemp = bcHit->GetDepositedEnergy();
         //       if(energyTemp/MeV>0.01) std::cout << "block energy is " << energyTemp/MeV << " MeV \n";
         //     }
         return(0);
      }

      /**  Correct the event.  */
      Int_t CorrectEvent();

      /// Timing information for each \todo too many timings??
      //InSANEDetectorTiming * bigcalTimings[4*56];
      //InSANEDetectorTiming * bigcalTriggerTimings[38];


      /// Calls Get(Timing/Pedestal) for each DetectorTiming or DetectorPedestal
      // InSANEDetectorPedestal * bigcalPedestals[32*56];

      /** Clears all temporary Event data and tree event structure data
       *
       * \note The event no longer needs to be cleared from here.
       * This is done within the implementation of InSANEAnalysis::ClearEvents()
       */
      virtual Int_t ClearEvent();

      /** Returns a histogram with the current calibration coefficients.
       *  if shouldDraw is set to true, it will call TH1F::Draw.
       */
      TH2F * GetCalibrationCoefficientHist(bool shouldDraw = false) {
         auto * calibCoeffHist =
            new TH2F("BigcalCalibCoeff", "Bigcal Calibration Coefficients", 32, 1, 33, 56, 1, 57);
         calibCoeffHist->Reset();
         for (int j = 1 ; j <= 56 ; j++) {
            for (int i = 1 ; i <= 32 ; i++) {
               if (!(i > 30 && j >= 33)) {
                  calibCoeffHist->Fill(i, j, fGeoCalc->GetCalibrationCoefficient(i, j));
                  //std::cout << "Block("<< i << "," << j << ") coeff: "
                  //          << bcgeo->GetCalibrationCoefficient(i,j) << std::endl;
               }
            }
         }
         calibCoeffHist->SetMaximum(3.0);
         if (shouldDraw) calibCoeffHist->Draw("colz");
         return(calibCoeffHist);
      }

      void SetCalibrationNumber(Int_t num) {
         //if (!(fGeoCalc->LoadCalibrationSeries(num))) 
         fGeoCalc->SetCalibrationSeries(num);
      }
      Int_t GetCalibrationNumber() {
         return(fGeoCalc->GetCalibrationSeries());
      }

      virtual void PrintCuts() const ; // *MENU*

      /** Post-process-action
       */
      virtual Int_t PostProcessAction() {
         /*     PrintCuts();*/
         return(0);
      }

      /**  Prepares the event data holders.
       *   This is called by InSANEAnalyzer and InSANECorrector when it as been
       *   added through their methods AddDetector(InSANEDetector*)
       */
      virtual Int_t ProcessEvent();

      void InitHistograms();


      TList * GetTriggerTimings() {
         return(&fTriggerGroupTimings);
      }
      InSANEDetectorTiming * GetDetectorTriggerTiming(Int_t i) {
         return((InSANEDetectorTiming*)(fTriggerGroupTimings.At(i)));
      }
      Int_t   GetNTriggerTimings() {
         return(fTriggerGroupTimings.GetEntries());
      }

      TList * GetTimingGroupPedestals() {
         return(&fTimingGroupPedestals);
      }
      InSANEDetectorPedestal * GetDetectorTimingGroupPedestal(Int_t i) {
         return((InSANEDetectorPedestal*)(fTimingGroupPedestals.At(i)));
      }
      Int_t   GetNTimingGroupPedestals() {
         return(fTimingGroupPedestals.GetEntries());
      }

      ClassDef(BigcalDetector,5)
};

#endif

