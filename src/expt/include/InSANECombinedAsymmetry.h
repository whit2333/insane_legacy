#ifndef InSANECombinedAsymmetry_HH
#define InSANECombinedAsymmetry_HH 1

#include "TNamed.h"
#include "InSANERunSummary.h"
#include "InSANEAveragedKinematics.h"
#include "InSANESystematic.h"
#include "TH1F.h"

#include <vector>

/** Combined Asymmetry for polarized DIS experiments.
 *
 *  In order to use the count method, you must set the address of
 *  an InSANEDISEvent using SetDISEvent.
 *
 *
 *  \ingroup asymmetries
 */
class InSANECombinedAsymmetry : public TNamed {

   public:
      int                 fNumber;
      InSANERunSummary    fRunSummary;

      Double_t            fdelta_Pb ;
      Double_t            fdelta_Pt ;
      Double_t            fdelta_df ;
      Double_t            fdelta_AQ ;
      Double_t            fdelta_ALT;

      TH1F fAsymmetryVsx;
      TH1F fAsymmetryVsW;
      TH1F fAsymmetryVsNu;
      TH1F fAsymmetryVsE;
      TH1F fAsymmetryVsTheta;
      TH1F fAsymmetryVsPhi;

      InSANEAveragedKinematics1D fAvgKineVsx;
      InSANEAveragedKinematics1D fAvgKineVsW;
      InSANEAveragedKinematics1D fAvgKineVsNu;
      InSANEAveragedKinematics1D fAvgKineVsE;
      InSANEAveragedKinematics1D fAvgKineVsTheta;
      InSANEAveragedKinematics1D fAvgKineVsPhi;

      TH1F fNPlusVsQ2;     
      TH1F fNMinusVsQ2;    


      TH1F fNPlusVsx;
      TH1F fNMinusVsx;

      TH1F fNPlusVsW;
      TH1F fNMinusVsW;

      TH1F fNPlusVsNu;
      TH1F fNMinusVsNu;

      TH1F fNPlusVsE;
      TH1F fNMinusVsE;

      TH1F fNPlusVsPhi;
      TH1F fNMinusVsPhi;

      TH1F fNPlusVsTheta;
      TH1F fNMinusVsTheta;

      TH1F fDilutionVsx;
      TH1F fDilutionVsW;
      TH1F fDilutionVsNu;
      TH1F fDilutionVsE;
      TH1F fDilutionVsTheta;
      TH1F fDilutionVsPhi;

      TH1F fSystErrVsx;
      TH1F fSystErrVsW;
      TH1F fSystErrVsNu;
      TH1F fSystErrVsE;
      TH1F fSystErrVsTheta;
      TH1F fSystErrVsPhi;

      std::vector<InSANESystematic>  fSystematics_x;
      std::vector<InSANESystematic>  fSystematics_W;
      std::vector<InSANESystematic>  fSystematics_Nu;
      std::vector<InSANESystematic>  fSystematics_E;
      std::vector<InSANESystematic>  fSystematics_Theta;
      std::vector<InSANESystematic>  fSystematics_Phi;

   public:
      InSANECombinedAsymmetry(Int_t n = 0);
      virtual ~InSANECombinedAsymmetry();

      void InitHists(const TH1F& h);
      void Reset(Option_t * opt = "");

      Bool_t         IsFolder() const {
         return kTRUE;
      }

      Double_t GetQ2() const { return (fNPlusVsQ2.GetMean() + fNMinusVsQ2.GetMean())/2.0 ;}

      void           Browse(TBrowser* b) {
         //   b->Add(&fHistograms,"Histograms");
         b->Add(&fAsymmetryVsx);
         b->Add(&fAvgKineVsx);
         b->Add(&fDilutionVsx);
         b->Add(&fSystErrVsx);
         //   if (fAsymmetryVsW)         b-> Add(fAsymmetryVsW);
         //   if (fAsymmetryVsNu)        b-> Add(fAsymmetryVsNu);
         //   if (fAsymmetryVsE)         b-> Add(fAsymmetryVsE);
      }


      ClassDef(InSANECombinedAsymmetry, 32)
};

#endif


