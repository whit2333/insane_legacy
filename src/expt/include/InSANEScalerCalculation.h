#ifndef InSANEScalerCalculation_HH
#define InSANEScalerCalculation_HH

#include "SANEScalers.h"
#include "InSANEScalerEvent.h"
#include "InSANEAnalysis.h"

/** ABC Scaler Calculation.
 *
 * Provides the basic functionality for Scaler calculations.
 * If you want to combine with detector data, use multiple inheritance of this and some InSANECalculation class.
 * This way when you a detector event is being procesed it calls the normal InSANECalculation::Calculate(), and
 * when a scaler event it is being processed it calls the Pure virtual method InSANEScalerCalculation::CalculateInterval().
 *
 * CalculateInterval() is called when the next scaler event is ready to be read.
 * Therefore you are ready to calculate, e.g., the deadtime since the last scaler event,
 * the average beam current for that interval, or total charge.
 *
 * \ingroup Scalers
 * \ingroup Calculations
 */
class InSANEScalerCalculation {
   public :

      InSANEScalerCalculation(InSANEAnalysis * analysis = nullptr) {
         fScalers = nullptr;
         fRun = nullptr;
         SetScalers(analysis->GetScalers());
         fScalerOutTree = nullptr;
      }

      virtual ~InSANEScalerCalculation() {

      }

   public:
      SANEScalers * fScalers;
      InSANERun * fRun;

      void SetScalers(InSANEAnalysisEvents * scalers) {
         if (scalers) fScalers = (SANEScalers*)scalers;
      }

      InSANEAnalysisEvents*  GetScalers() {
         return(fScalers);
      }

      /** Calculates the values at the end of the scaler interval.  */
      virtual Int_t CalculateInterval() = 0;


      /** Initialize.
       *
       * virtual method called by InSANECorrector::Initialize
       */
      virtual Int_t Initialize() {return(0); };

      TTree * fScalerOutTree;

      virtual void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  InSANEScalerCalculation - \n ";
         std::cout <<  "      \n";
         std::cout <<  "_______________________________________________________________________________\n";
      }


      ClassDef(InSANEScalerCalculation, 1)
};

#endif

