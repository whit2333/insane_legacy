#ifndef InSANECutter_H
#define InSANECutter_H
#include "TNamed.h"
#include "InSANECut.h"
#include "TList.h"
#include "TBrowser.h"

/** ABC for Cutter object on an event
 *
 * I think this should actually be named \em InSANEAxe ! But will hold off until the
 * motivation really comes to me to change it.
 *
 * The Cut defines the type of \em Axe you would use on the \em Tree .
 *
 * The Cutter is a collection of cuts that are applied to a larger structure
 *
 * Ideally we would do something like this:
 *
 *    InSANECutter * axe = new InSANECutter();
 *    InSANECut * a, * b, * c ... define them...
 *    axe->AddCut(a);
 *    axe->AddCut(b);// etc..
 *    ... Get event ...
 *    Bool_t result = axe->Chop(fEvent);
 *
 * \ingroup Cuts
 */
class InSANECutter : public TNamed {
public:
   InSANECutter(const char * n = "anAxe", const char * t = "Axe") : TNamed(n, t) {
      fListOfCuts.Clear();
   }

   virtual  ~InSANECutter() {
   }

   /** Necessary for Browsing */
   Bool_t IsFolder() const {
      return kTRUE;
   }
   /** Needed to make object browsable. */
   void Browse(TBrowser* b) {
      b->Add(&fListOfCuts);
   }

   void SetReferenceRunNumber(Int_t i) {
      fReferenceRunNumber = i;
   }
   Int_t GetReferenceRunNumber() {
      return(fReferenceRunNumber);
   }
   Int_t fReferenceRunNumber;

   TList fListOfCuts;
protected:
   TList fListOfHistograms;


public:
   /** Add a cut to the cutter */
   Int_t AddCut(InSANECut * acut) {
      if (acut) fListOfCuts.Add(acut);
      return(0);
   }

   InSANECut * GetCut(const char * name) {
      if (fListOfCuts.FindObject(name)) {
         auto * aCut = (InSANECut*)  fListOfCuts.FindObject(name);
         return(aCut);
      }
      return(nullptr);
   }

   /** Returns true if all the cuts are passed.  */
   virtual Bool_t ApplyCuts() {
//        InSANECut * acut;
//        TListIter cutIter(&fListOfCuts);
//        while ( (acut = (InSANECut*)cutIter.Next() ) ) {
//            if( !(acut->PassesCut()) ) return(false) ;
//        }
      return(true);
   }

   void PrintCuts() {
      for (int i = 0; i < fListOfCuts.GetEntries(); i++) {
         auto * aCut = (InSANECut*) fListOfCuts.At(i);
         aCut->Print();
      }
   }


//  Int_t RemoveCut(char * name);
//  Int_t SetCutEvent( InSANEEvent * );
   /** Process the tree return a new tree */
//  TTree * Process(TTree * atree);
   /** Print all the cuts */
//  Int_t PrintCuts();
//    void Describe(){ PrintCuts(); }
//  Int_t MakeHistograms();
//Int CreateBranches(TTree * );

   ClassDef(InSANECutter, 1)
};


#endif
