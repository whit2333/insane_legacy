#ifndef SANETrackCalculations_H
#define SANETrackCalculations_H 1

#include <vector>

#include "BETACalculation.h"
#include "SANEClusteringAnalysis.h"
#include "InSANEAnalyzer.h"
#include "InSANERunManager.h"
#include "SANERunManager.h"

#include "InSANEHitPosition.h"
#include "InSANEDISTrajectory.h"
#include "InSANEDISTrack.h"
//#include "CleanedBETAEvent.h"
#include "InSANETriggerEvent.h"
#include "InSANEDilutionFactor.h"
#include "InSANEReconstructedEvent.h"
#include "InSANETrackPropagator2.h"

#include "SANENeuralNetworks.h"

/** Creates a tree that is filled for every good track (see SANEElectronSelector1)
 *  Similar to SANEElectronSelector1 except that it outputs a different kind of tree.
 *
 * \ingroup Calculations
 */
class SANETrackCalculation1 : public BETACalculationWithClusters  {

   private: 
      TVector3   fp_naive;
      TVector3   fvertex;
      TVector3   fx_bigcalplane;
      Bool_t     fIsPerp;
      TVector3   Pvec;
      TVector3   PlaneNorm;
      TVector3   PlanePoint;
      TVector3   PlaneResult;
      TVector3   direction;

   protected:
      //NNParaElectronXYCorrectionEnergy          fNNParaElectronClusterDeltaE; 
      NNParaElectronXYCorrectionClusterDeltaX   fNNParaElectronClusterDeltaX;
      NNParaElectronXYCorrectionClusterDeltaY   fNNParaElectronClusterDeltaY;

      // Angle Corrections
      NNParaElectronAngleCorrectionDeltaTheta   fNNParaElectronDeltaTheta;
      NNParaElectronAngleCorrectionDeltaPhi     fNNParaElectronDeltaPhi;
      
      // Momentum Vector 
      NNParaElectronAngleCorrectionBCPDirTheta  fNNParaElectronBCPDirTheta;
      NNParaElectronAngleCorrectionBCPDirPhi    fNNParaElectronBCPDirPhi;

      //NNPerpElectronXYCorrectionEnergy          fNNPerpElectronClusterDeltaE; 
      NNPerpElectronXYCorrectionClusterDeltaX   fNNPerpElectronClusterDeltaX;
      NNPerpElectronXYCorrectionClusterDeltaY   fNNPerpElectronClusterDeltaY;

      NNPerpElectronAngleCorrectionDeltaTheta   fNNPerpElectronDeltaTheta;
      NNPerpElectronAngleCorrectionDeltaPhi     fNNPerpElectronDeltaPhi;
      
      NNPerpElectronAngleCorrectionBCPDirTheta  fNNPerpElectronBCPDirTheta;
      NNPerpElectronAngleCorrectionBCPDirPhi    fNNPerpElectronBCPDirPhi;

      ANNDisElectronEvent6                      fANNDisElectronEvent;  // For cluster XY correction
      ANNElectronThruFieldEvent2                fANNFieldEvent2;       // For track angle correction
      ANNElectronThruFieldEvent3                fANNFieldEvent3;       // For momentum vector correction

      Double_t                                  fNNParams[20];

      InSANETrackPropagator2                   * fTrackPropagator;
      ANNDisElectronEvent5                     * fANNEvent;
      TVector3                                 * fTrackPropMomentum0;
      TVector3                                 * fTrackPropVertex0;
      TVector3                                 * fTrackPropMomentum1;
      TVector3                                 * fTrackPropVertex1;

      Double_t                                   nnInputArray[9];
      ANNDisElectronEvent                      * nnEvent;

      Int_t fTrackNumber;
      Int_t fEventNumber;

      TVector3 v1;
      TVector3 N1;

      InSANEDISTrajectory                      * fTrajectory;
      InSANEDISTrack                      * fTrack;

      Bool_t fcGoodElectron;
      Bool_t fcOKElectron;
      Bool_t fcBadElectron;

      Double_t fClosestTrackerDistance;

      std::vector<double>                      * fGroups;

      TTree                                    * fOutTree2;

   public:

      InSANEReconstructedEvent                 * fReconEvent;
      Double_t fMaxLuciteMiss;  ///< Max allowed miss for lucite hits (cm)
      Double_t fMaxTrackerMiss; ///< Max allowed miss for tracker hits (cm)


      SANETrackCalculation1(InSANEAnalysis * analysis);
      virtual ~SANETrackCalculation1();

      virtual Int_t Initialize();

      /** Calculates the run asymmetry.  */
      void MakePlots() {
         auto Nplus = (Double_t) InSANERunManager::GetRunManager()->fCurrentRun->fTotalNPlus;
         auto Nminus = (Double_t) InSANERunManager::GetRunManager()->fCurrentRun->fTotalNMinus;
         SANERunManager::GetRunManager()->fCurrentRun->fTotalAsymmetry = (Nplus - Nminus) / (Nplus + Nminus);
      }

      /** does nothing because the output tree is filled in the calculate loop. */
      virtual void FillTree() {
         //if(fOutTree) if(fcGoodElectron) fOutTree->Fill();
      }

      void  Describe();
      Int_t Calculate();

      ClassDef(SANETrackCalculation1, 1)
};




/** Uses InSANETrackPropagator
 *
 * \ingroup Calculations
 */
class SANETrackCalculation2 : public BETACalculationWithClusters  {

   protected:
      ANNDisElectronEvent5                      fNNParaXYClusterEvent; // ANN event used for determing cluster x,y,and energy corrections 
      NNParaElectronXYCorrectionClusterDeltaX   nnParaXYClusterDeltaX; // ANN to determine DeltaX
      NNParaElectronXYCorrectionClusterDeltaY   nnParaXYClusterDeltaY; // ANN to determine DeltaY
      //NNParaElectronXYCorrectionEnergy          nnParaXYClusterEnergy; // ANN to determine DeltaE 

      NNParaElectronAngleCorrectionBCPDir       nnParaBCPDir; // Network to calculate momentum vector at bigcal.
      ANNElectronThruFieldEvent2                fNNElectronThruFieldEvent;

      InSANEDISTrajectory                     * fTrajectory;
      Bool_t fcGoodElectron;
      Bool_t fcOKElectron;
      Bool_t fcBadElectron;
      Double_t fClosestTrackerDistance;
      std::vector<double>                      * fGroups;

      TVector3 v1;
      TVector3 N1;
      Double_t nnInputArray[9];

      Int_t fTrackNumber;
      Int_t fEventNumber;
      TVector3                                 * fTrackPropMomentum0;
      TVector3                                 * fTrackPropVertex0;
      TVector3                                 * fTrackPropMomentum1;
      TVector3                                 * fTrackPropVertex1;

      InSANETrackPropagator2                  * fTrackPropagator;
      TTree                                   * fTrajectoryTree;
      TTree                                   * fTracksTree;
      TTree                                   * fOutTree2;

   public:

      InSANEReconstructedEvent                * fReconEvent;

      SANETrackCalculation2(InSANEAnalysis * analysis);
      virtual ~SANETrackCalculation2();

      virtual Int_t Initialize();

      void  Describe();
      Int_t Calculate();
      /** does nothing because the output tree is filled in the calculate loop. */
      virtual void FillTree() {
         //if(fOutTree) if(fcGoodElectron) fOutTree->Fill();
      }

      ClassDef(SANETrackCalculation2, 1)
};

#endif

