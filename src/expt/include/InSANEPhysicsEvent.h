#ifndef InSANEPhysicsEvent_h
#define InSANEPhysicsEvent_h

#include <TROOT.h>
#include <TObject.h>
#include <TH2.h>
#include <TClonesArray.h>
#include "InSANEEvent.h"

/** Base class for a physics event.
 *
 *  A physics event will generally have a reconstruction array
 *
 *
 * \ingroup Event
 */
class InSANEPhysicsEvent : public InSANEEvent {

   public:

      Int_t fPID;
      Int_t fNTracks;

   public:
      InSANEPhysicsEvent() ;

      virtual ~InSANEPhysicsEvent() ;

      virtual void Reset() ;

      virtual void ClearEvent(Option_t * opt = "");

      virtual void Print(Option_t * opt = "") const ;

      ClassDef(InSANEPhysicsEvent,4)
};

#endif



