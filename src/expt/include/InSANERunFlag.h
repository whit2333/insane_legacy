#ifndef InSANERunFlag_HH
#define InSANERunFlag_HH 1
#include "TNamed.h"
#include "TString.h"
#include "InSANEDatabaseManager.h"
#include "TBrowser.h"


/** A run flag which is created at every occurance
 *
 *  These should be created by the skimmer
 */
class InSANERunFlag : public TNamed {

   public:
      Double_t fSetValue;
      Double_t fValue;
      Bool_t   fFlagged;
      Int_t    fStartingEvent;
      Int_t    fEndingEvent;
      Int_t    fRunNumber;
      TString  fDescription;

   public:
      InSANERunFlag(const char * name = "aFlag", const char * title = "A run flag"):
         TNamed(name, title) {
            fFlagged = false;
            fStartingEvent = 0;
            fEndingEvent = 0;
            fSetValue = 0.0;
            fValue = 0.0;
            fRunNumber = 0;
         }
      InSANERunFlag(const InSANERunFlag& f) : TNamed(f) {
         (*this) = f;
      }

      /** Necessary for Browsing */
      Bool_t IsFolder() const {
         return kTRUE;
      }
      /** Needed to make object browsable. */
      void Browse(TBrowser* /*b*/) {
         /*b->Add(fCalibrations,"Calibrations");*/
      }

      InSANERunFlag& operator=(const InSANERunFlag &rhs) {
         // Check for self-assignment!
         if (this == &rhs)      // Same object?
            return *this;        // Yes, so skip assignment, and just return *this.
         // Deallocate, allocate new space, copy values...
         TNamed::operator=(rhs);
         fFlagged = rhs.fFlagged;
         fStartingEvent = rhs.fStartingEvent;
         fEndingEvent = rhs.fEndingEvent;
         fDescription = rhs.fDescription;
         fSetValue = rhs.fSetValue;
         fValue = rhs.fValue;
         return(*this);
      }

      virtual ~InSANERunFlag() {}

      virtual void Initialize() { }

      /** Raise the flag when a limiting condition is passed */
      virtual void RaiseFlag() {
         fFlagged = true;
         RecordFlag();
      }

      virtual void LowerFlag() {
         UpdateFlag();
         fFlagged = false;
      }

      /** Reset the run flag */
      void ClearFlag() {
         fFlagged = false;
      }

      virtual void UpdateFlag();

      virtual void RecordFlag();

      void Print(Option_t * opt = "") const ;  // *MENU*

      ClassDef(InSANERunFlag, 1)
};


#endif

