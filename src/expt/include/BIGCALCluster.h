#ifndef BIGCALCluster_h
#define BIGCALCluster_h 1

#include "BETAEvent.h"
#include "TMath.h"
#include "BIGCALGeometryCalculator.h"
#include "InSANECalorimeterCluster.h"
#include "TVector3.h"
#include "LuciteGeometryCalculator.h"


/** Concrete class for a BigCal Cluster
 *
 *  BIGCALCluster represents a single cluster in bigcal and is used for a TCLonesArray
 *  branch, "betaClusters". The size of the cluster is fixed to be 5x5 around the central
 *  block
 *
 *  \ingroup Clusters
 * \ingroup BIGCAL
 */
class BIGCALCluster : public InSANECalorimeterCluster {

   public:

      Double_t   fDeltaE; ///< Value of artificial Neural Network or other correction to the Energy
      Double_t   fDeltaX; /// Value of artificial Neural Network or other correction to the x position
      Double_t   fDeltaY; /// Value of artificial Neural Network or other correction to the x position

      Double_t   fDeltaTheta;  /// 
      Double_t   fDeltaPhi;    /// 

      Double_t   fDeltaThetaP; ///
      Double_t   fDeltaPhiP;   ///

      Bool_t fPassesTiming[10];

      Int_t fLevel; /// Level of clustering

      /** Type of cluster determined from mostly trigger type.
       *  <ul>
       *  <li>fType=1 beta1</li>
       *  <li>fType=2 beta2</li>
       *  <li>fType=3 pi0</li>
       *  <li>fType=4 pi0 with no cherenkov and on RCS/Protvino border</li>
       *  <li>fType=5 coin</li>
       *  </ul>
       *  All others are 10.
       */
      Int_t    fType;

      Double_t fCherenkovTDC;     // Cherenkov TDC value of primary mirror

      Double_t fLuciteTDC;        // Best single tdc hit
      Double_t fLuciteTDCSum;     // Best tdc sum
      Double_t fLuciteTDCDiff;    // Best tdc sum

      Double_t fTrackerTDC;       //

      Double_t fClusterTime;      /// Timing for cluster determined from peak cluster TDC values
      Double_t fClusterTime1;     ///
      Double_t fClusterTime2;     ///

      Int_t   fIsGood; /// Is a good cluster. TBD

      Bool_t  fGoodCherenkovTDCHit; // True if cluster has at least good cherenkov TDC hit in the sum
      Bool_t  fGoodCherenkovTiming; // True if cluster has a good cherenkov TDC hit in all mirrors used in sum

      Bool_t  fGoodBigcalTiming;
      Bool_t  fGoodCherenkov;
      Bool_t  fGoodLuciteTDCHit;
      Bool_t  fGoodDoubleLuciteTDCHit;
      Bool_t  fGoodLuciteTiming;
      Bool_t  fGoodDoubleLuciteTiming;
      Bool_t  fGoodLucitePosition;

      Int_t   fSubDetector;

      Double_t fCherenkovBestADCSum;     // This is the best sum of cherenkov mirrors given the clusters position
      Double_t fCherenkovBestNPESum;     // This is the best sum of cherenkov mirrors given in number of photoelectrons
      Double_t fCherenkovBestADCChannel; // This represents the, simple value for the cherenkov, it is the mirror which  is hit by the straight track.
      Double_t fCherenkovBestNPEChannel; // Same as fCherenkovBestADCChannel but in number of photoelectrons
      Int_t    fCherenkovBestADC;
      Double_t fCherenkovAvgTime;        //  tdc value of all cherenkov hits called "good" averaged
      Int_t    fNumberOfMirrors;         //  The number of mirrors used in the sums fCherenkovBestADCSum and fCherenkovBestNPESum. This should only be from 1-4 with 3 being very unlikely.

      Int_t    fNumberOfGoodMirrors;     // The number of mirrors used in the sums that have good tdc hit
      Int_t    fPrimaryMirror;           //  the mirror which should have collected the most light
      Int_t    fHelicity;

   public:
      BIGCALCluster(Int_t clustNum = -1);
      BIGCALCluster(const BIGCALCluster& rhs) : InSANECalorimeterCluster(rhs) {
         (*this) = rhs;
      }
      virtual ~BIGCALCluster();
      BIGCALCluster& operator=(const BIGCALCluster &rhs) ;
      BIGCALCluster& operator+=(const BIGCALCluster &rhs);

      /** Add this instance's value to other, and return a new instance
       * with the result.*/
      const BIGCALCluster operator+(const BIGCALCluster &other) const {
         BIGCALCluster result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
         result += other;            // Use += to add other to the copy.
         return result;              // All done!
      }

      /** Set the cluster time from to the peak location's TDC values.
       */
      virtual void SetClusterTime(Double_t time) {
         fClusterTime = time;
      }

      Double_t GetEnergy() const {
         return(fTotalE);
      }

      /** Returns theta using xmoment, ymoment, and BIGCALGeometryCalculator::fNominalRadialDistance
       */
      Double_t GetTheta() const {
         TVector3 v1(GetXmoment(),
               GetYmoment(),
               BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
         v1.RotateY(TMath::Pi() * 40.0 / 180.0);
         return(v1.Theta());
      }

      /** Returns phi using xmoment, ymoment, and BIGCALGeometryCalculator::fNominalRadialDistance
       */
      Double_t GetPhi() const {
         TVector3 v1(GetXmoment(),
               GetYmoment(),
               BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
         v1.RotateY(TMath::Pi() * 40.0 / 180.0);
         return(v1.Phi());
      }

      virtual void Clear(Option_t *opt = "");

      /** Using energy, angles, and their corrections, fDeltaE, fDeltaTheta, fDeltaPhi and 
       *  calculates a momentum vector  at the target in the human coordinate system. 
       */
      TVector3 GetMomentumVector() {
         TVector3 mom3(0, 0, 1);
         mom3.SetMagThetaPhi(GetEnergy() + fDeltaE ,
               GetTheta()  + fDeltaTheta ,
               GetPhi()    + fDeltaPhi);
         return(mom3);
      }

      Double_t GetCorrectedEnergy() const {
         return (GetEnergy() + fDeltaE) ;
      }
      Double_t GetCorrectedTheta()  const {
         return (GetTheta()  + fDeltaTheta) ;
      }
      Double_t GetCorrectedPhi()    const {
         return (GetPhi()    + fDeltaPhi) ;
      }


      /** @name Correlation methods for other detector hits
       *   Methods used with Geometry calculators for other detectors....
       *
       */
      /** Returns true if the lucite hodoscope bar number matches the
       *  cluster position.
       */
      Bool_t IsGoodHodoscopeBar(Int_t bar) {
         LuciteGeometryCalculator * geo = LuciteGeometryCalculator::GetCalculator();
         Int_t bcrow = geo->GetBigcalRow(bar);
         if (TMath::Abs(bcrow - fjPeak) < 3) return(true);
         return(false);
      }

      // Recalculate moments
      Int_t  CalculateMoments();

      ClassDef(BIGCALCluster,15)
};

#endif

