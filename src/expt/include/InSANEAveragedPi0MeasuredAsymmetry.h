#ifndef InSANEAveragedPi0MeasuredAsymmetry_HH
#define InSANEAveragedPi0MeasuredAsymmetry_HH 1

#include "TNamed.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "InSANEPi0MeasuredAsymmetry.h"

/** Used to combine multiple InSANEPi0MeasuredAsymmetry classes into one measurement.
 *  The Weighted mean for each bin is computed.
 */ 
class InSANEAveragedPi0MeasuredAsymmetry : public TNamed {

  protected:

      TH1F * fATheta1; //->
      TH1F * fATheta2; //->
      TH1F * fDFTheta1;//->
      TH1F * fDFTheta2;//->

      TH1F * fAPhi1; //->
      TH1F * fAPhi2; //->
      TH1F * fDFPhi1;//->
      TH1F * fDFPhi2;//->

      TH1F * fAPt1; //->
      TH1F * fAPt2; //->
      TH1F * fDFPt1;//->
      TH1F * fDFPt2;//->

      TH1F * fAE1; //->
      TH1F * fAE2; //->
      TH1F * fDFE1;//->
      TH1F * fDFE2;//->

      //TH2F * fAxQ21; //->
      //TH2F * fAxQ22; //->
      //TH2F * fDFxQ21;//->
      //TH2F * fDFxQ22;//->

      //TH2F * fAxPhi1; //->
      //TH2F * fAxPhi2; //->
      //TH2F * fDFxPhi1;//->
      //TH2F * fDFxPhi2;//->

      //TH3F * fAWxPhi1; //->
      //TH3F * fAWxPhi2; //->
      //TH3F * fDFWxPhi1;//->
      //TH3F * fDFWxPhi2;//->

      Int_t                     fNAsymmetries;
      InSANEPi0MeasuredAsymmetry * fAsymmetryResult; //->

   protected:

      Int_t InitHists(TH1F * h0, TH1F ** h1, TH1F ** h2);
      Int_t InitHists(TH2F * h0, TH2F ** h1, TH2F ** h2);
      Int_t InitHists(TH3F * h0, TH3F ** h1, TH3F ** h2);

      /** Takes the input hisogram (h0) and adds it to the sums. 
       *  \f$ \sum A_i/\sigma_i^2 \f$ and \f$ \sum 1/\sigma_i^2} \f$ 
       *
       *  These quantities are used in calculating the weighted means via
       *
       *  \f$ A_{\mu} = \frac{\sum A_i/\sigma_i^2}{\sum 1/\sigma_i^2} \f$ 
       *
       *  \f$ \sigma_{\mu}^2 = \frac{1}{\sum 1/\sigma_i^2} \f$
       */
      Int_t AddToSum(TH1F * h0, TH1F * h1, TH1F * h2);
      Int_t AddToSum(TH2F * h0, TH2F * h1, TH2F * h2);
      Int_t AddToSum(TH3F * h0, TH3F * h1, TH3F * h2);

      /** Calculates the weighted mean and proper error. */
      Int_t CalculateMean(TH1F * h0, TH1F * h1, TH1F * h2);
      Int_t CalculateMean(TH2F * h0, TH2F * h1, TH2F * h2);
      Int_t CalculateMean(TH3F * h0, TH3F * h1, TH3F * h2);

   public:

      InSANEAveragedPi0MeasuredAsymmetry();
      virtual ~InSANEAveragedPi0MeasuredAsymmetry();

      Bool_t         IsFolder() const { return kTRUE; }

      void           Browse(TBrowser* b) {
         if(fAsymmetryResult) b->Add(fAsymmetryResult,"A-Result");
      }
      
      InSANEPi0MeasuredAsymmetry * GetAsymmetryResult() const { return(fAsymmetryResult);}

      void Init(InSANEPi0MeasuredAsymmetry *a);

      /** Add asymmetry to average. 
       * This keeps a running tally of the sums 
       *  \f$ \sum A_i/\sigma_i^2 \f$ and \f$ \sum 1/\sigma_i^2} \f$ 
       *  in histograms which are initiallized by the first asymmetry added.
       *  See InSANEAveragedPi0MeasuredAsymmetry::Calculate() for more details.
       */ 
      Int_t Add(InSANEPi0MeasuredAsymmetry * asym);


      /** Calculates the weighted mean with the standard error on the mean.
       *
       *  \f$ A_{\mu} = \frac{\sum A_i/\sigma_i^2}{\sum 1/\sigma_i^2} \f$ 
       *
       *  \f$ \sigma_{\mu}^2 = \frac{1}{\sum 1/\sigma_i^2} \f$
       *  
       *  where the error is then \f$ \sqrt{\sigma_{\mu}^2} \f$.
       */ 
      InSANEPi0MeasuredAsymmetry * Calculate();

      ClassDef(InSANEAveragedPi0MeasuredAsymmetry,9)
};

#endif

