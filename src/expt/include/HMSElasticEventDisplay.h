#ifndef HMSElasticEventDisplay_HH
#define HMSElasticEventDisplay_HH

#include "InSANEEventDisplay.h"

class HMSElasticEventDisplay : public InSANEEventDisplay {
public:
   HMSElasticEventDisplay() : InSANEEventDisplay() {
      fUpdateTriggerBit = 4; // ctrig
   };

   virtual ~HMSElasticEventDisplay() {
      ;
   };

   ClassDef(HMSElasticEventDisplay, 1)
};

#endif
