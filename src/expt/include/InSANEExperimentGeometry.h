#ifndef InSANEExperimentGeometry_h
#define InSANEExperimentGeometry_h

#include <TROOT.h>
#include <TFile.h>
#include <TNamed.h>


/** ABC for experimental geometry
 *
 *  This class connects to a database and grabs the dimensions of things, and
 *  time dependent settings etc...
 *
 * \ingroup Detectors
 *
 * \ingroup Geometry
 */
class InSANEExperimentGeometry  : public TNamed {
public :
   InSANEExperimentGeometry();
   virtual   ~InSANEExperimentGeometry();


private :

   ClassDef(InSANEExperimentGeometry, 1)
};


#endif
