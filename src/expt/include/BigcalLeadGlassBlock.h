#ifndef BigcalLeadGlassBlock_h
#define BigcalLeadGlassBlock_h

#include "InSANELeadGlassBlock.h"
#include "BIGCALGeometryCalculator.h"

/**  Bigcal lead glass block
 *
 * \ingroup BIGCAL
 * \ingroup detComponents
 */
class BigcalLeadGlassBlock : public InSANELeadGlassBlock {
public:
   BigcalLeadGlassBlock(const char * name = "detComp" , const char * title  = "Det Comp", Int_t num = 0):
      InSANELeadGlassBlock(name, title, num) {
      fGeoCalc = BIGCALGeometryCalculator::GetCalculator();
      fTDCGroup = 0;
      fIn2TDCGroups = false;
      fTDCGroup2 = 0;
   }

   BigcalLeadGlassBlock(Int_t aBlockNumber) { }

   ~BigcalLeadGlassBlock() { }

   Int_t fTDCGroup;    /// TDC group number
   bool fIn2TDCGroups;
   Int_t fTDCGroup2;

protected:
   BIGCALGeometryCalculator * fGeoCalc; //!

   ClassDef(BigcalLeadGlassBlock, 1)
};


/** RCS lead glass block
 *
 * \ingroup detComponents
 */
class RCSLeadGlassBlock : public BigcalLeadGlassBlock {
public:
   RCSLeadGlassBlock() {
      fXSize = fGeoCalc->rcsCellSize;
      fYSize = fGeoCalc->rcsCellSize;
      fZSize = fGeoCalc->rcsCellZSize;
   }
   ~RCSLeadGlassBlock() {
   }


   ClassDef(RCSLeadGlassBlock, 1)
};

/** Protvino lead glass block
 *
 * \ingroup detComponents
 */
class ProtvinoLeadGlassBlock : public BigcalLeadGlassBlock {
public:
   ProtvinoLeadGlassBlock() {
      fXSize = fGeoCalc->protCellSize;
      fYSize = fGeoCalc->protCellSize;
      fZSize = fGeoCalc->protCellZSize;
   }
   ~ProtvinoLeadGlassBlock() {}


   ClassDef(ProtvinoLeadGlassBlock, 1)
};


#endif

