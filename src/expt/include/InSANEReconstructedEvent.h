#ifndef InSANEReconstructedEvent_HH
#define InSANEReconstructedEvent_HH 1

#include "TObject.h"
#include "TClonesArray.h"

#include "InSANEHitPosition.h"
#include "BIGCALCluster.h"
#include "InSANETrajectory.h"
#include "LuciteHodoscopePositionHit.h"
#include "ForwardTrackerPositionHit.h"



/** Reconstructed Event containing multiple trajectories, clusters, and position hits.
 *
 * \ingroup Event
 */
class InSANEReconstructedEvent : public TObject {

   public:

      //static TClonesArray     * fgTrajectories;
      //static TClonesArray     * fgTargetPositions;
      //static TClonesArray     * fgTrackerPositions;
      //static TClonesArray     * fgTracker2Positions;
      //static TClonesArray     * fgTrackerY1Positions;
      //static TClonesArray     * fgTrackerY2Positions;
      //static TClonesArray     * fgLucitePositions;

      TClonesArray     * fTrajectories;       //->
      TClonesArray     * fTargetPositions;    //->
      TClonesArray     * fTrackerPositions;   //->
      TClonesArray     * fTrackerY2Positions; //->
      TClonesArray     * fTrackerY1Positions; //->
      TClonesArray     * fTracker2Positions;  //->
      TClonesArray     * fLucitePositions;    //->

      Double_t           fClosestLuciteMissDistance;
      Int_t              fClosestLuciteHitNumber;
      Double_t           fClosestTrackerMissDistance;
      Int_t              fClosestTrackerHitNumber;

      Int_t              fRunNumber;
      Int_t              fEventNumber;
      Int_t              fNGoodPositions;

      Int_t              fNTargetPositions;
      Int_t              fNTrackerPositions;
      Int_t              fNTrackerY2Positions;
      Int_t              fNTrackerY1Positions;
      Int_t              fNTracker2Positions;
      Int_t              fNLucitePositions;
      Int_t              fNTrajectories;

   public:

      /** Creates a new trajectory from aCluster */
      InSANETrajectory * AddTrajectory() {
         auto * traj = new((*fTrajectories)[fNTrajectories])InSANETrajectory();
         fNTrajectories++;
         return(traj);
      }

      /** Generates a new lucite position.  */
      InSANEHitPosition * AddLucitePosition() {
         auto * hitpos = new((*fLucitePositions)[fNLucitePositions])InSANEHitPosition();
         fNLucitePositions++;
         return(hitpos);
      }

      /** Generates a new tracker position. */
      InSANEHitPosition * AddTrackerPosition() {
         auto * hitpos = new((*fTrackerPositions)[fNTrackerPositions])InSANEHitPosition();
         fNTrackerPositions++;
         return(hitpos);
      }

      /** Generates a new tracker position. */
      InSANEHitPosition * AddTracker2Position() {
         auto * hitpos = new((*fTracker2Positions)[fNTracker2Positions])InSANEHitPosition();
         fNTracker2Positions++;
         return(hitpos);
      }

      /** Generates a new tracker position. */
      InSANEHitPosition * AddTrackerY2Position() {
         auto * hitpos = new((*fTrackerY2Positions)[fNTrackerY2Positions])InSANEHitPosition();
         fNTrackerY2Positions++;
         return(hitpos);
      }

      /** Generates a new tracker position. */
      InSANEHitPosition * AddTrackerY1Position() {
         auto * hitpos = new((*fTrackerY1Positions)[fNTrackerY1Positions])InSANEHitPosition();
         fNTrackerY1Positions++;
         return(hitpos);
      }


      /** Generates a new target position. */
      InSANEHitPosition * AddTargetPosition() {
         auto * hitpos = new((*fTargetPositions)[fNTargetPositions])InSANEHitPosition();
         fNTargetPositions++;
         return(hitpos);
      }

   public:

      InSANEReconstructedEvent();
      virtual ~InSANEReconstructedEvent();

      virtual void ClearEvent(Option_t * opt = "");

      ClassDef(InSANEReconstructedEvent,7)
};

#endif

