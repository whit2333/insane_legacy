#ifndef BETAPedestalAnalysis_H
#define BETAPedestalAnalysis_H
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TClonesArray.h"
#include "SANEEvents.h"
#include "SANERunManager.h"
#include "InSANERun.h"
#include "InSANEAnalysis.h"
#include "InSANEDatabaseManager.h"
#include "InSANEDetectorPedestal.h"
#include "GasCherenkovHit.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "BIGCALGeometryCalculator.h"

/** Pedestal analysis of the detectors in the BETA detector package
 *
 *  Concrete class implementation of InSANEAnalysis
 *  Looks at the first 1000 events, which are pulser generated
 *  pedestal events.
 *
 * \deprecated
 * 
 * \ingroup Analyses
 *  \ingroup BETA
 */
class BETAPedestalAnalysis : public InSANEAnalysis {
   public :

      /**  Constructor setting the run number.  */
      BETAPedestalAnalysis(const char * sourceTreeName = "betaDetectors");

      ~BETAPedestalAnalysis();

      /**  Run the pedestal analysis for the run. */
      Int_t AnalyzeRun();

      Int_t Visualize() ;
      Int_t CreatePlots() ;
      Int_t FillDatabase();
      Int_t FitHistograms();

      TFile           * fAnalysisFile;
      TChain          * fAnalysisTree;
      TFile           * fOutputFile;
      TTree           * fOutputTree;
      BIGCALGeometryCalculator * bcgeo;

      SANEEvents * fEvents;

      /// Arrays of histograms
      TClonesArray * fCherenkovPedHists;
      TClonesArray * fLucitePedHists;
      TClonesArray * fTrackerPedHists;
      TClonesArray * fBigcalPedHists;
      TClonesArray * fBigcalTimingGroupPedHists;

      /// Arrays of pedestals
      TClonesArray * fCherenkovPeds;
      TClonesArray * fLucitePeds;
      TClonesArray * fTrackerPeds;
      TClonesArray * fBigcalPeds;
      TClonesArray * fBigcalTimingGroupPeds;

      InSANEDetectorPedestal * fPedestals;


      ClassDef(BETAPedestalAnalysis, 2)
};


#endif

