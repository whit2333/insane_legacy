#ifndef BigcalHit_h
#define BigcalHit_h
#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include <TClonesArray.h>
#include "InSANEDetectorHit.h"
#include "InSANERunManager.h"

/** Concrete class for BigCal Hits
 *
 * This class represents a single hit for BigCal.
 * Information contained is block position, section (rcs or protvino)
 * , ADC value, TDC value, energy, etc.
 *
 * fLevel indicates which type of hit it is to be considered as:
 * 0=ADC, 1=TDC(Sum Of 32) , 2=TDC(Sum Of 64) , 3=TDC(master trigger)
 *
 * For each ADC hit (single block), we record all the TDC hits associated with
 * said block. So any cell could have a few TDC values associated with it. For example,
 * if it is part of the over lap of two SumOf32, it will have (ideally) two values  in
 * addition to the two trigger, SumOf64 TDC hits.
 *
 * Group (or Column) for sums of 32 (fTDCLevel=1) or sum of 64 (fTDCLevel=2)
 * used for smallest block timing info.
 * See BIGCALGeometryCalculator::GetTDCGroup
 *
 * \ingroup Hits
 */
class BigcalHit : public InSANEDetectorHit {

   public :

      Int_t    fiCell; 
      Int_t    fjCell;
      Int_t    fTDC;

      Short_t  fTDCLevel; ///  1=(Sum Of 32) , 2=(Sum Of 64) , 3=(master trigger)

      Int_t    fTDCGroup;
      Int_t    fTDCRow;
      Int_t    fTDCRow2;
      Int_t    fADC;
      Double_t fEnergy;
      Int_t    fBlockNumber;
      Int_t    fTDC2;
      Int_t    fTDCAlign;
      Int_t    fTDCAlign2;
      Int_t    fTriggerHalf;

   public :

      BigcalHit();
      virtual ~BigcalHit();

      BigcalHit(const BigcalHit& rhs) : InSANEDetectorHit(rhs) { (*this) = rhs; }

      void Clear(Option_t *opt = "") ;

      BigcalHit& operator=(const BigcalHit &rhs) ;

      BigcalHit & operator+=(const BigcalHit &rhs) ;

      const BigcalHit operator+(const BigcalHit &other) const ;

      ClassDef(BigcalHit, 5)
};
#endif

