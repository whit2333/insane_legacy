#ifndef InSANEDISTrajectory_H
#define InSANEDISTrajectory_H 1

#include "TObject.h"
#include "InSANERunManager.h"
#include "TMath.h"
#include "InSANETrajectory.h"
#include "BIGCALCluster.h"
#include "InSANEFunctionManager.h"
#include "InSANEPhysicalConstants.h"

/** Trajectory for an DIS electron.
 *  This class includes not only kinematics variables but also kinematic functions
 *  used in extracting Compton asymmetries and structure functions.
 *
 *  \todo Add new kinematic functions.
 *
 * OLD: Note Members are used for all kinematic variables
 * NEW: Calling on InSANEFunctionManager
 *
 * \ingroup Events
 * \ingroup physics
 */
class InSANEDISTrajectory : public InSANETrajectory {
   public:

      BIGCALCluster fCluster;
      Int_t      fNClusters;

      TVector3   fPBigcal_B;
      TVector3   fXBigcal_B;
      TVector3   fPBigcal_C;
      TVector3   fXBigcal_C;

      TVector3   fPTracker_B;
      TVector3   fXTracker_B;
      TVector3   fPTracker_C;
      TVector3   fXTracker_C;

      TVector3   fPTarget_B;
      TVector3   fXTarget_B;
      TVector3   fPTarget_C;
      TVector3   fXTarget_C;

      TVector3   fTrackerMiss;
      Int_t      fNTrackerNeighbors;
      TVector3   fLuciteMiss;
      TVector3   fTrackerY1Miss;
      TVector3   fTrackerY2Miss;
      TVector3   fTargetPosition;
      TVector3   fReconTargetY1;
      TVector3   fReconTargetY2;
      TVector3   fReconTarget;
      TVector3   fReconTarget1;
      TVector3   fReconTarget2;
      TVector3   fTrackerPosition;
      Double_t   fLuciteMissDistance;
      Double_t   fBeamEnergy;
      Double_t   fEnergy;
      Double_t   fTheta;
      Double_t   fPhi;
      Int_t      fHelicity;
      Double_t   fDeltaTheta;
      Double_t   fDeltaPhi;
      Double_t   fDeltaEnergy;
      Int_t      fSubDetector;
      Bool_t     fIsGood;
      Double_t   fTrackerDeltaPhi;

   public:
      InSANEDISTrajectory() {
         fHelicity  = 0;
         fTheta = 0.0;
         fPhi = 0.0;
         fDeltaTheta = 0.0;
         fDeltaPhi = 0.0;
         fDeltaEnergy = 0.0;
         fSubDetector = 0;
         fRun = nullptr;
         fBeamEnergy = 0.0;
         Clear();
         fFunctionManager = InSANEFunctionManager::GetInstance();
         fStructureFunctions = fFunctionManager->GetStructureFunctions();
      }

      ~InSANEDISTrajectory() {   }

      void Clear(Option_t * opt = "") {

         fHelicity = 0;
         fEnergy = 0.0;
         fTheta = 0.0;
         fPhi = 0.0;
         fNTrackerNeighbors = 0;
         fDeltaTheta = 0.0;
         fDeltaPhi = 0.0;
         fDeltaEnergy = 0.0;
         fSubDetector = 0;
         fTrackerDeltaPhi = -9.9;
         fIsGood = false;
      }

      /** Set the run object which grabs the beam energy needed for calculations.
       */
      void SetRun(InSANERun * arun) {
         fRun                = arun;
         fBeamEnergy         = arun->GetBeamEnergy();
         fStructureFunctions = fFunctionManager->GetStructureFunctions();
      }

      InSANERun * GetRun() const {
         return(fRun);
      }

      /** @name Kinematic Functions and Coefficients
       *
       *  Kinematic functions and coefficients for electron scattering trajectories
       *  taken from Physics Reports 261(1995) Anselmino et al.
       *
       *  \f[ A_{\parallel} = D(A_1 + \eta A_2) \f]
       *
       *  \f[ A_{\perp} = d(A_2 - \xi A_1) \f]
       *
       *  \f[
       *      A_1 = \frac{ M \nu G_{1} - Q^{2} G_{2} }{ W_{1} }
       *  \f]
       *
       *  \f[
       *      A_1 = \frac{ g_{1} - (4 M^{2} x^{2} / \sqrt{ Q^2 } ) g_{2} }{F_{1}}
       *  \f]
       *
       *  \f[
       *      A_2 = \sqrt{Q^2} \frac{M  G_1 + \nu G_2 }{W_1}
       *  \f]
       *
       *  \f[
       *      A_2 =  \frac{2Mx}{\sqrt{Q^{2}} } \frac{g_{1} + g_{2} }{F_{1}}
       *  \f]
       *
       *  For SANE the set up is a bit different. We measured A80 instead of A90. So if we
       *  let the target angle, \f$ \alpha \f$, be arbitrary we get:
       *  \f[
       A_1 = \left(\frac{1}{1+\eta \zeta}\right) \left[ A_{180}\left(\frac{\zeta -\cot(\alpha)\chi}{D}\right) + A_{\alpha}\left( \frac{\csc(\alpha) \chi}{D} \right)  \right]
       *  \f]
       * and
       *  \f[
       A_2 = \left(\frac{1}{1+\eta \zeta}\right) \left[ A_{180} \left( \frac{\zeta -\cot(\alpha)\chi/\eta}{D}\right) + A_{\alpha} \left( \frac{\zeta  - \cot(\alpha) \chi/\eta}{D( \cos(\alpha) - \sin(\alpha) \cos^2(\phi) \chi) } \right)  \right]
       *  \f]
       *
       *
       *  @{
       **/

      /** \f$ Q^2 = 4EE^{\prime} Sin^2(\theta/2) \f$ */
      Double_t GetQSquared()  const {
         return(4.0 * fEnergy * GetBeamEnergy() * TMath::Sin(fTheta / 2.0) * TMath::Sin(fTheta / 2.0)) ;
      }

      /** \f$ \nu = E-E^{\prime} \f$ */
      Double_t GetPhotonEnergy() const {
         return(GetBeamEnergy() - fEnergy);
      }

      /** \f$ x = Q^2/2M\nu \f$ */
      Double_t GetBjorkenX()  const {
         return(GetQSquared() / (2.0 * (M_p/MeV) * GetPhotonEnergy()));
      }

      /** \f$ W^2 = M_{p}^{2}+2 \nu M_p - Q^2  \f$ */
      Double_t GetInvariantMassSquared()  const {
         return(2.0 * GetPhotonEnergy() * (M_p/MeV) - GetQSquared() + (M_p/MeV) * (M_p/MeV));
      }

      /** Beam energy,  \f$ E_{beam} = E \f$ */
      Double_t GetBeamEnergy() const {
         return(fBeamEnergy);
      }

      /** \f$ R = \frac{W_{1}}{W_{2}}(1+\frac{\nu^{2}}{Q^{2}})-1 \f$
       *  \f$ R = \frac{F_2}{2xF_{1}}(1+\frac{4M^2 x^2}{Q^2})-1  \f$
       */
      Double_t GetR() const {
         return(fStructureFunctions->F2p(GetBjorkenX(), GetQSquared()) /
               (2.0 * GetBjorkenX() * fStructureFunctions->F1p(GetBjorkenX(), GetQSquared())) *
               (1.0 + 4.0 * (M_p/MeV) * (M_p/MeV) * TMath::Power(GetBjorkenX(), 2) / GetQSquared()) - 1.0);
      }

      /** \f$ D = \frac{E-\epsilon E^{\prime} }{E(1+\epsilon R)} \f$ */
      Double_t GetD()  const {
         return((GetBeamEnergy() - GetEpsilon() * fEnergy) / (GetBeamEnergy() * (1.0 + GetEpsilon() * GetR()))) ;
      }

      /** \f$ d = D \sqrt{ \frac{2 \epsilon}{1+\epsilon} } \f$ */
      Double_t Getd()  const {
         return(GetD() * TMath::Sqrt(2.0 * GetEpsilon() / (1.0 + GetEpsilon())));
      }

      /** \f$ \eta =  \epsilon \frac{\sqrt{Q^2}}{E - \epsilon E^{\prime}} \f$ */
      Double_t GetEta()  const {
         return(GetEpsilon() * TMath::Sqrt(GetQSquared()) / (GetBeamEnergy() - GetEpsilon() * fEnergy));
      }

      /** \f$ \xi = \eta \frac{ 1+\epsilon}{2\epsilon} \f$ */
      Double_t GetXi()  const {
         return(GetEta() * (1.0 + GetEpsilon()) / (2.0 * GetEpsilon()));
      }

      /** \f$ 1/\epsilon = 1+ 2(1+\frac{\nu^{2}}{Q^{2}})tan^2(\theta/2) \f$ */
      Double_t GetOneOverEpsilon() const {
         return(1.0 +
               2.0 * (1.0 + GetPhotonEnergy() * GetPhotonEnergy() / GetQSquared())
               * TMath::Power(TMath::Tan(fTheta / 2.0), 2));
      }

      /** \f$ \epsilon = 1/( 1+ 2(1+\frac{\nu^{2}}{Q^{2}})tan^2(\theta/2) ) \f$ */
      Double_t GetEpsilon() const {
         return(1.0 / GetOneOverEpsilon());
      }

      /** New variable for calculating Virtual Compton Scattering Asymmetries using A180 and Aperp,
       *  where Aperp is not 90 degrees but some other arbitray angle, alpha.
       *  \f$ \chi = \frac{E^{'} \sin(\theta) \sec(\phi)}{E-E^{'} \cos(\theta)} \f$
       */
      Double_t GetChi() const {
         return((fEnergy * TMath::Sin(fTheta) * (1.0 / TMath::Cos(fPhi))) / (GetBeamEnergy() - fEnergy * TMath::Cos(fTheta)));
      }

      //@}

   protected:
      InSANERun * fRun; //!
      InSANEStructureFunctions * fStructureFunctions; //!
      InSANEFunctionManager * fFunctionManager; //!



      ClassDef(InSANEDISTrajectory, 15)
};

#endif

