#ifndef InSANEHitPosition_HH
#define InSANEHitPosition_HH 1

#include "TVector3.h"
#include "TObject.h"
#include <iostream>
#include <vector>

/** A position derived from a detector hit (or pair of hits).
 */
class InSANEHitPosition : public TObject {

   public:
      TVector3  fSeedingPosition;       /// Position calculated using some other projected position
      TVector3  fPosition;              /// Actual position calculated from hit
      TVector3  fPositionUncertainty;   /// Difference between Position and SeedingPosition
      Double_t  fMissDistance;          /// Distance between hitpoint and straightline from cluster to origin.
      Int_t     fHitNumber;             /// Hit number used to calculate position.
      Int_t     fClusterNumber;         /// Cluster number used to calculate seeding position
      Double_t  fTiming;                /// Timing of hit.
      Double_t  fNNeighbors;            /// Number of Neighbor hits

   public:
      InSANEHitPosition();
      InSANEHitPosition& operator=(const InSANEHitPosition& v);
      InSANEHitPosition(const InSANEHitPosition& v);

      Bool_t IsSortable() const { return true; }
      virtual ~InSANEHitPosition();
      Int_t   Compare(const TObject *obj) const ;

      bool operator==(const InSANEHitPosition &rhs) const ;
      bool operator!=(const InSANEHitPosition &rhs) const ;
      bool operator< (const InSANEHitPosition &rhs) const ;
      bool operator> (const InSANEHitPosition &rhs) const ;
      bool operator<=(const InSANEHitPosition &rhs) const ;
      bool operator>=(const InSANEHitPosition &rhs) const ;

      ClassDef(InSANEHitPosition, 7)
};



#endif

