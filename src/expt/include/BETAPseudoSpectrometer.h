#ifndef BETAPseudoSpectrometer_HH
#define BETAPseudoSpectrometer_HH
#include "InSANEDetector.h"
#include "InSANECherenkovMirror.h"
#include "BETACalculation.h"
#include "InSANECherenkovMirror.h"
#include "GasCherenkovGeometryCalculator.h"
#include "InSANEDISTrajectory.h"
#include "InSANERunManager.h"
#include "InSANEAnalyzer.h"
#include "InSANEAnalysis.h"
#include "InSANEAsymmetry.h"
#include "TClonesArray.h"
#include "InSANEDISEvent.h"

/** A Pseudo-Spectrometer class for parts of BETA.
 *  Each Pseudo spectrometer includes one cherenkov mirror,
 *  a subgroup of lucite bars, a subgroup of bigcal blocks.
 *  Furthermore, like a spectrometer, it has a momentum acceptance
 *  (actually it is energy). So this will act like a setting;
 *
 *  Mirror number should be set if the specNumber does not match the mirrornumber
 *
 *  \ingroup BETA
 */
class BETAPseudoSpectrometer : public InSANEDetector {
   public:
      BETAPseudoSpectrometer(Int_t specNumber = 0, Int_t mirrorNumber = 0) {
         fNumber  = specNumber;
         fSpectrometerNumber = specNumber;
         fMirror = new InSANECherenkovMirror(mirrorNumber);
         fEnergyBite    = 200.0; // MeV
         fCentralEnergy = 1000.0; // MeV
         fSolidAngle    = 0.0175; // Sr
      }

      ~BETAPseudoSpectrometer() {
         ;
      }

      Int_t ClearEvent() {
         fMirror->Clear();
         fCounts = 0;
         fCharge = 0.0;
         return(0);
      }

      virtual Int_t Build() {
         SetBuilt();
         return(0);
      }
      virtual void Initialize() {
         if (!IsBuilt()) Build();
      }


      Int_t fNumber;
      Int_t fCounts;
      Double_t fCharge;

   public:
      /** Process the trajectory event
       */
      Int_t ProcessEvent();

      /** Called at the end of the processing the events
       */
      Int_t PostProcessAction() {
         return(0);
      }

      /// The spectrometer number is the same as the cherenkov mirror number (1-8)
      Int_t fSpectrometerNumber;

      void SetTriggerEvent(InSANETriggerEvent* ev) {
         fTriggerEvent = ev;
      }
      void SetTrajectoryEvent(InSANEDISTrajectory* ev) {
         fTrajectory = ev;
      }
      InSANEDISTrajectory * fTrajectory;
      InSANETriggerEvent * fTriggerEvent;
      InSANECherenkovMirror * fMirror;

      /** @name Spectrometer Settings
       *  @{
       */

   public:
      Double_t GetCentralEnergy() {
         return fCentralEnergy;
      }
      /** Sets the central energy for the (pseudo)spectromter  */
      void     SetCentralEnergy(Double_t val) {
         fCentralEnergy = val;
      }

      Double_t GetEnergyBite() {
         return fEnergyBite;
      }
      /** Sets the energy bite of the spectromter
       *  min=fCentralEnergy-fEnergyBite/2.0
       *  max=fCentralEnergy+fEnergyBite/2.0
       */
      void     SetEnergyBite(Double_t val) {
         fEnergyBite = val;
      }

      Double_t GetSolidAngle() {
         return fSolidAngle;
      }
      void     SetSolidAngle(Double_t val) {
         fSolidAngle = val;
      }
      //@}

   private:
      Double_t fCherenkovCut;
      Double_t f2CherenkovCut;
      Double_t fEnergyBite; // not momentum because we are not bending(yet)
      Double_t fCentralEnergy;
      Double_t fSolidAngle;

      ClassDef(BETAPseudoSpectrometer, 1)
};




/** Trajectory Analysis
 * Simple class sets up input tree branchs.
 * - Creates trajectory and trigger event objects
 * - Sets their branch addresses.
 * - Creates the scaler analysis event object, and thus tree...
 *
 * \ingroup Analysis
 */
class InSANETrajectoryAnalysis : virtual public InSANEAnalysis {

   public:
      InSANETriggerEvent     * trigEvent;

   public:
      InSANETrajectoryAnalysis(const char * sourceTreeName = "trajectories") : InSANEAnalysis(sourceTreeName) {
         std::cout << " o InSANETrajectoryAnalysis Constructor\n";
         fDebug = 3;
         fOutputTree  = nullptr;
         fAnalysisTree = nullptr;
         auto * runManager = (SANERunManager*) SANERunManager::GetRunManager();
         fRunNumber  = runManager->GetCurrentRun()->GetRunNumber();
         fAnalysisFile     = runManager->GetCurrentFile();
         if (fAnalysisFile)   fAnalysisFile->cd();
         /// Gets the source tree for the analysis
         fAnalysisTree = (TTree*)gROOT->FindObject(sourceTreeName);
         if (!fAnalysisTree) printf("x- Tree:%s \nx-        was NOT FOUND!! (from InSANEDetectorAnalysis c'tor) \n", sourceTreeName);
         /// Seting the event addresses
         trigEvent = new InSANETriggerEvent();
         fTrajectory = new InSANEDISTrajectory();
         if (fAnalysisTree) {
            /// Trig branch
            TBranch * trigBranch = fAnalysisTree->GetBranch("triggerEvent");
            if (trigBranch)  trigBranch->SetAddress(&trigEvent);
            else {
               std::cout << " x Could not find branch triggerEvent in tree" << sourceTreeName << "!\n" ;
            }
            /// trajectory branch
            TBranch * trajectoryBranch = fAnalysisTree->GetBranch("trajectory");
            if (trajectoryBranch)  trajectoryBranch->SetAddress(&fTrajectory);
            else {
               std::cout << " x Could not find branch trajectory in tree" << sourceTreeName << "!\n" ;
            }
         } else {
            std::cout << " x Could not find tree " << sourceTreeName << "!\n" ;
         }
         printf(" + Creating SANE Scalers\n");
         fScalers = new SANEScalers();
         fPseudoSpectrometers = new std::vector<BETAPseudoSpectrometer*>;
         fPseudoSpectrometers->clear();
         BETAPseudoSpectrometer* aSpec;
         for (int i = 1; i <= 8; i++) {
            aSpec = new BETAPseudoSpectrometer(i, i);
            fPseudoSpectrometers->push_back(aSpec);
            aSpec->SetTrajectoryEvent(fTrajectory);
            aSpec->SetTriggerEvent(trigEvent);
         }
         for (int i = 9; i <= 16; i++) {
            aSpec = new BETAPseudoSpectrometer(i, i - 8);
            fPseudoSpectrometers->push_back(aSpec);
            aSpec->SetTrajectoryEvent(fTrajectory);
            aSpec->SetTriggerEvent(trigEvent);
            aSpec->SetCentralEnergy(800.0);
         }
         for (int i = 17; i <= 24; i++) {
            aSpec = new BETAPseudoSpectrometer(i, i - 16);
            fPseudoSpectrometers->push_back(aSpec);
            aSpec->SetTrajectoryEvent(fTrajectory);
            aSpec->SetTriggerEvent(trigEvent);
            aSpec->SetCentralEnergy(1200.0);
         }
         for (int i = 25; i <= 32; i++) {
            aSpec = new BETAPseudoSpectrometer(i, i - 24);
            fPseudoSpectrometers->push_back(aSpec);
            aSpec->SetTrajectoryEvent(fTrajectory);
            aSpec->SetTriggerEvent(trigEvent);
            aSpec->SetCentralEnergy(1400.0);
            aSpec->SetEnergyBite(400.0);
         }
      }

      ~InSANETrajectoryAnalysis() { }

      std::vector<BETAPseudoSpectrometer*> * fPseudoSpectrometers;

      void AddSpectrometer(BETAPseudoSpectrometer* spec) {
         fPseudoSpectrometers->push_back(spec);
         spec->SetTrajectoryEvent(fTrajectory);
         spec->SetTriggerEvent(trigEvent);
      }

   public:
      InSANEDISTrajectory * fTrajectory;
      //    BETAPseudoSpectrometer * fPseudoSpectrometers[8];

      ClassDef(InSANETrajectoryAnalysis, 1)
};



/** Trajectory Analyzer
 *  The input tree should have the following branches
 *  - "triggerEvent", InSANETriggerEvent
 *  - "trajectory", InSANEDISTrajectory
 *
 *  \ingroup Analyzers
 */
class SANETrajectoryAnalyzer :  public InSANETrajectoryAnalysis , public InSANEAnalyzer  {
   public:

      SANETrajectoryAnalyzer(const char * newTreeName = "newTrajectoryTree",
            const char * uncorrectedTreeName = "oldTrajectoryTree")
         : InSANETrajectoryAnalysis(uncorrectedTreeName) {
            if (SANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o SANETrajectoryAnalyzer Constructor\n";
            SANERunManager::GetRunManager()->GetCurrentFile()->cd();
            fInputTree = nullptr;
            if (fAnalysisTree) fInputTree = fAnalysisTree; // anlysis tree set in InSANEDetectorAnalysis
            if (!fInputTree) fInputTree = (TTree*)gROOT->FindObject(uncorrectedTreeName);
            if (!fInputTree) printf("x- Tree:%s  was NOT FOUND! (from SANEFirstPassAnalyzer c'tor)  \n", uncorrectedTreeName);

            if (!fScalerTree) fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName);
            fOutputFile = SANERunManager::GetRunManager()->GetCurrentFile();
         }

      virtual ~SANETrajectoryAnalyzer() {
         if (fCalculatedTree) {
            /*        std::cout<< " - Building First Pass Index for corrected tree\n";*/
            fCalculatedTree->Write(fCalculatedTree->GetName(), kWriteDelete);
            fCalculatedTree->FlushBaskets();
         }
         //SANERunManager::GetRunManager()->GetCurrentRun()->fAnalysisPass = 3;
         SANERunManager::GetRunManager()->WriteRun();
         SANERunManager::GetRunManager()->GetRunReport()->UpdateRunDatabase();

         SANERunManager::GetRunManager()->GetCurrentFile()->Flush();
         SANERunManager::GetRunManager()->GetCurrentFile()->Write();

         if (SANERunManager::GetRunManager()->GetVerbosity() > 1)  printf("\n o SANETrajectoryAnalyzers destructor \n");
      }

      virtual void Initialize(TTree * inTree = nullptr) {
         if (SANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o SANEFirstPassAnalyzer::Initialize(TTree *) \n";
         if (inTree)fInputTree = inTree;
         fCalculatedTree = fInputTree->CloneTree(0);
         fCalculatedTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s", fCalculatedTreeName.Data()));
         /*    if(fScalerTree) fOutputScalerTree = fScalerTree->CloneTree(0);*/
         //     fOutputScalerTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s",fCalculatedTreeName.Data() ));

         InitCorrections();

         InitCalculations();

         /// Important: Set the event to be used for triggering in the InSANEAnalyzer
         /*   InSANEAnalyzer::SetTriggerEvent(fScalers->fTriggerEvent); // does nothing*/
         InSANEAnalyzer::SetDetectorTriggerEvent(trigEvent);
         InSANEAnalyzer::SetScalerTriggerEvent(fScalers->fTriggerEvent);
      }

      /** Fill Trees then clear all event data
       */
      virtual void FillTrees() {
         if (fCalculatedTree)fCalculatedTree->Fill();
      }

      /**  Needed to clear event from InSANEAnalyzer::Process() or InSANECorrector::Process()
       *
       */
      void ClearEvents() {
         //       fClusterEvent->ClearEvent();
         //       fEvents->BETA->ClearEvent();
         //       fClusters->Clear("C");
      }

      /**  Executes post-first pass analysis scripts
       *
       *  \todo Make a list of script names that are to be executed instead of
       *  one long method
       */
      void MakePlots();



      ClassDef(SANETrajectoryAnalyzer, 1)
};


/**  Calculates the cross section measured
 *
 * \ingroup Calculations
 */
class BETAPseudoSpectrometersCalc : public InSANECalculation {
   public:

      BETAPseudoSpectrometersCalc(InSANEAnalysis * analysis) : InSANECalculation(analysis) {
         fDISEvent = new InSANEDISEvent();
      }

      ~BETAPseudoSpectrometersCalc() {
         if (fOutTree) fOutTree->FlushBaskets();
         if (fOutTree) fOutTree->Write();
      }

   public:
      InSANEDISTrajectory * fTrajectory;
      InSANETriggerEvent * fTriggerEvent;
      InSANEDISEvent * fDISEvent;

      TH2F * hQvsxRawAsym;
      TH2F * hQvsxCoefficientD;
      TH2F * hQvsxCoefficientd;
      TH2F * hQvsxCoefficientEta;
      TH2F * hQvsxCoefficientEpsilon;
      TH2F * hQvsxCoefficientXi;

      /// Detector object used to also set the event objects
      BETAPseudoSpectrometer * fPseudoSpectrometer;

      /// Asymmetry added to run object
      InSANEAsymmetry * fAsymmetry;

      /** sets the detector object and grabs the event objects */
      void SetSpectrometerDetector(BETAPseudoSpectrometer * spec) {
         fPseudoSpectrometer = spec ;
         fTrajectory = spec->fTrajectory ;
         fTriggerEvent = spec->fTriggerEvent;
      }

      //   void SetDetectors(InSANETrajectoryAnalysis * analysis) {
      //      fPseudoSpectrometers = &(analysis->fPseudoSpectrometers[0]);
      //      fTrajectory = analysis->fTrajectory;
      //      fTriggerEvent = analysis->trigEvent;
      //   }

      /** Method prints a description of the calculations implemented */
      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  BETAPseudoSpectrometersCalc - \n ";
         std::cout <<  "     Calculates a cross-section/asymmetry for each pseudo-spectrometer. \n";
         std::cout <<  "     Most generally, a pseudo-spectrometer (BETAPseudoSpectrometer) is a \n";
         std::cout <<  "     cherenkov mirror and its corresponding centrally located calorimeter \n";
         std::cout <<  "     blocks.\n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

      virtual Int_t Initialize() {
         if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pseudoSpectrometers"))
            InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("pseudoSpectrometers");
         InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pseudoSpectrometers");

         TObjArray * asymmetries = &(InSANERunManager::GetRunManager()->GetCurrentRun()->fAsymmetries);
         //fAsymmetry = new((*asymmetries)[asymmetries->GetEntries()]) InSANEAsymmetry(fPseudoSpectrometer->fNumber);
         fAsymmetry = new InSANEAsymmetry(fPseudoSpectrometer->fNumber);
         //      fAsymmetry->SetHelicityBit(&(fTrajectory->fHelicity));
         //      fAsymmetry->SetQsquaredAddress(&(fQsquared));
         //      fAsymmetry->SetxBjorkenAddress(&(fxBjorken));
         fAsymmetry->fDISEvent = fDISEvent;
         asymmetries->Add(fAsymmetry);
         fTrajectory->SetRun(InSANERunManager::GetRunManager()->GetCurrentRun());

         fOutTree = new TTree(Form("pseudoSpec%d", fPseudoSpectrometer->fNumber), "pseudoSpec");
         fOutTree->Branch("InSANEDISTrajectory", &fTrajectory);

         InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
         return(0);
      }

      Bool_t fGoodEvent;
      virtual void FillTree() {
         if (fOutTree) if (fGoodEvent) fOutTree->Fill();
         if (fGoodEvent) {
            fAsymmetry->CountEvent();
         }
         fGoodEvent = false;
      }

      void InitHistograms() { }

      Int_t PostProcessAction() {
         return(0);
      }

      /** First Pass Clustering of each event.
       *
       *  The clustering is executed for <b>all BETA Events</b>
       *  For each event the following is done:
       *   - Calorimeter clustering using the clustering algorithm
       *   -
       */
      virtual Int_t Calculate() {
         if (fPseudoSpectrometer->fMirror->fIsHit)
            if (TMath::Abs(fTrajectory->fEnergy - fPseudoSpectrometer->GetCentralEnergy()) < fPseudoSpectrometer->GetEnergyBite() / 2.0) {
               fGoodEvent = true;
               fQsquared = fTrajectory->GetQSquared();
               fxBjorken = fTrajectory->GetBjorkenX();
            }
         return(0);
      }
      //   Double_t fCherenkovNPECut;
      //   Double_t fEnergyCut;

      Double_t fxBjorken;
      Double_t fQsquared;

      //_______________________________________________________//
      ClassDef(BETAPseudoSpectrometersCalc, 1)
};






#endif

