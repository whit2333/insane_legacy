#ifndef BETAG4MonteCarloEvent_H
#define BETAG4MonteCarloEvent_H 1

#include <iostream>
#include "TTree.h"
#include "TClonesArray.h"
#include "InSANERunManager.h"
#include "InSANEFakePlaneHit.h"
#include "InSANEDetectorEvent.h"
#include "InSANEParticle.h"

/** Concrete implementation for all the Monte Carlo event information from BETAG4.
 * An Event class for monte carlo "thrown" event information
 * and other unrealistic data.
 * \ingroup Events
 */
class BETAG4MonteCarloEvent : public InSANEDetectorEvent {

   public:
      Int_t          fNumberOfParticlesThrown;
      TClonesArray * fTrackerPlaneHits; //-> array of tracker hits
      TClonesArray * fTrackerPlane2Hits; //-> array of tracker hits
      TClonesArray * fBigcalPlaneHits; //-> array of biZ
      TClonesArray * fThrownParticles; //->

   public :
      BETAG4MonteCarloEvent();
      virtual ~BETAG4MonteCarloEvent();

      TClonesArray * GetTrackerPlaneHitsArray() { return(fTrackerPlaneHits); }
      TClonesArray * GetTrackerPlane2HitsArray() { return(fTrackerPlane2Hits); }
      TClonesArray * GetBigcalPlaneHitsArray() { return(fBigcalPlaneHits); }
      TClonesArray * GetThrownParticlesArray() { return(fThrownParticles); }

   public:
      Int_t AllocateMemory();

      /**
       *  Allocates the memory and sets the pointers
       *   for each Detector Hit Array
       */
      Int_t AllocateHitsMemory();

      /// This adds to the tree in the argument a branch for each of
      /// BETA detector's HitsArray,
      /// \todo{
      /// Ideally we would want to not stream the array from the Event class
      /// but use this instead so that we don't have to stream those data members}
      Int_t AddHitsBranch(TTree * t);

      /// Sets The Appropriate branch addresses
      Int_t SetHitsBranch(TTree * t);

      /// Free hits and events memory
      Int_t FreeMemory();

      virtual void ClearEvent(Option_t * opt = "");

      ClassDef(BETAG4MonteCarloEvent, 8)
};

#endif
