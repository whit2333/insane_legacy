#ifndef LuciteHodoscopeBar_H
#define LuciteHodoscopeBar_H
#include <TROOT.h>
#include <TNamed.h>
#include <TClonesArray.h>
#include "InSANEDetectorComponent.h"
#include "TF1.h"

/** Lucite Hodoscope Bar has all associated calibrations.
 *
 *  Calibration should be retrieved from
 *  hodoscope/barCalibrations/objectarray[index]
 *  where the TObjArray is an array of LuciteHodoscopeBar objects
 *
 * \ingroup hodoscope
 */

class LuciteHodoscopeBar : public InSANEDetectorComponent {
public :
   LuciteHodoscopeBar() {
      fTDCDifferenceOffset = 0.0;
      GetTiming();
      //fHasTDCValue = true;
      //fTiming = new InSANEDetectorTiming();
   }

   virtual  ~LuciteHodoscopeBar() { }

   /**
    * Returns the number of pedestals for the detector
    */
// Int_t GetNumberOfPedestals() { return(fNumberOfPedestals); };

   /**
    * Add a pedestal to TClonesArray of pedestals
    */
//Bool_t AddPedestal(Int_t id, Double_t val, Double_t width);

   /**
    *  Set an already added pedestal value
    */
//Bool_t SetPedestal(Int_t id, Double_t val, Double_t width);

   /**
    * Print the pedestals for the detector
    */
//void PrintPedestals();

   /**
    * Set the number of pedestals... not really useful?
    */
//void SetNumberOfPedestals(Int_t num) { fNumberOfPedestals=num; };

   /**
    *
    */

   Double_t fTDCDifferenceOffset;
//    TF1 fXPositionFromCluster;


//  Double_t fNominalTDCCutWidth;
//  TH2F * fDetectorEventDisplay;
private :



//LuciteHodoscopeBarGeometry * fLuciteHodoscopeBarGeometry;


   ClassDef(LuciteHodoscopeBar, 2)
};



#endif
