#ifndef BETAG4Analysis_H
#define BETAG4Analysis_H
#include "TROOT.h"

#include "InSANEAnalysis.h"

#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "SANEEvents.h"
#include "BigcalEvent.h"

#include "InSANEDatabaseManager.h"

#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"
#include <exception>
#include "InSANERun.h"

/**  ABC Class for dealing with a montecarlo simulation analysis
 *
 *  ... ??? not a good idea???
 *
 * \ingroup Analyses
 */
class BETAG4Analysis : public InSANEAnalysis {
public :

   BETAG4Analysis(const char * sourceTreeName = "");

   BETAG4Analysis(Int_t run);

   virtual ~BETAG4Analysis();

   /**
    * Go through and produce the standard plots
    */
   virtual Int_t AnalyzeRun();

   /**
    * Go through and produce the standard plots
    */
   virtual Int_t Visualize() ;

   TFile  * fAnalysisFile;
   TChain * fAnalysisTree;
   TFile  * fOutputFile;
   TTree  * fOutputTree;

   InSANERun * fRun;

   /**
    * Allocates and defines histograms
    */
   virtual Int_t  AllocateHistograms() {
      return(0) ;
   }

/// The following are all
/// pointers to the memory allocated by new SANEEvents
   SANEEvents * events;
   BETAEvent * aBetaEvent;
   BigcalEvent * bcEvent;
   BigcalHit * aBigcalHit;
   LuciteHodoscopeEvent * lhEvent;
   LuciteHodoscopeHit * aLucHit;
   GasCherenkovEvent * gcEvent;
   GasCherenkovHit * aCerHit;
   ForwardTrackerEvent * ftEvent;
   ForwardTrackerHit * aTrackerHit;

   HallCBeamEvent        * beamEvent;
   HMSEvent              * hmsEvent;
   InSANEMonteCarloEvent * mcEvent;
   InSANETriggerEvent    * trigEvent;

   BIGCALGeometryCalculator * bcgeo;
   //InSANEDatabaseManager * dbManager;

private:

////// HISTOGRAMS ///////
//   TH1F * cer_tdc_hist[12];
//   TH1F * cer_adc_hist[12];
//   TH2F * cer_tdc_vs_adc_hist[12];
//
//   TH1F * cer_tdc_hist_luc[12*56];
//   TH1F * cer_adc_hist_luc[12*56];
//   TH2F * cer_tdc_vs_adc_hist_luc[12*56];
//
//   TH1F * luc_tdc_hist[12*56];
//   TH1F * luc_adc_hist[12*56];
//   TH2F * luc_tdc_vs_adc_hist[12*56];

   ClassDef(BETAG4Analysis, 1);
};


#endif

