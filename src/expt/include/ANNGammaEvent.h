#ifndef ANNGammaEvent_HH
#define ANNGammaEvent_HH 1

#include "TObject.h"
#include "TString.h"
#include "BIGCALCluster.h"

class ANNGammaEvent : public TObject {

   protected:
      TString  fInputNeurons;

   public:
      Double_t fTrueEnergy;
      Double_t fDelta_Energy;
      Double_t fTrueTheta;
      Double_t fDelta_Theta;
      Double_t fTruePhi;
      Double_t fDelta_Phi;
      Bool_t   fBackground;
      Bool_t   fSignal;
      Double_t fClusterTheta;
      Double_t fClusterPhi;
      Double_t fXCluster;
      Double_t fYCluster;
      Double_t fXClusterSigma;
      Double_t fYClusterSigma;
      Double_t fXClusterSkew;
      Double_t fYClusterSkew;
      Double_t fClusterEnergy;
      Double_t fCherenkovADC;
      Bool_t   fGoodCherenkovHit;
      Int_t    fNClusters;
      Double_t fXClusterKurt;
      Double_t fYClusterKurt;
      Double_t fClusterDeltaX;
      Double_t fClusterDeltaY;

   public:
      ANNGammaEvent();
      ~ANNGammaEvent();

      Int_t         GetNInputNeurons() const { return(9); }
      const char *  GetMLPInputNeurons();
      void          GetMLPInputNeuronArray(Double_t * par);

      void          Clear();

      void           SetEventValues(BIGCALCluster * clust);

   ClassDef(ANNGammaEvent,3)
};


#endif

