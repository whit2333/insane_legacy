#ifndef InSANECoordinateSystem_h
#define InSANECoordinateSystem_h

#include <TROOT.h>
// //#include <TChain.h>
#include <TFile.h>
#include <TNamed.h>
#include "TRotation.h"
#include "TVector3.h"
#include "TMath.h"
#include <iostream>
#include "TEveManager.h"
#include "TEveStraightLineSet.h"
#include "TEveVector.h"
#include "TEveArrow.h"
#include "TEveText.h"

/** ABC of a coordinate system.
 *
 *  This class defines the minimum functions
 *  necessary to define a coordinate system.
 *
 *  Defining an absolute coordinate system sounds like a bad idea.
 *  so this Abstract Base Class provides a way of describing the coordinate
 *  system (in words printed to screen) and transformation matricies from one relative to another.
 *  Thus by defining two coordinate systems, we gain usefulness.
 *
 * \note you need not know about all other coordinate systems, just a path to get to the relevent ones.
 *
 * \ingroup Coordinates
 */
class InSANECoordinateSystem  : public TNamed {

   protected :
      TRotation             fRotationToDefault;
      TRotation             fRotationFromDefault;
      TRotation             fRotationMatrix;
      TVector3              fTranslation;
      TEveStraightLineSet * fEveLineSet; //!
      TEveVector  *         fVertex;     //!
      TVector3              fXaxis;
      TVector3              fYaxis;
      TVector3              fZaxis;
      TVector3              fOrigin;
      Double_t              fAxisLength;
      Int_t                 fAxisLabelSize;

   public :
      InSANECoordinateSystem(const char * name = "InSANECoordinateSystem");
      virtual ~InSANECoordinateSystem();

      /** pure virtual method returning a reference to the rotation matrix */
      virtual TRotation & GetRotationMatrixTo(const char * system  = "humanCoords") = 0;

      /** pure virtual method returning a reference to the rotation matrix */
      virtual TVector3 & GetOriginTranslationTo(const char * system  = "humanCoords") = 0;

      /** pure virtual method which, when implemented, should print
       *  a description of the coordintate system
       */
      virtual void DescribeCoordinateSystem() = 0;

      /**  Uses ROOT's Eve library to draw the coordinate systems
       */
      void DrawCoordinateSystem(const char * coordsys = nullptr);

      ClassDef(InSANECoordinateSystem, 3)
};

#endif

