#ifndef LuciteHodoscopeHit_h
#define LuciteHodoscopeHit_h

#include "InSANEDetectorHit.h"
#include "InSANERunManager.h"

#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include <iostream>

/** Lucite Hodoscope Hit
 *
 * \ingroup Hits
 * \ingroup hodoscope
 */
class LuciteHodoscopeHit : public InSANEDetectorHit {

   public :
      LuciteHodoscopeHit() {
         Clear() ;
      }
      ~LuciteHodoscopeHit() { }

      LuciteHodoscopeHit(const LuciteHodoscopeHit& rhs) : InSANEDetectorHit(rhs) {
         (*this) = rhs;
      }

      void  Clear(Option_t *opt = "") {
         /*      if(InSANERunManager::GetRunManager()->fVerbosity > 2) Print();*/
         InSANEDetectorHit::Clear(opt);
         fADC = 0;
         fPMTNumber = 0;
         fRow = -1;
         fTDCHitNumber = 0;
         fPosition = 0;
         fTDC = -9999;
         fTDC2 = -9999;
         fTDCAlign = -9999;
      }

      Int_t fTDCHitNumber;
      Int_t fRow;
      Double_t fPosition;

      Int_t fPMTNumber;
      Int_t fTDC;
      Int_t fADC;
      Int_t fTDC2;
      Int_t fTDCAlign;



      void Print(Option_t * opt = "") const {
         std::cout << "++ LuciteHodoscopeHit " << this << std::endl;
         std::cout << "    - fTDC  = " << fTDC << std::endl;
         std::cout << "    - fPMTNumber  = " << fPMTNumber << std::endl;
         std::cout << "    - fHitNumber  = " << fHitNumber << std::endl;
         std::cout << "    - fTDCHitNumber  = " << fTDCHitNumber << std::endl;
         std::cout << "    - fTDCAlign  = " << fTDCAlign << std::endl;
         std::cout << "    - fRow  = " << fRow << std::endl;
         std::cout << "    - fPosition  = " << fPosition << std::endl;
      }

      LuciteHodoscopeHit& operator=(const LuciteHodoscopeHit &rhs) {
         // Check for self-assignment!
         if (this == &rhs)      // Same object?
            return *this;        // Yes, so skip assignment, and just return *this.
         // Deallocate, allocate new space, copy values...

         InSANEDetectorHit::operator=(rhs);

         fLevel           = rhs.fLevel;
         fHitNumber       = rhs.fHitNumber;
         fPassesTimingCut = rhs.fPassesTimingCut;

         fTDCHitNumber    = rhs.fTDCHitNumber;
         fRow             = rhs.fRow;
         fPMTNumber       = rhs.fPMTNumber;
         fADC             = rhs.fADC;
         fPosition        = rhs.fPosition;
         fTDC = rhs.fTDC;
         fTDC2 = rhs.fTDC2;
         fTDCAlign = rhs.fTDCAlign;
         return *this;
      }

      LuciteHodoscopeHit & operator+=(const LuciteHodoscopeHit &rhs) {
         fLevel += rhs.fLevel;
         fHitNumber += rhs.fHitNumber;
         fPassesTimingCut += rhs.fPassesTimingCut;

         fTDCHitNumber += rhs.fTDCHitNumber;
         fRow += rhs.fRow;
         fPMTNumber += rhs.fPMTNumber;
         fADC += rhs.fADC;
         fPosition += rhs.fPosition;
         fTDC += rhs.fTDC;
         fTDC2 += rhs.fTDC2;
         fTDCAlign += rhs.fTDCAlign;
         return *this;
      }

      // Add this instance's value to other, and return a new instance
      // with the result.
      const LuciteHodoscopeHit operator+(const LuciteHodoscopeHit &other) const {
         LuciteHodoscopeHit result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
         result += other;            // Use += to add other to the copy.
         return result;              // All done!
      }

      ClassDef(LuciteHodoscopeHit, 6)
};
#endif

