#ifndef BETATimingCalculation_HH
#define BETATimingCalculation_HH 1

#include "BETACalculation.h"

/**
 *
 */
class BETATimingCalculation : public BETACalculation {
public:
   BETATimingCalculation(InSANEAnalysis * analysis = 0) : BETACalculation(analysis) {
   }

   virtual ~BETATimingCalculation() {
   }

   /**   */
   virtual Int_t Initialize() {

      TH2F * h2 = 0;

//       h2 = new TH2F("hTrackerVsBigcal_i","Tracker-i vs Bigcal-i",32,1,33,73,0,73);
//       f2DHistograms.Add(h2);
//       h2 = new TH2F("hTrackerVsBigcal_j","Tracker-j vs Bigcal-j",56,1,57,133,0,133);
//       f2DHistograms.Add(h2);

      return(0);
   }

   virtual Int_t Finalize() {
      return(0);
   }

   virtual Int_t Calculate(){return(0);}

   void Describe() { }

   TList f2DHistograms;
   TList f1DHistograms;


   ClassDef(BETATimingCalculation, 1)
};
#endif
