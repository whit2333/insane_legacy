#ifndef BigcalClusterAnalyzer_HH
#define BigcalClusterAnalyzer_HH 1

#include "BIGCALCluster.h"
#include "InSANEAnalyzer.h"
#include "InSANEAnalysis.h"

class BigcalClusterAnalyzer : public InSANEAnalyzer , public InSANEAnalysis  {
public:
   /**
    */
   BigcalClusterAnalyzer(const char *newTreeName = "pi0Reconstruction", const char *uncorrectedTreeName = "Clusters"):
      InSANEAnalyzer(newTreeName, uncorrectedTreeName) {

   };

   /**
    */
   ~BigcalClusterAnalyzer() {

   };

   /**
    */
   virtual void Initialize(TTree * inTree = nullptr) {
      fInputTree = inTree;

      InitCorrections();

      InitCalculations();
   };

   ClassDef(BigcalClusterAnalyzer, 1)
};


#endif
