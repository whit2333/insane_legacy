#ifndef InSANEDetectorEvent_H
#define InSANEDetectorEvent_H

#include <TROOT.h>
#include <TObject.h>
#include <TClonesArray.h>
// #include "InSANEDetectorHit.h"
#include "InSANEEvent.h"



/** ABC for an event in a detector
 *
 * \ingroup Events
 */
class InSANEDetectorEvent : public InSANEEvent {

   public :
      Bool_t fGoodEvent;
      Int_t  fNumberOfHits;

   public :
      InSANEDetectorEvent();
      virtual ~InSANEDetectorEvent();

      virtual void Print(const Option_t * opt = "") const ;

      /** Implemented for concrete class.
       * Note: when TClonesArray is cleared with Clear("C") the clear method
       * of the hit class is also called. So ClearEvent is not called.
       * \todo should this be just Clear?
       */
      void ClearEvent(Option_t * opt = "");

      ClassDef(InSANEDetectorEvent, 5)
};


#endif

