#ifndef GasCherenkovGeometryCalculator_h
#define GasCherenkovGeometryCalculator_h
#include <iostream>
#include <vector>
#include <TROOT.h>
// //#include <TChain.h>
#include <TF1.h>
#include <TFile.h>
#include <TObject.h>
#include "InSANEGeometryCalculator.h"

/**  Concrete class for SANE gas Cherenkov detector's geometry definitions
 *
 *  \todo This class connects to a database and grabs the dimensions of stuff
 *
 * \ingroup Geometry
 * \ingroup Cherenkov
 */
class GasCherenkovGeometryCalculator  : public InSANEGeometryCalculator {
public :

   /** Returns the pointer to the static global variable.
    */
   static  GasCherenkovGeometryCalculator * GetCalculator();

   ~GasCherenkovGeometryCalculator();

   /** returns the horizontal index for the mirror (1 or 2). Note the mirror number is from (1-8)
    *  returns 0 otherwise. BigCal coordinates are used where the even mirrors have i=1.
    */
   Int_t iMirror(Int_t mirror);

   /** returns the vertical index for the mirror (1-4). Note the mirror number is from (1-8)
    *  returns 0 otherwise.
    */
   Int_t jMirror(Int_t mirror);

   /** Gets the x position of the center of the mirror referenced by number (which goes from 1-8).
    *  Quickly thrown together for visualization... need to check
    */
   Double_t GetXPosition(Int_t mirror) {
      auto i = (Double_t)(iMirror(mirror) - 1) ;
      return((i) * 35.0 - 35.0 / 2.0);
   }

   /** Gets the y position of the center of the mirror referenced by number (which goes from 1-8)
    *  Quickly thrown together for visualization... need to check
    */
   Double_t GetYPosition(Int_t mirror) {
      auto j = (Double_t)(jMirror(mirror) - 1) ;
      return((j) * 35.0 - 3.0 * 35.0 / 2.0);
   }


   /**  Determines whether the mirror number, potentialNeighbor, is neighboring the
    *   mirror number mir. Mirror numbers are in the range (1,8).
    *
    *   \note Note that if mir=potentialNeighbor false is returned. A mirror cannot
    *    be its own neighbor.
    */
   Bool_t IsNeighboringMirror(Int_t mir, Int_t potentialNeighbor);

   /**
    *  Determines from the position (in bigcal coordinates) whether the mirror number should be
    *  added to the cherenkov sum for that cluster. It does no overlap.
    */
   Bool_t AddMirrorToCluster(Int_t mirror, Double_t bcXcoord, Double_t bcYcoord) ;

   /**  Stupid way. Does not consider cone splitting.
    *   Determines from the position in bigcal coordinates if the mirror number should be
    *   added to from the cherenkov sum for that cluster. It does no overlap.
    */
   Bool_t AddMirrorToClusterSimple(Int_t mirror, Double_t bcXcoord, Double_t bcYcoord);

   /**
    *   Returns the mirror which should collect most of the Cherenkov light.
    */
   Int_t GetPrimaryMirror(Double_t bcXcoord, Double_t bcYcoord);

   /** Get the neighboring mirrors to add using the mirrs
    * returns a vector
    */
   std::vector<int> * GetMirrorsToAdd(Double_t bcXcoord, Double_t bcYcoord);

   /** Returns the distance from the target to the face of the detector. */
   Double_t GetDistanceFromTarget(){ return(fDistanceFromTarget);}

public :
   Double_t fDistanceFromTarget; /// To the face of the detector.
   Double_t fConeSize;
   TF1 * fMirrorHorizontalEdges[5];
   TF1 * fMirrorVerticalEdges[3];
   /// Rotation angles for mirrors
   Double_t alpha1, alpha2, alpha3, alpha4, alpha5, alpha6, alpha7, alpha8;
   /// Rotation angles for mirrors
   Double_t beta1, beta2, beta3, beta4, beta5, beta6, beta7, beta8;
   Double_t fMirrorGeoCutX[3];
   Double_t fMirrorGeoCutY[5];
   std::vector<int> * fMirrorsToSum;

private :
   static GasCherenkovGeometryCalculator * fgGasCherenkovGeometryCalculator;
   GasCherenkovGeometryCalculator();

   ClassDef(GasCherenkovGeometryCalculator, 1)
};


#endif
