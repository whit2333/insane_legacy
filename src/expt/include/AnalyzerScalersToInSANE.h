//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Oct 10 23:47:47 2010 by ROOT version 5.26/00b
// from TTree h9504/SCALERS1
// found on file: data/h2rootfiles/sane72994.1.1.root
//////////////////////////////////////////////////////////

#ifndef AnalyzerScalersToInSANE_h
#define AnalyzerScalersToInSANE_h

#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include "SANEScalerEvent.h"
#include "SANEScalers.h"


/** Concrete TSelector for first analysis of the scaler tree
 */
class AnalyzerScalersToInSANE : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   TTree * scalerTree; //!
   InSANERun * fRun;
   TFile * aFile;  //!
   SANEScalers * fSANEScalers;
   Double_t fTime;

   // NOMOREHANDWRITTENCODE
   Long64_t fScalers[581];
   Long64_t fRunTime;
   Long64_t fEventNumber;
   /*   UShort_t        nbcscalers;
      Double_t        runtime;
      Double_t        runtimebeam;
      Double_t        beamonruntime[2];
      Double_t        beamonruntimehelp[2];
      Double_t        beamonruntimehelm[2];
      Double_t        cerscaler[12];
      Double_t        cerscaler_change[12];
      Double_t        tkscaler[32];
      Double_t        tkscaler_change[32];
      Int_t           gen_event_id_number;
      Int_t           gen_event_type;
      Int_t           gen_event_class;
      Int_t           gen_event_roc_summary;
      Int_t           gen_event_sequence_n;*/
//    Int_t           gen_event_trigtype[12];
//   Long64_t fScalers[352];
//   Long64_t fRunTime;
//   Long64_t fEventNumber;
   // List of branches
   TBranch        *b_fRunTime;   //!
   TBranch        *b_fScalers;   //!
   TBranch        *b_fEventNumber;   //!
   /*
      TBranch        *b_beamonruntime;   //!
      TBranch        *b_beamonruntimehelp;   //!
      TBranch        *b_beamonruntimehelm;   //!
      TBranch        *b_cerscaler;   //!
      TBranch        *b_cerscaler_change;   //!
      TBranch        *b_tkscaler;   //!
      TBranch        *b_tkscaler_change;   //!
      TBranch        *b_gen_event_id_number;   //!
      TBranch        *b_gen_event_type;   //!
      TBranch        *b_gen_event_class;   //!
      TBranch        *b_gen_event_roc_summary;   //!
      TBranch        *b_gen_event_sequence_n;   //!
      TBranch        *b_gen_event_trigtype;   //!
   */
   AnalyzerScalersToInSANE(TTree * /*tree*/ = nullptr) { }
   virtual ~AnalyzerScalersToInSANE() { }
   virtual Int_t   Version() const {
      return 2;
   }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) {
      return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0;
   }
   virtual void    SetOption(const char *option) {
      fOption = option;
   }
   virtual void    SetObject(TObject *obj) {
      fObject = obj;
   }
   virtual void    SetInputList(TList *input) {
      fInput = input;
   }
   virtual TList  *GetOutputList() const {
      return fOutput;
   }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(AnalyzerScalersToInSANE, 1);
};

#endif

#ifdef AnalyzerScalersToInSANE_cxx
void AnalyzerScalersToInSANE::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);
   fChain->SetBranchAddress("fEventNumber", &fEventNumber, &b_fEventNumber);
   fChain->SetBranchAddress("fScalers", fScalers, &b_fScalers);
   fChain->SetBranchAddress("fRunTime", &fRunTime, &b_fRunTime);

//    fChain->SetBranchAddress("nbcscalers", &nbcscalers, &b_nbcscalers);
//    fChain->SetBranchAddress("runtime", &runtime, &b_runtime);
//    fChain->SetBranchAddress("runtimebeam", &runtimebeam, &b_runtimebeam);
//    fChain->SetBranchAddress("beamonruntime", beamonruntime, &b_beamonruntime);
//    fChain->SetBranchAddress("beamonruntimehelp", beamonruntimehelp, &b_beamonruntimehelp);
//    fChain->SetBranchAddress("beamonruntimehelm", beamonruntimehelm, &b_beamonruntimehelm);
//    fChain->SetBranchAddress("cerscaler", cerscaler, &b_cerscaler);
//    fChain->SetBranchAddress("cerscaler_change", cerscaler_change, &b_cerscaler_change);
//    fChain->SetBranchAddress("tkscaler", tkscaler, &b_tkscaler);
//    fChain->SetBranchAddress("tkscaler_change", tkscaler_change, &b_tkscaler_change);
//    fChain->SetBranchAddress("gen_event_id_number", &gen_event_id_number, &b_gen_event_id_number);
//    fChain->SetBranchAddress("gen_event_type", &gen_event_type, &b_gen_event_type);
//    fChain->SetBranchAddress("gen_event_class", &gen_event_class, &b_gen_event_class);
//    fChain->SetBranchAddress("gen_event_roc_summary", &gen_event_roc_summary, &b_gen_event_roc_summary);
//    fChain->SetBranchAddress("gen_event_sequence_n", &gen_event_sequence_n, &b_gen_event_sequence_n);
//    fChain->SetBranchAddress("gen_event_trigtype", gen_event_trigtype, &b_gen_event_trigtype);
}

Bool_t AnalyzerScalersToInSANE::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef AnalyzerScalersToInSANE_cxx
