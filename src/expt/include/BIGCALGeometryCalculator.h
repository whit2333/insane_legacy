#ifndef BIGCALGeometryCalculator_h
#define BIGCALGeometryCalculator_h 1

//#include <TROOT.h>
// //#include <TChain.h>
#include "TFile.h"
#include "TObject.h"
#include "TSQLRow.h"
#include "TSQLServer.h"
#include "TSQLResult.h"
#include "TSQLColumnInfo.h"
#include "TMath.h"
#include <vector>
#include <set>
#include <iostream>
#include <fstream>

#include "InSANEGeometryCalculator.h"
#include "InSANEDatabaseManager.h"
#include "TString.h"
#include "InSANEDetectorCalibration.h"
#include "TParameter.h"
#include "TRotation.h"
#include "TVector3.h"

/** Concrete class singleton for defining BigCal Geometries.
 *
 *  There are two groups:
 *    - TDCGroup
 *    - TriggerGroup
 *
 *
 * This is a singleton class containing a connection to the MySQL database where
 * the various geometry parameters are stored. It also handles the calibration
 * coefficients for each block.
 *
 * \ingroup Geometry
 *
 * \ingroup BIGCAL
 */
class BIGCALGeometryCalculator  : public InSANEGeometryCalculator {

   private:
      BIGCALGeometryCalculator();

   protected:
      Int_t fCalibrationSeries;

      std::vector<Int_t> * fTDCRows;
      std::vector<Int_t> * fTDCSumOf64Groups;

      TRotation               fRotFromBCcoords;  // Rotation matrix
      TVector3                fPosition;         // A position  

   public:


      double rcsXSize, rcsYSize, protXSize, protYSize;
      double rcsProtYSeparation; // center to center;
      double rcsProtXSeparation; // horizontal displacement
      double rcsCellSize;
      double protCellSize;
      double rcsCellZSize;
      double protCellZSize;
      double bcVerticalOffset ;
      double bcYSize ;
      double bcXSize ;
      double bigcalFace;
      double fNominalRadialDistance;
      double fNominalAngleSetting;
      int protNx, protNy, rcsNy, rcsNx;
      // evenly divide the mirrors starting in the middle
      double cherenkovMirrorXSize[8];
      double cherenkovMirrorYSize[8];
      double cherenkovMirrorPositionX[8];
      double cherenkovMirrorPositionY[8];
      double bc_cc[1744];
      double arcsCellSize;
      double aprotCellSize;
      Int_t fTDCGroupRowRes[2];

      Int_t fNSubDetectors;
      Double_t fSubDeltaX;
      Double_t fSubDeltaY;
      Double_t  fX0Sub[10];
      Double_t  fY0Sub[10];


   public:

      /** This function is called to create an instance of the class.
       *  Calling the constructor publicly is not allowed. The constructor
       *  is private and is only called by this function.
       */
      static BIGCALGeometryCalculator* GetCalculator();

      ~BIGCALGeometryCalculator();

      static BIGCALGeometryCalculator* fgBIGCALGeometryCalculator;


      Double_t GetBlockLength(Int_t block) const {
         int j = GetBlock_j(block);
         if(j>=33) return 40.0;
         return 45.0;
      }
      Double_t GetBlockLength(Int_t i, Int_t j) const {
         if(j>=33) return 40.0;
         return 45.0;
      }

      // Bad blocks set
      std::set<int>   fBadBlocks;
      Int_t LoadBadBlocks(const char * file ){
         if(!file) return(-1);
         std::ifstream inputfile(file);
         if( !(inputfile.is_open()) ) return(-1);
         Int_t ibl = 0;
         fBadBlocks.clear();
         while( !(inputfile.eof()) ) {
            inputfile >> ibl;
            if( !inputfile.eof() ) 
               fBadBlocks.insert(ibl);
         }
         return(0);
      }
      Int_t IsBadBlock(Int_t blocknumber){
         std::set<int>::iterator it;
         it = fBadBlocks.find(blocknumber);
         if( it == fBadBlocks.end() ) return(0);
         /*else*/
         return(1);
      }

      /** @name Individual block geometry
       *  Functions used for individual block geometry information
       *  @{
       */

      /** Returns the block number 1-1744.  */
      int GetBlockNumber(int i, int j) const ;

      /** Returns the block's horizontal index from the block number which goes from 1-1744 */
      int GetBlock_i(int block_number) const ;

      /** Returns the block's vertical index from the block number */
      int GetBlock_j(int block_number) const ;

      /** Returns false if block it is out of reach from pi0 calibration.*/
      bool IsBlockCalibrated(int i, int j) const ;

      bool IsBlockCalibrated(int block) const {
         return(IsBlockCalibrated(GetBlock_i(block), GetBlock_j(block)));
      }

      /** Cross cuts. 
       *  IsBlockInCross  : same as IsBlockCalibrated.
       *  IsBlockInCross1 : one block tighter than IsBlockInCross.
       *  IsBlockInCross2 : two blocks tighter than IsBlockInCross.
       */
      bool IsBlockInCross(int i, int j) const ;
      bool IsBlockInCross(int block) const { return(IsBlockInCross(GetBlock_i(block), GetBlock_j(block))); }
      bool IsBlockInCross1(int i, int j) const ;
      bool IsBlockInCross1(int block) const { return(IsBlockInCross1(GetBlock_i(block), GetBlock_j(block))); }
      bool IsBlockInCross2(int i, int j) const ;
      bool IsBlockInCross2(int block) const { return(IsBlockInCross2(GetBlock_i(block), GetBlock_j(block))); }
      bool IsBlockInCross3(int i, int j) const ;
      bool IsBlockInCross3(int block) const { return(IsBlockInCross3(GetBlock_i(block), GetBlock_j(block))); }
      bool IsBlockInCross4(int i, int j) const ;
      bool IsBlockInCross4(int block) const { return(IsBlockInCross4(GetBlock_i(block), GetBlock_j(block))); }
      bool IsBlockInCross5(int i, int j) const ;
      bool IsBlockInCross5(int block) const { return(IsBlockInCross5(GetBlock_i(block), GetBlock_j(block))); }

      bool IsBlockOnPerimeter(const int i, const int j) const ;

      bool IsBlockOnPerimeter(int block) const {
         return(IsBlockOnPerimeter(GetBlock_i(block), GetBlock_j(block)));
      }

      /** Get the X coordinate (in Prot coords.) of the column (which the interger argument).
       *  Coordinates are in an imaginary box centered on the Protvino (lower) section of BigCal
       */
      double GetProtCellX(int i)  const ;// coords centered in protvino frame

      /** Get the Y coordinate (in Prot coords.) of the row (which the interger argument).
       *  Coordinates are in
       *  an imaginary box centered on the Protvino (lower) section of BigCal
       */
      double GetProtCellY(int j)  const ;

      /** Get the X coordinate (in RCS coords.) of the column (which the interger argument).
       *  Coordinates are in an imaginary box centered on the RCS (upper) section of BigCal
       */
      double GetRCSCellX(int i);// coords centered in rcs frame

      /** Get the Y coordinate (in RCS coords.) of the column (which the interger argument).
       *  Coordinates are in
       *  an imaginary box centered on the RCS (upper) section of BigCal
       */
      double GetRCSCellY(int j);

      /** Get the X coordinate of a cell in the RCS section in
       *  <a href="http://quarks.temple.edu/~whit/SANE/BigCal/docs"> BigCal Coordinate System  </a>
       * of the column (which the interger argument).
       */
      double GetRCSCellX_BCCoords(int i);// coords centered in BC frame

      /**
       * Get the Y coordinate of a cell in the RCS section in
       * <a href="http://quarks.temple.edu/~whit/SANE/BigCal/docs"> BigCal Coordinate System  </a>
       * of the column (which the interger argument).
       */
      double GetRCSCellY_BCCoords(int j);

      /** Get the X coordinate of a cell in the Protvino section in
       *  <a href="http://quarks.temple.edu/~whit/SANE/BigCal/docs"> BigCal Coordinate System  </a>
       *  of the column (which the interger argument).
       */
      double GetProtCellX_BCCoords(int i);// coords centered in BC frame

      /** Get the Y coordinate of a cell in the Protvino section in
       *  <a href="http://quarks.temple.edu/~whit/SANE/BigCal/docs">BigCal Coordinate System</a>
       *  of the column (which the interger argument).
       */
      double GetProtCellY_BCCoords(int j);

      /** Get the X coordinate of a cell (i,j) in
       * <a href="http://quarks.temple.edu/~whit/SANE/BigCal/docs"> BigCal Coordinate System  </a>
       * of the column (which the interger argument).
       * NOTE: The arguments are for all of bigcal (i,j)=({1-32},{1-56} , however, there are two columns
       * in the rcs section (i={31,32},j={33-56}) where there is actually no block!
       */
      double GetBlockXij_BCCoords(int i, int j) ;

      /** Get the Y coordinate of a cell (i,j) in
       * <a href="http://quarks.temple.edu/~whit/SANE/BigCal/docs"> BigCal Coordinate System  </a>
       * of the column (which the interger argument).
       * NOTE: The arguments are for all of bigcal (i,j)=({1-32},{1-56} , however, there are two columns
       * in the rcs section (i={31,32},j={33-56}) where there is actually no block!
       */
      double GetBlockYij_BCCoords(int i, int j) ;

      double GetBlockX_BCCoords(int block) {
         return(GetBlockXij_BCCoords(GetBlock_i(block), GetBlock_j(block)));
      }
      double GetBlockY_BCCoords(int block) {
         return(GetBlockYij_BCCoords(GetBlock_i(block), GetBlock_j(block)));
      }

      const TVector3& GetBlockFacePosition_BCCoords(int block){
         fPosition.SetXYZ( GetBlockX_BCCoords(block), GetBlockY_BCCoords(block), fNominalRadialDistance);
         return(fPosition);
      }
      const TVector3& GetBlockFacePosition(int block){
         fPosition.SetXYZ( GetBlockX_BCCoords(block), GetBlockY_BCCoords(block), fNominalRadialDistance);
         fPosition.Transform(fRotFromBCcoords);
         return(fPosition);
      }
      const TVector3& GetBlockCenterPosition_BCCoords(int block){
         fPosition.SetXYZ( GetBlockX_BCCoords(block), GetBlockY_BCCoords(block), fNominalRadialDistance + GetBlockLength(block)/2.0 );
         return(fPosition);
      }
      const TVector3& GetBlockCenterPosition(int block){
         fPosition.SetXYZ( GetBlockX_BCCoords(block), GetBlockY_BCCoords(block), fNominalRadialDistance + GetBlockLength(block)/2.0 );
         fPosition.Transform(fRotFromBCcoords);
         return(fPosition);
      }
      const TVector3& GetBlockRearPosition_BCCoords(int block){
         fPosition.SetXYZ( GetBlockX_BCCoords(block), GetBlockY_BCCoords(block), fNominalRadialDistance + GetBlockLength(block) );
         return(fPosition);
      }
      const TVector3& GetBlockRearPosition(int block){
         fPosition.SetXYZ( GetBlockX_BCCoords(block), GetBlockY_BCCoords(block), fNominalRadialDistance + GetBlockLength(block) );
         fPosition.Transform(fRotFromBCcoords);
         return(fPosition);
      }
      void RotateFromBCCoords(TVector3& v) const {
         v.Transform(fRotFromBCcoords);
      }

      /** Returns the (horizontal) group number of the analog summed cells which contains cell_ij.
       *  The group runs from 1 to 4 with 1 at negative x in BigCal Coordinates.
       *  \todo {Check grouping <h3> Made some guesses here !!! Not sure if the numbering is right.</h3>}
       */
      Int_t  GetTDCGroup(Int_t i, Int_t j);

      /** Get TDC Row returns a pointer of dimension 2 (since a cell can only belong to at most 2
       * due to summing overlaps).
       * with the row numbers of the analog summed cells which contains cell_ij. If there
       * is only one row, the second value is <= 0.
       * The row numbers run from 1 to 19 and goes from bottom (-y in Bigcal Coords) to
       * top. Each group is 4 blocks tall and overlap both above and below one block.
       *
       * If there are two possible rows, The lower one is at [0] and the upper one is at [1].
       */
      Int_t * GetTDCRow(const Int_t i, const Int_t j) ;
      Int_t * GetTimingRow(const Int_t i, const Int_t j){ return GetTDCRow(i,j); }

      /** Same as GetTDCRow(i,j) but it returns a reference to a vector.  */
      std::vector<Int_t>& GetTDCRows(const Int_t i, const  Int_t j) ;
      //std::vector<Int_t>& GetTimingRows(const Int_t i, const  Int_t j) { return GetTDCRow(i,j); }

      /**  Get Group Number (1-224) from individual cell indicies
       *   Group number is numbered such that 1-4 form the bottom row, 5-8 form the next...
       */
      int GetGroupNumber(int i, int j) {
         return((j - 1) * 4 + GetTDCGroup(i, j));
      }
      int GetGroupNumber(int blocknumber){ return(GetGroupNumber(GetBlock_i(blocknumber), GetBlock_j(blocknumber))); }

      /**  Get Group Number (1-224) using TDCRow (vertical)  and TDCGroup (TDCGroup is horizontal span, not the same as Group number)
       *   Group number is numbered such that 1-4 form the bottom row, 5-8 form the next... up to 224.
       *  \deprecated
       */
      int GetGroupNumberFromTDCHit(int tdcGroup, int tdcRow) {
         return((tdcRow - 1) * 4 + tdcGroup);
      }

      //@}

      /** @name Trigger group
       *  Bigcal Trigger group functions.
       *  @{
       */
      std::vector<int> GetBlocksInSumOf64(int trigGroupNum) {
         std::vector<int> blocks;
         return blocks;
      }

      const TVector3& GetSumOf64Position(int trigGroupNum) {
         //only first for now
         int i = (trigGroupNum-1)/19;
         int j = (trigGroupNum-1)%19;
         double icenter = 8.0 + double(i)*16.0;
         double jcenter = 2.0 + double(j)*3.0;
         double xsize = 3.8;
         double ysize = 3.8;
         fPosition.SetXYZ( -bcXSize/2.0 + xsize*icenter, -bcYSize/2.0 + ysize*jcenter, fNominalRadialDistance  );
         return(fPosition);
      }

      /** Returns the trigger group column number that the block_ij belongs to.
       *  It is a number between 1 and 4 since bigcal is divided into 4 vertically
       *  and horizontally
       *
       * \todo check that the division is done between protvino and rcs sections or just in the middle?
       */
      Int_t   GetTriggerGroup(Int_t i, Int_t j) ;
      Int_t   GetTriggerGroup(int block_number) {
         return(GetTriggerGroup(GetBlock_i(block_number), GetBlock_j(block_number)));
      }

      /** Returns a pointer of dimension 2 to the Sum of 64 group number of the
       *  analog summed cells which contains cell_ij. If there
       *  is only one group, the second value is <= 0.
       *  The group number runs from 1 to 19. It is divided into two.
       *  1-19 go from bottom (-y in Bigcal Coords) to top with negative x.
       *  20-38 go from bottom to top with positive x values.
       *
       */
      Int_t * GetSumOf64Group(Int_t i, Int_t j) {
         return(nullptr);
      }
      const std::vector<Int_t>& GetSumOf64Groups(const Int_t i, const Int_t j) {
         return(*fTDCRows);
      }

      /** Same as GetSumOf64Group except it continues to 38 */
      std::vector<Int_t> GetSumOf64GroupNumber(Int_t i, Int_t j) ;
      std::vector<Int_t> GetSumOf64GroupNumber(int block_number)  {
         return(GetSumOf64GroupNumber(GetBlock_i(block_number), GetBlock_j(block_number)));
      }

      /** Same as GetSumOf64GroupNumber(i,j) but it returns a reference to a vector.  */
      std::vector<Int_t> &GetSumOf64GroupNumbers(const Int_t i, const  Int_t j) ;

      /** Returns the Tigger sum group formed by summing the groups of 64.
       *  This is fed into the trigger supervisor for forming the triggers.
       *  This can only return values between 1-4.
       */
      Int_t GetTriggerSumGroup(Int_t block_number) const ;

      //@}




      /** @name Calibration
       *  Calibration related functions
       *  @{
       */

      /** Sets the calibration series number
       *  fCalibrationSeries = 1 corresponds to cor_900_ , ie parallel
       *
       *  fCalibrationSeries = 2 corresponds to cor_72600_... , ie perp
       */
      void SetCalibrationSeries(Int_t j);

      /** callback used to set the values of the calibration coefficients */
      static int SetCalibration_callback(void * theGeoCalc, int argc, char **argv, char **azColName);

      Int_t GetCalibrationSeries() const { return(fCalibrationSeries); }

      /** Get the most resent calibration result for a run.
       *  If run is -1 the latest run series is returned
       */
      Int_t GetLatestCalibrationSeries(Int_t run = -1) const ;

      /** Loads existing calibration coefficients from a root file.
       *  The file is data/BigCalCalibrations.root and contains saved objects of the
       *  class InSANEDetectorCalibration. See the script detectors/bigcal/bigcal_calibration_save.cxx.
       */
      Int_t LoadCalibrationSeries(Int_t j);

      /** Retrieves the block's lastest calibration coefficient (from a database).
       *  
       */
      Double_t GetCalibrationCoefficient(Int_t i, Int_t j);

      /** Retrieves the calibration coefficient using the block number
       */
      Double_t GetCalibrationCoefficient(Int_t N) ;

      void SetCalibrationCoefficient(Int_t N, Double_t val) ;
      void SetCalibrationCoefficient(Int_t i, Int_t j, Double_t val) ;

      Double_t GetCalibration_SeriesBlock(Int_t series, Int_t block) const ;

      Int_t CreateNewCalibrationSeries(Int_t start_run, Int_t end_run);

      /** Prints the calibration coefficients
       */
      void PrintCalibrationCoefficients();

      //@}

   public:

      /** Returns an arbritrarily defined subdetector number for
       *  studying different sections of bigcal.
       */
      Int_t GetSubDetectorNumber(Double_t x, Double_t y) {
         Int_t result = -1;
         for (int i = 0; i < fNSubDetectors; i++) {
            if (TMath::Abs(x - fX0Sub[i]) < fSubDeltaX / 2.0)
               if (TMath::Abs(y - fY0Sub[i]) < fSubDeltaY / 2.0)
                  result = i;
         }
         return(result);
      }



      /** Returns the distance of the face of bigcal from the target */
      Double_t GetDistanceFromTarget() {
         return(fNominalRadialDistance);
      }


   public:

      void   Print(Option_t* option = "") const {
         using namespace std;
         cout << "------------------------------------------------------\n";
         TObject::Print();
         Int_t namecol = 20;
         cout << setw(namecol) <<  "rcsXSize" << " " <<  setw(10) <<  rcsXSize << "\n";
         cout << setw(namecol) <<  "rcsYSize" << " " << setw(10) <<  rcsYSize << "\n";
         cout << setw(namecol) <<  "protXSize" << " " << setw(10) <<  protXSize << "\n";
         cout << setw(namecol) <<  "protYSize" << " " << setw(10) <<  protYSize << "\n";
         cout << setw(namecol) <<  "rcsProtYSeparation" << " " << setw(10) <<  rcsProtYSeparation << "\n";
         cout << setw(namecol) <<  "rcsCellSize" << " " << setw(10) <<  rcsCellSize << "\n";
         cout << setw(namecol) <<  "protCellZSize" << " " << setw(10) <<  protCellZSize << "\n";
         cout << setw(namecol) <<  "bcVerticalOffset" << " " << setw(10) <<  bcVerticalOffset << "\n";
         cout << setw(namecol) <<  "bcYSize" << " " << setw(10) <<  bcYSize << "\n";
         cout << setw(namecol) <<  "bcXSize" << " " << setw(10) <<  bcXSize << "\n";
         cout << setw(namecol) <<  "bigcalFace" << " " << setw(10) <<  bigcalFace << "\n";
         cout << setw(namecol) <<  "fNominalRadialDistance" << " " << setw(10) <<  fNominalRadialDistance << "\n";
         cout << setw(namecol) <<  "fNominalAngleSetting" << " " << setw(10) <<  fNominalAngleSetting << "\n";
         cout << setw(namecol) <<  "protNx" << " " << setw(10) <<  protNx << "\n";
         cout << setw(namecol) <<  "protNy" << " " << setw(10) <<  protNy << "\n";
         cout << setw(namecol) <<  "rcsNy" << " " << setw(10) <<  rcsNy << "\n";
         cout << setw(namecol) <<  "rcsNx" << " " << setw(10) <<  rcsNx << "\n";
         cout << "------------------------------------------------------\n";

      }


      ClassDef(BIGCALGeometryCalculator, 4)
};


#endif
