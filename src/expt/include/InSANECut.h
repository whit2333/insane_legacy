#ifndef InSANECut_HH
#define InSANECut_HH 1
#include <iostream>
#include "TString.h"
#include "TList.h"
#include "TCut.h"


/** Base class for an analysis cut
 *
 * \ingroup Cuts
 */
class InSANECut : public TCut {
public:
   InSANECut(const char * aName = "noNameCut" ,
             const char * aTitle = "x<y",
             const char * aDescription = "Some Description",
             const char * aBranchName  = "no branch given")
      : TCut(aName, aTitle) {
      fDescription = aDescription;
      fBranchName = aBranchName;
      fMinValue = 0.0;
      fMaxValue = 1.0;
      fVariableName = "x";
   }

   virtual ~InSANECut() {

   }

   /** Necessary for Browsing */
   Bool_t IsFolder() const {
      return kTRUE;
   }
   /** Needed to make object browsable. */
   void Browse(TBrowser* b) {
//       b->Add(&fAsymmetries);
//       b->Add(&fResults);
//       b->Add(&fDilution);
//       b->Add(&fRunFlags);
   }

   void Print() {
      std::cout << " -c- " << GetName() <<  ", " << GetTitle() << ", Min = " << fMinValue << ", Max = " << fMaxValue << std::endl;

   }

   Double_t GetMaxValue() {
      return fMaxValue;
   }
   Double_t GetMaximumValue() {
      return fMaxValue;
   }
   Double_t GetMinValue() {
      return fMinValue;
   }
   Double_t GetMinimumValue() {
      return fMinValue;
   }



   /** Set the cut maximum
    */
   void SetMaximumValue(Double_t val) {
      fMaxValue = val;
   }
   void SetMaxValue(Double_t val) {
      fMaxValue = val;
   }

   /** Set the cut minimum
    */
   void SetMinimumValue(Double_t val) {
      fMinValue = val;
   }
   void SetMinValue(Double_t val) {
      fMinValue = val;
   }

   /** Describe the cut
    */
   virtual void Describe();

   /** This function does the work of evaluating the cut
    * for a simple value
    */
   virtual  Bool_t PassesCut(Double_t * const val) const {
      if ((*val) < fMaxValue && (*val) > fMinValue) return(true);
      else return(false);
   }

   TList fHistograms;
   TString fDetector;

   TString GetCutString() {
      return(TString(Form("%s.%s>%f&&%s.%s<%f", fBranchName.Data(), fVariableName.Data(), fMinValue, fBranchName.Data(), fVariableName.Data(), fMaxValue)));
   }

protected :
   Double_t fMinValue;
   Double_t fMaxValue;

public:
   TString fDescription;
   TString fBranchName;
   TString fVariableName;

   ClassDef(InSANECut, 3)
};




#endif
