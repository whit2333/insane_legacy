#ifndef HallCBeamCalculations_HH
#define HallCBeamCalculations_HH 1

#include "BETACalculation.h"
#include "HallCBeam.h"
#include "HallCBeamEvent.h"

/** Beam calculations for the first pass.
 *   - Raster positions
 *   - Raster calibrations?
 *  \ingroup Calculations
 */
class HallCBeamCalculation0 : public BETACalculation {
   public:
      HallCBeamCalculation0(InSANEAnalysis * analysis) : BETACalculation(analysis) {
         SetNameTitle("HallCBeamCalculation0","HallCBeamCalculation0");
         fBeamEvent = nullptr;
         fBeam = nullptr;
      }
      virtual ~HallCBeamCalculation0() {}

      virtual Int_t Initialize() {
         BETACalculation::Initialize();
         fBeamEvent = fEvents->BEAM;
         fBeam = (HallCRasteredBeam*)(InSANERunManager::GetRunManager()->GetCurrentRun()->fApparatus.FindObject("HallC-beam"));
         if (!fBeam) {
            fBeam = new HallCRasteredBeam();
            Warning("Initialize", "Creating default HallCBeam");
         }
         InSANERunManager * runman = InSANERunManager::GetRunManager();
         fBeam->fEnergy            = runman->GetCurrentRun()->fBeamEnergy;
         fBeam->fCurrent           = runman->GetCurrentRun()->fAverageBeamCurrent;
         fBeam->fNPass             = runman->GetCurrentRun()->fBeamPassNumber;
         fBeam->fWienAngle         = runman->GetCurrentRun()->fWienAngle;
         fBeam->fHalfwavePlate     = runman->GetCurrentRun()->fHalfWavePlate;
         std::cout << " Current Beam Pol is " << fBeam->GetPolarization() << "\n";
         return(0);
      }

      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  HallCBeamCalculation0 - \n";
         std::cout <<  "       Recalculates the raster positions using the apparatus, HallCBeam.\n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

      virtual Int_t Calculate() {
         fRasterPos = fBeam->GetSlowRasterPosition(fBeamEvent->fXSlowRasterADC, fBeamEvent->fYSlowRasterADC);
         fBeamEvent->fRasterPosition.SetXYZ(fRasterPos.X(), fRasterPos.Y(), 0.0);
         fBeamEvent->fXSlowRaster = fRasterPos.X();
         fBeamEvent->fYSlowRaster = fRasterPos.Y();
         fBeamEvent->fBeamEnergy  = fBeam->fEnergy;

         return(0);
      }

   protected:
      TVector2         fRasterPos;
      HallCBeamEvent * fBeamEvent;
      HallCRasteredBeam *      fBeam;

      ClassDef(HallCBeamCalculation0, 1)
};


#endif
