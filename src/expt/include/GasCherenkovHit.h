#ifndef GasCherenkovHit_h
#define GasCherenkovHit_h

#include <iostream>
#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include <TClonesArray.h>
#include "InSANEDetectorHit.h"
#include "InSANERunManager.h"


/** Concrete class for a Hit in the SANE gas cherenkov
 *
 * Concrete class for hits in the SANE gas cherenkov built by Temple University.
 * There are 8 pmt/mirrors in two columns.
 *
 * fLevel is almost obsolete
 *
 * fLevel=0 : ADC hits alone
 * fLevel=1 : TDC hits with corresponding ADC hit filled in
 *
 * \ingroup Hits
 * \ingroup Cherenkov
 */
class GasCherenkovHit : public InSANEDetectorHit  {
   public :

      GasCherenkovHit() {
         fTDCHitNumber = -1;
         fMirrorNumber = 0;
         fADC = 0;
         fADCAlign = 0.0;
         fNPE = 0.0;
         fTDC = -9999;
         fTDCAlign = -9999.0;
         fIsPrimary = false;
         Clear();
      }

      ~GasCherenkovHit() {
         ;
      };

      GasCherenkovHit(const GasCherenkovHit& rhs) : InSANEDetectorHit(rhs) {
         (*this) = rhs;
      }

      void Clear(Option_t *opt = "") {
         InSANEDetectorHit::Clear(opt);
         fTDCHitNumber = -1;
         fMirrorNumber = 0;
         fADC = 0;
         fNPE = 0.0;
         fADCAlign = 0.0;
         fTDC = -9999;
         fTDCAlign = -9999.0;
         fIsPrimary = false;
      }

      GasCherenkovHit& operator=(const GasCherenkovHit &rhs) {
         // Check for self-assignment!
         if (this == &rhs)      // Same object?
            return *this;        // Yes, so skip assignment, and just return *this.
         // Deallocate, allocate new space, copy values...
         InSANEDetectorHit::operator=(rhs);
         fLevel = rhs.fLevel;
         fHitNumber = rhs.fHitNumber;
         fTDCHitNumber = rhs.fTDCHitNumber;
         fMirrorNumber = rhs.fMirrorNumber;
         fPassesTimingCut = rhs.fPassesTimingCut;
         fADC = rhs.fADC;
         fADCAlign = rhs.fADCAlign;
         fNPE = rhs.fNPE;
         fTDC = rhs.fTDC;
         fTDCAlign = rhs.fTDCAlign;
         fIsPrimary = rhs.fIsPrimary;
         return *this;
      }

      /** Adding chrerenkov hits Adds the ADC,ADCAlign,and NPE values while
       *  for TDC values it takes the primary hit OR greatest value of fADCAlign.
       *  The Mirror number is also set accordingly.
       */
      GasCherenkovHit & operator+=(const GasCherenkovHit &rhs) {
         if (fIsPrimary && !(rhs.fIsPrimary)) {
            // TDC remains the same
         } else if (!fIsPrimary && rhs.fIsPrimary) {
            fTDC = rhs.fTDC;
            fTDCAlign = rhs.fTDCAlign;
            fMirrorNumber = rhs.fMirrorNumber;
            fIsPrimary = kTRUE;
         } else if (fADCAlign < rhs.fADCAlign) {
            fTDC = rhs.fTDC;
            fTDCAlign = rhs.fTDCAlign;
            fMirrorNumber = rhs.fMirrorNumber;
         }
         if (fLevel != rhs.fLevel) Warning("GasCherenkovHit::operator+=(GasCherenkovHit aHit)", "Hit levels do not match!");
         // ADCs are added
         fADC += rhs.fADC;
         fADCAlign += rhs.fADCAlign;
         fNPE += rhs.fNPE;
         return *this;
      }

      // Add this instance's value to other, and return a new instance
      // with the result.
      const GasCherenkovHit operator+(const GasCherenkovHit &other) const {
         GasCherenkovHit result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
         result += other;            // Use += to add other to the copy.
         return result;              // All done!
      }

      Int_t        fTDCHitNumber; // Hit number index
      Int_t        fMirrorNumber; // Mirror/channel of PMT
      Int_t        fADC;          // Pedestal subtracted ADC
      Double_t     fADCAlign;     // Signal Peak aligned ADC value
      Double_t     fNPE;          // Calibrated NPE
      Int_t        fTDC;          // Raw TDC value
      Double_t     fTDCAlign;     // TDC aligned at zero
      Bool_t       fIsPrimary;    // Main mirror hit (used with operator+)

      ClassDef(GasCherenkovHit, 5)
};
#endif

