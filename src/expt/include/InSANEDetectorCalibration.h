#ifndef InSANEDetectorCalibration_HH
#define InSANEDetectorCalibration_HH 1

#include "TNamed.h"
#include <vector>
#include <iomanip> 
#include "TClonesArray.h"
#include "TList.h"
#include "TF1.h"
#include "TH1.h"
#include "TParameter.h"
#include "TBrowser.h"

/**  Calibration objects for each "channel" for a Detector Calibration
 *
 */
class InSANECalibration : public TNamed {
   public:
      InSANECalibration(const char * name = "cal", const char * title = "Some Cal");
      ~InSANECalibration();

      Bool_t IsFolder() const { return kTRUE; }

      void   Browse(TBrowser* b) {
         b->Add(&fFunctions, "Functions");
         b->Add(&fParameters, "Parameters");
      }

      TF1 * GetFunction(Int_t n = 0) const ;

      /** Add a function to calibration. The parameters associated with the function
       *  are added to the list of paramters.
       */
      void AddFunction(TF1 * f, Int_t n = 0);

      TParameter<double> * GetParameter(Int_t n) const ;
      Int_t  AddParameter(TParameter<double> * par);
      void   Clear(Option_t * opt ="");
      void   AddHistogram(TH1 * h, Int_t n = 0);

      Int_t   GetChannel() const  { return fChannel; }
      void    SetChannel(Int_t chan) { fChannel = chan; }
      TList * GetFunctions() { return &fFunctions; }
      TList * GetParameters() { return &fParameters; }
      TList * GetHistograms() { return &fHistograms; }
      Int_t   GetNPar() const { return fNParameters; }

      void Print(Option_t * opt = "") const ;  // *MENU*

   private:
      TList fFunctions; //
      TList fParameters; // Array of TParameter
      TList fHistograms; //
      Int_t fChannel;      // Detector Channel Number for calibration
      Int_t fNParameters;  // Number of Parameters

      ClassDef(InSANECalibration, 5)
};

/** ABC Detector Calibration class.
 *
 *  This Class should be implemented for saving to a file
 *  Typically a script will perform all the calculations needed for an implementation.
 *  This object  is then  saved to a file where it is used by an InSANEDetector implementation
 */
class InSANEDetectorCalibration : public TNamed {
   public:
      InSANEDetectorCalibration(const char * name = "aCalibration", const char * title = "aCalibTitle");
      ~InSANEDetectorCalibration();

      Bool_t IsFolder() const { return kTRUE; }
      void Browse(TBrowser* b) {
         b->Add(&fCalibrations, "Channels");
      }

      Int_t               AddCalibration(InSANECalibration * cal);
      InSANECalibration * GetCalibration(Int_t n) const ;

      Int_t               AddChannel(InSANECalibration * cal) { return(AddCalibration(cal)); }
      InSANECalibration * GetChannel(Int_t n) { return(GetCalibration(n)); }

      Int_t fNChannels;
      TList fCalibrations;

   ClassDef(InSANEDetectorCalibration, 3)
};

#endif

