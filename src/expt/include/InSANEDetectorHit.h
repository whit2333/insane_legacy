#ifndef InSANEDetectorHit_H
#define InSANEDetectorHit_H 1

#include "TROOT.h"
#include "TObject.h"


/** ABC for a single detector Hit
 *
 * \ingroup Hits
 */
class InSANEDetectorHit : public TObject {
   public:

      Int_t    fHitNumber;
      Int_t    fLevel;
      Bool_t   fPassesTimingCut;
      Int_t    fChannel;

   public :
      InSANEDetectorHit();

      InSANEDetectorHit(const InSANEDetectorHit& rhs) : TObject(rhs) {
         (*this) = rhs;
      }
      virtual ~InSANEDetectorHit();

      void Clear(Option_t *opt = "");

      InSANEDetectorHit& operator=(const InSANEDetectorHit &rhs);

      InSANEDetectorHit & operator+=(const InSANEDetectorHit &rhs);

      /** Add this instance's value to other, and return a new instance
       * with the result.
       */
      const InSANEDetectorHit operator+(const InSANEDetectorHit &other) const;

      ClassDef(InSANEDetectorHit, 5)
};

#endif

