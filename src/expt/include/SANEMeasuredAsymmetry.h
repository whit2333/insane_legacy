#ifndef SANEMeasuredAsymmetry_HH
#define SANEMeasuredAsymmetry_HH 1

#include "InSANEMeasuredAsymmetry.h"
#include <vector>
#include "TMath.h"

/** Asymmetry with proper binning for SANE. 
 *  Following https://hallcweb.jlab.org/experiments/sane/wiki/index.php/Kinematics#02.2F14.2F2011
 * 
 *  \f$ \frac{\sigma_{E'}}{E'} = \frac{C_1}{\sqrt{E'}} + C_2 \f$
 *  
 *  \f$ E'_{min,max} = E'(1 \pm \frac{1}{2} \sigma_{E_{bin}}' ) \f$ 
 *
 *  First, we are fixing the Q2 bin. In order to define the first energy bin, we start at x=1 and calculate
 *  the photon energy, nu = Q2/(2Mx). Thus, E' = E0-nu. Using the definition of E'_min and E'_max above, we can calculate
 *  the limits nu_min and nu_max and subsequently x_min = Q2/(2M nu_max) and x_max = Q2/(2M nu_min).
 *
 *  Solving for the central value of E' for the next lower bin is a matter of solving the equation
 *
 *  \f$ E_{0}^{n+1} = E_{min}^{n} - \sigma_{E'}(E_0^{n+1}) E_{0}^{n+1}/2 \f$
 *
 *  The solution is :
 *
 *  \f$ \sqrt{E_{0}^{n+1}} = \frac{ -C_1/2 \pm \sqrt{(C_1/2)^2+4(1+C_2/2)E_{min}^{n}}}{2+C_2} \f$
 *
 *  \ingroup asymmetries
 */
class SANEMeasuredAsymmetry : public InSANEMeasuredAsymmetry {

   protected:
      Double_t C1;  /// Parameter for bigcal energy resolution
      Double_t C2;  /// Parameter for bigcal energy resolution

      std::vector<float>   fxBins;         /// x bins for histogram of size (fNxBins+1)
      Int_t                fNxBins;        /// Number of x bins 
      std::vector<float>   fWBins;         /// W bins
      Int_t                fNWBins;        /// Number of W bins
      std::vector<float>   fThetaBins;     /// theta bins
      Int_t                fNThetaBins;    /// N theta bins
      std::vector<float>   fEnergyBins;    /// Energy bins 
      Int_t                fNEnergyBins;   /// Number of energy bins
      std::vector<float>   fPhiBins;       /// Phi bins 
      Int_t                fNPhiBins;      /// Number of phi bins
      std::vector<float>   fNuBins;        /// Nu bins 
      Int_t                fNNuBins;       /// Number of Nu bins
      std::vector<float>   fQ2Bins;        /// Q2 bins 
      Int_t                fNQ2Bins;       /// Number of Q2 bins

   public:
      SANEMeasuredAsymmetry(Int_t n = 0);
      SANEMeasuredAsymmetry(const SANEMeasuredAsymmetry&)            = default;
      SANEMeasuredAsymmetry(SANEMeasuredAsymmetry&&)                 = default;
      SANEMeasuredAsymmetry& operator=(const SANEMeasuredAsymmetry&) = default;
      SANEMeasuredAsymmetry& operator=(SANEMeasuredAsymmetry&&)      = default;
      virtual ~SANEMeasuredAsymmetry();

      Bool_t IsGoodEvent();
      void   Initialize();
      double EnergyResolution(double E){
         Double_t multiplier = 1.0;
         Double_t e_max = 3.0;//C1*C1/(C2*C2) - 0.1; // ~4.53  solution when resolution equals zero minus 0.1
         if( E < e_max ) return( multiplier*2.0*(C1/(TMath::Sqrt(E)) + C2) );
         return(multiplier*2.0*(C1/(TMath::Sqrt(e_max)) + C2));
      }

   ClassDef(SANEMeasuredAsymmetry,18)
};

#endif

