#ifndef InSANERunReport_HH
#define InSANERunReport_HH 1
#include <vector>
#include <iostream>
#include "TString.h"
#include "TNamed.h"
#include "InSANEDatabaseManager.h"
#include "InSANERun.h"
#include "InSANEElectronBeam.h"

/** Data holder class for selecting and replacing data
 *
 *  The isUpdated data member should be set when the InSANERun object
 *  has been updated. This flag tells the run report that it needs to fill the
 *  database.
 *
 *  Upon creating a new InSANERun object, the RunManager creates
 *  a new RunReport by calling InSANERunManager::GeneratePreRunReport which
 *  is meant to be used to initialize the NEW InSANERun object.
 *  If a previous InSANERun is used, the RunManager creates a new RunReport
 *  by calling InSANERunManager::GenerateRunReport which fills.
 *
 *
 */
class InSANERunReportDatum : public TObject {
   public:
      InSANERunReportDatum(const char * name = "unknown", const char * val = "0", Bool_t isStr = false, Bool_t update = false);
      ~InSANERunReportDatum();
      TString fName;
      TString fValue;
      Bool_t isUpdated;
      Bool_t isString;

      void    SetUpdated(Bool_t v=true){isUpdated=v;}
      Bool_t  IsUpdated(){return isUpdated;}

      Int_t GatherInfo(TString * sql);
      Int_t GatherInfoSQLite(TString * sql);

      /** Simple SQLite callback expecting one or zero values which sets
       *  the datum's value
       */
      //static int GatherInfo_callback(void * datum, int argc, char **argv, char **azColName) {
      //   InSANERunReportDatum * someDatum = (InSANERunReportDatum*) datum;
      //   if (argc == 1) { // should equal 1!
   //      someDatum->fValue = argv[0];
   //      return 0;
   //   }
   //   else {
   //      std::cout << " x argc does not equal 1 it is " << argc << "\n";
   //      return 1;
   //   }
   //}

   ClassDef(InSANERunReportDatum, 2)
};

/**
 * \ingroup Runs
 */

/**  Base class for a run report which is stored in a SQL database.
 *
 *   RunReport is created two ways: For a new InSANERun or for an exsiting InSANERun.
 *   First for a newly created InSANERun :
 *     - SANERunManager::GeneratePreRunReport() which does nothing (yet) but call InSANERunManager::GeneratePreRunReport
 *     - InSANERunManager::GeneratePreRunReport()
 *       - Creates new RunReport.
 *       - Creates each of the RunReport's InSANERunReportDatum
 *       - Calls InSANERunReport::RetrieveRunInfo, which gets data from existing DB entry
 *         - Uses scripts/GatherRunInfo.cxx to scrape data from many external (primary) sources.
 *           These include databases other than "run_info".
 *       - Calls InSANERunReport::UpdateRunDatabase,
 *       - Calls InSANERunReport::UpdateRun
 *       - Continues to standard GenerateRunReport
 *
 *   For an existing InSANERun:
 *     - SANERunManager::GenerateRunReport() which does nothing (yet) but call InSANERunManager::GenerateRunReport
 *     - InSANERunManager::GenerateRunReport()
 *       - Creates new RunReport.
 *       - Creates each of the RunReport's InSANERunReportDatum (and sets some... needs work)
 *       - Calls InSANERunReport::UpdateRun which sets RunReportDatum values using the current run
 *
 */
class InSANERunReport : public TObject {
public:

   InSANERunReport(InSANERun * aRun = nullptr);

   virtual  ~InSANERunReport();

   /** Returns a pointer to a InSANERunReportDatum
    *  Returns Null pointer if not found.
    */
   InSANERunReportDatum * GetDatum(const char * name) {
      InSANERunReportDatum * result = nullptr;
      for (unsigned int i = 0; i < fReportData->size() ; i++) {
         if (!strcmp(fReportData->at(i)->fName.Data(), name)) {
            result = fReportData->at(i) ;
         }
      }
      return(result);
   }

   /** Returns the value (in string form) of the
    *  table column name given for the current run.
    */
   const char * GetValueOf(const char * name) {
      const char * result = "";
      for (unsigned int i = 0; i < fReportData->size() ; i++) {
         if (!strcmp(fReportData->at(i)->fName.Data(), name)) {
            result = fReportData->at(i)->fValue.Data() ;
         }
      }
      return(result);
   }

   /** Sets the value (in string form) of the
    *  table column name given for the current run.
    */
   void SetValueOf(const char * name, const char * value) {
      for (unsigned int i = 0; i < fReportData->size() ; i++) {
         if (!strcmp(fReportData->at(i)->fName.Data(), name)) {
            fReportData->at(i)->fValue = value;
            fReportData->at(i)->isUpdated = true;
         }
      }
   }


   /** Builds up the vector fReportData by adding InSANERunReportDatum
    * for each column in the mysql table 'run_info'
    */
   void SetRun(InSANERun * aRun);

   /** Add a run report datum to the list of datum
    *  Each datum should be a column in the run_info table
    */
   void Add(InSANERunReportDatum* datum) {
      fReportData->push_back(datum);
   }

   /** Called during the InSANERunManger::GeneratePreRunReport
    *
    *  Gathers info from many database tables
    *  These include:
    *  - run_info
    *  - qe_polsource
    */
   Int_t RetrieveRunInfo() ;

   /** SQLite callback used to get RunReport data stored in SQLite DB.
    */
   static int RetrieveRunInfo_callback(void * runRep, int argc, char **argv, char **azColName) {
//       int i;
//       std::cout << "RetrieveRunInfo_callback\n Number of args= " << argc << std::endl;
//       for(i=0; i<argc; i++) {
//          if(argv[i] ) /// Not Null
//             std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
//       }
//       std::cout << std::endl;
      auto * rep = (InSANERunReport*) runRep;
      if (argc > 0) {
         for (int k = 0; k < argc ; k++) {
            if (argv[k]) /// Not Null
               rep->fReportData->at(k)->fValue = argv[k];
            rep->fReportData->at(k)->isUpdated = true;
         }
      } else {
         std::cout << " x argc is " << argc << "\n";
      }
      return 0;
   }

   void Print(Option_t * opt = "") const ;

   /** Update the InSANERun object using this run report.
    */
   void UpdateRun();

   /** Updates this run report using the current InSANERun.
    */
   void Update();

   /** Updates the SQLite3 Database using the run report.
    */
   void UpdateRunDatabase();

   const char * GetInsertValues();

   InSANERun * fRun;
   TString * fReplaceString;
   TString fSelectString;

   /// The list of mysql datum
   std::vector<InSANERunReportDatum*> * fReportData;

   ClassDef(InSANERunReport, 1)
};
#endif

