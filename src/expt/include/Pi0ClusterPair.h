#ifndef Pi0ClusterPair_HH
#define Pi0ClusterPair_HH 1

#include "TObject.h"
#include "BIGCALCluster.h"

/** A pair of clusters which are used to reconstruct a pi0 event
 *
 * \ingroup pi0
 */
class Pi0ClusterPair : public TObject {

   public:
      Double_t      fAngle;     // Angle between two clusters
      Double_t      fMass;      // Invariant mass of two clusters
      Double_t      fMomentum;  // momentum sum of two clusters (massless particles)
      Double_t      fTheta;     // angle of momentum sum
      Double_t      fPhi;       // angle of momentum sum
      Double_t      fEnergy;    // Reconstructed pi0 energy
      Int_t         fHelicity;  // Beam helicity during event

      BIGCALCluster fCluster1;
      BIGCALCluster fCluster2;

   public:
      Pi0ClusterPair();
      Pi0ClusterPair(BIGCALCluster* c1, BIGCALCluster* c2);
      Pi0ClusterPair(const BIGCALCluster& c1, const BIGCALCluster& c2);
      virtual ~Pi0ClusterPair();
      const  Pi0ClusterPair& operator=(const Pi0ClusterPair& rhs);
      void Clear(Option_t * opt = "");

      ClassDef(Pi0ClusterPair,6)
};

#endif

