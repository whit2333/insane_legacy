#ifndef ANNEvents_HH
#define ANNEvents_HH 1

#include "TObject.h"
#include "TObjString.h"
#include "TString.h"

#include "BIGCALCluster.h"
#include "Pi0ClusterPair.h"


/** Generic version of ANNDisElectronEvent.  */
class ANNEvent : public TObject {

   private:
      TList fInputNeurons;
      TList fOutputNeurons;
      std::vector<Double_t*> fInputValues;
      std::vector<Double_t*> fOutputValues;

   public:
      ANNEvent() ;
      virtual ~ANNEvent();

      void         AddInputNeuron(const char * name, Double_t * address);
      void         AddOutputNeuron(const char * name, Double_t * address);
      const char * GetInputNeurons();
      const char * GetOutputNeurons();
      void         GetInputNeuronArray(Double_t * v);
      void         Clear();

      ClassDef(ANNEvent, 1)
};



/** Event structure used to train Neural Network for single DIS electron event.
 */
class ANNDisElectronEvent : public TObject {

   protected:
      TString  fInputNeurons;
      Int_t    fNInputNeurons;

   public:
      Double_t fTrueEnergy   ; // Energy output
      Double_t fDelta_Energy ; // Correction output
      Double_t fTrueTheta    ; // output 
      Double_t fDelta_Theta  ; // output
      Double_t fTruePhi      ; // output
      Double_t fDelta_Phi    ; // output
      Bool_t   fBackground   ; // output
      Bool_t   fSignal       ; // output Redundant?

      Double_t fClusterTheta     ; // input
      Double_t fClusterPhi       ; // input
      Double_t fXCluster         ; // input
      Double_t fYCluster         ; // input
      Double_t fXClusterSigma    ; // input
      Double_t fYClusterSigma    ; // input
      Double_t fXClusterSkew     ; // input
      Double_t fYClusterSkew     ; // input
      Double_t fClusterEnergy    ; // input
      Double_t fCherenkovADC     ; // input
      Bool_t   fGoodCherenkovHit ; // input
      Int_t    fNClusters        ; // input

   public:
      ANNDisElectronEvent();
      virtual ~ANNDisElectronEvent();

      virtual void   GetMLPInputNeuronArray(Double_t * par) const;
      virtual void   Clear();
      virtual void   SetEventValues(BIGCALCluster * clust);

      Int_t          GetNInputNeurons() const ;
      const char *   GetMLPInputNeurons() const;

      ClassDef(ANNDisElectronEvent,3)
};



/** Like ANNDisElectronEvent however it adds new inputs which might help separate electrons from positrons.
 *  Adds tracker DeltaX and DeltaY
 */
class ANNDisElectronEvent2 : public ANNDisElectronEvent {

   public:
      Double_t fTrackerDeltaX;
      Double_t fTrackerDeltaY;

   public:
      ANNDisElectronEvent2();
      virtual ~ANNDisElectronEvent2();

      virtual void GetMLPInputNeuronArray(Double_t * par) const ;
      virtual void Clear() ;

      ClassDef(ANNDisElectronEvent2,2)
};



/** Like ANNDisElectronEvent2 but without fNClusters as input.
 */
class ANNDisElectronEvent3 : public ANNDisElectronEvent2 {
   public:
      ANNDisElectronEvent3();
      virtual ~ANNDisElectronEvent3();

      virtual void GetMLPInputNeuronArray(Double_t * par) const ;

      ClassDef(ANNDisElectronEvent3,2)
};



/** Like ANNDisElectronEvent however it uses cluster x and y positions instead of theta and phi.
 */
class ANNDisElectronEvent4 : public ANNDisElectronEvent {
   public : 
      Double_t fClusterDeltaX;  // output
      Double_t fClusterDeltaY;  // output

   public:
      ANNDisElectronEvent4();
      virtual ~ANNDisElectronEvent4();

      virtual void GetMLPInputNeuronArray(Double_t * par) const ;
      virtual void Clear();

      ClassDef(ANNDisElectronEvent4,5)
};

/** Like ANNDisElectronEvent however it uses cluster x and y positions instead of theta and phi.
 *  Also it uses the kurtosis as an input. 
 *
 */
class ANNDisElectronEvent5 : public ANNDisElectronEvent {

   public:
      Double_t fXClusterKurt;  // input
      Double_t fYClusterKurt;  // input
      Double_t fClusterDeltaX; // output
      Double_t fClusterDeltaY; // output

   public:
      ANNDisElectronEvent5();
      virtual ~ANNDisElectronEvent5();

      virtual void GetMLPInputNeuronArray(Double_t * par) const ;
      virtual void Clear();
      virtual void SetEventValues(BIGCALCluster * clust);

      ClassDef(ANNDisElectronEvent5,2)
};



/** Like ANNDisElectronEvent5 but with raster x and y positions.
 */
class ANNDisElectronEvent6 : public ANNDisElectronEvent5 {

   public:
      Double_t fRasterX; // input
      Double_t fRasterY; // input

   public:
      ANNDisElectronEvent6();
      virtual ~ANNDisElectronEvent6();

      virtual void GetMLPInputNeuronArray(Double_t * par) const ;
      virtual void Clear();

      ClassDef(ANNDisElectronEvent6,2)
};



/** NN event for a pi0 decay to two gamma.
 *
 */
class ANNPi0TwoGammaEvent {
   public:
      ANNPi0TwoGammaEvent() {
         fClusterPair = nullptr;
         Clear();
      }
      virtual ~ANNPi0TwoGammaEvent() {}

      void Clear() {
         fNClusters = 0;

         fClustersMass = 0.0;
         fClustersOpeningAngle = 0.0;
         fEGamma1 = 0.0;
         fEGamma2 = 0.0;
         fDelta_OpeningAngle = 0.0;
         fDelta_EGamma1 = 0.0;
         fDelta_EGamma2 = 0.0;
         fSignal = false;
      }

      /// Total Number of clusters found in the event
      Int_t fNClusters;
      Double_t fClustersMass;
      Double_t fClustersOpeningAngle;
      Double_t fDelta_OpeningAngle;
      Double_t fEGamma1;
      Double_t fEGamma2;
      Double_t fDelta_EGamma1;
      Double_t fDelta_EGamma2;
      Bool_t   fSignal;
      Pi0ClusterPair * fClusterPair; //->
      TString fInputNeurons;

      ClassDef(ANNPi0TwoGammaEvent, 1)
};

#endif

