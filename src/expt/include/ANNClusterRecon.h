#ifndef ANNClusterRecon_HH
#define ANNClusterRecon_HH 1

#include "TVector3.h"
#include "TRotation.h"

#include "BIGCALCluster.h"
#include "SANENeuralNetworks.h"
#include "HallCBeamEvent.h"
//#include "SANEEvents.h"

/** The go to class for using the neural networks.
 * 
 *
 */
class ANNClusterRecon {

   private:
      Double_t                                fNNParams[20];
      TVector3                                fP_Naive;

   protected:
      BIGCALCluster  fCluster;
      Bool_t         fIsPerp;
      Double_t       fBigcalZ;

      // ------------------------------------------------------------------------
      // Electrons
      NNParaElectronXYCorrectionClusterDeltaX   fNNParaElectronClusterDeltaX;
      NNParaElectronXYCorrectionClusterDeltaY   fNNParaElectronClusterDeltaY;

      NNParaElectronAngleCorrectionDeltaTheta   fNNParaElectronDeltaTheta;
      NNParaElectronAngleCorrectionDeltaPhi     fNNParaElectronDeltaPhi;
      
      NNParaElectronAngleCorrectionBCPDirTheta  fNNParaElectronBCPDirTheta;
      NNParaElectronAngleCorrectionBCPDirPhi    fNNParaElectronBCPDirPhi;

      NNPerpElectronXYCorrectionClusterDeltaX   fNNPerpElectronClusterDeltaX;
      NNPerpElectronXYCorrectionClusterDeltaY   fNNPerpElectronClusterDeltaY;

      NNPerpElectronAngleCorrectionDeltaTheta   fNNPerpElectronDeltaTheta;
      NNPerpElectronAngleCorrectionDeltaPhi     fNNPerpElectronDeltaPhi;
      
      NNPerpElectronAngleCorrectionBCPDirTheta  fNNPerpElectronBCPDirTheta;
      NNPerpElectronAngleCorrectionBCPDirPhi    fNNPerpElectronBCPDirPhi;

      ANNDisElectronEvent6                      fANNDisElectronEvent;    // For cluster XY correction
      ANNElectronThruFieldEvent2                fANNFieldEvent2;         // For track angle correction
      ANNElectronThruFieldEvent3                fANNFieldEvent3;         // For momentum vector correction

      // ------------------------------------------------------------------------
      // Photons
      NNParaGammaXYCorrectionClusterDeltaX      fNNParaDeltaX;
      NNParaGammaXYCorrectionClusterDeltaY      fNNParaDeltaY;

      NNPerpGammaXYCorrectionClusterDeltaX      fNNPerpDeltaX;
      NNPerpGammaXYCorrectionClusterDeltaY      fNNPerpDeltaY;

      ANNGammaEvent                             fANNGammaEvent;

      // ------------------------------------------------------------------------
      // Positrons


   public:

      HallCBeamEvent                        * fBeamEvent;
      TVector3                                fX_BigcalPlane;
      TVector3                                fX_Vertex;
      TVector3                                fP_BigcalPlane;
      TRotation                               fRotFromBCcoords;

   public:
      ANNClusterRecon();
      ~ANNClusterRecon();

      const  BIGCALCluster& AsPhoton();
      const  BIGCALCluster& AsElectron();
      const  BIGCALCluster& AsPositron();

      void   SetCluster(const BIGCALCluster& clust) {fCluster = clust;}
      const  BIGCALCluster& GetCluster() { return fCluster;}

      void   SetBeamEvent(HallCBeamEvent * ev) { fBeamEvent = ev; }
      HallCBeamEvent * GetBeamEvent() { return fBeamEvent; }
         
      void   SetPerp(Bool_t v = 1){fIsPerp = v;}
      Bool_t IsPerp() const { return fIsPerp;}

      ClassDef(ANNClusterRecon,1)
};

#endif

