#ifndef InSANEAveragedMeasuredAsymmetry_HH
#define InSANEAveragedMeasuredAsymmetry_HH 1

#include "TNamed.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "InSANEMeasuredAsymmetry.h"
//#include "InSANECombinedAsymmetry.h"

/** Used to combine multiple InSANEMeasuredAsymmetry classes into one measurement.
 *  The Weighted mean for each bin is computed.
 */ 
class InSANEAveragedMeasuredAsymmetry : public TNamed {

   protected:

      Int_t                     fNAsymmetries;
      InSANEMeasuredAsymmetry   fAsymmetryResult;
      //InSANECombinedAsymmetry   fCombinedResult;

      InSANEMeasuredAsymmetry   fA1; // histograms for storing sum
      InSANEMeasuredAsymmetry   fA2; // histograms for storing weights

   public:

      InSANEAveragedMeasuredAsymmetry();
      InSANEAveragedMeasuredAsymmetry(const InSANEAveragedMeasuredAsymmetry&)            ;
      InSANEAveragedMeasuredAsymmetry& operator=(const InSANEAveragedMeasuredAsymmetry&) ;
      //InSANEAveragedMeasuredAsymmetry(InSANEAveragedMeasuredAsymmetry&&)                 = default;
      //InSANEAveragedMeasuredAsymmetry& operator=(InSANEAveragedMeasuredAsymmetry&&)      = default;
      virtual ~InSANEAveragedMeasuredAsymmetry();

      Bool_t         IsFolder() const { return kTRUE; }

      void           Browse(TBrowser* b) { b->Add(&fAsymmetryResult,"A-Result"); }

   protected:

      //Int_t InitHists(TH1F & h0, TH1F & h1, TH1F & h2);

      /** Takes the input hisogram (h0) and adds it to the sums. 
       *  \f$ \sum A_i/\sigma_i^2 \f$ and \f$ \sum 1/\sigma_i^2 \f$ 
       *
       *  These quantities are used in calculating the weighted means via
       *
       *  \f$ A_{\mu} = \frac{\sum A_i/\sigma_i^2}{\sum 1/\sigma_i^2} \f$ 
       *
       *  \f$ \sigma_{\mu}^2 = \frac{1}{\sum 1/\sigma_i^2} \f$
       */
      Int_t AddToSum(const TH1F & h0, TH1F & h1, TH1F & h2) const;

      Int_t CalculateMean(TH1F & h0, const TH1F & h1, const TH1F & h2);

   public:

      InSANEMeasuredAsymmetry* GetMeasuredAsymmetryResult() { return(&fAsymmetryResult);}

      //InSANECombinedAsymmetry* GetAsymmetryResult() { return(&fCombinedResult);}

      void Init(const InSANEMeasuredAsymmetry& a);

      /** Add asymmetry to average. 
       * This keeps a running tally of the sums 
       *  \f$ \sum A_i/\sigma_i^2 \f$ and \f$ \sum 1/\sigma_i^2} \f$ 
       *  in histograms which are initiallized by the first asymmetry added.
       *  See InSANEAveragedMeasuredAsymmetry::Calculate() for more details.
       */ 
      Int_t Add(const InSANEMeasuredAsymmetry & asym);

      /** Calculates the weighted mean with the standard error on the mean.
       *
       *  \f$ A_{\mu} = \frac{\sum A_i/\sigma_i^2}{\sum 1/\sigma_i^2} \f$ 
       *
       *  \f$ \sigma_{\mu}^2 = \frac{1}{\sum 1/\sigma_i^2} \f$
       *  
       *  where the error is then \f$ \sqrt{\sigma_{\mu}^2} \f$.
       */ 
      InSANEMeasuredAsymmetry Calculate();

      ClassDef(InSANEAveragedMeasuredAsymmetry,12)
   };

#endif

