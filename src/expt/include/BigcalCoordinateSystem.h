#ifndef BigcalCoordinateSystem_h
#define BigcalCoordinateSystem_h

#include <TROOT.h>
#include <TFile.h>
#include "InSANECoordinateSystem.h"
#include "TRotation.h"
#include "TVector3.h"
#include "BIGCALGeometryCalculator.h"

/** Bigcal coordinate system.
 *
 * Bigcal Coordinate system. (x is horizontal, y is vertical and z is rotated 40 degrees from beam)
 * The origin is at the face of bigcal!
 *
 * \note you need not know about all other coordinate systems, just a path to get to the relevent ones.
 *
 * \ingroup Coordinates
 * \ingroup BIGCAL
 */
class BigcalCoordinateSystem  : public InSANECoordinateSystem {
public :
   BigcalCoordinateSystem(const char * name = "bigcalCoords"):
      InSANECoordinateSystem(name) {
      ;
   }
   ~BigcalCoordinateSystem() {};

   TRotation & GetRotationMatrixTo(const char * system = "humanCoords") {
      BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
      if (!strcmp(system, "Human") ||
          !strcmp(system, "HUMAN") ||
          !strcmp(system, "human") ||
          !strcmp(system, "HumanCoords") ||
          !strcmp(system, "humanCoords")) {
         fRotationMatrix.SetToIdentity();
         fRotationMatrix.RotateY(geocalc->fNominalAngleSetting * TMath::Pi() / (180.0));
      } else if (!strcmp(system, "Hms") ||
                 !strcmp(system, "HMS") ||
                 !strcmp(system, "hms") ||
                 !strcmp(system, "hmsInSANECoordinateSystem")) {
         fRotationMatrix.SetToIdentity();
         fRotationMatrix.RotateY(-1.0 * geocalc->fNominalAngleSetting * TMath::Pi() / (2.0 * 180.0));
         fRotationMatrix.RotateZ(-1.0 * TMath::Pi() / 2.0);
      } else {
         std::cout << " Unknown coordinate system, " << system << "! Returning identity matrix.\n";
         fRotationMatrix.SetToIdentity();
      }
      return(fRotationMatrix);
   }

   TVector3 & GetOriginTranslationTo(const char * system  = "humanCoords") {
      BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
      if (!strcmp(system, "Human") ||
          !strcmp(system, "HUMAN") ||
          !strcmp(system, "human") ||
          !strcmp(system, "HumanCoords") ||
          !strcmp(system, "humanCoords")) {
         fTranslation = TVector3(
                           geocalc->fNominalRadialDistance * TMath::Sin(geocalc->fNominalAngleSetting * TMath::Pi() / 180.0),
                           0.0,
                           geocalc->fNominalRadialDistance * TMath::Cos(geocalc->fNominalAngleSetting * TMath::Pi() / 180.0));
      }
      return(fTranslation);
   }

   void DescribeCoordinateSystem() {
      std::cout << " == Bigcal Coordinate System == \n";
      std::cout << "Origin is at the face of BIGCAL \n";
   }

private :

   ClassDef(BigcalCoordinateSystem, 1)
};


#endif
