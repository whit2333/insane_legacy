#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedefs;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

//#pragma link C++ namespace CLHEP;
//#pragma link C++ namespace InSANE;
//#pragma link C++ namespace InSANE::Kine;
//#pragma link C++ namespace InSANE::Math;
//
#pragma link C++ class InSANECluster+;
#pragma link C++ class BIGCALCluster+;
#pragma link C++ class Pi0ClusterPair+;
#pragma link C++ class ANNClusterRecon+;

#pragma link C++ class InSANEClusterProcessor+;
#pragma link C++ class InSANECalorimeterCluster+;
#pragma link C++ class InSANEClusterDatum+;
#pragma link C++ class InSANEPartitionCentroid+;

#pragma link C++ class LocalMaxClusterProcessor+;

#pragma link C++ class KMeansClusterProcessor+;

#pragma link C++ class BIGCALClusterProcessor+;

#pragma link C++ class SANEClusteringAnalysis+;

#pragma link C++ class InSANEClusterPartitioning+;

// #pragma link C++ class InSANEClustering<TObject*>+;
// #pragma link C++ class InSANEClustering<InSANEClusteringSeed*>+;
// #pragma link C++ class InSANEClustering<BIGCALCluster*>+;
//#pragma link C++ class InSANEClustering<TObject*>+;


#pragma link C++ class InSANECut+;
#pragma link C++ class InSANECutter+;

#pragma link C++ class InSANESlowSkimmer+;
#pragma link C++ class InSANESkimmerFilter+;
#pragma link C++ class InSANEBeamCurrentFilter+;

#pragma link C++ class InSANECorrection+;
#pragma link C++ class InSANECalculation+;

#pragma link C++ class BETACorrection+;
#pragma link C++ class BETACalculation+;
#pragma link C++ class BETAHistogramCalc000+;
#pragma link C++ class BETAHistogramCalc0+;
#pragma link C++ class BETAHistogramCalc1+;
#pragma link C++ class BETAEventCounterCorrection+;

#pragma link C++ class BETAPedestalCalculation+;

#pragma link C++ class BETAPedestalCorrection+;
#pragma link C++ class BETATimingCorrection+;
#pragma link C++ class BETATimeWalkCorrection+;

#pragma link C++ class HallCBeamCalculation0+;


#pragma link C++ class BigcalCalculation1+;
#pragma link C++ class BETACalculationWithClusters+;

#pragma link C++ class GasCherenkovCalculation1+;
#pragma link C++ class GasCherenkovCalculation2+;
#pragma link C++ class GasCherenkovCalculation3+;
#pragma link C++ class GasCherenkovCalculation4+;

#pragma link C++ class LuciteHodoscopeCalculation1+;
#pragma link C++ class LuciteHodoscopeCalculation2+;
#pragma link C++ class LuciteHodoscopeCalculation3+;
#pragma link C++ class LuciteHodoscopeCalculation4+;

#pragma link C++ class ForwardTrackerCalculation1+;
#pragma link C++ class ForwardTrackerCalculation2+;



#pragma link C++ class BigcalClusteringCalculation1+;
#pragma link C++ class BigcalPi0Calculations+;
#pragma link C++ class Bigcal3ClusterPi0Calculation+;
#pragma link C++ class BigcalOverlapCalculation+;

#pragma link C++ class Pi0Calibration+;
#pragma link C++ class Pi0Calibration2+;

#pragma link C++ class SANETrajectoryCalculation1+;
#pragma link C++ class SANETrajectoryCalculation2+;
#pragma link C++ class SANETrajectoryCalculation3+;
#pragma link C++ class SANETrajectoryCalculation4+;

#pragma link C++ class SANEElectronSelector+;
#pragma link C++ class SANEElectronSelector2+;
#pragma link C++ class SANEElasticElectronSelector+;
#pragma link C++ class SANEAsymmetryCalculation1+;
// #pragma link C++ class SANEAsymmetryCalculation2+;
//
#pragma link C++ class SANETrackCalculation1+;
#pragma link C++ class SANETrackCalculation2+;

//#pragma link C++ class InSANECorrector+;
#pragma link C++ class InSANEAnalyzer+;

#pragma link C++ class SANEZerothPassAnalyzer+;
#pragma link C++ class SANEFirstPassAnalyzer+;
#pragma link C++ class SANESecondPassAnalyzer+;
#pragma link C++ class SANEPi0PassAnalyzer+;

// #pragma link C++ class InSANETrajectoryAnalysis+;
// #pragma link C++ class SANETrajectoryAnalyzer+;

#pragma link C++ class SANEScalerAnalyzer+;
#pragma link C++ class GasCherenkovLEDAnalyzer+;

#pragma link C++ class BigcalClusterAnalyzer+;
#pragma link C++ class BETAGeneralAnalyzer+;

//#pragma link C++ class SANEFinalAnalyzer+;

#pragma link C++ class InSANEScalerCalculation+;
#pragma link C++ class BETADeadTimeCalculation+;
#pragma link C++ class SANEScalerCalculation+;

#pragma link C++ class InSANERunFlag+;


#pragma link C++ class InSANEEvent+;

#pragma link C++ class InSANEDISEvent+;
#pragma link C++ class InSANEDetectorEvent+;
#pragma link C++ class InSANEDetectorPackageEvent+;
#pragma link C++ class InSANEDetectorHit+;
#pragma link C++ class InSANEScalerEvent+;
#pragma link C++ class InSANEMonteCarloEvent+;

//#pragma link C++ class InSANEEventGenerator+;
//#pragma link C++ class InSANETargetEventGenerator+;
//#pragma link C++ class BETAG4SavedEventGenerator+;

#pragma link C++ class SANEScalerEvent+;

#pragma link C++ class InSANEEventDisplay+;
#pragma link C++ class BETAEventDisplay+;

#pragma link C++ class InSANEEventDatum+;
#pragma link C++ class std::vector<InSANEEventDatum*>+;
// #pragma link C++ global fgInSANEEventDisplay+;

#pragma link C++ class Pi0EventDisplay+;
#pragma link C++ class HMSElasticEventDisplay+;

#pragma link C++ class BigcalHit+;
#pragma link C++ class GasCherenkovHit+;

#pragma link C++ class ForwardTrackerHit+;
#pragma link C++ class ForwardTrackerPositionHit+;

#pragma link C++ class LuciteHodoscopeHit+;
#pragma link C++ class LuciteHodoscopePositionHit+;

#pragma link C++ class HallCBeamEvent+;
#pragma link C++ class HMSEvent+;

#pragma link C++ global fgBigcalHits+;
#pragma link C++ global fgBigcalTimingGroupHits+;
#pragma link C++ global fgBigcalTriggerGroupHits+;

#pragma link C++ global fgGasCherenkovHits+;
#pragma link C++ global fgGasCherenkovTDCHits+;
#pragma link C++ global fgGasCherenkovADCHits+;

#pragma link C++ global fgForwardTrackerHits+;
#pragma link C++ global fgForwardTrackerPositionHits+;

#pragma link C++ global fgLuciteHodoscopeHits+;
#pragma link C++ global fgLuciteHodoscopePositionHits+;

#pragma link C++ class BigcalEvent+;
#pragma link C++ class InSANEFakePlaneHit+;
#pragma link C++ class GasCherenkovEvent+;
#pragma link C++ class ForwardTrackerEvent+;
#pragma link C++ class LuciteHodoscopeEvent+;
#pragma link C++ class BETAEvent+;
#pragma link C++ class BETAG4MonteCarloEvent+;


#pragma link C++ class Pi0Event+;
#pragma link C++ class Pi03ClusterEvent+;

#pragma link C++ class InSANETrajectory+;
#pragma link C++ class InSANEDISTrajectory+;

#pragma link C++ class InSANETrack+;
#pragma link C++ class InSANEDISTrack+;

#pragma link C++ class HMSElasticEvent+;

#pragma link C++ class CleanedBETAEvent+;

#pragma link C++ class InSANEPhysicsEvent+;
#pragma link C++ class InSANETriggerEvent+;
#pragma link C++ class InSANETriggerCounter+;

#pragma link C++ class InSANEAnalysisEvents+;
#pragma link C++ class SANEEvents+;
#pragma link C++ class SANEScalers+;

#pragma link C++ class InSANEHitPosition+;
#pragma link C++ class std::vector<InSANEHitPosition>+;
//#pragma link C++ class std::vector<InSANEHitPosition>::iterator+;

#pragma link C++ class InSANEClusterEvent+;
#pragma link C++ class InSANEReconstructedEvent+;

#pragma link C++ class ANNEvent+;
#pragma link C++ class ANNElectronThruFieldEvent+;
#pragma link C++ class ANNElectronThruFieldEvent2+;
#pragma link C++ class ANNElectronThruFieldEvent3+;
#pragma link C++ class ANNDisElectronEvent+;
#pragma link C++ class ANNDisElectronEvent2+;
#pragma link C++ class ANNDisElectronEvent3+;
#pragma link C++ class ANNDisElectronEvent4+;
#pragma link C++ class ANNDisElectronEvent5+;
#pragma link C++ class ANNDisElectronEvent6+;
#pragma link C++ class ANNPi0TwoGammaEvent+;
#pragma link C++ class ANNGammaEvent+;





//#pragma link C++ class std::vector<TString*>+;
//#pragma link C++ class std::map<int, double >+;
////#pragma link C++ class std::map<int, double >::iterator+;
////#pragma link C++ class std::pair<int, double >+;
//
////#pragma link C++ class std::map<int, std::vector<double> >+;
////#pragma link C++ class std::pair<int, std::vector<double> >+;
////#pragma link C++ class std::map<int, std::vector<double> >::iterator+;
//
//#pragma link C++ class std::vector<TGraph*>+;
//
//#pragma link C++ class InSANEAsymmetryBase+;
//#pragma link C++ class InSANEAsymmetriesFromStructureFunctions+;

//#pragma link C++ class InSANEFunctionManager+;
//#pragma link C++ global fgInSANEFunctionManager+;

#pragma link C++ class InSANEDetector+;
#pragma link C++ class InSANEAnalysis+;
#pragma link C++ class BETAG4Analysis+;
#pragma link C++ class BETAG4DetectorsAnalysis+;
#pragma link C++ class InSANEDetectorAnalysis+;
#pragma link C++ class InSANEScalerAnalysis+;
#pragma link C++ class SANEElasticAnalysis+;

#pragma link C++ class InSANEDetectorPackage+;
#pragma link C++ class InSANEDetectorPedestal+;
#pragma link C++ class InSANETDCSpectrum+;
#pragma link C++ class InSANEDetectorTiming+;
#pragma link C++ class std::vector<InSANEDetectorTiming*>+;
#pragma link C++ class std::vector<InSANEDetectorPedestal*>+;
#pragma link C++ class InSANEAsymmetryDetector+;
#pragma link C++ class InSANEAsymmetryDetectorEvent+;

#pragma link C++ class InSANECalorimeterDetector+;
#pragma link C++ class GasCherenkovDetector+;
#pragma link C++ class LuciteHodoscopeDetector+;
#pragma link C++ class ForwardTrackerDetector+;
#pragma link C++ class ForwardTrackerScintillator+;
#pragma link C++ class BigcalDetector+;
#pragma link C++ class BETADetectorPackage+;
#pragma link C++ class BETASubDetector+;
#pragma link C++ class BETAPseudoSpectrometer+;
#pragma link C++ class BETAPseudoSpectrometersCalc+;
#pragma link C++ class std::vector<BETAPseudoSpectrometer*>+;
#pragma link C++ class BETACherenkovMirrorArray+;

#pragma link C++ class BigcalPi0Calibrator+;
#pragma link C++ class Pi0PhotonEnergy+;
#pragma link C++ class Pi0MassSquared+;
#pragma link C++ class BigcalPi0Calibrator+;

#pragma link C++ class AnalyzerToInSANE+;
#pragma link C++ class AnalyzerScalersToInSANE+;

//#pragma link C++ class InSANEPhysics+;
//#pragma link C++ class InSANEQSquared+;

#pragma link C++ class InSANEAnalysis+;
#pragma link C++ class InSANERunNum+;
#pragma link C++ class InSANERunSummary+;
#pragma link C++ class InSANERun+;

#pragma link C++ enum InSANERun::SANETargets;
#pragma link C++ enum InSANERun::SANETargetSign;
#pragma link C++ enum InSANERun::SANETargetCup;

#pragma link C++ global kgSANETargets+;
#pragma link C++ global kgSANETargetNames+;
#pragma link C++ global kgSANETargetCupNames+;
#pragma link C++ global kgSANETargetSignName+;
#pragma link C++ global kgSANETargetCups+;
#pragma link C++ global kgSANETargetSign+;

#pragma link C++ class InSANERunReport+;
#pragma link C++ class InSANERunReportDatum+;
#pragma link C++ class std::vector<InSANERunReportDatum*>+;
#pragma link C++ class InSANERunTiming+;
#pragma link C++ class SANEProductionRun+;
#pragma link C++ class BETAG4SimulationRun+;

#pragma link C++ class std::map<Int_t,TFile*>+;
//#pragma link C++ class std::pair<Int_t,TFile*>+;

#pragma link C++ class SANERunManager+;
#pragma link C++ class InSANERunManager+;
#pragma link C++ global fgSANERunManager+;

#pragma link C++ class BigcalElasticClusterAnalysis+;
#pragma link C++ class BETAPedestalAnalysis+;
#pragma link C++ class BETATimingAnalysis+;
#pragma link C++ class SANEGasCherenkovAnalysis+;
#pragma link C++ class SANELuciteHodoscopeAnalysis+;

#pragma link C++ class InSANEClusterAnalysis+;

#pragma link C++ class InSANEReconstruction+;

#pragma link C++ class SANERunOverview+;
#pragma link C++ class SANEParallelRunOverview+;
#pragma link C++ class SANEPerpendicularRunOverview+;
#pragma link C++ class SANECarbonRunOverview+;

#pragma link C++ class InSANECalibration+;
#pragma link C++ class InSANEDetectorCalibration+;
#pragma link C++ class std::vector<InSANECalibration*>+;
#pragma link C++ class std::vector<TParameter<double>*>+;

#pragma link C++ class InSANEAnalysisManager+;
#pragma link C++ class SANEAnalysisManager+;
#pragma link C++ global fgInSANEAnalysisManager+;
#pragma link C++ global fgSANEAnalysisManager+;

#pragma link C++ class InSANETrajectoryAnalysis+;
#pragma link C++ class SANETrajectoryAnalyzer+;

#pragma link C++ class InSANERunQuantity+;
#pragma link C++ class InSANERunSeriesObject+;
#pragma link C++ class InSANERunSeries+;
#pragma link C++ class InSANEResult+;

#pragma link C++ class InSANERunPolarization+;
#pragma link C++ class InSANERunTargOfflinePol+;
#pragma link C++ class InSANERunTargOnlinePol+;
#pragma link C++ class InSANERunBeamPol+;
#pragma link C++ class InSANERunTargTopPosPol+;
#pragma link C++ class InSANERunTargBotPosPol+;
#pragma link C++ class InSANERunTargTopNegPol+;
#pragma link C++ class InSANERunTargBotNegPol+;
#pragma link C++ class InSANERunAsymmetry+;
#pragma link C++ class InSANERawRunAsymmetry+;
#pragma link C++ class InSANERawRunAsymmetryDilution+;
#pragma link C++ class InSANERawRunAsymmetryCounts+;
#pragma link C++ class InSANERunAsymmetryGroup+;

#pragma link C++ class std::map<int,InSANERun*>+;

#pragma link C++ class InSANESystematic+;
#pragma link C++ class std::vector<InSANESystematic>+;

#pragma link C++ class InSANEAsymmetry+;
#pragma link C++ class InSANEExpParameter+;
#pragma link C++ class InSANERawAsymmetry+;

#pragma link C++ class InSANESingleBinVariables+;
#pragma link C++ class InSANEAveragedKinematics1D+;
#pragma link C++ class InSANEAveragedKinematics2D+;
#pragma link C++ class InSANEAveragedKinematics3D+;
#pragma link C++ class InSANEAveragedPi0Kinematics1D+;

//#pragma link C++ class InSANERadiativeCorrections1D+;

#pragma link C++ class InSANEMeasuredAsymmetry+;
#pragma link C++ class InSANEAveragedMeasuredAsymmetry+;
#pragma link C++ class SANEMeasuredAsymmetry+;

#pragma link C++ class InSANECombinedAsymmetry+;

#pragma link C++ class InSANEPi0MeasuredAsymmetry+;
#pragma link C++ class InSANEAveragedPi0MeasuredAsymmetry+;

#pragma link C++ class InSANEKinematicCoefficient+;
#pragma link C++ class InSANEComptonAsymmetry+;

#pragma link C++ class InSANEDatabaseManager+;
#pragma link C++ global fgInSANEDatabaseManager+;



#pragma link C++ class InSANEGeometryCalculator+;

#pragma link C++ class InSANEExperimentGeometry+;
#pragma link C++ class InSANEDetectorGeometry+;
#pragma link C++ class InSANECoordinateSystem+;
#pragma link C++ class HMSCoordinateSystem+;
#pragma link C++ class BigcalCoordinateSystem+;
#pragma link C++ class HumanCoordinateSystem+;
#pragma link C++ class BPM1CoordinateSystem+;
#pragma link C++ class BPM2CoordinateSystem+;

#pragma link C++ class InSANEMagneticField+;
#pragma link C++ class UVAOxfordMagneticField+;
#pragma link C++ class UVAEveMagField+;
#pragma link C++ class InSANETrackPropagator2+;

#pragma link C++ class InSANEGeometryCalculator+;
#pragma link C++ class BIGCALGeometryCalculator+;
#pragma link C++ class LuciteGeometryCalculator+;
#pragma link C++ class GasCherenkovGeometryCalculator+;
#pragma link C++ class ForwardTrackerGeometryCalculator+;

#pragma link C++ global fgInSANEGeometryCalculator+;
#pragma link C++ global fgLuciteGeometryCalculator+;
#pragma link C++ global fgBIGCALGeometryCalculator+;
#pragma link C++ global fgGasCherenkovGeometryCalculator+;
#pragma link C++ global fgForwardTrackerGeometryCalculator+;

#pragma link C++ class InSANEDetectorComponent+;
#pragma link C++ class InSANELeadGlassBlock+;
#pragma link C++ class BigcalLeadGlassBlock+;
#pragma link C++ class RCSLeadGlassBlock+;
#pragma link C++ class ProtvinoLeadGlassBlock+;

#pragma link C++ class InSANEApparatus+;
#pragma link C++ class InSANEElectronBeam+;
#pragma link C++ class HallCPolarizedElectronBeam+;
#pragma link C++ class HallCRasteredBeam+;
#pragma link C++ function sane_pol_(double * ,double * ,double * ,int * ,int * ,double * )+;
//#pragma link C++ class InSANETargetMaterial+;
//#pragma link C++ class InSANETarget+;
//#pragma link C++ class InSANESimpleTarget+;

//#pragma link C++ class UVAPolarizedAmmoniaTarget+;
//#pragma link C++ class UVACarbonTarget+;
//#pragma link C++ class UVACrossHairTarget+;
//#pragma link C++ class UVAPureHeliumTarget+;

#pragma link C++ class InSANECherenkovMirror+;
#pragma link C++ class InSANEToroidalMirror+;
#pragma link C++ class InSANESphericalMirror+;

#pragma link C++ class LuciteHodoscopeBar+;

//#pragma link C++ class InSANETrackPropagator+;
//
//#pragma link C++ function InSANE::Kine::SetMomFromEThetaPhi(TParticle*, double *x);
//
////---------------------- 
//
//#pragma link C++ function InSANE::Math::TestFunction22(double);
//#pragma link C++ function InSANE::Math::CGLN_F1(double,double,double);
//#pragma link C++ function InSANE::Math::CGLN_F2(double,double,double);
//#pragma link C++ function InSANE::Math::CGLN_F3(double,double,double);
//#pragma link C++ function InSANE::Math::CGLN_F4(double,double,double);
//#pragma link C++ function InSANE::Math::CGLN_F5(double,double,double);
//#pragma link C++ function InSANE::Math::CGLN_F6(double,double,double);
//#pragma link C++ function InSANE::Math::CGLN_F7(double,double,double);
//#pragma link C++ function InSANE::Math::CGLN_F8(double,double,double);
//
//#pragma link C++ function InSANE::Math::multipole_E(double,double);
//#pragma link C++ function InSANE::Math::multipole_M(double,double);
//
//#pragma link C++ function InSANE::Math::f_rad_length(double);
//#pragma link C++ function InSANE::Math::rad_length(int,int);
//
//#pragma link C++ function InSANE::Kine::v0(double,double,double);
//#pragma link C++ function InSANE::Kine::vL(double,double);
//#pragma link C++ function InSANE::Kine::vT(double,double,double);
//#pragma link C++ function InSANE::Kine::vTT(double,double);
//#pragma link C++ function InSANE::Kine::vTL(double,double,double);
//#pragma link C++ function InSANE::Kine::vTprime(double,double,double);
//#pragma link C++ function InSANE::Kine::vTLprime(double,double,double);
//
//#pragma link C++ function InSANE::Kine::Sig_Mott(double,double);
//#pragma link C++ function InSANE::Kine::fRecoil(double,double,double);
//#pragma link C++ function InSANE::Kine::tau(double,double);
//#pragma link C++ function InSANE::Kine::q_abs(double,double);
//
//#pragma link C++ function InSANE::Kine::Qsquared(double,double,double);
//#pragma link C++ function InSANE::Kine::xBjorken(double,double,double);
//#pragma link C++ function InSANE::Kine::xBjorken_EEprimeTheta(double,double,double);
//#pragma link C++ function InSANE::Kine::nu(double,double);
//
//#pragma link C++ function InSANE::Kine::W_xQsq(double,double,double);
//#pragma link C++ function InSANE::Kine::W_EEprimeTheta(double,double,double);
//#pragma link C++ function InSANE::Kine::xBjorken_WQsq(double,double,double);
//#pragma link C++ function InSANE::Kine::Q2_xW(double,double,double);
//
//#pragma link C++ function InSANE::Kine::BeamEnergy_xQ2y(double,double,double,double);
//#pragma link C++ function InSANE::Kine::Eprime_xQ2y(double,double,double,double);
//#pragma link C++ function InSANE::Kine::Eprime_W2theta(double,double,double,double);
//#pragma link C++ function InSANE::Kine::Theta_xQ2y(double,double,double,double);
//#pragma link C++ function InSANE::Kine::Theta_epsilonQ2W2(double,double,double,double);
//
//#pragma link C++ function InSANE::Kine::D(double,double,double,double);
//#pragma link C++ function InSANE::Kine::d(double,double,double,double);
//#pragma link C++ function InSANE::Kine::Eta(double,double,double);
//#pragma link C++ function InSANE::Kine::Xi(double,double,double);
//#pragma link C++ function InSANE::Kine::Chi(double,double,double,double);
//
//
//
//#pragma link C++ class InSANEWaveFunction+;
//#pragma link C++ class BonnDeuteronWaveFunction+;
//
//#pragma link C++ class InSANEFragmentationFunctions+;
//#pragma link C++ class DSSFragmentationFunctions+;
//
//#pragma link C++ class InSANETransverseMomentumDistributions+;
//#pragma link C++ class MAIDExclusivePionDiffXSec3+;
//
//#pragma link C++ class InSANEStrongCouplingConstant+;
//
//#pragma link C++ class SANE_RCs_Model0+;
//#pragma link C++ class SANE_RCs_Model1+;
//
//#pragma link C++ class InSANEPhotoAbsorptionCrossSections+;
//#pragma link C++ class MAIDPhotoAbsorptionCrossSections+;
//#pragma link C++ class InSANEStructureFunctionsFromVPCSs+;
//
//#pragma link C++ function maid07tp_(double*,double*,double*,double*,double*,double*)+; 
//#pragma link C++ function maid07tot_(int *, double*,double*,  double*,double*,double*,double*,double*,double*,double*,double*)+; 
//
//
//#pragma link C++ class InSANEAsymmetryBase+;
//#pragma link C++ class InSANEAsymmetriesFromStructureFunctions+;
//
//#pragma link C++ class InSANEStructureFunctionBase+;
//#pragma link C++ class InSANEStructureFunctions+;
//#pragma link C++ class InSANECompositeStructureFunctions+;
//#pragma link C++ class InSANECompositePolarizedStructureFunctions+;
//#pragma link C++ class LowQ2StructureFunctions+;
//#pragma link C++ class LowQ2PolarizedStructureFunctions+;
//
//#pragma link C++ class InSANEPolarizedStructureFunctions+;
//#pragma link C++ class F1F209StructureFunctions+;
//#pragma link C++ class F1F209QuasiElasticStructureFunctions+;
//#pragma link C++ class F1F209QuasiElasticFormFactors+; 
//#pragma link C++ class NMC95StructureFunctions+;
//
//#pragma link C++ class std::vector<InSANEStructureFunctions*>+;
//#pragma link C++ class std::vector<InSANEPolarizedStructureFunctions*>+;
//#pragma link C++ class std::vector<InSANEStructureFunctionsFromPDFs*>+;
//#pragma link C++ class std::vector<InSANEPolarizedStructureFunctionsFromPDFs*>+;
//
//#pragma link C++ class InSANEFormFactors+;
//#pragma link C++ class InSANEDipoleFormFactors+;
//#pragma link C++ class AMTFormFactors+;
//#pragma link C++ class MSWFormFactors+;
//#pragma link C++ class GalsterFormFactors+;
//#pragma link C++ class KellyFormFactors+;
//#pragma link C++ class RiordanFormFactors+;
//#pragma link C++ class AmrounFormFactors+;
//#pragma link C++ class BilenkayaFormFactors+;
//#pragma link C++ class BostedFormFactors+; 
//
//#pragma link C++ class InSANEPDFBase+;
//#pragma link C++ enum  InSANEPDFBase::InSANEPartonFlavor;
//#pragma link C++ class InSANEPartonDistributionFunctions+;
//#pragma link C++ class InSANEPolarizedPartonDistributionFunctions+;
//#pragma link C++ class InSANEPolarizedStructureFunctionsFromPDFs+;
//#pragma link C++ class InSANEPolarizedStructureFunctionsFromVCSAs+;
//#pragma link C++ class InSANEPolSFsFromComptonAsymmetries+;
//
//#pragma link C++ class LCWFPartonDistributionFunctions+;
//#pragma link C++ class LCWFPolarizedPartonDistributionFunctions+;
//
//#pragma link C++ class InSANEPartonHelicityDistributions+;
//#pragma link C++ class InSANEUnpolarizedPDFsFromPHDs+;
//#pragma link C++ class InSANEPolarizedPDFsFromPHDs+;
//
////#pragma link C++ class LHAPDFStructureFunctions+;
//#pragma link C++ class InSANEStructureFunctionsFromPDFs+;
//#pragma link C++ class BETAG4StructureFunctions+;
//
//
//#pragma link C++ class BBSQuarkHelicityDistributions+; 
//#pragma link C++ class BBSUnpolarizedPDFs+; 
//#pragma link C++ class BBSPolarizedPDFs+;
//#pragma link C++ class AvakianQuarkHelicityDistributions+; 
//#pragma link C++ class AvakianUnpolarizedPDFs+; 
//#pragma link C++ class AvakianPolarizedPDFs+; 
//#pragma link C++ class LSS98QuarkHelicityDistributions+; 
//#pragma link C++ class LSS98UnpolarizedPDFs+; 
//#pragma link C++ class LSS98PolarizedPDFs+; 
//#pragma link C++ class StatisticalQuarkFits+;
//#pragma link C++ class StatisticalUnpolarizedPDFs+;
//#pragma link C++ class StatisticalPolarizedPDFs+;
//#pragma link C++ class Stat2015UnpolarizedPDFs+;
//#pragma link C++ class Stat2015PolarizedPDFs+;
////#pragma link C++ class LHAPDFUnpolarizedPDFs+;
////#pragma link C++ class LHAPDFPolarizedPDFs+;
//#pragma link C++ class AAC08PolarizedPDFs+;
//#pragma link C++ class BBPolarizedPDFs+;
//#pragma link C++ class JAMPolarizedPDFs+;
//#pragma link C++ class MHKPolarizedPDFs+;
//#pragma link C++ class DNS2005PolarizedPDFs+;
//#pragma link C++ class LSS2006PolarizedPDFs+;
//#pragma link C++ class LSS2010PolarizedPDFs+;
//#pragma link C++ class DSSVPolarizedPDFs+;
//
//#pragma link C++ class InSANEPhaseSpace+;
//#pragma link C++ class InSANEPhaseSpaceVariable+;
//#pragma link C++ class InSANEDiscretePhaseSpaceVariable+;
//#pragma link C++ class std::vector<InSANEPhaseSpaceVariable*>+;
//
//#pragma link C++ class InSANEDiffXSec+;
//#pragma link C++ class InSANECompositeDiffXSec+;
//#pragma link C++ class InSANEDiffXSecKinematicKey+;
//#pragma link C++ class InSANEGridDiffXSec+;
//#pragma link C++ class InSANEGridXSecValue+;
//#pragma link C++ class InSANEInclusiveDiffXSec+;
//#pragma link C++ class InSANEInclusiveDISXSec+;
//#pragma link C++ class InSANEInclusiveBornDISXSec+;
//#pragma link C++ class InSANEExclusiveDiffXSec+;
//
//#pragma link C++ class MAIDInclusiveDiffXSec+; 
//#pragma link C++ class MAIDNucleusInclusiveDiffXSec+; 
//#pragma link C++ class MAIDKinematicKey+; 
//#pragma link C++ class MAIDPolarizedTargetDiffXSec+; 
//#pragma link C++ class MAIDPolarizedKinematicKey+; 
//
//#pragma link C++ class InSANE_VCSABase+; 
//#pragma link C++ class InSANEVirtualComptonAsymmetries+; 
//#pragma link C++ class InSANEVirtualComptonAsymmetriesModel1+; 
//#pragma link C++ class MAIDVirtualComptonAsymmetries+; 
//
//#pragma link C++ class MAIDExclusivePionDiffXSec+; 
//#pragma link C++ class MAIDInclusiveElectronDiffXSec+; 
//#pragma link C++ class MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_2D_integrand+; 
//#pragma link C++ class MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_theta+; 
//#pragma link C++ class MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_phi+; 
//
//#pragma link C++ class MAIDExclusivePionDiffXSec2+; 
//
//#pragma link C++ class MAIDInclusivePionDiffXSec+; 
//#pragma link C++ class MAIDInclusivePionDiffXSec::MAIDXSec_5Fold_2D_integrand+; 
//#pragma link C++ class MAIDInclusivePi0DiffXSec+; 
//
//#pragma link C++ class InSANEPolarizedCrossSectionDifference+; 
//
//#pragma link C++ class InSANERADCOR+;
//#pragma link C++ class InSANERADCORVariables+;
//#pragma link C++ class InSANERADCORKinematics+;
//
//#pragma link C++ class InSANERADCOR::StripApproxIntegrand1Wrap+;
//#pragma link C++ class InSANERADCOR::StripApproxIntegrand1_withDWrap+;
//#pragma link C++ class InSANERADCOR::StripApproxIntegrand2Wrap+;
//#pragma link C++ class InSANERADCOR::ExternalOnly_ExactInelasticIntegrandWrap+;
//#pragma link C++ class InSANERADCOR::External2DEnergyIntegral_IntegrandWrap+;
//
//#pragma link C++ class EsFuncWrap+;
//#pragma link C++ class EpFuncWrap+;
//// exact forms 
//// internal 
//#pragma link C++ class IntRadFuncWrap+;
//#pragma link C++ class IntRadOmegaFuncWrap+;
//#pragma link C++ class IntRadCosThkFuncWrap+;
//// external 
//#pragma link C++ class TExactFuncWrap+;
//#pragma link C++ class EsExactFuncWrap+;
//#pragma link C++ class EpExactFuncWrap+;
//#pragma link C++ class EsExactFunc2Wrap+;
//#pragma link C++ class EpExactFunc2Wrap+;
//
//// Monte Carlo integration methods 
//#pragma link off function InSANERADCOR::MCAcceptReject(const int &,double (*)(double *),double *,double *,int,int,double &,double &); 
//#pragma link off function InSANERADCOR::MCSampleMean(const int &,double (*)(double *),double *,double *,int,double &,double &); 
//#pragma link off function InSANERADCOR::MCSampleMean(const int &,double (*)(double),double *,double *,int,double &,double &); 
//#pragma link off function InSANERADCOR::MCImportanceSampling(const int,double (*)(double *),double (*)(double *,double *,double *),double (*)(int,double *,double *),double *,double *,int,double &,double &); 
////// Adaptive Simpson integration method 
//#pragma link off function InSANERADCOR::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link off function InSANERADCOR::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//// #pragma link C++ class InSANERADCOR::Full_Elastic_integrand+;
//
//#pragma link C++ class InSANERADCORInternalUnpolarizedDiffXSec+;
//
//#pragma link C++ class InSANERADCORRadiatedDiffXSec+;
//#pragma link C++ class InSANERADCORRadiatedUnpolarizedDiffXSec+;
//
//#pragma link C++ class InSANERADCOR2+;
//#pragma link C++ class InSANERADCOR2::StripApproxIntegrand1Wrap+;
//#pragma link C++ class InSANERADCOR2::StripApproxIntegrand1_withDWrap+;
//#pragma link C++ class InSANERADCOR2::StripApproxIntegrand2Wrap+;
//#pragma link C++ class InSANERADCOR2::External2DEnergyIntegral_IntegrandWrap+;
//#pragma link C++ class InSANERADCOR2::External3DIntegral_IntegrandWrap+;
//
//
//#pragma link C++ class InSANEPOLRADVariables+;
//#pragma link C++ class InSANEPOLRADKinematics+;
//
//#pragma link C++ class InSANEPOLRAD+;
////#pragma link C++ class INSANEPOLRAD::IRT_R_3+;
//#pragma link C++ class InSANEPOLRAD::IRT_2D_integrand+;
//#pragma link C++ class InSANEPOLRAD::ERTFuncWrap+;
//#pragma link C++ class InSANEPOLRAD::QRTTauFuncWrap+;
//#pragma link C++ class InSANEPOLRAD::QRTRFuncWrap+;
//#pragma link C++ class InSANEPOLRAD::IRT_TauFuncWrap_delta+;
//#pragma link C++ class InSANEPOLRAD::IRT_TauFuncWrap_2+;
//#pragma link C++ class InSANEPOLRAD::IRT_RFuncWrap_3+;
//#pragma link C++ class InSANEPOLRAD::QRT_TauFuncWrap_QE_Full+;
//#pragma link C++ class InSANEPOLRAD::QRT_RFuncWrap_QE_Full+;
//#pragma link off function InSANEPOLRAD::SimpleIntegration(double (*)(double &), double,double,double,int); 
//#pragma link off function InSANEPOLRAD::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link off function InSANEPOLRAD::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class InSANEPOLRADUltraRelativistic+;
//#pragma link C++ class InSANEPOLRADUltraRelativistic::SIG_r_2D_integrand+;
//#pragma link off function InSANEPOLRADUltraRelativistic::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link off function InSANEPOLRADUltraRelativistic::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class InSANEPOLRADInternalPolarizedDiffXSec+;
//#pragma link C++ typedef InSANEPolradDiffXSec;
//#pragma link C++ class InSANEPOLRADElasticDiffXSec+;
//#pragma link C++ class InSANEPOLRADBornDiffXSec+;
//#pragma link C++ class InSANEPOLRADRadiatedDiffXSec+;
//#pragma link C++ class InSANEPOLRADElasticTailDiffXSec+;
//#pragma link C++ class InSANEPOLRADQuasiElasticTailDiffXSec+;
//#pragma link C++ class InSANEPOLRADInelasticTailDiffXSec+;
//#pragma link C++ class InSANEPOLRADUltraRelInelasticTailDiffXSec+;
////#pragma link off function InSANEPOLRADInternalPolarizedDiffXSec::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
////#pragma link off function InSANEPOLRADInternalPolarizedDiffXSec::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class InSANERadiativeTail+;
////#pragma link C++ class InSANERadiativeTail2+;
//#pragma link C++ class InSANEElasticRadiativeTail+;
//#pragma link C++ class InSANEQuasielasticRadiativeTail+;
//#pragma link C++ class InSANEInelasticRadiativeTail+;
//#pragma link C++ class InSANEInelasticRadiativeTail2+;
//#pragma link C++ class InSANEFullInelasticRadiativeTail+;
//
//#pragma link C++ class InSANEFlatInclusiveDiffXSec+;
//#pragma link C++ class InSANEFlatExclusiveDiffXSec+;
//
//#pragma link C++ class InSANEElectroProductionXSec+;
//
//#pragma link C++ class InSANEInclusiveMottXSec+;
//#pragma link C++ class InSANEExclusiveMottXSec+;
//#pragma link C++ class InSANEepElasticDiffXSec+;
//#pragma link C++ class InSANEeInclusiveElasticDiffXSec+;
//#pragma link C++ class InSANEpInclusiveElasticDiffXSec+;
//
//#pragma link C++ class InSANEBeamSpinAsymmetry+;
//#pragma link C++ class InSANEBeamTargetAsymmetry+;
//#pragma link C++ class InSANEPolarizedDISAsymmetry+;
//
//#pragma link C++ class OARPionDiffXSec+;
//#pragma link C++ class OARPionElectroDiffXSec+;
//#pragma link C++ class OARPionPhotoDiffXSec+;
//#pragma link C++ class ElectroOARPionDiffXSec+;
//#pragma link C++ class PhotoOARPionDiffXSec+;
//
//#pragma link C++ class InSANEInclusiveWiserXSec+;
//#pragma link C++ class InSANEPhaseSpaceSampler+;
//
//#pragma link C++ function formc_(double *)+; 
//#pragma link C++ function formm_(double *)+; 
//
//#pragma link C++ function inif1f209_()+;
//#pragma link C++ function emc_09_(float * ,float * ,int *,float *)+;
//#pragma link C++ function cross_tot_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function cross_qe_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function f1f2in09_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function cross_tot_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function cross_qe_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function f1f2in09_mod_(double * ,double * ,double *,double * ,double * ,double * ,double * ,double * )+;
//
//#pragma link C++ function EMC_Effect(double * ,double *)+;
//
//#pragma link C++ function qfs_(double*,double*,double*,double*,double*, double*,double*, double*)+;
//#pragma link C++ function qfs_born_(double*,double*,double*,double*,double*,double*,double*,double*, double*,double*, double*,int*)+;
//#pragma link C++ function qfs_radiated_(double*,double*,double*,double*,double*,double*,double*,double*)+;
//#pragma link C++ function wiser_sub_(double * ,int * ,double * ,double * , double * )+;
//#pragma link C++ function wiser_all_sig_(double * ,double * ,double * ,double * , int *  ,double * )+;
//#pragma link C++ function wiser_all_sig0_(double * ,double * ,double * ,double * , int *  ,double * )+;
//#pragma link C++ function wiser_fit_(int * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function vtp_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
//#pragma link C++ function aac08pdf_(double*,double*,int*,double*,double**)+;
//#pragma link C++ function dssvini_(int *)+;
//#pragma link C++ function dssvfit_(double *,double *,double *,double *,double *,double *,double *,double *)+;
//#pragma link C++ function partondf_(double *,double *,int *)+;
//#pragma link C++ function partonevol_(double *x, double *Q2, int *ipol, double * pdf, int * isingle, int *num)+;
//#pragma link C++ function mrst2002_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
//#pragma link C++ function mrst2001e_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
//#pragma link C++ function mrstpdfs_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
//#pragma link C++ function setctq6_(int*)+;
//#pragma link C++ function getctq6_(int* ,double*, double*, double*)+;
//#pragma link C++ function setct10_(int*)+; 
//#pragma link C++ function getct10_(int*,double*,double*,double*)+; 
//#pragma link C++ function getabkm09_(int*,double*,double*,int*,int*,int*,double*)+; 
//#pragma link C++ function getmstw08_(int*,int*,double*,double*,double*)+; 
//#pragma link C++ function ppdf_( int*, double*,double*,double*, double*,double*,double*, double*,double*,double*,double*,double*,double*,double*,double*)+;
//#pragma link C++ function ini_()+;
//#pragma link C++ function polfit_(int*,double*,double*,double*, double*, double*,double*,double*, double*,double*,double*)+;
//#pragma link C++ function lss2006init_()+;
//#pragma link C++ function lss2006_(int*,double*,double*, double*,double*,double*, double*,double*,double*,double*, double*, double*,double*,double*, double*,double*)+;
//#pragma link C++ function nloini_()+;
//#pragma link C++ function polnlo_(int* ,double*, double*, double*, double*, double*, double*, double*, double*)+;
//#pragma link C++ function epcv_single_(char* , int *, double *, double *,double*, double*, double*, double*)+;
//#pragma link C++ function epcv_single_v3_(char* , int *, double *, double *,double*, double*, double*, double*)+;
//#pragma link C++ function gpc_single_v3_(char* , int *, double *, double *,double*, double*, double*, double*)+;
//#pragma link C++ function fermi3_(double* , int *, double *)+;
//
////#pragma link C++ function sane_pol_(double * ,double * ,double * ,int * ,int * ,double * )+;
//
//#pragma link C++ class GSPolarizedPDFs+;
//#pragma link C++ class CTEQ6UnpolarizedPDFs+;
//#pragma link C++ class CTEQ10UnpolarizedPDFs+;
//#pragma link C++ class CJ12UnpolarizedPDFs+; 
//#pragma link C++ class ABKM09UnpolarizedPDFs+; 
//#pragma link C++ class MSTW08UnpolarizedPDFs+; 
//#pragma link C++ class MRST2001UnpolarizedPDFs+; 
//#pragma link C++ class MRST2002UnpolarizedPDFs+; 
//#pragma link C++ class MRST2006UnpolarizedPDFs+; 
//
//#pragma link C++ class CTEQ6eInclusiveDiffXSec+;
//#pragma link C++ class F1F209eInclusiveDiffXSec+;
//#pragma link C++ class F1F209QuasiElasticDiffXSec+;
//#pragma link C++ class InSANEPolIncDiffXSec+;
//// quasi elastic 
//#pragma link C++ class QuasiElasticInclusiveDiffXSec+; 
//#pragma link C++ class QEIntegral+; 
//#pragma link C++ class QEFuncWrap+; 
//
//#pragma link C++ class QFSInclusiveDiffXSec+;
//#pragma link C++ class QFSXSecConfiguration+;
//
//#pragma link C++ class InSANECrossSectionDifference+;
//
//#pragma link C++ class InSANEPolarizedDiffXSec+;
//#pragma link C++ class PolarizedDISXSec+;
//#pragma link C++ class PolarizedDISXSecParallelHelicity+;
//#pragma link C++ class PolarizedDISXSecAntiParallelHelicity+;
//
//#pragma link C++ class InSANEInclusiveWiserXSec+;
//#pragma link C++ class WiserInclusivePhotoXSec+;
//#pragma link C++ class WiserInclusiveElectroXSec+;
//#pragma link C++ class ElectroWiserDiffXSec+;
//#pragma link C++ class PhotoWiserDiffXSec+;
//
//
//#pragma link C++ class InSANEInclusiveEPCVXSec+;
//#pragma link C++ class InSANEInclusiveEPCVXSec2+;
//
//
////#pragma link C++ class InSANEKinematicCoefficient+;
////#pragma link C++ class InSANEComptonAsymmetry+;
//
//#pragma link C++ class InSANEFunctionManager+;
//#pragma link C++ global fgInSANEFunctionManager+;
//
//// Base is not used.
////#pragma link C++ class InSANERadiatorBase<InSANEInclusiveDiffXSec>+;
////#pragma link C++ class InSANERadiatorBase<F1F209eInclusiveDiffXSec>+;
//
//// Add all cross sections that can be radiated
//#pragma link C++ class InSANERadiator<InSANEInclusiveDiffXSec>+;
//#pragma link C++ class InSANERadiator<InSANEInclusiveBornDISXSec>+;
//#pragma link C++ class InSANERadiator<InSANECompositeDiffXSec>+;
//#pragma link C++ class InSANERadiator<F1F209eInclusiveDiffXSec>+;
//#pragma link C++ class InSANERadiator<F1F209QuasiElasticDiffXSec>+;
//#pragma link C++ class InSANERadiator<CTEQ6eInclusiveDiffXSec>+;
//#pragma link C++ class InSANERadiator<InSANEPOLRADBornDiffXSec>+;
//#pragma link C++ class InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>+;
//#pragma link C++ class InSANERadiator<F1F209QuasiElasticDiffXSec>+;
//#pragma link C++ class InSANERadiator<QuasiElasticInclusiveDiffXSec>+;
//#pragma link C++ class InSANERadiator<QFSInclusiveDiffXSec>+;
//
//#pragma link C++ class InSANEFermiMomentumDist+;

#endif

