#ifndef BETAEventDisplay_HH
#define BETAEventDisplay_HH

#include "InSANEEventDisplay.h"

/**
 *
 *  \ingroup BETA
 */
class BETAEventDisplay : public InSANEEventDisplay {
public:

   TObjArray * fLuciteHists;
   TObjArray * fCherenkovHists;
   TObjArray * fBigcalHists;

   BETAEventDisplay() {
      fUpdateTriggerBit = 2; // pi0trig
      fEventType = 5; //b2trig,b1trig, and pi0trig

      fLuciteHists  = new TObjArray(0);
      fCherenkovHists  = new TObjArray(0);
      fBigcalHists  = new TObjArray(0);

   };

   virtual  ~BETAEventDisplay() {
   };

   /** Called from InSANEAnalyzer */
   virtual void UpdateDisplay();

   ClassDef(BETAEventDisplay, 1)
};

#endif
