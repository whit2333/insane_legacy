#ifndef InSANEToroidalMirror_h
#define InSANEToroidalMirror_h

#include "InSANECherenkovMirror.h"

/** Concrete class for a toroidal mirror
 *
 *  \ingroup detComponents
 */
class InSANEToroidalMirror : public InSANECherenkovMirror  {

public :

   InSANEToroidalMirror(Int_t num = 0) : InSANECherenkovMirror(num) { }
   virtual ~InSANEToroidalMirror() { }

   Double_t GetVerticalSize() {
      return (36.5/*cm*/);
   };
   Double_t GetHorizontalSize() {
      return (43.0/*cm*/);
   };


   ClassDef(InSANEToroidalMirror, 1)
};


#endif
