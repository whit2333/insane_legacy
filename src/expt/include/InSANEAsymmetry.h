#ifndef InSANEAsymmetry_H
#define InSANEAsymmetry_H 1

#include <iostream>
#include <vector>
#include <array>

#include "TNamed.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "InSANEDilutionFactor.h"
#include "InSANERunManager.h"
#include "TDirectory.h"
#include "InSANEDISEvent.h"
#include "InSANEDilutionFactor.h"
#include "TH3F.h"
#include "TParameter.h"
#include "InSANERun.h"

/** A simple extension of TParameter for cuts.
 *  Not yet used.
 */
class InSANEExpParameter: public TParameter<double> {
   protected :
      Int_t    fGroup;
      Double_t fMinimum;
      Double_t fMaximum;
      TString  fTreeVariable;
      TString  fParName;

   public :
      InSANEExpParameter(const char * n = "") ;
      virtual ~InSANEExpParameter();
      const char * GetName() const { return(fParName.Data()); }
      const char * PrintTable();
      const char * PrintTableHead();
      const char * GetCut();
      ClassDef(InSANEExpParameter, 1)
};



/**  Asymmetry measurment base class.
 *  
 *  The helicity bit should actually be set to an Int_t and
 *  is either 1 or -1, corresponding to Positive and negative or
 *  array element 0 and 1.
 *
 *
 *  \ingroup asymmetries
 */
class InSANEAsymmetry : public TNamed {
   protected:
      Int_t      fNAdded         = 0;

   public:
      Int_t              fNumber = 0;
      Int_t              fCount[2]; //->
      Double_t           fValue[2]; //->
      Int_t              fGroup  = 0; // Single group number, group=0 includes everything
      std::vector<Int_t> fGroups;   // List of group numbers.

      Bool_t IsInGroupList(const std::vector<int>& groups) const ;
      Bool_t IsInGroup(Int_t n) const ;

      InSANEDISEvent * fDISEvent = nullptr; //! Transient

   public:
      InSANEAsymmetry(Int_t n = 0);
      virtual ~InSANEAsymmetry(){ }
      InSANEAsymmetry(const InSANEAsymmetry& rhs);
      InSANEAsymmetry(InSANEAsymmetry&&) = default;

    //base_of_five_defaults(const base_of_five_defaults&) = default;
    //base_of_five_defaults(base_of_five_defaults&&) = default;
    //base_of_five_defaults& operator=(const base_of_five_defaults&) = default;
    //base_of_five_defaults& operator=(base_of_five_defaults&&) = default;
    //virtual ~base_of_five_defaults() = default;

      InSANEAsymmetry&       operator=(const InSANEAsymmetry &rhs);
      InSANEAsymmetry&       operator=(InSANEAsymmetry&&) = default;
      InSANEAsymmetry&       operator+=(const InSANEAsymmetry &rhs) ;
      const InSANEAsymmetry  operator+(const InSANEAsymmetry &other) const ;

      virtual Bool_t IsGoodEvent() { return true; }

      virtual Int_t  CountEvent();
      virtual Int_t  CountEvent(const std::vector<int> & groups);

      Bool_t         IsFolder() const { return kTRUE;}
      void           Browse(TBrowser* b) {
         //b->Add(&fAsymmetries);
      }

      Int_t   GetTotalCount() const { return fCount[0] + fCount[1] ; }

      void Print(Option_t * opt = "") const ;  // *MENU* 

      virtual void Initialize(){;}

      virtual void PrintTable() const { std::cout << " \n" ; }

      /**  Gets the statistical error for a simple asymmetry, \f$ A=\frac{N_1-N_2}{N_1+N_2} \f$.
       *   The error follows the formula \f$ \delta A = 2 \sqrt{\frac{N_1 N_2}{(N_1+N_2)^3}} \f$.
       */
      Double_t       GetStatisticalError() const {
         return(2.0 * TMath::Sqrt(((double)(fCount[0] * fCount[1])) / TMath::Power((double)(fCount[0] + fCount[1]), 3)));
      }

      /**  Gets the statistical error for a simple asymmetry, \f$ A=\frac{N_1-N_2}{N_1+N_2} \f$.
       *   The error follows the formula \f$ \delta A = 2 \sqrt{\frac{N_1 N_2}{(N_1+N_2)^3}} \f$.
       */
      Double_t       GetStatisticalError(Double_t n1, Double_t n2) const {
         return(2.0 * TMath::Sqrt(((n1 * n2)) / TMath::Power((n1 + n2), 3)));
      }

      virtual Double_t GetSystematicError(Double_t N1, Double_t N2) const {
         return(0);
      }

      virtual Double_t GetTotalError(Double_t N1, Double_t N2) const {
         return(0);
      }

      Double_t GetRawAsymmetry() const {
         return(((double)(fCount[0] - fCount[1])) / ((double)(fCount[0] + fCount[1])));
      }

      void             SetDISEvent(InSANEDISEvent * ev){ fDISEvent = ev;}
      InSANEDISEvent * GetDISEvent(){ return fDISEvent ;}

      ClassDef(InSANEAsymmetry,14)
};

#endif

