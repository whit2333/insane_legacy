#ifndef SANEPi0PassAnalyzer_H
#define SANEPi0PassAnalyzer_H 1
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "InSANECalculation.h"

#include "InSANEAnalyzer.h"
#include "SANEClusteringAnalysis.h"
#include "TString.h"
#include "SANERunManager.h"



/** Second Pass Analyzer class
 *
 *  \ingroup Analyzers
 */
class SANEPi0PassAnalyzer :  public SANEClusteringAnalysis , public InSANEAnalyzer  {
public:

   /** Argument should be the name of the new tree to create.  */
   SANEPi0PassAnalyzer(const char * newTreeName = "correctedBetaDetectors", const char * uncorrectedTreeName = "betaDetectors") ;
   virtual ~SANEPi0PassAnalyzer() ;

   virtual void Initialize(TTree * t = nullptr);

   /**  Needed to clear event from InSANEAnalyzer::Process() or InSANECorrector::Process(). */
   void ClearEvents() {
      fEvents->ClearEvent();
   }

   /**  Fill Trees then clear all event data.  */
   virtual void FillTrees() {
      if (fCalculatedTree)fCalculatedTree->Fill();
   }

   virtual void MakePlots();

   ClassDef(SANEPi0PassAnalyzer, 1)
};


#endif

