#ifndef InSANEBeamSkimmer_HH
#define InSANEBeamSkimmer_HH

#include <iostream>
#include "TNamed.h"
#include "TChain.h"
#include "TList.h"

#include "InSANESkimmerFilter.h"
#include "InSANEScalerEvent.h"
#include "InSANERunManager.h"

/** Data skimmer used to raise flags,and filter the data etc.
 *
 *  The skimmer has filters attached to it. Then each filter has flag(s) associated with it.
 */
class InSANESlowSkimmer : public TNamed {

   protected:
      TList               fFilterList;
      Int_t               fNumberOfFilters;
      TTree             * fInputTree;
      InSANEScalerEvent * fScalerEvent;
      Bool_t              fIsFlagged;

   public:
      InSANESlowSkimmer(const char * n="",const char* t="");
      virtual ~InSANESlowSkimmer();

      void                AddFilter(InSANESkimmerFilter * filter);
      Bool_t              PassesFilter();
      void                SetScalerEvent(InSANEScalerEvent * ev);
      InSANEScalerEvent * GetScalerEvent();
      void                Initialize();
      void                ProcessFlags();

      /** Returns 0 if events are good and -1 or less if they should be skipped.*/
      int                 ProcessFilters();

      /** Used inside event loop with detectors to test if data falls inside of
       *  something like a beam trip.
       */
      Bool_t              IsFlagged() const ;

      ClassDef(InSANESlowSkimmer,2)
};

#endif

