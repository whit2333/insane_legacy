#ifndef InSANEAveragedKinematics_HH
#define InSANEAveragedKinematics_HH 1
#include "TNamed.h"
#include "InSANEFunctionManager.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "InSANEDISEvent.h"
#include "TBrowser.h"
#include "InSANEMathFunc.h"
#include "Pi0ClusterPair.h"


class InSANESingleBinVariables {
  public:
    double fN      = 0.0;
    double fx      = 0.0;
    double fW      = 0.0;
    double fQ2     = 0.0;
    double fNu     = 0.0;
    double fE      = 0.0;
    double fTheta  = 0.0;
    double fPhi    = 0.0;
    double fD      = 0.0;
    double fR      = 0.0;
    double fF1     = 0.0;
    double fChi    = 0.0;
    double fEta    = 0.0;
    double fXi     = 0.0;
    double fx_2    = 0.0; 
    double fW_2    = 0.0; 
    double fQ2_2   = 0.0; 
    double fNu_2   = 0.0; 
    double fE_2    = 0.0; 
    double fTheta_2= 0.0; 
    double fPhi_2  = 0.0; 
    double fD_2    = 0.0; 
    double fR_2    = 0.0; 
    double fF1_2   = 0.0; 
    double fChi_2  = 0.0; 
    double fEta_2  = 0.0; 
    double fXi_2   = 0.0; 

  public:
    InSANESingleBinVariables(){}
    virtual ~InSANESingleBinVariables(){}
    InSANESingleBinVariables(const InSANESingleBinVariables&)            = default ;
    InSANESingleBinVariables& operator=(const InSANESingleBinVariables&) = default ;

    InSANESingleBinVariables& operator+=(const InSANESingleBinVariables& rhs);
    InSANESingleBinVariables operator+(const InSANESingleBinVariables& rhs);

    InSANESingleBinVariables& operator*=(double );
    InSANESingleBinVariables& operator/=(double );
    InSANESingleBinVariables operator/(double );


    ClassDef(InSANESingleBinVariables,1)
};

/** Class for average kinematic variable or kinematic coefficient.
 *   
 *  While adding events, fx keeps a running sum of x and fx_2 keeps
 *  a running sum of x^2.
 *  After the event loop is done, Calculate() does the following:
 *    - divides fx and fx_2 by fN to get their averaged values. 
 *    - Using CalculateErrors() the errors on fx are set to the 
 *        square root of the variance, sigma.
 *    - fx_2 is then set to 1/(sigma^2), which is later used in
 *        adding other averaged kinematics (see operator+=).
 *
 *  After calling Calculate(), no more events can then be added.
 *  However, other instances can be added using the operator+=(),
 *  which calculates the weighted mean and sets the error to the 
 *  square root of the variance of the mean. fx_2 keeps the running
 *  sum of 1/(sigma^2)
 *    
 *  \TODO Add protection so that Calculate() can only be called once.
 */
class InSANEAveragedKinematics1D : public TNamed {
   private: 
      Bool_t                     fCalculated; 
      InSANEStructureFunctions * fSFs         = nullptr; //!

   public:

      //Int_t  fNAdded; ///< Number of kinematics added together. 
      //                ///< Used to get the average variance instead of the variance of the mean. 
      TH1F fN    ;
      TH1F fx    ;
      TH1F fW    ;
      TH1F fQ2   ;
      TH1F fNu   ;
      TH1F fE    ;
      TH1F fTheta;
      TH1F fPhi  ;
      TH1F fD    ;
      TH1F fR    ;
      TH1F fF1   ;
      TH1F fChi  ;
      TH1F fEta  ;
      TH1F fXi   ;

      TH1F fx_2    ; 
      TH1F fW_2    ; 
      TH1F fQ2_2   ; 
      TH1F fNu_2   ; 
      TH1F fE_2    ; 
      TH1F fTheta_2; 
      TH1F fPhi_2  ; 
      TH1F fD_2    ; 
      TH1F fR_2    ; 
      TH1F fF1_2   ; 
      TH1F fChi_2  ; 
      TH1F fEta_2  ; 
      TH1F fXi_2   ; 

   public:
      InSANEAveragedKinematics1D() : InSANEAveragedKinematics1D("avgkine","avgkine") { }
      InSANEAveragedKinematics1D(const char * n,const char * t="avgkine");
      InSANEAveragedKinematics1D(const InSANEAveragedKinematics1D&)            ;
      InSANEAveragedKinematics1D& operator=(const InSANEAveragedKinematics1D&) ;
      //InSANEAveragedKinematics1D(InSANEAveragedKinematics1D&&)                 = default;
      //InSANEAveragedKinematics1D& operator=(InSANEAveragedKinematics1D&&)      = default;
      virtual ~InSANEAveragedKinematics1D();

      //InSANEAveragedKinematics1D(const InSANEAveragedKinematics1D& rhs);
      //InSANEAveragedKinematics1D&  operator=(const InSANEAveragedKinematics1D &rhs);
      InSANEAveragedKinematics1D&  operator+=(const InSANEAveragedKinematics1D &rhs);
      const InSANEAveragedKinematics1D  operator+(const InSANEAveragedKinematics1D &other);

      InSANESingleBinVariables GetSingleBin(int bin) const;
      void SetSingleBin(int bin, const InSANESingleBinVariables& vars) ;

      /** Calculates the weighted mean where the s histograms store 1/sigma^2.
       *  The first histograms a1 and a2  are the modified result. This is used in
       *  operator+=().
       */
      void CalcWeightedMean(TH1F* a1, TH1F * s1, const TH1F * a2, const TH1F * s2);
      void CalcAverage(TH1F& a1, const TH1F& n1, const TH1F& a2, const TH1F& n2);

      Bool_t         IsFolder() const { return kTRUE; }
      void           Browse(TBrowser* b);

      Int_t  CreateHistograms(const TH1F& h0);                  ///< Uses the supplied histogram to create the binning. 
      Int_t  AddEvent(Double_t xval, InSANEDISEvent * ev); 
      Int_t  CalculateErrors(TH1 *h0, TH1* h1);            ///< Sets the bin error to std deviation
      Int_t  Calculate();
      //Bool_t IsCalculated(){ return fIsCalculated; }
      //void   Reset();                                      ///< Resets all histograms.



   ClassDef(InSANEAveragedKinematics1D,6)
};


/** 2D versions of InSANEAveragedKinematics
 */
class InSANEAveragedKinematics2D : public TNamed {

   private: 
      InSANEStructureFunctions * fSFs; //!
      Bool_t                     fCalculated; 

   public:
      TH2F * fN    ; //-> 
      TH2F * fx    ; //-> 
      TH2F * fW    ; //-> 
      TH2F * fQ2   ; //-> 
      TH2F * fNu   ; //-> 
      TH2F * fE    ; //-> 
      TH2F * fTheta; //-> 
      TH2F * fPhi  ; //-> 
      TH2F * fD    ; //-> 
      TH2F * fR    ; //-> 
      TH2F * fF1   ; //-> 
      TH2F * fChi  ; //-> 
      TH2F * fEta  ; //-> 
      TH2F * fXi   ; //-> 
      TH2F * fx_2    ; //-> 
      TH2F * fW_2    ; //-> 
      TH2F * fQ2_2   ; //-> 
      TH2F * fNu_2   ; //-> 
      TH2F * fE_2    ; //-> 
      TH2F * fTheta_2; //-> 
      TH2F * fPhi_2  ; //-> 
      TH2F * fD_2    ; //-> 
      TH2F * fR_2    ; //-> 
      TH2F * fF1_2   ; //-> 
      TH2F * fChi_2  ; //-> 
      TH2F * fEta_2  ; //-> 
      TH2F * fXi_2   ; //-> 

   public:
      InSANEAveragedKinematics2D(const char * n="avgkine",const char * t="avgkine");
      virtual ~InSANEAveragedKinematics2D();
      InSANEAveragedKinematics2D(const InSANEAveragedKinematics2D& rhs);
      InSANEAveragedKinematics2D&       operator=(const InSANEAveragedKinematics2D &rhs);
      InSANEAveragedKinematics2D&       operator+=(const InSANEAveragedKinematics2D &rhs);
      const InSANEAveragedKinematics2D  operator+(const InSANEAveragedKinematics2D &other);

      /** Calculates the weighted mean where the s histograms store 1/sigma^2.
       *  The first histograms a1 and a2  are the modified result.
       */
      void CalcWeightedMean(TH2F * a1, TH2F * s1, TH2F * a2, TH2F * s2);

      Bool_t         IsFolder() const { return kTRUE; }
      void           Browse(TBrowser* b);

      Int_t CreateHistograms(TH2F * h0);                  ///< Uses the supplied histogram to create the binning. 
      Int_t AddEvent(Double_t xval, Double_t yval,InSANEDISEvent * ev); 
      Int_t CalculateErrors(TH2 *h0, TH2* h1);            ///< Sets the bin error to std deviation
      Int_t Calculate();

   ClassDef(InSANEAveragedKinematics2D,4)
};


/** 3D versions of InSANEAveragedKinematics
 */
class InSANEAveragedKinematics3D : public TNamed {

   private: 
      InSANEStructureFunctions * fSFs; //!
      Bool_t                     fCalculated; 

   public:
      TH3F * fN    ; //-> 
      TH3F * fx    ; //-> 
      TH3F * fW    ; //-> 
      TH3F * fQ2   ; //-> 
      TH3F * fNu   ; //-> 
      TH3F * fE    ; //-> 
      TH3F * fTheta; //-> 
      TH3F * fPhi  ; //-> 
      TH3F * fD    ; //-> 
      TH3F * fR    ; //-> 
      TH3F * fF1   ; //-> 
      TH3F * fChi  ; //-> 
      TH3F * fEta  ; //-> 
      TH3F * fXi   ; //-> 
      TH3F * fx_2    ; //-> 
      TH3F * fW_2    ; //-> 
      TH3F * fQ2_2   ; //-> 
      TH3F * fNu_2   ; //-> 
      TH3F * fE_2    ; //-> 
      TH3F * fTheta_2; //-> 
      TH3F * fPhi_2  ; //-> 
      TH3F * fD_2    ; //-> 
      TH3F * fR_2    ; //-> 
      TH3F * fF1_2   ; //-> 
      TH3F * fChi_2  ; //-> 
      TH3F * fEta_2  ; //-> 
      TH3F * fXi_2   ; //-> 

   public:
      InSANEAveragedKinematics3D(const char * n="avgkine",const char * t="avgkine");
      ~InSANEAveragedKinematics3D();
      InSANEAveragedKinematics3D(const InSANEAveragedKinematics3D& rhs);
      InSANEAveragedKinematics3D&       operator=(const InSANEAveragedKinematics3D &rhs);
      InSANEAveragedKinematics3D&       operator+=(const InSANEAveragedKinematics3D &rhs);
      const InSANEAveragedKinematics3D  operator+(const InSANEAveragedKinematics3D &other);

      /** Calculates the weighted mean where the s histograms store 1/sigma^2.
       *  The first histograms a1 and a2  are the modified result.
       */
      void CalcWeightedMean(TH3F * a1, TH3F * s1, TH3F * a2, TH3F * s2);

      Bool_t         IsFolder() const { return kTRUE; }
      void           Browse(TBrowser* b);

      Int_t CreateHistograms(TH3F * h0);                  ///< Uses the supplied histogram to create the binning. 
      Int_t AddEvent(Double_t xval, Double_t yval, Double_t zval, InSANEDISEvent * ev); 
      Int_t CalculateErrors(TH3 *h0, TH3* h1);            ///< Sets the bin error to std deviation
      Int_t Calculate();

   ClassDef(InSANEAveragedKinematics3D,4)
};

class InSANEAveragedPi0Kinematics1D : public TNamed {
   private: 
      InSANEStructureFunctions * fSFs; //!
      Bool_t                     fCalculated; 

   public:
      //Int_t  fNAdded; ///< Number of kinematics added together. 
      //                ///< Used to get the average variance instead of the variance of the mean. 
      TH1F * fN    ; //-> 
      TH1F * fx    ; //-> 
      TH1F * fW    ; //-> 
      TH1F * fPt   ; //-> 
      TH1F * fNu   ; //-> 
      TH1F * fE    ; //-> 
      TH1F * fTheta; //-> 
      TH1F * fPhi  ; //-> 
      TH1F * fD    ; //-> 
      TH1F * fR    ; //-> 
      TH1F * fF1   ; //-> 
      TH1F * fChi  ; //-> 
      TH1F * fEta  ; //-> 
      TH1F * fXi   ; //-> 

      TH1F * fx_2    ; //-> 
      TH1F * fW_2    ; //-> 
      TH1F * fPt_2   ; //-> 
      TH1F * fNu_2   ; //-> 
      TH1F * fE_2    ; //-> 
      TH1F * fTheta_2; //-> 
      TH1F * fPhi_2  ; //-> 
      TH1F * fD_2    ; //-> 
      TH1F * fR_2    ; //-> 
      TH1F * fF1_2   ; //-> 
      TH1F * fChi_2  ; //-> 
      TH1F * fEta_2  ; //-> 
      TH1F * fXi_2   ; //-> 

   public:
      InSANEAveragedPi0Kinematics1D(const char * n="avgkine",const char * t="avgkine");
      ~InSANEAveragedPi0Kinematics1D();
      InSANEAveragedPi0Kinematics1D(const InSANEAveragedPi0Kinematics1D& rhs);
      InSANEAveragedPi0Kinematics1D&  operator=(const InSANEAveragedPi0Kinematics1D &rhs);
      InSANEAveragedPi0Kinematics1D&  operator+=(const InSANEAveragedPi0Kinematics1D &rhs);
      const InSANEAveragedPi0Kinematics1D  operator+(const InSANEAveragedPi0Kinematics1D &other);

      /** Calculates the weighted mean where the s histograms store 1/sigma^2.
       *  The first histograms a1 and a2  are the modified result. This is used in
       *  operator+=().
       */
      void CalcWeightedMean(TH1F * a1, TH1F * s1, const TH1F * a2, const TH1F * s2);
      void CalcAverage(TH1F * a1, const TH1F * n1, const TH1F * a2, const TH1F * n2);

      Bool_t         IsFolder() const { return kTRUE; }
      void           Browse(TBrowser* b);

      Int_t  CreateHistograms(TH1F * h0);                  ///< Uses the supplied histogram to create the binning. 
      Int_t  AddEvent(Double_t xval, Pi0ClusterPair * ev); 
      Int_t  CalculateErrors(TH1 *h0, TH1* h1);            ///< Sets the bin error to std deviation
      Int_t  Calculate();
      //Bool_t IsCalculated(){ return fIsCalculated; }
      //void   Reset();                                      ///< Resets all histograms.

   ClassDef(InSANEAveragedPi0Kinematics1D,5)
};
#endif

