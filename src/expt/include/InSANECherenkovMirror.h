#ifndef InSANECherenkovMirror_h
#define InSANECherenkovMirror_h
#include "TNamed.h"
#include "InSANEDISTrajectory.h"
#include "GasCherenkovGeometryCalculator.h"
#include "InSANEDetectorComponent.h"

/**  ABC class for a Mirror
 *
 *   Concrete class for a Detector: SANE gas cherenkov built by Temple University.
 *
 *  \ingroup detComponents
 */
class InSANECherenkovMirror : public InSANEDetectorComponent {

   public:
      GasCherenkovGeometryCalculator * fCherenkovGeoCalc; //!

      Double_t fSignal;
      Double_t fTiming;
      Int_t    fNHits;
      Bool_t   fIsHit;

      Int_t fMirrorNumber;
      Double_t fXPosition;
      Double_t fYPosition;
      Double_t fZPosition;

      Double_t fAlphaRotation;
      Double_t fBetaRotation;
      Double_t fGammaRotation;

   public :

      InSANECherenkovMirror(Int_t num = 0) : fMirrorNumber(num) {
         SetName(Form("CherenkovMirror%d", fMirrorNumber));
         fXPosition = 0.0;
         fYPosition = 0.0;
         fZPosition = 0.0;
         fCherenkovGeoCalc = GasCherenkovGeometryCalculator::GetCalculator();
      }

      virtual ~InSANECherenkovMirror() { }

      /** Clear the cherenkov mirrors */
      void Clear(Option_t * opt = "") {
         fNHits = 0;
         fSignal = 0.0;
         fTiming = 0.0;
         fIsHit = false;
      }

      /** Process a trajectory event */
      Int_t ProcessEvent(InSANEDISTrajectory * trajectory) {
         ///\todo fix this
         //    std::cout << "compare " << fCherenkovGeoCalc->GetPrimaryMirror(trajectory->fPoint0.X(), trajectory->fPoint0.Y()
         //              << " to " << fMirrorNumber << "\n";
         //      if(fCherenkovGeoCalc->GetPrimaryMirror(trajectory->fPoint0.X(), trajectory->fPoint0.Y()) == fMirrorNumber )
         fIsHit = true;
         return(0);
      }

      /** returns true if the detector has a good hit. */
      Bool_t   HasHit() { return(fIsHit); }

      ClassDef(InSANECherenkovMirror, 1)
};



#endif

