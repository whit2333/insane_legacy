#ifndef InSANECalorimeterCluster_h
#define InSANECalorimeterCluster_h

#include "TObject.h"
#include "TH2F.h"
#include "InSANECluster.h"

/**
 *  \ingroup Clusters
 */

/**
 * \brief ABC for a general cluster in a segmented calorimeter
 */

/**
 *  ABC for calorimeter cluster such as bigcal (RCS and Protvino calorimeters)
 *  or possibly the showers of BigBite etc.
 */
class InSANECalorimeterCluster : public InSANECluster {

   public:
      Double_t    fHighestEnergy;         /// Energy of highest energy block in cluster
      Double_t    fSecondHighestEnergy;   /// Energy of second-highest energy block in cluster
      Double_t    fThirdHighestEnergy;    /// Energy of third-highest energy block in cluster
      Bool_t      fIsNoisyChannel;
      Int_t       fNNonZeroBlocks;        /// Number of non zero blocks in the cluster
      Int_t       fiPeak;                 ///< Cluster central block i coordinate, in Bigcal coords.
      Int_t       fjPeak;                 ///< Cluster central block j coordinate, in Bigcal coords.
      Double_t    fTotalE;
      Double_t    fBlockEnergies[5][5];   /// 5x5 Array of block energies

      //Int_t fNNonZeroTimedBlocks; /// Number of non zero blocks with a good TDC hit

   public:
      InSANECalorimeterCluster(Int_t clustNum = -1) ;
      virtual ~InSANECalorimeterCluster();

      InSANECalorimeterCluster& operator=(const InSANECalorimeterCluster &rhs);

      InSANECalorimeterCluster(const InSANECalorimeterCluster& rhs) : InSANECluster(rhs) {
         (*this) = rhs;
      }

      /** Set the cluster peak location before calculating the quantities. */
      void SetClusterPeak(Int_t i, Int_t j) {
         fiPeak = i;
         fjPeak = j;
      }

      Bool_t  IsEqual(const TObject *obj) const { return fTotalE == ((InSANECalorimeterCluster*)obj)->fTotalE; }
      Bool_t  IsSortable() const { return kTRUE; }
      Int_t   Compare(const TObject *obj) const { if (fTotalE > ((InSANECalorimeterCluster*)obj)->fTotalE)
         return 1;
         else if (fTotalE < ((InSANECalorimeterCluster*)obj)->fTotalE)
            return -1;
         else
            return 0;
      }

      void ClearBlocks();

      virtual void Clear(Option_t *opt = "");

      ClassDef(InSANECalorimeterCluster,10)
};

#endif



