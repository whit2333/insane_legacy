#ifndef CleanedBETAEvent_HH
#define CleanedBETAEvent_HH 1

#include "InSANEDetectorEvent.h"
#include "BIGCALCluster.h"
#include "SANEEvents.h"
#include "TVector3.h"

/**  All Events clusters, hits etc, with cuts
 *   Merged into one class.
 *
 *   \ingroup Events
 */
class CleanedBETAEvent : public InSANEDetectorEvent {
   public:
      CleanedBETAEvent() { }
      ~CleanedBETAEvent() { }

      BIGCALCluster fCluster;

      TVector3 fTrackerPosition;
      TVector3 fLucitePosition;
      TVector3 fBigcalPosition;

      Double_t fCherenkovNPE;
      Double_t fBigcalEnergy;
      Int_t fNPossibleClusters;
      Double_t fCherenkovHodoscopeTimeSum;

      virtual void ClearEvent(const char * opt = ""){
         fCluster.Clear();
         fTrackerPosition.SetXYZ(0, 0, 0);
         fLucitePosition.SetXYZ(0, 0, 0);
         fBigcalPosition.SetXYZ(0, 0, 0);
         fCherenkovNPE = 0.0;
         fBigcalEnergy = 0.0;
         fNPossibleClusters = 0;
      }

      ClassDef(CleanedBETAEvent, 1)
};

#endif

