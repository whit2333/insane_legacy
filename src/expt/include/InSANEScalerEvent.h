#ifndef InSANEScalerEvent_h
#define InSANEScalerEvent_h
#include <TROOT.h>

#include <TObject.h>
#include "InSANEEvent.h"

/** Scaler Event Base class
 *
 * Class for Scaler Event
 * Note it is probably best to inherit this into some other concrete class
 * where the constructor of the new class sets the addresses of pointers
 * to their corresponding fgScalers
 *
 * \ingroup Events
 *  \ingroup Scalers
 */
class InSANEScalerEvent : public InSANEEvent  {

   public:
      Double_t      fDeadTime;           // Total dead time
      Double_t      fLiveTime;           // Total live time (1- deadtime)
      Double_t      fDeltaT;             // Time since last scaler (seconds)
      Double_t      fTime;               // Time since the start of the run (seconds)
      Double_t      fDeltaQ;             // Accumulated charge since last scaler
      Double_t      fDeltaQplus;             // Accumulated charge since last scaler
      Double_t      fDeltaQminus;             // Accumulated charge since last scaler
      Double_t      fTotalQ;             // Accumulated charge since the start of the run
      Double_t      fTotalQplus;         // Positive helicity gated accumulated charge since the begining of the run
      Double_t      fTotalQminus;        // Neagitve helicity gated accumulated charge since the begining of the run
      Double_t      fBCM1;               // Beam current monitor1 (nA)
      Double_t      fBCM2;               // Beam current monitor2 (nA)
      Double_t      fBeamCurrentAverage; // Average of BCM1 and BCM2
      Int_t         fNumberOfScalers;       // Size of raw scaler array
      Long64_t *    fScalers;          //[fNumberOfScalers] The raw scaler array

   public :
      InSANEScalerEvent() ;
      virtual ~InSANEScalerEvent();

      virtual void      ClearEvent(Option_t * opt = "C");
      virtual void      ClearCounts() {  }
      virtual Double_t  GetBeamCurrent() const { return fBeamCurrentAverage; }
      void              SetNumberOfScalers(Int_t num);

      ClassDef(InSANEScalerEvent, 4)
};

#endif

