#ifndef ForwardTrackerGeometryCalculator_h
#define ForwardTrackerGeometryCalculator_h

#ifndef NTRACKERYMODULES
#define NTRACKERYMODULES 16
#endif

#ifndef NTRACKERXMODULES
#define NTRACKERXMODULES 8
#endif

#include <TROOT.h>
 //#include <TChain.h
#include <TFile.h>
#include <TObject.h>
#include "InSANEGeometryCalculator.h"
#include "TVector3.h"
#include "TRotation.h"
#include <iostream>
#include <iomanip>
#include "TMath.h"

/** Concrete class for SANE Forward Tracker geometry information
 *
 * [trackerthickness]: http://quarks.temple.edu/~whit/SANE/analysis_main/images/InSANE/tracker/Tracker_Thickness.png "Tracker thickness as seen by a scattered particle"
 * [trackerx]: http://quarks.temple.edu/~whit/SANE/analysis_main/images/InSANE/tracker/Tracker_Plane_X.png "Tracker X plane"
 * [trackery]: http://quarks.temple.edu/~whit/SANE/analysis_main/images/InSANE/tracker/Tracker_Plane_Y_All.png "Tracker Y planes"
 *
 * #### Description
 *
 *  The Forward Tracker is constructed of 3 planes of 3mm thick plastic scintllator.
 *  The most forward plane is the Y1 plane followed by the Y2 plane.
 *  Each of the Y planes has 128 vertically stacked scintillators. Each Y plane scint is 22 cm long.
 *  The last plane was the X plane which has 64 scintillaotrs. Each X plane scint is 40 cm long.
 *
 * #### Details
 *
 * - The Y1 layer was measured to be 43.905 cm tall. It is twice as thick as the other layers (6mm).
 *
 * - The Y2 layer was measured to be 43.644 cm tall. 
 *
 * - The X1 layer was measured to be 22.262 cm wide.
 *
 * - Each scintillator wrapped with one layer of Teflon (125 um thick).
 *
 * - Each scintillator w/ Teflon thickness = 3.250 mm
 *
 * - One layer of Tedlar (50 um thick) separator between scintillators.
 *
 * - Groups of eight scintillators (a module) held together with scotch tape and Tedlar.
 *
 * - 23.6 cm long Tedlar sheet covers each module.
 *
 * - Vertical edges of a module have two layers of Tedlar (100 um) on one edge 
 *   and three layers of Tedlar (150 um) on the other.
 *
 * - Vertical height of each module should be: 
 *   \code 
 *   [(3.250 + 0.05)x8 + 0.10 + 0.15] mm = 26.65 mm 
 *   \endcode
 *
 * - Given the number of scints and the area of the detector plane,
 *   there is anywere from 439.05/128 -3.0 = 0.34mm to  222.62/64 -3 = 0.48mm
 *   of extra space between each scint.
 *
 * - The WLS fiber runs the length of each scint and has a diameter of 1.2mm
 *   so this thickness has to be added when considering the z postion of a hit
 *
 *
 * #### Construction Measurements
 *
 * ##### Measured Average Heights for Y1 and Y2 Planes
 * 
 *  \code
 *   -----------------------------------------------------------
 *   Module # Counter #      Y1   Y1 Sum       Y2   Y2 Sum
 *                          (mm)   (mm)       (mm)   (mm)
 *   -----------------------------------------------------------
 *      01    001 - 008    26.35   26.35     26.52   26.52
 *      02    009 - 016    26.70   53.05     26.94   53.45
 *      03    017 - 024    26.85   79.90     26.99   80.44
 *      04    025 - 032    27.05  106.95     27.01  107.45
 *      05    033 - 040    27.20  134.15     27.07  134.52
 *      06    041 - 048    27.40  161.55     27.25  161.77
 *      07    049 - 056    27.45  189.00     27.33  189.10
 *      08    057 - 064    27.50  216.50     27.36  216.46
 *      09    065 - 072    27.50  244.00     27.40  243.86
 *      10    073 - 080    27.50  271.50     27.44  271.29
 *      11    081 - 088    27.75  299.25     27.44  298.74
 *      12    089 - 096    27.90  327.15     27.47  326.20
 *      13    097 - 104    27.90  355.05     27.48  353.68
 *      14    105 - 112    27.95  383.00     27.48  381.16
 *      15    113 - 120    27.95  410.95     27.63  408.79
 *      16    121 - 128    28.10  439.05     27.65  436.44
 *   -----------------------------------------------------------
 *   Total Height of 16 Modules:  Y1=439.05 mm      Y2=436.44 mm
 *   -----------------------------------------------------------
 *  \endcode
 *
 * ##### Measured Average Horizontal Widths for X Plane
 * 
 *  \code
 *   -----------------------------------------------------------
 *   Module # Counter #      X    X Sum
 *                          (mm)   (mm)
 *   -----------------------------------------------------------
 *    01      001 - 008    28.00   28.00
 *    02      009 - 016    28.00   56.00
 *    03      017 - 024    27.83   83.83
 *    04      025 - 032    27.53  111.37
 *    05      033 - 040    27.68  139.05
 *    06      041 - 048    27.67  166.72
 *    07      049 - 056    27.62  194.34
 *    08      057 - 064    28.28  222.62
 *   -----------------------------------------------------------
 *   Total Width of 8 Modules:  X=222.62 mm
 *   -----------------------------------------------------------
 * \endcode
 *
 * ####  Tracker Package Thickness
 *
 *  - Scintillator = 3.0   mm
 *
 *  - Fiber        = 1.2   mm x 2 =  2.4  mm
 *
 *  - Teflon       = 0.125 mm x 2 =  0.25 mm
 *
 *  - Tedlar       = 0.050 mm x 3 =  0.15 mm
 *
 *  - G10 frame    = 3.20  mm x 4 = 12.8  mm
 *
 *  - Scint + Teflon + Tedlar = 3.4 mm --> use 3.5 mm
 *
 *  - Two fibers (+ glue)     = 2.4 mm --> use 2.6 mm
 *
 *  - Scintillator-fiber module = 6.1 mm
 *
 *  - Y1 Plane = 9.6 mm
 *
 *  - Y2 Plane = 6.1 mm
 *
 *  - X  Plane = 6.1 mm
 *
 *  - All Planes = 21.8 mm --> use 22.0 mm
 *
 *  - All Frames = 12.8 mm --> use 13.0 mm
 *
 *  - Total Tracker Package Thickness (along z-axis) = 35 mm
 *
 * ![trackerthickness]
 *
 * #### Tracker Frame 
 *  - G10 Frame opening is 450 mm
 *
 *  - Center of 128 counters = 439.05 mm/2 = 219.53 mm.  
 *    This corresponds to counter # 65.
 *
 * ![trackerx]
 *
 * ![trackery]
 *    
 *
 * #### A note about number and language
 * 
 *  - The planes X1,Y1,Y2 have have layer numbers 0,1,2 respectively.
 *
 *  - Channel (1- #scints), unique in whole detector.
 *
 *  - Layer (0 = X1, 1=Y1, 2=Y2)
 *
 *  - Row or ScintNumber (1 - #scints in layer), unique for given layer.
 *
 *  - Width is measured in the direction that the scints are stacked to form a given layer (x or y).
 *
 *  - Thickness is measured along the z direction.
 *
 *  - Length is the longest side of the scint.
 *
 * \ingroup Geometry
 * \ingroup tracker
 */
class ForwardTrackerGeometryCalculator  : public InSANEGeometryCalculator {
private:

   Double_t fY1ModuleWidth[NTRACKERYMODULES];      /// (mm) Measured Width of each module
   Double_t fY1ModuleWidthSum[NTRACKERYMODULES];   /// (mm) Measured Width of all modules up to and including the indexed module
   Double_t fY1ModuleScintWidth[NTRACKERYMODULES]; /// (mm) calculated from each module's measured width.

   Double_t fY2ModuleWidth[NTRACKERYMODULES];      /// (mm) Measured Width of each module
   Double_t fY2ModuleWidthSum[NTRACKERYMODULES];   /// (mm) Measured Width of all modules up to and including the indexed module
   Double_t fY2ModuleScintWidth[NTRACKERYMODULES]; /// (mm) calculated from each module's measured width.

   Double_t fX1ModuleWidth[NTRACKERXMODULES];      /// (mm) Measured Width of each module
   Double_t fX1ModuleWidthSum[NTRACKERXMODULES];   /// (mm) Measured Width of all modules up to and including the indexed module
   Double_t fX1ModuleScintWidth[NTRACKERXMODULES]; /// (mm) calculated from each module's measured width.

   TVector3 fTrackerBBoxOrigin;    /// cm, in BETA coords
   TVector3 fTrackerBBoxDim;       /// cm, Bounding box dimensions in cm. 
   TVector3 fTrackerDetectorBox;   /// cm, Tracker box when placed and rotated fits entirely in the orthogonal coordinates of the bounding box. 

   TVector3 fY1BBoxDim;       /// cm, Bounding box dimensions in cm. 
   TVector3 fY2BBoxDim;       /// cm, Bounding box dimensions in cm. 
   TVector3 fX1BBoxDim;       /// cm, Bounding box dimensions in cm. 

   TRotation fDetectorRotation; /// Rotation matrix in BETA coordinates

   TVector3 fTrackerActiveBBoxDim; /// cm, Bounding box around active area



   mutable TVector3 fScintZPos;

public :
   static  ForwardTrackerGeometryCalculator * GetCalculator();
   ~ForwardTrackerGeometryCalculator();

   /** Returns the module number from the row number. Note this is not the channel number.
    *  The module number starts at 0 and goes up to 15 for the Y planes and up to 7 for the X plane.
    */
   Int_t GetModuleNumber(Int_t row) const ;

   /** Returns the central position of the scintillator.
    *  Used with Width sums to get position of scint in a given plane.
    */
   Double_t GetScintPositionInLayer(Int_t chan) const ;

   /** Returns the central position of the scintillator.
    *  Used with Width sums to get position of scint in a given plane.
    */
   Double_t GetScintPositionInLayer(Int_t layer,Int_t row) const ;

   /** Returns the layer number from the channel number.
    *  The channel number starts at 1.
    *  layer = 0 is the front X layer, while 1-2 are the next Y1 and Y2 layers.
    */
   Int_t GetLayerNumber(Int_t chan) const ;

   /** Returns the scint number from the channel number. 
    */
   Int_t GetScintNumber(Int_t chan) const ;
   Int_t GetScintRowNumber(Int_t chan) const {return(GetScintNumber(chan)) ;}
   Int_t GetRowNumber(Int_t chan) const {return(GetScintNumber(chan)) ;}

   /** Returns the number of the scint
    *  layer = 0 is the front X layer, while 1-2 are the next Y1 and Y2 layers.
    */
   Int_t GetScintNumber(Int_t layer, Int_t row) const  ;

   /** Returns the cental position (in BETA coordinates) of the the scintillators
    *  with the channel number, chan. The central postion is the center of a box
    *  forming the scintialltor and used for positioning in geometry construction.
    *  The channel number is a unique number for each of the tracker's scints.
    *  Note this method is slower.
    */
   TVector3 GetPositioning(Int_t chan) const {
      Int_t layer = GetLayerNumber(chan);
      Int_t row   = GetRowNumber(chan);
      TVector3 res(0,0,0);
      if( layer == 1 ) {  //Y1 layer
         res.SetXYZ(0.0,GetY1Position(row),GetZPosition(layer));
      } else if ( layer == 2) { //Y2 layer
         res.SetXYZ(0.0,GetY2Position(row),GetZPosition(layer));
      } else if ( layer == 0) { //X1 layer
         res.SetXYZ(GetX1Position(row),0.0,GetZPosition(layer));
      } else {
         Error("GetPositioning","Bad Channel Number");
      } 
      return(res);
   }
   
   /** Returns a vector of dimensions for a bounding box for a scintillator
    *  with the channel number, chan.
    */
   TVector3 GetBoundingBoxDimensions(Int_t chan) const {
      Int_t layer = GetLayerNumber(chan);
      //Int_t row   = GetRowNumber(chan);
      TVector3 res(0,0,0);
      if( layer == 1  ) {   // Y1 and Y2 layers are the same
         res.SetXYZ(  GetY1ScintLength() ,
                      0.3,//GetScintHeight(layer,row),
                      fY1TotalThickness);
      } else if( layer == 2 ) {   // Y1 and Y2 layers are the same
         res.SetXYZ(  GetY2ScintLength() ,
                      0.3,//GetScintHeight(layer,row),
                      fY2TotalThickness);
      } else if ( layer == 0) {          //X1 layer
         res.SetXYZ(  0.3,//GetScintHeight(layer,row) ,
                      GetX1ScintLength(),
                      fX1TotalThickness);
      } else{
         Error("GetBoundingBoxDimensions","Bad Channel Number");
      }
      return(res);
   }

   /** Returns the channel number which is in the range [1,fNScints]
    *  where fNScints = 64 + 128 + 128.
    */
   Int_t GetChannelNumber(Int_t layer, Int_t scintNum) const  ;

   /** Returns the distance from the target origin to the face of the detector.
    */
   Double_t GetDistanceFromTarget() const { return fDistanceFromTarget; }

   /** Returns the x position of the scint number.
    *  The X1 plane's scintillators are oriented vetically number
    */
   Double_t GetX1Position(Int_t scintNum) const ;

   /** Returns an additive correction to the x position given the argument y, which is the 
    *  vertical position.
    *  The correction is a rotation in bigcal coordinates (x,y). dy = y*Tan(x1roll).
    *  x' = x + dx(y). 
    */ 
   Double_t GetXPositionCorrection(Double_t y) const {
      return(-1.0*y*TMath::Tan(fX1Roll) );
   }
   /** Returns the y position of the scint number.
    *  The Y1 plane's scintillators are oriented horizontally.
    */
   Double_t GetY1Position(Int_t scintNum) const ;

   /** Returns the y position of the scint number.
    *  The Y2 plane's scintillators are oriented horizontally.
    */
   Double_t GetY2Position(Int_t scintNum) const ;

   /**  Returns the Z position for a given layer.
    *   The layer number can be 0,1, or 2. If anthing else it just returns GetDistancdFromTarget().
    *   If scintNum is provided, the rotation of the detector is considered in calculating the z position.
    *   A vector centered in the layer pointing to the scint Num is rotated and the projection along z is added to the result.
    *   In this way, the other coordinate is neglected.
    */
   Double_t GetZPosition(Int_t layer,Int_t scintNum = -1) const;

   /** Returns the Z position of a layer within the coordinates of the Bounding Box.
    *  Used in constructing the tracker geometry.
    */
   Double_t GetLayerZPlacement(Int_t layer) const {
      Double_t g10layer = fTotalFrameThickness/4.0;
      Double_t res = -fTotalThickness/2.0;
      if( layer == 1 ) {
          res = -fTotalThickness/2.0 + 2.0*g10layer + fX1TotalThickness + fY1TotalThickness/2.0;
      } else if( layer == 2 ) {
          res = -fTotalThickness/2.0 + 3.0*g10layer + fX1TotalThickness + fY1TotalThickness + fY2TotalThickness/2.0;
      } else if( layer == 0 ) {
          res = -fTotalThickness/2.0 + 1.0*g10layer + fX1TotalThickness/2.0;
      } 
      return(res);
   }

   /** Returns the Z position for the Tedlar frame pieces of which there 
    *  are 4 of (0,1,2,3), 0 being in the front.
    */
   Double_t GetFrameZPlacement(Int_t layer) const {
      Double_t g10layer = fTotalFrameThickness/4.0;
      Double_t res = -fTotalThickness/2.0;
      if( layer == 0 ) {
          res = -fTotalThickness/2.0 + 0.5*g10layer;
      } else if( layer == 1 ) {
          res = -fTotalThickness/2.0 + 1.5*g10layer + fX1TotalThickness;
      } else if( layer == 2 ) {
          res = -fTotalThickness/2.0 + 2.5*g10layer + fX1TotalThickness + fY1TotalThickness ;
      } else if( layer == 3 ) {
          res = -fTotalThickness/2.0 + 3.5*g10layer + fX1TotalThickness + fY1TotalThickness + fY1TotalThickness;
      }       return(res);
   }


   Double_t GetY1ScintLength() const { return( fXScintLength );}
   Double_t GetY2ScintLength() const { return( fXScintLength );}
   Double_t GetX1ScintLength() const { return( fYScintLength );}

   Double_t GetY1ScintThickness() const { return( fScintThickness*2.0 );}
   Double_t GetY2ScintThickness() const { return( fScintThickness );}
   Double_t GetX1ScintThickness() const { return( fScintThickness );}

   /** Returns the scintillator height for a given channel.
    *  For each module the heights are the same.
    */
   Double_t GetScintHeight(Int_t chan) const;

   /** Returns the scintillator height for a given channel.
    *  For each module the heights are the same.
    */
   Double_t GetScintHeight(Int_t layer, Int_t row) const;

   void   Print(Option_t* option = "") const {
      using namespace std;
      cout << "------------------------------------------------------\n";
      TObject::Print();
      Int_t namecol = 20;
      cout << setw(namecol) <<  "fDistanceFromTarget" << " " << setw(10) <<  fDistanceFromTarget << "\n";
      cout << setw(namecol) <<  "fYScintLength" << " " << setw(10) <<  fYScintLength << "\n";
      cout << setw(namecol) <<  "fXScintLength" << " " << setw(10) <<  fXScintLength << "\n";
      cout << setw(namecol) <<  "fScintSideLength" << " " << setw(10) <<  fScintSideLength << "\n";
      cout << setw(namecol) <<  "fScintTotalThickness"  << " "<< setw(10) <<  fScintTotalThickness << "\n";
      cout << setw(namecol) <<  "fScintThickness" << " " << setw(10) <<  fScintThickness << "\n";
      cout << setw(namecol) <<  "fScintWrapThickness" << " " << setw(10) <<  fScintWrapThickness << "\n";
      cout << setw(namecol) <<  "fWLSThickness" << " " << setw(10) <<  fWLSThickness << "\n";
      cout << setw(namecol) <<  "fScintWidth" << " " << setw(10) <<  fScintWidth << "\n";
      cout << setw(namecol) <<  "fScintTotalWidth" << " " << setw(10) <<  fScintTotalWidth << "\n";
      cout << setw(namecol) <<  "fLayerGap" << " " << setw(10) <<  fLayerGap << "\n";
      cout << setw(namecol) <<  "fThickness" << " " << setw(10) <<  fThickness << "\n";
      cout << setw(namecol) <<  "fSurveyYaw" << " " << setw(10) <<  fSurveyYaw << "\n";
      cout << setw(namecol) <<  "fSurveyPitch" << " " << setw(10) <<  fSurveyPitch << "\n";
      cout << setw(namecol) <<  "fSurveyRoll" << " " << setw(10) <<  fSurveyRoll << "\n";

      cout << setw(namecol) <<  "fNY1Scints" << " " << setw(10) <<  fNY1Scints << "\n";
      cout << setw(namecol) <<  "fNY2Scints" << " " << setw(10) <<  fNY2Scints << "\n";
      cout << setw(namecol) <<  "fNX1Scints" << " " << setw(10) <<  fNX1Scints << "\n";
      cout << setw(namecol) <<  "fNScints" << " " << setw(10) <<  fNScints << "\n";
      cout << setw(namecol) <<  "fNLayers" << " " << setw(10) <<  fNLayers << "\n";

      cout << setw(namecol) <<  "fY1TotalThickness" << " " << setw(10) <<  fY1TotalThickness << "\n";
      cout << setw(namecol) <<  "fY2TotalThickness" << " " << setw(10) <<  fY2TotalThickness << "\n";
      cout << setw(namecol) <<  "fX1TotalThickness" << " " << setw(10) <<  fX1TotalThickness << "\n";

      cout << setw(namecol) <<  "fTotalThickness" << " " << setw(10) <<  fTotalThickness << "\n";
      cout << setw(namecol) <<  "fTotalFrameThickness" << " " << setw(10) <<  fTotalFrameThickness << "\n";
      cout << setw(namecol) <<  "fTotalScintThickness" << " " << setw(10) <<  fTotalScintThickness << "\n";

      cout << setw(namecol) <<  "fX1HalfLength" << " " << setw(10) <<  fX1HalfLength << "\n";
      cout << setw(namecol) <<  "fY1HalfLength" << " " << setw(10) <<  fY1HalfLength << "\n";
      cout << setw(namecol) <<  "fY2HalfLength" << " " << setw(10) <<  fY2HalfLength << "\n";

      cout << setw(namecol) <<  "fX1Offset" << " " << setw(10) <<  fX1Offset << "\n";
      cout << setw(namecol) <<  "fY1Offset" << " " << setw(10) <<  fY1Offset << "\n";
      cout << setw(namecol) <<  "fY2Offset"  << " "<< setw(10) <<  fY2Offset << "\n";
      cout << "------------------------------------------------------\n";

   }

   Double_t fDistanceFromTarget;   /// cm, distance from target to the face of the detector.
   Double_t fYScintLength;         /// cm, lenth of vertical scints (X1)
   Double_t fXScintLength;         /// cm, lenth of horizontal scints (Y1 and Y2)
   Double_t fScintSideLength;      /// cm, length of just the scintillator (eg 3mm)
   Double_t fScintTotalThickness;  /// cm, Total material thicknes traversed along the z direction
   Double_t fScintThickness;       /// cm, scint material thickness along z
   Double_t fScintWrapThickness;   /// cm, thickness of wrapping material (s.t. stacked scints are separted by twice this)
   Double_t fWLSThickness;         /// cm, thickness of WLS (which is glued along length of scint)
   Double_t fScintWidth;
   Double_t fScintTotalWidth;
   Double_t fLayerGap;             /// cm, gap between layers.
   Double_t fThickness;            /// cm, total thickness of tracker 

   Double_t fSurveyYaw;   /// Survey report measured yaw radians
   Double_t fSurveyPitch; /// Survey report measured yaw radians
   Double_t fSurveyRoll;  /// Survey report measured yaw radians

   Double_t fX1Roll;      /// A correction to the X position taking into account the possiblitiy that the X layer is not exactly perpendicular to the Y layers

   Double_t GetYaw() const {return(fSurveyYaw);}
   Double_t GetPitch() const {return(fSurveyPitch);}
   Double_t GetRoll() const {return(fSurveyRoll);}

   Int_t fNY1Scints;
   Int_t fNY2Scints;
   Int_t fNX1Scints;
   Int_t fNScints;
   Int_t fNLayers;

   Double_t fG10FrameThickness;    /// cm, single frame thickness (there are 4 of these total)
   Double_t fFrameWindowWidth;
   Double_t fFrameOpeningWidth;
   Double_t fFrameWindowHeight;
   Double_t fFrameOpeningHeight;

   Double_t fY1TotalThickness;     /// cm, scint + fiber + wrapping
   Double_t fY2TotalThickness;     /// cm, scint + fiber + wrapping
   Double_t fX1TotalThickness;     /// cm, scint + fiber + wrapping

   Double_t fTotalThickness;       /// cm, total detector thickness 
   Double_t fTotalFrameThickness;  /// cm, total thickness due to frame material
   Double_t fTotalScintThickness;  /// cm, total thickness due to scint + wrap + fiber + glue + tedlar + ...

   Double_t fX1HalfLength;         /// cm, measured half length of plane
   Double_t fY1HalfLength;         /// cm, measured half length of plane
   Double_t fY2HalfLength;         /// cm, measured half length of plane

   Double_t fX1Offset;             /// cm, offset that is ADDED in BETA coords
   Double_t fY1Offset;             /// cm, offset that is ADDED 
   Double_t fY2Offset;             /// cm, offset that is ADDED

   Double_t fY1Y2SeparationCorrection;

   /** Returns a vector of the dimensions of a bounding box around the whole detector.
    */
   TVector3    GetBoundingBoxDimensions(){ return(fTrackerBBoxDim);}
   TVector3    GetDetectorBoxDimensions(){ return(fTrackerDetectorBox);}

   /** Returns a vector of the dimensions of a bounding box around the layer.
    */
   TVector3    GetLayerBoundingBoxDimensions(Int_t layer);

   /** Returns a vector of the central positioning of the detector's bounding box in BETA coords.
    */
   TVector3    GetBoundingBoxOrigin(){ return(fTrackerBBoxOrigin);}

   TVector3    GetActiveBoundingBoxDimensions(){ return(fTrackerActiveBBoxDim);}


private :

   static ForwardTrackerGeometryCalculator * fgForwardTrackerGeometryCalculator;
   ForwardTrackerGeometryCalculator();

   ClassDef(ForwardTrackerGeometryCalculator, 1)
};


#endif
