#ifndef InSANERunSummary_HH
#define InSANERunSummary_HH 1

#include <iostream>
#include <iomanip>
#include "TNamed.h"
#include <vector>

// InSANERunNum is a simple container for an integer.
class InSANERunNum : public TObject {
   private:
      int  num;

   public:
      InSANERunNum(int i = 0) : num(i) { }
      ~InSANERunNum() {/*Printf("~InSANERunNum = %d", num);*/ }
      void    SetNum(int i) { num = i; }
      int     GetNum() { return num; }
      void    Print(Option_t *) const { Printf("InSANERunNum = %d", num); }
      ULong_t Hash() const { return num; }
      Bool_t  IsEqual(const TObject *obj) const { return num == ((InSANERunNum*)obj)->num; }
      Bool_t  IsSortable() const { return kTRUE; }
      Int_t   Compare(const TObject *obj) const { if (num > ((InSANERunNum*)obj)->num)
         return 1;
         else if (num < ((InSANERunNum*)obj)->num)
            return -1;
         else
            return 0;
      }
   ClassDef(InSANERunNum,1)
};


/** Run summary data holder. 
 *  Use this to add together run information. When these run summaries are added, 
 *  the values are charge weighted.  
 */ 
class InSANERunSummary : public TNamed {

   public:
      Double_t            fBeamEnergy      ; // Beam Energy
      Double_t            fPBeam           ; // Absolute value of Beam Polarization (0-100)
      Double_t            fPTarget         ; // Absolute value of Target Polarization (0-100)
      Double_t            fNPlus           ; // N+
      Double_t            fNMinus          ; // N-
      Double_t            fQPlus           ; // Q+
      Double_t            fQMinus          ; // Q-
      Double_t            fLPlus           ; // Live time +
      Double_t            fLMinus          ; // Live time -
      Double_t            fPbSign          ; // +1 or -1
      Double_t            fPtSign          ; // +1 or -1
      Double_t            fHalfWavePlate   ; // +1 or -1
      Double_t            fPackingFraction ;
      std::vector<int>    fRuns            ; // List of runs which are summarized

   public:

      InSANERunSummary(): InSANERunSummary("run-summary","run summary") { }
      InSANERunSummary(const char * n,const char * t = "run summary");
      InSANERunSummary(const InSANERunSummary&)            = default;
      InSANERunSummary(InSANERunSummary&&)                 = default;
      InSANERunSummary& operator=(const InSANERunSummary&) = default;
      InSANERunSummary& operator=(InSANERunSummary&&)      = default;
      virtual ~InSANERunSummary();

      Int_t GetNum() const {
         if(fRuns.size() > 0 ) return fRuns.at(0);
         /*else*/ return(0);
      }
      ULong_t Hash() const {
         if(fRuns.size() > 0 ) return fRuns.at(0);
         /*else*/ return(0);
      }
      Bool_t  IsEqual(const TObject *obj) const {
         return GetNum() == ((InSANERunSummary*)obj)->GetNum();
      }
      Bool_t  IsSortable() const {
         return kTRUE;
      }
      Int_t   Compare(const TObject *obj) const {
         if (GetNum() > ((InSANERunSummary*)obj)->GetNum())
            return 1;
         else if (GetNum() < ((InSANERunSummary*)obj)->GetNum())
            return -1;
         else
            return 0;
      }

      /** Add another runsummary to this one. 
       *  Take the charge weighted averages of polarizations and live times.
       *  \todo figure out how to add runs with differing polarization signs!!
       */ 
      InSANERunSummary & operator+=(const InSANERunSummary &rhs);

      // Add this instance's value to other, and return a new instance
      // with the result.
      const InSANERunSummary operator+(const InSANERunSummary &other) const {
         return InSANERunSummary(*this) += other;
      }

      Double_t GetOverallSign()     const { return( fPtSign*fPbSign*fHalfWavePlate ) ; }
      Int_t    GetNRuns()           const { return( fRuns.size() ); }
      Double_t GetTotalCharge()     const { return( fQPlus + fQMinus); }
      Double_t GetTotalLiveTime()   const { return( (fLPlus + fLMinus)/2.0); }
      Double_t GetTotalCount()      const { return( fNPlus + fNMinus ); }
      Double_t GetRate()            const { return( GetTotalCount()/(GetTotalCharge()*GetTotalLiveTime()) ); }
      Double_t GetNormCharge()      const { return( GetTotalCharge()*GetTotalLiveTime() ); }
      Double_t GetPackingFraction() const { return( fPackingFraction ); }

      Double_t GetChargeAsym()      const { return( (fQPlus-fQMinus)/(fQPlus+fQMinus) ); }
      Double_t GetLiveTimeAsym()    const { return( (fLPlus-fLMinus)/(fLPlus+fLMinus) ); }
      Double_t GetCountAsym()       const { return( (fNPlus-fNMinus)/(fNPlus+fNMinus) ); }
      Double_t GetLTCorrectedAsym() const { return( ((fNPlus/fLPlus)-(fNMinus/fLMinus))/((fNPlus/fLPlus)+(fNMinus/fLMinus)) ); }
      Double_t GetQCorrectedAsym()  const { return( ((fNPlus/fQPlus)-(fNMinus/fQMinus))/((fNPlus/fQPlus)+(fNMinus/fQMinus)) ); }
      Double_t GetQLTCorrectedAsym()const { return( ((fNPlus/fLPlus/fQPlus)-(fNMinus/fLMinus/fQMinus))/((fNPlus/fLPlus/fQPlus)+(fNMinus/fLMinus/fQMinus)) ); }

      Double_t GetPb() const { return( fPTarget*fPtSign ); }
      Double_t GetPt() const { return( fPBeam*fPbSign ); } 
      Double_t GetPbPt() const { return ( GetPb()*GetPt() ); }

      void SetPt(Double_t pt) { fPTarget = TMath::Abs(pt); fPtSign = TMath::Sign(1.0,pt) ; }
      void SetPb(Double_t pb) { fPBeam   = TMath::Abs(pb); fPbSign = TMath::Sign(1.0,pb) ; }

      void SetPackingFraction(Double_t pf) { fPackingFraction = pf ; }
      void Print(Option_t * opt = "") const ;  // *MENU*

      ClassDef(InSANERunSummary,9)
};

#endif


