#ifndef InSANEAnalysisManager_HH
#define InSANEAnalysisManager_HH 1

#include "TObject.h"
#include "SANERunManager.h"
#include "InSANECutter.h"
#include <iostream>
#include <fstream>
#include "InSANEAnalyzer.h"
#include "TChain.h"
#include <map>
#include "InSANERunSummary.h"

/**  Base class for analysis manger
 *
 *  \ingroup Management
 *  \ingroup Analysis
 */
class InSANEAnalysisManager : public TObject {


   public :

      virtual ~InSANEAnalysisManager();


      /** @name Cut Management
       *  Set the axe used in the current analysis pass
       *  An "axe" is just a whole bunch of cuts.
       */
      //@{


      /** Set the "axe" to use for current analysis */
      virtual Int_t SetAxe(InSANECutter * axe) {
         fAxe = axe;
         return(0);
      }
      /** get the current axe used */
      InSANECutter * GetAxe() { return(fAxe); }

      /** Load the axe/cuts  with name fName and located saved in the file   */
      Int_t LoadCuts(const char * axeName, const char * fName) {
         fCutsFile = new TFile(fName, "READ");
         InSANECutter * axe = nullptr;
         fCutsFile->GetObject(axeName, axe);
         if (axe) {
            SetAxe(axe);
            return(0);
         } /* else */
         return(-1);
      }



      //@}

   protected:
      InSANECutter * fAxe;
      TFile * fCutsFile;
   public:

      /** process queue in standard way
       *  \deprecated
       */
      Int_t ProcessQueue() {
         Int_t nProcessed = 0;
         while (fRunQueue->size() > 0) {
            //FullProcessOneRun();
            nProcessed++;
         }
         return(nProcessed);
      }

      /** @name Run Queues
       *  Methods to fill the run queue
       */
      //@{

      /** A better version of build queue which reads text files with comments that begin with #.
       *  It actually ignores everything except lines that start with a number.
       */
      Int_t BuildQueue2(const char * fName);

      /** Build up a run queue from a simple text file
       */
      Int_t BuildQueue(const char * fName, const int ncolumns = 1, const int runNumCol = 0);

      /** Using the run queue, a TChain is built from the trees name.
       *  It assumes that the tree is int he root file "data/rootfiles/InSANE####.0.root"
       *  \todo Add source file flexibility
       */
      TChain * BuildChain(const char * name) {
         auto * chain = new TChain(name);
         Int_t nConnected = 0;
         Int_t Ncon = 1;
         for (unsigned int i = 0; i < GetRunQueue()->size(); i++) {
            /// Hack to get around old file numbering (was 0 but now is -1)...
            nConnected = chain->Add(Form("data/rootfiles/InSANE%d.-1.root", GetRunQueue()->at(i)));
            if (Ncon != nConnected)
               nConnected = chain->Add(Form("data/rootfiles/InSANE%d.0.root", GetRunQueue()->at(i)));
            Ncon = nConnected;
         }
         return(chain);
      }

      /** Builds a map for the queue of pointers to the run objects.*/
      std::map<int,InSANERun*> * GetQueueRunMap(){
         fQueueRunMap = new std::map<int,InSANERun*>;
         for (unsigned int i = 0; i < GetRunQueue()->size(); i++) {
            InSANERunManager::GetRunManager()->SetRun( GetRunQueue()->at(i),-1,"Q");
            InSANERun * arun = InSANERunManager::GetRunManager()->GetCurrentRun();
            (*fQueueRunMap)[GetRunQueue()->at(i)] = arun;
         }
         return( fQueueRunMap);
      }

      InSANERunSummary GetQueueSummary(){
         InSANERunSummary aSummary; ;
         for (unsigned int i = 0; i < GetRunQueue()->size(); i++) {
            InSANERunManager::GetRunManager()->SetRun( GetRunQueue()->at(i),-1,"Q");
            InSANERun * arun = InSANERunManager::GetRunManager()->GetCurrentRun();
            if(i==0) aSummary = arun->GetRunSummary();
            else aSummary += arun->GetRunSummary();
            arun->GetRunSummary().Dump();
         }
         return( aSummary);

      }

      /** Adds test runs to queue
       */
      Int_t QueueUpTestRuns() {
         fRunQueue->push_back(72994);
         fRunQueue->push_back(72995);
         fRunQueue->push_back(72999);
         return(0);
      }

      /** Print the current run queue.
       *  If file name is provided, then the list is saved to a text
       *  file.
       */
      void PrintRunQueue(const char * file = nullptr) {
         if (file) {
            std::ofstream of;
            of.open(Form("lists/%s.txt", file), std::ios::trunc);
            for (unsigned int i = 0; i < fRunQueue->size(); i++) of <<  fRunQueue->at(i) << "\n";
            of.close();

         } else {
            for (unsigned int i = 0; i < fRunQueue->size(); i++) printf("%d\n", fRunQueue->at(i));
         }
      }

      void PrintQueueLine(){
         for (unsigned int i = 0; i < fRunQueue->size(); i++) printf("%d ", fRunQueue->at(i));
         std::cout << "\n";
      }

      void PrintQueue(){
         for (unsigned int i = 0; i < fRunQueue->size(); i++) printf("%d \n", fRunQueue->at(i));
         std::cout << "\n";
      }
      /** Resets the Analysis pass for each run in the run queue.
       *  The analysis pass is set to the value of the pass argument
       */
      void ResetQueue(Int_t pass) {
         for (unsigned int i = 0 ; i < fRunQueue->size(); i++) {
            auto * runman = (SANERunManager*) InSANERunManager::GetRunManager();
            runman->SetRun(fRunQueue->at(i));
            runman->ResetRunPass(pass);
            runman->WriteRun();
         }
      }

      //@}

      /** @name Operations on run Queues
       *  Perform operations on run queues
       */
      //@{

      Double_t GetTotalPositiveCharge() {
         Double_t total = 0.0;
         for (unsigned int i = 0 ; i < fRunQueue->size(); i++) {
            auto * runman = (SANERunManager*) InSANERunManager::GetRunManager();
            runman->SetRun(fRunQueue->at(i));
            total += runman->GetCurrentRun()->fTotalQPlus;
         }
         return(total);
      }

      Double_t GetTotalNegativeCharge() {
         Double_t total = 0.0;
         for (unsigned int i = 0 ; i < fRunQueue->size(); i++) {
            auto * runman = (SANERunManager*) InSANERunManager::GetRunManager();
            runman->SetRun(fRunQueue->at(i), 0, "Q");
            total += runman->GetCurrentRun()->fTotalQMinus;
         }
         return(total);
      }



      //@}

      /** This does everything EXCEPT replay using the Hall C Analyzer
       *  It assumes that this has aleady been done.
       */
      Int_t ProcessOneRun();

      /**
       */
      Int_t RunHallCReplay();

      /** \deprecated Don't use this ?
       */
      Int_t ProcessPreRunReportsOnly();

      std::vector<int> * GetRunQueue() {
         return fRunQueue;
      }

      /** List of runs to be analyzed */
      std::vector<int> * fRunQueue;

      void SetAnalyzer(InSANEAnalyzer* an) {
         fAnalyzer = an;
      }
      InSANEAnalyzer * GetAnalyzer() {
         return fAnalyzer;
      }

      /** \name Detector memeber functions
       *
       * @{
       */
      /** Tries to find a saved instance of the detector
       *  Returns 0 if non are found
       */
      InSANEDetector * GetDetector(const char * detName) {
         if (!fSaveDetectors) return nullptr;
         InSANEDetector * aDet = nullptr;
         aDet = (InSANEDetector*) gROOT->FindObject(detName);
         if (aDet) return(aDet);
         if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
            aDet = (InSANEDetector*) InSANERunManager::GetRunManager()->GetCurrentFile()->FindObject(detName);
         }
         if (aDet) return(aDet);
         return(nullptr);
      }
      Bool_t fSaveDetectors;
      void SaveDetectors(TList * detList) {

      }

      //@}
      /*
         static InSANEAnalysisManager * GetAnalysisManager() {
         if(!fgSANEAnalysisManager) fgSANEAnalysisManager = new SANEAnalysisManager();
         return(fgSANEAnalysisManager);
         }
       */
   protected:
      InSANEAnalyzer * fAnalyzer;
      std::map<int,InSANERun*> * fQueueRunMap; 
      /// static "global" pointer to singleton
      static InSANEAnalysisManager * fgInSANEAnalysisManager;

      InSANEAnalysisManager();

      ClassDef(InSANEAnalysisManager, 1)
};

#endif

