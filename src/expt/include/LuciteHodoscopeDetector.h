#ifndef LuciteHodoscopeDetector_h
#define LuciteHodoscopeDetector_h

#include <iostream>
#include <vector>
#include <TROOT.h>
 //#include <TChain.h>
#include "InSANEDetector.h"
#include  <TClonesArray.h>
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "LuciteGeometryCalculator.h"
#include "LuciteHodoscopeBar.h"
#include "TObjArray.h"
#include "InSANEDetectorCalibration.h"
#include "SANERunManager.h"
#include "LuciteHodoscopePositionHit.h"
#define NHODOSCOPEBARS 28
#include "TF1.h"

/** Concrete class for a Detector: SANE lucite hodoscope
 *
 *  Calibrations and Parameters:
 *    - Timing Peak for each tube                   (56)
 *    - TDC Sum timing peak for each bar            (28)
 *    - TDC Difference timing peak for each bar     (28)
 *    - X Position Calibration
 *
 * \ingroup hodoscope
 */
class LuciteHodoscopeDetector : public InSANEDetector {

   private :
      TSortedList     fSumTimings;
      TSortedList     fDiffTimings;

      TRotation fRotFromBCcoords;
      Int_t     fCurrentRunNumber;


   public :

      InSANEDetectorCalibration * fPositionCalibration; //!

      Double_t    cSeedHitMissDistance;

      Double_t    cBigcalXPositionMissDistance;
      Double_t    cBigcalYPositionMissDistance;

      Double_t    cTDCAlignMin;
      Double_t    cTDCAlignMax;

      Double_t    cTDCMin;
      Double_t    cTDCMax;
      Bool_t      cPassesTDC[NHODOSCOPEBARS*2];

      Double_t    cTDCDiffMin;
      Double_t    cTDCDiffMax;
      Bool_t      cPassesTDCDiff[NHODOSCOPEBARS];

      Double_t    cTDCSumMin;
      Double_t    cTDCSumMax;
      Bool_t      cPassesTDCSum[NHODOSCOPEBARS];

      Int_t       fRefBar;              /// Bar used as the timing reference.
      Double_t    fTDCSumOffset;        ///
      Double_t    fTDCDiffOffsets[28];  /// A timing offset for each pair of tubes to compensate for cable length differences

      Bool_t      fUseStaticCalibrations;
      Bool_t      fUseCurrentRunCalibration;
      Double_t    fLuciteIndexOfRefraction;

      std::vector<Int_t>       * fTDCHits[56];                       //! TDC hits for each PMT
      std::vector<Int_t>       * fNegativeTDCHits[28];               //!
      std::vector<Int_t>       * fPositiveTDCHits[28];               //!
      std::vector<Int_t>       * fTimedTDCHits[56];                  //! TDC hits passing a timing cut
      std::vector<Int_t>       * fTimedTDCHitNumber[56];             //!
      std::vector<Int_t>       * fTDCHitDifferences[28];             //! TDC differences  (Positive - Negative)
      std::vector<Double_t>    * fBigcalXPositions[28];              //!
      std::vector<Double_t>    * fBigcalYPositions[28];              //!
      std::vector<Int_t>       * fTDCDifferenceNumberPassingCut[28]; //!
      std::vector<Int_t>       * fTimedTDCHitDifferences[28];        //! Positive-Negative TDC differences where ONE tube has a timed TDC hit
      std::vector<Int_t>       * fTDCSumNumberPassingCut[28];        //!
      std::vector<Int_t>       * fTDCHitSums[28];                    //! Positive+Negative TDC Sums
      std::vector<Int_t>       * fTimedTDCHitSums[28];               //! Positive+Negative TDC Sums where ONE tube has a timed TDC hit
      std::vector<Int_t>       * fTDCHitAndTriggerSums[28];          //! Trigger+Positive+Negative TDC Sums
      std::vector<Int_t>       * fTimedTDCHitAndTriggerSums[28];     //! Trigger+Positive+Negative TDC Sums where one tube has a timed TDC hit
      LuciteGeometryCalculator * fGeoCalc;                           //!

      TH2F * fLuciteHitDisplay;                   //!

      TH2F * fEventDisplayHitDifferences;         //! Hit differences
      TH2F * fRunDisplayHitDifferences;           //!

      TH2F * fEventDisplayTimedHitDifferences;    //! Timed Hit differences
      TH2F * fRunDisplayTimedHitDifferences;      //!

      TH2F * fEventDisplayHitSums;                //! Hit sums
      TH2F * fRunDisplayHitSums;                  //!

      TH2F * fEventDisplayTimedHitSums;           //! Timed Hit sums
      TH2F * fRunDisplayTimedHitSums;             //!

      TH2F * fEventDisplayTimedHitAndTriggerSums; //! Hit sums plus trigger time
      TH2F * fRunDisplayTimedHitAndTriggerSums;   //!

      TH2F * fEventDisplayHitAndTriggerSums;      //! Timed Hit sums plus trigger time
      TH2F * fRunDisplayHitAndTriggerSums;        //!
      TH2F * fLuciteGoodHitDisplay;               //!
      TH2F * fBigcalYVsLuciteRow;                 //!

   public:
      LuciteHodoscopeDetector(Int_t runnum = 0);
      virtual ~LuciteHodoscopeDetector();

      Int_t          SetRun(Int_t runnum);

      virtual Int_t  Build();
      virtual void Initialize() {
         if (!IsBuilt()) Build();
      }

      Int_t          CorrectEvent();
      Int_t          ClearEvent();

      /** \name Geometry functions
       *  useful for event loops etc....
       * @{
       */

      /** Returns the cluster position in Human Coordinates
       *  z = Z0-R + sqrt(R^2-x^2) + zoffset
       */
      TVector3 GetPosition(LuciteHodoscopePositionHit * posHit) {
         TVector3 aPosition(posHit->fPositionVector.X(),
               posHit->fPositionVector.Y() ,
               fGeoCalc->GetDistanceFromTarget()
               - (fGeoCalc->fBarThickness/2.0+fGeoCalc->fBarRadius)
               + TMath::Sqrt((fGeoCalc->fBarThickness/2.0+fGeoCalc->fBarRadius)*(fGeoCalc->fBarThickness/2.0+fGeoCalc->fBarRadius)
                  - posHit->fPositionVector.X()*posHit->fPositionVector.X()) );
         aPosition.Transform(fRotFromBCcoords);
         return(aPosition);
      }
      //   TVector3 GetRotatedPosition(LuciteHodoscopePositionHit * posHit) {
      //      return(GetPosition(posHit));
      //   }

      /** Get the X position of the lucite hodoscope hit using a tdc difference.
       *  Assuming F1TDC was at 60 ps LSB and that there is a 97.4 cm arc length,
       *  l = -X0/2 + C*tdc, where C is the distance per channel (cm/chan).
       *  X0 is the total length traversed by a photon reflecting from one pmt to the other.
       *  The light guides were about 10cm (a guess from a picture in
       *  https://hallcweb.jlab.org/experiments/sane/sane/review/lucite.pdf).
       *  Thus X0 = 97.4 + 2*10 ~ 120 cm.
       *  Since the particles we want are traveling at beta~0.999, the cherenkov cone is about acos(1/n)~45 degrees.
       *  Therefore we add a sqrt(3).
       *  We arrive at C =  60*10^-12*3*10^10/(1.4999*sqrt(3)) = 0.692867
       *  The width of the tdc difference spectrum is about 200 channels which would give 
       *  C = 120/200 = 0.6 which is close to our number above.
       *  The tdc difference peak is aligned at zero (and should be the smallest scattering angle).
       *  If the tdc value falls out of the cTDCDiffMin/cTDCDiffMax range it returns -300.0
       *  Correcting for the arc:
       *  theta = l/R
       *  x = R*sin(theta) + xoffset
       *  z = R*cos(theta) + zoffset
       *
       *
       */
      Double_t GetXPosition(Int_t tdcValue) {
         if (cTDCDiffMin > tdcValue || cTDCDiffMax < tdcValue) return(-300.0);
         //Double_t l = -fGeoCalc->fTotalLightLength/2.0 + ((Double_t)tdcValue) * 0.4; /* 0.8505 cm/chan*/
         Double_t l = 5.0 -96.0/2.0 + ((Double_t)tdcValue) * 0.384; // 96cm/250channels=0.384
         Double_t theta = l/(fGeoCalc->fBarThickness/2.0+fGeoCalc->fBarRadius);
         Double_t xposition  = (fGeoCalc->fBarThickness/2.0+fGeoCalc->fBarRadius)*TMath::Sin(theta);
         return(xposition);
      }

      /** Get the vertical position in bigcal coordinates using the bar number.
       */
      Double_t GetYPosition(Int_t bar) {
         return(fGeoCalc->GetLuciteYPosition(bar));
      }

      Double_t GetMissDistance(TVector3 v1, TVector3 v2) {
         TVector3 res(v1 - v2);
         return(res.Mag());
      }

      /** Returns the point of intersection for a line from the seeding cluster
       *  to the origin with the cylindrical surface formed by the front of the hodoscope bars.
       *  Using Human Coordinates
       */
      TVector3 GetSeedingHitPosition(TVector3 seedvec);

      //@}


      /** Sets the position using calibrations.  */
      Int_t SetPositionOfHit(LuciteHodoscopePositionHit * hit) {
         InSANECalibration * calib = fPositionCalibration->GetChannel(hit->fBarNumber - 1);
         if (calib) {
            hit->fPositionVector.SetXYZ(
                  calib->GetFunction()->Eval(hit->fTDCDifference) ,
                  //fit->Eval(hit->fTDCDifference - fPositionCalibration->fConstants->at(hit->fBarNumber-1)),
                  fGeoCalc->GetLuciteYPosition(hit->fBarNumber),
                  fGeoCalc->fLuciteZPosition);
         } else {
            std::cout << " NO FIT FUNCTION FOUND !\n";
         }
         return(0);
      }

      /** Adds a TDC hit to..
       *  Fills the holder data for event
       *  pmtNum=odd gives positive sides
       *  pmtNum=even gives negative sides
       */
      Int_t AddTDCHit(Int_t pmtNum) {
         //if(fBarTimedTDCHit[pmtNum-1]) fBarDoubleTimedTDCHit[pmtNum-1]=true;
         //fBarTimedTDCHit[pmtNum-1]=true;
         //if((pmtNum-1)%2 == 0) { // positive
         //  fPositiveBarTDCHit[(pmtNum-1)/2]=true;
         //} else { // negative
         //  fNegativeBarTDCHit[(pmtNum-1)/2]=true;
         //}
         return(0);
      }

      //    Bool_t HasNeighboringHit(Int_t mirror);

      Bool_t PositionHitPassesTimingCut(LuciteHodoscopePositionHit * aPosHit) {
         if (aPosHit->fTDCSum > cTDCSumMin && aPosHit->fTDCSum < cTDCSumMax) return(true);
         else return(false);
      }

      /**  Retrieve  the position calibration from the root file.  */
      Int_t GetPositionCalibrations() {
         SANERunManager::GetRunManager()->GetCurrentFile()->cd();
         SANERunManager::GetRunManager()->GetCurrentFile()->cd("calibrations/hodoscope");
         gDirectory->GetObject("lucitePositionCalibration", fPositionCalibration);
         if (! fPositionCalibration) {
            std::cout << "x- Could not find lucitePositionCalibration \n";
         }
         return(0);
      }

      void PrintTimings() const ; // *MENU*

      /** Prepares the event data holders. This is called by InSANEAnalyzer and InSANECorrector when it as been
       *  added through their methods AddDetector(InSANEDetector*).
       */
      virtual Int_t ProcessEvent();
      virtual void  PrintCuts() const ; // *MENU*

      /**  Post-process-action.  */
      virtual Int_t PostProcessAction() {
         //PrintCuts();
         return(0);
      }

      TList *                 GetSumTimings() { return(&fSumTimings); }
      TList *                 GetDiffTimings() { return(&fDiffTimings); }

      InSANEDetectorTiming *  GetDetectorSumTiming(Int_t i) const { return((InSANEDetectorTiming*)(fSumTimings.At(i))); }
      InSANEDetectorTiming *  GetDetectorDiffTiming(Int_t i) const { return((InSANEDetectorTiming*)(fDiffTimings.At(i))); }

      Int_t                   GetNSumTimings() const { return(fSumTimings.GetEntries()); }
      Int_t                   GetNDiffTimings() const { return(fDiffTimings.GetEntries()); }

      ClassDef(LuciteHodoscopeDetector, 2)
};


#endif

