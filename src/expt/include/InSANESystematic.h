#ifndef InSANESystematic_HH
#define InSANESystematic_HH

#include "TNamed.h"
#include "TH1F.h"
#include "TBrowser.h"

class InSANESystematic : public TNamed {

   public:
      int    fNumber;
      double fAbsoluteValue;
      double fRelativeValue;

      TH1F   fBefore;
      TH1F   fAfter;
      TH1F   fValues;
      TH1F   fUncertainties;
      TH1F   fRelativeUncertainties;

   public:
      InSANESystematic() : InSANESystematic("","") { }
      InSANESystematic(const char * n, const char * t = "");
      InSANESystematic(const char * n , int N);
      InSANESystematic(const InSANESystematic&)           ;//= default;
      InSANESystematic& operator=(const InSANESystematic&);//= default;
      //InSANESystematic(InSANESystematic&&)                 = default;
      //InSANESystematic& operator=(InSANESystematic&&)      = default;
      virtual ~InSANESystematic();

      InSANESystematic &     operator+=(const InSANESystematic &rhs) ;
      const InSANESystematic operator+(const InSANESystematic &other)  ;

      void InitHists(const TH1F & h);
      void Reset(Option_t * opt = "");

      void Scale(double weight);

      void Add(const InSANESystematic& sys, double weight);

      Bool_t         IsFolder() const { return kTRUE; }
      void           Browse(TBrowser* b){
         b->Add(&fBefore);
         b->Add(&fAfter);
         b->Add(&fValues);
         b->Add(&fUncertainties);
         b->Add(&fRelativeUncertainties);
      } 

      ClassDef(InSANESystematic,2)
};



#endif

