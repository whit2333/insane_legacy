#ifndef InSANEClustering_HH
#define InSANEClustering_HH

#include <TROOT.h>
#include <TObject.h>
#include <TTree.h>
#include <TFile.h>
#include <TSpectrum2.h>
#include "InSANECalorimeterCluster.h"
#include "InSANECluster.h"
#include "InSANEEvent.h"
#include "InSANEGeometryCalculator.h"
#include <iostream>
#include <TClonesArray.h>

/**
 *  Template class which does the clustering. Namely determines the number of clusters
 *  and their respective maximums
 *  This is passed to the cluster processor for analysis reconstruction, analysis,
 *  and rejection
 *  The class T is the type of cluster, e.g., InSANECluster,BIGCALCluster,...
 *
 *  I guess clustering involes picking the number of clusters and IDing their locations.
 *  This is to say clustering partitionins the space.
 */
class InSANEClustering : public TObject {
public:

   InSANEClustering() ;

   ~InSANEClustering();

   Int_t  GetNumberOfClusters() ;

   Int_t fNClusters;

   ClassDef(InSANEClustering, 1)
};


#endif

