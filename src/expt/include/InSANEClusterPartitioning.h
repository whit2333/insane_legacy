#ifndef InSANEClusterPartitioning_HH
#define InSANEClusterPartitioning_HH 1
#include "TObject.h"
#include <iostream>
#include "TMath.h"
#include <vector>
#include <TClonesArray.h>

/** A datum, which during clustering, is shuffled from one cluster/partition to another
 *
 *  \ingroup Clustering
 */
class InSANEClusterDatum  : public TObject {
public:
   InSANEClusterDatum() {
      Clear();
   }
   Int_t fiCell;
   Int_t fjCell;
   Double_t fECell;
   Int_t fPartionNumber;
   void Clear() {
      fiCell = 0;
      fjCell = 0;
      fECell = 0.0;
      fPartionNumber = -1;
   }

   ClassDef(InSANEClusterDatum, 1)
};

/**  A partitioning centroid for a single partition
 *
 *  \ingroup Clustering
 */
class InSANEPartitionCentroid  : public TObject {
public:
   InSANEPartitionCentroid() {
      Clear();
   }
   Double_t fiCentroid;
   Double_t fjCentroid;
   Double_t fECentroid;
   Int_t fCentroidNumber;
   void Clear() {
      fiCentroid = 0.0;
      fjCentroid = 0.0;
      fECentroid = 0.0;
      fCentroidNumber = -1;
   }
   ClassDef(InSANEPartitionCentroid, 1)
};

/**  Base class for a partitioning for a clustering algorithm.
 *
 *  The "measure" or "distance" used in calculating the distance from
 *  each centroid should be defined here by implementing CalculateMeasure()
 *
 *  \ingroup Clustering
 */
class InSANEClusterPartitioning : public TObject {
public:

   InSANEClusterPartitioning() {
      fCentroids = new TClonesArray("InSANEPartitionCentroid", 1);
      fData = new TClonesArray("InSANEClusterDatum", 1);
   }

   virtual ~InSANEClusterPartitioning() {
      if (fData) fData->Delete();
      if (fCentroids) fCentroids->Delete();
   }

   void SetDatumPartitionNumber(Int_t iDatum, Int_t kPartition) {
      if (fData->GetEntries() < iDatum) {
         std::cout << "Datum index out of range";

      } else {
         auto * datum = (InSANEClusterDatum*)(*fData)[iDatum];
         datum->fPartionNumber = kPartition;
      }
   }

   void AddDatum(Int_t i, Int_t j, Double_t E) {
      auto * datum =
         new((*fData)[fNData]) InSANEClusterDatum();
      datum->fiCell = i;
      datum->fjCell = j;
      datum->fECell = E;
      fNData++;
   }

   void AddCentroid(double i, double j, Double_t E) {
      auto * centroid =
         new((*fCentroids)[fNPartitions]) InSANEPartitionCentroid();
      centroid->fiCentroid = i;
      centroid->fjCentroid = j;
      centroid->fECentroid = E;
      centroid->fCentroidNumber = fNPartitions;
      fNPartitions++;
   }

   /** Calculates the distance or measure for iDatum point w.r.t the centroid kCentroid
    *
    * This implementation just uses the distance
    */
   virtual Double_t CalculateMeasure(Int_t iDatum, Int_t kCentroid) {
      if (fCentroids->GetEntries() < kCentroid) {
         std::cout << "kCentroid out of range";
         return(-99999999.9);
      }
      auto * aCentroid = (InSANEPartitionCentroid*)(*fCentroids)[kCentroid];
      auto * aDatum = (InSANEClusterDatum*)(*fData)[iDatum];
      return(TMath::Sqrt((aCentroid->fiCentroid - aDatum->fiCell) * (aCentroid->fiCentroid - aDatum->fiCell) +
                         (aCentroid->fjCentroid - aDatum->fjCell) * (aCentroid->fjCentroid - aDatum->fjCell)));
   }

   /** Determines based upon the measure which partition centroid said datum belongs to
    */
   virtual Int_t GetPartition(Int_t iDatum) {
      Double_t distTemp = 99999999.0;
      Double_t dist = 99999999.0;
      Int_t closestPartition = -1;
      if (fData->GetEntries() < iDatum) {
         std::cout << "Datum index out of range";
         return(-1);
      }
//     InSANEClusterDatum * aDatum = (InSANEClusterDatum*)(*fData)[iDatum];
      for (int k = 0; k < fCentroids->GetEntries(); k++) {
         distTemp = CalculateMeasure(iDatum, k);
         if (distTemp < dist) {
            dist = distTemp;
            closestPartition = k;
         }
      }
      return(closestPartition);
   }

   /** Calculates and updates each centroid value */
   virtual void CalculateCentroids() {
      Double_t iNew, jNew, ENew;
      Int_t nDataPoints;
      InSANEPartitionCentroid * aCentroid;
      InSANEClusterDatum * aDatum;
      for (int k = 0; k < fCentroids->GetEntries(); k++) {
         nDataPoints = 0;
         iNew = 0.0;
         jNew = 0.0;
         ENew = 0.0;
         aCentroid = (InSANEPartitionCentroid*)(*fCentroids)[k];
         for (int i = 0; i < fData->GetEntries(); i++) {
            aDatum = (InSANEClusterDatum*)(*fData)[i];
            if (aDatum->fPartionNumber == aCentroid->fCentroidNumber) {
               nDataPoints++;
               iNew += (Double_t) aDatum->fiCell;
               jNew += (Double_t) aDatum->fjCell;
               ENew += (Double_t) aDatum->fECell;
            }
         } // done looping over all datapoints for this partition
         aCentroid->fiCentroid = (Int_t)(iNew / ((Double_t)nDataPoints));
         aCentroid->fjCentroid = (Int_t)(jNew / ((Double_t)nDataPoints));
         aCentroid->fECentroid = ENew / ((Double_t)nDataPoints);
      }
   }

   virtual Int_t Repartition() {
      Int_t numberOfPartitionsChanged = 0;
      InSANEClusterDatum * aDatum;
      Int_t iPart = -1;
      for (int i = 0; i < fData->GetEntries(); i++) {
         aDatum = (InSANEClusterDatum*)(*fData)[i];
         iPart  = GetPartition(i);
         if (iPart != aDatum->fPartionNumber) {
            numberOfPartitionsChanged++;
            aDatum->fPartionNumber = iPart;
         }
      }
      return(numberOfPartitionsChanged);
   }

   virtual void Clear() {
      fNPartitions = 0;
      fNData = 0;
      fData->Clear("C");
      fCentroids->Clear("C");
   }

protected:
   TClonesArray * fCentroids;
   TClonesArray * fData;
   Int_t fNPartitions;
   Int_t fNData;

   ClassDef(InSANEClusterPartitioning, 1)
};


#endif

