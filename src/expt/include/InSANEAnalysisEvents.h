#ifndef InSANEAnalysisEvent_H
#define InSANEAnalysisEvent_H
#include "TNamed.h"
#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include "TString.h"


/** Base class for handling the events for Analysis.
 *  NOTE: This and derived classes have eventS ... that is plural of event and
 *  should NOT be saved to a tree.
 *
 * ABC for an \em Analysis Event. This would like lead to some sort of
 * \em PhysicsAnalysis Event....
 */
class InSANEAnalysisEvents : public TNamed {
public :

   InSANEAnalysisEvents(const char * treename = "betaDetectors");
   ~InSANEAnalysisEvents();

   virtual  Int_t GetEvent(Long64_t event) = 0;

   virtual void ClearEvent(Option_t * opt = "") = 0;


   ClassDef(InSANEAnalysisEvents, 1)
};

#endif

