#ifndef SANEFirstPassAnalyzer_H
#define SANEFirstPassAnalyzer_H 1

#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "InSANECalculation.h"
#include "InSANEAnalyzer.h"
#include "SANEClusteringAnalysis.h"
#include "TString.h"
#include "SANERunManager.h"



/** First Pass Analysis class
 *
 *  <ol>
 *  <li> The first goal of this pass is to cluster the calorimeter data.</li>
 *  <li> The second goal of this pass is to analyze the cherenkov in the following way...
 *    <ul>
 *    <li>independent of all other detectors (of course connected through triggers)</li>
 *    <li>using just the new bigcal clusters calculated, determine geometry parameters</li>
 *    <li>using hodoscope with cherenkov timing cut, determine geometry parameters</li>
 *    </ul>
 *  </li>
 *  </ol>
 *
 *  \ingroup Analyzers
 */
class SANEFirstPassAnalyzer :  public SANEClusteringAnalysis , public InSANEAnalyzer  {
public:

   /** c'tor argument should be the name of the new tree to create
    *
    *   The constructor does the following
    *   <ul>
    *   <li>Calls InSANEDetectorAnalysis::InSANEDetectorAnalysis() c'tor
    *    which creates the event and detector objects (this is called before InSANEClusteringAnalysis())</li>
    *   <li>Calls InSANEClusteringAnalysis::InSANEClusteringAnalysis() c'tor
    *    which creates the
    *   InSANEClusterEvent and TClonesArray (of BIGCALCluster) objects
    *    which will become branches
    *   <li>Sets fInputTree to the InSANEAnalysis data member fAnalysisTree<li>
    *   <li>Finds the tree </li>
    *   </ul>
    *
    */
   SANEFirstPassAnalyzer(const char * newTreeName = "correctedBetaDetectors", const char * uncorrectedTreeName = "betaDetectors") ;

   virtual ~SANEFirstPassAnalyzer() ;

   virtual void Initialize(TTree * t = nullptr);

   /** Fill Trees then clear all event data
    */
   virtual void FillTrees() {
      if (fCalculatedTree) fCalculatedTree->Fill();
      //if(fClusterTree)fClusterTree->Fill();
   }

   /**  Needed to clear event from InSANEAnalyzer::Process() or InSANECorrector::Process()
    */
   void ClearEvents() {
      SANEClusteringAnalysis::ClearEvents();
      fClusterEvent->ClearEvent();
      fEvents->BETA->ClearEvent();
      fClusters->Clear();
   }

   /**  Executes post-first pass analysis scripts
    *
    *  These currently are:
    *  <ol>
    *  <li>scripts/bigcalCherenkov.cxx</li>
    *  <li>scripts/pi0mass.cxx</li>
    *  <li>scripts/RunAppend.cxx</li>
    *  <li>scripts/luciteBarGeometry.cxx</li>
    *  </ol>
    *
    *  \todo Make a list of script names that are to be executed instead of
    *  one long method
    */
   virtual void MakePlots();

   ClassDef(SANEFirstPassAnalyzer, 1)
};


#endif

