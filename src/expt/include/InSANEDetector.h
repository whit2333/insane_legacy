#ifndef InSANEDetector_H
#define InSANEDetector_H 1
#include "TROOT.h"
#include "TNamed.h"
#include <iostream>
#include <vector>
#include "InSANEDetectorGeometry.h"
#include "InSANEDetectorEvent.h"
#include "InSANEDetectorHit.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "InSANEDetectorComponent.h"
#include "InSANECoordinateSystem.h"
#include "TH2F.h"
#include "TList.h"
#include "TBrowser.h"
#include "TSortedList.h"

/*!
   \page detectorImp Detectors

   The detector geometries are defined through the database entries for the detector.
   Furthermore, all calibration constants associated with a particular piece of a geometry
   are contained with said database. The user hook for this is the InSANEDetectorGeometry class.

   For example, BIGCALGeometryCalculator is used to get the position of a lead glass block (i,j).
   It also knows the dimensions of the block which is a function of i and j.
   Since the calibration constants are a function of i and j, it seems like a natrual spot to insert these.

   \section database MySQL Database and Tables 
   InSANE relies on having access to a MySQL database and subsequently
   specific tables with a specific structure.

   \section concreteDetectors Concrete Implementations
   See \ref Detectors for more information.

   Here are some concrete detector implementations
       - \ref Cherenkov
       - \ref hodoscope
       - \ref BIGCAL
       - \ref tracker
       - \ref BETA
       - \ref beam
 */




/**  ABC class for a generic detector.
 *
 *   Ideally this would be inherited at the smallest level
 *   say a PMT or APD etc....
 *
 *   \ingroup Detectors
 */
class InSANEDetector : public TNamed {

   protected :
      std::vector<Int_t>   fNoisyChannels;
      TList                fDetectorCorrections ;
      TList                fHistograms;
      Int_t                fNComponents;
      TSortedList          fComponents;
      TSortedList          fPedestals;
      TSortedList          fTimings;

      TList                fCompPedestals;
      TList                fCompTimings;
      Bool_t               fIsBuilt;


   protected:

      // Keep these?
      InSANECoordinateSystem * fCoordinateSystem; //!
      InSANEDetectorEvent    * fEvent;            //!
      InSANEDetectorEvent    * fCorrectedEvent;   //!
      TList                    fEventDisplayHistograms; /// Array of event display histograms
      TH2F                   * fDetectorEventDisplay;   //! Histogram for event display
      TH1F                   * fDetectorEventDisplay1D; //!

      Int_t                 fNumberOfPedestals;
      Int_t                 fNumberOfTDCs;
      Int_t                 fNumberOfADCChannels;
      Int_t                 fNumberOfTDCChannels;
      Int_t                 fNumberOfScalerChannels;

      Int_t                 fNumberOfScalers;
      Long64_t *            fEventScaler;    //!
      Long64_t *            fPreviousScaler; //!
      Long64_t *            fCurrentScaler;  //!
      Long64_t *            fNextScaler;     //!
      Int_t                 fNumberOfScalerEvents;
      Long64_t *            fAverageScaler; //!
      std::vector<Int_t> *  fTimedTDCHitNumbers; //!

   protected:
      void     SetBuilt(Bool_t built = true) { fIsBuilt = built; }

   public:
      Int_t     fTDCRangeMin;
      Int_t     fTDCRangeMax;
      Int_t     fADCRangeMin;
      Int_t     fADCRangeMax;
      Double_t  fNominalTDCCutWidth;

      Int_t     fTypicalTDCPeak ;
      Int_t     fTypicalTDCPeakWidth ;
      Int_t     fTypicalPedestal;
      Int_t     fTypicalPedestalWidth;


   public:
      InSANEDetector(const char * n="", const char * t="");
      virtual ~InSANEDetector();

      Bool_t IsFolder() const { return kTRUE; }

      void Browse(TBrowser* b) {
         b->Add(&fComponents, "Components");
      }

      /**  \name Detector Components
       *   The detector components which may or may not have ACDs, TDCs, calibrations, etc.. associated with it
       *  @{
       */

      /** Pure virtual method which creates all the components of the detector.
       *  Do not call from constructor.
       */
      virtual Int_t Build() = 0;

      /** Initialize after all constructors are called.
       *  "Dynamic Binding During Initialization"
       */
      virtual void Initialize() = 0;

      /** Should process the event using data from only the detector. */
      virtual Int_t ProcessEvent() = 0;

      /** Add a component to the detector.
       *  If it has a pedestal or timing it is added to the fCompPedestals and fCompTimings
       */
      void          AddComponent(InSANEDetectorComponent * comp);
      TList *       GetComponents() { return(&fComponents); }

      /** Returns component number.
       *  Channel number starts at 0.
       */
      virtual InSANEDetectorComponent * GetComponent(int n);

      //@}

      /** Clear event in order to process the next event.
       *  Note: The event no longer needs to be cleared from here.
       *  This is done within the implementation of InSANEAnalysis::ClearEvents()
       */
      virtual Int_t ClearEvent() {
         /*  if(fEvent)fEvent->ClearEvent();*/
         return(0);
      }

      /** Sets the event address for correcting and analysis.
       */
      void SetEventAddress(InSANEDetectorEvent* event) {
         fEvent          = event;
         fCorrectedEvent = event;
      }

      /**  Sets the address of the scaler pointer. */
      void SetScalerAddress(Long64_t * scaler);

      virtual void  PrintDetectorConfiguration() const ; // *MENU*
      virtual void  PrintCuts() const { } // *MENU*
      virtual void  PrintEvent() const ; // *MENU*

      /** Post-process-action is executed just after the event loop for each detector.
       *  Here is a good place to print the cuts, save run histograms, process a run averaged value
       *  etc..
       */
      virtual Int_t PostProcessAction() {
         return(0);
      }

      virtual void InitHistograms() { }

      /** @name Pedestals and Timing Peaks
       * @{
       */

      Int_t    GetNumberOfPedestals() const { return(fNumberOfPedestals); }
      Int_t    GetNumberOfTDCs() const { return(fNumberOfTDCs); }

      /**  Add a pedestal to TClonesArray of pedestals.  */
      Bool_t   AddPedestal(Int_t id, Double_t val, Double_t width);

      /**  Set an already added pedestal value.
       *   Returns...
       */
      Bool_t   SetPedestal(Int_t id, Double_t val, Double_t width);

      /** FIX THIS */
      Int_t    AddTiming(Int_t /*id*/, Double_t /*val*/, Double_t /*width*/) {
         return(0);
      }

      void     PrintPedestals() const ; // *MENU*
      void     PrintTimings() const ; // *MENU*

      /** Set the number of pedestals... not really useful? */
      void     SetNumberOfPedestals(Int_t num) { fNumberOfPedestals = num; }

      /** Get the Pedestal value.
       *  Argument goes from 0-fNumberOfPedestals
       */
      Double_t GetPedestal(Int_t num) { return(GetDetectorPedestal(num)->fPedestalMean); }

      InSANEDetectorPedestal * GetDetectorPedestal(Int_t num) const {
         return((InSANEDetectorPedestal*)(fPedestals.At(num)));
      }

      /** Get the TDC peak value.
       *  Argument goes from 0-fNumberOfTDCs
       */
      Double_t GetTDCPeak(Int_t num) {
         return(GetDetectorTiming(num)->fTDCPeak);
      }

      InSANEDetectorTiming * GetDetectorTiming(Int_t num) const {
         return((InSANEDetectorTiming*)(fTimings.At(num)));
      }

      /** Checks the timing to make sure it isn't crazy or bad.
       *  Returns 0 if it is good and -1 if it is bad.
       */
      virtual Int_t CheckTiming(InSANEDetectorTiming * timing){
         if( TMath::Abs( timing->fTDCPeak - (double)fTypicalTDCPeak ) > 6.0*(double)fTypicalTDCPeakWidth ) {
            timing->fTDCPeak = fTypicalTDCPeak;
            return(-1);
         }
         return(0);
      }


      /** Get the Pedestal width.
       *  Argument goes from 0-fNumberOfPedestals
       */
      Double_t GetPedestalWidth(Int_t num) {
         return(GetDetectorPedestal(num)->fPedestalWidth);
      }
      /**  Get the TDC peak Width. Argument goes from 0-fNumberOfTDCs */
      Double_t GetTDCPeakWidth(Int_t num) {
         return(GetDetectorTiming(num)->fTDCPeakWidth);
      }

      TList * GetPedestals() { return(&fPedestals); }
      TList * GetTimings() { return(&fTimings); }

      /** Returns true if channel has been added to the noisy channel list */
      bool IsNoisyChannel(Int_t chan) {
         for (unsigned int i = 0; i < fNoisyChannels.size(); i++) {
            if (fNoisyChannels[i] == chan) return true;
         }
         return(false);
      }
      void AddNoisyChannel(Int_t chan) {
         fNoisyChannels.push_back(chan);
      }
      void PrintNoisyChannels() const ;

      TList * GetCorrections() {
         return &fDetectorCorrections;
      }

      Bool_t   IsBuilt() const { return(fIsBuilt); }

      ClassDef(InSANEDetector, 3)
};



#endif
