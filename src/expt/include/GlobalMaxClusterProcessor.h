#ifndef LocalMaxClusterProcessor_h
#define LocalMaxClusterProcessor_h

#include <TROOT.h>
#include <TObject.h>
#include <TTree.h>
#include <TFile.h>
#include <TSpectrum2.h>
#include "InSANECalorimeterCluster.h"
#include "InSANECluster.h"
#include "InSANEEvent.h"
#include "InSANEGeometryCalculator.h"

#include <iostream>
#include <TClonesArray.h>

/** Concrete class makes clusters around the local maxima
 *
 * This concrete class finds a limited number of local maxima and
 * produces clusters around them.
 *
 * \ingroup Clustering
 */
class LocalMaxClusterProcessor : public InSANEClusterProcessor {
   public:
      /**
       *  Give a histogram to the cluster finder, which it searches for peaks.
       */
      LocalMaxClusterProcessor();
      ~LocalMaxClusterProcessor();

      /**
       * ProcessEvent fills the TClonesArray with BIGCALCluster s. The TClonesArray can
       * then be written to a tree.
       */
      Int_t  ProcessEvent(TClonesArray * clusters);

      /**
       * Set the event address from which the source histogram is generated.
       */
      Int_t SetEventAddress(InSANEEvent * inEvent);

      /**
       * Calculates the various moments, energy, etc... for the ith peak in the source histogram
       *
       */
      Int_t CalculateQuantities(InSANECluster * acluster, Int_t k);

      //   TString      fOptions;
      //   TH2F *       fSourceHistogram;
      //   Int_t        fNCluster;
      //   Int_t        fNFound;
      //   Float_t *    fXCluster ;
      //   Float_t *    fYCluster;
      //   InSANEEvent * fEvent;

      Int_t SetEventReview(Bool_t  res) {
         fExamineEventByEvent = res;
      }
      Int_t IsEventReview() {
         return (fExamineEventByEvent);
      }

   private :
      Bool_t fExamineEventByEvent;
      InSANEGeometryCalculator * fGeoCalc;

      /** Sets the source histogram pointer. */
      Int_t   SetSourceHistogram(TH2F * hist) {
         fSourceHistogram = hist;
         return(0);
      } ;

      /** Returns the histogram used in clustering */
      TH2F *  GetSourceHistogram();

      /** Clears the histogram  */
      Int_t * Clear() {
         if (fSourceHistogram) fSourceHistogram->Reset();
      }

      /**
       *
       */
      //   Int_t    SetFindOptions(char * opt) { fOptions = opt; return(0); };

      /** Finds peaks in source histogram */
      Int_t   FindPeaks();



      ClassDef(LocalMaxClusterProcessor , 1)
};

#endif


