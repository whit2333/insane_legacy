#ifndef InSANERawAsymmetry_HH
#define InSANERawAsymmetry_HH 1

#include "InSANEAsymmetry.h"
#include "TH1F.h"
#include "TList.h"
#include "InSANERunSummary.h"
#include <iostream>
#include <fstream>

/** Raw asymmetry (not binned). 
 */ 
class InSANERawAsymmetry : public InSANEAsymmetry {
   protected:
      Int_t                  fRunNumber;
      Double_t               fDilution;
      Double_t               fPt;
      Double_t               fPb;
      Double_t               fHalfWavePlate;
      Double_t               fAsymmetry;
      Double_t               fError;
      Double_t               fCharge[2];
      Double_t               fLiveTime[2];
      TList                  fHistograms;
      Bool_t                 fChargeSwap;

   public: 
      InSANERawAsymmetry(); 
      virtual ~InSANERawAsymmetry(); 

      Bool_t         IsFolder() const { return kTRUE;}
      void           Browse(TBrowser* b) {
         b->Add(&fHistograms);
      }
      virtual Int_t  CountEvent();


      void Print();      // *MENU*
      void PrintTable(std::ostream &out); // *MENU*
      void PrintTableHeader(std::ostream &out); // *MENU*
      void Calculate();  // *MENU*

      Double_t GetAsymmetry() const { return fAsymmetry;}
      Double_t GetDilution()  const { return fDilution;}
      Double_t GetHWP()       const { return fHalfWavePlate;}
      Double_t GetTargetPol() const { return fPt;}
      Double_t GetBeamPol()   const { return fPb;}
      Double_t GetChargeNormalization() const { return fCharge[1]/fCharge[0] ; }
      Double_t GetLiveTime(int i = 0) { if( i< 2 && i>=0) return fLiveTime[i]; else return(0);} 
      Double_t GetCharge(int i = 0) { if( i< 2 && i>=0) return fCharge[i]; else return (0);} 
      Int_t    GetRunNumber() const { return fRunNumber; }

      void SetRunNumber(Int_t run ) { fRunNumber = run; }
      void SetTargetPol(Double_t p) { fPt = p; }
      void SetBeamPol(Double_t p)   { fPb = p; }
      void SetDilution(Double_t p)  { fDilution = p; }
      void SetHWP(Double_t p)       { fHalfWavePlate = p; }
      void SetCharge(Double_t q1, Double_t q2) { fCharge[0] = q1; fCharge[1] = q2; }
      void SetLiveTime(Double_t l1 , Double_t l2 ) { fLiveTime[0] = l1; fLiveTime[1] = l2; }
      void SetRunSummary(InSANERunSummary* rs) {
         if(rs)  {
            fHalfWavePlate = rs->fHalfWavePlate;
            if(fHalfWavePlate) fChargeSwap = true;
            else fChargeSwap = false;
            if(rs->fRuns.size()>0)fRunNumber = rs->fRuns.at(0);
            if(fChargeSwap) {
               fCharge[1] = rs->fQPlus;
               fCharge[0] = rs->fQMinus;
               fLiveTime[1] = rs->fLPlus;
               fLiveTime[0] = rs->fLMinus;
            }else{
               fCharge[0] = rs->fQPlus;
               fCharge[1] = rs->fQMinus;
               fLiveTime[0] = rs->fLPlus;
               fLiveTime[1] = rs->fLMinus;
            }
            fPb = rs->fPBeam*rs->fPbSign/100.0;
            fPt = rs->fPTarget*rs->fPtSign/100.0;
         }
      }

      Double_t GetMeanQ2(){ if(fQ2Hist) return( fQ2Hist->GetMean());  return(0); }
      Double_t GetMeanx() { if(fxHist)  return( fxHist->GetMean() );  return(0); }
      Double_t GetMeanW() { if(fWHist)  return( fWHist->GetMean() );  return(0); }

      TH1F                 * fQ2Hist; //->
      TH1F                 * fxHist;  //->
      TH1F                 * fWHist;  //->
      
      ClassDef(InSANERawAsymmetry,3)
};
#endif

