#ifndef AnalyzerToUncorrected_h
#define AnalyzerToUncorrected_h

/**
 * This TSelector grabs the raw data from a chain of root files which were converted from
 * cernlib/paw ntuples. In particular, I have added my own flag on the analyzer which
 * is " sane_ntuple_type = 3 ". It is called Whit's raw ntuple in the modified fortran code
 * and produces a tree after h2root conversion named "h9503".
 *
 * The tree betaDetectors is created and contains a (split) branch of class BETAEvent, which
 *  contains the four individual detector subclasses, BigcalEvent, LuciteHodoscopeEvent,
 *  GasCherenkovEvent and ForwardTrackerEvent.
 *
 *  In addition to the detectors, a tree, "hallcBeam", of class HallCBeamEvent is created with
 *  primary branch named "hallcBeamEvent".
 *
 */
#include "InSANEScalerEvent.h"
#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTree.h>
#include <BETAEvent.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <BIGCALGeometryCalculator.h>
#include <TClonesArray.h>
#include <BigcalEvent.h>
#include <BigcalHit.h>
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include  "HallCBeamEvent.h"
#include  "InSANEMonteCarloEvent.h"
#include  "HMSEvent.h"
#include  "InSANETriggerEvent.h"
#include <exception>
#include  "SANEEvents.h"

class AnalyzerToUncorrected : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   SANEEvents * fEvents;

   InSANERun * fRun;

   Int_t currentRunNumber;
   Int_t currentTreeNumber;

   BETAEvent * aBetaEvent;
   BigcalEvent * bcEvent;
   LuciteHodoscopeEvent * lhEvent;
   GasCherenkovEvent * gcEvent;
   ForwardTrackerEvent * ftEvent;

   HallCBeamEvent * beamEvent;

   HMSEvent * hmsEvent;

   InSANEScalerEvent * scalerEvent;

   BETAG4MonteCarloEvent * mcEvent;

   InSANETriggerEvent * trigEvent;

   BIGCALGeometryCalculator * bcgeo;

   TFile * fFileOut;
   TTree * fTreeOut;

   // NOMOREHANDWRITTENCODE
   Double_t        tcharge;
   Double_t        charge2s;
   Double_t        tcharge_help;
   Double_t        charge2s_help;
   Double_t        tcharge_helm;
   Double_t        charge2s_helm;
   Double_t        polarea;
   Double_t        polarization;
   Int_t           hel_p_scaler;
   Int_t           hel_n_scaler;
   Int_t           hel_p_trig;
   Int_t           hel_n_trig;
   Double_t        dtime_p;
   Double_t        dtime_n;
   Float_t         half_plate;
   Int_t           bgid;
   Int_t           bgtype;
   Int_t           btrigtype;
   Int_t           ngooda;
   Int_t           ngoodt;
   Int_t           ngoodta;
   Int_t           ngoodtt;
   Int_t           irowmax;
   Int_t           icolmax;
   Float_t         max_adc;
   Int_t           y1t_hit;
   Int_t           y1t_row[900];   //[y1t_hit]
   Int_t           y1t_tdc[900];   //[y1t_hit]
   Float_t         y1t_y[900];   //[y1t_hit]
   Int_t           y2t_hit;
   Int_t           y2t_row[900];   //[y2t_hit]
   Int_t           y2t_tdc[900];   //[y2t_hit]
   Float_t         y2t_y[900];   //[y2t_hit]
   Int_t           x1t_hit;
   Int_t           x1t_row[900];   //[x1t_hit]
   Int_t           x1t_tdc[900];   //[x1t_hit]
   Float_t         x1t_x[900];   //[x1t_hit]
   Int_t           cer_hit;
   Int_t           cer_num[50];   //[cer_hit]
   Int_t           cer_tdc[50];   //[cer_hit]
   Int_t           cer_adcc[50];   //[cer_hit]
   Int_t           ceradc_hit;
   Int_t           ceradc_num[15];   //[ceradc_hit]
   Int_t           cer_adc[15];   //[ceradc_hit]
   Int_t           luc_hit;
   Int_t           luc_row[90];   //[luc_hit]
   Int_t           ladc_pos[90];   //[luc_hit]
   Int_t           ladc_neg[90];   //[luc_hit]
   Int_t           ltdc_pos[90];   //[luc_hit]
   Int_t           ltdc_neg[90];   //[luc_hit]
   Float_t         luc_y[90];   //[luc_hit]
   Int_t           Bigcal_tdc_nhit;
   Int_t           Bigcal_tdc_raw_igroup[1792];   //[Bigcal_tdc_nhit]
   Int_t           Bigcal_tdc_raw_irow[1792];   //[Bigcal_tdc_nhit]
   Int_t           Bigcal_tdc_raw[1792];   //[Bigcal_tdc_nhit]
   Int_t           Bigcal_prot_nhit;
   Int_t           Bigcal_prot_ix[1024];   //[Bigcal_prot_nhit]
   Int_t           Bigcal_prot_iy[1024];   //[Bigcal_prot_nhit]
   Int_t           Bigcal_prot_adc_raw[1024];   //[Bigcal_prot_nhit]
   Int_t           Bigcal_rcs_nhit;
   Int_t           Bigcal_rcs_ix[720];   //[Bigcal_rcs_nhit]
   Int_t           Bigcal_rcs_iy[720];   //[Bigcal_rcs_nhit]
   Int_t           Bigcal_rcs_adc_raw[720];   //[Bigcal_rcs_nhit]
   Int_t           Bigcal_atrig_nhit;
   Int_t           Bigcal_atrig_igroup[38];   //[Bigcal_atrig_nhit]
   Int_t           Bigcal_atrig_ihalf[38];   //[Bigcal_atrig_nhit]
   Int_t           Bigcal_atrig_adc_raw[38];   //[Bigcal_atrig_nhit]
   Int_t           Bigcal_ttrig_nhit;
   Int_t           Bigcal_ttrig_igroup[336];   //[Bigcal_ttrig_nhit]
   Int_t           Bigcal_ttrig_ihalf[336];   //[Bigcal_ttrig_nhit]
   Int_t           Bigcal_ttrig_tdc_raw[336];   //[Bigcal_ttrig_nhit]
   Int_t           gen_event_id_number;
   Int_t           gen_event_type;
   Int_t           gen_event_class;
   Int_t           gen_event_roc_summary;
   Int_t           gen_event_sequence_n;
   Int_t           gen_event_trigtype[12];
   Double_t        gbcm1_gain;
   Double_t        gbcm2_gain;
   Double_t        gbcm3_gain;
   Double_t        gunser_gain;
   Double_t        gbcm2_offset;
   Double_t        gbcm3_offset;
   Double_t        gunser_offset;
   Double_t        gbcm1_charge;
   Double_t        gbcm2_charge;
   Double_t        gbcm3_charge;
   Double_t        gunser_charge;
   Double_t        gbcm1_charge_help;
   Double_t        gbcm1_charge_helm;
   Double_t        gbcm2_charge_help;
   Double_t        gbcm2_charge_helm;
   Double_t        g_beam_on_bcm_charge_help[2];
   Int_t           g_beam_on_thresh_cur[2];
   Int_t           gbcm2_index;
   Int_t           gbcm3_index;
   Int_t           gunser_index;
   Int_t           bcm_for_threshold_cut;
   Int_t           gscaler_event_num;
   Float_t         hms_p;
   Float_t         hms_e;
   Float_t         hms_theta;
   Float_t         hms_phi;
   Float_t         hsxfp_s;
   Float_t         hsyfp_s;
   Float_t         hsxpfp_s;
   Float_t         hsypfp_s;
   Float_t         hms_xtar;
   Float_t         hms_ytar;
   Float_t         hms_yptar;
   Float_t         hms_xptar;
   Float_t         hms_delta;
   Float_t         hms_start;
   Float_t         hsshtrk_s;
   Float_t         hsshsum_s;
   Float_t         hsbeta_s;
   Float_t         rast_x;
   Float_t         rast_y;
   Float_t         slow_rast_x;
   Float_t         slow_rast_y;
   Float_t         sem_x;
   Float_t         sem_y;
   Int_t           i_helicity;
   Float_t         hms_cer_npe1;
   Float_t         hms_cer_npe2;
   Float_t         hms_cer_adc1;
   Float_t         hms_cer_adc2;
   Float_t         T_trghms;
   Float_t         T_trgbig;
   Float_t         T_trgpi0;
   Float_t         T_trgbeta;
   Float_t         T_trgcoin1;
   Float_t         T_trgcoin2;

   // List of branches
   TBranch        *b_tcharge;   //!
   TBranch        *b_charge2s;   //!
   TBranch        *b_tcharge_help;   //!
   TBranch        *b_charge2s_help;   //!
   TBranch        *b_tcharge_helm;   //!
   TBranch        *b_charge2s_helm;   //!
   TBranch        *b_polarea;   //!
   TBranch        *b_polarization;   //!
   TBranch        *b_hel_p_scaler;   //!
   TBranch        *b_hel_n_scaler;   //!
   TBranch        *b_hel_p_trig;   //!
   TBranch        *b_hel_n_trig;   //!
   TBranch        *b_dtime_p;   //!
   TBranch        *b_dtime_n;   //!
   TBranch        *b_half_plate;   //!
   TBranch        *b_bgid;   //!
   TBranch        *b_bgtype;   //!
   TBranch        *b_btrigtype;   //!
   TBranch        *b_ngooda;   //!
   TBranch        *b_ngoodt;   //!
   TBranch        *b_ngoodta;   //!
   TBranch        *b_ngoodtt;   //!
   TBranch        *b_irowmax;   //!
   TBranch        *b_icolmax;   //!
   TBranch        *b_max_adc;   //!
   TBranch        *b_y1t_hit;   //!
   TBranch        *b_y1t_row;   //!
   TBranch        *b_y1t_tdc;   //!
   TBranch        *b_y1t_y;   //!
   TBranch        *b_y2t_hit;   //!
   TBranch        *b_y2t_row;   //!
   TBranch        *b_y2t_tdc;   //!
   TBranch        *b_y2t_y;   //!
   TBranch        *b_x1t_hit;   //!
   TBranch        *b_x1t_row;   //!
   TBranch        *b_x1t_tdc;   //!
   TBranch        *b_x1t_x;   //!
   TBranch        *b_cer_hit;   //!
   TBranch        *b_cer_num;   //!
   TBranch        *b_cer_tdc;   //!
   TBranch        *b_cer_adcc;   //!
   TBranch        *b_ceradc_hit;   //!
   TBranch        *b_ceradc_num;   //!
   TBranch        *b_cer_adc;   //!
   TBranch        *b_luc_hit;   //!
   TBranch        *b_luc_row;   //!
   TBranch        *b_ladc_pos;   //!
   TBranch        *b_ladc_neg;   //!
   TBranch        *b_ltdc_pos;   //!
   TBranch        *b_ltdc_neg;   //!
   TBranch        *b_luc_y;   //!
   TBranch        *b_Bigcal_tdc_nhit;   //!
   TBranch        *b_Bigcal_tdc_raw_igroup;   //!
   TBranch        *b_Bigcal_tdc_raw_irow;   //!
   TBranch        *b_Bigcal_tdc_raw;   //!
   TBranch        *b_Bigcal_prot_nhit;   //!
   TBranch        *b_Bigcal_prot_ix;   //!
   TBranch        *b_Bigcal_prot_iy;   //!
   TBranch        *b_Bigcal_prot_adc_raw;   //!
   TBranch        *b_Bigcal_rcs_nhit;   //!
   TBranch        *b_Bigcal_rcs_ix;   //!
   TBranch        *b_Bigcal_rcs_iy;   //!
   TBranch        *b_Bigcal_rcs_adc_raw;   //!
   TBranch        *b_Bigcal_atrig_nhit;   //!
   TBranch        *b_Bigcal_atrig_igroup;   //!
   TBranch        *b_Bigcal_atrig_ihalf;   //!
   TBranch        *b_Bigcal_atrig_adc_raw;   //!
   TBranch        *b_Bigcal_ttrig_nhit;   //!
   TBranch        *b_Bigcal_ttrig_igroup;   //!
   TBranch        *b_Bigcal_ttrig_ihalf;   //!
   TBranch        *b_Bigcal_ttrig_tdc_raw;   //!
   TBranch        *b_gen_event_id_number;   //!
   TBranch        *b_gen_event_type;   //!
   TBranch        *b_gen_event_class;   //!
   TBranch        *b_gen_event_roc_summary;   //!
   TBranch        *b_gen_event_sequence_n;   //!
   TBranch        *b_gen_event_trigtype;   //!
   TBranch        *b_gbcm1_gain;   //!
   TBranch        *b_gbcm2_gain;   //!
   TBranch        *b_gbcm3_gain;   //!
   TBranch        *b_gunser_gain;   //!
   TBranch        *b_gbcm2_offset;   //!
   TBranch        *b_gbcm3_offset;   //!
   TBranch        *b_gunser_offset;   //!
   TBranch        *b_gbcm1_charge;   //!
   TBranch        *b_gbcm2_charge;   //!
   TBranch        *b_gbcm3_charge;   //!
   TBranch        *b_gunser_charge;   //!
   TBranch        *b_gbcm1_charge_help;   //!
   TBranch        *b_gbcm1_charge_helm;   //!
   TBranch        *b_gbcm2_charge_help;   //!
   TBranch        *b_gbcm2_charge_helm;   //!
   TBranch        *b_g_beam_on_bcm_charge_help;   //!
   TBranch        *b_g_beam_on_thresh_cur;   //!
   TBranch        *b_gbcm2_index;   //!
   TBranch        *b_gbcm3_index;   //!
   TBranch        *b_gunser_index;   //!
   TBranch        *b_bcm_for_threshold_cut;   //!
   TBranch        *b_gscaler_event_num;   //!
   TBranch        *b_hms_p;   //!
   TBranch        *b_hms_e;   //!
   TBranch        *b_hms_theta;   //!
   TBranch        *b_hms_phi;   //!
   TBranch        *b_hsxfp_s;   //!
   TBranch        *b_hsyfp_s;   //!
   TBranch        *b_hsxpfp_s;   //!
   TBranch        *b_hsypfp_s;   //!
   TBranch        *b_hms_xtar;   //!
   TBranch        *b_hms_ytar;   //!
   TBranch        *b_hms_yptar;   //!
   TBranch        *b_hms_xptar;   //!
   TBranch        *b_hms_delta;   //!
   TBranch        *b_hms_start;   //!
   TBranch        *b_hsshtrk_s;   //!
   TBranch        *b_hsshsum_s;   //!
   TBranch        *b_hsbeta_s;   //!
   TBranch        *b_rast_x;   //!
   TBranch        *b_rast_y;   //!
   TBranch        *b_slow_rast_x;   //!
   TBranch        *b_slow_rast_y;   //!
   TBranch        *b_sem_x;   //!
   TBranch        *b_sem_y;   //!
   TBranch        *b_i_helicity;   //!
   TBranch        *b_hms_cer_npe1;   //!
   TBranch        *b_hms_cer_npe2;   //!
   TBranch        *b_hms_cer_adc1;   //!
   TBranch        *b_hms_cer_adc2;   //!
   TBranch        *b_T_trghms;   //!
   TBranch        *b_T_trgbig;   //!
   TBranch        *b_T_trgpi0;   //!
   TBranch        *b_T_trgbeta;   //!
   TBranch        *b_T_trgcoin1;   //!
   TBranch        *b_T_trgcoin2;   //!

   AnalyzerToUncorrected(TTree * /*tree*/ = 0) { }
   virtual ~AnalyzerToUncorrected() { }
   virtual Int_t   Version() const {
      return 2;
   }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) {
      return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0;
   }
   virtual void    SetOption(const char *option) {
      fOption = option;
   }
   virtual void    SetObject(TObject *obj) {
      fObject = obj;
   }
   virtual void    SetInputList(TList *input) {
      fInput = input;
   }
   virtual TList  *GetOutputList() const {
      return fOutput;
   }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(AnalyzerToUncorrected, 0);
};

#endif

#ifdef AnalyzerToUncorrected_cxx
void AnalyzerToUncorrected::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("tcharge", &tcharge, &b_tcharge);
   fChain->SetBranchAddress("charge2s", &charge2s, &b_charge2s);
   fChain->SetBranchAddress("tcharge_help", &tcharge_help, &b_tcharge_help);
   fChain->SetBranchAddress("charge2s_help", &charge2s_help, &b_charge2s_help);
   fChain->SetBranchAddress("tcharge_helm", &tcharge_helm, &b_tcharge_helm);
   fChain->SetBranchAddress("charge2s_helm", &charge2s_helm, &b_charge2s_helm);
   fChain->SetBranchAddress("polarea", &polarea, &b_polarea);
   fChain->SetBranchAddress("polarization", &polarization, &b_polarization);
   fChain->SetBranchAddress("hel_p_scaler", &hel_p_scaler, &b_hel_p_scaler);
   fChain->SetBranchAddress("hel_n_scaler", &hel_n_scaler, &b_hel_n_scaler);
   fChain->SetBranchAddress("hel_p_trig", &hel_p_trig, &b_hel_p_trig);
   fChain->SetBranchAddress("hel_n_trig", &hel_n_trig, &b_hel_n_trig);
   fChain->SetBranchAddress("dtime_p", &dtime_p, &b_dtime_p);
   fChain->SetBranchAddress("dtime_n", &dtime_n, &b_dtime_n);
   fChain->SetBranchAddress("half_plate", &half_plate, &b_half_plate);
   fChain->SetBranchAddress("bgid", &bgid, &b_bgid);
   fChain->SetBranchAddress("bgtype", &bgtype, &b_bgtype);
   fChain->SetBranchAddress("btrigtype", &btrigtype, &b_btrigtype);
   fChain->SetBranchAddress("ngooda", &ngooda, &b_ngooda);
   fChain->SetBranchAddress("ngoodt", &ngoodt, &b_ngoodt);
   fChain->SetBranchAddress("ngoodta", &ngoodta, &b_ngoodta);
   fChain->SetBranchAddress("ngoodtt", &ngoodtt, &b_ngoodtt);
   fChain->SetBranchAddress("irowmax", &irowmax, &b_irowmax);
   fChain->SetBranchAddress("icolmax", &icolmax, &b_icolmax);
   fChain->SetBranchAddress("max_adc", &max_adc, &b_max_adc);
   fChain->SetBranchAddress("y1t_hit", &y1t_hit, &b_y1t_hit);
   fChain->SetBranchAddress("y1t_row", y1t_row, &b_y1t_row);
   fChain->SetBranchAddress("y1t_tdc", y1t_tdc, &b_y1t_tdc);
   fChain->SetBranchAddress("y1t_y", y1t_y, &b_y1t_y);
   fChain->SetBranchAddress("y2t_hit", &y2t_hit, &b_y2t_hit);
   fChain->SetBranchAddress("y2t_row", y2t_row, &b_y2t_row);
   fChain->SetBranchAddress("y2t_tdc", y2t_tdc, &b_y2t_tdc);
   fChain->SetBranchAddress("y2t_y", y2t_y, &b_y2t_y);
   fChain->SetBranchAddress("x1t_hit", &x1t_hit, &b_x1t_hit);
   fChain->SetBranchAddress("x1t_row", x1t_row, &b_x1t_row);
   fChain->SetBranchAddress("x1t_tdc", x1t_tdc, &b_x1t_tdc);
   fChain->SetBranchAddress("x1t_x", x1t_x, &b_x1t_x);
   fChain->SetBranchAddress("cer_hit", &cer_hit, &b_cer_hit);
   fChain->SetBranchAddress("cer_num", cer_num, &b_cer_num);
   fChain->SetBranchAddress("cer_tdc", cer_tdc, &b_cer_tdc);
   fChain->SetBranchAddress("cer_adcc", cer_adcc, &b_cer_adcc);
   fChain->SetBranchAddress("ceradc_hit", &ceradc_hit, &b_ceradc_hit);
   fChain->SetBranchAddress("ceradc_num", ceradc_num, &b_ceradc_num);
   fChain->SetBranchAddress("cer_adc", cer_adc, &b_cer_adc);
   fChain->SetBranchAddress("luc_hit", &luc_hit, &b_luc_hit);
   fChain->SetBranchAddress("luc_row", luc_row, &b_luc_row);
   fChain->SetBranchAddress("ladc_pos", ladc_pos, &b_ladc_pos);
   fChain->SetBranchAddress("ladc_neg", ladc_neg, &b_ladc_neg);
   fChain->SetBranchAddress("ltdc_pos", ltdc_pos, &b_ltdc_pos);
   fChain->SetBranchAddress("ltdc_neg", ltdc_neg, &b_ltdc_neg);
   fChain->SetBranchAddress("luc_y", luc_y, &b_luc_y);
   fChain->SetBranchAddress("Bigcal_tdc_nhit", &Bigcal_tdc_nhit, &b_Bigcal_tdc_nhit);
   fChain->SetBranchAddress("Bigcal_tdc_raw_igroup", Bigcal_tdc_raw_igroup, &b_Bigcal_tdc_raw_igroup);
   fChain->SetBranchAddress("Bigcal_tdc_raw_irow", Bigcal_tdc_raw_irow, &b_Bigcal_tdc_raw_irow);
   fChain->SetBranchAddress("Bigcal_tdc_raw", Bigcal_tdc_raw, &b_Bigcal_tdc_raw);
   fChain->SetBranchAddress("Bigcal_prot_nhit", &Bigcal_prot_nhit, &b_Bigcal_prot_nhit);
   fChain->SetBranchAddress("Bigcal_prot_ix", Bigcal_prot_ix, &b_Bigcal_prot_ix);
   fChain->SetBranchAddress("Bigcal_prot_iy", Bigcal_prot_iy, &b_Bigcal_prot_iy);
   fChain->SetBranchAddress("Bigcal_prot_adc_raw", Bigcal_prot_adc_raw, &b_Bigcal_prot_adc_raw);
   fChain->SetBranchAddress("Bigcal_rcs_nhit", &Bigcal_rcs_nhit, &b_Bigcal_rcs_nhit);
   fChain->SetBranchAddress("Bigcal_rcs_ix", Bigcal_rcs_ix, &b_Bigcal_rcs_ix);
   fChain->SetBranchAddress("Bigcal_rcs_iy", Bigcal_rcs_iy, &b_Bigcal_rcs_iy);
   fChain->SetBranchAddress("Bigcal_rcs_adc_raw", Bigcal_rcs_adc_raw, &b_Bigcal_rcs_adc_raw);
   fChain->SetBranchAddress("Bigcal_atrig_nhit", &Bigcal_atrig_nhit, &b_Bigcal_atrig_nhit);
   fChain->SetBranchAddress("Bigcal_atrig_igroup", Bigcal_atrig_igroup, &b_Bigcal_atrig_igroup);
   fChain->SetBranchAddress("Bigcal_atrig_ihalf", Bigcal_atrig_ihalf, &b_Bigcal_atrig_ihalf);
   fChain->SetBranchAddress("Bigcal_atrig_adc_raw", Bigcal_atrig_adc_raw, &b_Bigcal_atrig_adc_raw);
   fChain->SetBranchAddress("Bigcal_ttrig_nhit", &Bigcal_ttrig_nhit, &b_Bigcal_ttrig_nhit);
   fChain->SetBranchAddress("Bigcal_ttrig_igroup", Bigcal_ttrig_igroup, &b_Bigcal_ttrig_igroup);
   fChain->SetBranchAddress("Bigcal_ttrig_ihalf", Bigcal_ttrig_ihalf, &b_Bigcal_ttrig_ihalf);
   fChain->SetBranchAddress("Bigcal_ttrig_tdc_raw", Bigcal_ttrig_tdc_raw, &b_Bigcal_ttrig_tdc_raw);
   fChain->SetBranchAddress("gen_event_id_number", &gen_event_id_number, &b_gen_event_id_number);
   fChain->SetBranchAddress("gen_event_type", &gen_event_type, &b_gen_event_type);
   fChain->SetBranchAddress("gen_event_class", &gen_event_class, &b_gen_event_class);
   fChain->SetBranchAddress("gen_event_roc_summary", &gen_event_roc_summary, &b_gen_event_roc_summary);
   fChain->SetBranchAddress("gen_event_sequence_n", &gen_event_sequence_n, &b_gen_event_sequence_n);
   fChain->SetBranchAddress("gen_event_trigtype", gen_event_trigtype, &b_gen_event_trigtype);
   fChain->SetBranchAddress("gbcm1_gain", &gbcm1_gain, &b_gbcm1_gain);
   fChain->SetBranchAddress("gbcm2_gain", &gbcm2_gain, &b_gbcm2_gain);
   fChain->SetBranchAddress("gbcm3_gain", &gbcm3_gain, &b_gbcm3_gain);
   fChain->SetBranchAddress("gunser_gain", &gunser_gain, &b_gunser_gain);
   fChain->SetBranchAddress("gbcm2_offset", &gbcm2_offset, &b_gbcm2_offset);
   fChain->SetBranchAddress("gbcm3_offset", &gbcm3_offset, &b_gbcm3_offset);
   fChain->SetBranchAddress("gunser_offset", &gunser_offset, &b_gunser_offset);
   fChain->SetBranchAddress("gbcm1_charge", &gbcm1_charge, &b_gbcm1_charge);
   fChain->SetBranchAddress("gbcm2_charge", &gbcm2_charge, &b_gbcm2_charge);
   fChain->SetBranchAddress("gbcm3_charge", &gbcm3_charge, &b_gbcm3_charge);
   fChain->SetBranchAddress("gunser_charge", &gunser_charge, &b_gunser_charge);
   fChain->SetBranchAddress("gbcm1_charge_help", &gbcm1_charge_help, &b_gbcm1_charge_help);
   fChain->SetBranchAddress("gbcm1_charge_helm", &gbcm1_charge_helm, &b_gbcm1_charge_helm);
   fChain->SetBranchAddress("gbcm2_charge_help", &gbcm2_charge_help, &b_gbcm2_charge_help);
   fChain->SetBranchAddress("gbcm2_charge_helm", &gbcm2_charge_helm, &b_gbcm2_charge_helm);
   fChain->SetBranchAddress("g_beam_on_bcm_charge_help", g_beam_on_bcm_charge_help, &b_g_beam_on_bcm_charge_help);
   fChain->SetBranchAddress("g_beam_on_thresh_cur", g_beam_on_thresh_cur, &b_g_beam_on_thresh_cur);
   fChain->SetBranchAddress("gbcm2_index", &gbcm2_index, &b_gbcm2_index);
   fChain->SetBranchAddress("gbcm3_index", &gbcm3_index, &b_gbcm3_index);
   fChain->SetBranchAddress("gunser_index", &gunser_index, &b_gunser_index);
   fChain->SetBranchAddress("bcm_for_threshold_cut", &bcm_for_threshold_cut, &b_bcm_for_threshold_cut);
   fChain->SetBranchAddress("gscaler_event_num", &gscaler_event_num, &b_gscaler_event_num);
   fChain->SetBranchAddress("hms_p", &hms_p, &b_hms_p);
   fChain->SetBranchAddress("hms_e", &hms_e, &b_hms_e);
   fChain->SetBranchAddress("hms_theta", &hms_theta, &b_hms_theta);
   fChain->SetBranchAddress("hms_phi", &hms_phi, &b_hms_phi);
   fChain->SetBranchAddress("hsxfp_s", &hsxfp_s, &b_hsxfp_s);
   fChain->SetBranchAddress("hsyfp_s", &hsyfp_s, &b_hsyfp_s);
   fChain->SetBranchAddress("hsxpfp_s", &hsxpfp_s, &b_hsxpfp_s);
   fChain->SetBranchAddress("hsypfp_s", &hsypfp_s, &b_hsypfp_s);
   fChain->SetBranchAddress("hms_xtar", &hms_xtar, &b_hms_xtar);
   fChain->SetBranchAddress("hms_ytar", &hms_ytar, &b_hms_ytar);
   fChain->SetBranchAddress("hms_yptar", &hms_yptar, &b_hms_yptar);
   fChain->SetBranchAddress("hms_xptar", &hms_xptar, &b_hms_xptar);
   fChain->SetBranchAddress("hms_delta", &hms_delta, &b_hms_delta);
   fChain->SetBranchAddress("hms_start", &hms_start, &b_hms_start);
   fChain->SetBranchAddress("hsshtrk_s", &hsshtrk_s, &b_hsshtrk_s);
   fChain->SetBranchAddress("hsshsum_s", &hsshsum_s, &b_hsshsum_s);
   fChain->SetBranchAddress("hsbeta_s", &hsbeta_s, &b_hsbeta_s);
   fChain->SetBranchAddress("rast_x", &rast_x, &b_rast_x);
   fChain->SetBranchAddress("rast_y", &rast_y, &b_rast_y);
   fChain->SetBranchAddress("slow_rast_x", &slow_rast_x, &b_slow_rast_x);
   fChain->SetBranchAddress("slow_rast_y", &slow_rast_y, &b_slow_rast_y);
   fChain->SetBranchAddress("sem_x", &sem_x, &b_sem_x);
   fChain->SetBranchAddress("sem_y", &sem_y, &b_sem_y);
   fChain->SetBranchAddress("i_helicity", &i_helicity, &b_i_helicity);
   fChain->SetBranchAddress("hms_cer_npe1", &hms_cer_npe1, &b_hms_cer_npe1);
   fChain->SetBranchAddress("hms_cer_npe2", &hms_cer_npe2, &b_hms_cer_npe2);
   fChain->SetBranchAddress("hms_cer_adc1", &hms_cer_adc1, &b_hms_cer_adc1);
   fChain->SetBranchAddress("hms_cer_adc2", &hms_cer_adc2, &b_hms_cer_adc2);
   fChain->SetBranchAddress("T_trghms", &T_trghms, &b_T_trghms);
   fChain->SetBranchAddress("T_trgbig", &T_trgbig, &b_T_trgbig);
   fChain->SetBranchAddress("T_trgpi0", &T_trgpi0, &b_T_trgpi0);
   fChain->SetBranchAddress("T_trgbeta", &T_trgbeta, &b_T_trgbeta);
   fChain->SetBranchAddress("T_trgcoin1", &T_trgcoin1, &b_T_trgcoin1);
   fChain->SetBranchAddress("T_trgcoin2", &T_trgcoin2, &b_T_trgcoin2);
}

Bool_t AnalyzerToUncorrected::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.
   printf(" Opening Tree # %d\n", ((TChain *)fChain)->GetTreeNumber());

   return kTRUE;
}

#endif // #ifdef AnalyzerToUncorrected_cxx
