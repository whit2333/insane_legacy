#ifndef SANEEvents_H
#define SANEEvents_H
#include "TROOT.h"
#include "TChain.h"
#include "InSANEAnalysisEvents.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "InSANETriggerEvent.h"
#include "BETAG4MonteCarloEvent.h"
#include "InSANEClusterEvent.h"

/** Concrete class for SANE Events which is used as a simplified
 * interface to the Event classes with file I/O.
 *
 * \note { This class should not be streamed to a tree!! }
 *
 * \ingroup Analysis
 */
class SANEEvents : public InSANEAnalysisEvents {

   public:
      Int_t                   fRunNumber;
      Int_t                   fEventNumber;
      HMSEvent              * HMS;
      HallCBeamEvent        * BEAM;
      BETAEvent             * BETA;
      InSANETriggerEvent    * TRIG;
      BETAG4MonteCarloEvent * MC;
      InSANEClusterEvent    * CLUSTER;
      TFile                 * fAnalysisFile;
      TChain                * fAnalysisTree;
      TTree                 * fTree;



      /**  Allocates memory for detector events.
       *   Finds the trees in the currently opened files and
       *   sets their branch addresses.
       */
      SANEEvents(const char * treename = "betaDetectors");

      /** Construct with a TChain.
       *  The TChain name is used for the treename
       */
      SANEEvents(TChain * chain);

      virtual ~SANEEvents();

      /**  For each input tree it sets the appropriate Branches.  */
      Int_t SetBranches();

      /**  Sets the branches for the tchain. Note that it does not create branches because
       *   it is a chain and the branches are assumed to exist.
       */
      Int_t SetBranches(TChain * chain);

      /**  For each input tree, tree->GetEvent(#) is called. Returns 0 on success.
       */
      virtual Int_t GetEvent(Long64_t);

      void FillTrees() ;

      InSANEClusterEvent *  AddClusterEvents();   ///< Uses fTree with SetClusterBranches(TTree*) to set the cluster branches

      InSANEClusterEvent *  SetClusterBranches(TTree * aTree); 
      InSANEClusterEvent *  SetClusterBranches(TChain * chain);

      virtual void ClearEvent(Option_t * opt = "") ;
      void         SetEventNumber(Int_t num);
      void         SetRunNumber(Int_t num); 
      void         Print(Option_t * opt = "") const ;

      ClassDef(SANEEvents, 4)
};


#endif
