#ifndef InSANERunManager_HH
#define InSANERunManager_HH 1

#include <iostream>
#include <map>
#include <utility>

#include "TFile.h"
#include "TRandom3.h"
#include "TSystem.h"

//class BIGCALGeometryCalculator;
//class LuciteGeometryCalculator;
//class GasCherenkovGeometryCalculator;
//class ForwardTrackerGeometryCalculator;

//#include "InSANEDatabaseManager.h"
#include "InSANERunReport.h"
#include "InSANERun.h"
#include "InSANETarget.h"

/*! \page Runinfo Run Information documentation page

     \section gatheringRunInfo Gathering Initial Run Information
     The first use of InSANERunManager::SetRun(#), which calls InSANERunManager::FindRun() 
     to search for
     a saved run object, creates a new InSANERun object if none is found. After creating a new
     run object InSANERunManager::GeneratePreRunReport() is called.

     Within InSANERunManager::GeneratePreRunReport() a new "Run Report" is created. 
     A "Run Report" or InSANERunReport is a transient object that is not saved and is 
     used to facilitate the transfer of data to and from the InSANERun object. 
     The InSANERunReport has a list of InSANERunReportDatum objects which represent a 
     column in the database. For example, in the GeneratePreRunReport you will find a 
     line like    fRunReport->Add( new InSANERunReportDatum("beam_energy") ) for the database 
     column "beam_energy".

     Since GeneratePreRunReport() is called only when the run is first created, it calls
     InSANERunReport::RetrieveRunInfo() to get existing values of each InSANERunReportDatum.
     RetrieveRunInfo() ends by executing the interpreted script GatherRunInfo.cxx which is where other sources
     of run info can be injected into the run report.

     Then InSANERunReport::UpdateRunDatabase() is called. This fills the database with
     newly gathered (from GatherRunInfo.cxx) run information.

     Finally,  InSANERunReport::UpdateRun() is called which sets the datamembers of the InSANERun object using the "Run Report"

     \section accumulatedRunInfo Accumulated Run Information
     If a run object is found, InSANERunManager::GenerateRunReport() is called (not
     InSANERunManager::GeneratePreRunReport() ). This method creates a new run report and adds
     the InSANERunReportDatum to the report. It ends by calling InSANERunReport::Update() which
     explicitly maps the Run Object's values to run report's values.

     \section runmanagerfiles File management
     The basic files are expected to be found in the directory data/rootfiles.
     Since we will have many analysis passes, some of which we will want to repeat
     without re-running all the pass before, there is a file for each pass of analysis.
     The previous pass (or input ) file will be read-only and the current pass (output)
     file will be written to.

     The "Run Manager" takes care of the files and provides access to the current analysis files
     and run information. See InSANERunManager and SANERunManager

*/

/**  Base class for a Run Manager
 *
 * It is worth noting that we can make use of a feature of C++ called a
 * covariant return type. See http://www.tolchz.net/?p=33 for a simple
 * example. Here the return type of GetRunManager() is "covariant" when inheriting
 * this base class. This allows for the singleton-type class to be inherited.
 *
 * \ingroup Runs
 * \ingroup Managers
 */
class InSANERunManager : public TObject {
   public :
      void cpp11test();

      /** Get pointer to singleton run manager class
       */
      static InSANERunManager* GetRunManager();
      virtual  ~InSANERunManager();

      /** @name File Management
       *  Manage the analysis pass and files for the run.
       * @{
       */
      /**  Set the run to be analyzed.
       *
       *  Sets the run manager's fCurrentRunNumber
       *  Opens files InSANE#run.#fileno.root and scalers#run.root
       *
       *  TFile pointers are fCurrentFile and fScalerFile
       *  TTree pointers are
       */
      Int_t          SetRun(Int_t run, Int_t pass = -1, const char * opt = "");
      Int_t          OpenRun(Int_t run);
      Int_t          CloseRun();
      Int_t          GetLastPassNumber(Int_t runnumber) const;
      Int_t          GetNumberOfPasses(Int_t runnumber) const;
      TFile *        GetPreviousFile() { return(fPreviousFile); }
      TFile *        GetCurrentFile() { return(fCurrentFile); }
      /** Set the file number or "pass".
       *  Returns 0 if all is well.
       */
      Int_t          SetPassFile(Int_t pass = 0);
      Int_t          OpenCurrentPassFile();
      Int_t          OpenPreviousPassFile();
      Int_t          CreateNewPass();

      /**  Creates the standard directory structure in the current outputfile
       *   InSANE(#).(pass).root:/
       */
      virtual void   CreateDirectories() ;

      /** CLOSE file -> increment fCurrentFileNumber -> Open file.
       */
      virtual void   CloseCurrentFile() {
         if (fCurrentFile) {
            WriteCurrentRunInfoToFile();
            //fCurrentFile->Flush();
            //fCurrentFile->Write();
            fCurrentFile->Close(/*"R"*/); // R deletes all the ProcessIDs
            fCurrentFile = nullptr;
         }
         if (fPreviousFile) {
            fPreviousFile->Close(/*"R"*/); // R deletes all the ProcessIDs
            fPreviousFile = nullptr;
         }
      }

      /** Used to reset the analysis level
       */
      void           ResetRunPass(Int_t pass = 0) {
         fCurrentRun->ResetRun(pass);
         fRunReport->UpdateRunDatabase();
      }

      /** \deprecated
       */
      Int_t          WriteCurrentRunInfoToFile() ;

      /** Determines if run number is set
       */
      Bool_t         IsRunSet() {
         /*    if(!fRunIsSet && fCurrentRun != 0) std::cout << "x- Run Not Set!\n";*/
         return(fRunIsSet);
      }

      /** Finds the stored InSANERun object and sets it to the fCurrentRun
       *  Setting opt="Q" sets it to quite (no printing to screen);
       */
      Int_t FindRun(const char * opt = "");

      /** Set the scaler file
       */
      Int_t SetScalers(Int_t scaler = 0);
      Int_t OpenScalers() {
         return SetScalers();
      }
      Int_t CloseScalers() {
         if (fScalerFile)  {
            fScalerFile->Close();
            fScalerFile = nullptr;
            fScalerFileName = "";
         }
         return(0);
      }
      //@}

      /**
       */
      InSANERunReport *   GetRunReport();

      /** Generates the first run report which is used to define/initialize the newly created
       *   (and  not yet  saved to  disk)
       * InSANERun object's data memebers. Should be executed when InSANERun object is first created
       *
       *  If the run object is read from a file, GenerateRunReport() is used
       */
      virtual Int_t       GeneratePreRunReport() ;

      /** virtual method  Generates a Run report Returns 0 if all is well
       */
      virtual Int_t       GenerateRunReport() ;

      /** Print various info about Manager */
      virtual void        PrintStatistics();

      /** Prints Run report */
      virtual void        PrintRunReport() {
         if (fRunReport) fRunReport->Print();
      }

      /**  Returns the pointer to the current InSANERun
       */
      InSANERun *         GetCurrentRun();
      TFile     *         GetScalerFile() {
         return(fScalerFile);
      }

      /** Updates the run report using the run object
       *  and udates the run report data in the run_info database
       */
      Int_t               WriteRunReport() ;

      /** Writes the current run object to file (replacing the old one).
       */
      Int_t       WriteRun() ;

      /** Sets the verbosity level
       *
       *  - 0 : quiet
       *  - 1 :
       *  - 2 : Prints "event" class' c'tors and d'tors, various method executions,
       *  - 3 : Prints
       *
       */
      void  SetVerbosity(Int_t verb) { fVerbosity = verb; }
      Int_t GetVerbosity() { return(fVerbosity); }

      /** returns true for target angles from 70-90 degrees. */
      Bool_t IsPerpendicularRun() ;

      /** returns true for target angles from 170-190 degrees. */
      Bool_t IsParallelRun() ;
      Bool_t IsSimulationRun() ; 

   public:
      std::map<Int_t, TFile*>   fFileMap;
      //InSANEDatabaseManager *  fDBManager;
      Int_t                    fRunNumber;         // Run number
      Int_t                    fCurrentRunNumber;  // Run number
      InSANERun *              fCurrentRun;        // Run object
      TString                  fCurrentFileName;   // Writable file name
      TString                  fPreviousFileName;  // Read-only file name
      TString                  fScalerFileName;
      Int_t                    fCurrentFileNumber; // Pass number
      Int_t                    fPreviousFileNumber;// Previous pass number
      InSANETarget * fTarget;

      /*  Int_t      fNumberOfRuns;*/
      Int_t      fNumberOfFiles;
      Int_t      fVerbosity;

      /** returns a common randomnumber generator */
      TRandom * GetRandom() {
         if (!fRandomNumberGenerator) fRandomNumberGenerator = new TRandom3(0);
         return (fRandomNumberGenerator);
      }

      virtual InSANETarget * BuildTarget(Double_t packingFraction = 0.65) { 
         return(nullptr); }

   private:
      TDirectory * timingDir;
      TDirectory * timingCerDir;
      TDirectory * timingLucDir;
      TDirectory * timingBigcalDir;
      TDirectory * timingtrackerDir;
      TDirectory * pedDir;
      TDirectory * pedCerDir;
      TDirectory * pedLucDir;
      TDirectory * pedBigcalDir;
      TDirectory * pedtrackerDir;
      TDirectory * calibCerDir;
      TDirectory * calibLucDir;
      TDirectory * calibBigcalDir;
      TDirectory * calibtrackerDir;
      TDirectory * calibDir;
      TDirectory * timewalkDir;
      TDirectory * detectorsDir;



   protected:
      /** static "global" pointer to singleton.
       */
      static InSANERunManager * fgSANERunManager;
      InSANERunManager();

      TRandom *          fRandomNumberGenerator;
      InSANERunReport *  fRunReport;
      Bool_t             fRunIsSet;

      TFile *            fScalerFile;
      TFile *            fCurrentFile;       // Writeable file
      TFile *            fPreviousFile;      // Read-only file

      ClassDef(InSANERunManager, 4)
};


#endif

