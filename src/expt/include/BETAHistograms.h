#ifndef BETAHistograms_HH
#define BETAHistograms_HH 1

#include "BETACalculation.h"
#include "TH2F.h"
#include "TH1F.h"

/** Calcultion for filling histograms at the event level.
 *  Does not include Clustering
 *
 *  \ingroup Calculations
 *  \ingroup BETA
 */
class BETAHistogramCalc000 : public BETACalculation {
public:
   BETAHistogramCalc000(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis) {
   }

   virtual ~BETAHistogramCalc000() {
   }

   /**   */
   virtual Int_t Initialize() {

      //TH2F * h2 = 0;

//       h2 = new TH2F("hTrackerVsBigcal_i","Tracker-i vs Bigcal-i",32,1,33,73,0,73);
//       f2DHistograms.Add(h2);
//       h2 = new TH2F("hTrackerVsBigcal_j","Tracker-j vs Bigcal-j",56,1,57,133,0,133);
//       f2DHistograms.Add(h2);
//
//       h2 = new TH2F("hTrackerVsBigcal_X","Tracker-X vs Bigcal-X",120,-60,60,80,-15,15);
//       f2DHistograms.Add(h2);
//       h2 = new TH2F("hTrackerVsBigcal_Y","Tracker-Y vs Bigcal-Y",120,-120,120,150,-20,20);
//       f2DHistograms.Add(h2);

      return(0);
   }

   virtual Int_t Finalize() {
      return(0);
   }

   virtual Int_t Calculate(){return(0);}

   void Describe() { }

   TList f2DHistograms;
   TList f1DHistograms;


   ClassDef(BETAHistogramCalc000, 1)
};


/** Calcultion for filling histograms at the event level.
 *  Does not include Clustering
 *
 *  \ingroup Calculations
 *  \ingroup BETA
 */
class BETAHistogramCalc0 : public BETACalculation {
public:
   BETAHistogramCalc0(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis) {
   }

   virtual ~BETAHistogramCalc0() {
   }

   /**   */
   virtual Int_t Initialize() {
      BETAHistogramCalc0::Initialize();

      //TH2F * h2 = 0;

//       h2 = new TH2F("hTrackerVsBigcal_i","Tracker-i vs Bigcal-i",32,1,33,73,0,73);
//       f2DHistograms.Add(h2);
//       h2 = new TH2F("hTrackerVsBigcal_j","Tracker-j vs Bigcal-j",56,1,57,133,0,133);
//       f2DHistograms.Add(h2);
//
//       h2 = new TH2F("hTrackerVsBigcal_X","Tracker-X vs Bigcal-X",120,-60,60,80,-15,15);
//       f2DHistograms.Add(h2);
//       h2 = new TH2F("hTrackerVsBigcal_Y","Tracker-Y vs Bigcal-Y",120,-120,120,150,-20,20);
//       f2DHistograms.Add(h2);

      return(0);
   }

   virtual Int_t Finalize() {
      return(0);
   }

   virtual Int_t Calculate();

   void Describe() { }

   TList f2DHistograms;
   TList f1DHistograms;


   ClassDef(BETAHistogramCalc0, 1)
};



/** Calcultion for filling histograms at the event level.
 *   - Includes clusters
 *
 *  \ingroup Calculations
 *  \ingroup BETA
 */
class BETAHistogramCalc1 : public BETACalculationWithClusters {
public:
   BETAHistogramCalc1(InSANEAnalysis * analysis = nullptr) : BETACalculationWithClusters(analysis) {
      SetNameTitle("BETAHistogramCalc1", "BETAHistogramCalc1");
   }

   virtual ~BETAHistogramCalc1() {
   }

   /**
    */
   virtual Int_t Initialize() {
      BETACalculationWithClusters::Initialize();

      TH2F * h2 = nullptr;

      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors/tracker");

      h2 = new TH2F("hTrackerY1VsBigcal_i", "TrackerY1-i vs Bigcal-i", 32, 1, 33, 73, 0, 73);
      f2DHistograms.Add(h2);
      h2 = new TH2F("hTrackerY2VsBigcal_i", "TrackerY2-i vs Bigcal-i", 32, 1, 33, 73, 0, 73);
      f2DHistograms.Add(h2);
      h2 = new TH2F("hTrackerY1VsBigcal_j", "TrackerY1-j vs Bigcal-j", 56, 1, 57, 133, 0, 133);
      f2DHistograms.Add(h2);
      h2 = new TH2F("hTrackerY2VsBigcal_j", "TrackerY2-j vs Bigcal-j", 56, 1, 57, 133, 0, 133);
      f2DHistograms.Add(h2);

      h2 = new TH2F("hTrackerVsBigcal_X", "Tracker-X vs Bigcal-X", 120, -60, 60, 80, -15, 15);
      f2DHistograms.Add(h2);

      h2 = new TH2F("hTrackerVsBigcal_Y", "Tracker-Y vs Bigcal-Y", 120, -120, 120, 150, -20, 20);
      f2DHistograms.Add(h2);

      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("");

      return(0);
   }

   virtual Int_t Finalize() {
      return(0);
   }

   virtual Int_t  Calculate();

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  BETAHistogramCalc1 - \n ";
      std::cout <<  "      Fills histograms from inside the event loops. \n";
      std::cout <<  "      Nested loops are not allowed in TTree::Draw() \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   TList f2DHistograms;
   TList f1DHistograms;


   ClassDef(BETAHistogramCalc1, 1)
};



/** Calcultion for filling histograms at the event level.
 *   - Includes clusters
 *
 *  \ingroup Calculations
 *  \ingroup BETA
 */
class BigcalHistogramCalc0 : public BETACalculation{
protected:
   /// Event level TDC histogram of bigcal sum of 8 groups (BigcalHit::TDCLevel==1)
   TH2I * fEventTimingHist;    //!
   TH2I * fEventTimingHistGood; //!

   /// Event level TDC histogram of bigcal  sum of 64 groups (BigcalHit::TDCLevel==3)
   TH2I * fEventTrigTimingHist; //!
   TH2I * fEventTrigTimingHistGood; //!

   TH2F * fClustersHist;  //!
   TH2F * fChargedPiClustersHist; //!
   TH2F * fElectronClustersHist; //!

   TH2F * fEventHist;  //!
   TH2F * fEventHistWithThreshold; //!

   TH1F * hADCWithoutCut[12];  //!
   TH1F * hTDCWithoutCut[NBIGCALTDC]; //!
   TH1F * hADCAlignedWithoutCut[12]; //!
   TH1F * hTDCAlignedWithoutCut[NBIGCALTDC]; //!

   TH1F * hADCWithTDCCut[12]; //!
   TH1F * hTDCWithTDCCut[NBIGCALTDC]; //!
   TH1F * hTDCAlignedWithTDCCut[NBIGCALTDC]; //!
   TH1F * hADCAlignedWithTDCCut[12]; //!

   TH1F * hADCWithADCCut[12]; //!
   TH1F * hTDCWithADCCut[NBIGCALTDC]; //!
   TH1F * hTDCAlignedWithADCCut[NBIGCALTDC]; //!
   TH1F * hADCAlignedWithADCCut[12]; //!

   TH1F * hADCWithADCandTDCCut[12]; //!
   TH1F * hTDCWithADCandTDCCut[NBIGCALTDC]; //!
   TH1F * hADCAlignedWithADCandTDCCut[12]; //!
   TH1F * hTDCAlignedWithADCandTDCCut[NBIGCALTDC]; //!

public:

   BigcalHistogramCalc0(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis) {
      SetNameTitle("BigcalHistogramCalc0", "BigcalHistogramCalc0");
   }

   virtual ~BigcalHistogramCalc0() {
   }

   /**
    */
   virtual Int_t Initialize() {
      BETACalculation::Initialize();

      TH2F * h2 = nullptr;
      //TH1F * h1 = 0;
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors/bigcal");

      h2 = new TH2F("hTrackerY1VsBigcal_i", "TrackerY1-i vs Bigcal-i", 32, 1, 33, 73, 0, 73);
      f2DHistograms.Add(h2);
      h2 = new TH2F("hTrackerY2VsBigcal_i", "TrackerY2-i vs Bigcal-i", 32, 1, 33, 73, 0, 73);
      f2DHistograms.Add(h2);
      h2 = new TH2F("hTrackerY1VsBigcal_j", "TrackerY1-j vs Bigcal-j", 56, 1, 57, 133, 0, 133);
      f2DHistograms.Add(h2);
      h2 = new TH2F("hTrackerY2VsBigcal_j", "TrackerY2-j vs Bigcal-j", 56, 1, 57, 133, 0, 133);
      f2DHistograms.Add(h2);

      h2 = new TH2F("hTrackerVsBigcal_X", "Tracker-X vs Bigcal-X", 120, -60, 60, 80, -15, 15);
      f2DHistograms.Add(h2);

      h2 = new TH2F("hTrackerVsBigcal_Y", "Tracker-Y vs Bigcal-Y", 120, -120, 120, 150, -20, 20);
      f2DHistograms.Add(h2);

      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("");

      return(0);
   }

   virtual Int_t Finalize() {
      return(0);
   }

   virtual Int_t  Calculate();

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  BETAHistogramCalc1 - \n ";
      std::cout <<  "      Fills histograms from inside the event loops. \n";
      std::cout <<  "      Nested loops are not allowed in TTree::Draw() \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   TList f2DHistograms;
   TList f1DHistograms;


   ClassDef(BigcalHistogramCalc0, 1)
};



#endif

