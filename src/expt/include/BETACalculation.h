#ifndef BETACalculation_HH
#define BETACalculation_HH 1

#include "TClonesArray.h"

#include "InSANEDatabaseManager.h"

#include "InSANECalculation.h"
#include "InSANEDetectorAnalysis.h"

#include "HMSEvent.h"
#include "SANEEvents.h"
#include "HallCBeamEvent.h"
#include "InSANETriggerEvent.h"
#include "BIGCALClusterProcessor.h"
#include "BIGCALGeometryCalculator.h"
#include "BigcalDetector.h"
#include "BigcalHit.h"
#include "InSANEClusterEvent.h"
#include "GasCherenkovDetector.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeDetector.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerDetector.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"

#include "InSANEMonteCarloEvent.h"


/** ABC for BETA Calculations.
 *  A "Calculation" is used with  an "Analyzer" to calculate new values for 
 *  output (see InSANECalculation and InSANEAnalyzer for more).
 *
 *  NOTE: The cluster event data member is set from BETACalculationWithClusters::Inititalize()
 *  so be sure to call it in inherited classes.
 *
 *  \ingroup Calculations
 *  \ingroup BETA
 */
class BETACalculation:  public InSANECalculation {
   protected:
      TClonesArray * fCopiedArray; // Temporary space for cloned objects during manipulations.

   public:
      TClonesArray            * fCerHits;
      TClonesArray            * fLucHits;
      TClonesArray            * fBigcalHits;
      TClonesArray            * fTrackerHits;
      TClonesArray            * fClusters;

      SANEEvents              * fEvents;
      BETAEvent               * aBetaEvent;
      BigcalEvent             * bcEvent;
      BigcalHit               * aBigcalHit;
      LuciteHodoscopeEvent    * lhEvent;
      LuciteHodoscopeHit      * aLucHit;
      GasCherenkovEvent       * gcEvent;
      GasCherenkovHit         * aCerHit;
      ForwardTrackerEvent     * ftEvent;
      ForwardTrackerHit       * aTrackerHit;
      InSANEClusterEvent      * fClusterEvent;
      InSANETriggerEvent      * aTrigEvent;

      ForwardTrackerDetector  * fTrackerDetector ;
      GasCherenkovDetector    * fCherenkovDetector;
      LuciteHodoscopeDetector * fHodoscopeDetector ;
      BigcalDetector          * fBigcalDetector    ;

   public:

      BETACalculation(InSANEAnalysis * analysis = nullptr);
      virtual ~BETACalculation();

      /**  Set the detectors using their instances created in the "detector analysis".
       *   This method is called by called by InSANEAnalyzer::Initialize
       *
       *   \note Add more Detectors
       *   \todo Add Detectors HERE
       */
      virtual Int_t SetDetectors(InSANEDetectorAnalysis * anAnalysis);

      /** Set the cluster event. */
      virtual Int_t SetClusterEvent(InSANEClusterEvent * ev);

      /** Initialize this calculation by setting event object pointers etc.
       *  This virtual method is called by InSANECorrector::Initialize .
       */
      virtual Int_t Initialize();

      virtual Int_t Finalize() { return(0); }

      virtual void DefineCorrection() { }
      virtual void DefineCalculation() { }

      ClassDef(BETACalculation, 1)
};


/** A BETA Calculation WITH Clusters.
 *  Neural Networks are added as data members ... perhaps this is not
 *  the best way if there are many calculations
 *
 * \ingroup Calculations
 */
class BETACalculationWithClusters : public BETACalculation {

   public:
      BIGCALClusterProcessor * fClusterProcessor; /// Cluster Processor used... must be set... \todo add check that it is set

   public:
      BETACalculationWithClusters(InSANEAnalysis * analysis = nullptr);
      virtual ~BETACalculationWithClusters();

      virtual Int_t Initialize();

      virtual void SetEvents(InSANEAnalysisEvents * e);

      /** Sets up the cluster processor used in calculations. 
       *  Optional argument provides non-standard processor.
       */
      virtual void SetClusterProcessor(BIGCALClusterProcessor * clusterProc = nullptr);

      InSANEClusterProcessor * GetClusterProcessor() const { return fClusterProcessor; }

      ClassDef(BETACalculationWithClusters, 1)
};



#endif


