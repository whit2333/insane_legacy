#ifndef InSANEDetectorGeometry_h
#define InSANEDetectorGeometry_h


#include <TROOT.h>
#include <TFile.h>
#include <TObject.h>
#include "TVector3.h"

/**  ABC for a general Detector Geometry. Concrete instances of this
 *  class should use a GeometryCalculator to make detector Geometries.
 *
 *  These geometries could use any modle thanks to VGM. (eg TGeo GEANT4 XML etc...)
 *
 * \ingroup Detectors
 *
 * \ingroup Geometry
 */
class InSANEDetectorGeometry  : public TObject {
public :
   InSANEDetectorGeometry() {
      fCenter.SetXYZ(0, 0, 0);
   }
   virtual   ~InSANEDetectorGeometry() { }

   TVector3 fCenter;


private :

   ClassDef(InSANEDetectorGeometry, 2)
};


#endif

