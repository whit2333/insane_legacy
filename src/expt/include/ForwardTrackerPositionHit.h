#ifndef ForwardTrackerPositionHit_h
#define ForwardTrackerPositionHit_h 1

#include "InSANEDetectorHit.h"
#include "TVector3.h"
#include <TFile.h>
#include <iostream>
#include "InSANERunManager.h"

/** Forward Tracker Position Hit
 *
 *  Using the TDC differences we get a horizontal position and using bar number
 *  a vertical position is determined.
 *  Every combination of hits is used when there were multiple hits per tube
 *
 * \ingroup tracker
 * \ingroup Hits
 */
class ForwardTrackerPositionHit : public InSANEDetectorHit {

   public:
      Int_t     fNNeighborHits;
      Int_t     fScintLayer;
      Int_t     fXRow;
      Int_t     fYRow;
      TVector3  fPositionVector;
      TVector3  fPositionVector2;
      Double_t  fTDCSum;
      Double_t  fTDCDifference;

   public:
      ForwardTrackerPositionHit();
      virtual ~ForwardTrackerPositionHit();
      ForwardTrackerPositionHit(const ForwardTrackerPositionHit& rhs);
      ForwardTrackerPositionHit& operator=(const ForwardTrackerPositionHit &rhs);

      void  Print(const Option_t * opt = "") const ;
      void  Clear(Option_t *opt = "");

      ClassDef(ForwardTrackerPositionHit, 3)
};
#endif

