#ifndef ANNThruFieldEvent_HH
#define ANNThruFieldEvent_HH 1

#include "TObject.h"
#include "BIGCALCluster.h"
#include "TString.h"
#include "Pi0ClusterPair.h"
#include "TObjString.h"


/** Simple magnetic field deflection correction.  
 *  Used when reconstructing quantities from a plane at bigcal 
 *  to somewhere near (or at) the target vertex.  
 */
class ANNElectronThruFieldEvent : public TObject {

   protected:
      TString  fInputNeurons;
      Int_t    fNInputNeurons;

   public:
      Double_t fTrueEnergy;             /// Thrown Energy
      Double_t fTrueTheta;              /// Thrown Theta
      Double_t fTruePhi;                /// Thrown Phi

      Double_t fDelta_Energy;           /// delta = thrown - bigcalPlane
      Double_t fDelta_Theta;            /// delta = thrown - bigcalPlane, s.t. reconstructed = bigcalplane + delta
      Double_t fDelta_Phi;              /// delta = thrown - bigcalPlane

      Double_t fBigcalPlaneTheta;       /// Thrown Theta
      Double_t fBigcalPlanePhi;         /// Thrown Phi

      Double_t fBigcalPlaneDeltaPTheta; /// correction the cluster theta for momentum direction
      Double_t fBigcalPlaneDeltaPPhi;   ///

      Double_t fBigcalPlaneX;           /// X position in bigcal plane
      Double_t fBigcalPlaneY;           /// Y position in bigcal plane
      Double_t fBigcalPlaneEnergy;      /// Cluster Energy

   public:
      ANNElectronThruFieldEvent();
      virtual ~ANNElectronThruFieldEvent();

      virtual void   GetMLPInputNeuronArray(Double_t * par) const;
      virtual void   Clear();

      Int_t          GetNInputNeurons() const ;
      const char *   GetMLPInputNeurons() const;

      ClassDef(ANNElectronThruFieldEvent,4)
};


/** Reconstruction through field with raster input neurons.
 *  
 */
class ANNElectronThruFieldEvent2 : public ANNElectronThruFieldEvent {

   public:
      Double_t    fRasterX;
      Double_t    fRasterY;

   public:
      ANNElectronThruFieldEvent2();
      virtual ~ANNElectronThruFieldEvent2();

      virtual void Clear();
      virtual void GetMLPInputNeuronArray(Double_t * par) const ;

      ClassDef(ANNElectronThruFieldEvent2,1)
};


/** Reconstruction through field with raster input neurons.
 *  Add the cluster quantities for getting the momentum vector.
 */
class ANNElectronThruFieldEvent3 : public ANNElectronThruFieldEvent2 {

   public:
      Double_t fXCluster;
      Double_t fYCluster;
      Double_t fXClusterSigma;
      Double_t fYClusterSigma;
      Double_t fXClusterSkew;
      Double_t fYClusterSkew;
      Double_t fXClusterKurt;
      Double_t fYClusterKurt;

   public:
      ANNElectronThruFieldEvent3();
      virtual ~ANNElectronThruFieldEvent3();

      virtual void Clear();
      virtual void GetMLPInputNeuronArray(Double_t * par) const ;

      ClassDef(ANNElectronThruFieldEvent3,1)
};

#endif

