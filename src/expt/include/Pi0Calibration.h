#ifndef Pi0Calibration_H
#define Pi0Calibration_H 1
#include "BIGCALClusterProcessor.h"
#include "BETACalculation.h"
#include "BigcalEvent.h"
#include "LocalMaxClusterProcessor.h"
#include "Pi0Event.h"
#include "TVector3.h"
#include "TMatrixD.h"
#include "TH1F.h"
#include "TH1D.h"
#include "InSANEClusterEvent.h"
#include "TClonesArray.h"
#include "BigcalPi0Calibrator.h"
#include "TGraph.h"
#include "Pi0EventDisplay.h"
#include "TCanvas.h"
#include <exception>
#include "BIGCALGeometryCalculator.h"
#include "BigcalCoordinateSystem.h"
#include "Pi0ClusterPair.h"
#include "TClonesArray.h"
#include <map>
#include "SANENeuralNetworks.h"
#include "ANNEvents.h"
#include "TVectorT.h"

class Pi0PhotonEnergy;
class Pi0MassSquared;

/** Pi0Calibration Calculations
 *
 *  Calculate() increments counter until
 *  enough events have been accumulated.
 *  Then Calls BuildMatricies()
 *  If the determinant of matrix fC is non-zero
 *  it performs one iteration of calibration.
 *
 * There are two types of blocks involved. Regular blocks and "active" blocks.
 * "Active" blocks just participate in reconstruction. They might not be calibrated.
 * All blocks added to fMatrixBlocks are considered active, but not all active blocks are calibrated.
 *
 * A block should be active before adding it to the list of calibrated blocks, ie,
 * AddActiveBlock(i,j) should be called before AddBlock(i,j).
 *
 * \ingroup Calibrations
 * \ingroup pi0
 */
class Pi0Calibration : public BETACalculationWithClusters {

   private:
      BIGCALGeometryCalculator * fBCGeo;
      double epsilon[1744];
      double epsilonLast[1744];

   protected:

      Double_t             fMass_0;                  /// Target particle mass 
      Double_t             fMass2_0;                 /// Target particle mass squared
      TTree              * fPi0Tree;                 ///
      Pi0Event           * fPi0Event;                /// 

      std::map<int, int>   fActiveBlocks;             ///< Blocks to be used for event purposes but not calibrated
      std::map<int, int>   fMatrixBlocks;             ///< Blocks to be calibrated.
      std::map<int, int>   fMatrixElementsInvolved;   ///< 

      TVector3 origin ;
      TVector3 v1     ;
      TVector3 v2     ;
      TVector3 k1     ;
      TVector3 k2     ;

      ANNGammaEvent                           fANNGammaEvent;

      NNParaGammaXYCorrectionClusterDeltaX    fNNParaDeltaX;
      NNParaGammaXYCorrectionClusterDeltaY    fNNParaDeltaY;

      NNPerpGammaXYCorrectionClusterDeltaX    fNNPerpDeltaX;
      NNPerpGammaXYCorrectionClusterDeltaY    fNNPerpDeltaY;

      Double_t                                fNNParams[20];


   public:
      TTree              * fTree;                       /// Output tree
      std::vector<Int_t> * fEigenVectorsWithStatistics; /// List of the eigenvectors which
      std::vector<Int_t>   fBlocksInvolved;             /// List of the eigenvectors which have good statistics
      Int_t                fNumberOfBlocks;             /// Number of blocks to be calibrated (size of fBlockNumberList
      Int_t                fNumberOfActiveBlocks;       /// Number of blocks inactive area 
      std::vector<Int_t>   fBlockNumberList ;           /// An array of block numbers to consider in the calibration

      Bool_t                                  fIsPerp;

      Pi0ClusterPair     * fClusterPair;
      Double_t             fLastMass;

      TRotation fRotFromBCcoords;
      BigcalCoordinateSystem * bcCoords;

      Int_t fCalibrationIteration;
      //Pi0EventDisplay * fDisplay; //!
      /// Vector  the change in energies for this iteration.
      TMatrixD fEpsilon;
      TMatrixD fC;
      TMatrixD fCInverted;
      TMatrixD fEpsilonLast;
      TMatrixD fL;
      TMatrixD fD;
      TMatrixD fBlockEnergies;

      Double_t fB_event; /// mass bias for the last event
      Double_t fB;       /// mass bias sum
      Double_t fLambda;  /// Lagrange multiplier

      Int_t fiMax, fiMin, fjMax, fjMin; /// extremum for selecting partial region of bigcal
      Int_t fBlockMaxNEvents;

      Int_t fNumberOfEvents;
      Int_t fBlockNumber;
      Int_t fLastEventNumber;
      Int_t fFirstEventNumber;

      Double_t fMaxEpsilon;            /// Maximum epsilon value allowed for a single iteration
      Double_t fEpsilonFraction;       /// Fraction of epsilon to retain for an iteration

      Int_t fInitSeries;
      Int_t fLaterSeries;
      Double_t fEigenValueThreshold;
      TVectorD fEigenValues;

      InSANEClusterEvent       * fClusterEvent; //!
      TClonesArray             * fClusters; //!
      Pi0PhotonEnergy          * fNu[2]; //->
      Pi0MassSquared           * fMPi0Squared; //->
      Double_t                   fBlocks[1744];
      Double_t                 * fLArray;
      Double_t                 * fDArray;

      TH2D * fEventClusters;
      TH2D * fEpsilonBlocks;
      TH1D * fEigenValueHist;
      TH1D * fLHist;
      TH1D * fDHist;
      TH1D * fEBLockHist;
      TH1D * fEpsilonHist;
      TH1D * fEpsilonLastHist;
      TH2D * fCHist;
      TH2D * fCInvHist;
      TH1D * fMassBiasHist;
      TH2D * fBigcalHist;

      TGraph * fLGraph;
      TGraph * fDGraph;
      TGraph * fEpsilonGraph;
      TGraph * fEpsionLastGraph;
      TCanvas * fCanvas;
      TCanvas * fCanvas2;
      TCanvas * fCanvas3;

   public:
      Pi0Calibration();
      Pi0Calibration(InSANEAnalysis * analysis);

      virtual ~Pi0Calibration() ;

      /** Calculate energy corrections,
       *
       *  Increments the number of pion events if it was a good one.
       *  iEvent is an event iterator
       *  iEigen is the eigenvector of C iterator
       *
       *
       * \ingroup pi0
       */
      virtual Int_t Calculate();

      void     SetMass(Double_t m) { fMass_0 = m; fMass2_0 = m*m; }
      void     SetMass2(Double_t m) { fMass2_0 = m; fMass_0 = TMath::Sqrt(m); }
      Double_t GetMass() { return fMass_0 ; }
      Double_t GetMass2() { return fMass2_0 ; }

      void Print(Option_t * opt = "") const;

      /** Prints definition of Pi0 Calibration */
      virtual void  DefineCalculation() {
         std::cout << "Pi0 Calibration following NIM  A 566 366-374 \n";
      }
      Int_t SetClusterPair(Pi0ClusterPair * pair);
      Int_t AddClusterBlocks(BIGCALCluster * clust,Int_t photon_i);
      Int_t CorrectClusterBlocks(BIGCALCluster * clust,Int_t photon_i);

      /** Sets the list of blocks which are to be calibrated */
      //Int_t SetBlockList(std::vector<Int_t> * list = 0);

      Int_t CreateNewCalibrationSeries(Int_t start_run, Int_t end_run);

      /** Returns true if the block is in the list to be calibrated */
      bool IsBlockInList(Int_t aBlockNum) const ;
      bool IsActiveBlock(Int_t aBlockNum) const ;

      bool IsBlockOnListPerimeter(Int_t aBlockNum) const ;
      bool IsBlockOnListPerimeter(Int_t i, Int_t j) const ;

      bool IsActivePair(Pi0ClusterPair * pair) {
         BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
         Int_t    b_num1    = geocalc->GetBlockNumber(pair->fCluster1.fiPeak,pair->fCluster1.fjPeak);
         Int_t    b_num2    = geocalc->GetBlockNumber(pair->fCluster2.fiPeak,pair->fCluster2.fjPeak);
         // Active blocks are assumed to also cover the blocks in the calibration list
         if( IsActiveBlock(b_num1) && IsActiveBlock(b_num2) ){
            if( IsBlockInList(b_num1) ) {
               //if( !(IsBlockOnListPerimeter(pair->fCluster1.fiPeak,pair->fCluster1.fjPeak) ) ){
                  return true;
               //} 
            }
            if( IsBlockInList(b_num2) ) {
               //if( !(IsBlockOnListPerimeter(pair->fCluster2.fiPeak,pair->fCluster2.fjPeak) ) ){
                  return true;
               //} 
            }
         }
         return false;
      }

      /** Checks to see if the centroids of both clusters is in the list of calibration blocks */
      bool IsPairInList(Pi0ClusterPair * pair) {
         BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
         Int_t    b_num1    = geocalc->GetBlockNumber(pair->fCluster1.fiPeak,pair->fCluster1.fjPeak);
         if( fMatrixBlocks.find(b_num1) == fMatrixBlocks.end() ) return false;
         Int_t    b_num2    = geocalc->GetBlockNumber(pair->fCluster2.fiPeak,pair->fCluster2.fjPeak);
         if( fMatrixBlocks.find(b_num2) == fMatrixBlocks.end() ) return false;
         return true;
      }

      /** returns the matrix index used in the calibration procedure for the block number provided */
      Int_t  GetBlockMatrixIndex(Int_t aBlockNum);

      Int_t fChecki;

      /** Check to see if you have enough statistics to do a worthwhile calibration */
      Bool_t HasEnoughStatistics();

      /**  virtual method called by InSANECorrector::Initialize
      */
      virtual Int_t SetClusterEvent(InSANEClusterEvent * ev) {
         fClusterEvent = ev;
         fClusters = ev->fClusters;
         return(0);
      }

      /** virtual method called by InSANECorrector::Initialize
      */
      virtual Int_t Initialize();

      /** Removes all blocks */
      Int_t ClearBlocks();

      /** Adds rectangle of blocks to be calibrated. If clear is false it appends to existing list. */
      Int_t InitBlocks(Int_t ia,Int_t ib,Int_t ja,Int_t jb, bool clear = true);

      /** Adds a block to the list of blocks to be calibrated */
      Int_t AddBlock(Int_t aBlockNum);
      Int_t AddBlock(Int_t i, Int_t j);

      /** Adds rectangle of blocks to be used in calibration. If clear is false it appends to existing list. */
      Int_t InitActiveBlocks(Int_t ia,Int_t ib,Int_t ja,Int_t jb, bool clear = true);

      Int_t AddActiveBlock(Int_t aBlockNum);
      Int_t AddActiveBlock(Int_t i, Int_t j);

      Double_t GetNewCalibrationCoefficient(Int_t blockNumber) const ;

      Int_t LoadEpsilonFromSeries(Int_t init_series, Int_t later_series);

      /** Adds to the matricies the current event.
      */
      Int_t BuildMatricies();

      Double_t CalculateMass();

      /** displays histograms for current calibration status
      */
      void ViewStatus();

      /** Resets all matricies and copies new to old fEpsilon coefficients
      */
      Int_t Reset() {
         //fEpsilonLast = fEpsilon;
         //fEpsilon.Zero();
         fL.Zero();
         fD.Zero();
         fC.Zero();
         fCInverted.Zero();
         fB = 0;
         fEventClusters->Reset();
         fLHist->Reset();
         fDHist->Reset();
         fEBLockHist->Reset();
         fEpsilonHist->Reset();
         fEpsilonLastHist->Reset();
         fCHist->Reset();
         fCInvHist->Reset();
         fMassBiasHist->Reset();
         fBigcalHist->Reset();
         return(0);
      }

      /** virtual method called by InSANECorrector::Initialize
      */
      Int_t ExecuteCalibration();

      void Describe() { };

      ClassDef(Pi0Calibration, 1)
};

#endif

