#ifndef SANENeuralNetworks_HH
#define SANENeuralNetworks_HH 1

#include "ANNEvents.h"
#include "ANNThruFieldEvent.h"
#include "ANNGammaEvent.h"

// Trained: April 2013
//#include "NNParaElectronAngleCorrectionDeltaEnergy.h"
#include "NNParaElectronAngleCorrectionDeltaTheta.h"
#include "NNParaElectronAngleCorrectionDeltaPhi.h"

// Trained: April 2013
//#include "NNParaElectronXYCorrectionEnergy.h"
#include "NNParaElectronXYCorrectionClusterDeltaX.h"
#include "NNParaElectronXYCorrectionClusterDeltaY.h"

// Trained: April 8 2013
#include "NNParaElectronAngleCorrectionBCPDirTheta.h"
#include "NNParaElectronAngleCorrectionBCPDirPhi.h"

// Trained: April , combines both theta nd phi 
#include "NNParaElectronAngleCorrectionBCPDir.h"

// Trained: June 2 2013
//#include "NNParaElectronAngleCorrectionDeltaEnergy.h"
#include "NNPerpElectronAngleCorrectionDeltaTheta.h"
#include "NNPerpElectronAngleCorrectionDeltaPhi.h"

// Trained: June 2 2013
//#include "NNPerpElectronXYCorrectionEnergy.h"
#include "NNPerpElectronXYCorrectionClusterDeltaX.h"
#include "NNPerpElectronXYCorrectionClusterDeltaY.h"

// Trained: June 2 2013
#include "NNPerpElectronAngleCorrectionBCPDirTheta.h"
#include "NNPerpElectronAngleCorrectionBCPDirPhi.h"

// Trained: April , combines both theta nd phi 
//#include "NNPerpElectronAngleCorrectionBCPDir.h"

// Trained December 2014
// These position correction assume the input energy is the
// "corrected energy"
#include "NNParaGammaXYCorrectionClusterDeltaX.h"
#include "NNParaGammaXYCorrectionClusterDeltaY.h"

// Trained December, 28 2014
#include "NNPerpGammaXYCorrectionClusterDeltaX.h"
#include "NNPerpGammaXYCorrectionClusterDeltaY.h"

// Trained: March 26, 2014 
//#include "NNParaGammaEXYCorrectionClusterDeltaX.h"
//#include "NNParaGammaEXYCorrectionClusterDeltaY.h"
//#include "NNParaGammaEXYCorrectionEnergy.h"

#endif

