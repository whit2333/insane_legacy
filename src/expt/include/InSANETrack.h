#ifndef InSANETrack_HH
#define InSANETrack_HH 1

#include "TObject.h"
#include "TVector3.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TClonesArray.h"
#include <vector>

/**  A particle Track.
 *
 *  \ingroup Events
 */
class InSANETrack : public TObject {

   public:

      TClonesArray  * fHitPositions; //->
      Int_t           fRunNumber;
      Int_t           fEventNumber;
      Double_t        fNCherenkovElectrons;
      Double_t        fCherenkovTDC;
      TLorentzVector  fMomentum;
      TVector3        fPosition0;             /// cluster seed position
      Int_t           fClusterNumber;
      Bool_t          fIsNoisyChannel;
      Int_t           fNPositions;

   public :
      InSANETrack();
      InSANETrack(const InSANETrack& rhs);
      virtual ~InSANETrack();
      InSANETrack& operator=(const InSANETrack &rhs);
      void Clear(Option_t * opt = "");

      ClassDef(InSANETrack,1)
};


#endif


