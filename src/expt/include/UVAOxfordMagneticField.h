#ifndef UVAOxfordMagneticField_h
#define UVAOxfordMagneticField_h 1

#include <iostream>
#include <fstream>
//#include <vector>
//#include <stdlib.h>
//#include <stdio.h>
//#include <math.h>

#include "TString.h"
#include "TMath.h"
#include "TCanvas.h"
#include "InSANEDatabaseManager.h"
#include "InSANEMagneticField.h"
#define fieldDataPoints 31161
#include "TEveTrackPropagator.h"
#include "TEveVector.h"

/** Concrete class for UVA magnetic built by Oxford.
 *  Note: Units are cm and Tesla.
 *
 * \ingroup emfields
 */
class UVAOxfordMagneticField  : public InSANEMagneticField {

   public :
      UVAOxfordMagneticField(const char * n = "UVAOxfordMagneticField");

      virtual ~UVAOxfordMagneticField();

      TString fFieldMapFile; /// Text file containing field map

      /** Fills the array Bfield[0-2], (xyz) components, given the spacetime point, Point
       *  The time coordinate is index (3).
       */
      virtual void GetFieldValue(const  Double_t Point[4], Double_t *Bfield) const;

      /** Reads a data file with field map.  */
      void ReadDataFile();

      /** Gets field map from SQLite database.
       *  If this fails, it looks for the field map in a
       *  file.
       */
      void LoadFieldMap();

      static int LoadFieldMap_callback(void * magField, int argc, char **argv, char **azColName);

      Int_t Getiz(Double_t Z) const {
         return (Int_t)TMath::Floor(Z / (2.0));
      }
      Int_t Getjr(Double_t R) const {
         return (Int_t)TMath::Floor(R / (2.0));
      }
      struct fieldDataPoint {
         Double_t z, r, Bz, Br;
      };

      Double_t RawBZ[fieldDataPoints];
      Double_t RawBR[fieldDataPoints];
      Double_t * BzRpoint[221];
      Double_t * BrRpoint[221];
      Double_t ** BzFieldRaw;
      Double_t ** BrFieldRaw;
      Double_t *** BF;
      Int_t numPoints;
      Double_t ** Field;

      ClassDef(UVAOxfordMagneticField, 1)
};


/** Implementation of UVAOxfordMagneticField for for ROOT's Event display package, EVE.
 *  Units needed for track propogation in eve are cm, Tesla, and GeV 
 *
 * \ingroup emfields
 */
class UVAEveMagField  :  public UVAOxfordMagneticField, public TEveMagField {
   public :
      UVAEveMagField();
      virtual ~UVAEveMagField();

      virtual TEveVector GetField(Float_t x, Float_t y, Float_t z) const;

      virtual Float_t  GetMaxFieldMag() const { return fMaximumField; }

      ClassDef(UVAEveMagField, 1)
};
#endif

