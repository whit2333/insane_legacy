#ifndef HallCBeamEvent_H
#define HallCBeamEvent_H

#include <TROOT.h>
 //#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>
#include "InSANEEvent.h"
#include <TVector3.h>


/** Concrete class for data on the Jlab beam in Hall C
 *
 * \ingroup Events
 * \ingroup beam
 */
class HallCBeamEvent : public InSANEEvent {
public :
   HallCBeamEvent() {
      ClearEvent();
   }
   virtual ~HallCBeamEvent() { }

   void ClearEvent(Option_t * opt = ""){
      fXRaster = 0.0;
      fYRaster = 0.0;
      fXSlowRaster = 0.0;
      fYSlowRaster = 0.0;
      fXSem = 0.0;
      fYSem = 0.0;
      fHelicity = 0;
      fInterpolatedBeamCurrent = 0.0;
      fRasterPosition.SetXYZ(0, 0, 0);
   }

   void Print(const Option_t * opt = "") const {
      InSANEEvent::Print(opt);
      std::cout << "   Beam Energy: " << fBeamEnergy << " , helicity: " << fHelicity << "\n";
   }

   /** Returns the raster postion. Z is set to 0. */
   TVector3 * GetRasterPosition() {
      //fRasterPosition.SetXYZ(fXSlowRaster, fYSlowRaster, 0.0);
      return(&fRasterPosition);
   }

   /** Set the beam energy in GeV */
   void     SetBeamEnergy(Double_t en) {
      fBeamEnergy = en;
   }
   Double_t GetBeamEnergy() {
      return(fBeamEnergy);
   }

   Double_t        fBeamEnergy;
   Double_t         fXRaster;
   Double_t         fYRaster;
   Double_t         fXSlowRaster;
   Double_t         fYSlowRaster;
   Double_t         fXSem;
   Double_t         fYSem;
   TVector3         fRasterPosition;
   Int_t            fHelicity;
   Double_t         fXSlowRasterADC;
   Double_t         fYSlowRasterADC;
   Double_t         fXRasterADC;
   Double_t         fYRasterADC;

   Double_t         fInterpolatedBeamCurrent;
   Double_t         fTotalCharge;
   Double_t         f2SecondCharge;
   Double_t         fTotalChargeHelicityPlus;
   Double_t         f2SecondChargeHelicityPlus;
   Double_t         fTotalChargeHelicityMinus;
   Double_t         f2SecondChargeHelicityMinus;
//    Int_t            fHelicityPlusScaler;
//    Int_t            fHelicityMinusScaler;
//    Int_t            fHelicityPlusTrigger;
//    Int_t            fHelicityMinusTrigger;
//    Float_t          fHalfWavePlate;

   // BEAM DATA
//    Double_t        energy;
//    Double_t         rast_x;
//    Double_t         rast_y;
//    Double_t         slow_rast_x;
//    Double_t         slow_rast_y;
//    Double_t         sem_x;
//    Double_t         sem_y;

//    Int_t            i_helicity;
//    Double_t        tcharge;
//    Double_t        charge2s;
//    Double_t        tcharge_help;
//    Double_t        charge2s_help;
//    Double_t        tcharge_helm;
//    Double_t        charge2s_helm;
//    Int_t           hel_p_scaler;
//    Int_t           hel_n_scaler;
//    Int_t           hel_p_trig;
//    Int_t           hel_n_trig;
//    Float_t         half_plate;


   ClassDef(HallCBeamEvent, 4)
};

#endif

