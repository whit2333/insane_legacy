#ifndef InSANELeadGlassBlock_h
#define InSANELeadGlassBlock_h

#include "TObject.h"
#include "InSANEDetectorComponent.h"


/** A Lead Glass Calorimeter block
 *
 *   \ingroup detComponents
 */
class InSANELeadGlassBlock : public InSANEDetectorComponent {
public:
   InSANELeadGlassBlock(const char * name = "detComp" , const char * title  = "Det Comp", Int_t num = 0): InSANEDetectorComponent(name, title, num),
      fDensity(3.86),
      fIndexOfRefraction(1.647),
      fRadiationLength(2.74) {
      fXSize = 0.0;
      fYSize = 0.0;
      fZSize = 0.0;
      fBlockTransparency = 1.0;
      fCalibrationCoefficient = 1.0;
   }

   ~InSANELeadGlassBlock() {

   }

   Int_t GetBlockNumber() {
      return(GetComponentNumber());
   }
   void SetBlockNumber(Int_t num) {
      SetComponentNumber(num);
   }

public:
   Double_t fXSize;
   Double_t fYSize;
   Double_t fZSize;
   Double_t fDensity; /// In g/cm^3
   Double_t fIndexOfRefraction;
   Double_t fRadiationLength; /// In g/cm^2
   Double_t fBlockTransparency;
   Double_t fCalibrationCoefficient;

   ClassDef(InSANELeadGlassBlock, 1)
};



#endif

