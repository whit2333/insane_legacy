#ifndef ForwardTrackerCalculations_HH
#define ForwardTrackerCalculations_HH
#include "BETACalculation.h"
#include "TClonesArray.h"

class ForwardTrackerCalculation1 : public BETACalculation {
   public:
      ForwardTrackerCalculation1(InSANEAnalysis * analysis) : BETACalculation(analysis) {
         //      fCopiedArray = 0;
         SetNameTitle("ForwardTrackerCalculation1", "ForwardTrackerCalculation1");
         fMaxHits = 150;
         fMaxPositionHits = 300;

      }
      virtual ~ForwardTrackerCalculation1() {
         //   if(fCopiedArray) delete fCopiedArray;
      }

      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  ForwardTrackerCalculation1 - \n ";
         std::cout <<  "      Counts hits and creates new hits for hits passing a timing cut. \n";
         std::cout <<  "      From these new timed hits, a new position hits branch is filled with all possible\n";
         std::cout <<  "      positions. Each of the tracker's y-plane hits is paired with the x-plane hits.   \n";
         std::cout <<  "      The correlation between two y-plane hits will be made later...   \n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

      virtual Int_t Calculate() ;
      Int_t fMaxPositionHits;
      Int_t fMaxHits;



      ClassDef(ForwardTrackerCalculation1, 1)
};

class ForwardTrackerCalculation2 : public BETACalculation {
public:
   ForwardTrackerCalculation2(InSANEAnalysis * analysis) : BETACalculation(analysis) {
      SetNameTitle("ForwardTrackerCalculation2", "ForwardTrackerCalculation2");
   }
   virtual ~ForwardTrackerCalculation2() {}

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  ForwardTrackerCalculation2 - \n ";
      std::cout <<  "      Counts hits and creates new hits in branch, fPositionHits\n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   virtual Int_t Calculate() ;

   ClassDef(ForwardTrackerCalculation2, 1)
};

#endif


