#ifndef InSANEDetectorTiming_H
#define InSANEDetectorTiming_H

#include <iostream>
//#include <stdlib.h>

#include "TNamed.h"
#include "TMath.h"
#include "Math/IFunction.h"
#include "Math/IParamFunction.h"

#include "InSANEEvent.h"
#include "InSANEDatabaseManager.h"
#include "TSQLServer.h"
#include "TSQLRow.h"
#include "TSQLResult.h"

/**  Function for fitting tdc spectra
 *
 *  The function is the sum of a lorentzian and a piecewise constant function
 *  The Lorentzian has 3 parameters: par0 is the amplitude, par1 is the width par2 is the mean
 *  A piecewise function is added for the background. It has 3 parameters, par3,par4, and par5 which are the
 *  amplitude (constant) , central (mean) value, and the width/2 of the piecewise function.
 */
class InSANETDCSpectrum: public ROOT::Math::IParametricFunctionOneDim {

   private:
      const double *pars;
      int fNPars;

   public:
      InSANETDCSpectrum();
      ~InSANETDCSpectrum();

      double DoEvalPar(double x, const double* p) const ;

      ROOT::Math::IBaseFunctionOneDim* Clone() const {
         return new InSANETDCSpectrum();
      }

      const double* Parameters() const { return pars; }
      void          SetParameters(const double* p) { pars = p; }
      unsigned int  NPar() const { return fNPars; }

      ClassDef(InSANETDCSpectrum, 1)
};


/** ABC for an event in a detector
 */
class InSANEDetectorTiming : public TNamed {
   public :
      InSANEDetectorTiming();
      InSANEDetectorTiming(Int_t id, Double_t val, Double_t width);
      ~InSANEDetectorTiming();

      ULong_t Hash() const { return (ULong_t)fId ; }
      Bool_t  IsEqual(const TObject *obj) const {
         return fId == ((InSANEDetectorTiming*)obj)->GetNumber();
      }
      Bool_t  IsSortable() const { return kTRUE; }
      Int_t   Compare(const TObject *obj) const {
         if (fId > ((InSANEDetectorTiming*)obj)->GetNumber())
            return 1;
         else if (fId < ((InSANEDetectorTiming*)obj)->GetNumber())
            return -1;
         else return 0;
      }
      Int_t GetNumber() { return fId; }
      void  SetWidth(Double_t w) { fTDCPeakWidth = w; }
      void  SetMean(Double_t m) { fTDCPeak = m; }

      Int_t           fTriggerType;
      Double_t        fTDCPeak;
      Double_t        fTDCPeakWidth;
      Int_t           fId;
      Double_t        fPeakFitChiSquared;
      Double_t        fTimewalkParameters[4];

      TString * GetSQLInsertString(const char * det, Int_t run_number);

      static int SetMeanWidth_callback(void * theTdcpeak, int argc, char **argv, char **azColName);

      /** Get the timing data stored in the database.
       *  Sets the datamembers. 
       */
      Int_t GetTiming(const char * det, Int_t run_number, Int_t id);

      void PrintTimingHead() const ; // *MENU*
      void PrintTiming() const ;     // *MENU*
      void Print(Option_t * opt = "") const ;           // *MENU*

      ClassDef(InSANEDetectorTiming, 3)
};



#endif
