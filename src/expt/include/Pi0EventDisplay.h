#ifndef Pi0EventDisplay_HH
#define Pi0EventDisplay_HH
#include "InSANEEventDisplay.h"

class Pi0EventDisplay : public InSANEEventDisplay {
public:

   Pi0EventDisplay() {
      fUpdateTriggerBit = 2; // pi0trig
      fEventType = 5; //b2trig,b1trig, and pi0trig

   };

   virtual  ~Pi0EventDisplay() {
   };

   /** Called from InSANEAnalyzer */
   virtual void UpdateDisplay();

   ClassDef(Pi0EventDisplay, 1)
};

#endif
