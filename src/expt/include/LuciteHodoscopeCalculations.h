#ifndef LuciteHodoscopeCalculations_H
#define LuciteHodoscopeCalculations_H 1
#include "LuciteHodoscopePositionHit.h"
#include "BETACalculation.h"
#include "InSANEAnalysis.h"

/**  First pass Calculations for SANE Lucite Hodoscope Detector.
 *
 *  Does the following:
 *   - Counts events
 *   - Fills the array, fLuciteEvent->fPositionHits, with LuciteHodoscopePositionHit hits.
 *   - Aligns the fPositionHits' TDCSums around zero.
 *   - Deletes the hits stored in fLuciteEvent->fHits if ...
 *      - there are not multiple TIMED sums, which are formed using every combination of tdc hits.
 *        for every timed multiple hit.
 *      - If there is not multiple timed hits in a bar
 *
 *  t_diff_corrected = t_diff - 2(t_pos - t_pos-ref) + (t_sum - t_sum-ref) 
 *
 * \ingroup hodoscope
 * \ingroup Calculations
 */
class LuciteHodoscopeCalculation1 : public BETACalculation {
public:
   LuciteHodoscopeCalculation1(InSANEAnalysis * analysis) : BETACalculation(analysis)  {
      SetNameTitle("LuciteHodoscopeCalculation1", "LuciteHodoscopeCalculation1");
   }

   ~LuciteHodoscopeCalculation1() {
   }

   Int_t          fNumberOfPositions;
   const char * fTreeName;

   void SetOutputTreeName(const char * name) {
      fTreeName = name;
   }

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  LuciteHodoscopeCalculation1 - \n ";
      std::cout <<  "      Counts hits and creates new hits branch which is filled with only (roughly) timed hits. \n";
      std::cout <<  "      From these hits it creates a new position hits branch which matches hits in the same bar. \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   virtual Int_t Calculate() ;

   ClassDef(LuciteHodoscopeCalculation1, 1)
};
//_______________________________________________________//




/** Does the following:
 *   -
 * \ingroup Calculations
 */
class LuciteHodoscopeCalculation2 : public BETACalculation {
public:
   LuciteHodoscopeCalculation2(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis)  {
      SetNameTitle("LuciteHodoscopeCalculation2", "LuciteHodoscopeCalculation2");
   };

   ~LuciteHodoscopeCalculation2() {
   };

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  LuciteHodoscopeCalculation2 - \n ";
      std::cout <<  "      Using the exp survey data,  \n";
      std::cout <<  "      fPositionHits positions\n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   virtual Int_t Calculate();

   ClassDef(LuciteHodoscopeCalculation2, 1)
};


/**  Creates a calibrated bigcal position from the lucite hodoscope position information
 */
class LuciteHodoscopeCalculation3 : public BETACalculation {
public:
   LuciteHodoscopeCalculation3(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis) {
      SetNameTitle("LuciteHodoscopeCalculation3", "LuciteHodoscopeCalculation3");
   }

   ~LuciteHodoscopeCalculation3() {
   };

   virtual Int_t Initialize() {
      BETACalculation::Initialize();
      fHodoscopeDetector->GetPositionCalibrations();
      return(0);
   }

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  LuciteHodoscopeCalculation3 - \n ";
      std::cout <<  "      \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }


   virtual Int_t Calculate() ;


   ClassDef(LuciteHodoscopeCalculation3, 1)
};


/**
 *  Fourth pass Calculations for Gas Cherenkov
 */
class LuciteHodoscopeCalculation4 : public BETACalculation {
public:
   LuciteHodoscopeCalculation4(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis)  {
   };

   ~LuciteHodoscopeCalculation4() {
   }

   /**  Initialize  a new output hits array
    *  Used here for output tree.
    */
//    virtual Int_t Initialize() {
//       if(SANERunManager::GetRunManager()->fVerbosity>2)
//          std::cout << " o SANEElectronSelectors::Initialize()\n";
//    }


   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  LuciteHodoscopeCalculation4 - \n ";
      std::cout <<  "      \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   virtual Int_t Calculate() = 0;


   ClassDef(LuciteHodoscopeCalculation4, 1)
};

#endif

