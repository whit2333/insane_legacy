#ifndef InSANEMeasuredAsymmetry_HH
#define InSANEMeasuredAsymmetry_HH 1

#include "InSANEAsymmetry.h"
#include "InSANEDilutionFactor.h"
#include "InSANEAveragedKinematics.h"
#include <vector>
#include "InSANESystematic.h"


/** Measured Asymmetry for polarized DIS experiments.
 *  
 *  In order to use the count method, you must set the address of 
 *  an InSANEDISEvent using SetDISEvent.
 *
 * 
 *  \ingroup asymmetries
 */
class InSANEMeasuredAsymmetry : public InSANEAsymmetry {

   public:

      Int_t                  fRunNumber  = 0;
      Double_t               fDilution   = 0.0;
      Double_t               fAsymmetry  = 0.0;
      Double_t               fError      = 0.0;

      Bool_t                 fChargeSwap    = false;
      Double_t               fHalfWavePlate = 1.0;
      Double_t               fPb            = 1.0;
      Double_t               fPt            = 1.0;
      std::vector<double>    fCharge   = {0,0};
      std::vector<double>    fLiveTime = {0,0} ;

      Double_t               fdelta_Pb ;
      Double_t               fdelta_Pt ;
      Double_t               fdelta_df ;
      Double_t               fdelta_AQ ;
      Double_t               fdelta_ALT;

      Double_t fQ2Min;
      Double_t fQ2Max;
      Double_t fxMin;
      Double_t fxMax;
      Double_t fEnergyMin;
      Double_t fEnergyMax;
      Double_t fNuMin;
      Double_t fNuMax;
      Double_t fWMin;
      Double_t fWMax;
      Double_t fBinningBeamEnergy;
      Double_t fThetaMin;
      Double_t fThetaMax;
      Double_t fPhiMin;
      Double_t fPhiMax;

      InSANERunSummary fRunSummary;

      TH1F fAsymmetryVsx;    
      TH1F fAsymmetryVsW;    
      TH1F fAsymmetryVsNu;   
      TH1F fAsymmetryVsE;    
      TH1F fAsymmetryVsTheta;
      TH1F fAsymmetryVsPhi;  

      InSANEAveragedKinematics1D fAvgKineVsx;    
      InSANEAveragedKinematics1D fAvgKineVsW;    
      InSANEAveragedKinematics1D fAvgKineVsNu;   
      InSANEAveragedKinematics1D fAvgKineVsE;    
      InSANEAveragedKinematics1D fAvgKineVsTheta;
      InSANEAveragedKinematics1D fAvgKineVsPhi;  

      TH1F fNPlusVsQ2;     
      TH1F fNMinusVsQ2;    

      TH1F fNPlusVsx;     
      TH1F fNMinusVsx;    
      TH1F fNPlusVsW;     
      TH1F fNMinusVsW;    
      TH1F fNPlusVsNu;    
      TH1F fNMinusVsNu;   
      TH1F fNPlusVsE;     
      TH1F fNMinusVsE;    
      TH1F fNPlusVsPhi;   
      TH1F fNMinusVsPhi;  
      TH1F fNPlusVsTheta; 
      TH1F fNMinusVsTheta;

      // Dilution histograms
      TH1F fDilutionVsx;    
      TH1F fDilutionVsW;    
      TH1F fDilutionVsNu;   
      TH1F fDilutionVsE;    
      TH1F fDilutionVsTheta;
      TH1F fDilutionVsPhi;  

      TH1F fSystErrVsx;    
      TH1F fSystErrVsW;    
      TH1F fSystErrVsNu;   
      TH1F fSystErrVsE;    
      TH1F fSystErrVsTheta;
      TH1F fSystErrVsPhi;  

      std::vector<InSANESystematic>  fSystematics_x;
      std::vector<InSANESystematic>  fSystematics_W;
      std::vector<InSANESystematic>  fSystematics_Nu;
      std::vector<InSANESystematic>  fSystematics_E;
      std::vector<InSANESystematic>  fSystematics_Theta;
      std::vector<InSANESystematic>  fSystematics_Phi;

      TH1F fMask_x;
      TH1F fMask_W;
      TH1F fMask_E;
      TH1F fMask_Nu;
      TH1F fMask_Theta;
      TH1F fMask_Phi;

      InSANEDilutionFromTarget * fDilutionFromTarget = nullptr; //!

   public:
      InSANEMeasuredAsymmetry(Int_t n);
      InSANEMeasuredAsymmetry() : InSANEMeasuredAsymmetry(0) { }
      InSANEMeasuredAsymmetry(const InSANEMeasuredAsymmetry&);
      InSANEMeasuredAsymmetry& operator=(const InSANEMeasuredAsymmetry&);
      //InSANEMeasuredAsymmetry(InSANEMeasuredAsymmetry&&)                 = default;
      //InSANEMeasuredAsymmetry& operator=(InSANEMeasuredAsymmetry&&)      = default;
      virtual ~InSANEMeasuredAsymmetry();

      virtual void Initialize();

      void Reset(Option_t * opt = "");
      void ResetMasks(Option_t * opt = "");
      void MaskIntersection(TH1F& m1, const TH1F& m2) const ;

      void CalculateBinMask_W();

      InSANEMeasuredAsymmetry &     operator+=(const InSANEMeasuredAsymmetry &rhs) ;
      const InSANEMeasuredAsymmetry operator+(const InSANEMeasuredAsymmetry &other)  ;

      /** Checks the kinematic and other variables to see if the event falls into the bin.
       *  Returns true if the event falls into the ranges of:
       *   - scattered energy
       *   - theta
       *   - phi
       *   - Qsquared
       */
      virtual Bool_t IsGoodEvent();

      virtual void SetColor(int c);

      virtual void Print(Option_t * opt = "") const; // *MENU*

      /** Count the event and bin the appropriate histograms depending on the helicity
       */
      virtual Int_t CountEvent();
      virtual Int_t CountEvent(std::vector<int>& groups);

      /** Calculate the physics asymmetry.
       *  Note that this uses a dilution factor, and  beam and target polarizations.
       */
      Double_t GetAsymmetry(Double_t N1, Double_t N2, Double_t df)        const  ;
      Double_t GetSystematicError(Double_t N1, Double_t N2, Double_t df)  const  ;
      Double_t GetStatisticalError(Double_t N1, Double_t N2, Double_t df) const  ;
      Double_t GetTotalError(Double_t N1, Double_t N2, Double_t df)       const  ;

      Double_t GetRelativeSystUncert_Pb(Double_t N1, Double_t N2, Double_t df)  const ;
      Double_t GetRelativeSystUncert_Pt(Double_t N1, Double_t N2, Double_t df)  const ;
      Double_t GetRelativeSystUncert_df(Double_t N1, Double_t N2, Double_t df)  const ;
      Double_t GetRelativeSystUncert_AQ(Double_t N1, Double_t N2, Double_t df)  const ;
      Double_t GetRelativeSystUncert_ALT(Double_t N1, Double_t N2, Double_t df) const ;
      Double_t GetRelativeSystUncert_tot(Double_t N1, Double_t N2, Double_t df) const ;

      void SetDirectory(TDirectory * dir = nullptr);

      Int_t CalculateDilutionFactors1D(TH1F * dfHist, const InSANEAveragedKinematics1D *kine);

      /** Calculates the physics asymmetries.
       *  \f$ A= 1/(df Pb Pt) (N+/Q+ - N-/Q-)/(N+/Q+ + N-/Q-) \f$
       */
      Int_t CalculateAsymmetries(); // *MENU*

      InSANESystematic* AddSystematic_W(const TH1F& h0, const char * syst_name);
      InSANESystematic* AddSystematic_x(const TH1F& h0, const char * syst_name);

      TH1F CreateAsymmetryHistogram1D(
            const TH1F& h0,
            const TH1F& h1,
            std::vector<InSANESystematic>& systematic,
            const TH1F * dfHist = nullptr,
            TH1F * systHist = nullptr);

      virtual void PrintBinnedTable(    std::ostream& stream) const;
      virtual void PrintBinnedTableHead(std::ostream& file) const;
      virtual void PrintTableHead(      std::ostream& file) const;
      virtual void PrintTableHeader(    std::ostream& file) const { PrintTableHead(file);}
      virtual void PrintTable(          std::ostream& file) const;
      virtual void PrintAsymTable(      std::ostream& stream) const;
      virtual void PrintAsymTableHead(  std::ostream& file) const;

      Double_t GetQ2() const { return (fNPlusVsQ2.GetMean() + fNMinusVsQ2.GetMean())/2.0 ;}
      Double_t GetAsymmetry() const { return fAsymmetry;}
      Double_t GetDilution()  const { return fDilution;}
      Double_t GetHWP()       const { return fHalfWavePlate;}
      Double_t GetTargetPol() const { return fPt;}
      Double_t GetBeamPol()   const { return fPb;}
      Double_t GetChargeNormalization() const { return fCharge[1]/fCharge[0] ; }

      Double_t GetLiveTime(int i = 0) { if( i< 2 && i>=0) return fLiveTime[i]; else return(0);}
      Double_t GetCharge(int i = 0) { if( i< 2 && i>=0)   return fCharge[i];   else return(0);}
      Int_t    GetRunNumber() const { return fRunNumber; }
      InSANEDilutionFromTarget * GetDilutionFromTarget() { return fDilutionFromTarget; }

      void SetRunNumber(Int_t run ) { fRunNumber = run; }
      void SetTargetPol(Double_t p) { fPt = p; }
      void SetBeamPol(Double_t p)   { fPb = p; }
      void SetDilution(Double_t p)  { fDilution = p; }
      void SetHWP(Double_t p)       { fHalfWavePlate = p; }
      void SetCharge(Double_t q1, Double_t q2) { fCharge[0] = q1; fCharge[1] = q2; }
      void SetLiveTime(Double_t l1 , Double_t l2 ) { fLiveTime[0] = l1; fLiveTime[1] = l2; }
      void SetDilutionFromTarget(InSANEDilutionFromTarget * d) { fDilutionFromTarget = d; }
      void SetRunSummary(InSANERunSummary* rs) ;

      Int_t Combine(InSANEMeasuredAsymmetry * a){
         return(0); 
      }

      Bool_t         IsFolder() const {
         return kTRUE;
      }

      void           Browse(TBrowser* b) ;



      ClassDef(InSANEMeasuredAsymmetry, 35)
};

#endif

