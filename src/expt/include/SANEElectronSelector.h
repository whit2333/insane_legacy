#ifndef SANEElectronSelector_H
#define SANEElectronSelector_H 1

#include "BETACalculation.h"
#include "SANEClusteringAnalysis.h"
#include "InSANEDISTrajectory.h"
#include "CleanedBETAEvent.h"
#include "InSANETriggerEvent.h"
#include <vector>
#include "SANENeuralNetworks.h"

/** Calculates the Electron's trajectory using the clusters and  selects clean electrons.
 *  A new tree is created with the Neural Netork corrected values.
 *  Calculates and sets the corrections to the cluster's angles and energy.
 *
 *  \section ElectronCuts Electron Cuts
 *  - Cherenkov cuts defined by method GasCherenkovDetector::PassesCuts()
 *  - and hard coded!!!
 *  - Fills fTrajectory (InSANEDISTrajectory) with values
 *
 * \ingroup Calculations
 */
class SANEElectronSelector : public BETACalculationWithClusters  {
   private: 
      TVector3 fp_naive;
      TVector3 fvertex;
      TVector3 fx_bigcalplane;
      Bool_t   fIsPerp;

   protected:
      InSANEDISTrajectory                     * fTrajectory;
      NNParaElectronXYCorrectionClusterDeltaX   fNNParaElectronClusterDeltaX;
      NNParaElectronXYCorrectionClusterDeltaY   fNNParaElectronClusterDeltaY;

      NNParaElectronAngleCorrectionDeltaTheta   fNNParaElectronDeltaTheta;
      NNParaElectronAngleCorrectionDeltaPhi     fNNParaElectronDeltaPhi;
      
      NNParaElectronAngleCorrectionBCPDirTheta  fNNParaElectronBCPDirTheta;
      NNParaElectronAngleCorrectionBCPDirPhi    fNNParaElectronBCPDirPhi;

      NNPerpElectronXYCorrectionClusterDeltaX   fNNPerpElectronClusterDeltaX;
      NNPerpElectronXYCorrectionClusterDeltaY   fNNPerpElectronClusterDeltaY;

      NNPerpElectronAngleCorrectionDeltaTheta   fNNPerpElectronDeltaTheta;
      NNPerpElectronAngleCorrectionDeltaPhi     fNNPerpElectronDeltaPhi;
      
      NNPerpElectronAngleCorrectionBCPDirTheta  fNNPerpElectronBCPDirTheta;
      NNPerpElectronAngleCorrectionBCPDirPhi    fNNPerpElectronBCPDirPhi;

      ANNDisElectronEvent6                      fANNDisElectronEvent;
      ANNElectronThruFieldEvent2                fANNFieldEvent2;
      ANNElectronThruFieldEvent3                fANNFieldEvent3;

      Double_t                                  fNNParams[20];

   public:
      SANEElectronSelector(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis)  {
         SetNameTitle("SANEElectronSelector", "SANEElectronSelector");

         fTrajectory = new InSANEDISTrajectory();
         fTrajectory->SetRun(InSANERunManager::GetRunManager()->fCurrentRun);
         fGroups = new std::vector<double>;
         fIsPerp = false;
      }

      ~SANEElectronSelector() {   }

      /** Initializes the calculation and output tree, "DISElectrons".
       *
       */
      virtual Int_t Initialize() {
         if (SANERunManager::GetRunManager()->GetVerbosity() > 2)
            std::cout << " o SANEElectronSelectors::Initialize()\n";
         BETACalculationWithClusters::Initialize();
         SANERunManager::GetRunManager()->GetCurrentFile()->cd();
        
         if(SANERunManager::GetRunManager()->IsPerpendicularRun()) fIsPerp = true;

         fOutTree = new TTree("DISElectrons", "DIS Electrons");
         fOutTree->Branch("trajectory", "InSANEDISTrajectory", &fTrajectory);
         fOutTree->Branch("triggerEvent", "InSANETriggerEvent", &(fEvents->TRIG));
         fOutTree->Branch("groups", fGroups);
         return(0);
      }

      /** Calculates the asymmetry
       *
       *\todo perhaps MakePlots is not the best name for this method
       */
      void MakePlots() {
         auto Nplus = (Double_t) InSANERunManager::GetRunManager()->fCurrentRun->fTotalNPlus;
         auto Nminus = (Double_t) InSANERunManager::GetRunManager()->fCurrentRun->fTotalNMinus;
         SANERunManager::GetRunManager()->fCurrentRun->fTotalAsymmetry = (Nplus - Nminus) / (Nplus + Nminus);
      }

      virtual void FillTree() {
         if (fOutTree) if (fcGoodElectron) fOutTree->Fill();
      }

      /** Adding cluster event
       * \todo is this needed?
       */
      //   void SetEvents(InSANEAnalysisEvents * e) {
      //       BETACalculation::SetEvents(e);
      //       this->SetClusterEvent(fEvents->CLUSTER);
      //   }
      //
      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  SANEElectronSelector1 - \n ";
         std::cout <<  "     Loops over clusters and fills a new branch of object InSANEDISTrajectory.\n";
         std::cout <<  "     The new tree is called DISElectrons. An artifical neural network is used to \n";
         std::cout <<  "     calculate the angular and energy corrections from the cluster quantities. \n";
         std::cout <<  "     Sets the values of the cluster's corrections for main detector tree as well. \n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

      /** Selects the clean electrons or non electrons and sets the tree filling flags.
       *
       *  Does the following for each cluster:
       *
       */
      Int_t Calculate() ;

   public:

      Bool_t fcGoodElectron;
      Bool_t fcOKElectron;
      Bool_t fcBadElectron;

      std::vector<double>  * fGroups;

      ClassDef(SANEElectronSelector, 3)
};


/** Creates the cleanedEvents tree which has a branch of CleandBETAEvent
 *
 * \ingroup Calculations
 */
class SANEElectronSelector2 : public BETACalculationWithClusters  {
   public:
      CleanedBETAEvent * fCleanedEvent;
      Bool_t fcGoodElectron;

   public:
      SANEElectronSelector2(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis)  {
         SetNameTitle("SANEElectronSelector2", "SANEElectronSelector2");
         fCleanedEvent = new CleanedBETAEvent();
         /*
            fOutTree = new TTree("cleanedEvents","cleaned BETA events");
            fOutTree->Branch("cleanedBETA","CleanedBETAEvent",&fCleanedEvent);
            fOutTree->Branch("triggerEvent",&(fEvents->TRIG) );*/
      }

      ~SANEElectronSelector2() {
      }

      /**  Initialize
       *  Used here for output tree.
       */
      virtual Int_t Initialize() {
         if (SANERunManager::GetRunManager()->GetVerbosity() > 2)
            std::cout << " o SANEElectronSelector2::Initialize()\n";
         BETACalculationWithClusters::Initialize();
         InSANERunManager::GetRunManager()->GetCurrentFile()->cd();

         fOutTree = new TTree("cleanedEvents", "cleaned BETA events");
         fOutTree->Branch("cleanedBETA", "CleanedBETAEvent", &fCleanedEvent);
         fOutTree->Branch("triggerEvent", "InSANETriggerEvent", &(fEvents->TRIG));
         return(0);
      }

      /**
       *
       */
      void MakePlots() {
         /*      Double_t Nplus = (Double_t) InSANERunManager::GetRunManager()->fCurrentRun->fTotalNPlus;
                 Double_t Nminus = (Double_t) InSANERunManager::GetRunManager()->fCurrentRun->fTotalNMinus;
                 InSANERunManager::GetRunManager()->fCurrentRun->fTotalAsymmetry = (Nplus-Nminus)/(Nplus+Nminus);*/

      }

      virtual void FillTree() {
         if (fOutTree) if (fcGoodElectron) fOutTree->Fill();
      }

      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  SANEElectronSelector2 - \n ";
         std::cout <<  "       Creates a cleanedEvents Tree with selected events. \n";
         std::cout <<  "_______________________________________________________________________________\n";
      }


      /**
       *
       */
      Int_t Calculate() ;

      ClassDef(SANEElectronSelector2, 1)
};



/** Creates the elastics tree which has a branch of
 *
 * \ingroup Calculations
 */
class SANEElasticElectronSelector : public BETACalculationWithClusters  {
   public:
      Bool_t fcGoodElectron;

   public:
      SANEElasticElectronSelector(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis)  {
         SetNameTitle("SANEElasticElectronSelector", "SANEElasticElectronSelector");

         /*
            fOutTree = new TTree("cleanedEvents","cleaned BETA events");
            fOutTree->Branch("cleanedBETA","CleanedBETAEvent",&fCleanedEvent);
            fOutTree->Branch("triggerEvent",&(fEvents->TRIG) );*/

      }

      ~SANEElasticElectronSelector() {
      }

      /**  Initialize
       *  Used here for output tree.
       */
      virtual Int_t Initialize() {
         if (SANERunManager::GetRunManager()->GetVerbosity() > 2)
            std::cout << " o SANEElasticElectronSelector::Initialize()\n";

         BETACalculationWithClusters::Initialize();
         InSANERunManager::GetRunManager()->GetCurrentFile()->cd();

         fOutTree = nullptr;
         fOutTree = fEvents->fTree->CloneTree(0);
         fOutTree->SetNameTitle("elastics", "Elastic Events");

         return(0);
      }

      /**
       *
       */
      void MakePlots() {

      }

      virtual void FillTree() {
         if (fOutTree) if (fcGoodElectron) fOutTree->Fill();
      }

      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  SANEElasticElectronSelector - \n ";
         std::cout <<  "       Elastic events \n";
         std::cout <<  "_______________________________________________________________________________\n";
      }


      /**
       *
       */
      Int_t Calculate() ;

      ClassDef(SANEElasticElectronSelector, 1)
};



#endif

