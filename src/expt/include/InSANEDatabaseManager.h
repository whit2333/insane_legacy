#ifndef InSANEDatabaseManager_HH
#define InSANEDatabaseManager_HH 1

#include <iostream>
//#include <vector>
//#include <cstdlib>
//#include "TROOT.h"
#include "TObject.h"
#include "TSQLServer.h"
#include "TSQLResult.h"
#include "TSQLRow.h"
#include "TString.h"
#include "TList.h"
//#include "sqlite3.h"
//#include <stdlib.h>

/** Base class for a database manager singleton.
 *
 *  \ingroup Database
 *  \ingroup Management
 */
class InSANEDatabaseManager : public TObject {
   public:
      enum InSANEDBType {
         kSQLite3,
         kMySQL,
         kBoth
      };

   protected:
      TString fTable;            /// \deprecated
      TString fWhereClause;      /// \deprecated

      TString fUserName;         /// \deprecated
      TString fPassword;         /// \deprecated
      TString fDatabaseName;     /// \deprecated
      TString fDatabaseFile;     /// \deprecated
      TString fDatabasePath;     /// \deprecated
      TString fDBServer;         /// \deprecated
      TString fConnectionString; /// \deprecated

      TString         fMyCnfGroup;
      InSANEDBType    fDBType;
      TSQLServer    * fMySQLServer;
      //sqlite3       * fSQLite3Server;
      void       * fSQLite3Server;

   public :
      static  InSANEDatabaseManager *  GetManager();
      ~InSANEDatabaseManager();

      InSANEDBType  GetDBType() { return(fDBType); }

      /** Opens a connetion to the server.
       *  It uses the environment variable InSANE_DB_PATH as the
       *  the directory where the database files are stored.
       */
      Int_t         ConnectDB();

      /** Close the connections to free up mysql server.
       *  This is important to do because the server has a
       *  maximum amount of connections (typically 150) it can have
       *  open at one time. Close the connections if they won't be 
       *  used soon. 
       */ 
      Int_t         CloseConnections(){
         if( fMySQLServer ) delete fMySQLServer;
         fMySQLServer = nullptr;
         //if ( fSQLite3Server ) sqlite3_close( fSQLite3Server);
         fSQLite3Server = nullptr;
         return(0);
      }

      void          ConnectMySQL();
      TSQLServer *  GetMySQLConnection();

      /** \deprecated Do not us  SQLite3. It does not work on
       *  Network file system (NFS). 
       */
      void          ConnectSQLite3();
      //sqlite3 *     GetSQLite3Connection();
      void *        GetSQLite3Connection();

      //static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
      //   std::cout << "Number of args= " << argc << std::endl;
      //   for (int i = 0; i < argc; i++) {
      //      std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
      //   }
      //   std::cout << std::endl;
      //   return 0;
      //}

      Int_t         ExecuteInsert(TString * sql);

      void          SetTable(const char * table) { fTable = table; }
      const char *  GetTable() { return (fTable.Data()); }
      void          SetWhereClause(const char * where) { fWhereClause = where; }
      const char *  GetWhereClause() { return (fWhereClause.Data()); }

      const char *  GetDatabaseName() { return(fDatabaseName.Data()); }

   protected :

      static InSANEDatabaseManager* fgInSANEDatabaseManager;

      InSANEDatabaseManager();

      ClassDef(InSANEDatabaseManager, 2)
};


#endif

