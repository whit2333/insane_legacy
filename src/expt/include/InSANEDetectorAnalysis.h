#ifndef InSANEDetectorAnalysis_H
#define InSANEDetectorAnalysis_H

#include "SANEScalers.h"

#include "TROOT.h"
#include "InSANEAnalysis.h"
#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "SANEEvents.h"
#include "BigcalEvent.h"
#include "SANERunManager.h"
#include "InSANEDatabaseManager.h"

#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"
#include <exception>
#include "SANERunManager.h"
#include "BETADetectorPackage.h"


/**  ABC for an analysis of a detectors.
 *
 * \ingroup Analysis
 */
class InSANEDetectorAnalysis : virtual public InSANEAnalysis {

   public:
      BETADetectorPackage     * fBETADetector;      ///< 
      ForwardTrackerDetector  * fTrackerDetector;   ///<
      GasCherenkovDetector    * fCherenkovDetector; ///<
      LuciteHodoscopeDetector * fHodoscopeDetector; ///<
      BigcalDetector          * fBigcalDetector;    ///<

   public :

      /** "Detector analysis" constructor creates the instantiations of each InSANEDetector
       *   as well as event memory (through creating a new SANEEvents() object )
       *
       *   The constructor does the following
       *   <ul>
       *   <li>Creates the SANEEvents object  \todo Maybe this shouldn't be in the c'tor?</li>
       *   <li>Creates the SANEScalers object  \todo Maybe this shouldn't be in the c'tor?</li>
       *   <li>Sets this classes event object pointers to the newly created SANEEvents objects </li>
       *   <li>Instantiates BETA Detector package and each detector object in it \todo again is this the
       *   place for this?</li>
       *   </ul>
       */
      InSANEDetectorAnalysis(const char * sourceTreeName = "correctedBetaDetectors");
      virtual ~InSANEDetectorAnalysis();

      virtual Int_t AnalyzeRun(); ///< Starts the loop over events and fills histograms.
      virtual Int_t Visualize() ; ///< 
      virtual Int_t AllocateHistograms() { return(0) ; }///< Allocates and defines histograms.

      ClassDef(InSANEDetectorAnalysis,5);
};


#endif

