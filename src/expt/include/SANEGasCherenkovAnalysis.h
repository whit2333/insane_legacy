#ifndef SANEGasCherenkovAnalysis_H
#define SANEGasCherenkovAnalysis_H
#include "TROOT.h"

#include "InSANEDetectorAnalysis.h"

#include "HMSEvent.h"
#include "HallCBeamEvent.h"
#include "BETAEvent.h"
#include "SANEEvents.h"
#include "InSANEDatabaseManager.h"
#include "ForwardTrackerDetector.h"
#include "GasCherenkovDetector.h"
#include "LuciteHodoscopeDetector.h"
#include "BigcalDetector.h"



/**  Concrete class implementation of InSANEAnalysis for the gas cherenkov
 *
 *  Fills a tree in a file CherenkovRUNNUMBER.root with a set selection criteria
 *
 *
 * \ingroup Analyses
 */
class SANEGasCherenkovAnalysis : public InSANEDetectorAnalysis {
public :

   SANEGasCherenkovAnalysis(const char * sourceTreeName = "correctedBetaDetectors");

   virtual ~SANEGasCherenkovAnalysis();
   /**
    *  Define histograms to fill inside loop
    */
   virtual Int_t  AllocateHistograms() {
      return(0);
   }

   Int_t AnalyzeRun();
   Int_t Visualize() ;

////// HISTOGRAMS ///////
   TH1F * cer_tdc_hist[12];
   TH1F * cer_adc_hist[12];
   TH2F * cer_tdc_vs_adc_hist[12];

   TH1F * cer_tdc_hist_luc[12*56];
   TH1F * cer_adc_hist_luc[12*56];
   TH2F * cer_tdc_vs_adc_hist_luc[12*56];

   TH1F * luc_tdc_hist[12*56];
   TH1F * luc_adc_hist[12*56];
   TH2F * luc_tdc_vs_adc_hist[12*56];

   ClassDef(SANEGasCherenkovAnalysis, 2);
};


#endif

