#ifndef BigcalCalculations_H
#define BigcalCalculations_H 1
#include "BIGCALClusterProcessor.h"
#include "BETACalculation.h"
#include "BigcalEvent.h"
#include "LocalMaxClusterProcessor.h"
#include "Pi0Event.h"
#include "TVector3.h"
#include "TMatrixD.h"

/**  First pass Calculations for Bigcal
 *
 *  \ingroup Calculations
 * \ingroup BIGCAL
 */
class BigcalCalculation1 : public BETACalculation {
public:
   BigcalCalculation1(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis)  {
      SetNameTitle("BigcalCalculation1", "BigcalCalculation1");

      fOutTree = nullptr;
   }

   ~BigcalCalculation1() {

   };

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  BigcalCalculation1 - \n ";
      std::cout <<  "      \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   /**
    *  First Pass Clustering is done here.
    *  For each event the following is done:
    *  -Calorimeter Clustering
    */
   virtual Int_t Calculate() ;

   ClassDef(BigcalCalculation1, 1)
};
//_______________________________________________________//
#endif

