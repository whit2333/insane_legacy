#ifndef SANEAsymmetryCalculation_HH
#define SANEAsymmetryCalculation_HH 1

#include "BETACalculation.h"
#include "SANEClusteringAnalysis.h"
#include "InSANEAnalyzer.h"
#include "InSANEDISTrajectory.h"
#include "InSANERunManager.h"
#include "SANERunManager.h"
//#include "NNParallel.h"
//#include "NNPerpendicular.h"
#include "CleanedBETAEvent.h"
#include "InSANETriggerEvent.h"
#include "InSANEDilutionFactor.h"
#include "SANEMeasuredAsymmetry.h"
//#include "NNParaElectronCorrectionEnergy.h"
//#include "NNParaElectronCorrectionTheta.h"
//#include "NNParaElectronCorrectionPhi.h"
//#include "NNPerpElectronCorrectionEnergy.h"
//#include "NNPerpElectronCorrectionTheta.h"
//#include "NNPerpElectronCorrectionPhi.h"
#include <vector>
//#include "ANNEvents.h"

/** Creates the asymmetries and relies on having the SANEElectronSelector1 preceed it.
 *  It needs to have the pointer to fTrajectory set.
 *
 *
 *  \ingroup Calculations
 */
class SANEAsymmetryCalculation1 : public BETACalculationWithClusters  {
public:
   SANEAsymmetryCalculation1(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis)  {
      SetNameTitle("SANEAsymmetryCalculation1", "SANEAsymmetryCalculation1");
      fAsymmetries = new TList();
      fTrajectory = nullptr;
      fDISEvent = new InSANEDISEvent();
   }

   ~SANEAsymmetryCalculation1() {
   }

   /**  Initialize the asymmetries. The kinematic binning is defined here.
    *
    */
   virtual Int_t Initialize() {
      if (SANERunManager::GetRunManager()->fVerbosity > 2)
         std::cout << " o SANEAsymmetryCalculation1::Initialize()\n";

      BETACalculationWithClusters::Initialize();
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
      InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("asymmetryHists");
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("asymmetryHists");
      /*
      fOutTree = 0;
      fOutTree = fEvents->fTree->CloneTree(0);
      fOutTree->SetNameTitle("elastics", "Elastic Events");*/
      fDISEvent->fBeamEnergy = InSANERunManager::GetRunManager()->GetCurrentRun()->GetBeamEnergy()/1000.0;

      std::cout << fDISEvent->fBeamEnergy << " BEAM ENERGY \n";

      Double_t QsqMin[4] = {1.89, 2.55, 3.45, 4.67};
      Double_t QsqMax[4] = {2.55, 3.45, 4.67, 6.31};

      InSANERunSummary fRunSummary = InSANERunManager::GetRunManager()->GetCurrentRun()->GetRunSummary();

      /// Protvino
      for (int i = 0; i < 4; i++) {
         auto * asym = new SANEMeasuredAsymmetry(i);
         asym->fQ2Min = QsqMin[i];
         asym->fQ2Max = QsqMax[i];
         asym->fGroup = 1;
         //asym->fBeamEnergy =  fDISEvent->fBeamEnergy ;
         asym->fDISEvent = fDISEvent;
         asym->fRunSummary = fRunSummary;//SetDirectory(f);
         fAsymmetries->Add(asym);
         asym->Initialize();
         InSANERunManager::GetRunManager()->GetCurrentRun()->fAsymmetries.Add(asym);
      }
      /// RCS
      for (int i = 4; i < 8; i++) {
         auto * asym = new SANEMeasuredAsymmetry(i);
         asym->fQ2Min = QsqMin[i-4];
         asym->fQ2Max = QsqMax[i-4];
         asym->fGroup = 2;
         //asym->fBeamEnergy =  fDISEvent->fBeamEnergy ;
         asym->fDISEvent = fDISEvent;
         asym->fRunSummary = fRunSummary;//SetDirectory(f);
         asym->Initialize();
         InSANERunManager::GetRunManager()->GetCurrentRun()->fAsymmetries.Add(asym);
         fAsymmetries->Add(asym);
      }

      /// Toroidal Mirror half
      for (int i = 8; i < 12; i++) {
         auto * asym = new SANEMeasuredAsymmetry(i);
         asym->fQ2Min = QsqMin[i-8];
         asym->fQ2Max = QsqMax[i-8];
         asym->fGroup = 3;
         //asym->fBeamEnergy =  fDISEvent->fBeamEnergy ;
         asym->fDISEvent = fDISEvent;
         asym->Initialize();
         fAsymmetries->Add(asym);
         InSANERunManager::GetRunManager()->GetCurrentRun()->fAsymmetries.Add(asym);
      }
      /// Spherical Mirror half
      for (int i = 12; i < 16; i++) {
         auto * asym = new SANEMeasuredAsymmetry(i);
         asym->fQ2Min = QsqMin[i-12];
         asym->fQ2Max = QsqMax[i-12];
         asym->fGroup = 4;
         //asym->fBeamEnergy =  fDISEvent->fBeamEnergy ;
         asym->fDISEvent = fDISEvent;
         asym->fRunSummary = fRunSummary;//SetDirectory(f);
         asym->Initialize();
         fAsymmetries->Add(asym);
         InSANERunManager::GetRunManager()->GetCurrentRun()->fAsymmetries.Add(asym);
      }

      InSANERunManager::GetRunManager()->GetCurrentFile()->cd();

      return(0);
   }

   /**
    *
    */
   void MakePlots() {
      SANERunManager::GetRunManager()->GetCurrentFile()->cd();
      //InSANEMeasuredAsymmetry * asym = 0;
      /*      for(int i = 0;i<fAsymmetries->GetEntries();i++){
               asym = ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(i)));
               asym->fChargePlus = InSANERunManager::GetRunManager()->fCurrentRun->fTotalQPlus;
               asym->fChargeMinus= InSANERunManager::GetRunManager()->fCurrentRun->fTotalQMinus;
            }
      */
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
      //SANERunManager::GetRunManager()->fCurrentRun->fAsymmetries.Write("BinnedAsymmetries");//,TObject::kOverwrite);

   }

   virtual void FillTree() {
      if (fOutTree) if (fcGoodElectron) fOutTree->Fill();
   }

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  SANEAsymmetryCalculation1 - \n ";
      std::cout <<  "       Calculates the binned asymmetries \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   Int_t Finalize() {
      InSANERunSummary fRunSummary = InSANERunManager::GetRunManager()->GetCurrentRun()->GetRunSummary();

      for (int i = 0; i < 16; i++) {
         auto* asym = (SANEMeasuredAsymmetry*)(fAsymmetries->At(i));
         asym->fRunSummary = fRunSummary;
         asym->CalculateAsymmetries();
      }
      TFile * f = InSANERunManager::GetRunManager()->GetCurrentFile();
      f->cd();
      f->WriteObject(fAsymmetries,Form("asymmetries-%d",0));//,TObject::kSingleKey); 
      return(0);
   }


   /**
    *
    */
   Int_t Calculate() ;

public:
   Bool_t fcGoodElectron;
   InSANEDISTrajectory * fTrajectory;
   InSANEDISEvent * fDISEvent;
   TList * fAsymmetries;


   ClassDef(SANEAsymmetryCalculation1, 2)
};



/** Creates the asymmetries and relies on having the SANEElectronSelector1 preceed it.
 *  It needs to have the pointer to fTrajectory set.
 *
 *
 *  \ingroup Calculations
 */
// class SANEAsymmetryCalculation2 : public SANEAsymmetryCalculation1 {
// public:
// 
//    SANEAsymmetryCalculation2(InSANEAnalysis * analysis) : SANEAsymmetryCalculation2(analysis)  {
//       
//       fTrajectory = new InSANEDISTrajectory();
// 
//    }
// 
//    ~SANEAsymmetryCalculation2() {
//    }
// 
//    Int_t Calculate(){
//       
//    }
// 
// ClassDef(SANEAsymmetryCalculation2,1)
// };
// 

// class SANEAsymmetryCalculation2 : public SANEAsymmetryCalculation1  {
// public:
//    SANEAsymmetryCalculation2(InSANEAnalysis * analysis) : SANEAsymmetryCalculation1(analysis) {
//       fFirstGroup = 20;
//       fNPELowCut[0] = 1.0;
//       fNPELowCut[1] = 2.5;
//       fNPELowCut[2] = 5.0;
//       fNPELowCut[3] = 7.5;
//       fNPEHighCut[0] = 30.0 - fNPELowCut[0];
//       fNPEHighCut[1] = 30.0 - fNPELowCut[1];
//       fNPEHighCut[2] = 30.0 - fNPELowCut[2];
//       fNPEHighCut[3] = 30.0 - fNPELowCut[3];
//    }
//    ~SANEAsymmetryCalculation2(){}
//
//
//    /**  Initialize the asymmetries. The kinematic binning is defined here.
//     *
//     */
//    virtual Int_t Initialize() {
//       if(SANERunManager::GetRunManager()->fVerbosity>2)
//          std::cout << " o SANEAsymmetryCalculation1::Initialize()\n";
//
//       BETACalculationWithClusters::Initialize();
//       InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
//       InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("asymmetryHists");
//       InSANERunManager::GetRunManager()->GetCurrentFile()->cd("asymmetryHists");
//       /*
//       fOutTree = 0;
//       fOutTree = fEvents->fTree->CloneTree(0);
//       fOutTree->SetNameTitle("elastics", "Elastic Events");*/
//       fDISEvent->fBeamEnergy = InSANERunManager::GetRunManager()->GetCurrentRun()->GetBeamEnergy();
//
//       Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
//       Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
//
//       /// Protvino
//       for(int i=0;i<4;i++){
//          InSANEMeasuredAsymmetry * asym = new InSANEMeasuredAsymmetry(i);
//          asym->fQ2Min = QsqMin[i];
//          asym->fQ2Max = QsqMax[i];
//          asym->fGroup = 1;
//     asym->fDISEvent = fDISEvent;
//     fAsymmetries->Add(asym);
//     InSANERunManager::GetRunManager()->GetCurrentRun()->fAsymmetries.Add(asym);
//       }
//       /// RCS
//       for(int i=4;i<8;i++){
//          InSANEMeasuredAsymmetry * asym = new InSANEMeasuredAsymmetry(i);
//          asym->fQ2Min = QsqMin[i-4];
//          asym->fQ2Max = QsqMax[i-4];
//          asym->fGroup = 2;
//     asym->fDISEvent = fDISEvent;
//     fAsymmetries->Add(asym);
//     InSANERunManager::GetRunManager()->GetCurrentRun()->fAsymmetries.Add(asym);
//       }
//
//       InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
//
//       return(0);
//    }
//
//    /**
//     *
//     */
//    void MakePlots() {
//       SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//       InSANEMeasuredAsymmetry * asym = 0;
// /*      for(int i = 0;i<fAsymmetries->GetEntries();i++){
//          asym = ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(i)));
//          asym->fChargePlus = InSANERunManager::GetRunManager()->fCurrentRun->fTotalQPlus;
//          asym->fChargeMinus= InSANERunManager::GetRunManager()->fCurrentRun->fTotalQMinus;
//       }
// */
//       InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
//       //SANERunManager::GetRunManager()->fCurrentRun->fAsymmetries.Write("BinnedAsymmetries");//,TObject::kOverwrite);
//
//    }
//
//    virtual void FillTree() {
//      if(fOutTree) if(fcGoodElectron) fOutTree->Fill();
//    }
//
//    void Describe() {
//   std::cout <<  "===============================================================================\n";
//   std::cout <<  "  SANEAsymmetryCalculation2 - \n ";
//   std::cout <<  "       Calculates asymmetries \n";
//   std::cout <<  "_______________________________________________________________________________\n";
//    }
//
//
//    /**
//     *
//     */
//    Int_t Calculate() ;
//
// public:
//    Int_t    fFirstGroup;
//    Double_t fNPELowCut[4];
//    Double_t fNPEHighCut[4];
//
// ClassDef(SANEAsymmetryCalculation2,1)
// };

#endif
