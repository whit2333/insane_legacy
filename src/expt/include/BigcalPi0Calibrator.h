#ifndef BigcalPi0Calibrator_H
#define BigcalPi0Calibrator_H 1
#include "TMatrixD.h"
#include "Pi0Calibration.h"
#include "Math/IFunction.h"

#include "InSANEAnalyzer.h"
#include "SANEClusteringAnalysis.h"
#include "TString.h"
#include "SANERunManager.h"

#include "Math/IFunction.h"
#include "Math/IParamFunction.h"
#include "Math/Functor.h"
#include "TMath.h"
#include <algorithm>
#include <vector>

#ifndef PI0MASSSQUARED
#define PI0MASSSQUARED 18218.683
#endif

/** Functor for Calculating the photon energy (and the derivative)
 * \ingroup pi0
 */
class Pi0PhotonEnergy { /* :  public ROOT::Math::IBaseFunctionMultiDim*/

   protected:

      const double * pars;
      double         g;
      int            fPhotonNumber;
      int            fnDim;

   public:
      std::vector<int> * fShowerBlocks;
      double         fDeltaE;

   public:
      Pi0PhotonEnergy(int n = 0) {
         fDeltaE       = 0.0;
         fPhotonNumber = n;
         fnDim         = 1744;
         g             = 0;
         fShowerBlocks = new std::vector<int>;
      }

      virtual ~Pi0PhotonEnergy() { }
      unsigned int NDim() const { return fnDim; }
      Pi0PhotonEnergy * Clone() const { return new Pi0PhotonEnergy(); } 

      /** A function of \nu\prime( \vector{epsilon} )
       *  The parameters are the energies of all the blocks
       */
      double operator()(const double *x) const { return(Eval(x)); }

      unsigned int  NPar() const { return 1744; }

      const double* Parameters() const { return pars; }
      int           GetPhotonNumber() const { return(fPhotonNumber); }
      void          SetParameters(const double* p) { pars = p; }
      void          SetPhotonNumber(const int n) { fPhotonNumber = n; }

      double Eval(const double *x) const {
         double sij = 0;
         for (std::vector<int>::const_iterator it = fShowerBlocks->begin(); it != fShowerBlocks->end(); ++it) {
            sij += pars[*it] * (1.0 + x[*it]) ;
         }
         return( (1.0 + g)*sij + fDeltaE);
      }

      double  Derivative(const double * x,   int icoord) const {
         double pij       = Eval(x);
         double sj        = 0;
         double EijBlock  = pars[icoord];
         for (std::vector<int>::const_iterator it = fShowerBlocks->begin(); it != fShowerBlocks->end(); ++it) {
            //pij += pars[*it];
            sj  += pars[*it]*(1.0 + x[*it]) ;
         }
         return( pij*EijBlock/(sj) );
      }

      bool HasBlock(int block) const {
         if( std::find(fShowerBlocks->begin(),fShowerBlocks->end(),block) != fShowerBlocks->end() ) return true;
         return false;
      }

      ClassDef(Pi0PhotonEnergy, 1)
};

//_____________________________________________________

#include "BIGCALCluster.h"

/** Functor for Calculating the derivative of the mass squared
 *  m^{2}_{i}(\epsilon) =  2 \nu_{1i}  \nu_{2i} (1-cos(\theta))
 *
 * \ingroup pi0
 */
class Pi0MassSquared { /*:  public ROOT::Math::IParametricFunctionMultiDim*/

   private:
      const double* pars;
      int fNShowers;

   protected:
      int fnDim;

   public:
      double nu0;
      double nu1;
      double fCosTheta;
      Pi0PhotonEnergy *p1;
      Pi0PhotonEnergy *p0;

   public:
      Pi0MassSquared(int n = 2) {
         fNShowers = n;
         fCosTheta = 0.0;
         p1        = nullptr;
         p0        = nullptr;
         pars      = nullptr;
         nu0       = 0.0;
         nu1       = 0.0;
         fnDim     = 1744;
      }

      virtual ~Pi0MassSquared() {   }

      Pi0MassSquared * Clone() const {
         return new Pi0MassSquared();
      }

      /** Implementing pure virtual method in IBaseFunctionMultiDim */
      double operator()(const double *x) const { return(Eval(x)); }

      double Eval(const double * x) const {
         if (!pars) {
            std::cout << "Block energies not set! \n" ;
            return(0.0);
         }
         return(2.0 * p0->Eval(x) * p1->Eval(x) * (1.0 - fCosTheta));
      }

      unsigned int NDim() const { return fnDim; }

      /** Returns the shower number for a block if it is in a shower otherwise it returns -1; */
      int ShowerNumberOfBlock(const int  block) const {
         if (!pars) {
            std::cout << "Block energies not set! b\n" ;
            return(-1);
         } else {
            for (std::vector<int>::const_iterator it = p0->fShowerBlocks->begin(); it != p0->fShowerBlocks->end(); ++it)
               if (block == (*it))   return(0);
            for (std::vector<int>::const_iterator it = p1->fShowerBlocks->begin(); it != p1->fShowerBlocks->end(); ++it)
               if (block == (*it))   return(1);
         }
         return(-1);
      }

      const double* Parameters() const { return pars; }
      void SetParameters(const double* p) { pars = p; }
      unsigned int NPar() const { return 1744; }

      double  Derivative(const double * x,   int icoord) const {
         double EijBlock = 0.0;
         int showerN = ShowerNumberOfBlock(icoord);
         //std::cout << "shower number " << showerN << "\n";
         if (showerN == -1)  {
            EijBlock = 0;
            return(0.0);
         } else {
            EijBlock = pars[icoord];
            //std::cout << "EijBlock "  << EijBlock << "\n";
            double mij        = Eval(x);
            //std::cout << "mij "  << mij << "\n";
            Pi0PhotonEnergy * photon = nullptr;
            if (showerN == 0)  photon = p0;
            else if (showerN == 1) photon = p1;
            else return(0.0);
           
            double sj        = photon->Eval(x);
            //for (std::vector<int>::const_iterator it = photon->fShowerBlocks->begin(); it != photon->fShowerBlocks->end(); ++it) {
            //   sj += pars[*it];
            //}
            //std::cout << "sj = " << sj << std::endl;
            return mij * EijBlock /(sj) ;
         }
      };

      ClassDef(Pi0MassSquared, 1)
};
//_____________________________________________________

/**  Bigcal Pi0 Calibrator
 *
 *  This class loops through an event filling in the matricies
 *  needed to calculate the energy corrections for each block of bigcal
 *
 *  See NIM  A 566 366-374 (RADPHI)
 *
 * \ingroup pi0
 */
class BigcalPi0Calibrator :  public SANEClusteringAnalysis, public InSANEAnalyzer {
   public:
      BigcalPi0Calibrator(const char * newTreeName = "Pi0Calibration",
            const char * uncorrectedTreeName = "betaDetectors1");
      virtual ~BigcalPi0Calibrator();

      /** Initialize the input sources */
      virtual void Initialize(TTree * t = nullptr);

      /** make the histograms which will fed to an event or calibration display */
      virtual void MakePlots();

      ClassDef(BigcalPi0Calibrator, 2)
};


#endif

