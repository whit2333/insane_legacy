#ifndef GasCherenkovEvent_h
#define GasCherenkovEvent_h
#include <iostream>
#include <vector>

#include <TROOT.h>
 //#include <TChain.h>
#include "InSANEDetectorEvent.h"

/** Concrete class for hits in the SANE gas cherenkov built by Temple University.
 *
 *  \ingroup Events
 *  \ingroup Cherenkov
 */
class GasCherenkovEvent : public InSANEDetectorEvent {
   public :
      GasCherenkovEvent();
      ~GasCherenkovEvent();

      /** Allocates memory for TClonesArray containing GasCherenkovHits */
      Int_t AllocateHitArray();

      /// Number of ADC channels to be readout for the event
      Int_t   fNumberOfADCHits;
      /// Sum of the the number of tdc hits on each channel
      Int_t   fNumberOfTDCHits;
      /// Number of ADC channels to be readout for the event AND pass timing cut
      Int_t   fNumberOfTimedADCHits;
      /// Sum of the the number of tdc hits on each channel AND pass timing cut
      Int_t   fNumberOfTimedTDCHits;

      TClonesArray * fGasCherenkovHits;    //->  array with all hits
      TClonesArray * fGasCherenkovTDCHits; //->  array with all TDC hits
      TClonesArray * fGasCherenkovADCHits; //->  array with all ADC hits

      /// Sums calculated by GetNPESum(int i)
      Double_t fNPESums[12];

      /** Clears the event  */
      void ClearEvent(Option_t * opt = "");

      void Print(const Option_t * opt = "") const {
         InSANEDetectorEvent::Print(opt);
         std::cout << " GasCherenkovEvent object " << this << std::endl;
         std::cout << " fNumberOfADCHits = " << fNumberOfADCHits << std::endl;
         std::cout << " fNumberOfTDCHits = " << fNumberOfTDCHits << std::endl;
         std::cout << " fNumberOfTimedADCHits = " << fNumberOfTimedADCHits << std::endl;
         std::cout << " fNumberOfTimedTDCHits = " << fNumberOfTimedTDCHits << std::endl;
         std::cout << " GasCherenkovEvent::fHits = " << fGasCherenkovHits << std::endl;
         std::cout << " fGasCherenkovTDCHits = " << fGasCherenkovTDCHits << std::endl;
         std::cout << " fgGasCherenkovHits = " << fgGasCherenkovHits << std::endl;
      }

   private :
      static TClonesArray * fgGasCherenkovHits; //!
      static TClonesArray * fgGasCherenkovTDCHits; //!
      static TClonesArray * fgGasCherenkovADCHits; //!

      ClassDef(GasCherenkovEvent, 5)
};


#endif

