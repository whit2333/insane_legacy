#ifndef SANEScalers_H
#define SANEScalers_H

#include "InSANEAnalysisEvents.h"
#include "SANEScalerEvent.h"
#include "InSANETriggerEvent.h"
#include "TChain.h"

/** Concrete class for SANE Scaler Events which is used as a simplified
 * interface to the Event classes with file I/O.
 *
 * \note { This class should not be streamed to a tree!! }
 *
 *  \ingroup Scalers
 * \ingroup Analysis
 */
class SANEScalers : public InSANEAnalysisEvents {

   public :
      SANEScalerEvent    * fScalerEvent;
      InSANETriggerEvent * fTriggerEvent;
      Int_t                fRunNumber;
      TFile              * fScalerFile;
      TChain             * fScalerTree;
      TTree              * fTree;

   public :

      /**  Finds the trees in the currently opened files and sets their branch addresses.  */
      SANEScalers(const char * treename = "Scalers");

      virtual ~SANEScalers();

      /**
       *  For each input tree
       *  it sets the appropriate Branches
       *   \todo{input trees are hard coded: Not a bad thing but maybe better?}
       */
      Int_t SetBranches();

      Int_t SetBranches(TTree * t);

      /**  For each input tree, tree->GetEvent(#) is called. 
       *   Returns 0 on success.
       */
      virtual Int_t GetEvent(Long64_t);
      virtual void  ClearEvent(Option_t * opt = "");
      virtual void  FillTrees() ;

      ClassDef(SANEScalers, 5)
};


#endif

