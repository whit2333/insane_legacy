#ifndef BigcalPi0Calculations_H
#define BigcalPi0Calculations_H 1

#include "BETACalculation.h"
#include "Pi0Event.h"
#include "Pi0ClusterPair.h"
#include "BigcalCoordinateSystem.h"
#include "TRotation.h"
#include "TVector3.h"
#include "Pi03ClusterEvent.h"
//#include "SANENeuralNetworks.h"
//#include "ANNEvents.h"

#include "ANNClusterRecon.h"


/** Pi0 Calculation which on includes the decay, \f$ pi_0 \rightarrow \gamma \gamma \f$.
 *
 *  Loops over bigcal clusters twice to make all combinations and creates a pi0clusterpair
 *
 *  Outputs a tree for analysis of pi0 reconstruction for the \f$ pi_0 \rightarrow \gamma \gamma \f$ decay alone.
 *  See Bigcal3ClusterP0Calculations.
 *
 *
 * \ingroup Calculations
 */
class BigcalPi0Calculations : public BETACalculationWithClusters {

   protected:
      BIGCALCluster            fCluster1;
      BIGCALCluster            fCluster2;

      TVector3                 origin ;
      TVector3                 v1     ;
      TVector3                 v2     ;
      TVector3                 k1     ;
      TVector3                 k2     ;

      ANNClusterRecon                         fANNClusterRecon;

      //ANNGammaEvent                           fANNGammaEvent;

      //NNParaGammaXYCorrectionClusterDeltaX    fNNParaDeltaX;
      //NNParaGammaXYCorrectionClusterDeltaY    fNNParaDeltaY;
      //NNParaGammaXYCorrectionEnergy           fNNParaDeltaE;

      //// FIX ME: Have not trained perp yet so we just use the parallel here...
      //NNParaGammaXYCorrectionClusterDeltaX    fNNPerpDeltaX;
      //NNParaGammaXYCorrectionClusterDeltaY    fNNPerpDeltaY;
      //NNParaGammaXYCorrectionEnergy           fNNPerpDeltaE;

      //Double_t                                fNNParams[20];
      Bool_t                                  fIsPerp;

   public:
      TRotation                  fRotFromBCcoords;
      Pi0Event                 * fPi0Event;
      BigcalCoordinateSystem   * bcCoords;

   public:
      BigcalPi0Calculations(InSANEAnalysis * analysis);
      virtual ~BigcalPi0Calculations();

      virtual Int_t Initialize();
      virtual void  Describe();
      Int_t         Calculate() ;
      virtual Int_t Finalize();

      ClassDef(BigcalPi0Calculations, 2)
};


//__________________________________________________________________________



/** Pi0 Calculation which on includes the decay, \f$ pi_0 \rightarrow \gamma \gamma^* \rightarrow e^+ e^- \gamma \f$.
 *
 *  Outputs a tree for analysis of pi0 reconstruction for the \f$ pi_0 \rightarrow \gamma \gamma \f$ decay alone.
 *  See Bigcal3ClusterP0Calculations.
 *
 *
 * \ingroup Calculations
 */
class Bigcal3ClusterPi0Calculation : public BETACalculationWithClusters {
public:
   Bigcal3ClusterPi0Calculation(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis) {
      SetNameTitle("Bigcal3ClusterPi0Calculation", "Bigcal3ClusterPi0Calculation");
      fOutTree = nullptr;
      bcCoords = new BigcalCoordinateSystem();
      /* TMatrix temp();*/
      fRotFromBCcoords = bcCoords->GetRotationMatrixTo();
      fOutTree = new TTree("3clusterPi0results", "pi0 Reconstruction");
      fPi0Event = new Pi03ClusterEvent();
      if (fOutTree) fOutTree->Branch("pi03ClusterEvents", "Pi03ClusterEvent", &fPi0Event);

   }

   ~Bigcal3ClusterPi0Calculation() {
      if (fOutTree)fOutTree->BuildIndex("pi0reconstruction.fRunNumber", "pi0reconstruction.fEventNumber");
      if (fOutTree)fOutTree->FlushBaskets();
      if (fOutTree)fOutTree->Write();
   }

   /** Method prints a description of the calculations implemented */
   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  " BigcalPi0Calaculations - \n ";
      std::cout <<  "       Loops over clusters and determines whether it is a good candidate for\n";
      std::cout <<  "       a photon coming from a pi0 decay. \n";
      std::cout <<  "       Then it reconstructs a pi0 from the selected clusters.\n";
      std::cout <<  "       Calculates neutral pion mass from first two clusters \n";///\todo calculate Mpi0 for all clusters in event
      std::cout <<  "_______________________________________________________________________________\n";
   }

   /** Calculates and reconstructs the pi0 mass
    */
   Int_t Calculate() ;

   TRotation fRotFromBCcoords;
   Pi03ClusterEvent * fPi0Event;
   BigcalCoordinateSystem * bcCoords;

   /*
      NNParaPositronCorrectionEnergy  fPositronNNEnergy;
      NNParaPositronCorrectionTheta   fPositronNNTheta;
      NNParaPositronCorrectionPhi     fPositronNNPhi;

      NNParaElectronCorrectionEnergy  fElectronNNEnergy;
      NNParaElectronCorrectionTheta   fElectronNNTheta;
      NNParaElectronCorrectionPhi     fElectronNNPhi ;

      NNParaGammaCorrectionEnergy  fGammaNNEnergy;
      NNParaGammaCorrectionTheta   fGammaNNTheta;
      NNParaGammaCorrectionPhi     fGammaNNPhi ;

      NNParaGammaCorrection fGammaNN;

      ANNDisElectronEvent * fANNEvent;


      Double_t positronInputs[7];
      Double_t electronInputs[7];
      Double_t gammaInputs[7];*/

   ClassDef(Bigcal3ClusterPi0Calculation, 1)
};


//______________________________________________________________________________________



/** Determines if event falls in the overlap region of the bigcal trigger
 *
 * \ingroup Calculations
 */
class BigcalOverlapCalculation : public BETACalculationWithClusters {
public:
   BigcalOverlapCalculation(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis) {
      SetNameTitle("BigcalOverlapCalculation", "BigcalOverlapCalculation");
      fOutTree = nullptr;
      fOutTree = new TTree("bigcalOverlap", "bical Overlap Events");
      fPi0Event = new Pi0Event();
      if (fOutTree) fOutTree->Branch("pi0reconstruction", "Pi0Event", &fPi0Event);
   }

   ~BigcalOverlapCalculation() {
      if (fOutTree)fOutTree->FlushBaskets();
      if (fOutTree)fOutTree->Write();
   }

   /** Method prints a description of the calculations implemented */
   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  " BigcalOverlapCalculation - \n ";
      std::cout <<  "      If a  pi0 event falls on the seam of RCS and Protvino \n";
      std::cout <<  "      then this event is thrown into\n";
      std::cout <<  "       \n";
      std::cout <<  "        \n";///\todo calculate Mpi0 for all clusters in event
      std::cout <<  "_______________________________________________________________________________\n";
   }

   /**
    *  Calculates and reconstructs the pi0 mass
    */
   Int_t Calculate() ;

   Pi0Event * fPi0Event;

   ClassDef(BigcalOverlapCalculation, 2)
};
//_______________________________________________________//
#endif

