#ifndef InSANEClusterEvent_h
#define InSANEClusterEvent_h

#include <TROOT.h>
#include <TObject.h>
#include <TH2.h>
#include <TClonesArray.h>
#include "InSANEDetectorEvent.h"
#include <TH2F.h>


/** A cluster event
 *
 * Base class for an event's "clustering". The "clustering" can be implemented anyway you want!
 * This ABC represents an event's worth of cluster objects (eg a branch on an event tree) and other info.
 *
 * \ingroup Clustering
 *
 * \ingroup Event
 */
class InSANEClusterEvent : public InSANEDetectorEvent {

   public:
      Double_t fCherenkovTotalNPE;
      Double_t fCherenkovTotalADC;
      Double_t fTwoClusterSeparation;
      Int_t    fNClusters;

      /// \deprecated
      Double_t fCherenkovTotalADC_GoodClust;
      Double_t fCherenkovTotalADC_GoodTDC;
      Double_t fCherenkovTotalNPE_GoodClust;
      Double_t fCherenkovTotalNPE_GoodTDC;

      TClonesArray   * fClusters; //! Array of clusters
      TH2F           * fClusterSourceHistogram; //!


   public:
      InSANEClusterEvent() ;
      virtual ~InSANEClusterEvent();

      /**  Reset the event */
      void Reset();

      /**  Clears the event. argument opt="C" calls clear for each object in TClonesArray fClusters */
      virtual void ClearEvent(Option_t * opt = ""){
         InSANEDetectorEvent::ClearEvent(opt);

         if (fClusters) if(fClusters->GetEntries() > 15 ) {
            std::cout << " calling  fClusters->ExpandCreate(5); " << " had " << fClusters->GetEntries() << " entries. \n";
            fClusters->ExpandCreate(5);
         }
         if (fClusters) fClusters->Clear(opt);
         fNClusters                   = 0;
         fCherenkovTotalNPE           = 0;
         fCherenkovTotalNPE_GoodClust = 0;
         fCherenkovTotalNPE_GoodTDC   = 0;
         fCherenkovTotalADC           = 0;
         fCherenkovTotalADC_GoodClust = 0;
         fCherenkovTotalADC_GoodTDC   = 0;
         fTwoClusterSeparation        = 0.0;
      }


      ClassDef(InSANEClusterEvent, 5)
};

#endif



