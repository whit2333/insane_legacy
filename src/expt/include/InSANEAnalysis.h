#ifndef InSANEAnalysis_H
#define InSANEAnalysis_H
#include "InSANEAnalysisEvents.h"
#include "TROOT.h"
#include "TNamed.h"
#include "SANEEvents.h"
#include "InSANEDatabaseManager.h"

#include "BIGCALGeometryCalculator.h"
#include "TClonesArray.h"
#include "BigcalHit.h"
#include "GasCherenkovEvent.h"
#include "GasCherenkovHit.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"
#include "ForwardTrackerEvent.h"
#include "ForwardTrackerHit.h"
#include "HallCBeamEvent.h"
#include "InSANEMonteCarloEvent.h"
#include "HMSEvent.h"
#include "InSANETriggerEvent.h"
#include "InSANERun.h"
#include "InSANERunManager.h"
#include "SANEScalers.h"

/**  ABC for a general analysis.
 *   An "Analysis" can be performed independent of an "Analyzer", ie InSANEAnalyzer. 
 *   An  instance of an "analysis" is meant to initialize everything it needs to do 
 *   some calculations, while the "analyzer" is meant to be the "engine of analysis".
 *   Through multiple inheritance of InSANEAnlyzer and a derived InSANEAnalysis class
 *   concrete analyzers are created. (see SANEZerothPassAnalyzer or SANEFirstPassAnalyzer).
 *
 * \ingroup Analysis
 */
class InSANEAnalysis : public virtual TNamed {

   protected:
      TFile                    * fOutputFile;   ///
      TFile                    * fAnalysisFile; ///
      TTree                    * fAnalysisTree; ///
      TTree                    * fOutputTree;   ///
      BIGCALGeometryCalculator * fBigcalGeo;    ///
      Bool_t                     fEventDisplay; ///
      Bool_t                     fRunDisplay;   ///
      Int_t                      fAnalysisType; ///< 
      Int_t                      fDebug;        ///< Debug level
      Int_t                      fRunNumber;    ///< Run number

      // The following are all pointers to the memory allocated by new SANEEvents
      BETAEvent              * aBetaEvent;
      BigcalEvent            * bcEvent;
      BigcalHit              * aBigcalHit;
      LuciteHodoscopeEvent   * lhEvent;
      LuciteHodoscopeHit     * aLucHit;
      GasCherenkovEvent      * gcEvent;
      GasCherenkovHit        * aCerHit;
      ForwardTrackerEvent    * ftEvent;
      ForwardTrackerHit      * aTrackerHit;
      HallCBeamEvent         * beamEvent;
      HMSEvent               * hmsEvent;
      InSANEMonteCarloEvent  * mcEvent;
      InSANETriggerEvent     * trigEvent;

   public:
      SANEScalers              * fScalers;      ///
      SANEEvents               * fEvents;       ///

   public :
      InSANEAnalysis(const char * sourceTreeName = "");
      virtual ~InSANEAnalysis();

      virtual Int_t         AnalyzeRun() { return(0); } 
      virtual Int_t         Visualize() { return(0); }   
      virtual void          SetEvents(SANEEvents * events);
      virtual SANEEvents *  GetEvents() const ;
      virtual void          SetScalers(SANEScalers * scalers);
      virtual SANEScalers * GetScalers() const ;
      virtual void          ClearEvents();


      ClassDef(InSANEAnalysis, 3)
};


#endif

