#ifndef SANETask_H
#define SANETask_H
#include <iostream>
#include "TTask.h"
/** \file SANETask.h This file contains all SANE task objects
 */

/** Produces Run Overview
 */
class SANERunOverview : public TTask {
public:
   SANERunOverview() {
      ;
   }
   SANERunOverview(const char *name, const char *title): TTask(name, title) {
      ;
   }
   virtual ~SANERunOverview() {
      ;
   }

   void Exec(Option_t *option) {
      std::cout << " SANE OVERVIEW \n";
   }
   ClassDef(SANERunOverview, 1)
};

/** Produces Run Overview
 */
class SANEParallelRunOverview : public TTask {
public:
   SANEParallelRunOverview() {
      ;
   }
   SANEParallelRunOverview(const char *name, const char *title): TTask(name, title) {
      ;
   }
   virtual ~SANEParallelRunOverview() {
      ;
   }

   void Exec(Option_t *option) {
      std::cout << " SANEParallelRunOverview OVERVIEW \n";
   }
   ClassDef(SANEParallelRunOverview, 1)
};

/** Produces Run Overview
 */
class SANEPerpendicularRunOverview : public TTask {
public:
   SANEPerpendicularRunOverview() {
      ;
   }
   SANEPerpendicularRunOverview(const char *name, const char *title): TTask(name, title) {
      ;
   }
   virtual ~SANEPerpendicularRunOverview() {
      ;;
   }

   void Exec(Option_t *option) {
      std::cout << " SANEPerpendicularRunOverview OVERVIEW \n";
   }
   ClassDef(SANEPerpendicularRunOverview, 1)
};

/** Produces Run Overview
 */
class SANECarbonRunOverview : public TTask {
public:
   SANECarbonRunOverview() {
      ;
   }
   SANECarbonRunOverview(const char *name, const char *title): TTask(name, title) {
      ;
   }
   virtual ~SANECarbonRunOverview() {
      ;
   }

   void Exec(Option_t *option) {
      std::cout << " SANECarbonRunOverview OVERVIEW \n";
   }
   ClassDef(SANECarbonRunOverview, 1)
};
#endif
