#ifndef InSANEAnalyzer_HH
#define InSANEAnalyzer_HH 1

#include <exception>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

//#include "TROOT.h"
#include "TObject.h"
#include "TNamed.h"
#include "TList.h"
#include "TString.h"
#include "TBranchElement.h"
#include "TStopwatch.h"
#include "InSANETriggerEvent.h"
#include "SANEScalerCalculations.h"
#include "InSANECalculation.h"
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "InSANECalculation.h"
#include "InSANECorrection.h"
#include "InSANEDatabaseManager.h"
#include "SANERunManager.h"
#include "InSANEDetector.h"
#include "InSANEEventDisplay.h"
#include "InSANERunFlag.h"
#include "InSANESlowSkimmer.h"


/**  InSANEAnalyzer class is the main "engine" class for analysis.
 *   
 *   \ingroup Analyzers
 */
class InSANEAnalyzer:  public virtual TNamed {

   private:
      Int_t iScalerEvent ;
      Int_t nDetectorEvents ;
      Int_t iDetectorEvent ;
      Int_t iEVENT;
      bool  exitEventLoop;
      Int_t nentries;

      std::ofstream  *fMemFile;


   protected:
      bool      fPrintMem;
      bool      fSaveMem;
      TString   fMemFileName;
      double    virtMem;
      double    resMem;

      TStopwatch fStopwatch;
      TStopwatch fCalculationStopwatch;

      std::vector<Double_t> fCalculationTimes;
      std::vector<Double_t> fCorrectionTimes;
      std::vector<Double_t> fDetectorTimes;
      std::vector<Double_t> fClearTimes;
      Double_t fFillTime;
      Double_t fClearTime;
      Double_t fTotalTime;
      Double_t fOtherTime;
      Double_t fSumOfTimes;

   protected:
      Int_t                  fNPrintUpdate;
      bool                   fMeasureTime;
      std::vector<TString *> fAnalysisScripts;

      // Not sure whats best here
      enum Analyzer_t {kEvents, kRuns, kScalers} fType;

      InSANETriggerEvent   fLastScalerTriggerEvent;
      InSANETriggerEvent   fNextScalerTriggerEvent;
      InSANETriggerEvent * fScalerTriggerEvent;
      InSANETriggerEvent * fDetectorTriggerEvent;

      InSANESlowSkimmer  * fSkimmer;

      Int_t      fNumberOfCorrections;
      TList      fCorrections;
      Int_t      fNumberOfCalculations;
      TList      fCalculations;
      Int_t      fNumberOfScalerCalculations;
      TList      fScalerCalculations;
      Int_t      fNumberOfFlags;
      TList      fRunFlags;
      Int_t      fNumberOfDetectors;
      TList      fDetectors;

      TTree    * fCalculatedTree;
      TString    fCalculatedTreeName;
      TTree    * fInputTree;
      TString    fInputTreeName;
      TTree    * fScalerTree;
      TString    fScalerTreeName;
      TTree    * fOutputScalerTree;
      TString    fOutputScalerTreeName;

      Bool_t     fShowEventDisplay;
      TList      fEventDisplays;

   public:
      /** Argument should be the name of the new tree to create */
      InSANEAnalyzer(const char * newTreeName = "correctedBetaDetectors",
                     const char * uncorrectedTreeName = "betaDetectors") ;

      virtual ~InSANEAnalyzer();

      /** Fills the trees if they exist */
      virtual void FillTrees() { if (fCalculatedTree)fCalculatedTree->Fill(); }

      /** Make plots */
      virtual void MakePlots() = 0;

      /** Initialize the corrections and calculations.
       *  This method, properly implemented sets up all the concrete 
       *  corrections and calculations to be done for each event.
       */
      virtual void Initialize(TTree * t = nullptr) = 0;

      /** After adding all corrections/calculations, Initializing, etc., this method sets the
       *  analyzer in motion.
       *
       *  The class InSANETriggerEvent is used to discriminate which type of event we are
       *  considering since there are multiple trees which need to be syncronized. Currently
       *  there are two of such trees, detector and scaler events.
       *
       *  Before entering the event loop, the first two scaler event numbers are read and the
       *  detector tree is incremented to its first event number greater than the first
       *  scaler event number.
       *
       *
       *  Inside of the event loop, this method does the following:
       *
       *  While the scaler event number is bigger than the detector event number...
       *   - Calls the virtual method InSANECorrection::Apply() for each correction in the list, fCorrections.
       *   - Calls the virtual method InSANEDetector::ProcessEvent() for each detector in the list, fDetectors.
       *   - Calls the virtual method InSANECalculation::Calculate() for each calculation in the list, fCalculations.
       *   - Calls the virtual method InSANECalculation::FillTree() for each calculation in the list, fCalculations.
       *   - Calls the virtual method InSANEAnalyzer::FillTrees() to fill the tree(s) associated with
       * the concrete implementation of this class.
       *   - Calls the virtual method InSANEDetector::ClearEvent() for each detector in the list, fDetectors.
       *
       *
       * When the detector event number is bigger than the scaler event is incremented. This
       * test is done at the begining of the event loop so that should it need incremented
       * the rest of the detector stuff can be executed as usual.
       * fNextScalerTriggerEvent -> fLastScalerTriggerEvent and fNextScalerTriggerEvent is
       * set after getting the next entry in the scaler tree.
       *
       * After the event loop it calls MakePlots() for both fCorrections and fCalculations.
       * Then it calls MakePlots() for this class.
       */
      void Process();

      void AddCorrection(InSANECorrection * cor);
      void AddCalculation(InSANECalculation * calc);
      void AddDetector(InSANEDetector* det);
      void AddScalerCalculation(InSANECalculation * calc); ///< Also adds calc to regular calculations list.
      void AddRunFlag(InSANERunFlag * flag);
      void AddEventDisplay(InSANEEventDisplay * display);
      void AddScript(const char * file); ///< Add a script with path relative to the main directory. 

      void SetTriggerEvent(InSANETriggerEvent * t) { fScalerTriggerEvent = t; } ///< \deprecated
      void SetDetectorTriggerEvent(InSANETriggerEvent * t) { fDetectorTriggerEvent = t; }
      void SetScalerTriggerEvent(InSANETriggerEvent * t) { fScalerTriggerEvent = t; }
      void SetSkimmer(InSANESlowSkimmer * skimmer) { fSkimmer = skimmer; }
      void SetNPrintUpdate(Int_t n=20000){fNPrintUpdate = n;}

   protected:

      /**  Virtual method should be implemented from analysis.  */
      virtual void ClearEvents(){ }

      /** Initialize corrections. Use this method in concrete implementation of
       *  InSANEAnalyzer::Initialize();
       */
      void InitCorrections();

      /** Initialize calculations.
       * Use this method in concrete implementation of  *  InSANEAnalyzer::Initialize();
       */
      void InitCalculations();

      /**  Analysis scripts to be run at the end of event processing.
       *   Called after detector's PostProcessAction and just before MakePlots
       *
       *   Scripts should have the following structure:
       *    - The first argument should be the run number.
       *    - Return 0 on success (negative for failures)
       *    - The path provided should be relative to the "main" directory
       */
      void ProcessScripts();


      /** Reads first scaler event to get the run number and then
       *  reads through detector events until it reaches the event
       *  just after the first scaler.
       */
      void SyncronizeEvents(Int_t firstscaler = 0);

      void SeekToEvent(Int_t event);

      /**  Called first for every event before analyzing the rest of the event.
       *   - If the next (detector) event number is greater than the "next scaler event",
       *     CalculateInterval() is called for each scaler calculation.
       *   - The output tree is filled.
       *   - The "next" scaler event becomes the "last" scaler event and the next scaler event is incremented.
       *   - Thus the next interval's scaler information becomes available (not the calculations being done at
       *     the current level of analysis (analysis pass). Only the data already written to the scaler tree.
       *   - The skimmer's flags are processed.
       *   - Then the filters are processed.
       */
      Int_t ProcessScalers();

      /** Takes two doubles by reference,
       *  attempts to read the system-dependent data for a process' virtual memory
       *  size and resident set size, and return the results in KB.
       *
       *  On failure, returns 0.0, 0.0
       *
       *  Copied from http://stackoverflow.com/questions/669438/how-to-get-memory-usage-at-run-time-in-c
       */
      void process_mem_usage(double &, double &);

      void PrintMemoryUsage();

      void PrintTimeUsage();

      ClassDef(InSANEAnalyzer, 4)
};


#endif


