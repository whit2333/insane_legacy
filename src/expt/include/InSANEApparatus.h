#ifndef InSANEApparatus_HH
#define InSANEApparatus_HH 1

#include "TNamed.h"
#include "TVector3.h"
#include "InSANECoordinateSystem.h"
#include "TList.h"
#include "TBrowser.h"
#include "HumanCoordinateSystem.h"

/** ABC for experimental apparatus.
 *  For example this might be a target, beamline component, detector, or shield...
 * \ingroup Apparatus
 */
class InSANEApparatus : public TNamed {

   public:
      TVector3                   fPosition; /// The position of the apparatus in the defined coordinate system
      InSANECoordinateSystem *   fCoordSystem; //! Coordinate system to use for the apparatus
      TList                      fCalibrations;
      TList                      fComponents;

   public:
      InSANEApparatus(const char * name = "apparatus", const char * title = "Apparatus");
      virtual ~InSANEApparatus();

      virtual void Print(Option_t * opt = "") const ;

      Bool_t IsFolder() const { return kTRUE; }
      void Browse(TBrowser* b) {
         b->Add(&fCalibrations, "Calibrations");
         b->Add(&fComponents, "Components");
         if (fCoordSystem)b->Add(fCoordSystem, "Coordinate System");
      }

      ClassDef(InSANEApparatus,2)
};

#endif

