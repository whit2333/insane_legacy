#ifndef SANEAnalysisManager_H
#define SANEAnalysisManager_H 1
#include "GasCherenkovLEDAnalyzer.h"


#include "TSQLResult.h"
#include "TSQLServer.h"
#include "TStopwatch.h"
#include <iostream>
#include "SANERunManager.h"
#include "InSANEAnalysisManager.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TVector.h"
#include "InSANECut.h"
#include "TFolder.h"
#include "TTask.h"
#include "TBrowser.h"
#include "InSANECutter.h"

/**  Concrete Analysis Manager
 *
 *  Use QueueUp() methods to query a database and create a vector of applicable run numbers.
 *
 *  The methods here should use implementations of
 *  InSANECorrector and InSANEAnalyzer along with implementations of
 *  InSANECorrections and InSANECalculations
 *
 *  InSANEAnalysis and InSANEDetectorAnalysis classes should not be used directly here
 *  but multiply inherited by the corrector on analyzers
 *
 * \ingroup Management
 * \ingroup Analysis
 */
class SANEAnalysisManager : public InSANEAnalysisManager {
   public:

      /** Get the pointer to this instanton class
       */
      static SANEAnalysisManager * GetAnalysisManager() {
         if (!fgSANEAnalysisManager) fgSANEAnalysisManager = new SANEAnalysisManager();
         return(fgSANEAnalysisManager);
      }

   protected :
      static SANEAnalysisManager * fgSANEAnalysisManager;
      SANEAnalysisManager();

   public:

      virtual ~SANEAnalysisManager() {
      }

      /** @name SANE run queue
       *  Methods providing a run queue specific to SANE
       */
      //@{
   public:

      /** Adds all parallel runs to run queue
       */
      Int_t QueueUpParallel();

      /** Adds all perpendicular runs to run queue
       */
      Int_t QueueUpPerpendicular();


      //@}

      /** Sets the cut values
       *
       * \todo remove data members and move to fAxe
       */
      void SetStandardCuts();

   public:
      InSANECut * cEnergyCut;
      InSANECut * cCherenkovCut;
      InSANECut * cLuciteCut;
      InSANECut * cTriggerCut;

   public:
      TFolder * fInSANERootFolder;
      TFolder * fReplayFolder;
      TFolder * fAnalysesFolder;
      TFolder * fPerpendicularFolder;
      TFolder * fParallelFolder;
      TFolder * fMaterialPropertiesFolder;
      TFolder * fPi0Reconstruction;

      TFolder * fCutsFolder;
      TFolder * fPureCutsFolder;
      TFolder * fStandardCutsFolder;
      TFolder * fRunFolder;

   public:
      void SetupFolders() {
         fInSANERootFolder = gROOT->GetRootFolder()->AddFolder("InSANE",
               "InSANE top level folders");
         // Add the hierarchy to the list of browsables
         gROOT->GetListOfBrowsables()->Add(fInSANERootFolder, "InSANE");

         fAnalysesFolder          = fInSANERootFolder->AddFolder("Analyses", "Various analysis passes ");

         fRunFolder               = fInSANERootFolder->AddFolder("Run", "Run an analysis  ");

         fCutsFolder              = fInSANERootFolder->AddFolder("Cuts", "Cut definitions used");
         fPureCutsFolder             = fCutsFolder->AddFolder("PureCuts", "Cuts used to define pure sample of electrons");
         fStandardCutsFolder             = fCutsFolder->AddFolder("StandardCuts", "Cuts used for standard electron selection");

      }

      void StartBrowser(int tbrowser = 1) {
         this->SetupFolders();
         if (tbrowser) new TBrowser();
      }


      void PlotAPara() ;




   public:
      Double_t fEnergyMinCut;
      Double_t fEnergyMaxCut;
      Double_t fBigcalTDCMinCut;
      Double_t fBigcalTDCMaxCut;
      Double_t fBigcalOverlapSize;

      Double_t fCherenkovMinNPECut;
      Double_t fCherenkovMaxNPECut;
      Double_t fCherenkovMinTDCCut;
      Double_t fCherenkovMaxTDCCut;
      Double_t fCherenkovMinADCCut;
      Double_t fCherenkovMaxADCCut;
      Double_t fCherenkovMinADCAlignCut;
      Double_t fCherenkovMaxADCAlignCut;
      Double_t fCherenkovMinTDCAlignCut;
      Double_t fCherenkovMaxTDCAlignCut;

      Double_t fLuciteMinTDCCut;
      Double_t fLuciteMaxTDCCut;
      Double_t fLuciteMinTDCAlignCut;
      Double_t fLuciteMaxTDCAlignCut;
      Double_t fLuciteSumMinTDCCut;
      Double_t fLuciteSumMaxTDCCut;
      Double_t fLuciteMissDistance;

      Double_t fTrackerMinTDCCut;
      Double_t fTrackerMaxTDCCut;
      Double_t fTrackerMissDistance;


   public:

      Int_t ProcessPi0();

      Int_t ExecutePi0();

      /**
       *  Runs everything up to and including zeroth pass corrections
       */
      //  Int_t ExecuteZerothPass(int batch = 1) ;

      /**
       *  Runs everything up to and including zeroth pass corrections
       */
      Int_t ExecuteSimulationAnalysis() ;


      /**
       *  Opens up a TTreeViewer with defined cuts and expressions
       */
      Int_t View(const char * treeName = "Clusters", const char * fileName = "InSANE0.root");

      /**
       *  Calibrates the Gas Cherenkov with an LED run
       */
      Int_t ExecuteLEDCalibration();


      ClassDef(SANEAnalysisManager, 1)
};


// A set of classes deriving from TTask see macro tasks.C. The Exec
// function of each class prints one line when it is called.
// #include "TTask.h"
// class SANEParallelAsymmetriesByRun : public TTask {
// public:
//    MyRun() { ; }
//    MyRun(const char *name,const char *title);
//    virtual ~MyRun() { ; }
//    void Exec(Option_t *option="");
//    ClassDef(MyRun,1)     // Run Reconstruction task
// };
//
// class SANERunAsymmetry : public TTask {
// public:
//    SANERunAsymmetry() { ; }
//    SANERunAsymmetry(const char *name,const char *title);
//    virtual ~SANERunAsymmetry() { ; }
//    void Exec(Option_t *option="");
//    ClassDef(SANERunAsymmetry,1)   // Event Reconstruction task
// };




#endif
