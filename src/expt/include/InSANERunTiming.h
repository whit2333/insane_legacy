#ifndef InSANERunTiming_H
#define InSANERunTiming_H

#include <TROOT.h>
#include "InSANEDatabaseManager.h"


/**  ABC for handeling some (detector) timing information for a run
 *
 * \ingroup Runs
 * \ingroup TDC
 * \ingroup timing
 */
class InSANERunTiming : public TObject {
public:
   InSANERunTiming() {
      ;
   };
   virtual  ~InSANERunTiming() {
      ;
   };

   //InSANEDatabaseManager * fDBManager;



   ClassDef(InSANERunTiming, 1)
};


#endif


