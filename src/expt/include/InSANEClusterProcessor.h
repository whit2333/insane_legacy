#ifndef InSANEClusterProcessor_h
#define InSANEClusterProcessor_h
#include <TROOT.h>
#include <TObject.h>
#include <TTree.h>
#include <TFile.h>
#include "InSANECalorimeterCluster.h"
#include "BIGCALCluster.h"
#include "InSANEDetectorEvent.h"
#include "InSANEGeometryCalculator.h"
#include <vector>
#include <iostream>
#include <TClonesArray.h>
#include "TStopwatch.h"
#include "TH1F.h"

/** ABC for a cluster processor.
 *
 *  Cluster Processor should be given an event address where
 *  the clustering data is stored.
 *
 *  Parameters fMaxNClusters ideally wouuld be set very high an the method Partition() would
 *  return a reasonable number of clusters.
 *
 *  A cluster processor does the following when ProcessEvent() is invoked:
 *   - First it calls FindPeaks() which fills the vectors fiPeaks and fjPeaks.
 *   - Next, Partition(), which proceeds to identify, the number of partitions to
 *     divide bigcal into.
 *   - Then, the method Cluster() which procedes with the concrete class' implementation
 *     of a clustering algorithm. The clusters should be created here, i.e.,
 *     new((*clusters)[kk]) BIGCALCluster();
 *   - In a loop, CalculateQuantities(BIGCALCluster* cluster) is called for each cluster object
 *   - The number of clusters is returned
 *
 * \ingroup ClusteringAlgorithms
 */
class InSANEClusterProcessor : public TObject {

   public:
      TString     fOptions;
      TH2F *      fSourceHistogram;
      TH2F *      fSourceCopy;
      Int_t       fNClusters;
      Int_t       fNFound;
      Float_t *   fXCluster ;
      Float_t *   fYCluster;

      InSANEDetectorEvent * fEvent;

      std::vector<double> * fiPeaks;/// Cluster central block i coordinate, in Bigcal coords.
      std::vector<double> * fjPeaks;/// Cluster central block j coordinate, in Bigcal coords.
      std::vector<double> * fEPeaks;/// Cluster central block Energy value.

   public:

      Int_t     fMaxNClusters;  /// First parameter for cluster finding
      Double_t  fMinimumEnergy; /// Second parameter, minimum energy for a cluster seed (of course this energy is only as good as the state of the calibration)
      Int_t     fNPartitions;   /// number of partitons to cluster
      Int_t     fNPeaks;        /// number of peaks found

   public:
      InSANEClusterProcessor();
      virtual   ~InSANEClusterProcessor();

      /** ProcessEvent fills the TClonesArray, clusters.
       */
      Int_t  ProcessEvent(TClonesArray * clusters);

      /** Pure virtual method which must be implemented
       *  Should return the number of partitions for clustering
       */
      virtual Int_t  Partition() = 0;

      /** Pure virtual method which must be implemented
       *  Should return the number of good clusters in processing
       */
      virtual Int_t Cluster() = 0;

      /**  Pure virtual method which must be implemented to
       *   finds peaks in source histogram and returns the number found.
       */
      virtual Int_t FindPeaks() = 0;

      /** Calculates the various moments, energy, etc for the ith peak in the source histogram. 
       */
      virtual Int_t CalculateQuantities(BIGCALCluster * acluster, Int_t k) = 0;

      /** Pure virtual method which must be implemented.
       * Should create the source histogram 
       */
      virtual  TH2F * GetSourceHistogram() = 0 ;

      /** Returns true if the surrounding blocks do
       *  not have any adc signal.
       */
      virtual Bool_t IsDetached(int x, int y, int m, int n) {
         return false ;
      }



   public:
      /** Set the event address from which the processed histogram is generated. */
      Int_t SetEventAddress(InSANEDetectorEvent * inEvent) {
         printf("InSANEClusterProcessor::SetEventAddress\n");
         fEvent = inEvent;
         return(0);
      }

      /*
         Int_t  SetEventAddress(BigcalEvent * ev)
         {
         if(ev) {
         fBigcalEvent = ev;
         fEvent = ev;
         cout << "BIGCALClusterProcessor::SetEventAddress(BigcalEvent*)\n";
         return(0);
         } else {
         cout << " Warning Invalid Pointer supplied in \n   BIGCALClusterProcessor::SetEventAddress(BigcalEvent*)\n";
         return(1);
         }
         }*/
      //_______________________________________________________//

      /** Sets the source histogram pointer.
       */
      virtual  Int_t  SetSourceHistogram(TH2F * hist) {
         fSourceHistogram = hist;
         return(0);
      } ;

      /** Zeros the histogram used for event clustering
       */
      virtual  Int_t * Clear() {
         if (fSourceHistogram) fSourceHistogram->Reset();
         return(nullptr);
      }

      void SetEventReview(Bool_t  res) {
         fExamineEventByEvent = res;
      }

      Bool_t IsEventReview() {
         return (fExamineEventByEvent);
      }

      /** Prints the time spent on each part
       */
      void PrintTime();

   public:
      TH1F * hTotalClusteringTime;
      TH1F * hPeakingFindingTime;
      TH1F * hClusteringTime;
      TH1F * hSourceHistTime;

   protected:
      TStopwatch * fTotalWatch;
      TStopwatch * fPeakWatch;
      TStopwatch * fQuantitiesWatch;
      TStopwatch * fSourceHistWatch;
      Double_t     fTotalTime;
      Double_t     fPeakTime;
      Double_t     fQuantitiesTime;
      Double_t     fSourceHistTime;

      TClonesArray * fClusters;

   private :
      Bool_t                     fExamineEventByEvent;
      InSANEGeometryCalculator * fGeoCalc;

      ClassDef(InSANEClusterProcessor , 1)
};

#endif


