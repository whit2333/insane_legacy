#ifndef InSANEDetectorComponent_H
#define InSANEDetectorComponent_H 1
#include "TNamed.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "InSANEDetectorHit.h"
#include "TBrowser.h"

/** Base class for a component of a detector.
 *
 *  For example, a Mirror, Lead Glass Block, a PMT , etc...
 *  A component is useful because it can store useful data
 *
 *  The components are numbered and sortable which is useful in a TSortedList or
 *  other sorting container (TClonesArray can sort...)
 *
 *   \ingroup detComponents
 */
class InSANEDetectorComponent : public TNamed {

   private:
      Int_t     fComponentNumber;
      Bool_t    fHasADCValue;
      Bool_t    fHasTDCValue;
      Bool_t    fHasScalerValue;

   protected:
      TList     fHits;
      Bool_t    fIsHit;

   public:
      Int_t     fTDCRangeMin;
      Int_t     fTDCRangeMax;
      Int_t     fNominalTDCCutWidth;
      Int_t     fTypicalTDCPeak ;
      Int_t     fTypicalTDCPeakWidth ;

      Int_t     fADCRangeMin;
      Int_t     fADCRangeMax;
      Int_t     fTypicalPedestal;
      Int_t     fTypicalPedestalWidth;

      InSANEDetectorTiming    fTiming; 
      InSANEDetectorPedestal  fPedestal; 


   public :
      InSANEDetectorComponent(const char * n = "detComp" , const char * t = "Det Comp", Int_t num = 0);
      virtual ~InSANEDetectorComponent();

      Bool_t IsFolder() const { return kTRUE; }
      void Browse(TBrowser* b) {
         b->Add(&fTiming,   "Timing");
         b->Add(&fPedestal, "Pedestal");
      }
      ULong_t Hash() const {
         return (ULong_t)fComponentNumber ;
      }
      Bool_t  IsEqual(const TObject *obj) const {
         return fComponentNumber == ((InSANEDetectorComponent*)obj)->GetComponentNumber();
      }
      Bool_t  IsSortable() const { return kTRUE; }
      Int_t   Compare(const TObject *obj) const {
         if (fComponentNumber > ((InSANEDetectorComponent*)obj)->GetComponentNumber())
            return 1;
         else if (fComponentNumber < ((InSANEDetectorComponent*)obj)->GetComponentNumber())
            return -1;
         else return 0;
      }

      Bool_t HasADCValue()    const { return fHasADCValue ; }
      Bool_t HasTDCValue()    const { return fHasTDCValue ; }
      Bool_t HasScalerValue() const { return fHasScalerValue ; }

      Int_t  GetComponentNumber() const { return fComponentNumber; }
      Int_t  GetChannelNumber()   const { return fComponentNumber; }
      Int_t  GetNumber()          const { return fComponentNumber ; }

      void   SetComponentNumber(Int_t num) { fComponentNumber = num; }
      void   SetNumber(Int_t num)          { fComponentNumber = num ; }

      Bool_t       IsHit() const { return(fIsHit); }
      virtual void AddHit(InSANEDetectorHit * hit) {
         fHits.Add(hit);
         fIsHit = kTRUE;
      }
      virtual void ClearEvent(Option_t * opt = "");

      InSANEDetectorTiming * GetTiming() { return(&fTiming); }
      void                   RemoveTiming() {
         fTiming.Clear();
         fHasTDCValue = false;
      }

      InSANEDetectorPedestal * GetPedestal() { return(&fPedestal); }
      void                     RemovePedestal() {
         fPedestal.Clear();
         fHasADCValue = false;
      }

      InSANEDetectorTiming * SetTiming(Double_t mean, Double_t width) {
         fHasTDCValue = true;
         fTiming.SetMean(mean);
         fTiming.SetWidth(width);
         return &fTiming;
      }
      InSANEDetectorPedestal * SetPedestal(Double_t mean, Double_t width) {
         fPedestal.SetMean(mean);
         fPedestal.SetWidth(width);
         fHasADCValue = true;
         return &fPedestal;
      }

      ClassDef(InSANEDetectorComponent, 1)
};



#endif
