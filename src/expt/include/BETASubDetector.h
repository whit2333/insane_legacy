#ifndef BETASubDetector_HH
#define BETASubDetector_HH

#include "InSANEDetector.h"

/** Bare concrete detector.
 *
 *  \ingroup BETA
 */
class BETASubDetector : public InSANEDetector {
public:
   BETASubDetector() {}
   ~BETASubDetector() {}
   virtual Int_t Build() {
      SetBuilt();
      return(0);
   }
   virtual void Initialize() {
      if (!IsBuilt()) Build();
   }
   Int_t ProcessEvent() {
      return(0);
   }

   ClassDef(BETASubDetector, 1)
};

#endif
