#ifndef BETAPedestalCalculation_HH
#define BETAPedestalCalculation_HH 1

#include "BETACalculation.h"

/** Calculates the pedestals for the BETA Detectors.
 *
 *  The histograms are created in Initialize.
 *  In the event loop, Calcualte fills the histograms.
 *  The canvas and plots are created in CreatePlots which is called at the
 *  beginning of Finalize(), which then fits the peds.
 *  At the end of Finialize FillDatabase() is called to add the peds to the sql database.
 *
 *  \ingroup Calculations
 */
class BETAPedestalCalculation : public BETACalculation {
public:
   BETAPedestalCalculation();
   virtual ~BETAPedestalCalculation();

   virtual Int_t    Initialize();
   virtual Int_t    Calculate();
   virtual Int_t    Finalize();

   Int_t            CreatePlots();
   Int_t            FillDatabase();

   void Describe() { }

   TList f2DHistograms;
   TList f1DHistograms;


   /// ___________________

   TFile  * fAnalysisFile;
   TChain * fAnalysisTree;
   TFile  * fOutputFile;
   TTree  * fOutputTree;
   BIGCALGeometryCalculator * bcgeo;

   /// Arrays of histograms
   TClonesArray * fCherenkovPedHists;
   TClonesArray * fLucitePedHists;
   TClonesArray * fTrackerPedHists;
   TClonesArray * fBigcalPedHists;

   /// Arrays of pedestals
   TClonesArray * fCherenkovPeds;
   TClonesArray * fLucitePeds;
   TClonesArray * fTrackerPeds;
   TClonesArray * fBigcalPeds;

   InSANEDetectorPedestal * fPedestals;

   ClassDef(BETAPedestalCalculation, 1)
};
#endif
