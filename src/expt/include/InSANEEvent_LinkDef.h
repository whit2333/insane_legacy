#ifdef __CINT__
/**
 *  @ingroup Events
*/

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;

#pragma link C++ class InSANEEvent+;
#pragma link C++ class InSANEDISEvent+;
#pragma link C++ class InSANEDetectorEvent+;
#pragma link C++ class InSANEDetectorPackageEvent+;
#pragma link C++ class InSANEDetectorHit+;
#pragma link C++ class InSANEScalerEvent+;
#pragma link C++ class InSANEMonteCarloEvent+;
#pragma link C++ class InSANEParticle+;

#pragma link C++ class InSANEEventGenerator+;
#pragma link C++ class InSANETargetEventGenerator+;
#pragma link C++ class BETAG4SavedEventGenerator+;

#pragma link C++ class SANEScalerEvent+;

#pragma link C++ class InSANEEventDisplay+;
#pragma link C++ class BETAEventDisplay+;

#pragma link C++ class InSANEEventDatum+;
#pragma link C++ class std::vector<InSANEEventDatum*>+;
// #pragma link C++ global fgInSANEEventDisplay+;

#pragma link C++ class Pi0EventDisplay+;
#pragma link C++ class HMSElasticEventDisplay+;

#pragma link C++ class BigcalHit+;
#pragma link C++ class GasCherenkovHit+;

#pragma link C++ class ForwardTrackerHit+;
#pragma link C++ class ForwardTrackerPositionHit+;

#pragma link C++ class LuciteHodoscopeHit+;
#pragma link C++ class LuciteHodoscopePositionHit+;

#pragma link C++ class HallCBeamEvent+;
#pragma link C++ class HMSEvent+;

#pragma link C++ global fgBigcalHits+;
#pragma link C++ global fgBigcalTimingGroupHits+;
#pragma link C++ global fgBigcalTriggerGroupHits+;

#pragma link C++ global fgGasCherenkovHits+;
#pragma link C++ global fgGasCherenkovTDCHits+;
#pragma link C++ global fgGasCherenkovADCHits+;

#pragma link C++ global fgForwardTrackerHits+;
#pragma link C++ global fgForwardTrackerPositionHits+;

#pragma link C++ global fgLuciteHodoscopeHits+;
#pragma link C++ global fgLuciteHodoscopePositionHits+;

#pragma link C++ class BigcalEvent+;
#pragma link C++ class InSANEFakePlaneHit+;
#pragma link C++ class GasCherenkovEvent+;
#pragma link C++ class ForwardTrackerEvent+;
#pragma link C++ class LuciteHodoscopeEvent+;
#pragma link C++ class BETAEvent+;
#pragma link C++ class BETAG4MonteCarloEvent+;


#pragma link C++ class Pi0Event+;
#pragma link C++ class Pi03ClusterEvent+;

#pragma link C++ class InSANETrajectory+;
#pragma link C++ class InSANEDISTrajectory+;

#pragma link C++ class InSANETrack+;
#pragma link C++ class InSANEDISTrack+;

#pragma link C++ class HMSElasticEvent+;

#pragma link C++ class CleanedBETAEvent+;

#pragma link C++ class InSANEPhysicsEvent+;
#pragma link C++ class InSANETriggerEvent+;
#pragma link C++ class InSANETriggerCounter+;

#pragma link C++ class InSANEAnalysisEvents+;
#pragma link C++ class SANEEvents+;
#pragma link C++ class SANEScalers+;

#pragma link C++ class InSANEHitPosition+;
#pragma link C++ class std::vector<InSANEHitPosition>+;
//#pragma link C++ class std::vector<InSANEHitPosition>::iterator+;

#pragma link C++ class InSANEClusterEvent+;
#pragma link C++ class InSANEReconstructedEvent+;

#pragma link C++ class ANNEvent+;
#pragma link C++ class ANNElectronThruFieldEvent+;
#pragma link C++ class ANNElectronThruFieldEvent2+;
#pragma link C++ class ANNElectronThruFieldEvent3+;
#pragma link C++ class ANNDisElectronEvent+;
#pragma link C++ class ANNDisElectronEvent2+;
#pragma link C++ class ANNDisElectronEvent3+;
#pragma link C++ class ANNDisElectronEvent4+;
#pragma link C++ class ANNDisElectronEvent5+;
#pragma link C++ class ANNDisElectronEvent6+;
#pragma link C++ class ANNPi0TwoGammaEvent+;
#pragma link C++ class ANNGammaEvent+;



#endif

