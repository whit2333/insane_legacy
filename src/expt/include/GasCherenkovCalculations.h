#ifndef GasCherenkovCalculations_H
#define GasCherenkovCalculations_H 1

#include "BETACalculation.h"

/**  First pass Calculations for SANE Gas Cherenkov.
 *
 *  <ul>
 *
 *  <li>BETA2 Event type only</li>
 *  <li>BETA2 trigger bit only</li>
 *  <li>Compare trigger bit vs. event type. Was there a race condition, (eg between BETA1 or BETA2)?</li>
 *  </ul>
 *
 * \ingroup Calculations
 *  \ingroup Cherenkov
 */
class GasCherenkovCalculation1 : public BETACalculation {
public:
   GasCherenkovCalculation1(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis)  {
      SetNameTitle("GasCherenkovCalculation1", "GasCherenkovCalculation1");
   }

   ~GasCherenkovCalculation1() {
   };

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  GasCherenkovCalculation1 - \n ";
      std::cout <<  "      \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }


   virtual void MakePlots() {

   }

   /**  In new tree, betaDetectors1, this correction does the following...
    *
    *   <ul>
    *   <li>Calculates fNPE</li>
    *   <li>Calculates fADCAlign</li>
    *   <li>Fills Hit array fTDCHits with tdc hits excluding the analog sums. </li>
    *   </ul>
    */
   virtual Int_t Calculate() ;

   ClassDef(GasCherenkovCalculation1, 1)
};
//_______________________________________________________//




/**
 *  Second pass Calculations for Gas Cherenkov
 */
class GasCherenkovCalculation2 : public BETACalculation {
public:
   GasCherenkovCalculation2(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis)  {
      SetNameTitle("GasCherenkovCalculation2", "GasCherenkovCalculation2");
   };

   ~GasCherenkovCalculation2() {
   };

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  GasCherenkovCalculation2 - \n ";
      std::cout <<  "      \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   virtual Int_t Calculate() ;

   ClassDef(GasCherenkovCalculation2, 1)
};


/**
 *  Third pass Calculations for Gas Cherenkov
 */
class GasCherenkovCalculation3 : public BETACalculation {
public:
   GasCherenkovCalculation3(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis)  {
      SetNameTitle("GasCherenkovCalculation3", "GasCherenkovCalculation3");
   };

   ~GasCherenkovCalculation3() {
   };

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  GasCherenkovCalculation3 - \n ";
      std::cout <<  "      \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   virtual Int_t Calculate() = 0;


   ClassDef(GasCherenkovCalculation3, 1)
};


/**
 *  Fourth pass Calculations for Gas Cherenkov
 */
class GasCherenkovCalculation4 : public BETACalculation {
public:
   GasCherenkovCalculation4(InSANEAnalysis * analysis = nullptr) : BETACalculation(analysis)  {
   };

   ~GasCherenkovCalculation4() {
   };

   void Describe() {
      std::cout <<  "===============================================================================\n";
      std::cout <<  "  GasCherenkovCalculation4 - \n ";
      std::cout <<  "      \n";
      std::cout <<  "_______________________________________________________________________________\n";
   }

   virtual Int_t Calculate() = 0;


   ClassDef(GasCherenkovCalculation4, 1)
};

#endif

