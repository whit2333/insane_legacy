#ifndef LuciteHodoscopePositionHit_h
#define LuciteHodoscopePositionHit_h 1

#include <iostream>
#include "InSANEDetectorHit.h"
#include "TVector3.h"
#include "TFile.h"

/** Lucite Hodoscope Position Hit
 *
 *  Using the TDC differences we get a horizontal position and using bar number
 *  a vertical position is determined.
 *  Every combination of hits is used when there were multiple hits per tube
 *
 * \ingroup hodoscope
 * \ingroup Hits
 */
class LuciteHodoscopePositionHit : public InSANEDetectorHit {

   public:
      Int_t       fNNeighborHits;
      Int_t       fBarNumber;
      Double_t    fTDCSum;
      Double_t    fTDCDifference;
      TVector3    fPositionVector;

   public :
      LuciteHodoscopePositionHit();
      virtual ~LuciteHodoscopePositionHit();
      LuciteHodoscopePositionHit(const LuciteHodoscopePositionHit& rhs);
      LuciteHodoscopePositionHit& operator=(const LuciteHodoscopePositionHit &rhs);


      Int_t   Compare(const TObject *obj) const ;
      virtual void      Print(Option_t * opt ="") const ;
      virtual void      Clear(Option_t *opt = "");
      virtual Bool_t    IsSortable() const { return true; }

      bool operator==(const LuciteHodoscopePositionHit &rhs) const ;
      bool operator!=(const LuciteHodoscopePositionHit &rhs) const ;
      bool operator< (const LuciteHodoscopePositionHit &rhs) const ;
      bool operator> (const LuciteHodoscopePositionHit &rhs) const ;
      bool operator<=(const LuciteHodoscopePositionHit &rhs) const ;
      bool operator>=(const LuciteHodoscopePositionHit &rhs) const ;


      ClassDef(LuciteHodoscopePositionHit,6)
};
#endif

