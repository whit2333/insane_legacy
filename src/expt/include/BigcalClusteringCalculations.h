#ifndef BigcalClusteringCalculations_H
#define BigcalClusteringCalculations_H 1

#include "BIGCALClusterProcessor.h"
#include "BETACalculation.h"
#include "BigcalEvent.h"
#include "LocalMaxClusterProcessor.h"
#include "Pi0Event.h"
#include "TVector3.h"
#include "TMatrixD.h"
#include "Pi0ClusterPair.h"
#include "BigcalCoordinateSystem.h"
#include "TRotation.h"
#include "Pi03ClusterEvent.h"


/** Primary Clustering Calculations for Bigcal.
 *
 * \ingroup Calculations
 * \ingroup BIGCAL
 */
class BigcalClusteringCalculation1 :  public BETACalculationWithClusters {
   public:
      BigcalClusteringCalculation1(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis) {
         SetNameTitle("BigcalClusteringCalculation1", "BigcalClusteringCalculation1");
      }

      ~BigcalClusteringCalculation1() {
      }

      /** Method prints a description of the calculations implemented
       */
      void Describe() {
         std::cout <<  "===============================================================================\n";
         std::cout <<  "  BigcalClusteringCalculation1 - \n";
         std::cout <<  "       First executes the clustering of bigcal with the set cluster processor.\n";
         std::cout <<  "       Loops over clusters and fills all data members which,\n";
         std::cout <<  "       requires a loop over the cherenkov and lucite detectors.\n";
         std::cout <<  "_______________________________________________________________________________\n";
      }

      /** First Pass Clustering of each event.
       *
       *  The clustering is executed for <b>all BETA Events</b>
       *  For each event the following is done:
       *   - Uses InSANETriggerEvent::IsClusterable() to determine if the event is relavent.
       *   - Clustering executed using the clustering algorithm/processor set in analysis configure script
       *   - Loop over clusters
       *     - See if it is a noisy channel
       *     - For each cluster, build a list of mirrors to sum
       *     - Sets fClusterTime and fClusterTime2
       *     - Get the array of BC TDC rows  involved for the cluster's centroid
       *       Cluster has 1 or 2 rows, depending on the tdc grouping overlap.
       *     - Assigns subdetector number  using???
       *     - Determines the primary chernekov mirror, adc sum, npe, tdc, etc...
       *   - Done
       */
      virtual Int_t Calculate() ;

      ClassDef(BigcalClusteringCalculation1, 1)
};


//_______________________________________________________________________________



///** Pi0 Calculation which on includes the decay, \f$ pi_0 \rightarrow \gamma \gamma \f$.
// *
// *  Loops over bigcal clusters twice to make all combinations and creates a pi0clusterpair
// *
// *  Outputs a tree for analysis of pi0 reconstruction for the \f$ pi_0 \rightarrow \gamma \gamma \f$ decay alone.
// *  See Bigcal3ClusterP0Calculations.
// *
// *
// * \ingroup Calculations
// */
//class BigcalPi0Calculations : public BETACalculationWithClusters {
//public:
//
//   BigcalPi0Calculations(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis) {
//      SetNameTitle("BigcalPi0Calculations", "BigcalPi0Calculations");
//
//      fOutTree = 0;
//      bcCoords = new BigcalCoordinateSystem();
//      /*TMatrix temp();*/
//      fRotFromBCcoords = bcCoords->GetRotationMatrixTo();
//
//      fOutTree = new TTree("pi0results", "pi0 Reconstruction");
//
//      fPi0Event = new Pi0Event();
//      if (fOutTree) fOutTree->Branch("pi0reconstruction", "Pi0Event", &fPi0Event);
//      //if(fOutTree) fOutTree->AddFriend("Clusters");
//   }
//
//   ~BigcalPi0Calculations() {
//      if (fOutTree)fOutTree->BuildIndex("pi0reconstruction.fRunNumber", "pi0reconstruction.fEventNumber");
//      if (fOutTree)fOutTree->FlushBaskets();
//      if (fOutTree)fOutTree->Write();
//   }
//
//   /** Method prints a description of the calculations implemented */
//   void Describe() {
//      std::cout <<  "===============================================================================\n";
//      std::cout <<  " BigcalPi0Calaculations - \n ";
//      std::cout <<  "       Loops over clusters and determines whether it is a good candidate for\n";
//      std::cout <<  "       a photon coming from a pi0 decay. \n";
//      std::cout <<  "       Then it reconstructs a pi0 from the selected clusters.\n";
//      std::cout <<  "       Calculates neutral pion mass from first two clusters \n";///\todo calculate Mpi0 for all clusters in event
//      std::cout <<  "_______________________________________________________________________________\n";
//   }
//
//   /** Calculates and reconstructs the pi0 mass
//    */
//   Int_t Calculate() ;
//
//   TRotation fRotFromBCcoords;
//   Pi0Event * fPi0Event;
//   BigcalCoordinateSystem * bcCoords;
//
//   ClassDef(BigcalPi0Calculations, 2)
//};
//
//
////__________________________________________________________________________
//
//
//
///** Pi0 Calculation which on includes the decay, \f$ pi_0 \rightarrow \gamma \gamma^* \rightarrow e^+ e^- \gamma \f$.
// *
// *  Outputs a tree for analysis of pi0 reconstruction for the \f$ pi_0 \rightarrow \gamma \gamma \f$ decay alone.
// *  See Bigcal3ClusterP0Calculations.
// *
// *
// * \ingroup Calculations
// */
//class Bigcal3ClusterPi0Calculation : public BETACalculationWithClusters {
//public:
//   Bigcal3ClusterPi0Calculation(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis) {
//      SetNameTitle("Bigcal3ClusterPi0Calculation", "Bigcal3ClusterPi0Calculation");
//      fOutTree = 0;
//      bcCoords = new BigcalCoordinateSystem();
//      /* TMatrix temp();*/
//      fRotFromBCcoords = bcCoords->GetRotationMatrixTo();
//      fOutTree = new TTree("3clusterPi0results", "pi0 Reconstruction");
//      fPi0Event = new Pi03ClusterEvent();
//      if (fOutTree) fOutTree->Branch("pi03ClusterEvents", "Pi03ClusterEvent", &fPi0Event);
//
//   }
//
//   ~Bigcal3ClusterPi0Calculation() {
//      if (fOutTree)fOutTree->BuildIndex("pi0reconstruction.fRunNumber", "pi0reconstruction.fEventNumber");
//      if (fOutTree)fOutTree->FlushBaskets();
//      if (fOutTree)fOutTree->Write();
//   }
//
//   /** Method prints a description of the calculations implemented */
//   void Describe() {
//      std::cout <<  "===============================================================================\n";
//      std::cout <<  " BigcalPi0Calaculations - \n ";
//      std::cout <<  "       Loops over clusters and determines whether it is a good candidate for\n";
//      std::cout <<  "       a photon coming from a pi0 decay. \n";
//      std::cout <<  "       Then it reconstructs a pi0 from the selected clusters.\n";
//      std::cout <<  "       Calculates neutral pion mass from first two clusters \n";///\todo calculate Mpi0 for all clusters in event
//      std::cout <<  "_______________________________________________________________________________\n";
//   }
//
//   /** Calculates and reconstructs the pi0 mass
//    */
//   Int_t Calculate() ;
//
//   TRotation fRotFromBCcoords;
//   Pi03ClusterEvent * fPi0Event;
//   BigcalCoordinateSystem * bcCoords;
//
//   /*
//      NNParaPositronCorrectionEnergy  fPositronNNEnergy;
//      NNParaPositronCorrectionTheta   fPositronNNTheta;
//      NNParaPositronCorrectionPhi     fPositronNNPhi;
//
//      NNParaElectronCorrectionEnergy  fElectronNNEnergy;
//      NNParaElectronCorrectionTheta   fElectronNNTheta;
//      NNParaElectronCorrectionPhi     fElectronNNPhi ;
//
//      NNParaGammaCorrectionEnergy  fGammaNNEnergy;
//      NNParaGammaCorrectionTheta   fGammaNNTheta;
//      NNParaGammaCorrectionPhi     fGammaNNPhi ;
//
//      NNParaGammaCorrection fGammaNN;
//
//      ANNDisElectronEvent * fANNEvent;
//
//
//      Double_t positronInputs[7];
//      Double_t electronInputs[7];
//      Double_t gammaInputs[7];*/
//
//   ClassDef(Bigcal3ClusterPi0Calculation, 1)
//};
//
//
////______________________________________________________________________________________
//
//
//
///** Determines if event falls in the overlap region of the bigcal trigger
// *
// * \ingroup Calculations
// */
//class BigcalOverlapCalculation : public BETACalculationWithClusters {
//public:
//   BigcalOverlapCalculation(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis) {
//      SetNameTitle("BigcalOverlapCalculation", "BigcalOverlapCalculation");
//      fOutTree = 0;
//      fOutTree = new TTree("bigcalOverlap", "bical Overlap Events");
//      fPi0Event = new Pi0Event();
//      if (fOutTree) fOutTree->Branch("pi0reconstruction", "Pi0Event", &fPi0Event);
//   }
//
//   ~BigcalOverlapCalculation() {
//      if (fOutTree)fOutTree->FlushBaskets();
//      if (fOutTree)fOutTree->Write();
//   }
//
//   /** Method prints a description of the calculations implemented */
//   void Describe() {
//      std::cout <<  "===============================================================================\n";
//      std::cout <<  " BigcalOverlapCalculation - \n ";
//      std::cout <<  "      If a  pi0 event falls on the seam of RCS and Protvino \n";
//      std::cout <<  "      then this event is thrown into\n";
//      std::cout <<  "       \n";
//      std::cout <<  "        \n";///\todo calculate Mpi0 for all clusters in event
//      std::cout <<  "_______________________________________________________________________________\n";
//   }
//
//   /**
//    *  Calculates and reconstructs the pi0 mass
//    */
//   Int_t Calculate() ;
//
//   Pi0Event * fPi0Event;
//
//   ClassDef(BigcalOverlapCalculation, 2)
//};
//_______________________________________________________//
#endif

