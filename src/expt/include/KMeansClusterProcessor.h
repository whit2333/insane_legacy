#ifndef KMeansClusterProcessor_h
#define KMeansClusterProcessor_h

#include <TROOT.h>
#include <TObject.h>
#include <TTree.h>
#include <TFile.h>
#include "InSANECalorimeterCluster.h"
#include "InSANECluster.h"
#include "InSANEEvent.h"
#include "InSANEClusterProcessor.h"
#include "TMath.h"
#include <iostream>
#include <TClonesArray.h>
#include "InSANEClusterPartitioning.h"


/** K-means method cluster processor
 *
 * This concrete class finds a limited number of local maxima and
 * produces clusters around them.
 *
 * \ingroup ClusteringAlgorithms
 */
class KMeansClusterProcessor : public InSANEClusterProcessor {
   public:
      Int_t fkMeans;

      /** Finds the peaks in a histogram and fills fiPeaks,fjPeaks.
       *  When a peak is found it zeros a 5x5 block area around the peak.
       */
      virtual Int_t FindPeaks();

      /** Uses the rule of thumb method to determine the number of partitions
       *  Uses the peaks found to initialize the k-means
       */
      virtual Int_t Partition() ;

      virtual Int_t Cluster();

      /** \todo This needs modification!
       */
      virtual Int_t CalculateQuantities(BIGCALCluster * acluster, Int_t k);

      virtual TH2F * GetSourceHistogram();

      Bool_t fQuit;

      BIGCALGeometryCalculator * fGeoCalc;

      KMeansClusterProcessor() ;

      virtual ~KMeansClusterProcessor();

      TH2F * fSourceCopy;

      Int_t fNIterations;

      InSANEClusterPartitioning * fPartitioning;

      ClassDef(KMeansClusterProcessor , 1)
};

#endif


