#ifndef ForwardTrackerEvent_h
#define ForwardTrackerEvent_h

#include <TClonesArray.h>
#include <TROOT.h>
 //#include <TChain.h>
#include "InSANEDetectorEvent.h"
#include "ForwardTrackerPositionHit.h"

/**Concrete class for an event in the Forward Tracker.
 *
 * Concrete class for the Forward tracker used during SANE
 * It consists of 3 layers of scintillators. 2 for y and 1 for x
 *
 *  \ingroup Events
 *  \ingroup tracker
 */
class ForwardTrackerEvent : public InSANEDetectorEvent {
   public :
      ForwardTrackerEvent();
      ~ForwardTrackerEvent();
      Int_t AllocateHitArray();

      //  Bool_t         fIsValid;           //
      /// Number of TDC channels to be readout for the event
      Int_t   fNumberOfTDCHits;
      /// Sum of the the number of tdc hits on each channel AND pass timing cut
      Int_t   fNumberOfTimedTDCHits;
      Int_t   fNumberOfTimedPositionHits;
      Int_t   fNumberOfTimed2PositionHits;

      TClonesArray * fTrackerHits; //->  array with all hits
      TClonesArray * fTrackerPositionHits; //->
      TClonesArray * fTrackerY1PositionHits; //->
      TClonesArray * fTrackerY2PositionHits; //->
      TClonesArray * fTracker2PositionHits; //->

      void Print(const Option_t * opt = "") const {
         std::cout << "ForwardTrackerEvent" << std::endl;
         std::cout << "  NTDCHits        " << fTrackerHits->GetEntries() << std::endl;
         std::cout << "  NPositionHits   " << fTrackerPositionHits->GetEntries() << std::endl;
         std::cout << "  NY1PositionHits " << fTrackerY1PositionHits->GetEntries() << std::endl;
         std::cout << "  NY2PositionHits " << fTrackerY2PositionHits->GetEntries() << std::endl;
         std::cout << "  N-2PositionHits " << fTracker2PositionHits->GetEntries() << std::endl;
      }

      void ClearEvent(Option_t * opt = "");

   private :

      static TClonesArray *fgForwardTrackerHits; //!
      static TClonesArray *fgForwardTrackerPositionHits; //!
      static TClonesArray *fgForwardTracker2PositionHits; //!
      static TClonesArray *fgForwardTrackerY1PositionHits; //!
      static TClonesArray *fgForwardTrackerY2PositionHits; //!


      ClassDef(ForwardTrackerEvent, 5)
};



#endif

