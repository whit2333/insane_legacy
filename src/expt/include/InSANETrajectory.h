#ifndef InSANETrajectory_HH
#define InSANETrajectory_HH 1

#include "TObject.h"
//#include "TRefArray.h"
#include "TVector3.h"
#include "TMath.h"
#include "TLorentzVector.h"

/**  Trajectory for a particle.
 *
 *  A trajectory is not quite a track as a track would have
 *  a detector devoted to "tracking". Trajectories in this context, thus,
 *  have a sort of vauge meaning.
 *
 *  \ingroup Events
 */
class InSANETrajectory : public TObject {

   public:
      Int_t           fRunNumber;
      Int_t           fEventNumber;
      Int_t           fClusterNumber;
      Bool_t          fIsNoisyChannel;
      Int_t           fNPositions;
      Double_t        fNCherenkovElectrons;
      Double_t        fCherenkovTDC;
      TLorentzVector  fMomentum;
      TVector3        fPosition0;        ///< cluster seed position
      //TRefArray       fHitPositions;   /// \deprecated

   public :
      InSANETrajectory();
      virtual ~InSANETrajectory();
      InSANETrajectory(const InSANETrajectory& rhs);
      InSANETrajectory& operator=(const InSANETrajectory &rhs);

      virtual void Clear(Option_t * opt = "");

      /** @name Kinematic Functions
       *  Not sure if these were a good idea....
       *  @{
       */
      /** \f$ \theta_{e}  \f$ */
      Double_t GetTheta() const { return(fMomentum.Theta()); }
      /** \f$ \phi_{e}  \f$ */
      Double_t GetPhi() const { return(fMomentum.Phi()); }
      /**  */
      Double_t GetMomentum() const { return(fMomentum.Vect().Mag()); }
      /**  */
      Double_t GetMass() const { return(fMomentum.M()); }
      /** Energy is calculated from momentum, \f$ E^{\prime} = \sqrt{p^{2} + m^{2} } \f$ */
      Double_t GetEnergy() const { return(fMomentum.T()); }
      //@}

      ClassDef(InSANETrajectory,13)
};


#endif


