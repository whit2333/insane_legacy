#ifndef InSANECorrection_H
#define InSANECorrection_H

#include "TNamed.h"
#include "TFile.h"
#include "TString.h"

#include "InSANEDetectorAnalysis.h"
#include "InSANEEvent.h"
#include "InSANEAnalysisEvents.h"


/** ABC for an experiment correction.
 *
 * ABC for an experiment wide correction to the data. Since this could include all, many or just a single run
 * we will use the TTreeIndex as a basis for implementing these corrections
 *
 * This class is closly related to the cut and cutter (axe) in the sense that when you swing your axe
 * and an event pops off you then maniulate it into a fine paper product :-p
 *
 * \ingroup Calculations
 */
class InSANECorrection : public TNamed {
   protected:

      InSANEAnalysisEvents    * fTransientDatum;
      TString                   fDescription;
      Bool_t                    fIsInitialized;

   public:
      InSANECorrection(InSANEAnalysis * analysis = nullptr);
      virtual ~InSANECorrection();

      virtual Int_t  Initialize() = 0;
      virtual Int_t  Finalize()   = 0;
      virtual Int_t  Apply()      = 0;
      virtual void   Describe()   = 0;

      /** Optional virtual method called at the end of the event loop but before
       * the InSANEAnalyzer::MakePlots().
       */
      virtual void MakePlots() { }

      void                   SetEvents(InSANEAnalysisEvents * e);
      InSANEAnalysisEvents * GetEvents() const ;
      bool                   IsEventsSet() const ;

      void                   SetInitialized();
      Bool_t                 IsInitialized() const ;

      ClassDef(InSANECorrection,2)
};


#endif



