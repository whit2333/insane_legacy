#ifndef InSANECalculation_H
#define InSANECalculation_H 1

#include "TNamed.h"

#include "InSANEAnalysis.h"
#include "InSANEDetectorAnalysis.h"

#include "TFile.h"
#include "TTreeIndex.h"

#include "SANEEvents.h"
#include "InSANEEvent.h"
#include "InSANEAnalysisEvents.h"
#include "SANEEvents.h"
#include "BETAEvent.h"
#include "SANERunManager.h"

/** ABC for a calculation of a quantity used in corrector or analyzer.
 *
 *  InSANECalculation::Initialize() is called by InSANEAnalyzer::Initialize(). 
 *  It must return 0 for success, otherwise it is not include in the analysis.
 *
 *  InSANECalculation::Description() is called by InSANEAnalyzer before entering 
 *  the event loop to provide an idea of the calculations that pass initialization. 
 *
 *  If properly initialized, InSANECalculation::Calculate() does the work.  
 *  After adding to an analyzer, they are called in the order in which they are added.
 *  So shared data strucutres can be used, but care should be taken to make sure 
 *  there is not a strong dependence (in the case the first calculation is removed or
 *  fails to initialize).
 *
 * \ingroup Calculations
 */
class InSANECalculation : public TNamed {

   protected:
      TTree                   * fOutTree;
      InSANEAnalysisEvents    * fTransientDatum;
      TString                   fDescription;
      Bool_t                    fFillEvent;
      Bool_t                    fIsInitialized;

   public:
      InSANECalculation(InSANEAnalysis * analysis = nullptr);
      virtual ~InSANECalculation();

      virtual Int_t   Calculate()  = 0;
      virtual void    Describe()   = 0;
      virtual Int_t   Initialize() = 0;

      virtual Int_t   CalculateInterval() { return(0); }
      virtual void    SetOutputTree(TTree * t) { if (t)fOutTree = t; }
      virtual void    DefineCalculation() { }

      virtual Int_t Finalize() { return(0); }

      /** Optional virtual method called at the end of the event loop but before
       * the InSANEAnalyzer::MakePlots()
       */
      virtual void MakePlots() { }

      virtual void            SetEvents(InSANEAnalysisEvents * e);
      InSANEAnalysisEvents *  GetEvents() const ;

      Bool_t IsInitialized() const ;
      void   SetInitialized();

      bool IsEventsSet() const ;

      /** Fill the tree if fFillEvent=true.
       *  NOTE: It is set to true by default. Use this in calculate to toggle
       *  tree filling for each event.
       */
      virtual void FillTree() {
         if (fOutTree) if (fFillEvent) fOutTree->Fill();
      }

      Int_t Apply() { return(this->Calculate()); } ///< \deprecated

      ClassDef(InSANECalculation,2)
};


#endif



