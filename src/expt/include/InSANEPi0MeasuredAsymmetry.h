#ifndef InSANEPi0MeasuredAsymmetry_HH
#define InSANEPi0MeasuredAsymmetry_HH

#include "InSANEAsymmetry.h"
#include "InSANEDilutionFactor.h"
#include "InSANEAveragedKinematics.h"
#include "Pi0ClusterPair.h"

class InSANEPi0MeasuredAsymmetry : public InSANEAsymmetry {
   public:

      // Asymmetry histograms
      TH1F * fAsymmetryVsPt;         //->
      TH1F * fAsymmetryVsE;         //->
      TH1F * fAsymmetryVsTheta;     //->
      TH1F * fAsymmetryVsPhi;       //->

      Int_t fHelicity;

      InSANEAveragedPi0Kinematics1D * fAvgKineVsPt;     //->
      InSANEAveragedPi0Kinematics1D * fAvgKineVsE;     //->
      InSANEAveragedPi0Kinematics1D * fAvgKineVsTheta; //->
      InSANEAveragedPi0Kinematics1D * fAvgKineVsPhi;   //->

      // Histograms vs E
      TH1F * fNPlusVsE;             //->
      TH1F * fNMinusVsE;            //->

      // Histograms vs Phi 
      TH1F * fNPlusVsPhi;           //->
      TH1F * fNMinusVsPhi;          //->

      // Histograms vs Theta 
      TH1F * fNPlusVsTheta;         //->
      TH1F * fNMinusVsTheta;        //->

      // Histograms vs Theta 
      TH1F * fNPlusVsPt;         //->
      TH1F * fNMinusVsPt;        //->

      Pi0ClusterPair * fPi0ClusterPair; //!

      InSANEDilutionFromTarget * fDilutionFromTarget; //!

      // Dilution histograms
      TH1F * fDilutionVsPt;         //->
      TH1F * fDilutionVsE;         //->
      TH1F * fDilutionVsTheta;     //->
      TH1F * fDilutionVsPhi;       //->

      TH1F * fSystErrVsPt;        //->
      TH1F * fSystErrVsE;         //->
      TH1F * fSystErrVsTheta;     //->
      TH1F * fSystErrVsPhi;       //->

      Int_t                  fRunNumber;
      Double_t               fDilution;
      Double_t               fPt;
      Double_t               fPb;
      Double_t               fHalfWavePlate;
      Double_t               fAsymmetry;
      Double_t               fError;
      Double_t               fCharge[2]; //->
      Double_t               fLiveTime[2]; //->
      TList                  fHistograms;
      Bool_t                 fChargeSwap;

      Double_t fPtMin;
      Double_t fPtMax;
      Double_t fEnergyMin;
      Double_t fEnergyMax;
      Double_t fBinningBeamEnergy;
      Double_t fThetaMin;
      Double_t fThetaMax;
      Double_t fPhiMin;
      Double_t fPhiMax;

      Double_t dfPbPt;
      Double_t Qplus;
      Double_t Qminus;
      Double_t Lplus;
      Double_t Lminus;

   public:
      InSANEPi0MeasuredAsymmetry(Int_t n = 0);
      virtual ~InSANEPi0MeasuredAsymmetry();

      InSANEPi0MeasuredAsymmetry(const InSANEPi0MeasuredAsymmetry& rhs);
      InSANEPi0MeasuredAsymmetry&      operator=(const InSANEPi0MeasuredAsymmetry &rhs) ;
      InSANEPi0MeasuredAsymmetry &     operator+=(const InSANEPi0MeasuredAsymmetry &rhs) ;
      const InSANEPi0MeasuredAsymmetry operator+(const InSANEPi0MeasuredAsymmetry &other)  ;

      /** Checks the kinematic and other variables to see if the event falls into the bin.
       *  Returns true if the event falls into the ranges of:
       *   - scattered energy
       *   - theta
       *   - phi
       *   - Qsquared
       */
      virtual Bool_t IsGoodEvent();

      virtual void Print(); // *MENU*
      virtual void Initialize();

      /** Count the event and bin the appropriate histograms depending on the helicity
       */
      virtual Int_t CountEvent();
      virtual Int_t CountEvent(std::vector<int>& groups);

      /** Calculate the physics asymmetry.
       *  Note that this uses a dilution factor, and  beam and target polarizations.
       */
      Double_t GetAsymmetry(Double_t N1, Double_t N2, Double_t df)  ;
      Double_t GetSystematicError(Double_t N1, Double_t N2, Double_t df)  ;
      Double_t GetStatisticalError(Double_t N1, Double_t N2, Double_t df)  ;
      Double_t GetTotalError(Double_t N1, Double_t N2, Double_t df)  ;

      void SetDirectory(TDirectory * dir = nullptr);

      Int_t CalculateDilutionFactors1D(TH1F * dfHist, const InSANEAveragedPi0Kinematics1D *kine);

      /** Calculates the physics asymmetries.
       *  \f$ A= 1/(df Pb Pt) (N+/Q+ - N-/Q-)/(N+/Q+ + N-/Q-) \f$
       */
      Int_t CalculateAsymmetries(); // *MENU*

      //TH1D * CreateAveragedHistogram1D(const TH1F * h0, const TH1F * h1, TH1D * hRes, const TH1F * df = 0); 
      //TH2D * CreateAveragedHistogram2D(const TH2F * h0, const TH2F * h1, TH2D * hRes, const TH2F * df = 0); 

      TH1F * CreateAsymmetryHistogram1D(const TH1F * h0, const TH1F * h1, TH1F * hRes, const TH1F * dfHist = nullptr, TH1F * systHist = nullptr);

      virtual void PrintPhiBinnedTable(std::ostream& stream);
      virtual void PrintBinnedTable(std::ostream& stream);
      virtual void PrintBinnedTableHead(std::ostream& file);
      virtual void PrintTableHead(std::ostream& file);
      virtual void PrintTableHeader(std::ostream& file){ PrintTableHead(file);}
      virtual void PrintTable(std::ostream& file);

      Double_t GetAsymmetry() const { return fAsymmetry;}
      Double_t GetDilution()  const { return fDilution;}
      Double_t GetHWP()       const { return fHalfWavePlate;}
      Double_t GetTargetPol() const { return fPt;}
      Double_t GetBeamPol()   const { return fPb;}
      Double_t GetChargeNormalization() const { return fCharge[1]/fCharge[0] ; }
      Double_t GetLiveTime(int i = 0) { if( i< 2 && i>=0) return fLiveTime[i]; else return(0);}
      Double_t GetCharge(int i = 0) { if( i< 2 && i>=0) return fCharge[i]; else return (0);}
      Int_t    GetRunNumber() const { return fRunNumber; }
      InSANEDilutionFromTarget * GetDilutionFromTarget() { return fDilutionFromTarget; }

      void SetRunNumber(Int_t run ) { fRunNumber = run; }
      void SetTargetPol(Double_t p) { fPt = p; }
      void SetBeamPol(Double_t p)   { fPb = p; }
      void SetDilution(Double_t p)  { fDilution = p; }
      void SetHWP(Double_t p)       { fHalfWavePlate = p; }
      void SetCharge(Double_t q1, Double_t q2) { fCharge[0] = q1; fCharge[1] = q2; }
      void SetLiveTime(Double_t l1 , Double_t l2 ) { fLiveTime[0] = l1; fLiveTime[1] = l2; }
      void SetDilutionFromTarget(InSANEDilutionFromTarget * d) { fDilutionFromTarget = d; }
      void SetRunSummary(InSANERunSummary* rs) ;

      /** combine measurements. Differs from adding statistics together. */
      Int_t Combine(InSANEPi0MeasuredAsymmetry * a){
         return(0); 
      }

      /** Necessary for Browsing */
      Bool_t         IsFolder() const {
         return kTRUE;
      }

      void           Browse(TBrowser* b) {
         b->Add(&fHistograms,"Histograms");
         if (fAsymmetryVsPt)        b-> Add(fAsymmetryVsPt);
         if (fAsymmetryVsE)         b-> Add(fAsymmetryVsE);
         if (fAsymmetryVsTheta)     b-> Add(fAsymmetryVsTheta);
         if (fAsymmetryVsPhi)       b-> Add(fAsymmetryVsPhi);

         if (fAvgKineVsPt) b->Add(fAvgKineVsPt);
         if (fAvgKineVsE) b->Add(fAvgKineVsE);
         if (fAvgKineVsTheta) b->Add(fAvgKineVsTheta);
         if (fAvgKineVsPhi) b->Add(fAvgKineVsPhi);

         if (fDilutionVsPt)        b-> Add(fDilutionVsPt);
         if (fDilutionVsE)         b-> Add(fDilutionVsE);
         if (fDilutionVsTheta)     b-> Add(fDilutionVsTheta);
         if (fDilutionVsPhi)       b-> Add(fDilutionVsPhi);

         if (fSystErrVsPt)        b-> Add(fSystErrVsPt);
         if (fSystErrVsE)         b-> Add(fSystErrVsE);
         if (fSystErrVsTheta)     b-> Add(fSystErrVsTheta);
         if (fSystErrVsPhi)       b-> Add(fSystErrVsPhi);

         b->Add(&fRunSummary);
      }

      InSANERunSummary fRunSummary;


      ClassDef(InSANEPi0MeasuredAsymmetry, 31)
};


#endif
