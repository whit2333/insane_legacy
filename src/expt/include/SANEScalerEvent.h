#ifndef SANEScalerEvent_h
#define SANEScalerEvent_h

/*!
\page scalers Scalers

   First, the scalers are exported via some fortran write() statments to a text file, which is easily
   imported to a TTree (via a simple constructor).
   Then the scaler event objects are created. With SANEScalers (which not streamed) you have an easy handle
   on the typically named tree, "Scalers".

    \section  scalerEventLooping Scaler Event Looping and threading

    Since each event has a unique number, we can know when to read a scaler or continue counting events.


    \section scalersAndDetectors Scalers and Detectors
    For each scaler event
 */

#include "TROOT.h"
#include "InSANEScalerEvent.h"
#include "InSANETriggerEvent.h"
#include "InSANETriggerCounter.h"


/** Scaler event for the SANE experiment
 *
 *  \ingroup Scalers
 */
class SANEScalerEvent : public InSANEScalerEvent {

   public:

      /** Constructor defines (hard coded) the coefficients and offsets
       *  needed to calculate the beam currents and helicity dependent
       *  accumulated charges.
       */
      SANEScalerEvent(/* const char * treename = "scalers"*/) ;
      virtual ~SANEScalerEvent();

      void SetAddresses();


      /** @name Scalers for trigger dead times
       *  Scalers and associated trigger counts for calculating dead times
       *  and live times as well as their helicity dependent dead times.
       * @{
       */

      InSANETriggerCounter fBETA1Counter;
      InSANETriggerCounter fBETA2Counter;
      InSANETriggerCounter fPi0Counter;
      InSANETriggerCounter fCoinCounter;
      InSANETriggerCounter fHMS1Counter;
      InSANETriggerCounter fHMS2Counter;
      InSANETriggerCounter fLEDCounter;
      InSANETriggerCounter fTSCounter;
      //@}

      Long64_t  fBCM1Scaler;                   // BCM1 Scaler
      Long64_t  fBCM2Scaler;                   // BCM2 Scaler

      Long64_t  f1MHzClockScaler;              // 0.998MHz reference scaler that went bad!
      Long64_t  f1MHzClockScaler2;             // 1MHz reference scaler
      Long64_t  fUnserScaler;                  // Unser? ?

      Long64_t  fBCM1PositiveHelicityScaler;   // BCM1 hel(+) gated scaler
      Long64_t  fBCM1NegativeHelicityScaler;   // BCM1 hel(1) gated scaler

      Long64_t  fBCM2PositiveHelicityScaler;   // BCM2 hel(+) gated scaler
      Long64_t  fBCM2NegativeHelicityScaler;   // BCM2 hel(-) gated scaler

      Long64_t  f1MHzClockPositiveHelicityScaler;
      Long64_t  f1MHzClockNegativeHelicityScaler;

      Long64_t  fHodoscopeScalers[12];
      Long64_t  fHodoscopeScalersChange[12];
      Long64_t  fCherenkovScalers[12];
      Int_t     fCherenkovCounts[12];
      Long64_t  fCherenkovScalersChange[12];
      Long64_t  fTrackerScalers[32];
      Long64_t  fTrackerScalersChange[32];
      Long64_t  fBigcalScalers[224];
      Long64_t  fBigcalScalersChange[224];

   private:
      /// The scale coeffcient used to convert a rate to nanoamps for
      Double_t fBCMScaleCoefficent[2];
      Double_t fBCMOffset[2];
      Double_t fBCMPositiveHelicityScaleCoefficent[2];
      Double_t fBCMPositiveHelicityOffset[2];
      Double_t fBCMNegativeHelicityScaleCoefficent[2];
      Double_t fBCMNegativeHelicityOffset[2];

   public:
      /** @name Beam Currents
       *  Get beam currents.
       *
       * @{
       */

      /** Calculates the average beam current in nA.
       */
      Double_t BeamCurrentAverage() const ;

      Double_t GetBeamCurrent() const ;

      /** In nanoamps following the formula from the wiki */
      Double_t BeamCurrentMonitor1() const ;

      /** Used in simulationg the BCM scalers */
      Double_t GetScalerBCM1(Double_t current) const ;

      /** Used in simulationg the BCM scalers */
      Double_t GetScalerBCM2(Double_t current) const ;

      /** In nanoamps following the formula from the wiki */
      Double_t BeamCurrentMonitor2() const ;

      /** In nanoamps following the formula from the wiki */
      Double_t BeamCurrentMonitor1PositiveHelicity() const ;

      /** In nanoamps following the formula from the wiki */
      Double_t BeamCurrentMonitor2PositiveHelicity() const ;

      Double_t GetPositiveScalerBCM1(Double_t current) const ;
      Double_t GetPositiveScalerBCM2(Double_t current) const ;

      /** In nanoamps following the formula from the wiki */
      Double_t BeamCurrentMonitor1NegativeHelicity() const ;

      /** In nanoamps following the formula from the wiki */
      Double_t BeamCurrentMonitor2NegativeHelicity() const ;

      Double_t GetNegativeScalerBCM1(Double_t current) const ;
      Double_t GetNegativeScalerBCM2(Double_t current) const ;

      //@}

      /** returns the time lapsed since the last scaler read. (seconds) */
      Double_t TimeLapsed() const ;

      /** HMS scaler to get time. */
      Double_t TimeLapsed2() const ;

      /** returns the charge deposited  since the last scaler read. (microC) */
      Double_t AverageDeltaQ() const ;

      Double_t BCM1ChargeAsymmetry() const ;

      Double_t BCM2ChargeAsymmetry() const ;

      /** Fraction of time interval spent with positive helicity. */
      Double_t PositiveTimeFraction() const ;
      /** Fraction of time interval spent with negative helicity. */
      Double_t NegativeTimeFraction() const ;

      /** Fraction of time interval spent with positive helicity. */
      Double_t PositiveTimeFraction2() const ;
      /** Fraction of time interval spent with negative helicity. */
      Double_t NegativeTimeFraction2() const ;

      /** calculates the average beam current for positive helicity*/
      Double_t BeamCurrentAveragePositiveHelicity() const ;

      /** calculates the average beam current for positive helicity*/
      Double_t BeamCurrentAverageNegativeHelicity() const ;

      /** returns the charge deposited  since the last scaler read for positive helicity. (microC) */
      Double_t AveragePositiveDeltaQ() const ;

      /** returns the charge deposited  since the last scaler read for positive helicity. (microC) */
      Double_t AverageNegativeDeltaQ() const ;


      Double_t BCM1PositiveDeltaQ() const ;
      Double_t BCM1NegativeDeltaQ() const ;
      Double_t BCM2PositiveDeltaQ() const ;
      Double_t BCM2NegativeDeltaQ() const ;

      /** Live Time for Positive Helicity gates
       */
      Double_t BETA2LiveTimePositive() const ;

      /** Live Time for Negative Helicity gates
       */
      Double_t BETA2LiveTimeNegative() const ;

      Double_t BETA2DeadTimePositive() const ;

      Double_t BETA2DeadTimeNegative() const ;

      void ClearEvent(Option_t * opt = "");

      void ClearCounts();


      Double_t GetCherenkovRate(Int_t n) ;
      Double_t GetBigcalRate(Int_t n) ;
      Double_t GetTrackerRate(Int_t n) ;


      ClassDef(SANEScalerEvent, 6)
};

#endif

