#include  "LuciteHodoscopeDetector.h"
#include  "LuciteHodoscopeHit.h"
#include  "LuciteHodoscopeEvent.h"
#include "InSANECherenkovMirror.h"
#include "InSANEToroidalMirror.h"
#include "InSANESphericalMirror.h"
#include <vector>
#include <iostream>
#include "InSANERunManager.h"
#include "TDirectory.h"
#include "BigcalCoordinateSystem.h"
#include "SANEAnalysisManager.h"

ClassImp(LuciteHodoscopeDetector)

//______________________________________________________________________________
LuciteHodoscopeDetector::LuciteHodoscopeDetector(Int_t runnum) {

   if (InSANERunManager::GetRunManager()->GetVerbosity() > 1)
      printf(" o LuciteHodoscopeDetector::Constructor \n");

   SetName("hodoscope");
   SetTitle("Lucite hodoscope");

   fUseStaticCalibrations    = false;
   fUseCurrentRunCalibration = true;

   fNumberOfADCChannels      = 28 * 2;
   fNumberOfTDCChannels      = 28 * 2;
   fNumberOfScalerChannels   = 28 * 2;
   fNumberOfScalers          = 28 * 2;

   fNominalTDCCutWidth       = 50.0;
   fTypicalPedestal          = 450;
   fTypicalPedestalWidth     = 15;
   fTypicalTDCPeak           = -2000;
   fTypicalTDCPeakWidth      = 40;

   fLuciteIndexOfRefraction  = 1.495;
   fRefBar                   = 15;
   fTDCSumOffset             = 3640.0;

   // Set as owner so they are deleted 
   fSumTimings.SetOwner();
   fDiffTimings.SetOwner();

   // -----------------------------
   // Get Cuts
   auto * fAnalysisManager  = (SANEAnalysisManager*)SANEAnalysisManager::GetAnalysisManager();
   // Individual TDC Cut
   cTDCMin = fAnalysisManager->fLuciteMinTDCCut;
   cTDCMax = fAnalysisManager->fLuciteMaxTDCCut;

   cTDCAlignMin = fAnalysisManager->fLuciteMinTDCAlignCut;
   cTDCAlignMax = fAnalysisManager->fLuciteMaxTDCAlignCut;

   // Sum TDC Cut
   cTDCSumMin = fAnalysisManager->fLuciteSumMinTDCCut;
   cTDCSumMax = fAnalysisManager->fLuciteSumMaxTDCCut;

   cTDCDiffMin = -500;
   cTDCDiffMax = 500;

   cBigcalXPositionMissDistance = fAnalysisManager->fLuciteMissDistance;
   cBigcalYPositionMissDistance = fAnalysisManager->fLuciteMissDistance;

   cSeedHitMissDistance = 30.0; // fAnalysisManager->fLuciteMissDistance;

   // -----------------------------
   // Pedestals
   TDirectory * aDir = nullptr;
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals/hodoscope")) {
         if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals"))
            aDir = InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("pedestals");
         if (aDir) aDir->cd();
         if( aDir) aDir = aDir->mkdir("hodoscope");
         if (aDir) aDir->cd();
      }
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals/hodoscope");
   }
   for (int i = 0; i < fNumberOfADCChannels; i++) {
      GetPedestals()->Add(new InSANEDetectorPedestal());
   }

   // -----------------------------
   // Timings
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("timing");
   }
   for (int i = 0; i < 28; i++) {
      GetSumTimings()->Add(new InSANEDetectorTiming());
      GetDiffTimings()->Add(new InSANEDetectorTiming());
   }
   for (int i = 0; i < 56; i++) {
      GetTimings()->Add(new InSANEDetectorTiming());
      fTDCHits[i]      = new std::vector<Int_t>;
      fTimedTDCHits[i] = new std::vector<Int_t>;
   }

   // -----------------------------
   // Hit counters and storage
   for (int i = 0; i < 28; i++) {
      fNegativeTDCHits[i]               = new std::vector<Int_t>;
      fPositiveTDCHits[i]               = new std::vector<Int_t>;

      fTDCDifferenceNumberPassingCut[i] = new std::vector<Int_t>;
      fTDCSumNumberPassingCut[i]        = new std::vector<Int_t>;

      fBigcalXPositions[i]              = new std::vector<Double_t>;
      fBigcalYPositions[i]              = new std::vector<Double_t>;

      fTDCHitDifferences[i]             = new std::vector<Int_t>;
      fTimedTDCHitDifferences[i]        = new std::vector<Int_t>;

      fTDCHitSums[i]                    = new std::vector<Int_t>;
      fTimedTDCHitSums[i]               = new std::vector<Int_t>;

      fTDCHitAndTriggerSums[i]          = new std::vector<Int_t>;
      fTimedTDCHitAndTriggerSums[i]     = new std::vector<Int_t>;
   }

   // -----------------------------------------------
   // Retrieve Position calibrations
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("calibrations/hodoscope")) {
         if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("calibrations")) {
            InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("calibrations");
            gDirectory->mkdir("hodoscope");
         }
         InSANERunManager::GetRunManager()->GetCurrentFile()->cd("calibrations/hodoscope");
      }
   }

   fPositionCalibration = nullptr;// (InSANEDetectorCalibration*)gROOT->FindObject("lucitePositionCalibration");
   if (!fPositionCalibration) {
      auto * f = new TFile("data/HodoscopeCalibrations.root","READ");
      if(f){
         f->GetObject("lucitePositionCalibration72994",fPositionCalibration );
         if(!fPositionCalibration) {
            Warning("LuciteHodoscopeDetector","Lucite Position Calibration not found! Using zeros.");
            std::cout << " x Lucite Position Calibration not found! Using zeros...\n";
            std::cout << " x Lucite Position Calibration not found! Using zeros...\n";
            std::cout << " x Lucite Position Calibration not found! Using zeros...\n";
            fPositionCalibration = new InSANEDetectorCalibration();
            for (int i = 0; i < 28; i++) {
               fPositionCalibration->AddCalibration(new InSANECalibration(Form("luciteXPos%d", i + 1), Form("Lucite bar %d x pos from TDCDiff", i + 1)));
               fPositionCalibration->GetChannel(i)->AddParameter(new TParameter<double>("tdcDiffOffset", 0.0));
            }
         }
         f->Close();
      }
   }

   // offsets
   for(int i = 0 ; i<28; i++) {
      fTDCDiffOffsets[i] = 0.0;//10.0 - (40.0/28.0)*float(i);
      if(fPositionCalibration){
         InSANECalibration * tdc_diff_align = fPositionCalibration->GetChannel(i);
         if(tdc_diff_align) {
            if(tdc_diff_align->GetNPar() > 0){
               fTDCDiffOffsets[i] = -1.0*tdc_diff_align->GetParameter(0)->GetVal() ;
               //std::cout << tdc_diff_align->GetNPar() << " parameters, fTDCDiffOffsets[" << i << "] = " << fTDCDiffOffsets[i] << "\n";
            }
         }
      }
   }


   fEvent                = nullptr;
   fEvent                = nullptr;
   fCorrectedEvent       = nullptr;
   fEventScaler          = nullptr;
   fCurrentScaler        = nullptr;
   fNextScaler           = nullptr;
   fPreviousScaler       = nullptr;
   fNumberOfScalerEvents = 0;
   fAverageScaler        = nullptr;

   SetRun(InSANERunManager::GetRunManager()->fCurrentRunNumber);

   fTimedTDCHitNumbers = new std::vector<Int_t>;
   fGeoCalc            = LuciteGeometryCalculator::GetCalculator();
   fCoordinateSystem   = new BigcalCoordinateSystem();
   fRotFromBCcoords    = fCoordinateSystem->GetRotationMatrixTo();

   // ------------------------------------------------
   // Histograms  
   InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals/hodoscope/hists")) {
         if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals/hodoscope")) {
            if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals")) {
               aDir = InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("pedestals");
               if (aDir) aDir->cd();
            }
            aDir = gDirectory->mkdir("hodoscope");
            if (aDir) aDir->cd();
         }
         aDir = gDirectory->mkdir("hists");
         if (aDir) aDir->cd();
      }
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals/hodoscope/hists");
   }
   fEventDisplayHitDifferences         = new TH2F("fEventDisplayHitDifferences", "Hodoscope TDC Differences", 100, -2000, 2000, 28, 1, 29);
   fEventDisplayTimedHitDifferences    = new TH2F("fEventDisplayTimedHitDifferences", "Hodoscope TDC Differences with Timed Hit", 100, -2000, 2000, 28, 1, 29);
   fRunDisplayHitDifferences           = new TH2F("fRunDisplayHitDifferences", "Hodoscope TDC Differences", 100, -2000, 2000, 28, 1, 29);
   fRunDisplayTimedHitDifferences      = new TH2F("fRunDisplayTimedHitDifferences", "Hodoscope TDC Differences with Timed Hit", 100, -2000, 2000, 28, 1, 29);
   fEventDisplayTimedHitSums           = new TH2F("fEventDisplayTimedHitSums", "Hodoscope TDC Sums with Timed Hit", 100, -6000, 0, 28, 1, 29);
   fRunDisplayTimedHitSums             = new TH2F("fRunDisplayTimedHitSums", "Hodoscope TDC Sums with Timed Hit", 100, -6000, 0, 28, 1, 29);
   fEventDisplayTimedHitAndTriggerSums = new TH2F("fEventDisplayTimedHitAndTriggerSums", "Hodoscope TDC Sums with Timed Hit", 100, -6000, 0, 28, 1, 29);
   fRunDisplayTimedHitAndTriggerSums   = new TH2F("fRunDisplayTimedHitAndTriggerSums", "Hodoscope TDC Sums with Timed Hit", 100, -6000, 0, 28, 1, 29);
   fLuciteHitDisplay                   = new TH2F("luciteHitDisplay", "Hodoscope Hits Display", 2, 1, 3, 28, 1, 29);
   fLuciteGoodHitDisplay               = new TH2F("luciteGoodHitDisplay", "Good Hodoscope Hits ", 2, 1, 3, 28, 1, 29);
   fBigcalYVsLuciteRow                 = new TH2F("ClusterYvsLucRow", "Cluster Y vs Luc Row", 110, -110, 110, 28, 1, 29);

   // These are deleted when InSANEDetecotr is deleted
   fHistograms.Add(fEventDisplayHitDifferences         );
   fHistograms.Add(fEventDisplayTimedHitDifferences    );
   fHistograms.Add(fRunDisplayHitDifferences           );
   fHistograms.Add(fRunDisplayTimedHitDifferences      );
   fHistograms.Add(fEventDisplayTimedHitSums           );
   fHistograms.Add(fRunDisplayTimedHitSums             );
   fHistograms.Add(fEventDisplayTimedHitAndTriggerSums );
   fHistograms.Add(fRunDisplayTimedHitAndTriggerSums   );
   fHistograms.Add(fLuciteHitDisplay                   );
   fHistograms.Add(fLuciteGoodHitDisplay               );
   fHistograms.Add(fBigcalYVsLuciteRow                 );

   InSANEDatabaseManager::GetManager()->CloseConnections();
}
//______________________________________________________________________________
LuciteHodoscopeDetector::~LuciteHodoscopeDetector() {
   delete fTimedTDCHitNumbers;
   delete fCoordinateSystem;
   for (int i = 0; i < 56; i++) {
      delete fTDCHits[i]      ;
      delete fTimedTDCHits[i] ;
   }
   for (int i = 0; i < 28; i++) {
      delete fNegativeTDCHits[i]               ;
      delete fPositiveTDCHits[i]               ;
      delete fTDCDifferenceNumberPassingCut[i] ;
      delete fTDCSumNumberPassingCut[i]        ;
      delete fBigcalXPositions[i]              ;
      delete fBigcalYPositions[i]              ;
      delete fTDCHitDifferences[i]             ;
      delete fTimedTDCHitDifferences[i]        ;
      delete fTDCHitSums[i]                    ;
      delete fTimedTDCHitSums[i]               ;
      delete fTDCHitAndTriggerSums[i]          ;
      delete fTimedTDCHitAndTriggerSums[i]     ;
   }
}
//______________________________________________________________________________
Int_t LuciteHodoscopeDetector::Build() {

   SetRun(72995);

   LuciteHodoscopeBar * aBar = nullptr;
   for (int i = 0; i < 28; i++) {
      aBar = new LuciteHodoscopeBar();
      aBar->SetNumber(i + 1);
      AddComponent(aBar);
   }
   SetBuilt();
   return(0);
}
//______________________________________________________________________________
Int_t LuciteHodoscopeDetector::SetRun(Int_t runnumber) {

   fCurrentRunNumber = runnumber;
   for (int i = 0; i < 56; i++) {
      GetDetectorTiming(i)->GetTiming("hodoscope", runnumber, i + 1);
      GetDetectorPedestal(i)->GetPedestal("hodoscope", runnumber, i + 1);
   }
   for (int i = 0; i < 28; i++) {
      GetDetectorDiffTiming(i)->GetTiming("hodoscopeDiffs", runnumber, i + 1);
      GetDetectorSumTiming(i)->GetTiming("hodoscopeSums", runnumber, i + 1);
   }

   InSANEDatabaseManager::GetManager()->CloseConnections();
   return(0);
}
//______________________________________________________________________________
Int_t LuciteHodoscopeDetector::ClearEvent() {

   InSANEDetector::ClearEvent();

   /*  fLucHit=false;*/
   for (int kk = 0; kk < 28; kk++) {

      cPassesTDCDiff[kk] = false;
      cPassesTDCSum[kk] = false;

      fNegativeTDCHits[kk]->clear();
      fPositiveTDCHits[kk]->clear();

      fTDCDifferenceNumberPassingCut[kk]->clear();
      fTDCSumNumberPassingCut[kk]->clear();

      fTDCHitDifferences[kk]->clear();
      fTimedTDCHitDifferences[kk]->clear();

      fTDCHitSums[kk]->clear();
      fTimedTDCHitSums[kk]->clear();

      fTimedTDCHitAndTriggerSums[kk]->clear();
      fTDCHitAndTriggerSums[kk]->clear();

      fBigcalXPositions[kk]->clear();
      fBigcalYPositions[kk]->clear();
   }

   for (int kk = 0; kk < 56; kk++) {
      cPassesTDC[kk] = false;

      fTDCHits[kk]->clear();
      fTimedTDCHits[kk]->clear();
   }

   fLuciteHitDisplay->Reset();
   fLuciteGoodHitDisplay->Reset();
   fEventDisplayHitDifferences->Reset();
   fEventDisplayTimedHitDifferences->Reset();
   fEventDisplayTimedHitSums->Reset();

   return(0);
}
//______________________________________________________________________________
Int_t LuciteHodoscopeDetector::CorrectEvent() {
   if (!fEvent) {
      std::cout << "Must set detector event address\n";
      return(-1);
   }
   return(0);
}
//______________________________________________________________________________
Int_t LuciteHodoscopeDetector::ProcessEvent() {
   if (fEvent) {
      LuciteHodoscopeHit* aLucHit;
      for (int kk = 0; kk < ((LuciteHodoscopeEvent*)fEvent)->fLuciteHits->GetEntries(); kk++) {
         aLucHit = (LuciteHodoscopeHit*)(*(((LuciteHodoscopeEvent*)fEvent)->fLuciteHits))[kk] ;
         // Loop over TDC HITS
         if (aLucHit->fLevel == 1) {
            if (aLucHit->fTDC < -1) { // TDC value should be negative

               // Fill the event hit display histogram
               //fLuciteHitDisplay->Fill(fGeoCalc->iBar(aLucHit->fPMTNumber), fGeoCalc->jBar(aLucHit->fPMTNumber));

               // Fill the vectors of Hits
               (fTDCHits[ aLucHit->fPMTNumber -1])->push_back(aLucHit->fTDC);
               // Fill the vector for the negative side hits
               if ((aLucHit->fPMTNumber - 1) % 2 == 0) {
                  (fTDCHits[ aLucHit->fPMTNumber -1])->push_back(aLucHit->fTDC);
               }
               // Place the standard TDC Cut
               if ((Double_t)aLucHit->fTDC > cTDCMin && (Double_t)aLucHit->fTDC < cTDCMax) {
                  cPassesTDC[aLucHit->fPMTNumber-1] = true;
                  // Fill the event display of "good hits"
                  //fLuciteGoodHitDisplay->Fill(fGeoCalc->iBar(aLucHit->fPMTNumber), fGeoCalc->jBar(aLucHit->fPMTNumber));
                  // Fill the vector of "Timed TDC Hits"
                  (fTimedTDCHits[ aLucHit->fPMTNumber -1])->push_back(aLucHit->fTDC);
                  /*               fTimedTDCHitNumbers[ aLucHit->fPMTNumber -1]->push_back(kk);*/
               }

            } // TDC <0
         }// fLevel==1
      }// end loop over lucite hits

      // Build up TDC Differences per bar
      for (int kk = 0; kk < 28; kk++) {
         // Differences and sums for all TDC hits
         for (unsigned int i1 = 0 ; i1 < fTDCHits[2*kk]->size() ; i1++)
            for (unsigned int i2 = 0; i2 < fTDCHits[2*kk+1]->size() ; i2++) {
               fTDCHitDifferences[kk]->push_back(fTDCHits[2*kk+1]->at(i2) - fTDCHits[2*kk]->at(i1));
               // create an aligned TDC sum
               fTDCHitSums[kk]->push_back(fTDCHits[2*kk+1]->at(i2) + fTDCHits[2*kk]->at(i1)
                     /* - fTimingSums->at(kk)->fTDCPeak */);
               fBigcalXPositions[kk]->push_back(fTDCHits[2*kk+1]->at(i2) - fTDCHits[2*kk]->at(i1)  /*- fPositionCalibration->fConstants->at(kk)*/);
               fBigcalYPositions[kk]->push_back(fGeoCalc->GetLuciteYPosition(kk + 1));

               // if the tdc sum passes the TDC sum cut, then add it's index to the bar's list of differences passing the cut
               if (fTDCHitSums[kk]->back() > cTDCSumMin && fTDCHitSums[kk]->back() < cTDCSumMax)
                  fTDCDifferenceNumberPassingCut[kk]->push_back(fTDCHitSums[kk]->size());

               // event display
               //fEventDisplayHitDifferences->Fill(fTDCHitDifferences[kk]->back(), kk + 1);
               //fRunDisplayHitDifferences->Fill(fTDCHitDifferences[kk]->back(), kk + 1);
            }
         // Differences and sums for all NEGATIVE side timed TDC hit with all untimed tdc hits
         for (unsigned int i1 = 0 ; i1 < fTimedTDCHits[2*kk]->size() ; i1++)
            for (unsigned int i2 = 0; i2 < fTDCHits[2*kk+1]->size() ; i2++) {
               fTimedTDCHitDifferences[kk]->push_back(fTDCHits[2*kk+1]->at(i2) - fTimedTDCHits[2*kk]->at(i1));
               fTimedTDCHitSums[kk]->push_back(fTDCHits[2*kk+1]->at(i2) + fTimedTDCHits[2*kk]->at(i1)
                     /*- fTimingSums->at(kk)->fTDCPeak*/);
               // Get the positions
               //               fBigcalXPositions[kk]->push_back( fTDCHits[2*kk+1]->at(i2) - fTimedTDCHits[2*kk]->at(i1) -fPositionCalibration->fConstants[kk] );
               //               fBigcalYPositions[kk]->push_back(fGeoCalc->GetLuciteYPosition(kk+1) );

               //fEventDisplayTimedHitDifferences->Fill(fTDCHitDifferences[kk]->back(), kk + 1);
               //fRunDisplayTimedHitDifferences->Fill(fTDCHitDifferences[kk]->back(), kk + 1);

               //fEventDisplayTimedHitSums->Fill(fTimedTDCHitSums[kk]->back(), kk + 1);
               //fRunDisplayTimedHitSums->Fill(fTimedTDCHitSums[kk]->back(), kk + 1);

               //fEventDisplayTimedHitAndTriggerSums->Fill(fTimedTDCHitSums[kk]->back(), kk + 1);
               //fRunDisplayTimedHitAndTriggerSums->Fill(fTimedTDCHitSums[kk]->back(), kk + 1);

            }
         // Differences and sums for all POSITIVE side timed TDC hit with all untimed tdc hits
         for (unsigned int i1 = 0 ; i1 < fTDCHits[2*kk]->size() ; i1++)
            for (unsigned int i2 = 0; i2 < fTimedTDCHits[2*kk+1]->size() ; i2++) {
               fTimedTDCHitDifferences[kk]->push_back(fTimedTDCHits[2*kk+1]->at(i2) - fTDCHits[2*kk]->at(i1));
               fTimedTDCHitSums[kk]->push_back(fTimedTDCHits[2*kk+1]->at(i2) + fTDCHits[2*kk]->at(i1)
                     /* - fTimingSums->at(kk)->fTDCPeak*/);
               // Get the positions
               //               fBigcalXPositions[kk]->push_back(fTimedTDCHits[2*kk+1]->at(i2) - fTDCHits[2*kk]->at(i1)-fPositionCalibration->fConstants[kk]);
               //               fBigcalYPositions[kk]->push_back(fGeoCalc->GetLuciteYPosition(kk+1) );

               //fEventDisplayTimedHitDifferences->Fill(fTDCHitDifferences[kk]->back(), kk + 1);
               //fRunDisplayTimedHitDifferences->Fill(fTDCHitDifferences[kk]->back(), kk + 1);

               //fEventDisplayTimedHitSums->Fill(fTimedTDCHitSums[kk]->back(), kk + 1);
               //fRunDisplayTimedHitSums->Fill(fTimedTDCHitSums[kk]->back(), kk + 1);

               //fEventDisplayTimedHitAndTriggerSums->Fill(fTimedTDCHitSums[kk]->back(), kk + 1);
               //fRunDisplayTimedHitAndTriggerSums->Fill(fTimedTDCHitSums[kk]->back(), kk + 1);
            }
      }
   } // fEvent exists

   return(0);
}
//______________________________________________________________________________
TVector3 LuciteHodoscopeDetector::GetSeedingHitPosition(TVector3 seedvec) {
   Double_t r0Mag = fGeoCalc->GetDistanceFromTarget() - fGeoCalc->fBarRadius; // about 253-240=13cm
   TVector3 r0;
   r0.SetMagThetaPhi(r0Mag, TMath::Pi() * 40.0 / 180.0, 0);
   // l is the length along the seedvec until it hits the cylinder surface
   Double_t Theta = seedvec.Theta();
   Double_t Phi = seedvec.Phi();
   Double_t R = fGeoCalc->fBarRadius;
   Double_t l = (r0.Z() * TMath::Cos(Theta) + r0.X() * TMath::Cos(Phi) * TMath::Sin(Theta) +
         TMath::Sqrt((TMath::Power(R, 2) - TMath::Power(r0.X(), 2)) * TMath::Power(TMath::Cos(Theta), 2) +
            TMath::Cos(Phi) * ((TMath::Power(R, 2) - TMath::Power(r0.Z(), 2)) * TMath::Cos(Phi) *
               TMath::Power(TMath::Sin(Theta), 2) + r0.X() * r0.Z() * TMath::Sin(2.0 * Theta)))) /
      (TMath::Power(TMath::Cos(Theta), 2) + TMath::Power(TMath::Cos(Phi), 2) * TMath::Power(TMath::Sin(Theta), 2));
   Double_t y = (-1.0 * (r0.Y() * TMath::Power(TMath::Cos(Theta), 2)) + r0.Z() * TMath::Cos(Theta) * TMath::Sin(Theta) * TMath::Sin(Phi) +
         TMath::Sin(Theta) * (-1.0 * (r0.Y() * TMath::Power(TMath::Cos(Phi), 2) * TMath::Sin(Theta)) +
            r0.X() * TMath::Cos(Phi) * TMath::Sin(Theta) * TMath::Sin(Phi) +
            TMath::Sqrt((TMath::Power(R, 2) - TMath::Power(r0.X(), 2)) * TMath::Power(TMath::Cos(Theta), 2) +
               TMath::Cos(Phi) * ((TMath::Power(R, 2) - TMath::Power(r0.Z(), 2)) * TMath::Cos(Phi) *
                  TMath::Power(TMath::Sin(Theta), 2) + r0.X() * r0.Z() * TMath::Sin(2.0 * Theta))) * TMath::Sin(Phi))) /
      (TMath::Power(TMath::Cos(Theta), 2) + TMath::Power(TMath::Cos(Phi), 2) * TMath::Power(TMath::Sin(Theta), 2));
   // gamma is the angle in cylindrical coords with its "z" along the y axis (vertical for the hodoscope)
   Double_t gamma = TMath::ACos(((r0.X() * TMath::Cos(Theta) * TMath::Cos(Phi) * TMath::Sin(Theta)) /
            (TMath::Power(TMath::Cos(Theta), 2) + TMath::Power(TMath::Cos(Phi), 2) * TMath::Power(TMath::Sin(Theta), 2)) -
            (r0.Z() * TMath::Power(TMath::Cos(Phi), 2) * TMath::Power(TMath::Sin(Theta), 2)) /
            (TMath::Power(TMath::Cos(Theta), 2) + TMath::Power(TMath::Cos(Phi), 2) * TMath::Power(TMath::Sin(Theta), 2)) +
            (TMath::Cos(Theta) * TMath::Sqrt(TMath::Power(R, 2) * TMath::Power(TMath::Cos(Theta), 2) -
                                             TMath::Power(r0.X(), 2) * TMath::Power(TMath::Cos(Theta), 2) +
                                             TMath::Power(R, 2) * TMath::Power(TMath::Cos(Phi), 2) * TMath::Power(TMath::Sin(Theta), 2) -
                                             TMath::Power(r0.Z(), 2) * TMath::Power(TMath::Cos(Phi), 2) * TMath::Power(TMath::Sin(Theta), 2) +
                                             r0.X() * r0.Z() * TMath::Cos(Phi) * TMath::Sin(2.0 * Theta))) /
            (TMath::Power(TMath::Cos(Theta), 2) + TMath::Power(TMath::Cos(Phi), 2) * TMath::Power(TMath::Sin(Theta), 2))) / R);
   //      std::cout << " l = " << l << " , R = " << R << " , y = " << y << " , gamma " << gamma*180.0/TMath::Pi() << " \n";
   //      Double_t denom = TMath::Cos(TMath::Pi()*40.0/180.0)*TMath::Cos(seedvec.Phi())*TMath::Sin(seedvec.Theta())
   //                + TMath::Cos(TMath::Pi()*40.0/180.0)*TMath::Cos(seedvec.Theta());
   TVector3 result;
   result.SetXYZ(l * TMath::Sin(gamma), y, l * TMath::Cos(gamma));
   return(result);
}
//______________________________________________________________________________
void LuciteHodoscopeDetector::PrintTimings() const {

   InSANEDetector::PrintTimings();
   Int_t tot = fSumTimings.GetEntries();
   if (tot > 0) {
      std::cout << "  Sums: \n";
      GetDetectorSumTiming(0)->PrintTimingHead();
      for (Int_t i = 0; i < tot  ; i++) {
         GetDetectorSumTiming(i)->PrintTiming();
      }
   }

   tot = fDiffTimings.GetEntries();
   if (tot > 0) {
      std::cout << "  Differences: \n";
      GetDetectorDiffTiming(0)->PrintTimingHead();
      for (Int_t i = 0; i < tot  ; i++) {
         GetDetectorDiffTiming(i)->PrintTiming();
      }
   }
}
//______________________________________________________________________________
void LuciteHodoscopeDetector::PrintCuts() const {
   PrintTimings();
   std::cout << "___________________________________\n";
   std::cout << "c      Lucite Hodoscope Cuts       \n";
   std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n";
   std::cout << Form("^  %6.1f  <    TDC    <  %6.1f     \n", cTDCMin, cTDCMax);
   std::cout << Form("^  %6.1f  <  TDCSUM   <  %6.1f     \n", cTDCSumMin, cTDCSumMax);
   std::cout << Form("^  %6.1f  <  TDCDIFF  <  %6.1f     \n", cTDCDiffMin, cTDCDiffMax);
   std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n";
}
//______________________________________________________________________________

