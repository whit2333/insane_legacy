#include "InSANEPhysicsEvent.h"

ClassImp(InSANEPhysicsEvent)

//______________________________________________________________________________
InSANEPhysicsEvent::InSANEPhysicsEvent() : fPID(0),fNTracks(0) {
}
//______________________________________________________________________________
InSANEPhysicsEvent::~InSANEPhysicsEvent() {
}
//______________________________________________________________________________
void InSANEPhysicsEvent::Reset() {
   fPID     = 0;
   fNTracks = 0;
}
//______________________________________________________________________________
void InSANEPhysicsEvent::ClearEvent(Option_t * opt){
   InSANEEvent::ClearEvent(opt);
   fPID         = 0;
   fNTracks     = 0;
}
//______________________________________________________________________________
void InSANEPhysicsEvent::Print(Option_t * opt) const {
   InSANEEvent::Print(opt);
   std::cout << "   fPID: " << fPID << " , fNTracks: " << fNTracks << "\n";
}
//______________________________________________________________________________

