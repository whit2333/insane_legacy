#include "HumanCoordinateSystem.h"

ClassImp(HumanCoordinateSystem)



void HumanCoordinateSystem::DrawCoordinateSystems()
{

//  Float_t length = 100.;
   /*  HMSCoordinateSystem * hmscoords = new HMSCoordinateSystem();*/
//   TEveManager * mgr =  TEveManager::Create();
//
// // Add BETA from VGM root file created in BETAG4
//   mgr->RegisterGeometryAlias("BETA", "/home/whit/sane08/BETAG4/run/VGM_test.root");
//   TGeoManager * geomgr = mgr->GetGeometryByAlias("BETA");
//
// // Define some materials and media
//   TGeoMaterial * matVacuum = new TGeoMaterial("Vacuum", 0,0,0);
//   TGeoMedium * Vacuum = new TGeoMedium("Vacuum",1, matVacuum);
//
// // The Main Volume
//   TGeoVolume * humanBETA = geomgr->MakeBox("humanBETA", Vacuum, 10.0*length, 10.0*length, 10.0*length);
//   TRotation * hmsR = hmscoords->GetRotationMatrixTo("human");
// // Get the top volume from VGM root file
//     humanBETA->AddNode( geomgr->GetTopVolume(),1,new TGeoRotation("HMS to Human",hmsR->GetXPsi(),hmsR->GetXTheta(),hmsR->GetXPhi() ) );
//     humanBETA->SetAsTopVolume();
//
//    TGeoNode* node1 = geomgr->GetTopNode();
//   TEveGeoTopNode* its = new TEveGeoTopNode(geomgr, node1);
//   mgr->AddGlobalElement(its);
//   mgr->Redraw3D(kTRUE);
//
// using namespace TMath;
//    TRandom r(0);
//
//    // -- Set Constants
//    Int_t nCones  = 10;
//    Int_t nTracks = 200;
//
//    Float_t coneRadius = 0.4;
//
//   TStyle * myStyle = new TStyle();
//
//    // -- Define palette
//    myStyle->SetPalette(1, 0);
//    myStyle->cd();
//    TEveRGBAPalette* pal = new TEveRGBAPalette(0, 500);
// // -----------------------------------------------------------------------------------
//    // -- Line sets
//    // -----------------------------------------------------------------------------------
//
//    // -- Define cone center
//    TEveStraightLineSet* xyaxes = new TEveStraightLineSet("Coordinate XY Axes");
//    xyaxes->SetLineColor(kGreen);
//    xyaxes->SetLineWidth(1);
//    xyaxes->AddLine(0., 0., 0., length, 0., 0.);
//    xyaxes->AddLine(0., 0., 0., 0., length, 0.);
//
//    TEveStraightLineSet* zaxis = new TEveStraightLineSet("Coordinate Z Axis");
//    zaxis->SetLineColor(kRed);
//    zaxis->SetLineWidth(1);
//    zaxis->AddLine(0., 0., 0., 0., 0., length);
//
//    mgr->AddElement(xyaxes);
//    mgr->AddElement(zaxis);
//    mgr->Redraw3D(kTRUE);


   /*
      TEveStraightLineSet* tracksXYZ = new TEveStraightLineSet("StraightLinesXYZ");
      tracksXYZ->SetLineColor(kRed);
      tracksXYZ->SetLineWidth(2);

      TEveStraightLineSet* tracksEtaPhi = new TEveStraightLineSet("StraightLinesEtaPhi");
      tracksEtaPhi->SetLineColor(kYellow);
      tracksEtaPhi->SetLineWidth(2);

      TEveStraightLineSet* tracksSeedEtaPhi = new TEveStraightLineSet("StraightLinesEtaPhiSeed");
      tracksSeedEtaPhi->SetLineColor(kBlue);
      tracksSeedEtaPhi->SetLineWidth(2);*/

// -----------------------------------------------------------------------------------
   // -- Draw track distribution in XYZ ( in TPC Volume ) ( +/-250, +/-250, +/-250 )
   // -----------------------------------------------------------------------------------

//    for ( Int_t track=0; track < nTracks ; track++ ) {
//
//       Float_t trackX = r.Uniform(-250.0, 250.0);
//       Float_t trackY = r.Uniform(-250.0, 250.0);
//       Float_t trackZ = r.Uniform(-250.0, 250.0);
//       Float_t trackR = (Float_t) Sqrt ( trackX*trackX + trackY*trackY + trackZ*trackZ );
//
//       /*
//         Float_t trackEta =  0.5 * (Float_t) Log( (Double_t)(( trackR+trackZ )/( trackR-trackZ )) );
//         Float_t trackPhi = (Float_t) ATan2( (Double_t) trackY, (Double_t) trackX );
//         if ( trackPhi < 0. ) trackPhi += (Float_t) TwoPi();
//       */
//
//       TEveVector trackDir(trackX/trackR, trackY/trackR ,trackZ/trackR);
//       TEveVector trackEnd = trackDir * length;
//       tracksXYZ->AddLine(0., 0., 0., trackEnd.fX, trackEnd.fY, trackEnd.fZ );
//    }




   /*
      TGeoManager *geoma = new TGeoManager("simple1", "Simple geometry");
     //--- define some materials
      TGeoMaterial *matVacuum = new TGeoMaterial("Vacuum", 0,0,0);
      TGeoMaterial *matAl = new TGeoMaterial("Al", 26.98,13,2.7);
   //   //--- define some media
      TGeoMedium *Vacuum = new TGeoMedium("Vacuum",1, matVacuum);
      TGeoMedium *Al = new TGeoMedium("Root Material",2, matAl);
    //--- make the top container volume

      TGeoVolume *top = geom->MakeBox("TOP", Vacuum, 270., 270., 270.);
      geom->SetTopVolume(top);

      TGeoVolume *replica = geom->MakeBox("REPLICA", Vacuum,10.0,10.0,10.0);
      replica->SetVisibility(kFALSE);

      TGeoVolume *coords = geom->MakeBox("coords", Vacuum,50.0,50.0,50.0);
      coords->SetVisibility(kFALSE);

      TGeoVolume *coords2 = geom->MakeBox("coords2", Vacuum,50.0,50.0,50.0);
      coords2->SetVisibility(kFALSE);
   //_____________________________________________________________________//
       TGeoRotation   *xrot = new TGeoRotation("xrot", 90.0,90.0, 0.0);
       TGeoRotation   *yrot = new TGeoRotation("yrot", 180.0, 90.0, 0.0);
       TGeoTranslation * t1 = new TGeoTranslation(0.0,0.0,10.0);
       TGeoTranslation * t2 = new TGeoTranslation(2.0,2.0,2.0);

      TGeoVolume *displacedTube = geom->MakeBox("displacedTube", Vacuum,5.0,5.0,20.0);
         displacedTube->SetVisibility(kFALSE);

      TGeoVolume * tubeForAxes = geom->MakeTube("tubeForAxes", Al, 0.0, 0.2, 10.);
      displacedTube->AddNode( tubeForAxes, 1,   t1);

      TGeoVolume *displacedrightanglesbox = geom->MakeBox("displacedrightanglesbox", Vacuum,10.0,10.0,10.0);
         displacedrightanglesbox->SetVisibility(kFALSE);

      TGeoVolume * rightanglesbox = geom->MakeBox("rightanglesbox", Vacuum,2.0,2.0,2.0);
      coords->SetLineColor(kGreen);
      displacedrightanglesbox->AddNode( rightanglesbox, 1,   t2);

      tubeForAxes->SetLineColor(kRed);
      TGeoVolume * xAxes = (TGeoVolume *) displacedTube->Clone("xAxes");

      tubeForAxes->SetLineColor(kRed);
      TGeoVolume * yAxes = (TGeoVolume *) displacedTube->Clone("yAxes");

      tubeForAxes->SetLineColor(kRed+3);
      TGeoVolume * zAxes = (TGeoVolume *) displacedTube->Clone("zAxes");

      tubeForAxes->SetLineColor(kBlue);
      TGeoVolume * xAxes2 = (TGeoVolume *) displacedTube->Clone("xAxes2");

      tubeForAxes->SetLineColor(kBlue);
      TGeoVolume * yAxes2 = (TGeoVolume *) displacedTube->Clone("yAxes2");

      tubeForAxes->SetLineColor(kBlue+3);
      TGeoVolume * zAxes2 = (TGeoVolume *) displacedTube->Clone("zAxes2");


      coords->AddNode(  xAxes, 1,   xrot);
      coords->AddNode(  yAxes, 1,   yrot);
      coords->AddNode(  zAxes, 1,   gGeoIdentity);
      coords->AddNode(  displacedrightanglesbox, 1,   gGeoIdentity);


      coords2->AddNode(xAxes2, 1,   xrot);
      coords2->AddNode(yAxes2, 1,   yrot);
      coords2->AddNode(zAxes2, 1,   gGeoIdentity);
      coords2->AddNode(  displacedrightanglesbox, 1,   gGeoIdentity);

   //   replica->AddNode(coords, 1, new TGeoRotation("a", 40.0,60.0, 10.0));
   //_____________________________________________________________________//

      top->AddNode(coords, 1, new TGeoTranslation(0, 0, 0));
      top->AddNode(coords2, 1,  new TGeoRotation("a", 40.0,60.0, 0.0));
   //   top->AddNode(replica, 3, new TGeoTranslation(150, 150, 0));
   //   top->AddNode(replica, 4, new TGeoTranslation(-150, 150, 0));
      //--- close the geometry
      geom->CloseGeometry();
      //--- draw the ROOT box.
      // by default the picture will appear in the standard ROOT TPad.
      //if you have activated the following line in system.rootrc,
      //it will appear in the GL viewer
      //#Viewer3D.DefaultDrawOption:   ogl
      geom->SetVisLevel(4);
      top->Draw("ogle");
   */
}

