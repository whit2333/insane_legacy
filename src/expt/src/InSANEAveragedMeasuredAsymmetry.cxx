#include "InSANEAveragedMeasuredAsymmetry.h"
#include "InSANEMathFunc.h"
#include "SANEMeasuredAsymmetry.h"


InSANEAveragedMeasuredAsymmetry::InSANEAveragedMeasuredAsymmetry() : fNAsymmetries(0)
{ }
//_____________________________________________________________________________
InSANEAveragedMeasuredAsymmetry::~InSANEAveragedMeasuredAsymmetry()
{ }
//_____________________________________________________________________________

InSANEAveragedMeasuredAsymmetry::InSANEAveragedMeasuredAsymmetry(const InSANEAveragedMeasuredAsymmetry& rhs)
  : TNamed(rhs)
{
  (*this) = rhs;
}
//______________________________________________________________________________

InSANEAveragedMeasuredAsymmetry& InSANEAveragedMeasuredAsymmetry::operator=(const InSANEAveragedMeasuredAsymmetry& rhs)
{
  TNamed::operator=(rhs);
  if(&rhs != this) {
      fNAsymmetries = rhs.fNAsymmetries;
      fAsymmetryResult = rhs.fAsymmetryResult;
      fA1 = rhs.fA1;
      fA2 = rhs.fA2;
  }
  return *this;
}
//______________________________________________________________________________

void InSANEAveragedMeasuredAsymmetry::Init(const InSANEMeasuredAsymmetry& a)
{
   bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);

   // Initialize the resulting Asymmetry's histograms.
   fAsymmetryResult  = InSANEMeasuredAsymmetry(a);
   std::string aname = std::string("combined") + std::to_string(a.fNumber);
   fAsymmetryResult.SetNameTitle(aname.c_str(),aname.c_str());
   fAsymmetryResult.SetRunNumber(0);

   fA1 = InSANEMeasuredAsymmetry(a);
   fA2 = InSANEMeasuredAsymmetry(a);
   fA1.Reset();
   fA2.Reset();

   //for(auto  syst: fSystematics_x) {
   //   stream << std::setw(10) << syst.fRelativeUncertainties.GetBinContent(i) << " ";
   //}

   TH1::AddDirectory(add_status);
}
//_____________________________________________________________________________
Int_t InSANEAveragedMeasuredAsymmetry::AddToSum(const TH1F & h0, TH1F & h1, TH1F & h2) const 
{
   for(int i = 0; i<=h0.GetNbinsX(); i++) {
      Double_t Ai = h0.GetBinContent(i);
      Double_t si = h0.GetBinError(i);
      if(Ai == 0.0) continue; // skip bins with 0 entries... need to fix this hack
      if( TMath::IsNaN(Ai) ){
         Ai = 0;
         continue;
      }
      if( si==0 || TMath::IsNaN(si) ) si = 0.000001;
      Double_t At = h1.GetBinContent(i);
      Double_t st = h2.GetBinContent(i);
      h1.SetBinContent(i,At + Ai/(si*si) );
      h2.SetBinContent(i,st + 1.0/(si*si) );
   }
   return 0;
}
//_____________________________________________________________________________

Int_t InSANEAveragedMeasuredAsymmetry::Add(const InSANEMeasuredAsymmetry& asym)
{

   bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   //double weight_previous = fAsymmetryResult.fRunSummary.GetNormCharge();
   double weight_added    = asym.fRunSummary.GetNormCharge();

   if( fNAsymmetries == 0 ) {

      Init(asym); 

      fAsymmetryResult.fSystErrVsx.Scale(weight_added);
      fAsymmetryResult.fSystErrVsW.Scale(weight_added);
      fAsymmetryResult.fSystErrVsE.Scale(weight_added);

      for(auto  syst: fAsymmetryResult.fSystematics_x) {
         syst.Scale(weight_added);
      }
      for(auto  syst: fAsymmetryResult.fSystematics_W) {
         syst.Scale(weight_added);
      }
      for(auto  syst: fAsymmetryResult.fSystematics_E) {
         syst.Scale(weight_added);
      }

   } else {

      fAsymmetryResult.fAvgKineVsx     += asym.fAvgKineVsx;
      fAsymmetryResult.fAvgKineVsW     += asym.fAvgKineVsW;
      fAsymmetryResult.fAvgKineVsNu    += asym.fAvgKineVsNu;
      fAsymmetryResult.fAvgKineVsE     += asym.fAvgKineVsE;
      fAsymmetryResult.fAvgKineVsTheta += asym.fAvgKineVsTheta;
      fAsymmetryResult.fAvgKineVsPhi   += asym.fAvgKineVsPhi;

   }

   fNAsymmetries++;

   // vs x
   AddToSum(asym.fDilutionVsx,  fA1.fDilutionVsx,  fA2.fDilutionVsx);
   AddToSum(asym.fAsymmetryVsx, fA1.fAsymmetryVsx, fA2.fAsymmetryVsx);

   // vs W
   AddToSum(asym.fDilutionVsW,  fA1.fDilutionVsW,  fA2.fDilutionVsW);
   AddToSum(asym.fAsymmetryVsW, fA1.fAsymmetryVsW, fA2.fAsymmetryVsW);

   // vs E
   AddToSum(asym.fDilutionVsE,  fA1.fDilutionVsE,  fA2.fDilutionVsE);
   AddToSum(asym.fAsymmetryVsE, fA1.fAsymmetryVsE, fA2.fAsymmetryVsE);

   if(fNAsymmetries > 1){

     // use the InSANEMeasuredAsymmetry::operator+=() 
      fAsymmetryResult += asym;

      // Systematic error histograms
      fAsymmetryResult.fSystErrVsx.Add(&asym.fSystErrVsx, weight_added);
      fAsymmetryResult.fSystErrVsW.Add(&asym.fSystErrVsW, weight_added);
      fAsymmetryResult.fSystErrVsE.Add(&asym.fSystErrVsE, weight_added);

      int i_sys = 0;
      for(auto  syst: fAsymmetryResult.fSystematics_x) {
         syst.Add(asym.fSystematics_x.at(i_sys), weight_added);
         i_sys++;
      }

      i_sys = 0;
      for(auto  syst: fAsymmetryResult.fSystematics_W) {
         syst.Add(asym.fSystematics_W.at(i_sys), weight_added);
         i_sys++;
      }

      i_sys = 0;
      for(auto  syst: fAsymmetryResult.fSystematics_E) {
         syst.Add(asym.fSystematics_E.at(i_sys), weight_added);
         i_sys++;
      }
   }
   TH1::AddDirectory(add_status);

   return(fNAsymmetries);
}
//_____________________________________________________________________________

Int_t InSANEAveragedMeasuredAsymmetry::CalculateMean(TH1F & h0, const TH1F & h1, const TH1F & h2)
{
   // Calculates the weighted mean and proper error.

   for(int i = 0; i <= h0.GetNbinsX(); i++) {
     double gbin = h0.GetBin(i);
      Double_t num = h1.GetBinContent(gbin);
      Double_t denom = h2.GetBinContent(gbin);
      if( denom==0 ) {
         denom = 0.00000001;
         num = 0.0;
      }
      h0.SetBinContent(gbin, num/denom );
      h0.SetBinError(  gbin, 1.0/TMath::Sqrt(denom));
   }
   return 0;
}
//_____________________________________________________________________________

InSANEMeasuredAsymmetry InSANEAveragedMeasuredAsymmetry::Calculate()
{

   bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   if( fNAsymmetries == 0 ) {
      Error("Calculate","No asymmetries added."); 
      return(fAsymmetryResult);
   }

   fAsymmetryResult.SetRunSummary(&(fAsymmetryResult.fRunSummary));

   double weight_total = fAsymmetryResult.fRunSummary.GetNormCharge();

   // vs x
   CalculateMean(fAsymmetryResult.fDilutionVsx,  fA1.fDilutionVsx,  fA2.fDilutionVsx);
   CalculateMean(fAsymmetryResult.fAsymmetryVsx, fA1.fAsymmetryVsx, fA2.fAsymmetryVsx);

   // vs W
   CalculateMean(fAsymmetryResult.fDilutionVsW,  fA1.fDilutionVsW,  fA2.fDilutionVsW);
   CalculateMean(fAsymmetryResult.fAsymmetryVsW, fA1.fAsymmetryVsW, fA2.fAsymmetryVsW);

   // vs E
   CalculateMean(fAsymmetryResult.fDilutionVsE,  fA1.fDilutionVsE,  fA2.fDilutionVsE);
   CalculateMean(fAsymmetryResult.fAsymmetryVsE, fA1.fAsymmetryVsE, fA2.fAsymmetryVsE);

   // Systematic error histograms
   fAsymmetryResult.fSystErrVsx.Scale( 1.0/weight_total );
   fAsymmetryResult.fSystErrVsW.Scale( 1.0/weight_total );
   fAsymmetryResult.fSystErrVsE.Scale( 1.0/weight_total );

   for(auto  syst: fAsymmetryResult.fSystematics_x) {
      syst.Scale(1.0/weight_total);
   }
   for(auto  syst: fAsymmetryResult.fSystematics_W) {
      syst.Scale(1.0/weight_total);
   }
   for(auto  syst: fAsymmetryResult.fSystematics_E) {
      syst.Scale(1.0/weight_total);
   }

   TH1::AddDirectory(add_status);
   return(fAsymmetryResult);
}
//_____________________________________________________________________________

