#include "InSANERunFlag.h"

ClassImp(InSANERunFlag)

//________________________________________________________________________________
void InSANERunFlag::Print(Option_t *) const { 
      std::cout << "Run Flag:" << GetTitle() << "\n";
      std::cout << "    run = " << fRunNumber << "\n";
      std::cout << "  value = " << fValue << "\n";
      std::cout << "    set = " << fSetValue << "\n";
      std::cout << "  start = " << fStartingEvent << "\n";
      std::cout << "    end = " << fEndingEvent << "\n";
      std::cout << fDescription.Data() << "\n";
}
//________________________________________________________________________________
void InSANERunFlag::UpdateFlag() {
   TString query = " UPDATE run_flags SET  run_number=";
   query += fRunNumber;
   query += ", ending_event=";
   query += fEndingEvent;
   query += ",event_description='";
   query += fDescription.Data();
   query += "' WHERE run_number=";
   query += fRunNumber;
   query += " AND starting_event=";
   query += fStartingEvent;
   query += " AND event_title='";
   query += GetTitle();
   query += "' ;";
   /*TSQLResult * res = */
   InSANEDatabaseManager::GetManager()->ExecuteInsert(&query);
   InSANEDatabaseManager::GetManager()->CloseConnections();
}
//________________________________________________________________________________
void InSANERunFlag::RecordFlag() {
   TString query = " INSERT INTO run_flags (flag_id,run_number";
   query += ",starting_event";
   query += ",ending_event";
   query += ",event_title";
   query += ",event_description ) VALUES ( NULL,";
   query += fRunNumber;
   query += ",";
   query += fStartingEvent;
   query += ",";
   query += fEndingEvent;
   query += ",'";
   query += GetTitle();
   query += "','";
   query += fDescription.Data();
   query += "' ) ;";
   /*TSQLResult * res =*/
   InSANEDatabaseManager::GetManager()->ExecuteInsert(&query);
   InSANEDatabaseManager::GetManager()->CloseConnections();
}

