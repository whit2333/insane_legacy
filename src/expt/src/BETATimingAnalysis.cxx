#define BETATimingAnalysis_cxx
#include "SANEEvents.h"
#include "GasCherenkovHit.h"
#include "InSANERun.h"
#include "TF1.h"
#include "BETATimingAnalysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TMath.h"
#include "SANERunManager.h"

ClassImp(BETATimingAnalysis)
//_____________________________________________________________________________

/// par0 is the amplitude, par1 is the width par2 is the mean
Double_t lorentzianPeak(Double_t *x, Double_t *par)
{
   return (0.5 * par[0] * par[1] / TMath::Pi()) /
          TMath::Max(1.e-10, (x[0] - par[2]) * (x[0] - par[2]) + .25 * par[1] * par[1]
                    );
}
//_____________________________________________________________________________

BETATimingAnalysis::BETATimingAnalysis(const char * sourceTreeName)
{

   auto * runman = (SANERunManager*) SANERunManager::GetRunManager();
   fRunNumber = runman->fCurrentRun->GetRunNumber();

   //InSANERun* aRun = runman->fCurrentRun; //runman->GetRunObject();
   // dbManager->PrintTables();

   //  fAnalysisFile = new TFile(Form("data/rootfiles/SANE%d.root",fRunNumber));
   fAnalysisFile = runman->GetCurrentFile();
   fAnalysisFile->cd();
   fEvents = new SANEEvents(sourceTreeName);
   fTDCRangeMin = -2500;
   fTDCRangeMax = -1500;
// Make sure this comes after opening the file
   /*  fAnalysisFile->mkdir("timing");*/
   fAnalysisFile->cd("timing");

//___________________________________________________________________
   /// Cherenkov
   /*  fAnalysisFile->mkdir("cherenkov");*/
   fAnalysisFile->cd("timing/cherenkov");
// Create Histograms to be filled by Analyze()
   fCherenkovTdcHists = new TClonesArray("TH1F", 12);
   for (int jj = 0; jj < 12; jj++)
      new((*fCherenkovTdcHists)[jj]) TH1F(Form("CerTDC%d", jj + 1), Form("Cherenkov %d", jj + 1), 200, fTDCRangeMin - 500, fTDCRangeMax + 500);
   for (int i = 0; i < 12; i++) {
      cer_tdc_hist2[i] = new TH1F(Form("CerTDC-2-%d", i + 1), Form("Cherenkov -2-  %d", i + 1), 300, -2000, -1500);
      cer_tdc_hist[i] = ((TH1F *)(*fCherenkovTdcHists)[i]);//new TH1F(Form("cer_tdc_hist_%d",i),Form("Cherenkov TDC %d",i), 200,-3000,-1200);
      cer_adc_hist[i] = new TH1F(Form("cer_adc_hist_%d", i), Form("Cherenkov ADC %d", i),  200, -1000, 3000);
      cer_tdc_vs_adc_hist[i] = new TH2F(Form("cer_tdc_vs_adc_hist_%d", i), Form("Cherenkov TDC %d", i), 200, -100, 1800,  200, -3000, -1200);
   }
// Create Timing data arrays
   fCherenkovTiming = new TClonesArray("InSANEDetectorTiming", 12);

//___________________________________________________________________
   /// Tracker
   /*  fAnalysisFile->cd("..");*/
   /*  fAnalysisFile->mkdir("tracker");*/
   fAnalysisFile->cd("timing/tracker");
   fTrackerTdcHists = new TClonesArray("TH1F", 2 * 128 + 64);
   for (int jj = 0; jj < 2 * 128 + 64; jj++) {
      new((*fTrackerTdcHists)[jj])
      TH1F(Form("TrackerTDC%d", jj + 1), Form("Tracker TDC %d", jj + 1), 125, -9000, -4000);
   }
   fTrackerTiming = new TClonesArray("InSANEDetectorTiming", 2 * 128 + 64);

//___________________________________________________________________
   /// Hodoscope

   /*  fAnalysisFile->cd("..");*/
   /*  fAnalysisFile->mkdir("hodoscope");*/
   fAnalysisFile->cd("timing/hodoscope");

   /*  fLuciteTDCHitsPerPMT = new std::vector<std::vector<Int_t> * >;*/

   fLuciteTdcHists = new TClonesArray("TH1F", 56);
   for (int jj = 0; jj < 56; jj++) {
      new((*fLuciteTdcHists)[jj]) TH1F(Form("LucTDC%d", jj + 1), Form("Lucite TDC %d", jj + 1), 100, -2500, -500);
      fLuciteTDCHitsPerPMT[jj] = new std::vector<Int_t>;
      fLuciteTDCHitsPerPMT[jj]->clear();
   }
   fLuciteTiming = new TClonesArray("InSANEDetectorTiming", 56);
   fLuciteSumTiming = new TClonesArray("InSANEDetectorTiming", 28);
   fLuciteDiffTiming = new TClonesArray("InSANEDetectorTiming", 28);

   fLuciteTdcDiffHists = new TClonesArray("TH1F", 28);
   fLuciteTdcSumHists = new TClonesArray("TH1F", 28);
   for (int jj = 0; jj < 28; jj++) {
      new((*fLuciteTdcDiffHists)[jj]) TH1F(Form("LucTDCRow%dDiff", jj + 1), Form("Lucite TDC Row %d TDC Diff", jj + 1), 500, -1000, 1000);
      new((*fLuciteTdcSumHists)[jj]) TH1F(Form("LucTDCRow%dSum", jj + 1), Form("Lucite TDC Row %d TDC Sum", jj + 1), 200, -5000, -2000);
   }

//___________________________________________________________________
   /// Bigcal
   /*  fAnalysisFile->cd("..");*/
   /*  fAnalysisFile->mkdir("bigcal");*/
   fAnalysisFile->cd("timing/bigcal");
   fBigcalTdcHists = new TClonesArray("TH1F", 42);
   fBigcalTimewalkHists = new TClonesArray("TH2F", 42);
   fBigcal32TdcHists = new TClonesArray("TH1F", 224);
   fBigcal32TimewalkHists = new TClonesArray("TH2F", 224);
   // Bigcal trigger logic TDCs
   for (int jj = 0; jj < 38; jj++) {
      new((*fBigcalTdcHists)[jj]) TH1F(Form("Bigcal%d", jj + 1), Form("Bigcal %d", jj + 1), 200, 200, 600);
      new((*fBigcalTimewalkHists)[jj]) TH2F(Form("BigcalTimewalk%d", jj + 1), Form("Bigcal Timewalk %d", jj + 1), 200, -1000, 9000,  200, 200, 600);
   }
   // Bigcal TDCs for groups of 32 cells (224= 128prot + 96rcs
   for (int jj = 0; jj < 224; jj++) {
      new((*fBigcal32TdcHists)[jj]) TH1F(Form("Bigcal32%d", jj + 1), Form("Bigcal 32 %d", jj + 1), 250, 250, 750);
      new((*fBigcal32TimewalkHists)[jj]) TH2F(Form("Bigcal32Timewalk%d", jj + 1), Form("Bigcal 32 Timewalk %d", jj + 1), 200, -1000, 9000,  200, 0, 1000);
   }

   fBigcalTiming = new TClonesArray("InSANEDetectorTiming", 224);
   fBigcalTriggerTiming = new TClonesArray("InSANEDetectorTiming", 224);


   fAnalysisFile->cd();
   fOutputTree = new TTree("BETATiming", "BETA Detector Timing Data");
   fOutputTree->Branch("CherenkovTimingArray", &fCherenkovTiming);
   fOutputTree->Branch("LuciteTimingArray", &fLuciteTiming);
   fOutputTree->Branch("LuciteSumTimingArray", &fLuciteSumTiming);
   fOutputTree->Branch("LuciteDiffTimingArray", &fLuciteDiffTiming);
   fOutputTree->Branch("LuciteDiffTimingArray", &fLuciteDiffTiming);
   fOutputTree->Branch("BigcalTimingArray", &fBigcalTiming);
   fOutputTree->Branch("BigcalTriggerTimingArray", &fBigcalTriggerTiming);
   fOutputTree->Branch("TrackerTimingArray", &fTrackerTiming);

//   fOutputTree->SetAutoSave();

   bcgeo = BIGCALGeometryCalculator::GetCalculator();
}
//_____________________________________________________________________________

BETATimingAnalysis::~BETATimingAnalysis()
{

   SANERunManager::GetRunManager()->fCurrentRun->fTimingAnalyzed = 1; /// \todo true but possibly more levels
   SANERunManager::GetRunManager()->WriteRun();

   // Delete the stuff you don't want to re-write to file
   if (fEvents->fTree) delete fEvents->fTree;
   if (fEvents) delete fEvents; // This Deletes the tree
   fEvents = nullptr;

   // Now write the rest to file
   if (fOutputTree)fOutputTree->FlushBaskets();
   if (fAnalysisFile) fAnalysisFile->Write();
   //fAnalysisFile->Write();
   //if(fOutputTree)fOutputTree->Write();

   for (int i = 0; i < 12; i++) {
      if (cer_adc_hist[i]) delete cer_adc_hist[i];
      if (cer_tdc_vs_adc_hist[i]) delete cer_tdc_vs_adc_hist[i];
   }

//    delete fEvents;

   if (fCherenkovTdcHists) {
      fCherenkovTdcHists->Delete();
      delete  fCherenkovTdcHists;
   }
   if (fLuciteTdcHists)  {
      fLuciteTdcHists->Delete();
      delete  fLuciteTdcHists;
   }
   if (fBigcalTdcHists) {
      fBigcalTdcHists->Delete();
      delete  fBigcalTdcHists;
   }
   if (fTrackerTdcHists) {
      fTrackerTdcHists->Delete();
      delete  fTrackerTdcHists;
   }
   if (fBigcalTimewalkHists) fBigcalTimewalkHists->Delete();
   if (fBigcal32TimewalkHists) fBigcal32TimewalkHists->Delete();
   if (fBigcal32TdcHists) fBigcal32TdcHists->Delete();
   if (fCherenkovTiming) {
      fCherenkovTiming->Delete();
      delete  fCherenkovTiming;
   }
   if (fLuciteTiming) {
      fLuciteTiming->Delete();
      delete  fLuciteTiming;
   }
   //     if(fTrackerTiming) {
   // //      fTrackerTiming->Delete();
   //       delete  fTrackerTiming;
   //     }
   if (fBigcalTiming) {
      fBigcalTiming->Delete();
      delete  fBigcalTiming;
   }

}
//_____________________________________________________________________________

Int_t BETATimingAnalysis::AnalyzeRun()
{
   GasCherenkovHit * aCerHit;
   LuciteHodoscopeHit * aLucHit;
   BigcalHit * aBigcalHit;
   //BigcalHit * tdcBigcalHit;
   //BigcalHit * triggerBigcalHit;
   ForwardTrackerHit * aTrackerHit;

   TClonesArray * cerHits = fEvents->BETA->fGasCherenkovEvent->fGasCherenkovHits;
   TClonesArray * lucHits = fEvents->BETA->fLuciteHodoscopeEvent->fLuciteHits;
   //TClonesArray * bigcalHits = fEvents->BETA->fBigcalEvent->fBigcalHits;
   TClonesArray * trackerHits = fEvents->BETA->fForwardTrackerEvent->fTrackerHits;

   Int_t nevent = fEvents->fTree->GetEntries();
   if (InSANERunManager::GetRunManager()->GetVerbosity() > 0)   std::cout << " o BETATimingAnalysis is using " << nevent << " events. " << std::endl;
   ///// EVENT LOOP  //////

   for (int iEVENT = 0; iEVENT < nevent && iEVENT < 400000; iEVENT++) {

      fEvents->BETA->ClearEvent("C");

      fEvents->GetEvent(iEVENT);

      if (fEvents->TRIG->IsBETA2Event()) {

// Cherenkov
         for (int kk = 0; kk < fEvents->BETA->fGasCherenkovEvent->fNumberOfHits; kk++) {
            aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
            ((TH1F*)(*fCherenkovTdcHists)[aCerHit->fMirrorNumber-1])->Fill(aCerHit->fTDC);
            cer_tdc_hist[aCerHit->fMirrorNumber - 1]->Fill(aCerHit->fTDC);
            cer_tdc_hist2[aCerHit->fMirrorNumber - 1]->Fill(aCerHit->fTDC);
            cer_adc_hist[aCerHit->fMirrorNumber - 1]->Fill(aCerHit->fADC);
            cer_tdc_vs_adc_hist[aCerHit->fMirrorNumber -1 ]->Fill(aCerHit->fADC, aCerHit->fTDC);
         } // End loop over Cherenkov signals

// Tracker\
//std::cout << "Event " <<  iEVENT << " hits " << fEvents->BETA->fForwardTrackerEvent->fNumberOfHits<<  "" << std::endl;
//      std::cout << "Hits: "
//                << fEvents->BETA->fForwardTrackerEvent->fNumberOfHits << "" << std::endl;

/// \note The first layer(layer=0) has 64 bars, the next two have 128 each. (layer=1,2)
         for (int kk = 0; kk < fEvents->BETA->fForwardTrackerEvent->fNumberOfHits; kk++) {
            aTrackerHit = (ForwardTrackerHit*)(*trackerHits)[kk];
//     std::cout << "Hit " <<  kk << "/"
//               << " " << fEvents->BETA->fForwardTrackerEvent->fNumberOfHits << "" << std::endl;
// std::cout << "layer " << aTrackerHit->fScintLayer
//           << " row "  << aTrackerHit->fRow
//           << " tdc "  << aTrackerHit->fTDC
//           << "" << std::endl;
            if (aTrackerHit->fScintLayer == 0)
               ((TH1F*)(*fTrackerTdcHists)[(aTrackerHit->fRow - 1)])->Fill(aTrackerHit->fTDC);
            if (aTrackerHit->fScintLayer == 1)
               ((TH1F*)(*fTrackerTdcHists)[ 64 + (aTrackerHit->fRow-1)])->Fill(aTrackerHit->fTDC);
            if (aTrackerHit->fScintLayer == 2)
               ((TH1F*)(*fTrackerTdcHists)[ 64 + 128 + (aTrackerHit->fRow-1)])->Fill(aTrackerHit->fTDC);
         } // End loop over Tracker signals

         /// Hodoscope
         for (int jj = 0; jj < 56; jj++) {
            fLuciteTDCHitsPerPMT[jj]->clear();
         }
         for (int kk = 0; kk < fEvents->BETA->fLuciteHodoscopeEvent->fNumberOfHits; kk++) {
            aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;
            (fLuciteTDCHitsPerPMT[ aLucHit->fPMTNumber -1])->push_back(aLucHit->fTDC);
            ((TH1F*)(*fLuciteTdcHists)[ aLucHit->fPMTNumber -1])->Fill(aLucHit->fTDC);
            /*      ((TH1F*)(*fLuciteTdcHists)[aLucHit->fRow-1+28])->Fill(aLucHit->fPositiveTDC);*/
         } // End loop over Lucite signals

         for (int kk = 0; kk < 28; kk++) {
            for (unsigned int i1 = 0 ; i1 < fLuciteTDCHitsPerPMT[2*kk]->size() ; i1++)
               for (unsigned int i2 = 0; i2 < fLuciteTDCHitsPerPMT[2*kk+1]->size() ; i2++) {
                  ((TH1F*)(*fLuciteTdcDiffHists)[kk])->Fill(fLuciteTDCHitsPerPMT[2*kk+1]->at(i2) - fLuciteTDCHitsPerPMT[2*kk]->at(i1));
                  ((TH1F*)(*fLuciteTdcSumHists)[kk])->Fill(fLuciteTDCHitsPerPMT[2*kk+1]->at(i2) + fLuciteTDCHitsPerPMT[2*kk]->at(i1));
               }
         }

         //  --------- Bigcal ----------
         for (int kk = 0; kk < fEvents->BETA->fBigcalEvent->fBigcalTimingGroupHits->GetEntries(); kk++) {
            aBigcalHit = (BigcalHit*)(*fEvents->BETA->fBigcalEvent->fBigcalTimingGroupHits)[kk] ;
            // Groups of 32 blocks
            if (aBigcalHit->fTDCLevel == 1) {
               ((TH1F*)(*fBigcal32TdcHists)[(aBigcalHit->fTDCRow-1)*4+aBigcalHit->fTDCGroup-1])->Fill(aBigcalHit->fTDC);
               ((TH2F*)(*fBigcal32TimewalkHists)[(aBigcalHit->fTDCRow-1)*4+aBigcalHit->fTDCGroup-1])->Fill(aBigcalHit->fADC, aBigcalHit->fTDC);

            }
         }
         for (int kk = 0; kk < fEvents->BETA->fBigcalEvent->fBigcalTriggerGroupHits->GetEntries(); kk++) {
            aBigcalHit = (BigcalHit*)(*fEvents->BETA->fBigcalEvent->fBigcalTriggerGroupHits)[kk] ;

            /// Trigger Logic
            //      if(aBigcalHit->fTDCLevel == 3) {
            ((TH1F*)(*fBigcalTdcHists)[(aBigcalHit->fTDCRow)+(aBigcalHit->fTDCGroup-1)*19-1])->Fill(aBigcalHit->fTDC);
            ((TH2F*)(*fBigcalTimewalkHists)[(aBigcalHit->fTDCRow)+(aBigcalHit->fTDCGroup-1)*19-1])->Fill(aBigcalHit->fADC, aBigcalHit->fTDC);

            //      }
         } // End loop over Bigcal signals

      }
   } // END OF EVENT LOOP
   if (SANERunManager::GetRunManager()->fVerbosity > 0)   std::cout << " o End of BETATimingAnalysis::AnalyzeRun() " << std::endl;

   return(0);
}
//_____________________________________________________________________________
Int_t BETATimingAnalysis::Visualize() {
   return(0);
}
//_____________________________________________________________________________
Int_t BETATimingAnalysis::FitHistograms() {

   if (SANERunManager::GetRunManager()->fVerbosity > 0)   
      std::cout << " o Start of BETATimingAnalysis::FitHistograms() " << std::endl;

   Int_t maxPeaks = 5;

   auto * timewalk = new TF1("timewalk", "[0]+[1]*x+[2]*x*x", 0, 4000);
   //TF1 * bgconst = new TF1("bgconst", "[0]*x/x", 0, 4000);
   auto *lorentzian = new TF1("lorentzian", lorentzianPeak, -2500, 4000, 3);
   //TF1 *lorentzianWithBG = new TF1 ("lorentzianWithBG", "lorentzian + bgconst",0,4000);
   auto* fc = new TF1("fc", "[0]*TMath::CauchyDist(x, [2], [1])+[3]", -8500, 4000); // couchy is lorentz
   lorentzian->SetParLimits(1, 2.0, 200.0); // widthrange
   lorentzian->SetParLimits(0, 100.0, 500000000.0); // amp
   //lorentzianWithBG->SetParLimits(1,2.0,900.0); // widthrange
   //lorentzianWithBG->SetParLimits(0,100.0,500000000.0); // amp
   fc->SetParLimits(1, 2.0, 900.0); // widthrange
   fc->SetParLimits(0, 100.0, 500000000.0); // amp
   auto *f1 = new TF1("gaussian", "gaus", -8500, 4000); //[0] is amplitude [1] is mean, [2] is width

   // ----------------------------------------------------
   // Cherenkov
   fAnalysisFile->cd("timing/cherenkov");
   fSpectrumAnalyzer             = nullptr;
   Double_t doublerange          = 200.0;
   InSANEDetectorTiming * timing = nullptr;
   TH1F * h                      = nullptr;
   Double_t  apeak;
   Double_t  peak[12];
   Double_t  peakwidth[12];
   Double_t  r[12][3];

   std::cout << " o Fitting peaks for Cherenkov TDC spectra " << std::endl;

   for (int i = 0; i < 8; i++) {
      // std::cout << " o Finding peak for Cherenkov TDC " << i << "" << std::endl;
      if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
      fSpectrumAnalyzer = new TSpectrum(maxPeaks);
      fSpectrumAnalyzer->Search(cer_tdc_hist[i], 10, "nobackground", 0.2);

      ///TODO fix this hard coded hack to avoid the peak around -1729
      if (TMath::Abs(fSpectrumAnalyzer->GetPositionX()[0] + 1729) < 32)
         peak[i] = fSpectrumAnalyzer->GetPositionX()[1];
      // The following prevented the peak from the simulation from being found
      //if (TMath::Abs(fSpectrumAnalyzer->GetPositionX()[0] + 2000) < 100)
      //   peak[i] = fSpectrumAnalyzer->GetPositionX()[1];
      else
         peak[i] = fSpectrumAnalyzer->GetPositionX()[0];

      //lorentzian->SetParameter(0,300000); #  sigma: sigma of searched peaks, for details we refer to manual
      lorentzian->SetParameter(2, peak[i]);
      lorentzian->SetParLimits(2, -3000.0, -1000.0); // peak
      lorentzian->SetParameter(1, 100.0);
      std::cout << " o Fitting Cherenkov TDC PEAK " << i << " " << std::endl;
      cer_tdc_hist[i]->Fit(lorentzian, "E,Q,0,WW", " ", peak[i] - doublerange, peak[i] + doublerange);
      peakwidth[i] = lorentzian->GetParameter(1);
      cer_tdc_vs_adc_hist[i]->ProfileX()->Fit(timewalk, "E,Q,0,WW", " " , -50, 1500);
      r[i][0] = timewalk->GetParameter(0);
      r[i][1] = timewalk->GetParameter(1);
      r[i][2] = timewalk->GetParameter(2);
      //c->cd(2*8+i+1);
      //cer_adc_hist[i]->Draw();
      //cout << "chi squared" << r->Chi2() << " \n ";
      timing = new((*fCherenkovTiming)[i]) InSANEDetectorTiming(i, peak[i], peakwidth[i]);
      timing->fTimewalkParameters[0] = r[i][0];
      timing->fTimewalkParameters[1] = r[i][1];
      timing->fTimewalkParameters[2] = r[i][2];
      timing->fTimewalkParameters[3] = 0.0;
      timing->fPeakFitChiSquared = timewalk->GetChisquare();
   }
   std::cout << "  ... now for the analog sums... " << std::endl;
   //  c->SaveAs( Form("plots/tdcpeaks/cerINDIVIDUALtimewalk%d.png",fRunNumber) );
   //  c->SaveAs( Form("plots/tdcpeaks/cerINDIVIDUALtimewalk%d.ps",fRunNumber) );
   //  delete c;
   //  c = new TCanvas("Sums");
   //  c->Divide(4,3);
   for (int i = 8; i < 12; i++) {
      std::cout << " o Finding peak for  Cherenkov TDC " << i << "" << std::endl;
      if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
      fSpectrumAnalyzer = new TSpectrum(maxPeaks);
      fSpectrumAnalyzer->Search(cer_tdc_hist[i], 10, "nobackground", 0.3);
      if (fSpectrumAnalyzer->GetNPeaks() > 0) peak[i] = fSpectrumAnalyzer->GetPositionX()[0];
      lorentzian->SetParameter(2, peak[i]);
      lorentzian->SetParameter(1, 12);
      std::cout << " o Fitting Cherenkov TDC PEAK " << i << " " << std::endl;

      cer_tdc_hist[i]->Fit(lorentzian, "Q,0,E,WW", " ", peak[i] - doublerange, peak[i] + doublerange);
      peakwidth[i] = lorentzian->GetParameter(1);

      cer_tdc_vs_adc_hist[i]->ProfileX()->Fit(timewalk, "E,Q,0", " " , -100, 1000);
      r[i][0] = timewalk->GetParameter(0);
      r[i][1] = timewalk->GetParameter(1);
      r[i][2] = timewalk->GetParameter(2);

      timing = new((*fCherenkovTiming)[i]) InSANEDetectorTiming(i, lorentzian->GetParameter(2), lorentzian->GetParameter(1));
      timing->fTimewalkParameters[0] = r[i][0];
      timing->fTimewalkParameters[1] = r[i][1];
      timing->fTimewalkParameters[2] = r[i][2];
      timing->fTimewalkParameters[3] = 0.0;
      timing->fPeakFitChiSquared = lorentzian->GetChisquare();
   }
   //  c->SaveAs( Form("plots/tdcpeaks/cerSUMStimewalk%d.png",fRunNumber) );
   //  c->SaveAs(Form("plots/tdcpeaks/cerSUMStimewalk%d.ps",fRunNumber));
   //  delete c;
   //  TCanvas * c2 = new TCanvas("PedAnalysisCanvas2","Lucite Timing",800,600);
   //  c2->Divide(10,6);
   if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
   fSpectrumAnalyzer = nullptr;
   fAnalysisFile->cd();

   // ----------------------------------------------------
   // Hodoscope
   fAnalysisFile->cd("timing/hodoscope");

   std::cout << " o Fitting peaks for hodoscope TDC spectra " << std::endl;
   for (int jj = 0; jj < 56; jj++) {
      //    c2->cd(jj+1);
      /*std::cout << " o Finding peak for hodoscope TDC " << jj << "" << std::endl;*/
      if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
      fSpectrumAnalyzer = new TSpectrum(maxPeaks);

      h = (TH1F*)(*fLuciteTdcHists)[jj];
      fSpectrumAnalyzer->Search(h, 3, "nobackground", 0.45);
      if (fSpectrumAnalyzer->GetNPeaks() > 0) apeak = fSpectrumAnalyzer->GetPositionX()[0];
      fc->SetParameter(2, apeak);
      fc->SetParLimits(2, -4000.0, 500.0); // peak\
      lorentzian->SetParLimits(1, 0.0, 500.0); // widthrange\

      fc->SetParameter(1, 40.0);
      // std::cout << " o Fitting hodoscope TDC PEAK " << jj << " " << std::endl;

      h->Fit(fc, "E,Q,0", " ", apeak - doublerange, apeak + doublerange);

      timing = new((*fLuciteTiming)[jj]) InSANEDetectorTiming(jj + 1, fc->GetParameter(2), fc->GetParameter(1));
      timing->fTimewalkParameters[0] = 0.0;
      timing->fTimewalkParameters[1] = 0.0;
      timing->fTimewalkParameters[2] = 0.0;
      timing->fTimewalkParameters[3] = 0.0;
      timing->fPeakFitChiSquared = fc->GetChisquare();
      //    h->Draw();
   }


   // ----------------------------------------------------
   // Hodoscope Sums
   /*std::cout << " o Fitting peaks for hodoscope TDC spectra " << std::endl;*/
   for (int jj = 0; jj < 28; jj++) {
      //    c2->cd(jj+1);
      // std::cout << " o Finding peak for hodoscope bar " << jj << "" << std::endl;

      if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
      fSpectrumAnalyzer = new TSpectrum(maxPeaks);

      h = (TH1F*)(*fLuciteTdcSumHists)[jj];
      fSpectrumAnalyzer->Search(h, 3, "nobackground", 0.45);
      if (fSpectrumAnalyzer->GetNPeaks() > 0) apeak = fSpectrumAnalyzer->GetPositionX()[0];
      fc->SetParameter(2, apeak);
      /*    fc->SetParLimits(2,-3000.0,500.0); // peak\*/
      lorentzian->SetParLimits(1, 0.0, 500.0); // widthrange
      fc->SetParameter(1, 40.0);
      // std::cout << " Fitting hodoscope bar PEAK " << jj << " " << std::endl;

      h->Fit(fc, "E,Q,0", " ", apeak - 2.0 * doublerange, apeak + 2.0 * doublerange);

      timing = new((*fLuciteSumTiming)[jj]) InSANEDetectorTiming(jj + 1, apeak/*fc->GetParameter(2)*/, fc->GetParameter(1));
      timing->fTimewalkParameters[0] = 0.0;
      timing->fTimewalkParameters[1] = 0.0;
      timing->fTimewalkParameters[2] = 0.0;
      timing->fTimewalkParameters[3] = 0.0;
      timing->fPeakFitChiSquared = fc->GetChisquare();

      // std::cout << " o Finding peak for hodoscope bar " << jj << "" << std::endl;
   }

   // ----------------------------------------------------
   // Hodoscope Diff
   for (int jj = 0; jj < 28; jj++) {
      if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
      fSpectrumAnalyzer = new TSpectrum(maxPeaks);

      Double_t hodoscope_fitwidth = 15;
      h = (TH1F*)(*fLuciteTdcDiffHists)[jj];
      fSpectrumAnalyzer->Search(h, 3, "nobackground", 0.45);
      if (fSpectrumAnalyzer->GetNPeaks() > 0) apeak = fSpectrumAnalyzer->GetPositionX()[0];
      Double_t peak2 = 0.0;
      // Use the leftmost peak
      if (fSpectrumAnalyzer->GetNPeaks() > 1) {
         peak2 = fSpectrumAnalyzer->GetPositionX()[1];
         if (peak2 < apeak) apeak = peak2;
      }

      fc->SetParameter(2, apeak);
      /*    fc->SetParLimits(2,-3000.0,500.0); // peak\*/
      lorentzian->SetParLimits(1, 0.0, 500.0); // widthrange
      fc->SetParameter(1, 40.0);
      // std::cout << " o Fitting hodoscope bar PEAK " << jj << " " << std::endl;

      h->Fit(fc, "E,Q,0", " ", apeak - hodoscope_fitwidth, apeak + hodoscope_fitwidth);

      timing = new((*fLuciteDiffTiming)[jj]) InSANEDetectorTiming(jj + 1, apeak/*fc->GetParameter(2)*/, fc->GetParameter(1));
      timing->fTimewalkParameters[0] = 0.0;
      timing->fTimewalkParameters[1] = 0.0;
      timing->fTimewalkParameters[2] = 0.0;
      timing->fTimewalkParameters[3] = 0.0;
      timing->fPeakFitChiSquared = fc->GetChisquare();

      //    h->Draw();
   }

   //  c2->SaveAs(Form("plots/tdcpeaks/luctdc%d.png",fRunNumber));
   //  c2->SaveAs(Form("plots/tdcpeaks/luctdc%d.ps",fRunNumber));
   //  delete c2;
   delete fSpectrumAnalyzer;
   fAnalysisFile->cd();

   // ----------------------------------------------------
   // Tracker
   fAnalysisFile->cd("timing/tracker");
   doublerange = 600.0;
   fSpectrumAnalyzer = new TSpectrum(4);

   std::cout << " o Fitting peaks for forward tracker TDC spectra " << std::endl;

   for (int jj = 0; jj < 64 + 2 * 128 ; jj++) {

      if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
      fSpectrumAnalyzer = new TSpectrum(maxPeaks);

      //    c2->cd(jj+1);
      h = (TH1F*)(*fTrackerTdcHists)[jj];

      //if( !h->GetEntries() < 1000) continue;

      /*std::cout << " o Finding peak for  tracker TDC " << jj << "" << std::endl;*/
      fSpectrumAnalyzer->Search(h, 2, "nobackground", 0.4);
      if (fSpectrumAnalyzer->GetNPeaks() > 0) apeak = fSpectrumAnalyzer->GetPositionX()[0];
      fc->SetParameter(2, apeak);
      fc->SetParLimits(1, 1.0, 1000.0);
      fc->SetParLimits(2, -8500.0, -4000.0);
      fc->SetParameter(1, 50.0);
      fc->SetParameter(3, 50.0);
      // std::cout << " o Fitting tracker TDC PEAK " << jj << " " << std::endl;

      h->Fit(fc, "E,Q,0", " ", apeak - doublerange, apeak + doublerange);

      timing = new((*fTrackerTiming)[jj]) InSANEDetectorTiming(jj, fc->GetParameter(2), fc->GetParameter(1));
      timing->fTimewalkParameters[0] = 0.0;
      timing->fTimewalkParameters[1] = 0.0;
      timing->fTimewalkParameters[2] = 0.0;
      timing->fTimewalkParameters[3] = 0.0;
      timing->fPeakFitChiSquared = fc->GetChisquare();
      //    h->Draw();
   }
   //  c2->SaveAs(Form("plots/tdcpeaks/luctdc%d.png",fRunNumber));
   //  c2->SaveAs(Form("plots/tdcpeaks/luctdc%d.ps",fRunNumber));
   //  delete c2;
   delete fSpectrumAnalyzer;
   fAnalysisFile->cd();

   // ----------------------------------------------------
   // BIGCAL
   fSpectrumAnalyzer = new TSpectrum(maxPeaks);
   doublerange = 300.0;
   fAnalysisFile->cd("timing/bigcal");

   std::cout << " o Fitting peaks for BigCal TDC spectra " << std::endl;

   //  TCanvas * c3;
   for (int ii = 0; ii < 4; ii++) { // loop over the group (or column) of which there are four
      //  c3 = new TCanvas("TdcAnalysisCanvas3",Form("Bigcal Timing - Row %d",ii),800,600);
      //  c3->Divide(8,8);
      for (int kk = 0; kk < 7; kk++) {
         for (int jj = 0; jj < 8; jj++) {
            //    c3->cd(jj+kk*8+1);
            h = (TH1F*)(*fBigcal32TdcHists)[jj+kk*8+56*ii];
            // std::cout << " o Finding peak for  BigCal TDC " << jj+kk*8+56*ii+1 << "" << std::endl;

            if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
            fSpectrumAnalyzer = new TSpectrum(maxPeaks);

            fSpectrumAnalyzer->Search(h, 10, "nobackground", 0.35);
            if (fSpectrumAnalyzer->GetNPeaks() > 0) apeak = fSpectrumAnalyzer->GetPositionX()[0];
            f1->SetParameter(1, apeak);
            f1->SetParameter(2, 50.0);
            f1->SetParLimits(2, 10.0, 500.0);
            f1->SetParLimits(1, -5000.0, 8000.0);
            // std::cout << " o Fitting BigCal TDC PEAK " <<  jj+kk*8+56*ii+1 << " " << std::endl;

            h->Fit(f1, "E,Q,0", " ", apeak - doublerange, apeak + doublerange);
            timing = new((*fBigcalTiming)[jj+kk*8+56*ii]) InSANEDetectorTiming(jj + kk * 8 + 56 * ii + 1, f1->GetParameter(1), f1->GetParameter(2));
            timing->fTimewalkParameters[0] = 0.0;
            timing->fTimewalkParameters[1] = 0.0;
            timing->fTimewalkParameters[2] = 0.0;
            timing->fTimewalkParameters[3] = 0.0;
            timing->fPeakFitChiSquared = f1->GetChisquare();
            //    h->Draw();
         }
      }
      //  c3->SaveAs(Form("plots/tdcpeaks/bigcalRow%dtdcs%d.png",ii+1,fRunNumber));
      //  c3->SaveAs(Form("plots/tdcpeaks/bigcalRow%dtdc%d.ps",ii+1,fRunNumber));
      //  delete c3;
   }

   // ----------------------------------------------------
   // BIGCAL timing groups
   for (int jj = 0; jj < 38; jj++) {
      //    c3->cd(jj+kk*8+1);
      h = (TH1F*)(*fBigcalTdcHists)[jj];
      // std::cout << " o Finding peak for  BigCal TDC " << jj+kk*8+56*ii+1 << "" << std::endl;

      if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;
      fSpectrumAnalyzer = new TSpectrum(maxPeaks);

      fSpectrumAnalyzer->Search(h, 2, "nobackground", 0.35);
      if (fSpectrumAnalyzer->GetNPeaks() > 0) apeak = fSpectrumAnalyzer->GetPositionX()[0];
      f1->SetParameter(1, apeak);
      f1->SetParameter(2, 50.0);
      f1->SetParLimits(2, 10.0, 500.0);
      f1->SetParLimits(1, -5000.0, 8000.0);
      // std::cout << " o Fitting BigCal TDC PEAK " <<  jj+kk*8+56*ii+1 << " " << std::endl;

      h->Fit(f1, "E,Q,0", " ", apeak - doublerange, apeak + doublerange);
      timing = new((*fBigcalTriggerTiming)[jj]) InSANEDetectorTiming(jj + 1, apeak/*f1->GetParameter(1)*/, f1->GetParameter(2));
      timing->fTimewalkParameters[0] = 0.0;
      timing->fTimewalkParameters[1] = 0.0;
      timing->fTimewalkParameters[2] = 0.0;
      timing->fTimewalkParameters[3] = 0.0;
      timing->fPeakFitChiSquared = f1->GetChisquare();
      //    h->Draw();
   }


   if (fOutputTree) fOutputTree->Fill();
   FillDatabase();
   if (fSpectrumAnalyzer) delete fSpectrumAnalyzer;

   fAnalysisFile->cd();

   return(0);
}
//_____________________________________________________________________________
Int_t BETATimingAnalysis::CreatePlots() {
   TH1F * h;
   const Int_t kNotDraw = 1 << 9;
   TCanvas * c1;

   c1  = new TCanvas("TDCAnalysisCanvas", "Cherenkov Timing");
   c1->Divide(4, 3);
   for (int jj = 0; jj < 12; jj++) {
      h = (TH1F*)(*fCherenkovTdcHists)[jj];
      c1->cd(jj + 1)->SetLogy(true);
            gPad->SetLogy(true);
      if (h) if (h->GetFunction("lorentzian"))h->GetFunction("lorentzian")->ResetBit(kNotDraw);
      if (h)h->Draw();
   }
   c1->SaveAs(Form("plots/%d/cerTDCs.png", fRunNumber));
   c1->SaveAs(Form("plots/%d/cerTDCs.ps", fRunNumber));
   delete c1;

   c1  = new TCanvas("TDCAnalysisCanvas", "Cherenkov Timing");
   c1->Divide(4, 3);
   for (int jj = 0; jj < 12; jj++) {
      h = cer_tdc_hist2[jj];
      c1->cd(jj + 1)->SetLogy(true);
            gPad->SetLogy(true);
      /*    if(h) if(h->GetFunction("lorentzian"))h->GetFunction("lorentzian")->ResetBit(kNotDraw);*/
      if (h)h->Draw();
   }
   c1->SaveAs(Form("plots/%d/cerTDCs2ndPeak.png", fRunNumber));
   c1->SaveAs(Form("plots/%d/cerTDCs2ndPeak.ps", fRunNumber));
   delete c1;

   c1  = new TCanvas("TDCAnalysisCanvas", "Hodoscope Timing");
   c1->Divide(10, 6);
   for (int jj = 0; jj < 56; jj++) {
      h = (TH1F*)(*fLuciteTdcHists)[jj];
      c1->cd(jj + 1)->SetLogy(true);
            gPad->SetLogy(true);
      if (h) if (h->GetFunction("fc"))h->GetFunction("fc")->ResetBit(kNotDraw);
      if (h)h->Draw();
   }
   c1->SaveAs(Form("plots/%d/lucTDCs.png", fRunNumber));
   c1->SaveAs(Form("plots/%d/lucTDCs.ps", fRunNumber));
   delete c1;


   c1  = new TCanvas("TDCAnalysisCanvas", "Hodoscope Timing Sums");
   c1->Divide(7, 4);
   for (int jj = 0; jj < 28; jj++) {
      h = (TH1F*)(*fLuciteTdcSumHists)[jj];
      c1->cd(jj + 1)->SetLogy(true);
            gPad->SetLogy(true);
      if (h) if (h->GetFunction("fc"))h->GetFunction("fc")->ResetBit(kNotDraw);
      if (h)h->Draw();
   }
   c1->SaveAs(Form("plots/%d/lucTDCSums.png", fRunNumber));
   c1->SaveAs(Form("plots/%d/lucTDCSums.ps", fRunNumber));
   delete c1;


   c1  = new TCanvas("TDCAnalysisCanvas", "Hodoscope Timing Differences");
   c1->Divide(7, 4);
   for (int jj = 0; jj < 28; jj++) {
      h = (TH1F*)(*fLuciteTdcDiffHists)[jj];
      c1->cd(jj + 1)->SetLogy(true);
            gPad->SetLogy(true);
      if (h) if (h->GetFunction("fc"))h->GetFunction("fc")->ResetBit(kNotDraw);
      if (h)h->Draw();
   }
   c1->SaveAs(Form("plots/%d/lucTDCDiffs.png", fRunNumber));
   c1->SaveAs(Form("plots/%d/lucTDCDiffs.ps", fRunNumber));
   delete c1;

   c1  = new TCanvas("TDCAnalysisCanvas", "Tracker Timing");
   c1->Divide(9, 8);
   for (int jj = 0; jj < 64/*+2*132*/; jj++) {
      h = (TH1F*)(*fTrackerTdcHists)[jj];
      if(h) if(h->GetEntries() < 1000) continue;

      c1->cd(jj + 1);
            gPad->SetLogy(true);
//    if(jj%8==0)c1->cd(jj/8+1);
      if (h) if (h->GetFunction("fc"))h->GetFunction("fc")->ResetBit(kNotDraw);
      if (h)h->Draw();
//    if(jj%8==0) {if(h)h->Draw();}
//    else  {if(h)h->Draw("same");}
   }
   c1->SaveAs(Form("plots/%d/tracker0TDCs.png", fRunNumber));
   c1->SaveAs(Form("plots/%d/tracker0TDCs.ps", fRunNumber));
   delete c1;
   c1  = new TCanvas("TDCAnalysisCanvas", "Tracker Timing");
   c1->Divide(11, 12);
   for (int jj = 64 ; jj < 64 + 56; jj++) {
      h = (TH1F*)(*fTrackerTdcHists)[jj];
      if(h) if(h->GetEntries() < 1000) continue;
      c1->cd(jj - 56 + 1);
            gPad->SetLogy(true);
      if (h) if (h->GetFunction("fc"))h->GetFunction("fc")->ResetBit(kNotDraw);
      if (h)h->Draw();
   }
   c1->SaveAs(Form("plots/%d/tracker1TDCs.png", fRunNumber));
   c1->SaveAs(Form("plots/%d/tracker1TDCs.ps", fRunNumber));
   delete c1;
   c1  = new TCanvas("TDCAnalysisCanvas", "Tracker Timing");
   c1->Divide(11, 12);
   for (int jj = 64 + 128; jj < 64 + 2 * 128; jj++) {
      h = (TH1F*)(*fTrackerTdcHists)[jj];
      if(h) if(h->GetEntries() < 1000) continue;

      c1->cd(jj - 64 - 128 + 1);
            gPad->SetLogy(true);
      if (h) if (h->GetFunction("fc"))h->GetFunction("fc")->ResetBit(kNotDraw);
      if (h)h->Draw();
   }
   c1->SaveAs(Form("plots/%d/tracker2TDCs.png", fRunNumber));
   c1->SaveAs(Form("plots/%d/tracker2TDCs.ps", fRunNumber));
   delete c1;

   for (int ii = 0; ii < 7; ii++) {
      c1 = new TCanvas("TDCAnalysisCanvas", Form("Bigcal Timing - Row %d", ii));
      c1->Divide(4, 8);
      for (int kk = 0; kk < 8; kk++)
         for (int jj = 0; jj < 4; jj++) {
            h = (TH1F*)(*fBigcal32TdcHists)[jj+kk*4+32*ii];
            c1->cd(jj + kk * 4 + 1);
            gPad->SetLogy(true);
            if (h)if (h->GetFunction("gaussian")) h->GetFunction("gaussian")->ResetBit(kNotDraw);
            if (h)h->Draw();
         }
      c1->SaveAs(Form("plots/%d/bigcalTDCsRow%d.png", fRunNumber, ii + 1));
      c1->SaveAs(Form("plots/%d/bigcalTDCsRow%d.ps", fRunNumber, ii + 1));
      delete c1;
   }

   //-----------
   c1 = new TCanvas("TDCAnalysisCanvas", Form("Bigcal Timing "));
   c1->Divide(5, 4);
   for (int kk = 0; kk < 4; kk++)
      for (int jj = 0; jj < 5; jj++) {
         if( jj+5*kk < 19 ) {
            h = (TH1F*)(*fBigcalTdcHists)[jj+5*kk];
            c1->cd(jj + kk * 5 + 1);
            gPad->SetLogy(true);
            if (h)if (h->GetFunction("gaussian")) h->GetFunction("gaussian")->ResetBit(kNotDraw);
            if (h)h->Draw();
         }
      }
   c1->SaveAs(Form("plots/%d/bigcalTDCs%d.png", fRunNumber, 0));
   c1->SaveAs(Form("plots/%d/bigcalTDCs%d.ps", fRunNumber, 0));
   delete c1;
   c1 = new TCanvas("TDCAnalysisCanvas", Form("Bigcal Timing "));
   c1->Divide(5, 4);
   for (int kk = 0; kk < 4; kk++)
      for (int jj = 0; jj < 5; jj++) {
         if( 19 + jj+5*kk < 38 ) {
            h = (TH1F*)(*fBigcalTdcHists)[19 + jj+5*kk];
            c1->cd(jj + kk * 5 + 1);
            gPad->SetLogy(true);
            if (h)if (h->GetFunction("gaussian")) h->GetFunction("gaussian")->ResetBit(kNotDraw);
            if (h)h->Draw();
         }
      }
   c1->SaveAs(Form("plots/%d/bigcalTDCs%d.png", fRunNumber, 1));
   c1->SaveAs(Form("plots/%d/bigcalTDCs%d.ps", fRunNumber, 1));
   delete c1;

   return(0);
}
//_____________________________________________________________________________

Int_t BETATimingAnalysis::FillDatabase() {

   InSANEDatabaseManager * dbManager = InSANEDatabaseManager::GetManager() ;

   std::cout << " o Filling database with Cherenkov Timings" << std::endl;
   for (int jj = 0; jj < 12; jj++) {
      dbManager->ExecuteInsert(((InSANEDetectorTiming*)(*fCherenkovTiming)[jj])->GetSQLInsertString("cherenkov", fRunNumber)) ;
   }

   std::cout << " o Filling database with hodoscope Timings" << std::endl;
   for (int jj = 0; jj < 56; jj++) {
      dbManager->ExecuteInsert(((InSANEDetectorTiming*)(*fLuciteTiming)[jj])->GetSQLInsertString("hodoscope", fRunNumber)) ;
   }

   std::cout << " o Filling database with hodoscopeSums Timings" << std::endl;
   for (int jj = 0; jj < 28; jj++) {
      dbManager->ExecuteInsert(((InSANEDetectorTiming*)(*fLuciteSumTiming)[jj])->GetSQLInsertString("hodoscopeSums", fRunNumber)) ;
   }

   std::cout << " o Filling database with hodoscopeDiffs Timings" << std::endl;
   for (int jj = 0; jj < 28; jj++) {
      dbManager->ExecuteInsert(((InSANEDetectorTiming*)(*fLuciteDiffTiming)[jj])->GetSQLInsertString("hodoscopeDiffs", fRunNumber)) ;
   }

   std::cout << " o Filling database with hodoscopeDiffs Timings" << std::endl;
   for (int jj = 0; jj < 224; jj++) {
      dbManager->ExecuteInsert(((InSANEDetectorTiming*)(*fBigcalTiming)[jj])->GetSQLInsertString("bigcalTiming", fRunNumber)) ;
   }

   std::cout << " o Filling database with bigcalTrigger Timings" << std::endl;
   for (int jj = 0; jj < 38; jj++) {
      dbManager->ExecuteInsert(((InSANEDetectorTiming*)(*fBigcalTriggerTiming)[jj])->GetSQLInsertString("bigcalTrigger", fRunNumber)) ;
   }

   std::cout << " o Filling database with Cherenkov Timings" << std::endl;
   for (int jj = 0; jj < 128 * 2 + 64; jj++) {
      auto *h = (TH1F*)(*fTrackerTdcHists)[jj];
      //if(h->GetEntries() > 1000) 
      dbManager->ExecuteInsert(((InSANEDetectorTiming*)(*fTrackerTiming)[jj])->GetSQLInsertString("tracker", fRunNumber)) ;
   }
   dbManager->CloseConnections();
   return(0);
}
//_____________________________________________________________________________

