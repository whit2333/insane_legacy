#define ForwardTrackerEvent_cxx
#include  "ForwardTrackerEvent.h"
#include <TClonesArray.h>
#include <iostream>
#include  "ForwardTrackerHit.h"
#include "InSANERunManager.h"
#include "InSANEHitPosition.h"

ClassImp(ForwardTrackerEvent)

TClonesArray * ForwardTrackerEvent::fgForwardTrackerHits = nullptr;
TClonesArray * ForwardTrackerEvent::fgForwardTrackerPositionHits = nullptr;
TClonesArray * ForwardTrackerEvent::fgForwardTracker2PositionHits = nullptr;
TClonesArray * ForwardTrackerEvent::fgForwardTrackerY1PositionHits = nullptr;
TClonesArray * ForwardTrackerEvent::fgForwardTrackerY2PositionHits = nullptr;

ForwardTrackerEvent::ForwardTrackerEvent()
{

   if (InSANERunManager::GetRunManager()->fVerbosity > 3) printf("  + new ForwardTrackerEvent\n");

   if (!fgForwardTrackerHits) {
      fgForwardTrackerHits = new TClonesArray("ForwardTrackerHit", 100);
   }
   fTrackerHits = fgForwardTrackerHits;

   if (!fgForwardTrackerPositionHits) {
      fgForwardTrackerPositionHits = new TClonesArray("ForwardTrackerPositionHit", 1);
   }
   fTrackerPositionHits = fgForwardTrackerPositionHits;// new TClonesArray("ForwardTrackerPositionHit",1);

   if (!fgForwardTracker2PositionHits) {
      fgForwardTracker2PositionHits = new TClonesArray("ForwardTrackerPositionHit", 1);
   }
   fTracker2PositionHits = fgForwardTracker2PositionHits;// new TClonesArray("ForwardTrackerPositionHit",1);

   if (!fgForwardTrackerY1PositionHits) {
      fgForwardTrackerY1PositionHits = new TClonesArray("ForwardTrackerPositionHit", 1);
   }
   fTrackerY1PositionHits = fgForwardTrackerY1PositionHits;// new TClonesArray("ForwardTrackerPositionHit",1);

   if (!fgForwardTrackerY2PositionHits) {
      fgForwardTrackerY2PositionHits = new TClonesArray("ForwardTrackerPositionHit", 1);
   }
   fTrackerY2PositionHits = fgForwardTrackerY2PositionHits;// new TClonesArray("ForwardTrackerPositionHit",1);
}

//______________________________________________________________________________
ForwardTrackerEvent::~ForwardTrackerEvent()
{
   /*
      if(fTrackerHits) delete fTrackerHits;
      fgForwardTrackerHits = 0;
      fTrackerHits = 0;
      if(fTrackerPositionHits) delete fTrackerPositionHits;
      fgForwardTrackerPositionHits=0;
      fTrackerPositionHits = 0;
   */
}

Int_t ForwardTrackerEvent::AllocateHitArray()
{
   if (!fgForwardTrackerHits) {
      fgForwardTrackerHits = new TClonesArray("ForwardTrackerHit", 20);
   }
   fTrackerHits = fgForwardTrackerHits;

   if (!fgForwardTrackerPositionHits) {
      fgForwardTrackerPositionHits = new TClonesArray("ForwardTrackerPositionHit", 1);
   }
   fTrackerPositionHits = fgForwardTrackerPositionHits;// new TClonesArray("ForwardTrackerPositionHit",1);

   if (!fgForwardTracker2PositionHits) {
      fgForwardTracker2PositionHits = new TClonesArray("ForwardTrackerPositionHit", 1);
   }
   fTracker2PositionHits = fgForwardTracker2PositionHits;// new TClonesArray("ForwardTrackerPositionHit",1);

   if (!fgForwardTrackerY1PositionHits) {
      fgForwardTrackerY1PositionHits = new TClonesArray("ForwardTrackerPositionHit", 1);
   }
   fTrackerY1PositionHits = fgForwardTrackerY1PositionHits;// new TClonesArray("ForwardTrackerPositionHit",1);

   if (!fgForwardTrackerY2PositionHits) {
      fgForwardTrackerY2PositionHits = new TClonesArray("ForwardTrackerPositionHit", 1);
   }
   fTrackerY2PositionHits = fgForwardTrackerY2PositionHits;// new TClonesArray("ForwardTrackerPositionHit",1);

   return(0);
}
//______________________________________________________________________________
void ForwardTrackerEvent::ClearEvent(Option_t * opt) {
//   if(fTrackerHits) std::cout << " o fTrackerHits->GetSize() = " << fTrackerHits->GetSize() << " ";
//   if(fTrackerPositionHits) std::cout << " , fTrackerPositionHits->GetSize() = " << fTrackerPositionHits->GetSize() << "\n";
   if (fTrackerHits)if(fTrackerHits->GetEntries() > 650 ) {
       std::cout << " calling  fTrackerHits->ExpandCreate(500); " << fTrackerHits->GetEntries() << " entries" << std::endl;
       fTrackerHits->ExpandCreate(500);
   }
   if (fTrackerHits) fTrackerHits->Clear(opt);

   if (fTrackerPositionHits)if(fTrackerPositionHits->GetEntries() > 650 ) {
       std::cout << " calling  fTrackerPositionHits->ExpandCreate(500); " << fTrackerPositionHits->GetEntries() << " entries" << std::endl;
       fTrackerPositionHits->ExpandCreate(500);
   }
   if (fTrackerPositionHits)fTrackerPositionHits->Clear(opt);

   if (fTracker2PositionHits)if(fTracker2PositionHits->GetEntries() > 500 ) {
       std::cout << " calling  fTracker2PositionHits->ExpandCreate(301); " << fTracker2PositionHits->GetEntries() << " entries" << std::endl;
       fTracker2PositionHits->ExpandCreate(300);
   }
   if (fTracker2PositionHits)fTracker2PositionHits->Clear(opt);

   if (fTrackerY1PositionHits)if(fTrackerY1PositionHits->GetEntries() > 500 ) {
       std::cout << " calling  fTrackerY1PositionHits->ExpandCreate(302); "  << fTrackerY1PositionHits->GetEntries() << " entries" << std::endl;
       fTrackerY1PositionHits->ExpandCreate(300);
   }
   if (fTrackerY1PositionHits)fTrackerY1PositionHits->Clear(opt);

   if (fTrackerY2PositionHits)if(fTrackerY2PositionHits->GetEntries() > 500 ) {
       std::cout << " calling  fTrackerY2PositionHits->ExpandCreate(303);  "  << fTrackerY2PositionHits->GetEntries() << " entries" << std::endl;
       fTrackerY2PositionHits->ExpandCreate(300);
   }
   if (fTrackerY2PositionHits)fTrackerY2PositionHits->Clear(opt);
   fNumberOfHits = 0;
   fNumberOfTDCHits = 0;
   fNumberOfTimedTDCHits = 0;
   fNumberOfTimedPositionHits = 0;
   fNumberOfTimed2PositionHits = 0;
   fGoodEvent = kFALSE;
}


