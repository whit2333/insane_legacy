#include "KMeansClusterProcessor.h"
#include "BigcalEvent.h"
ClassImp(KMeansClusterProcessor)

//___________________________________________________________________

KMeansClusterProcessor::KMeansClusterProcessor()
{
   fNClusters = 0;
   fMinimumEnergy = 200.0;
   fGeoCalc = BIGCALGeometryCalculator::GetCalculator();
   fPartitioning = new InSANEClusterPartitioning();
}
//___________________________________________________________________

KMeansClusterProcessor::~KMeansClusterProcessor()
{

}
//___________________________________________________________________

Int_t KMeansClusterProcessor::FindPeaks()
{

   Int_t peaksfound = 0;
   Int_t xpos, ypos, zpos;
   fQuit = false;
   fiPeaks->clear();
   fjPeaks->clear();
   fEPeaks->clear();
   // copy the source histogram. This copy will be modified
   fSourceCopy = (TH2F *) fSourceHistogram->Clone("clusterSourceCopy");
   /*  std::cout << " Looking for " << fNPartitions << " partitions\n";*/
   fSourceCopy->GetMaximumBin(xpos, ypos, zpos);
   //Double_t firstMax = fSourceHistogram->GetMaximum();
   Double_t tempMax;// = fSourceHistogram->GetMaximum();
   while (!fQuit) {
      fSourceCopy->GetMaximumBin(xpos, ypos, zpos);
//    cout << "peak " << xpos << " " << ypos << " \n";
      tempMax = fSourceCopy->GetBinContent(xpos, ypos);
      if (/*tempMax > fMinimumEnergy  peaksfound<fMaxNClusters && */peaksfound < fNPartitions) {
         fiPeaks->push_back((double)fSourceCopy->GetXaxis()->GetBinCenter(xpos)) ;
         fjPeaks->push_back((double)fSourceCopy->GetYaxis()->GetBinCenter(ypos)) ;
         fEPeaks->push_back((double)tempMax) ;
         peaksfound++;
         Int_t offset = -2;
         fSourceCopy->SetMaximum(tempMax);
         for (int n = offset ; n < 5 + offset ; n++)
            for (int m = offset ; m < 5 + offset ; m++)
               if (((ypos + n <= 56 && ypos + n > 32) && (xpos + m <= 30 && xpos + m > 0))/*RCS*/   ||
                   ((ypos + n <= 32 && ypos + n > 0) && (xpos + m <= 32 && xpos + m > 0))/*PROT*/)
                  fSourceCopy->SetBinContent(xpos + m, ypos + n, 0.0);
//      this->Review();
      } else {
         fQuit = true;
      }
   }
   delete fSourceCopy;
   return(peaksfound);
}
//___________________________________________________________________


Int_t KMeansClusterProcessor::Partition()
{
   BigcalHit * ahit = nullptr;
   Int_t iPart;
   TClonesArray * bchits = ((BigcalEvent*)fEvent)->fBigcalHits;
// Rule of thumb!
   fkMeans = (Int_t)TMath::Floor(TMath::Sqrt(fSourceHistogram->GetEntries() / 2.0));
   if (fkMeans > fNPeaks) {
      fkMeans = fNPeaks;
      std::cout << "  KMeansClusterProcessor::Partition() - Reducing kMeans to number of peaks.  " << fkMeans << "\n";
   }

// First create centroids from the kmeans
   for (int i = 0; i < fkMeans; i++) {
      fPartitioning->AddCentroid(fiPeaks->at(i), fjPeaks->at(i), fEPeaks->at(i));
   }
   Int_t numberOfDataPoints = 0;
// Then add datum checking the distance against each centroid
   for (Int_t i = 0; i < bchits->GetEntries(); i++) {
      ahit = (BigcalHit*)(*bchits)[i];
      if (ahit->fLevel == 0) {
         fPartitioning->AddDatum(ahit->fiCell, ahit->fjCell, ahit->fEnergy);
         if (((iPart = fPartitioning->GetPartition(numberOfDataPoints)) != -1))
            fPartitioning->SetDatumPartitionNumber(numberOfDataPoints, iPart);
         numberOfDataPoints++;
      }
   }
   return(fkMeans);
}
//___________________________________________________________________


Int_t KMeansClusterProcessor::Cluster()
{
   Int_t fNumberOfIterations = 5;
   for (int i = 0; i < fNumberOfIterations; i++) {
      fPartitioning->CalculateCentroids();
      if (fPartitioning->Repartition() == 0) {
         std::cout << "  KMeansClusterProcessor::Cluster() - No change in partitions after " << i << " iterations\n";
         break;
      }

   }
   for (int kk = 0; kk < fkMeans; kk++)
      new((*fClusters)[kk]) BIGCALCluster();

   return(fkMeans);
}
//___________________________________________________________________


Int_t  KMeansClusterProcessor::CalculateQuantities(BIGCALCluster * acluster, Int_t k)
{
   BIGCALGeometryCalculator *bcgeo = BIGCALGeometryCalculator::GetCalculator();
   Int_t m, n, i, j;
   i = (Int_t)TMath::Floor((Double_t)(*fiPeaks)[k]);
   j = (Int_t)TMath::Floor((Double_t)(*fjPeaks)[k]);
   acluster->SetClusterPeak(i, j);
//cout << "cords : " << bcgeo->getBlockXij_BCCoords(i,j) << " , " << bcgeo->getBlockYij_BCCoords(i,j) << "\n" ;
//cout << "total energy " << totalE << "\n";
/// The cluster is selected to be a 5x5 grid centered on the peaks
/// The following loops over all the blocks and adds
   Int_t offset = -2;
   for (n = offset ; n < 5 + offset ; n++) {
      for (m = offset ; m < 5 + offset ; m++) {
         if (((j + n <= 56 && j + n > 32) && (i + m <= 30 && i + m > 0))/*RCS*/   ||
             ((j + n <= 32 && j + n > 0) && (i + m <= 32 && i + m > 0))/*PROT*/) {
            acluster->fBlockEnergies[m-offset][n-offset] = fSourceHistogram->GetBinContent(i + m, j + n);
//      cluster->SetBinContent(i+m,j+n,block_energy[m+2][n+2] );
// Calculate the mean positions (first moments)
            acluster->fXMoment += acluster->fBlockEnergies[m-offset][n-offset] * bcgeo->GetBlockXij_BCCoords(i + m, j + n);
            acluster->fYMoment += acluster->fBlockEnergies[m-offset][n-offset] * bcgeo->GetBlockYij_BCCoords(i + m, j + n);
            acluster->fTotalE += acluster->fBlockEnergies[m-offset][n-offset];
         }
      }
   }
   acluster->fXMoment = acluster->fXMoment / (acluster->fTotalE);
   acluster->fYMoment = acluster->fYMoment / (acluster->fTotalE);
// Now we can calculate the central moments of interest (variance, skewness, and higher...)
   for (n = offset ; n < 5 + offset ; n++) {
      for (m = offset ; m < 5 + offset ; m++) {
         if (((j + n <= 56 && j + n > 32) && (i + m <= 30 && i + m > 0))/*RCS*/ || ((j + n <= 32 && j + n > 0) && (i + m <= 32 && i + m > 0))/*PROT*/)   {
            acluster->fX2Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockXij_BCCoords(i + m, j + n) - acluster->fXMoment , 2.0);
            acluster->fY2Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockYij_BCCoords(i + m, j + n) - acluster->fYMoment , 2.0);
            acluster->fX3Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockXij_BCCoords(i + m, j + n) - acluster->fXMoment , 3.0);
            acluster->fY3Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockYij_BCCoords(i + m, j + n) - acluster->fYMoment , 3.0);
         }
      }
   }
   acluster->fX2Moment = acluster->fX2Moment / (acluster->fTotalE); // second moment is the variance (sigma^2)
   acluster->fY2Moment = acluster->fY2Moment / (acluster->fTotalE);
   acluster->fX3Moment = acluster->fX3Moment / (acluster->fTotalE);
   acluster->fY3Moment = acluster->fY3Moment / (acluster->fTotalE);

   acluster->fXStdDeviation = TMath::Power(acluster->fX2Moment , 0.5);
   acluster->fYStdDeviation = TMath::Power(acluster->fY2Moment , 0.5);

   acluster->fXSkewness = acluster->fX3Moment * TMath::Power(1.0 / (acluster->fXStdDeviation) , 3.0);
   acluster->fYSkewness = acluster->fY3Moment * TMath::Power(1.0 / (acluster->fYStdDeviation) , 3.0);
//cout << "cords : " << bcgeo->getBlockXij_BCCoords(i,j) << " , " << bcgeo->getBlockYij_BCCoords(i,j) << "\n" ;
//cout << "total energy " << totalE << "\n";

   return(0);

}
//_______________________________________________________//


TH2F * KMeansClusterProcessor::GetSourceHistogram()
{
   if (!fSourceHistogram)
      fSourceHistogram = new TH2F("bigcalClusterSource", "Bigcal Cluster Sources", 32, 1, 33, 56, 1, 57);
   if ((!fEvent)) {
      std::cout << " Warning fEvent is NULL!c\n ";
      std::cout << " returning null pointer \n";
      return(fSourceHistogram);
   }

   fSourceHistogram->Reset();
   BigcalHit * ahit = nullptr;
   TClonesArray * bchits = ((BigcalEvent*)fEvent)->fBigcalHits;
//cout << " hits " << fBigcalEvent->fNumberOfHits;

   for (Int_t i = 0; i < bchits->GetEntries(); i++) {
      ahit = (BigcalHit*)(*bchits)[i];
      if (ahit->fTDCLevel == -1) {

         fSourceHistogram->Fill(ahit->fiCell,
                                ahit->fjCell,
                                ahit->fEnergy);
      }
   }
   return(fSourceHistogram);
}
//___________________________________________________________________
