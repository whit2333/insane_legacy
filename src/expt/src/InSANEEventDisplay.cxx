#include "InSANEEventDisplay.h"
#include "TPaveLabel.h"
#include "TEllipse.h"
#include "TBox.h"
#include "TPave.h"
#include "TPaveText.h"
#include "TPolyLine.h"
#include "TLine.h"
#include "TArrow.h"
#include "TEveManager.h"
#include "TGeoNode.h"
#include "TGLViewer.h"
#include "TGeoManager.h"
#include "TEveGeoNode.h"
#include "TButton.h"
#include "SANERunManager.h"


//_______________________________________________________//
/*
InSANEEventDisplay * InSANEEventDisplay::GetEventDisplay() {
 // if(!fgInSANEEventDisplay) fgInSANEEventDisplay = new InSANEEventDisplay();
  return(new InSANEEventDisplay(););
}*/
//_______________________________________________________//

InSANEEventDisplay::InSANEEventDisplay()
   : fWaitPrimitive(1)
{

   fUpdateTriggerBit = 3; // b2trig
   fEventType = 5; //b2trig,b1trig, and pi0trig
   fTriggerEvent = nullptr;

   fTextDisplay = new std::vector<InSANEEventDatum*>;
   fTextDisplay->clear();

   fWebDisplay = 0;

   canvasWidth = 800.;
   canvasHeight = 800.;
   botx = 50.;
   boty = 50.;

   fRun = InSANERunManager::GetRunManager()->GetCurrentRun();

   fDataHists = new TObjArray(0);
   fSimulationHists = new TObjArray(0);

   fCanvas = new TCanvas("EventDisplay", "Event Display", (Int_t)canvasWidth, (Int_t)canvasHeight);
   fCanvas->Range(0, 0, 1, 1);

   fCanvas->Divide(1, 4);
   fCanvas->cd(1);
   fCanvas->cd(1)->Divide(3, 1);

   fCanvas->cd(1)->cd(1);
   pel =  new TPaveLabel(0.1, 0.8, 0.9, 0.95, Form("Run %d - Event Display", fRun->fRunNumber));
   pel->SetFillColor(38);
   pel->Draw();

   auto * button = new TButton("", ".x tutorials/graphs/graph.C", 0.15, 0.15, 0.85, 0.38);
   button->SetFillColor(42);
   button->Draw();
   button->SetEditable(kTRUE);

   fCanvas->cd(1)->cd(2);
   fEventSummary  = new TPaveText(.05, .1, .95, .8);
   fEventSummary->AddText(Form("Run # %d", fRun->fRunNumber));


   fCanvas->cd(2)->Divide(6, 1);

   fCanvas->cd(3)->Divide(6, 1);
   canvasWidth = 800.;
   canvasHeight = 300.;

//   return TPaveLabel;
   fCanvas->cd(4)->Divide(8, 1);

// dummy cd TSpectrum
   fCanvas->cd(1)->cd(3);

}
//_______________________________________________________//

InSANEEventDisplay::~InSANEEventDisplay()
{

   fTextDisplay->clear();
   delete fTextDisplay;

}
//_______________________________________________________//

void InSANEEventDisplay::BuildBETA()
{

   Double_t x[7] = {(botx + 25 + 120) / canvasWidth, (botx + 25 + 120 + 60) / canvasWidth, (botx + 25 + 120 + 60 + 100) / canvasWidth,
                    (botx + 25 + 120 + 60 + 100) / canvasWidth, (botx + 25 + 120 + 60) / canvasWidth, (botx + 25 + 120) / canvasWidth
                    , (botx + 25 + 120) / canvasWidth
                   };
   Double_t y[7] = {(boty + 56 * 3) / canvasHeight, (boty + 56 * 3) / canvasHeight, (boty + 56 * 2) / canvasHeight, (boty + 56 * 1) / canvasHeight,
                    (boty + 56 * 0) / canvasHeight, (boty + 56 * 0) / canvasHeight, (boty + 56 * 3) / canvasHeight
                   };
   cherenkov = new TPolyLine(7, x, y);
   cherenkov->SetFillColor(38);
   cherenkov->SetLineColor(2);
   cherenkov->SetLineWidth(4);
   cherenkov->Draw("f");
   cherenkov->Draw();

   trackerbox = new TBox((botx + 25 + 120 + 60 + 100 + 15) / canvasWidth, (boty + 56) / canvasHeight, (botx + 25 + 120 + 60 + 100 + 15 + 15) / canvasWidth, (boty + 2.*56.) / canvasHeight);
   trackerbox->SetFillColor(42);
   bigcalbox = new TBox(botx / canvasWidth, boty / canvasHeight, (botx + 25) / canvasWidth, (boty + 56 * 3) / canvasHeight);
   bigcalbox->SetFillColor(43);
   Double_t x0 = (botx + 25. + 120. + 60. + 100. + 15. + 15.) / canvasWidth + 60. / canvasWidth; //x0
   Double_t y0 = ((boty + 56. / 2.0) / canvasHeight) + 60 / canvasHeight; //x0
   lucitebox = new TBox((botx + 25 + 60) / canvasWidth,        boty / canvasHeight,
                        (botx + 25 + 60 + 25) / canvasWidth, (boty + 56 * 3) / canvasHeight);
   lucitebox->SetFillColor(44);
   targ =  new TEllipse(x0, y0, 55. / canvasWidth, 55. / canvasHeight);

/// tan(40deg)*200 ~ 167
   beam = new TLine(x0 + 0.8 * 200.0 / canvasWidth ,
                    y0 - 0.8 * 167.0 / canvasHeight , x0 - 0.8 * 200. / canvasWidth, y0 + 0.8 * 167.0 / canvasHeight);
   beam->SetLineWidth(2);

/// tan(40deg)*200 ~ 167
   targPara = new TArrow(x0 - 45. / canvasWidth, y0 + 38.0 / canvasHeight, x0 + 45.0 / canvasWidth ,
                         y0 - 38.0 / canvasHeight);
   targPerp = new TArrow(x0 + 45. / canvasWidth, y0 + 38.0 / canvasHeight, x0 - 45.0 / canvasWidth ,
                         y0 - 38.0 / canvasHeight);

   targPara->SetLineWidth(7);
   targPerp->SetLineWidth(7);


   targ->Draw();
   beam->Draw();
   if (fRun->fTargetField > 5.0) {
      if (fRun->fTargetAngle > 170.0 && fRun->fTargetAngle < 190.0)   targPara->Draw();
      if (fRun->fTargetAngle > 70.0 && fRun->fTargetAngle < 90.0)  targPerp->Draw();
   }

   lucitebox->Draw();
   bigcalbox->Draw();
   trackerbox->Draw();
}

void InSANEEventDisplay::GenerateWebEvents(Int_t startEvent, Int_t numberOfEvents)
{

}
//_______________________________________________________//

void InSANEEventDisplay::OpenEveDisplay()
{

   TEveManager * teveMan = TEveManager::Create();

   teveMan->RegisterGeometryAlias("BETA", "/home/whit/sane08/InSANE/main/SANEGeometry.root");

   //TGeoManager * tMan = teveMan->GetGeometryByAlias("BETA");

   TGeoNode* node1 = gGeoManager->GetTopNode();
   auto* its = new TEveGeoTopNode(gGeoManager, node1);
   gEve->AddGlobalElement(its);

//    gGeoManager = gEve->GetGeometryByAlias("ATLAS");
//
//    TGeoNode* node2 = gGeoManager->GetTopVolume()->FindNode("OUTE_1");
//    TEveGeoTopNode* atlas = new TEveGeoTopNode(gGeoManager, node2);
//    gEve->AddGlobalElement(atlas);

   gEve->FullRedraw3D(kTRUE);

   // EClipType not exported to CINT (see TGLUtil.h):
   // 0 - no clip, 1 - clip plane, 2 - clip box
   TGLViewer *v = gEve->GetDefaultGLViewer();
   //v->GetClipSet()->SetClipType(1);
   v->RefreshPadEditor(v);

   v->CurrentCamera().RotateRad(-0.5, -2.4);
   v->DoDraw();
}
//_______________________________________________________//

void InSANEEventDisplay::UpdateDisplay()
{

   if (fTriggerEvent) if (fTriggerEvent->fTriggerBits[fUpdateTriggerBit] /*&& fTriggerEvent->fCodaType == fEventType*/) {
         std::cout << "fCodaType  is " << fTriggerEvent->fCodaType << " \n";

         TH1 * ahist;
         int i = 0;
         fEventSummary->Clear();
         fCanvas->cd(1)->cd(2);
         /*    fEventSummary->AddText("#frac{2s}{#pi#alpha^{2}}  #frac{d#sigma}{dcos#theta} \
             (e^{+}e^{-} #rightarrow f#bar{f} ) = #left| #frac{1}{1 - #Delta#alpha} \
             #right|^{2} (1+cos^{2}#theta)");
         */
         /*    fEventSummary->AddText("+ 4 Re #left{ #frac{2}{1 - #Delta#alpha} #chi(s) \
             #[]{#hat{g}_{#nu}^{e}#hat{g}_{#nu}^{f} \
          (1 + cos^{2}#theta) + 2 #hat{g}_{a}^{e}#hat{g}_{a}^{f} cos#theta) } #right}");
         */

         /*    fEventSummary->AddText("+ 16#left|#chi(s)#right|^{2}\
           #left[(#hat{g}_{a}^{e}^{2} + #hat{g}_{v}^{e}^{2})\
           (#hat{g}_{a}^{f}^{2} + #hat{g}_{v}^{f}^{2})(1+cos^{2}#theta)\
           + 8 #hat{g}_{a}^{e} #hat{g}_{a}^{f} #hat{g}_{v}^{e} \
           #hat{g}_{v}^{f}cos#theta#right] ");
         */
         fEventSummary->SetLabel(" Event");

         for (auto it = fTextDisplay->begin(); it != fTextDisplay->end(); ++it) {
            //fEventSummary->AddText(Form("%20s %s ",(*it)->fName.Data(), (*it)->fEvent->Print() ) );
         }
         fCanvas->cd(1)->cd(2);
         fEventSummary->Draw();

         fCanvas->cd(2);
         TIter simIter(fSimulationHists);
         while ((ahist = (TH1 *)simIter.Next())) {
            i++;
            fCanvas->cd(2)->cd(i);
            ahist->Draw("colz");
         }

         fCanvas->cd(3);
// fCanvas->cd(4)->Divide(6,1);
         fCanvas->cd(4);

         i = 0;
         TIter myiter(fDataHists);
         while ((ahist = (TH1 *)myiter.Next())) {
            i++;
            fCanvas->cd(4)->cd(i);
            ahist->Draw("colz");
         }

         fCanvas->Update();

         if (fWebDisplay == 0) {
            if (fWaitPrimitive) gPad->WaitPrimitive();
         } else {
            while (fWebDisplay != 0) {
               fCanvas->SaveAs(Form("plots/eventdisplay/event%d.png", fWebDisplay));
               fWebDisplay--;
            }
         }
      }
}
//_______________________________________________________//

Int_t InSANEEventDisplay::Initialize(InSANERun * aRun)
{
   if (aRun) {

   }
   return(0);
}
//_______________________________________________________//



