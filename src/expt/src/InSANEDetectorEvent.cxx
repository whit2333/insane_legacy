#include  "InSANEDetectorEvent.h"

ClassImp(InSANEDetectorEvent)

//______________________________________________________________________________
InSANEDetectorEvent::InSANEDetectorEvent() {
   fGoodEvent    = false;
   fNumberOfHits = 0;
   ClearEvent();
}
//______________________________________________________________________________
InSANEDetectorEvent::~InSANEDetectorEvent() {
}
//______________________________________________________________________________
void InSANEDetectorEvent::Print(const Option_t * opt) const {
   InSANEEvent::Print(opt);
   std::cout << "  fGoodEvent: ";
   if (fGoodEvent) std::cout << "true , ";
   else std::cout << "false , ";
   std::cout << "NHits: " << fNumberOfHits << " , ";
}
//______________________________________________________________________________
void InSANEDetectorEvent::ClearEvent(Option_t * ) {
   fGoodEvent    = kFALSE;
   fNumberOfHits = 0;
   fEventNumber  = 0;
}
//______________________________________________________________________________

