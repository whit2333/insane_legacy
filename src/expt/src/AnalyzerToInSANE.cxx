#define AnalyzerToInSANE_cxx
#include "InSANERun.h"
#include "AnalyzerToInSANE.h"
#include <TH2.h>
#include <TStyle.h>
#include <iostream>
#include "SANEScalerEvent.h"
#include "SANERunManager.h"
#include "LuciteHodoscopePositionHit.h"
#include "ForwardTrackerGeometryCalculator.h"

void AnalyzerToInSANE::Begin(TTree * tree)
{

   //Int_t split = 99;
   //Int_t bsize = 64000;
   totalNumberOfEvents = 0;
   fChain = tree;
   TString option = GetOption(); // OPTION IS the run number in string form
   currentRunNumber = option.Atoi();

   //SANERunManager::GetRunManager()->SetRun(currentRunNumber);//fCurrentRun;
   fRun = InSANERunManager::GetRunManager()->GetCurrentRun();
   fRun->ClearCounters();

   fFileOut = SANERunManager::GetRunManager()->GetCurrentFile();
   //  fFileOut->Dump();
   fFileOut->cd();

//    std::cout << " Do the run numbers agree???? " << currentRunNumber << " == " << fRun->GetRunNumber() << "\n";

   fTreeOut = new TTree("betaDetectors", "The BETA Detector Data");

   fEvents = new SANEEvents("betaDetectors");

   //  fRun = new InSANERun( option.Atoi() );
   bcEvent = fEvents->BETA->fBigcalEvent;
   lhEvent = fEvents->BETA->fLuciteHodoscopeEvent;
   gcEvent = fEvents->BETA->fGasCherenkovEvent;
   ftEvent = fEvents->BETA->fForwardTrackerEvent;
   aBetaEvent = fEvents->BETA;
   trigEvent = fEvents->TRIG;
   //scalerEvent = fEvents->SCALER;
   beamEvent = fEvents->BEAM;
   hmsEvent = fEvents->HMS;
   mcEvent = fEvents->MC;

   // Useful objects
   bcgeo = BIGCALGeometryCalculator::GetCalculator();
   Int_t cal_series = bcgeo->GetLatestCalibrationSeries(currentRunNumber);
   bcgeo->SetCalibrationSeries(cal_series);
   fRun->fBigcalCalibrationSeries = cal_series;


   if (SANERunManager::GetRunManager()->GetVerbosity() > 0)    {
      std::cout << " - Processing run " << currentRunNumber << "\n";
      //gDirectory->pwd();
      std::cout << " - " << fChain->GetEntries() << " entries "
                << "in " << ((TChain *)fChain)->GetNtrees()
                << " trees/files\n" ;
   }
}
//_____________________________________________________________________________

void AnalyzerToInSANE::SlaveBegin(TTree * /*tree*/)
{

   TString option = GetOption();

}
//_____________________________________________________________________________

/**
 * Extract the beam data and the detector data
 *
 */
Bool_t AnalyzerToInSANE::Process(Long64_t entry)
{
//if(entry<100000 && fChain->GetTreeNumber()<=1) {
   if (true) {
      totalNumberOfEvents++;
//printf("testing 123\n");
      fChain->GetTree()->GetEntry(entry);
      //Int_t ii = 0;
      Int_t i = 0;
      Int_t j = 0;

      if (gen_event_type == 0) {
         std::cout << " Scaler Event ! \n";
      }
      if (gen_event_type == 131) {
         std::cout << " Epics Event ! \n";
      }
      if (gen_event_type == 132) {
         std::cout << " Epics Event ! \n";
      }

      fEvents->fRunNumber =  currentRunNumber;
      fEvents->fEventNumber =  gen_event_id_number;
      beamEvent->fRunNumber =  currentRunNumber;
      beamEvent->fEventNumber =  gen_event_id_number;
      trigEvent->fRunNumber =  currentRunNumber;
      trigEvent->fEventNumber =  gen_event_id_number;
      aBetaEvent->fRunNumber =  currentRunNumber;
      aBetaEvent->fEventNumber =  gen_event_id_number;
      hmsEvent->fRunNumber =  currentRunNumber;
      hmsEvent->fEventNumber =  gen_event_id_number;
      mcEvent->fRunNumber =  currentRunNumber;
      mcEvent->fEventNumber =  gen_event_id_number;

// Trigger

// std::cout << " event id " << gen_event_id_number << " \n";

      trigEvent->fCodaType             = gen_event_type;
      trigEvent->gen_event_type        = gen_event_type;
      trigEvent->gen_event_class       = gen_event_class;
      trigEvent->gen_event_roc_summary = gen_event_roc_summary;
      trigEvent->gen_event_sequence_n  = gen_event_sequence_n;

      for (i = 0; i < 12; i++) {
         trigEvent->fTriggerBits[i]       = gen_event_trigtype[i];
         if (gen_event_trigtype[i])          fRun->fTriggers[i]++;
      }

      fRun->fNEvents++;
      if (trigEvent->IsBETA2Event())       fRun->fNBeta2Events++;
      if (trigEvent->IsBETA1Event())       fRun->fNBeta1Events++;
      if (trigEvent->IsPedestalEvent())    fRun->fNPedEvents++;
      if (trigEvent->IsHMSEvent())         fRun->fNHmsEvents++;
      if (trigEvent->IsPi0Event())         fRun->fNPi0Events++;
      if (trigEvent->IsLEDPulserEvent())   fRun->fNLedEvents++;
      if (trigEvent->IsCoincidenceEvent()) fRun->fNCoinEvents++;


      trigEvent->polarea      = polarea;
      trigEvent->polarization = polarization;
      trigEvent->dtime_p      = dtime_p;
      trigEvent->dtime_n      = dtime_n;

      trigEvent->T_trghms      = T_trghms;
      trigEvent->T_trgbig      = T_trgbig;
      trigEvent->T_trgpi0      = T_trgpi0;
      trigEvent->T_trgbeta     = T_trgbeta;
      trigEvent->T_trgcoin1    = T_trgcoin1;
      trigEvent->T_trgcoin2    = T_trgcoin2;



// Beam Data
//   beamEvent->tcharge       = tcharge;
//   beamEvent->charge2s      = charge2s;
//   beamEvent->tcharge_help  = tcharge_help;
//   beamEvent->charge2s_help = charge2s_help;
//   beamEvent->tcharge_helm  = tcharge_helm;
//   beamEvent->charge2s_helm = charge2s_helm;
//   beamEvent->hel_p_scaler  = hel_p_scaler;
//   beamEvent->hel_n_scaler  = hel_n_scaler;
//   beamEvent->hel_p_trig    = hel_p_trig;
//   beamEvent->hel_n_trig    = hel_n_trig;


//   beamEvent->half_plate    = half_plate;
//   beamEvent->fHalfWavePlate    = half_plate;
//
//   beamEvent->rast_x        = rast_x;
//   beamEvent->rast_y        = rast_y;
      beamEvent->fXRaster        = rast_x;
      beamEvent->fYRaster        = rast_y;

//   beamEvent->slow_rast_x   = slow_rast_x;
//   beamEvent->slow_rast_y   = slow_rast_y;
      beamEvent->fXSlowRaster   = slow_rast_x;
      beamEvent->fYSlowRaster   = slow_rast_y;

      beamEvent->fXSlowRasterADC   = srast_x_adc;
      beamEvent->fYSlowRasterADC   = srast_y_adc;
      beamEvent->fXRasterADC   = frast_x_adc;
      beamEvent->fYRasterADC   = frast_y_adc;

//   beamEvent->sem_x         = sem_x;
//   beamEvent->sem_y         = sem_y;
      beamEvent->fXSem    = sem_x;
      beamEvent->fYSem    = sem_y;

//  beamEvent->i_helicity    = gbeam_helicity;
      beamEvent->fHelicity    = gbeam_helicity;
//   std::cout << " helicity = " << gbeam_helicity << "\n";

      aBetaEvent->bgid       = bgid;
      aBetaEvent->bgtype     = bgtype;
      aBetaEvent->btrigtype  = btrigtype;
      aBetaEvent->ngooda     = ngooda;
      aBetaEvent->ngoodt     = ngoodt;
      aBetaEvent->ngoodta    = ngoodta;
      aBetaEvent->ngoodtt    = ngoodtt;
      aBetaEvent->irowmax    = irowmax;
      aBetaEvent->icolmax    = icolmax;
      aBetaEvent->max_adc    = max_adc;

      // Forward tracker data
      ftEvent->ClearEvent("C");
      TClonesArray  &trackerHits = *(ftEvent->fTrackerHits);
      //trackerHits.SetOwner(kTRUE);
      ftEvent->fRunNumber =  currentRunNumber;
      ftEvent->fEventNumber =  gen_event_id_number;
      ForwardTrackerGeometryCalculator * ftGeoCalc =  ForwardTrackerGeometryCalculator::GetCalculator();

      ForwardTrackerHit *aFThit;

      for (i = 0; i < y1t_hit; i++) {
         aFThit = new(trackerHits[ftEvent->fNumberOfHits]) ForwardTrackerHit();
         aFThit->fHitNumber          = ftEvent->fNumberOfHits;
         ftEvent->fNumberOfTDCHits++;
         ftEvent->fNumberOfHits++;
         aFThit->fLevel              = 1;
         aFThit->fScintLayer         = 1;
         aFThit->fPositionCoordinate = 2;
         aFThit->fRow                = y1t_row[i];
         aFThit->fTDC                = y1t_tdc[i];
         aFThit->fPosition           = y1t_y[i];
         aFThit->fChannel            = ftGeoCalc->GetScintNumber(aFThit->fScintLayer, aFThit->fRow);
// std::cout << "alayer " << aFThit->fScintLayer
//           << " row "  << aFThit->fRow
//           << " tdc "  << aFThit->fTDC
//           << "\n";
      }

      for (i = 0; i < y2t_hit; i++) {
         aFThit = new(trackerHits[ftEvent->fNumberOfHits]) ForwardTrackerHit();
         aFThit->fHitNumber          = ftEvent->fNumberOfHits; //
         ftEvent->fNumberOfTDCHits++;
         ftEvent->fNumberOfHits++;
         aFThit->fLevel              = 1; //
         aFThit->fScintLayer         = 2; //
         aFThit->fPositionCoordinate = 2; //
         aFThit->fRow                = y2t_row[i];
         aFThit->fTDC                = y2t_tdc[i];
         aFThit->fPosition           = y2t_y[i];
         aFThit->fChannel            = ftGeoCalc->GetScintNumber(aFThit->fScintLayer, aFThit->fRow);
// std::cout << "blayer " << aFThit->fScintLayer
//           << " row "  << aFThit->fRow
//           << " tdc "  << aFThit->fTDC
//           << "\n";
      }

      for (i = 0; i < x1t_hit; i++) {
         aFThit = new(trackerHits[ftEvent->fNumberOfHits]) ForwardTrackerHit();
         aFThit->fHitNumber          = ftEvent->fNumberOfHits; //
         ftEvent->fNumberOfTDCHits++;
         ftEvent->fNumberOfHits++;
         aFThit->fLevel              = 1; //
         aFThit->fScintLayer         = 0; //
         aFThit->fPositionCoordinate = 1; //
         aFThit->fRow                = x1t_row[i];
         aFThit->fTDC                = x1t_tdc[i];
         aFThit->fPosition           = x1t_x[i];
         aFThit->fChannel            = ftGeoCalc->GetScintNumber(aFThit->fScintLayer, aFThit->fRow);
// std::cout << "clayer " << aFThit->fScintLayer
//           << " row "  << aFThit->fRow
//           << " tdc "  << aFThit->fTDC
//           << "\n";
      }
//printf("testing b\n");
// Gas Cherenkov data

// We count the number of hits by TDC hits
      gcEvent->ClearEvent("C");
//gcEvent->fHits->Delete("C");
      gcEvent->fNumberOfHits = 0;
      TClonesArray &cherenkovHits = *(gcEvent->fGasCherenkovHits);
      GasCherenkovHit * aCERhit;
//  cherenkovHits.SetOwner(kTRUE);
//cherenkovHits.Delete();

      gcEvent->fRunNumber =  currentRunNumber;
      gcEvent->fEventNumber =  gen_event_id_number;

      /** Gas Cherenkov: Inside the tdc hit loop
       *  we add a hit for the cherenkov each time
       *  the mirror numbers are the same.
       */
//if(gen_event_type==4) { // PEDESTAL
      for (j = 0; j < ceradc_hit; j++) {
         aCERhit = new(cherenkovHits[gcEvent->fNumberOfHits]) GasCherenkovHit();
         aCERhit->fTDC           = 0;
         aCERhit->fLevel         = 0; /// Level zero is just ADC data
         aCERhit->fADC           = cer_adc[j];
         aCERhit->fHitNumber     = gcEvent->fNumberOfHits;;
         aCERhit->fMirrorNumber  = ceradc_num[j];
         aCERhit->fChannel = ceradc_num[j];
         gcEvent->fNumberOfHits++;
         gcEvent->fNumberOfADCHits++;
      }
//} else {
      for (i = 0; i < cer_hit; i++) { // TDC loop
         for (j = 0; j < ceradc_hit; j++) {
            if (cer_num[i] == ceradc_num[j]) {
               aCERhit = new(cherenkovHits[gcEvent->fNumberOfHits]) GasCherenkovHit();
               aCERhit->fTDC          = cer_tdc[i];
               aCERhit->fADC          = cer_adc[j];
               aCERhit->fLevel        = 1; /// Level 1 is TDC data with ADC data
               aCERhit->fHitNumber    = gcEvent->fNumberOfHits;
               aCERhit->fTDCHitNumber     = i;;
               aCERhit->fMirrorNumber = ceradc_num[j];
               aCERhit->fChannel = ceradc_num[j];
               gcEvent->fNumberOfHits++;
//      new( (*gcEvent->fGasCherenkovTDCHits)[gcEvent->fNumberOfTDCHits] ) GasCherenkovHit(*aCERhit);
//      gcEvent->fNumberOfTDCHits++;
            }
         }
      }
//}
//printf("testing c\n");

// Lucite Hodoscope data
      lhEvent->ClearEvent("C");
//lhEvent->fHits->Delete("C");
      TClonesArray &lucHits = *(lhEvent->fLuciteHits);
      //TClonesArray &lucPosHits = *(lhEvent->fLucitePositionHits);
// lucHits.SetOwner(kTRUE);
//  lucHits.SetOwner(kTRUE);
//lucHits.Delete();
      LuciteHodoscopeHit * aLUChit;
      //LuciteHodoscopePositionHit * aPosLUChit;

      lhEvent->fNumberOfHits = 0;
      lhEvent->fRunNumber    =  currentRunNumber;
      lhEvent->fEventNumber  =  gen_event_id_number;

      // The analyzer loops over the pos and neg hit arrays and mixes them together
      // The outer loop is the positive and the innner loop is the negative. So,
      // the first

      /// \note Lucite indices go from 1 to 56 where 1,3,5,... are positive sides, and 2,4,6... are negative
      for (i = 0; i < luc_hit; i++) {
         if (ltdc_pos[i] != -100000) {
            aLUChit = new(lucHits[lhEvent->fNumberOfHits]) LuciteHodoscopeHit();
            aLUChit->fRow       = luc_row[i];
            aLUChit->fPMTNumber = (luc_row[i] - 1) * 2 + 1;
            aLUChit->fADC       = ladc_pos[i];
            aLUChit->fTDC       = ltdc_pos[i];
            aLUChit->fPosition  = luc_y[i];
            aLUChit->fLevel     = 1;
            aLUChit->fHitNumber     = lhEvent->fNumberOfHits;
            aLUChit->fTDCHitNumber     = lhEvent->fNumberOfHits;
            aLUChit->fChannel = (luc_row[i] - 1) * 2 + 1;
            lhEvent->fNumberOfHits++;
            lhEvent->fNumberOfTDCHits++;
         }

         if (ltdc_neg[i] != -100000) {
            aLUChit = new(lucHits[lhEvent->fNumberOfHits]) LuciteHodoscopeHit();
            aLUChit->fRow       = luc_row[i];
            aLUChit->fPMTNumber = (luc_row[i] - 1) * 2 + 2;
            aLUChit->fADC       = ladc_neg[i];
            aLUChit->fTDC       = ltdc_neg[i];
            aLUChit->fPosition  = luc_y[i];
            aLUChit->fLevel     = 1;
            aLUChit->fHitNumber     = lhEvent->fNumberOfHits;
            aLUChit->fTDCHitNumber     = lhEvent->fNumberOfHits;
            aLUChit->fChannel = (luc_row[i] - 1) * 2 + 2;
            lhEvent->fNumberOfHits++;
            lhEvent->fNumberOfTDCHits++;
         }

         /*
             aPosLUChit = new(lucPosHits[lhEvent->fNumberOfPositionHits]) LuciteHodoscopePositionHit();
             aPosLUChit->fRow    = luc_row[i];
             aPosLUChit->fTDCSum    = ltdc_pos[i]+ltdc_neg[i];
             aPosLUChit->fTDCDifference  = ltdc_pos[i]-ltdc_neg[i];
             lhEvent->fNumberOfPositionHits++;
         */


      }
// printf("testing d\n");

      /// BigCal data
      bcEvent->ClearEvent("C");
      BigcalHit * trigGroupBigcalHit[263][128]; // 128 is arbitrary 32*2*2
      Int_t nTrigGrouphits[263];
      for (int i = 0; i < 263; i++) nTrigGrouphits[i] = 0;

      bcEvent->fRunNumber =  currentRunNumber;
      bcEvent->fEventNumber =  gen_event_id_number;

      //bcEvent->fHits->Delete("C");
      TClonesArray &BCHits = *(bcEvent->fBigcalHits);
      //BCHits.SetOwner(kTRUE);

      BigcalHit * aBChit;
      BigcalHit * tBChit;
      //Int_t * rows;
      Int_t grouprowNumber;

      bcEvent->fNumberOfHits = 0;
      bcEvent->fTotalEnergyDeposited = 0.0;

      Int_t lowerLeftGroup = 0;

      for (i = 0; i < Bigcal_prot_nhit; i++) {
         aBChit = new(BCHits[bcEvent->fNumberOfHits]) BigcalHit();
         bcEvent->fNumberOfHits++;
         bcEvent->fNumberOfADCHits++;
         aBChit->fiCell = Bigcal_prot_ix[i];
         aBChit->fjCell = Bigcal_prot_iy[i];
         aBChit->fADC = Bigcal_prot_adc_raw[i];
         aBChit->fBlockNumber = bcgeo->GetBlockNumber(aBChit->fiCell, aBChit->fjCell);
         aBChit->fChannel = bcgeo->GetBlockNumber(aBChit->fiCell, aBChit->fjCell);
         /// Group number is numbered such that 1-4 form the bottom row, 5-8 form the next...
         grouprowNumber = bcgeo->GetGroupNumber(aBChit->fiCell, aBChit->fjCell);

         ///  pointers to pointers to hits in trigger group for a given group
         ///  this is done so that we do not have too loop through again later
         ///  when going through the tdcs
         trigGroupBigcalHit[grouprowNumber-1][nTrigGrouphits[grouprowNumber-1]] = aBChit;

         /// Group number is numbered such that 1-4 form the bottom row, 5-8 form the next...
         nTrigGrouphits[grouprowNumber-1]++;

//     if( (bcgeo->GetTDCRow(aBChit->fiCell,aBChit->fjCell))[1] != -1 ) { // Block is not only in one TDC row
//       grouprowNumber=((bcgeo->GetTDCRow(aBChit->fiCell,aBChit->fjCell))[1]-1)*4 + bcgeo->GetTDCGroup(aBChit->fiCell,aBChit->fjCell);
//       trigGroupBigcalHit[grouprowNumber-1][nTrigGrouphits[grouprowNumber-1]]=aBChit;
//       nTrigGrouphits[grouprowNumber-1]++;
//      }
         aBChit->fTDC = 0;
//     aBChit->fTDCRow = (bcgeo->GetTDCRow(aBChit->fiCell,aBChit->fjCell))[0];
//     aBChit->fTDCRow2 = (bcgeo->GetTDCRow(aBChit->fiCell,aBChit->fjCell))[1];
//    aBChit->fTDCGroup = bcgeo->GetTDCGroup(aBChit->fiCell,aBChit->fjCell);
         aBChit->fTDCLevel = -1;
         aBChit->fLevel = 0;
         aBChit->fEnergy = (Float_t)Bigcal_prot_adc_raw[i] * (Float_t)bcgeo->GetCalibrationCoefficient(aBChit->fiCell, aBChit->fjCell);
         bcEvent->fTotalEnergyDeposited += aBChit->fEnergy;
      }
// printf("testing esd\n");

      for (i = 0; i < Bigcal_rcs_nhit; i++) {
         aBChit = new(BCHits[bcEvent->fNumberOfHits]) BigcalHit();
         bcEvent->fNumberOfHits++;
         aBChit->fiCell = Bigcal_rcs_ix[i];
         aBChit->fjCell = Bigcal_rcs_iy[i];
         aBChit->fADC = Bigcal_rcs_adc_raw[i];
         aBChit->fBlockNumber = bcgeo->GetBlockNumber(aBChit->fiCell, aBChit->fjCell);
         aBChit->fChannel = bcgeo->GetBlockNumber(aBChit->fiCell, aBChit->fjCell);

         /// Group number is numbered such that 1-4 form the bottom row, 5-8 form the next...
         grouprowNumber = bcgeo->GetGroupNumber(aBChit->fiCell, aBChit->fjCell);

         ///  pointers to pointers to hits in trigger group for a given group
         ///  this is done so that we do not have too loop through again later
         ///  when going through the tdcs
         trigGroupBigcalHit[grouprowNumber-1][nTrigGrouphits[grouprowNumber-1]] = aBChit;

         /// Group number is numbered such that 1-4 form the bottom row, 5-8 form the next...
         nTrigGrouphits[grouprowNumber-1]++;

         /*
             if( (bcgeo->GetTDCRow(aBChit->fiCell,aBChit->fjCell))[1] != -1 ) { // Block is not only in one TDC row
               grouprowNumber=((bcgeo->GetTDCRow(aBChit->fiCell,aBChit->fjCell))[1]-1) + bcgeo->GetTDCGroup(aBChit->fiCell,aBChit->fjCell);
               trigGroupBigcalHit[grouprowNumber-1][nTrigGrouphits[grouprowNumber-1]]=aBChit;
               nTrigGrouphits[grouprowNumber-1]++;
              }*/
         aBChit->fLevel = 0;
         aBChit->fTDC = 0;
//     aBChit->fTDCRow = (bcgeo->GetTDCRow(aBChit->fiCell,aBChit->fjCell))[0];
//     aBChit->fTDCRow2 = (bcgeo->GetTDCRow(aBChit->fiCell,aBChit->fjCell))[1];
//    aBChit->fTDCGroup = bcgeo->GetTDCGroup(aBChit->fiCell,aBChit->fjCell);
         aBChit->fTDCLevel = -1;
         aBChit->fEnergy = (Float_t)Bigcal_rcs_adc_raw[i] * (Float_t)bcgeo->GetCalibrationCoefficient(aBChit->fiCell, aBChit->fjCell);
         bcEvent->fTotalEnergyDeposited += aBChit->fEnergy;
      }
// printf("testing e\n");


      /// Bigcal tdc hits sum of 8 blocks
      // if tdc hit corresponds to block's row
      for (j = 0; j < Bigcal_tdc_nhit; j++)  {
         //aBChit = new(BCHits[bcEvent->fNumberOfHits]) BigcalHit();
         //aBChit->fADC=0;
         //aBChit->fEnergy=0;
         tBChit = new((*(bcEvent->fBigcalTimingGroupHits))[j]) BigcalHit();
         bcEvent->fNumberOfHits++;
         bcEvent->fNumberOfTDCGroupHits++;

         tBChit->fEnergy = 0;
         tBChit->fADC = 0;


         for (int i = 0; i < nTrigGrouphits[(Bigcal_tdc_raw_irow[j] - 1) * 4 + Bigcal_tdc_raw_igroup[j] - 1]; i++) {

            grouprowNumber  = (Bigcal_tdc_raw_irow[j] - 1) * 4 + Bigcal_tdc_raw_igroup[j];

            trigGroupBigcalHit[grouprowNumber-1][i]->fTDC       = Bigcal_tdc_raw[j];
//       trigGroupBigcalHit[grouprowNumber-1][i]->fTDC       = Bigcal_tdc_raw[j];
            trigGroupBigcalHit[grouprowNumber-1][i]->fTDCRow    = Bigcal_tdc_raw_irow[j];
            trigGroupBigcalHit[grouprowNumber-1][i]->fTDCGroup  = Bigcal_tdc_raw_igroup[j];

            //aBChit->fADC    += trigGroupBigcalHit[grouprowNumber-1][i]->fADC;
            //aBChit->fEnergy += trigGroupBigcalHit[grouprowNumber-1][i]->fEnergy;
            tBChit->fADC    += trigGroupBigcalHit[grouprowNumber-1][i]->fADC;
            tBChit->fEnergy += trigGroupBigcalHit[grouprowNumber-1][i]->fEnergy;
         }
//     aBChit->fiCell=-1;
//     aBChit->fjCell=-1;
//     aBChit->fTDC=Bigcal_tdc_raw[j];
//     aBChit->fTDCRow=Bigcal_tdc_raw_irow[j];
//     aBChit->fTDCGroup=Bigcal_tdc_raw_igroup[j];
//     aBChit->fTDCLevel=1;

         tBChit->fiCell = -1;
         tBChit->fjCell = -1;
         tBChit->fTDC = Bigcal_tdc_raw[j];
         tBChit->fTDCRow = Bigcal_tdc_raw_irow[j];
         tBChit->fTDCGroup = Bigcal_tdc_raw_igroup[j];
         tBChit->fTDCLevel = 1;
         tBChit->fChannel = bcgeo->GetGroupNumberFromTDCHit(tBChit->fTDCGroup,tBChit->fTDCRow);

         tBChit->fLevel = 1;


         //aBChit->fLevel=1;
      }

/// \note { BIGCAL_tdc_raw_igroup (see above) is a horizontal grouping of 8 blocks,
/// while the Bigcal_ttrig_igroup below is a vertical index of 1-19
/// group(1-19) Unlike the group for the sum of 8 which goes horizontally!
/// ihalf(1-2) Left-Right
      for (i = 0; i < Bigcal_atrig_nhit; i++)
         for (j = 0; j < Bigcal_ttrig_nhit; j++) {
            if (Bigcal_ttrig_igroup[j] == Bigcal_atrig_igroup[i]
                && Bigcal_atrig_ihalf[i] == Bigcal_ttrig_ihalf[j]) {

               aBChit = new((*bcEvent->fBigcalTriggerGroupHits)[bcEvent->fNumberOfTriggerGroupHits]) BigcalHit();
               bcEvent->fNumberOfHits++;
               bcEvent->fNumberOfTriggerGroupHits++;
               aBChit->fiCell = Bigcal_ttrig_ihalf[j];
               aBChit->fjCell = Bigcal_ttrig_igroup[j];
               aBChit->fTriggerHalf = Bigcal_atrig_ihalf[i];
               aBChit->fTDCGroup = Bigcal_atrig_ihalf[i];
               aBChit->fTDCRow = Bigcal_atrig_igroup[i];
               aBChit->fADC = Bigcal_atrig_adc_raw[i];
               aBChit->fTDC = Bigcal_ttrig_tdc_raw[j];
               aBChit->fChannel = (aBChit->fTDCRow - 1) * 2 + aBChit->fTDCGroup;
               aBChit->fLevel = 2;
               aBChit->fTDCLevel = 3;
               aBChit->fEnergy = 0.0;
               ///\todo Don't use just the first(last actually) bigcal trigger group for the trigger time
               trigEvent->fTriggerTDC = Bigcal_ttrig_tdc_raw[j];

/// The following sums all the energies in the "sum of 8" TDC groups
/// it grabs the lower left+1 "sum of 8" and steps through the 2(horizontal "sum of 8" groups) by (4 vert)
/// And if there is a hit in the group it adds the energy
///  \todo A cut needs to be placed here so that tdc hits that pass a timing cut are only added!!!
               for (int i2 = 0; i2 < 2; i2++)for (int j2 = 0; j2 < 4; j2++)
                     if ((Bigcal_atrig_igroup[i] - 1) * 3 + 1 + j2 < 57) { /*this if prevents the following from looping over rows 57,58 of the top sum of 64 groups which actully have 32-2*2=28 cells */
                        lowerLeftGroup =
                           bcgeo->GetTDCRow(
                              (Bigcal_atrig_ihalf[i] - 1) * 16 + i2 * 8 + 2, /*+2, not +1 avoids the imaginary cells in rcs*/
                              (Bigcal_atrig_igroup[i] - 1) * 3 + 1 + j2)[0];
                        for (int i3 = 0; i3 < nTrigGrouphits[lowerLeftGroup]; i3++)
                           aBChit->fEnergy += trigGroupBigcalHit[lowerLeftGroup][i3]->fEnergy;
                     }

//     new((*bcEvent->fBigcalTriggerGroupHits)[bcEvent->fNumberOfTriggerGroupHits]) BigcalHit(*aBChit);

//  std::cout << " bc  half:" << aBChit->fjCell << "\n";
//  std::cout << " bc group:" << aBChit->fiCell << "\n";
            }
         }


      /*
      /// Adjust for timing problem between bigcal and cherenkov
      if( currentRunNumber < 72900 && currentRunNumber > 71000)  {
         Double_t bigcal_cer_timing = trigEvent->fTriggerTDC - 370.0;
         bigcal_cer_timing = bigcal_cer_timing*(0.5/0.06);
         for (j = 0; j < cherenkovHits.GetEntries(); j++) {
            aCERhit = (GasCherenkovHit*)(cherenkovHits[j]);
            if(aCERhit->fLevel == 1 ) {
               aCERhit->fTDC = aCERhit->fTDC + bigcal_cer_timing; 
            }
         }
      }
      */

      hmsEvent->hms_p      = hms_p;
      hmsEvent->hms_e      = hms_e;
      hmsEvent->hms_theta  = hms_theta;
      hmsEvent->hms_phi    = hms_phi;
      hmsEvent->hsxfp_s    = hsxfp_s;
      hmsEvent->hsyfp_s    = hsyfp_s;
      hmsEvent->hsxpfp_s   = hsxpfp_s;
      hmsEvent->hsypfp_s   = hsypfp_s;
      hmsEvent->hms_xtar   = hms_xtar;
      hmsEvent->hms_ytar   = hms_ytar;
      hmsEvent->hms_yptar  = hms_yptar;
      hmsEvent->hms_xptar  = hms_xptar;
      hmsEvent->hms_delta  = hms_delta;
      hmsEvent->hms_start  = hms_start;
      hmsEvent->hsshtrk_s  = hsshtrk_s;
      hmsEvent->hsshsum_s  = hsshsum_s;
      hmsEvent->hsbeta_s   = hsbeta_s;
//printf("testing f\n");

      fTreeOut->Fill();
//printf("testing f1\n");
//beamTree->Fill();
//printf("testing f2\n");
//hmsTree->Fill();
//printf("testing f3\n");
//mcTree->Fill();
//printf("testing f4\n");
//aTree->Fill();
//printf("testing g\n");



   } // number of entries < 100000
   return kTRUE;
}
//_____________________________________________________________________________

void AnalyzerToInSANE::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.

}
//_____________________________________________________________________________

void AnalyzerToInSANE::Terminate()
{

   if (SANERunManager::GetRunManager()->GetVerbosity() > 0)   printf(" - Terminating Event Data Conversion.  \n");
   if (SANERunManager::GetRunManager()->GetVerbosity() > 0)  {
      printf(" - Run Info : %d\n", fRun->GetRunNumber());
   }
   //if(!fRun->FillRunInfoDatabase() ) printf(" - Run Info Database Filled\n");

   fFileOut->cd();
   SANERunManager::GetRunManager()->GetCurrentRun()->fAnalysisPass = -1;
   SANERunManager::GetRunManager()->WriteRun();

   if (SANERunManager::GetRunManager()->GetVerbosity() > 1)
      std::cout  << " o Building index for output tree in AnalyzerToInSANE::Terminate \n";
   fTreeOut->BuildIndex("fRunNumber", "fEventNumber");

//    if(fTreeOut)fTreeOut->FlushBaskets();

   fTreeOut->Write("", TObject::kOverwrite);
   /*fFileOut->Flush();*/
//    fFileOut->Write();

//    fFileOut->Close();

   if (SANERunManager::GetRunManager()->GetVerbosity() > 1)  printf(" - AnalyzerToInSANE writing done. \n");

}
//_____________________________________________________________________________
