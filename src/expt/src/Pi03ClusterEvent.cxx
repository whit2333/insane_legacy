#include "Pi03ClusterEvent.h"

ClassImp(Pi03ClusterEvent)


//______________________________________________________________________________
Pi03ClusterEvent::Pi03ClusterEvent() {
   fNumberOfGroups = 0;
   fNumberOfPairs  = 0;
   fReconstruction = new TClonesArray("InSANEReconstruction", 1);
   fClusterPairs   = new TClonesArray("Pi0ClusterPair", 1);
   fClusters       = new TClonesArray("BIGCALCluster", 1);
}
//______________________________________________________________________________
Pi03ClusterEvent::~Pi03ClusterEvent() {
}
//______________________________________________________________________________
void Pi03ClusterEvent::Reset() {
   //    fCherenkovHit= false;fPID=-1; fNTracks=0;
}
//______________________________________________________________________________
void Pi03ClusterEvent::ClearEvent(const char * opt) {
   InSANEReconstructedEvent::ClearEvent(opt);
   //      fNumberOfPairs=0;
   if (fClusterPairs) fClusterPairs->Clear(opt); /// Don't give it "C" because it is owned by original cluster TClonesArray
   if (fReconstruction) fReconstruction->Clear(opt);
   if (fClusters) fClusters->Clear(opt);
   fNumberOfPairs = 0;
   fNumberOfGroups = 0;
   //      /* this causes trouble... pointer is owned by clusters array  fClusterPair->Clear();*/
   //      fPID=-1;
   //     fNTracks=0;
}
//______________________________________________________________________________
const char * Pi03ClusterEvent::PrintEvent() const  {
   TString astr;
   //      for(int i = 0;i<fNTracks;i++) {
   //      astr+=Form( "M%d = %f",i,((InSANEReconstruction*)(*fReconstruction)[i])->fMass);
   /*astr+="M";astr+=i;astr+=" = ";astr+=((InSANEReconstruction*)(*fReconstruction)[i])->fMass;astr+=" \n";*/
   //      }
   return(astr.Data());
}
//______________________________________________________________________________
void Pi03ClusterEvent::Print() const {
   InSANEReconstructedEvent::Print();
   std::cout << PrintEvent() << "\n";
}
//______________________________________________________________________________
Double_t Pi03ClusterEvent::CalculateMass(Double_t nu1, Double_t E1, Double_t E2, Double_t thetapm, Double_t thetaGammaGamma) {
   //       Double_t res =  2.0*v1.Mag()*(k1.Mag()+k2.Mag()) -2.0*(v1*(k1+k2))
   //                      + 2.0*( k1.Mag()*k2.Mag() - (k1*k2) ) ;
   Double_t res = 2.0 * (E1 * E2) * (1.0 - TMath::Cos(thetapm)) + 2.0 * nu1 * (E1 + E2) * (1.0 - TMath::Cos(thetaGammaGamma));
   return(TMath::Sqrt(res));
}
//______________________________________________________________________________

