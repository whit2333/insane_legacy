#include "BigcalCalculations.h"

#include "InSANECorrection.h"
#include "BIGCALGeometryCalculator.h"
#include "InSANEReconstruction.h"
#include <exception>

ClassImp(BigcalCalculation1)

//______________________________________________________________________________
Int_t BigcalCalculation1::Calculate() {

   if (!(fEvents->TRIG->IsClusterable()))  return(0);
   aBetaEvent->fBigcalEvent->fNumberOfADCHits = 0;
   aBetaEvent->fBigcalEvent->fNumberOfTDCHits = 0;
   aBetaEvent->fBigcalEvent->fNumberOfTDCGroupHits = 0;
   aBetaEvent->fBigcalEvent->fNumberOfTriggerGroupHits = 0;
   aBetaEvent->fBigcalEvent->fNumberOfTimedTDCHits = 0;

   for (int kk = 0; kk < fEvents->BETA->fBigcalEvent->fBigcalHits->GetEntries(); kk++) {
      aBigcalHit = (BigcalHit*)(*fBigcalHits)[kk] ;
      if (aBigcalHit->fLevel == 0) {
         aBetaEvent->fBigcalEvent->fNumberOfADCHits++;
      }
   }
   for (int kk = 0; kk < fEvents->BETA->fBigcalEvent->fBigcalTimingGroupHits->GetEntries(); kk++) {
      aBigcalHit = (BigcalHit*)(*fEvents->BETA->fBigcalEvent->fBigcalTimingGroupHits)[kk] ;

      /// Count all TDC hits
      if (aBigcalHit->fLevel == 1) { /// sum of 32 tdc
         aBetaEvent->fBigcalEvent->fNumberOfTDCHits++;
         if (fBigcalDetector->HitPassesTDCCut(aBigcalHit))
            aBetaEvent->fBigcalEvent->fNumberOfTimedTDCHits++;
      }
   }
   for (int kk = 0; kk < fEvents->BETA->fBigcalEvent->fBigcalTriggerGroupHits->GetEntries(); kk++) {
      aBigcalHit = (BigcalHit*)(*fEvents->BETA->fBigcalEvent->fBigcalTriggerGroupHits)[kk] ;

      if (aBigcalHit->fLevel == 2) { /// sum of 64 tdc
         aBetaEvent->fBigcalEvent->fNumberOfTDCGroupHits++;

      }
      if (aBigcalHit->fTDCLevel == 2) { /// trigger timing groups
         aBetaEvent->fBigcalEvent->fNumberOfTriggerGroupHits++;
         //         new((*aBetaEvent->fBigcalEvent->fBigcalTriggerGroupHits)[aBetaEvent->fBigcalEvent->fNumberOfTriggerGroupHits]) BigcalHit(*aBigcalHit);

      }

   } // End loop over bigcal signals
   return(0);
}
//______________________________________________________________________________

