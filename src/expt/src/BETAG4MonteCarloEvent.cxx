#include "BETAG4MonteCarloEvent.h"




ClassImp(BETAG4MonteCarloEvent)
//_________________________________________________________________//

BETAG4MonteCarloEvent::BETAG4MonteCarloEvent()
{
   if (InSANERunManager::GetRunManager()->fVerbosity > 0) printf(" - Creating BETAG4MonteCarlo Event \n");
   fTrackerPlaneHits = nullptr;
   fTrackerPlane2Hits = nullptr;
   fBigcalPlaneHits = nullptr;
   fThrownParticles = nullptr;
   fNumberOfParticlesThrown = 0;
   /*   AllocateHitsMemory();*/
}
//_________________________________________________________________//

BETAG4MonteCarloEvent::~BETAG4MonteCarloEvent()
{
   if (InSANERunManager::GetRunManager()->fVerbosity > 0)
      std::cout << " - deleted BETAG4MonteCarloEvent   " << std::endl;

   if (fTrackerPlaneHits) fTrackerPlaneHits->Delete();
   fTrackerPlaneHits = nullptr;

   if (fTrackerPlane2Hits) fTrackerPlane2Hits->Delete();
   fTrackerPlane2Hits = nullptr;

   if (fBigcalPlaneHits) fBigcalPlaneHits->Delete();
   fBigcalPlaneHits = nullptr;

   if (fThrownParticles) fThrownParticles->Delete();
   fThrownParticles = nullptr;
}
//_________________________________________________________________//

Int_t BETAG4MonteCarloEvent::AllocateMemory()
{


   return(0);
}
//_________________________________________________________________//

Int_t BETAG4MonteCarloEvent::AllocateHitsMemory()
{
   /*   std::cout << " o BETAG4MonteCarloEvent::AllocateHitsMemory()  \n";*/
   if (!fTrackerPlaneHits) fTrackerPlaneHits = new TClonesArray("InSANEFakePlaneHit", 1);
   if (!fTrackerPlane2Hits) fTrackerPlane2Hits = new TClonesArray("InSANEFakePlaneHit", 1);
   if (!fThrownParticles) fThrownParticles = new TClonesArray("InSANEParticle", 1);
   if (!fBigcalPlaneHits) fBigcalPlaneHits = new TClonesArray("InSANEFakePlaneHit", 1);

   return(0);
}
//_________________________________________________________________//

Int_t BETAG4MonteCarloEvent::FreeMemory()
{
   if (fTrackerPlaneHits) fTrackerPlaneHits->Delete();
   fTrackerPlaneHits = nullptr;
   if (fTrackerPlane2Hits) fTrackerPlane2Hits->Delete();
   fTrackerPlane2Hits = nullptr;
   if (fBigcalPlaneHits) fBigcalPlaneHits->Delete();
   fBigcalPlaneHits = nullptr;
   if (fThrownParticles) fThrownParticles->Delete();
   fThrownParticles = nullptr;
   return(0);
}
//_________________________________________________________________//

Int_t BETAG4MonteCarloEvent::SetHitsBranch(TTree * t)
{

   t->SetBranchAddress("fTrackerMCPlane2Hits", &(fTrackerPlane2Hits));
   t->SetBranchAddress("fTrackerMCPlaneHits", &(fTrackerPlaneHits));
   t->SetBranchAddress("fBigcalMCPlaneHits", &(fBigcalPlaneHits));
   t->SetBranchAddress("fThrownParticles", &(fThrownParticles));

   return(0);
}
//_________________________________________________________________//

Int_t BETAG4MonteCarloEvent::AddHitsBranch(TTree * t)
{

   t->Branch("fTrackerMCPlaneHits", fTrackerPlaneHits);
   t->Branch("fTrackerMCPlane2Hits", fTrackerPlane2Hits);
   t->Branch("fBigcalMCPlaneHits",  fBigcalPlaneHits);
   t->Branch("fThrownParticles",  fThrownParticles);
   return(0);
}
//_________________________________________________________________//

void BETAG4MonteCarloEvent::ClearEvent(const char * opt ) {
      fNumberOfHits = 0;
      fNumberOfParticlesThrown = 0;

   if (fTrackerPlaneHits)if(fTrackerPlaneHits->GetEntries() > 10 ) {
       fTrackerPlaneHits->ExpandCreate(2);
   }
   if (fTrackerPlaneHits)fTrackerPlaneHits->Clear(opt);

   if (fTrackerPlane2Hits)if(fTrackerPlane2Hits->GetEntries() > 20 ) {
       fTrackerPlane2Hits->ExpandCreate(2);
   }
   if (fTrackerPlane2Hits)fTrackerPlane2Hits->Clear(opt);

   if (fBigcalPlaneHits)if(fBigcalPlaneHits->GetEntries() > 20 ) {
       fBigcalPlaneHits->ExpandCreate(2);
   }
   if (fBigcalPlaneHits)fBigcalPlaneHits->Clear(opt);

   if (fThrownParticles)fThrownParticles->Clear(opt);
}
//_________________________________________________________________//

