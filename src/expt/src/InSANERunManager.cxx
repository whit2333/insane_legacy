#include "InSANERunManager.h"
//#include "TSQLRow.h"
//#include "TSQLResult.h"
//#include "TStopwatch.h"
//#include <iostream>
//#include <fstream>
//#include <stdio.h>
//#include "TSQLStatement.h"
//#include <iostream>
//#include <string>
#include <vector>
#include <algorithm>
#include "InSANEHitPosition.h"

ClassImp(InSANERunManager)

//_____________________________________________________________________________

InSANERunManager * InSANERunManager::fgSANERunManager = nullptr;
//_____________________________________________________________________________

InSANERunManager::InSANERunManager() : fRunNumber(-1), fVerbosity(3)
{
   //std::cout << "InSANERunManager ctor" << std::endl;
   fCurrentFile = nullptr;
   fScalerFile = nullptr;
   fCurrentRun = nullptr;
   fCurrentRunNumber = -1;
   fCurrentFileNumber = 0;
   fCurrentFileName = "";
   fPreviousFileName = "";
   fPreviousFile = nullptr;
   fRunReport = nullptr;
   fRunIsSet = false;
   fRandomNumberGenerator = nullptr;
}
//_____________________________________________________________________________

InSANERunManager::~InSANERunManager()
{
}
//_____________________________________________________________________________

Int_t InSANERunManager::GetLastPassNumber(Int_t runnumber) const
{
   Int_t pass = -1;
   Long_t id, flags, modtime;
   Long64_t size;
   TString file = "";
   for (int i = -1 ; i < 5 ; i++) {
      file = Form("data/rootfiles/InSANE%d.%d.root", runnumber, i);
      if (!(gSystem->GetPathInfo(file.Data(), &id, &size, &flags, &modtime))) {
         pass = i;
      }
   }
   return(pass);
}
//_____________________________________________________________________________
void InSANERunManager::cpp11test(){
   std::vector<int> vec;
   vec.push_back( 1 );
   vec.push_back( 2 );
   //for (int& i : vec )
   //{
   //   i++; // increments the value in the vector
   //}
   //for (int i : vec )
   //{
   //   // show that the values are updated
   //   std::cout << i << std::endl;
   //}
   //int norm = 33; 
   //std::for_each(vec.begin(), vec.end(), [](int i) { std::cout << i << "^2 = " << i*i << std::endl; /*norm=i*i*norm;*/ });
   //auto l = [](double* xs, double *ps) { return xs[0]*xs[0]; };
   ////TF1 * f = new TF1("f", l, 0, 100, 0);
   ////f->Draw();
   ////std::cout << norm << std::endl;

   //// sorting test of own class
   //std::vector<InSANEHitPosition>  hits;
   //InSANEHitPosition h1 = InSANEHitPosition();
   //h1.fMissDistance = 5.0;
   //InSANEHitPosition h2 = InSANEHitPosition();
   //h2.fMissDistance = 2.0;
   //InSANEHitPosition h3 = InSANEHitPosition();
   //h3.fMissDistance = 3.0;

   //hits.push_back(h1);
   //hits.push_back(h2);
   //hits.push_back(h3);

   //std::cout << "hits contains:";

   //for (int i = 0; i< hits.size();i++)
   //   std::cout << ' ' << hits[i].fMissDistance;
   //std::cout << std::endl;
   //std::cout << "sorting.." << std::endl;
   //std::sort(hits.begin(),hits.end());
   //for (int i = 0; i< hits.size();i++)
   //   std::cout << ' ' << hits[i].fMissDistance;
   //std::cout << std::endl;

}

//_____________________________________________________________________________
Int_t InSANERunManager::GetNumberOfPasses(Int_t runnumber) const
{
   Int_t n = 0;
   Long_t id, flags, modtime;
   Long64_t size;
   TString file = "";
   for (int i = -1 ; i < 5 ; i++) {
      file = Form("data/rootfiles/InSANE%d.%d.root", runnumber, i);
      if (!(gSystem->GetPathInfo(file.Data(), &id, &size, &flags, &modtime))) {
         n++;
      }
   }
   return(n);
}
//_____________________________________________________________________________


InSANERunManager * InSANERunManager::GetRunManager()
{
   if (!fgSANERunManager) fgSANERunManager = new InSANERunManager();
   return(fgSANERunManager);
}
//_____________________________________________________________________________

Int_t InSANERunManager::OpenRun(Int_t run)
{

   return(0);
}
//_____________________________________________________________________________
Bool_t InSANERunManager::IsPerpendicularRun() {
   if (!fCurrentRun) {
      Warning("IsPerpendicularRun", "NO RUN SET");
      if(fCurrentRunNumber<72900 && fCurrentRunNumber>71000) return(true);
      else return(false);
   }
   if (fCurrentRun->fTargetAngle < 90.0 && fCurrentRun->fTargetAngle > 70.0)
      return(true);
   /* else */
   return(false);
}
//______________________________________________________________________________
Bool_t InSANERunManager::IsParallelRun() {
   if (fCurrentRun) if (fCurrentRun->fTargetAngle < 190.0 && fCurrentRun->fTargetAngle > 170.0)
      return(true);
   /* else */
   return(false);
}
//______________________________________________________________________________
Bool_t InSANERunManager::IsSimulationRun() { 
   if(fCurrentRunNumber<70000) return(true);
   return(false);
}
//______________________________________________________________________________
Int_t InSANERunManager::CloseRun() {
   CloseCurrentFile();
   if(fCurrentRun) delete fCurrentRun;
   fCurrentRun = nullptr;
   fRunNumber = 0;
   fCurrentRunNumber = 0;
   CloseScalers();
   //if(fCurrentRun)
   fRunIsSet = false;
   return(0);
}
//_____________________________________________________________________________

Int_t InSANERunManager::SetRun(Int_t run, Int_t pass, const char * opt)
{
   ///\todo fix it so that you can set it to open arbitrary pass (if it exists)
   Int_t returncode = 0;
   /// First Close current file
   if (IsRunSet()) CloseRun();
   fRunNumber = run;
   /// Set run number
   fRunNumber = run;
   fCurrentRunNumber = run; // redundant

   /// Open files and get run
   if (GetNumberOfPasses(fRunNumber) == 0) fCurrentFileNumber = pass;
   else fCurrentFileNumber = GetLastPassNumber(fRunNumber);
   returncode += OpenCurrentPassFile();
   if (GetNumberOfPasses(fRunNumber) > 1) {
      fPreviousFileNumber = fCurrentFileNumber - 1;
      returncode += OpenPreviousPassFile();
   }

   /// open file
   //  if( SetPassFile(pass) ) return(-1);

   /// Get the run object
   if (FindRun(opt) != 0) {
      Error("SetRun", "InSANERunManager::FindRun returned nonzero value!\n");
   }

   /// Open Scaler File
   if (SetScalers() != 0) {
      Error("SetRun", "InSANERunManager::SetScalers returned nonzero value!\n");
   }

   fRunIsSet = true;

   return(returncode);
}
//_____________________________________________________________________________

Int_t InSANERunManager::FindRun(const char * opt)
{
   /// get the latest run object
   if (fVerbosity > 1)
      std::cout << " - Finding run object ... \n";

   fCurrentFile->cd();

   fCurrentRun = dynamic_cast<InSANERun*>(gROOT->FindObject("SANERun"));
   /*   gDirectory->GetObject("SANERun",fCurrentRun);*/

   if (!fCurrentRun) {
      fCurrentFile->cd();
      fCurrentRun = dynamic_cast<InSANERun*>(gROOT->FindObject("SANERun"));
      /*      gDirectory->GetObject("SANERun",fCurrentRun);*/
   }

   if (fCurrentRun) {
      if (this->fVerbosity > 2) std::cout << " + Found Run!\n";
      /// if Q option is not given, print
      if (strcmp(opt, "Q")) fCurrentRun->Print();

      /// Using Generate run report which defines Run Report members using Run Object
      GenerateRunReport();

   } else {

      /// Create a NEW InSANERun
      if(fVerbosity > 0 ) std::cout << " + Creating NEW InSANERun object. \n" ;
      fCurrentRun = new InSANERun(fRunNumber);
      /// if Q option is not given
      //if( (strcmp(opt,"Q")) ) fCurrentRun->Print();

      /// create the directory structure of the file...
      /// since there is no run object ...
      CreateDirectories();

      /// Using Generate pre-run report which defines the Run Object's data members using
      /// the run report members which are defined from the database entries
      GeneratePreRunReport();
   }

   return(0);
}
//_____________________________________________________________________________

Int_t InSANERunManager::SetScalers(Int_t scaler)
{
   if (fScalerFile) CloseScalers();
   fScalerFileName = Form("data/rootfiles/scalers%d.root", fCurrentRunNumber);
   fScalerFile =  new TFile(fScalerFileName.Data(), "UPDATE");
   if (fScalerFile->IsZombie()) {
      std::cerr << "x- No scaler file for this run found at: " << fScalerFileName.Data()
                << " !\nx- It should have been created\nx- Are you in the correct directory?\n";
      return(-1);
   } else if (fVerbosity > 0)std::cout << " + Opened file: " << fScalerFileName.Data() << "\n";

   if (fCurrentFile)fCurrentFile->cd();
   return(0);
}
//_____________________________________________________________________________

Int_t InSANERunManager::SetPassFile(Int_t pass)
{
   fCurrentFileNumber = pass;
   TString oldFileName = fCurrentFileName ;
   fCurrentFileName = Form("data/rootfiles/InSANE%d.%d.root", fCurrentRunNumber, pass);
   if (!fCurrentFile) fCurrentFile =  new TFile(fCurrentFileName.Data(), "UPDATE");
   else std::cerr << "x- Current file is still open! File not opened.\n";
// For now putting everything in one file
//     if( !IsRunSet() )
//     {
//       printf("run not set\n");
//       return(-1);
//     } else if(fFileMap.size() == 0) // Nothing opened yet
//     {
//       fCurrentFileNumber = pass;
//       fCurrentFileName = new TString(Form("data/rootfiles/InSANE%d.%d.root",fCurrentRunNumber,fCurrentFileNumber));
//       fCurrentFile    =  new TFile(fCurrentFileName->Data(),"UPDATE");
//       fPreviousFile = fCurrentFile; // To avoid a null reference
//       return(0);
//     } else if(fCurrentFileNumber == pass) { // called with pass already set
//       fPreviousFile = fCurrentFile;
//       fCurrentFile = (TFile*)(fFileMap.find(pass)->second);
//       fCurrentFile->cd();
//       return(0);
//     } else if( (fFileMap.find(pass)->first) == pass ) {  // the file is opened
//       fPreviousFile = fCurrentFile;
//       iter = fFileMap.find(pass);
//       fCurrentFile = (TFile*)(iter->second);
//       fCurrentFile->cd();
//       fCurrentFileNumber = pass;
//       return(0);
//     } else {  // the file is not opened
//       fPreviousFile = fCurrentFile;
//       fCurrentFileNumber = pass;
//       fCurrentFileName = new TString(Form("data/rootfiles/InSANE%d.%d.root",fCurrentRunNumber,fCurrentFileNumber));
//       fCurrentFile    =  new TFile(fCurrentFileName->Data(),"UPDATE");
//       fFileMap.insert(std::pair<Int_t,TFile*>(fCurrentFileNumber,fCurrentFile));
//       fCurrentFile->cd();
//       return(0);
//     }
   return(0);
}
//_____________________________________________________________________________
Int_t          InSANERunManager::OpenCurrentPassFile() {
   fCurrentFileName = Form("data/rootfiles/InSANE%d.%d.root", fRunNumber, fCurrentFileNumber);
   fCurrentFile = new TFile(fCurrentFileName.Data() , "UPDATE");
   if (fCurrentFile) return(0);
   else Error("OpenCurrentPassFile", "Could not open file %s", fCurrentFileName.Data());
   return(-1);
}
//_____________________________________________________________________________
Int_t          InSANERunManager::OpenPreviousPassFile() {
   fPreviousFileName = Form("data/rootfiles/InSANE%d.%d.root", fRunNumber, fPreviousFileNumber);
   fPreviousFile = new TFile(fPreviousFileName.Data(), "READ");
   if (fPreviousFile) return(0);
   return(-1);
}
//_____________________________________________________________________________
Int_t          InSANERunManager::CreateNewPass() {
   fPreviousFileNumber = fCurrentFileNumber;
   fPreviousFileName   = fCurrentFileName;
   if (fPreviousFile) fPreviousFile->Close();
   fPreviousFile = nullptr;
   Int_t res = OpenPreviousPassFile();
   fCurrentFileNumber++;
   res += OpenCurrentPassFile();
   CreateDirectories();
   return(res);
}
//_____________________________________________________________________________
void InSANERunManager::PrintStatistics()
{
   if (fRunNumber == 0) {
      printf("x- Please use SetRun(#) first");
   }
}
//_____________________________________________________________________________
Int_t InSANERunManager::WriteCurrentRunInfoToFile()
{
   TFile * f = nullptr;
   f = fCurrentFile;
   if (f) if (fCurrentRun) {
         fCurrentFile->cd();
         fCurrentRun->Write("SANERun", kWriteDelete);
      }
//   f = fScalerFile;
//   if(f) f->Close();
   fCurrentFile->cd();
   return(0);
}
//______________________________________________________________________________
Int_t InSANERunManager::WriteRunReport() {
   if (fRunReport) {
      fRunReport->Update();
      fRunReport->UpdateRunDatabase();
      return(0);
   } else {
      std::cerr << "x- Run report not found in WriteRunReport()\n";
      return(-1);
   }
}
//_____________________________________________________________________________
Int_t InSANERunManager::WriteRun() {
   WriteRunReport();
   if (fCurrentFile) {
      if (fCurrentRun) {
         fCurrentFile->cd();
         Int_t nbytes = fCurrentRun->Write("SANERun", kWriteDelete);
         //std::cout << "Run object written in " << nbytes << " bytes\n";
         return(0);
      } else {
         std::cerr << "x- Error: no run object\n";
         return(1);
      }
   } else {
      std::cerr << "x- Error: no file found\n";
      return(1);
   }
}
//______________________________________________________________________________
InSANERun * InSANERunManager::GetCurrentRun()
{
   if (fCurrentRun) return(fCurrentRun);
   else {
      std::cerr << "x- Null fCurrentRun\n";
      return(fCurrentRun);
   }
}
//_____________________________________________________________________________

InSANERunReport * InSANERunManager::GetRunReport()
{
   if (fRunReport) return(fRunReport);
   else {
      std::cerr << "x- Null fRunReport\n";
      return(fRunReport);
   }
}
//_____________________________________________________________________________

void InSANERunManager::CreateDirectories()
{

   // TDC timing directory
   fCurrentFile->cd();
   timingDir = fCurrentFile->mkdir("timing");
   if (timingDir) {
      timingDir->cd();
      if ((timingCerDir = timingDir->mkdir("cherenkov"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"timing/cherenkov\" directory \n";
      } else std::cerr << " = Error Making \"timing/cherenkov\" directory! \n";
      if ((timingLucDir = timingDir->mkdir("hodoscope"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"timing/hodoscope\" directory \n";
      } else std::cerr << " = Error Making \"timing/hodoscope\" directory! \n";
      if ((timingBigcalDir = timingDir->mkdir("bigcal"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"timing/bigcal\" directory \n";
      } else std::cerr << " = Error Making \"timing/bigcal\" directory! \n";
      if ((timingtrackerDir = timingDir->mkdir("tracker"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"timing/tracker\" directory \n";
      } else std::cerr << " = Error Making \"timing/tracker\" directory! \n";
   } else {
      std::cerr << " = Error Making \"timing\" directory! \n";
   }

   // Pedestals directory
   fCurrentFile->cd();
   pedDir = fCurrentFile->mkdir("pedestals");
   if (pedDir) {
      pedDir->cd();
      if ((pedCerDir = pedDir->mkdir("cherenkov"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"pedestals/cherenkov\" directory \n";
      } else std::cerr << " = Error Making \"pedestals/cherenkov\" directory! \n";
      if ((pedLucDir = pedDir->mkdir("hodoscope"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"pedestals/hodoscope\" directory \n";
      } else std::cerr << " = Error Making \"pedestals/hodoscope\" directory! \n";
      if ((pedBigcalDir = pedDir->mkdir("bigcal"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"pedestals/bigcal\" directory \n";
      } else std::cerr << " = Error Making \"pedestals/bigcal\" directory! \n";
      if ((pedtrackerDir = pedDir->mkdir("tracker"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"pedestals/tracker\" directory \n";
      } else std::cerr << " = Error Making \"pedestals/tracker\" directory! \n";
   } else {
      std::cerr << " = Error Making \"pedestals\" directory! \n";
   }

// Timewalk results directory
   fCurrentFile->cd();
   timewalkDir = fCurrentFile->mkdir("timewalk");
   if (timewalkDir) {
      if (this->fVerbosity > 2) std::cerr << " + Created \"timewalk\" directory \n";
   } else {
      std::cerr << " = Error Making \"timewalk\" directory! \n";
   }

// detectors results directory
   fCurrentFile->cd();
   detectorsDir = fCurrentFile->mkdir("detectors");
   if (detectorsDir) {
      std::cout << " + Created \"detectors\" directory \n";

      if ((detectorsDir->mkdir("cherenkov"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"detectors/cherenkov\" directory \n";
      } else std::cerr << " = Error Making \"detectors/cherenkov\" directory! \n";
      if ((detectorsDir->mkdir("hodoscope"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"detectors/hodoscope\" directory \n";
      } else std::cerr << " = Error Making \"detectors/hodoscope\" directory! \n";
      if ((detectorsDir->mkdir("bigcal"))) {
         if (this->fVerbosity > 2)  std::cout << " + Created \"detectors/bigcal\" directory \n";
         detectorsDir->cd("bigcal");
         gDirectory->mkdir("hists");
      } else std::cerr << " = Error Making \"detectors/bigcal\" directory! \n";
      if ((detectorsDir->mkdir("tracker"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"detectors/tracker\" directory \n";
      } else std::cerr << " = Error Making \"detectors/tracker\" directory! \n";

   } else {
      std::cerr << " = Error Making \"detectors\" directory! \n";
   }

// Calibrations directory
   fCurrentFile->cd();
   calibDir = fCurrentFile->mkdir("calibrations");
   if (calibDir) {
      calibDir->cd();
      if ((calibCerDir = calibDir->mkdir("cherenkov"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"calibrations/cherenkov\" directory \n";
      } else std::cerr << " = Error Making \"calibrations/cherenkov\" directory! \n";
      if ((calibLucDir = calibDir->mkdir("hodoscope"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"calibrations/hodoscope\" directory \n";
      } else std::cerr << " = Error Making \"calibrations/hodoscope\" directory! \n";
      if ((calibBigcalDir = calibDir->mkdir("bigcal"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"calibrations/bigcal\" directory \n";
      } else std::cerr << " = Error Making \"calibrations/bigcal\" directory! \n";
      if ((calibtrackerDir = calibDir->mkdir("tracker"))) {
         if (this->fVerbosity > 2) std::cout << " + Created \"calibrations/tracker\" directory \n";
      } else std::cerr << " = Error Making \"calibrations/tracker\" directory! \n";
   } else {
      std::cerr << " = Error Making \"calibrations\" directory! \n";
   }

   fCurrentFile->cd();

}
//_____________________________________________________________________________

Int_t InSANERunManager::GeneratePreRunReport()
{
   /// There are a few steps here to generate the PRE-runreport:
   ///  - Create new (empty) report
   ///  - Add each datum object to the report
   ///  - We assume there exists some data in the table run_info from other scripts
   ///    along with other tables with useful run information
   ///  - Using RetrieveRunInfo(), gather all information for the run

   if (fVerbosity > 1) std::cout << " o InSANERunManager::GeneratePreRunReport \n";

   if (fRunReport) delete fRunReport;
   fRunReport   = new InSANERunReport(fCurrentRun);

   Bool_t isAString   = false;
   Bool_t isUpdated   = false;
   std::string        SQLdatetime, date, time;
   InSANERunReportDatum * dat = nullptr;

   fRunReport->Add(
      dat = new InSANERunReportDatum("Run_number",
                                     Form("%d", GetCurrentRun()->fRunNumber),
                                     isAString = false,
                                     isUpdated = false));

   /// AsSQLString() returns 1997-01-15 20:16:28
   /// so the i=10 splits them
   /*SQLdatetime = GetCurrentRun()->fStartDatetime.AsSQLString();
   date = SQLdatetime.substr(0,10);
   time = SQLdatetime.substr(11);
   std::cout << "start date is " << date << "\n";
   std::cout << "start time is " << time << "\n";*/
   /// AsSQLString() returns 1997-01-15 20:16:28
   /// so the i=10 splits them   SQLdatetime = fRun->fStartDatetime.AsSQLString();
   /*  SQLdatetime = GetCurrentRun()->fEndDatetime.AsSQLString();
   date = SQLdatetime.substr(0,10);
   time = SQLdatetime.substr(11);
   std::cout << "end date is " << date << "\n";
   std::cout << "end time is " << time << "\n"; */
   fRunReport->Add(dat = new InSANERunReportDatum("start_date"));
   dat->isString = true;
   fRunReport->Add(dat = new InSANERunReportDatum("start_time"));
   dat->isString = true;
   fRunReport->Add(dat = new InSANERunReportDatum("start_datetime"));
   dat->isString = true;

   fRunReport->Add(dat = new InSANERunReportDatum("end_date"));
   dat->isString = true;
   fRunReport->Add(dat = new InSANERunReportDatum("end_time"));
   dat->isString = true;
   fRunReport->Add(dat = new InSANERunReportDatum("end_datetime"));
   dat->isString = true;

   fRunReport->Add(new InSANERunReportDatum("iron_plate"));

   fRunReport->Add(new InSANERunReportDatum("beam_pol"));
   fRunReport->Add(new InSANERunReportDatum("beam_energy"));
   fRunReport->Add(new InSANERunReportDatum("beam_current"));
   fRunReport->Add(new InSANERunReportDatum("analysis_pass"));
   fRunReport->Add(new InSANERunReportDatum("halfwave_plate"));
   fRunReport->Add(new InSANERunReportDatum("pol_source_qe"));
   fRunReport->Add(new InSANERunReportDatum("beam_pass"));
   fRunReport->Add(new InSANERunReportDatum("wien_angle"));

   fRunReport->Add(new InSANERunReportDatum("hms_momentum"));
   fRunReport->Add(new InSANERunReportDatum("hms_angle"));
   fRunReport->Add(new InSANERunReportDatum("hms_sign" , "1", false, true));

   fRunReport->Add(new InSANERunReportDatum("dead_time"));

   fRunReport->Add(dat =  new InSANERunReportDatum("PS1", "1", isAString = false, isUpdated = false));
   fRunReport->Add(dat =  new InSANERunReportDatum("PS2", "1", false, false));
   fRunReport->Add(dat =  new InSANERunReportDatum("PS3", "1", false, false));
   fRunReport->Add(dat =  new InSANERunReportDatum("PS4", "1", false, false));
   fRunReport->Add(dat =  new InSANERunReportDatum("PS5", "1", false, false));
   fRunReport->Add(dat =  new InSANERunReportDatum("PS6", "1", false, false));

   fRunReport->Add(new InSANERunReportDatum("target_offline_pol"));
   fRunReport->Add(new InSANERunReportDatum("target_online_pol"));
   fRunReport->Add(dat = new InSANERunReportDatum("target_field", "0", false, true));
   fRunReport->Add(new InSANERunReportDatum("target_current"));
   fRunReport->Add(new InSANERunReportDatum("target_angle"));
   fRunReport->Add(dat =  new InSANERunReportDatum("target_insert"));
   dat->isString = true;
   fRunReport->Add(dat = new InSANERunReportDatum("target_type"));
   dat->isString = true;
   fRunReport->Add(dat = new InSANERunReportDatum("target_cup"));
   dat->isString = true;
   fRunReport->Add(dat =  new InSANERunReportDatum("target_pol_sign"));
   dat->isString = true;
   fRunReport->Add(new InSANERunReportDatum("target_start_pol"));
   fRunReport->Add(new InSANERunReportDatum("target_stop_pol"));
   fRunReport->Add(new InSANERunReportDatum("target_packing_fraction","0.59"));
   fRunReport->Add(new InSANERunReportDatum("number_of_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_coin_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_beta2_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_beta1_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_pi0_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_hms_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_LED_events"));

//    fRunReport->Print();

   /// get the run information from a database AND other databases!
   /// assigns values for each of the run report datum based upon SQL queries
   fRunReport->RetrieveRunInfo();

   /// Updates run_info database since GatherRunInfo.cxx updated the RunReport Datum
   fRunReport->UpdateRunDatabase();

   /// Fill in the InSANERun object's data with database
   fRunReport->UpdateRun();

   GenerateRunReport();

//   fRunReport->Print();

   return(0);
}
//_____________________________________________________________________________

Int_t InSANERunManager::GenerateRunReport()
{
   /// create a new run report
   if (this->fVerbosity > 1) std::cout << " o InSANERunManager::GenerateRunReport \n";

   if (fRunReport)  delete fRunReport;
   fRunReport                 = new InSANERunReport(fCurrentRun);
   InSANERunReportDatum * dat = nullptr ;

   Bool_t isAString   = false;
   Bool_t isUpdated   = false;
   std::string SQLdatetime, date, time;

   /// Begin adding the data that is going to be filled with retrieve data
   /// note we have isUpdated=false for the InSANERunDatum because they
   /// have not been changed. The entries in the database will define the

   fRunReport->Add(new InSANERunReportDatum("Run_number",     Form("%d", GetCurrentRun()->fRunNumber),
                                            isAString = false,
                                            isUpdated = false));

   /// AsSQLString() returns 1997-01-15 20:16:28
   /// so the i=10 splits them
   SQLdatetime = GetCurrentRun()->fStartDatetime.AsSQLString();
   date = SQLdatetime.substr(0, 10);
   time = SQLdatetime.substr(11);
//    std::cout << "start date is " << date << "\n";
//    std::cout << "start time is " << time << "\n";
   fRunReport->Add(new InSANERunReportDatum("start_date", date.c_str(),
                                            isAString = true));
   fRunReport->Add(new InSANERunReportDatum("start_time", time.c_str(),
                                            isAString = true));
   fRunReport->Add(new InSANERunReportDatum("start_datetime", GetCurrentRun()->fStartDatetime.AsSQLString()  ,
                                            isAString = true,
                                            isUpdated = false));

   TDatime now;
   GetCurrentRun()->fReplayDatetime = now;
   SQLdatetime = GetCurrentRun()->fReplayDatetime.AsSQLString();
   date = SQLdatetime.substr(0, 10);
   time = SQLdatetime.substr(11);
   fRunReport->Add(new InSANERunReportDatum("replay_date", date.c_str(),
                                            isAString = true,true));
   fRunReport->Add(new InSANERunReportDatum("replay_time", time.c_str(),
                                            isAString = true,true));
   fRunReport->Add(new InSANERunReportDatum("replay_datetime", GetCurrentRun()->fReplayDatetime.AsSQLString()  ,
                                            isAString = true,
                                            isUpdated = true));

   /// AsSQLString() returns 1997-01-15 20:16:28
   /// so the i=10 splits them   SQLdatetime = fRun->fStartDatetime.AsSQLString();
   SQLdatetime = GetCurrentRun()->fEndDatetime.AsSQLString();
   //date = SQLdatetime.substr(0, 10);
   //time = SQLdatetime.substr(11);
   //std::cout << "end date is " << date << "\n";
   //std::cout << "end time is " << time << "\n";
   fRunReport->Add(new InSANERunReportDatum("end_date", date.c_str(),
                                            isAString = true));
   fRunReport->Add(new InSANERunReportDatum("end_time", time.c_str(),
                                            isAString = true));
   fRunReport->Add(new InSANERunReportDatum("end_datetime",   GetCurrentRun()->fEndDatetime.AsSQLString()    ,
                                            isAString = true,
                                            isUpdated = false));
   fRunReport->Add(new InSANERunReportDatum("beam_pol",       Form("%f", GetCurrentRun()->fBeamPolarization)  ,
                                            isAString = false,
                                            isUpdated = false));
   fRunReport->Add(new InSANERunReportDatum("iron_plate",     Form("%d", GetCurrentRun()->fIronPlate)));
   fRunReport->Add(new InSANERunReportDatum("beam_energy",    Form("%f", GetCurrentRun()->fBeamEnergy)));
   fRunReport->Add(new InSANERunReportDatum("beam_current",   Form("%f", GetCurrentRun()->fAverageBeamCurrent)));
   fRunReport->Add(new InSANERunReportDatum("analysis_pass",  Form("%d", GetCurrentRun()->fAnalysisPass)));
   fRunReport->Add(new InSANERunReportDatum("halfwave_plate", Form("%d", GetCurrentRun()->fHalfWavePlate)));
   fRunReport->Add(new InSANERunReportDatum("pol_source_qe", Form("%f", GetCurrentRun()->fQuantumEfficiency)));
   fRunReport->Add(new InSANERunReportDatum("beam_pass", Form("%d", GetCurrentRun()->fBeamPassNumber)));
   fRunReport->Add(new InSANERunReportDatum("wien_angle", Form("%f", GetCurrentRun()->fWienAngle)));
   fRunReport->Add(new InSANERunReportDatum("hms_momentum",   Form("%f", GetCurrentRun()->fHMSMomentum)));
   fRunReport->Add(new InSANERunReportDatum("hms_angle",      Form("%f", GetCurrentRun()->fHMSAngle)));
   fRunReport->Add(new InSANERunReportDatum("hms_sign",       Form("%f", GetCurrentRun()->fHMSSign)));
   fRunReport->Add(new InSANERunReportDatum("dead_time",      Form("%f", GetCurrentRun()->fDeadTime)));
   fRunReport->Add(new InSANERunReportDatum("PS1",   Form("%d", GetCurrentRun()->fPreScale[0])));
   fRunReport->Add(new InSANERunReportDatum("PS2",   Form("%d", GetCurrentRun()->fPreScale[1])));
   fRunReport->Add(new InSANERunReportDatum("PS3",   Form("%d", GetCurrentRun()->fPreScale[2])));
   fRunReport->Add(new InSANERunReportDatum("PS4",   Form("%d", GetCurrentRun()->fPreScale[3])));
   fRunReport->Add(new InSANERunReportDatum("PS5",   Form("%d", GetCurrentRun()->fPreScale[4])));
   fRunReport->Add(new InSANERunReportDatum("PS6",   Form("%d", GetCurrentRun()->fPreScale[5])));

   fRunReport->Add(dat =  new InSANERunReportDatum("target_offline_pol"));
   fRunReport->Add(dat = new InSANERunReportDatum("target_online_pol"));
   fRunReport->Add(new InSANERunReportDatum("target_field"));
   fRunReport->Add(new InSANERunReportDatum("target_current"));
   fRunReport->Add(new InSANERunReportDatum("target_angle"));
   fRunReport->Add(dat =  new InSANERunReportDatum("target_insert"));
   dat->isString = true;
   fRunReport->Add(dat =  new InSANERunReportDatum("target_type"));
   dat->isString = true;
   fRunReport->Add(dat =  new InSANERunReportDatum("target_cup"));
   dat->isString = true;
   fRunReport->Add(dat =  new InSANERunReportDatum("target_pol_sign"));
   dat->isString = true;
   fRunReport->Add(new InSANERunReportDatum("target_start_pol"));
   fRunReport->Add(new InSANERunReportDatum("target_stop_pol"));
   fRunReport->Add(new InSANERunReportDatum("target_packing_fraction"));
   fRunReport->Add(new InSANERunReportDatum("number_of_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_coin_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_beta2_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_beta1_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_pi0_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_hms_events"));
   fRunReport->Add(new InSANERunReportDatum("number_of_LED_events"));

//    fCurrentRun->fBeamPolarization = atof(fRunReport->GetValueOf("beam_pol"));
//    fCurrentRun->fBeamEnergy = atof(fRunReport->GetValueOf("beam_energy"));
//    fCurrentRun->fAverageBeamCurrent = atof(fRunReport->GetValueOf("beam_current"));
//    fCurrentRun->fHalfWavePlate = atoi(fRunReport->GetValueOf("halfwave_plate"));
//    fCurrentRun->fAnalysisPass = atoi(fRunReport->GetValueOf("analysis_pass"));
   // Target
   /*   const char * tgttype  = fRunReport->GetValueOf("target_type");
      fCurrentRun->fTarget = InSANERUN::kUNKNOWN;
      for(int k =0;k<6;k++) if( !strcmp(tgttype,kgSANETargets[k]) ) fCurrentRun->fTarget = fTargetByNumber[k];*/
//    fCurrentRun->fTargetAngle = atof(fRunReport->GetValueOf("target_angle"));
//    fCurrentRun->fTargetCurrent = atof(fRunReport->GetValueOf("target_field"));
//    fCurrentRun->fStartingPolarization = atof(fRunReport->GetValueOf("target_start_pol"));
//    fCurrentRun->fEndingPolarization = atof(fRunReport->GetValueOf("target_stop_pol"));
//    fCurrentRun->fTargetOnlinePolarization = atof(fRunReport->GetValueOf("target_online_pol"));
//    fCurrentRun->fTargetOfflinePolarization = atof(fRunReport->GetValueOf("target_offline_pol"));
//
   /*   if(fVerbosity > 1) fRunReport->Print();*/
// set the values for this run from the run report
// Beam
   fRunReport->Update();
//    fCurrentRun->Print();

   return(0);
}
//_____________________________________________________________________________
