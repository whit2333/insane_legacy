#include "BigcalPi0Calibrator.h"
#include "SANERunManager.h"

ClassImp(Pi0PhotonEnergy)

//_______________________________________________________//

ClassImp(Pi0MassSquared)

//_______________________________________________________//

ClassImp(BigcalPi0Calibrator)

BigcalPi0Calibrator::BigcalPi0Calibrator(const char * newTreeName, const char * uncorrectedTreeName) :
SANEClusteringAnalysis(uncorrectedTreeName),  InSANEAnalyzer(newTreeName, uncorrectedTreeName)
{
   if (SANERunManager::GetRunManager()->GetVerbosity() > 0) std::cout << " o BigcalPi0Calibrator Constructor\n";
   /// Go to new file
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   fInputTree = nullptr;
   if (fAnalysisTree) fInputTree = fAnalysisTree; // anlysis tree set in InSANEDetectorAnalysis
   if (!fInputTree) fInputTree = (TTree*)gROOT->FindObject(uncorrectedTreeName);
   if (!fInputTree) printf("x- Tree:%s  was NOT FOUND! (from SANEFirstPassAnalyzer c'tor)  \n", uncorrectedTreeName);

   fScalerTreeName = "Scalers1";
   TFile * sf = InSANERunManager::GetRunManager()->GetScalerFile();
   if (sf) sf->cd();
   else std::cout << " NO SCALER FILE ?!?!?!\n";
   if (!fScalerTree) fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName.Data());
   fScalers->SetBranches(fScalerTree);
   fOutputScalerTreeName = "ScalersPi0";

   fOutputFile = SANERunManager::GetRunManager()->GetCurrentFile();

}
//_______________________________________________________//

BigcalPi0Calibrator::~BigcalPi0Calibrator()
{
   if (fOutputFile)fOutputFile->Flush();
   if (fOutputFile)fOutputFile->Write();
};
//_______________________________________________________//

void BigcalPi0Calibrator::Initialize(TTree * inTree)
{

   if (SANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o SANEFirstPassAnalyzer::Initialize(TTree *) \n";

   if (inTree)fInputTree = inTree;

   fClusterEvent = fEvents->SetClusterBranches(fInputTree);
   fClusters = fClusterEvent->fClusters;
   fCalculatedTree = fInputTree->CloneTree(0);
   fCalculatedTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s", fCalculatedTreeName.Data()));

//   if(fScalerTree) fOutputScalerTree = fScalerTree->CloneTree(0);
//     fOutputScalerTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s",fCalculatedTreeName.Data() ));
   SANERunManager::GetRunManager()->GetScalerFile()->cd();
   if (fScalerTree) fOutputScalerTree = fScalerTree->CloneTree(0);
   if (fOutputScalerTree) {
      fOutputScalerTree->SetNameTitle(fOutputScalerTreeName.Data(), Form("A corrected tree: %s", fOutputScalerTreeName.Data()));
      std::cout << " o Created output scaler tree, " << fOutputScalerTreeName.Data() << "\n";
   }


   InitCorrections();

   InitCalculations();

   /// Important: Set the event to be used for triggering in the InSANEAnalyzer
   InSANEAnalyzer::SetTriggerEvent(fScalers->fTriggerEvent);
   InSANEAnalyzer::SetDetectorTriggerEvent(fEvents->TRIG);
   InSANEAnalyzer::SetScalerTriggerEvent(fScalers->fTriggerEvent);

   //if(inTree) InSANEAnalyzer::fInputTree = inTree;

   //InitCorrections();

   //InitCalculations();
}
//_______________________________________________________//

void BigcalPi0Calibrator::MakePlots()
{
   //fClusterEvent->Draw("bigcalClusters.fCherenkovTotalNPE>>npe(400,0,40)","bigcalClusters.fNumberOfMirrors==1")

}

