#include "Pi0ClusterPair.h"

ClassImp(Pi0ClusterPair)

//______________________________________________________________________________
Pi0ClusterPair::Pi0ClusterPair() : fAngle(0.0), fMass(0.0), fMomentum(0.0), 
   fTheta(0.0), fPhi(0.0), fEnergy(0.0), fHelicity(0) 
{ }
//______________________________________________________________________________
Pi0ClusterPair::Pi0ClusterPair(const BIGCALCluster& c1, const BIGCALCluster& c2) : 
   fAngle(0.0), fMass(0.0), fMomentum(0.0), fTheta(0.0), fPhi(0.0), 
   fEnergy(0.0), fHelicity(0), fCluster1(c1), fCluster2(c2)
{ }
//______________________________________________________________________________
Pi0ClusterPair::Pi0ClusterPair(BIGCALCluster* c1, BIGCALCluster* c2) : 
   fAngle(0.0), fMass(0.0), fMomentum(0.0), fTheta(0.0), fPhi(0.0), 
   fEnergy(0.0), fHelicity(0), fCluster1(*c1), fCluster2(*c2)
{
   // deprecated
   //if (c1) fCluster1 = (*c1);
   //if (c2) fCluster2 = (*c2);
}
//______________________________________________________________________________
Pi0ClusterPair::~Pi0ClusterPair(){
}
//______________________________________________________________________________
const Pi0ClusterPair& Pi0ClusterPair::operator=(const Pi0ClusterPair& rhs){
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   TObject::operator=(rhs);
   fAngle    = rhs.fAngle    ;
   fMass     = rhs.fMass     ;
   fMomentum = rhs.fMomentum ;
   fTheta    = rhs.fTheta    ;
   fPhi      = rhs.fPhi      ;
   fEnergy   = rhs.fEnergy   ;
   fHelicity = rhs.fHelicity   ;
   fCluster1 = rhs.fCluster1;
   fCluster2 = rhs.fCluster2;
   return *this;
}
//______________________________________________________________________________
void Pi0ClusterPair::Clear(Option_t * opt) {
   fAngle    = 0.0;
   fMass     = 0.0;
   fMomentum = 0.0;
   fTheta    = 0.0;
   fPhi      = 0.0;
   fEnergy   = 0.0;
   fHelicity = 0;
   fCluster1.Clear(opt);
   fCluster2.Clear(opt);
}
//______________________________________________________________________________

