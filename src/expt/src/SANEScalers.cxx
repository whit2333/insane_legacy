#define SANEScalers_cxx
#include "SANEScalers.h"
#include <iostream>
#include "InSANERunManager.h"

ClassImp(SANEScalers)

//______________________________________________________________________________
SANEScalers::SANEScalers(const char * treename): InSANEAnalysisEvents(treename) {
   fTriggerEvent = new InSANETriggerEvent();
   fScalerEvent  = new SANEScalerEvent();
   //fScalerEvent->SetNumberOfScalers(352);
   fScalerEvent->SetNumberOfScalers(582);
   //printf("  - Setting Scaler's Branch Addresses\n");
   // Try finding the tree and setting the branches 
   if(!SetBranches()) if(InSANERunManager::GetRunManager()->GetVerbosity() > 1)  std::cout << "  + Branches Set\n";
}
//______________________________________________________________________________
SANEScalers::~SANEScalers() {
}
//______________________________________________________________________________
void SANEScalers::ClearEvent(Option_t * opt){
   fTriggerEvent->ClearEvent();
   fScalerEvent->ClearEvent();
}
//______________________________________________________________________________
void SANEScalers::FillTrees() {
   if (fTree)fTree->Fill();
   //   if(betaTree)betaTree->Fill();
   //   if(beamTree)beamTree->Fill();
   //   if(hmsTree)hmsTree->Fill();
   //   if(mcTree)mcTree->Fill();
}
//______________________________________________________________________________
Int_t SANEScalers::SetBranches() {
   Int_t returnvalue = 0;
   //Int_t setbrval=0;
   /// fTree is the Scaler Tree
   fTree = (TTree *)gROOT->FindObject(GetName());
   if (fTree) {
      //     TBranch * gscalersBranchEvent = fTree->GetBranch("fEventNumber");
      //     if(gscalersBranchEvent) {
      //       gscalersBranchEvent->SetAddress( &(fScalerEvent->fEventNumber) );
      //       std::cout << "   - Set branch fEventNumber \n" ;
      //
      //     } else {
      //       fTree->Branch( "fEventNumber",&fScalerEvent,"fEventNumber/L");
      //       std::cout << " o Created branch fEventNumber \n" ;
      //     }

      //     TBranch * gscalersBranch= fTree->GetBranch("fScalers");
      //     if(gscalersBranch) {
      //       gscalersBranch->SetAddress((fScalerEvent->fScalers));
      //       std::cout << "   - Set branch fEventNumber \n" ;
      //
      //     } else {
      //       std::cout << " x ERROR: Branch fScalers Should exist\n" ;
      // //       fTree->Branch( "saneScalerEvent","SANEScalerEvent",&fScalerEvent);
      //     }

      TBranch * scalerBranch = fTree->GetBranch("saneScalerEvent");
      if (scalerBranch) {
         scalerBranch->SetAddress(&fScalerEvent);
         if(InSANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << "  - Set branch saneScalerEvent \n" ;

      } else {
         if(InSANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o Created branch saneScalerEvent \n" ;
         fTree->Branch("saneScalerEvent", "SANEScalerEvent", &fScalerEvent);
      }

      TBranch * scalersTriggerEvent = fTree->GetBranch("triggerEvent");
      if (scalersTriggerEvent) {
         scalersTriggerEvent->SetAddress(&fTriggerEvent);
         if(InSANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << "   - Set branch triggerEvent \n" ;
      } else {
         if(InSANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o Created branch triggerEvent\n" ;
         fTree->Branch("triggerEvent", "InSANETriggerEvent", &fTriggerEvent);
      }

   } else {
      Warning("SetBranches()","Could not find tree %s in working directory.",GetName());
      returnvalue += 1;
   }
   return(returnvalue);
}
//______________________________________________________________________________
Int_t SANEScalers::SetBranches(TTree * t) {

   Int_t returnvalue = 0;
   //Int_t setbrval=0;
   /// fTree is the Scaler Tree
   if (!t) return (-1);
   fTree = t;
   if(fTree) {

      TBranch * scalerBranch = fTree->GetBranch("saneScalerEvent");
      if (scalerBranch) {
         scalerBranch->SetAddress(&fScalerEvent);
         if(InSANERunManager::GetRunManager()->GetVerbosity() > 1){
            std::cout << "  - Set branch saneScalerEvent \n"; 
         }

      } else {
         if(InSANERunManager::GetRunManager()->GetVerbosity() > 1) {
            std::cout << " o Created branch saneScalerEvent \n" ;
         }
         fTree->Branch("saneScalerEvent", "SANEScalerEvent", &fScalerEvent);
      }

      TBranch * scalersTriggerEvent = fTree->GetBranch("triggerEvent");
      if (scalersTriggerEvent) {
         scalersTriggerEvent->SetAddress(&fTriggerEvent);
         if(InSANERunManager::GetRunManager()->GetVerbosity() > 1) {
            std::cout << "   - Set branch triggerEvent \n";
         }
      } else {
         if(InSANERunManager::GetRunManager()->GetVerbosity() > 1) {
            std::cout << " o Created branch triggerEvent\n" ;
         }
         fTree->Branch("triggerEvent", "InSANETriggerEvent", &fTriggerEvent);
      }
   } else {
      Error("SetBranches(TTree*)","Could not find tree %s",GetName());
      returnvalue += 1;
   }

   return(returnvalue);
}
//______________________________________________________________________________
Int_t SANEScalers::GetEvent(Long64_t event) {
   if (fTree) fTree->GetEvent(event);
   else printf(" no fTree \n");
   return(0);
}
//______________________________________________________________________________

