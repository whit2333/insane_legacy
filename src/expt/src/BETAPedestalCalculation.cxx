#include "BETAPedestalCalculation.h"


BETAPedestalCalculation::BETAPedestalCalculation() {
   
   InSANERunManager * runman = InSANERunManager::GetRunManager();
   //fRunNumber = runman->fCurrentRun->GetRunNumber();
   //InSANERun* aRun = runman->fCurrentRun; //runman->GetRunObject();
   // dbManager->PrintTables();
   fAnalysisFile = runman->GetCurrentFile();
/*   fEvents = new SANEEvents(sourceTreeName);*/
}
//_____________________________________________________________________________

BETAPedestalCalculation::~BETAPedestalCalculation() {
}
//_____________________________________________________________________________

Int_t BETAPedestalCalculation::Initialize() {
   BETACalculation::Initialize();

   fAnalysisFile->cd("");
   fAnalysisFile->cd("pedestals");

   fBigcalPeds = new TClonesArray("InSANEDetectorPedestal", 32 * 32 + 30 * 24);
   fCherenkovPeds = new TClonesArray("InSANEDetectorPedestal", 12);
   fLucitePeds = new TClonesArray("InSANEDetectorPedestal", 56);
   fTrackerPeds = new TClonesArray("InSANEDetectorPedestal", 56);

   fAnalysisFile->cd("pedestals/cherenkov");
   fCherenkovPedHists = new TClonesArray("TH1F", 12);
   for (int jj = 0; jj < 12; jj++)
      new((*fCherenkovPedHists)[jj]) TH1F(Form("CerPed%d", jj + 1), Form("Cherenkov %d", jj + 1), 200, 1, 1501);


// Hodoscope
   /*   TDirectory * lucPedDir = fAnalysisFile->mkdir("HodoscopeHists");*/
   fAnalysisFile->cd("pedestals/hodoscope");
   fLucitePedHists = new TClonesArray("TH1F", 56);
   for (int jj = 0; jj < 56; jj++)
      new((*fLucitePedHists)[jj]) TH1F(Form("LucPed%d", jj + 1), Form("Lucite %d", jj + 1), 200, 1, 2001);

   fAnalysisFile->cd("");

// Bigcal
   /*   TDirectory * bcPedDir = fAnalysisFile->mkdir("BigcalHists");*/
   fAnalysisFile->cd("pedestals/bigcal");
   fBigcalPedHists = new TClonesArray("TH1F", 32 * 32 + 30 * 24);
   for (int jj = 0; jj < 32 * 32 + 30 * 24; jj++)
      new((*fBigcalPedHists)[jj]) TH1F(Form("BigcalPed%d", jj + 1), Form("Bigcal %d", jj + 1), 200, 1, 1001);

   fAnalysisFile->cd("");

// Tracker
   /*   TDirectory * trackerPedDir = fAnalysisFile->mkdir("TrackerHists");*/
   fAnalysisFile->cd("pedestals/tracker");
   fTrackerPedHists = new TClonesArray("TH1F", 64 + 2 * 128);
   for (int jj = 0; jj < 64 + 2 * 128; jj++)
      new((*fTrackerPedHists)[jj]) TH1F(Form("TrackerPed%d", jj + 1), Form("Tracker %d", jj + 1), 200, 1, 1001);

   fAnalysisFile->cd("");

   fOutputTree = new TTree("BETAPedestals", "Detector Pedestals Data");
   fOutputTree->Branch("CherenkovPedestalArray", &fCherenkovPeds);
   fOutputTree->Branch("LucitePedestalArray", &fLucitePeds);
   fOutputTree->Branch("BigcalPedestalArray", &fBigcalPeds);

   bcgeo = BIGCALGeometryCalculator::GetCalculator();
   /// \todo {Implement DETECTORPhysicsEvent }
   fAnalysisFile->cd();


   //TH2F * h2 = 0;
//       h2 = new TH2F("hTrackerVsBigcal_i","Tracker-i vs Bigcal-i",32,1,33,73,0,73);
//       f2DHistograms.Add(h2);
//       h2 = new TH2F("hTrackerVsBigcal_j","Tracker-j vs Bigcal-j",56,1,57,133,0,133);
//       f2DHistograms.Add(h2);
   return(0);
}
//_____________________________________________________________________________

Int_t BETAPedestalCalculation::Calculate(){

      /// Begin Loop over Cherenkov signals
      if (fEvents->TRIG->IsPedestalEvent()) {

         for (int kk = 0; kk < fEvents->BETA->fGasCherenkovEvent->fNumberOfHits; kk++) {
            aCerHit = (GasCherenkovHit*)(*fCerHits)[kk] ;
            ((TH1F*)(*fCherenkovPedHists)[aCerHit->fMirrorNumber-1])->Fill(aCerHit->fADC);
         } // End loop over Cherenkov signals

// Begin Loop over Lucite signals
//       for (int kk=0; kk< fEvents->BETA->fLuciteHodoscopeEvent->fNumberOfHits;kk++)
//       {
//         aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;
//         ((TH1F*)(*fLucitePedHists)[aLucHit->fPMTNumber-1])->Fill(aLucHit->fADC);
//       } // End loop over Lucite signals

         /// Begin Loop over Bigcal signals
         for (int kk = 0; kk < fEvents->BETA->fBigcalEvent->fBigcalHits->GetEntries(); kk++) {
            aBigcalHit = (BigcalHit*)(*fBigcalHits)[kk] ;
            if (aBigcalHit->fTDCLevel == -1) {
               ((TH1F*)(*fBigcalPedHists)[bcgeo->GetBlockNumber(aBigcalHit->fiCell, aBigcalHit->fjCell)-1])->Fill(aBigcalHit->fADC);
            }
         }
      } // ped trigger bit

   return(0);
}
//_____________________________________________________________________________

Int_t BETAPedestalCalculation::CreatePlots()
{
   InSANERunManager * runman = InSANERunManager::GetRunManager();
   Int_t runnum = runman->GetCurrentRun()->GetRunNumber();
   TF1* funcPointer = nullptr;
   TCanvas * c1 = nullptr;
   TH1F * h = nullptr;
   const Int_t kNotDraw = 1 << 9;

   c1  = new TCanvas("CerPedAnalysisCanvas", "Cherenkov Pedestals", 800, 600);
   c1->Divide(4, 3);

   for (int jj = 0; jj < 12; jj++) {
      h = (TH1F*)(*fCherenkovPedHists)[jj];
      c1->cd(jj + 1);
      if (h) if ( (funcPointer = h->GetFunction("f1")) )funcPointer->ResetBit(kNotDraw);
      if (h) h->Draw();
   }
   c1->SaveAs(Form("plots/%d/cerPeds.jpg",runnum ));
   c1->SaveAs(Form("plots/%d/cerPeds.ps", runnum));
   delete c1;

// Lucite Pedestals
//   c1  = new TCanvas("LucPedAnalysisCanvas","Lucite Pedestals",800,600);
//   c1->Divide(10,6);
//   for(int jj =0;jj<56;jj++) {
//     h = (TH1F*)(*fLucitePedHists)[jj];
//     c1->cd(jj+1);
//     if(h) if( funcPointer = h->GetFunction("f1") )funcPointer->ResetBit(kNotDraw);
//     if(h) h->Draw();
//   }
//   c1->SaveAs(Form("plots/%d/lucPeds.jpg",fRunNumber));
//   c1->SaveAs(Form("plots/%d/lucPeds.ps",fRunNumber));
//   delete c1;

// BIGCAL Pedestals. Each plot is for entire row of ADCs
   c1 = new TCanvas("BigcalPedAnalysisCanvas", "Bigcal Pedestals by row", 800, 600);
   c1->Divide(4, 8);
   for (int ii = 0; ii < 56; ii++) {

//     for(int jj =0;jj<32;jj++) {
//       c1->cd(jj+1);
//       h = (TH1F*)(*fBigcalPedHists)[jj+ii*32];
//       if(h)if( h->GetFunction("f1")  )h->GetFunction("f1")->ResetBit(kNotDraw);
//       if(h) h->Draw();
//     }
      if (ii < 32) {
         for (int jj = 0; jj < 32; jj++) {
            h = (TH1F*)(*fBigcalPedHists)[jj+ii*32];
            if (jj % 32 == 0)c1->cd(ii + 1);
            if (h) if( (funcPointer = h->GetFunction("f1")) )funcPointer->ResetBit(kNotDraw);
            if (jj % 32 == 0) h->Draw();
            else h->Draw("same");
         }
      } else {
         for (int jj = 0; jj < 30; jj++) {
            h = (TH1F*)(*fBigcalPedHists)[jj+(ii-32)*30+32*32];
            if (jj % 30 == 0)c1->cd(ii + 1);
            if (h) if( (funcPointer = h->GetFunction("f1")) )funcPointer->ResetBit(kNotDraw);
            if (jj % 30 == 0) h->Draw();
            else h->Draw("same");
         }
      }
   }
   c1->SaveAs(Form("plots/%d/bigcalPedsByRow.jpg", runnum));
   c1->SaveAs(Form("plots/%d/bigcalPedsByRow.ps", runnum));
   delete c1;

   return(0);
}
//_____________________________________________________________________________

Int_t BETAPedestalCalculation::Finalize() {

   CreatePlots();


   auto *f1 = new TF1("f1", "gaus", 0, 1000);
   //TF1* fc = new TF1("fc", "[0]*TMath::CauchyDist(x, [2], [1])+[3]", -8500, 8000); // couchy is lorentz

   TH1F * h;
   f1->SetParLimits(0, 0.0, 500000000.0); // amplitude
   f1->SetParLimits(1, 0.0, 2000.0); // mean range
   f1->SetParLimits(2, 0.0, 200.0); // width range
   Double_t startingwidth = 90.0;

   std::cout << " o Fitting ADC pedestals for Cherenkov \n";

   for (int jj = 0; jj < 12; jj++) {
// std::cout << " o Fitting ADC pedestal for Cherenkov " << jj << "\n";

      h = (TH1F*)(*fCherenkovPedHists)[jj];
      f1->SetParameter(2, startingwidth); // widthrange
      h->Fit("f1", "M,E,0,Q,WW");

      new((*fCherenkovPeds)[jj]) InSANEDetectorPedestal(jj, f1->GetParameter(1), f1->GetParameter(2));
   }
// std::cout << " o Fitting ADC pedestals for Hodoscope \n";
//
//   for(int jj =0;jj<56;jj++) {
// // std::cout << " o Fitting ADC pedestal for Hodoscope TDC " << jj << "\n";
//
//     h = (TH1F*)(*fLucitePedHists)[jj];
//     f1->SetParameter(2,startingwidth); // widthrange
//     h->Fit("f1", "M,E,0,Q,WW");
//     new((*fLucitePeds)[jj]) InSANEDetectorPedestal(jj,f1->GetParameter(1),f1->GetParameter(2));
//   }

   std::cout << " o Fitting ADC pedestals for Bigcal \n";

   for (int ii = 0; ii < 32; ii++) {
// std::cout << " o Fitting ADC pedestal for Bigcal Row " << ii << " at column...\n   ";

      f1->SetParameter(1, 10.0);
      for (int jj = 0; jj < 32; jj++) {
// std::cout << " " << jj << ".";

         h = (TH1F*)(*fBigcalPedHists)[jj+ii*32];
         f1->SetParameter(2, startingwidth); // widthrange
         h->Fit("f1", "M,E,0,Q,WW");
         new((*fBigcalPeds)[jj+ii*32]) InSANEDetectorPedestal(jj + ii * 32 + 1, f1->GetParameter(1), f1->GetParameter(2));
      }
// std::cout << "\n";

   }
   for (int ii = 32; ii < 56; ii++) {
      f1->SetParameter(1, 10.0);
      for (int jj = 0; jj < 30; jj++) {
         h = (TH1F*)(*fBigcalPedHists)[jj+(ii-32)*30 +32*32];
         f1->SetParameter(2, startingwidth); // widthrange
         h->Fit("f1", "M,E,0,Q,WW");
         new((*fBigcalPeds)[jj+(ii-32)*30 +32*32]) InSANEDetectorPedestal(
            jj + (ii - 32) * 30 + 32 * 32 + 1, f1->GetParameter(1), f1->GetParameter(2));
      }
// std::cout << "\n";

   }

   fOutputTree->Fill();

   FillDatabase();

   return(0);
}
//_____________________________________________________________________________

Int_t BETAPedestalCalculation::FillDatabase()
{
   InSANEDatabaseManager * dbManager = InSANEDatabaseManager::GetManager() ;
   InSANERunManager * runman = InSANERunManager::GetRunManager();
   Int_t runnum = runman->GetCurrentRun()->GetRunNumber();
   // TSQLServer * db = dbManager->dbServer;
   for (int jj = 0; jj < 12; jj++) {
      dbManager->ExecuteInsert(((InSANEDetectorPedestal*)(*fCherenkovPeds)[jj])->GetSQLInsertString("cherenkov", runnum));
//db->Query(((InSANEDetectorPedestal*)(*fCherenkovPeds)[jj])->GetSQLInsertString("cherenkov",fRunNumber)->Data()) ;
   }
//   for(int jj =0;jj<56;jj++) {
// db->Query(((InSANEDetectorPedestal*)(*fLucitePeds)[jj])->GetSQLInsertString("hodoscope",fRunNumber)->Data()) ;
//   }
   for (int jj = 0; jj < 32 * 32 + 24 * 30; jj++) {
      dbManager->ExecuteInsert(((InSANEDetectorPedestal*)(*fBigcalPeds)[jj])->GetSQLInsertString("bigcal", runnum));
//db->Query(((InSANEDetectorPedestal*)(*fBigcalPeds)[jj])->GetSQLInsertString("bigcal",fRunNumber)->Data()) ;
   }
   dbManager->CloseConnections();
   return(0);
}


