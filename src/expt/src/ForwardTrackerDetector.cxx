#include "ForwardTrackerDetector.h"

ClassImp(ForwardTrackerScintillator)

ClassImp(ForwardTrackerDetector)

//______________________________________________________________________________
ForwardTrackerDetector::ForwardTrackerDetector(Int_t runnum) {

   if (InSANERunManager::GetRunManager()->fVerbosity > 1)
      printf(" o ForwardTrackerDetector::Constructor \n");

   SetName("tracker");
   SetTitle("Forward tracker");

   fCurrentRunNumber   = runnum;

   fGeoCalc            = ForwardTrackerGeometryCalculator::GetCalculator();
   fNumberOfPedestals  = 0;
   fNumberOfTDCs       = fGeoCalc->fNScints;;

   fTypicalTDCPeak      = -6200;
   fTypicalTDCPeakWidth = 100;
   cTDCMin              = -6600;
   cTDCMax              = -5000;
   cTDCAlignMin         = -200;
   cTDCAlignMax         = 200;

   // Pedestals
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals");
   }
   //fPedestals = new TClonesArray("InSANEDetectorPedestal",fNumberOfPedestals);
   for (int i = 0; i < fNumberOfPedestals; i++) {
      GetPedestals()->Add(new InSANEDetectorPedestal());
   //trackerPedestals[i] = (InSANEDetectorPedestal*)(fPedestals)[i];
   }


   // Timings
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("timing");
   }
   //fTimings  = new TClonesArray("InSANEDetectorTiming",fNumberOfTDCs);
   for (int i = 0; i < fNumberOfTDCs; i++) {
      GetTimings()->Add(new InSANEDetectorTiming());
   //trackerTimings[i] = (InSANEDetectorTiming*)(fTimings)[i];
   }

   // Detectors
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors/tracker");
   }

   fEventHitsY1 = new TH2F("trackerHitsy1", "Tracker y1", 1, 1, 2, 132, 1, 133);
   fEventHitsY2 = new TH2F("trackerHitsy2", "Tracker y2", 1, 1, 2, 132, 1, 133);
   fEventHitsX  = new TH2F("trackerHitsx", "Tracker x", 72, 1, 73, 1, 1, 2);

   if (InSANERunManager::GetRunManager()->fVerbosity > 1)
      printf(" o ForwardTrackerDetector Set run\n");

   SetRun(runnum);

   for (int i = 0; i < 128; i++) {
      fLayer1PositionCounts[i] = new std::vector<Int_t>;
      fLayer1PositionCounts[i]->clear();
      fLayer2PositionCounts[i] = new std::vector<Int_t>;
      fLayer2PositionCounts[i]->clear();
   }


   if (InSANERunManager::GetRunManager()->fVerbosity > 1)
      printf(" o End of ForwardTrackerDetector Constructor \n");

}
//______________________________________________________________________________
ForwardTrackerDetector::~ForwardTrackerDetector() {
   for (int i = 0; i < 128; i++) {
      delete fLayer1PositionCounts[i];
      delete fLayer2PositionCounts[i];
   }
   delete fEventHitsY1 ; 
   delete fEventHitsY2 ; 
   delete fEventHitsX  ;
}
//______________________________________________________________________________
Int_t ForwardTrackerDetector::SetRun(Int_t runnumber) {

   if (runnumber == 0) return(1);
   fCurrentRunNumber = runnumber;

   // Use nice parallel calibration
   if (InSANERunManager::GetRunManager()->IsPerpendicularRun()) {
      runnumber = 72930;
   }
   std::cout << " Bad tracker timing for channels(peak): " ;
   for (int i = 0; i < fNumberOfTDCs; i++) {
      InSANEDetectorTiming * atiming =  GetDetectorTiming(i);
      atiming->GetTiming("tracker", runnumber, i);
      Double_t peak = atiming->fTDCPeak;
      if( CheckTiming(atiming ) ) std::cout << i+1 << "(" << peak << "),";
      //trackerPedestals[i]->GetPedestal("tracker",runnumber,i);
   }
   std::cout << std::endl;

   InSANEDatabaseManager::GetManager()->CloseConnections();
   return(0);
}
//______________________________________________________________________________
Int_t ForwardTrackerDetector::CorrectEvent() {
   if (!fEvent) {
      std::cout << "Must set detector event address\n";
      return(-1);
   }
   return(0);
}
//______________________________________________________________________________
Int_t ForwardTrackerDetector::ProcessEvent() {
   if (fEvent) {

      ForwardTrackerHit* aTrackerHit = nullptr;
      //if( ((ForwardTrackerEvent*)fEvent)->fTrackerHits->GetEntries()>200){
      // if(((ForwardTrackerEvent*)fEvent)->fTrackerHits) 
      //    std::cout << " o before fTrackerHits->GetEntries() = " << ((ForwardTrackerEvent*)fEvent)->fTrackerHits->GetEntries() << "\n";
      // if(((ForwardTrackerEvent*)fEvent)->fTrackerHits) 
      //    std::cout << " o before fTrackerHits->GetEntriesFast() = " << ((ForwardTrackerEvent*)fEvent)->fTrackerHits->GetEntriesFast() << "\n";
      // if(((ForwardTrackerEvent*)fEvent)->fTrackerHits) 
      //    std::cout << " o before fTrackerHits->GetSize() = " << ((ForwardTrackerEvent*)fEvent)->fTrackerHits->GetSize() << "\n";
      //}
      
      for (int kk = 0; kk < ((ForwardTrackerEvent*)fEvent)->fTrackerHits->GetEntries(); kk++) {
         aTrackerHit = (ForwardTrackerHit*)(*(((ForwardTrackerEvent*)fEvent)->fTrackerHits))[kk] ;
         auto * aScint = (ForwardTrackerScintillator*)GetComponent(aTrackerHit->fChannel - 1);
         if(aScint)aScint->AddHit(aTrackerHit);
         //if(aTrackerHit->fScintLayer==0) {
         //  if(fEventHitsX) fEventHitsX->Fill(aTrackerHit->fRow,1);
         //} else if(aTrackerHit->fScintLayer==2 ) {
         //  fEventHitsY2->Fill(1,aTrackerHit->fRow);
         //} else if( aTrackerHit->fScintLayer==1) {
         //  fEventHitsY1->Fill(1,aTrackerHit->fRow);
         //}
         //cout << "tracker layer" << aTrackerHit->fScintLayer << " " << aTrackerHit->fTDC << "\n";
         //cout << "tracker row  " << aTrackerHit->fRow << " " << aTrackerHit->fPosition << "\n";
      }
      ProcessPositionHits();
   }
   return(0);
}
//______________________________________________________________________________
Int_t ForwardTrackerDetector::ProcessPositionHits() {
   if (fEvent) {
      ForwardTrackerPositionHit * aTrackerPosHit = nullptr;
      for (int kk = 0; kk < ((ForwardTrackerEvent*)fEvent)->fTrackerPositionHits->GetEntries(); kk++) {
         aTrackerPosHit = (ForwardTrackerPositionHit*)(*(((ForwardTrackerEvent*)fEvent)->fTrackerPositionHits))[kk] ;
         if (aTrackerPosHit->fScintLayer == 1) {
            Int_t Ns = aTrackerPosHit->fChannel - 1 - 64;
            if (Ns < 128 && Ns >= 0)fLayer1PositionCounts[Ns]->push_back(aTrackerPosHit->fChannel);
         }
         if (aTrackerPosHit->fScintLayer == 2) {
            Int_t Ns = aTrackerPosHit->fChannel - 1 - 64 - 128;
            if (Ns < 128 && Ns >= 0)fLayer2PositionCounts[Ns]->push_back(aTrackerPosHit->fChannel);
         }
      }
   }
   return(0);
}
//______________________________________________________________________________
Int_t ForwardTrackerDetector::Build() {
   ForwardTrackerScintillator * aScint = nullptr;
   for (int i = 0; i < fGeoCalc->fNScints; i++) {
      aScint = new ForwardTrackerScintillator(i + 1);
      aScint->SetTiming(-6000, 500);
      AddComponent(aScint);
   }
   SetBuilt();
   return(0);
}
//______________________________________________________________________________
Int_t ForwardTrackerDetector::ClearEvent() {
   TList * comps = GetComponents();
   for (int i = 0; i < comps->GetEntries(); i++) {
      ((ForwardTrackerScintillator*)comps->At(i))->ClearEvent();
   }
   //if(fEvent)      ((ForwardTrackerEvent*)fEvent)->ClearEvent("");
   if(fEventHitsY1)fEventHitsY1->Reset();
   if(fEventHitsY2)fEventHitsY2->Reset();
   if(fEventHitsX) fEventHitsX->Reset();
   for (int i = 0; i < 128; i++) {
      fLayer1PositionCounts[i]->clear();
      fLayer2PositionCounts[i]->clear();
   }
   return(0);
}
//______________________________________________________________________________


//______________________________________________________________________________

