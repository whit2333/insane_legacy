#define LocalMaxClusterProcessor_cxx
#include "LocalMaxClusterProcessor.h"

#include <iostream>

ClassImp(LocalMaxClusterProcessor)
//___________________________________________________________________

LocalMaxClusterProcessor::LocalMaxClusterProcessor()
{
   fNClusters = 0;
   fMaxNClusters     = 10;
   fMinimumEnergy    = 400.0;

   fGeoCalc = BIGCALGeometryCalculator::GetCalculator();
   offset = -2;
}
//___________________________________________________________________

LocalMaxClusterProcessor::~LocalMaxClusterProcessor()
{

}
//___________________________________________________________________

Int_t LocalMaxClusterProcessor::FindPeaks()
{
   peaksfound = 0;
   fQuit = false;
   fiPeaks->clear();
   fjPeaks->clear();
   // copy the source histogram. This copy will be modified
   /*  fSourceCopy = (TH2F *) fSourceHistogram->Clone("clusterSourceCopy");*/
   fSourceCopy->GetMaximumBin(xpos, ypos, zpos);
   while (!fQuit) {
      fSourceCopy->GetMaximumBin(xpos, ypos, zpos);
      //cout << "peak " << xpos << " " << ypos << " \n";
      tempMax = fSourceCopy->GetBinContent(xpos, ypos);
      if (tempMax > fMinimumEnergy && peaksfound < fMaxNClusters) {
         fiPeaks->push_back((double)fSourceCopy->GetXaxis()->GetBinCenter(xpos)) ;
         fjPeaks->push_back((double)fSourceCopy->GetYaxis()->GetBinCenter(ypos)) ;
         peaksfound++;
         fSourceCopy->SetMaximum(tempMax);
         for (int n = offset ; n < 5 + offset ; n++)
            for (int m = offset ; m < 5 + offset ; m++)
               if( ((( (ypos + n <= 56) && (ypos + n > 32)) && ((xpos + m <= 30) && (xpos + m > 0)))/*RCS*/   ||
                   (((ypos + n <= 32) && (ypos + n > 0)) && ((xpos + m <= 32) && (xpos + m > 0)))/*PROT*/) )
                  if (n == -2 || n == 2 || m == -2 || m == 2) {
                     if (!IsDetached(xpos + m, ypos + n, m, n))
                        fSourceCopy->SetBinContent(xpos + m, ypos + n, 0.0);
                  } else {
                     fSourceCopy->SetBinContent(xpos + m, ypos + n, 0.0);
                  }
      } else {
         fQuit = true;
      }
   }
   /*   cout << " found " << peaksfound << " peaks using max: " << firstMax << " \n";*/
   //   delete fSourceCopy;
   return(peaksfound);
}
