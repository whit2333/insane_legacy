#include "InSANECalorimeterCluster.h"

ClassImp(InSANECalorimeterCluster)

//______________________________________________________________________________
InSANECalorimeterCluster::InSANECalorimeterCluster(Int_t clustNum) : InSANECluster(clustNum) {
   fSecondHighestEnergy = 0.0;
   fThirdHighestEnergy  = 0.0;
   fHighestEnergy       = 0.0;
   fNNonZeroBlocks      = 0;
   fTotalE              = 0.0;
   fiPeak               = -1;
   fjPeak               = -1;
   Clear();
}
//______________________________________________________________________________
InSANECalorimeterCluster::~InSANECalorimeterCluster() {
}
//______________________________________________________________________________
void InSANECalorimeterCluster::ClearBlocks() {
   for (int i = 0; i < 5; i++)for (int j = 0; j < 5; j++) fBlockEnergies[i][j] = 0.0;
}
//______________________________________________________________________________
void InSANECalorimeterCluster::Clear(Option_t *opt) { 
   InSANECluster::Clear(opt);
   fIsNoisyChannel      = false;
   fSecondHighestEnergy = 0.0;
   fThirdHighestEnergy  = 0.0;
   fHighestEnergy       = 0.0;
   fNNonZeroBlocks      = 0;
   fTotalE              = 0.0;
   fiPeak               = -1;
   fjPeak               = -1;
   for (int i = 0; i < 5; i++)
      for (int j = 0; j < 5; j++) fBlockEnergies[i][j] = 0.0;
}
//______________________________________________________________________________
InSANECalorimeterCluster& InSANECalorimeterCluster::operator=(const InSANECalorimeterCluster &rhs) {
   // Check for self-assignment!
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   // Deallocate, allocate new space, copy values...

   InSANECluster::operator=(rhs);

   fIsNoisyChannel      = rhs.fIsNoisyChannel;
   fNNonZeroBlocks      = rhs.fNNonZeroBlocks;
   fHighestEnergy       = rhs.fHighestEnergy;
   fSecondHighestEnergy = rhs.fSecondHighestEnergy;
   fThirdHighestEnergy  = rhs.fThirdHighestEnergy;
   fTotalE              = rhs.fTotalE;
   fiPeak               = rhs.fiPeak;
   fjPeak               = rhs.fjPeak;
   for (int i = 0; i < 5; i++)
      for (int j = 0; j < 5; j++) fBlockEnergies[i][j] = rhs.fBlockEnergies[i][j];
   return *this;
}
//______________________________________________________________________________


