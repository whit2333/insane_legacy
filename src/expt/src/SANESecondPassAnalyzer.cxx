#include "SANESecondPassAnalyzer.h"
#include "SANERunManager.h"
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "TFitResult.h"
#include "InSANEDatabaseManager.h"

ClassImp(SANESecondPassAnalyzer)
//_______________________________________________________//

SANESecondPassAnalyzer::SANESecondPassAnalyzer(const char * newTreeName, const char * uncorrectedTreeName)
   : SANEClusteringAnalysis(uncorrectedTreeName) , InSANEAnalyzer(newTreeName, uncorrectedTreeName)
{

   if (SANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o SANESecondPassAnalyzer Constructor\n";

   // ---------------------------------
   // Go to new file and get the tree
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   fInputTree = nullptr;
   if(fAnalysisTree) fInputTree = fAnalysisTree; // anlysis tree set in InSANEDetectorAnalysis
   if(!fInputTree)   fInputTree = (TTree*)gROOT->FindObject(uncorrectedTreeName);
   if(!fInputTree){
      Error("SANESecondPassAnalyzer","Tree: %s was not found!", uncorrectedTreeName);
   }

   // ---------------------------------
   // Scalers
   fScalerTreeName       = "Scalers1";
   fOutputScalerTreeName = "Scalers2";
   
   // Go to scaler file
   TFile * sf = InSANERunManager::GetRunManager()->GetScalerFile();
   if( sf ){
      sf->cd();
   } else {
      Error("SANESecondPassAnalyzer","No Scaler File.");
   }

   // Find the scaler tree
   // First look in the current directory
   fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName.Data());
   // If it is not there look in the main file.
   if(!fScalerTree) {
      SANERunManager::GetRunManager()->GetCurrentFile()->cd();
      fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName.Data());
   }
   // Copy the scaler tree structure 
   if(!fScalerTree){
      Error("SANESecondPassAnalyzer","scaler tree, %s, was not found!", fScalerTreeName.Data());
   } else {
      fScalers->SetBranches(fScalerTree);
   }

   fOutputFile = SANERunManager::GetRunManager()->GetCurrentFile();
   fOutputFile->cd();

   //// Go to new file
   //SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   //fInputTree = 0;
   //if (fAnalysisTree) fInputTree = fAnalysisTree; // anlysis tree set in InSANEDetectorAnalysis
   //if (!fInputTree) fInputTree = (TTree*)gROOT->FindObject(uncorrectedTreeName);
   //if (!fInputTree) printf("x- Tree:%s  was NOT FOUND! (from SANESecondPassAnalyzer c'tor)  \n", uncorrectedTreeName);

   //// Go to scaler file and get the tree
   //fScalerTreeName = "Scalers0";
   //TFile * sf = InSANERunManager::GetRunManager()->GetScalerFile();
   //if (sf) sf->cd();
   //else std::cout << " NO SCALER FILE ?!?!?!\n";
   //if (!fScalerTree) fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName.Data());
   //fScalers->SetBranches(fScalerTree);
   //fOutputScalerTreeName = "Scalers1";


   //fOutputFile = SANERunManager::GetRunManager()->GetCurrentFile();

   //if (!fScalerTree) fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName);

   //fOutputFile = SANERunManager::GetRunManager()->GetCurrentFile();
   //fOutputFile->cd();
//   gDirectory->pwd();

}
//_______________________________________________________//

SANESecondPassAnalyzer::~SANESecondPassAnalyzer()
{

   gDirectory->pwd();

   if (fCalculatedTree) {
      std::cout << " - Building First Pass Index for corrected tree\n";
      fCalculatedTree->BuildIndex("fRunNumber", "fEventNumber");
      fCalculatedTree->Write(fCalculatedTree->GetName(), kWriteDelete);
      fCalculatedTree->FlushBaskets();
   }

   SANERunManager::GetRunManager()->GetCurrentRun()->fAnalysisPass = 2;
   SANERunManager::GetRunManager()->WriteRun();
   SANERunManager::GetRunManager()->GetCurrentFile()->Flush();
   SANERunManager::GetRunManager()->GetCurrentFile()->Write();
   SANERunManager::GetRunManager()->GetRunReport()->UpdateRunDatabase();


   if (SANERunManager::GetRunManager()->GetVerbosity() > 0)  printf(" 0 SANESecondPassAnalyzer destructor \n");

}
//_______________________________________________________//

void SANESecondPassAnalyzer::Initialize(TTree * inTree)
{
   std::cout << " o SANESecondPassAnalyzer::Initialize(TTree *) \n";

   if (inTree)fInputTree = inTree;

   if (fInputTree) {
      fClusterEvent = fEvents->SetClusterBranches(fInputTree);
      fCalculatedTree = fInputTree->CloneTree(0);
      fCalculatedTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s", fCalculatedTreeName.Data()));
   } else {
      printf("x- No Input tree ! \n");
   }

   fClusters = fClusterEvent->fClusters;

   if (fScalerTree) fOutputScalerTree = fScalerTree->CloneTree(0);
//     fOutputScalerTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s",fCalculatedTreeName.Data() ));

   InitCorrections();

   InitCalculations();

   /// Important: Set the event to be used for triggering in the InSANEAnalyzer
   InSANEAnalyzer::SetTriggerEvent(fScalers->fTriggerEvent);
   InSANEAnalyzer::SetDetectorTriggerEvent(fEvents->TRIG);
   InSANEAnalyzer::SetScalerTriggerEvent(fScalers->fTriggerEvent);
}
//_______________________________________________________//


void SANESecondPassAnalyzer::MakePlots()
{

   SANERunManager::GetRunManager()->WriteRun();

   SANERunManager::GetRunManager()->GetCurrentFile()->Flush();
   SANERunManager::GetRunManager()->GetCurrentFile()->Write();
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();

//   std::cout << "SANESecondPassAnalyzer::MakePlots() \n";
//
//   gROOT->ProcessLine(Form(".x scripts/bigcalCherenkov.cxx(%d,%d) ",fRunNumber,0));
//   gDirectory->pwd();
//
// //  gROOT->ProcessLine(Form(".x scripts/findMirrorEdges.cxx(%d) ",fRunNumber));
//   gROOT->ProcessLine(Form(".x scripts/pi0mass.cxx(%d) ",fRunNumber));
//   gDirectory->pwd();
//   gROOT->ProcessLine(".x scripts/RunAppend.cxx ");

   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
}


