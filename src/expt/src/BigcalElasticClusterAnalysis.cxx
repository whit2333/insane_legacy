#define BigcalElasticClusterAnalysis_cxx
#include "SANEEvents.h"

#include "BigcalElasticClusterAnalysis.h"
#include "BIGCALClusterProcessor.h"

#include "TROOT.h"
#include "TCanvas.h"

ClassImp(BigcalElasticClusterAnalysis)

//__________________________________________________
BigcalElasticClusterAnalysis::BigcalElasticClusterAnalysis(Int_t run)
{
   fRunNumber = run;
   InSANEDatabaseManager * dbManager = InSANEDatabaseManager::GetManager() ;

   fAnalysisFile = new TFile(Form("data/rootfiles/SANE%d.root", fRunNumber));
   events = new SANEEvents();

   fOutputFile = new TFile(Form("data/rootfiles/Elastics%d.root", fRunNumber), "RECREATE");
// Make sure this comes after opening the file

   fOutputTree = new TTree("elasticEvents", "Elastic Detector Event Data");
   fOutputTree->Branch("elasticBetaDetectorEvent", "BETAEvent", &events->BETA);
   fOutputTree->Branch("elasticTrigDetectorEvent", "InSANETriggerEvent", &events->TRIG);
   fOutputTree->Branch("elasticHmsDetectorEvent", "HMSEvent", &events->HMS);
   fOutputTree->Branch("elasticBeamDetectorEvent", "HallCBeamEvent", &events->BEAM);

   fClusterProcessor = new BIGCALClusterProcessor();
   fClusterProcessor->SetEventAddress(events->BETA->fBigcalEvent);
   /// \todo {Implement DETECTORPhysicsEvent }
   fClusters = new TClonesArray("BIGCALCluster", 1);
   fOutputTree->Branch("elasticClusters", &fClusters);

   fOutputTree->SetAutoSave();

}

//__________________________________________________
BigcalElasticClusterAnalysis::~BigcalElasticClusterAnalysis()
{
   fOutputTree->Write();
   ;
}

//__________________________________________________
Int_t BigcalElasticClusterAnalysis::AnalyzeRun()
{
///// EVENT LOOP  //////
   Int_t nevent = events->fTree->GetEntries();

   std::cout << "\n" << nevent << " ENTRIES \n";

   for (int iEVENT = 0; iEVENT < nevent; iEVENT++) {
      events->GetEvent(iEVENT);///\todo{ I think I can remove this method by using TTreeIndex}

      if (events->TRIG->IsCoinEvent()) {  // coin trigger
         std::cout  << iEVENT << " event \n";

         fClusterProcessor->ProcessEvent(fClusters);
         /*    cout <<  events->BETA->fGasCherenkovEvent->fNumberOfHits << " cherenkov HITS \n";
             cout <<  events->HMS->hms_p << " GeV proton \n";
             cout <<  events->HMS->hms_theta << " GeV \n";
         if(events->beamTree){
             cout <<  events->BEAM->slow_rast_x << " " << events->BEAM->slow_rast_y << " slow rast   \n";
             cout <<  events->BEAM->rast_x << " " << events->BEAM->rast_y << "  rast  \n";
             cout <<  events->BEAM->energy << " GeV \n";
         }
         */
// loop over cherenkov hits for event
//for (kk=0; kk< BETA->fGasCherenkovEvent->fNumberOfHits;kk++)
//  {
//    aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
         /*    cout <<  aCerHit->fADC << " ADC \n";
             cout <<  aCerHit->fTDC << " TDC \n";
             cout <<  aCerHit->fMirrorNumber << " MirrorNumber \n";
             cout <<  aCerHit->fPMTNumber[0] << " PMTNumber \n";
         */
         /*    cer_adc_hist[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);

             if( aCerHit->fTDC > tdclow && aCerHit->fTDC < tdchigh)
               { //passes very wide tdc cut
         //if(odd_ceradc_hit) printf("cer_num[k] - 1 =%d\n",tempMirrorNumber);
               cer_adc_hist_wIndTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);
               cer_tdc_hist_wIndTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDC);
               }
         } // End loop over Cherenkov signals
             */
         fOutputTree->Fill();
      } // coin trigger bit

   } // END OF EVENT LOOP
   return(0);
}

//__________________________________________________
Int_t BigcalElasticClusterAnalysis::Visualize()
{
   auto * c1 = new TCanvas("ElasticAnalysisCanvas", "Elastic Events");
   c1->Divide(3, 2);
   c1->cd(1);
   fOutputTree->Draw("hms_p:fBigcalEvent.fTotalEnergyDeposited", "hms_p<4.0", "colz");

   c1->cd(2);
   fOutputTree->Draw("hms_p:fBigcalEvent.fTotalEnergyDeposited", "hms_p<4.0&&fGasCherenkovEvent.fNumberOfHits>2", "colz");

   c1->cd(4);
   fOutputTree->Draw("hms_theta:fBigcalEvent.fTotalEnergyDeposited", "hms_p<4.0", "colz");

   c1->cd(5);
   fOutputTree->Draw("hms_theta:fBigcalEvent.fTotalEnergyDeposited", "hms_p<4.0&&fGasCherenkovEvent.fNumberOfHits>2", "colz");


   return(0);
}

