#include "SANEZerothPassAnalyzer.h"


ClassImp(SANEZerothPassAnalyzer)

//______________________________________________________________________________
SANEZerothPassAnalyzer::SANEZerothPassAnalyzer(const char * newTreeName, const char * uncorrectedTreeName)
   : InSANEDetectorAnalysis(uncorrectedTreeName),InSANEAnalyzer(newTreeName, uncorrectedTreeName)
{
   SANERunManager * runManager = SANERunManager::GetRunManager();
   // Go to current file
   if (runManager->GetPreviousFile()) runManager->GetPreviousFile()->cd();
   else if (runManager->GetCurrentFile()) runManager->GetCurrentFile()->cd();
   // Find the tree
   fInputTree = nullptr;
   if (fAnalysisTree) fInputTree = fAnalysisTree; // anlysis tree set in InSANEDetectorAnalysis
   if (!fInputTree) fInputTree = (TTree*)gROOT->FindObject(uncorrectedTreeName);
   if (!fInputTree) printf("x- Tree:%s  was NOT FOUND! (from SANEFirstPassAnalyzer c'tor)  \n", uncorrectedTreeName);

   // Go to scaler file and get the tree
   TFile * sf = InSANERunManager::GetRunManager()->GetScalerFile();
   if (sf) sf->cd();
   else std::cout << " NO SCALER FILE ?!?!?!\n";
   if (!fScalerTree) fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName.Data());
   fScalers->SetBranches(fScalerTree);
   fOutputScalerTreeName = "Scalers0";

   fOutputFile = runManager->GetCurrentFile();
   if (fOutputFile) fOutputFile->cd();
   if (runManager->GetVerbosity() > 1)  printf(" o SANEZerothPassAnalyzer c'tor \n");

   fMemFileName = "mem_out0.txt";
}
//______________________________________________________________________________
SANEZerothPassAnalyzer::~SANEZerothPassAnalyzer() {

   // Update and save the output tree.
   if (fOutputFile) fOutputFile->cd();
   if (fCalculatedTree) {
      fCalculatedTree->BuildIndex("fRunNumber", "fEventNumber");
      //fCalculatedTree->Write("", TObject::kOverwrite);
      fCalculatedTree->FlushBaskets();
   }

   // Update run object/database and save to file
   if (fOutputFile) fOutputFile->cd();
   SANERunManager::GetRunManager()->GetCurrentRun()->fAnalysisPass = 0;
   SANERunManager::GetRunManager()->GetRunReport()->UpdateRunDatabase();
   SANERunManager::GetRunManager()->WriteRun();

   // Save Scaler tree
   SANERunManager::GetRunManager()->GetScalerFile()->cd();
   if (fOutputScalerTree) {
      //fOutputScalerTree->Write("", TObject::kOverwrite);
      fOutputScalerTree->FlushBaskets();
   }

   // Delete the input tree to avoid re-writing (creating a new key)
   //if (fInputTree) delete fInputTree;
   //fInputTree = 0;

   // Write the new file
   if (fOutputFile) {
      /*fOutputFile->Flush();*/
      fOutputFile->Write();
   }
}
//_____________________________________________________________________________

void SANEZerothPassAnalyzer::Initialize(TTree * inTree)
{
   if (SANERunManager::GetRunManager()->GetVerbosity() > 1)
      printf(" o SANEZerothPassAnalyzer::Initialize(TTree*) \n");

   //     fInputTree=inTree;
   //     fOutputFile->cd();
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   fCalculatedTree = fInputTree->CloneTree(0);
   fCalculatedTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s", fCalculatedTreeName.Data()));

   SANERunManager::GetRunManager()->GetScalerFile()->cd();
   if (fScalerTree) fOutputScalerTree = fScalerTree->CloneTree(0);
   if (fOutputScalerTree) {
      fOutputScalerTree->SetNameTitle(fOutputScalerTreeName.Data(), Form("A corrected tree: %s", fOutputScalerTreeName.Data()));
      std::cout << " o Created output scaler tree, " << fOutputScalerTreeName.Data() << "\n";
   }

   InitCorrections();
   InitCalculations();

   /// \todo the following should prob be elsewhere
   /// Important: Set the event to be used for triggering in the InSANEAnalyzer
   if (SANERunManager::GetRunManager()->GetVerbosity() > 1)
      std::cout << " Setting Analysis Trigger Events... \n";
   InSANEAnalyzer::SetTriggerEvent(fScalers->fTriggerEvent);
   InSANEAnalyzer::SetDetectorTriggerEvent(fEvents->TRIG);
   InSANEAnalyzer::SetScalerTriggerEvent(fScalers->fTriggerEvent);
   if (SANERunManager::GetRunManager()->GetVerbosity() > 1)
      std::cout << "Detector trigger event address " << fEvents->TRIG << "\n";
}
//_______________________________________________________//

void SANEZerothPassAnalyzer::MakePlots()
{
   SANERunManager::GetRunManager()->Print();


   SANERunManager::GetRunManager()->GetScalerFile()->cd();
   if (fOutputScalerTree) {
      fOutputScalerTree->Write("", TObject::kOverwrite);
   }


   SANERunManager::GetRunManager()->GetCurrentFile()->Flush();
   SANERunManager::GetRunManager()->GetCurrentFile()->Write();

   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//    std::cout << " = Executing pass0/time_walk.cxx\n";
//    gROOT->ProcessLine(Form(".x pass0/time_walk.cxx(%d) ",SANERunManager::GetRunManager()->fRunNumber));

//    std::cout << " = Executing pass0/beamMonitor.cxx\n";
//    gROOT->ProcessLine(Form(".x pass0/beamMonitor.cxx(%d) ",SANERunManager::GetRunManager()->fRunNumber));
//
//
//    std::cout << " = Executing pass0/liveTime.cxx\n";
//    gROOT->ProcessLine(Form(".x pass0/liveTime.cxx(%d) ",SANERunManager::GetRunManager()->fRunNumber));

   std::cout << " - Saving Run Object\n";
   //SANERunManager::GetRunManager()->WriteRun();

   //fClusterEvent->Draw("bigcalClusters.fCherenkovTotalNPE>>npe(400,0,40)","bigcalClusters.fNumberOfMirrors==1")

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 2 mirrors.
   /*fCorrectedTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_2mir(200,-65,65,300,-120,120)",
                      "bigcalClusters.fCherenkovBestNPESum>4 && bigcalClusters.fNumberOfMirrors==2&&bigcalClusters.fTotalE>1000");
   TH2 * bigcal_clusters_4npec_2mir = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_2mir");
   bigcal_clusters_4npec_2mir->SetTitle("2 Mirror Clusters with 4 or more PE");*/
   //bc_1_clust->Fit("gaus","E,M","",5,28);
//  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_2mir.ps",runnumber));
//   c->SaveAs(Form("plots/%d/raster/.jpg",runnumber));

}

