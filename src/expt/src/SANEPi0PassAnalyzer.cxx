#include "SANEPi0PassAnalyzer.h"
#include "SANERunManager.h"
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "TFitResult.h"
#include "InSANEDatabaseManager.h"

ClassImp(SANEPi0PassAnalyzer)

//______________________________________________________________________________
SANEPi0PassAnalyzer::SANEPi0PassAnalyzer(const char * newTreeName, const char * uncorrectedTreeName)
   : SANEClusteringAnalysis(uncorrectedTreeName) , InSANEAnalyzer(newTreeName, uncorrectedTreeName)
{
   if (fAnalysisTree) fInputTree = fAnalysisTree; // anlysis tree set in InSANEDetectorAnalysis
   if (!fInputTree) fInputTree = (TTree*)gROOT->FindObject(uncorrectedTreeName);
   if (!fInputTree) printf("x- Tree:%s  was NOT FOUND! (from SANEPi0PassAnalyzer c'tor)  \n", uncorrectedTreeName);
   if (!fScalerTree) fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName);

   fOutputFile = SANERunManager::GetRunManager()->GetCurrentFile();
   fOutputFile->cd();
}
//______________________________________________________________________________
SANEPi0PassAnalyzer::~SANEPi0PassAnalyzer() {

   gDirectory->pwd();

   //if (fCalculatedTree) {
   //   std::cout << " - Building First Pass Index for corrected tree\n";
   //   fCalculatedTree->BuildIndex("fRunNumber", "fEventNumber");
   //   fCalculatedTree->Write(fCalculatedTree->GetName(), kWriteDelete);
   //   fCalculatedTree->FlushBaskets();
   //}

   SANERunManager::GetRunManager()->GetCurrentRun()->fAnalysisPass = 2;
   SANERunManager::GetRunManager()->WriteRun();
   SANERunManager::GetRunManager()->GetCurrentFile()->Flush();
   SANERunManager::GetRunManager()->GetCurrentFile()->Write();
   SANERunManager::GetRunManager()->GetRunReport()->UpdateRunDatabase();


   if (SANERunManager::GetRunManager()->GetVerbosity() > 0)  printf(" 0 SANEPi0PassAnalyzer destructor \n");

}
//_______________________________________________________//

void SANEPi0PassAnalyzer::Initialize(TTree * inTree)
{
   std::cout << " o SANEPi0PassAnalyzer::Initialize(TTree *) \n";

   if (inTree)fInputTree = inTree;

   if (fInputTree) {
      fClusterEvent = fEvents->SetClusterBranches(fInputTree);
      fCalculatedTree = fInputTree->CloneTree(0);
      fCalculatedTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s", fCalculatedTreeName.Data()));
   } else {
      printf("x- No Input tree ! \n");
   }

   fClusters = fClusterEvent->fClusters;

   if (fScalerTree) fOutputScalerTree = fScalerTree->CloneTree(0);
//     fOutputScalerTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s",fCalculatedTreeName.Data() ));

   InitCorrections();

   InitCalculations();

   /// Important: Set the event to be used for triggering in the InSANEAnalyzer
   InSANEAnalyzer::SetTriggerEvent(fScalers->fTriggerEvent);
   InSANEAnalyzer::SetDetectorTriggerEvent(fEvents->TRIG);
   InSANEAnalyzer::SetScalerTriggerEvent(fScalers->fTriggerEvent);
}
//_______________________________________________________//


void SANEPi0PassAnalyzer::MakePlots()
{

   SANERunManager::GetRunManager()->WriteRun();

   SANERunManager::GetRunManager()->GetCurrentFile()->Flush();
   SANERunManager::GetRunManager()->GetCurrentFile()->Write();
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();

//   std::cout << "SANEPi0PassAnalyzer::MakePlots() \n";
//
//   gROOT->ProcessLine(Form(".x scripts/bigcalCherenkov.cxx(%d,%d) ",fRunNumber,0));
//   gDirectory->pwd();
//
// //  gROOT->ProcessLine(Form(".x scripts/findMirrorEdges.cxx(%d) ",fRunNumber));
//   gROOT->ProcessLine(Form(".x scripts/pi0mass.cxx(%d) ",fRunNumber));
//   gDirectory->pwd();
//   gROOT->ProcessLine(".x scripts/RunAppend.cxx ");

   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
}


