#include "InSANETrajectory.h"

ClassImp(InSANETrajectory)

//______________________________________________________________________________
InSANETrajectory::InSANETrajectory() {
   fEventNumber         = 0;
   fRunNumber           = 0;
   fCherenkovTDC        = -4000;
   fNCherenkovElectrons = 0;
   fClusterNumber       = 0;
   fIsNoisyChannel      = false;
   fNPositions          = 0;
   fPosition0.SetXYZ(0, 0, 0);
   fMomentum.SetXYZT(0, 0, 0, 0);
}
//______________________________________________________________________________
InSANETrajectory::InSANETrajectory(const InSANETrajectory& rhs) {
   (*this) = rhs;
}
//______________________________________________________________________________
InSANETrajectory::~InSANETrajectory() {
}
//______________________________________________________________________________
InSANETrajectory& InSANETrajectory::operator=(const InSANETrajectory &rhs) {
   if (this == &rhs)
      return *this;
   //fHitPositions = rhs.fHitPositions;
   fPosition0           = rhs.fPosition0;
   fMomentum            = rhs.fMomentum;
   fRunNumber           = rhs.fRunNumber;
   fEventNumber         = rhs.fEventNumber;
   fNPositions          = rhs.fNPositions;
   fNCherenkovElectrons = rhs.fNCherenkovElectrons;
   fCherenkovTDC        = rhs.fCherenkovTDC;
   fIsNoisyChannel      = rhs.fIsNoisyChannel;

   return *this;
}
//______________________________________________________________________________
void InSANETrajectory::Clear(Option_t * opt) {
   //fHitPositions.Delete();
   fIsNoisyChannel      = false;
   fNPositions          = 0;
   fCherenkovTDC        = -4000;
   fEventNumber         = 0;
   fNCherenkovElectrons = 0;
   fClusterNumber       = 0;
   fPosition0.SetXYZ(0, 0, 0);
   fMomentum.SetXYZT(0, 0, 0, 0);
}
//______________________________________________________________________________

