#include "ANNEvents.h"

ClassImp(ANNEvent)

//______________________________________________________________________________
ANNEvent::ANNEvent() {
   fInputNeurons.Clear();
   fOutputNeurons.Clear();
   fOutputValues.clear();
   fInputValues.clear();
}
//______________________________________________________________________________
ANNEvent::~ANNEvent() {
}
//______________________________________________________________________________
void ANNEvent::AddInputNeuron(const char * name, Double_t * address) {
   fInputNeurons.Add(new TObjString(name));
   fInputValues.push_back(address);
}
//______________________________________________________________________________
void ANNEvent::AddOutputNeuron(const char * name, Double_t * address) {
   fOutputNeurons.Add(new TObjString(name));
   fOutputValues.push_back(address);
}
//______________________________________________________________________________
const char * ANNEvent::GetInputNeurons() {
   TString result = "";
   for (int i = 0; i < fInputNeurons.GetEntries(); i++) {
      if (i != 0) result += ",";
      result += ((TObjString*)fInputNeurons.At(i))->GetString();
   }
   return(result.Data());
}
//______________________________________________________________________________
const char * ANNEvent::GetOutputNeurons() {
   TString result = "";
   for (int i = 0; i < fOutputNeurons.GetEntries(); i++) {
      if (i != 0) result += ",";
      result += ((TObjString*)fOutputNeurons.At(i))->GetString();
   }
   return(result.Data());
}
//______________________________________________________________________________
void ANNEvent::GetInputNeuronArray(Double_t * v) {
   for (unsigned int i = 0; i < fInputValues.size(); i++) {
      v[i] = *(fInputValues.at(i));
   }
}
//______________________________________________________________________________
void ANNEvent::Clear() {
   fInputNeurons.Clear();
   fOutputNeurons.Clear();
   fInputValues.clear();
   fOutputValues.clear();
}
//______________________________________________________________________________


ClassImp(ANNDisElectronEvent)

//______________________________________________________________________________
ANNDisElectronEvent::ANNDisElectronEvent() {

   fInputNeurons  = "@fClusterEnergy,@fXCluster,@fYCluster,@fXClusterSigma,@fYClusterSigma";
   fInputNeurons += ",@fXClusterSkew,@fYClusterSkew";
   fNInputNeurons = 6;

   fTrueEnergy       = 0;
   fDelta_Energy     = 0;
   fTrueTheta        = 0;
   fDelta_Theta      = 0;
   fTruePhi          = 0;
   fDelta_Phi        = 0;
   fXCluster         = 0;
   fYCluster         = 0;
   fXClusterSigma    = 0;
   fYClusterSigma    = 0;
   fXClusterSkew     = 0;
   fYClusterSkew     = 0;
   fClusterEnergy    = 0;
   fCherenkovADC     = 0;
   fNClusters        = 0;
   fGoodCherenkovHit = false;
   fBackground       = false;
   fSignal           = false;
   fClusterTheta     = 0.0;
   fClusterPhi       = 0.0;
}
//______________________________________________________________________________
ANNDisElectronEvent::~ANNDisElectronEvent() {   
}
//______________________________________________________________________________
void ANNDisElectronEvent::GetMLPInputNeuronArray(Double_t * par) const {
   par[0] = fClusterEnergy;
   par[1] = fXCluster;
   par[2] = fYCluster;
   par[3] = fXClusterSigma;
   par[4] = fYClusterSigma;
   par[5] = fXClusterSkew;
   par[6] = fYClusterSkew;
}
//______________________________________________________________________________
void ANNDisElectronEvent::Clear() {
   fTrueEnergy       = 0;
   fDelta_Energy     = 0;
   fTrueTheta        = 0;
   fDelta_Theta      = 0;
   fTruePhi          = 0;
   fDelta_Phi        = 0;
   fXCluster         = 0;
   fYCluster         = 0;
   fXClusterSigma    = 0;
   fYClusterSigma    = 0;
   fXClusterSkew     = 0;
   fYClusterSkew     = 0;
   fClusterEnergy    = 0;
   fCherenkovADC     = 0;
   fNClusters        = 0;
   fGoodCherenkovHit = false;
   fBackground       = false;
   fSignal           = false;
   fClusterTheta     = 0.0;
   fClusterPhi       = 0.0;
}
//______________________________________________________________________________
void ANNDisElectronEvent::SetEventValues(BIGCALCluster * clust) {
   fXCluster         = clust->GetXmoment();
   fYCluster         = clust->GetYmoment();
   fXClusterSigma    = clust->GetXStdDeviation();
   fYClusterSigma    = clust->GetYStdDeviation();
   fXClusterSkew     = clust->GetXSkewness();
   fYClusterSkew     = clust->GetYSkewness();
   fClusterEnergy    = clust->GetEnergy();
   fCherenkovADC     = clust->fCherenkovBestADCSum;
   fGoodCherenkovHit = clust->fGoodCherenkovTDCHit;
   fClusterTheta     = clust->GetTheta();
   fClusterPhi       = clust->GetPhi();
}
//______________________________________________________________________________
Int_t ANNDisElectronEvent::GetNInputNeurons() const { 
   return(fNInputNeurons);
}
//______________________________________________________________________________
const char * ANNDisElectronEvent::GetMLPInputNeurons() const {
   return(fInputNeurons.Data());
}
//______________________________________________________________________________
//const char * ANNDisElectronEvent::GetMLPInputNeurons() {
//   fInputNeurons = "fClusterEnergy,fXCluster,fYCluster,fXClusterSigma,fYClusterSigma";
//   fInputNeurons += ",fXClusterSkew,fYClusterSkew,fGoodCherenkovHit,fCherenkovADC,fNClusters";
//   return(fInputNeurons.Data());
//}
//______________________________________________________________________________
//void ANNDisElectronEvent::GetMLPInputNeuronArray(Double_t * par) {
//   ///params[9] = {fClusterEnergy,fXCluster,fYCluster,fXClusterSigma,fYClusterSigma,fXClusterSkew,fYClusterSkew,fGoodCherenkovHit,fCherenkovADC};
//   par[0] = fClusterEnergy;
//   par[1] = fXCluster;
//   par[2] = fYCluster;
//   par[3] = fXClusterSigma;
//   par[4] = fYClusterSigma;
//   par[5] = fXClusterSkew;
//   par[6] = fYClusterSkew;
//   par[7] = fGoodCherenkovHit;
//   par[8] = fCherenkovADC;
//   par[9] = fNClusters;
//}
//______________________________________________________________________________


ClassImp(ANNDisElectronEvent2)

//______________________________________________________________________________
ANNDisElectronEvent2::ANNDisElectronEvent2() {
   fInputNeurons = "fClusterEnergy,fXCluster,fYCluster,fXClusterSigma,fYClusterSigma";
   fInputNeurons += ",fXClusterSkew,fYClusterSkew,fGoodCherenkovHit,fCherenkovADC,fNClusters";
   fInputNeurons += ",fTrackerDeltaX,fTrackerDeltaY";
   fNInputNeurons = 12;
   fTrackerDeltaX = 0;
   fTrackerDeltaY = 0;
}
//______________________________________________________________________________
ANNDisElectronEvent2::~ANNDisElectronEvent2(){
}
//______________________________________________________________________________
void ANNDisElectronEvent2::GetMLPInputNeuronArray(Double_t * par) const {
   par[0]  = fClusterEnergy;
   par[1]  = fXCluster;
   par[2]  = fYCluster;
   par[3]  = fXClusterSigma;
   par[4]  = fYClusterSigma;
   par[5]  = fXClusterSkew;
   par[6]  = fYClusterSkew;
   par[7]  = fGoodCherenkovHit;
   par[8]  = fCherenkovADC;
   par[9]  = fNClusters;
   par[10] = fTrackerDeltaX;
   par[11] = fTrackerDeltaY;
}
//______________________________________________________________________________
void ANNDisElectronEvent2::Clear() {
   ANNDisElectronEvent::Clear();
   fTrackerDeltaX = 0;
   fTrackerDeltaY = 0;
}
//______________________________________________________________________________


ClassImp(ANNDisElectronEvent3)

//______________________________________________________________________________
ANNDisElectronEvent3::ANNDisElectronEvent3() {
   // Same as ANNDisElectronEvent2 except without fNClusters as input
   fInputNeurons = "fClusterEnergy,fXCluster,fYCluster,fXClusterSigma,fYClusterSigma";
   fInputNeurons += ",fXClusterSkew,fYClusterSkew,fGoodCherenkovHit,fCherenkovADC";
   fInputNeurons += ",fTrackerDeltaX,fTrackerDeltaY";
   fNInputNeurons = 11;
   fTrackerDeltaX = 0;
   fTrackerDeltaY = 0;
}
//______________________________________________________________________________
ANNDisElectronEvent3::~ANNDisElectronEvent3(){
}
//______________________________________________________________________________
void ANNDisElectronEvent3::GetMLPInputNeuronArray(Double_t * par) const {
   par[0] = fClusterEnergy;
   par[1] = fXCluster;
   par[2] = fYCluster;
   par[3] = fXClusterSigma;
   par[4] = fYClusterSigma;
   par[5] = fXClusterSkew;
   par[6] = fYClusterSkew;
   par[7] = fGoodCherenkovHit;
   par[8] = fCherenkovADC;
   par[9] = fTrackerDeltaX;
   par[10] = fTrackerDeltaY;
}
//______________________________________________________________________________


ClassImp(ANNDisElectronEvent4)

//______________________________________________________________________________
ANNDisElectronEvent4::ANNDisElectronEvent4(){
   fInputNeurons  = "@fClusterEnergy,@fXCluster,@fYCluster,@fXClusterSigma,@fYClusterSigma";
   fInputNeurons += ",@fXClusterSkew,@fYClusterSkew";
   fNInputNeurons = 7;
   fClusterDeltaX = 0;
   fClusterDeltaY = 0;
}
//______________________________________________________________________________
ANNDisElectronEvent4::~ANNDisElectronEvent4() {
}
//______________________________________________________________________________
void ANNDisElectronEvent4::GetMLPInputNeuronArray(Double_t * par) const {
   par[0] = fClusterEnergy;
   par[1] = fXCluster;
   par[2] = fYCluster;
   par[3] = fXClusterSigma;
   par[4] = fYClusterSigma;
   par[5] = fXClusterSkew;
   par[6] = fYClusterSkew;
}
//______________________________________________________________________________
void ANNDisElectronEvent4::Clear() {
   ANNDisElectronEvent::Clear();
   fClusterDeltaX = 0;
   fClusterDeltaY = 0;
}
//______________________________________________________________________________


ClassImp(ANNDisElectronEvent5)

//______________________________________________________________________________
ANNDisElectronEvent5::ANNDisElectronEvent5() {
   fInputNeurons  = "@fClusterEnergy,@fXCluster,@fYCluster,@fXClusterSigma,@fYClusterSigma";
   fInputNeurons += ",@fXClusterSkew,@fYClusterSkew";
   fInputNeurons += ",@fXClusterKurt,@fYClusterKurt";
   fNInputNeurons = 9;
}
//______________________________________________________________________________
ANNDisElectronEvent5::~ANNDisElectronEvent5() {
}
//______________________________________________________________________________
void ANNDisElectronEvent5::GetMLPInputNeuronArray(Double_t * par) const {
   par[0] = fClusterEnergy;
   par[1] = fXCluster;
   par[2] = fYCluster;
   par[3] = fXClusterSigma;
   par[4] = fYClusterSigma;
   par[5] = fXClusterSkew;
   par[6] = fYClusterSkew;
   par[7] = fXClusterKurt;
   par[8] = fYClusterKurt;
}
//______________________________________________________________________________
void ANNDisElectronEvent5::Clear() {
   ANNDisElectronEvent::Clear();
   fClusterDeltaX = 0;
   fClusterDeltaY = 0;
   fXClusterKurt  = 0;
   fYClusterKurt  = 0;
}
//______________________________________________________________________________
void ANNDisElectronEvent5::SetEventValues(BIGCALCluster * clust){
   fXCluster         = clust->GetXmoment();
   fYCluster         = clust->GetYmoment();
   fXClusterSigma    = clust->GetXStdDeviation();
   fYClusterSigma    = clust->GetYStdDeviation();
   fXClusterSkew     = clust->GetXSkewness();
   fYClusterSkew     = clust->GetYSkewness();
   fXClusterKurt     = clust->fXKurtosis;
   fYClusterKurt     = clust->fYKurtosis;
   fClusterEnergy    = clust->GetEnergy();
   fCherenkovADC     = clust->fCherenkovBestADCSum;
   fGoodCherenkovHit = clust->fGoodCherenkovTDCHit;
   fClusterTheta     = clust->GetTheta();
   fClusterPhi       = clust->GetPhi();
}
//______________________________________________________________________________


ClassImp(ANNDisElectronEvent6)

//______________________________________________________________________________
ANNDisElectronEvent6::ANNDisElectronEvent6() {
   fInputNeurons = "@fClusterEnergy,@fXCluster,@fYCluster,@fXClusterSigma,@fYClusterSigma";
   fInputNeurons += ",@fXClusterSkew,@fYClusterSkew";
   fInputNeurons += ",@fXClusterKurt,@fYClusterKurt";
   fInputNeurons += ",@fRasterX,@fRasterY";
   fNInputNeurons = 11;
}
//______________________________________________________________________________
ANNDisElectronEvent6::~ANNDisElectronEvent6() {
}
//______________________________________________________________________________
void ANNDisElectronEvent6::GetMLPInputNeuronArray(Double_t * par) const {
   par[0] = fClusterEnergy;
   par[1] = fXCluster;
   par[2] = fYCluster;
   par[3] = fXClusterSigma;
   par[4] = fYClusterSigma;
   par[5] = fXClusterSkew;
   par[6] = fYClusterSkew;
   par[7] = fXClusterKurt;
   par[8] = fYClusterKurt;
   par[9] = fRasterX;
   par[10] = fRasterY;
}
//______________________________________________________________________________
void ANNDisElectronEvent6::Clear() {
   ANNDisElectronEvent5::Clear();
   fRasterX = 0;
   fRasterY = 0;
}
//______________________________________________________________________________

ClassImp(ANNPi0TwoGammaEvent)

//______________________________________________________________________________

