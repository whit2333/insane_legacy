#include "InSANESystematic.h"

//______________________________________________________________________________

InSANESystematic::InSANESystematic(const char * n, const char * t) : 
   TNamed(n,t), fNumber(0), fAbsoluteValue(0.0), fRelativeValue(0.0) 
{ }
//______________________________________________________________________________

InSANESystematic::InSANESystematic(const char * n, int N) : 
   TNamed(n,n), fNumber(N), fAbsoluteValue(0.0), fRelativeValue(0.0) 
{ }
//______________________________________________________________________________

InSANESystematic::~InSANESystematic()
{ }
//______________________________________________________________________________
InSANESystematic::InSANESystematic(const InSANESystematic& rhs) : TNamed(rhs)
{
  (*this) = rhs;
}
//______________________________________________________________________________

InSANESystematic& InSANESystematic::operator=(const InSANESystematic& rhs)
{
  if(this == &rhs) {
    return *this;
  }
  TNamed::operator=(rhs);
  fNumber                =  rhs.fNumber               ;
  fAbsoluteValue         =  rhs.fAbsoluteValue        ;
  fRelativeValue         =  rhs.fRelativeValue        ;
  fBefore                =  rhs.fBefore               ;
  fAfter                 =  rhs.fAfter                ;
  fValues                =  rhs.fValues               ;
  fUncertainties         =  rhs.fUncertainties        ;
  fRelativeUncertainties =  rhs.fRelativeUncertainties;
}
//______________________________________________________________________________

InSANESystematic& InSANESystematic::operator+=(const InSANESystematic &rhs)
{
   //TNamed::operator+=(rhs);

   return *this;
}
//_____________________________________________________________________________

const InSANESystematic InSANESystematic::operator+(const InSANESystematic &other)
{
   InSANESystematic result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
   result += other;            // Use += to add other to the copy.
   return result;              // All done!
}
//_____________________________________________________________________________

void InSANESystematic::InitHists(const TH1F& h)
{
   bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   TH1F c = h;
   c.Reset();
   c.SetNameTitle(Form("Before_%s",GetName()),Form("Before_%s",GetName()));
   fBefore         = c;
   c.SetNameTitle(Form("After_%s",GetName()),Form("After_%s",GetName()));
   fAfter          = c;
   c.SetNameTitle(Form("Values_%s",GetName()),Form("Values_%s",GetName()));
   fValues         = c;
   c.SetNameTitle(Form("Uncert_%s",GetName()),Form("Uncert_%s",GetName()));
   fUncertainties  = c;
   c.SetNameTitle(Form("RelUncert_%s",GetName()),Form("RelUncert_%s",GetName()));
   fRelativeUncertainties = c;
   Reset();
   TH1::AddDirectory(add_status);
}
//______________________________________________________________________________

void InSANESystematic::Reset(Option_t * opt)
{
   fBefore.Reset(opt);
   fAfter.Reset(opt);
   fValues.Reset(opt);
   fUncertainties.Reset(opt);
   fRelativeUncertainties.Reset(opt);
}
//______________________________________________________________________________

void InSANESystematic::Scale(double weight)
{
   fBefore                .Scale(weight);
   fAfter                 .Scale(weight);
   fValues                .Scale(weight);
   fUncertainties         .Scale(weight);
   fRelativeUncertainties .Scale(weight);
}
//______________________________________________________________________________

void InSANESystematic::Add(const InSANESystematic& sys, double weight)
{
   fRelativeUncertainties.Add(&sys.fRelativeUncertainties, weight ) ;
   fUncertainties.Add(        &sys.fUncertainties        , weight ) ;
   fValues.Add(               &sys.fValues               , weight ) ;
   fBefore.Add(               &sys.fBefore               , weight ) ;
   fAfter .Add(               &sys.fAfter                , weight ) ;
}
//______________________________________________________________________________

