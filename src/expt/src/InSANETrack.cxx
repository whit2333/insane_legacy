#include "InSANETrack.h"
#include <iostream>


ClassImp(InSANETrack)

//______________________________________________________________________________
InSANETrack::InSANETrack() {
   fRunNumber = 0;
   fNPositions = 0;
   fHitPositions = new TClonesArray("InSANEHitPosition",1);
   Clear();
}
//______________________________________________________________________________
InSANETrack::InSANETrack(const InSANETrack& rhs) {
   (*this) = rhs;
}
//______________________________________________________________________________
InSANETrack::~InSANETrack() {
}
//______________________________________________________________________________
InSANETrack& InSANETrack::operator=(const InSANETrack &rhs) {
   if (this == &rhs)
      return *this;
   fPosition0    = rhs.fPosition0;
   fMomentum     = rhs.fMomentum;
   fRunNumber    = rhs.fRunNumber;
   fEventNumber  = rhs.fEventNumber;
   fNPositions   = rhs.fNPositions;
   fNCherenkovElectrons = rhs.fNCherenkovElectrons;
   fCherenkovTDC = rhs.fCherenkovTDC;
   fIsNoisyChannel = rhs.fIsNoisyChannel;

   return *this;
}
//______________________________________________________________________________
void InSANETrack::Clear(Option_t * opt) {
   fHitPositions->Clear("C");
   //if (fHitPositions)if(fHitPositions->GetEntries() > 10 ) {
   //    std::cout << " calling  fHitPositions->ExpandCreate(2); \n";
   //    std::cout << " had " << fHitPositions->GetEntries() << " entries. \n";
   //    //fHitPositions->ExpandCreate(50);
   //}
   fPosition0.SetXYZ(0, 0, 0);
   fMomentum.SetXYZT(0, 0, 0, 0);
   fIsNoisyChannel      = false;
   fNPositions          = 0;
   fCherenkovTDC        = -4000;
   fEventNumber         = 0;
   fNCherenkovElectrons = 0;
   fClusterNumber       = 0;
}
//______________________________________________________________________________

//______________________________________________________________________________

//______________________________________________________________________________

