#include "SANEAsymmetryCalculation.h"


ClassImp(SANEAsymmetryCalculation1)

//__________________________________________________________________________

Int_t SANEAsymmetryCalculation1::Calculate()
{

   fcGoodElectron = false;
   if (fTrajectory)
      if (!(fEvents->TRIG->IsBETAEvent()))  return(0);
   if (!(fTrajectory->fIsGood)) return(0);

   /*
   fHelicity = fTrajectory->fHelicity;
   fx        = fTrajectory->GetBjorkenX();
   fW        = TMath::Sqrt(fTrajectory->GetInvariantMassSquared())/1000.0;
   fQsquared = fTrajectory->GetQSquared()/1000000.0;
   fTheta    = fTrajectory->GetTheta();
   fPhi      = fTrajectory->GetPhi();
   fEnergy   = fTrajectory->GetEnergy();
   */
   fDISEvent->fHelicity = fTrajectory->fHelicity;
   fDISEvent->fx        = fTrajectory->GetBjorkenX();
   fDISEvent->fW        = TMath::Sqrt(fTrajectory->GetInvariantMassSquared()) / 1000.0;
   fDISEvent->fQ2       = fTrajectory->GetQSquared() / 1000000.0;
   fDISEvent->fTheta    = fTrajectory->GetTheta();
   fDISEvent->fPhi      = fTrajectory->GetPhi();
   fDISEvent->fEnergy   = fTrajectory->GetEnergy() / 1000.0;
   fDISEvent->fGroup    = fTrajectory->fSubDetector;

   for (int i = 0; i < 8 && i < fAsymmetries->GetEntries(); i++) {
      ((SANEMeasuredAsymmetry*)(fAsymmetries->At(i)))->CountEvent();
   }


   if(fDISEvent->fTheta/degree  < 40.0 ) fDISEvent->fGroup    = 3;
   else fDISEvent->fGroup    = 4;

   for (int i = 8; i < 14 && i < fAsymmetries->GetEntries(); i++) {
      ((SANEMeasuredAsymmetry*)(fAsymmetries->At(i)))->CountEvent();
   }

//    fcGoodElectron=true;

   return(0);
}


// ClassImp(SANEAsymmetryCalculation2)


