#define BETAG4DetectorsAnalysis_cxx
#include "SANEEvents.h"

#include "BETAG4DetectorsAnalysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "BETAEvent.h"
#include "GasCherenkovHit.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "TClonesArray.h"

ClassImp(BETAG4DetectorsAnalysis)

//_______________________________________________________//

BETAG4DetectorsAnalysis::BETAG4DetectorsAnalysis() : BETAG4Analysis(0)
{
   printf("-! No Run Number Given\n");


/// \todo{The following or something like it should be put in an ABC}
   printf("  - Output File: data/montecarlo/BETAG4.run.%d.root\n", fRunNumber);
   fOutputFile = new TFile(Form("data/montecarlo/BETAG4.run.%d.root", fRunNumber), "RECREATE");
   if (fOutputFile)  printf("  + Output File Opened\n");

//Make sure this comes after opening the file
   fOutputTree = new TTree("cherenkovEvents", "Detector Event Data");
   fOutputTree->Branch("cherenkovBetaDetectorEvent", "BETAEvent", &(events->BETA));
   fOutputTree->Branch("cherenkovTrigDetectorEvent", "InSANETriggerEvent", &events->TRIG);
   fOutputTree->Branch("cherenkovHmsDetectorEvent", "HMSEvent", &events->HMS);
   fOutputTree->Branch("cherenkovBeamDetectorEvent", "HallCBeamEvent", &events->BEAM);
   fOutputTree->SetAutoSave();


}
//_______________________________________________________//

BETAG4DetectorsAnalysis::BETAG4DetectorsAnalysis(Int_t run) : BETAG4Analysis(run)
{
/// \todo{The following or something like it should be put in an ABC}
   printf("  - Output File: data/rootfiles/INSANE.run.%d.root\n", fRunNumber);
   fOutputFile = new TFile(Form("data/montecarlo/INSANE.run.%d.root", fRunNumber), "RECREATE");
   if (fOutputFile)  printf("  + Output File Opened\n");

//Make sure this comes after opening the file
   fOutputTree = new TTree("betaG4Events", "Detector Event Data");
   fOutputTree->Branch("betaG4DetectorEvent", "BETAEvent", &(events->BETA));
   fOutputTree->Branch("cherenkovTrigDetectorEvent", "InSANETriggerEvent", &(events->TRIG));
   fOutputTree->Branch("cherenkovHmsDetectorEvent", "HMSEvent", &(events->HMS));
   fOutputTree->Branch("cherenkovBeamDetectorEvent", "HallCBeamEvent", &(events->BEAM));
   fOutputTree->SetAutoSave();



}
//_______________________________________________________//

BETAG4DetectorsAnalysis::~BETAG4DetectorsAnalysis()
{
   fRun->Write();

}
//_______________________________________________________//

Int_t BETAG4DetectorsAnalysis::AnalyzeRun()
{
   // Detectors used
   std::cout << " Creating detector configurations for this run... \n";
   //ForwardTrackerDetector *    tracker   = new ForwardTrackerDetector(fRunNumber);
   //GasCherenkovDetector *      cherenkov = new GasCherenkovDetector(fRunNumber);
   //LuciteHodoscopeDetector *   hodoscope = new LuciteHodoscopeDetector(fRunNumber);
   //BigcalDetector *            bigcal    = new BigcalDetector(fRunNumber);

   aBetaEvent = events->BETA;
   bcEvent = events->BETA->fBigcalEvent;
   lhEvent = events->BETA->fLuciteHodoscopeEvent;
   gcEvent = events->BETA->fGasCherenkovEvent;
   ftEvent = events->BETA->fForwardTrackerEvent;
   for (int i = 0; i < 12; i++) {
      cer_tdc_hist[i] = new TH1F(Form("cer_tdc_hist_%d", i), Form("Cherenkov TDC %d", i), 200, -2500, -500);
      cer_adc_hist[i] = new TH1F(Form("cer_adc_%d", i), Form("Cherenkov  ADC %d", i), 100, 0, 1000);
      cer_tdc_vs_adc_hist[i] = new TH2F(Form("cer_tdc_vs_adc_hist_%d", i), Form("Cherenkov TDC vs ADC%d", i), 200, 0, 1000, 200, -2500, -500);
   }
///// EVENT LOOP  //////
   Int_t nevent = events->fTree->GetEntries();
   std::cout << "\n" << nevent << " ENTRIES \n";
   TClonesArray * cerHits = gcEvent->fGasCherenkovHits;
   TClonesArray * lucHits = lhEvent->fLuciteHits;
   //TClonesArray * bigcalHits = bcEvent->fBigcalHits;
   //TClonesArray * trackerHits = ftEvent->fTrackerHits;

   for (int iEVENT = 0; iEVENT < nevent; iEVENT++)     {
      aBetaEvent->ClearEvent();
      events->GetEvent(iEVENT);
      std::cout << "\n" << gcEvent->fNumberOfHits << " HITS \n";

// loop over cherenkov hits for event
      for (int kk = 0; kk < gcEvent->fNumberOfHits; kk++) {
         aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
//      if( TMath::Abs(aCerHit->fTDC - cherenkov->cherenkovTimings[aCerHit->fMirrorNumber-1]->fTDCPeak) <  5.0*cherenkov->cherenkovTimings[aCerHit->fMirrorNumber-1]->fTDCPeakWidth )
//      { //passes VERY wide tdc cut
         cer_adc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fADC);
         cer_tdc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fTDC);
         cer_tdc_vs_adc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fADC, aCerHit->fTDC);
         //Fill separte 12 cherenkov hists for each bar
//   if(hodoscope->fLucHit) {
//        for(int jj=0;jj<56;jj++) if(hodoscope->fBarTimedTDCHit[jj]) {
         /*printf(" ner");*/
//         cer_adc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fADC);
//         cer_tdc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fTDC);
//         cer_tdc_vs_adc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fADC,aCerHit->fTDC);
//        }
//     }// end lucCut
//   }
//    fOutputTree->Fill();
      } // End loop over Cherenkov signals


      if (events->TRIG->IsBETAEvent()) {  // BETA
         // Loop over lucite hits
         for (int kk = 0; kk < lhEvent->fNumberOfHits; kk++) {
            aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;
//do something
         } // End loop over Lucite Hits
      } // beta trigger bit
   } // END OF EVENT LOOP

   return(0);
}
//_______________________________________________________//

Int_t BETAG4DetectorsAnalysis::Visualize()
{
   auto * c1 = new TCanvas("Vis", "Visualize Results");

   c1->Divide(2, 3);

   c1->cd(1);
   events->fTree->Draw("fGasCherenkovEvent.fNumberOfHits:fBigcalEvent.fTotalEnergyDeposited", "fGasCherenkovEvent->fHits[]->fTDC<8000", "colz");

   c1->cd(2);
   events->fTree->Draw("fGasCherenkovEvent->fNumberOfHits", "fGasCherenkovEvent->fHits[]->fTDC<8000");

   c1->cd(3);
   events->fTree->Draw("fGasCherenkovEvent->fNumberOfHits:fGasCherenkovEvent->fHits[]->fADC", "fGasCherenkovEvent->fHits[]->fTDC<8000", "colz");

   c1->cd(4);
   events->fTree->Draw("fGasCherenkovEvent->fHits[]->fADC:fGasCherenkovEvent->fHits[]->fTDC", "fGasCherenkovEvent->fHits[]->fTDC<8000", "colz");
   return(0);
}

