#include "BETAGeneralAnalyzer.h"

ClassImp(BETAGeneralAnalyzer)

//_____________________________________________________________________________

BETAGeneralAnalyzer::BETAGeneralAnalyzer(const char * newTreeName, const char * inputTreeName) 
   : InSANEAnalyzer(newTreeName,inputTreeName){

  if (InSANERunManager::GetRunManager()->GetVerbosity() > 0) std::cout << " o BETAGeneralAnalyzer Constructor\n";

   fEvents = new SANEEvents(fInputTreeName.Data());

   /// Find and set the input tree
   if (!fInputTree) fInputTree = (TTree*)gROOT->FindObject(fInputTreeName.Data());
   if (!fInputTree) Error("BETAGeneralAnalyzer", "Tree:%s  was NOT FOUND! (from SANESecondPassAnalyzer c'tor)", fInputTreeName.Data());

   /// Go to scaler file and get the tree
   TFile * sf = InSANERunManager::GetRunManager()->GetScalerFile();
   if (sf) sf->cd();
   else Error("BETAGeneralAnalyzer"," NO SCALER FILE ?!?!?!");
   fScalers = new SANEScalers(fScalerTreeName.Data());
   if (!fScalerTree) fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName.Data());
   fScalers->SetBranches(fScalerTree);
   fOutputScalerTreeName = "Scalers0-1";

   InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
}
//_____________________________________________________________________________

BETAGeneralAnalyzer::~BETAGeneralAnalyzer(){
}
//_____________________________________________________________________________

void BETAGeneralAnalyzer::Initialize(TTree * t){

   if (t)fInputTree = t;

   if (fInputTree) {
      fCalculatedTree = fInputTree->CloneTree(0);
      fCalculatedTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s", fCalculatedTreeName.Data()));
   } else {
      printf("x- No Input tree ! \n");
   }

   InitCorrections();

   InitCalculations();

   /// Important: Set the event to be used for triggering in the InSANEAnalyzer
   if(fScalers && fEvents){ 
      InSANEAnalyzer::SetTriggerEvent(fScalers->fTriggerEvent);
      InSANEAnalyzer::SetDetectorTriggerEvent(fEvents->TRIG);
      InSANEAnalyzer::SetScalerTriggerEvent(fScalers->fTriggerEvent);
   }

}
//_____________________________________________________________________________
void BETAGeneralAnalyzer::MakePlots(){

}

//_____________________________________________________________________________


//_____________________________________________________________________________
