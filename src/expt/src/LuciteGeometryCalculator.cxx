#include "LuciteGeometryCalculator.h"
#include <TObject.h>
#include <iostream>
#include "InSANESystemOfUnits.h"

ClassImp(LuciteGeometryCalculator)

LuciteGeometryCalculator * LuciteGeometryCalculator::fgLuciteGeometryCalculator = nullptr;

//_______________________________________________________________________________
LuciteGeometryCalculator::LuciteGeometryCalculator() {
      fBarRadius              = 240.0; // cm
      fBarThickness           = 3.5; //cm
      fBarHeight              = 6.0; //cm
      fBarArcLength           = 23.0*degree;
      fLuciteZPosition        = 253.57; //cm
      fDistanceFromTarget     = fLuciteZPosition;
      fBarWrapThickness       = 0.01;

      fLightGuideLength       = 10.0; ///cm
      fLightGuideEndHeight    = 6.0;  ///cm 
      fLightGudieEndWidth     = 4.9;  ///cm
      fLightGuideDiameter     = 4.9;  ///cm
      fTotalLightLength       = fLuciteZPosition*fBarArcLength + 2.0* fLightGuideLength;
      
      fVerticalOffset         = -0.90;
      fHorizontalOffset       = 0.33;
      
      fSurveyYaw              =-1.776*degree;
      fSurveyPitch            =-0.412*degree;
      fSurveyRoll             =-0.015*degree;

   fDetectorRotation.SetToIdentity();
   fDetectorRotation.RotateY(1.0*fSurveyYaw);
   fDetectorRotation.RotateX(-1.0*fSurveyPitch);
   fDetectorRotation.RotateZ(1.0*fSurveyRoll);

   }
//_______________________________________________________________________________

LuciteGeometryCalculator * LuciteGeometryCalculator::GetCalculator()
{
   if (!fgLuciteGeometryCalculator) fgLuciteGeometryCalculator = new LuciteGeometryCalculator();
   return(fgLuciteGeometryCalculator);
}
//_______________________________________________________________________________

LuciteGeometryCalculator::~LuciteGeometryCalculator()
{
   ;
}
//_______________________________________________________________________________
