#include  "InSANETriggerCounter.h"

//______________________________________________________________________________
ClassImp(InSANETriggerCounter)

//______________________________________________________________________________
InSANETriggerCounter::InSANETriggerCounter() {
   fLiveTimeAverage = 0.0;
   fDeadTimeAverage = 0.0;
   Clear();
}
//______________________________________________________________________________
InSANETriggerCounter::~InSANETriggerCounter() {
}
//______________________________________________________________________________
void InSANETriggerCounter::ClearCounts(Option_t *opt) {
   fTriggerCount = 0;
   fPositiveHelicityTriggerCount = 0;
   fNegativeHelicityTriggerCount = 0;
   fLiveTime = 0.0;
   fDeadTime = 0.0;
   fNegativeHelicityLiveTime = 0.0;
   fNegativeHelicityDeadTime = 0.0;
   fPositiveHelicityLiveTime = 0.0;
   fPositiveHelicityDeadTime = 0.0;
   fHasGoodDeadTime = true;
}
//______________________________________________________________________________
void InSANETriggerCounter::Clear(Option_t *opt) {
   fScaler = 0;
   fNegativeHelicityScaler = 0;
   fPositiveHelicityScaler = 0;
   fHasGoodDeadTime = 0;
   ClearCounts(opt);
}
//______________________________________________________________________________
void InSANETriggerCounter::Count(Int_t hel) {
   if (hel == 1) fPositiveHelicityTriggerCount++;
   else if (hel == -1) fNegativeHelicityTriggerCount++;
   fTriggerCount++;
}
//______________________________________________________________________________
void InSANETriggerCounter::Calculate() {
   /// Helicity independent dead/live times
   if (fScaler == 0) {
      fDeadTime = 0.0;
      fLiveTime = 1.0;
      fHasGoodDeadTime = false;
   } else {
      fLiveTime = ((double)fTriggerCount) / ((double)fScaler);
      fDeadTime = 1.0 - fLiveTime;
   }
   if (fDeadTime > 1.0 || fDeadTime < 0.0) {
      Warning("Calculate()", "Unrealistic fDeadTime = %f! Setting to 1.0.", fDeadTime);
      fLiveTime = 0.0;
      fDeadTime = 1.0 - fLiveTime;
      fHasGoodDeadTime = false;
   }


   /// Positive helicity dead/live times
   if (fScaler == 0) {
      fPositiveHelicityDeadTime = 0.0;
      fPositiveHelicityLiveTime = 1.0;
      //fHasGoodDeadTime = false;
   } else {
      fPositiveHelicityLiveTime = ((double)fPositiveHelicityTriggerCount) / ((double)fPositiveHelicityScaler);
      fPositiveHelicityDeadTime = 1.0 - fPositiveHelicityLiveTime;
   }
   if (fPositiveHelicityDeadTime > 1.0 || fPositiveHelicityDeadTime < 0.0) {
      //       Warning("Calculate()",Form("Unrealistic fPositiveHelicityDeadTime = %f ! Setting to 1.0.\n",fPositiveHelicityDeadTime));
      fPositiveHelicityLiveTime = 0.0;
      fPositiveHelicityDeadTime = 1.0 - fPositiveHelicityLiveTime;
      //fHasGoodDeadTime = false;
   }

   /// Negative helicity dead/live times
   if (fScaler == 0) {
      fNegativeHelicityDeadTime = 0.0;
      fNegativeHelicityLiveTime = 1.0;
   } else {
      fNegativeHelicityLiveTime = ((double)fNegativeHelicityTriggerCount) / ((double)fNegativeHelicityScaler);
      fNegativeHelicityDeadTime = 1.0 - fNegativeHelicityLiveTime;
   }
   if (fNegativeHelicityDeadTime > 1.0 || fNegativeHelicityDeadTime < 0.0) {
      //         Warning("Calculate()",Form("Unrealistic fNegativeHelicityDeadTime = %f ! Setting to 1.0.\n",fNegativeHelicityDeadTime));
      fNegativeHelicityLiveTime = 0.0;
      fNegativeHelicityDeadTime = 1.0 - fNegativeHelicityLiveTime;
      //fHasGoodDeadTime = false;
   }

   //       fLiveTimeAverage = (fLiveTimeAverage*((double)(fIntervalNumber-1))+fLiveTime)/((double)fIntervalNumber);
   //       fDeadTimeAverage = (fDeadTimeAverage*((double)(fIntervalNumber-1))+fDeadTime)/((double)fIntervalNumber);
}
//______________________________________________________________________________
void InSANETriggerCounter::UpdateLiveTimeAverages(Double_t * lt, Double_t * ltp, Double_t * ltm, Int_t i) {
   if (fHasGoodDeadTime) {
      (*lt) = ((*lt) * ((double)(i - 1)) + fLiveTime) / ((double)i);
      (*ltp) = ((*ltp) * ((double)(i - 1)) + fPositiveHelicityLiveTime) / ((double)i);
      (*ltm) = ((*ltm) * ((double)(i - 1)) + fNegativeHelicityLiveTime) / ((double)i);
   }
   fLiveTimeAverage = (*lt);
   fLiveTimeAverageMinus = (*ltm);
   fLiveTimeAveragePlus = (*ltp);
   fDeadTimeAverage = 1.0 -  fLiveTimeAverage;
   fDeadTimeAveragePlus = 1.0 -  fLiveTimeAveragePlus;
   fDeadTimeAverageMinus = 1.0 -  fLiveTimeAverageMinus;
}
//______________________________________________________________________________

