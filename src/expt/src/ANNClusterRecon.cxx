#include "ANNClusterRecon.h"
#include "BigcalCoordinateSystem.h"
#include "BIGCALGeometryCalculator.h"

//______________________________________________________________________________
ANNClusterRecon::ANNClusterRecon() : fIsPerp(false) {
   fBeamEvent = nullptr;

   BigcalCoordinateSystem bcCoords;
   fRotFromBCcoords  = bcCoords.GetRotationMatrixTo();
   fBigcalZ = BIGCALGeometryCalculator::GetCalculator()->GetDistanceFromTarget();
}
//______________________________________________________________________________
ANNClusterRecon::~ANNClusterRecon(){
}
//______________________________________________________________________________
const BIGCALCluster& ANNClusterRecon::AsPhoton(){

   // CLUSTER --> Bigcal Plane
   fANNGammaEvent.SetEventValues(&fCluster); 
   fANNGammaEvent.fXClusterKurt = fCluster.fXKurtosis;
   fANNGammaEvent.fYClusterKurt = fCluster.fYKurtosis;

   fANNGammaEvent.GetMLPInputNeuronArray(fNNParams);
   if(fIsPerp) {
      fCluster.fDeltaX  = fNNPerpDeltaX.Value(0,fNNParams);
      fCluster.fDeltaY  = fNNPerpDeltaY.Value(0,fNNParams);
      fCluster.fDeltaE  = 0.0;//fNNPerpDeltaE.Value(0,fNNParams);
   } else {
      fCluster.fDeltaX  = fNNParaDeltaX.Value(0,fNNParams);
      fCluster.fDeltaY  = fNNParaDeltaY.Value(0,fNNParams);
      fCluster.fDeltaE  = 0.0;//fNNParaDeltaE.Value(0,fNNParams);
   }
   return(fCluster);
}
//______________________________________________________________________________
const BIGCALCluster& ANNClusterRecon::AsElectron(){

   // CLUSTER --> Bigcal Plane
   fANNDisElectronEvent.SetEventValues(&fCluster); 
   fANNDisElectronEvent.fXClusterKurt = fCluster.fXKurtosis;
   fANNDisElectronEvent.fYClusterKurt = fCluster.fYKurtosis;
   fANNDisElectronEvent.fNClusters    = 1;//fClusterEvent->fNClusters; // is this needed in the input neurons?
   if(fBeamEvent){
      fANNDisElectronEvent.fRasterX      = fBeamEvent->fRasterPosition.X();
      fANNDisElectronEvent.fRasterY      = fBeamEvent->fRasterPosition.Y();
   } else {
      fANNDisElectronEvent.fRasterX      = 0.0;
      fANNDisElectronEvent.fRasterY      = 0.0;
   }

   fANNDisElectronEvent.GetMLPInputNeuronArray(fNNParams);

   if(fIsPerp) {
      fCluster.fDeltaE  = 0.0;//fNNPerpElectronClusterDeltaE.Value(0,fNNParams);
      fCluster.fDeltaX  = fNNPerpElectronClusterDeltaX.Value(0,fNNParams);
      fCluster.fDeltaY  = fNNPerpElectronClusterDeltaY.Value(0,fNNParams);
   } else {
      fCluster.fDeltaE  = 0.0;//fNNParaElectronClusterDeltaE.Value(0,fNNParams);
      fCluster.fDeltaX  = fNNParaElectronClusterDeltaX.Value(0,fNNParams);
      fCluster.fDeltaY  = fNNParaElectronClusterDeltaY.Value(0,fNNParams);
   }
   //fTrajectory->fEnergy      = aCluster->fTotalE + aCluster->fDeltaE ;
   //fTrajectory->fDeltaEnergy = aCluster->fDeltaE ;
   //fTrack->fEnergy           = aCluster->fTotalE + aCluster->fDeltaE ;
   //fTrack->fDeltaEnergy      = aCluster->fDeltaE ;

   // ----------------------------------------------------
   // Bigcal Plane --> Target variables
   if(fBeamEvent) fX_Vertex.SetXYZ(fBeamEvent->fRasterPosition.X(),fBeamEvent->fRasterPosition.Y(),0.0);
   else fX_Vertex.SetXYZ(0.0,0.0,0.0);
   fX_BigcalPlane.SetXYZ(fCluster.GetXmoment()+fCluster.fDeltaX,fCluster.GetYmoment()+fCluster.fDeltaY, fBigcalZ);
   fX_BigcalPlane.Transform(fRotFromBCcoords);
   //fX_BigcalPlane                          = fBigcalDetector->GetXYCorrectedPosition(aCluster);
   //fTrajectory->fXBigcal_B                 = fx_bigcalplane;
   //fTrack->fXBigcal_B                 = fx_bigcalplane;
   fP_Naive                                = fX_BigcalPlane - fX_Vertex; 
   fANNFieldEvent2.fBigcalPlaneTheta       = fP_Naive.Theta();//bigcalPlane->fPosition.Theta();
   fANNFieldEvent2.fBigcalPlanePhi         = fP_Naive.Phi();//bigcalPlane->fPosition.Phi();
   fANNFieldEvent2.fBigcalPlaneX           = fX_BigcalPlane.X();
   fANNFieldEvent2.fBigcalPlaneY           = fX_BigcalPlane.Y();
   fANNFieldEvent2.fBigcalPlaneEnergy      = fCluster.GetCorrectedEnergy();//bigcalPlane->fEnergy;
   fANNFieldEvent2.fRasterX                = fX_Vertex.X();
   fANNFieldEvent2.fRasterY                = fX_Vertex.Y();
   fANNFieldEvent2.GetMLPInputNeuronArray(fNNParams);
   if(fIsPerp) {
      fCluster.fDeltaTheta = fNNPerpElectronDeltaTheta.Value(0,fNNParams);
      fCluster.fDeltaPhi   = fNNPerpElectronDeltaPhi.Value(0,fNNParams);
   } else {
      fCluster.fDeltaTheta = fNNParaElectronDeltaTheta.Value(0,fNNParams);
      fCluster.fDeltaPhi   = fNNParaElectronDeltaPhi.Value(0,fNNParams);
   }
   return(fCluster);
}
//______________________________________________________________________________
const BIGCALCluster& ANNClusterRecon::AsPositron(){
   // NOT DONE
   return(fCluster);
}
//______________________________________________________________________________

