#include "HallCBeam.h"
#include "InSANEDetectorComponent.h"

ClassImp(HallCRasteredBeam)

HallCRasteredBeam::HallCRasteredBeam(const char * name, const char * title) : HallCPolarizedElectronBeam(name, title)
{
   fSRSize     = 1.0;
   fSRXSlope   = -1008;
   fSRYSlope   = -1008;
   fSRXADCZero = 6109;
   fSRYADCZero = 6668;
   fSRXOffset  = -0.13;
   fSRYOffset  = 0.068;
   fFRSize     = 0.15;
   fFRXSlope   = 157;
   fFRYSlope   = 146;
   fFRXADCZero = 3744;
   fFRYADCZero = 3590;
   fFRXOffset  = 0.0;
   fFRYOffset  = 0.0;

   auto * srxadc = new InSANEDetectorComponent("XsrADC", "X Slow raster adc", 1);
   fComponents.Add(srxadc);
   auto * sryadc = new InSANEDetectorComponent("YsrADC", "Y Slow raster adc", 2);
   fComponents.Add(sryadc);
   auto * frxadc = new InSANEDetectorComponent("XfrADC", "X Fast raster adc", 1);
   fComponents.Add(frxadc);
   auto * fryadc = new InSANEDetectorComponent("YfrADC", "Y Fast raster adc", 2);
   fComponents.Add(fryadc);
}
//_______________________________________________________________________________________

HallCRasteredBeam::~HallCRasteredBeam()
{

}
//_______________________________________________________________________________________

void HallCRasteredBeam::Print( const Option_t * opt) const
{
   InSANEApparatus::Print();
   std::cout << " Slow Raster : \n";
   std::cout << "   size    = " << fSRSize   << " cm\n";
   std::cout << "   xslope  = " << fSRXSlope << " cm\n";
   std::cout << "   yslope  = " << fSRYSlope << " cm\n";
   std::cout << "   xzero   = " << fSRXADCZero << " cm\n";
   std::cout << "   yzero   = " << fSRYADCZero << " cm\n";
   std::cout << "   xoffset = " << fSRXOffset << " cm\n";
   std::cout << "   yoffset = " << fSRYOffset << " cm\n";
}



