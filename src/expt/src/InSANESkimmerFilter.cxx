#include "InSANESkimmerFilter.h"


ClassImp(InSANESkimmerFilter)

//______________________________________________________________________________
InSANESkimmerFilter::InSANESkimmerFilter(const char * n, const char * t) : TNamed(n,t) {
   fIsSkipFlag   = false;
   fIsFlagRaised = false;
   fScalerEvent  = nullptr;
   fFilterState  = InSANESkimmerFilter::kPassesFilter;
   fRunFlag      = nullptr;
}
//______________________________________________________________________________
InSANESkimmerFilter::~InSANESkimmerFilter(){
}
//______________________________________________________________________________
void InSANESkimmerFilter::DeleteExistingFlags() {
   TString query = " DELETE FROM run_flags ";
   query += " WHERE run_number=";
   query += InSANERunManager::GetRunManager()->fCurrentRunNumber;
   query += " AND event_title='";
   query += GetTitle();
   query += "' ;";
   InSANEDatabaseManager::GetManager()->ExecuteInsert(&query);
   //std::cout << query.Data() << std::endl;
}
//______________________________________________________________________________
void InSANESkimmerFilter::Initialize() {
   DeleteExistingFlags();
}
//______________________________________________________________________________
void InSANESkimmerFilter::SetScalerEvent(InSANEScalerEvent * ev) { fScalerEvent = ev; }
//______________________________________________________________________________
InSANEScalerEvent * InSANESkimmerFilter::GetScalerEvent() const { return fScalerEvent; }
//______________________________________________________________________________
Bool_t InSANESkimmerFilter::IsSkipFlag() const { return fIsSkipFlag; }
//______________________________________________________________________________
Bool_t InSANESkimmerFilter::IsEventFlaggedSkip() const {
   if (!fIsSkipFlag) return false;
   else return(fIsFlagRaised);
}
//______________________________________________________________________________
InSANERunFlag * InSANESkimmerFilter::RaiseRunFlag() {
   fIsFlagRaised = true;
   CreateRunFlag();
   if (fRunFlag)fRunFlag->RaiseFlag();
   return(fRunFlag);
}
//______________________________________________________________________________
InSANERunFlag * InSANESkimmerFilter::LowerRunFlag() {
   fIsFlagRaised = false;
   fRunFlag->fEndingEvent = fScalerEvent->fEventNumber;
   if (fRunFlag)fRunFlag->LowerFlag();
   return(fRunFlag);
}
//______________________________________________________________________________



ClassImp(InSANEBeamCurrentFilter)

//______________________________________________________________________________
InSANEBeamCurrentFilter::InSANEBeamCurrentFilter(const char * n, const char * t) 
   : InSANESkimmerFilter(n,t) {
   fIsSkipFlag       = true;
   fFlagNumber       = 0;
   fCurrentThreshold = 60.0;
   SetNameTitle("beamCurrentFilter", Form("%d nA Beam Trip", (int)fCurrentThreshold));
}
//______________________________________________________________________________
InSANEBeamCurrentFilter::~InSANEBeamCurrentFilter() {
}
//______________________________________________________________________________
void InSANEBeamCurrentFilter::CreateRunFlag() {
   fRunFlag = new InSANERunFlag();
   if (fRunFlag) {
      InSANERunManager::GetRunManager()->GetCurrentRun()->GetRunFlags()->Add(fRunFlag);
      fRunFlag->fStartingEvent = fScalerEvent->fEventNumber;
      fRunFlag->SetNameTitle(Form("beamTrip-%d",fFlagNumber), Form("%d nA Beam Trip - %d", (int)fCurrentThreshold,fFlagNumber));
      fRunFlag->fSetValue = fCurrentThreshold;
      fRunFlag->fValue = fScalerEvent->GetBeamCurrent();
      fRunFlag->fRunNumber = fScalerEvent->fRunNumber;
      fFlagNumber++;
   }
}
//______________________________________________________________________________
int InSANEBeamCurrentFilter::Process() {
   if (fIsFlagRaised)
      if (fScalerEvent->GetBeamCurrent() > fCurrentThreshold) LowerRunFlag();
   if (!fIsFlagRaised)
      if (fScalerEvent->GetBeamCurrent() < fCurrentThreshold) {
         RaiseRunFlag();
      }
   return(0);
}
//______________________________________________________________________________


