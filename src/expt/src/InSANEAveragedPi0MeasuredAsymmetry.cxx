#include "InSANEAveragedPi0MeasuredAsymmetry.h"
#include "InSANEMathFunc.h"

ClassImp(InSANEAveragedPi0MeasuredAsymmetry)

//_____________________________________________________________________________
InSANEAveragedPi0MeasuredAsymmetry::InSANEAveragedPi0MeasuredAsymmetry() {
   fAsymmetryResult = nullptr;
   fNAsymmetries = 0;

   fATheta1 = nullptr;
   fATheta2 = nullptr;
   fDFTheta1 = nullptr;
   fDFTheta2 = nullptr;

   fAPhi1 = nullptr;
   fAPhi2 = nullptr;
   fDFPhi1 = nullptr;
   fDFPhi2 = nullptr;

   fAPt1 = nullptr;
   fAPt2 = nullptr;
   fDFPt1 = nullptr;
   fDFPt2 = nullptr;

   fAE1 = nullptr;
   fAE2 = nullptr;
   fDFE1 = nullptr;
   fDFE2 = nullptr;
}
//_____________________________________________________________________________
InSANEAveragedPi0MeasuredAsymmetry::~InSANEAveragedPi0MeasuredAsymmetry() {
   if(fAsymmetryResult) delete fAsymmetryResult;
   fAsymmetryResult = nullptr;
   if(fATheta1) delete fATheta1;
   if(fATheta2) delete fATheta2;
   if(fAPhi1) delete fAPhi1;
   if(fAPhi2) delete fAPhi2;
   if(fAPt1) delete fAPt1;
   if(fAPt2) delete fAPt2;
   if(fAE1) delete fAE1;
   if(fAE2) delete fAE2;
   if(fDFTheta1) delete fDFTheta1;
   if(fDFTheta2) delete fDFTheta2;
   if(fDFPhi1) delete fDFPhi1;
   if(fDFPhi2) delete fDFPhi2;
   if(fDFPt1) delete fDFPt1;
   if(fDFPt2) delete fDFPt2;
   if(fDFE1) delete fDFE1;
   if(fDFE2) delete fDFE2;
   //... need to finish
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::InitHists(TH1F * h0, TH1F ** h1, TH1F ** h2){
   if(!h0) return -1;
   (*h1) = new TH1F(*h0); // holds A/sigma^2
   (*h2) = new TH1F(*h0); // holds 1/(1/sigma^2)
   h0->Reset();
   (*h1)->Reset();
   (*h2)->Reset();
   return 0;
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::InitHists(TH2F * h0, TH2F ** h1, TH2F ** h2){
   if(!h0) return -1;
   (*h1) = new TH2F(*h0); // holds A/sigma^2
   (*h2) = new TH2F(*h0); // holds 1/(1/sigma^2)
   h0->Reset();
   (*h1)->Reset();
   (*h2)->Reset();
   return 0;
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::InitHists(TH3F * h0, TH3F ** h1, TH3F ** h2){
   if(!h0) return -1;
   (*h1) = new TH3F(*h0); // holds A/sigma^2
   (*h2) = new TH3F(*h0); // holds 1/(1/sigma^2)
   h0->Reset();
   (*h1)->Reset();
   (*h2)->Reset();
   return 0;
}
//_____________________________________________________________________________
void InSANEAveragedPi0MeasuredAsymmetry::Init(InSANEPi0MeasuredAsymmetry *a){
   // Initialize the resulting Asymmetry's histograms.
   fAsymmetryResult = new InSANEPi0MeasuredAsymmetry(*a);

   TH1::AddDirectory(kFALSE);

   // vs Theta
   InitHists(fAsymmetryResult->fAsymmetryVsTheta, &fATheta1,  &fATheta2);
   InitHists(fAsymmetryResult->fDilutionVsTheta, &fDFTheta1,&fDFTheta2);

   // vs Phi
   InitHists(fAsymmetryResult->fAsymmetryVsPhi, &fAPhi1,  &fAPhi2);
   InitHists(fAsymmetryResult->fDilutionVsPhi, &fDFPhi1,&fDFPhi2);

   // vs W 
   InitHists(fAsymmetryResult->fAsymmetryVsPt, &fAPt1, &fAPt2);
   InitHists(fAsymmetryResult->fDilutionVsPt,  &fDFPt1, &fDFPt2);

   // vs E 
   InitHists(fAsymmetryResult->fAsymmetryVsE, &fAE1, &fAE2);
   InitHists(fAsymmetryResult->fDilutionVsE,  &fDFE1, &fDFE2);

   TH1::AddDirectory(kTRUE);

}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::AddToSum(TH1F * h0, TH1F * h1, TH1F * h2){
   if(!h0) return -1;
   for(int i = 0; i<=h0->GetNbinsX(); i++) {
      Double_t Ai = h0->GetBinContent(i);
      Double_t si = h0->GetBinError(i);
      if(Ai == 0.0) continue; // skip bins with 0 entries... need to fix this hack
      if( TMath::IsNaN(Ai) ){
         Ai = 0;
         continue;
      }
      if( si==0 || TMath::IsNaN(si) ) si = 0.000001;
      Double_t At = h1->GetBinContent(i);
      Double_t st = h2->GetBinContent(i);
      h1->SetBinContent(i,At + Ai/(si*si) );
      h2->SetBinContent(i,st + 1.0/(si*si) );
   }
   return 0;
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::AddToSum(TH2F * h0, TH2F * h1, TH2F * h2){
   if(!h0) return -1;
   for(int i = 0; i <= h0->GetNbinsX(); i++) {
      for(int j = 0; j <= h0->GetNbinsY(); j++) {
         for(int k = 0; k <= h0->GetNbinsZ(); k++) {
            Double_t Ai = h0->GetBinContent(i,j,k);
            Double_t si = h0->GetBinError(i,j,k);
            if(Ai == 0.0) continue;
            if( TMath::IsNaN(Ai) ) { 
               Ai = 0;
               continue;
            }
            if( si==0 || TMath::IsNaN(si) ) si = 0.00000001;
            Double_t At = h1->GetBinContent(i,j,k);
            Double_t st = h2->GetBinContent(i,j,k);
            h1->SetBinContent(i,j,k,At + Ai/(si*si) );
            h2->SetBinContent(i,j,k,st + 1.0/(si*si) );
         }
      }
   }
   return 0;
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::AddToSum(TH3F * h0, TH3F * h1, TH3F * h2){
   if(!h0) return -1;
   for(int i = 0; i <= h0->GetNbinsX(); i++) {
      for(int j = 0; j <= h0->GetNbinsY(); j++) {
         for(int k = 0; k <= h0->GetNbinsZ(); k++) {
            Double_t Ai = h0->GetBinContent(i,j,k);
            Double_t si = h0->GetBinError(i,j,k);
            if(Ai == 0.0) continue;
            if( TMath::IsNaN(Ai) ) { 
               Ai = 0;
               continue;
            }
            if( si==0 || TMath::IsNaN(si) ) si = 0.00000001;
            Double_t At = h1->GetBinContent(i,j,k);
            Double_t st = h2->GetBinContent(i,j,k);
            h1->SetBinContent(i,j,k,At + Ai/(si*si) );
            h2->SetBinContent(i,j,k,st + 1.0/(si*si) );
         }
      }
   }
   return 0;
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::Add(InSANEPi0MeasuredAsymmetry *asym){

   if(!asym) return(-1);
   if( fNAsymmetries == 0 ) Init(asym); 

   if(fNAsymmetries > 0)(*(fAsymmetryResult->fAvgKineVsPt))     += (*(asym->fAvgKineVsPt));
   if(fNAsymmetries > 0)(*(fAsymmetryResult->fAvgKineVsE))     += (*(asym->fAvgKineVsE));
   if(fNAsymmetries > 0)(*(fAsymmetryResult->fAvgKineVsTheta)) += (*(asym->fAvgKineVsTheta));
   if(fNAsymmetries > 0)(*(fAsymmetryResult->fAvgKineVsPhi))   += (*(asym->fAvgKineVsPhi));

   fNAsymmetries++;

   //fAsymmetryResult->fRunSummary += asym->fRunSummary; 

   // vs Theta
   AddToSum(asym->fDilutionVsTheta,     fDFTheta1, fDFTheta2);
   AddToSum(asym->fAsymmetryVsTheta, fATheta1, fATheta2);

   // vs Phi
   AddToSum(asym->fDilutionVsPhi,     fDFPhi1, fDFPhi2);
   AddToSum(asym->fAsymmetryVsPhi, fAPhi1, fAPhi2);

   // vs Pt
   AddToSum(asym->fDilutionVsPt,     fDFPt1, fDFPt2);
   AddToSum(asym->fAsymmetryVsPt, fAPt1, fAPt2);

   // vs E
   AddToSum(asym->fDilutionVsE,     fDFE1, fDFE2);
   AddToSum(asym->fAsymmetryVsE, fAE1, fAE2);


   // Systematic error histograms
   if(fNAsymmetries > 1){

      (*fAsymmetryResult) += (*asym);

      fAsymmetryResult->fSystErrVsTheta->Add(asym->fSystErrVsTheta);
      fAsymmetryResult->fSystErrVsPhi->Add(asym->fSystErrVsPhi);
      fAsymmetryResult->fSystErrVsPt->Add(asym->fSystErrVsPt);
      fAsymmetryResult->fSystErrVsE->Add(asym->fSystErrVsE);
   }

   return(fNAsymmetries);
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::CalculateMean(TH1F * h0, TH1F * h1, TH1F * h2){
   for(int i = 0; i <= h0->GetNbinsX(); i++) {
      Double_t num = h1->GetBinContent(i);
      Double_t denom = h2->GetBinContent(i);
      if( denom==0 ) {
         denom = 0.00000001;
         num = 0.0;
      }
      h0->SetBinContent(i, num/denom );
      h0->SetBinError(i,   1.0/TMath::Sqrt(denom));
   }
   return 0;
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::CalculateMean(TH2F * h0, TH2F * h1, TH2F * h2){
   for(int i = 0; i<=h0->GetNbinsX(); i++) {
      for(int j = 0; j<=h0->GetNbinsY(); j++) {
         for(int k = 0; k<=h0->GetNbinsZ(); k++) {
            Double_t num   = h1->GetBinContent(i,j,k);
            Double_t denom = h2->GetBinContent(i,j,k);
            if( denom==0 ) {
               denom = 0.00000001;
               num = 0.0;
            }
            h0->SetBinContent(i,j,k,num/denom );
            h0->SetBinError(i,j,k,1.0/TMath::Sqrt(denom));
         }
      }
   }
   return 0;
}
//_____________________________________________________________________________
Int_t InSANEAveragedPi0MeasuredAsymmetry::CalculateMean(TH3F * h0, TH3F * h1, TH3F * h2){
   for(int i = 0; i<=h0->GetNbinsX(); i++) {
      for(int j = 0; j<=h0->GetNbinsY(); j++) {
         for(int k = 0; k<=h0->GetNbinsZ(); k++) {
            Double_t num   = h1->GetBinContent(i,j,k);
            Double_t denom = h2->GetBinContent(i,j,k);
            if( denom==0 ) {
               denom = 0.00000001;
               num = 0.0;
            }
            h0->SetBinContent(i,j,k,num/denom );
            h0->SetBinError(i,j,k,1.0/TMath::Sqrt(denom));
         }
      }
   }
   return 0;
}
//_____________________________________________________________________________
InSANEPi0MeasuredAsymmetry * InSANEAveragedPi0MeasuredAsymmetry::Calculate(){

   if( fNAsymmetries == 0 ) { Error("Calculate","No asymmetries added."); return (nullptr); }

   fAsymmetryResult->SetRunSummary(&(fAsymmetryResult->fRunSummary));

   // vs Theta
   CalculateMean(fAsymmetryResult->fDilutionVsTheta, fDFTheta1, fDFTheta2);
   CalculateMean(fAsymmetryResult->fAsymmetryVsTheta, fATheta1, fATheta2);

   // vs Phi
   CalculateMean(fAsymmetryResult->fDilutionVsPhi, fDFPhi1, fDFPhi2);
   CalculateMean(fAsymmetryResult->fAsymmetryVsPhi, fAPhi1, fAPhi2);

   // vs Pt
   CalculateMean(fAsymmetryResult->fDilutionVsPt, fDFPt1, fDFPt2);
   CalculateMean(fAsymmetryResult->fAsymmetryVsPt, fAPt1, fAPt2);

   // vs E
   CalculateMean(fAsymmetryResult->fDilutionVsE, fDFE1, fDFE2);
   CalculateMean(fAsymmetryResult->fAsymmetryVsE, fAE1, fAE2);


   // Systematic error histograms
   fAsymmetryResult->fSystErrVsTheta->    Scale( 1.0/double(fNAsymmetries));
   fAsymmetryResult->fSystErrVsPhi->    Scale( 1.0/double(fNAsymmetries));
   fAsymmetryResult->fSystErrVsPt->    Scale( 1.0/double(fNAsymmetries));
   fAsymmetryResult->fSystErrVsE->    Scale( 1.0/double(fNAsymmetries));

   return(fAsymmetryResult);
}
//_____________________________________________________________________________

