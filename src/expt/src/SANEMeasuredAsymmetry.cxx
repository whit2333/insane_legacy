#include "SANEMeasuredAsymmetry.h"
#include "InSANEPhysicalConstants.h"

ClassImp(SANEMeasuredAsymmetry)
//__________________________________________________________________
SANEMeasuredAsymmetry::SANEMeasuredAsymmetry(Int_t n) : InSANEMeasuredAsymmetry(n){

   C1           = 0.198;
   C2           = -0.093;
   fNxBins      = 0;
   fNWBins      = 0;
   fNThetaBins  = 0;
   fNEnergyBins = 0;
   fNNuBins     = 0;
   fNPhiBins    = 20;
   fNQ2Bins     = 5;
   fEnergyBins.clear();
   fNuBins.clear();
   fQ2Bins.clear();
   fxBins.clear();
   fThetaBins.clear();
   fWBins.clear();
   fPhiBins.clear();
   fEnergyBins.push_back(0);
   fNuBins.push_back(0);
   fxBins.push_back(0);
   fThetaBins.push_back(0);
   fWBins.push_back(0);
   fPhiBins.push_back(0);
   //SANEMeasuredAsymmetry::Initialize();
} 
//__________________________________________________________________

SANEMeasuredAsymmetry::~SANEMeasuredAsymmetry(){

}
//__________________________________________________________________
Bool_t SANEMeasuredAsymmetry::IsGoodEvent()
{
   if (!(fQ2Min < fDISEvent->fQ2 && fQ2Max > fDISEvent->fQ2)) return(false);
   //if( !(fEnergyMin < fDISEvent->fEnergy && fEnergyMax > fDISEvent->fEnergy) ) return(false);
   return(true);
}
//_____________________________________________________________________________
void SANEMeasuredAsymmetry::Initialize(){
   //if (SANERunManager::GetRunManager()->fVerbosity > 2)
   //   std::cout << " o SANEMeasuredAsymmetry::Initialize()\n";

   Int_t n = fNumber;
   using namespace CLHEP;
   Double_t nu_min,nu_max,x_min,x_max,W_min,W_max,E_min,E_max,theta_min,theta_max,phi_min,phi_max;
   Double_t x,nu,W,E,theta,phi,Q2,M;
   M = M_p/GeV; // proton mass in units of GeV/c^2

   /// Clear bins
   fEnergyBins.clear();
   fNuBins.clear();
   fQ2Bins.clear();
   fxBins.clear();
   fThetaBins.clear();
   fWBins.clear();
   fPhiBins.clear();

   fNxBins      = 0;
   fNWBins      = 0;
   fNThetaBins  = 0;
   fNEnergyBins = 0;
   fNNuBins     = 0;
   //fNPhiBins    = 0;
   //fNQ2Bins     = 0;

   // Binning is hard coded here to have Ebeam = 5.9
   if(fBinningBeamEnergy > 6.0) {
      Error("Initialize","Beam energy, %f, too big.",fBinningBeamEnergy);
      fBinningBeamEnergy = 5.9;
   } else {
      fBinningBeamEnergy = 5.9;
   } 

   // --------------------------------------------------------------
   // Calculate bins for a fixed Q2 using the energy resolution 
   // to set the minimum bin width in energy.
   // We start at x=1
   x         = 0.95;//0.93;
   Q2        = fQ2Min;//+(fQ2Max+fQ2Min)/2.0;
   nu        = Q2/(2.0*M*x);
   E         = fBinningBeamEnergy - nu;
   E_min     = E*( 1.0 - EnergyResolution(E)/2.0 );
   E_max     = E*( 1.0 + EnergyResolution(E)/2.0 );
   nu_min    = fBinningBeamEnergy - E_max;
   nu_max    = fBinningBeamEnergy - E_min;
   x_min     = Q2/(2.0*M*nu_max);
   x_max     = Q2/(2.0*M*nu_min);
   theta_min = TMath::ACos(1.0-Q2/(2.0*fBinningBeamEnergy*E_max));
   theta_max = TMath::ACos(1.0-Q2/(2.0*fBinningBeamEnergy*E_min));
   W_min     = TMath::Sqrt(M*M + 2.0*M*nu_min - Q2);
   W_max     = TMath::Sqrt(M*M + 2.0*M*nu_max - Q2);

   // Begin by setting the first upper limit
   fEnergyBins.insert(fEnergyBins.begin(),E_max );   
   fNuBins.push_back(nu_min); // push_back since nu increases 
   fxBins.insert(fxBins.begin(),x_max );  
   fThetaBins.push_back(theta_min); // push_back since theta increases 
   fWBins.push_back(W_min); // push_back since theta increases 

   if(E_min > E_max ) {
      Error("Initialize"," Problem in energy limit calculation!");
   }
   
   //std::cout << "Calculating bins for asymmetry " << n << std::endl; 
   Int_t N = 0;
   do {
      E_min     = E*( 1.0 - EnergyResolution(E)/2.0 );
      E_max     = E*( 1.0 + EnergyResolution(E)/2.0 );
      nu_min    = fBinningBeamEnergy - E_max;
      nu_max    = fBinningBeamEnergy - E_min;
      x_min     = Q2/(2.0*M*nu_max);
      x_max     = Q2/(2.0*M*nu_min);
      theta_min = TMath::ACos(1.0-Q2/(2.0*fBinningBeamEnergy*E_max));
      theta_max = TMath::ACos(1.0-Q2/(2.0*fBinningBeamEnergy*E_min));
      W_min     = TMath::Sqrt(M*M + 2.0*M*nu_min - Q2);
      W_max     = TMath::Sqrt(M*M + 2.0*M*nu_max - Q2);

      // to avoid theta=pi with bins that are the same.
      if(theta_min == theta_max) theta_max += float(N)*0.000001;
      if(theta_min > theta_max) theta_max = theta_min + float(N)*0.000001;

      fEnergyBins.insert(fEnergyBins.begin(),E_min );   
      fNuBins.push_back(nu_max); // push_back since nu increases 
      fxBins.insert(fxBins.begin(),x_min );  
      fThetaBins.push_back(theta_max); // push_back since theta increases 
      fWBins.push_back(W_max); // push_back since theta increases 

      // Evaluate cental values for the next bin. 
      E   = TMath::Power( (-C1/2.0 + TMath::Sqrt((C1/2.0)*(C1/2.0) + 4.0*(1.0 + C2/2.0)*E_min))/(2.0+C2)  ,2.0);
      // Check that the next bin is lower ... if not make it lower (causes overlapping bins)
      if(E > E_min) {
         //std::cout << "Error overlapping bin\n";
         E = E_min;
         //std::cout << "N: " << N << "\n";
         //std::cout << " Eres = " << EnergyResolution(E) << "\n";  
         //std::cout << " x_max= " << x_max << ", x_min = " << x_min << "\n";
         //std::cout << " E= " << E << ", nu = " << nu << ", theta_max/degree = " <<  theta_max/degree << "\n";
         //std::cout << " Emax= " << E_max << ", E_min = " << E_min << ", nu_min = " <<  nu_min << "\n";
      }
      nu  = fBinningBeamEnergy - E;
      x   = Q2/(2.0*M*nu);
      N++;
   } while ( (E > 0.4) /*&& (theta_max < TMath::Pi() )*/ && ( N < 400 ) );  

   // -----------------------
   // Fill x bins until x=0 
   Double_t x_step = 2.0*(x_max-x_min);
   x_min = x_min - x_step;
   while( x_min>0.2 ){ 
      fxBins.insert(fxBins.begin(),x_min );  
      x_min = x_min - x_step;
   }

   // -----------------------
   // Fill W bins until W=4.0 
   Double_t W_step = (W_max-W_min);
   while( W_max < 4.0 ){ 
      W_min = W_max;
      W_max = W_max + W_step;
      fWBins.push_back(W_max );  
   }

   // -----------------------
   // Redo the Energy binning
   fEnergyBins.clear();
   x         = 0.93;
   Q2        = fQ2Min;//+(fQ2Max+fQ2Min)/2.0;
   nu        = Q2/(2.0*M*x);
   E         = fBinningBeamEnergy - nu;
   E_min     = E*( 1.0 - EnergyResolution(E)/2.0 );
   E_max     = E*( 1.0 + EnergyResolution(E)/2.0 );
   N         = 0;
   fEnergyBins.insert(fEnergyBins.begin(),E_max );   
   do {
      // Here we split the bin in 2 
      E_min     = E*( 1.0 - EnergyResolution(E)/2.0 );
      E_max     = E*( 1.0 + EnergyResolution(E)/2.0 );
      fEnergyBins.insert(fEnergyBins.begin(),E );   
      fEnergyBins.insert(fEnergyBins.begin(),E_min );   
      // Evaluate cental values for the next bin. 
      E   = TMath::Power( (-C1/2.0 + TMath::Sqrt((C1/2.0)*(C1/2.0) + 4.0*(1.0 + C2/2.0)*E_min))/(2.0+C2)  ,2.0);
      // Check that the next bin is lower ... if not make it lower (causes overlapping bins)
      if(E > E_min) {
         E = E_min;
      }
      nu  = fBinningBeamEnergy - E;
      x   = Q2/(2.0*M*nu);
      N++;
   } while ( (E > 0.1) /*&& (theta_max < TMath::Pi() )*/ && ( N < 400 ) );  


   // number of bins is one less than then number of bin edges
   fNxBins      = fxBins.size() - 1;
   fNNuBins     = fNuBins.size() - 1;
   fNThetaBins  = fThetaBins.size() - 1;
   fNWBins      = fWBins.size() - 1;
   fNEnergyBins = fEnergyBins.size() - 1;
   //fNPhiBins    = 2;

   // Calculate the phi bins
   double deltaphi = (fPhiMax-fPhiMin)/(double(fNPhiBins));
   for(int i = 0; i<fNPhiBins+1; i++){
      fPhiBins.push_back(fPhiMin + deltaphi*double(i) );
   } 

   // Calculate the Q2 bins
   double deltaQ2 = (fQ2Max-fQ2Min)/(double(fNQ2Bins));
   for(int i = 0; i<fNQ2Bins+1; i++){
      fQ2Bins.push_back(fQ2Min + deltaQ2*double(i) );
   } 

   // Print the bins as a check.
   //std::cout << "x bins: ";
   //for(std::vector<float>::iterator it = fxBins.begin() ; it != fxBins.end(); ++it)
   //   std::cout << ' ' << *it;
   //std::cout << ' ' << std::endl;;
   //std::cout << "nu bins: ";
   //for(std::vector<float>::iterator it = fNuBins.begin() ; it != fNuBins.end(); ++it)
   //   std::cout << ' ' << *it;
   //std::cout << ' ' << std::endl;;
   //std::cout << "E bins: ";
   //for(std::vector<float>::iterator it = fEnergyBins.begin() ; it != fEnergyBins.end(); ++it)
   //   std::cout << ' ' << *it;
   //std::cout << ' ' << std::endl;;
   //std::cout << "W bins: ";
   //for(std::vector<float>::iterator it = fWBins.begin() ; it != fWBins.end(); ++it)
   //   std::cout << ' ' << *it;
   //std::cout << ' ' << std::endl;;
   //std::cout << "theta bins: ";
   //for(std::vector<float>::iterator it = fThetaBins.begin() ; it != fThetaBins.end(); ++it)
   //   std::cout << ' ' << *it;
   //std::cout << ' ' << std::endl;;

   // -----------------------------------------
   bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   //fHistograms.Clear();
   // -----------------------------------------
   fNPlusVsx         =  TH1F(Form("fNPlusVsx-%d", n),         Form("N_plus Vs x %d;x", n),  
                       fNxBins, &fxBins[0]);
   fNPlusVsNu        =  TH1F(Form("fNPlusVsNu-%d", n),        Form("N_plus Vs #nu %d;#nu", n),  
                       fNNuBins, &fNuBins[0]);
   fNPlusVsW         =  TH1F(Form("fNPlusVsW-%d", n),         Form("N_plus Vs W %d;W", n),  
                       fNWBins, &fWBins[0]);
   fNPlusVsPhi       =  TH1F(Form("fNPlusVsPhi-%d", n),       Form("N_plus Vs #phi %d;#phi", n), 
                       fNPhiBins,  &fPhiBins[0]);
   fNPlusVsTheta     =  TH1F(Form("fNPlusVsTheta-%d", n),     Form("N_plus Vs #theta %d;#theta", n), 
                       fNThetaBins, &fThetaBins[0]);
   fNPlusVsE         =  TH1F(Form("fNPlusVsE-%d", n),         Form("N_plus Vs E %d;E", n), 
                       fNEnergyBins ,&fEnergyBins[0]);
   fNPlusVsQ2         = TH1F(Form("fNPlusVsQ2-%d", n),         Form("N_plus Vs Q2 %d:Q2", n), 
                        50, fQ2Min ,fQ2Max);
   //---------------------
   fNMinusVsx         =  TH1F(Form("fNMinusVsx-%d", n),         Form("N_Minus Vs x %d;x", n),  
                       fNxBins, &fxBins[0]);
   fNMinusVsNu        =  TH1F(Form("fNMinusVsNu-%d", n),        Form("N_Minus Vs #nu %d;#nu", n),  
                       fNNuBins, &fNuBins[0]);
   fNMinusVsW         =  TH1F(Form("fNMinusVsW-%d", n),         Form("N_Minus Vs W %d;W", n),  
                       fNWBins, &fWBins[0]);
   fNMinusVsPhi       =  TH1F(Form("fNMinusVsPhi-%d", n),       Form("N_Minus Vs #phi %d;#phi", n), 
                       fNPhiBins,  &fPhiBins[0]);
   fNMinusVsTheta     =  TH1F(Form("fNMinusVsTheta-%d", n),     Form("N_Minus Vs #theta %d;#theta", n), 
                       fNThetaBins, &fThetaBins[0]);
   fNMinusVsE         =  TH1F(Form("fNMinusVsE-%d", n),         Form("N_Minus Vs E %d;E", n), 
                       fNEnergyBins ,&fEnergyBins[0]);
   fNMinusVsQ2         = TH1F(Form("fNMinusVsQ2-%d", n),         Form("N_Minus Vs Q2 %d:Q2", n), 
                        50, fQ2Min ,fQ2Max);
   //---------------------
   fAsymmetryVsx         =  TH1F(Form("fAsymmetryVsx-%d", n),         Form("Asymmetry Vs x %d;x", n),  
                       fNxBins, &fxBins[0]);
   fAsymmetryVsNu        =  TH1F(Form("fAsymmetryVsNu-%d", n),        Form("Asymmetry Vs #nu %d;#nu", n),  
                       fNNuBins, &fNuBins[0]);
   fAsymmetryVsW         =  TH1F(Form("fAsymmetryVsW-%d", n),         Form("Asymmetry Vs W %d;W", n),  
                       fNWBins, &fWBins[0]);
   fAsymmetryVsPhi       =  TH1F(Form("fAsymmetryVsPhi-%d", n),       Form("Asymmetry Vs #phi %d;#phi", n), 
                       fNPhiBins,  &fPhiBins[0]);
   fAsymmetryVsTheta     =  TH1F(Form("fAsymmetryVsTheta-%d", n),     Form("Asymmetry Vs #theta %d;#theta", n), 
                       fNThetaBins, &fThetaBins[0]);
   fAsymmetryVsE         =  TH1F(Form("fAsymmetryVsE-%d", n),         Form("Asymmetry Vs E %d;E", n), 
                       fNEnergyBins ,&fEnergyBins[0]);
   //---------------------
   fDilutionVsx         =  TH1F(Form("fDilutionVsx-%d", n),         Form("Dilution Vs x %d;x", n),  
                       fNxBins, &fxBins[0]);
   fDilutionVsNu        =  TH1F(Form("fDilutionVsNu-%d", n),        Form("Dilution Vs #nu %d;#nu", n),  
                       fNNuBins, &fNuBins[0]);
   fDilutionVsW         =  TH1F(Form("fDilutionVsW-%d", n),         Form("Dilution Vs W %d;W", n),  
                       fNWBins, &fWBins[0]);
   fDilutionVsPhi       =  TH1F(Form("fDilutionVsPhi-%d", n),       Form("Dilution Vs #phi %d;#phi", n), 
                       fNPhiBins,  &fPhiBins[0]);
   fDilutionVsTheta     =  TH1F(Form("fDilutionVsTheta-%d", n),     Form("Dilution Vs #theta %d;#theta", n), 
                       fNThetaBins, &fThetaBins[0]);
   fDilutionVsE         =  TH1F(Form("fDilutionVsE-%d", n),         Form("Dilution Vs E %d;E", n), 
                       fNEnergyBins ,&fEnergyBins[0]);
   //---------------------
   fSystErrVsx         =  TH1F(Form("fSystErrVsx-%d", n),         Form("SystErr Vs x %d;x", n),  
                       fNxBins, &fxBins[0]);
   fSystErrVsNu        =  TH1F(Form("fSystErrVsNu-%d", n),        Form("SystErr Vs #nu %d;#nu", n),  
                       fNNuBins, &fNuBins[0]);
   fSystErrVsW         =  TH1F(Form("fSystErrVsW-%d", n),         Form("SystErr Vs W %d;W", n),  
                       fNWBins, &fWBins[0]);
   fSystErrVsPhi       =  TH1F(Form("fSystErrVsPhi-%d", n),       Form("SystErr Vs #phi %d;#phi", n), 
                       fNPhiBins,  &fPhiBins[0]);
   fSystErrVsTheta     =  TH1F(Form("fSystErrVsTheta-%d", n),     Form("SystErr Vs #theta %d;#theta", n), 
                       fNThetaBins, &fThetaBins[0]);
   fSystErrVsE         =  TH1F(Form("fSystErrVsE-%d", n),         Form("SystErr Vs E %d;E", n), 
                       fNEnergyBins ,&fEnergyBins[0]);
   //-------------------------------
   fAvgKineVsx =  InSANEAveragedKinematics1D(Form("avgkine-%d-Vsx",n),Form("avg-kine-%d Vs. x",n));
   fAvgKineVsx.CreateHistograms(fAsymmetryVsx); 

   fAvgKineVsW =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsW",n),Form("avg-kine-%d Vs. W",n));
   fAvgKineVsW.CreateHistograms(fAsymmetryVsW); 

   fAvgKineVsNu =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsNu",n),Form("avg-kine-%d Vs. Nu",n));
   fAvgKineVsNu.CreateHistograms(fAsymmetryVsNu); 

   //fAvgKineVsQ2 =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsQ2",n),Form("avg-kine-%d Vs. Q2",n));
   //fAvgKineVsQ2.CreateHistograms(fAsymmetryVsQ2); 

   fAvgKineVsE =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsE",n),Form("avg-kine-%d Vs. E",n));
   fAvgKineVsE.CreateHistograms(fAsymmetryVsE); 

   fAvgKineVsTheta =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsTheta",n),Form("avg-kine-%d Vs. Theta",n));
   fAvgKineVsTheta.CreateHistograms(fAsymmetryVsTheta); 

   fAvgKineVsPhi =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsPhi",n),Form("avg-kine-%d Vs. phi",n));
   fAvgKineVsPhi.CreateHistograms(fAsymmetryVsPhi); 

   fMask_x     = fNPlusVsx;
   fMask_W     = fNPlusVsW;
   fMask_E     = fNPlusVsE;
   fMask_Nu    = fNPlusVsNu;
   fMask_Theta = fNPlusVsTheta;
   fMask_Phi   = fNPlusVsPhi;

   SetColor( 2000 + fNumber%4 );
   
   TH1::AddDirectory(add_status);
}
//_____________________________________________________________________________



