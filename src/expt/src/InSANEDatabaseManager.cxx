#include "InSANEDatabaseManager.h"
//#include "TString.h"
//#include "TMySQLServer.h"
#include <cstdlib>

ClassImp(InSANEDatabaseManager)

//_____________________________________________________________________________
InSANEDatabaseManager* InSANEDatabaseManager::fgInSANEDatabaseManager = nullptr;

//_____________________________________________________________________________
InSANEDatabaseManager *  InSANEDatabaseManager::GetManager()
{
   //std::cout << "Getting database manager" << std::endl;
   if (!fgInSANEDatabaseManager) {
      fgInSANEDatabaseManager = new InSANEDatabaseManager() ;
   }
   return(fgInSANEDatabaseManager);
}
//_____________________________________________________________________________
InSANEDatabaseManager::InSANEDatabaseManager() {
   //std::cout << "InSANEDatabaseManager ctor" << std::endl;

   fMyCnfGroup       = "InSANE";//"client"; // .my.cnf group name
   fMySQLServer      = nullptr;
   fSQLite3Server    = nullptr;

   fDBType           = InSANEDatabaseManager::kMySQL;

   fTable            = "run_info";
   fWhereClause      = " ";

   fUserName         = "";
   fPassword         = "";
   fDatabaseName     = "";
   fDatabaseFile     = "SANE.db"; // sqlite3 database file
   fDatabasePath     = "";
   fDBServer         = "";
   fConnectionString = "";
}
//_____________________________________________________________________________
void InSANEDatabaseManager::ConnectSQLite3() {
   Error("ExecuteInsert","SQLite3 no longer supported");
   //char * pPath;
   //sqlite3 * fSQLiteDB;
   //pPath = getenv("InSANE_DB_PATH");
   //if (pPath == NULL) pPath = "sqlite";
   //fDatabasePath = pPath;
   //fConnectionString = Form("%s/%s", fDatabasePath.Data(), fDatabaseFile.Data());
   ///*      printf ("The current path is: %s\n",pPath);*/
   //sqlite3_enable_shared_cache(true);
   ////sqlite3_open(fConnectionString.Data(), &fSQLiteDB);
   //sqlite3_open_v2(fConnectionString.Data(), &fSQLiteDB, SQLITE_OPEN_READWRITE,"unix-dotfile");
   //if( !fSQLiteDB ) { 
   //   sqlite3_open(fConnectionString.Data(), &fSQLiteDB);
   //   std::cout << "Opening non unix-dotfile SQLite3 connection\n";
   //}
   //sqlite3_busy_timeout(fSQLiteDB, 3000);
   //fSQLite3Server = fSQLiteDB;
}
//_____________________________________________________________________________
//sqlite3 * InSANEDatabaseManager::GetSQLite3Connection(){ 
void * InSANEDatabaseManager::GetSQLite3Connection(){ 
   if(!fSQLite3Server) ConnectSQLite3();
   return(fSQLite3Server);
}
//_____________________________________________________________________________
void InSANEDatabaseManager::ConnectMySQL() {
   if(!fMySQLServer) {
      fDBServer = std::getenv ("InSANE_DB_SERVER");
      //std::cout << "InSANE_DB_SERVER IS " << fDBServer << "\n";
      //if( !strcmp(fDBServer.Data(),"cccldb-1.jlab.org") ) {
      //   fDatabaseName = "sane08";
      //   std::cout << " o User: " << fUserName << " @ " << fDatabaseName << "\n";
      //} else {
      //   fDatabaseName = "SANE";
      //   std::cout << " o User: " << fUserName << " @ " << fDatabaseName << "\n";
      //}

      //TString serv = Form("mysql:///?cnf_group=%s&reconnect=1",fMyCnfGroup.Data());
      TString serv = Form("mysql:///?cnf_group=%s&timeout=120",fMyCnfGroup.Data());
      //std::cout << serv.Data() << std::endl;

      fMySQLServer = TSQLServer::Connect(serv.Data(),nullptr,nullptr);
      if(!fMySQLServer) 
         Error("ConnectMySQL","Failed to connect to MySQL server!\n Check my.cnf file!");
      //else 
      //   std::cout << " connected " << std::endl;
   }
}
//_____________________________________________________________________________

TSQLServer * InSANEDatabaseManager::GetMySQLConnection(){ 
   if(!fMySQLServer) ConnectMySQL();
   return(fMySQLServer);
}
//_____________________________________________________________________________

Int_t InSANEDatabaseManager::ConnectDB() {
   if( fDBType == InSANEDatabaseManager::kSQLite3 || fDBType == InSANEDatabaseManager::kBoth  ) {
      ConnectSQLite3();
   }
   if ( fDBType == InSANEDatabaseManager::kMySQL || fDBType == InSANEDatabaseManager::kBoth  ) {
      ConnectMySQL();
   }
   return(0);
}
//_____________________________________________________________________________
InSANEDatabaseManager::~InSANEDatabaseManager()
{
   /*   if (fDBConnection) sqlite3_close(fDBConnection);
        fDBConnection = 0;*/
   if ( fMySQLServer ) delete fMySQLServer;
   fMySQLServer = nullptr;
   //if ( fSQLite3Server ) sqlite3_close( fSQLite3Server);
   fSQLite3Server = nullptr;

}
//_____________________________________________________________________________

Int_t InSANEDatabaseManager::ExecuteInsert(TString *sql)
{
   if(fDBType == InSANEDatabaseManager::kSQLite3 || fDBType == InSANEDatabaseManager::kBoth ) {
      Warning("ExecuteInsert","SQLite3 no longer supported");
      //char *zErrMsg = 0;
      //int rc = sqlite3_exec(GetSQLite3Connection(),
      //      sql->Data(),
      //      InSANEDatabaseManager::callback,
      //      this,
      //      &zErrMsg);
      //if (rc != SQLITE_OK) {
      //   std::cerr << "SQLite error: " << zErrMsg << std::endl;
      //   sqlite3_free(zErrMsg);
      //}
   } 
   if ( fDBType == InSANEDatabaseManager::kMySQL || fDBType == InSANEDatabaseManager::kBoth  ) {
      GetMySQLConnection()->Query( sql->Data() ) ;
   }
   return(0);
}
//_____________________________________________________________________________

