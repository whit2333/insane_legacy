#define BIGCALClusterProcessor_cxx
#include "BIGCALClusterProcessor.h"
#include "SANERunManager.h"
#include "TCanvas.h"

ClassImp(BIGCALClusterProcessor)
//___________________________________________________________________

BIGCALClusterProcessor::BIGCALClusterProcessor(TH2F * hist,const char * opt)
{

   fSourceHistogram = hist;
   fOptions         = opt;

   fBigcalEvent = nullptr;
   bcgeo = BIGCALGeometryCalculator::GetCalculator();
   //s = new TSpectrum2(fMaxNClusters,10);
}
//___________________________________________________________________
BIGCALClusterProcessor::~BIGCALClusterProcessor() {
   //delete s;
}
//___________________________________________________________________

Int_t BIGCALClusterProcessor::FindPeaks()
{
// // Peak finder
// int i;
// // Double_t xmin  = fSourceHistogram->GetXaxis()->GetXmin();
// // Double_t xmax  = fSourceHistogram->GetXaxis()->GetXmax();
// // Double_t ymin  = fSourceHistogram->GetYaxis()->GetXmin();
// // Double_t ymax  = fSourceHistogram->GetYaxis()->GetXmax();
// if(!s) s = new TSpectrum2(fMaxNClusters,10);
// Double_t thresh = 0.02;
// fNFound = s->Search(fSourceHistogram,2,fOptions.Data(),thresh);
// fXPeaks = s->GetPositionX();
// fYPeaks = s->GetPositionY();
// return(fNFound);
   Int_t x1 = 0;
   Int_t y1 = 0;
   //Int_t z1;
   //Int_t maxbin = fSourceHistogram->GetMaximumBin(x1,y1,z1);
   Float_t binXCenter = fSourceHistogram->GetXaxis()->GetBinCenter(x1);
   Float_t binYCenter = fSourceHistogram->GetYaxis()->GetBinCenter(y1);
   fiPeaks->push_back(binXCenter);
   fiPeaks->push_back(binYCenter);
   std::cout << "(x,y) cluster bin center = ( " << binXCenter << " , " << binYCenter << " )\n";
   std::cout << "(x,y) cluster binxy= ( " << x1 << " , " << y1 << " )\n";
   std::cout << "(x,y) cluster peaks= ( " << (*fiPeaks)[0] << " , " << (*fiPeaks)[0] << " )\n";
   return(1);
}
//_______________________________________________________//

TH2F * BIGCALClusterProcessor::GetSourceHistogram()
{
   /* std::cout << "BIGCALClusterProcessor::GetSourceHistogram\n";*/
   if (!fSourceHistogram)
      fSourceHistogram = new TH2F("bigcalClusterSource", "Bigcal Cluster Sources", 32, 1, 33, 56, 1, 57);
   if (!fSourceCopy)
      fSourceCopy = new TH2F("bigcalClusterSourceCopy", "Bigcal Cluster Sources Copy", 32, 1, 33, 56, 1, 57);

   if ((!fBigcalEvent)) {
      std::cout << " Warning BIGCALClusterProcessor Ssource Histogram AND BigcalEvent are NULL!c\n ";
      std::cout << " returning null pointer \n";
      return(fSourceHistogram);
   }

   fSourceHistogram->Reset();
   fSourceCopy->Reset();

   BigcalHit * ahit = nullptr;
   TClonesArray * bchits = fBigcalEvent->fBigcalHits;
//cout << " hits " << fBigcalEvent->fNumberOfHits;

   for (Int_t i = 0; i < bchits->GetEntries(); i++) {
      ahit = (BigcalHit*)(*bchits)[i];
      if (ahit->fTDCLevel == -1) {

         fSourceHistogram->Fill(ahit->fiCell,
                                ahit->fjCell,
                                ahit->fEnergy);
         fSourceCopy->Fill(ahit->fiCell,
                           ahit->fjCell,
                           ahit->fEnergy);
      }
   }
   return(fSourceHistogram);
}
//______________________________________________________________________________
Int_t  BIGCALClusterProcessor::SetEventAddress(BigcalEvent * ev) {
   if (ev) {
      fBigcalEvent = ev;
      fEvent = ev;
      if (SANERunManager::GetRunManager()->fVerbosity > 2) std::cout << " 0 BIGCALClusterProcessor::SetEventAddress(BigcalEvent*)\n";
      return(0);
   } else {
      std::cout << "x- Error Invalid Pointer supplied in \n   BIGCALClusterProcessor::SetEventAddress(BigcalEvent*)\n";
      return(1);
   }
}
//______________________________________________________________________________
Int_t  BIGCALClusterProcessor::CalculateQuantities(BIGCALCluster * acluster, Int_t k) {
   /*  cout << " BIGCALClusterProcessor::CalculateQuantities \n";*/
   BIGCALGeometryCalculator *bcgeo = BIGCALGeometryCalculator::GetCalculator();
   Int_t m, n, i, j;
   i = (Int_t)TMath::Floor((Double_t)(*fiPeaks)[k]);
   j = (Int_t)TMath::Floor((Double_t)(*fjPeaks)[k]);
   acluster->SetClusterPeak(i, j);
//cout << "cords : " << bcgeo->getBlockXij_BCCoords(i,j) << " , " << bcgeo->getBlockYij_BCCoords(i,j) << "\n" ;
//cout << "total energy " << totalE << "\n";
   /// The cluster is selected to be a 5x5 grid centered on the peaks
   /// The following loops over all the blocks and adds
   Int_t offset = -2;
   /// initialize the hightest energy to the central block (as it should be)
   acluster->fHighestEnergy = acluster->fBlockEnergies[2][2];
   for (n = offset ; n < 5 + offset ; n++) {
      for (m = offset ; m < 5 + offset ; m++) {
         if (((j + n <= 56 && j + n > 32) && (i + m <= 30 && i + m > 0))/*RCS*/   ||
             ((j + n <= 32 && j + n > 0) && (i + m <= 32 && i + m > 0))/*PROT*/) {
            /// set the block energy if it is not detached
            if ((n == -2 || n == 2 || m == -2 || m == 2)) {
               if (IsDetached(i + m, j + n, m, n))
                  acluster->fBlockEnergies[m-offset][n-offset] = 0.0;
            } else {
               acluster->fBlockEnergies[m-offset][n-offset] = fSourceHistogram->GetBinContent(i + m, j + n);
            }
            /// if block is new highest
            /// \todo forget the 5x5 matrix and implement a Sort on an array of "blocks" that have hits and implement is detached etc....
            if (acluster->fHighestEnergy < acluster->fBlockEnergies[m-offset][n-offset]) {
               acluster->fThirdHighestEnergy  = acluster->fSecondHighestEnergy;
               acluster->fSecondHighestEnergy = acluster->fHighestEnergy;
               acluster->fHighestEnergy       = acluster->fBlockEnergies[m-offset][n-offset];
            } else if (acluster->fSecondHighestEnergy < acluster->fBlockEnergies[m-offset][n-offset]) {
               acluster->fThirdHighestEnergy  = acluster->fSecondHighestEnergy;
               acluster->fSecondHighestEnergy = acluster->fBlockEnergies[m-offset][n-offset];
            } else if (acluster->fThirdHighestEnergy < acluster->fBlockEnergies[m-offset][n-offset]) {
               acluster->fThirdHighestEnergy  = acluster->fBlockEnergies[m-offset][n-offset];
            }
            if (acluster->fBlockEnergies[m-offset][n-offset] > 0.0) acluster->fNNonZeroBlocks++;

            // cluster->SetBinContent(i+m,j+n,block_energy[m+2][n+2] );
            // Calculate the mean positions (first moments)
            acluster->fXMoment += acluster->fBlockEnergies[m-offset][n-offset] * bcgeo->GetBlockXij_BCCoords(i + m, j + n);
            acluster->fYMoment += acluster->fBlockEnergies[m-offset][n-offset] * bcgeo->GetBlockYij_BCCoords(i + m, j + n);
            acluster->fTotalE += acluster->fBlockEnergies[m-offset][n-offset];
         }
      }
   }
   acluster->fXMoment = acluster->fXMoment / (acluster->fTotalE);
   acluster->fYMoment = acluster->fYMoment / (acluster->fTotalE);
// Now we can calculate the central moments of interest (variance, skewness, and higher...)
   for (n = offset ; n < 5 + offset ; n++) {
      for (m = offset ; m < 5 + offset ; m++) {
         if (((j + n <= 56 && j + n > 32) && (i + m <= 30 && i + m > 0))/*RCS*/ || ((j + n <= 32 && j + n > 0) && (i + m <= 32 && i + m > 0))/*PROT*/)   {
            acluster->fX2Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockXij_BCCoords(i + m, j + n) - acluster->fXMoment , 2.0);
            acluster->fY2Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockYij_BCCoords(i + m, j + n) - acluster->fYMoment , 2.0);
            acluster->fX3Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockXij_BCCoords(i + m, j + n) - acluster->fXMoment , 3.0);
            acluster->fY3Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockYij_BCCoords(i + m, j + n) - acluster->fYMoment , 3.0);
            acluster->fX4Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockXij_BCCoords(i + m, j + n) - acluster->fXMoment , 4.0);
            acluster->fY4Moment += acluster->fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockYij_BCCoords(i + m, j + n) - acluster->fYMoment , 4.0);
         }
      }
   }
   acluster->fX2Moment = acluster->fX2Moment / (acluster->fTotalE); // second moment is the variance (sigma^2)
   acluster->fY2Moment = acluster->fY2Moment / (acluster->fTotalE);
   acluster->fX3Moment = acluster->fX3Moment / (acluster->fTotalE);
   acluster->fY3Moment = acluster->fY3Moment / (acluster->fTotalE);
   acluster->fX4Moment = acluster->fX4Moment / (acluster->fTotalE);
   acluster->fY4Moment = acluster->fY4Moment / (acluster->fTotalE);

   acluster->fXStdDeviation = TMath::Power(acluster->fX2Moment , 0.5);
   acluster->fYStdDeviation = TMath::Power(acluster->fY2Moment , 0.5);

   acluster->fXSkewness = acluster->fX3Moment * TMath::Power(1.0 / (acluster->fXStdDeviation) , 3.0);
   acluster->fYSkewness = acluster->fY3Moment * TMath::Power(1.0 / (acluster->fYStdDeviation) , 3.0);

   // Here we are calculating the "excess kurtosis" which is mu_4/sigma^4 - 3.0
   // where the minus 3 makes the kurtosis of a normal distribution 0. 
   acluster->fXKurtosis = acluster->fX4Moment * TMath::Power(1.0 / (acluster->fXStdDeviation) , 4.0) - 3.0;
   acluster->fYKurtosis = acluster->fY4Moment * TMath::Power(1.0 / (acluster->fYStdDeviation) , 4.0) - 3.0;

   //cout << "cords : " << bcgeo->getBlockXij_BCCoords(i,j) << " , " << bcgeo->getBlockYij_BCCoords(i,j) << "\n" ;
   //cout << "total energy " << totalE << "\n";

   return(0);

}
//_______________________________________________________//

Int_t BIGCALClusterProcessor::Review()
{

   if (!c) c = new TCanvas();
   c->cd(1);
   fSourceHistogram->Draw("colz");
   gPad->WaitPrimitive();
   return(0);
}
//_______________________________________________________//

