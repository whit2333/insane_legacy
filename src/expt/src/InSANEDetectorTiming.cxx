#include  "InSANEDetectorTiming.h"

//______________________________________________________________________________
ClassImp(InSANETDCSpectrum)

//______________________________________________________________________________
InSANETDCSpectrum::InSANETDCSpectrum() {
   fNPars = 6;
}
//______________________________________________________________________________
InSANETDCSpectrum::~InSANETDCSpectrum() {
}
//______________________________________________________________________________
double InSANETDCSpectrum::DoEvalPar(double x, const double* p) const {
   double lorentzian = (0.5 * p[0] * p[1] / TMath::Pi()) / TMath::Max(1.e-15, (x - p[2]) * (x - p[2]) + .25 * p[1] * p[1]) ;
   double piecewise = 0.0;
   if (TMath::Abs(x - p[4]) < p[5]) piecewise = p[3];
   else piecewise = 0.0;
   return(lorentzian + piecewise);
}
//______________________________________________________________________________

//______________________________________________________________________________
ClassImp(InSANEDetectorTiming)

//______________________________________________________________________________
InSANEDetectorTiming::InSANEDetectorTiming() {
   fTDCPeak           = 0.0;
   fTDCPeakWidth      = 0.0;
   fId                = -1;
   fPeakFitChiSquared = 0.0;
   fTriggerType       = -1;
}
//______________________________________________________________________________
InSANEDetectorTiming::InSANEDetectorTiming(Int_t id, Double_t val, Double_t width) {
   fTDCPeak = val;
   fTDCPeakWidth = width;
   fId = id;
}
//______________________________________________________________________________
InSANEDetectorTiming::~InSANEDetectorTiming() {
}
//______________________________________________________________________________
TString * InSANEDetectorTiming::GetSQLInsertString(const char * det, Int_t run_number) {
   TString SQLCOMMAND(" ");
   SQLCOMMAND += "REPLACE INTO detector_timing ( ";
   SQLCOMMAND += " `run_number`";
   SQLCOMMAND += ",`detector`";
   SQLCOMMAND += ",`tdc_number`";
   SQLCOMMAND += ",`tdc_mean`";
   SQLCOMMAND += ",`tdc_width` ";
   SQLCOMMAND += " ) VALUES ( ";
   SQLCOMMAND += run_number;
   SQLCOMMAND += ", '";
   SQLCOMMAND += det;
   SQLCOMMAND += "',";
   SQLCOMMAND += fId;
   SQLCOMMAND += ",";
   SQLCOMMAND += fTDCPeak;
   SQLCOMMAND += ",";
   SQLCOMMAND += fTDCPeakWidth;
   SQLCOMMAND += " )  ;";


   return(new TString(SQLCOMMAND.Data()));
}
//______________________________________________________________________________
int InSANEDetectorTiming::SetMeanWidth_callback(void * theTdcpeak, int argc, char **argv, char **azColName) {
   auto * peak = (InSANEDetectorTiming*) theTdcpeak;
   if (argc == 2) { // should equal 2!
      peak->fTDCPeak = atof(argv[0]);
      peak->fTDCPeakWidth = atof(argv[1]);
   } else {
      peak->Error("SetMeanWidth_callback", "argc does not equal 2! it is %d", argc);
   }
   return 0;
}
//______________________________________________________________________________
Int_t InSANEDetectorTiming::GetTiming(const char * det, Int_t run_number, Int_t id) {
   
   // Returns 0 on succes and -1 if no data was found.

   fId = id;
   InSANEDatabaseManager * dbManager = InSANEDatabaseManager::GetManager();

   TString SQLCOMMAND = "SELECT tdc_mean,tdc_width FROM detector_timing where run_number=";
   SQLCOMMAND += run_number;
   SQLCOMMAND += " AND `detector`='";
   SQLCOMMAND += det;
   SQLCOMMAND += "' AND `tdc_number`=";
   SQLCOMMAND += id;
   SQLCOMMAND += " LIMIT 1;";

   // Old : don't use SQLite3
   if( dbManager->GetDBType() == InSANEDatabaseManager::kSQLite3 ) {
      Error("GetTiming","SQLite3 no longer supported");
      //char *zErrMsg = 0;
      //int rc = sqlite3_exec(dbManager->GetSQLite3Connection(), SQLCOMMAND.Data(), InSANEDetectorTiming::SetMeanWidth_callback, this, &zErrMsg);
      //if (rc != SQLITE_OK) {
      //   std::cerr << "SQL error: " << zErrMsg << std::endl;
      //   std::cerr << "fullcommand: " << SQLCOMMAND.Data()  << "\n";
      //   sqlite3_free(zErrMsg);
      //}
   }

   if( dbManager->GetDBType() == InSANEDatabaseManager::kMySQL ||
         dbManager->GetDBType() == InSANEDatabaseManager::kBoth ) {
      TSQLServer * db = dbManager->GetMySQLConnection();
      if(db) {
         TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
         TSQLRow * row = nullptr;
         if(res) row = res->Next();
         if(row) {

            fTDCPeak      = atof(row->GetField(0));
            fTDCPeakWidth = atof(row->GetField(1));
            fId           = id;
            delete res;
            delete row;
            return(0);

         } else {
            return(-1);
         }
      } else  { 
         std::cout << " NO DB \n";
         return(-1);
      }
   }
   return(-1);
}
//______________________________________________________________________________
void InSANEDetectorTiming::PrintTimingHead() const {
   printf("%5s %7s %7s \n", "Id", "Mean", "Width");
}
//______________________________________________________________________________
void InSANEDetectorTiming::PrintTiming() const {
   printf("%5d %5.2f %5.2f\n", fId, fTDCPeak, fTDCPeakWidth);
}
//______________________________________________________________________________
void InSANEDetectorTiming::Print(Option_t * ) const {
   PrintTimingHead();
   PrintTiming();
}
//______________________________________________________________________________

