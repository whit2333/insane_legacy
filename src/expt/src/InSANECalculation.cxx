#define InSANECalculation_cxx
#include "InSANECalculation.h"

ClassImp(InSANECalculation)

//______________________________________________________________________________
InSANECalculation::InSANECalculation(InSANEAnalysis * analysis) {
   //if (SANERunManager::GetRunManager()->fVerbosity > 2) printf(" o InSANECalculation::InSANECalculation()\n");
   fDescription    = "";
   fIsInitialized  = false;
   fFillEvent      = true;
   fTransientDatum = nullptr;
   fOutTree        = nullptr;
}
//______________________________________________________________________________
InSANECalculation::~InSANECalculation() {
}
//______________________________________________________________________________
void InSANECalculation::SetEvents(InSANEAnalysisEvents * e) {
   if(e) fTransientDatum = e;
   else Error("SetEvents","Null event pointer provided.");
}
//______________________________________________________________________________
InSANEAnalysisEvents * InSANECalculation::GetEvents() const { return(fTransientDatum); }
//______________________________________________________________________________
Bool_t InSANECalculation::IsInitialized() const { return(fIsInitialized); }
//______________________________________________________________________________
void   InSANECalculation::SetInitialized() { fIsInitialized = true; }
//______________________________________________________________________________
bool InSANECalculation::IsEventsSet() const {
   if (fTransientDatum != nullptr) return true;
   else return false;
}
//______________________________________________________________________________

