#include  "ForwardTrackerPositionHit.h"


ClassImp(ForwardTrackerPositionHit)

//______________________________________________________________________________
ForwardTrackerPositionHit::ForwardTrackerPositionHit() {
   fPositionVector.SetXYZ(0.0, 0.0, 0.0);
   fPositionVector2.SetXYZ(0.0, 0.0, 0.0);
   fTDCSum        = -9999.0;
   fTDCDifference = -9999.0;
   fNNeighborHits = 0;
   fHitNumber     = 0;
   fScintLayer    = 0;
   fYRow          = 0;
   fXRow          = 0;
   Clear() ;
}
//______________________________________________________________________________
ForwardTrackerPositionHit::~ForwardTrackerPositionHit() {
   ;
}
//______________________________________________________________________________
ForwardTrackerPositionHit::ForwardTrackerPositionHit(const ForwardTrackerPositionHit& rhs) : InSANEDetectorHit(rhs) {
   (*this) = rhs;
}
//______________________________________________________________________________
void ForwardTrackerPositionHit::Print(const Option_t * ) const {
   std::cout << "++ ForwardTrackerPositionHit " << this << std::endl;
   std::cout << "    - fTDCSum  = " << fTDCSum << std::endl;
   std::cout << "    - fTDCDifference  = " << fTDCDifference << std::endl;
   std::cout << "    - fScintLayer  = " << fScintLayer << std::endl;
   std::cout << "    - fNNeighborHits  = " << fNNeighborHits << std::endl;
}
//______________________________________________________________________________
void  ForwardTrackerPositionHit::Clear(Option_t *opt) {
   /*      if(InSANERunManager::GetRunManager()->fVerbosity > 2) Print();*/
   InSANEDetectorHit::Clear(opt);
   fTDCSum = -9999.0;
   fTDCDifference = -9999.0;
   fNNeighborHits = 0;
   fPositionVector.SetXYZ(0.0, 0.0, 0.0);
   fPositionVector2.SetXYZ(0.0, 0.0, 0.0);
   fHitNumber = 0;
   fScintLayer = 0;
   fYRow = 0;
   fXRow = 0;
}
//______________________________________________________________________________
ForwardTrackerPositionHit& ForwardTrackerPositionHit::operator=(const ForwardTrackerPositionHit &rhs) {
   // Check for self-assignment!
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   InSANEDetectorHit::operator=(rhs);
   // Deallocate, allocate new space, copy values...
   fTDCSum = rhs.fTDCSum;
   fTDCDifference = rhs.fTDCDifference;
   fNNeighborHits = rhs.fNNeighborHits;
   fPositionVector = rhs.fPositionVector;
   fPositionVector2 = rhs.fPositionVector2;
   fHitNumber = rhs.fHitNumber;
   fScintLayer = rhs.fScintLayer;
   fYRow = rhs.fYRow;
   fXRow = rhs.fXRow;
   return *this;
}
//______________________________________________________________________________

