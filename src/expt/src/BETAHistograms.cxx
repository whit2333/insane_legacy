#include "BETAHistograms.h"

ClassImp(BETAHistogramCalc0)

Int_t    BETAHistogramCalc0::Calculate()
{

   if (!(fEvents->TRIG->IsBETAEvent()))  return(0);

   return(0);
}



ClassImp(BETAHistogramCalc1)

Int_t    BETAHistogramCalc1::Calculate()
{

   if (!(fEvents->TRIG->IsBETAEvent()))  return(0);

   ForwardTrackerPositionHit * aTrackerPosHit = nullptr;
   TH2F * h2 = nullptr;
   BIGCALCluster * aCluster = nullptr;

   for (int kk = 0; kk < fClusters->GetEntries(); kk++) {
      aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
      if (aCluster->fTotalE > 1000.0) {

         /// Tracker Y1 Positions
         for (int i = 0; i < ftEvent->fTrackerY1PositionHits->GetEntries(); i++) {
            aTrackerPosHit = (ForwardTrackerPositionHit*)(*(ftEvent->fTrackerY1PositionHits))[i];

            h2 = (TH2F*)f2DHistograms.At(0);
            h2->Fill(aCluster->fiPeak, aTrackerPosHit->fXRow);

            h2 = (TH2F*)f2DHistograms.At(2);
            h2->Fill(aCluster->fjPeak, aTrackerPosHit->fYRow);

            h2 = (TH2F*)f2DHistograms.At(4);
            h2->Fill(aCluster->GetXmoment(), aTrackerPosHit->fPositionVector.X());

            h2 = (TH2F*)f2DHistograms.At(5);
            h2->Fill(aCluster->GetYmoment(), aTrackerPosHit->fPositionVector.Y());

         }

         /// Tracker Y2 Positions
         for (int i = 0; i < ftEvent->fTrackerY2PositionHits->GetEntries(); i++) {
            aTrackerPosHit = (ForwardTrackerPositionHit*)(*(ftEvent->fTrackerY2PositionHits))[i];

            h2 = (TH2F*)f2DHistograms.At(1);
            h2->Fill(aCluster->fiPeak, aTrackerPosHit->fXRow);

            h2 = (TH2F*)f2DHistograms.At(3);
            h2->Fill(aCluster->fjPeak, aTrackerPosHit->fYRow);

            h2 = (TH2F*)f2DHistograms.At(4);
            h2->Fill(aCluster->GetXmoment(), aTrackerPosHit->fPositionVector.X());

            h2 = (TH2F*)f2DHistograms.At(5);
            h2->Fill(aCluster->GetYmoment(), aTrackerPosHit->fPositionVector.Y());

         }
      }
   }

   return(0);
}


