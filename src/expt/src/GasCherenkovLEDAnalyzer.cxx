#include "GasCherenkovLEDAnalyzer.h"
#include "SANERunManager.h"
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "TFitResult.h"
#include "InSANEDatabaseManager.h"

ClassImp(GasCherenkovLEDAnalyzer)
//_______________________________________________________//

void GasCherenkovLEDAnalyzer::Initialize(TTree * inTree)
{
   fInputTree = inTree;

   InitCorrections();

   InitCalculations();

}
//_______________________________________________________//

void GasCherenkovLEDAnalyzer::MakePlots()
{
//  TFile*f = new TFile(Form("data/rootfiles/LED%d.root",fRunNumber),"UPDATE");
   //TCanvas * c = new TCanvas("Cherenkov","Gas Cherenkov",500,800);

   gROOT->ProcessLine(Form(".x scripts/cherenkovLEDRun.cxx(%d,%d) ", fRunNumber, 0));



   fAnalysisFile->cd();

}


