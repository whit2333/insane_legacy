#include "BigcalPi0Calculations.h"

ClassImp(BigcalPi0Calculations)

//______________________________________________________________________________
BigcalPi0Calculations::BigcalPi0Calculations(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis) {
   SetNameTitle("BigcalPi0Calculations", "BigcalPi0Calculations");

   fOutTree         = nullptr;
   bcCoords         = new BigcalCoordinateSystem();
   fRotFromBCcoords = bcCoords->GetRotationMatrixTo();

   fOutTree      = new TTree("pi0results", "pi0 Reconstruction");
   fPi0Event     = new Pi0Event();
   if (fOutTree) fOutTree->Branch("pi0reconstruction", "Pi0Event", &fPi0Event);
   //if(fOutTree) fOutTree->AddFriend("Clusters");
   //for(int i = 0; i<20; i++) fNNParams[i] = 0.0;

}
//______________________________________________________________________________
BigcalPi0Calculations::~BigcalPi0Calculations() {
   // should delete stuff
}
//______________________________________________________________________________
Int_t BigcalPi0Calculations::Initialize() {

   if (SANERunManager::GetRunManager()->GetVerbosity() > 2)
      std::cout << " o BigcalPi0Calculations::Initialize()\n";

   BETACalculationWithClusters::Initialize();

   if(SANERunManager::GetRunManager()->IsPerpendicularRun()) {
      fIsPerp = true;
      fANNClusterRecon.SetPerp(true);
   }

   return(0);
}
//______________________________________________________________________________
Int_t BigcalPi0Calculations::Finalize(){
   if(fOutTree)fOutTree->BuildIndex("pi0reconstruction.fRunNumber", "pi0reconstruction.fEventNumber");
   if(fOutTree)fOutTree->FlushBaskets();
   if(fOutTree)fOutTree->Write();
}
//________________________________________________________________________________
void BigcalPi0Calculations::Describe() {
   std::cout <<  "===============================================================================\n";
   std::cout <<  " BigcalPi0Calaculations - \n ";
   std::cout <<  "       Loops over clusters and determines whether it is a good candidate for\n";
   std::cout <<  "       a photon coming from a pi0 decay. \n";
   std::cout <<  "       Then it reconstructs a pi0 from the selected clusters.\n";
   std::cout <<  "       Calculates neutral pion mass from first two clusters \n";
   std::cout <<  "_______________________________________________________________________________\n";
}
//______________________________________________________________________________
Int_t BigcalPi0Calculations::Calculate() {

   fPi0Event->ClearEvent();

   fFillEvent = false;

   //// Pi0 events can be either pi0 trigger OR BETA trigger
   //if( !(fEvents->TRIG->IsNeutralPionEvent() || fEvents->TRIG->IsBETAEvent()) ) return(0);

   // Pi0 events can only be be a pi0 trigger
   if( !(fEvents->TRIG->IsNeutralPionEvent()) ) return(0);

   // requires at least 2 clusters
   if(fClusterEvent->fNClusters < 2) return 0; 

   // CALORIMETER CLUSTERING
   /*#ifdef INSANE_VERBOSE_EVENT*/
   /*  if(SANERunManager::GetRunManager()->GetVerbosity()>3)  printf("neutral pion event  in BigcalPi0Calaculations::Calculate() \n");*/
   // #endif

   fPi0Event->fEventNumber = fClusterEvent->fEventNumber;
   fPi0Event->fRunNumber   = fClusterEvent->fRunNumber;

   InSANEReconstruction * aReconPi0   = nullptr;
   BIGCALCluster        * aCluster    = nullptr;
   BIGCALCluster        * bCluster    = nullptr;
   Pi0ClusterPair       * aClustPair  = nullptr;

   fANNClusterRecon.SetBeamEvent(fEvents->BEAM);

   origin.SetXYZ(fEvents->BEAM->fXSlowRaster, fEvents->BEAM->fYSlowRaster, 0.0);
   //v1.SetXYZ(1, 2, 3);
   //v2.SetXYZ(1, 2, 3);
   //k1.SetXYZ(1, 2, 3);
   //k2.SetXYZ(1, 2, 3);

   fPi0Event->fNTracks = 0;
   Double_t mass       = 0.0;
   Double_t distance   = 0.0;
   Double_t E1         = 0.0;
   Double_t E2         = 0.0;


   // --------------------------------------------------------
   // Begin loop over clusters
   for (int kk = 0; kk < fClusterEvent->fNClusters  ; kk++) {
      aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
      if (aCluster->fTotalE < 6000.0) {

         // --------------------------------------------------
         // Applies the NN gamma corrections to cluster
         fANNClusterRecon.SetCluster(*aCluster);
         fCluster1 = fANNClusterRecon.AsPhoton();

         v1.SetXYZ(fCluster1.GetXmoment() + fCluster1.fDeltaX,
                   fCluster1.GetYmoment() + fCluster1.fDeltaY,
                   BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
         E1 = fCluster1.fTotalE;
         k1 = v1 - origin;
         k1.SetMag(E1);
         k1.Transform(fRotFromBCcoords);

         /// Loop over all clusters with index j>k as to not double count
         for (int j = kk + 1; j < fClusterEvent->fNClusters ; j++) {
            bCluster = (BIGCALCluster*)(*fClusters)[j] ;
            if (bCluster->fTotalE < 6000.0) {

               // --------------------------------------------------
               // Neural network calculation for the second cluster
               fANNClusterRecon.SetCluster(*bCluster);
               fCluster2 = fANNClusterRecon.AsPhoton();

               v2.SetXYZ(fCluster2.GetXmoment() + fCluster2.fDeltaX,
                         fCluster2.GetYmoment() + fCluster2.fDeltaY,
                         BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);

               E2 = fCluster2.fTotalE;
               k2 = v2 - origin;
               k2.SetMag(E2);
               k2.Transform(fRotFromBCcoords);

               if (fCluster1.fGoodCherenkovTDCHit == true) fPi0Event->fCherenkovHit = true;
               else if (fCluster2.fGoodCherenkovTDCHit == true) fPi0Event->fCherenkovHit = true;
               else fPi0Event->fCherenkovHit = false;

               //      if(aCluster->fGoodCherenkovTDCHit==false && bCluster->fGoodCherenkovTDCHit==false) {
               mass     = fPi0Event->CalculateMass(E1, E2, k1.Angle(k2));
               distance = (v1-v2).Mag();//TMath::Sqrt(TMath::Power(bCluster->GetXmoment() - aCluster->GetXmoment(), 2)
                                        // + TMath::Power(bCluster->GetYmoment() - aCluster->GetYmoment(), 2));

               /// \todo better cuts
               if (mass > 20.0  && mass < 1500.0 /*&& (E1/E2) < 10.0 && (E1/E2)>0.01 && distance > 4.0*/ /*cm*/) {
                  fFillEvent                      = true;
                  aClustPair                      = new((*fPi0Event->fClusterPairs)[fPi0Event->fNumberOfPairs]) Pi0ClusterPair(&fCluster1,&fCluster2);
                  fPi0Event->fNumberOfPairs++;
                  //aClustPair.fCluster1           = (aCluster);
                  //aClustPair.fCluster2           = (bCluster);
                  /*std::cout << "E(cluster = 1)= " << E1 << ", E(cluster = 2) = " << E2 << "\n";*/
                  aReconPi0                       = new((*fPi0Event->fReconstruction)[fPi0Event->fNTracks]) InSANEReconstruction();
                  aReconPi0->fNumber              = fPi0Event->fNTracks;
                  fPi0Event->fNTracks++;
                  aClustPair->fMass               = mass;
                  aClustPair->fAngle              = k1.Angle(k2);
                  aReconPi0->fMass                = mass;
                  aReconPi0->fAngle               = k1.Angle(k2);
                  aReconPi0->fPi0Momentum         = k1 + k2;
                  aReconPi0->fE1                  = E1;
                  aReconPi0->fE2                  = E2;
                  aReconPi0->fClusterDistance     = distance;
                  aReconPi0->fMomentum            = aReconPi0->fPi0Momentum.Mag();
                  aClustPair->fMomentum           = aReconPi0->fPi0Momentum.Mag();
                  aReconPi0->fEnergy              = TMath::Sqrt((CLHEP::M_pi0/CLHEP::MeV)*(CLHEP::M_pi0/CLHEP::MeV) + aReconPi0->fPi0Momentum.Mag2());
                  aClustPair->fEnergy             = aReconPi0->fEnergy;
                  aReconPi0->fPhi                 = aReconPi0->fPi0Momentum.Phi();
                  aReconPi0->fTheta               = aReconPi0->fPi0Momentum.Theta();
                  aClustPair->fPhi                = aReconPi0->fPi0Momentum.Phi();
                  aClustPair->fTheta              = aReconPi0->fPi0Momentum.Theta();
                  aClustPair->fHelicity           = fEvents->BEAM->fHelicity;
               }
               //      }
            }
         }
      }
   }
   return(1);
}
//___________________________________________________________________________________


ClassImp(Bigcal3ClusterPi0Calculation)

//______________________________________________________________________________
Int_t Bigcal3ClusterPi0Calculation::Calculate() {

   fFillEvent = false;
   // Pi0 events can be either pi0 trigger OR BETA trigger
   if( !(fEvents->TRIG->IsNeutralPionEvent() || fEvents->TRIG->IsBETAEvent()) ) return(0);

   // requires at least 2 clusters
   if(fClusterEvent->fNClusters < 2) return 0; 
   fPi0Event->ClearEvent();

   fPi0Event->fEventNumber = fClusterEvent->fEventNumber;
   fPi0Event->fRunNumber = fClusterEvent->fRunNumber;
   fPi0Event->fNumberOfGroups = 0;
   // Begin loop over clusters
   InSANEReconstruction * aReconPi0;

   /// A Pi0ClusterPair defines the virtual photon! \f$ \gamma^* \rightarrow e^+ e^- \f$
   Pi0ClusterPair * aClustPair ;

   BIGCALCluster * aCluster;
   BIGCALCluster * bCluster;
   BIGCALCluster * cCluster;
   BIGCALCluster * dCluster;

   TVector3 v1(1, 2, 3);
   TVector3 v2(1, 2, 3);
   TVector3 v3(1, 2, 3);
   TVector3 k1(0, 0, 0);
   TVector3 k2(0, 0, 0);
   TVector3 k3(0, 0, 0);
   TVector3 Ksum(0, 0, 0);
   TVector3 origin(0, 0, 0);

   //   fPi0Event->fNTracks=0;

   //Double_t mass, distance;
   Double_t E1, E2, E3;
   //Double_t thetaGammaGamma = 0;
   //Double_t thetapm = 0;
   //Double_t nu1 = 0;

   /// - first loop over Clusters
   if (fClusterEvent->fClusters->GetEntries() > 2)
      for (int kk = 0; kk < fClusterEvent->fClusters->GetEntries()  ; kk++) {

         /// Get and define cluster quantities
         /// this cluster is assumed to be an electron
         aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
         if (aCluster->fTotalE < 6000.0) {

            /// Get the result from the electron neural network
            //fANNEvent->SetEventValues(aCluster);
            //fANNEvent->GetCorrectionMLPInputNeuronArray(electronInputs);
            //aCluster->fDeltaE     = fElectronNNEnergy.Value(0, electronInputs);
            //aCluster->fDeltaTheta = fElectronNNTheta.Value(0, electronInputs);
            //aCluster->fDeltaPhi   = fElectronNNPhi.Value(0, electronInputs);
            //       aCluster->fDeltaE     = 0;
            //       aCluster->fDeltaTheta = 0;
            //       aCluster->fDeltaPhi   = 0;

            /// define the momentum
            E1 = aCluster->fTotalE;// + aCluster->fDeltaE ;
            v1.SetMagThetaPhi(E1, aCluster->GetTheta() + aCluster->fDeltaTheta, aCluster->GetPhi() + aCluster->fDeltaPhi);
            k1 = v1 - origin;
            k1.SetMag(E1);
            //       k1.Transform(fRotFromBCcoords); // don't use with cluster->GetTheta() ...

            /// - Loop over all clusters assuming that this is the positron
            /// - ie second loop over clusters
            ///   Note here we are double counting on the pair because we assume charges
            ///   and thus need to have both combinations as to not miss the correct one
            for (int j = 0; j < fClusterEvent->fNClusters ; j++) {

               /// Get and define cluster quantities
               bCluster = (BIGCALCluster*)(*fClusters)[j] ;
               if (bCluster != aCluster)
                  if (bCluster->fTotalE < 6000.0) {

                     /// Get the result from the electron neural network
                     //fANNEvent->SetEventValues(bCluster);
                     //fANNEvent->GetCorrectionMLPInputNeuronArray(positronInputs);
                     //bCluster->fDeltaE     = fPositronNNEnergy.Value(0, positronInputs);
                     //bCluster->fDeltaTheta = fPositronNNTheta.Value(0, positronInputs);
                     //bCluster->fDeltaPhi   = fPositronNNPhi.Value(0, positronInputs);

                     /// Set the momentum vector
                     E2 = bCluster->fTotalE;// + bCluster->fDeltaE ;
                     v2.SetMagThetaPhi(E2, bCluster->GetTheta() + bCluster->fDeltaTheta, bCluster->GetPhi() + bCluster->fDeltaPhi);
                     k2 = v2 - origin;
                     k2.SetMag(E2);
                     /*         k2.Transform(fRotFromBCcoords);*/
                     Ksum = k1 + k2;

                     aClustPair = new((*fPi0Event->fClusterPairs)[fPi0Event->fNumberOfPairs]) Pi0ClusterPair(aCluster,bCluster);
                     fPi0Event->fNumberOfPairs++;
                     //aClustPair->fCluster1 = (aCluster);
                     //aClustPair->fCluster2 = (bCluster);
                     aClustPair->fAngle = k2.Angle(k1);
                     aClustPair->fTheta = Ksum.Theta();
                     aClustPair->fPhi   = Ksum.Phi();
                     aClustPair->fMomentum = Ksum.Mag();
                     aClustPair->fMass  = TMath::Sqrt(2.0 * (E1 * E2) - 2.0 * E1 * E2 * TMath::Cos(aClustPair->fAngle));

                     /// - Third loop over all clusters and exclude ones already in the pair
                     for (int ll = 0; ll < fClusterEvent->fNClusters ; ll++) {

                        /// Get and define cluster quantities|
                        /// This cluster is assumed to be the photon
                        cCluster = (BIGCALCluster*)(*fClusters)[ll] ;

                        if (cCluster->fTotalE < 6000.0)
                           if (cCluster != aCluster && cCluster != bCluster) {

                              //fANNEvent->SetEventValues(cCluster);
                              //fANNEvent->GetCorrectionMLPInputNeuronArray(gammaInputs);
                              //cCluster->fDeltaE     = fGammaNNEnergy.Value(0, gammaInputs);
                              //cCluster->fDeltaTheta = fGammaNNTheta.Value(0, gammaInputs);
                              //cCluster->fDeltaPhi   = fGammaNNPhi.Value(0, gammaInputs);

                              fFillEvent = true;

                              E3 = cCluster->fTotalE;// + cCluster->fDeltaE ;
                              v3.SetMagThetaPhi(E3, cCluster->GetTheta() + cCluster->fDeltaTheta, cCluster->GetPhi() + cCluster->fDeltaPhi);

                              //                v3.SetXYZ( cCluster->GetXmoment(),
                              //                           cCluster->GetYmoment(),
                              //                           BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance );
                              k3 = v3 - origin;
                              k3.SetMag(E3);
                              //                k3.Transform(fRotFromBCcoords);

                              dCluster = new((*fPi0Event->fClusters)[fPi0Event->fNumberOfGroups]) BIGCALCluster(*cCluster);

                              /// Create a reconstruction corresponding to this pair and cluster
                              aReconPi0 = new((*fPi0Event->fReconstruction)[fPi0Event->fNumberOfGroups]) InSANEReconstruction();
                              aReconPi0->fNumber = fPi0Event->fNumberOfPairs - 1;
                              fPi0Event->fNumberOfGroups++;
                              aReconPi0->fNu = E3;

                              /// Calculate the mass!
                              //Double_t nu1, Double_t E1, Double_t E2, Double_t thetapm, Double_t thetaGammaGamma
                              aReconPi0->fAngle = k3.Angle(Ksum);
                              aReconPi0->fMass = fPi0Event->CalculateMass(E3, E1, E2, aClustPair->fAngle, aReconPi0->fAngle);
                              aReconPi0->fPi0Momentum = k1 + k2 + k3;
                              aReconPi0->fE1 = E1;
                              aReconPi0->fE2 = E2;
                              aReconPi0->fMomentum = aReconPi0->fPi0Momentum.Mag();
                              aReconPi0->fEnergy = TMath::Sqrt(138.0 * 138.0 + aReconPi0->fPi0Momentum.Mag2());
                              aReconPi0->fPhi = aReconPi0->fPi0Momentum.Phi();
                              aReconPi0->fTheta = aReconPi0->fPi0Momentum.Theta();
                           }
                     } // c loop
                  }
            } //b loop
         } // 6GeV
      } // a loop
   return(1);
}

//___________________________________________________________________________________


   ClassImp(BigcalOverlapCalculation)
Int_t BigcalOverlapCalculation::Calculate()
{

   //      fPi0Event->Clear();
   fFillEvent = false;
   if (!(fEvents->TRIG->IsClusterable())) return(0);

   // CALORIMETER CLUSTERING
   /*#ifdef INSANE_VERBOSE_EVENT*/
   /*  if(SANERunManager::GetRunManager()->GetVerbosity()>3)  printf("neutral pion event  in BigcalPi0Calaculations::Calculate() \n");*/
   // #endif

   //      fPi0Event->fEventNumber = fClusterEvent->fEventNumber;
   //      fPi0Event->fRunNumber = fClusterEvent->fRunNumber;
   // //
   // // Begin loop over clusters
   //    InSANEReconstruction * aReconPi0;
   //    BIGCALCluster * aCluster;
   //    BIGCALCluster * bCluster;
   //
   //    TVector3 v1(1,2,3);
   //    TVector3 v2(1,2,3);
   //    fPi0Event->fNTracks=0;
   //    Double_t mass,distance;
   //
   //    Double_t E1,E2;
   //    for (int kk=0; kk<fClusterEvent->fNClusters  ;kk++)
   //    {
   //       aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
   //       v1.SetXYZ( aCluster->GetXmoment(),   aCluster->GetYmoment(),
   //                  BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
   //       E1 = aCluster->fTotalE;
   //   // Loop over all clusters with index j>k as to not double count
   //      for (int j = kk+1; j < fClusterEvent->fNClusters ;j++) {
   //         bCluster = (BIGCALCluster*)(*fClusters)[j] ;
   //         v2.SetXYZ( bCluster->GetXmoment(),   bCluster->GetYmoment(),
   //                    BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
   //         E2 = bCluster->fTotalE;
   //         if(aCluster->fGoodCherenkovTDCHit==true) fPi0Event->fCherenkovHit = true;
   //         if(bCluster->fGoodCherenkovTDCHit==true) fPi0Event->fCherenkovHit = true;
   // //      if(aCluster->fGoodCherenkovTDCHit==false && bCluster->fGoodCherenkovTDCHit==false) {
   //         mass = fPi0Event->CalculateMass(E1,E2,v1.Angle(v2));
   //         distance = TMath::Sqrt( TMath::Power(bCluster->GetXmoment() -aCluster->GetXmoment(),2)
   //                  + TMath::Power(bCluster->GetYmoment() - aCluster->GetYmoment(),2) );
   //
   //         /// \todo better cuts
   //         if(mass > 20.0  && mass < 800.0 && (E1/E2) < 10.0 && (E1/E2)>0.01 && distance > 4.0 /*cm*/ ) {
   //           fPi0Event->fClusterPair->fCluster1 = (aCluster);
   //           fPi0Event->fClusterPair->fCluster2 = (bCluster);
   //       std::cout << "E(cluster=1)=" << E1 << ", E(cluster=2)=" << E2 << "\n";
   //           aReconPi0 = new( (*fPi0Event->fReconstruction)[fPi0Event->fNTracks]) InSANEReconstruction();
   //           aReconPi0->fNumber = fPi0Event->fNTracks;
   //           fPi0Event->fNTracks++;
   //           aReconPi0->fMass = mass;
   //           aReconPi0->fAngle = v1.Angle(v2);
   //           aReconPi0->fPi0Momentum = v1+v2;
   //           aReconPi0->fE1 = E1;
   //           aReconPi0->fE2 = E2;
   //           aReconPi0->fClusterDistance = distance;
   //           aReconPi0->fEnergy = TMath::Abs(aCluster->fClusterTime-bCluster->fClusterTime);
   //           aReconPi0->fTime = aCluster->fClusterTime - bCluster->fClusterTime;
   //         }
   // //      }
   //     }
   //   }
   return(1);
}
// //_______________________________________________________//
//
// ClassImp(GasCherenkovCalculation3)
// //_______________________________________________________//
//
// ClassImp(GasCherenkovCalculation4)


