#include  "InSANEDetectorHit.h"

ClassImp(InSANEDetectorHit)

//______________________________________________________________________________
InSANEDetectorHit::InSANEDetectorHit() {
   fLevel = -1;
   fChannel = 0;
   fHitNumber = -1;
   fPassesTimingCut = false;
}
//______________________________________________________________________________
InSANEDetectorHit::~InSANEDetectorHit() {
}
//______________________________________________________________________________
InSANEDetectorHit& InSANEDetectorHit::operator=(const InSANEDetectorHit &rhs) {
   // Check for self-assignment!
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   // Deallocate, allocate new space, copy values...
   fLevel = rhs.fLevel;
   fHitNumber = rhs.fHitNumber;
   fPassesTimingCut = rhs.fPassesTimingCut;
   fChannel = rhs.fChannel;
   return *this;
}
//______________________________________________________________________________
InSANEDetectorHit & InSANEDetectorHit::operator+=(const InSANEDetectorHit &rhs) {
   fLevel += rhs.fLevel;
   fHitNumber += rhs.fHitNumber;
   fPassesTimingCut += rhs.fPassesTimingCut;
   fChannel = rhs.fChannel;
   return *this;
}
//______________________________________________________________________________

void InSANEDetectorHit::Clear(Option_t *opt ) {
   fLevel = -1;
   fHitNumber = 0;
   fPassesTimingCut = false;
   fChannel = -1;
}
//______________________________________________________________________________

const InSANEDetectorHit InSANEDetectorHit::operator+(const InSANEDetectorHit &other) const {
   InSANEDetectorHit result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
   result += other;            // Use += to add other to the copy.
   return result;              // All done!
}
//______________________________________________________________________________

