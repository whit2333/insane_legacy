#include "SANERunManager.h"

#include "BETATimingAnalysis.h"
#include "BETAPedestalAnalysis.h"
#include "SANEScalerAnalyzer.h"
#include "BigcalClusterAnalyzer.h"
#include "InSANEEventDisplay.h"
#include "AnalyzerToInSANE.h"
#include "SANETargets.h"

ClassImp(SANERunManager)
//____________________________________________________________

SANERunManager::SANERunManager()
{
   fResultsFile = nullptr;
   fTarget = nullptr;
}
//_____________________________________________________________________________

SANERunManager::~SANERunManager()
{
   ;
}
//_____________________________________________________________________________

InSANETarget * SANERunManager::BuildTarget(Double_t packingFraction)
{

   if (fTarget) delete fTarget;
   fTarget = nullptr;

   auto * matH3 = new InSANETargetMaterial("H3", "H3 in Ammonia", 1, 1);
   matH3->fLength          = 3.0;    //cm
   matH3->fDensity         = 2.0 * 0.0770; // g/cm3
   matH3->fIsPolarized     = true;
   matH3->fPackingFraction = packingFraction;

   auto * matN14 = new InSANETargetMaterial("14N", "14N in Ammonia", 7, 14);
   matN14->fLength          = 3.0;    //cm
   matN14->fDensity         = 2.0 * 0.3576; // g/cm3
   matN14->fPackingFraction = packingFraction;

   auto * matHe = new InSANETargetMaterial("He", "LHe around Ammonia beads", 2, 4);
   matHe->fLength          = 3.0;    //cm
   matHe->fDensity         = 0.1450; // g/cm3
   matHe->fPackingFraction = 1.0 - packingFraction;

   auto * matLHe = new InSANETargetMaterial("LHe", "Liquid Helium", 2, 4);
   matLHe->fLength          = 0.50000 * 2.0;  //cm
   matLHe->fDensity         = 0.1450; // g/cm3

   auto * matAlEndcap = new InSANETargetMaterial("Al-endcap", "Aluminum target endcp", 13, 27);
   matAlEndcap->fLength          = 2.0 * 0.00381 ;  //cm
   matAlEndcap->fDensity         = 2.7; // g/cm3

   auto * matAlTail = new InSANETargetMaterial("Al-tail", "Aluminum target tail", 13, 27);
   matAlTail->fLength          = 2.0 * 0.01016  ;  //cm
   matAlTail->fDensity         = 2.7; // g/cm3

   auto * matAl4kShield = new InSANETargetMaterial("Al-4kshield", "Aluminum target 4K shield", 13, 27);
   matAl4kShield->fLength          = 2.0 * 0.00254  ;  //cm
   matAl4kShield->fDensity         = 2.7; // g/cm3

   auto * matCu = new InSANETargetMaterial("Cu", "NMR-Cu ", 29, 64);
   matCu->fLength          = 0.00050   ;    //cm
   matCu->fDensity         = 8.9600 ; // g/cm3

   auto * matNi = new InSANETargetMaterial("Ni", "NMR-Ni  ", 28, 59);
   matCu->fLength          = 0.00022   ;    //cm
   matCu->fDensity         = 8.9020  ; // g/cm3

   /// UVA Polarized Target During SANE
   auto * target = new InSANETarget("UVATarget", "UVA Polarized Target");
   target->AddMaterial(matH3);
   target->AddMaterial(matN14);
   target->AddMaterial(matLHe);
   target->AddMaterial(matHe);
   target->AddMaterial(matAlEndcap);
   target->AddMaterial(matAlTail);
   target->AddMaterial(matAl4kShield);
   target->AddMaterial(matCu);
   target->AddMaterial(matNi);

   /*   fTarget = target;*/
   return(target);
}
//_____________________________________________________________________________


Int_t SANERunManager::GeneratePreRunReport()
{
   if (fVerbosity > 0) std::cout << " o SANERunManager::GeneratePreRunReport() ... Generating pre-run Report\n";
   if (!fCurrentRun) {
      printf("x- Please use SetRun(#) first");
      return(-1);
   }

   /// Base method which creates the run report (and lots of other things)
   InSANERunManager::GeneratePreRunReport();
   //InSANEDatabaseManager * DBM = InSANEDatabaseManager::GetManager();

   /// Currently this method does Nothing!!!

//    TString SQLCOMMAND(
//    "SELECT start_date, start_time, end_date, end_time, end_date-start_date, end_time-start_time from runs_on_disk WHERE ");
//    SQLCOMMAND +=" run_number=" ;
//    SQLCOMMAND +=fRunNumber ;
//    SQLCOMMAND +=";" ;
   /*TSQLRow * row;
   TSQLResult * res = serv->Query(SQLCOMMAND.Data());

   int nrows = res->GetRowCount();
   int nfields = res->GetFieldCount();
   for (int i = 0; i < nrows; i++)
   {
      row = res->Next();
      for (int j = 0; j < nfields; j++)
      {
     //    cout << TString(row->GetField(j) ) << "\n";
      }
    delete row;
   }
   delete res;
   */
   return(0);
}
//____________________________________________________________

Int_t SANERunManager::GenerateRunReport()
{
   std::cout << " o SANERunManager::GenerateRunReport() ... Generating run Report\n";
   InSANERunManager::GenerateRunReport();

   return(0);
}
//____________________________________________________________

Int_t SANERunManager::ConvertRawData()
{
   if (!IsRunSet()) {
      printf("x- Please use SetRun(#) first\n");
      return(-1);
   } else {
      TChain chain("h9509"); // name of tree
      chain.Add(Form("data/h2rootfiles/sane%d*root", fRunNumber));
      if (fVerbosity > 0) std::cout << " - Run Manager processing raw data (h2rootfiles) for run " << fRunNumber << ".\n";
      auto * analyzer = new AnalyzerToInSANE();
      chain.Process((TSelector *)analyzer, Form("%d", fRunNumber));
      if (GetVerbosity() > 0) std::cout << " - Run Manager Finished Processing raw data (h2rootfiles) for run " << fRunNumber << ".\n";

      return(0);
   }
}
//____________________________________________________________

Int_t SANERunManager::CalculatePedestals(const char * sourceTreeName)
{
   if (fVerbosity > 0) std::cout << " - SANERunManager::CalculatePedestals " << fRunNumber << ".\n";

   if (!IsRunSet()) {
      printf("Please use SetRun(#) first\n");
      return(-1);
   }

   auto * t = (TTree*)gROOT->FindObject(sourceTreeName);
   if (!t) {
      printf("Tree:%s \n       was NOT FOUND. Aborting... \n", sourceTreeName);
      return(-1);
   }

   auto * ped = new BETAPedestalAnalysis(sourceTreeName);
   ped->AnalyzeRun();
   ped->FitHistograms();
   ped->CreatePlots();
   delete ped;

   return(0);
}
//____________________________________________________________

Int_t SANERunManager::CalculateTiming(const char * sourceTreeName)
{
   if (!IsRunSet()) {
      printf("Please use SetRun(#) first");
      return(-1);
   }

   auto * t = (TTree*)gROOT->FindObject(sourceTreeName);
   if (!t) {
      printf("Tree:%s \n       was NOT FOUND. Aborting... \n", sourceTreeName);
      return(-1);
   }

   auto * timing = new BETATimingAnalysis(sourceTreeName);
   timing->AnalyzeRun();
   timing->FitHistograms();
   timing->CreatePlots();
   delete timing;

   return(0);

}
//____________________________________________________________
/*
Int_t SANERunManager::Cluster(const char * sourceTreeName) {
  if (!IsRunSet()) {printf("Please use SetRun(#) first"); return(-1);}

   TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
   if( !t ) {printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName); return(-1);}
   TTree * tOut = new TTree("Clusters","bigcal clustering");
  SANEClusteringAnalysis * clust= new SANEClusteringAnalysis(sourceTreeName);
//  clust->fClusterProcessor->SetEventReview(false);
  clust->fClusterProcessor->SetEventReview(false);
  clust->fEventDisplay=false;
  clust->fAnalysisType  = 3; // 2 pi0, 3 beta
  clust->SetClusterTree(tOut);
  clust->AnalyzeRun();
  delete clust;

  return(0);
}*/
//____________________________________________________________
/*
Int_t SANERunManager::ClusterWithDisplay(const char * sourceTreeName) {
  if( !fRunNumber || fRunNumber == 0 ) {
    printf("Please use SetRun(#) first");
    return(-1);
  } else {

   TTree * t =
     (TTree*)gROOT->FindObject(sourceTreeName);
   if( !t ) {
     printf("Tree:%s \n    was NOT FOUND. Aborting... \n",sourceTreeName); return(-1);
   }
   TTree * tOut = new TTree("Clusters","Clustering Results");
  SANEClusteringAnalysis * clust= new SANEClusteringAnalysis(sourceTreeName);
  clust->fEventDisplay = true;
  clust->fAnalysisType  = 3; // beta2 trigger
  clust->SetClusterTree(tOut);
  clust->AnalyzeRun();
  delete clust;

   return(0);
  }
}*/
//____________________________________________________________

/*Int_t SANERunManager::ClusterWithDisplayPi0(const char * sourceTreeName) {
  if( !fRunNumber || fRunNumber == 0 ) {
    printf("Please use SetRun(#) first");
    return(-1);
  } else {

   TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
   if( !t ) {printf("Tree:%s \n    was NOT FOUND. Aborting... \n",sourceTreeName); return(-1);}
   TTree * tOut = new TTree("Clusters","Clustering Results");
  SANEClusteringAnalysis * clust= new SANEClusteringAnalysis(sourceTreeName);
  clust->fEventDisplay = true;
  clust->fAnalysisType  = 2; // beta2 trigger
  clust->SetClusterTree(tOut);
  clust->AnalyzeRun();
  delete clust;

   return(0);
  }
}*/
//____________________________________________________________

/*Int_t SANERunManager::RunZerothPass(const char * sourceTreeName) {

  if (!IsRunSet()) {printf("Please use SetRun(#) first"); return(-1);}

// Tree that is used as cloned structure and sampling pool
  TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
   if( !t ) {
     printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName);
     return(-1);
   }
    gROOT->SetBatch(kTRUE);

// Create Zeroth Pass "corrector", to which, "corrections" are added.
  SANEZerothPassAnalyzer * corrector = new SANEZerothPassAnalyzer(Form("%sZerothPass",sourceTreeName) ,sourceTreeName);

  BETAPedestalCorrection* b1 = new BETAPedestalCorrection();
    b1->SetEvent( corrector->InSANEDetectorAnalysis::fEvents );
    b1->SetDetectors(corrector);
  corrector->AddCorrection(b1);

  corrector->Initialize(t);

  corrector->ApplyCorrections();
  delete corrector;
  delete b1;
    gROOT->SetBatch(kFALSE);

/// \todo this should go to a higher level
//  CreateNewFile();

   return(0);
}*/
//____________________________________________________________

/*Int_t SANERunManager::RunAlternateFirstPass(const char * sourceTreeName) {

  if (!IsRunSet()) {printf("Please use SetRun(#) first"); return(-1);}

// Tree that is used as cloned structure and sampling pool
  TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
   if( !t ) {
     printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName);
     return(-1);
   }

// Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
  SANEFirstPassAnalyzer * analyzer1 = new SANEFirstPassAnalyzer(Form("%sFirstPass",sourceTreeName) ,sourceTreeName);

  GasCherenkovCalculation1 * g1 = new GasCherenkovCalculation1();
    g1->SetEvent( analyzer1->InSANEDetectorAnalysis::fEvents );
    g1->SetDetectors(analyzer1);

  BigcalClusteringCalculation1 * b1 = new BigcalClusteringCalculation1();

    analyzer1->SetClusterTree( new TTree("Clusters","Cluster Data") );

  b1->SetEvent( analyzer1->InSANEDetectorAnalysis::fEvents );

    b1->SetClusterEvent( analyzer1->SANEClusteringAnalysis::fClusterEvent);
    b1->SetClusterProcessor();

  analyzer1->SetClusterProcessor((BIGCALClusterProcessor*)b1->GetClusterProcessor());

    b1->SetDetectors(analyzer1);

  analyzer1->AddCalculation(g1);
  analyzer1->AddCalculation(b1);

  analyzer1->Initialize(t);
  analyzer1->Process();


  delete analyzer1;
  delete b1;
  delete g1;


/// \todo this should go to a higher level
//  CreateNewFile();
   return(0);
}*/
//____________________________________________________________

/*Int_t SANERunManager::RunFirstPass(const char * sourceTreeName ) {
  if (!IsRunSet()) {printf("Please use SetRun(#) first"); return(-1);}
// Tree that is used as cloned structure and sampling pool
//   TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
//    if( !t ) {
//      printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName);
//      return(-1);
//    }

// Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
  SANEFirstPassAnalyzer * analyzer = new SANEFirstPassAnalyzer("betaDetectors1" ,sourceTreeName);*/
/*
  analyzer->InSANEAnalyzer::fEventDisplay = new InSANEEventDisplay();
  analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fBigcalDetector->fEventTimingHist);
  analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fBigcalDetector->fEventTrigTimingHist);
  analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fCherenkovDetector->fCerHits);
  GasCherenkovCalculation1 * g1 = new GasCherenkovCalculation1();
    g1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
    g1->SetDetectors(analyzer);

  LuciteHodoscopeCalculation1 * l1 = new LuciteHodoscopeCalculation1();
    l1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
    l1->SetDetectors(analyzer);

  BigcalClusteringCalculation1 * b1 = new BigcalClusteringCalculation1();
   BigcalPi0Calaculations * b2 = new BigcalPi0Calaculations();

  LocalMaxClusterProcessor * clusterProcessor = new LocalMaxClusterProcessor();

  analyzer->SetClusterTree( new TTree("Clusters","Cluster Data") );

    b1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
    b1->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
    b1->SetClusterProcessor(clusterProcessor);

    b2->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
    b2->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
//     b2->SetClusterProcessor(clusterProcessor); // not need for b2

  b1->SetDetectors(analyzer);
  b2->SetDetectors(analyzer);

  analyzer->SetClusterProcessor((BIGCALClusterProcessor*)b1->GetClusterProcessor());

  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fCherenkovDetector);
  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fBigcalDetector);

  analyzer->AddCalculation(g1);
  analyzer->AddCalculation(l1);
  analyzer->AddCalculation(b1);
  analyzer->AddCalculation(b2);

  analyzer->Initialize();
  analyzer->Process();


  delete analyzer;
  delete b1;
  delete l1;
  delete g1;

/// \todo this should go to a higher level
//  CreateNewFile();
   return(0);
}
*/
//____________________________________________________________//

// Int_t SANERunManager::RunScalerAnalysis(const char * sourceTreeName) {
//
//   if( !fRunNumber || fRunNumber == 0 ) {
//     printf("Please use SetRun(#) first");
//     return(-1);
//   } else {
//
// // Tree that is used as cloned structure and sampling pool
//   TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
//    if( !t ) {
//      printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName);
//      return(-1);
//    }
// //  TTree * t2 = new TTree("Scalers2","SANE Scalers");
// // Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
//   SANEScalerAnalyzer * analyzer = new SANEScalerAnalyzer("Scalers" ,sourceTreeName);
//
//  // analyzer->SetClusterTree(t2 );
//
//   analyzer->AddDetector(analyzer->fCherenkovDetector);
//
//   analyzer->Initialize(t);
//
//   analyzer->Process();
//
//
//   delete analyzer;
//
//
// /// \todo this should go to a higher level
// //  CreateNewFile();
//   }
//    return(0);
// }

/*Int_t SANERunManager::RunPi0Clustering(const char * sourceTreeName) {
  if (!IsRunSet()) {printf("Please use SetRun(#) first"); return(-1);}

   TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
   if( !t ) {printf("Tree:%s \n    was NOT FOUND. Aborting... \n",sourceTreeName); return(-1);}

// tOut needs to be named Clusters otherwise SANEEvents craps out????
   TTree * tOut = new TTree("Clusters","Pi0 Cluster Data");

//   BigcalClusterAnalyzer * analyzer = new BigcalClusterAnalyzer(Form("%sPi0",sourceTreeName) ,sourceTreeName);
//
//   BigcalPi0Calaculations * b1 = new BigcalPi0Calaculations();
//
//   analyzer->SetClusterTree( tOut );
//     b1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//     b1->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent );
//     b1->SetClusterProcessor( new LocalMaxClusterProcessor() );
//
//   analyzer->SetClusterProcessor( (BIGCALClusterProcessor*)b1->GetClusterProcessor() );
//     b1->SetDetectors(analyzer);
//
//   analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fCherenkovDetector);
//
//   analyzer->AddCalculation(b1);
//
//   analyzer->Initialize(t);
//
//   analyzer->Process();
  delete analyzer;
  delete b1;


   return(0);
}*/
//____________________________________________________________




