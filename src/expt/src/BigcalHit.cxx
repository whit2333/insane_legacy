#include  "BigcalHit.h"

ClassImp(BigcalHit)

//______________________________________________________________________________
BigcalHit::BigcalHit() : 
   fiCell(-99), fjCell(-99),
   fTDC(-9999), fTDCLevel(-1), fTDCGroup(-1),
   fTDCRow(-1), fTDCRow2(-1),
   fADC(0), fEnergy(0.0), fBlockNumber(-1),
   fTDC2(-9999), fTDCAlign(-9999), fTDCAlign2(-9999),
   fTriggerHalf(-1)
{
}
//______________________________________________________________________________
BigcalHit::~BigcalHit(){ }
//______________________________________________________________________________
void BigcalHit::Clear(Option_t *opt )
{
   InSANEDetectorHit::Clear(opt);
   fTriggerHalf = -1;
   fiCell       = -1;
   fjCell       = -1;
   fTDCLevel    = -1;
   fTDCGroup    = -1;
   fTDCRow      = -1;
   fTDCRow2     = -1;
   fADC         = 0;
   fEnergy      = 0.0;
   fTDC         = -9999;
   fTDC2        = -9999;
   fBlockNumber = -1;
   fTDCAlign    = -9999;
   fTDCAlign2   = -9999;
}
//______________________________________________________________________________
BigcalHit& BigcalHit::operator=(const BigcalHit &rhs)
{
   // Check for self-assignment!
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   // Deallocate, allocate new space, copy values...
   InSANEDetectorHit::operator=(rhs);

   fHitNumber       = rhs.fHitNumber;
   fPassesTimingCut = rhs.fPassesTimingCut;
   fBlockNumber     = rhs.fBlockNumber;
   fTriggerHalf     = rhs.fTriggerHalf;
   fEnergy          = rhs.fEnergy;
   fiCell           = rhs.fiCell;
   fjCell           = rhs.fjCell;
   fTDCLevel        = rhs.fTDCLevel;
   fTDCGroup        = rhs.fTDCGroup;
   fTDCRow          = rhs.fTDCRow;
   fTDCRow2         = rhs.fTDCRow2;
   fADC             = rhs.fADC;
   fTDC             = rhs.fTDC;
   fTDC2            = rhs.fTDC2;
   fTDCAlign        = rhs.fTDCAlign;
   fTDCAlign2       = rhs.fTDCAlign2;
   return *this;
}
//______________________________________________________________________________
BigcalHit& BigcalHit::operator+=(const BigcalHit &rhs)
{
   //fLevel           += rhs.fLevel;
   //fHitNumber       += rhs.fHitNumber;
   //fPassesTimingCut += rhs.fPassesTimingCut;
   fEnergy          += rhs.fEnergy;
   //fTDCLevel        += rhs.fTDCLevel;
   //fTDCGroup        += rhs.fTDCGroup;
   //fTDCRow          += rhs.fTDCRow;
   fADC             += rhs.fADC;
   //fTDC             += rhs.fTDC;
   //fTDC2            += rhs.fTDC2;
   //fTDCAlign        += rhs.fTDCAlign;
   //fTDCAlign2       += rhs.fTDCAlign2;
   return *this;
}
//______________________________________________________________________________
const BigcalHit BigcalHit::operator+(const BigcalHit &other) const
{
   // Add this instance's value to other, and return a new instance
   // with the result.
   BigcalHit result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
   result += other;            // Use += to add other to the copy.
   return result;              // All done!
}
//______________________________________________________________________________

