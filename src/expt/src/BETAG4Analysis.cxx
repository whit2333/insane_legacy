#define BETAG4Analysis_cxx
#include "SANEEvents.h"

#include "BETAG4Analysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "BETAEvent.h"
#include "GasCherenkovHit.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "TClonesArray.h"
#include "InSANERun.h"

ClassImp(BETAG4Analysis)
//_______________________________________________________//

BETAG4Analysis::BETAG4Analysis(const char * sourceTreeName) : InSANEAnalysis(sourceTreeName)
{
   events = nullptr;
   events = new SANEEvents();
/// \todo{The following or something like it should be put in an ABC}

   fAnalysisFile = new TFile(Form("data/montecarlo/beta.run.%d.root", fRunNumber));
   if (!fAnalysisFile) printf(" NO FILE data/montecarlo/beta.run.%d.root\n", fRunNumber);
//  fAnalysisTree=(TChain *)gROOT->FindObject("betaDetectors");
   printf("\n- Creating Events\n");
   //events->SetBranches();
   fRun = new InSANERun(0);
}

BETAG4Analysis::BETAG4Analysis(Int_t run): InSANEAnalysis(Form("run%d", run))
{

   if (run > 70000) {
      fAnalysisFile = new TFile(Form("data/rootfiles/SANE%d.root", fRunNumber));
      if (!fAnalysisFile) printf(" NO FILE \n");
   } else {
      fAnalysisFile = new TFile(Form("data/montecarlo/beta.run.%d.root", fRunNumber));
      if (!fAnalysisFile) printf(" NO FILE data/montecarlo/beta.run.%d.root\n", fRunNumber);
   }
//  fAnalysisFile = new TFile(Form("data/montecarlo/beta.run.%d.root",fRunNumber));
//  if(!fAnalysisFile) printf(" NO FILE data/montecarlo/beta.run.%d.root\n",fRunNumber);

   printf("- Creating Events\n");
   events = nullptr;
   events = new SANEEvents();
   fRun = new InSANERun(run);
   fRun->fBeamEnergy = 3.0;
//  fAnalysisTree=(TChain *)gROOT->FindObject("betaDetectors");


//  events->SetBranches();
//   aBetaEvent=events->BETA;
//   bcEvent = events->BETA->fBigcalEvent;
//   lhEvent = events->BETA->fLuciteHodoscopeEvent;
//   gcEvent = events->BETA->fGasCherenkovEvent;
//   ftEvent = events->BETA->fForwardTrackerEvent;

//  AllocateHistograms();
   /*
     printf("  - Output File: data/rootfiles/Cherenkov-1-%d.root\n",fRunNumber);
     fOutputFile = new TFile(Form("data/rootfiles/Cherenkov-1-%d.root",fRunNumber),"RECREATE");
     if( fOutputFile )  printf("  + Output File Opened\n");

   //Make sure this comes after opening the file
     fOutputTree = new TTree("cherenkovEvents","Detector Event Data");
     fOutputTree->Branch("cherenkovBetaDetectorEvent","BETAEvent",&(events->BETA) );
     fOutputTree->Branch("cherenkovTrigDetectorEvent","InSANETriggerEvent",&events->TRIG);
     fOutputTree->Branch("cherenkovHmsDetectorEvent","HMSEvent",&events->HMS);
     fOutputTree->Branch("cherenkovBeamDetectorEvent","HallCBeamEvent",&events->BEAM);
     fOutputTree->SetAutoSave();

   for(int i=0;i<12;i++) {
        cer_tdc_hist[i] = new TH1F(Form("cer_tdc_hist_%d",i),Form("Cherenkov TDC %d",i), 200,-3000,-1200);
        cer_adc_hist[i] = new TH1F(Form("cer_adc_%d",i),Form("Cherenkov  ADC %d",i), 100,0,4000);
        cer_tdc_vs_adc_hist[i] = new TH2F(Form("cer_tdc_vs_adc_hist_%d",i),Form("Cherenkov TDC vs ADC%d",i), 200,0,4000, 200,-3000,-1200);
   }

   for(int i=0;i<12*56;i++) {
        cer_tdc_hist_luc[i] = new TH1F(Form("cer_tdc_hist_luc_%d",i),Form("Cherenkov TDC %d",i), 200,-3000,-1200);
        cer_adc_hist_luc[i] = new TH1F(Form("cer_adc_luc_%d",i),Form("Cherenkov  ADC %d",i), 100,0,4000);
        cer_tdc_vs_adc_hist_luc[i] = new TH2F(Form("cer_tdc_vs_adc_hist_luc_%d",i),Form("Cherenkov TDC vs ADC%d",i), 100,0,4000, 200,-3000,-1200);
   }
   for(int i=0;i<12*56;i++) {
        luc_tdc_hist[i] = new TH1F(Form("luc_tdc_hist_%d",i),Form("Hodoscope TDC %d",i), 200,-3000,-1200);
        luc_adc_hist[i] = new TH1F(Form("luc_adc_%d",i),Form("Hodoscope  ADC %d",i), 200,-100,2900);
        luc_tdc_vs_adc_hist[i] = new TH2F(Form("luc_tdc_vs_adc_hist_%d",i),Form("Hodoscope TDC vs ADC %d",i), 200,-100,2900, 200,-3000,-1200);
   }

     /// \todo {Implement DETECTORPhysicsEvent }*/
}
//_______________________________________________________//

BETAG4Analysis::~BETAG4Analysis()
{

   if (fOutputTree)  fOutputTree->Write();
   if (fOutputTree)  fOutputFile->Write();
   if (fOutputFile)  fOutputFile->Close();

   ;
}
//_______________________________________________________//

Int_t BETAG4Analysis::AnalyzeRun()
{
// Detectors used
//   cout << " Creating detector configurations for this run... \n";
// //   ForwardTrackerDetector *    tracker   = new ForwardTrackerDetector(fRunNumber);
//    GasCherenkovDetector *      cherenkov = new GasCherenkovDetector(fRunNumber);
//    LuciteHodoscopeDetector *   hodoscope = new LuciteHodoscopeDetector(fRunNumber);
// //   BigcalDetector *            bigcal    = new BigcalDetector(fRunNumber);
//
// ///// EVENT LOOP  //////
//   Int_t nevent = events->betaTree->GetEntries();
//   cout << "\n" << nevent << " ENTRIES \n";
//   TClonesArray * cerHits = gcEvent->fHits;
//   TClonesArray * lucHits = lhEvent->fHits;
//   TClonesArray * bigcalHits = bcEvent->fHits;
//   TClonesArray * trackerHits = ftEvent->fHits;
//
//   for (int iEVENT=0;iEVENT<nevent;iEVENT++)     {
// //    printf(" what");
//
//     events->GetEvent(iEVENT);
// ///\todo{ I think I can remove this method by using TTreeIndex and set friends}
//     if( events->TRIG->gen_event_trigtype[3] ) { // BETA2
// //    printf(" ok");
//       // Loop over lucite hits
//       for(int kk=0; kk< lhEvent->fNumberOfHits;kk++) {
//       aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;
//
//       //Loose Lucite TDC Cut (Hits on Both sides of the bar pass a TDC window cut)
//       if(
//        TMath::Abs(aLucHit->fNegativeTDC - hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeak)
//        < 5.0*hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeakWidth  &&
//        TMath::Abs(aLucHit->fPositiveTDC - hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeak)
//        < 5.0*hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeakWidth) {
//                hodoscope->fLucHit=true;
//          if(hodoscope->fBarTimedTDCHit[aLucHit->fRow-1])
//          hodoscope->fBarDoubleTimedTDCHit[aLucHit->fRow-1]=true;
//          hodoscope->fBarTimedTDCHit[aLucHit->fRow-1]=true;
//       }
//     } // End loop over Lucite Hits
//
// // Now we have the hodoscope hits filled and want to translate this to an
// // image of the cherenkov. For each bar, we produce cherenkov ADC and TDC histograms
// // as well as fill the
//
// // loop over cherenkov hits for event
//     for (int kk=0; kk< gcEvent->fNumberOfHits;kk++) {
//       aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
//       if( TMath::Abs(aCerHit->fTDC - cherenkov->cherenkovTimings[aCerHit->fMirrorNumber-1]->fTDCPeak) <  5.0*cherenkov->cherenkovTimings[aCerHit->fMirrorNumber-1]->fTDCPeakWidth )
//       { //passes VERY wide tdc cut
//          cer_adc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fADC);
//          cer_tdc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fTDC);
//          cer_tdc_vs_adc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fADC,aCerHit->fTDC);
//         //Fill separte 12 cherenkov hists for each bar
//     if(hodoscope->fLucHit) {
//         for(int jj=0;jj<56;jj++) if(hodoscope->fBarTimedTDCHit[jj]) {
// /*printf(" ner");*/
//          cer_adc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fADC);
//          cer_tdc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fTDC);
//          cer_tdc_vs_adc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fADC,aCerHit->fTDC);
//         }
//       }// end lucCut
//     }
// //    fOutputTree->Fill();
//     } // End loop over Cherenkov signals
//     } // beta trigger bit
//         hodoscope->ClearEvent();
//         aBetaEvent->ClearEvent();
//   } // END OF EVENT LOOP

   return(0);
}
//_______________________________________________________//

Int_t BETAG4Analysis::Visualize()
{
//   TCanvas * c1;
//   for(int j=0;j<12;j++){
//     c1  = new TCanvas(Form("Cherenkov%dWithLuciteCut",j+1),Form("Cherenkov Mirror %d",j+1));
//     c1->Divide(7,4);
//       for(int i =0;i<28;i++) {
//       c1->cd(i+1);
//       cer_adc_hist_luc[j*56+27-i]->Draw();
//     }
//   c1->SaveAs(Form("plots/%d/CherenkovMirror%dByLuciteBar.jpg",fRunNumber,j));
//   c1->SaveAs(Form("plots/%d/CherenkovMirror%dByLuciteBar.ps",fRunNumber,j));
//   }
//
//   c1  = new TCanvas("CherenkovADCWithTDCCut","Cherenkov ADCs with a TDC cut");
//   c1->Divide(4,3);
//   for(int i=0;i<12;i++) {
//     c1->cd(i+1);
//     cer_adc_hist[i]->Draw();
//   }
//   c1->SaveAs(Form("plots/%d/CherenkovADCWithTDCCut.jpg",fRunNumber));
//   c1->SaveAs(Form("plots/%d/CherenkovADCWithTDCCut.ps",fRunNumber));
//
//   c1  = new TCanvas("CherenkovTDCWithTDCCut","Cherenkov TDCs with a TDC cut");
//   c1->Divide(4,3);
//   for(int i=0;i<12;i++) {
//     c1->cd(i+1);
//     cer_tdc_hist[i]->Draw();
//   }
//   c1->SaveAs(Form("plots/%d/CherenkovTDCWithTDCCut.jpg",fRunNumber));
//   c1->SaveAs(Form("plots/%d/CherenkovTDCWithTDCCut.ps",fRunNumber));
//
//   c1  = new TCanvas("CherenkovTDCvsADCWithTDCCut","Cherenkov TDC vs ADC with a TDC cut");
//   c1->Divide(4,3);
//   for(int i=0;i<12;i++) {
//     c1->cd(i+1);
//     cer_tdc_vs_adc_hist[i]->Draw();
//   }
//   c1->SaveAs(Form("plots/%d/CherenkovTDCvsADCWithTDCCut.jpg",fRunNumber));
//   c1->SaveAs(Form("plots/%d/CherenkovTDCvsADCWithTDCCut.ps",fRunNumber));
// //       cer_tdc_hist[i] = new TH1F(Form("cer_tdc_hist_%d",i),Form("Cherenkov TDC %d",i), 200,-3000,-1200);
// //       cer_adc_hist[i] = new TH1F(Form("cer_adc_%d",i),Form("Cherenkov  ADC %d",i), 100,0,4000);
// //       cer_tdc_vs_adc_hist[i] = new TH2F(Form("cer_tdc_vs_adc_hist_%d",i),Form("Cherenkov TDC vs ADC%d",i), 200,0,4000, 200,-3000,-1200);

   return(0);
}
//_______________________________________________________//

