#include  "LuciteHodoscopeEvent.h"
#include <TROOT.h>
#include <TClonesArray.h>
#include "InSANERunManager.h"

ClassImp(LuciteHodoscopeEvent)

TClonesArray *LuciteHodoscopeEvent::fgLuciteHodoscopeHits = nullptr;
TClonesArray *LuciteHodoscopeEvent::fgLuciteHodoscopePositionHits = nullptr;

LuciteHodoscopeEvent::LuciteHodoscopeEvent()
{

   if (!fgLuciteHodoscopeHits) {
      fgLuciteHodoscopeHits = new TClonesArray("LuciteHodoscopeHit", 5);
   }
   fLuciteHits = fgLuciteHodoscopeHits;

   if (!fgLuciteHodoscopePositionHits) {
      fgLuciteHodoscopePositionHits = new TClonesArray("LuciteHodoscopePositionHit", 5);
   }
   fLucitePositionHits = fgLuciteHodoscopePositionHits;

   ClearEvent();
}

//______________________________________________________________________________
LuciteHodoscopeEvent::~LuciteHodoscopeEvent()
{
   /*  if(fLuciteHits) delete fLuciteHits;
     fLuciteHits = 0;
     fgLuciteHodoscopeHits = 0;
     if(fLucitePositionHits) delete fLucitePositionHits;
     fgLuciteHodoscopePositionHits = 0;
     fLucitePositionHits = 0; */
}
//______________________________________________________________________________
Int_t LuciteHodoscopeEvent::AllocateHitArray() {
   if (!fgLuciteHodoscopeHits) {
      fgLuciteHodoscopeHits = new TClonesArray("LuciteHodoscopeHit", 5);
   }
   fLuciteHits = fgLuciteHodoscopeHits;

   if (!fgLuciteHodoscopePositionHits) {
      fgLuciteHodoscopePositionHits = new TClonesArray("LuciteHodoscopePositionHit", 5);
   }
   fLucitePositionHits = fgLuciteHodoscopePositionHits;

   return(0);
}
//______________________________________________________________________________
void LuciteHodoscopeEvent::ClearEvent(const char * opt) {
   /*  if(InSANERunManager::GetRunManager()->fVerbosity > 2) Print();*/
   if (fLuciteHits)if(fLuciteHits->GetEntries() > 110 ) {
       //std::cout << " calling  fLuciteHits->ExpandCreate(2); \n";
       //std::cout << " had " << fLuciteHits->GetEntries() << " entries. \n";
       fLuciteHits->ExpandCreate(50);
   }
   if (fLuciteHits)         fLuciteHits->Clear(opt);
   /*  std::cout << " Cleared fHits \n";*/
   if (fLucitePositionHits) if(fLucitePositionHits->GetEntries() > 50 ) {
       //std::cout << " calling  fLucitePositionHits->ExpandCreate(5); \n";
       //std::cout << " had " << fLucitePositionHits->GetEntries() << " entries. \n";
       fLucitePositionHits->ExpandCreate(10);
   }
   if (fLucitePositionHits) fLucitePositionHits->Clear(opt);
//   std::cout << " Cleared fPositionHits \n";
   fNumberOfPositionHits = 0;
   fNumberOfHits = 0;
   fNumberOfTDCHits = 0;
   fNumberOfADCHits = 0;
   fNumberOfTimedADCHits = 0;
   fNumberOfTimedTDCHits = 0;

}


