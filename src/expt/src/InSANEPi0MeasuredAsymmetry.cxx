#include "InSANEPi0MeasuredAsymmetry.h"
#include "InSANEMathFunc.h"
#include <math.h>

//_____________________________________________________________________________
ClassImp(InSANEPi0MeasuredAsymmetry)

//_____________________________________________________________________________
InSANEPi0MeasuredAsymmetry::InSANEPi0MeasuredAsymmetry(Int_t n): InSANEAsymmetry(n) {

   fDilutionFromTarget = nullptr;
   fPi0ClusterPair = nullptr;

   fDilution        = 0.12;
   fHalfWavePlate   = 1.0;
   fChargeSwap      = false;
   fPt              = 1.0;
   fPb              = 1.0;
   fAsymmetry       = 0.0;
   fError           = 0.0;
   fLiveTime[0]     = 1.0;
   fLiveTime[1]     = 1.0;
   fCharge[0]       = 1.0;
   fCharge[1]       = 1.0;
   fRunNumber       = 0;

   // Variable ranges. 
   fEnergyMin    = 200.0;
   fEnergyMax    = 4000.0;
   fPtMin        = 200;
   fPtMax        = 4000.0;
   fPhiMin      = -70.0 * TMath::Pi()/180.0;
   fPhiMax      =  70.0 * TMath::Pi()/180.0;
   fThetaMin    = 20.0 * TMath::Pi() / 180.0;
   fThetaMax    = 60.0 * TMath::Pi() / 180.0;

   fBinningBeamEnergy = 5.9;

   fAsymmetryVsPt          = nullptr;
   fAsymmetryVsE           = nullptr;
   fAsymmetryVsTheta       = nullptr;
   fAsymmetryVsPhi         = nullptr;

   fDilutionVsPt          = nullptr;
   fDilutionVsE           = nullptr;
   fDilutionVsTheta       = nullptr;
   fDilutionVsPhi         = nullptr;

   fSystErrVsPt          = nullptr;
   fSystErrVsE           = nullptr;
   fSystErrVsTheta       = nullptr;
   fSystErrVsPhi         = nullptr;

   fNPlusVsPt           = nullptr;
   fNMinusVsPt          = nullptr;

   fNPlusVsE            = nullptr;
   fNMinusVsE           = nullptr;

   fNPlusVsPhi          = nullptr;
   fNMinusVsPhi         = nullptr;

   fNPlusVsTheta        = nullptr;
   fNMinusVsTheta       = nullptr;

   
   fAvgKineVsPt = nullptr;
   fAvgKineVsE = nullptr;
   fAvgKineVsTheta = nullptr;
   fAvgKineVsPhi = nullptr;

   /// Call this so that  all histograms have been created
   //InSANEPi0MeasuredAsymmetry::Initialize();
   
}
//_____________________________________________________________________________

InSANEPi0MeasuredAsymmetry::~InSANEPi0MeasuredAsymmetry()
{
   //---------------------
   if(fNPlusVsPt) delete fNPlusVsPt;
   if(fNPlusVsPhi) delete fNPlusVsPhi;
   if(fNPlusVsTheta) delete fNPlusVsTheta;
   if(fNPlusVsE) delete fNPlusVsE;
   //---------------------
   if(fNMinusVsPt) delete fNMinusVsPt;
   if(fNMinusVsPhi) delete fNMinusVsPhi;
   if(fNMinusVsTheta) delete fNMinusVsTheta;
   if(fNMinusVsE) delete fNMinusVsE;
   //---------------------
   if(fAsymmetryVsPt) delete fAsymmetryVsPt;
   if(fAsymmetryVsPhi) delete fAsymmetryVsPhi;
   if(fAsymmetryVsTheta) delete fAsymmetryVsTheta;
   if(fAsymmetryVsE) delete fAsymmetryVsE;
   //---------------------
   if(fDilutionVsPt) delete fDilutionVsPt;
   if(fDilutionVsPhi) delete fDilutionVsPhi;
   if(fDilutionVsTheta) delete fDilutionVsTheta;
   if(fDilutionVsE) delete fDilutionVsE;
   //---------------------
   if(fSystErrVsPt) delete fSystErrVsPt;
   if(fSystErrVsPhi) delete fSystErrVsPhi;
   if(fSystErrVsTheta) delete fSystErrVsTheta;
   if(fSystErrVsE) delete fSystErrVsE;
   //---------------------
   if(fAvgKineVsPt) delete fAvgKineVsPt;
   if(fAvgKineVsE) delete fAvgKineVsE;
   if(fAvgKineVsTheta) delete fAvgKineVsTheta;
   if(fAvgKineVsPhi) delete fAvgKineVsPhi;
}
//_____________________________________________________________________________

void InSANEPi0MeasuredAsymmetry::Initialize() {

   TH1::AddDirectory(kFALSE);

   Int_t n = fNumber; 

   //---------------------
   if(fNPlusVsPt) delete fNPlusVsPt;
   fNPlusVsPt         = new TH1F(Form("fNPlusVsPt-%d", n),        Form("N_plus Vs #Pt %d;#Pt", n), 
                        20, fPtMin, fPtMax);
   if(fNPlusVsPhi) delete fNPlusVsPhi;
   fNPlusVsPhi        = new TH1F(Form("fNPlusVsPhi-%d", n),       Form("N_plus Vs #phi %d;#phi", n), 
                        20, fPhiMin, fPhiMax);
   if(fNPlusVsTheta) delete fNPlusVsTheta;
   fNPlusVsTheta      = new TH1F(Form("fNPlusVsTheta-%d", n),     Form("N_plus Vs #theta %d;#theta", n), 
                        20, fThetaMin,fThetaMax);
   if(fNPlusVsE) delete fNPlusVsE;
   fNPlusVsE          = new TH1F(Form("fNPlusVsE-%d", n),         Form("N_plus Vs E %d:E", n), 
                        20, fEnergyMin ,fEnergyMax);
   //---------------------
   if(fNMinusVsPt) delete fNMinusVsPt;
   fNMinusVsPt          = new TH1F(Form("fNMinusVsPt-%d", n),         Form("N_minus Vs Pt %d;Pt", n), 
                        20, fPtMin,fPtMax);
   if(fNMinusVsPhi) delete fNMinusVsPhi;
   fNMinusVsPhi        = new TH1F(Form("fNMinusVsPhi-%d", n),       Form("N_minus Vs #phi %d;#phi", n), 
                        20, fPhiMin, fPhiMax);
   if(fNMinusVsTheta) delete fNMinusVsTheta;
   fNMinusVsTheta      = new TH1F(Form("fNMinusVsTheta-%d", n),     Form("N_minus Vs #theta %d;#theta", n), 
                        20, fThetaMin,fThetaMax);
   if(fNMinusVsE) delete fNMinusVsE;
   fNMinusVsE          = new TH1F(Form("fNMinusVsE-%d", n),         Form("N_minus Vs E %d:E", n), 
                        20, fEnergyMin ,fEnergyMax);
   //---------------------
   if(fAsymmetryVsPt) delete fAsymmetryVsPt;
   fAsymmetryVsPt          = new TH1F(Form("fAsymmetryVsPt-%d", n),         Form("Asymmetry Vs Pt %d;Pt", n), 
                        20, fPtMin,fPtMax);
   if(fAsymmetryVsPhi) delete fAsymmetryVsPhi;
   fAsymmetryVsPhi        = new TH1F(Form("fAsymmetryVsPhi-%d", n),       Form("Asymmetry Vs #phi %d;#phi", n), 
                        20, fPhiMin, fPhiMax);
   if(fAsymmetryVsTheta) delete fAsymmetryVsTheta;
   fAsymmetryVsTheta      = new TH1F(Form("fAsymmetryVsTheta-%d", n),     Form("Asymmetry Vs #theta %d;#theta", n), 
                        20, fThetaMin,fThetaMax);
   if(fAsymmetryVsE) delete fAsymmetryVsE;
   fAsymmetryVsE          = new TH1F(Form("fAsymmetryVsE-%d", n),         Form("Asymmetry Vs E %d:E", n), 
                        20, fEnergyMin ,fEnergyMax);
   //---------------------
   if(fDilutionVsPt) delete fDilutionVsPt;
   fDilutionVsPt          = new TH1F(Form("fDilutionVsPt-%d", n),         Form("Dilution Vs Pt %d;Pt", n), 
                        20, fPtMin,fPtMax);
   if(fDilutionVsPhi) delete fDilutionVsPhi;
   fDilutionVsPhi        = new TH1F(Form("fDilutionVsPhi-%d", n),       Form("Dilution Vs #phi %d;#phi", n), 
                        20, fPhiMin, fPhiMax);
   if(fDilutionVsTheta) delete fDilutionVsTheta;
   fDilutionVsTheta      = new TH1F(Form("fDilutionVsTheta-%d", n),     Form("Dilution Vs #theta %d;#theta", n), 
                        20, fThetaMin,fThetaMax);
   if(fDilutionVsE) delete fDilutionVsE;
   fDilutionVsE          = new TH1F(Form("fDilutionVsE-%d", n),         Form("Dilution Vs E %d:E", n), 
                        20, fEnergyMin ,fEnergyMax);
   //---------------------
   if(fSystErrVsPt) delete fSystErrVsPt;
   fSystErrVsPt          = new TH1F(Form("fSystErrVsPt-%d", n),         Form("SystErr Vs Pt %d;Pt", n), 
                        20, fPtMin,fPtMax);
   if(fSystErrVsPhi) delete fSystErrVsPhi;
   fSystErrVsPhi        = new TH1F(Form("fSystErrVsPhi-%d", n),       Form("SystErr Vs #phi %d;#phi", n), 
                        20, fPhiMin, fPhiMax);
   if(fSystErrVsTheta) delete fSystErrVsTheta;
   fSystErrVsTheta      = new TH1F(Form("fSystErrVsTheta-%d", n),     Form("SystErr Vs #theta %d;#theta", n), 
                        20, fThetaMin,fThetaMax);
   if(fSystErrVsE) delete fSystErrVsE;
   fSystErrVsE          = new TH1F(Form("fSystErrVsE-%d", n),         Form("SystErr Vs E %d:E", n), 
                        20, fEnergyMin ,fEnergyMax);
   //---------------------
   if(fAvgKineVsPt) delete fAvgKineVsPt;
   fAvgKineVsPt = new InSANEAveragedPi0Kinematics1D(Form("avgkine-%d-VsPt",n),Form("avg-kine-%d Vs. Pt",n));
   fAvgKineVsPt->CreateHistograms(fAsymmetryVsPt); 

   if(fAvgKineVsE) delete fAvgKineVsE;
   fAvgKineVsE = new InSANEAveragedPi0Kinematics1D(Form("avgkine-%d-VsE",n),Form("avg-kine-%d Vs. E",n));
   fAvgKineVsE->CreateHistograms(fAsymmetryVsE); 

   if(fAvgKineVsTheta) delete fAvgKineVsTheta;
   fAvgKineVsTheta = new InSANEAveragedPi0Kinematics1D(Form("avgkine-%d-VsTheta",n),Form("avg-kine-%d Vs. Theta",n));
   fAvgKineVsTheta->CreateHistograms(fAsymmetryVsTheta); 

   if(fAvgKineVsPhi) delete fAvgKineVsPhi;
   fAvgKineVsPhi = new InSANEAveragedPi0Kinematics1D(Form("avgkine-%d-VsPhi",n),Form("avg-kine-%d Vs. phi",n));
   fAvgKineVsPhi->CreateHistograms(fAsymmetryVsPhi); 

   //---------------------
   TH1::AddDirectory(kTRUE);
}
//_____________________________________________________________________________

InSANEPi0MeasuredAsymmetry::InSANEPi0MeasuredAsymmetry(const InSANEPi0MeasuredAsymmetry& rhs): InSANEAsymmetry(rhs) {
      (*this) = rhs;
}
//_____________________________________________________________________________
void InSANEPi0MeasuredAsymmetry::SetRunSummary(InSANERunSummary* rs) {
   if(rs)  {
      fRunSummary = *rs; // this does everything we want why do we have the rest?
      fHalfWavePlate = rs->fHalfWavePlate;
      if(fHalfWavePlate) fChargeSwap = true;
      else fChargeSwap = false;
      if(rs->fRuns.size()>0)fRunNumber = rs->fRuns.at(0);
      if(fChargeSwap) {
         fCharge[0] = rs->fQPlus;
         fCharge[1] = rs->fQMinus;
         fLiveTime[0] = rs->fLPlus;
         fLiveTime[1] = rs->fLMinus;
      }else{
         fCharge[0] = rs->fQPlus;
         fCharge[1] = rs->fQMinus;
         fLiveTime[0] = rs->fLPlus;
         fLiveTime[1] = rs->fLMinus;
      }
      if(rs->fRuns.size()>0)fRunNumber = rs->fRuns.at(0);
      fPb = rs->fPBeam*rs->fPbSign/100.0;
      fPt = rs->fPTarget*rs->fPtSign/100.0;
   }
}
//______________________________________________________________________________
Bool_t InSANEPi0MeasuredAsymmetry::IsGoodEvent() {

   //if (!(fQ2Min < fDISEvent->fQ2 && fQ2Max > fDISEvent->fQ2)) return(false);
   /*      if( !(fEnergyMin < *fScatteredElectronEnergy && fEnergyMax > *fScatteredElectronEnergy) ) return(false);
         if( !(fThetaMin < *fScatteredElectronTheta && fThetaMax> *fScatteredElectronTheta )) return(false);
         if( !(fPhiMin < *fScatteredElectronPhi && fPhiMax > *fScatteredElectronPhi )) return(false);
         if( !(fWMin < *fW && fWMax > *fW )) return(false);*/
   return(true);
}
//_____________________________________________________________________________
TH1F * InSANEPi0MeasuredAsymmetry::CreateAsymmetryHistogram1D(const TH1F * h0, const TH1F * h1, TH1F * hRes, const TH1F * dfHist, TH1F * systHist)
{
   TH1::AddDirectory(kFALSE);

   if (!h0) {
      Error("CreateAsymmetryHistogram", "Histogram h0 is NULL!");
      return(nullptr);
   }
   if (!h1) {
      Error("CreateAsymmetryHistogram", "Histogram h1 is NULL!");
      return(nullptr);
   }
   if(!hRes) { 
      Warning("CreateAsymmetryHistogram","hRes is Null!");
   }
   if(!hRes) hRes = new TH1F(*h0);

   Int_t nXbins = h0->GetNbinsX();
   Int_t nYbins = h0->GetNbinsY();
   Int_t nZbins = h0->GetNbinsZ();
   Double_t n0   = 0.0;
   Double_t n1   = 0.0;
   Double_t A    = 0.0;
   Double_t eA   = 0.0;
   Double_t eAsys= 0.0;
   Double_t df   = 1.0;//fDilution;

   for(int i = 0; i<=nXbins; i++ ) {
      for(int j = 0; j<=nYbins; j++ ) {
         for(int k = 0; k<=nZbins; k++ ) {
            n0 = h0->GetBinContent(i,j,k); 
            n1 = h1->GetBinContent(i,j,k); 
            if(dfHist){
               df  = dfHist->GetBinContent(i,j,k);
            }
            A  = GetAsymmetry(n0,n1,df);
            if( TMath::IsNaN(A) ) A = 0.0;
            hRes->SetBinContent(i,j,k,A);
            if (n0 > 0.0 && n1 > 0.0) {
               eA    = GetStatisticalError(n0, n1,df);
               if( std::isinf(eA) ) eA = 0.0;
               hRes->SetBinError(i,j,k,eA);
               eAsys = GetSystematicError(n0,n1,df);
               if(systHist)systHist->SetBinContent(i,j,k,eAsys);
            } else {
               eA = 0.0;
               hRes->SetBinError(i,j,k,eA);
               if(systHist)systHist->SetBinContent(i,j,k,eA);
            }
            std::cout << "n0=" << n0 << " , n1=" << n1 << " ,A=" << A << ", eA=" << eA << "\n";
         }
      }
   }
   //TH1F * numerator   = new TH1F(*(h0));
   //TH1F * denominator = new TH1F(*(h1));

   //numerator->Add(h0, h1, 1.0 / (Qplus * Lplus), -1.0 / (Qminus * Lminus));
   //denominator->Add(h0, h1, 1.0 / (Qplus * Lplus), 1.0 / (Qminus * Lminus));

   //TH1F * asym = new TH1F(*numerator);
   //if(!hRes)asym->SetName(Form("%s-Asymmetry-%s", GetName(), numerator->GetName()));
   //else asym->SetName(Form("Asymmetry-%s", hRes->GetName()));
   //asym->Divide(denominator);

   //if (!dfHist) asym->Scale(1.0 / dfPbPt);
   //else {
   //   asym->Scale(1.0 / (fPb * fPt));
   //   asym->Divide(dfHist);
   //}

   //Double_t n0, n1;
   //Double_t eA;
   //Double_t df = fDilution;
   //Int_t    xmax = asym->GetNbinsX();
   //for (Int_t i = 0; i <= xmax; i++) {
   //   n0 = h0->GetBinContent(i);
   //   n1 = h1->GetBinContent(i);
   //   if(dfHist){
   //      df  = dfHist->GetBinContent(i);
   //   }
   //   if (n0 + n1 > 0.0) {
   //      eA = GetTotalError(n0, n1,df);
   //      asym->SetBinError(i, eA);
   //   } else {
   //      eA = 0.0;
   //      asym->SetBinError(i, 0.0);
   //   }
   //   //std::cout << "n0=" << n0 << " , n1=" << n1 << " , eA=" << eA << "\n";
   //}

   hRes->SetMarkerSize(1.0);
   hRes->SetMarkerStyle(20);
   
   //TString n = asym->GetName();
   //TString t = asym->GetTitle();
   //if(hRes){
   //   std::cout << "hRes exisits" << std::endl;
   //   TString n = hRes->GetName();
   //   TString t = hRes->GetTitle();
   //   delete hRes;
   //   hRes=0;
   //}
   //asym->SetNameTitle(n.Data(),t.Data()); 
   //hRes = new TH1F(*asym);
   //hRes = asym;
   //hRes->SetNameTitle(n.Data(),t.Data()); 

   //delete numerator;
   //delete denominator;
   //delete asym;
   //TH1::AddDirectory(kTRUE);

   return(hRes);
}
//_____________________________________________________________________________
Int_t InSANEPi0MeasuredAsymmetry::CalculateDilutionFactors1D(TH1F * dfHist, const InSANEAveragedPi0Kinematics1D *kine){

   Int_t nXbins = dfHist->GetNbinsX();
   Int_t nYbins = dfHist->GetNbinsY();
   Int_t nZbins = dfHist->GetNbinsZ();
   Int_t bin = 0;
   Double_t n0   = 0.0;
   Double_t n1   = 0.0;
   Double_t dilution = fDilution;
   Double_t dilution2 = fDilution;
   Double_t e_df = 0.001;
   Double_t eA   = 0.0;

   for(int i = 0; i<=nXbins; i++ ) {
      for(int j = 0; j<=nYbins; j++ ) {
         for(int k = 0; k<=nZbins; k++ ) {
            bin = dfHist->GetBin(i,j,k);
            //Double_t x  = kine->fx->GetBinContent(bin);
            //Double_t Q2 = kine->fQ2->GetBinContent(bin);
            //Double_t W  = kine->fW->GetBinContent(bin);
            //if(fDilutionFromTarget){
            //   dilution = fDilutionFromTarget->GetDilution(x,Q2); 
            //   //dilution2 = fDilutionFromTarget->GetDilution_WQ2(W,Q2); 
            //   e_df     = dilution*0.05;//TMath::Abs(dilution-dilution2);
            //   std::cout << " df_xQ2(" << x << "," << Q2 << ") = " << dilution << std::endl;
            //} else {
            //   dilution = fDilution;
            //}
            //std::cout << " df_WQ2(" << W << "," << Q2 << ") = " << dilution2 << std::endl;
            dfHist->SetBinContent(bin,dilution);
            dfHist->SetBinError(bin,e_df);

         }
      }
   }

   return(0);
}
//_____________________________________________________________________________
Int_t InSANEPi0MeasuredAsymmetry::CalculateAsymmetries() {

   // ----------------------------------------
   //df        = 0.17;
   fPb       = fRunSummary.fPbSign*fRunSummary.fPBeam/100.0;//run->fBeamPolarization / 100.0;
   fPt       = fRunSummary.fPtSign*fRunSummary.fPTarget/100.0;//run->fTargetOfflinePolarization / 100.0;
   dfPbPt    = fDilution * fPb * fPt ;
   Qplus     = fRunSummary.fQPlus;//run->fTotalQPlus;
   Qminus    = fRunSummary.fQMinus;//run->fTotalQMinus;
   Lplus     = fRunSummary.fLPlus;//run->fLiveTimePlus;
   Lminus    = fRunSummary.fLMinus;//run->fLiveTimeMinus;

   // ----------------------------------------
   // Average the kinematic variables and coefficients
   //if(fAvgKineVsQ2) fAvgKineVsQ2->Calculate();
   if(fAvgKineVsPt) fAvgKineVsPt->Calculate();
   if(fAvgKineVsE) fAvgKineVsE->Calculate();
   if(fAvgKineVsTheta) fAvgKineVsTheta->Calculate();
   if(fAvgKineVsPhi) fAvgKineVsPhi->Calculate();

   // ----------------------------------------
   // Calculate the dilutions 
   CalculateDilutionFactors1D(fDilutionVsPt,     fAvgKineVsPt);
   CalculateDilutionFactors1D(fDilutionVsE,     fAvgKineVsE);
   CalculateDilutionFactors1D(fDilutionVsTheta, fAvgKineVsTheta);
   CalculateDilutionFactors1D(fDilutionVsPhi,   fAvgKineVsPhi);

   //std::cout << "Q(+,-)=(" << fCharge[0] << "," << fCharge[1]<< "), Pb=" << fPb << ", Pt=" << fPt << ", df=" << fDilutionVsx->GetMean() << ", \n";

   /// 1-D Asymmetries
   fAsymmetryVsPt    = CreateAsymmetryHistogram1D(fNPlusVsPt    , fNMinusVsPt    , fAsymmetryVsPt    , fDilutionVsPt    , fSystErrVsPt     ) ;
   fAsymmetryVsTheta = CreateAsymmetryHistogram1D(fNPlusVsTheta, fNMinusVsTheta, fAsymmetryVsTheta, fDilutionVsTheta, fSystErrVsTheta ) ;
   fAsymmetryVsPhi   = CreateAsymmetryHistogram1D(fNPlusVsPhi  , fNMinusVsPhi  , fAsymmetryVsPhi  , fDilutionVsPhi  , fSystErrVsPhi   ) ;
   fAsymmetryVsE     = CreateAsymmetryHistogram1D(fNPlusVsE    , fNMinusVsE    , fAsymmetryVsE    , fDilutionVsE    , fSystErrVsE     ) ;



   /// Set colors
   fAsymmetryVsE->SetMarkerColor(2000 + fNumber % 4);
   fAsymmetryVsE->SetLineColor(2000 + fNumber % 4);
   fAsymmetryVsPt->SetMarkerColor(2000 + fNumber % 4);
   fAsymmetryVsPt->SetLineColor(2000 + fNumber % 4);
   fAsymmetryVsTheta->SetMarkerColor(2000 + fNumber % 4);
   fAsymmetryVsTheta->SetLineColor(2000 + fNumber % 4);
   fAsymmetryVsPhi->SetMarkerColor(2000 + fNumber % 4);
   fAsymmetryVsPhi->SetLineColor(2000 + fNumber % 4);

   return(0);
}
//_____________________________________________________________________________
Int_t InSANEPi0MeasuredAsymmetry::CountEvent() {
   if (!fPi0ClusterPair) {
      Warning("CountEvent", "fPi0ClusterPair is NULL");
      return(-1);
   }
   if (IsGoodEvent()) { 

      double energy  = fPi0ClusterPair->fEnergy;//TMath::Sqrt((fPi0ClusterPair->fMomentum)*(fPi0ClusterPair->fMomentum) + 135.0*135.0);
      double pt      = (fPi0ClusterPair->fMomentum)*TMath::Sin(fPi0ClusterPair->fTheta);

      std::cout << " pt = " << pt << std::endl;

      // Averaged kinematics
      fAvgKineVsPt->AddEvent(     pt                      , fPi0ClusterPair);
      fAvgKineVsE->AddEvent(      energy                  , fPi0ClusterPair);
      fAvgKineVsTheta->AddEvent(  fPi0ClusterPair->fTheta , fPi0ClusterPair);
      fAvgKineVsPhi->AddEvent(    fPi0ClusterPair->fPhi   , fPi0ClusterPair);

      if ((fPi0ClusterPair->fHelicity) == 1) {
         fCount[0]++;
         fNPlusVsPt->Fill(     pt );
         fNPlusVsE->Fill(      energy);
         fNPlusVsPhi->Fill(    fPi0ClusterPair->fPhi);
         fNPlusVsTheta->Fill(  fPi0ClusterPair->fTheta);
      }
      if ((fPi0ClusterPair->fHelicity) == -1) {
         fCount[1]++;
         fNMinusVsPt->Fill( pt);
         fNMinusVsE->Fill( energy);
         fNMinusVsPhi->Fill(fPi0ClusterPair->fPhi);
         fNMinusVsTheta->Fill(fPi0ClusterPair->fTheta);
      }
      return(1);
   }
   /* else */
   return(0);
}
//_____________________________________________________________________________
Int_t InSANEPi0MeasuredAsymmetry::CountEvent(std::vector<int>& groups) {
   if (!fDISEvent) {
      Warning("CountEvent", "No fDISEvent is null\n");
      return(-1);
   }
   //if (IsGoodEvent()) if( IsInGroupList(groups) )  {

   //   // Averaged kinematics
   //   fAvgKineVsx->AddEvent(fDISEvent->fx,fDISEvent);
   //   fAvgKineVsW->AddEvent(fDISEvent->fW,fDISEvent);
   //   fAvgKineVsNu->AddEvent(fDISEvent->fBeamEnergy-fDISEvent->fEnergy,fDISEvent);
   //   fAvgKineVsE->AddEvent(fDISEvent->fEnergy,fDISEvent);
   //   fAvgKineVsTheta->AddEvent(fDISEvent->fTheta,fDISEvent);
   //   fAvgKineVsPhi->AddEvent(fDISEvent->fPhi,fDISEvent);

   //   fAvgKineVsxPhi->AddEvent(fDISEvent->fx, fDISEvent->fPhi, fDISEvent);
   //   fAvgKineVsWPhi->AddEvent(fDISEvent->fW, fDISEvent->fPhi, fDISEvent);
   //   fAvgKineVsxQ2->AddEvent( fDISEvent->fx, fDISEvent->fQ2,  fDISEvent);
   //   //fAvgKineVsEPhi->AddEvent(fDISEvent->fEnergy,fDISEvent);
   //   //fAvgKineVsNuPhi->AddEvent(fDISEvent->fBeamEnergy-fDISEvent->fEnergy,fDISEvent);
   //   //fAvgKineVsThetaPhi->AddEvent(fDISEvent->fTheta,fDISEvent);

   //   if ((fDISEvent->fHelicity) == 1) {
   //      fCount[0]++;
   //      fNPlusVsx->Fill((fDISEvent->fx));
   //      fNPlusVsW->Fill((fDISEvent->fW));
   //      fNPlusVsNu->Fill(fDISEvent->fBeamEnergy-fDISEvent->fEnergy);
   //      fNPlusVsE->Fill((fDISEvent->fEnergy));
   //      fNPlusVsPhi->Fill(fDISEvent->fPhi);
   //      fNPlusVsTheta->Fill(fDISEvent->fTheta);
   //      fNPlusVsxPhi->Fill( fDISEvent->fx, fDISEvent->fPhi);
   //      fNPlusVsWPhi->Fill( fDISEvent->fW, fDISEvent->fPhi);
   //      fNPlusVsxQ2->Fill( fDISEvent->fx, fDISEvent->fQ2);
   //      //fNPlusVsEThetaPhi->Fill(fDISEvent->fEnergy, fDISEvent->fTheta, fDISEvent->fPhi);
   //      //fNPlusVsWxPhi->Fill(fDISEvent->fW, fDISEvent->fx, fDISEvent->fPhi);
   //      //fNPlusVsWNuPhi->Fill(fDISEvent->fW, (fDISEvent->fBeamEnergy-fDISEvent->fEnergy), fDISEvent->fPhi);
   //   }
   //   if ((fDISEvent->fHelicity) == -1) {
   //      fCount[1]++;
   //      fNMinusVsx->Fill((fDISEvent->fx));
   //      fNMinusVsW->Fill((fDISEvent->fW));
   //      fNMinusVsNu->Fill((fDISEvent->fBeamEnergy-fDISEvent->fEnergy));
   //      fNMinusVsE->Fill(fDISEvent->fEnergy);
   //      fNMinusVsPhi->Fill(fDISEvent->fPhi);
   //      fNMinusVsTheta->Fill(fDISEvent->fTheta);
   //      fNMinusVsxPhi->Fill( fDISEvent->fx, fDISEvent->fPhi);
   //      fNMinusVsWPhi->Fill( fDISEvent->fW, fDISEvent->fPhi);
   //      fNMinusVsxQ2->Fill( fDISEvent->fx, fDISEvent->fQ2);
   //      //fNMinusVsEThetaPhi->Fill(fDISEvent->fEnergy, fDISEvent->fTheta, fDISEvent->fPhi);
   //      //fNMinusVsWxPhi->Fill(fDISEvent->fW, fDISEvent->fx, fDISEvent->fPhi);
   //      //fNMinusVsWNuPhi->Fill(fDISEvent->fW, (fDISEvent->fBeamEnergy-fDISEvent->fEnergy), fDISEvent->fPhi);
   //   }
   //   return(1);
   //}
   ///* else */
   return(0);
}
//_____________________________________________________________________________
void InSANEPi0MeasuredAsymmetry::PrintPhiBinnedTable(std::ostream& stream) {
   std::cout << " InSANEPi0MeasuredAsymmetry::PrintPhiBinnedTable  Not implemented" << std::endl;
   //if(fAsymmetryVsx)
      //if(fAsymmetryVsWxPhi) {

      //   // Get the number of bins for (1D) x and (3D) W-x-Phi histograms
      //   Int_t nBins     = fAsymmetryVsx->GetNbinsX();
      //   Int_t nWBins    = fAsymmetryVsWxPhi->GetNbinsX(); 
      //   Int_t nxBins    = fAsymmetryVsWxPhi->GetNbinsY(); 
      //   Int_t nPhiBins  = fAsymmetryVsWxPhi->GetNbinsZ(); 
      //   // Rebin into a new temporary histogram the phi asymmetry and counts
      //   // Make only one bin in phi
      //   TH3 *hnew = fAsymmetryVsWxPhi->RebinZ(nPhiBins/2,"hnew"); 
      //   nPhiBins  = hnew->GetNbinsZ(); 

      //   for(int i = 1;i<= nxBins;i++){
      //      for(int j = 1;j<= nWBins;j++){

      //         Double_t W   = hnew->GetXaxis()->GetBinCenter(j);
      //         Double_t x   = hnew->GetYaxis()->GetBinCenter(i);
      //         Double_t A   = hnew->GetBinContent(j,i,nPhiBins-1);
      //         Double_t dA  = hnew->GetBinError(j,i,nPhiBins-1);

      //         if(A != 0 ) stream  
      //            << std::setw(10) << fRunNumber   << " "
      //               << std::setw(10) << fCount[0]    << " "
      //               << std::setw(10) << fCount[1]    << " "
      //               << std::setw(10) << fLiveTime[0] << " "
      //               << std::setw(10) << fLiveTime[1] << " "
      //               << std::setw(10) << fCharge[0]   << " "
      //               << std::setw(10) << fCharge[1]   << " "
      //               << std::setw(10) << fDilution    << " "
      //               << std::setw(10) << fPt          << " "
      //               << std::setw(10) << fPb          << " "
      //               << std::setw(10) << W            << " "
      //               << std::setw(10) << x            << " "
      //               << std::setw(10) << A            << " "
      //               << std::setw(10) << dA           << " "
      //               << std::endl;
      //      }
      //   }
      //   if(hnew) delete hnew;
      //}
}
//_____________________________________________________________________________

void InSANEPi0MeasuredAsymmetry::PrintBinnedTable(std::ostream& stream) {
   //if(fAsymmetryVsx)
   //{
   //   // Get the number of bins for (1D) x and (3D) W-x-Phi histograms
   //   Int_t nBins     = fAsymmetryVsx->GetNbinsX();

   //   for(int i = 1;i<= nBins;i++){

   //      Double_t x   = fAvgKineVsx->fx->GetBinContent(i);
   //      Double_t Q2  = fAvgKineVsx->fQ2->GetBinContent(i);
   //      Double_t W   = fAvgKineVsx->fW->GetBinContent(i);
   //      Double_t E   = fAvgKineVsx->fE->GetBinContent(i);
   //      Double_t th  = fAvgKineVsx->fTheta->GetBinContent(i);
   //      Double_t phi = fAvgKineVsx->fPhi->GetBinContent(i);
   //      Double_t df  = fDilutionVsx->GetBinContent(i);
   //      Double_t n0  = fNPlusVsx->GetBinContent(i);
   //      Double_t n1  = fNMinusVsx->GetBinContent(i);
   //      Double_t A   = fAsymmetryVsx->GetBinContent(i);
   //      Double_t dA  = fAsymmetryVsx->GetBinError(i);
   //      Double_t dAsys  = fSystErrVsx->GetBinContent(i);
   //      if(A != 0.0 ) 
   //         stream  
   //            << std::setw(10) << fRunNumber   << " "
   //            << std::setw(10) << n0    << " "
   //            << std::setw(10) << n1    << " "
   //            << std::setw(10) << fLiveTime[0] << " "
   //            << std::setw(10) << fLiveTime[1] << " "
   //            << std::setw(10) << fCharge[0]   << " "
   //            << std::setw(10) << fCharge[1]   << " "
   //            << std::setw(10) << df           << " "
   //            << std::setw(10) << fPt          << " "
   //            << std::setw(10) << fPb          << " "
   //            << std::setw(10) << A            << " "
   //            << std::setw(10) << dA           << " "
   //            << std::setw(10) << dAsys        << " "
   //            << std::setw(10) << x            << " "
   //            << std::setw(10) << Q2           << " "
   //            << std::setw(10) << W            << " "
   //            << std::setw(10) << E            << " "
   //            << std::setw(10) << th           << " "
   //            << std::setw(10) << phi          << " "
   //            << std::endl;
   //   }
   //}
}
//_____________________________________________________________________________
void InSANEPi0MeasuredAsymmetry::PrintBinnedTableHead(std::ostream& file) {
   file 
      << std::setw(10) << "RunNumber" << " "
      << std::setw(10) << "n0   " << " "
      << std::setw(10) << "n1   " << " "
      << std::setw(10) << "LT[0]" << " "
      << std::setw(10) << "LT[1]" << " "
      << std::setw(10) << "Q[0] "  << " "
      << std::setw(10) << "Q[1] "  << " "
      << std::setw(10) << "df   " << " "
      << std::setw(10) << "Pt   "       << " "
      << std::setw(10) << "Pb   "       << " "
      << std::setw(10) << "A    "        << " "
      << std::setw(10) << "dA(stat)"        << " "
      << std::setw(10) << "dA(syst)"        << " "
      << std::setw(10) << "x    "        << " "
      << std::setw(10) << "Q2   "        << " "
      << std::setw(10) << "W    "        << " "
      << std::setw(10) << "E    "        << " "
      << std::setw(10) << "th   "        << " "
      << std::setw(10) << "phi  "        << " "
      << std::endl;
}
//_____________________________________________________________________________
void InSANEPi0MeasuredAsymmetry::PrintTableHead(std::ostream& file) {
   file << std::setw(5)  << "Group" << " "
      << std::setw(10) << "RunNumber" << " "
      << std::setw(10) << "Q2_max" << " "
      << std::setw(10) << "Q2_min"  << " "
      << std::setw(10) << "Q2_avg"  << " "
      << std::setw(10) << "df"   << " "
      << std::setw(10) << "Pb"   << " "
      << std::setw(10) << "Pt"   << " "
      << std::setw(10) << "Qplus"   << " "
      << std::setw(10) << "Qminus"   << " "
      << std::setw(10) << "LTplus"   << " "
      << std::setw(10) << "LTminus"   << " "
      << std::setw(10) << "Nplus"   << " "
      << std::setw(10) << "Nminus"   << " "
      << std::setw(10) << "A" << " "
      << std::setw(10) << "A_error" << "\n";
}
//_____________________________________________________________________________
void InSANEPi0MeasuredAsymmetry::PrintTable(std::ostream& file) {
   file << std::showpoint;
   file << std::setw(5) << fGroup << " "
      << std::setw(10) << fRunNumber << " "
      << std::setw(10)
      << fDilution << " "
      << std::setw(10)
      << fPb << " "
      << std::setw(10)
      << fPt << " "
      << std::setw(10)
      << Qplus << " "
      << std::setw(10)
      << Qminus << " "
      << std::setw(10)
      << Lplus << " "
      << std::setw(10)
      << Lminus << " "
      << std::setw(10)
      << fCount[0] << " "
      << std::setw(10)
      << fCount[1] << " "
      << std::setw(10)
      << GetAsymmetry(fCount[0], fCount[1],fDilution) << " "
      << std::setw(10)
      << GetTotalError(fCount[0], fCount[1],fDilution) << "\n";

}
//_____________________________________________________________________________
void InSANEPi0MeasuredAsymmetry::Print() {
   PrintTableHeader(std::cout);
   PrintTable(std::cout);
}
//_____________________________________________________________________________
InSANEPi0MeasuredAsymmetry& InSANEPi0MeasuredAsymmetry::operator=(const InSANEPi0MeasuredAsymmetry &rhs)
{
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   TH1::AddDirectory(kFALSE);

   InSANEAsymmetry::operator=(rhs);

   fCharge[0]         = rhs.fCharge[0];
   fCharge[1]         = rhs.fCharge[1];
   fLiveTime[0]         = rhs.fLiveTime[0];
   fLiveTime[1]         = rhs.fLiveTime[1];
   fPb                = rhs.fPb;
   fPt                = rhs.fPt;
   fDilution          = rhs.fDilution;
   fEnergyMin         = rhs.fEnergyMin;
   fEnergyMax         = rhs.fEnergyMax;
   fPtMin              = rhs.fPtMin;
   fPtMax              = rhs.fPtMax;
   fThetaMin          = rhs.fThetaMin;
   fThetaMax          = rhs.fThetaMax;
   fBinningBeamEnergy = rhs.fBinningBeamEnergy;
   fPhiMin            = rhs.fPhiMin;
   fPhiMax            = rhs.fPhiMax;
   fHalfWavePlate     = rhs.fHalfWavePlate;
   fChargeSwap        = rhs.fChargeSwap;
   dfPbPt             = rhs.dfPbPt;
   Qplus              = rhs.Qplus;
   Qminus             = rhs.Qminus;
   Lplus              = rhs.Lplus;
   Lminus             = rhs.Lminus;
   fRunSummary        = rhs.fRunSummary;

   if (rhs.fAvgKineVsPt) fAvgKineVsPt = new InSANEAveragedPi0Kinematics1D(*(rhs.fAvgKineVsPt));
   if (rhs.fAvgKineVsE) fAvgKineVsE = new InSANEAveragedPi0Kinematics1D(*(rhs.fAvgKineVsE));
   if (rhs.fAvgKineVsTheta) fAvgKineVsTheta = new InSANEAveragedPi0Kinematics1D(*(rhs.fAvgKineVsTheta));
   if (rhs.fAvgKineVsPhi) fAvgKineVsPhi = new InSANEAveragedPi0Kinematics1D(*(rhs.fAvgKineVsPhi));
   //------------
   if (rhs.fAsymmetryVsPt) fAsymmetryVsPt = (TH1F*)rhs.fAsymmetryVsPt->Clone();
   if (rhs.fAsymmetryVsE) fAsymmetryVsE = (TH1F*)rhs.fAsymmetryVsE->Clone();
   if (rhs.fAsymmetryVsTheta) fAsymmetryVsTheta = (TH1F*)rhs.fAsymmetryVsTheta->Clone();
   if (rhs.fAsymmetryVsPhi) fAsymmetryVsPhi = (TH1F*)rhs.fAsymmetryVsPhi->Clone();
   //------------
   if (rhs.fDilutionVsPt) fDilutionVsPt = (TH1F*)rhs.fDilutionVsPt->Clone();
   if (rhs.fDilutionVsE) fDilutionVsE = (TH1F*)rhs.fDilutionVsE->Clone();
   if (rhs.fDilutionVsTheta) fDilutionVsTheta = (TH1F*)rhs.fDilutionVsTheta->Clone();
   if (rhs.fDilutionVsPhi) fDilutionVsPhi = (TH1F*)rhs.fDilutionVsPhi->Clone();
   //------------
   if (rhs.fSystErrVsPt) fSystErrVsPt = (TH1F*)rhs.fSystErrVsPt->Clone();
   if (rhs.fSystErrVsE) fSystErrVsE = (TH1F*)rhs.fSystErrVsE->Clone();
   if (rhs.fSystErrVsTheta) fSystErrVsTheta = (TH1F*)rhs.fSystErrVsTheta->Clone();
   if (rhs.fSystErrVsPhi) fSystErrVsPhi = (TH1F*)rhs.fSystErrVsPhi->Clone();
   //------------


   if (rhs.fNPlusVsPt)  fNPlusVsPt  = (TH1F*)rhs.fNPlusVsPt->Clone();
   if (rhs.fNMinusVsPt) fNMinusVsPt = (TH1F*)rhs.fNMinusVsPt->Clone();

   if (rhs.fNPlusVsE)  fNPlusVsE  = (TH1F*)rhs.fNPlusVsE->Clone();
   if (rhs.fNMinusVsE) fNMinusVsE = (TH1F*)rhs.fNMinusVsE->Clone();

   if (rhs.fNPlusVsPhi)  fNPlusVsPhi  = (TH1F*)rhs.fNPlusVsPhi->Clone();
   if (rhs.fNMinusVsPhi) fNMinusVsPhi = (TH1F*)rhs.fNMinusVsPhi->Clone();

   if (rhs.fNPlusVsTheta)  fNPlusVsTheta  = (TH1F*)rhs.fNPlusVsTheta->Clone();
   if (rhs.fNMinusVsTheta) fNMinusVsTheta = (TH1F*)rhs.fNMinusVsTheta->Clone();


   TH1::AddDirectory(kTRUE);

   return *this;
}
//_____________________________________________________________________________
InSANEPi0MeasuredAsymmetry & InSANEPi0MeasuredAsymmetry::operator+=(const InSANEPi0MeasuredAsymmetry &rhs)
{
   //fNumber    += rhs.fNumber;
   InSANEAsymmetry::operator+=(rhs);

   /// \deprecated {
   dfPbPt = (dfPbPt * (fNAdded + 1) + (rhs.dfPbPt * (rhs.fNAdded + 1))) / (fNAdded + rhs.fNAdded + 2);
   Qplus  = (Qplus * (fNAdded + 1) + (rhs.Qplus * (rhs.fNAdded + 1))) / (fNAdded + rhs.fNAdded + 2);
   Qminus = (Qminus * (fNAdded + 1) + (rhs.Qminus * (rhs.fNAdded + 1))) / (fNAdded + rhs.fNAdded + 2);
   Lplus  = (Lplus * (fNAdded + 1) + (rhs.Lplus * (rhs.fNAdded + 1))) / (fNAdded + rhs.fNAdded + 2);
   Lminus = (Lminus * (fNAdded + 1) + (rhs.Lminus * (rhs.fNAdded + 1))) / (fNAdded + rhs.fNAdded + 2);
   ///}

   //df  = (df * (fNAdded + 1) + (rhs.df * (rhs.fNAdded + 1))) / (fNAdded + rhs.fNAdded + 2);
   //Pb  = (Pb * (fNAdded + 1) + (rhs.Pb * (rhs.fNAdded + 1))) / (fNAdded + rhs.fNAdded + 2);
   //Pt  = (Pt * (fNAdded + 1) + (rhs.Pt * (rhs.fNAdded + 1))) / (fNAdded + rhs.fNAdded + 2);

   fRunSummary += rhs.fRunSummary; // this should take care of most of what is above.

   //if (fAsymmetryVsx)     if (rhs.fAsymmetryVsx) fAsymmetryVsx->Add(rhs.fAsymmetryVsx);
   //if (fAsymmetryVsW)     if (rhs.fAsymmetryVsW) fAsymmetryVsW->Add(rhs.fAsymmetryVsW);
   //if (fAsymmetryVsNu)     if (rhs.fAsymmetryVsNu) fAsymmetryVsNu->Add(rhs.fAsymmetryVsNu);
   //if (fAsymmetryVsTheta) if (rhs.fAsymmetryVsTheta) fAsymmetryVsTheta->Add(rhs.fAsymmetryVsTheta);
   //if (fAsymmetryVsPhi)   if (rhs.fAsymmetryVsPhi) fAsymmetryVsPhi->Add(rhs.fAsymmetryVsPhi);
   //if (fAsymmetryVsEThetaPhi) if (rhs.fAsymmetryVsEThetaPhi) fAsymmetryVsEThetaPhi->Add(rhs.fAsymmetryVsEThetaPhi);
   //if (fAsymmetryVsxPhi) if (rhs.fAsymmetryVsxPhi) fAsymmetryVsxPhi->Add(rhs.fAsymmetryVsxPhi);
   //if (fAsymmetryVsWPhi) if (rhs.fAsymmetryVsWPhi) fAsymmetryVsWPhi->Add(rhs.fAsymmetryVsWPhi);
   //if (fAsymmetryVsxQ2) if (rhs.fAsymmetryVsxQ2) fAsymmetryVsxQ2->Add(rhs.fAsymmetryVsxQ2);
   //if (fAsymmetryVsWxPhi) if (rhs.fAsymmetryVsWxPhi) fAsymmetryVsWxPhi->Add(rhs.fAsymmetryVsWxPhi);
   //if (fAsymmetryVsWNuPhi) if (rhs.fAsymmetryVsWNuPhi) fAsymmetryVsWxPhi->Add(rhs.fAsymmetryVsWNuPhi);

   if (fNPlusVsE)  if (rhs.fNPlusVsE)  fNPlusVsE->Add(rhs.fNPlusVsE);
   if (fNMinusVsE) if (rhs.fNMinusVsE) fNMinusVsE->Add(rhs.fNMinusVsE);

   if (fNPlusVsPhi) if (rhs.fNPlusVsPhi) fNPlusVsPhi->Add(rhs.fNPlusVsPhi);
   if (fNMinusVsPhi) if (rhs.fNMinusVsPhi) fNMinusVsPhi->Add(rhs.fNMinusVsPhi);

   if (fNPlusVsTheta) if (rhs.fNPlusVsTheta) fNPlusVsTheta->Add(rhs.fNPlusVsTheta);
   if (fNMinusVsTheta) if (rhs.fNMinusVsTheta) fNMinusVsTheta->Add(rhs.fNMinusVsTheta);

   //if (fNMinusVsEThetaPhi) if (rhs.fNMinusVsEThetaPhi) fNMinusVsEThetaPhi->Add(rhs.fNMinusVsEThetaPhi);
   //if (fNPlusVsEThetaPhi) if (rhs.fNPlusVsEThetaPhi) fNPlusVsEThetaPhi->Add(rhs.fNPlusVsEThetaPhi);

   //if (fNMinusVsWxPhi) if (rhs.fNMinusVsWxPhi) fNMinusVsWxPhi->Add(rhs.fNMinusVsWxPhi);
   //if (fNPlusVsWxPhi) if (rhs.fNPlusVsWxPhi) fNPlusVsWxPhi->Add(rhs.fNPlusVsWxPhi);

   //if (fNMinusVsWNuPhi) if (rhs.fNMinusVsWNuPhi) fNMinusVsWNuPhi->Add(rhs.fNMinusVsWNuPhi);
   //if (fNPlusVsWNuPhi) if (rhs.fNPlusVsWNuPhi) fNPlusVsWNuPhi->Add(rhs.fNPlusVsWNuPhi);

   return *this;
}
//_____________________________________________________________________________
const InSANEPi0MeasuredAsymmetry InSANEPi0MeasuredAsymmetry::operator+(const InSANEPi0MeasuredAsymmetry &other)
{
   InSANEPi0MeasuredAsymmetry result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
   result += other;            // Use += to add other to the copy.
   return result;              // All done!
}
//_____________________________________________________________________________
void InSANEPi0MeasuredAsymmetry::SetDirectory(TDirectory * dir)
{
   if (fAsymmetryVsE)         fAsymmetryVsE->SetDirectory(dir);
   if (fAsymmetryVsPt)        fAsymmetryVsPt->SetDirectory(dir);
   if (fAsymmetryVsTheta)     fAsymmetryVsTheta->SetDirectory(dir);
   if (fAsymmetryVsPhi)       fAsymmetryVsPhi->SetDirectory(dir);

   if (fDilutionVsPt)        fDilutionVsPt->SetDirectory(dir);
   if (fDilutionVsTheta)     fDilutionVsTheta->SetDirectory(dir);
   if (fDilutionVsPhi)       fDilutionVsPhi->SetDirectory(dir);

   if (fSystErrVsPt)        fSystErrVsPt->SetDirectory(dir);
   if (fSystErrVsTheta)     fSystErrVsTheta->SetDirectory(dir);
   if (fSystErrVsPhi)       fSystErrVsPhi->SetDirectory(dir);
   if (fSystErrVsE)      fSystErrVsE->SetDirectory(dir);

   if (fNPlusVsE)  fNPlusVsE->SetDirectory(dir);
   if (fNMinusVsE) fNMinusVsE->SetDirectory(dir);

   if (fNPlusVsPt)  fNPlusVsPt->SetDirectory(dir);
   if (fNMinusVsPt) fNMinusVsPt->SetDirectory(dir);

   if (fNPlusVsPhi)  fNPlusVsPhi->SetDirectory(dir);
   if (fNMinusVsPhi) fNMinusVsPhi->SetDirectory(dir);

   if (fNPlusVsTheta)  fNPlusVsTheta->SetDirectory(dir);
   if (fNMinusVsTheta) fNMinusVsTheta->SetDirectory(dir);

}
//_____________________________________________________________________________
Double_t InSANEPi0MeasuredAsymmetry::GetAsymmetry(Double_t N1, Double_t N2, Double_t df)  {
   if (N1 == 0 || N2 == 0) {
      //Error("GetAsymmetry", "Zero count.");
      return(0.0);
   }
   Double_t Pb = fPb;
   Double_t Pt = fPt;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];
   Double_t res = 1.0 / (df * Pb * Pt);
   res = res * (N1/(Q1*L1) - N2/(Q2*L2))/(N1/(Q1*L1) + N2/(Q2*L2));
   std::cout << "pi0 Asym = " << res << std::endl;
   return(res);
}
//_____________________________________________________________________________
Double_t InSANEPi0MeasuredAsymmetry::GetStatisticalError(Double_t N1, Double_t N2, Double_t df)  {
   // Returns statistical error of asymmetry
   // up to dilution and polarization corrections
   Double_t Pb = fPb;
   Double_t Pt = fPt;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // raw asymmetry statistical error squared
   Double_t Araw   = (N1-N2)/(N1+N2);
   Double_t eAraw2 = 4.0*N1*N2/(TMath::Power(N1+N2,3.0));  

   // asymmetry corrected for charge and livetime
   Double_t den0   = (Araw-1.0)*L1*Q1-(Araw+1.0)*L2*Q2; 
   Double_t eAexp2 = eAraw2*TMath::Power(4.0*L1*L2*Q1*Q2,2.0)/TMath::Power(den0,4.0);

   // asymmetry correcting for the dilution and polarizations
   Double_t eAmeas2 = eAexp2/TMath::Power(Pb*Pt*df,2.0);

   //Double_t error = TMath::Sqrt(( (0.0025 * TMath::Power(N1, 3) - 0.0025 * TMath::Power(N1, 2) * N2 - 0.0025 * N1 * TMath::Power(N2, 2) + 0.00250 * TMath::Power(N2, 3))
   //         * TMath::Power(fPb, 2) + (0.0025 * TMath::Power(N1, 3) - 0.0025 * TMath::Power(N1, 2) * N2 + 0.002500 * TMath::Power(N2, 3) +
   //            N1 * N2 * (-0.002500 * N2 + 4.*TMath::Power(fPb, 2))) *
   //         TMath::Power(fPt, 2)) / (TMath::Power(N1 + N2, 3) * TMath::Power(fPb, 4) * TMath::Power(fPt, 4))) ;

   Double_t error = TMath::Sqrt(eAmeas2);
   if( TMath::IsNaN(error) ) {
      error = 0.0;
   }
   return(error);
}
//_____________________________________________________________________________
Double_t InSANEPi0MeasuredAsymmetry::GetSystematicError(Double_t N1, Double_t N2, Double_t df)  {
   using namespace TMath;
   Double_t Pb = fPb;
   Double_t Pt = fPt;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];
   Double_t Q  = (Q1+Q2)/2.0;
   Double_t L  = (L1+L2)/2.0;
   Double_t edf   = 0.01;
   Double_t ePb   = 0.005;
   Double_t ePt   = 0.005;
   Double_t eQ    = 0.005;
   Double_t eL    = 0.005;

   // Raw asymmetry
   Double_t Araw   = (N1-N2)/(N1+N2);

   // Charge and livetime corrected asymmetry
   Double_t Aexp = (N1/(Q1*L1) - N2/(Q2*L2))/(N1/(Q1*L1) + N2/(Q2*L2));
   Double_t eAexpSys2 = TMath::Power(Araw*Araw-1.0,2.0)*(Q*Q*eL*eL+L*L*eQ*eQ)/(2.0*L*L*Q*Q);

   Double_t eAmeasSys2 = eAexpSys2/TMath::Power(df*Pb*Pt,2.0);
   eAmeasSys2 += Aexp*Aexp*(TMath::Power(df*Pt*ePb,2.0)+
                            TMath::Power(df*ePt*Pb,2.0)+
                            TMath::Power(edf*Pt*Pb,2.0))/TMath::Power(df*Pb*Pt,4.0);

   Double_t error = TMath::Sqrt(eAmeasSys2);
   //Double_t res = Sqrt(((4 * Power(df, 2) * N1 * N2 + Power(delDf, 2) * Power(N1 - N2, 2) * (N1 + N2)) * Power(Pb, 2) * Power(Pt, 2) +
   //         Power(delPb, 2) * Power(df, 2) * Power(N1 - N2, 2) * (N1 + N2) * (Power(Pb, 2) +
   //            Power(Pt, 2))) / (Power(df, 4) * Power(N1 + N2, 3) * Power(Pb, 4) * Power(Pt, 4)));
   if( TMath::IsNaN(error) ) return 0.0;
   return(error);
}
//_____________________________________________________________________________
Double_t InSANEPi0MeasuredAsymmetry::GetTotalError(Double_t N1, Double_t N2, Double_t df)  {
   using namespace TMath;
   Double_t Pb = fPb;
   Double_t Pt = fPt;
   Double_t delDf = 0.01; ///\todo fix this
   Double_t delPb = 0.05; ///\todo fix this
   //Double_t delPt = 0.05;
   Double_t res = Sqrt(((4 * Power(df, 2) * N1 * N2 + Power(delDf, 2) * Power(N1 - N2, 2) * (N1 + N2)) * Power(Pb, 2) * Power(Pt, 2) +
            Power(delPb, 2) * Power(df, 2) * Power(N1 - N2, 2) * (N1 + N2) * (Power(Pb, 2) +
               Power(Pt, 2))) / (Power(df, 4) * Power(N1 + N2, 3) * Power(Pb, 4) * Power(Pt, 4)));
   if( TMath::IsNaN(res) ) return 0.0;
   return(res);
}
//_____________________________________________________________________________

