#define SANEGasCherenkovAnalysis_cxx
#include "SANEEvents.h"
#include "SANEGasCherenkovAnalysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "BETAEvent.h"
#include "GasCherenkovHit.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "TClonesArray.h"
#include "InSANEEventDisplay.h"
#include "GasCherenkovGeometryCalculator.h"
#include "TH2F.h"
ClassImp(SANEGasCherenkovAnalysis)

//__________________________________________________
SANEGasCherenkovAnalysis::SANEGasCherenkovAnalysis(const char * sourceTreeName): InSANEDetectorAnalysis(sourceTreeName)
{


//Make sure this comes after opening the file
//   fOutputTree = new TTree("cherenkovEvents","Detector Event Data");
//   fOutputTree->Branch("cherenkovBetaDetectorEvent","BETAEvent",&(fEvents->BETA) );
//   fOutputTree->Branch("cherenkovTrigDetectorEvent","InSANETriggerEvent",&fEvents->TRIG);
//   fOutputTree->Branch("cherenkovHmsDetectorEvent","HMSEvent",&fEvents->HMS);
//   fOutputTree->Branch("cherenkovBeamDetectorEvent","HallCBeamEvent",&fEvents->BEAM);
//   fOutputTree->SetAutoSave();

   fAnalysisFile->cd();
   fAnalysisFile->mkdir("cherenkov");
   fAnalysisFile->cd("cherenkov");

   for (int i = 0; i < 12; i++) {
      cer_tdc_hist[i] = new TH1F(Form("cer_tdc_hist_%d", i), Form("Cherenkov TDC %d", i), 200, -3000, -1200);
      cer_adc_hist[i] = new TH1F(Form("cer_adc_%d", i), Form("Cherenkov  ADC %d", i), 100, 0, 4000);
      cer_tdc_vs_adc_hist[i] = new TH2F(Form("cer_tdc_vs_adc_hist_%d", i), Form("Cherenkov TDC vs ADC%d", i), 200, 0, 4000, 200, -3000, -1200);
   }

   for (int i = 0; i < 12 * 56; i++) {
      cer_tdc_hist_luc[i] = new TH1F(Form("cer_tdc_hist_luc_%d", i), Form("Cherenkov TDC %d", i), 200, -3000, -1200);
      cer_adc_hist_luc[i] = new TH1F(Form("cer_adc_luc_%d", i), Form("Cherenkov  ADC %d", i), 100, 0, 4000);
      cer_tdc_vs_adc_hist_luc[i] = new TH2F(Form("cer_tdc_vs_adc_hist_luc_%d", i), Form("Cherenkov TDC vs ADC%d", i), 100, 0, 4000, 200, -3000, -1200);
   }
   for (int i = 0; i < 12 * 56; i++) {
      luc_tdc_hist[i] = new TH1F(Form("luc_tdc_hist_%d", i), Form("Hodoscope TDC %d", i), 200, -3000, -1200);
      luc_adc_hist[i] = new TH1F(Form("luc_adc_%d", i), Form("Hodoscope  ADC %d", i), 200, -100, 2900);
      luc_tdc_vs_adc_hist[i] = new TH2F(Form("luc_tdc_vs_adc_hist_%d", i), Form("Hodoscope TDC vs ADC %d", i), 200, -100, 2900, 200, -3000, -1200);
   }
   fAnalysisFile->cd();

   /// \todo {Implement DETECTORPhysicsEvent }

}

//__________________________________________________
SANEGasCherenkovAnalysis::~SANEGasCherenkovAnalysis()
{
   fOutputTree->Write();
   fOutputFile->Write();
   fOutputFile->Close();

   ;
}

//__________________________________________________
Int_t SANEGasCherenkovAnalysis::AnalyzeRun()
{
   InSANEEventDisplay* display;
   if (fEventDisplay) display = new InSANEEventDisplay();
// Detectors used
   std::cout << " Creating detector configurations for this run... \n";
//   ForwardTrackerDetector *    tracker   = new ForwardTrackerDetector(fRunNumber);
   //GasCherenkovDetector *      cherenkov = new GasCherenkovDetector(fRunNumber);
   auto *   hodoscope = new LuciteHodoscopeDetector(fRunNumber);
//   BigcalDetector *            bigcal    = new BigcalDetector(fRunNumber);



///// EVENT LOOP  //////
   Int_t nevent = fEvents->fTree->GetEntries();
   std::cout << "\n" << nevent << " ENTRIES \n";
   TClonesArray * cerHits = gcEvent->fGasCherenkovHits;
   //TClonesArray * lucHits = lhEvent->fLuciteHits;
   //TClonesArray * bigcalHits = bcEvent->fBigcalHits;
   //TClonesArray * trackerHits = ftEvent->fTrackerHits;

   for (int iEVENT = 0; iEVENT < nevent; iEVENT++)     {
//    printf(" what");

      fEvents->GetEvent(iEVENT);
///\todo{ I think I can remove this method by using TTreeIndex and set friends}
      if (fEvents->TRIG->IsBETAEvent()) { // BETA2
//    printf(" ok");
         // Loop over lucite hits
//       for(int kk=0; kk< lhEvent->fNumberOfHits;kk++) {
//       aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;
//
//       //Loose Lucite TDC Cut (Hits on Both sides of the bar pass a TDC window cut)
//       if(
//        TMath::Abs(aLucHit->fNegativeTDC - hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeak)
//        < 5.0*hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeakWidth  &&
//        TMath::Abs(aLucHit->fPositiveTDC - hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeak)
//        < 5.0*hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeakWidth) {
//                hodoscope->fLucHit=true;
//          if(hodoscope->fBarTimedTDCHit[aLucHit->fRow-1])
//          hodoscope->fBarDoubleTimedTDCHit[aLucHit->fRow-1]=true;
//          hodoscope->fBarTimedTDCHit[aLucHit->fRow-1]=true;
//       }
//     } // End loop over Lucite Hits

// Now we have the hodoscope hits filled and want to translate this to an
// image of the cherenkov. For each bar, we produce cherenkov ADC and TDC histograms
// as well as fill the

// loop over cherenkov hits for event
         for (int kk = 0; kk < gcEvent->fNumberOfHits; kk++) {
            aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
//       if( TMath::Abs(aCerHit->fTDC - cherenkov->cherenkovTimings[aCerHit->fMirrorNumber-1]->fTDCPeak) <  5.0*cherenkov->cherenkovTimings[aCerHit->fMirrorNumber-1]->fTDCPeakWidth )
//       { //passes VERY wide tdc cut
            cer_adc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fADC);
            cer_tdc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fTDC);
            cer_tdc_vs_adc_hist[(aCerHit->fMirrorNumber-1)]->Fill(aCerHit->fADC, aCerHit->fTDC);
            //Fill separte 12 cherenkov hists for each bar
//     if(hodoscope->fLucHit) {
//         for(int jj=0;jj<56;jj++) if(hodoscope->fBarTimedTDCHit[jj]) {
// /*printf(" ner");*/
//          cer_adc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fADC);
//          cer_tdc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fTDC);
//          cer_tdc_vs_adc_hist_luc[(aCerHit->fMirrorNumber-1)*56+jj]->Fill(aCerHit->fADC,aCerHit->fTDC);
//         }
//      }// end lucCut
//    }
         } // End loop over Cherenkov signals
      } // beta trigger bit
      fOutputTree->Fill();

      hodoscope->ClearEvent();
      aBetaEvent->ClearEvent();
   } // END OF EVENT LOOP

   return(0);
}

//__________________________________________________
Int_t SANEGasCherenkovAnalysis::Visualize()
{
   TCanvas * c1;
//   for(int j=0;j<12;j++){
//     c1  = new TCanvas(Form("Cherenkov%dWithLuciteCut",j+1),Form("Cherenkov Mirror %d",j+1));
//     c1->Divide(7,4);
//       for(int i =0;i<28;i++) {
//       c1->cd(i+1);
//       cer_adc_hist_luc[j*56+27-i]->Draw();
//     }
//   c1->SaveAs(Form("plots/%d/CherenkovMirror%dByLuciteBar.jpg",fRunNumber,j));
//   c1->SaveAs(Form("plots/%d/CherenkovMirror%dByLuciteBar.ps",fRunNumber,j));
//   }

   c1  = new TCanvas("CherenkovADCWithTDCCut", "Cherenkov ADCs with a TDC cut");
   c1->Divide(4, 3);
   for (int i = 0; i < 12; i++) {
      c1->cd(i + 1);
      cer_adc_hist[i]->Draw();
   }
   c1->SaveAs(Form("plots/%d/CherenkovADCWithTDCCut.jpg", fRunNumber));
   c1->SaveAs(Form("plots/%d/CherenkovADCWithTDCCut.ps", fRunNumber));

   c1  = new TCanvas("CherenkovTDCWithTDCCut", "Cherenkov TDCs with a TDC cut");
   c1->Divide(4, 3);
   for (int i = 0; i < 12; i++) {
      c1->cd(i + 1);
      cer_tdc_hist[i]->Draw();
   }
   c1->SaveAs(Form("plots/%d/CherenkovTDCWithTDCCut.jpg", fRunNumber));
   c1->SaveAs(Form("plots/%d/CherenkovTDCWithTDCCut.ps", fRunNumber));

   c1  = new TCanvas("CherenkovTDCvsADCWithTDCCut", "Cherenkov TDC vs ADC with a TDC cut");
   c1->Divide(4, 3);
   for (int i = 0; i < 12; i++) {
      c1->cd(i + 1);
      cer_tdc_vs_adc_hist[i]->Draw();
   }
   c1->SaveAs(Form("plots/%d/CherenkovTDCvsADCWithTDCCut.jpg", fRunNumber));
   c1->SaveAs(Form("plots/%d/CherenkovTDCvsADCWithTDCCut.ps", fRunNumber));
//       cer_tdc_hist[i] = new TH1F(Form("cer_tdc_hist_%d",i),Form("Cherenkov TDC %d",i), 200,-3000,-1200);
//       cer_adc_hist[i] = new TH1F(Form("cer_adc_%d",i),Form("Cherenkov  ADC %d",i), 100,0,4000);
//       cer_tdc_vs_adc_hist[i] = new TH2F(Form("cer_tdc_vs_adc_hist_%d",i),Form("Cherenkov TDC vs ADC%d",i), 200,0,4000, 200,-3000,-1200);

   return(0);
}

