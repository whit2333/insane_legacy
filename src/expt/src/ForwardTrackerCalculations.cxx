#include "ForwardTrackerCalculations.h"
#include "SANEAnalysisManager.h"
#include "InSANEHitPosition.h"
#include "ForwardTrackerPositionHit.h"

ClassImp(ForwardTrackerCalculation1)

//______________________________________________________________________________
Int_t ForwardTrackerCalculation1::Calculate() {

   if (!(fEvents->TRIG->IsBETAEvent()))  return(0);
//  std::cout << fEvents->BETA->fLuciteHodoscopeEvent->fNumberOfHits << " tdc hits\n";

   // copy the array, clear it, then re-fill it
   ForwardTrackerHit * timedHit   = nullptr;
   if(!fCopiedArray) fCopiedArray = new TClonesArray("ForwardTrackerHit",100);
   fCopiedArray->Clear();
   TClonesArray &aCopy            = (*fCopiedArray);
   //fCopiedArray = (TClonesArray*) fTrackerHits->Clone("tempTrackerHits");

   // Manually copy it.
   for (int kk = 0; kk < fTrackerHits->GetEntries() ; kk++) {
      aTrackerHit = (ForwardTrackerHit*)(*fTrackerHits)[kk];
      new(aCopy[kk]) ForwardTrackerHit( *aTrackerHit );
   }

   // Clear the tree array
   fTrackerHits->Clear("");
   int numberKept = 0;

   //if(fTrackerHits) std::cout << " o before fTrackerHits->GetEntries() = " << fTrackerHits->GetEntries() << "\n";

   for (int kk = 0; kk < aCopy.GetEntries() ; kk++)
      /*    for (int kk=0; kk< fTrackerHits->GetEntries() ;kk++) */
   {
      aTrackerHit = (ForwardTrackerHit*)(aCopy)[kk] ;
      if (aTrackerHit->fLevel == 1) {
         /// Count all TDC hits
         //aBetaEvent->fForwardTrackerEvent->fNumberOfTDCHits++;
         /// AND if there  is a one sided timed tdc hit (with a realtively wide cut)
         if (numberKept < fMaxHits)
            if (fTrackerDetector->cTDCAlignMin < aTrackerHit->fTDCAlign && fTrackerDetector->cTDCAlignMax > aTrackerHit->fTDCAlign) {
               /*         if( fTrackerDetector->cTDCMin < aTrackerHit->fTDC && fTrackerDetector->cTDCMax > aTrackerHit->fTDC  ) {*/
               /// Then we keep this hit in the array for later  use.
               aBetaEvent->fForwardTrackerEvent->fNumberOfTimedTDCHits++;
               aTrackerHit->fPassesTimingCut = true;
               timedHit = new((*fTrackerHits)[numberKept]) ForwardTrackerHit(*aTrackerHit);
               numberKept++;
            } else { /// Remove un-timed hits.
               //fTrackerHits->RemoveAt(kk);
            }
      } // end level = 1
   } // End loop over tracker signals
//     if(fTrackerHits) std::cout << " o after fTrackerHits->GetEntries() = " << fTrackerHits->GetEntries() << "\n";

   /// Now create position hits using the detector and event objects (not another hit loop)
   Int_t fNumberOfPositions = 0;
   Int_t fNumberOfY1Positions = 0;
   Int_t fNumberOfY2Positions = 0;

   ForwardTrackerHit * bTrackerHit = nullptr;
   ForwardTrackerPositionHit * aPositionHit;

   Double_t x = 0.0;
   Double_t y = 0.0;
   Double_t z = 0.0;//fTrackerDetector->GetDistanceFromTarget();

   ftEvent->fNumberOfTimedPositionHits = 0;

   for (int i = 0; i < fTrackerHits->GetEntries(); i++) {
      aTrackerHit = (ForwardTrackerHit*)(*fTrackerHits)[i];

      if (aTrackerHit->fScintLayer == 0) // X1 plane
      if (TMath::Abs(aTrackerHit->fTDCAlign ) < 150) {   /// \todo fix hard coded tdc cut

         x = fTrackerDetector->GetXPosition(aTrackerHit);

         for (int j = 0; j < fTrackerHits->GetEntries(); j++) {
            bTrackerHit = (ForwardTrackerHit*)(*fTrackerHits)[j];

            if (fNumberOfPositions < fMaxPositionHits)
               if (bTrackerHit->fScintLayer == 1 || bTrackerHit->fScintLayer == 2)
                  if (TMath::Abs(bTrackerHit->fTDCAlign ) < 150) {   /// \todo fix hard coded tdc cut

                     y = fTrackerDetector->GetYPosition(bTrackerHit);
                     /// OLD: Average the Z plane positions ( because if it involves the combo (X1+Y2) there should be a position Y1 )
		     /// New: use just Y plane positions
		     /// Maybe use the Y plane z positon and correct the X position
		     ///  perhaps |x_correction| = (z_X - z_Y)*tan(theta)
		     double za = fTrackerDetector->GetZPosition(aTrackerHit);
		     double zb = fTrackerDetector->GetZPosition(bTrackerHit);
		     double deltaz = TMath::Abs(za - zb);
                     z = (zb + za)/2.0;
                     //z = (fTrackerDetector->GetZPosition(bTrackerHit));// + fTrackerDetector->GetZPosition(aTrackerHit))/2.0;
                     double deltax = 1.0*(x/z)*deltaz/2.0;
                     x = x + deltax + fTrackerDetector->fGeoCalc->GetXPositionCorrection(y);
                     double deltay = -1.0*(y/z)*deltaz/2.0; // deltay has opposite sign of deltax
                     y = y + deltay;
                     aPositionHit = new((*ftEvent->fTrackerPositionHits)[fNumberOfPositions]) ForwardTrackerPositionHit();
                     aPositionHit->fHitNumber = fNumberOfPositions;
                     fNumberOfPositions++;
                     ftEvent->fNumberOfTimedPositionHits++;
                     // label the position hit by the y channel number
                     aPositionHit->fChannel = fTrackerDetector->fGeoCalc->GetScintNumber(bTrackerHit->fScintLayer, bTrackerHit->fRow);
                     aPositionHit->fYRow = bTrackerHit->fRow;
                     aPositionHit->fXRow = aTrackerHit->fRow;

                     aPositionHit->fScintLayer = bTrackerHit->fScintLayer;//
                     aPositionHit->fPositionVector.SetXYZ(x, y, z);
                     aPositionHit->fTDCSum = aTrackerHit->fTDCAlign + bTrackerHit->fTDCAlign;
                     /*
                     if( bTrackerHit->fScintLayer == 1 )
                     {
                           new((*ftEvent->fTrackerY1PositionHits)[fNumberOfY1Positions]) ForwardTrackerPositionHit(*aPositionHit);
                        fNumberOfY1Positions++;
                     }
                     if( bTrackerHit->fScintLayer == 2 )
                     {
                           new((*ftEvent->fTrackerY2PositionHits)[fNumberOfY2Positions]) ForwardTrackerPositionHit(*aPositionHit);
                        fNumberOfY2Positions++;
                     }
                     */
                  }
         }
      }
   }
//events->BETA->fForwardTrackerEvent->fTrackerHits
   /// Process the new position hits
   fTrackerDetector->ProcessPositionHits();


   /// \todo this should go in Detector class Process Event?
   for (int kk = 0; kk < fEvents->BETA->fForwardTrackerEvent->fTrackerPositionHits->GetEntries(); kk++) {
      aPositionHit = (ForwardTrackerPositionHit*)(*(fEvents->BETA->fForwardTrackerEvent->fTrackerPositionHits))[kk];
      aPositionHit->fNNeighborHits = 0;

      Int_t N0 = 0;
      Int_t Ns = 0;

      /// Count the other layer with the same scint number
      if (aPositionHit->fScintLayer == 1) {
         N0 = aPositionHit->fChannel - 1 - 64;
         Ns = N0;
//         if(Ns<128&&Ns>=0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer2PositionCounts[Ns]->size();
         Ns = N0 - 1;
         if (Ns < 128 && Ns >= 0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer1PositionCounts[Ns]->size();
         Ns = N0 + 1;
         if (Ns < 128 && Ns >= 0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer1PositionCounts[Ns]->size();
      }
      if (aPositionHit->fScintLayer == 2) {
         N0 = aPositionHit->fChannel - 1 - 64 - 128;
         Ns = N0;
         //       if(Ns<128&&Ns>=0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer1PositionCounts[Ns]->size();
         Ns = N0 - 1;
         if (Ns < 128 && Ns >= 0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer2PositionCounts[Ns]->size();
         Ns = N0 + 1;
         if (Ns < 128 && Ns >= 0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer2PositionCounts[Ns]->size();
      }
      /*
            /// Count the hits on both sides from both layers
            // this is now done above and only for each layer.
            Ns = N0 - 1;
            if(Ns<132&&Ns>=0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer1PositionCounts[Ns]->size();
            if(Ns<132&&Ns>=0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer2PositionCounts[Ns]->size();
            Ns = N0 + 1;
            if(Ns<132&&Ns>=0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer1PositionCounts[Ns]->size();
            if(Ns<132&&Ns>=0) aPositionHit->fNNeighborHits += fTrackerDetector->fLayer2PositionCounts[Ns]->size();
      */
   }

   /// Process positions for each Y layer
   for (int kk = 0; kk < fEvents->BETA->fForwardTrackerEvent->fTrackerPositionHits->GetEntries(); kk++) {
      aPositionHit = (ForwardTrackerPositionHit*)(*(fEvents->BETA->fForwardTrackerEvent->fTrackerPositionHits))[kk];

      if (fNumberOfY1Positions < fMaxPositionHits)
         if (aPositionHit->fScintLayer == 1) {
            new((*ftEvent->fTrackerY1PositionHits)[fNumberOfY1Positions]) ForwardTrackerPositionHit(*aPositionHit);
            fNumberOfY1Positions++;
         }
      if (fNumberOfY2Positions < fMaxPositionHits)
         if (aPositionHit->fScintLayer == 2) {
            new((*ftEvent->fTrackerY2PositionHits)[fNumberOfY2Positions]) ForwardTrackerPositionHit(*aPositionHit);
            fNumberOfY2Positions++;
         }

   }

   /// Now clear and recombine each layer's hits...
   //if(fCopiedArray) delete  fCopiedArray;
   //fCopiedArray = (TClonesArray*) ftEvent->fTrackerPositionHits->Clone("tempTrackerHits");
   //TClonesArray &bCopy = (*fCopiedArray);
   //ftEvent->fTrackerPositionHits->Clear("C");
   ForwardTrackerPositionHit * aPosition = nullptr;
   ForwardTrackerPositionHit * bPosition = nullptr;
   ForwardTrackerPositionHit * cPosition = nullptr;
   ftEvent->fNumberOfTimed2PositionHits = 0;
   Int_t npositions = 0;
   for (int k1 = 0; k1 < ftEvent->fTrackerY1PositionHits->GetEntries(); k1++) {
      aPosition = (ForwardTrackerPositionHit*)(*(ftEvent->fTrackerY1PositionHits))[k1];
      for (int k2 = 0; k2 < ftEvent->fTrackerY2PositionHits->GetEntries(); k2++) {
         bPosition = (ForwardTrackerPositionHit*)(*(ftEvent->fTrackerY2PositionHits))[k2];

         /// Make sure that the Y1 and Y2  positions  are less than 2 cm away
         /// \todo  fix hard coded distance cut...
         if (npositions < fMaxPositionHits)
            if ( (aPosition->fPositionVector - bPosition->fPositionVector).Y() < 4.0) {
               cPosition = new((*(ftEvent->fTracker2PositionHits))[npositions]) ForwardTrackerPositionHit(*aPosition);
               cPosition->fPositionVector += bPosition->fPositionVector;
               cPosition->fPositionVector.SetMag(cPosition->fPositionVector.Mag() / 2.0);
               cPosition->fPositionVector2 = bPosition->fPositionVector;
               cPosition->fTDCSum += bPosition->fTDCSum;
               cPosition->fNNeighborHits += bPosition->fNNeighborHits;
               npositions++;
               ftEvent->fNumberOfTimed2PositionHits++;
            }
      }
   }

//         std::cout << " TDC position HITs done\n";

   return(0);
}
//_______________________________________________________//


ClassImp(ForwardTrackerCalculation2)

Int_t ForwardTrackerCalculation2::Calculate()
{

   if (!(fEvents->TRIG->IsClusterable()))  return(0);

   ForwardTrackerPositionHit * aHit;
   ForwardTrackerPositionHit * bHit;
   ForwardTrackerPositionHit * cHit;
   Int_t numHitPairs = 0;

   if (fCopiedArray) delete fCopiedArray; // I guess this is the only way... kinda slow
   fCopiedArray = (TClonesArray*) ftEvent->fTrackerPositionHits->Clone("tempTrackerPositionHits");
   TClonesArray &aCopy = (*fCopiedArray);
   ftEvent->fTrackerPositionHits->Clear("C");

   for (int i = 0; i < aCopy.GetEntries(); i++) {
      aHit = (ForwardTrackerPositionHit*)(aCopy)[i];
      if (aHit->fScintLayer == 1)
         for (int j = 0; j < aCopy.GetEntries(); j++) {
            bHit = (ForwardTrackerPositionHit*)(aCopy)[j];
            if (bHit->fScintLayer == 2)
               if (aHit != bHit)
                  if (TMath::Abs(aHit->fTDCSum) < 300 && TMath::Abs(bHit->fTDCSum) < 300)
                     if (fTrackerDetector->GetMissDistance(aHit->fPositionVector, bHit->fPositionVector) < 0.3 * 4.0 /* 4 bar widths */) {
                        cHit = new((ForwardTrackerPositionHit*)(*ftEvent->fTrackerPositionHits)[numHitPairs])ForwardTrackerPositionHit(*aHit);
                        cHit->fHitNumber = numHitPairs;
                        numHitPairs++;
                        cHit->fPositionVector2 = bHit->fPositionVector;
                        // should average position or should we record two???
                     }

         }


   }


   return(0);
}
//_______________________________________________________//

