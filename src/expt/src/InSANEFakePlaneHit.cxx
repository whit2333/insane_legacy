#include  "InSANEFakePlaneHit.h"



ClassImp(InSANEFakePlaneHit)

//______________________________________________________________________________
InSANEFakePlaneHit::InSANEFakePlaneHit() {
   fPID    = 0;
   fTheta  = 0.0;
   fPhi    = 0.0;
   fEnergy = 0.0;
   Clear();
}
//______________________________________________________________________________

InSANEFakePlaneHit::~InSANEFakePlaneHit()
{
}
//______________________________________________________________________________
InSANEFakePlaneHit::InSANEFakePlaneHit(const InSANEFakePlaneHit& rhs) : InSANEDetectorHit(rhs) {
   (*this) = rhs;
}
//______________________________________________________________________________
Int_t InSANEFakePlaneHit::GetPdgCode() const {
   return(fPID);
}
//______________________________________________________________________________
void InSANEFakePlaneHit::Clear(Option_t * opt) {
   InSANEDetectorHit::Clear(opt);
   fPID    = 0;
   fTheta  = 0.0;
   fPhi    = 0.0;
   fEnergy = 0.0;
   fPosition.SetXYZ(0, 0, 0);
   fLocalPosition.SetXYZ(0, 0, 0);
   fMomentum.SetXYZ(0, 0, 0);
   fMomentum4Vector.SetXYZT(0.0, 0.0, 0.0, 0.0);
}
//______________________________________________________________________________
InSANEFakePlaneHit& InSANEFakePlaneHit::operator=(const InSANEFakePlaneHit &rhs) {
   // Check for self-assignment!
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   // Deallocate, allocate new space, copy values...
   InSANEDetectorHit::operator=(rhs);
   fEnergy        = rhs.fEnergy;
   fPID           = rhs.fPID;
   fTheta         = rhs.fTheta;
   fPhi           = rhs.fPhi;
   fPosition      = rhs.fPosition;
   fLocalPosition = rhs.fLocalPosition;
   fMomentum      = rhs.fMomentum;
   return *this;
}
//______________________________________________________________________________

