#include "InSANECombinedAsymmetry.h"

InSANECombinedAsymmetry::InSANECombinedAsymmetry(Int_t n): fNumber(n)
{ }
//______________________________________________________________________________

InSANECombinedAsymmetry::~InSANECombinedAsymmetry()
{ }
//______________________________________________________________________________
void InSANECombinedAsymmetry::InitHists(const TH1F & h)
{
   TH1F c = h;
   c.Reset();
   fAsymmetryVsx     = c; 
   fAsymmetryVsW     = c; 
   fAsymmetryVsNu    = c; 
   fAsymmetryVsE     = c; 
   fAsymmetryVsTheta = c; 
   fAsymmetryVsPhi   = c; 

   fNPlusVsx         = c; 
   fNMinusVsx        = c; 

   fNPlusVsW         = c; 
   fNMinusVsW        = c; 

   fNPlusVsNu        = c; 
   fNMinusVsNu       = c; 

   fNPlusVsE         = c; 
   fNMinusVsE        = c; 

   fNPlusVsPhi       = c; 
   fNMinusVsPhi      = c; 

   fNPlusVsTheta     = c; 
   fNMinusVsTheta    = c; 

   fDilutionVsx      = c; 
   fDilutionVsW      = c; 
   fDilutionVsNu     = c; 
   fDilutionVsE      = c; 
   fDilutionVsTheta  = c; 
   fDilutionVsPhi    = c; 

   fSystErrVsx       = c; 
   fSystErrVsW       = c; 
   fSystErrVsNu      = c; 
   fSystErrVsE       = c; 
   fSystErrVsTheta   = c; 
   fSystErrVsPhi     = c; 

   fAvgKineVsx.CreateHistograms(h);
   fAvgKineVsW.CreateHistograms(h);
   fAvgKineVsNu.CreateHistograms(h);
   fAvgKineVsE.CreateHistograms(h);
   fAvgKineVsTheta.CreateHistograms(h);
   fAvgKineVsPhi.CreateHistograms(h);


   for( auto sys : fSystematics_x )     sys.InitHists(h);
   for( auto sys : fSystematics_W )     sys.InitHists(h);
   for( auto sys : fSystematics_Nu )    sys.InitHists(h);
   for( auto sys : fSystematics_E )     sys.InitHists(h);
   for( auto sys : fSystematics_Theta ) sys.InitHists(h);
   for( auto sys : fSystematics_Phi )   sys.InitHists(h);
   //fSystematics_x.InitHists(h);
   //fSystematics_W.InitHists(h);
   //fSystematics_Nu.InitHists(h);
   //fSystematics_E.InitHists(h);
   //fSystematics_Theta.InitHists(h);
   //fSystematics_Phi.InitHists(h);
} 
//______________________________________________________________________________


void InSANECombinedAsymmetry::Reset(Option_t * opt)
{
   fAsymmetryVsx.Reset(opt);
   fAsymmetryVsW.Reset(opt);
   fAsymmetryVsNu.Reset(opt);
   fAsymmetryVsE.Reset(opt);
   fAsymmetryVsTheta.Reset(opt);
   fAsymmetryVsPhi.Reset(opt);

   fNPlusVsx.Reset(opt);
   fNMinusVsx.Reset(opt);

   fNPlusVsW.Reset(opt);
   fNMinusVsW.Reset(opt);

   fNPlusVsNu.Reset(opt);
   fNMinusVsNu.Reset(opt);

   fNPlusVsE.Reset(opt);
   fNMinusVsE.Reset(opt);

   fNPlusVsPhi.Reset(opt);
   fNMinusVsPhi.Reset(opt);

   fNPlusVsTheta.Reset(opt);
   fNMinusVsTheta.Reset(opt);

   fDilutionVsx.Reset(opt);
   fDilutionVsW.Reset(opt);
   fDilutionVsNu.Reset(opt);
   fDilutionVsE.Reset(opt);
   fDilutionVsTheta.Reset(opt);
   fDilutionVsPhi.Reset(opt);

   fSystErrVsx.Reset(opt);
   fSystErrVsW.Reset(opt);
   fSystErrVsNu.Reset(opt);
   fSystErrVsE.Reset(opt);
   fSystErrVsTheta.Reset(opt);
   fSystErrVsPhi.Reset(opt);

   //fAvgKineVsx.CreateHistograms(h);
   //fAvgKineVsW.CreateHistograms(h);
   //fAvgKineVsNu.CreateHistograms(h);
   //fAvgKineVsE.CreateHistograms(h);
   //fAvgKineVsTheta.CreateHistograms(h);
   //fAvgKineVsPhi.CreateHistograms(h);

   for( auto sys : fSystematics_x )     sys.Reset(opt);
   for( auto sys : fSystematics_W )     sys.Reset(opt);
   for( auto sys : fSystematics_Nu )    sys.Reset(opt);
   for( auto sys : fSystematics_E )     sys.Reset(opt);
   for( auto sys : fSystematics_Theta ) sys.Reset(opt);
   for( auto sys : fSystematics_Phi )   sys.Reset(opt);
} 
//______________________________________________________________________________

