#include "UVAOxfordMagneticField.h"

//Double_t BilinearInterpolation(Double_t * cornerDataPoints, Double_t * corners, Double_t x, Double_t y);

ClassImp(UVAOxfordMagneticField)

//______________________________________________________________________________
UVAOxfordMagneticField::UVAOxfordMagneticField(const char * n) : InSANEMagneticField(n) {
   //std::cout << "UVa Magnet Constructed!" << std::endl;
   fMaximumField = 5.1;
   fFieldMapFile = "UVA-ExtendedFieldMap.csv";
   BzFieldRaw = BzRpoint;
   BrFieldRaw = BrRpoint;

   // Unfortunately, the data reads in the z's first,
   // so it is BzFieldRaw[R index][Z index]
   for (int g = 0; g < 221; g++) {
      BzRpoint[g] = &RawBZ[141*g];
      BrRpoint[g] = &RawBR[141*g];
   }
   LoadFieldMap(); // from MySQL DATABASE
}

//______________________________________________________________________________
UVAOxfordMagneticField::~UVAOxfordMagneticField() {
}
//______________________________________________________________________________
void UVAOxfordMagneticField::GetFieldValue(const Double_t Point[4], Double_t *Bfield) const {

   Bfield[2] = 0.0;
   Bfield[1] = 0.0;
   Bfield[0] = 0.0;
/// ROOT User's Guide: Physics Vectors
/// To get information on the TVector3 in spherical (rho,phi,theta) or
/// cylindrical (z,r,theta) coordinates, the
/// the member functions Mag() (=magnitude=rho in spherical coordinates), Mag2(),
/// Theta(), CosTheta(), Phi(), Perp() (the transverse component=r in cylindrical coordinates),
   TVector3 thePositionVector(Point[0], Point[1], Point[2]);
   thePositionVector.RotateY(-fPolarizationAngle);
/// Now if we were polarized at 80 degrees and wanted B at zero degrees,
/// We would rotate our position
/// back to polangle=0, interpolate the field. Get the vector.
/// Then rotate the field vector back to 80 degrees
   Double_t phi = thePositionVector.Phi();
   Double_t R   = thePositionVector.Perp();
   Double_t Z   = thePositionVector.Z();

/// POSITIVE Z
   if ((Z < 4.40 * 100.0/*meters2cm*/)
       && (Z >= 0.00/*meters2cm*/)
       && R <= 2.80 * 100.0/*meters2cm*/) {
      auto ii = (Int_t)TMath::Floor(Z / (2.0));
      auto jj = (Int_t)TMath::Floor(R / (2.0));

//  std::cout << "jj=" << jj<< " ii =" << ii<< " phi=" << phi*180.0/TMath::Pi() << std::endl;

      Double_t corners[4] = {ii * 2.0, (ii + 1) * 2.0, jj * 2.0, (jj + 1) * 2.0};

// get interpolated Bz value
      Double_t cornerDataPoints[4] = {BzFieldRaw[ii][jj], BzFieldRaw[ii+1][jj], BzFieldRaw[ii][jj+1], BzFieldRaw[ii+1][jj+1] };

      Double_t interpBz = BilinearInterpolation(&cornerDataPoints[0], &corners[0], Z , R);

// get interpoolated Br value
      Double_t cornerDataPoints2[4] = {BrFieldRaw[ii][jj], BrFieldRaw[ii+1][jj], BrFieldRaw[ii][jj+1], BrFieldRaw[ii+1][jj+1] };

      Double_t interpBr = BilinearInterpolation(&cornerDataPoints2[0], &corners[0], Z, R);


      TVector3 theField(interpBr * TMath::Cos(phi), interpBr * TMath::Sin(phi), interpBz);
//  theField.setRhoPhiZ(interpBr,phi, interpBz );
      theField.RotateY(fPolarizationAngle);
      Bfield[0] = theField.X() * fScalingCoefficient;
      Bfield[1] = theField.Y() * fScalingCoefficient;
      Bfield[2] = theField.Z() * fScalingCoefficient;

   }
/// / NEGATIVE Z
   else if ((Z < 0.0/*meters2cm*/) &&
            (Z > -4.40 * 100.0/*meters2cm*/) &&
            (R <= 2.80 * 100.0/*meters2cm*/)) {
      auto ii = (Int_t)TMath::Floor(TMath::Abs(Z / 2.0));
      auto jj = (Int_t)TMath::Floor(R / 2.0);
//  G4cout << "ii=" << ii<< " jj =" << jj<< G4endl;
      Double_t corners[4] = {ii * 2.0, (ii + 1) * 2.0, jj * 2.0, (jj + 1) * 2.0};
// get interpolated Bz value
      Double_t cornerDataPoints[4] = {BzFieldRaw[ii][jj], BzFieldRaw[ii+1][jj], BzFieldRaw[ii][jj+1], BzFieldRaw[ii+1][jj+1] };
      Double_t interpBz = BilinearInterpolation(&cornerDataPoints[0], &corners[0], TMath::Abs(Z) , R);

// get interpoolated Br value
      Double_t cornerDataPoints2[4] = {BrFieldRaw[ii][jj], BrFieldRaw[ii+1][jj], BrFieldRaw[ii][jj+1], BrFieldRaw[ii+1][jj+1] };
      Double_t interpBr = -1.0 * BilinearInterpolation(&cornerDataPoints2[0], &corners[0], TMath::Abs(Z), R);

      TVector3 theField(interpBr * TMath::Cos(phi), interpBr * TMath::Sin(phi), interpBz);
//  theField.setRhoPhiZ(interpBr,phi, interpBz );
      theField.RotateY(fPolarizationAngle);
      Bfield[0] = theField.X() * fScalingCoefficient;
      Bfield[1] = theField.Y() * fScalingCoefficient;
      Bfield[2] = theField.Z() * fScalingCoefficient;

   } else {
      Bfield[2] = 0.0;
      Bfield[1] = 0.0;
      Bfield[0] = 0.0;
   }


//printf("field x = %lf, y = %lf, z = %lf ",Bfield[0],Bfield[1],Bfield[2]);


// TURN FIELD OFF - UNCOMMENT
//   Bfield[2] = 0.0;
//   Bfield[1] = 0.0;
//   Bfield[0] = 0.0;
//printf("field x = %lf, y = %lf, z = %lf ",Bfield[0],Bfield[1],Bfield[2]);

}

//_____________________________________________________
void UVAOxfordMagneticField::ReadDataFile()
{
// this function is designed to read in the csv extended field map data for the UVA target
//   fieldDataPoint temp;
   std::ifstream input_file ;
   int i = 0;
   int j = 0;
   Double_t tempz, tempr, tempBz , tempBr;
   input_file.open("UVA-ExtendedFieldMap.csv");

   if (!input_file) {
      std::cout << " NO FILE NAMED " << fFieldMapFile << " FOUND \n";
   } else {

      for (j = 0; j < 141; j++) {
         for (i = 0; i < 221; i++) {
            input_file >> tempz >> tempr >> tempBz >> tempBr ;
            BzFieldRaw[i][j] = tempBz;
            BrFieldRaw[i][j] = tempBr;
         }
      }
      input_file.close();
   }
}
//______________________________________________________________________________
void UVAOxfordMagneticField::LoadFieldMap() {

   //std::cout << "tester" << std::endl;
   InSANEDatabaseManager * dbmanager = InSANEDatabaseManager::GetManager();
   char *       zErrMsg   = nullptr;
   int          rc        = 0;

   TString SQLCOMMAND("SELECT z,r,Bz,Br from magnetic_field_2cm_grid;");

   // SQLite3 sucks... don't use it
   //rc = sqlite3_exec(dbmanager->GetSQLite3Connection(), SQLCOMMAND.Data(),  UVAOxfordMagneticField::LoadFieldMap_callback, this, &zErrMsg);
   //if (rc != SQLITE_OK) {
   //   std::cerr << "SQL error: " << zErrMsg << std::endl;
   //   sqlite3_free(zErrMsg);
   //}

   TSQLServer * conn     = dbmanager->GetMySQLConnection();
   if(conn){
      TSQLResult * fieldMap = conn->Query(SQLCOMMAND);
      TSQLRow    * aRow     = nullptr;
      Double_t tempz, tempr, tempBz , tempBr;
      if(fieldMap){
         if(fieldMap->GetRowCount() == 141*221 ) {
            for (int j=0;j<141;j++) {
               for (int i=0;i<221;i++)
                  if( (aRow=fieldMap->Next()) ) {
                     tempz=(Double_t)atof(aRow->GetField(0));
                     tempr=(Double_t)atof(aRow->GetField(1));
                     tempBz=(Double_t)atof(aRow->GetField(2));
                     tempBr=(Double_t)atof(aRow->GetField(3));
                     BzFieldRaw[i][j] = tempBz;
                     BrFieldRaw[i][j] = tempBr;
                     delete aRow;
                     aRow = nullptr;
                  }
            }
         } else {
            std::cout << " x Check Failed: Assuming " << 141*221 << " data points in \n";
            std::cout << "   field map from MySQL database. \n";
         }
      } else {
         Error("LoadFieldMap","No results found in table.");
      }
   } else {
      Error("LoadFieldMap","No MySQL Server Found!");
   }

}
//______________________________________________________________________________
int UVAOxfordMagneticField::LoadFieldMap_callback(void * magField, int argc, char **argv, char **azColName) {
   auto * B = (UVAOxfordMagneticField*)magField;
   Double_t tempz, tempr, tempBz , tempBr;
   tempz = (Double_t)atof(argv[0]);
   tempr = (Double_t)atof(argv[1]);
   tempBz = (Double_t)atof(argv[2]);
   tempBr = (Double_t)atof(argv[3]);
   Int_t i = B->Getiz(tempz);
   Int_t j = B->Getjr(tempr);
   B->BzFieldRaw[i][j] = tempBz;
   B->BrFieldRaw[i][j] = tempBr;
   return 0;
}
//______________________________________________________________________________

ClassImp(UVAEveMagField)

//______________________________________________________________________________
UVAEveMagField::UVAEveMagField() {
   std::cout << "UVAEveMagField" << std::endl;
   fFieldConstant = false;
}
//______________________________________________________________________________
UVAEveMagField::~UVAEveMagField() {
}
//______________________________________________________________________________
TEveVector UVAEveMagField::GetField(Float_t x, Float_t y, Float_t z) const {
   Double_t field[3] = {0.0, 0.0, 0.0};
   Double_t pos[4] = {x, y, z, 0.0};
   GetFieldValue(pos, field);
   //std::cout << "x = " << pos[0] << ", y = " << pos[1] << ", z = " << pos[2] << "\n";
   //std::cout << "Bx = " << field[0] << ", By = " << field[1] << ", Bz = " << field[2] << "\n";
   return TEveVector(field[0], field[1], field[2]) ;
}
//______________________________________________________________________________

