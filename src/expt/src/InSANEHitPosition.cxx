#include "InSANEHitPosition.h"

ClassImp(InSANEHitPosition)

//______________________________________________________________________________
bool InSANEHitPosition::operator<(const InSANEHitPosition &rhs) const {
   return( fMissDistance < rhs.fMissDistance );
}
//______________________________________________________________________________
bool InSANEHitPosition::operator>(const InSANEHitPosition &rhs) const {
   return( fMissDistance > rhs.fMissDistance );
}
//______________________________________________________________________________
bool InSANEHitPosition::operator<=(const InSANEHitPosition &rhs) const {
   return( fMissDistance <= rhs.fMissDistance );
}
//______________________________________________________________________________
bool InSANEHitPosition::operator>=(const InSANEHitPosition &rhs) const {
   return( fMissDistance >= rhs.fMissDistance );
}
//______________________________________________________________________________
InSANEHitPosition::InSANEHitPosition() {
   fPosition.SetXYZ(0.0, 0.0, 0.0);
   fPositionUncertainty.SetXYZ(0.0, 0.0, 0.0);
   fMissDistance  = 0.0;
   fClusterNumber = -1;
   fHitNumber     = 0;
   fTiming        = 0;
   fNNeighbors    = 0;
}
//______________________________________________________________________________
InSANEHitPosition& InSANEHitPosition::operator=(const InSANEHitPosition& v) {
   if (this == &v)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   //TObject::operator=(v);
   fSeedingPosition     = v.fSeedingPosition     ;
   fPosition            = v.fPosition            ;
   fPositionUncertainty = v.fPositionUncertainty ;
   fMissDistance        = v.fMissDistance        ;
   fHitNumber           = v.fHitNumber           ;
   fClusterNumber       = v.fClusterNumber       ;
   fTiming              = v.fTiming              ;
   fNNeighbors          = v.fNNeighbors          ;
   return(*this);
}
//______________________________________________________________________________
InSANEHitPosition::InSANEHitPosition(const InSANEHitPosition& v) {
   *this = v;
}
//______________________________________________________________________________
InSANEHitPosition::~InSANEHitPosition(){
}
//______________________________________________________________________________
Int_t   InSANEHitPosition::Compare(const TObject *obj) const {
   if ( (*this) > ((InSANEHitPosition&)(*obj)))
      return 1;
   else if ( (*this) < ((InSANEHitPosition&)(*obj)))
      return -1;
   else
      return 0;
}
//______________________________________________________________________________
bool InSANEHitPosition::operator==(const InSANEHitPosition &rhs) const {
   if(fClusterNumber != rhs.fClusterNumber) return false;
   if(fHitNumber != rhs.fHitNumber) return false;
   return true;
}
//______________________________________________________________________________
bool InSANEHitPosition::operator!=(const InSANEHitPosition &rhs) const {
   return !(*this == rhs);
}
//______________________________________________________________________________

