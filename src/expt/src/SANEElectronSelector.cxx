#include "SANEElectronSelector.h"

ClassImp(SANEElectronSelector)
//___________________________________________________________________________
Int_t SANEElectronSelector::Calculate() {

   if (!(fEvents->TRIG->IsBETAEvent()))  return(0);
   fGroups->clear();
   fcGoodElectron = false;
   BIGCALCluster * aCluster;
   TVector3 Pvec;

   if (fClusterEvent) {

      for (int kk = 0; kk < fClusterEvent->fNClusters  ; kk++) {
         aCluster = (BIGCALCluster*)(*fClusters)[kk] ;

         fTrajectory->fNCherenkovElectrons = aCluster->fCherenkovBestADCSum;

         /// Cuts!!!
         //if( aCluster->fGoodCherenkovTDCHit )
         //if( aCluster->fCherenkovBestNPESum > 3.0 )
         //if (fCherenkovDetector->PassesCuts(aCluster))
         if(1)
            //if (fTrajectory->fNCherenkovElectrons > 0.2) 
            //&&
            // fTrajectory->fNCherenkovElectrons < 1.8)
            /*if( fBigcalDetector->cPassesClusterEnergy[aCluster->fClusterNumber] )*/
            /*if( !(fCherenkovDetector->HasNeighboringHit(aCluster->fPrimaryMirror)) )*/
         {

            /// Cluster Passes cuts
            fcGoodElectron       = true;
            fTrajectory->fIsGood = true;

            /// CLUSTER --> Bigcal Plane
            fANNDisElectronEvent.SetEventValues(aCluster); 
            fANNDisElectronEvent.fXClusterKurt = aCluster->fXKurtosis;
            fANNDisElectronEvent.fYClusterKurt = aCluster->fYKurtosis;
            fANNDisElectronEvent.fNClusters    = fClusterEvent->fNClusters;
            fANNDisElectronEvent.fRasterX      = fEvents->BEAM->fRasterPosition.X();
            fANNDisElectronEvent.fRasterY      = fEvents->BEAM->fRasterPosition.Y();

            fANNDisElectronEvent.GetMLPInputNeuronArray(fNNParams);

            if(fIsPerp) {
               aCluster->fDeltaE  = 0.0;//fNNPerpElectronClusterDeltaE.Value(0,fNNParams);
               aCluster->fDeltaX  = fNNPerpElectronClusterDeltaX.Value(0,fNNParams);
               aCluster->fDeltaY  = fNNPerpElectronClusterDeltaY.Value(0,fNNParams);
            } else {
               aCluster->fDeltaE  = 0.0;//fNNParaElectronClusterDeltaE.Value(0,fNNParams);
               aCluster->fDeltaX  = fNNParaElectronClusterDeltaX.Value(0,fNNParams);
               aCluster->fDeltaY  = fNNParaElectronClusterDeltaY.Value(0,fNNParams);
            }

            //std::cout << " fDeltaE = " << aCluster->fDeltaE << std::endl;
            //std::cout << " fDeltaX = " << aCluster->fDeltaX << std::endl;
            //std::cout << " fDeltaY = " << aCluster->fDeltaY << std::endl;

            /// Bigcal Plane --> Target
            fvertex.SetXYZ(fEvents->BEAM->fRasterPosition.X(),fEvents->BEAM->fRasterPosition.Y(),0.0);
            fx_bigcalplane = fBigcalDetector->GetXYCorrectedPosition(aCluster);
            // Position at bigcal plane
            fTrajectory->fXBigcal_B = fx_bigcalplane;

            fp_naive = fx_bigcalplane - fvertex; 

            fANNFieldEvent2.fBigcalPlaneTheta       = fp_naive.Theta();//bigcalPlane->fPosition.Theta();
            fANNFieldEvent2.fBigcalPlanePhi         = fp_naive.Phi();//bigcalPlane->fPosition.Phi();

            fANNFieldEvent2.fBigcalPlaneX           = aCluster->GetXmoment() + aCluster->fDeltaX; //bigcalPlane->fLocalPosition.X();
            fANNFieldEvent2.fBigcalPlaneY           = aCluster->GetXmoment() + aCluster->fDeltaY; //bigcalPlane->fLocalPosition.Y();
            fANNFieldEvent2.fBigcalPlaneEnergy      = aCluster->GetCorrectedEnergy();//bigcalPlane->fEnergy;

            fANNFieldEvent2.fRasterX                = fEvents->BEAM->fRasterPosition.X();
            fANNFieldEvent2.fRasterY                = fEvents->BEAM->fRasterPosition.Y();

            fANNFieldEvent2.GetMLPInputNeuronArray(fNNParams);

            if(fIsPerp) {
               aCluster->fDeltaTheta = fNNPerpElectronDeltaTheta.Value(0,fNNParams);
               aCluster->fDeltaPhi   = fNNPerpElectronDeltaPhi.Value(0,fNNParams);
            } else {
               aCluster->fDeltaTheta = fNNParaElectronDeltaTheta.Value(0,fNNParams);
               aCluster->fDeltaPhi   = fNNParaElectronDeltaPhi.Value(0,fNNParams);
            }

            /// Bigcal Plane X --> Bigcal Plane P
            fANNFieldEvent3.fBigcalPlaneTheta  = fp_naive.Theta();//bigcalPlane->fPosition.Theta();
            fANNFieldEvent3.fBigcalPlanePhi    = fp_naive.Phi();//bigcalPlane->fPosition.Phi();

            fANNFieldEvent3.fBigcalPlaneX      = aCluster->GetXmoment() + aCluster->fDeltaX; //bigcalPlane->fLocalPosition.X();
            fANNFieldEvent3.fBigcalPlaneY      = aCluster->GetXmoment() + aCluster->fDeltaY; //bigcalPlane->fLocalPosition.Y();
            fANNFieldEvent3.fBigcalPlaneEnergy = aCluster->GetCorrectedEnergy();//bigcalPlane->fEnergy;

            fANNFieldEvent3.fRasterX           = fEvents->BEAM->fRasterPosition.X();
            fANNFieldEvent3.fRasterY           = fEvents->BEAM->fRasterPosition.Y();

            fANNFieldEvent3.fXCluster          = aCluster->GetXmoment();
            fANNFieldEvent3.fYCluster          = aCluster->GetYmoment();

            fANNFieldEvent3.fXClusterSigma     = aCluster->GetXStdDeviation();
            fANNFieldEvent3.fYClusterSigma     = aCluster->GetYStdDeviation();

            fANNFieldEvent3.fXClusterSkew      = aCluster->fXSkewness;
            fANNFieldEvent3.fYClusterSkew      = aCluster->fYSkewness;

            fANNFieldEvent3.fXClusterKurt      = aCluster->fXKurtosis;
            fANNFieldEvent3.fYClusterKurt      = aCluster->fYKurtosis;

            fANNFieldEvent3.GetMLPInputNeuronArray(fNNParams);

            if(fIsPerp) {
               aCluster->fDeltaThetaP = fNNPerpElectronBCPDirTheta.Value(0,fNNParams);
               aCluster->fDeltaPhiP   = fNNPerpElectronBCPDirPhi.Value(0,fNNParams);
            } else {
               aCluster->fDeltaThetaP = fNNParaElectronBCPDirTheta.Value(0,fNNParams);
               aCluster->fDeltaPhiP   = fNNParaElectronBCPDirPhi.Value(0,fNNParams);
            }

            fTrajectory->fPBigcal_B.SetMagThetaPhi( fANNFieldEvent3.fBigcalPlaneEnergy,
                                                    fANNFieldEvent3.fBigcalPlaneTheta + aCluster->fDeltaThetaP,
                                                    fANNFieldEvent3.fBigcalPlanePhi   + aCluster->fDeltaPhiP);

            /// We call it a good electron
            //    std::cout << "good electron \n";

            /// Set the trajectory values
            fTrajectory->fHelicity     = aCluster->fHelicity;
            fTrajectory->fPhi          = aCluster->GetCorrectedPhi() ;
            fTrajectory->fTheta        = aCluster->GetCorrectedTheta() ;
            fTrajectory->fEnergy       = aCluster->GetCorrectedEnergy() ;

            fTrajectory->fSubDetector  = aCluster->fSubDetector;
            fGroups->push_back(aCluster->fSubDetector);
            if (aCluster->fXMoment < 0.0) fGroups->push_back(3);
            if (aCluster->fXMoment > 0.0) fGroups->push_back(4);

            fTrajectory->fEventNumber  = aCluster->fEventNumber;
            fTrajectory->fRunNumber    = aCluster->fRunNumber;
            fTrajectory->fPosition0    = fBigcalDetector->GetPosition(aCluster);
            Pvec.SetMagThetaPhi(fTrajectory->fEnergy, fTrajectory->fTheta, fTrajectory->fPhi);
            fTrajectory->fMomentum.SetVect(Pvec);
            fTrajectory->fMomentum.SetE(fTrajectory->fEnergy);

            fTrajectory->fCluster = (*aCluster);

            /* InSANERunManager::GetRunManager()->fCurrentRun->fTotalAsymmetry += aCluster->fHelicity;*/
            if (aCluster->fHelicity == 1) InSANERunManager::GetRunManager()->fCurrentRun->fTotalNPlus += 1;
            if (aCluster->fHelicity == -1) InSANERunManager::GetRunManager()->fCurrentRun->fTotalNMinus += 1;
            //std::cout << InSANERunManager::GetRunManager()->fCurrentRun->fTotalAsymmetry << "\n";
         }
      }
   }
   return(0);
}
//___________________________________________________________________________

ClassImp(SANEElectronSelector2)
   //___________________________________________________________________________

Int_t SANEElectronSelector2::Calculate()
{

   fCleanedEvent->ClearEvent();
   fcGoodElectron = false;

   // This is a BETA trigger without the pi0 bit
   if (fEvents->TRIG->IsBETA2Event() && !(fEvents->TRIG->IsPi0Event())) {
      //std::cout<< "BETA event without pi0 bit\n";

      BIGCALCluster * aCluster;
      if (fEvents->CLUSTER) {
         //std::cout<< "Cluster event... sweet... "<< fEvents->CLUSTER->fNClusters << " addr:" << fEvents->CLUSTER <<"\n";

         for (int kk = 0; kk < fEvents->CLUSTER->fNClusters  ; kk++) {
            aCluster = (BIGCALCluster*)(*fEvents->CLUSTER->fClusters)[kk] ;
            if (aCluster->fGoodCherenkovTDCHit)
               if (aCluster->fCherenkovBestNPESum > 3.0) {

                  fCleanedEvent->fNPossibleClusters++;
                  fcGoodElectron = true;
                  /*         std::cout<< "made it here\n";*/
                  (fCleanedEvent->fCluster) = (*aCluster);
               }
         }
      } // fClusterEvent
   } // end of trigger selection
   return(0);
}

//__________________________________________________________________________


//__________________________________________________________________________

ClassImp(SANEElasticElectronSelector)

Int_t SANEElasticElectronSelector::Calculate()
{

   fcGoodElectron = false;

   if (!(fEvents->TRIG->IsCoincidenceEvent()))  return(0);

   fcGoodElectron = true;

   return(0);
}

//__________________________________________________________________________

