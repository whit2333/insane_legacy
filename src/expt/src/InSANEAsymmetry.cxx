#include "InSANEAsymmetry.h"
#include "SANERunManager.h"

ClassImp(InSANEExpParameter)
//_____________________________________________________________________________
InSANEExpParameter::InSANEExpParameter(const char * n) {
      fParName = n;
      fMinimum = 0.0;
      fMaximum = 1.0;
      fGroup   = 0;
      fTreeVariable = "";
}
//_____________________________________________________________________________

InSANEExpParameter::~InSANEExpParameter() {
}
//_____________________________________________________________________________

   const char * InSANEExpParameter::PrintTable() {
      TString res = Form("%4.5f %4.5f %4.5f", fMinimum, fMaximum, (fMinimum + fMaximum) / 2.0);
      return(res.Data());
   }
//_____________________________________________________________________________

   const char * InSANEExpParameter::PrintTableHead() {
      TString res = Form("%s_min %s_max %s_avg", GetName(), GetName(), GetName());
      return(res.Data());
   }
//_____________________________________________________________________________

   const char * InSANEExpParameter::GetCut() {
      TString res = Form("%f>%s&&%f", fMinimum, GetName(), fMaximum);
      return(res.Data());
   }
//_____________________________________________________________________________


//_____________________________________________________________________________

ClassImp(InSANEAsymmetry)
//_____________________________________________________________________________

InSANEAsymmetry::InSANEAsymmetry(Int_t n): fNumber(n)
{
   SetNameTitle(Form("asym-%d", n), Form("asym %d", n));
   fNumber = n;
   /*fHelicityBit=0;
   fQsquared = 0;
   fxBjorken = 0;
   fScatteredElectronEnergy=0;
   fScatteredElectronTheta=0;
   fScatteredElectronPhi=0;*/
   /*      fDilutionFunc = 0;*/
   fDISEvent = nullptr;
   fCount[0] = 0;
   fCount[1] = 0;
   fValue[0] = 0;
   fValue[1] = 0;
   fNAdded = 0;
   fGroup = 0;
   

}
//_____________________________________________________________________________

InSANEAsymmetry::InSANEAsymmetry(const InSANEAsymmetry& rhs) : TNamed(rhs) {
  (*this) = rhs;
}
//_____________________________________________________________________________

InSANEAsymmetry& InSANEAsymmetry::operator=(const InSANEAsymmetry &rhs)
{
  if (this == &rhs){      // Same object?
    return *this;        // Yes, so skip assignment, and just return *this.
  }
  TNamed::operator=(rhs);
  fGroup     = rhs.fGroup;
  fNumber    = rhs.fNumber;
  fNAdded    = rhs.fNAdded;
  fCount[0]  = rhs.fCount[0];
  fCount[1]  = rhs.fCount[1];
  fValue[0]  = rhs.fValue[0];
  fValue[1]  = rhs.fValue[1];
  return *this;
}
//_____________________________________________________________________________

InSANEAsymmetry & InSANEAsymmetry::operator+=(const InSANEAsymmetry &rhs) {
  if (fNumber != rhs.fNumber) std::cout << "Warning Asymmetry numbers don't match using operator += \n";
      //std::cout << fNumber << " vs " << rhs.fNumber << std::endl;
      //fNumber    = rhs.fNumber;
      fNAdded++;
      fNAdded    +=  rhs.fNAdded;
      // Add counts and charge normalized counts
      fCount[0]  += rhs.fCount[0];
      fCount[1]  += rhs.fCount[1];
      fValue[0]  += rhs.fValue[0];
      fValue[1]  += rhs.fValue[1];
      return *this;
   }
//_____________________________________________________________________________

const InSANEAsymmetry InSANEAsymmetry::operator+(const InSANEAsymmetry &other) const
{
  InSANEAsymmetry result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
  result += other;            // Use += to add other to the copy.
  return result;              // All done!
}
//_____________________________________________________________________________

Bool_t InSANEAsymmetry::IsInGroup(Int_t n) const {
   // fGroup = 0 means all groups
   // fGroup > 0 means only fGroup is checked
   // fGroup = -1 means the vector fGroups is searched.
   if(fGroup == 0  ) return true;
   if(fGroup == n  ) return true;
   if(fGroup == -1 ) {
      Int_t c = std::count(fGroups.begin(),fGroups.end(),n);
      if(c>0) return true;
   }
   return false;
}
//_____________________________________________________________________________
Bool_t InSANEAsymmetry::IsInGroupList(const std::vector<int>& groups) const {
   // passed a vector of groups, if it matches any of the groups associated with the asymmetry
   // is considered in the group and returns true.
   if(fGroup != -1 ) return false;
   for(auto it = groups.begin() ; it != groups.end(); ++it) {
      int n = *it;
      Int_t c = std::count(fGroups.begin(),fGroups.end(),n);
      if(c>0) return true;
   }
   return false;
}
//_____________________________________________________________________________
Int_t InSANEAsymmetry::CountEvent(const std::vector<int> & groups){
   if (fDISEvent) if (IsGoodEvent()) if (IsInGroupList(groups)) {
         if ((fDISEvent->fHelicity) == 1) fCount[0]++;
         if ((fDISEvent->fHelicity) == -1) fCount[1]++;
         return(1);
      }
   /* else */
   return(0);
}
//_____________________________________________________________________________
Int_t InSANEAsymmetry::CountEvent()
{
   if (fDISEvent) if (IsGoodEvent()) {
         if ((fDISEvent->fHelicity) == 1) fCount[0]++;
         if ((fDISEvent->fHelicity) == -1) fCount[1]++;
         return(1);
      }
   /* else */
   return(0);
}
//_____________________________________________________________________________

void InSANEAsymmetry::Print(Option_t * ) const {
   int num = (fCount[0] - fCount[1]) ;
   int den = (fCount[0] + fCount[1]) ;
   std::cout << "A(" << fNumber << ") = \t";
   std::cout << num << "\t/" << den  << "\t=\t" << double(num)/double(den) ;
   std::cout << std::endl;

}
//_____________________________________________________________________________


