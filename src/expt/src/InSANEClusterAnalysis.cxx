#define InSANEClusterAnalysis_cxx
#include "SANEEvents.h"

#include "InSANEClusterAnalysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "BETAEvent.h"
#include "GasCherenkovHit.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "TClonesArray.h"

ClassImp(InSANEClusterAnalysis)

//__________________________________________________
InSANEClusterAnalysis::InSANEClusterAnalysis(const char * sourceTreeName)  :
   InSANEAnalysis(sourceTreeName)
{
   // DEBUG LEVEL
   fDebug = 3;

   // These are the trees used in analysis
   fClusterTree  = nullptr;
   fOutputTree   = nullptr;
   fAnalysisTree = nullptr;

/// Creates the cluster event object and cluster array
   if (!fEvents->CLUSTER) fClusterEvent = fEvents->AddClusterEvents();
   else fClusterEvent = fEvents->CLUSTER;
   fClusters = fClusterEvent->fClusters;

   auto * runManager = (SANERunManager*) SANERunManager::GetRunManager();
   fRunNumber = runManager->GetCurrentRun()->GetRunNumber();

   fAnalysisFile = runManager->GetCurrentFile();
   if (fAnalysisFile) fAnalysisFile->cd();

   AllocateHistograms();

}
//_____________________________________________________________________________

InSANEClusterAnalysis::~InSANEClusterAnalysis()
{
   std::cout << " o InSANEClusterAnalysis Destructor.\n";

   //   delete fTrackerDetector;
//   delete fCherenkovDetector;
//   delete fHodoscopeDetector;
//   delete fBigcalDetector;

}
//_____________________________________________________________________________

Int_t InSANEClusterAnalysis::SetClusterTree(TTree * t)
{

   if (!t) {
      printf("Null tree! Aborting \n ");
      return(-1);
   }

   fClusterTree = t;
   TBranch * aBranch = nullptr;

   aBranch = fClusterTree->GetBranch("bigcalClusterEvent");
   if (aBranch) fClusterTree->SetBranchAddress("bigcalClusterEvent", &fClusterEvent);
   else {
      std::cout << " + Creating Cluster Event Branch \n" ;
      fClusterTree->Branch("bigcalClusterEvent", "InSANEClusterEvent", &fClusterEvent);
   }

   aBranch = fClusterTree->GetBranch("bigcalClusters");
   if (aBranch) fClusterTree->SetBranchAddress("bigcalClusters", &fClusters);
   else {
      std::cout << " + Creating Clusters TClonesArray Branch\n" ;
      fClusterTree->Branch("bigcalClusters", &fClusters);
   }
   fClusterTree->SetAutoSave();

   return(0);
}
//_______________________________________________________//

Int_t InSANEClusterAnalysis::AnalyzeRun()
{
// Detectors used
// cout << " Creating detector configurations for this run... \n";
//
// ///// EVENT LOOP  //////
//   Int_t nevent = fEvents->fTree->GetEntries();
//   cout << "\n" << nevent << " ENTRIES \n";
//   TClonesArray * cerHits = gcEvent->fHits;
//   TClonesArray * lucHits = lhEvent->fHits;
//   TClonesArray * bigcalHits = bcEvent->fHits;
//   TClonesArray * trackerHits = ftEvent->fHits;
//
//   for (int iEVENT=0;iEVENT<nevent;iEVENT++)     {
//     fEvents->GetEvent(iEVENT);
//
// //    if( fEvents->TRIG->gen_event_trigtype[3] ) { // BETA2
//       // Loop over lucite hits
// //      for(int kk=0; kk< lhEvent->fNumberOfHits;kk++) {
// //      aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;
// //do something
// //      } // End loop over Lucite Hits
// //    } // beta trigger bit
//         aBetaEvent->ClearEvent();
//   } // END OF EVENT LOOP
   return(0);
}
//__________________________________________________

Int_t InSANEClusterAnalysis::Visualize()
{
// c1->cd(1);
// fOutputTree->Draw("hms_p:fBigcalEvent.fTotalEnergyDeposited","hms_p<4.0","colz");
//
// c1->cd(2);
// fOutputTree->Draw("hms_p:fBigcalEvent.fTotalEnergyDeposited","hms_p<4.0&&fGasCherenkovEvent.fNumberOfHits>2","colz");
//
// c1->cd(4);
// fOutputTree->Draw("hms_theta:fBigcalEvent.fTotalEnergyDeposited","hms_p<4.0","colz");
//
// c1->cd(5);
// fOutputTree->Draw("hms_theta:fBigcalEvent.fTotalEnergyDeposited","hms_p<4.0&&fGasCherenkovEvent.fNumberOfHits>2","colz");
   return(0);
}

