#include "BIGCALGeometryCalculator.h"
#include <TObject.h>
#include <iostream>

//______________________________________________________________________________
ClassImp(BIGCALGeometryCalculator)

//______________________________________________________________________________
BIGCALGeometryCalculator * BIGCALGeometryCalculator::fgBIGCALGeometryCalculator = nullptr;
//______________________________________________________________________________
BIGCALGeometryCalculator * BIGCALGeometryCalculator::GetCalculator()
{
   if (!fgBIGCALGeometryCalculator)   // Only allow one instance of class to be generated.
      fgBIGCALGeometryCalculator = new BIGCALGeometryCalculator();
   return fgBIGCALGeometryCalculator;
}
//______________________________________________________________________________
BIGCALGeometryCalculator::BIGCALGeometryCalculator() {

   if (LoadCalibrationSeries(1) < 0) SetCalibrationSeries(1);


   bigcalFace             = 335.42; // from target
   fNominalRadialDistance = bigcalFace;
   fNominalAngleSetting   = 40.0; //degrees
   rcsNx              = 30;
   rcsNy              = 24;
   protNx             = 32;
   protNy             = 32;
   rcsCellSize        = 4.02167 ;
   protCellSize       = 3.81 ;
   rcsXSize           = rcsCellSize * rcsNx; // as viewed from the back towards the target, the cells are right justified.
   rcsYSize           = rcsCellSize * rcsNy;
   protXSize          = protCellSize * protNx;
   protYSize          = protCellSize * protNy;
   rcsCellZSize       = 40.0;
   protCellZSize      = 45.0;
   rcsProtYSeparation = protYSize / 2.0 + rcsYSize / 2.0 ;
   rcsProtXSeparation = (protXSize - rcsXSize) / 2.0 ;
   bcVerticalOffset   = (protYSize + rcsYSize) / 2.0 - rcsYSize; // add to overall vertical placement
   bcYSize            = (protYSize + rcsYSize);
   bcXSize            = protXSize ;

   fRotFromBCcoords.SetToIdentity();
   fRotFromBCcoords.RotateY(fNominalAngleSetting * TMath::Pi() / (180.0));

   fTDCRows           = nullptr;
   fTDCSumOf64Groups  = nullptr;

   // For bigcal subedetector analysis
   fNSubDetectors = 6;
   fSubDeltaX     = 9.0;
   fSubDeltaY     = 9.0;

   fX0Sub[0]      = 0.0;
   fY0Sub[0]      = -30.0;

   fX0Sub[1]      = 0.0;
   fY0Sub[1]      = 30.0;

   fX0Sub[2]      = -20.0;
   fY0Sub[2]      = -10.0;

   fX0Sub[3]      = -20.0;
   fY0Sub[3]      = 10.0;

   fX0Sub[4]      = -20.0;
   fY0Sub[4]      = -30.0;

   fX0Sub[5]      = -20.0;
   fY0Sub[5]      = 30.0;

//bcYSize = (protYSize + rcsYSize);

// Cherenkov Mirrors
// evenly divide the mirrors starting in the middle (4, 5, 6 and 7)
// rectangular geometry definitions.... NOT VERY ACCURATE... but good enough for now.
// These defined below will mostly be used for boolean geometry so overlapping isn't
// a major problem, but it would be nice if they were mutually exclusive and covered
// all of bigcal.
// X sizes

   double middleMirrorHeight = 58.0 ;

//Spherical
   cherenkovMirrorXSize[0] = bcXSize / 2.0; //cm
   cherenkovMirrorXSize[2] = bcXSize / 2.0; //cm
   cherenkovMirrorXSize[4] = bcXSize / 2.0; //cm
   cherenkovMirrorXSize[6] = bcXSize / 2.0; //cm
//Toroidal
   cherenkovMirrorXSize[1] = bcXSize / 2.0; //cm
   cherenkovMirrorXSize[3] = bcXSize / 2.0; //cm
   cherenkovMirrorXSize[5] = bcXSize / 2.0; //cm
   cherenkovMirrorXSize[7] = bcXSize / 2.0; //cm

// Y sizes
//Spherical
   cherenkovMirrorYSize[0] = bcYSize / 2.0 - middleMirrorHeight; //cm
   cherenkovMirrorYSize[2] = middleMirrorHeight; //cm
   cherenkovMirrorYSize[4] = middleMirrorHeight; //cm
   cherenkovMirrorYSize[6] = bcYSize / 2.0 - middleMirrorHeight; //cm
//Toroidal
   cherenkovMirrorYSize[1] = bcYSize / 2.0 - middleMirrorHeight; //cm
   cherenkovMirrorYSize[3] = middleMirrorHeight; //cm
   cherenkovMirrorYSize[5] = middleMirrorHeight; //cm
   cherenkovMirrorYSize[7] = bcYSize / 2.0 - middleMirrorHeight; //cm


// Viewing from behind ( down stream ) mirror 0 is in the lower right corner.

// X positions
//Spherical
   cherenkovMirrorPositionX[0] = bcXSize / 4.0; //cm
   cherenkovMirrorPositionX[2] = bcXSize / 4.0; //cm
   cherenkovMirrorPositionX[4] = bcXSize / 4.0; //cm
   cherenkovMirrorPositionX[6] = bcXSize / 4.0; //cm
//Toroidal
   cherenkovMirrorPositionX[1] = -1.0 * bcXSize / 4.0; //cm
   cherenkovMirrorPositionX[3] = -1.0 * bcXSize / 4.0; //cm
   cherenkovMirrorPositionX[5] = -1.0 * bcXSize / 4.0; //cm
   cherenkovMirrorPositionX[7] = -1.0 * bcXSize / 4.0; //cm

// Y positions
//Spherical
   cherenkovMirrorPositionY[0] = -1.0 * (cherenkovMirrorYSize[2] + cherenkovMirrorYSize[0] / 2.0); //cm
   cherenkovMirrorPositionY[2] = -1.0 * cherenkovMirrorYSize[2] / 2.0; //cm
   cherenkovMirrorPositionY[4] = cherenkovMirrorYSize[4] / 2.0; //cm
   cherenkovMirrorPositionY[6] = cherenkovMirrorYSize[4] + cherenkovMirrorYSize[6] / 2.0; //cm
//Toroidal
   cherenkovMirrorPositionY[1] = -1.0 * (cherenkovMirrorYSize[3] + cherenkovMirrorYSize[1] / 2.0); //cm
   cherenkovMirrorPositionY[3] = -1.0 * cherenkovMirrorYSize[3] / 2.0; //cm
   cherenkovMirrorPositionY[5] = cherenkovMirrorYSize[5] / 2.0; //cm
   cherenkovMirrorPositionY[7] = cherenkovMirrorYSize[5] + cherenkovMirrorYSize[7] / 2.0; //cm

}
//______________________________________________________________________________
BIGCALGeometryCalculator::~BIGCALGeometryCalculator()
{
   ;
}
//______________________________________________________________________________
Double_t BIGCALGeometryCalculator::GetCalibrationCoefficient(Int_t i, Int_t j) {
   Int_t blockIndex = GetBlockNumber(i, j)-1;
   if(blockIndex<1744 && blockIndex >= 0) return(bc_cc[blockIndex]);
   return 0.0;
}
//______________________________________________________________________________
Double_t BIGCALGeometryCalculator::GetCalibrationCoefficient(Int_t N) {
   Int_t blockIndex = N-1;
   if(blockIndex<1744 && blockIndex >= 0) { 
      return(bc_cc[blockIndex]);
   } else {
      Error("GetCalibrationCoefficient","Bad block number.");
   }
   return 0.0;
   //return(GetCalibrationCoefficient(GetBlock_i(N), GetBlock_j(N)));
}
//______________________________________________________________________________
void BIGCALGeometryCalculator::SetCalibrationCoefficient(Int_t N, Double_t val) {
   Int_t blockIndex = N-1;
   if(blockIndex<1744 && blockIndex >= 0) { 
      bc_cc[blockIndex] = val ;
   } else {
      Error("SetCalibrationCoefficient","Bad block number.");
   }
}
//______________________________________________________________________________
void BIGCALGeometryCalculator::SetCalibrationCoefficient(Int_t i, Int_t j, Double_t val) {
   Int_t blockIndex = GetBlockNumber(i, j)-1;
   if(blockIndex<1744 && blockIndex >= 0) { 
      bc_cc[blockIndex] = val ;
   } else {
      Error("SetCalibrationCoefficient","Bad block number.");
   }
}
//______________________________________________________________________________
Int_t    BIGCALGeometryCalculator::GetTDCGroup(Int_t i, Int_t j) {
   /*   std::cout << "GetTDCGroup ( " << i << "," << j << ")\n";*/
   if (j > 32 && j < 57 && i > 0 && i < 31) { // RCS
      return((i - 1) / 8 + 1);
   } else if (j < 33 && j > 0 && i > 0 && i < 33) { //Protvino
      return((i - 1) / 8 + 1);
   } else {
      Error("GetTDCGroup(i,j)", "Cell ( %d , %d ) does not exist!", i, j);
      return(-1);
   }
}
//______________________________________________________________________________
Int_t*   BIGCALGeometryCalculator::GetTDCRow(Int_t i, Int_t j)
{
   fTDCGroupRowRes[0] = (j - 1) / 3 + 1;
   fTDCGroupRowRes[1] = -1 ;
   if ((j - 1) % 3 == 0 && j != 1) { // Two valuess
      fTDCGroupRowRes[1] = fTDCGroupRowRes[0] ;
      fTDCGroupRowRes[0] = fTDCGroupRowRes[0] - 1 ;
   }
   if (j > 32 && j < 57 && i > 0 && i < 31) { // RCS
      return(fTDCGroupRowRes);
   } else if (j < 33 && j > 0 && i > 0 && i < 33) { //Protvino
      return(fTDCGroupRowRes);
   } else {
      Error("GetTDCRow(i,j)", "Cell ( %d , %d ) does not exist!", i, j);
      return(fTDCGroupRowRes);
   }
}
//______________________________________________________________________________
std::vector<Int_t> & BIGCALGeometryCalculator::GetTDCRows(const Int_t i, const  Int_t j)
{
   if (!fTDCRows) fTDCRows = new std::vector<Int_t>;
   fTDCRows->clear();
   Int_t * fRows = GetTDCRow(i, j);
   if (fRows[1] == -1) fTDCRows->push_back(fRows[0]);
   else {
      fTDCRows->push_back(fRows[0]);
      fTDCRows->push_back(fRows[1]);
   }
   return((*fTDCRows));
}
//______________________________________________________________________________
std::vector<Int_t> BIGCALGeometryCalculator::GetSumOf64GroupNumber(Int_t i, Int_t j) {
   std::vector<Int_t> result;// = new Int_t [2];
   result.push_back((j-1)/3 + 1);
   result.push_back(-1);
   if (j > 32 && j < 57 && i > 0 && i < 31) { // RCS
      result[0] = result[0] + (i > 15) * 19;
      if(result[0] < 38 )
         if ((j - 1) % 3 == 0 && j != 1 && j != 56 ) result[1] = result[0] - 1 ; // Two values due to overlap
      //result[1] = result[1] + (i > 15) * 19;
      return(result);
   } else if (j < 33 && j > 0 && i > 0 && i < 33) { //Protvino
      result[0] = result[0] + (i > 16) * 19;
      if(result[0] < 38 )
         if ((j - 1) % 3 == 0 && j != 1 && j != 56 ) result[1] = result[0] - 1 ; // Two values due to overlap
      //result[1] = result[1] + (i > 16) * 19;
      return(result);
   } else {
      printf(" Warning : BIGCALGeometryCalculator::GetSumOf64Group(i,j)  \n");
      printf("           Cell ( %d , %d ) does not exist!  \n", i, j);
      return(result);
   }
}
//______________________________________________________________________________
std::vector<Int_t> & BIGCALGeometryCalculator::GetSumOf64GroupNumbers(const Int_t i, const  Int_t j) {
   if (!fTDCSumOf64Groups) fTDCSumOf64Groups = new std::vector<Int_t>;
   fTDCSumOf64Groups->clear();
   std::vector<Int_t> fRows = GetSumOf64GroupNumber(i, j);
   if (fRows[1] == -1) fTDCSumOf64Groups->push_back(fRows[0]);
   else {
      fTDCSumOf64Groups->push_back(fRows[0]);
      fTDCSumOf64Groups->push_back(fRows[1]);
   }
   return((*fTDCRows));
}
//______________________________________________________________________________
Int_t BIGCALGeometryCalculator::GetTriggerSumGroup(Int_t block_number) const {
   Int_t i = GetBlock_i(block_number);
   Int_t j = GetBlock_j(block_number);
   if(j>32){ // RCS
      if(i>15) { 
         return 4;
      } else {
         return 3;
      }
   } else { // Protvino
      if(i>16) { 
         return 2;
      } else {
         return 1;
      }
   }
   Error("GetTriggerSumGroup(block)", "Cell ( %d , %d ) does not exist!", i, j);
   return(1);
}
//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetProtCellX(int i) const
{
   if (i < 1 || i > 32) std::cout << " error in prot x index " << i << "\n";
   return(((double)i) * protCellSize - protXSize / 2.0 - protCellSize / 2.0);
}
//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetProtCellY(int j) const
{
   if (j < 1 || j > 32) std::cout << " error in prot y index " << j << "\n";
   return(((double)j) * protCellSize - protYSize / 2.0 - protCellSize / 2.0);
}
//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetRCSCellX(int i)
{
   if (i < 1 || i > 30) std::cout << " error in rcs x\n";
   return(((double)i) * rcsCellSize - rcsXSize / 2.0 - rcsCellSize / 2.0);

}
//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetRCSCellY(int j)
{
   if (j < 1 || j > 24) std::cout << " error in rcs y\n";
   return(((double)j) * rcsCellSize - rcsYSize / 2.0 - rcsCellSize / 2.0);
}


//______________________________________________________________________________
/**
 * \todo{Using horizontal center of protvino as BC coordinate x=0
 *  even though there is a right justifcation of the blocks
 *  NEED TO INSERT BEST SURVEY DATA HERE}
 */
double   BIGCALGeometryCalculator::GetProtCellX_BCCoords(int i)
{
   if (i < 1 || i > 32) std::cout << " \n error in PROT X cell# : " << i;
   return(((double)i) * protCellSize - protXSize / 2.0 - protCellSize / 2.0);
}

//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetProtCellY_BCCoords(int j)
{
   if (j < 1 || j > 32) std::cout << " \n error in PROT Y cell# : " << j;
   return(((double)j) * protCellSize - protYSize / 2.0 - protCellSize / 2.0 + bcVerticalOffset - protYSize / 2.0);

}

//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetRCSCellX_BCCoords(int i)
{
   if (i < 1 || i > 30) std::cout <<  " \n error in RCS  X cell# : " << i;
   return(((double)i) * rcsCellSize - rcsXSize / 2.0 - rcsCellSize / 2.0 + rcsProtXSeparation);
}

//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetRCSCellY_BCCoords(int j)
{
   if (j < 1 || j > 24) std::cout <<  " \n error in RCS  Y cell# : " << j;
   return(((double)j) * rcsCellSize - rcsYSize / 2.0 - rcsCellSize / 2.0 + bcVerticalOffset + rcsYSize / 2.0);
}

//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetBlockXij_BCCoords(int i, int j)
{
   double result;
   if (j > 32) { // RCS
      result = GetRCSCellX_BCCoords(i);
   } else {   //PROT
      result = GetProtCellX_BCCoords(i);
   }
   return(result);
}

//______________________________________________________________________________
double   BIGCALGeometryCalculator::GetBlockYij_BCCoords(int i, int j)
{
   double result;
   if (j > 32) { // RCS
      result = GetRCSCellY_BCCoords(j - 32);
      if (i > 30) {
         std::cerr << "error BIGCALGeometryCalculator::GetBlockYij_BCCoords" << std::endl;;
      }
   } else { // PROT
      result = GetProtCellY_BCCoords(j);
      if (i > 32) {
         std::cerr << "error BIGCALGeometryCalculator::GetBlockYij_BCCoords" << std::endl;;
      }
   }
   return(result);
}

//______________________________________________________________________________
int  BIGCALGeometryCalculator::GetBlockNumber(int i, int j) const {
   if (j < 33 && j > 0) { // Protvino
      return(((j - 1) * 32) + i);
   } else if (j > 32 && j < 57) { // RCS
      if (i > 30) return(-1);
      else return(32 * 32 + (j - 32 - 1) * 30 + i);
   } else {
      Error("GetBlockNumber(i,j)", "Block (%d,%d) does not exist.", i, j);
      return(-1);
   }
}

//______________________________________________________________________________
int BIGCALGeometryCalculator::GetBlock_i(int block_number) const {
   if (block_number < 1025) { //PROT
      return((block_number - 1) % 32 + 1) ;
   } else if (block_number > 1024) { //RCS
      return((block_number - 1 - 1024) % 30 + 1);
   } else {
      printf(" Warning : BIGCALGeometryCalculator::GetBlock_i(num)  \n");
      printf("           block (%d ) does not exist!  \n", block_number);
      return(-1);
   }
}

//______________________________________________________________________________
int BIGCALGeometryCalculator::GetBlock_j(int block_number) const {
   if (block_number < 1025) { //PROT
      return((block_number - 1) / 32 + 1) ;
   } else if (block_number > 1024) { //RCS
      return(32 + (block_number - 1024 - 1) / 30 + 1);
   } else {
      Error("GetBlock_j", " - block (%d ) does not exist!", block_number);
      return(-1);
   }
}
//______________________________________________________________________________
bool BIGCALGeometryCalculator::IsBlockCalibrated(int i, int j) const {
   // The pi0 calibration has a reach of about 10 Protvino blocks 
   // and 9 RCS blocks.
   if( j <= 32 && j > 0) {
      // Protvino
      // The center of protvino is between block 16 and 17

      if( abs(i-16) <= 10 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 10 ) {
         // in horizontal strip
         return true;
      }
      return false;

   } else if( j > 32 && j <= 56 ) {
      // Protvino
      // The center of protvino is between block 15 and 16

      if( abs(i-15) <= 9 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 9 ) {
         // in horizontal strip
         return true;
      }
      return false;
   }
   Error("IsBlockCalibrated","Should not have reached here.");
   return false;
}
//______________________________________________________________________________
bool BIGCALGeometryCalculator::IsBlockInCross(int i, int j) const {
   // The pi0 calibration has a reach of about 10 Protvino blocks 
   // and 9 RCS blocks.
   if( j <= 32 && j > 0) {
      // Protvino
      // The center of protvino is between block 16 and 17

      if( abs(i-16) <= 10 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 10 ) {
         // in horizontal strip
         return true;
      }
      return false;

   } else if( j > 32 && j <= 56 ) {
      // Protvino
      // The center of protvino is between block 15 and 16

      if( abs(i-15) <= 9 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 9 ) {
         // in horizontal strip
         return true;
      }
      return false;
   }
   Error("IsBlockCalibrated","Should not have reached here.");
   return false;
}
//______________________________________________________________________________
bool BIGCALGeometryCalculator::IsBlockInCross1(int i, int j) const {
   // One block tighter than IsBlockInCross
   // The pi0 calibration has a reach of about 10 Protvino blocks 
   // and 9 RCS blocks.
   if( j <= 32 && j > 0) {
      // Protvino
      // The center of protvino is between block 16 and 17

      if( abs(i-16) <= 10-1 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 10-1) {
         // in horizontal strip
         return true;
      }
      return false;

   } else if( j > 32 && j <= 56 ) {
      // Protvino
      // The center of protvino is between block 15 and 16

      if( abs(i-15) <= 9-1 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 9-1 ) {
         // in horizontal strip
         return true;
      }
      return false;
   }
   Error("IsBlockCalibrated","Should not have reached here.");
   return false;
}
//______________________________________________________________________________
bool BIGCALGeometryCalculator::IsBlockInCross2(int i, int j) const {
   // Two blocks tighter than IsBlockInCross
   // The pi0 calibration has a reach of about 10 Protvino blocks 
   // and 9 RCS blocks.
   if( j <= 32 && j > 0) {
      // Protvino
      // The center of protvino is between block 16 and 17

      if( abs(i-16) <= 10-2 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 10-2) {
         // in horizontal strip
         return true;
      }
      return false;

   } else if( j > 32 && j <= 56 ) {
      // Protvino
      // The center of protvino is between block 15 and 16

      if( abs(i-15) <= 9-2 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 9-2 ) {
         // in horizontal strip
         return true;
      }
      return false;
   }
   Error("IsBlockCalibrated","Should not have reached here.");
   return false;
}
//______________________________________________________________________________
bool BIGCALGeometryCalculator::IsBlockInCross3(int i, int j) const {
   // Two blocks tighter than IsBlockInCross
   // The pi0 calibration has a reach of about 10 Protvino blocks 
   // and 9 RCS blocks.
   if( j <= 32 && j > 0) {
      // Protvino
      // The center of protvino is between block 16 and 17

      if( abs(i-16) <= 10-3 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 10-3) {
         // in horizontal strip
         return true;
      }
      return false;

   } else if( j > 32 && j <= 56 ) {
      // Protvino
      // The center of protvino is between block 15 and 16

      if( abs(i-15) <= 9-3 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 9-3 ) {
         // in horizontal strip
         return true;
      }
      return false;
   }
   Error("IsBlockCalibrated","Should not have reached here.");
   return false;
}
//______________________________________________________________________________
bool BIGCALGeometryCalculator::IsBlockInCross4(int i, int j) const {
   // Two blocks tighter than IsBlockInCross
   // The pi0 calibration has a reach of about 10 Protvino blocks 
   // and 9 RCS blocks.
   if( j <= 32 && j > 0) {
      // Protvino
      // The center of protvino is between block 16 and 17

      if( abs(i-16) <= 10-4 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 10-4) {
         // in horizontal strip
         return true;
      }
      return false;

   } else if( j > 32 && j <= 56 ) {
      // Protvino
      // The center of protvino is between block 15 and 16

      if( abs(i-15) <= 9-4 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 9-4 ) {
         // in horizontal strip
         return true;
      }
      return false;
   }
   Error("IsBlockCalibrated","Should not have reached here.");
   return false;
}
//______________________________________________________________________________
bool BIGCALGeometryCalculator::IsBlockInCross5(int i, int j) const {
   // Two blocks tighter than IsBlockInCross
   // The pi0 calibration has a reach of about 10 Protvino blocks 
   // and 9 RCS blocks.
   if( j <= 32 && j > 0) {
      // Protvino
      // The center of protvino is between block 16 and 17

      if( abs(i-16) <= 10-5 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 10-5) {
         // in horizontal strip
         return true;
      }
      return false;

   } else if( j > 32 && j <= 56 ) {
      // Protvino
      // The center of protvino is between block 15 and 16

      if( abs(i-15) <= 9-5 ) {
         // in the vertical strip
         return true; 
      } 
      if( abs(j-32) <= 9-5 ) {
         // in horizontal strip
         return true;
      }
      return false;
   }
   Error("IsBlockCalibrated","Should not have reached here.");
   return false;
}
//______________________________________________________________________________

bool BIGCALGeometryCalculator::IsBlockOnPerimeter(const int i, const int j) const {
   if (j == 1) return true;
   if (j == 56) return true;
   if (i == 1) return true;
   if (j > 1 && j < 33) {
      if (i == 32) return true;
   }
   if (j > 32 && j < 57) {
      if (i == 30) return true;
   }
   /* else */
   return(false);
}

//______________________________________________________________________________
void BIGCALGeometryCalculator::PrintCalibrationCoefficients()
{
   for (int j = 56; j > 32; j--) {
      for (int i = 1; i <= 30; i++) {
         printf("%2.1f ", GetCalibrationCoefficient(i, j));
      }
      printf("\n");
   }
   for (int j = 32; j > 0; j--) {
      for (int i = 1; i <= 32; i++) {
         printf("%2.1f ", GetCalibrationCoefficient(i, j));
      }
      printf("\n");
   }
}
//______________________________________________________________________________
Int_t    BIGCALGeometryCalculator::GetTriggerGroup(Int_t i, Int_t j)
{
   if (j > 32 && j < 57 && i > 0 && i < 31) { // RCS
      return((i - 1) / 15 + 1 + 2);
   } else if (j < 33 && j > 0 && i > 0 && i < 33) { //Protvino
      return((i - 1) / 16 + 1);
   } else {
      Error("GetTriggerGroup(i,j)", "Cell ( %d , %d ) does not exist!", i, j);
      return(-1);
   }
}
//______________________________________________________________________________
Int_t BIGCALGeometryCalculator::GetLatestCalibrationSeries(Int_t run) const {
   Int_t series = 0;

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TSQLServer * db               = dbman->GetMySQLConnection();
   TString SQLCOMMAND = "";
   if( run == -1 ) SQLCOMMAND = "SELECT series FROM bigcal_calibrations WHERE block_number=1 order by recorded desc LIMIT 1 ; ";
   else SQLCOMMAND = Form("SELECT series FROM bigcal_calibrations WHERE block_number=1 AND end_run>=%d AND start_run<=%d order by recorded desc LIMIT 1 ; ", run,run);
   TSQLRow* row = nullptr;
   TSQLResult * res = nullptr;
   res = db->Query(SQLCOMMAND.Data());
   int nrows = res->GetRowCount();
   if(nrows == 0) {
      std::cerr << " Unable to find calibration series for run " << run << ". " << std::endl;
      delete res;
      return 1;
   }
   int nfields = res->GetFieldCount();
   for (int i = 0; i < nrows; i++) {
      row = res->Next();
      series = atoi(row->GetField(0));
      //bc_cc[TString(row->GetField(0)).Atoi() -1] = TString(  row->GetField(1)  ).Atof();
      delete row;
   }
   delete res;

   return series;
}
//______________________________________________________________________________
void BIGCALGeometryCalculator::SetCalibrationSeries(Int_t j) {
   //std::cout << " o BIGCALGeometryCalculator::SetCalibrationSeries(" << j << ")" << std::endl;;

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TSQLServer * db               = dbman->GetMySQLConnection();
   fCalibrationSeries            = j;

   TString SQLCOMMAND = Form("SELECT block_number,coeff FROM bigcal_calibrations WHERE series=%d ; ", fCalibrationSeries);
   TSQLRow* row = nullptr;
   TSQLResult * res = nullptr;
   res = db->Query(Form("SELECT block_number,coeff FROM bigcal_calibrations WHERE series=%d ; ",fCalibrationSeries));
   int nrows = res->GetRowCount();
   //int nfields = res->GetFieldCount();
   for (int i = 0; i < nrows; i++) {
      row = res->Next();
      bc_cc[TString(row->GetField(0)).Atoi() -1] = TString(  row->GetField(1)  ).Atof();
      delete row;
   }
   delete res;
}
//______________________________________________________________________________
int BIGCALGeometryCalculator::SetCalibration_callback(void * theGeoCalc, int argc, char **argv, char **azColName) {
   //int i;
   //std::cout << "Number of args= " << argc << std::endl;
   //for(i=0; i<argc; i++)
   //{
   //   std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
   //}
   //std::cout << std::endl;
   auto * geocalc = (BIGCALGeometryCalculator*) theGeoCalc;
   if (argc == 2) { // should equal 2!
      geocalc->bc_cc[atoi(argv[0])] = atof(argv[1]);
   } else {
      std::cout << " x argc does not equal 2! it is " << argc << "\n";
   }
   return 0;
}
//______________________________________________________________________________

Int_t BIGCALGeometryCalculator::LoadCalibrationSeries(Int_t j) {
   //std::cout << " o BIGCALGeometryCalculator::LoadCalibrationSeries(" << j << ")" << std::cout;
   fCalibrationSeries                = j;
   SetCalibrationSeries(j);

   //InSANEDetectorCalibration * calib = 0;
   //InSANECalibration * chanCalib     = 0;
   //TParameter<double>  * aPar        = 0;

   //TFile * f = new TFile("data/BigCalCalibrations.root", "READ");
   //if (f->IsZombie()) {
   //   Error("LoadCalibrationSeries", "Could not open file, data/BigCalCalibrations.root");
   //   return(-1);
   //}
   //calib = (InSANEDetectorCalibration*)gROOT->FindObject(Form("BigCal-Calibration-%d", fCalibrationSeries));
   //if (!calib)  {
   //   Error("LoadCalibrationSeries", "Could read InSANEDetectorCalibration");
   //   return(-2);
   //}

   //        if(calib)calib->Print();*/
   //for (int i = 0; i < 1744; i++) {
   //   chanCalib = (InSANECalibration*)calib->fCalibrations.At(i);
   //   /*            if(chanCalib)chanCalib->Dump();*/
   //   aPar = chanCalib->GetParameter(0);
   //   bc_cc[i] = aPar->GetVal();
   //}

   //f->Close();
   return(0);
}
//______________________________________________________________________________
Double_t BIGCALGeometryCalculator::GetCalibration_SeriesBlock(Int_t series, Int_t block) const {
   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TSQLServer * db               = dbman->GetMySQLConnection();

   TString SQLCOMMAND = Form("SELECT coeff FROM bigcal_calibrations WHERE series=%d AND block_number=%d; ", series,block);
   TSQLRow* row       = nullptr;
   TSQLResult * res   = nullptr;
   res                = db->Query(SQLCOMMAND.Data());
   int nrows          = res->GetRowCount();
   Double_t  coeff    = 1.0;
   if(nrows > 0) {
      row = res->Next();
      coeff = TString(row->GetField(0)).Atof();
      delete row;
   } else {
      Error("GetCalibration_SeriesBlock","could not find series-block combination in database");
   }
   delete res;
   return coeff;
}
//______________________________________________________________________________
Int_t BIGCALGeometryCalculator::CreateNewCalibrationSeries(Int_t start_run, Int_t end_run){
   InSANEDatabaseManager * dbman      = InSANEDatabaseManager::GetManager();
   TSQLServer * db                    = dbman->GetMySQLConnection();
   Int_t series                       = GetLatestCalibrationSeries();
   series++;
   TString SQLCOMMAND = "";
   Double_t C_new     = 1.0; 
   for(int i = 1; i<=1744; i++){
      C_new      = GetCalibrationCoefficient(i);
      SQLCOMMAND = "INSERT INTO bigcal_calibrations (block_number,coeff,series,start_run,end_run) VALUES (";
      SQLCOMMAND += i;
      SQLCOMMAND += ",";
      SQLCOMMAND += C_new;
      SQLCOMMAND += ",";
      SQLCOMMAND += series;
      SQLCOMMAND += ",";
      SQLCOMMAND += start_run;
      SQLCOMMAND += ",";
      SQLCOMMAND += end_run;
      SQLCOMMAND += ");";
      db->Query(SQLCOMMAND.Data());
   }
   std::cout << " Created new series: " << series << std::endl;
   return series;
}


//______________________________________________________________________________

//______________________________________________________________________________

