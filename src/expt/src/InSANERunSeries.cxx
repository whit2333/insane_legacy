#include "InSANERunSeries.h"

/*
    /// see mathematica notebook on errors assuming 5% error on beam and target.... for now
    statErrors.push_back(
    TMath::Sqrt(((0.0025000000000000005*TMath::Power(N1,3) -
         0.0025000000000000005*TMath::Power(N1,2)*N2 -
         0.0025000000000000005*N1*TMath::Power(N2,2) +
         0.0025000000000000005*TMath::Power(N2,3))*TMath::Power(Pb,2) +
      (0.0025000000000000005*TMath::Power(N1,3) -
         0.0025000000000000005*TMath::Power(N1,2)*N2 +
         0.0025000000000000005*TMath::Power(N2,3) +
         N1*N2*(-0.0025000000000000005*N2 + 4.*TMath::Power(Pb,2)))*
       TMath::Power(Pt,2))/(TMath::Power(N1 + N2,3)*TMath::Power(Pb,4)*TMath::Power(Pt,4))) );

       pbptProducts.push_back(Pb*Pt/10.0);
       numberOfEventsInRun.push_back(aRun->fNBeta2Events);
       physicsAsymmetries.push_back(( double)aRun->fTotalAsymmetry /(Pb*Pt));
      fNDataPoints++;
*/

//___________________________________________________________________//
ClassImp(InSANERunSeriesObject)

//___________________________________________________________________//
ClassImp(InSANERunQuantity)

InSANERunQuantity::InSANERunQuantity(const char * name, const char * title) :
   TNamed(name, title)
{
   fRun=nullptr;
   fGraph = nullptr;
   fValue = 0;
   fNDataPoints = 0;
   fiPoint = 0;
   fMarkerStyle = 21;
   fMarkerColor = 4;
   fMarkerSize = 1.3;
}

void InSANERunQuantity::FillGraph(InSANERunSeriesObject * runObj)
{
   auto xvalue = (Double_t)fiPoint;
   if (runObj) xvalue = runObj->fXValue;
   if (IsValidRun()) {
      fGraph->SetPoint(fiPoint, xvalue, GetQuantity());
      /*         fGraph->GetXaxis()->SetBinLabel(fiPoint,Form("%d",fRun->fRunNumber));*/
      fiPoint++;
   }
}

TGraph * InSANERunQuantity::CreateGraph()
{
   if (!fNDataPoints) printf(" CreateGraph() called with 0 data points!");
   fGraph = new TGraph(fNDataPoints);
   fGraph->SetMarkerColor(fMarkerColor);
   fGraph->SetMarkerStyle(fMarkerStyle);
   fGraph->SetMarkerSize(fMarkerSize);
   fGraph->SetNameTitle(GetName(), GetTitle());
   return(fGraph);
}

//___________________________________________________________________//
ClassImp(InSANERunSeries)

//___________________________________________________________________//

InSANERunSeries::InSANERunSeries(const char * legtext)
{
   fLegendName = legtext;
   fLegend = new TLegend(0.85, 0.7, 0.97, 0.9);
   fLegend->SetHeader(legtext);
   fMarkerStyle = 20;
   fMarkerColor = 2;
   fNumberOfRuns = 0;
   fNumberOfQuantities = 0;
   fRunList.Clear();
   fRunQuatities.Clear();
}
//___________________________________________________________________//

Int_t InSANERunSeries::AddRun(InSANERun * aRun)
{
   fNumberOfRuns++;
   if (aRun) fRunList.Add(new InSANERunSeriesObject(fNumberOfRuns, aRun)) ;
   else return(-1);

   return(0);
}
//___________________________________________________________________//

TList * InSANERunSeries::GenerateGraphs()
{
   InSANERunSeriesObject * runObject = nullptr;
   InSANERun * aRun = nullptr;
   InSANERunQuantity * aQuantity = nullptr;
   Int_t N = fRunList.GetEntries();

   /// Loop over each Run and create the graph and add it to the multigraph
   for (int i = 0; i < N; i++) {
      runObject = (InSANERunSeriesObject*)fRunList.At(i);
      if (runObject) aRun = runObject->GetRun();
      if (aRun)  for (int j = 0; j < fRunQuatities.GetEntries(); j++) {
            aQuantity = (InSANERunQuantity*)fRunQuatities.At(j);
            aQuantity->SetRun(aRun);
            /// Count number of valid runs for each quantity
            aQuantity->CountRun();
         }
   }

   /// Create the graph for each quantity and add it to the multigraph
   for (int j = 0; j < fRunQuatities.GetEntries(); j++) {
      aQuantity = (InSANERunQuantity*)fRunQuatities.At(j);
      TGraph * g = aQuantity->CreateGraph();
      if (g) printf("created graph!\n");
   }


   /// Loop over each Run (in a sorted list)
   for (int i = 0; i < N; i++) {
      runObject = (InSANERunSeriesObject*)fRunList.At(i);
      if (runObject) aRun = runObject->GetRun();
      if (aRun) {
         /// Loop over each quantity to calculate values and fill graph
         for (int j = 0; j < fRunQuatities.GetEntries(); j++) {
            aQuantity = (InSANERunQuantity*)fRunQuatities.At(j);
            aQuantity->SetRun(aRun);
            aQuantity->FillGraph(runObject);
         }
      }
   }

   /// Create the graph for each quantity and add it to the multigraph
   for (int j = 0; j < fRunQuatities.GetEntries(); j++) {
      aQuantity = (InSANERunQuantity*)fRunQuatities.At(j);
      TGraph * g = aQuantity->GetGraph();
      fLegend->AddEntry(g, aQuantity->GetTitle(), "ep");
      fMultiGraph.Add(g, "p");
   }

   return(fMultiGraph.GetListOfGraphs());
}

Int_t InSANERunSeries::Draw()
{
   //fCanvas = new TCanvas("runSeries", "Run Series", 1200, 400);
   fMultiGraph.Draw("a");
   TAxis *ax = fMultiGraph.GetHistogram()->GetXaxis();
   //Double_t x1 = ax->GetBinUpEdge(1);
   //Double_t x2 = ax->GetBinLowEdge(ax->GetNbins());
   if(fNumberOfRuns>0)fMultiGraph.GetHistogram()->GetXaxis()->Set(fNumberOfRuns, 0.5 , (double)fNumberOfRuns+0.5);

   InSANERunSeriesObject * runObject = nullptr;
   InSANERun * aRun = nullptr;
   for (int i = 0; i < fRunList.GetEntries(); i++) {
      runObject = (InSANERunSeriesObject*)fRunList.At(i);
      if (runObject) aRun = runObject->GetRun();
      if (aRun) {
         /* for(int b = 0; b< fMultiGraph.GetXaxis->SetNbins(); b++) */
         fMultiGraph.GetHistogram()->GetXaxis()->SetBinLabel(i+1 , Form("%d", aRun->fRunNumber));
      }
   }
   fMultiGraph.GetXaxis()->LabelsOption("v");
   fMultiGraph.Draw("a");

   if (fLegend) fLegend->Draw();
   return 0;
}

//___________________________________________________________________//

