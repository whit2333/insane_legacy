#include  "InSANEDetectorPedestal.h"
//#include "BIGCALGeometryCalculator.h"



//______________________________________________________________________________
ClassImp(InSANEDetectorPedestal)
//______________________________________________________________________________
InSANEDetectorPedestal::InSANEDetectorPedestal()
{
   fPedestalMean = 0.0;
   fPedestalWidth = 0.0;
   fPedestalId = -1;
}
//______________________________________________________________________________
InSANEDetectorPedestal::InSANEDetectorPedestal(Int_t id, Double_t val, Double_t width) {
   fPedestalMean = val;
   fPedestalWidth = width;
   fPedestalId = id;
}
//______________________________________________________________________________
InSANEDetectorPedestal::~InSANEDetectorPedestal(){
}
//______________________________________________________________________________
void InSANEDetectorPedestal::Print(Option_t * ) const {
   PrintPedestal();
}
//______________________________________________________________________________
void InSANEDetectorPedestal::PrintPedestalHead() const {
   printf("%5s %7s %7s \n", "Id", "Mean", "Width");
}
//______________________________________________________________________________
void InSANEDetectorPedestal::PrintPedestal() const {
   printf("%5d %5.2f %5.2f\n", fPedestalId, fPedestalMean, fPedestalWidth);
}
//______________________________________________________________________________
TString * InSANEDetectorPedestal::GetSQLInsertString(const char * det, Int_t run_number)
{
   TString SQLCOMMAND(" ");
   //   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   SQLCOMMAND += "REPLACE INTO detector_pedestals ( ";
   SQLCOMMAND += " `run_number`";
   SQLCOMMAND += ",`detector`";
   SQLCOMMAND += ",`ped_number`";
   SQLCOMMAND += ",`ped_mean`";
   SQLCOMMAND += ",`ped_width` ";
   SQLCOMMAND += " ) VALUES ( ";
   SQLCOMMAND += run_number;
   SQLCOMMAND += ", '";
   SQLCOMMAND += det;
   SQLCOMMAND += "',";
   SQLCOMMAND += fPedestalId;
   SQLCOMMAND += ",";
   SQLCOMMAND += fPedestalMean;
   SQLCOMMAND += ",";
   SQLCOMMAND += fPedestalWidth;
   SQLCOMMAND += " )  ;";
   return(new TString(SQLCOMMAND.Data()));
}
//______________________________________________________________________________
Int_t InSANEDetectorPedestal::GetPedestal(const char * det, Int_t run_number, Int_t id) {

   // Returns 0 on success and -1 if it didn't find any existing data.
   fPedestalId = id;
   InSANEDatabaseManager * dbManager = InSANEDatabaseManager::GetManager();
   
   //TString SQLCOMMAND(
   //   Form("SELECT ped_mean,ped_width FROM %s.detector_pedestals where run_number=",dbManager->GetDatabaseName()) );
   TString SQLCOMMAND = "SELECT ped_mean,ped_width FROM detector_pedestals where run_number=";
   SQLCOMMAND += run_number;
   SQLCOMMAND += " AND `detector`='";
   SQLCOMMAND += det;
   SQLCOMMAND += "' AND `ped_number`=";
   SQLCOMMAND += id;
   SQLCOMMAND += "  LIMIT 1 ;";


   // Old : don't use SQLite3
   if( dbManager->GetDBType() == InSANEDatabaseManager::kSQLite3 ) {
      Error("GetTiming","SQLite3 no longer supported");
      //char *zErrMsg = 0;
      //int rc = sqlite3_exec(dbManager->GetSQLite3Connection(),
      //      SQLCOMMAND.Data(),
      //      InSANEDetectorPedestal::SetMeanWidth_callback, (void*)this, &zErrMsg);
      //if (rc != SQLITE_OK) {
      //   std::cerr << "SQL error: " << zErrMsg << std::endl;
      //   std::cerr << "fullcommand: " << SQLCOMMAND.Data()  << "\n";
      //   sqlite3_free(zErrMsg);
      //}
   }

   if( dbManager->GetDBType() == InSANEDatabaseManager::kMySQL ||
         dbManager->GetDBType() == InSANEDatabaseManager::kBoth ) {
      TSQLServer * db  = dbManager->GetMySQLConnection();
      TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
      TSQLRow    * row = nullptr;
      if(res) row = res->Next();
      if(row) {
         fPedestalMean  = atof(row->GetField(0));
         fPedestalWidth = atof(row->GetField(1));
         fPedestalId    = id;
         delete row;
         delete res;
         return(0);
      } else {
         return(-1);
      }
   }
   return(-1);
}
//______________________________________________________________________________
int InSANEDetectorPedestal::SetMeanWidth_callback(void * thePed, int argc, char **argv, char **azColName) {
   //int i;
   //std::cout << "Number of args= " << argc << std::endl;
   //for(i=0; i<argc; i++)
   //{
   //   std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
   //}
   //std::cout << std::endl;
   auto * ped = (InSANEDetectorPedestal*) thePed;
   if (argc == 2) { // should equal 2!
         ped->fPedestalMean = atof(argv[0]);
         ped->fPedestalWidth = atof(argv[1]);
      } else {
         ped->Error("SetMeanWidth_callback", "argc does not equal 2! it is %d" , argc);
      }
      return 0;
   }
//______________________________________________________________________________

