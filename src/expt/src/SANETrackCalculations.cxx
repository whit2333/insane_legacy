#include "SANETrackCalculations.h"

ClassImp(SANETrackCalculation1)

//________________________________________________________________________________
SANETrackCalculation1::SANETrackCalculation1(InSANEAnalysis * analysis) : 
   BETACalculationWithClusters(analysis)  {

   SetNameTitle("SANETrackCalculation1", "SANETrackCalculation1");

   fTrack      = new InSANEDISTrack();
   fTrajectory = new InSANEDISTrajectory();
   fTrajectory->SetRun(InSANERunManager::GetRunManager()->fCurrentRun);

   fGroups             = new std::vector<double>;
   fIsPerp             = false;
   fTrackPropagator    = nullptr;

   fReconEvent         = nullptr;
   fANNEvent           = new ANNDisElectronEvent5();

   fTrackPropMomentum0 = new TVector3();
   fTrackPropVertex0   = new TVector3();
   fTrackPropMomentum1 = new TVector3();
   fTrackPropVertex1   = new TVector3();

   fOutTree2           = nullptr;
   fEventNumber        = 0;
   fTrackNumber        = 0;

   fMaxLuciteMiss      = 10.0; //cm
   fMaxTrackerMiss     = 3.0;  //cm
   }
//________________________________________________________________________________
SANETrackCalculation1::~SANETrackCalculation1() {
   if (fOutTree)fOutTree->FlushBaskets();
   if (fOutTree)fOutTree->Write();
   if (fOutTree2)fOutTree2->Write();
}
//________________________________________________________________________________
void SANETrackCalculation1::Describe() {
   std::cout <<  "===============================================================================\n";
   std::cout <<  "  SANETrackCalculation1 - \n ";
   std::cout <<  "     \n";
   std::cout <<  "_______________________________________________________________________________\n";
}
//________________________________________________________________________________
Int_t SANETrackCalculation1::Initialize() {
   if (SANERunManager::GetRunManager()->GetVerbosity() > 2)
      std::cout << " o SANETrackCalculation1::Initialize()\n";
   BETACalculationWithClusters::Initialize();
   fTrackPropagator          = new InSANETrackPropagator2();
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();

   std::cout << " derp " << std::endl;
   // Get the recon event branch 
   auto * t = (TTree*) SANERunManager::GetRunManager()->GetCurrentFile()->FindObject("trackingPositions");
   if(!t) { t = (TTree*) gROOT->FindObject("trackingPositions"); }
   if(!t) {
      Error("Initialize"," ***In the voice of Aziz Ansari*** trackingPositions tree not found!!!!!!! OOOOOOOOOOOOOOOhhhhhhhh nooooooooooooooo.");
      return(-1);
   }
   if(!fReconEvent){
      fReconEvent = (InSANEReconstructedEvent*)(t->GetBranch("uncorrelatedPositions")->GetAddress());
   }
   //std::cout << " fReconEvent = " << fReconEvent << std::endl;

   fOutTree = new TTree("Tracks", "Tracks");
   //fOutTree->Branch("fRunNumber", &fRunNumber, "fRunNumber/I");
   fOutTree->Branch("fEventNumber", &fEventNumber, "fEventNumber/I");
   fOutTree->Branch("fTrackNumber", &fTrackNumber, "fTrackNumber/I");
   fOutTree->Branch("track", "InSANEDISTrack", &fTrack);
   fOutTree->Branch("trajectory", "InSANEDISTrajectory", &fTrajectory);
   fOutTree->Branch("triggerEvent", "InSANETriggerEvent", &(fEvents->TRIG));
   fOutTree->Branch("groups", fGroups);

   fOutTree2 = new TTree("PropTracks", "PropTracks");
   fOutTree2->Branch("triggerEvent", "InSANETriggerEvent", &(fEvents->TRIG));
   fOutTree2->Branch("fTrackPropVertex0", "TVector3", &fTrackPropVertex0);
   fOutTree2->Branch("fTrackPropMomentum0", "TVector3", &fTrackPropMomentum0);
   fOutTree2->Branch("fTrackPropVertex1", "TVector3", &fTrackPropVertex1);
   fOutTree2->Branch("fTrackPropMomentum1", "TVector3", &fTrackPropMomentum1);
   //fOutTree->Branch("NNElectron5", "ANNDisElectronEvent5", &fANNEvent);

   if(SANERunManager::GetRunManager()->IsPerpendicularRun()) fIsPerp = true;
   return(0);
}
//________________________________________________________________________________
Int_t SANETrackCalculation1::Calculate() {

   //if (!(fEvents->TRIG->IsBETA2Event()))  return(0);
   if (!(fEvents->TRIG->IsClusterable()))  return(0);
   fGroups->clear();
   fcGoodElectron = false;
   BIGCALCluster * aCluster = nullptr;
   InSANEHitPosition * aHitPos = nullptr;
   fTrack->Clear();
   fTrajectory->Clear();

   if (fClusterEvent) {

      fEventNumber = 0;
      fTrackNumber = 0;

      for (int kk = 0; kk < fClusterEvent->fNClusters  ; kk++) {
         aCluster = (BIGCALCluster*)(*fClusters)[kk] ;

         fEventNumber = aCluster->fEventNumber;

         fClosestTrackerDistance = 9999;

         /// Create a trajectory for every cluster but with loose cuts
         //if( aCluster->fCherenkovBestNPESum > 3.0 )
         //if( fBigcalDetector->cPassesClusterEnergy[aCluster->fClusterNumber] )
         if (aCluster->fTotalE < 6000.0 && aCluster->fTotalE > 0.0) {

            if(TMath::Abs(aCluster->fCherenkovTDC) < 25) fcGoodElectron = true;
            if (aCluster->fXMoment < 0.0) fGroups->push_back(3);
            if (aCluster->fXMoment > 0.0) fGroups->push_back(4);
            fGroups->push_back(aCluster->fSubDetector);

            fTrajectory->fNClusters                                     = fClusterEvent->fNClusters;
            fTrajectory->fEventNumber                                   = aCluster->fEventNumber;
            fTrajectory->fRunNumber                                     = aCluster->fRunNumber;
            fTrajectory->fNCherenkovElectrons                           = aCluster->fCherenkovBestADCSum;
            fTrajectory->fCherenkovTDC                                  = aCluster->fCherenkovTDC;
            fTrajectory->fIsNoisyChannel                                = aCluster->fIsNoisyChannel;
            fTrajectory->fIsGood                                        = aCluster->fIsGood;
            fTrajectory->fHelicity                                      = aCluster->fHelicity;
            fTrajectory->fPhi                                           = aCluster->GetPhi() + aCluster->fDeltaPhi ;
            fTrajectory->fTheta                                         = aCluster->GetTheta() + aCluster->fDeltaTheta ;
            fTrajectory->fEnergy                                        = aCluster->GetCorrectedEnergy() ;
            fTrajectory->fSubDetector                                   = aCluster->fSubDetector;
            fTrajectory->fClusterNumber                                 = aCluster->fClusterNumber;
            fTrajectory->fPosition0                                     = fBigcalDetector->GetXYCorrectedPosition(aCluster);
            Pvec.SetMagThetaPhi(fTrajectory->fEnergy, fTrajectory->fTheta, fTrajectory->fPhi);
            fTrajectory->fMomentum.SetVect(Pvec);
            fTrajectory->fMomentum.SetE(fTrajectory->fEnergy);

            // Track
            fTrack->fNClusters                                     = fClusterEvent->fNClusters;
            fTrack->fEventNumber                                   = aCluster->fEventNumber;
            fTrack->fRunNumber                                     = aCluster->fRunNumber;
            fTrack->fNCherenkovElectrons                           = aCluster->fCherenkovBestADCSum;
            fTrack->fCherenkovTDC                                  = aCluster->fCherenkovTDC;
            fTrack->fIsNoisyChannel                                = aCluster->fIsNoisyChannel;
            fTrack->fIsGood                                        = aCluster->fIsGood;
            fTrack->fHelicity                                      = aCluster->fHelicity;
            fTrack->fPhi                                           = aCluster->GetPhi() + aCluster->fDeltaPhi ;
            fTrack->fTheta                                         = aCluster->GetTheta() + aCluster->fDeltaTheta ;
            fTrack->fEnergy                                        = aCluster->GetCorrectedEnergy() ;
            fTrack->fSubDetector                                   = aCluster->fSubDetector;
            fTrack->fClusterNumber                                 = aCluster->fClusterNumber;
            fTrack->fPosition0                                     = fBigcalDetector->GetXYCorrectedPosition(aCluster);
            Pvec.SetMagThetaPhi(fTrack->fEnergy, fTrack->fTheta, fTrack->fPhi);
            fTrack->fMomentum.SetVect(Pvec);
            fTrack->fMomentum.SetE(fTrack->fEnergy);


            //---------------------------------------------------------------------
            // Neural Networks 
            //---------------------------------------------------------------------

            /// CLUSTER --> Bigcal Plane
            fANNDisElectronEvent.SetEventValues(aCluster); 
            fANNDisElectronEvent.fXClusterKurt = aCluster->fXKurtosis;
            fANNDisElectronEvent.fYClusterKurt = aCluster->fYKurtosis;
            fANNDisElectronEvent.fNClusters    = fClusterEvent->fNClusters;
            fANNDisElectronEvent.fRasterX      = fEvents->BEAM->fRasterPosition.X();
            fANNDisElectronEvent.fRasterY      = fEvents->BEAM->fRasterPosition.Y();

            fANNDisElectronEvent.GetMLPInputNeuronArray(fNNParams);

            if(fIsPerp) {
               aCluster->fDeltaE  = 0.0;//fNNPerpElectronClusterDeltaE.Value(0,fNNParams);
               aCluster->fDeltaX  = fNNPerpElectronClusterDeltaX.Value(0,fNNParams);
               aCluster->fDeltaY  = fNNPerpElectronClusterDeltaY.Value(0,fNNParams);
            } else {
               aCluster->fDeltaE  = 0.0;//fNNParaElectronClusterDeltaE.Value(0,fNNParams);
               aCluster->fDeltaX  = fNNParaElectronClusterDeltaX.Value(0,fNNParams);
               aCluster->fDeltaY  = fNNParaElectronClusterDeltaY.Value(0,fNNParams);
            }
            //if(aCluster->fjPeak<32) {
            //   aCluster->fDeltaE *= 5.0; // Adhoc 
            //}

            fTrajectory->fEnergy      = aCluster->fTotalE + aCluster->fDeltaE ;
            fTrajectory->fDeltaEnergy = aCluster->fDeltaE ;
            fTrack->fEnergy           = aCluster->fTotalE + aCluster->fDeltaE ;
            fTrack->fDeltaEnergy      = aCluster->fDeltaE ;

            //std::cout << " fDeltaE = " << aCluster->fDeltaE << std::endl;
            //std::cout << " fDeltaX = " << aCluster->fDeltaX << std::endl;
            //std::cout << " fDeltaY = " << aCluster->fDeltaY << std::endl;

            /// Bigcal Plane --> Target variables
            fvertex.SetXYZ(fEvents->BEAM->fRasterPosition.X(),fEvents->BEAM->fRasterPosition.Y(),0.0);
            fx_bigcalplane                          = fBigcalDetector->GetXYCorrectedPosition(aCluster);
            // Position at bigcal plane
            fTrajectory->fXBigcal_B                 = fx_bigcalplane;
            fTrack->fXBigcal_B                      = fx_bigcalplane;
            fp_naive                                = fx_bigcalplane - fvertex; 
            fANNFieldEvent2.fBigcalPlaneTheta       = fp_naive.Theta();//bigcalPlane->fPosition.Theta();
            fANNFieldEvent2.fBigcalPlanePhi         = fp_naive.Phi();//bigcalPlane->fPosition.Phi();
            fANNFieldEvent2.fBigcalPlaneX           = aCluster->GetXmoment() + aCluster->fDeltaX; //bigcalPlane->fLocalPosition.X();
            fANNFieldEvent2.fBigcalPlaneY           = aCluster->GetXmoment() + aCluster->fDeltaY; //bigcalPlane->fLocalPosition.Y();
            fANNFieldEvent2.fBigcalPlaneEnergy      = aCluster->GetCorrectedEnergy();//bigcalPlane->fEnergy;
            fANNFieldEvent2.fRasterX                = fEvents->BEAM->fRasterPosition.X();
            fANNFieldEvent2.fRasterY                = fEvents->BEAM->fRasterPosition.Y();
            fANNFieldEvent2.GetMLPInputNeuronArray(fNNParams);
            if(fIsPerp) {
               aCluster->fDeltaTheta = fNNPerpElectronDeltaTheta.Value(0,fNNParams);
               aCluster->fDeltaPhi   = fNNPerpElectronDeltaPhi.Value(0,fNNParams);
            } else {
               aCluster->fDeltaTheta = fNNParaElectronDeltaTheta.Value(0,fNNParams);
               aCluster->fDeltaPhi   = fNNParaElectronDeltaPhi.Value(0,fNNParams);
            }

            fTrajectory->fPhi         = fx_bigcalplane.Phi()   + aCluster->fDeltaPhi ;
            fTrajectory->fTheta       = fx_bigcalplane.Theta() + aCluster->fDeltaTheta ;
            fTrajectory->fDeltaPhi    = aCluster->fDeltaPhi ;
            fTrajectory->fDeltaTheta  = aCluster->fDeltaTheta ;
            fTrack->fPhi         = fx_bigcalplane.Phi()   + aCluster->fDeltaPhi ;
            fTrack->fTheta       = fx_bigcalplane.Theta() + aCluster->fDeltaTheta ;
            fTrack->fDeltaPhi    = aCluster->fDeltaPhi ;
            fTrack->fDeltaTheta  = aCluster->fDeltaTheta ;

            //std::cout << "a" << std::endl;
            fTrack->fPTarget_B.SetMagThetaPhi( fTrack->fEnergy, fTrack->fTheta, fTrack->fPhi);
            fTrack->fMomentum.SetVectMag(   fTrack->fPTarget_B,   fTrack->fEnergy);

            //std::cout << "b" << std::endl;
            fTrajectory->fPTarget_B.SetMagThetaPhi( fTrack->fEnergy, fTrack->fTheta, fTrack->fPhi);
            fTrajectory->fMomentum.SetVectMag(fTrack->fPTarget_B,   fTrack->fEnergy);


            /// Bigcal Plane X --> Bigcal Plane P
            fANNFieldEvent3.fBigcalPlaneTheta  = fp_naive.Theta();//bigcalPlane->fPosition.Theta();
            fANNFieldEvent3.fBigcalPlanePhi    = fp_naive.Phi();//bigcalPlane->fPosition.Phi();
            fANNFieldEvent3.fBigcalPlaneX      = aCluster->GetXmoment() + aCluster->fDeltaX; //bigcalPlane->fLocalPosition.X();
            fANNFieldEvent3.fBigcalPlaneY      = aCluster->GetXmoment() + aCluster->fDeltaY; //bigcalPlane->fLocalPosition.Y();
            fANNFieldEvent3.fBigcalPlaneEnergy = aCluster->GetCorrectedEnergy();//bigcalPlane->fEnergy;
            fANNFieldEvent3.fRasterX           = fEvents->BEAM->fRasterPosition.X();
            fANNFieldEvent3.fRasterY           = fEvents->BEAM->fRasterPosition.Y();
            fANNFieldEvent3.fXCluster          = aCluster->GetXmoment();
            fANNFieldEvent3.fYCluster          = aCluster->GetYmoment();
            fANNFieldEvent3.fXClusterSigma     = aCluster->GetXStdDeviation();
            fANNFieldEvent3.fYClusterSigma     = aCluster->GetYStdDeviation();
            fANNFieldEvent3.fXClusterSkew      = aCluster->fXSkewness;
            fANNFieldEvent3.fYClusterSkew      = aCluster->fYSkewness;
            fANNFieldEvent3.fXClusterKurt      = aCluster->fXKurtosis;
            fANNFieldEvent3.fYClusterKurt      = aCluster->fYKurtosis;
            fANNFieldEvent3.GetMLPInputNeuronArray(fNNParams);
            if(fIsPerp) {
               aCluster->fDeltaThetaP = fNNPerpElectronBCPDirTheta.Value(0,fNNParams);
               aCluster->fDeltaPhiP   = fNNPerpElectronBCPDirPhi.Value(0,fNNParams);
            } else {
               aCluster->fDeltaThetaP = fNNParaElectronBCPDirTheta.Value(0,fNNParams);
               aCluster->fDeltaPhiP   = fNNParaElectronBCPDirPhi.Value(0,fNNParams);
            }

            //std::cout << "c" << std::endl;
            fTrajectory->fPBigcal_B.SetMagThetaPhi( fANNFieldEvent3.fBigcalPlaneEnergy,
                  fANNFieldEvent3.fBigcalPlaneTheta + aCluster->fDeltaThetaP,
                  fANNFieldEvent3.fBigcalPlanePhi   + aCluster->fDeltaPhiP);
            fTrack->fPBigcal_B.SetMagThetaPhi( fANNFieldEvent3.fBigcalPlaneEnergy,
                  fANNFieldEvent3.fBigcalPlaneTheta + aCluster->fDeltaThetaP,
                  fANNFieldEvent3.fBigcalPlanePhi   + aCluster->fDeltaPhiP);

            fTrajectory->fCluster = (*aCluster);
            fTrack->fCluster = (*aCluster);

            /// Keep the closest tracker position hit
            if (fReconEvent) {

               //------------------------------------------------------------------------------------------
               // Tracker 
               //------------------------------------------------------------------------------------------

               //std::cout << "fReconEvent = " << fReconEvent << std::endl;
               //std::cout << "fReconEvent->fTargetPositions = " << fReconEvent->fTargetPositions << std::endl;
               //std::cout << "fTargetPositions->GetEntries() = " << fReconEvent->fTargetPositions->GetEntries() << std::endl;
               auto * targpos = (InSANEHitPosition*)(*(fReconEvent->fTargetPositions))[0];
               //std::cout << "targpos = " << targpos << std::endl;
               fTrajectory->fTargetPosition = targpos->fPosition;
               fTrack->fTargetPosition = targpos->fPosition;
               //fTrajectory->fTargetPosition.Dump();
               fClosestTrackerDistance = 9999;
               for (int i = 0; i < fReconEvent->fTrackerPositions->GetEntries(); i++) {
                  aHitPos = (InSANEHitPosition*)(*fReconEvent->fTrackerPositions)[i];

                  // Now take the best qualified tracker position.... needs work.
                  if (fClosestTrackerDistance > aHitPos->fPositionUncertainty.Mag()) {
                     //      if(aHitPos->fScintLayer == 1 ) fTrajectory->fTrackerY1Miss = aHitPos->fPositionUncertainty;
                     //      if(aHitPos->fScintLayer == 2 ) fTrajectory->fTrackerY2Miss = aHitPos->fPositionUncertainty;
                     fTrajectory->fTrackerMiss       = aHitPos->fPositionUncertainty;
                     fTrajectory->fTrackerPosition   = aHitPos->fPosition;
                     fTrajectory->fNTrackerNeighbors = aHitPos->fNNeighbors;
                     fTrack->fTrackerMiss       = aHitPos->fPositionUncertainty;
                     fTrack->fTrackerPosition   = aHitPos->fPosition;
                     fTrack->fNTrackerNeighbors = aHitPos->fNNeighbors;
                     fClosestTrackerDistance         = aHitPos->fPositionUncertainty.Mag();

                     // Calculate the track's y position in a y-z plane located at x = x_raster 
                     v1 = (aHitPos->fPosition - fTrajectory->fPosition0);
                     N1 = v1;
                     N1.SetMag(1);
                     ///  Calculate the y position for the x-z plane
                     double yrecon = (N1.Y()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Y();
                     ///  Calculate the z position for the x-z plane
                     double zrecon = (N1.Z()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Z();

                     /// Calculate the x position for a x-z plane at the y= y_raster
                     //double xrecon = (N1.X()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.X();
                     //double zrecon2 = (N1.Z()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.Z();

                     fTrajectory->fReconTarget.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);
                     fTrack->fReconTarget.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);
                     if ( aHitPos->fPositionUncertainty.Mag() < fMaxTrackerMiss ) {
                        new((*(fTrack->fHitPositions))[fTrack->fNPositions])InSANEHitPosition(*aHitPos);
                        fTrack->fNPositions++;
                     }
                  }
               }

               fClosestTrackerDistance = 9999;
               for (int i = 0; i < fReconEvent->fTrackerY1Positions->GetEntries(); i++) {
                  aHitPos = (InSANEHitPosition*)(*fReconEvent->fTrackerY1Positions)[i];
                  if (fClosestTrackerDistance > aHitPos->fPositionUncertainty.Mag()) {
                     //if(aHitPos->fScintLayer == 1 ) fTrajectory->fTrackerY1Miss = aHitPos->fPositionUncertainty;
                     //if(aHitPos->fScintLayer == 2 ) fTrajectory->fTrackerY2Miss = aHitPos->fPositionUncertainty;
                     fTrajectory->fTrackerMiss         = aHitPos->fPositionUncertainty;
                     fTrajectory->fTrackerPosition     = aHitPos->fPosition;
                     //fTrajectory->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                     fTrack->fTrackerMiss         = aHitPos->fPositionUncertainty;
                     fTrack->fTrackerPosition     = aHitPos->fPosition;
                     //fTrack->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                     fClosestTrackerDistance           = aHitPos->fPositionUncertainty.Mag();

                     /// Calculate the track's y position in a y-z plane located at x = x_raster 
                     v1 = (aHitPos->fPosition - fTrajectory->fPosition0);
                     N1 = v1;
                     N1.SetMag(1);
                     ///  Calculate the y position for the x-z plane
                     double yrecon = (N1.Y()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Y();
                     ///  Calculate the z position for the x-z plane
                     double zrecon = (N1.Z()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Z();

                     /// Calculate the x position for a x-z plane at the y= y_raster
                     //double xrecon = (N1.X()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.X();
                     //double zrecon2 = (N1.Z()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.Z();

                     fTrajectory->fReconTargetY1.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);
                     fTrack->fReconTargetY1.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);
                     //if ( aHitPos->fPositionUncertainty.Mag() < fMaxTrackerMiss ) {
                     //   new((*(fTrack->fHitPositions))[fTrack->fNPositions])InSANEHitPosition(*aHitPos);
                     //   fTrack->fNPositions++;
                     //}

                  }
               }

               fClosestTrackerDistance = 9999;
               for (int i = 0; i < fReconEvent->fTrackerY2Positions->GetEntries(); i++) {
                  aHitPos = (InSANEHitPosition*)(*fReconEvent->fTrackerY2Positions)[i];
                  if (fClosestTrackerDistance > aHitPos->fPositionUncertainty.Mag()) {
                     //if(aHitPos->fScintLayer == 1 ) fTrajectory->fTrackerY1Miss = aHitPos->fPositionUncertainty;
                     //if(aHitPos->fScintLayer == 2 ) fTrajectory->fTrackerY2Miss = aHitPos->fPositionUncertainty;
                     fTrajectory->fTrackerMiss         = aHitPos->fPositionUncertainty;
                     fTrajectory->fTrackerPosition     = aHitPos->fPosition;
                     //fTrajectory->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                     fTrack->fTrackerMiss         = aHitPos->fPositionUncertainty;
                     fTrack->fTrackerPosition     = aHitPos->fPosition;
                     //fTrack->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                     fClosestTrackerDistance           = aHitPos->fPositionUncertainty.Mag();

                     /// Calculate the track's y position in a y-z plane located at x = x_raster 
                     v1 = (aHitPos->fPosition - fTrajectory->fPosition0);
                     N1 = v1;
                     N1.SetMag(1);
                     ///  Calculate the y position for the x-z plane
                     double yrecon = (N1.Y()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Y();
                     ///  Calculate the z position for the x-z plane
                     double zrecon = (N1.Z()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Z();

                     /// Calculate the x position for a x-z plane at the y= y_raster
                     //double xrecon = (N1.X()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.X();
                     //double zrecon2 = (N1.Z()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.Z();

                     fTrajectory->fReconTargetY2.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);
                     fTrack->fReconTargetY2.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);
                     //if ( aHitPos->fPositionUncertainty.Mag() < fMaxTrackerMiss ) {
                     //   new((*(fTrack->fHitPositions))[fTrack->fNPositions])InSANEHitPosition(*aHitPos);
                     //   fTrack->fNPositions++;
                     //}

                  }
               }


               //------------------------------------------------------------------------------------------
               // Lucite Hodoscope
               //------------------------------------------------------------------------------------------
               fClosestTrackerDistance = 9999;
               for (int i = 0; i < fReconEvent->fNLucitePositions; i++) {
                  aHitPos = (InSANEHitPosition*)(*fReconEvent->fLucitePositions)[i];
                  if (fClosestTrackerDistance > aHitPos->fPositionUncertainty.Mag()) {
                     fTrajectory->fTrackerMiss         = aHitPos->fPositionUncertainty;
                     fTrajectory->fTrackerPosition     = aHitPos->fPosition;
                     //fTrajectory->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                     fTrack->fTrackerMiss         = aHitPos->fPositionUncertainty;
                     fTrack->fTrackerPosition     = aHitPos->fPosition;
                     //fTrack->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                     fClosestTrackerDistance           = aHitPos->fPositionUncertainty.Mag();

                     /// Calculate the track's y position in a y-z plane located at x = x_raster 
                     v1 = (aHitPos->fPosition - fTrajectory->fPosition0);
                     N1 = v1;
                     N1.SetMag(1);
                     ///  Calculate the y position for the x-z plane
                     double yrecon = (N1.Y()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Y();
                     ///  Calculate the z position for the x-z plane
                     double zrecon = (N1.Z()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Z();

                     /// Calculate the x position for a x-z plane at the y= y_raster
                     //double xrecon = (N1.X()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.X();
                     //double zrecon2 = (N1.Z()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.Z();

                     fTrajectory->fReconTargetY2.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);
                     fTrack->fReconTargetY2.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);
                     if ( aHitPos->fPositionUncertainty.Mag() < fMaxLuciteMiss ) {
                        new((*(fTrack->fHitPositions))[fTrack->fNPositions])InSANEHitPosition(*aHitPos);
                        fTrack->fNPositions++;
                     }

                  }
               }

               /// Get the only target position 
               fTrajectory->fLuciteMissDistance = fReconEvent->fClosestLuciteMissDistance;
               fTrack->fLuciteMissDistance = fReconEvent->fClosestLuciteMissDistance;
            }
            if (fOutTree)fOutTree->Fill();
            fTrackNumber++;
         } // Passes cuts
      } // loop over clusters
   } // cluster event
   return(0);
}
//________________________________________________________________________________

//________________________________________________________________________________
ClassImp(SANETrackCalculation2)

//________________________________________________________________________________
SANETrackCalculation2::SANETrackCalculation2(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis)  {
   SetNameTitle("SANETrackCalculation2", "SANETrackCalculation2");

   fTrackPropagator          = nullptr;//new InSANETrackPropagator2();
   fTrackPropMomentum0 = new TVector3();
   fTrackPropVertex0   = new TVector3();
   fTrackPropMomentum1 = new TVector3();
   fTrackPropVertex1   = new TVector3();

   fTrajectory     = new InSANEDISTrajectory();
   fReconEvent     = new InSANEReconstructedEvent();
   fGroups         = new std::vector<double>;

   fTrajectoryTree = nullptr;
   fOutTree2       = nullptr;
   fTracksTree     = nullptr;
}
//________________________________________________________________________________
SANETrackCalculation2::~SANETrackCalculation2() {
}
//________________________________________________________________________________
void SANETrackCalculation2::Describe() {
   std::cout <<  "===============================================================================\n";
   std::cout <<  "  SANETrackCalculation2 - \n ";
   std::cout <<  "     Almost the same as SANETrackCalculation1 but it uses the \"TrackPropagator\".\n";
   std::cout <<  "_______________________________________________________________________________\n";
}
//________________________________________________________________________________
Int_t SANETrackCalculation2::Initialize() {
   if (SANERunManager::GetRunManager()->GetVerbosity() > 2)
      std::cout << " o SANETrackCalculation2::Initialize()\n";

   BETACalculationWithClusters::Initialize();
   fTrackPropagator          = new InSANETrackPropagator2();

   // Get the tree created by SANETrajectoryCalculation3
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   fTrajectoryTree = (TTree*) SANERunManager::GetRunManager()->GetCurrentFile()->FindObject("trackingPositions");
   if(!fTrajectoryTree) { fTrajectoryTree = (TTree*) gROOT->FindObject("trackingPositions"); }
   if(!fTrajectoryTree) {
      Error("Initialize"," ***In the voice of Aziz Ansari*** Trajectory tree not found!!!!!!! OOOOOOOOOOOOOOOhhhhhhhh nooooooooooooooo.");
      return(-1);
   }
   fTrajectoryTree->SetBranchAddress("uncorrelatedPositions",&fReconEvent);
   fTrajectoryTree->BuildIndex("fRunNumber","fEventNumber");
   std::cout << "Trajectory Tree initialized" << std::endl;

   // Get the tree created by SANETrackCalculation1 
   fTracksTree = (TTree*) SANERunManager::GetRunManager()->GetCurrentFile()->FindObject("Tracks");
   if(!fTracksTree) {
      fTracksTree = (TTree*) gROOT->FindObject("Tracks");
   }
   if(!fTracksTree) {
      Error("Initialize"," ***In the voice of Aziz Ansari*** Trajectory tree not found!!!!!!! OOOOOOOOOOOOOOOhhhhhhhh nooooooooooooooo.");
      return(-1);
   }
   fTracksTree->SetBranchAddress("trajectory",    &fTrajectory);
   fTracksTree->SetBranchAddress("triggerEvent",  &(fEvents->TRIG));
   fTracksTree->SetBranchAddress("groups",        &fGroups);
   fTracksTree->BuildIndex("fEventNumber","fTrackNumber");
   fOutTree = fTracksTree->CloneTree(0);
   fOutTree->SetNameTitle("Tracks2", "Tracks2");
   std::cout << " fOutTree = " << fOutTree << std::endl;
   std::cout << " fTracksTree = " << fTracksTree << std::endl;
   std::cout << " Tracks Tree initialized" << std::endl;

   //fOutTree = new TTree("Tracks2", "Tracks2");
   //fOutTree->Branch("trajectory", "InSANEDISTrajectory", &fTrajectory);
   //fOutTree->Branch("triggerEvent", "InSANETriggerEvent", &(fEvents->TRIG));
   //fOutTree->Branch("groups", fGroups);

   fOutTree2 = new TTree("PropTracks", "PropTracks");
   fOutTree2->Branch("triggerEvent", "InSANETriggerEvent", &(fEvents->TRIG));
   fOutTree2->Branch("fTrackPropVertex0", "TVector3", &fTrackPropVertex0);
   fOutTree2->Branch("fTrackPropMomentum0", "TVector3", &fTrackPropMomentum0);
   fOutTree2->Branch("fTrackPropVertex1", "TVector3", &fTrackPropVertex1);
   fOutTree2->Branch("fTrackPropMomentum1", "TVector3", &fTrackPropMomentum1);
   //fOutTree->Branch("NNElectron5", "ANNDisElectronEvent5", &fANNEvent);
   return(0);
}

//________________________________________________________________________________
Int_t SANETrackCalculation2::Calculate() {

   if (!(fEvents->TRIG->IsBETA2Event()))  return(0);

   //if(!fTrajectoryTree) return(0); 
   // if 0 bytes are read ... ignore event.
   if(!fTrajectoryTree->GetEntryWithIndex(fEvents->TRIG->fRunNumber,fEvents->TRIG->fEventNumber)) return(0);


   //fGroups->clear();
   fcGoodElectron = false;
   BIGCALCluster * aCluster;
   TVector3 Pvec;
   InSANEHitPosition * aHitPos = nullptr;
   InSANEHitPosition * bHitPos = nullptr;
   TVector3 PlaneNorm;
   TVector3 PlanePoint;
   TVector3 PlaneResult;
   TVector3 direction;

   if (fClusterEvent) {

      //if(fClusterEvent->fNClusters>1){
         //std::cout << "clust " << fTrajectory->fClusterNumber << "\n";
         //std::cout << fClusterEvent->fNClusters << " clusters " << std::endl;
         //std::cout << fReconEvent << " recon event " << std::endl;
         //}
      for(Int_t itrack = 0; itrack < fReconEvent->fNTrajectories; itrack++) {
         //std::cout << "Event " << fReconEvent->fEventNumber << "\n";
         //std::cout << "track = " << itrack << std::endl;
         //std::cout << "tree entries = " << fTracksTree->GetEntries() << std::endl;
         Int_t entryindex = fTracksTree->GetEntryNumberWithIndex(fReconEvent->fEventNumber,itrack);
         //std::cout << "index = " << entryindex << std::endl;
         if(entryindex<0) return 0;
         fTracksTree->GetEntry(entryindex);
         //Int_t bytesread = fTracksTree->GetEntryWithIndex(fReconEvent->fEventNumber,itrack);
         //std::cout << bytesread << std::endl;
         //if(!bytesread )  return(0);

         //for (int kk = 0; kk < fClusterEvent->fNClusters  ; kk++) {
         //aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
         aCluster = &(fTrajectory->fCluster);

         fClosestTrackerDistance = 9999;

         /// Create a trajectory for every cluster but with loose cuts
         if( aCluster->fCherenkovBestNPESum > 0.5 )
         if (aCluster->fIsGood)
         if (aCluster->fTotalE < 6000.0 && aCluster->fTotalE > 0.0) {

            fTrajectory->fNCherenkovElectrons = aCluster->fCherenkovBestADCSum;
            if (TMath::Abs(aCluster->fCherenkovTDC) < 30) fcGoodElectron       = true;

               //fTrajectory->fEventNumber    = aCluster->fEventNumber;
               //fTrajectory->fRunNumber      = aCluster->fRunNumber;
               //fTrajectory->fCherenkovTDC   = aCluster->fCherenkovTDC;
               //fTrajectory->fIsNoisyChannel = aCluster->fIsNoisyChannel;
               //fTrajectory->fIsGood         = aCluster->fIsGood;

               /// Get the target (raster) position
               if (fReconEvent) {
                  fTrajectory->fTargetPosition = ((InSANEHitPosition*)((*fReconEvent->fTargetPositions)[0]))->fPosition;
               }

               //----- Neural Networks -----
               
               /// Define the NN input (Cluster->BigcalPlane)
               fNNParaXYClusterEvent.SetEventValues(aCluster);
               fNNParaXYClusterEvent.GetMLPInputNeuronArray(nnInputArray);

               aCluster->fDeltaX = nnParaXYClusterDeltaX.Value(0,nnInputArray); 
               aCluster->fDeltaY = nnParaXYClusterDeltaY.Value(0,nnInputArray);
               aCluster->fDeltaE = 0.0;//nnParaXYClusterEnergy.Value(0,nnInputArray);

               fTrajectory->fXBigcal_C = fBigcalDetector->GetXYCorrectedPosition(aCluster);
               fTrajectory->fXBigcal_B = fBigcalDetector->GetXYCorrectedPosition(aCluster);
              
               /// Define the NN input (BigcalPlane X -> BigcalPlane P)
               /// Note the same input is used for getting the momentum direction at the BigCal Plane
               /// as well as the momentum vector at the target. 
               fNNElectronThruFieldEvent.fBigcalPlaneX       = aCluster->GetXmoment() + aCluster->fDeltaX;
               fNNElectronThruFieldEvent.fBigcalPlaneY       = aCluster->GetYmoment() + aCluster->fDeltaY;
               fNNElectronThruFieldEvent.fRasterX            = fTrajectory->fTargetPosition.X();
               fNNElectronThruFieldEvent.fRasterY            = fTrajectory->fTargetPosition.Y();
               fNNElectronThruFieldEvent.fBigcalPlaneTheta   = fTrajectory->fXBigcal_C.Theta();
               fNNElectronThruFieldEvent.fBigcalPlanePhi     = fTrajectory->fXBigcal_C.Phi();
               fNNElectronThruFieldEvent.fBigcalPlaneEnergy  = aCluster->GetEnergy() + aCluster->fDeltaE;
               fNNElectronThruFieldEvent.GetMLPInputNeuronArray(nnInputArray);

               /// The Momentum direction at BigcalPlane
               Double_t P_theta = aCluster->GetTheta() + nnParaBCPDir.Value(1,nnInputArray); 
               Double_t P_phi   = aCluster->GetPhi()   + nnParaBCPDir.Value(1,nnInputArray); 
               fTrajectory->fPBigcal_B.SetMagThetaPhi(aCluster->GetEnergy(),P_theta,P_phi);

               /// Get the momentum direction at the target
               /// Set the trajectory values
               fTrajectory->fHelicity      = aCluster->fHelicity;
               fTrajectory->fPhi           = aCluster->GetCorrectedPhi() ;
               fTrajectory->fTheta         = aCluster->GetCorrectedTheta() ;
               fTrajectory->fEnergy        = aCluster->GetCorrectedEnergy() ;
               fTrajectory->fSubDetector   = aCluster->fSubDetector;
               fTrajectory->fClusterNumber = aCluster->fClusterNumber;


               /// Group
               fGroups->push_back(aCluster->fSubDetector);
               if (aCluster->fXMoment < 0.0) fGroups->push_back(3);
               if (aCluster->fXMoment > 0.0) fGroups->push_back(4);

               
               fTrajectory->fPosition0    = fBigcalDetector->GetPosition(aCluster);
               Pvec.SetMagThetaPhi(fTrajectory->fEnergy, fTrajectory->fTheta, fTrajectory->fPhi);
               fTrajectory->fMomentum.SetVect(Pvec);
               fTrajectory->fMomentum.SetE(fTrajectory->fEnergy);

               /// Propagate Track back to the target
               /// Right now we are using the "dumb" momentum and unrefined cluster position
               if(fTrackPropagator){
                  (*fTrackPropVertex0)   = fTrajectory->fPosition0;
                  (*fTrackPropMomentum0) = fTrajectory->fMomentum.Vect();

                  fTrackPropagator->SetTrackMomentumPosition((*fTrackPropMomentum0),(*fTrackPropVertex0));
                  //fTrackPropagator->GetMomentumPositionAtTarget(fTrackPropMomentum1,fTrackPropVertex1);
                  //if(fOutTree2) fOutTree2->Fill();
               }

               /// Keep the closest tracker position hit
               if (fReconEvent) {

                  fTrajectory->fTargetPosition = ((InSANEHitPosition*)((*fReconEvent->fTargetPositions)[0]))->fPosition;

                  fClosestTrackerDistance = 9999;
                  //std::cout << " Tracker Positions : " << fReconEvent->fTrackerPositions->GetEntries() << "\n";
                  for (int i = 0; i < fReconEvent->fTrackerPositions->GetEntries(); i++) {
                     aHitPos = (InSANEHitPosition*)(*fReconEvent->fTrackerPositions)[i];

                     /// Use the track propagator
                     if(fTrackPropagator){
                        bHitPos = fReconEvent->AddTracker2Position();
                        bHitPos->fPosition          = aHitPos->fPosition;
                        bHitPos->fClusterNumber     = aHitPos->fClusterNumber;
                        bHitPos->fHitNumber         = aHitPos->fHitNumber;
                        bHitPos->fTiming            = aHitPos->fTiming;

                        /// Crude positions ... needs fixed.
                        PlaneNorm.SetMagThetaPhi(1.0,40.0*degree,0.0*degree);
                        PlanePoint = bHitPos->fPosition;//.SetMagThetaPhi(51.0,40.0*degree,0.0*degree);
                        /// dir =  bigcal position - tracker position
                        TVector3 direction = (*fTrackPropVertex0) - aHitPos->fPosition;
                        fTrackPropMomentum0->SetTheta(P_theta);
                        fTrackPropMomentum0->SetPhi(P_phi);
                        fTrackPropagator->SetTrackMomentumPosition((*fTrackPropMomentum0),(*fTrackPropVertex0));
                        fTrackPropagator->GetPositionOnPlane(PlaneNorm,PlanePoint,PlaneResult);
                        bHitPos->fSeedingPosition       = PlaneResult;
                        bHitPos->fPositionUncertainty   = bHitPos->fPosition - bHitPos->fSeedingPosition;
                        bHitPos->fMissDistance          = bHitPos->fPositionUncertainty.Mag();
                        //fTrackPropagator->SetTrackMomentumPosition((*fTrackPropMomentum0),(*fTrackPropVertex0));
                        //fTrackPropagator->GetPositionOnPlane(PlaneNorm,PlanePoint,PlaneResult);
                        //std::cout << " Propagator Difference to tracker \n";
                        //bHitPos->fSeedingPosition.Print();
                        //PlaneResult.Print();
                        //(bHitPos->fSeedingPosition - PlaneResult).Print();

                     }

                     /// Now take the best qualified tracker position.... needs work.
                     if ( fClosestTrackerDistance > aHitPos->fPositionUncertainty.Y() ) {
                        //      if(aHitPos->fScintLayer == 1 ) fTrajectory->fTrackerY1Miss = aHitPos->fPositionUncertainty;
                        //      if(aHitPos->fScintLayer == 2 ) fTrajectory->fTrackerY2Miss = aHitPos->fPositionUncertainty;
                        fTrajectory->fTrackerMiss = aHitPos->fPositionUncertainty;
                        fTrajectory->fTrackerPosition = aHitPos->fPosition;
                        //      fTrajectory->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                        fClosestTrackerDistance = aHitPos->fPositionUncertainty.Y();

                        /// Calculate the track's y position in a y-z plane located at x = x_raster 
                        v1 = (aHitPos->fPosition - fTrajectory->fPosition0);
                        N1 = v1;
                        N1.SetMag(1);
                        ///  Calculate the y position for the x-z plane
                        double yrecon = (N1.Y()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Y();
                        ///  Calculate the z position for the x-z plane
                        double zrecon = (N1.Z()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Z();

                        /// Calculate the x position for a x-z plane at the y= y_raster
                        // 		  double xrecon = (N1.X()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.X();
                        // 		  double zrecon2 = (N1.Z()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.Z();

                        fTrajectory->fReconTarget.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);

                        /// Use the track propagator
                        if(fTrackPropagator){

                           /// Crude positions ... needs fixed.
                           // X0
                           PlaneNorm.SetXYZ(1.0,0,0);
                           PlanePoint.SetXYZ(fTrajectory->fTargetPosition.X(),0.0,0.0);

                           /// dir =  bigcal position - tracker position
                           direction = (*fTrackPropVertex0) - aHitPos->fPosition;
                           fTrackPropMomentum0->SetTheta(P_theta);
                           fTrackPropMomentum0->SetPhi(P_phi);

                           fTrackPropagator->SetTrackMomentumPosition((*fTrackPropMomentum0),(*fTrackPropVertex0));
                           fTrackPropagator->GetPositionOnPlane(PlaneNorm,PlanePoint,PlaneResult);
                           fTrajectory->fReconTarget1       = PlaneResult;

                           /// Crude positions ... needs fixed.
                           PlaneNorm.SetXYZ(0.0,1.0,0);
                           PlanePoint.SetXYZ(0.0,fTrajectory->fTargetPosition.Y(),0.0);

                           /// dir =  bigcal position - tracker position
                           direction = (*fTrackPropVertex0) - aHitPos->fPosition;
                           fTrackPropMomentum0->SetTheta(P_theta);
                           fTrackPropMomentum0->SetPhi(P_phi);

                           fTrackPropagator->SetTrackMomentumPosition((*fTrackPropMomentum0),(*fTrackPropVertex0));
                           fTrackPropagator->GetPositionOnPlane(PlaneNorm,PlanePoint,PlaneResult);
                           fTrajectory->fReconTarget2       = PlaneResult;
                        }

                     } // closest
                     //if (fOutTree)fOutTree->Fill();
                  } // Tracker loop

                  //std::cout << " Tracker y1 Positions : " << fReconEvent->fTrackerY1Positions->GetEntries() << "\n";
                  fClosestTrackerDistance = 9999;
                  for (int i = 0; i < fReconEvent->fTrackerY1Positions->GetEntries(); i++) {
                     aHitPos = (InSANEHitPosition*)(*fReconEvent->fTrackerY1Positions)[i];
                     if (fClosestTrackerDistance > aHitPos->fPositionUncertainty.Mag()) {
                        //      if(aHitPos->fScintLayer == 1 ) fTrajectory->fTrackerY1Miss = aHitPos->fPositionUncertainty;
                        //      if(aHitPos->fScintLayer == 2 ) fTrajectory->fTrackerY2Miss = aHitPos->fPositionUncertainty;
                        fTrajectory->fTrackerMiss = aHitPos->fPositionUncertainty;
                        fTrajectory->fTrackerPosition = aHitPos->fPosition;
                        //      fTrajectory->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                        fClosestTrackerDistance = aHitPos->fPositionUncertainty.Mag();

                        /// Calculate the track's y position in a y-z plane located at x = x_raster 
                        v1 = (aHitPos->fPosition - fTrajectory->fPosition0);
                        N1 = v1;
                        N1.SetMag(1);
                        ///  Calculate the y position for the x-z plane
                        double yrecon = (N1.Y()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Y();
                        ///  Calculate the z position for the x-z plane
                        double zrecon = (N1.Z()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Z();

                        /// Calculate the x position for a x-z plane at the y= y_raster
                        //  double xrecon = (N1.X()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.X();
                        //  double zrecon2 = (N1.Z()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.Z();

                        fTrajectory->fReconTargetY1.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);

                     }
                  }

                  //std::cout << " Tracker Y2 Positions : " << fReconEvent->fTrackerY2Positions->GetEntries() << "\n";
                  fClosestTrackerDistance = 9999;
                  for (int i = 0; i < fReconEvent->fTrackerY2Positions->GetEntries(); i++) {
                     aHitPos = (InSANEHitPosition*)(*fReconEvent->fTrackerY2Positions)[i];
                     if (fClosestTrackerDistance > aHitPos->fPositionUncertainty.Mag()) {
                        //      if(aHitPos->fScintLayer == 1 ) fTrajectory->fTrackerY1Miss = aHitPos->fPositionUncertainty;
                        //      if(aHitPos->fScintLayer == 2 ) fTrajectory->fTrackerY2Miss = aHitPos->fPositionUncertainty;
                        fTrajectory->fTrackerMiss = aHitPos->fPositionUncertainty;
                        fTrajectory->fTrackerPosition = aHitPos->fPosition;
                        //      fTrajectory->fNTrackerNeighbors = aHitPos->fNNeighborHits;
                        fClosestTrackerDistance = aHitPos->fPositionUncertainty.Mag();

                        /// Calculate the track's y position in a y-z plane located at x = x_raster 
                        v1 = (aHitPos->fPosition - fTrajectory->fPosition0);
                        N1 = v1;
                        N1.SetMag(1);
                        ///  Calculate the y position for the x-z plane
                        double yrecon = (N1.Y()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Y();
                        ///  Calculate the z position for the x-z plane
                        double zrecon = (N1.Z()/N1.X())*(fTrajectory->fTargetPosition.X()-aHitPos->fPosition.X()) +aHitPos->fPosition.Z();

                        /// Calculate the x position for a x-z plane at the y= y_raster
                        // 		  double xrecon = (N1.X()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.X();
                        // 		  double zrecon2 = (N1.Z()/N1.Y())*(fTrajectory->fTargetPosition.Y()-hitpos->fPosition.Y()) +hitpos->fPosition.Z();

                        fTrajectory->fReconTargetY2.SetXYZ(fTrajectory->fTargetPosition.X(),yrecon,zrecon);

                     }
                  }

                  fTrajectory->fLuciteMissDistance = fReconEvent->fClosestLuciteMissDistance;
               }
               if (fOutTree)fOutTree->Fill();
            } // Passes cuts
      }/// cluster loop
   }
   return(0);
}



