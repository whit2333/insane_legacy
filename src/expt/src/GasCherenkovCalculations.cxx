#include "GasCherenkovCalculations.h"
#include "InSANECorrection.h"

//_______________________________________________________//

ClassImp(GasCherenkovCalculation1)

Int_t GasCherenkovCalculation1::Calculate()
{
   /// Proceeds only if it is a BETAEvent
   if (!(fEvents->TRIG->IsClusterable()))  return(0);
   Int_t fNumberOfNewTDCHits = 0;
   //GasCherenkovHit * bCerHit = 0;
   //GasCherenkovHit * cCerHit = 0;

   /// Initialize all 12 ADC values
   for (int j = 0; j < 12; j++) {
      aCerHit = new((*aBetaEvent->fGasCherenkovEvent->fGasCherenkovADCHits)[j]) GasCherenkovHit();
      aCerHit->fMirrorNumber = j + 1;
   }

   /// Loops over hits
   for (int kk = 0; kk < fCerHits->GetEntries(); kk++) {
      aCerHit = (GasCherenkovHit*)(*fCerHits)[kk] ;

      if (aCerHit->fLevel == 1) {
         /// Count all TDC hits
         aBetaEvent->fGasCherenkovEvent->fNumberOfTDCHits++;
      }

      if (aCerHit->fLevel == 0 || aCerHit->fLevel == 1) {

         /// Align the ADC  signal peaks
         aCerHit->fADCAlign =
            ((Double_t)aCerHit->fADC) / ((Double_t) fCherenkovDetector->fCherenkovAlignmentCoef[aCerHit->fMirrorNumber-1]);

         /// Determine the number of photoelectrons
         aCerHit->fNPE = ((Double_t) aCerHit->fADC) / ((Double_t)fCherenkovDetector->Get1PECalibration(aCerHit->fMirrorNumber));


      }

      /// Set the ADC hits for the individual mirrors (software sums will be defined using these);
      if (aCerHit->fLevel == 0) {
         /// Count ADC Hits
         aBetaEvent->fGasCherenkovEvent->fNumberOfADCHits++;

         /// Set the value of the ADC hit array which is indexed by "mirror number -1"
         //bCerHit = (GasCherenkovHit*)(*aBetaEvent->fGasCherenkovEvent->fGasCherenkovADCHits)[aCerHit->fMirrorNumber-1];
         ////bCerHit = aCerHit;
         //bCerHit = 
         new((*aBetaEvent->fGasCherenkovEvent->fGasCherenkovADCHits)[aCerHit->fMirrorNumber-1]) GasCherenkovHit(*aCerHit);

      }

      /// Fill individual (no analog sums) mirror TDCHits array
      if (aCerHit->fLevel == 1 &&  aCerHit->fMirrorNumber < 9 &&  aCerHit->fMirrorNumber > 0) {
         //cCerHit = 
         new((*aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits)[fNumberOfNewTDCHits]) GasCherenkovHit(*aCerHit);
         /*          (*bCerHit)=(*aCerHit);*/
         fNumberOfNewTDCHits++;
         aBetaEvent->fGasCherenkovEvent->fNumberOfTDCHits++;
      }

   } // end of cherenkov hit loop

   /// Set the NPE Sums for each mirror
   for (int i = 0; i < 8; i++) {
      aBetaEvent->fGasCherenkovEvent->fNPESums[i] = fCherenkovDetector->GetNPESum(i + 1);
   }

   return(0);
}
//_______________________________________________________//

ClassImp(GasCherenkovCalculation2)

Int_t GasCherenkovCalculation2::Calculate()
{

   /// Proceeds only if it is a BETAEvent
   if (!(fEvents->TRIG->IsBETAEvent()))  return(0);
   //Int_t fNumberOfNewTDCHits = 0;
   GasCherenkovHit * bCerHit = nullptr;

   /// Copy and delete TDC Hits
   if (fCopiedArray) delete fCopiedArray;
   fCopiedArray = (TClonesArray*) aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits->Clone("tempCerTDCHits");
   TClonesArray &aCopy = (*fCopiedArray);

   /// Clear TDC Hits ClonesArray
   aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits->Clear("C");
   Int_t NGoodTDCHits = 0;
   for (int i = 0; i < aCopy.GetEntries(); i++) {
      aCerHit = (GasCherenkovHit*)(aCopy[i]);

      /// If in main TDC peak add to new tdc hits array
      if (aCerHit->fTDCAlign > fCherenkovDetector->cTDCAlignMin && aCerHit->fTDCAlign < fCherenkovDetector->cTDCAlignMax) {

         bCerHit = new((* aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits)[NGoodTDCHits]) GasCherenkovHit(*aCerHit);
         bCerHit->fPassesTimingCut = true;
         NGoodTDCHits++;

      }
      /// Second window added too for investigating odd bump around -1850
      else  if (aCerHit->fTDC/*Align */ > fCherenkovDetector->cTDCAlignMin2 && aCerHit->fTDC/*Align*/  < fCherenkovDetector->cTDCAlignMax2) {

         bCerHit = new((* aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits)[NGoodTDCHits]) GasCherenkovHit(*aCerHit);
         bCerHit->fPassesTimingCut = true;
         NGoodTDCHits++;

      }

   }


   return(0);
}
//_______________________________________________________//

ClassImp(GasCherenkovCalculation3)
//_______________________________________________________//

ClassImp(GasCherenkovCalculation4)


