#include  "BigcalEvent.h"
#include <TClonesArray.h>
#include  "BigcalHit.h"
#include  "InSANEDetectorEvent.h"
#include <iostream>
#include "InSANERunManager.h"

ClassImp(BigcalEvent)

TClonesArray * BigcalEvent::fgBigcalHits = nullptr;
TClonesArray * BigcalEvent::fgBigcalTimingGroupHits = nullptr;
TClonesArray * BigcalEvent::fgBigcalTriggerGroupHits = nullptr;

BigcalEvent::BigcalEvent()
{
   fNumberOfADCHits = 0;
   fNumberOfTDCHits = 0;
   fNumberOfTimedTDCHits = 0;
   fNumberOfTimedADCHits = 0;
   fNumberOfTriggerGroupHits = 0;
   fNumberOfTDCGroupHits = 0;
   fTotalEnergyDeposited = 0.0;

   if (!fgBigcalHits) {
      fgBigcalHits = new TClonesArray("BigcalHit", 100);
      if (InSANERunManager::GetRunManager()->fVerbosity > 0) printf("  - Creating Static Global Variable fgBigcalHits \n");
   }
   fBigcalHits = fgBigcalHits;

   if (!fgBigcalTimingGroupHits) {
      fgBigcalTimingGroupHits = new TClonesArray("BigcalHit", 5);
   }
   fBigcalTimingGroupHits = fgBigcalTimingGroupHits;

   if (!fgBigcalTriggerGroupHits) {
      fgBigcalTriggerGroupHits = new TClonesArray("BigcalHit", 5);
   }
   fBigcalTriggerGroupHits = fgBigcalTriggerGroupHits;
}

//______________________________________________________________________________
BigcalEvent::~BigcalEvent()
{
   /*  if(fgBigcalHits) delete fgBigcalHits;
     fgBigcalHits = 0;
     fBigcalHits = 0;
     if(fgBigcalTimingGroupHits) delete fgBigcalTimingGroupHits;
     fgBigcalTimingGroupHits=0;
     fBigcalTimingGroupHits=0;
     if(fgBigcalTriggerGroupHits) delete fgBigcalTriggerGroupHits;
     fgBigcalTriggerGroupHits=0;
     fBigcalTriggerGroupHits=0;
   */
   //   if(InSANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " - deleted BigcalEvent   " << std::endl;
}

//______________________________________________________________________________
Int_t BigcalEvent::AllocateHitArray() {
   if (!fgBigcalHits) {
      fgBigcalHits = new TClonesArray("BigcalHit", 40);
      if (InSANERunManager::GetRunManager()->fVerbosity > 0) printf("  - Creating Static Global Variable fgBigcalHits \n");
   }
   fBigcalHits = fgBigcalHits;

   if (!fgBigcalTimingGroupHits) {
      fgBigcalTimingGroupHits = new TClonesArray("BigcalHit", 5);
   }
   fBigcalTimingGroupHits = fgBigcalTimingGroupHits;

   if (!fgBigcalTriggerGroupHits) {
      fgBigcalTriggerGroupHits = new TClonesArray("BigcalHit", 1);
   }
   fBigcalTriggerGroupHits = fgBigcalTriggerGroupHits;

   return(0);
}
//______________________________________________________________________________
void BigcalEvent::ClearEvent(const char * option) {

   InSANEDetectorEvent::ClearEvent(option);

   if (fBigcalHits)if(fBigcalHits->GetEntries() > 250 ) {
       //std::cout << " calling  fBigcalHits->ExpandCreate(20); " << " had " << fBigcalHits->GetEntries() << " entries. \n";
       fBigcalHits->ExpandCreate(100);
   }
   if (fBigcalHits)       fBigcalHits->Clear(option);

   if (fBigcalTimingGroupHits)if(fBigcalTimingGroupHits->GetEntries() > 100 ) {
       //std::cout << " calling  fBigcalTimingGroupHits->ExpandCreate(20); " << " had " << fBigcalTimingGroupHits->GetEntries() << " entries. \n";
       fBigcalTimingGroupHits->ExpandCreate(10);
   }
   if (fBigcalTimingGroupHits)  fBigcalTimingGroupHits->Clear(option);

   if (fBigcalTriggerGroupHits)if(fBigcalTriggerGroupHits->GetEntries() > 50 ) {
       //std::cout << " calling  fBigcalTriggerGroupHits->ExpandCreate(10); " << " had " << fBigcalTriggerGroupHits->GetEntries() << " entries. \n";
       fBigcalTriggerGroupHits->ExpandCreate(20);
   }
   if (fBigcalTriggerGroupHits) fBigcalTriggerGroupHits->Clear(option);

   fNumberOfHits = 0;

   fNumberOfADCHits = 0;
   fNumberOfTDCHits = 0;

   fNumberOfTimedADCHits = 0;
   fNumberOfTimedTDCHits = 0;

   fNumberOfTDCGroupHits = 0;
   fNumberOfTriggerGroupHits = 0;
}
//______________________________________________________________________________

