#include  "InSANEDetector.h"

ClassImp(InSANEDetector)

//______________________________________________________________________________
InSANEDetector::InSANEDetector(const char * n, const char * t) : TNamed(n,t),
   fNumberOfPedestals(0), fNumberOfTDCs(0), fNumberOfScalers(0) {

   fTDCRangeMin          = 0;
   fTDCRangeMax          = 0;
   fADCRangeMin          = 0;
   fADCRangeMax          = 0;
   fTypicalTDCPeak       = 0;
   fTypicalTDCPeakWidth  = 0;
   fNominalTDCCutWidth   = 0;
   fTypicalPedestal      = 0;
   fTypicalPedestalWidth = 0;

   fEvent            = nullptr;
   fCorrectedEvent   = nullptr;
   fEventScaler      = nullptr;
   fCurrentScaler    = nullptr;
   fNextScaler       = nullptr;
   fPreviousScaler   = nullptr;
   fAverageScaler    = nullptr;

   fCoordinateSystem = nullptr;
   fIsBuilt          = false;

   fNoisyChannels.clear();

   fNComponents = 0;

   fNumberOfTDCChannels    = 0;
   fNumberOfADCChannels    = 0;
   fNumberOfScalerChannels = 0;


   // Own the collections so that when the detector is deleted
   // all the objects are deleted too.
   fDetectorCorrections.SetOwner();
   fHistograms.SetOwner();
   fComponents.SetOwner();
   fCompPedestals.SetOwner();
   fCompTimings.SetOwner();
   fPedestals.SetOwner();
   fTimings.SetOwner();
   fEventDisplayHistograms.SetOwner();
}
//_____________________________________________________________________________
InSANEDetector::~InSANEDetector() {
   if (fPreviousScaler) delete [] fPreviousScaler;
   if (fNextScaler) delete [] fNextScaler;
   if (fAverageScaler) delete [] fAverageScaler;
   // delete fHistograms;
}
//_____________________________________________________________________________
Bool_t InSANEDetector::AddPedestal(Int_t id, Double_t val, Double_t width) {
   fPedestals.Add(new InSANEDetectorPedestal(id, val, width));
   fNumberOfPedestals++;
   return(true);
}
//______________________________________________________________________________
Bool_t InSANEDetector::SetPedestal(Int_t id, Double_t val, Double_t width) {
   if (fPedestals.GetEntries() < id) {
      Error("SetPedestal", "pedestal id, %d, does not exist", id);
      return false;
   } else {
      return(GetDetectorPedestal(id)->DefinePedestal(id, val, width));
   }
}
//______________________________________________________________________________
void InSANEDetector::PrintPedestals() const {
   Int_t tot = fPedestals.GetEntries();
   if (tot > 0) {
      GetDetectorPedestal(0)->PrintPedestalHead();
      for (Int_t i = 0; i < tot && i < fNumberOfPedestals ; i++) {
         GetDetectorPedestal(i)->PrintPedestal();
      }
   }
}
//______________________________________________________________________________
void InSANEDetector::PrintTimings() const {
   Int_t tot = fTimings.GetEntries();
   if (tot > 0) {
      GetDetectorTiming(0)->PrintTimingHead();
      for (Int_t i = 0; i < tot && i < fNumberOfTDCs ; i++) {
         GetDetectorTiming(i)->PrintTiming();
      }
   }
}
//______________________________________________________________________________
void InSANEDetector::PrintDetectorConfiguration() const {
   std::cout << "========================================\n";
   std::cout << " Detector : " << GetTitle() << "\n";
   std::cout << " Name     : " << GetName() << "\n";
   std::cout << "========================================\n";
   std::cout << "number of pedestals  : " << GetNumberOfPedestals() << "\n";
   PrintPedestals();
   std::cout << "number of TDCs       : " << GetNumberOfTDCs() << "\n";
   PrintTimings();
}
//______________________________________________________________________________
void InSANEDetector::PrintEvent() const {
   std::cout << "========================================\n";
   std::cout << " Detector : " << GetTitle() << "\n";
   std::cout << " Name     : " << GetName() << "\n";
   std::cout << "========================================\n";
   std::cout << " Event address   : " << fEvent << "\n";
}
//______________________________________________________________________________
void InSANEDetector::AddComponent(InSANEDetectorComponent * comp) {
   if (comp) {
      fComponents.Add(comp);
      fNComponents++;
      /*if(comp->HasTDCValue()){
              fCompTimings.Add(comp->GetTiming());
      }
      if(comp->HasADCValue()){
         fCompPedestals.Add(comp->GetPedestal());
      }*/
   } else {
      std::cout <<  " InSANEDetector::AddComponent  -- NULL COMPONENT IN ARGUMENT\n" ;
   }
}
//______________________________________________________________________________
void InSANEDetector::PrintNoisyChannels() const {
   std::cout << "========================================\n";
   std::cout << " Detector : " << GetTitle() << "\n";
   std::cout << " Name     : " << GetName() << "\n";
   std::cout << "========================================\n";
   std::cout << " Noisy Channel numbers : " << "\n";
   for (unsigned int i = 0; i < fNoisyChannels.size(); i++) {
      std::cout << fNoisyChannels[i] << "\n";
   }
   std::cout << "\n";
   std::cout << "========================================\n";
}
//______________________________________________________________________________
void InSANEDetector::SetScalerAddress(Long64_t * scaler) {
   fCurrentScaler  = scaler;
   fPreviousScaler = new Long64_t[fNumberOfScalers];
   fNextScaler     = new Long64_t[fNumberOfScalers];
   fAverageScaler  = new Long64_t[fNumberOfScalers];
   for (int i = 0; i < fNumberOfScalers; i++) fAverageScaler[i] = 0;
}
//______________________________________________________________________________
InSANEDetectorComponent * InSANEDetector::GetComponent(int n) {
   if (n >= 0 && n < fNComponents) return((InSANEDetectorComponent*)fComponents.At(n));
   Error("GetComponent(Chan)", "Argument chan=%d is out of range", n);
   return(nullptr);
}
//______________________________________________________________________________

