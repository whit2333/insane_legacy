#include "InSANEApparatus.h"


ClassImp(InSANEApparatus)

//______________________________________________________________________________
InSANEApparatus::InSANEApparatus(const char * name, const char * title): TNamed(name, title) {
   fCoordSystem = nullptr;//new HumanCoordinateSystem();
   fPosition.SetXYZ(0, 0, 0);
}
//______________________________________________________________________________
InSANEApparatus::~InSANEApparatus() {
}
//______________________________________________________________________________
void InSANEApparatus::Print(Option_t *) const {
   std::cout <<  "Apparatus : " << this->GetName() << "\n";
}
//______________________________________________________________________________

