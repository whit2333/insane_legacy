#define InSANEScalerAnalysis_cxx
#include "SANEEvents.h"

#include "InSANEScalerAnalysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "BETAEvent.h"
#include "GasCherenkovHit.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "TClonesArray.h"

ClassImp(InSANEScalerAnalysis)

//__________________________________________________
InSANEScalerAnalysis::InSANEScalerAnalysis(const char * sourceTreeName)
{
// DEBUG LEVEL
   fDebug = 3;

// These are the trees used in analysis
   fOutputTree = nullptr;
   fAnalysisTree = nullptr;

   fRunManager = (SANERunManager*) SANERunManager::GetRunManager();
   fRunNumber = fRunManager->GetCurrentRun()->GetRunNumber();

   fAnalysisFile = fRunManager->GetScalerFile();
   if (fAnalysisFile) fAnalysisFile->cd();

   printf("- Creating Events\n");
/// Allocates and sets branches for event,hit memory from the sourceTreeName
   fSANEScalers = new SANEScalers(sourceTreeName);

   if (fAnalysisFile) fAnalysisFile->cd();

   fTrackerDetector   = new ForwardTrackerDetector(fRunNumber);
   fCherenkovDetector = new GasCherenkovDetector(fRunNumber);
   fCherenkovDetector->SetScalerAddress(&(fSANEScalers->fScalerEvent->fScalers[288]));
   fHodoscopeDetector = new LuciteHodoscopeDetector(fRunNumber);
   fBigcalDetector    = new BigcalDetector(fRunNumber);

   AllocateHistograms();

}
//_______________________________________________________//

InSANEScalerAnalysis::~InSANEScalerAnalysis()
{
//   delete fTrackerDetector;
//   delete fCherenkovDetector;
//   delete fHodoscopeDetector;
//   delete fBigcalDetector;

}
//_______________________________________________________//

Int_t InSANEScalerAnalysis::AnalyzeRun()
{

///// EVENT LOOP  //////
   Int_t nevent = fSANEScalers->fTree->GetEntries();
   std::cout << "\n" << nevent << " ENTRIES \n";


   for (int iEVENT = 0; iEVENT < nevent; iEVENT++)     {
      fSANEScalers->GetEvent(iEVENT);


   } // END OF EVENT LOOP

   return(0);
}
//__________________________________________________

Int_t InSANEScalerAnalysis::Visualize()
{


   return(0);
}

