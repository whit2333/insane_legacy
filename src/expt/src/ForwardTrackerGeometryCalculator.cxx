#include "ForwardTrackerGeometryCalculator.h"
#include <TObject.h>
#include "TMath.h"
#include <iostream>
ClassImp(ForwardTrackerGeometryCalculator)

ForwardTrackerGeometryCalculator * ForwardTrackerGeometryCalculator::fgForwardTrackerGeometryCalculator = nullptr;

ForwardTrackerGeometryCalculator * ForwardTrackerGeometryCalculator::GetCalculator()
{
   if (!fgForwardTrackerGeometryCalculator) fgForwardTrackerGeometryCalculator = new ForwardTrackerGeometryCalculator();
   return(fgForwardTrackerGeometryCalculator);
}

ForwardTrackerGeometryCalculator::~ForwardTrackerGeometryCalculator()
{
   ;
}

ForwardTrackerGeometryCalculator::ForwardTrackerGeometryCalculator()
{

   fDistanceFromTarget = 49.0; //cm
   //fDistanceFromTarget = 49.4; //cm
   //fDistanceFromTarget = 51.5; //cm

   fNLayers = 3;
   fNY1Scints = 128;
   fNY2Scints = 128;
   fNX1Scints = 64;
   fNScints = fNY1Scints + fNY2Scints + fNX1Scints;

     fY1ModuleWidth[0] = 26.35;//   26.35     26.52   26.52
     fY1ModuleWidth[1] = 26.70;//   53.05     26.94   53.45
     fY1ModuleWidth[2] = 26.85;//   79.90     26.99   80.44
     fY1ModuleWidth[3] = 27.05;//  106.95     27.01  107.45
     fY1ModuleWidth[4] = 27.20;//  134.15     27.07  134.52
     fY1ModuleWidth[5] = 27.40;//  161.55     27.25  161.77
     fY1ModuleWidth[6] = 27.45;//  189.00     27.33  189.10
     fY1ModuleWidth[7] = 27.50;//  216.50     27.36  216.46
     fY1ModuleWidth[8] = 27.50;//  244.00     27.40  243.86
     fY1ModuleWidth[9] = 27.50;//  271.50     27.44  271.29
     fY1ModuleWidth[10] = 27.75;//  299.25     27.44  298.74
     fY1ModuleWidth[11] = 27.90;//  327.15     27.47  326.20
     fY1ModuleWidth[12] = 27.90;//  355.05     27.48  353.68
     fY1ModuleWidth[13] = 27.95;//  383.00     27.48  381.16
     fY1ModuleWidth[14] = 27.95;//  410.95     27.63  408.79
     fY1ModuleWidth[15] = 28.10;//  439.05     27.65  436.44

     fY1ModuleWidthSum[0] = 26.35;//     26.52   26.52
     fY1ModuleWidthSum[1] = 53.05;//     26.94   53.45
     fY1ModuleWidthSum[2] = 79.90;//     26.99   80.44
     fY1ModuleWidthSum[3] = 106.95;//     27.01  107.45
     fY1ModuleWidthSum[4] = 134.15;//     27.07  134.52
     fY1ModuleWidthSum[5] = 161.55;//     27.25  161.77
     fY1ModuleWidthSum[6] = 189.00;//     27.33  189.10
     fY1ModuleWidthSum[7] = 216.50;//     27.36  216.46
     fY1ModuleWidthSum[8] = 244.00;//     27.40  243.86
     fY1ModuleWidthSum[9] = 271.50;//     27.44  271.29
     fY1ModuleWidthSum[10] = 299.25 ;//    27.44  298.74
     fY1ModuleWidthSum[11] = 327.15 ;//    27.47  326.20
     fY1ModuleWidthSum[12] = 355.05;//     27.48  353.68
     fY1ModuleWidthSum[13] = 383.00;//     27.48  381.16
     fY1ModuleWidthSum[14] = 410.95;//     27.63  408.79
     fY1ModuleWidthSum[15] = 439.05;//     27.65  436.44

     fY2ModuleWidth[0] = 26.52;//   26.52
     fY2ModuleWidth[1] = 26.94;//   53.45
     fY2ModuleWidth[2] = 26.99;//   80.44
     fY2ModuleWidth[3] = 27.01;//  107.45
     fY2ModuleWidth[4] = 27.07;//  134.52
     fY2ModuleWidth[5] = 27.25;//  161.77
     fY2ModuleWidth[6] = 27.33;//  189.10
     fY2ModuleWidth[7] = 27.36;//  216.46
     fY2ModuleWidth[8] = 27.40;//  243.86
     fY2ModuleWidth[9] = 27.44;//  271.29
     fY2ModuleWidth[10] = 27.44;//  298.74
     fY2ModuleWidth[11] = 27.47;//  326.20
     fY2ModuleWidth[12] = 27.48;//  353.68
     fY2ModuleWidth[13] = 27.48;//  381.16
     fY2ModuleWidth[14] = 27.63;//  408.79
     fY2ModuleWidth[15] = 27.65;//  436.44

     fY2ModuleWidthSum[0] = 26.52;//
     fY2ModuleWidthSum[1] = 53.45;//
     fY2ModuleWidthSum[2] = 80.44;//
     fY2ModuleWidthSum[3] = 107.45;//
     fY2ModuleWidthSum[4] = 134.52;//
     fY2ModuleWidthSum[5] = 161.77;//
     fY2ModuleWidthSum[6] = 189.10;//
     fY2ModuleWidthSum[7] = 216.46;//
     fY2ModuleWidthSum[8] = 243.86;//
     fY2ModuleWidthSum[9] = 271.29;//
     fY2ModuleWidthSum[10] = 298.74;//
     fY2ModuleWidthSum[11] = 326.20;//
     fY2ModuleWidthSum[12] = 353.68;//
     fY2ModuleWidthSum[13] = 381.16;//
     fY2ModuleWidthSum[14] = 408.79;//
     fY2ModuleWidthSum[15] = 436.44;//

     fX1ModuleWidth[0] = 28.00;//   28.00
     fX1ModuleWidth[1] = 28.00;//   56.00
     fX1ModuleWidth[2] = 27.83;//   83.83
     fX1ModuleWidth[3] = 27.53;//  111.37
     fX1ModuleWidth[4] = 27.68;//  139.05
     fX1ModuleWidth[5] = 27.67;//  166.72
     fX1ModuleWidth[6] = 27.62;//  194.34
     fX1ModuleWidth[7] = 28.28;//  222.62

     fX1ModuleWidthSum[0] = 28.00;
     fX1ModuleWidthSum[1] = 56.00;
     fX1ModuleWidthSum[2] = 83.83;
     fX1ModuleWidthSum[3] = 111.37;
     fX1ModuleWidthSum[4] = 139.05;
     fX1ModuleWidthSum[5] = 166.72;
     fX1ModuleWidthSum[6] = 194.34;
     fX1ModuleWidthSum[7] = 222.62;

   // scinth is the width of each wrapped scint without the tedlar between each module (0.25 mm for each module).
   for( int i = 0; i<NTRACKERYMODULES; i++){
      fY1ModuleScintWidth[i] = (fY1ModuleWidth[i] - 0.25)/8.0;
      fY2ModuleScintWidth[i] = (fY2ModuleWidth[i] - 0.25)/8.0;
   }
   for( int i = 0; i<NTRACKERXMODULES; i++){
      fX1ModuleScintWidth[i] = (fX1ModuleWidth[i] - 0.25)/8.0;
   }

   fXScintLength    = 22.0; //cm, lenth of horizontal scints 
   fYScintLength    = 40.0; //cm, lenth of vertical scints
   fScintSideLength = 0.3;  //cm
   fWLSThickness        = 0.12;       //cm

   fX1HalfLength      = 22.262/2.0; //cm
   fY1HalfLength      = 43.905/2.0; //cm
   fY2HalfLength      = 43.644/2.0; //cm

   fG10FrameThickness   = 0.32; //cm
   fFrameWindowHeight   = 52.0; //cm
   fFrameWindowWidth    = 28.0; //cm
   fFrameOpeningHeight  = 45.0; //cm
   fFrameOpeningWidth   = 21.0; //cm

   fY1TotalThickness    = 0.97;  //cm 0.96 in material, 0.97 for positioning
   fY2TotalThickness    = 0.615;  //cm 0.61 in material, 0.615 for positioning
   fX1TotalThickness    = 0.615;   //cm 0.61 in material, 0.615 for positioning
   fY1Y2SeparationCorrection = 0.0;//-1.4; //cm Bring Y1 and Y2 2mm closer (symmetrically) 
   fTotalFrameThickness = 1.3;    //1.28 cm in material -> 1.3 with extra gaps and non-ideal shapes
   fTotalScintThickness = 2.2;    // sum of all scint layers, 2.18 cm in material -> 2.2 with extra gaps and non-ideal shapes
   fTotalThickness      = fTotalFrameThickness + fTotalScintThickness; // = 3.5 cm 

   fScintThickness      = 0.3;        //cm
   fScintWidth          = fScintThickness;

   fSurveyYaw           = -0.07*TMath::Pi()/180.0;
   fSurveyPitch         = -0.71*TMath::Pi()/180.0;
   fSurveyRoll          = -0.001*TMath::Pi()/180.0;

   fX1Roll              = -0.45*TMath::Pi()/180.0;

   /// Following the survey report definitions of yaw,pitch and roll.
   /// "A + yaw is counter clockwise looking from above, a + pitch is ccw
   /// looking from the beam right, and a + roll is cw looking from upstream."
   fDetectorRotation.SetToIdentity();
   fDetectorRotation.RotateY(1.0*fSurveyYaw);
   fDetectorRotation.RotateX(-1.0*fSurveyPitch);
   fDetectorRotation.RotateZ(1.0*fSurveyRoll);

   Double_t YcommonOffset = 0.05; // 2mm 
   fX1Offset = -fScintThickness / 2.0 - 0.0*fScintThickness ; // Added so the position is in the middle of the bar
   fY1Offset = -fScintThickness / 2.0 - 1.5*fScintThickness + YcommonOffset ; // Added so the position is in the middle of the bar
   fY2Offset = -fScintThickness       - 1.5*fScintThickness + YcommonOffset ;       // The Y2 plane is offset by half the side length

   fTrackerBBoxOrigin.SetXYZ(0.0,0.0,fDistanceFromTarget + (fTotalThickness)/2.0);

   /// All 8 corners of the original bounding box
   TVector3 xcorners[8];
   xcorners[0] = TVector3(fFrameWindowWidth/2.0,fFrameWindowHeight/2.0,fTotalThickness/2.0);
   xcorners[1] = TVector3(-fFrameWindowWidth/2.0,fFrameWindowHeight/2.0,fTotalThickness/2.0);
   xcorners[2] = TVector3(-fFrameWindowWidth/2.0,-fFrameWindowHeight/2.0,fTotalThickness/2.0);
   xcorners[3] = TVector3(fFrameWindowWidth/2.0,-fFrameWindowHeight/2.0,fTotalThickness/2.0);
   xcorners[4] = TVector3(fFrameWindowWidth/2.0,fFrameWindowHeight/2.0,-fTotalThickness/2.0);
   xcorners[5] = TVector3(-fFrameWindowWidth/2.0,fFrameWindowHeight/2.0,-fTotalThickness/2.0);
   xcorners[6] = TVector3(-fFrameWindowWidth/2.0,-fFrameWindowHeight/2.0,-fTotalThickness/2.0);
   xcorners[7] = TVector3(fFrameWindowWidth/2.0,-fFrameWindowHeight/2.0,-fTotalThickness/2.0);
   /// Rotation of all the corners
   for(int i = 0;i<8;i++) xcorners[i] = fDetectorRotation*xcorners[i];
   Double_t cornermin[3] = { 9999, 9999 ,9999};
   Double_t cornermax[3] = { -9999, -9999 ,-9999};
   for(int i = 0;i<8;i++){
      if(xcorners[i].X() > cornermax[0]) cornermax[0] = xcorners[i].X();
      if(xcorners[i].Y() > cornermax[1]) cornermax[1] = xcorners[i].Y();
      if(xcorners[i].Z() > cornermax[2]) cornermax[2] = xcorners[i].Z();
      if(xcorners[i].X() < cornermin[0]) cornermin[0] = xcorners[i].X();
      if(xcorners[i].Y() < cornermin[1]) cornermin[1] = xcorners[i].Y();
      if(xcorners[i].Z() < cornermin[2]) cornermin[2] = xcorners[i].Z();
   }
   fTrackerBBoxDim.SetXYZ( cornermax[0] - cornermin[0] ,
                           cornermax[1] - cornermin[1] , 
                           cornermax[2] - cornermin[2] );

   fTrackerDetectorBox.SetXYZ( fFrameWindowWidth ,fFrameWindowHeight, fTotalThickness );

   fY1BBoxDim.SetXYZ( fXScintLength ,fY1HalfLength*2.0 + TMath::Abs(fY1Offset)*2.0, fY1TotalThickness );
   fY2BBoxDim.SetXYZ( fXScintLength ,fY2HalfLength*2.0 + TMath::Abs(fY2Offset)*2.0, fY2TotalThickness );
   fX1BBoxDim.SetXYZ( fX1HalfLength*2.0+ TMath::Abs(fX1Offset)*20.0 ,fYScintLength, fX1TotalThickness );

   /// deprecated
   fScintWrapThickness  = 0.043007;   //cm
   fLayerGap            = 0.05;       //cm
   fScintTotalThickness = fScintThickness + 2.0*fScintWrapThickness + 2.0*fWLSThickness; //
   fScintTotalWidth     = fScintThickness + 2.0*fScintWrapThickness; //
   fThickness           = 3.0*fScintTotalThickness + 2.0*fLayerGap;

   // subtract one from the Y layers because we count only the overlap
   fTrackerActiveBBoxDim.SetXYZ( ((Double_t)fNX1Scints)*fScintTotalWidth,
                                 ((Double_t)(TMath::Min(fNY1Scints,fNY2Scints)-1))*fScintTotalWidth,
                                 fThickness );

}
//_____________________________________________________________________________

Int_t ForwardTrackerGeometryCalculator::GetLayerNumber(Int_t chan) const
{
   if (chan < 1 || chan > fNScints) {
      Warning("ForwardTrackerGeometryCalculator::GetLayerNumber(chan)", "Bad channel number.");
      return(0);
   }
   if (chan - fNX1Scints <= 0) return(0) ;
   chan = chan - fNX1Scints;
   if (chan - fNY1Scints <= 0) return(1) ;
   /*else*/
   return(2) ;
}
//_____________________________________________________________________________

Int_t ForwardTrackerGeometryCalculator::GetScintNumber(Int_t chan) const
{
   if (chan < 1 || chan > fNScints) {
      Warning("ForwardTrackerGeometryCalculator::GetLayerNumber(chan)", "Bad channel number.");
      return(0);
   }
   if (chan - fNX1Scints <= 0) return(chan) ;
   chan = chan - fNX1Scints;
   if (chan - fNY1Scints <= 0) return(chan) ;
   /*else*/
   return(chan - fNY1Scints) ;
}
//_____________________________________________________________________________

Int_t ForwardTrackerGeometryCalculator::GetScintNumber(Int_t layer, Int_t row) const
{
   if (layer == 0) return(row);
   if (layer == 1) return(fNX1Scints + row);
   if (layer == 2) return(fNX1Scints + fNY1Scints + row);
   /* else */
   return(-1);
}
//_____________________________________________________________________________

Int_t   ForwardTrackerGeometryCalculator::GetChannelNumber(Int_t layer, Int_t scintNum)const
{
   if (layer < 0 || layer > fNLayers) {
      Warning("ForwardTrackerGeometryCalculator::GetChannelNumber(layer,scint)", "Bad layer number");
      return(0);
   }
   if (layer == 0 && (scintNum < 1 || scintNum > fNX1Scints)) {
      Warning("ForwardTrackerGeometryCalculator::GetChannelNumber(layer,scint)", "Bad scint Number");
      return(0);
   }
   if (layer == 1 && (scintNum < 1 || scintNum > fNY1Scints)) {
      Warning("ForwardTrackerGeometryCalculator::GetChannelNumber(layer,scint)", "Bad scint Number");
      return(0);
   }
   if (layer == 2 && (scintNum < 1 || scintNum > fNY2Scints)) {
      Warning("ForwardTrackerGeometryCalculator::GetChannelNumber(layer,scint)", "Bad scint Number");
      return(0);
   }
   return(GetScintNumber(layer, scintNum));
}
//_____________________________________________________________________________

Double_t ForwardTrackerGeometryCalculator::GetX1Position(Int_t scintNum) const
{
   if (scintNum < 1 || scintNum > fNX1Scints) {
      Warning("ForwardTrackerGeometryCalculator::GetX1Position(Int_t scintNum)", "Argument scintNum is out of range. Returning -100.0");
      return(-30.0);
   } /*else*/
   // set position to the bottom, add the offset, add the distance starting from the bottom.
   return(-fX1HalfLength  + fX1Offset + GetScintPositionInLayer(0,scintNum) );
}
//_____________________________________________________________________________

Double_t ForwardTrackerGeometryCalculator::GetY1Position(Int_t scintNum) const
{
   if (scintNum < 1 || scintNum > fNY1Scints) {
      Warning("GetY1Position(Int_t scintNum)", "Argument scintNum: %d, is out of range. Returning -100.0",scintNum);
      return(-30.0);
   } /*else*/
   //return( -fYScintLength/2.0 + fScintSideLength*((Double_t)scintNum) + fY1Offset );
   //return(-fScintTotalWidth * ((Double_t)fNY1Scints) / 2.0 +  fScintTotalWidth * ((Double_t)scintNum) + fY1Offset);
   return(-fY1HalfLength  + fY1Offset + GetScintPositionInLayer(1,scintNum) );
}
//_____________________________________________________________________________

Double_t ForwardTrackerGeometryCalculator::GetY2Position(Int_t scintNum) const
{
   if (scintNum < 1 || scintNum > fNY2Scints) {
      Warning("ForwardTrackerGeometryCalculator::GetY2Position(Int_t scintNum)", "Argument scintNum is out of range. Returning -100.0");
      return(-30.0);
   } /*else*/
   //return( -fYScintLength/2.0 + fScintSideLength*((Double_t)scintNum) + fY2Offset );
   //return(-fScintTotalWidth * ((Double_t)fNY2Scints) / 2.0 +  fScintTotalWidth * ((Double_t)scintNum) + fY2Offset);
   return(-fY2HalfLength  + fY2Offset + GetScintPositionInLayer(2,scintNum) );

}
//_____________________________________________________________________________

Int_t ForwardTrackerGeometryCalculator::GetModuleNumber(Int_t row) const {
      if(row <= 0  || row > 128 ){
         Error("GetModuleNumber","Row number is out of range : %d",row);
         return(0);
      }
      return( (row-1)/8 );
}
//_____________________________________________________________________________

Double_t ForwardTrackerGeometryCalculator::GetScintPositionInLayer(Int_t chan) const {
      Int_t     layer  = GetLayerNumber(chan);
      Int_t     row    = GetRowNumber(chan);
   return( GetScintPositionInLayer(layer,row) );
}

//_____________________________________________________________________________

Double_t ForwardTrackerGeometryCalculator::GetScintPositionInLayer(Int_t layer,Int_t row) const {
      Int_t     module = GetModuleNumber(row);
      Int_t     n      = 7 - (row-1)%8; // number of scints above this scint. 
      Double_t  h      = 0;
      //Double_t  w      = 0;
      Double_t  scintw = 0;

      // scinth is the width of each wrapped scint without the tedlar between each module (0.25 mm for each module).
      if( layer == 1 ){
         h = fY1ModuleWidthSum[module];
         //w = fY1ModuleWidth[module];
         scintw = fY1ModuleScintWidth[module];
      }
      else if( layer == 2 ){
         h = fY2ModuleWidthSum[module];
         //w = fY2ModuleWidth[module];
         scintw = fY2ModuleScintWidth[module];
      }
      else if( layer == 0 ){
         h = fX1ModuleWidthSum[module];
         //w = fX1ModuleWidth[module];
         scintw = fX1ModuleScintWidth[module];
      }
      // Subtract from the measured total width (up to and including this module) the (module dependent) 
      // scintillator width for each scint above the one of interest. Then half a scint width to get the 
      // center. Also subtract (half) the tedlar that is at the end of the modules
      Double_t res = h - ((double)n + 0.5)*scintw - 0.125;
      return( res/10.0); // convert to cm
   }
//_____________________________________________________________________________

Double_t ForwardTrackerGeometryCalculator::GetZPosition(Int_t layer,Int_t scintNum) const {
      //
      Double_t deltaz = 0;
      Double_t res    = GetDistanceFromTarget();
      if (layer == 1 ) {
         res += 2.0*fG10FrameThickness + fX1TotalThickness + (fY1TotalThickness-fY1Y2SeparationCorrection)/2.0;
         if( scintNum != -1) {
            fScintZPos.SetXYZ(0.0,GetY1Position(scintNum),0.0);
            fScintZPos = fDetectorRotation*fScintZPos;
            deltaz = fScintZPos.Z();
            res += deltaz;
         }
      } else if (layer == 2 ) {
         res += 3.0*fG10FrameThickness + fX1TotalThickness + fY1TotalThickness  + (fY2TotalThickness+fY1Y2SeparationCorrection)/2.0;
         if( scintNum != -1) {
            fScintZPos.SetXYZ(0.0,GetY1Position(scintNum),0.0);
            fScintZPos = fDetectorRotation*fScintZPos;
            deltaz = fScintZPos.Z();
            res += deltaz;
         }
      } else if (layer == 0 ) {
         res += fG10FrameThickness + fX1TotalThickness/2.0;
         if( scintNum != -1) {
            fScintZPos.SetXYZ(GetX1Position(scintNum),0.0,0.0);
            fScintZPos = fDetectorRotation*fScintZPos;
            deltaz = fScintZPos.Z();
            res += deltaz;
         }
      }
      return(res);
   }
//_____________________________________________________________________________

TVector3   ForwardTrackerGeometryCalculator::GetLayerBoundingBoxDimensions(Int_t layer){
   if(layer == 1 ) return( fY1BBoxDim );
   else if(layer == 2 ) return( fY2BBoxDim );
   else if(layer == 0 ) return( fX1BBoxDim );
   else {
      Error("GetLayerBoundingBoxDimensions","Bad layer number: %d",layer);
      return( TVector3(0,0,0));
   }
}
//_____________________________________________________________________________

Double_t ForwardTrackerGeometryCalculator::GetScintHeight(Int_t chan) const {
      Int_t     layer  = GetLayerNumber(chan);
      Int_t     row    = GetRowNumber(chan);
   return( GetScintHeight(layer,row) );
}
//_____________________________________________________________________________

Double_t ForwardTrackerGeometryCalculator::GetScintHeight(Int_t layer,Int_t row) const {
      Int_t     module = GetModuleNumber(row);
      if( layer == 1 ){
         return(fY1ModuleScintWidth[module] );
      }
      else if( layer == 2 ){
         return(fY2ModuleScintWidth[module] );
      }
      else if( layer == 0 ){
         return(fX1ModuleScintWidth[module] );
      } else {
         Error("GetScintHeight","Bad layer number: %d",layer);
         return(0.0);
      }
}
//_____________________________________________________________________________


//_____________________________________________________________________________

