#include "InSANECorrection.h"

ClassImp(InSANECorrection)

//______________________________________________________________________________
InSANECorrection::InSANECorrection(InSANEAnalysis * analysis) {
   fIsInitialized  = false;
   fTransientDatum = nullptr;
   fDescription    = "";
}
//______________________________________________________________________________
InSANECorrection::~InSANECorrection() {
}
//______________________________________________________________________________
void InSANECorrection::SetEvents(InSANEAnalysisEvents * e) { fTransientDatum = e; }
//______________________________________________________________________________
InSANEAnalysisEvents * InSANECorrection::GetEvents() const { return(fTransientDatum); }
//______________________________________________________________________________
bool InSANECorrection::IsEventsSet() const {
   if (fTransientDatum != nullptr) return true;
   else return false;
}
//______________________________________________________________________________
Bool_t InSANECorrection::IsInitialized() const { return(fIsInitialized); }
//______________________________________________________________________________
void   InSANECorrection::SetInitialized() { fIsInitialized = true; }
//______________________________________________________________________________




