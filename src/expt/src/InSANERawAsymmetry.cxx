#include "InSANERawAsymmetry.h"
#include <cmath>

ClassImp(InSANERawAsymmetry)

//________________________________________________________________________________
InSANERawAsymmetry::InSANERawAsymmetry(){
   fDilution        = 0.12;
   fHalfWavePlate   = 0.0;
   fPt              = 1.0;
   fPb              = 1.0;
   fAsymmetry       = 0.0;
   fError           = 0.0;
   fLiveTime[0]     = 1.0;
   fLiveTime[1]     = 1.0;
   fChargeSwap      = false;
   fCharge[0]       = 1.0;
   fCharge[1]       = 1.0;
   fRunNumber       = 0;

   fQ2Hist = new TH1F("fQ2Hist","Q2;Q^{2} [GeV^{2}/c^{2}]",100,0,10.0);
   fxHist  = new TH1F("fxHist","x;x",100,0.0,1.0);
   fWHist  = new TH1F("fWHist","W ; W [GeV]",100,0.0,5.0);

   fHistograms.Add(fQ2Hist);
   fHistograms.Add(fxHist);
   fHistograms.Add(fWHist);
}
//________________________________________________________________________________
InSANERawAsymmetry::~InSANERawAsymmetry(){
}
//________________________________________________________________________________
void InSANERawAsymmetry::Print(){
   PrintTableHeader(std::cout);
   PrintTable(std::cout);

}
//________________________________________________________________________________
void InSANERawAsymmetry::PrintTableHeader(std::ostream &out){
   out << std::setw(10) << "RunNumber" << "\t" 
       << std::setw(10) << "Count[0]" << "\t"  
       << std::setw(10) << "Count[1]" << "\t"  
       << std::setw(10) << "LiveTime[0]" << "\t"  
       << std::setw(10) << "LiveTime[1]" << "\t"  
       << std::setw(10) << "Charge[0]" << "\t"  
       << std::setw(10) << "Charge[1]" << "\t"  
       << std::setw(10) << "Dilution" << "\t"  
       << std::setw(10) << "Pt" << "\t"  
       << std::setw(10) << "Pb" << "\t"  
       << std::setw(10) << "Asymmetry" << "\t"  
       << std::setw(10) << "Error_stat" << "\t"  
       << std::setw(10) << "HWP" << "\t"  
       << std::setw(10) << "Q_swap" << "\t"  
       << std::setw(10) << "Q2_mean" << "\t"  
       << std::setw(10) << "x_mean" << "\t"  
       << std::setw(10) << "W_mean" << "\t"  
       << std::endl;

}
//________________________________________________________________________________
void InSANERawAsymmetry::PrintTable(std::ostream &out){
   out << std::setw(10) << fRunNumber << "\t" 
       << std::setw(10) << fCount[0] << "\t"  
       << std::setw(10) << fCount[1] << "\t"  
       << std::setw(10) << fLiveTime[0] << "\t"  
       << std::setw(10) << fLiveTime[1] << "\t"  
       << std::setw(10) << fCharge[0] << "\t"  
       << std::setw(10) << fCharge[1] << "\t"  
       << std::setw(10) << fDilution << "\t"  
       << std::setw(10) << fPt << "\t"  
       << std::setw(10) << fPb << "\t"  
       << std::setw(10) << fAsymmetry << "\t"  
       << std::setw(10) << fError << "\t"  
       << std::setw(10) << fHalfWavePlate << "\t"  
       << std::setw(10) << fChargeSwap << "\t"  
       << std::setw(10) << GetMeanQ2() << "\t"  
       << std::setw(10) << GetMeanx() << "\t"  
       << std::setw(10) << GetMeanW() << "\t"  
       << std::endl;

}
//________________________________________________________________________________
void InSANERawAsymmetry::Calculate(){
   using namespace TMath;

   Double_t N1 = fCount[0];// /(fCharge[0]*fLiveTime[0]);
   Double_t N2 = fCount[1];// /(fCharge[1]*fLiveTime[1]);
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];
   //Double_t RQ = Q1/Q2;
   Double_t f  = fDilution;
   Double_t Pb = fPb;
   Double_t Pt = fPt;

   Double_t sum  = float(N1/(Q1*L1) + N2/(Q2*L2));
   Double_t diff = float(N1/(Q1*L1) - N2/(Q2*L2));

   //fAsymmetry = fHalfWavePlate/(fDilution*fPt*fPb)*diff/sum; 
   fAsymmetry = 1.0/(fDilution*fPt*fPb)*diff/sum; 
   //fAsymmetry *= TMath::Sign(fPt,*fPt);
   fError = 2*Sqrt((Power(L1,2)*Power(L2,2)*N1*N2*(N1 + N2)*Power(Q1,2)*Power(Q2,2))/(Power(f,2)*Power(Pb,2)*Power(Pt,2)*Power(L1*N2*Q1 + L2*N1*Q2,4))) ;
   //fError     = TMath::Sqrt(( (0.0025 * TMath::Power(N1, 3) - 0.0025 * TMath::Power(N1, 2) * N2 - 0.0025 * N1 * TMath::Power(N2, 2) + 0.00250 * TMath::Power(N2, 3))
   //                           * TMath::Power(fPb, 2) + (0.0025 * TMath::Power(N1, 3) - 0.0025 * TMath::Power(N1, 2) * N2 + 0.002500 * TMath::Power(N2, 3) +
   //                                                    N1 * N2 * (-0.002500 * N2 + 4.*TMath::Power(fPb, 2))) *
   //                                                    TMath::Power(fPt, 2)) / (TMath::Power(N1 + N2, 3) * TMath::Power(fPb, 4) * TMath::Power(fPt, 4))) ;
   //fError = fError/fDilution;

   if( TMath::IsNaN(fAsymmetry) || TMath::IsNaN(fError)  || std::isinf(fAsymmetry) || std::isinf(fError) ) {
      fAsymmetry = 0.0;
      fError = 0.0;
   }

}
//________________________________________________________________________________
Int_t InSANERawAsymmetry::CountEvent()
{
   if (fDISEvent) if (IsGoodEvent()) {
      if ((fDISEvent->fHelicity) == 1) fCount[0]++;
      if ((fDISEvent->fHelicity) == -1) fCount[1]++;
      fxHist->Fill(fDISEvent->fx);
      fWHist->Fill(fDISEvent->fW);
      fQ2Hist->Fill(fDISEvent->fQ2);
      return(1);
   }
   return(0);
}

