#include "BigcalClusteringCalculations.h"

#include "InSANECorrection.h"
#include "BIGCALClusterProcessor.h"
#include "LocalMaxClusterProcessor.h"
#include "BIGCALGeometryCalculator.h"
#include "InSANEReconstruction.h"
#include "Pi0ClusterPair.h"

ClassImp(BigcalClusteringCalculation1)

Int_t BigcalClusteringCalculation1::Calculate()
{

   if (!(fEvents->TRIG->IsClusterable())) return(0);

   /// Execute Clustering of Bigcal Event
   fClusterEvent->fNClusters = fClusterProcessor->ProcessEvent(fClusters);

   /// Now that the Clustering is complete, assign all the relevent detector data
   fClusterEvent->fEventNumber = aBetaEvent->fEventNumber;
   fClusterEvent->fRunNumber = aBetaEvent->fRunNumber;

   BIGCALCluster * aCluster;
   BIGCALCluster * bCluster;

   fClusterEvent->fTwoClusterSeparation = 0.0;
   if (fClusterEvent->fNClusters == 2) {
      aCluster = (BIGCALCluster*)(*fClusters)[0] ;
      bCluster = (BIGCALCluster*)(*fClusters)[1] ;
      fClusterEvent->fTwoClusterSeparation = TMath::Sqrt(TMath::Power(aCluster->fXMoment - bCluster->fXMoment, 2) +
            TMath::Power(aCluster->fYMoment - bCluster->fYMoment, 2));

   }

   /// - Begin loop over clusters
   for (int kk = 0; kk < fClusterEvent->fNClusters ; kk++) {
      aCluster = (BIGCALCluster*)(*fClusters)[kk] ;

      ///   - See if it is a noisy channel
      aCluster->fIsGood = !(fBigcalDetector->IsNoisyChannel(fBigcalDetector->fGeoCalc->GetBlockNumber(aCluster->fiPeak, aCluster->fjPeak)));
      aCluster->fIsNoisyChannel = (fBigcalDetector->IsNoisyChannel(fBigcalDetector->fGeoCalc->GetBlockNumber(aCluster->fiPeak, aCluster->fjPeak)));

      if (fBigcalDetector->fGeoCalc->IsBlockOnPerimeter(aCluster->fiPeak, aCluster->fjPeak)) aCluster->fIsGood = false;


      if (aCluster->GetEnergy() > 6000.0) aCluster->fIsGood = false;

      ///   - For each cluster, build a list of mirrors to sum
      std::vector<int> * mirrors =
         fCherenkovDetector->fGeoCalc->GetMirrorsToAdd(aCluster->fXMoment, aCluster->fYMoment);
      // std::cout << " number of mirrors: " << mirrors->size() << " \n";

      ///   - Sets fClusterTime and fClusterTime2
      if (fBigcalDetector->cPassesTDC[ fBigcalDetector->fGeoCalc->GetGroupNumber(aCluster->fiPeak, aCluster->fjPeak)-1 ])
         aCluster->fGoodBigcalTiming = true;

      if (fBigcalDetector->fBigcalTDCs[fBigcalDetector->fGeoCalc->GetGroupNumber(aCluster->fiPeak, aCluster->fjPeak)-1]->size() > 0)
         aCluster->fClusterTime = fBigcalDetector->fBigcalTDCAligns[fBigcalDetector->fGeoCalc->GetGroupNumber(aCluster->fiPeak, aCluster->fjPeak)-1]->at(0);
      /*fBigcalDetector->fEventTimingHist->GetBinContent( fBigcalDetector->fGeoCalc->GetTDCGroup(aCluster->fiPeak,aCluster->fjPeak), BCrows.at(0));*/

      ///   - Get the array of BC TDC rows  involved for the cluster's centroid
      ///     Cluster has 1 or 2 rows, depending on the tdc grouping overlap.
      std::vector<Int_t> BCrows =
         fBigcalDetector->fGeoCalc->GetTDCRows(aCluster->fiPeak, aCluster->fjPeak);

      //std::cout << "made it here" << std::endl;

      for(unsigned int iRow = 0; iRow < BCrows.size(); iRow++){
         Int_t trigChan = fBigcalDetector->fGeoCalc->GetSumOf64GroupNumber(aCluster->fiPeak, aCluster->fjPeak)[iRow];
         //std::cout << "   trigChan= " << trigChan << std::endl;
         if (fBigcalDetector->fBigcal64SumTDCAligns[ trigChan-1 ]->size() > 0) {
            if(iRow==0) aCluster->fClusterTime1 = fBigcalDetector->fBigcal64SumTDCAligns[ trigChan-1  ]->at(0);
            if(iRow==1) aCluster->fClusterTime2 = fBigcalDetector->fBigcal64SumTDCAligns[ trigChan-1  ]->at(0);
            if (fBigcalDetector->fBigcal64SumTDCAligns[ trigChan -1 ]->size() > 1) {
               std::cout << "ERROR the bigcal TDC has multiple hits!!" <<  std::endl;
            }
            //if( BCrows.size() == 1 ) {
            //   std::cout << "   trigChan= " << trigChan << std::endl;
            //   std::cout << " Bigcal_ij " << aCluster->fiPeak << " " << aCluster->fjPeak << std::endl; 
            //   std::cout << "     time1 = " << aCluster->fClusterTime1 << std::endl;
            //   std::cout << " " << fBigcalDetector->fBigcal64SumTDCAligns[ trigChan ]->size() << std::endl;
            //   //if( trigChan < 37 )
            //   //   if (fBigcalDetector->fBigcal64SumTDCAligns[ trigChan  ]->size() > 0) {
            //   //      std::cout << " " << fBigcalDetector->fBigcal64SumTDCAligns[ trigChan  ]->at(0) << std::endl;
            //   //   }
            //}
         }
      }

      // old way
      //if (fBigcalDetector->fBigcal64SumTDCAligns[
      //      fBigcalDetector->fGeoCalc->GetSumOf64GroupNumber(aCluster->fiPeak, aCluster->fjPeak)[0] - 1
      //      ]->size() > 0)
      //   aCluster->fClusterTime1 = fBigcalDetector->fBigcal64SumTDCAligns[fBigcalDetector->fGeoCalc->GetSumOf64GroupNumber(aCluster->fiPeak, aCluster->fjPeak)[0] - 1]->at(0);
      //if (fBigcalDetector->fGeoCalc->GetSumOf64GroupNumber(aCluster->fiPeak, aCluster->fjPeak)[1] > 0)
      //   if (fBigcalDetector->fBigcal64SumTDCAligns[fBigcalDetector->fGeoCalc->GetSumOf64GroupNumber(aCluster->fiPeak, aCluster->fjPeak)[1] - 1]->size() > 0)
      //      aCluster->fClusterTime2 =
      //         fBigcalDetector->fBigcal64SumTDCAligns[
      //         fBigcalDetector->fGeoCalc->GetSumOf64GroupNumber(aCluster->fiPeak, aCluster->fjPeak)[1] - 1
      //         ]->at(0);

      //     if(BCrows.size() > 1) {
      //        aCluster->fClusterTime2 = fBigcalDetector->fEventTimingHist->GetBinContent(
      //           fBigcalDetector->fGeoCalc->GetTDCGroup(aCluster->fiPeak,aCluster->fjPeak),
      //           BCrows.at(1));
      //     } else {
      //        aCluster->fClusterTime2 = 0;
      //     }

      //    std::cout << "cluster time1: " <<     aCluster->fClusterTime << " \n";
      // initialize variables
      aCluster->fCherenkovBestADCSum = 0;
      aCluster->fCherenkovBestNPESum = 0.0;
      aCluster->fHelicity            = fEvents->BEAM->fHelicity;
      aCluster->fNumberOfMirrors     = 0;
      aCluster->fGoodCherenkovTDCHit = false;
      aCluster->fGoodCherenkovTiming = true;
      aCluster->fEventNumber         = aBetaEvent->fEventNumber;
      aCluster->fRunNumber           = aBetaEvent->fRunNumber;
      aCluster->fCherenkovAvgTime    = -900;
      aCluster->fCherenkovTDC        = -900;
      Int_t numberOfMirrorTDCHits    = 0;

      /// - Get sub-detector number, fSubDetector
      //aCluster->fSubDetector =  fBigcalDetector->fGeoCalc->GetSubDetectorNumber(aCluster->fXMoment,aCluster->fYMoment);
      if (aCluster->fjPeak > 32) aCluster->fSubDetector = 2;
      else aCluster->fSubDetector = 1;

      /// - Determine the Primary mirror from the cluster centroid
      aCluster->fPrimaryMirror = fCherenkovDetector->fGeoCalc->GetPrimaryMirror(aCluster->fXMoment, aCluster->fYMoment);

      // - Loop over only relevant mirrors and add up quantities
      for (auto it = mirrors->begin(); it != mirrors->end(); ++it) {

         /// Aligned ADC is scaled so that the peak is at 1
         if (fCherenkovDetector->fCherenkovADCsAligned[(*it)-1]->size() > 0) {
            aCluster->fCherenkovBestADCSum   += fCherenkovDetector->fCherenkovADCsAligned[(*it)-1]->at(0);
         }
         ///
         if (fCherenkovDetector->fCherenkovNPEs[(*it)-1]->size() > 0) {
            /*std::cout << "adding " << fCherenkovDetector->fCherenkovNPEs[(*it)-1]->at(0) << " to cer NPE SUM\n";*/
            aCluster->fCherenkovBestNPESum  += fCherenkovDetector->fCherenkovNPEs[(*it)-1]->at(0);
         }
         aCluster->fNumberOfMirrors++;

         ///    - Average all cherenkov times for relevant mirrors
         for (unsigned int i = 0; i < fCherenkovDetector->fCherenkovTDCs[(*it)-1]->size(); i++)  {
            aCluster->fCherenkovAvgTime += (Double_t) fCherenkovDetector->fCherenkovTDCs[(*it)-1]->at(i);
            numberOfMirrorTDCHits++; // denominator after loop over relevant mirrors
         }

         ///    - Average all hits in primary mirror's TDC
         if ((*it) == aCluster->fPrimaryMirror) {
            for (unsigned int i = 0; i < fCherenkovDetector->fCherenkovTDCs[(*it)-1]->size(); i++)
               aCluster->fCherenkovTDC = (Double_t) fCherenkovDetector->fCherenkovTDCsAligned[(*it)-1]->at(i);

            if (fCherenkovDetector->fCherenkovADCs[(*it)-1]->size() > 0)
               aCluster->fCherenkovBestADC = (Int_t) fCherenkovDetector->fCherenkovADCs[(*it)-1]->at(0);
            //          if(fCherenkovDetector->fCherenkovTDCs[(*it)-1]->size() != 0)
            //             aCluster->fCherenkovTDC = (Double_t)aCluster->fCherenkovTDC/((Double_t)fCherenkovDetector->fCherenkovTDCs[(*it)-1]->size());
            //
         }
         if (fCherenkovDetector->fCherenkovADCsAligned[(*it)-1]->size() > 0)
            fClusterEvent->fCherenkovTotalADC_GoodClust += (Double_t) fCherenkovDetector->fCherenkovADCsAligned[(*it)-1]->at(0);
         if (fCherenkovDetector->fCherenkovNPEs[(*it)-1]->size() > 0)fClusterEvent->fCherenkovTotalNPE_GoodClust += (Int_t)fCherenkovDetector->fCherenkovNPEs[(*it)-1]->at(0);

         ///    - If the relevant mirror does not have a TDC hit then it is missing a hit...
         ///      but not exactly a bad cherenkov though...
         /// \todo needs a new name.
         if (!(fCherenkovDetector->cPassesTDC[(*it)-1]))   aCluster->fGoodCherenkovTiming = false;
         if (fCherenkovDetector->cPassesTDC[(*it)-1]) {
            aCluster->fNumberOfGoodMirrors++;

            if (fCherenkovDetector->cPassesTDCAlign[(*it)-1])     /// using TDCAlign !
               if ((*it) == aCluster->fPrimaryMirror)
                  aCluster->fGoodCherenkovTDCHit = true;

            /*        std::cout << "Good Cherenkov TDC HIT\n";*/
            if (fCherenkovDetector->fCherenkovADCsAligned[(*it)-1]->size() > 0)fClusterEvent->fCherenkovTotalADC_GoodTDC += fCherenkovDetector->fCherenkovADCsAligned[(*it)-1]->at(0);
            if (fCherenkovDetector->fCherenkovNPEs[(*it)-1]->size() > 0)fClusterEvent->fCherenkovTotalNPE_GoodTDC += fCherenkovDetector->fCherenkovNPEs[(*it)-1]->at(0);
         }
      } // end loop over relevant mirrors
      if (numberOfMirrorTDCHits != 0)
         aCluster->fCherenkovAvgTime = aCluster->fCherenkovAvgTime / numberOfMirrorTDCHits;

      /// - Loop over ALL mirror and add up quantities
      for (Int_t k = 1; k < 9; k++) {
         if (fCherenkovDetector->fGeoCalc->AddMirrorToClusterSimple(k, aCluster->fXMoment, aCluster->fYMoment)) {
            if (fCherenkovDetector->fCherenkovADCsAligned[k-1]->size() > 0)   aCluster->fCherenkovBestADCChannel =
               fCherenkovDetector->fCherenkovADCsAligned[k-1]->at(0);
            if (fCherenkovDetector->fCherenkovNPEs[k-1]->size() > 0)   aCluster->fCherenkovBestNPEChannel =
               ((Double_t)fCherenkovDetector->fCherenkovNPEs[k-1]->at(0));

            if (fCherenkovDetector->fCherenkovADCsAligned[k-1]->size() > 0)  fClusterEvent->fCherenkovTotalADC +=
               fCherenkovDetector->fCherenkovADCsAligned[k-1]->at(0);
            if (fCherenkovDetector->fCherenkovNPEs[k-1]->size() > 0)   fClusterEvent->fCherenkovTotalNPE +=
               fCherenkovDetector->fCherenkovNPEs[k-1]->at(0);
         }
      }

      if (aCluster->fCherenkovBestNPESum > fCherenkovDetector->cNPEMin &&
            aCluster->fCherenkovBestNPESum < fCherenkovDetector->cNPEMax &&
            aCluster->fTotalE > fBigcalDetector->cClusterEnergyMin /*                &&
                                                                                     aCluster->fGoodCherenkovTDCHit*/)
         aCluster->fGoodCherenkov = true;
      else aCluster->fGoodCherenkov = false;

      /** The cluster type is essentially determined by the coda type.
       *  The only addition here is if there is a pi0 trig located at the border of RCS and Protvino
       *  and there is no cherenkov hit, See BIGCALCluster::fType for types.
       */
      if (fEvents->TRIG->IsBETA1Event()) aCluster->fType = 1;
      else if (fEvents->TRIG->IsBETA2Event() && !(fEvents->TRIG->IsPi0Event())) aCluster->fType = 2;
      else if (!(fEvents->TRIG->IsBETA2Event()) && (fEvents->TRIG->IsPi0Event())) aCluster->fType = 3;
      else if (fEvents->TRIG->IsBETA2Event()) aCluster->fType = 4;
      else if (fEvents->TRIG->IsCoinEvent()) aCluster->fType = 5;
      /*     else if( fEvents->TRIG->IsPi0Event() && !aCluster->fGoodCherenkovTDCHit &&
             ( fBigcalDetector->fGeoCalc->GetTDCRow(aCluster->fiPeak,aCluster->fjPeak)[0] >= 10 && fBigcalDetector->fGeoCalc->GetTDCRow(aCluster->fiPeak,aCluster->fjPeak)[0] <= 12 ) ) aCluster->fType = 4;*/

      else  aCluster->fType = 10;

      // -------------------------------------------------
      // Lucite quantities
      aCluster->fGoodLuciteTDCHit       = false;
      aCluster->fGoodDoubleLuciteTDCHit = false;
      aCluster->fGoodLucitePosition     = false;
      aCluster->fGoodDoubleLuciteTiming = false;
      aCluster->fGoodLuciteTiming       = false;

      for (int kk = 0; kk < aBetaEvent->fLuciteHodoscopeEvent->fLuciteHits->GetEntries(); kk++) {

         aLucHit = (LuciteHodoscopeHit*)(*fLucHits)[kk] ;

         if (aLucHit->fLevel > 0) { // tdc

            // If the lucite hit (approximately close to the cluster)
            if ( fHodoscopeDetector->fGeoCalc->GetLuciteRow(aCluster->fjPeak) - 2 <= aLucHit->fRow &&
                 fHodoscopeDetector->fGeoCalc->GetLuciteRow(aCluster->fjPeak) + 2 >= aLucHit->fRow)
            {
               // If the hit passes the defined timing cut then the cluster has a
               // "Good lucite TDC Hit"
               if ( aLucHit->fPassesTimingCut > 0 ) 
               {
                  aCluster->fGoodLuciteTDCHit = true;
               }

               // If there are tdc hits on both sides of the bar then it has Good timing
               if ( fHodoscopeDetector->fNegativeTDCHits[aLucHit->fRow-1]->size() > 0 &&
                    fHodoscopeDetector->fPositiveTDCHits[aLucHit->fRow-1]->size() > 0)
               {
                  aCluster->fGoodLuciteTiming = true;
               }

               if (aCluster->fGoodLuciteTiming){
                  aCluster->fGoodDoubleLuciteTiming = true;
               }

               if (fHodoscopeDetector->fTimedTDCHits[aLucHit->fPMTNumber-1]->size() > 0){
                  //aCluster->fGoodLuciteTiming = true;
                  if( TMath::Abs(aLucHit->fTDCAlign) < TMath::Abs(aCluster->fLuciteTDC) ) {
                     aCluster->fLuciteTDC = aLucHit->fTDCAlign;
                  }
               }

            }
         }
      } // end loop over lucite hits

      for (int kk = 0; kk < aBetaEvent->fLuciteHodoscopeEvent->fLucitePositionHits->GetEntries(); kk++) {

         auto * aLucPosHit = (LuciteHodoscopePositionHit*)(*(aBetaEvent->fLuciteHodoscopeEvent->fLucitePositionHits))[kk] ;

         // If the lucite hit (approximately close to the cluster)
         if ( fHodoscopeDetector->fGeoCalc->GetLuciteRow(aCluster->fjPeak) - 2 <= aLucPosHit->fBarNumber &&
              fHodoscopeDetector->fGeoCalc->GetLuciteRow(aCluster->fjPeak) + 2 >= aLucPosHit->fBarNumber)
         {

            int bar_index = aLucPosHit->fBarNumber - 1;
            if (fHodoscopeDetector->fTDCHitDifferences[bar_index]->size() > 0)
            {

               // Ad hoc offset of 100 for the difference spread
               if( TMath::Abs(aLucPosHit->fTDCDifference-100) < TMath::Abs(aCluster->fLuciteTDCDiff) )
               {
                  aCluster->fLuciteTDCDiff = aLucPosHit->fTDCDifference;
               }

               if( TMath::Abs(aLucPosHit->fTDCSum) < TMath::Abs(aCluster->fLuciteTDCSum) )
               {
                  aCluster->fLuciteTDCSum = aLucPosHit->fTDCSum;
               }
            }
         }
      } // end loop over lucite hits

   }// end loop over clusters

   // Tracker
   //    for (int kk=0; kk< aBetaEvent->fForwardTrackerEvent->fNumberOfHits;kk++)
   //    {
   //       aTrackerHit = (ForwardTrackerHit*)(*trackerHits)[kk] ;
   //       aTrackerHit->fPassesTiming[0] =
   //             TMath::Abs((Double_t)aTrackerHit->fTDC - tracker->trackerTimings[ tracker->fGeoCalc->GetScintNumber(aTrackerHit->fScintLayer,aTrackerHit->fRow)]->fTDCPeak) <
   //         (1.5)*5.0*((Double_t)tracker->trackerTimings[tracker->fGeoCalc->GetScintNumber(aTrackerHit->fScintLayer,aTrackerHit->fRow)]->fTDCPeakWidth);
   //
   //        for(int k0=1;k0<10;k0++)
   //       aTrackerHit->fPassesTiming[k0] =
   //             TMath::Abs((Double_t)aTrackerHit->fTDC - tracker->trackerTimings[ tracker->fGeoCalc->GetScintNumber(aTrackerHit->fScintLayer,aTrackerHit->fRow) ]->fTDCPeak) <
   //         ((Double_t)k0)*5.0*((Double_t)tracker->trackerTimings[ tracker->fGeoCalc->GetScintNumber(aTrackerHit->fScintLayer,aTrackerHit->fRow) ]->fTDCPeakWidth);
   //
   // /// Currently using largest window in tdc  cut
   //       if(aTrackerHit->fPassesTiming[9])   aBetaEvent->fForwardTrackerEvent->fNumberOfTimedTDCHits++;

   //aTrackerHit->fADC=aTrackerHit->fADC-tracker->trackerPedestals[f]
   //      }
   //     cout << "tracker layer" << aTrackerHit->fScintLayer << " " << aTrackerHit->fTDC << "\n";
   //     cout << "tracker row  " << aTrackerHit->fRow << " " << aTrackerHit->fPosition << "\n";
   //     } // end loop over tracker hits
   //      ((TH1F*)(*fLucitePedHists)[aLucHit->fRow-1+28])->Fill(aLucHit->fPositiveADC);

return(0);
}

//___________________________________________________________________________________


//   ClassImp(BigcalPi0Calculations)
//Int_t BigcalPi0Calculations::Calculate()
//{
//
//   fPi0Event->ClearEvent();
//   fFillEvent = false;
//
//   if (!(fEvents->TRIG->IsNeutralPionEvent())) return(0);
//
//   // CALORIMETER CLUSTERING
//   /*#ifdef INSANE_VERBOSE_EVENT*/
//   /*  if(SANERunManager::GetRunManager()->GetVerbosity()>3)  printf("neutral pion event  in BigcalPi0Calaculations::Calculate() \n");*/
//   // #endif
//
//   fPi0Event->fEventNumber = fClusterEvent->fEventNumber;
//   fPi0Event->fRunNumber = fClusterEvent->fRunNumber;
//   //
//   // Begin loop over clusters
//   InSANEReconstruction * aReconPi0;
//
//   BIGCALCluster * aCluster;
//   BIGCALCluster * bCluster;
//   Pi0ClusterPair * aClustPair ;
//
//   TVector3 origin(0.0, 0.0, 0.0);
//   TVector3 v1(1, 2, 3);
//   TVector3 v2(1, 2, 3);
//   TVector3 k1(1, 2, 3);
//   TVector3 k2(1, 2, 3);
//   fPi0Event->fNTracks = 0;
//   Double_t mass, distance;
//   Double_t E1, E2;
//
//
//   for (int kk = 0; kk < fClusterEvent->fNClusters  ; kk++) {
//      aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
//      if (aCluster->fTotalE < 6000.0) {
//
//         v1.SetXYZ(aCluster->GetXmoment(),   aCluster->GetYmoment(),
//               BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
//         E1 = aCluster->fTotalE;
//         k1 = v1 - origin;
//         k1.SetMag(E1);
//         k1.Transform(fRotFromBCcoords);
//
//         /// Loop over all clusters with index j>k as to not double count
//         for (int j = kk + 1; j < fClusterEvent->fNClusters ; j++) {
//            bCluster = (BIGCALCluster*)(*fClusters)[j] ;
//            if (bCluster->fTotalE < 6000.0) {
//
//
//               v2.SetXYZ(bCluster->GetXmoment(),   bCluster->GetYmoment(),
//                     BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
//               E2 = bCluster->fTotalE;
//               k2 = v2 - origin;
//               k2.SetMag(E2);
//               k2.Transform(fRotFromBCcoords);
//
//
//               if (aCluster->fGoodCherenkovTDCHit == true) fPi0Event->fCherenkovHit = true;
//               if (bCluster->fGoodCherenkovTDCHit == true) fPi0Event->fCherenkovHit = true;
//               //      if(aCluster->fGoodCherenkovTDCHit==false && bCluster->fGoodCherenkovTDCHit==false) {
//               mass = fPi0Event->CalculateMass(E1, E2, k1.Angle(k2));
//               distance = TMath::Sqrt(TMath::Power(bCluster->GetXmoment() - aCluster->GetXmoment(), 2)
//                     + TMath::Power(bCluster->GetYmoment() - aCluster->GetYmoment(), 2));
//
//               /// \todo better cuts
//               if (mass > 20.0  && mass < 1400.0 /*&& (E1/E2) < 10.0 && (E1/E2)>0.01 && distance > 4.0*/ /*cm*/) {
//                  fFillEvent = true;
//                  aClustPair = new((*fPi0Event->fClusterPairs)[fPi0Event->fNumberOfPairs]) Pi0ClusterPair();
//                  fPi0Event->fNumberOfPairs++;
//                  aClustPair->fCluster1 = (aCluster);
//                  aClustPair->fCluster2 = (bCluster);
//                  /*      std::cout << "E(cluster=1)=" << E1 << ", E(cluster=2)=" << E2 << "\n";*/
//                  aReconPi0 = new((*fPi0Event->fReconstruction)[fPi0Event->fNTracks]) InSANEReconstruction();
//                  aReconPi0->fNumber = fPi0Event->fNTracks;
//                  fPi0Event->fNTracks++;
//                  aReconPi0->fMass = mass;
//                  aReconPi0->fAngle = k1.Angle(k2);
//                  aReconPi0->fPi0Momentum =  k1 + k2;
//                  aReconPi0->fE1 = E1;
//                  aReconPi0->fE2 = E2;
//                  aReconPi0->fClusterDistance = distance;
//                  aReconPi0->fMomentum = aReconPi0->fPi0Momentum.Mag();
//                  aReconPi0->fEnergy = TMath::Sqrt(138.0 * 138.0 + aReconPi0->fPi0Momentum.Mag2());
//                  aReconPi0->fPhi = aReconPi0->fPi0Momentum.Phi();
//                  aReconPi0->fTheta = aReconPi0->fPi0Momentum.Theta();
//               }
//               //      }
//            }
//         }
//      }
//   }
//   return(1);
//}
//
////___________________________________________________________________________________
//
//
//ClassImp(Bigcal3ClusterPi0Calculation)
//
//Int_t Bigcal3ClusterPi0Calculation::Calculate()
//{
//
//   fPi0Event->ClearEvent();
//   fFillEvent = false;
//   if (!(fEvents->TRIG->IsPi0Event())) return(0);
//
//   fPi0Event->fEventNumber = fClusterEvent->fEventNumber;
//   fPi0Event->fRunNumber = fClusterEvent->fRunNumber;
//   fPi0Event->fNumberOfGroups = 0;
//   // Begin loop over clusters
//   InSANEReconstruction * aReconPi0;
//
//   /// A Pi0ClusterPair defines the virtual photon! \f$ \gamma^* \rightarrow e^+ e^- \f$
//   Pi0ClusterPair * aClustPair ;
//
//   BIGCALCluster * aCluster;
//   BIGCALCluster * bCluster;
//   BIGCALCluster * cCluster;
//   BIGCALCluster * dCluster;
//
//   TVector3 v1(1, 2, 3);
//   TVector3 v2(1, 2, 3);
//   TVector3 v3(1, 2, 3);
//   TVector3 k1(0, 0, 0);
//   TVector3 k2(0, 0, 0);
//   TVector3 k3(0, 0, 0);
//   TVector3 Ksum(0, 0, 0);
//   TVector3 origin(0, 0, 0);
//
//   //   fPi0Event->fNTracks=0;
//
//   Double_t mass, distance;
//   Double_t E1, E2, E3;
//   Double_t thetaGammaGamma = 0;
//   Double_t thetapm = 0;
//   Double_t nu1 = 0;
//
//   /// - first loop over Clusters
//   if (fClusterEvent->fClusters->GetEntries() > 2)
//      for (int kk = 0; kk < fClusterEvent->fClusters->GetEntries()  ; kk++) {
//
//         /// Get and define cluster quantities
//         /// this cluster is assumed to be an electron
//         aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
//         if (aCluster->fTotalE < 6000.0) {
//
//            /// Get the result from the electron neural network
//            //fANNEvent->SetEventValues(aCluster);
//            //fANNEvent->GetCorrectionMLPInputNeuronArray(electronInputs);
//            //aCluster->fDeltaE     = fElectronNNEnergy.Value(0, electronInputs);
//            //aCluster->fDeltaTheta = fElectronNNTheta.Value(0, electronInputs);
//            //aCluster->fDeltaPhi   = fElectronNNPhi.Value(0, electronInputs);
//            //       aCluster->fDeltaE     = 0;
//            //       aCluster->fDeltaTheta = 0;
//            //       aCluster->fDeltaPhi   = 0;
//
//            /// define the momentum
//            E1 = aCluster->fTotalE + aCluster->fDeltaE ;
//            v1.SetMagThetaPhi(E1, aCluster->GetTheta() + aCluster->fDeltaTheta, aCluster->GetPhi() + aCluster->fDeltaPhi);
//            k1 = v1 - origin;
//            k1.SetMag(E1);
//            //       k1.Transform(fRotFromBCcoords); // don't use with cluster->GetTheta() ...
//
//            /// - Loop over all clusters assuming that this is the positron
//            /// - ie second loop over clusters
//            ///   Note here we are double counting on the pair because we assume charges
//            ///   and thus need to have both combinations as to not miss the correct one
//            for (int j = 0; j < fClusterEvent->fNClusters ; j++) {
//
//               /// Get and define cluster quantities
//               bCluster = (BIGCALCluster*)(*fClusters)[j] ;
//               if (bCluster != aCluster)
//                  if (bCluster->fTotalE < 6000.0) {
//
//                     /// Get the result from the electron neural network
//                     //fANNEvent->SetEventValues(bCluster);
//                     //fANNEvent->GetCorrectionMLPInputNeuronArray(positronInputs);
//                     //bCluster->fDeltaE     = fPositronNNEnergy.Value(0, positronInputs);
//                     //bCluster->fDeltaTheta = fPositronNNTheta.Value(0, positronInputs);
//                     //bCluster->fDeltaPhi   = fPositronNNPhi.Value(0, positronInputs);
//
//                     /// Set the momentum vector
//                     E2 = bCluster->fTotalE + bCluster->fDeltaE ;
//                     v2.SetMagThetaPhi(E2, bCluster->GetTheta() + bCluster->fDeltaTheta, bCluster->GetPhi() + bCluster->fDeltaPhi);
//                     k2 = v2 - origin;
//                     k2.SetMag(E2);
//                     /*         k2.Transform(fRotFromBCcoords);*/
//                     Ksum = k1 + k2;
//
//                     aClustPair = new((*fPi0Event->fClusterPairs)[fPi0Event->fNumberOfPairs]) Pi0ClusterPair();
//                     fPi0Event->fNumberOfPairs++;
//                     aClustPair->fCluster1 = (aCluster);
//                     aClustPair->fCluster2 = (bCluster);
//                     aClustPair->fAngle = k2.Angle(k1);
//                     aClustPair->fTheta = Ksum.Theta();
//                     aClustPair->fPhi   = Ksum.Phi();
//                     aClustPair->fMomentum = Ksum.Mag();
//                     aClustPair->fMass  = TMath::Sqrt(2.0 * (E1 * E2) - 2.0 * E1 * E2 * TMath::Cos(aClustPair->fAngle));
//
//                     /// - Third loop over all clusters and exclude ones already in the pair
//                     for (int ll = 0; ll < fClusterEvent->fNClusters ; ll++) {
//
//                        /// Get and define cluster quantities|
//                        /// This cluster is assumed to be the photon
//                        cCluster = (BIGCALCluster*)(*fClusters)[ll] ;
//
//                        if (cCluster->fTotalE < 6000.0)
//                           if (cCluster != aCluster && cCluster != bCluster) {
//
//                              //fANNEvent->SetEventValues(cCluster);
//                              //fANNEvent->GetCorrectionMLPInputNeuronArray(gammaInputs);
//                              //cCluster->fDeltaE     = fGammaNNEnergy.Value(0, gammaInputs);
//                              //cCluster->fDeltaTheta = fGammaNNTheta.Value(0, gammaInputs);
//                              //cCluster->fDeltaPhi   = fGammaNNPhi.Value(0, gammaInputs);
//
//                              fFillEvent = true;
//
//                              E3 = cCluster->fTotalE + cCluster->fDeltaE ;
//                              v3.SetMagThetaPhi(E3, cCluster->GetTheta() + cCluster->fDeltaTheta, cCluster->GetPhi() + cCluster->fDeltaPhi);
//
//                              //                v3.SetXYZ( cCluster->GetXmoment(),
//                              //                           cCluster->GetYmoment(),
//                              //                           BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance );
//                              k3 = v3 - origin;
//                              k3.SetMag(E3);
//                              //                k3.Transform(fRotFromBCcoords);
//
//                              dCluster = new((*fPi0Event->fClusters)[fPi0Event->fNumberOfGroups]) BIGCALCluster(*cCluster);
//
//                              /// Create a reconstruction corresponding to this pair and cluster
//                              aReconPi0 = new((*fPi0Event->fReconstruction)[fPi0Event->fNumberOfGroups]) InSANEReconstruction();
//                              aReconPi0->fNumber = fPi0Event->fNumberOfPairs - 1;
//                              fPi0Event->fNumberOfGroups++;
//                              aReconPi0->fNu = E3;
//
//                              /// Calculate the mass!
//                              //Double_t nu1, Double_t E1, Double_t E2, Double_t thetapm, Double_t thetaGammaGamma
//                              aReconPi0->fAngle = k3.Angle(Ksum);
//                              aReconPi0->fMass = fPi0Event->CalculateMass(E3, E1, E2, aClustPair->fAngle, aReconPi0->fAngle);
//                              aReconPi0->fPi0Momentum = k1 + k2 + k3;
//                              aReconPi0->fE1 = E1;
//                              aReconPi0->fE2 = E2;
//                              aReconPi0->fMomentum = aReconPi0->fPi0Momentum.Mag();
//                              aReconPi0->fEnergy = TMath::Sqrt(138.0 * 138.0 + aReconPi0->fPi0Momentum.Mag2());
//                              aReconPi0->fPhi = aReconPi0->fPi0Momentum.Phi();
//                              aReconPi0->fTheta = aReconPi0->fPi0Momentum.Theta();
//                           }
//                     } // c loop
//                  }
//            } //b loop
//         } // 6GeV
//      } // a loop
//   return(1);
//}
//
////___________________________________________________________________________________
//
//
//   ClassImp(BigcalOverlapCalculation)
//Int_t BigcalOverlapCalculation::Calculate()
//{
//
//   //      fPi0Event->Clear();
//   fFillEvent = false;
//   if (!(fEvents->TRIG->IsClusterable())) return(0);
//
//   // CALORIMETER CLUSTERING
//   /*#ifdef INSANE_VERBOSE_EVENT*/
//   /*  if(SANERunManager::GetRunManager()->GetVerbosity()>3)  printf("neutral pion event  in BigcalPi0Calaculations::Calculate() \n");*/
//   // #endif
//
//   //      fPi0Event->fEventNumber = fClusterEvent->fEventNumber;
//   //      fPi0Event->fRunNumber = fClusterEvent->fRunNumber;
//   // //
//   // // Begin loop over clusters
//   //    InSANEReconstruction * aReconPi0;
//   //    BIGCALCluster * aCluster;
//   //    BIGCALCluster * bCluster;
//   //
//   //    TVector3 v1(1,2,3);
//   //    TVector3 v2(1,2,3);
//   //    fPi0Event->fNTracks=0;
//   //    Double_t mass,distance;
//   //
//   //    Double_t E1,E2;
//   //    for (int kk=0; kk<fClusterEvent->fNClusters  ;kk++)
//   //    {
//   //       aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
//   //       v1.SetXYZ( aCluster->GetXmoment(),   aCluster->GetYmoment(),
//   //                  BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
//   //       E1 = aCluster->fTotalE;
//   //   // Loop over all clusters with index j>k as to not double count
//   //      for (int j = kk+1; j < fClusterEvent->fNClusters ;j++) {
//   //         bCluster = (BIGCALCluster*)(*fClusters)[j] ;
//   //         v2.SetXYZ( bCluster->GetXmoment(),   bCluster->GetYmoment(),
//   //                    BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
//   //         E2 = bCluster->fTotalE;
//   //         if(aCluster->fGoodCherenkovTDCHit==true) fPi0Event->fCherenkovHit = true;
//   //         if(bCluster->fGoodCherenkovTDCHit==true) fPi0Event->fCherenkovHit = true;
//   // //      if(aCluster->fGoodCherenkovTDCHit==false && bCluster->fGoodCherenkovTDCHit==false) {
//   //         mass = fPi0Event->CalculateMass(E1,E2,v1.Angle(v2));
//   //         distance = TMath::Sqrt( TMath::Power(bCluster->GetXmoment() -aCluster->GetXmoment(),2)
//   //                  + TMath::Power(bCluster->GetYmoment() - aCluster->GetYmoment(),2) );
//   //
//   //         /// \todo better cuts
//   //         if(mass > 20.0  && mass < 800.0 && (E1/E2) < 10.0 && (E1/E2)>0.01 && distance > 4.0 /*cm*/ ) {
//   //           fPi0Event->fClusterPair->fCluster1 = (aCluster);
//   //           fPi0Event->fClusterPair->fCluster2 = (bCluster);
//   //       std::cout << "E(cluster=1)=" << E1 << ", E(cluster=2)=" << E2 << "\n";
//   //           aReconPi0 = new( (*fPi0Event->fReconstruction)[fPi0Event->fNTracks]) InSANEReconstruction();
//   //           aReconPi0->fNumber = fPi0Event->fNTracks;
//   //           fPi0Event->fNTracks++;
//   //           aReconPi0->fMass = mass;
//   //           aReconPi0->fAngle = v1.Angle(v2);
//   //           aReconPi0->fPi0Momentum = v1+v2;
//   //           aReconPi0->fE1 = E1;
//   //           aReconPi0->fE2 = E2;
//   //           aReconPi0->fClusterDistance = distance;
//   //           aReconPi0->fEnergy = TMath::Abs(aCluster->fClusterTime-bCluster->fClusterTime);
//   //           aReconPi0->fTime = aCluster->fClusterTime - bCluster->fClusterTime;
//   //         }
//   // //      }
//   //     }
//   //   }
//   return(1);
//}
// //_______________________________________________________//
//
// ClassImp(GasCherenkovCalculation3)
// //_______________________________________________________//
//
// ClassImp(GasCherenkovCalculation4)


