#include "BETACalculation.h"
#include "InSANECalculation.h"

ClassImp(BETACalculation)

//______________________________________________________________________________
BETACalculation::BETACalculation(InSANEAnalysis * analysis) : InSANECalculation(analysis) {
   fCerHits           = nullptr;
   fLucHits           = nullptr;
   fBigcalHits        = nullptr;
   fTrackerHits       = nullptr;
   fClusters          = nullptr;

   fEvents            = nullptr;
   aBetaEvent         = nullptr;
   bcEvent            = nullptr;
   aBigcalHit         = nullptr;
   lhEvent            = nullptr;
   aLucHit            = nullptr;
   gcEvent            = nullptr;
   aCerHit            = nullptr;
   ftEvent            = nullptr;
   aTrackerHit        = nullptr;
   fClusterEvent      = nullptr;
   aTrigEvent         = nullptr;

   fTrackerDetector   = nullptr;
   fCherenkovDetector = nullptr;
   fHodoscopeDetector = nullptr;
   fBigcalDetector    = nullptr;

   fCopiedArray       = nullptr;
   // Sets the SANEEvents which were created in the InSANEDetectorAnalysis class
   if(analysis) this->SetEvents(analysis->fEvents);
}
//______________________________________________________________________________
BETACalculation::~BETACalculation() {
   if (fCopiedArray) delete fCopiedArray;
}
//______________________________________________________________________________
Int_t BETACalculation::SetDetectors(InSANEDetectorAnalysis * anAnalysis) {
   fTrackerDetector   = anAnalysis->fTrackerDetector;
   fCherenkovDetector = anAnalysis->fCherenkovDetector;
   fHodoscopeDetector = anAnalysis->fHodoscopeDetector;
   fBigcalDetector    = anAnalysis->fBigcalDetector;
   return(0);
}
//______________________________________________________________________________
Int_t BETACalculation::SetClusterEvent(InSANEClusterEvent * ev) {
   //if(SANERunManager::GetRunManager()->fVerbosity>2)
   //   std::cout << " o BETACalculation: Setting cluster event to addr: " << ev << "\n";
   if (ev) {
      fClusterEvent = ev;
      fClusters = ev->fClusters;
   } else {
      std::cout << "x- Null Cluster event provided in BETACalculation::SetClusterEvent()\n";
   }
   return(0);
}
//______________________________________________________________________________
Int_t BETACalculation::Initialize() {
   if(SANERunManager::GetRunManager()->fVerbosity>2)
      std::cout << " o BETACalculation::Initialize()\n";
   if(fTransientDatum){
      auto* events = (SANEEvents*)fTransientDatum;
      fEvents = events;
      aBetaEvent = events->BETA;
      bcEvent = events->BETA->fBigcalEvent;
      lhEvent = events->BETA->fLuciteHodoscopeEvent;
      gcEvent = events->BETA->fGasCherenkovEvent;
      ftEvent = events->BETA->fForwardTrackerEvent;
      aTrigEvent = events->TRIG;
      fCerHits = events->BETA->fGasCherenkovEvent->fGasCherenkovHits;
      fLucHits = events->BETA->fLuciteHodoscopeEvent->fLuciteHits;
      fBigcalHits = events->BETA->fBigcalEvent->fBigcalHits;
      fTrackerHits = events->BETA->fForwardTrackerEvent->fTrackerHits;
   } else {
      Error("Inititalize","fTransientDatum not set");
   }
   return(0);
}
//______________________________________________________________________________





ClassImp(BETACalculationWithClusters)

//______________________________________________________________________________
BETACalculationWithClusters::BETACalculationWithClusters(InSANEAnalysis * analysis)
   : BETACalculation(analysis) {
   fClusterProcessor = nullptr;
}
//______________________________________________________________________________
BETACalculationWithClusters::~BETACalculationWithClusters() {
}
//______________________________________________________________________________
Int_t BETACalculationWithClusters::Initialize() {
   if(SANERunManager::GetRunManager()->fVerbosity>2)
      std::cout << " o BETACalculationWithClusters::Initialize()\n";
   BETACalculation::Initialize();
   SetClusterEvent(fEvents->CLUSTER);
   return(0);
}
//______________________________________________________________________________
void BETACalculationWithClusters::SetEvents(InSANEAnalysisEvents * e) {
   if (e) {
      BETACalculation::SetEvents(e);
      SetClusterEvent(fEvents->CLUSTER);
   } else {
      std::cout << "x- Null Cluster event provided in BETACalculationWithClusters::SetClusterEvent()\n";
   }
}
//______________________________________________________________________________
void BETACalculationWithClusters::SetClusterProcessor(BIGCALClusterProcessor * clusterProc) {
   if (clusterProc) {
      fClusterProcessor = clusterProc;
   } else {
      std::cout << " + Creating new cluster processor from BigcalClusteringCalculation1 \n";
      fClusterProcessor = new BIGCALClusterProcessor();
   }
   if (IsEventsSet()) {
      fClusterProcessor->SetEventAddress((BigcalEvent*)((SANEEvents*)GetEvents())->BETA->fBigcalEvent);
   } else {
      std::cout << "x- WARNING: BigcalClusteringCalculation1::SetClusterProcessor called without BigcalClusteringCalculation1::SetEvent first\n";
   }
}
//______________________________________________________________________________

