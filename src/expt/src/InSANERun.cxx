#include "InSANERun.h"
#include "TSQLResult.h"
#include "TSQLRow.h"
#include "TString.h"
#include "TDatime.h"

ClassImp(InSANERun)

const char * InSANERun::kgSANETargetNames[] =
{"NH3", "Carbon", "CrossHair", "C+He",  "Hole", "CrossHair+He", "Unknown"};
const  InSANERun::SANETargets   InSANERun::kgSANETargets[] =
{kNH3, kCARBON, kCROSSHAIR, kCARBON_PLUS_HE, kHOLE, kCROSSHAIR_PLUS_HE , kUNKNOWN };

const char * InSANERun::kgSANETargetCupNames[] = {"Top", "Bottom", "None", "N/A"};
const InSANERun::SANETargetCup InSANERun::kgSANETargetCups[] = {kTOP, kBOTTOM, kNONE , kNA};

const char * InSANERun::kgSANETargetSignNames[] = { "Positive", "Negative", "Unpolarized"};
const InSANERun::SANETargetSign InSANERun::kgSANETargetSign[] = { kPOSITIVE, kNEGATIVE , kUNPOLARIZED};

const char * InSANERun::kgSANETargetOrientationNames[] = { "Antiparallel", "Perpendicular", "Nofield"};
const InSANERun::SANETargetOrientation InSANERun::kgSANETargetOrientations[] = { kANTIPARALLEL, kPERPENDICULAR, kNOFIELD};

//______________________________________________________
InSANERun::InSANERun(Int_t runnum) {
   this->SetName(Form("run%d", runnum));
   //this->SetTitle(Form("data/rootfiles/InSANE%d.root",runnum));
   this->SetTitle(Form("SANE RUN %d", runnum));
   fRunNumber             = runnum;
   fIsFlagged = false;
   fRunFlags.Clear();
   fResults.Clear();
   fCherenkovTimeWalkFits.Clear();
   fBigcalTimeWalkFits.Clear();

   fRunFlags.SetOwner(true);
   fResults.SetOwner(true);
   fCherenkovTimeWalkFits.SetOwner(true);
   fBigcalTimeWalkFits.SetOwner(true);
   fDilution.SetOwner(true);
   fApparatus.SetOwner(true); 
   fDetectors.SetOwner(true); 

   hAsymmetries = nullptr;

   // Analysis
   fNumberOfScalerReads = 0;
   fPedestalsAnalyzed   = 0;
   fAnalysisPass        = -9;
   fTimingAnalyzed      = 0;

   fTotalAsymmetry      = 0;
   fTotalNPlus          = 0;
   fTotalNMinus         = 0;
   fTotalQPlus          = 0;
   fTotalQMinus         = 0;
   fDeltaNPlus          = 0;
   fDeltaNMinus         = 0;
   fTotalQ              = 0;
   /// BETA

   /// Spectrometer
   fHMSSign     = 1.0;
   fHMSAngle    = 0.0;
   fHMSMomentum = 0.0;

   /// Beam
   fBeamEnergy = 0;
   fBeamPolarization = 0;
   fAverageBeamCurrent = 0;
   fHalfWavePlate = 0;
   fBeamPassNumber = 0;
   fAverageBeamPolarization = 0;
   fWienAngle = 0.0;
   fBeamTrips = 0;
   fQuantumEfficiency = 0.0;

   /// DAQ
   fDeadTime = 0;
   fLiveTime = 0;
   fDeadTimePlus = 0;
   fDeadTimeMinus = 0;
   fLiveTimePlus = 0;
   fLiveTimeMinus = 0;
   for (int i = 0; i < 15; i++) fPreScale[i] = 1;

   /// Apparatus
   fIronPlate = 0;
   fBigcalCalibrationSeries = 0;

   /// Target
   fAverageTargetPolarization = 0;
   fTargetPolarizationSign = kUNPOLARIZED;
   fEndingPolarization = 0;
   fStartingPolarization = 0;
   fTargetAngle = 0;
   fTargetField = 0;
   fTargetOnlinePolarization = 0;
   fTargetOfflinePolarization = 0;
   fTarget = kUNKNOWN;
   fTargetCup = kNONE;
   fPackingFraction = 0.6;
   SANETargets tgtByNumber[] = { kNH3, kCARBON, kCROSSHAIR, kCARBON_PLUS_HE, kHOLE, kCROSSHAIR_PLUS_HE , kUNKNOWN };
   fTargetByNumber = &tgtByNumber[0];

//   fBeam = new HallCPolarizedElectronBeam();

//   fAsymmetries = new TClonesArray("InSANEAsymmetry",1);
//   fAsymmetries->Clear();
   // fAsymmetries = new TList();//TClonesArray("InSANEAsymmetry",1);
   //fAsymmetries->Clear();
   //fDilution.SetOwner(kTRUE);
   //fAsymmetries.SetOwner(kTRUE);

   ClearCounters();
}
//______________________________________________________________________________
InSANERun& InSANERun::operator=(const InSANERun& rhs) {
   if (this != &rhs) {  // make sure not same object
      TNamed::operator=(rhs);
      fRunNumber = rhs.fRunNumber;
      fRunNumber             = rhs.fRunNumber;
      fIsFlagged =  rhs.fIsFlagged;
      for (int i = 0; i < fRunFlags.GetEntries(); i++) fRunFlags.Add(rhs.fRunFlags.At(i));
      for (int i = 0; i < fResults.GetEntries(); i++) fResults.Add(rhs.fResults.At(i));
      /*  fResults=rhs.fResults;*/
      fCherenkovTimeWalkFits = rhs.fCherenkovTimeWalkFits;
      fBigcalTimeWalkFits    = rhs.fBigcalTimeWalkFits;

      // Analysis
      fNumberOfScalerReads = rhs.fNumberOfScalerReads;
      fPedestalsAnalyzed   = rhs.fPedestalsAnalyzed;
      fAnalysisPass        = rhs.fAnalysisPass;
      fTimingAnalyzed      = rhs.fTimingAnalyzed;

      fTotalAsymmetry      = rhs.fTotalAsymmetry;
      fTotalNPlus          = rhs.fTotalNPlus;
      fTotalNMinus         = rhs.fTotalNMinus;
      fTotalQPlus          = rhs.fTotalQPlus;
      fTotalQMinus         = rhs.fTotalQMinus;
      fDeltaNPlus          = rhs.fDeltaNPlus;
      fDeltaNMinus         = rhs.fDeltaNMinus;

      // Beam
      fBeamEnergy              = rhs.fBeamEnergy;
      fBeamPolarization        = rhs.fBeamPolarization;
      fAverageBeamCurrent      = rhs.fAverageBeamCurrent;
      fHalfWavePlate           = rhs.fHalfWavePlate;
      fBeamPassNumber          = rhs.fBeamPassNumber;
      fAverageBeamPolarization = rhs.fAverageBeamPolarization;
      fWienAngle               = rhs.fWienAngle;
      fBeamTrips               = rhs.fBeamTrips;
      fQuantumEfficiency       = rhs.fQuantumEfficiency;

      // DAQ
      fDeadTime = rhs.fDeadTime;
      for (int i = 0; i < 15; i++) fPreScale[i] = rhs.fPreScale[i];

      // Apparatus
      fIronPlate = rhs.fIronPlate;

      fDilution.AddAll( &rhs.fDilution);
      fResults.AddAll( &rhs.fResults);
      fDetectors.AddAll( &rhs.fDetectors);
      fApparatus.AddAll( &rhs.fApparatus);

      // Target
      fAverageTargetPolarization = rhs.fAverageTargetPolarization;
      fTargetPolarizationSign    = rhs.fTargetPolarizationSign;
      fEndingPolarization        = rhs.fEndingPolarization;
      fStartingPolarization      = rhs.fStartingPolarization;
      fTargetAngle               = rhs.fTargetAngle;
      fTargetField               = rhs.fTargetField;
      fTargetOnlinePolarization  = rhs.fTargetOnlinePolarization;
      fTargetOfflinePolarization = rhs.fTargetOfflinePolarization;
      fTarget                    = rhs.fTarget;
      fTargetCup                 = rhs.fTargetCup;
      fTargetByNumber            = rhs.fTargetByNumber;

      
      //fAsymmetries = rhs.fAsymmetries;
      //for(int i =0;i< *fAsymmetries.GetEntries();i++) *fAsymmetries.Add(rhs.(*fAsymmetries).At(i));

      fNEvents      = rhs.fNEvents;
      fNCoinEvents  = rhs.fNCoinEvents;
      fNBeta2Events = rhs.fNBeta2Events;
      fNBeta1Events = rhs.fNBeta1Events;
      fNPi0Events   = rhs.fNPi0Events;
      fNHmsEvents   = rhs.fNHmsEvents;
      fNLedEvents   = rhs.fNLedEvents;
      fNPedEvents   = rhs.fNPedEvents;

      for (int i = 0; i < 12; i++) fTriggers[i] = rhs.fTriggers[i];
   }
   return *this;  // Return ref for multiple assignment
}
//______________________________________________________________________________
void InSANERun::ClearCounters() {

   fNEvents      = 0;
   fNCoinEvents  = 0;
   fNBeta2Events = 0;
   fNBeta1Events = 0;
   fNPi0Events   = 0;
   fNHmsEvents   = 0;
   fNLedEvents   = 0;
   fNPedEvents   = 0;

   for (int i = 0; i < 12; i++) fTriggers[i] = 0;

}
//______________________________________________________________________________
InSANERun::~InSANERun() {
   printf(" - InSANERun deleted, %s", this->GetName());
}
//______________________________________________________________________________
Bool_t InSANERun::IsValid() {
   Bool_t result;
   result = true;
   /// Tests for nans
   double temp;

   if (fTotalAsymmetry != fTotalAsymmetry) result = false;
   if (fTotalNPlus != fTotalNPlus) result = false;
   if (fTotalNMinus != fTotalNMinus) result = false;

   temp = 1.0 / fTargetOnlinePolarization;
   if (temp !=  temp) result = false;
   temp = 1.0 / fTargetOfflinePolarization;
   if (temp !=  temp) result = false;
   temp = 1.0 / fAverageBeamPolarization;
   if (temp !=  temp) result = false;
   temp = 1.0 / fAverageTargetPolarization;
   if (temp !=  temp) result = false;
   temp = 1.0 / fEndingPolarization;
   if (temp !=  temp) result = false;
   if (temp !=  temp) result = false;
   temp = 1.0 / fStartingPolarization;
   if (temp !=  temp) result = false;
   return(result);
}
//______________________________________________________________________________
void InSANERun::Print(Option_t *) const {
   using namespace std;
   /*    InSANEDatabaseManager::GetManager()->PrintCurrentRunDBEntry();*/
   fDuration = fEndDatetime.Convert() - fStartDatetime.Convert();
   fTimeDuration.Set(fEndDatetime.Convert() - fStartDatetime.Convert());

   cout << setfill('_') << setw(60)  << " " << endl;
   cout << "    Run "   << fRunNumber <<  "      current pass: " << fAnalysisPass << endl;
   cout << setfill('^') << setw(60)   << " " << endl;
   cout << setfill(' ');
   /*   cout << setprecision(3);*/
   std::cout << "    Duration " << fDuration << " seconds " << std::endl;
   std::cout << "Results:  " << std::endl;
   std::cout << "      Total Asym......... " << setw(9) << right << fTotalAsymmetry << " " << std::endl;
   std::cout << "      Total N+ .......... " << setw(9) << right << fTotalNPlus << " " << std::endl;
   std::cout << "      Total N- .......... " << setw(9) << right << fTotalNMinus << " " << std::endl;
   std::cout << "      Total Q+ .......... " << setw(9) << right << fTotalQPlus << " " << std::endl;
   std::cout << "      Total Q- .......... " << setw(9) << right << fTotalQMinus << " " << std::endl;
   std::cout << "Beam:  " << std::endl;
   std::cout << "      Energy ............ " << setw(9) << right << fBeamEnergy << " MeV" << std::endl;
   std::cout << "      Current ........... " << setw(9) << right << fAverageBeamCurrent << " nA " << std::endl;
   std::cout << "      Beam trips ........ " << setw(9) << right << fBeamTrips << "  " << std::endl;
   std::cout << "      Polarization ...... " << setw(9) << right << fBeamPolarization << " %" << std::endl;
   std::cout << "      Half Wave Plate ... " << setw(9) << right << fHalfWavePlate << "  " << std::endl;
   std::cout << "      Number Of Passes .. " << setw(9) << right << fBeamPassNumber << "  " << std::endl;
   std::cout << "      Pol.Source QE ..... " << setw(9) << right << fQuantumEfficiency << " %" << std::endl;
   std::cout << "      Wien Angle ........ " << setw(9) << right << fWienAngle << "  " << std::endl;
   std::cout << "Target: " << std::endl;
   std::cout << "      Target material ... " << kgSANETargetNames[fTarget] << "  " << std::endl;
   std::cout << "      Angle ............. " << setw(9) << right << fTargetAngle << " degrees" << std::endl;
   std::cout << "      Offline  Pol ...... " << setw(9) << right << fTargetOfflinePolarization << " %" << std::endl;
   std::cout << "      Online   Pol ...... " << setw(9) << right << fTargetOnlinePolarization << " %" << std::endl;
   std::cout << "      Starting Pol ...... " << setw(9) << right << fStartingPolarization << " %" << std::endl;
   std::cout << "      Ending   Pol ...... " << setw(9) << right << fEndingPolarization << " %" << std::endl;
   std::cout << "      Cup ............... " << setw(9) << right << kgSANETargetCupNames[fTargetCup] << "  " << std::endl;
   std::cout << "      PF ................ " << setw(9) << right << fPackingFraction << " %" << std::endl;
   std::cout << "      Pol. Direction .... " << kgSANETargetSignNames[fTargetPolarizationSign] << "  " << std::endl;
   std::cout << "DAQ: " << std::endl;
   std::cout << "      Dead time ......... " << setw(9) << right << setprecision(5) << fDeadTime << "  " << std::endl;
   std::cout << "      ... Hel dep (+,-) . " << setw(9) << right << setprecision(5) << fDeadTimePlus << ", " << setw(9) << right << fDeadTimePlus << "  " << std::endl;
   std::cout << "      Live time ......... " << setw(9) << right << setprecision(5) << fLiveTime << "  " << std::endl;
   std::cout << "      ... Hel dep (+,-) . " << setw(9) << right << setprecision(5) << fLiveTimePlus << ", " << setw(9) << right << fLiveTimePlus << "  " << std::endl;
   std::cout << "      Prescales ......... " ;
   for (int i = 0; i < 9; i++) std::cout << " PS" << i + 1 << "=" << fPreScale[i];
   std::cout << " \n" ;
   std::cout << "Detectors: " << std::endl;
   std::cout << "      BigCal calib ...... " << setw(9) << right << fBigcalCalibrationSeries  << std::endl;
   std::cout << "Number of Events: " << std::endl;
   std::cout << "      All ............... " << setw(9) << right << fNEvents << " " << std::endl;
   std::cout << "      BETA1 ............. " << setw(9) << right << fNBeta1Events << " " << std::endl;
   std::cout << "      BETA2 ............. " << setw(9) << right << fNBeta2Events << " " << std::endl;
   std::cout << "      Pi0 ............... " << setw(9) << right << fNPi0Events << " " << std::endl;
   std::cout << "      HMS ............... " << setw(9) << right << fNHmsEvents << " " << std::endl;
}
//______________________________________________________________________________
void  InSANERun::Clear(Option_t * ) {
//    if(fRunReport) delete fRunReport;
//
// // create a new run report
// /// \todo put this in GenerateRunReport
//
// // get the run information from a database
//    fRunReport->RetrieveRunInfo();
//
// //    fRunReport->Print();
//
// // set the values for this run from the run report
//    // Beam
//    this->fBeamPolarization = atof(fRunReport->GetValueOf("beam_pol"));
//    this->fBeamEnergy = atof(fRunReport->GetValueOf("beam_energy"));
//    this->fAverageBeamCurrent = atof(fRunReport->GetValueOf("beam_current"));
//    this->fHalfWavePlate = atoi(fRunReport->GetValueOf("halfwave_plate"));
//
//
//    // Target
//    const char * tgttype  = fRunReport->GetValueOf("target_type");
//    this->fTarget = kUNKNOWN;
//    for(int k =0;k<6;k++) if( !strcmp(tgttype,kgSANETargets[k]) ) this->fTarget = fTargetByNumber[k];
//    this->fTargetAngle = atof(fRunReport->GetValueOf("target_angle"));
//    this->fTargetCurrent = atof(fRunReport->GetValueOf("target_field"));
//    this->fStartingPolarization = atof(fRunReport->GetValueOf("target_start_pol"));
//    this->fEndingPolarization = atof(fRunReport->GetValueOf("target_stop_pol"));
//    this->fTargetOnlinePolarization = atof(fRunReport->GetValueOf("target_online_pol"));
//    this->fTargetOfflinePolarization = atof(fRunReport->GetValueOf("target_offline_pol"));
//    this->fTargetPolarizationSign = atof(fRunReport->GetValueOf("target_field"));

//   gROOT->ProcessLine(Form(".x scripts/GatherRunInfo.cxx(%d)",fRunNumber));

}
//________________________________________________________________________________





