#include "BETATimingCorrection.h"

ClassImp(BETATimingCorrection)

Int_t BETATimingCorrection::Apply()
{

   //if (!(fEvents->TRIG->IsPedestalCorrectable())) return(0);

   /// - Gas Cherenkov
   ///   - loop over cherenkov signals
   for (int kk = 0; kk < aBetaEvent->fGasCherenkovEvent->fGasCherenkovHits->GetEntries(); kk++) {
      aCerHit = (GasCherenkovHit*)(*fCerHits)[kk] ;
      if (aCerHit->fLevel == 1) {
         /// Align the Cherenkov TDC signals, fTDCAlign
         aCerHit->fTDCAlign = (Double_t)aCerHit->fTDC - fCherenkovDetector->GetTDCPeak(aCerHit->fMirrorNumber - 1);
      }

   }

   ///   - Now loop over the cherenkov TDC hits alone
//    TClonesArray * fCerTDCHits = aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits;
//    for (int kk=0; kk< fCerTDCHits->GetEntries();kk++)
//    {
//       aCerHit = (GasCherenkovHit*)(*fCerTDCHits)[kk] ;
//       /// Align the Cherenkov TDC signals, fTDCAlign
//       aCerHit->fTDCAlign = (Double_t)aCerHit->fTDC - fCherenkovDetector->GetTDCPeak(aCerHit->fMirrorNumber-1);
//    } // End loop over Cherenkov signals

   /// - Lucite Hodoscope
   /// Begin loop over Hodoscope signals
   for (int kk = 0; kk < aBetaEvent->fLuciteHodoscopeEvent->fLuciteHits->GetEntries(); kk++) {
      aLucHit = (LuciteHodoscopeHit*)(*fLucHits)[kk] ;
      if (aLucHit->fLevel == 1) {
         /// Align the Lucite TDC signals, fTDCAlign
         aLucHit->fTDCAlign = int(aLucHit->fTDC - fHodoscopeDetector->GetTDCPeak(aLucHit->fPMTNumber - 1));

      }
   } // End loop over Hodoscope signals

   /// BigCal
   /// Begin loop over Bigcal TDC signals
//     Int_t aBlockNum=0;
//     for (int kk=0; kk< aBetaEvent->fBigcalEvent->fBigcalTimingGroupHits->GetEntries();kk++)
//     {
//       aBigcalHit = (BigcalHit*)(*aBetaEvent->fBigcalEvent->fBigcalTimingGroupHits)[kk] ;
//       /// Align the Bigcal TDC signals, fTDCAlign
//       if( aBigcalHit->fLevel == 1 ) {
//           aBigcalHit->fTDCAlign = (Double_t)aBigcalHit->fTDC - fBigcalDetector->GetTDCPeak((aBigcalHit->fTDCRow-1)*4 + aBigcalHit->fTDCGroup);
// //          if(aBigcalHit->fTDCRow2>0)
// //             aBigcalHit->fTDCAlign2 = (Double_t)aBigcalHit->fTDC2
// //                      - fBigcalDetector->GetTDCPeak((aBigcalHit->fTDCRow2-1)*4 + aBigcalHit->fTDCGroup);
//       }
//       /// Bigcal trigger group
// //       if( aBigcalHit->fLevel == 3 ) {
// //            aBigcalHit->fTDCAlign = (Double_t)aBigcalHit->fTDC - fBigcalDetector->GetTDCPeak((aBigcalHit->fTDCRow-1)*2 + aBigcalHit->fTDCGroup);
// //       }
//     }

   /// - Bigcal Timing Group Hits
   for (int kk = 0; kk < aBetaEvent->fBigcalEvent->fBigcalTimingGroupHits->GetEntries(); kk++) {
      aBigcalHit = (BigcalHit*)(*aBetaEvent->fBigcalEvent->fBigcalTimingGroupHits)[kk] ;
      ///   - Align the Bigcal TDC signals, fTDCAlign
      aBigcalHit->fChannel = (aBigcalHit->fTDCRow - 1) * 4 + aBigcalHit->fTDCGroup;
      aBigcalHit->fTDCAlign = int(aBigcalHit->fTDC - fBigcalDetector->GetDetectorTiming(aBigcalHit->fChannel - 1)->fTDCPeak);
//           aBigcalHit->Dump();

   }

   /// - Bigcal Trigger Group Hits
   for (int kk = 0; kk < aBetaEvent->fBigcalEvent->fBigcalTriggerGroupHits->GetEntries(); kk++) {
      aBigcalHit = (BigcalHit*)(*aBetaEvent->fBigcalEvent->fBigcalTriggerGroupHits)[kk] ;
      ///   - Align the Bigcal TDC signals, fTDCAlign
      aBigcalHit->fChannel = (aBigcalHit->fTDCRow) + (aBigcalHit->fTriggerHalf - 1) * 19;
      aBigcalHit->fTDCAlign = int(aBigcalHit->fTDC - fBigcalDetector->GetDetectorTriggerTiming(aBigcalHit->fChannel - 1)->fTDCPeak);
//      aBigcalHit->fTDCAlign = (Double_t)aBigcalHit->fTDC - fBigcalDetector->GetTDCPeak(aBigcalHit->fChannel-1);
//           aBigcalHit->Dump();

   }

   aBetaEvent->fForwardTrackerEvent->fNumberOfHits = fTrackerHits->GetEntries();
   aBetaEvent->fForwardTrackerEvent->fNumberOfTDCHits = fTrackerHits->GetEntries();
   /// Tracker
   for (int kk = 0; kk < fTrackerHits->GetEntries(); kk++) {
      /// Align the Tracker TDC signals, fTDCAlign
      aTrackerHit = (ForwardTrackerHit*)(*fTrackerHits)[kk] ;
      aTrackerHit->fHitNumber = kk; /// for old simulation, it is now fixed in BETAGDigitizer::Readout()
      aTrackerHit->fLevel     = 1;  ///
      aTrackerHit->fTDCAlign  = int(aTrackerHit->fTDC - fTrackerDetector->GetDetectorTiming(fTrackerDetector->fGeoCalc->GetScintNumber(aTrackerHit->fScintLayer, aTrackerHit->fRow) - 1)->fTDCPeak);
   }

   return(0);
}
//_______________________________________________________//


Int_t BETATimeWalkCorrection::Apply()
{

   if (!(fEvents->TRIG->IsPedestalCorrectable())) return(0);

// Begin loop over cherenkov signals
   for (int kk = 0; kk < aBetaEvent->fGasCherenkovEvent->fGasCherenkovHits->GetEntries(); kk++) {
      aCerHit = (GasCherenkovHit*)(*fCerHits)[kk] ;
      if (aCerHit->fLevel == 1 && aCerHit->fMirrorNumber < 9)
         if (fCherenkovTimeWalk[aCerHit->fMirrorNumber-1]) { // only applies correction if function exists
/// Align the Aligned-TDC signal peaks using time walk fits
            aCerHit->fTDCAlign = (Double_t)aCerHit->fTDCAlign - fCherenkovTimeWalk[aCerHit->fMirrorNumber-1]->Eval(aCerHit->fADC) - fCherenkovTimewalkZeros[aCerHit->fMirrorNumber-1];
            aCerHit->fTDC = int(aCerHit->fTDC - fCherenkovTimeWalk[aCerHit->fMirrorNumber-1]->Eval(aCerHit->fADC) - fCherenkovTimewalkZeros[aCerHit->fMirrorNumber-1]);
         }

   } // End loop over Cherenkov signals

   for (int kk = 0; kk < aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits->GetEntries(); kk++) {
      aCerHit = (GasCherenkovHit*)(*aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits)[kk] ;
      if (aCerHit->fLevel == 1 && aCerHit->fMirrorNumber < 9)
         if (fCherenkovTimeWalk[aCerHit->fMirrorNumber-1]) { // only applies correction if function exists
/// Align the Aligned-TDC signal peaks using time walk fits
            aCerHit->fTDCAlign = (Double_t)aCerHit->fTDCAlign - fCherenkovTimeWalk[aCerHit->fMirrorNumber-1]->Eval(aCerHit->fADC) - fCherenkovTimewalkZeros[aCerHit->fMirrorNumber-1]
                                 ;
            aCerHit->fTDC = int(aCerHit->fTDC - fCherenkovTimeWalk[aCerHit->fMirrorNumber-1]->Eval(aCerHit->fADC) - fCherenkovTimewalkZeros[aCerHit->fMirrorNumber-1]);
         }

   } // End loop over Cherenkov signals

// Begin loop over Hodoscope signals
//     for (int kk=0; kk< aBetaEvent->fLuciteHodoscopeEvent->fNumberOfHits;kk++)
//     {
//       aLucHit = (LuciteHodoscopeHit*)(*fLucHits)[kk] ;
//       if(aLucHit->fLevel == 1 ) {
// /// Align the TDC  signal peaks
//          //aLucHit->fTDCAlign = (Double_t)aLucHit->fTDC - fHodoscopeDetector->GetTDCPeak(aLucHit->fPMTNumber-1);
//       }
//     } // End loop over Hodoscope signals

///\TODO
// Begin loop over Bigcal signals
   //Int_t aBlockNum = 0;
   for (int kk = 0; kk < aBetaEvent->fBigcalEvent->fBigcalHits->GetEntries(); kk++) {
      aBigcalHit = (BigcalHit*)(*fBigcalHits)[kk] ;
      /*
            if( aBigcalHit->fLevel == 1 ) {
                aBigcalHit->fTDCAlign = (Double_t)aBigcalHit->fTDC - fBigcalDetector->GetTDCPeak((aBigcalHit->fTDCRow-1)*4 + aBigcalHit->fTDCGroup);

            }

            if( aBigcalHit->fLevel == 3 ) {
                 aBigcalHit->fTDCAlign = (Double_t)aBigcalHit->fTDC - fBigcalDetector->GetTDCPeak((aBigcalHit->fTDCRow-1)*2 + aBigcalHit->fTDCGroup);
            }*/

//      ((TH1F*)(*fLucitePedHists)[aLucHit->fRow-1+28])->Fill(aLucHit->fPositiveADC);
   }

// Tracker
//     for (int kk=0; kk< aBetaEvent->fForwardTrackerEvent->fNumberOfHits;kk++)
//     {
// /*      aTrackerHit = (ForwardTrackerHit*)(*trackerHits)[kk] ;*/
// //          aLucHit->fTDCAlign = (Double_t)aLucHit->fTDC - fHodoscopeDetector->GetTDCPeak(aLucHit->-1);
//
//      }
//      ((TH1F*)(*fLucitePedHists)[aLucHit->fRow-1+28])->Fill(aLucHit->fPositiveADC);


   return(0);
}
//_______________________________________________________//

