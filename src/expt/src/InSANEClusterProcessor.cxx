#define InSANEClusterProcessor_cxx
#include "InSANEClusterProcessor.h"
#include "BIGCALCluster.h"
#include <iostream>

ClassImp(InSANEClusterProcessor)

//___________________________________________________________________________//
InSANEClusterProcessor::InSANEClusterProcessor() {

   fExamineEventByEvent = false;
   fClusters            = nullptr;
   fMaxNClusters        = 10;
   fMinimumEnergy       = 10.0;
   fNPeaks              = 0;
   fNPartitions         = 0;
   fOptions             = "";
   fSourceHistogram     = nullptr;
   fSourceCopy          = nullptr;
   fNClusters           = 0;
   fNFound              = 0;
   fEvent               = nullptr;
   fGeoCalc             = nullptr;

   fiPeaks = new std::vector<double>;
   fjPeaks = new std::vector<double>;
   fEPeaks = new std::vector<double>;

   // Computation times
   fTotalWatch      = new TStopwatch();
   fPeakWatch       = new TStopwatch();
   fQuantitiesWatch = new TStopwatch();
   fSourceHistWatch = new TStopwatch();

   fTotalWatch->Reset();
   fPeakWatch->Reset();
   fQuantitiesWatch->Reset();
   fSourceHistWatch->Reset();

   fTotalTime      = 0.0;
   fPeakTime       = 0.0;
   fQuantitiesTime = 0.0;
   fSourceHistTime = 0.0;

   InSANERunManager::GetRunManager()->GetCurrentFile()->cd("");
   if (! InSANERunManager::GetRunManager()->GetCurrentFile()->GetDirectory("clustering")) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("clustering");
   }
   InSANERunManager::GetRunManager()->GetCurrentFile()->cd("clustering");


   hTotalClusteringTime = new TH1F("hTotalClusteringTime", "Time spent in ProcessEvent()", 200, 0, 0.001);
   hPeakingFindingTime = new TH1F("hPeakingFindingTime", "Time spent in FindPeaks()", 200, 0, 0.001);
   hClusteringTime = new TH1F("hClusteringTime", "Time spent in Cluster()", 200, 0, 0.0005);
   hSourceHistTime = new TH1F("hSourceHistTime", "Time spent in GetSourceHistogram()", 200, 0, 0.001);

   InSANERunManager::GetRunManager()->GetCurrentFile()->cd("");
}
//______________________________________________________________________________
InSANEClusterProcessor::~InSANEClusterProcessor()
{
   if (fiPeaks) delete [] fiPeaks;
   if (fjPeaks) delete [] fjPeaks;
   if (fEPeaks) delete [] fEPeaks;
   delete fSourceHistWatch;
   delete fTotalWatch;
   delete fQuantitiesWatch;
   delete fPeakWatch;
   //delete s;
}
//___________________________________________________________________________

Int_t InSANEClusterProcessor::ProcessEvent(TClonesArray * clusters)
{
   Double_t tempTime;
   fClusters = clusters;

   fTotalWatch->Start(); //
   fSourceHistWatch->Start();
   if (!fSourceHistogram && !fEvent) {
      std::cout << " Error No event supplied for Cluster Searches!\n" ;
      return(1);
   }
   if (fEvent) fSourceHistogram = GetSourceHistogram();

   fSourceHistWatch->Stop();
   tempTime = fSourceHistWatch->RealTime();
   hSourceHistTime->Fill(tempTime);
   fSourceHistTime += tempTime;
   fPeakWatch->Start();

   fNPeaks      = FindPeaks();

   fPeakWatch->Stop();
   tempTime = fPeakWatch->RealTime();
   hPeakingFindingTime->Fill(tempTime);
   fPeakTime += tempTime;
   fQuantitiesWatch->Start();

   fNPartitions = Partition();
   fNClusters   = Cluster();

   fQuantitiesWatch->Stop();
   tempTime = fQuantitiesWatch->RealTime();
   hClusteringTime->Fill(tempTime);
   fQuantitiesWatch->Start();

   for (int kk = 0; kk < fClusters->GetEntries(); kk++) {
      /*      new((*clusters)[kk]) BIGCALCluster();*/
      CalculateQuantities((BIGCALCluster*)(*fClusters)[kk] , kk);
   }

   fQuantitiesWatch->Stop();
   fQuantitiesTime += (fQuantitiesWatch->RealTime());

   fTotalWatch->Stop();
   tempTime = fTotalWatch->RealTime();
   fTotalTime += tempTime;
   hTotalClusteringTime->Fill(tempTime);

   return (fNClusters);

//   if(!clusters) std::cout << " you suck, give me something to work with \n";
//   if(!fSourceHistogram && !fEvent)
//   {
//     std::cout << " Error No Histogram or event supplied for Cluster Searches!\n" ;
//     return(1);
//   }
//
//   if(fEvent) fSourceHistogram = GetSourceHistogram(); // fills the histogram everytime
//   fNCluster = 0;

//   for (Int_t kk=0;kk<fNCluster;kk++)
//   {
//     new((*clusters)[kk]) InSANECluster();
//     CalculateQuantities( (InSANECluster*)(*clusters)[kk] ,kk );
//   }
//return(fNClusters);
}
//___________________________________________________________________________//

void InSANEClusterProcessor::PrintTime()
{
   std::cout << "        Total Time clustering:  "
             <<  fTotalTime << " seconds\n"
             << "  Fraction creating histogram:  "
             <<  fSourceHistTime / fTotalTime << " \n"
             << "                finding peaks:  "
             <<  fPeakTime / fTotalTime << " \n"
             << "          calculating moments:  "
             <<  fQuantitiesTime / fTotalTime << " \n";
}
//___________________________________________________________________________//


