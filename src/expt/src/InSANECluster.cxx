#define InSANECluster_cxx
#include "InSANECluster.h"


ClassImp(InSANECluster)


//______________________________________________________________________________
InSANECluster::InSANECluster(Int_t clustNum) {
   Clear();
   fClusterNumber = clustNum;
}
//______________________________________________________________________________
InSANECluster::~InSANECluster() {
} 
//______________________________________________________________________________
void InSANECluster::Clear(Option_t * opt) {
   fXMoment       = -100.0;
   fYMoment       = -150.0;
   fX2Moment      = 0.0;
   fY2Moment      = 0.0;
   fX3Moment      = 0.0;
   fY3Moment      = 0.0;
   fXStdDeviation = 0.0;
   fYStdDeviation = 0.0;
   fXSkewness     = 0.0;
   fYSkewness     = 0.0;
   fX4Moment      = 0.0;
   fY4Moment      = 0.0;
   fXKurtosis     = 0.0;
   fYKurtosis     = 0.0;
   fRunNumber     = 0;
   fEventNumber   = 0;
   fClusterNumber = 0;
}
//______________________________________________________________________________
InSANECluster& InSANECluster::operator=(const InSANECluster &rhs) {
   // Check for self-assignment!
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   // Deallocate, allocate new space, copy values...
   fClusterNumber = rhs.fClusterNumber;
   fRunNumber     = rhs.fRunNumber;
   fEventNumber   = rhs.fEventNumber;
   fXMoment       = rhs.fXMoment;
   fYMoment       = rhs.fYMoment ;
   fX2Moment      = rhs.fX2Moment   ;
   fY2Moment      = rhs.fY2Moment;
   fX3Moment      = rhs.fX3Moment;
   fY3Moment      = rhs.fY3Moment;
   fX4Moment      = rhs.fX4Moment   ;
   fY4Moment      = rhs.fY4Moment;
   fXStdDeviation = rhs.fXStdDeviation;
   fYStdDeviation = rhs.fYStdDeviation;
   fXSkewness     = rhs.fXSkewness;
   fYSkewness     = rhs.fYSkewness;
   fXKurtosis     = rhs.fXKurtosis;
   fYKurtosis     = rhs.fYKurtosis;
   return *this;
}

