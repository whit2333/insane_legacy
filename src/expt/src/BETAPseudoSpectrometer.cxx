#include "BETAPseudoSpectrometer.h"
#include "SANERunManager.h"

ClassImp(InSANETrajectoryAnalysis)

ClassImp(SANETrajectoryAnalyzer)

void SANETrajectoryAnalyzer::MakePlots()
{
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   std::cout << " = Executing pass3/lookAtAsymmetries.cxx\n";
   gROOT->ProcessLine(Form(".x pass3/lookAtAsymmetries.cxx(%d) ", fRunNumber));
}

ClassImp(BETAPseudoSpectrometer)

Int_t BETAPseudoSpectrometer::ProcessEvent()
{
   if (!(fTriggerEvent->IsBETA2Event()))
      return(0);
   fMirror->ProcessEvent(fTrajectory);
   if (fMirror->HasHit()) fCounts++;
   return(0);
}

ClassImp(BETAPseudoSpectrometersCalc)


