#define InSANEDetectorAnalysis_cxx
#include "SANEEvents.h"
#include "InSANEDetectorAnalysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "BETAEvent.h"
#include "GasCherenkovHit.h"
#include "InSANEDetectorPedestal.h"
#include "InSANEDetectorTiming.h"
#include "TClonesArray.h"

ClassImp(InSANEDetectorAnalysis)
//_____________________________________________________________________________

InSANEDetectorAnalysis::InSANEDetectorAnalysis(const char * sourceTreeName)
   : InSANEAnalysis(sourceTreeName)
{
   //if (InSANERunManager::GetRunManager()->GetVerbosity() > 0) std::cout << " o InSANEDetectorAnalysis Constructor\n";
   auto * runManager = (SANERunManager*) SANERunManager::GetRunManager();
   fDebug                      = runManager->GetVerbosity();
   fOutputTree                 = nullptr;
   fAnalysisTree               = nullptr;
   fRunNumber                  = runManager->GetCurrentRun()->GetRunNumber();

   /// \todo Fix in future for multiple files per run 
   // Get the appropriate file for input (READ ONLY)
   if (runManager->GetPreviousFile()) fAnalysisFile = runManager->GetPreviousFile();
   else                               fAnalysisFile = runManager->GetCurrentFile();

   if(fAnalysisFile)                  fAnalysisFile->cd();
   else Error("InSANEDetectorAnalysis Constructor","Input analysis file not found");

   // Gets the source tree for the analysis (READ ONLY)
   fAnalysisTree = (TTree*)gROOT->FindObject(sourceTreeName);
   if (!fAnalysisTree) Error("InSANEDetectorAnalysis constructor","Input analysis tree not found!");

   // Allocates and sets branches for event,hit memory from the sourceTreeName
   if( fDebug > 1) printf(" + Creating SANE Events \n");
   fEvents = new SANEEvents(sourceTreeName);

   // Scalers
   if (InSANERunManager::GetRunManager()->GetVerbosity() > 0) {
      printf(" + Creating SANE Scalers\n");
   }
   fScalers = new SANEScalers();
   //fScalers->SetTriggerBranch(fEvents->TRIG);

   runManager->GetCurrentFile()->cd();

   // useful pointers
   //aBetaEvent = fEvents->BETA;
   //bcEvent    = fEvents->BETA->fBigcalEvent;
   //lhEvent    = fEvents->BETA->fLuciteHodoscopeEvent;
   //gcEvent    = fEvents->BETA->fGasCherenkovEvent;
   //ftEvent    = fEvents->BETA->fForwardTrackerEvent;

   // All the BETA detectors are created by BETADetectorPackage
   fBETADetector      = new BETADetectorPackage("BETA-DetectorPackage", fRunNumber);
   fTrackerDetector   = fBETADetector->fTrackerDetector;
   fCherenkovDetector = fBETADetector->fCherenkovDetector;
   fHodoscopeDetector = fBETADetector->fHodoscopeDetector;
   fBigcalDetector    = fBETADetector->fBigcalDetector;

   fBETADetector->SetEventsAddress(fEvents->BETA);
   fTrackerDetector->SetEventAddress(fEvents->BETA->fForwardTrackerEvent);
   fCherenkovDetector->SetEventAddress(fEvents->BETA->fGasCherenkovEvent);
   fHodoscopeDetector->SetEventAddress(fEvents->BETA->fLuciteHodoscopeEvent);
   fBigcalDetector->SetEventAddress(fEvents->BETA->fBigcalEvent);

   fAnalysisFile->cd();

   if (fDebug > 1) std::cout << " o End of InSANEDetectorAnalysis Constructor \n";

}
//_____________________________________________________________________________

InSANEDetectorAnalysis::~InSANEDetectorAnalysis()
{
   std::cout << " o InSANEDetectorAnalysis Destructor. \n";
   if (fBETADetector) delete fBETADetector;
   fBETADetector = nullptr;
}
//_____________________________________________________________________________

Int_t InSANEDetectorAnalysis::AnalyzeRun()
{
   // Detectors used
   std::cout << " Creating detector configurations for this run... \n";

   ///// EVENT LOOP  //////
   Int_t nevent = fEvents->fTree->GetEntries();
   std::cout << "\n" << nevent << " ENTRIES \n";
   //TClonesArray * cerHits = gcEvent->fGasCherenkovHits;
   //TClonesArray * lucHits = lhEvent->fLuciteHits;
   //TClonesArray * bigcalHits = bcEvent->fBigcalHits;
   //TClonesArray * trackerHits = ftEvent->fTrackerHits;

   for (int iEVENT = 0; iEVENT < nevent; iEVENT++)     {
      fEvents->GetEvent(iEVENT);
      //if( fEvents->TRIG->gen_event_trigtype[3] ) { // BETA2
      // Loop over lucite hits
      //      for(int kk=0; kk< lhEvent->fNumberOfHits;kk++) {
      //      aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;
      //do something
      //} // End loop over Lucite Hits
      //} // beta trigger bit
      aBetaEvent->ClearEvent();
   } // END OF EVENT LOOP

   return(0);
}
//_____________________________________________________________________________

Int_t InSANEDetectorAnalysis::Visualize()
{

   return(0);
}
//_____________________________________________________________________________

