#define SANELuciteHodoscopeAnalysis_cxx
#include "SANEEvents.h"
#include "SANELuciteHodoscopeAnalysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "LuciteHodoscopeEvent.h"
#include "LuciteHodoscopeHit.h"

ClassImp(SANELuciteHodoscopeAnalysis)

//__________________________________________________
SANELuciteHodoscopeAnalysis::SANELuciteHodoscopeAnalysis(const char * sourceTreeName): InSANEDetectorAnalysis(sourceTreeName)
{
   /*
   // Make sure this comes after opening the file
     printf("  - Output File: data/rootfiles/InSANE.%d.root\n",fRunNumber);
     fOutputFile = new TFile(Form("data/rootfiles/InSANE.%d.root\n",fRunNumber),"UPDATE");
     if( fOutputFile )  printf("  + Output File Opened\n");*/

   fOutputTree = new TTree("hodoscopeEvents", "Detector Event Data");
   fOutputTree->Branch("betaDetectorEvent", "BETAEvent", &fEvents->BETA);
   fOutputTree->Branch("trigDetectorEvent", "InSANETriggerEvent", &fEvents->TRIG);
   fOutputTree->Branch("hmsDetectorEvent", "HMSEvent", &fEvents->HMS);
   fOutputTree->Branch("beamDetectorEvent", "HallCBeamEvent", &fEvents->BEAM);
   fOutputTree->SetAutoSave();

   AllocateHistograms();
//   fEvents = (InSANEAnalysisEvents*) fEvents;
   fAnalysisFile->cd();

   /// \todo {Implement DETECTORPhysicsEvent }

}

//__________________________________________________
Int_t  SANELuciteHodoscopeAnalysis::AllocateHistograms()
{
   fAnalysisFile->cd();

   fAnalysisFile->mkdir("hodoscope");
   fAnalysisFile->cd("hodoscope");
   for (int i = 0; i < 56; i++) {
      luc_tdc_hist_pos[i] = new TH1F(Form("luc_tdc_hist_pos%d", i), Form("Hodoscope TDC %d", i), 200, -3000, -1200);
      luc_adc_hist_pos[i] = new TH1F(Form("luc_adc_hist_pos%d", i), Form("Hodoscope  ADC %d", i), 200, -100, 2900);
      luc_tdc_vs_adc_hist_pos[i] = new TH2F(Form("luc_tdc_vs_adc_hist_pos%d", i), Form("Hodoscope TDC vs ADC%d", i), 200, -100, 2900, 200, -3000, -1200);
      luc_tdc_hist_neg[i] = new TH1F(Form("luc_tdc_hist_neg%d", i), Form("Hodoscope TDC %d", i), 200, -3000, -1200);
      luc_adc_hist_neg[i] = new TH1F(Form("luc_adc_hist_neg%d", i), Form("Hodoscope  ADC %d", i), 200, -100, 2900);
      luc_tdc_vs_adc_hist_neg[i] = new TH2F(Form("luc_tdc_vs_adc_hist_neg%d", i), Form("Hodoscope TDC vs ADC%d", i), 200, -100, 2900, 200, -3000, -1200);

      luc_tdc_hist_diff[i] = new TH1F(Form("luc_tdc_hist_diff%d", i), Form("Hodoscope TDC %d", i), 200, -400, 600);
      luc_bar_hit[i] = new TH2F(Form("luc_bar_hit%d", i), Form("Hodoscope TDC hits per bar%d", i), 5, 0, 5, 28, 1, 29);
   }

   for (int i = 0; i < 12 * 28; i++) {
      cer_tdc_hist_luc_pos[i] = new TH1F(Form("cer_tdc_hist_luc_%d_pos", i), Form("Cherenkov TDC %d with lucite hit", i), 200, -3000, -1200);
      cer_adc_hist_luc_pos[i] = new TH1F(Form("cer_adc_luc_%d_pos", i), Form("Cherenkov  ADC %d with lucite hit", i), 100, 0, 4000);
      cer_tdc_vs_adc_hist_luc_pos[i] = new TH2F(Form("cer_tdc_vs_adc_hist_luc_%d_pos", i), Form("Cherenkov TDC vs ADC with lucite hit%d", i), 100, 0, 4000, 200, -3000, -1200);

      cer_tdc_hist_luc_neg[i] = new TH1F(Form("cer_tdc_hist_luc_%d_neg", i), Form("Cherenkov TDC %d with lucite hit", i), 200, -3000, -1200);
      cer_adc_hist_luc_neg[i] = new TH1F(Form("cer_adc_luc_%d_neg", i), Form("Cherenkov  ADC %d with lucite hit", i), 100, 0, 4000);
      cer_tdc_vs_adc_hist_luc_neg[i] = new TH2F(Form("cer_tdc_vs_adc_hist_luc_%d_neg", i), Form("Cherenkov TDC vs ADC with lucite hit%d", i), 100, 0, 4000, 200, -3000, -1200);
   }

   return(0);

}
//__________________________________________________

SANELuciteHodoscopeAnalysis::~SANELuciteHodoscopeAnalysis()
{
   fOutputTree->Write();
   fOutputFile->Close();
}
//__________________________________________________

Int_t SANELuciteHodoscopeAnalysis::AnalyzeRun()
{


   //LuciteHodoscopeHit* aLucHit;
   //TClonesArray * lucHits = fEvents->BETA->fLuciteHodoscopeEvent->fLuciteHits;
// Detectors used
   std::cout << " Creating detector configurations for this run... \n";
//   ForwardTrackerDetector *    tracker   = new ForwardTrackerDetector(fRunNumber);
//   GasCherenkovDetector *      cherenkov = new GasCherenkovDetector(fRunNumber);
   //LuciteHodoscopeDetector *   hodoscope = new LuciteHodoscopeDetector(fRunNumber);
//   BigcalDetector *            bigcal    = new BigcalDetector(fRunNumber);
///// EVENT LOOP  //////

   //TClonesArray * cerHits = gcEvent->fGasCherenkovHits;

///// EVENT LOOP  //////
   Int_t nevent = fEvents->fTree->GetEntries();
   for (int iEVENT = 0; iEVENT < nevent; iEVENT++) {
      fEvents->GetEvent(iEVENT);
      /*
        if( fEvents->TRIG->gen_event_trigtype[3]) { // BETA2
      //cout  << iEVENT << " event \n";

      // loop over hodoscope hits for event
        for (int kk=0; kk< fEvents->BETA->fLuciteHodoscopeEvent->fNumberOfHits;kk++)
       {
         aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;


            //look to see if there are considerable hits with only one side firing...
            if(TMath::Abs(
               aLucHit->fNegativeTDC - hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeak)
               < 10.0*hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeakWidth ) {
      // Negative side fires only
           hodoscope->fNegativeBarTDCHit[aLucHit->fRow-1]=true;
           cer_tdc_hist_luc_neg[aLucHit->fRow-1]->Fill(aLucHit->fNegativeTDC);*/
      /*     cer_tdc_vs_adc_hist_luc_neg[i]->Fill(fNegativeADC,fNegativeTDC);*/
//       }

//        if( TMath::Abs(aLucHit->fPositiveTDC - hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeak)
//        < 10.0*hodoscope->hodoscopeTimings[aLucHit->fRow-1]->fTDCPeakWidth ) {
//      hodoscope->fPositiveBarTDCHit[aLucHit->fRow-1]=true;
//
//      cer_tdc_hist_luc_pos[aLucHit->fRow-1]->Fill(aLucHit->fPositiveTDC);
// /*     cer_tdc_vs_adc_hist_luc_neg[i]->Fill(fPositiveADC,fPositiveTDC);*/
//        }
//       //Loose Lucite TDC Cut (Hits on Both sides of the bar pass a TDC window cut)
//       if(
//        hodoscope->fNegativeBarTDCHit[aLucHit->fRow-1]  &&
//        hodoscope->fPositiveBarTDCHit[aLucHit->fRow-1] ) {
//          hodoscope->fLucHit=true;
//          if(hodoscope->fBarTimedTDCHit[aLucHit->fRow-1])
//          hodoscope->fBarDoubleTimedTDCHit[aLucHit->fRow-1]=true;
//          hodoscope->fBarTimedTDCHit[aLucHit->fRow-1]=true;


//       luc_tdc_hist_pos[aLucHit->fRow-1]->Fill(aLucHit->fPositiveTDC);
//       luc_adc_hist_pos[aLucHit->fRow-1]->Fill(aLucHit->fPositiveADC);
//       luc_tdc_vs_adc_hist_pos[aLucHit->fRow-1]->Fill(aLucHit->fPositiveADC,aLucHit->fPositiveTDC);
//
//       luc_tdc_hist_neg[aLucHit->fRow-1]->Fill(aLucHit->fNegativeTDC);
//       luc_adc_hist_neg[aLucHit->fRow-1]->Fill(aLucHit->fNegativeADC);
//       luc_tdc_vs_adc_hist_neg[aLucHit->fRow-1]->Fill(aLucHit->fNegativeADC,aLucHit->fNegativeTDC);
//
//       luc_tdc_hist_diff[aLucHit->fRow-1]->Fill(aLucHit->fPositiveTDC - aLucHit->fNegativeTDC);
//       }
//     } // End loop over Lucite Hits
//
//     for(int jj=0;jj<28;jj++){
//       if(hodoscope->fBarTimedTDCHit[jj]) {
//         for(int k=0;k<28;k++){
//           if(hodoscope->fBarTimedTDCHit[k])luc_bar_hit[jj]->Fill(1.0,k+1);
//           if(hodoscope->fBarDoubleTimedTDCHit[k]) luc_bar_hit[jj]->Fill(2.0,k+1);
//         }
//       }
//     }

// loop over cherenkov hits for event     j*56

//     for (int kk=0; kk< gcEvent->fNumberOfHits;kk++) {
//       for(int jj=0;jj<28;jj++){
//         aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
//         if(hodoscope->fNegativeBarTDCHit[jj])
//           cer_adc_hist_luc_neg[(aCerHit->fMirrorNumber-1)*28+jj]->Fill(aCerHit->fADC);
//         if(hodoscope->fPositiveBarTDCHit[jj])
//           cer_adc_hist_luc_pos[(aCerHit->fMirrorNumber-1)*28+jj]->Fill(aCerHit->fADC);
//       }
//     }
//
//     if(hodoscope->fLucHit) fOutputTree->Fill();
//
//     } // coin trigger bit
//         hodoscope->ClearEvent();
//         aBetaEvent->ClearEvent();
   } // END OF EVENT LOOP

   return(0);
}

//__________________________________________________
Int_t SANELuciteHodoscopeAnalysis::Visualize()
{
   TCanvas * c1;
   for (int j = 0; j < 1; j++) {
      c1  = new TCanvas(Form("LuciteBarHit%d", j), Form("Lucite hits per bar %d", j));
      c1->Divide(7, 4);
      for (int i = 0; i < 28; i++) {
         c1->cd(i + 1);
         luc_bar_hit[27-i]->Draw("colz");
      }
      c1->SaveAs(Form("plots/%d/LuciteBarHits.jpg", fRunNumber));
      c1->SaveAs(Form("plots/%d/LuciteBarHits.ps", fRunNumber));
   }

//   for(int j=0;j<12;j++){
//   c1  = new TCanvas(Form("Cherenkov%dWithNegLucHit",j),Form("Cherenkov %d with Negative Lucite Hits",j));
//   c1->Divide(7,4);
//   for(int i =0;i<28;i++) {
//   c1->cd(i+1);
//   cer_adc_hist_luc_pos[j*28+27-i]->Draw();
//   }
//   c1->SaveAs(Form("plots/%d/Cherenkov%dWithNegLucHit.jpg",fRunNumber,j));
//   c1->SaveAs(Form("plots/%d/Cherenkov%dWithNegLucHit.ps",fRunNumber,j));
//   }

   for (int j = 0; j < 12; j++) {
      c1  = new TCanvas(Form("Cherenkov%dWithPosNegLucHit", j), Form("Cherenkov %d with a Lucite Hit", j));
      c1->Divide(7, 4);
      for (int i = 0; i < 28; i++) {
         c1->cd(i + 1);
         cer_adc_hist_luc_neg[j*28+27-i]->SetLineColor(9);
         cer_adc_hist_luc_neg[j*28+27-i]->Draw();
         cer_adc_hist_luc_pos[j*28+27-i]->SetLineColor(8);
         cer_adc_hist_luc_pos[j*28+27-i]->Draw("same");

      }
      c1->SaveAs(Form("plots/%d/Cherenkov%dWithPosNegLucHit.jpg", fRunNumber, j));
      c1->SaveAs(Form("plots/%d/Cherenkov%dWithPosNegLucHit.ps", fRunNumber, j));
   }

   TH1F * h2;
   for (int j = 0; j < 12; j++) {
      c1  = new TCanvas(Form("Cherenkov%dWithALucHitDiff", j), Form("Cherenkov %d with a Lucite Hit Diff", j));
      c1->Divide(7, 4);
      for (int i = 0; i < 28; i++) {
         h2 = (TH1F *)cer_adc_hist_luc_pos[j*28+27-i]->Clone();
         c1->cd(i + 1);
         h2->Add(cer_adc_hist_luc_neg[j*28+27-i], -1.0);
         h2->Draw();
      }
      c1->SaveAs(Form("plots/%d/Cherenkov%dWithALucHitDiff.jpg", fRunNumber, j));
      c1->SaveAs(Form("plots/%d/Cherenkov%dWithALucHitDiff.ps", fRunNumber, j));
   }



   return(0);

}




