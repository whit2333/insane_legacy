#include "InSANERunSummary.h"

ClassImp(InSANERunSummary)
//______________________________________________________________________________
InSANERunSummary::InSANERunSummary(const char * n ,const char * t ) : 
   TNamed(n,t), fBeamEnergy(0.0), fPBeam(0.0), fPTarget(0.0), 
   fNPlus(0.0), fNMinus(0.0), fQPlus(0.0), fQMinus(0.0),
   fLPlus(0.0), fLMinus(0.0), fPbSign(1.0), fPtSign(1.0),
   fHalfWavePlate(0.0), fPackingFraction(0.0)
{ }
////______________________________________________________________________________
//InSANERunSummary::InSANERunSummary(const InSANERunSummary& rhs):TNamed(rhs) {
//   fBeamEnergy    = rhs.fBeamEnergy;
//   fPBeam         = rhs.fPBeam;
//   fPTarget       = rhs.fPTarget;
//   fNPlus         = rhs.fNPlus;
//   fNMinus        = rhs.fNMinus;
//   fQPlus         = rhs.fQPlus;
//   fQMinus        = rhs.fQMinus;
//   fLPlus         = rhs.fLPlus;
//   fLMinus        = rhs.fLMinus;
//   fPtSign        = rhs.fPtSign;
//   fPbSign        = rhs.fPbSign;
//   fHalfWavePlate = rhs.fHalfWavePlate;
//   fRuns.clear();
//   for(unsigned int i = 0;i<rhs.fRuns.size() ; i++) fRuns.push_back(rhs.fRuns.at(i));
//}
//______________________________________________________________________________
InSANERunSummary::~InSANERunSummary(){
}
//______________________________________________________________________________
//InSANERunSummary& InSANERunSummary::operator=(const InSANERunSummary &rhs) {
//   // Only do assignment if RHS is a different object from this.
//   if (this != &rhs) {
//      TNamed::operator=(rhs);
//      fBeamEnergy    = rhs.fBeamEnergy;
//      fHalfWavePlate = rhs.fHalfWavePlate;
//      fPBeam     = rhs.fPBeam;
//      fPTarget   = rhs.fPTarget;
//      fNPlus     = rhs.fNPlus;
//      fNMinus    = rhs.fNMinus;
//      fQPlus     = rhs.fQPlus;
//      fQMinus    = rhs.fQMinus;
//      fLPlus     = rhs.fLPlus;
//      fLMinus    = rhs.fLMinus;
//      fPtSign    = rhs.fPtSign;
//      fPbSign    = rhs.fPbSign;
//      fRuns.clear();
//      for(unsigned int i = 0;i<rhs.fRuns.size() ; i++) fRuns.push_back(rhs.fRuns.at(i));
//   }
//   return *this;
//}
////______________________________________________________________________________
InSANERunSummary & InSANERunSummary::operator+=(const InSANERunSummary &rhs) {
   //if( GetOverallSign() != rhs.GetOverallSign() ) {
   // Warning("operator+=","GetOverallSign signs don't match: %.1f and %.1f",GetOverallSign(), rhs.GetOverallSign());
   //}
   //if(fPbSign != rhs.fPbSign) Warning("operator+=","Adding runs with different beam polarization sign!");
   //if(fPtSign != rhs.fPtSign) Warning("operator+=","Adding runs with different target polarization sign!");

   // Here norm charge returns the live-time times the charge : L*Q
   Double_t Qn1 = GetNormCharge();
   Double_t Qn2 = rhs.GetNormCharge();

   fPbSign      = 1.0;
   fPtSign      = 1.0;

   fBeamEnergy= ((Qn1)*fBeamEnergy + (Qn2)*rhs.fBeamEnergy) /(Qn1 + Qn2);
   fPBeam     = ((Qn1)*fPBeam      + (Qn2)*rhs.fPBeam)      /(Qn1 + Qn2);
   fPTarget   = ((Qn1)*fPTarget    + (Qn2)*rhs.fPTarget)    /(Qn1 + Qn2);

   fNPlus     = ((Qn1)*fNPlus      + (Qn2)*rhs.fNPlus)     / (Qn1 + Qn2);
   fNMinus    = ((Qn1)*fNMinus     + (Qn2)*rhs.fNMinus)    / (Qn1 + Qn2);

   // The live time is only charge weighted
   fLPlus     = ((fQPlus) *fLPlus  + (rhs.fQPlus) *rhs.fLPlus )/(fQPlus  + rhs.fQPlus );
   fLMinus    = ((fQMinus)*fLMinus + (rhs.fQMinus)*rhs.fLMinus)/(fQMinus + rhs.fQMinus);

   fQPlus     = fQPlus  + rhs.fQPlus ;
   fQMinus    = fQMinus + rhs.fQMinus;

   for(unsigned int i = 0;i<rhs.fRuns.size() ; i++) fRuns.push_back(rhs.fRuns.at(i));
   return *this;
}
//______________________________________________________________________________
void InSANERunSummary::Print(Option_t *) const {
   std::cout << std::setw(15) << "Run Summary:" << std::setw(10) << GetNum() << std::endl; 
   std::cout << std::setw(15) << "Beam Energy = " << std::setw(10) << fBeamEnergy << std::endl; 
   std::cout << std::setw(15) << "HWP         = " << std::setw(10) << fHalfWavePlate << std::endl; 
   std::cout << std::setw(15) << "Beam   Pol  = " << std::setw(10) << fPBeam << std::endl; 
   std::cout << std::setw(15) << "Beam   sign = " << std::setw(10) << fPbSign << std::endl; 
   std::cout << std::setw(15) << "Target Pol  = " << std::setw(10) << fPTarget << std::endl; 
   std::cout << std::setw(15) << "Target sign = " << std::setw(10) << fPtSign << std::endl; 
   std::cout << std::setw(15) << "LPlus       = " << std::setw(10) << fLPlus << std::endl; 
   std::cout << std::setw(15) << "LMinus      = " << std::setw(10) << fLMinus << std::endl; 
   std::cout << std::setw(15) << "QPlus       = " << std::setw(10) << fQPlus << std::endl; 
   std::cout << std::setw(15) << "QMinus      = " << std::setw(10) << fQMinus << std::endl; 
   std::cout << std::setw(15) << "Runs added  : ";
   for(unsigned int i = 0;i<fRuns.size() ; i++) {
      std::cout << " " << fRuns.at(i);
   }
   std::cout << std::endl;
}
//______________________________________________________________________________

