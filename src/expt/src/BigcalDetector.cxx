#include "BigcalDetector.h"
#include "SANERunManager.h"
#include "SANEAnalysisManager.h"

ClassImp(BigcalDetector)

//______________________________________________________________________________
BigcalDetector::BigcalDetector(Int_t runnum) {

   SetName("bigcal");
   SetTitle("BIGCAL");

   fBlockThreshold    = 15; //MeV
   fCalibrationSeries = 1;

   if (InSANERunManager::GetRunManager()->GetVerbosity() > 1)
      printf(" o BigcalDetector::Constructor \n");

   // ------------------------------------------
   // Get CUTS!
   SANEAnalysisManager * fAnalysisManager  = SANEAnalysisManager::GetAnalysisManager();

   // Energy Cuts
   cClusterEnergyMin     = fAnalysisManager->fEnergyMinCut;
   cClusterEnergyMax     = fAnalysisManager->fEnergyMaxCut;
   cTimingGroupEnergyMin = 400.0;
   cTimingGroupEnergyMax = 5900.0;

   // TDC Cuts
   cTDCMin      = fAnalysisManager->fBigcalTDCMinCut;
   cTDCMax      = fAnalysisManager->fBigcalTDCMaxCut;
   cTDCAlignMin = -50.0;
   cTDCAlignMax = 150.0;

   fClusterEvent        = nullptr;
   fNumberOfADCChannels = 32 * 32 + 30 * 24;
   fNumberOfTDCChannels = 56 * 4;

   fGeoCalc          = BIGCALGeometryCalculator::GetCalculator();
   fCoordinateSystem = new BigcalCoordinateSystem();
   fRotFromBCcoords  = fCoordinateSystem->GetRotationMatrixTo();

   fTypicalPedestal      = 450;
   fTypicalPedestalWidth = 3;
   fTypicalTDCPeak       = 450;
   fTypicalTDCPeakWidth  = 8;

   for (int i = 0; i < 38; i++) {
      fBigcal64SumTDCs[i] = new std::vector<Double_t>;
      fBigcal64SumTDCs[i]->clear();
      fBigcal64SumTDCAligns[i] = new std::vector<Double_t>;
      fBigcal64SumTDCAligns[i]->clear();
   }

   for (int i = 0; i < NBIGCALTDC; i++) {
      fBigcalTDCs[i] = new std::vector<Double_t>;
      fBigcalTDCs[i]->clear();
      fBigcalTDCAligns[i] = new std::vector<Double_t>;
      fBigcalTDCAligns[i]->clear();
      fBigcalTimingGroupEnergy[i] = 0.0;
      fBigcalTimingGroupTDC[i] = 0.0;
   }
}
//______________________________________________________________________________
BigcalDetector::~BigcalDetector() {
   for (int i = 0; i < 38; i++) {
      delete fBigcal64SumTDCs[i];
      delete fBigcal64SumTDCAligns[i];
   }
   for (int i = 0; i < NBIGCALTDC; i++) {
      delete fBigcalTDCs[i];
      delete fBigcalTDCAligns[i];
   }
   delete fCoordinateSystem;
   //if(fEventTimingHist) delete fEventTimingHist;
   //if(fEventADCHist) delete fEventADCHist;
}
//______________________________________________________________________________
void BigcalDetector::Initialize() {

   Int_t runNumber = InSANERunManager::GetRunManager()->GetCurrentRun()->GetRunNumber();

   // ------------------------------------
   // Pedestals
   //std::cout << " o BigcalDetector Constructor ... Intializing Pedestals ... \n";
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals");
   }
   //  fPedestals = new TClonesArray("InSANEDetectorPedestal",fNumberOfPedestals);
   for (Int_t i = 0; i < fNumberOfADCChannels; i++) {
      GetPedestals()->Add(new InSANEDetectorPedestal());
      //    bigcalPedestals[i] = (InSANEDetectorPedestal*)(fPedestals)[i];
   }

   // ------------------------------------
   // Timings
   //std::cout << " o BigcalDetector Constructor ... Intializing Timings ... \n";
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("timing");
   }
//  fTimings  = new TClonesArray("InSANEDetectorTiming",fNumberOfTDCs);
   for (Int_t i = 0 ; i < fNumberOfTDCChannels ; i++) {
      GetTimings()->Add(new InSANEDetectorTiming());
      //bigcalTimings[i] = (InSANEDetectorTiming*)(fTimings)[i];
   }
   for (Int_t i = 0 ; i < 38 ; i++) {
      GetTriggerTimings()->Add(new InSANEDetectorTiming());
   }

   // ------------------------------------
   // Timing group Pedestals
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals");
   }
   //  fPedestals = new TClonesArray("InSANEDetectorPedestal",fNumberOfPedestals);
   for (Int_t i = 0; i < fNumberOfTDCChannels; i++) {
      GetTimingGroupPedestals()->Add(new InSANEDetectorPedestal());
      //    bigcalPedestals[i] = (InSANEDetectorPedestal*)(fPedestals)[i];
   }


   SetRun( runNumber );

   // ------------------------------------
   // Detectors
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors/bigcal");
   }

   //std::cout << " o BigcalDetector Constructor ... Intializing histograms ... \n";
   InitHistograms();

   // ------------------------------------
   // Set Calibration Coefficients
   //std::cout << " o BigcalDetector Constructor ... Setting Calibration Coefficients... \n";
   if (InSANERunManager::GetRunManager()->GetCurrentRun()) {
      fCalibrationSeries = BIGCALGeometryCalculator::GetCalculator()->GetLatestCalibrationSeries(runNumber);
      //if (InSANERunManager::GetRunManager()->IsPerpendicularRun()) SetCalibrationNumber(2);
      //if (InSANERunManager::GetRunManager()->IsParallelRun()) SetCalibrationNumber(1);
   } else {
      fCalibrationSeries = 1;
   }
   SetCalibrationNumber(fCalibrationSeries);

   // ------------------------------------
   // Noisy Blocks 
   /// \todo how to handle noisy channels?
   //std::cout << " o BigcalDetector Constructor ... Adding noisy channels... \n";
   Int_t xi,yi;
   std::ifstream noisyfile("detectors/bigcal/bigcal_noisy_blocks.txt");
   if(noisyfile.is_open() && noisyfile.good() ) {
      while(!noisyfile.eof() && noisyfile.good()){
         noisyfile >> xi >> yi;
         if(!noisyfile.eof()){
            AddNoisyChannel(fGeoCalc->GetBlockNumber(xi,yi));
         }
      }
   }
   noisyfile.close();
   //AddNoisyChannel(1);
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(1, 1));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(2, 1));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(2, 2));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(1, 28));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(3, 54));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(3, 52));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(18, 8));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(16, 54));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(2, 49));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(6, 49));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(19, 9));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(5, 50));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(4, 52));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(20, 6));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(17, 55));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(16, 50));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(5, 5));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(27, 30));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(24, 11));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(18, 56));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(23, 34));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(24, 34));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(25, 34));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(26, 34));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(27, 34));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(28, 34));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(29, 34));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(30, 34));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(23, 28));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(23, 55));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(16, 55));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(4, 50));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(4, 53));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(4, 54));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(18, 56));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(2, 1));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(29, 20));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(29, 25));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(9, 6));
   //AddNoisyChannel(fGeoCalc->GetBlockNumber(13, 6));

   if (!IsBuilt()) Build();
   InSANEDatabaseManager::GetManager()->CloseConnections();
}
//______________________________________________________________________________


Int_t BigcalDetector::ClearEvent()
{
   /*   if(fEvent)  ((BigcalEvent*)fEvent)->ClearEvent("C");*/
   for (int i = 0; i < NBIGCALTDC; i++) {
      fBigcalTimingGroupEnergy[i] = 0.0;
      fBigcalTimingGroupTDC[i] = 0.0;
      cPassesTimingGroupEnergy[i] = false;
      cPassesClusterEnergy[i] = false;
      cPassesTDC[i] = false;
      fBigcalTDCs[i]->clear();
      fBigcalTDCAligns[i]->clear();
   }

   for (int i = 0; i < 38; i++) {
      fBigcal64SumTDCs[i]->clear();
      fBigcal64SumTDCAligns[i]->clear();
   }

   /// Clears the Event histograms
//    if (fEventTimingHist)         fEventTimingHist->Reset();
//    if (fEventADCHist)            fEventADCHist->Reset();
//    if (fEventTrigTimingHist)     fEventTrigTimingHist->Reset();
//    if (fEventTimingHistGood)     fEventTimingHistGood->Reset();
//    if (fEventTrigTimingHistGood) fEventTrigTimingHistGood->Reset();
//    if (fEventHist)               fEventHist->Reset();
//    if (fEventHistWithThreshold)  fEventHistWithThreshold->Reset();
   return(0);
}
//______________________________________________________________________________
Int_t BigcalDetector::SetRun(Int_t runnumber) {

   // pedestals
   fCurrentRunNumber = runnumber;
   for (int i = 0; i < fNumberOfADCChannels; i++) {
      GetDetectorPedestal(i)->GetPedestal("bigcal", runnumber, i + 1);
   }

   // timing group pedestals
   for (int i = 0; i < fNumberOfTDCChannels; i++) {
      GetDetectorTimingGroupPedestal(i)->GetPedestal("bigcalTimingGroup", runnumber, i + 1);
   }

   // smaller group tdc peaks
   for (int i = 0; i < fNumberOfTDCChannels; i++) {
      GetDetectorTiming(i)->GetTiming("bigcal", runnumber, i + 1);
   }

   // trigger group tdc peaks
   for (int i = 0; i < 38; i++) {
      GetDetectorTriggerTiming(i)->GetTiming("bigcalTrigger", runnumber, i + 1);
   }
   InSANEDatabaseManager::GetManager()->CloseConnections();

   return(0);
}
//______________________________________________________________________________

void BigcalDetector::PrintCuts() const {
   using std::cout;
   cout << "___________________________________\n";
   cout << "c      BigCal Cuts          \n";
   cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n";
   cout <<  Form("^  %6.1f  <  E(cluster)  <  %6.1f [MeV]    \n", cClusterEnergyMin, cClusterEnergyMax);
   //   cout <<  Form("^  %6d  <  NPE  <  %6d     \n",cNPEMin,cNPEMax);
   cout <<  Form("^  %6.1f  <    TDC    <  %6.1f     \n", cTDCMin, cTDCMax);
   cout <<  Form("^  %6.1f  <  E(TimingGroup) <  %6.1f     \n", cTimingGroupEnergyMin, cTimingGroupEnergyMax);
   //   cout <<  Form("^  %6d  <  ADC  <  %6d     \n",cADCMin,cPassesTDCAlign);
   cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n";
}

//______________________________________________________________________________
Int_t BigcalDetector::ProcessEvent() {

   if (fEvent) {
      BigcalHit * aBigcalHit = nullptr;

      // loop over bigcal hits
      for (int kk = 0; kk < ((BigcalEvent*)fEvent)->fBigcalHits->GetEntries(); kk++) {
         aBigcalHit = (BigcalHit*)(*(((BigcalEvent*)fEvent)->fBigcalHits))[kk] ;

         // ADC hits
         if (aBigcalHit->fLevel == 0) {
            //std::cout << "Group number = " << fGeoCalc->GetGroupNumber(aBigcalHit->fiCell, aBigcalHit->fjCell) << "\n";
            fBigcalTimingGroupEnergy[ fGeoCalc->GetGroupNumber(aBigcalHit->fiCell, aBigcalHit->fjCell) -1 ] += aBigcalHit->fEnergy;
         }
      }

      // loop over bigcal hits
      for (int kk = 0; kk < ((BigcalEvent*)fEvent)->fBigcalTriggerGroupHits->GetEntries(); kk++) {
         aBigcalHit = (BigcalHit*)(*(((BigcalEvent*)fEvent)->fBigcalTriggerGroupHits))[kk] ;

         // Trigger groups
         if (aBigcalHit->fTDCLevel == 3) {
            fBigcal64SumTDCs[     (aBigcalHit->fTDCRow)+(aBigcalHit->fTDCGroup-1)*19 -1]->push_back(aBigcalHit->fTDC);
            fBigcal64SumTDCAligns[(aBigcalHit->fTDCRow)+(aBigcalHit->fTDCGroup-1)*19 -1]->push_back(aBigcalHit->fTDCAlign);
         }
      }

   }

   if (fClusterEvent) {
      BIGCALCluster * aCluster;

      for (int i = 0; i < fClusterEvent->fNClusters; i++) {
         aCluster = (BIGCALCluster*)(*(fClusterEvent->fClusters))[i];

         // Cluster Energy Cuts
         if (aCluster->GetEnergy() > cClusterEnergyMin && aCluster->GetEnergy() < cClusterEnergyMax)
            if (aCluster->fClusterNumber < NBIGCALTDC) {
               cPassesClusterEnergy[aCluster->fClusterNumber] = true;
            }
         //aCluster = 
         //fClustersHist->Fill(aCluster->GetXmoment(), aCluster->GetYmoment());
         if (aCluster->fType <= 3) {
            fElectronClustersHist->Fill(aCluster->GetXmoment(), aCluster->GetYmoment());
         }
         if (aCluster->fType == 4) {
            fChargedPiClustersHist->Fill(aCluster->GetXmoment(), aCluster->GetYmoment());
         }
      }
   }
   return(0);
}
//______________________________________________________________________________
Int_t BigcalDetector::CorrectEvent() {
   if (!fEvent) {
      std::cout << "Must set detector event address\n";
      return(-1);
   }
   return(0);
}
//______________________________________________________________________________
void BigcalDetector::InitHistograms() {

   TFile * f = InSANERunManager::GetRunManager()->GetCurrentFile();
   f->cd();
   if (f) {
      /// Run accumulated histograms
      // something buggy here
      for (int i = 0; i < NBIGCALTDC; i++) {
         //f->cd();
         //std::cout << Form("detectors/bigcal/hist%d", i + 1) << std::endl;
         //if ( !(f->GetDirectory(Form("/detectors/bigcal/hist%d",i + 1))) ) {
               f->mkdir(Form("/detectors/bigcal/hist%d", i + 1));
         //}
         //f->cd(Form("detectors/bigcal/hist%d", i + 1));
      }

      f->cd("detectors/bigcal");
   }
   //std::cout << " test " << std::endl;

   //fEventDisplayHistograms = new TList();
   fEventDisplayHistograms.Clear();

   //fEventTimingHist = new TH2I("EventTimingHist", "Bigcal Event level timings", 4, 1, 4, 56, 1, 56);
   //fEventTimingHist->SetOption("colz");
   //fEventDisplayHistograms.Add(fEventTimingHist);

   //fEventTimingHistGood = new TH2I("EventTimingHistGood", "Bigcal Good Event level timings", 4, 1, 5, 56, 1, 57);
   //fEventTimingHistGood->SetOption("colz");
   //fEventDisplayHistograms.Add(fEventTimingHistGood);

   //fClustersHist = new TH2F("ClustersHist1", "Clusters Histogram", 35, -60, 60, 60, -110, 110);
   //fClustersHist->SetOption("colz");
   //fEventDisplayHistograms.Add(fClustersHist);

   //fChargedPiClustersHist = new TH2F("ClustersHist", "Clusters Histogram", 60, -60, 60, 110, -110, 110);
   //fChargedPiClustersHist->SetOption("colz");
   ////fEventDisplayHistograms->Add(fChargedPiClustersHist);

   //fEventTrigTimingHist = new TH2I("EventTrigTimingHist", "Bigcal Event level timings", 2, 1, 3, 19, 1, 20);
   //fEventTrigTimingHist->SetOption("colz");
   //fEventDisplayHistograms.Add(fEventTrigTimingHist);

   //fEventTrigTimingHistGood = new TH2I("EventTrigTimingHistGood", "Bigcal Good Event level timings", 2, 1, 3, 19, 1, 20);
   //fEventTrigTimingHistGood->SetOption("colz");
   //fEventDisplayHistograms.Add(fEventTrigTimingHistGood);

   //fEventADCHist = new TH2I("EventADCHist", "Bigcal Event level ADCs", 32, 1, 33, 56, 1, 57);
   //fEventADCHist->SetOption("colz");
   ////fEventDisplayHistograms->Add(fEventADCHist);

   //fEventHist = new TH2F("fEventHist", "Bigcal Event Energy", 32, 1, 33, 56, 1, 57);
   //fEventHist->SetOption("colz");
   ////fEventDisplayHistograms->Add(fEventHist);

   //fEventHistWithThreshold = new TH2F("fEventHistWithThreshold", Form("Bigcal Event Energy with threshold of %d MeV", fBlockThreshold), 32, 1, 33, 56, 1, 57);
   //fEventHistWithThreshold->SetOption("colz");
   //fEventDisplayHistograms.Add(fEventHistWithThreshold);

   //fElectronClustersHist = new TH2F("ClustersHist2", "Clusters Histogram", 60, -60, 60, 110, -110, 110);
   //fElectronClustersHist->SetOption("colz");
   ////fEventDisplayHistograms->Add(fElectronClustersHist);

   f->cd();
}
//______________________________________________________________________________

