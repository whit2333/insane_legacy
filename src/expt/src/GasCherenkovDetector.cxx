#include  "GasCherenkovDetector.h"
#include "InSANECherenkovMirror.h"
#include "InSANEToroidalMirror.h"
#include "InSANESphericalMirror.h"
#include "InSANERunManager.h"
#include "SANEAnalysisManager.h"
#include "InSANEDatabaseManager.h"
#include "InSANEDetectorCalibration.h"
#include "InSANECut.h"
#include "InSANECutter.h"

ClassImp(GasCherenkovDetector)

//______________________________________________________________________________
GasCherenkovDetector::GasCherenkovDetector(Int_t runnum) {

   if (InSANERunManager::GetRunManager()->GetVerbosity() > 1)
      printf(" o GasCherenkovDetector::Constructor \n");

   SetName("cherenkov");
   SetTitle("Gas Cherenkov");

   // Apparatus
   for (int i = 0; i < 12; i++) {
      AddComponent(new InSANECherenkovMirror(i + 1));
   }

   // Get Cuts!
   SANEAnalysisManager * fAnalysisManager  = SANEAnalysisManager::GetAnalysisManager();
   //InSANECutter * axe = fAnalysisManager->GetAxe();
   //InSANECut * aCut = 0;

   // TDC Cuts
   cTDCMin       = fAnalysisManager->fCherenkovMinTDCCut;
   cTDCMax       = fAnalysisManager->fCherenkovMaxTDCCut;
   cTDCAlignMin  = fAnalysisManager->fCherenkovMinTDCAlignCut;
   cTDCAlignMax  = fAnalysisManager->fCherenkovMaxTDCAlignCut;
   cTDCAlignMin2 = -1960.;
   cTDCAlignMax2 = -1835.0;

   // ADC Cuts
   cADCMin      = fAnalysisManager->fCherenkovMinADCCut;
   cADCMax      = fAnalysisManager->fCherenkovMaxADCCut;
   cADCAlignMin = fAnalysisManager->fCherenkovMinADCAlignCut;
   cADCAlignMax = fAnalysisManager->fCherenkovMaxADCAlignCut;

   // NPE Cuts
   cNPEMin = fAnalysisManager->fCherenkovMinNPECut;
   cNPEMax = fAnalysisManager->fCherenkovMaxNPECut;

   fNPEThresholdCut      = 5.0;
   fADCThresholdCut      = 500;

   fMirrorsToSum         = nullptr;
   fNumberOfPedestals    = 12;
   fNumberOfTDCs         = 12;
   fNumberOfScalers      = 12;

   fTypicalPedestal      =  500;    // channels
   fTypicalPedestalWidth =  6;      // channels
   fTypicalTDCPeak       = -2250;   // channels
   fTypicalTDCPeakWidth  =  15;     // channels

   fNominalTDCCutWidth   =  10.0;   // 

   fTDCRangeMin          = -2500;
   fTDCRangeMax          = -1500;

   TDirectory * aDir = nullptr;

   // Cut result histograms
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      if (! InSANERunManager::GetRunManager()->GetCurrentFile()->GetDirectory("detectors/cherenkov"))
         if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors"))
            aDir = InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("detectors");
      //          if(aDir) aDir->cd();
      //          aDir = aDir->mkdir("cherenkov");
      //          if(aDir) aDir->cd();
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors/cherenkov");
   }

   //   fCerHits = new TH2F("cherenkovHits","cherenkov",2,1,3,4,1,5);


   // Pedestals
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("pedestals");
   }
   //  fPedestals = new TClonesArray("InSANEDetectorPedestal",fNumberOfPedestals);
   for (int i = 0; i < 12; i++) {
      GetPedestals()->Add(new InSANEDetectorPedestal());
      //  cherenkovPedestals[i] = (InSANEDetectorPedestal*)(GetPedestals())[i];
   }

   // Timings
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("timing");
   }
   //  fTimings  = new TClonesArray("InSANEDetectorTiming",fNumberOfTDCs);
   for (int i = 0; i < 12; i++) {
      GetTimings()->Add(new InSANEDetectorTiming());
      //    cherenkovTimings[i] = (InSANEDetectorTiming*)(GetTimings())[i];
   }

   // Detectors
   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors/cherenkov");
   }


   fCurrentRunNumber = InSANERunManager::GetRunManager()->fCurrentRunNumber;

   InitHistograms();

   fEvent                = nullptr;
   fCorrectedEvent       = nullptr;
   fEventScaler          = nullptr;
   fCurrentScaler        = nullptr;
   fNextScaler           = nullptr;
   fPreviousScaler       = nullptr;
   fNumberOfScalerEvents = 0;
   fAverageScaler        = nullptr;

   fMirrorGeoCutX[0] = -60.0;
   fMirrorGeoCutX[1] = 0.0;
   fMirrorGeoCutX[2] = 60.0;

   fMirrorGeoCutY[0] = -112.0;
   fMirrorGeoCutY[1] = -55.0;
   fMirrorGeoCutY[2] = 0.0;
   fMirrorGeoCutY[3] = 55.0;
   fMirrorGeoCutY[4] = 112.0;

   SetRun(fCurrentRunNumber);

   fGeoCalc = GasCherenkovGeometryCalculator::GetCalculator();

   // --------------------------------------
   // Storage vectors
   for (int i = 0; i < 12 ; i++) {
      fCherenkovADCs[i]                       = new std::vector<Int_t>;
      fCherenkovNPEs[i]                       = new std::vector<Double_t>;
      fCherenkovADCsAligned[i]                = new std::vector<Double_t>;
      fCherenkovTDCs[i]                       = new std::vector<Int_t>;
      fCherenkovTDCsAligned[i]                = new std::vector<Double_t>;
      fCherenkovTimedTDCs[i]                  = new std::vector<Int_t>;
      fCherenkovTimedTDCsAligned[i]           = new std::vector<Double_t>;
   }

   fTimedTDCHitNumbers = new std::vector<Int_t>;

   for (int i = 0; i < 12; i++) {
      fMirrorTDCHit[i] = false;
      cPassesADC[i] = false;
      cPassesNPE[i] = false;
      cPassesTDC[i] = false;
   }


}
//______________________________________________________________________________
GasCherenkovDetector::~GasCherenkovDetector() {
   delete fTimedTDCHitNumbers;
   for (int i = 0; i < 12 ; i++) {
      delete fCherenkovADCs[i]             ; 
      delete fCherenkovNPEs[i]             ; 
      delete fCherenkovADCsAligned[i]      ; 
      delete fCherenkovTDCs[i]             ; 
      delete fCherenkovTDCsAligned[i]      ; 
      delete fCherenkovTimedTDCs[i]        ; 
      delete fCherenkovTimedTDCsAligned[i] ; 
   }
}
//______________________________________________________________________________
void GasCherenkovDetector::InitHistograms() {

   if (InSANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o GasCherenkovDetector::InitHistograms \n";

   //fEventDisplayHistograms = new TList();
   fEventDisplayHistograms.Clear();

   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      /// Run accumulated histograms
      for (int i = 0; i < 12; i++) {
         if (! InSANERunManager::GetRunManager()->GetCurrentFile()->GetDirectory(Form("detectors/cherenkov/hist%d", i + 1))) {
            if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors/cherenkov")) {
               if (! InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors"))
                  InSANERunManager::GetRunManager()->GetCurrentFile()->mkdir("detectors");
               gDirectory->mkdir("cherenkov");
            }
            gDirectory->mkdir(Form("hist%d", i + 1));
         }
         InSANERunManager::GetRunManager()->GetCurrentFile()->cd(Form("detectors/cherenkov/hist%d", i + 1));

         //hADCWithoutCut[i] = (TH1F*)gROOT->FindObject(Form("hADCWithoutCut%d", i + 1));
         //if (!hADCWithoutCut[i]) hADCWithoutCut[i] = new TH1F(Form("hADCWithoutCut%d", i + 1), "ADC Without Cut", 200, 0, 5000);

         //hADCWithADCCut[i] = (TH1F*)gROOT->FindObject(Form("hADCWithADCCut%d", i + 1));
         //if (!hADCWithADCCut[i]) hADCWithADCCut[i] = new TH1F(Form("hADCWithADCCut%d", i + 1), "ADC With an ADC Cut", 200, 0, 5000);

         //hADCWithTDCCut[i] = (TH1F*)gROOT->FindObject(Form("hADCWithTDCCut%d", i + 1));
         //if (!hADCWithTDCCut[i]) hADCWithTDCCut[i] = new TH1F(Form("hADCWithTDCCut%d", i + 1), "ADC With a TDC Cut", 200, 0, 5000);

         //hADCWithADCandTDCCut[i] = (TH1F*)gROOT->FindObject(Form("hADCWithADCandTDCCut%d", i + 1));
         //if (!hADCWithADCandTDCCut[i]) hADCWithADCandTDCCut[i] = new TH1F(Form("hADCWithADCandTDCCut%d", i + 1), "ADC With TDC  and ADC Cuts", 200, 0, 5000);

         //hTDCWithoutCut[i] = (TH1F*)gROOT->FindObject(Form("hTDCWithoutCut%d", i + 1));
         //if (!hTDCWithoutCut[i]) hTDCWithoutCut[i] = new TH1F(Form("hTDCWithoutCut%d", i + 1), "TDC Without Cut", 100, -2500, -1500);

         //hTDCWithADCCut[i] = (TH1F*)gROOT->FindObject(Form("hTDCWithADCCut%d", i + 1));
         //if (!hTDCWithADCCut[i]) hTDCWithADCCut[i] = new TH1F(Form("hTDCWithADCCut%d", i + 1), "TDC With an ADC Cut", 100, -2500, -1500);

         //hTDCWithTDCCut[i] = (TH1F*)gROOT->FindObject(Form("hTDCWithTDCCut%d", i + 1));
         //if (!hTDCWithTDCCut[i]) hTDCWithTDCCut[i] = new TH1F(Form("hTDCWithTDCCut%d", i + 1), "TDC With a TDC Cut", 100, -2500, -1500);

         //hTDCWithADCandTDCCut[i] = (TH1F*)gROOT->FindObject(Form("hTDCWithADCandTDCCut%d", i + 1));
         //if (!hTDCWithADCandTDCCut[i]) hTDCWithADCandTDCCut[i] = new TH1F(Form("hTDCWithADCandTDCCut%d", i + 1), "TDC With TDC  and ADC Cuts", 100, -2500, -1500);

         //hADCAlignedWithoutCut[i] = (TH1F*)gROOT->FindObject(Form("hADCAlignedWithoutCut%d", i + 1));
         //if (!hADCAlignedWithoutCut[i]) hADCAlignedWithoutCut[i] = new TH1F(Form("hADCAlignedWithoutCut%d", i + 1), "ADCAlign Without Cut", 100, 0, 10);

         //hADCAlignedWithTDCCut[i] = (TH1F*)gROOT->FindObject(Form("hADCAlignedWithTDCCut%d", i + 1));
         //if (!hADCAlignedWithTDCCut[i]) hADCAlignedWithTDCCut[i] = new TH1F(Form("hADCAlignedWithTDCCut%d", i + 1), "ADCAlign With an ADC Cut", 100, 0, 10);

         //hADCAlignedWithADCCut[i] = (TH1F*)gROOT->FindObject(Form("hADCAlignedWithADCCut%d", i + 1));
         //if (!hADCAlignedWithADCCut[i]) hADCAlignedWithADCCut[i] = new TH1F(Form("hADCAlignedWithADCCut%d", i + 1), "ADCAlign With a TDC Cut", 100, 0, 10);

         //hADCAlignedWithADCandTDCCut[i] = (TH1F*)gROOT->FindObject(Form("hADCAlignedWithADCandTDCCut%d", i + 1));
         //if (!hADCAlignedWithADCandTDCCut[i]) hADCAlignedWithADCandTDCCut[i] = new TH1F(Form("hADCAlignedWithADCandTDCCut%d", i + 1), "ADCAlign With TDC  and ADC Cuts", 100, 0, 10);

         //hTDCAlignedWithoutCut[i] = (TH1F*)gROOT->FindObject(Form("hTDCAlignedWithoutCut%d", i + 1));
         //if (!hTDCAlignedWithoutCut[i]) hTDCAlignedWithoutCut[i] = new TH1F(Form("hTDCAlignedWithoutCut%d", i + 1), "TDCAlign Without Cut", 100, -500, 500);

         //hTDCAlignedWithTDCCut[i] = (TH1F*)gROOT->FindObject(Form("hTDCAlignedWithTDCCut%d", i + 1));
         //if (!hTDCAlignedWithTDCCut[i]) hTDCAlignedWithTDCCut[i] = new TH1F(Form("hTDCAlignedWithTDCCut%d", i + 1), "TDCAlign With an ADC Cut", 100, -500, 500);

         //hTDCAlignedWithADCCut[i] = (TH1F*)gROOT->FindObject(Form("hTDCAlignedWithADCCut%d", i + 1));
         //if (!hTDCAlignedWithADCCut[i]) hTDCAlignedWithADCCut[i] = new TH1F(Form("hTDCAlignedWithADCCut%d", i + 1), "TDCAlign With a TDC Cut", 100, -500, 500);

         //hTDCAlignedWithADCandTDCCut[i] = (TH1F*)gROOT->FindObject(Form("hTDCAlignedWithADCandTDCCut%d", i + 1));
         //if (!hTDCAlignedWithADCandTDCCut[i]) hTDCAlignedWithADCandTDCCut[i] = new TH1F(Form("hTDCAlignedWithADCandTDCCut%d", i + 1), "TDCAlign With TDC  and ADC Cuts", 100, -500, 500);
      }
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors/cherenkov");
   }

   fCerHits = new TH2F("cherenkovHits", "cherenkov", 2, 1, 3, 4, 1, 5);
   fCerHits->SetOption("colz");
   fEventDisplayHistograms.Add(fCerHits);

   fCerHitsGood = new TH2F("cherenkovHitsGood", "Good Cherenkov Hits", 2, 1, 3, 4, 1, 5);
   fCerHitsGood->SetOption("colz");
   fEventDisplayHistograms.Add(fCerHitsGood);
}
//______________________________________________________________________________
Int_t GasCherenkovDetector::SetRun(Int_t runnumber) {

   fCurrentRunNumber = runnumber;

   for (int i = 0; i < 12; i++) {
      GetDetectorTiming(i)->GetTiming("cherenkov", runnumber, i);
      GetDetectorPedestal(i)->GetPedestal("cherenkov", runnumber, i);
   }

   Set1PECalibrations(runnumber);

   GetADCAlignmentCalibrations(runnumber);
   InSANEDatabaseManager::GetManager()->CloseConnections();
   return(0);
}
//______________________________________________________________________________
Int_t GasCherenkovDetector::ProcessEvent() {
   if (fEvent) {
      GasCherenkovHit * aCerHit;

      ((GasCherenkovEvent*)fEvent)->fNumberOfTimedTDCHits = 0;

      for (int kk = 0; kk < ((GasCherenkovEvent*)fEvent)->fNumberOfHits; kk++) {
         aCerHit = (GasCherenkovHit*)(*(((GasCherenkovEvent*)fEvent)->fGasCherenkovHits))[kk];

         // \note All histogram binning is done with the TDC hits which
         // after the zeroth pass have the ADC values set

         if (aCerHit->fLevel == 0) {

            // Add an ADC and NPE value
            fCherenkovADCs[aCerHit->fMirrorNumber-1]->push_back(aCerHit->fADC);
            // (re) calculate the Number of photo electrons
            aCerHit->fNPE = ((Double_t) aCerHit->fADC) / ((Double_t)Get1PECalibration(aCerHit->fMirrorNumber));
            fCherenkovNPEs[aCerHit->fMirrorNumber-1]->push_back(aCerHit->fNPE);
            // Add an Aligned ADC value (alignment occurs in GasCherenkovCalculation1::ProcessEvent() )
            fCherenkovADCsAligned[aCerHit->fMirrorNumber-1]->push_back(aCerHit->fADCAlign);
            // Set the mirror values
            GetMirror(aCerHit->fMirrorNumber)->fSignal = aCerHit->fADCAlign;

         }

         if (aCerHit->fLevel == 1) {

            // Add a TDC value
            fCherenkovTDCs[aCerHit->fMirrorNumber-1]->push_back(aCerHit->fTDC);
            // Add an Aligned TDC value (alignment occurs in GasCherenkovCalculation1::ProcessEvent() )
            fCherenkovTDCsAligned[aCerHit->fMirrorNumber-1]->push_back(aCerHit->fTDCAlign);
            fMirrorTDCHit[aCerHit->fMirrorNumber-1] = true;
            // For every hit add quantities to histogram
            // \note this may repeat ADC values for multiple (bad) tdc hits
            //hADCWithoutCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);
            //hADCAlignedWithoutCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADCAlign);
            //hTDCWithoutCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDC);
            //hTDCAlignedWithoutCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDCAlign);

            // Caclulate cuts for each mirror.
            // Assuming for every tdc there is an adc hit.not always true due to coda readout ignoring some ADC values
            if (aCerHit->fNPE > cNPEMin && aCerHit->fNPE < cNPEMax) {
               cPassesNPE[aCerHit->fMirrorNumber-1] = true;
            }
            if (aCerHit->fADCAlign > cADCAlignMin && aCerHit->fADCAlign < cADCAlignMax) {
               cPassesADCAlign[aCerHit->fMirrorNumber-1] = true;
            }
            if (aCerHit->fTDCAlign > cTDCAlignMin && aCerHit->fTDCAlign < cTDCAlignMax) {
               cPassesTDCAlign[aCerHit->fMirrorNumber-1] = true;
            }
            if (aCerHit->fADC > cADCMin && aCerHit->fADC < cADCMax) {
               cPassesADC[aCerHit->fMirrorNumber-1] = true;

               // histograms
               //hADCWithADCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);
               //hADCAlignedWithADCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADCAlign);
               //hTDCWithADCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDC);
               //hTDCAlignedWithADCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDCAlign);
            }
            if (aCerHit->fTDC > cTDCMin && aCerHit->fTDC < cTDCMax) {
               cPassesTDC[aCerHit->fMirrorNumber-1] = true;
               // histograms
               //hADCWithTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);
               //hADCAlignedWithTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADCAlign);
               //hTDCWithTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDC);
               //hTDCAlignedWithTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDCAlign);
               // Accumulate timed-hit index numbers
               fTimedTDCHitNumbers->push_back(aCerHit->fHitNumber);
               // Set mirror values only if there is a timing hit
               GetMirror(aCerHit->fMirrorNumber)->fTiming = aCerHit->fTDCAlign;
               GetMirror(aCerHit->fMirrorNumber)->fNHits++;
               GetMirror(aCerHit->fMirrorNumber)->fIsHit = true;
               ((GasCherenkovEvent*)fEvent)->fNumberOfTimedTDCHits++;

            }
            if (cPassesADC[aCerHit->fMirrorNumber-1] && cPassesTDC[aCerHit->fMirrorNumber-1]) {
               //hADCWithADCandTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);
               //hADCAlignedWithADCandTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADCAlign);
               //hTDCWithADCandTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDC);
               //hTDCAlignedWithADCandTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDCAlign);
               ((GasCherenkovEvent*)fEvent)->fNumberOfTimedADCHits++;
            }

            if (aCerHit->fMirrorNumber <= 8) {
               // fill event display histogram
               fCerHits->Fill(fGeoCalc->iMirror(aCerHit->fMirrorNumber),
                              fGeoCalc->jMirror(aCerHit->fMirrorNumber));
               // fill event display for tdc hits with adc passing threshold
               if (cPassesNPE)
                  fCerHitsGood->Fill(fGeoCalc->iMirror(aCerHit->fMirrorNumber),
                                     fGeoCalc->jMirror(aCerHit->fMirrorNumber));
            } // end num <8
         } // end fLevel == 1

      } // end of cherenkov hits loop

      ((GasCherenkovEvent*)fEvent)->fNumberOfTimedTDCHits = fTimedTDCHitNumbers->size();


   } // fEvent

   if (fCurrentScaler != nullptr) {
      fNumberOfScalerEvents++;
      for (int i = 0; i < fNumberOfScalers; i++) {
         fAverageScaler[i] = (fAverageScaler[i] * ((Long64_t)(fNumberOfScalerEvents - 1)) + fCurrentScaler[i]) / ((Long64_t)fNumberOfScalerEvents);
      }
   }
   return(0);
}
//______________________________________________________________________________
Int_t GasCherenkovDetector::PostProcessAction() {
   //    PrintDetectorConfiguration();
   //    PrintCuts();
   return(0);
}
//______________________________________________________________________________
Int_t GasCherenkovDetector::CorrectEvent() {
   if (!fEvent) {
      std::cout << "Must set detector event address\n";
      return(-1);
   }
   return(0);
}
//______________________________________________________________________________
void GasCherenkovDetector::Set1PECalibrations(Int_t runnumber) {
   // The calibration coefficients below are from https://hallcweb.jlab.org/hclog/0812_archive/081220133641.html
   /*   for(int i =0;i<12;i++) {*/
   f1PECalibration[0] = 106.184;
   f1PECalibration[1] = 104.39;
   f1PECalibration[2] = 98.5437;
   f1PECalibration[3] = 100.831;
   f1PECalibration[4] = 99.8849;
   f1PECalibration[5] = 102.702;
   f1PECalibration[6] = 102.49;
   f1PECalibration[7] = 95.8557;
   f1PECalibration[8] = 0;
   f1PECalibration[9] = 0;
   f1PECalibration[10] = 0;
   f1PECalibration[11] = 0;

   //correct for lowered gains during perpendicular runs
   if (runnumber < 72900 && runnumber > 70000) {
      f1PECalibration[0] = f1PECalibration[0] * 0.95;
      f1PECalibration[1] = f1PECalibration[1] * 0.95;
      f1PECalibration[2] = f1PECalibration[2] * 0.55;
      f1PECalibration[3] = f1PECalibration[3] * 0.85;
      f1PECalibration[4] = f1PECalibration[4] * 0.85;
      f1PECalibration[5] = f1PECalibration[5] * 0.80;
      f1PECalibration[6] = f1PECalibration[6] * 0.60;
      f1PECalibration[7] = f1PECalibration[7] * 0.83;
      f1PECalibration[8] = 0;
      f1PECalibration[9] = 0;
      f1PECalibration[10] = 0;
      f1PECalibration[11] = 0;
   }
}
//______________________________________________________________________________
Int_t GasCherenkovDetector::GetADCAlignmentCalibrations(Int_t runnumber) {

   for (int i = 0; i < fNumberOfPedestals; i++) {
      fCherenkovAlignmentCoef[i] = 1700.0;
      fCherenkovSingleElectronWidth[i] = 500.0;
      fCherenkovDoubleElectronWidth[i] = 500.0;
   }
   InSANEDatabaseManager * dbManager = InSANEDatabaseManager::GetManager();

   TSQLServer * db = dbManager->GetMySQLConnection();

   TString SQLCOMMAND = "SELECT channel,npe_1_mean,npe_1_width,npe_2_width FROM cherenkov_performance where run_number=";

   if(!(InSANERunManager::GetRunManager()->IsSimulationRun())){
      if (InSANERunManager::GetRunManager()->IsParallelRun()) SQLCOMMAND += "-1";
      else if (InSANERunManager::GetRunManager()->IsPerpendicularRun()) SQLCOMMAND += "-2";
      else  SQLCOMMAND += runnumber;
   } else {
      SQLCOMMAND += "-3";
   }

   SQLCOMMAND += " ;";

   if(db) {
      TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
      TSQLRow * row;
      if(res) {
         while( (row = res->Next()) ) {
            int j = atoi(row->GetField(0))-1;
            if(j>=0 && j<8) fCherenkovAlignmentCoef[j] = atof(row->GetField(1));
            if(j>=0 && j<8) fCherenkovSingleElectronWidth[j] = atof(row->GetField(2));
            if(j>=0 && j<8) fCherenkovDoubleElectronWidth[j] = atof(row->GetField(3));
         }
      }
      dbManager->CloseConnections();
      return(0);
   } else {
      std::cout << " NO DB \n";
      return(-1);
   }
   dbManager->CloseConnections();
   return(0);

}
//______________________________________________________________________________
Int_t GasCherenkovDetector::ClearEvent() {

   /*     if(fEvent) ((GasCherenkovEvent*)fEvent)->ClearEvent("C");*/
   fTimedTDCHitNumbers->clear();
   fCherenkovHit = false;
   for (int i = 0; i < 12; i++) {
      GetMirror(i + 1)->Clear();
      fCherenkovADCs[i]->clear();
      fCherenkovNPEs[i]->clear();
      fCherenkovADCsAligned[i]->clear();
      fCherenkovTDCs[i]->clear();
      fCherenkovTDCsAligned[i]->clear();
      fCherenkovTimedTDCs[i]->clear();
      fCherenkovTimedTDCsAligned[i]->clear();
      fMirrorTDCHit[i] = false;
      cPassesADC[i] = false;
      cPassesNPE[i] = false;
      cPassesTDC[i] = false;
   }
   fCerHits->Reset();
   fCerHitsGood->Reset();
   return(0);
}
//______________________________________________________________________________
void GasCherenkovDetector::PrintCuts() const {
   using std::cout;
   cout << "___________________________________\n";
   cout << "c      Gas Cherenkov Cuts          \n";
   cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n";
   cout <<  Form("^  %6.0f  <  ADC  <  %6.0f     \n", cADCMin, cADCMax);
   cout <<  Form("^  %6.0f  <  NPE  <  %6.0f    \n", cNPEMin, cNPEMax);
   cout <<  Form("^  %6.0f  <  TDC  <  %6.0f    \n", cTDCMin, cNPEMax);
   cout <<  Form("^  %6.0f  <  TDC0 <  %6.0f    \n", cTDCAlignMin, cTDCMax);
   /*  cout <<  Form("^  %6.0f  <  ADC  <  %6.0f    \n",cADCMin,cPassesTDCAlign);*/
   cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n";
}
//______________________________________________________________________________
void GasCherenkovDetector::PrintEvent() const {
   InSANEDetector::PrintEvent(); // prints detector title, name, and event addr
   for (int i = 0; i < 8 ; i++) {
      std::cout << " # of ADC hits on pmt " << i << " : " << fCherenkovADCs[i]->size() << "\n";
      std::cout << " # of TDC hits on pmt " << i << " : " << fCherenkovTDCs[i]->size() << "\n";
      std::cout << " # of Timed TDC hits on pmt " << i << " : " << fCherenkovTimedTDCs[i]->size() << "\n";
   }
}
//______________________________________________________________________________
Bool_t GasCherenkovDetector::HasNeighboringHit(Int_t mirror) {
   if (mirror > 8 || mirror < 1) {
      printf("HasNeighboringHit called with bad mirror number, %d!!!\n", mirror);
      return(false);
   }
   for (int i = 1 ; i < 9; i++) {
      if (fGeoCalc->IsNeighboringMirror(mirror, i))
         if (cPassesTDC[i-1]) return(true);
   }
   return(false);
}
//______________________________________________________________________________

