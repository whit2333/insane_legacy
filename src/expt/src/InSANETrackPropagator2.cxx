// Whitney Armstrong's modified version of the TEveTrackPropagator
// Authors: Matevz Tadel & Alja Mrak-Tadel: 2006, 2007
/*************************************************************************
 * Copyright (C) 1995-2007, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#include "InSANETrackPropagator2.h"

namespace
{
   const Double_t kBMin     = 1e-6;
   const Double_t kPtMinSqr = 1e-20;
   const Double_t kAMin     = 1e-10;
   const Double_t kStepEps  = 1e-3;
}

//______________________________________________________________________________
InSANETrackPropagator2::Helix_t::Helix_t() :
   fCharge(0),
   fMaxAng(0.1), fMaxStep(10.f), fDelta(0.001),
   fPhi(0), fValid(kFALSE),
   fLam(-1), fR(-1), fPhiStep(-1), fSin(-1), fCos(-1),
   fRKStep(20.0),
   fPtMag(-1), fPlMag(-1), fLStep(-1)
{
   // Default constructor.
}

//______________________________________________________________________________
void InSANETrackPropagator2::Helix_t::UpdateCommon(const TVector3& p, const TVector3& b)
{
   // Common update code for helix and RK propagation.

   fB = b;

   // base vectors
   fE1 = b;
   fE1 *= (1.0/fE1.Mag());
   //fE1.Normalize();
   fPlMag = p.Dot(fE1);
   fPl    = fE1*fPlMag;

   fPt    = p - fPl;
   fPtMag = fPt.Mag();
   fE2    = fPt;
   //fE2.Normalize();
   fE2 *= (1.0/fE2.Mag());
}

//______________________________________________________________________________
void InSANETrackPropagator2::Helix_t::UpdateHelix(const TVector3& p, const TVector3& b,
                                               Bool_t full_update, Bool_t enforce_max_step)
{
   // Update helix parameters.

   UpdateCommon(p, b);

   // helix parameters
   //TMath::Cross(fE1.Arr(), fE2.Arr(), fE3.Arr());
   fE3 = fE1.Cross(fE2);
   //if (fCharge < 0) fE3.NegateXYZ();
   if (fCharge < 0) fE3 *= (-1.0);

   if (full_update)
   {
      using namespace TMath;
      Double_t a = fgkB2C * b.Mag() * Abs(fCharge);
      if (a > kAMin && fPtMag*fPtMag > kPtMinSqr)
      {
         fValid = kTRUE;

         fR   = Abs(fPtMag / a);
         fLam = fPlMag / fPtMag;

         // get phi step, compare fMaxAng with fDelta
         fPhiStep = fMaxAng * DegToRad();
         if (fR > fDelta)
         {
            Double_t ang  = 2.0 * ACos(1.0f - fDelta/fR);
            if (ang < fPhiStep)
               fPhiStep = ang;
         }

         // check max step size
         Double_t curr_step = fR * fPhiStep * Sqrt(1.0f + fLam*fLam);
         if (curr_step > fMaxStep || enforce_max_step)
            fPhiStep *= fMaxStep / curr_step;

         fLStep = fR * fPhiStep * fLam;
         fSin   = Sin(fPhiStep);
         fCos   = Cos(fPhiStep);
      }
      else
      {
         fValid = kFALSE;
      }
   }
}

//______________________________________________________________________________
void InSANETrackPropagator2::Helix_t::UpdateRK(const TVector3& p, const TVector3& b)
{
   // Update helix for stepper RungeKutta.

   UpdateCommon(p, b);

   if (fCharge)
   {
      fValid = kTRUE;

      // cached values for propagator
      fB = b;
      fPlMag = p.Dot(fB);
   }
   else
   {
      fValid = kFALSE;
   }
}

//______________________________________________________________________________
void InSANETrackPropagator2::Helix_t::Step(const TLorentzVector& v, const TVector3& p,
                                        TLorentzVector& vOut, TVector3& pOut)
{
   // Step helix for given momentum p from vertex v.

   vOut = v;
   
   if (fValid)
   {
      TVector3 d = fE2*(fR*fSin) + fE3*(fR*(1-fCos)) + fE1*fLStep;
      vOut    += TLorentzVector(d,TMath::Abs(fLStep));
      //vOut.fT += TMath::Abs(fLStep);

      pOut = fPl + fE2*(fPtMag*fCos) + fE3*(fPtMag*fSin);

      fPhi += fPhiStep;
   }
   else
   {
      // case: pT < kPtMinSqr or B < kBMin
      // might happen if field directon changes pT ~ 0 or B becomes zero
      vOut    += TLorentzVector(p * (fMaxStep / p.Mag()),fMaxStep);
      //vOut.fT += fMaxStep;
      pOut  = p;
   }
}


//==============================================================================
// InSANETrackPropagator2
//==============================================================================

//______________________________________________________________________________
//
// Holding structure for a number of track rendering parameters.
// Calculates path taking into account the parameters.
//
// NOTE: Magnetic field direction convention is inverted.
//
// This is decoupled from TEveTrack/TEveTrackList to allow sharing of the
// Propagator among several instances. Back references are kept so the tracks
// can be recreated when the parameters change.
//
// TEveTrackList has Get/Set methods for RnrStlye. TEveTrackEditor and
// TEveTrackListEditor provide editor access.
//
// Enum EProjTrackBreaking_e and member fProjTrackBreaking spec.Y() whether 2D
// projected tracks get broken into several segments when the projected space
// consists of separate domains (like Rho-Z). The track-breaking is enabled by
// default.
//

ClassImp(InSANETrackPropagator2);

Double_t             InSANETrackPropagator2::fgDefMagField = 0.5;
const Double_t       InSANETrackPropagator2::fgkB2C        = 0.299792458e-2;
//InSANETrackPropagator2  InSANETrackPropagator2::fgDefault;

Double_t             InSANETrackPropagator2::fgEditorMaxR  = 2000;
Double_t             InSANETrackPropagator2::fgEditorMaxZ  = 4000;

//______________________________________________________________________________
InSANETrackPropagator2::InSANETrackPropagator2(const char* n, const char* t,
      InSANEMagneticField *field, Bool_t own_field) :
   fStepper(kHelix),
   //fStepper(kRungeKutta),
   fMagFieldObj(field),
   fOwnMagFiledObj(own_field),
   fMaxR    (350),   fMaxZ    (500),
   fNMax    (4096),  fMaxOrbs (0.5),
   fV()
{
   // Default constructor.
   //std::cout << "InSANETrackPropagator2" << std::endl;

   fOrigin.SetXYZ(0.0,0.0,0.0);
   //initialize plane vectors
   fZXPlaneNormal.SetXYZ(0.0,1.0,0.0);
   fYZPlaneNormal.SetXYZ(1.0,0.0,0.0);
   fXYPlaneNormal.SetXYZ(0.0,0.0,1.0);

   fZXPlanePoint.SetXYZ(0.0,0.0,0.0);
   fYZPlanePoint.SetXYZ(0.0,0.0,0.0);
   fXYPlanePoint.SetXYZ(0.0,0.0,0.0);

   // Particle is electron by default
   fParticle = new TParticle();
   fParticle->SetPdgCode(11);
   fParticle->SetMomentum(-1.0*TMath::Sin(40.0*TMath::Pi()/180.0),0.0,-1.0*TMath::Cos(40.0*TMath::Pi()/180.0),TMath::Sqrt(0.000511*0.000511 + 1.0*1.0));
   fParticle->SetProductionVertex(335.0*TMath::Sin(40.0*TMath::Pi()/180.0),0.0,335.0*TMath::Cos(40.0*TMath::Pi()/180.0),/*t=*/0);

   /// Setup the magnetic field
   auto * Bf = new UVAOxfordMagneticField();//new TEveMagFieldConst(0., -10.0, 0.0);
   Bf->SetPolarizationAngle(TMath::Pi()*180.0/180.0);
   this->SetMagFieldObj(Bf,/*prop owns field*/false); // set the eve magnetic field

   if (fMagFieldObj == nullptr) {
      fMagFieldObj = new InSANEMagneticField();//TEveMagFieldConst(0., 0., fgDefMagField);
      fOwnMagFiledObj = kTRUE;
   }
}

//______________________________________________________________________________
InSANETrackPropagator2::~InSANETrackPropagator2()
{
   // Destructor.

   if (fOwnMagFiledObj)
   {
      delete fMagFieldObj;
   }
}

//______________________________________________________________________________
//void InSANETrackPropagator2::OnZeroRefCount()
//{
//   // Virtual from TEveRefBackPtr - track reference count has reached zero.
//
//   CheckReferenceCount("InSANETrackPropagator2::OnZeroRefCount ");
//}

//______________________________________________________________________________
//void InSANETrackPropagator2::CheckReferenceCount(const TEveException& eh)
//{
//   // Check reference count - virtual from TEveElement.
//   // Must also take into account references from TEveRefBackPtr.
//
//   if (fRefCount <= 0)
//   {
//      TEveElementList::CheckReferenceCount(eh);
//   }
//}

//______________________________________________________________________________
//void InSANETrackPropagator2::ElementChanged(Bool_t update_scenes, Bool_t redraw)
//{
//   // Element-change notification.
//   // Stamp all tracks as requiring display-list regeneration.
//   // Virtual from TEveElement.
//
//   TEveTrack* track;
//   RefMap_i i = fBackRefs.begin();
//   while (i != fBackRefs.end())
//   {
//      track = dynamic_cast<TEveTrack*>(i->first);
//      track->StampObjProps();
//      ++i;
//   }
//   TEveElementList::ElementChanged(update_scenes, redraw);
//}

//==============================================================================

//______________________________________________________________________________
void InSANETrackPropagator2::InitTrack(const TVector3 &v, Int_t charge)
{
   // Initialize internal data-members for given particle parameters.

   fV = TLorentzVector(v,0);
   fPoints.push_back(fV);

   // init helix
   fH.fPhi    = 0;
   fH.fCharge = charge;
}

//______________________________________________________________________________
//void InSANETrackPropagator2::InitTrack(const TVector3F& v, Int_t charge)
//{
//   // TVector3F wrapper.
//
//   TVector3 vd(v);
//   InitTrack(vd, charge);
//}

//______________________________________________________________________________
void InSANETrackPropagator2::ResetTrack()
{
   // Reset cache holding particle trajectory.

   fPoints.clear();

   // reset helix
   fH.fPhi = 0;
}

//______________________________________________________________________________
Int_t InSANETrackPropagator2::GetCurrentPoint() const
{
   // Get index of current point on track.

   return fPoints.size() - 1;
}

//______________________________________________________________________________
Double_t InSANETrackPropagator2::GetTrackLength(Int_t start_point, Int_t end_point) const
{
   // Calculate track length from start_point to end_point.
   // If end_point is less than 0, distance to the end is returned.

   if (end_point < 0) end_point = fPoints.size() - 1;

   Double_t sum = 0;
   for (Int_t i = start_point; i < end_point; ++i)
   {
      sum += (fPoints[i+1] - fPoints[i]).Mag();
   }
   return sum;
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::GoToVertex(TVector3& v, TVector3& p)
{
   // Propagate particle with momentum p to vertex v.

   Update(fV, p, kTRUE);

   if ((v-fV.Vect()).Mag() < kStepEps)
   {
      fPoints.push_back(TLorentzVector(v,0));
      return kTRUE;
   }

   return fH.fValid ? LoopToVertex(v, p) : LineToVertex(v);
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::GoToPlane(const TVector3& point, const TVector3& norm, TVector3& p)
{
   // Propagate particle with momentum p to line with start point s and vector r to the second point.

   Update(fV, p, kTRUE);

   if (!fH.fValid)
   {
      Warning("GoToPlane","fH.fValid == false ");
   //   TVector3 v;
   //   ClosestPointBetweenLines(s, r, fV.Vect(), p, v);
   //   LineToVertex(v);
      return kFALSE;
   }
   else
   {
      return LoopToPlane(point, norm, p);
   }
}
//______________________________________________________________________________
Bool_t InSANETrackPropagator2::GoToLineSegment(const TVector3& s, const TVector3& r, TVector3& p)
{
   // Propagate particle with momentum p to line with start point s and vector r to the second point.

   Update(fV, p, kTRUE);

   if (!fH.fValid)
   {
      TVector3 v;
      ClosestPointBetweenLines(s, r, fV.Vect(), p, v);
      LineToVertex(v);
      return kTRUE;
   }
   else
   {
      return LoopToLineSegment(s, r, p);
   }
}

//______________________________________________________________________________
//Bool_t InSANETrackPropagator2::GoToVertex(TVector3F& v, TEveVectorF& p)
//{
//   // TVector3F wrapper.
//
//   TVector3 vd(v), pd(p);
//   Bool_t result = GoToVertex(vd, pd);
//   v = vd; p = pd;
//   return result;
//}

//______________________________________________________________________________
//Bool_t InSANETrackPropagator2::GoToLineSegment(const TVector3F& s, const TEveVectorF& r, TEveVectorF& p)
//{
//   // TVector3F wrapper.
//
//   TVector3 sd(s), rd(r), pd(p);
//   Bool_t result = GoToLineSegment(sd, rd, pd);
//   p = pd;
//   return result;
//}

//______________________________________________________________________________
void InSANETrackPropagator2::GoToBounds(TVector3& p)
{
   // Propagate particle to bounds.
   // Return TRUE if hit bounds.

   Update(fV, p, kTRUE);

   fH.fValid ? LoopToBounds(p): LineToBounds(p);
}

//______________________________________________________________________________
//void InSANETrackPropagator2::GoToBounds(TVector3F& p)
//{
//   // TVector3F wrapper.
//
//   TVector3 pd(p);
//   GoToBounds(pd);
//   p = pd;
//}

//______________________________________________________________________________
void InSANETrackPropagator2::Update(const TLorentzVector& v, const TVector3& p,
                                 Bool_t full_update, Bool_t enforce_max_step)
{
   // Update helix / B-field projection state.

   if (fStepper == kHelix)
   {
      fH.UpdateHelix(p, fMagFieldObj->GetField(v.X(),v.Y(),v.Z()), !fMagFieldObj->IsConst() || full_update, enforce_max_step);
   }
   else
   {
      fH.UpdateRK(p, fMagFieldObj->GetField(v.Vect()));

      if (full_update)
      {
         using namespace TMath;

         Float_t a = fgkB2C * fMagFieldObj->GetMaxFieldMag() * Abs(fH.fCharge);
	 if (a > kAMin)
	 {
            fH.fR = p.Mag() / a;

            // get phi step, compare fDelta with MaxAng
            fH.fPhiStep = fH.fMaxAng * DegToRad();
            if (fH.fR > fH.fDelta )
            {
               Double_t ang  = 2.0 * ACos(1.0f - fH.fDelta/fH.fR);
               if (ang < fH.fPhiStep)
                  fH.fPhiStep = ang;
            }

            // check against maximum step-size
            fH.fRKStep = fH.fR * fH.fPhiStep * Sqrt(1 + fH.fLam*fH.fLam);
            if (fH.fRKStep > fH.fMaxStep || enforce_max_step)
            {
               fH.fPhiStep *= fH.fMaxStep / fH.fRKStep;
               fH.fRKStep   = fH.fMaxStep;
            }
	 }
	 else
	 {
            fH.fRKStep = fH.fMaxStep;
	 }
      }
   }
}

//______________________________________________________________________________
void InSANETrackPropagator2::Step(const TLorentzVector &v, const TVector3 &p, TLorentzVector &vOut, TVector3 &pOut)
{
   // Wrapper to step helix.

   if (fStepper == kHelix)
   {
      fH.Step(v, p, vOut, pOut);
   }
   else
   {
      Double_t vecRKIn[7];
      vecRKIn[0] = v.X();
      vecRKIn[1] = v.Y();
      vecRKIn[2] = v.Z();
      Double_t pm = p.Mag();
      Double_t nm = 1.0 / pm;
      vecRKIn[3] = p.X()*nm;
      vecRKIn[4] = p.Y()*nm;
      vecRKIn[5] = p.Z()*nm;
      vecRKIn[6] = p.Mag();

      Double_t vecRKOut[7];
      StepRungeKutta(fH.fRKStep, vecRKIn, vecRKOut);

      vOut.SetX(vecRKOut[0]);
      vOut.SetY( vecRKOut[1]);
      vOut.SetZ( vecRKOut[2]);
      vOut.SetT( v.T() + fH.fRKStep );
      pm = vecRKOut[6];
      pOut.SetX( vecRKOut[3]*pm );
      pOut.SetY( vecRKOut[4]*pm );
      pOut.SetZ( vecRKOut[5]*pm );
   }
}

//______________________________________________________________________________
void InSANETrackPropagator2::LoopToBounds(TVector3& p)
{
   // Propagate charged particle with momentum p to bounds.
   // It is expected that Update() with full-update was called before.

   const Double_t maxRsq = fMaxR*fMaxR;

   TLorentzVector currV(fV);
   TLorentzVector forwV(fV);
   TVector3  forwP (p);

   Int_t np = fPoints.size();
   Double_t maxPhi = fMaxOrbs*TMath::TwoPi();

   while (fH.fPhi < maxPhi && np<fNMax)
   {
      Step(currV, p, forwV, forwP);

      // cross R
      if (forwV.Perp2() > maxRsq)
      {
         Float_t t = (fMaxR - currV.Vect().Perp()) / (forwV.Vect().Perp() - currV.Vect().Perp());
         if (t < 0 || t > 1)
         {
            Warning("HelixToBounds", "In MaxR crossing expected t>=0 && t<=1: t=%f, r1=%f, r2=%f, MaxR=%f.",
                    t, currV.Vect().Perp(), forwV.Vect().Perp(), fMaxR);
            return;
         }
         TVector3 d(forwV.Vect());
         d -= currV.Vect();
         d *= t;
         d += currV.Vect();
         fPoints.push_back(TLorentzVector(d,0));
         return;
      }

      // cross Z
      else if (TMath::Abs(forwV.Z()) > fMaxZ)
      {
         Double_t t = (fMaxZ - TMath::Abs(currV.Z())) / TMath::Abs((forwV.Z() - currV.Z()));
         if (t < 0 || t > 1)
         {
            Warning("HelixToBounds", "In MaxZ crossing expected t>=0 && t<=1: t=%f, z1=%f, z2=%f, MaxZ=%f.",
                    t, currV.Z(), forwV.Z(), fMaxZ);
            return;
         }
         TVector3 d(forwV.Vect() -currV.Vect());
         d *= t;
         d += currV.Vect();
         fPoints.push_back(TLorentzVector(d,0));
         return;
      }

      currV = forwV;
      p     = forwP;
      Update(currV, p);

      fPoints.push_back(currV);
      ++np;
   }
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::LoopToVertex(TVector3& v, TVector3& p)
{
   // Propagate charged particle with momentum p to vertex v.
   // It is expected that Update() with full-update was called before.

   const Double_t maxRsq = fMaxR * fMaxR;

   TLorentzVector currV(fV);
   TLorentzVector forwV(fV);
   TVector3  forwP(p);

   Int_t first_point = fPoints.size();
   Int_t np          = first_point;

   Double_t prod0=0, prod1;

   do
   {
      Step(currV, p, forwV, forwP);
      Update(forwV, forwP);

      if (PointOverVertex(TLorentzVector(v,0), forwV, &prod1))
      {
         std::cout << "Point Over vertex\n";
         v.Print();
         break;
      }

      if (IsOutsideBounds(forwV.Vect(), maxRsq, fMaxZ))
      {
         fV = currV;
         return kFALSE;
      }

      fPoints.push_back(forwV);
      currV = forwV;
      p     = forwP;
      fMomentum = p;
      fVertex = currV.Vect();
      prod0 = prod1;
      ++np;
   } while (np < fNMax);

   // make the remaining fractional step
   if (np > first_point)
   {
      if ((v - currV.Vect()).Mag() > kStepEps)
      {
         Double_t step_frac = prod0 / (prod0 - prod1);
         if (step_frac > 0)
         {
            // Step for fraction of previous step size.
            // We pass 'enforce_max_step' flag to Update().
            Float_t orig_max_step = fH.fMaxStep;
            fH.fMaxStep = step_frac * (forwV.Vect() - currV.Vect()).Mag();
            Update(currV, p, kTRUE, kTRUE);
            Step(currV, p, forwV, forwP);
            p     = forwP;
            currV = forwV;
            fPoints.push_back(currV);
            ++np;
            fH.fMaxStep = orig_max_step;
         }

         // Distribute offset to desired crossing point over all segment.

         TVector3 off(v - currV.Vect());
         off *= 1.0f / currV.T();
         DistributeOffset(off,  first_point, np, p);
         fV = TLorentzVector(v,0);
         return kTRUE;
      }
   }

   fPoints.push_back(TLorentzVector(v,0));
   fV = TLorentzVector(v,0);
   return kTRUE;
}
//______________________________________________________________________________
Bool_t InSANETrackPropagator2::LoopToPlane(const TVector3& point, const TVector3& normal, TVector3& p)
{
   // Propagate charged particle with momentum p to plane defined with point and normal.
   // It is expected that Update() with full-update was called before. Returns kFALSE if hits bounds.
   
   const Double_t maxRsq = fMaxR * fMaxR;
   TLorentzVector currV(fV);
   TLorentzVector forwV(fV);
   TVector3  forwP(p);

   Int_t first_point = fPoints.size();
   Int_t np          = first_point;

   TVector3 forwC;
   TVector3 currC;
   Double_t p0; 
   Double_t p1;
   
   do
   {
      Step(currV, p, forwV, forwP);
      Update(forwV, forwP);

      // Is this needed? WRA
      //ClosestPointFromVertexToPlane(forwV.Vect(), point, normal, forwC);

      // check if forwV is on the other side of the plane.
      p0 = normal.Dot( currV.Vect() - point);
      p1 = normal.Dot( forwV.Vect() - point);
      if( TMath::Sign(1.0,p0) != TMath::Sign(1.0,p1) ) {
         break;
      }

      if (IsOutsideBounds(forwV.Vect(), maxRsq, fMaxZ))
      {
         fV = currV;
         return kFALSE;
      }

      //fPoints.push_back(forwV);
      fMomentum = forwP;
      currV = forwV;
      p     = forwP;
      currC = forwC;
      ++np;
   } while (np < fNMax);

   // Get closest point on segment relative to line with forw and currV points.
   TVector3 l    = currV.Vect();
   TVector3 ldir = (currV.Vect() - forwV.Vect() );
   ldir *= (1.0/ldir.Mag());
   TVector3 v;
   LinePlaneIntersectionPoint(l, ldir, point, normal, v);

   //v.Print();

   if (np > first_point)
   {
      if ((v - currV.Vect()).Mag() > kStepEps)
      {
         // now break the last step up into roughly 10 pieces by setting the max step size. 
         TVector3 last_step = forwV.Vect() - currV.Vect();
         TVector3 delta     = v - currV.Vect();
         Double_t  step_frac  = last_step.Dot(delta) / last_step.Mag2();
         //Double_t deltastep = delta.Mag()/20.0;
         if ( step_frac > 0)
         {
            // We pass 'enforce_max_step' flag to Update().
            Float_t orig_max_step = fH.fMaxStep;
            //fH.fMaxStep = step_frac * (forwV.Vect() - currV.Vect()).Mag();
            fH.fMaxStep = step_frac * (forwV.Vect() - currV.Vect()).Mag();
            
            do { 
               Update(currV, p, kTRUE, kTRUE);
               Step(currV, p, forwV, forwP);

               // check if forwV is on the other side of the plane.
               p0 = normal.Dot(currV.Vect() - point);
               p1 = normal.Dot(forwV.Vect() - point);
               if( TMath::Sign(1.0,p0) != TMath::Sign(1.0,p1) ) {
                  break;
               }

               p     = forwP;
               currV = forwV;
               //fPoints.push_back(currV);
               ++np;

            } while( np < fNMax );

            // Now treat the boundry crossing as a straight line.
            l     = currV.Vect();
            ldir  = (currV.Vect()-forwV.Vect() );
            ldir *= (1.0/ldir.Mag());
            LinePlaneIntersectionPoint(l, ldir, point, normal, v);
            forwV.SetX(v.X());
            forwV.SetY(v.Y());
            forwV.SetZ(v.Z());

            currV = forwV;
            //fPoints.push_back(currV);
            ++np;
            fH.fMaxStep = orig_max_step;
         }

         // Distribute offset to desired crossing point over all segment.
         //TVector3 off(v - currV.Vect());
         //off *= 1.0f / currV.T();
         //DistributeOffset(off, first_point, np, p);
         fV = currV;
         return kTRUE;
      }
   }

   fV = currV;
   //fPoints.push_back(fV);
   return kTRUE;
} 
//______________________________________________________________________________
Bool_t InSANETrackPropagator2::LoopToLineSegment(const TVector3& s, const TVector3& r, TVector3& p)
{
   // Propagate charged particle with momentum p to line segment with point s and vector r to the second point.
   // It is expected that Update() with full-update was called before. Returns kFALSE if hits bounds.

   const Double_t maxRsq = fMaxR * fMaxR;
   const Double_t rMagInv = 1./r.Mag();

   TLorentzVector currV(fV);
   TLorentzVector forwV(fV);
   TVector3  forwP(p);

   Int_t first_point = fPoints.size();
   Int_t np          = first_point;

   TVector3 forwC;
   TVector3 currC;
   do
   {
      Step(currV, p, forwV, forwP);
      Update(forwV, forwP);

      ClosestPointFromVertexToLineSegment(forwV.Vect(), s, r, rMagInv, forwC);

      // check forwV is over segment with orthogonal component of
      // momentum to vector r
      TVector3 b = r;
      b *= (1.0/b.Mag()); 
      //b.Normalize();
      Double_t    x = forwP.Dot(b);
      TVector3 pTPM = forwP - x*b;
      if (pTPM.Dot(forwC - forwV.Vect()) < 0)
      {
         break;
      }

      if (IsOutsideBounds(forwV.Vect(), maxRsq, fMaxZ))
      {
         fV = currV;
         return kFALSE;
      }

      fPoints.push_back(forwV);
      currV = forwV;
      p     = forwP;
      currC = forwC;
      ++np;
   } while (np < fNMax);

   // Get closest point on segment relative to line with forw and currV points.
   TVector3 v;
   ClosestPointBetweenLines(s, r, currV.Vect(), forwV.Vect() - currV.Vect(), v);

   // make the remaining fractional step
   if (np > first_point)
   {
      if ((v - currV.Vect()).Mag() > kStepEps)
      {
         TVector3 last_step = forwV.Vect() - currV.Vect();
         TVector3 delta     = v - currV.Vect();
         Double_t  step_frac  = last_step.Dot(delta) / last_step.Mag2();
         if (step_frac > 0)
         {
            // Step for fraction of previous step size.
            // We pass 'enforce_max_step' flag to Update().
            Float_t orig_max_step = fH.fMaxStep;
            fH.fMaxStep = step_frac * (forwV - currV).Mag();
            Update(currV, p, kTRUE, kTRUE);
            Step(currV, p, forwV, forwP);
            p     = forwP;
            currV = forwV;
            fPoints.push_back(currV);
            ++np;
            fH.fMaxStep = orig_max_step;
         }

         // Distribute offset to desired crossing point over all segment.

         TVector3 off(v - currV.Vect());
         off *= 1.0f / currV.T();
         DistributeOffset(off, first_point, np, p);
         fV = TLorentzVector(v,0);
         return kTRUE;
      }
   }

   fV = TLorentzVector(v,0);
   fPoints.push_back(fV);
   return kTRUE;
}

//______________________________________________________________________________
void InSANETrackPropagator2::DistributeOffset(const TVector3& off, Int_t first_point, Int_t np, TVector3& p)
{
   // Distribute offset between first and last point index and rotate
   // momentum.

   // Calculate the required momentum rotation.
   // lpd - last-points-delta
   TVector3 lpd0(fPoints[np-1].Vect());
   lpd0 -= fPoints[np-2].Vect();
   lpd0 *= (1.0/lpd0.Mag());
   //lpd0.Normalize();

   for (Int_t i = first_point; i < np; ++i)
   {
      fPoints[i] += TLorentzVector(off * fPoints[i].T(),fPoints[i].T());
   }

   TVector3 lpd1(fPoints[np-1].Vect());
   lpd1 -= fPoints[np-2].Vect();
   lpd1 *= (1.0/lpd1.Mag());
   //lpd1.Normalize();


   TEveVector v0(lpd0.X(),lpd0.Y(),lpd0.Z());
   TEveVector v1(lpd1.X(),lpd1.Y(),lpd1.Z());

   // Need to come back to this.....
   TEveTrans tt;
   tt.SetupFromToVec(v0, v1);

   tt.RotateIP(p);

   // TVector3 pb4(p);
   // printf("Rotating momentum: p0 = "); p.Dump();
   // printf("                   p1 = "); p.Dump();
   // printf("  n1=%f, n2=%f, dp = %f deg\n", pb4.Mag(), p.Mag(),
   //        TMath::RadToDeg()*TMath::ACos(p.Dot(pb4)/(pb4.Mag()*p.Mag())));
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::LineToVertex(TVector3& v)
{
   // Propagate neutral particle to vertex v.

   TLorentzVector currV(v,0);

   //currV.X() = v.X();
   //currV.Y() = v.Y();
   //currV.Z() = v.Z();
   fPoints.push_back(currV);

   fV = currV;
   //fV = v;
   return kTRUE;
}

//______________________________________________________________________________
void InSANETrackPropagator2::LineToBounds(TVector3& p)
{
   // Propagatate neutral particle with momentum p to bounds.

   Double_t tZ = 0, tR = 0, tB = 0;

   // time where particle intersect +/- fMaxZ
   if (p.Z() > 0)
      tZ = (fMaxZ - fV.Z()) / p.Z();
   else if (p.Z() < 0)
      tZ = - (fMaxZ + fV.Z()) / p.Z();

   // time where particle intersects cylinder
   Double_t a = p.X()*p.X() + p.Y()*p.Y();
   Double_t b = 2.0 * (fV.X()*p.X() + fV.Y()*p.Y());
   Double_t c = fV.X()*fV.X() + fV.Y()*fV.Y() - fMaxR*fMaxR;
   Double_t d = b*b - 4.0*a*c;
   if (d >= 0) {
      Double_t sqrtD = TMath::Sqrt(d);
      tR = (-b - sqrtD) / (2.0 * a);
      if (tR < 0) {
         tR = (-b + sqrtD) / (2.0 * a);
      }
      tB = tR < tZ ? tR : tZ; // compare the two times
   } else {
      tB = tZ;
   }
   TVector3 nv(fV.X() + p.X()*tB, fV.Y() + p.Y()*tB, fV.Z() + p.Z()*tB);
   LineToVertex(nv);
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::HelixIntersectPlane(const TVector3& p,
                                                const TVector3& point,
                                                const TVector3& normal,
                                                TVector3& itsect)
{
   // Intersect helix with a plane. Current position and argument p define
   // the helix.
   TVector3 pos(fV.Vect());
   TVector3 mom(p);
   if (fMagFieldObj->IsConst())
      fH.UpdateHelix(mom, fMagFieldObj->GetField(pos), kFALSE, kFALSE);

   TVector3 n(normal);
   TVector3 delta = pos - point;
   Double_t d = delta.Dot(n);
   if (d > 0) {
      n*=(-1.0); // Turn normal around so that we approach from negative side of the plane
      //n.NegateXYZ(); // Turn normal around so that we approach from negative side of the plane
      d = -d;
   }

   TLorentzVector forwV;
   TVector3  forwP;
   TLorentzVector pos4(pos,fV.T());
   while (kTRUE)
   {
      Update(pos4, mom);
      Step(pos4, mom, forwV , forwP);
      fVertex = forwV.Vect();
      fMomentum = forwP;
      Double_t new_d = (forwV.Vect() - point).Dot(n);
      if (new_d < d)
      {
         // We are going further away ... fail intersect.
         Warning("HelixIntersectPlane", "going away from the plane.");
         return kFALSE;
      }
      if (new_d > 0)
      {
         delta = forwV.Vect() - pos;
         itsect = pos + delta * (d / (d - new_d));
         return kTRUE;
      }
      pos4 = forwV;
      mom  = forwP;
   }
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::LineIntersectPlane(const TVector3& p,
                                               const TVector3& point,
                                               const TVector3& normal,
                                                     TVector3& itsect)
{
   // Intersect line with a plane. Current position and argument p define
   // the line.

   TVector3 pos(fV.X(), fV.Y(), fV.Z());
   TVector3 delta = pos - point;

   Double_t d = delta.Dot(normal);
   if (d == 0) {
      itsect = pos;
      return kTRUE;
   }

   Double_t t = (p.Dot(normal)) / d;
   if (t < 0) {
      return kFALSE;
   } else {
      itsect = pos + p*t;
      return kTRUE;
   }
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::IntersectPlane(const TVector3& p,
                                           const TVector3& point,
                                           const TVector3& normal,
                                                 TVector3& itsect)
{
   // Find intersection of currently propagated track with a plane.
   // Current track position is used as starting point.
   //
   // Args:
   //  p        - track momentum to use for extrapolation
   //  point    - a point on a plane
   //  normal   - normal of the plane
   //  itsect   - output, point of intersection
   // Returns:
   //  kFALSE if intersection can not be found, kTRUE otherwise.

   if (fH.fCharge && fMagFieldObj && p.Perp2() > kPtMinSqr)
      return HelixIntersectPlane(p, point, normal, itsect);
   else
      return LineIntersectPlane(p, point, normal, itsect);
}
//______________________________________________________________________________
void InSANETrackPropagator2::ClosestPointFromVertexToPlane(const TVector3& v,
                                                           const TVector3& point,
                                                           const TVector3& norm,
                                                           TVector3& c) {
   TVector3 w = v - point;
   Double_t D = w.Dot(norm);
   c = v - (D*norm);
}

//______________________________________________________________________________
void InSANETrackPropagator2::ClosestPointFromVertexToLineSegment(const TVector3& v,
                                                              const TVector3& s,
                                                              const TVector3& r,
                                                              Double_t rMagInv,
                                                              TVector3& c)
{
   // Get closest point from given vertex v to line segment defined with s and r.
   // Argument rMagInv is cached. rMagInv= 1./rMag()

   TVector3 dir = v - s;
   TVector3 b1  = r * rMagInv;

   // paralell distance
   Double_t dot     = dir.Dot(b1);
   TVector3 dirI = dot * b1;

   Double_t facX = dot * rMagInv;

   if (facX <= 0)
      c = s;
   else if (facX >= 1)
      c = s + r;
   else
      c = s + dirI;
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::ClosestPointBetweenLines(const TVector3& p0,
                                                     const TVector3& u,
                                                     const TVector3& q0,
                                                     const TVector3& v,
                                                     TVector3& out)
{
   // Get closest point on line defined with vector p0 and u.
   // Return false if the point is forced on the line segment.

   TVector3 w0 = p0 -q0;
   Double_t a = u.Mag2();
   Double_t b = u.Dot(v);
   Double_t c = v.Mag2();
   Double_t d = u.Dot(w0);
   Double_t e = v.Dot(w0);

   Double_t x = (b*e - c*d)/(a*c -b*b);
   Bool_t force = (x < 0 || x > 1);
   out = p0 + TMath::Range(0., 1., x) * u;
   return force;
}

//______________________________________________________________________________
//void InSANETrackPropagator2::FillPointSet(TEvePointSet* ps) const
//{
//   // Reset ps and populate it with points in propagation cache.
//
//   Int_t size = TMath::Min(fNMax, (Int_t)fPoints.size());
//   ps->Reset(size);
//   for (Int_t i = 0; i < size; ++i)
//   {
//      const TLorentzVector& v = fPoints[i];
//      ps->SetNextPoint(v.X(), v.Y(), v.Z());
//   }
//}

/******************************************************************************/

//______________________________________________________________________________
void InSANETrackPropagator2::RebuildTracks()
{
   // Rebuild all tracks using this render-style.
   /*
   TEveTrack* track;
   RefMap_i i = fBackRefs.begin();
   while (i != fBackRefs.end())
   {
      track = dynamic_cast<TEveTrack*>(i->first);
      track->MakeTrack();
      track->StampObjProps();
      ++i;
   }
   */
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetMagField(Double_t bX, Double_t bY, Double_t bZ)
{
   // Set constant magnetic field and rebuild tracks.

   //SetMagFieldObj(new TEveMagFieldConst(bX, bY, bZ));
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetMagFieldObj(InSANEMagneticField* field, Bool_t own_field)
{
   // Set constant magnetic field and rebuild tracks.

   if (fMagFieldObj && fOwnMagFiledObj) delete fMagFieldObj;

   fMagFieldObj    = field;
   fOwnMagFiledObj = own_field;

   RebuildTracks();
}

//______________________________________________________________________________
void InSANETrackPropagator2::PrintMagField(Double_t x, Double_t y, Double_t z) const
{
   if (fMagFieldObj) fMagFieldObj->PrintField(x, y, z);
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetMaxR(Double_t x)
{
   // Set maximum radius and rebuild tracks.

   fMaxR = x;
   RebuildTracks();
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetMaxZ(Double_t x)
{
   // Set maximum z and rebuild tracks.

   fMaxZ = x;
   RebuildTracks();
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetMaxOrbs(Double_t x)
{
   // Set maximum number of orbits and rebuild tracks.

   fMaxOrbs = x;
   RebuildTracks();
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetMinAng(Double_t x)
{
   // Set maximum step angle and rebuild tracks.
   // WARNING -- this method / variable was mis-named.

   Warning("SetMinAng", "This method was mis-named, use SetMaxAng() instead!");
   SetMaxAng(x);
}
//______________________________________________________________________________
Double_t InSANETrackPropagator2::GetMinAng() const
{
   // Get maximum step angle.
   // WARNING -- this method / variable was mis-named.

   Warning("GetMinAng", "This method was mis-named, use GetMaxAng() instead!");
   return GetMaxAng();
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetMaxAng(Double_t x)
{
   // Set maximum step angle and rebuild tracks.

   fH.fMaxAng = x;
   RebuildTracks();
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetMaxStep(Double_t x)
{
   // Set maximum step-size and rebuild tracks.

   fH.fMaxStep = x;
   RebuildTracks();
}

//______________________________________________________________________________
void InSANETrackPropagator2::SetDelta(Double_t x)
{
   // Set maximum error and rebuild tracks.

   fH.fDelta = x;
   RebuildTracks();
}

//______________________________________________________________________________
void InSANETrackPropagator2::StepRungeKutta(Double_t step,
                                         Double_t* vect, Double_t* vout)
{
  // Wrapper to step with method RungeKutta.

  ///	******************************************************************
  ///	*								 *
  ///	*  Runge-Kutta method for tracking a particle through a magnetic *
  ///	*  field. Uses Nystroem algorithm (See Handbook Nat. Bur. of	 *
  ///	*  Standards, procedure 25.5.20)				 *
  ///	*								 *
  ///	*  Input parameters						 *
  ///	*	CHARGE    Particle charge				 *
  ///	*	STEP	  Step size					 *
  ///	*	VECT	  Initial co-ords,direction cosines,momentum	 *
  ///	*  Output parameters						 *
  ///	*	VOUT	  Output co-ords,direction cosines,momentum	 *
  ///	*  User routine called  					 *
  ///	*	CALL GUFLD(X,F) 					 *
  ///	*								 *
  ///	*    ==>Called by : <USER>, GUSWIM				 *
  ///	*	Authors    R.Brun, M.Hansroul  *********		 *
  ///	*		   V.Perevoztchikov (CUT STEP implementation)	 *
  ///	*								 *
  ///	*								 *
  ///	******************************************************************

  Double_t h2, h4, f[4];
  Double_t /* xyzt[3], */ a, b, c, ph,ph2;
  Double_t secxs[4],secys[4],seczs[4],hxp[3];
  Double_t g1, g2, g3, g4, g5, g6, ang2, dxt, dyt, dzt;
  Double_t est, at, bt, ct, cba;
  Double_t f1, f2, f3, f4, rho, tet, hnorm, hp, rho1, sint, cost;

  Double_t x;
  Double_t y;
  Double_t z;

  Double_t xt;
  Double_t yt;
  Double_t zt;

  // const Int_t maxit = 1992;
  const Int_t maxit  = 500;
  const Int_t maxcut = 11;

  const Double_t hmin   = 1e-4; // !!! MT ADD,  should be member
  const Double_t kdlt   = 1e-3; // !!! MT CHANGE from 1e-4, should be member
  const Double_t kdlt32 = kdlt/32.;
  const Double_t kthird = 1./3.;
  const Double_t khalf  = 0.5;
  const Double_t kec    = 2.9979251e-3;

  const Double_t kpisqua = 9.86960440109;
  const Int_t kix  = 0;
  const Int_t kiy  = 1;
  const Int_t kiz  = 2;
  const Int_t kipx = 3;
  const Int_t kipy = 4;
  const Int_t kipz = 5;

  // *.
  // *.    ------------------------------------------------------------------
  // *.
  // *             this constant is for units cm,gev/c and kgauss
  // *
  Int_t iter = 0;
  Int_t ncut = 0;
  for(Int_t j = 0; j < 7; j++)
    vout[j] = vect[j];

  Double_t pinv   = kec * fH.fCharge / vect[6];
  Double_t tl     = 0.;
  Double_t h      = step;
  Double_t rest;

  do {
    rest  = step - tl;
    if (TMath::Abs(h) > TMath::Abs(rest))
       h = rest;

    f[0] = -fH.fB.X();
    f[1] = -fH.fB.Y();
    f[2] = -fH.fB.Z();

    // * start of integration
    x      = vout[0];
    y      = vout[1];
    z      = vout[2];
    a      = vout[3];
    b      = vout[4];
    c      = vout[5];

    h2     = khalf * h;
    h4     = khalf * h2;
    ph     = pinv * h;
    ph2    = khalf * ph;
    secxs[0] = (b * f[2] - c * f[1]) * ph2;
    secys[0] = (c * f[0] - a * f[2]) * ph2;
    seczs[0] = (a * f[1] - b * f[0]) * ph2;
    ang2 = (secxs[0]*secxs[0] + secys[0]*secys[0] + seczs[0]*seczs[0]);
    if (ang2 > kpisqua) break;

    dxt    = h2 * a + h4 * secxs[0];
    dyt    = h2 * b + h4 * secys[0];
    dzt    = h2 * c + h4 * seczs[0];
    xt     = x + dxt;
    yt     = y + dyt;
    zt     = z + dzt;

    // * second intermediate point
    est = TMath::Abs(dxt) + TMath::Abs(dyt) + TMath::Abs(dzt);
    if (est > h) {
      if (ncut++ > maxcut) break;
      h *= khalf;
      continue;
    }

    // xyzt[0] = xt;
    // xyzt[1] = yt;
    // xyzt[2] = zt;

    fH.fB = fMagFieldObj->GetField(xt, yt, zt);
    f[0] = -fH.fB.X();
    f[1] = -fH.fB.Y();
    f[2] = -fH.fB.Z();

    at     = a + secxs[0];
    bt     = b + secys[0];
    ct     = c + seczs[0];

    secxs[1] = (bt * f[2] - ct * f[1]) * ph2;
    secys[1] = (ct * f[0] - at * f[2]) * ph2;
    seczs[1] = (at * f[1] - bt * f[0]) * ph2;
    at     = a + secxs[1];
    bt     = b + secys[1];
    ct     = c + seczs[1];
    secxs[2] = (bt * f[2] - ct * f[1]) * ph2;
    secys[2] = (ct * f[0] - at * f[2]) * ph2;
    seczs[2] = (at * f[1] - bt * f[0]) * ph2;
    dxt    = h * (a + secxs[2]);
    dyt    = h * (b + secys[2]);
    dzt    = h * (c + seczs[2]);
    xt     = x + dxt;
    yt     = y + dyt;
    zt     = z + dzt;
    at     = a + 2.*secxs[2];
    bt     = b + 2.*secys[2];
    ct     = c + 2.*seczs[2];

    est = TMath::Abs(dxt)+TMath::Abs(dyt)+TMath::Abs(dzt);
    if (est > 2.*TMath::Abs(h)) {
      if (ncut++ > maxcut) break;
      h *= khalf;
      continue;
    }

    // xyzt[0] = xt;
    // xyzt[1] = yt;
    // xyzt[2] = zt;

    fH.fB = fMagFieldObj->GetField(xt, yt, zt);
    f[0] = -fH.fB.X();
    f[1] = -fH.fB.Y();
    f[2] = -fH.fB.Z();

    z      = z + (c + (seczs[0] + seczs[1] + seczs[2]) * kthird) * h;
    y      = y + (b + (secys[0] + secys[1] + secys[2]) * kthird) * h;
    x      = x + (a + (secxs[0] + secxs[1] + secxs[2]) * kthird) * h;

    secxs[3] = (bt*f[2] - ct*f[1])* ph2;
    secys[3] = (ct*f[0] - at*f[2])* ph2;
    seczs[3] = (at*f[1] - bt*f[0])* ph2;
    a      = a+(secxs[0]+secxs[3]+2. * (secxs[1]+secxs[2])) * kthird;
    b      = b+(secys[0]+secys[3]+2. * (secys[1]+secys[2])) * kthird;
    c      = c+(seczs[0]+seczs[3]+2. * (seczs[1]+seczs[2])) * kthird;

    est    = TMath::Abs(secxs[0]+secxs[3] - (secxs[1]+secxs[2]))
      + TMath::Abs(secys[0]+secys[3] - (secys[1]+secys[2]))
      + TMath::Abs(seczs[0]+seczs[3] - (seczs[1]+seczs[2]));

    if (est > kdlt && TMath::Abs(h) > hmin) {
      if (ncut++ > maxcut) break;
      h *= khalf;
      continue;
    }

    ncut = 0;
    // * if too many iterations, go to helix
    if (iter++ > maxit) break;

    tl += h;
    if (est < kdlt32)
      h *= 2.;
    cba    = 1./ TMath::Sqrt(a*a + b*b + c*c);
    vout[0] = x;
    vout[1] = y;
    vout[2] = z;
    vout[3] = cba*a;
    vout[4] = cba*b;
    vout[5] = cba*c;
    rest = step - tl;
    if (step < 0.) rest = -rest;
    if (rest < 1.e-5*TMath::Abs(step))
    {
       Float_t dot = (vout[3]*vect[3] + vout[4]*vect[4] + vout[5]*vect[5]);
       fH.fPhi += TMath::ACos(dot);
       return;
    }

  } while(1);

  // angle too big, use helix

  f1  = f[0];
  f2  = f[1];
  f3  = f[2];
  f4  = TMath::Sqrt(f1*f1+f2*f2+f3*f3);
  rho = -f4*pinv;
  tet = rho * step;

  hnorm = 1./f4;
  f1 = f1*hnorm;
  f2 = f2*hnorm;
  f3 = f3*hnorm;

  hxp[0] = f2*vect[kipz] - f3*vect[kipy];
  hxp[1] = f3*vect[kipx] - f1*vect[kipz];
  hxp[2] = f1*vect[kipy] - f2*vect[kipx];

  hp = f1*vect[kipx] + f2*vect[kipy] + f3*vect[kipz];

  rho1 = 1./rho;
  sint = TMath::Sin(tet);
  cost = 2.*TMath::Sin(khalf*tet)*TMath::Sin(khalf*tet);

  g1 = sint*rho1;
  g2 = cost*rho1;
  g3 = (tet-sint) * hp*rho1;
  g4 = -cost;
  g5 = sint;
  g6 = cost * hp;

  vout[kix] = vect[kix] + g1*vect[kipx] + g2*hxp[0] + g3*f1;
  vout[kiy] = vect[kiy] + g1*vect[kipy] + g2*hxp[1] + g3*f2;
  vout[kiz] = vect[kiz] + g1*vect[kipz] + g2*hxp[2] + g3*f3;

  vout[kipx] = vect[kipx] + g4*vect[kipx] + g5*hxp[0] + g6*f1;
  vout[kipy] = vect[kipy] + g4*vect[kipy] + g5*hxp[1] + g6*f2;
  vout[kipz] = vect[kipz] + g4*vect[kipz] + g5*hxp[2] + g6*f3;

  fH.fPhi += tet;
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::IsOutsideBounds(const TVector3& point,
      Double_t           maxRsqr,
      Double_t           maxZ)
{
   // Return true if point% is outside of cylindrical bounds detrmined by
   // square radius and z.

   return TMath::Abs(point.Z()) > maxZ ||
      point.X()*point.X() + point.Y()*point.Y() > maxRsqr;
}

//______________________________________________________________________________
Bool_t InSANETrackPropagator2::PointOverVertex(const TLorentzVector &v0,
      const TLorentzVector &v,
      Double_t           *p)
{
   static const Double_t kMinPl = 1e-5;

   TVector3 dv  = v0.Vect() - v.Vect(); // dv.Sub(v0, v);

   Double_t dotV;

   if (TMath::Abs(fH.fPlMag) > kMinPl)
   {
      // Use longitudinal momentum to determine crossing point.
      // Works ok for spiraling helices, also for loopers.

      dotV = fH.fE1.Dot(dv);
      if (fH.fPlMag < 0)
         dotV = -dotV;
   }
   else
   {
      // Use full momentum, which is pT, under this conditions.

      dotV = fH.fE2.Dot(dv);
   }

   if (p)
      *p = dotV;

   return dotV < 0;
}

