#include "InSANEElectronBeam.h"

ClassImp(InSANEElectronBeam)

//______________________________________________________________________________
InSANEElectronBeam::InSANEElectronBeam(const char * name, const char * title):InSANEApparatus(name,title) {
      fEnergy = 0.0;
      fCurrent = 0.0;
      fNPass = 0;
//       InSANERunManager * runman = InSANERunManager::GetRunManager();
//       fEnergy = runman->GetCurrentRun()->fBeamEnergy;
//       fCurrent = runman->GetCurrentRun()->fAverageBeamCurrent;
//       fNPass = runman->GetCurrentRun()->fBeamPassNumber;

   }
//______________________________________________________________________________
InSANEElectronBeam::~InSANEElectronBeam() {
}
//______________________________________________________________________________

void InSANEElectronBeam::Print(const Option_t * opt) const {
   InSANEApparatus::Print(opt);

}
//______________________________________________________________________________

ClassImp(HallCPolarizedElectronBeam)

//______________________________________________________________________________
HallCPolarizedElectronBeam::HallCPolarizedElectronBeam(const char * name, const char * title):InSANEElectronBeam(name,title) {
   fPolarization = 0.0;
   fWienAngle = 0.0;
   fHalfwavePlate = 0;
   fQuantumEfficiency = 0.0;
}
//______________________________________________________________________________
HallCPolarizedElectronBeam::~HallCPolarizedElectronBeam() {
}
//______________________________________________________________________________
Double_t HallCPolarizedElectronBeam::GetPolarization() {
   double result = 0.0;
   //InSANERunManager * runman = InSANERunManager::GetRunManager();
   //if( runman->GetCurrentRun() ) {
      //fWienAngle = runman->GetCurrentRun()->fWienAngle;
      /*      fQuantumEfficiency = runman->GetCurrentRun()->fQuantumEfficiency;*/
      //fHalfwavePlate = runman->GetCurrentRun()->fHalfWavePlate;
      sane_pol_(&fEnergy, &fWienAngle, &fQuantumEfficiency, &fNPass, &fHalfwavePlate, &result);
      fPolarization = result;
   //} else { std::cout << "ERROR NO RUN SET. \n";}
   return(result);
}
//______________________________________________________________________________
void HallCPolarizedElectronBeam::Print(const Option_t * opt) const
{
   InSANEElectronBeam::Print();

}

//______________________________________________________________________________

