#include  "GasCherenkovEvent.h"
#include  <TClonesArray.h>
#include  "GasCherenkovHit.h"
#include "InSANERunManager.h"

ClassImp(GasCherenkovEvent)
//___________________________________________________________________

TClonesArray * GasCherenkovEvent::fgGasCherenkovHits = nullptr;
TClonesArray * GasCherenkovEvent::fgGasCherenkovTDCHits = nullptr;
TClonesArray * GasCherenkovEvent::fgGasCherenkovADCHits = nullptr;

//___________________________________________________________________

GasCherenkovEvent::GasCherenkovEvent()
{

   if (InSANERunManager::GetRunManager()->fVerbosity > 3) printf("  + new GasCherenkovEvent\n");

   fNumberOfHits = 0;
   fNumberOfTDCHits = 0;
   fNumberOfADCHits = 0;
   fNumberOfTimedADCHits = 0;
   fNumberOfTimedTDCHits = 0;

   if (!fgGasCherenkovHits) {
      fgGasCherenkovHits = new TClonesArray("GasCherenkovHit", 12);
      /*    if(InSANERunManager::GetRunManager()->fVerbosity > 0) printf("  - Creating Static Global Variable fgGasCherenkovHits \n");*/
   }
   fGasCherenkovHits = fgGasCherenkovHits;

   if (!fgGasCherenkovTDCHits) {
      fgGasCherenkovTDCHits = new TClonesArray("GasCherenkovHit", 12);
      /*    if(InSANERunManager::GetRunManager()->fVerbosity > 0) printf("  - Creating Static Global Variable fgGasCherenkovHits \n");*/
   }
   fGasCherenkovTDCHits = fgGasCherenkovTDCHits;

   if (!fgGasCherenkovADCHits) {
      fgGasCherenkovADCHits = new TClonesArray("GasCherenkovHit", 12);
   }
   fGasCherenkovADCHits = fgGasCherenkovADCHits;

}
//___________________________________________________________________

GasCherenkovEvent::~GasCherenkovEvent()
{
   ClearEvent();
   /*   if(fGasCherenkovHits) delete fGasCherenkovHits;
      fgGasCherenkovHits = 0;
      fGasCherenkovHits = 0;
      if(fGasCherenkovTDCHits) delete fGasCherenkovTDCHits;
      fgGasCherenkovTDCHits=0;
      fGasCherenkovTDCHits=0;
   */
//    if(InSANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " - deleted GasCherenkovEvent   " << std::endl;

//    Clear();
//    if (fH == fgHist) fgHist = 0;
//    delete fH; fH = 0;
//    delete fHighPt; fHighPt = 0;
//    delete fMuons;  fMuons = 0;
//    delete [] fClosestDistance;
//    if (fEventName) delete [] fEventName;
}
//___________________________________________________________________

Int_t GasCherenkovEvent::AllocateHitArray()
{
   if (!fgGasCherenkovHits) {
      fgGasCherenkovHits = new TClonesArray("GasCherenkovHit", 1);
   }
   fGasCherenkovHits = fgGasCherenkovHits;

   if (!fgGasCherenkovTDCHits) {
      fgGasCherenkovTDCHits = new TClonesArray("GasCherenkovHit", 1);
   }
   fGasCherenkovTDCHits = fgGasCherenkovTDCHits;

   if (!fgGasCherenkovADCHits) {
      fgGasCherenkovADCHits = new TClonesArray("GasCherenkovHit", 1);
   }
   fGasCherenkovADCHits = fgGasCherenkovADCHits;


   /*else*/
   return(0);
}
//___________________________________________________________________

void GasCherenkovEvent::ClearEvent(const char * opt)
{
   InSANEDetectorEvent::ClearEvent(opt);

   if (fGasCherenkovHits)if(fGasCherenkovHits->GetEntries() > 40 ) {
       std::cout << " calling  fGasCherenkovHits->ExpandCreate(10); " << " had " << fGasCherenkovHits->GetEntries() << " entries. \n";
       fGasCherenkovHits->ExpandCreate(10);
   }
   if (fGasCherenkovHits)     fGasCherenkovHits->Clear(opt);

   if (fGasCherenkovTDCHits)if(fGasCherenkovTDCHits->GetEntries() > 40 ) {
       std::cout << " calling  fGasCherenkovTDCHits->ExpandCreate(10); " << " had " << fGasCherenkovTDCHits->GetEntries() << " entries. \n";
       fGasCherenkovTDCHits->ExpandCreate(10);
   }
   if (fGasCherenkovTDCHits)  fGasCherenkovTDCHits->Clear(opt);

   if (fGasCherenkovADCHits) {
      if (fGasCherenkovADCHits)if(fGasCherenkovADCHits->GetEntries() > 20 ) {
         std::cout << " calling  fGasCherenkovADCHits->ExpandCreate(10); " << " had " << fGasCherenkovADCHits->GetEntries() << " entries. \n";
         fGasCherenkovADCHits->ExpandCreate(10);
      }
      fGasCherenkovADCHits->Clear(opt);
   }
   fNumberOfHits = 0;
   fNumberOfTDCHits = 0;
   fNumberOfADCHits = 0;
   fNumberOfTimedADCHits = 0;
   fNumberOfTimedTDCHits = 0;
   for (int i = 0; i < 12; i++) fNPESums[i] = 0.0;

}
//___________________________________________________________________






