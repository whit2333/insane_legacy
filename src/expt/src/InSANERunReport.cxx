#include "InSANERunReport.h"


ClassImp(InSANERunReportDatum)

//______________________________________________________________________________
InSANERunReportDatum::InSANERunReportDatum(const char * name , const char * val, Bool_t isStr, Bool_t update) {
   fName = name;
   fValue = val;
   isUpdated = update;
   isString = isStr;
}
//______________________________________________________________________________
InSANERunReportDatum::~InSANERunReportDatum(){
}
//______________________________________________________________________________
Int_t InSANERunReportDatum::GatherInfo(TString * sql) {
   InSANEDatabaseManager * DBM = InSANEDatabaseManager::GetManager();
   TSQLServer * db = DBM->GetMySQLConnection();
   if(db) {
      TSQLResult *   res = db->Query(sql->Data()) ;
      TSQLRow * row;
      if(res) row = res->Next();
      if(row) {
         fValue = row->GetField(0);
         //std::cout << " Found value of " << fValue << std::endl;
         DBM->CloseConnections();
         return(0);
      } else {
         return(-1);
      }
   }
   DBM->CloseConnections();
   return(0);
}
//______________________________________________________________________________
Int_t InSANERunReportDatum::GatherInfoSQLite(TString * sql) {

      Warning("GatherInfoSQLite","SQLite3 no longer supported.");
      InSANEDatabaseManager * DBM = InSANEDatabaseManager::GetManager();

      //char *       zErrMsg   = 0;
      //int          rc        = 0;
      //rc = sqlite3_exec(DBM->GetSQLite3Connection(), sql->Data(),  InSANERunReportDatum::GatherInfo_callback, this, &zErrMsg);
      //if (rc != SQLITE_OK) {
      //   std::cerr << "SQL error: " << zErrMsg << std::endl;
      //   sqlite3_free(zErrMsg);
      //}

      TSQLServer * db = DBM->GetMySQLConnection();
      if(db) {
         TSQLResult *   res = db->Query(sql->Data()) ;
         TSQLRow * row;
         if(res) row = res->Next();
         if(row) {
            fValue = row->GetField(0);
            std::cout << " Found value of " << fValue << std::endl;
            DBM->CloseConnections();
            return(0);
         } else {
            return(-1);
         }
         DBM->CloseConnections();
      }
      return(0);
   }
//______________________________________________________________________________



ClassImp(InSANERunReport)

//______________________________________________________________________________
InSANERunReport::InSANERunReport(InSANERun * aRun) {
   fReportData = new std::vector<InSANERunReportDatum*>;

   if (!aRun) {
      std::cout << "x- Error: null InSANERun given to InSANERunReport constructor!\n";
   } else {
      SetRun(aRun);

   }
}
//______________________________________________________________________________
InSANERunReport::~InSANERunReport()
{
   if(fReportData) fReportData->clear();
   if(fReportData) delete fReportData;
}
//______________________________________________________________________________
void InSANERunReport::SetRun(InSANERun * aRun)
{
   fRun = aRun;
}
//______________________________________________________________________________
void InSANERunReport::Print(Option_t *) const {
   std::cout << "========== Run Report ========== \n";
   for (unsigned int i = 0; i < fReportData->size() ; i++) {
      std::cout << fReportData->at(i)->fName.Data() << " = " <<
                fReportData->at(i)->fValue.Data() << "\n";
   }
}
//______________________________________________________________________________
Int_t InSANERunReport::RetrieveRunInfo()
{
   /// - Updates each datum
   InSANEDatabaseManager * dbManager = InSANEDatabaseManager::GetManager();

   /// Build up SQL query
   TString SQLCOMMAND("SELECT ");
   for (unsigned int i = 0; i < fReportData->size() ; i++) {
      SQLCOMMAND += " ";
      SQLCOMMAND += fReportData->at(i)->fName.Data();
      if (i + 1 != fReportData->size())  SQLCOMMAND += ", ";
      else SQLCOMMAND += " ";
   }
   SQLCOMMAND += " FROM run_info WHERE Run_number=";
   SQLCOMMAND += fRun->fRunNumber;
   SQLCOMMAND += " ;";
   /*   std::cout << " the comm on of interest is : " << SQLCOMMAND.Data() << "\n";*/
   fSelectString = SQLCOMMAND;

   if (dbManager->GetDBType() == InSANEDatabaseManager::kSQLite3 || dbManager->GetDBType() == InSANEDatabaseManager::kBoth) {
      Error("RetrieveRunInfo","Sqlite3 no longer supported");
      //char *zErrMsg = 0;
      //int rc = sqlite3_exec(dbManager->GetSQLite3Connection(),
      //      SQLCOMMAND.Data(),
      //      InSANERunReport::RetrieveRunInfo_callback,
      //      this,
      //      &zErrMsg);
      //if (rc != SQLITE_OK) {
      //   std::cerr << "SQLite error: " << zErrMsg << std::endl;
      //   sqlite3_free(zErrMsg);
      //}
   }

   if (dbManager->GetDBType() == InSANEDatabaseManager::kMySQL || dbManager->GetDBType() == InSANEDatabaseManager::kBoth) {
      TSQLResult * res;
      TSQLRow * row;
      res = dbManager->GetMySQLConnection()->Query(fSelectString.Data());
      int nrows = res->GetRowCount();
      int nfields = res->GetFieldCount();
      /// Loop over database query result setting the values.
      /// note you cannot have an error in the SQL command otherwise it will crash
      /// \todo Add method that generates an error free query, checking if datum exists
      //if(nrows !=1 ) std::cout << "Number of rows, " << nrows << ", unexpected!\n";
      for (int i = 0; i < nrows; i++)
      {
         row = res->Next();
         for (int j = 0; j < nfields; j++)
         {
            /*         std::cout << TString(row->GetField(j) ) << "\n";*/
            /// \todo here we need a test for NULL in MYSQL... or if the value is not legit
            /*         if(fReportData->at(j)->fValue != row->GetField(j) ) fReportData->at(j)->isUpdated=true;*/
            fReportData->at(j)->fValue = row->GetField(j);
         }
         delete row;
      }
      delete res;
   }

   /// Executes GatherRunInfo.cxx which grabs data from many databases

   Int_t scriptExitCode = gROOT->ProcessLine(".x GatherRunInfo.cxx");

   //UpdateRunDatabase();

   dbManager->CloseConnections();

   return(scriptExitCode);
}
//______________________________________________________________________________

void InSANERunReport::UpdateRun() {
   ///\todo Add Script? isn't dynamic since we are making explicit references
   // Run date, time and GetValueOf("start_date")DAQ settings
   if( strcmp(GetValueOf("start_date"),"0" ) ) { 
      fRun->fStartDatetime.Set(Form("%s %s", GetValueOf("start_date"), GetValueOf("start_time"))) ;
   } else {
      fRun->fStartDatetime = TDatime();
   }
   if( strcmp(GetValueOf("end_date"),"0") ) {
      fRun->fEndDatetime.Set(Form("%s %s", GetValueOf("end_date"), GetValueOf("end_time")));
   } else {
      fRun->fEndDatetime = TDatime();
   }
   //std::cout << " testing datetime \n"
   //   << Form("%s %s",GetValueOf("start_date"),GetValueOf("start_time")) << "\n"
   //   << Form("%s %s",GetValueOf("end_date"),GetValueOf("end_time")) << "\n";
   fRun->fPreScale[0] = atoi(GetValueOf("PS1"));
   fRun->fPreScale[1] = atoi(GetValueOf("PS2"));
   fRun->fPreScale[2] = atoi(GetValueOf("PS3"));
   fRun->fPreScale[3] = atoi(GetValueOf("PS4"));
   fRun->fPreScale[4] = atoi(GetValueOf("PS5"));
   fRun->fPreScale[5] = atoi(GetValueOf("PS6"));

   fRun->fIronPlate         = atoi(GetValueOf("iron_plate"));
   // Beam settings
   fRun->fAverageBeamCurrent = atof(GetValueOf("beam_current"));
   fRun->fBeamEnergy         = atof(GetValueOf("beam_energy"));
   fRun->fWienAngle          = atof(GetValueOf("wien_angle"));
   fRun->fQuantumEfficiency  = atof(GetValueOf("pol_source_qe"));
   fRun->fBeamPassNumber     = atoi(GetValueOf("beam_pass"));
   fRun->fHalfWavePlate      = atoi(GetValueOf("halfwave_plate"));
   // Call fortran subroutine
   sane_pol_(&fRun->fBeamEnergy, &fRun->fWienAngle, &fRun->fQuantumEfficiency, &fRun->fBeamPassNumber, &fRun->fHalfWavePlate, &fRun->fBeamPolarization);
   //std:: cout << "sane_pol_ result : " << fRun->fBeamPolarization << std::endl; 
   //fRun->fBeamPolarization *= -1.0;

   // Analysis
   fRun->fAnalysisPass = atoi(GetValueOf("analysis_pass"));
   fRun->fDeadTime     = atof(GetValueOf("dead_time"));

   // Target
   TString tgttype  = GetValueOf("target_type");
   fRun->fTarget = InSANERun::kUNKNOWN;
   for (int k = 0; k < 6; k++) {
      if( tgttype.Contains(InSANERun::kgSANETargetNames[k],TString::kIgnoreCase) )
         fRun->fTarget = InSANERun::kgSANETargets[k];
   }

   TString tgtsign  = GetValueOf("target_pol_sign");
   fRun->fTargetPolarizationSign = InSANERun::kUNPOLARIZED;
   for (int k = 0; k < 3; k++) {
      if( tgtsign.Contains(InSANERun::kgSANETargetSignNames[k],TString::kIgnoreCase) )
         fRun->fTargetPolarizationSign = InSANERun::kgSANETargetSign[k];
   }

   TString tgtcup  = GetValueOf("target_cup");
   fRun->fTargetCup = InSANERun::kNONE;
   for (int k = 0; k < 4; k++) {
      if( tgtcup.Contains(InSANERun::kgSANETargetCupNames[k],TString::kIgnoreCase) )
         fRun->fTargetCup = InSANERun::kgSANETargetCups[k];
   }

   /*   const char * tgtsogm  = GetValueOf("target_sign");*/
   //    fRun->fTarget = InSANERun::kUNKNOWN;
//    for(int k =0;k<6;k++) if( !strcmp(tgttype,InSANERun::kgSANETargets[k]) )
//       fRun->fTarget = InSANERun::kgSANETargets[k]);

   fRun->fTargetAngle               = atof(GetValueOf("target_angle"));
   fRun->fTargetCurrent             = atof(GetValueOf("target_field"));
   fRun->fStartingPolarization      = atof(GetValueOf("target_start_pol"));
   fRun->fEndingPolarization        = atof(GetValueOf("target_stop_pol"));
   fRun->fTargetOnlinePolarization  = atof(GetValueOf("target_online_pol"));
   fRun->fTargetOfflinePolarization = atof(GetValueOf("target_offline_pol"));
   fRun->fPackingFraction           = atof(GetValueOf("target_packing_fraction"));


}
//______________________________________________________________________________

void InSANERunReport::Update()
{

   /// Must set the run number (so that isUPdated=true)
   /// otherwise SQL entry will no have run_number(primary key) in query
   /// when UpdateRunDatabase is called

   /// this should be a script???
   SetValueOf("Run_number",          Form("%d", fRun->GetRunNumber()));
   SetValueOf("analysis_pass",       Form("%d", fRun->fAnalysisPass));
   SetValueOf("replay_datetime",     Form("%s", fRun->fReplayDatetime.AsSQLString()));
   SetValueOf("number_of_beta2_events", Form("%d", fRun->fNBeta2Events));
   SetValueOf("number_of_beta1_events", Form("%d", fRun->fNBeta1Events));
   SetValueOf("number_of_events", Form("%d", fRun->fNEvents));
   SetValueOf("number_of_coin_events", Form("%d", fRun->fNCoinEvents));
   SetValueOf("number_of_pi0_events", Form("%d", fRun->fNPi0Events));
   SetValueOf("number_of_hms_events", Form("%d", fRun->fNHmsEvents));
   SetValueOf("number_of_LED_events", Form("%d", fRun->fNLedEvents));


   SetValueOf("pol_source_qe", Form("%f", fRun->fQuantumEfficiency));
   SetValueOf("beam_pol", Form("%f", fRun->fBeamPolarization));
   SetValueOf("beam_pass", Form("%d", fRun->fBeamPassNumber));
//    SetValueOf("analysis_pass",Form("%d",fRun->fAnalysisPass));
//    SetValueOf("analysis_pass",Form("%d",fRun->fAnalysisPass));
//    SetValueOf("analysis_pass",Form("%d",fRun->fAnalysisPass));
//    SetValueOf("analysis_pass",Form("%d",fRun->fAnalysisPass));
//    SetValueOf("analysis_pass",Form("%d",fRun->fAnalysisPass));

}
//______________________________________________________________________________
void InSANERunReport::UpdateRunDatabase()
{

   // Must set the run number (so that isUPdated=true)
   // otherwise MySQL entry will no have run_number(primary key) in query
   // - the above is no longer true since there is no SQLite "ON DUPLICATE..."
   SetValueOf("Run_number", Form("%d", fRun->GetRunNumber()));
   InSANEDatabaseManager * DBM = InSANEDatabaseManager::GetManager();

   TString vals = "";
   TString sql  = "";

   // For SQLite we don't have "ON Duplicate ..." so have to first
   // do a INSERT OR IGNORE ... followed by an UPDATE
   // Is there a better way?
   //vals = GetInsertValues();
   //sql  = "INSERT OR IGNORE INTO run_info " + vals + ";";
   ////std::cout << sql.Data() << std::endl;
   //DBM->ExecuteInsert(&sql);
   //sql = "UPDATE run_info SET " ;
   //InSANERunReportDatum * fDatum = 0;
   //int first = 0;
   //for (unsigned int i = 0; i < fReportData->size(); i++) {
   //   fDatum = (InSANERunReportDatum*) fReportData->at(i);
   //   if (fDatum->isUpdated) {
   //      if (first != 0) sql += ",";
   //      first = 1;
   //      sql += fDatum->fName;
   //      sql += "=";
   //      if (fDatum->isString) {
   //         sql += "'";
   //         sql += fDatum->fValue;
   //         sql += "'";
   //      } else {
   //         sql += fDatum->fValue;
   //      }
   //   }
   //}
   //sql += Form(" WHERE Run_number=%d ;", fRun->GetRunNumber());
   ////std::cout << sql.Data() << std::endl;
   //DBM->ExecuteInsert(&sql);


   sql  = "INSERT INTO ";
   sql += DBM->GetTable();
   sql += " SET ";
   InSANERunReportDatum * fDatum = nullptr;
   int first = 0;
   for(unsigned int i = 0;i < fReportData->size(); i++) {
      fDatum = (InSANERunReportDatum*) fReportData->at(i);
      if(fDatum->isUpdated) {
         if(first!=0) sql+= ",";
         first=1;
         sql += fDatum->fName;
         sql += "=";
         if(fDatum->isString) {
            sql += "'";
            sql += fDatum->fValue;
            sql += "'";
         } else {
            if(!strcmp(fDatum->fValue.Data(),"")){
               sql += 0;
            } else {
               sql += fDatum->fValue;
            }
         }
      }
   }
   first = 0;
   sql += " ON DUPLICATE KEY UPDATE ";
   for(unsigned int i = 0;i < fReportData->size(); i++) {
      fDatum = (InSANERunReportDatum*) fReportData->at(i);
      if(fDatum->isUpdated) {
         if(first!=0) sql+= ",";
         first=1;
         sql += fDatum->fName;
         sql += "=";
         if(fDatum->isString) {
            sql += "'";
            sql += fDatum->fValue;
            sql += "'";
         } else {
            if(!strcmp(fDatum->fValue.Data(),"")){
               sql += 0;
            } else {
               sql += fDatum->fValue;
            }
         }
      }
   }
   sql += " ;";
   //std::cout << "SQL COMMAND:\n" << sql.Data() << "\n";
   DBM->GetMySQLConnection()->Query(sql.Data());
   DBM->CloseConnections();
}
//______________________________________________________________________________
const char * InSANERunReport::GetInsertValues() {

   // Creates string like:
   // "(x,y,z) VALUES (1,2,'3')
   TString vals = "";
   if (fReportData->size() == 0) return vals.Data();
   vals = "(";
   for (unsigned int i = 0; i < fReportData->size(); i++)
      if (fReportData->at(i)->isUpdated) {
         vals += fReportData->at(i)->fName;
         vals += " ,";
      }
   vals.Remove(TString::kTrailing, ',');
   vals += ") VALUES (";
   for (unsigned int i = 0; i < fReportData->size(); i++)
      if (fReportData->at(i)->isUpdated) {
         if (fReportData->at(i)->isString) {
            vals += "'";
            vals += fReportData->at(i)->fValue;
            vals += "'";
         } else {
            vals += fReportData->at(i)->fValue;
         }
         vals += " ,";
      }
   vals.Remove(TString::kTrailing, ',');
   vals += ") ";
   return(vals.Data());
}
//______________________________________________________________________________

