#include "SANEFirstPassAnalyzer.h"
#include "SANERunManager.h"
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "TFitResult.h"
#include "InSANEDatabaseManager.h"


ClassImp(SANEFirstPassAnalyzer)
//_____________________________________________________________________________

SANEFirstPassAnalyzer::SANEFirstPassAnalyzer(const char * newTreeName, const char * uncorrectedTreeName)
   : SANEClusteringAnalysis(uncorrectedTreeName) , InSANEAnalyzer(newTreeName, uncorrectedTreeName)
{
   if (SANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o SANEFirstPassAnalyzer Constructor\n";

   // ---------------------------------
   // Go to new file
   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   fInputTree = nullptr;
   if(fAnalysisTree) fInputTree = fAnalysisTree; // anlysis tree set in InSANEDetectorAnalysis
   if(!fInputTree)   fInputTree = (TTree*)gROOT->FindObject(uncorrectedTreeName);
   if(!fInputTree){
      Error("SANEFirstPassAnalyzer","Tree: %s was not found!", uncorrectedTreeName);
   }

   // ---------------------------------
   // Scalers
   fScalerTreeName       = "Scalers0";
   fOutputScalerTreeName = "Scalers1";
   
   // Go to scaler file
   TFile * sf = InSANERunManager::GetRunManager()->GetScalerFile();
   if( sf ){
      sf->cd();
   } else {
      Error("SANEFirstPassAnalyzer","No Scaler File.");
   }

   // Find the scaler tree
   // First look in the current directory
   fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName.Data());
   // If it is not there look in the main file.
   if(!fScalerTree) {
      SANERunManager::GetRunManager()->GetCurrentFile()->cd();
      fScalerTree = (TTree*)gROOT->FindObject(fScalerTreeName.Data());
   }
   // Copy the scaler tree structure 
   if(!fScalerTree){
      Error("SANEFirstPassAnalyzer","scaler tree, %s, was not found!", fScalerTreeName.Data());
   } else {
      fScalers->SetBranches(fScalerTree);
   }

   fOutputFile = SANERunManager::GetRunManager()->GetCurrentFile();
   fOutputFile->cd();
   fMemFileName = "mem_out1.txt";
}
//______________________________________________________________________________
SANEFirstPassAnalyzer::~SANEFirstPassAnalyzer() {

   std::cout << " o SANEFirstPassAnalyzer Destructor." << std::endl;;

   SANERunManager::GetRunManager()->GetCurrentFile()->cd();

   if (fCalculatedTree) {
      std::cout << " - Building First Pass Index for corrected tree\n";
      fCalculatedTree->BuildIndex("fRunNumber", "fEventNumber");
      fCalculatedTree->Write();
   }

   if (fClusterTree) if (! strcmp(fCalculatedTree->GetName(), fClusterTree->GetName())) {
      std::cout << " - Building First Pass Index for cluster tree\n";
      fClusterTree->BuildIndex("bigcalClusterEvent.fRunNumber", "bigcalClusterEvent.fEventNumber");
   }

   SANERunManager::GetRunManager()->GetScalerFile()->cd();
   if (fOutputScalerTree) {
      fOutputScalerTree->Write("", TObject::kOverwrite);
   }

   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   SANERunManager::GetRunManager()->GetCurrentRun()->fAnalysisPass = 1;
   SANERunManager::GetRunManager()->GetRunReport()->UpdateRunDatabase();
   SANERunManager::GetRunManager()->WriteRun();

   std::cout << " Updating Run complete." << std::endl;

   //SANERunManager::GetRunManager()->GetCurrentFile()->Flush();
   //SANERunManager::GetRunManager()->GetCurrentFile()->Write();

   if (SANERunManager::GetRunManager()->GetVerbosity() > 0)  printf(" o SANEFirstPassAnalyzer destructor \n");

   std::cout << " o  END OF SANEFirstPassAnalyzer Destructor. \n";

}
//_____________________________________________________________________________
void SANEFirstPassAnalyzer::Initialize(TTree * inTree) {

   if (SANERunManager::GetRunManager()->GetVerbosity() > 1) std::cout << " o SANEFirstPassAnalyzer::Initialize(TTree *) \n";

   if (inTree)fInputTree = inTree;

   fClusterEvent = fEvents->SetClusterBranches(fInputTree);
   fClusters = fClusterEvent->fClusters;
   fCalculatedTree = fInputTree->CloneTree(0);
   fCalculatedTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s", fCalculatedTreeName.Data()));

//   if(fScalerTree) fOutputScalerTree = fScalerTree->CloneTree(0);
//     fOutputScalerTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s",fCalculatedTreeName.Data() ));
   SANERunManager::GetRunManager()->GetScalerFile()->cd();
   if (fScalerTree) fOutputScalerTree = fScalerTree->CloneTree(0);
   if (fOutputScalerTree) {
      fOutputScalerTree->SetNameTitle(fOutputScalerTreeName.Data(), Form("A corrected tree: %s", fOutputScalerTreeName.Data()));
      std::cout << " o Created output scaler tree, " << fOutputScalerTreeName.Data() << "\n";
   }


   InitCorrections();

   InitCalculations();

   /// Important: Set the event to be used for triggering in the InSANEAnalyzer
   InSANEAnalyzer::SetTriggerEvent(fScalers->fTriggerEvent);
   InSANEAnalyzer::SetDetectorTriggerEvent(fEvents->TRIG);
   InSANEAnalyzer::SetScalerTriggerEvent(fScalers->fTriggerEvent);
}
//_____________________________________________________________________________

void SANEFirstPassAnalyzer::MakePlots()
{
   if (SANERunManager::GetRunManager()->fVerbosity > 2) std::cout << " o SANEFirstPassAnalyzer::MakePlots() \n";

   //SANERunManager::GetRunManager()->WriteRun();
   //SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//   std::cout << " = Executing pass1/bigcalCherenkov.cxx\n";
//   gROOT->ProcessLine(Form(".x pass1/bigcalCherenkov.cxx(%d,%d) ",fRunNumber,0));
//
//   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//   std::cout << " = Executing pass1/pi0mass.cxx\n";
//   gROOT->ProcessLine(Form(".x pass1/pi0mass.cxx(%d) ",fRunNumber));
//
//   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//   std::cout << " = Executing pass1/RunAppend.cxx\n";
//   gROOT->ProcessLine(".x pass1/RunAppend.cxx ");

//   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//   std::cout << " = Executing pass1/luciteBarGeometry.cxx\n";
//   gROOT->ProcessLine(Form(".x pass1/luciteBarGeometry.cxx(%d) ",fRunNumber));
//
//   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//   std::cout << " = Executing pass1/findMirrorEdges.cxx\n";
//   gROOT->ProcessLine(Form(".x pass1/findMirrorEdges.cxx(%d) ",fRunNumber));
//
//   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
//
//   std::cout << " = Executing pass1/cherenkov.cxx\n";
//   gROOT->ProcessLine(Form(".x pass1/cherenkov.cxx(%d) ",fRunNumber));
//
//   std::cout << " = Executing pass1/cherenkov2.cxx\n";
//   gROOT->ProcessLine(Form(".x pass1/cherenkov2.cxx(%d) ",fRunNumber));

   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   SANERunManager::GetRunManager()->WriteRun();

//  SANERunManager::GetRunManager()->GetCurrentFile()->Flush();
//  SANERunManager::GetRunManager()->GetCurrentFile()->Write();
}


