#define BIGCALCluster_cxx
#include "BIGCALCluster.h"


ClassImp(BIGCALCluster)


//______________________________________________________________________________
BIGCALCluster::BIGCALCluster(Int_t clustNum) : InSANECalorimeterCluster(clustNum) {
   fPrimaryMirror           = 0;
   fNumberOfMirrors         = 0;
   fNumberOfGoodMirrors     = 0;
   fLuciteTDC               = -9999;
   fLuciteTDCSum            = -9999;
   fLuciteTDCDiff           = -9999;
   fCherenkovAvgTime        = 0;
   fCherenkovBestNPESum     = 0;
   fCherenkovBestADCSum     = 0;
   fCherenkovBestADC        = 0;
   fCherenkovBestNPEChannel = 0;
   fCherenkovBestADCChannel = 0;
   fGoodBigcalTiming        = false;
   fCherenkovTDC            = -9999;
   fIsGood                  = 0;
   fClusterTime             = -9999.0;
   fType                    = 0;
   fSubDetector             = 0;
   fDeltaE                  = 0;
   fDeltaTheta              = 0;
   fDeltaPhi                = 0;
   fDeltaThetaP             = 0;
   fDeltaPhiP               = 0;
   fDeltaX                  = 0;
   fDeltaY                  = 0;
   fGoodCherenkovTDCHit     = false;
   fGoodCherenkovTiming     = false;
   fLevel                   = -1;
   fHelicity                = 0;
   for (int i = 0; i < 10; i++) {
      fPassesTiming[i] = false;
   }
   Clear();
   //if(!cluster) cluster = new TH2I("clusterTemp","BigCal Cluster",32,1,32,56,1,56);
}
//______________________________________________________________________________
BIGCALCluster::~BIGCALCluster() {
}
//______________________________________________________________________________
void BIGCALCluster::Clear(Option_t * opt) {
   InSANECalorimeterCluster::Clear(opt);
   fPrimaryMirror           = 0;
   fNumberOfGoodMirrors     = 0;
   fLuciteTDC               = -9999;
   fLuciteTDCSum            = -9999;
   fLuciteTDCDiff           = -9999;
   fCherenkovTDC            = -9999;
   fCherenkovAvgTime        = -9999;
   fCherenkovBestNPESum     = 0;
   fCherenkovBestADCSum     = 0;
   fCherenkovBestADC        = 0;
   fCherenkovBestNPEChannel = 0;
   fCherenkovBestADCChannel = 0;
   fGoodBigcalTiming        = false;
   fIsGood                  = 0;
   fType                    = 0;
   fTotalE                  = 0.0;
   fDeltaE                  = 0;
   fDeltaTheta              = 0;
   fDeltaPhi                = 0;
   fDeltaThetaP             = 0;
   fDeltaPhiP               = 0;
   fDeltaX                  = 0;
   fDeltaY                  = 0;
   fSubDetector             = 0;
   fClusterTime             = -9999.0;
   fClusterTime1            = -9999.0;
   fClusterTime2            = -9999.0;
   fGoodCherenkovTDCHit     = false;
   fGoodCherenkovTiming     = false;
   fLevel                   = -1;
   fNumberOfMirrors         = 0;
   for (int i = 0; i < 10; i++) {
      fPassesTiming[i] = false;
   }
   fHelicity = 0;
}
//______________________________________________________________________________
BIGCALCluster& BIGCALCluster::operator=(const BIGCALCluster &rhs) {
   // Check for self-assignment!
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   // Deallocate, allocate new space, copy values...
   InSANECalorimeterCluster::operator=(rhs);

   fGoodBigcalTiming        = rhs.fGoodBigcalTiming;
   fPrimaryMirror           = rhs.fPrimaryMirror;
   fNumberOfMirrors         = rhs.fNumberOfMirrors;
   fNumberOfGoodMirrors     = rhs.fNumberOfGoodMirrors;
   fCherenkovTDC            = rhs.fCherenkovTDC;
   fCherenkovAvgTime        = rhs.fCherenkovAvgTime;
   fCherenkovBestNPESum     = rhs.fCherenkovBestNPESum;
   fCherenkovBestADCSum     = rhs.fCherenkovBestADCSum;
   fCherenkovBestADC        = rhs.fCherenkovBestADC;
   fCherenkovBestNPEChannel = rhs.fCherenkovBestNPEChannel;
   fCherenkovBestADCChannel = rhs.fCherenkovBestADCChannel;
   fIsGood                  = rhs.fIsGood;
   fType                    = rhs.fType;
   fSubDetector             = rhs.fSubDetector;
   fClusterTime             = rhs.fClusterTime;
   fClusterTime1            = rhs.fClusterTime1;
   fClusterTime2            = rhs.fClusterTime2;
   fGoodCherenkovTDCHit     = rhs.fGoodCherenkovTDCHit;
   fGoodCherenkovTiming     = rhs.fGoodCherenkovTiming;
   fLevel                   = rhs.fLevel;
   fHelicity                = rhs.fHelicity;
   fDeltaE                  = rhs.fDeltaE;
   fDeltaTheta              = rhs.fDeltaTheta;
   fDeltaPhi                = rhs.fDeltaPhi;
   fDeltaThetaP             = rhs.fDeltaThetaP;
   fDeltaPhiP               = rhs.fDeltaPhiP;
   fDeltaX                  = rhs.fDeltaX;
   fDeltaY                  = rhs.fDeltaY;
   return *this;
}
//______________________________________________________________________________
BIGCALCluster & BIGCALCluster::operator+=(const BIGCALCluster &rhs) {
   // not used
   fXMoment                 += rhs.fXMoment;
   fYMoment                 += rhs.fYMoment   ;
   fX2Moment                += rhs.fX2Moment  ;
   fY2Moment                += rhs.fY2Moment;
   fX3Moment                += rhs.fX3Moment;
   fY3Moment                += rhs.fY3Moment;
   fXStdDeviation           += rhs.fXStdDeviation;
   fYStdDeviation           += rhs.fYStdDeviation;
   fXSkewness               += rhs.fXSkewness;
   fYSkewness               += rhs.fYSkewness;
   fTotalE                  += rhs.fTotalE;
   fiPeak                   += rhs.fiPeak;
   fjPeak                   += rhs.fjPeak;
   fPrimaryMirror           += rhs.fPrimaryMirror;
   fNumberOfGoodMirrors     += rhs.fNumberOfGoodMirrors;
   fCherenkovAvgTime        += rhs.fCherenkovAvgTime;
   fCherenkovBestNPESum     += rhs.fCherenkovBestNPESum;
   fCherenkovBestADCSum     += rhs.fCherenkovBestADCSum;
   fCherenkovBestADC        += rhs.fCherenkovBestADC;
   fCherenkovBestNPEChannel += rhs.fCherenkovBestNPEChannel;
   fCherenkovBestADCChannel += rhs.fCherenkovBestADCChannel;
   fIsGood                  += rhs.fIsGood;
   fType                    += rhs.fType;
   fSubDetector             += rhs.fSubDetector;
   fClusterTime             += rhs.fClusterTime;
   fGoodCherenkovTDCHit     += rhs.fGoodCherenkovTDCHit;
   fGoodCherenkovTiming     += rhs.fGoodCherenkovTiming;
   fGoodBigcalTiming        += rhs.fGoodBigcalTiming;
   fLevel                   += rhs.fLevel;
   fCherenkovTDC            += rhs.fCherenkovTDC;

   return *this;
}
//______________________________________________________________________________
Int_t  BIGCALCluster::CalculateMoments() {
   /*  cout << " BIGCALClusterProcessor::CalculateQuantities \n";*/
   BIGCALGeometryCalculator *bcgeo = BIGCALGeometryCalculator::GetCalculator();
   Int_t m, n, i, j;
   i = fiPeak;//(Int_t)TMath::Floor((Double_t)(*fiPeaks)[k]);
   j = fjPeak;//(Int_t)TMath::Floor((Double_t)(*fjPeaks)[k]);
   //acluster->SetClusterPeak(i, j);
   /// The cluster is selected to be a 5x5 grid centered on the peaks
   /// The following loops over all the blocks and adds
   Int_t offset = -2;
   /// initialize the hightest energy to the central block (as it should be)
   fHighestEnergy       = 0.0;//fBlockEnergies[2][2];
   fSecondHighestEnergy = 0.0;//fBlockEnergies[2][2];
   fThirdHighestEnergy  = 0.0;//fBlockEnergies[2][2];
   fXMoment = 0.0;
   fYMoment = 0.0;
   fX2Moment = 0.0;
   fY2Moment = 0.0;
   fX3Moment = 0.0;
   fY3Moment = 0.0;
   fX4Moment = 0.0;
   fY4Moment = 0.0;
   fTotalE  = 0.0;
   fNNonZeroBlocks = 0;
   for (n = offset ; n < 5 + offset ; n++) {
      for (m = offset ; m < 5 + offset ; m++) {
         if (((j + n <= 56 && j + n > 32) && (i + m <= 30 && i + m > 0))/*RCS*/   ||
             ((j + n <= 32 && j + n > 0) && (i + m <= 32 && i + m > 0))/*PROT*/) {

            if(fBlockEnergies[m-offset][n-offset] > fHighestEnergy ) fHighestEnergy = fBlockEnergies[m-offset][n-offset];
            /// if block is new highest
            if (fHighestEnergy < fBlockEnergies[m-offset][n-offset]) {
               fThirdHighestEnergy  = fSecondHighestEnergy;
               fSecondHighestEnergy = fHighestEnergy;
               fHighestEnergy       = fBlockEnergies[m-offset][n-offset];
            } else if (fSecondHighestEnergy < fBlockEnergies[m-offset][n-offset]) {
               fThirdHighestEnergy  = fSecondHighestEnergy;
               fSecondHighestEnergy = fBlockEnergies[m-offset][n-offset];
            } else if (fThirdHighestEnergy < fBlockEnergies[m-offset][n-offset]) {
               fThirdHighestEnergy  = fBlockEnergies[m-offset][n-offset];
            }
            if (fBlockEnergies[m-offset][n-offset] > 0.0) fNNonZeroBlocks++;

            // cluster->SetBinContent(i+m,j+n,block_energy[m+2][n+2] );
            // Calculate the mean positions (first moments)
            fXMoment += fBlockEnergies[m-offset][n-offset] * bcgeo->GetBlockXij_BCCoords(i + m, j + n);
            fYMoment += fBlockEnergies[m-offset][n-offset] * bcgeo->GetBlockYij_BCCoords(i + m, j + n);
            fTotalE += fBlockEnergies[m-offset][n-offset];
         }
      }
   }
   fXMoment = fXMoment / (fTotalE);
   fYMoment = fYMoment / (fTotalE);
// Now we can calculate the central moments of interest (variance, skewness, and higher...)
   for (n = offset ; n < 5 + offset ; n++) {
      for (m = offset ; m < 5 + offset ; m++) {
         if (((j + n <= 56 && j + n > 32) && (i + m <= 30 && i + m > 0))/*RCS*/ || ((j + n <= 32 && j + n > 0) && (i + m <= 32 && i + m > 0))/*PROT*/)   {
            fX2Moment += fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockXij_BCCoords(i + m, j + n) - fXMoment , 2.0);
            fY2Moment += fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockYij_BCCoords(i + m, j + n) - fYMoment , 2.0);
            fX3Moment += fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockXij_BCCoords(i + m, j + n) - fXMoment , 3.0);
            fY3Moment += fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockYij_BCCoords(i + m, j + n) - fYMoment , 3.0);
            fX4Moment += fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockXij_BCCoords(i + m, j + n) - fXMoment , 4.0);
            fY4Moment += fBlockEnergies[m-offset][n-offset] * TMath::Power(
                                      bcgeo->GetBlockYij_BCCoords(i + m, j + n) - fYMoment , 4.0);
         }
      }
   }
   fX2Moment = fX2Moment / (fTotalE); // second moment is the variance (sigma^2)
   fY2Moment = fY2Moment / (fTotalE);
   fX3Moment = fX3Moment / (fTotalE);
   fY3Moment = fY3Moment / (fTotalE);
   fX4Moment = fX4Moment / (fTotalE);
   fY4Moment = fY4Moment / (fTotalE);

   fXStdDeviation = TMath::Power(fX2Moment , 0.5);
   fYStdDeviation = TMath::Power(fY2Moment , 0.5);

   fXSkewness = fX3Moment * TMath::Power(1.0 / (fXStdDeviation) , 3.0);
   fYSkewness = fY3Moment * TMath::Power(1.0 / (fYStdDeviation) , 3.0);

   // Here we are calculating the "excess kurtosis" which is mu_4/sigma^4 - 3.0
   // where the minus 3 makes the kurtosis of a normal distribution 0. 
   fXKurtosis = fX4Moment * TMath::Power(1.0 / (fXStdDeviation) , 4.0) - 3.0;
   fYKurtosis = fY4Moment * TMath::Power(1.0 / (fYStdDeviation) , 4.0) - 3.0;

   //cout << "cords : " << bcgeo->getBlockXij_BCCoords(i,j) << " , " << bcgeo->getBlockYij_BCCoords(i,j) << "\n" ;
   //cout << "total energy " << totalE << "\n";

   return(0);

}
