#define InSANEClusterEvent_cxx
#include "InSANEClusterEvent.h"

ClassImp(InSANEClusterEvent)


//______________________________________________________________________________
InSANEClusterEvent::InSANEClusterEvent() {
   Reset();
   fClusters = nullptr;
   fClusterSourceHistogram = nullptr;
}
//______________________________________________________________________________
InSANEClusterEvent::~InSANEClusterEvent() {

}
//______________________________________________________________________________
void InSANEClusterEvent::Reset() {
   fNClusters = 0;
}
//______________________________________________________________________________

