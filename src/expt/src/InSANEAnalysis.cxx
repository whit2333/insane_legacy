#include "InSANEAnalysis.h"

ClassImp(InSANEAnalysis)

//______________________________________________________________________________
InSANEAnalysis::InSANEAnalysis(const char * sourceTreeName)
{
   fOutputFile   = nullptr;
   fAnalysisFile = nullptr;
   fAnalysisTree = nullptr;
   fOutputTree   = nullptr;
   fScalers      = nullptr;
   fEvents       = nullptr;
   fEventDisplay = false;
   fRunDisplay   = false;
   fAnalysisType = 3;
   fDebug        = 0;
   fRunNumber    = 0;
   fBigcalGeo    = nullptr;
   aBetaEvent    = nullptr;
   bcEvent       = nullptr;
   aBigcalHit    = nullptr;
   lhEvent       = nullptr;
   aLucHit       = nullptr;
   gcEvent       = nullptr;
   aCerHit       = nullptr;
   ftEvent       = nullptr;
   aTrackerHit   = nullptr;
   beamEvent     = nullptr;
   hmsEvent      = nullptr;
   mcEvent       = nullptr;
   trigEvent     = nullptr;
}
//______________________________________________________________________________
InSANEAnalysis::~InSANEAnalysis() {
}
//______________________________________________________________________________
void         InSANEAnalysis::SetEvents(SANEEvents * events) { fEvents = events; }
//______________________________________________________________________________
SANEEvents * InSANEAnalysis::GetEvents() const { return(fEvents); }
//______________________________________________________________________________
void         InSANEAnalysis::ClearEvents() { if (fEvents)fEvents->ClearEvent(); }
//______________________________________________________________________________
void         InSANEAnalysis::SetScalers(SANEScalers * scalers) { fScalers = scalers; }
//______________________________________________________________________________
SANEScalers * InSANEAnalysis::GetScalers() const { return(fScalers); }
//______________________________________________________________________________





