#define BETAEvent_cxx
#include "BETAEvent.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "InSANERunManager.h"

/** BETAEvent Class is to be used to store raw or simulation data to the same
 * raw data format.
 */

ClassImp(BETAEvent)

//_________________________________________________________________//

BETAEvent::BETAEvent()
{

//   if(InSANERunManager::GetRunManager()->fVerbosity > 0) printf(" - Creating BETA Event \n");

   fBigcalEvent = nullptr;//new BigcalEvent();
   fGasCherenkovEvent = nullptr; // new GasCherenkovEvent();
   fLuciteHodoscopeEvent = nullptr;//new LuciteHodoscopeEvent();
   fForwardTrackerEvent = nullptr;//new ForwardTrackerEvent();



}
//_________________________________________________________________//

Int_t BETAEvent::AllocateMemory()
{
   fBigcalEvent = new BigcalEvent();
//fBigcalEvent->AllocateHitArray();
   fGasCherenkovEvent = new GasCherenkovEvent();
//fGasCherenkovEvent->AllocateHitArray();
   fLuciteHodoscopeEvent = new LuciteHodoscopeEvent();
//fLuciteHodoscopeEvent->AllocateHitArray();
   fForwardTrackerEvent = new ForwardTrackerEvent();
//fForwardTrackerEvent->AllocateHitArray();
   return(0);
}
//_________________________________________________________________//

Int_t BETAEvent::AllocateHitsMemory()
{
   if (fBigcalEvent) fBigcalEvent->AllocateHitArray();
   if (fGasCherenkovEvent) fGasCherenkovEvent->AllocateHitArray();
   if (fLuciteHodoscopeEvent) fLuciteHodoscopeEvent->AllocateHitArray();
   if (fForwardTrackerEvent) fForwardTrackerEvent->AllocateHitArray();
   return(0);
}
//_________________________________________________________________//

Int_t BETAEvent::FreeMemory()
{
   if (fBigcalEvent) delete fBigcalEvent;
   fBigcalEvent = nullptr;
   if (fGasCherenkovEvent) delete fGasCherenkovEvent;
   fGasCherenkovEvent = nullptr;
   if (fLuciteHodoscopeEvent) delete fLuciteHodoscopeEvent;
   fLuciteHodoscopeEvent = nullptr;
   if (fForwardTrackerEvent) delete fForwardTrackerEvent;
   fForwardTrackerEvent = nullptr;
   return(0);
}
//_________________________________________________________________//

BETAEvent::~BETAEvent()
{
   if (InSANERunManager::GetRunManager()->fVerbosity > 0)  std::cout << " - deleted BETAEvent   " << std::endl;
}

Int_t BETAEvent::SetHitsBranch(TTree * t)
{
   t->SetBranchAddress("bigcalHits", &(fBigcalEvent->fBigcalHits));
   t->SetBranchAddress("cherenkovHits", &(fGasCherenkovEvent->fGasCherenkovHits));
   t->SetBranchAddress("luciteHits", &(fLuciteHodoscopeEvent->fLuciteHits));
   t->SetBranchAddress("lucitePositionHits", &(fLuciteHodoscopeEvent->fLucitePositionHits));
   t->SetBranchAddress("trackerHits", &(fForwardTrackerEvent->fTrackerHits));
   return(0);
}
//______________________________________________________________________________
Int_t BETAEvent::AddHitsBranch(TTree * t) {
   t->Branch("bigcalHits", fBigcalEvent->fBigcalHits);
   t->Branch("cherenkovHits", fGasCherenkovEvent->fGasCherenkovHits);
   t->Branch("luciteHits", fLuciteHodoscopeEvent->fLuciteHits);
   t->Branch("lucitePositionHits", fLuciteHodoscopeEvent->fLucitePositionHits);
   t->Branch("trackerHits", fForwardTrackerEvent->fTrackerHits);
   return(0);
}
//______________________________________________________________________________
void BETAEvent::ClearEvent(const char * option) {
   fEventNumber   = -1;
   fGoodEvent     = kFALSE;
   fRunNumber     = -1;
   fNumberOfHits  = 0;
   eventNumber    = -1;
   fGoodTiming    = false;
   fGoodCherenkov = false;
   fGoodGeometry  = false;
   if (fGasCherenkovEvent)    fGasCherenkovEvent->ClearEvent(option);
   if (fLuciteHodoscopeEvent) fLuciteHodoscopeEvent->ClearEvent(option);
   if (fBigcalEvent)          fBigcalEvent->ClearEvent(option);
   if (fForwardTrackerEvent)  fForwardTrackerEvent->ClearEvent(option);
}


