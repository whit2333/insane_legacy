#include "InSANESlowSkimmer.h"

ClassImp(InSANESlowSkimmer)

//______________________________________________________________________________
InSANESlowSkimmer::InSANESlowSkimmer(const char * n,const char* t) : TNamed(n,t) {
   fFilterList.Clear();
   fIsFlagged = false;
   fScalerEvent = nullptr;
}
//______________________________________________________________________________
InSANESlowSkimmer::~InSANESlowSkimmer() {
}
//______________________________________________________________________________
void InSANESlowSkimmer::AddFilter(InSANESkimmerFilter * filter) {
   fFilterList.Add(filter);
}
//______________________________________________________________________________
Bool_t InSANESlowSkimmer::PassesFilter() {
   Int_t result = 0;
   InSANESkimmerFilter *afilter = nullptr;
   TListIter iter(&fFilterList);
   while ((afilter = (InSANESkimmerFilter *)iter.Next()))
      result += 0;// afilter->Apply(/*next.GetOption()*/);
   if (result == 0) return (true);
   else return(false);
}
//______________________________________________________________________________
void InSANESlowSkimmer::SetScalerEvent(InSANEScalerEvent * ev) {
   fScalerEvent = ev;
}
//______________________________________________________________________________
InSANEScalerEvent * InSANESlowSkimmer::GetScalerEvent() {
   return fScalerEvent;
}
//______________________________________________________________________________
Bool_t InSANESlowSkimmer::IsFlagged() const { return fIsFlagged; }
//______________________________________________________________________________
void InSANESlowSkimmer::Initialize() {
   //      fRunFlags = InSANERunManager::GetRunManager()->GetCurrentRun()->GetRunFlags();
   InSANESkimmerFilter * aFilter = nullptr;
   TListIter filterIter(&fFilterList);
   while ((aFilter = (InSANESkimmerFilter*)filterIter.Next())) {
      if (fScalerEvent) aFilter->SetScalerEvent(fScalerEvent);
      aFilter->Initialize();
   }
   //InSANERunFlag * aFlag=0;
   //TListIter flagIter(fRunFlags);
   //while ( (aFlag = (InSANERunFlag*)flagIter.Next() ) )
   //{
   //aFlag->Initialize();
   //}
   //
}
//______________________________________________________________________________
void InSANESlowSkimmer::ProcessFlags() {
   fIsFlagged = false; //...
   InSANESkimmerFilter * aFilter = nullptr;
   TListIter filterIter(&fFilterList);
   while ((aFilter = (InSANESkimmerFilter*)filterIter.Next())) {
      aFilter->Process();
   }
}
//______________________________________________________________________________
int InSANESlowSkimmer::ProcessFilters() {
   Int_t ret_val = 0;
   InSANESkimmerFilter * aFilter = nullptr;
   TListIter filterIter(&fFilterList);
   while ((aFilter = (InSANESkimmerFilter*)filterIter.Next())) {
      if (aFilter->IsEventFlaggedSkip()) ret_val--;
   }
   return(ret_val);
}
//______________________________________________________________________________





