#include "InSANEDetectorCalibration.h"

//______________________________________________________________________________
ClassImp(InSANECalibration)

//______________________________________________________________________________
InSANECalibration::InSANECalibration(const char * name, const char * title) : TNamed(name, title),
   fChannel(0), fNParameters(0)  {
   auto * f = new TF1(Form("func%s", GetName()), "[0]+x*[1]", -1, 1);
   //fParameters = new std::vector<TParameter<double>*>;//new TClonesArray("TParameter<double>",1);
   //fFunctions = new TList();
   fFunctions.Add(f);
   //      fParameters->SetName(Form("%sPars",GetName()));
    // fParameters->SetOwner(kTRUE);
}
//______________________________________________________________________________
InSANECalibration::~InSANECalibration() {
   //     if(fFunction) delete fFunction;
   //      if(fParameters) delete fParameters;
}

//______________________________________________________________________________
TF1 * InSANECalibration::GetFunction(Int_t n)  const {
   if (fFunctions.GetEntries() > 0)
      if (n >= 0 &&  n < fFunctions.GetEntries()) {
         return((TF1*)fFunctions.At(n));
      }
   Error(" InSANECalibration::GetFunction(Int_t n)", "Argument n=%d is out of range.", n);
   return(nullptr);
}
//______________________________________________________________________________
void InSANECalibration::AddFunction(TF1 * f, Int_t n) {
   if (f) {
      if (n == 0) fFunctions.Clear();
      Int_t npar = f->GetNpar();
      for (int i = 0; i < npar; i++) {
         AddParameter(new TParameter<double>(Form("par-%d", i), f->GetParameter(i)));
      }
      fFunctions.Add(f);
   } else {
      Warning("AddFunction", "Null function argument");
   }

}
//______________________________________________________________________________
TParameter<double> * InSANECalibration::GetParameter(Int_t n) const {
   if (n >= 0 &&  n < fNParameters) {
      return((TParameter<double>*)fParameters.At(n));
   } /* else */
   Error(" InSANECalibration::GetParameter(Int_t chan)", "Argument chan is out of range");
   return(nullptr);
}
//______________________________________________________________________________

Int_t InSANECalibration::AddParameter(TParameter<double> * par) {
   //    new( (*fParameters)[fNParameters]) TParameter<double>(par->GetName(),par->GetVal());
   fParameters.Add(par);
   fNParameters++;
   return(fNParameters);
}
//______________________________________________________________________________

void InSANECalibration::Clear(Option_t *) {
   fFunctions.Clear();
   fParameters.Clear();
   fHistograms.Clear();
   fChannel = -1;
   fNParameters = 0;
}

//______________________________________________________________________________
void    InSANECalibration::AddHistogram(TH1 * h, Int_t n) {
   if (h) {
      if (n == 0) fHistograms.Clear();
      fHistograms.Add(h);
   } else {
      Warning("AddHistogram", "Null histogram argument");
   }
}
//______________________________________________________________________________
void InSANECalibration::Print(Option_t * ) const {
   std::cout << " Calibration: " << std::setw(20) << GetName() << " ( " <<  GetTitle() << " )\n";
   std::cout << std::setfill('.');
   std::cout << " " <<  std::right << std::setw(8) << " " <<  "Channel" << '\t' << GetChannel() << std::endl;
   std::cout << " " << std::right << std::setw(8) << " " << "N pars " << '\t' << GetNPar() << std::endl;
   for (int i = 0; i < GetNPar() ; i++) {
      std::cout << " " << std::right << std::setw(16) << " " << "Par-" << i << '\t' << GetParameter(i)->GetVal() << std::endl;
   }
   std::cout << " " << std::right << std::setw(8) << " " << "N func " << '\t' << fFunctions.GetEntries() << std::endl;
   for (int i = 0; i < fFunctions.GetEntries() ; i++) {
      std::cout << " " << std::right << std::setw(16) << " " << "func-" << i << '\t' << GetFunction(i)->GetExpFormula("p").Data() << std::endl;
   }
   std::cout << " " << std::right << std::setw(8) << " " << "N hist " << '\t' << fHistograms.GetEntries() << std::endl;
   std::cout << std::setfill(' ');
}

//______________________________________________________________________________

//______________________________________________________________________________
ClassImp(InSANEDetectorCalibration)

//______________________________________________________________________________
InSANEDetectorCalibration::InSANEDetectorCalibration(const char * name, const char * title)
   : TNamed(name, title) {
      fNChannels = 0;
      //       fCalibrations = new TList();
      //       fCalibrations->SetName(Form("%sCalibs",GetName()));
      //       fCalibrations->SetOwner(kTRUE);
   }
//______________________________________________________________________________
InSANEDetectorCalibration::~InSANEDetectorCalibration() {
}
//______________________________________________________________________________
Int_t InSANEDetectorCalibration::AddCalibration(InSANECalibration * cal) {
   fCalibrations.Add(cal);
   fNChannels = fCalibrations.GetEntries();
   return(fNChannels);
}

//______________________________________________________________________________
InSANECalibration * InSANEDetectorCalibration::GetCalibration(Int_t n) const {
   if (/*n < fNChannels &&*/ n >= 0) {
      return((InSANECalibration*)fCalibrations.At(n));
   }
   Error("InSANECalibration::GetCalibration(Int_t chan)", "Argument chan is out of range.");
   return(nullptr);
}
//______________________________________________________________________________

