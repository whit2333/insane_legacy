#include "GasCherenkovGeometryCalculator.h"
#include <TObject.h>
#include "TMath.h"
#include <iostream>

ClassImp(GasCherenkovGeometryCalculator)
//___________________________________________________________________

GasCherenkovGeometryCalculator * GasCherenkovGeometryCalculator::fgGasCherenkovGeometryCalculator = nullptr;
//___________________________________________________________________

GasCherenkovGeometryCalculator * GasCherenkovGeometryCalculator::GetCalculator()
{
   if (!fgGasCherenkovGeometryCalculator) fgGasCherenkovGeometryCalculator = new GasCherenkovGeometryCalculator();
   return(fgGasCherenkovGeometryCalculator);
}
//___________________________________________________________________

GasCherenkovGeometryCalculator::~GasCherenkovGeometryCalculator()
{
   ;
}
//___________________________________________________________________

GasCherenkovGeometryCalculator::GasCherenkovGeometryCalculator() : fConeSize(15.0)
{

   fMirrorGeoCutX[0] = -64.0;
   fMirrorGeoCutX[1] = 0.0;
   fMirrorGeoCutX[2] = 64.0;

   fMirrorGeoCutY[0] = -112.0;
   fMirrorGeoCutY[1] = -64.0;
   fMirrorGeoCutY[2] = 0.0;
   fMirrorGeoCutY[3] = 64.0;
   fMirrorGeoCutY[4] = 112.0;
   /*   fMirrorHorizontalEdges[0] = new TF1("")*/

   alpha2 = -14.0 * TMath::Pi() / 180.0; //16.35*pi/180,   // 14
   beta2 = -32.5 * TMath::Pi() / 180.0; //31.7*pi/180;  // 32.5
   alpha4 = -7.50000048 * TMath::Pi() / 180.0; //8.75*pi/180; //7.50000048
   beta4 = -33.0 * TMath::Pi() / 180.0; // 32.62*pi/180;  // 33.0
   alpha6 = 7.50000048 * TMath::Pi() / 180.0; //-8.75*pi/180; //trial1: 9.1 //-.50000048
   beta6 = -32.5 * TMath::Pi() / 180.0; //32.25*pi/180; // trial2: 32.25 //32.5
   alpha8 = 14.0 * TMath::Pi() / 180.0; //-16.35*pi/180; // -14
   beta8 = -32.5 * TMath::Pi() / 180.0; //31.7*pi/180; //32.5*

   alpha1 = -3.5 * TMath::Pi() / 180.0;
   beta1 = -9.0 * TMath::Pi() / 180.0;
   alpha3 =  2.0 * TMath::Pi() / 180.0;
   beta3 =  -9.0 * TMath::Pi() / 180.0;
   alpha5 = -2.0 * TMath::Pi() / 180.0;
   beta5 =  -9.0 * TMath::Pi() / 180.0;
   alpha7 = 4.0 * TMath::Pi() / 180.0;
   beta7 =  -9.0 * TMath::Pi() / 180.0;

   fDistanceFromTarget = 62.5; //cm


   fMirrorsToSum = new std::vector<int>;
}
//___________________________________________________________________

Int_t GasCherenkovGeometryCalculator::iMirror(Int_t mirror)
{
   if (mirror < 9 && mirror > 0) {
      if ((mirror) % 2 == 0) return(1);
      else return(2);
   } else {
      return(0);
   }
}
//___________________________________________________________________

Int_t GasCherenkovGeometryCalculator::jMirror(Int_t mirror)
{
   if (mirror < 9 && mirror > 0) {
      return((mirror - 1) / 2 + 1);
   } else {
      return(0);
   }
}
//___________________________________________________________________

Bool_t GasCherenkovGeometryCalculator::IsNeighboringMirror(Int_t mir, Int_t potentialNeighbor)
{
   if (mir == potentialNeighbor) return(false);
   if (mir > 8 || mir < 1 || potentialNeighbor > 8 || potentialNeighbor < 1) return(false);
   int low  = (mir - iMirror(mir) % 2) - 3;
   int high = (mir - iMirror(mir) % 2) + 3;
   if (potentialNeighbor > low  && potentialNeighbor <= high) return(true);
   return(false);
}
//___________________________________________________________________

Bool_t GasCherenkovGeometryCalculator::AddMirrorToCluster(Int_t mirror, Double_t bcXcoord, Double_t bcYcoord)
{
   //check the horizontal position first
   Int_t iMir, jMir;
   iMir = iMirror(mirror);
   jMir = jMirror(mirror);
   if (!iMir || !jMir) {
      printf("block index error in GasCherenkovGeometryCalculator::AddMirrorToCluster");
      return(kFALSE);
   }
   if (fMirrorGeoCutX[iMir-1] - fConeSize < bcXcoord &&
       fMirrorGeoCutX[iMir] + fConeSize > bcXcoord &&
       fMirrorGeoCutY[jMir-1] - fConeSize < bcYcoord &&
       fMirrorGeoCutY[jMir] + fConeSize > bcYcoord) {
//      printf(" %d %d , %f %f \n",iMir,jMir,bcXcoord,bcYcoord);
      return(true);
   } else {
      return(false);
   }
}

Bool_t GasCherenkovGeometryCalculator::AddMirrorToClusterSimple(Int_t mirror, Double_t bcXcoord, Double_t bcYcoord)
{
   //check the horizontal position first
   Int_t iMir, jMir;
   iMir = iMirror(mirror);
   jMir = jMirror(mirror);
   if (!iMir || !jMir) {
      printf("block index error in GasCherenkovGeometryCalculator::AddMirrorToCluster");
      return(kFALSE);
   }
   if (fMirrorGeoCutX[iMir-1] < bcXcoord &&
       fMirrorGeoCutX[iMir] > bcXcoord &&
       fMirrorGeoCutY[jMir-1] < bcYcoord &&
       fMirrorGeoCutY[jMir] > bcYcoord) {
//      printf(" %d %d , %f %f \n",iMir,jMir,bcXcoord,bcYcoord);
      return(true);
   } else {
      return(false);
   }
}
//___________________________________________________________________

Int_t GasCherenkovGeometryCalculator::GetPrimaryMirror(Double_t bcXcoord, Double_t bcYcoord)
{
   //check the horizontal position first
   Int_t iMir, jMir;
   for (int k = 1; k < 9; k++) {
      iMir = iMirror(k);
      jMir = jMirror(k);
      if (!iMir || !jMir) {
         printf("block index error in GasCherenkovGeometryCalculator::AddMirrorToCluster");
         return(-1);
      }
      if (fMirrorGeoCutX[iMir-1] < bcXcoord &&
          fMirrorGeoCutX[iMir] > bcXcoord &&
          fMirrorGeoCutY[jMir-1] < bcYcoord &&
          fMirrorGeoCutY[jMir] > bcYcoord) {
         return(k);
      }
   }
   return(-1);
}
//___________________________________________________________________

std::vector<int> * GasCherenkovGeometryCalculator::GetMirrorsToAdd(Double_t bcXcoord, Double_t bcYcoord)
{
   fMirrorsToSum->clear();
   for (int i = 1; i < 9; i++) {
      if (AddMirrorToCluster((Int_t)i, bcXcoord, bcYcoord)) {
//        printf("%f,%f is good on mirror %d\n",bcXcoord,bcYcoord,i);
         fMirrorsToSum->push_back(i);
      }
   }
   return(fMirrorsToSum);
}
//___________________________________________________________________


