#define AnalyzerScalersToInSANE_cxx
// The class definition in AnalyzerScalersToInSANE.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("AnalyzerScalersToInSANE.C")
// Root > T->Process("AnalyzerScalersToInSANE.C","some options")
// Root > T->Process("AnalyzerScalersToInSANE.C+")
//
#include "SANEScalerEvent.h"
#include "TFile.h"
#include "TTree.h"
#include "InSANERun.h"
#include "SANERunManager.h"
#include "InSANETriggerEvent.h"

#include "AnalyzerScalersToInSANE.h"
#include <TH2.h>
#include <TStyle.h>


void AnalyzerScalersToInSANE::Begin(TTree * tree)
{

   if (InSANERunManager::GetRunManager()->fVerbosity > 1)  std::cout << " o AnalyzerScalersToInSANE::Begin() \n";
   fChain = tree;
   TString option = GetOption();
   fTime = 0;
   fRun = SANERunManager::GetRunManager()->GetCurrentRun();
   aFile = SANERunManager::GetRunManager()->GetCurrentFile();
   TFile * scalerFile = InSANERunManager::GetRunManager()->GetScalerFile();
   //  aFile = new TFile(Form("data/rootfiles/InSANE%s.root",option.Data()),"UPDATE");
   scalerFile->cd();
   scalerTree = new TTree("Scalers", "The SANE Scaler Data");
   //   scalerTree->Branch("fScalerEvent","SANEScalerEvent",&(fSANEScalers->fScalerEvent));
   fSANEScalers = new SANEScalers("Scalers");

   aFile->cd();

}
//_____________________________________________________________________________

void AnalyzerScalersToInSANE::SlaveBegin(TTree * /*tree*/)
{
   // The SlaveBegin() function is called after the Begin() function.
   // When running with PROOF SlaveBegin() is called on each slave server.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();
}
//_____________________________________________________________________________

Bool_t AnalyzerScalersToInSANE::Process(Long64_t entry)
{

   fChain->GetTree()->GetEntry(entry);

   fSANEScalers->fTriggerEvent->fCodaType = 0;
   fSANEScalers->fTriggerEvent->fEventNumber = fEventNumber;
   fSANEScalers->fTriggerEvent->fRunNumber = fRun->fRunNumber;

   fSANEScalers->fScalerEvent->fEventNumber = fEventNumber;
   fSANEScalers->fScalerEvent->fRunNumber = fRun->fRunNumber;

   fSANEScalers->fScalerEvent->f1MHzClockScaler2                  = fScalers[173]; // HMS 1MHz scaler

   fSANEScalers->fScalerEvent->f1MHzClockScaler                   = fScalers[502];
   fSANEScalers->fScalerEvent->fUnserScaler                       = fScalers[503];
   fSANEScalers->fScalerEvent->fBCM1Scaler                        = fScalers[504];
   fSANEScalers->fScalerEvent->fBCM2Scaler                        = fScalers[505];
   fSANEScalers->fScalerEvent->fHMS1Counter.fScaler               = fScalers[506];
   fSANEScalers->fScalerEvent->fBETA1Counter.fScaler              = fScalers[507];
   fSANEScalers->fScalerEvent->fPi0Counter.fScaler                = fScalers[508];
   fSANEScalers->fScalerEvent->fBETA2Counter.fScaler              = (fScalers[509]);
   fSANEScalers->fScalerEvent->fCoinCounter.fScaler               = (fScalers[510]);
   fSANEScalers->fScalerEvent->fHMS2Counter.fScaler               = fScalers[549];
   //fSANEScalers->fScalerEvent->fTSCounter.fScaler                 = (fScalers[511]);
   //Not sure if the prescale factors have been accounted for. 
   fSANEScalers->fScalerEvent->fTSCounter.fScaler                 = /*fScalers[549]*/
                                                                  + fScalers[506]/fRun->fPreScale[0]
                                                                  //+ fScalers[507]/fRun->fPreScale[1]
                                                                  + fScalers[508]/fRun->fPreScale[2]
                                                                  + fScalers[509]/fRun->fPreScale[3]
                                                                  + fScalers[510]/fRun->fPreScale[4];
   // Fix to add HMS2 due to CODA "cosmics" flag 
   if(fRun->fRunNumber >= 72532 && fRun->fRunNumber <= 72600)  
      fSANEScalers->fScalerEvent->fTSCounter.fScaler              += fScalers[549];

   fSANEScalers->fScalerEvent->fBCM1PositiveHelicityScaler        = fScalers[512];
   fSANEScalers->fScalerEvent->fBCM1NegativeHelicityScaler        = fScalers[513];
   fSANEScalers->fScalerEvent->f1MHzClockPositiveHelicityScaler   = fScalers[514];
   fSANEScalers->fScalerEvent->f1MHzClockNegativeHelicityScaler   = fScalers[515];
   fSANEScalers->fScalerEvent->fBCM2PositiveHelicityScaler        = fScalers[516];
   fSANEScalers->fScalerEvent->fBCM2NegativeHelicityScaler        = fScalers[517];
   /* 518 - 529 are the cherenkov followed by vacant */
   fSANEScalers->fScalerEvent->fHMS1Counter.fNegativeHelicityScaler  = fScalers[534];
   fSANEScalers->fScalerEvent->fBETA1Counter.fNegativeHelicityScaler = fScalers[535];
   fSANEScalers->fScalerEvent->fPi0Counter.fNegativeHelicityScaler   = fScalers[536];
   fSANEScalers->fScalerEvent->fBETA2Counter.fNegativeHelicityScaler = fScalers[537];
   fSANEScalers->fScalerEvent->fCoinCounter.fNegativeHelicityScaler  = fScalers[538];
   fSANEScalers->fScalerEvent->fHMS2Counter.fNegativeHelicityScaler  = fScalers[540];
   //fSANEScalers->fScalerEvent->fTSCounter.fNegativeHelicityScaler    = fScalers[539];
   fSANEScalers->fScalerEvent->fTSCounter.fNegativeHelicityScaler    = fScalers[534]/fRun->fPreScale[0]
                                                                     //+ fScalers[535]/fRun->fPreScale[1]
                                                                     + fScalers[536]/fRun->fPreScale[2]
                                                                     + fScalers[537]/fRun->fPreScale[3]
                                                                     + fScalers[538]/fRun->fPreScale[4]
                                                                     /*+ fScalers[540]*/; 
   // Fix to add HMS2 due to CODA "cosmics" flag 
   if(fRun->fRunNumber >= 72532 && fRun->fRunNumber <= 72600)  
      fSANEScalers->fScalerEvent->fTSCounter.fNegativeHelicityScaler   += fScalers[540];

   /* next are tracker scalers followed by vacant  */

   fSANEScalers->fScalerEvent->fDeltaT = fSANEScalers->fScalerEvent->TimeLapsed2();
   fTime += fSANEScalers->fScalerEvent->fDeltaT;
   fSANEScalers->fScalerEvent->fTime = fTime;

   /*
     fSANEScalers->fScalerEvent->fHMS1Scaler     = (fScalers[506]);
     fSANEScalers->fScalerEvent->fBETA1Scaler    = (fScalers[507]);
     fSANEScalers->fScalerEvent->fPi0Scaler      = (fScalers[508]);
     fSANEScalers->fScalerEvent->fBETA2Scaler   = (fScalers[509]);
     fSANEScalers->fScalerEvent->fCoinScaler     = (fScalers[510]);
     fSANEScalers->fScalerEvent->fTSOutScaler    = (fScalers[511]);

     fSANEScalers->fScalerEvent->fBCM1NegativeHelicityScaler   = (fScalers[512]);
     fSANEScalers->fScalerEvent->fBCM1PositiveHelicityScaler   = (fScalers[513]);

     fSANEScalers->fScalerEvent->fBETA2NegativeHelicityScaler   = (fScalers[537]);
     fSANEScalers->fScalerEvent->fBETA2PositiveHelicityScaler =
         fSANEScalers->fScalerEvent->fBETA2Scaler*0.985 - fSANEScalers->fScalerEvent->fBETA2NegativeHelicityScaler;
     fSANEScalers->fScalerEvent->fCoinNegativeHelicityScaler   = (fScalers[538]);

     fSANEScalers->fScalerEvent->fTSOutNegativeHelicityScaler   = (fScalers[539]);
     fSANEScalers->fScalerEvent->fTSOutPositiveHelicityScaler =
         fSANEScalers->fScalerEvent->fTSOutScaler*0.985 -  fSANEScalers->fScalerEvent->fTSOutNegativeHelicityScaler;

     fSANEScalers->fScalerEvent->fHMS2NegativeHelicityScaler   = (fScalers[540]);
     fSANEScalers->fScalerEvent->fHMS2Scaler = fScalers[549];
     fSANEScalers->fScalerEvent->fCoinNegativeHelicityScaler   = (fScalers[538]);
   */

   // new way to do scalers






//   fSANEScalers->fScalerEvent->fPi0Counter.fScaler    = (fScalers[278]);
//   fSANEScalers->fScalerEvent->fCoinCounter.fScaler    = (fScalers[280]);
//   fSANEScalers->fScalerEvent->fHMS1Counter.fScaler    = (fScalers[276]);
//   fSANEScalers->fScalerEvent->fHMS2Counter.fScaler    = (fScalers[319]);
// //  fSANEScalers->fScalerEvent->fLEDCounter.fScaler    = (fScalers[278]);
//   fSANEScalers->fScalerEvent->fTSCounter.fScaler    = (fScalers[281]);



   for (int i = 0; i < 224 ; i++)
      fSANEScalers->fScalerEvent->fBigcalScalers[i] = (fScalers[230+i]);

   for (int i = 0; i < 12 ; i++)
      fSANEScalers->fScalerEvent->fCherenkovScalers[i] = (fScalers[518+i]);

   for (int i = 0; i < 32 ; i++)
      fSANEScalers->fScalerEvent->fTrackerScalers[i] = (fScalers[550+i]);


   fRun->fNumberOfScalerReads++;
   scalerTree->Fill();
   return kTRUE;
}
//_____________________________________________________________________________

void AnalyzerScalersToInSANE::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.

}
//_____________________________________________________________________________

void AnalyzerScalersToInSANE::Terminate()
{
   if (InSANERunManager::GetRunManager()->GetVerbosity() > 0) printf(" o AnalyzerScalersToInSANE::Terminate()  ");
   //fRun->Write();

   InSANERunManager::GetRunManager()->WriteRun();

   InSANERunManager::GetRunManager()->GetScalerFile()->cd();

   /*   scalerTree->Write();*/
   //scalerTree->FlushBaskets();
   scalerTree->Write("", TObject::kOverwrite);

   if (aFile) aFile->Write();
   //fFileOut->Close();
   //if(aFile) aFile->Flush();

   // The Terminate() function is the last function to be called during
   // a query. It always runs on the client, it can be used to present
   // the results graphically or save the results to file.s
}
//_____________________________________________________________________________
