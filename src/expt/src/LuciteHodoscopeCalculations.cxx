#include "LuciteHodoscopeCalculations.h"
#include "InSANECorrection.h"
#include "SANEAnalysisManager.h"

ClassImp(LuciteHodoscopeCalculation1)

//______________________________________________________________________________
Int_t LuciteHodoscopeCalculation1::Calculate() {

   if (!(fEvents->TRIG->IsClusterable()))  return(0);
   //std::cout << fEvents->BETA->fLuciteHodoscopeEvent->fNumberOfHits << " tdc hits\n";


   /// \todo Figure out a better place to define/reference this
   Int_t fReferenceBar = 25;
   auto * sumRefTiming =
      (InSANEDetectorTiming*) fHodoscopeDetector->GetDetectorSumTiming(fReferenceBar);
   //Double_t T0 = sumRefTiming->fTDCPeak;

   // copy the array, clear it, then re-fill it
   //if (fCopiedArray) delete fCopiedArray;
   //fCopiedArray = (TClonesArray*) fLucHits->Clone("tempLuciteHits");
   if(!fCopiedArray) fCopiedArray = new TClonesArray("LuciteHodoscopeHit",10);
   fCopiedArray->Clear();
   TClonesArray &aCopy = (*fCopiedArray);

   // Manually copy it.
   for (int kk = 0; kk < fLucHits->GetEntries() ; kk++) {
      aLucHit = (LuciteHodoscopeHit*)(*fLucHits)[kk];
      new(aCopy[kk]) LuciteHodoscopeHit( *aLucHit );
   }

   fLucHits->Clear("");
   aBetaEvent->fLuciteHodoscopeEvent->fNumberOfTDCHits = 0;
   aBetaEvent->fLuciteHodoscopeEvent->fNumberOfTimedTDCHits = 0;
   int numberKept = 0;
   /// Now refill the array with "roughly" good hits
   for (int kk = 0; kk < aCopy.GetEntries() ; kk++) {
      /*      aLucHit = (LuciteHodoscopeHit*)(*fLucHits)[kk] ;*/
      aLucHit = (LuciteHodoscopeHit*)(aCopy)[kk] ;
      if (aLucHit->fLevel == 1) {
         /// Count all TDC hits
         aBetaEvent->fLuciteHodoscopeEvent->fNumberOfTDCHits++;
         /// AND if there  is a one sided timed tdc hit (with a realtively wide cut)
         if (fHodoscopeDetector->cTDCMin < aLucHit->fTDC && fHodoscopeDetector->cTDCMax > aLucHit->fTDC) {
            /// Then we keep this hit in the array for later  use.
            aBetaEvent->fLuciteHodoscopeEvent->fNumberOfTimedTDCHits++;
            aLucHit->fPassesTimingCut = false;
            if (fHodoscopeDetector->cTDCAlignMin < aLucHit->fTDCAlign && fHodoscopeDetector->cTDCAlignMax > aLucHit->fTDCAlign)
               aLucHit->fPassesTimingCut = true;
            new((*fLucHits)[numberKept]) LuciteHodoscopeHit(*aLucHit);
            numberKept++;
         }
      } // end level = 1
   } // End loop over Lucite signals

   auto * sumTimingRef =
            (InSANEDetectorTiming*) fHodoscopeDetector->GetDetectorSumTiming(fHodoscopeDetector->fRefBar);
   //InSANEDetectorTiming * diffTimingRef =
   //         (InSANEDetectorTiming*) fHodoscopeDetector->GetDetectorDiffTiming(fHodoscopeDetector->fRefBar);
   auto * posTimingRef = 
            (InSANEDetectorTiming*) fHodoscopeDetector->GetDetectorTiming(2*fHodoscopeDetector->fRefBar+1);
   // Now create position hits using the detector and event objects (not another hit loop)
   fNumberOfPositions = 0;
   LuciteHodoscopePositionHit * aPositionHit;
   for (int kk = 0; kk < 28; kk++) {
      //   std::cout << " difference array size for bar " << kk << " is " <<  fHodoscopeDetector->fTDCHitDifferences[kk]->size() << "\n";
      for (unsigned int i = 0; i < fHodoscopeDetector->fTDCHitDifferences[kk]->size(); i++) {

         // Get the sum and difference timings
         auto * sumTiming =
            (InSANEDetectorTiming*) fHodoscopeDetector->GetDetectorSumTiming(kk);
         //InSANEDetectorTiming * diffTiming =
         //   (InSANEDetectorTiming*) fHodoscopeDetector->GetDetectorDiffTiming(kk);
         auto * posTiming = 
            (InSANEDetectorTiming*) fHodoscopeDetector->GetDetectorTiming(2*kk+1);

         // Create a new luc-PositionHit
         aPositionHit =
            new((*(aBetaEvent->fLuciteHodoscopeEvent->fLucitePositionHits))[fNumberOfPositions]) LuciteHodoscopePositionHit();
         aPositionHit->fBarNumber = kk + 1;
         aPositionHit->fHitNumber = fNumberOfPositions;
         aPositionHit->fTDCSum    =
            fHodoscopeDetector->fTDCHitSums[kk]->at(i) - sumTiming->fTDCPeak /*+ fHodoscopeDetector->fTDCSumOffset*/;
         
         // This seems to work pretty good : 
         aPositionHit->fTDCDifference = fHodoscopeDetector->fTDCHitDifferences[kk]->at(i)
                                      - 2.0*(posTiming->fTDCPeak - posTimingRef->fTDCPeak)
                                      + (sumTiming->fTDCPeak - sumTimingRef->fTDCPeak)
                                      + fHodoscopeDetector->fTDCDiffOffsets[kk];
//             fHodoscopeDetector->fTDCHitDifferences[kk]->at(i)/* - diffTiming->fTDCPeak*/ + fHodoscopeDetector->fTDCDiffOffsets[kk];

         aPositionHit->fNNeighborHits = 0;
         if (kk - 1 >= 0)
            if (fHodoscopeDetector->fTDCDifferenceNumberPassingCut[kk-1]->size() > 0)
               aPositionHit->fNNeighborHits =
                  fHodoscopeDetector->fTDCDifferenceNumberPassingCut[kk-1]->size() ;
         if (kk + 1 < 28)
            if (fHodoscopeDetector->fTDCDifferenceNumberPassingCut[kk+1]->size() > 0)
               aPositionHit->fNNeighborHits +=
                  fHodoscopeDetector->fTDCDifferenceNumberPassingCut[kk+1]->size() ;

         if (fHodoscopeDetector->PositionHitPassesTimingCut(aPositionHit)) aPositionHit->fPassesTimingCut = true;
         else aPositionHit->fPassesTimingCut = false;


         // Set the positions using the bar and TDCDiff
         // Note: these are in BigcalCoordinates use GetRotatedPosition(LuciteHodoscopePositionHit*) to get
         // position in human coordinates
         aPositionHit->fPositionVector.SetXYZ(
            fHodoscopeDetector->GetXPosition(int(aPositionHit->fTDCDifference)),
            fHodoscopeDetector->GetYPosition(aPositionHit->fBarNumber),
            fHodoscopeDetector->fGeoCalc->fLuciteZPosition);

         /*         aPositionHit->fPositionVector.SetXYZ(fHodoscopeDetector->fBigcalXPositions[kk]->at(i),fHodoscopeDetector->fBigcalYPositions[kk]->at(i),fHodoscopeDetector->fGeoCalc->fLuciteZPosition);*/
         fNumberOfPositions++;
      }
   }
   aBetaEvent->fLuciteHodoscopeEvent->fNumberOfPositionHits = fNumberOfPositions;
   //std::cout << " TDC position HITs done\n";
   //aBetaEvent->fLuciteHodoscopeEvent->fLucitePositionHits->Sort();
   return(0);
}

//______________________________________________________________________________
ClassImp(LuciteHodoscopeCalculation2)

//______________________________________________________________________________
Int_t LuciteHodoscopeCalculation2::Calculate() {

   if (!(fEvents->TRIG->IsBETAEvent()))  return(0);

   /// Now create position hits using the detector and event objects (not another hit loop)
   //fNumberOfPositions = 0;
   LuciteHodoscopePositionHit * aPositionHit;
   for (int i = 0; i < aBetaEvent->fLuciteHodoscopeEvent->fLucitePositionHits->GetEntries(); i++) {
      aPositionHit =
         (LuciteHodoscopePositionHit*)(*(aBetaEvent->fLuciteHodoscopeEvent->fLucitePositionHits))[i];

      /// Sets the data member of the PositionHit
      /// See LuciteHodoscopeDetector::SetPositionOfHit(LuciteHodoscopeHit*)
      /*            fHodoscopeDetector->SetPositionOfHit(aPositionHit)*/
   } // End loop over Lucite signals*/

   return(0);
}

//______________________________________________________________________________
ClassImp(LuciteHodoscopeCalculation3)

//______________________________________________________________________________
Int_t LuciteHodoscopeCalculation3::Calculate()
{
   if (!(fEvents->TRIG->IsBETAEvent()))  return(0);

   LuciteHodoscopePositionHit * aPositionHit;

   for (int kk = 0; kk < fEvents->BETA->fLuciteHodoscopeEvent->fLucitePositionHits->GetEntries(); kk++) {
      aPositionHit = (LuciteHodoscopePositionHit*)(*(aBetaEvent->fLuciteHodoscopeEvent->fLucitePositionHits))[kk] ;
      if (aPositionHit->fPositionVector.y() - 1 >= 0 &&
          aPositionHit->fPositionVector.y() - 1 < 28) {
         /*       aPositionHit->fTDCDifference =
                   aPositionHit->fTDCDifference - fHodoscopeDetector->fPositionCalibration->fConstants->at((Int_t)aPositionHit->fPositionVector.y()-1);
                 InSANECalibration * aCalib = ((InSANECalibration*)fHodoscopeDetector->fPositionCalibration->fCalibrations.At((Int_t)aPositionHit->fBarNumber-1));
                 TF1 * func = &(aCalib->fFunction );
            aPositionHit->fPositionVector.SetX(func->Eval(aPositionHit->fTDCDifference));
            */
      }
//    for (int kk=0; kk< 28;kk++) {
//       for(int i = 0; i < fHodoscopeDetector->fTDCHitDifferences[kk]->size(); i++) {
//          aPositionHit = new( (*)[fNumberOfPositions]) LuciteHodoscopePositionHit();
//          aPositionHit->fHitNumber = fNumberOfPositions;
//          aPositionHit->fTDCSum = fHodoscopeDetector->fTDCHitSums[kk]->at(i);
//          aPositionHit->fTDCDifference = fHodoscopeDetector->fTDCHitDifferences[kk]->at(i);
//          aPositionHit->fPositionVector.SetXYZ(aPositionHit->fTDCDifference,kk,300.0);
//          fNumberOfPositions++;
//       }
//    }

   } // End loop over Lucite signals
   return(0);
}

ClassImp(LuciteHodoscopeCalculation4)


