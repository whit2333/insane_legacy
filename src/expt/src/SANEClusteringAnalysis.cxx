#include "SANEClusteringAnalysis.h"
#include "SANEEvents.h"
#include "GasCherenkovHit.h"
#include "TF1.h"
#include "BETAPedestalAnalysis.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "SANERunManager.h"
#include "InSANEDatabaseManager.h"
#include "InSANERun.h"
#include "TClonesArray.h"
#include "BIGCALClusterProcessor.h"
#include "TH2F.h"
#include "InSANEEventDisplay.h"
#include "LuciteGeometryCalculator.h"
#include "ForwardTrackerHit.h"
#include "InSANEEventDisplay.h"
#include "TStopwatch.h"
#include "THStack.h"
#include "TPaveText.h"
#include "TList.h"
#include "InSANEDetectorAnalysis.h"

ClassImp(SANEClusteringAnalysis)
//_________________________________________________________//

SANEClusteringAnalysis::SANEClusteringAnalysis(const char * sourceTreeName) :
   InSANEDetectorAnalysis(sourceTreeName)
{

   fClusterTree = nullptr;

/// Creates the cluster event object and cluster array
   fClusterEvent = fEvents->AddClusterEvents();
   fClusters = fClusterEvent->fClusters;

/// The following is now taken care of by SANEEvents::AddClusterEvents()
//   fClusters = new TClonesArray("BIGCALCluster",10);
//   fClusterEvent = new InSANEClusterEvent();
//  fClusterEvent = fEvents->SetClusterBranches();
//  fClusters = fClusterEvent->fClusters;
//  fClusterEvent->fClusterSourceHistogram = fClusterProcessor->fSourceHistogram;
}
//_________________________________________________________//

SANEClusteringAnalysis::~SANEClusteringAnalysis()
{
   std::cout << " o SANEClusteringAnalysis Destructor \n";

   /* Moved all writing to Analyzer
      if(fOutputTree) {
         fOutputTree->Write(fOutputTree->GetName(),kWriteDelete);
         fOutputTree->FlushBaskets();
      }
      if(fAnalysisFile){
         fAnalysisFile->Flush();
      }
      if(fOutputFile){
        fOutputFile->Flush();
        fOutputFile->Write();
      }
      */
}
//_________________________________________________________//
// Int_t SANEClusteringAnalysis::SetClusterTree(TTree * t) {
//   if(fOutputFile)fOutputFile->cd();
//   if(!t) {printf("Null tree! Aborting from SANEClusteringAnalysis::SetClusterTree(TTree*) \n "); return(-1);}
//   fOutputTree=t;
//   fClusterTree=fOutputTree;
//   fOutputTree->Branch("bigcalClusterEvent","InSANEClusterEvent",&fClusterEvent );
// //     fOutputTree->Branch("LucitePedestalArray",&fLucitePeds);
//   fOutputTree->Branch("bigcalClusters",&fClusters);
//   fOutputTree->SetAutoSave();
// /*  fEvents->fTree->AddFriend(fOutputTree,Form("data/rootfiles/InSANE%d.%d.root",fRunManager->fRunNumber,1));*/
// /*  fOutputTree->AddFriend(fEvents->fTree,Form("data/rootfiles/InSANE%d.%d.root",fRunManager->fRunNumber,0));*/
//   return(0);
// }
//_________________________________________________________//

Int_t SANEClusteringAnalysis::AnalyzeRun()
{

   auto * clusterStopwatch = new TStopwatch();
   auto * eventStopwatch = new TStopwatch();
   auto * readStopwatch = new TStopwatch();
   clusterStopwatch->Reset();
   eventStopwatch->Reset();
   readStopwatch->Reset();

   if (!fOutputTree) SetClusterTree(new TTree("Clusters", "Cluster Data"));

   if (!fEvents->fTree) {
      std::cout << "error no tree!!!\n";
      return(-1);
   }

// Define HISTOGRAMS
   fAnalysisFile->cd();
   fAnalysisFile->mkdir("eventdisplay");
   fAnalysisFile->cd("eventdisplay");
// Histograms for EVENT Display
   //TH1F * h1; // generic pointer
   auto * cerhits = new TH2F("cherenkovHits", "cherenkov", 2, 1, 3, 4, 1, 5);
   auto * luchits = new TH2F("luciteHits", "Hodoscope", 2, 1, 3, 28, 1, 29);
   auto * bchits = new TH2F("bigcalHits", "Bigcal", 32, 1, 33, 56, 1, 57);
   auto * trackhitsy = new TH2F("trackerHitsy", "Tracker y", 1, 1, 2, 132, 1, 133);
   auto * trackhitsx = new TH2F("trackerHitsx", "Tracker x", 72, 1, 73, 1, 1, 2);
// Histograms for entire RUN
   //TH1F * hcerGoodClusters = new TH1F("hcerGoodClusters","Cherenkov with good clusters",200,0,12000);

   auto * hcerPassesTimingADC = new THStack("hcerPassesTimingADC", "Cherenkov ADCs with ever tighter Timing window");
   TH1F * hcerPassesTimingADCList[10];

   for (int i = 0; i < 10; i++) {
      hcerPassesTimingADCList[i] = new TH1F(Form("hcerPassesTimingADC%d", i), Form("Cherenkov ADCs with %d #mu", i), 200, 0, 8000);
      hcerPassesTimingADCList[i]->SetFillColor(kRed - 5 + i);
      ;
      hcerPassesTimingADC->Add(hcerPassesTimingADCList[i]);
   }
   fAnalysisFile->cd();
//

//      hs->Draw("nostack");


// EVENT DISPLAY
   InSANEEventDisplay * display = nullptr;
   TPaveText * clusteringResults = nullptr;
   if (fEventDisplay) {
      clusteringResults = new TPaveText(0, 0, 1, 1, "NDC");
      display = new InSANEEventDisplay();
      display->fDataHists->Add(fClusterProcessor->GetSourceHistogram());
      display->fDataHists->Add(bchits);
      display->fDataHists->Add(cerhits);
      display->fDataHists->Add(luchits);
      display->fDataHists->Add(trackhitsy);
      display->fDataHists->Add(trackhitsx);

      display->fSimulationHists->Add(bchits);
      display->fSimulationHists->Add(cerhits);
      display->fSimulationHists->Add(luchits);
      display->fSimulationHists->Add(trackhitsy);
      display->fSimulationHists->Add(trackhitsx);
   }

//   Useful Memory
   //GasCherenkovHit * aCerHit;
   LuciteHodoscopeHit * aLucHit;
   BigcalHit * aBigcalHit;
   ForwardTrackerHit* aTrackerHit;
   BIGCALCluster * aCluster;

   //TClonesArray * cerHits = fEvents->BETA->fGasCherenkovEvent->fGasCherenkovHits;
   TClonesArray * lucHits = fEvents->BETA->fLuciteHodoscopeEvent->fLuciteHits;
   TClonesArray * bigcalHits = fEvents->BETA->fBigcalEvent->fBigcalHits;
   TClonesArray * trackerHits = fEvents->BETA->fForwardTrackerEvent->fTrackerHits;
   //TClonesArray * clusterHits = fClusterEvent->fClusters;

   fCherenkovDetector->SetEventAddress(fEvents->BETA->fGasCherenkovEvent);

//_________________________________________________________//
//
// EVENT LOOP
//_________________________________________________________//
   Int_t nevent = fEvents->fTree->GetEntries();
   if (SANERunManager::GetRunManager()->fVerbosity > 1)  std::cout << nevent << " ENTRIES \n";

   for (int iEVENT = 0; iEVENT < nevent; iEVENT++)   {


      if (iEVENT % 1000 == 0) {
         if (SANERunManager::GetRunManager()->fVerbosity > 2) {
            std::cout << "Entry " << iEVENT << "\n";
            std::cout << "Average times: event=" << eventStopwatch->CpuTime()
                 << " cluster=" << clusterStopwatch->CpuTime()
                 << " read=" << readStopwatch->CpuTime()
                 << " clustertime=" << clusterStopwatch->CpuTime() / eventStopwatch->CpuTime()
                 << " readtime=" << readStopwatch->CpuTime() / eventStopwatch->CpuTime()
                 << "\n";

            fClusterProcessor->PrintTime();
         }
         eventStopwatch->Reset();
         clusterStopwatch->Reset();
         readStopwatch->Reset();
      }
      eventStopwatch->Start();

// RESETS
// reset histograms
      if (fEventDisplay) {
         cerhits->Reset();
         luchits->Reset();
         bchits->Reset();
         trackhitsy->Reset();
         trackhitsx->Reset();
      }
      fClusterEvent->ClearEvent("C");
      fEvents->BETA->ClearEvent("");
/// \todo should be BETADetector class for a fBETADetector->ClearAllDetectors();
      fCherenkovDetector->Clear();
      fBigcalDetector->Clear();

      readStopwatch->Start();
      fEvents->GetEvent(iEVENT);
      readStopwatch->Stop();
// END RESETS

// TRIGGER BIT
      /// \todo think of a better trigger bit representation
      if (fEvents->TRIG->IsClusterable()) {

// Timing for debugging
         clusterStopwatch->Start();

//////////////////////////
// CALORIMETER CLUSTERING
/// Currently Only One cluster per event!!!???
         fClusterEvent->fNClusters = fClusterProcessor->ProcessEvent(fClusters);
         fClusterEvent->fEventNumber = fEvents->BETA->fEventNumber;
         fClusterEvent->fRunNumber = fEvents->BETA->fRunNumber;
         clusterStopwatch->Stop();
// Post clustering Detector analysis
//        cout << "Hits: " << fEvents->BETA->fForwardTrackerEvent->fNumberOfHits << " "
//                         << fEvents->BETA->fGasCherenkovEvent->fNumberOfHits << " "
//                         << fEvents->BETA->fLuciteHodoscopeEvent->fNumberOfHits << " "
//                         << fEvents->BETA->fBigcalEvent->fNumberOfHits
//
//        << "\n";
//////////////////////////

// SHARE the cluster information



// GAS CHERENKOV LOOP (A) Hits Loop Outside Cluster loop
// fLevel=0 means ADC only. Use for mirror summing
// fLevel=1 means ADC and TDC
//     for (int kk=0; kk< fEvents->BETA->fGasCherenkovEvent->fNumberOfHits;kk++)
//     {
//       aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
//
// // Use only the hits with ADCs to sum all mirrors
//       if( aCerHit->fLevel == 0 && aCerHit->fMirrorNumber <= 8 ) {
//       fClusterEvent->fCherenkovTotalADC +=
//         aCerHit->fADC;
//       fClusterEvent->fCherenkovTotalNPE +=
//         aCerHit->fADC / fCherenkovDetector->f1PECalibration[ aCerHit->fMirrorNumber -1];
//
//       }
//
//       if( aCerHit->fLevel == 1 && aCerHit->fMirrorNumber <= 8 )
//         if(fEventDisplay)
//           cerhits->Fill(
//             fCherenkovDetector->fGeoCalc->iMirror(aCerHit->fMirrorNumber),
//             fCherenkovDetector->fGeoCalc->jMirror(aCerHit->fMirrorNumber)   );
// // Fill some histograms along the way
// // Level=1 hits are ADC and TDC HITS
//
//     }
//  END GAS CHERENKOV LOOP (A)
/////////////////////////
// BEGIN CLUSTER LOOP
// BIGCALCluster * aCluster;
//     for (int kk=0; kk< fClusterEvent->fNClusters ;kk++)
//     {
//       aCluster = (BIGCALCluster*)(*fClusters)[kk] ;
//
//      vector<Int_t> * mirrors = fCherenkovDetector->GetMirrorsFromHit(aCluster);
//     for (vector<Int_t>::iterator it = mirrors->begin(); it!=mirrors->end(); ++it) {
//     cout << *it << endl;
//     }
// // GAS CHERENKOV LOOP (B) Hits Loop inside Cluster loop
// // fLevel=0 means ADC only. Use for mirror summing
// // fLevel=1 means ADC and TDC
//     for (int kk=0; kk< fEvents->BETA->fGasCherenkovEvent->fNumberOfHits;kk++)
//     {
//       aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
//
// // Use the location of bigcal
// // to determine the BEST MIRROR SUM
//      if(aCerHit->fMirrorNumber <= 8 ) {
//
//       if( aCerHit->fLevel == 0 )
//       if( aCerHit->fPassesTiming[1] ) {
//
//         aCluster->fCherenkovBestADCSum +=
//           aCerHit->fADC;
//         aCluster->fCherenkovBestNPESum +=
//           (Double_t)aCerHit->fADC / fCherenkovDetector->f1PECalibration[ aCerHit->fMirrorNumber -1];
//       }
// // Fill some histograms along the way
// // Level=1 hits are ADC and TDC HITS
//       if( aCerHit->fLevel == 1 ) {
//        for(int k0=1;k0<10;k0++) {
//          if( !aCluster->fPassesTiming[k0])
//          if( aCerHit->fPassesTiming[k0]  )
//            aCluster->fPassesTiming[k0]=true;
//          }
//        }
//       }// mirrornumber <= 8
//
//     }
// //  END GAS CHERENKOV LOOP (B)
//
// // GAS CHERENKOV LOOP (C) Hits Loop inside Cluster loop
// // fLevel=0 means ADC only. Use for mirror summing
// // fLevel=1 means ADC and TDC
// //     for (int kk=0; kk< fEvents->BETA->fGasCherenkovEvent->fNumberOfHits;kk++)
// //     {
// //       if( aCerHit->fLevel == 1 ) {
// // fPassesTiming is a cut with a width 10*(tdc fit peak width)*[index]
//         for(int k0=9;k0>=0;k0--) {
//           if(aCluster->fPassesTiming[k0]) ((TH1F*)hcerPassesTimingADCList[k0])->Fill(aCluster->fCherenkovBestADCSum);
//         }
// //       }
// // }
// //  END GAS CHERENKOV LOOP (C)
// //
//
//
//     }
// END CLUSTER LOOP
/////////////////////////


         for (int kk = 0; kk < fEvents->BETA->fLuciteHodoscopeEvent->fNumberOfHits; kk++) {
            aLucHit = (LuciteHodoscopeHit*)(*lucHits)[kk] ;
            if (aLucHit->fTDC < -1)
               if (fEventDisplay)      {

                  luchits->Fill(fHodoscopeDetector->fGeoCalc->iBar(aLucHit->fPMTNumber), fHodoscopeDetector->fGeoCalc->jBar(aLucHit->fPMTNumber));
                  // (aLucHit->fPMTNumber-1)/2+1,aLucHit->fRow%28+1);
                  /*     cout << "luc bar" << aLucHit->fPMTNumber << "\n";
                       cout << "luc bar ij " << luciteGeo->iBar(aLucHit->fPMTNumber) << " " << luciteGeo->jBar(aLucHit->fPMTNumber) << "\n";*/
               }


//      ((TH1F*)(*fLucitePedHists)[aLucHit->fRow-1+28])->Fill(aLucHit->fPositiveADC);
         } // End loop over Lucite signals

         for (int kk = 0; kk < fEvents->BETA->fBigcalEvent->fNumberOfHits; kk++) {
            aBigcalHit = (BigcalHit*)(*bigcalHits)[kk] ;
            if (aBigcalHit->fTDCLevel == -1) {
               if (fEventDisplay)      {

                  bchits->Fill(aBigcalHit->fiCell, aBigcalHit->fjCell, aBigcalHit->fEnergy);
//     cout << "bc cell" << aBigcalHit->fiCell << " " << aBigcalHit->fjCell << "\n";
//     cout << "bc cell ij " << aBigcalHit->fEnergy << " " << aBigcalHit->fTDC << "\n";
               }
            }
//      ((TH1F*)(*fLucitePedHists)[aLucHit->fRow-1+28])->Fill(aLucHit->fPositiveADC);
         }

// Tracker
         for (int kk = 0; kk < fEvents->BETA->fForwardTrackerEvent->fNumberOfHits; kk++) {
            aTrackerHit = (ForwardTrackerHit*)(*trackerHits)[kk] ;
            if (fEventDisplay)      {
               if (aTrackerHit->fScintLayer == 0) {
                  trackhitsx->Fill(aTrackerHit->fRow, 1);
               } else if (aTrackerHit->fScintLayer == 2 || aTrackerHit->fScintLayer == 1) {
                  trackhitsy->Fill(1, aTrackerHit->fRow);
               }
//     cout << "tracker layer" << aTrackerHit->fScintLayer << " " << aTrackerHit->fTDC << "\n";
//     cout << "tracker row  " << aTrackerHit->fRow << " " << aTrackerHit->fPosition << "\n";
            }
         }
//      ((TH1F*)(*fLucitePedHists)[aLucHit->fRow-1+28])->Fill(aLucHit->fPositiveADC);

         fOutputTree->Fill();

         if (fEventDisplay) {
            clusteringResults->Clear();

            display->fCanvas->cd(1)->cd(2);
            for (int kk = 0; kk < fClusterEvent->fNClusters; kk++) {
               aCluster = (BIGCALCluster*)(*fClusters)[kk];
               clusteringResults->AddText(
                  Form("Cluster at %d %d \n", aCluster->fiPeak, aCluster->fjPeak)
               );
            }
            clusteringResults->Draw();

            display->UpdateDisplay();
         }

      } //trigger

//     for (int kk=0; kk< fEvents->BETA->fBigcalEvent->fNumberOfHits;kk++)
//     {
//       aBigcalHit = (BigcalHit*)(*bigcalHits)[kk] ;
//       if(aBigcalHit->fTDCLevel == -1) {
//         ((TH1F*)(*fBigcalPedHists)[bcgeo->GetBlockNumber(aBigcalHit->fiCell,aBigcalHit->fjCell)-1])->Fill(aBigcalHit->fADC);
//       }
//     }
//

      fClusters->Clear("C");

      eventStopwatch->Stop();


   } // END OF EVENT LOOP

   for (int i = 0; i < 10; i++) {
      hcerPassesTimingADC->Add(hcerPassesTimingADCList[i]);
   }
   hcerPassesTimingADC->Draw();
   hcerPassesTimingADC->Write();

   return(0);
}
//_________________________________________________________//



