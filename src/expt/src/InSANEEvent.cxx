#include  "InSANEEvent.h"


ClassImp(InSANEEvent)

//______________________________________________________________________________
InSANEEvent::InSANEEvent() {
   fEventNumber = 0;
   fRunNumber = 0;
}
//______________________________________________________________________________
InSANEEvent::~InSANEEvent() {
}
//______________________________________________________________________________
void  InSANEEvent::Print(Option_t * ) const {
   std::cout << " Run: " << fRunNumber << " , Event: " << fEventNumber << " \n";
}
//______________________________________________________________________________
void InSANEEvent::ClearEvent(Option_t * ) {
   fEventNumber = 0;
   fRunNumber = 0;
}
//______________________________________________________________________________

