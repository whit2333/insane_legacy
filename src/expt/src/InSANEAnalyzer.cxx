#include "InSANEAnalyzer.h"


ClassImp(InSANEAnalyzer)

//______________________________________________________________________________
InSANEAnalyzer::InSANEAnalyzer(const char * newTreeName, const char * uncorrectedTreeName)
   : fNumberOfCorrections(0), fNumberOfCalculations(0), fNumberOfScalerCalculations(0)
{
   SetName(newTreeName);

   fNPrintUpdate         = 50000;
   fCalculatedTree       = nullptr;
   fInputTree            = nullptr;
   fScalerTree           = nullptr;
   fOutputScalerTree     = nullptr;
   fSkimmer              = nullptr;
   fTotalTime            = 0;
   fFillTime             = 0;
   fSumOfTimes           = 0;
   fClearTime            = 0;
   fOtherTime            = 0;
   fPrintMem             = true;
   fSaveMem              = true;
   fMeasureTime          = true;
   fShowEventDisplay     = false;
   fCalculatedTreeName   = newTreeName;
   fInputTreeName        = uncorrectedTreeName;

   fScalerTreeName       = "Scalers";
   fOutputScalerTreeName = "NextScalers";

   //fEventDisplays        = new TList();
   //fCorrections          = new TList();
   //fScalerCalculations   = new TList();
   //fCalculations         = new TList();
   //fRunFlags             = new TList();
   //fDetectors            = new TList();

   fEventDisplays.SetOwner(true);
   fCorrections.SetOwner(true);
   //fScalerCalculations.SetOwner(true); // Cannot be the owner because scaler calcs are added to fCalculations too.
   fCalculations.SetOwner(true);
   fRunFlags.SetOwner(true);
   //fDetectors.SetOwner(true);

   fEventDisplays.Clear();
   fCorrections.Clear();
   fScalerCalculations.Clear();
   fCalculations.Clear();
   fRunFlags.Clear();
   fDetectors.Clear();

   fMemFileName = "mem_out.txt";
   fMemFile = nullptr;//new ofstream("mem_out.txt");

   fCalculationStopwatch.Reset();
   fStopwatch.Reset();

   if ((!SANERunManager::GetRunManager()->GetCurrentRun()->fPedestalsAnalyzed
        && !SANERunManager::GetRunManager()->GetCurrentRun()->fTimingAnalyzed)) {
      Error("InSANEAnalyzer","You need to analyze the pedestals and timing first!!!");
   }

}
//______________________________________________________________________________
InSANEAnalyzer::~InSANEAnalyzer() {

   if (SANERunManager::GetRunManager()->GetVerbosity() > 0) std::cout << " o InSANEAnalyzer destructor " << std::endl;

   SANERunManager::GetRunManager()->GetCurrentFile()->cd();
   SANERunManager::GetRunManager()->WriteRun();

   if (fCalculatedTree) fCalculatedTree->FlushBaskets();
   if (fCalculatedTree) fCalculatedTree->Write();

   SANERunManager::GetRunManager()->GetCurrentFile()->Write();

   //if(SANERunManager::GetRunManager()->fVerbosity>0)  printf("\n InSANEAnalyzer destructor \n");

   //fCorrections->Delete();
   //fCalculations->Delete();
   //fDetectors->Delete();

   //delete fEvents;
   //delete fCorrectedTree;
   //if(SANERunManager::GetRunManager()->GetVerbosity() > 2)  
   //printf(" o InSANEAnalyzer destructor END \n");
   if (SANERunManager::GetRunManager()->GetVerbosity() > 0) std::cout << " o InSANEAnalyzer destructor END" << std::endl;
}
//______________________________________________________________________________
void InSANEAnalyzer::SyncronizeEvents(Int_t firstscaler)
{
   // syncronizes the detector tree and the scaler tree
   if (fScalerTree) {
      fScalerTree->GetEntry(firstscaler);
      if (fOutputScalerTree) {
         if (firstscaler == 0) {
            fOutputScalerTree->Fill();
         }
      }
      /// set the initial scaler ... (not sure if the fill above is needed...)
      fLastScalerTriggerEvent = (*fScalerTriggerEvent);

      /// set the "next scaler"
      fScalerTree->GetEntry(firstscaler + 1);
      fNextScalerTriggerEvent = (*fScalerTriggerEvent);
   } else {
      std::cout << " InSANEAnalyzer::SyncronizeEvents has no scaler tree\n";
   }


   if (firstscaler == 0) {
      iDetectorEvent = 0;
      fInputTree->GetEntry(0);
   }

   if (fScalerTree) SeekToEvent(fLastScalerTriggerEvent.fEventNumber);

//   if(  fDetectorTriggerEvent->fEventNumber < fLastScalerTriggerEvent.fEventNumber)
//   while( fDetectorTriggerEvent->fEventNumber < fLastScalerTriggerEvent.fEventNumber ) {
   /*       std::cout << " - skipping up to event " << fDetectorTriggerEvent->fEventNumber << "\n";*/
//         iDetectorEvent++;
//         fInputTree->GetEntry(iDetectorEvent);
//   }

//  fLastScalerTriggerEvent.PrintEvent();
//  fNextScalerTriggerEvent.PrintEvent();
}
//______________________________________________________________________________
void InSANEAnalyzer::SeekToEvent(Int_t event)
{
   if (fDetectorTriggerEvent->fEventNumber < event)
      while (fDetectorTriggerEvent->fEventNumber < event) {
         fInputTree->GetEntry(iDetectorEvent);
         iDetectorEvent++;
         iEVENT++;
         //     std::cout << " skipping to event " << iDetectorEvent << "\n";
      }
}
//______________________________________________________________________________
Int_t InSANEAnalyzer::ProcessScalers()
{

   InSANECalculation * aCalculation;

   /// Make sure that the detector event number is less than the "next" scaler event number.
   /// if it is not, increment the scaler event and execute scaler
   if (fScalerTree)
      if (fDetectorTriggerEvent->fEventNumber > fNextScalerTriggerEvent.fEventNumber && !exitEventLoop) {

         /// Before incrementing the scaler trigger events, execute the calculations for
         /// the current scaler event.
         /// \NOTE The scaler event is describing the past events so the "scaler calculations"
         // scaler stuff here
         TListIter scIter(&fScalerCalculations);
         while ((aCalculation = (InSANECalculation*)scIter.Next())) {
            aCalculation->CalculateInterval();
            //((InSANEScalerCalculation*)aCalculation)->CalculateInterval();
         }

         if (fOutputScalerTree) {
            /*std::cout << " FILLING OUTPUT SCALER TREE!!!\n";*/
            fOutputScalerTree->Fill();
         }
         // end scaler stuff

         /// START LOOP HERE  THAT INCRMENTS
         /// THEN CHECKS THE FLAGS/FILTERS
         /// OTHERWISE LOOPING BACK TO INCREMENT.
         /// Ending WITH A SYNCRONIZATION

         int skipScalerEvent = 0;
         do {

            /// increment the scaler to get the next event number
            iScalerEvent++;

            //if( fScalerTree->GetEntry(iScalerEvent-1) == 0) exitEventLoop = true;
            fLastScalerTriggerEvent = (*fScalerTriggerEvent);

            /// Set the next scaler trigger-event
            if (fScalerTree->GetEntry(iScalerEvent) == 0) exitEventLoop = true;
            fNextScalerTriggerEvent = (*fScalerTriggerEvent);

            /// Process the skimmer flags
            if(fSkimmer) {
               fSkimmer->ProcessFlags();
               skipScalerEvent = fSkimmer->ProcessFilters();
               if( skipScalerEvent ) std::cout << " skipping events " << iScalerEvent << "\n";
            }

            iEVENT++;
         } while (skipScalerEvent != 0 && !exitEventLoop);

         /*   SeekToEvent(fLastScalerTriggerEvent.fEventNumber);*/
         nDetectorEvents = 0;

      }
   return(0);
}
//______________________________________________________________________________
void InSANEAnalyzer::Process()
{

   if (SANERunManager::GetRunManager()->fVerbosity > 2)
      std::cout << " = Procesing calculation in InSANEAnalyzer\n";

   fStopwatch.Start();
   unsigned int iTime = 0;

   InSANECorrection   * aCorrection;
   InSANECalculation  * aCalculation;
   InSANEDetector     * aDetector;
   InSANEEventDisplay * anEventDisplay;

   bool useScalers = true;
   if (!fScalerTree) {
      useScalers = false;
      std::cout << " x Scaler tree not found \n";
   }
   if (fScalerTree) if (fScalerTree->GetEntries() < 5) useScalers = false;

   if (useScalers) if (fSkimmer) fSkimmer->Initialize();

   if (SANERunManager::GetRunManager()->fVerbosity > 0) {
      printf(" - List of Corrections:\n");
      fCorrections.Print();
      TListIter aaaaaIter(&fCorrections);
      while ((aCorrection = (InSANECorrection*)aaaaaIter.Next())) {
         aCorrection->Describe();
      }
      printf(" - List of Calculations:\n");
      fCalculations.Print();
      TListIter bbbbIter(&fCalculations);
      while ((aCalculation = (InSANECalculation*)bbbbIter.Next())) {
         aCalculation->Describe();
      }
   }
   nentries = fInputTree->GetEntries();
   if (useScalers)nentries += fScalerTree->GetEntries();

   // Before entering the event loop, close all the database connections
   // because they are just left idle during the loop. 
   std::cout << " About to close db connections\n";
   InSANEDatabaseManager::GetManager()->CloseConnections();
   std::cout << " db connections closed.\n";

   /**  Start of Event LOOP
    *
    *    - Get total number of "scaler" and "event" events
    *    - Get the first scaler event. Get the second scaler event
    *
    */

   std::cout << " Syncronizing scalers ...\n";
   if (useScalers) SyncronizeEvents(); // queue up first scaler and the following det event
   std::cout << " Scalers syncronized\n";

   exitEventLoop   = false;
   iEVENT          = iDetectorEvent;
   iScalerEvent    = 1;
   nDetectorEvents = 0;

   Int_t NdetEvents = fInputTree->GetEntries() - iDetectorEvent;

   std::cout << " Entering Event Loop ... \n";

   if (fPrintMem) PrintMemoryUsage();

   while (nentries > iEVENT && !exitEventLoop && NdetEvents > nDetectorEvents) {

      if (iDetectorEvent % fNPrintUpdate == 0) { //if(SANERunManager::GetRunManager()->fVerbosity>0)
         std::cout << "Entry " << iEVENT << "\n";
         if (fPrintMem) PrintMemoryUsage();
      }
      /*     cout <<  " entries =" << nentries << " iEvent=" << iEVENT << "\n";  */

      fInputTree->GetEntry(iDetectorEvent);

      if (useScalers) ProcessScalers();

      iTime = 0;
      TListIter aIter(&fCorrections);
      while ((aCorrection = (InSANECorrection*)aIter.Next())) {
         if (fMeasureTime) fCalculationStopwatch.Start();
         aCorrection->Apply();
         if (fMeasureTime) {
            fCalculationStopwatch.Stop();
            if (iTime < fCorrectionTimes.size()) fCorrectionTimes[iTime] += fCalculationStopwatch.RealTime();
            iTime++;
         }
      }

      iTime = 0;
      TListIter dIter(&fDetectors);
      while ((aDetector = (InSANEDetector*)dIter.Next())) {
         if (fMeasureTime) fCalculationStopwatch.Start();
         aDetector->ProcessEvent();
         if (fMeasureTime) {
            fCalculationStopwatch.Stop();
            if (iTime < fDetectorTimes.size()) fDetectorTimes[iTime] += fCalculationStopwatch.RealTime();
            iTime++;
         }

      }

      iTime = 0;
      TListIter bIter(&fCalculations);
      while ((aCalculation = (InSANECalculation*)bIter.Next())) {
         if (fMeasureTime) fCalculationStopwatch.Start();
         aCalculation->Calculate();
         if (fMeasureTime) {
            fCalculationStopwatch.Stop();
            if (iTime < fCalculationTimes.size()) fCalculationTimes[iTime] += fCalculationStopwatch.RealTime();
            iTime++;
         }
      }

      if (fShowEventDisplay)
         if (iEVENT > 1000) {
            TListIter dispIter(&fEventDisplays);
            while ((anEventDisplay = (InSANEEventDisplay*)dispIter.Next())) {
               anEventDisplay->UpdateSimpleDisplay();
            }
         }

      // Fill the trees
      if (fMeasureTime) fCalculationStopwatch.Start();
      TListIter eIter(&fCalculations);
      while ((aCalculation = (InSANECalculation*)eIter.Next())) {
         aCalculation->FillTree();
      }
      this->FillTrees();

      if (fMeasureTime) {
         fCalculationStopwatch.Stop();
         fFillTime += fCalculationStopwatch.RealTime();
      }

      // Clear the event
      if (fMeasureTime) fCalculationStopwatch.Start();
      ClearEvents();
      TListIter cIter(&fDetectors);
      while ((aDetector = (InSANEDetector*)cIter.Next())) {
         aDetector->ClearEvent();
      }
      if (fMeasureTime) {
         fCalculationStopwatch.Stop();
         fClearTime += fCalculationStopwatch.RealTime();
      }

      nDetectorEvents++;
      iDetectorEvent++;
      iEVENT++;
   }// END OF EVENT LOOP

   // ---------------------------------------------------------------------
   // End of run analyses

   if (fPrintMem) PrintMemoryUsage();

   TListIter adetIter(&fDetectors);
   while ((aDetector = (InSANEDetector*)adetIter.Next())) {
      aDetector->PostProcessAction();
   }

   if (fPrintMem) PrintMemoryUsage();

   TListIter finalizeCorrIter(&fCorrections);
   while ((aCorrection = (InSANECorrection*)finalizeCorrIter.Next())) {
      aCorrection->Finalize();
   }

   TListIter finalizeIter(&fCalculations);
   while ((aCalculation = (InSANECalculation*)finalizeIter.Next())) {
      aCalculation->Finalize();
   }

   if (fPrintMem) PrintMemoryUsage();

   this->MakePlots();

   if (fPrintMem) PrintMemoryUsage();

   /// Process list of scripts
   ProcessScripts();

   //std::cout << " o Done Processing Scripts. \n";

   if (fPrintMem) PrintMemoryUsage();

   TListIter aIter(&fCorrections);
   while ((aCorrection = (InSANECorrection*)aIter.Next())) {
      aCorrection->MakePlots();
   }
   //std::cout << " o Done with correction's MakePlots(). \n";

   if (fPrintMem) PrintMemoryUsage();

   TListIter eIter(&fCalculations);
   while ((aCalculation = (InSANECalculation*)eIter.Next())) {
      aCalculation->MakePlots();
   }
   //std::cout << " o Done with Calculation's MakePlots(). \n";


}
//______________________________________________________________________________
void InSANEAnalyzer::InitCorrections() {
   Int_t runNumber = SANERunManager::GetRunManager()->GetCurrentRun()->GetRunNumber();
   if(fSaveMem) {
      fMemFile = new std::ofstream(Form("log/%d/%s",runNumber,fMemFileName.Data()));
   }
   if (SANERunManager::GetRunManager()->fVerbosity > 1) {
      printf(" - List of Corrections:\n");
      fCorrections.Print();
   }

   TListIter aIter(&fCorrections);
   while (auto * aCorrection = (InSANECorrection *)aIter.Next()) {
      if (!(aCorrection->IsInitialized())) {
         if( aCorrection->Initialize() ) {
            Error("InitCorrections"," Failed to initialize correction, %s",aCorrection->GetName());    
            std::cout << "Removing correction " << aCorrection->GetName() << "\n";
            fCorrections.Remove(aCorrection);
            fCorrections.Print(); 
         }
      }
      //aCorrection->Initialize();
   }

}
//______________________________________________________________________________
void InSANEAnalyzer::InitCalculations()
{
   if (SANERunManager::GetRunManager()->fVerbosity > 1) {
      printf(" - List of Calculations:\n");
      fCalculations.Print();
      fScalerCalculations.Print();
   }
   InSANECalculation * aCalculation = nullptr;
   TListIter bIter(&fCalculations);
   while ( (aCalculation = (InSANECalculation *)bIter.Next()) ) {
      if (!(aCalculation->IsInitialized())) {
         if( aCalculation->Initialize() ) {
            Error("InitCalculations"," Failed to initialize calculation, %s",aCalculation->GetName());    
            std::cout << "Removing calculation " << aCalculation->GetName() << "\n";
            fCalculations.Remove(aCalculation);
            fCalculations.Print(); 
         }
      }
   }

   TListIter scIter(&fScalerCalculations);
   while ((aCalculation = (InSANECalculation*)scIter.Next())) {
      if (!(aCalculation->IsInitialized()))aCalculation->Initialize();
   }
}
//______________________________________________________________________________
void InSANEAnalyzer::process_mem_usage(double& vm_usage, double& resident_set)
{
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage     = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat", ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
               >> utime >> stime >> cutime >> cstime >> priority >> nice
               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage     = vsize / 1024.0;
   resident_set = rss * page_size_kb;
}
//______________________________________________________________________________
void InSANEAnalyzer::PrintMemoryUsage() {
   process_mem_usage(virtMem, resMem);
   std::cout << " o Virt = " << virtMem/1024.0 << " MB, Res = " << resMem/1024.0 << " MB\n";
   if (fMeasureTime) PrintTimeUsage();
   if(fMemFile) {
      (*fMemFile) << resMem/1024.0 << std::endl;
   } 
}
//______________________________________________________________________________
void InSANEAnalyzer::PrintTimeUsage()
{
   fStopwatch.Stop();
   fTotalTime += fStopwatch.RealTime();
   fStopwatch.Start();
   unsigned int iTime = 0;
   fSumOfTimes = 0;
   Double_t aTime = 0;

   std::cout << std::setfill('_') << std::setw(40) << "Time Usage" << std::setfill('_') << std::setw(40) << "\n";
   //Tree fill time
   aTime = fFillTime;
   fSumOfTimes += aTime;
   std::cout.precision(3);
   std::cout <<  std::showpoint;
   std::cout <<  std::fixed;
   std::cout <<  std::setfill(' ')
             << std::setw(40) << "Tree filling"
             << '\t' << std::left << aTime
             << '\t' << "s"
             << ' ' << "("
             << ' ' << std::right << 100.0 *(aTime / (fTotalTime + 0.00000001))
             << '\t' << "%)\n";
   // Event Clear time
   aTime = fClearTime;
   fSumOfTimes += aTime;
   std::cout << std::setw(40) << "Event clearing"
             << '\t' << std::left << aTime
             << '\t' << "s"
             << ' ' << "("
             << ' ' << std::right << 100.0 *(aTime / (fTotalTime + 0.00000001))
             << '\t' << "%)\n";
   // First the detectors
   std::cout <<  std::setfill(' ') << " Detectors: \n";
   InSANEDetector* aDetector = nullptr;
   TListIter dIter(&fDetectors);
   iTime = 0;
   while ((aDetector = (InSANEDetector*)dIter.Next())) {
      if (iTime < fDetectorTimes.size()) {
         aTime = fDetectorTimes[iTime];
         fSumOfTimes += aTime;
         std::cout << std::setw(40) << aDetector->GetName()
                   << '\t' << std::left << aTime
                   << '\t' << "s"
                   << ' ' << "("
                   << ' ' << std::right << 100.0 *(aTime / (fTotalTime + 0.00000001))
                   << '\t' << "%)\n";
         iTime++;
      }
   }
   // Then the corrections
   std::cout << " Corrections: \n";
   InSANECorrection* aCorrection = nullptr;
   TListIter cIter(&fCorrections);
   iTime = 0;
   while ((aCorrection = (InSANECorrection*)cIter.Next())) {
      if (iTime < fCorrectionTimes.size()) {
         aTime = fCorrectionTimes[iTime];
         fSumOfTimes += aTime;
         std::cout <<  std::setw(40) << aCorrection->GetName()
                   << '\t' << std::left << aTime
                   << '\t' << "s"
                   << ' ' << "("
                   << ' ' << std::right << 100.0 *(aTime / (fTotalTime + 0.00000001))
                   << '\t' << "%)\n";
         iTime++;
      }
   }
   // Then the calculations
   std::cout << " Calculations: \n";
   InSANECalculation* aCalculation = nullptr;
   TListIter bIter(&fCalculations);
   iTime = 0;
   while ((aCalculation = (InSANECalculation*)bIter.Next())) {
      if (iTime < fCalculationTimes.size()) {
         aTime = fCalculationTimes[iTime];
         fSumOfTimes += aTime;
         std::cout << std::setw(40) << aCalculation->GetName()
                   << '\t' << std::left << aTime
                   << '\t' << "s"
                   << ' ' << "("
                   << ' ' << std::right << 100.0 *(aTime / (fTotalTime + 0.00000001))
                   << '\t' << "%)\n";
         iTime++;
      }
   }
   //Sum of all the times measured
   fOtherTime = fTotalTime - fSumOfTimes;
   aTime = fOtherTime;
   std::cout <<  std::setfill(' ') << std::setw(40) << "Other"
             << '\t' << std::left << aTime
             << '\t' << "s"
             << ' ' << "("
             << ' ' << std::right << 100.0 *(aTime / (fTotalTime + 0.00000001))
             << '\t' << "%)\n";
   //total time
   aTime = fTotalTime;
   std::cout <<  std::setfill(' ') << std::setw(40) << "Total"
             << '\t' << std::left << aTime
             << '\t' << "s"
             << ' ' << "("
             << ' ' << std::right << 100.0 *(aTime / (fTotalTime + 0.00000001))
             << '\t' << "%)\n";
   //total time
   aTime = fSumOfTimes;
   std::cout <<  std::setfill(' ') << std::setw(40) << "Measured"
             << '\t' << std::left << aTime
             << '\t' << "s"
             << ' ' << "("
             << ' ' << std::right << 100.0 *(aTime / (fTotalTime + 0.00000001))
             << '\t' << "%)\n";
}
//______________________________________________________________________________
void InSANEAnalyzer::AddCorrection(InSANECorrection * cor) {
   fCorrections.Add(cor);
   fCorrectionTimes.push_back(0.0);
   fNumberOfCorrections++;
}
//______________________________________________________________________________
void InSANEAnalyzer::AddCalculation(InSANECalculation * calc) {
   fCalculations.Add(calc);
   fCalculationTimes.push_back(0.0);
   fNumberOfCalculations++;
}
//______________________________________________________________________________
void InSANEAnalyzer::AddScalerCalculation(InSANECalculation * calc) {
   fScalerCalculations.Add(calc);
   fNumberOfScalerCalculations++;
   AddCalculation(calc);
}
//______________________________________________________________________________
void InSANEAnalyzer::AddDetector(InSANEDetector* det) {
   fDetectors.Add(det);
   fNumberOfDetectors++;
   fDetectorTimes.push_back(0.0);
   fClearTimes.push_back(0.0);
}
//______________________________________________________________________________
void InSANEAnalyzer::AddRunFlag(InSANERunFlag * flag) {
   fRunFlags.Add(flag);
   fNumberOfFlags++;
}
//______________________________________________________________________________
void InSANEAnalyzer::AddEventDisplay(InSANEEventDisplay * display) {
   if (display) {
      fEventDisplays.Add(display);
      fShowEventDisplay = true;
   }
}
//______________________________________________________________________________
void InSANEAnalyzer::AddScript(const char * file) {
   fAnalysisScripts.push_back(new TString(file));
}
//______________________________________________________________________________
void InSANEAnalyzer::ProcessScripts() {
   TString * aString = nullptr;

   for (unsigned int i = 0; i < fAnalysisScripts.size(); i++) {

      SANERunManager::GetRunManager()->GetCurrentFile()->cd();
      aString = (TString*)fAnalysisScripts.at(i);
      std::cout << " = Executing " << aString->Data() << "\n";
      gROOT->ProcessLine(
            Form(".x %s(%d) ",
               aString->Data(),
               SANERunManager::GetRunManager()->fCurrentRunNumber));
   }
}
//______________________________________________________________________________




