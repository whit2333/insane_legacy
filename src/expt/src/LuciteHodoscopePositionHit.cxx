#include  "LuciteHodoscopePositionHit.h"

ClassImp(LuciteHodoscopePositionHit)

bool LuciteHodoscopePositionHit::operator<(const LuciteHodoscopePositionHit &rhs) const {
   //return( fMissDistance < rhs.fMissDistance );
   return( fBarNumber < rhs.fBarNumber );
}

bool LuciteHodoscopePositionHit::operator>(const LuciteHodoscopePositionHit &rhs) const {
   return( fBarNumber > rhs.fBarNumber );
}

bool LuciteHodoscopePositionHit::operator<=(const LuciteHodoscopePositionHit &rhs) const {
   return( fBarNumber <= rhs.fBarNumber );
}

bool LuciteHodoscopePositionHit::operator>=(const LuciteHodoscopePositionHit &rhs) const {
   return( fBarNumber >= rhs.fBarNumber );
}
//______________________________________________________________________________
bool LuciteHodoscopePositionHit::operator==(const LuciteHodoscopePositionHit &rhs) const {
   if(fBarNumber != rhs.fBarNumber) return false;
   if(fHitNumber != rhs.fHitNumber) return false;
   return true;
}
//______________________________________________________________________________
bool LuciteHodoscopePositionHit::operator!=(const LuciteHodoscopePositionHit &rhs) const {
   return !(*this == rhs);
}
//______________________________________________________________________________
Int_t   LuciteHodoscopePositionHit::Compare(const TObject *obj) const {
   if ( (*this) > ((LuciteHodoscopePositionHit&)(*obj)))
      return 1;
   else if ( (*this) < ((LuciteHodoscopePositionHit&)(*obj)))
      return -1;
   else
      return 0;
}

//______________________________________________________________________________
LuciteHodoscopePositionHit::LuciteHodoscopePositionHit() {
   fTDCSum        = -9999.0;
   fTDCDifference = 0.0;
   fNNeighborHits = 0;
   fBarNumber     = 0;
}
//______________________________________________________________________________
LuciteHodoscopePositionHit::~LuciteHodoscopePositionHit() {
}
//______________________________________________________________________________
LuciteHodoscopePositionHit::LuciteHodoscopePositionHit(const LuciteHodoscopePositionHit& rhs) : InSANEDetectorHit(rhs) {
   (*this) = rhs;
}
//______________________________________________________________________________
void LuciteHodoscopePositionHit::Print(Option_t *) const {
   std::cout << " LuciteHodoscopePositionHit " << this << std::endl;
   std::cout << "    - fTDCSum  = " << fTDCSum << std::endl;
   std::cout << "    - fTDCDifference  = " << fTDCDifference << std::endl;
   std::cout << "    - fBarNumber  = " << fBarNumber << std::endl;
   std::cout << "    - fNNeighborHits  = " << fNNeighborHits << std::endl;
}
//______________________________________________________________________________
void  LuciteHodoscopePositionHit::Clear(Option_t *opt) {
   /*      if(InSANERunManager::GetRunManager()->fVerbosity > 2) Print();*/
   InSANEDetectorHit::Clear(opt);
   fPositionVector.SetXYZ(0.0, 0.0, 0.0);
   fTDCSum        = -9999.0;
   fTDCDifference = 0.0;
   fNNeighborHits = 0;
   fHitNumber     = 0;
   fBarNumber     = 0;
}
//______________________________________________________________________________
LuciteHodoscopePositionHit& LuciteHodoscopePositionHit::operator=(const LuciteHodoscopePositionHit &rhs) {
   // Check for self-assignment!
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   InSANEDetectorHit::operator=(rhs);
   // Deallocate, allocate new space, copy values...
   fTDCSum = rhs.fTDCSum;
   fTDCDifference = rhs.fTDCDifference;
   fNNeighborHits = rhs.fNNeighborHits;
   fPositionVector = rhs.fPositionVector;
   fHitNumber = rhs.fHitNumber;
   fBarNumber = rhs.fBarNumber;
   return *this;
}
//______________________________________________________________________________


