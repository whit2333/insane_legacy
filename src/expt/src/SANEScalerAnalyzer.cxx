#include "SANEScalerAnalyzer.h"
#include "SANERunManager.h"
#include "BETACalculation.h"
#include "GasCherenkovCalculations.h"
#include "BigcalClusteringCalculations.h"
#include "TFitResult.h"
#include "InSANEDatabaseManager.h"

ClassImp(SANEScalerAnalyzer)
//_______________________________________________________//

void SANEScalerAnalyzer::Initialize(TTree * inTree)
{
   fInputTree = inTree;

   SANERunManager::GetRunManager()->GetScalerFile()->cd();

   fCalculatedTree = fInputTree->CloneTree(0);
   fCalculatedTree->SetNameTitle(fCalculatedTreeName.Data(), Form("A corrected tree: %s", fCalculatedTreeName.Data()));

   InitCorrections();

   InitCalculations();

}
//_______________________________________________________//

void SANEScalerAnalyzer::MakePlots()
{

   fAnalysisFile->cd();

}


