#include "BETACorrection.h"
#include "InSANECorrection.h"


ClassImp(BETACorrection)

//______________________________________________________________________________
BETACorrection::BETACorrection(InSANEAnalysis * analysis) : InSANECorrection(analysis) {
   fCerHits               = nullptr;
   fLucHits               = nullptr;
   fBigcalHits            = nullptr;
   fBigcalTimingGroupHits = nullptr;
   fTrackerHits           = nullptr;
   fClusters              = nullptr;

   aBetaEvent             = nullptr;
   bcEvent                = nullptr;
   aBigcalHit             = nullptr;
   lhEvent                = nullptr;
   aLucHit                = nullptr;
   gcEvent                = nullptr;
   aCerHit                = nullptr;
   ftEvent                = nullptr;
   aTrackerHit            = nullptr;

   fEvents                = nullptr;

   fTrackerDetector       = nullptr;
   fCherenkovDetector     = nullptr;
   fHodoscopeDetector     = nullptr;
   fBigcalDetector        = nullptr;

   if(analysis) SetEvents(analysis->fEvents);
}
//______________________________________________________________________________
BETACorrection::~BETACorrection() {
}
//______________________________________________________________________________
Int_t BETACorrection::SetDetectors(InSANEDetectorAnalysis * anAnalysis) {
   fTrackerDetector   = anAnalysis->fTrackerDetector;
   fCherenkovDetector = anAnalysis->fCherenkovDetector;
   fHodoscopeDetector = anAnalysis->fHodoscopeDetector;
   fBigcalDetector    = anAnalysis->fBigcalDetector;
   return(0);
}
//______________________________________________________________________________
void BETACorrection::SetEvents(SANEEvents * e) {
   //std::cout << " o BETACorrection: Setting event addr: " << e << "\n";
   InSANECorrection::SetEvents(e);
   fEvents = e;
}
//______________________________________________________________________________
Int_t BETACorrection::Initialize() {
   //if(SANERunManager::GetRunManager()->fVerbosity>1)
   //   std::cout << " o BETACorrection::Initialize Setting event addr: " << fEvents << "\n";
   fEvents                = (SANEEvents*)fTransientDatum;
   aBetaEvent             = fEvents->BETA;
   bcEvent                = fEvents->BETA->fBigcalEvent;
   lhEvent                = fEvents->BETA->fLuciteHodoscopeEvent;
   gcEvent                = fEvents->BETA->fGasCherenkovEvent;
   ftEvent                = fEvents->BETA->fForwardTrackerEvent;

   fCerHits               = fEvents->BETA->fGasCherenkovEvent->fGasCherenkovHits;
   fLucHits               = fEvents->BETA->fLuciteHodoscopeEvent->fLuciteHits;
   fBigcalHits            = fEvents->BETA->fBigcalEvent->fBigcalHits;
   fBigcalTimingGroupHits = fEvents->BETA->fBigcalEvent->fBigcalTimingGroupHits;
   fTrackerHits           = fEvents->BETA->fForwardTrackerEvent->fTrackerHits;
   return(0);
}
//______________________________________________________________________________



ClassImp(BETAEventCounterCorrection)

//______________________________________________________________________________
BETAEventCounterCorrection::BETAEventCounterCorrection(InSANEAnalysis * analysis) : BETACorrection(analysis) {
   fRun = SANERunManager::GetRunManager()->GetCurrentRun();
   fRun->ClearCounters();
}
//______________________________________________________________________________
BETAEventCounterCorrection::~BETAEventCounterCorrection(){
}
//______________________________________________________________________________
void BETAEventCounterCorrection::Describe() {
   std::cout <<  "===============================================================================\n";
   std::cout <<  "  BETAEventCounterCorrection - \n ";
   std::cout <<  "       Counts events for various detectors and triggers. \n";
   std::cout <<  "_______________________________________________________________________________\n";
}
//______________________________________________________________________________
Int_t BETAEventCounterCorrection::Apply() {

   fRun->fNEvents++;
   if (fEvents->TRIG->IsBETA2Event())       fRun->fNBeta2Events++;
   if (fEvents->TRIG->IsBETA1Event())       fRun->fNBeta1Events++;
   if (fEvents->TRIG->IsPedestalEvent())    fRun->fNPedEvents++;
   if (fEvents->TRIG->IsHMSEvent())         fRun->fNHmsEvents++;
   if (fEvents->TRIG->IsPi0Event())         fRun->fNPi0Events++;
   if (fEvents->TRIG->IsLEDPulserEvent())   fRun->fNLedEvents++;
   if (fEvents->TRIG->IsCoincidenceEvent()) fRun->fNCoinEvents++;

      return(0);
   }
//_______________________________________________________//

