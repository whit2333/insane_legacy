#include "ANNThruFieldEvent.h"



ClassImp(ANNElectronThruFieldEvent)

//______________________________________________________________________________
ANNElectronThruFieldEvent::ANNElectronThruFieldEvent() {
   fInputNeurons  = "@fBigcalPlaneTheta,@fBigcalPlanePhi";
   fInputNeurons += ",@fBigcalPlaneX,@fBigcalPlaneY,@fBigcalPlaneEnergy";
   fNInputNeurons = 5;

   fTrueEnergy             = 0;
   fTrueTheta              = 0;
   fTruePhi                = 0;

   fDelta_Energy           = 0;
   fDelta_Theta            = 0;
   fDelta_Phi              = 0;

   fBigcalPlaneTheta       = 0;
   fBigcalPlanePhi         = 0;
   fBigcalPlaneDeltaPTheta = 0;
   fBigcalPlaneDeltaPPhi   = 0;
   fBigcalPlaneX           = 0;
   fBigcalPlaneY           = 0;
   fBigcalPlaneEnergy      = 0;
}
//______________________________________________________________________________
ANNElectronThruFieldEvent::~ANNElectronThruFieldEvent() {
}
//______________________________________________________________________________
Int_t ANNElectronThruFieldEvent::GetNInputNeurons() const { 
   return(fNInputNeurons);
}
//______________________________________________________________________________
const char * ANNElectronThruFieldEvent::GetMLPInputNeurons() const {
   return(fInputNeurons.Data());
}
//______________________________________________________________________________
void ANNElectronThruFieldEvent::GetMLPInputNeuronArray(Double_t * par) const {
   ///params[9] = {fClusterEnergy,fXCluster,fYCluster,fXClusterSigma,fYClusterSigma,fXClusterSkew,fYClusterSkew,fGoodCherenkovHit,fCherenkovADC};
   par[0] = fBigcalPlaneTheta;
   par[1] = fBigcalPlanePhi;
   par[2] = fBigcalPlaneX;
   par[3] = fBigcalPlaneY;
   par[4] = fBigcalPlaneEnergy;

}
//______________________________________________________________________________
void ANNElectronThruFieldEvent::Clear(){
   fTrueEnergy             = 0;
   fTrueTheta              = 0;
   fTruePhi                = 0;

   fDelta_Energy           = 0;
   fDelta_Theta            = 0;
   fDelta_Phi              = 0;

   fBigcalPlaneTheta       = 0;
   fBigcalPlanePhi         = 0;
   fBigcalPlaneDeltaPTheta = 0;
   fBigcalPlaneDeltaPPhi   = 0;
   fBigcalPlaneX           = 0;
   fBigcalPlaneY           = 0;
   fBigcalPlaneEnergy      = 0;
}
//______________________________________________________________________________



//______________________________________________________________________________
ANNElectronThruFieldEvent2::ANNElectronThruFieldEvent2(){
   fInputNeurons = "@fBigcalPlaneTheta,@fBigcalPlanePhi";
   fInputNeurons += ",@fBigcalPlaneX,@fBigcalPlaneY,@fBigcalPlaneEnergy";
   fInputNeurons += ",@fRasterX,@fRasterY";
   fNInputNeurons = 7;
   fRasterX = 0.0;
   fRasterY = 0.0;
}
//______________________________________________________________________________
ANNElectronThruFieldEvent2::~ANNElectronThruFieldEvent2(){
}
//______________________________________________________________________________
void ANNElectronThruFieldEvent2::Clear(){
   ANNElectronThruFieldEvent::Clear();
   fRasterX = 0.0;
   fRasterY = 0.0;
}
//______________________________________________________________________________
void ANNElectronThruFieldEvent2::GetMLPInputNeuronArray(Double_t * par) const {
   par[0] = fBigcalPlaneTheta;
   par[1] = fBigcalPlanePhi;
   par[2] = fBigcalPlaneX;
   par[3] = fBigcalPlaneY;
   par[4] = fBigcalPlaneEnergy;
   par[5] = fRasterX;
   par[6] = fRasterY;
}
//______________________________________________________________________________


//______________________________________________________________________________
ANNElectronThruFieldEvent3::ANNElectronThruFieldEvent3(){
   fInputNeurons = "@fBigcalPlaneTheta,@fBigcalPlanePhi";
   fInputNeurons += ",@fBigcalPlaneX,@fBigcalPlaneY,@fBigcalPlaneEnergy";
   fInputNeurons += ",@fRasterX,@fRasterY";
   fInputNeurons += ",@fXCluster,@fYCluster,@fXClusterSigma,@fYClusterSigma";
   fInputNeurons += ",@fXClusterSkew,@fYClusterSkew";
   fInputNeurons += ",@fXClusterKurt,@fYClusterKurt";
   fNInputNeurons = 15;
   fBigcalPlaneTheta  = 0;
   fBigcalPlanePhi    = 0;
   fBigcalPlaneX      = 0;
   fBigcalPlaneY      = 0;
   fBigcalPlaneEnergy = 0;
   fRasterX           = 0;
   fRasterY           = 0;
}
//______________________________________________________________________________
ANNElectronThruFieldEvent3::~ANNElectronThruFieldEvent3(){
}
//______________________________________________________________________________
void ANNElectronThruFieldEvent3::Clear(){
   ANNElectronThruFieldEvent2::Clear();
   fBigcalPlaneTheta  = 0;
   fBigcalPlanePhi    = 0;
   fBigcalPlaneX      = 0;
   fBigcalPlaneY      = 0;
   fBigcalPlaneEnergy = 0;
   fRasterX           = 0;
   fRasterY           = 0;
}
//______________________________________________________________________________
void ANNElectronThruFieldEvent3::GetMLPInputNeuronArray(Double_t * par) const {
   par[0] = fBigcalPlaneTheta;
   par[1] = fBigcalPlanePhi;
   par[2] = fBigcalPlaneX;
   par[3] = fBigcalPlaneY;
   par[4] = fBigcalPlaneEnergy;
   par[5] = fRasterX;
   par[6] = fRasterY;
   par[7] = fXCluster;
   par[8] = fYCluster;
   par[9] = fXClusterSigma;
   par[10] = fYClusterSigma;
   par[11] = fXClusterSkew;
   par[12] = fYClusterSkew;
   par[13] = fXClusterKurt;
   par[14] = fYClusterKurt;
}
//______________________________________________________________________________

