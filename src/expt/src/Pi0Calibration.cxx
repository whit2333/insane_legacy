#include "Pi0Calibration.h"
#include "TVectorT.h"

ClassImp(Pi0Calibration)
//______________________________________________________________________________
Pi0Calibration::Pi0Calibration() {

   fMass_0  = 134.9766;
   fMass2_0 = fMass_0*fMass_0;

   SetNameTitle("Pi0Calibration", "Pi0Calibration");
   fBCGeo = BIGCALGeometryCalculator::GetCalculator();

   fPi0Tree        = nullptr;
   fPi0Event       = new Pi0Event();

   fInitSeries = -1;
   fLaterSeries = -1;

   fCanvas  = nullptr;
   fCanvas2 = nullptr;
   fCanvas3 = nullptr;

   fiMax           = 32;//29;//32;
   fiMin           = 1;//5;//3;
   fjMax           = 56;//54;
   fjMin           = 1;//3;
   fNumberOfBlocks = 0;
   InitActiveBlocks(fiMin,fjMin,fiMax,fjMax);
   InitBlocks(fiMin,fjMin,fiMax,fjMax);

   fBlockNumber          = 1;
   fFirstEventNumber     = 0;
   fLastEventNumber      = 0;
   fNumberOfEvents       = 0;
   fCalibrationIteration = 0;

   for (int jj = 0; jj < 1744; jj++) {
      epsilon[jj]     = 0.0;
      epsilonLast[jj] = 0.0;
   }
   fMaxEpsilon      = 0.8;
   fEpsilonFraction = 0.01;

   fNu[0] = new Pi0PhotonEnergy(0);
   fNu[1] = new Pi0PhotonEnergy(1);
   fMPi0Squared = new Pi0MassSquared();
   fMPi0Squared->p0 = fNu[0];
   fMPi0Squared->p1 = fNu[1];
   fClusterPair   = nullptr;
   fIsPerp        = false;
   fBlockMaxNEvents = 50000;
   fB             = 0.0;
   fB_event       = 0.0;
   fChecki        = 1;
   fDGraph        = nullptr;
   fLGraph        = nullptr;
   fCanvas        = nullptr;
   fLHist         = nullptr;
   fEigenValueHist = nullptr;
   fEpsilonBlocks = nullptr;
   fDHist         = nullptr;
   fEventClusters = nullptr;
   fCHist         = nullptr;
   fLastMass      = 0.0;
   fEigenValueThreshold = 1.0;//e-4;
}
//_______________________________________________________//
Pi0Calibration::Pi0Calibration(InSANEAnalysis * analysis) : BETACalculationWithClusters(analysis) {

   SetNameTitle("Pi0Calibration", "Pi0Calibration");
   fBCGeo = BIGCALGeometryCalculator::GetCalculator();

   fPi0Tree        = nullptr;
   fPi0Event       = new Pi0Event();

   fiMax           = 32;
   fiMin           = 1;
   fjMax           = 57;
   fjMin           = 1;
   fNumberOfBlocks = 1;
   InitBlocks(fiMin,fjMin,fiMax,fjMax);

   fBlockNumber          = 1;
   fFirstEventNumber     = 0;
   fLastEventNumber      = 0;
   fNumberOfEvents       = 0;
   fCalibrationIteration = 0;

   fNu[0] = new Pi0PhotonEnergy(0);
   fNu[1] = new Pi0PhotonEnergy(1);
   fMPi0Squared = new Pi0MassSquared();
   fMPi0Squared->p0 = fNu[0];
   fMPi0Squared->p1 = fNu[1];

   fB             = 0.0;
   fChecki        = 1;
   fDGraph        = nullptr;
   fLGraph        = nullptr;
   fCanvas        = nullptr;
   fLHist         = nullptr;
   fDHist         = nullptr;
   fEventClusters = nullptr;
   fCHist         = nullptr;
}
//______________________________________________________________________________
Pi0Calibration::~Pi0Calibration() {
   //if (fBlockNumberList) delete fBlockNumberList;
   if (fEventClusters) delete fEventClusters;
   if (fLHist) delete fLHist;
   if (fEpsilonHist) delete fEpsilonHist;
   if (fDHist) delete fDHist;
   if (fCHist) delete fCHist;
   if (fBigcalHist) delete fBigcalHist;
   if (fEpsilonLastHist) delete fEpsilonLastHist;
}
//______________________________________________________________________________
Int_t Pi0Calibration::SetClusterPair(Pi0ClusterPair * pair){
   fClusterPair = pair;
   return (0);
}
//______________________________________________________________________________
Int_t  Pi0Calibration::GetBlockMatrixIndex(Int_t aBlockNum) {
   // deprecated use map instead
   Int_t res = -1;
   for (unsigned int i = 0 ; i < fBlockNumberList.size() ; i++) {
      if (fBlockNumberList.at(i) == aBlockNum) {
         res = i;
         return(res);
      }
   }
   return(res);
}
//______________________________________________________________________________
bool Pi0Calibration::IsActiveBlock(Int_t aBlockNum) const {
   if( fActiveBlocks.count(aBlockNum) == 0 ) return false;
   return true;
}
//______________________________________________________________________________
bool Pi0Calibration::IsBlockInList(Int_t aBlockNum) const {
   if( fMatrixBlocks.count(aBlockNum) == 0 ) return false;
   return true;
}
//______________________________________________________________________________
bool Pi0Calibration::IsBlockOnListPerimeter(Int_t i,Int_t j) const {
   //if( !(IsBlockInList(aBlockNum)) ) return false; 
   if (j == fjMin) return true;
   if (j == fjMax) return true;
   if (i == fiMin) return true;
   if (i == fiMax) return true;
   if (j > 1 && j < 33) {
      if (i == fiMax) return true;
   }
   if (j > 32 && j < 57) {
      if (i == 30) return true;
   }
   /* else */
   return false;
}
//______________________________________________________________________________
bool Pi0Calibration::IsBlockOnListPerimeter(Int_t aBlockNum) const {
   //if( !(IsBlockInList(aBlockNum)) ) return false; 
   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   int i = geocalc->GetBlock_i(aBlockNum);
   int j = geocalc->GetBlock_j(aBlockNum);
   if (j == fjMin) return true;
   if (j == fjMax) return true;
   if (i == fiMin) return true;
   if (i == fiMax) return true;
   if (j > 1 && j < 33) {
      if (i == fiMax) return true;
   }
   if (j > 32 && j < 57) {
      if (i == 30) return true;
   }
   /* else */
   return false;
}
//______________________________________________________________________________
Int_t Pi0Calibration::AddBlock(Int_t i,Int_t j){
   Int_t blockNumber = fBCGeo->GetBlockNumber(i,j); 
   if(!(IsActiveBlock(blockNumber))) {
      Error("AddBlock","Block is not in active block list. Not adding block");
      return -1;
   }
   return AddBlock(blockNumber);
}
//______________________________________________________________________________
Int_t Pi0Calibration::AddBlock(Int_t aBlockNum) {
   if(aBlockNum>0 && aBlockNum <= 1744) {
      if(!IsBlockInList(aBlockNum)){
         fBlockNumberList.push_back(aBlockNum);
         fMatrixBlocks[aBlockNum] = fNumberOfBlocks;
         fNumberOfBlocks++;
      }
      return 0;
   }
   return -1;
}
//______________________________________________________________________________
Int_t Pi0Calibration::AddActiveBlock(Int_t i,Int_t j){
   Int_t blockNumber = fBCGeo->GetBlockNumber(i,j); 
   return AddActiveBlock(blockNumber);
}
//______________________________________________________________________________
Int_t Pi0Calibration::AddActiveBlock(Int_t aBlockNum) {
   if(aBlockNum>0 && aBlockNum <= 1744) {
      if(!IsActiveBlock(aBlockNum)){
      //if(!IsBlockInList(aBlockNum)){
         //fBlockNumberList.push_back(aBlockNum);
         fActiveBlocks[aBlockNum] = fNumberOfActiveBlocks;
         // mapped value should not be used for matrix index
         fNumberOfActiveBlocks++;
      }
      return 0;
   }
   return -1;
}
//______________________________________________________________________________
Int_t Pi0Calibration::InitActiveBlocks(Int_t i1,Int_t j1,Int_t i2,Int_t j2,bool clear){
   // Run this before InitBlocks() 
   if(clear){
      fNumberOfActiveBlocks = 0;
      fActiveBlocks.clear();
   }
   for(int j = j1; j<= j2 ; j++) {
      for(int i = i1; i<= i2 ; i++) {
         if(j>32 && i>30) continue;
         AddActiveBlock(i,j);
      }
   }
   //fNumberOfBlocks = fBlockNumberList.size();
   std::cout << " Number of blocks: " << fNumberOfBlocks << std::endl;
   return(0);
}
//______________________________________________________________________________
Int_t Pi0Calibration::ClearBlocks(){
   fNumberOfBlocks = 0;
   fNumberOfActiveBlocks = 0;
   fActiveBlocks.clear();
   fMatrixBlocks.clear();
   fBlockNumberList.clear();
   return 0;
}
//______________________________________________________________________________
Int_t Pi0Calibration::InitBlocks(Int_t i1,Int_t j1,Int_t i2,Int_t j2, bool clear){
   // run InitActiveBlocks first
   if(clear){
      fiMax = i2;
      fiMin = i1;
      fjMax = j2;
      fjMin = j1;
      fNumberOfBlocks = 0;
      fMatrixBlocks.clear();
      fBlockNumberList.clear();
   }
   for(int j = j1; j<= j2 ; j++) {
      for(int i = i1; i<= i2 ; i++) {
         if(j>32 && i>30) continue;
         AddBlock(i,j);
      }
   }
   //fNumberOfBlocks = fBlockNumberList.size();
   std::cout << " Number of blocks: " << fNumberOfBlocks << std::endl;
   return(0);
}
//______________________________________________________________________________
Int_t Pi0Calibration::Initialize() {
   // Get the pi0 tree and set the branches
   //fPi0Tree = (TTree*) gROOT->FindObject("pi0results");
   //if(!fPi0Tree) {
   //   std::cout << " AHHHHHHHHHHHHHHHHHHHHHHH " << std::endl;
   //   Error("Initialize","Could not find Pi0Tree");
   //   return -1;
   //}
   //fPi0Tree->SetBranchAddress("pi0reconstruction", &fPi0Event);
   //fPi0Tree->BuildIndex("fRunNumber","fEventNumber");
   //if(SANERunManager::GetRunManager()->fVerbosity>2)
   //   std::cout << " o Pi0Calibration::Initialize()\n";
   //BETACalculationWithClusters::Initialize();

   // This might take WAY too much memory....
   //std::cout << "Initializing ... " << std::cout;
   fEigenVectorsWithStatistics = new std::vector<Int_t>;

   fC.ResizeTo(fNumberOfBlocks, fNumberOfBlocks); // Matrix
   fCInverted.ResizeTo(fNumberOfBlocks, fNumberOfBlocks); // Matrix
   fEpsilon.ResizeTo(fNumberOfBlocks, 1);              // Vector
   fL.ResizeTo(fNumberOfBlocks, 1);              // Vector
   fD.ResizeTo(fNumberOfBlocks, 1);              // Vector
   fEpsilonLast.ResizeTo(fNumberOfBlocks, 1);              // Matrix
   fBlockEnergies.ResizeTo(1744, 1);

   fCInverted.Zero();
   fC.Zero();
   fEpsilon.Zero();
   fL.Zero();
   fD.Zero();
   fEpsilonLast.Zero();
   fBlockEnergies.Zero();
   fB = 0.0;
   fLambda = 0.0;

   for(int i = 0 ; i < fEpsilonLast.GetNoElements(); i++){
      fEpsilonLast[i][0] = 0.0;
      fEpsilon[i][0] = 0.0;
   }

   fMassBiasHist    = new TH1D("fMassBiasHist", "fMassBiasHist", 100, -1.0e6, 1.0e6);
   fEventClusters   = new TH2D("Pi0EventClusters", "Bigcal Pi0 Event Clusters", 32, 1, 33, 56, 1, 57);
   fEpsilonBlocks   = new TH2D("EpsilonBlocks", "Blocks #epsilon", 32, 1, 33, 56, 1, 57);
   fLHist           = new TH1D("fLHist", "L Vector", fNumberOfBlocks, 0, fNumberOfBlocks );
   fDHist           = new TH1D("fDHist", "D Vector", fNumberOfBlocks, 0, fNumberOfBlocks );
   fEBLockHist      = new TH1D("fEBLockHist", "fEBLockHist", 1744, 0, 1744 );
   fEigenValueHist  = new TH1D("fEigenValueHist", "fEigenValueHist", fNumberOfBlocks, 0, fNumberOfBlocks );
   fEpsilonHist     = new TH1D("fEpsilonHist", "fEpsilonHist", fNumberOfBlocks, 0, fNumberOfBlocks );
   fEpsilonLastHist = new TH1D("fEpsilonLastHist", "fEpsilonLastHist", fNumberOfBlocks, 0, fNumberOfBlocks );
   fBigcalHist      = new TH2D("fBigcalHist", "Bigcal Event Energy", 32, 1, 33, 56, 1, 57);
   fCHist           = new TH2D("fCHist", "C matrix", fNumberOfBlocks, 0, fNumberOfBlocks , fNumberOfBlocks, 0, fNumberOfBlocks );
   fCInvHist        = new TH2D("fCInvHist", "Inverted C matrix", fNumberOfBlocks, 0, fNumberOfBlocks , fNumberOfBlocks, 0, fNumberOfBlocks );
   fBigcalHist->SetOption("colz");
   fCHist->Reset();
   fCInvHist->Reset();
   fBigcalHist->Reset();

   bcCoords         = new BigcalCoordinateSystem();
   fRotFromBCcoords = bcCoords->GetRotationMatrixTo();

   return(0);
}
//______________________________________________________________________________
Int_t Pi0Calibration::Calculate() {
   // this method is when this class is used as an InSANECalculation

   if(!fEvents->TRIG->IsPi0Event()) return(0);
   if (fClusterEvent->fNClusters < 2) return (0);

   Int_t bytesread = fPi0Tree->GetEntryWithIndex(fEvents->TRIG->fRunNumber,fEvents->TRIG->fEventNumber);
   if(!bytesread) return(-1);

   //std::cout << "trig event: " << fEvents->TRIG->fEventNumber << std::endl;
   //std::cout << "pi0  event: " << fPi0Event->fEventNumber << std::endl;

   //for(int i = 0; i< fPi0Event->fReconstruction->GetEntries() ; i++){
   //   InSANEReconstruction * recon = (InSANEReconstruction*)(*(fPi0Event->fReconstruction))[i];
   //}
   
   //for(int i = 0; i< fPi0Event->fClusterPairs->GetEntries() ; i++){
   //   Pi0ClusterPair * pair = (Pi0ClusterPair*)(*(fPi0Event->fClusterPairs))[i];
   //   InSANEReconstruction * recon = (InSANEReconstruction*)(*(fPi0Event->fReconstruction))[i];
   //   if(TMath::Abs(recon->fMass - 135.0) <50) {
   //      std::cout << " mass = " << recon->fMass << std::endl;
   //      pair->fMass = recon->fMass;
   //      SetClusterPair(pair);
   //      if( BuildMatricies() != -1)    fNumberOfEvents++;
   //   }
   //}
   //if (HasEnoughStatistics()) {
   //   ExecuteCalibration();
   //}
   return(0);
}
//______________________________________________________________________________
Bool_t Pi0Calibration::HasEnoughStatistics() {
   if (fNumberOfEvents > 20 * fChecki) {
      Double_t det1 = 0;
      ViewStatus();
      fCInverted = fC;
      fCInverted.Invert(&det1);
      if (det1 == 0) {
         std::cout << "Determinant is zero!!!" << std::endl;
         fChecki++;
         //fC.Print("f= %3.2g  ");
         return(false);
      } else {
         fChecki++;
         return(true);
      }
   } else {
      return (false);
   }
}
//______________________________________________________________________________
void Pi0Calibration::Print(Option_t * opt) const {
}
//______________________________________________________________________________
Int_t Pi0Calibration::ExecuteCalibration() {
   // Create a matrix for Cinverse composed of eigen vectors with non zero eigenvalues 
   std::cout << " Calculating Eigenvalues and Eigenvectors ..." << std::endl;
   TMatrixD Cinv_eigen = fC;
   Cinv_eigen.Zero();
   TMatrixD ee_alpha = fC; 
   ee_alpha.Zero();
   //TVectorD eigenVal;
   TMatrixD eigenVec = fC.EigenVectors(fEigenValues);
   //TMatrixD eigenVecInv = eigenVec;
   //eigenVecInv.Invert();
   Double_t largestEigenVal = fEigenValues[0];//eigenVal[0];
   int nrow = fEigenValues.GetNrows();
   int nZeroEigenValues = 0;
   std::cout << " Largest Eigenvalue is " << largestEigenVal << std::endl;
   //std::cout << "EigenValues:" << std::endl;
   for(int i = 0; i< nrow ;i++){

      if( TMath::Abs(fEigenValues[i] ) < fEigenValueThreshold ) {
         std::cout << i << " " << fEigenValues[i]; 
         std::cout << " skipped " << std::endl;
         nZeroEigenValues++;
         continue;
      }
      //std::cout << std::endl; 

      //TMatrixD e_alpha = eigenVec.GetSub(0,nrow-1,i,i);
      ee_alpha.Zero();
      for(int j = 0; j< nrow ;j++){
         for(int k = 0; k< nrow ;k++){
            //Double_t xj = e_alpha[j][0];
            //Double_t xk = e_alpha[k][0];
            Double_t xj = eigenVec[j][i];
            Double_t xk = eigenVec[k][i];
            ee_alpha[j][k] = (xj*xk);
            //std::cout << j <<" " << e_alpha[0][j] << std::endl; 
            //std::cout << k << " " << e_alpha[0][k] << std::endl; 
            //std::cout << j <<","<<k << " " << ee_alpha[j][k] << std::endl; 
         }
      }
      ee_alpha *= (1.0/fEigenValues[i]);
      //ee_alpha.Print();
      //TMatrixD(e_alpha,TMatrixD::kMultTranspose,e_alpha_trans); // doesn't work :(
      Cinv_eigen += ee_alpha;
   }
   std::cout << "Eigen-decomposition has " << nZeroEigenValues << " eigen values that are zero and skipped." << std::endl; 
   std::cout << "Executing Pi0 Calibration " << fCalibrationIteration << std::endl;
   Double_t det1 = 0;
   fCInverted = Cinv_eigen;
   TMatrixD LTrans(1, fNumberOfBlocks);
   TMatrixD CInvD(fNumberOfBlocks, 1);
   TMatrixD CInvL(fNumberOfBlocks, 1);
   LTrans.Transpose(fL);
   CInvD = (fCInverted * fD);
   CInvL = (fCInverted * fL);
   Double_t LtransCinvD = (LTrans * CInvD)[0][0];
   Double_t LtransCinvL = (LTrans * CInvL)[0][0];
   std::cout << " lambda = (B + Lt C^-1 D)/(Lt C^-1 L) = (" << fB << " + " << LtransCinvD << ")/(" << LtransCinvL << ")" << std::endl;
   fLambda = (fB + LtransCinvD) / (LtransCinvL);
   std::cout << " lambda = " << fLambda << std::endl;
   fEpsilonLast = fEpsilon;
   // A diagonal matrix for multiplying each component
   TMatrixD diagMEpsilonLast = TMatrixD(fEpsilonLast.GetNrows(),fEpsilonLast.GetNrows());
   diagMEpsilonLast.Zero();
   for(int id = 0; id<fEpsilonLast.GetNrows(); id++){
      diagMEpsilonLast[id][id] = fEpsilonLast[id][0];
   }

   // Solution for new epsilon values
   CInvL *= fLambda;
   TMatrixD epsilonSol     = (CInvD - CInvL);
   //std::cout << "fEpsilonLast " << std::endl;
   //fEpsilonLast.Print();
   for(int i = 0; i< epsilonSol.GetNoElements(); i++){
      if( TMath::Abs(epsilonSol[i][0]) > fMaxEpsilon ){
         // Normalize to one but keep the sign of the epsilon
         epsilonSol[i][0] = fMaxEpsilon*epsilonSol[i][0]/TMath::Abs(epsilonSol[i][0]);
      }
      epsilonSol[i][0] *= fEpsilonFraction;
   }

   // Merge the new epsilon with the last epsilon :
   // E' -> E'' = (1+eps'_k)E' = (1+eps'_k)(1+eps_k)E
   // eps_new =  eps_k + eps'_k + eps_k*eps'_k
   fEpsilon = (fEpsilonLast + epsilonSol + (diagMEpsilonLast*epsilonSol));

   //std::cout << "New fEpsilon " << std::endl;
   //fEpsilon.Print();
   fCalibrationIteration++;

   return(0);
}
//______________________________________________________________________________
Double_t Pi0Calibration::CalculateMass(){
   // Use this to calculate the latest mass. Note that this should be used before
   // BuildMatricies()
   fBlocksInvolved.clear();
   fMatrixElementsInvolved.clear();
   fNu[0]->fShowerBlocks->clear();
   fNu[1]->fShowerBlocks->clear();
   // zero all the blocks
   fBlockEnergies.Zero();
   //fEventClusters->Reset();

   fNu[0]->SetParameters(fBlockEnergies.GetMatrixArray());
   fNu[1]->SetParameters(fBlockEnergies.GetMatrixArray());
   fMPi0Squared->SetParameters(fBlockEnergies.GetMatrixArray());

   // Add the clusters
   BIGCALCluster * aCluster = &(fClusterPair->fCluster1);//(BIGCALCluster*)(*fClusters)[0] ;
   BIGCALCluster * bCluster = &(fClusterPair->fCluster2);//(BIGCALCluster*)(*fClusters)[1] ;

   // Loop over each cluster and add to list of involved blocks
   CorrectClusterBlocks(aCluster,0);
   CorrectClusterBlocks(bCluster,1);
   // AddClusterBlocks also updates the clusters moments and energies

   // NOTE: here we make the assumption that the change in the cluster's energy 
   // due to calibration does not change much the NN calculated energy correction.

   // Set the vectors
   origin.SetXYZ(0.0, 0.0, 0.0);
   v1.SetXYZ(aCluster->GetXmoment()+ aCluster->fDeltaX,   
             aCluster->GetYmoment()+ aCluster->fDeltaY,
             BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
   k1 = v1 - origin;
   k1.SetMag(aCluster->fTotalE /*+ aCluster->fDeltaE*/);
   k1.Transform(fRotFromBCcoords);
   v2.SetXYZ(bCluster->GetXmoment()+ bCluster->fDeltaX,   
             bCluster->GetYmoment()+ bCluster->fDeltaY,
             BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
   k2 = v2 - origin;
   k2.SetMag(bCluster->fTotalE /*+ bCluster->fDeltaE*/);
   k2.Transform(fRotFromBCcoords);

   fMPi0Squared->fCosTheta = TMath::Cos(k2.Angle(k1));

   fNu[0]->fDeltaE = 0.0;//aCluster->fDeltaE;
   fNu[1]->fDeltaE = 0.0;//bCluster->fDeltaE;
   fNu[0]->SetParameters(fBlockEnergies.GetMatrixArray());
   fNu[1]->SetParameters(fBlockEnergies.GetMatrixArray());
   fMPi0Squared->SetParameters(fBlockEnergies.GetMatrixArray());

   // setup dummy arrays which have all blocks instead of just those in the list
   for (int jj = 0; jj < 1744; jj++) {
      epsilon[jj]     = 0.0;
      epsilonLast[jj] = 0.0;
   }
   for (unsigned int jj = 0; jj < fBlockNumberList.size(); jj++) {
      epsilon[     fBlockNumberList.at(jj)-1] = fEpsilon[    jj ][0];
      epsilonLast[ fBlockNumberList.at(jj)-1] = fEpsilonLast[jj ][0];
   }
   return TMath::Sqrt(fMPi0Squared->Eval(epsilon));
}
//______________________________________________________________________________
Int_t Pi0Calibration::CorrectClusterBlocks(BIGCALCluster * clust, Int_t photon_i){

   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   Int_t i = clust->fiPeak;
   Int_t j = clust->fjPeak;
   // Check that the cluster is within the section we are calibrating
   // loop over 5x5 block around peak  (m and n go from -2 to 2)
   Int_t offset = -2;
   for (int n = offset ; n < 5 + offset ; n++) {
      for (int m = offset ; m < 5 + offset ; m++) {
         if (((j + n <= 56 && j + n > 32) && (i + m <= 30 && i + m > 0))/*RCS*/   ||
               ((j + n <= 32 && j + n > 0) && (i + m <= 32 && i + m > 0))/*PROT*/) {

            //    std::cout << "block  " << geocalc->GetBlockNumber(i+m,j+n)
            //              << " (" << i+m << "," << j+n << ") "
            //              << "E=" << clust->fBlockEnergies[m+2][n+2] << std::endl;
            Double_t b_energy = clust->fBlockEnergies[m+2][n+2];
            Int_t    b_num    = geocalc->GetBlockNumber(i + m, j + n);
            // if the block is to be calibrated use the last iteration of epsilon to 
            // update the energy.
            if( IsBlockInList(b_num)) {
               b_energy *= (1.0+fEpsilon[GetBlockMatrixIndex(b_num)][0]);
               clust->fBlockEnergies[m+2][n+2] = b_energy;
               //fBigcalHist->Fill(i + m, j + n);
               fMatrixElementsInvolved[fMatrixBlocks[b_num]] = b_num-1;
            }
            fBlocksInvolved.push_back( b_num );
            fNu[photon_i]->fShowerBlocks->push_back(b_num - 1);
            //fEventClusters->Fill(i + m, j + n, b_energy);
            // fBlockEnergies is an array used by fNu
            fBlockEnergies[b_num-1][0] = b_energy ;
         }
      }
   }
   clust->CalculateMoments();

   // Neural network corrections for photons 
   // Turning off NN
   fANNGammaEvent.SetEventValues(clust); 
   fANNGammaEvent.fXClusterKurt = clust->fXKurtosis;
   fANNGammaEvent.fYClusterKurt = clust->fYKurtosis;

   fANNGammaEvent.GetMLPInputNeuronArray(fNNParams);
   if(fIsPerp) {
      clust->fDeltaX  = fNNPerpDeltaX.Value(0,fNNParams);
      clust->fDeltaY  = fNNPerpDeltaY.Value(0,fNNParams);
      clust->fDeltaE  = 0.0;//fNNPerpDeltaE.Value(0,fNNParams);
   } else {
      clust->fDeltaX  = fNNParaDeltaX.Value(0,fNNParams);
      clust->fDeltaY  = fNNParaDeltaY.Value(0,fNNParams);
      clust->fDeltaE  = 0.0;//fNNParaDeltaE.Value(0,fNNParams);
   }

   return 0;
}
//______________________________________________________________________________
Int_t Pi0Calibration::AddClusterBlocks(BIGCALCluster * clust, Int_t photon_i){

   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   Int_t i = clust->fiPeak;
   Int_t j = clust->fjPeak;
   // Check that the cluster is within the section we are calibrating
   // loop over 5x5 block around peak  (m and n go from -2 to 2)
   Int_t offset = -2;
   for (int n = offset ; n < 5 + offset ; n++) {
      for (int m = offset ; m < 5 + offset ; m++) {
         if (((j + n <= 56 && j + n > 32) && (i + m <= 30 && i + m > 0))/*RCS*/   ||
               ((j + n <= 32 && j + n > 0) && (i + m <= 32 && i + m > 0))/*PROT*/) {

            //    std::cout << "block  " << geocalc->GetBlockNumber(i+m,j+n)
            //              << " (" << i+m << "," << j+n << ") "
            //              << "E=" << clust->fBlockEnergies[m+2][n+2] << std::endl;
            Double_t b_energy = clust->fBlockEnergies[m+2][n+2];
            Int_t    b_num    = geocalc->GetBlockNumber(i + m, j + n);
            // if the block is to be calibrated use the last iteration of epsilon to 
            // update the energy.
            if( IsBlockInList(b_num)) {
               // Note we assume that this has been done in CorrectClusterBlocks
               //b_energy *= (1.0+fEpsilon[GetBlockMatrixIndex(b_num)][0]);
               //clust->fBlockEnergies[m+2][n+2] = b_energy;
               fBigcalHist->Fill(i + m, j + n);
               fMatrixElementsInvolved[fMatrixBlocks[b_num]] = b_num-1;
            }
            fBlocksInvolved.push_back( b_num );
            fNu[photon_i]->fShowerBlocks->push_back(b_num - 1);
            fEventClusters->Fill(i + m, j + n, b_energy);
            //fBlockEnergies[geocalc->GetBlockNumber(i+m, j+n)-1] = b_energy ;
            fBlockEnergies[b_num-1][0] = b_energy ;
         }
      }
   }
   clust->CalculateMoments();
   return 0;
}
//______________________________________________________________________________
Int_t Pi0Calibration::BuildMatricies() {

   // First clear out last event
   // clear the lists of blocks involved
   //if (fNumberOfEvents % 100 == 0)
   //   std::cout << "Building Matrix for event " << fNumberOfEvents << std::endl;
   fBlocksInvolved.clear();
   fMatrixElementsInvolved.clear();
   fNu[0]->fShowerBlocks->clear();
   fNu[1]->fShowerBlocks->clear();
   // zero all the blocks
   fBlockEnergies.Zero();
   //fEventClusters->Reset();

   fNu[0]->SetParameters(fBlockEnergies.GetMatrixArray());
   fNu[1]->SetParameters(fBlockEnergies.GetMatrixArray());
   fMPi0Squared->SetParameters(fBlockEnergies.GetMatrixArray());

   // Add the clusters
   BIGCALCluster * aCluster = &(fClusterPair->fCluster1);//(BIGCALCluster*)(*fClusters)[0] ;
   BIGCALCluster * bCluster = &(fClusterPair->fCluster2);//(BIGCALCluster*)(*fClusters)[1] ;

   // Loop over each cluster and add to list of involved blocks
   AddClusterBlocks(aCluster,0);
   AddClusterBlocks(bCluster,1);
   // AddClusterBlocks also updates the clusters moments and energies

   // NOTE: here we make the assumption that the change in the cluster's energy 
   // due to calibration does not change much the NN calculated energy correction.

   // Set the vectors
   origin.SetXYZ(0.0, 0.0, 0.0);
   v1.SetXYZ(aCluster->GetXmoment()+ aCluster->fDeltaX,   
             aCluster->GetYmoment()+ aCluster->fDeltaY,
             BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
   k1 = v1 - origin;
   k1.SetMag(aCluster->fTotalE /*+ aCluster->fDeltaE*/);
   k1.Transform(fRotFromBCcoords);
   v2.SetXYZ(bCluster->GetXmoment()+ bCluster->fDeltaX,   
             bCluster->GetYmoment()+ bCluster->fDeltaY,
             BIGCALGeometryCalculator::GetCalculator()->fNominalRadialDistance);
   k2 = v2 - origin;
   k2.SetMag(bCluster->fTotalE /*+ bCluster->fDeltaE*/);
   k2.Transform(fRotFromBCcoords);

   fMPi0Squared->fCosTheta = TMath::Cos(k2.Angle(k1));

   fNu[0]->fDeltaE = 0.0;//aCluster->fDeltaE;
   fNu[1]->fDeltaE = 0.0;//bCluster->fDeltaE;
   fNu[0]->SetParameters(fBlockEnergies.GetMatrixArray());
   fNu[1]->SetParameters(fBlockEnergies.GetMatrixArray());
   fMPi0Squared->SetParameters(fBlockEnergies.GetMatrixArray());

   // setup dummy arrays which have all blocks instead of just those in the list
   for (int jj = 0; jj < 1744; jj++) {
      epsilon[jj]     = 0.0;
      epsilonLast[jj] = 0.0;
   }
   for (unsigned int jj = 0; jj < fBlockNumberList.size(); jj++) {
      epsilon[     fBlockNumberList.at(jj)-1] = fEpsilon[    jj ][0];
      epsilonLast[ fBlockNumberList.at(jj)-1] = fEpsilonLast[jj ][0];
   }
   fLastMass = TMath::Sqrt(fMPi0Squared->Eval(epsilon));
   fB_event  = 0.0;

   //std::cout << "calculated Mass: " << TMath::Sqrt(fMPi0Squared->Eval(epsilon)) << std::endl;
   // Two loops over the blocks used in constructing kxk matrix
   // fMatrixElementsInvolved is a map (Matrix index , blockd number -1) sorted by matrix index.
   for (std::map<int,int>::const_iterator ki = fMatrixElementsInvolved.begin(); ki != fMatrixElementsInvolved.end(); ++ki) {
      int i_matrix = (*ki).first; 
      int i_block  = (*ki).second;  // block number minus 1

      //std::cout << "i  " << i_matrix << "    " << i_block << std::endl;

      for (std::map<int,int>::const_iterator kj = fMatrixElementsInvolved.begin(); ((*kj).first <= (*ki).first) && (kj != fMatrixElementsInvolved.end()); ++kj) {

         int j_matrix = (*kj).first; 
         int j_block  = (*kj).second;  // block number minus 1
         //std::cout << "j  " << j_matrix << "    " << j_block << std::endl;

         double cME1 = fMPi0Squared->Derivative(epsilon, i_block);
         double cME2 = fMPi0Squared->Derivative(epsilon, j_block);
         double cME  = cME1 * cME2 ;

         fC[i_matrix][j_matrix] += cME ;

         // It is a symmetric matrix
         if( j_matrix != i_matrix ) {
            fC[j_matrix][i_matrix] += cME ;
            //fC[j_block][i_block] += cME ;
         }
      }

      double derivative   = fMPi0Squared->Derivative(epsilon,i_block);
      double msquaredbias = (fMPi0Squared->Eval(epsilon) - (PI0MASSSQUARED));
      double Dmatrixpiece = -1.0 * (fMPi0Squared->Eval(epsilon) - (PI0MASSSQUARED)) * fMPi0Squared->Derivative(epsilon,i_block);
      //if(msquaredbias != 0.0) std::cout << "msquaredbias = " << msquaredbias << std::endl;
      //if(Dmatrixpiece != 0.0) std::cout << "Dmatrixpiece = " << Dmatrixpiece << std::endl;

      fL[i_matrix] += derivative ;
      fD[i_matrix] += Dmatrixpiece ;
      fB_event     += msquaredbias;
      fB           += msquaredbias;
   }
   fMassBiasHist->Fill(fB_event);

   //std::cout << "fB = " << fB << ", \n";

   return(0);
}
//__________________________________________________________________________________________
void Pi0Calibration::ViewStatus()
{
   if (!fCanvas) {
      fCanvas = new TCanvas("pi0Calibrator", "Pi0 Calibrations", 1400, 1000);
      fCanvas->Divide(4, 2);
   }
   fCanvas->cd(1);
   fBigcalHist->Draw();

   fCanvas->cd(2);
   fEventClusters->Draw("colz");

   fCanvas->cd(3);
   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   if (fEpsilonBlocks) {
      fEpsilonBlocks->Reset();
      for(int i = 1; i<=1744; i++){
         if(IsBlockInList(i)){
            int imatrix = fMatrixBlocks[i];
            int ibin = fEpsilonBlocks->FindBin(geocalc->GetBlock_i(i),geocalc->GetBlock_j(i));
            fEpsilonBlocks->SetBinContent(ibin, fEpsilon[imatrix][0] );
         }
      }
      fEpsilonBlocks->Draw("COLZ");
   }
   //if (fEigenValueHist) {
   //   //fEBLockHist->SetContent(fBlockEnergies.GetMatrixArray());
   //   for(int i = 0; i < fEigenValues.GetNoElements(); i++){
   //      fEigenValueHist->SetBinContent(i+1, fEigenValues[i] );
   //   }
   //   //fEigenValueHist->SetContent(fEigenValues.GetMatrixArray());
   //   fEigenValueHist->Draw("");
   //}
   //
   fCanvas->cd(4);
   if (fMassBiasHist) {
      fMassBiasHist->Draw("");
   }
//    fCanvas->cd(4);
   fCanvas->cd(5);
   if (fEpsilonHist) {
      for(int i = 0; i < fEpsilon.GetNoElements(); i++){
         fEpsilonHist->SetBinContent(i+1, fEpsilon[i][0] );
      }
      //fEpsilonHist->SetContent(fEpsilon.GetMatrixArray());
      fEpsilonHist->Draw("");
      fEpsilonHist->GetYaxis()->SetRangeUser(-0.8,0.8);
   }
   if(fEpsilonLastHist) {
      for(int i = 0; i < fEpsilonLast.GetNoElements(); i++){
         fEpsilonLastHist->SetBinContent(i+1, fEpsilonLast[i][0] );
      }
      //fEpsilonLastHist->SetContent(fEpsilonLast.GetMatrixArray());
      fEpsilonLastHist->SetLineColor(kRed);
      fEpsilonLastHist->Draw("same");
   }

   fCanvas->cd(6);//->SetLogy();
   if (fLHist) {
      for(int i = 0; i < fL.GetNoElements(); i++){
         fLHist->SetBinContent(i+1, fL[i][0] );
      }
      //fLHist->SetContent(fL.GetMatrixArray());
      fLHist->Draw("");
   }

   fCanvas->cd(7);//->SetLogy();
   if (fDHist) {
      for(int i = 0; i < fD.GetNoElements(); i++){
         fDHist->SetBinContent(i+1, fD[i][0] );
      }
      //fDHist->SetContent(fD.GetMatrixArray());
      fDHist->Draw("");
   }

   fCanvas->cd(8);//->SetLogz();
   if (fCHist) {
      for(int i = 0; i< fC.GetNrows() ;i++){
         for(int j = 0; j< fC.GetNcols() ;j++){
            Int_t bin = fCHist->GetBin(i,j);
            fCHist->SetBinContent(bin,fC[i][j]);
         }
      }
      fCHist->DrawCopy("colz");
   }
   //fCHist->SetContent(fC.GetMatrixArray());
   //}

   if (!fCanvas2) {
      fCanvas2 = new TCanvas("pi0Calibrator2", "Pi0 Calibrations - 2");
   }
   fCanvas2->cd();
   if (fCHist) {
      fCHist->Draw("colz");
   }

   if (!fCanvas3) {
      fCanvas3 = new TCanvas("pi0Calibrator3", "Pi0 Calibrations - 3");
   }
   fCanvas3->cd();
   if (fCInvHist) {
      for(int i = 0; i< fCInverted.GetNrows() ;i++){
         for(int j = 0; j< fCInverted.GetNcols() ;j++){
            Int_t bin = fCInvHist->GetBin(i,j);
            fCInvHist->SetBinContent(bin,fCInverted[i][j]);
         }
      }
      fCInvHist->Draw("colz");
   }

   //fC.Print();
//    fCanvas->cd(3);
//    if(fLGraph) {
//      delete fLGraph;
//      fLGraph=0;
//    }
//    fLGraph = new TGraph(fNumberOfBlocks,fBlocks,fL.GetMatrixArray());
//    fLGraph->Draw("AP");
//
//    fCanvas->cd(4);
//    if(fDGraph) {
//      delete fDGraph;
//      fDGraph=0;
//    }
//    fDGraph = new TGraph(fNumberOfBlocks,fBlocks,fD.GetMatrixArray());
//    fDGraph->Draw("AP");

   fCanvas->Update();
   /*   gPad->WaitPrimitive();*/
}
//______________________________________________________________________________
Int_t Pi0Calibration::CreateNewCalibrationSeries(Int_t start_run, Int_t end_run){
   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   InSANEDatabaseManager * dbman      = InSANEDatabaseManager::GetManager();
   TSQLServer * db                    = dbman->GetMySQLConnection();
   Int_t series                       = geocalc->GetLatestCalibrationSeries();
   series++;
   TString SQLCOMMAND = "";
   Double_t C_new     = 1.0; 
   for(int i = 1; i<=1744; i++){
      C_new      = GetNewCalibrationCoefficient(i);
      SQLCOMMAND = "INSERT INTO bigcal_calibrations (block_number,coeff,series,start_run,end_run) VALUES (";
      SQLCOMMAND += i;
      SQLCOMMAND += ",";
      SQLCOMMAND += C_new;
      SQLCOMMAND += ",";
      SQLCOMMAND += series;
      SQLCOMMAND += ",";
      SQLCOMMAND += start_run;
      SQLCOMMAND += ",";
      SQLCOMMAND += end_run;
      SQLCOMMAND += ");";
      db->Query(SQLCOMMAND.Data());
   }
   std::cout << " Started with series " << geocalc->GetCalibrationSeries() << std::endl;
   return series;
}
//______________________________________________________________________________
Double_t Pi0Calibration::GetNewCalibrationCoefficient(const Int_t blockNumber) const {
   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   Double_t C_orig = geocalc->GetCalibrationCoefficient(blockNumber);
   Double_t C_old = C_orig;
   if( IsBlockInList(blockNumber) ){
      auto res = fMatrixBlocks.find(blockNumber);
      Int_t i_matrix = (*res).second;
      Double_t  eps = fEpsilon[i_matrix][0];
      C_orig *= (1.0 + eps);
   } else {
      // Use the calibration series that we started with. Set in LoadEpsilonFromSeries.
      // 
      if(fLaterSeries != -1) 
         C_orig = geocalc->GetCalibration_SeriesBlock(fLaterSeries,blockNumber);
   }
   //std::cout << " block: " << blockNumber << " " << 100.0*(C_orig-C_old)/C_orig << " % change " <<std::endl;
   return C_orig;
}
//______________________________________________________________________________
Int_t Pi0Calibration::LoadEpsilonFromSeries(Int_t init_series, Int_t later_series){
   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   InSANEDatabaseManager * dbman      = InSANEDatabaseManager::GetManager();
   TSQLServer * db                    = dbman->GetMySQLConnection();
   fInitSeries  = init_series;
   fLaterSeries = later_series;
   Double_t C1     = 1.0; 
   Double_t C2     = 1.0; 
   Double_t eps    = 0.0; 
   for(int i = 1; i<=1744; i++){
      C1  = 1.0;
      C2  = 1.0;
      eps = 0.0;
      if( IsBlockInList(i) ){
         std::map<int,int>::const_iterator res = fMatrixBlocks.find(i/*-1*/);
         Int_t i_matrix = (*res).second;
         C1      = geocalc->GetCalibration_SeriesBlock(init_series,i);
         C2      = geocalc->GetCalibration_SeriesBlock(later_series,i);
         eps = (C2/C1) - 1.0;
         fEpsilon[i_matrix][0] = eps;
         fEpsilonLast[i_matrix][0] = eps;
      }
   }
   return 0;
}
//______________________________________________________________________________

