#include "Pi0Event.h"

ClassImp(Pi0Event)

//______________________________________________________________________________
Pi0Event::Pi0Event() : fNumberOfPairs(0),fCherenkovHit(false) {

   fReconstruction = new TClonesArray("InSANEReconstruction", 1);
   fClusterPairs   = new TClonesArray("Pi0ClusterPair", 1);
   fClusterPairs->SetOwner(kFALSE);

}
//______________________________________________________________________________
Pi0Event::~Pi0Event() {

}
//______________________________________________________________________________
void Pi0Event::Reset() {
   InSANEPhysicsEvent::Reset();
   fCherenkovHit = false;
   fNTracks      = 0;
}
//______________________________________________________________________________
void Pi0Event::ClearEvent(Option_t * opt) {
   InSANEPhysicsEvent::ClearEvent(opt);

   if (fClusterPairs)if(fClusterPairs->GetEntries() > 6 ) {
      std::cout << " calling  fClusterPairs->ExpandCreate(1); " << " had " << fClusterPairs->GetEntries() << " entries. \n";
      fClusterPairs->ExpandCreate(10);
   }
   if (fClusterPairs) fClusterPairs->Clear(opt);

   if (fReconstruction)if(fReconstruction->GetEntries() > 6 ) {
      std::cout << " calling  fReconstruction->ExpandCreate(1); " << " had " << fReconstruction->GetEntries() << " entries. \n";
      fReconstruction->ExpandCreate(10);
   }
   if (fReconstruction) fReconstruction->Clear(opt);

   fNumberOfPairs = 0;
}
//______________________________________________________________________________
const char * Pi0Event::PrintEvent() const  {
   TString astr;
   std::cout << " InSANEPhysicsEvent::fReconstruction: " << fReconstruction << "\n";
   for (int i = 0; i < fNTracks; i++) {
      astr += Form("M%d = %f", i, ((InSANEReconstruction*)(*fReconstruction)[i])->fMass);
      /*astr+="M";astr+=i;astr+=" = ";astr+=((InSANEReconstruction*)(*fReconstruction)[i])->fMass;astr+=" \n";*/
   }
   return(astr.Data());
}
//______________________________________________________________________________
void Pi0Event::Print(Option_t * opt) const {
   InSANEPhysicsEvent::Print(opt);
   std::cout << PrintEvent() << "\n";
}
//______________________________________________________________________________
Double_t Pi0Event::CalculateMass(Double_t E1, Double_t E2, Double_t theta) {
   return(TMath::Sqrt(2.0 * E1 * E2 * (1.0 - TMath::Cos(theta))));
}
//______________________________________________________________________________

