#include "Pi0EventDisplay.h"

ClassImp(Pi0EventDisplay)

void Pi0EventDisplay::UpdateDisplay()
{

//  if(fTriggerEvent) if(fTriggerEvent->fTriggerBits[fUpdateTriggerBit] /*&& fTriggerEvent->fCodaType == fEventType*/)
   if (1) {

// std::cout << "fCodaType  is " << fTriggerEvent->fCodaType << " \n";

      TH1 * ahist;
      int i = 0;
      fEventSummary->Clear();
      fCanvas->cd(1)->cd(2);
//    fEventSummary->AddText("#frac{2s}{#pi#alpha^{2}}  #frac{d#sigma}{dcos#theta} 
//    (e^{+}e^{-} #rightarrow f#bar{f} ) = #left| #frac{1}{1 - #Delta#alpha} 
//    #right|^{2} (1+cos^{2}#theta)");

//    fEventSummary->AddText("+ 4 Re #left{ #frac{2}{1 - #Delta#alpha} #chi(s) 
//    #[]{#hat{g}_{#nu}^{e}#hat{g}_{#nu}^{f} 
// (1 + cos^{2}#theta) + 2 #hat{g}_{a}^{e}#hat{g}_{a}^{f} cos#theta) } #right}");
//
//    fEventSummary->AddText("+ 16#left|#chi(s)#right|^{2}
//  #left[(#hat{g}_{a}^{e}^{2} + #hat{g}_{v}^{e}^{2})
//  (#hat{g}_{a}^{f}^{2} + #hat{g}_{v}^{f}^{2})(1+cos^{2}#theta)
//  + 8 #hat{g}_{a}^{e} #hat{g}_{a}^{f} #hat{g}_{v}^{e} 
//  #hat{g}_{v}^{f}cos#theta#right] ");

      fEventSummary->SetLabel(" Event");

      for (auto it = fTextDisplay->begin(); it != fTextDisplay->end(); ++it) {
         //fEventSummary->AddText(Form("%20s %s ",(*it)->fName.Data(), (*it)->fEvent->PrintEvent() ) );
      }
      fCanvas->cd(1)->cd(2);
      fEventSummary->Draw();

      fCanvas->cd(2);
      TIter simIter(fSimulationHists);
      while ((ahist = (TH1 *)simIter.Next())) {
         i++;
         fCanvas->cd(2)->cd(i);
         ahist->Draw("colz");
      }

      fCanvas->cd(3);
// fCanvas->cd(4)->Divide(6,1);
      fCanvas->cd(4);

      i = 0;
      TIter myiter(fDataHists);
      while ((ahist = (TH1 *)myiter.Next())) {
         i++;
         fCanvas->cd(4)->cd(i);
         ahist->Draw("colz");
      }

      fCanvas->Update();

      if (fWebDisplay == 0) {
         if (fWaitPrimitive) gPad->WaitPrimitive();
      } else {
         while (fWebDisplay != 0) {
            fCanvas->SaveAs(Form("plots/eventdisplay/event%d.png", fWebDisplay));
            fWebDisplay--;
         }
      }
   }
}
//_______________________________________________________//
