#include "ANNGammaEvent.h"

//______________________________________________________________________________
ANNGammaEvent::ANNGammaEvent(){
   fTrueEnergy       = 0;
   fDelta_Energy     = 0;
   fTrueTheta        = 0;
   fDelta_Theta      = 0;
   fTruePhi          = 0;
   fDelta_Phi        = 0;
   fXCluster         = 0;
   fYCluster         = 0;
   fXClusterSigma    = 0;
   fYClusterSigma    = 0;
   fXClusterSkew     = 0;
   fYClusterSkew     = 0;
   fClusterEnergy    = 0;
   fCherenkovADC     = 0;
   fNClusters        = 0;
   fGoodCherenkovHit = false;
   fBackground       = false;
   fSignal           = false;
   fClusterTheta     = 0.0;
   fClusterPhi       = 0.0;
   fClusterDeltaX = 0;
   fClusterDeltaY = 0;
   fXClusterKurt = 0;
   fYClusterKurt = 0;
   fInputNeurons = "";
}
//______________________________________________________________________________
ANNGammaEvent::~ANNGammaEvent(){
}
//______________________________________________________________________________
void ANNGammaEvent::Clear() {
   fInputNeurons = "";
   fTrueEnergy       = 0;
   fDelta_Energy     = 0;
   fTrueTheta        = 0;
   fDelta_Theta      = 0;
   fTruePhi          = 0;
   fDelta_Phi        = 0;
   fXCluster         = 0;
   fYCluster         = 0;
   fXClusterSigma    = 0;
   fYClusterSigma    = 0;
   fXClusterSkew     = 0;
   fYClusterSkew     = 0;
   fClusterEnergy    = 0;
   fCherenkovADC     = 0;
   fNClusters        = 0;
   fGoodCherenkovHit = false;
   fBackground       = false;
   fSignal           = false;
   fClusterTheta     = 0.0;
   fClusterPhi       = 0.0;
   fClusterDeltaX = 0;
   fClusterDeltaY = 0;
   fXClusterKurt = 0;
   fYClusterKurt = 0;
}
//______________________________________________________________________________
const char * ANNGammaEvent::GetMLPInputNeurons() {
   fInputNeurons = "@fClusterEnergy,@fXCluster,@fYCluster,@fXClusterSigma,@fYClusterSigma";
   fInputNeurons += ",@fXClusterSkew,@fYClusterSkew";
   fInputNeurons += ",@fXClusterKurt,@fYClusterKurt";
   return(fInputNeurons.Data());
}
//______________________________________________________________________________
void ANNGammaEvent::GetMLPInputNeuronArray(Double_t * par) {
   par[0] = fClusterEnergy;
   par[1] = fXCluster;
   par[2] = fYCluster;
   par[3] = fXClusterSigma;
   par[4] = fYClusterSigma;
   par[5] = fXClusterSkew;
   par[6] = fYClusterSkew;
   par[7] = fXClusterKurt;
   par[8] = fYClusterKurt;
}
//______________________________________________________________________________
void ANNGammaEvent::SetEventValues(BIGCALCluster * clust){
   fXCluster         = clust->GetXmoment();
   fYCluster         = clust->GetYmoment();
   fXClusterSigma    = clust->GetXStdDeviation();
   fYClusterSigma    = clust->GetYStdDeviation();
   fXClusterSkew     = clust->GetXSkewness();
   fYClusterSkew     = clust->GetYSkewness();
   fXClusterKurt     = clust->fXKurtosis;
   fYClusterKurt     = clust->fYKurtosis;
   fClusterEnergy    = clust->GetEnergy();
   fCherenkovADC     = clust->fCherenkovBestADCSum;
   fGoodCherenkovHit = clust->fGoodCherenkovTDCHit;
   fClusterTheta     = clust->GetTheta();
   fClusterPhi       = clust->GetPhi();
}
//______________________________________________________________________________

