#include "SANEAnalysisManager.h"
#include "TTreeViewer.h"
#include "TTVSession.h"
#include "TTVLVContainer.h"

ClassImp(SANEAnalysisManager)
//_______________________________________________________//

SANEAnalysisManager * SANEAnalysisManager::fgSANEAnalysisManager = nullptr;
//_______________________________________________________//

SANEAnalysisManager::SANEAnalysisManager()
{
   //std::cout << " + Created SANEAnalysisManager \n";

   SetStandardCuts();

   fInSANERootFolder = nullptr;
   fReplayFolder = nullptr;
   fAnalysesFolder = nullptr;
   fCutsFolder = nullptr;
   fPerpendicularFolder = nullptr;
   fParallelFolder = nullptr;
   fMaterialPropertiesFolder = nullptr;
   fPureCutsFolder = nullptr;
   fRunFolder = nullptr;
}
//_______________________________________________________//
void SANEAnalysisManager::SetStandardCuts() {
   //      fAxe = new InSANECutter();
   //InSANECut * acut = 0;

   fEnergyMinCut = 500.0;
   fEnergyMaxCut = 5900.0;
   //      acut = new InSANECut("bigcalEnergyCut","","Bigcal energy cut");
   //      acut->SetMinimumValue(fEnergyMinCut);
   //      acut->SetMaximumValue(fEnergyMaxCut);
   //      fAxe->AddCut(acut);

   fBigcalTDCMinCut = 325.0;
   fBigcalTDCMaxCut = 650.0;
   //      acut = new InSANECut("bigcalTimingCut","","Bigcal timing cut");
   //     acut->SetMinimumValue(fBigcalTDCMinCut);
   //      acut->SetMaximumValue(fBigcalTDCMaxCut);
   //      fAxe->AddCut(acut);

   fBigcalOverlapSize = 10.0; //cm

   fCherenkovMinADCAlignCut = 0.2;
   fCherenkovMaxADCAlignCut = 4.0;
   //      acut = new InSANECut("cherenkovTimingCut","","Cherenkov timing cut");
   //      acut->SetMinimumValue(fCherenkovMinADCAlignCut);
   //      acut->SetMaximumValue(fCherenkovMaxADCAlignCut);
   //      fAxe->AddCut(acut);

   fCherenkovMinNPECut  =    4.0;
   fCherenkovMaxNPECut  =   60.0;

   fCherenkovMinADCCut  =  400.0;
   fCherenkovMaxADCCut  = 8000.0;

   fCherenkovMinTDCCut  = -2600.0;
   fCherenkovMaxTDCCut  = -1600.0;

   fCherenkovMinTDCAlignCut  = -50.0;
   fCherenkovMaxTDCAlignCut  = 50.0;

   fLuciteMinTDCCut  = -3000.0; //
   fLuciteMaxTDCCut  = -1000.0; //

   fLuciteMinTDCAlignCut  = -150.0; //
   fLuciteMaxTDCAlignCut  = 150.0; //

   fLuciteSumMinTDCCut  = -200.0; //
   fLuciteSumMaxTDCCut  = 200.0;  //
   fLuciteMissDistance  = 25.0;   //cm

   fTrackerMinTDCCut  = -100.0; //cm
   fTrackerMaxTDCCut  = 100.0; //
   fTrackerMissDistance  = 30.0 * 0.3; //cm

   //   if(  LoadCuts("cuts/StandardCuts.root","StandardCuts") ) Error("SetStandardCuts()","Failed to open Standard Cuts!");
   //   else GetAxe()->PrintCuts();
}
//_______________________________________________________//
Int_t SANEAnalysisManager::ProcessPi0()
{

   if (!(fRunQueue->size() > 0)) return(-1);


   return(0);
}
//_______________________________________________________//
Int_t SANEAnalysisManager::QueueUpParallel() {
   std::cout << "tester" << std::endl;
   TString query = "SELECT run_number FROM run_info WHERE target_angle>170 AND target_angle<190 ORDER BY run_number DESC; ";
   TSQLResult * res = InSANEDatabaseManager::GetManager()->GetMySQLConnection()->Query(query.Data());
   TSQLRow * row;
   fRunQueue->clear();
   for(int i = 0 ;i< res->GetRowCount();i++ ){
      row = res->Next();
      fRunQueue->push_back(atoi(row->GetField(0)) );
      delete row;
   }
   delete res;
   return(0);
}
//_______________________________________________________//
Int_t SANEAnalysisManager::QueueUpPerpendicular() {
   std::cout << "tester" << std::endl;
   TString query = " SELECT run_number FROM run_info WHERE  target_angle>70 AND target_angle<100  ORDER BY run_number DESC; ";
   TSQLResult * res = InSANEDatabaseManager::GetManager()->GetMySQLConnection()->Query(query.Data());
   TSQLRow * row;
   fRunQueue->clear();
   for(int i = 0 ;i< res->GetRowCount();i++ ){
      row = res->Next();
      fRunQueue->push_back(atoi(row->GetField(0)) );
      delete row;
   }
   delete res;
   return(0);
}
//_______________________________________________________//
Int_t SANEAnalysisManager::ExecuteLEDCalibration() {
   auto * runManager = (SANERunManager*)SANERunManager::GetRunManager();
   runManager->SetRun(fRunQueue->back());
   if (!(runManager->fRunNumber) || (runManager->fRunNumber) == 0) {
      printf("Please use SetRun(#) first ... aborting");
      return(-1);
   } else {

      gROOT->SetBatch(kTRUE);


      RunHallCReplay();
      runManager->ConvertRawData();
      runManager->CalculatePedestals();
      runManager->GeneratePreRunReport();
      //     runManager->RunZerothPass();
      //     const char * sourceTreeName = "betaDetectorsZerothPass" ;
      //     TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
      //     if( !t ) {printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName); return(-1);}
      //
      // // Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
      //     GasCherenkovLEDAnalyzer * analyzer = new GasCherenkovLEDAnalyzer("betaLED",sourceTreeName );
      //
      // //   GasCherenkovCalculation1 * g1 = new GasCherenkovCalculation1();
      // //     g1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
      // //     g1->SetDetectors(analyzer);
      // //
      // //   BigcalClusteringCalculation1 * b1 = new BigcalClusteringCalculation1();
      // //   analyzer->SetOutputTree( new TTree("Clusters","Cluster Data") );
      // //     b1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
      // //     b1->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
      // //     b1->SetClusterProcessor(new LocalMaxClusterProcessor());
      // //   analyzer->SetClusterProcessor((BIGCALClusterProcessor*)b1->GetClusterProcessor());
      // //     b1->SetDetectors(analyzer);
      //      analyzer->AddDetector(analyzer->fCherenkovDetector);
      // //   analyzer->AddCalculation(g1);
      // //   analyzer->AddCalculation(b1);
      //     analyzer->Initialize(t);
      //     analyzer->Process();
      //     delete analyzer;
      return(0);
   }
   return(0);
}
//_______________________________________________________//
void SANEAnalysisManager::PlotAPara()
{
   auto *c1 = new TCanvas("c1", "A Simple Graph Example", 200, 10, 700, 500);
   // c1->SetFillColor(42);
   c1->SetGrid();
   c1->Divide(1, 2);

   auto * fAsymVsRun = new TH1F("fAsymVsRun", "Asymmetry vs Run", 200, 72900, 73100);

   auto * rman = (SANERunManager*)SANERunManager::GetRunManager();

   std::vector<double> xvals;
   std::vector<double> Avals;
   std::vector<double> PTvals;
   std::vector<double> PBvals;
   std::vector<double> PBTvals;
   std::vector<double> APhysvals;


   std::vector<double> NEventsvals;


   this->QueueUpParallel();

   for (unsigned int i = 0; i < fRunQueue->size(); i++) {
      rman->SetRun(fRunQueue->at(i));

      xvals.push_back(fRunQueue->at(i));

      Avals.push_back(rman->fCurrentRun->fTotalAsymmetry);
      PTvals.push_back(rman->fCurrentRun->fStartingPolarization / 1000.0);
      PBvals.push_back(rman->fCurrentRun->fEndingPolarization / 1000.0);
      PBTvals.push_back(rman->fCurrentRun->fTotalAsymmetry / 1000.0);
      NEventsvals.push_back(rman->fCurrentRun->fNBeta2Events);
      APhysvals.push_back((double)rman->fCurrentRun->fTotalAsymmetry / (rman->fCurrentRun->fStartingPolarization / 100.0));


   }
   //    double * xv;
   //    double * xy;
   // memcpy( xv, &xv[0], sizeof( int ) * myVector.size() );

   Int_t N = xvals.size();

   TVectorD x;
   x.Use(N, &xvals[0]);

   TVectorD a;
   a.Use(N, &Avals[0]);

   TVectorD aphys;
   aphys.Use(N, &APhysvals[0]);

   TVectorD pt;
   pt.Use(N, &PTvals[0]);

   TVectorD pb;
   pb.Use(N, &PBvals[0]);

   TVectorD pbt;
   pbt.Use(N, &PBTvals[0]);

   TVectorD nevents;
   nevents.Use(N, &NEventsvals[0]);


   auto *mg = new TMultiGraph();

   auto * grA = new TGraph(x, aphys);
   grA->SetLineColor(2);
   grA->SetLineWidth(4);
   grA->SetMarkerColor(4);
   grA->SetMarkerSize(1.5);
   grA->SetMarkerStyle(21);
   grA->SetTitle("NEvents vs Run number");
   grA->GetXaxis()->SetTitle("Run Number");
   grA->GetYaxis()->SetTitle("events");
   //grA->Draw("AP");

   auto * grPT = new TGraph(x, pt);
   grPT->SetMarkerColor(1);
   grPT->SetLineColor(2);
   grPT->SetLineWidth(3);
   grPT->SetMarkerSize(1.5);
   grPT->SetMarkerStyle(23);

   auto* grNEvents = new TGraph(x, nevents);
   grNEvents->SetMarkerColor(1);
   grNEvents->SetLineColor(2);
   grNEvents->SetLineWidth(3);
   grNEvents->SetMarkerSize(1.5);
   grNEvents->SetMarkerStyle(23);

   mg->Add(grA, "P");
   mg->Add(grPT, "P");

   c1->cd(1);
   mg->Draw("a");

   c1->cd(2);
   grNEvents->Draw("ap");

}
//_______________________________________________________//

/*
//Int_t SANEAnalysisManager::ExecuteZerothPass(int batch) {
//   if(batch) gROOT->SetBatch(kTRUE);
//   TStopwatch * watch = new TStopwatch();
//   watch->Reset();
//   std::cout << "\n Queue has " << fRunQueue->size()  << " runs to be processed. \n";
//   while(fRunQueue->size() > 0)
//   {
//   std::cout << "\n Processing Run " << fRunQueue->back() << "\n\n";
//   watch->Start(kFALSE);
//
// //fRunManager->RunHallCReplay(); /// \todo this method should be in SANERunManager ?
//
//   std::cout << "Analyzer replay and h2root conversion took " << watch->RealTime() << " seconds\n";
//   watch->Start(kFALSE);
//
//   fRunManager->SetRun(fRunQueue->back());
//   fRunManager->GeneratePreRunReport();
//   fRunManager->ConvertRawData();
//
//   std::cout << "ConvertRawData() took " << watch->RealTime() << " seconds\n";
//   watch->Start(kFALSE);
//
//   fRunManager->CalculatePedestals();
//
//   std::cout << "CalculatePedestals() took " << watch->RealTime() << " seconds\n";
//   watch->Start(kFALSE);
//
//   fRunManager->CalculateTiming();
//
//   std::cout << "CalculateTiming() took " << watch->RealTime() << " seconds\n";
//   watch->Start(kFALSE);
//
//   fRunManager->RunZerothPass("betaDetectors");
//
//   std::cout << "CreateCorrectedTree() took " << watch->RealTime() << " seconds\n";
//   watch->Start(kFALSE);
//
// // First Pass Analysis and Corrections
// //   fRunManager->Cluster("correctedBetaDetectors");
// //
// //   std::cout << "Clustering took " << watch->RealTime() << " seconds\n";
//
//   fRunManager->GenerateRunReport();
//
// // remove run from Queue
//       fRunQueue->pop_back();
//     //  gROOT->Reset();
//     }
//     gROOT->SetBatch(kFALSE);
//    return(0);
//}
 */
//_______________________________________________________//

Int_t SANEAnalysisManager::ExecutePi0()
{
   gROOT->SetBatch(kTRUE);
   auto * watch = new TStopwatch();
   watch->Reset();
   /*
   //    fRunQueue->push_back(72828);
   fRunQueue->push_back(72994);
   //     fRunQueue->push_back(72995);
   //     fRunQueue->push_back(73005);
   //     fRunQueue->push_back(72994);

   std::cout << "\n Queue has " << fRunQueue->size()  << "runs to be processed. \n";

   while(fRunQueue->size() > 0)
   //  for(vector<int>::const_iterator it = fRunQueue->begin(); it != fRunQueue->end(); ++it)
   {
   std::cout << "\n Processing Run " << fRunQueue->back() << "\n\n";

   watch->Start(kFALSE);
   //fRunManager->RunHallCReplay(); /// \todo this method should be in SANERunManager ?
   std::cout << "Analyzer replay and h2root conversion took " << watch->RealTime() << " seconds\n";
   watch->Start(kFALSE);

   fRunManager->SetRun(fRunQueue->back());
   fRunManager->GeneratePreRunReport();
   fRunManager->ConvertRawData();

   std::cout << "ConvertRawData() took " << watch->RealTime() << " seconds\n";
   watch->Start(kFALSE);

   fRunManager->CalculatePedestals();

   std::cout << "CalculatePedestals() took " << watch->RealTime() << " seconds\n";
   watch->Start(kFALSE);

   fRunManager->CalculateTiming();

   std::cout << "CalculateTiming() took " << watch->RealTime() << " seconds\n";
   watch->Start(kFALSE);

   // Zeroth Pass Corrections
   fRunManager->RunZerothPass("betaDetectors");

   std::cout << "CreateCorrectedTree() took " << watch->RealTime() << " seconds\n";
   watch->Start(kFALSE);

   // First Pass Analysis and Corrections

   fRunManager->Cluster("correctedBetaDetectors");

   std::cout << "Clustering took " << watch->RealTime() << " seconds\n";

   fRunManager->GenerateRunReport();

   // remove run from Queue
   fRunQueue->pop_back();
   //  gROOT->Reset();
   }
   gROOT->SetBatch(kFALSE);*/
   return(0);
};
//_______________________________________________________//

Int_t SANEAnalysisManager::View(const char * treeName, const char * fileName)
{

   //   gSystem->Load("libTreeViewer");
   TTreeViewer *treeview = nullptr;
   if (!treeview) treeview = new TTreeViewer();
   auto *tv_tree = (TTree*)gROOT->FindObject(treeName);
   auto *tv_file = (TFile*)gROOT->GetListOfFiles()->FindObject(fileName);
   if (!tv_tree) {
      if (!tv_file) tv_file = new TFile(Form("data/rootfiles/%s", fileName));
      if (tv_file)  tv_tree = (TTree*)tv_file->Get(treeName);
      if (!tv_tree) {
         printf("Tree %s not found", treeName);
         return(0);
      }
   }

   treeview->SetTreeName(treeName);
   treeview->SetNexpressions(21);
   //         Set expressions on axis and cut
   TTVLVEntry *item;
   //   X expression
   item = treeview->ExpressionItem(0);
   item->SetExpression("", "-empty-");
   //   Y expression
   item = treeview->ExpressionItem(1);
   item->SetExpression("", "-empty-");
   //   Z expression
   item = treeview->ExpressionItem(2);
   item->SetExpression("", "-empty-");
   //   Cut expression
   item = treeview->ExpressionItem(3);
   item->SetExpression("", "");
   //         Scan list
   item = treeview->ExpressionItem(4);
   item->SetExpression("", "Scan box");
   //         User defined expressions
   item = treeview->ExpressionItem(5);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000", "~one mirror", kTRUE);
   item = treeview->ExpressionItem(6);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==2&&bigcalClusters.fTotalE>1000", "~two mirror", kTRUE);
   item = treeview->ExpressionItem(7);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==4&&bigcalClusters.fTotalE>1000", "~four mirror", kTRUE);
   item = treeview->ExpressionItem(8);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fPrimaryMirror==1", "~m1", kTRUE);
   item = treeview->ExpressionItem(9);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fPrimaryMirror==2", "~m2", kTRUE);
   item = treeview->ExpressionItem(10);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fPrimaryMirror==3", "~m3", kTRUE);
   item = treeview->ExpressionItem(11);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fPrimaryMirror==4", "~m4", kTRUE);
   item = treeview->ExpressionItem(12);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fPrimaryMirror==5", "~m5", kTRUE);
   item = treeview->ExpressionItem(13);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fPrimaryMirror==6", "~m6", kTRUE);
   item = treeview->ExpressionItem(14);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fPrimaryMirror==7", "~m7", kTRUE);
   item = treeview->ExpressionItem(15);
   item->SetExpression("bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fPrimaryMirror==8", "~m8", kTRUE);
   item = treeview->ExpressionItem(16);
   item->SetExpression("fGasCherenkovEvent->fHits[]->fADC  ", "~ceradcchannel", kTRUE);
   item = treeview->ExpressionItem(17);
   item->SetExpression("fGasCherenkovEvent->fHits[]->fLevel==0", "~cerlevel", kTRUE);
   item = treeview->ExpressionItem(18);
   item->SetExpression("~cerlevel && fGasCherenkovEvent->fGasCherenkovHits->fMirrorNumber==1", "~selectmirror1", kTRUE);
   item = treeview->ExpressionItem(19);
   item->SetExpression("fGasCherenkovEvent->fHits[]->fADC >= 1000 && fGasCherenkovEvent->fHits[]->fADC < 8000 ", "~ceradccut", kTRUE);
   item = treeview->ExpressionItem(20);
   item->SetExpression(" ~ceradccut && ~selectmirror1", "~mirror1adc", kTRUE);
   item = treeview->ExpressionItem(21);
   item->SetExpression("fGasCherenkovEvent->fGasCherenkovHits[]->fPassesTiming[9]", "~timingWide", kTRUE);
   item = treeview->ExpressionItem(22);
   item->SetExpression("~timingWide && ~mirror1tdc", "~timingWithMirror1", kTRUE);
   item = treeview->ExpressionItem(23);
   item->SetExpression("fGasCherenkovEvent->fGasCherenkovHits[]->fLevel==1&& fGasCherenkovEvent->fHits->fMirrorNumber==1", "~mirror1tdc", kTRUE);
   item = treeview->ExpressionItem(24);
   item->SetExpression("fGasCherenkovEvent->fGasCherenkovHits[]->fPassesTiming[1]", "~timingNarrow", kTRUE);
   item = treeview->ExpressionItem(25);
   item->SetExpression("~timingNarrow && ~mirror1tdc", "~narrowTimingWithMirror1", kTRUE);
   //--- session object
   auto * tv_session = new TTVSession(treeview);
   treeview->SetSession(tv_session);
   //--- Connect first record
   tv_session->First();
   return(0);
}
//_______________________________________________________//

Int_t SANEAnalysisManager::ExecuteSimulationAnalysis()
{
   gROOT->SetBatch(kTRUE);
   auto * watch = new TStopwatch();
   watch->Reset();

   std::cout << "\n Queue has " << fRunQueue->size()  << " runs to be processed. \n";

   while (fRunQueue->size() > 0)
      /*  for(vector<int>::const_iterator it = fRunQueue->begin(); it != fRunQueue->end(); ++it)*/
   {
      std::cout << "\n Processing Run " << fRunQueue->back() << "\n\n";

      watch->Start(kFALSE);
      //  /*fRunManager->*/RunHallCReplay(); /// \todo this method should be in SANERunManager ?
      std::cout << "Analyzer replay and h2root conversion took " << watch->RealTime() << " seconds\n";
      watch->Start(kFALSE);

      SANERunManager::GetRunManager()->SetRun(fRunQueue->back());
      SANERunManager::GetRunManager()->GeneratePreRunReport();
      //  fRunManager->ConvertRawData();

      std::cout << "ConvertRawData() took " << watch->RealTime() << " seconds\n";
      watch->Start(kFALSE);

      ((SANERunManager*)SANERunManager::GetRunManager())->CalculatePedestals();

      std::cout << "CalculatePedestals() took " << watch->RealTime() << " seconds\n";
      watch->Start(kFALSE);

      ((SANERunManager*)SANERunManager::GetRunManager())->CalculateTiming();

      std::cout << "CalculateTiming() took " << watch->RealTime() << " seconds\n";
      watch->Start(kFALSE);

      // Zeroth Pass Corrections
      //   fRunManager->RunAlternateFirstPass();

      std::cout << "CreateCorrectedTree() took " << watch->RealTime() << " seconds\n";
      watch->Start(kFALSE);

      // First Pass Analysis and Corrections
      //   fRunManager->Cluster("correctedBetaDetectors");
      //   std::cout << "Clustering took " << watch->RealTime() << " seconds\n";

      InSANERunManager::GetRunManager()->GenerateRunReport();

      // remove run from Queue
      fRunQueue->pop_back();
      //  gROOT->Reset();
   }
   gROOT->SetBatch(kFALSE);
   return(0);
}
//_______________________________________________________//
