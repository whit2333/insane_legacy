#include "InSANETrackPropagator.h"

ClassImp(InSANETrackPropagator)
namespace
{
   const Double_t kBMin     = 1e-6;
   const Double_t kPtMinSqr = 1e-20;
   const Double_t kAMin     = 1e-10;
   const Double_t kStepEps  = 1e-3;
}
//_____________________________________________________________________________

InSANETrackPropagator::InSANETrackPropagator(){
      fEveManager = 0;
      fTrackList  = 0;
      this->SetNameTitle("InSANETrackPropagator","SANE Track Propagator");
      this->SetFitDaughters(kFALSE);
      this->SetMaxZ(500);
      //SetStepper(TEveTrackPropagator::kRungeKutta);

      fH.fMaxAng = 1;
      fH.fDelta  = 0.001;

      fOrigin = TEveVectorD(0.0,0.0,0.0);

      /// Setup the magnetic field
      UVAEveMagField * field = new UVAEveMagField();//new TEveMagFieldConst(0., -10.0, 0.0);
      field->SetPolarizationAngle(TMath::Pi()*180.0/180.0);
      fEveMagField = field;
      fMagField = field;
      this->SetMagFieldObj(fEveMagField,/*prop owns field*/false); // set the eve magnetic field 
      //list->SetElementName(Form("%s, duoB", list->GetElementName()));

      //initialize plane vectors
      fZXPlaneNormal.Set(0.0,1.0,0.0);
      fYZPlaneNormal.Set(1.0,0.0,0.0);
      fXYPlaneNormal.Set(0.0,0.0,1.0);

      fZXPlanePoint.Set(0.0,0.0,0.0);
      fYZPlanePoint.Set(0.0,0.0,0.0);
      fXYPlanePoint.Set(0.0,0.0,0.0);

      // Particle is electron by default
      fParticle = new TParticle();
      fParticle->SetPdgCode(11);
      fParticle->SetMomentum(-1.0*TMath::Sin(40.0*TMath::Pi()/180.0),0.0,-1.0*TMath::Cos(40.0*TMath::Pi()/180.0),TMath::Sqrt(0.000511*0.000511 + 1.0*1.0));
      fParticle->SetProductionVertex(335.0*TMath::Sin(40.0*TMath::Pi()/180.0),0.0,335.0*TMath::Cos(40.0*TMath::Pi()/180.0),/*t=*/0);
      // create the track
      fTrack = new TEveTrack(fParticle,0,this);
   }
//_____________________________________________________________________________

InSANETrackPropagator::~InSANETrackPropagator(){

}
//_____________________________________________________________________________
   void InSANETrackPropagator::PrintIntersections(){
      Double_t x,y,z;
      fTrack->MakeTrack();
      std::cout << "vert = (" << fTrack->GetVertex().fX << "," << fTrack->GetVertex().fY << "," << fTrack->GetVertex().fZ << ")\n";

      TEveVectorD p = fTrack->GetMomentum();

      /// Get the position in a plane near the target.
      std::cout << " Intersections \n";
      if(this->IntersectPlane(p,fZXPlanePoint,fZXPlaneNormal,fZXPlaneResult) ) {
         std::cout << "P(ZX) = (" << fZXPlaneResult.fX << "," << fZXPlaneResult.fY << "," << fZXPlaneResult.fZ << ")\n";
      }
      if(this->IntersectPlane(p,fYZPlanePoint,fYZPlaneNormal,fYZPlaneResult) ) {
         std::cout << "P(YZ) = (" << fYZPlaneResult.fX << "," << fYZPlaneResult.fY << "," << fYZPlaneResult.fZ << ")\n";
      }
      if(this->IntersectPlane(p,fXYPlanePoint,fXYPlaneNormal,fXYPlaneResult) ) {
         std::cout << "P(XY) = (" << fXYPlaneResult.fX << "," << fXYPlaneResult.fY << "," << fXYPlaneResult.fZ << ")\n";
      }

      TEveVectorD origin(0,0,0);
      /// Here we are doing something like TEveTrack::MakeTrack ...
      InitTrack(fTrack->GetVertex(), fTrack->GetCharge());
      Update(fV, p, kTRUE,kTRUE);
      LoopToVertex(p,origin);
      FillPointSet(fTrack);
      ResetTrack();

      //fTrack->MakeTrack();
/*
      std::cout << "Current point : " << this->GetCurrentPoint() << "\n";
      fTrack->GetPoint(this->GetCurrentPoint(),x,y,z);
      std::cout << "p(0) = (" << x << "," << y << "," << z << ")\n";
      std::cout << "mom = (" << fMomentum.fX << "," << fMomentum.fY << "," << fMomentum.fZ << ")\n";
      std::cout << "vert = (" << fVertex.fX << "," << fVertex.fY << "," << fVertex.fZ << ")\n";
*/

   }

//_____________________________________________________________________________

   void InSANETrackPropagator::DrawTrack(){
      if(!fEveManager) fEveManager = TEveManager::Create();
      if(!fTrackList){
         fTrackList = new TEveTrackList();
         fTrackList->SetPropagator(this);
         fTrackList->SetLineColor(kMagenta);
         fEveManager->AddElement(fTrackList);
      }

      // Clone Track because it is reused.
      TEveTrack * clonedTrack = new TEveTrack(*fTrack);
      fTrackList->AddElement(fTrack);
      fTrack->SetLineColor(fTrackList->GetLineColor());
      fTrack = clonedTrack;


/*      PrintIntersections();*/
      //fTrack->MakeTrack();

      TEveViewer *ev = gEve->GetDefaultViewer();
      TGLViewer  *gv = ev->GetGLViewer();
      gv->SetGuideState(TGLUtil::kAxesOrigin, kTRUE, kFALSE, 0);

      fEveManager->Redraw3D(kTRUE);
      //gSystem->ProcessEvents();

      gv->CurrentCamera().RotateRad(-0.5, 1.4);
      gv->RequestDraw();

   }
//_____________________________________________________________________________

Bool_t InSANETrackPropagator::LoopToVertex(TEveVectorD& v, TEveVectorD& p)
{
   // Propagate charged particle with momentum p to vertex v.
   // It is expected that Update() with full-update was called before.

   const Double_t maxRsq = fMaxR * fMaxR;

   TEveVector4D currV(fV);
   TEveVector4D forwV(fV);
   TEveVectorD  forwP(p);

   Int_t first_point = fPoints.size();
   Int_t np          = first_point;

   Double_t prod0=0, prod1;

   do
   {
      Step(currV, p, forwV, forwP);
      Update(forwV, forwP);

      if (PointOverVertex(v, forwV, &prod1))
      {
         break;
      }

      if (IsOutsideBounds(forwV, maxRsq, fMaxZ))
      {
         fV = currV;
         return kFALSE;
      }

      fPoints.push_back(forwV);
      currV = forwV;
      p     = forwP;
      fMomentum = p;
      fVertex = currV;
      prod0 = prod1;
      ++np;
   } while (np < fNMax);

   // make the remaining fractional step
   if (np > first_point)
   {
      if ((v - currV).Mag() > kStepEps)
      {
         Double_t step_frac = prod0 / (prod0 - prod1);
         if (step_frac > 0)
         {
            // Step for fraction of previous step size.
            // We pass 'enforce_max_step' flag to Update().
            Float_t orig_max_step = fH.fMaxStep;
            fH.fMaxStep = step_frac * (forwV - currV).Mag();
            Update(currV, p, kTRUE, kTRUE);
            Step(currV, p, forwV, forwP);
            p     = forwP;
            currV = forwV;
            fPoints.push_back(currV);
            ++np;
            fH.fMaxStep = orig_max_step;
         }

         // Distribute offset to desired crossing point over all segment.

         TEveVectorD off(v - currV);
         off *= 1.0f / currV.fT;
         this->DistributeOffset(off,  first_point, np, p);
         fV = v;
         return kTRUE;
      }
   }

   fPoints.push_back(v);
   fV = v;
   return kTRUE;
}

//______________________________________________________________________________

void InSANETrackPropagator::DistributeOffset(const TEveVectorD& off, Int_t first_point, Int_t np, TEveVectorD& p)
{
   // Distribute offset between first and last point index and rotate
   // momentum.

   // Calculate the required momentum rotation.
   // lpd - last-points-delta
   TEveVectorD lpd0(fPoints[np-1]);
   lpd0 -= fPoints[np-2];
   lpd0.Normalize();

   for (Int_t i = first_point; i < np; ++i)
   {
      fPoints[i] += off * fPoints[i].fT;
   }

   TEveVectorD lpd1(fPoints[np-1]);
   lpd1 -= fPoints[np-2];
   lpd1.Normalize();

   TEveTrans tt;
   tt.SetupFromToVec(lpd0, lpd1);

   // TEveVectorD pb4(p);
   // printf("Rotating momentum: p0 = "); p.Dump();
   tt.RotateIP(p);
   // printf("                   p1 = "); p.Dump();
   // printf("  n1=%f, n2=%f, dp = %f deg\n", pb4.Mag(), p.Mag(),
   //        TMath::RadToDeg()*TMath::ACos(p.Dot(pb4)/(pb4.Mag()*p.Mag())));
}
//_____________________________________________________________________________
