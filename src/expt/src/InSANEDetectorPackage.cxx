#include "InSANEDetectorPackage.h"

ClassImp(InSANEDetectorPackageEvent)

//______________________________________________________________________________
InSANEDetectorPackageEvent::InSANEDetectorPackageEvent() {
   fGoodEvent      = true;
   fNumberOfHits   = 0;
   fNumberOfTracks = 0;
   fEventNumber    = 0;
   fRunNumber      = 0;
}
//______________________________________________________________________________
InSANEDetectorPackageEvent::~InSANEDetectorPackageEvent() {
}
//______________________________________________________________________________
void InSANEDetectorPackageEvent::ClearEvent(const char * opt) {
   fGoodEvent      = true;
   fNumberOfHits   = 0;
   fEventNumber    = 0;
   fNumberOfHits   = 0;
   fNumberOfTracks = 0;
}
//______________________________________________________________________________



ClassImp(InSANEDetectorPackage)

//______________________________________________________________________________
InSANEDetectorPackage::InSANEDetectorPackage(const char * n, const char * t) : TNamed(n,t) {
   fDetectorList  = new TList();
}
//______________________________________________________________________________
InSANEDetectorPackage::~InSANEDetectorPackage() {
}
//______________________________________________________________________________
void InSANEDetectorPackage::SetReferenceTDC(Int_t val) {
   fReferenceTDC = val;
}
//______________________________________________________________________________
Int_t InSANEDetectorPackage::ClearEvent(Option_t * opt) {

   if (fEvent) fEvent->ClearEvent();
   fReferenceTDC = 0;
   fReferenceTime = 0.0;
   TObjLink *lnk = fDetectorList->FirstLink();
   if (lnk) {
      while (lnk) {
         ((InSANEDetector*)(lnk->GetObject()))->ClearEvent();
         lnk = lnk->Next();
      }
      return(0);
   } else {
      return(-1);
   }
   return(0);
}
//______________________________________________________________________________
void InSANEDetectorPackage::SetEventsAddress(InSANEDetectorPackageEvent* event) {
   fEvent = event;
}
//______________________________________________________________________________



ClassImp(InSANEAsymmetryDetectorEvent)

//______________________________________________________________________________
InSANEAsymmetryDetectorEvent::InSANEAsymmetryDetectorEvent(){ }
//______________________________________________________________________________
InSANEAsymmetryDetectorEvent::~InSANEAsymmetryDetectorEvent(){ }
//______________________________________________________________________________
void InSANEAsymmetryDetectorEvent::ClearEvent(const char * opt) {
   fAsymmetry.Clear();
   InSANEDetectorPackageEvent::ClearEvent(opt);
}
//______________________________________________________________________________



ClassImp(InSANEAsymmetryDetector)

//______________________________________________________________________________
InSANEAsymmetryDetector::InSANEAsymmetryDetector(InSANEDetector *  det1, InSANEDetector * det2) {
   if (det1) fDetectorList->Add(det1);
   if (det2) fDetectorList->Add(det2);
   if (fDetectorList->GetEntries() != 2) std::cout << "\n Two detectors are needed !!!!\n" << std::endl;
}
//______________________________________________________________________________
InSANEAsymmetryDetector::~InSANEAsymmetryDetector() {

}
//______________________________________________________________________________
Int_t InSANEAsymmetryDetector::ProcessEvent() {
   if (fDetectorList->GetEntries() == 2)
      for (int i = 0; i < fDetectorList->GetEntries(); i++) {
         //
         if (((InSANEAsymmetryDetectorEvent*)fDetectorList->At(i))->fGoodEvent)
            ((InSANEAsymmetryDetectorEvent*)fDetectorList->At(i))->fAsymmetry.CountEvent();
      }
   return(0);
}
//______________________________________________________________________________




