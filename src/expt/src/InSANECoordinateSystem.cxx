#include "InSANECoordinateSystem.h"


ClassImp(InSANECoordinateSystem)
//______________________________________________________________________________
InSANECoordinateSystem::InSANECoordinateSystem(const char * name ) : TNamed(name, name) {
   fAxisLength = 100.0; // cm
   fRotationMatrix.SetToIdentity();
   fTranslation.SetXYZ(0.0, 0.0, 0.0);
   fVertex = nullptr;
   fXaxis = TVector3(fAxisLength, 0, 0);
   fYaxis = TVector3(0, fAxisLength, 0);
   fZaxis = TVector3(0, 0, fAxisLength);
   fAxisLabelSize = 10;
   fOrigin = TVector3(0, 0, 0);
}
//______________________________________________________________________________
InSANECoordinateSystem::~InSANECoordinateSystem() {
}
//______________________________________________________________________________
void InSANECoordinateSystem::DrawCoordinateSystem(const char * coordsys)
{
   TEveManager * eve = TEveManager::Create();
   TRotation rm;
   TVector3 trans(0, 0, 0);
   rm.SetToIdentity();
   if (coordsys) {
      rm = GetRotationMatrixTo(coordsys);
      trans = GetOriginTranslationTo(coordsys);
   }
   fXaxis *= rm;
   fYaxis *= rm;
   fZaxis *= rm;
   fOrigin += trans;

   fVertex = new TEveVector(fOrigin.X(), fOrigin.Y(), fOrigin.Z());
   /// X axis arrow
   auto* a1 = new TEveArrow(fXaxis.X(), fXaxis.Y(), fXaxis.Z(),
                                 fOrigin.X(), fOrigin.Y(), fOrigin.Z());
   a1->SetMainColor(kBlue);
   a1->SetTubeR(0.01);
   a1->SetPickable(kTRUE);
   eve->AddElement(a1);

   /// X axis Label
   auto* t1 = new TEveText(Form("x_%s", GetName()));
   t1->SetFontSize(fAxisLabelSize);
   TEveVector xLabelPos = (a1->GetVector());
   xLabelPos += (*fVertex);
   t1->RefMainTrans().SetPos(xLabelPos.Arr());
   a1->AddElement(t1);

   /// Y axis arrow
   auto* a2 = new TEveArrow(fYaxis.X(), fYaxis.Y(), fYaxis.Z(),
                                 fOrigin.X(), fOrigin.Y(), fOrigin.Z());
   a2->SetMainColor(kGreen);
   a2->SetTubeR(0.01);
   a2->SetPickable(kTRUE);
   eve->AddElement(a2);

   /// Y axis Label
   auto* t2 = new TEveText(Form("y_%s", GetName()));
   t2->SetFontSize(fAxisLabelSize);
   TEveVector yLabelPos = (a2->GetVector()) ;
   yLabelPos += (*fVertex);
   t2->RefMainTrans().SetPos(yLabelPos.Arr());
   a2->AddElement(t2);

   /// Z axis arrow
   auto* a3 = new TEveArrow(fZaxis.X(), fZaxis.Y(), fZaxis.Z(),
                                 fOrigin.X(), fOrigin.Y(), fOrigin.Z());
   a3->SetMainColor(kOrange);
   a3->SetTubeR(0.01);
   a3->SetPickable(kTRUE);
   eve->AddElement(a3);

   /// Z axis Label
   auto* t3 = new TEveText(Form("z_%s", GetName()));
   t3->SetFontSize(fAxisLabelSize);
   TEveVector zLabelPos = (a3->GetVector()) ;
   zLabelPos += (*fVertex);
   t3->RefMainTrans().SetPos(zLabelPos.Arr());
   a3->AddElement(t3);

   eve->Redraw3D();
}
