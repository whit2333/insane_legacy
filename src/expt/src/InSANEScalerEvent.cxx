#include  "InSANEScalerEvent.h"

ClassImp(InSANEScalerEvent)

InSANEScalerEvent::InSANEScalerEvent()
{
   fScalers = nullptr;
   fNumberOfScalers = 0;
   fTime = 0.0;
   fTotalQ = 0.0;
   fTotalQplus = 0.0;
   fTotalQminus = 0.0;
   ClearEvent();
}


InSANEScalerEvent::~InSANEScalerEvent()
{
   if (fScalers) delete [] fScalers;
}

void InSANEScalerEvent::ClearEvent(const char * opt)
{
   fDeltaT        = 0.0;
   fDeadTime      = 0.0;
   fLiveTime      = 0.0;
   fDeltaQ        = 0.0;
   fDeltaQplus    = 0.0;
   fDeltaQminus   = 0.0;
   fBCM1          = 0.0;
   fBCM2          = 0.0;
   fBeamCurrentAverage = 0.0;
}

void InSANEScalerEvent::SetNumberOfScalers(Int_t num)
{
   fNumberOfScalers = num;
   if (fScalers) delete [] fScalers;
   fScalers = new Long64_t[num];
}
