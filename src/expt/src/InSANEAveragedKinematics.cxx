#include "InSANEAveragedKinematics.h"

InSANESingleBinVariables& InSANESingleBinVariables::operator+=(const InSANESingleBinVariables& rhs)
{
  fN      += rhs.fN      ;
  fx      += rhs.fx      ;
  fW      += rhs.fW      ;
  fQ2     += rhs.fQ2     ;
  fNu     += rhs.fNu     ;
  fE      += rhs.fE      ;
  fTheta  += rhs.fTheta  ;
  fPhi    += rhs.fPhi    ;
  fD      += rhs.fD      ;
  fR      += rhs.fR      ;
  fF1     += rhs.fF1     ;
  fChi    += rhs.fChi    ;
  fEta    += rhs.fEta    ;
  fXi     += rhs.fXi     ;
  fx_2    += rhs.fx_2    ; 
  fW_2    += rhs.fW_2    ; 
  fQ2_2   += rhs.fQ2_2   ; 
  fNu_2   += rhs.fNu_2   ; 
  fE_2    += rhs.fE_2    ; 
  fTheta_2+= rhs.fTheta_2; 
  fPhi_2  += rhs.fPhi_2  ; 
  fD_2    += rhs.fD_2    ; 
  fR_2    += rhs.fR_2    ; 
  fF1_2   += rhs.fF1_2   ; 
  fChi_2  += rhs.fChi_2  ; 
  fEta_2  += rhs.fEta_2  ; 
  fXi_2   += rhs.fXi_2   ; 
  return *this;
}
//______________________________________________________________________________

InSANESingleBinVariables InSANESingleBinVariables::operator+(const InSANESingleBinVariables& rhs)
{
  InSANESingleBinVariables result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
  result += rhs;            // Use += to add other to the copy.
  return result;
}
//______________________________________________________________________________

InSANESingleBinVariables& InSANESingleBinVariables::operator*=(double num)
{

  fN      *= num;
  fx      *= num;
  fW      *= num;
  fQ2     *= num;
  fNu     *= num;
  fE      *= num;
  fTheta  *= num;
  fPhi    *= num;
  fD      *= num;
  fR      *= num;
  fF1     *= num;
  fChi    *= num;
  fEta    *= num;
  fXi     *= num;
  fx_2    *= num; 
  fW_2    *= num; 
  fQ2_2   *= num; 
  fNu_2   *= num; 
  fE_2    *= num; 
  fTheta_2*= num; 
  fPhi_2  *= num; 
  fD_2    *= num; 
  fR_2    *= num; 
  fF1_2   *= num; 
  fChi_2  *= num; 
  fEta_2  *= num; 
  fXi_2   *= num; 
  return *this;
}
//______________________________________________________________________________

InSANESingleBinVariables& InSANESingleBinVariables::operator/=(double den)
{

  fN      /= den;
  fx      /= den;
  fW      /= den;
  fQ2     /= den;
  fNu     /= den;
  fE      /= den;
  fTheta  /= den;
  fPhi    /= den;
  fD      /= den;
  fR      /= den;
  fF1     /= den;
  fChi    /= den;
  fEta    /= den;
  fXi     /= den;
  fx_2    /= den; 
  fW_2    /= den; 
  fQ2_2   /= den; 
  fNu_2   /= den; 
  fE_2    /= den; 
  fTheta_2/= den; 
  fPhi_2  /= den; 
  fD_2    /= den; 
  fR_2    /= den; 
  fF1_2   /= den; 
  fChi_2  /= den; 
  fEta_2  /= den; 
  fXi_2   /= den; 
  return *this;
}
//______________________________________________________________________________

InSANESingleBinVariables InSANESingleBinVariables::operator/(double den)
{
  InSANESingleBinVariables result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
  result /= den;            // Use += to add other to the copy.
  return result;
}
//______________________________________________________________________________

InSANEAveragedKinematics1D::InSANEAveragedKinematics1D(const char * n,const char * t) :
  TNamed(n,t), fCalculated(false)//, fSFs(0)
{ }
//________________________________________________________________________________

InSANEAveragedKinematics1D::~InSANEAveragedKinematics1D()
{ }
//______________________________________________________________________________

InSANEAveragedKinematics1D::InSANEAveragedKinematics1D(const InSANEAveragedKinematics1D& rhs):
  TNamed(rhs)
{
  if(&rhs != this) {
    (*this) = rhs;
  }
}
//______________________________________________________________________________

InSANEAveragedKinematics1D& InSANEAveragedKinematics1D::operator=(const InSANEAveragedKinematics1D& rhs)
{
  if(&rhs != this) {
    bool add_status = TH1::AddDirectoryStatus();
    TNamed::operator=(rhs);
    TH1::AddDirectory(kFALSE);
      fSFs        = nullptr        ;
      fCalculated = rhs.fCalculated ;
      fN          = rhs.fN          ;
      fx          = rhs.fx          ;
      fW          = rhs.fW          ;
      fQ2         = rhs.fQ2         ;
      fNu         = rhs.fNu         ;
      fE          = rhs.fE          ;
      fTheta      = rhs.fTheta      ;
      fPhi        = rhs.fPhi        ;
      fD          = rhs.fD          ;
      fR          = rhs.fR          ;
      fF1         = rhs.fF1         ;
      fChi        = rhs.fChi        ;
      fEta        = rhs.fEta        ;
      fXi         = rhs.fXi         ;
      fx_2        = rhs.fx_2        ;
      fW_2        = rhs.fW_2        ;
      fQ2_2       = rhs.fQ2_2       ;
      fNu_2       = rhs.fNu_2       ;
      fE_2        = rhs.fE_2        ;
      fTheta_2    = rhs.fTheta_2    ;
      fPhi_2      = rhs.fPhi_2      ;
      fD_2        = rhs.fD_2        ;
      fR_2        = rhs.fR_2        ;
      fF1_2       = rhs.fF1_2       ;
      fChi_2      = rhs.fChi_2      ;
      fEta_2      = rhs.fEta_2      ;
      fXi_2       = rhs.fXi_2       ;
   TH1::AddDirectory(add_status);
  }
  return *this;
}
//________________________________________________________________________________

InSANEAveragedKinematics1D&  InSANEAveragedKinematics1D::operator+=(const InSANEAveragedKinematics1D &rhs){
    bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   //std::cout << "test" << std::endl;
   CalcAverage(fx       , fN , rhs.fx       , rhs.fN);
   CalcAverage(fx_2     , fN , rhs.fx_2     , rhs.fN);
   CalcAverage(fW       , fN , rhs.fW       , rhs.fN);
   CalcAverage(fW_2     , fN , rhs.fW_2     , rhs.fN);
   CalcAverage(fQ2      , fN , rhs.fQ2      , rhs.fN);
   CalcAverage(fQ2_2    , fN , rhs.fQ2_2    , rhs.fN);
   CalcAverage(fE       , fN , rhs.fE       , rhs.fN);
   CalcAverage(fE_2     , fN , rhs.fE_2     , rhs.fN);
   CalcAverage(fTheta   , fN , rhs.fTheta   , rhs.fN);
   CalcAverage(fTheta_2 , fN , rhs.fTheta_2 , rhs.fN);
   CalcAverage(fPhi     , fN , rhs.fPhi     , rhs.fN);
   CalcAverage(fPhi_2   , fN , rhs.fPhi_2   , rhs.fN);
   CalcAverage(fR       , fN , rhs.fR       , rhs.fN);
   CalcAverage(fR_2     , fN , rhs.fR_2     , rhs.fN);
   CalcAverage(fF1      , fN , rhs.fF1      , rhs.fN);
   CalcAverage(fF1_2    , fN , rhs.fF1_2    , rhs.fN);
   CalcAverage(fD       , fN , rhs.fD       , rhs.fN);
   CalcAverage(fD_2     , fN , rhs.fD_2     , rhs.fN);
   CalcAverage(fChi     , fN , rhs.fChi     , rhs.fN);
   CalcAverage(fChi_2   , fN , rhs.fChi_2   , rhs.fN);
   CalcAverage(fEta     , fN , rhs.fEta     , rhs.fN);
   CalcAverage(fEta_2   , fN , rhs.fEta_2   , rhs.fN);
   CalcAverage(fXi      , fN , rhs.fXi      , rhs.fN);
   CalcAverage(fXi_2    , fN , rhs.fXi_2    , rhs.fN);
   fN.Add(&rhs.fN);
   //CalcWeightedMean(fx     , fx_2     , rhs.fx     , rhs.fx_2);
   //CalcWeightedMean(fW     , fW_2     , rhs.fW     , rhs.fW_2);
   //CalcWeightedMean(fNu    , fNu_2    , rhs.fNu    , rhs.fNu_2);
   //CalcWeightedMean(fQ2    , fQ2_2    , rhs.fQ2    , rhs.fQ2_2);
   //CalcWeightedMean(fE     , fE_2     , rhs.fE     , rhs.fE_2);
   //CalcWeightedMean(fTheta , fTheta_2 , rhs.fTheta , rhs.fTheta_2);
   //CalcWeightedMean(fPhi   , fPhi_2   , rhs.fPhi   , rhs.fPhi_2);
   //CalcWeightedMean(fR     , fR_2     , rhs.fR     , rhs.fR_2);
   //CalcWeightedMean(fF1     , fF1_2     , rhs.fF1     , rhs.fF1_2);
   //CalcWeightedMean(fD     , fD_2     , rhs.fD     , rhs.fD_2);
   //CalcWeightedMean(fChi   , fChi_2   , rhs.fChi   , rhs.fChi_2);
   //CalcWeightedMean(fEta   , fEta_2   , rhs.fEta   , rhs.fEta_2);
   //CalcWeightedMean(fXi    , fXi_2    , rhs.fXi    , rhs.fXi_2);
   TH1::AddDirectory(add_status);
   return *this;
}
//________________________________________________________________________________

InSANESingleBinVariables InSANEAveragedKinematics1D::GetSingleBin(int bin) const
{
  InSANESingleBinVariables res;
  res.fN       = fN      .GetBinContent(bin);
  res.fx       = fx      .GetBinContent(bin);
  res.fW       = fW      .GetBinContent(bin);
  res.fQ2      = fQ2     .GetBinContent(bin);
  res.fNu      = fNu     .GetBinContent(bin);
  res.fE       = fE      .GetBinContent(bin);
  res.fTheta   = fTheta  .GetBinContent(bin);
  res.fPhi     = fPhi    .GetBinContent(bin);
  res.fD       = fD      .GetBinContent(bin);
  res.fR       = fR      .GetBinContent(bin);
  res.fF1      = fF1     .GetBinContent(bin);
  res.fChi     = fChi    .GetBinContent(bin);
  res.fEta     = fEta    .GetBinContent(bin);
  res.fXi      = fXi     .GetBinContent(bin);
  res.fx_2     = fx_2    .GetBinContent(bin); 
  res.fW_2     = fW_2    .GetBinContent(bin); 
  res.fQ2_2    = fQ2_2   .GetBinContent(bin); 
  res.fNu_2    = fNu_2   .GetBinContent(bin); 
  res.fE_2     = fE_2    .GetBinContent(bin); 
  res.fTheta_2 = fTheta_2.GetBinContent(bin); 
  res.fPhi_2   = fPhi_2  .GetBinContent(bin); 
  res.fD_2     = fD_2    .GetBinContent(bin); 
  res.fR_2     = fR_2    .GetBinContent(bin); 
  res.fF1_2    = fF1_2   .GetBinContent(bin); 
  res.fChi_2   = fChi_2  .GetBinContent(bin); 
  res.fEta_2   = fEta_2  .GetBinContent(bin); 
  res.fXi_2    = fXi_2   .GetBinContent(bin);
  return res;
}
//______________________________________________________________________________
      
void InSANEAveragedKinematics1D::SetSingleBin(int bin, const InSANESingleBinVariables& vars)
{
  fN      .SetBinContent(bin, vars.fN       );
  fx      .SetBinContent(bin, vars.fx       );
  fW      .SetBinContent(bin, vars.fW       );
  fQ2     .SetBinContent(bin, vars.fQ2      );
  fNu     .SetBinContent(bin, vars.fNu      );
  fE      .SetBinContent(bin, vars.fE       );
  fTheta  .SetBinContent(bin, vars.fTheta   );
  fPhi    .SetBinContent(bin, vars.fPhi     );
  fD      .SetBinContent(bin, vars.fD       );
  fR      .SetBinContent(bin, vars.fR       );
  fF1     .SetBinContent(bin, vars.fF1      );
  fChi    .SetBinContent(bin, vars.fChi     );
  fEta    .SetBinContent(bin, vars.fEta     );
  fXi     .SetBinContent(bin, vars.fXi      );
  fx_2    .SetBinContent(bin, vars.fx_2     ); 
  fW_2    .SetBinContent(bin, vars.fW_2     ); 
  fQ2_2   .SetBinContent(bin, vars.fQ2_2    ); 
  fNu_2   .SetBinContent(bin, vars.fNu_2    ); 
  fE_2    .SetBinContent(bin, vars.fE_2     ); 
  fTheta_2.SetBinContent(bin, vars.fTheta_2 ); 
  fPhi_2  .SetBinContent(bin, vars.fPhi_2   ); 
  fD_2    .SetBinContent(bin, vars.fD_2     ); 
  fR_2    .SetBinContent(bin, vars.fR_2     ); 
  fF1_2   .SetBinContent(bin, vars.fF1_2    ); 
  fChi_2  .SetBinContent(bin, vars.fChi_2   ); 
  fEta_2  .SetBinContent(bin, vars.fEta_2   ); 
  fXi_2   .SetBinContent(bin, vars.fXi_2    );
}
//______________________________________________________________________________

const InSANEAveragedKinematics1D  InSANEAveragedKinematics1D::operator+(const InSANEAveragedKinematics1D &other){ 
  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);
  InSANEAveragedKinematics1D result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
  result += other;            // Use += to add other to the copy.
  TH1::AddDirectory(add_status);
  return result;              // All done!
}
//________________________________________________________________________________
void InSANEAveragedKinematics1D::CalcAverage(TH1F& a1, const TH1F& n1, const TH1F& a2, const TH1F& n2){
  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);
   // Calculates the average
   // res = (a1*n1 + a2*n2)/(n1+n2)
   TH1F ntotal(n1);
   ntotal.Add(&n2);

   TH1F a2_copy(a2);
   a2_copy.Multiply(&n2);

   a1.Multiply(&n1);
   a1.Add(&a2_copy);
   a1.Divide(&ntotal);

   //delete ntotal;
   //delete a2_copy;
  TH1::AddDirectory(add_status);
}
//______________________________________________________________________________
void InSANEAveragedKinematics1D::CalcWeightedMean(TH1F * a1, TH1F * s1, const TH1F * a2, const TH1F * s2){
  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);
   // Calculates the weighted mean of two quantities.
   //     x_mean = ( x_1/(sig_1^2) x_1/(sig_2^2) )/( 1/sig_1^2 + 1/sig_2^2 )
   // 1/sig_mean = 1/sig_1^2 + 1/sig_2^2
   // Note : Here s = 1/sig^2
   auto * htemp = (TH1F*)a2->Clone();// create a temp because we cannot modify a2,s2
   a1->Multiply(s1);
   htemp->Multiply(s2); 
   a1->Add(htemp);
   s1->Add(s2); 
   // create a copy without any zeros for dividing 
   auto * htemp2 = (TH1F*)s1->Clone();
   //loop on s1 to make sure there are no zeros before dividing...
   for(int i = 0; i<=htemp2->GetNbinsX(); i++) {
      if( htemp2->GetBinContent(i) == 0.0) htemp2->SetBinContent(i,1.0E-200); 
   }
   a1->Divide(htemp2);
   // Set the error
   for(int i = 0; i<=a1->GetNbinsX(); i++) {
      // Set the Error to the variance of the mean.
      Double_t oneOverSig2 = s1->GetBinContent(i);
      if( oneOverSig2 != 0.0 )a1->SetBinError(i,TMath::Sqrt(1.0/oneOverSig2));
      else a1->SetBinError(i,0.0);
      
      // Set the Error as the average variance.

   }
   delete htemp;
   delete htemp2;
  TH1::AddDirectory(add_status);
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics1D::CreateHistograms(const TH1F& h0){
  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);
   TH1F c = h0; 
   fN = c; 
   fN.SetName(Form("%s-N",GetName()));
   fN.SetTitle(Form("%s-N",GetTitle()));
   fx = c; 
   fx_2 = c;
   fx.SetName(Form("%s-x",GetName()));
   fx.SetTitle(Form("%s-x",GetTitle()));
   fW = c;
   fW_2 = c;
   fW.SetName(Form("%s-W",GetName()));
   fW.SetTitle(Form("%s-W",GetTitle()));
   fNu   = c;
   fNu_2 = c;
   fNu.SetName(Form("%s-Nu",GetName()));
   fNu.SetTitle(Form("%s-Nu",GetTitle()));
   fQ2   = c; 
   fQ2_2 = c; 
   fQ2.SetName(Form("%s-Q2",GetName()));
   fQ2.SetTitle(Form("%s-Q2",GetTitle()));
   fE   = c;
   fE_2 = c;
   fE.SetName(Form("%s-E",GetName()));
   fE.SetTitle(Form("%s-E",GetTitle()));
   fTheta   = c;
   fTheta_2 = c;
   fTheta.SetName(Form("%s-Theta",GetName()));
   fTheta.SetTitle(Form("%s-Theta",GetTitle()));
   fPhi   = c;
   fPhi_2 = c;
   fPhi.SetName(Form("%s-Phi",GetName()));
   fPhi.SetTitle(Form("%s-Phi",GetTitle()));
   fR   = c;
   fR_2 = c;
   fR.SetName(Form("%s-R",GetName()));
   fR.SetTitle(Form("%s-R",GetTitle()));
   fF1   = c;
   fF1_2 = c;
   fF1.SetName(Form("%s-F1",GetName()));
   fF1.SetTitle(Form("%s-F1",GetTitle()));
   fD   = c;
   fD_2 = c;
   fD.SetName(Form("%s-D",GetName()));
   fD.SetTitle(Form("%s-D",GetTitle()));
   fChi   = c;
   fChi_2 = c;
   fChi.SetName(Form("%s-chi",GetName()));
   fChi.SetTitle(Form("%s-chi",GetTitle()));
   fEta   = c;
   fEta_2 = c;
   fEta.SetName(Form("%s-eta",GetName()));
   fEta.SetTitle(Form("%s-eta",GetTitle()));
   fXi   = c;
   fXi_2 = c;
   fXi.SetName(Form("%s-xi",GetName()));
   fXi.SetTitle(Form("%s-xi",GetTitle()));

   fN.Reset();
   fx.Reset();
   fW.Reset();
   fQ2.Reset();
   fNu.Reset();
   fE.Reset();
   fTheta.Reset();
   fPhi.Reset();
   fR.Reset();
   fF1.Reset();
   fD.Reset();
   fChi.Reset();
   fEta.Reset();
   fXi.Reset();
   fx_2.Reset();
   fW_2.Reset();
   fQ2_2.Reset();
   fNu_2.Reset();
   fE_2.Reset();
   fTheta_2.Reset();
   fPhi_2.Reset();
   fR_2.Reset();
   fF1_2.Reset();
   fD_2.Reset();
   fChi_2.Reset();
   fEta_2.Reset();
   fXi_2.Reset();

  TH1::AddDirectory(add_status);
   return 0;
}

//________________________________________________________________________________
void InSANEAveragedKinematics1D::Browse(TBrowser* b) {
   b->Add(&fN);
   b->Add(&fx);
   b->Add(&fW);
   b->Add(&fNu);
   b->Add(&fQ2);
   b->Add(&fE);
   b->Add(&fTheta);
   b->Add(&fPhi);
   b->Add(&fD);
   b->Add(&fR);
   b->Add(&fF1);
   b->Add(&fChi);
   b->Add(&fEta);
   b->Add(&fXi);
}

//________________________________________________________________________________
Int_t InSANEAveragedKinematics1D::AddEvent(Double_t xval, InSANEDISEvent * ev) {
   if(!ev) return(-1);
   Int_t bin = fx.FindFixBin(xval);
   fN.SetBinContent(bin,  fN.GetBinContent(bin) + 1.0 ); 
   fx.SetBinContent(bin,  fx.GetBinContent(bin) + ev->fx ); 
   fW.SetBinContent(bin,  fW.GetBinContent(bin) + ev->fW ); 
   fNu.SetBinContent(bin, fNu.GetBinContent(bin) + (ev->fBeamEnergy - ev->fEnergy) ); 
   fQ2.SetBinContent(bin, fQ2.GetBinContent(bin) + ev->fQ2 ); 
   fE.SetBinContent(bin,  fE.GetBinContent(bin) + ev->fEnergy ); 
   fTheta.SetBinContent(bin,  fTheta.GetBinContent(bin) + ev->fTheta ); 
   fPhi.SetBinContent(bin,  fPhi.GetBinContent(bin) + ev->fPhi ); 
   // value squared
   fx_2.SetBinContent(bin,  fx_2.GetBinContent(bin) + ev->fx*ev->fx ); 
   fW_2.SetBinContent(bin,  fW_2.GetBinContent(bin) + ev->fW*ev->fW ); 
   fNu_2.SetBinContent(bin, fNu_2.GetBinContent(bin) + (ev->fBeamEnergy - ev->fEnergy)*(ev->fBeamEnergy - ev->fEnergy) ); 
   fQ2_2.SetBinContent(bin, fQ2_2.GetBinContent(bin) + ev->fQ2*ev->fQ2 ); 
   fE_2.SetBinContent(bin,  fE_2.GetBinContent(bin) + ev->fEnergy*ev->fEnergy ); 
   fTheta_2.SetBinContent(bin,  fTheta_2.GetBinContent(bin) + ev->fTheta*ev->fTheta ); 
   fPhi_2.SetBinContent(bin,  fPhi_2.GetBinContent(bin) + ev->fPhi*ev->fPhi ); 

   if(!fSFs) fSFs = InSANEFunctionManager::GetInstance()->GetStructureFunctions();

   Double_t R = fSFs->R(ev->fx,ev->fQ2); 
   Double_t F1 = fSFs->F1p(ev->fx,ev->fQ2); 
   Double_t D = InSANE::Kine::D(ev->fBeamEnergy,ev->fEnergy,ev->fTheta,R);
   Double_t chi = InSANE::Kine::Chi(ev->fBeamEnergy,ev->fEnergy,ev->fTheta,ev->fPhi);
   Double_t eta = InSANE::Kine::Eta(ev->fBeamEnergy,ev->fEnergy,ev->fTheta);
   Double_t xi = InSANE::Kine::Xi(ev->fBeamEnergy,ev->fEnergy,ev->fTheta);
   fR.SetBinContent(bin,  fR.GetBinContent(bin) + R ); 
   fF1.SetBinContent(bin,  fF1.GetBinContent(bin) + F1 ); 
   fD.SetBinContent(bin,  fD.GetBinContent(bin) + D ); 
   fChi.SetBinContent(bin,  fChi.GetBinContent(bin) + chi ); 
   fEta.SetBinContent(bin,  fEta.GetBinContent(bin) + eta ); 
   fXi.SetBinContent(bin,  fXi.GetBinContent(bin) + xi ); 

   fR_2.SetBinContent(bin,  fR_2.GetBinContent(bin) + R*R ); 
   fF1_2.SetBinContent(bin,  fF1_2.GetBinContent(bin) + F1*F1 ); 
   fD_2.SetBinContent(bin,  fD_2.GetBinContent(bin) + D*D ); 
   fChi_2.SetBinContent(bin,  fChi_2.GetBinContent(bin) + chi*chi ); 
   fEta_2.SetBinContent(bin,  fEta_2.GetBinContent(bin) + eta*eta ); 
   fXi_2.SetBinContent(bin,  fXi_2.GetBinContent(bin) + xi*xi ); 
   return 0;
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics1D::CalculateErrors(TH1 *h0, TH1* h1){
   if(!h0) return(-1);
   if(!h1) return(-2);
   Double_t x,x2,var;
   for(int i = 0; i<h0->GetNbinsX(); i++) {
      x   = h0->GetBinContent(i);
      x2  = h1->GetBinContent(i);
      var = TMath::Abs(x2-x*x);
      h0->SetBinError(i,TMath::Sqrt(var));
      if(var != 0.0) h1->SetBinContent(i,1.0/var);
      else h1->SetBinContent(i,0.0);
   }
   return(0);
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics1D::Calculate(){
   if( fCalculated ) {
      std::cout << "Kinematics already calculated." << std::endl;
      return 1;
   }
   fx_2.Divide     (&fN);
   fW_2.Divide     (&fN);
   fQ2_2.Divide    (&fN);
   fNu_2.Divide    (&fN);
   fE_2.Divide     (&fN);
   fTheta_2.Divide (&fN);
   fPhi_2.Divide   (&fN);
   fR_2.Divide     (&fN);
   fF1_2.Divide    (&fN);
   fD_2.Divide     (&fN);
   fChi_2.Divide   (&fN);
   fEta_2.Divide   (&fN);
   fXi_2.Divide    (&fN);
   fx.Divide       (&fN);
   fW.Divide       (&fN);
   fQ2.Divide      (&fN);
   fNu.Divide      (&fN);
   fE.Divide       (&fN);
   fTheta.Divide   (&fN);
   fPhi.Divide     (&fN);
   fR.Divide       (&fN);
   fF1.Divide      (&fN);
   fD.Divide       (&fN);
   fChi.Divide     (&fN);
   fEta.Divide     (&fN);
   fXi.Divide      (&fN);
   CalculateErrors(&fx     , &fx_2);
   CalculateErrors(&fW     , &fW_2);
   CalculateErrors(&fQ2    , &fQ2_2);
   CalculateErrors(&fNu    , &fNu_2);
   CalculateErrors(&fE     , &fE_2);
   CalculateErrors(&fTheta , &fTheta_2);
   CalculateErrors(&fPhi   , &fPhi_2);
   CalculateErrors(&fR     , &fR_2);
   CalculateErrors(&fF1    , &fF1_2);
   CalculateErrors(&fD     , &fD_2);
   CalculateErrors(&fChi   , &fChi_2);
   CalculateErrors(&fEta   , &fEta_2);
   CalculateErrors(&fXi    , &fXi_2);
   fCalculated = true;
   return(0);
}
//________________________________________________________________________________



ClassImp(InSANEAveragedKinematics2D)

//________________________________________________________________________________
InSANEAveragedKinematics2D::InSANEAveragedKinematics2D(const char * n,const char * t): TNamed(n,t){
   fSFs   = nullptr ; 
   fN     = nullptr ;
   fx     = nullptr ;
   fW     = nullptr ;
   fQ2    = nullptr ;
   fNu    = nullptr ;
   fE     = nullptr ;
   fTheta = nullptr ;
   fPhi   = nullptr ;
   fR     = nullptr ;
   fF1     = nullptr ;
   fD     = nullptr ;
   fChi   = nullptr ;
   fEta   = nullptr ;
   fXi    = nullptr ;
   fx_2     = nullptr ;
   fW_2     = nullptr ;
   fQ2_2    = nullptr ;
   fNu_2    = nullptr ;
   fE_2     = nullptr ;
   fTheta_2 = nullptr ;
   fPhi_2   = nullptr ;
   fR_2     = nullptr ;
   fF1_2     = nullptr ;
   fD_2     = nullptr ;
   fChi_2   = nullptr ;
   fEta_2   = nullptr ;
   fXi_2    = nullptr ;
   fCalculated = false;
}
//________________________________________________________________________________
InSANEAveragedKinematics2D::~InSANEAveragedKinematics2D(){
   if(fN) delete fN;
   if(fx) delete fx;
   if(fNu) delete fNu;
   if(fQ2) delete fQ2;
   if(fE) delete fE;
   if(fPhi) delete fPhi;
   if(fTheta) delete fTheta;
   if(fR) delete fR;
   if(fF1) delete fF1;
   if(fD) delete fD;
   if(fChi) delete fChi;
   if(fEta) delete fEta;
   if(fXi) delete fXi;
   if(fx_2) delete fx_2;
   if(fNu_2) delete fNu_2;
   if(fQ2_2) delete fQ2_2;
   if(fE_2) delete fE_2;
   if(fPhi_2) delete fPhi_2;
   if(fTheta_2) delete fTheta_2;
   if(fR_2) delete fR_2;
   if(fF1_2) delete fF1_2;
   if(fD_2) delete fD_2;
   if(fChi_2) delete fChi_2;
   if(fEta_2) delete fEta_2;
   if(fXi_2) delete fXi_2;
}
//________________________________________________________________________________
InSANEAveragedKinematics2D::InSANEAveragedKinematics2D(const InSANEAveragedKinematics2D& rhs):TNamed(rhs){
   (*this) = rhs;
}
//________________________________________________________________________________
InSANEAveragedKinematics2D&       InSANEAveragedKinematics2D::operator=(const InSANEAveragedKinematics2D &rhs){ 
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   //TNamed::operator=(rhs);
   if (rhs.fN) fN             = (TH2F*)rhs.fN->Clone();
   if (rhs.fx) fx             = (TH2F*)rhs.fx->Clone();
   if (rhs.fx_2) fx_2         = (TH2F*)rhs.fx_2->Clone();
   if (rhs.fW) fW             = (TH2F*)rhs.fW->Clone();
   if (rhs.fW_2) fW_2         = (TH2F*)rhs.fW_2->Clone();
   if (rhs.fNu) fNu           = (TH2F*)rhs.fNu->Clone();
   if (rhs.fNu_2) fNu_2       = (TH2F*)rhs.fNu_2->Clone();
   if (rhs.fQ2)   fQ2         = (TH2F*)rhs.fQ2->Clone();
   if (rhs.fQ2_2) fQ2_2       = (TH2F*)rhs.fQ2_2->Clone();
   if (rhs.fE)   fE           = (TH2F*)rhs.fE->Clone();
   if (rhs.fE_2) fE_2         = (TH2F*)rhs.fE_2->Clone();
   if (rhs.fTheta)   fTheta   = (TH2F*)rhs.fTheta->Clone();
   if (rhs.fTheta_2) fTheta_2 = (TH2F*)rhs.fTheta_2->Clone();
   if (rhs.fPhi)   fPhi       = (TH2F*)rhs.fPhi->Clone();
   if (rhs.fPhi_2) fPhi_2     = (TH2F*)rhs.fPhi_2->Clone();
   if (rhs.fD)   fD           = (TH2F*)rhs.fD->Clone();
   if (rhs.fD_2) fD_2         = (TH2F*)rhs.fD_2->Clone();
   if (rhs.fR)   fR           = (TH2F*)rhs.fR->Clone();
   if (rhs.fR_2) fR_2         = (TH2F*)rhs.fR_2->Clone();
   if (rhs.fF1)   fF1           = (TH2F*)rhs.fF1->Clone();
   if (rhs.fF1_2) fF1_2         = (TH2F*)rhs.fF1_2->Clone();
   if (rhs.fChi)   fChi       = (TH2F*)rhs.fChi->Clone();
   if (rhs.fChi_2) fChi_2     = (TH2F*)rhs.fChi_2->Clone();
   if (rhs.fEta)   fEta       = (TH2F*)rhs.fEta->Clone();
   if (rhs.fEta_2) fEta_2     = (TH2F*)rhs.fEta_2->Clone();
   if (rhs.fXi)   fXi         = (TH2F*)rhs.fXi->Clone();
   if (rhs.fXi_2) fXi_2       = (TH2F*)rhs.fXi_2->Clone();
   return(*this);
}
//________________________________________________________________________________
InSANEAveragedKinematics2D&      InSANEAveragedKinematics2D::operator+=(const InSANEAveragedKinematics2D &rhs){
    bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   fN->Add(rhs.fN);
   CalcWeightedMean(fx     , fx_2     , rhs.fx     , rhs.fx_2);
   CalcWeightedMean(fW     , fW_2     , rhs.fW     , rhs.fW_2);
   CalcWeightedMean(fNu    , fNu_2    , rhs.fNu    , rhs.fNu_2);
   CalcWeightedMean(fQ2    , fQ2_2    , rhs.fQ2    , rhs.fQ2_2);
   CalcWeightedMean(fE     , fE_2     , rhs.fE     , rhs.fE_2);
   CalcWeightedMean(fTheta , fTheta_2 , rhs.fTheta , rhs.fTheta_2);
   CalcWeightedMean(fPhi   , fPhi_2   , rhs.fPhi   , rhs.fPhi_2);
   CalcWeightedMean(fR     , fR_2     , rhs.fR     , rhs.fR_2);
   CalcWeightedMean(fF1     , fF1_2     , rhs.fF1     , rhs.fF1_2);
   CalcWeightedMean(fD     , fD_2     , rhs.fD     , rhs.fD_2);
   CalcWeightedMean(fChi   , fChi_2   , rhs.fChi   , rhs.fChi_2);
   CalcWeightedMean(fEta   , fEta_2   , rhs.fEta   , rhs.fEta_2);
   CalcWeightedMean(fXi    , fXi_2    , rhs.fXi    , rhs.fXi_2);
   TH1::AddDirectory(add_status);
   return(*this);
}
//________________________________________________________________________________
const InSANEAveragedKinematics2D  InSANEAveragedKinematics2D::operator+(const InSANEAveragedKinematics2D &other){ 
   InSANEAveragedKinematics2D result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
   result += other;            // Use += to add other to the copy.
   return result;              // All done!
}
//________________________________________________________________________________
void InSANEAveragedKinematics2D::CalcWeightedMean(TH2F * a1, TH2F * s1, TH2F * a2, TH2F * s2){
   auto * htemp = (TH2F*)a2->Clone();// create a temp because we cannot modify a2,s2
   a1->Multiply(s1);
   htemp->Multiply(s2); 
   a1->Add(htemp);
   s1->Add(s2); 
   //loop on s1 to make sure there are no zeros before dividing...
   int nx = s1->GetNbinsX();
   int ny = s1->GetNbinsY();
   int nz = s1->GetNbinsZ();
   for(int i = 0; i<=nx; i++) {
      for(int j = 0; j<=ny; j++) {
         for(int k = 0; k<=nz; k++) {
            if( s1->GetBinContent(i,j,k) == 0.0) s1->SetBinContent(i,j,k,0.00001); 
         }
      }
   }
   a1->Divide(s1);
   for(int i = 0; i<=nx; i++) {
      for(int j = 0; j<=ny; j++) {
         for(int k = 0; k<=nz; k++) {
            Double_t oneOverSig2 = s1->GetBinContent(i,j,k);
            a1->SetBinError(i,j,k,TMath::Sqrt(1.0/oneOverSig2));
         }
      }
   }
   delete htemp;
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics2D::CreateHistograms(TH2F * h0){
   fN = new TH2F(*h0); 
   fN->SetName(Form("%s-N",GetName()));
   fN->SetTitle(Form("%s-N",GetTitle()));
   fx = new TH2F(*h0); 
   fx_2 = new TH2F(*h0); 
   fx->SetName(Form("%s-x",GetName()));
   fx->SetTitle(Form("%s-x",GetTitle()));
   fW = new TH2F(*h0); 
   fW_2 = new TH2F(*h0); 
   fW->SetName(Form("%s-W",GetName()));
   fW->SetTitle(Form("%s-W",GetTitle()));
   fNu = new TH2F(*h0); 
   fNu_2 = new TH2F(*h0); 
   fNu->SetName(Form("%s-Nu",GetName()));
   fNu->SetTitle(Form("%s-Nu",GetTitle()));
   fQ2 = new TH2F(*h0); 
   fQ2_2 = new TH2F(*h0); 
   fQ2->SetName(Form("%s-Q2",GetName()));
   fQ2->SetTitle(Form("%s-Q2",GetTitle()));
   fE = new TH2F(*h0); 
   fE_2 = new TH2F(*h0); 
   fE->SetName(Form("%s-E",GetName()));
   fE->SetTitle(Form("%s-E",GetTitle()));
   fTheta = new TH2F(*h0); 
   fTheta_2 = new TH2F(*h0); 
   fTheta->SetName(Form("%s-Theta",GetName()));
   fTheta->SetTitle(Form("%s-Theta",GetTitle()));
   fPhi = new TH2F(*h0); 
   fPhi_2 = new TH2F(*h0); 
   fPhi->SetName(Form("%s-Phi",GetName()));
   fPhi->SetTitle(Form("%s-Phi",GetTitle()));
   fR = new TH2F(*h0); 
   fR_2 = new TH2F(*h0); 
   fR->SetName(Form("%s-R",GetName()));
   fR->SetTitle(Form("%s-R",GetTitle()));
   fF1 = new TH2F(*h0); 
   fF1_2 = new TH2F(*h0); 
   fF1->SetName(Form("%s-F1",GetName()));
   fF1->SetTitle(Form("%s-F1",GetTitle()));
   fD = new TH2F(*h0); 
   fD_2 = new TH2F(*h0); 
   fD->SetName(Form("%s-D",GetName()));
   fD->SetTitle(Form("%s-D",GetTitle()));
   fChi = new TH2F(*h0); 
   fChi_2 = new TH2F(*h0); 
   fChi->SetName(Form("%s-chi",GetName()));
   fChi->SetTitle(Form("%s-chi",GetTitle()));
   fEta = new TH2F(*h0); 
   fEta_2 = new TH2F(*h0); 
   fEta->SetName(Form("%s-eta",GetName()));
   fEta->SetTitle(Form("%s-eta",GetTitle()));
   fXi = new TH2F(*h0); 
   fXi_2 = new TH2F(*h0); 
   fXi->SetName(Form("%s-xi",GetName()));
   fXi->SetTitle(Form("%s-xi",GetTitle()));

   fN->Reset();
   fx->Reset();
   fW->Reset();
   fQ2->Reset();
   fNu->Reset();
   fE->Reset();
   fTheta->Reset();
   fPhi->Reset();
   fR->Reset();
   fF1->Reset();
   fD->Reset();
   fChi->Reset();
   fEta->Reset();
   fXi->Reset();
   fx_2->Reset();
   fW_2->Reset();
   fQ2_2->Reset();
   fNu_2->Reset();
   fE_2->Reset();
   fTheta_2->Reset();
   fPhi_2->Reset();
   fR_2->Reset();
   fF1_2->Reset();
   fD_2->Reset();
   fChi_2->Reset();
   fEta_2->Reset();
   fXi_2->Reset();
   return 0;
}

//________________________________________________________________________________
void InSANEAveragedKinematics2D::Browse(TBrowser* b) {
   if(fN)b->Add(fN);
   if(fx)b->Add(fx);
   if(fW)b->Add(fW);
   if(fNu)b->Add(fNu);
   if(fQ2)b->Add(fQ2);
   if(fE)b->Add(fE);
   if(fTheta)b->Add(fTheta);
   if(fPhi)b->Add(fPhi);
   if(fD)b->Add(fD);
   if(fR)b->Add(fR);
   if(fF1)b->Add(fF1);
   if(fChi)b->Add(fChi);
   if(fEta)b->Add(fEta);
   if(fXi)b->Add(fXi);
}

//________________________________________________________________________________
Int_t InSANEAveragedKinematics2D::AddEvent(Double_t xval,Double_t yval, InSANEDISEvent * ev) {
   if(!ev) return(-1);
   Int_t bin = fx->FindFixBin(xval,yval);
   fN->SetBinContent(bin,  fN->GetBinContent(bin) + 1.0 ); 
   fx->SetBinContent(bin,  fx->GetBinContent(bin) + ev->fx ); 
   fW->SetBinContent(bin,  fW->GetBinContent(bin) + ev->fW ); 
   fNu->SetBinContent(bin, fNu->GetBinContent(bin) + (ev->fBeamEnergy - ev->fEnergy) ); 
   fQ2->SetBinContent(bin, fQ2->GetBinContent(bin) + ev->fQ2 ); 
   fE->SetBinContent(bin,  fE->GetBinContent(bin) + ev->fEnergy ); 
   fTheta->SetBinContent(bin,  fTheta->GetBinContent(bin) + ev->fTheta ); 
   fPhi->SetBinContent(bin,  fPhi->GetBinContent(bin) + ev->fPhi ); 
   // value squared
   fx_2->SetBinContent(bin,  fx_2->GetBinContent(bin) + ev->fx*ev->fx ); 
   fW_2->SetBinContent(bin,  fW_2->GetBinContent(bin) + ev->fW*ev->fW ); 
   fNu_2->SetBinContent(bin, fNu_2->GetBinContent(bin) + (ev->fBeamEnergy - ev->fEnergy)*(ev->fBeamEnergy - ev->fEnergy) ); 
   fQ2_2->SetBinContent(bin, fQ2_2->GetBinContent(bin) + ev->fQ2*ev->fQ2 ); 
   fE_2->SetBinContent(bin,  fE_2->GetBinContent(bin) + ev->fEnergy*ev->fEnergy ); 
   fTheta_2->SetBinContent(bin,  fTheta_2->GetBinContent(bin) + ev->fTheta*ev->fTheta ); 
   fPhi_2->SetBinContent(bin,  fPhi_2->GetBinContent(bin) + ev->fPhi*ev->fPhi ); 

   if(!fSFs) fSFs = InSANEFunctionManager::GetInstance()->GetStructureFunctions();

   Double_t R = fSFs->R(ev->fx,ev->fQ2); 
   Double_t F1= fSFs->F1p(ev->fx,ev->fQ2); 
   Double_t D = InSANE::Kine::D(ev->fBeamEnergy,ev->fEnergy,ev->fTheta,R);
   Double_t chi = InSANE::Kine::Chi(ev->fBeamEnergy,ev->fEnergy,ev->fTheta,ev->fPhi);
   Double_t eta = InSANE::Kine::Eta(ev->fBeamEnergy,ev->fEnergy,ev->fTheta);
   Double_t xi = InSANE::Kine::Xi(ev->fBeamEnergy,ev->fEnergy,ev->fTheta);
   fR->SetBinContent(bin,  fR->GetBinContent(bin) + R ); 
   fF1->SetBinContent(bin,  fF1->GetBinContent(bin) + F1 ); 
   fD->SetBinContent(bin,  fD->GetBinContent(bin) + D ); 
   fChi->SetBinContent(bin,  fChi->GetBinContent(bin) + chi ); 
   fEta->SetBinContent(bin,  fEta->GetBinContent(bin) + eta ); 
   fXi->SetBinContent(bin,  fXi->GetBinContent(bin) + xi ); 

   fR_2->SetBinContent(bin,  fR_2->GetBinContent(bin) + R*R ); 
   fF1_2->SetBinContent(bin,  fF1_2->GetBinContent(bin) + F1*F1 ); 
   fD_2->SetBinContent(bin,  fD_2->GetBinContent(bin) + D*D ); 
   fChi_2->SetBinContent(bin,  fChi_2->GetBinContent(bin) + chi*chi ); 
   fEta_2->SetBinContent(bin,  fEta_2->GetBinContent(bin) + eta*eta ); 
   fXi_2->SetBinContent(bin,  fXi_2->GetBinContent(bin) + xi*xi ); 
   return 0;
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics2D::CalculateErrors(TH2 *h0, TH2* h1){
   if(!h0) return(-1);
   if(!h1) return(-2);
   Double_t x,x2;
   int nx = h0->GetNbinsX();
   int ny = h0->GetNbinsY();
   int nz = h0->GetNbinsZ();
   for(int i = 0; i<=nx; i++) {
      for(int j = 0; j<=ny; j++) {
         for(int k = 0; k<=nz; k++) {
            x  = h0->GetBinContent(i,j,k);
            x2 = h1->GetBinContent(i,j,k);
            h0->SetBinError(i,j,k,TMath::Sqrt(TMath::Abs(x2-x*x)));
            if(x2 != 0.0) h1->SetBinContent(i,j,k,1.0/TMath::Abs(x2-x*x));
            else h1->SetBinContent(i,j,k,0.00001);
         }
      }
   }
   return(0);
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics2D::Calculate(){
   if( fCalculated ) {
      std::cout << "Kinematics already calculated." << std::endl;
      return 1;
   }
   fx_2->Divide(fN);
   fW_2->Divide(fN);
   fQ2_2->Divide(fN);
   fNu_2->Divide(fN);
   fE_2->Divide(fN);
   fTheta_2->Divide(fN);
   fPhi_2->Divide(fN);
   fR_2->Divide(fN);
   fF1_2->Divide(fN);
   fD_2->Divide(fN);
   fChi_2->Divide(fN);
   fEta_2->Divide(fN);
   fXi_2->Divide(fN);
   fx->Divide(fN);
   fW->Divide(fN);
   fQ2->Divide(fN);
   fNu->Divide(fN);
   fE->Divide(fN);
   fTheta->Divide(fN);
   fPhi->Divide(fN);
   fR->Divide(fN);
   fF1->Divide(fN);
   fD->Divide(fN);
   fChi->Divide(fN);
   fEta->Divide(fN);
   fXi->Divide(fN);
   CalculateErrors(fx,fx_2);
   CalculateErrors(fW,fW_2);
   CalculateErrors(fQ2,fQ2_2);
   CalculateErrors(fNu,fNu_2);
   CalculateErrors(fE,fE_2);
   CalculateErrors(fTheta,fTheta_2);
   CalculateErrors(fPhi,fPhi_2);
   CalculateErrors(fR,fR_2);
   CalculateErrors(fF1,fF1_2);
   CalculateErrors(fD,fD_2);
   CalculateErrors(fChi,fChi_2);
   CalculateErrors(fEta,fEta_2);
   CalculateErrors(fXi,fXi_2);

   fCalculated = true;
   return(0);
}
//________________________________________________________________________________



ClassImp(InSANEAveragedKinematics3D)

//________________________________________________________________________________
InSANEAveragedKinematics3D::InSANEAveragedKinematics3D(const char * n,const char * t): TNamed(n,t){
   fSFs   = nullptr ; 
   fN     = nullptr ;
   fx     = nullptr ;
   fW     = nullptr ;
   fQ2    = nullptr ;
   fNu    = nullptr ;
   fE     = nullptr ;
   fTheta = nullptr ;
   fPhi   = nullptr ;
   fR     = nullptr ;
   fF1     = nullptr ;
   fD     = nullptr ;
   fChi   = nullptr ;
   fEta   = nullptr ;
   fXi    = nullptr ;
   fx_2     = nullptr ;
   fW_2     = nullptr ;
   fQ2_2    = nullptr ;
   fNu_2    = nullptr ;
   fE_2     = nullptr ;
   fTheta_2 = nullptr ;
   fPhi_2   = nullptr ;
   fR_2     = nullptr ;
   fF1_2     = nullptr ;
   fD_2     = nullptr ;
   fChi_2   = nullptr ;
   fEta_2   = nullptr ;
   fXi_2    = nullptr ;
   fCalculated = false;
}
//________________________________________________________________________________
InSANEAveragedKinematics3D::~InSANEAveragedKinematics3D(){
   if(fN) delete fN;
   if(fx) delete fx;
   if(fNu) delete fNu;
   if(fQ2) delete fQ2;
   if(fE) delete fE;
   if(fPhi) delete fPhi;
   if(fTheta) delete fTheta;
   if(fF1) delete fF1;
   if(fR) delete fR;
   if(fD) delete fD;
   if(fChi) delete fChi;
   if(fEta) delete fEta;
   if(fXi) delete fXi;
   if(fx_2) delete fx_2;
   if(fNu_2) delete fNu_2;
   if(fQ2_2) delete fQ2_2;
   if(fE_2) delete fE_2;
   if(fPhi_2) delete fPhi_2;
   if(fTheta_2) delete fTheta_2;
   if(fR_2) delete fR_2;
   if(fF1_2) delete fF1_2;
   if(fD_2) delete fD_2;
   if(fChi_2) delete fChi_2;
   if(fEta_2) delete fEta_2;
   if(fXi_2) delete fXi_2;
}
//________________________________________________________________________________
InSANEAveragedKinematics3D::InSANEAveragedKinematics3D(const InSANEAveragedKinematics3D& rhs):TNamed(rhs){
   (*this) = rhs;
}
//________________________________________________________________________________
InSANEAveragedKinematics3D&       InSANEAveragedKinematics3D::operator=(const InSANEAveragedKinematics3D &rhs){ 
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   //TNamed::operator=(rhs);
   if (rhs.fN) fN             = (TH3F*)rhs.fN->Clone();
   if (rhs.fx) fx             = (TH3F*)rhs.fx->Clone();
   if (rhs.fx_2) fx_2         = (TH3F*)rhs.fx_2->Clone();
   if (rhs.fW) fW             = (TH3F*)rhs.fW->Clone();
   if (rhs.fW_2) fW_2         = (TH3F*)rhs.fW_2->Clone();
   if (rhs.fNu) fNu           = (TH3F*)rhs.fNu->Clone();
   if (rhs.fNu_2) fNu_2       = (TH3F*)rhs.fNu_2->Clone();
   if (rhs.fQ2)   fQ2         = (TH3F*)rhs.fQ2->Clone();
   if (rhs.fQ2_2) fQ2_2       = (TH3F*)rhs.fQ2_2->Clone();
   if (rhs.fE)   fE           = (TH3F*)rhs.fE->Clone();
   if (rhs.fE_2) fE_2         = (TH3F*)rhs.fE_2->Clone();
   if (rhs.fTheta)   fTheta   = (TH3F*)rhs.fTheta->Clone();
   if (rhs.fTheta_2) fTheta_2 = (TH3F*)rhs.fTheta_2->Clone();
   if (rhs.fPhi)   fPhi       = (TH3F*)rhs.fPhi->Clone();
   if (rhs.fPhi_2) fPhi_2     = (TH3F*)rhs.fPhi_2->Clone();
   if (rhs.fD)   fD           = (TH3F*)rhs.fD->Clone();
   if (rhs.fD_2) fD_2         = (TH3F*)rhs.fD_2->Clone();
   if (rhs.fR)   fR           = (TH3F*)rhs.fR->Clone();
   if (rhs.fR_2) fR_2         = (TH3F*)rhs.fR_2->Clone();
   if (rhs.fF1)   fF1           = (TH3F*)rhs.fF1->Clone();
   if (rhs.fF1_2) fF1_2         = (TH3F*)rhs.fF1_2->Clone();
   if (rhs.fChi)   fChi       = (TH3F*)rhs.fChi->Clone();
   if (rhs.fChi_2) fChi_2     = (TH3F*)rhs.fChi_2->Clone();
   if (rhs.fEta)   fEta       = (TH3F*)rhs.fEta->Clone();
   if (rhs.fEta_2) fEta_2     = (TH3F*)rhs.fEta_2->Clone();
   if (rhs.fXi)   fXi         = (TH3F*)rhs.fXi->Clone();
   if (rhs.fXi_2) fXi_2       = (TH3F*)rhs.fXi_2->Clone();
   return(*this);
}
//________________________________________________________________________________
InSANEAveragedKinematics3D&      InSANEAveragedKinematics3D::operator+=(const InSANEAveragedKinematics3D &rhs){
    bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   fN->Add(rhs.fN);
   CalcWeightedMean(fx     , fx_2     , rhs.fx     , rhs.fx_2);
   CalcWeightedMean(fW     , fW_2     , rhs.fW     , rhs.fW_2);
   CalcWeightedMean(fNu    , fNu_2    , rhs.fNu    , rhs.fNu_2);
   CalcWeightedMean(fQ2    , fQ2_2    , rhs.fQ2    , rhs.fQ2_2);
   CalcWeightedMean(fE     , fE_2     , rhs.fE     , rhs.fE_2);
   CalcWeightedMean(fTheta , fTheta_2 , rhs.fTheta , rhs.fTheta_2);
   CalcWeightedMean(fPhi   , fPhi_2   , rhs.fPhi   , rhs.fPhi_2);
   CalcWeightedMean(fR     , fR_2     , rhs.fR     , rhs.fR_2);
   CalcWeightedMean(fF1     , fF1_2     , rhs.fF1     , rhs.fF1_2);
   CalcWeightedMean(fD     , fD_2     , rhs.fD     , rhs.fD_2);
   CalcWeightedMean(fChi   , fChi_2   , rhs.fChi   , rhs.fChi_2);
   CalcWeightedMean(fEta   , fEta_2   , rhs.fEta   , rhs.fEta_2);
   CalcWeightedMean(fXi    , fXi_2    , rhs.fXi    , rhs.fXi_2);
   TH1::AddDirectory(add_status);
   return(*this);
}
//________________________________________________________________________________
const InSANEAveragedKinematics3D  InSANEAveragedKinematics3D::operator+(const InSANEAveragedKinematics3D &other){ 
   InSANEAveragedKinematics3D result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
   result += other;            // Use += to add other to the copy.
   return result;              // All done!
}
//________________________________________________________________________________
void InSANEAveragedKinematics3D::CalcWeightedMean(TH3F * a1, TH3F * s1, TH3F * a2, TH3F * s2){
   auto * htemp = (TH3F*)a2->Clone();// create a temp because we cannot modify a2,s2
   a1->Multiply(s1);
   htemp->Multiply(s2); 
   a1->Add(htemp);
   s1->Add(s2); 
   //loop on s1 to make sure there are no zeros before dividing...
   int nx = s1->GetNbinsX();
   int ny = s1->GetNbinsY();
   int nz = s1->GetNbinsZ();
   for(int i = 0; i<=nx; i++) {
      for(int j = 0; j<=ny; j++) {
         for(int k = 0; k<=nz; k++) {
            if( s1->GetBinContent(i,j,k) == 0.0) s1->SetBinContent(i,j,k,0.0000001); 
         }
      }
   }
   a1->Divide(s1);
   for(int i = 0; i<=nx; i++) {
      for(int j = 0; j<=ny; j++) {
         for(int k = 0; k<=nz; k++) {
            Double_t oneOverSig2 = s1->GetBinContent(i,j,k);
            a1->SetBinError(i,j,k,TMath::Sqrt(1.0/oneOverSig2));
         }
      }
   }
   delete htemp;
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics3D::CreateHistograms(TH3F * h0){
   fN = new TH3F(*h0); 
   fN->SetName(Form("%s-N",GetName()));
   fN->SetTitle(Form("%s-N",GetTitle()));
   fx = new TH3F(*h0); 
   fx_2 = new TH3F(*h0); 
   fx->SetName(Form("%s-x",GetName()));
   fx->SetTitle(Form("%s-x",GetTitle()));
   fW = new TH3F(*h0); 
   fW_2 = new TH3F(*h0); 
   fW->SetName(Form("%s-W",GetName()));
   fW->SetTitle(Form("%s-W",GetTitle()));
   fNu = new TH3F(*h0); 
   fNu_2 = new TH3F(*h0); 
   fNu->SetName(Form("%s-Nu",GetName()));
   fNu->SetTitle(Form("%s-Nu",GetTitle()));
   fQ2 = new TH3F(*h0); 
   fQ2_2 = new TH3F(*h0); 
   fQ2->SetName(Form("%s-Q2",GetName()));
   fQ2->SetTitle(Form("%s-Q2",GetTitle()));
   fE = new TH3F(*h0); 
   fE_2 = new TH3F(*h0); 
   fE->SetName(Form("%s-E",GetName()));
   fE->SetTitle(Form("%s-E",GetTitle()));
   fTheta = new TH3F(*h0); 
   fTheta_2 = new TH3F(*h0); 
   fTheta->SetName(Form("%s-Theta",GetName()));
   fTheta->SetTitle(Form("%s-Theta",GetTitle()));
   fPhi = new TH3F(*h0); 
   fPhi_2 = new TH3F(*h0); 
   fPhi->SetName(Form("%s-Phi",GetName()));
   fPhi->SetTitle(Form("%s-Phi",GetTitle()));
   fR = new TH3F(*h0); 
   fR_2 = new TH3F(*h0); 
   fR->SetName(Form("%s-R",GetName()));
   fR->SetTitle(Form("%s-R",GetTitle()));
   fF1 = new TH3F(*h0); 
   fF1_2 = new TH3F(*h0); 
   fF1->SetName(Form("%s-F1",GetName()));
   fF1->SetTitle(Form("%s-F1",GetTitle()));
   fD = new TH3F(*h0); 
   fD_2 = new TH3F(*h0); 
   fD->SetName(Form("%s-D",GetName()));
   fD->SetTitle(Form("%s-D",GetTitle()));
   fChi = new TH3F(*h0); 
   fChi_2 = new TH3F(*h0); 
   fChi->SetName(Form("%s-chi",GetName()));
   fChi->SetTitle(Form("%s-chi",GetTitle()));
   fEta = new TH3F(*h0); 
   fEta_2 = new TH3F(*h0); 
   fEta->SetName(Form("%s-eta",GetName()));
   fEta->SetTitle(Form("%s-eta",GetTitle()));
   fXi = new TH3F(*h0); 
   fXi_2 = new TH3F(*h0); 
   fXi->SetName(Form("%s-xi",GetName()));
   fXi->SetTitle(Form("%s-xi",GetTitle()));

   fN->Reset();
   fx->Reset();
   fW->Reset();
   fQ2->Reset();
   fNu->Reset();
   fE->Reset();
   fTheta->Reset();
   fPhi->Reset();
   fR->Reset();
   fF1->Reset();
   fD->Reset();
   fChi->Reset();
   fEta->Reset();
   fXi->Reset();
   fx_2->Reset();
   fW_2->Reset();
   fQ2_2->Reset();
   fNu_2->Reset();
   fE_2->Reset();
   fTheta_2->Reset();
   fPhi_2->Reset();
   fR_2->Reset();
   fF1_2->Reset();
   fD_2->Reset();
   fChi_2->Reset();
   fEta_2->Reset();
   fXi_2->Reset();
   return 0;
}

//________________________________________________________________________________
void InSANEAveragedKinematics3D::Browse(TBrowser* b) {
   if(fN)b->Add(fN);
   if(fx)b->Add(fx);
   if(fW)b->Add(fW);
   if(fNu)b->Add(fNu);
   if(fQ2)b->Add(fQ2);
   if(fE)b->Add(fE);
   if(fTheta)b->Add(fTheta);
   if(fPhi)b->Add(fPhi);
   if(fD)b->Add(fD);
   if(fR)b->Add(fR);
   if(fF1)b->Add(fF1);
   if(fChi)b->Add(fChi);
   if(fEta)b->Add(fEta);
   if(fXi)b->Add(fXi);
}

//________________________________________________________________________________
Int_t InSANEAveragedKinematics3D::AddEvent(Double_t xval,Double_t yval,Double_t zval, InSANEDISEvent * ev) {
   if(!ev) return(-1);
   Int_t bin = fx->FindFixBin(xval,yval,zval);
   fN->SetBinContent(bin,  fN->GetBinContent(bin) + 1.0 ); 
   fx->SetBinContent(bin,  fx->GetBinContent(bin) + ev->fx ); 
   fW->SetBinContent(bin,  fW->GetBinContent(bin) + ev->fW ); 
   fNu->SetBinContent(bin, fNu->GetBinContent(bin) + (ev->fBeamEnergy - ev->fEnergy) ); 
   fQ2->SetBinContent(bin, fQ2->GetBinContent(bin) + ev->fQ2 ); 
   fE->SetBinContent(bin,  fE->GetBinContent(bin) + ev->fEnergy ); 
   fTheta->SetBinContent(bin,  fTheta->GetBinContent(bin) + ev->fTheta ); 
   fPhi->SetBinContent(bin,  fPhi->GetBinContent(bin) + ev->fPhi ); 
   // value squared
   fx_2->SetBinContent(bin,  fx_2->GetBinContent(bin) + ev->fx*ev->fx ); 
   fW_2->SetBinContent(bin,  fW_2->GetBinContent(bin) + ev->fW*ev->fW ); 
   fNu_2->SetBinContent(bin, fNu_2->GetBinContent(bin) + (ev->fBeamEnergy - ev->fEnergy)*(ev->fBeamEnergy - ev->fEnergy) ); 
   fQ2_2->SetBinContent(bin, fQ2_2->GetBinContent(bin) + ev->fQ2*ev->fQ2 ); 
   fE_2->SetBinContent(bin,  fE_2->GetBinContent(bin) + ev->fEnergy*ev->fEnergy ); 
   fTheta_2->SetBinContent(bin,  fTheta_2->GetBinContent(bin) + ev->fTheta*ev->fTheta ); 
   fPhi_2->SetBinContent(bin,  fPhi_2->GetBinContent(bin) + ev->fPhi*ev->fPhi ); 

   if(!fSFs) fSFs = InSANEFunctionManager::GetInstance()->GetStructureFunctions();

   Double_t R = fSFs->R(ev->fx,ev->fQ2); 
   Double_t F1= fSFs->F1p(ev->fx,ev->fQ2); 
   Double_t D = InSANE::Kine::D(ev->fBeamEnergy,ev->fEnergy,ev->fTheta,R);
   Double_t chi = InSANE::Kine::Chi(ev->fBeamEnergy,ev->fEnergy,ev->fTheta,ev->fPhi);
   Double_t eta = InSANE::Kine::Eta(ev->fBeamEnergy,ev->fEnergy,ev->fTheta);
   Double_t xi = InSANE::Kine::Xi(ev->fBeamEnergy,ev->fEnergy,ev->fTheta);
   fR->SetBinContent(bin,  fR->GetBinContent(bin) + R ); 
   fF1->SetBinContent(bin,  fF1->GetBinContent(bin) + F1 );
   fD->SetBinContent(bin,  fD->GetBinContent(bin) + D ); 
   fChi->SetBinContent(bin,  fChi->GetBinContent(bin) + chi ); 
   fEta->SetBinContent(bin,  fEta->GetBinContent(bin) + eta ); 
   fXi->SetBinContent(bin,  fXi->GetBinContent(bin) + xi ); 

   fR_2->SetBinContent(bin,  fR_2->GetBinContent(bin) + R*R ); 
   fF1_2->SetBinContent(bin,  fF1_2->GetBinContent(bin) + F1*F1 ); 
   fD_2->SetBinContent(bin,  fD_2->GetBinContent(bin) + D*D ); 
   fChi_2->SetBinContent(bin,  fChi_2->GetBinContent(bin) + chi*chi ); 
   fEta_2->SetBinContent(bin,  fEta_2->GetBinContent(bin) + eta*eta ); 
   fXi_2->SetBinContent(bin,  fXi_2->GetBinContent(bin) + xi*xi ); 
   return 0;
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics3D::CalculateErrors(TH3 *h0, TH3* h1){
   if(!h0) return(-1);
   if(!h1) return(-2);
   Double_t x,x2;
   int nx = h0->GetNbinsX();
   int ny = h0->GetNbinsY();
   int nz = h0->GetNbinsZ();
   for(int i = 0; i<=nx; i++) {
      for(int j = 0; j<=ny; j++) {
         for(int k = 0; k<=nz; k++) {
            x  = h0->GetBinContent(i,j,k);
            x2 = h1->GetBinContent(i,j,k);
            h0->SetBinError(i,j,k,TMath::Sqrt(TMath::Abs(x2-x*x)));
            if(x2 != 0.0) h1->SetBinContent(i,j,k,1.0/TMath::Abs(x2-x*x));
            else h1->SetBinContent(i,j,k,0.00001);
         }
      }
   }
   return(0);
}
//________________________________________________________________________________
Int_t InSANEAveragedKinematics3D::Calculate(){
   if( fCalculated ) {
      std::cout << "Kinematics already calculated." << std::endl;
      return 1;
   }
   fx_2->Divide(fN);
   fW_2->Divide(fN);
   fQ2_2->Divide(fN);
   fNu_2->Divide(fN);
   fE_2->Divide(fN);
   fTheta_2->Divide(fN);
   fPhi_2->Divide(fN);
   fR_2->Divide(fN);
   fF1_2->Divide(fN);
   fD_2->Divide(fN);
   fChi_2->Divide(fN);
   fEta_2->Divide(fN);
   fXi_2->Divide(fN);
   fx->Divide(fN);
   fW->Divide(fN);
   fQ2->Divide(fN);
   fNu->Divide(fN);
   fE->Divide(fN);
   fTheta->Divide(fN);
   fPhi->Divide(fN);
   fR->Divide(fN);
   fF1->Divide(fN);
   fD->Divide(fN);
   fChi->Divide(fN);
   fEta->Divide(fN);
   fXi->Divide(fN);
   CalculateErrors(fx,fx_2);
   CalculateErrors(fW,fW_2);
   CalculateErrors(fQ2,fQ2_2);
   CalculateErrors(fNu,fNu_2);
   CalculateErrors(fE,fE_2);
   CalculateErrors(fTheta,fTheta_2);
   CalculateErrors(fPhi,fPhi_2);
   CalculateErrors(fR,fR_2);
   CalculateErrors(fF1,fF1_2);
   CalculateErrors(fD,fD_2);
   CalculateErrors(fChi,fChi_2);
   CalculateErrors(fEta,fEta_2);
   CalculateErrors(fXi,fXi_2);

   fCalculated = true;
   return(0);
}
//________________________________________________________________________________

ClassImp(InSANEAveragedPi0Kinematics1D)

//________________________________________________________________________________
InSANEAveragedPi0Kinematics1D::InSANEAveragedPi0Kinematics1D(const char * n,const char * t): TNamed(n,t){
   //fNAdded = 0;
   fSFs   = nullptr ; 
   fN     = nullptr ;
   fx     = nullptr ;
   fW     = nullptr ;
   fPt    = nullptr ;
   fNu    = nullptr ;
   fE     = nullptr ;
   fTheta = nullptr ;
   fPhi   = nullptr ;
   fR     = nullptr ;
   fF1    = nullptr ;
   fD     = nullptr ;
   fChi   = nullptr ;
   fEta   = nullptr ;
   fXi    = nullptr ;
   fx_2     = nullptr ;
   fW_2     = nullptr ;
   fPt_2    = nullptr ;
   fNu_2    = nullptr ;
   fE_2     = nullptr ;
   fTheta_2 = nullptr ;
   fPhi_2   = nullptr ;
   fR_2     = nullptr ;
   fF1_2    = nullptr ;
   fD_2     = nullptr ;
   fChi_2   = nullptr ;
   fEta_2   = nullptr ;
   fXi_2    = nullptr ;
   fCalculated = false;
}

//________________________________________________________________________________
InSANEAveragedPi0Kinematics1D::~InSANEAveragedPi0Kinematics1D(){
   if(fN) delete fN;
   if(fx) delete fx;
   if(fNu) delete fNu;
   if(fPt) delete fPt;
   if(fE) delete fE;
   if(fPhi) delete fPhi;
   if(fTheta) delete fTheta;
   if(fR) delete fR;
   if(fF1) delete fF1;
   if(fD) delete fD;
   if(fChi) delete fChi;
   if(fEta) delete fEta;
   if(fXi) delete fXi;
   if(fx_2) delete fx_2;
   if(fNu_2) delete fNu_2;
   if(fPt_2) delete fPt_2;
   if(fE_2) delete fE_2;
   if(fPhi_2) delete fPhi_2;
   if(fTheta_2) delete fTheta_2;
   if(fR_2) delete fR_2;
   if(fF1_2) delete fF1_2;
   if(fD_2) delete fD_2;
   if(fChi_2) delete fChi_2;
   if(fEta_2) delete fEta_2;
   if(fXi_2) delete fXi_2;
}
//________________________________________________________________________________
InSANEAveragedPi0Kinematics1D::InSANEAveragedPi0Kinematics1D(const InSANEAveragedPi0Kinematics1D& rhs) : TNamed(rhs){
   (*this) = rhs;
}
//________________________________________________________________________________
InSANEAveragedPi0Kinematics1D& InSANEAveragedPi0Kinematics1D::operator=(const InSANEAveragedPi0Kinematics1D &rhs){ 
   if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.
   //TNamed::operator=(rhs);
   if (rhs.fN) fN             = (TH1F*)rhs.fN->Clone();
   if (rhs.fx) fx             = (TH1F*)rhs.fx->Clone();
   if (rhs.fx_2) fx_2         = (TH1F*)rhs.fx_2->Clone();
   if (rhs.fW) fW             = (TH1F*)rhs.fW->Clone();
   if (rhs.fW_2) fW_2         = (TH1F*)rhs.fW_2->Clone();
   if (rhs.fNu) fNu           = (TH1F*)rhs.fNu->Clone();
   if (rhs.fNu_2) fNu_2       = (TH1F*)rhs.fNu_2->Clone();
   if (rhs.fPt)   fPt         = (TH1F*)rhs.fPt->Clone();
   if (rhs.fPt_2) fPt_2       = (TH1F*)rhs.fPt_2->Clone();
   if (rhs.fE)   fE           = (TH1F*)rhs.fE->Clone();
   if (rhs.fE_2) fE_2         = (TH1F*)rhs.fE_2->Clone();
   if (rhs.fTheta)   fTheta   = (TH1F*)rhs.fTheta->Clone();
   if (rhs.fTheta_2) fTheta_2 = (TH1F*)rhs.fTheta_2->Clone();
   if (rhs.fPhi)   fPhi       = (TH1F*)rhs.fPhi->Clone();
   if (rhs.fPhi_2) fPhi_2     = (TH1F*)rhs.fPhi_2->Clone();
   if (rhs.fD)   fD           = (TH1F*)rhs.fD->Clone();
   if (rhs.fD_2) fD_2         = (TH1F*)rhs.fD_2->Clone();
   if (rhs.fR)   fR           = (TH1F*)rhs.fR->Clone();
   if (rhs.fF1)  fF1           = (TH1F*)rhs.fF1->Clone();
   if (rhs.fR_2) fR_2         = (TH1F*)rhs.fR_2->Clone();
   if (rhs.fF1_2) fF1_2         = (TH1F*)rhs.fF1_2->Clone();
   if (rhs.fChi)   fChi       = (TH1F*)rhs.fChi->Clone();
   if (rhs.fChi_2) fChi_2     = (TH1F*)rhs.fChi_2->Clone();
   if (rhs.fEta)   fEta       = (TH1F*)rhs.fEta->Clone();
   if (rhs.fEta_2) fEta_2     = (TH1F*)rhs.fEta_2->Clone();
   if (rhs.fXi)   fXi         = (TH1F*)rhs.fXi->Clone();
   if (rhs.fXi_2) fXi_2       = (TH1F*)rhs.fXi_2->Clone();
   return *this;
}
//________________________________________________________________________________
InSANEAveragedPi0Kinematics1D&      InSANEAveragedPi0Kinematics1D::operator+=(const InSANEAveragedPi0Kinematics1D &rhs){
    bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   //fNAdded++;
   //std::cout << "test" << std::endl;
   CalcAverage(fx   , fN , rhs.fx   , rhs.fN);
   CalcAverage(fx_2 , fN , rhs.fx_2 , rhs.fN);
   CalcAverage(fW   , fN , rhs.fW   , rhs.fN);
   CalcAverage(fW_2 , fN , rhs.fW_2 , rhs.fN);
   CalcAverage(fPt   , fN , rhs.fPt   , rhs.fN);
   CalcAverage(fPt_2 , fN , rhs.fPt_2 , rhs.fN);
   CalcAverage(fE   , fN , rhs.fE   , rhs.fN);
   CalcAverage(fE_2 , fN , rhs.fE_2 , rhs.fN);
   CalcAverage(fTheta   , fN , rhs.fTheta   , rhs.fN);
   CalcAverage(fTheta_2 , fN , rhs.fTheta_2 , rhs.fN);
   CalcAverage(fPhi   , fN , rhs.fPhi   , rhs.fN);
   CalcAverage(fPhi_2 , fN , rhs.fPhi_2 , rhs.fN);
   CalcAverage(fR   , fN , rhs.fR   , rhs.fN);
   CalcAverage(fR_2 , fN , rhs.fR_2 , rhs.fN);
   CalcAverage(fF1   , fN , rhs.fF1   , rhs.fN);
   CalcAverage(fF1_2 , fN , rhs.fF1_2 , rhs.fN);
   CalcAverage(fD   , fN , rhs.fD   , rhs.fN);
   CalcAverage(fD_2 , fN , rhs.fD_2 , rhs.fN);
   CalcAverage(fChi   , fN , rhs.fChi   , rhs.fN);
   CalcAverage(fChi_2 , fN , rhs.fChi_2 , rhs.fN);
   CalcAverage(fEta   , fN , rhs.fEta   , rhs.fN);
   CalcAverage(fEta_2 , fN , rhs.fEta_2 , rhs.fN);
   CalcAverage(fXi   , fN , rhs.fXi   , rhs.fN);
   CalcAverage(fXi_2 , fN , rhs.fXi_2 , rhs.fN);
   fN->Add(rhs.fN);
   //CalcWeightedMean(fx     , fx_2     , rhs.fx     , rhs.fx_2);
   //CalcWeightedMean(fW     , fW_2     , rhs.fW     , rhs.fW_2);
   //CalcWeightedMean(fNu    , fNu_2    , rhs.fNu    , rhs.fNu_2);
   //CalcWeightedMean(fPt    , fPt_2    , rhs.fPt    , rhs.fPt_2);
   //CalcWeightedMean(fE     , fE_2     , rhs.fE     , rhs.fE_2);
   //CalcWeightedMean(fTheta , fTheta_2 , rhs.fTheta , rhs.fTheta_2);
   //CalcWeightedMean(fPhi   , fPhi_2   , rhs.fPhi   , rhs.fPhi_2);
   //CalcWeightedMean(fR     , fR_2     , rhs.fR     , rhs.fR_2);
   //CalcWeightedMean(fF1     , fF1_2     , rhs.fF1     , rhs.fF1_2);
   //CalcWeightedMean(fD     , fD_2     , rhs.fD     , rhs.fD_2);
   //CalcWeightedMean(fChi   , fChi_2   , rhs.fChi   , rhs.fChi_2);
   //CalcWeightedMean(fEta   , fEta_2   , rhs.fEta   , rhs.fEta_2);
   //CalcWeightedMean(fXi    , fXi_2    , rhs.fXi    , rhs.fXi_2);
   TH1::AddDirectory(add_status);
   return *this;
}
//________________________________________________________________________________
const InSANEAveragedPi0Kinematics1D  InSANEAveragedPi0Kinematics1D::operator+(const InSANEAveragedPi0Kinematics1D &other){ 
   InSANEAveragedPi0Kinematics1D result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
   result += other;            // Use += to add other to the copy.
   return result;              // All done!
}
//________________________________________________________________________________
void InSANEAveragedPi0Kinematics1D::CalcAverage(TH1F * a1, const TH1F * n1, const TH1F * a2, const TH1F * n2){
   // Calculates the average
   // res = (a1*n1 + a2*n2)/(n1+n2)
   auto * ntotal = (TH1F*)n1->Clone();
   ntotal->Add(n2);

   auto * a2_copy = (TH1F*)a2->Clone();
   a2_copy->Multiply(n2);

   a1->Multiply(n1);
   a1->Add(a2_copy);
   a1->Divide(ntotal);

   delete ntotal;
   delete a2_copy;
}
//______________________________________________________________________________
void InSANEAveragedPi0Kinematics1D::CalcWeightedMean(TH1F * a1, TH1F * s1, const TH1F * a2, const TH1F * s2){
   // Calculates the weighted mean of two quantities.
   //     x_mean = ( x_1/(sig_1^2) x_1/(sig_2^2) )/( 1/sig_1^2 + 1/sig_2^2 )
   // 1/sig_mean = 1/sig_1^2 + 1/sig_2^2
   // Note : Here s = 1/sig^2
   auto * htemp = (TH1F*)a2->Clone();// create a temp because we cannot modify a2,s2
   a1->Multiply(s1);
   htemp->Multiply(s2); 
   a1->Add(htemp);
   s1->Add(s2); 
   // create a copy without any zeros for dividing 
   auto * htemp2 = (TH1F*)s1->Clone();
   //loop on s1 to make sure there are no zeros before dividing...
   for(int i = 0; i<=htemp2->GetNbinsX(); i++) {
      if( htemp2->GetBinContent(i) == 0.0) htemp2->SetBinContent(i,1.0E-200); 
   }
   a1->Divide(htemp2);
   // Set the error
   for(int i = 0; i<=a1->GetNbinsX(); i++) {
      // Set the Error to the variance of the mean.
      Double_t oneOverSig2 = s1->GetBinContent(i);
      if( oneOverSig2 != 0.0 )a1->SetBinError(i,TMath::Sqrt(1.0/oneOverSig2));
      else a1->SetBinError(i,0.0);
      
      // Set the Error as the average variance.

   }
   delete htemp;
   delete htemp2;
}
//________________________________________________________________________________
Int_t InSANEAveragedPi0Kinematics1D::CreateHistograms(TH1F * h0){
   fN = new TH1F(*h0); 
   fN->SetName(Form("%s-N",GetName()));
   fN->SetTitle(Form("%s-N",GetTitle()));
   fx = new TH1F(*h0); 
   fx_2 = new TH1F(*h0); 
   fx->SetName(Form("%s-x",GetName()));
   fx->SetTitle(Form("%s-x",GetTitle()));
   fW = new TH1F(*h0); 
   fW_2 = new TH1F(*h0); 
   fW->SetName(Form("%s-W",GetName()));
   fW->SetTitle(Form("%s-W",GetTitle()));
   fNu = new TH1F(*h0); 
   fNu_2 = new TH1F(*h0); 
   fNu->SetName(Form("%s-Nu",GetName()));
   fNu->SetTitle(Form("%s-Nu",GetTitle()));
   fPt = new TH1F(*h0); 
   fPt_2 = new TH1F(*h0); 
   fPt->SetName(Form("%s-Pt",GetName()));
   fPt->SetTitle(Form("%s-Pt",GetTitle()));
   fE = new TH1F(*h0); 
   fE_2 = new TH1F(*h0); 
   fE->SetName(Form("%s-E",GetName()));
   fE->SetTitle(Form("%s-E",GetTitle()));
   fTheta = new TH1F(*h0); 
   fTheta_2 = new TH1F(*h0); 
   fTheta->SetName(Form("%s-Theta",GetName()));
   fTheta->SetTitle(Form("%s-Theta",GetTitle()));
   fPhi = new TH1F(*h0); 
   fPhi_2 = new TH1F(*h0); 
   fPhi->SetName(Form("%s-Phi",GetName()));
   fPhi->SetTitle(Form("%s-Phi",GetTitle()));
   fR = new TH1F(*h0); 
   fR_2 = new TH1F(*h0); 
   fR->SetName(Form("%s-R",GetName()));
   fR->SetTitle(Form("%s-R",GetTitle()));
   fF1 = new TH1F(*h0); 
   fF1_2 = new TH1F(*h0); 
   fF1->SetName(Form("%s-F1",GetName()));
   fF1->SetTitle(Form("%s-F1",GetTitle()));
   fD = new TH1F(*h0); 
   fD_2 = new TH1F(*h0); 
   fD->SetName(Form("%s-D",GetName()));
   fD->SetTitle(Form("%s-D",GetTitle()));
   fChi = new TH1F(*h0); 
   fChi_2 = new TH1F(*h0); 
   fChi->SetName(Form("%s-chi",GetName()));
   fChi->SetTitle(Form("%s-chi",GetTitle()));
   fEta = new TH1F(*h0); 
   fEta_2 = new TH1F(*h0); 
   fEta->SetName(Form("%s-eta",GetName()));
   fEta->SetTitle(Form("%s-eta",GetTitle()));
   fXi = new TH1F(*h0); 
   fXi_2 = new TH1F(*h0); 
   fXi->SetName(Form("%s-xi",GetName()));
   fXi->SetTitle(Form("%s-xi",GetTitle()));

   fN->Reset();
   fx->Reset();
   fW->Reset();
   fPt->Reset();
   fNu->Reset();
   fE->Reset();
   fTheta->Reset();
   fPhi->Reset();
   fR->Reset();
   fF1->Reset();
   fD->Reset();
   fChi->Reset();
   fEta->Reset();
   fXi->Reset();
   fx_2->Reset();
   fW_2->Reset();
   fPt_2->Reset();
   fNu_2->Reset();
   fE_2->Reset();
   fTheta_2->Reset();
   fPhi_2->Reset();
   fR_2->Reset();
   fF1_2->Reset();
   fD_2->Reset();
   fChi_2->Reset();
   fEta_2->Reset();
   fXi_2->Reset();
   return 0;
}

//________________________________________________________________________________
void InSANEAveragedPi0Kinematics1D::Browse(TBrowser* b) {
   if(fN)b->Add(fN);
   if(fx)b->Add(fx);
   if(fW)b->Add(fW);
   if(fNu)b->Add(fNu);
   if(fPt)b->Add(fPt);
   if(fE)b->Add(fE);
   if(fTheta)b->Add(fTheta);
   if(fPhi)b->Add(fPhi);
   if(fD)b->Add(fD);
   if(fR)b->Add(fR);
   if(fF1)b->Add(fF1);
   if(fChi)b->Add(fChi);
   if(fEta)b->Add(fEta);
   if(fXi)b->Add(fXi);
}

//________________________________________________________________________________
Int_t InSANEAveragedPi0Kinematics1D::AddEvent(Double_t xval, Pi0ClusterPair * ev) {
   if(!ev) return(-1);
   Int_t bin = fx->FindFixBin(xval);

      double energy  = TMath::Sqrt((ev->fMomentum)*(ev->fMomentum) + 135.0*135.0);
      double pt      = (ev->fMomentum)*TMath::Sin(ev->fTheta);

   fN->SetBinContent(bin,  fN->GetBinContent(bin) + 1.0 ); 
   fPt->SetBinContent(bin, fPt->GetBinContent(bin) + pt ); 
   fE->SetBinContent(bin,  fE->GetBinContent(bin) + energy ); 
   fTheta->SetBinContent(bin,  fTheta->GetBinContent(bin) + ev->fTheta ); 
   fPhi->SetBinContent(bin,  fPhi->GetBinContent(bin) + ev->fPhi ); 

   // value squared
   fPt_2->SetBinContent(bin, fPt_2->GetBinContent(bin) + pt*pt ); 
   fE_2->SetBinContent(bin,  fE_2->GetBinContent(bin) +  energy*energy ); 
   fTheta_2->SetBinContent(bin,  fTheta_2->GetBinContent(bin) + ev->fTheta*ev->fTheta ); 
   fPhi_2->SetBinContent(bin,  fPhi_2->GetBinContent(bin) + ev->fPhi*ev->fPhi ); 


   return 0;
}
//________________________________________________________________________________
Int_t InSANEAveragedPi0Kinematics1D::CalculateErrors(TH1 *h0, TH1* h1){
   if(!h0) return(-1);
   if(!h1) return(-2);
   Double_t x,x2,var;
   for(int i = 0; i<h0->GetNbinsX(); i++) {
      x   = h0->GetBinContent(i);
      x2  = h1->GetBinContent(i);
      var = TMath::Abs(x2-x*x);
      h0->SetBinError(i,TMath::Sqrt(var));
      if(var != 0.0) h1->SetBinContent(i,1.0/var);
      else h1->SetBinContent(i,0.0);
   }
   return(0);
}
//________________________________________________________________________________
Int_t InSANEAveragedPi0Kinematics1D::Calculate(){
   if( fCalculated ) {
      std::cout << "Kinematics already calculated." << std::endl;
      return 1;
   }
   fx_2->Divide(fN);
   fW_2->Divide(fN);
   fPt_2->Divide(fN);
   fNu_2->Divide(fN);
   fE_2->Divide(fN);
   fTheta_2->Divide(fN);
   fPhi_2->Divide(fN);
   fR_2->Divide(fN);
   fF1_2->Divide(fN);
   fD_2->Divide(fN);
   fChi_2->Divide(fN);
   fEta_2->Divide(fN);
   fXi_2->Divide(fN);
   fx->Divide(fN);
   fW->Divide(fN);
   fPt->Divide(fN);
   fNu->Divide(fN);
   fE->Divide(fN);
   fTheta->Divide(fN);
   fPhi->Divide(fN);
   fR->Divide(fN);
   fF1->Divide(fN);
   fD->Divide(fN);
   fChi->Divide(fN);
   fEta->Divide(fN);
   fXi->Divide(fN);
   CalculateErrors(fx,fx_2);
   CalculateErrors(fW,fW_2);
   CalculateErrors(fPt,fPt_2);
   CalculateErrors(fNu,fNu_2);
   CalculateErrors(fE,fE_2);
   CalculateErrors(fTheta,fTheta_2);
   CalculateErrors(fPhi,fPhi_2);
   CalculateErrors(fR,fR_2);
   CalculateErrors(fF1,fF1_2);
   CalculateErrors(fD,fD_2);
   CalculateErrors(fChi,fChi_2);
   CalculateErrors(fEta,fEta_2);
   CalculateErrors(fXi,fXi_2);
   fCalculated = true;
   return(0);
}
//________________________________________________________________________________
