#include "BETADetectorPackage.h"
#include "InSANERunManager.h"
#include "SANEAnalysisManager.h"


ClassImp(BETADetectorPackage)

//______________________________________________________________________________
BETADetectorPackage::BETADetectorPackage(const char * name, Int_t runNumber) : InSANEDetectorPackage(name)
{

   if (SANERunManager::GetRunManager()->fVerbosity > 1)
      printf(" o BETADetectorPackage::Constructor \n");

   fTrackerDetector  = (ForwardTrackerDetector*) SANEAnalysisManager::GetAnalysisManager()->GetDetector("ForwardTracker");
   if (!fTrackerDetector) {
      fTrackerDetector   = new ForwardTrackerDetector(runNumber);
      fTrackerDetector->Build();
   }

   fCherenkovDetector  = (GasCherenkovDetector*) SANEAnalysisManager::GetAnalysisManager()->GetDetector("GasCherenkov");
   if (!fCherenkovDetector) {
      fCherenkovDetector = new GasCherenkovDetector(runNumber);
      fCherenkovDetector->Build();
   }

   fHodoscopeDetector  = (LuciteHodoscopeDetector*) SANEAnalysisManager::GetAnalysisManager()->GetDetector("LuciteHodoscope");
   if (!fHodoscopeDetector) {
      fHodoscopeDetector = new LuciteHodoscopeDetector(runNumber);
      fHodoscopeDetector->Build();
   }

   fBigcalDetector  = (BigcalDetector*) SANEAnalysisManager::GetAnalysisManager()->GetDetector("BigCal");
   if (!fBigcalDetector) {
      fBigcalDetector    = new BigcalDetector(runNumber);
      fBigcalDetector->Initialize();
   }

   fEvent = nullptr;

   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd("detectors");
   }

   fEventLuciteTimeSum  = new TH2F("fEventLuciteTimeSum", "Hodoscope TDC Sums", 100, -6000, 0, 28, 1, 29);
   fRunLuciteTimeSum  = new TH2F("fRunLuciteTimeSum", "Hodoscope TDC Sums", 100, -6000, 0, 28, 1, 29);
   fEventLuciteTimeDifference  = new TH2F("fEventLuciteTimeDifference", "Hodoscope TDC Differences", 100, -6000, -2000, 28, 1, 29);
   fRunLuciteTimeDifference  = new TH2F("fRunLuciteTimeDifference", "Hodoscope TDC Differences", 100, -6000, -2000, 28, 1, 29);

   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
   }
   if (SANERunManager::GetRunManager()->fVerbosity > 1)
      printf(" o End of BETADetectorPackage::Constructor \n");
}
//______________________________________________________________________________
BETADetectorPackage::~BETADetectorPackage() {

   if (InSANERunManager::GetRunManager()->GetCurrentFile()) {
      InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
      /*
            std::cout << " Saving the detectors to file. \n";
            if(fTrackerDetector) fTrackerDetector->Write("ForwardTracker",kWriteDelete);
            if(fCherenkovDetector) fCherenkovDetector->Write("GasCherenkov",kWriteDelete);
            if(fHodoscopeDetector) fHodoscopeDetector->Write("LuciteHodoscope",kWriteDelete);
            if(fBigcalDetector) fBigcalDetector->Write("BigCal",kWriteDelete);
      */
   } else {
      std::cout << " could not save detectors. \n";
   }
}
//______________________________________________________________________________
Int_t BETADetectorPackage::ClearEvent(Option_t * opt) {
   /*    if(fHits) fHits->Clear(opt);*/
   fEventLuciteTimeSum->Reset();
   fEventLuciteTimeDifference->Reset();
   return 0;
}
//______________________________________________________________________________
Int_t BETADetectorPackage::ProcessEvent() {
   if (fEvent) {
      if (fHodoscopeDetector) {
         for (unsigned int kk = 0; kk < 28; kk++) { // loop over lucite bars
            ///  sums for all tdc hits in lucite bars
            for (unsigned int i2 = 0; i2 < fHodoscopeDetector->fTimedTDCHitSums[kk]->size() ; i2++) {
//                fEventLuciteTimeDifference->Fill(fHodoscopeDetector->fTDCHitDifferences[kk]->back(),kk+1);
//                fRunLuciteTimeDifference->Fill(fHodoscopeDetector->fTDCHitDifferences[kk]->back(),kk+1);
               fEventLuciteTimeSum->Fill(fReferenceTDC + fHodoscopeDetector->fTimedTDCHitSums[kk]->at(i2), kk + 1);
               fRunLuciteTimeSum->Fill(fReferenceTDC + fHodoscopeDetector->fTimedTDCHitSums[kk]->at(i2), kk + 1);
               std::cout << "ref: " << fReferenceTDC <<  " sum: " << fHodoscopeDetector->fTimedTDCHitSums[kk]->at(i2) << "\n";
            }
         }
      }//fHodoscopeDetector
   }//fEvent
   return(0);
}
//______________________________________________________________________________

