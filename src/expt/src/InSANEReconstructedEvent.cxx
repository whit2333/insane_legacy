#include "InSANEReconstructedEvent.h"

ClassImp(InSANEReconstructedEvent)

//TClonesArray * InSANEReconstructedEvent::fgTargetPositions   = 0;
//TClonesArray * InSANEReconstructedEvent::fgTrackerPositions  = 0;
//TClonesArray * InSANEReconstructedEvent::fgTracker2Positions = 0;
//TClonesArray * InSANEReconstructedEvent::fgTrackerY2Positions = 0;
//TClonesArray * InSANEReconstructedEvent::fgTrackerY1Positions = 0;
//TClonesArray * InSANEReconstructedEvent::fgLucitePositions   = 0;
//TClonesArray * InSANEReconstructedEvent::fgTrajectories      = 0;

//_____________________________________________________________________________
InSANEReconstructedEvent::InSANEReconstructedEvent() {

   fTrajectories       = nullptr;
   fTargetPositions    = nullptr;
   fTracker2Positions  = nullptr;
   fTrackerY2Positions = nullptr;
   fTrackerY1Positions = nullptr;
   fTrackerPositions   = nullptr;
   fLucitePositions    = nullptr;

   fRunNumber           = 0;
   fEventNumber         = 0;
   fNGoodPositions      = 0;

   fClosestLuciteMissDistance  = 0;
   fClosestLuciteHitNumber     = 0;
   fClosestTrackerMissDistance = 0;
   fClosestTrackerHitNumber    = 0;

   fNTrajectories       = 0;
   fNLucitePositions    = 0;
   fNTargetPositions    = 0;
   fNTrackerPositions   = 0;
   fNTracker2Positions  = 0;
   fNTrackerY2Positions = 0;
   fNTrackerY1Positions = 0;

   if (!fTrajectories)       fTrajectories       = new TClonesArray("InSANETrajectory", 1);
   if (!fTargetPositions)    fTargetPositions    = new TClonesArray("InSANEHitPosition", 1);
   if (!fTracker2Positions)  fTracker2Positions  = new TClonesArray("InSANEHitPosition", 1);
   if (!fTrackerY2Positions) fTrackerY2Positions = new TClonesArray("InSANEHitPosition", 1);
   if (!fTrackerY1Positions) fTrackerY1Positions = new TClonesArray("InSANEHitPosition", 1);
   if (!fTrackerPositions)   fTrackerPositions   = new TClonesArray("InSANEHitPosition", 1);
   if (!fLucitePositions)    fLucitePositions    = new TClonesArray("InSANEHitPosition", 1);


}
//_____________________________________________________________________________
InSANEReconstructedEvent::~InSANEReconstructedEvent(){

}
//_____________________________________________________________________________
void InSANEReconstructedEvent::ClearEvent(Option_t * opt){
   fNTrajectories              = 0;
   fNTargetPositions           = 0;
   fNTrackerPositions          = 0;
   fNTracker2Positions         = 0;
   fNTrackerY2Positions        = 0;
   fNTrackerY1Positions        = 0;
   fNLucitePositions           = 0;
   fNGoodPositions             = 0;
   fEventNumber                = 0;
   fClosestLuciteMissDistance  = 999;
   fClosestTrackerMissDistance = 999;

   if(fTrajectories->GetEntries() > 20 ) {
      std::cout << " calling  fTrajectories->ExpandCreate(3); " << fTrajectories->GetEntries() << " entries " << std::endl;
      fTrajectories->ExpandCreate(3);
   }
   fTrajectories->Clear("C");
   fTargetPositions->Clear("C");

   if(fTrackerPositions->GetEntries() > 600 ) {
      std::cout << " calling  fTrackerPositions->ExpandCreate(50); \n";
      fTrackerPositions->ExpandCreate(200);
   }
   fTrackerPositions->Clear("C");

   if(fTrackerY1Positions->GetEntries() > 500 ) {
      std::cout << " calling  fTrackerY1Positions->ExpandCreate(51); \n";
      fTrackerY1Positions->ExpandCreate(200);
   }
   fTrackerY1Positions->Clear("C");

   if(fTrackerY2Positions->GetEntries() > 500 ) {
      std::cout << " calling  fTrackerY2Positions->ExpandCreate(52); \n";
      fTrackerY2Positions->ExpandCreate(200);
   }
   fTrackerY2Positions->Clear("C");

   if(fTracker2Positions->GetEntries() > 500 ) {
      std::cout << " calling  fTracker2Positions->ExpandCreate(53); \n";
      fTracker2Positions->ExpandCreate(200);
   }
   fTracker2Positions->Clear("C");

   if(fLucitePositions->GetEntries() > 50 ) {
      std::cout << " calling  fLucitePositions->ExpandCreate(10); \n";
      fLucitePositions->ExpandCreate(5);
   }
   fLucitePositions->Clear("C");
   //       fBigcalClusters->Clear("C");
   //       fLuciteHitPos->Clear("C");
   //fGoodPositions.Clear();
}


//_____________________________________________________________________________


//_____________________________________________________________________________







