#include "InSANEDetectorComponent.h"

ClassImp(InSANEDetectorComponent)

//______________________________________________________________________________
InSANEDetectorComponent::InSANEDetectorComponent(const char * n, const char * t, Int_t num)
   : TNamed(n,t), fComponentNumber(num), fHasADCValue(false), fHasTDCValue(false), fHasScalerValue(false)
{
   fTiming.Clear();
   fPedestal.Clear();
   fHits.SetOwner(kFALSE);
   fIsHit                = false;
   fTDCRangeMin          = 0;
   fTDCRangeMax          = 0;
   fNominalTDCCutWidth   = 0;
   fTypicalTDCPeak       = 0;
   fTypicalTDCPeakWidth  = 0;
   fADCRangeMin          = 0;
   fADCRangeMax          = 0;
   fTypicalPedestal      = 0;
   fTypicalPedestalWidth = 0;
}
//______________________________________________________________________________
InSANEDetectorComponent::~InSANEDetectorComponent() {
}
//______________________________________________________________________________
void InSANEDetectorComponent::ClearEvent(Option_t * ) {
   fIsHit = false;
   fHits.Clear();
}
//______________________________________________________________________________

//______________________________________________________________________________

//______________________________________________________________________________

//______________________________________________________________________________

//______________________________________________________________________________





