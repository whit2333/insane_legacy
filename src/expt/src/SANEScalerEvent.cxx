#include  "SANEScalerEvent.h"

ClassImp(SANEScalerEvent)

//______________________________________________________________________________
SANEScalerEvent::SANEScalerEvent(/* const char * treename = "scalers"*/)
{

   fBCMScaleCoefficent[0] = 0.0137053;
   fBCMScaleCoefficent[1] = 0.0131785;
   fBCMOffset[0] = 250769;
   fBCMOffset[1] = 250632;

   // these helicity scaler coefficients are now the same as above.
   // latest values reflect Mark's fit constrained at zero 
   // (https://userweb.jlab.org/~jones/sane/lt-chasym-data/notes_charge_lifetime.txt)

   fBCMPositiveHelicityScaleCoefficent[0] = 0.0000188;//0.0137053; //0.0137112;
   fBCMPositiveHelicityScaleCoefficent[1] = 0.0000188;//0.0131785; //0.0131851;
   fBCMNegativeHelicityScaleCoefficent[0] = 0.0000188;//0.0137053; //0.0136978;
   fBCMNegativeHelicityScaleCoefficent[1] = 0.0000188;//0.0131785; //0.0131694;

   fBCMPositiveHelicityOffset[0] = 252621;//250769; //250764 ;
   fBCMPositiveHelicityOffset[1] = 252621;//250632; //250628;
   fBCMNegativeHelicityOffset[0] = 252621;//250769; //250763;
   fBCMNegativeHelicityOffset[1] = 252621;//250632; //250623;

   ClearCounts();
   ClearEvent();
}
//______________________________________________________________________________
void SANEScalerEvent::SetAddresses()
{
   /*
     fBigcalScalers = &(fScalers[0]);
     &f1MHzClockScaler  = &(fScalers[272]);
     &fUnserScaler   = &(fScalers[273]);
     &fBCM1Scaler   = &(fScalers[274]);
     &fBCM2Scaler   = &(fScalers[275]);
     &fHMS1Scaler   = &(fScalers[276]);
     &fBETA1Scaler   = &(fScalers[277]);
     &fPi0Scaler   = &(fScalers[278]);
     &fBETA2Scaler   = &(fScalers[279]);
     &fCoinScaler   = &(fScalers[280]);
     &fTSOutScaler   = &(fScalers[281]);
     &fBCM1NegativeHelicityScaler   = &(fScalers[282]);
     &fBCM1PositiveHelicityScaler   = &(fScalers[283]);
     &f1MHzClockNegativeHelicityScaler   = &(fScalers[284]);
     &f1MHzClockPositiveHelicityScaler   = &(fScalers[285]);
     &fBCM2NegativeHelicityScaler   = &(fScalers[286]);
     &fBCM2PositiveHelicityScaler   = &(fScalers[287]);
     fCherenkovScalers = &(fScalers[288]);

     &fHMS1NegativeHelicityScaler   = &(fScalers[304]);
     &fBETA1NegativeHelicityScaler   = &(fScalers[305]);
     &fPi0NegativeHelicityScaler   = &(fScalers[306]);
     &fBETA2NegativeHelicityScaler   = &(fScalers[307]);
     &fCoinNegativeHelicityScaler   = &(fScalers[308]);
     &fTSOutNegativeHelicityScaler   = &(fScalers[309]);
     &fHMS2NegativeHelicityScaler   = &(fScalers[310]);

     &fHMS2Scaler = &(fScalers[319]);
     fTrackerScalers = &(fScalers[320]);

   */
}
//______________________________________________________________________________

SANEScalerEvent::~SANEScalerEvent()
{ }
//______________________________________________________________________________

void SANEScalerEvent::ClearEvent(Option_t *)
{
   InSANEScalerEvent::ClearEvent();

   fBETA1Counter.Clear();
   fBETA2Counter.Clear();
   fLEDCounter.Clear();
   fPi0Counter.Clear();
   fCoinCounter.Clear();
   fHMS1Counter.Clear();
   fHMS2Counter.Clear();
   fTSCounter.Clear();
   fBCM1Scaler = 0;
   fBCM2Scaler = 0;
}
//______________________________________________________________________________
void SANEScalerEvent::ClearCounts()
{

   InSANEScalerEvent::ClearCounts();

   fBETA1Counter.ClearCounts();
   fBETA2Counter.ClearCounts();
   fLEDCounter.ClearCounts();
   fPi0Counter.ClearCounts();
   fCoinCounter.ClearCounts();
   fHMS1Counter.ClearCounts();
   fHMS2Counter.ClearCounts();

   for (int i = 0; i < 12; i++) fCherenkovCounts[i] = 0;
}
//______________________________________________________________________________
/** Calculates the average beam current*/
Double_t SANEScalerEvent::BeamCurrentAverage() const 
{
   return((BeamCurrentMonitor1() + BeamCurrentMonitor2()) / 2.0);
}

Double_t SANEScalerEvent::GetBeamCurrent() const { return BeamCurrentAverage(); }

/** In nanoamps following the formula from the wiki */
Double_t SANEScalerEvent::BeamCurrentMonitor1() const {
   return(fBCMScaleCoefficent[0] * (fBCM1Scaler / ((double)f1MHzClockScaler2 * (1.0 / 1000000.0)) - (double)fBCMOffset[0]));
}

/** Used in simulationg the BCM scalers */
Double_t SANEScalerEvent::GetScalerBCM1(Double_t current) const {
   return( (current/fBCMScaleCoefficent[0] + (double)fBCMOffset[0]) * ( ((double)f1MHzClockScaler2*(1.0/1000000.0))) );
}

/** Used in simulationg the BCM scalers */
Double_t SANEScalerEvent::GetScalerBCM2(Double_t current) const {
   return( (current/fBCMScaleCoefficent[1] + (double)fBCMOffset[1]) * ( ((double)f1MHzClockScaler2*(1.0/1000000.0))) );
}

/** In nanoamps following the formula from the wiki */
Double_t SANEScalerEvent::BeamCurrentMonitor2() const {
   return(fBCMScaleCoefficent[1] * (fBCM2Scaler / ((double)f1MHzClockScaler2 * (1.0 / 1000000.0)) - (double)fBCMOffset[1]));
}

/** In nanoamps following the formula from the wiki */
Double_t SANEScalerEvent::BeamCurrentMonitor1PositiveHelicity() const {
   return(fBCMPositiveHelicityScaleCoefficent[0] *
         (fBCM1PositiveHelicityScaler/((double)f1MHzClockPositiveHelicityScaler * (1.0 / 1000000.0)) - (double)fBCMPositiveHelicityOffset[0]));
}

/** In nanoamps following the formula from the wiki */
Double_t SANEScalerEvent::BeamCurrentMonitor2PositiveHelicity() const {
   return(fBCMPositiveHelicityScaleCoefficent[1] * (fBCM2PositiveHelicityScaler / ((double)f1MHzClockPositiveHelicityScaler * (1.0 / 1000000.0)) - (double)fBCMPositiveHelicityOffset[1]));
}

Double_t SANEScalerEvent::GetPositiveScalerBCM1(Double_t current) const {
   return( (current/fBCMPositiveHelicityScaleCoefficent[0] + (double)fBCMPositiveHelicityOffset[0]) * ( ((double)f1MHzClockPositiveHelicityScaler*(1.0/1000000.0))) );
}
Double_t SANEScalerEvent::GetPositiveScalerBCM2(Double_t current) const {
   return( (current/fBCMPositiveHelicityScaleCoefficent[1] + (double)fBCMPositiveHelicityOffset[1]) * ( ((double)f1MHzClockPositiveHelicityScaler*(1.0/1000000.0))) );
}

/** In nanoamps following the formula from the wiki */
Double_t SANEScalerEvent::BeamCurrentMonitor1NegativeHelicity() const {
   return(fBCMNegativeHelicityScaleCoefficent[0] * (fBCM1NegativeHelicityScaler/((double)f1MHzClockNegativeHelicityScaler * (1.0 / 1000000.0)) - (double)fBCMNegativeHelicityOffset[0]));
}

/** In nanoamps following the formula from the wiki */
Double_t SANEScalerEvent::BeamCurrentMonitor2NegativeHelicity() const {
   return(fBCMNegativeHelicityScaleCoefficent[1] * (fBCM2NegativeHelicityScaler/((double)f1MHzClockNegativeHelicityScaler * (1.0 / 1000000.0)) - (double)fBCMNegativeHelicityOffset[1]));
}

Double_t SANEScalerEvent::GetNegativeScalerBCM1(Double_t current) const {
   return( (current/fBCMNegativeHelicityScaleCoefficent[0] + (double)fBCMNegativeHelicityOffset[0]) * ( ((double)f1MHzClockNegativeHelicityScaler*(1.0/1000000.0))) );
}
Double_t SANEScalerEvent::GetNegativeScalerBCM2(Double_t current) const {
   return( (current/fBCMNegativeHelicityScaleCoefficent[1] + (double)fBCMNegativeHelicityOffset[1]) * ( ((double)f1MHzClockNegativeHelicityScaler*(1.0/1000000.0))) );
}

//@}

/** returns the time lapsed since the last scaler read. (seconds) */
Double_t SANEScalerEvent::TimeLapsed() const { // *MENU*
   return((double)f1MHzClockScaler * (1.0 / 998000.0));
}

/** HMS scaler to get time. */
Double_t SANEScalerEvent::TimeLapsed2() const { // *MENU*
   return((double)f1MHzClockScaler2 * (1.0 / 1000000.0));
}

/** returns the charge deposited  since the last scaler read. (microC) */
Double_t SANEScalerEvent::AverageDeltaQ() const {
   return(TimeLapsed() * BeamCurrentAverage() / 1000.0);
}

Double_t SANEScalerEvent::BCM1ChargeAsymmetry() const {
   return(
         (BeamCurrentMonitor1PositiveHelicity() - BeamCurrentMonitor1NegativeHelicity()) /
         (BeamCurrentMonitor1PositiveHelicity() + BeamCurrentMonitor1NegativeHelicity())
         );
}

Double_t SANEScalerEvent::BCM2ChargeAsymmetry() const {
   return(
         (BeamCurrentMonitor2PositiveHelicity() - BeamCurrentMonitor2NegativeHelicity()) /
         (BeamCurrentMonitor2PositiveHelicity() + BeamCurrentMonitor2NegativeHelicity())
         );
}

/** Fraction of time interval spent with positive helicity. */
Double_t SANEScalerEvent::PositiveTimeFraction() const {
   return (double(f1MHzClockPositiveHelicityScaler)/double(f1MHzClockScaler));
}
/** Fraction of time interval spent with negative helicity. */
Double_t SANEScalerEvent::NegativeTimeFraction() const {
   return (double(f1MHzClockNegativeHelicityScaler)/double(f1MHzClockScaler) );
}

/** Fraction of time interval spent with positive helicity. */
Double_t SANEScalerEvent::PositiveTimeFraction2() const {
   return (double(f1MHzClockPositiveHelicityScaler)/double(f1MHzClockScaler2));
}
/** Fraction of time interval spent with negative helicity. */
Double_t SANEScalerEvent::NegativeTimeFraction2() const {
   return (double(f1MHzClockNegativeHelicityScaler)/double(f1MHzClockScaler2) );
}

/** calculates the average beam current for positive helicity*/
Double_t SANEScalerEvent::BeamCurrentAveragePositiveHelicity() const {
   return((BeamCurrentMonitor1PositiveHelicity() + BeamCurrentMonitor2PositiveHelicity()) / 2.0);
}

/** calculates the average beam current for positive helicity*/
Double_t SANEScalerEvent::BeamCurrentAverageNegativeHelicity() const {
   return((BeamCurrentMonitor1NegativeHelicity() + BeamCurrentMonitor2NegativeHelicity()) / 2.0);
}

/** returns the charge deposited  since the last scaler read for positive helicity. (microC) */
Double_t SANEScalerEvent::AveragePositiveDeltaQ() const {
   return(TimeLapsed()*PositiveTimeFraction2() * BeamCurrentAveragePositiveHelicity() / 1000.0);
}

/** returns the charge deposited  since the last scaler read for positive helicity. (microC) */
Double_t SANEScalerEvent::AverageNegativeDeltaQ() const {
   return(TimeLapsed()*NegativeTimeFraction2() * BeamCurrentAverageNegativeHelicity() / 1000.0);
}


Double_t SANEScalerEvent::BCM1PositiveDeltaQ() const {
   // BCM1 delta q+
   return(TimeLapsed()*PositiveTimeFraction2() * BeamCurrentMonitor1PositiveHelicity() / 1000.0);
}
Double_t SANEScalerEvent::BCM1NegativeDeltaQ() const {
   // BCM1 delta q-
   return(TimeLapsed()*NegativeTimeFraction2() * BeamCurrentMonitor1NegativeHelicity() / 1000.0);
}
Double_t SANEScalerEvent::BCM2PositiveDeltaQ() const {
   // BCM2 delta q+
   return(TimeLapsed()*PositiveTimeFraction2() * BeamCurrentMonitor2PositiveHelicity() / 1000.0);
}
Double_t SANEScalerEvent::BCM2NegativeDeltaQ() const {
   // BCM2 delta q-
   return(TimeLapsed()*NegativeTimeFraction2() * BeamCurrentMonitor2NegativeHelicity() / 1000.0);
}

/** Live Time for Positive Helicity gates
 */
Double_t SANEScalerEvent::BETA2LiveTimePositive() const {
   return(fBETA2Counter.fPositiveHelicityTriggerCount / (fBETA2Counter.fPositiveHelicityScaler + 0.00000000000001));
}

/** Live Time for Negative Helicity gates
 */
Double_t SANEScalerEvent::BETA2LiveTimeNegative() const {
   return(fBETA2Counter.fNegativeHelicityTriggerCount / (fBETA2Counter.fNegativeHelicityScaler + 0.00000000000001));
}

Double_t SANEScalerEvent::BETA2DeadTimePositive() const {
   return(1.0 - BETA2LiveTimePositive());
}

Double_t SANEScalerEvent::BETA2DeadTimeNegative() const {
   return(1.0 - BETA2LiveTimePositive());
}

Double_t SANEScalerEvent::GetCherenkovRate(Int_t n) {
   if (n >= 0 && n < 12) return(((double)fCherenkovScalers[n]) / ((double)f1MHzClockScaler * (1.0 / 1000000.0)));
   else return(0.0);
}

Double_t SANEScalerEvent::GetBigcalRate(Int_t n) {
   if (n >= 0 && n < 224) return(((double)fBigcalScalers[n]) / ((double)f1MHzClockScaler * (1.0 / 1000000.0)));
   else return(0.0);
}

Double_t SANEScalerEvent::GetTrackerRate(Int_t n) {
   if (n >= 0 && n < 32) return(((double)fTrackerScalers[n]) / ((double)f1MHzClockScaler * (1.0 / 1000000.0)));
   else return(0.0);
}
