#include "InSANEMagneticField.h"

//#include "InSANEDatabaseManager.h"

ClassImp(InSANEMagneticField)

//_____________________________________________________
InSANEMagneticField::InSANEMagneticField(const char * n) : TVirtualMagField(n), fScalingCoefficient(1.0), fMaximumField(1.0)
{
   fIsConst           = false;
   fIsInitialized     = false;
   fMaxFieldMag       = 7.0;
   fPolarizationAngle = TMath::Pi();
   fBAbsGraph         = nullptr;
   fBrGraph           = nullptr;
   fBxGraph           = nullptr;
   fByGraph           = nullptr;
   fBzGraph           = nullptr;
   fBrayGraph         = nullptr;
   fCanvas            = nullptr;
   fTestPoint.SetXYZ(0.0, 0.0, 0.0);
}

//_____________________________________________________
InSANEMagneticField::~InSANEMagneticField()
{
   ;
}
//_____________________________________________________
TVector3 InSANEMagneticField::GetField(const TVector3& v) { 
   return(GetField(v.X(),v.Y(),v.Z()));
} 
//_____________________________________________________
TVector3 InSANEMagneticField::GetField(Double_t x,Double_t y, Double_t z) {
   //if(!IsInitialized()) Initialize();
   Double_t point[4] = {x,y,z,0};
   Double_t B[3] = {0,0,0};
   GetFieldValue(point,B);
   TVector3 field(B[0],B[1],B[2]); 
   return field;
}
//_____________________________________________________
void InSANEMagneticField::PrintField(Double_t x, Double_t y, Double_t z) {
   TVector3 b = GetField(x, y, z);
   printf("v(%f, %f, %f) B(%f, %f, %f) \n", x, y, z, b.X(), b.Y(), b.Z());
}
//_____________________________________________________
void InSANEMagneticField::GetFieldWithVector(const  TVector3& point, TVector3& bfield) const {
   Double_t Point[4] = { point.X(), point.Y(), point.Z() , 0.0};
   Double_t BFIELD[3] = {0, 0, 0};
   GetFieldValue(Point, BFIELD);
   bfield.SetXYZ(BFIELD[0], BFIELD[1], BFIELD[2]);
}
//_____________________________________________________
void InSANEMagneticField::GetFieldValue(const Double_t Point[4], Double_t *Bfield) const {
   // Just a uniform magntic field
   Bfield[2] = fScalingCoefficient * 1.0;
   Bfield[1] = 0.0;
   Bfield[0] = 0.0;

}

//_____________________________________________________
Double_t InSANEMagneticField::BilinearInterpolation(Double_t * cornerDataPoints, Double_t * corners, Double_t x, Double_t y) const
{
/// Z -> i, R -> j
   Double_t Q11 = cornerDataPoints[0];
   Double_t Q21 = cornerDataPoints[1];
   Double_t Q12 = cornerDataPoints[2];
   Double_t Q22 = cornerDataPoints[3];
   Double_t x1 = corners[0];
   Double_t x2 = corners[1];
   Double_t y1 = corners[2];
   Double_t y2 = corners[3];
   Double_t dxdy = (x2 - x1) * (y2 - y1);
   Double_t result  = Q11 * (x2 - x) * (y2 - y) / dxdy +
                      Q21 * (x - x1) * (y2 - y) / dxdy +
                      Q12 * (x2 - x) * (y - y1) / dxdy +
                      Q22 * (x - x1) * (y - y1) / dxdy  ;
   return(result);
}

//_____________________________________________________
Double_t InSANEMagneticField::IntegrateBdl(Double_t theta, Double_t phi)
{
   TVector3 position(0, 0, 0);
   TVector3 aMagneticField(0, 0, 0);

   Double_t bfield[3];
   Double_t point[4];
   TVector3 direction(0, 0, 1);
   direction.SetMagThetaPhi(1.0, theta * TMath::Pi() / 180.0, phi * TMath::Pi() / 180.0);
   Double_t total = 0.0;

   Double_t drStep = 0.5; // 1 cm
   for (int i = 0; i < 300; i++) {
      position.SetMagThetaPhi(drStep * i, theta * TMath::Pi() / 180.0, phi * TMath::Pi() / 180.0);
      point[0] = position.X();
      point[1] = position.Y();
      point[2] = position.Z();
      this->GetFieldValue(point, bfield);
      aMagneticField.SetXYZ(bfield[0], bfield[1], bfield[2]);
      total += aMagneticField.Dot(direction) * drStep;
   }
   printf("Integral Bdl=%f Tesla*cm \n", total);
   return(total);
}
//_____________________________________________________

void InSANEMagneticField::SwitchPolarization()
{
   if (fPolarizationAngle == TMath::Pi()) {
      std::cout << "Target switching to TRANSVERSE field orientation." << std::endl;
      fPolarizationAngle = (-80.*TMath::Pi() / 180.);
   } else {
      std::cout << "Target switching to PARALLEL field orientation." << std::endl;
      fPolarizationAngle = TMath::Pi();
   }
}
//_____________________________________________________

void InSANEMagneticField::TestInterpolation()
{
   Double_t y[10];
   Double_t x[10];
   //TGraph * test = new TGraph();
   //TCanvas *c = new TCanvas("c", "sine test",200,10,400,400);
   for (int t = 0; t < 10; t++) {
      y[t] = TMath::Sin(t);
      x[t] = t;
   }
   auto * graph = new TGraph(10, x, y);
   graph->Draw("ap");
}
//_____________________________________________________

void InSANEMagneticField::LookAtFieldZComponent(const Int_t n , Double_t step/*cm*/)
{

   GetCanvas();

   Double_t /*Br[n*n],*/ Bz[n*n], y[n*n], z[n*n];
   Double_t point[4] = {0, 0, 0, 0};
   Double_t Bfield[3] = {0, 0, 0};

   Int_t N = 0;
   for (Int_t i = 0; i < n; i++) {
      for (Int_t j = 0; j < n; j++) {
         point[0] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[1] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[2] = ((Double_t)j - (Double_t)n / 2.0) * step;
         GetFieldValue(&point[0], &Bfield[0]);
         y[N] = point[0];
         z[N] = point[2];
         Bz[N] = Bfield[2];
         //Br[N] = TMath::Sqrt(Bfield[0] * Bfield[0] + Bfield[1] * Bfield[1]);
         N++;

      }
   }
   if (fBzGraph) delete fBzGraph;
   fBzGraph = new TGraph2D(Form("TargetBz%fdeg", fPolarizationAngle * 180.0 / TMath::Pi()),
                           Form("Target Field, #scale[1.4]{B_{z}}, with #theta_{targ}=%f deg", fPolarizationAngle * 180.0 / TMath::Pi()),
                           n * n, y, z, Bz);
   fBzGraph->GetXaxis()->SetTitle("x [cm]");
   fBzGraph->GetYaxis()->SetTitle("z [cm]");
   fCanvas->cd(2)->cd(3);
   fBzGraph->Draw("colz");
   TArrow ar1(0.0, 0.0, 15.0 * TMath::Sin(40.0 * TMath::Pi() / 180.0), 15.0 * TMath::Cos(40.0 * TMath::Pi() / 180.0), 0.02, "|>");
   TArrow ar2(-15.0 * TMath::Sin(0.0 * TMath::Pi() / 180.0), -15.0 * TMath::Cos(0.0 * TMath::Pi() / 180.0), 0.0, 0.0, 0.02, "|>");
   ar1.SetLineWidth(2);
   ar2.SetLineWidth(2);
   ar1.DrawClone();
   ar2.DrawClone();
   fCanvas->Update();
   PrintTestPoint();
}
//_____________________________________________________

void InSANEMagneticField::LookAtFieldRadialComponent(const Int_t n, Double_t step /*cm*/)
{
   Double_t Br[n*n], /*Bz[n*n],*/ y[n*n], z[n*n];
   Double_t point[4] = {0.0, 0.0, 0.0, 0.0};
   Double_t Bfield[3] = {0.0, 0.0, 0.0};
   GetCanvas();
   Int_t N = 0;
   for (Int_t i = 0; i < n; i++) {
      for (Int_t j = 0; j < n; j++) {
         point[0] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[1] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[2] = ((Double_t)j - (Double_t)n / 2.0) * step;
         GetFieldValue(&point[0], &Bfield[0]);
         y[N] = point[0];
         z[N] = point[2];
         //Bz[N] = Bfield[2];
         Br[N] = TMath::Sqrt(Bfield[0] * Bfield[0] + Bfield[1] * Bfield[1]);
         N++;
      }
   }
   if (fBrGraph) delete fBrGraph;
   fBrGraph = new TGraph2D(Form("TargetBr%fdeg", fPolarizationAngle * 180.0 / TMath::Pi()),
                           Form("Target Field, #scale[1.4]{B_{#rho}}, with #theta_{targ}=%f deg", fPolarizationAngle * 180.0 / TMath::Pi()),
                           n * n, y, z, Br);

   fBrGraph->GetXaxis()->SetTitle("x [cm]");
   fBrGraph->GetYaxis()->SetTitle("z [cm]");
   fCanvas->cd(1)->cd(1);
   fBrGraph->Draw("colz");
   TArrow ar1(0.0, 0.0, 15.0 * TMath::Sin(40.0 * TMath::Pi() / 180.0), 15.0 * TMath::Cos(40.0 * TMath::Pi() / 180.0), 0.02, "|>");
   TArrow ar2(-15.0 * TMath::Sin(0.0 * TMath::Pi() / 180.0), -15.0 * TMath::Cos(0.0 * TMath::Pi() / 180.0), 0.0, 0.0, 0.02, "|>");
   ar1.SetLineWidth(2);
   ar2.SetLineWidth(2);
   ar1.DrawClone();
   ar2.DrawClone();
   fCanvas->Update();
   PrintTestPoint();

}
//_____________________________________________________

void InSANEMagneticField::LookAtFieldXComponent(const Int_t n, Double_t step /*cm*/)
{
   Double_t Br[n*n], /*Bz[n*n],*/ y[n*n], z[n*n];
   Double_t point[4] = {0.0, 0.0, 0.0, 0.0};
   Double_t Bfield[3] = {0.0, 0.0, 0.0};
   GetCanvas();

   Int_t N = 0;
   for (Int_t i = 0; i < n; i++) {
      for (Int_t j = 0; j < n; j++) {
         point[0] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[1] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[2] = ((Double_t)j - (Double_t)n / 2.0) * step;
         GetFieldValue(&point[0], &Bfield[0]);
         y[N] = point[0];
         z[N] = point[2];
         //Bz[N] = Bfield[2];
         Br[N] = Bfield[0];
         N++;
      }
   }
   if (fBxGraph) delete fBxGraph;
   fBxGraph = new TGraph2D(Form("TargetBx%fdeg", fPolarizationAngle * 180.0 / TMath::Pi()),
                           Form("Target Field, #scale[1.4]{B_{x}}, with #theta_{targ}=%f deg", fPolarizationAngle * 180.0 / TMath::Pi()),
                           n * n, y, z, Br);
   fBxGraph->GetXaxis()->SetTitle("x [cm]");
   fBxGraph->GetYaxis()->SetTitle("z [cm]");
   fCanvas->cd(2)->cd(1);
   fBxGraph->Draw("colz");
   TArrow ar1(0.0, 0.0, 15.0 * TMath::Sin(40.0 * TMath::Pi() / 180.0), 15.0 * TMath::Cos(40.0 * TMath::Pi() / 180.0), 0.02, "|>");
   TArrow ar2(-15.0 * TMath::Sin(0.0 * TMath::Pi() / 180.0), -15.0 * TMath::Cos(0.0 * TMath::Pi() / 180.0), 0.0, 0.0, 0.02, "|>");
   ar1.SetLineWidth(2);
   ar2.SetLineWidth(2);
   ar1.DrawClone();
   ar2.DrawClone();

   fCanvas->Update();
   PrintTestPoint();
}
//_____________________________________________________

void InSANEMagneticField::LookAtFieldYComponent(const Int_t n, Double_t step /*cm*/)
{
   Double_t Br[n*n], /*Bz[n*n],*/ y[n*n], z[n*n];
   Double_t point[4] = {0.0, 0.0, 0.0, 0.0};
   Double_t Bfield[3] = {0.0, 0.0, 0.0};

   GetCanvas();
   Int_t N = 0;
   for (Int_t i = 0; i < n; i++) {
      for (Int_t j = 0; j < n; j++) {
         point[0] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[1] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[2] = ((Double_t)j - (Double_t)n / 2.0) * step;
         GetFieldValue(&point[0], &Bfield[0]);
         y[N] = point[0];
         z[N] = point[2];
         //Bz[N] = Bfield[2];
         Br[N] = Bfield[1];
         N++;
      }
   }
   if (fByGraph) delete fByGraph;
   fByGraph = new TGraph2D(Form("TargetBy%fdeg", fPolarizationAngle * 180.0 / TMath::Pi()),
                           Form("Target Field, #scale[1.4]{B_{y}}, with #theta_{targ}=%f deg", fPolarizationAngle * 180.0 / TMath::Pi()),
                           n * n, y, z, Br);
   fByGraph->GetXaxis()->SetTitle("y [cm]");
   fByGraph->GetYaxis()->SetTitle("z [cm]");
   fCanvas->cd(2)->cd(2);
   fByGraph->Draw("colz");
   TArrow ar1(0.0, 0.0, 15.0 * TMath::Sin(40.0 * TMath::Pi() / 180.0), 15.0 * TMath::Cos(40.0 * TMath::Pi() / 180.0), 0.02, "|>");
   TArrow ar2(-15.0 * TMath::Sin(0.0 * TMath::Pi() / 180.0), -15.0 * TMath::Cos(0.0 * TMath::Pi() / 180.0), 0.0, 0.0, 0.02, "|>");
   ar1.SetLineWidth(2);
   ar2.SetLineWidth(2);
   ar1.DrawClone();
   ar2.DrawClone();
   fCanvas->Update();
   PrintTestPoint();
}
//_____________________________________________________

void InSANEMagneticField::PrintTestPoint()
{
   Double_t point[4];
   Double_t Bfield[3] = {0.0, 0.0, 0.0};
   point[0] = fTestPoint.X();
   point[1] = fTestPoint.Y();
   point[2] = fTestPoint.Z();
   point[3] = 0;

   GetFieldValue(&point[0], &Bfield[0]);

   TVector3 fieldAtTestPoint(Bfield[0], Bfield[1], Bfield[2]);
   std::cout << " The magnetic field at (" << point[0] << ", " << point[1] << ", " << point[2] << ") [cm] " << std::endl;
   std::cout << " is (" << Bfield[0] << ", " << Bfield[1] << ", " << Bfield[2] << ") [T] " << std::endl;
   std::cout << " and the polar and azimuthal angles are "
             << fieldAtTestPoint.Theta() * 180.0 / TMath::Pi() << " [deg] and "
             << fieldAtTestPoint.Phi() * 180.0 / TMath::Pi() << " [deg] respectively.\n";
}
//_____________________________________________________

void InSANEMagneticField::PlotFieldAlongRay(Double_t theta, Double_t phi, const Int_t n , Double_t step)
{
   GetCanvas();
   TVector3 direction;
   direction.SetMagThetaPhi(1.0, theta * TMath::Pi() / 180.0, phi * TMath::Pi() / 180.0);
   TVector3 stepInDirection(direction);
   stepInDirection.SetMag(step);

   std::cout << " Printing tests \n";
   direction.Print();
   stepInDirection.Print();

   TVector3 magField(0, 0, 0);
   TVector3 aPosition(0, 0, 0);

   Double_t /*Br[n], Bz[n], Bx[n], By[n],*/ x[n], Babs[n];

   Int_t N = 0;
   for (Int_t i = 0; i < n; i++) {
      GetFieldWithVector(aPosition, magField);
      x[N]  = aPosition.Mag();
      //Bz[N] = magField.Z();
      //Bx[N] = magField.X();
      //By[N] = magField.Y();
      Babs[N] = magField.Mag();
      //Br[N] = TMath::Sqrt(magField.X() * magField.X() + magField.Y() * magField.Y());
      N++;
      aPosition += stepInDirection;
   }

   if (fBrayGraph) delete fBrayGraph;
   fBrayGraph = new TGraph(
      n, x, Babs);
   /*Form("TargetBray%ddeg",(int)(fPolarizationAngle*180.0/TMath::Pi())),
             Form("Target Field along ray, #scale[1.3]{#font[22]{|B|}}, with #theta_{targ}=%f deg",fPolarizationAngle*180.0/TMath::Pi()),*/
   fBrayGraph->GetXaxis()->SetTitle("x [cm]");
   fBrayGraph->GetYaxis()->SetTitle("B [T]");
   fCanvas->cd(1)->cd(2);
   fBrayGraph->Draw("alp");
   fCanvas->Update();

}
//_____________________________________________________

void InSANEMagneticField::LookAtField(const Int_t n, Double_t step /*cm*/)
{
   this->LookAtFieldRadialComponent();
   this->LookAtFieldAbsoluteValue();
   this->LookAtFieldXComponent();
   this->LookAtFieldYComponent();
   this->LookAtFieldZComponent();
}
//_____________________________________________________

void InSANEMagneticField::LookAtFieldAbsoluteValue(const Int_t n , Double_t step)
{
   Double_t BAbs[n*n], /*Bz[n*n],*/ y[n*n], z[n*n];
   Double_t point[4] = {0.0, 0.0, 0.0, 0.0};
   Double_t Bfield[3] = {0.0, 0.0, 0.0};
   GetCanvas();
   Int_t N = 0;
   for (Int_t i = 0; i < n; i++) {
      for (Int_t j = 0; j < n; j++) {
         point[0] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[1] = ((Double_t)i - (Double_t)n / 2.0) * step;
         point[2] = ((Double_t)j - (Double_t)n / 2.0) * step;
         GetFieldValue(&point[0], &Bfield[0]);
         y[N] = point[0];
         z[N] = point[2];
         BAbs[N] = TMath::Sqrt(Bfield[2] * Bfield[2] + Bfield[1] * Bfield[1] + Bfield[0] * Bfield[0]);
         N++;
      }
   }
   if (fBAbsGraph) delete fBAbsGraph;
   fBAbsGraph = new TGraph2D(Form("TargetBAbs%fdeg", fPolarizationAngle * 180.0 / TMath::Pi()),
                             Form("Target Field, #scale[1.3]{#font[22]{|B|}}, with #theta_{targ}=%f deg", fPolarizationAngle * 180.0 / TMath::Pi()),
                             n * n, y, z, BAbs);
   fBAbsGraph->GetXaxis()->SetTitle("x [cm]");
   fBAbsGraph->GetYaxis()->SetTitle("z [cm]");
   fCanvas->cd(1)->cd(2);
   fBAbsGraph->Draw("colz");
   TArrow ar1(0.0, 0.0, 15.0 * TMath::Sin(40.0 * TMath::Pi() / 180.0), 15.0 * TMath::Cos(40.0 * TMath::Pi() / 180.0), 0.02, "|>");
   TArrow ar2(-15.0 * TMath::Sin(0.0 * TMath::Pi() / 180.0), -15.0 * TMath::Cos(0.0 * TMath::Pi() / 180.0), 0.0, 0.0, 0.02, "|>");
   ar1.SetLineWidth(2);
   ar2.SetLineWidth(2);
   ar1.DrawClone();
   ar2.DrawClone();
   fCanvas->Update();
   PrintTestPoint();
}
//_____________________________________________________

