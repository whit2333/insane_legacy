#include "InSANEMeasuredAsymmetry.h"
#include "InSANEMathFunc.h"

//_____________________________________________________________________________

InSANEMeasuredAsymmetry::InSANEMeasuredAsymmetry(Int_t n): InSANEAsymmetry(n),
   fRunNumber(0), fDilution(0.12), fAsymmetry(0.0), fError(0.0),
   fChargeSwap(false), fHalfWavePlate(1.0), fPb(0.0), fPt(0.0) 
   //fCharge({0.0,0.0}), fLiveTime({0.0,0.0})
{
  fCharge[0] = 1.0;
  fCharge[1] = 1.0;
  fLiveTime[0] = 1.0;
  fLiveTime[1] = 1.0;
   // systematic errors
   fdelta_Pb   =  0.01;
   fdelta_Pt   =  0.04;
   fdelta_df   =  0.02;
   fdelta_AQ   =  0.0015;
   fdelta_ALT  =  0.003;

   // Variable ranges. 
   fEnergyMin   = 0.5;
   fEnergyMax   = 3.0;
   fxMin        = 0.1;
   fxMax        = 0.9;
   fQ2Min = 0.0;
   fQ2Max = 9.0;
   fWMin        = 1.0;
   fWMax        = 4.0;
   fNuMin       = 0.0;
   fNuMax       = 6.0;
   fPhiMin      = -70.0 * TMath::Pi()/180.0;
   fPhiMax      =  70.0 * TMath::Pi()/180.0;
   fThetaMin    = 20.0 * TMath::Pi() / 180.0;
   fThetaMax    = 60.0 * TMath::Pi() / 180.0;

   fBinningBeamEnergy = 5.9;

   fDilutionFromTarget = nullptr;

   //fLiveTime[0]     = 1.0;
   //fLiveTime[1]     = 1.0;
   //fCharge[0]       = 1.0;
   //fCharge[1]       = 1.0;
   //InSANEMeasuredAsymmetry::Initialize();
}
//_____________________________________________________________________________

InSANEMeasuredAsymmetry::~InSANEMeasuredAsymmetry()
{ }
//_____________________________________________________________________________

InSANEMeasuredAsymmetry::InSANEMeasuredAsymmetry(const InSANEMeasuredAsymmetry& rhs)
  :InSANEAsymmetry(rhs)
{
  (*this) = rhs;
}
//______________________________________________________________________________

InSANEMeasuredAsymmetry& InSANEMeasuredAsymmetry::operator=(const InSANEMeasuredAsymmetry& rhs)
{
  InSANEAsymmetry::operator=(rhs);
  if(&rhs != this) {
    bool add_status = TH1::AddDirectoryStatus();
    TH1::AddDirectory(kFALSE);
    fRunNumber         = rhs.fRunNumber         ;
    fDilution          = rhs.fDilution          ;
    fAsymmetry         = rhs.fAsymmetry         ;
    fError             = rhs.fError             ;
    fChargeSwap        = rhs.fChargeSwap        ;
    fHalfWavePlate     = rhs.fHalfWavePlate     ;
    fPb                = rhs.fPb                ;
    fPt                = rhs.fPt                ;
    fCharge            = rhs.fCharge            ;
    fLiveTime          = rhs.fLiveTime          ;
    fdelta_Pb          = rhs.fdelta_Pb          ;
    fdelta_Pt          = rhs.fdelta_Pt          ;
    fdelta_df          = rhs.fdelta_df          ;
    fdelta_AQ          = rhs.fdelta_AQ          ;
    fdelta_ALT         = rhs.fdelta_ALT         ;
    fQ2Min             = rhs.fQ2Min             ;
    fQ2Max             = rhs.fQ2Max             ;
    fxMin              = rhs.fxMin              ;
    fxMax              = rhs.fxMax              ;
    fEnergyMin         = rhs.fEnergyMin         ;
    fEnergyMax         = rhs.fEnergyMax         ;
    fNuMin             = rhs.fNuMin             ;
    fNuMax             = rhs.fNuMax             ;
    fWMin              = rhs.fWMin              ;
    fWMax              = rhs.fWMax              ;
    fBinningBeamEnergy = rhs.fBinningBeamEnergy ;
    fThetaMin          = rhs.fThetaMin          ;
    fThetaMax          = rhs.fThetaMax          ;
    fPhiMin            = rhs.fPhiMin            ;
    fPhiMax            = rhs.fPhiMax            ;
    fRunSummary        = rhs.fRunSummary        ;
    fAsymmetryVsx      = rhs.fAsymmetryVsx      ;
    fAsymmetryVsW      = rhs.fAsymmetryVsW      ;
    fAsymmetryVsNu     = rhs.fAsymmetryVsNu     ;
    fAsymmetryVsE      = rhs.fAsymmetryVsE      ;
    fAsymmetryVsTheta  = rhs.fAsymmetryVsTheta  ;
    fAsymmetryVsPhi    = rhs.fAsymmetryVsPhi    ;
    fAvgKineVsx        = rhs.fAvgKineVsx        ;
    fAvgKineVsW        = rhs.fAvgKineVsW        ;
    fAvgKineVsNu       = rhs.fAvgKineVsNu       ;
    fAvgKineVsE        = rhs.fAvgKineVsE        ;
    fAvgKineVsTheta    = rhs.fAvgKineVsTheta    ;
    fAvgKineVsPhi      = rhs.fAvgKineVsPhi      ;
    fNPlusVsQ2         = rhs.fNPlusVsQ2         ;
    fNMinusVsQ2        = rhs.fNMinusVsQ2        ;
    fNPlusVsx          = rhs.fNPlusVsx          ;
    fNMinusVsx         = rhs.fNMinusVsx         ;
    fNPlusVsW          = rhs.fNPlusVsW          ;
    fNMinusVsW         = rhs.fNMinusVsW         ;
    fNPlusVsNu         = rhs.fNPlusVsNu         ;
    fNMinusVsNu        = rhs.fNMinusVsNu        ;
    fNPlusVsE          = rhs.fNPlusVsE          ;
    fNMinusVsE         = rhs.fNMinusVsE         ;
    fNPlusVsPhi        = rhs.fNPlusVsPhi        ;
    fNMinusVsPhi       = rhs.fNMinusVsPhi       ;
    fNPlusVsTheta      = rhs.fNPlusVsTheta      ;
    fNMinusVsTheta     = rhs.fNMinusVsTheta     ;
    fDilutionVsx       = rhs.fDilutionVsx       ;
    fDilutionVsW       = rhs.fDilutionVsW       ;
    fDilutionVsNu      = rhs.fDilutionVsNu      ;
    fDilutionVsE       = rhs.fDilutionVsE       ;
    fDilutionVsTheta   = rhs.fDilutionVsTheta   ;
    fDilutionVsPhi     = rhs.fDilutionVsPhi     ;
    fSystErrVsx        = rhs.fSystErrVsx        ;
    fSystErrVsW        = rhs.fSystErrVsW        ;
    fSystErrVsNu       = rhs.fSystErrVsNu       ;
    fSystErrVsE        = rhs.fSystErrVsE        ;
    fSystErrVsTheta    = rhs.fSystErrVsTheta    ;
    fSystErrVsPhi      = rhs.fSystErrVsPhi      ;
    fSystematics_x     = rhs.fSystematics_x     ;
    fSystematics_W     = rhs.fSystematics_W     ;
    fSystematics_Nu    = rhs.fSystematics_Nu    ;
    fSystematics_E     = rhs.fSystematics_E     ;
    fSystematics_Theta = rhs.fSystematics_Theta ;
    fSystematics_Phi   = rhs.fSystematics_Phi   ;
    fMask_x            = rhs.fMask_x            ;
    fMask_W            = rhs.fMask_W            ;
    fMask_E            = rhs.fMask_E            ;
    fMask_Nu           = rhs.fMask_Nu           ;
    fMask_Theta        = rhs.fMask_Theta        ;
    fMask_Phi          = rhs.fMask_Phi          ;
    fDilutionFromTarget = nullptr;
    TH1::AddDirectory(add_status);
  }
  return *this;
}
//______________________________________________________________________________

void InSANEMeasuredAsymmetry::Initialize()
{
    bool add_status = TH1::AddDirectoryStatus();
   TH1::AddDirectory(kFALSE);
   Int_t n = fNumber; 

   //---------------------
   fNPlusVsx          = TH1F(Form("fNPlusVsx-%d", n),         Form("N_plus Vs x %d;x", n), 
                        5, fxMin, fxMax);
   fNPlusVsNu         = TH1F(Form("fNPlusVsNu-%d", n),        Form("N_plus Vs #nu %d;#nu", n), 
                        5, fNuMin, fNuMax);
   fNPlusVsW          = TH1F(Form("fNPlusVsW-%d", n),         Form("N_plus Vs W %d;W", n), 
                        5, fWMin,fWMax);
   fNPlusVsPhi        = TH1F(Form("fNPlusVsPhi-%d", n),       Form("N_plus Vs #phi %d;#phi", n), 
                        5, fPhiMin, fPhiMax);
   fNPlusVsTheta      = TH1F(Form("fNPlusVsTheta-%d", n),     Form("N_plus Vs #theta %d;#theta", n), 
                        5, fThetaMin,fThetaMax);
   fNPlusVsE          = TH1F(Form("fNPlusVsE-%d", n),         Form("N_plus Vs E %d:E", n), 
                        5, fEnergyMin ,fEnergyMax);
   fNPlusVsQ2         = TH1F(Form("fNPlusVsQ2-%d", n),         Form("N_plus Vs Q2 %d:Q2", n), 
                        50, fQ2Min ,fQ2Max);
   //---------------------
   fNMinusVsx          =  TH1F(Form("fNMinusVsx-%d", n),         Form("N_minus Vs x %d;x", n), 
                        5, fxMin, fxMax);
   fNMinusVsNu         =  TH1F(Form("fNMinusVsNu-%d", n),        Form("N_minus Vs #nu %d;#nu", n), 
                        5, fNuMin, fNuMax);
   fNMinusVsW          =  TH1F(Form("fNMinusVsW-%d", n),         Form("N_minus Vs W %d;W", n), 
                        5, fWMin,fWMax);
   fNMinusVsPhi        =  TH1F(Form("fNMinusVsPhi-%d", n),       Form("N_minus Vs #phi %d;#phi", n), 
                        5, fPhiMin, fPhiMax);
   fNMinusVsTheta      =  TH1F(Form("fNMinusVsTheta-%d", n),     Form("N_minus Vs #theta %d;#theta", n), 
                        5, fThetaMin,fThetaMax);
   fNMinusVsE          =  TH1F(Form("fNMinusVsE-%d", n),         Form("N_minus Vs E %d:E", n), 
                        5, fEnergyMin ,fEnergyMax);
   fNMinusVsQ2         = TH1F(Form("fNMinusVsQ2-%d", n),         Form("N_Minus Vs Q2 %d:Q2", n), 
                        50, fQ2Min ,fQ2Max);
   //---------------------
   fAsymmetryVsx          =  TH1F(Form("fAsymmetryVsx-%d", n),         Form("Asymmetry Vs x %d;x", n), 
                        5, fxMin, fxMax);
   fAsymmetryVsNu         =  TH1F(Form("fAsymmetryVsNu-%d", n),        Form("Asymmetry Vs #nu %d;#nu", n), 
                        5, fNuMin, fNuMax);
   fAsymmetryVsW          =  TH1F(Form("fAsymmetryVsW-%d", n),         Form("Asymmetry Vs W %d;W", n), 
                        5, fWMin,fWMax);
   fAsymmetryVsPhi        =  TH1F(Form("fAsymmetryVsPhi-%d", n),       Form("Asymmetry Vs #phi %d;#phi", n), 
                        5, fPhiMin, fPhiMax);
   fAsymmetryVsTheta      =  TH1F(Form("fAsymmetryVsTheta-%d", n),     Form("Asymmetry Vs #theta %d;#theta", n), 
                        5, fThetaMin,fThetaMax);
   fAsymmetryVsE          =  TH1F(Form("fAsymmetryVsE-%d", n),         Form("Asymmetry Vs E %d:E", n), 
                        5, fEnergyMin ,fEnergyMax);
   //---------------------
   fDilutionVsx          =  TH1F(Form("fDilutionVsx-%d", n),         Form("Dilution Vs x %d;x", n), 
                        5, fxMin, fxMax);
   fDilutionVsNu         =  TH1F(Form("fDilutionVsNu-%d", n),        Form("Dilution Vs #nu %d;#nu", n), 
                        5, fNuMin, fNuMax);
   fDilutionVsW          =  TH1F(Form("fDilutionVsW-%d", n),         Form("Dilution Vs W %d;W", n), 
                        5, fWMin,fWMax);
   fDilutionVsPhi        =  TH1F(Form("fDilutionVsPhi-%d", n),       Form("Dilution Vs #phi %d;#phi", n), 
                        5, fPhiMin, fPhiMax);
   fDilutionVsTheta      =  TH1F(Form("fDilutionVsTheta-%d", n),     Form("Dilution Vs #theta %d;#theta", n), 
                        5, fThetaMin,fThetaMax);
   fDilutionVsE          =  TH1F(Form("fDilutionVsE-%d", n),         Form("Dilution Vs E %d:E", n), 
                        5, fEnergyMin ,fEnergyMax);
   //---------------------
   fSystErrVsx          =  TH1F(Form("fSystErrVsx-%d", n),         Form("SystErr Vs x %d;x", n), 
                        5, fxMin, fxMax);
   fSystErrVsNu         =  TH1F(Form("fSystErrVsNu-%d", n),        Form("SystErr Vs #nu %d;#nu", n), 
                        5, fNuMin, fNuMax);
   fSystErrVsW          =  TH1F(Form("fSystErrVsW-%d", n),         Form("SystErr Vs W %d;W", n), 
                        5, fWMin,fWMax);
   fSystErrVsPhi        =  TH1F(Form("fSystErrVsPhi-%d", n),       Form("SystErr Vs #phi %d;#phi", n), 
                        5, fPhiMin, fPhiMax);
   fSystErrVsTheta      =  TH1F(Form("fSystErrVsTheta-%d", n),     Form("SystErr Vs #theta %d;#theta", n), 
                        5, fThetaMin,fThetaMax);
   fSystErrVsE          =  TH1F(Form("fSystErrVsE-%d", n),         Form("SystErr Vs E %d:E", n), 
                        5, fEnergyMin ,fEnergyMax);
   //---------------------
   fAvgKineVsx =  InSANEAveragedKinematics1D(Form("avgkine-%d-Vsx",n),Form("avg-kine-%d Vs. x",n));
   fAvgKineVsx.CreateHistograms(fAsymmetryVsx); 

   fAvgKineVsW =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsW",n),Form("avg-kine-%d Vs. W",n));
   fAvgKineVsW.CreateHistograms(fAsymmetryVsW); 

   fAvgKineVsNu =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsNu",n),Form("avg-kine-%d Vs. Nu",n));
   fAvgKineVsNu.CreateHistograms(fAsymmetryVsNu); 

   fAvgKineVsE =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsE",n),Form("avg-kine-%d Vs. E",n));
   fAvgKineVsE.CreateHistograms(fAsymmetryVsE); 

   fAvgKineVsTheta =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsTheta",n),Form("avg-kine-%d Vs. Theta",n));
   fAvgKineVsTheta.CreateHistograms(fAsymmetryVsTheta); 

   fAvgKineVsPhi =  InSANEAveragedKinematics1D(Form("avgkine-%d-VsPhi",n),Form("avg-kine-%d Vs. phi",n));
   fAvgKineVsPhi.CreateHistograms(fAsymmetryVsPhi); 

   fMask_x     = fNPlusVsx;
   fMask_W     = fNPlusVsW;
   fMask_E     = fNPlusVsE;
   fMask_Nu    = fNPlusVsNu;
   fMask_Theta = fNPlusVsTheta;
   fMask_Phi   = fNPlusVsPhi;

   TH1::AddDirectory(add_status);
}
//_____________________________________________________________________________

void InSANEMeasuredAsymmetry::Reset(Option_t * opt)
{
   fAsymmetryVsx.Reset(opt);
   fAsymmetryVsW.Reset(opt);
   fAsymmetryVsNu.Reset(opt);
   fAsymmetryVsE.Reset(opt);
   fAsymmetryVsTheta.Reset(opt);
   fAsymmetryVsPhi.Reset(opt);

   fNPlusVsx.Reset(opt);
   fNMinusVsx.Reset(opt);

   fNPlusVsW.Reset(opt);
   fNMinusVsW.Reset(opt);

   fNPlusVsNu.Reset(opt);
   fNMinusVsNu.Reset(opt);

   fNPlusVsE.Reset(opt);
   fNMinusVsE.Reset(opt);

   fNPlusVsPhi.Reset(opt);
   fNMinusVsPhi.Reset(opt);

   fNPlusVsTheta.Reset(opt);
   fNMinusVsTheta.Reset(opt);

   fDilutionVsx.Reset(opt);
   fDilutionVsW.Reset(opt);
   fDilutionVsNu.Reset(opt);
   fDilutionVsE.Reset(opt);
   fDilutionVsTheta.Reset(opt);
   fDilutionVsPhi.Reset(opt);

   fSystErrVsx.Reset(opt);
   fSystErrVsW.Reset(opt);
   fSystErrVsNu.Reset(opt);
   fSystErrVsE.Reset(opt);
   fSystErrVsTheta.Reset(opt);
   fSystErrVsPhi.Reset(opt);

   //fAvgKineVsx.CreateHistograms(h);
   //fAvgKineVsW.CreateHistograms(h);
   //fAvgKineVsNu.CreateHistograms(h);
   //fAvgKineVsE.CreateHistograms(h);
   //fAvgKineVsTheta.CreateHistograms(h);
   //fAvgKineVsPhi.CreateHistograms(h);

   for( auto sys : fSystematics_x )     sys.Reset(opt);
   for( auto sys : fSystematics_W )     sys.Reset(opt);
   for( auto sys : fSystematics_Nu )    sys.Reset(opt);
   for( auto sys : fSystematics_E )     sys.Reset(opt);
   for( auto sys : fSystematics_Theta ) sys.Reset(opt);
   for( auto sys : fSystematics_Phi )   sys.Reset(opt);

   ResetMasks(opt);
}
//______________________________________________________________________________

void InSANEMeasuredAsymmetry::ResetMasks(Option_t * opt)
{
   fMask_x.Reset(opt);
   fMask_W.Reset(opt);
   fMask_E.Reset(opt);
   fMask_Nu.Reset(opt);
   fMask_Theta.Reset(opt);
   fMask_Phi.Reset(opt);

   int Nbins = 0;

   Nbins =  fMask_x.GetNbinsX();
   for(int i = 1; i<=Nbins; i++ ) {
      fMask_x.SetBinContent( i, 1.0 );
   }

   Nbins =  fMask_W.GetNbinsX();
   for(int i = 1; i<=Nbins; i++ ) {
      fMask_W.SetBinContent( i, 1.0 );
   }

   Nbins =  fMask_E.GetNbinsX();
   for(int i = 1; i<=Nbins; i++ ) {
      fMask_E.SetBinContent( i, 1.0 );
   }

   Nbins =  fMask_Nu.GetNbinsX();
   for(int i = 1; i<=Nbins; i++ ) {
      fMask_Nu.SetBinContent( i, 1.0 );
   }

   Nbins =  fMask_Theta.GetNbinsX();
   for(int i = 1; i<=Nbins; i++ ) {
      fMask_Theta.SetBinContent( i, 1.0 );
   }

   Nbins =  fMask_Phi.GetNbinsX();
   for(int i = 1; i<=Nbins; i++ ) {
      fMask_Phi.SetBinContent( i, 1.0 );
   }
} 
//______________________________________________________________________________

void InSANEMeasuredAsymmetry::MaskIntersection(TH1F& m1, const TH1F& m2) const 
{
   int Nbins1 =  m1.GetNbinsX();
   int Nbins2 =  m2.GetNbinsX();
   if( Nbins1 != Nbins2 ) {
      Error("MaskIntersection","Histograms have differing numbers of bins. Doing nothing.");
      return;
   }
   for(int i = 1; i<=Nbins1; i++ ) {
      double v1 = m1.GetBinContent( i );
      double v2 = m2.GetBinContent( i );
      m1.SetBinContent( i, v1*v2 );
   }
}
//______________________________________________________________________________

InSANEMeasuredAsymmetry & InSANEMeasuredAsymmetry::operator+=(const InSANEMeasuredAsymmetry &rhs)
{
   //fNumber    += rhs.fNumber;
   InSANEAsymmetry::operator+=(rhs);

   fNPlusVsQ2.Add(      &rhs.fNPlusVsQ2);
   fNMinusVsQ2.Add(     &rhs.fNMinusVsQ2);

   fNPlusVsx.Add(      &rhs.fNPlusVsx);
   fNMinusVsx.Add(     &rhs.fNMinusVsx);

   fNPlusVsW.Add(      &rhs.fNPlusVsW);
   fNMinusVsW.Add(     &rhs.fNMinusVsW);

   fNPlusVsNu.Add(     &rhs.fNPlusVsNu);
   fNMinusVsNu.Add(    &rhs.fNMinusVsNu);

   fNPlusVsE.Add(      &rhs.fNPlusVsE);
   fNMinusVsE.Add(     &rhs.fNMinusVsE);

   fNPlusVsPhi.Add(    &rhs.fNPlusVsPhi);
   fNMinusVsPhi.Add(   &rhs.fNMinusVsPhi);

   fNPlusVsTheta.Add(  &rhs.fNPlusVsTheta);
   fNMinusVsTheta.Add( &rhs.fNMinusVsTheta);

   MaskIntersection(fMask_x, rhs.fMask_x);
   MaskIntersection(fMask_W, rhs.fMask_W);
   MaskIntersection(fMask_E, rhs.fMask_E);
   MaskIntersection(fMask_Nu, rhs.fMask_Nu);
   MaskIntersection(fMask_Theta, rhs.fMask_Theta);
   MaskIntersection(fMask_Phi, rhs.fMask_Phi);

   fRunSummary += rhs.fRunSummary;

   return *this;
}
//_____________________________________________________________________________
const InSANEMeasuredAsymmetry InSANEMeasuredAsymmetry::operator+(const InSANEMeasuredAsymmetry &other)
{
   InSANEMeasuredAsymmetry result = *this;     // Make a copy of myself.  Same as MyClass result(*this);
   result += other;            // Use += to add other to the copy.
   return result;              // All done!
}
//_____________________________________________________________________________

void InSANEMeasuredAsymmetry::SetRunSummary(InSANERunSummary* rs)
{
   if(rs)  {
      fRunSummary    = *rs; // this does everything we want why do we have the rest?
      fHalfWavePlate = rs->fHalfWavePlate;
      if(rs->fHalfWavePlate) {
         fChargeSwap = true;
      } else {
         fChargeSwap = false;
      }
      if(fChargeSwap) {
         fCharge[0] = rs->fQPlus;
         fCharge[1] = rs->fQMinus;
         fLiveTime[0] = rs->fLPlus;
         fLiveTime[1] = rs->fLMinus;
      }else{
         fCharge[0] = rs->fQPlus;
         fCharge[1] = rs->fQMinus;
         fLiveTime[0] = rs->fLPlus;
         fLiveTime[1] = rs->fLMinus;
      }
      if(rs->fRuns.size()>0)fRunNumber = rs->fRuns.at(0);

      fPb = rs->fPBeam*rs->fPbSign/100.0;
      fPt = rs->fPTarget*rs->fPtSign/100.0;
   }
}
//______________________________________________________________________________

Bool_t InSANEMeasuredAsymmetry::IsGoodEvent()
{
   if(!(fQ2Min < fDISEvent->fQ2 && fQ2Max > fDISEvent->fQ2)) return(false);
   return(true);
}
//_____________________________________________________________________________

InSANESystematic* InSANEMeasuredAsymmetry::AddSystematic_x(
      const TH1F& h0, const char * syst_name
    )
{
  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);

  InSANESystematic syst_unc(Form("%s-x",syst_name));
  syst_unc.InitHists(h0);
  unsigned int N = fSystematics_x.size();
  fSystematics_x.push_back(syst_unc);
  TH1::AddDirectory(add_status);
  return (&(fSystematics_x[N]));
}
//______________________________________________________________________________

InSANESystematic* InSANEMeasuredAsymmetry::AddSystematic_W(
      const TH1F& h0, const char * syst_name
    )
{
  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);

  InSANESystematic syst_unc(Form("%s-W",syst_name));
  syst_unc.InitHists(h0);
  unsigned int N = fSystematics_W.size();
  fSystematics_W.push_back(syst_unc);
  TH1::AddDirectory(add_status);
  return (&(fSystematics_W[N]));
}
//______________________________________________________________________________

TH1F InSANEMeasuredAsymmetry::CreateAsymmetryHistogram1D(
      const TH1F& h0,
      const TH1F& h1,
      std::vector<InSANESystematic>& systematic,
      const TH1F * dfHist,
      TH1F * systHist)
            
{
  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);

   Int_t nXbins = h0.GetNbinsX();
   Int_t nYbins = h0.GetNbinsY();
   Int_t nZbins = h0.GetNbinsZ();
   Double_t n0   = 0.0;
   Double_t n1   = 0.0;
   Double_t A    = 0.0;
   Double_t eA   = 0.0;
   Double_t eAsys= 0.0;
   Double_t df   = fDilution;

   TH1F hRes = TH1F(h0);

   systematic.clear();

   InSANESystematic syst_tot("tot_Syst");
   InSANESystematic syst_Pt( "Pt_Syst");
   InSANESystematic syst_Pb( "Pb_Syst");
   InSANESystematic syst_df( "df_Syst");
   InSANESystematic syst_AQ( "AQ_Syst");
   InSANESystematic syst_ALT("ALT_Syst");

   syst_tot.InitHists(h0);
   syst_Pt.InitHists(h0);
   syst_Pb.InitHists(h0);
   syst_df.InitHists(h0);
   syst_AQ.InitHists(h0);
   syst_ALT.InitHists(h0);

   //std::cout << "================ " << fNumber << " =================\n";
   
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   Double_t ALT     = (L1-L2)/(L1+L2);

   //Double_t Araw    = (N1-N2)/(N1+N2);
   double Pt = fPt;
   double Pb = fPb;



   for(int i = 1; i<=nXbins; i++ ) {
      for(int j = 1; j<=nYbins; j++ ) {
         for(int k = 1; k<=nZbins; k++ ) {

            n0 = h0.GetBinContent(i,j,k); 
            n1 = h1.GetBinContent(i,j,k); 
            if(dfHist){
               df  = dfHist->GetBinContent(i,j,k);
            }
            A  = GetAsymmetry(n0,n1,df);
            hRes.SetBinContent(i,j,k,A);

            double delta_dfPtPb = fdelta_Pb*fdelta_Pb/(df*df*Pb*Pb*Pt*Pt) + 
              fdelta_Pt*fdelta_Pt*fdelta_Pb/(df*df*Pb*Pb*Pt*Pt) +
              fdelta_df*fdelta_df*fdelta_Pb/(df*df*Pb*Pb*Pt*Pt);
            delta_dfPtPb = TMath::Sqrt(delta_dfPtPb);

            if (n0 + n1 > 0.0) {
               eA    = GetStatisticalError(n0, n1,df);
               hRes.SetBinError(i,j,k,eA);

               eAsys = GetSystematicError(n0,n1,df);
               if(systHist)systHist->SetBinContent(i,j,k,eAsys);

               //std::cout << " A(" << i << ") = " << A << " +- " << eA << " +- " << eAsys << "\n";
               double dtotdA  = GetRelativeSystUncert_tot(n0,n1,df);
               double dAQdA   = GetRelativeSystUncert_AQ(n0,n1,df);
               double dALTdA  = GetRelativeSystUncert_ALT(n0,n1,df);
               double dPbdA   = GetRelativeSystUncert_Pb(n0,n1,df);
               double dPtdA   = GetRelativeSystUncert_Pt(n0,n1,df);
               double ddfdA   = GetRelativeSystUncert_df(n0,n1,df);

               syst_Pt.fRelativeUncertainties.SetBinContent( i,j,k,dPtdA);
               syst_Pb.fRelativeUncertainties.SetBinContent( i,j,k,dPbdA);
               syst_df.fRelativeUncertainties.SetBinContent( i,j,k,ddfdA);
               syst_AQ.fRelativeUncertainties.SetBinContent( i,j,k,dAQdA);
               syst_ALT.fRelativeUncertainties.SetBinContent(i,j,k,dALTdA);
               syst_tot.fRelativeUncertainties.SetBinContent(i,j,k,dtotdA); 

               syst_Pt.fUncertainties.SetBinContent( i,j,k,fdelta_Pb );
               syst_Pb.fUncertainties.SetBinContent( i,j,k,fdelta_Pt );
               syst_df.fUncertainties.SetBinContent( i,j,k,fdelta_df );
               syst_AQ.fUncertainties.SetBinContent( i,j,k,fdelta_AQ );
               syst_ALT.fUncertainties.SetBinContent(i,j,k,fdelta_ALT);
               syst_tot.fUncertainties.SetBinContent(i,j,k,delta_dfPtPb); 

               syst_Pt. fValues.SetBinContent( i,j,k,fPt );
               syst_Pb. fValues.SetBinContent( i,j,k,fPb);
               syst_df. fValues.SetBinContent( i,j,k,df );
               syst_AQ. fValues.SetBinContent( i,j,k,AQ);
               syst_ALT.fValues.SetBinContent(i,j,k,ALT);
               syst_tot.fValues.SetBinContent(i,j,k,1.0/(fPt*fPb*df)); 
 
            } else {
               eA = 0.0;
               hRes.SetBinError(i,j,k,eA);
               if(systHist)systHist->SetBinContent(i,j,k,eA);
            }
            //std::cout << "n0=" << n0 << " , n1=" << n1 << " ,A=" << A << ", eA=" << eA << "\n";
         }
      }
   }

   hRes.SetMarkerSize(1.0);
   hRes.SetMarkerStyle(20);

   systematic.push_back(syst_tot);
   systematic.push_back(syst_Pt);
   systematic.push_back(syst_Pb);
   systematic.push_back(syst_df);
   systematic.push_back(syst_AQ);
   systematic.push_back(syst_ALT);

   TH1::AddDirectory(add_status);
   return(hRes);
}
//_____________________________________________________________________________
Int_t InSANEMeasuredAsymmetry::CalculateDilutionFactors1D(TH1F * dfHist, const InSANEAveragedKinematics1D *kine){

   Int_t nXbins = dfHist->GetNbinsX();
   Int_t nYbins = dfHist->GetNbinsY();
   Int_t nZbins = dfHist->GetNbinsZ();
   Int_t bin = 0;
   Double_t n0   = 0.0;
   Double_t n1   = 0.0;
   Double_t dilution = fDilution;
   Double_t dilution2 = fDilution;
   Double_t e_df = 0.001;
   Double_t eA   = 0.0;

   for(int i = 0; i<=nXbins; i++ ) {
      for(int j = 0; j<=nYbins; j++ ) {
         for(int k = 0; k<=nZbins; k++ ) {
            bin = dfHist->GetBin(i,j,k);
            Double_t x  = kine->fx.GetBinContent(bin);
            Double_t Q2 = kine->fQ2.GetBinContent(bin);
            Double_t W  = kine->fW.GetBinContent(bin);
            if(fDilutionFromTarget){
               dilution = fDilutionFromTarget->GetDilution(x,Q2); 
               //dilution2 = fDilutionFromTarget->GetDilution_WQ2(W,Q2); 
               e_df     = dilution*0.05;//TMath::Abs(dilution-dilution2);
               std::cout << " df_xQ2(" << x << "," << Q2 << ") = " << dilution << std::endl;
            } else {
               dilution = fDilution;
            }
            //std::cout << " df_WQ2(" << W << "," << Q2 << ") = " << dilution2 << std::endl;
            dfHist->SetBinContent(bin,dilution);
            dfHist->SetBinError(bin,e_df);

         }
      }
   }

   return(0);
}
//______________________________________________________________________________

Int_t InSANEMeasuredAsymmetry::CalculateAsymmetries() {

  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);
   // ----------------------------------------
   //df        = 0.17;
   //fPb       = fRunSummary.fPbSign*fRunSummary.fPBeam/100.0;//run->fBeamPolarization / 100.0;
   //fPt       = fRunSummary.fPtSign*fRunSummary.fPTarget/100.0;//run->fTargetOfflinePolarization / 100.0;
   double dfPbPt    = fDilution * fPb * fPt ;
   double Qplus     = fRunSummary.fQPlus;
   double Qminus    = fRunSummary.fQMinus;
   double Lplus     = fRunSummary.fLPlus;
   double Lminus    = fRunSummary.fLMinus;

   // ----------------------------------------
   // Average the kinematic variables and coefficients
   fAvgKineVsx.Calculate();
   fAvgKineVsW.Calculate();
   fAvgKineVsNu.Calculate();
   fAvgKineVsE.Calculate();
   fAvgKineVsTheta.Calculate();
   fAvgKineVsPhi.Calculate();
   // fAvgKineVsQ2.Calculate();

   // ----------------------------------------
   // Calculate the dilutions 
   CalculateDilutionFactors1D(&fDilutionVsx,     &fAvgKineVsx);
   CalculateDilutionFactors1D(&fDilutionVsW,     &fAvgKineVsW);
   CalculateDilutionFactors1D(&fDilutionVsNu,    &fAvgKineVsNu);
   CalculateDilutionFactors1D(&fDilutionVsE,     &fAvgKineVsE);
   CalculateDilutionFactors1D(&fDilutionVsTheta, &fAvgKineVsTheta);
   CalculateDilutionFactors1D(&fDilutionVsPhi,   &fAvgKineVsPhi);

   fAsymmetryVsx     = CreateAsymmetryHistogram1D(fNPlusVsx    , fNMinusVsx    , fSystematics_x    , &fDilutionVsx    , &fSystErrVsx     ) ;
   fAsymmetryVsW     = CreateAsymmetryHistogram1D(fNPlusVsW    , fNMinusVsW    , fSystematics_W    , &fDilutionVsW    , &fSystErrVsW     ) ;
   fAsymmetryVsNu    = CreateAsymmetryHistogram1D(fNPlusVsNu   , fNMinusVsNu   , fSystematics_Nu   , &fDilutionVsNu   , &fSystErrVsNu    ) ;
   fAsymmetryVsTheta = CreateAsymmetryHistogram1D(fNPlusVsTheta, fNMinusVsTheta, fSystematics_Theta, &fDilutionVsTheta, &fSystErrVsTheta ) ;
   fAsymmetryVsPhi   = CreateAsymmetryHistogram1D(fNPlusVsPhi  , fNMinusVsPhi  , fSystematics_Phi  , &fDilutionVsPhi  , &fSystErrVsPhi   ) ;
   fAsymmetryVsE     = CreateAsymmetryHistogram1D(fNPlusVsE    , fNMinusVsE    , fSystematics_E    , &fDilutionVsE    , &fSystErrVsE     ) ;

   SetColor( 2000 + fNumber%4 );
   TH1::AddDirectory(add_status);

   return(0);
}
//______________________________________________________________________________

void InSANEMeasuredAsymmetry::SetColor(int c)
{
   fAsymmetryVsx.SetMarkerColor(     c);
   fAsymmetryVsx.SetLineColor(       c);
   fAsymmetryVsE.SetMarkerColor(     c);
   fAsymmetryVsE.SetLineColor(       c);
   fAsymmetryVsW.SetMarkerColor(     c);
   fAsymmetryVsW.SetLineColor(       c);
   fAsymmetryVsTheta.SetMarkerColor( c);
   fAsymmetryVsTheta.SetLineColor(   c);
   fAsymmetryVsPhi.SetMarkerColor(   c);
   fAsymmetryVsPhi.SetLineColor(     c);
   fAsymmetryVsNu.SetMarkerColor(    c);
   fAsymmetryVsNu.SetLineColor(      c);

   fNPlusVsx.SetMarkerColor(     c);
   fNPlusVsx.SetLineColor(       c);
   fNPlusVsE.SetMarkerColor(     c);
   fNPlusVsE.SetLineColor(       c);
   fNPlusVsW.SetMarkerColor(     c);
   fNPlusVsW.SetLineColor(       c);
   fNPlusVsTheta.SetMarkerColor( c);
   fNPlusVsTheta.SetLineColor(   c);
   fNPlusVsPhi.SetMarkerColor(   c);
   fNPlusVsPhi.SetLineColor(     c);
   fNPlusVsNu.SetMarkerColor(    c);
   fNPlusVsNu.SetLineColor(      c);
   fNPlusVsQ2.SetMarkerColor(    c);
   fNPlusVsQ2.SetLineColor(      c);

   fNMinusVsx.SetMarkerColor(     c);
   fNMinusVsx.SetLineColor(       c);
   fNMinusVsE.SetMarkerColor(     c);
   fNMinusVsE.SetLineColor(       c);
   fNMinusVsW.SetMarkerColor(     c);
   fNMinusVsW.SetLineColor(       c);
   fNMinusVsTheta.SetMarkerColor( c);
   fNMinusVsTheta.SetLineColor(   c);
   fNMinusVsPhi.SetMarkerColor(   c);
   fNMinusVsPhi.SetLineColor(     c);
   fNMinusVsNu.SetMarkerColor(    c);
   fNMinusVsNu.SetLineColor(      c);
   fNMinusVsQ2.SetMarkerColor(    c);
   fNMinusVsQ2.SetLineColor(      c);
}
//_____________________________________________________________________________

Int_t InSANEMeasuredAsymmetry::CountEvent() {
   if (!fDISEvent) {
      Warning("CountEvent", "No fDISEvent is null\n");
      return(-1);
   }
   if (IsGoodEvent()) if( IsInGroup(fDISEvent->fGroup) )  {

      // Averaged kinematics
      fAvgKineVsx.AddEvent(fDISEvent->fx,fDISEvent);
      fAvgKineVsW.AddEvent(fDISEvent->fW,fDISEvent);
      fAvgKineVsNu.AddEvent(fDISEvent->fBeamEnergy-fDISEvent->fEnergy,fDISEvent);
      fAvgKineVsE.AddEvent(fDISEvent->fEnergy,fDISEvent);
      fAvgKineVsTheta.AddEvent(fDISEvent->fTheta,fDISEvent);
      fAvgKineVsPhi.AddEvent(fDISEvent->fPhi,fDISEvent);

      if ((fDISEvent->fHelicity) == 1) {
         fCount[0]++;
         fNPlusVsx.Fill((fDISEvent->fx));
         fNPlusVsW.Fill((fDISEvent->fW));
         fNPlusVsNu.Fill(fDISEvent->fBeamEnergy-fDISEvent->fEnergy);
         fNPlusVsQ2.Fill( fDISEvent->fQ2 );
         fNPlusVsE.Fill((fDISEvent->fEnergy));
         fNPlusVsPhi.Fill(fDISEvent->fPhi);
         fNPlusVsTheta.Fill(fDISEvent->fTheta);
      } else if ((fDISEvent->fHelicity) == -1) {
         fCount[1]++;
         fNMinusVsx.Fill((fDISEvent->fx));
         fNMinusVsW.Fill((fDISEvent->fW));
         fNMinusVsNu.Fill((fDISEvent->fBeamEnergy-fDISEvent->fEnergy));
         fNMinusVsE.Fill(fDISEvent->fEnergy);
         fNMinusVsQ2.Fill( fDISEvent->fQ2 );
         fNMinusVsPhi.Fill(fDISEvent->fPhi);
         fNMinusVsTheta.Fill(fDISEvent->fTheta);
      }
      return(1);
   }
   /* else */
   return(0);
}
//_____________________________________________________________________________

Int_t InSANEMeasuredAsymmetry::CountEvent(std::vector<int>& groups) {
   if (!fDISEvent) {
      Warning("CountEvent", "No fDISEvent is null\n");
      return(-1);
   }
   if (IsGoodEvent()) if( IsInGroupList(groups) )  {

      // Averaged kinematics
      fAvgKineVsx.AddEvent(fDISEvent->fx,fDISEvent);
      fAvgKineVsW.AddEvent(fDISEvent->fW,fDISEvent);
      fAvgKineVsNu.AddEvent(fDISEvent->fBeamEnergy-fDISEvent->fEnergy,fDISEvent);
      fAvgKineVsE.AddEvent(fDISEvent->fEnergy,fDISEvent);
      fAvgKineVsTheta.AddEvent(fDISEvent->fTheta,fDISEvent);
      fAvgKineVsPhi.AddEvent(fDISEvent->fPhi,fDISEvent);

      if ((fDISEvent->fHelicity) == 1) {
         fCount[0]++;
         fNPlusVsx.Fill((fDISEvent->fx));
         fNPlusVsW.Fill((fDISEvent->fW));
         fNPlusVsNu.Fill(fDISEvent->fBeamEnergy-fDISEvent->fEnergy);
         fNPlusVsE.Fill((fDISEvent->fEnergy));
         fNPlusVsPhi.Fill(fDISEvent->fPhi);
         fNPlusVsTheta.Fill(fDISEvent->fTheta);
         fNPlusVsQ2.Fill( fDISEvent->fQ2 );
      } else if ((fDISEvent->fHelicity) == -1) {
         fCount[1]++;
         fNMinusVsx.Fill((fDISEvent->fx));
         fNMinusVsW.Fill((fDISEvent->fW));
         fNMinusVsNu.Fill((fDISEvent->fBeamEnergy-fDISEvent->fEnergy));
         fNMinusVsE.Fill(fDISEvent->fEnergy);
         fNMinusVsPhi.Fill(fDISEvent->fPhi);
         fNMinusVsTheta.Fill(fDISEvent->fTheta);
         fNMinusVsQ2.Fill( fDISEvent->fQ2 );
      }
      return(1);
   }
   /* else */
   return(0);
}
//______________________________________________________________________________

void InSANEMeasuredAsymmetry::CalculateBinMask_W()
{
   // Get the number of bins for (1D) x and (3D) W-x-Phi histograms
   Int_t nBins     = fAsymmetryVsW.GetNbinsX();
   for(int i = 1;i<= nBins;i++){
      bool     is_bad_bin = false;
      Double_t x   = fAvgKineVsW.fx.GetBinContent(i);
      Double_t Q2  = fAvgKineVsW.fQ2.GetBinContent(i);
      Double_t W   = fAvgKineVsW.fW.GetBinContent(i);
      Double_t E   = fAvgKineVsW.fE.GetBinContent(i);
      Double_t th  = fAvgKineVsW.fTheta.GetBinContent(i);
      Double_t phi = fAvgKineVsW.fPhi.GetBinContent(i);
      Double_t df  = fDilutionVsW.GetBinContent(i);
      Double_t n0  = fNPlusVsW.GetBinContent(i);
      Double_t n1  = fNMinusVsW.GetBinContent(i);
      Double_t A   = fAsymmetryVsW.GetBinContent(i);
      Double_t dA  = fAsymmetryVsW.GetBinError(i);
      Double_t dAsys  = fSystErrVsW.GetBinContent(i);

      if( A     == 0.0 ) is_bad_bin = true;
      if( n1+n0 == 0   ) is_bad_bin = true;
      if( dA    == 0.0 ) is_bad_bin = true;
      if( dAsys == 0.0 ) is_bad_bin = true;
      if( x > 0.9  ) is_bad_bin = true;
      if( x < 0.1  ) is_bad_bin = true;

      if( is_bad_bin ) {
        fMask_W.SetBinContent(i,0.0);
      }

   }
}
//______________________________________________________________________________

void InSANEMeasuredAsymmetry::PrintBinnedTable(std::ostream& stream) const
{
   // Get the number of bins for (1D) x and (3D) W-x-Phi histograms
   Int_t nBins     = fAsymmetryVsW.GetNbinsX();
   for(int i = 1;i<= nBins;i++){

      Double_t x   = fAvgKineVsW.fx.GetBinContent(i);
      Double_t Q2  = fAvgKineVsW.fQ2.GetBinContent(i);
      Double_t W   = fAvgKineVsW.fW.GetBinContent(i);
      Double_t E   = fAvgKineVsW.fE.GetBinContent(i);
      Double_t th  = fAvgKineVsW.fTheta.GetBinContent(i);
      Double_t phi = fAvgKineVsW.fPhi.GetBinContent(i);
      Double_t df  = fDilutionVsW.GetBinContent(i);
      Double_t n0  = fNPlusVsW.GetBinContent(i);
      Double_t n1  = fNMinusVsW.GetBinContent(i);
      Double_t A   = fAsymmetryVsW.GetBinContent(i);
      Double_t dA  = fAsymmetryVsW.GetBinError(i);
      Double_t dAsys  = fSystErrVsW.GetBinContent(i);
      if( A == 0.0 ) continue;
      if( n1+n0 == 0 ) continue;
      //if( n1 == 0 ) continue;
      stream << std::setprecision(6)
         << std::setw(6) << fRunNumber   << " ";
      stream << std::setprecision(9)
         << std::setw(9) << n0    << " "
         << std::setw(9) << n1    << " ";
      stream << std::setprecision(4)
         << std::setw(8) << fLiveTime[0] << " "
         << std::setw(8) << fLiveTime[1] << " "
         << std::setw(8) << fCharge[0]   << " "
         << std::setw(8) << fCharge[1]   << " "
         << std::setw(8) << df           << " "
         << std::setw(8) << fPt          << " "
         << std::setw(8) << fPb          << " "
         << std::setw(8) << A            << " "
         << std::setw(8) << dA           << " "
         << std::setw(8) << dAsys        << " "
         << std::setw(8) << x            << " "
         << std::setw(8) << Q2           << " "
         << std::setw(8) << W            << " "
         << std::setw(8) << E            << " "
         << std::setw(8) << th           << " "
         << std::setw(8) << phi          << " ";
      for(auto  syst: fSystematics_W) {
         stream << std::setw(15) << syst.fValues.GetBinContent(i) << " ";
         stream << std::setw(15) << syst.fRelativeUncertainties.GetBinContent(i) << " ";
      }
      stream << "\n";
   }
}
//_____________________________________________________________________________
void InSANEMeasuredAsymmetry::PrintBinnedTableHead(std::ostream& file) const
{
   file 
      << std::setw(6) << "Run" << " "
      << std::setw(9) << "n0"        << " "
      << std::setw(9) << "n1"        << " "
      << std::setw(8) << "LT[0]"     << " "
      << std::setw(8) << "LT[1]"     << " "
      << std::setw(8) << "Q[0]"      << " "
      << std::setw(8) << "Q[1]"      << " "
      << std::setw(8) << "df"        << " "
      << std::setw(8) << "Pt"        << " "
      << std::setw(8) << "Pb"        << " "
      << std::setw(8) << "A"         << " "
      << std::setw(8) << "dA(stat)"  << " "
      << std::setw(8) << "dA(syst)"  << " "
      << std::setw(8) << "x"         << " "
      << std::setw(8) << "Q2"        << " "
      << std::setw(8) << "W"         << " "
      << std::setw(8) << "E"         << " "
      << std::setw(8) << "th"        << " "
      << std::setw(8) << "phi"       << " ";
   for(auto  syst: fSystematics_W) {
      file << std::setw(15) << syst.GetName() << " ";
      file << std::setw(9) << syst.GetName() << "RelUnc" << " ";
   }
   file << "\n";
}
//______________________________________________________________________________
void InSANEMeasuredAsymmetry::PrintAsymTable(std::ostream& stream) const
{

   stream  << "\\begin{tabular}{|l|l|l|l|l|l|l|l|l|}\n";
   stream  << "\\hline \n";
   PrintAsymTableHead(stream);
   stream  << "\\hline \n";


   // Get the number of bins for (1D) x and (3D) W-x-Phi histograms
   Int_t nBins     = fAsymmetryVsW.GetNbinsX();

   for(int i = 1;i<= nBins;i++){

      Double_t x   = fAvgKineVsW.fx.GetBinContent(i);
      Double_t Q2  = fAvgKineVsW.fQ2.GetBinContent(i);
      Double_t W   = fAvgKineVsW.fW.GetBinContent(i);
      Double_t E   = fAvgKineVsW.fE.GetBinContent(i);
      Double_t th  = fAvgKineVsW.fTheta.GetBinContent(i);
      Double_t phi = fAvgKineVsW.fPhi.GetBinContent(i);
      Double_t A   = fAsymmetryVsW.GetBinContent(i);
      Double_t dA  = fAsymmetryVsW.GetBinError(i);
      Double_t dAsys  = fSystErrVsW.GetBinContent(i);
      if( x>=0.2 && A != 0.0 && dAsys != 0.0 && dA != 0.0 ) 
         stream  
            << std::setw(10) << std::setprecision(5) << x            << " & "
            << std::setw(10) << std::setprecision(5) << Q2           << " & "
            << std::setw(10) << std::setprecision(5) << W            << " & "
            << std::setw(10) << std::setprecision(5) << E            << " & "
            << std::setw(10) << std::setprecision(5) << th/degree           << " & "
            << std::setw(10) << std::setprecision(5) << phi/degree         << " & "
            << std::setw(10) << std::setprecision(5) << A            << " & "
            << std::setw(10) << std::setprecision(5) << dA           << " & "
            << std::setw(10) << std::setprecision(5) << dAsys        << " \\\\ "
            << std::endl;
   }
   stream  << "\\hline \n";
   stream  << "\\end{tabular}\n";
}
//_____________________________________________________________________________
void InSANEMeasuredAsymmetry::PrintAsymTableHead(std::ostream& file) const
{
   file 
      << std::setw(10) << "$x$    "        << " & "
      << std::setw(10) << "$Q^2$ [GeV$^2$]   "        << " & "
      << std::setw(10) << "$W$ [GeV]    "        << " & "
      << std::setw(10) << "$E^{\\prime}$ [GeV]    "        << " & "
      << std::setw(10) << "$\\theta$ [deg.]   "        << " & "
      << std::setw(10) << "$\\phi$  [deg.]"        << " & "
      << std::setw(10) << "$A$    "        << " & "
      << std::setw(10) << "$\\delta_A$ (stat.)"     << " & "
      << std::setw(10) << "$\\delta_A$ (syst.)"     << " \\\\ "
      << std::endl;
}
//_____________________________________________________________________________
//_____________________________________________________________________________
void InSANEMeasuredAsymmetry::PrintTableHead(std::ostream& file) const
{
   file << std::setw(5)  << "Group" << " "
      << std::setw(10) << "RunNumber" << " "
      << std::setw(10) << "Q2_max" << " "
      << std::setw(10) << "Q2_min"  << " "
      << std::setw(10) << "Q2_avg"  << " "
      << std::setw(10) << "df"   << " "
      << std::setw(10) << "Pb"   << " "
      << std::setw(10) << "Pt"   << " "
      << std::setw(10) << "Qplus"   << " "
      << std::setw(10) << "Qminus"   << " "
      << std::setw(10) << "LTplus"   << " "
      << std::setw(10) << "LTminus"   << " "
      << std::setw(10) << "Nplus"   << " "
      << std::setw(10) << "Nminus"   << " "
      << std::setw(10) << "A" << " "
      << std::setw(10) << "A_error" << "\n";
}
//_____________________________________________________________________________
void InSANEMeasuredAsymmetry::PrintTable(std::ostream& file) const
{
   file << std::showpoint;
   file << std::setw(5) << fGroup << " "
      << std::setw(10) << fRunNumber << " "
      << std::setw(10) << fQ2Min << " "
      << std::setw(10)
      << fQ2Max << " "
      << std::setw(10)
      << (fQ2Min + fQ2Max) / 2.0 << " "
      << std::setw(10)
      << fDilution << " "
      << std::setw(10)
      << fPb << " "
      << std::setw(10)
      << fPt << " "
      << std::setw(10)
      << fCharge[0] << " "
      << std::setw(10)
      << fCharge[1] << " "
      << std::setw(10)
      << fLiveTime[0] << " "
      << std::setw(10)
      << fLiveTime[1] << " "
      << std::setw(10)
      << fCount[0] << " "
      << std::setw(10)
      << fCount[1] << " "
      << std::setw(10)
      << GetAsymmetry(fCount[0], fCount[1],fDilution) << " "
      << std::setw(10)
      << GetTotalError(fCount[0], fCount[1],fDilution) << "\n";

}
//_____________________________________________________________________________
void InSANEMeasuredAsymmetry::Print(Option_t * opt) const
{
   PrintTableHeader(std::cout);
   PrintTable(std::cout);
}
//_____________________________________________________________________________

void InSANEMeasuredAsymmetry::SetDirectory(TDirectory * dir)
{
   fAsymmetryVsx.SetDirectory(dir);
   fAsymmetryVsW.SetDirectory(dir);
   fAsymmetryVsNu.SetDirectory(dir);
   fAsymmetryVsTheta.SetDirectory(dir);
   fAsymmetryVsPhi.SetDirectory(dir);

   fDilutionVsx.SetDirectory(dir);
   fDilutionVsW.SetDirectory(dir);
   fDilutionVsNu.SetDirectory(dir);
   fDilutionVsTheta.SetDirectory(dir);
   fDilutionVsPhi.SetDirectory(dir);

   fSystErrVsx.SetDirectory(dir);
   fSystErrVsW.SetDirectory(dir);
   fSystErrVsNu.SetDirectory(dir);
   fSystErrVsTheta.SetDirectory(dir);
   fSystErrVsPhi.SetDirectory(dir);

   fNPlusVsQ2.SetDirectory(dir);
   fNMinusVsQ2.SetDirectory(dir);

   fNPlusVsx.SetDirectory(dir);
   fNMinusVsx.SetDirectory(dir);

   fNPlusVsW.SetDirectory(dir);
   fNMinusVsW.SetDirectory(dir);

   fNPlusVsNu.SetDirectory(dir);
   fNMinusVsNu.SetDirectory(dir);

   fNPlusVsE.SetDirectory(dir);
   fNMinusVsE.SetDirectory(dir);

   fNPlusVsPhi.SetDirectory(dir);
   fNMinusVsPhi.SetDirectory(dir);

   fNPlusVsTheta.SetDirectory(dir);
   fNMinusVsTheta.SetDirectory(dir);

}
//_____________________________________________________________________________

Double_t InSANEMeasuredAsymmetry::GetAsymmetry(Double_t N1, Double_t N2, Double_t df) const
{
   if(N1 == 0 || N2 == 0) {
      //Error("GetAsymmetry", "Zero count.");
      return(0.0);
   }
   if( (df == 0) ) {
      return(0.0);
   }
   if( (fPt == 0) || (fPb  == 0) ) {
      //Error("GetAsymmetry", "Zero count.");
      return(0.0);
   }
   Double_t Pb = fPb;
   Double_t Pt = fPt;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   Double_t ALT     = (L1-L2)/(L1+L2);
   Double_t Araw    = (N1-N2)/(N1+N2);
   Double_t Aexp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1.0 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t res = 1.0 / (df * Pb * Pt);
   res = res * (Aexp);
   return(res);
}
//_____________________________________________________________________________

Double_t InSANEMeasuredAsymmetry::GetStatisticalError(Double_t N1, Double_t N2, Double_t df) const
{
   // Returns statistical error of asymmetry
   // up to dilution and polarization corrections
   using namespace TMath;
   Double_t Pb = fPb;
   Double_t Pt = fPt;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // Charge asymmetry
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   // Livetime asymmetry
   Double_t ALT     = (L1-L2)/(L1+L2);

   // raw asymmetry statistical error squared
   Double_t Araw   = (N1-N2)/(N1+N2);
   Double_t eAraw2 = 4.0*N1*N2/(TMath::Power(N1+N2,3.0));  

   // asymmetry corrected for charge and livetime asymmetries
   Double_t Aexp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1.0 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t eAexp2   = (Power(-1.0 + ALT*ALT,2.0)*Power(-1.0 + AQ*AQ,2.0)*eAraw2)/Power(-1.0 + AQ*Araw + ALT*(-AQ + Araw),4.0);
   //Double_t den0   = (Araw-1.0)*L1*Q1-(Araw+1.0)*L2*Q2; 
   //Double_t eAexp2 = eAraw2*TMath::Power(4.0*L1*L2*Q1*Q2,2.0)/TMath::Power(den0,4.0);

   // asymmetry correcting for the dilution and polarizations
   Double_t Ameas   = Aexp/(df*Pb*Pt);
   Double_t eAmeas2 = eAexp2/TMath::Power(Pb*Pt*df,2.0);

   //Double_t error = TMath::Sqrt(( (0.0025 * TMath::Power(N1, 3) - 0.0025 * TMath::Power(N1, 2) * N2 - 0.0025 * N1 * TMath::Power(N2, 2) + 0.00250 * TMath::Power(N2, 3))
   //         * TMath::Power(fPb, 2) + (0.0025 * TMath::Power(N1, 3) - 0.0025 * TMath::Power(N1, 2) * N2 + 0.002500 * TMath::Power(N2, 3) +
   //            N1 * N2 * (-0.002500 * N2 + 4.*TMath::Power(fPb, 2))) *
   //         TMath::Power(fPt, 2)) / (TMath::Power(N1 + N2, 3) * TMath::Power(fPb, 4) * TMath::Power(fPt, 4))) ;

   Double_t error = TMath::Sqrt(eAmeas2);
   if( TMath::IsNaN(error) ) {
      Error("GetStatisticalError","Error is NaN");
      error = 0.0;
   }
   return(error);
}
//_____________________________________________________________________________

Double_t InSANEMeasuredAsymmetry::GetSystematicError(Double_t N1, Double_t N2, Double_t df) const
{
   // Systematic error propagation
   using namespace TMath;
   if(N1 == 0 || N2 == 0) {
      //Error("GetAsymmetry", "Zero count.");
      return(0.0);
   }
   if( (df == 0) ) {
      return(0.0);
   }
   if( (fPt == 0) || (fPb  == 0) ) {
      //Error("GetAsymmetry", "Zero count.");
      return(0.0);
   }
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // Beam
   Double_t Pb         = fPb;
   Double_t delta_Pb   = fdelta_Pb;

   // Target 
   Double_t Pt         = fPt;
   Double_t delta_Pt   = fdelta_Pt;

   // Dilution
   Double_t delta_df   = fdelta_df;

   // Raw count asymmetry
   Double_t Araw    = (N1-N2)/(N1+N2);

   // Charge asymmetry
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   Double_t delta_AQ =  fdelta_AQ;

   // Livetime asymmetry
   Double_t ALT     = (L1-L2)/(L1+L2);
   Double_t delta_ALT =  fdelta_ALT;

   // Asymmetry corrected for charge and livetime
   Double_t A_exp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t sig2_Aexp = (Power(-1.0 + Power(Araw,2.0),2.0)*(
                         Power(-1.0 + Power(AQ,  2.0),2.0)*Power(delta_ALT,2.0) + 
                         Power(-1.0 + Power(ALT, 2.0),2.0)*Power(delta_AQ, 2.0)))/
                         Power( 1.0 + ALT*AQ - (ALT + AQ)*Araw,4.0);


   // Asymmetry corrected for target dilution
   Double_t A_meas       = A_exp/(df*Pb*Pt);
   Double_t sig2_Ameas   = sig2_Aexp/(df*df*Pb*Pb*Pt*Pt) + 
                           (A_exp*A_exp*(df*df*delta_Pb*delta_Pb*Pt*Pt + 
                                              Pb*Pb*(Pt*Pt*delta_df*delta_df + 
                                                 df*df*delta_Pt*delta_Pt)))/
                           (Power(df,4.0)*Power(Pb,4.0)*Power(Pt,4.0));

   return( Sqrt(sig2_Ameas) );
}
//_____________________________________________________________________________
Double_t InSANEMeasuredAsymmetry::GetTotalError(Double_t N1, Double_t N2, Double_t df) const
{
   using namespace TMath;
   Double_t Pb = fPb;
   Double_t Pt = fPt;
   Double_t delDf = 0.01; ///\todo fix this
   Double_t delPb = 0.05; ///\todo fix this
   //Double_t delPt = 0.05;
   Double_t res = Sqrt(((4 * Power(df, 2) * N1 * N2 + Power(delDf, 2) * Power(N1 - N2, 2) * (N1 + N2)) * Power(Pb, 2) * Power(Pt, 2) +
            Power(delPb, 2) * Power(df, 2) * Power(N1 - N2, 2) * (N1 + N2) * (Power(Pb, 2) +
               Power(Pt, 2))) / (Power(df, 4) * Power(N1 + N2, 3) * Power(Pb, 4) * Power(Pt, 4)));
   return(res);
}
//_____________________________________________________________________________
Double_t InSANEMeasuredAsymmetry::GetRelativeSystUncert_Pb(Double_t N1, Double_t N2, Double_t df) const 
{
   // Systematic error propagation
   using namespace TMath;
   Double_t full_syst = GetSystematicError(N1,N2,df);
   full_syst *= full_syst ;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // Beam
   Double_t Pb         = fPb;
   Double_t delta_Pb   = fdelta_Pb;

   // Target 
   Double_t Pt         = fPt;
   Double_t delta_Pt   = 0.0;//fdelta_Pt;

   // Dilution
   Double_t delta_df   = 0.0;//fdelta_df;

   // Raw count asymmetry
   Double_t Araw    = (N1-N2)/(N1+N2);

   // Charge asymmetry
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   Double_t delta_AQ =  0.0;//fdelta_AQ;

   // Livetime asymmetry
   Double_t ALT     = (L1-L2)/(L1+L2);
   Double_t delta_ALT =  0.0;//fdelta_ALT;

   // Asymmetry corrected for charge and livetime
   Double_t A_exp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t sig2_Aexp = (Power(-1.0 + Power(Araw,2.0),2.0)*(
                         Power(-1.0 + Power(AQ,  2.0),2.0)*Power(delta_ALT,2.0) + 
                         Power(-1.0 + Power(ALT, 2.0),2.0)*Power(delta_AQ, 2.0)))/
                         Power( 1.0 + ALT*AQ - (ALT + AQ)*Araw,4.0);


   // Asymmetry corrected for target dilution
   Double_t A_meas       = A_exp/(df*Pb*Pt);
   Double_t sig2_Ameas   = sig2_Aexp/(df*df*Pb*Pb*Pt*Pt) + 
                           (A_exp*A_exp*(df*df*delta_Pb*delta_Pb*Pt*Pt + 
                                              Pb*Pb*(Pt*Pt*delta_df*delta_df + 
                                                 df*df*delta_Pt*delta_Pt)))/
                           (Power(df,4.0)*Power(Pb,4.0)*Power(Pt,4.0));
   double relative_uncertainty  = Abs(Sqrt(sig2_Ameas)/A_meas);
   //std::cout << " rel_sys_Pb = " << Abs(relative_uncertainty) << "\n";
   return( relative_uncertainty );
}
//______________________________________________________________________________
Double_t InSANEMeasuredAsymmetry::GetRelativeSystUncert_Pt(Double_t N1, Double_t N2, Double_t df) const
{
   // Systematic error propagation
   Double_t full_syst = GetSystematicError(N1,N2,df);
   full_syst *= full_syst ;
   using namespace TMath;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // Beam
   Double_t Pb         = fPb;
   Double_t delta_Pb   = 0.0;//fdelta_Pb;

   // Target 
   Double_t Pt         = fPt;
   Double_t delta_Pt   = fdelta_Pt;

   // Dilution
   Double_t delta_df   = 0.0;//fdelta_df;

   // Raw count asymmetry
   Double_t Araw    = (N1-N2)/(N1+N2);

   // Charge asymmetry
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   Double_t delta_AQ =  0.0;//fdelta_AQ;

   // Livetime asymmetry
   Double_t ALT     = (L1-L2)/(L1+L2);
   Double_t delta_ALT =  0.0;//fdelta_ALT;

   // Asymmetry corrected for charge and livetime
   Double_t A_exp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t sig2_Aexp = (Power(-1.0 + Power(Araw,2.0),2.0)*(
                         Power(-1.0 + Power(AQ,  2.0),2.0)*Power(delta_ALT,2.0) + 
                         Power(-1.0 + Power(ALT, 2.0),2.0)*Power(delta_AQ, 2.0)))/
                         Power( 1.0 + ALT*AQ - (ALT + AQ)*Araw,4.0);


   // Asymmetry corrected for target dilution
   Double_t A_meas       = A_exp/(df*Pb*Pt);
   Double_t sig2_Ameas   = sig2_Aexp/(df*df*Pb*Pb*Pt*Pt) + 
                           (A_exp*A_exp*(df*df*delta_Pb*delta_Pb*Pt*Pt + 
                                              Pb*Pb*(Pt*Pt*delta_df*delta_df + 
                                                 df*df*delta_Pt*delta_Pt)))/
                           (Power(df,4.0)*Power(Pb,4.0)*Power(Pt,4.0));
   double relative_uncertainty  = Abs(Sqrt(sig2_Ameas)/A_meas);
   //std::cout << " rel_sys_Pt = " << Abs(relative_uncertainty) << "\n";
   return( relative_uncertainty );
}
//______________________________________________________________________________

Double_t InSANEMeasuredAsymmetry::GetRelativeSystUncert_df(Double_t N1, Double_t N2, Double_t df) const
{
   // Systematic error propagation
   Double_t full_syst = GetSystematicError(N1,N2,df);
   full_syst *= full_syst ;
   using namespace TMath;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // Beam
   Double_t Pb         = fPb;
   Double_t delta_Pb   = 0.0;//fdelta_Pb;

   // Target 
   Double_t Pt         = fPt;
   Double_t delta_Pt   = 0.0;//fdelta_Pt;

   // Dilution
   Double_t delta_df   = fdelta_df;

   // Raw count asymmetry
   Double_t Araw    = (N1-N2)/(N1+N2);

   // Charge asymmetry
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   Double_t delta_AQ =  0.0;//fdelta_AQ;

   // Livetime asymmetry
   Double_t ALT     = (L1-L2)/(L1+L2);
   Double_t delta_ALT =  0.0;//fdelta_ALT;

   // Asymmetry corrected for charge and livetime
   Double_t A_exp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t sig2_Aexp = (Power(-1.0 + Power(Araw,2.0),2.0)*(
                         Power(-1.0 + Power(AQ,  2.0),2.0)*Power(delta_ALT,2.0) + 
                         Power(-1.0 + Power(ALT, 2.0),2.0)*Power(delta_AQ, 2.0)))/
                         Power( 1.0 + ALT*AQ - (ALT + AQ)*Araw,4.0);


   // Asymmetry corrected for target dilution
   Double_t A_meas       = A_exp/(df*Pb*Pt);
   Double_t sig2_Ameas   = sig2_Aexp/(df*df*Pb*Pb*Pt*Pt) + 
                           (A_exp*A_exp*(df*df*delta_Pb*delta_Pb*Pt*Pt + 
                                              Pb*Pb*(Pt*Pt*delta_df*delta_df + 
                                                 df*df*delta_Pt*delta_Pt)))/
                           (Power(df,4.0)*Power(Pb,4.0)*Power(Pt,4.0));
   double relative_uncertainty  = Abs(Sqrt(sig2_Ameas)/A_meas);
   //std::cout << " rel_sys_df = " << Abs(relative_uncertainty) << "\n";
   return( relative_uncertainty );
}
//______________________________________________________________________________

Double_t InSANEMeasuredAsymmetry::GetRelativeSystUncert_AQ(Double_t N1, Double_t N2, Double_t df) const
{
   // Systematic error propagation
   Double_t full_syst = GetSystematicError(N1,N2,df);
   full_syst *= full_syst ;
   using namespace TMath;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // Beam
   Double_t Pb         = fPb;
   Double_t delta_Pb   = 0.0;//fdelta_Pb;

   // Target 
   Double_t Pt         = fPt;
   Double_t delta_Pt   = 0.0;//fdelta_Pt;

   // Dilution
   Double_t delta_df   = 0.0;//fdelta_df;

   // Raw count asymmetry
   Double_t Araw    = (N1-N2)/(N1+N2);

   // Charge asymmetry
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   Double_t delta_AQ =  fdelta_AQ;

   // Livetime asymmetry
   Double_t ALT     = (L1-L2)/(L1+L2);
   Double_t delta_ALT =  0.0;//fdelta_ALT;

   // Asymmetry corrected for charge and livetime
   Double_t A_exp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t sig2_Aexp = (Power(-1.0 + Power(Araw,2.0),2.0)*(
                         Power(-1.0 + Power(AQ,  2.0),2.0)*Power(delta_ALT,2.0) + 
                         Power(-1.0 + Power(ALT, 2.0),2.0)*Power(delta_AQ, 2.0)))/
                         Power( 1.0 + ALT*AQ - (ALT + AQ)*Araw,4.0);


   // Asymmetry corrected for target dilution
   Double_t A_meas       = A_exp/(df*Pb*Pt);
   Double_t sig2_Ameas   = sig2_Aexp/(df*df*Pb*Pb*Pt*Pt) + 
                           (A_exp*A_exp*(df*df*delta_Pb*delta_Pb*Pt*Pt + 
                                              Pb*Pb*(Pt*Pt*delta_df*delta_df + 
                                                 df*df*delta_Pt*delta_Pt)))/
                           (Power(df,4.0)*Power(Pb,4.0)*Power(Pt,4.0));
   double relative_uncertainty  = Abs(Sqrt(sig2_Ameas)/A_meas);
   //std::cout << " rel_sys_AQ = " << Abs(relative_uncertainty) << "\n";
   return( relative_uncertainty );
}
//______________________________________________________________________________

Double_t InSANEMeasuredAsymmetry::GetRelativeSystUncert_ALT(Double_t N1, Double_t N2, Double_t df) const
{
   // Systematic error propagation
   Double_t full_syst = GetSystematicError(N1,N2,df);
   full_syst *= full_syst ;
   using namespace TMath;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // Beam
   Double_t Pb         = fPb;
   Double_t delta_Pb   = 0.0;//fdelta_Pb;

   // Target 
   Double_t Pt         = fPt;
   Double_t delta_Pt   = 0.0;//fdelta_Pt;

   // Dilution
   Double_t delta_df   = 0.0;//fdelta_df;

   // Raw count asymmetry
   Double_t Araw    = (N1-N2)/(N1+N2);

   // Charge asymmetry
   Double_t AQ      = (Q1-Q2)/(Q1+Q2);
   Double_t delta_AQ =  0.0;//fdelta_AQ;

   // Livetime asymmetry
   Double_t ALT     = (L1-L2)/(L1+L2);
   Double_t delta_ALT =  fdelta_ALT;

   // Asymmetry corrected for charge and livetime
   Double_t A_exp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t sig2_Aexp = (Power(-1.0 + Power(Araw,2.0),2.0)*(
                         Power(-1.0 + Power(AQ,  2.0),2.0)*Power(delta_ALT,2.0) + 
                         Power(-1.0 + Power(ALT, 2.0),2.0)*Power(delta_AQ, 2.0)))/
                         Power( 1.0 + ALT*AQ - (ALT + AQ)*Araw,4.0);


   // Asymmetry corrected for target dilution
   Double_t A_meas       = A_exp/(df*Pb*Pt);
   Double_t sig2_Ameas   = sig2_Aexp/(df*df*Pb*Pb*Pt*Pt) + 
                           (A_exp*A_exp*(df*df*delta_Pb*delta_Pb*Pt*Pt + 
                                              Pb*Pb*(Pt*Pt*delta_df*delta_df + 
                                                 df*df*delta_Pt*delta_Pt)))/
                           (Power(df,4.0)*Power(Pb,4.0)*Power(Pt,4.0));
   double relative_uncertainty  = Abs(Sqrt(sig2_Ameas)/A_meas);
   //std::cout << " rel_sys_ALT = " << Abs(relative_uncertainty) << "\n";
   return( relative_uncertainty );
}
//______________________________________________________________________________

Double_t InSANEMeasuredAsymmetry::GetRelativeSystUncert_tot(Double_t N1, Double_t N2, Double_t df) const
{
   // Systematic error propagation
   Double_t full_syst = GetSystematicError(N1,N2,df);
   full_syst *= full_syst ;
   using namespace TMath;
   Double_t L1 = fLiveTime[0];
   Double_t L2 = fLiveTime[1];
   Double_t Q1 = fCharge[0];
   Double_t Q2 = fCharge[1];

   // Beam
   Double_t Pb         = fPb;
   Double_t delta_Pb   = fdelta_Pb;

   // Target 
   Double_t Pt         = fPt;
   Double_t delta_Pt   = fdelta_Pt;

   // Dilution
   Double_t delta_df   = fdelta_df;

   // Raw count asymmetry
   Double_t Araw    = (N1-N2)/(N1+N2);

   // Charge asymmetry
   Double_t AQ       = (Q1-Q2)/(Q1+Q2);
   Double_t delta_AQ =  fdelta_AQ;

   // Livetime asymmetry
   Double_t ALT     = (L1-L2)/(L1+L2);
   Double_t delta_ALT =  fdelta_ALT;

   // Asymmetry corrected for charge and livetime
   Double_t A_exp     = (ALT + AQ - Araw - ALT*AQ*Araw)/(-1 + AQ*Araw + ALT*(-AQ + Araw));
   Double_t sig2_Aexp = (Power(-1.0 + Power(Araw,2.0),2.0)*(
                         Power(-1.0 + Power(AQ,  2.0),2.0)*Power(delta_ALT,2.0) + 
                         Power(-1.0 + Power(ALT, 2.0),2.0)*Power(delta_AQ, 2.0)))/
                         Power( 1.0 + ALT*AQ - (ALT + AQ)*Araw,4.0);


   // Asymmetry corrected for target dilution
   Double_t A_meas       = A_exp/(df*Pb*Pt);
   Double_t sig2_Ameas   = sig2_Aexp/(df*df*Pb*Pb*Pt*Pt) + 
                           (A_exp*A_exp*(df*df*delta_Pb*delta_Pb*Pt*Pt + 
                                              Pb*Pb*(Pt*Pt*delta_df*delta_df + 
                                                 df*df*delta_Pt*delta_Pt)))/
                           (Power(df,4.0)*Power(Pb,4.0)*Power(Pt,4.0));
   double relative_uncertainty  = Abs(Sqrt(sig2_Ameas)/A_meas);
   //std::cout << " rel_sys_tot = " << Abs(relative_uncertainty) << "\n";
   return( relative_uncertainty );
}
//______________________________________________________________________________
void           InSANEMeasuredAsymmetry::Browse(TBrowser* b)
{
   b->Add(&fAsymmetryVsx     , "Asym Vs x") ;
   b->Add(&fAsymmetryVsW     , "Asym Vs W") ;
   //b->Add(&fAsymmetryVsNu    , "Asym Vs nu") ;
   //b->Add(&fAsymmetryVsE     , "Asym Vs Eprime") ;
   //b->Add(&fAsymmetryVsTheta , "Asym Vs theta") ;
   //b->Add(&fAsymmetryVsPhi   , "Asym Vs phi") ;

   b->Add(&fAvgKineVsx      , "kine vs x") ;
   b->Add(&fAvgKineVsW      , "kine vs W") ;
   //b->Add(&fAvgKineVsNu     , "kine vs nu") ;
   //b->Add(&fAvgKineVsE      , "kine vs E") ;
   //b->Add(&fAvgKineVsTheta  , "kine vs theta") ;
   //b->Add(&fAvgKineVsPhi    , "kine vs phi") ;

   b->Add(&fDilutionVsx     ) ;
   b->Add(&fDilutionVsW     ) ;
   //b->Add(&fDilutionVsNu    ) ;
   //b->Add(&fDilutionVsE     ) ;
   //b->Add(&fDilutionVsTheta ) ;
   //b->Add(&fDilutionVsPhi   ) ;

   b->Add(&fSystErrVsx      , "systematics vs x") ;
   b->Add(&fSystErrVsW      , "systematics vs W") ;
   //b->Add(&fSystErrVsNu     , "systematics vs nu") ;
   //b->Add(&fSystErrVsE      , "systematics vs E") ;
   //b->Add(&fSystErrVsTheta  , "systematics vs theta") ;
   //b->Add(&fSystErrVsPhi    , "systematics vs phi") ;

   auto * list = new TList();
   for(auto syst : fSystematics_x ) {
      list->Add(new InSANESystematic(syst));
   }
   b->Add(list     );

   b->Add(&fMask_x , "x binned mask"   );
   b->Add(&fMask_W , "W binned mask"   );
   //b->Add(&fSystematics_Nu    );
   //b->Add(&fSystematics_E     );
   //b->Add(&fSystematics_Theta );
   //b->Add(&fSystematics_Phi   );

}
//______________________________________________________________________________

