#include "SANETrajectoryCalculations.h"

ClassImp(SANETrajectoryCalculation1)

//______________________________________________________________________________
SANETrajectoryCalculation1::SANETrajectoryCalculation1(InSANEAnalysis * analysis) 
   : BETACalculationWithClusters(analysis)  {
   SetNameTitle("SANETrajectoryCalculation1", "SANETrajectoryCalculation1");
   fReconEvent = new InSANEReconstructedEvent();
}
//______________________________________________________________________________
SANETrajectoryCalculation1::~SANETrajectoryCalculation1() {
   if (fOutTree)fOutTree->FlushBaskets();
   if (fOutTree)fOutTree->Write();
}
//______________________________________________________________________________
void SANETrajectoryCalculation1::Describe() {
   std::cout <<  "===============================================================================" << std::endl;
   std::cout <<  "  SANETrajectoryCalculation1 - " << std::endl;
   std::cout <<  "     Loops over clusters,lucite hodoscope hits and fills a new tree called betaTrajectories." << std::endl;
   std::cout <<  "     Creates new TClonesArray branches : " << std::endl;
   std::cout <<  "               (fTrajectories -> InSANETrajectory),(fPositions -> InSANEHitPosition) " << std::endl;
   std::cout <<  "               (fBigcalClusters -> BIGCALCluster),(fLucitePosHits -> LuciteHodoscopePositionHit) " << std::endl;
   std::cout <<  "     where the last two branches are for the TRef and TRefArrays data members of the first two. " << std::endl;
   std::cout <<  "_______________________________________________________________________________" << std::endl;
}
//______________________________________________________________________________
Int_t SANETrajectoryCalculation1::Initialize() {

   BETACalculationWithClusters::Initialize();

   InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
   fOutTree = new TTree("betaTrajectories1", "BETA Trajectories - SANETrajectoryCalculation1");
   fOutTree->Branch("fReconEvent", "InSANEReconstructedEvent", &fReconEvent);
   //fOutTree->BranchRef();
   return(0);
}
//______________________________________________________________________________
Int_t SANETrajectoryCalculation1::Calculate() {

   if (!(fEvents->TRIG->IsClusterable()))  return(0);

   fReconEvent->ClearEvent();

   fReconEvent->fEventNumber = fEvents->BETA->fEventNumber;
   fReconEvent->fRunNumber   = fEvents->BETA->fRunNumber;

return(0);
}
//______________________________________________________________________________



ClassImp(SANETrajectoryCalculation2)

//______________________________________________________________________________
SANETrajectoryCalculation2::SANETrajectoryCalculation2(InSANEAnalysis * analysis)
   : BETACalculationWithClusters(analysis)  {
   SetNameTitle("SANETrajectoryCalculation2", "SANETrajectoryCalculation2");
   fReconEvent = new InSANEReconstructedEvent();
}
//______________________________________________________________________________
SANETrajectoryCalculation2::~SANETrajectoryCalculation2() {
   if (fOutTree)fOutTree->FlushBaskets();
   if (fOutTree)fOutTree->Write();
}
//______________________________________________________________________________
void SANETrajectoryCalculation2::Describe() {
   std::cout <<  "===============================================================================" << std::endl;
   std::cout <<  "  SANETrajectoryCalculation2 - " << std::endl;
   std::cout <<  "      Uses lucite PositionHits to seed trajctories then constructed using ." << std::endl;
   std::cout <<  "      the Forward tracker alone" << std::endl;
   std::cout <<  "_______________________________________________________________________________" << std::endl;
}
//______________________________________________________________________________
Int_t SANETrajectoryCalculation2::Initialize() {
   BETACalculationWithClusters::Initialize();

   InSANERunManager::GetRunManager()->GetCurrentFile()->cd();

   fOutTree = new TTree("trackingDetectors", "BETA Tracking Detectors - No Shower");
   fOutTree->Branch("trackingReconEvent", "InSANEReconstructedEvent", &fReconEvent);
   fOutTree->BranchRef();

   return(0);
}
//______________________________________________________________________________
Int_t SANETrajectoryCalculation2::Calculate() {

   if (!(fEvents->TRIG->IsClusterable()))  return(0);

   fReconEvent->ClearEvent();
   fReconEvent->fEventNumber = fEvents->BETA->fEventNumber;
   fReconEvent->fRunNumber   = fEvents->BETA->fRunNumber;

   return(0);
}
//______________________________________________________________________________



ClassImp(SANETrajectoryCalculation3)

//______________________________________________________________________________
SANETrajectoryCalculation3::SANETrajectoryCalculation3(InSANEAnalysis * analysis)
   : BETACalculationWithClusters(analysis)  {
   SetNameTitle("SANETrajectoryCalculation3", "SANETrajectoryCalculation3");
   fReconEvent     = new InSANEReconstructedEvent();
   aTrajectory     = nullptr;
   aLucPositionHit = nullptr;
   aCluster        = nullptr;
   aTrackerPosHit  = nullptr;
   aPos            = nullptr;
   bPos            = nullptr;
   tPos            = nullptr;
   missDist        = 0.0;
   fMaxLuciteMiss  = 20.0; //cm
   fMaxTrackerMiss = 5.0;  //cm
}
//______________________________________________________________________________
SANETrajectoryCalculation3::~SANETrajectoryCalculation3() {
   if (fOutTree)fOutTree->FlushBaskets();
   if (fOutTree)fOutTree->Write();
}
//______________________________________________________________________________
void SANETrajectoryCalculation3::Describe() {
   std::cout <<  "===============================================================================" << std::endl;
   std::cout <<  "  SANETrajectoryCalculation3 - " << std::endl;
   std::cout <<  "      Same thing as SANETrajectoryCalculation2 but it does not seed any "   << std::endl;
   std::cout <<  "      positions for comparison. It just calculates the positions for "      << std::endl;
   std::cout <<  "      the Lucite Hodoscope and Tracker independent of any other detector. " << std::endl;
   std::cout <<  "      Places Y position correlation cut between tracker and hodoscope."     << std::endl;
   std::cout <<  "_______________________________________________________________________________" << std::endl;
}
//______________________________________________________________________________
Int_t SANETrajectoryCalculation3::Initialize() {
   BETACalculationWithClusters::Initialize();

   InSANERunManager::GetRunManager()->GetCurrentFile()->cd();

   fOutTree = new TTree("trackingPositions", "BETA Tracking Detectors - No Shower");
   fOutTree->Branch("uncorrelatedPositions", "InSANEReconstructedEvent", &fReconEvent);

   return(0);
}
//______________________________________________________________________________
Int_t SANETrajectoryCalculation3::Calculate() {

   if (!(fEvents->TRIG->IsClusterable()))  return(0);

   fReconEvent->ClearEvent();
   fReconEvent->fEventNumber = fEvents->BETA->fEventNumber;
   fReconEvent->fRunNumber   = fEvents->BETA->fRunNumber;

   // -------------------------------------------------------
   // Set the target position from the raster adc position
   TVector3 & fTargetPos = fEvents->BEAM->fRasterPosition;  // used later
   tPos                  = fReconEvent->AddTargetPosition();
   tPos->fPosition       = fTargetPos;

   //std::cout << fReconEvent << " recon event addr" << std::endl;
   // -------------------------------------------------------
   // Begin loop over clusters
   for (int kk = 0; kk < fClusterEvent->fClusters->GetEntries() ; kk++) {
      aCluster = (BIGCALCluster*)(*fClusters)[kk];

      /// \todo apply cluster selection for clean electrons.
      if (1) { //if(aCluster->fIsGood) 

         // Create a new trajectory for this cluster
         aTrajectory                 = fReconEvent->AddTrajectory();
         aTrajectory->fRunNumber     = fReconEvent->fRunNumber;
         aTrajectory->fEventNumber   = fReconEvent->fEventNumber;
         aTrajectory->fClusterNumber = aCluster->fClusterNumber;
         // Set all trajectory's cluster related information.
         aTrajectory->fPosition0     = fBigcalDetector->GetPosition(aCluster);
         fBigcalPos                  = aTrajectory->fPosition0;
         aTrajectory->fMomentum.SetVect(aCluster->GetMomentumVector());
         aTrajectory->fMomentum.SetE(aCluster->GetEnergy() + aCluster->fDeltaE);

         // Set the Cherenkov signal
         aTrajectory->fNCherenkovElectrons = aCluster->fCherenkovBestADCSum;
         //if(bCluster->fGoodCherenkovTDCHit) aTrajectory->fNCherenkovElectrons = 1;
         //else aTrajectory->fNCherenkovElectrons = 0;

         fReconEvent->fClosestLuciteMissDistance = 4000.0;
         fReconEvent->fClosestLuciteHitNumber  = 0;

         // --------------------------------------------------
         // Now we get the position hits from the 
         // Lucite Hodoscope and Forward Tracker
         // --------------------------------------------------

         // --------------------------------------------------------------------
         // Loop over lucite position hits
         if (lhEvent->fLucitePositionHits) {
            for (int li = 0; li < lhEvent->fLucitePositionHits->GetEntries(); li++) {
               aLucPositionHit = (LuciteHodoscopePositionHit*)(*lhEvent->fLucitePositionHits)[li];

               /// \todo fix this hard coding
               if(TMath::Abs(aLucPositionHit->fTDCSum) < 100) {

                  // Get the lucite position from the "position hit"
                  fLucPos  =  fHodoscopeDetector->GetPosition(aLucPositionHit) ;
                  // Get the straight-line projected position in lucite from bigcal cluster.
                  fSeedPos =  fHodoscopeDetector->GetSeedingHitPosition(fBigcalPos) ;

                  /// Calculate the "miss" from the seeding (0 because it is seeding for now)
                  Double_t missDist =  fHodoscopeDetector->GetMissDistance(fLucPos, fSeedPos);
                  if (missDist < fReconEvent->fClosestLuciteMissDistance) {
                     fReconEvent->fClosestLuciteMissDistance = missDist;
                     fReconEvent->fClosestLuciteHitNumber    = li;
                  }

                  /// Correlate tracker an lucite hits if "miss distance" is not too large
                  //     if( missDist <  50.0 /*fHodoscopeDetector->cSeedHitMissDistance*/ )

                  /// Create InSANEHitPosition
                  aPos = fReconEvent->AddLucitePosition();
                  //        bLucPositionHit = fReconEvent->AddLuciteHit( aLucPositionHit );

                  /// Add hit position to the TRefArray
                  //aTrajectory->fHitPositions.Add(aPos);

                  // //        std::cout << " new position added " << std::endl;
                  // //        lucPos.Print();
                  // //        seedPos.Print();
                  // //        std::cout << " missDist = " << missDist << "" << std::endl;
                  //aPos->fPositionUncertainty.SetXYZ( 0, 0,0);
                  aPos->fPositionUncertainty = (fLucPos - fSeedPos);
                  aPos->fPosition            = fLucPos;
                  aPos->fHitNumber           = aLucPositionHit->fHitNumber;
                  aPos->fTiming              = aLucPositionHit->fTDCSum;
                  aPos->fClusterNumber       = aCluster->fClusterNumber;

                  aPos->fMissDistance        = missDist;
                  aPos->fSeedingPosition     = fSeedPos;
                  //aPos->fCluster         = bCluster;
                  //aPos->fHit             = bLucPositionHit;
                  //aTrajectory->fNPositions++;
                  //if(missDist < fHodoscopeDetector->cSeedHitMissDistance  ){
                  //   fReconEvent->fGoodPositions.Add(aPos);
                  //   fReconEvent->fNGoodPositions++;
                  //}
               }
            }  // end loop over lucite position hits
         }     // lucite events check

         // ------------------------------------------------------
         /// Loop over tracker double Y-plane position hits
         for (int li = 0; li < ftEvent->fTracker2PositionHits->GetEntries(); li++) {
            aTrackerPosHit = (ForwardTrackerPositionHit*)(*ftEvent->fTracker2PositionHits)[li];

            // Get the tracker position from the "position hit"
            fTrackerPos               = fTrackerDetector->GetPosition(aTrackerPosHit);
            // Uses the bigcal hit
            fTrackerSeed              = fTrackerDetector->GetSeedingHitPosition(fBigcalPos - fTargetPos);
            Double_t trackerMissDist  = fTrackerDetector->GetMissDistance(fTrackerPos, fTrackerSeed);
            // Add the hit and position hit to the arrays
            bPos                 = fReconEvent->AddTracker2Position();
            bPos->fHitNumber     = aTrackerPosHit->fHitNumber;
            bPos->fTiming        = aTrackerPosHit->fTDCSum;
            bPos->fClusterNumber = aCluster->fClusterNumber;
            bPos->fNNeighbors    = aTrackerPosHit->fNNeighborHits;
            bPos->fPositionUncertainty = fTrackerPos - fTrackerSeed;
            bPos->fPosition        = fTrackerPos;
            bPos->fMissDistance    = trackerMissDist;
            bPos->fSeedingPosition = fTrackerSeed;
         }

         // ------------------------------------------------------
         /// Loop over tracker Y2 position hits
         for (int li = 0; li < ftEvent->fTrackerY2PositionHits->GetEntries(); li++) {
            aTrackerPosHit = (ForwardTrackerPositionHit*)(*ftEvent->fTrackerY2PositionHits)[li];
            // Get the tracker position from the "position hit"
            fTrackerPos               = fTrackerDetector->GetPosition(aTrackerPosHit);
            // Uses the bigcal hit
            fTrackerSeed              = fTrackerDetector->GetSeedingHitPosition(fBigcalPos - fTargetPos,2);
            Double_t trackerMissDist  = fTrackerDetector->GetMissDistance(fTrackerPos, fTrackerSeed);
            // Add the hit and position hit to the arrays
            bPos                 = fReconEvent->AddTrackerY2Position();
            bPos->fHitNumber     = aTrackerPosHit->fHitNumber;
            bPos->fTiming        = aTrackerPosHit->fTDCSum;
            bPos->fClusterNumber = aCluster->fClusterNumber;
            bPos->fNNeighbors    = aTrackerPosHit->fNNeighborHits;
            bPos->fPositionUncertainty = fTrackerPos - fTrackerSeed;
            bPos->fPosition        = fTrackerPos;
            bPos->fMissDistance    = trackerMissDist;
            bPos->fSeedingPosition = fTrackerSeed;
            // Add to array of all hit positions
            aPos                   = fReconEvent->AddTrackerPosition();
            (*aPos)                = (*bPos);
         }

         // ------------------------------------------------------
         /// Loop over tracker Y1 position hits
         for (int li = 0; li < ftEvent->fTrackerY1PositionHits->GetEntries(); li++) {
            aTrackerPosHit = (ForwardTrackerPositionHit*)(*ftEvent->fTrackerY1PositionHits)[li];
            // Get the tracker position from the "position hit"
            fTrackerPos               = fTrackerDetector->GetPosition(aTrackerPosHit);
            // Uses the bigcal hit
            fTrackerSeed              = fTrackerDetector->GetSeedingHitPosition(fBigcalPos - fTargetPos,1);
            Double_t trackerMissDist = fTrackerDetector->GetMissDistance(fTrackerPos, fTrackerSeed);
            // Add the hit and position hit to the arrays
            bPos                 = fReconEvent->AddTrackerY1Position();
            bPos->fHitNumber     = aTrackerPosHit->fHitNumber;
            bPos->fTiming        = aTrackerPosHit->fTDCSum;
            bPos->fClusterNumber = aCluster->fClusterNumber;
            bPos->fNNeighbors    = aTrackerPosHit->fNNeighborHits;
            bPos->fPositionUncertainty = fTrackerPos - fTrackerSeed;
            bPos->fPosition        = fTrackerPos;
            bPos->fMissDistance    = trackerMissDist;
            bPos->fSeedingPosition = fTrackerSeed;
            aPos                   = fReconEvent->AddTrackerPosition();
            (*aPos)                = (*bPos);
         }

      }
   } // Loop over clusters


   return(0);
}
//______________________________________________________________________________



ClassImp(SANETrajectoryCalculation4)

//______________________________________________________________________________
Int_t SANETrajectoryCalculation4::Calculate()
{

   if (!(fEvents->TRIG->IsClusterable()))  return(0);

   fReconEvent->ClearEvent();

   fReconEvent->fEventNumber = fEvents->BETA->fEventNumber;
   fReconEvent->fRunNumber   = fEvents->BETA->fRunNumber;

   //InSANETrajectory * aTrajectory = 0;
   //LuciteHodoscopePositionHit * aLucPositionHit = 0;
   //LuciteHodoscopePositionHit * bLucPositionHit = 0;
   //BIGCALCluster * aCluster = 0;
   //BIGCALCluster * bCluster = 0;
   //ForwardTrackerPositionHit * aTrackerPosHit = 0;
   //ForwardTrackerPositionHit * bTrackerPosHit = 0;
   //InSANEHitPosition * aPos = 0;
/*
   /// Begin loop over clusters
   for (int kk = 0; kk < fClusterEvent->fClusters->GetEntries() ; kk++) {
      aCluster = (BIGCALCluster*)(*fClusters)[kk];
      if (aCluster->fIsGood) {

         /// Now we get the position hits
         /// Loop over lucite position hits
         if (lhEvent->fLucitePositionHits)
            for (int li = 0; li < lhEvent->fLucitePositionHits->GetEntries(); li++) {
               aLucPositionHit = (LuciteHodoscopePositionHit*)(*lhEvent->fLucitePositionHits)[li];

               /// Basic TDCDiff cut
               if (TMath::Abs(aLucPositionHit->fTDCDifference - 90) < 250) {

                  /// Get the lucite position from the "position hit"
                  TVector3    lucPos(fHodoscopeDetector->GetPosition(aLucPositionHit));
                  /// Get the straight-line projected position in lucite from bigcal cluster.
                  TVector3   seedPos(fBigcalDetector->GetSeedingHitPosition(lucPos));

                  /// Add a copy of the cluster to new tree
                  bCluster    =  aClusterfReconEvent->AddCluster(aCluster);

                  /// Assume cluster is an electron and calculate the NN corrections
                  fANNEvent->SetEventValues(bCluster);
                  fANNEvent->GetCorrectionMLPInputNeuronArray(electronInputs);
                  bCluster->fDeltaE     = fElectronNNEnergy.Value(0, electronInputs);
                  bCluster->fDeltaTheta = fElectronNNTheta.Value(0, electronInputs);
                  bCluster->fDeltaPhi   = fElectronNNPhi.Value(0, electronInputs);

                  /// Create a new trajectory for this cluster
                  aTrajectory = fReconEvent->AddTrajectory();
                  aTrajectory->fRunNumber   = fReconEvent->fRunNumber;
                  aTrajectory->fEventNumber = fReconEvent->fEventNumber;

                  /// Set all trajectory's cluster related information.
                  aTrajectory->fPosition0 = lucPos;//fBigcalDetector->GetPosition(bCluster);
                  aTrajectory->fMomentum.SetVect(lucPos);//bCluster->GetMomentumVector());
                  aTrajectory->fMomentum.SetE(bCluster->GetEnergy() + bCluster->fDeltaE);

                  TVector3 bigcalPos = fBigcalDetector->GetPosition(bCluster);

                  Double_t missDist =  fBigcalDetector->GetMissDistance(bigcalPos, seedPos);

//     TVector3 mom3(aTrajectory->fPosition0);
//     mom3.SetMag(bCluster->GetEnergy());
//     aTrajectory->fMomentum.SetVect(mom3);
//          aTrajectory->fMomentum.SetE(bCluster->GetEnergy()); /// \todo when to use ANN?

                  /// Set the Cherenkov signal
                  aTrajectory->fNCherenkovElectrons = bCluster->fCherenkovBestADCSum;
//     if(bCluster->fGoodCherenkovTDCHit) aTrajectory->fNCherenkovElectrons = 1;
//     else aTrajectory->fNCherenkovElectrons = 0;
//     if(bCluster->fCherenkovBestNPESum > 20) aTrajectory->fNCherenkovElectrons = 2;

//        if( missDist <  50.0  ){ //fHodoscopeDetector->cSeedHitMissDistance

                  aPos = fReconEvent->AddLucitePosition();
                  //new( (*fReconEvent->fLucitePositions)[fReconEvent->fNLucitePositions])InSANEHitPosition();
                  bLucPositionHit = fReconEvent->AddLuciteHit(aLucPositionHit);
                  //new( (*fReconEvent->fLuciteHitPos)[fReconEvent->fNLuciteHitPos])LuciteHodoscopePositionHit(*aLucPositionHit);

                  /// Add hit position to the TRefArray
                  aTrajectory->fHitPositions.Add(aPos);

//        std::cout << " new position added " << std::endl;
//        lucPos.Print();
//        seedPos.Print();
//        std::cout << " missDist = " << missDist << "" << std::endl;
                  aPos->fPositionUncertainty.SetXYZ(lucPos.X() - bigcalPos.X(),
                                                    lucPos.Y() - bigcalPos.Y(),
                                                    lucPos.Z() - bigcalPos.Z());
                  aPos->fPosition        = lucPos;
                  aPos->fMissDistance    = missDist;
                  aPos->fSeedingPosition = seedPos;
                  aPos->fCluster         = bCluster;
                  aPos->fHit             = bLucPositionHit;
                  aTrajectory->fNPositions++;

//           if(missDist < fHodoscopeDetector->cSeedHitMissDistance  ){
//              fReconEvent->fGoodPositions.Add(aPos);
//         fReconEvent->fNGoodPositions++;
//           }
               } // Basic lucite TDCDiff cut
            }  // end loop over lucite position hits

         /// Loop over tracker position hits
//          if(ftEvent->fTrackerPositionHits)
//          for(int li=0; li< ftEvent->fTrackerPositionHits->GetEntries();li++) {
//             aTrackerPosHit = (ForwardTrackerPositionHit*)(*ftEvent->fTrackerPositionHits)[li];
//
//             /// Get the tracker position from the "position hit"
//        TVector3    trackerPos( fTrackerDetector->GetPosition(aTrackerPosHit) );
//
//             /// Get the projected position form a straight line to the plane of the tracker
//        TVector3   trackerSeed( fTrackerDetector->GetSeedingHitPosition(aTrajectory->fPosition0));
//
//        Double_t trackerMissDist = fTrackerDetector->GetMissDistance(trackerPos,trackerSeed);
//        if( trackerMissDist < 20.0*0.30 ) {
//                aPos = fReconEvent->AddTrackerPosition();
//           bTrackerPosHit = fReconEvent->AddTrackerHit(aTrackerPosHit);
//                aPos->fPositionUncertainty.SetXYZ(trackerPos.X()-trackerSeed.X(),trackerPos.Y()-trackerSeed.Y(),trackerPos.Z()-trackerSeed.Z());
//           aPos->fPosition = trackerPos;
//           aPos->fMissDistance = trackerMissDist;
//           aPos->fSeedingPosition = trackerSeed;
//           aPos->fCluster = bCluster;
//           aPos->fHit = bTrackerPosHit;
//           aTrajectory->fNPositions++;
//
//        }
//     }// tracker loop
      } // bigcal cluster cut
   }  // end bigcal cluster loops
 */

   return(0);
}
