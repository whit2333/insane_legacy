#include "BETAPedestalCorrection.h"
#include "InSANECorrection.h"
#include "BigcalHit.h"

ClassImp(BETAPedestalCorrection)

//______________________________________________________________________________
void BETAPedestalCorrection::DefineCorrection() {
   ;
}
//______________________________________________________________________________
Int_t BETAPedestalCorrection::Apply() {

   if (!(fEvents->TRIG->IsPedestalCorrectable())) return(0);

   /// Begin loop over cherenkov signals
   for (int kk = 0; kk < aBetaEvent->fGasCherenkovEvent->fGasCherenkovHits->GetEntries(); kk++) {
      aCerHit = (GasCherenkovHit*)(*fCerHits)[kk] ;

      /// Subtract the pedestal
      aCerHit->fADC =
         aCerHit->fADC - int(fCherenkovDetector->GetPedestal(aCerHit->fMirrorNumber - 1));
      aCerHit->fADCAlign =
         ((Double_t)aCerHit->fADC) / ((Double_t) fCherenkovDetector->fCherenkovAlignmentCoef[aCerHit->fMirrorNumber-1]);
   } // End loop over Cherenkov signals

   /// Begin loop over cherenkov TDC HITS
   for (int kk = 0; kk < aBetaEvent->fGasCherenkovEvent->fGasCherenkovTDCHits->GetEntries(); kk++) {
      aCerHit = (GasCherenkovHit*)(*fCerHits)[kk] ;
      // Subtract the pedestal
      aCerHit->fADC =
         aCerHit->fADC - int(fCherenkovDetector->GetPedestal(aCerHit->fMirrorNumber - 1));
      aCerHit->fADCAlign =
         ((Double_t)aCerHit->fADC) / ((Double_t) fCherenkovDetector->fCherenkovAlignmentCoef[aCerHit->fMirrorNumber-1]);
   } // End loop over Cherenkov signals


   // Begin loop over Hodoscope signals
   //     for (int kk=0; kk< aBetaEvent->fLuciteHodoscopeEvent->fNumberOfHits;kk++)
   //     {
   //       aLucHit = (LuciteHodoscopeHit*)(*fLucHits)[kk] ;
   //
   //     } // End loop over Hodoscope signals

   // Begin loop over Bigcal signals
   Int_t aBlockNum = 0;
   for (int kk = 0; kk < aBetaEvent->fBigcalEvent->fBigcalHits->GetEntries(); kk++) {
      aBigcalHit = (BigcalHit*)(*fBigcalHits)[kk] ;
      if (aBigcalHit->fLevel == 0) {
         aBlockNum = fBigcalDetector->fGeoCalc->GetBlockNumber(aBigcalHit->fiCell, aBigcalHit->fjCell);
         aBigcalHit->fBlockNumber = aBlockNum;
         aBigcalHit->fADC = aBigcalHit->fADC - int(fBigcalDetector->GetPedestal(aBlockNum - 1));
         // Energy is just recalculated
         aBigcalHit->fEnergy = double(aBigcalHit->fADC)*fBigcalDetector->fGeoCalc->GetCalibrationCoefficient(aBigcalHit->fiCell, aBigcalHit->fjCell);

      }
      //aBigcalHit->Dump();
   }

   for (int kk = 0; kk < fBigcalTimingGroupHits->GetEntries(); kk++) {
      aBigcalHit = (BigcalHit*)(*fBigcalTimingGroupHits)[kk] ;
      if (aBigcalHit->fLevel == 1) {
         //double adcbefore = (double)(aBigcalHit->fADC);
         aBigcalHit->fADC = aBigcalHit->fADC - int(fBigcalDetector->GetDetectorTimingGroupPedestal(aBigcalHit->fChannel-1)->fPedestalMean);
         /// Energy is just recalculated as : E = (E_old/adc_old)*adc_new where
         /// C = E_old/adc_old is an effective calibration coefficent used
         //aBigcalHit->fEnergy = (aBigCalHit->fEnergy/adcbefore) * aBigcalHit->fADC ;
      }
   }

   //std::cout << "APPLYING BETAPedestalCorrection \n";

   return(0);
}
//______________________________________________________________________________


