#include "InSANEAnalysisManager.h"
#include "TStopwatch.h"
#include <iostream>

const int MAX_CHARS_PER_LINE = 512;
const int MAX_TOKENS_PER_LINE = 20;
const char* const DELIMITER = " ";
#include <iostream>
using std::cout;
using std::endl;
#include <fstream>
using std::ifstream;
#include <cstring>


//______________________________________________________________________________
ClassImp(InSANEAnalysisManager)

//______________________________________________________________________________
InSANEAnalysisManager * InSANEAnalysisManager::fgInSANEAnalysisManager = nullptr;

//______________________________________________________________________________
InSANEAnalysisManager::InSANEAnalysisManager()
{

   fRunQueue = new std::vector<int>;
   fAxe = nullptr;
   fSaveDetectors = true;
   fCutsFile = nullptr;

//  if(  LoadCuts("cuts/StandardCuts.root","StandardCuts") ) Error("InSANEAnalysisManager()","Failed to open Standard Cuts!");

}

InSANEAnalysisManager::~InSANEAnalysisManager()
{

}
//_______________________________________________________//

// InSANEAnalysisManager * InSANEAnalysisManager::GetAnalysisManager() {
//   if(!fgInSANEAnalysisManager) fgInSANEAnalysisManager = new InSANEAnalysisManager();
//   return(fgInSANEAnalysisManager);
// }
//____________________________________________________________//

Int_t InSANEAnalysisManager::RunHallCReplay()
{
   gROOT->ProcessLine(Form(".! ./bin/sane_replay -r %d -n 500000 >> /dev/null ", fRunQueue->back()));
   return(0); ///\todo need figure out how to handle errors from command line
}
//_____________________________________________________________________________

Int_t InSANEAnalysisManager::BuildQueue2(const char * fName) {
   // Modified from http://cs.dvc.edu/HowTo_Cparse.html
   // create a file-reading object
   Int_t nruns = 0;
   ifstream fin;
   fin.open(fName); // open a file
   if (!fin.good()) 
    return -1; // exit if file not found
   // read each line of the file
   while (!fin.eof())
   {
      // read an entire line into memory
      char buf[MAX_CHARS_PER_LINE];
      fin.getline(buf, MAX_CHARS_PER_LINE);
    
       // parse the line into blank-delimited tokens
       int n = 0; // a for-loop index
    
       // array to store memory addresses of the tokens in buf
       const char* token[MAX_TOKENS_PER_LINE] = {nullptr}; // initialize to 0
    
       // parse the line
       token[0] = strtok(buf, DELIMITER); // first token
       if (token[0]) // zero if line is blank
       {
         for (n = 1; n < MAX_TOKENS_PER_LINE; n++)
         {
            token[n] = strtok(nullptr, DELIMITER); // subsequent tokens
            if (!token[n]) break; // no more tokens
         }
       }

      // process the tokens
      // The first one should be a number that is positive
      if(n>0)
         if( atoi(token[0]) > 0 ) {
/*            std::cout << " adding run number " << atoi(token[0]) << "\n";*/
            fRunQueue->push_back( atoi(token[0]) );
            nruns++;
      }
       //for (int i = 0; i < n; i++) // n = #of tokens
       //   cout << "Token[" << i << "] = " << token[i] << endl;
       //cout << endl;
   }

   return(nruns);
}
//_____________________________________________________________________________

Int_t InSANEAnalysisManager::BuildQueue(const char * fName, const int ncolumns , const int runNumCol) {
      std::ifstream runlist(fName);
      int aRunNumber;
      int acolumn[ncolumns];
      //char commentchar = '#';
      //char dump[80];
      if (!runlist.is_open()) return (-1);
      while (!runlist.eof()) {
         /*          runlist.get(commentchar);
                   std::cout << "first char = " << commentchar << " \n";
                   runlist.unget();
         //          if( !strcmp( &commentchar,"#") ) {
                   if( commentchar == '#' ) {
                      runlist.getline(dump,80);
                      std::cout << " skipping " << dump << "  \n";
                      runlist >> aRunNumber; // skipping
                      continue;
                   } else {
                     runlist.unget();
                   }*/
         runlist >> aRunNumber;
         if (runlist.eof()) break;
         for (int i = 1; i < ncolumns; i++) {
            runlist >> acolumn[i-1];
         }
         if (ncolumns > 1) {
            if (acolumn[0] == 1)  fRunQueue->push_back(aRunNumber);
         } else {
            /*             std::cout << "pushing " << aRunNumber << " ... \n";     */
            fRunQueue->push_back(aRunNumber);
         }

      }
      runlist.close();
      return(0);
   }
//_____________________________________________________________________________

Int_t InSANEAnalysisManager::ProcessPreRunReportsOnly()
{
   if (fRunQueue->size() > 0)
      for (std::vector<int>::const_iterator it = fRunQueue->begin(); it != fRunQueue->end(); ++it) {
         SANERunManager::GetRunManager()->SetRun(*it);
         SANERunManager::GetRunManager()->GeneratePreRunReport();
      }
   return(0);
}
//_______________________________________________________//



Int_t InSANEAnalysisManager::ProcessOneRun()
{

   gROOT->SetBatch(kTRUE);

   auto * watch = new TStopwatch();
   watch->Reset();

   if (fRunQueue->size() > 0)
//  for(vector<int>::const_iterator it = fRunQueue->begin(); it != fRunQueue->end(); ++it)
   {
      watch->Start(kFALSE);


//   gROOT->ProcessLine(Form(".!./bin/sane_replay -r %d -n 90000 >> /dev/null ",fRunQueue->back()));
//    std::cout << "Analyzer replay and h2root conversion took " << watch->RealTime() << " seconds\n";
//   watch->Start(kFALSE);

      /*
        rm->SetRun(fRunQueue->back());

        SANERunManager::GetRunManager()->GeneratePreRunReport();

        rm->ConvertRawData();
        std::cout << "ConvertRawData() took " << watch->RealTime() << " seconds\n";
        watch->Start(kFALSE);

        rm->CalculatePedestals();
        std::cout << "CalculatePedestals() took " << watch->RealTime() << " seconds\n";
        watch->Start(kFALSE);

        rm->CalculateTiming();
        std::cout << "CalculateTiming() took " << watch->RealTime() << " seconds\n";
        watch->Start(kFALSE);

        rm->RunZerothPass();
        std::cout << "CreateCorrectedTree() took " << watch->RealTime() << " seconds\n";
        watch->Start(kFALSE);

        rm->Cluster("correctedBetaDetectors");
        std::cout << "Clustering took " << watch->RealTime() << " seconds\n";
        SANERunManager::GetRunManager()->GenerateRunReport();

        gROOT->Reset();*/
      fRunQueue->pop_back();
   }
   gROOT->SetBatch(kFALSE);
   return(0);
}
//_______________________________________________________//


