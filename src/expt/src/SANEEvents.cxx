#define SANEEvents_cxx
#include "SANEEvents.h"
#include "TROOT.h"
#include "SANERunManager.h"

ClassImp(SANEEvents)

//______________________________________________________________________________
SANEEvents::SANEEvents(const char * treename): InSANEAnalysisEvents(treename) {

   HMS  = new HMSEvent();
   BEAM = new HallCBeamEvent();
   BETA = new BETAEvent();
   BETA->AllocateMemory();
   BETA->AllocateHitsMemory();
   TRIG = new InSANETriggerEvent();
   MC = new BETAG4MonteCarloEvent();
   MC->AllocateMemory();
   MC->AllocateHitsMemory();

   CLUSTER = nullptr;

   if (SANERunManager::GetRunManager()->fVerbosity > 0) printf(" o Setting Branch Addresses...\n");
   if (!  SetBranches())if (SANERunManager::GetRunManager()->fVerbosity > 0)    printf(" - Branches Set.\n");

   //if(fTree) SetClusterBranches(fTree);

   /// \todo{ Implement DETECTORPhysicsEvent }
}
//______________________________________________________________________________
SANEEvents::SANEEvents(TChain * chain): InSANEAnalysisEvents(chain->GetName()) {

   HMS  = new HMSEvent();
   BEAM = new HallCBeamEvent();
   BETA = new BETAEvent();
   BETA->AllocateMemory();
   BETA->AllocateHitsMemory();
   TRIG = new InSANETriggerEvent();
   MC = new BETAG4MonteCarloEvent();
   MC->AllocateMemory();
   MC->AllocateHitsMemory();

   CLUSTER = nullptr;

   fAnalysisTree = chain;

   if (SANERunManager::GetRunManager()->fVerbosity > 0) printf(" o Setting Branch Addresses...\n");
   if (!  SetBranches(chain))if (SANERunManager::GetRunManager()->fVerbosity > 0)    printf(" - Branches Set.\n");

   /// \todo{ Implement DETECTORPhysicsEvent }
}
//______________________________________________________________________________
SANEEvents::~SANEEvents() {
   if (HMS) delete HMS;
   if (BEAM) delete BEAM;
   if (TRIG) delete TRIG;
   if (BETA) delete BETA;
   if (MC) delete MC;
   if (CLUSTER) delete CLUSTER;
}
//______________________________________________________________________________
void SANEEvents::FillTrees() {
   if (fTree)fTree->Fill();
}
//______________________________________________________________________________
Int_t SANEEvents::SetBranches() {

   Int_t returnvalue = 0;
   //Int_t setbrval=0;
   fTree = (TTree *)gROOT->FindObject(GetName());

   /// First set the branch for the BETAEvent
   /// \note this does not include the individual detector events
   if (fTree) {
      TBranch * ftBranch = nullptr;
      TBranch * gcBranch = nullptr;
      TBranch * lhBranch = nullptr;
      TBranch * bcBranch = nullptr;
      TBranch * betaBranch = nullptr;
      TBranch * runNumBranch = nullptr;
      TBranch * eventNumBranch = nullptr;
      TBranch * mcBranch = nullptr;
      ///Event and runnumbers
      runNumBranch = fTree->GetBranch("fRunNumber");
      if (runNumBranch) {
         runNumBranch->SetAddress(&(fRunNumber));
      } else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch fRunNumber to tree " << fTree->GetName() << ". \n"  ;
         fTree->Branch("fRunNumber", &fRunNumber, "fRunNumber/I");
      }
      eventNumBranch = fTree->GetBranch("fEventNumber");
      if (eventNumBranch) {
         eventNumBranch->SetAddress(&(fEventNumber));
      } else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch fEventNumber to tree " << fTree->GetName() << ". \n"  ;
         fTree->Branch("fEventNumber", &fEventNumber, "fEventNumber/I");
      }

      ///BETA
      betaBranch = fTree->GetBranch("betaDetectorEvent");
      if (betaBranch) {
         betaBranch->SetAddress(&BETA);
      } else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch betaDetectorEvent to tree " << fTree->GetName() << ". \n"  ;
         fTree->Branch("betaDetectorEvent", "BETAEvent", &BETA/*,32000,2*/);
      }
      ///Bigcal
      bcBranch = fTree->GetBranch("fBigcalEvent");
      if (bcBranch) {
         bcBranch->SetAddress(&(BETA->fBigcalEvent));
      } else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch fBigcalEvent to tree " << fTree->GetName() << ". \n"  ;
         fTree->Branch("fBigcalEvent", "BigcalEvent", &(BETA->fBigcalEvent));
      }
      ///GasCherenkov
      gcBranch = fTree->GetBranch("fGasCherenkovEvent");
      if (gcBranch) {
         gcBranch->SetAddress(&(BETA->fGasCherenkovEvent));
      } else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch fGasCherenkovEvent to tree " << fTree->GetName() << ". \n"  ;
         fTree->Branch("fGasCherenkovEvent", "GasCherenkovEvent", &(BETA->fGasCherenkovEvent));
      }
      ///LuciteHodoscope
      lhBranch = fTree->GetBranch("fLuciteHodoscopeEvent");
      if (lhBranch) {
         lhBranch->SetAddress(&(BETA->fLuciteHodoscopeEvent));
      } else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch fLuciteHodoscopeEvent to tree " << fTree->GetName() << ". \n"  ;
         fTree->Branch("fLuciteHodoscopeEvent", "LuciteHodoscopeEvent", &(BETA->fLuciteHodoscopeEvent));
      }
      ///ForwardTracker
      ftBranch = fTree->GetBranch("fForwardTrackerEvent");
      if (ftBranch) {
         ftBranch->SetAddress(&(BETA->fForwardTrackerEvent));
      } else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch fForwardTrackerEvent to tree " << fTree->GetName() << ". \n"  ;
         fTree->Branch("fForwardTrackerEvent", "ForwardTrackerEvent", &(BETA->fForwardTrackerEvent));
      }
      /// HMS branch
      TBranch * hmsBranch = fTree->GetBranch("hmsDetectorEvent");
      if (hmsBranch) hmsBranch->SetAddress(&HMS);
      else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch hmsDetectorEvent to tree betaDetectors\n" ;
         fTree->Branch("hmsDetectorEvent", "HMSEvent", &HMS);
      }

      /// Beam branch
      TBranch * beamBranch = fTree->GetBranch("hallcBeamEvent");
      if (beamBranch) beamBranch->SetAddress(&BEAM);
      else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch hmsDetectorEvent to tree betaDetectors\n" ;
         fTree->Branch("hallcBeamEvent", "HallCBeamEvent", &BEAM);
      }

      /// Trig branch
      TBranch * trigBranch = fTree->GetBranch("triggerEvent");
      if (trigBranch)  trigBranch->SetAddress(&TRIG);
      else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch triggerEvent to tree betaDetectors\n" ;
         fTree->Branch("triggerEvent", "InSANETriggerEvent", &TRIG);
      }
      //     cout << " Trigger branch address is " << (InSANETriggerEvent*)trigBranch->GetAddress()
      //          << " with object address " << TRIG << "\n";

      /// Monte Carlo branch
      mcBranch = fTree->GetBranch("monteCarloEvent");
      if (mcBranch) {
         mcBranch->SetAddress(&MC);
         /*       MC->SetHitsBranch(fTree); */
      } else {
         if (SANERunManager::GetRunManager()->fVerbosity > 0) std::cout << " + Adding NEW branch monteCarloEvent to tree betaDetectors\n" ;
         fTree->Branch("monteCarloEvent", "BETAG4MonteCarloEvent", &MC);
         /*      MC->AddHitsBranch(fTree); */
      }

      ///  Speed up read out
      //fTree->SetCacheSize(10000000);
      //fTree->AddBranchToCache("*");

   } else {
      std::cerr << "x- SANEEvents could not find tree  " << GetName() << "\n" ;
      returnvalue += 1;
   }

   return(returnvalue);
   //return(0);
}
//______________________________________________________________________________
Int_t SANEEvents::GetEvent(Long64_t event) {
   if (fTree) fTree->GetEvent(event);
   else printf(" no fTree \n");
   return(0);
}
//______________________________________________________________________________
Int_t SANEEvents::SetBranches(TChain * chain) {
   Int_t returnvalue = 0;
   //Int_t setbrval=0;
   if (chain) {
      ///Event and runnumbers
      chain->SetBranchAddress("fRunNumber", &(fRunNumber));
      chain->SetBranchAddress("fEventNumber", &(fEventNumber));
      ///BETA
      chain->SetBranchAddress("betaDetectorEvent", &BETA);
      ///Bigcal
      chain->SetBranchAddress("fBigcalEvent", &(BETA->fBigcalEvent));
      ///GasCherenkov
      chain->SetBranchAddress("fGasCherenkovEvent", &(BETA->fGasCherenkovEvent));
      ///LuciteHodoscope
      chain->SetBranchAddress("fLuciteHodoscopeEvent", &(BETA->fLuciteHodoscopeEvent));
      ///ForwardTracker
      chain->SetBranchAddress("fForwardTrackerEvent", &(BETA->fForwardTrackerEvent));
      /// HMS branch
      chain->SetBranchAddress("hmsDetectorEvent", &HMS);
      /// Beam branch
      chain->SetBranchAddress("hallcBeamEvent", &BEAM);
      /// Trig branch
      chain->SetBranchAddress("triggerEvent", &TRIG);
      /// Monte Carlo branch
      chain->SetBranchAddress("monteCarloEvent", &MC);
   } else {
      std::cerr << "x- SANEEvents could not find chain  " << GetName() << "\n" ;
      returnvalue += 1;
   }

   return(returnvalue);
}
//______________________________________________________________________________
InSANEClusterEvent *   SANEEvents::AddClusterEvents() {
   if (SANERunManager::GetRunManager()->fVerbosity > 0)std::cout << " + Creating InSANEClusterEvent \n";
   CLUSTER = new InSANEClusterEvent();
   if (SANERunManager::GetRunManager()->fVerbosity > 0)std::cout << " + Creating BIGCALCluster Array \n";
   CLUSTER->fClusters = new TClonesArray("BIGCALCluster", 1);
   return(CLUSTER);
}
//______________________________________________________________________________
InSANEClusterEvent *  SANEEvents::SetClusterBranches(TTree * aTree) {
   if (SANERunManager::GetRunManager()->fVerbosity > 0)std::cout << " o SANEEvents::SetClusterBranches(TTree *) \n";
   if (!CLUSTER) AddClusterEvents();

   if (!aTree) {
      std::cerr << "x- Null Tree\n";
      return(CLUSTER);
   }
   TBranch *clustBranch;
   clustBranch = aTree->GetBranch("bigcalClusterEvent");
   if (clustBranch) {
      clustBranch->SetAddress(&(CLUSTER));
   } else {
      if (SANERunManager::GetRunManager()->fVerbosity > 0)std::cout << " + Adding NEW branch bigcalClusterEvent to tree " << aTree->GetName() << ". \n"  ;
      aTree->Branch("bigcalClusterEvent", "InSANEClusterEvent", &(CLUSTER));
   }
   TBranch *clustArrayBranch = nullptr;
   clustArrayBranch = aTree->GetBranch("bigcalClusters");
   if (clustArrayBranch) {
      clustArrayBranch->SetAddress(&(CLUSTER->fClusters));
   } else {
      if (SANERunManager::GetRunManager()->fVerbosity > 0)std::cout << " + Adding NEW branch bigcalClusters to tree " << aTree->GetName() << ". \n"  ;
      aTree->Branch("bigcalClusters", &(CLUSTER->fClusters));
   }
   //       std::cout<< " o Cluster event... sweet...  fClusters addr: "<< CLUSTER->fClusters << ", event addr: " << CLUSTER <<"\n";
   return(CLUSTER);
}
//______________________________________________________________________________
InSANEClusterEvent *  SANEEvents::SetClusterBranches(TChain * chain) {
   if (SANERunManager::GetRunManager()->fVerbosity > 0)std::cout << " o SANEEvents::SetClusterBranches(TChain *) \n";
   if (!CLUSTER) AddClusterEvents();
   if (!chain) {
      std::cerr << "x- Null chain\n";
      return(CLUSTER);
   }
   chain->SetBranchAddress("bigcalClusterEvent", &(CLUSTER));
   chain->SetBranchAddress("bigcalClusters", &(CLUSTER->fClusters));
   return(CLUSTER);
}
//______________________________________________________________________________
void SANEEvents::ClearEvent(Option_t * opt) {
   BETA->ClearEvent();
   HMS->ClearEvent();
   BEAM->ClearEvent();
   TRIG->ClearEvent();
   MC->ClearEvent();
   if (CLUSTER)CLUSTER->ClearEvent();
}
//______________________________________________________________________________
void SANEEvents::SetEventNumber(Int_t num) {
   fEventNumber       = num;
   BETA->fEventNumber = num;
   BEAM->fEventNumber = num;
   TRIG->fEventNumber = num;
   HMS->fEventNumber  = num;
   MC->fEventNumber   = num;
}
//______________________________________________________________________________
void SANEEvents::SetRunNumber(Int_t num) {
   fRunNumber       = num;
   BETA->fRunNumber = num;
   BEAM->fRunNumber = num;
   TRIG->fRunNumber = num;
   HMS->fRunNumber  = num;
   MC->fRunNumber   = num;
}
//______________________________________________________________________________
void SANEEvents::Print(Option_t * opt ) const { 
   /// \todo Add options and prints for event classes
   std::cout << "  SANEEvents for tree " << fTree << "\n";
   std::cout << "             or chain " << fAnalysisTree << "\n";
   std::cout << "    BETAEvent       : " << BETA << "\n";
   std::cout << "    HMSEvent        : " << HMS << "\n";
   std::cout << "    TriggerEvent    : " << TRIG << "\n";
   std::cout << "    MonteCarloEvent : " << MC << "\n";
   std::cout << "    MC thrown       : " << MC->fThrownParticles << "\n";
   std::cout << "    BeamEvent       : " << BEAM << "\n";
   std::cout << "    BeamEvent       : " << BEAM << "\n";
   std::cout << "    ClusterEvent    : " << CLUSTER << "\n";
}
//______________________________________________________________________________

