#include "SANEScalerCalculations.h"

ClassImp(SANEScalerCalculation)
//______________________________________________________________________________
SANEScalerCalculation::SANEScalerCalculation(InSANEAnalysis * analysis) : BETACalculation(analysis) {
   SetNameTitle("SANEScalerCalculation", "SANEScalerCalculation");
   fScalers                        = nullptr;
   SetScalers(analysis->fScalers);
   fTotalCharge                    = 0.0;
   fScalerOutTree                  = nullptr;
   fClearCounts                    = true;
   fNegativeLiveTimeSum            = 0.0;
   fPositiveLiveTimeSum            = 0.0;
   fNumberOfIntervals              = 0;
   fLiveTimeSum                    = 0.0;
   fBETA2LiveTimeAverage           = 0.0;
}
//______________________________________________________________________________
SANEScalerCalculation::~SANEScalerCalculation() {
}
//______________________________________________________________________________
Int_t SANEScalerCalculation::Initialize() {

   Int_t result = BETACalculation::Initialize();

   if (SANERunManager::GetRunManager()->fVerbosity > 2)
      std::cout << " o SANEScalerCalculation::Initialize()\n";

   fRun = SANERunManager::GetRunManager()->GetCurrentRun();
   InSANERunManager::GetRunManager()->GetCurrentFile()->cd();

   fRun->fDuration      = 0;
   fRun->fTotalQ        = 0;
   fRun->fTotalQPlus    = 0;
   fRun->fTotalQMinus   = 0;
   fRun->fDeadTime      = 0;
   fRun->fDeadTimePlus  = 0;
   fRun->fDeadTimeMinus = 0;
   fRun->fLiveTime      = 0;
   fRun->fLiveTimePlus  = 0;
   fRun->fLiveTimeMinus = 0;
   fRun->fBCM1ChargeAsymmetry  = 0;
   fRun->fBCM1GatedPlusCharge  = 0;
   fRun->fBCM1GatedMinusCharge = 0;
   fRun->fBCM2ChargeAsymmetry  = 0;
   fRun->fBCM2GatedPlusCharge  = 0;
   fRun->fBCM2GatedMinusCharge = 0;

   if(fScalers->fScalerEvent) fScalers->fScalerEvent->ClearEvent();

   return(result);
}
//______________________________________________________________________________
Int_t SANEScalerCalculation::Calculate() {
   // Clear the counts if the interval has just been calculated
   if (fClearCounts) {
      fScalers->fScalerEvent->ClearCounts();
      fClearCounts = false;
   }
   // Count BETA1 Events
   if (fEvents->TRIG->IsBETA1Event()) {
      fScalers->fScalerEvent->fBETA1Counter.Count(fEvents->BEAM->fHelicity);
   }
   // Count BETA2 Events
   if (fEvents->TRIG->IsBETA2Event()) {
      fScalers->fScalerEvent->fBETA2Counter.Count(fEvents->BEAM->fHelicity);
   }
   // Count Pi0 Events
   if (fEvents->TRIG->IsPi0Event()) {
      fScalers->fScalerEvent->fPi0Counter.Count(fEvents->BEAM->fHelicity);
   }
   // Count Coin Events
   if (fEvents->TRIG->IsCoinEvent()) {
      fScalers->fScalerEvent->fCoinCounter.Count(fEvents->BEAM->fHelicity);
   }
   // Count HMS Events
   if (fEvents->TRIG->IsHMSEvent()) {
      fScalers->fScalerEvent->fHMS1Counter.Count(fEvents->BEAM->fHelicity);
   }
   // Count HMS2 Events
   if (fEvents->TRIG->IsHMS2Event()) {
      fScalers->fScalerEvent->fHMS2Counter.Count(fEvents->BEAM->fHelicity);
   }
   // Count LED Events
   if (fEvents->TRIG->IsLEDEvent()) {
      fScalers->fScalerEvent->fLEDCounter.Count(fEvents->BEAM->fHelicity);
   }
   // Count all triggers
   fScalers->fScalerEvent->fTSCounter.Count(fEvents->BEAM->fHelicity);

   return(0);
}
//______________________________________________________________________________
Int_t SANEScalerCalculation::CalculateInterval() {

   fNumberOfIntervals++;

   // Calculate the time and charges
   SANEScalerEvent * SE = fScalers->fScalerEvent;

   SE->fDeltaT         = SE->TimeLapsed();
   SE->fDeltaQ         = SE->AverageDeltaQ();
   SE->fDeltaQplus     = SE->AveragePositiveDeltaQ();
   SE->fDeltaQminus    = SE->AverageNegativeDeltaQ();
   fRun->fDuration    += SE->fDeltaT ;
   fRun->fTotalQ      += SE->fDeltaQ ;
   fRun->fTotalQPlus  += SE->fDeltaQplus ;
   fRun->fTotalQMinus += SE->fDeltaQminus ;
   // Swap the charges
   //fRun->fTotalQMinus += SE->fDeltaQplus ;
   //fRun->fTotalQPlus  += SE->fDeltaQminus ;


   //std::cout << "BCM1(+) dQ = " << SE->BCM1PositiveDeltaQ() << std::endl;
   //std::cout << "BCM1(-) dQ = " << SE->BCM1NegativeDeltaQ() << std::endl;
   
   fRun->fBCM1GatedPlusCharge  += SE->BCM1PositiveDeltaQ();
   fRun->fBCM1GatedMinusCharge += SE->BCM1NegativeDeltaQ();
   fRun->fBCM2GatedPlusCharge  += SE->BCM2PositiveDeltaQ();
   fRun->fBCM2GatedMinusCharge += SE->BCM2NegativeDeltaQ();

   // Now update the scaler event with the totals so far
   SE->fTime          = fRun->fDuration;
   SE->fTotalQ        = fRun->fTotalQ;
   SE->fTotalQplus    = fRun->fTotalQPlus;
   SE->fTotalQminus   = fRun->fTotalQMinus;

   // Beam currents
   SE->fBCM1               = SE->BeamCurrentMonitor1();
   SE->fBCM2               = SE->BeamCurrentMonitor2();
   SE->fBeamCurrentAverage = SE->GetBeamCurrent();

   //std::cout << " Calculating scaler live times for event " << SE->fEventNumber << "\n";
   // Update trigger counters to get the dead/live times
   SE->fBETA2Counter.Calculate();
   SE->fBETA1Counter.Calculate();
   SE->fPi0Counter.Calculate();
   SE->fCoinCounter.Calculate();
   SE->fLEDCounter.Calculate();
   SE->fHMS1Counter.Calculate();
   SE->fHMS2Counter.Calculate();
   SE->fTSCounter.Calculate();

   SE->fDeadTime = SE->fBETA2Counter.fDeadTime;
   SE->fLiveTime = SE->fBETA2Counter.fLiveTime;

   //fBETA2LiveTimeAverage = (fBETA2LiveTimeAverage*((double)(fNumberOfIntervals-1))+SE->fBETA2Counter.fLiveTime)/((double)fNumberOfIntervals);
   //SE->fBETA2Counter.fLiveTimeAverage = fBETA2LiveTimeAverage;
   //    SE->fBETA2Counter.Dump();
   // This should have been used for the main dead/live times shown
   /*SE->fTSCounter.UpdateLiveTimeAverages( &(fRun->fLiveTime),
                                          &(fRun->fLiveTimePlus),
                                          &(fRun->fLiveTimeMinus),
                                          fNumberOfIntervals);
   SE->fTSCounter.Dump();
   */
   fPi0LiveTimeAverage = (fPi0LiveTimeAverage * ((double)(fNumberOfIntervals - 1)) + SE->fPi0Counter.fLiveTime) / ((double)fNumberOfIntervals);
   //SE->fPi0Counter.fLiveTimeAverage = fPi0LiveTimeAverage;
   //    SE->fPi0Counter.Dump();

   // Calculate the BETA2 helicity dependent Dead/Live time
   //       SE->fBETA2NegativeDeadTime    = ((double)SE->fBETA2NegativeHelicityScaler - (double)SE->fBETA2NegativeHelicityTriggers)/((double)SE->fBETA2NegativeHelicityScaler + 0.0000001);
   //       SE->fBETA2PositiveDeadTime    = ((double)SE->fBETA2PositiveHelicityScaler - (double)SE->fBETA2PositiveHelicityTriggers)/((double)SE->fBETA2PositiveHelicityScaler + 0.0000001);
   //       SE->fBETA2NegativeLiveTime    = 1.0 - SE->fBETA2NegativeDeadTime;
   //       SE->fBETA2PositiveLiveTime    = 1.0 - SE->fBETA2PositiveDeadTime;

   // Update the run sums used for calcualting the run average in finalize
   //fNegativeLiveTimeSum += SE->fBETA2NegativeLiveTime;
   //fPositiveLiveTimeSum += SE->fBETA2PositiveLiveTime;
   //fLiveTimeSum         += SE->fLiveTime;

   //fRun->Print();//fTotalQMinus += fScalers->fScalerEvent->fTotalQminus;

   fRun->fTotalNPlus    += fScalers->fScalerEvent->fTSCounter.fPositiveHelicityTriggerCount;
   fRun->fTotalNMinus   += fScalers->fScalerEvent->fTSCounter.fNegativeHelicityTriggerCount;

   // The following triggers a reset of the event counters at the start of the next (detector) event
   fClearCounts = true;
   return(0);
}
//______________________________________________________________________________
Int_t SANEScalerCalculation::Finalize() {
   std::cout << "SANEScalerCalculation::Finalize() " << std::endl;
   //fRun->fLiveTime       = (double)fLiveTimeSum/((double)fNumberOfIntervals + 0.0000001);
   //fRun->fLiveTimePlus   = (double)fPositiveLiveTimeSum/((double)fNumberOfIntervals + 0.0000001);
   //fRun->fLiveTimeMinus  = (double)fNegativeLiveTimeSum/((double)fNumberOfIntervals + 0.0000001);
   //
   //fRun->fDeadTime       = 1.0 -  (double)fLiveTimeSum/((double)fNumberOfIntervals + 0.0000001);
   //fRun->fDeadTimePlus   = 1.0 -  (double)fPositiveLiveTimeSum/((double)fNumberOfIntervals + 0.0000001);
   //fRun->fDeadTimeMinus  = 1.0 -  (double)fNegativeLiveTimeSum/((double)fNumberOfIntervals + 0.0000001);


   InSANERun * arun = SANERunManager::GetRunManager()->GetCurrentRun();
   if(arun)  {
      //arun->Dump();
      arun->fBCM1ChargeAsymmetry  = (arun->fBCM1GatedPlusCharge-arun->fBCM1GatedMinusCharge)/(arun->fBCM1GatedPlusCharge+arun->fBCM1GatedMinusCharge);
      arun->fBCM2ChargeAsymmetry  = (arun->fBCM2GatedPlusCharge-arun->fBCM2GatedMinusCharge)/(arun->fBCM2GatedPlusCharge+arun->fBCM2GatedMinusCharge);

      // problem during runs 72460-72600 
      if( arun->GetRunNumber() >= 72460 && arun->GetRunNumber() < 72600 ){
         // use the average Q asym from moller and neighboring runs
         double AQ_avg = -0.001;
         double QT = arun->fTotalQ;

         arun->fBCM1ChargeAsymmetry = AQ_avg;
         arun->fBCM2ChargeAsymmetry = AQ_avg;
         //fRun->fDuration    += SE->fDeltaT ;
         //fRun->fTotalQ      += SE->fDeltaQ ;
         arun->fTotalQPlus           = QT*((AQ_avg+1.0)/2.0);
         arun->fTotalQMinus          = QT*(1.0-(AQ_avg+1.0)/2.0);
         arun->fBCM1GatedPlusCharge  = QT*((AQ_avg+1.0)/2.0);
         arun->fBCM1GatedMinusCharge = QT*(1.0-(AQ_avg+1.0)/2.0);
         arun->fBCM2GatedPlusCharge  = QT*((AQ_avg+1.0)/2.0);
         arun->fBCM2GatedMinusCharge = QT*(1.0-(AQ_avg+1.0)/2.0);
      }

   } else {
      Error("Finalize","NULL RUN POINTER!?");
   }


   return(0);
}
//______________________________________________________________________________



ClassImp(BETADeadTimeCalculation)

//______________________________________________________________________________
Int_t BETADeadTimeCalculation::Initialize() {

   if (SANERunManager::GetRunManager()->fVerbosity > 2)
      std::cout << " o BETADeadTimeCalculation::Initialize()\n";

   Int_t result  = BETACalculation::Initialize();
   fRun          = SANERunManager::GetRunManager()->GetCurrentRun();
   Bool_t isPerp = false;
   if(fRun->GetRunNumber() <72900) isPerp = true ;

   fParaLiveTimeSlope = -8.29801e-05;
   fPerpLiveTimeSlope = -7.28862e-05;

   // Live time fit is created in the script pass0/liveTime.cxx
   fBETA2LiveTimeFit = new TF1("LiveTimeFit","1.0+x*[0]",200,3200);
   fBETA2LiveTimeFit->SetParameter(0,0.00000001); // ideal for simulation

   if( fRun->GetRunNumber() > 70000 ) {
      //Warning("Initialize()", "Could not find LiveTimeFit!");
      fBETA2LiveTimeFit = new TF1("LiveTimeFit","1.0+x*[0]",200,3200);
      if(isPerp){
         if( fRun->GetRunNumber() > 72900 ) {
            Warning("Initialize()", "THIS IS NOT A PERP RUN!!!!!!!!!!1");
         }
         //Warning("Initialize()", "Could not find LiveTimeFit!");
         fBETA2LiveTimeFit->SetParameter(0,fPerpLiveTimeSlope); 
      } else {
         fBETA2LiveTimeFit->SetParameter(0,fParaLiveTimeSlope); 
      }
   }
   //InSANERunManager::GetRunManager()->GetCurrentFile()->cd();
   fRun->fDuration      = 0;
   fRun->fTotalQ        = 0;
   fRun->fTotalQPlus    = 0;
   fRun->fTotalQMinus   = 0;
   fRun->fDeadTime      = 0;
   fRun->fDeadTimePlus  = 0;
   fRun->fDeadTimeMinus = 0;
   fRun->fLiveTime      = 0;
   fRun->fLiveTimePlus  = 0;
   fRun->fLiveTimeMinus = 0;
   fNumberOfIntervals   = 0;

   return(result);
}
//______________________________________________________________________________
Int_t BETADeadTimeCalculation::CalculateInterval() {

   fNumberOfIntervals++;
   SANEScalerEvent * SE = fScalers->fScalerEvent;

   // Calculate the missing helicity scalers \todo add rest of counters
   if (fBETA2LiveTimeFit) {
      SE->fBETA2Counter.fNegativeHelicityLiveTime  =
         fBETA2LiveTimeFit->Eval(SE->fTSCounter.fNegativeHelicityTriggerCount/(SE->NegativeTimeFraction()));
      SE->fBETA2Counter.fNegativeHelicityDeadTime = 1.0 - SE->fBETA2Counter.fNegativeHelicityLiveTime;
      SE->fBETA2Counter.fNegativeHelicityScaler = (Long64_t)(SE->fBETA2Counter.fNegativeHelicityTriggerCount / SE->fBETA2Counter.fNegativeHelicityLiveTime);

      SE->fBETA2Counter.fPositiveHelicityLiveTime  =
         fBETA2LiveTimeFit->Eval(SE->fTSCounter.fPositiveHelicityTriggerCount/(SE->PositiveTimeFraction()));
      SE->fBETA2Counter.fPositiveHelicityDeadTime = 1.0 - SE->fBETA2Counter.fPositiveHelicityLiveTime;
      SE->fBETA2Counter.fPositiveHelicityScaler = (Long64_t) (SE->fBETA2Counter.fPositiveHelicityTriggerCount / SE->fBETA2Counter.fPositiveHelicityLiveTime);
   }

   SE->fDeltaT         = SE->TimeLapsed();
   SE->fDeltaQ         = SE->AverageDeltaQ();
   SE->fDeltaQplus     = SE->AveragePositiveDeltaQ();
   SE->fDeltaQminus    = SE->AverageNegativeDeltaQ();
   fRun->fDuration    += SE->fDeltaT ;
   fRun->fTotalQ      += SE->fDeltaQ ;
   fRun->fTotalQPlus  += SE->fDeltaQplus ;
   fRun->fTotalQMinus += SE->fDeltaQminus ;

   // Now update the scaler event with the totals so far
   SE->fTime          = fRun->fDuration;
   SE->fTotalQ        = fRun->fTotalQ;
   SE->fTotalQplus    = fRun->fTotalQPlus;
   SE->fTotalQminus   = fRun->fTotalQMinus;

   SE->fBETA2Counter.UpdateLiveTimeAverages(&(fRun->fLiveTime),
                                            &(fRun->fLiveTimePlus),
                                            &(fRun->fLiveTimeMinus),
                                            fNumberOfIntervals);
   fRun->fDeadTime      = 1.0 - fRun->fLiveTime;//SE->fBETA2Counter.fDeadTimeAverage;
   fRun->fDeadTimePlus  = 1.0 - fRun->fLiveTimePlus;//SE->fBETA2Counter.fDeadTimeAveragePlus;
   fRun->fDeadTimeMinus = 1.0 - fRun->fLiveTimeMinus;//SE->fBETA2Counter.fDeadTimeAverageMinus;

   //std::cout << " Interval : " << fNumberOfIntervals << std::endl;
   //std::cout << " Live Time : " << fRun->fLiveTime << std::endl;
   //std::cout << " Live Time+: " << fRun->fLiveTimePlus << std::endl;
   //std::cout << " Live Time-: " << fRun->fLiveTimeMinus << std::endl;

   return(0);
}
//_____________________________________________________________________________

