#include "InSANEMaterial.h"
ClassImp(InSANEMaterial) 
//______________________________________________________________________________________
InSANEMaterial::InSANEMaterial(TString Name,Double_t Z,Double_t A,Double_t L,Double_t T,
                               Double_t Rho, Double_t TR,Double_t X0){

	fName = Name;
	fZ    = Z;
	fA    = A;
	fL    = L;
	fT    = T;
	fRho  = Rho;  
	fTR   = TR;
	fX0   = X0;

}
//______________________________________________________________________________________
InSANEMaterial::~InSANEMaterial(){

}
//______________________________________________________________________________________
void InSANEMaterial::Clear(){
	fA    = 0;
	fZ    = 0;
	fL    = 0;
	fT    = 0;
	fTR   = 0;
	fX0   = 0;
	fRho  = 0;  
	fName = "Unknown";
}
//______________________________________________________________________________________
void InSANEMaterial::Process(){

       fT = fL*fRho; 
     
       if(fX0>1e-10){
	       fTR = fT/fX0;  
       }else{
	       std::cout << "[InSANEMaterial]: WARNING: X0 = 0!  Cannot calculate TR = T/X0." << std::endl;
       }

}
//______________________________________________________________________________________
void InSANEMaterial::CalculateRadiationLength(){

	// For mixtures.  Need Z and A! 
        // NOTE: The user needs to call this method on their own if they wish to use it. 

        Double_t alpha = fine_structure_const; 
	Double_t a     = alpha*fZ;
        Double_t a2    = a*a; 
        Double_t a4    = a2*a2; 
        Double_t a6    = a2*a4; 
	Double_t f_Z   = a2*(1./(1+a2) + 0.20206 - 0.0369*a2 +0.0083*a4 - 0.002*a6);
	Double_t Lrad,Lrad_prime;

	if(fZ==1){
		Lrad       = 5.31;
		Lrad_prime = 6.144;
	}else if(fZ==2){
		Lrad       = 4.79;
		Lrad_prime = 5.621;
	}else if(fZ==3){
		Lrad       = 4.74;
		Lrad_prime = 5.805;
	}
	else if(fZ==4){
		Lrad       = 4.71;
		Lrad_prime = 5.924;
	}else{
		Lrad       = log(184.15*pow(fZ,-1./3.));
		Lrad_prime = log(1194.*pow(fZ,-2./3.));
	}

	Double_t T1,T2;
	if( (fZ!=0)&&(fA!=0) ){
		T1  = 716.408*fA; 
		T2  = fZ*fZ*(Lrad-f_Z) + fZ*Lrad_prime;
		fX0 = 1./(T1*T2);
	}else{
		fX0 = 0;
	}

}
//______________________________________________________________________________________
void InSANEMaterial::Print(){

        std::cout << "------------------------------------------" << std::endl;
	std::cout << "Name: " << fName                            << std::endl; 
        std::cout << "Z:    " << Form("%.2f",fZ  ) << " [-]"      << std::endl; 
        std::cout << "A:    " << Form("%.2f",fA  ) << " [g/mol]"  << std::endl; 
        std::cout << "L:    " << Form("%.2f",fL  ) << " [cm]"     << std::endl; 
        std::cout << "rho:  " << Form("%.3E",fRho) << " [g/cm^3]" << std::endl; 
        std::cout << "T:    " << Form("%.3E",fT  ) << " [g/cm^2]" << std::endl; 
        std::cout << "X0:   " << Form("%.3E",fX0 ) << " [g/cm^2]" << std::endl; 
        std::cout << "TR:   " << Form("%.3E",fTR ) << " [-]"      << std::endl; 

}
