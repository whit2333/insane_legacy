#include "InSANEParticle.h"

ClassImp(InSANEParticle)

//______________________________________________________________________________
InSANEParticle::InSANEParticle() {
   Clear();
}
//______________________________________________________________________________
InSANEParticle::~InSANEParticle() {  }
//______________________________________________________________________________
//InSANEParticle::InSANEParticle(const InSANEParticle& rhs):TParticle(rhs) {
//   (*this) = rhs;
//}
////______________________________________________________________________________
//InSANEParticle& InSANEParticle::operator=(const InSANEParticle &rhs) {
//   if(this!=&rhs) {
//      TParticle::operator=(rhs);
//      fHelicity = rhs.fHelicity;
//      fMomentum4Vector = rhs.fMomentum4Vector;
//   }
//   return(*this);
//}
//______________________________________________________________________________
void InSANEParticle::CalculateVectors() {
   fMomentum4Vector.SetXYZT(Px(), Py(), Pz(), Energy());
}
//______________________________________________________________________________
void InSANEParticle::Clear(Option_t * opt ) {
   TParticle::Clear(opt);
   fHelicity = 0;
   fXSId = 0;
   fMomentum4Vector.SetXYZT(0.0, 0.0, 0.0, 0.0);
}
//______________________________________________________________________________
