#ifndef INSANEMATERIAL_H
#define INSANEMATERIAL_H 1

#include <cstdlib> 
#include <iostream>
#include <iomanip> 
#include <cmath> 
#include "TString.h"
#include "InSANESystemOfUnits.h"
#include "InSANEPhysicalConstants.h"

class InSANEMaterial{

	private:
                TString fName;         /// Name of material  
		Double_t fZ,fA;        /// Z   = Atomic number,  A = Atomic mass (g/mol)
                Double_t fL,fT;        /// L   = thickness (cm), T = thickness (g/cm^2),
                Double_t fX0,fTR;      /// X0  = Radiation length (g/cm^2), TR = Number of radiation lengths (= T/X0)  
                Double_t fRho;         /// Rho = density (g/cm^3) 

        public: 
		InSANEMaterial(TString Name="",Double_t Z=0,Double_t A=0,Double_t L=0,
                               Double_t T=0,Double_t Rho=0,Double_t TR=0,Double_t X0=0);

		virtual ~InSANEMaterial();

		void Clear();
                void Print();
                void Process();                     /// Compute T, TR, etc.  
                void CalculateRadiationLength();    /// for mixtures  

		void SetZ(Double_t z){fZ = z;}
		void SetA(Double_t a){fA = a;}
		void SetL(Double_t l){fL = l;}
		void SetX0(Double_t x0){fX0 = x0;}
                void SetT(Double_t t){fT = t;} 
                void SetTR(Double_t tr){fTR = tr;} 
                void SetRho(Double_t rho){fRho = rho;} 
		void SetName(TString name){fName = name;} 
 
                TString GetName(){return fName;}

                Double_t GetZ(){return fZ;}
                Double_t GetA(){return fA;}
                Double_t GetL(){return fL;}
                Double_t GetT(){return fT;}
                Double_t GetTR(){return fTR;}
                Double_t GetX0(){return fX0;}
                Double_t GetRho(){return fRho;}

                ClassDef(InSANEMaterial,1) 

}; 

#endif 
