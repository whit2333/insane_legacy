#ifndef InSANENucleus_HH
#define InSANENucleus_HH 1

#include <cstdlib>   
#include <iostream>
#include "TNamed.h"
#include "InSANEPhysicalConstants.h"
#include "TGeoElement.h"

/** General nucleus object. 
 */
class InSANENucleus : public TNamed {

   public:
      enum NucleusType {kProton,kNeutron,kDeuteron,k3He,kTriton,k4He,k56Fe,kOther} fType;

   protected:
      Int_t    fZ;        /// Number of protons
      Int_t    fA;        /// Mass Number Number of nucleons (including hyper nuclei)
      Int_t    fN;        /// Number of neutrons
      Int_t    fL;        /// Number of lambdas
      Int_t    fPdgCode;  /// non-nucleon: 10LZZZAAAI, proton: 2212, neutron: 2112
      Double_t fDelta;    /// Binding energy per nucleon
      Double_t fs;        /// Spin of nucleus 
      Double_t fm_a;      /// atomic mass of nucleus 
      Double_t fP_p;      /// Effective polarization of the proton  in a nucleus 
      Double_t fP_n;      /// Effective polarization of the neutron in a nucleus
      /// If the nucleus N has polarization P, then nucleon i polarization is P_i^(N) = P_i*P  
                          /// If the nucleus N has polarization P, then nucleon i polarization is P_i^(N) = P_i*P  
                          //
      TGeoElementRN * fElementRN = nullptr; //!

      void SetType(NucleusType);  ///< \deprecated Do not use when defining nuclei. Put everything in

      double CalcMass();
      int    CalcPdgCode() const;

   protected:

      static InSANENucleus * fgProton;
      static InSANENucleus * fgNeutron;
      static InSANENucleus * fgDeuteron;
      static InSANENucleus * fgHe3;
      static InSANENucleus * fgHe4;
      static InSANENucleus * fgTriton;
      static InSANENucleus * fgC12;
      static InSANENucleus * fgFe56;

   public:
      InSANENucleus(Int_t Z = 1, Int_t A = 1, Int_t L = 0);
      virtual ~InSANENucleus(){ }

      InSANENucleus(const InSANENucleus& rhs)            = default ;
      InSANENucleus& operator=(const InSANENucleus& rhs) = default ;
      InSANENucleus(InSANENucleus&&)                     = default;
      InSANENucleus& operator=(InSANENucleus&&)          = default;

      InSANENucleus&       operator+=(const InSANENucleus& rhs);
      InSANENucleus&       operator-=(const InSANENucleus& rhs);
      const InSANENucleus  operator+(const InSANENucleus &other) const ;
      const InSANENucleus  operator-(const InSANENucleus &other) const ;


      //InSANENucleus(const InSANENucleus& rhs): TNamed(rhs) { (*this) = rhs; }
      //InSANENucleus& operator=(const InSANENucleus& rhs);

      /** Comparison operator for testing against other nuclei.
       *   This is especially  useful when using the static pointers.
       */
      bool operator==(const InSANENucleus& other) const;
            
      /** Static definition of a proton. */
      static const InSANENucleus & Proton();
      static const InSANENucleus & Neutron();
      static const InSANENucleus & Deuteron();
      static const InSANENucleus & He3();
      static const InSANENucleus & He4();
      static const InSANENucleus & C12();
      static const InSANENucleus & Fe56();

      void Print(const Option_t * opt = "") const; 
      void Print(std::ostream& stream) const; 

      void SetZ(Int_t z){ fZ = z; fN = fA - fZ - fL; }
      void SetA(Int_t a){ fA = a; fN = fA - fZ - fL; }
      void SetL(Int_t l){ fL = l; fN = fA - fZ - fL; }
      void SetSpin(Double_t s){ fs = s; }   
      void SetMass(Double_t m){ fm_a = m; }   
      void SetBindingEnergy(Double_t e){ fDelta = e; }   
      void SetEffectiveProtonPolarization(Double_t p){ fP_p = p; }   
      void SetEffectiveNeutronPolarization(Double_t p){ fP_n = p; }   

      Int_t       A()                            const {return fA;}
      Int_t       Z()                            const {return fZ;}
      Int_t       N()                            const {return fN;}
      Int_t       L()                            const {return fL;}

      NucleusType GetType()                         const {return fType;}
      Int_t       GetA()                            const {return fA;}
      Int_t       GetZ()                            const {return fZ;}
      Int_t       GetN()                            const {return fN;}
      Int_t       GetL()                            const {return fL;}
      Double_t    GetSpin()                         const {return fs;} 
      Double_t    GetMass()                         const {return fm_a;}
      Double_t    GetBindingEnergy()                const {return fDelta;} 
      Double_t    GetEffectiveProtonPolarization()  const {return fP_p;}  
      Double_t    GetEffectiveNeutronPolarization() const {return fP_n;}  

      int GetPdgCode() const {return fPdgCode;}

      ClassDef(InSANENucleus,4)
};

#endif

