#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;

#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace CLHEP;
#pragma link C++ namespace InSANE;
#pragma link C++ namespace InSANE::Kine;
#pragma link C++ namespace InSANE::Phys;
#pragma link C++ namespace InSANE::Math;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::integrate;
#pragma link C++ namespace insane::math;

#pragma link C++ class std::vector<double>+;
#pragma link C++ class std::function<double(double)>+;

#pragma link C++ function insane::math::StandardPhi(double); 
#pragma link C++ function insane::integrate::gaus(std::function<double(double)>, double, double);
#pragma link C++ function insane::integrate::WW(std::function<double(double)>, double, double);


//-----------------------------------------------------------------------
//

//#pragma link C++ class std::vector<TString*>+;
//#pragma link C++ class std::map<int, double >+;
//#pragma link C++ class std::vector<TGraph*>+;

#pragma link C++ class InSANEPMTResponse+;
#pragma link C++ class InSANEPMTResponse2+;
#pragma link C++ class InSANEPMTResponse3+;
#pragma link C++ class InSANEPMTResponse32+;
#pragma link C++ class InSANEPMTResponse33+;
#pragma link C++ class InSANEPMTResponse4+;
#pragma link C++ class InSANEPMTResponse42+;
#pragma link C++ class InSANEPMTResponse5+;

#pragma link C++ class InSANENucleus+; 
#pragma link C++ global fgProton+;
#pragma link C++ global fgNeutron+;
#pragma link C++ global fgDeuteron+;
#pragma link C++ global fg3He+;
#pragma link C++ global fgTriton+;
// #pragma link C++ global fgIron+;
// #pragma link C++ global fgOther+;

//#pragma link C++ class std::map<Int_t,TFile*>+;
//#pragma link C++ class std::pair<Int_t,TFile*>+;


#pragma link C++ class InSANEParticle+;
#pragma link C++ class std::vector<InSANEParticle*>+;
#pragma link C++ class InSANEMaterial+; 

#pragma link C++ function InSANE::Kine::SetMomFromEThetaPhi(TParticle*, double *x);

#pragma link C++ function InSANE::Math::TestFunction22(double);
#pragma link C++ function InSANE::Math::CGLN_F1(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F2(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F3(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F4(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F5(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F6(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F7(double,double,double);
#pragma link C++ function InSANE::Math::CGLN_F8(double,double,double);

#pragma link C++ function InSANE::Math::multipole_E(double,double);
#pragma link C++ function InSANE::Math::multipole_M(double,double);

#pragma link C++ function InSANE::Math::f_rad_length(double);
#pragma link C++ function InSANE::Math::rad_length(int,int);

#pragma link C++ function InSANE::Kine::v0(double,double,double);
#pragma link C++ function InSANE::Kine::vL(double,double);
#pragma link C++ function InSANE::Kine::vT(double,double,double);
#pragma link C++ function InSANE::Kine::vTT(double,double);
#pragma link C++ function InSANE::Kine::vTL(double,double,double);
#pragma link C++ function InSANE::Kine::vTprime(double,double,double);
#pragma link C++ function InSANE::Kine::vTLprime(double,double,double);

#pragma link C++ function InSANE::Kine::Sig_Mott(double,double);
#pragma link C++ function InSANE::Kine::fRecoil(double,double,double);
#pragma link C++ function InSANE::Kine::tau(double,double);
#pragma link C++ function InSANE::Kine::q_abs(double,double);

#pragma link C++ function InSANE::Kine::Qsquared(double,double,double);
#pragma link C++ function InSANE::Kine::xBjorken(double,double,double);
#pragma link C++ function InSANE::Kine::xBjorken_EEprimeTheta(double,double,double);
#pragma link C++ function InSANE::Kine::nu(double,double);

#pragma link C++ function InSANE::Kine::W_xQsq(double,double,double);
#pragma link C++ function InSANE::Kine::W_EEprimeTheta(double,double,double);
#pragma link C++ function InSANE::Kine::xBjorken_WQsq(double,double,double);
#pragma link C++ function InSANE::Kine::Q2_xW(double,double,double);

#pragma link C++ function InSANE::Kine::BeamEnergy_xQ2y(double,double,double,double);
#pragma link C++ function InSANE::Kine::Eprime_xQ2y(double,double,double,double);
#pragma link C++ function InSANE::Kine::Eprime_W2theta(double,double,double,double);
#pragma link C++ function InSANE::Kine::Theta_xQ2y(double,double,double,double);
#pragma link C++ function InSANE::Kine::Theta_epsilonQ2W2(double,double,double,double);

#pragma link C++ function InSANE::Kine::D(double,double,double,double);
#pragma link C++ function InSANE::Kine::d(double,double,double,double);
#pragma link C++ function InSANE::Kine::Eta(double,double,double);
#pragma link C++ function InSANE::Kine::Xi(double,double,double);
#pragma link C++ function InSANE::Kine::Chi(double,double,double,double);


#endif

