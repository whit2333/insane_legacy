#ifndef InSANEParticle_HH
#define InSANEParticle_HH

#include "TParticle.h"
#include "TLorentzVector.h"

/** Container class for Monte Carlo.
 *  Typically there is only one of these which are stored, but sometimes,
 *  e.g. pi0 decay, you have more than one.
 *
 * \ingroup Events
 */
class InSANEParticle : public TParticle {

   public: 
      TLorentzVector   fMomentum4Vector;
      Int_t            fHelicity;
      Int_t            fXSId; 

   public: 
      InSANEParticle();
      virtual ~InSANEParticle();

      InSANEParticle(const InSANEParticle&) = default;               // Copy constructor
      InSANEParticle(InSANEParticle&&) = default;                    // Move constructor
      InSANEParticle& operator=(const InSANEParticle&) & = default;  // Copy assignment operator
      InSANEParticle& operator=(InSANEParticle&&) & = default;       // Move assignment operato
      //virtual ~InSANEParticle() { }

      //InSANEParticle(const InSANEParticle& rhs);
      //InSANEParticle& operator=(const InSANEParticle &rhs);

      void CalculateVectors();
      void Clear(Option_t * opt = "");

      ClassDef(InSANEParticle, 6)
};

#endif

