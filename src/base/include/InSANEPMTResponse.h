#ifndef InSANEPMTResponse_HH
#define InSANEPMTResponse_HH
#include "TMath.h"

#include "TF1.h"
#include "TObject.h"


/**
 * NIMA 451 (2000) 623}637
 */
class  InSANEPMTResponse {
   protected:
      Int_t  fNmax;
      Int_t  fNIntPoints;
      double fXIntMin;
      double fXIntMax;

      TF1 * fFit_low_light;
      TF1 * fFit;
      Double_t fA,fN0,fP0,fP1,fpe,fxp,fsigp,fx0,fsig0,fx1,fsig1,fmu;
      Double_t fA2;
      Double_t fgN;

   public:
      InSANEPMTResponse();
      virtual ~InSANEPMTResponse();
      Int_t GetMaxNPE() const {return(fNmax);}
      void  SetMaxNPE(Int_t n){ fNmax = n;}
      Int_t GetNIntPoints() const {return(fNIntPoints);}
      void  SetNIntPoints(Int_t n){ fNIntPoints = n;}

      /** Sets all the internal parameters.*/
      virtual void SetParameters(double *p);
      void PrintParameters();

      /** PMT response fit function with 12 parameters.  */
      virtual double PMT_fit(double *x, double *p) const ;

      double SER0(double *x, double *p) const ;
      double SER0_exp(double *x, double *p) const ;
      double SER0_peak(double *x, double *p) const ;
      double SER(double *x, double *p) const ;
      double SER_exp(double *x, double *p) const ;
      double SER_peak(double *x, double *p) const ;
      double M_pmt(double *x, double *p) const ;
      double M_pmt1(double *x, double *p) const ;

      double f_N_0(double *x, double *p,int n) const ;
      double f_N(double *x, double *p,int n) const ;
      double f_N_wrap(double *x, double *p) const { return(f_N(x,p,int(p[6])) ); }
      double conv_fit(double *x, double *p) const {return(0.0);}

      /** Light light version of PMT_fit with 10 parameters.
       */
      double PMT_fit_low_light(double *x, double *p) const ;

      TF1 * GetPeakFunction(Int_t n = 1) const ;
      TF1 * GetDoublePeakFunction(Int_t n = 1) const ;
      TF1 * GetExpFunction() const ;

ClassDef(InSANEPMTResponse,1)
};

/** Fit iwth 9 parameters for small mu only. Approximates sig1 and x1 for small mu only. */ 
class  InSANEPMTResponse2 : public InSANEPMTResponse  {
public :
      /** PMT response fit function with 9 parameters. For small mu only.  */
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(InSANEPMTResponse2,1)
};

/** Fit with 9 parameters for all mu.  Approximates sig1 and x1 as sig0 and x1. */ 
class  InSANEPMTResponse3 : public InSANEPMTResponse  {
public :
      /** PMT response fit function with 9 parameters  for n > 1  */
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(InSANEPMTResponse3,1)
};

/** Fit with 11 parameters for all mu.  sig1 and x1  are now free parameters. */ 
class  InSANEPMTResponse32 : public InSANEPMTResponse  {
public :
      /** PMT response fit function with 9 parameters  for n > 1  */
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(InSANEPMTResponse32,1)
};

/** Fit with 10 parameters for all mu.  
 *  sig1 and x1  are now free parameters. 
 *  P0 and P0 are both calculated from mu using Poisson dist.
 */ 
class  InSANEPMTResponse33 : public InSANEPMTResponse  {
public :
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(InSANEPMTResponse33,1)
};

/** Fit includes a second peak for the "double track"
 */
class  InSANEPMTResponse4 : public InSANEPMTResponse  {
public :
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(InSANEPMTResponse4,1)
};

/** Fit includes a second peak for the "double track"
 */
class  InSANEPMTResponse42 : public InSANEPMTResponse  {
   //public:
   //   double fSmear;
   public:
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

      ClassDef(InSANEPMTResponse42,1)
};


/** Fit includes a second peak for the "double track" and an exponential background.
 * 
 */
class  InSANEPMTResponse5 : public InSANEPMTResponse  {
public :
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);
      TF1 * GetBackgroundExpFunction() const;
      Double_t fA0;   /// Amplitude multiplying exponential background 
ClassDef(InSANEPMTResponse5,1)
};


#endif
