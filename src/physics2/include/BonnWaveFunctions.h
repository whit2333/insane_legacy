#ifndef BonnWaveFunctions_HH
#define BonnWaveFunctions_HH 1

#include "WaveFunctions.h"
#include "TMath.h"
#include <iostream>

namespace InSANE {
   namespace physics {

/** The Bonn meson-exchange model for NN interation.
 *  R. Machleidt (Los Alamos & UCLA) , K. Holinde, C. Elster (Bonn U.) 
 *  http://inspirehep.net/record/249900
 *
 *  Deuteron wave functions following Appendix D.
 *  These are the momentum space wavefunctions for a nucelonic Fock space.
 */
class BonnDeuteronWaveFunction : public WaveFunction {
   protected:
      int fNu; // Number of terms in D.25
      int fNw; // Number of terms in D.25

      double fCj[11];  // coefficients int D.25
      double fDj[11];  // coefficients int D.25
      double fm0;
      double fAlpha;

      double mj( int j){ return( fAlpha + double(j-1)*fm0 ); }
      double mj2(int j){ return( mj(j)*mj(j) ); }
      double Dj(int nw_2,int nw_1, int nw);

   public:

      BonnDeuteronWaveFunction();
      virtual ~BonnDeuteronWaveFunction();

      void Check(int N = 1000);

      double Psi_a_0(double q2);
      double Psi_a_2(double q2);

      double u_a(double r); // WF in position space 
      double w_a(double r); // Eqn D.25

      virtual std::complex<double> Evaluate(double *,double *);

   ClassDef(BonnDeuteronWaveFunction,1)
};
}
}

#endif

