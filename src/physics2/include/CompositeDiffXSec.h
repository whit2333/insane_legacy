#ifndef CompositeDiffXSec_HH
#define CompositeDiffXSec_HH 1

#include "InSANEDiffXSec.h"
#include "InSANEInelasticRadiativeTail2.h"
#include "InSANEFortranWrappers.h"
#include "InSANEMathFunc.h"


namespace InSANE {
   namespace physics {

      /** Builds a cross section for a target nucleus from the supplied
       *  proton and neutron cross secitons.
       *
       * \ingroup XSec
       */
      class CompositeDiffXSec : public InSANEDiffXSec {

         protected:
            InSANEDiffXSec * fProtonXSec;   //->
            InSANEDiffXSec * fNeutronXSec;  //->

         public:

            CompositeDiffXSec();
            CompositeDiffXSec(const CompositeDiffXSec& rhs) ;
            virtual ~CompositeDiffXSec();
            CompositeDiffXSec& operator=(const CompositeDiffXSec& rhs) ;
            CompositeDiffXSec(CompositeDiffXSec&&) = default;
            CompositeDiffXSec& operator=(CompositeDiffXSec&&) & = default;       // Move assignment operator

            virtual CompositeDiffXSec*  Clone(const char * newname) const ;
            virtual CompositeDiffXSec*  Clone() const ; 

            virtual Double_t EvaluateXSec(const Double_t * x) const ;

            // Here we copied a bunch of setters from InSANEDiffXSec because we need to set not only this class
            // but the proton and neutron cross sections. The getters are not changed because they should be identical
            // to this class (and really not used).
            virtual void SetTargetMaterial(const InSANETargetMaterial& mat) ;
            virtual void SetTargetNucleus(const InSANENucleus & targ) ;

            void       SetTargetMaterialIndex(Int_t i) ;

            virtual void  SetUnits(const char * t);
            virtual void  SetBeamEnergy(Double_t en);
            virtual void  SetPhaseSpace(InSANEPhaseSpace * ps);
            virtual void  InitializePhaseSpaceVariables();
            virtual void  InitializeFinalStateParticles();
            virtual void  SetParticleType(Int_t pdgcode, Int_t part = 0) ;

            ClassDef(CompositeDiffXSec,1)
      };

   }
}


#endif

