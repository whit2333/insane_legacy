#ifndef InSANE_physics_ComptonFormFactors_HH
#define InSANE_physics_ComptonFormFactors_HH

#include "TObject.h"

namespace InSANE {
   namespace physics {

      class ComptonFormFactors {
         public:
            ComptonFormFactors(){ } 
            virtual ~ComptonFormFactors(){ } 

            ClassDef(ComptonFormFactors,1)
      };

   }
}

#endif
