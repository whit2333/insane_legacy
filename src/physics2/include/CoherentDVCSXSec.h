#ifndef CoherentDVCSXSec_H
#define CoherentDVCSXSec_H 1

#include "InSANEExclusiveDiffXSec.h"
#include "InSANEInclusiveDiffXSec.h"
#include "TMath.h"
#include "InSANEFormFactors.h"
#include "TVector3.h"
#include "InSANEMathFunc.h"

#include "InSANEDipoleFormFactors.h"


namespace InSANE {
   namespace physics {

      /** Elastic scattering using the Dipole FFs.
       * \ingroup exclusiveXSec
       */
      class CoherentDVCSXSec : public InSANEExclusiveDiffXSec {

         protected:

            // BSA fit parameters... crude.
            double falpha = 0.3;
            double fbeta  =-0.1;

            double fNuclearTargetMass = 3.7;

            struct dvcsvar_t { double Q2  , t    , x , phi; };
            struct dvcspar_t {
               double Q2_0   ;//= 1.0;
               double alpha  ;//= 2.5;
               double b      ;//= -11.0;
               double beta   ;//=  12.0;
               double xc     ;//=   0.2;
               double c      ;//=   0.2;
               double d      ;//=   0.4;
            };
            //______________________________________________________________________________
            //                           Q20  alpha      b    beta    xc   c     d
            dvcspar_t PAR_COHDVCS   = {  1.0,  2.5,  -11.0,    12.0, 0.2, 0.2,  0.4}; // b,beta from t-fit to tc
            dvcspar_t PAR_INCDVCS   = {  1.0,  1.5,  -1.408,   4.0,  0.2, 0.2,  0.4};
            dvcspar_t PAR_COHPI0    = {  1.0,  3.0,  -8.8,     7.3,  0.2, 0.3,  0.0};
            dvcspar_t PAR_INCPI0    = {  1.0,  3.0,  -1.408,   4.0,  0.2, 0.3,  0.0};

            // What are the cross section units?
            double xsec_e1dvcs(const dvcsvar_t &x,const dvcspar_t &p) const;

            mutable InSANEDipoleFormFactors   fDipoleFFs;
            dvcsvar_t fRefDVCSVars  = { 1.0  , -0.001  , 1.0 , 0.0 };

            double xsec_normalization() const {
               double E0       = GetBeamEnergy();
               double M        = 4.0*0.938;
               double nu       = fRefDVCSVars.Q2/(2.0*M);
               double theta    = InSANE::Kine::Theta_xQ2y(1.0,fRefDVCSVars.Q2,nu/E0,M);
               double sig_mott = InSANE::Kine::Sig_Mott(E0,theta)*hbarc2_gev_nb;
               double Fc       = fDipoleFFs.FCHe4(fRefDVCSVars.Q2);
               double sig_el   = Fc*Fc*sig_mott;
               double res      = xsec_e1dvcs(fRefDVCSVars, PAR_COHDVCS);
               return(sig_el/res);
            }

         public:
            CoherentDVCSXSec();
            virtual ~CoherentDVCSXSec();
            CoherentDVCSXSec(const CoherentDVCSXSec& rhs) = default ;
            CoherentDVCSXSec& operator=(const CoherentDVCSXSec& rhs) = default ;
            CoherentDVCSXSec(CoherentDVCSXSec&&) = default;
            CoherentDVCSXSec& operator=(CoherentDVCSXSec&&) & = default; // Move assignment operator

            virtual CoherentDVCSXSec*  Clone(const char * newname) const {
               std::cout << "CoherentDVCSXSec::Clone()\n";
               auto * copy = new CoherentDVCSXSec();
               (*copy) = (*this);
               return copy;
            }
            virtual CoherentDVCSXSec*  Clone() const { return( Clone("") ); } 

            virtual void       InitializePhaseSpaceVariables() ;
            virtual void       DefineEvent(Double_t * vars);
            virtual Double_t * GetDependentVariables(const Double_t * x) const ;
            virtual Double_t   EvaluateXSec(const Double_t * x) const;
            virtual Double_t   GetBeamSpinAsymmetry(const Double_t * x) const;

            /**  Returns the scattered electron energy using the angle. */
            Double_t GetEPrime(const Double_t theta) const ;
            virtual Double_t Jacobian(const Double_t * vars) const;

            ClassDef(CoherentDVCSXSec, 1)
      };

   }
}

#endif

