#ifndef InSANE_CLASTargets_HH
#define InSANE_CLASTargets_HH

#include "InSANETarget.h"

namespace InSANE {
   namespace physics {

      class Helium4GasTarget : public InSANETarget {
         public:
            Helium4GasTarget(const char * name  = "Helium4GasTarget",
                             const char * title = "Helium4 Gas Target" );
            virtual ~Helium4GasTarget();

            virtual void DefineMaterials();

            ClassDef(Helium4GasTarget,1)
      };

      class LH2Target : public InSANETarget {
         public:
            LH2Target(const char * name  = "LH2Target",
                             const char * title = "Liquid H2 Target" );
            virtual ~LH2Target();

            virtual void DefineMaterials();

            ClassDef(LH2Target,1)
      };

   }
}

#endif

