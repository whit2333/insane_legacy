#include "CLASTargets.h"

namespace InSANE {
   namespace physics {

      Helium4GasTarget::Helium4GasTarget(const char * name, const char * title) : 
         InSANETarget(name,title)
      { 
         DefineMaterials();
      }
      //______________________________________________________________________________

      Helium4GasTarget::~Helium4GasTarget()
      { }
      //______________________________________________________________________________

      void Helium4GasTarget::DefineMaterials()
      {
         using namespace CLHEP;
         double       target_length = 40.0;
         auto * matHe4 = new InSANETargetMaterial("He4","He4",2,4);

         double a_noUnit     = 4.003;
         double pre_noUnit   = 3.0;
         double tempe_noUnit = 298.;
         double pressure     = 3.0*atmosphere;
         double temperature  = 298.*kelvin;
         double He4_density  = (a_noUnit*pre_noUnit)/(0.0821*tempe_noUnit)*kg/m3; //0.164*kg/m3 at 1 atm;

         matHe4->fLength          = target_length;    //cm
         matHe4->fDensity         = He4_density/(g/cm3); // g/cm3
         matHe4->fZposition       = 0.0;     //cm

         double window_thickness   =  15.0/10000.0 ;   //15 um
         auto * matAlEndcap1 = new InSANETargetMaterial("Al-endcap1", "Aluminum target endcap upstream", 13, 27);
         matAlEndcap1->fLength               =  window_thickness ;   //15 um
         matAlEndcap1->fDensity              =  2.7;        // g/cm3
         matAlEndcap1->fZposition            = -(target_length/2.0+window_thickness/2.0);     //cm

         auto * matAlEndcap2 = new InSANETargetMaterial("Al-endcap2", "Aluminum target endcap downstream", 13, 27);
         matAlEndcap2->fLength               =  window_thickness ;  //cm
         matAlEndcap2->fDensity              =  2.7;       // g/cm3
         matAlEndcap2->fZposition            =  target_length/2.0;     //cm
         matAlEndcap2->fZposition            = (target_length/2.0+window_thickness/2.0);     //cm

         this->AddMaterial(matHe4);
         this->AddMaterial(matAlEndcap1);
         this->AddMaterial(matAlEndcap2);
      }
      //______________________________________________________________________________
      

      LH2Target::LH2Target(const char * name, const char * title) : 
         InSANETarget(name,title)
      { 
         DefineMaterials();
      }
      //______________________________________________________________________________

      LH2Target::~LH2Target()
      { }
      //______________________________________________________________________________

      void LH2Target::DefineMaterials()
      {
         Double_t       LH2_density   = 0.07085; //g/cm^3
         Double_t       target_length = 5.0;
         auto * matLH2 = new InSANETargetMaterial("LH2","LH2",1,1);
         matLH2->fLength          = target_length;    //cm
         matLH2->fDensity         = LH2_density; // g/cm3
         matLH2->fZposition       = 0.0;     //cm

         auto * matAlEndcap1 = new InSANETargetMaterial("Al-endcap1", "Aluminum target endcap upstream", 13, 27);
         matAlEndcap1->fLength               =  0.00381 ;   //cm
         matAlEndcap1->fDensity              =  2.7;        // g/cm3
         matAlEndcap1->fZposition            = -target_length/2.0;     //cm

         auto * matAlEndcap2 = new InSANETargetMaterial("Al-endcap2", "Aluminum target endcap downstream", 13, 27);
         matAlEndcap2->fLength               =  0.00381 ;  //cm
         matAlEndcap2->fDensity              =  2.7;       // g/cm3
         matAlEndcap2->fZposition            =  target_length/2.0;     //cm

         this->AddMaterial(matLH2);
         this->AddMaterial(matAlEndcap1);
         this->AddMaterial(matAlEndcap2);

      }
      //______________________________________________________________________________

   }
}
//______________________________________________________________________________

