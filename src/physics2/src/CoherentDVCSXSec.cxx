#include "CoherentDVCSXSec.h"
#include "InSANEPhysicalConstants.h"
#include <cmath>

using namespace InSANE::physics;

CoherentDVCSXSec::CoherentDVCSXSec()
{
   fID            = 201000000;
   SetTitle("CoherentDVCSXSec");
   SetPlotTitle("DVCS cross-section");
   fLabel         = "#frac{d#sigma}{dt dx dQ^{2} d#phi d#phi_{e'} }";
   fUnits         = "nb/sr/GeV^{4}";
   fnDim          = 5;
   fPIDs.clear();
   fnParticles    = 3;
   fPIDs.push_back(11);
   fPIDs.push_back(1000020040);
   fPIDs.push_back(22);

}
//______________________________________________________________________________

CoherentDVCSXSec::~CoherentDVCSXSec()
{ }
//______________________________________________________________________________

double CoherentDVCSXSec::xsec_e1dvcs(const dvcsvar_t &var,const dvcspar_t &par) const
{
   using namespace TMath;
   //std::cout << "x.Q2" << x.Q2 << std::endl;
   //std::cout << "x.x" << x.x << std::endl;
   //std::cout << "x.t" << x.t << std::endl;
   double T1 = Power( par.Q2_0/var.Q2, par.alpha);
   double dx = (var.x - par.xc)/par.c;
   double T2 = 1.0/(1.0+dx*dx);
   double T3 = Power(1.0/(1.0+par.b*var.t), par.beta);
   double T4 = (1.0-par.d*(1.0-Cos(var.phi)));
   return( T1*T2*T3*T4 );
}
//______________________________________________________________________________

void CoherentDVCSXSec::InitializePhaseSpaceVariables()
{

   using namespace CLHEP;
   InSANEPhaseSpace * ps = GetPhaseSpace();
   if(ps) delete ps;
   ps = nullptr;
   std::cout << " Creating NEW InSANEPhaseSpace for CoherentDVCSXSec\n";
   ps = new InSANEPhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new InSANEPhaseSpaceVariable("energy_e", "E_{e'}",0.5, 5.0);
   varEnergy->SetParticleIndex(0);
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto * varTheta = new InSANEPhaseSpaceVariable("theta_e", "#theta_{e'}",5.0*degree, 180.0*degree);
   varTheta->SetParticleIndex(0);
   varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto * varPhi = new InSANEPhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree, 180.0*degree);
   varPhi->SetParticleIndex(0);
   //varPhi->SetUniform(true);
   ps->AddVariable(varPhi);

   // ------------------------------
   // struck nucleon
   auto * varEnergy_p = new InSANEPhaseSpaceVariable("P_A", "P_{A}",0.25,1.0);
   varEnergy_p->SetParticleIndex(1);
   varEnergy_p->SetDependent(true);
   ps->AddVariable(varEnergy_p);

   auto * varTheta_p = new InSANEPhaseSpaceVariable("theta_A", "#theta_{A}",30*degree, 180.0*degree);
   varTheta_p->SetParticleIndex(1);
   varTheta_p->SetDependent(true);
   ps->AddVariable(varTheta_p);

   auto * varPhi_p = new InSANEPhaseSpaceVariable("phi_A", "#phi_{A}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_p->SetParticleIndex(1);
   varPhi_p->SetDependent(true);
   ps->AddVariable(varPhi_p);

   // ------------------------------
   // photon
   auto * varEnergy_g = new InSANEPhaseSpaceVariable("energy_gamma", "E_{#gamma}",0.3,11.0);
   varEnergy_g->SetParticleIndex(2);
   varEnergy_g->SetDependent(true);
   ps->AddVariable(varEnergy_g);

   auto * varTheta_g = new InSANEPhaseSpaceVariable("theta_gamma", "#theta_{#gamma}",2.0*degree, 170.0*degree);
   varTheta_g->SetParticleIndex(2);
   varTheta_g->SetDependent(true);
   ps->AddVariable(varTheta_g);

   auto * varPhi_g = new InSANEPhaseSpaceVariable("phi_g", "#phi_{#gamma}",-1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_g->SetParticleIndex(2);
   varPhi_g->SetDependent(true);
   ps->AddVariable(varPhi_g);


   // -------------------------
   auto * var_x = new InSANEPhaseSpaceVariable("x", "x",0.02, 1.0);
   var_x->SetParticleIndex(-1);
   ps->AddVariable(var_x);

   auto * var_t = new InSANEPhaseSpaceVariable("t", "t",-2.0, 0.0);
   var_t->SetParticleIndex(-1);
   ps->AddVariable(var_t);

   auto * var_Q2 = new InSANEPhaseSpaceVariable("Q2", "Q^{2}",0.5, 6.0);
   var_Q2->SetParticleIndex(-1);
   ps->AddVariable(var_Q2);

   auto * var_phi = new InSANEPhaseSpaceVariable("phi", "#varphi",-180.0*degree, 180.0*degree);
   var_phi->SetParticleIndex(-1);
   ps->AddVariable(var_phi);

   fNuclearTargetMass = GetTargetNucleus().GetMass();

   SetPhaseSpace(ps);
}
//______________________________________________________________________________

void CoherentDVCSXSec::DefineEvent(Double_t * vars)
{

   Int_t totvars = 0;
   for (int i = 0; i < 3; i++) {
      if( (i==1) ) {
         InSANE::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      } else {
         InSANE::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      }
      //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
      totvars += GetNParticleVars(i);
   }

}
//______________________________________________________________________________

Double_t * CoherentDVCSXSec::GetDependentVariables(const Double_t * x) const
{
   using namespace TMath;

   double phi_e    =  x[0];
   double xBjorken =  x[1];
   double t        =  x[2];
   double Q2       =  x[3];
   double phi      =  x[4];

   double M       = 0.938;
   //std::cout << " M = " << M <<std::endl;
   double MT      = fNuclearTargetMass;
   double E0      = GetBeamEnergy();
   double nu      = Q2/(2.0*M*xBjorken);
   double y       = nu/E0;
   double eprime  = E0 - nu;
   double theta_e = 2.0*ASin(M*xBjorken*y/Sqrt(Q2*(1-y)));
   //std::cout << "t = " << t << std::endl;;
   if(y>1.0) return nullptr;
   if(std::isnan(theta_e)) return nullptr;

   TVector3 k1(0, 0, fBeamEnergy); // incident electron
   TVector3 k2(0, 0, eprime);          // scattered electron
   k2.SetMagThetaPhi(eprime, theta_e, phi_e);
   TVector3 q1 = k1 - k2;
   TVector3 n_gamma = k1.Cross(k2);
   n_gamma.SetMag(1.0);
   TVector3 q1_B = q1;
   q1_B.Rotate(q1.Theta(), n_gamma);
   //q1_B.Print();

   double E2_A  = (2.0*MT*MT-t)/(2.0*MT);
   double P2_A  = Sqrt(E2_A*E2_A - MT*MT);
   double nu2_A = MT + nu - E2_A;
   double cosTheta_qq2_B = (t+Q2+2.0*nu*nu2_A)/(2.0*nu2_A*q1.Mag());
   double theta_qq2_B    = ACos(cosTheta_qq2_B);
   double sintheta_qp2_B = nu2_A*Sin(theta_qq2_B)/Sqrt(E2_A*E2_A - MT*MT);
   double theta_qp2_B    = ASin(sintheta_qp2_B);
   double phi_p2 = q1_B.Phi() + phi;
   TVector3 p2 = {0,0,1} ;
   TVector3 q2 = {0,0,1} ;
   p2.SetMagThetaPhi(P2_A, theta_qp2_B,phi_p2);
   q2.SetMagThetaPhi(nu2_A,theta_qq2_B,phi_p2+180.0*degree);

   q2.Rotate(-q1.Theta(), n_gamma);
   p2.Rotate(-q1.Theta(), n_gamma);
   q1.Rotate(-q1.Theta(), n_gamma);

   fDependentVariables[0] = eprime;
   fDependentVariables[1] = k2.Theta();
   fDependentVariables[2] = k2.Phi();
   fDependentVariables[3] = P2_A;
   fDependentVariables[4] = p2.Theta();
   fDependentVariables[5] = p2.Phi();
   fDependentVariables[6] = nu2_A;
   fDependentVariables[7] = q2.Theta();
   fDependentVariables[8] = q2.Phi();
   fDependentVariables[9] = xBjorken;
   fDependentVariables[10] = t;
   fDependentVariables[11] = Q2;
   fDependentVariables[12] = phi;
   //std::cout << "vectors" << std::endl;
   //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
   //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
   //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
   //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
   //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
   //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
   //k1.Print();
   //k2.Print();
   //p2.Print();
   //if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   auto net_p = (k1 - k2 - p2 - q2);
   if( net_p.Mag() > 0.001 ) {
      //net_p.Print();
      return nullptr;
   }
   return(fDependentVariables);
}
//______________________________________________________________________________

Double_t CoherentDVCSXSec::GetEPrime(const Double_t theta)const  {
   // Returns the scattered electron energy using the angle. */
   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________

Double_t  CoherentDVCSXSec::EvaluateXSec(const Double_t * x) const
{
   if(!x) return(0.0);
   // Get the recoiling proton momentum
   if (!VariablesInPhaseSpace(9, x)) return(0.0);

   double xBjorken =  x[9];
   double t        =  x[10];
   double Q2       =  x[11];
   double phi      =  x[12];
   dvcsvar_t  vars; 
   vars.Q2 = Q2;
   vars.t = t;
   vars.x = xBjorken;
   vars.phi = phi;
   double norm = 0.12;
   //double norm = xsec_normalization();
   //std::cout << " Norm: " << norm << std::endl;
   //
   double sig0 = norm*xsec_e1dvcs(vars, PAR_COHDVCS);
   //res = res * hbarc2_gev_nb;

   double A       = GetBeamSpinAsymmetry(x);
   double hel     = GetHelicity();

   double sig_plus  = sig0*(1.0 + hel*A)/2.0;
   double sig_minus = sig0*(1.0 - hel*A)/2.0;

   double jac = Jacobian(x);

   if(jac < 0 ) {
      return 0.0;
   }

   return( jac*(sig_plus + sig_minus) );
}
//______________________________________________________________________________

Double_t   CoherentDVCSXSec::GetBeamSpinAsymmetry(const Double_t * x) const
{
   double xBjorken =  x[9];
   double t        =  x[10];
   double Q2       =  x[11];
   double phi      =  x[12];
   return( falpha*TMath::Sin(phi)/(1.0+fbeta*TMath::Cos(phi)));
}
//______________________________________________________________________________

double CoherentDVCSXSec::Jacobian(const double * vars) const
{
   using namespace TMath;
   auto Csc = [](double x){ return 1.0/TMath::Sin(x) ; };
   auto ArcCos = [](double x){ return TMath::ACos(x) ; };
   double M = fTargetNucleus.GetMass();
   double k1 = fBeamEnergy;

   double k2      = vars[0];
   double thetak2 = vars[1];
   double phik2   = vars[2];

   double q2      = vars[6];
   double thetaq2 = vars[7];
   double phiq2   = vars[8];

   double res = 
(4.*Power(k1,2)*Power(k2,3)*(1. - 1.*Cos(thetak2))*(k1*(k2 - M) + \
k2*M - k1*k2*Cos(thetak2))*Power(Sin(thetak2),2)*Sin(thetaq2)*Power(\
k1 - k2 + M - k1*Cos(thetaq2) + k2*Cos(thetak2)*Cos(thetaq2) + \
k2*Cos(phik2 - \
phiq2)*Sin(thetak2)*Sin(thetaq2),2)*(k2*Power(Sin(phik2 - \
phiq2),2)*Sin(thetak2)*(Power(k1,2) - 2*k1*k2*Cos(thetak2) + \
Power(k2,2)*Power(Cos(thetak2),2) + \
Power(k2,2)*Power(Sin(thetak2),2))*Power(Sin(thetaq2),2) + \
ArcCos((k2*Sin(thetak2)*(k2*Cos(thetaq2)*Sin(thetak2) + Cos(phik2 - \
phiq2)*(k1 - k2*Cos(thetak2))*Sin(thetaq2)))/(Power(k1,2) + \
Power(k2,2) - 2*k1*k2*Cos(thetak2)))*(k2*Power(Cos(phik2 - \
phiq2),2)*Cos(thetaq2)*Sin(thetak2) + k2*Cos(thetaq2)*Power(Sin(phik2 \
- phiq2),2)*Sin(thetak2) + Cos(phik2 - phiq2)*(k1 - \
k2*Cos(thetak2))*Sin(thetaq2))*Sqrt(Power(k1,4) + \
2*Power(k1,2)*Power(k2,2) + Power(k2,4) - \
Power(k2,4)*Power(Cos(thetaq2),2)*Power(Sin(thetak2),4) - \
2*k1*Power(k2,3)*Cos(phik2 - \
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) - \
Power(k1,2)*Power(k2,2)*Power(Cos(phik2 - \
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) + \
Cos(thetak2)*(-4*k1*k2*(Power(k1,2) + Power(k2,2)) + \
2*Power(k2,4)*Cos(phik2 - \
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) + \
2*k1*Power(k2,3)*Power(Cos(phik2 - \
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2)) + \
Power(Cos(thetak2),2)*(4*Power(k1,2)*Power(k2,2) - \
Power(k2,4)*Power(Cos(phik2 - \
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2)))))/(Power(k1 - \
1.*k2,2)*Power(Power(k1,2) + Power(k2,2) - \
2*k1*k2*Cos(thetak2),1.5)*Power(-1.*k1 + k2 - 1.*M + (k1 - \
1.*k2*Cos(thetak2))*Cos(thetaq2) - 1.*k2*Cos(phik2 - \
phiq2)*Sin(thetak2)*Sin(thetaq2),4)*Sqrt(1 - \
(Power(k2,2)*Power(Sin(thetak2),2)*Power(k2*Cos(thetaq2)*Sin(thetak2) \
+ Cos(phik2 - phiq2)*(k1 - \
k2*Cos(thetak2))*Sin(thetaq2),2))/Power(Power(k1,2) + Power(k2,2) - \
2*k1*k2*Cos(thetak2),2)));
   return res;
}
//______________________________________________________________________________



