#include "NNParaElectronXYCorrectionClusterDeltaX.h"
#include <cmath>

double NNParaElectronXYCorrectionClusterDeltaX::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10) {
   input0 = (in0 - 1931.89)/1024.17;
   input1 = (in1 - -3.92757)/33.4126;
   input2 = (in2 - -1.75708)/60.7242;
   input3 = (in3 - 1.30402)/0.374511;
   input4 = (in4 - 1.38855)/0.399413;
   input5 = (in5 - 0.279299)/1.99665;
   input6 = (in6 - 0.276484)/1.83307;
   input7 = (in7 - 6.6926)/10.2788;
   input8 = (in8 - 5.61334)/10.0137;
   input9 = (in9 - 0.00823462)/0.746842;
   input10 = (in10 - -0.000800713)/0.755077;
   switch(index) {
     case 0:
         return neuron0x8d39780();
     default:
         return 0.;
   }
}

double NNParaElectronXYCorrectionClusterDeltaX::Value(int index, double* input) {
   input0 = (input[0] - 1931.89)/1024.17;
   input1 = (input[1] - -3.92757)/33.4126;
   input2 = (input[2] - -1.75708)/60.7242;
   input3 = (input[3] - 1.30402)/0.374511;
   input4 = (input[4] - 1.38855)/0.399413;
   input5 = (input[5] - 0.279299)/1.99665;
   input6 = (input[6] - 0.276484)/1.83307;
   input7 = (input[7] - 6.6926)/10.2788;
   input8 = (input[8] - 5.61334)/10.0137;
   input9 = (input[9] - 0.00823462)/0.746842;
   input10 = (input[10] - -0.000800713)/0.755077;
   switch(index) {
     case 0:
         return neuron0x8d39780();
     default:
         return 0.;
   }
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1c360() {
   return input0;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1c4d0() {
   return input1;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1c6d0() {
   return input2;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1c8d0() {
   return input3;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1cad0() {
   return input4;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1ccd0() {
   return input5;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1cee8() {
   return input6;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1d100() {
   return input7;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1d318() {
   return input8;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1d530() {
   return input9;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1d730() {
   return input10;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1da70() {
   double input = -0.412151;
   input += synapse0x8bef198();
   input += synapse0x8d1dc28();
   input += synapse0x8d1dc50();
   input += synapse0x8d1dc78();
   input += synapse0x8d1dca0();
   input += synapse0x8d1dcc8();
   input += synapse0x8d1dcf0();
   input += synapse0x8d1dd18();
   input += synapse0x8d1dd40();
   input += synapse0x8d1dd68();
   input += synapse0x8d1dd90();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1da70() {
   double input = input0x8d1da70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1ddb8() {
   double input = -0.102263;
   input += synapse0x8d1dfb8();
   input += synapse0x8d1dfe0();
   input += synapse0x8d1e008();
   input += synapse0x8d1e030();
   input += synapse0x8d1e058();
   input += synapse0x8d1e080();
   input += synapse0x8d1e130();
   input += synapse0x8d1e158();
   input += synapse0x8d1e180();
   input += synapse0x8d1e1a8();
   input += synapse0x8d1e1d0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1ddb8() {
   double input = input0x8d1ddb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1e1f8() {
   double input = 0.0117853;
   input += synapse0x8d1e3b0();
   input += synapse0x8d1e3d8();
   input += synapse0x8d1e400();
   input += synapse0x8d1e428();
   input += synapse0x8d1e450();
   input += synapse0x8d1e478();
   input += synapse0x8d1e4a0();
   input += synapse0x8d1e4c8();
   input += synapse0x8d1e4f0();
   input += synapse0x8d1e518();
   input += synapse0x8d1e540();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1e1f8() {
   double input = input0x8d1e1f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1e568() {
   double input = -0.50516;
   input += synapse0x8d1e768();
   input += synapse0x8d1e790();
   input += synapse0x8d1e7b8();
   input += synapse0x8d1e7e0();
   input += synapse0x8d1e808();
   input += synapse0x8d1e830();
   input += synapse0x8d1e858();
   input += synapse0x8d1e880();
   input += synapse0x8d1e8a8();
   input += synapse0x8d1e8d0();
   input += synapse0x8d1e8f8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1e568() {
   double input = input0x8d1e568();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1e920() {
   double input = 0.407093;
   input += synapse0x8d1eb20();
   input += synapse0x8d1eb48();
   input += synapse0x8d1eb70();
   input += synapse0x8d1eb98();
   input += synapse0x8d1ebc0();
   input += synapse0x8d1ebe8();
   input += synapse0x8d1ec10();
   input += synapse0x8d1ec38();
   input += synapse0x8d1ec60();
   input += synapse0x8d1ec88();
   input += synapse0x8d1ecb0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1e920() {
   double input = input0x8d1e920();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1ecd8() {
   double input = 0.430218;
   input += synapse0x8d1eed8();
   input += synapse0x8d1ef00();
   input += synapse0x8d1ef28();
   input += synapse0x8d1ef50();
   input += synapse0x8d1ef78();
   input += synapse0x8d1efa0();
   input += synapse0x8d1efc8();
   input += synapse0x8d1eff0();
   input += synapse0x8d1f018();
   input += synapse0x8d1f040();
   input += synapse0x8bec9c0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1ecd8() {
   double input = input0x8d1ecd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1f270() {
   double input = 0.692486;
   input += synapse0x8d1f428();
   input += synapse0x8d1f450();
   input += synapse0x8d1f478();
   input += synapse0x8d1f4a0();
   input += synapse0x8d1f4c8();
   input += synapse0x8d1f4f0();
   input += synapse0x8d1f518();
   input += synapse0x8d1f540();
   input += synapse0x8d1f568();
   input += synapse0x8d1f590();
   input += synapse0x8d1f5b8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1f270() {
   double input = input0x8d1f270();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1f5e0() {
   double input = 0.988067;
   input += synapse0x8d1f7f8();
   input += synapse0x8d1f820();
   input += synapse0x8d1f848();
   input += synapse0x8d1f870();
   input += synapse0x8d1f898();
   input += synapse0x8d1f8c0();
   input += synapse0x8d1f8e8();
   input += synapse0x8d1f910();
   input += synapse0x8d1f938();
   input += synapse0x8d1f960();
   input += synapse0x8d1f988();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1f5e0() {
   double input = input0x8d1f5e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1f9b0() {
   double input = -0.453519;
   input += synapse0x8d1fbc8();
   input += synapse0x8d1fbf0();
   input += synapse0x8d1fc18();
   input += synapse0x8d1fc40();
   input += synapse0x8d1fc68();
   input += synapse0x8d1fc90();
   input += synapse0x8d1fcb8();
   input += synapse0x8d1fce0();
   input += synapse0x8d1fd08();
   input += synapse0x8d1fd30();
   input += synapse0x8d1fd58();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1f9b0() {
   double input = input0x8d1f9b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1fd80() {
   double input = -0.0162174;
   input += synapse0x8d1ff98();
   input += synapse0x8d1ffc0();
   input += synapse0x8d1ffe8();
   input += synapse0x8d20010();
   input += synapse0x8d20038();
   input += synapse0x8d20060();
   input += synapse0x8d20088();
   input += synapse0x8d200b0();
   input += synapse0x8d200d8();
   input += synapse0x8d20100();
   input += synapse0x8d20128();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1fd80() {
   double input = input0x8d1fd80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d20150() {
   double input = 1.68638;
   input += synapse0x8d20368();
   input += synapse0x8d20390();
   input += synapse0x8d203b8();
   input += synapse0x8d203e0();
   input += synapse0x8d20408();
   input += synapse0x8d20430();
   input += synapse0x8d20458();
   input += synapse0x8d20480();
   input += synapse0x8d204a8();
   input += synapse0x8d204d0();
   input += synapse0x8d204f8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d20150() {
   double input = input0x8d20150();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d20520() {
   double input = -0.104439;
   input += synapse0x8d20738();
   input += synapse0x8d20760();
   input += synapse0x8d20788();
   input += synapse0x8d207b0();
   input += synapse0x8d207d8();
   input += synapse0x8d20800();
   input += synapse0x8d20828();
   input += synapse0x8d20850();
   input += synapse0x8cfc5c0();
   input += synapse0x8a387e8();
   input += synapse0x8bef0c8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d20520() {
   double input = input0x8d20520();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d1f068() {
   double input = 0.404643;
   input += synapse0x8bef0f0();
   input += synapse0x8bef118();
   input += synapse0x8bef140();
   input += synapse0x8bef168();
   input += synapse0x8cd69c0();
   input += synapse0x8cd69e8();
   input += synapse0x8cd6a10();
   input += synapse0x8cd6a38();
   input += synapse0x8cd6a60();
   input += synapse0x8cd6a88();
   input += synapse0x8cfc458();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d1f068() {
   double input = input0x8d1f068();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d20c80() {
   double input = 0.111002;
   input += synapse0x8d20e80();
   input += synapse0x8d20ea8();
   input += synapse0x8d20ed0();
   input += synapse0x8d20ef8();
   input += synapse0x8d20f20();
   input += synapse0x8d20f48();
   input += synapse0x8d20f70();
   input += synapse0x8d20f98();
   input += synapse0x8d20fc0();
   input += synapse0x8d20fe8();
   input += synapse0x8d21010();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d20c80() {
   double input = input0x8d20c80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d21038() {
   double input = 1.7041;
   input += synapse0x8d21238();
   input += synapse0x8d21260();
   input += synapse0x8d21288();
   input += synapse0x8d212b0();
   input += synapse0x8d212d8();
   input += synapse0x8d21300();
   input += synapse0x8d21328();
   input += synapse0x8d21350();
   input += synapse0x8d21378();
   input += synapse0x8d213a0();
   input += synapse0x8d213c8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d21038() {
   double input = input0x8d21038();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d213f0() {
   double input = 0.492529;
   input += synapse0x8d21608();
   input += synapse0x8d216b8();
   input += synapse0x8d21768();
   input += synapse0x8d21818();
   input += synapse0x8d218c8();
   input += synapse0x8d21978();
   input += synapse0x8d21a28();
   input += synapse0x8d21ad8();
   input += synapse0x8d21b88();
   input += synapse0x8d21c38();
   input += synapse0x8d21ce8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d213f0() {
   double input = input0x8d213f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d21d98() {
   double input = -0.43312;
   input += synapse0x8d21ed8();
   input += synapse0x8d21f00();
   input += synapse0x8d21f28();
   input += synapse0x8d21f50();
   input += synapse0x8d21f78();
   input += synapse0x8d21fa0();
   input += synapse0x8d21fc8();
   input += synapse0x8d21ff0();
   input += synapse0x8d22018();
   input += synapse0x8d22040();
   input += synapse0x8d22068();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d21d98() {
   double input = input0x8d21d98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d230d8() {
   double input = 0.35252;
   input += synapse0x8d22090();
   input += synapse0x8d220b8();
   input += synapse0x8d220e0();
   input += synapse0x8d22108();
   input += synapse0x8cfc480();
   input += synapse0x8cfc4a8();
   input += synapse0x8cfc4d0();
   input += synapse0x8cfc4f8();
   input += synapse0x8cfc520();
   input += synapse0x8d23200();
   input += synapse0x8d23228();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d230d8() {
   double input = input0x8d230d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d23250() {
   double input = -0.14587;
   input += synapse0x8d23378();
   input += synapse0x8d233a0();
   input += synapse0x8d233c8();
   input += synapse0x8d233f0();
   input += synapse0x8d23418();
   input += synapse0x8d23440();
   input += synapse0x8d23468();
   input += synapse0x8d23490();
   input += synapse0x8d234b8();
   input += synapse0x8d234e0();
   input += synapse0x8d23508();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d23250() {
   double input = input0x8d23250();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d23530() {
   double input = -0.176724;
   input += synapse0x8d236a0();
   input += synapse0x8d236c8();
   input += synapse0x8d236f0();
   input += synapse0x8d23718();
   input += synapse0x8d23740();
   input += synapse0x8d23768();
   input += synapse0x8d23790();
   input += synapse0x8d237b8();
   input += synapse0x8d237e0();
   input += synapse0x8d23808();
   input += synapse0x8d23830();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d23530() {
   double input = input0x8d23530();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d23858() {
   double input = -0.419817;
   input += synapse0x8d23a58();
   input += synapse0x8d23a80();
   input += synapse0x8d23aa8();
   input += synapse0x8d23ad0();
   input += synapse0x8d23af8();
   input += synapse0x8d23b20();
   input += synapse0x8d23b48();
   input += synapse0x8d23b70();
   input += synapse0x8d23b98();
   input += synapse0x8d23bc0();
   input += synapse0x8d23be8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d23858() {
   double input = input0x8d23858();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d23c10() {
   double input = -0.438443;
   input += synapse0x8d1e0a8();
   input += synapse0x8d1e0d0();
   input += synapse0x8d1e0f8();
   input += synapse0x8d23f18();
   input += synapse0x8d23f40();
   input += synapse0x8d23f68();
   input += synapse0x8d23f90();
   input += synapse0x8d23fb8();
   input += synapse0x8d23fe0();
   input += synapse0x8d24008();
   input += synapse0x8d24030();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d23c10() {
   double input = input0x8d23c10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d24058() {
   double input = 0.0492215;
   input += synapse0x8d24258();
   input += synapse0x8d24280();
   input += synapse0x8d242a8();
   input += synapse0x8d242d0();
   input += synapse0x8d242f8();
   input += synapse0x8d24320();
   input += synapse0x8d24348();
   input += synapse0x8d24370();
   input += synapse0x8d24398();
   input += synapse0x8d243c0();
   input += synapse0x8d243e8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d24058() {
   double input = input0x8d24058();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d24410() {
   double input = 0.0633938;
   input += synapse0x8d24610();
   input += synapse0x8d24638();
   input += synapse0x8d24660();
   input += synapse0x8d24688();
   input += synapse0x8d20878();
   input += synapse0x8d208a0();
   input += synapse0x8d208c8();
   input += synapse0x8d208f0();
   input += synapse0x8d20918();
   input += synapse0x8d20940();
   input += synapse0x8d20968();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d24410() {
   double input = input0x8d24410();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d20990() {
   double input = -0.0833499;
   input += synapse0x8d20ba8();
   input += synapse0x8d20bd0();
   input += synapse0x8d20bf8();
   input += synapse0x8d20c20();
   input += synapse0x8d20c48();
   input += synapse0x8d24eb8();
   input += synapse0x8d24ee0();
   input += synapse0x8d24f08();
   input += synapse0x8d24f30();
   input += synapse0x8d24f58();
   input += synapse0x8d24f80();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d20990() {
   double input = input0x8d20990();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d24fa8() {
   double input = -0.416343;
   input += synapse0x8d251c0();
   input += synapse0x8d251e8();
   input += synapse0x8d25210();
   input += synapse0x8d25238();
   input += synapse0x8d25260();
   input += synapse0x8d25288();
   input += synapse0x8d252b0();
   input += synapse0x8d252d8();
   input += synapse0x8d25300();
   input += synapse0x8d25328();
   input += synapse0x8d25350();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d24fa8() {
   double input = input0x8d24fa8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d25378() {
   double input = 0.463759;
   input += synapse0x8d25590();
   input += synapse0x8d255b8();
   input += synapse0x8d255e0();
   input += synapse0x8d25608();
   input += synapse0x8d25630();
   input += synapse0x8d25658();
   input += synapse0x8d25680();
   input += synapse0x8d256a8();
   input += synapse0x8d256d0();
   input += synapse0x8d256f8();
   input += synapse0x8d25720();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d25378() {
   double input = input0x8d25378();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d25748() {
   double input = -0.13948;
   input += synapse0x8d25960();
   input += synapse0x8d25988();
   input += synapse0x8d259b0();
   input += synapse0x8d259d8();
   input += synapse0x8d25a00();
   input += synapse0x8d25a28();
   input += synapse0x8d25a50();
   input += synapse0x8d25a78();
   input += synapse0x8d25aa0();
   input += synapse0x8d25ac8();
   input += synapse0x8d25af0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d25748() {
   double input = input0x8d25748();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d25b18() {
   double input = 0.23565;
   input += synapse0x8d25d30();
   input += synapse0x8d25d58();
   input += synapse0x8d25d80();
   input += synapse0x8d25da8();
   input += synapse0x8d25dd0();
   input += synapse0x8d25df8();
   input += synapse0x8d25e20();
   input += synapse0x8d25e48();
   input += synapse0x8d25e70();
   input += synapse0x8d25e98();
   input += synapse0x8d25ec0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d25b18() {
   double input = input0x8d25b18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d25ee8() {
   double input = 0.243536;
   input += synapse0x8d26100();
   input += synapse0x8d26128();
   input += synapse0x8d26150();
   input += synapse0x8d26178();
   input += synapse0x8d261a0();
   input += synapse0x8d261c8();
   input += synapse0x8d261f0();
   input += synapse0x8d26218();
   input += synapse0x8d26240();
   input += synapse0x8d26268();
   input += synapse0x8d26290();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d25ee8() {
   double input = input0x8d25ee8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d262b8() {
   double input = -0.247043;
   input += synapse0x8d264d0();
   input += synapse0x8d264f8();
   input += synapse0x8d26520();
   input += synapse0x8d26548();
   input += synapse0x8d26570();
   input += synapse0x8d26598();
   input += synapse0x8d265c0();
   input += synapse0x8d265e8();
   input += synapse0x8d26610();
   input += synapse0x8d26638();
   input += synapse0x8d26660();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d262b8() {
   double input = input0x8d262b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d26688() {
   double input = 0.00232987;
   input += synapse0x8d268a0();
   input += synapse0x8d21630();
   input += synapse0x8d21658();
   input += synapse0x8d21680();
   input += synapse0x8d216e0();
   input += synapse0x8d21708();
   input += synapse0x8d21730();
   input += synapse0x8d21790();
   input += synapse0x8d217b8();
   input += synapse0x8d217e0();
   input += synapse0x8d21840();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d26688() {
   double input = input0x8d26688();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d27420() {
   double input = -2.12071;
   input += synapse0x8d219e8();
   input += synapse0x8d21938();
   input += synapse0x8d21a50();
   input += synapse0x8d21a78();
   input += synapse0x8d21aa0();
   input += synapse0x8d21b00();
   input += synapse0x8d21b28();
   input += synapse0x8d21b50();
   input += synapse0x8d21bb0();
   input += synapse0x8d21bd8();
   input += synapse0x8d21c00();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d27420() {
   double input = input0x8d27420();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d27548() {
   double input = -0.229781;
   input += synapse0x8d21ca8();
   input += synapse0x8d21d58();
   input += synapse0x8d276b8();
   input += synapse0x8d276e0();
   input += synapse0x8d27708();
   input += synapse0x8d27730();
   input += synapse0x8d27758();
   input += synapse0x8d27780();
   input += synapse0x8d277a8();
   input += synapse0x8d277d0();
   input += synapse0x8d277f8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d27548() {
   double input = input0x8d27548();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d27820() {
   double input = -0.519616;
   input += synapse0x8d27a20();
   input += synapse0x8d27a48();
   input += synapse0x8d27a70();
   input += synapse0x8d27a98();
   input += synapse0x8d27ac0();
   input += synapse0x8d27ae8();
   input += synapse0x8d27b10();
   input += synapse0x8d27b38();
   input += synapse0x8d27b60();
   input += synapse0x8d27b88();
   input += synapse0x8d27bb0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d27820() {
   double input = input0x8d27820();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d27bd8() {
   double input = 0.685293;
   input += synapse0x8d27dd8();
   input += synapse0x8d27e00();
   input += synapse0x8d27e28();
   input += synapse0x8d27e50();
   input += synapse0x8d27e78();
   input += synapse0x8d27ea0();
   input += synapse0x8d27ec8();
   input += synapse0x8d27ef0();
   input += synapse0x8d27f18();
   input += synapse0x8d27f40();
   input += synapse0x8d27f68();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d27bd8() {
   double input = input0x8d27bd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d27f90() {
   double input = 0.404215;
   input += synapse0x8d28190();
   input += synapse0x8d281b8();
   input += synapse0x8d281e0();
   input += synapse0x8d28208();
   input += synapse0x8d28230();
   input += synapse0x8d28258();
   input += synapse0x8d28280();
   input += synapse0x8d282a8();
   input += synapse0x8d282d0();
   input += synapse0x8d282f8();
   input += synapse0x8d28320();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d27f90() {
   double input = input0x8d27f90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d28348() {
   double input = 0.235765;
   input += synapse0x8d28560();
   input += synapse0x8d28588();
   input += synapse0x8d285b0();
   input += synapse0x8d285d8();
   input += synapse0x8d28600();
   input += synapse0x8d28628();
   input += synapse0x8d28650();
   input += synapse0x8d28678();
   input += synapse0x8d286a0();
   input += synapse0x8d286c8();
   input += synapse0x8d286f0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d28348() {
   double input = input0x8d28348();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d28718() {
   double input = -0.156332;
   input += synapse0x8d28930();
   input += synapse0x8d28958();
   input += synapse0x8d28980();
   input += synapse0x8d289a8();
   input += synapse0x8d289d0();
   input += synapse0x8d289f8();
   input += synapse0x8d28a20();
   input += synapse0x8d28a48();
   input += synapse0x8d28a70();
   input += synapse0x8d28a98();
   input += synapse0x8d28ac0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d28718() {
   double input = input0x8d28718();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d28ae8() {
   double input = 0.317425;
   input += synapse0x8d28d00();
   input += synapse0x8d28d28();
   input += synapse0x8d28d50();
   input += synapse0x8d28d78();
   input += synapse0x8d28da0();
   input += synapse0x8d28dc8();
   input += synapse0x8d28df0();
   input += synapse0x8d28e18();
   input += synapse0x8d28e40();
   input += synapse0x8d28e68();
   input += synapse0x8d28e90();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d28ae8() {
   double input = input0x8d28ae8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d28eb8() {
   double input = 0.0825042;
   input += synapse0x8d290d0();
   input += synapse0x8d290f8();
   input += synapse0x8d29120();
   input += synapse0x8d29148();
   input += synapse0x8d29170();
   input += synapse0x8d29198();
   input += synapse0x8d291c0();
   input += synapse0x8d291e8();
   input += synapse0x8d29210();
   input += synapse0x8d29238();
   input += synapse0x8d29260();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d28eb8() {
   double input = input0x8d28eb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d29288() {
   double input = 0.768599;
   input += synapse0x8d294a0();
   input += synapse0x8d294c8();
   input += synapse0x8d294f0();
   input += synapse0x8d29518();
   input += synapse0x8d29540();
   input += synapse0x8d29568();
   input += synapse0x8d29590();
   input += synapse0x8d295b8();
   input += synapse0x8d295e0();
   input += synapse0x8d29608();
   input += synapse0x8d29630();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d29288() {
   double input = input0x8d29288();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d29658() {
   double input = -0.516212;
   input += synapse0x8d29870();
   input += synapse0x8d29898();
   input += synapse0x8d298c0();
   input += synapse0x8d298e8();
   input += synapse0x8d29910();
   input += synapse0x8d29938();
   input += synapse0x8d29960();
   input += synapse0x8d29988();
   input += synapse0x8d299b0();
   input += synapse0x8d299d8();
   input += synapse0x8d29a00();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d29658() {
   double input = input0x8d29658();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d29a28() {
   double input = 0.672699;
   input += synapse0x8d29c40();
   input += synapse0x8d29c68();
   input += synapse0x8d29c90();
   input += synapse0x8d29cb8();
   input += synapse0x8d29ce0();
   input += synapse0x8d29d08();
   input += synapse0x8d29d30();
   input += synapse0x8d29d58();
   input += synapse0x8d29d80();
   input += synapse0x8d29da8();
   input += synapse0x8d29dd0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d29a28() {
   double input = input0x8d29a28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d29df8() {
   double input = -0.157624;
   input += synapse0x8d2a010();
   input += synapse0x8d2a038();
   input += synapse0x8d2a060();
   input += synapse0x8d2a088();
   input += synapse0x8d2a0b0();
   input += synapse0x8d2a0d8();
   input += synapse0x8d2a100();
   input += synapse0x8d2a128();
   input += synapse0x8d2a150();
   input += synapse0x8d2a178();
   input += synapse0x8d2a1a0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d29df8() {
   double input = input0x8d29df8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2a1c8() {
   double input = -0.00645487;
   input += synapse0x8d2a3e0();
   input += synapse0x8d2a408();
   input += synapse0x8d2a430();
   input += synapse0x8d2a458();
   input += synapse0x8d2a480();
   input += synapse0x8d2a4a8();
   input += synapse0x8d2a4d0();
   input += synapse0x8d2a4f8();
   input += synapse0x8d2a520();
   input += synapse0x8d2a548();
   input += synapse0x8d2a570();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2a1c8() {
   double input = input0x8d2a1c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2a598() {
   double input = -0.594502;
   input += synapse0x8d2a7b0();
   input += synapse0x8d2a7d8();
   input += synapse0x8d2a800();
   input += synapse0x8d2a828();
   input += synapse0x8d2a850();
   input += synapse0x8d2a878();
   input += synapse0x8d2a8a0();
   input += synapse0x8d246b0();
   input += synapse0x8d246d8();
   input += synapse0x8d24700();
   input += synapse0x8d24728();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2a598() {
   double input = input0x8d2a598();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d24750() {
   double input = 0.0636253;
   input += synapse0x8d24968();
   input += synapse0x8d24990();
   input += synapse0x8d249b8();
   input += synapse0x8d249e0();
   input += synapse0x8d24a08();
   input += synapse0x8d24a30();
   input += synapse0x8d24a58();
   input += synapse0x8d24a80();
   input += synapse0x8d24aa8();
   input += synapse0x8d24ad0();
   input += synapse0x8d24af8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d24750() {
   double input = input0x8d24750();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d24b20() {
   double input = 0.0956862;
   input += synapse0x8d24d38();
   input += synapse0x8d24d60();
   input += synapse0x8d24d88();
   input += synapse0x8d24db0();
   input += synapse0x8d24dd8();
   input += synapse0x8d24e00();
   input += synapse0x8d24e28();
   input += synapse0x8d24e50();
   input += synapse0x8d24e78();
   input += synapse0x8d2b8d0();
   input += synapse0x8d2b8f8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d24b20() {
   double input = input0x8d24b20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2b920() {
   double input = -0.330843;
   input += synapse0x8d2bb20();
   input += synapse0x8d2bb48();
   input += synapse0x8d2bb70();
   input += synapse0x8d2bb98();
   input += synapse0x8d2bbc0();
   input += synapse0x8d2bbe8();
   input += synapse0x8d2bc10();
   input += synapse0x8d2bc38();
   input += synapse0x8d2bc60();
   input += synapse0x8d2bc88();
   input += synapse0x8d2bcb0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2b920() {
   double input = input0x8d2b920();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2bcd8() {
   double input = 0.340458;
   input += synapse0x8d2bef0();
   input += synapse0x8d2bf18();
   input += synapse0x8d2bf40();
   input += synapse0x8d2bf68();
   input += synapse0x8d2bf90();
   input += synapse0x8d2bfb8();
   input += synapse0x8d2bfe0();
   input += synapse0x8d2c008();
   input += synapse0x8d2c030();
   input += synapse0x8d2c058();
   input += synapse0x8d2c080();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2bcd8() {
   double input = input0x8d2bcd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2c0a8() {
   double input = -0.537698;
   input += synapse0x8d2c2c0();
   input += synapse0x8d2c2e8();
   input += synapse0x8d2c310();
   input += synapse0x8d2c338();
   input += synapse0x8d2c360();
   input += synapse0x8d2c388();
   input += synapse0x8d2c3b0();
   input += synapse0x8d2c3d8();
   input += synapse0x8d2c400();
   input += synapse0x8d2c428();
   input += synapse0x8d2c450();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2c0a8() {
   double input = input0x8d2c0a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2c478() {
   double input = 0.268058;
   input += synapse0x8d2c690();
   input += synapse0x8d2c6b8();
   input += synapse0x8d2c6e0();
   input += synapse0x8d2c708();
   input += synapse0x8d2c730();
   input += synapse0x8d2c758();
   input += synapse0x8d2c780();
   input += synapse0x8d2c7a8();
   input += synapse0x8d2c7d0();
   input += synapse0x8d2c7f8();
   input += synapse0x8d2c820();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2c478() {
   double input = input0x8d2c478();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2c848() {
   double input = 0.58458;
   input += synapse0x8d23e10();
   input += synapse0x8d23e38();
   input += synapse0x8d23e60();
   input += synapse0x8d23e88();
   input += synapse0x8d23eb0();
   input += synapse0x8d23ed8();
   input += synapse0x8d2cc68();
   input += synapse0x8d2cc90();
   input += synapse0x8d2ccb8();
   input += synapse0x8d2cce0();
   input += synapse0x8d2cd08();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2c848() {
   double input = input0x8d2c848();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2cd30() {
   double input = 0.137601;
   input += synapse0x8d2cf30();
   input += synapse0x8d2cf58();
   input += synapse0x8d2cf80();
   input += synapse0x8d2cfa8();
   input += synapse0x8d2cfd0();
   input += synapse0x8d2cff8();
   input += synapse0x8d2d020();
   input += synapse0x8d2d048();
   input += synapse0x8d2d070();
   input += synapse0x8d2d098();
   input += synapse0x8d2d0c0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2cd30() {
   double input = input0x8d2cd30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2d0e8() {
   double input = -0.277581;
   input += synapse0x8d2d300();
   input += synapse0x8d2d328();
   input += synapse0x8d2d350();
   input += synapse0x8d2d378();
   input += synapse0x8d2d3a0();
   input += synapse0x8d2d3c8();
   input += synapse0x8d2d3f0();
   input += synapse0x8d2d418();
   input += synapse0x8d2d440();
   input += synapse0x8d2d468();
   input += synapse0x8d2d490();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2d0e8() {
   double input = input0x8d2d0e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2d4b8() {
   double input = -0.976485;
   input += synapse0x8d2d6d0();
   input += synapse0x8d2d6f8();
   input += synapse0x8d2d720();
   input += synapse0x8d2d748();
   input += synapse0x8d2d770();
   input += synapse0x8d2d798();
   input += synapse0x8d2d7c0();
   input += synapse0x8d2d7e8();
   input += synapse0x8d2d810();
   input += synapse0x8d2d838();
   input += synapse0x8d2d860();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2d4b8() {
   double input = input0x8d2d4b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2d888() {
   double input = 0.761593;
   input += synapse0x8d2daa0();
   input += synapse0x8d2dac8();
   input += synapse0x8d2daf0();
   input += synapse0x8d2db18();
   input += synapse0x8d2db40();
   input += synapse0x8d2db68();
   input += synapse0x8d2db90();
   input += synapse0x8d2dbb8();
   input += synapse0x8d2dbe0();
   input += synapse0x8d2dc08();
   input += synapse0x8d2dc30();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2d888() {
   double input = input0x8d2d888();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2dc58() {
   double input = 0.268285;
   input += synapse0x8d2de70();
   input += synapse0x8d2de98();
   input += synapse0x8d2dec0();
   input += synapse0x8d2dee8();
   input += synapse0x8d2df10();
   input += synapse0x8d2df38();
   input += synapse0x8d2df60();
   input += synapse0x8d2df88();
   input += synapse0x8d2dfb0();
   input += synapse0x8d2dfd8();
   input += synapse0x8d2e000();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2dc58() {
   double input = input0x8d2dc58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2e028() {
   double input = 0.435647;
   input += synapse0x8d2e240();
   input += synapse0x8d2e268();
   input += synapse0x8d2e290();
   input += synapse0x8d2e2b8();
   input += synapse0x8d2e2e0();
   input += synapse0x8d2e308();
   input += synapse0x8d2e330();
   input += synapse0x8d2e358();
   input += synapse0x8d2e380();
   input += synapse0x8d2e3a8();
   input += synapse0x8d2e3d0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2e028() {
   double input = input0x8d2e028();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2e3f8() {
   double input = -0.158631;
   input += synapse0x8d2e610();
   input += synapse0x8d2e638();
   input += synapse0x8d2e660();
   input += synapse0x8d2e688();
   input += synapse0x8d2e6b0();
   input += synapse0x8d2e6d8();
   input += synapse0x8d2e700();
   input += synapse0x8d2e728();
   input += synapse0x8d2e750();
   input += synapse0x8d2e778();
   input += synapse0x8d2e7a0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2e3f8() {
   double input = input0x8d2e3f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2e7c8() {
   double input = -0.160997;
   input += synapse0x8d2e9e0();
   input += synapse0x8d2ea08();
   input += synapse0x8d2ea30();
   input += synapse0x8d2ea58();
   input += synapse0x8d2ea80();
   input += synapse0x8d2eaa8();
   input += synapse0x8d2ead0();
   input += synapse0x8d2eaf8();
   input += synapse0x8d2eb20();
   input += synapse0x8d2eb48();
   input += synapse0x8d2eb70();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2e7c8() {
   double input = input0x8d2e7c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2eb98() {
   double input = 0.445235;
   input += synapse0x8d2edb0();
   input += synapse0x8d2edd8();
   input += synapse0x8d2ee00();
   input += synapse0x8d2ee28();
   input += synapse0x8d2ee50();
   input += synapse0x8d2ee78();
   input += synapse0x8d2eea0();
   input += synapse0x8d2eec8();
   input += synapse0x8d2eef0();
   input += synapse0x8d2ef18();
   input += synapse0x8d2ef40();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2eb98() {
   double input = input0x8d2eb98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2ef68() {
   double input = -0.671454;
   input += synapse0x8d2f180();
   input += synapse0x8d268c8();
   input += synapse0x8d268f0();
   input += synapse0x8d26918();
   input += synapse0x8d26b48();
   input += synapse0x8d26b70();
   input += synapse0x8d26da0();
   input += synapse0x8d26dc8();
   input += synapse0x8d27000();
   input += synapse0x8d27028();
   input += synapse0x8d27050();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2ef68() {
   double input = input0x8d2ef68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d27280() {
   double input = -0.220029;
   input += synapse0x8d30070();
   input += synapse0x8d30098();
   input += synapse0x8d300c0();
   input += synapse0x8d300e8();
   input += synapse0x8d30110();
   input += synapse0x8d30138();
   input += synapse0x8d30160();
   input += synapse0x8d30188();
   input += synapse0x8d301b0();
   input += synapse0x8d301d8();
   input += synapse0x8d30200();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d27280() {
   double input = input0x8d27280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d30228() {
   double input = 0.615298;
   input += synapse0x8d30428();
   input += synapse0x8d30450();
   input += synapse0x8d30478();
   input += synapse0x8d304a0();
   input += synapse0x8d304c8();
   input += synapse0x8d304f0();
   input += synapse0x8d30518();
   input += synapse0x8d30540();
   input += synapse0x8d30568();
   input += synapse0x8d30590();
   input += synapse0x8d305b8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d30228() {
   double input = input0x8d30228();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d305e0() {
   double input = -0.465509;
   input += synapse0x8d307f8();
   input += synapse0x8d30820();
   input += synapse0x8d30848();
   input += synapse0x8d30870();
   input += synapse0x8d30898();
   input += synapse0x8d308c0();
   input += synapse0x8d308e8();
   input += synapse0x8d30910();
   input += synapse0x8d30938();
   input += synapse0x8d30960();
   input += synapse0x8d30988();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d305e0() {
   double input = input0x8d305e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d309b0() {
   double input = 0.0146091;
   input += synapse0x8d30bc8();
   input += synapse0x8d30bf0();
   input += synapse0x8d30c18();
   input += synapse0x8d30c40();
   input += synapse0x8d30c68();
   input += synapse0x8d30c90();
   input += synapse0x8d30cb8();
   input += synapse0x8d30ce0();
   input += synapse0x8d30d08();
   input += synapse0x8d30d30();
   input += synapse0x8d30d58();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d309b0() {
   double input = input0x8d309b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d30d80() {
   double input = -0.104084;
   input += synapse0x8d30f98();
   input += synapse0x8d30fc0();
   input += synapse0x8d30fe8();
   input += synapse0x8d31010();
   input += synapse0x8d31038();
   input += synapse0x8d31060();
   input += synapse0x8d31088();
   input += synapse0x8d310b0();
   input += synapse0x8d310d8();
   input += synapse0x8d31100();
   input += synapse0x8d31128();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d30d80() {
   double input = input0x8d30d80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d31150() {
   double input = 0.209851;
   input += synapse0x8d31368();
   input += synapse0x8d31390();
   input += synapse0x8d313b8();
   input += synapse0x8d313e0();
   input += synapse0x8d31408();
   input += synapse0x8d31430();
   input += synapse0x8d31458();
   input += synapse0x8d31480();
   input += synapse0x8d314a8();
   input += synapse0x8d314d0();
   input += synapse0x8d314f8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d31150() {
   double input = input0x8d31150();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d31520() {
   double input = -0.128632;
   input += synapse0x8d31738();
   input += synapse0x8d31760();
   input += synapse0x8d31788();
   input += synapse0x8d317b0();
   input += synapse0x8d317d8();
   input += synapse0x8d31800();
   input += synapse0x8d31828();
   input += synapse0x8d31850();
   input += synapse0x8d31878();
   input += synapse0x8d318a0();
   input += synapse0x8d318c8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d31520() {
   double input = input0x8d31520();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d318f0() {
   double input = 1.63178;
   input += synapse0x8d31b08();
   input += synapse0x8d31b30();
   input += synapse0x8d31b58();
   input += synapse0x8d31b80();
   input += synapse0x8d31ba8();
   input += synapse0x8d31bd0();
   input += synapse0x8d31bf8();
   input += synapse0x8d31c20();
   input += synapse0x8d31c48();
   input += synapse0x8d31c70();
   input += synapse0x8d31c98();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d318f0() {
   double input = input0x8d318f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d31cc0() {
   double input = -0.526059;
   input += synapse0x8d31ed8();
   input += synapse0x8d31f00();
   input += synapse0x8d31f28();
   input += synapse0x8d31f50();
   input += synapse0x8d31f78();
   input += synapse0x8d31fa0();
   input += synapse0x8d31fc8();
   input += synapse0x8d31ff0();
   input += synapse0x8d32018();
   input += synapse0x8d32040();
   input += synapse0x8d32068();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d31cc0() {
   double input = input0x8d31cc0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d32090() {
   double input = -0.130825;
   input += synapse0x8d322a8();
   input += synapse0x8d322d0();
   input += synapse0x8d322f8();
   input += synapse0x8d32320();
   input += synapse0x8d32348();
   input += synapse0x8d32370();
   input += synapse0x8d32398();
   input += synapse0x8d323c0();
   input += synapse0x8d323e8();
   input += synapse0x8d32410();
   input += synapse0x8d32438();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d32090() {
   double input = input0x8d32090();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d32460() {
   double input = -1.62498;
   input += synapse0x8d32678();
   input += synapse0x8d326a0();
   input += synapse0x8d326c8();
   input += synapse0x8d326f0();
   input += synapse0x8d32718();
   input += synapse0x8d32740();
   input += synapse0x8d32768();
   input += synapse0x8d32790();
   input += synapse0x8d327b8();
   input += synapse0x8d327e0();
   input += synapse0x8d32808();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d32460() {
   double input = input0x8d32460();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d32830() {
   double input = 0.518549;
   input += synapse0x8d32a48();
   input += synapse0x8d32a70();
   input += synapse0x8d32a98();
   input += synapse0x8d32ac0();
   input += synapse0x8d32ae8();
   input += synapse0x8d32b10();
   input += synapse0x8d32b38();
   input += synapse0x8d32b60();
   input += synapse0x8d32b88();
   input += synapse0x8d32bb0();
   input += synapse0x8d32bd8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d32830() {
   double input = input0x8d32830();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d32c00() {
   double input = -0.454205;
   input += synapse0x8d32e18();
   input += synapse0x8d32e40();
   input += synapse0x8d32e68();
   input += synapse0x8d32e90();
   input += synapse0x8d32eb8();
   input += synapse0x8d32ee0();
   input += synapse0x8d32f08();
   input += synapse0x8d32f30();
   input += synapse0x8d32f58();
   input += synapse0x8d32f80();
   input += synapse0x8d32fa8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d32c00() {
   double input = input0x8d32c00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d32fd0() {
   double input = 0.535799;
   input += synapse0x8d331e8();
   input += synapse0x8d33210();
   input += synapse0x8d33238();
   input += synapse0x8d33260();
   input += synapse0x8d33288();
   input += synapse0x8d332b0();
   input += synapse0x8d332d8();
   input += synapse0x8d33300();
   input += synapse0x8d33328();
   input += synapse0x8d33350();
   input += synapse0x8d33378();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d32fd0() {
   double input = input0x8d32fd0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d333a0() {
   double input = 0.46486;
   input += synapse0x8d335b8();
   input += synapse0x8d335e0();
   input += synapse0x8d33608();
   input += synapse0x8d33630();
   input += synapse0x8d33658();
   input += synapse0x8d33680();
   input += synapse0x8d336a8();
   input += synapse0x8d336d0();
   input += synapse0x8d336f8();
   input += synapse0x8d33720();
   input += synapse0x8d33748();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d333a0() {
   double input = input0x8d333a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d33770() {
   double input = -0.264899;
   input += synapse0x8d33988();
   input += synapse0x8d339b0();
   input += synapse0x8d339d8();
   input += synapse0x8d33a00();
   input += synapse0x8d33a28();
   input += synapse0x8d33a50();
   input += synapse0x8d33a78();
   input += synapse0x8d33aa0();
   input += synapse0x8d33ac8();
   input += synapse0x8d33af0();
   input += synapse0x8d33b18();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d33770() {
   double input = input0x8d33770();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d33b40() {
   double input = 1.20486;
   input += synapse0x8d33d58();
   input += synapse0x8d33d80();
   input += synapse0x8d33da8();
   input += synapse0x8d33dd0();
   input += synapse0x8d33df8();
   input += synapse0x8d33e20();
   input += synapse0x8d33e48();
   input += synapse0x8d33e70();
   input += synapse0x8d33e98();
   input += synapse0x8d33ec0();
   input += synapse0x8d33ee8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d33b40() {
   double input = input0x8d33b40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d33f10() {
   double input = 0.522749;
   input += synapse0x8d34128();
   input += synapse0x8d34150();
   input += synapse0x8d34178();
   input += synapse0x8d341a0();
   input += synapse0x8d341c8();
   input += synapse0x8d341f0();
   input += synapse0x8d34218();
   input += synapse0x8d34240();
   input += synapse0x8d34268();
   input += synapse0x8d34290();
   input += synapse0x8d342b8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d33f10() {
   double input = input0x8d33f10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d342e0() {
   double input = -0.241265;
   input += synapse0x8d344f8();
   input += synapse0x8d34520();
   input += synapse0x8d34548();
   input += synapse0x8d34570();
   input += synapse0x8d34598();
   input += synapse0x8d345c0();
   input += synapse0x8d345e8();
   input += synapse0x8d34610();
   input += synapse0x8d34638();
   input += synapse0x8d34660();
   input += synapse0x8d34688();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d342e0() {
   double input = input0x8d342e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d346b0() {
   double input = 0.623438;
   input += synapse0x8d348c8();
   input += synapse0x8d348f0();
   input += synapse0x8d34918();
   input += synapse0x8d34940();
   input += synapse0x8d34968();
   input += synapse0x8d34990();
   input += synapse0x8d349b8();
   input += synapse0x8d349e0();
   input += synapse0x8d34a08();
   input += synapse0x8d34a30();
   input += synapse0x8d34a58();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d346b0() {
   double input = input0x8d346b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d34a80() {
   double input = 0.548079;
   input += synapse0x8d34c98();
   input += synapse0x8d34cc0();
   input += synapse0x8d34ce8();
   input += synapse0x8d34d10();
   input += synapse0x8d34d38();
   input += synapse0x8d34d60();
   input += synapse0x8d34d88();
   input += synapse0x8d34db0();
   input += synapse0x8d34dd8();
   input += synapse0x8d34e00();
   input += synapse0x8d34e28();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d34a80() {
   double input = input0x8d34a80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d34e50() {
   double input = 0.122779;
   input += synapse0x8d35068();
   input += synapse0x8d35090();
   input += synapse0x8d350b8();
   input += synapse0x8d350e0();
   input += synapse0x8d35108();
   input += synapse0x8d35130();
   input += synapse0x8d35158();
   input += synapse0x8d35180();
   input += synapse0x8d351a8();
   input += synapse0x8d351d0();
   input += synapse0x8d351f8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d34e50() {
   double input = input0x8d34e50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d35220() {
   double input = 0.718362;
   input += synapse0x8d35438();
   input += synapse0x8d35460();
   input += synapse0x8d35488();
   input += synapse0x8d354b0();
   input += synapse0x8d354d8();
   input += synapse0x8d35500();
   input += synapse0x8d35528();
   input += synapse0x8d35550();
   input += synapse0x8d35578();
   input += synapse0x8d355a0();
   input += synapse0x8d355c8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d35220() {
   double input = input0x8d35220();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d355f0() {
   double input = 0.409335;
   input += synapse0x8d35808();
   input += synapse0x8d35830();
   input += synapse0x8d35858();
   input += synapse0x8d35880();
   input += synapse0x8d358a8();
   input += synapse0x8d358d0();
   input += synapse0x8d358f8();
   input += synapse0x8d35920();
   input += synapse0x8d35948();
   input += synapse0x8d35970();
   input += synapse0x8d35998();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d355f0() {
   double input = input0x8d355f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d359c0() {
   double input = 0.108847;
   input += synapse0x8d35bd8();
   input += synapse0x8d35c00();
   input += synapse0x8d35c28();
   input += synapse0x8d35c50();
   input += synapse0x8d35c78();
   input += synapse0x8d35ca0();
   input += synapse0x8d35cc8();
   input += synapse0x8d35cf0();
   input += synapse0x8d35d18();
   input += synapse0x8d35d40();
   input += synapse0x8d35d68();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d359c0() {
   double input = input0x8d359c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d35d90() {
   double input = -0.446072;
   input += synapse0x8d35fa8();
   input += synapse0x8d35fd0();
   input += synapse0x8d35ff8();
   input += synapse0x8d36020();
   input += synapse0x8d36048();
   input += synapse0x8d36070();
   input += synapse0x8d36098();
   input += synapse0x8d360c0();
   input += synapse0x8d360e8();
   input += synapse0x8d36110();
   input += synapse0x8d36138();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d35d90() {
   double input = input0x8d35d90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d36160() {
   double input = -0.0930486;
   input += synapse0x8d36378();
   input += synapse0x8d363a0();
   input += synapse0x8d363c8();
   input += synapse0x8d363f0();
   input += synapse0x8d36418();
   input += synapse0x8d36440();
   input += synapse0x8d36468();
   input += synapse0x8d36490();
   input += synapse0x8d364b8();
   input += synapse0x8d364e0();
   input += synapse0x8d36508();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d36160() {
   double input = input0x8d36160();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d36530() {
   double input = -0.300249;
   input += synapse0x8d36748();
   input += synapse0x8d36770();
   input += synapse0x8d36798();
   input += synapse0x8d367c0();
   input += synapse0x8d367e8();
   input += synapse0x8d36810();
   input += synapse0x8d36838();
   input += synapse0x8d36860();
   input += synapse0x8d36888();
   input += synapse0x8d368b0();
   input += synapse0x8d368d8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d36530() {
   double input = input0x8d36530();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d36900() {
   double input = -0.238983;
   input += synapse0x8d36b18();
   input += synapse0x8d36b40();
   input += synapse0x8d36b68();
   input += synapse0x8d36b90();
   input += synapse0x8d36bb8();
   input += synapse0x8d36be0();
   input += synapse0x8d36c08();
   input += synapse0x8d36c30();
   input += synapse0x8d36c58();
   input += synapse0x8d36c80();
   input += synapse0x8d36ca8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d36900() {
   double input = input0x8d36900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d36cd0() {
   double input = -0.433394;
   input += synapse0x8d36ee8();
   input += synapse0x8d36f10();
   input += synapse0x8d2a8c8();
   input += synapse0x8d2a8f0();
   input += synapse0x8d2a918();
   input += synapse0x8d2a940();
   input += synapse0x8d2a968();
   input += synapse0x8d2a990();
   input += synapse0x8d2a9b8();
   input += synapse0x8d2a9e0();
   input += synapse0x8d2aa08();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d36cd0() {
   double input = input0x8d36cd0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2aa30() {
   double input = -0.617287;
   input += synapse0x8d2ac48();
   input += synapse0x8d2ac70();
   input += synapse0x8d2ac98();
   input += synapse0x8d2acc0();
   input += synapse0x8d2ace8();
   input += synapse0x8d2ad10();
   input += synapse0x8d2ad38();
   input += synapse0x8d2ad60();
   input += synapse0x8d2ad88();
   input += synapse0x8d2adb0();
   input += synapse0x8d2add8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2aa30() {
   double input = input0x8d2aa30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2ae00() {
   double input = -0.00106163;
   input += synapse0x8d2b018();
   input += synapse0x8d2b040();
   input += synapse0x8d2b068();
   input += synapse0x8d2b090();
   input += synapse0x8d2b0b8();
   input += synapse0x8d2b0e0();
   input += synapse0x8d2b108();
   input += synapse0x8d2b130();
   input += synapse0x8d2b158();
   input += synapse0x8d2b180();
   input += synapse0x8d2b1a8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2ae00() {
   double input = input0x8d2ae00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2b1d0() {
   double input = 0.731018;
   input += synapse0x8d2b3e8();
   input += synapse0x8d2b410();
   input += synapse0x8d2b438();
   input += synapse0x8d2b460();
   input += synapse0x8d2b488();
   input += synapse0x8d2b4b0();
   input += synapse0x8d2b4d8();
   input += synapse0x8d2b500();
   input += synapse0x8d2b528();
   input += synapse0x8d2b550();
   input += synapse0x8d2b578();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2b1d0() {
   double input = input0x8d2b1d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d2b5a0() {
   double input = -0.950895;
   input += synapse0x8d2b7b8();
   input += synapse0x8d2b7e0();
   input += synapse0x8d2b808();
   input += synapse0x8d2b830();
   input += synapse0x8d2b858();
   input += synapse0x8d2b880();
   input += synapse0x8d2b8a8();
   input += synapse0x8d38f40();
   input += synapse0x8d38f68();
   input += synapse0x8d38f90();
   input += synapse0x8d38fb8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d2b5a0() {
   double input = input0x8d2b5a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d38fe0() {
   double input = 1.22361;
   input += synapse0x8d391f8();
   input += synapse0x8d39220();
   input += synapse0x8d39248();
   input += synapse0x8d39270();
   input += synapse0x8d39298();
   input += synapse0x8d392c0();
   input += synapse0x8d392e8();
   input += synapse0x8d39310();
   input += synapse0x8d39338();
   input += synapse0x8d39360();
   input += synapse0x8d39388();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d38fe0() {
   double input = input0x8d38fe0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d393b0() {
   double input = 0.129564;
   input += synapse0x8d395c8();
   input += synapse0x8d395f0();
   input += synapse0x8d39618();
   input += synapse0x8d39640();
   input += synapse0x8d39668();
   input += synapse0x8d39690();
   input += synapse0x8d396b8();
   input += synapse0x8d396e0();
   input += synapse0x8d39708();
   input += synapse0x8d39730();
   input += synapse0x8d39758();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d393b0() {
   double input = input0x8d393b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::input0x8d39780() {
   double input = -0.104908;
   input += synapse0x8d398a8();
   input += synapse0x8d398d0();
   input += synapse0x8d398f8();
   input += synapse0x8d39920();
   input += synapse0x8d39948();
   input += synapse0x8d39970();
   input += synapse0x8d39998();
   input += synapse0x8d399c0();
   input += synapse0x8d399e8();
   input += synapse0x8d39a10();
   input += synapse0x8d39a38();
   input += synapse0x8d39a60();
   input += synapse0x8d39a88();
   input += synapse0x8d39ab0();
   input += synapse0x8d39ad8();
   input += synapse0x8d39b00();
   input += synapse0x8d39bb0();
   input += synapse0x8d39bd8();
   input += synapse0x8d39c00();
   input += synapse0x8d39c28();
   input += synapse0x8d39c50();
   input += synapse0x8d39c78();
   input += synapse0x8d39ca0();
   input += synapse0x8d39cc8();
   input += synapse0x8d39cf0();
   input += synapse0x8d39d18();
   input += synapse0x8d39d40();
   input += synapse0x8d39d68();
   input += synapse0x8d39d90();
   input += synapse0x8d39db8();
   input += synapse0x8d39de0();
   input += synapse0x8d39e08();
   input += synapse0x8d39b28();
   input += synapse0x8d39b50();
   input += synapse0x8d39b78();
   input += synapse0x8d39f38();
   input += synapse0x8d39f60();
   input += synapse0x8d39f88();
   input += synapse0x8d39fb0();
   input += synapse0x8d39fd8();
   input += synapse0x8d3a000();
   input += synapse0x8d3a028();
   input += synapse0x8d3a050();
   input += synapse0x8d3a078();
   input += synapse0x8d3a0a0();
   input += synapse0x8d3a0c8();
   input += synapse0x8d3a0f0();
   input += synapse0x8d3a118();
   input += synapse0x8d3a140();
   input += synapse0x8d3a168();
   input += synapse0x8d3a190();
   input += synapse0x8d3a1b8();
   input += synapse0x8d3a1e0();
   input += synapse0x8d3a208();
   input += synapse0x8d3a230();
   input += synapse0x8d3a258();
   input += synapse0x8d3a280();
   input += synapse0x8d3a2a8();
   input += synapse0x8d3a2d0();
   input += synapse0x8d3a2f8();
   input += synapse0x8d3a320();
   input += synapse0x8d3a348();
   input += synapse0x8d3a370();
   input += synapse0x8d3a398();
   input += synapse0x8d1d988();
   input += synapse0x8d39e30();
   input += synapse0x8d39e58();
   input += synapse0x8d39e80();
   input += synapse0x8d39ea8();
   input += synapse0x8d39ed0();
   input += synapse0x8d39ef8();
   input += synapse0x8d3a5c8();
   input += synapse0x8d3a5f0();
   input += synapse0x8d3a618();
   input += synapse0x8d3a640();
   input += synapse0x8d3a668();
   input += synapse0x8d3a690();
   input += synapse0x8d3a6b8();
   input += synapse0x8d3a6e0();
   input += synapse0x8d3a708();
   input += synapse0x8d3a730();
   input += synapse0x8d3a758();
   input += synapse0x8d3a780();
   input += synapse0x8d3a7a8();
   input += synapse0x8d3a7d0();
   input += synapse0x8d3a7f8();
   input += synapse0x8d3a820();
   input += synapse0x8d3a848();
   input += synapse0x8d3a870();
   input += synapse0x8d3a898();
   input += synapse0x8d3a8c0();
   input += synapse0x8d3a8e8();
   input += synapse0x8d3a910();
   input += synapse0x8d3a938();
   input += synapse0x8d3a960();
   input += synapse0x8d3a988();
   input += synapse0x8d3a9b0();
   input += synapse0x8d3a9d8();
   input += synapse0x8d3aa00();
   input += synapse0x8d3aa28();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaX::neuron0x8d39780() {
   double input = input0x8d39780();
   return (input * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8bef198() {
   return (neuron0x8d1c360()*-0.0824871);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dc28() {
   return (neuron0x8d1c4d0()*0.025484);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dc50() {
   return (neuron0x8d1c6d0()*0.715807);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dc78() {
   return (neuron0x8d1c8d0()*-0.0770837);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dca0() {
   return (neuron0x8d1cad0()*-0.306154);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dcc8() {
   return (neuron0x8d1ccd0()*0.0603614);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dcf0() {
   return (neuron0x8d1cee8()*-0.4451);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dd18() {
   return (neuron0x8d1d100()*-0.230429);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dd40() {
   return (neuron0x8d1d318()*-0.127113);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dd68() {
   return (neuron0x8d1d530()*0.405046);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dd90() {
   return (neuron0x8d1d730()*0.561983);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dfb8() {
   return (neuron0x8d1c360()*-0.292356);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1dfe0() {
   return (neuron0x8d1c4d0()*0.136933);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e008() {
   return (neuron0x8d1c6d0()*0.0832895);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e030() {
   return (neuron0x8d1c8d0()*-0.491753);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e058() {
   return (neuron0x8d1cad0()*0.203256);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e080() {
   return (neuron0x8d1ccd0()*0.403696);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e130() {
   return (neuron0x8d1cee8()*-0.473981);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e158() {
   return (neuron0x8d1d100()*-0.250694);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e180() {
   return (neuron0x8d1d318()*-0.237008);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e1a8() {
   return (neuron0x8d1d530()*-0.0697976);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e1d0() {
   return (neuron0x8d1d730()*0.314294);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e3b0() {
   return (neuron0x8d1c360()*0.0256248);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e3d8() {
   return (neuron0x8d1c4d0()*-0.14073);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e400() {
   return (neuron0x8d1c6d0()*0.121591);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e428() {
   return (neuron0x8d1c8d0()*0.457101);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e450() {
   return (neuron0x8d1cad0()*0.140698);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e478() {
   return (neuron0x8d1ccd0()*0.0975853);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e4a0() {
   return (neuron0x8d1cee8()*-0.411179);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e4c8() {
   return (neuron0x8d1d100()*-0.287218);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e4f0() {
   return (neuron0x8d1d318()*-0.322502);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e518() {
   return (neuron0x8d1d530()*0.346097);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e540() {
   return (neuron0x8d1d730()*0.31293);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e768() {
   return (neuron0x8d1c360()*0.19868);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e790() {
   return (neuron0x8d1c4d0()*0.177927);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e7b8() {
   return (neuron0x8d1c6d0()*-0.286106);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e7e0() {
   return (neuron0x8d1c8d0()*-0.648399);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e808() {
   return (neuron0x8d1cad0()*0.454979);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e830() {
   return (neuron0x8d1ccd0()*-0.614081);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e858() {
   return (neuron0x8d1cee8()*-0.293794);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e880() {
   return (neuron0x8d1d100()*0.585585);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e8a8() {
   return (neuron0x8d1d318()*-0.275853);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e8d0() {
   return (neuron0x8d1d530()*0.0816397);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e8f8() {
   return (neuron0x8d1d730()*0.30212);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1eb20() {
   return (neuron0x8d1c360()*0.221085);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1eb48() {
   return (neuron0x8d1c4d0()*0.748961);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1eb70() {
   return (neuron0x8d1c6d0()*1.04559);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1eb98() {
   return (neuron0x8d1c8d0()*-0.038974);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ebc0() {
   return (neuron0x8d1cad0()*0.280768);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ebe8() {
   return (neuron0x8d1ccd0()*-0.102681);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ec10() {
   return (neuron0x8d1cee8()*-0.456653);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ec38() {
   return (neuron0x8d1d100()*-0.0381519);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ec60() {
   return (neuron0x8d1d318()*0.304354);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ec88() {
   return (neuron0x8d1d530()*0.0219103);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ecb0() {
   return (neuron0x8d1d730()*0.498312);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1eed8() {
   return (neuron0x8d1c360()*-0.050268);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ef00() {
   return (neuron0x8d1c4d0()*0.784911);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ef28() {
   return (neuron0x8d1c6d0()*0.333392);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ef50() {
   return (neuron0x8d1c8d0()*0.56782);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ef78() {
   return (neuron0x8d1cad0()*0.401423);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1efa0() {
   return (neuron0x8d1ccd0()*-0.150254);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1efc8() {
   return (neuron0x8d1cee8()*-0.557138);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1eff0() {
   return (neuron0x8d1d100()*-0.225465);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f018() {
   return (neuron0x8d1d318()*-0.0848613);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f040() {
   return (neuron0x8d1d530()*-0.767699);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8bec9c0() {
   return (neuron0x8d1d730()*0.743792);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f428() {
   return (neuron0x8d1c360()*0.00351414);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f450() {
   return (neuron0x8d1c4d0()*0.183144);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f478() {
   return (neuron0x8d1c6d0()*0.375771);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f4a0() {
   return (neuron0x8d1c8d0()*-0.39751);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f4c8() {
   return (neuron0x8d1cad0()*-0.205666);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f4f0() {
   return (neuron0x8d1ccd0()*0.622538);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f518() {
   return (neuron0x8d1cee8()*0.607172);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f540() {
   return (neuron0x8d1d100()*0.842993);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f568() {
   return (neuron0x8d1d318()*0.255545);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f590() {
   return (neuron0x8d1d530()*-0.00766909);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f5b8() {
   return (neuron0x8d1d730()*-0.32633);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f7f8() {
   return (neuron0x8d1c360()*0.953377);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f820() {
   return (neuron0x8d1c4d0()*-0.476274);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f848() {
   return (neuron0x8d1c6d0()*-1.0286);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f870() {
   return (neuron0x8d1c8d0()*-0.438015);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f898() {
   return (neuron0x8d1cad0()*0.419874);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f8c0() {
   return (neuron0x8d1ccd0()*-0.156382);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f8e8() {
   return (neuron0x8d1cee8()*0.357471);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f910() {
   return (neuron0x8d1d100()*0.196854);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f938() {
   return (neuron0x8d1d318()*-0.151224);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f960() {
   return (neuron0x8d1d530()*-0.809709);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1f988() {
   return (neuron0x8d1d730()*0.113621);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fbc8() {
   return (neuron0x8d1c360()*-0.0463503);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fbf0() {
   return (neuron0x8d1c4d0()*-0.440752);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fc18() {
   return (neuron0x8d1c6d0()*-0.137604);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fc40() {
   return (neuron0x8d1c8d0()*-1.39297);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fc68() {
   return (neuron0x8d1cad0()*0.0680827);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fc90() {
   return (neuron0x8d1ccd0()*-0.165043);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fcb8() {
   return (neuron0x8d1cee8()*-0.0226826);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fce0() {
   return (neuron0x8d1d100()*-0.559233);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fd08() {
   return (neuron0x8d1d318()*-0.0495912);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fd30() {
   return (neuron0x8d1d530()*0.30075);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1fd58() {
   return (neuron0x8d1d730()*0.0797299);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ff98() {
   return (neuron0x8d1c360()*0.0462305);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ffc0() {
   return (neuron0x8d1c4d0()*-0.389073);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1ffe8() {
   return (neuron0x8d1c6d0()*-0.428888);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20010() {
   return (neuron0x8d1c8d0()*-0.288631);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20038() {
   return (neuron0x8d1cad0()*0.312688);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20060() {
   return (neuron0x8d1ccd0()*0.455664);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20088() {
   return (neuron0x8d1cee8()*0.56161);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d200b0() {
   return (neuron0x8d1d100()*0.436652);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d200d8() {
   return (neuron0x8d1d318()*-0.467531);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20100() {
   return (neuron0x8d1d530()*0.0603031);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20128() {
   return (neuron0x8d1d730()*0.322864);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20368() {
   return (neuron0x8d1c360()*-0.413775);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20390() {
   return (neuron0x8d1c4d0()*-0.279283);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d203b8() {
   return (neuron0x8d1c6d0()*-0.344622);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d203e0() {
   return (neuron0x8d1c8d0()*-0.144483);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20408() {
   return (neuron0x8d1cad0()*-0.0464196);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20430() {
   return (neuron0x8d1ccd0()*-0.476582);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20458() {
   return (neuron0x8d1cee8()*0.414818);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20480() {
   return (neuron0x8d1d100()*0.4562);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d204a8() {
   return (neuron0x8d1d318()*-0.0308578);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d204d0() {
   return (neuron0x8d1d530()*-0.8544);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d204f8() {
   return (neuron0x8d1d730()*-0.176155);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20738() {
   return (neuron0x8d1c360()*0.341382);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20760() {
   return (neuron0x8d1c4d0()*0.140755);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20788() {
   return (neuron0x8d1c6d0()*-0.648273);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d207b0() {
   return (neuron0x8d1c8d0()*0.184301);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d207d8() {
   return (neuron0x8d1cad0()*-0.608651);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20800() {
   return (neuron0x8d1ccd0()*0.35315);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20828() {
   return (neuron0x8d1cee8()*0.302787);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20850() {
   return (neuron0x8d1d100()*-0.170027);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cfc5c0() {
   return (neuron0x8d1d318()*0.0893646);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8a387e8() {
   return (neuron0x8d1d530()*0.392017);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8bef0c8() {
   return (neuron0x8d1d730()*-0.0997129);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8bef0f0() {
   return (neuron0x8d1c360()*0.439397);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8bef118() {
   return (neuron0x8d1c4d0()*1.02868);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8bef140() {
   return (neuron0x8d1c6d0()*-0.362724);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8bef168() {
   return (neuron0x8d1c8d0()*-0.826021);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cd69c0() {
   return (neuron0x8d1cad0()*-0.152272);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cd69e8() {
   return (neuron0x8d1ccd0()*0.2873);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cd6a10() {
   return (neuron0x8d1cee8()*-0.31262);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cd6a38() {
   return (neuron0x8d1d100()*0.342952);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cd6a60() {
   return (neuron0x8d1d318()*-0.328356);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cd6a88() {
   return (neuron0x8d1d530()*0.490738);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cfc458() {
   return (neuron0x8d1d730()*0.401739);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20e80() {
   return (neuron0x8d1c360()*0.10718);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20ea8() {
   return (neuron0x8d1c4d0()*-0.141803);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20ed0() {
   return (neuron0x8d1c6d0()*-0.912163);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20ef8() {
   return (neuron0x8d1c8d0()*0.292396);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20f20() {
   return (neuron0x8d1cad0()*0.423486);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20f48() {
   return (neuron0x8d1ccd0()*-0.0425061);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20f70() {
   return (neuron0x8d1cee8()*-0.130789);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20f98() {
   return (neuron0x8d1d100()*-0.131048);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20fc0() {
   return (neuron0x8d1d318()*-0.129571);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20fe8() {
   return (neuron0x8d1d530()*-0.161927);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21010() {
   return (neuron0x8d1d730()*0.559837);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21238() {
   return (neuron0x8d1c360()*-0.312961);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21260() {
   return (neuron0x8d1c4d0()*-0.421943);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21288() {
   return (neuron0x8d1c6d0()*0.0470199);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d212b0() {
   return (neuron0x8d1c8d0()*0.809484);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d212d8() {
   return (neuron0x8d1cad0()*-0.340047);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21300() {
   return (neuron0x8d1ccd0()*0.16959);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21328() {
   return (neuron0x8d1cee8()*-0.267092);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21350() {
   return (neuron0x8d1d100()*0.201522);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21378() {
   return (neuron0x8d1d318()*-0.659045);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d213a0() {
   return (neuron0x8d1d530()*0.18708);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d213c8() {
   return (neuron0x8d1d730()*0.145334);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21608() {
   return (neuron0x8d1c360()*-0.459794);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d216b8() {
   return (neuron0x8d1c4d0()*-0.293812);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21768() {
   return (neuron0x8d1c6d0()*0.0856193);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21818() {
   return (neuron0x8d1c8d0()*-0.444227);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d218c8() {
   return (neuron0x8d1cad0()*0.19795);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21978() {
   return (neuron0x8d1ccd0()*-0.437554);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21a28() {
   return (neuron0x8d1cee8()*0.284014);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21ad8() {
   return (neuron0x8d1d100()*0.152704);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21b88() {
   return (neuron0x8d1d318()*-0.272027);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21c38() {
   return (neuron0x8d1d530()*0.444436);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21ce8() {
   return (neuron0x8d1d730()*0.0265168);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21ed8() {
   return (neuron0x8d1c360()*-0.468679);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21f00() {
   return (neuron0x8d1c4d0()*0.122271);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21f28() {
   return (neuron0x8d1c6d0()*-0.39082);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21f50() {
   return (neuron0x8d1c8d0()*-0.299766);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21f78() {
   return (neuron0x8d1cad0()*0.179806);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21fa0() {
   return (neuron0x8d1ccd0()*0.325717);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21fc8() {
   return (neuron0x8d1cee8()*0.462457);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21ff0() {
   return (neuron0x8d1d100()*0.449656);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d22018() {
   return (neuron0x8d1d318()*-0.17388);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d22040() {
   return (neuron0x8d1d530()*-0.0793233);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d22068() {
   return (neuron0x8d1d730()*-0.820022);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d22090() {
   return (neuron0x8d1c360()*0.510686);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d220b8() {
   return (neuron0x8d1c4d0()*-0.413958);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d220e0() {
   return (neuron0x8d1c6d0()*-0.161779);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d22108() {
   return (neuron0x8d1c8d0()*0.393631);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cfc480() {
   return (neuron0x8d1cad0()*0.161018);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cfc4a8() {
   return (neuron0x8d1ccd0()*0.0803353);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cfc4d0() {
   return (neuron0x8d1cee8()*0.444438);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cfc4f8() {
   return (neuron0x8d1d100()*0.320502);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8cfc520() {
   return (neuron0x8d1d318()*0.408139);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23200() {
   return (neuron0x8d1d530()*0.0487356);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23228() {
   return (neuron0x8d1d730()*-0.161789);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23378() {
   return (neuron0x8d1c360()*-0.0836665);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d233a0() {
   return (neuron0x8d1c4d0()*0.217491);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d233c8() {
   return (neuron0x8d1c6d0()*0.198156);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d233f0() {
   return (neuron0x8d1c8d0()*0.0628941);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23418() {
   return (neuron0x8d1cad0()*-0.362704);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23440() {
   return (neuron0x8d1ccd0()*-0.411883);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23468() {
   return (neuron0x8d1cee8()*0.00186196);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23490() {
   return (neuron0x8d1d100()*-0.558581);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d234b8() {
   return (neuron0x8d1d318()*-0.421646);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d234e0() {
   return (neuron0x8d1d530()*0.225798);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23508() {
   return (neuron0x8d1d730()*-0.228826);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d236a0() {
   return (neuron0x8d1c360()*-0.504269);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d236c8() {
   return (neuron0x8d1c4d0()*-0.274173);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d236f0() {
   return (neuron0x8d1c6d0()*0.123663);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23718() {
   return (neuron0x8d1c8d0()*-0.152689);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23740() {
   return (neuron0x8d1cad0()*0.248213);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23768() {
   return (neuron0x8d1ccd0()*0.0873448);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23790() {
   return (neuron0x8d1cee8()*-0.515491);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d237b8() {
   return (neuron0x8d1d100()*-0.455054);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d237e0() {
   return (neuron0x8d1d318()*0.327795);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23808() {
   return (neuron0x8d1d530()*0.673374);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23830() {
   return (neuron0x8d1d730()*0.0901821);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23a58() {
   return (neuron0x8d1c360()*-0.499027);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23a80() {
   return (neuron0x8d1c4d0()*0.0402114);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23aa8() {
   return (neuron0x8d1c6d0()*0.34291);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23ad0() {
   return (neuron0x8d1c8d0()*0.284689);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23af8() {
   return (neuron0x8d1cad0()*0.159435);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23b20() {
   return (neuron0x8d1ccd0()*0.137596);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23b48() {
   return (neuron0x8d1cee8()*0.0900772);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23b70() {
   return (neuron0x8d1d100()*0.39726);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23b98() {
   return (neuron0x8d1d318()*-0.661303);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23bc0() {
   return (neuron0x8d1d530()*-0.624843);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23be8() {
   return (neuron0x8d1d730()*0.137075);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e0a8() {
   return (neuron0x8d1c360()*0.286068);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e0d0() {
   return (neuron0x8d1c4d0()*0.247393);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1e0f8() {
   return (neuron0x8d1c6d0()*0.0307091);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23f18() {
   return (neuron0x8d1c8d0()*-0.100404);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23f40() {
   return (neuron0x8d1cad0()*-0.301626);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23f68() {
   return (neuron0x8d1ccd0()*0.0346807);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23f90() {
   return (neuron0x8d1cee8()*0.106007);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23fb8() {
   return (neuron0x8d1d100()*0.304612);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23fe0() {
   return (neuron0x8d1d318()*-0.265531);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24008() {
   return (neuron0x8d1d530()*0.45757);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24030() {
   return (neuron0x8d1d730()*0.189941);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24258() {
   return (neuron0x8d1c360()*-0.394362);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24280() {
   return (neuron0x8d1c4d0()*0.12531);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d242a8() {
   return (neuron0x8d1c6d0()*0.584542);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d242d0() {
   return (neuron0x8d1c8d0()*-0.457687);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d242f8() {
   return (neuron0x8d1cad0()*-0.0851077);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24320() {
   return (neuron0x8d1ccd0()*-0.128307);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24348() {
   return (neuron0x8d1cee8()*-0.450381);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24370() {
   return (neuron0x8d1d100()*0.219419);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24398() {
   return (neuron0x8d1d318()*-0.238155);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d243c0() {
   return (neuron0x8d1d530()*-0.0613145);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d243e8() {
   return (neuron0x8d1d730()*-0.497215);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24610() {
   return (neuron0x8d1c360()*-0.155015);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24638() {
   return (neuron0x8d1c4d0()*0.87618);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24660() {
   return (neuron0x8d1c6d0()*-0.0375545);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24688() {
   return (neuron0x8d1c8d0()*0.422578);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20878() {
   return (neuron0x8d1cad0()*0.234366);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d208a0() {
   return (neuron0x8d1ccd0()*0.943283);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d208c8() {
   return (neuron0x8d1cee8()*0.105128);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d208f0() {
   return (neuron0x8d1d100()*-1.29095);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20918() {
   return (neuron0x8d1d318()*-0.0238781);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20940() {
   return (neuron0x8d1d530()*-0.877853);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20968() {
   return (neuron0x8d1d730()*-0.0805828);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20ba8() {
   return (neuron0x8d1c360()*0.277173);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20bd0() {
   return (neuron0x8d1c4d0()*-0.621744);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20bf8() {
   return (neuron0x8d1c6d0()*-0.0779053);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20c20() {
   return (neuron0x8d1c8d0()*0.224298);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d20c48() {
   return (neuron0x8d1cad0()*0.823602);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24eb8() {
   return (neuron0x8d1ccd0()*0.0406276);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24ee0() {
   return (neuron0x8d1cee8()*-0.504119);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24f08() {
   return (neuron0x8d1d100()*0.770573);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24f30() {
   return (neuron0x8d1d318()*0.113685);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24f58() {
   return (neuron0x8d1d530()*-0.769427);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24f80() {
   return (neuron0x8d1d730()*0.3154);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d251c0() {
   return (neuron0x8d1c360()*0.134319);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d251e8() {
   return (neuron0x8d1c4d0()*0.357629);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25210() {
   return (neuron0x8d1c6d0()*0.238833);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25238() {
   return (neuron0x8d1c8d0()*-0.0305929);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25260() {
   return (neuron0x8d1cad0()*0.507407);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25288() {
   return (neuron0x8d1ccd0()*-0.282914);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d252b0() {
   return (neuron0x8d1cee8()*-0.321187);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d252d8() {
   return (neuron0x8d1d100()*0.0333572);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25300() {
   return (neuron0x8d1d318()*0.188967);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25328() {
   return (neuron0x8d1d530()*-0.12904);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25350() {
   return (neuron0x8d1d730()*-0.37654);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25590() {
   return (neuron0x8d1c360()*-0.177031);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d255b8() {
   return (neuron0x8d1c4d0()*-1.13516);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d255e0() {
   return (neuron0x8d1c6d0()*-0.819818);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25608() {
   return (neuron0x8d1c8d0()*-0.723336);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25630() {
   return (neuron0x8d1cad0()*0.621017);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25658() {
   return (neuron0x8d1ccd0()*0.900405);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25680() {
   return (neuron0x8d1cee8()*-0.360609);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d256a8() {
   return (neuron0x8d1d100()*-0.573145);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d256d0() {
   return (neuron0x8d1d318()*0.396116);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d256f8() {
   return (neuron0x8d1d530()*0.0449438);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25720() {
   return (neuron0x8d1d730()*0.247913);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25960() {
   return (neuron0x8d1c360()*0.341452);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25988() {
   return (neuron0x8d1c4d0()*0.325571);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d259b0() {
   return (neuron0x8d1c6d0()*-0.40825);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d259d8() {
   return (neuron0x8d1c8d0()*-0.0464595);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25a00() {
   return (neuron0x8d1cad0()*-0.0455516);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25a28() {
   return (neuron0x8d1ccd0()*0.37906);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25a50() {
   return (neuron0x8d1cee8()*0.394456);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25a78() {
   return (neuron0x8d1d100()*0.11874);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25aa0() {
   return (neuron0x8d1d318()*-0.0172035);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25ac8() {
   return (neuron0x8d1d530()*0.0492635);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25af0() {
   return (neuron0x8d1d730()*0.710935);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25d30() {
   return (neuron0x8d1c360()*-0.237388);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25d58() {
   return (neuron0x8d1c4d0()*-0.0371209);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25d80() {
   return (neuron0x8d1c6d0()*0.191125);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25da8() {
   return (neuron0x8d1c8d0()*0.317472);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25dd0() {
   return (neuron0x8d1cad0()*0.0395982);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25df8() {
   return (neuron0x8d1ccd0()*-0.348583);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25e20() {
   return (neuron0x8d1cee8()*-0.615394);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25e48() {
   return (neuron0x8d1d100()*-0.231851);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25e70() {
   return (neuron0x8d1d318()*0.13009);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25e98() {
   return (neuron0x8d1d530()*-0.508387);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d25ec0() {
   return (neuron0x8d1d730()*0.00909832);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26100() {
   return (neuron0x8d1c360()*0.605475);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26128() {
   return (neuron0x8d1c4d0()*0.15178);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26150() {
   return (neuron0x8d1c6d0()*0.180115);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26178() {
   return (neuron0x8d1c8d0()*-0.127307);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d261a0() {
   return (neuron0x8d1cad0()*0.46698);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d261c8() {
   return (neuron0x8d1ccd0()*0.260645);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d261f0() {
   return (neuron0x8d1cee8()*0.228302);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26218() {
   return (neuron0x8d1d100()*0.601519);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26240() {
   return (neuron0x8d1d318()*-0.00842863);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26268() {
   return (neuron0x8d1d530()*-0.291397);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26290() {
   return (neuron0x8d1d730()*-0.0921299);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d264d0() {
   return (neuron0x8d1c360()*0.0888994);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d264f8() {
   return (neuron0x8d1c4d0()*-0.610494);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26520() {
   return (neuron0x8d1c6d0()*0.0211566);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26548() {
   return (neuron0x8d1c8d0()*-0.370818);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26570() {
   return (neuron0x8d1cad0()*0.179271);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26598() {
   return (neuron0x8d1ccd0()*-0.0547953);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d265c0() {
   return (neuron0x8d1cee8()*-0.293094);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d265e8() {
   return (neuron0x8d1d100()*0.205338);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26610() {
   return (neuron0x8d1d318()*-0.254461);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26638() {
   return (neuron0x8d1d530()*-0.00944592);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26660() {
   return (neuron0x8d1d730()*0.232781);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d268a0() {
   return (neuron0x8d1c360()*0.44639);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21630() {
   return (neuron0x8d1c4d0()*-0.325963);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21658() {
   return (neuron0x8d1c6d0()*-0.193876);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21680() {
   return (neuron0x8d1c8d0()*0.261275);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d216e0() {
   return (neuron0x8d1cad0()*0.226882);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21708() {
   return (neuron0x8d1ccd0()*0.531393);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21730() {
   return (neuron0x8d1cee8()*-0.195573);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21790() {
   return (neuron0x8d1d100()*-0.487423);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d217b8() {
   return (neuron0x8d1d318()*0.369803);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d217e0() {
   return (neuron0x8d1d530()*0.114555);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21840() {
   return (neuron0x8d1d730()*-0.344204);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d219e8() {
   return (neuron0x8d1c360()*0.00908083);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21938() {
   return (neuron0x8d1c4d0()*0.40293);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21a50() {
   return (neuron0x8d1c6d0()*-0.0836614);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21a78() {
   return (neuron0x8d1c8d0()*1.24537);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21aa0() {
   return (neuron0x8d1cad0()*0.0515427);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21b00() {
   return (neuron0x8d1ccd0()*-3.34715);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21b28() {
   return (neuron0x8d1cee8()*-0.0334114);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21b50() {
   return (neuron0x8d1d100()*-0.0627126);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21bb0() {
   return (neuron0x8d1d318()*-0.0577142);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21bd8() {
   return (neuron0x8d1d530()*-0.0212796);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21c00() {
   return (neuron0x8d1d730()*-0.258699);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21ca8() {
   return (neuron0x8d1c360()*-0.212848);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d21d58() {
   return (neuron0x8d1c4d0()*-0.306132);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d276b8() {
   return (neuron0x8d1c6d0()*-0.138043);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d276e0() {
   return (neuron0x8d1c8d0()*0.288165);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27708() {
   return (neuron0x8d1cad0()*-0.0151938);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27730() {
   return (neuron0x8d1ccd0()*0.101365);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27758() {
   return (neuron0x8d1cee8()*0.272449);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27780() {
   return (neuron0x8d1d100()*-0.273658);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d277a8() {
   return (neuron0x8d1d318()*-0.340584);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d277d0() {
   return (neuron0x8d1d530()*0.234044);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d277f8() {
   return (neuron0x8d1d730()*-0.0888172);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27a20() {
   return (neuron0x8d1c360()*-0.0456119);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27a48() {
   return (neuron0x8d1c4d0()*0.52243);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27a70() {
   return (neuron0x8d1c6d0()*0.154203);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27a98() {
   return (neuron0x8d1c8d0()*0.27406);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27ac0() {
   return (neuron0x8d1cad0()*-0.0749406);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27ae8() {
   return (neuron0x8d1ccd0()*-0.259745);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27b10() {
   return (neuron0x8d1cee8()*0.404684);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27b38() {
   return (neuron0x8d1d100()*-0.74156);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27b60() {
   return (neuron0x8d1d318()*0.187073);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27b88() {
   return (neuron0x8d1d530()*0.553029);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27bb0() {
   return (neuron0x8d1d730()*0.409208);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27dd8() {
   return (neuron0x8d1c360()*0.0450299);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27e00() {
   return (neuron0x8d1c4d0()*0.852473);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27e28() {
   return (neuron0x8d1c6d0()*0.468594);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27e50() {
   return (neuron0x8d1c8d0()*0.627154);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27e78() {
   return (neuron0x8d1cad0()*0.0104156);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27ea0() {
   return (neuron0x8d1ccd0()*0.15776);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27ec8() {
   return (neuron0x8d1cee8()*0.0294331);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27ef0() {
   return (neuron0x8d1d100()*0.0150439);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27f18() {
   return (neuron0x8d1d318()*-0.0517398);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27f40() {
   return (neuron0x8d1d530()*-0.228319);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27f68() {
   return (neuron0x8d1d730()*-0.0700025);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28190() {
   return (neuron0x8d1c360()*0.118098);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d281b8() {
   return (neuron0x8d1c4d0()*-1.55125);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d281e0() {
   return (neuron0x8d1c6d0()*0.00249688);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28208() {
   return (neuron0x8d1c8d0()*0.219456);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28230() {
   return (neuron0x8d1cad0()*-0.622846);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28258() {
   return (neuron0x8d1ccd0()*1.18412);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28280() {
   return (neuron0x8d1cee8()*-0.30466);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d282a8() {
   return (neuron0x8d1d100()*-0.67194);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d282d0() {
   return (neuron0x8d1d318()*-0.232998);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d282f8() {
   return (neuron0x8d1d530()*-0.500618);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28320() {
   return (neuron0x8d1d730()*-0.424889);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28560() {
   return (neuron0x8d1c360()*0.397963);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28588() {
   return (neuron0x8d1c4d0()*0.121815);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d285b0() {
   return (neuron0x8d1c6d0()*-0.312676);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d285d8() {
   return (neuron0x8d1c8d0()*-0.157092);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28600() {
   return (neuron0x8d1cad0()*0.270881);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28628() {
   return (neuron0x8d1ccd0()*0.103305);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28650() {
   return (neuron0x8d1cee8()*-0.0809923);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28678() {
   return (neuron0x8d1d100()*0.102508);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d286a0() {
   return (neuron0x8d1d318()*0.10118);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d286c8() {
   return (neuron0x8d1d530()*0.0151428);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d286f0() {
   return (neuron0x8d1d730()*0.0299332);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28930() {
   return (neuron0x8d1c360()*0.0180865);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28958() {
   return (neuron0x8d1c4d0()*0.606746);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28980() {
   return (neuron0x8d1c6d0()*-0.292366);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d289a8() {
   return (neuron0x8d1c8d0()*0.449556);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d289d0() {
   return (neuron0x8d1cad0()*-0.321707);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d289f8() {
   return (neuron0x8d1ccd0()*-0.275702);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28a20() {
   return (neuron0x8d1cee8()*-0.15528);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28a48() {
   return (neuron0x8d1d100()*-0.0743665);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28a70() {
   return (neuron0x8d1d318()*-0.378756);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28a98() {
   return (neuron0x8d1d530()*0.170351);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28ac0() {
   return (neuron0x8d1d730()*-0.379836);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28d00() {
   return (neuron0x8d1c360()*-0.21467);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28d28() {
   return (neuron0x8d1c4d0()*-0.40908);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28d50() {
   return (neuron0x8d1c6d0()*0.191796);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28d78() {
   return (neuron0x8d1c8d0()*-0.0453786);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28da0() {
   return (neuron0x8d1cad0()*-0.0013177);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28dc8() {
   return (neuron0x8d1ccd0()*-0.0630996);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28df0() {
   return (neuron0x8d1cee8()*0.128012);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28e18() {
   return (neuron0x8d1d100()*-0.128669);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28e40() {
   return (neuron0x8d1d318()*0.0783165);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28e68() {
   return (neuron0x8d1d530()*0.152219);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d28e90() {
   return (neuron0x8d1d730()*0.104504);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d290d0() {
   return (neuron0x8d1c360()*0.313953);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d290f8() {
   return (neuron0x8d1c4d0()*-0.315168);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29120() {
   return (neuron0x8d1c6d0()*0.058223);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29148() {
   return (neuron0x8d1c8d0()*-0.121945);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29170() {
   return (neuron0x8d1cad0()*0.228588);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29198() {
   return (neuron0x8d1ccd0()*-0.99846);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d291c0() {
   return (neuron0x8d1cee8()*-0.24762);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d291e8() {
   return (neuron0x8d1d100()*-0.789783);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29210() {
   return (neuron0x8d1d318()*-0.0883891);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29238() {
   return (neuron0x8d1d530()*0.326546);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29260() {
   return (neuron0x8d1d730()*-0.0702824);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d294a0() {
   return (neuron0x8d1c360()*-0.275422);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d294c8() {
   return (neuron0x8d1c4d0()*0.21032);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d294f0() {
   return (neuron0x8d1c6d0()*-0.549249);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29518() {
   return (neuron0x8d1c8d0()*-1.04302);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29540() {
   return (neuron0x8d1cad0()*0.0538663);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29568() {
   return (neuron0x8d1ccd0()*0.891169);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29590() {
   return (neuron0x8d1cee8()*0.142047);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d295b8() {
   return (neuron0x8d1d100()*0.443569);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d295e0() {
   return (neuron0x8d1d318()*-0.179869);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29608() {
   return (neuron0x8d1d530()*0.170151);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29630() {
   return (neuron0x8d1d730()*0.251133);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29870() {
   return (neuron0x8d1c360()*-0.176422);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29898() {
   return (neuron0x8d1c4d0()*-0.138489);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d298c0() {
   return (neuron0x8d1c6d0()*0.187232);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d298e8() {
   return (neuron0x8d1c8d0()*-0.155147);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29910() {
   return (neuron0x8d1cad0()*-0.0318108);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29938() {
   return (neuron0x8d1ccd0()*0.387708);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29960() {
   return (neuron0x8d1cee8()*-0.122174);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29988() {
   return (neuron0x8d1d100()*-0.518237);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d299b0() {
   return (neuron0x8d1d318()*-0.290907);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d299d8() {
   return (neuron0x8d1d530()*-0.296034);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29a00() {
   return (neuron0x8d1d730()*0.017004);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29c40() {
   return (neuron0x8d1c360()*-0.0907627);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29c68() {
   return (neuron0x8d1c4d0()*-0.149427);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29c90() {
   return (neuron0x8d1c6d0()*0.556329);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29cb8() {
   return (neuron0x8d1c8d0()*0.44395);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29ce0() {
   return (neuron0x8d1cad0()*0.116387);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29d08() {
   return (neuron0x8d1ccd0()*0.948928);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29d30() {
   return (neuron0x8d1cee8()*-0.138937);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29d58() {
   return (neuron0x8d1d100()*-0.357687);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29d80() {
   return (neuron0x8d1d318()*-0.731121);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29da8() {
   return (neuron0x8d1d530()*-0.0441172);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d29dd0() {
   return (neuron0x8d1d730()*-0.122757);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a010() {
   return (neuron0x8d1c360()*-0.00585855);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a038() {
   return (neuron0x8d1c4d0()*-0.244738);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a060() {
   return (neuron0x8d1c6d0()*0.590255);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a088() {
   return (neuron0x8d1c8d0()*-1.23189);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a0b0() {
   return (neuron0x8d1cad0()*-0.207487);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a0d8() {
   return (neuron0x8d1ccd0()*-0.129929);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a100() {
   return (neuron0x8d1cee8()*-0.295216);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a128() {
   return (neuron0x8d1d100()*-0.206385);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a150() {
   return (neuron0x8d1d318()*0.276154);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a178() {
   return (neuron0x8d1d530()*0.740885);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a1a0() {
   return (neuron0x8d1d730()*0.546603);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a3e0() {
   return (neuron0x8d1c360()*0.413098);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a408() {
   return (neuron0x8d1c4d0()*0.95845);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a430() {
   return (neuron0x8d1c6d0()*-0.498635);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a458() {
   return (neuron0x8d1c8d0()*0.393504);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a480() {
   return (neuron0x8d1cad0()*-0.374012);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a4a8() {
   return (neuron0x8d1ccd0()*-0.200787);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a4d0() {
   return (neuron0x8d1cee8()*0.0909566);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a4f8() {
   return (neuron0x8d1d100()*0.575539);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a520() {
   return (neuron0x8d1d318()*-1.00002);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a548() {
   return (neuron0x8d1d530()*-0.265866);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a570() {
   return (neuron0x8d1d730()*-0.344771);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a7b0() {
   return (neuron0x8d1c360()*0.00771786);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a7d8() {
   return (neuron0x8d1c4d0()*1.02296);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a800() {
   return (neuron0x8d1c6d0()*0.218383);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a828() {
   return (neuron0x8d1c8d0()*-1.48037);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a850() {
   return (neuron0x8d1cad0()*-0.238153);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a878() {
   return (neuron0x8d1ccd0()*0.655794);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a8a0() {
   return (neuron0x8d1cee8()*-0.0271862);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d246b0() {
   return (neuron0x8d1d100()*1.02538);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d246d8() {
   return (neuron0x8d1d318()*0.290886);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24700() {
   return (neuron0x8d1d530()*-0.185731);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24728() {
   return (neuron0x8d1d730()*-0.0719231);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24968() {
   return (neuron0x8d1c360()*-0.448856);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24990() {
   return (neuron0x8d1c4d0()*0.161584);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d249b8() {
   return (neuron0x8d1c6d0()*0.350296);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d249e0() {
   return (neuron0x8d1c8d0()*-0.243258);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24a08() {
   return (neuron0x8d1cad0()*0.312307);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24a30() {
   return (neuron0x8d1ccd0()*0.0209273);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24a58() {
   return (neuron0x8d1cee8()*0.0727915);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24a80() {
   return (neuron0x8d1d100()*0.0790275);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24aa8() {
   return (neuron0x8d1d318()*-0.105611);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24ad0() {
   return (neuron0x8d1d530()*-0.421318);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24af8() {
   return (neuron0x8d1d730()*-0.212526);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24d38() {
   return (neuron0x8d1c360()*-0.25875);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24d60() {
   return (neuron0x8d1c4d0()*0.321385);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24d88() {
   return (neuron0x8d1c6d0()*-0.042602);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24db0() {
   return (neuron0x8d1c8d0()*-0.0769195);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24dd8() {
   return (neuron0x8d1cad0()*0.091185);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24e00() {
   return (neuron0x8d1ccd0()*0.0844429);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24e28() {
   return (neuron0x8d1cee8()*0.374644);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24e50() {
   return (neuron0x8d1d100()*-0.478959);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d24e78() {
   return (neuron0x8d1d318()*-0.432376);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b8d0() {
   return (neuron0x8d1d530()*-0.345864);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b8f8() {
   return (neuron0x8d1d730()*-0.565355);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bb20() {
   return (neuron0x8d1c360()*0.211196);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bb48() {
   return (neuron0x8d1c4d0()*0.886606);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bb70() {
   return (neuron0x8d1c6d0()*-0.858347);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bb98() {
   return (neuron0x8d1c8d0()*0.177466);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bbc0() {
   return (neuron0x8d1cad0()*0.103876);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bbe8() {
   return (neuron0x8d1ccd0()*0.648345);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bc10() {
   return (neuron0x8d1cee8()*-0.341278);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bc38() {
   return (neuron0x8d1d100()*0.673643);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bc60() {
   return (neuron0x8d1d318()*0.0265157);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bc88() {
   return (neuron0x8d1d530()*-0.205138);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bcb0() {
   return (neuron0x8d1d730()*0.354522);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bef0() {
   return (neuron0x8d1c360()*-0.0168872);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bf18() {
   return (neuron0x8d1c4d0()*-0.461776);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bf40() {
   return (neuron0x8d1c6d0()*0.279255);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bf68() {
   return (neuron0x8d1c8d0()*0.237571);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bf90() {
   return (neuron0x8d1cad0()*0.279898);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bfb8() {
   return (neuron0x8d1ccd0()*-0.0112046);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2bfe0() {
   return (neuron0x8d1cee8()*0.653779);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c008() {
   return (neuron0x8d1d100()*0.0754138);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c030() {
   return (neuron0x8d1d318()*-0.0965079);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c058() {
   return (neuron0x8d1d530()*-0.23953);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c080() {
   return (neuron0x8d1d730()*0.422677);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c2c0() {
   return (neuron0x8d1c360()*0.0958448);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c2e8() {
   return (neuron0x8d1c4d0()*0.616593);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c310() {
   return (neuron0x8d1c6d0()*0.0619204);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c338() {
   return (neuron0x8d1c8d0()*-0.0689509);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c360() {
   return (neuron0x8d1cad0()*-0.267589);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c388() {
   return (neuron0x8d1ccd0()*0.786399);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c3b0() {
   return (neuron0x8d1cee8()*0.453741);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c3d8() {
   return (neuron0x8d1d100()*-0.138424);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c400() {
   return (neuron0x8d1d318()*0.468539);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c428() {
   return (neuron0x8d1d530()*0.0804688);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c450() {
   return (neuron0x8d1d730()*0.152448);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c690() {
   return (neuron0x8d1c360()*-0.123195);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c6b8() {
   return (neuron0x8d1c4d0()*0.412429);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c6e0() {
   return (neuron0x8d1c6d0()*-0.116358);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c708() {
   return (neuron0x8d1c8d0()*0.23265);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c730() {
   return (neuron0x8d1cad0()*0.426315);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c758() {
   return (neuron0x8d1ccd0()*0.0974531);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c780() {
   return (neuron0x8d1cee8()*-0.193881);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c7a8() {
   return (neuron0x8d1d100()*-0.0997774);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c7d0() {
   return (neuron0x8d1d318()*-0.341514);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c7f8() {
   return (neuron0x8d1d530()*0.0178254);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2c820() {
   return (neuron0x8d1d730()*-0.0187992);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23e10() {
   return (neuron0x8d1c360()*-0.267833);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23e38() {
   return (neuron0x8d1c4d0()*0.130547);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23e60() {
   return (neuron0x8d1c6d0()*0.44063);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23e88() {
   return (neuron0x8d1c8d0()*0.313764);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23eb0() {
   return (neuron0x8d1cad0()*0.255531);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d23ed8() {
   return (neuron0x8d1ccd0()*-0.00781978);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cc68() {
   return (neuron0x8d1cee8()*-0.208985);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cc90() {
   return (neuron0x8d1d100()*-0.160129);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ccb8() {
   return (neuron0x8d1d318()*0.00554238);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cce0() {
   return (neuron0x8d1d530()*-0.118494);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cd08() {
   return (neuron0x8d1d730()*0.0559686);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cf30() {
   return (neuron0x8d1c360()*0.123969);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cf58() {
   return (neuron0x8d1c4d0()*-0.339537);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cf80() {
   return (neuron0x8d1c6d0()*-0.0159142);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cfa8() {
   return (neuron0x8d1c8d0()*0.0832723);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cfd0() {
   return (neuron0x8d1cad0()*0.187078);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2cff8() {
   return (neuron0x8d1ccd0()*0.546497);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d020() {
   return (neuron0x8d1cee8()*0.198846);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d048() {
   return (neuron0x8d1d100()*-0.337353);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d070() {
   return (neuron0x8d1d318()*-0.0244057);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d098() {
   return (neuron0x8d1d530()*0.221028);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d0c0() {
   return (neuron0x8d1d730()*0.258688);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d300() {
   return (neuron0x8d1c360()*0.393442);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d328() {
   return (neuron0x8d1c4d0()*0.010877);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d350() {
   return (neuron0x8d1c6d0()*0.148115);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d378() {
   return (neuron0x8d1c8d0()*0.341295);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d3a0() {
   return (neuron0x8d1cad0()*-0.312229);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d3c8() {
   return (neuron0x8d1ccd0()*0.74543);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d3f0() {
   return (neuron0x8d1cee8()*-0.170877);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d418() {
   return (neuron0x8d1d100()*0.220184);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d440() {
   return (neuron0x8d1d318()*-0.0179622);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d468() {
   return (neuron0x8d1d530()*0.431915);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d490() {
   return (neuron0x8d1d730()*-0.116247);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d6d0() {
   return (neuron0x8d1c360()*0.731436);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d6f8() {
   return (neuron0x8d1c4d0()*0.641647);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d720() {
   return (neuron0x8d1c6d0()*-0.542264);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d748() {
   return (neuron0x8d1c8d0()*0.529596);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d770() {
   return (neuron0x8d1cad0()*-0.00550944);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d798() {
   return (neuron0x8d1ccd0()*-0.38861);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d7c0() {
   return (neuron0x8d1cee8()*-0.349933);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d7e8() {
   return (neuron0x8d1d100()*0.790336);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d810() {
   return (neuron0x8d1d318()*-0.232914);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d838() {
   return (neuron0x8d1d530()*0.380057);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2d860() {
   return (neuron0x8d1d730()*0.00368862);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2daa0() {
   return (neuron0x8d1c360()*-0.428721);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dac8() {
   return (neuron0x8d1c4d0()*-0.242859);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2daf0() {
   return (neuron0x8d1c6d0()*0.0950258);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2db18() {
   return (neuron0x8d1c8d0()*0.509857);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2db40() {
   return (neuron0x8d1cad0()*-0.465572);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2db68() {
   return (neuron0x8d1ccd0()*-0.114236);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2db90() {
   return (neuron0x8d1cee8()*0.0309622);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dbb8() {
   return (neuron0x8d1d100()*0.781722);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dbe0() {
   return (neuron0x8d1d318()*-0.241643);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dc08() {
   return (neuron0x8d1d530()*-0.268462);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dc30() {
   return (neuron0x8d1d730()*-0.285816);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2de70() {
   return (neuron0x8d1c360()*-0.289688);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2de98() {
   return (neuron0x8d1c4d0()*0.752057);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dec0() {
   return (neuron0x8d1c6d0()*-0.0300239);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dee8() {
   return (neuron0x8d1c8d0()*-0.59335);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2df10() {
   return (neuron0x8d1cad0()*0.120561);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2df38() {
   return (neuron0x8d1ccd0()*-4.35607);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2df60() {
   return (neuron0x8d1cee8()*0.0994521);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2df88() {
   return (neuron0x8d1d100()*-0.793826);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dfb0() {
   return (neuron0x8d1d318()*0.652441);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2dfd8() {
   return (neuron0x8d1d530()*-0.383924);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e000() {
   return (neuron0x8d1d730()*-0.365634);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e240() {
   return (neuron0x8d1c360()*0.600176);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e268() {
   return (neuron0x8d1c4d0()*0.0178967);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e290() {
   return (neuron0x8d1c6d0()*0.439194);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e2b8() {
   return (neuron0x8d1c8d0()*-0.228467);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e2e0() {
   return (neuron0x8d1cad0()*0.16973);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e308() {
   return (neuron0x8d1ccd0()*0.0359758);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e330() {
   return (neuron0x8d1cee8()*0.513683);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e358() {
   return (neuron0x8d1d100()*0.419375);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e380() {
   return (neuron0x8d1d318()*-0.174179);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e3a8() {
   return (neuron0x8d1d530()*0.149945);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e3d0() {
   return (neuron0x8d1d730()*0.203503);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e610() {
   return (neuron0x8d1c360()*0.0246227);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e638() {
   return (neuron0x8d1c4d0()*0.241949);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e660() {
   return (neuron0x8d1c6d0()*0.39086);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e688() {
   return (neuron0x8d1c8d0()*0.764711);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e6b0() {
   return (neuron0x8d1cad0()*-0.391997);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e6d8() {
   return (neuron0x8d1ccd0()*-0.966495);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e700() {
   return (neuron0x8d1cee8()*0.440538);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e728() {
   return (neuron0x8d1d100()*-0.0354748);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e750() {
   return (neuron0x8d1d318()*-0.0487593);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e778() {
   return (neuron0x8d1d530()*-0.117392);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e7a0() {
   return (neuron0x8d1d730()*-0.144732);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2e9e0() {
   return (neuron0x8d1c360()*0.296022);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ea08() {
   return (neuron0x8d1c4d0()*-0.214777);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ea30() {
   return (neuron0x8d1c6d0()*0.173598);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ea58() {
   return (neuron0x8d1c8d0()*0.324456);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ea80() {
   return (neuron0x8d1cad0()*-0.177608);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2eaa8() {
   return (neuron0x8d1ccd0()*-0.0320471);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ead0() {
   return (neuron0x8d1cee8()*-0.226959);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2eaf8() {
   return (neuron0x8d1d100()*0.594887);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2eb20() {
   return (neuron0x8d1d318()*0.0323879);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2eb48() {
   return (neuron0x8d1d530()*-0.0178286);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2eb70() {
   return (neuron0x8d1d730()*0.350462);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2edb0() {
   return (neuron0x8d1c360()*0.0127648);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2edd8() {
   return (neuron0x8d1c4d0()*-0.306094);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ee00() {
   return (neuron0x8d1c6d0()*-0.383044);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ee28() {
   return (neuron0x8d1c8d0()*1.60663);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ee50() {
   return (neuron0x8d1cad0()*-0.0705849);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ee78() {
   return (neuron0x8d1ccd0()*0.790802);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2eea0() {
   return (neuron0x8d1cee8()*-0.251957);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2eec8() {
   return (neuron0x8d1d100()*0.985604);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2eef0() {
   return (neuron0x8d1d318()*0.153144);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ef18() {
   return (neuron0x8d1d530()*0.0593615);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ef40() {
   return (neuron0x8d1d730()*0.164588);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2f180() {
   return (neuron0x8d1c360()*0.0423545);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d268c8() {
   return (neuron0x8d1c4d0()*-0.931994);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d268f0() {
   return (neuron0x8d1c6d0()*0.241273);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26918() {
   return (neuron0x8d1c8d0()*1.05645);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26b48() {
   return (neuron0x8d1cad0()*0.014436);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26b70() {
   return (neuron0x8d1ccd0()*2.41522);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26da0() {
   return (neuron0x8d1cee8()*0.295759);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d26dc8() {
   return (neuron0x8d1d100()*-1.50588);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27000() {
   return (neuron0x8d1d318()*-0.115252);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27028() {
   return (neuron0x8d1d530()*-0.186479);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d27050() {
   return (neuron0x8d1d730()*0.115946);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30070() {
   return (neuron0x8d1c360()*-0.301705);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30098() {
   return (neuron0x8d1c4d0()*0.271719);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d300c0() {
   return (neuron0x8d1c6d0()*0.0115346);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d300e8() {
   return (neuron0x8d1c8d0()*0.0697076);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30110() {
   return (neuron0x8d1cad0()*-0.526362);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30138() {
   return (neuron0x8d1ccd0()*0.0987558);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30160() {
   return (neuron0x8d1cee8()*0.16757);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30188() {
   return (neuron0x8d1d100()*0.453755);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d301b0() {
   return (neuron0x8d1d318()*-0.248664);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d301d8() {
   return (neuron0x8d1d530()*0.136386);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30200() {
   return (neuron0x8d1d730()*0.381162);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30428() {
   return (neuron0x8d1c360()*-0.142567);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30450() {
   return (neuron0x8d1c4d0()*1.08027);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30478() {
   return (neuron0x8d1c6d0()*0.124023);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d304a0() {
   return (neuron0x8d1c8d0()*-0.21807);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d304c8() {
   return (neuron0x8d1cad0()*0.0354693);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d304f0() {
   return (neuron0x8d1ccd0()*0.770013);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30518() {
   return (neuron0x8d1cee8()*0.130977);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30540() {
   return (neuron0x8d1d100()*0.505592);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30568() {
   return (neuron0x8d1d318()*-0.0379587);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30590() {
   return (neuron0x8d1d530()*0.193781);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d305b8() {
   return (neuron0x8d1d730()*0.248931);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d307f8() {
   return (neuron0x8d1c360()*-0.27326);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30820() {
   return (neuron0x8d1c4d0()*-0.288993);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30848() {
   return (neuron0x8d1c6d0()*0.296883);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30870() {
   return (neuron0x8d1c8d0()*-0.0889751);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30898() {
   return (neuron0x8d1cad0()*0.524952);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d308c0() {
   return (neuron0x8d1ccd0()*0.564814);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d308e8() {
   return (neuron0x8d1cee8()*-0.223034);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30910() {
   return (neuron0x8d1d100()*-0.342266);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30938() {
   return (neuron0x8d1d318()*0.290006);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30960() {
   return (neuron0x8d1d530()*0.100059);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30988() {
   return (neuron0x8d1d730()*0.814177);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30bc8() {
   return (neuron0x8d1c360()*-0.203736);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30bf0() {
   return (neuron0x8d1c4d0()*0.0434091);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30c18() {
   return (neuron0x8d1c6d0()*0.0111925);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30c40() {
   return (neuron0x8d1c8d0()*0.224522);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30c68() {
   return (neuron0x8d1cad0()*-0.158968);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30c90() {
   return (neuron0x8d1ccd0()*0.0438015);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30cb8() {
   return (neuron0x8d1cee8()*0.0216948);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30ce0() {
   return (neuron0x8d1d100()*0.399336);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30d08() {
   return (neuron0x8d1d318()*0.0953995);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30d30() {
   return (neuron0x8d1d530()*-0.526634);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30d58() {
   return (neuron0x8d1d730()*-0.40011);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30f98() {
   return (neuron0x8d1c360()*-0.187734);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30fc0() {
   return (neuron0x8d1c4d0()*-0.393147);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d30fe8() {
   return (neuron0x8d1c6d0()*-0.375943);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31010() {
   return (neuron0x8d1c8d0()*-0.402414);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31038() {
   return (neuron0x8d1cad0()*0.0536433);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31060() {
   return (neuron0x8d1ccd0()*0.787515);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31088() {
   return (neuron0x8d1cee8()*0.579076);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d310b0() {
   return (neuron0x8d1d100()*0.159595);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d310d8() {
   return (neuron0x8d1d318()*0.213337);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31100() {
   return (neuron0x8d1d530()*0.225742);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31128() {
   return (neuron0x8d1d730()*0.0215825);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31368() {
   return (neuron0x8d1c360()*-0.860015);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31390() {
   return (neuron0x8d1c4d0()*0.289271);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d313b8() {
   return (neuron0x8d1c6d0()*-0.115059);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d313e0() {
   return (neuron0x8d1c8d0()*0.301811);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31408() {
   return (neuron0x8d1cad0()*0.466405);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31430() {
   return (neuron0x8d1ccd0()*-0.102011);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31458() {
   return (neuron0x8d1cee8()*0.15797);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31480() {
   return (neuron0x8d1d100()*0.240785);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d314a8() {
   return (neuron0x8d1d318()*0.394121);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d314d0() {
   return (neuron0x8d1d530()*-0.016752);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d314f8() {
   return (neuron0x8d1d730()*0.186692);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31738() {
   return (neuron0x8d1c360()*0.115186);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31760() {
   return (neuron0x8d1c4d0()*-0.860134);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31788() {
   return (neuron0x8d1c6d0()*0.182535);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d317b0() {
   return (neuron0x8d1c8d0()*-0.65051);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d317d8() {
   return (neuron0x8d1cad0()*-0.165746);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31800() {
   return (neuron0x8d1ccd0()*0.0447119);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31828() {
   return (neuron0x8d1cee8()*-0.814651);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31850() {
   return (neuron0x8d1d100()*-0.767387);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31878() {
   return (neuron0x8d1d318()*-0.542263);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d318a0() {
   return (neuron0x8d1d530()*0.0655167);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d318c8() {
   return (neuron0x8d1d730()*-0.777181);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31b08() {
   return (neuron0x8d1c360()*-0.179678);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31b30() {
   return (neuron0x8d1c4d0()*0.253197);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31b58() {
   return (neuron0x8d1c6d0()*-6.43636);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31b80() {
   return (neuron0x8d1c8d0()*-0.0633309);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31ba8() {
   return (neuron0x8d1cad0()*-0.0148436);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31bd0() {
   return (neuron0x8d1ccd0()*-0.044634);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31bf8() {
   return (neuron0x8d1cee8()*-0.105431);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31c20() {
   return (neuron0x8d1d100()*-0.181897);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31c48() {
   return (neuron0x8d1d318()*-0.273759);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31c70() {
   return (neuron0x8d1d530()*-0.3142);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31c98() {
   return (neuron0x8d1d730()*0.0208737);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31ed8() {
   return (neuron0x8d1c360()*-0.319852);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31f00() {
   return (neuron0x8d1c4d0()*-0.700509);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31f28() {
   return (neuron0x8d1c6d0()*-0.731762);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31f50() {
   return (neuron0x8d1c8d0()*0.612771);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31f78() {
   return (neuron0x8d1cad0()*0.359136);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31fa0() {
   return (neuron0x8d1ccd0()*-0.577221);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31fc8() {
   return (neuron0x8d1cee8()*-0.195297);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d31ff0() {
   return (neuron0x8d1d100()*-0.560769);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32018() {
   return (neuron0x8d1d318()*0.623132);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32040() {
   return (neuron0x8d1d530()*-0.142806);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32068() {
   return (neuron0x8d1d730()*-0.00490249);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d322a8() {
   return (neuron0x8d1c360()*-0.153765);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d322d0() {
   return (neuron0x8d1c4d0()*0.269844);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d322f8() {
   return (neuron0x8d1c6d0()*0.302773);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32320() {
   return (neuron0x8d1c8d0()*-0.0879967);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32348() {
   return (neuron0x8d1cad0()*-0.162787);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32370() {
   return (neuron0x8d1ccd0()*-0.391507);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32398() {
   return (neuron0x8d1cee8()*-0.495904);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d323c0() {
   return (neuron0x8d1d100()*-0.204776);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d323e8() {
   return (neuron0x8d1d318()*-0.0665678);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32410() {
   return (neuron0x8d1d530()*0.230766);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32438() {
   return (neuron0x8d1d730()*-0.197856);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32678() {
   return (neuron0x8d1c360()*-0.120528);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d326a0() {
   return (neuron0x8d1c4d0()*-0.940609);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d326c8() {
   return (neuron0x8d1c6d0()*-0.52501);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d326f0() {
   return (neuron0x8d1c8d0()*1.06841);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32718() {
   return (neuron0x8d1cad0()*-0.0643315);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32740() {
   return (neuron0x8d1ccd0()*1.29854);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32768() {
   return (neuron0x8d1cee8()*-0.240174);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32790() {
   return (neuron0x8d1d100()*-1.70289);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d327b8() {
   return (neuron0x8d1d318()*0.63804);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d327e0() {
   return (neuron0x8d1d530()*0.492874);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32808() {
   return (neuron0x8d1d730()*0.353883);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32a48() {
   return (neuron0x8d1c360()*-0.0546454);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32a70() {
   return (neuron0x8d1c4d0()*-0.134003);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32a98() {
   return (neuron0x8d1c6d0()*0.491169);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32ac0() {
   return (neuron0x8d1c8d0()*-0.215755);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32ae8() {
   return (neuron0x8d1cad0()*-0.575887);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32b10() {
   return (neuron0x8d1ccd0()*0.704525);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32b38() {
   return (neuron0x8d1cee8()*0.0667829);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32b60() {
   return (neuron0x8d1d100()*0.482034);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32b88() {
   return (neuron0x8d1d318()*0.384542);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32bb0() {
   return (neuron0x8d1d530()*0.00694406);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32bd8() {
   return (neuron0x8d1d730()*0.395603);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32e18() {
   return (neuron0x8d1c360()*0.381151);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32e40() {
   return (neuron0x8d1c4d0()*-0.37631);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32e68() {
   return (neuron0x8d1c6d0()*0.261435);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32e90() {
   return (neuron0x8d1c8d0()*0.55876);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32eb8() {
   return (neuron0x8d1cad0()*-0.0446876);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32ee0() {
   return (neuron0x8d1ccd0()*1.16596);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32f08() {
   return (neuron0x8d1cee8()*-0.159292);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32f30() {
   return (neuron0x8d1d100()*-0.0940422);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32f58() {
   return (neuron0x8d1d318()*-0.744074);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32f80() {
   return (neuron0x8d1d530()*-0.162002);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d32fa8() {
   return (neuron0x8d1d730()*0.314315);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d331e8() {
   return (neuron0x8d1c360()*-0.404913);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33210() {
   return (neuron0x8d1c4d0()*0.486662);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33238() {
   return (neuron0x8d1c6d0()*-0.198251);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33260() {
   return (neuron0x8d1c8d0()*0.380862);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33288() {
   return (neuron0x8d1cad0()*-0.424175);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d332b0() {
   return (neuron0x8d1ccd0()*0.0283857);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d332d8() {
   return (neuron0x8d1cee8()*0.372236);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33300() {
   return (neuron0x8d1d100()*0.124801);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33328() {
   return (neuron0x8d1d318()*0.3515);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33350() {
   return (neuron0x8d1d530()*-0.157458);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33378() {
   return (neuron0x8d1d730()*-0.29179);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d335b8() {
   return (neuron0x8d1c360()*0.152743);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d335e0() {
   return (neuron0x8d1c4d0()*-0.0387366);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33608() {
   return (neuron0x8d1c6d0()*0.755959);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33630() {
   return (neuron0x8d1c8d0()*0.663684);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33658() {
   return (neuron0x8d1cad0()*0.306965);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33680() {
   return (neuron0x8d1ccd0()*0.805042);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d336a8() {
   return (neuron0x8d1cee8()*0.379531);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d336d0() {
   return (neuron0x8d1d100()*0.114264);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d336f8() {
   return (neuron0x8d1d318()*0.311728);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33720() {
   return (neuron0x8d1d530()*-0.208686);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33748() {
   return (neuron0x8d1d730()*0.155765);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33988() {
   return (neuron0x8d1c360()*0.366226);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d339b0() {
   return (neuron0x8d1c4d0()*-0.125595);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d339d8() {
   return (neuron0x8d1c6d0()*-0.0650177);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33a00() {
   return (neuron0x8d1c8d0()*0.358722);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33a28() {
   return (neuron0x8d1cad0()*-0.565749);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33a50() {
   return (neuron0x8d1ccd0()*-0.0722123);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33a78() {
   return (neuron0x8d1cee8()*-0.294479);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33aa0() {
   return (neuron0x8d1d100()*0.174786);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33ac8() {
   return (neuron0x8d1d318()*0.208005);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33af0() {
   return (neuron0x8d1d530()*0.0465352);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33b18() {
   return (neuron0x8d1d730()*0.376708);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33d58() {
   return (neuron0x8d1c360()*0.24568);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33d80() {
   return (neuron0x8d1c4d0()*0.325727);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33da8() {
   return (neuron0x8d1c6d0()*-0.134232);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33dd0() {
   return (neuron0x8d1c8d0()*0.64359);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33df8() {
   return (neuron0x8d1cad0()*0.0570603);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33e20() {
   return (neuron0x8d1ccd0()*-0.971742);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33e48() {
   return (neuron0x8d1cee8()*0.0534472);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33e70() {
   return (neuron0x8d1d100()*0.800953);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33e98() {
   return (neuron0x8d1d318()*-0.600112);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33ec0() {
   return (neuron0x8d1d530()*-1.1951);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d33ee8() {
   return (neuron0x8d1d730()*0.181281);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34128() {
   return (neuron0x8d1c360()*-0.171261);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34150() {
   return (neuron0x8d1c4d0()*0.727436);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34178() {
   return (neuron0x8d1c6d0()*-0.377074);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d341a0() {
   return (neuron0x8d1c8d0()*-0.127625);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d341c8() {
   return (neuron0x8d1cad0()*-0.276299);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d341f0() {
   return (neuron0x8d1ccd0()*0.229772);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34218() {
   return (neuron0x8d1cee8()*-0.227663);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34240() {
   return (neuron0x8d1d100()*0.0717457);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34268() {
   return (neuron0x8d1d318()*0.0905112);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34290() {
   return (neuron0x8d1d530()*0.323314);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d342b8() {
   return (neuron0x8d1d730()*-0.223115);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d344f8() {
   return (neuron0x8d1c360()*-0.183512);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34520() {
   return (neuron0x8d1c4d0()*0.282769);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34548() {
   return (neuron0x8d1c6d0()*0.0580168);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34570() {
   return (neuron0x8d1c8d0()*0.254974);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34598() {
   return (neuron0x8d1cad0()*-0.187392);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d345c0() {
   return (neuron0x8d1ccd0()*-0.547319);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d345e8() {
   return (neuron0x8d1cee8()*-0.150281);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34610() {
   return (neuron0x8d1d100()*-0.160741);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34638() {
   return (neuron0x8d1d318()*0.185085);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34660() {
   return (neuron0x8d1d530()*-0.337518);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34688() {
   return (neuron0x8d1d730()*-0.20518);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d348c8() {
   return (neuron0x8d1c360()*0.47888);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d348f0() {
   return (neuron0x8d1c4d0()*-0.100079);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34918() {
   return (neuron0x8d1c6d0()*-0.175078);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34940() {
   return (neuron0x8d1c8d0()*0.0614354);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34968() {
   return (neuron0x8d1cad0()*-0.165393);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34990() {
   return (neuron0x8d1ccd0()*0.556152);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d349b8() {
   return (neuron0x8d1cee8()*-0.126824);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d349e0() {
   return (neuron0x8d1d100()*0.0537131);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34a08() {
   return (neuron0x8d1d318()*0.26092);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34a30() {
   return (neuron0x8d1d530()*0.213572);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34a58() {
   return (neuron0x8d1d730()*0.0252936);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34c98() {
   return (neuron0x8d1c360()*0.0335935);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34cc0() {
   return (neuron0x8d1c4d0()*-0.289365);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34ce8() {
   return (neuron0x8d1c6d0()*-0.408332);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34d10() {
   return (neuron0x8d1c8d0()*-0.104424);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34d38() {
   return (neuron0x8d1cad0()*-0.0895035);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34d60() {
   return (neuron0x8d1ccd0()*0.173791);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34d88() {
   return (neuron0x8d1cee8()*0.0736774);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34db0() {
   return (neuron0x8d1d100()*0.447308);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34dd8() {
   return (neuron0x8d1d318()*-0.438375);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34e00() {
   return (neuron0x8d1d530()*0.534238);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d34e28() {
   return (neuron0x8d1d730()*-0.197967);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35068() {
   return (neuron0x8d1c360()*0.304393);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35090() {
   return (neuron0x8d1c4d0()*-0.506391);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d350b8() {
   return (neuron0x8d1c6d0()*0.19155);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d350e0() {
   return (neuron0x8d1c8d0()*-0.352273);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35108() {
   return (neuron0x8d1cad0()*-0.0252849);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35130() {
   return (neuron0x8d1ccd0()*-0.81507);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35158() {
   return (neuron0x8d1cee8()*-0.663291);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35180() {
   return (neuron0x8d1d100()*0.532782);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d351a8() {
   return (neuron0x8d1d318()*-0.820958);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d351d0() {
   return (neuron0x8d1d530()*-0.210472);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d351f8() {
   return (neuron0x8d1d730()*0.334307);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35438() {
   return (neuron0x8d1c360()*-0.441235);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35460() {
   return (neuron0x8d1c4d0()*1.01941);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35488() {
   return (neuron0x8d1c6d0()*-0.214364);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d354b0() {
   return (neuron0x8d1c8d0()*0.484038);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d354d8() {
   return (neuron0x8d1cad0()*-0.155155);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35500() {
   return (neuron0x8d1ccd0()*-0.235571);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35528() {
   return (neuron0x8d1cee8()*0.202434);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35550() {
   return (neuron0x8d1d100()*0.0271525);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35578() {
   return (neuron0x8d1d318()*-0.145327);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d355a0() {
   return (neuron0x8d1d530()*-0.0993601);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d355c8() {
   return (neuron0x8d1d730()*0.15649);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35808() {
   return (neuron0x8d1c360()*-0.235108);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35830() {
   return (neuron0x8d1c4d0()*-0.762042);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35858() {
   return (neuron0x8d1c6d0()*1.42583);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35880() {
   return (neuron0x8d1c8d0()*0.447414);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d358a8() {
   return (neuron0x8d1cad0()*0.0323556);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d358d0() {
   return (neuron0x8d1ccd0()*-0.225312);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d358f8() {
   return (neuron0x8d1cee8()*0.205712);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35920() {
   return (neuron0x8d1d100()*-0.205988);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35948() {
   return (neuron0x8d1d318()*-1.08489);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35970() {
   return (neuron0x8d1d530()*-0.0090463);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35998() {
   return (neuron0x8d1d730()*0.289381);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35bd8() {
   return (neuron0x8d1c360()*0.0635873);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35c00() {
   return (neuron0x8d1c4d0()*-0.324087);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35c28() {
   return (neuron0x8d1c6d0()*-0.239278);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35c50() {
   return (neuron0x8d1c8d0()*0.409267);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35c78() {
   return (neuron0x8d1cad0()*-0.114119);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35ca0() {
   return (neuron0x8d1ccd0()*0.152488);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35cc8() {
   return (neuron0x8d1cee8()*-0.386065);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35cf0() {
   return (neuron0x8d1d100()*-0.316274);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35d18() {
   return (neuron0x8d1d318()*-0.221927);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35d40() {
   return (neuron0x8d1d530()*0.251501);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35d68() {
   return (neuron0x8d1d730()*0.0137787);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35fa8() {
   return (neuron0x8d1c360()*-0.880772);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35fd0() {
   return (neuron0x8d1c4d0()*1.06159);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d35ff8() {
   return (neuron0x8d1c6d0()*-0.228572);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36020() {
   return (neuron0x8d1c8d0()*0.516287);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36048() {
   return (neuron0x8d1cad0()*-0.0436533);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36070() {
   return (neuron0x8d1ccd0()*-0.889664);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36098() {
   return (neuron0x8d1cee8()*-0.363829);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d360c0() {
   return (neuron0x8d1d100()*-1.27052);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d360e8() {
   return (neuron0x8d1d318()*-0.0880518);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36110() {
   return (neuron0x8d1d530()*-0.0966736);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36138() {
   return (neuron0x8d1d730()*-0.19143);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36378() {
   return (neuron0x8d1c360()*-0.45242);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d363a0() {
   return (neuron0x8d1c4d0()*-0.529677);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d363c8() {
   return (neuron0x8d1c6d0()*0.367905);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d363f0() {
   return (neuron0x8d1c8d0()*-0.011988);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36418() {
   return (neuron0x8d1cad0()*-0.946722);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36440() {
   return (neuron0x8d1ccd0()*-0.348838);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36468() {
   return (neuron0x8d1cee8()*-0.0150885);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36490() {
   return (neuron0x8d1d100()*-0.106371);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d364b8() {
   return (neuron0x8d1d318()*0.689808);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d364e0() {
   return (neuron0x8d1d530()*-0.149822);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36508() {
   return (neuron0x8d1d730()*-0.440847);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36748() {
   return (neuron0x8d1c360()*0.410819);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36770() {
   return (neuron0x8d1c4d0()*0.530031);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36798() {
   return (neuron0x8d1c6d0()*0.0551634);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d367c0() {
   return (neuron0x8d1c8d0()*0.286035);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d367e8() {
   return (neuron0x8d1cad0()*0.103569);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36810() {
   return (neuron0x8d1ccd0()*0.266151);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36838() {
   return (neuron0x8d1cee8()*-0.0568397);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36860() {
   return (neuron0x8d1d100()*-0.178117);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36888() {
   return (neuron0x8d1d318()*-0.394131);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d368b0() {
   return (neuron0x8d1d530()*0.233015);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d368d8() {
   return (neuron0x8d1d730()*-0.389544);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36b18() {
   return (neuron0x8d1c360()*-0.127425);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36b40() {
   return (neuron0x8d1c4d0()*-0.495197);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36b68() {
   return (neuron0x8d1c6d0()*0.252503);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36b90() {
   return (neuron0x8d1c8d0()*-0.0441711);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36bb8() {
   return (neuron0x8d1cad0()*-0.269016);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36be0() {
   return (neuron0x8d1ccd0()*-0.0200403);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36c08() {
   return (neuron0x8d1cee8()*0.117719);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36c30() {
   return (neuron0x8d1d100()*-0.51026);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36c58() {
   return (neuron0x8d1d318()*-0.146172);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36c80() {
   return (neuron0x8d1d530()*-0.205918);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36ca8() {
   return (neuron0x8d1d730()*0.296527);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36ee8() {
   return (neuron0x8d1c360()*-0.0264845);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d36f10() {
   return (neuron0x8d1c4d0()*-0.115013);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a8c8() {
   return (neuron0x8d1c6d0()*-0.106546);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a8f0() {
   return (neuron0x8d1c8d0()*-0.130752);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a918() {
   return (neuron0x8d1cad0()*0.646526);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a940() {
   return (neuron0x8d1ccd0()*-0.185131);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a968() {
   return (neuron0x8d1cee8()*-0.128483);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a990() {
   return (neuron0x8d1d100()*0.294342);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a9b8() {
   return (neuron0x8d1d318()*-0.74535);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2a9e0() {
   return (neuron0x8d1d530()*0.647422);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2aa08() {
   return (neuron0x8d1d730()*0.496073);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ac48() {
   return (neuron0x8d1c360()*0.522886);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ac70() {
   return (neuron0x8d1c4d0()*-0.265377);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ac98() {
   return (neuron0x8d1c6d0()*0.311257);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2acc0() {
   return (neuron0x8d1c8d0()*-0.402786);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ace8() {
   return (neuron0x8d1cad0()*-0.171758);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ad10() {
   return (neuron0x8d1ccd0()*0.237652);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ad38() {
   return (neuron0x8d1cee8()*0.505801);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ad60() {
   return (neuron0x8d1d100()*0.724705);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2ad88() {
   return (neuron0x8d1d318()*0.361183);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2adb0() {
   return (neuron0x8d1d530()*-0.685491);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2add8() {
   return (neuron0x8d1d730()*0.243827);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b018() {
   return (neuron0x8d1c360()*0.28799);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b040() {
   return (neuron0x8d1c4d0()*-0.798009);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b068() {
   return (neuron0x8d1c6d0()*-0.277904);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b090() {
   return (neuron0x8d1c8d0()*0.266048);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b0b8() {
   return (neuron0x8d1cad0()*0.217979);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b0e0() {
   return (neuron0x8d1ccd0()*-0.00627927);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b108() {
   return (neuron0x8d1cee8()*-0.247815);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b130() {
   return (neuron0x8d1d100()*0.371834);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b158() {
   return (neuron0x8d1d318()*-0.86236);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b180() {
   return (neuron0x8d1d530()*0.33338);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b1a8() {
   return (neuron0x8d1d730()*0.77697);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b3e8() {
   return (neuron0x8d1c360()*-0.144);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b410() {
   return (neuron0x8d1c4d0()*0.60376);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b438() {
   return (neuron0x8d1c6d0()*-0.633657);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b460() {
   return (neuron0x8d1c8d0()*-0.190752);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b488() {
   return (neuron0x8d1cad0()*-0.282378);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b4b0() {
   return (neuron0x8d1ccd0()*0.548802);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b4d8() {
   return (neuron0x8d1cee8()*0.0906814);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b500() {
   return (neuron0x8d1d100()*-0.611262);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b528() {
   return (neuron0x8d1d318()*0.036138);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b550() {
   return (neuron0x8d1d530()*-0.025379);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b578() {
   return (neuron0x8d1d730()*0.210135);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b7b8() {
   return (neuron0x8d1c360()*-0.219129);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b7e0() {
   return (neuron0x8d1c4d0()*-0.297172);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b808() {
   return (neuron0x8d1c6d0()*-0.139611);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b830() {
   return (neuron0x8d1c8d0()*0.0592093);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b858() {
   return (neuron0x8d1cad0()*-0.678676);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b880() {
   return (neuron0x8d1ccd0()*-0.869378);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d2b8a8() {
   return (neuron0x8d1cee8()*0.280353);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d38f40() {
   return (neuron0x8d1d100()*-0.811977);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d38f68() {
   return (neuron0x8d1d318()*-0.271909);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d38f90() {
   return (neuron0x8d1d530()*0.106922);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d38fb8() {
   return (neuron0x8d1d730()*0.539569);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d391f8() {
   return (neuron0x8d1c360()*-0.0136123);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39220() {
   return (neuron0x8d1c4d0()*-0.702015);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39248() {
   return (neuron0x8d1c6d0()*-0.0755166);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39270() {
   return (neuron0x8d1c8d0()*-0.80016);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39298() {
   return (neuron0x8d1cad0()*0.0208429);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d392c0() {
   return (neuron0x8d1ccd0()*1.36385);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d392e8() {
   return (neuron0x8d1cee8()*0.0472316);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39310() {
   return (neuron0x8d1d100()*0.600728);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39338() {
   return (neuron0x8d1d318()*0.117472);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39360() {
   return (neuron0x8d1d530()*0.543128);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39388() {
   return (neuron0x8d1d730()*0.0800466);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d395c8() {
   return (neuron0x8d1c360()*0.161645);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d395f0() {
   return (neuron0x8d1c4d0()*0.620522);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39618() {
   return (neuron0x8d1c6d0()*0.555208);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39640() {
   return (neuron0x8d1c8d0()*-0.506949);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39668() {
   return (neuron0x8d1cad0()*0.128259);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39690() {
   return (neuron0x8d1ccd0()*0.183331);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d396b8() {
   return (neuron0x8d1cee8()*-0.302403);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d396e0() {
   return (neuron0x8d1d100()*0.331897);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39708() {
   return (neuron0x8d1d318()*0.105502);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39730() {
   return (neuron0x8d1d530()*0.13454);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39758() {
   return (neuron0x8d1d730()*-0.238108);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d398a8() {
   return (neuron0x8d1da70()*-0.668705);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d398d0() {
   return (neuron0x8d1ddb8()*0.0205864);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d398f8() {
   return (neuron0x8d1e1f8()*-0.152418);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39920() {
   return (neuron0x8d1e568()*0.770186);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39948() {
   return (neuron0x8d1e920()*-0.433947);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39970() {
   return (neuron0x8d1ecd8()*-0.629041);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39998() {
   return (neuron0x8d1f270()*-0.408784);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d399c0() {
   return (neuron0x8d1f5e0()*-0.607719);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d399e8() {
   return (neuron0x8d1f9b0()*0.955133);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39a10() {
   return (neuron0x8d1fd80()*-0.367296);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39a38() {
   return (neuron0x8d20150()*-0.758406);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39a60() {
   return (neuron0x8d20520()*0.362701);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39a88() {
   return (neuron0x8d1f068()*0.601978);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39ab0() {
   return (neuron0x8d20c80()*-0.710379);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39ad8() {
   return (neuron0x8d21038()*1.09514);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39b00() {
   return (neuron0x8d213f0()*-0.351449);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39bb0() {
   return (neuron0x8d21d98()*-0.513511);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39bd8() {
   return (neuron0x8d230d8()*0.164083);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39c00() {
   return (neuron0x8d23250()*0.174059);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39c28() {
   return (neuron0x8d23530()*-0.456687);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39c50() {
   return (neuron0x8d23858()*0.57444);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39c78() {
   return (neuron0x8d23c10()*-0.142342);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39ca0() {
   return (neuron0x8d24058()*-0.0313656);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39cc8() {
   return (neuron0x8d24410()*0.760717);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39cf0() {
   return (neuron0x8d20990()*0.685663);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39d18() {
   return (neuron0x8d24fa8()*-0.0951417);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39d40() {
   return (neuron0x8d25378()*0.794887);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39d68() {
   return (neuron0x8d25748()*0.306461);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39d90() {
   return (neuron0x8d25b18()*0.326391);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39db8() {
   return (neuron0x8d25ee8()*0.377772);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39de0() {
   return (neuron0x8d262b8()*0.392238);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39e08() {
   return (neuron0x8d26688()*-0.383875);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39b28() {
   return (neuron0x8d27420()*-1.27548);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39b50() {
   return (neuron0x8d27548()*0.206274);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39b78() {
   return (neuron0x8d27820()*0.537445);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39f38() {
   return (neuron0x8d27bd8()*-0.922688);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39f60() {
   return (neuron0x8d27f90()*0.465995);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39f88() {
   return (neuron0x8d28348()*0.111645);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39fb0() {
   return (neuron0x8d28718()*0.320362);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39fd8() {
   return (neuron0x8d28ae8()*0.297154);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a000() {
   return (neuron0x8d28eb8()*-0.139291);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a028() {
   return (neuron0x8d29288()*-0.766504);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a050() {
   return (neuron0x8d29658()*-0.430933);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a078() {
   return (neuron0x8d29a28()*-0.730314);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a0a0() {
   return (neuron0x8d29df8()*0.980018);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a0c8() {
   return (neuron0x8d2a1c8()*-1.08499);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a0f0() {
   return (neuron0x8d2a598()*-0.53324);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a118() {
   return (neuron0x8d24750()*-0.165042);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a140() {
   return (neuron0x8d24b20()*-0.19021);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a168() {
   return (neuron0x8d2b920()*-0.415483);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a190() {
   return (neuron0x8d2bcd8()*-0.0217958);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a1b8() {
   return (neuron0x8d2c0a8()*0.569744);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a1e0() {
   return (neuron0x8d2c478()*0.135927);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a208() {
   return (neuron0x8d2c848()*-0.294611);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a230() {
   return (neuron0x8d2cd30()*-0.442232);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a258() {
   return (neuron0x8d2d0e8()*0.35701);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a280() {
   return (neuron0x8d2d4b8()*-0.732415);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a2a8() {
   return (neuron0x8d2d888()*1.01046);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a2d0() {
   return (neuron0x8d2dc58()*-0.765798);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a2f8() {
   return (neuron0x8d2e028()*0.135595);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a320() {
   return (neuron0x8d2e3f8()*0.867356);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a348() {
   return (neuron0x8d2e7c8()*0.444364);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a370() {
   return (neuron0x8d2eb98()*-1.14541);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a398() {
   return (neuron0x8d2ef68()*1.055);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d1d988() {
   return (neuron0x8d27280()*0.00476024);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39e30() {
   return (neuron0x8d30228()*0.381032);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39e58() {
   return (neuron0x8d305e0()*-0.451527);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39e80() {
   return (neuron0x8d309b0()*0.575571);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39ea8() {
   return (neuron0x8d30d80()*-0.214019);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39ed0() {
   return (neuron0x8d31150()*0.548864);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d39ef8() {
   return (neuron0x8d31520()*0.789559);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a5c8() {
   return (neuron0x8d318f0()*0.788733);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a5f0() {
   return (neuron0x8d31cc0()*0.563178);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a618() {
   return (neuron0x8d32090()*0.220841);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a640() {
   return (neuron0x8d32460()*0.43455);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a668() {
   return (neuron0x8d32830()*-0.798773);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a690() {
   return (neuron0x8d32c00()*-0.180093);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a6b8() {
   return (neuron0x8d32fd0()*-0.0156568);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a6e0() {
   return (neuron0x8d333a0()*-0.657838);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a708() {
   return (neuron0x8d33770()*0.344647);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a730() {
   return (neuron0x8d33b40()*0.990222);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a758() {
   return (neuron0x8d33f10()*0.192828);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a780() {
   return (neuron0x8d342e0()*0.532481);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a7a8() {
   return (neuron0x8d346b0()*-0.365824);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a7d0() {
   return (neuron0x8d34a80()*-0.0279564);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a7f8() {
   return (neuron0x8d34e50()*-0.857915);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a820() {
   return (neuron0x8d35220()*-0.670075);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a848() {
   return (neuron0x8d355f0()*0.750865);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a870() {
   return (neuron0x8d359c0()*-0.249196);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a898() {
   return (neuron0x8d35d90()*-0.465285);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a8c0() {
   return (neuron0x8d36160()*-0.554267);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a8e8() {
   return (neuron0x8d36530()*0.364921);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a910() {
   return (neuron0x8d36900()*-0.404295);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a938() {
   return (neuron0x8d36cd0()*0.389724);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a960() {
   return (neuron0x8d2aa30()*0.654386);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a988() {
   return (neuron0x8d2ae00()*0.363888);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a9b0() {
   return (neuron0x8d2b1d0()*0.347274);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3a9d8() {
   return (neuron0x8d2b5a0()*-0.605751);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3aa00() {
   return (neuron0x8d38fe0()*0.134953);
}

double NNParaElectronXYCorrectionClusterDeltaX::synapse0x8d3aa28() {
   return (neuron0x8d393b0()*-0.461188);
}

