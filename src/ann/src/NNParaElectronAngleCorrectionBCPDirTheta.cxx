#include "NNParaElectronAngleCorrectionBCPDirTheta.h"
#include <cmath>

double NNParaElectronAngleCorrectionBCPDirTheta::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10,double in11,double in12,double in13,double in14) {
   input0 = (in0 - 0.704494)/0.092045;
   input1 = (in1 - -0.00255061)/0.258907;
   input2 = (in2 - -3.42702)/31.587;
   input3 = (in3 - -0.528546)/56.5134;
   input4 = (in4 - 2626.33)/1277.16;
   input5 = (in5 - 0.00246648)/0.751318;
   input6 = (in6 - 0.00600458)/0.749842;
   input7 = (in7 - -3.92171)/32.3447;
   input8 = (in8 - -1.06857)/57.8975;
   input9 = (in9 - 1.37759)/0.329963;
   input10 = (in10 - 1.4804)/0.355224;
   input11 = (in11 - 0.149358)/1.47685;
   input12 = (in12 - 1.13264)/98.776;
   input13 = (in13 - 5.32237)/6.08756;
   input14 = (in14 - 1.24121)/301.843;
   switch(index) {
     case 0:
         return neuron0x8ddc0f8();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionBCPDirTheta::Value(int index, double* input) {
   input0 = (input[0] - 0.704494)/0.092045;
   input1 = (input[1] - -0.00255061)/0.258907;
   input2 = (input[2] - -3.42702)/31.587;
   input3 = (input[3] - -0.528546)/56.5134;
   input4 = (input[4] - 2626.33)/1277.16;
   input5 = (input[5] - 0.00246648)/0.751318;
   input6 = (input[6] - 0.00600458)/0.749842;
   input7 = (input[7] - -3.92171)/32.3447;
   input8 = (input[8] - -1.06857)/57.8975;
   input9 = (input[9] - 1.37759)/0.329963;
   input10 = (input[10] - 1.4804)/0.355224;
   input11 = (input[11] - 0.149358)/1.47685;
   input12 = (input[12] - 1.13264)/98.776;
   input13 = (input[13] - 5.32237)/6.08756;
   input14 = (input[14] - 1.24121)/301.843;
   switch(index) {
     case 0:
         return neuron0x8ddc0f8();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc76a0() {
   return input0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc7880() {
   return input1;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc7aa8() {
   return input2;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc7ca8() {
   return input3;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc7ea8() {
   return input4;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc80d0() {
   return input5;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc82d0() {
   return input6;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc84d0() {
   return input7;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc86d0() {
   return input8;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc88d0() {
   return input9;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc8ad0() {
   return input10;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc8ce8() {
   return input11;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc8f00() {
   return input12;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc9118() {
   return input13;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc9330() {
   return input14;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dc9670() {
   double input = -0.275934;
   input += synapse0x8d5bbd8();
   input += synapse0x8bf2768();
   input += synapse0x8bf27b0();
   input += synapse0x8bf2880();
   input += synapse0x8dc9828();
   input += synapse0x8dc9850();
   input += synapse0x8dc9878();
   input += synapse0x8dc98a0();
   input += synapse0x8dc98c8();
   input += synapse0x8dc98f0();
   input += synapse0x8dc9918();
   input += synapse0x8dc9940();
   input += synapse0x8dc9968();
   input += synapse0x8dc9990();
   input += synapse0x8dc99b8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc9670() {
   double input = input0x8dc9670();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dc99e0() {
   double input = 0.0213817;
   input += synapse0x8dc9c68();
   input += synapse0x8dc9c90();
   input += synapse0x8dc9d40();
   input += synapse0x8dc9d68();
   input += synapse0x8dc9d90();
   input += synapse0x8dc9db8();
   input += synapse0x8dc9de0();
   input += synapse0x8dc9e08();
   input += synapse0x8dc9e30();
   input += synapse0x8dc9e58();
   input += synapse0x8dc9e80();
   input += synapse0x8dc9ea8();
   input += synapse0x8dc9ed0();
   input += synapse0x8dc9ef8();
   input += synapse0x8dc9f20();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc99e0() {
   double input = input0x8dc99e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dc9f48() {
   double input = 0.147193;
   input += synapse0x8dca0b8();
   input += synapse0x8dca0e0();
   input += synapse0x8dca108();
   input += synapse0x8dc9cb8();
   input += synapse0x8dc9ce0();
   input += synapse0x8dc9d08();
   input += synapse0x8d5bc00();
   input += synapse0x8d5bc28();
   input += synapse0x8d5bc50();
   input += synapse0x8d5bc78();
   input += synapse0x8d5bca0();
   input += synapse0x8dca238();
   input += synapse0x8dca260();
   input += synapse0x8dca288();
   input += synapse0x8dca2b0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dc9f48() {
   double input = input0x8dc9f48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dca2d8() {
   double input = 0.111923;
   input += synapse0x8dca4d8();
   input += synapse0x8dca500();
   input += synapse0x8dca528();
   input += synapse0x8dca550();
   input += synapse0x8dca578();
   input += synapse0x8dca5a0();
   input += synapse0x8dca5c8();
   input += synapse0x8dca5f0();
   input += synapse0x8dca618();
   input += synapse0x8dca640();
   input += synapse0x8dca668();
   input += synapse0x8dca690();
   input += synapse0x8dca6b8();
   input += synapse0x8dca6e0();
   input += synapse0x8dca708();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dca2d8() {
   double input = input0x8dca2d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dca730() {
   double input = -0.334423;
   input += synapse0x8dca930();
   input += synapse0x8dca958();
   input += synapse0x8dca980();
   input += synapse0x8dca9a8();
   input += synapse0x8dca9d0();
   input += synapse0x8bf2680();
   input += synapse0x8bf26a8();
   input += synapse0x8bf28a8();
   input += synapse0x8bf28d0();
   input += synapse0x8bf28f8();
   input += synapse0x8bf2920();
   input += synapse0x8dce478();
   input += synapse0x8dce4a0();
   input += synapse0x8dce4c8();
   input += synapse0x8dce4f0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dca730() {
   double input = input0x8dca730();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcac00() {
   double input = 0.128985;
   input += synapse0x8dcae00();
   input += synapse0x8dcae28();
   input += synapse0x8dcae50();
   input += synapse0x8dcae78();
   input += synapse0x8dcaea0();
   input += synapse0x8dcaec8();
   input += synapse0x8dcaef0();
   input += synapse0x8dcaf18();
   input += synapse0x8dcaf40();
   input += synapse0x8dcaf68();
   input += synapse0x8dcaf90();
   input += synapse0x8dcafb8();
   input += synapse0x8dcafe0();
   input += synapse0x8dcb008();
   input += synapse0x8dcb030();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcac00() {
   double input = input0x8dcac00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcb058() {
   double input = 0.306363;
   input += synapse0x8dcb270();
   input += synapse0x8dcb298();
   input += synapse0x8dcb2c0();
   input += synapse0x8dcb2e8();
   input += synapse0x8dcb310();
   input += synapse0x8dcb338();
   input += synapse0x8dcb360();
   input += synapse0x8dcb388();
   input += synapse0x8dcb3b0();
   input += synapse0x8dcb3d8();
   input += synapse0x8dcb400();
   input += synapse0x8dcb428();
   input += synapse0x8dcb450();
   input += synapse0x8dcb478();
   input += synapse0x8dcb4a0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcb058() {
   double input = input0x8dcb058();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcb4c8() {
   double input = -0.181788;
   input += synapse0x8dcb6e0();
   input += synapse0x8dcb708();
   input += synapse0x8dcb730();
   input += synapse0x8dcb758();
   input += synapse0x8dcb780();
   input += synapse0x8dcb7a8();
   input += synapse0x8dcb7d0();
   input += synapse0x8dcb7f8();
   input += synapse0x8dcb820();
   input += synapse0x8dcb848();
   input += synapse0x8dcb870();
   input += synapse0x8dcb898();
   input += synapse0x8dcb8c0();
   input += synapse0x8dcb8e8();
   input += synapse0x8dcb910();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcb4c8() {
   double input = input0x8dcb4c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcb938() {
   double input = -0.160545;
   input += synapse0x8dcbb50();
   input += synapse0x8dcbb78();
   input += synapse0x8dcbba0();
   input += synapse0x8dcbbc8();
   input += synapse0x8dcbbf0();
   input += synapse0x8dcbc18();
   input += synapse0x8dcbc40();
   input += synapse0x8dcbc68();
   input += synapse0x8dcbc90();
   input += synapse0x8dce518();
   input += synapse0x8bf2998();
   input += synapse0x8bf29c0();
   input += synapse0x8bf29e8();
   input += synapse0x8bf2a10();
   input += synapse0x8bf2a38();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcb938() {
   double input = input0x8dcb938();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dca9f8() {
   double input = 0.349363;
   input += synapse0x8dca130();
   input += synapse0x8dca158();
   input += synapse0x8dca180();
   input += synapse0x8dca1a8();
   input += synapse0x8dca1d0();
   input += synapse0x8dca1f8();
   input += synapse0x8dcc0c0();
   input += synapse0x8dcc0e8();
   input += synapse0x8dcc110();
   input += synapse0x8dcc138();
   input += synapse0x8dcc160();
   input += synapse0x8dcc188();
   input += synapse0x8dcc1b0();
   input += synapse0x8dcc1d8();
   input += synapse0x8dcc200();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dca9f8() {
   double input = input0x8dca9f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcc228() {
   double input = -0.500282;
   input += synapse0x8dcc428();
   input += synapse0x8dcc450();
   input += synapse0x8dcc478();
   input += synapse0x8dcc4a0();
   input += synapse0x8dcc4c8();
   input += synapse0x8dcc4f0();
   input += synapse0x8dcc518();
   input += synapse0x8dcc540();
   input += synapse0x8dcc568();
   input += synapse0x8dcc590();
   input += synapse0x8dcc5b8();
   input += synapse0x8dcc5e0();
   input += synapse0x8dcc608();
   input += synapse0x8dcc630();
   input += synapse0x8dcc658();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcc228() {
   double input = input0x8dcc228();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcc680() {
   double input = 0.38435;
   input += synapse0x8dcc880();
   input += synapse0x8dcc8a8();
   input += synapse0x8dcc8d0();
   input += synapse0x8dcc8f8();
   input += synapse0x8dcc920();
   input += synapse0x8dcc948();
   input += synapse0x8dcc970();
   input += synapse0x8dcc998();
   input += synapse0x8dcc9c0();
   input += synapse0x8dcc9e8();
   input += synapse0x8dcca10();
   input += synapse0x8dcca38();
   input += synapse0x8dcca60();
   input += synapse0x8dcca88();
   input += synapse0x8dccab0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcc680() {
   double input = input0x8dcc680();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dccad8() {
   double input = 0.283496;
   input += synapse0x8dcccf0();
   input += synapse0x8dccd18();
   input += synapse0x8dccd40();
   input += synapse0x8dccd68();
   input += synapse0x8dccd90();
   input += synapse0x8dccdb8();
   input += synapse0x8dccde0();
   input += synapse0x8dcce08();
   input += synapse0x8dcce30();
   input += synapse0x8dcce58();
   input += synapse0x8dcce80();
   input += synapse0x8dccea8();
   input += synapse0x8dcced0();
   input += synapse0x8dccef8();
   input += synapse0x8dccf20();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dccad8() {
   double input = input0x8dccad8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dccf48() {
   double input = -0.3241;
   input += synapse0x8dcd160();
   input += synapse0x8dcd188();
   input += synapse0x8dcd1b0();
   input += synapse0x8dcd1d8();
   input += synapse0x8dcd200();
   input += synapse0x8dcd228();
   input += synapse0x8dcd250();
   input += synapse0x8dcd278();
   input += synapse0x8dcd2a0();
   input += synapse0x8dcd2c8();
   input += synapse0x8dcd2f0();
   input += synapse0x8dcd318();
   input += synapse0x8dcd340();
   input += synapse0x8dcd368();
   input += synapse0x8dcd390();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dccf48() {
   double input = input0x8dccf48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dce5a8() {
   double input = 0.463001;
   input += synapse0x8dcd448();
   input += synapse0x8dce718();
   input += synapse0x8dce740();
   input += synapse0x8dce768();
   input += synapse0x8dce790();
   input += synapse0x8dce7b8();
   input += synapse0x8dce7e0();
   input += synapse0x8dce808();
   input += synapse0x8dce830();
   input += synapse0x8dce858();
   input += synapse0x8dce880();
   input += synapse0x8dce8a8();
   input += synapse0x8dce8d0();
   input += synapse0x8dce8f8();
   input += synapse0x8dce920();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dce5a8() {
   double input = input0x8dce5a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dce948() {
   double input = -0.347872;
   input += synapse0x8dceb48();
   input += synapse0x8dcebf8();
   input += synapse0x8dceca8();
   input += synapse0x8dced58();
   input += synapse0x8dcee08();
   input += synapse0x8dceeb8();
   input += synapse0x8dcef68();
   input += synapse0x8dcf018();
   input += synapse0x8dcf0c8();
   input += synapse0x8dcf178();
   input += synapse0x8dcf228();
   input += synapse0x8dcf2d8();
   input += synapse0x8dcf388();
   input += synapse0x8dcf438();
   input += synapse0x8dcf4e8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dce948() {
   double input = input0x8dce948();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcf598() {
   double input = -0.318567;
   input += synapse0x8dcf6c0();
   input += synapse0x8dcf6e8();
   input += synapse0x8dcf710();
   input += synapse0x8dcf738();
   input += synapse0x8dcf760();
   input += synapse0x8dcf788();
   input += synapse0x8dcf7b0();
   input += synapse0x8dcf7d8();
   input += synapse0x8dcf800();
   input += synapse0x8dcf828();
   input += synapse0x8dcf850();
   input += synapse0x8dcf878();
   input += synapse0x8dcf8a0();
   input += synapse0x8dcf8c8();
   input += synapse0x8dcf8f0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcf598() {
   double input = input0x8dcf598();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcf918() {
   double input = -0.0915838;
   input += synapse0x8dc9be0();
   input += synapse0x8dc9c08();
   input += synapse0x8dc9c30();
   input += synapse0x8dc8840();
   input += synapse0x8dc8640();
   input += synapse0x8dc8440();
   input += synapse0x8dc8240();
   input += synapse0x8dc8040();
   input += synapse0x8dc7e18();
   input += synapse0x8dc7c18();
   input += synapse0x8dc7a18();
   input += synapse0x8dc77f0();
   input += synapse0x8dcbcb8();
   input += synapse0x8dcbce0();
   input += synapse0x8dcbd08();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcf918() {
   double input = input0x8dcf918();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcbd30() {
   double input = 0.293307;
   input += synapse0x8dcbf48();
   input += synapse0x8dcbf70();
   input += synapse0x8dcbf98();
   input += synapse0x8dcbfc0();
   input += synapse0x8dcbfe8();
   input += synapse0x8dcc010();
   input += synapse0x8dcc038();
   input += synapse0x8dcc060();
   input += synapse0x8dcc088();
   input += synapse0x8dd0350();
   input += synapse0x8dd0378();
   input += synapse0x8dd03a0();
   input += synapse0x8dd03c8();
   input += synapse0x8dd03f0();
   input += synapse0x8dd0418();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcbd30() {
   double input = input0x8dcbd30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd0440() {
   double input = -0.320475;
   input += synapse0x8dd0640();
   input += synapse0x8dd0668();
   input += synapse0x8dd0690();
   input += synapse0x8dd06b8();
   input += synapse0x8dd06e0();
   input += synapse0x8dd0708();
   input += synapse0x8dd0730();
   input += synapse0x8dd0758();
   input += synapse0x8dd0780();
   input += synapse0x8dd07a8();
   input += synapse0x8dd07d0();
   input += synapse0x8dd07f8();
   input += synapse0x8dd0820();
   input += synapse0x8dd0848();
   input += synapse0x8dd0870();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd0440() {
   double input = input0x8dd0440();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd0898() {
   double input = -0.381771;
   input += synapse0x8dd0a98();
   input += synapse0x8dd0ac0();
   input += synapse0x8dd0ae8();
   input += synapse0x8dd0b10();
   input += synapse0x8dd0b38();
   input += synapse0x8dd0b60();
   input += synapse0x8dd0b88();
   input += synapse0x8dd0bb0();
   input += synapse0x8dd0bd8();
   input += synapse0x8dd0c00();
   input += synapse0x8dd0c28();
   input += synapse0x8dd0c50();
   input += synapse0x8dd0c78();
   input += synapse0x8dd0ca0();
   input += synapse0x8dd0cc8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd0898() {
   double input = input0x8dd0898();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd0cf0() {
   double input = -0.250142;
   input += synapse0x8dd0ef0();
   input += synapse0x8dd0f18();
   input += synapse0x8dd0f40();
   input += synapse0x8dd0f68();
   input += synapse0x8dd0f90();
   input += synapse0x8dd0fb8();
   input += synapse0x8dd0fe0();
   input += synapse0x8dd1008();
   input += synapse0x8dd1030();
   input += synapse0x8dd1058();
   input += synapse0x8dd1080();
   input += synapse0x8dd10a8();
   input += synapse0x8dd10d0();
   input += synapse0x8dd10f8();
   input += synapse0x8dd1120();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd0cf0() {
   double input = input0x8dd0cf0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd1148() {
   double input = 0.432641;
   input += synapse0x8dd1348();
   input += synapse0x8dd1370();
   input += synapse0x8dd1398();
   input += synapse0x8dd13c0();
   input += synapse0x8dd13e8();
   input += synapse0x8dd1410();
   input += synapse0x8dd1438();
   input += synapse0x8dd1460();
   input += synapse0x8dd1488();
   input += synapse0x8dd14b0();
   input += synapse0x8dd14d8();
   input += synapse0x8dd1500();
   input += synapse0x8dd1528();
   input += synapse0x8dd1550();
   input += synapse0x8dd1578();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd1148() {
   double input = input0x8dd1148();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd15a0() {
   double input = 0.172004;
   input += synapse0x8dd17a0();
   input += synapse0x8dd17c8();
   input += synapse0x8dd17f0();
   input += synapse0x8dd1818();
   input += synapse0x8dd1840();
   input += synapse0x8dd1868();
   input += synapse0x8dd1890();
   input += synapse0x8dd18b8();
   input += synapse0x8dd18e0();
   input += synapse0x8dd1908();
   input += synapse0x8dd1930();
   input += synapse0x8dd1958();
   input += synapse0x8dd1980();
   input += synapse0x8dd19a8();
   input += synapse0x8dd19d0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd15a0() {
   double input = input0x8dd15a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd19f8() {
   double input = -0.0920738;
   input += synapse0x8dd1bf8();
   input += synapse0x8dd1c20();
   input += synapse0x8dd1c48();
   input += synapse0x8dd1c70();
   input += synapse0x8dd1c98();
   input += synapse0x8dd1cc0();
   input += synapse0x8dd1ce8();
   input += synapse0x8dd1d10();
   input += synapse0x8dd1d38();
   input += synapse0x8dd1d60();
   input += synapse0x8dd1d88();
   input += synapse0x8dd1db0();
   input += synapse0x8dd1dd8();
   input += synapse0x8dd1e00();
   input += synapse0x8dd1e28();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd19f8() {
   double input = input0x8dd19f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd1e50() {
   double input = -0.266028;
   input += synapse0x8dd2050();
   input += synapse0x8dd2078();
   input += synapse0x8dd20a0();
   input += synapse0x8dd20c8();
   input += synapse0x8dd20f0();
   input += synapse0x8dd2118();
   input += synapse0x8dd2140();
   input += synapse0x8dd2168();
   input += synapse0x8dd2190();
   input += synapse0x8dd21b8();
   input += synapse0x8dd21e0();
   input += synapse0x8dd2208();
   input += synapse0x8dd2230();
   input += synapse0x8dd2258();
   input += synapse0x8dd2280();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd1e50() {
   double input = input0x8dd1e50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd22a8() {
   double input = 0.309557;
   input += synapse0x8dd24a8();
   input += synapse0x8dd24d0();
   input += synapse0x8dd24f8();
   input += synapse0x8dd2520();
   input += synapse0x8dd2548();
   input += synapse0x8dd2570();
   input += synapse0x8dd2598();
   input += synapse0x8dd25c0();
   input += synapse0x8dd25e8();
   input += synapse0x8dd2610();
   input += synapse0x8dd2638();
   input += synapse0x8dd2660();
   input += synapse0x8dd2688();
   input += synapse0x8dd26b0();
   input += synapse0x8dd26d8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd22a8() {
   double input = input0x8dd22a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd2700() {
   double input = 0.233385;
   input += synapse0x8dd2900();
   input += synapse0x8dd2928();
   input += synapse0x8dd2950();
   input += synapse0x8dd2978();
   input += synapse0x8dd29a0();
   input += synapse0x8dd29c8();
   input += synapse0x8dd29f0();
   input += synapse0x8dd2a18();
   input += synapse0x8dd2a40();
   input += synapse0x8dd2a68();
   input += synapse0x8dd2a90();
   input += synapse0x8dd2ab8();
   input += synapse0x8dd2ae0();
   input += synapse0x8dd2b08();
   input += synapse0x8dd2b30();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd2700() {
   double input = input0x8dd2700();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd2b58() {
   double input = -0.232107;
   input += synapse0x8dd2d58();
   input += synapse0x8dd2d80();
   input += synapse0x8dd2da8();
   input += synapse0x8dd2dd0();
   input += synapse0x8dd2df8();
   input += synapse0x8dd2e20();
   input += synapse0x8dd2e48();
   input += synapse0x8dd2e70();
   input += synapse0x8dd2e98();
   input += synapse0x8dd2ec0();
   input += synapse0x8dd2ee8();
   input += synapse0x8dd2f10();
   input += synapse0x8dd2f38();
   input += synapse0x8dd2f60();
   input += synapse0x8dd2f88();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd2b58() {
   double input = input0x8dd2b58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd2fb0() {
   double input = 0.101726;
   input += synapse0x8dd31b0();
   input += synapse0x8dd31d8();
   input += synapse0x8dd3200();
   input += synapse0x8dd3228();
   input += synapse0x8dd3250();
   input += synapse0x8dd3278();
   input += synapse0x8dd32a0();
   input += synapse0x8dd32c8();
   input += synapse0x8dd32f0();
   input += synapse0x8dd3318();
   input += synapse0x8dd3340();
   input += synapse0x8dd3368();
   input += synapse0x8dd3390();
   input += synapse0x8dd33b8();
   input += synapse0x8dd33e0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd2fb0() {
   double input = input0x8dd2fb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd3408() {
   double input = 0.161487;
   input += synapse0x8dd3608();
   input += synapse0x8dd3630();
   input += synapse0x8dd3658();
   input += synapse0x8dd3680();
   input += synapse0x8dd36a8();
   input += synapse0x8dd36d0();
   input += synapse0x8dd36f8();
   input += synapse0x8dd3720();
   input += synapse0x8dd3748();
   input += synapse0x8dd3770();
   input += synapse0x8dd3798();
   input += synapse0x8dd37c0();
   input += synapse0x8dd37e8();
   input += synapse0x8dd3810();
   input += synapse0x8dd3838();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd3408() {
   double input = input0x8dd3408();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd3860() {
   double input = -0.396453;
   input += synapse0x8dd3a60();
   input += synapse0x8dceb70();
   input += synapse0x8dceb98();
   input += synapse0x8dcebc0();
   input += synapse0x8dcec20();
   input += synapse0x8dcec48();
   input += synapse0x8dcec70();
   input += synapse0x8dcecd0();
   input += synapse0x8dcecf8();
   input += synapse0x8dced20();
   input += synapse0x8dced80();
   input += synapse0x8dceda8();
   input += synapse0x8dcedd0();
   input += synapse0x8dcee30();
   input += synapse0x8dcee58();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd3860() {
   double input = input0x8dd3860();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd4a00() {
   double input = -0.307278;
   input += synapse0x8dcf088();
   input += synapse0x8dcee80();
   input += synapse0x8dcef28();
   input += synapse0x8dcefd8();
   input += synapse0x8dcf0f0();
   input += synapse0x8dcf118();
   input += synapse0x8dcf140();
   input += synapse0x8dcf1a0();
   input += synapse0x8dcf1c8();
   input += synapse0x8dcf1f0();
   input += synapse0x8dcf250();
   input += synapse0x8dcf278();
   input += synapse0x8dcf2a0();
   input += synapse0x8dcf300();
   input += synapse0x8dcf328();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd4a00() {
   double input = input0x8dd4a00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd4b28() {
   double input = -0.365298;
   input += synapse0x8dcf558();
   input += synapse0x8dcf350();
   input += synapse0x8dcf3f8();
   input += synapse0x8dcf4a8();
   input += synapse0x8dd4c50();
   input += synapse0x8dd4c78();
   input += synapse0x8dd4ca0();
   input += synapse0x8dd4cc8();
   input += synapse0x8dd4cf0();
   input += synapse0x8dd4d18();
   input += synapse0x8dd4d40();
   input += synapse0x8dd4d68();
   input += synapse0x8dd4d90();
   input += synapse0x8dd4db8();
   input += synapse0x8dd4de0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd4b28() {
   double input = input0x8dd4b28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd4e08() {
   double input = 0.34354;
   input += synapse0x8dd5008();
   input += synapse0x8dd5030();
   input += synapse0x8dd5058();
   input += synapse0x8dcfb48();
   input += synapse0x8dcfb70();
   input += synapse0x8dcfb98();
   input += synapse0x8dcfbc0();
   input += synapse0x8dcfbe8();
   input += synapse0x8dcfc10();
   input += synapse0x8dcfc38();
   input += synapse0x8dcfc60();
   input += synapse0x8dcfc88();
   input += synapse0x8dcfcb0();
   input += synapse0x8dcfcd8();
   input += synapse0x8dcfd00();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd4e08() {
   double input = input0x8dd4e08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dcfd28() {
   double input = 0.21966;
   input += synapse0x8dcff28();
   input += synapse0x8dcff50();
   input += synapse0x8dcff78();
   input += synapse0x8dcffa0();
   input += synapse0x8dcffc8();
   input += synapse0x8dcfff0();
   input += synapse0x8dd0018();
   input += synapse0x8dd0040();
   input += synapse0x8dd0068();
   input += synapse0x8dd0090();
   input += synapse0x8dd00b8();
   input += synapse0x8dd00e0();
   input += synapse0x8dd0108();
   input += synapse0x8dd0130();
   input += synapse0x8dd0158();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dcfd28() {
   double input = input0x8dcfd28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd0180() {
   double input = -0.288975;
   input += synapse0x8dd60d0();
   input += synapse0x8dd60f8();
   input += synapse0x8dd6120();
   input += synapse0x8dd6148();
   input += synapse0x8dd6170();
   input += synapse0x8dd6198();
   input += synapse0x8dd61c0();
   input += synapse0x8dd61e8();
   input += synapse0x8dd6210();
   input += synapse0x8dd6238();
   input += synapse0x8dd6260();
   input += synapse0x8dd6288();
   input += synapse0x8dd62b0();
   input += synapse0x8dd62d8();
   input += synapse0x8dd6300();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd0180() {
   double input = input0x8dd0180();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd6328() {
   double input = -0.0556555;
   input += synapse0x8dd6528();
   input += synapse0x8dd6550();
   input += synapse0x8dd6578();
   input += synapse0x8dd65a0();
   input += synapse0x8dd65c8();
   input += synapse0x8dd65f0();
   input += synapse0x8dd6618();
   input += synapse0x8dd6640();
   input += synapse0x8dd6668();
   input += synapse0x8dd6690();
   input += synapse0x8dd66b8();
   input += synapse0x8dd66e0();
   input += synapse0x8dd6708();
   input += synapse0x8dd6730();
   input += synapse0x8dd6758();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd6328() {
   double input = input0x8dd6328();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd6780() {
   double input = -0.44706;
   input += synapse0x8dd6980();
   input += synapse0x8dd69a8();
   input += synapse0x8dd69d0();
   input += synapse0x8dd69f8();
   input += synapse0x8dd6a20();
   input += synapse0x8dd6a48();
   input += synapse0x8dd6a70();
   input += synapse0x8dd6a98();
   input += synapse0x8dd6ac0();
   input += synapse0x8dd6ae8();
   input += synapse0x8dd6b10();
   input += synapse0x8dd6b38();
   input += synapse0x8dd6b60();
   input += synapse0x8dd6b88();
   input += synapse0x8dd6bb0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd6780() {
   double input = input0x8dd6780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd6bd8() {
   double input = 0.221741;
   input += synapse0x8dd6dd8();
   input += synapse0x8dd6e00();
   input += synapse0x8dd6e28();
   input += synapse0x8dd6e50();
   input += synapse0x8dd6e78();
   input += synapse0x8dd6ea0();
   input += synapse0x8dd6ec8();
   input += synapse0x8dd6ef0();
   input += synapse0x8dd6f18();
   input += synapse0x8dd6f40();
   input += synapse0x8dd6f68();
   input += synapse0x8dd6f90();
   input += synapse0x8dd6fb8();
   input += synapse0x8dd6fe0();
   input += synapse0x8dd7008();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd6bd8() {
   double input = input0x8dd6bd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd7030() {
   double input = 0.492309;
   input += synapse0x8dd7230();
   input += synapse0x8dd7258();
   input += synapse0x8dd7280();
   input += synapse0x8dd72a8();
   input += synapse0x8dd72d0();
   input += synapse0x8dd72f8();
   input += synapse0x8dd7320();
   input += synapse0x8dd7348();
   input += synapse0x8dd7370();
   input += synapse0x8dd7398();
   input += synapse0x8dd73c0();
   input += synapse0x8dd73e8();
   input += synapse0x8dd7410();
   input += synapse0x8dd7438();
   input += synapse0x8dd7460();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd7030() {
   double input = input0x8dd7030();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd7488() {
   double input = -0.255282;
   input += synapse0x8dd76a0();
   input += synapse0x8dd76c8();
   input += synapse0x8dd76f0();
   input += synapse0x8dd7718();
   input += synapse0x8dd7740();
   input += synapse0x8dd7768();
   input += synapse0x8dd7790();
   input += synapse0x8dd77b8();
   input += synapse0x8dd77e0();
   input += synapse0x8dd7808();
   input += synapse0x8dd7830();
   input += synapse0x8dd7858();
   input += synapse0x8dd7880();
   input += synapse0x8dd78a8();
   input += synapse0x8dd78d0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd7488() {
   double input = input0x8dd7488();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd78f8() {
   double input = 0.429474;
   input += synapse0x8dd7b10();
   input += synapse0x8dd7b38();
   input += synapse0x8dd7b60();
   input += synapse0x8dd7b88();
   input += synapse0x8dd7bb0();
   input += synapse0x8dd7bd8();
   input += synapse0x8dd7c00();
   input += synapse0x8dd7c28();
   input += synapse0x8dd7c50();
   input += synapse0x8dd7c78();
   input += synapse0x8dd7ca0();
   input += synapse0x8dd7cc8();
   input += synapse0x8dd7cf0();
   input += synapse0x8dd7d18();
   input += synapse0x8dd7d40();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd78f8() {
   double input = input0x8dd78f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd7d68() {
   double input = 0.0112787;
   input += synapse0x8dd7f80();
   input += synapse0x8dd7fa8();
   input += synapse0x8dd7fd0();
   input += synapse0x8dd7ff8();
   input += synapse0x8dd8020();
   input += synapse0x8dd8048();
   input += synapse0x8dd8070();
   input += synapse0x8dd8098();
   input += synapse0x8dd80c0();
   input += synapse0x8dd80e8();
   input += synapse0x8dd8110();
   input += synapse0x8dd8138();
   input += synapse0x8dd8160();
   input += synapse0x8dd8188();
   input += synapse0x8dd81b0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd7d68() {
   double input = input0x8dd7d68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd81d8() {
   double input = -0.655186;
   input += synapse0x8dd83f0();
   input += synapse0x8dd8418();
   input += synapse0x8dd8440();
   input += synapse0x8dd8468();
   input += synapse0x8dd8490();
   input += synapse0x8dd84b8();
   input += synapse0x8dd84e0();
   input += synapse0x8dd8508();
   input += synapse0x8dd8530();
   input += synapse0x8dd8558();
   input += synapse0x8dd8580();
   input += synapse0x8dd85a8();
   input += synapse0x8dd85d0();
   input += synapse0x8dd85f8();
   input += synapse0x8dd8620();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd81d8() {
   double input = input0x8dd81d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd8648() {
   double input = 0.202472;
   input += synapse0x8dd8860();
   input += synapse0x8dd8888();
   input += synapse0x8dd88b0();
   input += synapse0x8dd88d8();
   input += synapse0x8dd8900();
   input += synapse0x8dd8928();
   input += synapse0x8dd8950();
   input += synapse0x8dd8978();
   input += synapse0x8dd89a0();
   input += synapse0x8dd89c8();
   input += synapse0x8dd89f0();
   input += synapse0x8dd8a18();
   input += synapse0x8dd8a40();
   input += synapse0x8dd8a68();
   input += synapse0x8dd8a90();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd8648() {
   double input = input0x8dd8648();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd8ab8() {
   double input = 0.37679;
   input += synapse0x8dd8cd0();
   input += synapse0x8dd8cf8();
   input += synapse0x8dd8d20();
   input += synapse0x8dd8d48();
   input += synapse0x8dd8d70();
   input += synapse0x8dd8d98();
   input += synapse0x8dd8dc0();
   input += synapse0x8dd8de8();
   input += synapse0x8dd8e10();
   input += synapse0x8dd8e38();
   input += synapse0x8dd8e60();
   input += synapse0x8dd8e88();
   input += synapse0x8dd8eb0();
   input += synapse0x8dd8ed8();
   input += synapse0x8dd8f00();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd8ab8() {
   double input = input0x8dd8ab8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd8f28() {
   double input = 0.268024;
   input += synapse0x8dd9140();
   input += synapse0x8dd9168();
   input += synapse0x8dd9190();
   input += synapse0x8dd91b8();
   input += synapse0x8dd91e0();
   input += synapse0x8dd9208();
   input += synapse0x8dd9230();
   input += synapse0x8dd9258();
   input += synapse0x8dd9280();
   input += synapse0x8dd92a8();
   input += synapse0x8dd92d0();
   input += synapse0x8dd92f8();
   input += synapse0x8dd9320();
   input += synapse0x8dd9348();
   input += synapse0x8dd9370();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd8f28() {
   double input = input0x8dd8f28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd9398() {
   double input = 0.131439;
   input += synapse0x8dd95b0();
   input += synapse0x8dd95d8();
   input += synapse0x8dd9600();
   input += synapse0x8dd9628();
   input += synapse0x8dd9650();
   input += synapse0x8dd9678();
   input += synapse0x8dd96a0();
   input += synapse0x8dd96c8();
   input += synapse0x8dd96f0();
   input += synapse0x8dd9718();
   input += synapse0x8dd9740();
   input += synapse0x8dd9768();
   input += synapse0x8dd9790();
   input += synapse0x8dd97b8();
   input += synapse0x8dd97e0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd9398() {
   double input = input0x8dd9398();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd9808() {
   double input = -0.0732576;
   input += synapse0x8dcfa40();
   input += synapse0x8dcfa68();
   input += synapse0x8dcfa90();
   input += synapse0x8dcfab8();
   input += synapse0x8dcfae0();
   input += synapse0x8dcfb08();
   input += synapse0x8dd9c28();
   input += synapse0x8dd9c50();
   input += synapse0x8dd9c78();
   input += synapse0x8dd9ca0();
   input += synapse0x8dd9cc8();
   input += synapse0x8dd9cf0();
   input += synapse0x8dd9d18();
   input += synapse0x8dd9d40();
   input += synapse0x8dd9d68();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd9808() {
   double input = input0x8dd9808();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dd9d90() {
   double input = -0.425551;
   input += synapse0x8dd9f90();
   input += synapse0x8dd9fb8();
   input += synapse0x8dd9fe0();
   input += synapse0x8dda008();
   input += synapse0x8dda030();
   input += synapse0x8dda058();
   input += synapse0x8dda080();
   input += synapse0x8dda0a8();
   input += synapse0x8dda0d0();
   input += synapse0x8dda0f8();
   input += synapse0x8dda120();
   input += synapse0x8dda148();
   input += synapse0x8dda170();
   input += synapse0x8dda198();
   input += synapse0x8dda1c0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dd9d90() {
   double input = input0x8dd9d90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dda1e8() {
   double input = -0.0136715;
   input += synapse0x8dda400();
   input += synapse0x8dda428();
   input += synapse0x8dda450();
   input += synapse0x8dda478();
   input += synapse0x8dda4a0();
   input += synapse0x8dda4c8();
   input += synapse0x8dda4f0();
   input += synapse0x8dda518();
   input += synapse0x8dda540();
   input += synapse0x8dda568();
   input += synapse0x8dda590();
   input += synapse0x8dda5b8();
   input += synapse0x8dda5e0();
   input += synapse0x8dda608();
   input += synapse0x8dda630();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dda1e8() {
   double input = input0x8dda1e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8dda658() {
   double input = -0.406779;
   input += synapse0x8dda870();
   input += synapse0x8dda898();
   input += synapse0x8dda8c0();
   input += synapse0x8dda8e8();
   input += synapse0x8dda910();
   input += synapse0x8dda938();
   input += synapse0x8dda960();
   input += synapse0x8dda988();
   input += synapse0x8dda9b0();
   input += synapse0x8dda9d8();
   input += synapse0x8ddaa00();
   input += synapse0x8ddaa28();
   input += synapse0x8ddaa50();
   input += synapse0x8ddaa78();
   input += synapse0x8ddaaa0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8dda658() {
   double input = input0x8dda658();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8ddaac8() {
   double input = -0.0752377;
   input += synapse0x8ddace0();
   input += synapse0x8ddad08();
   input += synapse0x8ddad30();
   input += synapse0x8ddad58();
   input += synapse0x8ddad80();
   input += synapse0x8ddada8();
   input += synapse0x8ddadd0();
   input += synapse0x8ddadf8();
   input += synapse0x8ddae20();
   input += synapse0x8ddae48();
   input += synapse0x8ddae70();
   input += synapse0x8ddae98();
   input += synapse0x8ddaec0();
   input += synapse0x8ddaee8();
   input += synapse0x8ddaf10();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8ddaac8() {
   double input = input0x8ddaac8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8ddaf38() {
   double input = 0.412533;
   input += synapse0x8ddb150();
   input += synapse0x8ddb178();
   input += synapse0x8ddb1a0();
   input += synapse0x8ddb1c8();
   input += synapse0x8ddb1f0();
   input += synapse0x8ddb218();
   input += synapse0x8ddb240();
   input += synapse0x8ddb268();
   input += synapse0x8ddb290();
   input += synapse0x8ddb2b8();
   input += synapse0x8ddb2e0();
   input += synapse0x8ddb308();
   input += synapse0x8ddb330();
   input += synapse0x8ddb358();
   input += synapse0x8ddb380();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8ddaf38() {
   double input = input0x8ddaf38();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8ddb3a8() {
   double input = 0.944488;
   input += synapse0x8ddb5c0();
   input += synapse0x8ddb5e8();
   input += synapse0x8ddb610();
   input += synapse0x8ddb638();
   input += synapse0x8ddb660();
   input += synapse0x8ddb688();
   input += synapse0x8ddb6b0();
   input += synapse0x8ddb6d8();
   input += synapse0x8ddb700();
   input += synapse0x8ddb728();
   input += synapse0x8ddb750();
   input += synapse0x8ddb778();
   input += synapse0x8ddb7a0();
   input += synapse0x8ddb7c8();
   input += synapse0x8ddb7f0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8ddb3a8() {
   double input = input0x8ddb3a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8ddb818() {
   double input = -0.126032;
   input += synapse0x8ddba30();
   input += synapse0x8ddba58();
   input += synapse0x8ddba80();
   input += synapse0x8ddbaa8();
   input += synapse0x8ddbad0();
   input += synapse0x8ddbaf8();
   input += synapse0x8ddbb20();
   input += synapse0x8ddbb48();
   input += synapse0x8ddbb70();
   input += synapse0x8ddbb98();
   input += synapse0x8ddbbc0();
   input += synapse0x8ddbbe8();
   input += synapse0x8ddbc10();
   input += synapse0x8ddbc38();
   input += synapse0x8ddbc60();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8ddb818() {
   double input = input0x8ddb818();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8ddbc88() {
   double input = -0.0794702;
   input += synapse0x8ddbea0();
   input += synapse0x8ddbec8();
   input += synapse0x8ddbef0();
   input += synapse0x8ddbf18();
   input += synapse0x8ddbf40();
   input += synapse0x8ddbf68();
   input += synapse0x8ddbf90();
   input += synapse0x8ddbfb8();
   input += synapse0x8ddbfe0();
   input += synapse0x8ddc008();
   input += synapse0x8ddc030();
   input += synapse0x8ddc058();
   input += synapse0x8ddc080();
   input += synapse0x8ddc0a8();
   input += synapse0x8ddc0d0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8ddbc88() {
   double input = input0x8ddbc88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::input0x8ddc0f8() {
   double input = -0.241952;
   input += synapse0x8ddc220();
   input += synapse0x8ddc248();
   input += synapse0x8ddc270();
   input += synapse0x8ddc298();
   input += synapse0x8ddc2c0();
   input += synapse0x8ddc2e8();
   input += synapse0x8ddc310();
   input += synapse0x8ddc338();
   input += synapse0x8ddc360();
   input += synapse0x8ddc388();
   input += synapse0x8ddc3b0();
   input += synapse0x8ddc3d8();
   input += synapse0x8ddc400();
   input += synapse0x8ddc428();
   input += synapse0x8ddc450();
   input += synapse0x8ddc478();
   input += synapse0x8ddc528();
   input += synapse0x8ddc550();
   input += synapse0x8ddc578();
   input += synapse0x8ddc5a0();
   input += synapse0x8ddc5c8();
   input += synapse0x8ddc5f0();
   input += synapse0x8ddc618();
   input += synapse0x8ddc640();
   input += synapse0x8ddc668();
   input += synapse0x8ddc690();
   input += synapse0x8ddc6b8();
   input += synapse0x8ddc6e0();
   input += synapse0x8ddc708();
   input += synapse0x8ddc730();
   input += synapse0x8ddc758();
   input += synapse0x8ddc780();
   input += synapse0x8ddc4a0();
   input += synapse0x8ddc4c8();
   input += synapse0x8ddc4f0();
   input += synapse0x8ddc8b0();
   input += synapse0x8ddc8d8();
   input += synapse0x8ddc900();
   input += synapse0x8ddc928();
   input += synapse0x8ddc950();
   input += synapse0x8ddc978();
   input += synapse0x8ddc9a0();
   input += synapse0x8ddc9c8();
   input += synapse0x8ddc9f0();
   input += synapse0x8ddca18();
   input += synapse0x8ddca40();
   input += synapse0x8ddca68();
   input += synapse0x8ddca90();
   input += synapse0x8ddcab8();
   input += synapse0x8ddcae0();
   input += synapse0x8ddcb08();
   input += synapse0x8ddcb30();
   input += synapse0x8ddcb58();
   input += synapse0x8ddcb80();
   input += synapse0x8ddcba8();
   input += synapse0x8ddcbd0();
   input += synapse0x8ddcbf8();
   input += synapse0x8ddcc20();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirTheta::neuron0x8ddc0f8() {
   double input = input0x8ddc0f8();
   return (input * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8d5bbd8() {
   return (neuron0x8dc76a0()*0.000917357);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf2768() {
   return (neuron0x8dc7880()*0.495551);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf27b0() {
   return (neuron0x8dc7aa8()*-0.241911);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf2880() {
   return (neuron0x8dc7ca8()*0.521217);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9828() {
   return (neuron0x8dc7ea8()*-0.0585833);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9850() {
   return (neuron0x8dc80d0()*0.267412);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9878() {
   return (neuron0x8dc82d0()*0.0523241);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc98a0() {
   return (neuron0x8dc84d0()*-0.133073);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc98c8() {
   return (neuron0x8dc86d0()*0.36178);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc98f0() {
   return (neuron0x8dc88d0()*0.102854);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9918() {
   return (neuron0x8dc8ad0()*-0.0747235);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9940() {
   return (neuron0x8dc8ce8()*0.330433);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9968() {
   return (neuron0x8dc8f00()*-0.196374);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9990() {
   return (neuron0x8dc9118()*-0.138415);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc99b8() {
   return (neuron0x8dc9330()*-0.461663);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9c68() {
   return (neuron0x8dc76a0()*0.0339149);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9c90() {
   return (neuron0x8dc7880()*-0.254899);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9d40() {
   return (neuron0x8dc7aa8()*-0.34867);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9d68() {
   return (neuron0x8dc7ca8()*-0.239931);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9d90() {
   return (neuron0x8dc7ea8()*-0.0153807);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9db8() {
   return (neuron0x8dc80d0()*-0.361276);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9de0() {
   return (neuron0x8dc82d0()*0.246941);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9e08() {
   return (neuron0x8dc84d0()*0.350872);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9e30() {
   return (neuron0x8dc86d0()*0.36251);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9e58() {
   return (neuron0x8dc88d0()*-0.449463);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9e80() {
   return (neuron0x8dc8ad0()*-0.19925);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9ea8() {
   return (neuron0x8dc8ce8()*0.31854);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9ed0() {
   return (neuron0x8dc8f00()*0.0834065);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9ef8() {
   return (neuron0x8dc9118()*-0.0276289);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9f20() {
   return (neuron0x8dc9330()*-0.0634666);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca0b8() {
   return (neuron0x8dc76a0()*-0.204837);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca0e0() {
   return (neuron0x8dc7880()*0.00550912);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca108() {
   return (neuron0x8dc7aa8()*-0.310503);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9cb8() {
   return (neuron0x8dc7ca8()*0.137206);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9ce0() {
   return (neuron0x8dc7ea8()*0.246206);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9d08() {
   return (neuron0x8dc80d0()*0.383669);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8d5bc00() {
   return (neuron0x8dc82d0()*0.465334);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8d5bc28() {
   return (neuron0x8dc84d0()*-0.300341);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8d5bc50() {
   return (neuron0x8dc86d0()*-0.157687);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8d5bc78() {
   return (neuron0x8dc88d0()*-0.419397);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8d5bca0() {
   return (neuron0x8dc8ad0()*-0.123141);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca238() {
   return (neuron0x8dc8ce8()*0.249504);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca260() {
   return (neuron0x8dc8f00()*-0.370998);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca288() {
   return (neuron0x8dc9118()*-0.274557);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca2b0() {
   return (neuron0x8dc9330()*-0.21254);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca4d8() {
   return (neuron0x8dc76a0()*0.26551);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca500() {
   return (neuron0x8dc7880()*0.18554);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca528() {
   return (neuron0x8dc7aa8()*-0.0554392);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca550() {
   return (neuron0x8dc7ca8()*0.28077);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca578() {
   return (neuron0x8dc7ea8()*-0.297952);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca5a0() {
   return (neuron0x8dc80d0()*0.438903);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca5c8() {
   return (neuron0x8dc82d0()*-0.227409);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca5f0() {
   return (neuron0x8dc84d0()*0.436866);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca618() {
   return (neuron0x8dc86d0()*-0.112022);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca640() {
   return (neuron0x8dc88d0()*0.044532);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca668() {
   return (neuron0x8dc8ad0()*0.266136);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca690() {
   return (neuron0x8dc8ce8()*-0.164424);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca6b8() {
   return (neuron0x8dc8f00()*-0.279467);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca6e0() {
   return (neuron0x8dc9118()*-0.44071);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca708() {
   return (neuron0x8dc9330()*0.0677741);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca930() {
   return (neuron0x8dc76a0()*0.363334);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca958() {
   return (neuron0x8dc7880()*-0.218077);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca980() {
   return (neuron0x8dc7aa8()*0.423507);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca9a8() {
   return (neuron0x8dc7ca8()*-0.211286);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca9d0() {
   return (neuron0x8dc7ea8()*0.396916);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf2680() {
   return (neuron0x8dc80d0()*-0.103129);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf26a8() {
   return (neuron0x8dc82d0()*-0.409096);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf28a8() {
   return (neuron0x8dc84d0()*-0.387527);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf28d0() {
   return (neuron0x8dc86d0()*0.251269);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf28f8() {
   return (neuron0x8dc88d0()*-0.275654);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf2920() {
   return (neuron0x8dc8ad0()*0.355394);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce478() {
   return (neuron0x8dc8ce8()*0.222967);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce4a0() {
   return (neuron0x8dc8f00()*0.406678);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce4c8() {
   return (neuron0x8dc9118()*-0.200469);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce4f0() {
   return (neuron0x8dc9330()*-0.0400165);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcae00() {
   return (neuron0x8dc76a0()*-0.288441);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcae28() {
   return (neuron0x8dc7880()*-0.489125);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcae50() {
   return (neuron0x8dc7aa8()*0.441911);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcae78() {
   return (neuron0x8dc7ca8()*-0.453386);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcaea0() {
   return (neuron0x8dc7ea8()*-0.313127);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcaec8() {
   return (neuron0x8dc80d0()*-0.0614079);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcaef0() {
   return (neuron0x8dc82d0()*0.39652);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcaf18() {
   return (neuron0x8dc84d0()*-0.489271);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcaf40() {
   return (neuron0x8dc86d0()*-0.0568099);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcaf68() {
   return (neuron0x8dc88d0()*-0.373908);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcaf90() {
   return (neuron0x8dc8ad0()*0.461801);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcafb8() {
   return (neuron0x8dc8ce8()*0.3564);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcafe0() {
   return (neuron0x8dc8f00()*0.317543);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb008() {
   return (neuron0x8dc9118()*-0.00317594);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb030() {
   return (neuron0x8dc9330()*-0.258986);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb270() {
   return (neuron0x8dc76a0()*0.189567);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb298() {
   return (neuron0x8dc7880()*0.111028);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb2c0() {
   return (neuron0x8dc7aa8()*-0.231977);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb2e8() {
   return (neuron0x8dc7ca8()*0.32549);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb310() {
   return (neuron0x8dc7ea8()*-0.453112);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb338() {
   return (neuron0x8dc80d0()*-0.465723);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb360() {
   return (neuron0x8dc82d0()*-0.0252818);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb388() {
   return (neuron0x8dc84d0()*-0.0537897);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb3b0() {
   return (neuron0x8dc86d0()*0.17448);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb3d8() {
   return (neuron0x8dc88d0()*0.0765362);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb400() {
   return (neuron0x8dc8ad0()*-0.0462079);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb428() {
   return (neuron0x8dc8ce8()*0.354333);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb450() {
   return (neuron0x8dc8f00()*-0.484464);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb478() {
   return (neuron0x8dc9118()*0.182387);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb4a0() {
   return (neuron0x8dc9330()*-0.293973);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb6e0() {
   return (neuron0x8dc76a0()*0.447487);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb708() {
   return (neuron0x8dc7880()*-0.29442);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb730() {
   return (neuron0x8dc7aa8()*0.0289604);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb758() {
   return (neuron0x8dc7ca8()*0.591487);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb780() {
   return (neuron0x8dc7ea8()*-0.240015);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb7a8() {
   return (neuron0x8dc80d0()*0.107333);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb7d0() {
   return (neuron0x8dc82d0()*-0.0128481);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb7f8() {
   return (neuron0x8dc84d0()*-0.35146);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb820() {
   return (neuron0x8dc86d0()*-0.265898);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb848() {
   return (neuron0x8dc88d0()*0.278453);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb870() {
   return (neuron0x8dc8ad0()*-0.126785);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb898() {
   return (neuron0x8dc8ce8()*0.0992156);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb8c0() {
   return (neuron0x8dc8f00()*0.487573);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb8e8() {
   return (neuron0x8dc9118()*-0.0988535);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcb910() {
   return (neuron0x8dc9330()*-0.266456);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbb50() {
   return (neuron0x8dc76a0()*-0.164397);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbb78() {
   return (neuron0x8dc7880()*-0.283448);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbba0() {
   return (neuron0x8dc7aa8()*0.0544014);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbbc8() {
   return (neuron0x8dc7ca8()*0.230201);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbbf0() {
   return (neuron0x8dc7ea8()*-0.262913);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbc18() {
   return (neuron0x8dc80d0()*-0.387914);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbc40() {
   return (neuron0x8dc82d0()*0.255081);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbc68() {
   return (neuron0x8dc84d0()*0.221257);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbc90() {
   return (neuron0x8dc86d0()*0.107625);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce518() {
   return (neuron0x8dc88d0()*-0.151327);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf2998() {
   return (neuron0x8dc8ad0()*-0.11317);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf29c0() {
   return (neuron0x8dc8ce8()*-0.081902);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf29e8() {
   return (neuron0x8dc8f00()*-0.370757);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf2a10() {
   return (neuron0x8dc9118()*0.0847824);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8bf2a38() {
   return (neuron0x8dc9330()*0.274248);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca130() {
   return (neuron0x8dc76a0()*0.284911);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca158() {
   return (neuron0x8dc7880()*0.334742);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca180() {
   return (neuron0x8dc7aa8()*-0.160644);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca1a8() {
   return (neuron0x8dc7ca8()*0.464377);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca1d0() {
   return (neuron0x8dc7ea8()*0.450081);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dca1f8() {
   return (neuron0x8dc80d0()*0.0661809);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc0c0() {
   return (neuron0x8dc82d0()*0.117637);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc0e8() {
   return (neuron0x8dc84d0()*0.423153);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc110() {
   return (neuron0x8dc86d0()*-0.364058);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc138() {
   return (neuron0x8dc88d0()*-0.289548);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc160() {
   return (neuron0x8dc8ad0()*-0.191823);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc188() {
   return (neuron0x8dc8ce8()*0.135362);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc1b0() {
   return (neuron0x8dc8f00()*-0.476878);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc1d8() {
   return (neuron0x8dc9118()*-0.219983);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc200() {
   return (neuron0x8dc9330()*-0.367079);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc428() {
   return (neuron0x8dc76a0()*-0.127922);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc450() {
   return (neuron0x8dc7880()*0.0543165);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc478() {
   return (neuron0x8dc7aa8()*0.0715109);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc4a0() {
   return (neuron0x8dc7ca8()*-0.239225);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc4c8() {
   return (neuron0x8dc7ea8()*-0.243564);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc4f0() {
   return (neuron0x8dc80d0()*0.0815551);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc518() {
   return (neuron0x8dc82d0()*0.0207972);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc540() {
   return (neuron0x8dc84d0()*0.408506);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc568() {
   return (neuron0x8dc86d0()*0.0617797);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc590() {
   return (neuron0x8dc88d0()*-0.237251);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc5b8() {
   return (neuron0x8dc8ad0()*0.245381);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc5e0() {
   return (neuron0x8dc8ce8()*-0.147484);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc608() {
   return (neuron0x8dc8f00()*-0.521941);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc630() {
   return (neuron0x8dc9118()*-0.20047);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc658() {
   return (neuron0x8dc9330()*-0.0277059);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc880() {
   return (neuron0x8dc76a0()*0.0991766);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc8a8() {
   return (neuron0x8dc7880()*0.232629);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc8d0() {
   return (neuron0x8dc7aa8()*-0.135553);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc8f8() {
   return (neuron0x8dc7ca8()*0.404829);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc920() {
   return (neuron0x8dc7ea8()*-0.354174);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc948() {
   return (neuron0x8dc80d0()*-0.340451);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc970() {
   return (neuron0x8dc82d0()*-0.0201974);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc998() {
   return (neuron0x8dc84d0()*-0.329299);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc9c0() {
   return (neuron0x8dc86d0()*0.176938);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc9e8() {
   return (neuron0x8dc88d0()*-0.458017);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcca10() {
   return (neuron0x8dc8ad0()*0.0732184);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcca38() {
   return (neuron0x8dc8ce8()*-0.453654);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcca60() {
   return (neuron0x8dc8f00()*0.217832);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcca88() {
   return (neuron0x8dc9118()*-0.304342);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccab0() {
   return (neuron0x8dc9330()*-0.0361571);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcccf0() {
   return (neuron0x8dc76a0()*0.37498);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccd18() {
   return (neuron0x8dc7880()*0.427515);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccd40() {
   return (neuron0x8dc7aa8()*-0.401423);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccd68() {
   return (neuron0x8dc7ca8()*0.0992058);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccd90() {
   return (neuron0x8dc7ea8()*0.292287);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccdb8() {
   return (neuron0x8dc80d0()*-0.353673);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccde0() {
   return (neuron0x8dc82d0()*-0.389517);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcce08() {
   return (neuron0x8dc84d0()*0.27039);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcce30() {
   return (neuron0x8dc86d0()*0.350581);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcce58() {
   return (neuron0x8dc88d0()*-0.211473);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcce80() {
   return (neuron0x8dc8ad0()*-0.268594);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccea8() {
   return (neuron0x8dc8ce8()*0.128003);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcced0() {
   return (neuron0x8dc8f00()*-0.340772);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccef8() {
   return (neuron0x8dc9118()*-0.413739);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dccf20() {
   return (neuron0x8dc9330()*0.176942);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd160() {
   return (neuron0x8dc76a0()*0.2262);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd188() {
   return (neuron0x8dc7880()*0.464759);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd1b0() {
   return (neuron0x8dc7aa8()*-0.210084);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd1d8() {
   return (neuron0x8dc7ca8()*-0.0947443);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd200() {
   return (neuron0x8dc7ea8()*0.261416);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd228() {
   return (neuron0x8dc80d0()*-0.233573);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd250() {
   return (neuron0x8dc82d0()*0.402818);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd278() {
   return (neuron0x8dc84d0()*-0.18574);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd2a0() {
   return (neuron0x8dc86d0()*-0.244194);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd2c8() {
   return (neuron0x8dc88d0()*0.198978);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd2f0() {
   return (neuron0x8dc8ad0()*-0.0444626);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd318() {
   return (neuron0x8dc8ce8()*-0.0507164);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd340() {
   return (neuron0x8dc8f00()*0.315134);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd368() {
   return (neuron0x8dc9118()*0.0148665);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd390() {
   return (neuron0x8dc9330()*0.0903528);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcd448() {
   return (neuron0x8dc76a0()*-0.121761);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce718() {
   return (neuron0x8dc7880()*0.463087);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce740() {
   return (neuron0x8dc7aa8()*0.331628);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce768() {
   return (neuron0x8dc7ca8()*0.266818);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce790() {
   return (neuron0x8dc7ea8()*0.324189);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce7b8() {
   return (neuron0x8dc80d0()*-0.214891);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce7e0() {
   return (neuron0x8dc82d0()*-0.24115);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce808() {
   return (neuron0x8dc84d0()*0.00817624);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce830() {
   return (neuron0x8dc86d0()*0.00105058);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce858() {
   return (neuron0x8dc88d0()*0.30063);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce880() {
   return (neuron0x8dc8ad0()*0.467097);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce8a8() {
   return (neuron0x8dc8ce8()*-0.491078);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce8d0() {
   return (neuron0x8dc8f00()*0.198639);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce8f8() {
   return (neuron0x8dc9118()*0.0310354);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dce920() {
   return (neuron0x8dc9330()*0.0571336);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dceb48() {
   return (neuron0x8dc76a0()*0.216172);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcebf8() {
   return (neuron0x8dc7880()*-0.087356);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dceca8() {
   return (neuron0x8dc7aa8()*-0.453117);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dced58() {
   return (neuron0x8dc7ca8()*-0.00808201);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcee08() {
   return (neuron0x8dc7ea8()*-0.0547808);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dceeb8() {
   return (neuron0x8dc80d0()*-0.439514);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcef68() {
   return (neuron0x8dc82d0()*0.355866);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf018() {
   return (neuron0x8dc84d0()*0.186012);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf0c8() {
   return (neuron0x8dc86d0()*0.0728103);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf178() {
   return (neuron0x8dc88d0()*-0.370247);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf228() {
   return (neuron0x8dc8ad0()*-0.228233);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf2d8() {
   return (neuron0x8dc8ce8()*-0.324538);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf388() {
   return (neuron0x8dc8f00()*0.351778);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf438() {
   return (neuron0x8dc9118()*0.0385505);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf4e8() {
   return (neuron0x8dc9330()*-0.178544);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf6c0() {
   return (neuron0x8dc76a0()*0.0970303);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf6e8() {
   return (neuron0x8dc7880()*-0.114732);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf710() {
   return (neuron0x8dc7aa8()*-0.356742);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf738() {
   return (neuron0x8dc7ca8()*-0.474853);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf760() {
   return (neuron0x8dc7ea8()*0.213045);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf788() {
   return (neuron0x8dc80d0()*0.255158);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf7b0() {
   return (neuron0x8dc82d0()*0.293179);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf7d8() {
   return (neuron0x8dc84d0()*0.179009);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf800() {
   return (neuron0x8dc86d0()*0.296754);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf828() {
   return (neuron0x8dc88d0()*-0.401787);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf850() {
   return (neuron0x8dc8ad0()*-0.344584);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf878() {
   return (neuron0x8dc8ce8()*-0.408243);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf8a0() {
   return (neuron0x8dc8f00()*0.420581);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf8c8() {
   return (neuron0x8dc9118()*-0.330055);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf8f0() {
   return (neuron0x8dc9330()*0.304557);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9be0() {
   return (neuron0x8dc76a0()*0.349311);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9c08() {
   return (neuron0x8dc7880()*0.142122);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc9c30() {
   return (neuron0x8dc7aa8()*-0.157615);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc8840() {
   return (neuron0x8dc7ca8()*0.414363);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc8640() {
   return (neuron0x8dc7ea8()*-0.237466);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc8440() {
   return (neuron0x8dc80d0()*0.356343);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc8240() {
   return (neuron0x8dc82d0()*-0.00753322);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc8040() {
   return (neuron0x8dc84d0()*0.192612);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc7e18() {
   return (neuron0x8dc86d0()*0.145042);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc7c18() {
   return (neuron0x8dc88d0()*0.362992);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc7a18() {
   return (neuron0x8dc8ad0()*-0.0283547);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dc77f0() {
   return (neuron0x8dc8ce8()*0.433258);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbcb8() {
   return (neuron0x8dc8f00()*-0.235397);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbce0() {
   return (neuron0x8dc9118()*0.268601);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbd08() {
   return (neuron0x8dc9330()*-0.368179);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbf48() {
   return (neuron0x8dc76a0()*0.328261);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbf70() {
   return (neuron0x8dc7880()*-0.283536);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbf98() {
   return (neuron0x8dc7aa8()*-0.176855);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbfc0() {
   return (neuron0x8dc7ca8()*-0.411297);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcbfe8() {
   return (neuron0x8dc7ea8()*0.313718);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc010() {
   return (neuron0x8dc80d0()*0.101853);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc038() {
   return (neuron0x8dc82d0()*0.206017);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc060() {
   return (neuron0x8dc84d0()*-0.24918);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcc088() {
   return (neuron0x8dc86d0()*-0.28422);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0350() {
   return (neuron0x8dc88d0()*0.244173);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0378() {
   return (neuron0x8dc8ad0()*-0.130683);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd03a0() {
   return (neuron0x8dc8ce8()*0.169524);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd03c8() {
   return (neuron0x8dc8f00()*0.397021);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd03f0() {
   return (neuron0x8dc9118()*0.266759);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0418() {
   return (neuron0x8dc9330()*0.264028);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0640() {
   return (neuron0x8dc76a0()*-0.0913195);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0668() {
   return (neuron0x8dc7880()*-0.513909);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0690() {
   return (neuron0x8dc7aa8()*0.381792);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd06b8() {
   return (neuron0x8dc7ca8()*0.231983);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd06e0() {
   return (neuron0x8dc7ea8()*-0.0737246);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0708() {
   return (neuron0x8dc80d0()*0.254241);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0730() {
   return (neuron0x8dc82d0()*-0.352485);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0758() {
   return (neuron0x8dc84d0()*-0.427232);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0780() {
   return (neuron0x8dc86d0()*0.0934452);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd07a8() {
   return (neuron0x8dc88d0()*0.367677);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd07d0() {
   return (neuron0x8dc8ad0()*-0.165216);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd07f8() {
   return (neuron0x8dc8ce8()*-0.288369);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0820() {
   return (neuron0x8dc8f00()*-0.333624);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0848() {
   return (neuron0x8dc9118()*-0.0658487);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0870() {
   return (neuron0x8dc9330()*0.426903);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0a98() {
   return (neuron0x8dc76a0()*-0.0379679);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ac0() {
   return (neuron0x8dc7880()*0.288501);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ae8() {
   return (neuron0x8dc7aa8()*-0.14771);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0b10() {
   return (neuron0x8dc7ca8()*0.361137);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0b38() {
   return (neuron0x8dc7ea8()*0.219149);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0b60() {
   return (neuron0x8dc80d0()*-0.247716);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0b88() {
   return (neuron0x8dc82d0()*0.309548);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0bb0() {
   return (neuron0x8dc84d0()*-0.0938517);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0bd8() {
   return (neuron0x8dc86d0()*0.105409);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0c00() {
   return (neuron0x8dc88d0()*-0.304087);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0c28() {
   return (neuron0x8dc8ad0()*-0.420165);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0c50() {
   return (neuron0x8dc8ce8()*0.457826);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0c78() {
   return (neuron0x8dc8f00()*-0.312069);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ca0() {
   return (neuron0x8dc9118()*0.168644);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0cc8() {
   return (neuron0x8dc9330()*-0.491497);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ef0() {
   return (neuron0x8dc76a0()*-0.468141);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0f18() {
   return (neuron0x8dc7880()*0.282866);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0f40() {
   return (neuron0x8dc7aa8()*0.55609);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0f68() {
   return (neuron0x8dc7ca8()*0.418152);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0f90() {
   return (neuron0x8dc7ea8()*-0.0488445);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0fb8() {
   return (neuron0x8dc80d0()*-0.00655318);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0fe0() {
   return (neuron0x8dc82d0()*-0.0094606);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1008() {
   return (neuron0x8dc84d0()*-0.125922);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1030() {
   return (neuron0x8dc86d0()*-0.147335);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1058() {
   return (neuron0x8dc88d0()*0.0753897);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1080() {
   return (neuron0x8dc8ad0()*0.0488274);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd10a8() {
   return (neuron0x8dc8ce8()*-0.0658239);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd10d0() {
   return (neuron0x8dc8f00()*0.320874);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd10f8() {
   return (neuron0x8dc9118()*0.0654684);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1120() {
   return (neuron0x8dc9330()*-0.0729131);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1348() {
   return (neuron0x8dc76a0()*-0.00794621);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1370() {
   return (neuron0x8dc7880()*-0.410131);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1398() {
   return (neuron0x8dc7aa8()*-0.372091);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd13c0() {
   return (neuron0x8dc7ca8()*-0.0757313);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd13e8() {
   return (neuron0x8dc7ea8()*0.13213);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1410() {
   return (neuron0x8dc80d0()*0.453777);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1438() {
   return (neuron0x8dc82d0()*-0.362746);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1460() {
   return (neuron0x8dc84d0()*0.188986);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1488() {
   return (neuron0x8dc86d0()*-0.32664);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd14b0() {
   return (neuron0x8dc88d0()*0.14761);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd14d8() {
   return (neuron0x8dc8ad0()*0.250505);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1500() {
   return (neuron0x8dc8ce8()*-0.0580423);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1528() {
   return (neuron0x8dc8f00()*0.464457);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1550() {
   return (neuron0x8dc9118()*-0.470916);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1578() {
   return (neuron0x8dc9330()*-0.181388);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd17a0() {
   return (neuron0x8dc76a0()*0.0607768);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd17c8() {
   return (neuron0x8dc7880()*0.322044);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd17f0() {
   return (neuron0x8dc7aa8()*-0.268014);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1818() {
   return (neuron0x8dc7ca8()*0.197184);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1840() {
   return (neuron0x8dc7ea8()*-0.274873);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1868() {
   return (neuron0x8dc80d0()*-0.24862);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1890() {
   return (neuron0x8dc82d0()*-0.232557);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd18b8() {
   return (neuron0x8dc84d0()*-0.128184);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd18e0() {
   return (neuron0x8dc86d0()*0.375317);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1908() {
   return (neuron0x8dc88d0()*0.275828);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1930() {
   return (neuron0x8dc8ad0()*-0.24606);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1958() {
   return (neuron0x8dc8ce8()*-0.138873);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1980() {
   return (neuron0x8dc8f00()*0.434733);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd19a8() {
   return (neuron0x8dc9118()*-0.483679);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd19d0() {
   return (neuron0x8dc9330()*0.366264);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1bf8() {
   return (neuron0x8dc76a0()*-0.367857);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1c20() {
   return (neuron0x8dc7880()*-0.503074);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1c48() {
   return (neuron0x8dc7aa8()*-0.0300124);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1c70() {
   return (neuron0x8dc7ca8()*-0.147297);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1c98() {
   return (neuron0x8dc7ea8()*-0.327637);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1cc0() {
   return (neuron0x8dc80d0()*-0.0190782);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1ce8() {
   return (neuron0x8dc82d0()*0.200792);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1d10() {
   return (neuron0x8dc84d0()*-0.149959);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1d38() {
   return (neuron0x8dc86d0()*0.403137);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1d60() {
   return (neuron0x8dc88d0()*-0.471185);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1d88() {
   return (neuron0x8dc8ad0()*-0.198767);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1db0() {
   return (neuron0x8dc8ce8()*-0.212875);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1dd8() {
   return (neuron0x8dc8f00()*0.196217);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1e00() {
   return (neuron0x8dc9118()*-0.0307548);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd1e28() {
   return (neuron0x8dc9330()*-0.0848958);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2050() {
   return (neuron0x8dc76a0()*-0.26396);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2078() {
   return (neuron0x8dc7880()*0.106549);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd20a0() {
   return (neuron0x8dc7aa8()*-0.165263);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd20c8() {
   return (neuron0x8dc7ca8()*-0.129817);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd20f0() {
   return (neuron0x8dc7ea8()*0.065296);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2118() {
   return (neuron0x8dc80d0()*0.361735);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2140() {
   return (neuron0x8dc82d0()*-0.0997624);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2168() {
   return (neuron0x8dc84d0()*-0.212615);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2190() {
   return (neuron0x8dc86d0()*0.278064);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd21b8() {
   return (neuron0x8dc88d0()*0.333168);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd21e0() {
   return (neuron0x8dc8ad0()*-0.158065);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2208() {
   return (neuron0x8dc8ce8()*0.143738);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2230() {
   return (neuron0x8dc8f00()*0.0118247);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2258() {
   return (neuron0x8dc9118()*0.412396);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2280() {
   return (neuron0x8dc9330()*-0.187788);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd24a8() {
   return (neuron0x8dc76a0()*-0.203072);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd24d0() {
   return (neuron0x8dc7880()*-0.111626);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd24f8() {
   return (neuron0x8dc7aa8()*-0.351395);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2520() {
   return (neuron0x8dc7ca8()*0.109157);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2548() {
   return (neuron0x8dc7ea8()*0.182895);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2570() {
   return (neuron0x8dc80d0()*-0.341481);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2598() {
   return (neuron0x8dc82d0()*0.113373);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd25c0() {
   return (neuron0x8dc84d0()*-0.0417804);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd25e8() {
   return (neuron0x8dc86d0()*0.424165);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2610() {
   return (neuron0x8dc88d0()*0.41918);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2638() {
   return (neuron0x8dc8ad0()*-0.379164);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2660() {
   return (neuron0x8dc8ce8()*-0.119471);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2688() {
   return (neuron0x8dc8f00()*-0.285603);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd26b0() {
   return (neuron0x8dc9118()*0.31597);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd26d8() {
   return (neuron0x8dc9330()*-0.417458);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2900() {
   return (neuron0x8dc76a0()*-0.0208406);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2928() {
   return (neuron0x8dc7880()*0.486045);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2950() {
   return (neuron0x8dc7aa8()*-0.135251);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2978() {
   return (neuron0x8dc7ca8()*0.279328);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd29a0() {
   return (neuron0x8dc7ea8()*0.208911);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd29c8() {
   return (neuron0x8dc80d0()*0.153814);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd29f0() {
   return (neuron0x8dc82d0()*0.111874);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2a18() {
   return (neuron0x8dc84d0()*-0.384124);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2a40() {
   return (neuron0x8dc86d0()*0.317003);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2a68() {
   return (neuron0x8dc88d0()*-0.189046);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2a90() {
   return (neuron0x8dc8ad0()*0.0894488);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2ab8() {
   return (neuron0x8dc8ce8()*0.251123);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2ae0() {
   return (neuron0x8dc8f00()*0.462295);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2b08() {
   return (neuron0x8dc9118()*-0.263681);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2b30() {
   return (neuron0x8dc9330()*0.23547);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2d58() {
   return (neuron0x8dc76a0()*-0.300839);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2d80() {
   return (neuron0x8dc7880()*-0.305343);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2da8() {
   return (neuron0x8dc7aa8()*0.263614);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2dd0() {
   return (neuron0x8dc7ca8()*-0.291376);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2df8() {
   return (neuron0x8dc7ea8()*0.0586587);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2e20() {
   return (neuron0x8dc80d0()*-0.387489);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2e48() {
   return (neuron0x8dc82d0()*-0.00544789);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2e70() {
   return (neuron0x8dc84d0()*0.333686);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2e98() {
   return (neuron0x8dc86d0()*-0.0302669);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2ec0() {
   return (neuron0x8dc88d0()*-0.133399);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2ee8() {
   return (neuron0x8dc8ad0()*-0.206459);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2f10() {
   return (neuron0x8dc8ce8()*-0.248755);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2f38() {
   return (neuron0x8dc8f00()*-0.0492681);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2f60() {
   return (neuron0x8dc9118()*-0.137488);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd2f88() {
   return (neuron0x8dc9330()*-0.41102);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd31b0() {
   return (neuron0x8dc76a0()*-0.438014);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd31d8() {
   return (neuron0x8dc7880()*-0.511979);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3200() {
   return (neuron0x8dc7aa8()*0.185315);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3228() {
   return (neuron0x8dc7ca8()*0.386031);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3250() {
   return (neuron0x8dc7ea8()*-0.102121);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3278() {
   return (neuron0x8dc80d0()*0.261278);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd32a0() {
   return (neuron0x8dc82d0()*-0.389207);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd32c8() {
   return (neuron0x8dc84d0()*0.00613768);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd32f0() {
   return (neuron0x8dc86d0()*0.237925);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3318() {
   return (neuron0x8dc88d0()*0.29223);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3340() {
   return (neuron0x8dc8ad0()*0.341423);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3368() {
   return (neuron0x8dc8ce8()*0.325819);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3390() {
   return (neuron0x8dc8f00()*-0.35495);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd33b8() {
   return (neuron0x8dc9118()*-0.3388);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd33e0() {
   return (neuron0x8dc9330()*0.410799);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3608() {
   return (neuron0x8dc76a0()*0.555385);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3630() {
   return (neuron0x8dc7880()*0.263611);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3658() {
   return (neuron0x8dc7aa8()*-0.164662);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3680() {
   return (neuron0x8dc7ca8()*0.0837221);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd36a8() {
   return (neuron0x8dc7ea8()*-0.444381);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd36d0() {
   return (neuron0x8dc80d0()*-0.0458916);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd36f8() {
   return (neuron0x8dc82d0()*-0.506493);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3720() {
   return (neuron0x8dc84d0()*-0.115113);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3748() {
   return (neuron0x8dc86d0()*0.300195);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3770() {
   return (neuron0x8dc88d0()*0.291574);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3798() {
   return (neuron0x8dc8ad0()*0.425777);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd37c0() {
   return (neuron0x8dc8ce8()*-0.222191);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd37e8() {
   return (neuron0x8dc8f00()*-0.331681);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3810() {
   return (neuron0x8dc9118()*0.26692);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3838() {
   return (neuron0x8dc9330()*-0.228315);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd3a60() {
   return (neuron0x8dc76a0()*0.158515);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dceb70() {
   return (neuron0x8dc7880()*0.359241);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dceb98() {
   return (neuron0x8dc7aa8()*0.0850459);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcebc0() {
   return (neuron0x8dc7ca8()*0.487032);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcec20() {
   return (neuron0x8dc7ea8()*-0.307281);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcec48() {
   return (neuron0x8dc80d0()*0.194713);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcec70() {
   return (neuron0x8dc82d0()*0.0934284);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcecd0() {
   return (neuron0x8dc84d0()*-0.261553);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcecf8() {
   return (neuron0x8dc86d0()*0.46218);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dced20() {
   return (neuron0x8dc88d0()*0.433155);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dced80() {
   return (neuron0x8dc8ad0()*0.355381);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dceda8() {
   return (neuron0x8dc8ce8()*-0.202952);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcedd0() {
   return (neuron0x8dc8f00()*-0.24204);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcee30() {
   return (neuron0x8dc9118()*0.264649);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcee58() {
   return (neuron0x8dc9330()*0.386067);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf088() {
   return (neuron0x8dc76a0()*0.0897024);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcee80() {
   return (neuron0x8dc7880()*-0.0168749);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcef28() {
   return (neuron0x8dc7aa8()*-0.340497);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcefd8() {
   return (neuron0x8dc7ca8()*0.264144);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf0f0() {
   return (neuron0x8dc7ea8()*0.176638);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf118() {
   return (neuron0x8dc80d0()*0.0207319);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf140() {
   return (neuron0x8dc82d0()*0.114496);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf1a0() {
   return (neuron0x8dc84d0()*0.315057);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf1c8() {
   return (neuron0x8dc86d0()*-0.573969);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf1f0() {
   return (neuron0x8dc88d0()*-0.0165016);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf250() {
   return (neuron0x8dc8ad0()*-0.188639);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf278() {
   return (neuron0x8dc8ce8()*0.343639);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf2a0() {
   return (neuron0x8dc8f00()*-0.337801);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf300() {
   return (neuron0x8dc9118()*-0.44678);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf328() {
   return (neuron0x8dc9330()*-0.195741);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf558() {
   return (neuron0x8dc76a0()*0.395607);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf350() {
   return (neuron0x8dc7880()*0.154717);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf3f8() {
   return (neuron0x8dc7aa8()*-0.517123);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcf4a8() {
   return (neuron0x8dc7ca8()*-0.0182225);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4c50() {
   return (neuron0x8dc7ea8()*-0.291288);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4c78() {
   return (neuron0x8dc80d0()*-0.129256);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4ca0() {
   return (neuron0x8dc82d0()*0.112569);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4cc8() {
   return (neuron0x8dc84d0()*0.244918);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4cf0() {
   return (neuron0x8dc86d0()*0.213375);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4d18() {
   return (neuron0x8dc88d0()*-0.441821);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4d40() {
   return (neuron0x8dc8ad0()*0.382667);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4d68() {
   return (neuron0x8dc8ce8()*-0.540276);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4d90() {
   return (neuron0x8dc8f00()*-0.414961);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4db8() {
   return (neuron0x8dc9118()*0.475126);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd4de0() {
   return (neuron0x8dc9330()*-0.226613);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd5008() {
   return (neuron0x8dc76a0()*0.211636);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd5030() {
   return (neuron0x8dc7880()*0.37124);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd5058() {
   return (neuron0x8dc7aa8()*0.10826);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfb48() {
   return (neuron0x8dc7ca8()*0.421966);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfb70() {
   return (neuron0x8dc7ea8()*0.226291);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfb98() {
   return (neuron0x8dc80d0()*-0.0960833);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfbc0() {
   return (neuron0x8dc82d0()*-0.0309962);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfbe8() {
   return (neuron0x8dc84d0()*0.215653);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfc10() {
   return (neuron0x8dc86d0()*0.408671);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfc38() {
   return (neuron0x8dc88d0()*0.321832);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfc60() {
   return (neuron0x8dc8ad0()*-0.452204);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfc88() {
   return (neuron0x8dc8ce8()*0.0217816);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfcb0() {
   return (neuron0x8dc8f00()*0.321283);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfcd8() {
   return (neuron0x8dc9118()*0.290454);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfd00() {
   return (neuron0x8dc9330()*0.478676);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcff28() {
   return (neuron0x8dc76a0()*-0.0959148);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcff50() {
   return (neuron0x8dc7880()*-0.475389);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcff78() {
   return (neuron0x8dc7aa8()*-0.0589244);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcffa0() {
   return (neuron0x8dc7ca8()*0.308289);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcffc8() {
   return (neuron0x8dc7ea8()*-0.0620226);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfff0() {
   return (neuron0x8dc80d0()*0.351566);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0018() {
   return (neuron0x8dc82d0()*-0.109963);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0040() {
   return (neuron0x8dc84d0()*-0.107702);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0068() {
   return (neuron0x8dc86d0()*0.296037);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0090() {
   return (neuron0x8dc88d0()*0.272731);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd00b8() {
   return (neuron0x8dc8ad0()*-0.320869);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd00e0() {
   return (neuron0x8dc8ce8()*0.0800864);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0108() {
   return (neuron0x8dc8f00()*0.132675);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0130() {
   return (neuron0x8dc9118()*0.388045);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd0158() {
   return (neuron0x8dc9330()*-0.150605);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd60d0() {
   return (neuron0x8dc76a0()*-0.1236);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd60f8() {
   return (neuron0x8dc7880()*0.0504568);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6120() {
   return (neuron0x8dc7aa8()*-0.287781);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6148() {
   return (neuron0x8dc7ca8()*0.254677);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6170() {
   return (neuron0x8dc7ea8()*0.375804);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6198() {
   return (neuron0x8dc80d0()*0.0363922);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd61c0() {
   return (neuron0x8dc82d0()*-0.181798);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd61e8() {
   return (neuron0x8dc84d0()*0.0267847);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6210() {
   return (neuron0x8dc86d0()*-0.32653);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6238() {
   return (neuron0x8dc88d0()*0.345359);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6260() {
   return (neuron0x8dc8ad0()*-0.431311);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6288() {
   return (neuron0x8dc8ce8()*0.159953);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd62b0() {
   return (neuron0x8dc8f00()*0.405044);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd62d8() {
   return (neuron0x8dc9118()*-0.414125);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6300() {
   return (neuron0x8dc9330()*-0.11899);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6528() {
   return (neuron0x8dc76a0()*-0.0773489);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6550() {
   return (neuron0x8dc7880()*-0.0949699);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6578() {
   return (neuron0x8dc7aa8()*0.415772);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd65a0() {
   return (neuron0x8dc7ca8()*0.147444);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd65c8() {
   return (neuron0x8dc7ea8()*0.0776994);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd65f0() {
   return (neuron0x8dc80d0()*-0.177787);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6618() {
   return (neuron0x8dc82d0()*0.144573);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6640() {
   return (neuron0x8dc84d0()*-0.384704);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6668() {
   return (neuron0x8dc86d0()*-0.0130396);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6690() {
   return (neuron0x8dc88d0()*0.105079);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd66b8() {
   return (neuron0x8dc8ad0()*0.275606);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd66e0() {
   return (neuron0x8dc8ce8()*-0.189619);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6708() {
   return (neuron0x8dc8f00()*0.173797);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6730() {
   return (neuron0x8dc9118()*-0.271518);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6758() {
   return (neuron0x8dc9330()*0.243329);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6980() {
   return (neuron0x8dc76a0()*-0.169605);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd69a8() {
   return (neuron0x8dc7880()*-0.367385);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd69d0() {
   return (neuron0x8dc7aa8()*-0.00713685);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd69f8() {
   return (neuron0x8dc7ca8()*-0.074373);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6a20() {
   return (neuron0x8dc7ea8()*0.241582);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6a48() {
   return (neuron0x8dc80d0()*0.238638);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6a70() {
   return (neuron0x8dc82d0()*0.303082);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6a98() {
   return (neuron0x8dc84d0()*-0.147414);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6ac0() {
   return (neuron0x8dc86d0()*-0.271272);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6ae8() {
   return (neuron0x8dc88d0()*-0.375452);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6b10() {
   return (neuron0x8dc8ad0()*0.0584276);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6b38() {
   return (neuron0x8dc8ce8()*-0.438534);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6b60() {
   return (neuron0x8dc8f00()*0.207615);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6b88() {
   return (neuron0x8dc9118()*-0.176146);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6bb0() {
   return (neuron0x8dc9330()*-0.47603);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6dd8() {
   return (neuron0x8dc76a0()*0.420546);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6e00() {
   return (neuron0x8dc7880()*0.0842109);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6e28() {
   return (neuron0x8dc7aa8()*0.0606381);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6e50() {
   return (neuron0x8dc7ca8()*0.473904);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6e78() {
   return (neuron0x8dc7ea8()*0.423227);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6ea0() {
   return (neuron0x8dc80d0()*-0.222292);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6ec8() {
   return (neuron0x8dc82d0()*0.153701);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6ef0() {
   return (neuron0x8dc84d0()*-0.187637);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6f18() {
   return (neuron0x8dc86d0()*-0.451387);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6f40() {
   return (neuron0x8dc88d0()*0.373806);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6f68() {
   return (neuron0x8dc8ad0()*-0.228775);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6f90() {
   return (neuron0x8dc8ce8()*0.419032);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6fb8() {
   return (neuron0x8dc8f00()*-0.421493);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd6fe0() {
   return (neuron0x8dc9118()*0.321521);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7008() {
   return (neuron0x8dc9330()*-0.110662);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7230() {
   return (neuron0x8dc76a0()*-0.319352);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7258() {
   return (neuron0x8dc7880()*0.191566);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7280() {
   return (neuron0x8dc7aa8()*0.224005);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd72a8() {
   return (neuron0x8dc7ca8()*-0.3629);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd72d0() {
   return (neuron0x8dc7ea8()*0.223778);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd72f8() {
   return (neuron0x8dc80d0()*-0.225073);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7320() {
   return (neuron0x8dc82d0()*0.288853);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7348() {
   return (neuron0x8dc84d0()*-0.308605);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7370() {
   return (neuron0x8dc86d0()*0.459221);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7398() {
   return (neuron0x8dc88d0()*0.170024);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd73c0() {
   return (neuron0x8dc8ad0()*0.41794);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd73e8() {
   return (neuron0x8dc8ce8()*0.493103);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7410() {
   return (neuron0x8dc8f00()*-0.197846);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7438() {
   return (neuron0x8dc9118()*-0.299532);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7460() {
   return (neuron0x8dc9330()*-0.0879692);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd76a0() {
   return (neuron0x8dc76a0()*0.31003);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd76c8() {
   return (neuron0x8dc7880()*0.0648043);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd76f0() {
   return (neuron0x8dc7aa8()*0.330228);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7718() {
   return (neuron0x8dc7ca8()*0.0837542);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7740() {
   return (neuron0x8dc7ea8()*-0.251669);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7768() {
   return (neuron0x8dc80d0()*0.312761);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7790() {
   return (neuron0x8dc82d0()*0.265322);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd77b8() {
   return (neuron0x8dc84d0()*-0.323262);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd77e0() {
   return (neuron0x8dc86d0()*0.422049);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7808() {
   return (neuron0x8dc88d0()*0.428311);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7830() {
   return (neuron0x8dc8ad0()*0.157713);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7858() {
   return (neuron0x8dc8ce8()*-0.157634);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7880() {
   return (neuron0x8dc8f00()*-0.0548981);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd78a8() {
   return (neuron0x8dc9118()*-0.291268);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd78d0() {
   return (neuron0x8dc9330()*0.166976);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7b10() {
   return (neuron0x8dc76a0()*-0.372695);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7b38() {
   return (neuron0x8dc7880()*-0.243064);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7b60() {
   return (neuron0x8dc7aa8()*0.486253);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7b88() {
   return (neuron0x8dc7ca8()*-0.235836);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7bb0() {
   return (neuron0x8dc7ea8()*-0.342782);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7bd8() {
   return (neuron0x8dc80d0()*-0.141219);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7c00() {
   return (neuron0x8dc82d0()*0.0164908);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7c28() {
   return (neuron0x8dc84d0()*-0.19437);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7c50() {
   return (neuron0x8dc86d0()*0.488209);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7c78() {
   return (neuron0x8dc88d0()*-0.0416817);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7ca0() {
   return (neuron0x8dc8ad0()*-0.116649);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7cc8() {
   return (neuron0x8dc8ce8()*-0.111044);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7cf0() {
   return (neuron0x8dc8f00()*0.2033);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7d18() {
   return (neuron0x8dc9118()*0.271025);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7d40() {
   return (neuron0x8dc9330()*-0.0465828);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7f80() {
   return (neuron0x8dc76a0()*-0.155517);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7fa8() {
   return (neuron0x8dc7880()*-0.19162);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7fd0() {
   return (neuron0x8dc7aa8()*0.172789);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd7ff8() {
   return (neuron0x8dc7ca8()*0.454199);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8020() {
   return (neuron0x8dc7ea8()*-0.380637);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8048() {
   return (neuron0x8dc80d0()*-0.238011);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8070() {
   return (neuron0x8dc82d0()*-0.409333);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8098() {
   return (neuron0x8dc84d0()*0.0260216);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd80c0() {
   return (neuron0x8dc86d0()*-0.367944);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd80e8() {
   return (neuron0x8dc88d0()*0.254186);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8110() {
   return (neuron0x8dc8ad0()*0.274887);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8138() {
   return (neuron0x8dc8ce8()*-0.306931);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8160() {
   return (neuron0x8dc8f00()*-0.379854);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8188() {
   return (neuron0x8dc9118()*-0.395007);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd81b0() {
   return (neuron0x8dc9330()*0.0308525);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd83f0() {
   return (neuron0x8dc76a0()*-0.118874);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8418() {
   return (neuron0x8dc7880()*0.473993);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8440() {
   return (neuron0x8dc7aa8()*0.232919);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8468() {
   return (neuron0x8dc7ca8()*0.064688);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8490() {
   return (neuron0x8dc7ea8()*0.106001);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd84b8() {
   return (neuron0x8dc80d0()*-0.0179054);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd84e0() {
   return (neuron0x8dc82d0()*0.0181376);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8508() {
   return (neuron0x8dc84d0()*0.0396244);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8530() {
   return (neuron0x8dc86d0()*0.186663);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8558() {
   return (neuron0x8dc88d0()*-0.162171);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8580() {
   return (neuron0x8dc8ad0()*-0.0220596);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd85a8() {
   return (neuron0x8dc8ce8()*0.0281168);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd85d0() {
   return (neuron0x8dc8f00()*-0.217288);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd85f8() {
   return (neuron0x8dc9118()*-0.140562);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8620() {
   return (neuron0x8dc9330()*0.182486);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8860() {
   return (neuron0x8dc76a0()*0.188999);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8888() {
   return (neuron0x8dc7880()*0.00612822);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd88b0() {
   return (neuron0x8dc7aa8()*0.452278);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd88d8() {
   return (neuron0x8dc7ca8()*-0.0748857);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8900() {
   return (neuron0x8dc7ea8()*-0.201315);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8928() {
   return (neuron0x8dc80d0()*-0.134121);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8950() {
   return (neuron0x8dc82d0()*-0.382492);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8978() {
   return (neuron0x8dc84d0()*-0.377223);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd89a0() {
   return (neuron0x8dc86d0()*-0.0214145);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd89c8() {
   return (neuron0x8dc88d0()*-0.256866);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd89f0() {
   return (neuron0x8dc8ad0()*0.379835);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8a18() {
   return (neuron0x8dc8ce8()*0.399866);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8a40() {
   return (neuron0x8dc8f00()*-0.0883275);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8a68() {
   return (neuron0x8dc9118()*-0.258464);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8a90() {
   return (neuron0x8dc9330()*0.0035186);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8cd0() {
   return (neuron0x8dc76a0()*-0.181436);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8cf8() {
   return (neuron0x8dc7880()*-0.0935428);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8d20() {
   return (neuron0x8dc7aa8()*-0.10395);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8d48() {
   return (neuron0x8dc7ca8()*-0.127926);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8d70() {
   return (neuron0x8dc7ea8()*-0.103405);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8d98() {
   return (neuron0x8dc80d0()*0.117732);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8dc0() {
   return (neuron0x8dc82d0()*-0.0138977);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8de8() {
   return (neuron0x8dc84d0()*0.216549);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8e10() {
   return (neuron0x8dc86d0()*-0.297217);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8e38() {
   return (neuron0x8dc88d0()*0.24882);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8e60() {
   return (neuron0x8dc8ad0()*0.0313683);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8e88() {
   return (neuron0x8dc8ce8()*-0.0130294);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8eb0() {
   return (neuron0x8dc8f00()*-0.326995);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8ed8() {
   return (neuron0x8dc9118()*0.17778);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd8f00() {
   return (neuron0x8dc9330()*0.192258);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9140() {
   return (neuron0x8dc76a0()*-0.0672025);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9168() {
   return (neuron0x8dc7880()*0.197205);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9190() {
   return (neuron0x8dc7aa8()*-0.265765);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd91b8() {
   return (neuron0x8dc7ca8()*-0.452326);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd91e0() {
   return (neuron0x8dc7ea8()*0.318326);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9208() {
   return (neuron0x8dc80d0()*-0.279325);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9230() {
   return (neuron0x8dc82d0()*-0.285068);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9258() {
   return (neuron0x8dc84d0()*-0.392274);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9280() {
   return (neuron0x8dc86d0()*0.284106);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd92a8() {
   return (neuron0x8dc88d0()*-0.297962);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd92d0() {
   return (neuron0x8dc8ad0()*-0.348446);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd92f8() {
   return (neuron0x8dc8ce8()*-0.291357);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9320() {
   return (neuron0x8dc8f00()*0.486671);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9348() {
   return (neuron0x8dc9118()*0.0238534);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9370() {
   return (neuron0x8dc9330()*0.274511);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd95b0() {
   return (neuron0x8dc76a0()*0.292971);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd95d8() {
   return (neuron0x8dc7880()*-0.00649061);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9600() {
   return (neuron0x8dc7aa8()*-0.000485328);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9628() {
   return (neuron0x8dc7ca8()*0.36765);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9650() {
   return (neuron0x8dc7ea8()*-0.229412);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9678() {
   return (neuron0x8dc80d0()*0.176583);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd96a0() {
   return (neuron0x8dc82d0()*-0.127189);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd96c8() {
   return (neuron0x8dc84d0()*-0.305433);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd96f0() {
   return (neuron0x8dc86d0()*0.282895);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9718() {
   return (neuron0x8dc88d0()*0.192316);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9740() {
   return (neuron0x8dc8ad0()*0.296918);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9768() {
   return (neuron0x8dc8ce8()*-0.153419);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9790() {
   return (neuron0x8dc8f00()*-0.195859);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd97b8() {
   return (neuron0x8dc9118()*-0.170848);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd97e0() {
   return (neuron0x8dc9330()*-0.251522);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfa40() {
   return (neuron0x8dc76a0()*-0.336772);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfa68() {
   return (neuron0x8dc7880()*-0.403388);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfa90() {
   return (neuron0x8dc7aa8()*0.121744);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfab8() {
   return (neuron0x8dc7ca8()*-0.303775);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfae0() {
   return (neuron0x8dc7ea8()*-0.307102);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dcfb08() {
   return (neuron0x8dc80d0()*-0.0937957);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9c28() {
   return (neuron0x8dc82d0()*-0.0641127);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9c50() {
   return (neuron0x8dc84d0()*-0.114225);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9c78() {
   return (neuron0x8dc86d0()*0.074866);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9ca0() {
   return (neuron0x8dc88d0()*0.13512);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9cc8() {
   return (neuron0x8dc8ad0()*0.138719);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9cf0() {
   return (neuron0x8dc8ce8()*-0.0537701);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9d18() {
   return (neuron0x8dc8f00()*-0.242565);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9d40() {
   return (neuron0x8dc9118()*0.147959);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9d68() {
   return (neuron0x8dc9330()*-0.160318);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9f90() {
   return (neuron0x8dc76a0()*0.0723025);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9fb8() {
   return (neuron0x8dc7880()*0.425823);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dd9fe0() {
   return (neuron0x8dc7aa8()*-0.32325);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda008() {
   return (neuron0x8dc7ca8()*-0.209658);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda030() {
   return (neuron0x8dc7ea8()*-0.0261816);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda058() {
   return (neuron0x8dc80d0()*0.295543);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda080() {
   return (neuron0x8dc82d0()*-0.270369);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda0a8() {
   return (neuron0x8dc84d0()*-0.277517);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda0d0() {
   return (neuron0x8dc86d0()*0.0487813);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda0f8() {
   return (neuron0x8dc88d0()*-0.109244);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda120() {
   return (neuron0x8dc8ad0()*-0.156613);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda148() {
   return (neuron0x8dc8ce8()*0.283359);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda170() {
   return (neuron0x8dc8f00()*0.0356651);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda198() {
   return (neuron0x8dc9118()*-0.0110946);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda1c0() {
   return (neuron0x8dc9330()*0.0831327);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda400() {
   return (neuron0x8dc76a0()*0.0121618);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda428() {
   return (neuron0x8dc7880()*0.411934);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda450() {
   return (neuron0x8dc7aa8()*0.314388);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda478() {
   return (neuron0x8dc7ca8()*-0.413777);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda4a0() {
   return (neuron0x8dc7ea8()*0.0952553);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda4c8() {
   return (neuron0x8dc80d0()*-0.053528);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda4f0() {
   return (neuron0x8dc82d0()*0.144736);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda518() {
   return (neuron0x8dc84d0()*-0.452127);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda540() {
   return (neuron0x8dc86d0()*0.0221655);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda568() {
   return (neuron0x8dc88d0()*0.114741);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda590() {
   return (neuron0x8dc8ad0()*-0.15004);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda5b8() {
   return (neuron0x8dc8ce8()*0.149662);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda5e0() {
   return (neuron0x8dc8f00()*0.182532);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda608() {
   return (neuron0x8dc9118()*-0.119566);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda630() {
   return (neuron0x8dc9330()*-0.391183);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda870() {
   return (neuron0x8dc76a0()*0.341994);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda898() {
   return (neuron0x8dc7880()*-0.483959);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda8c0() {
   return (neuron0x8dc7aa8()*0.492186);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda8e8() {
   return (neuron0x8dc7ca8()*0.228918);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda910() {
   return (neuron0x8dc7ea8()*-0.281688);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda938() {
   return (neuron0x8dc80d0()*0.441858);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda960() {
   return (neuron0x8dc82d0()*-0.372673);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda988() {
   return (neuron0x8dc84d0()*-0.0403918);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda9b0() {
   return (neuron0x8dc86d0()*0.379778);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8dda9d8() {
   return (neuron0x8dc88d0()*-0.445112);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddaa00() {
   return (neuron0x8dc8ad0()*0.258167);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddaa28() {
   return (neuron0x8dc8ce8()*-0.383146);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddaa50() {
   return (neuron0x8dc8f00()*-0.170439);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddaa78() {
   return (neuron0x8dc9118()*-0.240165);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddaaa0() {
   return (neuron0x8dc9330()*0.358131);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddace0() {
   return (neuron0x8dc76a0()*-0.292232);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddad08() {
   return (neuron0x8dc7880()*-0.00584567);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddad30() {
   return (neuron0x8dc7aa8()*-0.0870967);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddad58() {
   return (neuron0x8dc7ca8()*0.262868);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddad80() {
   return (neuron0x8dc7ea8()*-0.00329066);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddada8() {
   return (neuron0x8dc80d0()*0.207623);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddadd0() {
   return (neuron0x8dc82d0()*0.326524);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddadf8() {
   return (neuron0x8dc84d0()*-0.225474);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddae20() {
   return (neuron0x8dc86d0()*-0.170318);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddae48() {
   return (neuron0x8dc88d0()*-0.152364);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddae70() {
   return (neuron0x8dc8ad0()*-0.379616);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddae98() {
   return (neuron0x8dc8ce8()*0.418172);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddaec0() {
   return (neuron0x8dc8f00()*0.123351);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddaee8() {
   return (neuron0x8dc9118()*0.166702);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddaf10() {
   return (neuron0x8dc9330()*-0.00557371);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb150() {
   return (neuron0x8dc76a0()*-0.430846);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb178() {
   return (neuron0x8dc7880()*0.294875);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb1a0() {
   return (neuron0x8dc7aa8()*0.0783915);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb1c8() {
   return (neuron0x8dc7ca8()*0.441749);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb1f0() {
   return (neuron0x8dc7ea8()*0.270431);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb218() {
   return (neuron0x8dc80d0()*-0.0852545);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb240() {
   return (neuron0x8dc82d0()*0.345598);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb268() {
   return (neuron0x8dc84d0()*-0.302751);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb290() {
   return (neuron0x8dc86d0()*0.0439822);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb2b8() {
   return (neuron0x8dc88d0()*-0.252305);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb2e0() {
   return (neuron0x8dc8ad0()*0.413937);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb308() {
   return (neuron0x8dc8ce8()*0.325719);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb330() {
   return (neuron0x8dc8f00()*-0.340322);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb358() {
   return (neuron0x8dc9118()*0.172396);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb380() {
   return (neuron0x8dc9330()*-0.314739);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb5c0() {
   return (neuron0x8dc76a0()*0.44538);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb5e8() {
   return (neuron0x8dc7880()*0.49573);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb610() {
   return (neuron0x8dc7aa8()*-0.377984);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb638() {
   return (neuron0x8dc7ca8()*0.320503);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb660() {
   return (neuron0x8dc7ea8()*-0.0369226);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb688() {
   return (neuron0x8dc80d0()*-0.00791318);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb6b0() {
   return (neuron0x8dc82d0()*0.00602688);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb6d8() {
   return (neuron0x8dc84d0()*-0.0673162);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb700() {
   return (neuron0x8dc86d0()*0.0207969);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb728() {
   return (neuron0x8dc88d0()*0.0589019);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb750() {
   return (neuron0x8dc8ad0()*-0.014622);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb778() {
   return (neuron0x8dc8ce8()*0.00703137);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb7a0() {
   return (neuron0x8dc8f00()*-0.352103);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb7c8() {
   return (neuron0x8dc9118()*0.061607);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddb7f0() {
   return (neuron0x8dc9330()*-0.0239991);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddba30() {
   return (neuron0x8dc76a0()*0.496445);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddba58() {
   return (neuron0x8dc7880()*-0.287154);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddba80() {
   return (neuron0x8dc7aa8()*-0.395861);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbaa8() {
   return (neuron0x8dc7ca8()*0.380505);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbad0() {
   return (neuron0x8dc7ea8()*0.228812);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbaf8() {
   return (neuron0x8dc80d0()*0.117075);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbb20() {
   return (neuron0x8dc82d0()*0.0691037);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbb48() {
   return (neuron0x8dc84d0()*-0.0159667);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbb70() {
   return (neuron0x8dc86d0()*-0.357527);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbb98() {
   return (neuron0x8dc88d0()*-0.087111);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbbc0() {
   return (neuron0x8dc8ad0()*-0.318988);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbbe8() {
   return (neuron0x8dc8ce8()*-0.239404);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbc10() {
   return (neuron0x8dc8f00()*-0.169419);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbc38() {
   return (neuron0x8dc9118()*0.173965);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbc60() {
   return (neuron0x8dc9330()*-0.104583);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbea0() {
   return (neuron0x8dc76a0()*0.238446);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbec8() {
   return (neuron0x8dc7880()*-0.303881);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbef0() {
   return (neuron0x8dc7aa8()*-0.154428);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbf18() {
   return (neuron0x8dc7ca8()*0.199871);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbf40() {
   return (neuron0x8dc7ea8()*0.243677);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbf68() {
   return (neuron0x8dc80d0()*-0.0423657);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbf90() {
   return (neuron0x8dc82d0()*0.107257);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbfb8() {
   return (neuron0x8dc84d0()*0.308613);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddbfe0() {
   return (neuron0x8dc86d0()*-0.140593);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc008() {
   return (neuron0x8dc88d0()*0.204699);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc030() {
   return (neuron0x8dc8ad0()*0.424055);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc058() {
   return (neuron0x8dc8ce8()*-0.452321);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc080() {
   return (neuron0x8dc8f00()*-0.222453);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc0a8() {
   return (neuron0x8dc9118()*0.395589);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc0d0() {
   return (neuron0x8dc9330()*-0.0683708);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc220() {
   return (neuron0x8dc9670()*0.0065122);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc248() {
   return (neuron0x8dc99e0()*0.00101708);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc270() {
   return (neuron0x8dc9f48()*0.000128404);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc298() {
   return (neuron0x8dca2d8()*0.00334634);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc2c0() {
   return (neuron0x8dca730()*0.00651461);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc2e8() {
   return (neuron0x8dcac00()*0.00078563);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc310() {
   return (neuron0x8dcb058()*0.00754615);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc338() {
   return (neuron0x8dcb4c8()*-0.0497529);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc360() {
   return (neuron0x8dcb938()*-0.0166353);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc388() {
   return (neuron0x8dca9f8()*0.0447238);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc3b0() {
   return (neuron0x8dcc228()*-0.0282026);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc3d8() {
   return (neuron0x8dcc680()*-0.0045265);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc400() {
   return (neuron0x8dccad8()*-0.00598434);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc428() {
   return (neuron0x8dccf48()*-0.0265278);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc450() {
   return (neuron0x8dce5a8()*-0.00230743);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc478() {
   return (neuron0x8dce948()*0.020027);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc528() {
   return (neuron0x8dcf598()*-0.00401102);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc550() {
   return (neuron0x8dcf918()*0.00709028);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc578() {
   return (neuron0x8dcbd30()*-0.00811378);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc5a0() {
   return (neuron0x8dd0440()*0.00642811);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc5c8() {
   return (neuron0x8dd0898()*0.00654907);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc5f0() {
   return (neuron0x8dd0cf0()*0.246473);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc618() {
   return (neuron0x8dd1148()*-0.00117086);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc640() {
   return (neuron0x8dd15a0()*0.00231497);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc668() {
   return (neuron0x8dd19f8()*-0.00963838);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc690() {
   return (neuron0x8dd1e50()*-0.0254933);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc6b8() {
   return (neuron0x8dd22a8()*-0.00627372);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc6e0() {
   return (neuron0x8dd2700()*-0.0121925);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc708() {
   return (neuron0x8dd2b58()*-0.00479218);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc730() {
   return (neuron0x8dd2fb0()*0.0199844);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc758() {
   return (neuron0x8dd3408()*0.00728599);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc780() {
   return (neuron0x8dd3860()*0.00632462);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc4a0() {
   return (neuron0x8dd4a00()*0.0227743);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc4c8() {
   return (neuron0x8dd4b28()*0.00625806);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc4f0() {
   return (neuron0x8dd4e08()*0.016766);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc8b0() {
   return (neuron0x8dcfd28()*0.0221697);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc8d8() {
   return (neuron0x8dd0180()*-0.00535215);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc900() {
   return (neuron0x8dd6328()*0.026435);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc928() {
   return (neuron0x8dd6780()*0.00238924);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc950() {
   return (neuron0x8dd6bd8()*-0.0137016);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc978() {
   return (neuron0x8dd7030()*-0.00328184);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc9a0() {
   return (neuron0x8dd7488()*0.00757335);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc9c8() {
   return (neuron0x8dd78f8()*0.0477437);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddc9f0() {
   return (neuron0x8dd7d68()*0.00196116);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddca18() {
   return (neuron0x8dd81d8()*0.339396);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddca40() {
   return (neuron0x8dd8648()*-0.00962233);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddca68() {
   return (neuron0x8dd8ab8()*0.174429);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddca90() {
   return (neuron0x8dd8f28()*-0.000934523);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcab8() {
   return (neuron0x8dd9398()*-0.0409596);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcae0() {
   return (neuron0x8dd9808()*0.115197);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcb08() {
   return (neuron0x8dd9d90()*0.0170915);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcb30() {
   return (neuron0x8dda1e8()*0.0998981);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcb58() {
   return (neuron0x8dda658()*-0.000455969);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcb80() {
   return (neuron0x8ddaac8()*-7.8701e-05);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcba8() {
   return (neuron0x8ddaf38()*0.00166554);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcbd0() {
   return (neuron0x8ddb3a8()*-0.337203);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcbf8() {
   return (neuron0x8ddb818()*0.00755727);
}

double NNParaElectronAngleCorrectionBCPDirTheta::synapse0x8ddcc20() {
   return (neuron0x8ddbc88()*0.00794261);
}

