#include "NNPerpElectronAngleCorrectionBCPDirTheta.h"
#include <cmath>

double NNPerpElectronAngleCorrectionBCPDirTheta::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10,double in11,double in12,double in13,double in14) {
   input0 = (in0 - 0.70338)/0.0918753;
   input1 = (in1 - -0.0158536)/0.256935;
   input2 = (in2 - -3.74358)/31.4802;
   input3 = (in3 - -3.51704)/56.0822;
   input4 = (in4 - 3068.51)/1543.52;
   input5 = (in5 - -0.00355158)/0.749607;
   input6 = (in6 - -0.0154733)/0.75028;
   input7 = (in7 - -4.12133)/32.3487;
   input8 = (in8 - -5.23583)/57.6425;
   input9 = (in9 - 1.38816)/0.32817;
   input10 = (in10 - 1.48199)/0.36367;
   input11 = (in11 - -0.0323618)/14.7341;
   input12 = (in12 - 0.193437)/1.21351;
   input13 = (in13 - 1.25754)/412.206;
   input14 = (in14 - 4.00453)/5.88751;
   switch(index) {
     case 0:
         return neuron0x8ddb388();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionBCPDirTheta::Value(int index, double* input) {
   input0 = (input[0] - 0.70338)/0.0918753;
   input1 = (input[1] - -0.0158536)/0.256935;
   input2 = (input[2] - -3.74358)/31.4802;
   input3 = (input[3] - -3.51704)/56.0822;
   input4 = (input[4] - 3068.51)/1543.52;
   input5 = (input[5] - -0.00355158)/0.749607;
   input6 = (input[6] - -0.0154733)/0.75028;
   input7 = (input[7] - -4.12133)/32.3487;
   input8 = (input[8] - -5.23583)/57.6425;
   input9 = (input[9] - 1.38816)/0.32817;
   input10 = (input[10] - 1.48199)/0.36367;
   input11 = (input[11] - -0.0323618)/14.7341;
   input12 = (input[12] - 0.193437)/1.21351;
   input13 = (input[13] - 1.25754)/412.206;
   input14 = (input[14] - 4.00453)/5.88751;
   switch(index) {
     case 0:
         return neuron0x8ddb388();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc6480() {
   return input0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc6660() {
   return input1;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc6888() {
   return input2;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc6a88() {
   return input3;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc6c88() {
   return input4;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc6eb0() {
   return input5;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc70b0() {
   return input6;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc72b0() {
   return input7;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc74b0() {
   return input8;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc76b0() {
   return input9;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc78b0() {
   return input10;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc7ac8() {
   return input11;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc7ce0() {
   return input12;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc7ef8() {
   return input13;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc8110() {
   return input14;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dc8450() {
   double input = -0.254613;
   input += synapse0x8dcd328();
   input += synapse0x8bf1560();
   input += synapse0x8bf15a8();
   input += synapse0x8bf1678();
   input += synapse0x8dc8608();
   input += synapse0x8dc8630();
   input += synapse0x8dc8658();
   input += synapse0x8dc8680();
   input += synapse0x8dc86a8();
   input += synapse0x8dc86d0();
   input += synapse0x8dc86f8();
   input += synapse0x8dc8720();
   input += synapse0x8dc8748();
   input += synapse0x8dc8770();
   input += synapse0x8dc8798();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc8450() {
   double input = input0x8dc8450();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dc87c0() {
   double input = 0.285048;
   input += synapse0x8dc8a48();
   input += synapse0x8dc8a70();
   input += synapse0x8dc8b20();
   input += synapse0x8dc8b48();
   input += synapse0x8dc8b70();
   input += synapse0x8dc8b98();
   input += synapse0x8dc8bc0();
   input += synapse0x8dc8be8();
   input += synapse0x8dc8c10();
   input += synapse0x8dc8c38();
   input += synapse0x8dc8c60();
   input += synapse0x8dc8c88();
   input += synapse0x8dc8cb0();
   input += synapse0x8dc8cd8();
   input += synapse0x8dc8d00();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc87c0() {
   double input = input0x8dc87c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dc8d28() {
   double input = -0.116469;
   input += synapse0x8dc8e98();
   input += synapse0x8dc8ec0();
   input += synapse0x8dc8ee8();
   input += synapse0x8dc8a98();
   input += synapse0x8dc8ac0();
   input += synapse0x8dc8ae8();
   input += synapse0x8dc8f10();
   input += synapse0x8dc8f38();
   input += synapse0x8dc8f60();
   input += synapse0x8dc8f88();
   input += synapse0x8dc8fb0();
   input += synapse0x8dc8fd8();
   input += synapse0x8dc9000();
   input += synapse0x8dc9028();
   input += synapse0x8dc9050();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc8d28() {
   double input = input0x8dc8d28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dc9078() {
   double input = -0.492434;
   input += synapse0x8dc9278();
   input += synapse0x8dc92a0();
   input += synapse0x8dc92c8();
   input += synapse0x8dc92f0();
   input += synapse0x8dc9318();
   input += synapse0x8dc9340();
   input += synapse0x8dc9368();
   input += synapse0x8dc9390();
   input += synapse0x8dc93b8();
   input += synapse0x8dc93e0();
   input += synapse0x8dc9408();
   input += synapse0x8dc9430();
   input += synapse0x8dc9458();
   input += synapse0x8dc9480();
   input += synapse0x8dc94a8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc9078() {
   double input = input0x8dc9078();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dc94d0() {
   double input = 0.412546;
   input += synapse0x8dc96d0();
   input += synapse0x8dc96f8();
   input += synapse0x8dc9720();
   input += synapse0x8dc9748();
   input += synapse0x8dc9770();
   input += synapse0x8bf0bb8();
   input += synapse0x8bf16a0();
   input += synapse0x8bf16c8();
   input += synapse0x8bf16f0();
   input += synapse0x8bf1718();
   input += synapse0x8dcd258();
   input += synapse0x8dcd280();
   input += synapse0x8dcd2a8();
   input += synapse0x8dcd2d0();
   input += synapse0x8dcd2f8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc94d0() {
   double input = input0x8dc94d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dc99a0() {
   double input = 0.349688;
   input += synapse0x8dc9bb8();
   input += synapse0x8dc9be0();
   input += synapse0x8dc9c08();
   input += synapse0x8dc9c30();
   input += synapse0x8dc9c58();
   input += synapse0x8dc9c80();
   input += synapse0x8dc9ca8();
   input += synapse0x8dc9cd0();
   input += synapse0x8dc9cf8();
   input += synapse0x8dc9d20();
   input += synapse0x8dc9d48();
   input += synapse0x8dc9d70();
   input += synapse0x8dc9d98();
   input += synapse0x8dc9dc0();
   input += synapse0x8dc9de8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc99a0() {
   double input = input0x8dc99a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dc9e10() {
   double input = 0.0398537;
   input += synapse0x8dca028();
   input += synapse0x8dca050();
   input += synapse0x8dca078();
   input += synapse0x8dca0a0();
   input += synapse0x8dca0c8();
   input += synapse0x8dca0f0();
   input += synapse0x8dca118();
   input += synapse0x8dca140();
   input += synapse0x8dca168();
   input += synapse0x8dca190();
   input += synapse0x8dca1b8();
   input += synapse0x8dca1e0();
   input += synapse0x8dca208();
   input += synapse0x8dca230();
   input += synapse0x8dca258();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc9e10() {
   double input = input0x8dc9e10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dca280() {
   double input = -0.0880622;
   input += synapse0x8dca498();
   input += synapse0x8dca4c0();
   input += synapse0x8dca4e8();
   input += synapse0x8dca510();
   input += synapse0x8dca538();
   input += synapse0x8dca560();
   input += synapse0x8dca588();
   input += synapse0x8dca5b0();
   input += synapse0x8dca5d8();
   input += synapse0x8dca600();
   input += synapse0x8dca628();
   input += synapse0x8dca650();
   input += synapse0x8dca678();
   input += synapse0x8dca6a0();
   input += synapse0x8dca6c8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dca280() {
   double input = input0x8dca280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dca6f0() {
   double input = -0.230855;
   input += synapse0x8dca908();
   input += synapse0x8dca930();
   input += synapse0x8dca958();
   input += synapse0x8dca980();
   input += synapse0x8dca9a8();
   input += synapse0x8dca9d0();
   input += synapse0x8dca9f8();
   input += synapse0x8dcaa20();
   input += synapse0x8dcaa48();
   input += synapse0x8bf1790();
   input += synapse0x8bf17b8();
   input += synapse0x8bf17e0();
   input += synapse0x8bf1808();
   input += synapse0x8bf1830();
   input += synapse0x8d5ab00();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dca6f0() {
   double input = input0x8dca6f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dc9798() {
   double input = -0.279266;
   input += synapse0x8d5ab28();
   input += synapse0x8d5ab50();
   input += synapse0x8d5ab78();
   input += synapse0x8d5aba0();
   input += synapse0x8d5abc8();
   input += synapse0x8dcae78();
   input += synapse0x8dcaea0();
   input += synapse0x8dcaec8();
   input += synapse0x8dcaef0();
   input += synapse0x8dcaf18();
   input += synapse0x8dcaf40();
   input += synapse0x8dcaf68();
   input += synapse0x8dcaf90();
   input += synapse0x8dcafb8();
   input += synapse0x8dcafe0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dc9798() {
   double input = input0x8dc9798();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcb008() {
   double input = -0.348379;
   input += synapse0x8dcb208();
   input += synapse0x8dcb230();
   input += synapse0x8dcb258();
   input += synapse0x8dcb280();
   input += synapse0x8dcb2a8();
   input += synapse0x8dcb2d0();
   input += synapse0x8dcb2f8();
   input += synapse0x8dcb320();
   input += synapse0x8dcb348();
   input += synapse0x8dcb370();
   input += synapse0x8dcb398();
   input += synapse0x8dcb3c0();
   input += synapse0x8dcb3e8();
   input += synapse0x8dcb410();
   input += synapse0x8dcb438();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcb008() {
   double input = input0x8dcb008();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcb460() {
   double input = -0.39664;
   input += synapse0x8dcb678();
   input += synapse0x8dcb6a0();
   input += synapse0x8dcb6c8();
   input += synapse0x8dcb6f0();
   input += synapse0x8dcb718();
   input += synapse0x8dcb740();
   input += synapse0x8dcb768();
   input += synapse0x8dcb790();
   input += synapse0x8dcb7b8();
   input += synapse0x8dcb7e0();
   input += synapse0x8dcb808();
   input += synapse0x8dcb830();
   input += synapse0x8dcb858();
   input += synapse0x8dcb880();
   input += synapse0x8dcb8a8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcb460() {
   double input = input0x8dcb460();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcb8d0() {
   double input = -0.265617;
   input += synapse0x8dcbae8();
   input += synapse0x8dcbb10();
   input += synapse0x8dcbb38();
   input += synapse0x8dcbb60();
   input += synapse0x8dcbb88();
   input += synapse0x8dcbbb0();
   input += synapse0x8dcbbd8();
   input += synapse0x8dcbc00();
   input += synapse0x8dcbc28();
   input += synapse0x8dcbc50();
   input += synapse0x8dcbc78();
   input += synapse0x8dcbca0();
   input += synapse0x8dcbcc8();
   input += synapse0x8dcbcf0();
   input += synapse0x8dcbd18();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcb8d0() {
   double input = input0x8dcb8d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcbd40() {
   double input = 0.0416404;
   input += synapse0x8dcbf58();
   input += synapse0x8dcbf80();
   input += synapse0x8dcbfa8();
   input += synapse0x8dcbfd0();
   input += synapse0x8dcbff8();
   input += synapse0x8dcc020();
   input += synapse0x8dcc048();
   input += synapse0x8dcc070();
   input += synapse0x8dcc098();
   input += synapse0x8dcc0c0();
   input += synapse0x8dcc0e8();
   input += synapse0x8dcc110();
   input += synapse0x8dcc138();
   input += synapse0x8dcc160();
   input += synapse0x8dcc188();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcbd40() {
   double input = input0x8dcbd40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcd3a0() {
   double input = -0.814231;
   input += synapse0x8dcd510();
   input += synapse0x8dcd538();
   input += synapse0x8dcd560();
   input += synapse0x8dcd588();
   input += synapse0x8dcd5b0();
   input += synapse0x8dcd5d8();
   input += synapse0x8dcd600();
   input += synapse0x8dcd628();
   input += synapse0x8dcd650();
   input += synapse0x8dcd678();
   input += synapse0x8dcd6a0();
   input += synapse0x8dcd6c8();
   input += synapse0x8dcd6f0();
   input += synapse0x8dcd718();
   input += synapse0x8dcd740();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcd3a0() {
   double input = input0x8dcd3a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcd768() {
   double input = 0.225939;
   input += synapse0x8dcd968();
   input += synapse0x8dcda18();
   input += synapse0x8dcdac8();
   input += synapse0x8dcdb78();
   input += synapse0x8dcdc28();
   input += synapse0x8dcdcd8();
   input += synapse0x8dcdd88();
   input += synapse0x8dcde38();
   input += synapse0x8dcdee8();
   input += synapse0x8dcdf98();
   input += synapse0x8dce048();
   input += synapse0x8dce0f8();
   input += synapse0x8dce1a8();
   input += synapse0x8dce258();
   input += synapse0x8dce308();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcd768() {
   double input = input0x8dcd768();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dce3b8() {
   double input = 0.0578494;
   input += synapse0x8dce4e0();
   input += synapse0x8dce508();
   input += synapse0x8dce530();
   input += synapse0x8dce558();
   input += synapse0x8dce580();
   input += synapse0x8dce5a8();
   input += synapse0x8dce5d0();
   input += synapse0x8dce5f8();
   input += synapse0x8dce620();
   input += synapse0x8dce648();
   input += synapse0x8dce670();
   input += synapse0x8dce698();
   input += synapse0x8dce6c0();
   input += synapse0x8dce6e8();
   input += synapse0x8dce710();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dce3b8() {
   double input = input0x8dce3b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dce738() {
   double input = 0.340124;
   input += synapse0x8dc89c0();
   input += synapse0x8dc89e8();
   input += synapse0x8dc8a10();
   input += synapse0x8dc7620();
   input += synapse0x8dc7420();
   input += synapse0x8dc7220();
   input += synapse0x8dc7020();
   input += synapse0x8dc6e20();
   input += synapse0x8dc6bf8();
   input += synapse0x8dc69f8();
   input += synapse0x8dc67f8();
   input += synapse0x8dc65d0();
   input += synapse0x8dcaa70();
   input += synapse0x8dcaa98();
   input += synapse0x8dcaac0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dce738() {
   double input = input0x8dce738();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcaae8() {
   double input = -0.0511702;
   input += synapse0x8dcad00();
   input += synapse0x8dcad28();
   input += synapse0x8dcad50();
   input += synapse0x8dcad78();
   input += synapse0x8dcada0();
   input += synapse0x8dcadc8();
   input += synapse0x8dcadf0();
   input += synapse0x8dcae18();
   input += synapse0x8dcae40();
   input += synapse0x8dcf170();
   input += synapse0x8dcf198();
   input += synapse0x8dcf1c0();
   input += synapse0x8dcf1e8();
   input += synapse0x8dcf210();
   input += synapse0x8dcf238();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcaae8() {
   double input = input0x8dcaae8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcf260() {
   double input = 0.410224;
   input += synapse0x8dcf460();
   input += synapse0x8dcf488();
   input += synapse0x8dcf4b0();
   input += synapse0x8dcf4d8();
   input += synapse0x8dcf500();
   input += synapse0x8dcf528();
   input += synapse0x8dcf550();
   input += synapse0x8dcf578();
   input += synapse0x8dcf5a0();
   input += synapse0x8dcf5c8();
   input += synapse0x8dcf5f0();
   input += synapse0x8dcf618();
   input += synapse0x8dcf640();
   input += synapse0x8dcf668();
   input += synapse0x8dcf690();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcf260() {
   double input = input0x8dcf260();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcf6b8() {
   double input = -0.0629776;
   input += synapse0x8dcf8b8();
   input += synapse0x8dcf8e0();
   input += synapse0x8dcf908();
   input += synapse0x8dcf930();
   input += synapse0x8dcf958();
   input += synapse0x8dcf980();
   input += synapse0x8dcf9a8();
   input += synapse0x8dcf9d0();
   input += synapse0x8dcf9f8();
   input += synapse0x8dcfa20();
   input += synapse0x8dcfa48();
   input += synapse0x8dcfa70();
   input += synapse0x8dcfa98();
   input += synapse0x8dcfac0();
   input += synapse0x8dcfae8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcf6b8() {
   double input = input0x8dcf6b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcfb10() {
   double input = -0.261844;
   input += synapse0x8dcfd10();
   input += synapse0x8dcfd38();
   input += synapse0x8dcfd60();
   input += synapse0x8dcfd88();
   input += synapse0x8dcfdb0();
   input += synapse0x8dcfdd8();
   input += synapse0x8dcfe00();
   input += synapse0x8dcfe28();
   input += synapse0x8dcfe50();
   input += synapse0x8dcfe78();
   input += synapse0x8dcfea0();
   input += synapse0x8dcfec8();
   input += synapse0x8dcfef0();
   input += synapse0x8dcff18();
   input += synapse0x8dcff40();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcfb10() {
   double input = input0x8dcfb10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcff68() {
   double input = 0.230185;
   input += synapse0x8dd0168();
   input += synapse0x8dd0190();
   input += synapse0x8dd01b8();
   input += synapse0x8dd01e0();
   input += synapse0x8dd0208();
   input += synapse0x8dd0230();
   input += synapse0x8dd0258();
   input += synapse0x8dd0280();
   input += synapse0x8dd02a8();
   input += synapse0x8dd02d0();
   input += synapse0x8dd02f8();
   input += synapse0x8dd0320();
   input += synapse0x8dd0348();
   input += synapse0x8dd0370();
   input += synapse0x8dd0398();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcff68() {
   double input = input0x8dcff68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd03c0() {
   double input = -0.0665168;
   input += synapse0x8dd05c0();
   input += synapse0x8dd05e8();
   input += synapse0x8dd0610();
   input += synapse0x8dd0638();
   input += synapse0x8dd0660();
   input += synapse0x8dd0688();
   input += synapse0x8dd06b0();
   input += synapse0x8dd06d8();
   input += synapse0x8dd0700();
   input += synapse0x8dd0728();
   input += synapse0x8dd0750();
   input += synapse0x8dd0778();
   input += synapse0x8dd07a0();
   input += synapse0x8dd07c8();
   input += synapse0x8dd07f0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd03c0() {
   double input = input0x8dd03c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd0818() {
   double input = 0.199839;
   input += synapse0x8dd0a18();
   input += synapse0x8dd0a40();
   input += synapse0x8dd0a68();
   input += synapse0x8dd0a90();
   input += synapse0x8dd0ab8();
   input += synapse0x8dd0ae0();
   input += synapse0x8dd0b08();
   input += synapse0x8dd0b30();
   input += synapse0x8dd0b58();
   input += synapse0x8dd0b80();
   input += synapse0x8dd0ba8();
   input += synapse0x8dd0bd0();
   input += synapse0x8dd0bf8();
   input += synapse0x8dd0c20();
   input += synapse0x8dd0c48();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd0818() {
   double input = input0x8dd0818();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd0c70() {
   double input = 0.0899299;
   input += synapse0x8dd0e70();
   input += synapse0x8dd0e98();
   input += synapse0x8dd0ec0();
   input += synapse0x8dd0ee8();
   input += synapse0x8dd0f10();
   input += synapse0x8dd0f38();
   input += synapse0x8dd0f60();
   input += synapse0x8dd0f88();
   input += synapse0x8dd0fb0();
   input += synapse0x8dd0fd8();
   input += synapse0x8dd1000();
   input += synapse0x8dd1028();
   input += synapse0x8dd1050();
   input += synapse0x8dd1078();
   input += synapse0x8dd10a0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd0c70() {
   double input = input0x8dd0c70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd10c8() {
   double input = -0.284269;
   input += synapse0x8dd12c8();
   input += synapse0x8dd12f0();
   input += synapse0x8dd1318();
   input += synapse0x8dd1340();
   input += synapse0x8dd1368();
   input += synapse0x8dd1390();
   input += synapse0x8dd13b8();
   input += synapse0x8dd13e0();
   input += synapse0x8dd1408();
   input += synapse0x8dd1430();
   input += synapse0x8dd1458();
   input += synapse0x8dd1480();
   input += synapse0x8dd14a8();
   input += synapse0x8dd14d0();
   input += synapse0x8dd14f8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd10c8() {
   double input = input0x8dd10c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd1520() {
   double input = -0.065197;
   input += synapse0x8dd1720();
   input += synapse0x8dd1748();
   input += synapse0x8dd1770();
   input += synapse0x8dd1798();
   input += synapse0x8dd17c0();
   input += synapse0x8dd17e8();
   input += synapse0x8dd1810();
   input += synapse0x8dd1838();
   input += synapse0x8dd1860();
   input += synapse0x8dd1888();
   input += synapse0x8dd18b0();
   input += synapse0x8dd18d8();
   input += synapse0x8dd1900();
   input += synapse0x8dd1928();
   input += synapse0x8dd1950();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd1520() {
   double input = input0x8dd1520();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd1978() {
   double input = -0.316252;
   input += synapse0x8dd1b78();
   input += synapse0x8dd1ba0();
   input += synapse0x8dd1bc8();
   input += synapse0x8dd1bf0();
   input += synapse0x8dd1c18();
   input += synapse0x8dd1c40();
   input += synapse0x8dd1c68();
   input += synapse0x8dd1c90();
   input += synapse0x8dd1cb8();
   input += synapse0x8dd1ce0();
   input += synapse0x8dd1d08();
   input += synapse0x8dd1d30();
   input += synapse0x8dd1d58();
   input += synapse0x8dd1d80();
   input += synapse0x8dd1da8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd1978() {
   double input = input0x8dd1978();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd1dd0() {
   double input = 0.442815;
   input += synapse0x8dd1fd0();
   input += synapse0x8dd1ff8();
   input += synapse0x8dd2020();
   input += synapse0x8dd2048();
   input += synapse0x8dd2070();
   input += synapse0x8dd2098();
   input += synapse0x8dd20c0();
   input += synapse0x8dd20e8();
   input += synapse0x8dd2110();
   input += synapse0x8dd2138();
   input += synapse0x8dd2160();
   input += synapse0x8dd2188();
   input += synapse0x8dd21b0();
   input += synapse0x8dd21d8();
   input += synapse0x8dd2200();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd1dd0() {
   double input = input0x8dd1dd0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd2228() {
   double input = -0.0976239;
   input += synapse0x8dd2428();
   input += synapse0x8dd2450();
   input += synapse0x8dd2478();
   input += synapse0x8dd24a0();
   input += synapse0x8dd24c8();
   input += synapse0x8dd24f0();
   input += synapse0x8dd2518();
   input += synapse0x8dd2540();
   input += synapse0x8dd2568();
   input += synapse0x8dd2590();
   input += synapse0x8dd25b8();
   input += synapse0x8dd25e0();
   input += synapse0x8dd2608();
   input += synapse0x8dd2630();
   input += synapse0x8dd2658();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd2228() {
   double input = input0x8dd2228();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd2680() {
   double input = 0.0368511;
   input += synapse0x8dd2880();
   input += synapse0x8dcd990();
   input += synapse0x8dcd9b8();
   input += synapse0x8dcd9e0();
   input += synapse0x8dcda40();
   input += synapse0x8dcda68();
   input += synapse0x8dcda90();
   input += synapse0x8dcdaf0();
   input += synapse0x8dcdb18();
   input += synapse0x8dcdb40();
   input += synapse0x8dcdba0();
   input += synapse0x8dcdbc8();
   input += synapse0x8dcdbf0();
   input += synapse0x8dcdc50();
   input += synapse0x8dcdc78();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd2680() {
   double input = input0x8dd2680();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd3820() {
   double input = 0.253522;
   input += synapse0x8dcdea8();
   input += synapse0x8dcdca0();
   input += synapse0x8dcdd48();
   input += synapse0x8dcddf8();
   input += synapse0x8dcdf10();
   input += synapse0x8dcdf38();
   input += synapse0x8dcdf60();
   input += synapse0x8dcdfc0();
   input += synapse0x8dcdfe8();
   input += synapse0x8dce010();
   input += synapse0x8dce070();
   input += synapse0x8dce098();
   input += synapse0x8dce0c0();
   input += synapse0x8dce120();
   input += synapse0x8dce148();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd3820() {
   double input = input0x8dd3820();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd3948() {
   double input = 0.513206;
   input += synapse0x8dce378();
   input += synapse0x8dce170();
   input += synapse0x8dce218();
   input += synapse0x8dce2c8();
   input += synapse0x8dd3a70();
   input += synapse0x8dd3a98();
   input += synapse0x8dd3ac0();
   input += synapse0x8dd3ae8();
   input += synapse0x8dd3b10();
   input += synapse0x8dd3b38();
   input += synapse0x8dd3b60();
   input += synapse0x8dd3b88();
   input += synapse0x8dd3bb0();
   input += synapse0x8dd3bd8();
   input += synapse0x8dd3c00();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd3948() {
   double input = input0x8dd3948();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd3c28() {
   double input = -0.439359;
   input += synapse0x8dd3e28();
   input += synapse0x8dd3e50();
   input += synapse0x8dd3e78();
   input += synapse0x8dce968();
   input += synapse0x8dce990();
   input += synapse0x8dce9b8();
   input += synapse0x8dce9e0();
   input += synapse0x8dcea08();
   input += synapse0x8dcea30();
   input += synapse0x8dcea58();
   input += synapse0x8dcea80();
   input += synapse0x8dceaa8();
   input += synapse0x8dcead0();
   input += synapse0x8dceaf8();
   input += synapse0x8dceb20();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd3c28() {
   double input = input0x8dd3c28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dceb48() {
   double input = -0.119631;
   input += synapse0x8dced48();
   input += synapse0x8dced70();
   input += synapse0x8dced98();
   input += synapse0x8dcedc0();
   input += synapse0x8dcede8();
   input += synapse0x8dcee10();
   input += synapse0x8dcee38();
   input += synapse0x8dcee60();
   input += synapse0x8dcee88();
   input += synapse0x8dceeb0();
   input += synapse0x8dceed8();
   input += synapse0x8dcef00();
   input += synapse0x8dcef28();
   input += synapse0x8dcef50();
   input += synapse0x8dcef78();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dceb48() {
   double input = input0x8dceb48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dcefa0() {
   double input = -0.118033;
   input += synapse0x8dd4ef0();
   input += synapse0x8dd4f18();
   input += synapse0x8dd4f40();
   input += synapse0x8dd4f68();
   input += synapse0x8dd4f90();
   input += synapse0x8dd4fb8();
   input += synapse0x8dd4fe0();
   input += synapse0x8dd5008();
   input += synapse0x8dd5030();
   input += synapse0x8dd5058();
   input += synapse0x8dd5080();
   input += synapse0x8dd50a8();
   input += synapse0x8dd50d0();
   input += synapse0x8dd50f8();
   input += synapse0x8dd5120();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dcefa0() {
   double input = input0x8dcefa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd5148() {
   double input = 0.31982;
   input += synapse0x8dd5348();
   input += synapse0x8dd5370();
   input += synapse0x8dd5398();
   input += synapse0x8dd53c0();
   input += synapse0x8dd53e8();
   input += synapse0x8dd5410();
   input += synapse0x8dd5438();
   input += synapse0x8dd5460();
   input += synapse0x8dd5488();
   input += synapse0x8dd54b0();
   input += synapse0x8dd54d8();
   input += synapse0x8dd5500();
   input += synapse0x8dd5528();
   input += synapse0x8dd5550();
   input += synapse0x8dd5578();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd5148() {
   double input = input0x8dd5148();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd55a0() {
   double input = -0.082567;
   input += synapse0x8dd57a0();
   input += synapse0x8dd57c8();
   input += synapse0x8dd57f0();
   input += synapse0x8dd5818();
   input += synapse0x8dd5840();
   input += synapse0x8dd5868();
   input += synapse0x8dd5890();
   input += synapse0x8dd58b8();
   input += synapse0x8dd58e0();
   input += synapse0x8dd5908();
   input += synapse0x8dd5930();
   input += synapse0x8dd5958();
   input += synapse0x8dd5980();
   input += synapse0x8dd59a8();
   input += synapse0x8dd59d0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd55a0() {
   double input = input0x8dd55a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd59f8() {
   double input = -0.403021;
   input += synapse0x8dd5bf8();
   input += synapse0x8dd5c20();
   input += synapse0x8dd5c48();
   input += synapse0x8dd5c70();
   input += synapse0x8dd5c98();
   input += synapse0x8dd5cc0();
   input += synapse0x8dd5ce8();
   input += synapse0x8dd5d10();
   input += synapse0x8dd5d38();
   input += synapse0x8dd5d60();
   input += synapse0x8dd5d88();
   input += synapse0x8dd5db0();
   input += synapse0x8dd5dd8();
   input += synapse0x8dd5e00();
   input += synapse0x8dd5e28();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd59f8() {
   double input = input0x8dd59f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd5e50() {
   double input = -0.280219;
   input += synapse0x8dd6050();
   input += synapse0x8dd6078();
   input += synapse0x8dd60a0();
   input += synapse0x8dd60c8();
   input += synapse0x8dd60f0();
   input += synapse0x8dd6118();
   input += synapse0x8dd6140();
   input += synapse0x8dd6168();
   input += synapse0x8dd6190();
   input += synapse0x8dd61b8();
   input += synapse0x8dd61e0();
   input += synapse0x8dd6208();
   input += synapse0x8dd6230();
   input += synapse0x8dd6258();
   input += synapse0x8dd6280();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd5e50() {
   double input = input0x8dd5e50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd62a8() {
   double input = -0.243532;
   input += synapse0x8dd64c0();
   input += synapse0x8dd64e8();
   input += synapse0x8dd6510();
   input += synapse0x8dd6538();
   input += synapse0x8dd6560();
   input += synapse0x8dd6588();
   input += synapse0x8dd65b0();
   input += synapse0x8dd65d8();
   input += synapse0x8dd6600();
   input += synapse0x8dd6628();
   input += synapse0x8dd6650();
   input += synapse0x8dd6678();
   input += synapse0x8dd66a0();
   input += synapse0x8dd66c8();
   input += synapse0x8dd66f0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd62a8() {
   double input = input0x8dd62a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd6718() {
   double input = 0.425186;
   input += synapse0x8dd6930();
   input += synapse0x8dd6958();
   input += synapse0x8dd6980();
   input += synapse0x8dd69a8();
   input += synapse0x8dd69d0();
   input += synapse0x8dd69f8();
   input += synapse0x8dd6a20();
   input += synapse0x8dd6a48();
   input += synapse0x8dd6a70();
   input += synapse0x8dd6a98();
   input += synapse0x8dd6ac0();
   input += synapse0x8dd6ae8();
   input += synapse0x8dd6b10();
   input += synapse0x8dd6b38();
   input += synapse0x8dd6b60();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd6718() {
   double input = input0x8dd6718();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd6b88() {
   double input = -0.32508;
   input += synapse0x8dd6da0();
   input += synapse0x8dd6dc8();
   input += synapse0x8dd6df0();
   input += synapse0x8dd6e18();
   input += synapse0x8dd6e40();
   input += synapse0x8dd6e68();
   input += synapse0x8dd6e90();
   input += synapse0x8dd6eb8();
   input += synapse0x8dd6ee0();
   input += synapse0x8dd6f08();
   input += synapse0x8dd6f30();
   input += synapse0x8dd6f58();
   input += synapse0x8dd6f80();
   input += synapse0x8dd6fa8();
   input += synapse0x8dd6fd0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd6b88() {
   double input = input0x8dd6b88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd6ff8() {
   double input = 0.427629;
   input += synapse0x8dd7210();
   input += synapse0x8dd7238();
   input += synapse0x8dd7260();
   input += synapse0x8dd7288();
   input += synapse0x8dd72b0();
   input += synapse0x8dd72d8();
   input += synapse0x8dd7300();
   input += synapse0x8dd7328();
   input += synapse0x8dd7350();
   input += synapse0x8dd7378();
   input += synapse0x8dd73a0();
   input += synapse0x8dd73c8();
   input += synapse0x8dd73f0();
   input += synapse0x8dd7418();
   input += synapse0x8dd7440();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd6ff8() {
   double input = input0x8dd6ff8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd7468() {
   double input = 0.0905878;
   input += synapse0x8dd7680();
   input += synapse0x8dd76a8();
   input += synapse0x8dd76d0();
   input += synapse0x8dd76f8();
   input += synapse0x8dd7720();
   input += synapse0x8dd7748();
   input += synapse0x8dd7770();
   input += synapse0x8dd7798();
   input += synapse0x8dd77c0();
   input += synapse0x8dd77e8();
   input += synapse0x8dd7810();
   input += synapse0x8dd7838();
   input += synapse0x8dd7860();
   input += synapse0x8dd7888();
   input += synapse0x8dd78b0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd7468() {
   double input = input0x8dd7468();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd78d8() {
   double input = -0.756291;
   input += synapse0x8dd7af0();
   input += synapse0x8dd7b18();
   input += synapse0x8dd7b40();
   input += synapse0x8dd7b68();
   input += synapse0x8dd7b90();
   input += synapse0x8dd7bb8();
   input += synapse0x8dd7be0();
   input += synapse0x8dd7c08();
   input += synapse0x8dd7c30();
   input += synapse0x8dd7c58();
   input += synapse0x8dd7c80();
   input += synapse0x8dd7ca8();
   input += synapse0x8dd7cd0();
   input += synapse0x8dd7cf8();
   input += synapse0x8dd7d20();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd78d8() {
   double input = input0x8dd78d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd7d48() {
   double input = 0.407718;
   input += synapse0x8dd7f60();
   input += synapse0x8dd7f88();
   input += synapse0x8dd7fb0();
   input += synapse0x8dd7fd8();
   input += synapse0x8dd8000();
   input += synapse0x8dd8028();
   input += synapse0x8dd8050();
   input += synapse0x8dd8078();
   input += synapse0x8dd80a0();
   input += synapse0x8dd80c8();
   input += synapse0x8dd80f0();
   input += synapse0x8dd8118();
   input += synapse0x8dd8140();
   input += synapse0x8dd8168();
   input += synapse0x8dd8190();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd7d48() {
   double input = input0x8dd7d48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd81b8() {
   double input = 0.10642;
   input += synapse0x8dd83d0();
   input += synapse0x8dd83f8();
   input += synapse0x8dd8420();
   input += synapse0x8dd8448();
   input += synapse0x8dd8470();
   input += synapse0x8dd8498();
   input += synapse0x8dd84c0();
   input += synapse0x8dd84e8();
   input += synapse0x8dd8510();
   input += synapse0x8dd8538();
   input += synapse0x8dd8560();
   input += synapse0x8dd8588();
   input += synapse0x8dd85b0();
   input += synapse0x8dd85d8();
   input += synapse0x8dd8600();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd81b8() {
   double input = input0x8dd81b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd8628() {
   double input = -0.512035;
   input += synapse0x8dce860();
   input += synapse0x8dce888();
   input += synapse0x8dce8b0();
   input += synapse0x8dce8d8();
   input += synapse0x8dce900();
   input += synapse0x8dce928();
   input += synapse0x8dd8a48();
   input += synapse0x8dd8a70();
   input += synapse0x8dd8a98();
   input += synapse0x8dd8ac0();
   input += synapse0x8dd8ae8();
   input += synapse0x8dd8b10();
   input += synapse0x8dd8b38();
   input += synapse0x8dd8b60();
   input += synapse0x8dd8b88();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd8628() {
   double input = input0x8dd8628();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd8bb0() {
   double input = -0.185263;
   input += synapse0x8dd8db0();
   input += synapse0x8dd8dd8();
   input += synapse0x8dd8e00();
   input += synapse0x8dd8e28();
   input += synapse0x8dd8e50();
   input += synapse0x8dd8e78();
   input += synapse0x8dd8ea0();
   input += synapse0x8dd8ec8();
   input += synapse0x8dd8ef0();
   input += synapse0x8dd8f18();
   input += synapse0x8dd8f40();
   input += synapse0x8dd8f68();
   input += synapse0x8dd8f90();
   input += synapse0x8dd8fb8();
   input += synapse0x8dd8fe0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd8bb0() {
   double input = input0x8dd8bb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd9008() {
   double input = -0.0997095;
   input += synapse0x8dd9220();
   input += synapse0x8dd9248();
   input += synapse0x8dd9270();
   input += synapse0x8dd9298();
   input += synapse0x8dd92c0();
   input += synapse0x8dd92e8();
   input += synapse0x8dd9310();
   input += synapse0x8dd9338();
   input += synapse0x8dd9360();
   input += synapse0x8dd9388();
   input += synapse0x8dd93b0();
   input += synapse0x8dd93d8();
   input += synapse0x8dd9400();
   input += synapse0x8dd9428();
   input += synapse0x8dd9450();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd9008() {
   double input = input0x8dd9008();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd9478() {
   double input = 0.121371;
   input += synapse0x8dd9690();
   input += synapse0x8dd96b8();
   input += synapse0x8dd96e0();
   input += synapse0x8dd9708();
   input += synapse0x8dd9730();
   input += synapse0x8dd9758();
   input += synapse0x8dd9780();
   input += synapse0x8dd97a8();
   input += synapse0x8dd97d0();
   input += synapse0x8dd97f8();
   input += synapse0x8dd9820();
   input += synapse0x8dd9848();
   input += synapse0x8dd9870();
   input += synapse0x8dd9898();
   input += synapse0x8dd98c0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd9478() {
   double input = input0x8dd9478();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd98e8() {
   double input = 0.407502;
   input += synapse0x8dd9b00();
   input += synapse0x8dd9b28();
   input += synapse0x8dd9b50();
   input += synapse0x8dd9b78();
   input += synapse0x8dd9ba0();
   input += synapse0x8dd9bc8();
   input += synapse0x8dd9bf0();
   input += synapse0x8dd9c18();
   input += synapse0x8dd9c40();
   input += synapse0x8dd9c68();
   input += synapse0x8dd9c90();
   input += synapse0x8dd9cb8();
   input += synapse0x8dd9ce0();
   input += synapse0x8dd9d08();
   input += synapse0x8dd9d30();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd98e8() {
   double input = input0x8dd98e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dd9d58() {
   double input = -0.42113;
   input += synapse0x8dd9f70();
   input += synapse0x8dd9f98();
   input += synapse0x8dd9fc0();
   input += synapse0x8dd9fe8();
   input += synapse0x8dda010();
   input += synapse0x8dda038();
   input += synapse0x8dda060();
   input += synapse0x8dda088();
   input += synapse0x8dda0b0();
   input += synapse0x8dda0d8();
   input += synapse0x8dda100();
   input += synapse0x8dda128();
   input += synapse0x8dda150();
   input += synapse0x8dda178();
   input += synapse0x8dda1a0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dd9d58() {
   double input = input0x8dd9d58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dda1c8() {
   double input = -0.469167;
   input += synapse0x8dda3e0();
   input += synapse0x8dda408();
   input += synapse0x8dda430();
   input += synapse0x8dda458();
   input += synapse0x8dda480();
   input += synapse0x8dda4a8();
   input += synapse0x8dda4d0();
   input += synapse0x8dda4f8();
   input += synapse0x8dda520();
   input += synapse0x8dda548();
   input += synapse0x8dda570();
   input += synapse0x8dda598();
   input += synapse0x8dda5c0();
   input += synapse0x8dda5e8();
   input += synapse0x8dda610();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dda1c8() {
   double input = input0x8dda1c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8dda638() {
   double input = 0.0512311;
   input += synapse0x8dda850();
   input += synapse0x8dda878();
   input += synapse0x8dda8a0();
   input += synapse0x8dda8c8();
   input += synapse0x8dda8f0();
   input += synapse0x8dda918();
   input += synapse0x8dda940();
   input += synapse0x8dda968();
   input += synapse0x8dda990();
   input += synapse0x8dda9b8();
   input += synapse0x8dda9e0();
   input += synapse0x8ddaa08();
   input += synapse0x8ddaa30();
   input += synapse0x8ddaa58();
   input += synapse0x8ddaa80();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8dda638() {
   double input = input0x8dda638();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8ddaaa8() {
   double input = 0.114497;
   input += synapse0x8ddacc0();
   input += synapse0x8ddace8();
   input += synapse0x8ddad10();
   input += synapse0x8ddad38();
   input += synapse0x8ddad60();
   input += synapse0x8ddad88();
   input += synapse0x8ddadb0();
   input += synapse0x8ddadd8();
   input += synapse0x8ddae00();
   input += synapse0x8ddae28();
   input += synapse0x8ddae50();
   input += synapse0x8ddae78();
   input += synapse0x8ddaea0();
   input += synapse0x8ddaec8();
   input += synapse0x8ddaef0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8ddaaa8() {
   double input = input0x8ddaaa8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8ddaf18() {
   double input = 0.155525;
   input += synapse0x8ddb130();
   input += synapse0x8ddb158();
   input += synapse0x8ddb180();
   input += synapse0x8ddb1a8();
   input += synapse0x8ddb1d0();
   input += synapse0x8ddb1f8();
   input += synapse0x8ddb220();
   input += synapse0x8ddb248();
   input += synapse0x8ddb270();
   input += synapse0x8ddb298();
   input += synapse0x8ddb2c0();
   input += synapse0x8ddb2e8();
   input += synapse0x8ddb310();
   input += synapse0x8ddb338();
   input += synapse0x8ddb360();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8ddaf18() {
   double input = input0x8ddaf18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::input0x8ddb388() {
   double input = -0.240779;
   input += synapse0x8ddb4b0();
   input += synapse0x8ddb4d8();
   input += synapse0x8ddb500();
   input += synapse0x8ddb528();
   input += synapse0x8ddb550();
   input += synapse0x8ddb578();
   input += synapse0x8ddb5a0();
   input += synapse0x8ddb5c8();
   input += synapse0x8ddb5f0();
   input += synapse0x8ddb618();
   input += synapse0x8ddb640();
   input += synapse0x8ddb668();
   input += synapse0x8ddb690();
   input += synapse0x8ddb6b8();
   input += synapse0x8ddb6e0();
   input += synapse0x8ddb708();
   input += synapse0x8ddb7b8();
   input += synapse0x8ddb7e0();
   input += synapse0x8ddb808();
   input += synapse0x8ddb830();
   input += synapse0x8ddb858();
   input += synapse0x8ddb880();
   input += synapse0x8ddb8a8();
   input += synapse0x8ddb8d0();
   input += synapse0x8ddb8f8();
   input += synapse0x8ddb920();
   input += synapse0x8ddb948();
   input += synapse0x8ddb970();
   input += synapse0x8ddb998();
   input += synapse0x8ddb9c0();
   input += synapse0x8ddb9e8();
   input += synapse0x8ddba10();
   input += synapse0x8ddb730();
   input += synapse0x8ddb758();
   input += synapse0x8ddb780();
   input += synapse0x8ddbb40();
   input += synapse0x8ddbb68();
   input += synapse0x8ddbb90();
   input += synapse0x8ddbbb8();
   input += synapse0x8ddbbe0();
   input += synapse0x8ddbc08();
   input += synapse0x8ddbc30();
   input += synapse0x8ddbc58();
   input += synapse0x8ddbc80();
   input += synapse0x8ddbca8();
   input += synapse0x8ddbcd0();
   input += synapse0x8ddbcf8();
   input += synapse0x8ddbd20();
   input += synapse0x8ddbd48();
   input += synapse0x8ddbd70();
   input += synapse0x8ddbd98();
   input += synapse0x8ddbdc0();
   input += synapse0x8ddbde8();
   input += synapse0x8ddbe10();
   input += synapse0x8ddbe38();
   input += synapse0x8ddbe60();
   input += synapse0x8ddbe88();
   input += synapse0x8ddbeb0();
   input += synapse0x8ddbed8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::neuron0x8ddb388() {
   double input = input0x8ddb388();
   return (input * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd328() {
   return (neuron0x8dc6480()*-0.489386);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf1560() {
   return (neuron0x8dc6660()*-0.162368);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf15a8() {
   return (neuron0x8dc6888()*-0.208964);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf1678() {
   return (neuron0x8dc6a88()*0.0067558);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8608() {
   return (neuron0x8dc6c88()*-0.298695);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8630() {
   return (neuron0x8dc6eb0()*-0.26521);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8658() {
   return (neuron0x8dc70b0()*0.00465016);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8680() {
   return (neuron0x8dc72b0()*0.499276);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc86a8() {
   return (neuron0x8dc74b0()*-0.343436);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc86d0() {
   return (neuron0x8dc76b0()*-0.182899);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc86f8() {
   return (neuron0x8dc78b0()*-0.284497);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8720() {
   return (neuron0x8dc7ac8()*0.396226);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8748() {
   return (neuron0x8dc7ce0()*-0.174556);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8770() {
   return (neuron0x8dc7ef8()*0.238445);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8798() {
   return (neuron0x8dc8110()*-0.106909);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8a48() {
   return (neuron0x8dc6480()*0.405042);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8a70() {
   return (neuron0x8dc6660()*-0.389559);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8b20() {
   return (neuron0x8dc6888()*0.0064602);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8b48() {
   return (neuron0x8dc6a88()*0.346997);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8b70() {
   return (neuron0x8dc6c88()*-0.256584);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8b98() {
   return (neuron0x8dc6eb0()*0.0594992);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8bc0() {
   return (neuron0x8dc70b0()*0.232163);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8be8() {
   return (neuron0x8dc72b0()*-0.304751);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8c10() {
   return (neuron0x8dc74b0()*0.487385);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8c38() {
   return (neuron0x8dc76b0()*0.225472);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8c60() {
   return (neuron0x8dc78b0()*0.19299);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8c88() {
   return (neuron0x8dc7ac8()*-0.274643);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8cb0() {
   return (neuron0x8dc7ce0()*0.326032);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8cd8() {
   return (neuron0x8dc7ef8()*-0.0117984);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8d00() {
   return (neuron0x8dc8110()*-0.188858);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8e98() {
   return (neuron0x8dc6480()*-0.119194);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8ec0() {
   return (neuron0x8dc6660()*-0.139827);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8ee8() {
   return (neuron0x8dc6888()*-0.037765);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8a98() {
   return (neuron0x8dc6a88()*0.511602);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8ac0() {
   return (neuron0x8dc6c88()*-0.10475);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8ae8() {
   return (neuron0x8dc6eb0()*-0.330561);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8f10() {
   return (neuron0x8dc70b0()*0.271554);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8f38() {
   return (neuron0x8dc72b0()*-0.350982);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8f60() {
   return (neuron0x8dc74b0()*-0.37044);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8f88() {
   return (neuron0x8dc76b0()*0.570691);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8fb0() {
   return (neuron0x8dc78b0()*0.119099);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8fd8() {
   return (neuron0x8dc7ac8()*-0.0763578);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9000() {
   return (neuron0x8dc7ce0()*-0.0781656);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9028() {
   return (neuron0x8dc7ef8()*0.087222);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9050() {
   return (neuron0x8dc8110()*-0.300624);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9278() {
   return (neuron0x8dc6480()*-0.00930699);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc92a0() {
   return (neuron0x8dc6660()*-0.0423341);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc92c8() {
   return (neuron0x8dc6888()*-0.385904);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc92f0() {
   return (neuron0x8dc6a88()*-0.502424);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9318() {
   return (neuron0x8dc6c88()*0.28683);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9340() {
   return (neuron0x8dc6eb0()*0.117093);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9368() {
   return (neuron0x8dc70b0()*0.160412);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9390() {
   return (neuron0x8dc72b0()*0.414746);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc93b8() {
   return (neuron0x8dc74b0()*-0.445842);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc93e0() {
   return (neuron0x8dc76b0()*0.196093);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9408() {
   return (neuron0x8dc78b0()*-0.10792);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9430() {
   return (neuron0x8dc7ac8()*-0.23319);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9458() {
   return (neuron0x8dc7ce0()*0.172711);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9480() {
   return (neuron0x8dc7ef8()*-0.420688);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc94a8() {
   return (neuron0x8dc8110()*0.430471);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc96d0() {
   return (neuron0x8dc6480()*-0.188242);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc96f8() {
   return (neuron0x8dc6660()*0.140958);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9720() {
   return (neuron0x8dc6888()*0.344573);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9748() {
   return (neuron0x8dc6a88()*-0.137766);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9770() {
   return (neuron0x8dc6c88()*0.309195);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf0bb8() {
   return (neuron0x8dc6eb0()*0.155667);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf16a0() {
   return (neuron0x8dc70b0()*-0.461499);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf16c8() {
   return (neuron0x8dc72b0()*0.241631);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf16f0() {
   return (neuron0x8dc74b0()*0.093543);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf1718() {
   return (neuron0x8dc76b0()*-0.116282);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd258() {
   return (neuron0x8dc78b0()*0.25808);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd280() {
   return (neuron0x8dc7ac8()*-0.34974);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd2a8() {
   return (neuron0x8dc7ce0()*0.402067);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd2d0() {
   return (neuron0x8dc7ef8()*0.458287);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd2f8() {
   return (neuron0x8dc8110()*0.331385);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9bb8() {
   return (neuron0x8dc6480()*0.0956042);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9be0() {
   return (neuron0x8dc6660()*-0.0388634);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9c08() {
   return (neuron0x8dc6888()*-0.165841);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9c30() {
   return (neuron0x8dc6a88()*-0.483517);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9c58() {
   return (neuron0x8dc6c88()*0.0811789);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9c80() {
   return (neuron0x8dc6eb0()*0.0463872);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9ca8() {
   return (neuron0x8dc70b0()*-0.0383984);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9cd0() {
   return (neuron0x8dc72b0()*-0.212552);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9cf8() {
   return (neuron0x8dc74b0()*-0.133319);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9d20() {
   return (neuron0x8dc76b0()*-0.0219791);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9d48() {
   return (neuron0x8dc78b0()*0.0446718);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9d70() {
   return (neuron0x8dc7ac8()*-0.103326);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9d98() {
   return (neuron0x8dc7ce0()*0.0218885);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9dc0() {
   return (neuron0x8dc7ef8()*-0.0952041);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc9de8() {
   return (neuron0x8dc8110()*-0.189036);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca028() {
   return (neuron0x8dc6480()*-0.476092);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca050() {
   return (neuron0x8dc6660()*-0.0142348);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca078() {
   return (neuron0x8dc6888()*-0.221405);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca0a0() {
   return (neuron0x8dc6a88()*-0.0300851);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca0c8() {
   return (neuron0x8dc6c88()*0.17424);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca0f0() {
   return (neuron0x8dc6eb0()*0.490006);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca118() {
   return (neuron0x8dc70b0()*-0.156209);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca140() {
   return (neuron0x8dc72b0()*0.125317);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca168() {
   return (neuron0x8dc74b0()*-0.215099);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca190() {
   return (neuron0x8dc76b0()*-0.16938);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca1b8() {
   return (neuron0x8dc78b0()*-0.378228);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca1e0() {
   return (neuron0x8dc7ac8()*0.0214676);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca208() {
   return (neuron0x8dc7ce0()*-0.441797);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca230() {
   return (neuron0x8dc7ef8()*-0.411386);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca258() {
   return (neuron0x8dc8110()*0.271467);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca498() {
   return (neuron0x8dc6480()*-0.0427817);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca4c0() {
   return (neuron0x8dc6660()*0.0795605);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca4e8() {
   return (neuron0x8dc6888()*-0.434649);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca510() {
   return (neuron0x8dc6a88()*-0.0186937);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca538() {
   return (neuron0x8dc6c88()*0.193443);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca560() {
   return (neuron0x8dc6eb0()*-0.00568089);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca588() {
   return (neuron0x8dc70b0()*0.070333);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca5b0() {
   return (neuron0x8dc72b0()*-0.438667);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca5d8() {
   return (neuron0x8dc74b0()*0.175157);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca600() {
   return (neuron0x8dc76b0()*0.230199);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca628() {
   return (neuron0x8dc78b0()*-0.0447602);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca650() {
   return (neuron0x8dc7ac8()*-0.104696);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca678() {
   return (neuron0x8dc7ce0()*-0.135309);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca6a0() {
   return (neuron0x8dc7ef8()*0.246868);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca6c8() {
   return (neuron0x8dc8110()*-0.420976);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca908() {
   return (neuron0x8dc6480()*0.192972);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca930() {
   return (neuron0x8dc6660()*0.49551);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca958() {
   return (neuron0x8dc6888()*0.0288899);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca980() {
   return (neuron0x8dc6a88()*-0.223887);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca9a8() {
   return (neuron0x8dc6c88()*0.571501);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca9d0() {
   return (neuron0x8dc6eb0()*0.0957481);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dca9f8() {
   return (neuron0x8dc70b0()*-0.27244);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaa20() {
   return (neuron0x8dc72b0()*0.0321389);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaa48() {
   return (neuron0x8dc74b0()*0.0418005);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf1790() {
   return (neuron0x8dc76b0()*-0.0674241);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf17b8() {
   return (neuron0x8dc78b0()*0.434355);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf17e0() {
   return (neuron0x8dc7ac8()*0.0130829);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf1808() {
   return (neuron0x8dc7ce0()*-0.339126);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8bf1830() {
   return (neuron0x8dc7ef8()*0.453288);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8d5ab00() {
   return (neuron0x8dc8110()*-0.0166478);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8d5ab28() {
   return (neuron0x8dc6480()*0.289466);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8d5ab50() {
   return (neuron0x8dc6660()*0.000418573);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8d5ab78() {
   return (neuron0x8dc6888()*0.109035);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8d5aba0() {
   return (neuron0x8dc6a88()*-0.0555431);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8d5abc8() {
   return (neuron0x8dc6c88()*-0.330768);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcae78() {
   return (neuron0x8dc6eb0()*0.468613);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaea0() {
   return (neuron0x8dc70b0()*0.258665);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaec8() {
   return (neuron0x8dc72b0()*-0.12422);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaef0() {
   return (neuron0x8dc74b0()*-0.0800532);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaf18() {
   return (neuron0x8dc76b0()*0.152641);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaf40() {
   return (neuron0x8dc78b0()*0.00758662);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaf68() {
   return (neuron0x8dc7ac8()*-0.203678);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaf90() {
   return (neuron0x8dc7ce0()*-0.0668526);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcafb8() {
   return (neuron0x8dc7ef8()*-0.192986);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcafe0() {
   return (neuron0x8dc8110()*0.288322);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb208() {
   return (neuron0x8dc6480()*-0.43757);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb230() {
   return (neuron0x8dc6660()*0.16916);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb258() {
   return (neuron0x8dc6888()*-0.30831);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb280() {
   return (neuron0x8dc6a88()*0.389824);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb2a8() {
   return (neuron0x8dc6c88()*-0.0462248);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb2d0() {
   return (neuron0x8dc6eb0()*-0.0204967);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb2f8() {
   return (neuron0x8dc70b0()*-0.395275);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb320() {
   return (neuron0x8dc72b0()*-0.0961873);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb348() {
   return (neuron0x8dc74b0()*0.400831);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb370() {
   return (neuron0x8dc76b0()*-0.379184);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb398() {
   return (neuron0x8dc78b0()*-0.128951);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb3c0() {
   return (neuron0x8dc7ac8()*-0.230974);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb3e8() {
   return (neuron0x8dc7ce0()*0.420843);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb410() {
   return (neuron0x8dc7ef8()*-0.146391);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb438() {
   return (neuron0x8dc8110()*-0.377268);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb678() {
   return (neuron0x8dc6480()*0.440879);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb6a0() {
   return (neuron0x8dc6660()*0.194638);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb6c8() {
   return (neuron0x8dc6888()*0.17423);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb6f0() {
   return (neuron0x8dc6a88()*-0.275904);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb718() {
   return (neuron0x8dc6c88()*0.406938);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb740() {
   return (neuron0x8dc6eb0()*0.262963);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb768() {
   return (neuron0x8dc70b0()*-0.422071);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb790() {
   return (neuron0x8dc72b0()*-0.531452);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb7b8() {
   return (neuron0x8dc74b0()*-0.461678);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb7e0() {
   return (neuron0x8dc76b0()*0.322602);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb808() {
   return (neuron0x8dc78b0()*-0.406572);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb830() {
   return (neuron0x8dc7ac8()*0.311409);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb858() {
   return (neuron0x8dc7ce0()*0.100558);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb880() {
   return (neuron0x8dc7ef8()*0.443123);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcb8a8() {
   return (neuron0x8dc8110()*0.438792);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbae8() {
   return (neuron0x8dc6480()*0.152821);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbb10() {
   return (neuron0x8dc6660()*0.566997);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbb38() {
   return (neuron0x8dc6888()*-0.274167);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbb60() {
   return (neuron0x8dc6a88()*-0.141376);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbb88() {
   return (neuron0x8dc6c88()*0.363992);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbbb0() {
   return (neuron0x8dc6eb0()*-0.0226281);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbbd8() {
   return (neuron0x8dc70b0()*0.293857);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbc00() {
   return (neuron0x8dc72b0()*-0.222194);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbc28() {
   return (neuron0x8dc74b0()*-0.257085);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbc50() {
   return (neuron0x8dc76b0()*0.0205228);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbc78() {
   return (neuron0x8dc78b0()*0.249654);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbca0() {
   return (neuron0x8dc7ac8()*0.235085);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbcc8() {
   return (neuron0x8dc7ce0()*0.0655591);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbcf0() {
   return (neuron0x8dc7ef8()*-0.199792);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbd18() {
   return (neuron0x8dc8110()*0.045517);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbf58() {
   return (neuron0x8dc6480()*0.38677);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbf80() {
   return (neuron0x8dc6660()*-0.153952);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbfa8() {
   return (neuron0x8dc6888()*-0.386018);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbfd0() {
   return (neuron0x8dc6a88()*-0.352702);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcbff8() {
   return (neuron0x8dc6c88()*0.0688611);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc020() {
   return (neuron0x8dc6eb0()*-0.420865);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc048() {
   return (neuron0x8dc70b0()*-0.186229);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc070() {
   return (neuron0x8dc72b0()*0.297875);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc098() {
   return (neuron0x8dc74b0()*-0.257924);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc0c0() {
   return (neuron0x8dc76b0()*0.319966);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc0e8() {
   return (neuron0x8dc78b0()*0.0797348);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc110() {
   return (neuron0x8dc7ac8()*-0.0233072);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc138() {
   return (neuron0x8dc7ce0()*-0.00327186);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc160() {
   return (neuron0x8dc7ef8()*0.142618);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcc188() {
   return (neuron0x8dc8110()*0.0109235);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd510() {
   return (neuron0x8dc6480()*-0.335061);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd538() {
   return (neuron0x8dc6660()*-0.345656);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd560() {
   return (neuron0x8dc6888()*0.39831);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd588() {
   return (neuron0x8dc6a88()*-0.466903);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd5b0() {
   return (neuron0x8dc6c88()*0.0287608);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd5d8() {
   return (neuron0x8dc6eb0()*0.00829769);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd600() {
   return (neuron0x8dc70b0()*-0.035842);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd628() {
   return (neuron0x8dc72b0()*-0.00239604);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd650() {
   return (neuron0x8dc74b0()*-0.0100326);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd678() {
   return (neuron0x8dc76b0()*-0.0259452);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd6a0() {
   return (neuron0x8dc78b0()*0.0333448);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd6c8() {
   return (neuron0x8dc7ac8()*0.0309725);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd6f0() {
   return (neuron0x8dc7ce0()*-0.0236382);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd718() {
   return (neuron0x8dc7ef8()*0.203935);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd740() {
   return (neuron0x8dc8110()*-0.046929);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd968() {
   return (neuron0x8dc6480()*-0.0842788);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcda18() {
   return (neuron0x8dc6660()*-0.305707);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdac8() {
   return (neuron0x8dc6888()*0.0259283);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdb78() {
   return (neuron0x8dc6a88()*0.242398);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdc28() {
   return (neuron0x8dc6c88()*-0.246238);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdcd8() {
   return (neuron0x8dc6eb0()*-0.386977);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdd88() {
   return (neuron0x8dc70b0()*-0.272409);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcde38() {
   return (neuron0x8dc72b0()*-0.220752);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdee8() {
   return (neuron0x8dc74b0()*-0.27876);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdf98() {
   return (neuron0x8dc76b0()*-0.0391679);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce048() {
   return (neuron0x8dc78b0()*-0.0809743);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce0f8() {
   return (neuron0x8dc7ac8()*-0.0536493);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce1a8() {
   return (neuron0x8dc7ce0()*0.0448961);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce258() {
   return (neuron0x8dc7ef8()*-0.0539505);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce308() {
   return (neuron0x8dc8110()*0.373813);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce4e0() {
   return (neuron0x8dc6480()*-0.0586964);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce508() {
   return (neuron0x8dc6660()*-0.000608652);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce530() {
   return (neuron0x8dc6888()*0.362838);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce558() {
   return (neuron0x8dc6a88()*0.326338);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce580() {
   return (neuron0x8dc6c88()*-0.122711);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce5a8() {
   return (neuron0x8dc6eb0()*-0.426565);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce5d0() {
   return (neuron0x8dc70b0()*-0.0902955);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce5f8() {
   return (neuron0x8dc72b0()*0.452884);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce620() {
   return (neuron0x8dc74b0()*-0.323108);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce648() {
   return (neuron0x8dc76b0()*-0.341485);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce670() {
   return (neuron0x8dc78b0()*0.480892);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce698() {
   return (neuron0x8dc7ac8()*-0.196584);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce6c0() {
   return (neuron0x8dc7ce0()*-0.214236);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce6e8() {
   return (neuron0x8dc7ef8()*-0.380437);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce710() {
   return (neuron0x8dc8110()*-0.164668);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc89c0() {
   return (neuron0x8dc6480()*-0.116093);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc89e8() {
   return (neuron0x8dc6660()*-0.465704);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc8a10() {
   return (neuron0x8dc6888()*0.240695);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc7620() {
   return (neuron0x8dc6a88()*0.0778154);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc7420() {
   return (neuron0x8dc6c88()*0.190093);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc7220() {
   return (neuron0x8dc6eb0()*-0.208094);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc7020() {
   return (neuron0x8dc70b0()*0.109244);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc6e20() {
   return (neuron0x8dc72b0()*-0.272595);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc6bf8() {
   return (neuron0x8dc74b0()*0.0185505);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc69f8() {
   return (neuron0x8dc76b0()*-0.277071);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc67f8() {
   return (neuron0x8dc78b0()*0.320212);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dc65d0() {
   return (neuron0x8dc7ac8()*0.132709);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaa70() {
   return (neuron0x8dc7ce0()*-0.447976);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaa98() {
   return (neuron0x8dc7ef8()*0.269727);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcaac0() {
   return (neuron0x8dc8110()*0.303575);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcad00() {
   return (neuron0x8dc6480()*-0.277488);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcad28() {
   return (neuron0x8dc6660()*0.12351);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcad50() {
   return (neuron0x8dc6888()*0.435844);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcad78() {
   return (neuron0x8dc6a88()*0.0307276);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcada0() {
   return (neuron0x8dc6c88()*0.0848724);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcadc8() {
   return (neuron0x8dc6eb0()*-0.204325);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcadf0() {
   return (neuron0x8dc70b0()*0.133065);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcae18() {
   return (neuron0x8dc72b0()*0.177637);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcae40() {
   return (neuron0x8dc74b0()*-0.0249013);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf170() {
   return (neuron0x8dc76b0()*-0.455138);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf198() {
   return (neuron0x8dc78b0()*-0.347086);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf1c0() {
   return (neuron0x8dc7ac8()*0.145108);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf1e8() {
   return (neuron0x8dc7ce0()*0.351427);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf210() {
   return (neuron0x8dc7ef8()*-0.009153);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf238() {
   return (neuron0x8dc8110()*-0.115785);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf460() {
   return (neuron0x8dc6480()*-0.157079);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf488() {
   return (neuron0x8dc6660()*0.135831);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf4b0() {
   return (neuron0x8dc6888()*0.309436);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf4d8() {
   return (neuron0x8dc6a88()*-0.0125085);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf500() {
   return (neuron0x8dc6c88()*0.255833);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf528() {
   return (neuron0x8dc6eb0()*0.304573);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf550() {
   return (neuron0x8dc70b0()*0.164614);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf578() {
   return (neuron0x8dc72b0()*0.191897);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf5a0() {
   return (neuron0x8dc74b0()*0.293482);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf5c8() {
   return (neuron0x8dc76b0()*-0.341059);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf5f0() {
   return (neuron0x8dc78b0()*0.361246);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf618() {
   return (neuron0x8dc7ac8()*-0.359402);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf640() {
   return (neuron0x8dc7ce0()*-0.152129);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf668() {
   return (neuron0x8dc7ef8()*-0.0676729);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf690() {
   return (neuron0x8dc8110()*-0.0279958);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf8b8() {
   return (neuron0x8dc6480()*-0.240305);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf8e0() {
   return (neuron0x8dc6660()*0.00812549);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf908() {
   return (neuron0x8dc6888()*0.289347);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf930() {
   return (neuron0x8dc6a88()*0.417732);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf958() {
   return (neuron0x8dc6c88()*0.445815);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf980() {
   return (neuron0x8dc6eb0()*-0.178919);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf9a8() {
   return (neuron0x8dc70b0()*-0.439364);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf9d0() {
   return (neuron0x8dc72b0()*0.233335);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcf9f8() {
   return (neuron0x8dc74b0()*-0.0668157);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfa20() {
   return (neuron0x8dc76b0()*0.298715);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfa48() {
   return (neuron0x8dc78b0()*-0.0786297);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfa70() {
   return (neuron0x8dc7ac8()*0.372225);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfa98() {
   return (neuron0x8dc7ce0()*-0.40472);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfac0() {
   return (neuron0x8dc7ef8()*0.305922);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfae8() {
   return (neuron0x8dc8110()*-0.110553);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfd10() {
   return (neuron0x8dc6480()*-0.338528);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfd38() {
   return (neuron0x8dc6660()*-0.085401);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfd60() {
   return (neuron0x8dc6888()*-0.297746);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfd88() {
   return (neuron0x8dc6a88()*-0.405675);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfdb0() {
   return (neuron0x8dc6c88()*0.166571);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfdd8() {
   return (neuron0x8dc6eb0()*-0.0592187);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfe00() {
   return (neuron0x8dc70b0()*0.347968);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfe28() {
   return (neuron0x8dc72b0()*0.386263);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfe50() {
   return (neuron0x8dc74b0()*0.228082);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfe78() {
   return (neuron0x8dc76b0()*0.326314);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfea0() {
   return (neuron0x8dc78b0()*-0.147633);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfec8() {
   return (neuron0x8dc7ac8()*0.414888);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcfef0() {
   return (neuron0x8dc7ce0()*-0.416855);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcff18() {
   return (neuron0x8dc7ef8()*-0.15088);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcff40() {
   return (neuron0x8dc8110()*-0.240361);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0168() {
   return (neuron0x8dc6480()*-0.182639);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0190() {
   return (neuron0x8dc6660()*-0.0441439);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd01b8() {
   return (neuron0x8dc6888()*0.20408);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd01e0() {
   return (neuron0x8dc6a88()*0.0616465);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0208() {
   return (neuron0x8dc6c88()*0.0698152);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0230() {
   return (neuron0x8dc6eb0()*0.0673481);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0258() {
   return (neuron0x8dc70b0()*-0.12445);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0280() {
   return (neuron0x8dc72b0()*-0.329951);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd02a8() {
   return (neuron0x8dc74b0()*0.124374);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd02d0() {
   return (neuron0x8dc76b0()*0.0185364);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd02f8() {
   return (neuron0x8dc78b0()*0.0730302);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0320() {
   return (neuron0x8dc7ac8()*-0.20904);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0348() {
   return (neuron0x8dc7ce0()*-0.00554373);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0370() {
   return (neuron0x8dc7ef8()*0.433541);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0398() {
   return (neuron0x8dc8110()*0.153548);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd05c0() {
   return (neuron0x8dc6480()*-0.0870161);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd05e8() {
   return (neuron0x8dc6660()*0.250721);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0610() {
   return (neuron0x8dc6888()*-0.0783666);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0638() {
   return (neuron0x8dc6a88()*0.320728);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0660() {
   return (neuron0x8dc6c88()*-0.127383);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0688() {
   return (neuron0x8dc6eb0()*-0.13784);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd06b0() {
   return (neuron0x8dc70b0()*0.0990095);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd06d8() {
   return (neuron0x8dc72b0()*0.421475);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0700() {
   return (neuron0x8dc74b0()*-0.0307449);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0728() {
   return (neuron0x8dc76b0()*0.183918);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0750() {
   return (neuron0x8dc78b0()*0.342394);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0778() {
   return (neuron0x8dc7ac8()*-0.155887);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd07a0() {
   return (neuron0x8dc7ce0()*-0.135995);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd07c8() {
   return (neuron0x8dc7ef8()*0.325441);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd07f0() {
   return (neuron0x8dc8110()*0.392229);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0a18() {
   return (neuron0x8dc6480()*0.326536);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0a40() {
   return (neuron0x8dc6660()*-0.025214);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0a68() {
   return (neuron0x8dc6888()*-0.515863);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0a90() {
   return (neuron0x8dc6a88()*0.0938873);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ab8() {
   return (neuron0x8dc6c88()*-0.183569);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ae0() {
   return (neuron0x8dc6eb0()*-0.265865);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0b08() {
   return (neuron0x8dc70b0()*0.103113);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0b30() {
   return (neuron0x8dc72b0()*0.287426);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0b58() {
   return (neuron0x8dc74b0()*0.133986);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0b80() {
   return (neuron0x8dc76b0()*-0.294492);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ba8() {
   return (neuron0x8dc78b0()*-0.325759);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0bd0() {
   return (neuron0x8dc7ac8()*0.162976);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0bf8() {
   return (neuron0x8dc7ce0()*0.351699);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0c20() {
   return (neuron0x8dc7ef8()*-0.197089);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0c48() {
   return (neuron0x8dc8110()*0.252755);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0e70() {
   return (neuron0x8dc6480()*0.0147135);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0e98() {
   return (neuron0x8dc6660()*-0.298806);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ec0() {
   return (neuron0x8dc6888()*-0.454731);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0ee8() {
   return (neuron0x8dc6a88()*-0.140541);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0f10() {
   return (neuron0x8dc6c88()*-0.332085);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0f38() {
   return (neuron0x8dc6eb0()*-0.16252);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0f60() {
   return (neuron0x8dc70b0()*-0.33065);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0f88() {
   return (neuron0x8dc72b0()*-0.378639);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0fb0() {
   return (neuron0x8dc74b0()*-0.447304);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd0fd8() {
   return (neuron0x8dc76b0()*0.155524);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1000() {
   return (neuron0x8dc78b0()*-0.198166);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1028() {
   return (neuron0x8dc7ac8()*-0.129735);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1050() {
   return (neuron0x8dc7ce0()*0.351741);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1078() {
   return (neuron0x8dc7ef8()*-0.188933);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd10a0() {
   return (neuron0x8dc8110()*-0.432051);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd12c8() {
   return (neuron0x8dc6480()*0.363166);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd12f0() {
   return (neuron0x8dc6660()*-0.0815787);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1318() {
   return (neuron0x8dc6888()*-0.0147619);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1340() {
   return (neuron0x8dc6a88()*-0.381779);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1368() {
   return (neuron0x8dc6c88()*-0.290238);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1390() {
   return (neuron0x8dc6eb0()*0.429013);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd13b8() {
   return (neuron0x8dc70b0()*-0.296681);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd13e0() {
   return (neuron0x8dc72b0()*-0.134799);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1408() {
   return (neuron0x8dc74b0()*0.417112);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1430() {
   return (neuron0x8dc76b0()*0.101635);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1458() {
   return (neuron0x8dc78b0()*0.397201);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1480() {
   return (neuron0x8dc7ac8()*0.0508658);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd14a8() {
   return (neuron0x8dc7ce0()*-0.055822);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd14d0() {
   return (neuron0x8dc7ef8()*0.2556);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd14f8() {
   return (neuron0x8dc8110()*-0.0440312);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1720() {
   return (neuron0x8dc6480()*0.0345976);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1748() {
   return (neuron0x8dc6660()*0.320099);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1770() {
   return (neuron0x8dc6888()*-0.167166);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1798() {
   return (neuron0x8dc6a88()*0.121757);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd17c0() {
   return (neuron0x8dc6c88()*-0.285321);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd17e8() {
   return (neuron0x8dc6eb0()*0.0101188);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1810() {
   return (neuron0x8dc70b0()*-0.29111);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1838() {
   return (neuron0x8dc72b0()*-0.257065);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1860() {
   return (neuron0x8dc74b0()*0.409595);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1888() {
   return (neuron0x8dc76b0()*-0.287751);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd18b0() {
   return (neuron0x8dc78b0()*0.277363);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd18d8() {
   return (neuron0x8dc7ac8()*-0.474528);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1900() {
   return (neuron0x8dc7ce0()*0.170913);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1928() {
   return (neuron0x8dc7ef8()*0.440978);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1950() {
   return (neuron0x8dc8110()*-0.156088);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1b78() {
   return (neuron0x8dc6480()*0.387981);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1ba0() {
   return (neuron0x8dc6660()*0.299983);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1bc8() {
   return (neuron0x8dc6888()*0.000752373);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1bf0() {
   return (neuron0x8dc6a88()*-0.172196);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1c18() {
   return (neuron0x8dc6c88()*-0.142001);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1c40() {
   return (neuron0x8dc6eb0()*-0.0969644);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1c68() {
   return (neuron0x8dc70b0()*-0.0396766);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1c90() {
   return (neuron0x8dc72b0()*0.253484);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1cb8() {
   return (neuron0x8dc74b0()*0.348837);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1ce0() {
   return (neuron0x8dc76b0()*0.104718);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1d08() {
   return (neuron0x8dc78b0()*-0.226782);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1d30() {
   return (neuron0x8dc7ac8()*0.460209);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1d58() {
   return (neuron0x8dc7ce0()*-0.211659);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1d80() {
   return (neuron0x8dc7ef8()*-0.222355);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1da8() {
   return (neuron0x8dc8110()*0.533994);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1fd0() {
   return (neuron0x8dc6480()*-0.0150766);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd1ff8() {
   return (neuron0x8dc6660()*0.34441);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2020() {
   return (neuron0x8dc6888()*-0.202181);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2048() {
   return (neuron0x8dc6a88()*-0.102955);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2070() {
   return (neuron0x8dc6c88()*0.366279);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2098() {
   return (neuron0x8dc6eb0()*-0.133999);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd20c0() {
   return (neuron0x8dc70b0()*0.167388);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd20e8() {
   return (neuron0x8dc72b0()*0.0581526);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2110() {
   return (neuron0x8dc74b0()*-0.397762);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2138() {
   return (neuron0x8dc76b0()*0.226045);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2160() {
   return (neuron0x8dc78b0()*-0.289495);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2188() {
   return (neuron0x8dc7ac8()*0.179704);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd21b0() {
   return (neuron0x8dc7ce0()*-0.0182572);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd21d8() {
   return (neuron0x8dc7ef8()*0.429612);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2200() {
   return (neuron0x8dc8110()*0.416521);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2428() {
   return (neuron0x8dc6480()*0.266299);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2450() {
   return (neuron0x8dc6660()*-0.494249);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2478() {
   return (neuron0x8dc6888()*0.413402);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd24a0() {
   return (neuron0x8dc6a88()*0.453493);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd24c8() {
   return (neuron0x8dc6c88()*-0.00133133);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd24f0() {
   return (neuron0x8dc6eb0()*0.334313);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2518() {
   return (neuron0x8dc70b0()*0.33029);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2540() {
   return (neuron0x8dc72b0()*0.152441);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2568() {
   return (neuron0x8dc74b0()*-0.336304);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2590() {
   return (neuron0x8dc76b0()*0.0707802);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd25b8() {
   return (neuron0x8dc78b0()*0.24425);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd25e0() {
   return (neuron0x8dc7ac8()*-0.38806);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2608() {
   return (neuron0x8dc7ce0()*-0.0823145);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2630() {
   return (neuron0x8dc7ef8()*-0.315289);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2658() {
   return (neuron0x8dc8110()*0.103994);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd2880() {
   return (neuron0x8dc6480()*0.195477);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd990() {
   return (neuron0x8dc6660()*-0.100123);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd9b8() {
   return (neuron0x8dc6888()*0.500238);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcd9e0() {
   return (neuron0x8dc6a88()*-0.116609);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcda40() {
   return (neuron0x8dc6c88()*-0.350169);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcda68() {
   return (neuron0x8dc6eb0()*0.116463);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcda90() {
   return (neuron0x8dc70b0()*-0.0775125);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdaf0() {
   return (neuron0x8dc72b0()*0.257786);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdb18() {
   return (neuron0x8dc74b0()*0.0302721);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdb40() {
   return (neuron0x8dc76b0()*0.207995);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdba0() {
   return (neuron0x8dc78b0()*0.0290286);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdbc8() {
   return (neuron0x8dc7ac8()*0.383099);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdbf0() {
   return (neuron0x8dc7ce0()*-0.421226);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdc50() {
   return (neuron0x8dc7ef8()*-0.0120724);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdc78() {
   return (neuron0x8dc8110()*0.342911);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdea8() {
   return (neuron0x8dc6480()*0.347456);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdca0() {
   return (neuron0x8dc6660()*-0.513034);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdd48() {
   return (neuron0x8dc6888()*0.267315);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcddf8() {
   return (neuron0x8dc6a88()*0.449822);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdf10() {
   return (neuron0x8dc6c88()*-0.043464);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdf38() {
   return (neuron0x8dc6eb0()*0.30961);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdf60() {
   return (neuron0x8dc70b0()*-0.0791933);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdfc0() {
   return (neuron0x8dc72b0()*-0.375458);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcdfe8() {
   return (neuron0x8dc74b0()*-0.209947);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce010() {
   return (neuron0x8dc76b0()*0.103307);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce070() {
   return (neuron0x8dc78b0()*-0.145391);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce098() {
   return (neuron0x8dc7ac8()*-0.215067);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce0c0() {
   return (neuron0x8dc7ce0()*0.0634811);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce120() {
   return (neuron0x8dc7ef8()*-0.357974);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce148() {
   return (neuron0x8dc8110()*-0.169495);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce378() {
   return (neuron0x8dc6480()*-0.157304);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce170() {
   return (neuron0x8dc6660()*0.411042);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce218() {
   return (neuron0x8dc6888()*-0.443079);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce2c8() {
   return (neuron0x8dc6a88()*0.457326);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3a70() {
   return (neuron0x8dc6c88()*-0.00532663);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3a98() {
   return (neuron0x8dc6eb0()*-0.21695);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3ac0() {
   return (neuron0x8dc70b0()*0.285768);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3ae8() {
   return (neuron0x8dc72b0()*0.142926);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3b10() {
   return (neuron0x8dc74b0()*0.146087);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3b38() {
   return (neuron0x8dc76b0()*-0.27069);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3b60() {
   return (neuron0x8dc78b0()*0.281516);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3b88() {
   return (neuron0x8dc7ac8()*0.180868);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3bb0() {
   return (neuron0x8dc7ce0()*-0.365969);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3bd8() {
   return (neuron0x8dc7ef8()*-0.261198);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3c00() {
   return (neuron0x8dc8110()*-0.362122);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3e28() {
   return (neuron0x8dc6480()*-0.362156);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3e50() {
   return (neuron0x8dc6660()*-0.00959377);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd3e78() {
   return (neuron0x8dc6888()*-0.150541);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce968() {
   return (neuron0x8dc6a88()*-0.0167691);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce990() {
   return (neuron0x8dc6c88()*-0.0696093);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce9b8() {
   return (neuron0x8dc6eb0()*-0.332368);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce9e0() {
   return (neuron0x8dc70b0()*-0.110051);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcea08() {
   return (neuron0x8dc72b0()*0.480127);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcea30() {
   return (neuron0x8dc74b0()*0.0485417);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcea58() {
   return (neuron0x8dc76b0()*0.406903);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcea80() {
   return (neuron0x8dc78b0()*0.111296);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dceaa8() {
   return (neuron0x8dc7ac8()*-0.216577);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcead0() {
   return (neuron0x8dc7ce0()*-0.11117);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dceaf8() {
   return (neuron0x8dc7ef8()*0.00879081);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dceb20() {
   return (neuron0x8dc8110()*0.426553);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dced48() {
   return (neuron0x8dc6480()*0.426028);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dced70() {
   return (neuron0x8dc6660()*0.251938);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dced98() {
   return (neuron0x8dc6888()*0.351523);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcedc0() {
   return (neuron0x8dc6a88()*0.0659622);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcede8() {
   return (neuron0x8dc6c88()*-0.290699);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcee10() {
   return (neuron0x8dc6eb0()*-0.00418849);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcee38() {
   return (neuron0x8dc70b0()*0.0106051);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcee60() {
   return (neuron0x8dc72b0()*-0.109207);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcee88() {
   return (neuron0x8dc74b0()*-0.0940062);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dceeb0() {
   return (neuron0x8dc76b0()*-0.343169);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dceed8() {
   return (neuron0x8dc78b0()*0.352749);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcef00() {
   return (neuron0x8dc7ac8()*-0.186959);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcef28() {
   return (neuron0x8dc7ce0()*0.225777);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcef50() {
   return (neuron0x8dc7ef8()*0.0339407);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dcef78() {
   return (neuron0x8dc8110()*-0.109297);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd4ef0() {
   return (neuron0x8dc6480()*-0.155259);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd4f18() {
   return (neuron0x8dc6660()*-0.0861716);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd4f40() {
   return (neuron0x8dc6888()*0.240116);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd4f68() {
   return (neuron0x8dc6a88()*0.335673);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd4f90() {
   return (neuron0x8dc6c88()*-0.175103);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd4fb8() {
   return (neuron0x8dc6eb0()*0.168378);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd4fe0() {
   return (neuron0x8dc70b0()*-0.163056);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5008() {
   return (neuron0x8dc72b0()*-0.167956);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5030() {
   return (neuron0x8dc74b0()*-0.0066875);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5058() {
   return (neuron0x8dc76b0()*-0.0765531);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5080() {
   return (neuron0x8dc78b0()*0.0625097);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd50a8() {
   return (neuron0x8dc7ac8()*0.189826);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd50d0() {
   return (neuron0x8dc7ce0()*-0.143964);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd50f8() {
   return (neuron0x8dc7ef8()*0.375804);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5120() {
   return (neuron0x8dc8110()*0.192683);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5348() {
   return (neuron0x8dc6480()*-0.297529);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5370() {
   return (neuron0x8dc6660()*0.141485);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5398() {
   return (neuron0x8dc6888()*0.325162);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd53c0() {
   return (neuron0x8dc6a88()*0.174135);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd53e8() {
   return (neuron0x8dc6c88()*-0.119813);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5410() {
   return (neuron0x8dc6eb0()*0.0615786);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5438() {
   return (neuron0x8dc70b0()*0.211558);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5460() {
   return (neuron0x8dc72b0()*-0.187566);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5488() {
   return (neuron0x8dc74b0()*-0.0404078);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd54b0() {
   return (neuron0x8dc76b0()*0.138075);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd54d8() {
   return (neuron0x8dc78b0()*-0.0754624);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5500() {
   return (neuron0x8dc7ac8()*0.0112261);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5528() {
   return (neuron0x8dc7ce0()*0.19649);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5550() {
   return (neuron0x8dc7ef8()*-0.0253694);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5578() {
   return (neuron0x8dc8110()*0.150224);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd57a0() {
   return (neuron0x8dc6480()*0.382094);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd57c8() {
   return (neuron0x8dc6660()*-0.109498);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd57f0() {
   return (neuron0x8dc6888()*-0.412784);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5818() {
   return (neuron0x8dc6a88()*0.0135089);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5840() {
   return (neuron0x8dc6c88()*0.16401);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5868() {
   return (neuron0x8dc6eb0()*-0.0457603);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5890() {
   return (neuron0x8dc70b0()*-0.0749636);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd58b8() {
   return (neuron0x8dc72b0()*-0.375678);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd58e0() {
   return (neuron0x8dc74b0()*0.118508);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5908() {
   return (neuron0x8dc76b0()*-8.19697e-05);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5930() {
   return (neuron0x8dc78b0()*-0.344905);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5958() {
   return (neuron0x8dc7ac8()*-0.0113558);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5980() {
   return (neuron0x8dc7ce0()*-0.224079);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd59a8() {
   return (neuron0x8dc7ef8()*-0.0236468);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd59d0() {
   return (neuron0x8dc8110()*-0.290658);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5bf8() {
   return (neuron0x8dc6480()*-0.463434);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5c20() {
   return (neuron0x8dc6660()*-0.0478362);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5c48() {
   return (neuron0x8dc6888()*0.356657);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5c70() {
   return (neuron0x8dc6a88()*-0.229061);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5c98() {
   return (neuron0x8dc6c88()*-0.267047);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5cc0() {
   return (neuron0x8dc6eb0()*0.0561317);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5ce8() {
   return (neuron0x8dc70b0()*0.181137);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5d10() {
   return (neuron0x8dc72b0()*0.154323);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5d38() {
   return (neuron0x8dc74b0()*-0.0893417);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5d60() {
   return (neuron0x8dc76b0()*0.0603938);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5d88() {
   return (neuron0x8dc78b0()*-0.0235087);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5db0() {
   return (neuron0x8dc7ac8()*0.466859);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5dd8() {
   return (neuron0x8dc7ce0()*-0.0266736);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5e00() {
   return (neuron0x8dc7ef8()*-0.429874);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd5e28() {
   return (neuron0x8dc8110()*0.123327);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6050() {
   return (neuron0x8dc6480()*0.147065);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6078() {
   return (neuron0x8dc6660()*0.165166);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd60a0() {
   return (neuron0x8dc6888()*-0.250071);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd60c8() {
   return (neuron0x8dc6a88()*-0.356351);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd60f0() {
   return (neuron0x8dc6c88()*0.144448);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6118() {
   return (neuron0x8dc6eb0()*-0.325017);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6140() {
   return (neuron0x8dc70b0()*-0.237857);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6168() {
   return (neuron0x8dc72b0()*0.135339);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6190() {
   return (neuron0x8dc74b0()*-0.256195);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd61b8() {
   return (neuron0x8dc76b0()*-0.0602645);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd61e0() {
   return (neuron0x8dc78b0()*-0.0332877);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6208() {
   return (neuron0x8dc7ac8()*-0.348876);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6230() {
   return (neuron0x8dc7ce0()*-0.154449);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6258() {
   return (neuron0x8dc7ef8()*-0.0904033);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6280() {
   return (neuron0x8dc8110()*-0.39401);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd64c0() {
   return (neuron0x8dc6480()*-0.429497);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd64e8() {
   return (neuron0x8dc6660()*0.367592);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6510() {
   return (neuron0x8dc6888()*-0.0287658);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6538() {
   return (neuron0x8dc6a88()*0.26295);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6560() {
   return (neuron0x8dc6c88()*0.218876);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6588() {
   return (neuron0x8dc6eb0()*-0.349328);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd65b0() {
   return (neuron0x8dc70b0()*0.0596645);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd65d8() {
   return (neuron0x8dc72b0()*-0.445194);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6600() {
   return (neuron0x8dc74b0()*-0.303002);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6628() {
   return (neuron0x8dc76b0()*-0.0972534);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6650() {
   return (neuron0x8dc78b0()*-0.261039);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6678() {
   return (neuron0x8dc7ac8()*0.0748935);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd66a0() {
   return (neuron0x8dc7ce0()*-0.282319);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd66c8() {
   return (neuron0x8dc7ef8()*-0.407689);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd66f0() {
   return (neuron0x8dc8110()*-0.473351);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6930() {
   return (neuron0x8dc6480()*0.283562);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6958() {
   return (neuron0x8dc6660()*0.326655);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6980() {
   return (neuron0x8dc6888()*-0.0165525);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd69a8() {
   return (neuron0x8dc6a88()*0.259855);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd69d0() {
   return (neuron0x8dc6c88()*-0.403735);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd69f8() {
   return (neuron0x8dc6eb0()*0.0936162);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6a20() {
   return (neuron0x8dc70b0()*0.0790426);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6a48() {
   return (neuron0x8dc72b0()*0.0670144);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6a70() {
   return (neuron0x8dc74b0()*-0.211131);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6a98() {
   return (neuron0x8dc76b0()*0.47778);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6ac0() {
   return (neuron0x8dc78b0()*-0.097318);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6ae8() {
   return (neuron0x8dc7ac8()*0.0416496);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6b10() {
   return (neuron0x8dc7ce0()*0.227957);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6b38() {
   return (neuron0x8dc7ef8()*0.0227661);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6b60() {
   return (neuron0x8dc8110()*0.0642698);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6da0() {
   return (neuron0x8dc6480()*0.451087);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6dc8() {
   return (neuron0x8dc6660()*-0.210662);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6df0() {
   return (neuron0x8dc6888()*-0.220789);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6e18() {
   return (neuron0x8dc6a88()*0.233692);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6e40() {
   return (neuron0x8dc6c88()*-0.117715);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6e68() {
   return (neuron0x8dc6eb0()*-0.105288);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6e90() {
   return (neuron0x8dc70b0()*-0.0809277);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6eb8() {
   return (neuron0x8dc72b0()*-0.0549528);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6ee0() {
   return (neuron0x8dc74b0()*-0.32517);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6f08() {
   return (neuron0x8dc76b0()*0.201962);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6f30() {
   return (neuron0x8dc78b0()*0.103547);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6f58() {
   return (neuron0x8dc7ac8()*0.103073);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6f80() {
   return (neuron0x8dc7ce0()*0.34165);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6fa8() {
   return (neuron0x8dc7ef8()*0.50731);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd6fd0() {
   return (neuron0x8dc8110()*0.294841);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7210() {
   return (neuron0x8dc6480()*0.148006);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7238() {
   return (neuron0x8dc6660()*-0.0525417);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7260() {
   return (neuron0x8dc6888()*-0.441959);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7288() {
   return (neuron0x8dc6a88()*0.39829);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd72b0() {
   return (neuron0x8dc6c88()*-0.304677);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd72d8() {
   return (neuron0x8dc6eb0()*0.153869);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7300() {
   return (neuron0x8dc70b0()*0.205819);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7328() {
   return (neuron0x8dc72b0()*-0.321075);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7350() {
   return (neuron0x8dc74b0()*-0.19024);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7378() {
   return (neuron0x8dc76b0()*0.23229);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd73a0() {
   return (neuron0x8dc78b0()*-0.0703208);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd73c8() {
   return (neuron0x8dc7ac8()*0.320425);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd73f0() {
   return (neuron0x8dc7ce0()*0.245195);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7418() {
   return (neuron0x8dc7ef8()*0.233951);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7440() {
   return (neuron0x8dc8110()*0.21732);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7680() {
   return (neuron0x8dc6480()*-0.100122);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd76a8() {
   return (neuron0x8dc6660()*0.11891);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd76d0() {
   return (neuron0x8dc6888()*-0.443718);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd76f8() {
   return (neuron0x8dc6a88()*-0.36223);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7720() {
   return (neuron0x8dc6c88()*-0.0854991);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7748() {
   return (neuron0x8dc6eb0()*-0.345259);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7770() {
   return (neuron0x8dc70b0()*0.332343);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7798() {
   return (neuron0x8dc72b0()*0.279696);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd77c0() {
   return (neuron0x8dc74b0()*-0.188495);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd77e8() {
   return (neuron0x8dc76b0()*0.351957);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7810() {
   return (neuron0x8dc78b0()*-0.0407909);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7838() {
   return (neuron0x8dc7ac8()*-0.0866058);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7860() {
   return (neuron0x8dc7ce0()*-0.00553767);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7888() {
   return (neuron0x8dc7ef8()*0.104215);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd78b0() {
   return (neuron0x8dc8110()*0.190088);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7af0() {
   return (neuron0x8dc6480()*-0.490354);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7b18() {
   return (neuron0x8dc6660()*0.223214);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7b40() {
   return (neuron0x8dc6888()*0.446572);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7b68() {
   return (neuron0x8dc6a88()*0.537147);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7b90() {
   return (neuron0x8dc6c88()*0.0347405);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7bb8() {
   return (neuron0x8dc6eb0()*0.0331292);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7be0() {
   return (neuron0x8dc70b0()*-0.0252514);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7c08() {
   return (neuron0x8dc72b0()*0.0466225);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7c30() {
   return (neuron0x8dc74b0()*0.0813833);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7c58() {
   return (neuron0x8dc76b0()*-0.0288637);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7c80() {
   return (neuron0x8dc78b0()*0.011019);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7ca8() {
   return (neuron0x8dc7ac8()*-0.0357492);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7cd0() {
   return (neuron0x8dc7ce0()*-0.000824768);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7cf8() {
   return (neuron0x8dc7ef8()*-0.154271);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7d20() {
   return (neuron0x8dc8110()*-0.115793);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7f60() {
   return (neuron0x8dc6480()*-0.0897252);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7f88() {
   return (neuron0x8dc6660()*-0.0331743);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7fb0() {
   return (neuron0x8dc6888()*-0.0839851);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd7fd8() {
   return (neuron0x8dc6a88()*-0.498291);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8000() {
   return (neuron0x8dc6c88()*-0.151079);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8028() {
   return (neuron0x8dc6eb0()*0.0577211);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8050() {
   return (neuron0x8dc70b0()*-0.0101468);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8078() {
   return (neuron0x8dc72b0()*-0.249169);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd80a0() {
   return (neuron0x8dc74b0()*0.275245);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd80c8() {
   return (neuron0x8dc76b0()*-0.229831);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd80f0() {
   return (neuron0x8dc78b0()*0.114801);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8118() {
   return (neuron0x8dc7ac8()*-0.366761);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8140() {
   return (neuron0x8dc7ce0()*-0.272833);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8168() {
   return (neuron0x8dc7ef8()*-0.0658259);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8190() {
   return (neuron0x8dc8110()*0.13826);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd83d0() {
   return (neuron0x8dc6480()*-0.105327);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd83f8() {
   return (neuron0x8dc6660()*0.465028);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8420() {
   return (neuron0x8dc6888()*0.100376);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8448() {
   return (neuron0x8dc6a88()*0.200598);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8470() {
   return (neuron0x8dc6c88()*0.0327713);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8498() {
   return (neuron0x8dc6eb0()*0.0750778);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd84c0() {
   return (neuron0x8dc70b0()*-0.394734);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd84e8() {
   return (neuron0x8dc72b0()*0.145416);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8510() {
   return (neuron0x8dc74b0()*-0.230591);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8538() {
   return (neuron0x8dc76b0()*0.169644);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8560() {
   return (neuron0x8dc78b0()*0.0925662);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8588() {
   return (neuron0x8dc7ac8()*-0.196137);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd85b0() {
   return (neuron0x8dc7ce0()*-0.0392296);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd85d8() {
   return (neuron0x8dc7ef8()*-0.266312);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8600() {
   return (neuron0x8dc8110()*-0.0321793);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce860() {
   return (neuron0x8dc6480()*0.364139);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce888() {
   return (neuron0x8dc6660()*0.327064);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce8b0() {
   return (neuron0x8dc6888()*0.243937);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce8d8() {
   return (neuron0x8dc6a88()*-0.207659);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce900() {
   return (neuron0x8dc6c88()*0.145014);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dce928() {
   return (neuron0x8dc6eb0()*0.0616037);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8a48() {
   return (neuron0x8dc70b0()*-0.401651);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8a70() {
   return (neuron0x8dc72b0()*0.103154);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8a98() {
   return (neuron0x8dc74b0()*0.0809126);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8ac0() {
   return (neuron0x8dc76b0()*0.0785846);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8ae8() {
   return (neuron0x8dc78b0()*-0.201104);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8b10() {
   return (neuron0x8dc7ac8()*-0.323557);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8b38() {
   return (neuron0x8dc7ce0()*-0.00601071);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8b60() {
   return (neuron0x8dc7ef8()*-0.472314);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8b88() {
   return (neuron0x8dc8110()*0.417016);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8db0() {
   return (neuron0x8dc6480()*-0.281396);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8dd8() {
   return (neuron0x8dc6660()*-0.368232);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8e00() {
   return (neuron0x8dc6888()*-0.0124139);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8e28() {
   return (neuron0x8dc6a88()*-0.00296097);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8e50() {
   return (neuron0x8dc6c88()*-0.171801);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8e78() {
   return (neuron0x8dc6eb0()*-0.0298302);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8ea0() {
   return (neuron0x8dc70b0()*0.306627);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8ec8() {
   return (neuron0x8dc72b0()*0.204447);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8ef0() {
   return (neuron0x8dc74b0()*0.0879722);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8f18() {
   return (neuron0x8dc76b0()*0.126427);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8f40() {
   return (neuron0x8dc78b0()*0.0349075);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8f68() {
   return (neuron0x8dc7ac8()*0.13948);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8f90() {
   return (neuron0x8dc7ce0()*-0.289934);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8fb8() {
   return (neuron0x8dc7ef8()*0.491221);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd8fe0() {
   return (neuron0x8dc8110()*0.206485);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9220() {
   return (neuron0x8dc6480()*0.0806028);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9248() {
   return (neuron0x8dc6660()*0.182562);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9270() {
   return (neuron0x8dc6888()*-0.240315);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9298() {
   return (neuron0x8dc6a88()*0.030595);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd92c0() {
   return (neuron0x8dc6c88()*-0.314312);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd92e8() {
   return (neuron0x8dc6eb0()*0.301489);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9310() {
   return (neuron0x8dc70b0()*0.144689);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9338() {
   return (neuron0x8dc72b0()*0.198762);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9360() {
   return (neuron0x8dc74b0()*0.337111);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9388() {
   return (neuron0x8dc76b0()*0.315013);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd93b0() {
   return (neuron0x8dc78b0()*-0.428767);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd93d8() {
   return (neuron0x8dc7ac8()*0.390844);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9400() {
   return (neuron0x8dc7ce0()*0.340521);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9428() {
   return (neuron0x8dc7ef8()*-0.122947);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9450() {
   return (neuron0x8dc8110()*-0.283604);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9690() {
   return (neuron0x8dc6480()*-0.316783);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd96b8() {
   return (neuron0x8dc6660()*0.392248);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd96e0() {
   return (neuron0x8dc6888()*-0.195591);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9708() {
   return (neuron0x8dc6a88()*0.167315);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9730() {
   return (neuron0x8dc6c88()*0.0912466);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9758() {
   return (neuron0x8dc6eb0()*0.232862);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9780() {
   return (neuron0x8dc70b0()*-0.0896815);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd97a8() {
   return (neuron0x8dc72b0()*0.0928048);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd97d0() {
   return (neuron0x8dc74b0()*0.368167);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd97f8() {
   return (neuron0x8dc76b0()*-0.131385);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9820() {
   return (neuron0x8dc78b0()*0.147569);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9848() {
   return (neuron0x8dc7ac8()*0.308295);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9870() {
   return (neuron0x8dc7ce0()*-0.20602);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9898() {
   return (neuron0x8dc7ef8()*-0.0647511);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd98c0() {
   return (neuron0x8dc8110()*-0.102778);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9b00() {
   return (neuron0x8dc6480()*-0.236262);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9b28() {
   return (neuron0x8dc6660()*-0.37669);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9b50() {
   return (neuron0x8dc6888()*0.0146482);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9b78() {
   return (neuron0x8dc6a88()*-0.174879);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9ba0() {
   return (neuron0x8dc6c88()*-0.120449);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9bc8() {
   return (neuron0x8dc6eb0()*-0.00405369);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9bf0() {
   return (neuron0x8dc70b0()*0.0495838);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9c18() {
   return (neuron0x8dc72b0()*0.112577);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9c40() {
   return (neuron0x8dc74b0()*0.3224);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9c68() {
   return (neuron0x8dc76b0()*0.0416927);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9c90() {
   return (neuron0x8dc78b0()*-0.322394);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9cb8() {
   return (neuron0x8dc7ac8()*-0.431995);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9ce0() {
   return (neuron0x8dc7ce0()*-0.281958);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9d08() {
   return (neuron0x8dc7ef8()*-0.336749);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9d30() {
   return (neuron0x8dc8110()*0.415045);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9f70() {
   return (neuron0x8dc6480()*0.529862);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9f98() {
   return (neuron0x8dc6660()*-0.099289);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9fc0() {
   return (neuron0x8dc6888()*-0.138509);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dd9fe8() {
   return (neuron0x8dc6a88()*0.467016);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda010() {
   return (neuron0x8dc6c88()*0.129819);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda038() {
   return (neuron0x8dc6eb0()*0.335942);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda060() {
   return (neuron0x8dc70b0()*-0.326375);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda088() {
   return (neuron0x8dc72b0()*-0.132796);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda0b0() {
   return (neuron0x8dc74b0()*-0.0946396);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda0d8() {
   return (neuron0x8dc76b0()*-0.325042);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda100() {
   return (neuron0x8dc78b0()*0.248273);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda128() {
   return (neuron0x8dc7ac8()*0.0085017);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda150() {
   return (neuron0x8dc7ce0()*-0.141242);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda178() {
   return (neuron0x8dc7ef8()*-0.141924);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda1a0() {
   return (neuron0x8dc8110()*-0.273849);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda3e0() {
   return (neuron0x8dc6480()*-0.398999);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda408() {
   return (neuron0x8dc6660()*-0.00737499);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda430() {
   return (neuron0x8dc6888()*-0.239977);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda458() {
   return (neuron0x8dc6a88()*-0.328693);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda480() {
   return (neuron0x8dc6c88()*-0.210951);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda4a8() {
   return (neuron0x8dc6eb0()*-0.213062);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda4d0() {
   return (neuron0x8dc70b0()*-0.241535);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda4f8() {
   return (neuron0x8dc72b0()*0.403137);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda520() {
   return (neuron0x8dc74b0()*-0.30776);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda548() {
   return (neuron0x8dc76b0()*0.27667);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda570() {
   return (neuron0x8dc78b0()*-0.166509);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda598() {
   return (neuron0x8dc7ac8()*0.0669837);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda5c0() {
   return (neuron0x8dc7ce0()*0.305508);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda5e8() {
   return (neuron0x8dc7ef8()*-0.0857906);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda610() {
   return (neuron0x8dc8110()*0.182821);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda850() {
   return (neuron0x8dc6480()*-0.113053);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda878() {
   return (neuron0x8dc6660()*0.202122);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda8a0() {
   return (neuron0x8dc6888()*0.334306);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda8c8() {
   return (neuron0x8dc6a88()*0.220743);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda8f0() {
   return (neuron0x8dc6c88()*0.352887);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda918() {
   return (neuron0x8dc6eb0()*0.302038);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda940() {
   return (neuron0x8dc70b0()*0.450259);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda968() {
   return (neuron0x8dc72b0()*-0.324265);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda990() {
   return (neuron0x8dc74b0()*-0.299695);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda9b8() {
   return (neuron0x8dc76b0()*-0.352989);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8dda9e0() {
   return (neuron0x8dc78b0()*0.0832145);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddaa08() {
   return (neuron0x8dc7ac8()*0.276557);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddaa30() {
   return (neuron0x8dc7ce0()*0.144428);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddaa58() {
   return (neuron0x8dc7ef8()*0.150346);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddaa80() {
   return (neuron0x8dc8110()*-0.414854);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddacc0() {
   return (neuron0x8dc6480()*-0.123231);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddace8() {
   return (neuron0x8dc6660()*0.148015);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddad10() {
   return (neuron0x8dc6888()*0.259655);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddad38() {
   return (neuron0x8dc6a88()*0.403015);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddad60() {
   return (neuron0x8dc6c88()*0.371697);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddad88() {
   return (neuron0x8dc6eb0()*-0.293244);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddadb0() {
   return (neuron0x8dc70b0()*-0.166257);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddadd8() {
   return (neuron0x8dc72b0()*0.223898);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddae00() {
   return (neuron0x8dc74b0()*-0.110776);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddae28() {
   return (neuron0x8dc76b0()*-0.43468);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddae50() {
   return (neuron0x8dc78b0()*0.433128);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddae78() {
   return (neuron0x8dc7ac8()*0.04898);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddaea0() {
   return (neuron0x8dc7ce0()*-0.444743);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddaec8() {
   return (neuron0x8dc7ef8()*-0.10338);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddaef0() {
   return (neuron0x8dc8110()*0.14969);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb130() {
   return (neuron0x8dc6480()*-0.473714);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb158() {
   return (neuron0x8dc6660()*0.174012);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb180() {
   return (neuron0x8dc6888()*0.452254);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb1a8() {
   return (neuron0x8dc6a88()*-0.118402);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb1d0() {
   return (neuron0x8dc6c88()*0.017565);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb1f8() {
   return (neuron0x8dc6eb0()*0.0240323);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb220() {
   return (neuron0x8dc70b0()*-0.0349345);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb248() {
   return (neuron0x8dc72b0()*-0.209198);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb270() {
   return (neuron0x8dc74b0()*-0.291398);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb298() {
   return (neuron0x8dc76b0()*-0.0307321);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb2c0() {
   return (neuron0x8dc78b0()*0.0773958);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb2e8() {
   return (neuron0x8dc7ac8()*-0.210748);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb310() {
   return (neuron0x8dc7ce0()*-0.0121425);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb338() {
   return (neuron0x8dc7ef8()*-0.0925852);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb360() {
   return (neuron0x8dc8110()*-0.172074);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb4b0() {
   return (neuron0x8dc8450()*0.00547227);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb4d8() {
   return (neuron0x8dc87c0()*-0.00483618);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb500() {
   return (neuron0x8dc8d28()*0.00466848);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb528() {
   return (neuron0x8dc9078()*0.00839423);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb550() {
   return (neuron0x8dc94d0()*0.00287838);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb578() {
   return (neuron0x8dc99a0()*-0.153994);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb5a0() {
   return (neuron0x8dc9e10()*-0.0045264);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb5c8() {
   return (neuron0x8dca280()*-0.00882339);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb5f0() {
   return (neuron0x8dca6f0()*0.0112914);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb618() {
   return (neuron0x8dc9798()*-0.0136043);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb640() {
   return (neuron0x8dcb008()*-0.00237837);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb668() {
   return (neuron0x8dcb460()*0.000834933);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb690() {
   return (neuron0x8dcb8d0()*0.00719691);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb6b8() {
   return (neuron0x8dcbd40()*-0.00132688);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb6e0() {
   return (neuron0x8dcd3a0()*0.313407);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb708() {
   return (neuron0x8dcd768()*-0.00384824);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb7b8() {
   return (neuron0x8dce3b8()*0.00264299);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb7e0() {
   return (neuron0x8dce738()*-0.00267493);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb808() {
   return (neuron0x8dcaae8()*-0.00455774);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb830() {
   return (neuron0x8dcf260()*0.00449087);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb858() {
   return (neuron0x8dcf6b8()*0.0031189);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb880() {
   return (neuron0x8dcfb10()*0.00598721);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb8a8() {
   return (neuron0x8dcff68()*0.0948647);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb8d0() {
   return (neuron0x8dd03c0()*-0.0109897);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb8f8() {
   return (neuron0x8dd0818()*0.00716275);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb920() {
   return (neuron0x8dd0c70()*-0.00191723);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb948() {
   return (neuron0x8dd10c8()*-0.00260772);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb970() {
   return (neuron0x8dd1520()*-0.000743831);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb998() {
   return (neuron0x8dd1978()*-0.0115758);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb9c0() {
   return (neuron0x8dd1dd0()*-0.00893051);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb9e8() {
   return (neuron0x8dd2228()*0.00231358);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddba10() {
   return (neuron0x8dd2680()*0.00192496);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb730() {
   return (neuron0x8dd3820()*-0.0327666);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb758() {
   return (neuron0x8dd3948()*-0.00201296);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddb780() {
   return (neuron0x8dd3c28()*-0.000225136);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbb40() {
   return (neuron0x8dceb48()*0.000862128);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbb68() {
   return (neuron0x8dcefa0()*0.0289545);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbb90() {
   return (neuron0x8dd5148()*0.0904809);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbbb8() {
   return (neuron0x8dd55a0()*0.0218404);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbbe0() {
   return (neuron0x8dd59f8()*0.0783208);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbc08() {
   return (neuron0x8dd5e50()*0.0125486);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbc30() {
   return (neuron0x8dd62a8()*-0.00952857);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbc58() {
   return (neuron0x8dd6718()*0.00309176);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbc80() {
   return (neuron0x8dd6b88()*-0.0230443);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbca8() {
   return (neuron0x8dd6ff8()*-0.0127669);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbcd0() {
   return (neuron0x8dd7468()*0.00285886);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbcf8() {
   return (neuron0x8dd78d8()*0.230164);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbd20() {
   return (neuron0x8dd7d48()*-0.0269868);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbd48() {
   return (neuron0x8dd81b8()*-0.0173041);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbd70() {
   return (neuron0x8dd8628()*0.000309577);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbd98() {
   return (neuron0x8dd8bb0()*-0.0218683);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbdc0() {
   return (neuron0x8dd9008()*-0.00360104);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbde8() {
   return (neuron0x8dd9478()*-0.0254108);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbe10() {
   return (neuron0x8dd98e8()*0.0210766);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbe38() {
   return (neuron0x8dd9d58()*-0.00264157);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbe60() {
   return (neuron0x8dda1c8()*0.00939868);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbe88() {
   return (neuron0x8dda638()*-0.0034846);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbeb0() {
   return (neuron0x8ddaaa8()*-0.00626322);
}

double NNPerpElectronAngleCorrectionBCPDirTheta::synapse0x8ddbed8() {
   return (neuron0x8ddaf18()*0.12284);
}

