#include "NNPerpPositronXYCorrectionClusterDeltaY.h"
#include <cmath>

double NNPerpPositronXYCorrectionClusterDeltaY::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8) {
   input0 = (in0 - 1674.66)/716.24;
   input1 = (in1 - -4.27309)/34.3691;
   input2 = (in2 - 9.67386)/60.444;
   input3 = (in3 - 1.3363)/0.381166;
   input4 = (in4 - 1.46325)/0.384923;
   input5 = (in5 - 0.114001)/10.7812;
   input6 = (in6 - 0.235447)/11.8782;
   input7 = (in7 - -2.34564)/849.298;
   input8 = (in8 - -0.871867)/680.621;
   switch(index) {
     case 0:
         return neuron0x93ee1f8();
     default:
         return 0.;
   }
}

double NNPerpPositronXYCorrectionClusterDeltaY::Value(int index, double* input) {
   input0 = (input[0] - 1674.66)/716.24;
   input1 = (input[1] - -4.27309)/34.3691;
   input2 = (input[2] - 9.67386)/60.444;
   input3 = (input[3] - 1.3363)/0.381166;
   input4 = (input[4] - 1.46325)/0.384923;
   input5 = (input[5] - 0.114001)/10.7812;
   input6 = (input[6] - 0.235447)/11.8782;
   input7 = (input[7] - -2.34564)/849.298;
   input8 = (input[8] - -0.871867)/680.621;
   switch(index) {
     case 0:
         return neuron0x93ee1f8();
     default:
         return 0.;
   }
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93da2b0() {
   return input0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93da468() {
   return input1;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93da668() {
   return input2;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93da868() {
   return input3;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93e8da0() {
   return input4;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93e8f10() {
   return input5;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93e9110() {
   return input6;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93e9310() {
   return input7;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93e9510() {
   return input8;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93e9838() {
   double input = -2.68495;
   input += synapse0x91ab610();
   input += synapse0x93e99f0();
   input += synapse0x93e9a18();
   input += synapse0x93e9a40();
   input += synapse0x93e9a68();
   input += synapse0x93e9a90();
   input += synapse0x93e9ab8();
   input += synapse0x93e9ae0();
   input += synapse0x93e9b08();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93e9838() {
   double input = input0x93e9838();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93e9b30() {
   double input = 0.724063;
   input += synapse0x93e9d30();
   input += synapse0x93e9d58();
   input += synapse0x93e9d80();
   input += synapse0x93e9da8();
   input += synapse0x93e9dd0();
   input += synapse0x93e9df8();
   input += synapse0x93e9e20();
   input += synapse0x93e9e48();
   input += synapse0x93e9ef8();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93e9b30() {
   double input = input0x93e9b30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93e9f20() {
   double input = -6.70278;
   input += synapse0x93ea0d8();
   input += synapse0x93ea100();
   input += synapse0x93ea128();
   input += synapse0x93ea150();
   input += synapse0x93ea178();
   input += synapse0x93ea1a0();
   input += synapse0x93ea1c8();
   input += synapse0x93ea1f0();
   input += synapse0x93ea218();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93e9f20() {
   double input = input0x93e9f20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ea240() {
   double input = -25.9321;
   input += synapse0x93ea440();
   input += synapse0x93ea468();
   input += synapse0x93ea490();
   input += synapse0x93ea4b8();
   input += synapse0x93ea4e0();
   input += synapse0x93ea508();
   input += synapse0x93e9e70();
   input += synapse0x93e9e98();
   input += synapse0x93e9ec0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ea240() {
   double input = input0x93ea240();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ea638() {
   double input = -7.94179;
   input += synapse0x93ea838();
   input += synapse0x93ea860();
   input += synapse0x93ea888();
   input += synapse0x93ea8b0();
   input += synapse0x93ea8d8();
   input += synapse0x93ea900();
   input += synapse0x93ea928();
   input += synapse0x93ea950();
   input += synapse0x93ea978();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ea638() {
   double input = input0x93ea638();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ea9a0() {
   double input = -19.2823;
   input += synapse0x93eaba0();
   input += synapse0x93eabc8();
   input += synapse0x93eabf0();
   input += synapse0x93eac18();
   input += synapse0x93eac40();
   input += synapse0x93eac68();
   input += synapse0x93eac90();
   input += synapse0x93eacb8();
   input += synapse0x93eace0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ea9a0() {
   double input = input0x93ea9a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ead08() {
   double input = -1.74846;
   input += synapse0x93eaf08();
   input += synapse0x93eaf30();
   input += synapse0x93eaf58();
   input += synapse0x93eaf80();
   input += synapse0x93eafa8();
   input += synapse0x93eafd0();
   input += synapse0x93eaff8();
   input += synapse0x93eb020();
   input += synapse0x93eb048();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ead08() {
   double input = input0x93ead08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93eb070() {
   double input = -0.542419;
   input += synapse0x93eb2f8();
   input += synapse0x93eb320();
   input += synapse0x91ab460();
   input += synapse0x91aaf28();
   input += synapse0x918af20();
   input += synapse0x918af48();
   input += synapse0x93ea530();
   input += synapse0x93ea558();
   input += synapse0x93ea580();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93eb070() {
   double input = input0x93eb070();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93eb550() {
   double input = -4.11398;
   input += synapse0x93eb6c0();
   input += synapse0x93eb6e8();
   input += synapse0x93eb710();
   input += synapse0x93eb738();
   input += synapse0x93eb760();
   input += synapse0x93eb788();
   input += synapse0x93eb7b0();
   input += synapse0x93eb7d8();
   input += synapse0x93eb800();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93eb550() {
   double input = input0x93eb550();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93eb828() {
   double input = 6.01925;
   input += synapse0x93eba28();
   input += synapse0x93eba50();
   input += synapse0x93eba78();
   input += synapse0x93ebaa0();
   input += synapse0x93ebac8();
   input += synapse0x93ebaf0();
   input += synapse0x93ebb18();
   input += synapse0x93ebb40();
   input += synapse0x93ebb68();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93eb828() {
   double input = input0x93eb828();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ebb90() {
   double input = 0.673687;
   input += synapse0x93ebd90();
   input += synapse0x93ebdb8();
   input += synapse0x93ebde0();
   input += synapse0x93ebe08();
   input += synapse0x93ebe30();
   input += synapse0x93ebe58();
   input += synapse0x93ebe80();
   input += synapse0x93ebea8();
   input += synapse0x93ebed0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ebb90() {
   double input = input0x93ebb90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ebef8() {
   double input = 0.0513408;
   input += synapse0x93ec0f8();
   input += synapse0x93ec120();
   input += synapse0x93ec148();
   input += synapse0x93ec170();
   input += synapse0x93ec198();
   input += synapse0x93ec1c0();
   input += synapse0x93ec1e8();
   input += synapse0x93ec210();
   input += synapse0x93ec238();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ebef8() {
   double input = input0x93ebef8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ec260() {
   double input = 23.9183;
   input += synapse0x93ec460();
   input += synapse0x93ec488();
   input += synapse0x93ec4b0();
   input += synapse0x93ec4d8();
   input += synapse0x93ec500();
   input += synapse0x93ec528();
   input += synapse0x93ec550();
   input += synapse0x93ec578();
   input += synapse0x93ec5a0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ec260() {
   double input = input0x93ec260();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ec5c8() {
   double input = 0.615414;
   input += synapse0x93ec7c8();
   input += synapse0x93ec7f0();
   input += synapse0x93ec818();
   input += synapse0x93ec840();
   input += synapse0x93ec868();
   input += synapse0x93ec890();
   input += synapse0x93ec8b8();
   input += synapse0x93ec8e0();
   input += synapse0x93ec908();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ec5c8() {
   double input = input0x93ec5c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ec930() {
   double input = -1.89572;
   input += synapse0x93ecb30();
   input += synapse0x93ecb58();
   input += synapse0x93ecb80();
   input += synapse0x93eb348();
   input += synapse0x93eb370();
   input += synapse0x93eb398();
   input += synapse0x93eb3c0();
   input += synapse0x93eb3e8();
   input += synapse0x93eb410();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ec930() {
   double input = input0x93ec930();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ecfb0() {
   double input = -5.07223;
   input += synapse0x93eb510();
   input += synapse0x93ed160();
   input += synapse0x93ed210();
   input += synapse0x93ed2c0();
   input += synapse0x93ed370();
   input += synapse0x93ed420();
   input += synapse0x93ed4d0();
   input += synapse0x93ed580();
   input += synapse0x93ed630();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ecfb0() {
   double input = input0x93ecfb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ed6e0() {
   double input = 0.0682831;
   input += synapse0x93ed808();
   input += synapse0x93ed830();
   input += synapse0x93ed858();
   input += synapse0x93ed880();
   input += synapse0x93ed8a8();
   input += synapse0x93ed8d0();
   input += synapse0x93ed8f8();
   input += synapse0x93ed920();
   input += synapse0x93ed948();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ed6e0() {
   double input = input0x93ed6e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ed970() {
   double input = -16.7455;
   input += synapse0x93eda98();
   input += synapse0x93edac0();
   input += synapse0x93edae8();
   input += synapse0x93edb10();
   input += synapse0x93edb38();
   input += synapse0x93edb60();
   input += synapse0x93edb88();
   input += synapse0x93edbb0();
   input += synapse0x93edbd8();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ed970() {
   double input = input0x93ed970();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93edc00() {
   double input = -12.1979;
   input += synapse0x93edd28();
   input += synapse0x93edd50();
   input += synapse0x93edd78();
   input += synapse0x93edda0();
   input += synapse0x93eddc8();
   input += synapse0x93eddf0();
   input += synapse0x93ede18();
   input += synapse0x93ede40();
   input += synapse0x93ede68();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93edc00() {
   double input = input0x93edc00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ede90() {
   double input = -3.69062;
   input += synapse0x93ee090();
   input += synapse0x93ee0b8();
   input += synapse0x93ee0e0();
   input += synapse0x93ee108();
   input += synapse0x93ee130();
   input += synapse0x93ee158();
   input += synapse0x93ee180();
   input += synapse0x93ee1a8();
   input += synapse0x93ee1d0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ede90() {
   double input = input0x93ede90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::input0x93ee1f8() {
   double input = -2.21869;
   input += synapse0x93e9810();
   input += synapse0x93ee320();
   input += synapse0x93ee348();
   input += synapse0x93ee370();
   input += synapse0x93ee398();
   input += synapse0x93ee3c0();
   input += synapse0x93ee3e8();
   input += synapse0x93ee410();
   input += synapse0x93ee438();
   input += synapse0x93ee460();
   input += synapse0x93ee488();
   input += synapse0x93ee4b0();
   input += synapse0x93ee4d8();
   input += synapse0x93ee500();
   input += synapse0x93ee528();
   input += synapse0x93ee550();
   input += synapse0x93ee600();
   input += synapse0x93ee628();
   input += synapse0x93ee650();
   input += synapse0x93ee678();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaY::neuron0x93ee1f8() {
   double input = input0x93ee1f8();
   return (input * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x91ab610() {
   return (neuron0x93da2b0()*-4.5523);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e99f0() {
   return (neuron0x93da468()*-1.04256);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9a18() {
   return (neuron0x93da668()*-3.20375);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9a40() {
   return (neuron0x93da868()*0.242101);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9a68() {
   return (neuron0x93e8da0()*-5.56129);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9a90() {
   return (neuron0x93e8f10()*-1.16181);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9ab8() {
   return (neuron0x93e9110()*-0.666174);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9ae0() {
   return (neuron0x93e9310()*-0.646413);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9b08() {
   return (neuron0x93e9510()*0.776862);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9d30() {
   return (neuron0x93da2b0()*3.36218);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9d58() {
   return (neuron0x93da468()*3.24932);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9d80() {
   return (neuron0x93da668()*4.82639);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9da8() {
   return (neuron0x93da868()*-1.31973);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9dd0() {
   return (neuron0x93e8da0()*0.203342);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9df8() {
   return (neuron0x93e8f10()*2.91556);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9e20() {
   return (neuron0x93e9110()*4.1033);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9e48() {
   return (neuron0x93e9310()*0.135225);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9ef8() {
   return (neuron0x93e9510()*0.368314);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea0d8() {
   return (neuron0x93da2b0()*-4.98588);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea100() {
   return (neuron0x93da468()*-0.248764);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea128() {
   return (neuron0x93da668()*-0.491817);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea150() {
   return (neuron0x93da868()*-0.132491);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea178() {
   return (neuron0x93e8da0()*1.79092);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea1a0() {
   return (neuron0x93e8f10()*0.0677435);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea1c8() {
   return (neuron0x93e9110()*-2.71803);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea1f0() {
   return (neuron0x93e9310()*0.180706);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea218() {
   return (neuron0x93e9510()*0.333206);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea440() {
   return (neuron0x93da2b0()*-17.2177);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea468() {
   return (neuron0x93da468()*-2.82378);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea490() {
   return (neuron0x93da668()*0.200079);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea4b8() {
   return (neuron0x93da868()*-4.24225);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea4e0() {
   return (neuron0x93e8da0()*6.36563);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea508() {
   return (neuron0x93e8f10()*-5.60619);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9e70() {
   return (neuron0x93e9110()*-13.1051);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9e98() {
   return (neuron0x93e9310()*3.20138);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9ec0() {
   return (neuron0x93e9510()*0.0917413);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea838() {
   return (neuron0x93da2b0()*-2.52476);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea860() {
   return (neuron0x93da468()*0.289911);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea888() {
   return (neuron0x93da668()*-2.46706);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea8b0() {
   return (neuron0x93da868()*-2.1402);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea8d8() {
   return (neuron0x93e8da0()*-0.641604);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea900() {
   return (neuron0x93e8f10()*0.897031);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea928() {
   return (neuron0x93e9110()*-1.41643);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea950() {
   return (neuron0x93e9310()*7.89134);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea978() {
   return (neuron0x93e9510()*-1.63292);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eaba0() {
   return (neuron0x93da2b0()*-4.83206);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eabc8() {
   return (neuron0x93da468()*0.490334);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eabf0() {
   return (neuron0x93da668()*-6.30984);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eac18() {
   return (neuron0x93da868()*-5.58082);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eac40() {
   return (neuron0x93e8da0()*-2.06209);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eac68() {
   return (neuron0x93e8f10()*3.54936);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eac90() {
   return (neuron0x93e9110()*-4.12529);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eacb8() {
   return (neuron0x93e9310()*-0.190634);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eace0() {
   return (neuron0x93e9510()*2.14141);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eaf08() {
   return (neuron0x93da2b0()*1.6393);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eaf30() {
   return (neuron0x93da468()*-0.522227);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eaf58() {
   return (neuron0x93da668()*1.843);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eaf80() {
   return (neuron0x93da868()*-0.477138);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eafa8() {
   return (neuron0x93e8da0()*-3.12555);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eafd0() {
   return (neuron0x93e8f10()*-4.61931);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eaff8() {
   return (neuron0x93e9110()*-1.44811);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb020() {
   return (neuron0x93e9310()*0.820184);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb048() {
   return (neuron0x93e9510()*1.10195);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb2f8() {
   return (neuron0x93da2b0()*-2.37688);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb320() {
   return (neuron0x93da468()*-2.18926);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x91ab460() {
   return (neuron0x93da668()*-3.70681);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x91aaf28() {
   return (neuron0x93da868()*1.38896);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x918af20() {
   return (neuron0x93e8da0()*-0.257699);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x918af48() {
   return (neuron0x93e8f10()*-7.79832);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea530() {
   return (neuron0x93e9110()*-2.13355);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea558() {
   return (neuron0x93e9310()*0.0726609);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ea580() {
   return (neuron0x93e9510()*1.02251);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb6c0() {
   return (neuron0x93da2b0()*-0.00360429);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb6e8() {
   return (neuron0x93da468()*-3.86128);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb710() {
   return (neuron0x93da668()*-2.22683);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb738() {
   return (neuron0x93da868()*0.83739);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb760() {
   return (neuron0x93e8da0()*0.68499);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb788() {
   return (neuron0x93e8f10()*-0.330851);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb7b0() {
   return (neuron0x93e9110()*-0.516619);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb7d8() {
   return (neuron0x93e9310()*0.948879);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb800() {
   return (neuron0x93e9510()*1.24017);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eba28() {
   return (neuron0x93da2b0()*6.91127);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eba50() {
   return (neuron0x93da468()*-0.737449);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eba78() {
   return (neuron0x93da668()*0.0829047);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebaa0() {
   return (neuron0x93da868()*-0.326749);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebac8() {
   return (neuron0x93e8da0()*2.95944);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebaf0() {
   return (neuron0x93e8f10()*-3.06335);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebb18() {
   return (neuron0x93e9110()*-5.24269);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebb40() {
   return (neuron0x93e9310()*1.02288);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebb68() {
   return (neuron0x93e9510()*-0.182275);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebd90() {
   return (neuron0x93da2b0()*1.21705);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebdb8() {
   return (neuron0x93da468()*1.4029);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebde0() {
   return (neuron0x93da668()*2.38244);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebe08() {
   return (neuron0x93da868()*-0.916231);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebe30() {
   return (neuron0x93e8da0()*0.350542);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebe58() {
   return (neuron0x93e8f10()*7.81217);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebe80() {
   return (neuron0x93e9110()*-1.23555);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebea8() {
   return (neuron0x93e9310()*4.8794);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ebed0() {
   return (neuron0x93e9510()*0.112875);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec0f8() {
   return (neuron0x93da2b0()*0.606044);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec120() {
   return (neuron0x93da468()*-0.108397);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec148() {
   return (neuron0x93da668()*-0.250229);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec170() {
   return (neuron0x93da868()*-1.19075);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec198() {
   return (neuron0x93e8da0()*-0.152958);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec1c0() {
   return (neuron0x93e8f10()*1.33943);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec1e8() {
   return (neuron0x93e9110()*4.09898);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec210() {
   return (neuron0x93e9310()*-0.774519);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec238() {
   return (neuron0x93e9510()*1.82924);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec460() {
   return (neuron0x93da2b0()*15.4339);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec488() {
   return (neuron0x93da468()*2.39966);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec4b0() {
   return (neuron0x93da668()*-0.576772);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec4d8() {
   return (neuron0x93da868()*3.61312);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec500() {
   return (neuron0x93e8da0()*-6.24749);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec528() {
   return (neuron0x93e8f10()*4.33957);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec550() {
   return (neuron0x93e9110()*11.9389);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec578() {
   return (neuron0x93e9310()*0.360144);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec5a0() {
   return (neuron0x93e9510()*-1.39532);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec7c8() {
   return (neuron0x93da2b0()*-0.766234);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec7f0() {
   return (neuron0x93da468()*0.69515);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec818() {
   return (neuron0x93da668()*-0.314899);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec840() {
   return (neuron0x93da868()*0.397208);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec868() {
   return (neuron0x93e8da0()*-1.34529);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec890() {
   return (neuron0x93e8f10()*0.368315);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec8b8() {
   return (neuron0x93e9110()*-3.32635);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec8e0() {
   return (neuron0x93e9310()*0.378978);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ec908() {
   return (neuron0x93e9510()*0.263813);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ecb30() {
   return (neuron0x93da2b0()*-4.00199);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ecb58() {
   return (neuron0x93da468()*-0.64953);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ecb80() {
   return (neuron0x93da668()*-2.37693);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb348() {
   return (neuron0x93da868()*0.121357);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb370() {
   return (neuron0x93e8da0()*-4.46041);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb398() {
   return (neuron0x93e8f10()*-3.29249);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb3c0() {
   return (neuron0x93e9110()*-0.85363);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb3e8() {
   return (neuron0x93e9310()*2.4649);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb410() {
   return (neuron0x93e9510()*-0.371517);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eb510() {
   return (neuron0x93da2b0()*-2.161);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed160() {
   return (neuron0x93da468()*-4.70579);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed210() {
   return (neuron0x93da668()*-4.18916);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed2c0() {
   return (neuron0x93da868()*0.180089);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed370() {
   return (neuron0x93e8da0()*0.393901);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed420() {
   return (neuron0x93e8f10()*1.81843);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed4d0() {
   return (neuron0x93e9110()*-1.18386);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed580() {
   return (neuron0x93e9310()*0.468642);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed630() {
   return (neuron0x93e9510()*1.42812);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed808() {
   return (neuron0x93da2b0()*0.303096);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed830() {
   return (neuron0x93da468()*-0.250501);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed858() {
   return (neuron0x93da668()*0.380739);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed880() {
   return (neuron0x93da868()*-0.317163);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed8a8() {
   return (neuron0x93e8da0()*0.412008);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed8d0() {
   return (neuron0x93e8f10()*0.222536);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed8f8() {
   return (neuron0x93e9110()*0.801863);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed920() {
   return (neuron0x93e9310()*-0.159002);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ed948() {
   return (neuron0x93e9510()*-0.549996);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eda98() {
   return (neuron0x93da2b0()*-3.40309);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edac0() {
   return (neuron0x93da468()*-1.70702);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edae8() {
   return (neuron0x93da668()*-4.06316);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edb10() {
   return (neuron0x93da868()*2.85117);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edb38() {
   return (neuron0x93e8da0()*-8.46927);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edb60() {
   return (neuron0x93e8f10()*1.56701);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edb88() {
   return (neuron0x93e9110()*5.34195);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edbb0() {
   return (neuron0x93e9310()*0.645643);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edbd8() {
   return (neuron0x93e9510()*0.923255);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edd28() {
   return (neuron0x93da2b0()*-1.74531);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edd50() {
   return (neuron0x93da468()*-0.87692);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edd78() {
   return (neuron0x93da668()*-2.55647);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93edda0() {
   return (neuron0x93da868()*2.17028);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eddc8() {
   return (neuron0x93e8da0()*-6.04477);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93eddf0() {
   return (neuron0x93e8f10()*-0.643149);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ede18() {
   return (neuron0x93e9110()*5.15733);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ede40() {
   return (neuron0x93e9310()*4.07014);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ede68() {
   return (neuron0x93e9510()*4.31551);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee090() {
   return (neuron0x93da2b0()*-1.03996);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee0b8() {
   return (neuron0x93da468()*-3.4838);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee0e0() {
   return (neuron0x93da668()*-2.73105);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee108() {
   return (neuron0x93da868()*0.418904);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee130() {
   return (neuron0x93e8da0()*0.35522);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee158() {
   return (neuron0x93e8f10()*0.33985);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee180() {
   return (neuron0x93e9110()*-1.29637);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee1a8() {
   return (neuron0x93e9310()*0.692485);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee1d0() {
   return (neuron0x93e9510()*-2.18336);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93e9810() {
   return (neuron0x93e9838()*3.60022);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee320() {
   return (neuron0x93e9b30()*-4.12393);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee348() {
   return (neuron0x93e9f20()*3.92585);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee370() {
   return (neuron0x93ea240()*17.0586);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee398() {
   return (neuron0x93ea638()*-10.4492);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee3c0() {
   return (neuron0x93ea9a0()*11.6708);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee3e8() {
   return (neuron0x93ead08()*-1.17882);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee410() {
   return (neuron0x93eb070()*-7.42761);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee438() {
   return (neuron0x93eb550()*7.37033);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee460() {
   return (neuron0x93eb828()*0.478351);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee488() {
   return (neuron0x93ebb90()*-6.2286);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee4b0() {
   return (neuron0x93ebef8()*3.65877);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee4d8() {
   return (neuron0x93ec260()*19.0274);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee500() {
   return (neuron0x93ec5c8()*-3.72728);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee528() {
   return (neuron0x93ec930()*-3.6726);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee550() {
   return (neuron0x93ecfb0()*10.6867);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee600() {
   return (neuron0x93ed6e0()*-12.8085);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee628() {
   return (neuron0x93ed970()*10.9026);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee650() {
   return (neuron0x93edc00()*-13.592);
}

double NNPerpPositronXYCorrectionClusterDeltaY::synapse0x93ee678() {
   return (neuron0x93ede90()*-20.153);
}

