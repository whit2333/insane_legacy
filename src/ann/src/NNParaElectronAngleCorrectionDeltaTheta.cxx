#include "NNParaElectronAngleCorrectionDeltaTheta.h"
#include <cmath>

double NNParaElectronAngleCorrectionDeltaTheta::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - 0.707086)/0.0954631;
   input1 = (in1 - -0.00717654)/0.27641;
   input2 = (in2 - -3.34888)/32.7675;
   input3 = (in3 - -1.60046)/60.7523;
   input4 = (in4 - 2198.21)/971.743;
   input5 = (in5 - 0.00360118)/0.748592;
   input6 = (in6 - -0.0026409)/0.753729;
   switch(index) {
     case 0:
         return neuron0x8c396d0();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionDeltaTheta::Value(int index, double* input) {
   input0 = (input[0] - 0.707086)/0.0954631;
   input1 = (input[1] - -0.00717654)/0.27641;
   input2 = (input[2] - -3.34888)/32.7675;
   input3 = (input[3] - -1.60046)/60.7523;
   input4 = (input[4] - 2198.21)/971.743;
   input5 = (input[5] - 0.00360118)/0.748592;
   input6 = (input[6] - -0.0026409)/0.753729;
   switch(index) {
     case 0:
         return neuron0x8c396d0();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8bfe1e8() {
   return input0;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1e138() {
   return input1;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1e360() {
   return input2;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1e578() {
   return input3;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1e790() {
   return input4;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1e9b8() {
   return input5;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1ebb8() {
   return input6;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c1eef8() {
   double input = -0.379461;
   input += synapse0x8af6410();
   input += synapse0x8af6438();
   input += synapse0x8af64a8();
   input += synapse0x8af63e8();
   input += synapse0x8c1f0b0();
   input += synapse0x8c1f0d8();
   input += synapse0x8c1f100();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1eef8() {
   double input = input0x8c1eef8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c1f128() {
   double input = 0.420179;
   input += synapse0x8c1f328();
   input += synapse0x8c1f350();
   input += synapse0x8c1f378();
   input += synapse0x8c1f3a0();
   input += synapse0x8c1f3c8();
   input += synapse0x8c1f3f0();
   input += synapse0x8c1f418();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1f128() {
   double input = input0x8c1f128();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c1f440() {
   double input = -0.00340861;
   input += synapse0x8c1f640();
   input += synapse0x8c1f668();
   input += synapse0x8c1f690();
   input += synapse0x8c1f6b8();
   input += synapse0x8c1f6e0();
   input += synapse0x8c1f708();
   input += synapse0x8c1f730();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1f440() {
   double input = input0x8c1f440();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c1f758() {
   double input = 0.301051;
   input += synapse0x8c1f910();
   input += synapse0x8c1f938();
   input += synapse0x8c1f960();
   input += synapse0x8c1f988();
   input += synapse0x8c1f9b0();
   input += synapse0x8c1f9d8();
   input += synapse0x8c1fa00();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1f758() {
   double input = input0x8c1f758();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c1fa28() {
   double input = -0.0230381;
   input += synapse0x8c1fc40();
   input += synapse0x8c1fc68();
   input += synapse0x8c1fc90();
   input += synapse0x8c1fcb8();
   input += synapse0x8c1fce0();
   input += synapse0x8bd88f8();
   input += synapse0x8bfe3c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1fa28() {
   double input = input0x8c1fa28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c1fe10() {
   double input = -0.0369649;
   input += synapse0x8c20010();
   input += synapse0x8c20038();
   input += synapse0x8c20060();
   input += synapse0x8c20088();
   input += synapse0x8c200b0();
   input += synapse0x8c200d8();
   input += synapse0x8c20100();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c1fe10() {
   double input = input0x8c1fe10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c20128() {
   double input = 0.350031;
   input += synapse0x8c20340();
   input += synapse0x8c20368();
   input += synapse0x8c20390();
   input += synapse0x8c203b8();
   input += synapse0x8c203e0();
   input += synapse0x8c20408();
   input += synapse0x8c20430();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c20128() {
   double input = input0x8c20128();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c20458() {
   double input = 0.122122;
   input += synapse0x8c20670();
   input += synapse0x8c20698();
   input += synapse0x8c206c0();
   input += synapse0x8c206e8();
   input += synapse0x8c20710();
   input += synapse0x8c20738();
   input += synapse0x8c20760();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c20458() {
   double input = input0x8c20458();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c20788() {
   double input = 0.0557364;
   input += synapse0x8c209a0();
   input += synapse0x8c209c8();
   input += synapse0x8c209f0();
   input += synapse0x8c20a18();
   input += synapse0x8c20a40();
   input += synapse0x8c20a68();
   input += synapse0x8c20a90();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c20788() {
   double input = input0x8c20788();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c20ab8() {
   double input = 0.0590207;
   input += synapse0x8c20cd0();
   input += synapse0x8c20cf8();
   input += synapse0x8bfe3f0();
   input += synapse0x8af6368();
   input += synapse0x8ba2b58();
   input += synapse0x8c1fd08();
   input += synapse0x8c1fd30();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c20ab8() {
   double input = input0x8c20ab8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c20f28() {
   double input = -0.550224;
   input += synapse0x8c21140();
   input += synapse0x8c21168();
   input += synapse0x8c21190();
   input += synapse0x8c211b8();
   input += synapse0x8c211e0();
   input += synapse0x8c21208();
   input += synapse0x8c21230();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c20f28() {
   double input = input0x8c20f28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c21258() {
   double input = -0.301272;
   input += synapse0x8c21470();
   input += synapse0x8c21498();
   input += synapse0x8c214c0();
   input += synapse0x8c214e8();
   input += synapse0x8c21510();
   input += synapse0x8c21538();
   input += synapse0x8c21560();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c21258() {
   double input = input0x8c21258();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c21588() {
   double input = 0.0773137;
   input += synapse0x8c217a0();
   input += synapse0x8c217c8();
   input += synapse0x8c217f0();
   input += synapse0x8c21818();
   input += synapse0x8c21840();
   input += synapse0x8c21868();
   input += synapse0x8c21890();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c21588() {
   double input = input0x8c21588();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c218b8() {
   double input = -0.159685;
   input += synapse0x8c21ad0();
   input += synapse0x8c21af8();
   input += synapse0x8c21b20();
   input += synapse0x8c21b48();
   input += synapse0x8c21b70();
   input += synapse0x8c21b98();
   input += synapse0x8c21bc0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c218b8() {
   double input = input0x8c218b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c21be8() {
   double input = 0.272617;
   input += synapse0x8c21e00();
   input += synapse0x8c21e28();
   input += synapse0x8c21e50();
   input += synapse0x8c21e78();
   input += synapse0x8c21ea0();
   input += synapse0x8c21ec8();
   input += synapse0x8c21ef0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c21be8() {
   double input = input0x8c21be8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c21f18() {
   double input = -0.0330929;
   input += synapse0x8c22130();
   input += synapse0x8c221e0();
   input += synapse0x8c22290();
   input += synapse0x8c22340();
   input += synapse0x8c223f0();
   input += synapse0x8c224a0();
   input += synapse0x8c22550();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c21f18() {
   double input = input0x8c21f18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c22600() {
   double input = -0.467988;
   input += synapse0x8c22740();
   input += synapse0x8c22768();
   input += synapse0x8c22790();
   input += synapse0x8c227b8();
   input += synapse0x8c227e0();
   input += synapse0x8c22808();
   input += synapse0x8c22830();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c22600() {
   double input = input0x8c22600();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c22858() {
   double input = 0.112121;
   input += synapse0x8c22998();
   input += synapse0x8c229c0();
   input += synapse0x8c229e8();
   input += synapse0x8c22a10();
   input += synapse0x8c22a38();
   input += synapse0x8c22a60();
   input += synapse0x8c22a88();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c22858() {
   double input = input0x8c22858();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c22ab0() {
   double input = -0.32797;
   input += synapse0x8c22c80();
   input += synapse0x8c22ca8();
   input += synapse0x8c22cd0();
   input += synapse0x8c1fd58();
   input += synapse0x8c1fd80();
   input += synapse0x8c1fda8();
   input += synapse0x8c1fdd0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c22ab0() {
   double input = input0x8c22ab0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c20d20() {
   double input = -0.00260241;
   input += synapse0x8bfe0b0();
   input += synapse0x8bfe0d8();
   input += synapse0x8bfe100();
   input += synapse0x8bfe128();
   input += synapse0x8bfe150();
   input += synapse0x8bfe178();
   input += synapse0x8c23100();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c20d20() {
   double input = input0x8c20d20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c23128() {
   double input = -0.419628;
   input += synapse0x8c23340();
   input += synapse0x8c23368();
   input += synapse0x8c23390();
   input += synapse0x8c233b8();
   input += synapse0x8c233e0();
   input += synapse0x8c23408();
   input += synapse0x8c23430();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c23128() {
   double input = input0x8c23128();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c23458() {
   double input = 0.207307;
   input += synapse0x8c23670();
   input += synapse0x8c23698();
   input += synapse0x8c236c0();
   input += synapse0x8c236e8();
   input += synapse0x8c23710();
   input += synapse0x8c23738();
   input += synapse0x8c23760();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c23458() {
   double input = input0x8c23458();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c23788() {
   double input = -0.295567;
   input += synapse0x8c239a0();
   input += synapse0x8c239c8();
   input += synapse0x8c239f0();
   input += synapse0x8c23a18();
   input += synapse0x8c23a40();
   input += synapse0x8c23a68();
   input += synapse0x8c23a90();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c23788() {
   double input = input0x8c23788();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c23ab8() {
   double input = 0.412191;
   input += synapse0x8c23cd0();
   input += synapse0x8c23cf8();
   input += synapse0x8c23d20();
   input += synapse0x8c23d48();
   input += synapse0x8c23d70();
   input += synapse0x8c23d98();
   input += synapse0x8c23dc0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c23ab8() {
   double input = input0x8c23ab8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c24eb0() {
   double input = -0.0611436;
   input += synapse0x8c23ec0();
   input += synapse0x8c24fd8();
   input += synapse0x8c25000();
   input += synapse0x8c25028();
   input += synapse0x8c25050();
   input += synapse0x8c25078();
   input += synapse0x8c250a0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c24eb0() {
   double input = input0x8c24eb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c250c8() {
   double input = -0.136874;
   input += synapse0x8857780();
   input += synapse0x88577a8();
   input += synapse0x88577d0();
   input += synapse0x8c253d0();
   input += synapse0x8c253f8();
   input += synapse0x8c25420();
   input += synapse0x8c25448();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c250c8() {
   double input = input0x8c250c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c25470() {
   double input = 0.387658;
   input += synapse0x8c25670();
   input += synapse0x8c25698();
   input += synapse0x8c256c0();
   input += synapse0x8c256e8();
   input += synapse0x8c25710();
   input += synapse0x8c25738();
   input += synapse0x8c25760();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c25470() {
   double input = input0x8c25470();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c25788() {
   double input = -0.0779795;
   input += synapse0x8c25988();
   input += synapse0x8c259b0();
   input += synapse0x8c259d8();
   input += synapse0x8c25a00();
   input += synapse0x8c25a28();
   input += synapse0x8c25a50();
   input += synapse0x8c25a78();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c25788() {
   double input = input0x8c25788();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c25aa0() {
   double input = 0.120997;
   input += synapse0x8c25ca0();
   input += synapse0x8c25cc8();
   input += synapse0x8c25cf0();
   input += synapse0x8c25d18();
   input += synapse0x8c25d40();
   input += synapse0x8c25d68();
   input += synapse0x8c25d90();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c25aa0() {
   double input = input0x8c25aa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c25db8() {
   double input = 0.0649743;
   input += synapse0x8c25fb8();
   input += synapse0x8c25fe0();
   input += synapse0x8c26008();
   input += synapse0x8c26030();
   input += synapse0x8c26058();
   input += synapse0x8c26080();
   input += synapse0x8c260a8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c25db8() {
   double input = input0x8c25db8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c260d0() {
   double input = -0.569567;
   input += synapse0x8c262d0();
   input += synapse0x8c262f8();
   input += synapse0x8c26320();
   input += synapse0x8c26348();
   input += synapse0x8c26370();
   input += synapse0x8c26398();
   input += synapse0x8c263c0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c260d0() {
   double input = input0x8c260d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c263e8() {
   double input = -0.47206;
   input += synapse0x8c265e8();
   input += synapse0x8c22158();
   input += synapse0x8c22180();
   input += synapse0x8c221a8();
   input += synapse0x8c22208();
   input += synapse0x8c22230();
   input += synapse0x8c22258();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c263e8() {
   double input = input0x8c263e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c26d48() {
   double input = -0.0946028;
   input += synapse0x8c22460();
   input += synapse0x8c22300();
   input += synapse0x8c223b0();
   input += synapse0x8c224c8();
   input += synapse0x8c224f0();
   input += synapse0x8c22518();
   input += synapse0x8c22578();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c26d48() {
   double input = input0x8c26d48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c26e70() {
   double input = -0.0670072;
   input += synapse0x8c27028();
   input += synapse0x8c27050();
   input += synapse0x8c27078();
   input += synapse0x8c270a0();
   input += synapse0x8c270c8();
   input += synapse0x8c270f0();
   input += synapse0x8c27118();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c26e70() {
   double input = input0x8c26e70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c27140() {
   double input = 0.0616207;
   input += synapse0x8c27340();
   input += synapse0x8c27368();
   input += synapse0x8c27390();
   input += synapse0x8c273b8();
   input += synapse0x8c273e0();
   input += synapse0x8c27408();
   input += synapse0x8c27430();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c27140() {
   double input = input0x8c27140();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c27458() {
   double input = 0.431804;
   input += synapse0x8c27658();
   input += synapse0x8c27680();
   input += synapse0x8c276a8();
   input += synapse0x8c276d0();
   input += synapse0x8c276f8();
   input += synapse0x8c27720();
   input += synapse0x8c27748();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c27458() {
   double input = input0x8c27458();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c27770() {
   double input = 0.294025;
   input += synapse0x8c27970();
   input += synapse0x8c27998();
   input += synapse0x8c279c0();
   input += synapse0x8c279e8();
   input += synapse0x8c27a10();
   input += synapse0x8c22cf8();
   input += synapse0x8c22d20();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c27770() {
   double input = input0x8c27770();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c22d48() {
   double input = 0.422719;
   input += synapse0x8c22f60();
   input += synapse0x8c22f88();
   input += synapse0x8c22fb0();
   input += synapse0x8c22fd8();
   input += synapse0x8c23000();
   input += synapse0x8c23028();
   input += synapse0x8c23050();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c22d48() {
   double input = input0x8c22d48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c28240() {
   double input = 0.313396;
   input += synapse0x8c230c0();
   input += synapse0x8c283f8();
   input += synapse0x8c28420();
   input += synapse0x8c28448();
   input += synapse0x8c28470();
   input += synapse0x8c28498();
   input += synapse0x8c284c0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c28240() {
   double input = input0x8c28240();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c284e8() {
   double input = -0.432772;
   input += synapse0x8c286e8();
   input += synapse0x8c28710();
   input += synapse0x8c28738();
   input += synapse0x8c28760();
   input += synapse0x8c28788();
   input += synapse0x8c287b0();
   input += synapse0x8c287d8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c284e8() {
   double input = input0x8c284e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c28800() {
   double input = -0.122213;
   input += synapse0x8c28a00();
   input += synapse0x8c28a28();
   input += synapse0x8c28a50();
   input += synapse0x8c28a78();
   input += synapse0x8c28aa0();
   input += synapse0x8c28ac8();
   input += synapse0x8c28af0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c28800() {
   double input = input0x8c28800();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c28b18() {
   double input = 0.145328;
   input += synapse0x8c28d18();
   input += synapse0x8c28d40();
   input += synapse0x8c28d68();
   input += synapse0x8c28d90();
   input += synapse0x8c28db8();
   input += synapse0x8c28de0();
   input += synapse0x8c28e08();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c28b18() {
   double input = input0x8c28b18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c28e30() {
   double input = -0.38343;
   input += synapse0x8c29030();
   input += synapse0x8c29058();
   input += synapse0x8c29080();
   input += synapse0x8c290a8();
   input += synapse0x8c290d0();
   input += synapse0x8c290f8();
   input += synapse0x8c29120();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c28e30() {
   double input = input0x8c28e30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c29148() {
   double input = -0.0979904;
   input += synapse0x8c29360();
   input += synapse0x8c29388();
   input += synapse0x8c293b0();
   input += synapse0x8c293d8();
   input += synapse0x8c29400();
   input += synapse0x8c29428();
   input += synapse0x8c29450();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c29148() {
   double input = input0x8c29148();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c29478() {
   double input = 0.0707432;
   input += synapse0x8c29690();
   input += synapse0x8c296b8();
   input += synapse0x8c296e0();
   input += synapse0x8c29708();
   input += synapse0x8c29730();
   input += synapse0x8c29758();
   input += synapse0x8c29780();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c29478() {
   double input = input0x8c29478();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c297a8() {
   double input = 0.0899656;
   input += synapse0x8c299c0();
   input += synapse0x8c299e8();
   input += synapse0x8c29a10();
   input += synapse0x8c29a38();
   input += synapse0x8c29a60();
   input += synapse0x8c29a88();
   input += synapse0x8c29ab0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c297a8() {
   double input = input0x8c297a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c29ad8() {
   double input = -0.309709;
   input += synapse0x8c29cf0();
   input += synapse0x8c29d18();
   input += synapse0x8c29d40();
   input += synapse0x8c29d68();
   input += synapse0x8c29d90();
   input += synapse0x8c29db8();
   input += synapse0x8c29de0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c29ad8() {
   double input = input0x8c29ad8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c29e08() {
   double input = 0.324196;
   input += synapse0x8c2a020();
   input += synapse0x8c2a048();
   input += synapse0x8c2a070();
   input += synapse0x8c2a098();
   input += synapse0x8c2a0c0();
   input += synapse0x8c2a0e8();
   input += synapse0x8c2a110();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c29e08() {
   double input = input0x8c29e08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2a138() {
   double input = 0.450148;
   input += synapse0x8c2a350();
   input += synapse0x8c2a378();
   input += synapse0x8c2a3a0();
   input += synapse0x8c2a3c8();
   input += synapse0x8c2a3f0();
   input += synapse0x8c2a418();
   input += synapse0x8c2a440();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2a138() {
   double input = input0x8c2a138();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2a468() {
   double input = 0.0660841;
   input += synapse0x8c2a680();
   input += synapse0x8c2a6a8();
   input += synapse0x8c2a6d0();
   input += synapse0x8c2a6f8();
   input += synapse0x8c2a720();
   input += synapse0x8c2a748();
   input += synapse0x8c2a770();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2a468() {
   double input = input0x8c2a468();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2a798() {
   double input = -0.0321498;
   input += synapse0x8c2a9b0();
   input += synapse0x8c2a9d8();
   input += synapse0x8c2aa00();
   input += synapse0x8c2aa28();
   input += synapse0x8c2aa50();
   input += synapse0x8c2aa78();
   input += synapse0x8c2aaa0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2a798() {
   double input = input0x8c2a798();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2aac8() {
   double input = -0.0887282;
   input += synapse0x8c2ace0();
   input += synapse0x8c2ad08();
   input += synapse0x8c2ad30();
   input += synapse0x8c2ad58();
   input += synapse0x8c2ad80();
   input += synapse0x8c2ada8();
   input += synapse0x8c2add0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2aac8() {
   double input = input0x8c2aac8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2adf8() {
   double input = -0.182289;
   input += synapse0x8c2b010();
   input += synapse0x8c2b038();
   input += synapse0x8c2b060();
   input += synapse0x8c2b088();
   input += synapse0x8c2b0b0();
   input += synapse0x8c2b0d8();
   input += synapse0x8c2b100();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2adf8() {
   double input = input0x8c2adf8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2b128() {
   double input = 0.187457;
   input += synapse0x8c2b340();
   input += synapse0x8c2b368();
   input += synapse0x8c2b390();
   input += synapse0x8c2b3b8();
   input += synapse0x8c2b3e0();
   input += synapse0x8c2b408();
   input += synapse0x8c2b430();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2b128() {
   double input = input0x8c2b128();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2b458() {
   double input = -0.147176;
   input += synapse0x8c2b670();
   input += synapse0x8c2b698();
   input += synapse0x8c2b6c0();
   input += synapse0x8c2b6e8();
   input += synapse0x8c2b710();
   input += synapse0x8c2b738();
   input += synapse0x8c2b760();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2b458() {
   double input = input0x8c2b458();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2b788() {
   double input = -0.0811532;
   input += synapse0x8c2b9a0();
   input += synapse0x8c2b9c8();
   input += synapse0x8c2b9f0();
   input += synapse0x8c2ba18();
   input += synapse0x8c2ba40();
   input += synapse0x8c2ba68();
   input += synapse0x8c2ba90();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2b788() {
   double input = input0x8c2b788();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2bab8() {
   double input = -0.376095;
   input += synapse0x8c2bcd0();
   input += synapse0x8c2bcf8();
   input += synapse0x8c2bd20();
   input += synapse0x8c2bd48();
   input += synapse0x8c2bd70();
   input += synapse0x8c2bd98();
   input += synapse0x8c2bdc0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2bab8() {
   double input = input0x8c2bab8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2bde8() {
   double input = -0.0268072;
   input += synapse0x8c252c8();
   input += synapse0x8c252f0();
   input += synapse0x8c25318();
   input += synapse0x8c25340();
   input += synapse0x8c25368();
   input += synapse0x8c25390();
   input += synapse0x8c2c208();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2bde8() {
   double input = input0x8c2bde8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2c230() {
   double input = 0.338837;
   input += synapse0x8c2c430();
   input += synapse0x8c2c458();
   input += synapse0x8c2c480();
   input += synapse0x8c2c4a8();
   input += synapse0x8c2c4d0();
   input += synapse0x8c2c4f8();
   input += synapse0x8c2c520();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2c230() {
   double input = input0x8c2c230();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2c548() {
   double input = 0.0945279;
   input += synapse0x8c2c760();
   input += synapse0x8c2c788();
   input += synapse0x8c2c7b0();
   input += synapse0x8c2c7d8();
   input += synapse0x8c2c800();
   input += synapse0x8c2c828();
   input += synapse0x8c2c850();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2c548() {
   double input = input0x8c2c548();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2c878() {
   double input = 0.258853;
   input += synapse0x8c2ca90();
   input += synapse0x8c2cab8();
   input += synapse0x8c2cae0();
   input += synapse0x8c2cb08();
   input += synapse0x8c2cb30();
   input += synapse0x8c2cb58();
   input += synapse0x8c2cb80();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2c878() {
   double input = input0x8c2c878();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2cba8() {
   double input = -0.340542;
   input += synapse0x8c2cdc0();
   input += synapse0x8c2cde8();
   input += synapse0x8c2ce10();
   input += synapse0x8c2ce38();
   input += synapse0x8c2ce60();
   input += synapse0x8c2ce88();
   input += synapse0x8c2ceb0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2cba8() {
   double input = input0x8c2cba8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2ced8() {
   double input = 0.250704;
   input += synapse0x8c2d0f0();
   input += synapse0x8c2d118();
   input += synapse0x8c2d140();
   input += synapse0x8c2d168();
   input += synapse0x8c2d190();
   input += synapse0x8c2d1b8();
   input += synapse0x8c2d1e0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2ced8() {
   double input = input0x8c2ced8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2d208() {
   double input = 0.0628187;
   input += synapse0x8c2d420();
   input += synapse0x8c26610();
   input += synapse0x8c26638();
   input += synapse0x8c26660();
   input += synapse0x8c26890();
   input += synapse0x8c268b8();
   input += synapse0x8c26ae8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2d208() {
   double input = input0x8c2d208();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c26b10() {
   double input = 0.175481;
   input += synapse0x8c2de70();
   input += synapse0x8c2de98();
   input += synapse0x8c2dec0();
   input += synapse0x8c2dee8();
   input += synapse0x8c2df10();
   input += synapse0x8c2df38();
   input += synapse0x8c2df60();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c26b10() {
   double input = input0x8c26b10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2df88() {
   double input = 0.185461;
   input += synapse0x8c2e188();
   input += synapse0x8c2e1b0();
   input += synapse0x8c2e1d8();
   input += synapse0x8c2e200();
   input += synapse0x8c2e228();
   input += synapse0x8c2e250();
   input += synapse0x8c2e278();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2df88() {
   double input = input0x8c2df88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2e2a0() {
   double input = -0.158816;
   input += synapse0x8c2e4b8();
   input += synapse0x8c2e4e0();
   input += synapse0x8c2e508();
   input += synapse0x8c2e530();
   input += synapse0x8c2e558();
   input += synapse0x8c2e580();
   input += synapse0x8c2e5a8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2e2a0() {
   double input = input0x8c2e2a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2e5d0() {
   double input = -0.617161;
   input += synapse0x8c2e7e8();
   input += synapse0x8c2e810();
   input += synapse0x8c2e838();
   input += synapse0x8c2e860();
   input += synapse0x8c2e888();
   input += synapse0x8c2e8b0();
   input += synapse0x8c2e8d8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2e5d0() {
   double input = input0x8c2e5d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2e900() {
   double input = -0.454951;
   input += synapse0x8c2eb18();
   input += synapse0x8c2eb40();
   input += synapse0x8c2eb68();
   input += synapse0x8c2eb90();
   input += synapse0x8c2ebb8();
   input += synapse0x8c2ebe0();
   input += synapse0x8c2ec08();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2e900() {
   double input = input0x8c2e900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2ec30() {
   double input = -0.0173751;
   input += synapse0x8c2ee48();
   input += synapse0x8c2ee70();
   input += synapse0x8c2ee98();
   input += synapse0x8c2eec0();
   input += synapse0x8c2eee8();
   input += synapse0x8c2ef10();
   input += synapse0x8c2ef38();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2ec30() {
   double input = input0x8c2ec30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2ef60() {
   double input = 0.383031;
   input += synapse0x8c2f178();
   input += synapse0x8c2f1a0();
   input += synapse0x8c2f1c8();
   input += synapse0x8c2f1f0();
   input += synapse0x8c2f218();
   input += synapse0x8c2f240();
   input += synapse0x8c2f268();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2ef60() {
   double input = input0x8c2ef60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2f290() {
   double input = -0.132094;
   input += synapse0x8c2f4a8();
   input += synapse0x8c2f4d0();
   input += synapse0x8c2f4f8();
   input += synapse0x8c2f520();
   input += synapse0x8c2f548();
   input += synapse0x8c2f570();
   input += synapse0x8c2f598();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2f290() {
   double input = input0x8c2f290();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2f5c0() {
   double input = -0.270265;
   input += synapse0x8c2f7d8();
   input += synapse0x8c2f800();
   input += synapse0x8c2f828();
   input += synapse0x8c2f850();
   input += synapse0x8c2f878();
   input += synapse0x8c2f8a0();
   input += synapse0x8c2f8c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2f5c0() {
   double input = input0x8c2f5c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c2f8f0() {
   double input = 0.336114;
   input += synapse0x8c2fb08();
   input += synapse0x8c2fb30();
   input += synapse0x8c27a38();
   input += synapse0x8c27a60();
   input += synapse0x8c27a88();
   input += synapse0x8c27ab0();
   input += synapse0x8c27ad8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c2f8f0() {
   double input = input0x8c2f8f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c27b00() {
   double input = -0.0515654;
   input += synapse0x8c27d18();
   input += synapse0x8c27d40();
   input += synapse0x8c27d68();
   input += synapse0x8c27d90();
   input += synapse0x8c27db8();
   input += synapse0x8c27de0();
   input += synapse0x8c27e08();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c27b00() {
   double input = input0x8c27b00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c27e30() {
   double input = 0.203387;
   input += synapse0x8c28048();
   input += synapse0x8c28070();
   input += synapse0x8c28098();
   input += synapse0x8c280c0();
   input += synapse0x8c280e8();
   input += synapse0x8c28110();
   input += synapse0x8c28138();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c27e30() {
   double input = input0x8c27e30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c30b60() {
   double input = -0.538936;
   input += synapse0x8c28208();
   input += synapse0x8c30cd0();
   input += synapse0x8c30cf8();
   input += synapse0x8c30d20();
   input += synapse0x8c30d48();
   input += synapse0x8c30d70();
   input += synapse0x8c30d98();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c30b60() {
   double input = input0x8c30b60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c30dc0() {
   double input = 0.441332;
   input += synapse0x8c30fd8();
   input += synapse0x8c31000();
   input += synapse0x8c31028();
   input += synapse0x8c31050();
   input += synapse0x8c31078();
   input += synapse0x8c310a0();
   input += synapse0x8c310c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c30dc0() {
   double input = input0x8c30dc0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c310f0() {
   double input = 0.195094;
   input += synapse0x8c31308();
   input += synapse0x8c31330();
   input += synapse0x8c31358();
   input += synapse0x8c31380();
   input += synapse0x8c313a8();
   input += synapse0x8c313d0();
   input += synapse0x8c313f8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c310f0() {
   double input = input0x8c310f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c31420() {
   double input = -0.0769425;
   input += synapse0x8c31638();
   input += synapse0x8c31660();
   input += synapse0x8c31688();
   input += synapse0x8c316b0();
   input += synapse0x8c316d8();
   input += synapse0x8c31700();
   input += synapse0x8c31728();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c31420() {
   double input = input0x8c31420();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c31750() {
   double input = 0.00856923;
   input += synapse0x8c31968();
   input += synapse0x8c31990();
   input += synapse0x8c319b8();
   input += synapse0x8c319e0();
   input += synapse0x8c31a08();
   input += synapse0x8c31a30();
   input += synapse0x8c31a58();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c31750() {
   double input = input0x8c31750();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c31a80() {
   double input = 0.0404505;
   input += synapse0x8c31c98();
   input += synapse0x8c31cc0();
   input += synapse0x8c31ce8();
   input += synapse0x8c31d10();
   input += synapse0x8c31d38();
   input += synapse0x8c31d60();
   input += synapse0x8c31d88();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c31a80() {
   double input = input0x8c31a80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c31db0() {
   double input = 0.0870642;
   input += synapse0x8c31fc8();
   input += synapse0x8c31ff0();
   input += synapse0x8c32018();
   input += synapse0x8c32040();
   input += synapse0x8c32068();
   input += synapse0x8c32090();
   input += synapse0x8c320b8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c31db0() {
   double input = input0x8c31db0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c320e0() {
   double input = 0.0394958;
   input += synapse0x8c322f8();
   input += synapse0x8c32320();
   input += synapse0x8c32348();
   input += synapse0x8c32370();
   input += synapse0x8c32398();
   input += synapse0x8c323c0();
   input += synapse0x8c323e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c320e0() {
   double input = input0x8c320e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c32410() {
   double input = -0.147266;
   input += synapse0x8c32628();
   input += synapse0x8c32650();
   input += synapse0x8c32678();
   input += synapse0x8c326a0();
   input += synapse0x8c326c8();
   input += synapse0x8c326f0();
   input += synapse0x8c32718();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c32410() {
   double input = input0x8c32410();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c32740() {
   double input = 0.0191895;
   input += synapse0x8c32958();
   input += synapse0x8c32980();
   input += synapse0x8c329a8();
   input += synapse0x8c329d0();
   input += synapse0x8c329f8();
   input += synapse0x8c32a20();
   input += synapse0x8c32a48();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c32740() {
   double input = input0x8c32740();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c32a70() {
   double input = -0.435655;
   input += synapse0x8c32c88();
   input += synapse0x8c32cb0();
   input += synapse0x8c32cd8();
   input += synapse0x8c32d00();
   input += synapse0x8c32d28();
   input += synapse0x8c32d50();
   input += synapse0x8c32d78();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c32a70() {
   double input = input0x8c32a70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c32da0() {
   double input = -0.431235;
   input += synapse0x8c32fb8();
   input += synapse0x8c32fe0();
   input += synapse0x8c33008();
   input += synapse0x8c33030();
   input += synapse0x8c33058();
   input += synapse0x8c33080();
   input += synapse0x8c330a8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c32da0() {
   double input = input0x8c32da0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c330d0() {
   double input = -0.385548;
   input += synapse0x8c332e8();
   input += synapse0x8c33310();
   input += synapse0x8c33338();
   input += synapse0x8c33360();
   input += synapse0x8c33388();
   input += synapse0x8c333b0();
   input += synapse0x8c333d8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c330d0() {
   double input = input0x8c330d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c33400() {
   double input = 0.649705;
   input += synapse0x8c33618();
   input += synapse0x8c33640();
   input += synapse0x8c33668();
   input += synapse0x8c33690();
   input += synapse0x8c336b8();
   input += synapse0x8c336e0();
   input += synapse0x8c33708();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c33400() {
   double input = input0x8c33400();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c33730() {
   double input = -0.288881;
   input += synapse0x8c33948();
   input += synapse0x8c33970();
   input += synapse0x8c33998();
   input += synapse0x8c339c0();
   input += synapse0x8c339e8();
   input += synapse0x8c33a10();
   input += synapse0x8c33a38();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c33730() {
   double input = input0x8c33730();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c33a60() {
   double input = -0.0748676;
   input += synapse0x8c33c78();
   input += synapse0x8c33ca0();
   input += synapse0x8c33cc8();
   input += synapse0x8c33cf0();
   input += synapse0x8c33d18();
   input += synapse0x8c33d40();
   input += synapse0x8c33d68();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c33a60() {
   double input = input0x8c33a60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c33d90() {
   double input = -0.318272;
   input += synapse0x8c33fa8();
   input += synapse0x8c33fd0();
   input += synapse0x8c33ff8();
   input += synapse0x8c34020();
   input += synapse0x8c34048();
   input += synapse0x8c34070();
   input += synapse0x8c34098();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c33d90() {
   double input = input0x8c33d90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c340c0() {
   double input = -0.301063;
   input += synapse0x8c342d8();
   input += synapse0x8c34300();
   input += synapse0x8c34328();
   input += synapse0x8c34350();
   input += synapse0x8c34378();
   input += synapse0x8c343a0();
   input += synapse0x8c343c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c340c0() {
   double input = input0x8c340c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c343f0() {
   double input = -0.121465;
   input += synapse0x8c34608();
   input += synapse0x8c34630();
   input += synapse0x8c34658();
   input += synapse0x8c34680();
   input += synapse0x8c346a8();
   input += synapse0x8c346d0();
   input += synapse0x8c346f8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c343f0() {
   double input = input0x8c343f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c34720() {
   double input = -0.373949;
   input += synapse0x8c34938();
   input += synapse0x8c34960();
   input += synapse0x8c34988();
   input += synapse0x8c349b0();
   input += synapse0x8c349d8();
   input += synapse0x8c34a00();
   input += synapse0x8c34a28();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c34720() {
   double input = input0x8c34720();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c34a50() {
   double input = 0.438273;
   input += synapse0x8c34c68();
   input += synapse0x8c34c90();
   input += synapse0x8c34cb8();
   input += synapse0x8c34ce0();
   input += synapse0x8c34d08();
   input += synapse0x8c34d30();
   input += synapse0x8c34d58();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c34a50() {
   double input = input0x8c34a50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c34d80() {
   double input = 0.453578;
   input += synapse0x8c34f98();
   input += synapse0x8c34fc0();
   input += synapse0x8c34fe8();
   input += synapse0x8c35010();
   input += synapse0x8c35038();
   input += synapse0x8c35060();
   input += synapse0x8c35088();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c34d80() {
   double input = input0x8c34d80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c350b0() {
   double input = -0.385478;
   input += synapse0x8c352c8();
   input += synapse0x8c352f0();
   input += synapse0x8c35318();
   input += synapse0x8c35340();
   input += synapse0x8c35368();
   input += synapse0x8c35390();
   input += synapse0x8c353b8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c350b0() {
   double input = input0x8c350b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c353e0() {
   double input = -0.321437;
   input += synapse0x8c355f8();
   input += synapse0x8c35620();
   input += synapse0x8c35648();
   input += synapse0x8c35670();
   input += synapse0x8c35698();
   input += synapse0x8c356c0();
   input += synapse0x8c356e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c353e0() {
   double input = input0x8c353e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c35710() {
   double input = -0.285946;
   input += synapse0x8c35928();
   input += synapse0x8c35950();
   input += synapse0x8c35978();
   input += synapse0x8c359a0();
   input += synapse0x8c359c8();
   input += synapse0x8c359f0();
   input += synapse0x8c35a18();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c35710() {
   double input = input0x8c35710();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c35a40() {
   double input = 0.169864;
   input += synapse0x8c35c58();
   input += synapse0x8c35c80();
   input += synapse0x8c35ca8();
   input += synapse0x8c35cd0();
   input += synapse0x8c35cf8();
   input += synapse0x8c35d20();
   input += synapse0x8c35d48();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c35a40() {
   double input = input0x8c35a40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c35d70() {
   double input = -0.445564;
   input += synapse0x8c35f88();
   input += synapse0x8c35fb0();
   input += synapse0x8c35fd8();
   input += synapse0x8c36000();
   input += synapse0x8c36028();
   input += synapse0x8c36050();
   input += synapse0x8c36078();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c35d70() {
   double input = input0x8c35d70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c360a0() {
   double input = 0.277389;
   input += synapse0x8c362b8();
   input += synapse0x8c362e0();
   input += synapse0x8c36308();
   input += synapse0x8c36330();
   input += synapse0x8c36358();
   input += synapse0x8c36380();
   input += synapse0x8c363a8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c360a0() {
   double input = input0x8c360a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c363d0() {
   double input = -0.107981;
   input += synapse0x8c365e8();
   input += synapse0x8c36610();
   input += synapse0x8c36638();
   input += synapse0x8c36660();
   input += synapse0x8c36688();
   input += synapse0x8c366b0();
   input += synapse0x8c366d8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c363d0() {
   double input = input0x8c363d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c36700() {
   double input = -0.0572233;
   input += synapse0x8c36918();
   input += synapse0x8c36940();
   input += synapse0x8c36968();
   input += synapse0x8c36990();
   input += synapse0x8c369b8();
   input += synapse0x8c369e0();
   input += synapse0x8c36a08();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c36700() {
   double input = input0x8c36700();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c36a30() {
   double input = 0.0993885;
   input += synapse0x8c36c48();
   input += synapse0x8c36c70();
   input += synapse0x8c36c98();
   input += synapse0x8c36cc0();
   input += synapse0x8c36ce8();
   input += synapse0x8c36d10();
   input += synapse0x8c36d38();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c36a30() {
   double input = input0x8c36a30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c36d60() {
   double input = 0.160565;
   input += synapse0x8c36f78();
   input += synapse0x8c36fa0();
   input += synapse0x8c36fc8();
   input += synapse0x8c36ff0();
   input += synapse0x8c37018();
   input += synapse0x8c37040();
   input += synapse0x8c37068();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c36d60() {
   double input = input0x8c36d60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c37090() {
   double input = 0.00913645;
   input += synapse0x8c372a8();
   input += synapse0x8c372d0();
   input += synapse0x8c372f8();
   input += synapse0x8c37320();
   input += synapse0x8c37348();
   input += synapse0x8c37370();
   input += synapse0x8c37398();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c37090() {
   double input = input0x8c37090();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c373c0() {
   double input = 0.417467;
   input += synapse0x8c375d8();
   input += synapse0x8c37600();
   input += synapse0x8c37628();
   input += synapse0x8c37650();
   input += synapse0x8c37678();
   input += synapse0x8c376a0();
   input += synapse0x8c376c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c373c0() {
   double input = input0x8c373c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c376f0() {
   double input = 0.366768;
   input += synapse0x8c37908();
   input += synapse0x8c37930();
   input += synapse0x8c37958();
   input += synapse0x8c37980();
   input += synapse0x8c379a8();
   input += synapse0x8c379d0();
   input += synapse0x8c379f8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c376f0() {
   double input = input0x8c376f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c37a20() {
   double input = 0.266411;
   input += synapse0x8c37c38();
   input += synapse0x8c37c60();
   input += synapse0x8c37c88();
   input += synapse0x8c37cb0();
   input += synapse0x8c37cd8();
   input += synapse0x8c37d00();
   input += synapse0x8c37d28();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c37a20() {
   double input = input0x8c37a20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c37d50() {
   double input = 0.117852;
   input += synapse0x8c37f68();
   input += synapse0x8c37f90();
   input += synapse0x8c37fb8();
   input += synapse0x8c37fe0();
   input += synapse0x8c38008();
   input += synapse0x8c38030();
   input += synapse0x8c38058();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c37d50() {
   double input = input0x8c37d50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c38080() {
   double input = 0.4007;
   input += synapse0x8c38298();
   input += synapse0x8c382c0();
   input += synapse0x8c382e8();
   input += synapse0x8c38310();
   input += synapse0x8c38338();
   input += synapse0x8c38360();
   input += synapse0x8c38388();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c38080() {
   double input = input0x8c38080();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c383b0() {
   double input = -0.377962;
   input += synapse0x8c385c8();
   input += synapse0x8c385f0();
   input += synapse0x8c38618();
   input += synapse0x8c38640();
   input += synapse0x8c38668();
   input += synapse0x8c38690();
   input += synapse0x8c386b8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c383b0() {
   double input = input0x8c383b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c386e0() {
   double input = 0.00798652;
   input += synapse0x8c388f8();
   input += synapse0x8c38920();
   input += synapse0x8c38948();
   input += synapse0x8c38970();
   input += synapse0x8c38998();
   input += synapse0x8c389c0();
   input += synapse0x8c389e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c386e0() {
   double input = input0x8c386e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c38a10() {
   double input = -0.163581;
   input += synapse0x8c38c28();
   input += synapse0x8c38c50();
   input += synapse0x8c38c78();
   input += synapse0x8c38ca0();
   input += synapse0x8c38cc8();
   input += synapse0x8c38cf0();
   input += synapse0x8c38d18();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c38a10() {
   double input = input0x8c38a10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c38d40() {
   double input = 0.0991272;
   input += synapse0x8c38f58();
   input += synapse0x8c38f80();
   input += synapse0x8c38fa8();
   input += synapse0x8c38fd0();
   input += synapse0x8c38ff8();
   input += synapse0x8c39020();
   input += synapse0x8c39048();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c38d40() {
   double input = input0x8c38d40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c39070() {
   double input = 0.385182;
   input += synapse0x8c39288();
   input += synapse0x8c392b0();
   input += synapse0x8c392d8();
   input += synapse0x8c39300();
   input += synapse0x8c39328();
   input += synapse0x8c39350();
   input += synapse0x8c39378();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c39070() {
   double input = input0x8c39070();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c393a0() {
   double input = -0.193315;
   input += synapse0x8c395b8();
   input += synapse0x8c395e0();
   input += synapse0x8c39608();
   input += synapse0x8c39630();
   input += synapse0x8c39658();
   input += synapse0x8c39680();
   input += synapse0x8c396a8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c393a0() {
   double input = input0x8c393a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::input0x8c396d0() {
   double input = -0.00594424;
   input += synapse0x8c397f8();
   input += synapse0x8c39820();
   input += synapse0x8c39848();
   input += synapse0x8c39870();
   input += synapse0x8c39898();
   input += synapse0x8c398c0();
   input += synapse0x8c398e8();
   input += synapse0x8c39910();
   input += synapse0x8c39938();
   input += synapse0x8c39960();
   input += synapse0x8c39988();
   input += synapse0x8c399b0();
   input += synapse0x8c399d8();
   input += synapse0x8c39a00();
   input += synapse0x8c39a28();
   input += synapse0x8c39a50();
   input += synapse0x8c39b00();
   input += synapse0x8c39b28();
   input += synapse0x8c39b50();
   input += synapse0x8c39b78();
   input += synapse0x8c39ba0();
   input += synapse0x8c39bc8();
   input += synapse0x8c39bf0();
   input += synapse0x8c39c18();
   input += synapse0x8c39c40();
   input += synapse0x8c39c68();
   input += synapse0x8c39c90();
   input += synapse0x8c39cb8();
   input += synapse0x8c39ce0();
   input += synapse0x8c39d08();
   input += synapse0x8c39d30();
   input += synapse0x8c39d58();
   input += synapse0x8c39a78();
   input += synapse0x8c39aa0();
   input += synapse0x8c39ac8();
   input += synapse0x8c39e88();
   input += synapse0x8c39eb0();
   input += synapse0x8c39ed8();
   input += synapse0x8c39f00();
   input += synapse0x8c39f28();
   input += synapse0x8c39f50();
   input += synapse0x8c39f78();
   input += synapse0x8c39fa0();
   input += synapse0x8c39fc8();
   input += synapse0x8c39ff0();
   input += synapse0x8c3a018();
   input += synapse0x8c3a040();
   input += synapse0x8c3a068();
   input += synapse0x8c3a090();
   input += synapse0x8c3a0b8();
   input += synapse0x8c3a0e0();
   input += synapse0x8c3a108();
   input += synapse0x8c3a130();
   input += synapse0x8c3a158();
   input += synapse0x8c3a180();
   input += synapse0x8c3a1a8();
   input += synapse0x8c3a1d0();
   input += synapse0x8c3a1f8();
   input += synapse0x8c3a220();
   input += synapse0x8c3a248();
   input += synapse0x8c3a270();
   input += synapse0x8c3a298();
   input += synapse0x8c3a2c0();
   input += synapse0x8c3a2e8();
   input += synapse0x8c1ee10();
   input += synapse0x8c39d80();
   input += synapse0x8c39da8();
   input += synapse0x8c39dd0();
   input += synapse0x8c39df8();
   input += synapse0x8c39e20();
   input += synapse0x8c39e48();
   input += synapse0x8c3a518();
   input += synapse0x8c3a540();
   input += synapse0x8c3a568();
   input += synapse0x8c3a590();
   input += synapse0x8c3a5b8();
   input += synapse0x8c3a5e0();
   input += synapse0x8c3a608();
   input += synapse0x8c3a630();
   input += synapse0x8c3a658();
   input += synapse0x8c3a680();
   input += synapse0x8c3a6a8();
   input += synapse0x8c3a6d0();
   input += synapse0x8c3a6f8();
   input += synapse0x8c3a720();
   input += synapse0x8c3a748();
   input += synapse0x8c3a770();
   input += synapse0x8c3a798();
   input += synapse0x8c3a7c0();
   input += synapse0x8c3a7e8();
   input += synapse0x8c3a810();
   input += synapse0x8c3a838();
   input += synapse0x8c3a860();
   input += synapse0x8c3a888();
   input += synapse0x8c3a8b0();
   input += synapse0x8c3a8d8();
   input += synapse0x8c3a900();
   input += synapse0x8c3a928();
   input += synapse0x8c3a950();
   input += synapse0x8c3a978();
   input += synapse0x8c3a9a0();
   input += synapse0x8c3a9c8();
   input += synapse0x8c3a9f0();
   input += synapse0x8c3aa18();
   input += synapse0x8c3aa40();
   input += synapse0x8c3aa68();
   input += synapse0x8c3aa90();
   input += synapse0x8c3aab8();
   input += synapse0x8c3aae0();
   input += synapse0x8c3ab08();
   input += synapse0x8c3ab30();
   input += synapse0x8c3ab58();
   input += synapse0x8c3ab80();
   input += synapse0x8c3aba8();
   input += synapse0x8c3abd0();
   input += synapse0x8c3abf8();
   input += synapse0x8c3ac20();
   input += synapse0x8c3ac48();
   input += synapse0x8c3ac70();
   input += synapse0x8c3ac98();
   return input;
}

double NNParaElectronAngleCorrectionDeltaTheta::neuron0x8c396d0() {
   double input = input0x8c396d0();
   return (input * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8af6410() {
   return (neuron0x8bfe1e8()*0.340826);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8af6438() {
   return (neuron0x8c1e138()*-0.000256505);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8af64a8() {
   return (neuron0x8c1e360()*0.102014);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8af63e8() {
   return (neuron0x8c1e578()*-0.289228);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f0b0() {
   return (neuron0x8c1e790()*0.209098);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f0d8() {
   return (neuron0x8c1e9b8()*0.050427);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f100() {
   return (neuron0x8c1ebb8()*0.168294);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f328() {
   return (neuron0x8bfe1e8()*-0.0525797);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f350() {
   return (neuron0x8c1e138()*-0.043319);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f378() {
   return (neuron0x8c1e360()*0.225877);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f3a0() {
   return (neuron0x8c1e578()*-0.300721);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f3c8() {
   return (neuron0x8c1e790()*-0.0419401);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f3f0() {
   return (neuron0x8c1e9b8()*0.475478);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f418() {
   return (neuron0x8c1ebb8()*0.355022);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f640() {
   return (neuron0x8bfe1e8()*0.248281);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f668() {
   return (neuron0x8c1e138()*0.207134);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f690() {
   return (neuron0x8c1e360()*-0.441967);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f6b8() {
   return (neuron0x8c1e578()*0.229462);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f6e0() {
   return (neuron0x8c1e790()*0.148421);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f708() {
   return (neuron0x8c1e9b8()*-0.32027);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f730() {
   return (neuron0x8c1ebb8()*-0.112538);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f910() {
   return (neuron0x8bfe1e8()*0.257359);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f938() {
   return (neuron0x8c1e138()*-0.0311838);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f960() {
   return (neuron0x8c1e360()*0.0362111);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f988() {
   return (neuron0x8c1e578()*-0.263203);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f9b0() {
   return (neuron0x8c1e790()*0.0070439);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1f9d8() {
   return (neuron0x8c1e9b8()*-0.0419259);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fa00() {
   return (neuron0x8c1ebb8()*-0.317074);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fc40() {
   return (neuron0x8bfe1e8()*0.0893826);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fc68() {
   return (neuron0x8c1e138()*-0.297949);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fc90() {
   return (neuron0x8c1e360()*0.180936);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fcb8() {
   return (neuron0x8c1e578()*0.37546);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fce0() {
   return (neuron0x8c1e790()*-0.222538);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bd88f8() {
   return (neuron0x8c1e9b8()*-0.345817);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bfe3c8() {
   return (neuron0x8c1ebb8()*-0.100833);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20010() {
   return (neuron0x8bfe1e8()*0.00100078);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20038() {
   return (neuron0x8c1e138()*-0.255519);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20060() {
   return (neuron0x8c1e360()*0.243282);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20088() {
   return (neuron0x8c1e578()*0.221031);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c200b0() {
   return (neuron0x8c1e790()*0.273625);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c200d8() {
   return (neuron0x8c1e9b8()*0.323948);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20100() {
   return (neuron0x8c1ebb8()*0.181381);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20340() {
   return (neuron0x8bfe1e8()*-0.470574);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20368() {
   return (neuron0x8c1e138()*-0.43114);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20390() {
   return (neuron0x8c1e360()*0.230969);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c203b8() {
   return (neuron0x8c1e578()*-0.236333);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c203e0() {
   return (neuron0x8c1e790()*0.139823);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20408() {
   return (neuron0x8c1e9b8()*0.0529628);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20430() {
   return (neuron0x8c1ebb8()*-0.182786);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20670() {
   return (neuron0x8bfe1e8()*-0.145163);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20698() {
   return (neuron0x8c1e138()*0.0990661);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c206c0() {
   return (neuron0x8c1e360()*0.360388);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c206e8() {
   return (neuron0x8c1e578()*-0.000256785);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20710() {
   return (neuron0x8c1e790()*-0.102685);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20738() {
   return (neuron0x8c1e9b8()*0.152463);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20760() {
   return (neuron0x8c1ebb8()*-0.0707278);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c209a0() {
   return (neuron0x8bfe1e8()*-0.0358118);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c209c8() {
   return (neuron0x8c1e138()*-0.298884);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c209f0() {
   return (neuron0x8c1e360()*-0.414215);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20a18() {
   return (neuron0x8c1e578()*0.371364);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20a40() {
   return (neuron0x8c1e790()*0.446431);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20a68() {
   return (neuron0x8c1e9b8()*-0.281716);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20a90() {
   return (neuron0x8c1ebb8()*-0.126719);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20cd0() {
   return (neuron0x8bfe1e8()*0.382539);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c20cf8() {
   return (neuron0x8c1e138()*-0.227519);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bfe3f0() {
   return (neuron0x8c1e360()*-0.167776);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8af6368() {
   return (neuron0x8c1e578()*-0.354038);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8ba2b58() {
   return (neuron0x8c1e790()*0.0122878);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fd08() {
   return (neuron0x8c1e9b8()*0.0407163);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fd30() {
   return (neuron0x8c1ebb8()*0.374983);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21140() {
   return (neuron0x8bfe1e8()*0.148408);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21168() {
   return (neuron0x8c1e138()*-0.281911);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21190() {
   return (neuron0x8c1e360()*-0.247498);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c211b8() {
   return (neuron0x8c1e578()*0.440642);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c211e0() {
   return (neuron0x8c1e790()*-0.529197);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21208() {
   return (neuron0x8c1e9b8()*0.0139304);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21230() {
   return (neuron0x8c1ebb8()*0.0214634);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21470() {
   return (neuron0x8bfe1e8()*-0.0750075);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21498() {
   return (neuron0x8c1e138()*0.330022);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c214c0() {
   return (neuron0x8c1e360()*0.340496);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c214e8() {
   return (neuron0x8c1e578()*-0.426266);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21510() {
   return (neuron0x8c1e790()*0.415167);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21538() {
   return (neuron0x8c1e9b8()*-0.129874);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21560() {
   return (neuron0x8c1ebb8()*-0.223974);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c217a0() {
   return (neuron0x8bfe1e8()*0.348096);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c217c8() {
   return (neuron0x8c1e138()*-0.245523);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c217f0() {
   return (neuron0x8c1e360()*0.121443);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21818() {
   return (neuron0x8c1e578()*-0.0521779);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21840() {
   return (neuron0x8c1e790()*0.0149299);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21868() {
   return (neuron0x8c1e9b8()*0.301816);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21890() {
   return (neuron0x8c1ebb8()*-0.296583);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21ad0() {
   return (neuron0x8bfe1e8()*-0.370247);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21af8() {
   return (neuron0x8c1e138()*0.466738);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21b20() {
   return (neuron0x8c1e360()*0.0188326);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21b48() {
   return (neuron0x8c1e578()*-0.31016);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21b70() {
   return (neuron0x8c1e790()*-0.272011);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21b98() {
   return (neuron0x8c1e9b8()*0.399384);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21bc0() {
   return (neuron0x8c1ebb8()*-0.270207);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21e00() {
   return (neuron0x8bfe1e8()*0.144422);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21e28() {
   return (neuron0x8c1e138()*0.314675);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21e50() {
   return (neuron0x8c1e360()*-0.44339);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21e78() {
   return (neuron0x8c1e578()*0.243988);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21ea0() {
   return (neuron0x8c1e790()*-0.260237);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21ec8() {
   return (neuron0x8c1e9b8()*-0.0145116);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c21ef0() {
   return (neuron0x8c1ebb8()*-0.326917);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22130() {
   return (neuron0x8bfe1e8()*-0.374594);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c221e0() {
   return (neuron0x8c1e138()*-0.21794);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22290() {
   return (neuron0x8c1e360()*0.0333812);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22340() {
   return (neuron0x8c1e578()*0.14642);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c223f0() {
   return (neuron0x8c1e790()*0.336218);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c224a0() {
   return (neuron0x8c1e9b8()*0.416022);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22550() {
   return (neuron0x8c1ebb8()*-0.329956);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22740() {
   return (neuron0x8bfe1e8()*-0.254639);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22768() {
   return (neuron0x8c1e138()*-0.224345);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22790() {
   return (neuron0x8c1e360()*0.158975);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c227b8() {
   return (neuron0x8c1e578()*0.499887);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c227e0() {
   return (neuron0x8c1e790()*0.132855);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22808() {
   return (neuron0x8c1e9b8()*-0.359891);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22830() {
   return (neuron0x8c1ebb8()*0.411198);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22998() {
   return (neuron0x8bfe1e8()*0.350232);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c229c0() {
   return (neuron0x8c1e138()*-0.0844245);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c229e8() {
   return (neuron0x8c1e360()*-0.479158);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22a10() {
   return (neuron0x8c1e578()*-0.277032);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22a38() {
   return (neuron0x8c1e790()*0.463574);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22a60() {
   return (neuron0x8c1e9b8()*0.403837);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22a88() {
   return (neuron0x8c1ebb8()*0.177139);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22c80() {
   return (neuron0x8bfe1e8()*-0.202309);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22ca8() {
   return (neuron0x8c1e138()*-0.318613);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22cd0() {
   return (neuron0x8c1e360()*-0.192615);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fd58() {
   return (neuron0x8c1e578()*-0.0641713);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fd80() {
   return (neuron0x8c1e790()*0.285434);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fda8() {
   return (neuron0x8c1e9b8()*0.181712);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1fdd0() {
   return (neuron0x8c1ebb8()*0.184535);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bfe0b0() {
   return (neuron0x8bfe1e8()*0.426223);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bfe0d8() {
   return (neuron0x8c1e138()*0.131206);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bfe100() {
   return (neuron0x8c1e360()*-0.214284);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bfe128() {
   return (neuron0x8c1e578()*-0.154123);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bfe150() {
   return (neuron0x8c1e790()*-0.204826);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8bfe178() {
   return (neuron0x8c1e9b8()*0.27648);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23100() {
   return (neuron0x8c1ebb8()*0.380132);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23340() {
   return (neuron0x8bfe1e8()*-0.432441);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23368() {
   return (neuron0x8c1e138()*0.29642);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23390() {
   return (neuron0x8c1e360()*0.35613);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c233b8() {
   return (neuron0x8c1e578()*0.384563);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c233e0() {
   return (neuron0x8c1e790()*0.414783);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23408() {
   return (neuron0x8c1e9b8()*0.388734);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23430() {
   return (neuron0x8c1ebb8()*0.243981);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23670() {
   return (neuron0x8bfe1e8()*0.381835);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23698() {
   return (neuron0x8c1e138()*-0.327702);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c236c0() {
   return (neuron0x8c1e360()*-0.427728);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c236e8() {
   return (neuron0x8c1e578()*-0.133964);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23710() {
   return (neuron0x8c1e790()*0.476364);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23738() {
   return (neuron0x8c1e9b8()*-0.0631363);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23760() {
   return (neuron0x8c1ebb8()*-0.196332);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c239a0() {
   return (neuron0x8bfe1e8()*-0.189827);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c239c8() {
   return (neuron0x8c1e138()*-0.111359);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c239f0() {
   return (neuron0x8c1e360()*0.193162);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23a18() {
   return (neuron0x8c1e578()*-0.141364);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23a40() {
   return (neuron0x8c1e790()*-0.0464662);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23a68() {
   return (neuron0x8c1e9b8()*-0.206407);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23a90() {
   return (neuron0x8c1ebb8()*0.212654);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23cd0() {
   return (neuron0x8bfe1e8()*0.0579496);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23cf8() {
   return (neuron0x8c1e138()*0.178454);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23d20() {
   return (neuron0x8c1e360()*0.187796);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23d48() {
   return (neuron0x8c1e578()*-0.40089);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23d70() {
   return (neuron0x8c1e790()*-0.412193);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23d98() {
   return (neuron0x8c1e9b8()*0.0758838);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23dc0() {
   return (neuron0x8c1ebb8()*0.212608);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23ec0() {
   return (neuron0x8bfe1e8()*-0.322774);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c24fd8() {
   return (neuron0x8c1e138()*0.0722564);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25000() {
   return (neuron0x8c1e360()*0.00652717);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25028() {
   return (neuron0x8c1e578()*-0.434838);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25050() {
   return (neuron0x8c1e790()*-0.371057);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25078() {
   return (neuron0x8c1e9b8()*-0.394997);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c250a0() {
   return (neuron0x8c1ebb8()*0.413654);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8857780() {
   return (neuron0x8bfe1e8()*-0.463432);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x88577a8() {
   return (neuron0x8c1e138()*0.37735);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x88577d0() {
   return (neuron0x8c1e360()*0.0799029);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c253d0() {
   return (neuron0x8c1e578()*-0.309936);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c253f8() {
   return (neuron0x8c1e790()*0.0673724);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25420() {
   return (neuron0x8c1e9b8()*0.402515);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25448() {
   return (neuron0x8c1ebb8()*-0.291008);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25670() {
   return (neuron0x8bfe1e8()*-0.275575);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25698() {
   return (neuron0x8c1e138()*0.306711);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c256c0() {
   return (neuron0x8c1e360()*0.0457323);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c256e8() {
   return (neuron0x8c1e578()*-0.313604);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25710() {
   return (neuron0x8c1e790()*0.302716);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25738() {
   return (neuron0x8c1e9b8()*0.0209575);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25760() {
   return (neuron0x8c1ebb8()*-0.522599);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25988() {
   return (neuron0x8bfe1e8()*0.461907);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c259b0() {
   return (neuron0x8c1e138()*0.376586);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c259d8() {
   return (neuron0x8c1e360()*0.102787);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25a00() {
   return (neuron0x8c1e578()*0.356976);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25a28() {
   return (neuron0x8c1e790()*0.047431);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25a50() {
   return (neuron0x8c1e9b8()*0.295224);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25a78() {
   return (neuron0x8c1ebb8()*0.348611);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25ca0() {
   return (neuron0x8bfe1e8()*0.349122);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25cc8() {
   return (neuron0x8c1e138()*0.246052);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25cf0() {
   return (neuron0x8c1e360()*0.0224954);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25d18() {
   return (neuron0x8c1e578()*0.25816);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25d40() {
   return (neuron0x8c1e790()*0.173092);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25d68() {
   return (neuron0x8c1e9b8()*0.285532);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25d90() {
   return (neuron0x8c1ebb8()*-0.0392765);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25fb8() {
   return (neuron0x8bfe1e8()*0.365619);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25fe0() {
   return (neuron0x8c1e138()*0.346041);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26008() {
   return (neuron0x8c1e360()*-0.142048);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26030() {
   return (neuron0x8c1e578()*0.340445);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26058() {
   return (neuron0x8c1e790()*-0.0665163);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26080() {
   return (neuron0x8c1e9b8()*0.159602);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c260a8() {
   return (neuron0x8c1ebb8()*0.036896);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c262d0() {
   return (neuron0x8bfe1e8()*0.193774);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c262f8() {
   return (neuron0x8c1e138()*0.160738);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26320() {
   return (neuron0x8c1e360()*-0.241514);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26348() {
   return (neuron0x8c1e578()*-0.206504);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26370() {
   return (neuron0x8c1e790()*-0.456924);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26398() {
   return (neuron0x8c1e9b8()*0.0627446);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c263c0() {
   return (neuron0x8c1ebb8()*-0.0466039);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c265e8() {
   return (neuron0x8bfe1e8()*0.0958493);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22158() {
   return (neuron0x8c1e138()*-0.485214);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22180() {
   return (neuron0x8c1e360()*0.285013);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c221a8() {
   return (neuron0x8c1e578()*0.317574);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22208() {
   return (neuron0x8c1e790()*0.068153);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22230() {
   return (neuron0x8c1e9b8()*-0.19157);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22258() {
   return (neuron0x8c1ebb8()*-0.338511);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22460() {
   return (neuron0x8bfe1e8()*-0.287172);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22300() {
   return (neuron0x8c1e138()*0.34229);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c223b0() {
   return (neuron0x8c1e360()*0.283587);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c224c8() {
   return (neuron0x8c1e578()*-0.290595);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c224f0() {
   return (neuron0x8c1e790()*0.0236467);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22518() {
   return (neuron0x8c1e9b8()*-0.114942);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22578() {
   return (neuron0x8c1ebb8()*0.126636);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27028() {
   return (neuron0x8bfe1e8()*-0.253527);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27050() {
   return (neuron0x8c1e138()*0.115711);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27078() {
   return (neuron0x8c1e360()*-0.131417);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c270a0() {
   return (neuron0x8c1e578()*0.0120311);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c270c8() {
   return (neuron0x8c1e790()*0.251673);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c270f0() {
   return (neuron0x8c1e9b8()*0.335597);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27118() {
   return (neuron0x8c1ebb8()*-0.098818);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27340() {
   return (neuron0x8bfe1e8()*0.211676);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27368() {
   return (neuron0x8c1e138()*-0.237029);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27390() {
   return (neuron0x8c1e360()*0.25959);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c273b8() {
   return (neuron0x8c1e578()*0.0144535);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c273e0() {
   return (neuron0x8c1e790()*0.196045);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27408() {
   return (neuron0x8c1e9b8()*-0.396688);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27430() {
   return (neuron0x8c1ebb8()*0.00346147);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27658() {
   return (neuron0x8bfe1e8()*0.159753);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27680() {
   return (neuron0x8c1e138()*0.36112);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c276a8() {
   return (neuron0x8c1e360()*-0.21697);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c276d0() {
   return (neuron0x8c1e578()*0.0770246);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c276f8() {
   return (neuron0x8c1e790()*-0.252494);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27720() {
   return (neuron0x8c1e9b8()*0.316501);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27748() {
   return (neuron0x8c1ebb8()*-0.247819);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27970() {
   return (neuron0x8bfe1e8()*-0.366899);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27998() {
   return (neuron0x8c1e138()*0.230439);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c279c0() {
   return (neuron0x8c1e360()*0.358789);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c279e8() {
   return (neuron0x8c1e578()*0.308783);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27a10() {
   return (neuron0x8c1e790()*0.423969);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22cf8() {
   return (neuron0x8c1e9b8()*-0.268592);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22d20() {
   return (neuron0x8c1ebb8()*-0.33449);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22f60() {
   return (neuron0x8bfe1e8()*-0.18534);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22f88() {
   return (neuron0x8c1e138()*-0.363724);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22fb0() {
   return (neuron0x8c1e360()*0.249699);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c22fd8() {
   return (neuron0x8c1e578()*-0.304037);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23000() {
   return (neuron0x8c1e790()*-0.252786);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23028() {
   return (neuron0x8c1e9b8()*0.29243);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c23050() {
   return (neuron0x8c1ebb8()*-0.164703);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c230c0() {
   return (neuron0x8bfe1e8()*-0.339122);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c283f8() {
   return (neuron0x8c1e138()*0.365668);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28420() {
   return (neuron0x8c1e360()*-0.143966);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28448() {
   return (neuron0x8c1e578()*-0.0201305);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28470() {
   return (neuron0x8c1e790()*0.172421);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28498() {
   return (neuron0x8c1e9b8()*-0.16149);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c284c0() {
   return (neuron0x8c1ebb8()*-0.00894624);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c286e8() {
   return (neuron0x8bfe1e8()*0.389216);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28710() {
   return (neuron0x8c1e138()*0.0154573);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28738() {
   return (neuron0x8c1e360()*0.17861);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28760() {
   return (neuron0x8c1e578()*-0.424526);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28788() {
   return (neuron0x8c1e790()*0.319396);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c287b0() {
   return (neuron0x8c1e9b8()*0.0132265);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c287d8() {
   return (neuron0x8c1ebb8()*0.385967);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28a00() {
   return (neuron0x8bfe1e8()*0.409977);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28a28() {
   return (neuron0x8c1e138()*-0.139078);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28a50() {
   return (neuron0x8c1e360()*0.249441);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28a78() {
   return (neuron0x8c1e578()*-0.259185);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28aa0() {
   return (neuron0x8c1e790()*-0.103926);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28ac8() {
   return (neuron0x8c1e9b8()*-0.49748);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28af0() {
   return (neuron0x8c1ebb8()*-0.0295885);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28d18() {
   return (neuron0x8bfe1e8()*0.137969);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28d40() {
   return (neuron0x8c1e138()*-0.503059);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28d68() {
   return (neuron0x8c1e360()*-0.0403271);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28d90() {
   return (neuron0x8c1e578()*-0.175627);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28db8() {
   return (neuron0x8c1e790()*-0.196461);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28de0() {
   return (neuron0x8c1e9b8()*0.229246);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28e08() {
   return (neuron0x8c1ebb8()*0.177077);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29030() {
   return (neuron0x8bfe1e8()*0.414604);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29058() {
   return (neuron0x8c1e138()*-0.277852);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29080() {
   return (neuron0x8c1e360()*0.433034);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c290a8() {
   return (neuron0x8c1e578()*0.326374);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c290d0() {
   return (neuron0x8c1e790()*0.114376);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c290f8() {
   return (neuron0x8c1e9b8()*-0.40467);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29120() {
   return (neuron0x8c1ebb8()*0.120457);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29360() {
   return (neuron0x8bfe1e8()*0.0453036);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29388() {
   return (neuron0x8c1e138()*0.278653);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c293b0() {
   return (neuron0x8c1e360()*0.188177);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c293d8() {
   return (neuron0x8c1e578()*0.230711);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29400() {
   return (neuron0x8c1e790()*0.240235);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29428() {
   return (neuron0x8c1e9b8()*0.32896);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29450() {
   return (neuron0x8c1ebb8()*-0.0627823);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29690() {
   return (neuron0x8bfe1e8()*0.109404);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c296b8() {
   return (neuron0x8c1e138()*0.0450681);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c296e0() {
   return (neuron0x8c1e360()*-0.412217);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29708() {
   return (neuron0x8c1e578()*0.123856);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29730() {
   return (neuron0x8c1e790()*0.327215);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29758() {
   return (neuron0x8c1e9b8()*-0.294032);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29780() {
   return (neuron0x8c1ebb8()*0.0346714);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c299c0() {
   return (neuron0x8bfe1e8()*0.423989);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c299e8() {
   return (neuron0x8c1e138()*0.219043);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29a10() {
   return (neuron0x8c1e360()*-0.401381);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29a38() {
   return (neuron0x8c1e578()*-0.380266);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29a60() {
   return (neuron0x8c1e790()*-0.274158);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29a88() {
   return (neuron0x8c1e9b8()*0.120479);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29ab0() {
   return (neuron0x8c1ebb8()*-0.318384);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29cf0() {
   return (neuron0x8bfe1e8()*0.00166926);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29d18() {
   return (neuron0x8c1e138()*0.151057);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29d40() {
   return (neuron0x8c1e360()*-0.320791);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29d68() {
   return (neuron0x8c1e578()*-0.482315);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29d90() {
   return (neuron0x8c1e790()*-0.311854);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29db8() {
   return (neuron0x8c1e9b8()*0.281526);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c29de0() {
   return (neuron0x8c1ebb8()*0.236319);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a020() {
   return (neuron0x8bfe1e8()*0.24899);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a048() {
   return (neuron0x8c1e138()*0.435316);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a070() {
   return (neuron0x8c1e360()*-0.26533);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a098() {
   return (neuron0x8c1e578()*0.123679);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a0c0() {
   return (neuron0x8c1e790()*-0.4103);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a0e8() {
   return (neuron0x8c1e9b8()*0.0394818);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a110() {
   return (neuron0x8c1ebb8()*-0.322427);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a350() {
   return (neuron0x8bfe1e8()*0.0113753);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a378() {
   return (neuron0x8c1e138()*-0.314036);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a3a0() {
   return (neuron0x8c1e360()*-0.0330839);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a3c8() {
   return (neuron0x8c1e578()*0.0666972);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a3f0() {
   return (neuron0x8c1e790()*0.409296);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a418() {
   return (neuron0x8c1e9b8()*-0.0778099);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a440() {
   return (neuron0x8c1ebb8()*-0.318168);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a680() {
   return (neuron0x8bfe1e8()*0.330446);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a6a8() {
   return (neuron0x8c1e138()*0.25432);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a6d0() {
   return (neuron0x8c1e360()*0.413408);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a6f8() {
   return (neuron0x8c1e578()*-0.43073);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a720() {
   return (neuron0x8c1e790()*0.223654);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a748() {
   return (neuron0x8c1e9b8()*-0.301105);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a770() {
   return (neuron0x8c1ebb8()*0.227314);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a9b0() {
   return (neuron0x8bfe1e8()*0.254142);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2a9d8() {
   return (neuron0x8c1e138()*-0.248717);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2aa00() {
   return (neuron0x8c1e360()*0.350636);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2aa28() {
   return (neuron0x8c1e578()*-0.00252251);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2aa50() {
   return (neuron0x8c1e790()*-0.209493);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2aa78() {
   return (neuron0x8c1e9b8()*0.105272);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2aaa0() {
   return (neuron0x8c1ebb8()*-0.039471);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ace0() {
   return (neuron0x8bfe1e8()*0.101906);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ad08() {
   return (neuron0x8c1e138()*0.0474099);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ad30() {
   return (neuron0x8c1e360()*-0.0118043);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ad58() {
   return (neuron0x8c1e578()*0.382412);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ad80() {
   return (neuron0x8c1e790()*0.159581);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ada8() {
   return (neuron0x8c1e9b8()*-0.0682811);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2add0() {
   return (neuron0x8c1ebb8()*0.330152);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b010() {
   return (neuron0x8bfe1e8()*0.226201);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b038() {
   return (neuron0x8c1e138()*-0.484477);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b060() {
   return (neuron0x8c1e360()*-0.374366);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b088() {
   return (neuron0x8c1e578()*0.0782465);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b0b0() {
   return (neuron0x8c1e790()*-0.446208);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b0d8() {
   return (neuron0x8c1e9b8()*-0.292187);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b100() {
   return (neuron0x8c1ebb8()*-0.327041);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b340() {
   return (neuron0x8bfe1e8()*0.00529831);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b368() {
   return (neuron0x8c1e138()*0.163541);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b390() {
   return (neuron0x8c1e360()*0.0153203);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b3b8() {
   return (neuron0x8c1e578()*0.000941625);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b3e0() {
   return (neuron0x8c1e790()*-0.267746);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b408() {
   return (neuron0x8c1e9b8()*0.444665);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b430() {
   return (neuron0x8c1ebb8()*-0.155533);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b670() {
   return (neuron0x8bfe1e8()*-0.425257);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b698() {
   return (neuron0x8c1e138()*-0.21555);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b6c0() {
   return (neuron0x8c1e360()*0.23111);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b6e8() {
   return (neuron0x8c1e578()*-0.257242);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b710() {
   return (neuron0x8c1e790()*-0.0135698);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b738() {
   return (neuron0x8c1e9b8()*-0.0426423);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b760() {
   return (neuron0x8c1ebb8()*0.155329);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b9a0() {
   return (neuron0x8bfe1e8()*0.486644);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b9c8() {
   return (neuron0x8c1e138()*0.472545);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2b9f0() {
   return (neuron0x8c1e360()*-0.357905);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ba18() {
   return (neuron0x8c1e578()*-0.424548);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ba40() {
   return (neuron0x8c1e790()*-0.0124049);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ba68() {
   return (neuron0x8c1e9b8()*-0.179027);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ba90() {
   return (neuron0x8c1ebb8()*-0.0525742);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2bcd0() {
   return (neuron0x8bfe1e8()*-0.196072);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2bcf8() {
   return (neuron0x8c1e138()*-0.104671);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2bd20() {
   return (neuron0x8c1e360()*-0.193822);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2bd48() {
   return (neuron0x8c1e578()*0.285408);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2bd70() {
   return (neuron0x8c1e790()*0.819036);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2bd98() {
   return (neuron0x8c1e9b8()*-0.0199819);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2bdc0() {
   return (neuron0x8c1ebb8()*-0.137003);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c252c8() {
   return (neuron0x8bfe1e8()*0.00465763);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c252f0() {
   return (neuron0x8c1e138()*-0.269406);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25318() {
   return (neuron0x8c1e360()*-0.340234);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25340() {
   return (neuron0x8c1e578()*0.383775);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25368() {
   return (neuron0x8c1e790()*0.124822);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c25390() {
   return (neuron0x8c1e9b8()*-0.378352);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c208() {
   return (neuron0x8c1ebb8()*0.274377);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c430() {
   return (neuron0x8bfe1e8()*0.383231);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c458() {
   return (neuron0x8c1e138()*-0.42467);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c480() {
   return (neuron0x8c1e360()*-0.451068);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c4a8() {
   return (neuron0x8c1e578()*0.418168);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c4d0() {
   return (neuron0x8c1e790()*0.433674);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c4f8() {
   return (neuron0x8c1e9b8()*-0.157152);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c520() {
   return (neuron0x8c1ebb8()*-0.0385582);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c760() {
   return (neuron0x8bfe1e8()*0.153866);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c788() {
   return (neuron0x8c1e138()*0.456703);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c7b0() {
   return (neuron0x8c1e360()*0.254943);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c7d8() {
   return (neuron0x8c1e578()*-0.433227);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c800() {
   return (neuron0x8c1e790()*-0.0301752);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c828() {
   return (neuron0x8c1e9b8()*-0.201072);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2c850() {
   return (neuron0x8c1ebb8()*0.299058);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ca90() {
   return (neuron0x8bfe1e8()*0.137817);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2cab8() {
   return (neuron0x8c1e138()*-0.332413);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2cae0() {
   return (neuron0x8c1e360()*-0.0504969);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2cb08() {
   return (neuron0x8c1e578()*-0.190874);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2cb30() {
   return (neuron0x8c1e790()*0.0152187);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2cb58() {
   return (neuron0x8c1e9b8()*-0.0404715);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2cb80() {
   return (neuron0x8c1ebb8()*0.489867);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2cdc0() {
   return (neuron0x8bfe1e8()*-0.224309);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2cde8() {
   return (neuron0x8c1e138()*0.387234);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ce10() {
   return (neuron0x8c1e360()*-0.00915103);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ce38() {
   return (neuron0x8c1e578()*0.317664);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ce60() {
   return (neuron0x8c1e790()*0.150896);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ce88() {
   return (neuron0x8c1e9b8()*-0.407399);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ceb0() {
   return (neuron0x8c1ebb8()*-0.369074);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2d0f0() {
   return (neuron0x8bfe1e8()*-0.321331);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2d118() {
   return (neuron0x8c1e138()*0.093554);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2d140() {
   return (neuron0x8c1e360()*0.448242);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2d168() {
   return (neuron0x8c1e578()*-0.191908);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2d190() {
   return (neuron0x8c1e790()*0.0194824);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2d1b8() {
   return (neuron0x8c1e9b8()*0.0691831);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2d1e0() {
   return (neuron0x8c1ebb8()*-0.14184);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2d420() {
   return (neuron0x8bfe1e8()*0.198064);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26610() {
   return (neuron0x8c1e138()*-0.246187);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26638() {
   return (neuron0x8c1e360()*0.0294025);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26660() {
   return (neuron0x8c1e578()*0.166564);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26890() {
   return (neuron0x8c1e790()*-0.397964);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c268b8() {
   return (neuron0x8c1e9b8()*0.170252);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c26ae8() {
   return (neuron0x8c1ebb8()*-0.0575066);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2de70() {
   return (neuron0x8bfe1e8()*-0.332878);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2de98() {
   return (neuron0x8c1e138()*0.421088);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2dec0() {
   return (neuron0x8c1e360()*-0.426521);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2dee8() {
   return (neuron0x8c1e578()*0.223217);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2df10() {
   return (neuron0x8c1e790()*-0.345715);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2df38() {
   return (neuron0x8c1e9b8()*0.175032);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2df60() {
   return (neuron0x8c1ebb8()*0.0101186);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e188() {
   return (neuron0x8bfe1e8()*-0.295805);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e1b0() {
   return (neuron0x8c1e138()*0.346673);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e1d8() {
   return (neuron0x8c1e360()*0.264072);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e200() {
   return (neuron0x8c1e578()*0.174534);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e228() {
   return (neuron0x8c1e790()*-0.0628835);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e250() {
   return (neuron0x8c1e9b8()*-0.00348731);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e278() {
   return (neuron0x8c1ebb8()*0.236569);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e4b8() {
   return (neuron0x8bfe1e8()*0.411956);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e4e0() {
   return (neuron0x8c1e138()*0.139038);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e508() {
   return (neuron0x8c1e360()*-0.436985);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e530() {
   return (neuron0x8c1e578()*-0.111047);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e558() {
   return (neuron0x8c1e790()*-0.233251);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e580() {
   return (neuron0x8c1e9b8()*0.439636);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e5a8() {
   return (neuron0x8c1ebb8()*-0.301847);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e7e8() {
   return (neuron0x8bfe1e8()*0.236169);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e810() {
   return (neuron0x8c1e138()*-0.234014);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e838() {
   return (neuron0x8c1e360()*0.0520847);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e860() {
   return (neuron0x8c1e578()*0.0270123);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e888() {
   return (neuron0x8c1e790()*-0.296351);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e8b0() {
   return (neuron0x8c1e9b8()*-0.177278);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2e8d8() {
   return (neuron0x8c1ebb8()*0.284516);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2eb18() {
   return (neuron0x8bfe1e8()*-0.223169);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2eb40() {
   return (neuron0x8c1e138()*-0.344515);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2eb68() {
   return (neuron0x8c1e360()*0.35691);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2eb90() {
   return (neuron0x8c1e578()*0.319475);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ebb8() {
   return (neuron0x8c1e790()*0.184238);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ebe0() {
   return (neuron0x8c1e9b8()*-0.400582);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ec08() {
   return (neuron0x8c1ebb8()*0.460603);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ee48() {
   return (neuron0x8bfe1e8()*-0.18448);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ee70() {
   return (neuron0x8c1e138()*-0.152424);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ee98() {
   return (neuron0x8c1e360()*-0.0319307);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2eec0() {
   return (neuron0x8c1e578()*0.396667);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2eee8() {
   return (neuron0x8c1e790()*-0.0188842);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ef10() {
   return (neuron0x8c1e9b8()*0.415827);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2ef38() {
   return (neuron0x8c1ebb8()*-0.172692);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f178() {
   return (neuron0x8bfe1e8()*0.321779);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f1a0() {
   return (neuron0x8c1e138()*-0.255825);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f1c8() {
   return (neuron0x8c1e360()*-0.351426);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f1f0() {
   return (neuron0x8c1e578()*0.147509);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f218() {
   return (neuron0x8c1e790()*0.307765);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f240() {
   return (neuron0x8c1e9b8()*-0.247538);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f268() {
   return (neuron0x8c1ebb8()*0.394839);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f4a8() {
   return (neuron0x8bfe1e8()*0.319032);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f4d0() {
   return (neuron0x8c1e138()*-0.294602);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f4f8() {
   return (neuron0x8c1e360()*0.364423);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f520() {
   return (neuron0x8c1e578()*0.0519019);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f548() {
   return (neuron0x8c1e790()*-0.222697);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f570() {
   return (neuron0x8c1e9b8()*0.391047);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f598() {
   return (neuron0x8c1ebb8()*-0.178126);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f7d8() {
   return (neuron0x8bfe1e8()*-0.153972);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f800() {
   return (neuron0x8c1e138()*0.117777);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f828() {
   return (neuron0x8c1e360()*0.417102);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f850() {
   return (neuron0x8c1e578()*-0.0339609);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f878() {
   return (neuron0x8c1e790()*-0.167819);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f8a0() {
   return (neuron0x8c1e9b8()*0.47361);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2f8c8() {
   return (neuron0x8c1ebb8()*-0.399824);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2fb08() {
   return (neuron0x8bfe1e8()*0.0521986);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c2fb30() {
   return (neuron0x8c1e138()*-0.180733);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27a38() {
   return (neuron0x8c1e360()*-0.321284);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27a60() {
   return (neuron0x8c1e578()*-0.193291);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27a88() {
   return (neuron0x8c1e790()*-0.450213);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27ab0() {
   return (neuron0x8c1e9b8()*0.274048);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27ad8() {
   return (neuron0x8c1ebb8()*-0.285789);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27d18() {
   return (neuron0x8bfe1e8()*0.078607);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27d40() {
   return (neuron0x8c1e138()*-0.461947);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27d68() {
   return (neuron0x8c1e360()*-0.0355056);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27d90() {
   return (neuron0x8c1e578()*0.0391368);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27db8() {
   return (neuron0x8c1e790()*-0.192993);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27de0() {
   return (neuron0x8c1e9b8()*-0.0593146);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c27e08() {
   return (neuron0x8c1ebb8()*0.313188);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28048() {
   return (neuron0x8bfe1e8()*0.147567);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28070() {
   return (neuron0x8c1e138()*0.271643);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28098() {
   return (neuron0x8c1e360()*-0.258398);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c280c0() {
   return (neuron0x8c1e578()*0.148657);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c280e8() {
   return (neuron0x8c1e790()*-0.283777);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28110() {
   return (neuron0x8c1e9b8()*0.186964);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28138() {
   return (neuron0x8c1ebb8()*0.310486);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c28208() {
   return (neuron0x8bfe1e8()*0.270028);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c30cd0() {
   return (neuron0x8c1e138()*-0.28933);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c30cf8() {
   return (neuron0x8c1e360()*-0.0498178);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c30d20() {
   return (neuron0x8c1e578()*-0.00700052);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c30d48() {
   return (neuron0x8c1e790()*0.443294);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c30d70() {
   return (neuron0x8c1e9b8()*0.231562);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c30d98() {
   return (neuron0x8c1ebb8()*0.0942612);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c30fd8() {
   return (neuron0x8bfe1e8()*0.272341);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31000() {
   return (neuron0x8c1e138()*-0.0503476);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31028() {
   return (neuron0x8c1e360()*-0.27613);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31050() {
   return (neuron0x8c1e578()*0.172948);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31078() {
   return (neuron0x8c1e790()*0.0517682);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c310a0() {
   return (neuron0x8c1e9b8()*-0.381823);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c310c8() {
   return (neuron0x8c1ebb8()*-0.227888);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31308() {
   return (neuron0x8bfe1e8()*0.0384582);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31330() {
   return (neuron0x8c1e138()*-0.13252);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31358() {
   return (neuron0x8c1e360()*-0.451942);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31380() {
   return (neuron0x8c1e578()*0.474981);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c313a8() {
   return (neuron0x8c1e790()*-0.0679263);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c313d0() {
   return (neuron0x8c1e9b8()*0.446781);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c313f8() {
   return (neuron0x8c1ebb8()*0.24122);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31638() {
   return (neuron0x8bfe1e8()*0.0447272);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31660() {
   return (neuron0x8c1e138()*-0.351402);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31688() {
   return (neuron0x8c1e360()*-0.426321);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c316b0() {
   return (neuron0x8c1e578()*0.0434936);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c316d8() {
   return (neuron0x8c1e790()*0.39105);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31700() {
   return (neuron0x8c1e9b8()*-0.325034);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31728() {
   return (neuron0x8c1ebb8()*-0.00153284);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31968() {
   return (neuron0x8bfe1e8()*0.154097);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31990() {
   return (neuron0x8c1e138()*-0.119464);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c319b8() {
   return (neuron0x8c1e360()*-0.0321146);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c319e0() {
   return (neuron0x8c1e578()*0.333454);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31a08() {
   return (neuron0x8c1e790()*-0.212702);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31a30() {
   return (neuron0x8c1e9b8()*0.440238);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31a58() {
   return (neuron0x8c1ebb8()*0.258434);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31c98() {
   return (neuron0x8bfe1e8()*0.459338);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31cc0() {
   return (neuron0x8c1e138()*-0.190374);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31ce8() {
   return (neuron0x8c1e360()*-0.320483);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31d10() {
   return (neuron0x8c1e578()*0.411641);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31d38() {
   return (neuron0x8c1e790()*-0.384906);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31d60() {
   return (neuron0x8c1e9b8()*0.0542144);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31d88() {
   return (neuron0x8c1ebb8()*0.42265);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31fc8() {
   return (neuron0x8bfe1e8()*-0.383996);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c31ff0() {
   return (neuron0x8c1e138()*0.20838);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32018() {
   return (neuron0x8c1e360()*0.19754);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32040() {
   return (neuron0x8c1e578()*0.0282751);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32068() {
   return (neuron0x8c1e790()*0.254525);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32090() {
   return (neuron0x8c1e9b8()*-0.275576);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c320b8() {
   return (neuron0x8c1ebb8()*0.219857);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c322f8() {
   return (neuron0x8bfe1e8()*-0.0306966);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32320() {
   return (neuron0x8c1e138()*-0.337);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32348() {
   return (neuron0x8c1e360()*0.146921);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32370() {
   return (neuron0x8c1e578()*0.331033);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32398() {
   return (neuron0x8c1e790()*0.158339);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c323c0() {
   return (neuron0x8c1e9b8()*-0.421141);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c323e8() {
   return (neuron0x8c1ebb8()*0.252745);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32628() {
   return (neuron0x8bfe1e8()*-0.334778);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32650() {
   return (neuron0x8c1e138()*0.382713);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32678() {
   return (neuron0x8c1e360()*0.461476);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c326a0() {
   return (neuron0x8c1e578()*0.181251);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c326c8() {
   return (neuron0x8c1e790()*-0.339462);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c326f0() {
   return (neuron0x8c1e9b8()*-0.00412588);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32718() {
   return (neuron0x8c1ebb8()*-0.232109);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32958() {
   return (neuron0x8bfe1e8()*-0.117731);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32980() {
   return (neuron0x8c1e138()*-0.23994);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c329a8() {
   return (neuron0x8c1e360()*-0.03028);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c329d0() {
   return (neuron0x8c1e578()*-0.32271);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c329f8() {
   return (neuron0x8c1e790()*-0.305818);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32a20() {
   return (neuron0x8c1e9b8()*-0.26767);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32a48() {
   return (neuron0x8c1ebb8()*0.230756);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32c88() {
   return (neuron0x8bfe1e8()*-0.00597581);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32cb0() {
   return (neuron0x8c1e138()*0.255749);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32cd8() {
   return (neuron0x8c1e360()*-0.000185198);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32d00() {
   return (neuron0x8c1e578()*0.340932);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32d28() {
   return (neuron0x8c1e790()*0.329088);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32d50() {
   return (neuron0x8c1e9b8()*0.232934);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32d78() {
   return (neuron0x8c1ebb8()*0.260194);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32fb8() {
   return (neuron0x8bfe1e8()*0.0147128);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c32fe0() {
   return (neuron0x8c1e138()*-0.350188);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33008() {
   return (neuron0x8c1e360()*0.084231);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33030() {
   return (neuron0x8c1e578()*-0.234019);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33058() {
   return (neuron0x8c1e790()*-0.13319);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33080() {
   return (neuron0x8c1e9b8()*0.335973);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c330a8() {
   return (neuron0x8c1ebb8()*0.261513);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c332e8() {
   return (neuron0x8bfe1e8()*-0.425646);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33310() {
   return (neuron0x8c1e138()*-0.10585);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33338() {
   return (neuron0x8c1e360()*-0.119463);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33360() {
   return (neuron0x8c1e578()*0.286708);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33388() {
   return (neuron0x8c1e790()*-0.13888);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c333b0() {
   return (neuron0x8c1e9b8()*0.0169704);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c333d8() {
   return (neuron0x8c1ebb8()*-0.0516773);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33618() {
   return (neuron0x8bfe1e8()*-0.238298);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33640() {
   return (neuron0x8c1e138()*-0.251179);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33668() {
   return (neuron0x8c1e360()*0.225326);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33690() {
   return (neuron0x8c1e578()*0.17327);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c336b8() {
   return (neuron0x8c1e790()*1.11275);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c336e0() {
   return (neuron0x8c1e9b8()*-0.0234204);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33708() {
   return (neuron0x8c1ebb8()*0.00700241);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33948() {
   return (neuron0x8bfe1e8()*0.00803933);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33970() {
   return (neuron0x8c1e138()*0.310731);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33998() {
   return (neuron0x8c1e360()*-0.351704);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c339c0() {
   return (neuron0x8c1e578()*0.235139);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c339e8() {
   return (neuron0x8c1e790()*-0.168161);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33a10() {
   return (neuron0x8c1e9b8()*-0.491584);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33a38() {
   return (neuron0x8c1ebb8()*-0.131945);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33c78() {
   return (neuron0x8bfe1e8()*-0.442212);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33ca0() {
   return (neuron0x8c1e138()*-0.394871);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33cc8() {
   return (neuron0x8c1e360()*0.442072);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33cf0() {
   return (neuron0x8c1e578()*-0.0219576);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33d18() {
   return (neuron0x8c1e790()*-0.323213);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33d40() {
   return (neuron0x8c1e9b8()*-0.316784);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33d68() {
   return (neuron0x8c1ebb8()*-0.403672);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33fa8() {
   return (neuron0x8bfe1e8()*-0.232458);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33fd0() {
   return (neuron0x8c1e138()*0.263178);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c33ff8() {
   return (neuron0x8c1e360()*0.391885);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34020() {
   return (neuron0x8c1e578()*-0.440057);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34048() {
   return (neuron0x8c1e790()*0.0316173);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34070() {
   return (neuron0x8c1e9b8()*-0.11635);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34098() {
   return (neuron0x8c1ebb8()*0.259226);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c342d8() {
   return (neuron0x8bfe1e8()*0.145603);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34300() {
   return (neuron0x8c1e138()*0.381147);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34328() {
   return (neuron0x8c1e360()*-0.150093);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34350() {
   return (neuron0x8c1e578()*0.299411);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34378() {
   return (neuron0x8c1e790()*0.00757518);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c343a0() {
   return (neuron0x8c1e9b8()*-0.348451);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c343c8() {
   return (neuron0x8c1ebb8()*-0.201678);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34608() {
   return (neuron0x8bfe1e8()*0.152426);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34630() {
   return (neuron0x8c1e138()*-0.213338);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34658() {
   return (neuron0x8c1e360()*0.250221);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34680() {
   return (neuron0x8c1e578()*-0.328755);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c346a8() {
   return (neuron0x8c1e790()*-0.358439);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c346d0() {
   return (neuron0x8c1e9b8()*-0.138013);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c346f8() {
   return (neuron0x8c1ebb8()*0.146374);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34938() {
   return (neuron0x8bfe1e8()*0.147655);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34960() {
   return (neuron0x8c1e138()*0.317457);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34988() {
   return (neuron0x8c1e360()*-0.35847);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c349b0() {
   return (neuron0x8c1e578()*0.391342);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c349d8() {
   return (neuron0x8c1e790()*-0.0171003);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34a00() {
   return (neuron0x8c1e9b8()*-0.021013);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34a28() {
   return (neuron0x8c1ebb8()*-0.14208);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34c68() {
   return (neuron0x8bfe1e8()*-0.324466);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34c90() {
   return (neuron0x8c1e138()*-0.43346);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34cb8() {
   return (neuron0x8c1e360()*0.176375);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34ce0() {
   return (neuron0x8c1e578()*-0.299959);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34d08() {
   return (neuron0x8c1e790()*-0.121985);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34d30() {
   return (neuron0x8c1e9b8()*-0.325442);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34d58() {
   return (neuron0x8c1ebb8()*0.0204505);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34f98() {
   return (neuron0x8bfe1e8()*-0.0397155);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34fc0() {
   return (neuron0x8c1e138()*0.0882138);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c34fe8() {
   return (neuron0x8c1e360()*-0.428661);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35010() {
   return (neuron0x8c1e578()*0.222104);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35038() {
   return (neuron0x8c1e790()*0.0988492);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35060() {
   return (neuron0x8c1e9b8()*0.239531);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35088() {
   return (neuron0x8c1ebb8()*0.116233);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c352c8() {
   return (neuron0x8bfe1e8()*0.133605);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c352f0() {
   return (neuron0x8c1e138()*-0.152258);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35318() {
   return (neuron0x8c1e360()*-0.163955);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35340() {
   return (neuron0x8c1e578()*-0.267838);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35368() {
   return (neuron0x8c1e790()*0.394101);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35390() {
   return (neuron0x8c1e9b8()*0.437458);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c353b8() {
   return (neuron0x8c1ebb8()*0.232618);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c355f8() {
   return (neuron0x8bfe1e8()*0.373175);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35620() {
   return (neuron0x8c1e138()*0.358358);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35648() {
   return (neuron0x8c1e360()*-0.0969712);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35670() {
   return (neuron0x8c1e578()*0.212192);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35698() {
   return (neuron0x8c1e790()*-0.0207829);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c356c0() {
   return (neuron0x8c1e9b8()*-0.463822);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c356e8() {
   return (neuron0x8c1ebb8()*-0.337122);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35928() {
   return (neuron0x8bfe1e8()*-0.243064);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35950() {
   return (neuron0x8c1e138()*-0.0695996);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35978() {
   return (neuron0x8c1e360()*-0.164516);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c359a0() {
   return (neuron0x8c1e578()*0.335332);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c359c8() {
   return (neuron0x8c1e790()*0.271431);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c359f0() {
   return (neuron0x8c1e9b8()*0.276264);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35a18() {
   return (neuron0x8c1ebb8()*-0.215409);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35c58() {
   return (neuron0x8bfe1e8()*-0.0644941);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35c80() {
   return (neuron0x8c1e138()*-0.190519);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35ca8() {
   return (neuron0x8c1e360()*-0.305152);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35cd0() {
   return (neuron0x8c1e578()*0.00217214);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35cf8() {
   return (neuron0x8c1e790()*-0.327661);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35d20() {
   return (neuron0x8c1e9b8()*0.182596);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35d48() {
   return (neuron0x8c1ebb8()*0.216867);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35f88() {
   return (neuron0x8bfe1e8()*0.205232);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35fb0() {
   return (neuron0x8c1e138()*0.0760219);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c35fd8() {
   return (neuron0x8c1e360()*-0.215903);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36000() {
   return (neuron0x8c1e578()*-0.155667);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36028() {
   return (neuron0x8c1e790()*0.369054);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36050() {
   return (neuron0x8c1e9b8()*-0.378669);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36078() {
   return (neuron0x8c1ebb8()*-0.129117);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c362b8() {
   return (neuron0x8bfe1e8()*-0.204521);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c362e0() {
   return (neuron0x8c1e138()*-0.0482028);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36308() {
   return (neuron0x8c1e360()*0.388985);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36330() {
   return (neuron0x8c1e578()*0.366952);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36358() {
   return (neuron0x8c1e790()*0.417462);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36380() {
   return (neuron0x8c1e9b8()*-0.0206522);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c363a8() {
   return (neuron0x8c1ebb8()*0.139879);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c365e8() {
   return (neuron0x8bfe1e8()*0.281189);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36610() {
   return (neuron0x8c1e138()*0.103398);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36638() {
   return (neuron0x8c1e360()*-0.427414);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36660() {
   return (neuron0x8c1e578()*0.114898);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36688() {
   return (neuron0x8c1e790()*0.37075);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c366b0() {
   return (neuron0x8c1e9b8()*0.0892722);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c366d8() {
   return (neuron0x8c1ebb8()*-0.187094);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36918() {
   return (neuron0x8bfe1e8()*0.45052);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36940() {
   return (neuron0x8c1e138()*-0.000749855);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36968() {
   return (neuron0x8c1e360()*-0.0552422);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36990() {
   return (neuron0x8c1e578()*-0.219538);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c369b8() {
   return (neuron0x8c1e790()*0.192739);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c369e0() {
   return (neuron0x8c1e9b8()*0.431905);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36a08() {
   return (neuron0x8c1ebb8()*-0.3879);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36c48() {
   return (neuron0x8bfe1e8()*-0.291057);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36c70() {
   return (neuron0x8c1e138()*-0.197176);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36c98() {
   return (neuron0x8c1e360()*0.340211);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36cc0() {
   return (neuron0x8c1e578()*0.0950163);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36ce8() {
   return (neuron0x8c1e790()*-0.271954);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36d10() {
   return (neuron0x8c1e9b8()*-0.0208371);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36d38() {
   return (neuron0x8c1ebb8()*-0.124146);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36f78() {
   return (neuron0x8bfe1e8()*0.367864);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36fa0() {
   return (neuron0x8c1e138()*-0.207653);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36fc8() {
   return (neuron0x8c1e360()*0.0312182);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c36ff0() {
   return (neuron0x8c1e578()*0.272272);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37018() {
   return (neuron0x8c1e790()*0.037172);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37040() {
   return (neuron0x8c1e9b8()*0.0881517);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37068() {
   return (neuron0x8c1ebb8()*0.350429);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c372a8() {
   return (neuron0x8bfe1e8()*0.0118599);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c372d0() {
   return (neuron0x8c1e138()*-0.142093);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c372f8() {
   return (neuron0x8c1e360()*0.00563298);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37320() {
   return (neuron0x8c1e578()*-0.236969);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37348() {
   return (neuron0x8c1e790()*0.1227);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37370() {
   return (neuron0x8c1e9b8()*0.117542);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37398() {
   return (neuron0x8c1ebb8()*-0.510983);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c375d8() {
   return (neuron0x8bfe1e8()*-0.366944);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37600() {
   return (neuron0x8c1e138()*0.447051);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37628() {
   return (neuron0x8c1e360()*0.157314);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37650() {
   return (neuron0x8c1e578()*0.265664);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37678() {
   return (neuron0x8c1e790()*0.0682286);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c376a0() {
   return (neuron0x8c1e9b8()*-0.385063);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c376c8() {
   return (neuron0x8c1ebb8()*-0.0856701);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37908() {
   return (neuron0x8bfe1e8()*-0.383432);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37930() {
   return (neuron0x8c1e138()*0.33585);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37958() {
   return (neuron0x8c1e360()*0.138978);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37980() {
   return (neuron0x8c1e578()*-0.16793);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c379a8() {
   return (neuron0x8c1e790()*0.0490653);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c379d0() {
   return (neuron0x8c1e9b8()*-0.235621);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c379f8() {
   return (neuron0x8c1ebb8()*0.279818);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37c38() {
   return (neuron0x8bfe1e8()*-0.290993);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37c60() {
   return (neuron0x8c1e138()*0.272907);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37c88() {
   return (neuron0x8c1e360()*0.251803);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37cb0() {
   return (neuron0x8c1e578()*0.147366);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37cd8() {
   return (neuron0x8c1e790()*-0.188447);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37d00() {
   return (neuron0x8c1e9b8()*-0.260462);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37d28() {
   return (neuron0x8c1ebb8()*-0.367888);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37f68() {
   return (neuron0x8bfe1e8()*0.385828);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37f90() {
   return (neuron0x8c1e138()*-0.502503);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37fb8() {
   return (neuron0x8c1e360()*0.290059);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c37fe0() {
   return (neuron0x8c1e578()*-0.0325945);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38008() {
   return (neuron0x8c1e790()*0.0872799);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38030() {
   return (neuron0x8c1e9b8()*0.275001);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38058() {
   return (neuron0x8c1ebb8()*-0.143612);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38298() {
   return (neuron0x8bfe1e8()*-0.00719683);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c382c0() {
   return (neuron0x8c1e138()*0.456881);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c382e8() {
   return (neuron0x8c1e360()*-0.0312364);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38310() {
   return (neuron0x8c1e578()*0.19007);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38338() {
   return (neuron0x8c1e790()*-0.162673);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38360() {
   return (neuron0x8c1e9b8()*0.150985);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38388() {
   return (neuron0x8c1ebb8()*-0.278015);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c385c8() {
   return (neuron0x8bfe1e8()*0.240543);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c385f0() {
   return (neuron0x8c1e138()*0.215954);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38618() {
   return (neuron0x8c1e360()*-0.499407);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38640() {
   return (neuron0x8c1e578()*-0.491398);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38668() {
   return (neuron0x8c1e790()*0.22002);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38690() {
   return (neuron0x8c1e9b8()*0.0192958);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c386b8() {
   return (neuron0x8c1ebb8()*0.0278394);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c388f8() {
   return (neuron0x8bfe1e8()*0.246879);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38920() {
   return (neuron0x8c1e138()*-0.380629);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38948() {
   return (neuron0x8c1e360()*-0.397659);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38970() {
   return (neuron0x8c1e578()*-0.283542);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38998() {
   return (neuron0x8c1e790()*-0.0146114);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c389c0() {
   return (neuron0x8c1e9b8()*0.0129601);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c389e8() {
   return (neuron0x8c1ebb8()*-0.161232);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38c28() {
   return (neuron0x8bfe1e8()*-0.417238);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38c50() {
   return (neuron0x8c1e138()*0.328111);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38c78() {
   return (neuron0x8c1e360()*0.201917);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38ca0() {
   return (neuron0x8c1e578()*-0.286379);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38cc8() {
   return (neuron0x8c1e790()*0.373102);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38cf0() {
   return (neuron0x8c1e9b8()*0.150619);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38d18() {
   return (neuron0x8c1ebb8()*0.127453);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38f58() {
   return (neuron0x8bfe1e8()*0.10671);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38f80() {
   return (neuron0x8c1e138()*-0.0904511);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38fa8() {
   return (neuron0x8c1e360()*-0.166732);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38fd0() {
   return (neuron0x8c1e578()*0.143652);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c38ff8() {
   return (neuron0x8c1e790()*0.144435);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39020() {
   return (neuron0x8c1e9b8()*-0.167052);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39048() {
   return (neuron0x8c1ebb8()*-0.275417);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39288() {
   return (neuron0x8bfe1e8()*0.228217);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c392b0() {
   return (neuron0x8c1e138()*-0.294934);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c392d8() {
   return (neuron0x8c1e360()*-0.30744);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39300() {
   return (neuron0x8c1e578()*0.413635);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39328() {
   return (neuron0x8c1e790()*0.304994);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39350() {
   return (neuron0x8c1e9b8()*0.236062);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39378() {
   return (neuron0x8c1ebb8()*0.0961984);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c395b8() {
   return (neuron0x8bfe1e8()*-0.127663);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c395e0() {
   return (neuron0x8c1e138()*0.258193);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39608() {
   return (neuron0x8c1e360()*-0.0540372);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39630() {
   return (neuron0x8c1e578()*0.00199312);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39658() {
   return (neuron0x8c1e790()*0.23477);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39680() {
   return (neuron0x8c1e9b8()*-0.32442);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c396a8() {
   return (neuron0x8c1ebb8()*-0.354294);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c397f8() {
   return (neuron0x8c1eef8()*-0.451047);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39820() {
   return (neuron0x8c1f128()*0.0575858);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39848() {
   return (neuron0x8c1f440()*-0.359438);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39870() {
   return (neuron0x8c1f758()*0.161255);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39898() {
   return (neuron0x8c1fa28()*0.22535);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c398c0() {
   return (neuron0x8c1fe10()*-0.318612);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c398e8() {
   return (neuron0x8c20128()*-0.160392);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39910() {
   return (neuron0x8c20458()*0.31475);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39938() {
   return (neuron0x8c20788()*0.135556);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39960() {
   return (neuron0x8c20ab8()*0.195659);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39988() {
   return (neuron0x8c20f28()*0.615695);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c399b0() {
   return (neuron0x8c21258()*0.249652);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c399d8() {
   return (neuron0x8c21588()*0.336029);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39a00() {
   return (neuron0x8c218b8()*0.194956);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39a28() {
   return (neuron0x8c21be8()*0.288449);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39a50() {
   return (neuron0x8c21f18()*0.121177);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39b00() {
   return (neuron0x8c22600()*0.162509);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39b28() {
   return (neuron0x8c22858()*0.0139647);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39b50() {
   return (neuron0x8c22ab0()*0.0730747);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39b78() {
   return (neuron0x8c20d20()*-0.326906);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39ba0() {
   return (neuron0x8c23128()*-0.15163);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39bc8() {
   return (neuron0x8c23458()*0.183145);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39bf0() {
   return (neuron0x8c23788()*-0.0878943);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39c18() {
   return (neuron0x8c23ab8()*-0.566058);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39c40() {
   return (neuron0x8c24eb0()*-0.00171977);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39c68() {
   return (neuron0x8c250c8()*-0.285447);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39c90() {
   return (neuron0x8c25470()*0.128892);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39cb8() {
   return (neuron0x8c25788()*0.0123549);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39ce0() {
   return (neuron0x8c25aa0()*-0.159402);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39d08() {
   return (neuron0x8c25db8()*-0.0823148);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39d30() {
   return (neuron0x8c260d0()*0.32384);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39d58() {
   return (neuron0x8c263e8()*-0.0138072);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39a78() {
   return (neuron0x8c26d48()*0.233114);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39aa0() {
   return (neuron0x8c26e70()*0.333164);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39ac8() {
   return (neuron0x8c27140()*0.201118);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39e88() {
   return (neuron0x8c27458()*-0.332941);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39eb0() {
   return (neuron0x8c27770()*0.165559);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39ed8() {
   return (neuron0x8c22d48()*-0.0763389);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39f00() {
   return (neuron0x8c28240()*-0.482763);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39f28() {
   return (neuron0x8c284e8()*0.163996);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39f50() {
   return (neuron0x8c28800()*0.0321828);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39f78() {
   return (neuron0x8c28b18()*-0.132603);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39fa0() {
   return (neuron0x8c28e30()*-0.0104164);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39fc8() {
   return (neuron0x8c29148()*0.308108);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39ff0() {
   return (neuron0x8c29478()*0.221106);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a018() {
   return (neuron0x8c297a8()*-0.357097);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a040() {
   return (neuron0x8c29ad8()*0.0187057);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a068() {
   return (neuron0x8c29e08()*-0.196083);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a090() {
   return (neuron0x8c2a138()*-0.295006);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a0b8() {
   return (neuron0x8c2a468()*-0.06907);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a0e0() {
   return (neuron0x8c2a798()*-0.296747);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a108() {
   return (neuron0x8c2aac8()*-0.41284);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a130() {
   return (neuron0x8c2adf8()*-0.136617);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a158() {
   return (neuron0x8c2b128()*0.0799529);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a180() {
   return (neuron0x8c2b458()*-0.356357);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a1a8() {
   return (neuron0x8c2b788()*0.0802846);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a1d0() {
   return (neuron0x8c2bab8()*-0.278421);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a1f8() {
   return (neuron0x8c2bde8()*0.0654973);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a220() {
   return (neuron0x8c2c230()*-0.273221);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a248() {
   return (neuron0x8c2c548()*0.286651);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a270() {
   return (neuron0x8c2c878()*-0.0901787);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a298() {
   return (neuron0x8c2cba8()*0.142291);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a2c0() {
   return (neuron0x8c2ced8()*0.037588);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a2e8() {
   return (neuron0x8c2d208()*-0.113652);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c1ee10() {
   return (neuron0x8c26b10()*0.0205838);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39d80() {
   return (neuron0x8c2df88()*0.209662);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39da8() {
   return (neuron0x8c2e2a0()*-0.315029);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39dd0() {
   return (neuron0x8c2e5d0()*0.332684);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39df8() {
   return (neuron0x8c2e900()*-0.231098);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39e20() {
   return (neuron0x8c2ec30()*0.149452);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c39e48() {
   return (neuron0x8c2ef60()*-0.122744);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a518() {
   return (neuron0x8c2f290()*0.00520174);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a540() {
   return (neuron0x8c2f5c0()*-0.0290896);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a568() {
   return (neuron0x8c2f8f0()*-0.0801928);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a590() {
   return (neuron0x8c27b00()*0.489442);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a5b8() {
   return (neuron0x8c27e30()*0.103975);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a5e0() {
   return (neuron0x8c30b60()*0.283892);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a608() {
   return (neuron0x8c30dc0()*0.414512);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a630() {
   return (neuron0x8c310f0()*0.142587);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a658() {
   return (neuron0x8c31420()*-0.110716);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a680() {
   return (neuron0x8c31750()*0.204432);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a6a8() {
   return (neuron0x8c31a80()*-0.130121);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a6d0() {
   return (neuron0x8c31db0()*-0.34833);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a6f8() {
   return (neuron0x8c320e0()*-0.251724);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a720() {
   return (neuron0x8c32410()*0.230135);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a748() {
   return (neuron0x8c32740()*0.0449144);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a770() {
   return (neuron0x8c32a70()*0.389984);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a798() {
   return (neuron0x8c32da0()*0.30675);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a7c0() {
   return (neuron0x8c330d0()*-0.277446);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a7e8() {
   return (neuron0x8c33400()*0.321535);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a810() {
   return (neuron0x8c33730()*-0.0206092);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a838() {
   return (neuron0x8c33a60()*0.129953);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a860() {
   return (neuron0x8c33d90()*-0.41305);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a888() {
   return (neuron0x8c340c0()*-0.000375067);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a8b0() {
   return (neuron0x8c343f0()*-0.216261);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a8d8() {
   return (neuron0x8c34720()*-0.236572);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a900() {
   return (neuron0x8c34a50()*0.103116);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a928() {
   return (neuron0x8c34d80()*-0.157479);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a950() {
   return (neuron0x8c350b0()*0.0487321);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a978() {
   return (neuron0x8c353e0()*-0.0323697);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a9a0() {
   return (neuron0x8c35710()*-0.0827625);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a9c8() {
   return (neuron0x8c35a40()*0.182007);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3a9f0() {
   return (neuron0x8c35d70()*0.263832);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3aa18() {
   return (neuron0x8c360a0()*-0.19137);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3aa40() {
   return (neuron0x8c363d0()*0.259257);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3aa68() {
   return (neuron0x8c36700()*-0.0818453);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3aa90() {
   return (neuron0x8c36a30()*-0.175169);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3aab8() {
   return (neuron0x8c36d60()*0.17762);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3aae0() {
   return (neuron0x8c37090()*-0.107465);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3ab08() {
   return (neuron0x8c373c0()*0.142934);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3ab30() {
   return (neuron0x8c376f0()*0.179236);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3ab58() {
   return (neuron0x8c37a20()*0.066554);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3ab80() {
   return (neuron0x8c37d50()*-0.106426);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3aba8() {
   return (neuron0x8c38080()*0.0590176);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3abd0() {
   return (neuron0x8c383b0()*0.20816);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3abf8() {
   return (neuron0x8c386e0()*0.237889);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3ac20() {
   return (neuron0x8c38a10()*0.324187);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3ac48() {
   return (neuron0x8c38d40()*-0.331024);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3ac70() {
   return (neuron0x8c39070()*-0.37957);
}

double NNParaElectronAngleCorrectionDeltaTheta::synapse0x8c3ac98() {
   return (neuron0x8c393a0()*-0.376374);
}

