#include "NNParaElectronAngleCorrectionBCPDir.h"
#include <cmath>

double NNParaElectronAngleCorrectionBCPDir::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - 0.707859)/0.098362;
   input1 = (in1 - -0.0064129)/0.273513;
   input2 = (in2 - -2.94094)/33.8704;
   input3 = (in3 - -1.40874)/59.9912;
   input4 = (in4 - 1844.57)/787.294;
   input5 = (in5 - 0.00566612)/1.06453;
   input6 = (in6 - 0.00183522)/1.05676;
   switch(index) {
     case 0:
         return neuron0x8bdabc8();
     case 1:
         return neuron0x8bdd010();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionBCPDir::Value(int index, double* input) {
   input0 = (input[0] - 0.707859)/0.098362;
   input1 = (input[1] - -0.0064129)/0.273513;
   input2 = (input[2] - -2.94094)/33.8704;
   input3 = (input[3] - -1.40874)/59.9912;
   input4 = (input[4] - 1844.57)/787.294;
   input5 = (input[5] - 0.00566612)/1.06453;
   input6 = (input[6] - 0.00183522)/1.05676;
   switch(index) {
     case 0:
         return neuron0x8bdabc8();
     case 1:
         return neuron0x8bdd010();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bcbb60() {
   return input0;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bda130() {
   return input1;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bda258() {
   return input2;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bda3c8() {
   return input3;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bda5c8() {
   return input4;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bda7c8() {
   return input5;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bda9c8() {
   return input6;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdacf0() {
   double input = 0.168133;
   input += synapse0x8ba7200();
   input += synapse0x8b54c68();
   input += synapse0x8b54cb0();
   input += synapse0x8b54d80();
   input += synapse0x8bcbdd0();
   input += synapse0x8bdaea8();
   input += synapse0x8bdaed0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdacf0() {
   double input = input0x8bdacf0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdaef8() {
   double input = 0.225323;
   input += synapse0x8bdb0f8();
   input += synapse0x8bdb120();
   input += synapse0x8bdb148();
   input += synapse0x8bdb170();
   input += synapse0x8bdb198();
   input += synapse0x8bdb1c0();
   input += synapse0x8bdb1e8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdaef8() {
   double input = input0x8bdaef8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdb210() {
   double input = 0.312278;
   input += synapse0x8bdb410();
   input += synapse0x8bdb438();
   input += synapse0x8bdb460();
   input += synapse0x8bdb510();
   input += synapse0x8bdb538();
   input += synapse0x8bdb560();
   input += synapse0x8bdb588();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdb210() {
   double input = input0x8bdb210();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdb5b0() {
   double input = 0.30181;
   input += synapse0x8bdb768();
   input += synapse0x8bdb790();
   input += synapse0x8bdb7b8();
   input += synapse0x8bdb7e0();
   input += synapse0x8bdb808();
   input += synapse0x8bdb830();
   input += synapse0x8bdb858();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdb5b0() {
   double input = input0x8bdb5b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdb880() {
   double input = -0.244057;
   input += synapse0x8bdba98();
   input += synapse0x8bdbac0();
   input += synapse0x8bdbae8();
   input += synapse0x8bdbb10();
   input += synapse0x8bdbb38();
   input += synapse0x8bdb488();
   input += synapse0x8bdb4b0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdb880() {
   double input = input0x8bdb880();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdbc68() {
   double input = -0.168662;
   input += synapse0x8bdbe68();
   input += synapse0x8bdbe90();
   input += synapse0x8bdbeb8();
   input += synapse0x8bdbee0();
   input += synapse0x8bdbf08();
   input += synapse0x8bdbf30();
   input += synapse0x8bdbf58();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdbc68() {
   double input = input0x8bdbc68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdbf80() {
   double input = 0.266209;
   input += synapse0x8bdc180();
   input += synapse0x8bdc1a8();
   input += synapse0x8bdc1d0();
   input += synapse0x8bdc1f8();
   input += synapse0x8bdc220();
   input += synapse0x8bdc248();
   input += synapse0x8bdc270();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdbf80() {
   double input = input0x8bdbf80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdc298() {
   double input = -0.376937;
   input += synapse0x8bdc4b0();
   input += synapse0x8bdc4d8();
   input += synapse0x8bdc500();
   input += synapse0x8bdc528();
   input += synapse0x8bdc550();
   input += synapse0x8bdc578();
   input += synapse0x8bdc5a0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdc298() {
   double input = input0x8bdc298();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdc5c8() {
   double input = -0.0109429;
   input += synapse0x8bdc7e0();
   input += synapse0x8bdc808();
   input += synapse0x8bdc830();
   input += synapse0x8bdc858();
   input += synapse0x8bdc880();
   input += synapse0x8bdc8a8();
   input += synapse0x8bdc8d0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdc5c8() {
   double input = input0x8bdc5c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdc8f8() {
   double input = -0.0417212;
   input += synapse0x8bdcb98();
   input += synapse0x8bdcbc0();
   input += synapse0x8b54da8();
   input += synapse0x8ba71b8();
   input += synapse0x8b4c9e0();
   input += synapse0x8bdbb60();
   input += synapse0x8bdbb88();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdc8f8() {
   double input = input0x8bdc8f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdabc8() {
   double input = -0.0361655;
   input += synapse0x8bdce80();
   input += synapse0x8bdcea8();
   input += synapse0x8bdced0();
   input += synapse0x8bdcef8();
   input += synapse0x8bdcf20();
   input += synapse0x8bdcf48();
   input += synapse0x8bdcf70();
   input += synapse0x8bdcf98();
   input += synapse0x8bdcfc0();
   input += synapse0x8bdcfe8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdabc8() {
   double input = input0x8bdabc8();
   return (input * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::input0x8bdd010() {
   double input = 0.170938;
   input += synapse0x8bdd238();
   input += synapse0x8bdd260();
   input += synapse0x8bdd288();
   input += synapse0x8bdd2b0();
   input += synapse0x8bdd2d8();
   input += synapse0x8bdd300();
   input += synapse0x8bdd328();
   input += synapse0x8bdd350();
   input += synapse0x8bdd378();
   input += synapse0x8bdd3a0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDir::neuron0x8bdd010() {
   double input = input0x8bdd010();
   return (input * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8ba7200() {
   return (neuron0x8bcbb60()*-0.35758);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8b54c68() {
   return (neuron0x8bda130()*-0.371079);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8b54cb0() {
   return (neuron0x8bda258()*0.125571);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8b54d80() {
   return (neuron0x8bda3c8()*-0.139414);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bcbdd0() {
   return (neuron0x8bda5c8()*0.322936);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdaea8() {
   return (neuron0x8bda7c8()*-0.43352);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdaed0() {
   return (neuron0x8bda9c8()*-0.00806506);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb0f8() {
   return (neuron0x8bcbb60()*-0.039521);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb120() {
   return (neuron0x8bda130()*-0.109087);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb148() {
   return (neuron0x8bda258()*-0.023375);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb170() {
   return (neuron0x8bda3c8()*-0.0114902);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb198() {
   return (neuron0x8bda5c8()*0.105434);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb1c0() {
   return (neuron0x8bda7c8()*-0.125238);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb1e8() {
   return (neuron0x8bda9c8()*0.151391);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb410() {
   return (neuron0x8bcbb60()*0.191801);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb438() {
   return (neuron0x8bda130()*-0.269243);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb460() {
   return (neuron0x8bda258()*0.390296);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb510() {
   return (neuron0x8bda3c8()*0.258964);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb538() {
   return (neuron0x8bda5c8()*-0.012631);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb560() {
   return (neuron0x8bda7c8()*0.156646);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb588() {
   return (neuron0x8bda9c8()*-0.160989);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb768() {
   return (neuron0x8bcbb60()*0.135964);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb790() {
   return (neuron0x8bda130()*-0.348136);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb7b8() {
   return (neuron0x8bda258()*-0.0690674);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb7e0() {
   return (neuron0x8bda3c8()*-0.00217458);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb808() {
   return (neuron0x8bda5c8()*-0.459459);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb830() {
   return (neuron0x8bda7c8()*0.32677);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb858() {
   return (neuron0x8bda9c8()*-0.35319);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdba98() {
   return (neuron0x8bcbb60()*0.182813);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbac0() {
   return (neuron0x8bda130()*0.264898);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbae8() {
   return (neuron0x8bda258()*-0.394461);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbb10() {
   return (neuron0x8bda3c8()*0.264266);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbb38() {
   return (neuron0x8bda5c8()*0.377827);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb488() {
   return (neuron0x8bda7c8()*-0.43124);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdb4b0() {
   return (neuron0x8bda9c8()*0.293767);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbe68() {
   return (neuron0x8bcbb60()*0.25981);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbe90() {
   return (neuron0x8bda130()*0.0465771);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbeb8() {
   return (neuron0x8bda258()*-0.223641);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbee0() {
   return (neuron0x8bda3c8()*0.0164732);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbf08() {
   return (neuron0x8bda5c8()*-0.137776);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbf30() {
   return (neuron0x8bda7c8()*0.227076);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbf58() {
   return (neuron0x8bda9c8()*-0.184993);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc180() {
   return (neuron0x8bcbb60()*0.152059);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc1a8() {
   return (neuron0x8bda130()*-0.102624);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc1d0() {
   return (neuron0x8bda258()*-0.197564);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc1f8() {
   return (neuron0x8bda3c8()*-0.150636);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc220() {
   return (neuron0x8bda5c8()*0.0554645);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc248() {
   return (neuron0x8bda7c8()*0.467691);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc270() {
   return (neuron0x8bda9c8()*-0.0706198);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc4b0() {
   return (neuron0x8bcbb60()*-0.224027);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc4d8() {
   return (neuron0x8bda130()*0.0745117);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc500() {
   return (neuron0x8bda258()*0.331048);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc528() {
   return (neuron0x8bda3c8()*0.0847471);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc550() {
   return (neuron0x8bda5c8()*-0.115426);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc578() {
   return (neuron0x8bda7c8()*0.0439498);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc5a0() {
   return (neuron0x8bda9c8()*-0.250339);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc7e0() {
   return (neuron0x8bcbb60()*0.138225);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc808() {
   return (neuron0x8bda130()*-0.294605);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc830() {
   return (neuron0x8bda258()*-0.136354);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc858() {
   return (neuron0x8bda3c8()*0.0750767);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc880() {
   return (neuron0x8bda5c8()*0.0530083);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc8a8() {
   return (neuron0x8bda7c8()*0.0649561);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdc8d0() {
   return (neuron0x8bda9c8()*-0.10163);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcb98() {
   return (neuron0x8bcbb60()*-0.044751);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcbc0() {
   return (neuron0x8bda130()*0.464201);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8b54da8() {
   return (neuron0x8bda258()*-0.239047);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8ba71b8() {
   return (neuron0x8bda3c8()*-0.447996);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8b4c9e0() {
   return (neuron0x8bda5c8()*-0.12592);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbb60() {
   return (neuron0x8bda7c8()*0.225362);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdbb88() {
   return (neuron0x8bda9c8()*0.267194);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdce80() {
   return (neuron0x8bdacf0()*0.0049148);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcea8() {
   return (neuron0x8bdaef8()*0.0586479);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdced0() {
   return (neuron0x8bdb210()*-0.00178392);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcef8() {
   return (neuron0x8bdb5b0()*0.00601879);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcf20() {
   return (neuron0x8bdb880()*0.00384423);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcf48() {
   return (neuron0x8bdbc68()*0.0328654);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcf70() {
   return (neuron0x8bdbf80()*0.0151406);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcf98() {
   return (neuron0x8bdc298()*0.00722011);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcfc0() {
   return (neuron0x8bdc5c8()*-0.045233);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdcfe8() {
   return (neuron0x8bdc8f8()*-0.0154232);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd238() {
   return (neuron0x8bdacf0()*0.00320116);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd260() {
   return (neuron0x8bdaef8()*-0.23212);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd288() {
   return (neuron0x8bdb210()*-0.000468022);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd2b0() {
   return (neuron0x8bdb5b0()*0.021774);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd2d8() {
   return (neuron0x8bdb880()*0.0178177);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd300() {
   return (neuron0x8bdbc68()*-0.130044);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd328() {
   return (neuron0x8bdbf80()*0.0053599);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd350() {
   return (neuron0x8bdc298()*-0.0653438);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd378() {
   return (neuron0x8bdc5c8()*0.0385191);
}

double NNParaElectronAngleCorrectionBCPDir::synapse0x8bdd3a0() {
   return (neuron0x8bdc8f8()*2.9557e-05);
}

