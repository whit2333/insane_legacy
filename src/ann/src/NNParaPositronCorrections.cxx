#include "NNParaPositronCorrections.h"
#include <cmath>

double NNParaPositronCorrections::Value(int index, double in0, double in1, double in2, double in3, double in4, double in5, double in6)
{
   input0 = (in0 - 709.742) / 283.769;
   input1 = (in1 - -10.4329) / 29.7961;
   input2 = (in2 - 0.248384) / 59.6594;
   input3 = (in3 - 1.51601) / 0.493568;
   input4 = (in4 - 1.67833) / 0.553181;
   input5 = (in5 - 323.894) / 68887.8;
   input6 = (in6 - 0.89725) / 303.325;
   switch (index) {
      case 0:
         return neuron0x8b822d0();
      case 1:
         return neuron0x8b86bd8();
      case 2:
         return neuron0x8b87150();
      default:
         return 0.;
   }
}

double NNParaPositronCorrections::Value(int index, double* input)
{
   input0 = (input[0] - 709.742) / 283.769;
   input1 = (input[1] - -10.4329) / 29.7961;
   input2 = (input[2] - 0.248384) / 59.6594;
   input3 = (input[3] - 1.51601) / 0.493568;
   input4 = (input[4] - 1.67833) / 0.553181;
   input5 = (input[5] - 323.894) / 68887.8;
   input6 = (input[6] - 0.89725) / 303.325;
   switch (index) {
      case 0:
         return neuron0x8b822d0();
      case 1:
         return neuron0x8b86bd8();
      case 2:
         return neuron0x8b87150();
      default:
         return 0.;
   }
}

double NNParaPositronCorrections::neuron0x8b81530()
{
   return input0;
}

double NNParaPositronCorrections::neuron0x8b816a0()
{
   return input1;
}

double NNParaPositronCorrections::neuron0x8b818a0()
{
   return input2;
}

double NNParaPositronCorrections::neuron0x8b81aa0()
{
   return input3;
}

double NNParaPositronCorrections::neuron0x8b81ca0()
{
   return input4;
}

double NNParaPositronCorrections::neuron0x8b81ea0()
{
   return input5;
}

double NNParaPositronCorrections::neuron0x8b820b8()
{
   return input6;
}

double NNParaPositronCorrections::input0x8b823f8()
{
   double input = -13.5424;
   input += synapse0x8b825b0();
   input += synapse0x8b825d8();
   input += synapse0x8b82600();
   input += synapse0x8b82628();
   input += synapse0x8b82650();
   input += synapse0x8b82678();
   input += synapse0x8b826a0();
   return input;
}

double NNParaPositronCorrections::neuron0x8b823f8()
{
   double input = input0x8b823f8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b826c8()
{
   double input = -4.33461;
   input += synapse0x8b828c8();
   input += synapse0x8b828f0();
   input += synapse0x8b82918();
   input += synapse0x8b82940();
   input += synapse0x8b82968();
   input += synapse0x8b82990();
   input += synapse0x8b829b8();
   return input;
}

double NNParaPositronCorrections::neuron0x8b826c8()
{
   double input = input0x8b826c8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b829e0()
{
   double input = -19.4816;
   input += synapse0x8b82be0();
   input += synapse0x8b82c08();
   input += synapse0x8b82c30();
   input += synapse0x8b82ce0();
   input += synapse0x8b82d08();
   input += synapse0x8b82d30();
   input += synapse0x8b82d58();
   return input;
}

double NNParaPositronCorrections::neuron0x8b829e0()
{
   double input = input0x8b829e0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b82d80()
{
   double input = 3.95053;
   input += synapse0x8b82f38();
   input += synapse0x8b82f60();
   input += synapse0x8b82f88();
   input += synapse0x8b82fb0();
   input += synapse0x8b82fd8();
   input += synapse0x8b83000();
   input += synapse0x8b83028();
   return input;
}

double NNParaPositronCorrections::neuron0x8b82d80()
{
   double input = input0x8b82d80();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b83050()
{
   double input = -11.28;
   input += synapse0x8b83250();
   input += synapse0x8b83278();
   input += synapse0x8b832a0();
   input += synapse0x8b832c8();
   input += synapse0x8b832f0();
   input += synapse0x8b82c58();
   input += synapse0x8b82c80();
   return input;
}

double NNParaPositronCorrections::neuron0x8b83050()
{
   double input = input0x8b83050();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b83420()
{
   double input = -21.3415;
   input += synapse0x8b83620();
   input += synapse0x8b83648();
   input += synapse0x8b83670();
   input += synapse0x8b83698();
   input += synapse0x8b836c0();
   input += synapse0x8b836e8();
   input += synapse0x8b83710();
   return input;
}

double NNParaPositronCorrections::neuron0x8b83420()
{
   double input = input0x8b83420();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b83738()
{
   double input = -32.5588;
   input += synapse0x8b83950();
   input += synapse0x8b83978();
   input += synapse0x8b839a0();
   input += synapse0x8b839c8();
   input += synapse0x8b839f0();
   input += synapse0x8b83a18();
   input += synapse0x8b83a40();
   return input;
}

double NNParaPositronCorrections::neuron0x8b83738()
{
   double input = input0x8b83738();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b83a68()
{
   double input = 25.2153;
   input += synapse0x8b83c80();
   input += synapse0x8b83ca8();
   input += synapse0x8b83cd0();
   input += synapse0x8b83cf8();
   input += synapse0x8b83d20();
   input += synapse0x8b83d48();
   input += synapse0x8b83d70();
   return input;
}

double NNParaPositronCorrections::neuron0x8b83a68()
{
   double input = input0x8b83a68();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b83d98()
{
   double input = -25.2954;
   input += synapse0x8b83fb0();
   input += synapse0x8b83fd8();
   input += synapse0x8b84000();
   input += synapse0x8b84028();
   input += synapse0x8b84050();
   input += synapse0x8b84078();
   input += synapse0x8b840a0();
   return input;
}

double NNParaPositronCorrections::neuron0x8b83d98()
{
   double input = input0x8b83d98();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b840c8()
{
   double input = -24.5918;
   input += synapse0x8b84368();
   input += synapse0x8b84390();
   input += synapse0x8b611c0();
   input += synapse0x8b82ca8();
   input += synapse0x8afdfd0();
   input += synapse0x8b62670();
   input += synapse0x8afd938();
   return input;
}

double NNParaPositronCorrections::neuron0x8b840c8()
{
   double input = input0x8b840c8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b845c0()
{
   double input = -15.7588;
   input += synapse0x8b847c0();
   input += synapse0x8b847e8();
   input += synapse0x8b84810();
   input += synapse0x8b84838();
   input += synapse0x8b84860();
   input += synapse0x8b84888();
   input += synapse0x8b848b0();
   return input;
}

double NNParaPositronCorrections::neuron0x8b845c0()
{
   double input = input0x8b845c0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b848d8()
{
   double input = -27.3812;
   input += synapse0x8b84af0();
   input += synapse0x8b84b18();
   input += synapse0x8b84b40();
   input += synapse0x8b84b68();
   input += synapse0x8b84b90();
   input += synapse0x8b84bb8();
   input += synapse0x8b84be0();
   return input;
}

double NNParaPositronCorrections::neuron0x8b848d8()
{
   double input = input0x8b848d8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b84c08()
{
   double input = 14.0124;
   input += synapse0x8b84e20();
   input += synapse0x8b84e48();
   input += synapse0x8b84e70();
   input += synapse0x8b84e98();
   input += synapse0x8b84ec0();
   input += synapse0x8b84ee8();
   input += synapse0x8b84f10();
   return input;
}

double NNParaPositronCorrections::neuron0x8b84c08()
{
   double input = input0x8b84c08();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b84f38()
{
   double input = -0.339546;
   input += synapse0x8b85150();
   input += synapse0x8b85178();
   input += synapse0x8b851a0();
   input += synapse0x8b851c8();
   input += synapse0x8b851f0();
   input += synapse0x8b85218();
   input += synapse0x8b85240();
   return input;
}

double NNParaPositronCorrections::neuron0x8b84f38()
{
   double input = input0x8b84f38();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b85268()
{
   double input = -15.6029;
   input += synapse0x8b85480();
   input += synapse0x8b854a8();
   input += synapse0x8b854d0();
   input += synapse0x8b854f8();
   input += synapse0x8b85520();
   input += synapse0x8b85548();
   input += synapse0x8b85570();
   return input;
}

double NNParaPositronCorrections::neuron0x8b85268()
{
   double input = input0x8b85268();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b85598()
{
   double input = 1.60611;
   input += synapse0x8b857b0();
   input += synapse0x8b85860();
   input += synapse0x8b85910();
   input += synapse0x8b859c0();
   input += synapse0x8b85a70();
   input += synapse0x8b85b20();
   input += synapse0x8b85bd0();
   return input;
}

double NNParaPositronCorrections::neuron0x8b85598()
{
   double input = input0x8b85598();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b85c80()
{
   double input = 15.3041;
   input += synapse0x8b85dc0();
   input += synapse0x8b85de8();
   input += synapse0x8b85e10();
   input += synapse0x8b85e38();
   input += synapse0x8b85e60();
   input += synapse0x8b85e88();
   input += synapse0x8b85eb0();
   return input;
}

double NNParaPositronCorrections::neuron0x8b85c80()
{
   double input = input0x8b85c80();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b85ed8()
{
   double input = -27.1773;
   input += synapse0x8b86018();
   input += synapse0x8b86040();
   input += synapse0x8b86068();
   input += synapse0x8b86090();
   input += synapse0x8b860b8();
   input += synapse0x8b860e0();
   input += synapse0x8b86108();
   return input;
}

double NNParaPositronCorrections::neuron0x8b85ed8()
{
   double input = input0x8b85ed8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b86130()
{
   double input = -4.10013;
   input += synapse0x8b86300();
   input += synapse0x8b86328();
   input += synapse0x8b86350();
   input += synapse0x8afe0a0();
   input += synapse0x8afe0c8();
   input += synapse0x8b88350();
   input += synapse0x8b88378();
   return input;
}

double NNParaPositronCorrections::neuron0x8b86130()
{
   double input = input0x8b86130();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b843b8()
{
   double input = 4.43397;
   input += synapse0x8b883e8();
   input += synapse0x8b84588();
   input += synapse0x8b88410();
   input += synapse0x8b83318();
   input += synapse0x8b83340();
   input += synapse0x8b83368();
   input += synapse0x8b83390();
   return input;
}

double NNParaPositronCorrections::neuron0x8b843b8()
{
   double input = input0x8b843b8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaPositronCorrections::input0x8b822d0()
{
   double input = 117.334;
   input += synapse0x8b833f8();
   input += synapse0x8b86858();
   input += synapse0x8b86880();
   input += synapse0x8b868a8();
   input += synapse0x8b868d0();
   input += synapse0x8b868f8();
   input += synapse0x8b86920();
   input += synapse0x8b86948();
   input += synapse0x8b86970();
   input += synapse0x8b86998();
   input += synapse0x8b869c0();
   input += synapse0x8b869e8();
   input += synapse0x8b86a10();
   input += synapse0x8b86a38();
   input += synapse0x8b86a60();
   input += synapse0x8b86a88();
   input += synapse0x8b86b38();
   input += synapse0x8b86b60();
   input += synapse0x8b86b88();
   input += synapse0x8b86bb0();
   return input;
}

double NNParaPositronCorrections::neuron0x8b822d0()
{
   double input = input0x8b822d0();
   return (input * 1) + 0;
}

double NNParaPositronCorrections::input0x8b86bd8()
{
   double input = -0.24654;
   input += synapse0x8b86da8();
   input += synapse0x8b86dd0();
   input += synapse0x8b86df8();
   input += synapse0x8b86e20();
   input += synapse0x8b86e48();
   input += synapse0x8b86e70();
   input += synapse0x8b86e98();
   input += synapse0x8b86ec0();
   input += synapse0x8b86ee8();
   input += synapse0x8b86f10();
   input += synapse0x8b86f38();
   input += synapse0x8b86f60();
   input += synapse0x8b86f88();
   input += synapse0x8b86fb0();
   input += synapse0x8b86fd8();
   input += synapse0x8b87000();
   input += synapse0x8b870b0();
   input += synapse0x8b870d8();
   input += synapse0x8b87100();
   input += synapse0x8b87128();
   return input;
}

double NNParaPositronCorrections::neuron0x8b86bd8()
{
   double input = input0x8b86bd8();
   return (input * 1) + 0;
}

double NNParaPositronCorrections::input0x8b87150()
{
   double input = -0.300894;
   input += synapse0x8b833b8();
   input += synapse0x8b872c0();
   input += synapse0x8b884e0();
   input += synapse0x8b88508();
   input += synapse0x8b88530();
   input += synapse0x8b88558();
   input += synapse0x8b88580();
   input += synapse0x8b885a8();
   input += synapse0x8b885d0();
   input += synapse0x8b885f8();
   input += synapse0x8b88620();
   input += synapse0x8b88648();
   input += synapse0x8b88670();
   input += synapse0x8b88698();
   input += synapse0x8b886c0();
   input += synapse0x8b886e8();
   input += synapse0x8b88798();
   input += synapse0x8b887c0();
   input += synapse0x8b887e8();
   input += synapse0x8b88810();
   return input;
}

double NNParaPositronCorrections::neuron0x8b87150()
{
   double input = input0x8b87150();
   return (input * 1) + 0;
}

double NNParaPositronCorrections::synapse0x8b825b0()
{
   return (neuron0x8b81530() * 8.57099);
}

double NNParaPositronCorrections::synapse0x8b825d8()
{
   return (neuron0x8b816a0() * -13.7695);
}

double NNParaPositronCorrections::synapse0x8b82600()
{
   return (neuron0x8b818a0() * -3.93647);
}

double NNParaPositronCorrections::synapse0x8b82628()
{
   return (neuron0x8b81aa0() * -1.99177);
}

double NNParaPositronCorrections::synapse0x8b82650()
{
   return (neuron0x8b81ca0() * -0.157552);
}

double NNParaPositronCorrections::synapse0x8b82678()
{
   return (neuron0x8b81ea0() * -0.387876);
}

double NNParaPositronCorrections::synapse0x8b826a0()
{
   return (neuron0x8b820b8() * -1.02475);
}

double NNParaPositronCorrections::synapse0x8b828c8()
{
   return (neuron0x8b81530() * -0.135901);
}

double NNParaPositronCorrections::synapse0x8b828f0()
{
   return (neuron0x8b816a0() * 0.126288);
}

double NNParaPositronCorrections::synapse0x8b82918()
{
   return (neuron0x8b818a0() * 22.9636);
}

double NNParaPositronCorrections::synapse0x8b82940()
{
   return (neuron0x8b81aa0() * 0.285586);
}

double NNParaPositronCorrections::synapse0x8b82968()
{
   return (neuron0x8b81ca0() * 0.511567);
}

double NNParaPositronCorrections::synapse0x8b82990()
{
   return (neuron0x8b81ea0() * -0.079238);
}

double NNParaPositronCorrections::synapse0x8b829b8()
{
   return (neuron0x8b820b8() * -3.82778);
}

double NNParaPositronCorrections::synapse0x8b82be0()
{
   return (neuron0x8b81530() * 2.76425);
}

double NNParaPositronCorrections::synapse0x8b82c08()
{
   return (neuron0x8b816a0() * -12.5975);
}

double NNParaPositronCorrections::synapse0x8b82c30()
{
   return (neuron0x8b818a0() * -0.756443);
}

double NNParaPositronCorrections::synapse0x8b82ce0()
{
   return (neuron0x8b81aa0() * -1.86449);
}

double NNParaPositronCorrections::synapse0x8b82d08()
{
   return (neuron0x8b81ca0() * -0.0626943);
}

double NNParaPositronCorrections::synapse0x8b82d30()
{
   return (neuron0x8b81ea0() * -0.248194);
}

double NNParaPositronCorrections::synapse0x8b82d58()
{
   return (neuron0x8b820b8() * 9.85677);
}

double NNParaPositronCorrections::synapse0x8b82f38()
{
   return (neuron0x8b81530() * 3.65311);
}

double NNParaPositronCorrections::synapse0x8b82f60()
{
   return (neuron0x8b816a0() * -0.802699);
}

double NNParaPositronCorrections::synapse0x8b82f88()
{
   return (neuron0x8b818a0() * -4.79244);
}

double NNParaPositronCorrections::synapse0x8b82fb0()
{
   return (neuron0x8b81aa0() * 1.17565);
}

double NNParaPositronCorrections::synapse0x8b82fd8()
{
   return (neuron0x8b81ca0() * 1.22052);
}

double NNParaPositronCorrections::synapse0x8b83000()
{
   return (neuron0x8b81ea0() * -1.76075);
}

double NNParaPositronCorrections::synapse0x8b83028()
{
   return (neuron0x8b820b8() * -10.4233);
}

double NNParaPositronCorrections::synapse0x8b83250()
{
   return (neuron0x8b81530() * 4.20988);
}

double NNParaPositronCorrections::synapse0x8b83278()
{
   return (neuron0x8b816a0() * -3.32635);
}

double NNParaPositronCorrections::synapse0x8b832a0()
{
   return (neuron0x8b818a0() * 3.19163);
}

double NNParaPositronCorrections::synapse0x8b832c8()
{
   return (neuron0x8b81aa0() * 6.3939);
}

double NNParaPositronCorrections::synapse0x8b832f0()
{
   return (neuron0x8b81ca0() * 3.61935);
}

double NNParaPositronCorrections::synapse0x8b82c58()
{
   return (neuron0x8b81ea0() * 0.50549);
}

double NNParaPositronCorrections::synapse0x8b82c80()
{
   return (neuron0x8b820b8() * -0.937907);
}

double NNParaPositronCorrections::synapse0x8b83620()
{
   return (neuron0x8b81530() * -4.53534);
}

double NNParaPositronCorrections::synapse0x8b83648()
{
   return (neuron0x8b816a0() * 1.79995);
}

double NNParaPositronCorrections::synapse0x8b83670()
{
   return (neuron0x8b818a0() * -10.0948);
}

double NNParaPositronCorrections::synapse0x8b83698()
{
   return (neuron0x8b81aa0() * 0.672151);
}

double NNParaPositronCorrections::synapse0x8b836c0()
{
   return (neuron0x8b81ca0() * -1.42619);
}

double NNParaPositronCorrections::synapse0x8b836e8()
{
   return (neuron0x8b81ea0() * -0.437745);
}

double NNParaPositronCorrections::synapse0x8b83710()
{
   return (neuron0x8b820b8() * -4.14288);
}

double NNParaPositronCorrections::synapse0x8b83950()
{
   return (neuron0x8b81530() * -18.1993);
}

double NNParaPositronCorrections::synapse0x8b83978()
{
   return (neuron0x8b816a0() * 3.22828);
}

double NNParaPositronCorrections::synapse0x8b839a0()
{
   return (neuron0x8b818a0() * -4.38849);
}

double NNParaPositronCorrections::synapse0x8b839c8()
{
   return (neuron0x8b81aa0() * -0.127204);
}

double NNParaPositronCorrections::synapse0x8b839f0()
{
   return (neuron0x8b81ca0() * 0.772705);
}

double NNParaPositronCorrections::synapse0x8b83a18()
{
   return (neuron0x8b81ea0() * -0.574391);
}

double NNParaPositronCorrections::synapse0x8b83a40()
{
   return (neuron0x8b820b8() * -0.219154);
}

double NNParaPositronCorrections::synapse0x8b83c80()
{
   return (neuron0x8b81530() * -17.5169);
}

double NNParaPositronCorrections::synapse0x8b83ca8()
{
   return (neuron0x8b816a0() * -1.55005);
}

double NNParaPositronCorrections::synapse0x8b83cd0()
{
   return (neuron0x8b818a0() * -1.0369);
}

double NNParaPositronCorrections::synapse0x8b83cf8()
{
   return (neuron0x8b81aa0() * -1.32309);
}

double NNParaPositronCorrections::synapse0x8b83d20()
{
   return (neuron0x8b81ca0() * 0.281511);
}

double NNParaPositronCorrections::synapse0x8b83d48()
{
   return (neuron0x8b81ea0() * -0.178274);
}

double NNParaPositronCorrections::synapse0x8b83d70()
{
   return (neuron0x8b820b8() * 0.0019614);
}

double NNParaPositronCorrections::synapse0x8b83fb0()
{
   return (neuron0x8b81530() * -14.1906);
}

double NNParaPositronCorrections::synapse0x8b83fd8()
{
   return (neuron0x8b816a0() * 5.80162);
}

double NNParaPositronCorrections::synapse0x8b84000()
{
   return (neuron0x8b818a0() * -1.47195);
}

double NNParaPositronCorrections::synapse0x8b84028()
{
   return (neuron0x8b81aa0() * 0.112233);
}

double NNParaPositronCorrections::synapse0x8b84050()
{
   return (neuron0x8b81ca0() * 0.680061);
}

double NNParaPositronCorrections::synapse0x8b84078()
{
   return (neuron0x8b81ea0() * 0.567841);
}

double NNParaPositronCorrections::synapse0x8b840a0()
{
   return (neuron0x8b820b8() * 0.205182);
}

double NNParaPositronCorrections::synapse0x8b84368()
{
   return (neuron0x8b81530() * -0.829367);
}

double NNParaPositronCorrections::synapse0x8b84390()
{
   return (neuron0x8b816a0() * 0.404587);
}

double NNParaPositronCorrections::synapse0x8b611c0()
{
   return (neuron0x8b818a0() * -13.5532);
}

double NNParaPositronCorrections::synapse0x8b82ca8()
{
   return (neuron0x8b81aa0() * -0.329454);
}

double NNParaPositronCorrections::synapse0x8afdfd0()
{
   return (neuron0x8b81ca0() * -1.36411);
}

double NNParaPositronCorrections::synapse0x8b62670()
{
   return (neuron0x8b81ea0() * -0.879953);
}

double NNParaPositronCorrections::synapse0x8afd938()
{
   return (neuron0x8b820b8() * 3.10489);
}

double NNParaPositronCorrections::synapse0x8b847c0()
{
   return (neuron0x8b81530() * 14.0167);
}

double NNParaPositronCorrections::synapse0x8b847e8()
{
   return (neuron0x8b816a0() * -11.0734);
}

double NNParaPositronCorrections::synapse0x8b84810()
{
   return (neuron0x8b818a0() * 6.2425);
}

double NNParaPositronCorrections::synapse0x8b84838()
{
   return (neuron0x8b81aa0() * 0.312744);
}

double NNParaPositronCorrections::synapse0x8b84860()
{
   return (neuron0x8b81ca0() * -1.45474);
}

double NNParaPositronCorrections::synapse0x8b84888()
{
   return (neuron0x8b81ea0() * -0.931791);
}

double NNParaPositronCorrections::synapse0x8b848b0()
{
   return (neuron0x8b820b8() * -5.71158);
}

double NNParaPositronCorrections::synapse0x8b84af0()
{
   return (neuron0x8b81530() * -15.8269);
}

double NNParaPositronCorrections::synapse0x8b84b18()
{
   return (neuron0x8b816a0() * 2.13253);
}

double NNParaPositronCorrections::synapse0x8b84b40()
{
   return (neuron0x8b818a0() * -2.97929);
}

double NNParaPositronCorrections::synapse0x8b84b68()
{
   return (neuron0x8b81aa0() * 0.380756);
}

double NNParaPositronCorrections::synapse0x8b84b90()
{
   return (neuron0x8b81ca0() * 1.5274);
}

double NNParaPositronCorrections::synapse0x8b84bb8()
{
   return (neuron0x8b81ea0() * -0.23691);
}

double NNParaPositronCorrections::synapse0x8b84be0()
{
   return (neuron0x8b820b8() * -1.17724);
}

double NNParaPositronCorrections::synapse0x8b84e20()
{
   return (neuron0x8b81530() * 16.5207);
}

double NNParaPositronCorrections::synapse0x8b84e48()
{
   return (neuron0x8b816a0() * -3.65104);
}

double NNParaPositronCorrections::synapse0x8b84e70()
{
   return (neuron0x8b818a0() * 0.214356);
}

double NNParaPositronCorrections::synapse0x8b84e98()
{
   return (neuron0x8b81aa0() * 0.212539);
}

double NNParaPositronCorrections::synapse0x8b84ec0()
{
   return (neuron0x8b81ca0() * -1.91372);
}

double NNParaPositronCorrections::synapse0x8b84ee8()
{
   return (neuron0x8b81ea0() * 2.85425);
}

double NNParaPositronCorrections::synapse0x8b84f10()
{
   return (neuron0x8b820b8() * -3.87344);
}

double NNParaPositronCorrections::synapse0x8b85150()
{
   return (neuron0x8b81530() * 3.48368);
}

double NNParaPositronCorrections::synapse0x8b85178()
{
   return (neuron0x8b816a0() * -5.0091);
}

double NNParaPositronCorrections::synapse0x8b851a0()
{
   return (neuron0x8b818a0() * -1.71792);
}

double NNParaPositronCorrections::synapse0x8b851c8()
{
   return (neuron0x8b81aa0() * -0.897387);
}

double NNParaPositronCorrections::synapse0x8b851f0()
{
   return (neuron0x8b81ca0() * 0.604887);
}

double NNParaPositronCorrections::synapse0x8b85218()
{
   return (neuron0x8b81ea0() * 1.88745);
}

double NNParaPositronCorrections::synapse0x8b85240()
{
   return (neuron0x8b820b8() * 1.67158);
}

double NNParaPositronCorrections::synapse0x8b85480()
{
   return (neuron0x8b81530() * -0.489504);
}

double NNParaPositronCorrections::synapse0x8b854a8()
{
   return (neuron0x8b816a0() * 0.0403088);
}

double NNParaPositronCorrections::synapse0x8b854d0()
{
   return (neuron0x8b818a0() * -0.672236);
}

double NNParaPositronCorrections::synapse0x8b854f8()
{
   return (neuron0x8b81aa0() * 0.919002);
}

double NNParaPositronCorrections::synapse0x8b85520()
{
   return (neuron0x8b81ca0() * -11.188);
}

double NNParaPositronCorrections::synapse0x8b85548()
{
   return (neuron0x8b81ea0() * -0.0827613);
}

double NNParaPositronCorrections::synapse0x8b85570()
{
   return (neuron0x8b820b8() * -0.834044);
}

double NNParaPositronCorrections::synapse0x8b857b0()
{
   return (neuron0x8b81530() * 3.84756);
}

double NNParaPositronCorrections::synapse0x8b85860()
{
   return (neuron0x8b816a0() * -1.59436);
}

double NNParaPositronCorrections::synapse0x8b85910()
{
   return (neuron0x8b818a0() * 2.58043);
}

double NNParaPositronCorrections::synapse0x8b859c0()
{
   return (neuron0x8b81aa0() * 0.134912);
}

double NNParaPositronCorrections::synapse0x8b85a70()
{
   return (neuron0x8b81ca0() * 0.46828);
}

double NNParaPositronCorrections::synapse0x8b85b20()
{
   return (neuron0x8b81ea0() * -0.441186);
}

double NNParaPositronCorrections::synapse0x8b85bd0()
{
   return (neuron0x8b820b8() * -0.931364);
}

double NNParaPositronCorrections::synapse0x8b85dc0()
{
   return (neuron0x8b81530() * 0.158974);
}

double NNParaPositronCorrections::synapse0x8b85de8()
{
   return (neuron0x8b816a0() * -1.27953);
}

double NNParaPositronCorrections::synapse0x8b85e10()
{
   return (neuron0x8b818a0() * -0.381522);
}

double NNParaPositronCorrections::synapse0x8b85e38()
{
   return (neuron0x8b81aa0() * 2.41475);
}

double NNParaPositronCorrections::synapse0x8b85e60()
{
   return (neuron0x8b81ca0() * -1.60801);
}

double NNParaPositronCorrections::synapse0x8b85e88()
{
   return (neuron0x8b81ea0() * -0.223697);
}

double NNParaPositronCorrections::synapse0x8b85eb0()
{
   return (neuron0x8b820b8() * 0.382858);
}

double NNParaPositronCorrections::synapse0x8b86018()
{
   return (neuron0x8b81530() * -0.261875);
}

double NNParaPositronCorrections::synapse0x8b86040()
{
   return (neuron0x8b816a0() * -16.3196);
}

double NNParaPositronCorrections::synapse0x8b86068()
{
   return (neuron0x8b818a0() * 0.049201);
}

double NNParaPositronCorrections::synapse0x8b86090()
{
   return (neuron0x8b81aa0() * -0.992244);
}

double NNParaPositronCorrections::synapse0x8b860b8()
{
   return (neuron0x8b81ca0() * -0.248671);
}

double NNParaPositronCorrections::synapse0x8b860e0()
{
   return (neuron0x8b81ea0() * -2.35738);
}

double NNParaPositronCorrections::synapse0x8b86108()
{
   return (neuron0x8b820b8() * -5.75159);
}

double NNParaPositronCorrections::synapse0x8b86300()
{
   return (neuron0x8b81530() * 9.80514);
}

double NNParaPositronCorrections::synapse0x8b86328()
{
   return (neuron0x8b816a0() * -6.07718);
}

double NNParaPositronCorrections::synapse0x8b86350()
{
   return (neuron0x8b818a0() * 3.98778);
}

double NNParaPositronCorrections::synapse0x8afe0a0()
{
   return (neuron0x8b81aa0() * 0.0174581);
}

double NNParaPositronCorrections::synapse0x8afe0c8()
{
   return (neuron0x8b81ca0() * -1.20216);
}

double NNParaPositronCorrections::synapse0x8b88350()
{
   return (neuron0x8b81ea0() * -0.00298884);
}

double NNParaPositronCorrections::synapse0x8b88378()
{
   return (neuron0x8b820b8() * 6.21219);
}

double NNParaPositronCorrections::synapse0x8b883e8()
{
   return (neuron0x8b81530() * 9.7689);
}

double NNParaPositronCorrections::synapse0x8b84588()
{
   return (neuron0x8b816a0() * -5.13897);
}

double NNParaPositronCorrections::synapse0x8b88410()
{
   return (neuron0x8b818a0() * 2.48219);
}

double NNParaPositronCorrections::synapse0x8b83318()
{
   return (neuron0x8b81aa0() * -1.4652);
}

double NNParaPositronCorrections::synapse0x8b83340()
{
   return (neuron0x8b81ca0() * -1.13062);
}

double NNParaPositronCorrections::synapse0x8b83368()
{
   return (neuron0x8b81ea0() * 0.908111);
}

double NNParaPositronCorrections::synapse0x8b83390()
{
   return (neuron0x8b820b8() * 16.9404);
}

double NNParaPositronCorrections::synapse0x8b833f8()
{
   return (neuron0x8b823f8() * 76.1389);
}

double NNParaPositronCorrections::synapse0x8b86858()
{
   return (neuron0x8b826c8() * 94.2524);
}

double NNParaPositronCorrections::synapse0x8b86880()
{
   return (neuron0x8b829e0() * 95.1814);
}

double NNParaPositronCorrections::synapse0x8b868a8()
{
   return (neuron0x8b82d80() * 57.2925);
}

double NNParaPositronCorrections::synapse0x8b868d0()
{
   return (neuron0x8b83050() * 60.2659);
}

double NNParaPositronCorrections::synapse0x8b868f8()
{
   return (neuron0x8b83420() * 71.4543);
}

double NNParaPositronCorrections::synapse0x8b86920()
{
   return (neuron0x8b83738() * 75.5721);
}

double NNParaPositronCorrections::synapse0x8b86948()
{
   return (neuron0x8b83a68() * 52.9158);
}

double NNParaPositronCorrections::synapse0x8b86970()
{
   return (neuron0x8b83d98() * 53.5991);
}

double NNParaPositronCorrections::synapse0x8b86998()
{
   return (neuron0x8b840c8() * 83.1905);
}

double NNParaPositronCorrections::synapse0x8b869c0()
{
   return (neuron0x8b845c0() * 67.5584);
}

double NNParaPositronCorrections::synapse0x8b869e8()
{
   return (neuron0x8b848d8() * 68.481);
}

double NNParaPositronCorrections::synapse0x8b86a10()
{
   return (neuron0x8b84c08() * 64.6914);
}

double NNParaPositronCorrections::synapse0x8b86a38()
{
   return (neuron0x8b84f38() * 75.521);
}

double NNParaPositronCorrections::synapse0x8b86a60()
{
   return (neuron0x8b85268() * 61.1511);
}

double NNParaPositronCorrections::synapse0x8b86a88()
{
   return (neuron0x8b85598() * 83.8386);
}

double NNParaPositronCorrections::synapse0x8b86b38()
{
   return (neuron0x8b85c80() * 38.9357);
}

double NNParaPositronCorrections::synapse0x8b86b60()
{
   return (neuron0x8b85ed8() * 91.8368);
}

double NNParaPositronCorrections::synapse0x8b86b88()
{
   return (neuron0x8b86130() * 92.2823);
}

double NNParaPositronCorrections::synapse0x8b86bb0()
{
   return (neuron0x8b843b8() * 77.123);
}

double NNParaPositronCorrections::synapse0x8b86da8()
{
   return (neuron0x8b823f8() * 0.144797);
}

double NNParaPositronCorrections::synapse0x8b86dd0()
{
   return (neuron0x8b826c8() * -0.0895427);
}

double NNParaPositronCorrections::synapse0x8b86df8()
{
   return (neuron0x8b829e0() * 0.00349033);
}

double NNParaPositronCorrections::synapse0x8b86e20()
{
   return (neuron0x8b82d80() * 0.00751263);
}

double NNParaPositronCorrections::synapse0x8b86e48()
{
   return (neuron0x8b83050() * 0.110818);
}

double NNParaPositronCorrections::synapse0x8b86e70()
{
   return (neuron0x8b83420() * 0.333744);
}

double NNParaPositronCorrections::synapse0x8b86e98()
{
   return (neuron0x8b83738() * -0.282183);
}

double NNParaPositronCorrections::synapse0x8b86ec0()
{
   return (neuron0x8b83a68() * 0.0744218);
}

double NNParaPositronCorrections::synapse0x8b86ee8()
{
   return (neuron0x8b83d98() * -0.447592);
}

double NNParaPositronCorrections::synapse0x8b86f10()
{
   return (neuron0x8b840c8() * -0.115309);
}

double NNParaPositronCorrections::synapse0x8b86f38()
{
   return (neuron0x8b845c0() * 0.0532526);
}

double NNParaPositronCorrections::synapse0x8b86f60()
{
   return (neuron0x8b848d8() * 0.285015);
}

double NNParaPositronCorrections::synapse0x8b86f88()
{
   return (neuron0x8b84c08() * -0.140572);
}

double NNParaPositronCorrections::synapse0x8b86fb0()
{
   return (neuron0x8b84f38() * 0.0224515);
}

double NNParaPositronCorrections::synapse0x8b86fd8()
{
   return (neuron0x8b85268() * 0.0111445);
}

double NNParaPositronCorrections::synapse0x8b87000()
{
   return (neuron0x8b85598() * 0.0857239);
}

double NNParaPositronCorrections::synapse0x8b870b0()
{
   return (neuron0x8b85c80() * 0.105576);
}

double NNParaPositronCorrections::synapse0x8b870d8()
{
   return (neuron0x8b85ed8() * 0.194704);
}

double NNParaPositronCorrections::synapse0x8b87100()
{
   return (neuron0x8b86130() * -0.148961);
}

double NNParaPositronCorrections::synapse0x8b87128()
{
   return (neuron0x8b843b8() * -0.0141158);
}

double NNParaPositronCorrections::synapse0x8b833b8()
{
   return (neuron0x8b823f8() * -0.180785);
}

double NNParaPositronCorrections::synapse0x8b872c0()
{
   return (neuron0x8b826c8() * 0.129404);
}

double NNParaPositronCorrections::synapse0x8b884e0()
{
   return (neuron0x8b829e0() * 0.0362076);
}

double NNParaPositronCorrections::synapse0x8b88508()
{
   return (neuron0x8b82d80() * 0.104011);
}

double NNParaPositronCorrections::synapse0x8b88530()
{
   return (neuron0x8b83050() * -0.0083086);
}

double NNParaPositronCorrections::synapse0x8b88558()
{
   return (neuron0x8b83420() * 0.415703);
}

double NNParaPositronCorrections::synapse0x8b88580()
{
   return (neuron0x8b83738() * 0.365456);
}

double NNParaPositronCorrections::synapse0x8b885a8()
{
   return (neuron0x8b83a68() * -0.116871);
}

double NNParaPositronCorrections::synapse0x8b885d0()
{
   return (neuron0x8b83d98() * -0.483528);
}

double NNParaPositronCorrections::synapse0x8b885f8()
{
   return (neuron0x8b840c8() * 0.0775805);
}

double NNParaPositronCorrections::synapse0x8b88620()
{
   return (neuron0x8b845c0() * 0.089687);
}

double NNParaPositronCorrections::synapse0x8b88648()
{
   return (neuron0x8b848d8() * -0.311198);
}

double NNParaPositronCorrections::synapse0x8b88670()
{
   return (neuron0x8b84c08() * 0.0886267);
}

double NNParaPositronCorrections::synapse0x8b88698()
{
   return (neuron0x8b84f38() * -0.124503);
}

double NNParaPositronCorrections::synapse0x8b886c0()
{
   return (neuron0x8b85268() * 0.179279);
}

double NNParaPositronCorrections::synapse0x8b886e8()
{
   return (neuron0x8b85598() * -0.197813);
}

double NNParaPositronCorrections::synapse0x8b88798()
{
   return (neuron0x8b85c80() * 0.0635699);
}

double NNParaPositronCorrections::synapse0x8b887c0()
{
   return (neuron0x8b85ed8() * -0.215631);
}

double NNParaPositronCorrections::synapse0x8b887e8()
{
   return (neuron0x8b86130() * 0.201752);
}

double NNParaPositronCorrections::synapse0x8b88810()
{
   return (neuron0x8b843b8() * 0.165642);
}

