#include "NNPerpElectronCorrectionTheta.h"
#include <cmath>

double NNPerpElectronCorrectionTheta::Value(int index, double in0, double in1, double in2, double in3, double in4, double in5, double in6)
{
   input0 = (in0 - 1807.44) / 957.222;
   input1 = (in1 - -4.86487) / 33.9831;
   input2 = (in2 - -10.8703) / 58.8882;
   input3 = (in3 - 1.39247) / 0.359494;
   input4 = (in4 - 1.52167) / 0.372969;
   input5 = (in5 - 1.12442) / 50.8091;
   input6 = (in6 - 0.205599) / 7.17982;
   switch (index) {
      case 0:
         return neuron0x8dfdf20();
      default:
         return 0.;
   }
}

double NNPerpElectronCorrectionTheta::Value(int index, double* input)
{
   input0 = (input[0] - 1807.44) / 957.222;
   input1 = (input[1] - -4.86487) / 33.9831;
   input2 = (input[2] - -10.8703) / 58.8882;
   input3 = (input[3] - 1.39247) / 0.359494;
   input4 = (input[4] - 1.52167) / 0.372969;
   input5 = (input[5] - 1.12442) / 50.8091;
   input6 = (input[6] - 0.205599) / 7.17982;
   switch (index) {
      case 0:
         return neuron0x8dfdf20();
      default:
         return 0.;
   }
}

double NNPerpElectronCorrectionTheta::neuron0x8df6d10()
{
   return input0;
}

double NNPerpElectronCorrectionTheta::neuron0x8df6ec8()
{
   return input1;
}

double NNPerpElectronCorrectionTheta::neuron0x8df70c8()
{
   return input2;
}

double NNPerpElectronCorrectionTheta::neuron0x8df72c8()
{
   return input3;
}

double NNPerpElectronCorrectionTheta::neuron0x8df74c8()
{
   return input4;
}

double NNPerpElectronCorrectionTheta::neuron0x8df76c8()
{
   return input5;
}

double NNPerpElectronCorrectionTheta::neuron0x8df78e0()
{
   return input6;
}

double NNPerpElectronCorrectionTheta::input0x8df7c20()
{
   double input = 0.0220878;
   input += synapse0x8df7dd8();
   input += synapse0x8df7e00();
   input += synapse0x8df7e28();
   input += synapse0x8df7e50();
   input += synapse0x8df7e78();
   input += synapse0x8df7ea0();
   input += synapse0x8df7ec8();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df7c20()
{
   double input = input0x8df7c20();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df7ef0()
{
   double input = 1.28464;
   input += synapse0x8df80f0();
   input += synapse0x8df8118();
   input += synapse0x8df8140();
   input += synapse0x8df8168();
   input += synapse0x8df8190();
   input += synapse0x8df81b8();
   input += synapse0x8df81e0();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df7ef0()
{
   double input = input0x8df7ef0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df8208()
{
   double input = 0.158123;
   input += synapse0x8df8408();
   input += synapse0x8df8430();
   input += synapse0x8df8458();
   input += synapse0x8df8508();
   input += synapse0x8df8530();
   input += synapse0x8df8558();
   input += synapse0x8df8580();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df8208()
{
   double input = input0x8df8208();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df85a8()
{
   double input = -0.685639;
   input += synapse0x8df8760();
   input += synapse0x8df8788();
   input += synapse0x8df87b0();
   input += synapse0x8df87d8();
   input += synapse0x8df8800();
   input += synapse0x8df8828();
   input += synapse0x8df8850();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df85a8()
{
   double input = input0x8df85a8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df8878()
{
   double input = -0.399158;
   input += synapse0x8df8a78();
   input += synapse0x8df8aa0();
   input += synapse0x8df8ac8();
   input += synapse0x8df8af0();
   input += synapse0x8df8b18();
   input += synapse0x8d082b0();
   input += synapse0x8df8480();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df8878()
{
   double input = input0x8df8878();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df8c48()
{
   double input = -0.126531;
   input += synapse0x8df8e48();
   input += synapse0x8df8e70();
   input += synapse0x8df8e98();
   input += synapse0x8df8ec0();
   input += synapse0x8df8ee8();
   input += synapse0x8df8f10();
   input += synapse0x8df8f38();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df8c48()
{
   double input = input0x8df8c48();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df8f60()
{
   double input = -0.206845;
   input += synapse0x8df9178();
   input += synapse0x8df91a0();
   input += synapse0x8df91c8();
   input += synapse0x8df91f0();
   input += synapse0x8df9218();
   input += synapse0x8df9240();
   input += synapse0x8df9268();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df8f60()
{
   double input = input0x8df8f60();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df9290()
{
   double input = 0.244798;
   input += synapse0x8df94a8();
   input += synapse0x8df94d0();
   input += synapse0x8df94f8();
   input += synapse0x8df9520();
   input += synapse0x8df9548();
   input += synapse0x8df9570();
   input += synapse0x8df9598();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df9290()
{
   double input = input0x8df9290();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df95c0()
{
   double input = 0.311956;
   input += synapse0x8df97d8();
   input += synapse0x8df9800();
   input += synapse0x8df9828();
   input += synapse0x8df9850();
   input += synapse0x8df9878();
   input += synapse0x8df98a0();
   input += synapse0x8df98c8();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df95c0()
{
   double input = input0x8df95c0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df98f0()
{
   double input = -0.238219;
   input += synapse0x8df9b90();
   input += synapse0x8df9bb8();
   input += synapse0x8d19540();
   input += synapse0x8d195f0();
   input += synapse0x8d191b8();
   input += synapse0x8d08260();
   input += synapse0x8d08288();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df98f0()
{
   double input = input0x8df98f0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df9de8()
{
   double input = -0.142499;
   input += synapse0x8df9fe8();
   input += synapse0x8dfa010();
   input += synapse0x8dfa038();
   input += synapse0x8dfa060();
   input += synapse0x8dfa088();
   input += synapse0x8dfa0b0();
   input += synapse0x8dfa0d8();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df9de8()
{
   double input = input0x8df9de8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfa100()
{
   double input = -0.450047;
   input += synapse0x8dfa318();
   input += synapse0x8dfa340();
   input += synapse0x8dfa368();
   input += synapse0x8dfa390();
   input += synapse0x8dfa3b8();
   input += synapse0x8dfa3e0();
   input += synapse0x8dfa408();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfa100()
{
   double input = input0x8dfa100();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfa430()
{
   double input = 0.113328;
   input += synapse0x8dfa648();
   input += synapse0x8dfa670();
   input += synapse0x8dfa698();
   input += synapse0x8dfa6c0();
   input += synapse0x8dfa6e8();
   input += synapse0x8dfa710();
   input += synapse0x8dfa738();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfa430()
{
   double input = input0x8dfa430();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfa760()
{
   double input = 0.343042;
   input += synapse0x8dfa978();
   input += synapse0x8dfa9a0();
   input += synapse0x8dfa9c8();
   input += synapse0x8dfa9f0();
   input += synapse0x8dfaa18();
   input += synapse0x8dfaa40();
   input += synapse0x8dfaa68();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfa760()
{
   double input = input0x8dfa760();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfaa90()
{
   double input = 0.413865;
   input += synapse0x8dfaca8();
   input += synapse0x8dfacd0();
   input += synapse0x8dfacf8();
   input += synapse0x8dfad20();
   input += synapse0x8dfad48();
   input += synapse0x8dfad70();
   input += synapse0x8dfad98();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfaa90()
{
   double input = input0x8dfaa90();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfadc0()
{
   double input = -0.722478;
   input += synapse0x8dfafd8();
   input += synapse0x8dfb088();
   input += synapse0x8dfb138();
   input += synapse0x8dfb1e8();
   input += synapse0x8dfb298();
   input += synapse0x8dfb348();
   input += synapse0x8dfb3f8();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfadc0()
{
   double input = input0x8dfadc0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfb4a8()
{
   double input = -0.867652;
   input += synapse0x8dfb5e8();
   input += synapse0x8dfb610();
   input += synapse0x8dfb638();
   input += synapse0x8dfb660();
   input += synapse0x8dfb688();
   input += synapse0x8dfb6b0();
   input += synapse0x8dfb6d8();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfb4a8()
{
   double input = input0x8dfb4a8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfb700()
{
   double input = -0.271026;
   input += synapse0x8dfb840();
   input += synapse0x8dfb868();
   input += synapse0x8dfb890();
   input += synapse0x8dfb8b8();
   input += synapse0x8dfb8e0();
   input += synapse0x8dfb908();
   input += synapse0x8dfb930();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfb700()
{
   double input = input0x8dfb700();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfb958()
{
   double input = -0.0538874;
   input += synapse0x8dfbb28();
   input += synapse0x8dfbb50();
   input += synapse0x8dfbb78();
   input += synapse0x8df84a8();
   input += synapse0x8df84d0();
   input += synapse0x8dd6ee8();
   input += synapse0x8dd6f10();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfb958()
{
   double input = input0x8dfb958();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8df9be0()
{
   double input = 0.214133;
   input += synapse0x8df9db0();
   input += synapse0x8df8b40();
   input += synapse0x8df8b68();
   input += synapse0x8df8b90();
   input += synapse0x8df8bb8();
   input += synapse0x8df8be0();
   input += synapse0x8df8c08();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8df9be0()
{
   double input = input0x8df9be0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfbfa8()
{
   double input = 0.232006;
   input += synapse0x8dfc1a8();
   input += synapse0x8dfc1d0();
   input += synapse0x8dfc1f8();
   input += synapse0x8dfc220();
   input += synapse0x8dfc248();
   input += synapse0x8dfc270();
   input += synapse0x8dfc298();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfbfa8()
{
   double input = input0x8dfbfa8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfc2c0()
{
   double input = -0.334146;
   input += synapse0x8dfc4d8();
   input += synapse0x8dfc500();
   input += synapse0x8dfc528();
   input += synapse0x8dfc550();
   input += synapse0x8dfc578();
   input += synapse0x8dfc5a0();
   input += synapse0x8dfc5c8();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfc2c0()
{
   double input = input0x8dfc2c0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfc5f0()
{
   double input = -0.420213;
   input += synapse0x8dfc808();
   input += synapse0x8dfc830();
   input += synapse0x8dfc858();
   input += synapse0x8dfc880();
   input += synapse0x8dfc8a8();
   input += synapse0x8dfc8d0();
   input += synapse0x8dfc8f8();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfc5f0()
{
   double input = input0x8dfc5f0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfc920()
{
   double input = 0.553067;
   input += synapse0x8dfcaa8();
   input += synapse0x8dfdb18();
   input += synapse0x8dfdb40();
   input += synapse0x8dfdb68();
   input += synapse0x8dfdb90();
   input += synapse0x8dfdbb8();
   input += synapse0x8dfdbe0();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfc920()
{
   double input = input0x8dfc920();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfdc08()
{
   double input = 0.205524;
   input += synapse0x8dfde08();
   input += synapse0x8dfde30();
   input += synapse0x8dfde58();
   input += synapse0x8dfde80();
   input += synapse0x8dfdea8();
   input += synapse0x8dfded0();
   input += synapse0x8dfdef8();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfdc08()
{
   double input = input0x8dfdc08();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionTheta::input0x8dfdf20()
{
   double input = 0.394445;
   input += synapse0x8df7bf8();
   input += synapse0x8dfe048();
   input += synapse0x8dfe070();
   input += synapse0x8dfe098();
   input += synapse0x8dfe0c0();
   input += synapse0x8dfe0e8();
   input += synapse0x8dfe110();
   input += synapse0x8dfe138();
   input += synapse0x8dfe160();
   input += synapse0x8dfe188();
   input += synapse0x8dfe1b0();
   input += synapse0x8dfe1d8();
   input += synapse0x8dfe200();
   input += synapse0x8dfe228();
   input += synapse0x8dfe250();
   input += synapse0x8dfe278();
   input += synapse0x8dfe328();
   input += synapse0x8dfe350();
   input += synapse0x8dfe378();
   input += synapse0x8dfe3a0();
   input += synapse0x8dfe3c8();
   input += synapse0x8dfe3f0();
   input += synapse0x8dfe418();
   input += synapse0x8dfe440();
   input += synapse0x8dfe468();
   return input;
}

double NNPerpElectronCorrectionTheta::neuron0x8dfdf20()
{
   double input = input0x8dfdf20();
   return (input * 1) + 0;
}

double NNPerpElectronCorrectionTheta::synapse0x8df7dd8()
{
   return (neuron0x8df6d10() * -0.473217);
}

double NNPerpElectronCorrectionTheta::synapse0x8df7e00()
{
   return (neuron0x8df6ec8() * 0.182833);
}

double NNPerpElectronCorrectionTheta::synapse0x8df7e28()
{
   return (neuron0x8df70c8() * -0.241365);
}

double NNPerpElectronCorrectionTheta::synapse0x8df7e50()
{
   return (neuron0x8df72c8() * 0.477359);
}

double NNPerpElectronCorrectionTheta::synapse0x8df7e78()
{
   return (neuron0x8df74c8() * 0.099328);
}

double NNPerpElectronCorrectionTheta::synapse0x8df7ea0()
{
   return (neuron0x8df76c8() * -0.49621);
}

double NNPerpElectronCorrectionTheta::synapse0x8df7ec8()
{
   return (neuron0x8df78e0() * -0.226289);
}

double NNPerpElectronCorrectionTheta::synapse0x8df80f0()
{
   return (neuron0x8df6d10() * 0.522716);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8118()
{
   return (neuron0x8df6ec8() * 0.183116);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8140()
{
   return (neuron0x8df70c8() * -0.581074);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8168()
{
   return (neuron0x8df72c8() * 0.0180362);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8190()
{
   return (neuron0x8df74c8() * 0.0237832);
}

double NNPerpElectronCorrectionTheta::synapse0x8df81b8()
{
   return (neuron0x8df76c8() * 0.0475221);
}

double NNPerpElectronCorrectionTheta::synapse0x8df81e0()
{
   return (neuron0x8df78e0() * -0.0581793);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8408()
{
   return (neuron0x8df6d10() * -0.398339);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8430()
{
   return (neuron0x8df6ec8() * 0.083063);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8458()
{
   return (neuron0x8df70c8() * -0.580561);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8508()
{
   return (neuron0x8df72c8() * 0.11197);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8530()
{
   return (neuron0x8df74c8() * 0.0198749);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8558()
{
   return (neuron0x8df76c8() * -0.412444);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8580()
{
   return (neuron0x8df78e0() * -0.207544);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8760()
{
   return (neuron0x8df6d10() * -0.202541);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8788()
{
   return (neuron0x8df6ec8() * 0.502519);
}

double NNPerpElectronCorrectionTheta::synapse0x8df87b0()
{
   return (neuron0x8df70c8() * -0.144882);
}

double NNPerpElectronCorrectionTheta::synapse0x8df87d8()
{
   return (neuron0x8df72c8() * 0.323491);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8800()
{
   return (neuron0x8df74c8() * -0.0352773);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8828()
{
   return (neuron0x8df76c8() * -0.215782);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8850()
{
   return (neuron0x8df78e0() * -0.454762);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8a78()
{
   return (neuron0x8df6d10() * -0.166103);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8aa0()
{
   return (neuron0x8df6ec8() * 0.154715);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8ac8()
{
   return (neuron0x8df70c8() * -0.487599);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8af0()
{
   return (neuron0x8df72c8() * 0.066553);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8b18()
{
   return (neuron0x8df74c8() * 0.382026);
}

double NNPerpElectronCorrectionTheta::synapse0x8d082b0()
{
   return (neuron0x8df76c8() * 0.162035);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8480()
{
   return (neuron0x8df78e0() * 0.0551296);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8e48()
{
   return (neuron0x8df6d10() * 0.0436674);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8e70()
{
   return (neuron0x8df6ec8() * -0.142618);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8e98()
{
   return (neuron0x8df70c8() * -0.267879);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8ec0()
{
   return (neuron0x8df72c8() * -0.334264);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8ee8()
{
   return (neuron0x8df74c8() * 0.123439);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8f10()
{
   return (neuron0x8df76c8() * -0.370489);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8f38()
{
   return (neuron0x8df78e0() * -0.284964);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9178()
{
   return (neuron0x8df6d10() * 0.0189221);
}

double NNPerpElectronCorrectionTheta::synapse0x8df91a0()
{
   return (neuron0x8df6ec8() * -0.180247);
}

double NNPerpElectronCorrectionTheta::synapse0x8df91c8()
{
   return (neuron0x8df70c8() * 0.353825);
}

double NNPerpElectronCorrectionTheta::synapse0x8df91f0()
{
   return (neuron0x8df72c8() * -0.503958);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9218()
{
   return (neuron0x8df74c8() * -0.181539);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9240()
{
   return (neuron0x8df76c8() * 0.342772);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9268()
{
   return (neuron0x8df78e0() * 0.386462);
}

double NNPerpElectronCorrectionTheta::synapse0x8df94a8()
{
   return (neuron0x8df6d10() * 0.303249);
}

double NNPerpElectronCorrectionTheta::synapse0x8df94d0()
{
   return (neuron0x8df6ec8() * 0.204141);
}

double NNPerpElectronCorrectionTheta::synapse0x8df94f8()
{
   return (neuron0x8df70c8() * -0.436893);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9520()
{
   return (neuron0x8df72c8() * -0.0325108);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9548()
{
   return (neuron0x8df74c8() * -0.30253);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9570()
{
   return (neuron0x8df76c8() * 0.25743);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9598()
{
   return (neuron0x8df78e0() * -0.464338);
}

double NNPerpElectronCorrectionTheta::synapse0x8df97d8()
{
   return (neuron0x8df6d10() * 0.52803);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9800()
{
   return (neuron0x8df6ec8() * -0.284472);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9828()
{
   return (neuron0x8df70c8() * -0.0761348);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9850()
{
   return (neuron0x8df72c8() * 0.0481259);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9878()
{
   return (neuron0x8df74c8() * -0.357444);
}

double NNPerpElectronCorrectionTheta::synapse0x8df98a0()
{
   return (neuron0x8df76c8() * 0.351189);
}

double NNPerpElectronCorrectionTheta::synapse0x8df98c8()
{
   return (neuron0x8df78e0() * 0.349468);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9b90()
{
   return (neuron0x8df6d10() * 0.673069);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9bb8()
{
   return (neuron0x8df6ec8() * 0.235632);
}

double NNPerpElectronCorrectionTheta::synapse0x8d19540()
{
   return (neuron0x8df70c8() * -0.243595);
}

double NNPerpElectronCorrectionTheta::synapse0x8d195f0()
{
   return (neuron0x8df72c8() * 0.103999);
}

double NNPerpElectronCorrectionTheta::synapse0x8d191b8()
{
   return (neuron0x8df74c8() * 0.277044);
}

double NNPerpElectronCorrectionTheta::synapse0x8d08260()
{
   return (neuron0x8df76c8() * -0.282173);
}

double NNPerpElectronCorrectionTheta::synapse0x8d08288()
{
   return (neuron0x8df78e0() * 0.439735);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9fe8()
{
   return (neuron0x8df6d10() * 0.528491);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa010()
{
   return (neuron0x8df6ec8() * -0.0351283);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa038()
{
   return (neuron0x8df70c8() * 0.75897);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa060()
{
   return (neuron0x8df72c8() * 0.0348068);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa088()
{
   return (neuron0x8df74c8() * -0.185063);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa0b0()
{
   return (neuron0x8df76c8() * -0.368139);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa0d8()
{
   return (neuron0x8df78e0() * -0.373618);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa318()
{
   return (neuron0x8df6d10() * -0.282838);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa340()
{
   return (neuron0x8df6ec8() * -0.0743883);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa368()
{
   return (neuron0x8df70c8() * -0.145494);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa390()
{
   return (neuron0x8df72c8() * -0.156721);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa3b8()
{
   return (neuron0x8df74c8() * -0.367821);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa3e0()
{
   return (neuron0x8df76c8() * -0.159186);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa408()
{
   return (neuron0x8df78e0() * -0.312393);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa648()
{
   return (neuron0x8df6d10() * 0.11118);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa670()
{
   return (neuron0x8df6ec8() * 0.308096);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa698()
{
   return (neuron0x8df70c8() * -0.0173244);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa6c0()
{
   return (neuron0x8df72c8() * -0.14463);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa6e8()
{
   return (neuron0x8df74c8() * -0.317523);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa710()
{
   return (neuron0x8df76c8() * -0.244112);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa738()
{
   return (neuron0x8df78e0() * -0.159643);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa978()
{
   return (neuron0x8df6d10() * -0.419037);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa9a0()
{
   return (neuron0x8df6ec8() * 0.0895063);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa9c8()
{
   return (neuron0x8df70c8() * 0.379661);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfa9f0()
{
   return (neuron0x8df72c8() * 0.315893);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfaa18()
{
   return (neuron0x8df74c8() * 0.0978871);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfaa40()
{
   return (neuron0x8df76c8() * -0.465066);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfaa68()
{
   return (neuron0x8df78e0() * 0.233847);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfaca8()
{
   return (neuron0x8df6d10() * 0.117451);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfacd0()
{
   return (neuron0x8df6ec8() * 0.0516931);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfacf8()
{
   return (neuron0x8df70c8() * 0.523122);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfad20()
{
   return (neuron0x8df72c8() * -0.121624);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfad48()
{
   return (neuron0x8df74c8() * 0.0203929);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfad70()
{
   return (neuron0x8df76c8() * 0.446559);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfad98()
{
   return (neuron0x8df78e0() * -0.167096);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfafd8()
{
   return (neuron0x8df6d10() * -0.405655);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb088()
{
   return (neuron0x8df6ec8() * 0.0448345);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb138()
{
   return (neuron0x8df70c8() * 0.348073);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb1e8()
{
   return (neuron0x8df72c8() * 0.196485);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb298()
{
   return (neuron0x8df74c8() * -0.144389);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb348()
{
   return (neuron0x8df76c8() * -0.0749583);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb3f8()
{
   return (neuron0x8df78e0() * -0.343139);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb5e8()
{
   return (neuron0x8df6d10() * -0.119962);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb610()
{
   return (neuron0x8df6ec8() * -0.222578);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb638()
{
   return (neuron0x8df70c8() * -0.877791);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb660()
{
   return (neuron0x8df72c8() * -0.125353);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb688()
{
   return (neuron0x8df74c8() * -0.068008);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb6b0()
{
   return (neuron0x8df76c8() * -0.292143);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb6d8()
{
   return (neuron0x8df78e0() * -0.336062);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb840()
{
   return (neuron0x8df6d10() * -0.155982);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb868()
{
   return (neuron0x8df6ec8() * 0.297383);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb890()
{
   return (neuron0x8df70c8() * -0.0941407);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb8b8()
{
   return (neuron0x8df72c8() * -0.0404815);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb8e0()
{
   return (neuron0x8df74c8() * -0.276499);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb908()
{
   return (neuron0x8df76c8() * 0.398978);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfb930()
{
   return (neuron0x8df78e0() * 0.0718534);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfbb28()
{
   return (neuron0x8df6d10() * -0.306755);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfbb50()
{
   return (neuron0x8df6ec8() * -0.175392);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfbb78()
{
   return (neuron0x8df70c8() * -0.161191);
}

double NNPerpElectronCorrectionTheta::synapse0x8df84a8()
{
   return (neuron0x8df72c8() * 0.419646);
}

double NNPerpElectronCorrectionTheta::synapse0x8df84d0()
{
   return (neuron0x8df74c8() * -0.348142);
}

double NNPerpElectronCorrectionTheta::synapse0x8dd6ee8()
{
   return (neuron0x8df76c8() * 0.190455);
}

double NNPerpElectronCorrectionTheta::synapse0x8dd6f10()
{
   return (neuron0x8df78e0() * -0.295591);
}

double NNPerpElectronCorrectionTheta::synapse0x8df9db0()
{
   return (neuron0x8df6d10() * -0.305405);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8b40()
{
   return (neuron0x8df6ec8() * 0.162676);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8b68()
{
   return (neuron0x8df70c8() * -0.182995);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8b90()
{
   return (neuron0x8df72c8() * -0.305774);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8bb8()
{
   return (neuron0x8df74c8() * 0.452369);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8be0()
{
   return (neuron0x8df76c8() * 0.458444);
}

double NNPerpElectronCorrectionTheta::synapse0x8df8c08()
{
   return (neuron0x8df78e0() * 0.321082);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc1a8()
{
   return (neuron0x8df6d10() * -0.987569);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc1d0()
{
   return (neuron0x8df6ec8() * -0.165731);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc1f8()
{
   return (neuron0x8df70c8() * 0.328314);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc220()
{
   return (neuron0x8df72c8() * 0.00268775);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc248()
{
   return (neuron0x8df74c8() * -0.00128679);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc270()
{
   return (neuron0x8df76c8() * -0.442699);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc298()
{
   return (neuron0x8df78e0() * 0.307124);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc4d8()
{
   return (neuron0x8df6d10() * -0.0238176);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc500()
{
   return (neuron0x8df6ec8() * -0.429121);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc528()
{
   return (neuron0x8df70c8() * -0.145534);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc550()
{
   return (neuron0x8df72c8() * -0.494444);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc578()
{
   return (neuron0x8df74c8() * -0.0278008);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc5a0()
{
   return (neuron0x8df76c8() * 0.198674);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc5c8()
{
   return (neuron0x8df78e0() * -0.353577);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc808()
{
   return (neuron0x8df6d10() * -0.563031);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc830()
{
   return (neuron0x8df6ec8() * 0.270421);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc858()
{
   return (neuron0x8df70c8() * -0.0870996);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc880()
{
   return (neuron0x8df72c8() * 0.175508);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc8a8()
{
   return (neuron0x8df74c8() * 0.165572);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc8d0()
{
   return (neuron0x8df76c8() * -0.419939);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfc8f8()
{
   return (neuron0x8df78e0() * 0.431609);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfcaa8()
{
   return (neuron0x8df6d10() * 0.197019);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfdb18()
{
   return (neuron0x8df6ec8() * -0.0650732);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfdb40()
{
   return (neuron0x8df70c8() * 0.507068);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfdb68()
{
   return (neuron0x8df72c8() * -0.153723);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfdb90()
{
   return (neuron0x8df74c8() * -0.254541);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfdbb8()
{
   return (neuron0x8df76c8() * -0.47414);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfdbe0()
{
   return (neuron0x8df78e0() * -0.337177);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfde08()
{
   return (neuron0x8df6d10() * -0.474389);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfde30()
{
   return (neuron0x8df6ec8() * 0.273585);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfde58()
{
   return (neuron0x8df70c8() * 0.367958);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfde80()
{
   return (neuron0x8df72c8() * -0.440969);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfdea8()
{
   return (neuron0x8df74c8() * 0.333048);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfded0()
{
   return (neuron0x8df76c8() * 0.359717);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfdef8()
{
   return (neuron0x8df78e0() * 0.330562);
}

double NNPerpElectronCorrectionTheta::synapse0x8df7bf8()
{
   return (neuron0x8df7c20() * 0.101735);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe048()
{
   return (neuron0x8df7ef0() * -0.517752);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe070()
{
   return (neuron0x8df8208() * 0.364855);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe098()
{
   return (neuron0x8df85a8() * -0.0963536);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe0c0()
{
   return (neuron0x8df8878() * -0.22588);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe0e8()
{
   return (neuron0x8df8c48() * -0.0832299);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe110()
{
   return (neuron0x8df8f60() * 0.0905754);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe138()
{
   return (neuron0x8df9290() * -0.0176107);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe160()
{
   return (neuron0x8df95c0() * -0.00163657);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe188()
{
   return (neuron0x8df98f0() * 0.167451);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe1b0()
{
   return (neuron0x8df9de8() * -0.313542);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe1d8()
{
   return (neuron0x8dfa100() * 0.10622);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe200()
{
   return (neuron0x8dfa430() * -0.0712851);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe228()
{
   return (neuron0x8dfa760() * -0.0301474);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe250()
{
   return (neuron0x8dfaa90() * 0.228329);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe278()
{
   return (neuron0x8dfadc0() * 0.141075);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe328()
{
   return (neuron0x8dfb4a8() * -0.278633);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe350()
{
   return (neuron0x8dfb700() * -0.100318);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe378()
{
   return (neuron0x8dfb958() * -0.0466039);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe3a0()
{
   return (neuron0x8df9be0() * 0.00709838);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe3c8()
{
   return (neuron0x8dfbfa8() * -0.403942);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe3f0()
{
   return (neuron0x8dfc2c0() * 0.118476);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe418()
{
   return (neuron0x8dfc5f0() * 0.146803);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe440()
{
   return (neuron0x8dfc920() * 0.150704);
}

double NNPerpElectronCorrectionTheta::synapse0x8dfe468()
{
   return (neuron0x8dfdc08() * -0.0704644);
}

