#include "NNPerpElectronAngleCorrectionBCPDirPhi.h"
#include <cmath>

double NNPerpElectronAngleCorrectionBCPDirPhi::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10,double in11,double in12,double in13,double in14) {
   input0 = (in0 - 0.70338)/0.0918753;
   input1 = (in1 - -0.0158536)/0.256935;
   input2 = (in2 - -3.74358)/31.4802;
   input3 = (in3 - -3.51704)/56.0822;
   input4 = (in4 - 3068.51)/1543.52;
   input5 = (in5 - -0.00355158)/0.749607;
   input6 = (in6 - -0.0154733)/0.75028;
   input7 = (in7 - -4.12133)/32.3487;
   input8 = (in8 - -5.23583)/57.6425;
   input9 = (in9 - 1.38816)/0.32817;
   input10 = (in10 - 1.48199)/0.36367;
   input11 = (in11 - -0.0323618)/14.7341;
   input12 = (in12 - 0.193437)/1.21351;
   input13 = (in13 - 1.25754)/412.206;
   input14 = (in14 - 4.00453)/5.88751;
   switch(index) {
     case 0:
         return neuron0x9345cd8();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionBCPDirPhi::Value(int index, double* input) {
   input0 = (input[0] - 0.70338)/0.0918753;
   input1 = (input[1] - -0.0158536)/0.256935;
   input2 = (input[2] - -3.74358)/31.4802;
   input3 = (input[3] - -3.51704)/56.0822;
   input4 = (input[4] - 3068.51)/1543.52;
   input5 = (input[5] - -0.00355158)/0.749607;
   input6 = (input[6] - -0.0154733)/0.75028;
   input7 = (input[7] - -4.12133)/32.3487;
   input8 = (input[8] - -5.23583)/57.6425;
   input9 = (input[9] - 1.38816)/0.32817;
   input10 = (input[10] - 1.48199)/0.36367;
   input11 = (input[11] - -0.0323618)/14.7341;
   input12 = (input[12] - 0.193437)/1.21351;
   input13 = (input[13] - 1.25754)/412.206;
   input14 = (input[14] - 4.00453)/5.88751;
   switch(index) {
     case 0:
         return neuron0x9345cd8();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9323918() {
   return input0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9323ac0() {
   return input1;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9332238() {
   return input2;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93323f0() {
   return input3;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93325f0() {
   return input4;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93327f0() {
   return input5;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93329f0() {
   return input6;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9332bf0() {
   return input7;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9332df0() {
   return input8;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9332ff0() {
   return input9;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93331f0() {
   return input10;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93333f0() {
   return input11;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93335f0() {
   return input12;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93337f0() {
   return input13;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9333a08() {
   return input14;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9333d48() {
   double input = 0.092134;
   input += synapse0x92d2620();
   input += synapse0x90b0870();
   input += synapse0x90b0898();
   input += synapse0x90b0908();
   input += synapse0x9333f00();
   input += synapse0x9333f28();
   input += synapse0x9333f50();
   input += synapse0x9333f78();
   input += synapse0x9333fa0();
   input += synapse0x9333fc8();
   input += synapse0x9333ff0();
   input += synapse0x9334018();
   input += synapse0x9334040();
   input += synapse0x9334068();
   input += synapse0x9334090();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9333d48() {
   double input = input0x9333d48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x93340b8() {
   double input = 0.408988;
   input += synapse0x90af698();
   input += synapse0x90af6c0();
   input += synapse0x93343c8();
   input += synapse0x93343f0();
   input += synapse0x9334418();
   input += synapse0x9334440();
   input += synapse0x9334468();
   input += synapse0x9334490();
   input += synapse0x93344b8();
   input += synapse0x93344e0();
   input += synapse0x9334508();
   input += synapse0x9334530();
   input += synapse0x9334558();
   input += synapse0x9334580();
   input += synapse0x93345a8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93340b8() {
   double input = input0x93340b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x93345d0() {
   double input = -0.101192;
   input += synapse0x9334788();
   input += synapse0x93347b0();
   input += synapse0x93347d8();
   input += synapse0x9334340();
   input += synapse0x9334368();
   input += synapse0x9334390();
   input += synapse0x9334908();
   input += synapse0x9334930();
   input += synapse0x9334958();
   input += synapse0x9334980();
   input += synapse0x93349a8();
   input += synapse0x93349d0();
   input += synapse0x93349f8();
   input += synapse0x9334a20();
   input += synapse0x9334a48();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93345d0() {
   double input = input0x93345d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9334a70() {
   double input = -0.305948;
   input += synapse0x9334c70();
   input += synapse0x9334c98();
   input += synapse0x9334cc0();
   input += synapse0x9334ce8();
   input += synapse0x9334d10();
   input += synapse0x9334d38();
   input += synapse0x9334d60();
   input += synapse0x9334d88();
   input += synapse0x9334db0();
   input += synapse0x9334dd8();
   input += synapse0x9334e00();
   input += synapse0x9334e28();
   input += synapse0x9334e50();
   input += synapse0x9334e78();
   input += synapse0x9334ea0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9334a70() {
   double input = input0x9334a70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9334ec8() {
   double input = -0.421734;
   input += synapse0x93350c8();
   input += synapse0x93350f0();
   input += synapse0x9335118();
   input += synapse0x9335140();
   input += synapse0x9335168();
   input += synapse0x90b07c8();
   input += synapse0x9323878();
   input += synapse0x93238a0();
   input += synapse0x90b0960();
   input += synapse0x90b0988();
   input += synapse0x90b09b0();
   input += synapse0x90b09d8();
   input += synapse0x9334800();
   input += synapse0x9334828();
   input += synapse0x9334850();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9334ec8() {
   double input = input0x9334ec8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9335398() {
   double input = 0.384134;
   input += synapse0x9335508();
   input += synapse0x9335530();
   input += synapse0x9335558();
   input += synapse0x9335580();
   input += synapse0x93355a8();
   input += synapse0x93355d0();
   input += synapse0x93355f8();
   input += synapse0x9335620();
   input += synapse0x9335648();
   input += synapse0x9335670();
   input += synapse0x9335698();
   input += synapse0x93356c0();
   input += synapse0x93356e8();
   input += synapse0x9335710();
   input += synapse0x9335738();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9335398() {
   double input = input0x9335398();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9335760() {
   double input = 0.299883;
   input += synapse0x9335960();
   input += synapse0x9335988();
   input += synapse0x93359b0();
   input += synapse0x93359d8();
   input += synapse0x9335a00();
   input += synapse0x9335a28();
   input += synapse0x9335a50();
   input += synapse0x9335a78();
   input += synapse0x9335aa0();
   input += synapse0x9335ac8();
   input += synapse0x9335af0();
   input += synapse0x9335b18();
   input += synapse0x9335b40();
   input += synapse0x9335b68();
   input += synapse0x9335b90();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9335760() {
   double input = input0x9335760();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9335bb8() {
   double input = 0.446802;
   input += synapse0x9335db8();
   input += synapse0x9335de0();
   input += synapse0x9335e08();
   input += synapse0x9335e30();
   input += synapse0x9335e58();
   input += synapse0x9335e80();
   input += synapse0x9335ea8();
   input += synapse0x9335ed0();
   input += synapse0x9335ef8();
   input += synapse0x9335f20();
   input += synapse0x9335f48();
   input += synapse0x9335f70();
   input += synapse0x9335f98();
   input += synapse0x9335fc0();
   input += synapse0x9335fe8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9335bb8() {
   double input = input0x9335bb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9336010() {
   double input = 0.185035;
   input += synapse0x9336210();
   input += synapse0x9336238();
   input += synapse0x9336260();
   input += synapse0x9336288();
   input += synapse0x93362b0();
   input += synapse0x93362d8();
   input += synapse0x9336300();
   input += synapse0x9336328();
   input += synapse0x9336350();
   input += synapse0x9335190();
   input += synapse0x93351b8();
   input += synapse0x93351e0();
   input += synapse0x9335208();
   input += synapse0x9335230();
   input += synapse0x9335258();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9336010() {
   double input = input0x9336010();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9336780() {
   double input = -0.387263;
   input += synapse0x9335358();
   input += synapse0x93368a8();
   input += synapse0x93368d0();
   input += synapse0x93368f8();
   input += synapse0x9336920();
   input += synapse0x9336948();
   input += synapse0x9336970();
   input += synapse0x9336998();
   input += synapse0x93369c0();
   input += synapse0x93369e8();
   input += synapse0x9336a10();
   input += synapse0x9336a38();
   input += synapse0x9336a60();
   input += synapse0x9336a88();
   input += synapse0x9336ab0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9336780() {
   double input = input0x9336780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9336ad8() {
   double input = -0.368556;
   input += synapse0x9336cd8();
   input += synapse0x9336d00();
   input += synapse0x9336d28();
   input += synapse0x9336d50();
   input += synapse0x9336d78();
   input += synapse0x9336da0();
   input += synapse0x9336dc8();
   input += synapse0x9336df0();
   input += synapse0x9336e18();
   input += synapse0x9336e40();
   input += synapse0x9336e68();
   input += synapse0x9336e90();
   input += synapse0x9336eb8();
   input += synapse0x9336ee0();
   input += synapse0x9336f08();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9336ad8() {
   double input = input0x9336ad8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9336f30() {
   double input = 0.445894;
   input += synapse0x9337130();
   input += synapse0x9337158();
   input += synapse0x9337180();
   input += synapse0x93371a8();
   input += synapse0x93371d0();
   input += synapse0x93371f8();
   input += synapse0x9337220();
   input += synapse0x9337248();
   input += synapse0x9337270();
   input += synapse0x9337298();
   input += synapse0x93372c0();
   input += synapse0x93372e8();
   input += synapse0x9337310();
   input += synapse0x9337338();
   input += synapse0x9337360();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9336f30() {
   double input = input0x9336f30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9337388() {
   double input = 0.444464;
   input += synapse0x9337588();
   input += synapse0x93375b0();
   input += synapse0x93375d8();
   input += synapse0x9337600();
   input += synapse0x9337628();
   input += synapse0x9337650();
   input += synapse0x9337678();
   input += synapse0x93376a0();
   input += synapse0x93376c8();
   input += synapse0x93376f0();
   input += synapse0x9337718();
   input += synapse0x9337740();
   input += synapse0x9337768();
   input += synapse0x9337790();
   input += synapse0x93377b8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9337388() {
   double input = input0x9337388();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x93377e0() {
   double input = 0.448074;
   input += synapse0x93379e0();
   input += synapse0x9337a08();
   input += synapse0x9337a30();
   input += synapse0x9337a58();
   input += synapse0x9337a80();
   input += synapse0x9337aa8();
   input += synapse0x9337ad0();
   input += synapse0x9337af8();
   input += synapse0x9337b20();
   input += synapse0x9337b48();
   input += synapse0x9337b70();
   input += synapse0x9337b98();
   input += synapse0x9337bc0();
   input += synapse0x9337be8();
   input += synapse0x9337c10();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93377e0() {
   double input = input0x93377e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9337c38() {
   double input = -0.329547;
   input += synapse0x9337e38();
   input += synapse0x9337e60();
   input += synapse0x9337e88();
   input += synapse0x9337eb0();
   input += synapse0x9337ed8();
   input += synapse0x9337f00();
   input += synapse0x9337f28();
   input += synapse0x9337f50();
   input += synapse0x9337f78();
   input += synapse0x9337fa0();
   input += synapse0x9337fc8();
   input += synapse0x9337ff0();
   input += synapse0x9338018();
   input += synapse0x9338040();
   input += synapse0x9338068();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9337c38() {
   double input = input0x9337c38();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9338090() {
   double input = -0.297033;
   input += synapse0x9338290();
   input += synapse0x92d26b8();
   input += synapse0x92d26e0();
   input += synapse0x9338450();
   input += synapse0x9338500();
   input += synapse0x93385b0();
   input += synapse0x9338660();
   input += synapse0x9338710();
   input += synapse0x93387c0();
   input += synapse0x9338870();
   input += synapse0x9338920();
   input += synapse0x93389d0();
   input += synapse0x9338a80();
   input += synapse0x9338b30();
   input += synapse0x9338be0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9338090() {
   double input = input0x9338090();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9338c90() {
   double input = -0.48192;
   input += synapse0x9338db8();
   input += synapse0x9338de0();
   input += synapse0x9338e08();
   input += synapse0x9338e30();
   input += synapse0x9338e58();
   input += synapse0x9338e80();
   input += synapse0x9338ea8();
   input += synapse0x9338ed0();
   input += synapse0x9338ef8();
   input += synapse0x9338f20();
   input += synapse0x9338f48();
   input += synapse0x9338f70();
   input += synapse0x9338f98();
   input += synapse0x9338fc0();
   input += synapse0x9338fe8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9338c90() {
   double input = input0x9338c90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9339010() {
   double input = -0.387322;
   input += synapse0x93342b8();
   input += synapse0x93342e0();
   input += synapse0x9334308();
   input += synapse0x9332f60();
   input += synapse0x9332d60();
   input += synapse0x9332b60();
   input += synapse0x9332960();
   input += synapse0x9332760();
   input += synapse0x9332560();
   input += synapse0x9332360();
   input += synapse0x9323c58();
   input += synapse0x9336378();
   input += synapse0x93363a0();
   input += synapse0x93363c8();
   input += synapse0x93363f0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9339010() {
   double input = input0x9339010();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9336418() {
   double input = 0.450953;
   input += synapse0x9336630();
   input += synapse0x9336658();
   input += synapse0x9336680();
   input += synapse0x93366a8();
   input += synapse0x93366d0();
   input += synapse0x93366f8();
   input += synapse0x9336720();
   input += synapse0x9336748();
   input += synapse0x9339a48();
   input += synapse0x9339a70();
   input += synapse0x9339a98();
   input += synapse0x9339ac0();
   input += synapse0x9339ae8();
   input += synapse0x9339b10();
   input += synapse0x9339b38();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9336418() {
   double input = input0x9336418();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9339b60() {
   double input = 0.2488;
   input += synapse0x9339d60();
   input += synapse0x9339d88();
   input += synapse0x9339db0();
   input += synapse0x9339dd8();
   input += synapse0x9339e00();
   input += synapse0x9339e28();
   input += synapse0x9339e50();
   input += synapse0x9339e78();
   input += synapse0x9339ea0();
   input += synapse0x9339ec8();
   input += synapse0x9339ef0();
   input += synapse0x9339f18();
   input += synapse0x9339f40();
   input += synapse0x9339f68();
   input += synapse0x9339f90();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9339b60() {
   double input = input0x9339b60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9339fb8() {
   double input = 0.0946124;
   input += synapse0x933a1b8();
   input += synapse0x933a1e0();
   input += synapse0x933a208();
   input += synapse0x933a230();
   input += synapse0x933a258();
   input += synapse0x933a280();
   input += synapse0x933a2a8();
   input += synapse0x933a2d0();
   input += synapse0x933a2f8();
   input += synapse0x933a320();
   input += synapse0x933a348();
   input += synapse0x933a370();
   input += synapse0x933a398();
   input += synapse0x933a3c0();
   input += synapse0x933a3e8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9339fb8() {
   double input = input0x9339fb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933a410() {
   double input = 0.303862;
   input += synapse0x933a610();
   input += synapse0x933a638();
   input += synapse0x933a660();
   input += synapse0x933a688();
   input += synapse0x933a6b0();
   input += synapse0x933a6d8();
   input += synapse0x933a700();
   input += synapse0x933a728();
   input += synapse0x933a750();
   input += synapse0x933a778();
   input += synapse0x933a7a0();
   input += synapse0x933a7c8();
   input += synapse0x933a7f0();
   input += synapse0x933a818();
   input += synapse0x933a840();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933a410() {
   double input = input0x933a410();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933a868() {
   double input = -0.148041;
   input += synapse0x933aa68();
   input += synapse0x933aa90();
   input += synapse0x933aab8();
   input += synapse0x933aae0();
   input += synapse0x933ab08();
   input += synapse0x933ab30();
   input += synapse0x933ab58();
   input += synapse0x933ab80();
   input += synapse0x933aba8();
   input += synapse0x933abd0();
   input += synapse0x933abf8();
   input += synapse0x933ac20();
   input += synapse0x933ac48();
   input += synapse0x933ac70();
   input += synapse0x933ac98();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933a868() {
   double input = input0x933a868();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933acc0() {
   double input = -0.376927;
   input += synapse0x933aec0();
   input += synapse0x933aee8();
   input += synapse0x933af10();
   input += synapse0x933af38();
   input += synapse0x933af60();
   input += synapse0x933af88();
   input += synapse0x933afb0();
   input += synapse0x933afd8();
   input += synapse0x933b000();
   input += synapse0x933b028();
   input += synapse0x933b050();
   input += synapse0x933b078();
   input += synapse0x933b0a0();
   input += synapse0x933b0c8();
   input += synapse0x933b0f0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933acc0() {
   double input = input0x933acc0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933b118() {
   double input = -0.252419;
   input += synapse0x933b318();
   input += synapse0x933b340();
   input += synapse0x933b368();
   input += synapse0x933b390();
   input += synapse0x933b3b8();
   input += synapse0x933b3e0();
   input += synapse0x933b408();
   input += synapse0x933b430();
   input += synapse0x933b458();
   input += synapse0x933b480();
   input += synapse0x933b4a8();
   input += synapse0x933b4d0();
   input += synapse0x933b4f8();
   input += synapse0x933b520();
   input += synapse0x933b548();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933b118() {
   double input = input0x933b118();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933b570() {
   double input = 0.203026;
   input += synapse0x933b770();
   input += synapse0x933b798();
   input += synapse0x933b7c0();
   input += synapse0x933b7e8();
   input += synapse0x933b810();
   input += synapse0x933b838();
   input += synapse0x933b860();
   input += synapse0x933b888();
   input += synapse0x933b8b0();
   input += synapse0x933b8d8();
   input += synapse0x933b900();
   input += synapse0x933b928();
   input += synapse0x933b950();
   input += synapse0x933b978();
   input += synapse0x933b9a0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933b570() {
   double input = input0x933b570();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933b9c8() {
   double input = -0.13279;
   input += synapse0x933bbc8();
   input += synapse0x933bbf0();
   input += synapse0x933bc18();
   input += synapse0x933bc40();
   input += synapse0x933bc68();
   input += synapse0x933bc90();
   input += synapse0x933bcb8();
   input += synapse0x933bce0();
   input += synapse0x933bd08();
   input += synapse0x933bd30();
   input += synapse0x933bd58();
   input += synapse0x933bd80();
   input += synapse0x933bda8();
   input += synapse0x933bdd0();
   input += synapse0x933bdf8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933b9c8() {
   double input = input0x933b9c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933be20() {
   double input = 0.377016;
   input += synapse0x933c020();
   input += synapse0x933c048();
   input += synapse0x933c070();
   input += synapse0x933c098();
   input += synapse0x933c0c0();
   input += synapse0x933c0e8();
   input += synapse0x933c110();
   input += synapse0x933c138();
   input += synapse0x933c160();
   input += synapse0x933c188();
   input += synapse0x933c1b0();
   input += synapse0x933c1d8();
   input += synapse0x933c200();
   input += synapse0x933c228();
   input += synapse0x933c250();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933be20() {
   double input = input0x933be20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933c278() {
   double input = 0.0847006;
   input += synapse0x933c478();
   input += synapse0x933c4a0();
   input += synapse0x933c4c8();
   input += synapse0x933c4f0();
   input += synapse0x933c518();
   input += synapse0x933c540();
   input += synapse0x933c568();
   input += synapse0x933c590();
   input += synapse0x933c5b8();
   input += synapse0x933c5e0();
   input += synapse0x933c608();
   input += synapse0x933c630();
   input += synapse0x933c658();
   input += synapse0x933c680();
   input += synapse0x933c6a8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933c278() {
   double input = input0x933c278();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933c6d0() {
   double input = 0.0728217;
   input += synapse0x933c8d0();
   input += synapse0x933c8f8();
   input += synapse0x933c920();
   input += synapse0x933c948();
   input += synapse0x933c970();
   input += synapse0x933c998();
   input += synapse0x933c9c0();
   input += synapse0x933c9e8();
   input += synapse0x933ca10();
   input += synapse0x933ca38();
   input += synapse0x933ca60();
   input += synapse0x933ca88();
   input += synapse0x933cab0();
   input += synapse0x933cad8();
   input += synapse0x933cb00();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933c6d0() {
   double input = input0x933c6d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933cb28() {
   double input = 0.294842;
   input += synapse0x933cd40();
   input += synapse0x933cd68();
   input += synapse0x933cd90();
   input += synapse0x933cdb8();
   input += synapse0x933cde0();
   input += synapse0x933ce08();
   input += synapse0x933ce30();
   input += synapse0x933ce58();
   input += synapse0x933ce80();
   input += synapse0x933cea8();
   input += synapse0x933ced0();
   input += synapse0x933cef8();
   input += synapse0x933cf20();
   input += synapse0x933cf48();
   input += synapse0x933cf70();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933cb28() {
   double input = input0x933cb28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933cf98() {
   double input = -0.0925085;
   input += synapse0x933d1b0();
   input += synapse0x93382b8();
   input += synapse0x93382e0();
   input += synapse0x9338308();
   input += synapse0x9338478();
   input += synapse0x93384a0();
   input += synapse0x93384c8();
   input += synapse0x9338528();
   input += synapse0x9338550();
   input += synapse0x9338578();
   input += synapse0x93385d8();
   input += synapse0x9338600();
   input += synapse0x9338628();
   input += synapse0x9338688();
   input += synapse0x93386b0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933cf98() {
   double input = input0x933cf98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933e048() {
   double input = 0.459949;
   input += synapse0x93388e0();
   input += synapse0x93386d8();
   input += synapse0x9338780();
   input += synapse0x9338830();
   input += synapse0x9338948();
   input += synapse0x9338970();
   input += synapse0x9338998();
   input += synapse0x93389f8();
   input += synapse0x9338a20();
   input += synapse0x9338a48();
   input += synapse0x9338aa8();
   input += synapse0x9338ad0();
   input += synapse0x9338af8();
   input += synapse0x9338b58();
   input += synapse0x9338b80();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933e048() {
   double input = input0x933e048();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933e170() {
   double input = 0.178793;
   input += synapse0x9338ba8();
   input += synapse0x9338c50();
   input += synapse0x933e328();
   input += synapse0x933e350();
   input += synapse0x933e378();
   input += synapse0x933e3a0();
   input += synapse0x933e3c8();
   input += synapse0x933e3f0();
   input += synapse0x933e418();
   input += synapse0x933e440();
   input += synapse0x933e468();
   input += synapse0x933e490();
   input += synapse0x933e4b8();
   input += synapse0x933e4e0();
   input += synapse0x933e508();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933e170() {
   double input = input0x933e170();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933e530() {
   double input = 0.222437;
   input += synapse0x933e730();
   input += synapse0x933e758();
   input += synapse0x933e780();
   input += synapse0x9339240();
   input += synapse0x9339268();
   input += synapse0x9339290();
   input += synapse0x93392b8();
   input += synapse0x93392e0();
   input += synapse0x9339308();
   input += synapse0x9339330();
   input += synapse0x9339358();
   input += synapse0x9339380();
   input += synapse0x93393a8();
   input += synapse0x93393d0();
   input += synapse0x93393f8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933e530() {
   double input = input0x933e530();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9339420() {
   double input = 0.0465918;
   input += synapse0x9339620();
   input += synapse0x9339648();
   input += synapse0x9339670();
   input += synapse0x9339698();
   input += synapse0x93396c0();
   input += synapse0x93396e8();
   input += synapse0x9339710();
   input += synapse0x9339738();
   input += synapse0x9339760();
   input += synapse0x9339788();
   input += synapse0x93397b0();
   input += synapse0x93397d8();
   input += synapse0x9339800();
   input += synapse0x9339828();
   input += synapse0x9339850();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9339420() {
   double input = input0x9339420();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9339878() {
   double input = -0.461575;
   input += synapse0x933f7f8();
   input += synapse0x933f820();
   input += synapse0x933f848();
   input += synapse0x933f870();
   input += synapse0x933f898();
   input += synapse0x933f8c0();
   input += synapse0x933f8e8();
   input += synapse0x933f910();
   input += synapse0x933f938();
   input += synapse0x933f960();
   input += synapse0x933f988();
   input += synapse0x933f9b0();
   input += synapse0x933f9d8();
   input += synapse0x933fa00();
   input += synapse0x933fa28();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9339878() {
   double input = input0x9339878();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933fa50() {
   double input = -0.479842;
   input += synapse0x933fc50();
   input += synapse0x933fc78();
   input += synapse0x933fca0();
   input += synapse0x933fcc8();
   input += synapse0x933fcf0();
   input += synapse0x933fd18();
   input += synapse0x933fd40();
   input += synapse0x933fd68();
   input += synapse0x933fd90();
   input += synapse0x933fdb8();
   input += synapse0x933fde0();
   input += synapse0x933fe08();
   input += synapse0x933fe30();
   input += synapse0x933fe58();
   input += synapse0x933fe80();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933fa50() {
   double input = input0x933fa50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x933fea8() {
   double input = -0.314506;
   input += synapse0x93400c0();
   input += synapse0x93400e8();
   input += synapse0x9340110();
   input += synapse0x9340138();
   input += synapse0x9340160();
   input += synapse0x9340188();
   input += synapse0x93401b0();
   input += synapse0x93401d8();
   input += synapse0x9340200();
   input += synapse0x9340228();
   input += synapse0x9340250();
   input += synapse0x9340278();
   input += synapse0x93402a0();
   input += synapse0x93402c8();
   input += synapse0x93402f0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x933fea8() {
   double input = input0x933fea8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9340318() {
   double input = 0.111037;
   input += synapse0x9340530();
   input += synapse0x9340558();
   input += synapse0x9340580();
   input += synapse0x93405a8();
   input += synapse0x93405d0();
   input += synapse0x93405f8();
   input += synapse0x9340620();
   input += synapse0x9340648();
   input += synapse0x9340670();
   input += synapse0x9340698();
   input += synapse0x93406c0();
   input += synapse0x93406e8();
   input += synapse0x9340710();
   input += synapse0x9340738();
   input += synapse0x9340760();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9340318() {
   double input = input0x9340318();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9340788() {
   double input = 0.411809;
   input += synapse0x93409a0();
   input += synapse0x93409c8();
   input += synapse0x93409f0();
   input += synapse0x9340a18();
   input += synapse0x9340a40();
   input += synapse0x9340a68();
   input += synapse0x9340a90();
   input += synapse0x9340ab8();
   input += synapse0x9340ae0();
   input += synapse0x9340b08();
   input += synapse0x9340b30();
   input += synapse0x9340b58();
   input += synapse0x9340b80();
   input += synapse0x9340ba8();
   input += synapse0x9340bd0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9340788() {
   double input = input0x9340788();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9340bf8() {
   double input = -0.0251373;
   input += synapse0x9340e10();
   input += synapse0x9340e38();
   input += synapse0x9340e60();
   input += synapse0x9340e88();
   input += synapse0x9340eb0();
   input += synapse0x9340ed8();
   input += synapse0x9340f00();
   input += synapse0x9340f28();
   input += synapse0x9340f50();
   input += synapse0x9340f78();
   input += synapse0x9340fa0();
   input += synapse0x9340fc8();
   input += synapse0x9340ff0();
   input += synapse0x9341018();
   input += synapse0x9341040();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9340bf8() {
   double input = input0x9340bf8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9341068() {
   double input = -0.347481;
   input += synapse0x9341280();
   input += synapse0x93412a8();
   input += synapse0x93412d0();
   input += synapse0x93412f8();
   input += synapse0x9341320();
   input += synapse0x9341348();
   input += synapse0x9341370();
   input += synapse0x9341398();
   input += synapse0x93413c0();
   input += synapse0x93413e8();
   input += synapse0x9341410();
   input += synapse0x9341438();
   input += synapse0x9341460();
   input += synapse0x9341488();
   input += synapse0x93414b0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9341068() {
   double input = input0x9341068();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x93414d8() {
   double input = 0.123782;
   input += synapse0x93416f0();
   input += synapse0x9341718();
   input += synapse0x9341740();
   input += synapse0x9341768();
   input += synapse0x9341790();
   input += synapse0x93417b8();
   input += synapse0x93417e0();
   input += synapse0x9341808();
   input += synapse0x9341830();
   input += synapse0x9341858();
   input += synapse0x9341880();
   input += synapse0x93418a8();
   input += synapse0x93418d0();
   input += synapse0x93418f8();
   input += synapse0x9341920();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93414d8() {
   double input = input0x93414d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9341948() {
   double input = 0.169312;
   input += synapse0x9341b60();
   input += synapse0x9341b88();
   input += synapse0x9341bb0();
   input += synapse0x9341bd8();
   input += synapse0x9341c00();
   input += synapse0x9341c28();
   input += synapse0x9341c50();
   input += synapse0x9341c78();
   input += synapse0x9341ca0();
   input += synapse0x9341cc8();
   input += synapse0x9341cf0();
   input += synapse0x9341d18();
   input += synapse0x9341d40();
   input += synapse0x9341d68();
   input += synapse0x9341d90();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9341948() {
   double input = input0x9341948();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9341db8() {
   double input = 0.203036;
   input += synapse0x9341fd0();
   input += synapse0x9341ff8();
   input += synapse0x9342020();
   input += synapse0x9342048();
   input += synapse0x9342070();
   input += synapse0x9342098();
   input += synapse0x93420c0();
   input += synapse0x93420e8();
   input += synapse0x9342110();
   input += synapse0x9342138();
   input += synapse0x9342160();
   input += synapse0x9342188();
   input += synapse0x93421b0();
   input += synapse0x93421d8();
   input += synapse0x9342200();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9341db8() {
   double input = input0x9341db8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9342228() {
   double input = -0.21423;
   input += synapse0x9342440();
   input += synapse0x9342468();
   input += synapse0x9342490();
   input += synapse0x93424b8();
   input += synapse0x93424e0();
   input += synapse0x9342508();
   input += synapse0x9342530();
   input += synapse0x9342558();
   input += synapse0x9342580();
   input += synapse0x93425a8();
   input += synapse0x93425d0();
   input += synapse0x93425f8();
   input += synapse0x9342620();
   input += synapse0x9342648();
   input += synapse0x9342670();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9342228() {
   double input = input0x9342228();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9342698() {
   double input = -0.428024;
   input += synapse0x93428b0();
   input += synapse0x93428d8();
   input += synapse0x9342900();
   input += synapse0x9342928();
   input += synapse0x9342950();
   input += synapse0x9342978();
   input += synapse0x93429a0();
   input += synapse0x93429c8();
   input += synapse0x93429f0();
   input += synapse0x9342a18();
   input += synapse0x9342a40();
   input += synapse0x9342a68();
   input += synapse0x9342a90();
   input += synapse0x9342ab8();
   input += synapse0x9342ae0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9342698() {
   double input = input0x9342698();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9342b08() {
   double input = 0.190356;
   input += synapse0x9342d20();
   input += synapse0x9342d48();
   input += synapse0x9342d70();
   input += synapse0x9342d98();
   input += synapse0x9342dc0();
   input += synapse0x9342de8();
   input += synapse0x9342e10();
   input += synapse0x9342e38();
   input += synapse0x9342e60();
   input += synapse0x9342e88();
   input += synapse0x9342eb0();
   input += synapse0x9342ed8();
   input += synapse0x9342f00();
   input += synapse0x9342f28();
   input += synapse0x9342f50();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9342b08() {
   double input = input0x9342b08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9342f78() {
   double input = 0.216208;
   input += synapse0x9339138();
   input += synapse0x9339160();
   input += synapse0x9339188();
   input += synapse0x93391b0();
   input += synapse0x93391d8();
   input += synapse0x9339200();
   input += synapse0x9343398();
   input += synapse0x93433c0();
   input += synapse0x93433e8();
   input += synapse0x9343410();
   input += synapse0x9343438();
   input += synapse0x9343460();
   input += synapse0x9343488();
   input += synapse0x93434b0();
   input += synapse0x93434d8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9342f78() {
   double input = input0x9342f78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9343500() {
   double input = -0.112076;
   input += synapse0x9343700();
   input += synapse0x9343728();
   input += synapse0x9343750();
   input += synapse0x9343778();
   input += synapse0x93437a0();
   input += synapse0x93437c8();
   input += synapse0x93437f0();
   input += synapse0x9343818();
   input += synapse0x9343840();
   input += synapse0x9343868();
   input += synapse0x9343890();
   input += synapse0x93438b8();
   input += synapse0x93438e0();
   input += synapse0x9343908();
   input += synapse0x9343930();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9343500() {
   double input = input0x9343500();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9343958() {
   double input = -0.225488;
   input += synapse0x9343b70();
   input += synapse0x9343b98();
   input += synapse0x9343bc0();
   input += synapse0x9343be8();
   input += synapse0x9343c10();
   input += synapse0x9343c38();
   input += synapse0x9343c60();
   input += synapse0x9343c88();
   input += synapse0x9343cb0();
   input += synapse0x9343cd8();
   input += synapse0x9343d00();
   input += synapse0x9343d28();
   input += synapse0x9343d50();
   input += synapse0x9343d78();
   input += synapse0x9343da0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9343958() {
   double input = input0x9343958();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9343dc8() {
   double input = -0.178201;
   input += synapse0x9343fe0();
   input += synapse0x9344008();
   input += synapse0x9344030();
   input += synapse0x9344058();
   input += synapse0x9344080();
   input += synapse0x93440a8();
   input += synapse0x93440d0();
   input += synapse0x93440f8();
   input += synapse0x9344120();
   input += synapse0x9344148();
   input += synapse0x9344170();
   input += synapse0x9344198();
   input += synapse0x93441c0();
   input += synapse0x93441e8();
   input += synapse0x9344210();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9343dc8() {
   double input = input0x9343dc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9344238() {
   double input = -0.150716;
   input += synapse0x9344450();
   input += synapse0x9344478();
   input += synapse0x93444a0();
   input += synapse0x93444c8();
   input += synapse0x93444f0();
   input += synapse0x9344518();
   input += synapse0x9344540();
   input += synapse0x9344568();
   input += synapse0x9344590();
   input += synapse0x93445b8();
   input += synapse0x93445e0();
   input += synapse0x9344608();
   input += synapse0x9344630();
   input += synapse0x9344658();
   input += synapse0x9344680();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9344238() {
   double input = input0x9344238();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x93446a8() {
   double input = -0.493575;
   input += synapse0x93448c0();
   input += synapse0x93448e8();
   input += synapse0x9344910();
   input += synapse0x9344938();
   input += synapse0x9344960();
   input += synapse0x9344988();
   input += synapse0x93449b0();
   input += synapse0x93449d8();
   input += synapse0x9344a00();
   input += synapse0x9344a28();
   input += synapse0x9344a50();
   input += synapse0x9344a78();
   input += synapse0x9344aa0();
   input += synapse0x9344ac8();
   input += synapse0x9344af0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93446a8() {
   double input = input0x93446a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9344b18() {
   double input = -0.00121346;
   input += synapse0x9344d30();
   input += synapse0x9344d58();
   input += synapse0x9344d80();
   input += synapse0x9344da8();
   input += synapse0x9344dd0();
   input += synapse0x9344df8();
   input += synapse0x9344e20();
   input += synapse0x9344e48();
   input += synapse0x9344e70();
   input += synapse0x9344e98();
   input += synapse0x9344ec0();
   input += synapse0x9344ee8();
   input += synapse0x9344f10();
   input += synapse0x9344f38();
   input += synapse0x9344f60();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9344b18() {
   double input = input0x9344b18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9344f88() {
   double input = -0.36169;
   input += synapse0x93451a0();
   input += synapse0x93451c8();
   input += synapse0x93451f0();
   input += synapse0x9345218();
   input += synapse0x9345240();
   input += synapse0x9345268();
   input += synapse0x9345290();
   input += synapse0x93452b8();
   input += synapse0x93452e0();
   input += synapse0x9345308();
   input += synapse0x9345330();
   input += synapse0x9345358();
   input += synapse0x9345380();
   input += synapse0x93453a8();
   input += synapse0x93453d0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9344f88() {
   double input = input0x9344f88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x93453f8() {
   double input = -0.167752;
   input += synapse0x9345610();
   input += synapse0x9345638();
   input += synapse0x9345660();
   input += synapse0x9345688();
   input += synapse0x93456b0();
   input += synapse0x93456d8();
   input += synapse0x9345700();
   input += synapse0x9345728();
   input += synapse0x9345750();
   input += synapse0x9345778();
   input += synapse0x93457a0();
   input += synapse0x93457c8();
   input += synapse0x93457f0();
   input += synapse0x9345818();
   input += synapse0x9345840();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x93453f8() {
   double input = input0x93453f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9345868() {
   double input = -0.108337;
   input += synapse0x9345a80();
   input += synapse0x9345aa8();
   input += synapse0x9345ad0();
   input += synapse0x9345af8();
   input += synapse0x9345b20();
   input += synapse0x9345b48();
   input += synapse0x9345b70();
   input += synapse0x9345b98();
   input += synapse0x9345bc0();
   input += synapse0x9345be8();
   input += synapse0x9345c10();
   input += synapse0x9345c38();
   input += synapse0x9345c60();
   input += synapse0x9345c88();
   input += synapse0x9345cb0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9345868() {
   double input = input0x9345868();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::input0x9345cd8() {
   double input = 0.0292264;
   input += synapse0x9345e00();
   input += synapse0x9345e28();
   input += synapse0x9345e50();
   input += synapse0x9345e78();
   input += synapse0x9345ea0();
   input += synapse0x9345ec8();
   input += synapse0x9345ef0();
   input += synapse0x9345f18();
   input += synapse0x9345f40();
   input += synapse0x9345f68();
   input += synapse0x9345f90();
   input += synapse0x9345fb8();
   input += synapse0x9345fe0();
   input += synapse0x9346008();
   input += synapse0x9346030();
   input += synapse0x9346058();
   input += synapse0x9346108();
   input += synapse0x9346130();
   input += synapse0x9346158();
   input += synapse0x9346180();
   input += synapse0x93461a8();
   input += synapse0x93461d0();
   input += synapse0x93461f8();
   input += synapse0x9346220();
   input += synapse0x9346248();
   input += synapse0x9346270();
   input += synapse0x9346298();
   input += synapse0x93462c0();
   input += synapse0x93462e8();
   input += synapse0x9346310();
   input += synapse0x9346338();
   input += synapse0x9346360();
   input += synapse0x9346080();
   input += synapse0x93460a8();
   input += synapse0x93460d0();
   input += synapse0x9346490();
   input += synapse0x93464b8();
   input += synapse0x93464e0();
   input += synapse0x9346508();
   input += synapse0x9346530();
   input += synapse0x9346558();
   input += synapse0x9346580();
   input += synapse0x93465a8();
   input += synapse0x93465d0();
   input += synapse0x93465f8();
   input += synapse0x9346620();
   input += synapse0x9346648();
   input += synapse0x9346670();
   input += synapse0x9346698();
   input += synapse0x93466c0();
   input += synapse0x93466e8();
   input += synapse0x9346710();
   input += synapse0x9346738();
   input += synapse0x9346760();
   input += synapse0x9346788();
   input += synapse0x93467b0();
   input += synapse0x93467d8();
   input += synapse0x9346800();
   input += synapse0x9346828();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::neuron0x9345cd8() {
   double input = input0x9345cd8();
   return (input * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x92d2620() {
   return (neuron0x9323918()*-0.025662);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90b0870() {
   return (neuron0x9323ac0()*0.270215);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90b0898() {
   return (neuron0x9332238()*0.132178);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90b0908() {
   return (neuron0x93323f0()*0.341093);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9333f00() {
   return (neuron0x93325f0()*0.548277);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9333f28() {
   return (neuron0x93327f0()*-0.0109528);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9333f50() {
   return (neuron0x93329f0()*-0.358172);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9333f78() {
   return (neuron0x9332bf0()*-0.146572);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9333fa0() {
   return (neuron0x9332df0()*-0.484937);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9333fc8() {
   return (neuron0x9332ff0()*0.231103);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9333ff0() {
   return (neuron0x93331f0()*-0.00333104);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334018() {
   return (neuron0x93333f0()*-0.151264);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334040() {
   return (neuron0x93335f0()*0.133489);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334068() {
   return (neuron0x93337f0()*0.287092);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334090() {
   return (neuron0x9333a08()*0.0764315);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90af698() {
   return (neuron0x9323918()*0.368397);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90af6c0() {
   return (neuron0x9323ac0()*-0.153386);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93343c8() {
   return (neuron0x9332238()*0.103054);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93343f0() {
   return (neuron0x93323f0()*-0.0260549);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334418() {
   return (neuron0x93325f0()*0.00838886);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334440() {
   return (neuron0x93327f0()*-0.156807);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334468() {
   return (neuron0x93329f0()*-0.211963);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334490() {
   return (neuron0x9332bf0()*-0.365476);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93344b8() {
   return (neuron0x9332df0()*-0.12671);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93344e0() {
   return (neuron0x9332ff0()*0.0354749);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334508() {
   return (neuron0x93331f0()*-0.211691);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334530() {
   return (neuron0x93333f0()*-0.0473323);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334558() {
   return (neuron0x93335f0()*-0.381388);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334580() {
   return (neuron0x93337f0()*0.394014);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93345a8() {
   return (neuron0x9333a08()*0.234876);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334788() {
   return (neuron0x9323918()*-0.206766);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93347b0() {
   return (neuron0x9323ac0()*0.0744426);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93347d8() {
   return (neuron0x9332238()*0.152109);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334340() {
   return (neuron0x93323f0()*-0.123162);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334368() {
   return (neuron0x93325f0()*0.022292);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334390() {
   return (neuron0x93327f0()*-0.397548);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334908() {
   return (neuron0x93329f0()*-0.349924);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334930() {
   return (neuron0x9332bf0()*0.341246);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334958() {
   return (neuron0x9332df0()*0.106137);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334980() {
   return (neuron0x9332ff0()*-0.0762782);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93349a8() {
   return (neuron0x93331f0()*-0.287495);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93349d0() {
   return (neuron0x93333f0()*-0.114666);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93349f8() {
   return (neuron0x93335f0()*-0.186726);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334a20() {
   return (neuron0x93337f0()*0.132402);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334a48() {
   return (neuron0x9333a08()*0.385801);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334c70() {
   return (neuron0x9323918()*0.304597);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334c98() {
   return (neuron0x9323ac0()*-0.414583);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334cc0() {
   return (neuron0x9332238()*-0.318751);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334ce8() {
   return (neuron0x93323f0()*-0.485275);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334d10() {
   return (neuron0x93325f0()*0.231435);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334d38() {
   return (neuron0x93327f0()*-0.258033);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334d60() {
   return (neuron0x93329f0()*-0.411417);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334d88() {
   return (neuron0x9332bf0()*0.401297);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334db0() {
   return (neuron0x9332df0()*0.230463);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334dd8() {
   return (neuron0x9332ff0()*-0.211753);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334e00() {
   return (neuron0x93331f0()*0.246885);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334e28() {
   return (neuron0x93333f0()*0.322777);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334e50() {
   return (neuron0x93335f0()*-0.291852);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334e78() {
   return (neuron0x93337f0()*0.0419047);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334ea0() {
   return (neuron0x9333a08()*-0.00477974);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93350c8() {
   return (neuron0x9323918()*0.525923);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93350f0() {
   return (neuron0x9323ac0()*0.048653);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335118() {
   return (neuron0x9332238()*0.511254);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335140() {
   return (neuron0x93323f0()*0.428099);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335168() {
   return (neuron0x93325f0()*0.275806);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90b07c8() {
   return (neuron0x93327f0()*-0.428059);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9323878() {
   return (neuron0x93329f0()*0.165879);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93238a0() {
   return (neuron0x9332bf0()*-0.445751);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90b0960() {
   return (neuron0x9332df0()*-0.0772404);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90b0988() {
   return (neuron0x9332ff0()*0.410523);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90b09b0() {
   return (neuron0x93331f0()*-0.371626);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x90b09d8() {
   return (neuron0x93333f0()*0.273373);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334800() {
   return (neuron0x93335f0()*-0.172393);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334828() {
   return (neuron0x93337f0()*0.480396);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334850() {
   return (neuron0x9333a08()*0.279421);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335508() {
   return (neuron0x9323918()*-0.100866);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335530() {
   return (neuron0x9323ac0()*0.191483);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335558() {
   return (neuron0x9332238()*0.00893754);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335580() {
   return (neuron0x93323f0()*0.515475);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93355a8() {
   return (neuron0x93325f0()*-0.259494);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93355d0() {
   return (neuron0x93327f0()*0.086228);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93355f8() {
   return (neuron0x93329f0()*-0.340878);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335620() {
   return (neuron0x9332bf0()*-0.357462);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335648() {
   return (neuron0x9332df0()*-0.254377);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335670() {
   return (neuron0x9332ff0()*-0.0554619);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335698() {
   return (neuron0x93331f0()*-0.458604);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93356c0() {
   return (neuron0x93333f0()*-0.343927);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93356e8() {
   return (neuron0x93335f0()*-0.369702);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335710() {
   return (neuron0x93337f0()*0.271677);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335738() {
   return (neuron0x9333a08()*-0.358295);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335960() {
   return (neuron0x9323918()*0.242881);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335988() {
   return (neuron0x9323ac0()*0.414045);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93359b0() {
   return (neuron0x9332238()*-0.191841);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93359d8() {
   return (neuron0x93323f0()*-0.176895);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335a00() {
   return (neuron0x93325f0()*-0.344499);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335a28() {
   return (neuron0x93327f0()*0.0727845);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335a50() {
   return (neuron0x93329f0()*0.0661776);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335a78() {
   return (neuron0x9332bf0()*0.251602);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335aa0() {
   return (neuron0x9332df0()*0.046567);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335ac8() {
   return (neuron0x9332ff0()*-0.0813993);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335af0() {
   return (neuron0x93331f0()*0.263297);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335b18() {
   return (neuron0x93333f0()*0.0874769);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335b40() {
   return (neuron0x93335f0()*-0.182451);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335b68() {
   return (neuron0x93337f0()*-0.462906);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335b90() {
   return (neuron0x9333a08()*-0.175003);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335db8() {
   return (neuron0x9323918()*-0.192015);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335de0() {
   return (neuron0x9323ac0()*0.390222);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335e08() {
   return (neuron0x9332238()*-0.0307482);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335e30() {
   return (neuron0x93323f0()*0.294922);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335e58() {
   return (neuron0x93325f0()*-0.0611391);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335e80() {
   return (neuron0x93327f0()*-0.373717);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335ea8() {
   return (neuron0x93329f0()*-0.35003);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335ed0() {
   return (neuron0x9332bf0()*0.0935998);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335ef8() {
   return (neuron0x9332df0()*-0.100662);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335f20() {
   return (neuron0x9332ff0()*0.0591677);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335f48() {
   return (neuron0x93331f0()*0.489915);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335f70() {
   return (neuron0x93333f0()*0.0333026);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335f98() {
   return (neuron0x93335f0()*0.333346);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335fc0() {
   return (neuron0x93337f0()*0.488477);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335fe8() {
   return (neuron0x9333a08()*0.0157872);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336210() {
   return (neuron0x9323918()*0.000373345);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336238() {
   return (neuron0x9323ac0()*-0.363685);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336260() {
   return (neuron0x9332238()*-0.305816);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336288() {
   return (neuron0x93323f0()*0.339579);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93362b0() {
   return (neuron0x93325f0()*-0.0472978);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93362d8() {
   return (neuron0x93327f0()*-0.352151);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336300() {
   return (neuron0x93329f0()*0.279018);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336328() {
   return (neuron0x9332bf0()*-0.105605);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336350() {
   return (neuron0x9332df0()*-0.178328);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335190() {
   return (neuron0x9332ff0()*0.510902);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93351b8() {
   return (neuron0x93331f0()*-0.195798);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93351e0() {
   return (neuron0x93333f0()*-0.378873);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335208() {
   return (neuron0x93335f0()*-0.0363671);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335230() {
   return (neuron0x93337f0()*0.0957003);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335258() {
   return (neuron0x9333a08()*-0.0179608);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9335358() {
   return (neuron0x9323918()*0.180809);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93368a8() {
   return (neuron0x9323ac0()*0.00337485);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93368d0() {
   return (neuron0x9332238()*-0.306981);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93368f8() {
   return (neuron0x93323f0()*0.249305);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336920() {
   return (neuron0x93325f0()*0.433356);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336948() {
   return (neuron0x93327f0()*-0.334506);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336970() {
   return (neuron0x93329f0()*0.101721);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336998() {
   return (neuron0x9332bf0()*-0.302267);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93369c0() {
   return (neuron0x9332df0()*-0.0603382);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93369e8() {
   return (neuron0x9332ff0()*0.200353);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336a10() {
   return (neuron0x93331f0()*0.12633);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336a38() {
   return (neuron0x93333f0()*0.0806277);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336a60() {
   return (neuron0x93335f0()*-0.309837);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336a88() {
   return (neuron0x93337f0()*-0.148403);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336ab0() {
   return (neuron0x9333a08()*0.0700611);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336cd8() {
   return (neuron0x9323918()*-0.00463881);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336d00() {
   return (neuron0x9323ac0()*0.106051);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336d28() {
   return (neuron0x9332238()*0.151914);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336d50() {
   return (neuron0x93323f0()*-0.299463);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336d78() {
   return (neuron0x93325f0()*-0.286148);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336da0() {
   return (neuron0x93327f0()*-0.0938756);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336dc8() {
   return (neuron0x93329f0()*-0.00294017);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336df0() {
   return (neuron0x9332bf0()*0.114157);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336e18() {
   return (neuron0x9332df0()*0.314788);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336e40() {
   return (neuron0x9332ff0()*-0.372847);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336e68() {
   return (neuron0x93331f0()*-0.18537);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336e90() {
   return (neuron0x93333f0()*0.0388867);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336eb8() {
   return (neuron0x93335f0()*0.313223);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336ee0() {
   return (neuron0x93337f0()*0.095249);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336f08() {
   return (neuron0x9333a08()*-0.095259);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337130() {
   return (neuron0x9323918()*0.422809);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337158() {
   return (neuron0x9323ac0()*0.422753);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337180() {
   return (neuron0x9332238()*0.323414);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93371a8() {
   return (neuron0x93323f0()*0.427036);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93371d0() {
   return (neuron0x93325f0()*-0.108787);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93371f8() {
   return (neuron0x93327f0()*-0.312639);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337220() {
   return (neuron0x93329f0()*-0.320081);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337248() {
   return (neuron0x9332bf0()*0.401227);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337270() {
   return (neuron0x9332df0()*0.317844);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337298() {
   return (neuron0x9332ff0()*0.171807);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93372c0() {
   return (neuron0x93331f0()*0.473313);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93372e8() {
   return (neuron0x93333f0()*-0.256716);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337310() {
   return (neuron0x93335f0()*0.302928);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337338() {
   return (neuron0x93337f0()*-0.127659);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337360() {
   return (neuron0x9333a08()*0.048295);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337588() {
   return (neuron0x9323918()*-0.0766468);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93375b0() {
   return (neuron0x9323ac0()*-0.398638);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93375d8() {
   return (neuron0x9332238()*0.00610485);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337600() {
   return (neuron0x93323f0()*0.037049);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337628() {
   return (neuron0x93325f0()*-0.196957);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337650() {
   return (neuron0x93327f0()*-0.142898);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337678() {
   return (neuron0x93329f0()*-0.0104672);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93376a0() {
   return (neuron0x9332bf0()*0.0183963);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93376c8() {
   return (neuron0x9332df0()*0.195628);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93376f0() {
   return (neuron0x9332ff0()*0.0699288);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337718() {
   return (neuron0x93331f0()*0.385091);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337740() {
   return (neuron0x93333f0()*0.266395);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337768() {
   return (neuron0x93335f0()*0.0618872);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337790() {
   return (neuron0x93337f0()*-0.312909);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93377b8() {
   return (neuron0x9333a08()*0.155949);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93379e0() {
   return (neuron0x9323918()*-0.149981);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337a08() {
   return (neuron0x9323ac0()*0.28255);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337a30() {
   return (neuron0x9332238()*0.033698);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337a58() {
   return (neuron0x93323f0()*-0.254914);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337a80() {
   return (neuron0x93325f0()*0.424234);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337aa8() {
   return (neuron0x93327f0()*-0.4745);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337ad0() {
   return (neuron0x93329f0()*0.480449);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337af8() {
   return (neuron0x9332bf0()*-0.0904935);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337b20() {
   return (neuron0x9332df0()*-0.388314);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337b48() {
   return (neuron0x9332ff0()*0.0919593);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337b70() {
   return (neuron0x93331f0()*0.499956);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337b98() {
   return (neuron0x93333f0()*-0.184601);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337bc0() {
   return (neuron0x93335f0()*-0.115266);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337be8() {
   return (neuron0x93337f0()*0.019719);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337c10() {
   return (neuron0x9333a08()*0.00808745);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337e38() {
   return (neuron0x9323918()*0.395416);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337e60() {
   return (neuron0x9323ac0()*0.0136009);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337e88() {
   return (neuron0x9332238()*0.0152273);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337eb0() {
   return (neuron0x93323f0()*-0.0183731);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337ed8() {
   return (neuron0x93325f0()*-0.353887);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337f00() {
   return (neuron0x93327f0()*0.431094);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337f28() {
   return (neuron0x93329f0()*-0.377639);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337f50() {
   return (neuron0x9332bf0()*-0.158386);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337f78() {
   return (neuron0x9332df0()*-0.432904);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337fa0() {
   return (neuron0x9332ff0()*-0.271022);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337fc8() {
   return (neuron0x93331f0()*0.065817);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9337ff0() {
   return (neuron0x93333f0()*0.0996985);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338018() {
   return (neuron0x93335f0()*-0.16136);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338040() {
   return (neuron0x93337f0()*-0.00753802);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338068() {
   return (neuron0x9333a08()*0.330681);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338290() {
   return (neuron0x9323918()*-0.14422);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x92d26b8() {
   return (neuron0x9323ac0()*-0.195679);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x92d26e0() {
   return (neuron0x9332238()*-0.203889);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338450() {
   return (neuron0x93323f0()*0.166955);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338500() {
   return (neuron0x93325f0()*-0.381729);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93385b0() {
   return (neuron0x93327f0()*-0.00141197);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338660() {
   return (neuron0x93329f0()*0.0404222);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338710() {
   return (neuron0x9332bf0()*0.245622);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93387c0() {
   return (neuron0x9332df0()*-0.152401);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338870() {
   return (neuron0x9332ff0()*-0.332211);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338920() {
   return (neuron0x93331f0()*-0.0638089);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93389d0() {
   return (neuron0x93333f0()*-0.40672);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338a80() {
   return (neuron0x93335f0()*0.205198);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338b30() {
   return (neuron0x93337f0()*0.409854);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338be0() {
   return (neuron0x9333a08()*0.254902);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338db8() {
   return (neuron0x9323918()*0.423692);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338de0() {
   return (neuron0x9323ac0()*0.336638);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338e08() {
   return (neuron0x9332238()*-0.0837032);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338e30() {
   return (neuron0x93323f0()*-0.318847);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338e58() {
   return (neuron0x93325f0()*0.0548105);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338e80() {
   return (neuron0x93327f0()*-0.0687918);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338ea8() {
   return (neuron0x93329f0()*-0.462353);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338ed0() {
   return (neuron0x9332bf0()*0.453103);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338ef8() {
   return (neuron0x9332df0()*0.108934);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338f20() {
   return (neuron0x9332ff0()*-0.0975944);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338f48() {
   return (neuron0x93331f0()*-0.0530736);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338f70() {
   return (neuron0x93333f0()*-0.169322);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338f98() {
   return (neuron0x93335f0()*-0.103561);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338fc0() {
   return (neuron0x93337f0()*0.20245);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338fe8() {
   return (neuron0x9333a08()*-0.401733);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93342b8() {
   return (neuron0x9323918()*0.215241);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93342e0() {
   return (neuron0x9323ac0()*-0.398547);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9334308() {
   return (neuron0x9332238()*-0.111755);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9332f60() {
   return (neuron0x93323f0()*-0.48703);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9332d60() {
   return (neuron0x93325f0()*-0.302927);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9332b60() {
   return (neuron0x93327f0()*-0.251293);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9332960() {
   return (neuron0x93329f0()*0.0724514);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9332760() {
   return (neuron0x9332bf0()*0.0908997);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9332560() {
   return (neuron0x9332df0()*0.128352);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9332360() {
   return (neuron0x9332ff0()*0.113225);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9323c58() {
   return (neuron0x93331f0()*-0.0427444);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336378() {
   return (neuron0x93333f0()*-0.23545);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93363a0() {
   return (neuron0x93335f0()*-0.165041);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93363c8() {
   return (neuron0x93337f0()*0.264567);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93363f0() {
   return (neuron0x9333a08()*0.348764);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336630() {
   return (neuron0x9323918()*0.357134);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336658() {
   return (neuron0x9323ac0()*0.0277736);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336680() {
   return (neuron0x9332238()*-0.191459);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93366a8() {
   return (neuron0x93323f0()*-0.115286);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93366d0() {
   return (neuron0x93325f0()*0.166558);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93366f8() {
   return (neuron0x93327f0()*0.193695);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336720() {
   return (neuron0x93329f0()*-0.0675503);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9336748() {
   return (neuron0x9332bf0()*-0.191422);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339a48() {
   return (neuron0x9332df0()*0.172283);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339a70() {
   return (neuron0x9332ff0()*0.179548);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339a98() {
   return (neuron0x93331f0()*-0.150761);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339ac0() {
   return (neuron0x93333f0()*0.0829757);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339ae8() {
   return (neuron0x93335f0()*-0.0699116);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339b10() {
   return (neuron0x93337f0()*-0.00339395);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339b38() {
   return (neuron0x9333a08()*-0.118952);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339d60() {
   return (neuron0x9323918()*0.203259);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339d88() {
   return (neuron0x9323ac0()*0.128303);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339db0() {
   return (neuron0x9332238()*-0.160614);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339dd8() {
   return (neuron0x93323f0()*0.11687);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339e00() {
   return (neuron0x93325f0()*-0.363411);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339e28() {
   return (neuron0x93327f0()*0.109762);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339e50() {
   return (neuron0x93329f0()*-0.0744504);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339e78() {
   return (neuron0x9332bf0()*0.000692306);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339ea0() {
   return (neuron0x9332df0()*-0.0679596);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339ec8() {
   return (neuron0x9332ff0()*0.105329);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339ef0() {
   return (neuron0x93331f0()*0.291781);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339f18() {
   return (neuron0x93333f0()*-0.0116895);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339f40() {
   return (neuron0x93335f0()*0.394016);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339f68() {
   return (neuron0x93337f0()*-0.182987);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339f90() {
   return (neuron0x9333a08()*-0.295511);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a1b8() {
   return (neuron0x9323918()*0.0539022);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a1e0() {
   return (neuron0x9323ac0()*0.108142);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a208() {
   return (neuron0x9332238()*0.318095);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a230() {
   return (neuron0x93323f0()*-0.103965);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a258() {
   return (neuron0x93325f0()*-0.401403);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a280() {
   return (neuron0x93327f0()*0.239178);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a2a8() {
   return (neuron0x93329f0()*0.000271807);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a2d0() {
   return (neuron0x9332bf0()*0.392623);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a2f8() {
   return (neuron0x9332df0()*0.412953);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a320() {
   return (neuron0x9332ff0()*0.47101);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a348() {
   return (neuron0x93331f0()*-0.419828);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a370() {
   return (neuron0x93333f0()*0.204494);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a398() {
   return (neuron0x93335f0()*-0.365838);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a3c0() {
   return (neuron0x93337f0()*0.153001);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a3e8() {
   return (neuron0x9333a08()*0.315264);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a610() {
   return (neuron0x9323918()*-0.0425002);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a638() {
   return (neuron0x9323ac0()*-0.217374);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a660() {
   return (neuron0x9332238()*-0.175552);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a688() {
   return (neuron0x93323f0()*-0.34905);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a6b0() {
   return (neuron0x93325f0()*0.282632);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a6d8() {
   return (neuron0x93327f0()*-0.409413);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a700() {
   return (neuron0x93329f0()*0.318066);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a728() {
   return (neuron0x9332bf0()*-0.0316276);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a750() {
   return (neuron0x9332df0()*-0.278157);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a778() {
   return (neuron0x9332ff0()*0.172559);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a7a0() {
   return (neuron0x93331f0()*0.27102);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a7c8() {
   return (neuron0x93333f0()*0.200018);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a7f0() {
   return (neuron0x93335f0()*-0.141912);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a818() {
   return (neuron0x93337f0()*0.0716001);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933a840() {
   return (neuron0x9333a08()*0.398667);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933aa68() {
   return (neuron0x9323918()*-0.200281);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933aa90() {
   return (neuron0x9323ac0()*-0.449806);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933aab8() {
   return (neuron0x9332238()*0.0136596);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933aae0() {
   return (neuron0x93323f0()*-0.140569);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ab08() {
   return (neuron0x93325f0()*0.133395);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ab30() {
   return (neuron0x93327f0()*0.14931);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ab58() {
   return (neuron0x93329f0()*0.317979);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ab80() {
   return (neuron0x9332bf0()*0.033114);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933aba8() {
   return (neuron0x9332df0()*-0.500004);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933abd0() {
   return (neuron0x9332ff0()*0.210848);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933abf8() {
   return (neuron0x93331f0()*0.392903);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ac20() {
   return (neuron0x93333f0()*-0.174894);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ac48() {
   return (neuron0x93335f0()*-0.163253);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ac70() {
   return (neuron0x93337f0()*0.474729);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ac98() {
   return (neuron0x9333a08()*-0.343922);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933aec0() {
   return (neuron0x9323918()*0.357878);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933aee8() {
   return (neuron0x9323ac0()*0.238355);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933af10() {
   return (neuron0x9332238()*-0.250509);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933af38() {
   return (neuron0x93323f0()*0.0208984);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933af60() {
   return (neuron0x93325f0()*-0.353106);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933af88() {
   return (neuron0x93327f0()*0.159079);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933afb0() {
   return (neuron0x93329f0()*-0.478921);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933afd8() {
   return (neuron0x9332bf0()*-0.211578);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b000() {
   return (neuron0x9332df0()*0.417875);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b028() {
   return (neuron0x9332ff0()*0.0360506);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b050() {
   return (neuron0x93331f0()*-0.474296);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b078() {
   return (neuron0x93333f0()*0.276981);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b0a0() {
   return (neuron0x93335f0()*-0.0896521);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b0c8() {
   return (neuron0x93337f0()*-0.241677);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b0f0() {
   return (neuron0x9333a08()*0.0294646);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b318() {
   return (neuron0x9323918()*0.366792);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b340() {
   return (neuron0x9323ac0()*0.0536059);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b368() {
   return (neuron0x9332238()*0.32197);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b390() {
   return (neuron0x93323f0()*0.444472);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b3b8() {
   return (neuron0x93325f0()*0.0530399);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b3e0() {
   return (neuron0x93327f0()*0.342343);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b408() {
   return (neuron0x93329f0()*-0.419872);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b430() {
   return (neuron0x9332bf0()*-0.336102);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b458() {
   return (neuron0x9332df0()*0.154335);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b480() {
   return (neuron0x9332ff0()*0.50532);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b4a8() {
   return (neuron0x93331f0()*0.0138262);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b4d0() {
   return (neuron0x93333f0()*0.319139);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b4f8() {
   return (neuron0x93335f0()*-0.366538);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b520() {
   return (neuron0x93337f0()*-0.318025);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b548() {
   return (neuron0x9333a08()*-0.424334);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b770() {
   return (neuron0x9323918()*-0.253758);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b798() {
   return (neuron0x9323ac0()*-0.219539);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b7c0() {
   return (neuron0x9332238()*-0.473895);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b7e8() {
   return (neuron0x93323f0()*-0.116909);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b810() {
   return (neuron0x93325f0()*0.00269622);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b838() {
   return (neuron0x93327f0()*-0.411996);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b860() {
   return (neuron0x93329f0()*0.0844819);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b888() {
   return (neuron0x9332bf0()*0.178676);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b8b0() {
   return (neuron0x9332df0()*0.4866);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b8d8() {
   return (neuron0x9332ff0()*-0.207714);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b900() {
   return (neuron0x93331f0()*0.366536);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b928() {
   return (neuron0x93333f0()*0.334976);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b950() {
   return (neuron0x93335f0()*0.0453179);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b978() {
   return (neuron0x93337f0()*-0.322295);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933b9a0() {
   return (neuron0x9333a08()*0.360011);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bbc8() {
   return (neuron0x9323918()*-0.144095);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bbf0() {
   return (neuron0x9323ac0()*-0.402299);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bc18() {
   return (neuron0x9332238()*0.0733631);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bc40() {
   return (neuron0x93323f0()*0.02769);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bc68() {
   return (neuron0x93325f0()*0.207494);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bc90() {
   return (neuron0x93327f0()*-0.360661);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bcb8() {
   return (neuron0x93329f0()*0.0240324);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bce0() {
   return (neuron0x9332bf0()*-0.245369);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bd08() {
   return (neuron0x9332df0()*0.460888);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bd30() {
   return (neuron0x9332ff0()*0.278734);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bd58() {
   return (neuron0x93331f0()*-0.451899);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bd80() {
   return (neuron0x93333f0()*0.103145);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bda8() {
   return (neuron0x93335f0()*0.266433);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bdd0() {
   return (neuron0x93337f0()*0.354223);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933bdf8() {
   return (neuron0x9333a08()*-0.240345);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c020() {
   return (neuron0x9323918()*0.404578);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c048() {
   return (neuron0x9323ac0()*-0.276123);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c070() {
   return (neuron0x9332238()*-0.352706);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c098() {
   return (neuron0x93323f0()*0.0271912);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c0c0() {
   return (neuron0x93325f0()*0.163632);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c0e8() {
   return (neuron0x93327f0()*0.223985);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c110() {
   return (neuron0x93329f0()*-0.0358958);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c138() {
   return (neuron0x9332bf0()*0.321922);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c160() {
   return (neuron0x9332df0()*0.157537);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c188() {
   return (neuron0x9332ff0()*0.412593);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c1b0() {
   return (neuron0x93331f0()*-0.407452);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c1d8() {
   return (neuron0x93333f0()*-0.307219);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c200() {
   return (neuron0x93335f0()*0.277597);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c228() {
   return (neuron0x93337f0()*0.276735);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c250() {
   return (neuron0x9333a08()*0.0882156);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c478() {
   return (neuron0x9323918()*0.245084);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c4a0() {
   return (neuron0x9323ac0()*-0.384045);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c4c8() {
   return (neuron0x9332238()*-0.0178313);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c4f0() {
   return (neuron0x93323f0()*-0.0641632);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c518() {
   return (neuron0x93325f0()*-0.17379);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c540() {
   return (neuron0x93327f0()*0.0504368);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c568() {
   return (neuron0x93329f0()*0.0520564);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c590() {
   return (neuron0x9332bf0()*-0.201103);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c5b8() {
   return (neuron0x9332df0()*-0.00537299);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c5e0() {
   return (neuron0x9332ff0()*0.19496);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c608() {
   return (neuron0x93331f0()*-0.16394);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c630() {
   return (neuron0x93333f0()*-0.457906);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c658() {
   return (neuron0x93335f0()*-0.279192);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c680() {
   return (neuron0x93337f0()*0.203609);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c6a8() {
   return (neuron0x9333a08()*-0.521094);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c8d0() {
   return (neuron0x9323918()*-0.0605079);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c8f8() {
   return (neuron0x9323ac0()*-0.255958);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c920() {
   return (neuron0x9332238()*0.0841167);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c948() {
   return (neuron0x93323f0()*-0.22632);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c970() {
   return (neuron0x93325f0()*0.119499);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c998() {
   return (neuron0x93327f0()*0.174091);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c9c0() {
   return (neuron0x93329f0()*-0.32039);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933c9e8() {
   return (neuron0x9332bf0()*0.298886);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ca10() {
   return (neuron0x9332df0()*-0.418902);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ca38() {
   return (neuron0x9332ff0()*-0.0790616);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ca60() {
   return (neuron0x93331f0()*0.134223);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ca88() {
   return (neuron0x93333f0()*0.0328935);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cab0() {
   return (neuron0x93335f0()*-0.298175);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cad8() {
   return (neuron0x93337f0()*-0.0724816);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cb00() {
   return (neuron0x9333a08()*-0.226608);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cd40() {
   return (neuron0x9323918()*-0.0260818);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cd68() {
   return (neuron0x9323ac0()*-0.369413);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cd90() {
   return (neuron0x9332238()*-0.405212);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cdb8() {
   return (neuron0x93323f0()*-0.0579669);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cde0() {
   return (neuron0x93325f0()*-0.166925);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ce08() {
   return (neuron0x93327f0()*-0.188656);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ce30() {
   return (neuron0x93329f0()*0.11853);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ce58() {
   return (neuron0x9332bf0()*0.242883);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ce80() {
   return (neuron0x9332df0()*-0.132993);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cea8() {
   return (neuron0x9332ff0()*-0.0139402);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933ced0() {
   return (neuron0x93331f0()*-0.21043);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cef8() {
   return (neuron0x93333f0()*0.303083);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cf20() {
   return (neuron0x93335f0()*-0.0240382);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cf48() {
   return (neuron0x93337f0()*0.0842514);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933cf70() {
   return (neuron0x9333a08()*-0.330761);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933d1b0() {
   return (neuron0x9323918()*0.0630678);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93382b8() {
   return (neuron0x9323ac0()*0.251905);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93382e0() {
   return (neuron0x9332238()*0.118928);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338308() {
   return (neuron0x93323f0()*0.147185);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338478() {
   return (neuron0x93325f0()*-0.284966);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93384a0() {
   return (neuron0x93327f0()*0.0612664);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93384c8() {
   return (neuron0x93329f0()*-0.0181879);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338528() {
   return (neuron0x9332bf0()*-0.487559);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338550() {
   return (neuron0x9332df0()*-0.159802);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338578() {
   return (neuron0x9332ff0()*0.110134);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93385d8() {
   return (neuron0x93331f0()*0.0596514);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338600() {
   return (neuron0x93333f0()*-0.164997);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338628() {
   return (neuron0x93335f0()*0.0323709);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338688() {
   return (neuron0x93337f0()*-0.296928);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93386b0() {
   return (neuron0x9333a08()*0.107117);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93388e0() {
   return (neuron0x9323918()*0.205349);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93386d8() {
   return (neuron0x9323ac0()*0.0969757);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338780() {
   return (neuron0x9332238()*-0.00197473);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338830() {
   return (neuron0x93323f0()*0.243036);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338948() {
   return (neuron0x93325f0()*-0.445306);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338970() {
   return (neuron0x93327f0()*-0.240862);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338998() {
   return (neuron0x93329f0()*0.172197);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93389f8() {
   return (neuron0x9332bf0()*-0.441027);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338a20() {
   return (neuron0x9332df0()*0.175976);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338a48() {
   return (neuron0x9332ff0()*-0.421538);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338aa8() {
   return (neuron0x93331f0()*-0.0883656);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338ad0() {
   return (neuron0x93333f0()*-0.16963);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338af8() {
   return (neuron0x93335f0()*-0.196653);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338b58() {
   return (neuron0x93337f0()*0.2995);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338b80() {
   return (neuron0x9333a08()*-0.0461034);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338ba8() {
   return (neuron0x9323918()*0.309057);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9338c50() {
   return (neuron0x9323ac0()*-0.486762);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e328() {
   return (neuron0x9332238()*-0.211103);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e350() {
   return (neuron0x93323f0()*0.143785);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e378() {
   return (neuron0x93325f0()*0.28831);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e3a0() {
   return (neuron0x93327f0()*0.39758);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e3c8() {
   return (neuron0x93329f0()*0.159943);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e3f0() {
   return (neuron0x9332bf0()*0.140816);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e418() {
   return (neuron0x9332df0()*0.0935839);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e440() {
   return (neuron0x9332ff0()*0.21205);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e468() {
   return (neuron0x93331f0()*-0.406451);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e490() {
   return (neuron0x93333f0()*-0.0718278);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e4b8() {
   return (neuron0x93335f0()*0.122227);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e4e0() {
   return (neuron0x93337f0()*-0.379958);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e508() {
   return (neuron0x9333a08()*-0.521141);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e730() {
   return (neuron0x9323918()*-0.307466);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e758() {
   return (neuron0x9323ac0()*0.396887);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933e780() {
   return (neuron0x9332238()*0.0981509);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339240() {
   return (neuron0x93323f0()*0.202782);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339268() {
   return (neuron0x93325f0()*-0.262017);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339290() {
   return (neuron0x93327f0()*-0.1147);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93392b8() {
   return (neuron0x93329f0()*-0.380795);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93392e0() {
   return (neuron0x9332bf0()*0.180051);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339308() {
   return (neuron0x9332df0()*0.277861);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339330() {
   return (neuron0x9332ff0()*-0.459031);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339358() {
   return (neuron0x93331f0()*0.0251474);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339380() {
   return (neuron0x93333f0()*-0.31372);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93393a8() {
   return (neuron0x93335f0()*0.0515801);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93393d0() {
   return (neuron0x93337f0()*0.0746765);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93393f8() {
   return (neuron0x9333a08()*0.0722883);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339620() {
   return (neuron0x9323918()*-0.35142);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339648() {
   return (neuron0x9323ac0()*0.210958);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339670() {
   return (neuron0x9332238()*0.286444);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339698() {
   return (neuron0x93323f0()*-0.0710429);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93396c0() {
   return (neuron0x93325f0()*-0.228284);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93396e8() {
   return (neuron0x93327f0()*-0.111181);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339710() {
   return (neuron0x93329f0()*-0.417083);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339738() {
   return (neuron0x9332bf0()*-0.0651657);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339760() {
   return (neuron0x9332df0()*0.242495);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339788() {
   return (neuron0x9332ff0()*0.267547);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93397b0() {
   return (neuron0x93331f0()*-0.298927);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93397d8() {
   return (neuron0x93333f0()*-0.35489);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339800() {
   return (neuron0x93335f0()*0.000805522);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339828() {
   return (neuron0x93337f0()*0.108613);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339850() {
   return (neuron0x9333a08()*-0.293346);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f7f8() {
   return (neuron0x9323918()*0.301069);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f820() {
   return (neuron0x9323ac0()*-0.195092);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f848() {
   return (neuron0x9332238()*-0.0814952);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f870() {
   return (neuron0x93323f0()*0.0244659);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f898() {
   return (neuron0x93325f0()*-0.478652);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f8c0() {
   return (neuron0x93327f0()*0.0636911);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f8e8() {
   return (neuron0x93329f0()*0.31708);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f910() {
   return (neuron0x9332bf0()*-0.333589);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f938() {
   return (neuron0x9332df0()*0.171506);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f960() {
   return (neuron0x9332ff0()*-0.346237);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f988() {
   return (neuron0x93331f0()*-0.096247);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f9b0() {
   return (neuron0x93333f0()*-0.0374265);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933f9d8() {
   return (neuron0x93335f0()*-0.0484132);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fa00() {
   return (neuron0x93337f0()*0.126143);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fa28() {
   return (neuron0x9333a08()*-0.338406);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fc50() {
   return (neuron0x9323918()*0.235015);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fc78() {
   return (neuron0x9323ac0()*-0.103662);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fca0() {
   return (neuron0x9332238()*0.20694);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fcc8() {
   return (neuron0x93323f0()*0.189444);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fcf0() {
   return (neuron0x93325f0()*-0.13527);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fd18() {
   return (neuron0x93327f0()*0.197811);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fd40() {
   return (neuron0x93329f0()*0.311331);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fd68() {
   return (neuron0x9332bf0()*-0.149654);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fd90() {
   return (neuron0x9332df0()*0.0488417);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fdb8() {
   return (neuron0x9332ff0()*-0.185647);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fde0() {
   return (neuron0x93331f0()*0.353012);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fe08() {
   return (neuron0x93333f0()*-0.314221);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fe30() {
   return (neuron0x93335f0()*0.256927);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fe58() {
   return (neuron0x93337f0()*-0.40802);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x933fe80() {
   return (neuron0x9333a08()*-0.234036);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93400c0() {
   return (neuron0x9323918()*0.407739);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93400e8() {
   return (neuron0x9323ac0()*0.0771527);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340110() {
   return (neuron0x9332238()*-0.313485);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340138() {
   return (neuron0x93323f0()*0.371502);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340160() {
   return (neuron0x93325f0()*0.0418862);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340188() {
   return (neuron0x93327f0()*-0.14956);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93401b0() {
   return (neuron0x93329f0()*-0.219112);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93401d8() {
   return (neuron0x9332bf0()*-0.402574);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340200() {
   return (neuron0x9332df0()*-0.23814);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340228() {
   return (neuron0x9332ff0()*0.109625);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340250() {
   return (neuron0x93331f0()*-0.0401856);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340278() {
   return (neuron0x93333f0()*-0.0714956);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93402a0() {
   return (neuron0x93335f0()*-0.255443);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93402c8() {
   return (neuron0x93337f0()*0.0316099);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93402f0() {
   return (neuron0x9333a08()*-0.3927);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340530() {
   return (neuron0x9323918()*-0.190722);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340558() {
   return (neuron0x9323ac0()*0.413356);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340580() {
   return (neuron0x9332238()*0.00482939);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93405a8() {
   return (neuron0x93323f0()*0.00899597);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93405d0() {
   return (neuron0x93325f0()*0.183397);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93405f8() {
   return (neuron0x93327f0()*-0.0979832);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340620() {
   return (neuron0x93329f0()*-0.241135);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340648() {
   return (neuron0x9332bf0()*-0.255858);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340670() {
   return (neuron0x9332df0()*-0.386535);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340698() {
   return (neuron0x9332ff0()*0.114014);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93406c0() {
   return (neuron0x93331f0()*-0.0829971);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93406e8() {
   return (neuron0x93333f0()*-0.418003);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340710() {
   return (neuron0x93335f0()*0.190728);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340738() {
   return (neuron0x93337f0()*-0.421282);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340760() {
   return (neuron0x9333a08()*0.122283);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93409a0() {
   return (neuron0x9323918()*-0.450847);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93409c8() {
   return (neuron0x9323ac0()*0.189872);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93409f0() {
   return (neuron0x9332238()*0.278073);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340a18() {
   return (neuron0x93323f0()*0.226425);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340a40() {
   return (neuron0x93325f0()*-0.0494556);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340a68() {
   return (neuron0x93327f0()*-0.306109);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340a90() {
   return (neuron0x93329f0()*-0.10524);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340ab8() {
   return (neuron0x9332bf0()*0.0613691);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340ae0() {
   return (neuron0x9332df0()*0.291288);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340b08() {
   return (neuron0x9332ff0()*-0.266298);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340b30() {
   return (neuron0x93331f0()*-0.415948);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340b58() {
   return (neuron0x93333f0()*-0.47774);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340b80() {
   return (neuron0x93335f0()*0.0210657);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340ba8() {
   return (neuron0x93337f0()*-0.130087);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340bd0() {
   return (neuron0x9333a08()*0.3116);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340e10() {
   return (neuron0x9323918()*0.324568);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340e38() {
   return (neuron0x9323ac0()*0.267235);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340e60() {
   return (neuron0x9332238()*-0.266685);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340e88() {
   return (neuron0x93323f0()*0.224902);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340eb0() {
   return (neuron0x93325f0()*0.126788);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340ed8() {
   return (neuron0x93327f0()*-0.355993);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340f00() {
   return (neuron0x93329f0()*0.268171);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340f28() {
   return (neuron0x9332bf0()*-0.425889);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340f50() {
   return (neuron0x9332df0()*0.353469);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340f78() {
   return (neuron0x9332ff0()*0.134413);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340fa0() {
   return (neuron0x93331f0()*0.29133);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340fc8() {
   return (neuron0x93333f0()*0.120679);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9340ff0() {
   return (neuron0x93335f0()*-0.443447);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341018() {
   return (neuron0x93337f0()*-0.490744);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341040() {
   return (neuron0x9333a08()*0.179775);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341280() {
   return (neuron0x9323918()*-0.348333);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93412a8() {
   return (neuron0x9323ac0()*-0.110887);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93412d0() {
   return (neuron0x9332238()*0.286657);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93412f8() {
   return (neuron0x93323f0()*-0.244687);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341320() {
   return (neuron0x93325f0()*0.253888);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341348() {
   return (neuron0x93327f0()*-0.394011);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341370() {
   return (neuron0x93329f0()*0.12286);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341398() {
   return (neuron0x9332bf0()*-0.0305925);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93413c0() {
   return (neuron0x9332df0()*-0.0254945);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93413e8() {
   return (neuron0x9332ff0()*0.132557);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341410() {
   return (neuron0x93331f0()*0.121671);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341438() {
   return (neuron0x93333f0()*0.434076);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341460() {
   return (neuron0x93335f0()*0.193275);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341488() {
   return (neuron0x93337f0()*-0.232115);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93414b0() {
   return (neuron0x9333a08()*0.0555279);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93416f0() {
   return (neuron0x9323918()*0.256967);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341718() {
   return (neuron0x9323ac0()*0.308366);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341740() {
   return (neuron0x9332238()*-0.338657);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341768() {
   return (neuron0x93323f0()*-0.308945);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341790() {
   return (neuron0x93325f0()*-0.0546915);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93417b8() {
   return (neuron0x93327f0()*0.227251);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93417e0() {
   return (neuron0x93329f0()*-0.0142205);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341808() {
   return (neuron0x9332bf0()*0.407513);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341830() {
   return (neuron0x9332df0()*0.0344512);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341858() {
   return (neuron0x9332ff0()*-0.00608393);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341880() {
   return (neuron0x93331f0()*-0.326987);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93418a8() {
   return (neuron0x93333f0()*-0.0266756);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93418d0() {
   return (neuron0x93335f0()*0.0556895);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93418f8() {
   return (neuron0x93337f0()*-0.198916);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341920() {
   return (neuron0x9333a08()*0.204197);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341b60() {
   return (neuron0x9323918()*-0.0122911);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341b88() {
   return (neuron0x9323ac0()*0.329569);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341bb0() {
   return (neuron0x9332238()*0.0840523);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341bd8() {
   return (neuron0x93323f0()*0.0921822);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341c00() {
   return (neuron0x93325f0()*0.269409);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341c28() {
   return (neuron0x93327f0()*0.4051);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341c50() {
   return (neuron0x93329f0()*-0.134153);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341c78() {
   return (neuron0x9332bf0()*0.00456018);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341ca0() {
   return (neuron0x9332df0()*0.049562);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341cc8() {
   return (neuron0x9332ff0()*-0.00847581);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341cf0() {
   return (neuron0x93331f0()*-0.0563065);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341d18() {
   return (neuron0x93333f0()*-0.102419);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341d40() {
   return (neuron0x93335f0()*-0.229792);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341d68() {
   return (neuron0x93337f0()*-0.417059);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341d90() {
   return (neuron0x9333a08()*0.429058);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341fd0() {
   return (neuron0x9323918()*0.176342);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9341ff8() {
   return (neuron0x9323ac0()*-0.0940025);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342020() {
   return (neuron0x9332238()*0.333176);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342048() {
   return (neuron0x93323f0()*0.391789);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342070() {
   return (neuron0x93325f0()*0.253843);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342098() {
   return (neuron0x93327f0()*0.23209);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93420c0() {
   return (neuron0x93329f0()*0.0293722);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93420e8() {
   return (neuron0x9332bf0()*0.170742);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342110() {
   return (neuron0x9332df0()*-0.167841);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342138() {
   return (neuron0x9332ff0()*-0.273896);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342160() {
   return (neuron0x93331f0()*-0.293785);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342188() {
   return (neuron0x93333f0()*-0.306094);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93421b0() {
   return (neuron0x93335f0()*-0.370783);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93421d8() {
   return (neuron0x93337f0()*0.0761291);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342200() {
   return (neuron0x9333a08()*0.116126);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342440() {
   return (neuron0x9323918()*0.2769);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342468() {
   return (neuron0x9323ac0()*0.373177);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342490() {
   return (neuron0x9332238()*0.0834908);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93424b8() {
   return (neuron0x93323f0()*-0.281522);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93424e0() {
   return (neuron0x93325f0()*0.053804);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342508() {
   return (neuron0x93327f0()*-0.0110437);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342530() {
   return (neuron0x93329f0()*-0.0117239);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342558() {
   return (neuron0x9332bf0()*-0.325237);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342580() {
   return (neuron0x9332df0()*-0.0756474);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93425a8() {
   return (neuron0x9332ff0()*-0.0577619);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93425d0() {
   return (neuron0x93331f0()*-0.27682);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93425f8() {
   return (neuron0x93333f0()*-0.162114);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342620() {
   return (neuron0x93335f0()*-0.0519859);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342648() {
   return (neuron0x93337f0()*-0.0749924);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342670() {
   return (neuron0x9333a08()*0.151025);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93428b0() {
   return (neuron0x9323918()*-0.138596);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93428d8() {
   return (neuron0x9323ac0()*-0.34299);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342900() {
   return (neuron0x9332238()*0.404444);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342928() {
   return (neuron0x93323f0()*0.189337);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342950() {
   return (neuron0x93325f0()*-0.358563);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342978() {
   return (neuron0x93327f0()*-0.119394);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93429a0() {
   return (neuron0x93329f0()*-0.228897);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93429c8() {
   return (neuron0x9332bf0()*0.221465);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93429f0() {
   return (neuron0x9332df0()*0.328923);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342a18() {
   return (neuron0x9332ff0()*0.265191);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342a40() {
   return (neuron0x93331f0()*-0.108314);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342a68() {
   return (neuron0x93333f0()*0.099009);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342a90() {
   return (neuron0x93335f0()*0.110006);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342ab8() {
   return (neuron0x93337f0()*0.315135);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342ae0() {
   return (neuron0x9333a08()*-0.330056);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342d20() {
   return (neuron0x9323918()*0.105782);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342d48() {
   return (neuron0x9323ac0()*0.435412);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342d70() {
   return (neuron0x9332238()*0.273018);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342d98() {
   return (neuron0x93323f0()*0.426694);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342dc0() {
   return (neuron0x93325f0()*-0.378406);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342de8() {
   return (neuron0x93327f0()*0.275279);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342e10() {
   return (neuron0x93329f0()*0.371346);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342e38() {
   return (neuron0x9332bf0()*0.0410698);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342e60() {
   return (neuron0x9332df0()*0.414494);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342e88() {
   return (neuron0x9332ff0()*-0.462495);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342eb0() {
   return (neuron0x93331f0()*-0.0959292);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342ed8() {
   return (neuron0x93333f0()*-0.309681);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342f00() {
   return (neuron0x93335f0()*0.192312);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342f28() {
   return (neuron0x93337f0()*0.478372);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9342f50() {
   return (neuron0x9333a08()*0.0821451);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339138() {
   return (neuron0x9323918()*-0.0642392);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339160() {
   return (neuron0x9323ac0()*0.075979);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339188() {
   return (neuron0x9332238()*0.176679);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93391b0() {
   return (neuron0x93323f0()*0.124095);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93391d8() {
   return (neuron0x93325f0()*0.32017);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9339200() {
   return (neuron0x93327f0()*0.370663);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343398() {
   return (neuron0x93329f0()*-0.378528);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93433c0() {
   return (neuron0x9332bf0()*0.0630111);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93433e8() {
   return (neuron0x9332df0()*0.196852);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343410() {
   return (neuron0x9332ff0()*0.0112504);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343438() {
   return (neuron0x93331f0()*-0.105086);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343460() {
   return (neuron0x93333f0()*0.451295);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343488() {
   return (neuron0x93335f0()*-0.350836);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93434b0() {
   return (neuron0x93337f0()*-0.426026);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93434d8() {
   return (neuron0x9333a08()*0.335331);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343700() {
   return (neuron0x9323918()*-0.44296);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343728() {
   return (neuron0x9323ac0()*0.421674);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343750() {
   return (neuron0x9332238()*0.44389);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343778() {
   return (neuron0x93323f0()*-0.275003);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93437a0() {
   return (neuron0x93325f0()*0.196117);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93437c8() {
   return (neuron0x93327f0()*0.447155);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93437f0() {
   return (neuron0x93329f0()*0.276253);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343818() {
   return (neuron0x9332bf0()*0.0402441);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343840() {
   return (neuron0x9332df0()*-0.224331);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343868() {
   return (neuron0x9332ff0()*-0.255126);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343890() {
   return (neuron0x93331f0()*0.152211);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93438b8() {
   return (neuron0x93333f0()*0.416773);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93438e0() {
   return (neuron0x93335f0()*0.253404);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343908() {
   return (neuron0x93337f0()*-0.0285839);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343930() {
   return (neuron0x9333a08()*-0.415629);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343b70() {
   return (neuron0x9323918()*0.272696);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343b98() {
   return (neuron0x9323ac0()*0.104861);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343bc0() {
   return (neuron0x9332238()*0.313867);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343be8() {
   return (neuron0x93323f0()*0.23759);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343c10() {
   return (neuron0x93325f0()*-0.0325021);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343c38() {
   return (neuron0x93327f0()*0.154461);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343c60() {
   return (neuron0x93329f0()*0.204498);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343c88() {
   return (neuron0x9332bf0()*-0.0919177);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343cb0() {
   return (neuron0x9332df0()*0.0528699);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343cd8() {
   return (neuron0x9332ff0()*0.315714);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343d00() {
   return (neuron0x93331f0()*-0.261457);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343d28() {
   return (neuron0x93333f0()*-0.0143973);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343d50() {
   return (neuron0x93335f0()*-0.238587);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343d78() {
   return (neuron0x93337f0()*-0.0532091);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343da0() {
   return (neuron0x9333a08()*0.409963);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9343fe0() {
   return (neuron0x9323918()*0.427474);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344008() {
   return (neuron0x9323ac0()*-0.149755);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344030() {
   return (neuron0x9332238()*0.180037);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344058() {
   return (neuron0x93323f0()*-0.0965452);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344080() {
   return (neuron0x93325f0()*-0.30262);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93440a8() {
   return (neuron0x93327f0()*0.261924);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93440d0() {
   return (neuron0x93329f0()*0.250779);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93440f8() {
   return (neuron0x9332bf0()*-0.199639);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344120() {
   return (neuron0x9332df0()*0.326794);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344148() {
   return (neuron0x9332ff0()*-0.335886);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344170() {
   return (neuron0x93331f0()*0.619669);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344198() {
   return (neuron0x93333f0()*-0.404259);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93441c0() {
   return (neuron0x93335f0()*0.453711);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93441e8() {
   return (neuron0x93337f0()*0.10448);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344210() {
   return (neuron0x9333a08()*0.233474);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344450() {
   return (neuron0x9323918()*-0.0860518);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344478() {
   return (neuron0x9323ac0()*0.372857);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93444a0() {
   return (neuron0x9332238()*-0.31681);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93444c8() {
   return (neuron0x93323f0()*-0.282811);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93444f0() {
   return (neuron0x93325f0()*-0.451608);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344518() {
   return (neuron0x93327f0()*-0.203774);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344540() {
   return (neuron0x93329f0()*0.0231952);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344568() {
   return (neuron0x9332bf0()*-0.0505023);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344590() {
   return (neuron0x9332df0()*-0.281868);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93445b8() {
   return (neuron0x9332ff0()*0.0233266);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93445e0() {
   return (neuron0x93331f0()*0.107365);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344608() {
   return (neuron0x93333f0()*-0.298824);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344630() {
   return (neuron0x93335f0()*0.031908);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344658() {
   return (neuron0x93337f0()*0.0143742);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344680() {
   return (neuron0x9333a08()*-0.162759);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93448c0() {
   return (neuron0x9323918()*0.000805872);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93448e8() {
   return (neuron0x9323ac0()*0.266808);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344910() {
   return (neuron0x9332238()*-0.257516);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344938() {
   return (neuron0x93323f0()*0.248435);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344960() {
   return (neuron0x93325f0()*-0.275642);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344988() {
   return (neuron0x93327f0()*-0.221649);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93449b0() {
   return (neuron0x93329f0()*-0.0521685);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93449d8() {
   return (neuron0x9332bf0()*-0.388778);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344a00() {
   return (neuron0x9332df0()*-0.0412716);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344a28() {
   return (neuron0x9332ff0()*-0.306533);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344a50() {
   return (neuron0x93331f0()*0.203059);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344a78() {
   return (neuron0x93333f0()*-0.177552);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344aa0() {
   return (neuron0x93335f0()*-0.679879);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344ac8() {
   return (neuron0x93337f0()*0.238131);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344af0() {
   return (neuron0x9333a08()*-0.267699);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344d30() {
   return (neuron0x9323918()*-0.442649);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344d58() {
   return (neuron0x9323ac0()*-0.114854);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344d80() {
   return (neuron0x9332238()*0.435582);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344da8() {
   return (neuron0x93323f0()*-0.171673);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344dd0() {
   return (neuron0x93325f0()*-0.242101);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344df8() {
   return (neuron0x93327f0()*0.45072);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344e20() {
   return (neuron0x93329f0()*0.47539);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344e48() {
   return (neuron0x9332bf0()*-0.0471305);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344e70() {
   return (neuron0x9332df0()*-0.0461864);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344e98() {
   return (neuron0x9332ff0()*-0.434405);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344ec0() {
   return (neuron0x93331f0()*0.435708);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344ee8() {
   return (neuron0x93333f0()*-0.462556);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344f10() {
   return (neuron0x93335f0()*-0.357551);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344f38() {
   return (neuron0x93337f0()*-0.00655326);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9344f60() {
   return (neuron0x9333a08()*-0.150045);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93451a0() {
   return (neuron0x9323918()*0.494382);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93451c8() {
   return (neuron0x9323ac0()*0.182417);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93451f0() {
   return (neuron0x9332238()*-0.0493165);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345218() {
   return (neuron0x93323f0()*0.405913);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345240() {
   return (neuron0x93325f0()*-0.411492);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345268() {
   return (neuron0x93327f0()*0.0371932);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345290() {
   return (neuron0x93329f0()*-0.270899);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93452b8() {
   return (neuron0x9332bf0()*0.154607);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93452e0() {
   return (neuron0x9332df0()*-0.211887);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345308() {
   return (neuron0x9332ff0()*0.0741858);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345330() {
   return (neuron0x93331f0()*-0.103612);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345358() {
   return (neuron0x93333f0()*0.362557);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345380() {
   return (neuron0x93335f0()*-0.231811);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93453a8() {
   return (neuron0x93337f0()*0.249005);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93453d0() {
   return (neuron0x9333a08()*-0.249533);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345610() {
   return (neuron0x9323918()*-0.35111);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345638() {
   return (neuron0x9323ac0()*-0.0727165);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345660() {
   return (neuron0x9332238()*0.313984);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345688() {
   return (neuron0x93323f0()*-0.243931);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93456b0() {
   return (neuron0x93325f0()*-0.0905905);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93456d8() {
   return (neuron0x93327f0()*-0.263431);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345700() {
   return (neuron0x93329f0()*-0.251439);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345728() {
   return (neuron0x9332bf0()*-0.0516118);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345750() {
   return (neuron0x9332df0()*-0.128219);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345778() {
   return (neuron0x9332ff0()*-0.109784);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93457a0() {
   return (neuron0x93331f0()*-0.36684);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93457c8() {
   return (neuron0x93333f0()*0.295271);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93457f0() {
   return (neuron0x93335f0()*0.129987);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345818() {
   return (neuron0x93337f0()*-0.183393);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345840() {
   return (neuron0x9333a08()*0.176961);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345a80() {
   return (neuron0x9323918()*0.23549);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345aa8() {
   return (neuron0x9323ac0()*0.429084);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345ad0() {
   return (neuron0x9332238()*-0.323353);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345af8() {
   return (neuron0x93323f0()*0.196699);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345b20() {
   return (neuron0x93325f0()*0.00997353);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345b48() {
   return (neuron0x93327f0()*0.0213604);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345b70() {
   return (neuron0x93329f0()*0.22548);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345b98() {
   return (neuron0x9332bf0()*-0.178822);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345bc0() {
   return (neuron0x9332df0()*-0.440021);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345be8() {
   return (neuron0x9332ff0()*-0.0882996);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345c10() {
   return (neuron0x93331f0()*-0.303357);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345c38() {
   return (neuron0x93333f0()*0.39458);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345c60() {
   return (neuron0x93335f0()*-0.287975);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345c88() {
   return (neuron0x93337f0()*0.240294);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345cb0() {
   return (neuron0x9333a08()*-0.17106);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345e00() {
   return (neuron0x9333d48()*0.0420571);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345e28() {
   return (neuron0x93340b8()*-0.0112979);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345e50() {
   return (neuron0x93345d0()*0.0125736);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345e78() {
   return (neuron0x9334a70()*-0.000562016);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345ea0() {
   return (neuron0x9334ec8()*0.00385355);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345ec8() {
   return (neuron0x9335398()*-0.00402499);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345ef0() {
   return (neuron0x9335760()*-0.0104448);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345f18() {
   return (neuron0x9335bb8()*-0.0124316);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345f40() {
   return (neuron0x9336010()*-0.00442845);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345f68() {
   return (neuron0x9336780()*0.0139117);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345f90() {
   return (neuron0x9336ad8()*-0.0154819);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345fb8() {
   return (neuron0x9336f30()*0.000214666);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9345fe0() {
   return (neuron0x9337388()*0.0306999);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346008() {
   return (neuron0x93377e0()*-0.00888231);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346030() {
   return (neuron0x9337c38()*0.00506247);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346058() {
   return (neuron0x9338090()*-0.0231483);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346108() {
   return (neuron0x9338c90()*0.00574893);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346130() {
   return (neuron0x9339010()*-0.00357139);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346158() {
   return (neuron0x9336418()*-0.0658724);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346180() {
   return (neuron0x9339b60()*0.0239531);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93461a8() {
   return (neuron0x9339fb8()*0.00131285);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93461d0() {
   return (neuron0x933a410()*0.0158308);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93461f8() {
   return (neuron0x933a868()*-0.0121387);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346220() {
   return (neuron0x933acc0()*-0.00871443);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346248() {
   return (neuron0x933b118()*0.00126661);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346270() {
   return (neuron0x933b570()*-0.00588107);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346298() {
   return (neuron0x933b9c8()*0.0287653);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93462c0() {
   return (neuron0x933be20()*0.0368094);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93462e8() {
   return (neuron0x933c278()*-0.00923969);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346310() {
   return (neuron0x933c6d0()*-0.00560748);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346338() {
   return (neuron0x933cb28()*0.0304741);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346360() {
   return (neuron0x933cf98()*-0.0102252);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346080() {
   return (neuron0x933e048()*0.00396216);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93460a8() {
   return (neuron0x933e170()*-0.0108698);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93460d0() {
   return (neuron0x933e530()*-0.0017131);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346490() {
   return (neuron0x9339420()*0.0178293);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93464b8() {
   return (neuron0x9339878()*0.0288259);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93464e0() {
   return (neuron0x933fa50()*-0.0389825);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346508() {
   return (neuron0x933fea8()*-0.0399171);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346530() {
   return (neuron0x9340318()*-0.0257671);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346558() {
   return (neuron0x9340788()*-0.0178704);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346580() {
   return (neuron0x9340bf8()*-0.00685186);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93465a8() {
   return (neuron0x9341068()*-0.0447832);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93465d0() {
   return (neuron0x93414d8()*-0.0453115);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93465f8() {
   return (neuron0x9341948()*0.0245726);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346620() {
   return (neuron0x9341db8()*0.00464075);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346648() {
   return (neuron0x9342228()*0.03551);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346670() {
   return (neuron0x9342698()*-0.00863545);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346698() {
   return (neuron0x9342b08()*0.00188104);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93466c0() {
   return (neuron0x9342f78()*-0.0214816);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93466e8() {
   return (neuron0x9343500()*0.00323819);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346710() {
   return (neuron0x9343958()*-0.00900996);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346738() {
   return (neuron0x9343dc8()*0.0200821);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346760() {
   return (neuron0x9344238()*-0.00618657);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346788() {
   return (neuron0x93446a8()*0.0140864);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93467b0() {
   return (neuron0x9344b18()*0.0110489);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x93467d8() {
   return (neuron0x9344f88()*0.00434145);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346800() {
   return (neuron0x93453f8()*-0.0183899);
}

double NNPerpElectronAngleCorrectionBCPDirPhi::synapse0x9346828() {
   return (neuron0x9345868()*0.0217369);
}

