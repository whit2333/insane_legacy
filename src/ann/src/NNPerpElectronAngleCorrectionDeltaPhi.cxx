#include "NNPerpElectronAngleCorrectionDeltaPhi.h"
#include <cmath>

double NNPerpElectronAngleCorrectionDeltaPhi::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - 0.703042)/0.0942299;
   input1 = (in1 - -0.0193889)/0.272452;
   input2 = (in2 - -4.54411)/32.4866;
   input3 = (in3 - -4.42958)/59.3589;
   input4 = (in4 - 2147.14)/963.804;
   input5 = (in5 - 0.00290412)/0.75057;
   input6 = (in6 - 0.00347513)/0.749626;
   switch(index) {
     case 0:
         return neuron0x9424d60();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionDeltaPhi::Value(int index, double* input) {
   input0 = (input[0] - 0.703042)/0.0942299;
   input1 = (input[1] - -0.0193889)/0.272452;
   input2 = (input[2] - -4.54411)/32.4866;
   input3 = (input[3] - -4.42958)/59.3589;
   input4 = (input[4] - 2147.14)/963.804;
   input5 = (input[5] - 0.00290412)/0.75057;
   input6 = (input[6] - 0.00347513)/0.749626;
   switch(index) {
     case 0:
         return neuron0x9424d60();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9415bb0() {
   return input0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9415de8() {
   return input1;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9416010() {
   return input2;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94245a8() {
   return input3;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9424760() {
   return input4;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9424960() {
   return input5;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9424b60() {
   return input6;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9424e88() {
   double input = -0.0108914;
   input += synapse0x8d67518();
   input += synapse0x8d44b90();
   input += synapse0x8d44bd8();
   input += synapse0x8d44ca8();
   input += synapse0x9425040();
   input += synapse0x9425068();
   input += synapse0x9425090();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9424e88() {
   double input = input0x9424e88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94250b8() {
   double input = 0.0146094;
   input += synapse0x94252b8();
   input += synapse0x94252e0();
   input += synapse0x9425308();
   input += synapse0x9425330();
   input += synapse0x9425358();
   input += synapse0x9425380();
   input += synapse0x94253a8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94250b8() {
   double input = input0x94250b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94253d0() {
   double input = -0.429807;
   input += synapse0x94255d0();
   input += synapse0x94255f8();
   input += synapse0x9425620();
   input += synapse0x94256d0();
   input += synapse0x94256f8();
   input += synapse0x9425720();
   input += synapse0x9425748();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94253d0() {
   double input = input0x94253d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9425770() {
   double input = 0.225442;
   input += synapse0x9425928();
   input += synapse0x9425950();
   input += synapse0x9425978();
   input += synapse0x94259a0();
   input += synapse0x94259c8();
   input += synapse0x94259f0();
   input += synapse0x9425a18();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9425770() {
   double input = input0x9425770();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9425a40() {
   double input = -0.293921;
   input += synapse0x9425c40();
   input += synapse0x9425c68();
   input += synapse0x9425c90();
   input += synapse0x9425cb8();
   input += synapse0x9425ce0();
   input += synapse0x9425648();
   input += synapse0x9425670();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9425a40() {
   double input = input0x9425a40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9425e10() {
   double input = -0.252775;
   input += synapse0x9425698();
   input += synapse0x9426010();
   input += synapse0x9426038();
   input += synapse0x9426060();
   input += synapse0x9426088();
   input += synapse0x94260b0();
   input += synapse0x94260d8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9425e10() {
   double input = input0x9425e10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9426100() {
   double input = -0.383853;
   input += synapse0x9426300();
   input += synapse0x9426328();
   input += synapse0x9426350();
   input += synapse0x9426378();
   input += synapse0x94263a0();
   input += synapse0x94263c8();
   input += synapse0x94263f0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9426100() {
   double input = input0x9426100();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9426418() {
   double input = -0.0292087;
   input += synapse0x9426618();
   input += synapse0x9426640();
   input += synapse0x9426668();
   input += synapse0x9426690();
   input += synapse0x94266b8();
   input += synapse0x94266e0();
   input += synapse0x9426708();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9426418() {
   double input = input0x9426418();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9426730() {
   double input = -0.498924;
   input += synapse0x9426948();
   input += synapse0x9426970();
   input += synapse0x9426998();
   input += synapse0x94269c0();
   input += synapse0x94269e8();
   input += synapse0x9426a10();
   input += synapse0x9426a38();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9426730() {
   double input = input0x9426730();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9426a60() {
   double input = 0.267147;
   input += synapse0x9426d00();
   input += synapse0x9426d28();
   input += synapse0x8d44ae8();
   input += synapse0x8d44cd0();
   input += synapse0x8d446a0();
   input += synapse0x9425d08();
   input += synapse0x9425d30();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9426a60() {
   double input = input0x9426a60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9426f58() {
   double input = -0.0590881;
   input += synapse0x9425de8();
   input += synapse0x94270c8();
   input += synapse0x94270f0();
   input += synapse0x9427118();
   input += synapse0x9427140();
   input += synapse0x9427168();
   input += synapse0x9427190();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9426f58() {
   double input = input0x9426f58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94271b8() {
   double input = -0.39422;
   input += synapse0x94273b8();
   input += synapse0x94273e0();
   input += synapse0x9427408();
   input += synapse0x9427430();
   input += synapse0x9427458();
   input += synapse0x9427480();
   input += synapse0x94274a8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94271b8() {
   double input = input0x94271b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94274d0() {
   double input = 0.16918;
   input += synapse0x94276d0();
   input += synapse0x94276f8();
   input += synapse0x9427720();
   input += synapse0x9427748();
   input += synapse0x9427770();
   input += synapse0x9427798();
   input += synapse0x94277c0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94274d0() {
   double input = input0x94274d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94277e8() {
   double input = -0.14952;
   input += synapse0x94279e8();
   input += synapse0x9427a10();
   input += synapse0x9427a38();
   input += synapse0x9427a60();
   input += synapse0x9427a88();
   input += synapse0x9427ab0();
   input += synapse0x9427ad8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94277e8() {
   double input = input0x94277e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9427b00() {
   double input = -0.524538;
   input += synapse0x9427d00();
   input += synapse0x9427d28();
   input += synapse0x9427d50();
   input += synapse0x9427d78();
   input += synapse0x9427da0();
   input += synapse0x9427dc8();
   input += synapse0x9427df0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9427b00() {
   double input = input0x9427b00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9427e18() {
   double input = 0.379678;
   input += synapse0x9428018();
   input += synapse0x94280c8();
   input += synapse0x9428178();
   input += synapse0x9428228();
   input += synapse0x94282d8();
   input += synapse0x9428388();
   input += synapse0x9428438();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9427e18() {
   double input = input0x9427e18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94284e8() {
   double input = -0.211127;
   input += synapse0x9428610();
   input += synapse0x9428638();
   input += synapse0x9428660();
   input += synapse0x9428688();
   input += synapse0x94286b0();
   input += synapse0x94286d8();
   input += synapse0x9428700();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94284e8() {
   double input = input0x94284e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9428728() {
   double input = 0.471035;
   input += synapse0x9428850();
   input += synapse0x9428878();
   input += synapse0x94288a0();
   input += synapse0x94288c8();
   input += synapse0x94288f0();
   input += synapse0x9428918();
   input += synapse0x9428940();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9428728() {
   double input = input0x9428728();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9428968() {
   double input = 0.0473604;
   input += synapse0x9428b20();
   input += synapse0x9428b48();
   input += synapse0x9428b70();
   input += synapse0x9426d50();
   input += synapse0x9426d78();
   input += synapse0x9426da0();
   input += synapse0x9426dc8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9428968() {
   double input = input0x9428968();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9426df0() {
   double input = -0.148458;
   input += synapse0x9426f30();
   input += synapse0x9429078();
   input += synapse0x94290a0();
   input += synapse0x94290c8();
   input += synapse0x94290f0();
   input += synapse0x9429118();
   input += synapse0x9429140();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9426df0() {
   double input = input0x9426df0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9429168() {
   double input = 0.110265;
   input += synapse0x9429380();
   input += synapse0x94293a8();
   input += synapse0x94293d0();
   input += synapse0x94293f8();
   input += synapse0x9429420();
   input += synapse0x9429448();
   input += synapse0x9429470();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9429168() {
   double input = input0x9429168();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9429498() {
   double input = 0.402954;
   input += synapse0x94296b0();
   input += synapse0x94296d8();
   input += synapse0x9429700();
   input += synapse0x9429728();
   input += synapse0x9429750();
   input += synapse0x9429778();
   input += synapse0x94297a0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9429498() {
   double input = input0x9429498();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94297c8() {
   double input = 0.246626;
   input += synapse0x94299e0();
   input += synapse0x9429a08();
   input += synapse0x9429a30();
   input += synapse0x9429a58();
   input += synapse0x9429a80();
   input += synapse0x9429aa8();
   input += synapse0x9429ad0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94297c8() {
   double input = input0x94297c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9429af8() {
   double input = 0.303489;
   input += synapse0x9429d10();
   input += synapse0x9429d38();
   input += synapse0x9429d60();
   input += synapse0x9429d88();
   input += synapse0x9429db0();
   input += synapse0x9429dd8();
   input += synapse0x9429e00();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9429af8() {
   double input = input0x9429af8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9429e28() {
   double input = -0.250761;
   input += synapse0x942a040();
   input += synapse0x942a068();
   input += synapse0x942a090();
   input += synapse0x942a0b8();
   input += synapse0x942a0e0();
   input += synapse0x942a108();
   input += synapse0x942a130();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9429e28() {
   double input = input0x9429e28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942a158() {
   double input = 0.443133;
   input += synapse0x9426c78();
   input += synapse0x9426ca0();
   input += synapse0x9426cc8();
   input += synapse0x942a478();
   input += synapse0x942a4a0();
   input += synapse0x942a4c8();
   input += synapse0x942a4f0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942a158() {
   double input = input0x942a158();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942a518() {
   double input = 0.221691;
   input += synapse0x942a730();
   input += synapse0x942a758();
   input += synapse0x942a780();
   input += synapse0x942a7a8();
   input += synapse0x942a7d0();
   input += synapse0x942a7f8();
   input += synapse0x942a820();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942a518() {
   double input = input0x942a518();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942a848() {
   double input = -0.331345;
   input += synapse0x942aa60();
   input += synapse0x942aa88();
   input += synapse0x942aab0();
   input += synapse0x942aad8();
   input += synapse0x942ab00();
   input += synapse0x942ab28();
   input += synapse0x942ab50();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942a848() {
   double input = input0x942a848();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942ab78() {
   double input = 0.243031;
   input += synapse0x942ad90();
   input += synapse0x942adb8();
   input += synapse0x942ade0();
   input += synapse0x942ae08();
   input += synapse0x942ae30();
   input += synapse0x942ae58();
   input += synapse0x942ae80();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942ab78() {
   double input = input0x942ab78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942aea8() {
   double input = -0.171924;
   input += synapse0x942b0c0();
   input += synapse0x942b0e8();
   input += synapse0x942b110();
   input += synapse0x942b138();
   input += synapse0x942b160();
   input += synapse0x942b188();
   input += synapse0x942b1b0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942aea8() {
   double input = input0x942aea8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942b1d8() {
   double input = 0.527916;
   input += synapse0x942b3f0();
   input += synapse0x942b418();
   input += synapse0x942b440();
   input += synapse0x942b468();
   input += synapse0x942b490();
   input += synapse0x942b4b8();
   input += synapse0x942b4e0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942b1d8() {
   double input = input0x942b1d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942b508() {
   double input = -0.0812454;
   input += synapse0x942b720();
   input += synapse0x9428040();
   input += synapse0x9428068();
   input += synapse0x9428090();
   input += synapse0x94280f0();
   input += synapse0x9428118();
   input += synapse0x9428140();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942b508() {
   double input = input0x942b508();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942be80() {
   double input = 0.180558;
   input += synapse0x9428200();
   input += synapse0x9428348();
   input += synapse0x9428298();
   input += synapse0x94283b0();
   input += synapse0x94283d8();
   input += synapse0x9428400();
   input += synapse0x9428460();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942be80() {
   double input = input0x942be80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942bfa8() {
   double input = 0.186654;
   input += synapse0x942c160();
   input += synapse0x942c188();
   input += synapse0x942c1b0();
   input += synapse0x942c1d8();
   input += synapse0x942c200();
   input += synapse0x942c228();
   input += synapse0x942c250();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942bfa8() {
   double input = input0x942bfa8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942c278() {
   double input = 0.221437;
   input += synapse0x942c478();
   input += synapse0x942c4a0();
   input += synapse0x942c4c8();
   input += synapse0x942c4f0();
   input += synapse0x942c518();
   input += synapse0x942c540();
   input += synapse0x942c568();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942c278() {
   double input = input0x942c278();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942c590() {
   double input = 0.541035;
   input += synapse0x942c790();
   input += synapse0x942c7b8();
   input += synapse0x942c7e0();
   input += synapse0x942c808();
   input += synapse0x942c830();
   input += synapse0x942c858();
   input += synapse0x942c880();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942c590() {
   double input = input0x942c590();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942c8a8() {
   double input = -0.449015;
   input += synapse0x942cac0();
   input += synapse0x942cae8();
   input += synapse0x942cb10();
   input += synapse0x942cb38();
   input += synapse0x942cb60();
   input += synapse0x9428b98();
   input += synapse0x9428bc0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942c8a8() {
   double input = input0x942c8a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9428be8() {
   double input = 0.353287;
   input += synapse0x9428e00();
   input += synapse0x9428e28();
   input += synapse0x9428e50();
   input += synapse0x9428e78();
   input += synapse0x9428ea0();
   input += synapse0x9428ec8();
   input += synapse0x9428ef0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9428be8() {
   double input = input0x9428be8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942d390() {
   double input = 0.0234615;
   input += synapse0x9428f78();
   input += synapse0x942d548();
   input += synapse0x942d570();
   input += synapse0x942d598();
   input += synapse0x942d5c0();
   input += synapse0x942d5e8();
   input += synapse0x942d610();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942d390() {
   double input = input0x942d390();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942d638() {
   double input = -0.415234;
   input += synapse0x942d850();
   input += synapse0x942d878();
   input += synapse0x942d8a0();
   input += synapse0x942d8c8();
   input += synapse0x942d8f0();
   input += synapse0x942d918();
   input += synapse0x942d940();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942d638() {
   double input = input0x942d638();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942d968() {
   double input = 0.371891;
   input += synapse0x942db80();
   input += synapse0x942dba8();
   input += synapse0x942dbd0();
   input += synapse0x942dbf8();
   input += synapse0x942dc20();
   input += synapse0x942dc48();
   input += synapse0x942dc70();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942d968() {
   double input = input0x942d968();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942dc98() {
   double input = 0.0281068;
   input += synapse0x942deb0();
   input += synapse0x942ded8();
   input += synapse0x942df00();
   input += synapse0x942df28();
   input += synapse0x942df50();
   input += synapse0x942df78();
   input += synapse0x942dfa0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942dc98() {
   double input = input0x942dc98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942dfc8() {
   double input = -0.183762;
   input += synapse0x942e1e0();
   input += synapse0x942e208();
   input += synapse0x942e230();
   input += synapse0x942e258();
   input += synapse0x942e280();
   input += synapse0x942e2a8();
   input += synapse0x942e2d0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942dfc8() {
   double input = input0x942dfc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942e2f8() {
   double input = 0.461583;
   input += synapse0x942e510();
   input += synapse0x942e538();
   input += synapse0x942e560();
   input += synapse0x942e588();
   input += synapse0x942e5b0();
   input += synapse0x942e5d8();
   input += synapse0x942e600();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942e2f8() {
   double input = input0x942e2f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942e628() {
   double input = -0.36293;
   input += synapse0x942e840();
   input += synapse0x942e868();
   input += synapse0x942e890();
   input += synapse0x942e8b8();
   input += synapse0x942e8e0();
   input += synapse0x942e908();
   input += synapse0x942e930();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942e628() {
   double input = input0x942e628();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942e958() {
   double input = -0.49203;
   input += synapse0x942eb70();
   input += synapse0x942eb98();
   input += synapse0x942ebc0();
   input += synapse0x942ebe8();
   input += synapse0x942ec10();
   input += synapse0x942ec38();
   input += synapse0x942ec60();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942e958() {
   double input = input0x942e958();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942ec88() {
   double input = -0.0528861;
   input += synapse0x942eea0();
   input += synapse0x942eec8();
   input += synapse0x942eef0();
   input += synapse0x942ef18();
   input += synapse0x942ef40();
   input += synapse0x942ef68();
   input += synapse0x942ef90();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942ec88() {
   double input = input0x942ec88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942efb8() {
   double input = 0.131969;
   input += synapse0x942f1d0();
   input += synapse0x942f1f8();
   input += synapse0x942f220();
   input += synapse0x942f248();
   input += synapse0x942f270();
   input += synapse0x942f298();
   input += synapse0x942f2c0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942efb8() {
   double input = input0x942efb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942f2e8() {
   double input = -0.714153;
   input += synapse0x942f500();
   input += synapse0x942f528();
   input += synapse0x942f550();
   input += synapse0x942f578();
   input += synapse0x942f5a0();
   input += synapse0x942f5c8();
   input += synapse0x942f5f0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942f2e8() {
   double input = input0x942f2e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942f618() {
   double input = 0.528342;
   input += synapse0x942f830();
   input += synapse0x942f858();
   input += synapse0x942f880();
   input += synapse0x942f8a8();
   input += synapse0x942f8d0();
   input += synapse0x942f8f8();
   input += synapse0x942f920();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942f618() {
   double input = input0x942f618();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942f948() {
   double input = -0.232111;
   input += synapse0x942fb60();
   input += synapse0x942fb88();
   input += synapse0x942fbb0();
   input += synapse0x942fbd8();
   input += synapse0x942fc00();
   input += synapse0x942fc28();
   input += synapse0x942fc50();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942f948() {
   double input = input0x942f948();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942fc78() {
   double input = -0.154552;
   input += synapse0x942fe90();
   input += synapse0x942feb8();
   input += synapse0x942fee0();
   input += synapse0x942ff08();
   input += synapse0x942ff30();
   input += synapse0x942ff58();
   input += synapse0x942ff80();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942fc78() {
   double input = input0x942fc78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942ffa8() {
   double input = -0.18914;
   input += synapse0x94301c0();
   input += synapse0x94301e8();
   input += synapse0x9430210();
   input += synapse0x9430238();
   input += synapse0x9430260();
   input += synapse0x9430288();
   input += synapse0x94302b0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942ffa8() {
   double input = input0x942ffa8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94302d8() {
   double input = 0.41085;
   input += synapse0x94304f0();
   input += synapse0x9430518();
   input += synapse0x9430540();
   input += synapse0x9430568();
   input += synapse0x9430590();
   input += synapse0x94305b8();
   input += synapse0x94305e0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94302d8() {
   double input = input0x94302d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9430608() {
   double input = -0.205549;
   input += synapse0x9430820();
   input += synapse0x9430848();
   input += synapse0x9430870();
   input += synapse0x9430898();
   input += synapse0x94308c0();
   input += synapse0x94308e8();
   input += synapse0x9430910();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9430608() {
   double input = input0x9430608();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9430938() {
   double input = 0.273282;
   input += synapse0x9430b50();
   input += synapse0x9430b78();
   input += synapse0x9430ba0();
   input += synapse0x9430bc8();
   input += synapse0x9430bf0();
   input += synapse0x9430c18();
   input += synapse0x9430c40();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9430938() {
   double input = input0x9430938();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9430c68() {
   double input = 0.266593;
   input += synapse0x9430e80();
   input += synapse0x9430ea8();
   input += synapse0x9430ed0();
   input += synapse0x9430ef8();
   input += synapse0x9430f20();
   input += synapse0x9430f48();
   input += synapse0x9430f70();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9430c68() {
   double input = input0x9430c68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9430f98() {
   double input = -0.344251;
   input += synapse0x942a370();
   input += synapse0x942a398();
   input += synapse0x942a3c0();
   input += synapse0x942a3e8();
   input += synapse0x942a410();
   input += synapse0x942a438();
   input += synapse0x94313b8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9430f98() {
   double input = input0x9430f98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94313e0() {
   double input = 0.238136;
   input += synapse0x94315e0();
   input += synapse0x9431608();
   input += synapse0x9431630();
   input += synapse0x9431658();
   input += synapse0x9431680();
   input += synapse0x94316a8();
   input += synapse0x94316d0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94313e0() {
   double input = input0x94313e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94316f8() {
   double input = -0.0314334;
   input += synapse0x9431910();
   input += synapse0x9431938();
   input += synapse0x9431960();
   input += synapse0x9431988();
   input += synapse0x94319b0();
   input += synapse0x94319d8();
   input += synapse0x9431a00();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94316f8() {
   double input = input0x94316f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9431a28() {
   double input = 0.183911;
   input += synapse0x9431c40();
   input += synapse0x9431c68();
   input += synapse0x9431c90();
   input += synapse0x9431cb8();
   input += synapse0x9431ce0();
   input += synapse0x9431d08();
   input += synapse0x9431d30();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9431a28() {
   double input = input0x9431a28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9431d58() {
   double input = -0.394035;
   input += synapse0x9431f70();
   input += synapse0x9431f98();
   input += synapse0x9431fc0();
   input += synapse0x9431fe8();
   input += synapse0x9432010();
   input += synapse0x9432038();
   input += synapse0x9432060();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9431d58() {
   double input = input0x9431d58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9432088() {
   double input = -0.215861;
   input += synapse0x94322a0();
   input += synapse0x94322c8();
   input += synapse0x94322f0();
   input += synapse0x9432318();
   input += synapse0x9432340();
   input += synapse0x9432368();
   input += synapse0x9432390();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9432088() {
   double input = input0x9432088();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94323b8() {
   double input = -0.418104;
   input += synapse0x94325d0();
   input += synapse0x942b748();
   input += synapse0x942b770();
   input += synapse0x942b798();
   input += synapse0x942b9c8();
   input += synapse0x942b9f0();
   input += synapse0x942bc20();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94323b8() {
   double input = input0x94323b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942bc48() {
   double input = 0.120164;
   input += synapse0x9433020();
   input += synapse0x9433048();
   input += synapse0x9433070();
   input += synapse0x9433098();
   input += synapse0x94330c0();
   input += synapse0x94330e8();
   input += synapse0x9433110();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942bc48() {
   double input = input0x942bc48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9433138() {
   double input = -0.440619;
   input += synapse0x9433338();
   input += synapse0x9433360();
   input += synapse0x9433388();
   input += synapse0x94333b0();
   input += synapse0x94333d8();
   input += synapse0x9433400();
   input += synapse0x9433428();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9433138() {
   double input = input0x9433138();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9433450() {
   double input = 0.597045;
   input += synapse0x9433668();
   input += synapse0x9433690();
   input += synapse0x94336b8();
   input += synapse0x94336e0();
   input += synapse0x9433708();
   input += synapse0x9433730();
   input += synapse0x9433758();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9433450() {
   double input = input0x9433450();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9433780() {
   double input = -0.234482;
   input += synapse0x9433998();
   input += synapse0x94339c0();
   input += synapse0x94339e8();
   input += synapse0x9433a10();
   input += synapse0x9433a38();
   input += synapse0x9433a60();
   input += synapse0x9433a88();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9433780() {
   double input = input0x9433780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9433ab0() {
   double input = 0.303779;
   input += synapse0x9433cc8();
   input += synapse0x9433cf0();
   input += synapse0x9433d18();
   input += synapse0x9433d40();
   input += synapse0x9433d68();
   input += synapse0x9433d90();
   input += synapse0x9433db8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9433ab0() {
   double input = input0x9433ab0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9433de0() {
   double input = 0.317979;
   input += synapse0x9433ff8();
   input += synapse0x9434020();
   input += synapse0x9434048();
   input += synapse0x9434070();
   input += synapse0x9434098();
   input += synapse0x94340c0();
   input += synapse0x94340e8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9433de0() {
   double input = input0x9433de0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9434110() {
   double input = 0.0344764;
   input += synapse0x9434328();
   input += synapse0x9434350();
   input += synapse0x9434378();
   input += synapse0x94343a0();
   input += synapse0x94343c8();
   input += synapse0x94343f0();
   input += synapse0x9434418();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9434110() {
   double input = input0x9434110();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9434440() {
   double input = -0.0295094;
   input += synapse0x9434658();
   input += synapse0x9434680();
   input += synapse0x94346a8();
   input += synapse0x94346d0();
   input += synapse0x94346f8();
   input += synapse0x9434720();
   input += synapse0x9434748();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9434440() {
   double input = input0x9434440();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9434770() {
   double input = 0.297322;
   input += synapse0x9434988();
   input += synapse0x94349b0();
   input += synapse0x94349d8();
   input += synapse0x9434a00();
   input += synapse0x9434a28();
   input += synapse0x9434a50();
   input += synapse0x9434a78();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9434770() {
   double input = input0x9434770();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9434aa0() {
   double input = -0.197559;
   input += synapse0x9434cb8();
   input += synapse0x9434ce0();
   input += synapse0x942cb88();
   input += synapse0x942cbb0();
   input += synapse0x942cbd8();
   input += synapse0x942cc00();
   input += synapse0x942cc28();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9434aa0() {
   double input = input0x9434aa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942cc50() {
   double input = -0.639019;
   input += synapse0x942ce68();
   input += synapse0x942ce90();
   input += synapse0x942ceb8();
   input += synapse0x942cee0();
   input += synapse0x942cf08();
   input += synapse0x942cf30();
   input += synapse0x942cf58();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942cc50() {
   double input = input0x942cc50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x942cf80() {
   double input = 0.0824627;
   input += synapse0x942d198();
   input += synapse0x942d1c0();
   input += synapse0x942d1e8();
   input += synapse0x942d210();
   input += synapse0x942d238();
   input += synapse0x942d260();
   input += synapse0x942d288();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x942cf80() {
   double input = input0x942cf80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9435d10() {
   double input = -0.44104;
   input += synapse0x942d358();
   input += synapse0x9435e80();
   input += synapse0x9435ea8();
   input += synapse0x9435ed0();
   input += synapse0x9435ef8();
   input += synapse0x9435f20();
   input += synapse0x9435f48();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9435d10() {
   double input = input0x9435d10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9435f70() {
   double input = 0.158957;
   input += synapse0x9436188();
   input += synapse0x94361b0();
   input += synapse0x94361d8();
   input += synapse0x9436200();
   input += synapse0x9436228();
   input += synapse0x9436250();
   input += synapse0x9436278();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9435f70() {
   double input = input0x9435f70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94362a0() {
   double input = -0.336346;
   input += synapse0x94364b8();
   input += synapse0x94364e0();
   input += synapse0x9436508();
   input += synapse0x9436530();
   input += synapse0x9436558();
   input += synapse0x9436580();
   input += synapse0x94365a8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94362a0() {
   double input = input0x94362a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94365d0() {
   double input = -0.331253;
   input += synapse0x94367e8();
   input += synapse0x9436810();
   input += synapse0x9436838();
   input += synapse0x9436860();
   input += synapse0x9436888();
   input += synapse0x94368b0();
   input += synapse0x94368d8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94365d0() {
   double input = input0x94365d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9436900() {
   double input = -0.27985;
   input += synapse0x9436b18();
   input += synapse0x9436b40();
   input += synapse0x9436b68();
   input += synapse0x9436b90();
   input += synapse0x9436bb8();
   input += synapse0x9436be0();
   input += synapse0x9436c08();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9436900() {
   double input = input0x9436900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9436c30() {
   double input = -0.338812;
   input += synapse0x9436e48();
   input += synapse0x9436e70();
   input += synapse0x9436e98();
   input += synapse0x9436ec0();
   input += synapse0x9436ee8();
   input += synapse0x9436f10();
   input += synapse0x9436f38();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9436c30() {
   double input = input0x9436c30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9436f60() {
   double input = -0.138822;
   input += synapse0x9437178();
   input += synapse0x94371a0();
   input += synapse0x94371c8();
   input += synapse0x94371f0();
   input += synapse0x9437218();
   input += synapse0x9437240();
   input += synapse0x9437268();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9436f60() {
   double input = input0x9436f60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9437290() {
   double input = -0.173195;
   input += synapse0x94374a8();
   input += synapse0x94374d0();
   input += synapse0x94374f8();
   input += synapse0x9437520();
   input += synapse0x9437548();
   input += synapse0x9437570();
   input += synapse0x9437598();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9437290() {
   double input = input0x9437290();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94375c0() {
   double input = 0.463153;
   input += synapse0x94377d8();
   input += synapse0x9437800();
   input += synapse0x9437828();
   input += synapse0x9437850();
   input += synapse0x9437878();
   input += synapse0x94378a0();
   input += synapse0x94378c8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94375c0() {
   double input = input0x94375c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94378f0() {
   double input = 0.0182654;
   input += synapse0x9437b08();
   input += synapse0x9437b30();
   input += synapse0x9437b58();
   input += synapse0x9437b80();
   input += synapse0x9437ba8();
   input += synapse0x9437bd0();
   input += synapse0x9437bf8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94378f0() {
   double input = input0x94378f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9437c20() {
   double input = -0.0940569;
   input += synapse0x9437e38();
   input += synapse0x9437e60();
   input += synapse0x9437e88();
   input += synapse0x9437eb0();
   input += synapse0x9437ed8();
   input += synapse0x9437f00();
   input += synapse0x9437f28();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9437c20() {
   double input = input0x9437c20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9437f50() {
   double input = -0.0455703;
   input += synapse0x9438168();
   input += synapse0x9438190();
   input += synapse0x94381b8();
   input += synapse0x94381e0();
   input += synapse0x9438208();
   input += synapse0x9438230();
   input += synapse0x9438258();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9437f50() {
   double input = input0x9437f50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9438280() {
   double input = -0.39553;
   input += synapse0x9438498();
   input += synapse0x94384c0();
   input += synapse0x94384e8();
   input += synapse0x9438510();
   input += synapse0x9438538();
   input += synapse0x9438560();
   input += synapse0x9438588();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9438280() {
   double input = input0x9438280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94385b0() {
   double input = -0.340047;
   input += synapse0x94387c8();
   input += synapse0x94387f0();
   input += synapse0x9438818();
   input += synapse0x9438840();
   input += synapse0x9438868();
   input += synapse0x9438890();
   input += synapse0x94388b8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94385b0() {
   double input = input0x94385b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94388e0() {
   double input = -0.433831;
   input += synapse0x9438af8();
   input += synapse0x9438b20();
   input += synapse0x9438b48();
   input += synapse0x9438b70();
   input += synapse0x9438b98();
   input += synapse0x9438bc0();
   input += synapse0x9438be8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94388e0() {
   double input = input0x94388e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9438c10() {
   double input = 0.278604;
   input += synapse0x9438e28();
   input += synapse0x9438e50();
   input += synapse0x9438e78();
   input += synapse0x9438ea0();
   input += synapse0x9438ec8();
   input += synapse0x9438ef0();
   input += synapse0x9438f18();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9438c10() {
   double input = input0x9438c10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9438f40() {
   double input = 0.29712;
   input += synapse0x9439158();
   input += synapse0x9439180();
   input += synapse0x94391a8();
   input += synapse0x94391d0();
   input += synapse0x94391f8();
   input += synapse0x9439220();
   input += synapse0x9439248();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9438f40() {
   double input = input0x9438f40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9439270() {
   double input = -0.782561;
   input += synapse0x9439488();
   input += synapse0x94394b0();
   input += synapse0x94394d8();
   input += synapse0x9439500();
   input += synapse0x9439528();
   input += synapse0x9439550();
   input += synapse0x9439578();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9439270() {
   double input = input0x9439270();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94395a0() {
   double input = -0.0539979;
   input += synapse0x94397b8();
   input += synapse0x94397e0();
   input += synapse0x9439808();
   input += synapse0x9439830();
   input += synapse0x9439858();
   input += synapse0x9439880();
   input += synapse0x94398a8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94395a0() {
   double input = input0x94395a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x94398d0() {
   double input = -0.194271;
   input += synapse0x9439ae8();
   input += synapse0x9439b10();
   input += synapse0x9439b38();
   input += synapse0x9439b60();
   input += synapse0x9439b88();
   input += synapse0x9439bb0();
   input += synapse0x9439bd8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x94398d0() {
   double input = input0x94398d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9439c00() {
   double input = 0.227073;
   input += synapse0x9439e18();
   input += synapse0x9439e40();
   input += synapse0x9439e68();
   input += synapse0x9439e90();
   input += synapse0x9439eb8();
   input += synapse0x9439ee0();
   input += synapse0x9439f08();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9439c00() {
   double input = input0x9439c00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9439f30() {
   double input = 0.0291255;
   input += synapse0x943a148();
   input += synapse0x943a170();
   input += synapse0x943a198();
   input += synapse0x943a1c0();
   input += synapse0x943a1e8();
   input += synapse0x943a210();
   input += synapse0x943a238();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9439f30() {
   double input = input0x9439f30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943a260() {
   double input = -0.449234;
   input += synapse0x943a478();
   input += synapse0x943a4a0();
   input += synapse0x943a4c8();
   input += synapse0x943a4f0();
   input += synapse0x943a518();
   input += synapse0x943a540();
   input += synapse0x943a568();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943a260() {
   double input = input0x943a260();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943a590() {
   double input = 0.352001;
   input += synapse0x943a7a8();
   input += synapse0x943a7d0();
   input += synapse0x943a7f8();
   input += synapse0x943a820();
   input += synapse0x943a848();
   input += synapse0x943a870();
   input += synapse0x943a898();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943a590() {
   double input = input0x943a590();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943a8c0() {
   double input = -0.292024;
   input += synapse0x943aad8();
   input += synapse0x943ab00();
   input += synapse0x943ab28();
   input += synapse0x943ab50();
   input += synapse0x943ab78();
   input += synapse0x943aba0();
   input += synapse0x943abc8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943a8c0() {
   double input = input0x943a8c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943abf0() {
   double input = -0.365754;
   input += synapse0x943ae08();
   input += synapse0x943ae30();
   input += synapse0x943ae58();
   input += synapse0x943ae80();
   input += synapse0x943aea8();
   input += synapse0x943aed0();
   input += synapse0x943aef8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943abf0() {
   double input = input0x943abf0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943af20() {
   double input = 0.325883;
   input += synapse0x943b138();
   input += synapse0x943b160();
   input += synapse0x943b188();
   input += synapse0x943b1b0();
   input += synapse0x943b1d8();
   input += synapse0x943b200();
   input += synapse0x943b228();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943af20() {
   double input = input0x943af20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943b250() {
   double input = -0.7122;
   input += synapse0x943b468();
   input += synapse0x943b490();
   input += synapse0x943b4b8();
   input += synapse0x943b4e0();
   input += synapse0x943b508();
   input += synapse0x943b530();
   input += synapse0x943b558();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943b250() {
   double input = input0x943b250();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943b580() {
   double input = 0.268288;
   input += synapse0x943b798();
   input += synapse0x943b7c0();
   input += synapse0x943b7e8();
   input += synapse0x943b810();
   input += synapse0x943b838();
   input += synapse0x943b860();
   input += synapse0x943b888();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943b580() {
   double input = input0x943b580();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943b8b0() {
   double input = 0.414313;
   input += synapse0x943bac8();
   input += synapse0x943baf0();
   input += synapse0x943bb18();
   input += synapse0x943bb40();
   input += synapse0x943bb68();
   input += synapse0x943bb90();
   input += synapse0x943bbb8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943b8b0() {
   double input = input0x943b8b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943bbe0() {
   double input = -0.302201;
   input += synapse0x943bdf8();
   input += synapse0x943be20();
   input += synapse0x943be48();
   input += synapse0x943be70();
   input += synapse0x943be98();
   input += synapse0x943bec0();
   input += synapse0x943bee8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943bbe0() {
   double input = input0x943bbe0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943bf10() {
   double input = -0.431944;
   input += synapse0x943c128();
   input += synapse0x943c150();
   input += synapse0x943c178();
   input += synapse0x943c1a0();
   input += synapse0x943c1c8();
   input += synapse0x943c1f0();
   input += synapse0x943c218();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943bf10() {
   double input = input0x943bf10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943c240() {
   double input = 0.49469;
   input += synapse0x943c458();
   input += synapse0x943c480();
   input += synapse0x943c4a8();
   input += synapse0x943c4d0();
   input += synapse0x943c4f8();
   input += synapse0x943c520();
   input += synapse0x943c548();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943c240() {
   double input = input0x943c240();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x943c570() {
   double input = 0.029147;
   input += synapse0x943c788();
   input += synapse0x943c7b0();
   input += synapse0x943c7d8();
   input += synapse0x943c800();
   input += synapse0x943c828();
   input += synapse0x943c850();
   input += synapse0x943c878();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x943c570() {
   double input = input0x943c570();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::input0x9424d60() {
   double input = 0.0104792;
   input += synapse0x943c978();
   input += synapse0x943c9a0();
   input += synapse0x943c9c8();
   input += synapse0x943c9f0();
   input += synapse0x943ca18();
   input += synapse0x943ca40();
   input += synapse0x943ca68();
   input += synapse0x943ca90();
   input += synapse0x943cab8();
   input += synapse0x943cae0();
   input += synapse0x943cb08();
   input += synapse0x943cb30();
   input += synapse0x943cb58();
   input += synapse0x943cb80();
   input += synapse0x943cba8();
   input += synapse0x943cbd0();
   input += synapse0x943cc80();
   input += synapse0x943cca8();
   input += synapse0x943ccd0();
   input += synapse0x943ccf8();
   input += synapse0x943cd20();
   input += synapse0x943cd48();
   input += synapse0x943cd70();
   input += synapse0x943cd98();
   input += synapse0x943cdc0();
   input += synapse0x943cde8();
   input += synapse0x943ce10();
   input += synapse0x943ce38();
   input += synapse0x943ce60();
   input += synapse0x943ce88();
   input += synapse0x943ceb0();
   input += synapse0x943ced8();
   input += synapse0x943cbf8();
   input += synapse0x943cc20();
   input += synapse0x943cc48();
   input += synapse0x943d008();
   input += synapse0x943d030();
   input += synapse0x943d058();
   input += synapse0x943d080();
   input += synapse0x943d0a8();
   input += synapse0x943d0d0();
   input += synapse0x943d0f8();
   input += synapse0x943d120();
   input += synapse0x943d148();
   input += synapse0x943d170();
   input += synapse0x943d198();
   input += synapse0x943d1c0();
   input += synapse0x943d1e8();
   input += synapse0x943d210();
   input += synapse0x943d238();
   input += synapse0x943d260();
   input += synapse0x943d288();
   input += synapse0x943d2b0();
   input += synapse0x943d2d8();
   input += synapse0x943d300();
   input += synapse0x943d328();
   input += synapse0x943d350();
   input += synapse0x943d378();
   input += synapse0x943d3a0();
   input += synapse0x943d3c8();
   input += synapse0x943d3f0();
   input += synapse0x943d418();
   input += synapse0x943d440();
   input += synapse0x943d468();
   input += synapse0x943c8a0();
   input += synapse0x943cf00();
   input += synapse0x943cf28();
   input += synapse0x943cf50();
   input += synapse0x943cf78();
   input += synapse0x943cfa0();
   input += synapse0x943cfc8();
   input += synapse0x943d698();
   input += synapse0x943d6c0();
   input += synapse0x943d6e8();
   input += synapse0x943d710();
   input += synapse0x943d738();
   input += synapse0x943d760();
   input += synapse0x943d788();
   input += synapse0x943d7b0();
   input += synapse0x943d7d8();
   input += synapse0x943d800();
   input += synapse0x943d828();
   input += synapse0x943d850();
   input += synapse0x943d878();
   input += synapse0x943d8a0();
   input += synapse0x943d8c8();
   input += synapse0x943d8f0();
   input += synapse0x943d918();
   input += synapse0x943d940();
   input += synapse0x943d968();
   input += synapse0x943d990();
   input += synapse0x943d9b8();
   input += synapse0x943d9e0();
   input += synapse0x943da08();
   input += synapse0x943da30();
   input += synapse0x943da58();
   input += synapse0x943da80();
   input += synapse0x943daa8();
   input += synapse0x943dad0();
   input += synapse0x943daf8();
   input += synapse0x943db20();
   input += synapse0x943db48();
   input += synapse0x943db70();
   input += synapse0x943db98();
   input += synapse0x943dbc0();
   input += synapse0x943dbe8();
   input += synapse0x943dc10();
   input += synapse0x943dc38();
   input += synapse0x943dc60();
   input += synapse0x943dc88();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaPhi::neuron0x9424d60() {
   double input = input0x9424d60();
   return (input * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x8d67518() {
   return (neuron0x9415bb0()*-0.0546258);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x8d44b90() {
   return (neuron0x9415de8()*-0.017532);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x8d44bd8() {
   return (neuron0x9416010()*-0.384574);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x8d44ca8() {
   return (neuron0x94245a8()*-0.468199);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425040() {
   return (neuron0x9424760()*-0.0776274);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425068() {
   return (neuron0x9424960()*0.0122759);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425090() {
   return (neuron0x9424b60()*-0.0175955);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94252b8() {
   return (neuron0x9415bb0()*-0.37765);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94252e0() {
   return (neuron0x9415de8()*-0.398334);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425308() {
   return (neuron0x9416010()*0.0829166);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425330() {
   return (neuron0x94245a8()*-0.232008);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425358() {
   return (neuron0x9424760()*0.179025);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425380() {
   return (neuron0x9424960()*0.0128106);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94253a8() {
   return (neuron0x9424b60()*0.283015);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94255d0() {
   return (neuron0x9415bb0()*-0.385644);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94255f8() {
   return (neuron0x9415de8()*0.325336);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425620() {
   return (neuron0x9416010()*0.31253);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94256d0() {
   return (neuron0x94245a8()*0.119535);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94256f8() {
   return (neuron0x9424760()*0.14397);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425720() {
   return (neuron0x9424960()*0.16277);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425748() {
   return (neuron0x9424b60()*-0.250851);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425928() {
   return (neuron0x9415bb0()*0.415116);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425950() {
   return (neuron0x9415de8()*-0.168133);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425978() {
   return (neuron0x9416010()*0.320636);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94259a0() {
   return (neuron0x94245a8()*-0.296151);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94259c8() {
   return (neuron0x9424760()*0.112094);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94259f0() {
   return (neuron0x9424960()*-0.115537);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425a18() {
   return (neuron0x9424b60()*0.133523);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425c40() {
   return (neuron0x9415bb0()*0.471422);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425c68() {
   return (neuron0x9415de8()*-0.114851);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425c90() {
   return (neuron0x9416010()*0.0751882);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425cb8() {
   return (neuron0x94245a8()*-0.0316169);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425ce0() {
   return (neuron0x9424760()*-0.106007);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425648() {
   return (neuron0x9424960()*0.375984);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425670() {
   return (neuron0x9424b60()*-0.119875);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425698() {
   return (neuron0x9415bb0()*0.152683);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426010() {
   return (neuron0x9415de8()*0.24346);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426038() {
   return (neuron0x9416010()*0.238051);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426060() {
   return (neuron0x94245a8()*-0.522799);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426088() {
   return (neuron0x9424760()*-0.365719);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94260b0() {
   return (neuron0x9424960()*0.453947);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94260d8() {
   return (neuron0x9424b60()*-0.448022);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426300() {
   return (neuron0x9415bb0()*0.453126);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426328() {
   return (neuron0x9415de8()*0.189566);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426350() {
   return (neuron0x9416010()*-0.24772);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426378() {
   return (neuron0x94245a8()*0.328342);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94263a0() {
   return (neuron0x9424760()*0.0674998);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94263c8() {
   return (neuron0x9424960()*0.273828);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94263f0() {
   return (neuron0x9424b60()*-0.0513317);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426618() {
   return (neuron0x9415bb0()*0.246064);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426640() {
   return (neuron0x9415de8()*-0.0427118);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426668() {
   return (neuron0x9416010()*0.264729);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426690() {
   return (neuron0x94245a8()*-0.182954);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94266b8() {
   return (neuron0x9424760()*-0.328731);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94266e0() {
   return (neuron0x9424960()*0.21766);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426708() {
   return (neuron0x9424b60()*0.39364);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426948() {
   return (neuron0x9415bb0()*-0.148728);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426970() {
   return (neuron0x9415de8()*0.272879);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426998() {
   return (neuron0x9416010()*0.292901);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94269c0() {
   return (neuron0x94245a8()*-0.353562);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94269e8() {
   return (neuron0x9424760()*0.475037);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426a10() {
   return (neuron0x9424960()*-0.147381);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426a38() {
   return (neuron0x9424b60()*-0.274022);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426d00() {
   return (neuron0x9415bb0()*-0.355689);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426d28() {
   return (neuron0x9415de8()*-0.446601);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x8d44ae8() {
   return (neuron0x9416010()*-0.2143);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x8d44cd0() {
   return (neuron0x94245a8()*-0.383174);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x8d446a0() {
   return (neuron0x9424760()*-0.370282);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425d08() {
   return (neuron0x9424960()*-0.288936);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425d30() {
   return (neuron0x9424b60()*-0.0305872);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9425de8() {
   return (neuron0x9415bb0()*0.315201);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94270c8() {
   return (neuron0x9415de8()*0.210655);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94270f0() {
   return (neuron0x9416010()*0.154881);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427118() {
   return (neuron0x94245a8()*-0.266805);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427140() {
   return (neuron0x9424760()*0.044884);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427168() {
   return (neuron0x9424960()*-0.418421);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427190() {
   return (neuron0x9424b60()*0.342857);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94273b8() {
   return (neuron0x9415bb0()*0.306843);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94273e0() {
   return (neuron0x9415de8()*0.0312341);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427408() {
   return (neuron0x9416010()*0.0650063);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427430() {
   return (neuron0x94245a8()*0.168017);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427458() {
   return (neuron0x9424760()*0.15587);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427480() {
   return (neuron0x9424960()*0.18674);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94274a8() {
   return (neuron0x9424b60()*0.125146);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94276d0() {
   return (neuron0x9415bb0()*0.0224872);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94276f8() {
   return (neuron0x9415de8()*0.072937);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427720() {
   return (neuron0x9416010()*0.441125);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427748() {
   return (neuron0x94245a8()*-0.146894);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427770() {
   return (neuron0x9424760()*-0.215442);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427798() {
   return (neuron0x9424960()*0.237374);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94277c0() {
   return (neuron0x9424b60()*0.325058);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94279e8() {
   return (neuron0x9415bb0()*-0.411493);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427a10() {
   return (neuron0x9415de8()*-0.0964793);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427a38() {
   return (neuron0x9416010()*0.506293);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427a60() {
   return (neuron0x94245a8()*0.22277);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427a88() {
   return (neuron0x9424760()*-0.428411);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427ab0() {
   return (neuron0x9424960()*0.0759832);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427ad8() {
   return (neuron0x9424b60()*-0.461964);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427d00() {
   return (neuron0x9415bb0()*0.267342);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427d28() {
   return (neuron0x9415de8()*-0.31352);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427d50() {
   return (neuron0x9416010()*0.250193);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427d78() {
   return (neuron0x94245a8()*-0.404886);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427da0() {
   return (neuron0x9424760()*-0.44015);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427dc8() {
   return (neuron0x9424960()*0.369763);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9427df0() {
   return (neuron0x9424b60()*0.17535);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428018() {
   return (neuron0x9415bb0()*0.2345);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94280c8() {
   return (neuron0x9415de8()*0.477062);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428178() {
   return (neuron0x9416010()*0.193038);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428228() {
   return (neuron0x94245a8()*-0.126538);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94282d8() {
   return (neuron0x9424760()*0.153585);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428388() {
   return (neuron0x9424960()*0.0118677);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428438() {
   return (neuron0x9424b60()*-0.00291142);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428610() {
   return (neuron0x9415bb0()*0.155168);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428638() {
   return (neuron0x9415de8()*0.403404);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428660() {
   return (neuron0x9416010()*-0.508458);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428688() {
   return (neuron0x94245a8()*0.304961);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94286b0() {
   return (neuron0x9424760()*-0.249485);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94286d8() {
   return (neuron0x9424960()*0.21702);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428700() {
   return (neuron0x9424b60()*0.35041);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428850() {
   return (neuron0x9415bb0()*-0.22347);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428878() {
   return (neuron0x9415de8()*0.131094);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94288a0() {
   return (neuron0x9416010()*0.218096);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94288c8() {
   return (neuron0x94245a8()*-0.28644);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94288f0() {
   return (neuron0x9424760()*-0.22147);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428918() {
   return (neuron0x9424960()*-0.238632);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428940() {
   return (neuron0x9424b60()*0.391744);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428b20() {
   return (neuron0x9415bb0()*0.348935);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428b48() {
   return (neuron0x9415de8()*-0.362134);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428b70() {
   return (neuron0x9416010()*-0.191891);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426d50() {
   return (neuron0x94245a8()*0.0690606);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426d78() {
   return (neuron0x9424760()*-0.611574);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426da0() {
   return (neuron0x9424960()*-0.00795056);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426dc8() {
   return (neuron0x9424b60()*-0.306845);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426f30() {
   return (neuron0x9415bb0()*0.0203074);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429078() {
   return (neuron0x9415de8()*-0.423848);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94290a0() {
   return (neuron0x9416010()*0.027249);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94290c8() {
   return (neuron0x94245a8()*-0.011379);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94290f0() {
   return (neuron0x9424760()*-0.345572);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429118() {
   return (neuron0x9424960()*-0.347107);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429140() {
   return (neuron0x9424b60()*-0.441328);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429380() {
   return (neuron0x9415bb0()*0.273076);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94293a8() {
   return (neuron0x9415de8()*-0.276032);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94293d0() {
   return (neuron0x9416010()*-0.428601);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94293f8() {
   return (neuron0x94245a8()*-0.0512862);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429420() {
   return (neuron0x9424760()*0.0392767);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429448() {
   return (neuron0x9424960()*-0.25715);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429470() {
   return (neuron0x9424b60()*0.124539);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94296b0() {
   return (neuron0x9415bb0()*0.362753);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94296d8() {
   return (neuron0x9415de8()*-0.113372);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429700() {
   return (neuron0x9416010()*0.13933);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429728() {
   return (neuron0x94245a8()*-0.256045);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429750() {
   return (neuron0x9424760()*-0.244656);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429778() {
   return (neuron0x9424960()*0.268473);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94297a0() {
   return (neuron0x9424b60()*0.233651);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94299e0() {
   return (neuron0x9415bb0()*0.406815);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429a08() {
   return (neuron0x9415de8()*0.437598);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429a30() {
   return (neuron0x9416010()*-0.212721);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429a58() {
   return (neuron0x94245a8()*-0.264984);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429a80() {
   return (neuron0x9424760()*0.279691);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429aa8() {
   return (neuron0x9424960()*-0.265815);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429ad0() {
   return (neuron0x9424b60()*-0.291453);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429d10() {
   return (neuron0x9415bb0()*0.0618745);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429d38() {
   return (neuron0x9415de8()*0.117174);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429d60() {
   return (neuron0x9416010()*0.237022);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429d88() {
   return (neuron0x94245a8()*0.01646);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429db0() {
   return (neuron0x9424760()*0.396504);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429dd8() {
   return (neuron0x9424960()*-0.159432);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9429e00() {
   return (neuron0x9424b60()*0.258589);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a040() {
   return (neuron0x9415bb0()*0.282699);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a068() {
   return (neuron0x9415de8()*0.2736);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a090() {
   return (neuron0x9416010()*0.289717);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a0b8() {
   return (neuron0x94245a8()*-0.353597);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a0e0() {
   return (neuron0x9424760()*-0.102861);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a108() {
   return (neuron0x9424960()*0.398958);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a130() {
   return (neuron0x9424b60()*0.308314);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426c78() {
   return (neuron0x9415bb0()*0.418777);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426ca0() {
   return (neuron0x9415de8()*0.0348823);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9426cc8() {
   return (neuron0x9416010()*-0.291985);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a478() {
   return (neuron0x94245a8()*0.132514);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a4a0() {
   return (neuron0x9424760()*0.360664);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a4c8() {
   return (neuron0x9424960()*-0.129281);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a4f0() {
   return (neuron0x9424b60()*0.287833);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a730() {
   return (neuron0x9415bb0()*0.572005);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a758() {
   return (neuron0x9415de8()*-0.28589);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a780() {
   return (neuron0x9416010()*-0.330063);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a7a8() {
   return (neuron0x94245a8()*0.0773508);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a7d0() {
   return (neuron0x9424760()*-0.527771);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a7f8() {
   return (neuron0x9424960()*0.134433);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a820() {
   return (neuron0x9424b60()*0.278765);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942aa60() {
   return (neuron0x9415bb0()*-0.17136);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942aa88() {
   return (neuron0x9415de8()*-0.14475);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942aab0() {
   return (neuron0x9416010()*0.237263);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942aad8() {
   return (neuron0x94245a8()*0.034166);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ab00() {
   return (neuron0x9424760()*-0.211057);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ab28() {
   return (neuron0x9424960()*-0.184863);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ab50() {
   return (neuron0x9424b60()*-0.32782);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ad90() {
   return (neuron0x9415bb0()*0.362591);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942adb8() {
   return (neuron0x9415de8()*-0.0922116);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ade0() {
   return (neuron0x9416010()*0.18722);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ae08() {
   return (neuron0x94245a8()*-0.460392);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ae30() {
   return (neuron0x9424760()*-0.480004);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ae58() {
   return (neuron0x9424960()*0.438843);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ae80() {
   return (neuron0x9424b60()*0.138107);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b0c0() {
   return (neuron0x9415bb0()*-0.120344);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b0e8() {
   return (neuron0x9415de8()*-0.048912);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b110() {
   return (neuron0x9416010()*-0.0786896);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b138() {
   return (neuron0x94245a8()*0.358437);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b160() {
   return (neuron0x9424760()*0.218704);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b188() {
   return (neuron0x9424960()*-0.133936);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b1b0() {
   return (neuron0x9424b60()*-0.336987);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b3f0() {
   return (neuron0x9415bb0()*0.29431);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b418() {
   return (neuron0x9415de8()*-0.0759114);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b440() {
   return (neuron0x9416010()*0.238558);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b468() {
   return (neuron0x94245a8()*0.467264);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b490() {
   return (neuron0x9424760()*-0.314228);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b4b8() {
   return (neuron0x9424960()*-0.260513);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b4e0() {
   return (neuron0x9424b60()*0.145044);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b720() {
   return (neuron0x9415bb0()*-0.147355);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428040() {
   return (neuron0x9415de8()*0.309979);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428068() {
   return (neuron0x9416010()*0.246142);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428090() {
   return (neuron0x94245a8()*0.25627);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94280f0() {
   return (neuron0x9424760()*-0.246536);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428118() {
   return (neuron0x9424960()*0.141753);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428140() {
   return (neuron0x9424b60()*-0.436407);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428200() {
   return (neuron0x9415bb0()*-0.153147);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428348() {
   return (neuron0x9415de8()*0.308594);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428298() {
   return (neuron0x9416010()*-0.00524807);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94283b0() {
   return (neuron0x94245a8()*0.282418);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94283d8() {
   return (neuron0x9424760()*0.301498);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428400() {
   return (neuron0x9424960()*-0.028553);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428460() {
   return (neuron0x9424b60()*0.397501);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c160() {
   return (neuron0x9415bb0()*0.318893);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c188() {
   return (neuron0x9415de8()*0.294638);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c1b0() {
   return (neuron0x9416010()*-0.0525945);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c1d8() {
   return (neuron0x94245a8()*0.113236);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c200() {
   return (neuron0x9424760()*0.526501);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c228() {
   return (neuron0x9424960()*-0.158826);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c250() {
   return (neuron0x9424b60()*0.387942);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c478() {
   return (neuron0x9415bb0()*-0.395038);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c4a0() {
   return (neuron0x9415de8()*-0.173577);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c4c8() {
   return (neuron0x9416010()*0.242839);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c4f0() {
   return (neuron0x94245a8()*-0.31531);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c518() {
   return (neuron0x9424760()*0.212027);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c540() {
   return (neuron0x9424960()*0.443514);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c568() {
   return (neuron0x9424b60()*-0.0783549);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c790() {
   return (neuron0x9415bb0()*0.205263);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c7b8() {
   return (neuron0x9415de8()*-0.0711261);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c7e0() {
   return (neuron0x9416010()*0.202993);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c808() {
   return (neuron0x94245a8()*0.433723);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c830() {
   return (neuron0x9424760()*0.266345);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c858() {
   return (neuron0x9424960()*-0.150219);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942c880() {
   return (neuron0x9424b60()*0.269429);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cac0() {
   return (neuron0x9415bb0()*-0.394138);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cae8() {
   return (neuron0x9415de8()*-0.117149);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cb10() {
   return (neuron0x9416010()*0.324564);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cb38() {
   return (neuron0x94245a8()*-0.105645);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cb60() {
   return (neuron0x9424760()*-0.0258686);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428b98() {
   return (neuron0x9424960()*0.42315);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428bc0() {
   return (neuron0x9424b60()*-0.0822082);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428e00() {
   return (neuron0x9415bb0()*-0.365528);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428e28() {
   return (neuron0x9415de8()*0.348336);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428e50() {
   return (neuron0x9416010()*0.331027);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428e78() {
   return (neuron0x94245a8()*0.5215);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428ea0() {
   return (neuron0x9424760()*-0.309713);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428ec8() {
   return (neuron0x9424960()*0.0244926);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428ef0() {
   return (neuron0x9424b60()*-0.114887);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9428f78() {
   return (neuron0x9415bb0()*0.174034);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d548() {
   return (neuron0x9415de8()*0.449934);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d570() {
   return (neuron0x9416010()*0.394886);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d598() {
   return (neuron0x94245a8()*0.303864);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d5c0() {
   return (neuron0x9424760()*0.503196);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d5e8() {
   return (neuron0x9424960()*-0.0744587);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d610() {
   return (neuron0x9424b60()*-0.196547);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d850() {
   return (neuron0x9415bb0()*-0.0102102);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d878() {
   return (neuron0x9415de8()*0.210336);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d8a0() {
   return (neuron0x9416010()*0.0925271);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d8c8() {
   return (neuron0x94245a8()*-0.402788);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d8f0() {
   return (neuron0x9424760()*0.656579);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d918() {
   return (neuron0x9424960()*-0.0872323);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d940() {
   return (neuron0x9424b60()*0.250544);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942db80() {
   return (neuron0x9415bb0()*-0.314372);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942dba8() {
   return (neuron0x9415de8()*0.332714);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942dbd0() {
   return (neuron0x9416010()*0.425655);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942dbf8() {
   return (neuron0x94245a8()*0.36215);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942dc20() {
   return (neuron0x9424760()*0.273164);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942dc48() {
   return (neuron0x9424960()*0.00569438);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942dc70() {
   return (neuron0x9424b60()*-0.234583);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942deb0() {
   return (neuron0x9415bb0()*0.18758);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ded8() {
   return (neuron0x9415de8()*-0.21837);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942df00() {
   return (neuron0x9416010()*-0.110119);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942df28() {
   return (neuron0x94245a8()*-0.169542);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942df50() {
   return (neuron0x9424760()*-0.577714);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942df78() {
   return (neuron0x9424960()*0.0107254);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942dfa0() {
   return (neuron0x9424b60()*-0.280369);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e1e0() {
   return (neuron0x9415bb0()*0.0240418);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e208() {
   return (neuron0x9415de8()*0.247578);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e230() {
   return (neuron0x9416010()*0.525998);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e258() {
   return (neuron0x94245a8()*0.449834);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e280() {
   return (neuron0x9424760()*0.466841);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e2a8() {
   return (neuron0x9424960()*-0.253535);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e2d0() {
   return (neuron0x9424b60()*-0.351904);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e510() {
   return (neuron0x9415bb0()*0.302514);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e538() {
   return (neuron0x9415de8()*-0.227872);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e560() {
   return (neuron0x9416010()*0.072253);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e588() {
   return (neuron0x94245a8()*-0.253119);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e5b0() {
   return (neuron0x9424760()*-0.0586907);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e5d8() {
   return (neuron0x9424960()*-0.314266);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e600() {
   return (neuron0x9424b60()*0.266972);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e840() {
   return (neuron0x9415bb0()*-0.186677);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e868() {
   return (neuron0x9415de8()*-0.146791);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e890() {
   return (neuron0x9416010()*0.0966903);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e8b8() {
   return (neuron0x94245a8()*-0.299371);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e8e0() {
   return (neuron0x9424760()*0.158825);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e908() {
   return (neuron0x9424960()*0.229429);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942e930() {
   return (neuron0x9424b60()*-0.392285);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942eb70() {
   return (neuron0x9415bb0()*-0.413172);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942eb98() {
   return (neuron0x9415de8()*-0.11058);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ebc0() {
   return (neuron0x9416010()*-0.31809);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ebe8() {
   return (neuron0x94245a8()*-0.122549);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ec10() {
   return (neuron0x9424760()*0.401865);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ec38() {
   return (neuron0x9424960()*-0.472241);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ec60() {
   return (neuron0x9424b60()*-0.388792);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942eea0() {
   return (neuron0x9415bb0()*0.332074);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942eec8() {
   return (neuron0x9415de8()*-0.462301);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942eef0() {
   return (neuron0x9416010()*-0.283796);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ef18() {
   return (neuron0x94245a8()*0.267221);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ef40() {
   return (neuron0x9424760()*0.270433);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ef68() {
   return (neuron0x9424960()*-0.322166);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ef90() {
   return (neuron0x9424b60()*-0.346856);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f1d0() {
   return (neuron0x9415bb0()*0.416838);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f1f8() {
   return (neuron0x9415de8()*-0.504193);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f220() {
   return (neuron0x9416010()*0.457651);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f248() {
   return (neuron0x94245a8()*0.237535);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f270() {
   return (neuron0x9424760()*-0.28518);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f298() {
   return (neuron0x9424960()*-0.382705);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f2c0() {
   return (neuron0x9424b60()*0.379904);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f500() {
   return (neuron0x9415bb0()*-0.0806225);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f528() {
   return (neuron0x9415de8()*-0.0750828);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f550() {
   return (neuron0x9416010()*-0.00708142);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f578() {
   return (neuron0x94245a8()*0.485639);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f5a0() {
   return (neuron0x9424760()*-0.578737);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f5c8() {
   return (neuron0x9424960()*0.0369557);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f5f0() {
   return (neuron0x9424b60()*0.0796246);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f830() {
   return (neuron0x9415bb0()*-0.204946);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f858() {
   return (neuron0x9415de8()*0.158244);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f880() {
   return (neuron0x9416010()*0.33625);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f8a8() {
   return (neuron0x94245a8()*-0.227942);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f8d0() {
   return (neuron0x9424760()*0.374949);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f8f8() {
   return (neuron0x9424960()*-0.305909);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942f920() {
   return (neuron0x9424b60()*0.305358);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fb60() {
   return (neuron0x9415bb0()*0.290805);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fb88() {
   return (neuron0x9415de8()*0.116493);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fbb0() {
   return (neuron0x9416010()*-0.23311);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fbd8() {
   return (neuron0x94245a8()*-0.204172);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fc00() {
   return (neuron0x9424760()*0.602268);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fc28() {
   return (neuron0x9424960()*0.18573);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fc50() {
   return (neuron0x9424b60()*-0.243404);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fe90() {
   return (neuron0x9415bb0()*0.382821);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942feb8() {
   return (neuron0x9415de8()*0.357172);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942fee0() {
   return (neuron0x9416010()*-0.142872);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ff08() {
   return (neuron0x94245a8()*-0.156128);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ff30() {
   return (neuron0x9424760()*-0.0676517);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ff58() {
   return (neuron0x9424960()*-0.296358);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ff80() {
   return (neuron0x9424b60()*-0.415373);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94301c0() {
   return (neuron0x9415bb0()*-0.0715003);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94301e8() {
   return (neuron0x9415de8()*0.154015);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430210() {
   return (neuron0x9416010()*0.411759);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430238() {
   return (neuron0x94245a8()*-0.361584);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430260() {
   return (neuron0x9424760()*0.00121263);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430288() {
   return (neuron0x9424960()*0.170489);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94302b0() {
   return (neuron0x9424b60()*-0.128507);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94304f0() {
   return (neuron0x9415bb0()*0.0422261);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430518() {
   return (neuron0x9415de8()*0.180895);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430540() {
   return (neuron0x9416010()*0.00731981);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430568() {
   return (neuron0x94245a8()*0.0222048);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430590() {
   return (neuron0x9424760()*-0.195253);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94305b8() {
   return (neuron0x9424960()*-0.392059);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94305e0() {
   return (neuron0x9424b60()*-0.319178);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430820() {
   return (neuron0x9415bb0()*-0.206175);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430848() {
   return (neuron0x9415de8()*0.111691);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430870() {
   return (neuron0x9416010()*0.245598);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430898() {
   return (neuron0x94245a8()*0.170113);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94308c0() {
   return (neuron0x9424760()*-0.293616);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94308e8() {
   return (neuron0x9424960()*0.384609);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430910() {
   return (neuron0x9424b60()*-0.340213);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430b50() {
   return (neuron0x9415bb0()*0.0664628);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430b78() {
   return (neuron0x9415de8()*0.403706);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430ba0() {
   return (neuron0x9416010()*0.173921);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430bc8() {
   return (neuron0x94245a8()*0.22123);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430bf0() {
   return (neuron0x9424760()*0.171071);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430c18() {
   return (neuron0x9424960()*0.144257);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430c40() {
   return (neuron0x9424b60()*0.438806);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430e80() {
   return (neuron0x9415bb0()*0.417523);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430ea8() {
   return (neuron0x9415de8()*-0.186356);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430ed0() {
   return (neuron0x9416010()*-0.0263114);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430ef8() {
   return (neuron0x94245a8()*0.363259);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430f20() {
   return (neuron0x9424760()*-0.27403);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430f48() {
   return (neuron0x9424960()*0.0136865);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9430f70() {
   return (neuron0x9424b60()*-0.194451);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a370() {
   return (neuron0x9415bb0()*0.133893);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a398() {
   return (neuron0x9415de8()*0.0177876);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a3c0() {
   return (neuron0x9416010()*-0.136467);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a3e8() {
   return (neuron0x94245a8()*-0.434645);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a410() {
   return (neuron0x9424760()*0.280418);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942a438() {
   return (neuron0x9424960()*-0.439894);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94313b8() {
   return (neuron0x9424b60()*0.320183);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94315e0() {
   return (neuron0x9415bb0()*-0.0372126);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431608() {
   return (neuron0x9415de8()*-0.246552);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431630() {
   return (neuron0x9416010()*-0.384866);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431658() {
   return (neuron0x94245a8()*0.128277);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431680() {
   return (neuron0x9424760()*-0.103281);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94316a8() {
   return (neuron0x9424960()*0.339675);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94316d0() {
   return (neuron0x9424b60()*-0.470799);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431910() {
   return (neuron0x9415bb0()*-0.358729);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431938() {
   return (neuron0x9415de8()*0.101137);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431960() {
   return (neuron0x9416010()*-0.12821);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431988() {
   return (neuron0x94245a8()*0.33521);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94319b0() {
   return (neuron0x9424760()*0.153634);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94319d8() {
   return (neuron0x9424960()*-0.0268871);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431a00() {
   return (neuron0x9424b60()*-0.0318276);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431c40() {
   return (neuron0x9415bb0()*0.276394);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431c68() {
   return (neuron0x9415de8()*-0.344515);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431c90() {
   return (neuron0x9416010()*0.159052);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431cb8() {
   return (neuron0x94245a8()*0.375366);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431ce0() {
   return (neuron0x9424760()*0.537352);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431d08() {
   return (neuron0x9424960()*0.0270051);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431d30() {
   return (neuron0x9424b60()*0.481477);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431f70() {
   return (neuron0x9415bb0()*0.225227);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431f98() {
   return (neuron0x9415de8()*-0.279765);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431fc0() {
   return (neuron0x9416010()*0.276588);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9431fe8() {
   return (neuron0x94245a8()*0.00273636);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9432010() {
   return (neuron0x9424760()*0.0266855);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9432038() {
   return (neuron0x9424960()*-0.417075);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9432060() {
   return (neuron0x9424b60()*0.450643);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94322a0() {
   return (neuron0x9415bb0()*-0.15224);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94322c8() {
   return (neuron0x9415de8()*-0.184878);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94322f0() {
   return (neuron0x9416010()*-0.0721036);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9432318() {
   return (neuron0x94245a8()*0.179002);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9432340() {
   return (neuron0x9424760()*-0.922875);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9432368() {
   return (neuron0x9424960()*0.163263);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9432390() {
   return (neuron0x9424b60()*0.200224);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94325d0() {
   return (neuron0x9415bb0()*-0.285847);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b748() {
   return (neuron0x9415de8()*-0.478677);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b770() {
   return (neuron0x9416010()*0.313184);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b798() {
   return (neuron0x94245a8()*-0.092433);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b9c8() {
   return (neuron0x9424760()*-0.208906);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942b9f0() {
   return (neuron0x9424960()*-0.178103);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942bc20() {
   return (neuron0x9424b60()*0.0453294);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433020() {
   return (neuron0x9415bb0()*0.0859158);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433048() {
   return (neuron0x9415de8()*-0.0240987);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433070() {
   return (neuron0x9416010()*0.128637);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433098() {
   return (neuron0x94245a8()*0.382205);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94330c0() {
   return (neuron0x9424760()*-0.891314);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94330e8() {
   return (neuron0x9424960()*-0.197716);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433110() {
   return (neuron0x9424b60()*-0.10029);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433338() {
   return (neuron0x9415bb0()*0.0783661);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433360() {
   return (neuron0x9415de8()*0.293016);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433388() {
   return (neuron0x9416010()*0.187333);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94333b0() {
   return (neuron0x94245a8()*-0.0167796);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94333d8() {
   return (neuron0x9424760()*-0.131036);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433400() {
   return (neuron0x9424960()*0.136788);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433428() {
   return (neuron0x9424b60()*0.128693);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433668() {
   return (neuron0x9415bb0()*0.382708);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433690() {
   return (neuron0x9415de8()*0.00110147);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94336b8() {
   return (neuron0x9416010()*-0.0167291);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94336e0() {
   return (neuron0x94245a8()*-0.460815);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433708() {
   return (neuron0x9424760()*0.373793);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433730() {
   return (neuron0x9424960()*0.304244);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433758() {
   return (neuron0x9424b60()*0.379596);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433998() {
   return (neuron0x9415bb0()*-0.36639);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94339c0() {
   return (neuron0x9415de8()*0.248318);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94339e8() {
   return (neuron0x9416010()*0.209547);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433a10() {
   return (neuron0x94245a8()*0.220962);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433a38() {
   return (neuron0x9424760()*-0.00652713);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433a60() {
   return (neuron0x9424960()*0.389411);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433a88() {
   return (neuron0x9424b60()*0.0788615);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433cc8() {
   return (neuron0x9415bb0()*0.059092);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433cf0() {
   return (neuron0x9415de8()*0.188387);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433d18() {
   return (neuron0x9416010()*0.291658);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433d40() {
   return (neuron0x94245a8()*-0.345608);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433d68() {
   return (neuron0x9424760()*-0.573695);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433d90() {
   return (neuron0x9424960()*0.0848957);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433db8() {
   return (neuron0x9424b60()*-0.374542);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9433ff8() {
   return (neuron0x9415bb0()*0.0302249);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434020() {
   return (neuron0x9415de8()*0.0562735);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434048() {
   return (neuron0x9416010()*0.344151);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434070() {
   return (neuron0x94245a8()*0.031742);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434098() {
   return (neuron0x9424760()*0.180588);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94340c0() {
   return (neuron0x9424960()*-0.244639);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94340e8() {
   return (neuron0x9424b60()*-0.348573);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434328() {
   return (neuron0x9415bb0()*0.466026);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434350() {
   return (neuron0x9415de8()*0.149952);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434378() {
   return (neuron0x9416010()*-0.381805);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94343a0() {
   return (neuron0x94245a8()*-0.202262);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94343c8() {
   return (neuron0x9424760()*-0.377555);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94343f0() {
   return (neuron0x9424960()*-0.299857);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434418() {
   return (neuron0x9424b60()*-0.384546);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434658() {
   return (neuron0x9415bb0()*0.0777229);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434680() {
   return (neuron0x9415de8()*-0.484063);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94346a8() {
   return (neuron0x9416010()*0.267859);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94346d0() {
   return (neuron0x94245a8()*-0.244577);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94346f8() {
   return (neuron0x9424760()*0.275982);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434720() {
   return (neuron0x9424960()*-0.2645);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434748() {
   return (neuron0x9424b60()*0.275809);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434988() {
   return (neuron0x9415bb0()*-0.50888);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94349b0() {
   return (neuron0x9415de8()*0.150699);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94349d8() {
   return (neuron0x9416010()*0.395179);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434a00() {
   return (neuron0x94245a8()*-0.132011);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434a28() {
   return (neuron0x9424760()*-0.0331565);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434a50() {
   return (neuron0x9424960()*-0.182051);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434a78() {
   return (neuron0x9424b60()*-0.148121);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434cb8() {
   return (neuron0x9415bb0()*-0.184439);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9434ce0() {
   return (neuron0x9415de8()*0.113816);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cb88() {
   return (neuron0x9416010()*0.35038);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cbb0() {
   return (neuron0x94245a8()*-0.41014);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cbd8() {
   return (neuron0x9424760()*-0.384914);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cc00() {
   return (neuron0x9424960()*0.462634);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cc28() {
   return (neuron0x9424b60()*-0.202152);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ce68() {
   return (neuron0x9415bb0()*0.325077);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ce90() {
   return (neuron0x9415de8()*-0.1342);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942ceb8() {
   return (neuron0x9416010()*-0.175417);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cee0() {
   return (neuron0x94245a8()*0.241124);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cf08() {
   return (neuron0x9424760()*-0.58039);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cf30() {
   return (neuron0x9424960()*-0.113513);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942cf58() {
   return (neuron0x9424b60()*0.0362459);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d198() {
   return (neuron0x9415bb0()*-0.427918);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d1c0() {
   return (neuron0x9415de8()*-0.306211);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d1e8() {
   return (neuron0x9416010()*0.153756);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d210() {
   return (neuron0x94245a8()*-0.343544);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d238() {
   return (neuron0x9424760()*0.083001);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d260() {
   return (neuron0x9424960()*-0.278451);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d288() {
   return (neuron0x9424b60()*-0.226919);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x942d358() {
   return (neuron0x9415bb0()*0.0243349);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9435e80() {
   return (neuron0x9415de8()*-0.0586536);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9435ea8() {
   return (neuron0x9416010()*-0.189768);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9435ed0() {
   return (neuron0x94245a8()*-0.164834);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9435ef8() {
   return (neuron0x9424760()*0.202035);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9435f20() {
   return (neuron0x9424960()*-0.271529);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9435f48() {
   return (neuron0x9424b60()*0.425408);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436188() {
   return (neuron0x9415bb0()*0.00889427);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94361b0() {
   return (neuron0x9415de8()*-0.362203);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94361d8() {
   return (neuron0x9416010()*-0.406356);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436200() {
   return (neuron0x94245a8()*0.113448);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436228() {
   return (neuron0x9424760()*-0.31407);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436250() {
   return (neuron0x9424960()*-0.228487);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436278() {
   return (neuron0x9424b60()*0.167831);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94364b8() {
   return (neuron0x9415bb0()*-0.240331);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94364e0() {
   return (neuron0x9415de8()*0.419936);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436508() {
   return (neuron0x9416010()*0.451724);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436530() {
   return (neuron0x94245a8()*-0.470243);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436558() {
   return (neuron0x9424760()*0.200352);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436580() {
   return (neuron0x9424960()*0.00860408);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94365a8() {
   return (neuron0x9424b60()*-0.0433548);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94367e8() {
   return (neuron0x9415bb0()*-0.175891);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436810() {
   return (neuron0x9415de8()*0.0151708);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436838() {
   return (neuron0x9416010()*0.167963);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436860() {
   return (neuron0x94245a8()*-0.451534);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436888() {
   return (neuron0x9424760()*0.163507);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94368b0() {
   return (neuron0x9424960()*-0.40047);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94368d8() {
   return (neuron0x9424b60()*-0.352036);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436b18() {
   return (neuron0x9415bb0()*-0.0977499);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436b40() {
   return (neuron0x9415de8()*0.290965);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436b68() {
   return (neuron0x9416010()*0.11042);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436b90() {
   return (neuron0x94245a8()*0.30058);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436bb8() {
   return (neuron0x9424760()*0.513745);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436be0() {
   return (neuron0x9424960()*0.145372);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436c08() {
   return (neuron0x9424b60()*0.0889575);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436e48() {
   return (neuron0x9415bb0()*0.335162);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436e70() {
   return (neuron0x9415de8()*-0.478653);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436e98() {
   return (neuron0x9416010()*0.197447);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436ec0() {
   return (neuron0x94245a8()*-0.066796);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436ee8() {
   return (neuron0x9424760()*0.651466);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436f10() {
   return (neuron0x9424960()*-0.151852);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9436f38() {
   return (neuron0x9424b60()*-0.0764156);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437178() {
   return (neuron0x9415bb0()*-0.185334);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94371a0() {
   return (neuron0x9415de8()*0.1691);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94371c8() {
   return (neuron0x9416010()*0.322043);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94371f0() {
   return (neuron0x94245a8()*0.38776);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437218() {
   return (neuron0x9424760()*0.251579);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437240() {
   return (neuron0x9424960()*0.289387);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437268() {
   return (neuron0x9424b60()*-0.290957);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94374a8() {
   return (neuron0x9415bb0()*0.104346);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94374d0() {
   return (neuron0x9415de8()*-0.19169);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94374f8() {
   return (neuron0x9416010()*-0.00130277);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437520() {
   return (neuron0x94245a8()*0.066044);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437548() {
   return (neuron0x9424760()*-0.347264);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437570() {
   return (neuron0x9424960()*-0.226358);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437598() {
   return (neuron0x9424b60()*0.14133);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94377d8() {
   return (neuron0x9415bb0()*0.128098);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437800() {
   return (neuron0x9415de8()*0.119505);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437828() {
   return (neuron0x9416010()*-0.073494);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437850() {
   return (neuron0x94245a8()*-0.226739);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437878() {
   return (neuron0x9424760()*-0.0570454);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94378a0() {
   return (neuron0x9424960()*0.101963);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94378c8() {
   return (neuron0x9424b60()*-0.135913);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437b08() {
   return (neuron0x9415bb0()*0.253831);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437b30() {
   return (neuron0x9415de8()*-0.149042);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437b58() {
   return (neuron0x9416010()*0.275721);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437b80() {
   return (neuron0x94245a8()*-0.0750936);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437ba8() {
   return (neuron0x9424760()*-0.474368);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437bd0() {
   return (neuron0x9424960()*0.223457);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437bf8() {
   return (neuron0x9424b60()*-0.444104);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437e38() {
   return (neuron0x9415bb0()*0.376955);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437e60() {
   return (neuron0x9415de8()*-0.202933);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437e88() {
   return (neuron0x9416010()*0.0727686);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437eb0() {
   return (neuron0x94245a8()*0.00534737);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437ed8() {
   return (neuron0x9424760()*0.382679);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437f00() {
   return (neuron0x9424960()*0.458182);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9437f28() {
   return (neuron0x9424b60()*0.0082288);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438168() {
   return (neuron0x9415bb0()*-0.0427169);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438190() {
   return (neuron0x9415de8()*-0.468912);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94381b8() {
   return (neuron0x9416010()*0.330411);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94381e0() {
   return (neuron0x94245a8()*0.383);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438208() {
   return (neuron0x9424760()*-0.257174);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438230() {
   return (neuron0x9424960()*0.0745018);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438258() {
   return (neuron0x9424b60()*0.0859837);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438498() {
   return (neuron0x9415bb0()*-0.185085);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94384c0() {
   return (neuron0x9415de8()*0.292545);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94384e8() {
   return (neuron0x9416010()*0.353958);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438510() {
   return (neuron0x94245a8()*0.442747);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438538() {
   return (neuron0x9424760()*0.30618);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438560() {
   return (neuron0x9424960()*-0.465461);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438588() {
   return (neuron0x9424b60()*0.0566692);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94387c8() {
   return (neuron0x9415bb0()*-0.418705);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94387f0() {
   return (neuron0x9415de8()*0.100612);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438818() {
   return (neuron0x9416010()*0.340192);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438840() {
   return (neuron0x94245a8()*-0.0101752);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438868() {
   return (neuron0x9424760()*-0.354203);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438890() {
   return (neuron0x9424960()*-0.033338);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94388b8() {
   return (neuron0x9424b60()*0.261183);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438af8() {
   return (neuron0x9415bb0()*-0.0350856);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438b20() {
   return (neuron0x9415de8()*-0.4038);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438b48() {
   return (neuron0x9416010()*-0.0915475);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438b70() {
   return (neuron0x94245a8()*-0.420172);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438b98() {
   return (neuron0x9424760()*-0.277717);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438bc0() {
   return (neuron0x9424960()*0.123842);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438be8() {
   return (neuron0x9424b60()*0.0894134);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438e28() {
   return (neuron0x9415bb0()*0.268222);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438e50() {
   return (neuron0x9415de8()*0.331046);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438e78() {
   return (neuron0x9416010()*0.00767889);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438ea0() {
   return (neuron0x94245a8()*-0.0895633);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438ec8() {
   return (neuron0x9424760()*-0.283592);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438ef0() {
   return (neuron0x9424960()*0.242698);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9438f18() {
   return (neuron0x9424b60()*-0.202821);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439158() {
   return (neuron0x9415bb0()*-0.272654);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439180() {
   return (neuron0x9415de8()*0.0107519);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94391a8() {
   return (neuron0x9416010()*0.474656);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94391d0() {
   return (neuron0x94245a8()*0.268036);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94391f8() {
   return (neuron0x9424760()*0.0453326);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439220() {
   return (neuron0x9424960()*0.132076);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439248() {
   return (neuron0x9424b60()*-0.177805);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439488() {
   return (neuron0x9415bb0()*0.165497);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94394b0() {
   return (neuron0x9415de8()*0.0175221);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94394d8() {
   return (neuron0x9416010()*-0.164045);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439500() {
   return (neuron0x94245a8()*0.111103);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439528() {
   return (neuron0x9424760()*-0.626775);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439550() {
   return (neuron0x9424960()*0.167814);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439578() {
   return (neuron0x9424b60()*0.0413424);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94397b8() {
   return (neuron0x9415bb0()*-0.378425);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94397e0() {
   return (neuron0x9415de8()*0.236872);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439808() {
   return (neuron0x9416010()*0.0129875);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439830() {
   return (neuron0x94245a8()*-0.290559);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439858() {
   return (neuron0x9424760()*-0.177882);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439880() {
   return (neuron0x9424960()*-0.272476);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x94398a8() {
   return (neuron0x9424b60()*-0.206178);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439ae8() {
   return (neuron0x9415bb0()*0.413058);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439b10() {
   return (neuron0x9415de8()*-0.145511);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439b38() {
   return (neuron0x9416010()*-0.450459);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439b60() {
   return (neuron0x94245a8()*0.365335);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439b88() {
   return (neuron0x9424760()*0.144719);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439bb0() {
   return (neuron0x9424960()*-0.00780865);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439bd8() {
   return (neuron0x9424b60()*0.222337);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439e18() {
   return (neuron0x9415bb0()*0.34847);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439e40() {
   return (neuron0x9415de8()*-0.0541334);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439e68() {
   return (neuron0x9416010()*0.366166);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439e90() {
   return (neuron0x94245a8()*-0.108553);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439eb8() {
   return (neuron0x9424760()*0.537662);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439ee0() {
   return (neuron0x9424960()*-0.0592257);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x9439f08() {
   return (neuron0x9424b60()*0.0274372);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a148() {
   return (neuron0x9415bb0()*0.0299963);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a170() {
   return (neuron0x9415de8()*0.344204);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a198() {
   return (neuron0x9416010()*0.354179);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a1c0() {
   return (neuron0x94245a8()*-0.19861);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a1e8() {
   return (neuron0x9424760()*-0.0244817);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a210() {
   return (neuron0x9424960()*0.342755);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a238() {
   return (neuron0x9424b60()*0.0504647);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a478() {
   return (neuron0x9415bb0()*0.418351);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a4a0() {
   return (neuron0x9415de8()*-0.0277783);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a4c8() {
   return (neuron0x9416010()*0.343523);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a4f0() {
   return (neuron0x94245a8()*-0.00186274);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a518() {
   return (neuron0x9424760()*-0.366404);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a540() {
   return (neuron0x9424960()*-0.418806);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a568() {
   return (neuron0x9424b60()*0.29372);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a7a8() {
   return (neuron0x9415bb0()*-0.312385);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a7d0() {
   return (neuron0x9415de8()*-0.268382);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a7f8() {
   return (neuron0x9416010()*0.177724);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a820() {
   return (neuron0x94245a8()*-0.203572);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a848() {
   return (neuron0x9424760()*0.0969521);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a870() {
   return (neuron0x9424960()*-0.352381);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943a898() {
   return (neuron0x9424b60()*0.0427611);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943aad8() {
   return (neuron0x9415bb0()*-0.355387);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ab00() {
   return (neuron0x9415de8()*0.270409);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ab28() {
   return (neuron0x9416010()*-0.0678286);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ab50() {
   return (neuron0x94245a8()*0.26695);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ab78() {
   return (neuron0x9424760()*0.104513);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943aba0() {
   return (neuron0x9424960()*0.111172);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943abc8() {
   return (neuron0x9424b60()*0.334298);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ae08() {
   return (neuron0x9415bb0()*0.328087);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ae30() {
   return (neuron0x9415de8()*0.288746);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ae58() {
   return (neuron0x9416010()*-0.0725244);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ae80() {
   return (neuron0x94245a8()*-0.2433);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943aea8() {
   return (neuron0x9424760()*0.0373228);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943aed0() {
   return (neuron0x9424960()*0.0264998);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943aef8() {
   return (neuron0x9424b60()*-0.336153);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b138() {
   return (neuron0x9415bb0()*-0.277328);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b160() {
   return (neuron0x9415de8()*0.447167);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b188() {
   return (neuron0x9416010()*-0.218528);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b1b0() {
   return (neuron0x94245a8()*-0.145529);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b1d8() {
   return (neuron0x9424760()*-0.106136);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b200() {
   return (neuron0x9424960()*0.209092);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b228() {
   return (neuron0x9424b60()*0.129698);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b468() {
   return (neuron0x9415bb0()*-0.325318);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b490() {
   return (neuron0x9415de8()*0.403389);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b4b8() {
   return (neuron0x9416010()*-0.00662699);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b4e0() {
   return (neuron0x94245a8()*-0.383843);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b508() {
   return (neuron0x9424760()*-0.296892);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b530() {
   return (neuron0x9424960()*-0.129546);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b558() {
   return (neuron0x9424b60()*0.0159881);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b798() {
   return (neuron0x9415bb0()*-0.189486);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b7c0() {
   return (neuron0x9415de8()*0.0539421);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b7e8() {
   return (neuron0x9416010()*-0.195397);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b810() {
   return (neuron0x94245a8()*0.364947);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b838() {
   return (neuron0x9424760()*-0.369209);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b860() {
   return (neuron0x9424960()*0.393164);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943b888() {
   return (neuron0x9424b60()*0.151094);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bac8() {
   return (neuron0x9415bb0()*0.229497);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943baf0() {
   return (neuron0x9415de8()*0.331438);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bb18() {
   return (neuron0x9416010()*-0.265822);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bb40() {
   return (neuron0x94245a8()*0.0130154);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bb68() {
   return (neuron0x9424760()*-0.378491);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bb90() {
   return (neuron0x9424960()*-0.400245);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bbb8() {
   return (neuron0x9424b60()*0.184985);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bdf8() {
   return (neuron0x9415bb0()*-0.0159705);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943be20() {
   return (neuron0x9415de8()*0.203063);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943be48() {
   return (neuron0x9416010()*0.0499258);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943be70() {
   return (neuron0x94245a8()*0.288652);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943be98() {
   return (neuron0x9424760()*0.457939);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bec0() {
   return (neuron0x9424960()*0.304273);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943bee8() {
   return (neuron0x9424b60()*-0.0853249);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c128() {
   return (neuron0x9415bb0()*-0.21685);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c150() {
   return (neuron0x9415de8()*-0.171912);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c178() {
   return (neuron0x9416010()*0.00242098);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c1a0() {
   return (neuron0x94245a8()*0.427567);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c1c8() {
   return (neuron0x9424760()*-0.16351);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c1f0() {
   return (neuron0x9424960()*-0.375111);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c218() {
   return (neuron0x9424b60()*0.194858);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c458() {
   return (neuron0x9415bb0()*-0.452948);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c480() {
   return (neuron0x9415de8()*-0.458252);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c4a8() {
   return (neuron0x9416010()*0.291525);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c4d0() {
   return (neuron0x94245a8()*0.112788);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c4f8() {
   return (neuron0x9424760()*0.424485);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c520() {
   return (neuron0x9424960()*0.362338);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c548() {
   return (neuron0x9424b60()*0.293733);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c788() {
   return (neuron0x9415bb0()*0.411208);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c7b0() {
   return (neuron0x9415de8()*0.429842);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c7d8() {
   return (neuron0x9416010()*-0.449358);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c800() {
   return (neuron0x94245a8()*0.0594103);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c828() {
   return (neuron0x9424760()*0.349918);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c850() {
   return (neuron0x9424960()*0.23421);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c878() {
   return (neuron0x9424b60()*-0.156647);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c978() {
   return (neuron0x9424e88()*0.439471);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c9a0() {
   return (neuron0x94250b8()*-0.136112);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c9c8() {
   return (neuron0x94253d0()*0.192917);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c9f0() {
   return (neuron0x9425770()*-0.3291);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ca18() {
   return (neuron0x9425a40()*-0.219894);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ca40() {
   return (neuron0x9425e10()*0.0581502);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ca68() {
   return (neuron0x9426100()*-0.0991699);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ca90() {
   return (neuron0x9426418()*-0.377122);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cab8() {
   return (neuron0x9426730()*0.341522);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cae0() {
   return (neuron0x9426a60()*-0.11783);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cb08() {
   return (neuron0x9426f58()*-0.307194);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cb30() {
   return (neuron0x94271b8()*0.365079);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cb58() {
   return (neuron0x94274d0()*0.184034);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cb80() {
   return (neuron0x94277e8()*0.119538);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cba8() {
   return (neuron0x9427b00()*0.104057);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cbd0() {
   return (neuron0x9427e18()*-0.39604);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cc80() {
   return (neuron0x94284e8()*0.228212);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cca8() {
   return (neuron0x9428728()*0.0816427);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ccd0() {
   return (neuron0x9428968()*-0.310493);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ccf8() {
   return (neuron0x9426df0()*0.0336148);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cd20() {
   return (neuron0x9429168()*-0.0977246);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cd48() {
   return (neuron0x9429498()*0.314804);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cd70() {
   return (neuron0x94297c8()*-0.202595);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cd98() {
   return (neuron0x9429af8()*-0.0456161);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cdc0() {
   return (neuron0x9429e28()*-0.0117724);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cde8() {
   return (neuron0x942a158()*-0.40422);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ce10() {
   return (neuron0x942a518()*-0.339267);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ce38() {
   return (neuron0x942a848()*0.393183);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ce60() {
   return (neuron0x942ab78()*-0.0907747);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ce88() {
   return (neuron0x942aea8()*-0.318013);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ceb0() {
   return (neuron0x942b1d8()*0.352071);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943ced8() {
   return (neuron0x942b508()*-0.146426);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cbf8() {
   return (neuron0x942be80()*-0.308703);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cc20() {
   return (neuron0x942bfa8()*0.250147);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cc48() {
   return (neuron0x942c278()*0.2024);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d008() {
   return (neuron0x942c590()*-0.329717);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d030() {
   return (neuron0x942c8a8()*-0.117929);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d058() {
   return (neuron0x9428be8()*0.482604);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d080() {
   return (neuron0x942d390()*0.190769);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d0a8() {
   return (neuron0x942d638()*0.428393);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d0d0() {
   return (neuron0x942d968()*-0.274626);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d0f8() {
   return (neuron0x942dc98()*-0.269649);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d120() {
   return (neuron0x942dfc8()*0.140611);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d148() {
   return (neuron0x942e2f8()*0.171973);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d170() {
   return (neuron0x942e628()*-0.443189);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d198() {
   return (neuron0x942e958()*-0.0276697);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d1c0() {
   return (neuron0x942ec88()*-0.11762);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d1e8() {
   return (neuron0x942efb8()*0.0775198);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d210() {
   return (neuron0x942f2e8()*0.592057);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d238() {
   return (neuron0x942f618()*-0.363998);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d260() {
   return (neuron0x942f948()*0.275121);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d288() {
   return (neuron0x942fc78()*-0.264033);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d2b0() {
   return (neuron0x942ffa8()*-0.270646);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d2d8() {
   return (neuron0x94302d8()*0.268628);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d300() {
   return (neuron0x9430608()*-0.255928);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d328() {
   return (neuron0x9430938()*-0.148994);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d350() {
   return (neuron0x9430c68()*0.430289);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d378() {
   return (neuron0x9430f98()*-0.00954953);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d3a0() {
   return (neuron0x94313e0()*0.0499569);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d3c8() {
   return (neuron0x94316f8()*-0.375581);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d3f0() {
   return (neuron0x9431a28()*0.360709);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d418() {
   return (neuron0x9431d58()*0.0878512);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d440() {
   return (neuron0x9432088()*-0.550344);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d468() {
   return (neuron0x94323b8()*0.452668);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943c8a0() {
   return (neuron0x942bc48()*-0.471067);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cf00() {
   return (neuron0x9433138()*0.0744985);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cf28() {
   return (neuron0x9433450()*-0.189878);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cf50() {
   return (neuron0x9433780()*-0.198096);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cf78() {
   return (neuron0x9433ab0()*-0.155509);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cfa0() {
   return (neuron0x9433de0()*-0.262394);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943cfc8() {
   return (neuron0x9434110()*0.314993);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d698() {
   return (neuron0x9434440()*-0.135869);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d6c0() {
   return (neuron0x9434770()*0.272989);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d6e8() {
   return (neuron0x9434aa0()*-0.0151305);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d710() {
   return (neuron0x942cc50()*0.620298);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d738() {
   return (neuron0x942cf80()*0.289026);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d760() {
   return (neuron0x9435d10()*-0.390438);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d788() {
   return (neuron0x9435f70()*0.0683047);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d7b0() {
   return (neuron0x94362a0()*-0.0997202);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d7d8() {
   return (neuron0x94365d0()*-0.452873);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d800() {
   return (neuron0x9436900()*0.472478);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d828() {
   return (neuron0x9436c30()*0.286686);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d850() {
   return (neuron0x9436f60()*-0.174157);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d878() {
   return (neuron0x9437290()*-0.113387);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d8a0() {
   return (neuron0x94375c0()*-0.36896);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d8c8() {
   return (neuron0x94378f0()*0.0227287);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d8f0() {
   return (neuron0x9437c20()*0.238743);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d918() {
   return (neuron0x9437f50()*0.200778);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d940() {
   return (neuron0x9438280()*0.0627675);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d968() {
   return (neuron0x94385b0()*0.188477);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d990() {
   return (neuron0x94388e0()*0.554518);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d9b8() {
   return (neuron0x9438c10()*0.232207);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943d9e0() {
   return (neuron0x9438f40()*-0.348195);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943da08() {
   return (neuron0x9439270()*0.583841);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943da30() {
   return (neuron0x94395a0()*0.202649);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943da58() {
   return (neuron0x94398d0()*-0.517217);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943da80() {
   return (neuron0x9439c00()*0.126051);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943daa8() {
   return (neuron0x9439f30()*0.160864);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943dad0() {
   return (neuron0x943a260()*0.036182);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943daf8() {
   return (neuron0x943a590()*0.0298549);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943db20() {
   return (neuron0x943a8c0()*-0.231583);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943db48() {
   return (neuron0x943abf0()*-0.182497);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943db70() {
   return (neuron0x943af20()*0.237729);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943db98() {
   return (neuron0x943b250()*0.660387);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943dbc0() {
   return (neuron0x943b580()*-0.170249);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943dbe8() {
   return (neuron0x943b8b0()*-0.103878);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943dc10() {
   return (neuron0x943bbe0()*0.418309);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943dc38() {
   return (neuron0x943bf10()*0.277753);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943dc60() {
   return (neuron0x943c240()*-0.0622811);
}

double NNPerpElectronAngleCorrectionDeltaPhi::synapse0x943dc88() {
   return (neuron0x943c570()*-0.0367146);
}

