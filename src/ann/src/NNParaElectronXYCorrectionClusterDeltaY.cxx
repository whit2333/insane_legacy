#include "NNParaElectronXYCorrectionClusterDeltaY.h"
#include <cmath>

double NNParaElectronXYCorrectionClusterDeltaY::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10) {
   input0 = (in0 - 1931.89)/1024.17;
   input1 = (in1 - -3.92757)/33.4126;
   input2 = (in2 - -1.75708)/60.7242;
   input3 = (in3 - 1.30402)/0.374511;
   input4 = (in4 - 1.38855)/0.399413;
   input5 = (in5 - 0.279299)/1.99665;
   input6 = (in6 - 0.276484)/1.83307;
   input7 = (in7 - 6.6926)/10.2788;
   input8 = (in8 - 5.61334)/10.0137;
   input9 = (in9 - 0.00823462)/0.746842;
   input10 = (in10 - -0.000800713)/0.755077;
   switch(index) {
     case 0:
         return neuron0x9116968();
     default:
         return 0.;
   }
}

double NNParaElectronXYCorrectionClusterDeltaY::Value(int index, double* input) {
   input0 = (input[0] - 1931.89)/1024.17;
   input1 = (input[1] - -3.92757)/33.4126;
   input2 = (input[2] - -1.75708)/60.7242;
   input3 = (input[3] - 1.30402)/0.374511;
   input4 = (input[4] - 1.38855)/0.399413;
   input5 = (input[5] - 0.279299)/1.99665;
   input6 = (input[6] - 0.276484)/1.83307;
   input7 = (input[7] - 6.6926)/10.2788;
   input8 = (input[8] - 5.61334)/10.0137;
   input9 = (input[9] - 0.00823462)/0.746842;
   input10 = (input[10] - -0.000800713)/0.755077;
   switch(index) {
     case 0:
         return neuron0x9116968();
     default:
         return 0.;
   }
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90f93a8() {
   return input0;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90f9560() {
   return input1;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90f9760() {
   return input2;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90f9960() {
   return input3;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90f9b78() {
   return input4;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90f9d90() {
   return input5;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90f9fa8() {
   return input6;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fa1c0() {
   return input7;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fa3d8() {
   return input8;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fa5f0() {
   return input9;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fa7f0() {
   return input10;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fab30() {
   double input = -0.712655;
   input += synapse0x8f088f8();
   input += synapse0x90face8();
   input += synapse0x90fad10();
   input += synapse0x90fad38();
   input += synapse0x90fad60();
   input += synapse0x90fad88();
   input += synapse0x90fadb0();
   input += synapse0x90fadd8();
   input += synapse0x90fae00();
   input += synapse0x90fae28();
   input += synapse0x90fae50();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fab30() {
   double input = input0x90fab30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fae78() {
   double input = -0.0695648;
   input += synapse0x90fb078();
   input += synapse0x90fb0a0();
   input += synapse0x90fb0c8();
   input += synapse0x90fb0f0();
   input += synapse0x90fb118();
   input += synapse0x90fb140();
   input += synapse0x90fb1f0();
   input += synapse0x90fb218();
   input += synapse0x90fb240();
   input += synapse0x90fb268();
   input += synapse0x90fb290();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fae78() {
   double input = input0x90fae78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fb2b8() {
   double input = -0.376285;
   input += synapse0x90fb470();
   input += synapse0x90fb498();
   input += synapse0x90fb4c0();
   input += synapse0x90fb4e8();
   input += synapse0x90fb510();
   input += synapse0x90fb538();
   input += synapse0x90fb560();
   input += synapse0x90fb588();
   input += synapse0x90fb5b0();
   input += synapse0x90fb5d8();
   input += synapse0x90fb600();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fb2b8() {
   double input = input0x90fb2b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fb730() {
   double input = -0.419536;
   input += synapse0x90fb930();
   input += synapse0x90fb958();
   input += synapse0x90fb980();
   input += synapse0x90fb9a8();
   input += synapse0x90fb9d0();
   input += synapse0x90fb9f8();
   input += synapse0x90fba20();
   input += synapse0x90fba48();
   input += synapse0x90fba70();
   input += synapse0x90fba98();
   input += synapse0x90fbac0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fb730() {
   double input = input0x90fb730();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fbae8() {
   double input = -0.525517;
   input += synapse0x90fbce8();
   input += synapse0x90fbd10();
   input += synapse0x90fbd38();
   input += synapse0x90fbd60();
   input += synapse0x90fbd88();
   input += synapse0x90fbdb0();
   input += synapse0x90fbdd8();
   input += synapse0x90fbe00();
   input += synapse0x90fbe28();
   input += synapse0x90fbe50();
   input += synapse0x90fbe78();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fbae8() {
   double input = input0x90fbae8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fbea0() {
   double input = -0.0274699;
   input += synapse0x90fc0a0();
   input += synapse0x90fc0c8();
   input += synapse0x90fc0f0();
   input += synapse0x90fc118();
   input += synapse0x90fc140();
   input += synapse0x90fc168();
   input += synapse0x90fc190();
   input += synapse0x90fc1b8();
   input += synapse0x90fc1e0();
   input += synapse0x90fc208();
   input += synapse0x8f08678();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fbea0() {
   double input = input0x90fbea0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fc438() {
   double input = 1.53328;
   input += synapse0x90fc608();
   input += synapse0x90fc630();
   input += synapse0x90fc658();
   input += synapse0x90fc680();
   input += synapse0x90fc6a8();
   input += synapse0x90fc6d0();
   input += synapse0x90fc6f8();
   input += synapse0x90fc720();
   input += synapse0x90fc748();
   input += synapse0x90fc770();
   input += synapse0x90fc798();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fc438() {
   double input = input0x90fc438();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fc7c0() {
   double input = -0.250044;
   input += synapse0x90fc9d8();
   input += synapse0x90fca00();
   input += synapse0x90fca28();
   input += synapse0x90fca50();
   input += synapse0x90fca78();
   input += synapse0x90fcaa0();
   input += synapse0x90fcac8();
   input += synapse0x90fcaf0();
   input += synapse0x90fcb18();
   input += synapse0x90fcb40();
   input += synapse0x90fcb68();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fc7c0() {
   double input = input0x90fc7c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fcb90() {
   double input = 0.574355;
   input += synapse0x90fcda8();
   input += synapse0x90fcdd0();
   input += synapse0x90fcdf8();
   input += synapse0x90fce20();
   input += synapse0x90fce48();
   input += synapse0x90fce70();
   input += synapse0x90fce98();
   input += synapse0x90fcec0();
   input += synapse0x90fcee8();
   input += synapse0x90fcf10();
   input += synapse0x90fcf38();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fcb90() {
   double input = input0x90fcb90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fcf60() {
   double input = -0.220721;
   input += synapse0x90fd178();
   input += synapse0x90fd1a0();
   input += synapse0x90fd1c8();
   input += synapse0x90fd1f0();
   input += synapse0x90fd218();
   input += synapse0x90fd240();
   input += synapse0x90fd268();
   input += synapse0x90fd290();
   input += synapse0x90fd2b8();
   input += synapse0x90fd2e0();
   input += synapse0x90fd308();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fcf60() {
   double input = input0x90fcf60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fd330() {
   double input = -0.275992;
   input += synapse0x90fd548();
   input += synapse0x90fd570();
   input += synapse0x90fd598();
   input += synapse0x90fd5c0();
   input += synapse0x90fd5e8();
   input += synapse0x90fd610();
   input += synapse0x90fd638();
   input += synapse0x90fd660();
   input += synapse0x90fd688();
   input += synapse0x90fd6b0();
   input += synapse0x90fd6d8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fd330() {
   double input = input0x90fd330();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fd700() {
   double input = 0.114302;
   input += synapse0x90fd918();
   input += synapse0x90fd940();
   input += synapse0x90fd968();
   input += synapse0x90fd990();
   input += synapse0x90fd9b8();
   input += synapse0x90fd9e0();
   input += synapse0x90fda08();
   input += synapse0x90fda30();
   input += synapse0x8f08728();
   input += synapse0x8f08750();
   input += synapse0x8f08810();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fd700() {
   double input = input0x90fd700();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fc230() {
   double input = -0.273451;
   input += synapse0x8f08880();
   input += synapse0x8f088a8();
   input += synapse0x8f088d0();
   input += synapse0x90fc400();
   input += synapse0x90fb628();
   input += synapse0x90fb650();
   input += synapse0x90fb678();
   input += synapse0x90fb6a0();
   input += synapse0x90fb6c8();
   input += synapse0x90fb6f0();
   input += synapse0x90da4d8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fc230() {
   double input = input0x90fc230();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fde60() {
   double input = -0.224018;
   input += synapse0x90fe060();
   input += synapse0x90fe088();
   input += synapse0x90fe0b0();
   input += synapse0x90fe0d8();
   input += synapse0x90fe100();
   input += synapse0x90fe128();
   input += synapse0x90fe150();
   input += synapse0x90fe178();
   input += synapse0x90fe1a0();
   input += synapse0x90fe1c8();
   input += synapse0x90fe1f0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fde60() {
   double input = input0x90fde60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fe218() {
   double input = 1.0187;
   input += synapse0x90fe430();
   input += synapse0x90fe458();
   input += synapse0x90fe480();
   input += synapse0x90fe4a8();
   input += synapse0x90fe4d0();
   input += synapse0x90fe4f8();
   input += synapse0x90fe520();
   input += synapse0x90fe548();
   input += synapse0x90fe570();
   input += synapse0x90fe598();
   input += synapse0x90fe5c0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fe218() {
   double input = input0x90fe218();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fe5e8() {
   double input = -0.143469;
   input += synapse0x90fe800();
   input += synapse0x90fe8b0();
   input += synapse0x90fe960();
   input += synapse0x90fea10();
   input += synapse0x90feac0();
   input += synapse0x90feb70();
   input += synapse0x90fec20();
   input += synapse0x90fecd0();
   input += synapse0x90fed80();
   input += synapse0x90fee30();
   input += synapse0x90feee0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fe5e8() {
   double input = input0x90fe5e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fef90() {
   double input = -0.212497;
   input += synapse0x90ff0d0();
   input += synapse0x90ff0f8();
   input += synapse0x90ff120();
   input += synapse0x90ff148();
   input += synapse0x90da500();
   input += synapse0x90da528();
   input += synapse0x90da550();
   input += synapse0x90da578();
   input += synapse0x90da5a0();
   input += synapse0x9100120();
   input += synapse0x9100148();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fef90() {
   double input = input0x90fef90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9100170() {
   double input = 2.0598;
   input += synapse0x9100298();
   input += synapse0x91002c0();
   input += synapse0x91002e8();
   input += synapse0x9100310();
   input += synapse0x9100338();
   input += synapse0x9100360();
   input += synapse0x9100388();
   input += synapse0x91003b0();
   input += synapse0x91003d8();
   input += synapse0x9100400();
   input += synapse0x9100428();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9100170() {
   double input = input0x9100170();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9100450() {
   double input = -0.0608303;
   input += synapse0x9100578();
   input += synapse0x91005a0();
   input += synapse0x91005c8();
   input += synapse0x91005f0();
   input += synapse0x9100618();
   input += synapse0x9100640();
   input += synapse0x9100668();
   input += synapse0x9100690();
   input += synapse0x91006b8();
   input += synapse0x91006e0();
   input += synapse0x9100708();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9100450() {
   double input = input0x9100450();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9100730() {
   double input = 0.300259;
   input += synapse0x91008a0();
   input += synapse0x91008c8();
   input += synapse0x91008f0();
   input += synapse0x9100918();
   input += synapse0x9100940();
   input += synapse0x9100968();
   input += synapse0x9100990();
   input += synapse0x91009b8();
   input += synapse0x91009e0();
   input += synapse0x9100a08();
   input += synapse0x9100a30();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9100730() {
   double input = input0x9100730();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9100a58() {
   double input = 0.329086;
   input += synapse0x9100c58();
   input += synapse0x9100c80();
   input += synapse0x9100ca8();
   input += synapse0x9100cd0();
   input += synapse0x9100cf8();
   input += synapse0x9100d20();
   input += synapse0x9100d48();
   input += synapse0x9100d70();
   input += synapse0x9100d98();
   input += synapse0x9100dc0();
   input += synapse0x9100de8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9100a58() {
   double input = input0x9100a58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9100e10() {
   double input = -0.377518;
   input += synapse0x90fb168();
   input += synapse0x90fb190();
   input += synapse0x90fb1b8();
   input += synapse0x9101118();
   input += synapse0x9101140();
   input += synapse0x9101168();
   input += synapse0x9101190();
   input += synapse0x91011b8();
   input += synapse0x91011e0();
   input += synapse0x9101208();
   input += synapse0x9101230();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9100e10() {
   double input = input0x9100e10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9101258() {
   double input = 1.54667;
   input += synapse0x9101458();
   input += synapse0x9101480();
   input += synapse0x91014a8();
   input += synapse0x91014d0();
   input += synapse0x91014f8();
   input += synapse0x9101520();
   input += synapse0x9101548();
   input += synapse0x9101570();
   input += synapse0x9101598();
   input += synapse0x91015c0();
   input += synapse0x91015e8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9101258() {
   double input = input0x9101258();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9101610() {
   double input = -0.12516;
   input += synapse0x9101810();
   input += synapse0x9101838();
   input += synapse0x9101860();
   input += synapse0x9101888();
   input += synapse0x90fda58();
   input += synapse0x90fda80();
   input += synapse0x90fdaa8();
   input += synapse0x90fdad0();
   input += synapse0x90fdaf8();
   input += synapse0x90fdb20();
   input += synapse0x90fdb48();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9101610() {
   double input = input0x9101610();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x90fdb70() {
   double input = -0.097467;
   input += synapse0x90fdd88();
   input += synapse0x90fddb0();
   input += synapse0x90fddd8();
   input += synapse0x90fde00();
   input += synapse0x90fde28();
   input += synapse0x91020b8();
   input += synapse0x91020e0();
   input += synapse0x9102108();
   input += synapse0x9102130();
   input += synapse0x9102158();
   input += synapse0x9102180();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x90fdb70() {
   double input = input0x90fdb70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91021a8() {
   double input = -0.571388;
   input += synapse0x91023a8();
   input += synapse0x91023d0();
   input += synapse0x91023f8();
   input += synapse0x9102420();
   input += synapse0x9102448();
   input += synapse0x9102470();
   input += synapse0x9102498();
   input += synapse0x91024c0();
   input += synapse0x91024e8();
   input += synapse0x9102510();
   input += synapse0x9102538();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91021a8() {
   double input = input0x91021a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9102560() {
   double input = -0.27076;
   input += synapse0x9102778();
   input += synapse0x91027a0();
   input += synapse0x91027c8();
   input += synapse0x91027f0();
   input += synapse0x9102818();
   input += synapse0x9102840();
   input += synapse0x9102868();
   input += synapse0x9102890();
   input += synapse0x91028b8();
   input += synapse0x91028e0();
   input += synapse0x9102908();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9102560() {
   double input = input0x9102560();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9102930() {
   double input = 1.01121;
   input += synapse0x9102b48();
   input += synapse0x9102b70();
   input += synapse0x9102b98();
   input += synapse0x9102bc0();
   input += synapse0x9102be8();
   input += synapse0x9102c10();
   input += synapse0x9102c38();
   input += synapse0x9102c60();
   input += synapse0x9102c88();
   input += synapse0x9102cb0();
   input += synapse0x9102cd8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9102930() {
   double input = input0x9102930();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9102d00() {
   double input = -2.28504;
   input += synapse0x9102f18();
   input += synapse0x9102f40();
   input += synapse0x9102f68();
   input += synapse0x9102f90();
   input += synapse0x9102fb8();
   input += synapse0x9102fe0();
   input += synapse0x9103008();
   input += synapse0x9103030();
   input += synapse0x9103058();
   input += synapse0x9103080();
   input += synapse0x91030a8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9102d00() {
   double input = input0x9102d00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91030d0() {
   double input = -0.137082;
   input += synapse0x91032e8();
   input += synapse0x9103310();
   input += synapse0x9103338();
   input += synapse0x9103360();
   input += synapse0x9103388();
   input += synapse0x91033b0();
   input += synapse0x91033d8();
   input += synapse0x9103400();
   input += synapse0x9103428();
   input += synapse0x9103450();
   input += synapse0x9103478();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91030d0() {
   double input = input0x91030d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91034a0() {
   double input = 0.493279;
   input += synapse0x91036b8();
   input += synapse0x91036e0();
   input += synapse0x9103708();
   input += synapse0x9103730();
   input += synapse0x9103758();
   input += synapse0x9103780();
   input += synapse0x91037a8();
   input += synapse0x91037d0();
   input += synapse0x91037f8();
   input += synapse0x9103820();
   input += synapse0x9103848();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91034a0() {
   double input = input0x91034a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9103870() {
   double input = 0.092827;
   input += synapse0x9103a88();
   input += synapse0x90fe828();
   input += synapse0x90fe850();
   input += synapse0x90fe878();
   input += synapse0x90fe8d8();
   input += synapse0x90fe900();
   input += synapse0x90fe928();
   input += synapse0x90fe988();
   input += synapse0x90fe9b0();
   input += synapse0x90fe9d8();
   input += synapse0x90fea38();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9103870() {
   double input = input0x9103870();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9104608() {
   double input = -0.683208;
   input += synapse0x90febe0();
   input += synapse0x90feb30();
   input += synapse0x90fec48();
   input += synapse0x90fec70();
   input += synapse0x90fec98();
   input += synapse0x90fecf8();
   input += synapse0x90fed20();
   input += synapse0x90fed48();
   input += synapse0x90feda8();
   input += synapse0x90fedd0();
   input += synapse0x90fedf8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9104608() {
   double input = input0x9104608();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9104730() {
   double input = 0.186703;
   input += synapse0x90feea0();
   input += synapse0x90fef50();
   input += synapse0x91048a0();
   input += synapse0x91048c8();
   input += synapse0x91048f0();
   input += synapse0x9104918();
   input += synapse0x9104940();
   input += synapse0x9104968();
   input += synapse0x9104990();
   input += synapse0x91049b8();
   input += synapse0x91049e0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9104730() {
   double input = input0x9104730();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9104a08() {
   double input = 0.600861;
   input += synapse0x9104c08();
   input += synapse0x9104c30();
   input += synapse0x9104c58();
   input += synapse0x9104c80();
   input += synapse0x9104ca8();
   input += synapse0x9104cd0();
   input += synapse0x9104cf8();
   input += synapse0x9104d20();
   input += synapse0x9104d48();
   input += synapse0x9104d70();
   input += synapse0x9104d98();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9104a08() {
   double input = input0x9104a08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9104dc0() {
   double input = 1.13112;
   input += synapse0x9104fc0();
   input += synapse0x9104fe8();
   input += synapse0x9105010();
   input += synapse0x9105038();
   input += synapse0x9105060();
   input += synapse0x9105088();
   input += synapse0x91050b0();
   input += synapse0x91050d8();
   input += synapse0x9105100();
   input += synapse0x9105128();
   input += synapse0x9105150();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9104dc0() {
   double input = input0x9104dc0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9105178() {
   double input = 0.164104;
   input += synapse0x9105378();
   input += synapse0x91053a0();
   input += synapse0x91053c8();
   input += synapse0x91053f0();
   input += synapse0x9105418();
   input += synapse0x9105440();
   input += synapse0x9105468();
   input += synapse0x9105490();
   input += synapse0x91054b8();
   input += synapse0x91054e0();
   input += synapse0x9105508();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9105178() {
   double input = input0x9105178();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9105530() {
   double input = 0.146915;
   input += synapse0x9105748();
   input += synapse0x9105770();
   input += synapse0x9105798();
   input += synapse0x91057c0();
   input += synapse0x91057e8();
   input += synapse0x9105810();
   input += synapse0x9105838();
   input += synapse0x9105860();
   input += synapse0x9105888();
   input += synapse0x91058b0();
   input += synapse0x91058d8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9105530() {
   double input = input0x9105530();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9105900() {
   double input = 0.0979825;
   input += synapse0x9105b18();
   input += synapse0x9105b40();
   input += synapse0x9105b68();
   input += synapse0x9105b90();
   input += synapse0x9105bb8();
   input += synapse0x9105be0();
   input += synapse0x9105c08();
   input += synapse0x9105c30();
   input += synapse0x9105c58();
   input += synapse0x9105c80();
   input += synapse0x9105ca8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9105900() {
   double input = input0x9105900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9105cd0() {
   double input = -2.50073;
   input += synapse0x9105ee8();
   input += synapse0x9105f10();
   input += synapse0x9105f38();
   input += synapse0x9105f60();
   input += synapse0x9105f88();
   input += synapse0x9105fb0();
   input += synapse0x9105fd8();
   input += synapse0x9106000();
   input += synapse0x9106028();
   input += synapse0x9106050();
   input += synapse0x9106078();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9105cd0() {
   double input = input0x9105cd0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91060a0() {
   double input = 0.320658;
   input += synapse0x91062b8();
   input += synapse0x91062e0();
   input += synapse0x9106308();
   input += synapse0x9106330();
   input += synapse0x9106358();
   input += synapse0x9106380();
   input += synapse0x91063a8();
   input += synapse0x91063d0();
   input += synapse0x91063f8();
   input += synapse0x9106420();
   input += synapse0x9106448();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91060a0() {
   double input = input0x91060a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9106470() {
   double input = 0.22919;
   input += synapse0x9106688();
   input += synapse0x91066b0();
   input += synapse0x91066d8();
   input += synapse0x9106700();
   input += synapse0x9106728();
   input += synapse0x9106750();
   input += synapse0x9106778();
   input += synapse0x91067a0();
   input += synapse0x91067c8();
   input += synapse0x91067f0();
   input += synapse0x9106818();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9106470() {
   double input = input0x9106470();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9106840() {
   double input = 0.07847;
   input += synapse0x9106a58();
   input += synapse0x9106a80();
   input += synapse0x9106aa8();
   input += synapse0x9106ad0();
   input += synapse0x9106af8();
   input += synapse0x9106b20();
   input += synapse0x9106b48();
   input += synapse0x9106b70();
   input += synapse0x9106b98();
   input += synapse0x9106bc0();
   input += synapse0x9106be8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9106840() {
   double input = input0x9106840();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9106c10() {
   double input = 0.491824;
   input += synapse0x9106e28();
   input += synapse0x9106e50();
   input += synapse0x9106e78();
   input += synapse0x9106ea0();
   input += synapse0x9106ec8();
   input += synapse0x9106ef0();
   input += synapse0x9106f18();
   input += synapse0x9106f40();
   input += synapse0x9106f68();
   input += synapse0x9106f90();
   input += synapse0x9106fb8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9106c10() {
   double input = input0x9106c10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9106fe0() {
   double input = -0.0432228;
   input += synapse0x91071f8();
   input += synapse0x9107220();
   input += synapse0x9107248();
   input += synapse0x9107270();
   input += synapse0x9107298();
   input += synapse0x91072c0();
   input += synapse0x91072e8();
   input += synapse0x9107310();
   input += synapse0x9107338();
   input += synapse0x9107360();
   input += synapse0x9107388();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9106fe0() {
   double input = input0x9106fe0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91073b0() {
   double input = 0.458062;
   input += synapse0x91075c8();
   input += synapse0x91075f0();
   input += synapse0x9107618();
   input += synapse0x9107640();
   input += synapse0x9107668();
   input += synapse0x9107690();
   input += synapse0x91076b8();
   input += synapse0x91076e0();
   input += synapse0x9107708();
   input += synapse0x9107730();
   input += synapse0x9107758();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91073b0() {
   double input = input0x91073b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9107780() {
   double input = 0.0232879;
   input += synapse0x9107998();
   input += synapse0x91079c0();
   input += synapse0x91079e8();
   input += synapse0x9107a10();
   input += synapse0x9107a38();
   input += synapse0x9107a60();
   input += synapse0x9107a88();
   input += synapse0x91018b0();
   input += synapse0x91018d8();
   input += synapse0x9101900();
   input += synapse0x9101928();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9107780() {
   double input = input0x9107780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9101950() {
   double input = -0.0509902;
   input += synapse0x9101b68();
   input += synapse0x9101b90();
   input += synapse0x9101bb8();
   input += synapse0x9101be0();
   input += synapse0x9101c08();
   input += synapse0x9101c30();
   input += synapse0x9101c58();
   input += synapse0x9101c80();
   input += synapse0x9101ca8();
   input += synapse0x9101cd0();
   input += synapse0x9101cf8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9101950() {
   double input = input0x9101950();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9101d20() {
   double input = -0.069881;
   input += synapse0x9101f38();
   input += synapse0x9101f60();
   input += synapse0x9101f88();
   input += synapse0x9101fb0();
   input += synapse0x9101fd8();
   input += synapse0x9102000();
   input += synapse0x9102028();
   input += synapse0x9102050();
   input += synapse0x9102078();
   input += synapse0x9108ab8();
   input += synapse0x9108ae0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9101d20() {
   double input = input0x9101d20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9108b08() {
   double input = 0.250482;
   input += synapse0x9108d08();
   input += synapse0x9108d30();
   input += synapse0x9108d58();
   input += synapse0x9108d80();
   input += synapse0x9108da8();
   input += synapse0x9108dd0();
   input += synapse0x9108df8();
   input += synapse0x9108e20();
   input += synapse0x9108e48();
   input += synapse0x9108e70();
   input += synapse0x9108e98();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9108b08() {
   double input = input0x9108b08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9108ec0() {
   double input = -0.34552;
   input += synapse0x91090d8();
   input += synapse0x9109100();
   input += synapse0x9109128();
   input += synapse0x9109150();
   input += synapse0x9109178();
   input += synapse0x91091a0();
   input += synapse0x91091c8();
   input += synapse0x91091f0();
   input += synapse0x9109218();
   input += synapse0x9109240();
   input += synapse0x9109268();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9108ec0() {
   double input = input0x9108ec0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9109290() {
   double input = -0.0656211;
   input += synapse0x91094a8();
   input += synapse0x91094d0();
   input += synapse0x91094f8();
   input += synapse0x9109520();
   input += synapse0x9109548();
   input += synapse0x9109570();
   input += synapse0x9109598();
   input += synapse0x91095c0();
   input += synapse0x91095e8();
   input += synapse0x9109610();
   input += synapse0x9109638();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9109290() {
   double input = input0x9109290();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9109660() {
   double input = -0.246735;
   input += synapse0x9109878();
   input += synapse0x91098a0();
   input += synapse0x91098c8();
   input += synapse0x91098f0();
   input += synapse0x9109918();
   input += synapse0x9109940();
   input += synapse0x9109968();
   input += synapse0x9109990();
   input += synapse0x91099b8();
   input += synapse0x91099e0();
   input += synapse0x9109a08();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9109660() {
   double input = input0x9109660();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9109a30() {
   double input = 0.97684;
   input += synapse0x9101010();
   input += synapse0x9101038();
   input += synapse0x9101060();
   input += synapse0x9101088();
   input += synapse0x91010b0();
   input += synapse0x91010d8();
   input += synapse0x9109e50();
   input += synapse0x9109e78();
   input += synapse0x9109ea0();
   input += synapse0x9109ec8();
   input += synapse0x9109ef0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9109a30() {
   double input = input0x9109a30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9109f18() {
   double input = -0.0518554;
   input += synapse0x910a118();
   input += synapse0x910a140();
   input += synapse0x910a168();
   input += synapse0x910a190();
   input += synapse0x910a1b8();
   input += synapse0x910a1e0();
   input += synapse0x910a208();
   input += synapse0x910a230();
   input += synapse0x910a258();
   input += synapse0x910a280();
   input += synapse0x910a2a8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9109f18() {
   double input = input0x9109f18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910a2d0() {
   double input = -1.89032;
   input += synapse0x910a4e8();
   input += synapse0x910a510();
   input += synapse0x910a538();
   input += synapse0x910a560();
   input += synapse0x910a588();
   input += synapse0x910a5b0();
   input += synapse0x910a5d8();
   input += synapse0x910a600();
   input += synapse0x910a628();
   input += synapse0x910a650();
   input += synapse0x910a678();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910a2d0() {
   double input = input0x910a2d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910a6a0() {
   double input = 0.444542;
   input += synapse0x910a8b8();
   input += synapse0x910a8e0();
   input += synapse0x910a908();
   input += synapse0x910a930();
   input += synapse0x910a958();
   input += synapse0x910a980();
   input += synapse0x910a9a8();
   input += synapse0x910a9d0();
   input += synapse0x910a9f8();
   input += synapse0x910aa20();
   input += synapse0x910aa48();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910a6a0() {
   double input = input0x910a6a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910aa70() {
   double input = -0.252756;
   input += synapse0x910ac88();
   input += synapse0x910acb0();
   input += synapse0x910acd8();
   input += synapse0x910ad00();
   input += synapse0x910ad28();
   input += synapse0x910ad50();
   input += synapse0x910ad78();
   input += synapse0x910ada0();
   input += synapse0x910adc8();
   input += synapse0x910adf0();
   input += synapse0x910ae18();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910aa70() {
   double input = input0x910aa70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910ae40() {
   double input = 0.355191;
   input += synapse0x910b058();
   input += synapse0x910b080();
   input += synapse0x910b0a8();
   input += synapse0x910b0d0();
   input += synapse0x910b0f8();
   input += synapse0x910b120();
   input += synapse0x910b148();
   input += synapse0x910b170();
   input += synapse0x910b198();
   input += synapse0x910b1c0();
   input += synapse0x910b1e8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910ae40() {
   double input = input0x910ae40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910b210() {
   double input = 0.0757441;
   input += synapse0x910b428();
   input += synapse0x910b450();
   input += synapse0x910b478();
   input += synapse0x910b4a0();
   input += synapse0x910b4c8();
   input += synapse0x910b4f0();
   input += synapse0x910b518();
   input += synapse0x910b540();
   input += synapse0x910b568();
   input += synapse0x910b590();
   input += synapse0x910b5b8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910b210() {
   double input = input0x910b210();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910b5e0() {
   double input = -0.59512;
   input += synapse0x910b7f8();
   input += synapse0x910b820();
   input += synapse0x910b848();
   input += synapse0x910b870();
   input += synapse0x910b898();
   input += synapse0x910b8c0();
   input += synapse0x910b8e8();
   input += synapse0x910b910();
   input += synapse0x910b938();
   input += synapse0x910b960();
   input += synapse0x910b988();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910b5e0() {
   double input = input0x910b5e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910b9b0() {
   double input = 0.42562;
   input += synapse0x910bbc8();
   input += synapse0x910bbf0();
   input += synapse0x910bc18();
   input += synapse0x910bc40();
   input += synapse0x910bc68();
   input += synapse0x910bc90();
   input += synapse0x910bcb8();
   input += synapse0x910bce0();
   input += synapse0x910bd08();
   input += synapse0x910bd30();
   input += synapse0x910bd58();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910b9b0() {
   double input = input0x910b9b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910bd80() {
   double input = 0.279072;
   input += synapse0x910bf98();
   input += synapse0x910bfc0();
   input += synapse0x910bfe8();
   input += synapse0x910c010();
   input += synapse0x910c038();
   input += synapse0x910c060();
   input += synapse0x910c088();
   input += synapse0x910c0b0();
   input += synapse0x910c0d8();
   input += synapse0x910c100();
   input += synapse0x910c128();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910bd80() {
   double input = input0x910bd80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910c150() {
   double input = 0.25065;
   input += synapse0x910c368();
   input += synapse0x9103ab0();
   input += synapse0x9103ad8();
   input += synapse0x9103b00();
   input += synapse0x9103d30();
   input += synapse0x9103d58();
   input += synapse0x9103f88();
   input += synapse0x9103fb0();
   input += synapse0x91041e8();
   input += synapse0x9104210();
   input += synapse0x9104238();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910c150() {
   double input = input0x910c150();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9104468() {
   double input = 0.954758;
   input += synapse0x910d258();
   input += synapse0x910d280();
   input += synapse0x910d2a8();
   input += synapse0x910d2d0();
   input += synapse0x910d2f8();
   input += synapse0x910d320();
   input += synapse0x910d348();
   input += synapse0x910d370();
   input += synapse0x910d398();
   input += synapse0x910d3c0();
   input += synapse0x910d3e8();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9104468() {
   double input = input0x9104468();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910d410() {
   double input = -0.0429218;
   input += synapse0x910d610();
   input += synapse0x910d638();
   input += synapse0x910d660();
   input += synapse0x910d688();
   input += synapse0x910d6b0();
   input += synapse0x910d6d8();
   input += synapse0x910d700();
   input += synapse0x910d728();
   input += synapse0x910d750();
   input += synapse0x910d778();
   input += synapse0x910d7a0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910d410() {
   double input = input0x910d410();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910d7c8() {
   double input = 0.348724;
   input += synapse0x910d9e0();
   input += synapse0x910da08();
   input += synapse0x910da30();
   input += synapse0x910da58();
   input += synapse0x910da80();
   input += synapse0x910daa8();
   input += synapse0x910dad0();
   input += synapse0x910daf8();
   input += synapse0x910db20();
   input += synapse0x910db48();
   input += synapse0x910db70();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910d7c8() {
   double input = input0x910d7c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910db98() {
   double input = -0.128745;
   input += synapse0x910ddb0();
   input += synapse0x910ddd8();
   input += synapse0x910de00();
   input += synapse0x910de28();
   input += synapse0x910de50();
   input += synapse0x910de78();
   input += synapse0x910dea0();
   input += synapse0x910dec8();
   input += synapse0x910def0();
   input += synapse0x910df18();
   input += synapse0x910df40();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910db98() {
   double input = input0x910db98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910df68() {
   double input = -0.0800608;
   input += synapse0x910e180();
   input += synapse0x910e1a8();
   input += synapse0x910e1d0();
   input += synapse0x910e1f8();
   input += synapse0x910e220();
   input += synapse0x910e248();
   input += synapse0x910e270();
   input += synapse0x910e298();
   input += synapse0x910e2c0();
   input += synapse0x910e2e8();
   input += synapse0x910e310();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910df68() {
   double input = input0x910df68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910e338() {
   double input = -0.32017;
   input += synapse0x910e550();
   input += synapse0x910e578();
   input += synapse0x910e5a0();
   input += synapse0x910e5c8();
   input += synapse0x910e5f0();
   input += synapse0x910e618();
   input += synapse0x910e640();
   input += synapse0x910e668();
   input += synapse0x910e690();
   input += synapse0x910e6b8();
   input += synapse0x910e6e0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910e338() {
   double input = input0x910e338();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910e708() {
   double input = 0.370825;
   input += synapse0x910e920();
   input += synapse0x910e948();
   input += synapse0x910e970();
   input += synapse0x910e998();
   input += synapse0x910e9c0();
   input += synapse0x910e9e8();
   input += synapse0x910ea10();
   input += synapse0x910ea38();
   input += synapse0x910ea60();
   input += synapse0x910ea88();
   input += synapse0x910eab0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910e708() {
   double input = input0x910e708();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910ead8() {
   double input = -0.370273;
   input += synapse0x910ecf0();
   input += synapse0x910ed18();
   input += synapse0x910ed40();
   input += synapse0x910ed68();
   input += synapse0x910ed90();
   input += synapse0x910edb8();
   input += synapse0x910ede0();
   input += synapse0x910ee08();
   input += synapse0x910ee30();
   input += synapse0x910ee58();
   input += synapse0x910ee80();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910ead8() {
   double input = input0x910ead8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910eea8() {
   double input = -0.175382;
   input += synapse0x910f0c0();
   input += synapse0x910f0e8();
   input += synapse0x910f110();
   input += synapse0x910f138();
   input += synapse0x910f160();
   input += synapse0x910f188();
   input += synapse0x910f1b0();
   input += synapse0x910f1d8();
   input += synapse0x910f200();
   input += synapse0x910f228();
   input += synapse0x910f250();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910eea8() {
   double input = input0x910eea8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910f278() {
   double input = 0.339842;
   input += synapse0x910f490();
   input += synapse0x910f4b8();
   input += synapse0x910f4e0();
   input += synapse0x910f508();
   input += synapse0x910f530();
   input += synapse0x910f558();
   input += synapse0x910f580();
   input += synapse0x910f5a8();
   input += synapse0x910f5d0();
   input += synapse0x910f5f8();
   input += synapse0x910f620();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910f278() {
   double input = input0x910f278();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910f648() {
   double input = -0.119717;
   input += synapse0x910f860();
   input += synapse0x910f888();
   input += synapse0x910f8b0();
   input += synapse0x910f8d8();
   input += synapse0x910f900();
   input += synapse0x910f928();
   input += synapse0x910f950();
   input += synapse0x910f978();
   input += synapse0x910f9a0();
   input += synapse0x910f9c8();
   input += synapse0x910f9f0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910f648() {
   double input = input0x910f648();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910fa18() {
   double input = -0.0781162;
   input += synapse0x910fc30();
   input += synapse0x910fc58();
   input += synapse0x910fc80();
   input += synapse0x910fca8();
   input += synapse0x910fcd0();
   input += synapse0x910fcf8();
   input += synapse0x910fd20();
   input += synapse0x910fd48();
   input += synapse0x910fd70();
   input += synapse0x910fd98();
   input += synapse0x910fdc0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910fa18() {
   double input = input0x910fa18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x910fde8() {
   double input = -0.541093;
   input += synapse0x9110000();
   input += synapse0x9110028();
   input += synapse0x9110050();
   input += synapse0x9110078();
   input += synapse0x91100a0();
   input += synapse0x91100c8();
   input += synapse0x91100f0();
   input += synapse0x9110118();
   input += synapse0x9110140();
   input += synapse0x9110168();
   input += synapse0x9110190();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x910fde8() {
   double input = input0x910fde8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91101b8() {
   double input = 1.23123;
   input += synapse0x91103d0();
   input += synapse0x91103f8();
   input += synapse0x9110420();
   input += synapse0x9110448();
   input += synapse0x9110470();
   input += synapse0x9110498();
   input += synapse0x91104c0();
   input += synapse0x91104e8();
   input += synapse0x9110510();
   input += synapse0x9110538();
   input += synapse0x9110560();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91101b8() {
   double input = input0x91101b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9110588() {
   double input = -0.0648235;
   input += synapse0x91107a0();
   input += synapse0x91107c8();
   input += synapse0x91107f0();
   input += synapse0x9110818();
   input += synapse0x9110840();
   input += synapse0x9110868();
   input += synapse0x9110890();
   input += synapse0x91108b8();
   input += synapse0x91108e0();
   input += synapse0x9110908();
   input += synapse0x9110930();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9110588() {
   double input = input0x9110588();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9110958() {
   double input = 0.0128041;
   input += synapse0x9110b70();
   input += synapse0x9110b98();
   input += synapse0x9110bc0();
   input += synapse0x9110be8();
   input += synapse0x9110c10();
   input += synapse0x9110c38();
   input += synapse0x9110c60();
   input += synapse0x9110c88();
   input += synapse0x9110cb0();
   input += synapse0x9110cd8();
   input += synapse0x9110d00();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9110958() {
   double input = input0x9110958();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9110d28() {
   double input = -0.605693;
   input += synapse0x9110f40();
   input += synapse0x9110f68();
   input += synapse0x9110f90();
   input += synapse0x9110fb8();
   input += synapse0x9110fe0();
   input += synapse0x9111008();
   input += synapse0x9111030();
   input += synapse0x9111058();
   input += synapse0x9111080();
   input += synapse0x91110a8();
   input += synapse0x91110d0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9110d28() {
   double input = input0x9110d28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91110f8() {
   double input = -0.231789;
   input += synapse0x9111310();
   input += synapse0x9111338();
   input += synapse0x9111360();
   input += synapse0x9111388();
   input += synapse0x91113b0();
   input += synapse0x91113d8();
   input += synapse0x9111400();
   input += synapse0x9111428();
   input += synapse0x9111450();
   input += synapse0x9111478();
   input += synapse0x91114a0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91110f8() {
   double input = input0x91110f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91114c8() {
   double input = -0.283922;
   input += synapse0x91116e0();
   input += synapse0x9111708();
   input += synapse0x9111730();
   input += synapse0x9111758();
   input += synapse0x9111780();
   input += synapse0x91117a8();
   input += synapse0x91117d0();
   input += synapse0x91117f8();
   input += synapse0x9111820();
   input += synapse0x9111848();
   input += synapse0x9111870();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91114c8() {
   double input = input0x91114c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9111898() {
   double input = -0.0729615;
   input += synapse0x9111ab0();
   input += synapse0x9111ad8();
   input += synapse0x9111b00();
   input += synapse0x9111b28();
   input += synapse0x9111b50();
   input += synapse0x9111b78();
   input += synapse0x9111ba0();
   input += synapse0x9111bc8();
   input += synapse0x9111bf0();
   input += synapse0x9111c18();
   input += synapse0x9111c40();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9111898() {
   double input = input0x9111898();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9111c68() {
   double input = -0.470697;
   input += synapse0x9111e80();
   input += synapse0x9111ea8();
   input += synapse0x9111ed0();
   input += synapse0x9111ef8();
   input += synapse0x9111f20();
   input += synapse0x9111f48();
   input += synapse0x9111f70();
   input += synapse0x9111f98();
   input += synapse0x9111fc0();
   input += synapse0x9111fe8();
   input += synapse0x9112010();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9111c68() {
   double input = input0x9111c68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9112038() {
   double input = -0.423658;
   input += synapse0x9112250();
   input += synapse0x9112278();
   input += synapse0x91122a0();
   input += synapse0x91122c8();
   input += synapse0x91122f0();
   input += synapse0x9112318();
   input += synapse0x9112340();
   input += synapse0x9112368();
   input += synapse0x9112390();
   input += synapse0x91123b8();
   input += synapse0x91123e0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9112038() {
   double input = input0x9112038();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9112408() {
   double input = -0.535321;
   input += synapse0x9112620();
   input += synapse0x9112648();
   input += synapse0x9112670();
   input += synapse0x9112698();
   input += synapse0x91126c0();
   input += synapse0x91126e8();
   input += synapse0x9112710();
   input += synapse0x9112738();
   input += synapse0x9112760();
   input += synapse0x9112788();
   input += synapse0x91127b0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9112408() {
   double input = input0x9112408();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91127d8() {
   double input = -0.326971;
   input += synapse0x91129f0();
   input += synapse0x9112a18();
   input += synapse0x9112a40();
   input += synapse0x9112a68();
   input += synapse0x9112a90();
   input += synapse0x9112ab8();
   input += synapse0x9112ae0();
   input += synapse0x9112b08();
   input += synapse0x9112b30();
   input += synapse0x9112b58();
   input += synapse0x9112b80();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91127d8() {
   double input = input0x91127d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9112ba8() {
   double input = 0.626097;
   input += synapse0x9112dc0();
   input += synapse0x9112de8();
   input += synapse0x9112e10();
   input += synapse0x9112e38();
   input += synapse0x9112e60();
   input += synapse0x9112e88();
   input += synapse0x9112eb0();
   input += synapse0x9112ed8();
   input += synapse0x9112f00();
   input += synapse0x9112f28();
   input += synapse0x9112f50();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9112ba8() {
   double input = input0x9112ba8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9112f78() {
   double input = -0.0175184;
   input += synapse0x9113190();
   input += synapse0x91131b8();
   input += synapse0x91131e0();
   input += synapse0x9113208();
   input += synapse0x9113230();
   input += synapse0x9113258();
   input += synapse0x9113280();
   input += synapse0x91132a8();
   input += synapse0x91132d0();
   input += synapse0x91132f8();
   input += synapse0x9113320();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9112f78() {
   double input = input0x9112f78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9113348() {
   double input = 0.308519;
   input += synapse0x9113560();
   input += synapse0x9113588();
   input += synapse0x91135b0();
   input += synapse0x91135d8();
   input += synapse0x9113600();
   input += synapse0x9113628();
   input += synapse0x9113650();
   input += synapse0x9113678();
   input += synapse0x91136a0();
   input += synapse0x91136c8();
   input += synapse0x91136f0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9113348() {
   double input = input0x9113348();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9113718() {
   double input = -1.38041;
   input += synapse0x9113930();
   input += synapse0x9113958();
   input += synapse0x9113980();
   input += synapse0x91139a8();
   input += synapse0x91139d0();
   input += synapse0x91139f8();
   input += synapse0x9113a20();
   input += synapse0x9113a48();
   input += synapse0x9113a70();
   input += synapse0x9113a98();
   input += synapse0x9113ac0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9113718() {
   double input = input0x9113718();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9113ae8() {
   double input = 0.275305;
   input += synapse0x9113d00();
   input += synapse0x9113d28();
   input += synapse0x9113d50();
   input += synapse0x9113d78();
   input += synapse0x9113da0();
   input += synapse0x9113dc8();
   input += synapse0x9113df0();
   input += synapse0x9113e18();
   input += synapse0x9113e40();
   input += synapse0x9113e68();
   input += synapse0x9113e90();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9113ae8() {
   double input = input0x9113ae8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9113eb8() {
   double input = 0.110198;
   input += synapse0x91140d0();
   input += synapse0x91140f8();
   input += synapse0x9107ab0();
   input += synapse0x9107ad8();
   input += synapse0x9107b00();
   input += synapse0x9107b28();
   input += synapse0x9107b50();
   input += synapse0x9107b78();
   input += synapse0x9107ba0();
   input += synapse0x9107bc8();
   input += synapse0x9107bf0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9113eb8() {
   double input = input0x9113eb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9107c18() {
   double input = 0.63811;
   input += synapse0x9107e30();
   input += synapse0x9107e58();
   input += synapse0x9107e80();
   input += synapse0x9107ea8();
   input += synapse0x9107ed0();
   input += synapse0x9107ef8();
   input += synapse0x9107f20();
   input += synapse0x9107f48();
   input += synapse0x9107f70();
   input += synapse0x9107f98();
   input += synapse0x9107fc0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9107c18() {
   double input = input0x9107c18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9107fe8() {
   double input = -0.458139;
   input += synapse0x9108200();
   input += synapse0x9108228();
   input += synapse0x9108250();
   input += synapse0x9108278();
   input += synapse0x91082a0();
   input += synapse0x91082c8();
   input += synapse0x91082f0();
   input += synapse0x9108318();
   input += synapse0x9108340();
   input += synapse0x9108368();
   input += synapse0x9108390();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9107fe8() {
   double input = input0x9107fe8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91083b8() {
   double input = 0.138392;
   input += synapse0x91085d0();
   input += synapse0x91085f8();
   input += synapse0x9108620();
   input += synapse0x9108648();
   input += synapse0x9108670();
   input += synapse0x9108698();
   input += synapse0x91086c0();
   input += synapse0x91086e8();
   input += synapse0x9108710();
   input += synapse0x9108738();
   input += synapse0x9108760();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91083b8() {
   double input = input0x91083b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9108788() {
   double input = -0.127925;
   input += synapse0x91089a0();
   input += synapse0x91089c8();
   input += synapse0x91089f0();
   input += synapse0x9108a18();
   input += synapse0x9108a40();
   input += synapse0x9108a68();
   input += synapse0x9108a90();
   input += synapse0x9116128();
   input += synapse0x9116150();
   input += synapse0x9116178();
   input += synapse0x91161a0();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9108788() {
   double input = input0x9108788();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x91161c8() {
   double input = -0.99439;
   input += synapse0x91163e0();
   input += synapse0x9116408();
   input += synapse0x9116430();
   input += synapse0x9116458();
   input += synapse0x9116480();
   input += synapse0x91164a8();
   input += synapse0x91164d0();
   input += synapse0x91164f8();
   input += synapse0x9116520();
   input += synapse0x9116548();
   input += synapse0x9116570();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x91161c8() {
   double input = input0x91161c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9116598() {
   double input = -0.0745298;
   input += synapse0x91167b0();
   input += synapse0x91167d8();
   input += synapse0x9116800();
   input += synapse0x9116828();
   input += synapse0x9116850();
   input += synapse0x9116878();
   input += synapse0x91168a0();
   input += synapse0x91168c8();
   input += synapse0x91168f0();
   input += synapse0x9116918();
   input += synapse0x9116940();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9116598() {
   double input = input0x9116598();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::input0x9116968() {
   double input = -0.166214;
   input += synapse0x9116a90();
   input += synapse0x9116ab8();
   input += synapse0x9116ae0();
   input += synapse0x9116b08();
   input += synapse0x9116b30();
   input += synapse0x9116b58();
   input += synapse0x9116b80();
   input += synapse0x9116ba8();
   input += synapse0x9116bd0();
   input += synapse0x9116bf8();
   input += synapse0x9116c20();
   input += synapse0x9116c48();
   input += synapse0x9116c70();
   input += synapse0x9116c98();
   input += synapse0x9116cc0();
   input += synapse0x9116ce8();
   input += synapse0x9116d98();
   input += synapse0x9116dc0();
   input += synapse0x9116de8();
   input += synapse0x9116e10();
   input += synapse0x9116e38();
   input += synapse0x9116e60();
   input += synapse0x9116e88();
   input += synapse0x9116eb0();
   input += synapse0x9116ed8();
   input += synapse0x9116f00();
   input += synapse0x9116f28();
   input += synapse0x9116f50();
   input += synapse0x9116f78();
   input += synapse0x9116fa0();
   input += synapse0x9116fc8();
   input += synapse0x9116ff0();
   input += synapse0x9116d10();
   input += synapse0x9116d38();
   input += synapse0x9116d60();
   input += synapse0x9117120();
   input += synapse0x9117148();
   input += synapse0x9117170();
   input += synapse0x9117198();
   input += synapse0x91171c0();
   input += synapse0x91171e8();
   input += synapse0x9117210();
   input += synapse0x9117238();
   input += synapse0x9117260();
   input += synapse0x9117288();
   input += synapse0x91172b0();
   input += synapse0x91172d8();
   input += synapse0x9117300();
   input += synapse0x9117328();
   input += synapse0x9117350();
   input += synapse0x9117378();
   input += synapse0x91173a0();
   input += synapse0x91173c8();
   input += synapse0x91173f0();
   input += synapse0x9117418();
   input += synapse0x9117440();
   input += synapse0x9117468();
   input += synapse0x9117490();
   input += synapse0x91174b8();
   input += synapse0x91174e0();
   input += synapse0x9117508();
   input += synapse0x9117530();
   input += synapse0x9117558();
   input += synapse0x9117580();
   input += synapse0x90faa48();
   input += synapse0x9117018();
   input += synapse0x9117040();
   input += synapse0x9117068();
   input += synapse0x9117090();
   input += synapse0x91170b8();
   input += synapse0x91170e0();
   input += synapse0x91177b0();
   input += synapse0x91177d8();
   input += synapse0x9117800();
   input += synapse0x9117828();
   input += synapse0x9117850();
   input += synapse0x9117878();
   input += synapse0x91178a0();
   input += synapse0x91178c8();
   input += synapse0x91178f0();
   input += synapse0x9117918();
   input += synapse0x9117940();
   input += synapse0x9117968();
   input += synapse0x9117990();
   input += synapse0x91179b8();
   input += synapse0x91179e0();
   input += synapse0x9117a08();
   input += synapse0x9117a30();
   input += synapse0x9117a58();
   input += synapse0x9117a80();
   input += synapse0x9117aa8();
   input += synapse0x9117ad0();
   input += synapse0x9117af8();
   input += synapse0x9117b20();
   input += synapse0x9117b48();
   input += synapse0x9117b70();
   input += synapse0x9117b98();
   input += synapse0x9117bc0();
   input += synapse0x9117be8();
   input += synapse0x9117c10();
   return input;
}

double NNParaElectronXYCorrectionClusterDeltaY::neuron0x9116968() {
   double input = input0x9116968();
   return (input * 1)+0;
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x8f088f8() {
   return (neuron0x90f93a8()*0.338958);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90face8() {
   return (neuron0x90f9560()*-0.216145);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fad10() {
   return (neuron0x90f9760()*-0.519869);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fad38() {
   return (neuron0x90f9960()*0.246231);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fad60() {
   return (neuron0x90f9b78()*0.0706616);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fad88() {
   return (neuron0x90f9d90()*0.785154);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fadb0() {
   return (neuron0x90f9fa8()*0.390071);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fadd8() {
   return (neuron0x90fa1c0()*0.550287);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fae00() {
   return (neuron0x90fa3d8()*-0.0387263);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fae28() {
   return (neuron0x90fa5f0()*0.0465215);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fae50() {
   return (neuron0x90fa7f0()*-0.422341);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb078() {
   return (neuron0x90f93a8()*-0.127215);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb0a0() {
   return (neuron0x90f9560()*0.236464);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb0c8() {
   return (neuron0x90f9760()*-0.460372);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb0f0() {
   return (neuron0x90f9960()*0.503094);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb118() {
   return (neuron0x90f9b78()*0.0734881);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb140() {
   return (neuron0x90f9d90()*0.150228);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb1f0() {
   return (neuron0x90f9fa8()*-0.158656);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb218() {
   return (neuron0x90fa1c0()*-0.590998);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb240() {
   return (neuron0x90fa3d8()*-0.0782714);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb268() {
   return (neuron0x90fa5f0()*0.642621);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb290() {
   return (neuron0x90fa7f0()*0.191927);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb470() {
   return (neuron0x90f93a8()*-0.358025);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb498() {
   return (neuron0x90f9560()*0.203671);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb4c0() {
   return (neuron0x90f9760()*-0.396861);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb4e8() {
   return (neuron0x90f9960()*-0.167487);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb510() {
   return (neuron0x90f9b78()*-0.641807);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb538() {
   return (neuron0x90f9d90()*-0.0293267);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb560() {
   return (neuron0x90f9fa8()*0.905113);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb588() {
   return (neuron0x90fa1c0()*-0.279577);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb5b0() {
   return (neuron0x90fa3d8()*-0.641013);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb5d8() {
   return (neuron0x90fa5f0()*-0.0234128);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb600() {
   return (neuron0x90fa7f0()*-0.1824);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb930() {
   return (neuron0x90f93a8()*0.501518);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb958() {
   return (neuron0x90f9560()*0.125387);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb980() {
   return (neuron0x90f9760()*0.218793);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb9a8() {
   return (neuron0x90f9960()*0.449801);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb9d0() {
   return (neuron0x90f9b78()*0.0152103);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb9f8() {
   return (neuron0x90f9d90()*-0.0999342);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fba20() {
   return (neuron0x90f9fa8()*-0.490323);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fba48() {
   return (neuron0x90fa1c0()*-0.00165902);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fba70() {
   return (neuron0x90fa3d8()*0.341326);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fba98() {
   return (neuron0x90fa5f0()*0.446607);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbac0() {
   return (neuron0x90fa7f0()*-0.0286464);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbce8() {
   return (neuron0x90f93a8()*0.558369);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbd10() {
   return (neuron0x90f9560()*0.212396);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbd38() {
   return (neuron0x90f9760()*-0.778021);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbd60() {
   return (neuron0x90f9960()*-0.0478834);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbd88() {
   return (neuron0x90f9b78()*0.375444);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbdb0() {
   return (neuron0x90f9d90()*-0.132788);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbdd8() {
   return (neuron0x90f9fa8()*0.260258);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbe00() {
   return (neuron0x90fa1c0()*-0.0470171);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbe28() {
   return (neuron0x90fa3d8()*0.215556);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbe50() {
   return (neuron0x90fa5f0()*0.07613);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fbe78() {
   return (neuron0x90fa7f0()*-0.090544);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc0a0() {
   return (neuron0x90f93a8()*-0.344061);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc0c8() {
   return (neuron0x90f9560()*-0.328035);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc0f0() {
   return (neuron0x90f9760()*0.0715814);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc118() {
   return (neuron0x90f9960()*-0.0627185);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc140() {
   return (neuron0x90f9b78()*0.163181);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc168() {
   return (neuron0x90f9d90()*-0.468032);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc190() {
   return (neuron0x90f9fa8()*0.280828);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc1b8() {
   return (neuron0x90fa1c0()*0.31635);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc1e0() {
   return (neuron0x90fa3d8()*-0.120638);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc208() {
   return (neuron0x90fa5f0()*0.181539);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x8f08678() {
   return (neuron0x90fa7f0()*-0.336521);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc608() {
   return (neuron0x90f93a8()*-0.302547);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc630() {
   return (neuron0x90f9560()*0.479531);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc658() {
   return (neuron0x90f9760()*-0.400345);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc680() {
   return (neuron0x90f9960()*-0.54725);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc6a8() {
   return (neuron0x90f9b78()*-0.067096);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc6d0() {
   return (neuron0x90f9d90()*0.342596);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc6f8() {
   return (neuron0x90f9fa8()*0.215073);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc720() {
   return (neuron0x90fa1c0()*-0.326286);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc748() {
   return (neuron0x90fa3d8()*-1.25189);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc770() {
   return (neuron0x90fa5f0()*-0.331495);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc798() {
   return (neuron0x90fa7f0()*0.105743);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc9d8() {
   return (neuron0x90f93a8()*0.296421);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fca00() {
   return (neuron0x90f9560()*-0.0771529);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fca28() {
   return (neuron0x90f9760()*0.0314905);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fca50() {
   return (neuron0x90f9960()*0.310675);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fca78() {
   return (neuron0x90f9b78()*-0.00487772);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcaa0() {
   return (neuron0x90f9d90()*0.401757);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcac8() {
   return (neuron0x90f9fa8()*0.521526);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcaf0() {
   return (neuron0x90fa1c0()*-0.482213);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcb18() {
   return (neuron0x90fa3d8()*0.04983);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcb40() {
   return (neuron0x90fa5f0()*0.405306);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcb68() {
   return (neuron0x90fa7f0()*-0.124294);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcda8() {
   return (neuron0x90f93a8()*-0.0638189);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcdd0() {
   return (neuron0x90f9560()*-0.124412);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcdf8() {
   return (neuron0x90f9760()*3.57356);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fce20() {
   return (neuron0x90f9960()*-0.520372);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fce48() {
   return (neuron0x90f9b78()*1.03609);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fce70() {
   return (neuron0x90f9d90()*0.376654);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fce98() {
   return (neuron0x90f9fa8()*-1.29123);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcec0() {
   return (neuron0x90fa1c0()*-0.15023);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcee8() {
   return (neuron0x90fa3d8()*-1.17531);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcf10() {
   return (neuron0x90fa5f0()*-0.017187);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fcf38() {
   return (neuron0x90fa7f0()*0.370487);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd178() {
   return (neuron0x90f93a8()*-0.310844);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd1a0() {
   return (neuron0x90f9560()*0.295502);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd1c8() {
   return (neuron0x90f9760()*-0.457823);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd1f0() {
   return (neuron0x90f9960()*0.215688);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd218() {
   return (neuron0x90f9b78()*-0.353005);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd240() {
   return (neuron0x90f9d90()*-0.610285);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd268() {
   return (neuron0x90f9fa8()*0.0472585);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd290() {
   return (neuron0x90fa1c0()*-0.0694295);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd2b8() {
   return (neuron0x90fa3d8()*0.41973);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd2e0() {
   return (neuron0x90fa5f0()*0.0527626);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd308() {
   return (neuron0x90fa7f0()*0.38558);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd548() {
   return (neuron0x90f93a8()*-0.331239);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd570() {
   return (neuron0x90f9560()*0.38694);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd598() {
   return (neuron0x90f9760()*0.299228);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd5c0() {
   return (neuron0x90f9960()*0.120632);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd5e8() {
   return (neuron0x90f9b78()*-0.0198608);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd610() {
   return (neuron0x90f9d90()*0.288903);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd638() {
   return (neuron0x90f9fa8()*-0.395231);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd660() {
   return (neuron0x90fa1c0()*0.604743);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd688() {
   return (neuron0x90fa3d8()*0.505085);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd6b0() {
   return (neuron0x90fa5f0()*0.364384);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd6d8() {
   return (neuron0x90fa7f0()*-0.386402);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd918() {
   return (neuron0x90f93a8()*0.562805);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd940() {
   return (neuron0x90f9560()*0.634452);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd968() {
   return (neuron0x90f9760()*0.058354);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd990() {
   return (neuron0x90f9960()*-0.399587);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd9b8() {
   return (neuron0x90f9b78()*0.0540925);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fd9e0() {
   return (neuron0x90f9d90()*0.192888);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fda08() {
   return (neuron0x90f9fa8()*-0.185549);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fda30() {
   return (neuron0x90fa1c0()*0.0628435);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x8f08728() {
   return (neuron0x90fa3d8()*0.13489);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x8f08750() {
   return (neuron0x90fa5f0()*0.440169);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x8f08810() {
   return (neuron0x90fa7f0()*-0.192032);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x8f08880() {
   return (neuron0x90f93a8()*-0.25442);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x8f088a8() {
   return (neuron0x90f9560()*-0.340848);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x8f088d0() {
   return (neuron0x90f9760()*-0.0832174);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fc400() {
   return (neuron0x90f9960()*0.21608);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb628() {
   return (neuron0x90f9b78()*0.688593);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb650() {
   return (neuron0x90f9d90()*0.954227);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb678() {
   return (neuron0x90f9fa8()*-0.0604985);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb6a0() {
   return (neuron0x90fa1c0()*0.349796);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb6c8() {
   return (neuron0x90fa3d8()*0.792496);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb6f0() {
   return (neuron0x90fa5f0()*-1.05681);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90da4d8() {
   return (neuron0x90fa7f0()*0.0549569);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe060() {
   return (neuron0x90f93a8()*-0.555215);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe088() {
   return (neuron0x90f9560()*0.00743452);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe0b0() {
   return (neuron0x90f9760()*0.11624);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe0d8() {
   return (neuron0x90f9960()*-0.541087);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe100() {
   return (neuron0x90f9b78()*0.176471);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe128() {
   return (neuron0x90f9d90()*-0.172373);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe150() {
   return (neuron0x90f9fa8()*0.181972);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe178() {
   return (neuron0x90fa1c0()*0.353301);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe1a0() {
   return (neuron0x90fa3d8()*-0.0194618);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe1c8() {
   return (neuron0x90fa5f0()*-0.112648);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe1f0() {
   return (neuron0x90fa7f0()*0.268746);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe430() {
   return (neuron0x90f93a8()*-0.277384);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe458() {
   return (neuron0x90f9560()*-0.125759);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe480() {
   return (neuron0x90f9760()*0.856807);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe4a8() {
   return (neuron0x90f9960()*-0.452509);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe4d0() {
   return (neuron0x90f9b78()*0.738855);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe4f8() {
   return (neuron0x90f9d90()*-0.393169);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe520() {
   return (neuron0x90f9fa8()*0.470607);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe548() {
   return (neuron0x90fa1c0()*0.178075);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe570() {
   return (neuron0x90fa3d8()*-0.361184);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe598() {
   return (neuron0x90fa5f0()*0.131883);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe5c0() {
   return (neuron0x90fa7f0()*-0.273008);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe800() {
   return (neuron0x90f93a8()*-0.0262639);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe8b0() {
   return (neuron0x90f9560()*0.438929);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe960() {
   return (neuron0x90f9760()*0.464526);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fea10() {
   return (neuron0x90f9960()*0.516884);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90feac0() {
   return (neuron0x90f9b78()*0.156705);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90feb70() {
   return (neuron0x90f9d90()*0.553136);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fec20() {
   return (neuron0x90f9fa8()*0.179583);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fecd0() {
   return (neuron0x90fa1c0()*-0.299463);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fed80() {
   return (neuron0x90fa3d8()*-0.124831);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fee30() {
   return (neuron0x90fa5f0()*-0.246977);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90feee0() {
   return (neuron0x90fa7f0()*-0.203401);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90ff0d0() {
   return (neuron0x90f93a8()*0.205696);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90ff0f8() {
   return (neuron0x90f9560()*-0.0384706);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90ff120() {
   return (neuron0x90f9760()*-1.35454);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90ff148() {
   return (neuron0x90f9960()*0.735223);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90da500() {
   return (neuron0x90f9b78()*-0.837947);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90da528() {
   return (neuron0x90f9d90()*0.255697);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90da550() {
   return (neuron0x90f9fa8()*-0.743684);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90da578() {
   return (neuron0x90fa1c0()*0.287593);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90da5a0() {
   return (neuron0x90fa3d8()*2.41648);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100120() {
   return (neuron0x90fa5f0()*-0.53672);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100148() {
   return (neuron0x90fa7f0()*0.299811);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100298() {
   return (neuron0x90f93a8()*-0.179986);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91002c0() {
   return (neuron0x90f9560()*-0.0715998);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91002e8() {
   return (neuron0x90f9760()*1.38554);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100310() {
   return (neuron0x90f9960()*-0.0652816);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100338() {
   return (neuron0x90f9b78()*-0.507489);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100360() {
   return (neuron0x90f9d90()*0.15551);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100388() {
   return (neuron0x90f9fa8()*0.131524);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91003b0() {
   return (neuron0x90fa1c0()*-0.319919);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91003d8() {
   return (neuron0x90fa3d8()*-1.32634);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100400() {
   return (neuron0x90fa5f0()*-0.0077412);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100428() {
   return (neuron0x90fa7f0()*-0.0930471);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100578() {
   return (neuron0x90f93a8()*0.0615824);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91005a0() {
   return (neuron0x90f9560()*-0.030289);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91005c8() {
   return (neuron0x90f9760()*0.285409);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91005f0() {
   return (neuron0x90f9960()*-0.185962);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100618() {
   return (neuron0x90f9b78()*0.105133);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100640() {
   return (neuron0x90f9d90()*0.238781);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100668() {
   return (neuron0x90f9fa8()*-0.349668);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100690() {
   return (neuron0x90fa1c0()*-0.0200858);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91006b8() {
   return (neuron0x90fa3d8()*0.310378);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91006e0() {
   return (neuron0x90fa5f0()*-0.506209);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100708() {
   return (neuron0x90fa7f0()*0.155217);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91008a0() {
   return (neuron0x90f93a8()*0.352411);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91008c8() {
   return (neuron0x90f9560()*-0.0170586);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91008f0() {
   return (neuron0x90f9760()*1.21626);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100918() {
   return (neuron0x90f9960()*-0.26684);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100940() {
   return (neuron0x90f9b78()*-0.282028);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100968() {
   return (neuron0x90f9d90()*0.560238);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100990() {
   return (neuron0x90f9fa8()*-0.425674);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91009b8() {
   return (neuron0x90fa1c0()*-0.453562);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91009e0() {
   return (neuron0x90fa3d8()*0.255113);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100a08() {
   return (neuron0x90fa5f0()*0.717636);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100a30() {
   return (neuron0x90fa7f0()*0.0749907);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100c58() {
   return (neuron0x90f93a8()*-0.145694);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100c80() {
   return (neuron0x90f9560()*-0.273317);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100ca8() {
   return (neuron0x90f9760()*0.423914);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100cd0() {
   return (neuron0x90f9960()*-0.495972);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100cf8() {
   return (neuron0x90f9b78()*-0.549953);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100d20() {
   return (neuron0x90f9d90()*-0.350705);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100d48() {
   return (neuron0x90f9fa8()*-0.0584611);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100d70() {
   return (neuron0x90fa1c0()*-0.318719);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100d98() {
   return (neuron0x90fa3d8()*-0.0585971);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100dc0() {
   return (neuron0x90fa5f0()*-0.261703);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9100de8() {
   return (neuron0x90fa7f0()*0.190207);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb168() {
   return (neuron0x90f93a8()*-0.397255);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb190() {
   return (neuron0x90f9560()*-0.100902);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fb1b8() {
   return (neuron0x90f9760()*0.492591);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101118() {
   return (neuron0x90f9960()*0.0706708);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101140() {
   return (neuron0x90f9b78()*0.368901);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101168() {
   return (neuron0x90f9d90()*-0.0902742);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101190() {
   return (neuron0x90f9fa8()*-0.531473);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91011b8() {
   return (neuron0x90fa1c0()*0.143918);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91011e0() {
   return (neuron0x90fa3d8()*-0.320462);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101208() {
   return (neuron0x90fa5f0()*-0.599641);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101230() {
   return (neuron0x90fa7f0()*0.250953);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101458() {
   return (neuron0x90f93a8()*0.163307);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101480() {
   return (neuron0x90f9560()*-0.212478);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91014a8() {
   return (neuron0x90f9760()*-1.63351);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91014d0() {
   return (neuron0x90f9960()*0.466034);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91014f8() {
   return (neuron0x90f9b78()*-0.423414);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101520() {
   return (neuron0x90f9d90()*0.22866);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101548() {
   return (neuron0x90f9fa8()*1.14579);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101570() {
   return (neuron0x90fa1c0()*0.416198);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101598() {
   return (neuron0x90fa3d8()*-1.38508);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91015c0() {
   return (neuron0x90fa5f0()*0.0938057);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91015e8() {
   return (neuron0x90fa7f0()*-0.0655598);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101810() {
   return (neuron0x90f93a8()*0.516814);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101838() {
   return (neuron0x90f9560()*0.038587);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101860() {
   return (neuron0x90f9760()*-0.625462);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101888() {
   return (neuron0x90f9960()*-0.479989);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fda58() {
   return (neuron0x90f9b78()*-0.310989);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fda80() {
   return (neuron0x90f9d90()*-0.171516);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fdaa8() {
   return (neuron0x90f9fa8()*0.288564);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fdad0() {
   return (neuron0x90fa1c0()*-0.429901);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fdaf8() {
   return (neuron0x90fa3d8()*-0.207603);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fdb20() {
   return (neuron0x90fa5f0()*0.356441);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fdb48() {
   return (neuron0x90fa7f0()*0.151535);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fdd88() {
   return (neuron0x90f93a8()*0.0265862);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fddb0() {
   return (neuron0x90f9560()*0.0798719);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fddd8() {
   return (neuron0x90f9760()*0.0843688);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fde00() {
   return (neuron0x90f9960()*-0.157866);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fde28() {
   return (neuron0x90f9b78()*0.31969);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91020b8() {
   return (neuron0x90f9d90()*0.135884);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91020e0() {
   return (neuron0x90f9fa8()*0.0750579);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102108() {
   return (neuron0x90fa1c0()*-0.466385);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102130() {
   return (neuron0x90fa3d8()*-0.611292);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102158() {
   return (neuron0x90fa5f0()*0.0928569);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102180() {
   return (neuron0x90fa7f0()*-0.393229);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91023a8() {
   return (neuron0x90f93a8()*0.186503);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91023d0() {
   return (neuron0x90f9560()*-0.251555);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91023f8() {
   return (neuron0x90f9760()*-0.769826);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102420() {
   return (neuron0x90f9960()*-0.489006);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102448() {
   return (neuron0x90f9b78()*-0.545305);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102470() {
   return (neuron0x90f9d90()*-0.0552897);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102498() {
   return (neuron0x90f9fa8()*0.654522);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91024c0() {
   return (neuron0x90fa1c0()*-0.695834);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91024e8() {
   return (neuron0x90fa3d8()*-0.872881);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102510() {
   return (neuron0x90fa5f0()*-0.122532);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102538() {
   return (neuron0x90fa7f0()*0.157106);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102778() {
   return (neuron0x90f93a8()*0.399676);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91027a0() {
   return (neuron0x90f9560()*-0.665934);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91027c8() {
   return (neuron0x90f9760()*-0.774547);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91027f0() {
   return (neuron0x90f9960()*0.263325);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102818() {
   return (neuron0x90f9b78()*0.0807959);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102840() {
   return (neuron0x90f9d90()*-0.38544);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102868() {
   return (neuron0x90f9fa8()*-0.663078);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102890() {
   return (neuron0x90fa1c0()*-0.360353);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91028b8() {
   return (neuron0x90fa3d8()*0.20047);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91028e0() {
   return (neuron0x90fa5f0()*-0.0429969);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102908() {
   return (neuron0x90fa7f0()*-0.00212679);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102b48() {
   return (neuron0x90f93a8()*0.252465);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102b70() {
   return (neuron0x90f9560()*0.0345539);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102b98() {
   return (neuron0x90f9760()*-0.854681);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102bc0() {
   return (neuron0x90f9960()*0.0388603);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102be8() {
   return (neuron0x90f9b78()*0.951755);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102c10() {
   return (neuron0x90f9d90()*0.286491);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102c38() {
   return (neuron0x90f9fa8()*0.75231);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102c60() {
   return (neuron0x90fa1c0()*-0.124305);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102c88() {
   return (neuron0x90fa3d8()*0.948285);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102cb0() {
   return (neuron0x90fa5f0()*-0.15483);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102cd8() {
   return (neuron0x90fa7f0()*0.0376199);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102f18() {
   return (neuron0x90f93a8()*-0.20716);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102f40() {
   return (neuron0x90f9560()*0.209266);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102f68() {
   return (neuron0x90f9760()*-1.76354);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102f90() {
   return (neuron0x90f9960()*-0.302803);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102fb8() {
   return (neuron0x90f9b78()*-0.65798);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102fe0() {
   return (neuron0x90f9d90()*-0.0283867);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103008() {
   return (neuron0x90f9fa8()*-0.518312);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103030() {
   return (neuron0x90fa1c0()*-0.225029);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103058() {
   return (neuron0x90fa3d8()*0.712774);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103080() {
   return (neuron0x90fa5f0()*0.363998);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91030a8() {
   return (neuron0x90fa7f0()*0.252523);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91032e8() {
   return (neuron0x90f93a8()*-0.226537);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103310() {
   return (neuron0x90f9560()*-0.0845455);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103338() {
   return (neuron0x90f9760()*-0.546899);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103360() {
   return (neuron0x90f9960()*0.16952);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103388() {
   return (neuron0x90f9b78()*0.0571321);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91033b0() {
   return (neuron0x90f9d90()*-0.111397);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91033d8() {
   return (neuron0x90f9fa8()*0.631395);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103400() {
   return (neuron0x90fa1c0()*-0.0016179);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103428() {
   return (neuron0x90fa3d8()*0.257843);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103450() {
   return (neuron0x90fa5f0()*0.337866);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103478() {
   return (neuron0x90fa7f0()*-0.353665);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91036b8() {
   return (neuron0x90f93a8()*-0.049898);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91036e0() {
   return (neuron0x90f9560()*0.00181811);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103708() {
   return (neuron0x90f9760()*0.0569722);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103730() {
   return (neuron0x90f9960()*-0.0366664);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103758() {
   return (neuron0x90f9b78()*0.0518058);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103780() {
   return (neuron0x90f9d90()*0.166736);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91037a8() {
   return (neuron0x90f9fa8()*0.125584);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91037d0() {
   return (neuron0x90fa1c0()*0.509133);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91037f8() {
   return (neuron0x90fa3d8()*0.249976);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103820() {
   return (neuron0x90fa5f0()*0.400048);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103848() {
   return (neuron0x90fa7f0()*-0.236776);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103a88() {
   return (neuron0x90f93a8()*-0.401075);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe828() {
   return (neuron0x90f9560()*0.328293);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe850() {
   return (neuron0x90f9760()*-0.199539);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe878() {
   return (neuron0x90f9960()*0.136956);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe8d8() {
   return (neuron0x90f9b78()*0.366825);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe900() {
   return (neuron0x90f9d90()*0.317033);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe928() {
   return (neuron0x90f9fa8()*-0.104821);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe988() {
   return (neuron0x90fa1c0()*0.239008);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe9b0() {
   return (neuron0x90fa3d8()*0.0798375);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fe9d8() {
   return (neuron0x90fa5f0()*-0.0771472);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fea38() {
   return (neuron0x90fa7f0()*0.43494);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90febe0() {
   return (neuron0x90f93a8()*0.363558);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90feb30() {
   return (neuron0x90f9560()*0.0609467);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fec48() {
   return (neuron0x90f9760()*-0.670199);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fec70() {
   return (neuron0x90f9960()*-0.270411);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fec98() {
   return (neuron0x90f9b78()*-0.247206);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fecf8() {
   return (neuron0x90f9d90()*-0.129308);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fed20() {
   return (neuron0x90f9fa8()*-0.173488);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fed48() {
   return (neuron0x90fa1c0()*0.150518);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90feda8() {
   return (neuron0x90fa3d8()*0.200868);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fedd0() {
   return (neuron0x90fa5f0()*0.526043);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fedf8() {
   return (neuron0x90fa7f0()*-0.446273);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90feea0() {
   return (neuron0x90f93a8()*0.134832);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90fef50() {
   return (neuron0x90f9560()*0.165865);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91048a0() {
   return (neuron0x90f9760()*-0.0636423);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91048c8() {
   return (neuron0x90f9960()*-0.366308);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91048f0() {
   return (neuron0x90f9b78()*-0.0527015);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104918() {
   return (neuron0x90f9d90()*0.373002);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104940() {
   return (neuron0x90f9fa8()*-0.0777423);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104968() {
   return (neuron0x90fa1c0()*-0.389534);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104990() {
   return (neuron0x90fa3d8()*0.550929);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91049b8() {
   return (neuron0x90fa5f0()*0.281229);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91049e0() {
   return (neuron0x90fa7f0()*0.145186);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104c08() {
   return (neuron0x90f93a8()*-0.487865);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104c30() {
   return (neuron0x90f9560()*0.342902);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104c58() {
   return (neuron0x90f9760()*0.142391);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104c80() {
   return (neuron0x90f9960()*-0.504485);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104ca8() {
   return (neuron0x90f9b78()*-0.531458);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104cd0() {
   return (neuron0x90f9d90()*0.431354);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104cf8() {
   return (neuron0x90f9fa8()*0.719216);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104d20() {
   return (neuron0x90fa1c0()*0.0554574);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104d48() {
   return (neuron0x90fa3d8()*0.550205);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104d70() {
   return (neuron0x90fa5f0()*-0.178773);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104d98() {
   return (neuron0x90fa7f0()*0.829317);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104fc0() {
   return (neuron0x90f93a8()*-0.228989);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104fe8() {
   return (neuron0x90f9560()*-0.0223198);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105010() {
   return (neuron0x90f9760()*-1.00476);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105038() {
   return (neuron0x90f9960()*-1.00179);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105060() {
   return (neuron0x90f9b78()*0.00171698);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105088() {
   return (neuron0x90f9d90()*-0.3448);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91050b0() {
   return (neuron0x90f9fa8()*0.296781);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91050d8() {
   return (neuron0x90fa1c0()*-0.752353);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105100() {
   return (neuron0x90fa3d8()*-0.386449);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105128() {
   return (neuron0x90fa5f0()*0.302021);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105150() {
   return (neuron0x90fa7f0()*0.0532162);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105378() {
   return (neuron0x90f93a8()*-0.27415);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91053a0() {
   return (neuron0x90f9560()*-0.849754);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91053c8() {
   return (neuron0x90f9760()*-0.113006);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91053f0() {
   return (neuron0x90f9960()*-0.0704547);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105418() {
   return (neuron0x90f9b78()*0.0919507);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105440() {
   return (neuron0x90f9d90()*-0.656512);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105468() {
   return (neuron0x90f9fa8()*-0.398734);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105490() {
   return (neuron0x90fa1c0()*-0.0577379);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91054b8() {
   return (neuron0x90fa3d8()*0.403386);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91054e0() {
   return (neuron0x90fa5f0()*-0.253843);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105508() {
   return (neuron0x90fa7f0()*0.0173765);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105748() {
   return (neuron0x90f93a8()*-0.276425);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105770() {
   return (neuron0x90f9560()*-0.0254677);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105798() {
   return (neuron0x90f9760()*0.390959);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91057c0() {
   return (neuron0x90f9960()*-0.0595512);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91057e8() {
   return (neuron0x90f9b78()*0.389883);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105810() {
   return (neuron0x90f9d90()*-0.0211242);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105838() {
   return (neuron0x90f9fa8()*0.0207131);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105860() {
   return (neuron0x90fa1c0()*-0.392332);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105888() {
   return (neuron0x90fa3d8()*0.772898);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91058b0() {
   return (neuron0x90fa5f0()*0.0425423);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91058d8() {
   return (neuron0x90fa7f0()*-0.219415);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105b18() {
   return (neuron0x90f93a8()*-0.186789);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105b40() {
   return (neuron0x90f9560()*0.288471);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105b68() {
   return (neuron0x90f9760()*0.107119);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105b90() {
   return (neuron0x90f9960()*-0.0426175);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105bb8() {
   return (neuron0x90f9b78()*-0.0315886);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105be0() {
   return (neuron0x90f9d90()*0.0686144);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105c08() {
   return (neuron0x90f9fa8()*-0.537916);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105c30() {
   return (neuron0x90fa1c0()*-0.221535);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105c58() {
   return (neuron0x90fa3d8()*0.474615);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105c80() {
   return (neuron0x90fa5f0()*-0.363953);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105ca8() {
   return (neuron0x90fa7f0()*-0.623595);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105ee8() {
   return (neuron0x90f93a8()*0.121906);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105f10() {
   return (neuron0x90f9560()*0.0097863);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105f38() {
   return (neuron0x90f9760()*-0.705346);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105f60() {
   return (neuron0x90f9960()*0.130043);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105f88() {
   return (neuron0x90f9b78()*1.58154);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105fb0() {
   return (neuron0x90f9d90()*-0.184693);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9105fd8() {
   return (neuron0x90f9fa8()*-1.93111);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106000() {
   return (neuron0x90fa1c0()*0.00349807);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106028() {
   return (neuron0x90fa3d8()*-0.0907214);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106050() {
   return (neuron0x90fa5f0()*0.110705);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106078() {
   return (neuron0x90fa7f0()*-0.0249454);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91062b8() {
   return (neuron0x90f93a8()*-1.14318);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91062e0() {
   return (neuron0x90f9560()*-0.0342894);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106308() {
   return (neuron0x90f9760()*-0.665982);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106330() {
   return (neuron0x90f9960()*0.78901);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106358() {
   return (neuron0x90f9b78()*-0.312484);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106380() {
   return (neuron0x90f9d90()*-0.371871);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91063a8() {
   return (neuron0x90f9fa8()*0.00464919);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91063d0() {
   return (neuron0x90fa1c0()*-0.972173);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91063f8() {
   return (neuron0x90fa3d8()*-0.448451);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106420() {
   return (neuron0x90fa5f0()*0.179238);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106448() {
   return (neuron0x90fa7f0()*-0.0482142);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106688() {
   return (neuron0x90f93a8()*0.704904);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91066b0() {
   return (neuron0x90f9560()*-0.31122);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91066d8() {
   return (neuron0x90f9760()*-0.0935892);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106700() {
   return (neuron0x90f9960()*0.290361);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106728() {
   return (neuron0x90f9b78()*-0.0266359);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106750() {
   return (neuron0x90f9d90()*-0.287442);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106778() {
   return (neuron0x90f9fa8()*-0.2602);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91067a0() {
   return (neuron0x90fa1c0()*0.240187);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91067c8() {
   return (neuron0x90fa3d8()*0.353264);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91067f0() {
   return (neuron0x90fa5f0()*0.136615);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106818() {
   return (neuron0x90fa7f0()*-0.497534);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106a58() {
   return (neuron0x90f93a8()*-0.55283);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106a80() {
   return (neuron0x90f9560()*0.62478);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106aa8() {
   return (neuron0x90f9760()*-0.311555);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106ad0() {
   return (neuron0x90f9960()*0.336136);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106af8() {
   return (neuron0x90f9b78()*-0.433284);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106b20() {
   return (neuron0x90f9d90()*0.706344);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106b48() {
   return (neuron0x90f9fa8()*0.257203);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106b70() {
   return (neuron0x90fa1c0()*-0.409059);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106b98() {
   return (neuron0x90fa3d8()*0.241505);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106bc0() {
   return (neuron0x90fa5f0()*0.443959);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106be8() {
   return (neuron0x90fa7f0()*0.141525);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106e28() {
   return (neuron0x90f93a8()*-0.5425);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106e50() {
   return (neuron0x90f9560()*0.498587);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106e78() {
   return (neuron0x90f9760()*0.696114);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106ea0() {
   return (neuron0x90f9960()*0.207491);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106ec8() {
   return (neuron0x90f9b78()*-0.886408);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106ef0() {
   return (neuron0x90f9d90()*-0.0816107);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106f18() {
   return (neuron0x90f9fa8()*0.598707);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106f40() {
   return (neuron0x90fa1c0()*0.318995);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106f68() {
   return (neuron0x90fa3d8()*-0.0252706);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106f90() {
   return (neuron0x90fa5f0()*-0.749952);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9106fb8() {
   return (neuron0x90fa7f0()*0.430636);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91071f8() {
   return (neuron0x90f93a8()*-0.402987);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107220() {
   return (neuron0x90f9560()*0.265201);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107248() {
   return (neuron0x90f9760()*0.163014);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107270() {
   return (neuron0x90f9960()*-0.141517);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107298() {
   return (neuron0x90f9b78()*0.503161);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91072c0() {
   return (neuron0x90f9d90()*0.248408);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91072e8() {
   return (neuron0x90f9fa8()*0.748849);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107310() {
   return (neuron0x90fa1c0()*-0.337049);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107338() {
   return (neuron0x90fa3d8()*0.243091);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107360() {
   return (neuron0x90fa5f0()*-0.589801);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107388() {
   return (neuron0x90fa7f0()*0.0691255);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91075c8() {
   return (neuron0x90f93a8()*0.438367);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91075f0() {
   return (neuron0x90f9560()*0.588516);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107618() {
   return (neuron0x90f9760()*-0.0578223);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107640() {
   return (neuron0x90f9960()*0.00586149);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107668() {
   return (neuron0x90f9b78()*-0.96845);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107690() {
   return (neuron0x90f9d90()*0.390739);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91076b8() {
   return (neuron0x90f9fa8()*-0.767819);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91076e0() {
   return (neuron0x90fa1c0()*-0.103867);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107708() {
   return (neuron0x90fa3d8()*0.307344);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107730() {
   return (neuron0x90fa5f0()*-0.659702);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107758() {
   return (neuron0x90fa7f0()*0.358941);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107998() {
   return (neuron0x90f93a8()*-0.387712);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91079c0() {
   return (neuron0x90f9560()*-0.2226);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91079e8() {
   return (neuron0x90f9760()*1.85729);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107a10() {
   return (neuron0x90f9960()*0.0277031);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107a38() {
   return (neuron0x90f9b78()*-1.41406);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107a60() {
   return (neuron0x90f9d90()*-0.177165);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107a88() {
   return (neuron0x90f9fa8()*-1.13779);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91018b0() {
   return (neuron0x90fa1c0()*-0.00302459);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91018d8() {
   return (neuron0x90fa3d8()*0.729464);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101900() {
   return (neuron0x90fa5f0()*0.0640712);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101928() {
   return (neuron0x90fa7f0()*-0.091154);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101b68() {
   return (neuron0x90f93a8()*-0.15703);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101b90() {
   return (neuron0x90f9560()*0.45208);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101bb8() {
   return (neuron0x90f9760()*0.168917);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101be0() {
   return (neuron0x90f9960()*-0.18657);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101c08() {
   return (neuron0x90f9b78()*0.126393);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101c30() {
   return (neuron0x90f9d90()*-0.236771);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101c58() {
   return (neuron0x90f9fa8()*-0.170582);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101c80() {
   return (neuron0x90fa1c0()*0.546916);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101ca8() {
   return (neuron0x90fa3d8()*-1.2536);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101cd0() {
   return (neuron0x90fa5f0()*-0.617606);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101cf8() {
   return (neuron0x90fa7f0()*-0.456135);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101f38() {
   return (neuron0x90f93a8()*-0.378028);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101f60() {
   return (neuron0x90f9560()*-0.444609);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101f88() {
   return (neuron0x90f9760()*-0.218979);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101fb0() {
   return (neuron0x90f9960()*0.632944);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101fd8() {
   return (neuron0x90f9b78()*0.212199);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102000() {
   return (neuron0x90f9d90()*0.404039);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102028() {
   return (neuron0x90f9fa8()*-0.405994);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102050() {
   return (neuron0x90fa1c0()*-0.674279);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9102078() {
   return (neuron0x90fa3d8()*0.0325138);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108ab8() {
   return (neuron0x90fa5f0()*-0.402287);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108ae0() {
   return (neuron0x90fa7f0()*-0.462046);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108d08() {
   return (neuron0x90f93a8()*-0.44586);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108d30() {
   return (neuron0x90f9560()*0.317479);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108d58() {
   return (neuron0x90f9760()*-0.180189);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108d80() {
   return (neuron0x90f9960()*0.167905);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108da8() {
   return (neuron0x90f9b78()*0.115896);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108dd0() {
   return (neuron0x90f9d90()*-0.154423);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108df8() {
   return (neuron0x90f9fa8()*0.0488132);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108e20() {
   return (neuron0x90fa1c0()*0.168078);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108e48() {
   return (neuron0x90fa3d8()*-0.392437);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108e70() {
   return (neuron0x90fa5f0()*0.200107);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108e98() {
   return (neuron0x90fa7f0()*-0.154923);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91090d8() {
   return (neuron0x90f93a8()*0.0540138);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109100() {
   return (neuron0x90f9560()*0.232406);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109128() {
   return (neuron0x90f9760()*-0.370484);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109150() {
   return (neuron0x90f9960()*0.186286);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109178() {
   return (neuron0x90f9b78()*-0.419604);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91091a0() {
   return (neuron0x90f9d90()*-0.0855804);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91091c8() {
   return (neuron0x90f9fa8()*0.617997);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91091f0() {
   return (neuron0x90fa1c0()*-0.110396);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109218() {
   return (neuron0x90fa3d8()*0.113914);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109240() {
   return (neuron0x90fa5f0()*-0.138515);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109268() {
   return (neuron0x90fa7f0()*0.185843);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91094a8() {
   return (neuron0x90f93a8()*0.185137);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91094d0() {
   return (neuron0x90f9560()*0.191004);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91094f8() {
   return (neuron0x90f9760()*-0.169992);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109520() {
   return (neuron0x90f9960()*-0.245582);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109548() {
   return (neuron0x90f9b78()*0.527456);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109570() {
   return (neuron0x90f9d90()*0.143607);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109598() {
   return (neuron0x90f9fa8()*-0.102691);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91095c0() {
   return (neuron0x90fa1c0()*0.088224);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91095e8() {
   return (neuron0x90fa3d8()*-0.255056);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109610() {
   return (neuron0x90fa5f0()*-0.0234841);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109638() {
   return (neuron0x90fa7f0()*-0.0509069);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109878() {
   return (neuron0x90f93a8()*0.168779);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91098a0() {
   return (neuron0x90f9560()*-0.103143);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91098c8() {
   return (neuron0x90f9760()*0.621943);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91098f0() {
   return (neuron0x90f9960()*-0.0847235);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109918() {
   return (neuron0x90f9b78()*-1.24379);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109940() {
   return (neuron0x90f9d90()*0.020625);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109968() {
   return (neuron0x90f9fa8()*0.119181);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109990() {
   return (neuron0x90fa1c0()*-0.102813);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91099b8() {
   return (neuron0x90fa3d8()*0.152361);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91099e0() {
   return (neuron0x90fa5f0()*0.0564903);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109a08() {
   return (neuron0x90fa7f0()*-0.538531);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101010() {
   return (neuron0x90f93a8()*-0.351084);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101038() {
   return (neuron0x90f9560()*0.216932);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101060() {
   return (neuron0x90f9760()*0.457361);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9101088() {
   return (neuron0x90f9960()*-0.53236);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91010b0() {
   return (neuron0x90f9b78()*-0.130431);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91010d8() {
   return (neuron0x90f9d90()*-0.521036);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109e50() {
   return (neuron0x90f9fa8()*0.645174);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109e78() {
   return (neuron0x90fa1c0()*0.272134);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109ea0() {
   return (neuron0x90fa3d8()*-0.347956);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109ec8() {
   return (neuron0x90fa5f0()*0.178293);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9109ef0() {
   return (neuron0x90fa7f0()*0.118173);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a118() {
   return (neuron0x90f93a8()*-0.979113);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a140() {
   return (neuron0x90f9560()*0.175467);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a168() {
   return (neuron0x90f9760()*0.458387);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a190() {
   return (neuron0x90f9960()*0.32042);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a1b8() {
   return (neuron0x90f9b78()*0.31284);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a1e0() {
   return (neuron0x90f9d90()*0.0264903);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a208() {
   return (neuron0x90f9fa8()*-0.503505);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a230() {
   return (neuron0x90fa1c0()*-0.396643);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a258() {
   return (neuron0x90fa3d8()*-0.272023);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a280() {
   return (neuron0x90fa5f0()*0.218002);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a2a8() {
   return (neuron0x90fa7f0()*0.628762);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a4e8() {
   return (neuron0x90f93a8()*0.873514);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a510() {
   return (neuron0x90f9560()*0.317561);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a538() {
   return (neuron0x90f9760()*-0.974565);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a560() {
   return (neuron0x90f9960()*-0.285802);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a588() {
   return (neuron0x90f9b78()*0.66478);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a5b0() {
   return (neuron0x90f9d90()*0.275609);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a5d8() {
   return (neuron0x90f9fa8()*1.53495);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a600() {
   return (neuron0x90fa1c0()*0.00892129);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a628() {
   return (neuron0x90fa3d8()*-1.36503);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a650() {
   return (neuron0x90fa5f0()*0.168165);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a678() {
   return (neuron0x90fa7f0()*-0.400083);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a8b8() {
   return (neuron0x90f93a8()*0.570963);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a8e0() {
   return (neuron0x90f9560()*0.148523);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a908() {
   return (neuron0x90f9760()*-0.681844);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a930() {
   return (neuron0x90f9960()*-0.0124585);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a958() {
   return (neuron0x90f9b78()*-0.37322);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a980() {
   return (neuron0x90f9d90()*-0.0964165);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a9a8() {
   return (neuron0x90f9fa8()*2.92369);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a9d0() {
   return (neuron0x90fa1c0()*0.152994);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910a9f8() {
   return (neuron0x90fa3d8()*0.722691);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910aa20() {
   return (neuron0x90fa5f0()*-0.151758);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910aa48() {
   return (neuron0x90fa7f0()*0.017253);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ac88() {
   return (neuron0x90f93a8()*-0.00956847);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910acb0() {
   return (neuron0x90f9560()*0.0316783);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910acd8() {
   return (neuron0x90f9760()*0.119182);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ad00() {
   return (neuron0x90f9960()*-0.509893);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ad28() {
   return (neuron0x90f9b78()*-0.429418);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ad50() {
   return (neuron0x90f9d90()*0.28451);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ad78() {
   return (neuron0x90f9fa8()*-0.200873);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ada0() {
   return (neuron0x90fa1c0()*0.119443);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910adc8() {
   return (neuron0x90fa3d8()*0.185395);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910adf0() {
   return (neuron0x90fa5f0()*0.105966);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ae18() {
   return (neuron0x90fa7f0()*0.159538);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b058() {
   return (neuron0x90f93a8()*0.165879);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b080() {
   return (neuron0x90f9560()*-0.24832);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b0a8() {
   return (neuron0x90f9760()*-0.333546);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b0d0() {
   return (neuron0x90f9960()*-0.124669);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b0f8() {
   return (neuron0x90f9b78()*0.101013);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b120() {
   return (neuron0x90f9d90()*-0.164211);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b148() {
   return (neuron0x90f9fa8()*0.396848);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b170() {
   return (neuron0x90fa1c0()*-0.0558049);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b198() {
   return (neuron0x90fa3d8()*0.400757);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b1c0() {
   return (neuron0x90fa5f0()*0.0511805);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b1e8() {
   return (neuron0x90fa7f0()*-0.0549291);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b428() {
   return (neuron0x90f93a8()*0.194586);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b450() {
   return (neuron0x90f9560()*0.131532);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b478() {
   return (neuron0x90f9760()*-0.184521);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b4a0() {
   return (neuron0x90f9960()*-0.436286);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b4c8() {
   return (neuron0x90f9b78()*-0.0247685);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b4f0() {
   return (neuron0x90f9d90()*0.251);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b518() {
   return (neuron0x90f9fa8()*0.0237074);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b540() {
   return (neuron0x90fa1c0()*-0.262992);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b568() {
   return (neuron0x90fa3d8()*-0.32706);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b590() {
   return (neuron0x90fa5f0()*-0.433884);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b5b8() {
   return (neuron0x90fa7f0()*-0.365291);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b7f8() {
   return (neuron0x90f93a8()*0.26757);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b820() {
   return (neuron0x90f9560()*-0.0792219);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b848() {
   return (neuron0x90f9760()*-0.984437);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b870() {
   return (neuron0x90f9960()*-0.0362021);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b898() {
   return (neuron0x90f9b78()*-0.0972481);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b8c0() {
   return (neuron0x90f9d90()*0.313878);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b8e8() {
   return (neuron0x90f9fa8()*-0.186639);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b910() {
   return (neuron0x90fa1c0()*-0.0873647);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b938() {
   return (neuron0x90fa3d8()*1.05049);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b960() {
   return (neuron0x90fa5f0()*-0.0604265);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910b988() {
   return (neuron0x90fa7f0()*-0.103173);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bbc8() {
   return (neuron0x90f93a8()*0.224845);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bbf0() {
   return (neuron0x90f9560()*-0.0303974);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bc18() {
   return (neuron0x90f9760()*-0.448568);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bc40() {
   return (neuron0x90f9960()*0.261937);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bc68() {
   return (neuron0x90f9b78()*-0.660419);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bc90() {
   return (neuron0x90f9d90()*-1.31526);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bcb8() {
   return (neuron0x90f9fa8()*0.628998);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bce0() {
   return (neuron0x90fa1c0()*-0.279246);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bd08() {
   return (neuron0x90fa3d8()*0.32611);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bd30() {
   return (neuron0x90fa5f0()*0.21877);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bd58() {
   return (neuron0x90fa7f0()*-0.0458307);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bf98() {
   return (neuron0x90f93a8()*-0.162288);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bfc0() {
   return (neuron0x90f9560()*-0.0318983);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910bfe8() {
   return (neuron0x90f9760()*0.414509);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c010() {
   return (neuron0x90f9960()*0.795908);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c038() {
   return (neuron0x90f9b78()*0.0267265);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c060() {
   return (neuron0x90f9d90()*0.264117);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c088() {
   return (neuron0x90f9fa8()*-0.34939);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c0b0() {
   return (neuron0x90fa1c0()*0.0192486);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c0d8() {
   return (neuron0x90fa3d8()*0.667214);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c100() {
   return (neuron0x90fa5f0()*0.392286);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c128() {
   return (neuron0x90fa7f0()*0.0557593);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910c368() {
   return (neuron0x90f93a8()*-0.437787);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103ab0() {
   return (neuron0x90f9560()*0.414072);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103ad8() {
   return (neuron0x90f9760()*-0.170819);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103b00() {
   return (neuron0x90f9960()*0.0946331);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103d30() {
   return (neuron0x90f9b78()*0.175488);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103d58() {
   return (neuron0x90f9d90()*-0.0394213);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103f88() {
   return (neuron0x90f9fa8()*-0.109279);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9103fb0() {
   return (neuron0x90fa1c0()*0.0783534);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91041e8() {
   return (neuron0x90fa3d8()*-0.146788);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104210() {
   return (neuron0x90fa5f0()*-0.43028);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9104238() {
   return (neuron0x90fa7f0()*-0.150635);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d258() {
   return (neuron0x90f93a8()*-0.100141);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d280() {
   return (neuron0x90f9560()*0.210298);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d2a8() {
   return (neuron0x90f9760()*-1.38671);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d2d0() {
   return (neuron0x90f9960()*0.251355);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d2f8() {
   return (neuron0x90f9b78()*-0.563935);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d320() {
   return (neuron0x90f9d90()*0.307535);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d348() {
   return (neuron0x90f9fa8()*1.50413);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d370() {
   return (neuron0x90fa1c0()*0.558612);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d398() {
   return (neuron0x90fa3d8()*1.15493);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d3c0() {
   return (neuron0x90fa5f0()*0.332615);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d3e8() {
   return (neuron0x90fa7f0()*0.401448);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d610() {
   return (neuron0x90f93a8()*0.0912612);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d638() {
   return (neuron0x90f9560()*0.335277);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d660() {
   return (neuron0x90f9760()*0.646052);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d688() {
   return (neuron0x90f9960()*0.307852);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d6b0() {
   return (neuron0x90f9b78()*0.52504);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d6d8() {
   return (neuron0x90f9d90()*0.102105);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d700() {
   return (neuron0x90f9fa8()*0.500806);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d728() {
   return (neuron0x90fa1c0()*0.665146);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d750() {
   return (neuron0x90fa3d8()*-0.176509);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d778() {
   return (neuron0x90fa5f0()*0.303563);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d7a0() {
   return (neuron0x90fa7f0()*-0.684876);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910d9e0() {
   return (neuron0x90f93a8()*-0.164109);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910da08() {
   return (neuron0x90f9560()*-0.390033);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910da30() {
   return (neuron0x90f9760()*-0.737596);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910da58() {
   return (neuron0x90f9960()*0.0662979);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910da80() {
   return (neuron0x90f9b78()*0.318152);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910daa8() {
   return (neuron0x90f9d90()*-0.73823);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910dad0() {
   return (neuron0x90f9fa8()*0.147808);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910daf8() {
   return (neuron0x90fa1c0()*0.285833);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910db20() {
   return (neuron0x90fa3d8()*0.39917);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910db48() {
   return (neuron0x90fa5f0()*0.626575);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910db70() {
   return (neuron0x90fa7f0()*0.000576065);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ddb0() {
   return (neuron0x90f93a8()*0.170577);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ddd8() {
   return (neuron0x90f9560()*-0.372264);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910de00() {
   return (neuron0x90f9760()*-0.628734);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910de28() {
   return (neuron0x90f9960()*-0.0705387);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910de50() {
   return (neuron0x90f9b78()*-0.0113676);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910de78() {
   return (neuron0x90f9d90()*-0.321225);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910dea0() {
   return (neuron0x90f9fa8()*-0.275612);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910dec8() {
   return (neuron0x90fa1c0()*0.541112);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910def0() {
   return (neuron0x90fa3d8()*0.524762);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910df18() {
   return (neuron0x90fa5f0()*0.242615);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910df40() {
   return (neuron0x90fa7f0()*-0.192304);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e180() {
   return (neuron0x90f93a8()*0.0153275);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e1a8() {
   return (neuron0x90f9560()*0.207248);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e1d0() {
   return (neuron0x90f9760()*0.361631);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e1f8() {
   return (neuron0x90f9960()*-0.0368776);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e220() {
   return (neuron0x90f9b78()*-0.272091);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e248() {
   return (neuron0x90f9d90()*-0.300059);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e270() {
   return (neuron0x90f9fa8()*-0.329377);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e298() {
   return (neuron0x90fa1c0()*0.0223851);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e2c0() {
   return (neuron0x90fa3d8()*0.0718055);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e2e8() {
   return (neuron0x90fa5f0()*-0.0665063);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e310() {
   return (neuron0x90fa7f0()*0.0399916);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e550() {
   return (neuron0x90f93a8()*-0.252069);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e578() {
   return (neuron0x90f9560()*0.301516);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e5a0() {
   return (neuron0x90f9760()*-0.283235);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e5c8() {
   return (neuron0x90f9960()*0.159029);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e5f0() {
   return (neuron0x90f9b78()*-0.381388);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e618() {
   return (neuron0x90f9d90()*0.199282);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e640() {
   return (neuron0x90f9fa8()*0.1862);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e668() {
   return (neuron0x90fa1c0()*0.168665);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e690() {
   return (neuron0x90fa3d8()*0.0658696);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e6b8() {
   return (neuron0x90fa5f0()*-0.196306);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e6e0() {
   return (neuron0x90fa7f0()*-0.300013);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e920() {
   return (neuron0x90f93a8()*0.416692);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e948() {
   return (neuron0x90f9560()*0.539459);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e970() {
   return (neuron0x90f9760()*-0.0268672);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e998() {
   return (neuron0x90f9960()*0.236765);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e9c0() {
   return (neuron0x90f9b78()*0.323451);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910e9e8() {
   return (neuron0x90f9d90()*-0.127535);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ea10() {
   return (neuron0x90f9fa8()*-0.470065);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ea38() {
   return (neuron0x90fa1c0()*-0.388531);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ea60() {
   return (neuron0x90fa3d8()*-1.08687);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ea88() {
   return (neuron0x90fa5f0()*-0.167794);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910eab0() {
   return (neuron0x90fa7f0()*-0.501608);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ecf0() {
   return (neuron0x90f93a8()*0.236158);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ed18() {
   return (neuron0x90f9560()*-0.504056);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ed40() {
   return (neuron0x90f9760()*-0.429545);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ed68() {
   return (neuron0x90f9960()*0.289096);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ed90() {
   return (neuron0x90f9b78()*0.420426);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910edb8() {
   return (neuron0x90f9d90()*-0.127201);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ede0() {
   return (neuron0x90f9fa8()*0.0782003);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ee08() {
   return (neuron0x90fa1c0()*-0.143567);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ee30() {
   return (neuron0x90fa3d8()*0.169342);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ee58() {
   return (neuron0x90fa5f0()*0.358641);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910ee80() {
   return (neuron0x90fa7f0()*-0.100598);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f0c0() {
   return (neuron0x90f93a8()*-0.47847);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f0e8() {
   return (neuron0x90f9560()*0.589341);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f110() {
   return (neuron0x90f9760()*-0.629223);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f138() {
   return (neuron0x90f9960()*0.181793);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f160() {
   return (neuron0x90f9b78()*-0.562711);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f188() {
   return (neuron0x90f9d90()*-0.722071);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f1b0() {
   return (neuron0x90f9fa8()*0.181464);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f1d8() {
   return (neuron0x90fa1c0()*-0.361726);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f200() {
   return (neuron0x90fa3d8()*0.328623);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f228() {
   return (neuron0x90fa5f0()*0.168429);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f250() {
   return (neuron0x90fa7f0()*0.26993);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f490() {
   return (neuron0x90f93a8()*0.203138);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f4b8() {
   return (neuron0x90f9560()*0.144829);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f4e0() {
   return (neuron0x90f9760()*0.784518);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f508() {
   return (neuron0x90f9960()*0.00665807);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f530() {
   return (neuron0x90f9b78()*0.532303);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f558() {
   return (neuron0x90f9d90()*0.468247);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f580() {
   return (neuron0x90f9fa8()*-0.692218);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f5a8() {
   return (neuron0x90fa1c0()*-0.414489);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f5d0() {
   return (neuron0x90fa3d8()*-0.0694157);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f5f8() {
   return (neuron0x90fa5f0()*-0.358031);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f620() {
   return (neuron0x90fa7f0()*-0.025851);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f860() {
   return (neuron0x90f93a8()*0.247749);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f888() {
   return (neuron0x90f9560()*0.0693026);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f8b0() {
   return (neuron0x90f9760()*0.0778652);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f8d8() {
   return (neuron0x90f9960()*0.49657);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f900() {
   return (neuron0x90f9b78()*-0.6274);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f928() {
   return (neuron0x90f9d90()*0.130893);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f950() {
   return (neuron0x90f9fa8()*0.0278205);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f978() {
   return (neuron0x90fa1c0()*0.133664);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f9a0() {
   return (neuron0x90fa3d8()*0.54578);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f9c8() {
   return (neuron0x90fa5f0()*-0.982329);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910f9f0() {
   return (neuron0x90fa7f0()*0.711461);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fc30() {
   return (neuron0x90f93a8()*-0.420938);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fc58() {
   return (neuron0x90f9560()*0.167974);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fc80() {
   return (neuron0x90f9760()*0.0881721);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fca8() {
   return (neuron0x90f9960()*-0.368346);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fcd0() {
   return (neuron0x90f9b78()*0.00586934);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fcf8() {
   return (neuron0x90f9d90()*0.283141);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fd20() {
   return (neuron0x90f9fa8()*-0.172297);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fd48() {
   return (neuron0x90fa1c0()*-0.172262);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fd70() {
   return (neuron0x90fa3d8()*-0.0357496);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fd98() {
   return (neuron0x90fa5f0()*0.397285);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x910fdc0() {
   return (neuron0x90fa7f0()*-0.0614832);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110000() {
   return (neuron0x90f93a8()*0.153715);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110028() {
   return (neuron0x90f9560()*-0.522096);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110050() {
   return (neuron0x90f9760()*0.00978677);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110078() {
   return (neuron0x90f9960()*-0.00308846);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91100a0() {
   return (neuron0x90f9b78()*0.26056);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91100c8() {
   return (neuron0x90f9d90()*-0.487164);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91100f0() {
   return (neuron0x90f9fa8()*-0.118534);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110118() {
   return (neuron0x90fa1c0()*-0.0509933);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110140() {
   return (neuron0x90fa3d8()*0.609158);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110168() {
   return (neuron0x90fa5f0()*0.08442);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110190() {
   return (neuron0x90fa7f0()*-0.568655);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91103d0() {
   return (neuron0x90f93a8()*-0.0729599);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91103f8() {
   return (neuron0x90f9560()*0.874195);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110420() {
   return (neuron0x90f9760()*-0.330072);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110448() {
   return (neuron0x90f9960()*-0.145137);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110470() {
   return (neuron0x90f9b78()*0.628441);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110498() {
   return (neuron0x90f9d90()*-0.171807);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91104c0() {
   return (neuron0x90f9fa8()*0.117792);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91104e8() {
   return (neuron0x90fa1c0()*0.150148);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110510() {
   return (neuron0x90fa3d8()*-0.316836);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110538() {
   return (neuron0x90fa5f0()*-0.328822);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110560() {
   return (neuron0x90fa7f0()*-0.164583);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91107a0() {
   return (neuron0x90f93a8()*0.246259);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91107c8() {
   return (neuron0x90f9560()*0.462319);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91107f0() {
   return (neuron0x90f9760()*0.312628);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110818() {
   return (neuron0x90f9960()*-0.609739);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110840() {
   return (neuron0x90f9b78()*-0.458284);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110868() {
   return (neuron0x90f9d90()*-0.184376);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110890() {
   return (neuron0x90f9fa8()*0.303121);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91108b8() {
   return (neuron0x90fa1c0()*-0.344538);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91108e0() {
   return (neuron0x90fa3d8()*-0.260087);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110908() {
   return (neuron0x90fa5f0()*0.579339);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110930() {
   return (neuron0x90fa7f0()*0.302906);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110b70() {
   return (neuron0x90f93a8()*-0.233076);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110b98() {
   return (neuron0x90f9560()*-0.438885);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110bc0() {
   return (neuron0x90f9760()*-1.18273);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110be8() {
   return (neuron0x90f9960()*0.952416);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110c10() {
   return (neuron0x90f9b78()*-0.379593);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110c38() {
   return (neuron0x90f9d90()*-0.0778318);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110c60() {
   return (neuron0x90f9fa8()*-1.31294);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110c88() {
   return (neuron0x90fa1c0()*-0.295747);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110cb0() {
   return (neuron0x90fa3d8()*-0.127718);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110cd8() {
   return (neuron0x90fa5f0()*-0.168388);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110d00() {
   return (neuron0x90fa7f0()*-0.354393);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110f40() {
   return (neuron0x90f93a8()*0.635356);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110f68() {
   return (neuron0x90f9560()*0.556634);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110f90() {
   return (neuron0x90f9760()*-0.136856);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110fb8() {
   return (neuron0x90f9960()*-0.551436);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9110fe0() {
   return (neuron0x90f9b78()*0.713456);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111008() {
   return (neuron0x90f9d90()*0.258051);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111030() {
   return (neuron0x90f9fa8()*0.223042);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111058() {
   return (neuron0x90fa1c0()*-0.288846);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111080() {
   return (neuron0x90fa3d8()*-0.907983);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91110a8() {
   return (neuron0x90fa5f0()*0.660047);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91110d0() {
   return (neuron0x90fa7f0()*0.3865);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111310() {
   return (neuron0x90f93a8()*0.578737);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111338() {
   return (neuron0x90f9560()*-0.723455);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111360() {
   return (neuron0x90f9760()*-0.773817);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111388() {
   return (neuron0x90f9960()*-0.374271);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91113b0() {
   return (neuron0x90f9b78()*0.301095);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91113d8() {
   return (neuron0x90f9d90()*-0.389438);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111400() {
   return (neuron0x90f9fa8()*0.568113);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111428() {
   return (neuron0x90fa1c0()*-0.0982831);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111450() {
   return (neuron0x90fa3d8()*-0.258938);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111478() {
   return (neuron0x90fa5f0()*0.149264);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91114a0() {
   return (neuron0x90fa7f0()*0.357799);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91116e0() {
   return (neuron0x90f93a8()*-0.408563);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111708() {
   return (neuron0x90f9560()*-0.493313);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111730() {
   return (neuron0x90f9760()*0.503429);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111758() {
   return (neuron0x90f9960()*-0.120213);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111780() {
   return (neuron0x90f9b78()*0.388564);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91117a8() {
   return (neuron0x90f9d90()*0.020132);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91117d0() {
   return (neuron0x90f9fa8()*0.0763419);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91117f8() {
   return (neuron0x90fa1c0()*0.160123);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111820() {
   return (neuron0x90fa3d8()*0.921423);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111848() {
   return (neuron0x90fa5f0()*0.0388163);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111870() {
   return (neuron0x90fa7f0()*0.617955);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111ab0() {
   return (neuron0x90f93a8()*0.567218);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111ad8() {
   return (neuron0x90f9560()*0.122962);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111b00() {
   return (neuron0x90f9760()*-0.445652);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111b28() {
   return (neuron0x90f9960()*0.0302502);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111b50() {
   return (neuron0x90f9b78()*0.242039);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111b78() {
   return (neuron0x90f9d90()*-0.133352);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111ba0() {
   return (neuron0x90f9fa8()*0.33619);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111bc8() {
   return (neuron0x90fa1c0()*0.580494);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111bf0() {
   return (neuron0x90fa3d8()*0.0803233);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111c18() {
   return (neuron0x90fa5f0()*0.659882);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111c40() {
   return (neuron0x90fa7f0()*0.239566);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111e80() {
   return (neuron0x90f93a8()*-0.412069);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111ea8() {
   return (neuron0x90f9560()*0.118205);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111ed0() {
   return (neuron0x90f9760()*0.178762);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111ef8() {
   return (neuron0x90f9960()*0.0929984);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111f20() {
   return (neuron0x90f9b78()*-0.208865);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111f48() {
   return (neuron0x90f9d90()*-0.0305639);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111f70() {
   return (neuron0x90f9fa8()*-0.252852);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111f98() {
   return (neuron0x90fa1c0()*-0.232729);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111fc0() {
   return (neuron0x90fa3d8()*0.324203);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9111fe8() {
   return (neuron0x90fa5f0()*-0.269597);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112010() {
   return (neuron0x90fa7f0()*-0.0598979);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112250() {
   return (neuron0x90f93a8()*-0.218462);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112278() {
   return (neuron0x90f9560()*-0.361815);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91122a0() {
   return (neuron0x90f9760()*-0.276989);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91122c8() {
   return (neuron0x90f9960()*0.144637);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91122f0() {
   return (neuron0x90f9b78()*0.290273);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112318() {
   return (neuron0x90f9d90()*-0.549141);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112340() {
   return (neuron0x90f9fa8()*0.322583);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112368() {
   return (neuron0x90fa1c0()*0.386393);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112390() {
   return (neuron0x90fa3d8()*-0.0686501);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91123b8() {
   return (neuron0x90fa5f0()*-0.301811);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91123e0() {
   return (neuron0x90fa7f0()*0.291477);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112620() {
   return (neuron0x90f93a8()*0.0858296);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112648() {
   return (neuron0x90f9560()*0.348961);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112670() {
   return (neuron0x90f9760()*0.282884);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112698() {
   return (neuron0x90f9960()*0.0966338);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91126c0() {
   return (neuron0x90f9b78()*-0.302585);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91126e8() {
   return (neuron0x90f9d90()*0.0321354);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112710() {
   return (neuron0x90f9fa8()*-0.207881);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112738() {
   return (neuron0x90fa1c0()*-0.192242);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112760() {
   return (neuron0x90fa3d8()*0.186942);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112788() {
   return (neuron0x90fa5f0()*-0.420722);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91127b0() {
   return (neuron0x90fa7f0()*0.506115);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91129f0() {
   return (neuron0x90f93a8()*0.0413238);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112a18() {
   return (neuron0x90f9560()*-0.634411);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112a40() {
   return (neuron0x90f9760()*-0.311536);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112a68() {
   return (neuron0x90f9960()*-0.0930182);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112a90() {
   return (neuron0x90f9b78()*-0.100702);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112ab8() {
   return (neuron0x90f9d90()*0.568631);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112ae0() {
   return (neuron0x90f9fa8()*0.293664);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112b08() {
   return (neuron0x90fa1c0()*0.0853357);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112b30() {
   return (neuron0x90fa3d8()*-0.440803);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112b58() {
   return (neuron0x90fa5f0()*0.465831);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112b80() {
   return (neuron0x90fa7f0()*-0.0130904);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112dc0() {
   return (neuron0x90f93a8()*0.255545);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112de8() {
   return (neuron0x90f9560()*-0.0949311);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112e10() {
   return (neuron0x90f9760()*-0.138008);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112e38() {
   return (neuron0x90f9960()*-0.418167);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112e60() {
   return (neuron0x90f9b78()*0.883704);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112e88() {
   return (neuron0x90f9d90()*0.248105);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112eb0() {
   return (neuron0x90f9fa8()*0.862477);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112ed8() {
   return (neuron0x90fa1c0()*-0.14546);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112f00() {
   return (neuron0x90fa3d8()*-0.456313);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112f28() {
   return (neuron0x90fa5f0()*-0.297981);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9112f50() {
   return (neuron0x90fa7f0()*-0.170119);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113190() {
   return (neuron0x90f93a8()*0.444795);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91131b8() {
   return (neuron0x90f9560()*0.333457);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91131e0() {
   return (neuron0x90f9760()*0.0504129);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113208() {
   return (neuron0x90f9960()*0.623246);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113230() {
   return (neuron0x90f9b78()*0.104817);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113258() {
   return (neuron0x90f9d90()*0.488383);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113280() {
   return (neuron0x90f9fa8()*0.0136123);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91132a8() {
   return (neuron0x90fa1c0()*-0.49977);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91132d0() {
   return (neuron0x90fa3d8()*0.452774);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91132f8() {
   return (neuron0x90fa5f0()*-0.1588);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113320() {
   return (neuron0x90fa7f0()*0.594432);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113560() {
   return (neuron0x90f93a8()*0.322263);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113588() {
   return (neuron0x90f9560()*-0.375522);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91135b0() {
   return (neuron0x90f9760()*-0.0354429);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91135d8() {
   return (neuron0x90f9960()*-0.113168);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113600() {
   return (neuron0x90f9b78()*-0.0737361);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113628() {
   return (neuron0x90f9d90()*-0.39271);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113650() {
   return (neuron0x90f9fa8()*0.231811);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113678() {
   return (neuron0x90fa1c0()*-0.138214);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91136a0() {
   return (neuron0x90fa3d8()*0.188659);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91136c8() {
   return (neuron0x90fa5f0()*-0.462752);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91136f0() {
   return (neuron0x90fa7f0()*0.128765);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113930() {
   return (neuron0x90f93a8()*0.183074);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113958() {
   return (neuron0x90f9560()*-0.554842);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113980() {
   return (neuron0x90f9760()*-1.2782);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91139a8() {
   return (neuron0x90f9960()*0.0186525);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91139d0() {
   return (neuron0x90f9b78()*0.59461);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91139f8() {
   return (neuron0x90f9d90()*0.029575);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113a20() {
   return (neuron0x90f9fa8()*0.394851);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113a48() {
   return (neuron0x90fa1c0()*-0.226043);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113a70() {
   return (neuron0x90fa3d8()*0.0402154);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113a98() {
   return (neuron0x90fa5f0()*0.486671);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113ac0() {
   return (neuron0x90fa7f0()*-0.13502);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113d00() {
   return (neuron0x90f93a8()*-0.229268);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113d28() {
   return (neuron0x90f9560()*-0.0299317);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113d50() {
   return (neuron0x90f9760()*-0.0225014);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113d78() {
   return (neuron0x90f9960()*-0.133893);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113da0() {
   return (neuron0x90f9b78()*0.486935);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113dc8() {
   return (neuron0x90f9d90()*0.131347);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113df0() {
   return (neuron0x90f9fa8()*0.497715);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113e18() {
   return (neuron0x90fa1c0()*-0.059239);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113e40() {
   return (neuron0x90fa3d8()*-0.132498);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113e68() {
   return (neuron0x90fa5f0()*-0.0746028);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9113e90() {
   return (neuron0x90fa7f0()*-0.175223);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91140d0() {
   return (neuron0x90f93a8()*0.287703);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91140f8() {
   return (neuron0x90f9560()*0.283762);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107ab0() {
   return (neuron0x90f9760()*-0.101524);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107ad8() {
   return (neuron0x90f9960()*0.391408);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107b00() {
   return (neuron0x90f9b78()*-0.577237);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107b28() {
   return (neuron0x90f9d90()*0.344535);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107b50() {
   return (neuron0x90f9fa8()*0.0544102);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107b78() {
   return (neuron0x90fa1c0()*-0.0303979);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107ba0() {
   return (neuron0x90fa3d8()*-0.206574);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107bc8() {
   return (neuron0x90fa5f0()*-0.172964);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107bf0() {
   return (neuron0x90fa7f0()*-0.425088);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107e30() {
   return (neuron0x90f93a8()*-0.472285);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107e58() {
   return (neuron0x90f9560()*-0.458615);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107e80() {
   return (neuron0x90f9760()*-1.14296);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107ea8() {
   return (neuron0x90f9960()*-1.14973);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107ed0() {
   return (neuron0x90f9b78()*-0.352136);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107ef8() {
   return (neuron0x90f9d90()*-0.610181);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107f20() {
   return (neuron0x90f9fa8()*0.843843);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107f48() {
   return (neuron0x90fa1c0()*-0.116026);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107f70() {
   return (neuron0x90fa3d8()*0.66597);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107f98() {
   return (neuron0x90fa5f0()*0.0809778);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9107fc0() {
   return (neuron0x90fa7f0()*-0.0511674);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108200() {
   return (neuron0x90f93a8()*0.254479);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108228() {
   return (neuron0x90f9560()*0.388352);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108250() {
   return (neuron0x90f9760()*-0.489479);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108278() {
   return (neuron0x90f9960()*0.657796);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91082a0() {
   return (neuron0x90f9b78()*-0.0680196);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91082c8() {
   return (neuron0x90f9d90()*0.258234);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91082f0() {
   return (neuron0x90f9fa8()*-1.18614);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108318() {
   return (neuron0x90fa1c0()*-0.184484);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108340() {
   return (neuron0x90fa3d8()*1.00441);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108368() {
   return (neuron0x90fa5f0()*0.368705);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108390() {
   return (neuron0x90fa7f0()*0.154271);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91085d0() {
   return (neuron0x90f93a8()*0.246901);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91085f8() {
   return (neuron0x90f9560()*-0.114218);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108620() {
   return (neuron0x90f9760()*-0.341679);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108648() {
   return (neuron0x90f9960()*0.336354);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108670() {
   return (neuron0x90f9b78()*0.390757);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108698() {
   return (neuron0x90f9d90()*0.0432495);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91086c0() {
   return (neuron0x90f9fa8()*0.280945);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91086e8() {
   return (neuron0x90fa1c0()*-0.467906);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108710() {
   return (neuron0x90fa3d8()*0.1687);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108738() {
   return (neuron0x90fa5f0()*-0.0842648);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108760() {
   return (neuron0x90fa7f0()*-0.311994);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91089a0() {
   return (neuron0x90f93a8()*-0.200131);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91089c8() {
   return (neuron0x90f9560()*-0.0777088);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91089f0() {
   return (neuron0x90f9760()*0.158824);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108a18() {
   return (neuron0x90f9960()*0.269113);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108a40() {
   return (neuron0x90f9b78()*0.379654);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108a68() {
   return (neuron0x90f9d90()*0.191425);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9108a90() {
   return (neuron0x90f9fa8()*0.403904);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116128() {
   return (neuron0x90fa1c0()*-0.632743);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116150() {
   return (neuron0x90fa3d8()*0.383065);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116178() {
   return (neuron0x90fa5f0()*0.167292);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91161a0() {
   return (neuron0x90fa7f0()*0.514691);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91163e0() {
   return (neuron0x90f93a8()*0.167767);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116408() {
   return (neuron0x90f9560()*-0.0348077);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116430() {
   return (neuron0x90f9760()*-1.0203);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116458() {
   return (neuron0x90f9960()*-0.0236236);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116480() {
   return (neuron0x90f9b78()*1.14574);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91164a8() {
   return (neuron0x90f9d90()*0.158065);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91164d0() {
   return (neuron0x90f9fa8()*1.85999);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91164f8() {
   return (neuron0x90fa1c0()*0.132472);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116520() {
   return (neuron0x90fa3d8()*1.00288);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116548() {
   return (neuron0x90fa5f0()*-0.0374744);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116570() {
   return (neuron0x90fa7f0()*0.146457);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91167b0() {
   return (neuron0x90f93a8()*-0.0737646);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91167d8() {
   return (neuron0x90f9560()*0.228022);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116800() {
   return (neuron0x90f9760()*1.04476);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116828() {
   return (neuron0x90f9960()*0.364102);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116850() {
   return (neuron0x90f9b78()*-0.300375);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116878() {
   return (neuron0x90f9d90()*0.0571221);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91168a0() {
   return (neuron0x90f9fa8()*-1.06938);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91168c8() {
   return (neuron0x90fa1c0()*-0.117855);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91168f0() {
   return (neuron0x90fa3d8()*0.949135);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116918() {
   return (neuron0x90fa5f0()*-0.155285);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116940() {
   return (neuron0x90fa7f0()*0.323241);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116a90() {
   return (neuron0x90fab30()*0.156584);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116ab8() {
   return (neuron0x90fae78()*-0.538482);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116ae0() {
   return (neuron0x90fb2b8()*-0.68921);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116b08() {
   return (neuron0x90fb730()*-0.231752);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116b30() {
   return (neuron0x90fbae8()*0.0243198);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116b58() {
   return (neuron0x90fbea0()*-0.129482);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116b80() {
   return (neuron0x90fc438()*1.16874);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116ba8() {
   return (neuron0x90fc7c0()*0.119122);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116bd0() {
   return (neuron0x90fcb90()*-0.869848);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116bf8() {
   return (neuron0x90fcf60()*-0.345877);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116c20() {
   return (neuron0x90fd330()*0.161052);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116c48() {
   return (neuron0x90fd700()*-0.376686);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116c70() {
   return (neuron0x90fc230()*-0.774665);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116c98() {
   return (neuron0x90fde60()*0.410387);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116cc0() {
   return (neuron0x90fe218()*0.312013);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116ce8() {
   return (neuron0x90fe5e8()*-0.0476348);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116d98() {
   return (neuron0x90fef90()*0.958167);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116dc0() {
   return (neuron0x9100170()*-1.56437);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116de8() {
   return (neuron0x9100450()*0.585935);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116e10() {
   return (neuron0x9100730()*-0.524974);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116e38() {
   return (neuron0x9100a58()*0.127328);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116e60() {
   return (neuron0x9100e10()*0.504701);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116e88() {
   return (neuron0x9101258()*1.63876);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116eb0() {
   return (neuron0x9101610()*0.136306);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116ed8() {
   return (neuron0x90fdb70()*0.116457);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116f00() {
   return (neuron0x91021a8()*-0.642218);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116f28() {
   return (neuron0x9102560()*0.822357);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116f50() {
   return (neuron0x9102930()*-1.44858);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116f78() {
   return (neuron0x9102d00()*0.723435);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116fa0() {
   return (neuron0x91030d0()*-0.625329);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116fc8() {
   return (neuron0x91034a0()*-0.0237586);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116ff0() {
   return (neuron0x9103870()*0.115918);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116d10() {
   return (neuron0x9104608()*0.675301);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116d38() {
   return (neuron0x9104730()*0.346409);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9116d60() {
   return (neuron0x9104a08()*-0.625475);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117120() {
   return (neuron0x9104dc0()*1.10933);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117148() {
   return (neuron0x9105178()*0.498321);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117170() {
   return (neuron0x9105530()*-0.0530893);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117198() {
   return (neuron0x9105900()*0.486371);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91171c0() {
   return (neuron0x9105cd0()*-0.925016);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91171e8() {
   return (neuron0x91060a0()*0.831263);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117210() {
   return (neuron0x9106470()*-0.837995);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117238() {
   return (neuron0x9106840()*0.328982);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117260() {
   return (neuron0x9106c10()*0.633925);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117288() {
   return (neuron0x9106fe0()*-0.378641);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91172b0() {
   return (neuron0x91073b0()*-0.558037);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91172d8() {
   return (neuron0x9107780()*-1.15372);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117300() {
   return (neuron0x9101950()*0.771408);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117328() {
   return (neuron0x9101d20()*0.353304);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117350() {
   return (neuron0x9108b08()*-0.111459);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117378() {
   return (neuron0x9108ec0()*-0.239164);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91173a0() {
   return (neuron0x9109290()*-0.0601129);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91173c8() {
   return (neuron0x9109660()*-0.255928);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91173f0() {
   return (neuron0x9109a30()*-0.528397);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117418() {
   return (neuron0x9109f18()*-0.523419);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117440() {
   return (neuron0x910a2d0()*-0.0205777);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117468() {
   return (neuron0x910a6a0()*0.718146);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117490() {
   return (neuron0x910aa70()*0.49557);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91174b8() {
   return (neuron0x910ae40()*-0.316622);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91174e0() {
   return (neuron0x910b210()*0.00771574);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117508() {
   return (neuron0x910b5e0()*0.0173126);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117530() {
   return (neuron0x910b9b0()*0.495262);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117558() {
   return (neuron0x910bd80()*0.548508);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117580() {
   return (neuron0x910c150()*-0.260921);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x90faa48() {
   return (neuron0x9104468()*0.912283);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117018() {
   return (neuron0x910d410()*0.755385);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117040() {
   return (neuron0x910d7c8()*-0.772593);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117068() {
   return (neuron0x910db98()*0.135034);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117090() {
   return (neuron0x910df68()*0.173234);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91170b8() {
   return (neuron0x910e338()*-0.113487);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91170e0() {
   return (neuron0x910e708()*0.399212);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91177b0() {
   return (neuron0x910ead8()*0.189914);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91177d8() {
   return (neuron0x910eea8()*-0.882552);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117800() {
   return (neuron0x910f278()*0.641734);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117828() {
   return (neuron0x910f648()*-0.726773);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117850() {
   return (neuron0x910fa18()*0.484446);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117878() {
   return (neuron0x910fde8()*-0.430763);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91178a0() {
   return (neuron0x91101b8()*-0.962601);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91178c8() {
   return (neuron0x9110588()*-0.373943);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91178f0() {
   return (neuron0x9110958()*-0.948051);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117918() {
   return (neuron0x9110d28()*0.752649);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117940() {
   return (neuron0x91110f8()*0.719972);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117968() {
   return (neuron0x91114c8()*-0.568931);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117990() {
   return (neuron0x9111898()*-0.782732);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91179b8() {
   return (neuron0x9111c68()*0.640338);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x91179e0() {
   return (neuron0x9112038()*0.216913);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117a08() {
   return (neuron0x9112408()*0.55849);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117a30() {
   return (neuron0x91127d8()*-0.779277);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117a58() {
   return (neuron0x9112ba8()*-0.92533);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117a80() {
   return (neuron0x9112f78()*0.439775);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117aa8() {
   return (neuron0x9113348()*-0.171742);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117ad0() {
   return (neuron0x9113718()*1.23593);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117af8() {
   return (neuron0x9113ae8()*-0.197325);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117b20() {
   return (neuron0x9113eb8()*-0.283899);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117b48() {
   return (neuron0x9107c18()*0.509373);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117b70() {
   return (neuron0x9107fe8()*0.609299);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117b98() {
   return (neuron0x91083b8()*-0.239437);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117bc0() {
   return (neuron0x9108788()*0.467894);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117be8() {
   return (neuron0x91161c8()*1.16472);
}

double NNParaElectronXYCorrectionClusterDeltaY::synapse0x9117c10() {
   return (neuron0x9116598()*-0.27326);
}

