#include "NNParaGammaXYCorrectionClusterDeltaX.h"
#include <cmath>

double NNParaGammaXYCorrectionClusterDeltaX::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8) {
   input0 = (in0 - 2122.17)/956.309;
   input1 = (in1 - -4.21955)/33.6873;
   input2 = (in2 - 0.0276887)/60.1795;
   input3 = (in3 - 1.35737)/0.353415;
   input4 = (in4 - 1.46875)/0.364824;
   input5 = (in5 - 0.201504)/1.73176;
   input6 = (in6 - 0.253821)/1.35266;
   input7 = (in7 - 6.58314)/12.4187;
   input8 = (in8 - 4.54498)/7.86055;
   switch(index) {
     case 0:
         return neuron0x8cef908();
     default:
         return 0.;
   }
}

double NNParaGammaXYCorrectionClusterDeltaX::Value(int index, double* input) {
   input0 = (input[0] - 2122.17)/956.309;
   input1 = (input[1] - -4.21955)/33.6873;
   input2 = (input[2] - 0.0276887)/60.1795;
   input3 = (input[3] - 1.35737)/0.353415;
   input4 = (input[4] - 1.46875)/0.364824;
   input5 = (input[5] - 0.201504)/1.73176;
   input6 = (input[6] - 0.253821)/1.35266;
   input7 = (input[7] - 6.58314)/12.4187;
   input8 = (input[8] - 4.54498)/7.86055;
   switch(index) {
     case 0:
         return neuron0x8cef908();
     default:
         return 0.;
   }
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cb64d8() {
   return input0;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cb6690() {
   return input1;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cb6890() {
   return input2;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cb6a90() {
   return input3;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8c6af08() {
   return input4;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8c6b120() {
   return input5;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8c6b338() {
   return input6;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8c6b550() {
   return input7;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd6a48() {
   return input8;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd6da0() {
   double input = -0.248662;
   input += synapse0x8bfff68();
   input += synapse0x8bd14b8();
   input += synapse0x8bd0958();
   input += synapse0x8cd6f58();
   input += synapse0x8cd6f80();
   input += synapse0x8cd6fa8();
   input += synapse0x8cd6fd0();
   input += synapse0x8cd6ff8();
   input += synapse0x8cd7020();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd6da0() {
   double input = input0x8cd6da0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd7048() {
   double input = 0.041788;
   input += synapse0x8cd7248();
   input += synapse0x8cd7270();
   input += synapse0x8cd7298();
   input += synapse0x8cd72c0();
   input += synapse0x8cd72e8();
   input += synapse0x8cd7310();
   input += synapse0x8cd7338();
   input += synapse0x8cd7360();
   input += synapse0x8cd7410();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd7048() {
   double input = input0x8cd7048();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd7438() {
   double input = -0.426195;
   input += synapse0x8cd75f0();
   input += synapse0x8cd7618();
   input += synapse0x8cd7640();
   input += synapse0x8cd7668();
   input += synapse0x8cd7690();
   input += synapse0x8cd76b8();
   input += synapse0x8cd76e0();
   input += synapse0x8cd7708();
   input += synapse0x8cd7730();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd7438() {
   double input = input0x8cd7438();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd7758() {
   double input = 0.226047;
   input += synapse0x8cd7958();
   input += synapse0x8cd7980();
   input += synapse0x8cd79a8();
   input += synapse0x8cd79d0();
   input += synapse0x8cd79f8();
   input += synapse0x8cd7a20();
   input += synapse0x8cb6c60();
   input += synapse0x8c91178();
   input += synapse0x8c6b690();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd7758() {
   double input = input0x8cd7758();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd7b50() {
   double input = -0.368068;
   input += synapse0x8cd7d50();
   input += synapse0x8cd7d78();
   input += synapse0x8cd7da0();
   input += synapse0x8cd7dc8();
   input += synapse0x8cd7df0();
   input += synapse0x8cd7e18();
   input += synapse0x8cd7e40();
   input += synapse0x8cd7e68();
   input += synapse0x8cd7e90();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd7b50() {
   double input = input0x8cd7b50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd7eb8() {
   double input = -0.112444;
   input += synapse0x8cd80b8();
   input += synapse0x8cd80e0();
   input += synapse0x8cd8108();
   input += synapse0x8cd8130();
   input += synapse0x8cd8158();
   input += synapse0x8cd8180();
   input += synapse0x8cd81a8();
   input += synapse0x8cd81d0();
   input += synapse0x8cd81f8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd7eb8() {
   double input = input0x8cd7eb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd8220() {
   double input = -0.117583;
   input += synapse0x8cd8420();
   input += synapse0x8cd8448();
   input += synapse0x8cd8470();
   input += synapse0x8cd8498();
   input += synapse0x8cd84c0();
   input += synapse0x8cd84e8();
   input += synapse0x8cd8510();
   input += synapse0x8cd8538();
   input += synapse0x8cd8560();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd8220() {
   double input = input0x8cd8220();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd8588() {
   double input = 0.703882;
   input += synapse0x8858108();
   input += synapse0x8c002d8();
   input += synapse0x8c00300();
   input += synapse0x8bcf4d0();
   input += synapse0x8bcf0a0();
   input += synapse0x8bcb428();
   input += synapse0x8bd0920();
   input += synapse0x8bd1460();
   input += synapse0x8cd7a48();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd8588() {
   double input = input0x8cd8588();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd8990() {
   double input = 0.834819;
   input += synapse0x8cd8b90();
   input += synapse0x8cd8bb8();
   input += synapse0x8cd8be0();
   input += synapse0x8cd8c08();
   input += synapse0x8cd8c30();
   input += synapse0x8cd8c58();
   input += synapse0x8cd8c80();
   input += synapse0x8cd8ca8();
   input += synapse0x8cd8cd0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd8990() {
   double input = input0x8cd8990();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd8cf8() {
   double input = -0.5886;
   input += synapse0x8cd8ef8();
   input += synapse0x8cd8f20();
   input += synapse0x8cd8f48();
   input += synapse0x8cd8f70();
   input += synapse0x8cd8f98();
   input += synapse0x8cd8fc0();
   input += synapse0x8cd8fe8();
   input += synapse0x8cd9010();
   input += synapse0x8cd9038();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd8cf8() {
   double input = input0x8cd8cf8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd9060() {
   double input = -0.29422;
   input += synapse0x8cd9260();
   input += synapse0x8cd9288();
   input += synapse0x8cd92b0();
   input += synapse0x8cd92d8();
   input += synapse0x8cd9300();
   input += synapse0x8cd9328();
   input += synapse0x8cd9350();
   input += synapse0x8cd9378();
   input += synapse0x8cd93a0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd9060() {
   double input = input0x8cd9060();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd93c8() {
   double input = -1.06017;
   input += synapse0x8cd95c8();
   input += synapse0x8cd95f0();
   input += synapse0x8cd9618();
   input += synapse0x8cd9640();
   input += synapse0x8cd9668();
   input += synapse0x8cd9690();
   input += synapse0x8cd96b8();
   input += synapse0x8cd96e0();
   input += synapse0x8cd9708();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd93c8() {
   double input = input0x8cd93c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd9730() {
   double input = -0.346288;
   input += synapse0x8cd9930();
   input += synapse0x8cd9958();
   input += synapse0x8cd9980();
   input += synapse0x8cd99a8();
   input += synapse0x8cd99d0();
   input += synapse0x8cd99f8();
   input += synapse0x8cd9a20();
   input += synapse0x8cd9a48();
   input += synapse0x8cd9a70();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd9730() {
   double input = input0x8cd9730();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd9a98() {
   double input = -0.202258;
   input += synapse0x8cd9c98();
   input += synapse0x8cd9cc0();
   input += synapse0x8cd9ce8();
   input += synapse0x8cd9d10();
   input += synapse0x8cd9d38();
   input += synapse0x8cd9d60();
   input += synapse0x8cd9d88();
   input += synapse0x8cd9db0();
   input += synapse0x8cd9dd8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd9a98() {
   double input = input0x8cd9a98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd9e00() {
   double input = 0.396657;
   input += synapse0x8cda018();
   input += synapse0x8cda040();
   input += synapse0x8cda068();
   input += synapse0x8cd7a70();
   input += synapse0x8cd7a98();
   input += synapse0x8cd7ac0();
   input += synapse0x8cd7ae8();
   input += synapse0x8cd7b10();
   input += synapse0x8cd8788();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd9e00() {
   double input = input0x8cd9e00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cd87b0() {
   double input = 0.393793;
   input += synapse0x8cd8968();
   input += synapse0x8cda568();
   input += synapse0x8cda618();
   input += synapse0x8cda6c8();
   input += synapse0x8cda778();
   input += synapse0x8cda828();
   input += synapse0x8cda8d8();
   input += synapse0x8cda988();
   input += synapse0x8cdaa38();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cd87b0() {
   double input = input0x8cd87b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdaae8() {
   double input = 0.181249;
   input += synapse0x8cdac28();
   input += synapse0x8cdac50();
   input += synapse0x8cdac78();
   input += synapse0x8cdaca0();
   input += synapse0x8cdacc8();
   input += synapse0x8cdacf0();
   input += synapse0x8cdad18();
   input += synapse0x8cdad40();
   input += synapse0x8cdad68();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdaae8() {
   double input = input0x8cdaae8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdad90() {
   double input = -0.991696;
   input += synapse0x8cdaed0();
   input += synapse0x8cdaef8();
   input += synapse0x8cdaf20();
   input += synapse0x8cdaf48();
   input += synapse0x8cdaf70();
   input += synapse0x8cdaf98();
   input += synapse0x8cdafc0();
   input += synapse0x8cdafe8();
   input += synapse0x8cdb010();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdad90() {
   double input = input0x8cdad90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdb038() {
   double input = -0.888074;
   input += synapse0x8cdb178();
   input += synapse0x8cdb1a0();
   input += synapse0x8cdb1c8();
   input += synapse0x8cdb1f0();
   input += synapse0x8cdb218();
   input += synapse0x8cdb240();
   input += synapse0x8cdb268();
   input += synapse0x8cdb290();
   input += synapse0x8cdb2b8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdb038() {
   double input = input0x8cdb038();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdb2e0() {
   double input = 0.608909;
   input += synapse0x8cdb4f8();
   input += synapse0x8cdb520();
   input += synapse0x8cdb548();
   input += synapse0x8cdb570();
   input += synapse0x8cdb598();
   input += synapse0x8cdb5c0();
   input += synapse0x8cdb5e8();
   input += synapse0x8cdb610();
   input += synapse0x8cdb638();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdb2e0() {
   double input = input0x8cdb2e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdb660() {
   double input = -0.287266;
   input += synapse0x8cdb878();
   input += synapse0x8cdb8a0();
   input += synapse0x8cdb8c8();
   input += synapse0x8cdb8f0();
   input += synapse0x8cdb918();
   input += synapse0x8cdb940();
   input += synapse0x8cdb968();
   input += synapse0x8cdb990();
   input += synapse0x8cdb9b8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdb660() {
   double input = input0x8cdb660();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdb9e0() {
   double input = -0.0270945;
   input += synapse0x8cdbbf8();
   input += synapse0x8cdbc20();
   input += synapse0x8cdbc48();
   input += synapse0x8cdbc70();
   input += synapse0x8cdbc98();
   input += synapse0x8cdbcc0();
   input += synapse0x8cdbce8();
   input += synapse0x8cdbd10();
   input += synapse0x8cdbd38();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdb9e0() {
   double input = input0x8cdb9e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdbd60() {
   double input = -0.896125;
   input += synapse0x8cdbf78();
   input += synapse0x8cdbfa0();
   input += synapse0x8cdbfc8();
   input += synapse0x8cdbff0();
   input += synapse0x8cdc018();
   input += synapse0x8cdc040();
   input += synapse0x8cdc068();
   input += synapse0x8cdc090();
   input += synapse0x8cdc0b8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdbd60() {
   double input = input0x8cdbd60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdc0e0() {
   double input = -0.214372;
   input += synapse0x8cd7388();
   input += synapse0x8cd73b0();
   input += synapse0x8cd73d8();
   input += synapse0x8cdc400();
   input += synapse0x8cdc428();
   input += synapse0x8cdc450();
   input += synapse0x8cdc478();
   input += synapse0x8cdc4a0();
   input += synapse0x8cdc4c8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdc0e0() {
   double input = input0x8cdc0e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdc4f0() {
   double input = 0.462413;
   input += synapse0x8cdc708();
   input += synapse0x8cdc730();
   input += synapse0x8cdc758();
   input += synapse0x8cdd730();
   input += synapse0x8cdd758();
   input += synapse0x8cdd780();
   input += synapse0x8cdd7a8();
   input += synapse0x8cdd7d0();
   input += synapse0x8cdd7f8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdc4f0() {
   double input = input0x8cdc4f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdd820() {
   double input = 0.62342;
   input += synapse0x8cdda20();
   input += synapse0x8cdda48();
   input += synapse0x8cdda70();
   input += synapse0x8cdda98();
   input += synapse0x8cddac0();
   input += synapse0x8cddae8();
   input += synapse0x8cddb10();
   input += synapse0x8cddb38();
   input += synapse0x8cddb60();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdd820() {
   double input = input0x8cdd820();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cddb88() {
   double input = -0.0955302;
   input += synapse0x8cddd88();
   input += synapse0x8cdddb0();
   input += synapse0x8cdddd8();
   input += synapse0x8cdde00();
   input += synapse0x8cdde28();
   input += synapse0x8cdde50();
   input += synapse0x8cdde78();
   input += synapse0x8cddea0();
   input += synapse0x8cddec8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cddb88() {
   double input = input0x8cddb88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cddef0() {
   double input = -0.499609;
   input += synapse0x8cde0f0();
   input += synapse0x8cde118();
   input += synapse0x8cde140();
   input += synapse0x8cde168();
   input += synapse0x8cde190();
   input += synapse0x8cde1b8();
   input += synapse0x8cde1e0();
   input += synapse0x8cde208();
   input += synapse0x8cde230();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cddef0() {
   double input = input0x8cddef0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cde258() {
   double input = -0.666051;
   input += synapse0x8cde458();
   input += synapse0x8cde480();
   input += synapse0x8cde4a8();
   input += synapse0x8cde4d0();
   input += synapse0x8cde4f8();
   input += synapse0x8cda090();
   input += synapse0x8cda0b8();
   input += synapse0x8cda0e0();
   input += synapse0x8cda108();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cde258() {
   double input = input0x8cde258();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cda130() {
   double input = 0.219262;
   input += synapse0x8cda348();
   input += synapse0x8cda370();
   input += synapse0x8cda398();
   input += synapse0x8cda3c0();
   input += synapse0x8cda3e8();
   input += synapse0x8cda410();
   input += synapse0x8cda438();
   input += synapse0x8cda460();
   input += synapse0x8cded28();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cda130() {
   double input = input0x8cda130();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cded50() {
   double input = 0.0646566;
   input += synapse0x8cdef50();
   input += synapse0x8cdef78();
   input += synapse0x8cdefa0();
   input += synapse0x8cdefc8();
   input += synapse0x8cdeff0();
   input += synapse0x8cdf018();
   input += synapse0x8cdf040();
   input += synapse0x8cdf068();
   input += synapse0x8cdf090();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cded50() {
   double input = input0x8cded50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdf0b8() {
   double input = -0.0718934;
   input += synapse0x8cdf2d0();
   input += synapse0x8cda4e0();
   input += synapse0x8cda508();
   input += synapse0x8cda530();
   input += synapse0x8cda590();
   input += synapse0x8cda5b8();
   input += synapse0x8cda5e0();
   input += synapse0x8cda640();
   input += synapse0x8cda668();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdf0b8() {
   double input = input0x8cdf0b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdfc40() {
   double input = 0.249826;
   input += synapse0x8cda898();
   input += synapse0x8cda738();
   input += synapse0x8cda7e8();
   input += synapse0x8cda900();
   input += synapse0x8cda928();
   input += synapse0x8cda950();
   input += synapse0x8cda9b0();
   input += synapse0x8cda9d8();
   input += synapse0x8cdaa00();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdfc40() {
   double input = input0x8cdfc40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdfd68() {
   double input = 0.524696;
   input += synapse0x8cdaaa8();
   input += synapse0x8cdff20();
   input += synapse0x8cdff48();
   input += synapse0x8cdff70();
   input += synapse0x8cdff98();
   input += synapse0x8cdffc0();
   input += synapse0x8cdffe8();
   input += synapse0x8ce0010();
   input += synapse0x8ce0038();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdfd68() {
   double input = input0x8cdfd68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce0060() {
   double input = -0.194102;
   input += synapse0x8ce0260();
   input += synapse0x8ce0288();
   input += synapse0x8ce02b0();
   input += synapse0x8ce02d8();
   input += synapse0x8ce0300();
   input += synapse0x8ce0328();
   input += synapse0x8ce0350();
   input += synapse0x8ce0378();
   input += synapse0x8ce03a0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce0060() {
   double input = input0x8ce0060();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce03c8() {
   double input = 0.495705;
   input += synapse0x8ce05c8();
   input += synapse0x8ce05f0();
   input += synapse0x8ce0618();
   input += synapse0x8ce0640();
   input += synapse0x8ce0668();
   input += synapse0x8ce0690();
   input += synapse0x8ce06b8();
   input += synapse0x8ce06e0();
   input += synapse0x8ce0708();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce03c8() {
   double input = input0x8ce03c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce0730() {
   double input = 0.193585;
   input += synapse0x8ce0930();
   input += synapse0x8ce0958();
   input += synapse0x8ce0980();
   input += synapse0x8ce09a8();
   input += synapse0x8ce09d0();
   input += synapse0x8ce09f8();
   input += synapse0x8ce0a20();
   input += synapse0x8ce0a48();
   input += synapse0x8ce0a70();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce0730() {
   double input = input0x8ce0730();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce0a98() {
   double input = 0.225877;
   input += synapse0x8ce0c98();
   input += synapse0x8ce0cc0();
   input += synapse0x8ce0ce8();
   input += synapse0x8ce0d10();
   input += synapse0x8ce0d38();
   input += synapse0x8ce0d60();
   input += synapse0x8ce0d88();
   input += synapse0x8ce0db0();
   input += synapse0x8ce0dd8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce0a98() {
   double input = input0x8ce0a98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce0e00() {
   double input = 0.0533336;
   input += synapse0x8ce1018();
   input += synapse0x8ce1040();
   input += synapse0x8ce1068();
   input += synapse0x8ce1090();
   input += synapse0x8ce10b8();
   input += synapse0x8ce10e0();
   input += synapse0x8ce1108();
   input += synapse0x8ce1130();
   input += synapse0x8ce1158();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce0e00() {
   double input = input0x8ce0e00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce1180() {
   double input = 0.103561;
   input += synapse0x8ce1398();
   input += synapse0x8ce13c0();
   input += synapse0x8ce13e8();
   input += synapse0x8ce1410();
   input += synapse0x8ce1438();
   input += synapse0x8ce1460();
   input += synapse0x8ce1488();
   input += synapse0x8ce14b0();
   input += synapse0x8ce14d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce1180() {
   double input = input0x8ce1180();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce1500() {
   double input = -0.311739;
   input += synapse0x8ce1718();
   input += synapse0x8ce1740();
   input += synapse0x8ce1768();
   input += synapse0x8ce1790();
   input += synapse0x8ce17b8();
   input += synapse0x8ce17e0();
   input += synapse0x8ce1808();
   input += synapse0x8ce1830();
   input += synapse0x8ce1858();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce1500() {
   double input = input0x8ce1500();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce1880() {
   double input = 0.533856;
   input += synapse0x8ce1a98();
   input += synapse0x8ce1ac0();
   input += synapse0x8ce1ae8();
   input += synapse0x8ce1b10();
   input += synapse0x8ce1b38();
   input += synapse0x8ce1b60();
   input += synapse0x8ce1b88();
   input += synapse0x8ce1bb0();
   input += synapse0x8ce1bd8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce1880() {
   double input = input0x8ce1880();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce1c00() {
   double input = -0.140725;
   input += synapse0x8ce1e18();
   input += synapse0x8ce1e40();
   input += synapse0x8ce1e68();
   input += synapse0x8ce1e90();
   input += synapse0x8ce1eb8();
   input += synapse0x8ce1ee0();
   input += synapse0x8ce1f08();
   input += synapse0x8ce1f30();
   input += synapse0x8ce1f58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce1c00() {
   double input = input0x8ce1c00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce1f80() {
   double input = -0.68969;
   input += synapse0x8ce2198();
   input += synapse0x8ce21c0();
   input += synapse0x8ce21e8();
   input += synapse0x8ce2210();
   input += synapse0x8ce2238();
   input += synapse0x8ce2260();
   input += synapse0x8ce2288();
   input += synapse0x8ce22b0();
   input += synapse0x8ce22d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce1f80() {
   double input = input0x8ce1f80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce2300() {
   double input = 0.316456;
   input += synapse0x8ce2518();
   input += synapse0x8ce2540();
   input += synapse0x8ce2568();
   input += synapse0x8ce2590();
   input += synapse0x8ce25b8();
   input += synapse0x8ce25e0();
   input += synapse0x8ce2608();
   input += synapse0x8ce2630();
   input += synapse0x8ce2658();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce2300() {
   double input = input0x8ce2300();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce2680() {
   double input = -0.0553335;
   input += synapse0x8ce2898();
   input += synapse0x8ce28c0();
   input += synapse0x8ce28e8();
   input += synapse0x8ce2910();
   input += synapse0x8ce2938();
   input += synapse0x8ce2960();
   input += synapse0x8ce2988();
   input += synapse0x8ce29b0();
   input += synapse0x8ce29d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce2680() {
   double input = input0x8ce2680();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce2a00() {
   double input = -0.0623506;
   input += synapse0x8ce2c18();
   input += synapse0x8ce2c40();
   input += synapse0x8ce2c68();
   input += synapse0x8ce2c90();
   input += synapse0x8ce2cb8();
   input += synapse0x8ce2ce0();
   input += synapse0x8ce2d08();
   input += synapse0x8ce2d30();
   input += synapse0x8ce2d58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce2a00() {
   double input = input0x8ce2a00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce2d80() {
   double input = -0.315305;
   input += synapse0x8ce2f98();
   input += synapse0x8ce2fc0();
   input += synapse0x8ce2fe8();
   input += synapse0x8ce3010();
   input += synapse0x8ce3038();
   input += synapse0x8ce3060();
   input += synapse0x8ce3088();
   input += synapse0x8ce30b0();
   input += synapse0x8ce30d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce2d80() {
   double input = input0x8ce2d80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce3100() {
   double input = 1.13291;
   input += synapse0x8ce3318();
   input += synapse0x8ce3340();
   input += synapse0x8ce3368();
   input += synapse0x8ce3390();
   input += synapse0x8ce33b8();
   input += synapse0x8ce33e0();
   input += synapse0x8ce3408();
   input += synapse0x8ce3430();
   input += synapse0x8ce3458();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce3100() {
   double input = input0x8ce3100();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce3480() {
   double input = -0.296694;
   input += synapse0x8ce3698();
   input += synapse0x8ce36c0();
   input += synapse0x8ce36e8();
   input += synapse0x8ce3710();
   input += synapse0x8ce3738();
   input += synapse0x8ce3760();
   input += synapse0x8ce3788();
   input += synapse0x8ce37b0();
   input += synapse0x8ce37d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce3480() {
   double input = input0x8ce3480();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce3800() {
   double input = 0.399105;
   input += synapse0x8ce3a18();
   input += synapse0x8ce3a40();
   input += synapse0x8ce3a68();
   input += synapse0x8ce3a90();
   input += synapse0x8ce3ab8();
   input += synapse0x8ce3ae0();
   input += synapse0x8ce3b08();
   input += synapse0x8ce3b30();
   input += synapse0x8ce3b58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce3800() {
   double input = input0x8ce3800();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce3b80() {
   double input = -0.656156;
   input += synapse0x8ce3d98();
   input += synapse0x8ce3dc0();
   input += synapse0x8ce3de8();
   input += synapse0x8ce3e10();
   input += synapse0x8ce3e38();
   input += synapse0x8ce3e60();
   input += synapse0x8ce3e88();
   input += synapse0x8ce3eb0();
   input += synapse0x8ce3ed8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce3b80() {
   double input = input0x8ce3b80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce3f00() {
   double input = 0.837081;
   input += synapse0x8ce4118();
   input += synapse0x8ce4140();
   input += synapse0x8ce4168();
   input += synapse0x8ce4190();
   input += synapse0x8ce41b8();
   input += synapse0x8ce41e0();
   input += synapse0x8ce4208();
   input += synapse0x8ce4230();
   input += synapse0x8ce4258();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce3f00() {
   double input = input0x8ce3f00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce4280() {
   double input = -0.168622;
   input += synapse0x8ce4498();
   input += synapse0x8ce44c0();
   input += synapse0x8ce44e8();
   input += synapse0x8ce4510();
   input += synapse0x8ce4538();
   input += synapse0x8ce4560();
   input += synapse0x8ce4588();
   input += synapse0x8ce45b0();
   input += synapse0x8ce45d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce4280() {
   double input = input0x8ce4280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce4600() {
   double input = -0.135646;
   input += synapse0x8ce4818();
   input += synapse0x8ce4840();
   input += synapse0x8ce4868();
   input += synapse0x8ce4890();
   input += synapse0x8ce48b8();
   input += synapse0x8ce48e0();
   input += synapse0x8ce4908();
   input += synapse0x8ce4930();
   input += synapse0x8ce4958();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce4600() {
   double input = input0x8ce4600();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce4980() {
   double input = -0.458293;
   input += synapse0x8cdc2f8();
   input += synapse0x8cdc320();
   input += synapse0x8cdc348();
   input += synapse0x8cdc370();
   input += synapse0x8cdc398();
   input += synapse0x8cdc3c0();
   input += synapse0x8ce4da0();
   input += synapse0x8ce4dc8();
   input += synapse0x8ce4df0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce4980() {
   double input = input0x8ce4980();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce4e18() {
   double input = -0.0346571;
   input += synapse0x8ce5018();
   input += synapse0x8ce5040();
   input += synapse0x8ce5068();
   input += synapse0x8ce5090();
   input += synapse0x8ce50b8();
   input += synapse0x8ce50e0();
   input += synapse0x8ce5108();
   input += synapse0x8ce5130();
   input += synapse0x8ce5158();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce4e18() {
   double input = input0x8ce4e18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cde520() {
   double input = -1.79335;
   input += synapse0x8cde738();
   input += synapse0x8cde760();
   input += synapse0x8cde788();
   input += synapse0x8cde7b0();
   input += synapse0x8cde7d8();
   input += synapse0x8cde800();
   input += synapse0x8cde828();
   input += synapse0x8cde850();
   input += synapse0x8cde878();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cde520() {
   double input = input0x8cde520();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cde8a0() {
   double input = 0.157775;
   input += synapse0x8cdeab8();
   input += synapse0x8cdeae0();
   input += synapse0x8cdeb08();
   input += synapse0x8cdeb30();
   input += synapse0x8cdeb58();
   input += synapse0x8cdeb80();
   input += synapse0x8cdeba8();
   input += synapse0x8cdebd0();
   input += synapse0x8cdebf8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cde8a0() {
   double input = input0x8cde8a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce6188() {
   double input = -0.621583;
   input += synapse0x8ce62b0();
   input += synapse0x8ce62d8();
   input += synapse0x8ce6300();
   input += synapse0x8ce6328();
   input += synapse0x8ce6350();
   input += synapse0x8ce6378();
   input += synapse0x8ce63a0();
   input += synapse0x8ce63c8();
   input += synapse0x8ce63f0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce6188() {
   double input = input0x8ce6188();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce6418() {
   double input = -0.144224;
   input += synapse0x8ce6618();
   input += synapse0x8ce6640();
   input += synapse0x8ce6668();
   input += synapse0x8ce6690();
   input += synapse0x8ce66b8();
   input += synapse0x8ce66e0();
   input += synapse0x8ce6708();
   input += synapse0x8ce6730();
   input += synapse0x8ce6758();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce6418() {
   double input = input0x8ce6418();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce6780() {
   double input = 0.0558885;
   input += synapse0x8ce6998();
   input += synapse0x8ce69c0();
   input += synapse0x8ce69e8();
   input += synapse0x8ce6a10();
   input += synapse0x8ce6a38();
   input += synapse0x8ce6a60();
   input += synapse0x8ce6a88();
   input += synapse0x8ce6ab0();
   input += synapse0x8ce6ad8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce6780() {
   double input = input0x8ce6780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce6b00() {
   double input = 0.702784;
   input += synapse0x8ce6d18();
   input += synapse0x8ce6d40();
   input += synapse0x8ce6d68();
   input += synapse0x8ce6d90();
   input += synapse0x8ce6db8();
   input += synapse0x8ce6de0();
   input += synapse0x8ce6e08();
   input += synapse0x8ce6e30();
   input += synapse0x8ce6e58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce6b00() {
   double input = input0x8ce6b00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce6e80() {
   double input = -0.0730061;
   input += synapse0x8ce7098();
   input += synapse0x8cdf2f8();
   input += synapse0x8cdf320();
   input += synapse0x8cdf348();
   input += synapse0x8cdf578();
   input += synapse0x8cdf5a0();
   input += synapse0x8cdf7d0();
   input += synapse0x8cdf7f8();
   input += synapse0x8cdfa30();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce6e80() {
   double input = input0x8ce6e80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cdfa58() {
   double input = 0.759944;
   input += synapse0x8ce7d38();
   input += synapse0x8ce7d60();
   input += synapse0x8ce7d88();
   input += synapse0x8ce7db0();
   input += synapse0x8ce7dd8();
   input += synapse0x8ce7e00();
   input += synapse0x8ce7e28();
   input += synapse0x8ce7e50();
   input += synapse0x8ce7e78();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cdfa58() {
   double input = input0x8cdfa58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce7ea0() {
   double input = 0.31538;
   input += synapse0x8ce80a0();
   input += synapse0x8ce80c8();
   input += synapse0x8ce80f0();
   input += synapse0x8ce8118();
   input += synapse0x8ce8140();
   input += synapse0x8ce8168();
   input += synapse0x8ce8190();
   input += synapse0x8ce81b8();
   input += synapse0x8ce81e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce7ea0() {
   double input = input0x8ce7ea0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce8208() {
   double input = 0.297643;
   input += synapse0x8ce8420();
   input += synapse0x8ce8448();
   input += synapse0x8ce8470();
   input += synapse0x8ce8498();
   input += synapse0x8ce84c0();
   input += synapse0x8ce84e8();
   input += synapse0x8ce8510();
   input += synapse0x8ce8538();
   input += synapse0x8ce8560();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce8208() {
   double input = input0x8ce8208();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce8588() {
   double input = -0.781489;
   input += synapse0x8ce87a0();
   input += synapse0x8ce87c8();
   input += synapse0x8ce87f0();
   input += synapse0x8ce8818();
   input += synapse0x8ce8840();
   input += synapse0x8ce8868();
   input += synapse0x8ce8890();
   input += synapse0x8ce88b8();
   input += synapse0x8ce88e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce8588() {
   double input = input0x8ce8588();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce8908() {
   double input = -0.159213;
   input += synapse0x8ce8b20();
   input += synapse0x8ce8b48();
   input += synapse0x8ce8b70();
   input += synapse0x8ce8b98();
   input += synapse0x8ce8bc0();
   input += synapse0x8ce8be8();
   input += synapse0x8ce8c10();
   input += synapse0x8ce8c38();
   input += synapse0x8ce8c60();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce8908() {
   double input = input0x8ce8908();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce8c88() {
   double input = 0.0201638;
   input += synapse0x8ce8ea0();
   input += synapse0x8ce8ec8();
   input += synapse0x8ce8ef0();
   input += synapse0x8ce8f18();
   input += synapse0x8ce8f40();
   input += synapse0x8ce8f68();
   input += synapse0x8ce8f90();
   input += synapse0x8ce8fb8();
   input += synapse0x8ce8fe0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce8c88() {
   double input = input0x8ce8c88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce9008() {
   double input = 1.01931;
   input += synapse0x8ce9220();
   input += synapse0x8ce9248();
   input += synapse0x8ce9270();
   input += synapse0x8ce9298();
   input += synapse0x8ce92c0();
   input += synapse0x8ce92e8();
   input += synapse0x8ce9310();
   input += synapse0x8ce9338();
   input += synapse0x8ce9360();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce9008() {
   double input = input0x8ce9008();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce9388() {
   double input = 0.323701;
   input += synapse0x8ce95a0();
   input += synapse0x8ce95c8();
   input += synapse0x8ce95f0();
   input += synapse0x8ce9618();
   input += synapse0x8ce9640();
   input += synapse0x8ce9668();
   input += synapse0x8ce9690();
   input += synapse0x8ce96b8();
   input += synapse0x8ce96e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce9388() {
   double input = input0x8ce9388();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce9708() {
   double input = -0.162647;
   input += synapse0x8ce9920();
   input += synapse0x8ce9948();
   input += synapse0x8ce9970();
   input += synapse0x8ce9998();
   input += synapse0x8ce99c0();
   input += synapse0x8ce99e8();
   input += synapse0x8ce9a10();
   input += synapse0x8ce9a38();
   input += synapse0x8ce9a60();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce9708() {
   double input = input0x8ce9708();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce9a88() {
   double input = -0.112452;
   input += synapse0x8ce9ca0();
   input += synapse0x8ce9cc8();
   input += synapse0x8ce9cf0();
   input += synapse0x8ce9d18();
   input += synapse0x8ce9d40();
   input += synapse0x8ce9d68();
   input += synapse0x8ce9d90();
   input += synapse0x8ce9db8();
   input += synapse0x8ce9de0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce9a88() {
   double input = input0x8ce9a88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ce9e08() {
   double input = 0.199593;
   input += synapse0x8cea020();
   input += synapse0x8cea048();
   input += synapse0x8cea070();
   input += synapse0x8cea098();
   input += synapse0x8cea0c0();
   input += synapse0x8cea0e8();
   input += synapse0x8cea110();
   input += synapse0x8cea138();
   input += synapse0x8cea160();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ce9e08() {
   double input = input0x8ce9e08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cea188() {
   double input = 0.84577;
   input += synapse0x8cea3a0();
   input += synapse0x8cea3c8();
   input += synapse0x8cea3f0();
   input += synapse0x8cea418();
   input += synapse0x8cea440();
   input += synapse0x8cea468();
   input += synapse0x8cea490();
   input += synapse0x8cea4b8();
   input += synapse0x8cea4e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cea188() {
   double input = input0x8cea188();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cea508() {
   double input = -0.197802;
   input += synapse0x8cea720();
   input += synapse0x8cea748();
   input += synapse0x8cea770();
   input += synapse0x8cea798();
   input += synapse0x8cea7c0();
   input += synapse0x8cea7e8();
   input += synapse0x8cea810();
   input += synapse0x8cea838();
   input += synapse0x8cea860();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cea508() {
   double input = input0x8cea508();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cea888() {
   double input = -0.100206;
   input += synapse0x8ceaaa0();
   input += synapse0x8ceaac8();
   input += synapse0x8ceaaf0();
   input += synapse0x8ceab18();
   input += synapse0x8ceab40();
   input += synapse0x8ceab68();
   input += synapse0x8ceab90();
   input += synapse0x8ceabb8();
   input += synapse0x8ceabe0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cea888() {
   double input = input0x8cea888();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ceac08() {
   double input = -0.328626;
   input += synapse0x8ceae20();
   input += synapse0x8ceae48();
   input += synapse0x8ceae70();
   input += synapse0x8ceae98();
   input += synapse0x8ceaec0();
   input += synapse0x8ceaee8();
   input += synapse0x8ceaf10();
   input += synapse0x8ceaf38();
   input += synapse0x8ceaf60();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ceac08() {
   double input = input0x8ceac08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ceaf88() {
   double input = 0.26278;
   input += synapse0x8ceb1a0();
   input += synapse0x8ceb1c8();
   input += synapse0x8ceb1f0();
   input += synapse0x8ceb218();
   input += synapse0x8ceb240();
   input += synapse0x8ceb268();
   input += synapse0x8ceb290();
   input += synapse0x8ceb2b8();
   input += synapse0x8ceb2e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ceaf88() {
   double input = input0x8ceaf88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ceb308() {
   double input = 0.372432;
   input += synapse0x8ceb520();
   input += synapse0x8ceb548();
   input += synapse0x8ceb570();
   input += synapse0x8ceb598();
   input += synapse0x8ceb5c0();
   input += synapse0x8ceb5e8();
   input += synapse0x8ceb610();
   input += synapse0x8ceb638();
   input += synapse0x8ceb660();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ceb308() {
   double input = input0x8ceb308();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ceb688() {
   double input = -1.98882;
   input += synapse0x8ceb8a0();
   input += synapse0x8ceb8c8();
   input += synapse0x8ceb8f0();
   input += synapse0x8ceb918();
   input += synapse0x8ceb940();
   input += synapse0x8ceb968();
   input += synapse0x8ceb990();
   input += synapse0x8ceb9b8();
   input += synapse0x8ceb9e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ceb688() {
   double input = input0x8ceb688();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ceba08() {
   double input = 0.513028;
   input += synapse0x8cebc20();
   input += synapse0x8cebc48();
   input += synapse0x8cebc70();
   input += synapse0x8cebc98();
   input += synapse0x8cebcc0();
   input += synapse0x8cebce8();
   input += synapse0x8cebd10();
   input += synapse0x8cebd38();
   input += synapse0x8cebd60();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ceba08() {
   double input = input0x8ceba08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cebd88() {
   double input = 0.0946539;
   input += synapse0x8cebfa0();
   input += synapse0x8cebfc8();
   input += synapse0x8cebff0();
   input += synapse0x8cec018();
   input += synapse0x8cec040();
   input += synapse0x8cec068();
   input += synapse0x8cec090();
   input += synapse0x8cec0b8();
   input += synapse0x8cec0e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cebd88() {
   double input = input0x8cebd88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cec108() {
   double input = 0.31109;
   input += synapse0x8cec320();
   input += synapse0x8cec348();
   input += synapse0x8cec370();
   input += synapse0x8cec398();
   input += synapse0x8cec3c0();
   input += synapse0x8cec3e8();
   input += synapse0x8cec410();
   input += synapse0x8cec438();
   input += synapse0x8cec460();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cec108() {
   double input = input0x8cec108();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cec488() {
   double input = 0.0918818;
   input += synapse0x8cec6a0();
   input += synapse0x8cec6c8();
   input += synapse0x8cec6f0();
   input += synapse0x8cec718();
   input += synapse0x8cec740();
   input += synapse0x8cec768();
   input += synapse0x8cec790();
   input += synapse0x8cec7b8();
   input += synapse0x8cec7e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cec488() {
   double input = input0x8cec488();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cec808() {
   double input = -0.0621307;
   input += synapse0x8ceca20();
   input += synapse0x8ceca48();
   input += synapse0x8ceca70();
   input += synapse0x8ceca98();
   input += synapse0x8cecac0();
   input += synapse0x8cecae8();
   input += synapse0x8cecb10();
   input += synapse0x8cecb38();
   input += synapse0x8cecb60();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cec808() {
   double input = input0x8cec808();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cecb88() {
   double input = 0.000946702;
   input += synapse0x8cecda0();
   input += synapse0x8cecdc8();
   input += synapse0x8cecdf0();
   input += synapse0x8cece18();
   input += synapse0x8cece40();
   input += synapse0x8cece68();
   input += synapse0x8cece90();
   input += synapse0x8ceceb8();
   input += synapse0x8cecee0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cecb88() {
   double input = input0x8cecb88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cecf08() {
   double input = 0.431257;
   input += synapse0x8ced120();
   input += synapse0x8ced148();
   input += synapse0x8ced170();
   input += synapse0x8ced198();
   input += synapse0x8ced1c0();
   input += synapse0x8ced1e8();
   input += synapse0x8ced210();
   input += synapse0x8ced238();
   input += synapse0x8ced260();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cecf08() {
   double input = input0x8cecf08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ced288() {
   double input = 0.309139;
   input += synapse0x8ced4a0();
   input += synapse0x8ced4c8();
   input += synapse0x8ced4f0();
   input += synapse0x8ced518();
   input += synapse0x8ced540();
   input += synapse0x8ced568();
   input += synapse0x8ced590();
   input += synapse0x8ced5b8();
   input += synapse0x8ced5e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ced288() {
   double input = input0x8ced288();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ced608() {
   double input = 0.939592;
   input += synapse0x8ced820();
   input += synapse0x8ced848();
   input += synapse0x8ced870();
   input += synapse0x8ced898();
   input += synapse0x8ced8c0();
   input += synapse0x8ced8e8();
   input += synapse0x8ced910();
   input += synapse0x8ced938();
   input += synapse0x8ced960();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ced608() {
   double input = input0x8ced608();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ced988() {
   double input = 1.13635;
   input += synapse0x8cedba0();
   input += synapse0x8cedbc8();
   input += synapse0x8cedbf0();
   input += synapse0x8cedc18();
   input += synapse0x8cedc40();
   input += synapse0x8cedc68();
   input += synapse0x8cedc90();
   input += synapse0x8cedcb8();
   input += synapse0x8cedce0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ced988() {
   double input = input0x8ced988();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cedd08() {
   double input = -0.149081;
   input += synapse0x8cedf20();
   input += synapse0x8cedf48();
   input += synapse0x8cedf70();
   input += synapse0x8cedf98();
   input += synapse0x8cedfc0();
   input += synapse0x8cedfe8();
   input += synapse0x8cee010();
   input += synapse0x8cee038();
   input += synapse0x8cee060();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cedd08() {
   double input = input0x8cedd08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cee088() {
   double input = -0.476594;
   input += synapse0x8cee2a0();
   input += synapse0x8cee2c8();
   input += synapse0x8cee2f0();
   input += synapse0x8cee318();
   input += synapse0x8cee340();
   input += synapse0x8cee368();
   input += synapse0x8cee390();
   input += synapse0x8cee3b8();
   input += synapse0x8cee3e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cee088() {
   double input = input0x8cee088();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cee408() {
   double input = 0.306839;
   input += synapse0x8cee620();
   input += synapse0x8cee648();
   input += synapse0x8cee670();
   input += synapse0x8cee698();
   input += synapse0x8cee6c0();
   input += synapse0x8cee6e8();
   input += synapse0x8cee710();
   input += synapse0x8cee738();
   input += synapse0x8cee760();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cee408() {
   double input = input0x8cee408();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cee788() {
   double input = 0.285252;
   input += synapse0x8cee9a0();
   input += synapse0x8cee9c8();
   input += synapse0x8cee9f0();
   input += synapse0x8ceea18();
   input += synapse0x8ceea40();
   input += synapse0x8ceea68();
   input += synapse0x8ceea90();
   input += synapse0x8ceeab8();
   input += synapse0x8ceeae0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cee788() {
   double input = input0x8cee788();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ceeb08() {
   double input = 0.0116561;
   input += synapse0x8ceed20();
   input += synapse0x8ceed48();
   input += synapse0x8ceed70();
   input += synapse0x8ceed98();
   input += synapse0x8ceedc0();
   input += synapse0x8ceede8();
   input += synapse0x8ceee10();
   input += synapse0x8ceee38();
   input += synapse0x8ceee60();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ceeb08() {
   double input = input0x8ceeb08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8ceee88() {
   double input = -0.214131;
   input += synapse0x8cef0a0();
   input += synapse0x8cef0c8();
   input += synapse0x8cef0f0();
   input += synapse0x8cef118();
   input += synapse0x8cef140();
   input += synapse0x8cef168();
   input += synapse0x8cef190();
   input += synapse0x8cef1b8();
   input += synapse0x8cef1e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8ceee88() {
   double input = input0x8ceee88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cef208() {
   double input = 0.871273;
   input += synapse0x8cef420();
   input += synapse0x8cef448();
   input += synapse0x8cef470();
   input += synapse0x8cef498();
   input += synapse0x8cef4c0();
   input += synapse0x8cef4e8();
   input += synapse0x8cef510();
   input += synapse0x8cef538();
   input += synapse0x8cef560();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cef208() {
   double input = input0x8cef208();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cef588() {
   double input = 0.588776;
   input += synapse0x8cef7a0();
   input += synapse0x8cef7c8();
   input += synapse0x8cef7f0();
   input += synapse0x8cef818();
   input += synapse0x8cef840();
   input += synapse0x8cef868();
   input += synapse0x8cef890();
   input += synapse0x8cef8b8();
   input += synapse0x8cef8e0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cef588() {
   double input = input0x8cef588();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::input0x8cef908() {
   double input = -0.0279591;
   input += synapse0x8cefa30();
   input += synapse0x8cefa58();
   input += synapse0x8cefa80();
   input += synapse0x8cefaa8();
   input += synapse0x8cefad0();
   input += synapse0x8cefaf8();
   input += synapse0x8cefb20();
   input += synapse0x8cefb48();
   input += synapse0x8cefb70();
   input += synapse0x8cefb98();
   input += synapse0x8cefbc0();
   input += synapse0x8cefbe8();
   input += synapse0x8cefc10();
   input += synapse0x8cefc38();
   input += synapse0x8cefc60();
   input += synapse0x8cefc88();
   input += synapse0x8cefd38();
   input += synapse0x8cefd60();
   input += synapse0x8cefd88();
   input += synapse0x8cefdb0();
   input += synapse0x8cefdd8();
   input += synapse0x8cefe00();
   input += synapse0x8cefe28();
   input += synapse0x8cefe50();
   input += synapse0x8cefe78();
   input += synapse0x8cefea0();
   input += synapse0x8cefec8();
   input += synapse0x8cefef0();
   input += synapse0x8ceff18();
   input += synapse0x8ceff40();
   input += synapse0x8ceff68();
   input += synapse0x8ceff90();
   input += synapse0x8cefcb0();
   input += synapse0x8cefcd8();
   input += synapse0x8cefd00();
   input += synapse0x8cf00c0();
   input += synapse0x8cf00e8();
   input += synapse0x8cf0110();
   input += synapse0x8cf0138();
   input += synapse0x8cf0160();
   input += synapse0x8cf0188();
   input += synapse0x8cf01b0();
   input += synapse0x8cf01d8();
   input += synapse0x8cf0200();
   input += synapse0x8cf0228();
   input += synapse0x8cf0250();
   input += synapse0x8cf0278();
   input += synapse0x8cf02a0();
   input += synapse0x8cf02c8();
   input += synapse0x8cf02f0();
   input += synapse0x8cf0318();
   input += synapse0x8cf0340();
   input += synapse0x8cf0368();
   input += synapse0x8cf0390();
   input += synapse0x8cf03b8();
   input += synapse0x8cf03e0();
   input += synapse0x8cf0408();
   input += synapse0x8cf0430();
   input += synapse0x8cf0458();
   input += synapse0x8cf0480();
   input += synapse0x8cf04a8();
   input += synapse0x8cf04d0();
   input += synapse0x8cf04f8();
   input += synapse0x8cf0520();
   input += synapse0x8cd6cb8();
   input += synapse0x8ceffb8();
   input += synapse0x8ceffe0();
   input += synapse0x8cf0008();
   input += synapse0x8cf0030();
   input += synapse0x8cf0058();
   input += synapse0x8cf0080();
   input += synapse0x8cf0750();
   input += synapse0x8cf0778();
   input += synapse0x8cf07a0();
   input += synapse0x8cf07c8();
   input += synapse0x8cf07f0();
   input += synapse0x8cf0818();
   input += synapse0x8cf0840();
   input += synapse0x8cf0868();
   input += synapse0x8cf0890();
   input += synapse0x8cf08b8();
   input += synapse0x8cf08e0();
   input += synapse0x8cf0908();
   input += synapse0x8cf0930();
   input += synapse0x8cf0958();
   input += synapse0x8cf0980();
   input += synapse0x8cf09a8();
   input += synapse0x8cf09d0();
   input += synapse0x8cf09f8();
   input += synapse0x8cf0a20();
   input += synapse0x8cf0a48();
   input += synapse0x8cf0a70();
   input += synapse0x8cf0a98();
   input += synapse0x8cf0ac0();
   input += synapse0x8cf0ae8();
   input += synapse0x8cf0b10();
   input += synapse0x8cf0b38();
   input += synapse0x8cf0b60();
   input += synapse0x8cf0b88();
   input += synapse0x8cf0bb0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaX::neuron0x8cef908() {
   double input = input0x8cef908();
   return (input * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8bfff68() {
   return (neuron0x8cb64d8()*0.280572);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8bd14b8() {
   return (neuron0x8cb6690()*0.063065);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8bd0958() {
   return (neuron0x8cb6890()*0.0316185);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd6f58() {
   return (neuron0x8cb6a90()*0.119692);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd6f80() {
   return (neuron0x8c6af08()*0.517269);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd6fa8() {
   return (neuron0x8c6b120()*-0.376223);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd6fd0() {
   return (neuron0x8c6b338()*0.40038);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd6ff8() {
   return (neuron0x8c6b550()*-0.624242);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7020() {
   return (neuron0x8cd6a48()*-0.147182);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7248() {
   return (neuron0x8cb64d8()*-0.130808);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7270() {
   return (neuron0x8cb6690()*-0.131511);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7298() {
   return (neuron0x8cb6890()*-0.231217);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd72c0() {
   return (neuron0x8cb6a90()*0.292637);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd72e8() {
   return (neuron0x8c6af08()*0.337506);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7310() {
   return (neuron0x8c6b120()*0.261728);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7338() {
   return (neuron0x8c6b338()*0.0154299);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7360() {
   return (neuron0x8c6b550()*-0.468662);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7410() {
   return (neuron0x8cd6a48()*0.277399);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd75f0() {
   return (neuron0x8cb64d8()*0.949359);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7618() {
   return (neuron0x8cb6690()*-0.380977);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7640() {
   return (neuron0x8cb6890()*0.157071);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7668() {
   return (neuron0x8cb6a90()*0.429867);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7690() {
   return (neuron0x8c6af08()*0.220132);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd76b8() {
   return (neuron0x8c6b120()*0.0878722);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd76e0() {
   return (neuron0x8c6b338()*-0.224468);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7708() {
   return (neuron0x8c6b550()*-0.0983118);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7730() {
   return (neuron0x8cd6a48()*-0.390013);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7958() {
   return (neuron0x8cb64d8()*-0.249226);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7980() {
   return (neuron0x8cb6690()*0.121191);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd79a8() {
   return (neuron0x8cb6890()*0.292614);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd79d0() {
   return (neuron0x8cb6a90()*-0.378623);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd79f8() {
   return (neuron0x8c6af08()*-0.407524);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7a20() {
   return (neuron0x8c6b120()*0.102216);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cb6c60() {
   return (neuron0x8c6b338()*-0.0928005);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8c91178() {
   return (neuron0x8c6b550()*0.136553);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8c6b690() {
   return (neuron0x8cd6a48()*0.131144);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7d50() {
   return (neuron0x8cb64d8()*-0.256074);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7d78() {
   return (neuron0x8cb6690()*-0.143273);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7da0() {
   return (neuron0x8cb6890()*-0.338784);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7dc8() {
   return (neuron0x8cb6a90()*0.284742);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7df0() {
   return (neuron0x8c6af08()*0.314913);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7e18() {
   return (neuron0x8c6b120()*0.287626);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7e40() {
   return (neuron0x8c6b338()*-0.0912769);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7e68() {
   return (neuron0x8c6b550()*-0.476145);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7e90() {
   return (neuron0x8cd6a48()*0.260963);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd80b8() {
   return (neuron0x8cb64d8()*-0.169234);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd80e0() {
   return (neuron0x8cb6690()*0.0637419);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8108() {
   return (neuron0x8cb6890()*-0.233665);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8130() {
   return (neuron0x8cb6a90()*0.0283155);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8158() {
   return (neuron0x8c6af08()*0.332851);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8180() {
   return (neuron0x8c6b120()*0.0602391);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd81a8() {
   return (neuron0x8c6b338()*0.217794);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd81d0() {
   return (neuron0x8c6b550()*0.134657);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd81f8() {
   return (neuron0x8cd6a48()*-0.655222);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8420() {
   return (neuron0x8cb64d8()*-0.445421);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8448() {
   return (neuron0x8cb6690()*0.214805);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8470() {
   return (neuron0x8cb6890()*-0.172116);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8498() {
   return (neuron0x8cb6a90()*0.368658);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd84c0() {
   return (neuron0x8c6af08()*-0.390282);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd84e8() {
   return (neuron0x8c6b120()*-0.0377389);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8510() {
   return (neuron0x8c6b338()*0.21819);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8538() {
   return (neuron0x8c6b550()*-0.161044);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8560() {
   return (neuron0x8cd6a48()*0.128886);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8858108() {
   return (neuron0x8cb64d8()*0.285895);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8c002d8() {
   return (neuron0x8cb6690()*-0.6135);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8c00300() {
   return (neuron0x8cb6890()*-0.027541);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8bcf4d0() {
   return (neuron0x8cb6a90()*-0.449561);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8bcf0a0() {
   return (neuron0x8c6af08()*0.214889);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8bcb428() {
   return (neuron0x8c6b120()*-0.877261);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8bd0920() {
   return (neuron0x8c6b338()*0.0900177);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8bd1460() {
   return (neuron0x8c6b550()*1.182);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7a48() {
   return (neuron0x8cd6a48()*-0.0340022);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8b90() {
   return (neuron0x8cb64d8()*-0.469647);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8bb8() {
   return (neuron0x8cb6690()*-0.324874);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8be0() {
   return (neuron0x8cb6890()*0.160541);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8c08() {
   return (neuron0x8cb6a90()*-0.72907);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8c30() {
   return (neuron0x8c6af08()*0.239815);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8c58() {
   return (neuron0x8c6b120()*0.481729);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8c80() {
   return (neuron0x8c6b338()*-0.127522);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8ca8() {
   return (neuron0x8c6b550()*0.410724);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8cd0() {
   return (neuron0x8cd6a48()*0.0221407);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8ef8() {
   return (neuron0x8cb64d8()*0.0425757);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8f20() {
   return (neuron0x8cb6690()*0.00516554);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8f48() {
   return (neuron0x8cb6890()*0.316955);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8f70() {
   return (neuron0x8cb6a90()*0.385002);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8f98() {
   return (neuron0x8c6af08()*-0.213277);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8fc0() {
   return (neuron0x8c6b120()*0.464912);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8fe8() {
   return (neuron0x8c6b338()*0.0374035);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9010() {
   return (neuron0x8c6b550()*-0.610988);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9038() {
   return (neuron0x8cd6a48()*0.245856);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9260() {
   return (neuron0x8cb64d8()*0.401586);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9288() {
   return (neuron0x8cb6690()*0.254251);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd92b0() {
   return (neuron0x8cb6890()*-0.439252);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd92d8() {
   return (neuron0x8cb6a90()*0.392833);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9300() {
   return (neuron0x8c6af08()*-0.173845);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9328() {
   return (neuron0x8c6b120()*0.366552);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9350() {
   return (neuron0x8c6b338()*0.235164);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9378() {
   return (neuron0x8c6b550()*-0.418337);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd93a0() {
   return (neuron0x8cd6a48()*-0.0481981);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd95c8() {
   return (neuron0x8cb64d8()*0.0730807);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd95f0() {
   return (neuron0x8cb6690()*-0.46053);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9618() {
   return (neuron0x8cb6890()*-0.232864);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9640() {
   return (neuron0x8cb6a90()*0.889078);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9668() {
   return (neuron0x8c6af08()*-0.121767);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9690() {
   return (neuron0x8c6b120()*2.78093);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd96b8() {
   return (neuron0x8c6b338()*-0.0575926);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd96e0() {
   return (neuron0x8c6b550()*0.289704);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9708() {
   return (neuron0x8cd6a48()*-0.199141);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9930() {
   return (neuron0x8cb64d8()*0.078888);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9958() {
   return (neuron0x8cb6690()*0.959212);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9980() {
   return (neuron0x8cb6890()*-0.190927);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd99a8() {
   return (neuron0x8cb6a90()*0.181021);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd99d0() {
   return (neuron0x8c6af08()*-0.499082);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd99f8() {
   return (neuron0x8c6b120()*-1.59373);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9a20() {
   return (neuron0x8c6b338()*0.188894);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9a48() {
   return (neuron0x8c6b550()*-0.0579752);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9a70() {
   return (neuron0x8cd6a48()*0.0167488);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9c98() {
   return (neuron0x8cb64d8()*-0.346454);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9cc0() {
   return (neuron0x8cb6690()*-0.34506);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9ce8() {
   return (neuron0x8cb6890()*0.474373);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9d10() {
   return (neuron0x8cb6a90()*-0.231779);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9d38() {
   return (neuron0x8c6af08()*-0.300082);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9d60() {
   return (neuron0x8c6b120()*-0.00190281);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9d88() {
   return (neuron0x8c6b338()*0.354288);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9db0() {
   return (neuron0x8c6b550()*0.0404452);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd9dd8() {
   return (neuron0x8cd6a48()*-0.317132);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda018() {
   return (neuron0x8cb64d8()*0.472152);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda040() {
   return (neuron0x8cb6690()*-0.207822);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda068() {
   return (neuron0x8cb6890()*-0.230822);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7a70() {
   return (neuron0x8cb6a90()*0.39657);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7a98() {
   return (neuron0x8c6af08()*0.238648);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7ac0() {
   return (neuron0x8c6b120()*-0.820016);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7ae8() {
   return (neuron0x8c6b338()*-0.0521212);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7b10() {
   return (neuron0x8c6b550()*-0.909315);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8788() {
   return (neuron0x8cd6a48()*0.15157);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd8968() {
   return (neuron0x8cb64d8()*-0.152049);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda568() {
   return (neuron0x8cb6690()*-1.11251);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda618() {
   return (neuron0x8cb6890()*0.645844);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda6c8() {
   return (neuron0x8cb6a90()*-0.123778);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda778() {
   return (neuron0x8c6af08()*0.609182);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda828() {
   return (neuron0x8c6b120()*0.479952);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda8d8() {
   return (neuron0x8c6b338()*0.361909);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda988() {
   return (neuron0x8c6b550()*-0.31367);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaa38() {
   return (neuron0x8cd6a48()*0.333342);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdac28() {
   return (neuron0x8cb64d8()*-0.113269);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdac50() {
   return (neuron0x8cb6690()*0.397107);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdac78() {
   return (neuron0x8cb6890()*-0.609929);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaca0() {
   return (neuron0x8cb6a90()*0.230226);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdacc8() {
   return (neuron0x8c6af08()*0.235605);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdacf0() {
   return (neuron0x8c6b120()*-0.0576833);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdad18() {
   return (neuron0x8c6b338()*-0.47512);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdad40() {
   return (neuron0x8c6b550()*0.0271856);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdad68() {
   return (neuron0x8cd6a48()*-0.192607);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaed0() {
   return (neuron0x8cb64d8()*-0.104157);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaef8() {
   return (neuron0x8cb6690()*-0.827577);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaf20() {
   return (neuron0x8cb6890()*-0.16888);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaf48() {
   return (neuron0x8cb6a90()*-0.272673);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaf70() {
   return (neuron0x8c6af08()*-0.145573);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaf98() {
   return (neuron0x8c6b120()*-0.799278);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdafc0() {
   return (neuron0x8c6b338()*0.0549665);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdafe8() {
   return (neuron0x8c6b550()*-0.621031);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb010() {
   return (neuron0x8cd6a48()*-0.226087);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb178() {
   return (neuron0x8cb64d8()*-0.321199);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb1a0() {
   return (neuron0x8cb6690()*1.07323);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb1c8() {
   return (neuron0x8cb6890()*0.116223);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb1f0() {
   return (neuron0x8cb6a90()*1.40511);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb218() {
   return (neuron0x8c6af08()*-0.277379);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb240() {
   return (neuron0x8c6b120()*-1.55907);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb268() {
   return (neuron0x8c6b338()*-0.00302575);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb290() {
   return (neuron0x8c6b550()*-1.04175);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb2b8() {
   return (neuron0x8cd6a48()*-0.304734);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb4f8() {
   return (neuron0x8cb64d8()*-0.174913);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb520() {
   return (neuron0x8cb6690()*0.233801);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb548() {
   return (neuron0x8cb6890()*-0.142702);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb570() {
   return (neuron0x8cb6a90()*-0.406561);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb598() {
   return (neuron0x8c6af08()*-0.438036);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb5c0() {
   return (neuron0x8c6b120()*-0.0749283);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb5e8() {
   return (neuron0x8c6b338()*-0.474322);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb610() {
   return (neuron0x8c6b550()*0.410243);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb638() {
   return (neuron0x8cd6a48()*-0.0885241);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb878() {
   return (neuron0x8cb64d8()*0.158604);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb8a0() {
   return (neuron0x8cb6690()*-0.0696714);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb8c8() {
   return (neuron0x8cb6890()*-0.301962);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb8f0() {
   return (neuron0x8cb6a90()*0.0373972);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb918() {
   return (neuron0x8c6af08()*0.341594);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb940() {
   return (neuron0x8c6b120()*-0.109132);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb968() {
   return (neuron0x8c6b338()*-0.185839);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb990() {
   return (neuron0x8c6b550()*0.286461);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdb9b8() {
   return (neuron0x8cd6a48()*-0.107548);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbbf8() {
   return (neuron0x8cb64d8()*0.649307);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbc20() {
   return (neuron0x8cb6690()*0.163776);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbc48() {
   return (neuron0x8cb6890()*0.281181);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbc70() {
   return (neuron0x8cb6a90()*0.280551);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbc98() {
   return (neuron0x8c6af08()*0.0107555);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbcc0() {
   return (neuron0x8c6b120()*-0.256333);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbce8() {
   return (neuron0x8c6b338()*-0.488109);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbd10() {
   return (neuron0x8c6b550()*-0.459418);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbd38() {
   return (neuron0x8cd6a48()*-0.381358);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbf78() {
   return (neuron0x8cb64d8()*-0.152983);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbfa0() {
   return (neuron0x8cb6690()*1.04127);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbfc8() {
   return (neuron0x8cb6890()*0.251019);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdbff0() {
   return (neuron0x8cb6a90()*0.434047);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc018() {
   return (neuron0x8c6af08()*-0.0618254);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc040() {
   return (neuron0x8c6b120()*-0.694854);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc068() {
   return (neuron0x8c6b338()*0.0977046);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc090() {
   return (neuron0x8c6b550()*0.779465);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc0b8() {
   return (neuron0x8cd6a48()*0.0630397);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd7388() {
   return (neuron0x8cb64d8()*0.074185);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd73b0() {
   return (neuron0x8cb6690()*0.458654);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd73d8() {
   return (neuron0x8cb6890()*0.167607);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc400() {
   return (neuron0x8cb6a90()*-0.0197121);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc428() {
   return (neuron0x8c6af08()*0.41652);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc450() {
   return (neuron0x8c6b120()*-1.1031);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc478() {
   return (neuron0x8c6b338()*-0.0260443);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc4a0() {
   return (neuron0x8c6b550()*0.224254);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc4c8() {
   return (neuron0x8cd6a48()*0.0135895);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc708() {
   return (neuron0x8cb64d8()*0.0832294);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc730() {
   return (neuron0x8cb6690()*-0.188139);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc758() {
   return (neuron0x8cb6890()*0.0715287);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdd730() {
   return (neuron0x8cb6a90()*-0.306122);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdd758() {
   return (neuron0x8c6af08()*0.254264);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdd780() {
   return (neuron0x8c6b120()*-0.11371);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdd7a8() {
   return (neuron0x8c6b338()*0.0919628);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdd7d0() {
   return (neuron0x8c6b550()*0.354632);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdd7f8() {
   return (neuron0x8cd6a48()*-0.190597);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdda20() {
   return (neuron0x8cb64d8()*0.0621059);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdda48() {
   return (neuron0x8cb6690()*-0.545754);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdda70() {
   return (neuron0x8cb6890()*0.209479);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdda98() {
   return (neuron0x8cb6a90()*0.433173);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cddac0() {
   return (neuron0x8c6af08()*0.379477);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cddae8() {
   return (neuron0x8c6b120()*0.535242);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cddb10() {
   return (neuron0x8c6b338()*0.0993487);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cddb38() {
   return (neuron0x8c6b550()*0.614664);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cddb60() {
   return (neuron0x8cd6a48()*0.535837);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cddd88() {
   return (neuron0x8cb64d8()*-0.234126);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdddb0() {
   return (neuron0x8cb6690()*-0.0414718);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdddd8() {
   return (neuron0x8cb6890()*1.90845);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdde00() {
   return (neuron0x8cb6a90()*0.0414314);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdde28() {
   return (neuron0x8c6af08()*0.0710652);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdde50() {
   return (neuron0x8c6b120()*-0.0888106);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdde78() {
   return (neuron0x8c6b338()*-0.229288);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cddea0() {
   return (neuron0x8c6b550()*-0.456384);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cddec8() {
   return (neuron0x8cd6a48()*0.289097);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde0f0() {
   return (neuron0x8cb64d8()*0.533458);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde118() {
   return (neuron0x8cb6690()*-0.326335);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde140() {
   return (neuron0x8cb6890()*0.160568);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde168() {
   return (neuron0x8cb6a90()*0.346939);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde190() {
   return (neuron0x8c6af08()*-0.140059);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde1b8() {
   return (neuron0x8c6b120()*-0.0873673);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde1e0() {
   return (neuron0x8c6b338()*-0.101494);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde208() {
   return (neuron0x8c6b550()*-0.294373);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde230() {
   return (neuron0x8cd6a48()*0.381001);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde458() {
   return (neuron0x8cb64d8()*-0.0390833);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde480() {
   return (neuron0x8cb6690()*-0.25321);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde4a8() {
   return (neuron0x8cb6890()*0.608265);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde4d0() {
   return (neuron0x8cb6a90()*0.546593);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde4f8() {
   return (neuron0x8c6af08()*0.120463);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda090() {
   return (neuron0x8c6b120()*-0.161335);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda0b8() {
   return (neuron0x8c6b338()*-0.0235323);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda0e0() {
   return (neuron0x8c6b550()*-0.242194);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda108() {
   return (neuron0x8cd6a48()*0.322603);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda348() {
   return (neuron0x8cb64d8()*0.335394);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda370() {
   return (neuron0x8cb6690()*-0.177351);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda398() {
   return (neuron0x8cb6890()*-0.172872);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda3c0() {
   return (neuron0x8cb6a90()*-0.342223);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda3e8() {
   return (neuron0x8c6af08()*-0.23995);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda410() {
   return (neuron0x8c6b120()*0.545012);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda438() {
   return (neuron0x8c6b338()*-0.14756);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda460() {
   return (neuron0x8c6b550()*-0.404024);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cded28() {
   return (neuron0x8cd6a48()*0.151044);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdef50() {
   return (neuron0x8cb64d8()*0.191417);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdef78() {
   return (neuron0x8cb6690()*0.212751);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdefa0() {
   return (neuron0x8cb6890()*0.56331);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdefc8() {
   return (neuron0x8cb6a90()*-0.503053);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdeff0() {
   return (neuron0x8c6af08()*0.0678643);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf018() {
   return (neuron0x8c6b120()*-0.595047);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf040() {
   return (neuron0x8c6b338()*0.351793);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf068() {
   return (neuron0x8c6b550()*0.372239);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf090() {
   return (neuron0x8cd6a48()*0.476645);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf2d0() {
   return (neuron0x8cb64d8()*0.360544);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda4e0() {
   return (neuron0x8cb6690()*-0.483633);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda508() {
   return (neuron0x8cb6890()*-0.529168);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda530() {
   return (neuron0x8cb6a90()*0.680452);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda590() {
   return (neuron0x8c6af08()*0.386605);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda5b8() {
   return (neuron0x8c6b120()*-0.165006);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda5e0() {
   return (neuron0x8c6b338()*0.32852);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda640() {
   return (neuron0x8c6b550()*-0.973717);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda668() {
   return (neuron0x8cd6a48()*-0.235754);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda898() {
   return (neuron0x8cb64d8()*0.0668146);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda738() {
   return (neuron0x8cb6690()*-0.361226);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda7e8() {
   return (neuron0x8cb6890()*-0.921318);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda900() {
   return (neuron0x8cb6a90()*0.358076);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda928() {
   return (neuron0x8c6af08()*0.226797);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda950() {
   return (neuron0x8c6b120()*0.844576);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda9b0() {
   return (neuron0x8c6b338()*0.0252854);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cda9d8() {
   return (neuron0x8c6b550()*-0.0139289);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaa00() {
   return (neuron0x8cd6a48()*0.414959);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdaaa8() {
   return (neuron0x8cb64d8()*0.467676);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdff20() {
   return (neuron0x8cb6690()*-0.282609);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdff48() {
   return (neuron0x8cb6890()*-0.0316205);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdff70() {
   return (neuron0x8cb6a90()*0.0372331);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdff98() {
   return (neuron0x8c6af08()*-0.182456);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdffc0() {
   return (neuron0x8c6b120()*0.179637);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdffe8() {
   return (neuron0x8c6b338()*0.184084);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0010() {
   return (neuron0x8c6b550()*-0.105666);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0038() {
   return (neuron0x8cd6a48()*0.294305);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0260() {
   return (neuron0x8cb64d8()*-0.133741);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0288() {
   return (neuron0x8cb6690()*-0.0872586);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce02b0() {
   return (neuron0x8cb6890()*-0.232966);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce02d8() {
   return (neuron0x8cb6a90()*0.0647156);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0300() {
   return (neuron0x8c6af08()*-0.322716);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0328() {
   return (neuron0x8c6b120()*-0.24347);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0350() {
   return (neuron0x8c6b338()*-0.144856);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0378() {
   return (neuron0x8c6b550()*-0.0364759);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce03a0() {
   return (neuron0x8cd6a48()*0.0953355);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce05c8() {
   return (neuron0x8cb64d8()*0.178701);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce05f0() {
   return (neuron0x8cb6690()*-0.517344);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0618() {
   return (neuron0x8cb6890()*-0.200714);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0640() {
   return (neuron0x8cb6a90()*0.926307);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0668() {
   return (neuron0x8c6af08()*-0.0712717);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0690() {
   return (neuron0x8c6b120()*-0.126313);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce06b8() {
   return (neuron0x8c6b338()*-0.236353);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce06e0() {
   return (neuron0x8c6b550()*-0.801984);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0708() {
   return (neuron0x8cd6a48()*0.301284);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0930() {
   return (neuron0x8cb64d8()*-0.188512);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0958() {
   return (neuron0x8cb6690()*-0.144948);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0980() {
   return (neuron0x8cb6890()*0.115192);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce09a8() {
   return (neuron0x8cb6a90()*-0.204057);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce09d0() {
   return (neuron0x8c6af08()*0.318916);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce09f8() {
   return (neuron0x8c6b120()*-0.112363);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0a20() {
   return (neuron0x8c6b338()*0.357369);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0a48() {
   return (neuron0x8c6b550()*0.714598);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0a70() {
   return (neuron0x8cd6a48()*0.568046);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0c98() {
   return (neuron0x8cb64d8()*-0.203886);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0cc0() {
   return (neuron0x8cb6690()*0.198968);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0ce8() {
   return (neuron0x8cb6890()*-0.128978);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0d10() {
   return (neuron0x8cb6a90()*0.159662);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0d38() {
   return (neuron0x8c6af08()*0.587521);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0d60() {
   return (neuron0x8c6b120()*-0.331859);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0d88() {
   return (neuron0x8c6b338()*-0.284395);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0db0() {
   return (neuron0x8c6b550()*-0.0382557);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce0dd8() {
   return (neuron0x8cd6a48()*0.194035);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1018() {
   return (neuron0x8cb64d8()*-0.131232);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1040() {
   return (neuron0x8cb6690()*0.497925);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1068() {
   return (neuron0x8cb6890()*-0.396677);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1090() {
   return (neuron0x8cb6a90()*-0.200521);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce10b8() {
   return (neuron0x8c6af08()*-0.0388444);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce10e0() {
   return (neuron0x8c6b120()*-0.0439355);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1108() {
   return (neuron0x8c6b338()*-0.522179);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1130() {
   return (neuron0x8c6b550()*0.539996);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1158() {
   return (neuron0x8cd6a48()*0.444989);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1398() {
   return (neuron0x8cb64d8()*-0.147372);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce13c0() {
   return (neuron0x8cb6690()*-0.186605);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce13e8() {
   return (neuron0x8cb6890()*0.485626);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1410() {
   return (neuron0x8cb6a90()*-0.070518);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1438() {
   return (neuron0x8c6af08()*-0.102384);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1460() {
   return (neuron0x8c6b120()*-0.129827);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1488() {
   return (neuron0x8c6b338()*0.315237);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce14b0() {
   return (neuron0x8c6b550()*0.411272);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce14d8() {
   return (neuron0x8cd6a48()*0.376241);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1718() {
   return (neuron0x8cb64d8()*0.121399);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1740() {
   return (neuron0x8cb6690()*0.191297);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1768() {
   return (neuron0x8cb6890()*0.5064);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1790() {
   return (neuron0x8cb6a90()*-0.2164);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce17b8() {
   return (neuron0x8c6af08()*0.581526);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce17e0() {
   return (neuron0x8c6b120()*0.269822);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1808() {
   return (neuron0x8c6b338()*-0.162058);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1830() {
   return (neuron0x8c6b550()*-0.359372);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1858() {
   return (neuron0x8cd6a48()*-0.751608);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1a98() {
   return (neuron0x8cb64d8()*-0.13612);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1ac0() {
   return (neuron0x8cb6690()*0.194992);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1ae8() {
   return (neuron0x8cb6890()*-1.77299);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1b10() {
   return (neuron0x8cb6a90()*-0.578157);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1b38() {
   return (neuron0x8c6af08()*-0.0184056);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1b60() {
   return (neuron0x8c6b120()*-0.0384418);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1b88() {
   return (neuron0x8c6b338()*-0.143781);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1bb0() {
   return (neuron0x8c6b550()*-0.450056);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1bd8() {
   return (neuron0x8cd6a48()*0.293975);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1e18() {
   return (neuron0x8cb64d8()*-0.256508);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1e40() {
   return (neuron0x8cb6690()*0.429297);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1e68() {
   return (neuron0x8cb6890()*0.195275);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1e90() {
   return (neuron0x8cb6a90()*0.246459);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1eb8() {
   return (neuron0x8c6af08()*-0.491595);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1ee0() {
   return (neuron0x8c6b120()*0.0694714);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1f08() {
   return (neuron0x8c6b338()*-0.526237);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1f30() {
   return (neuron0x8c6b550()*0.4507);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce1f58() {
   return (neuron0x8cd6a48()*-0.372053);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2198() {
   return (neuron0x8cb64d8()*0.0185033);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce21c0() {
   return (neuron0x8cb6690()*0.649588);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce21e8() {
   return (neuron0x8cb6890()*-0.0200375);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2210() {
   return (neuron0x8cb6a90()*-0.494361);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2238() {
   return (neuron0x8c6af08()*-0.718948);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2260() {
   return (neuron0x8c6b120()*-0.0185255);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2288() {
   return (neuron0x8c6b338()*-0.0227412);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce22b0() {
   return (neuron0x8c6b550()*-1.00772);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce22d8() {
   return (neuron0x8cd6a48()*-0.415397);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2518() {
   return (neuron0x8cb64d8()*0.0171557);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2540() {
   return (neuron0x8cb6690()*-0.18296);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2568() {
   return (neuron0x8cb6890()*0.0720999);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2590() {
   return (neuron0x8cb6a90()*-0.121619);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce25b8() {
   return (neuron0x8c6af08()*-0.273934);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce25e0() {
   return (neuron0x8c6b120()*-0.213386);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2608() {
   return (neuron0x8c6b338()*0.166054);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2630() {
   return (neuron0x8c6b550()*-0.908745);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2658() {
   return (neuron0x8cd6a48()*0.179881);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2898() {
   return (neuron0x8cb64d8()*-0.196431);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce28c0() {
   return (neuron0x8cb6690()*-0.0809934);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce28e8() {
   return (neuron0x8cb6890()*-0.287128);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2910() {
   return (neuron0x8cb6a90()*0.298934);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2938() {
   return (neuron0x8c6af08()*0.111983);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2960() {
   return (neuron0x8c6b120()*0.0970666);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2988() {
   return (neuron0x8c6b338()*0.32242);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce29b0() {
   return (neuron0x8c6b550()*0.186659);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce29d8() {
   return (neuron0x8cd6a48()*-0.442989);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2c18() {
   return (neuron0x8cb64d8()*-0.0414414);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2c40() {
   return (neuron0x8cb6690()*-0.254256);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2c68() {
   return (neuron0x8cb6890()*-0.090956);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2c90() {
   return (neuron0x8cb6a90()*-0.366325);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2cb8() {
   return (neuron0x8c6af08()*-0.143302);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2ce0() {
   return (neuron0x8c6b120()*-0.151356);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2d08() {
   return (neuron0x8c6b338()*0.0484711);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2d30() {
   return (neuron0x8c6b550()*0.358988);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2d58() {
   return (neuron0x8cd6a48()*0.0196715);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2f98() {
   return (neuron0x8cb64d8()*0.101635);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2fc0() {
   return (neuron0x8cb6690()*0.293102);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce2fe8() {
   return (neuron0x8cb6890()*-0.437897);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3010() {
   return (neuron0x8cb6a90()*0.001055);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3038() {
   return (neuron0x8c6af08()*0.823685);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3060() {
   return (neuron0x8c6b120()*-0.0670289);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3088() {
   return (neuron0x8c6b338()*0.745121);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce30b0() {
   return (neuron0x8c6b550()*-0.0702414);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce30d8() {
   return (neuron0x8cd6a48()*-0.414284);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3318() {
   return (neuron0x8cb64d8()*-0.286692);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3340() {
   return (neuron0x8cb6690()*-0.847438);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3368() {
   return (neuron0x8cb6890()*0.130622);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3390() {
   return (neuron0x8cb6a90()*-0.428331);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce33b8() {
   return (neuron0x8c6af08()*0.172799);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce33e0() {
   return (neuron0x8c6b120()*2.42145);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3408() {
   return (neuron0x8c6b338()*-0.115717);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3430() {
   return (neuron0x8c6b550()*-1.34497);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3458() {
   return (neuron0x8cd6a48()*0.130969);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3698() {
   return (neuron0x8cb64d8()*-0.525026);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce36c0() {
   return (neuron0x8cb6690()*-0.098056);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce36e8() {
   return (neuron0x8cb6890()*0.289677);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3710() {
   return (neuron0x8cb6a90()*0.426834);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3738() {
   return (neuron0x8c6af08()*0.192655);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3760() {
   return (neuron0x8c6b120()*-0.448038);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3788() {
   return (neuron0x8c6b338()*0.0439501);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce37b0() {
   return (neuron0x8c6b550()*0.39897);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce37d8() {
   return (neuron0x8cd6a48()*0.42132);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3a18() {
   return (neuron0x8cb64d8()*-0.490212);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3a40() {
   return (neuron0x8cb6690()*-0.189733);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3a68() {
   return (neuron0x8cb6890()*0.160569);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3a90() {
   return (neuron0x8cb6a90()*0.409722);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3ab8() {
   return (neuron0x8c6af08()*-0.304468);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3ae0() {
   return (neuron0x8c6b120()*-0.27984);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3b08() {
   return (neuron0x8c6b338()*0.128633);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3b30() {
   return (neuron0x8c6b550()*0.78044);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3b58() {
   return (neuron0x8cd6a48()*0.208751);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3d98() {
   return (neuron0x8cb64d8()*-0.0537154);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3dc0() {
   return (neuron0x8cb6690()*0.536573);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3de8() {
   return (neuron0x8cb6890()*-0.288114);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3e10() {
   return (neuron0x8cb6a90()*-0.882182);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3e38() {
   return (neuron0x8c6af08()*-0.14586);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3e60() {
   return (neuron0x8c6b120()*0.390713);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3e88() {
   return (neuron0x8c6b338()*0.0550225);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3eb0() {
   return (neuron0x8c6b550()*0.594065);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce3ed8() {
   return (neuron0x8cd6a48()*0.323278);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4118() {
   return (neuron0x8cb64d8()*-0.140905);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4140() {
   return (neuron0x8cb6690()*-1.04199);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4168() {
   return (neuron0x8cb6890()*0.0756106);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4190() {
   return (neuron0x8cb6a90()*-0.56259);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce41b8() {
   return (neuron0x8c6af08()*0.0572711);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce41e0() {
   return (neuron0x8c6b120()*-1.60484);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4208() {
   return (neuron0x8c6b338()*-0.128964);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4230() {
   return (neuron0x8c6b550()*0.559085);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4258() {
   return (neuron0x8cd6a48()*0.341401);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4498() {
   return (neuron0x8cb64d8()*0.0995753);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce44c0() {
   return (neuron0x8cb6690()*0.549191);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce44e8() {
   return (neuron0x8cb6890()*-0.520214);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4510() {
   return (neuron0x8cb6a90()*0.351052);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4538() {
   return (neuron0x8c6af08()*-0.332981);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4560() {
   return (neuron0x8c6b120()*-0.191233);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4588() {
   return (neuron0x8c6b338()*0.341507);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce45b0() {
   return (neuron0x8c6b550()*0.552207);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce45d8() {
   return (neuron0x8cd6a48()*-0.304994);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4818() {
   return (neuron0x8cb64d8()*-0.0603982);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4840() {
   return (neuron0x8cb6690()*0.0353958);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4868() {
   return (neuron0x8cb6890()*0.641373);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4890() {
   return (neuron0x8cb6a90()*-0.409452);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce48b8() {
   return (neuron0x8c6af08()*-0.110631);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce48e0() {
   return (neuron0x8c6b120()*0.110531);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4908() {
   return (neuron0x8c6b338()*-0.0146037);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4930() {
   return (neuron0x8c6b550()*0.960854);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4958() {
   return (neuron0x8cd6a48()*-0.305317);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc2f8() {
   return (neuron0x8cb64d8()*-0.230341);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc320() {
   return (neuron0x8cb6690()*-0.173847);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc348() {
   return (neuron0x8cb6890()*-0.087466);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc370() {
   return (neuron0x8cb6a90()*-0.430976);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc398() {
   return (neuron0x8c6af08()*-0.0395801);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdc3c0() {
   return (neuron0x8c6b120()*1.83025);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4da0() {
   return (neuron0x8c6b338()*0.173075);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4dc8() {
   return (neuron0x8c6b550()*-0.360747);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce4df0() {
   return (neuron0x8cd6a48()*-0.240031);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce5018() {
   return (neuron0x8cb64d8()*0.271873);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce5040() {
   return (neuron0x8cb6690()*-0.360472);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce5068() {
   return (neuron0x8cb6890()*-0.400386);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce5090() {
   return (neuron0x8cb6a90()*0.107105);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce50b8() {
   return (neuron0x8c6af08()*-0.512208);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce50e0() {
   return (neuron0x8c6b120()*0.513923);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce5108() {
   return (neuron0x8c6b338()*0.374206);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce5130() {
   return (neuron0x8c6b550()*-0.272259);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce5158() {
   return (neuron0x8cd6a48()*0.107782);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde738() {
   return (neuron0x8cb64d8()*0.0482809);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde760() {
   return (neuron0x8cb6690()*-0.0820519);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde788() {
   return (neuron0x8cb6890()*8.92115);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde7b0() {
   return (neuron0x8cb6a90()*0.0730763);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde7d8() {
   return (neuron0x8c6af08()*0.293451);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde800() {
   return (neuron0x8c6b120()*-0.0816071);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde828() {
   return (neuron0x8c6b338()*0.106307);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde850() {
   return (neuron0x8c6b550()*-0.0421763);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cde878() {
   return (neuron0x8cd6a48()*0.382015);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdeab8() {
   return (neuron0x8cb64d8()*-0.0530288);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdeae0() {
   return (neuron0x8cb6690()*-0.0265805);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdeb08() {
   return (neuron0x8cb6890()*-0.660248);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdeb30() {
   return (neuron0x8cb6a90()*0.189184);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdeb58() {
   return (neuron0x8c6af08()*0.131194);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdeb80() {
   return (neuron0x8c6b120()*-0.731369);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdeba8() {
   return (neuron0x8c6b338()*0.207617);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdebd0() {
   return (neuron0x8c6b550()*0.0258514);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdebf8() {
   return (neuron0x8cd6a48()*-0.0833541);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce62b0() {
   return (neuron0x8cb64d8()*0.0969388);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce62d8() {
   return (neuron0x8cb6690()*0.223867);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6300() {
   return (neuron0x8cb6890()*-0.166813);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6328() {
   return (neuron0x8cb6a90()*-0.787489);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6350() {
   return (neuron0x8c6af08()*0.332864);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6378() {
   return (neuron0x8c6b120()*-1.34026);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce63a0() {
   return (neuron0x8c6b338()*0.100616);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce63c8() {
   return (neuron0x8c6b550()*-0.0382474);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce63f0() {
   return (neuron0x8cd6a48()*-0.481364);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6618() {
   return (neuron0x8cb64d8()*0.207607);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6640() {
   return (neuron0x8cb6690()*-0.424906);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6668() {
   return (neuron0x8cb6890()*-0.62869);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6690() {
   return (neuron0x8cb6a90()*-0.222601);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce66b8() {
   return (neuron0x8c6af08()*-0.0867441);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce66e0() {
   return (neuron0x8c6b120()*-0.534013);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6708() {
   return (neuron0x8c6b338()*-0.650131);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6730() {
   return (neuron0x8c6b550()*0.614878);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6758() {
   return (neuron0x8cd6a48()*0.0742738);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6998() {
   return (neuron0x8cb64d8()*-0.219819);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce69c0() {
   return (neuron0x8cb6690()*-0.374337);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce69e8() {
   return (neuron0x8cb6890()*0.088696);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6a10() {
   return (neuron0x8cb6a90()*-0.101749);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6a38() {
   return (neuron0x8c6af08()*0.806608);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6a60() {
   return (neuron0x8c6b120()*-0.00392648);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6a88() {
   return (neuron0x8c6b338()*-0.0661222);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6ab0() {
   return (neuron0x8c6b550()*-0.757542);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6ad8() {
   return (neuron0x8cd6a48()*-0.750481);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6d18() {
   return (neuron0x8cb64d8()*-0.107992);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6d40() {
   return (neuron0x8cb6690()*-0.378838);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6d68() {
   return (neuron0x8cb6890()*0.25835);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6d90() {
   return (neuron0x8cb6a90()*0.402075);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6db8() {
   return (neuron0x8c6af08()*0.306977);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6de0() {
   return (neuron0x8c6b120()*1.56024);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6e08() {
   return (neuron0x8c6b338()*-0.0371685);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6e30() {
   return (neuron0x8c6b550()*0.543822);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce6e58() {
   return (neuron0x8cd6a48()*0.136406);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7098() {
   return (neuron0x8cb64d8()*-0.234863);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf2f8() {
   return (neuron0x8cb6690()*-0.337049);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf320() {
   return (neuron0x8cb6890()*0.432155);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf348() {
   return (neuron0x8cb6a90()*-0.0500499);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf578() {
   return (neuron0x8c6af08()*0.179181);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf5a0() {
   return (neuron0x8c6b120()*-0.519259);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf7d0() {
   return (neuron0x8c6b338()*-0.26482);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdf7f8() {
   return (neuron0x8c6b550()*-0.157533);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cdfa30() {
   return (neuron0x8cd6a48()*-0.126769);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7d38() {
   return (neuron0x8cb64d8()*0.266822);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7d60() {
   return (neuron0x8cb6690()*-0.342108);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7d88() {
   return (neuron0x8cb6890()*-0.0218628);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7db0() {
   return (neuron0x8cb6a90()*-0.716735);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7dd8() {
   return (neuron0x8c6af08()*-0.182508);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7e00() {
   return (neuron0x8c6b120()*3.95974);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7e28() {
   return (neuron0x8c6b338()*-0.0122295);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7e50() {
   return (neuron0x8c6b550()*-0.293172);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce7e78() {
   return (neuron0x8cd6a48()*-0.131843);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce80a0() {
   return (neuron0x8cb64d8()*0.290197);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce80c8() {
   return (neuron0x8cb6690()*0.501026);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce80f0() {
   return (neuron0x8cb6890()*-0.0826249);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8118() {
   return (neuron0x8cb6a90()*-0.123357);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8140() {
   return (neuron0x8c6af08()*0.0350093);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8168() {
   return (neuron0x8c6b120()*-0.00830132);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8190() {
   return (neuron0x8c6b338()*-0.142769);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce81b8() {
   return (neuron0x8c6b550()*0.501232);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce81e0() {
   return (neuron0x8cd6a48()*0.467755);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8420() {
   return (neuron0x8cb64d8()*-0.361891);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8448() {
   return (neuron0x8cb6690()*-0.314871);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8470() {
   return (neuron0x8cb6890()*0.336526);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8498() {
   return (neuron0x8cb6a90()*0.0125254);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce84c0() {
   return (neuron0x8c6af08()*0.505501);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce84e8() {
   return (neuron0x8c6b120()*-0.490594);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8510() {
   return (neuron0x8c6b338()*-0.204975);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8538() {
   return (neuron0x8c6b550()*0.599595);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8560() {
   return (neuron0x8cd6a48()*0.118482);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce87a0() {
   return (neuron0x8cb64d8()*-0.588627);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce87c8() {
   return (neuron0x8cb6690()*-0.390824);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce87f0() {
   return (neuron0x8cb6890()*0.433749);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8818() {
   return (neuron0x8cb6a90()*-0.252648);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8840() {
   return (neuron0x8c6af08()*0.0188931);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8868() {
   return (neuron0x8c6b120()*0.0123704);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8890() {
   return (neuron0x8c6b338()*0.210326);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce88b8() {
   return (neuron0x8c6b550()*0.110742);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce88e0() {
   return (neuron0x8cd6a48()*0.00383399);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8b20() {
   return (neuron0x8cb64d8()*-0.214306);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8b48() {
   return (neuron0x8cb6690()*0.128977);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8b70() {
   return (neuron0x8cb6890()*0.276206);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8b98() {
   return (neuron0x8cb6a90()*0.162823);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8bc0() {
   return (neuron0x8c6af08()*0.348071);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8be8() {
   return (neuron0x8c6b120()*-0.918788);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8c10() {
   return (neuron0x8c6b338()*0.270292);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8c38() {
   return (neuron0x8c6b550()*0.521706);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8c60() {
   return (neuron0x8cd6a48()*-0.197813);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8ea0() {
   return (neuron0x8cb64d8()*0.169483);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8ec8() {
   return (neuron0x8cb6690()*-0.474876);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8ef0() {
   return (neuron0x8cb6890()*-0.805772);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8f18() {
   return (neuron0x8cb6a90()*0.221238);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8f40() {
   return (neuron0x8c6af08()*-0.195696);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8f68() {
   return (neuron0x8c6b120()*-0.301688);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8f90() {
   return (neuron0x8c6b338()*-0.376251);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8fb8() {
   return (neuron0x8c6b550()*-0.690121);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce8fe0() {
   return (neuron0x8cd6a48()*0.00862112);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9220() {
   return (neuron0x8cb64d8()*-0.201825);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9248() {
   return (neuron0x8cb6690()*1.05031);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9270() {
   return (neuron0x8cb6890()*0.142686);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9298() {
   return (neuron0x8cb6a90()*-0.447865);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce92c0() {
   return (neuron0x8c6af08()*-0.0409656);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce92e8() {
   return (neuron0x8c6b120()*-0.228202);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9310() {
   return (neuron0x8c6b338()*0.0127613);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9338() {
   return (neuron0x8c6b550()*-2.1456);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9360() {
   return (neuron0x8cd6a48()*0.022287);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce95a0() {
   return (neuron0x8cb64d8()*0.411673);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce95c8() {
   return (neuron0x8cb6690()*-0.142414);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce95f0() {
   return (neuron0x8cb6890()*0.284564);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9618() {
   return (neuron0x8cb6a90()*-0.130087);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9640() {
   return (neuron0x8c6af08()*0.0530479);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9668() {
   return (neuron0x8c6b120()*-0.112063);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9690() {
   return (neuron0x8c6b338()*0.0635757);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce96b8() {
   return (neuron0x8c6b550()*-0.276814);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce96e0() {
   return (neuron0x8cd6a48()*-0.0757719);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9920() {
   return (neuron0x8cb64d8()*-0.293943);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9948() {
   return (neuron0x8cb6690()*-0.328448);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9970() {
   return (neuron0x8cb6890()*0.0340563);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9998() {
   return (neuron0x8cb6a90()*-0.253406);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce99c0() {
   return (neuron0x8c6af08()*0.218969);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce99e8() {
   return (neuron0x8c6b120()*-0.132015);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9a10() {
   return (neuron0x8c6b338()*0.206294);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9a38() {
   return (neuron0x8c6b550()*-0.249247);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9a60() {
   return (neuron0x8cd6a48()*-0.463615);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9ca0() {
   return (neuron0x8cb64d8()*-0.261851);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9cc8() {
   return (neuron0x8cb6690()*0.0880831);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9cf0() {
   return (neuron0x8cb6890()*-0.169465);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9d18() {
   return (neuron0x8cb6a90()*-0.23221);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9d40() {
   return (neuron0x8c6af08()*0.117598);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9d68() {
   return (neuron0x8c6b120()*0.388502);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9d90() {
   return (neuron0x8c6b338()*-0.129396);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9db8() {
   return (neuron0x8c6b550()*-0.920147);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ce9de0() {
   return (neuron0x8cd6a48()*-0.0296247);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea020() {
   return (neuron0x8cb64d8()*-0.254819);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea048() {
   return (neuron0x8cb6690()*-0.502165);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea070() {
   return (neuron0x8cb6890()*0.156985);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea098() {
   return (neuron0x8cb6a90()*-0.117331);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea0c0() {
   return (neuron0x8c6af08()*0.223719);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea0e8() {
   return (neuron0x8c6b120()*0.328333);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea110() {
   return (neuron0x8c6b338()*-0.0440684);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea138() {
   return (neuron0x8c6b550()*1.80883);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea160() {
   return (neuron0x8cd6a48()*0.219612);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea3a0() {
   return (neuron0x8cb64d8()*-0.00668383);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea3c8() {
   return (neuron0x8cb6690()*0.351274);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea3f0() {
   return (neuron0x8cb6890()*0.320855);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea418() {
   return (neuron0x8cb6a90()*0.444598);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea440() {
   return (neuron0x8c6af08()*0.139708);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea468() {
   return (neuron0x8c6b120()*-1.33743);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea490() {
   return (neuron0x8c6b338()*0.15251);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea4b8() {
   return (neuron0x8c6b550()*0.739731);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea4e0() {
   return (neuron0x8cd6a48()*-0.0751823);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea720() {
   return (neuron0x8cb64d8()*-0.323445);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea748() {
   return (neuron0x8cb6690()*0.561994);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea770() {
   return (neuron0x8cb6890()*0.570508);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea798() {
   return (neuron0x8cb6a90()*0.67942);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea7c0() {
   return (neuron0x8c6af08()*0.666687);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea7e8() {
   return (neuron0x8c6b120()*-0.43202);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea810() {
   return (neuron0x8c6b338()*-0.500338);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea838() {
   return (neuron0x8c6b550()*-0.0447282);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cea860() {
   return (neuron0x8cd6a48()*-0.395781);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceaaa0() {
   return (neuron0x8cb64d8()*0.541042);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceaac8() {
   return (neuron0x8cb6690()*-1.0588);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceaaf0() {
   return (neuron0x8cb6890()*0.0573715);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceab18() {
   return (neuron0x8cb6a90()*0.22886);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceab40() {
   return (neuron0x8c6af08()*0.0339473);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceab68() {
   return (neuron0x8c6b120()*0.689316);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceab90() {
   return (neuron0x8c6b338()*0.0200604);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceabb8() {
   return (neuron0x8c6b550()*0.342489);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceabe0() {
   return (neuron0x8cd6a48()*0.63296);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceae20() {
   return (neuron0x8cb64d8()*0.597855);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceae48() {
   return (neuron0x8cb6690()*0.889852);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceae70() {
   return (neuron0x8cb6890()*0.510325);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceae98() {
   return (neuron0x8cb6a90()*0.46342);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceaec0() {
   return (neuron0x8c6af08()*-0.420082);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceaee8() {
   return (neuron0x8c6b120()*-0.106578);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceaf10() {
   return (neuron0x8c6b338()*0.358989);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceaf38() {
   return (neuron0x8c6b550()*0.701535);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceaf60() {
   return (neuron0x8cd6a48()*-0.442255);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb1a0() {
   return (neuron0x8cb64d8()*0.203007);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb1c8() {
   return (neuron0x8cb6690()*-0.151803);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb1f0() {
   return (neuron0x8cb6890()*-0.0462216);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb218() {
   return (neuron0x8cb6a90()*-0.49839);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb240() {
   return (neuron0x8c6af08()*-0.493064);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb268() {
   return (neuron0x8c6b120()*0.0809391);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb290() {
   return (neuron0x8c6b338()*0.454645);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb2b8() {
   return (neuron0x8c6b550()*1.09378);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb2e0() {
   return (neuron0x8cd6a48()*0.0117123);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb520() {
   return (neuron0x8cb64d8()*0.16635);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb548() {
   return (neuron0x8cb6690()*0.562947);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb570() {
   return (neuron0x8cb6890()*0.0488512);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb598() {
   return (neuron0x8cb6a90()*-0.400939);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb5c0() {
   return (neuron0x8c6af08()*-0.483315);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb5e8() {
   return (neuron0x8c6b120()*0.0841074);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb610() {
   return (neuron0x8c6b338()*0.287704);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb638() {
   return (neuron0x8c6b550()*0.278972);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb660() {
   return (neuron0x8cd6a48()*-0.184141);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb8a0() {
   return (neuron0x8cb64d8()*-0.0405796);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb8c8() {
   return (neuron0x8cb6690()*-0.675963);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb8f0() {
   return (neuron0x8cb6890()*0.0315733);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb918() {
   return (neuron0x8cb6a90()*1.09);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb940() {
   return (neuron0x8c6af08()*-0.0316871);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb968() {
   return (neuron0x8c6b120()*-1.85776);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb990() {
   return (neuron0x8c6b338()*-0.183075);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb9b8() {
   return (neuron0x8c6b550()*-0.49414);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceb9e0() {
   return (neuron0x8cd6a48()*-0.285747);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebc20() {
   return (neuron0x8cb64d8()*-0.526359);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebc48() {
   return (neuron0x8cb6690()*-0.541321);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebc70() {
   return (neuron0x8cb6890()*0.125484);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebc98() {
   return (neuron0x8cb6a90()*-0.423883);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebcc0() {
   return (neuron0x8c6af08()*-0.236612);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebce8() {
   return (neuron0x8c6b120()*-0.0547033);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebd10() {
   return (neuron0x8c6b338()*-0.0289726);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebd38() {
   return (neuron0x8c6b550()*-0.0170295);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebd60() {
   return (neuron0x8cd6a48()*-0.0286735);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebfa0() {
   return (neuron0x8cb64d8()*-0.260894);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebfc8() {
   return (neuron0x8cb6690()*0.0747691);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cebff0() {
   return (neuron0x8cb6890()*0.564502);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec018() {
   return (neuron0x8cb6a90()*-0.780899);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec040() {
   return (neuron0x8c6af08()*0.414956);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec068() {
   return (neuron0x8c6b120()*-0.296002);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec090() {
   return (neuron0x8c6b338()*0.369857);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec0b8() {
   return (neuron0x8c6b550()*-0.0557561);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec0e0() {
   return (neuron0x8cd6a48()*-0.206837);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec320() {
   return (neuron0x8cb64d8()*-0.337363);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec348() {
   return (neuron0x8cb6690()*0.111768);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec370() {
   return (neuron0x8cb6890()*0.424345);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec398() {
   return (neuron0x8cb6a90()*-0.242968);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec3c0() {
   return (neuron0x8c6af08()*0.368551);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec3e8() {
   return (neuron0x8c6b120()*0.642685);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec410() {
   return (neuron0x8c6b338()*0.224287);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec438() {
   return (neuron0x8c6b550()*0.573113);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec460() {
   return (neuron0x8cd6a48()*-0.0872394);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec6a0() {
   return (neuron0x8cb64d8()*0.0209492);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec6c8() {
   return (neuron0x8cb6690()*0.168516);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec6f0() {
   return (neuron0x8cb6890()*-0.308784);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec718() {
   return (neuron0x8cb6a90()*-0.849419);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec740() {
   return (neuron0x8c6af08()*0.192551);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec768() {
   return (neuron0x8c6b120()*-0.412983);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec790() {
   return (neuron0x8c6b338()*0.615865);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec7b8() {
   return (neuron0x8c6b550()*0.326607);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cec7e0() {
   return (neuron0x8cd6a48()*-0.344448);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceca20() {
   return (neuron0x8cb64d8()*0.271577);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceca48() {
   return (neuron0x8cb6690()*-0.193335);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceca70() {
   return (neuron0x8cb6890()*0.245606);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceca98() {
   return (neuron0x8cb6a90()*-0.0237308);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecac0() {
   return (neuron0x8c6af08()*-0.283689);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecae8() {
   return (neuron0x8c6b120()*-0.0880356);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecb10() {
   return (neuron0x8c6b338()*0.258897);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecb38() {
   return (neuron0x8c6b550()*-0.0325537);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecb60() {
   return (neuron0x8cd6a48()*-0.034452);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecda0() {
   return (neuron0x8cb64d8()*0.0924039);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecdc8() {
   return (neuron0x8cb6690()*0.727341);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecdf0() {
   return (neuron0x8cb6890()*-0.322216);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cece18() {
   return (neuron0x8cb6a90()*-0.274718);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cece40() {
   return (neuron0x8c6af08()*-0.0963544);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cece68() {
   return (neuron0x8c6b120()*-0.423383);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cece90() {
   return (neuron0x8c6b338()*0.00260666);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceceb8() {
   return (neuron0x8c6b550()*-0.142159);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cecee0() {
   return (neuron0x8cd6a48()*0.286022);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced120() {
   return (neuron0x8cb64d8()*-0.560118);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced148() {
   return (neuron0x8cb6690()*0.531213);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced170() {
   return (neuron0x8cb6890()*-0.411577);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced198() {
   return (neuron0x8cb6a90()*-0.300537);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced1c0() {
   return (neuron0x8c6af08()*0.274631);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced1e8() {
   return (neuron0x8c6b120()*-0.0595302);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced210() {
   return (neuron0x8c6b338()*-0.282187);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced238() {
   return (neuron0x8c6b550()*0.317372);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced260() {
   return (neuron0x8cd6a48()*-0.639942);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced4a0() {
   return (neuron0x8cb64d8()*-0.13773);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced4c8() {
   return (neuron0x8cb6690()*-0.209933);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced4f0() {
   return (neuron0x8cb6890()*0.53954);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced518() {
   return (neuron0x8cb6a90()*-1.12);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced540() {
   return (neuron0x8c6af08()*0.0919749);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced568() {
   return (neuron0x8c6b120()*-0.469738);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced590() {
   return (neuron0x8c6b338()*-0.0798587);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced5b8() {
   return (neuron0x8c6b550()*0.122128);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced5e0() {
   return (neuron0x8cd6a48()*0.294677);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced820() {
   return (neuron0x8cb64d8()*-0.392606);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced848() {
   return (neuron0x8cb6690()*-0.894877);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced870() {
   return (neuron0x8cb6890()*-0.30868);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced898() {
   return (neuron0x8cb6a90()*0.973324);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced8c0() {
   return (neuron0x8c6af08()*-0.303335);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced8e8() {
   return (neuron0x8c6b120()*0.418099);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced910() {
   return (neuron0x8c6b338()*0.407314);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced938() {
   return (neuron0x8c6b550()*-0.724519);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ced960() {
   return (neuron0x8cd6a48()*-0.0875615);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedba0() {
   return (neuron0x8cb64d8()*-0.179356);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedbc8() {
   return (neuron0x8cb6690()*-0.355211);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedbf0() {
   return (neuron0x8cb6890()*0.126349);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedc18() {
   return (neuron0x8cb6a90()*0.310569);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedc40() {
   return (neuron0x8c6af08()*-0.145723);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedc68() {
   return (neuron0x8c6b120()*-1.57364);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedc90() {
   return (neuron0x8c6b338()*0.109128);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedcb8() {
   return (neuron0x8c6b550()*0.448737);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedce0() {
   return (neuron0x8cd6a48()*0.12369);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedf20() {
   return (neuron0x8cb64d8()*0.0487388);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedf48() {
   return (neuron0x8cb6690()*0.387206);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedf70() {
   return (neuron0x8cb6890()*-0.291503);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedf98() {
   return (neuron0x8cb6a90()*-0.172732);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedfc0() {
   return (neuron0x8c6af08()*0.41199);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cedfe8() {
   return (neuron0x8c6b120()*0.337388);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee010() {
   return (neuron0x8c6b338()*-0.188656);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee038() {
   return (neuron0x8c6b550()*-0.00717531);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee060() {
   return (neuron0x8cd6a48()*-0.204143);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee2a0() {
   return (neuron0x8cb64d8()*0.0411694);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee2c8() {
   return (neuron0x8cb6690()*-0.0245053);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee2f0() {
   return (neuron0x8cb6890()*-0.342805);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee318() {
   return (neuron0x8cb6a90()*0.00956812);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee340() {
   return (neuron0x8c6af08()*0.0261219);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee368() {
   return (neuron0x8c6b120()*-1.13233);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee390() {
   return (neuron0x8c6b338()*-0.0433913);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee3b8() {
   return (neuron0x8c6b550()*-0.120378);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee3e0() {
   return (neuron0x8cd6a48()*0.330797);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee620() {
   return (neuron0x8cb64d8()*-0.342133);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee648() {
   return (neuron0x8cb6690()*-0.653217);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee670() {
   return (neuron0x8cb6890()*0.179578);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee698() {
   return (neuron0x8cb6a90()*-0.32591);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee6c0() {
   return (neuron0x8c6af08()*-0.353103);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee6e8() {
   return (neuron0x8c6b120()*0.114574);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee710() {
   return (neuron0x8c6b338()*0.116749);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee738() {
   return (neuron0x8c6b550()*-0.0782533);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee760() {
   return (neuron0x8cd6a48()*0.0141113);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee9a0() {
   return (neuron0x8cb64d8()*-0.0365315);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee9c8() {
   return (neuron0x8cb6690()*0.205412);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cee9f0() {
   return (neuron0x8cb6890()*-0.158468);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceea18() {
   return (neuron0x8cb6a90()*1.27348);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceea40() {
   return (neuron0x8c6af08()*0.510486);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceea68() {
   return (neuron0x8c6b120()*0.616677);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceea90() {
   return (neuron0x8c6b338()*-0.114018);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceeab8() {
   return (neuron0x8c6b550()*-0.157438);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceeae0() {
   return (neuron0x8cd6a48()*-0.130261);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceed20() {
   return (neuron0x8cb64d8()*0.523089);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceed48() {
   return (neuron0x8cb6690()*0.0538116);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceed70() {
   return (neuron0x8cb6890()*0.386289);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceed98() {
   return (neuron0x8cb6a90()*-0.490536);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceedc0() {
   return (neuron0x8c6af08()*-0.465317);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceede8() {
   return (neuron0x8c6b120()*-0.820737);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceee10() {
   return (neuron0x8c6b338()*-0.204284);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceee38() {
   return (neuron0x8c6b550()*0.302179);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceee60() {
   return (neuron0x8cd6a48()*-0.278093);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef0a0() {
   return (neuron0x8cb64d8()*0.141793);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef0c8() {
   return (neuron0x8cb6690()*-0.895447);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef0f0() {
   return (neuron0x8cb6890()*-0.101038);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef118() {
   return (neuron0x8cb6a90()*-1.45572);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef140() {
   return (neuron0x8c6af08()*0.0156853);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef168() {
   return (neuron0x8c6b120()*-0.0262109);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef190() {
   return (neuron0x8c6b338()*0.0269748);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef1b8() {
   return (neuron0x8c6b550()*-0.145074);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef1e0() {
   return (neuron0x8cd6a48()*-0.343256);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef420() {
   return (neuron0x8cb64d8()*-0.0910347);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef448() {
   return (neuron0x8cb6690()*0.540615);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef470() {
   return (neuron0x8cb6890()*-0.0476191);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef498() {
   return (neuron0x8cb6a90()*-0.455787);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef4c0() {
   return (neuron0x8c6af08()*0.199286);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef4e8() {
   return (neuron0x8c6b120()*-2.49063);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef510() {
   return (neuron0x8c6b338()*0.0741721);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef538() {
   return (neuron0x8c6b550()*3.09369);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef560() {
   return (neuron0x8cd6a48()*0.239065);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef7a0() {
   return (neuron0x8cb64d8()*0.0268479);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef7c8() {
   return (neuron0x8cb6690()*0.810034);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef7f0() {
   return (neuron0x8cb6890()*0.0271656);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef818() {
   return (neuron0x8cb6a90()*-1.39064);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef840() {
   return (neuron0x8c6af08()*-0.24676);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef868() {
   return (neuron0x8c6b120()*-0.609598);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef890() {
   return (neuron0x8c6b338()*0.199143);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef8b8() {
   return (neuron0x8c6b550()*1.86313);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cef8e0() {
   return (neuron0x8cd6a48()*-0.412585);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefa30() {
   return (neuron0x8cd6da0()*0.0650747);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefa58() {
   return (neuron0x8cd7048()*-0.161121);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefa80() {
   return (neuron0x8cd7438()*0.393972);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefaa8() {
   return (neuron0x8cd7758()*-0.188276);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefad0() {
   return (neuron0x8cd7b50()*-0.213851);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefaf8() {
   return (neuron0x8cd7eb8()*0.314716);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefb20() {
   return (neuron0x8cd8220()*0.0442394);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefb48() {
   return (neuron0x8cd8588()*-0.260228);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefb70() {
   return (neuron0x8cd8990()*0.182532);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefb98() {
   return (neuron0x8cd8cf8()*-0.0956333);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefbc0() {
   return (neuron0x8cd9060()*-0.0834502);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefbe8() {
   return (neuron0x8cd93c8()*1.36347);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefc10() {
   return (neuron0x8cd9730()*0.143121);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefc38() {
   return (neuron0x8cd9a98()*0.0473211);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefc60() {
   return (neuron0x8cd9e00()*-0.703227);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefc88() {
   return (neuron0x8cd87b0()*0.909477);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefd38() {
   return (neuron0x8cdaae8()*0.347244);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefd60() {
   return (neuron0x8cdad90()*-0.744675);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefd88() {
   return (neuron0x8cdb038()*-0.254196);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefdb0() {
   return (neuron0x8cdb2e0()*0.252332);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefdd8() {
   return (neuron0x8cdb660()*0.0864831);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefe00() {
   return (neuron0x8cdb9e0()*-0.425866);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefe28() {
   return (neuron0x8cdbd60()*-1.09362);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefe50() {
   return (neuron0x8cdc0e0()*0.101579);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefe78() {
   return (neuron0x8cdc4f0()*-0.104151);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefea0() {
   return (neuron0x8cdd820()*-0.908365);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefec8() {
   return (neuron0x8cddb88()*0.310749);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefef0() {
   return (neuron0x8cddef0()*0.182491);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceff18() {
   return (neuron0x8cde258()*0.0107398);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceff40() {
   return (neuron0x8cda130()*-0.337803);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceff68() {
   return (neuron0x8cded50()*0.455345);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceff90() {
   return (neuron0x8cdf0b8()*0.610061);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefcb0() {
   return (neuron0x8cdfc40()*-0.3498);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefcd8() {
   return (neuron0x8cdfd68()*-0.0996118);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cefd00() {
   return (neuron0x8ce0060()*-0.093278);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf00c0() {
   return (neuron0x8ce03c8()*0.590549);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf00e8() {
   return (neuron0x8ce0730()*0.120238);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0110() {
   return (neuron0x8ce0a98()*-0.114064);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0138() {
   return (neuron0x8ce0e00()*0.308772);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0160() {
   return (neuron0x8ce1180()*-0.00169452);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0188() {
   return (neuron0x8ce1500()*0.195455);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf01b0() {
   return (neuron0x8ce1880()*-0.554822);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf01d8() {
   return (neuron0x8ce1c00()*-0.326812);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0200() {
   return (neuron0x8ce1f80()*1.33113);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0228() {
   return (neuron0x8ce2300()*0.335764);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0250() {
   return (neuron0x8ce2680()*0.502044);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0278() {
   return (neuron0x8ce2a00()*-0.0828526);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf02a0() {
   return (neuron0x8ce2d80()*-0.44755);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf02c8() {
   return (neuron0x8ce3100()*1.26685);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf02f0() {
   return (neuron0x8ce3480()*0.570365);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0318() {
   return (neuron0x8ce3800()*-0.0234607);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0340() {
   return (neuron0x8ce3b80()*-0.56538);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0368() {
   return (neuron0x8ce3f00()*-0.875115);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0390() {
   return (neuron0x8ce4280()*-0.277193);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf03b8() {
   return (neuron0x8ce4600()*0.202236);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf03e0() {
   return (neuron0x8ce4980()*-0.945632);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0408() {
   return (neuron0x8ce4e18()*-0.361938);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0430() {
   return (neuron0x8cde520()*-0.82416);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0458() {
   return (neuron0x8cde8a0()*0.252725);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0480() {
   return (neuron0x8ce6188()*0.532237);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf04a8() {
   return (neuron0x8ce6418()*0.382472);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf04d0() {
   return (neuron0x8ce6780()*0.477184);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf04f8() {
   return (neuron0x8ce6b00()*-1.10548);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0520() {
   return (neuron0x8ce6e80()*-0.495474);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cd6cb8() {
   return (neuron0x8cdfa58()*0.843662);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceffb8() {
   return (neuron0x8ce7ea0()*0.332739);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8ceffe0() {
   return (neuron0x8ce8208()*-0.273029);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0008() {
   return (neuron0x8ce8588()*-0.589862);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0030() {
   return (neuron0x8ce8908()*0.425412);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0058() {
   return (neuron0x8ce8c88()*0.0983418);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0080() {
   return (neuron0x8ce9008()*-1.23289);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0750() {
   return (neuron0x8ce9388()*-0.347437);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0778() {
   return (neuron0x8ce9708()*-0.214971);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf07a0() {
   return (neuron0x8ce9a88()*-0.490969);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf07c8() {
   return (neuron0x8ce9e08()*0.62718);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf07f0() {
   return (neuron0x8cea188()*1.09819);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0818() {
   return (neuron0x8cea508()*-0.523927);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0840() {
   return (neuron0x8cea888()*0.648452);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0868() {
   return (neuron0x8ceac08()*-0.651712);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0890() {
   return (neuron0x8ceaf88()*-0.519872);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf08b8() {
   return (neuron0x8ceb308()*0.0845995);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf08e0() {
   return (neuron0x8ceb688()*-0.517119);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0908() {
   return (neuron0x8ceba08()*0.254861);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0930() {
   return (neuron0x8cebd88()*-0.642109);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0958() {
   return (neuron0x8cec108()*0.382033);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0980() {
   return (neuron0x8cec488()*0.346798);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf09a8() {
   return (neuron0x8cec808()*-0.147914);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf09d0() {
   return (neuron0x8cecb88()*-0.254196);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf09f8() {
   return (neuron0x8cecf08()*-0.149335);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0a20() {
   return (neuron0x8ced288()*0.603191);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0a48() {
   return (neuron0x8ced608()*0.473176);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0a70() {
   return (neuron0x8ced988()*1.11546);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0a98() {
   return (neuron0x8cedd08()*0.0377858);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0ac0() {
   return (neuron0x8cee088()*0.86657);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0ae8() {
   return (neuron0x8cee408()*0.234023);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0b10() {
   return (neuron0x8cee788()*-0.549103);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0b38() {
   return (neuron0x8ceeb08()*-0.240979);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0b60() {
   return (neuron0x8ceee88()*0.718176);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0b88() {
   return (neuron0x8cef208()*-0.768559);
}

double NNParaGammaXYCorrectionClusterDeltaX::synapse0x8cf0bb0() {
   return (neuron0x8cef588()*-0.418343);
}

