#include "NNPerpElectronAngleCorrectionBCPDir.h"
#include <cmath>

double NNPerpElectronAngleCorrectionBCPDir::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10,double in11,double in12,double in13,double in14) {
   input0 = (in0 - 0.70338)/0.0918753;
   input1 = (in1 - -0.0158536)/0.256935;
   input2 = (in2 - -3.74358)/31.4802;
   input3 = (in3 - -3.51704)/56.0822;
   input4 = (in4 - 3068.51)/1543.52;
   input5 = (in5 - -0.00355158)/0.749607;
   input6 = (in6 - -0.0154733)/0.75028;
   input7 = (in7 - -4.12133)/32.3487;
   input8 = (in8 - -5.23583)/57.6425;
   input9 = (in9 - 1.38816)/0.32817;
   input10 = (in10 - 1.48199)/0.36367;
   input11 = (in11 - -0.0323618)/14.7341;
   input12 = (in12 - 0.193437)/1.21351;
   input13 = (in13 - 1.25754)/412.206;
   input14 = (in14 - 4.00453)/5.88751;
   switch(index) {
     case 0:
         return neuron0x8dd1d90();
     case 1:
         return neuron0x8dd2598();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionBCPDir::Value(int index, double* input) {
   input0 = (input[0] - 0.70338)/0.0918753;
   input1 = (input[1] - -0.0158536)/0.256935;
   input2 = (input[2] - -3.74358)/31.4802;
   input3 = (input[3] - -3.51704)/56.0822;
   input4 = (input[4] - 3068.51)/1543.52;
   input5 = (input[5] - -0.00355158)/0.749607;
   input6 = (input[6] - -0.0154733)/0.75028;
   input7 = (input[7] - -4.12133)/32.3487;
   input8 = (input[8] - -5.23583)/57.6425;
   input9 = (input[9] - 1.38816)/0.32817;
   input10 = (input[10] - 1.48199)/0.36367;
   input11 = (input[11] - -0.0323618)/14.7341;
   input12 = (input[12] - 0.193437)/1.21351;
   input13 = (input[13] - 1.25754)/412.206;
   input14 = (input[14] - 4.00453)/5.88751;
   switch(index) {
     case 0:
         return neuron0x8dd1d90();
     case 1:
         return neuron0x8dd2598();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc30e0() {
   return input0;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc3278() {
   return input1;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc34a0() {
   return input2;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc36a0() {
   return input3;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc38a0() {
   return input4;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc3ac8() {
   return input5;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc3cc8() {
   return input6;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc3ec8() {
   return input7;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc40c8() {
   return input8;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc42c8() {
   return input9;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc44c8() {
   return input10;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc46c8() {
   return input11;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc48c8() {
   return input12;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc4ae0() {
   return input13;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc4cf8() {
   return input14;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc5038() {
   double input = -0.213966;
   input += synapse0x8d57638();
   input += synapse0x8bee188();
   input += synapse0x8bee1d0();
   input += synapse0x8bee2a0();
   input += synapse0x8dc51f0();
   input += synapse0x8dc5218();
   input += synapse0x8dc5240();
   input += synapse0x8dc5268();
   input += synapse0x8dc5290();
   input += synapse0x8dc52b8();
   input += synapse0x8dc52e0();
   input += synapse0x8dc5308();
   input += synapse0x8dc5330();
   input += synapse0x8dc5358();
   input += synapse0x8dc5380();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc5038() {
   double input = input0x8dc5038();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc53a8() {
   double input = 0.391242;
   input += synapse0x8dc5630();
   input += synapse0x8dc5658();
   input += synapse0x8dc5708();
   input += synapse0x8dc5730();
   input += synapse0x8dc5758();
   input += synapse0x8dc5780();
   input += synapse0x8dc57a8();
   input += synapse0x8dc57d0();
   input += synapse0x8dc57f8();
   input += synapse0x8dc5820();
   input += synapse0x8dc5848();
   input += synapse0x8dc5870();
   input += synapse0x8dc5898();
   input += synapse0x8dc58c0();
   input += synapse0x8dc58e8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc53a8() {
   double input = input0x8dc53a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc5910() {
   double input = 0.290105;
   input += synapse0x8dc5a80();
   input += synapse0x8dc5aa8();
   input += synapse0x8dc5ad0();
   input += synapse0x8dc5680();
   input += synapse0x8dc56a8();
   input += synapse0x8dc56d0();
   input += synapse0x8d57660();
   input += synapse0x8d57688();
   input += synapse0x8d576b0();
   input += synapse0x8d576d8();
   input += synapse0x8dc5c00();
   input += synapse0x8dc5c28();
   input += synapse0x8dc5c50();
   input += synapse0x8dc5c78();
   input += synapse0x8dc5ca0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc5910() {
   double input = input0x8dc5910();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc5cc8() {
   double input = 0.30153;
   input += synapse0x8dc5ec8();
   input += synapse0x8dc5ef0();
   input += synapse0x8dc5f18();
   input += synapse0x8dc5f40();
   input += synapse0x8dc5f68();
   input += synapse0x8dc5f90();
   input += synapse0x8dc5fb8();
   input += synapse0x8dc5fe0();
   input += synapse0x8dc6008();
   input += synapse0x8dc6030();
   input += synapse0x8dc6058();
   input += synapse0x8dc6080();
   input += synapse0x8dc60a8();
   input += synapse0x8dc60d0();
   input += synapse0x8dc60f8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc5cc8() {
   double input = input0x8dc5cc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc6120() {
   double input = 0.0744098;
   input += synapse0x8dc6320();
   input += synapse0x8dc6348();
   input += synapse0x8dc6370();
   input += synapse0x8dc6398();
   input += synapse0x8dc63c0();
   input += synapse0x8d57600();
   input += synapse0x8bee2c8();
   input += synapse0x8bee2f0();
   input += synapse0x8bee318();
   input += synapse0x8bee340();
   input += synapse0x8bee3b8();
   input += synapse0x8bee3e0();
   input += synapse0x8bee408();
   input += synapse0x8bee430();
   input += synapse0x8bee458();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc6120() {
   double input = input0x8dc6120();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc65f0() {
   double input = 0.021981;
   input += synapse0x8dc6808();
   input += synapse0x8dc6830();
   input += synapse0x8dc6858();
   input += synapse0x8dc6880();
   input += synapse0x8dc68a8();
   input += synapse0x8dc68d0();
   input += synapse0x8dc68f8();
   input += synapse0x8dc6920();
   input += synapse0x8dc6948();
   input += synapse0x8dc6970();
   input += synapse0x8dc6998();
   input += synapse0x8dc69c0();
   input += synapse0x8dc69e8();
   input += synapse0x8dc6a10();
   input += synapse0x8dc6a38();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc65f0() {
   double input = input0x8dc65f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc6a60() {
   double input = -0.340969;
   input += synapse0x8dc6c78();
   input += synapse0x8dc6ca0();
   input += synapse0x8dc6cc8();
   input += synapse0x8dc6cf0();
   input += synapse0x8dc6d18();
   input += synapse0x8dc6d40();
   input += synapse0x8dc6d68();
   input += synapse0x8dc6d90();
   input += synapse0x8dc6db8();
   input += synapse0x8dc6de0();
   input += synapse0x8dc6e08();
   input += synapse0x8dc6e30();
   input += synapse0x8dc6e58();
   input += synapse0x8dc6e80();
   input += synapse0x8dc6ea8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc6a60() {
   double input = input0x8dc6a60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc6ed0() {
   double input = -0.0399835;
   input += synapse0x8dc70e8();
   input += synapse0x8dc7110();
   input += synapse0x8dc7138();
   input += synapse0x8dc7160();
   input += synapse0x8dc7188();
   input += synapse0x8dc71b0();
   input += synapse0x8dc71d8();
   input += synapse0x8dc7200();
   input += synapse0x8dc7228();
   input += synapse0x8dc7250();
   input += synapse0x8dc7278();
   input += synapse0x8dc72a0();
   input += synapse0x8dc72c8();
   input += synapse0x8dc72f0();
   input += synapse0x8dc7318();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc6ed0() {
   double input = input0x8dc6ed0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc7340() {
   double input = 0.0734418;
   input += synapse0x8dc7558();
   input += synapse0x8dc7580();
   input += synapse0x8dc75a8();
   input += synapse0x8dc75d0();
   input += synapse0x8dc75f8();
   input += synapse0x8dc7620();
   input += synapse0x8dc7648();
   input += synapse0x8dc7670();
   input += synapse0x8dc7698();
   input += synapse0x8dc9ea0();
   input += synapse0x8dc9ec8();
   input += synapse0x8dc9ef0();
   input += synapse0x8dc9f18();
   input += synapse0x8dc9f40();
   input += synapse0x8dc9f68();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc7340() {
   double input = input0x8dc7340();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc63e8() {
   double input = 0.189378;
   input += synapse0x8dc5b40();
   input += synapse0x8dc5b68();
   input += synapse0x8dc5b90();
   input += synapse0x8dc5bb8();
   input += synapse0x8dc65b8();
   input += synapse0x8dc7ac8();
   input += synapse0x8dc7af0();
   input += synapse0x8dc7b18();
   input += synapse0x8dc7b40();
   input += synapse0x8dc7b68();
   input += synapse0x8dc7b90();
   input += synapse0x8dc7bb8();
   input += synapse0x8dc7be0();
   input += synapse0x8dc7c08();
   input += synapse0x8dc7c30();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc63e8() {
   double input = input0x8dc63e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc7c58() {
   double input = 0.278256;
   input += synapse0x8dc7e70();
   input += synapse0x8dc7e98();
   input += synapse0x8dc7ec0();
   input += synapse0x8dc7ee8();
   input += synapse0x8dc7f10();
   input += synapse0x8dc7f38();
   input += synapse0x8dc7f60();
   input += synapse0x8dc7f88();
   input += synapse0x8dc7fb0();
   input += synapse0x8dc7fd8();
   input += synapse0x8dc8000();
   input += synapse0x8dc8028();
   input += synapse0x8dc8050();
   input += synapse0x8dc8078();
   input += synapse0x8dc80a0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc7c58() {
   double input = input0x8dc7c58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc80c8() {
   double input = -0.278432;
   input += synapse0x8dc82e0();
   input += synapse0x8dc8308();
   input += synapse0x8dc8330();
   input += synapse0x8dc8358();
   input += synapse0x8dc8380();
   input += synapse0x8dc83a8();
   input += synapse0x8dc83d0();
   input += synapse0x8dc83f8();
   input += synapse0x8dc8420();
   input += synapse0x8dc8448();
   input += synapse0x8dc8470();
   input += synapse0x8dc8498();
   input += synapse0x8dc84c0();
   input += synapse0x8dc84e8();
   input += synapse0x8dc8510();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc80c8() {
   double input = input0x8dc80c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc8538() {
   double input = 0.264454;
   input += synapse0x8dc8750();
   input += synapse0x8dc8778();
   input += synapse0x8dc87a0();
   input += synapse0x8dc87c8();
   input += synapse0x8dc87f0();
   input += synapse0x8dc8818();
   input += synapse0x8dc8840();
   input += synapse0x8dc8868();
   input += synapse0x8dc8890();
   input += synapse0x8dc88b8();
   input += synapse0x8dc88e0();
   input += synapse0x8dc8908();
   input += synapse0x8dc8930();
   input += synapse0x8dc8958();
   input += synapse0x8dc8980();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc8538() {
   double input = input0x8dc8538();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc89a8() {
   double input = -0.173416;
   input += synapse0x8dc8bc0();
   input += synapse0x8dc8be8();
   input += synapse0x8dc8c10();
   input += synapse0x8dc8c38();
   input += synapse0x8dc8c60();
   input += synapse0x8dc8c88();
   input += synapse0x8dc8cb0();
   input += synapse0x8dc8cd8();
   input += synapse0x8dc8d00();
   input += synapse0x8dc8d28();
   input += synapse0x8dc8d50();
   input += synapse0x8dc8d78();
   input += synapse0x8dc8da0();
   input += synapse0x8dc8dc8();
   input += synapse0x8dc8df0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc89a8() {
   double input = input0x8dc89a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc9fe8() {
   double input = 0.0577903;
   input += synapse0x8dca158();
   input += synapse0x8dca180();
   input += synapse0x8dca1a8();
   input += synapse0x8dca1d0();
   input += synapse0x8dca1f8();
   input += synapse0x8dca220();
   input += synapse0x8dca248();
   input += synapse0x8dca270();
   input += synapse0x8dca298();
   input += synapse0x8dca2c0();
   input += synapse0x8dca2e8();
   input += synapse0x8dca310();
   input += synapse0x8dca338();
   input += synapse0x8dca360();
   input += synapse0x8dca388();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc9fe8() {
   double input = input0x8dc9fe8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dca3b0() {
   double input = 0.348665;
   input += synapse0x8dca5b0();
   input += synapse0x8dca660();
   input += synapse0x8dca710();
   input += synapse0x8dca7c0();
   input += synapse0x8dca870();
   input += synapse0x8dca920();
   input += synapse0x8dca9d0();
   input += synapse0x8dcaa80();
   input += synapse0x8dcab30();
   input += synapse0x8dcabe0();
   input += synapse0x8dcac90();
   input += synapse0x8dcad40();
   input += synapse0x8dcadf0();
   input += synapse0x8dcaea0();
   input += synapse0x8dcaf50();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dca3b0() {
   double input = input0x8dca3b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcb000() {
   double input = -0.425549;
   input += synapse0x8dcb128();
   input += synapse0x8dcb150();
   input += synapse0x8dcb178();
   input += synapse0x8dcb1a0();
   input += synapse0x8dcb1c8();
   input += synapse0x8dcb1f0();
   input += synapse0x8dcb218();
   input += synapse0x8dcb240();
   input += synapse0x8dcb268();
   input += synapse0x8dcb290();
   input += synapse0x8dcb2b8();
   input += synapse0x8dcb2e0();
   input += synapse0x8dcb308();
   input += synapse0x8dcb330();
   input += synapse0x8dcb358();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcb000() {
   double input = input0x8dcb000();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcb380() {
   double input = 0.0178044;
   input += synapse0x8dc55a8();
   input += synapse0x8dc55d0();
   input += synapse0x8dc55f8();
   input += synapse0x8dc4238();
   input += synapse0x8dc4038();
   input += synapse0x8dc3e38();
   input += synapse0x8dc3c38();
   input += synapse0x8dc3a38();
   input += synapse0x8dc3810();
   input += synapse0x8dc3610();
   input += synapse0x8dc3410();
   input += synapse0x8da31e8();
   input += synapse0x8dc76c0();
   input += synapse0x8dc76e8();
   input += synapse0x8dc7710();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcb380() {
   double input = input0x8dcb380();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dc7738() {
   double input = 0.384175;
   input += synapse0x8dc7950();
   input += synapse0x8dc7978();
   input += synapse0x8dc79a0();
   input += synapse0x8dc79c8();
   input += synapse0x8dc79f0();
   input += synapse0x8dc7a18();
   input += synapse0x8dc7a40();
   input += synapse0x8dc7a68();
   input += synapse0x8dc7a90();
   input += synapse0x8dcbdb8();
   input += synapse0x8dcbde0();
   input += synapse0x8dcbe08();
   input += synapse0x8dcbe30();
   input += synapse0x8dcbe58();
   input += synapse0x8dcbe80();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dc7738() {
   double input = input0x8dc7738();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcbea8() {
   double input = 0.189854;
   input += synapse0x8dcc0a8();
   input += synapse0x8dcc0d0();
   input += synapse0x8dcc0f8();
   input += synapse0x8dcc120();
   input += synapse0x8dcc148();
   input += synapse0x8dcc170();
   input += synapse0x8dcc198();
   input += synapse0x8dcc1c0();
   input += synapse0x8dcc1e8();
   input += synapse0x8dcc210();
   input += synapse0x8dcc238();
   input += synapse0x8dcc260();
   input += synapse0x8dcc288();
   input += synapse0x8dcc2b0();
   input += synapse0x8dcc2d8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcbea8() {
   double input = input0x8dcbea8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcc300() {
   double input = 0.270733;
   input += synapse0x8dcc500();
   input += synapse0x8dcc528();
   input += synapse0x8dcc550();
   input += synapse0x8dcc578();
   input += synapse0x8dcc5a0();
   input += synapse0x8dcc5c8();
   input += synapse0x8dcc5f0();
   input += synapse0x8dcc618();
   input += synapse0x8dcc640();
   input += synapse0x8dcc668();
   input += synapse0x8dcc690();
   input += synapse0x8dcc6b8();
   input += synapse0x8dcc6e0();
   input += synapse0x8dcc708();
   input += synapse0x8dcc730();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcc300() {
   double input = input0x8dcc300();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcc758() {
   double input = 0.0749623;
   input += synapse0x8dcc958();
   input += synapse0x8dcc980();
   input += synapse0x8dcc9a8();
   input += synapse0x8dcc9d0();
   input += synapse0x8dcc9f8();
   input += synapse0x8dcca20();
   input += synapse0x8dcca48();
   input += synapse0x8dcca70();
   input += synapse0x8dcca98();
   input += synapse0x8dccac0();
   input += synapse0x8dccae8();
   input += synapse0x8dccb10();
   input += synapse0x8dccb38();
   input += synapse0x8dccb60();
   input += synapse0x8dccb88();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcc758() {
   double input = input0x8dcc758();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dccbb0() {
   double input = 0.373541;
   input += synapse0x8dccdb0();
   input += synapse0x8dccdd8();
   input += synapse0x8dcce00();
   input += synapse0x8dcce28();
   input += synapse0x8dcce50();
   input += synapse0x8dcce78();
   input += synapse0x8dccea0();
   input += synapse0x8dccec8();
   input += synapse0x8dccef0();
   input += synapse0x8dccf18();
   input += synapse0x8dccf40();
   input += synapse0x8dccf68();
   input += synapse0x8dccf90();
   input += synapse0x8dccfb8();
   input += synapse0x8dccfe0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dccbb0() {
   double input = input0x8dccbb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcd008() {
   double input = 0.454962;
   input += synapse0x8dcd208();
   input += synapse0x8dcd230();
   input += synapse0x8dcd258();
   input += synapse0x8dcd280();
   input += synapse0x8dcd2a8();
   input += synapse0x8dcd2d0();
   input += synapse0x8dcd2f8();
   input += synapse0x8dcd320();
   input += synapse0x8dcd348();
   input += synapse0x8dcd370();
   input += synapse0x8dcd398();
   input += synapse0x8dcd3c0();
   input += synapse0x8dcd3e8();
   input += synapse0x8dcd410();
   input += synapse0x8dcd438();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcd008() {
   double input = input0x8dcd008();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcd460() {
   double input = 0.0311047;
   input += synapse0x8dcd660();
   input += synapse0x8dcd688();
   input += synapse0x8dcd6b0();
   input += synapse0x8dcd6d8();
   input += synapse0x8dcd700();
   input += synapse0x8dcd728();
   input += synapse0x8dcd750();
   input += synapse0x8dcd778();
   input += synapse0x8dcd7a0();
   input += synapse0x8dcd7c8();
   input += synapse0x8dcd7f0();
   input += synapse0x8dcd818();
   input += synapse0x8dcd840();
   input += synapse0x8dcd868();
   input += synapse0x8dcd890();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcd460() {
   double input = input0x8dcd460();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcd8b8() {
   double input = 0.0927709;
   input += synapse0x8dcdab8();
   input += synapse0x8dcdae0();
   input += synapse0x8dcdb08();
   input += synapse0x8dcdb30();
   input += synapse0x8dcdb58();
   input += synapse0x8dcdb80();
   input += synapse0x8dcdba8();
   input += synapse0x8dcdbd0();
   input += synapse0x8dcdbf8();
   input += synapse0x8dcdc20();
   input += synapse0x8dcdc48();
   input += synapse0x8dcdc70();
   input += synapse0x8dcdc98();
   input += synapse0x8dcdcc0();
   input += synapse0x8dcdce8();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcd8b8() {
   double input = input0x8dcd8b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcdd10() {
   double input = 0.31353;
   input += synapse0x8dcdf10();
   input += synapse0x8dcdf38();
   input += synapse0x8dcdf60();
   input += synapse0x8dcdf88();
   input += synapse0x8dcdfb0();
   input += synapse0x8dcdfd8();
   input += synapse0x8dce000();
   input += synapse0x8dce028();
   input += synapse0x8dce050();
   input += synapse0x8dce078();
   input += synapse0x8dce0a0();
   input += synapse0x8dce0c8();
   input += synapse0x8dce0f0();
   input += synapse0x8dce118();
   input += synapse0x8dce140();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcdd10() {
   double input = input0x8dcdd10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dce168() {
   double input = -0.206625;
   input += synapse0x8dce368();
   input += synapse0x8dce390();
   input += synapse0x8dce3b8();
   input += synapse0x8dce3e0();
   input += synapse0x8dce408();
   input += synapse0x8dce430();
   input += synapse0x8dce458();
   input += synapse0x8dce480();
   input += synapse0x8dce4a8();
   input += synapse0x8dce4d0();
   input += synapse0x8dce4f8();
   input += synapse0x8dce520();
   input += synapse0x8dce548();
   input += synapse0x8dce570();
   input += synapse0x8dce598();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dce168() {
   double input = input0x8dce168();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dce5c0() {
   double input = 0.113173;
   input += synapse0x8dce7c0();
   input += synapse0x8dce7e8();
   input += synapse0x8dce810();
   input += synapse0x8dce838();
   input += synapse0x8dce860();
   input += synapse0x8dce888();
   input += synapse0x8dce8b0();
   input += synapse0x8dce8d8();
   input += synapse0x8dce900();
   input += synapse0x8dce928();
   input += synapse0x8dce950();
   input += synapse0x8dce978();
   input += synapse0x8dce9a0();
   input += synapse0x8dce9c8();
   input += synapse0x8dce9f0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dce5c0() {
   double input = input0x8dce5c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcea18() {
   double input = 0.372508;
   input += synapse0x8dcec18();
   input += synapse0x8dcec40();
   input += synapse0x8dcec68();
   input += synapse0x8dcec90();
   input += synapse0x8dcecb8();
   input += synapse0x8dcece0();
   input += synapse0x8dced08();
   input += synapse0x8dced30();
   input += synapse0x8dced58();
   input += synapse0x8dced80();
   input += synapse0x8dceda8();
   input += synapse0x8dcedd0();
   input += synapse0x8dcedf8();
   input += synapse0x8dcee20();
   input += synapse0x8dcee48();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcea18() {
   double input = input0x8dcea18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcee70() {
   double input = -0.397545;
   input += synapse0x8dcf070();
   input += synapse0x8dcf098();
   input += synapse0x8dcf0c0();
   input += synapse0x8dcf0e8();
   input += synapse0x8dcf110();
   input += synapse0x8dcf138();
   input += synapse0x8dcf160();
   input += synapse0x8dcf188();
   input += synapse0x8dcf1b0();
   input += synapse0x8dcf1d8();
   input += synapse0x8dcf200();
   input += synapse0x8dcf228();
   input += synapse0x8dcf250();
   input += synapse0x8dcf278();
   input += synapse0x8dcf2a0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcee70() {
   double input = input0x8dcee70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcf2c8() {
   double input = -0.212846;
   input += synapse0x8dcf4c8();
   input += synapse0x8dca5d8();
   input += synapse0x8dca600();
   input += synapse0x8dca628();
   input += synapse0x8dca688();
   input += synapse0x8dca6b0();
   input += synapse0x8dca6d8();
   input += synapse0x8dca738();
   input += synapse0x8dca760();
   input += synapse0x8dca788();
   input += synapse0x8dca7e8();
   input += synapse0x8dca810();
   input += synapse0x8dca838();
   input += synapse0x8dca898();
   input += synapse0x8dca8c0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcf2c8() {
   double input = input0x8dcf2c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dd0468() {
   double input = -0.151424;
   input += synapse0x8dcaaf0();
   input += synapse0x8dca8e8();
   input += synapse0x8dca990();
   input += synapse0x8dcaa40();
   input += synapse0x8dcab58();
   input += synapse0x8dcab80();
   input += synapse0x8dcaba8();
   input += synapse0x8dcac08();
   input += synapse0x8dcac30();
   input += synapse0x8dcac58();
   input += synapse0x8dcacb8();
   input += synapse0x8dcace0();
   input += synapse0x8dcad08();
   input += synapse0x8dcad68();
   input += synapse0x8dcad90();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dd0468() {
   double input = input0x8dd0468();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dd0590() {
   double input = -0.359848;
   input += synapse0x8dcafc0();
   input += synapse0x8dcadb8();
   input += synapse0x8dcae60();
   input += synapse0x8dcaf10();
   input += synapse0x8dd06b8();
   input += synapse0x8dd06e0();
   input += synapse0x8dd0708();
   input += synapse0x8dd0730();
   input += synapse0x8dd0758();
   input += synapse0x8dd0780();
   input += synapse0x8dd07a8();
   input += synapse0x8dd07d0();
   input += synapse0x8dd07f8();
   input += synapse0x8dd0820();
   input += synapse0x8dd0848();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dd0590() {
   double input = input0x8dd0590();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dd0870() {
   double input = -0.0988111;
   input += synapse0x8dd0a70();
   input += synapse0x8dd0a98();
   input += synapse0x8dd0ac0();
   input += synapse0x8dcb5b0();
   input += synapse0x8dcb5d8();
   input += synapse0x8dcb600();
   input += synapse0x8dcb628();
   input += synapse0x8dcb650();
   input += synapse0x8dcb678();
   input += synapse0x8dcb6a0();
   input += synapse0x8dcb6c8();
   input += synapse0x8dcb6f0();
   input += synapse0x8dcb718();
   input += synapse0x8dcb740();
   input += synapse0x8dcb768();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dd0870() {
   double input = input0x8dd0870();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcb790() {
   double input = -0.326934;
   input += synapse0x8dcb990();
   input += synapse0x8dcb9b8();
   input += synapse0x8dcb9e0();
   input += synapse0x8dcba08();
   input += synapse0x8dcba30();
   input += synapse0x8dcba58();
   input += synapse0x8dcba80();
   input += synapse0x8dcbaa8();
   input += synapse0x8dcbad0();
   input += synapse0x8dcbaf8();
   input += synapse0x8dcbb20();
   input += synapse0x8dcbb48();
   input += synapse0x8dcbb70();
   input += synapse0x8dcbb98();
   input += synapse0x8dcbbc0();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcb790() {
   double input = input0x8dcb790();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dcbbe8() {
   double input = -0.0250778;
   input += synapse0x8dd1b38();
   input += synapse0x8dd1b60();
   input += synapse0x8dd1b88();
   input += synapse0x8dd1bb0();
   input += synapse0x8dd1bd8();
   input += synapse0x8dd1c00();
   input += synapse0x8dd1c28();
   input += synapse0x8dd1c50();
   input += synapse0x8dd1c78();
   input += synapse0x8dd1ca0();
   input += synapse0x8dd1cc8();
   input += synapse0x8dd1cf0();
   input += synapse0x8dd1d18();
   input += synapse0x8dd1d40();
   input += synapse0x8dd1d68();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dcbbe8() {
   double input = input0x8dcbbe8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dd1d90() {
   double input = -0.148202;
   input += synapse0x8dd1eb8();
   input += synapse0x8dd1ee0();
   input += synapse0x8dd1f08();
   input += synapse0x8dd1f30();
   input += synapse0x8dd1f58();
   input += synapse0x8dd1f80();
   input += synapse0x8dd1fa8();
   input += synapse0x8dd1fd0();
   input += synapse0x8dd1ff8();
   input += synapse0x8dd2020();
   input += synapse0x8dd2048();
   input += synapse0x8dd2070();
   input += synapse0x8dd2098();
   input += synapse0x8dd20c0();
   input += synapse0x8dd20e8();
   input += synapse0x8dd2110();
   input += synapse0x8dd21c0();
   input += synapse0x8dd21e8();
   input += synapse0x8dd2210();
   input += synapse0x8dd2238();
   input += synapse0x8dd2260();
   input += synapse0x8dd2288();
   input += synapse0x8dd22b0();
   input += synapse0x8dd22d8();
   input += synapse0x8dd2300();
   input += synapse0x8dd2328();
   input += synapse0x8dd2350();
   input += synapse0x8dd2378();
   input += synapse0x8dd23a0();
   input += synapse0x8dd23c8();
   input += synapse0x8dd23f0();
   input += synapse0x8dd2418();
   input += synapse0x8dd2138();
   input += synapse0x8dd2160();
   input += synapse0x8dd2188();
   input += synapse0x8dd2548();
   input += synapse0x8dd2570();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dd1d90() {
   double input = input0x8dd1d90();
   return (input * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::input0x8dd2598() {
   double input = -0.215362;
   input += synapse0x8dd2778();
   input += synapse0x8dd27a0();
   input += synapse0x8dd27c8();
   input += synapse0x8dd27f0();
   input += synapse0x8dd2818();
   input += synapse0x8dd2840();
   input += synapse0x8dd2868();
   input += synapse0x8dd2890();
   input += synapse0x8dd28b8();
   input += synapse0x8dd28e0();
   input += synapse0x8dd2908();
   input += synapse0x8dd2930();
   input += synapse0x8dd2958();
   input += synapse0x8dd2980();
   input += synapse0x8dd29a8();
   input += synapse0x8dd29d0();
   input += synapse0x8dd2a80();
   input += synapse0x8dd2aa8();
   input += synapse0x8dd2ad0();
   input += synapse0x8dd2af8();
   input += synapse0x8dd2b20();
   input += synapse0x8dd2b48();
   input += synapse0x8dd2b70();
   input += synapse0x8dd2b98();
   input += synapse0x8dd2bc0();
   input += synapse0x8dd2be8();
   input += synapse0x8dd2c10();
   input += synapse0x8dd2c38();
   input += synapse0x8dd2c60();
   input += synapse0x8dd2c88();
   input += synapse0x8dd2cb0();
   input += synapse0x8dd2cd8();
   input += synapse0x8dd29f8();
   input += synapse0x8dd2a20();
   input += synapse0x8dd2a48();
   input += synapse0x8dd2e08();
   input += synapse0x8dd2e30();
   return input;
}

double NNPerpElectronAngleCorrectionBCPDir::neuron0x8dd2598() {
   double input = input0x8dd2598();
   return (input * 1)+0;
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8d57638() {
   return (neuron0x8dc30e0()*-0.248222);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee188() {
   return (neuron0x8dc3278()*-0.0426547);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee1d0() {
   return (neuron0x8dc34a0()*-0.109788);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee2a0() {
   return (neuron0x8dc36a0()*-0.276502);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc51f0() {
   return (neuron0x8dc38a0()*0.0339728);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5218() {
   return (neuron0x8dc3ac8()*-0.130107);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5240() {
   return (neuron0x8dc3cc8()*-0.418937);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5268() {
   return (neuron0x8dc3ec8()*-0.0373134);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5290() {
   return (neuron0x8dc40c8()*-0.187351);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc52b8() {
   return (neuron0x8dc42c8()*-0.139914);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc52e0() {
   return (neuron0x8dc44c8()*0.0329712);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5308() {
   return (neuron0x8dc46c8()*-0.0145191);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5330() {
   return (neuron0x8dc48c8()*0.190456);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5358() {
   return (neuron0x8dc4ae0()*-0.459829);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5380() {
   return (neuron0x8dc4cf8()*-0.259446);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5630() {
   return (neuron0x8dc30e0()*-0.00941659);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5658() {
   return (neuron0x8dc3278()*0.0321461);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5708() {
   return (neuron0x8dc34a0()*-0.32737);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5730() {
   return (neuron0x8dc36a0()*-0.130649);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5758() {
   return (neuron0x8dc38a0()*0.434609);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5780() {
   return (neuron0x8dc3ac8()*-0.453096);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc57a8() {
   return (neuron0x8dc3cc8()*-0.381637);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc57d0() {
   return (neuron0x8dc3ec8()*0.332192);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc57f8() {
   return (neuron0x8dc40c8()*-0.295323);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5820() {
   return (neuron0x8dc42c8()*-0.0884974);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5848() {
   return (neuron0x8dc44c8()*0.369355);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5870() {
   return (neuron0x8dc46c8()*0.152845);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5898() {
   return (neuron0x8dc48c8()*-0.408652);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc58c0() {
   return (neuron0x8dc4ae0()*-0.207394);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc58e8() {
   return (neuron0x8dc4cf8()*-0.140647);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5a80() {
   return (neuron0x8dc30e0()*-0.129075);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5aa8() {
   return (neuron0x8dc3278()*0.289433);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5ad0() {
   return (neuron0x8dc34a0()*-0.456127);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5680() {
   return (neuron0x8dc36a0()*0.227543);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc56a8() {
   return (neuron0x8dc38a0()*0.233801);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc56d0() {
   return (neuron0x8dc3ac8()*0.325828);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8d57660() {
   return (neuron0x8dc3cc8()*-0.267988);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8d57688() {
   return (neuron0x8dc3ec8()*0.296606);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8d576b0() {
   return (neuron0x8dc40c8()*-0.0574016);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8d576d8() {
   return (neuron0x8dc42c8()*-0.211481);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5c00() {
   return (neuron0x8dc44c8()*-0.262728);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5c28() {
   return (neuron0x8dc46c8()*-0.20028);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5c50() {
   return (neuron0x8dc48c8()*0.0185606);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5c78() {
   return (neuron0x8dc4ae0()*0.410381);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5ca0() {
   return (neuron0x8dc4cf8()*0.00785274);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5ec8() {
   return (neuron0x8dc30e0()*-0.288978);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5ef0() {
   return (neuron0x8dc3278()*-0.37992);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5f18() {
   return (neuron0x8dc34a0()*-0.45692);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5f40() {
   return (neuron0x8dc36a0()*0.0750913);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5f68() {
   return (neuron0x8dc38a0()*0.31104);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5f90() {
   return (neuron0x8dc3ac8()*-0.0601662);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5fb8() {
   return (neuron0x8dc3cc8()*-0.357606);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5fe0() {
   return (neuron0x8dc3ec8()*0.0181898);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6008() {
   return (neuron0x8dc40c8()*0.0817726);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6030() {
   return (neuron0x8dc42c8()*-0.284415);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6058() {
   return (neuron0x8dc44c8()*-0.22274);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6080() {
   return (neuron0x8dc46c8()*-0.302819);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc60a8() {
   return (neuron0x8dc48c8()*-0.24949);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc60d0() {
   return (neuron0x8dc4ae0()*-0.384179);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc60f8() {
   return (neuron0x8dc4cf8()*-0.30568);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6320() {
   return (neuron0x8dc30e0()*0.170253);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6348() {
   return (neuron0x8dc3278()*-0.00525945);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6370() {
   return (neuron0x8dc34a0()*-0.39289);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6398() {
   return (neuron0x8dc36a0()*-0.0821823);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc63c0() {
   return (neuron0x8dc38a0()*0.369992);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8d57600() {
   return (neuron0x8dc3ac8()*-0.0410954);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee2c8() {
   return (neuron0x8dc3cc8()*-0.185872);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee2f0() {
   return (neuron0x8dc3ec8()*0.190177);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee318() {
   return (neuron0x8dc40c8()*0.294297);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee340() {
   return (neuron0x8dc42c8()*-0.33325);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee3b8() {
   return (neuron0x8dc44c8()*0.17214);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee3e0() {
   return (neuron0x8dc46c8()*-0.421825);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee408() {
   return (neuron0x8dc48c8()*-0.267589);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee430() {
   return (neuron0x8dc4ae0()*-0.338967);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8bee458() {
   return (neuron0x8dc4cf8()*-0.165671);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6808() {
   return (neuron0x8dc30e0()*-0.109028);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6830() {
   return (neuron0x8dc3278()*0.449308);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6858() {
   return (neuron0x8dc34a0()*-0.0472818);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6880() {
   return (neuron0x8dc36a0()*-0.017022);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc68a8() {
   return (neuron0x8dc38a0()*0.0831411);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc68d0() {
   return (neuron0x8dc3ac8()*-0.184746);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc68f8() {
   return (neuron0x8dc3cc8()*0.366148);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6920() {
   return (neuron0x8dc3ec8()*0.153377);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6948() {
   return (neuron0x8dc40c8()*0.0645635);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6970() {
   return (neuron0x8dc42c8()*-0.196681);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6998() {
   return (neuron0x8dc44c8()*0.199962);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc69c0() {
   return (neuron0x8dc46c8()*0.289513);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc69e8() {
   return (neuron0x8dc48c8()*-0.311594);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6a10() {
   return (neuron0x8dc4ae0()*0.457459);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6a38() {
   return (neuron0x8dc4cf8()*-0.0783636);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6c78() {
   return (neuron0x8dc30e0()*-0.272144);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6ca0() {
   return (neuron0x8dc3278()*-0.211652);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6cc8() {
   return (neuron0x8dc34a0()*0.384717);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6cf0() {
   return (neuron0x8dc36a0()*-0.174373);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6d18() {
   return (neuron0x8dc38a0()*0.22237);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6d40() {
   return (neuron0x8dc3ac8()*0.0821905);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6d68() {
   return (neuron0x8dc3cc8()*0.263792);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6d90() {
   return (neuron0x8dc3ec8()*0.38557);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6db8() {
   return (neuron0x8dc40c8()*0.313375);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6de0() {
   return (neuron0x8dc42c8()*-0.128876);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6e08() {
   return (neuron0x8dc44c8()*-0.105074);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6e30() {
   return (neuron0x8dc46c8()*0.400575);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6e58() {
   return (neuron0x8dc48c8()*-0.213059);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6e80() {
   return (neuron0x8dc4ae0()*-0.49578);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc6ea8() {
   return (neuron0x8dc4cf8()*-0.129461);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc70e8() {
   return (neuron0x8dc30e0()*0.0071929);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7110() {
   return (neuron0x8dc3278()*-0.311584);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7138() {
   return (neuron0x8dc34a0()*-0.309033);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7160() {
   return (neuron0x8dc36a0()*0.109494);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7188() {
   return (neuron0x8dc38a0()*0.107764);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc71b0() {
   return (neuron0x8dc3ac8()*0.110117);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc71d8() {
   return (neuron0x8dc3cc8()*-0.161607);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7200() {
   return (neuron0x8dc3ec8()*0.131214);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7228() {
   return (neuron0x8dc40c8()*0.309913);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7250() {
   return (neuron0x8dc42c8()*0.083242);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7278() {
   return (neuron0x8dc44c8()*-0.0758986);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc72a0() {
   return (neuron0x8dc46c8()*-0.327454);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc72c8() {
   return (neuron0x8dc48c8()*-0.160044);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc72f0() {
   return (neuron0x8dc4ae0()*-0.502314);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7318() {
   return (neuron0x8dc4cf8()*0.22891);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7558() {
   return (neuron0x8dc30e0()*-0.332125);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7580() {
   return (neuron0x8dc3278()*0.215448);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc75a8() {
   return (neuron0x8dc34a0()*0.309161);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc75d0() {
   return (neuron0x8dc36a0()*-0.265213);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc75f8() {
   return (neuron0x8dc38a0()*0.250668);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7620() {
   return (neuron0x8dc3ac8()*-0.188245);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7648() {
   return (neuron0x8dc3cc8()*-0.0905173);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7670() {
   return (neuron0x8dc3ec8()*-0.116688);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7698() {
   return (neuron0x8dc40c8()*-0.368529);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc9ea0() {
   return (neuron0x8dc42c8()*-0.175447);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc9ec8() {
   return (neuron0x8dc44c8()*0.247734);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc9ef0() {
   return (neuron0x8dc46c8()*-0.209613);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc9f18() {
   return (neuron0x8dc48c8()*-0.260596);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc9f40() {
   return (neuron0x8dc4ae0()*-0.0836517);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc9f68() {
   return (neuron0x8dc4cf8()*-0.245882);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5b40() {
   return (neuron0x8dc30e0()*-0.40597);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5b68() {
   return (neuron0x8dc3278()*-0.109874);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5b90() {
   return (neuron0x8dc34a0()*-0.0625254);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc5bb8() {
   return (neuron0x8dc36a0()*0.0652448);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc65b8() {
   return (neuron0x8dc38a0()*-0.0318478);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7ac8() {
   return (neuron0x8dc3ac8()*-0.0350668);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7af0() {
   return (neuron0x8dc3cc8()*-0.163196);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7b18() {
   return (neuron0x8dc3ec8()*-0.221835);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7b40() {
   return (neuron0x8dc40c8()*0.0630517);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7b68() {
   return (neuron0x8dc42c8()*0.193565);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7b90() {
   return (neuron0x8dc44c8()*0.259583);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7bb8() {
   return (neuron0x8dc46c8()*0.200931);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7be0() {
   return (neuron0x8dc48c8()*0.430534);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7c08() {
   return (neuron0x8dc4ae0()*-0.0637813);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7c30() {
   return (neuron0x8dc4cf8()*0.187754);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7e70() {
   return (neuron0x8dc30e0()*-0.00536358);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7e98() {
   return (neuron0x8dc3278()*0.222011);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7ec0() {
   return (neuron0x8dc34a0()*-0.191216);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7ee8() {
   return (neuron0x8dc36a0()*-0.104351);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7f10() {
   return (neuron0x8dc38a0()*0.148083);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7f38() {
   return (neuron0x8dc3ac8()*-0.259457);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7f60() {
   return (neuron0x8dc3cc8()*0.150012);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7f88() {
   return (neuron0x8dc3ec8()*0.372143);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7fb0() {
   return (neuron0x8dc40c8()*-0.144464);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7fd8() {
   return (neuron0x8dc42c8()*-0.0859061);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8000() {
   return (neuron0x8dc44c8()*0.152464);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8028() {
   return (neuron0x8dc46c8()*0.0982547);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8050() {
   return (neuron0x8dc48c8()*-0.0253591);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8078() {
   return (neuron0x8dc4ae0()*0.0406972);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc80a0() {
   return (neuron0x8dc4cf8()*0.376813);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc82e0() {
   return (neuron0x8dc30e0()*0.0280051);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8308() {
   return (neuron0x8dc3278()*-0.393634);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8330() {
   return (neuron0x8dc34a0()*0.146303);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8358() {
   return (neuron0x8dc36a0()*0.294094);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8380() {
   return (neuron0x8dc38a0()*0.0737123);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc83a8() {
   return (neuron0x8dc3ac8()*0.00678069);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc83d0() {
   return (neuron0x8dc3cc8()*0.137369);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc83f8() {
   return (neuron0x8dc3ec8()*0.130304);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8420() {
   return (neuron0x8dc40c8()*0.362736);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8448() {
   return (neuron0x8dc42c8()*0.236659);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8470() {
   return (neuron0x8dc44c8()*0.0921749);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8498() {
   return (neuron0x8dc46c8()*0.232427);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc84c0() {
   return (neuron0x8dc48c8()*0.0194021);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc84e8() {
   return (neuron0x8dc4ae0()*0.0527357);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8510() {
   return (neuron0x8dc4cf8()*-0.191423);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8750() {
   return (neuron0x8dc30e0()*0.358104);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8778() {
   return (neuron0x8dc3278()*0.284867);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc87a0() {
   return (neuron0x8dc34a0()*-0.0309941);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc87c8() {
   return (neuron0x8dc36a0()*0.0979833);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc87f0() {
   return (neuron0x8dc38a0()*-0.142679);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8818() {
   return (neuron0x8dc3ac8()*0.162385);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8840() {
   return (neuron0x8dc3cc8()*0.431075);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8868() {
   return (neuron0x8dc3ec8()*0.142785);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8890() {
   return (neuron0x8dc40c8()*0.249376);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc88b8() {
   return (neuron0x8dc42c8()*0.177795);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc88e0() {
   return (neuron0x8dc44c8()*-0.0760803);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8908() {
   return (neuron0x8dc46c8()*-0.0516114);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8930() {
   return (neuron0x8dc48c8()*-0.27347);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8958() {
   return (neuron0x8dc4ae0()*-0.26899);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8980() {
   return (neuron0x8dc4cf8()*0.320351);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8bc0() {
   return (neuron0x8dc30e0()*-0.277702);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8be8() {
   return (neuron0x8dc3278()*0.12841);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8c10() {
   return (neuron0x8dc34a0()*-0.324028);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8c38() {
   return (neuron0x8dc36a0()*-0.182645);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8c60() {
   return (neuron0x8dc38a0()*0.0256619);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8c88() {
   return (neuron0x8dc3ac8()*0.156247);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8cb0() {
   return (neuron0x8dc3cc8()*0.00805756);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8cd8() {
   return (neuron0x8dc3ec8()*0.354635);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8d00() {
   return (neuron0x8dc40c8()*0.0736389);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8d28() {
   return (neuron0x8dc42c8()*0.0656329);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8d50() {
   return (neuron0x8dc44c8()*-0.413319);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8d78() {
   return (neuron0x8dc46c8()*-0.384497);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8da0() {
   return (neuron0x8dc48c8()*-0.210821);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8dc8() {
   return (neuron0x8dc4ae0()*-0.140212);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc8df0() {
   return (neuron0x8dc4cf8()*-0.406916);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca158() {
   return (neuron0x8dc30e0()*0.142282);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca180() {
   return (neuron0x8dc3278()*0.0525705);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca1a8() {
   return (neuron0x8dc34a0()*0.144143);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca1d0() {
   return (neuron0x8dc36a0()*0.0556455);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca1f8() {
   return (neuron0x8dc38a0()*0.355088);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca220() {
   return (neuron0x8dc3ac8()*0.0150996);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca248() {
   return (neuron0x8dc3cc8()*0.363715);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca270() {
   return (neuron0x8dc3ec8()*0.217306);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca298() {
   return (neuron0x8dc40c8()*0.242781);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca2c0() {
   return (neuron0x8dc42c8()*0.267025);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca2e8() {
   return (neuron0x8dc44c8()*-0.00188152);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca310() {
   return (neuron0x8dc46c8()*-0.397422);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca338() {
   return (neuron0x8dc48c8()*-0.275711);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca360() {
   return (neuron0x8dc4ae0()*-0.251302);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca388() {
   return (neuron0x8dc4cf8()*-0.0474279);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca5b0() {
   return (neuron0x8dc30e0()*0.124457);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca660() {
   return (neuron0x8dc3278()*-0.0547254);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca710() {
   return (neuron0x8dc34a0()*0.015442);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca7c0() {
   return (neuron0x8dc36a0()*-0.296913);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca870() {
   return (neuron0x8dc38a0()*-0.021549);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca920() {
   return (neuron0x8dc3ac8()*-0.247542);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca9d0() {
   return (neuron0x8dc3cc8()*-0.197082);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcaa80() {
   return (neuron0x8dc3ec8()*-0.0384162);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcab30() {
   return (neuron0x8dc40c8()*-0.197335);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcabe0() {
   return (neuron0x8dc42c8()*0.176162);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcac90() {
   return (neuron0x8dc44c8()*0.281825);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcad40() {
   return (neuron0x8dc46c8()*-0.38088);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcadf0() {
   return (neuron0x8dc48c8()*-0.0737066);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcaea0() {
   return (neuron0x8dc4ae0()*0.40163);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcaf50() {
   return (neuron0x8dc4cf8()*0.209515);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb128() {
   return (neuron0x8dc30e0()*-0.326325);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb150() {
   return (neuron0x8dc3278()*0.137065);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb178() {
   return (neuron0x8dc34a0()*-0.439263);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb1a0() {
   return (neuron0x8dc36a0()*-0.12619);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb1c8() {
   return (neuron0x8dc38a0()*-0.184833);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb1f0() {
   return (neuron0x8dc3ac8()*-0.413068);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb218() {
   return (neuron0x8dc3cc8()*-0.261195);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb240() {
   return (neuron0x8dc3ec8()*-0.185629);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb268() {
   return (neuron0x8dc40c8()*-0.229829);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb290() {
   return (neuron0x8dc42c8()*-0.198595);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb2b8() {
   return (neuron0x8dc44c8()*0.0243611);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb2e0() {
   return (neuron0x8dc46c8()*-0.467129);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb308() {
   return (neuron0x8dc48c8()*-0.360888);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb330() {
   return (neuron0x8dc4ae0()*-0.460157);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb358() {
   return (neuron0x8dc4cf8()*0.441245);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc55a8() {
   return (neuron0x8dc30e0()*0.11928);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc55d0() {
   return (neuron0x8dc3278()*-0.464867);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc55f8() {
   return (neuron0x8dc34a0()*-0.366821);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc4238() {
   return (neuron0x8dc36a0()*-0.390609);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc4038() {
   return (neuron0x8dc38a0()*-0.204017);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc3e38() {
   return (neuron0x8dc3ac8()*-0.026019);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc3c38() {
   return (neuron0x8dc3cc8()*0.320768);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc3a38() {
   return (neuron0x8dc3ec8()*-0.357404);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc3810() {
   return (neuron0x8dc40c8()*0.153928);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc3610() {
   return (neuron0x8dc42c8()*-0.080464);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc3410() {
   return (neuron0x8dc44c8()*0.0488516);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8da31e8() {
   return (neuron0x8dc46c8()*0.209976);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc76c0() {
   return (neuron0x8dc48c8()*0.190793);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc76e8() {
   return (neuron0x8dc4ae0()*0.367176);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7710() {
   return (neuron0x8dc4cf8()*0.471644);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7950() {
   return (neuron0x8dc30e0()*0.339502);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7978() {
   return (neuron0x8dc3278()*0.0261003);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc79a0() {
   return (neuron0x8dc34a0()*0.13853);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc79c8() {
   return (neuron0x8dc36a0()*-0.0837264);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc79f0() {
   return (neuron0x8dc38a0()*-0.214321);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7a18() {
   return (neuron0x8dc3ac8()*-0.26705);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7a40() {
   return (neuron0x8dc3cc8()*-0.429555);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7a68() {
   return (neuron0x8dc3ec8()*0.147803);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dc7a90() {
   return (neuron0x8dc40c8()*-0.16022);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbdb8() {
   return (neuron0x8dc42c8()*-0.356836);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbde0() {
   return (neuron0x8dc44c8()*0.0411312);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbe08() {
   return (neuron0x8dc46c8()*-0.40001);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbe30() {
   return (neuron0x8dc48c8()*-0.268127);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbe58() {
   return (neuron0x8dc4ae0()*-0.350256);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbe80() {
   return (neuron0x8dc4cf8()*0.369022);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc0a8() {
   return (neuron0x8dc30e0()*-0.396456);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc0d0() {
   return (neuron0x8dc3278()*0.0834999);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc0f8() {
   return (neuron0x8dc34a0()*0.215304);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc120() {
   return (neuron0x8dc36a0()*0.0198609);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc148() {
   return (neuron0x8dc38a0()*0.072927);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc170() {
   return (neuron0x8dc3ac8()*0.201854);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc198() {
   return (neuron0x8dc3cc8()*-0.316072);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc1c0() {
   return (neuron0x8dc3ec8()*0.110964);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc1e8() {
   return (neuron0x8dc40c8()*-0.0842963);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc210() {
   return (neuron0x8dc42c8()*0.356868);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc238() {
   return (neuron0x8dc44c8()*-0.268066);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc260() {
   return (neuron0x8dc46c8()*-0.167875);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc288() {
   return (neuron0x8dc48c8()*-0.0929481);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc2b0() {
   return (neuron0x8dc4ae0()*-0.39936);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc2d8() {
   return (neuron0x8dc4cf8()*0.14342);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc500() {
   return (neuron0x8dc30e0()*-0.135771);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc528() {
   return (neuron0x8dc3278()*-0.126648);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc550() {
   return (neuron0x8dc34a0()*0.337109);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc578() {
   return (neuron0x8dc36a0()*-0.132148);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc5a0() {
   return (neuron0x8dc38a0()*-0.161668);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc5c8() {
   return (neuron0x8dc3ac8()*-0.366763);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc5f0() {
   return (neuron0x8dc3cc8()*-0.329036);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc618() {
   return (neuron0x8dc3ec8()*0.0123157);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc640() {
   return (neuron0x8dc40c8()*-0.216414);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc668() {
   return (neuron0x8dc42c8()*0.13186);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc690() {
   return (neuron0x8dc44c8()*0.353037);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc6b8() {
   return (neuron0x8dc46c8()*-0.216011);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc6e0() {
   return (neuron0x8dc48c8()*-0.250834);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc708() {
   return (neuron0x8dc4ae0()*-0.19667);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc730() {
   return (neuron0x8dc4cf8()*0.283778);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc958() {
   return (neuron0x8dc30e0()*-0.421023);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc980() {
   return (neuron0x8dc3278()*0.0845718);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc9a8() {
   return (neuron0x8dc34a0()*0.335987);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc9d0() {
   return (neuron0x8dc36a0()*0.00425479);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcc9f8() {
   return (neuron0x8dc38a0()*-0.157466);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcca20() {
   return (neuron0x8dc3ac8()*0.0780464);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcca48() {
   return (neuron0x8dc3cc8()*-0.0580191);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcca70() {
   return (neuron0x8dc3ec8()*-0.121994);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcca98() {
   return (neuron0x8dc40c8()*-0.155544);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccac0() {
   return (neuron0x8dc42c8()*0.118062);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccae8() {
   return (neuron0x8dc44c8()*0.363441);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccb10() {
   return (neuron0x8dc46c8()*0.253059);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccb38() {
   return (neuron0x8dc48c8()*0.326571);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccb60() {
   return (neuron0x8dc4ae0()*0.359892);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccb88() {
   return (neuron0x8dc4cf8()*0.0572604);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccdb0() {
   return (neuron0x8dc30e0()*-0.0528712);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccdd8() {
   return (neuron0x8dc3278()*-0.362994);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcce00() {
   return (neuron0x8dc34a0()*-0.061629);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcce28() {
   return (neuron0x8dc36a0()*-0.134434);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcce50() {
   return (neuron0x8dc38a0()*0.250671);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcce78() {
   return (neuron0x8dc3ac8()*-0.0924763);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccea0() {
   return (neuron0x8dc3cc8()*-0.221848);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccec8() {
   return (neuron0x8dc3ec8()*-0.378649);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccef0() {
   return (neuron0x8dc40c8()*-0.285163);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccf18() {
   return (neuron0x8dc42c8()*0.250938);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccf40() {
   return (neuron0x8dc44c8()*-0.0344853);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccf68() {
   return (neuron0x8dc46c8()*0.135831);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccf90() {
   return (neuron0x8dc48c8()*0.0853048);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccfb8() {
   return (neuron0x8dc4ae0()*-0.491539);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dccfe0() {
   return (neuron0x8dc4cf8()*-0.322305);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd208() {
   return (neuron0x8dc30e0()*0.289409);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd230() {
   return (neuron0x8dc3278()*0.458994);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd258() {
   return (neuron0x8dc34a0()*-0.253883);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd280() {
   return (neuron0x8dc36a0()*0.460853);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd2a8() {
   return (neuron0x8dc38a0()*-0.0701731);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd2d0() {
   return (neuron0x8dc3ac8()*0.249208);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd2f8() {
   return (neuron0x8dc3cc8()*-0.201638);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd320() {
   return (neuron0x8dc3ec8()*-0.4651);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd348() {
   return (neuron0x8dc40c8()*0.172517);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd370() {
   return (neuron0x8dc42c8()*-0.0240088);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd398() {
   return (neuron0x8dc44c8()*0.258359);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd3c0() {
   return (neuron0x8dc46c8()*-0.407034);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd3e8() {
   return (neuron0x8dc48c8()*0.292332);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd410() {
   return (neuron0x8dc4ae0()*0.0760526);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd438() {
   return (neuron0x8dc4cf8()*-0.0462963);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd660() {
   return (neuron0x8dc30e0()*0.381259);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd688() {
   return (neuron0x8dc3278()*-0.140333);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd6b0() {
   return (neuron0x8dc34a0()*-0.385562);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd6d8() {
   return (neuron0x8dc36a0()*-0.108175);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd700() {
   return (neuron0x8dc38a0()*-0.0891366);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd728() {
   return (neuron0x8dc3ac8()*0.342811);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd750() {
   return (neuron0x8dc3cc8()*0.106343);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd778() {
   return (neuron0x8dc3ec8()*0.107758);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd7a0() {
   return (neuron0x8dc40c8()*-0.00594513);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd7c8() {
   return (neuron0x8dc42c8()*-0.144805);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd7f0() {
   return (neuron0x8dc44c8()*0.19082);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd818() {
   return (neuron0x8dc46c8()*-0.371973);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd840() {
   return (neuron0x8dc48c8()*-0.0841273);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd868() {
   return (neuron0x8dc4ae0()*0.398247);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcd890() {
   return (neuron0x8dc4cf8()*0.247945);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdab8() {
   return (neuron0x8dc30e0()*-0.128972);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdae0() {
   return (neuron0x8dc3278()*0.132703);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdb08() {
   return (neuron0x8dc34a0()*0.438205);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdb30() {
   return (neuron0x8dc36a0()*0.117949);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdb58() {
   return (neuron0x8dc38a0()*0.455398);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdb80() {
   return (neuron0x8dc3ac8()*0.0571369);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdba8() {
   return (neuron0x8dc3cc8()*0.33039);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdbd0() {
   return (neuron0x8dc3ec8()*0.435943);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdbf8() {
   return (neuron0x8dc40c8()*-0.413792);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdc20() {
   return (neuron0x8dc42c8()*0.0945555);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdc48() {
   return (neuron0x8dc44c8()*0.106704);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdc70() {
   return (neuron0x8dc46c8()*-0.236322);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdc98() {
   return (neuron0x8dc48c8()*-0.0561732);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdcc0() {
   return (neuron0x8dc4ae0()*0.041749);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdce8() {
   return (neuron0x8dc4cf8()*-0.471519);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdf10() {
   return (neuron0x8dc30e0()*0.422552);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdf38() {
   return (neuron0x8dc3278()*-0.35895);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdf60() {
   return (neuron0x8dc34a0()*-0.162899);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdf88() {
   return (neuron0x8dc36a0()*-0.0346395);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdfb0() {
   return (neuron0x8dc38a0()*0.0743758);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcdfd8() {
   return (neuron0x8dc3ac8()*-0.0238522);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce000() {
   return (neuron0x8dc3cc8()*-0.086511);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce028() {
   return (neuron0x8dc3ec8()*-0.331991);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce050() {
   return (neuron0x8dc40c8()*0.389106);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce078() {
   return (neuron0x8dc42c8()*-0.2933);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce0a0() {
   return (neuron0x8dc44c8()*0.0818504);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce0c8() {
   return (neuron0x8dc46c8()*-0.0616332);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce0f0() {
   return (neuron0x8dc48c8()*-0.195099);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce118() {
   return (neuron0x8dc4ae0()*-0.452959);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce140() {
   return (neuron0x8dc4cf8()*0.371561);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce368() {
   return (neuron0x8dc30e0()*-0.127914);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce390() {
   return (neuron0x8dc3278()*-0.0464425);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce3b8() {
   return (neuron0x8dc34a0()*0.245679);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce3e0() {
   return (neuron0x8dc36a0()*-0.237829);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce408() {
   return (neuron0x8dc38a0()*0.284109);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce430() {
   return (neuron0x8dc3ac8()*-0.159087);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce458() {
   return (neuron0x8dc3cc8()*0.00118975);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce480() {
   return (neuron0x8dc3ec8()*-0.363175);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce4a8() {
   return (neuron0x8dc40c8()*-0.304769);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce4d0() {
   return (neuron0x8dc42c8()*-0.242311);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce4f8() {
   return (neuron0x8dc44c8()*0.145182);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce520() {
   return (neuron0x8dc46c8()*-0.00943595);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce548() {
   return (neuron0x8dc48c8()*-0.106358);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce570() {
   return (neuron0x8dc4ae0()*0.475319);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce598() {
   return (neuron0x8dc4cf8()*-0.407177);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce7c0() {
   return (neuron0x8dc30e0()*0.157229);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce7e8() {
   return (neuron0x8dc3278()*-0.121504);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce810() {
   return (neuron0x8dc34a0()*0.218527);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce838() {
   return (neuron0x8dc36a0()*-0.119286);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce860() {
   return (neuron0x8dc38a0()*-0.0212126);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce888() {
   return (neuron0x8dc3ac8()*-0.0595667);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce8b0() {
   return (neuron0x8dc3cc8()*0.00175971);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce8d8() {
   return (neuron0x8dc3ec8()*-0.239453);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce900() {
   return (neuron0x8dc40c8()*-0.20979);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce928() {
   return (neuron0x8dc42c8()*0.277779);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce950() {
   return (neuron0x8dc44c8()*0.222631);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce978() {
   return (neuron0x8dc46c8()*-0.107308);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce9a0() {
   return (neuron0x8dc48c8()*0.159221);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce9c8() {
   return (neuron0x8dc4ae0()*-0.381376);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dce9f0() {
   return (neuron0x8dc4cf8()*0.183972);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcec18() {
   return (neuron0x8dc30e0()*-0.304986);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcec40() {
   return (neuron0x8dc3278()*0.0584696);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcec68() {
   return (neuron0x8dc34a0()*0.281517);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcec90() {
   return (neuron0x8dc36a0()*-0.34246);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcecb8() {
   return (neuron0x8dc38a0()*0.273068);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcece0() {
   return (neuron0x8dc3ac8()*-0.398928);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dced08() {
   return (neuron0x8dc3cc8()*0.0555713);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dced30() {
   return (neuron0x8dc3ec8()*0.378156);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dced58() {
   return (neuron0x8dc40c8()*0.355574);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dced80() {
   return (neuron0x8dc42c8()*0.0192468);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dceda8() {
   return (neuron0x8dc44c8()*-0.100345);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcedd0() {
   return (neuron0x8dc46c8()*0.362952);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcedf8() {
   return (neuron0x8dc48c8()*-0.330389);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcee20() {
   return (neuron0x8dc4ae0()*0.400291);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcee48() {
   return (neuron0x8dc4cf8()*0.44764);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf070() {
   return (neuron0x8dc30e0()*0.162268);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf098() {
   return (neuron0x8dc3278()*0.237778);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf0c0() {
   return (neuron0x8dc34a0()*-0.408868);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf0e8() {
   return (neuron0x8dc36a0()*-0.361648);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf110() {
   return (neuron0x8dc38a0()*0.333428);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf138() {
   return (neuron0x8dc3ac8()*0.248389);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf160() {
   return (neuron0x8dc3cc8()*0.405744);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf188() {
   return (neuron0x8dc3ec8()*0.440012);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf1b0() {
   return (neuron0x8dc40c8()*-0.19489);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf1d8() {
   return (neuron0x8dc42c8()*-0.159034);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf200() {
   return (neuron0x8dc44c8()*-0.177146);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf228() {
   return (neuron0x8dc46c8()*-0.194136);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf250() {
   return (neuron0x8dc48c8()*-0.418416);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf278() {
   return (neuron0x8dc4ae0()*0.155732);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf2a0() {
   return (neuron0x8dc4cf8()*0.124393);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcf4c8() {
   return (neuron0x8dc30e0()*0.31006);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca5d8() {
   return (neuron0x8dc3278()*-0.351718);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca600() {
   return (neuron0x8dc34a0()*-0.0363689);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca628() {
   return (neuron0x8dc36a0()*0.507453);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca688() {
   return (neuron0x8dc38a0()*0.0399295);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca6b0() {
   return (neuron0x8dc3ac8()*0.132535);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca6d8() {
   return (neuron0x8dc3cc8()*0.310423);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca738() {
   return (neuron0x8dc3ec8()*-0.211799);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca760() {
   return (neuron0x8dc40c8()*0.382644);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca788() {
   return (neuron0x8dc42c8()*-0.169925);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca7e8() {
   return (neuron0x8dc44c8()*-0.122211);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca810() {
   return (neuron0x8dc46c8()*0.51184);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca838() {
   return (neuron0x8dc48c8()*-0.150395);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca898() {
   return (neuron0x8dc4ae0()*-0.321121);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca8c0() {
   return (neuron0x8dc4cf8()*0.0511116);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcaaf0() {
   return (neuron0x8dc30e0()*0.0924503);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca8e8() {
   return (neuron0x8dc3278()*0.16902);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dca990() {
   return (neuron0x8dc34a0()*-0.274654);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcaa40() {
   return (neuron0x8dc36a0()*-0.197245);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcab58() {
   return (neuron0x8dc38a0()*-0.367042);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcab80() {
   return (neuron0x8dc3ac8()*0.180564);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcaba8() {
   return (neuron0x8dc3cc8()*0.249331);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcac08() {
   return (neuron0x8dc3ec8()*0.248846);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcac30() {
   return (neuron0x8dc40c8()*0.0795897);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcac58() {
   return (neuron0x8dc42c8()*0.205849);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcacb8() {
   return (neuron0x8dc44c8()*-0.182931);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcace0() {
   return (neuron0x8dc46c8()*0.231048);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcad08() {
   return (neuron0x8dc48c8()*0.276228);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcad68() {
   return (neuron0x8dc4ae0()*-0.14385);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcad90() {
   return (neuron0x8dc4cf8()*0.210809);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcafc0() {
   return (neuron0x8dc30e0()*0.229002);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcadb8() {
   return (neuron0x8dc3278()*-0.37674);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcae60() {
   return (neuron0x8dc34a0()*0.117476);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcaf10() {
   return (neuron0x8dc36a0()*-0.138252);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd06b8() {
   return (neuron0x8dc38a0()*-0.243221);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd06e0() {
   return (neuron0x8dc3ac8()*-0.133177);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0708() {
   return (neuron0x8dc3cc8()*0.0431351);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0730() {
   return (neuron0x8dc3ec8()*-0.0250424);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0758() {
   return (neuron0x8dc40c8()*0.158926);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0780() {
   return (neuron0x8dc42c8()*0.242045);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd07a8() {
   return (neuron0x8dc44c8()*0.379218);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd07d0() {
   return (neuron0x8dc46c8()*0.273998);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd07f8() {
   return (neuron0x8dc48c8()*-0.0567679);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0820() {
   return (neuron0x8dc4ae0()*-0.280981);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0848() {
   return (neuron0x8dc4cf8()*-0.116256);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0a70() {
   return (neuron0x8dc30e0()*0.324132);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0a98() {
   return (neuron0x8dc3278()*-0.286273);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd0ac0() {
   return (neuron0x8dc34a0()*0.375536);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb5b0() {
   return (neuron0x8dc36a0()*0.235636);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb5d8() {
   return (neuron0x8dc38a0()*-0.151716);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb600() {
   return (neuron0x8dc3ac8()*0.0690769);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb628() {
   return (neuron0x8dc3cc8()*-0.190382);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb650() {
   return (neuron0x8dc3ec8()*0.249377);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb678() {
   return (neuron0x8dc40c8()*0.308562);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb6a0() {
   return (neuron0x8dc42c8()*0.0583291);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb6c8() {
   return (neuron0x8dc44c8()*0.0647077);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb6f0() {
   return (neuron0x8dc46c8()*-0.125141);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb718() {
   return (neuron0x8dc48c8()*0.0996938);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb740() {
   return (neuron0x8dc4ae0()*0.034124);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb768() {
   return (neuron0x8dc4cf8()*0.187485);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb990() {
   return (neuron0x8dc30e0()*-0.313684);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb9b8() {
   return (neuron0x8dc3278()*0.337679);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcb9e0() {
   return (neuron0x8dc34a0()*0.245206);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcba08() {
   return (neuron0x8dc36a0()*-0.217073);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcba30() {
   return (neuron0x8dc38a0()*-0.0208843);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcba58() {
   return (neuron0x8dc3ac8()*-0.0992081);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcba80() {
   return (neuron0x8dc3cc8()*-0.223946);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbaa8() {
   return (neuron0x8dc3ec8()*-0.194001);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbad0() {
   return (neuron0x8dc40c8()*-0.377364);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbaf8() {
   return (neuron0x8dc42c8()*-0.0981735);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbb20() {
   return (neuron0x8dc44c8()*-0.13505);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbb48() {
   return (neuron0x8dc46c8()*-0.0250471);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbb70() {
   return (neuron0x8dc48c8()*-0.00902346);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbb98() {
   return (neuron0x8dc4ae0()*-0.0720935);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dcbbc0() {
   return (neuron0x8dc4cf8()*-0.232335);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1b38() {
   return (neuron0x8dc30e0()*-0.0598974);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1b60() {
   return (neuron0x8dc3278()*0.466194);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1b88() {
   return (neuron0x8dc34a0()*-0.214916);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1bb0() {
   return (neuron0x8dc36a0()*0.0298935);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1bd8() {
   return (neuron0x8dc38a0()*0.0562854);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1c00() {
   return (neuron0x8dc3ac8()*-0.195061);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1c28() {
   return (neuron0x8dc3cc8()*0.347362);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1c50() {
   return (neuron0x8dc3ec8()*0.27112);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1c78() {
   return (neuron0x8dc40c8()*-0.0698032);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1ca0() {
   return (neuron0x8dc42c8()*-0.269529);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1cc8() {
   return (neuron0x8dc44c8()*0.289077);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1cf0() {
   return (neuron0x8dc46c8()*-0.3284);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1d18() {
   return (neuron0x8dc48c8()*-0.317655);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1d40() {
   return (neuron0x8dc4ae0()*-0.174756);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1d68() {
   return (neuron0x8dc4cf8()*0.0616097);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1eb8() {
   return (neuron0x8dc5038()*-0.14371);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1ee0() {
   return (neuron0x8dc53a8()*0.0346072);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1f08() {
   return (neuron0x8dc5910()*0.00803867);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1f30() {
   return (neuron0x8dc5cc8()*0.013048);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1f58() {
   return (neuron0x8dc6120()*0.0667927);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1f80() {
   return (neuron0x8dc65f0()*-0.0643839);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1fa8() {
   return (neuron0x8dc6a60()*-0.0112566);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1fd0() {
   return (neuron0x8dc6ed0()*-0.00886469);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd1ff8() {
   return (neuron0x8dc7340()*0.0175304);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2020() {
   return (neuron0x8dc63e8()*-0.00220709);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2048() {
   return (neuron0x8dc7c58()*-0.018784);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2070() {
   return (neuron0x8dc80c8()*0.0816103);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2098() {
   return (neuron0x8dc8538()*-0.0721234);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd20c0() {
   return (neuron0x8dc89a8()*0.00212866);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd20e8() {
   return (neuron0x8dc9fe8()*-0.006549);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2110() {
   return (neuron0x8dca3b0()*-0.133043);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd21c0() {
   return (neuron0x8dcb000()*0.00104284);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd21e8() {
   return (neuron0x8dcb380()*0.00516225);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2210() {
   return (neuron0x8dc7738()*-0.0060764);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2238() {
   return (neuron0x8dcbea8()*0.00610108);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2260() {
   return (neuron0x8dcc300()*0.0535732);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2288() {
   return (neuron0x8dcc758()*0.0308906);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd22b0() {
   return (neuron0x8dccbb0()*-0.00396176);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd22d8() {
   return (neuron0x8dcd008()*-0.00815879);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2300() {
   return (neuron0x8dcd460()*-0.00991998);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2328() {
   return (neuron0x8dcd8b8()*0.00473967);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2350() {
   return (neuron0x8dcdd10()*0.0665169);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2378() {
   return (neuron0x8dce168()*0.00268006);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd23a0() {
   return (neuron0x8dce5c0()*0.0833145);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd23c8() {
   return (neuron0x8dcea18()*0.00680494);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd23f0() {
   return (neuron0x8dcee70()*0.00857183);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2418() {
   return (neuron0x8dcf2c8()*-0.0195207);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2138() {
   return (neuron0x8dd0468()*0.128691);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2160() {
   return (neuron0x8dd0590()*-0.00775253);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2188() {
   return (neuron0x8dd0870()*0.0108695);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2548() {
   return (neuron0x8dcb790()*0.145751);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2570() {
   return (neuron0x8dcbbe8()*0.0623936);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2778() {
   return (neuron0x8dc5038()*0.203942);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd27a0() {
   return (neuron0x8dc53a8()*-0.00907913);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd27c8() {
   return (neuron0x8dc5910()*0.0323172);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd27f0() {
   return (neuron0x8dc5cc8()*-0.000908741);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2818() {
   return (neuron0x8dc6120()*-0.0740457);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2840() {
   return (neuron0x8dc65f0()*-0.0609177);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2868() {
   return (neuron0x8dc6a60()*0.0144235);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2890() {
   return (neuron0x8dc6ed0()*-0.104905);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd28b8() {
   return (neuron0x8dc7340()*-0.0831814);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd28e0() {
   return (neuron0x8dc63e8()*-0.00621396);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2908() {
   return (neuron0x8dc7c58()*0.0783301);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2930() {
   return (neuron0x8dc80c8()*-0.0156696);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2958() {
   return (neuron0x8dc8538()*0.122824);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2980() {
   return (neuron0x8dc89a8()*0.0830158);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd29a8() {
   return (neuron0x8dc9fe8()*0.023694);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd29d0() {
   return (neuron0x8dca3b0()*0.132074);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2a80() {
   return (neuron0x8dcb000()*-0.000329741);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2aa8() {
   return (neuron0x8dcb380()*-0.00285384);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2ad0() {
   return (neuron0x8dc7738()*-0.00190279);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2af8() {
   return (neuron0x8dcbea8()*0.0387898);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2b20() {
   return (neuron0x8dcc300()*-0.0304275);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2b48() {
   return (neuron0x8dcc758()*0.0609493);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2b70() {
   return (neuron0x8dccbb0()*0.00266051);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2b98() {
   return (neuron0x8dcd008()*0.00994828);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2bc0() {
   return (neuron0x8dcd460()*-0.0107615);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2be8() {
   return (neuron0x8dcd8b8()*-0.0013751);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2c10() {
   return (neuron0x8dcdd10()*0.0254818);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2c38() {
   return (neuron0x8dce168()*0.0487945);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2c60() {
   return (neuron0x8dce5c0()*-0.0541638);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2c88() {
   return (neuron0x8dcea18()*-0.0115917);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2cb0() {
   return (neuron0x8dcee70()*0.00442792);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2cd8() {
   return (neuron0x8dcf2c8()*0.098303);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd29f8() {
   return (neuron0x8dd0468()*-0.0903203);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2a20() {
   return (neuron0x8dd0590()*0.0268844);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2a48() {
   return (neuron0x8dd0870()*-0.00382979);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2e08() {
   return (neuron0x8dcb790()*-0.0869166);
}

double NNPerpElectronAngleCorrectionBCPDir::synapse0x8dd2e30() {
   return (neuron0x8dcbbe8()*0.052832);
}

