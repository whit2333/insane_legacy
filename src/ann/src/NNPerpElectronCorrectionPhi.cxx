#include "NNPerpElectronCorrectionPhi.h"
#include <cmath>

double NNPerpElectronCorrectionPhi::Value(int index, double in0, double in1, double in2, double in3, double in4, double in5, double in6)
{
   input0 = (in0 - 1807.44) / 957.222;
   input1 = (in1 - -4.86487) / 33.9831;
   input2 = (in2 - -10.8703) / 58.8882;
   input3 = (in3 - 1.39247) / 0.359494;
   input4 = (in4 - 1.52167) / 0.372969;
   input5 = (in5 - 1.12442) / 50.8091;
   input6 = (in6 - 0.205599) / 7.17982;
   switch (index) {
      case 0:
         return neuron0x8ff6838();
      default:
         return 0.;
   }
}

double NNPerpElectronCorrectionPhi::Value(int index, double* input)
{
   input0 = (input[0] - 1807.44) / 957.222;
   input1 = (input[1] - -4.86487) / 33.9831;
   input2 = (input[2] - -10.8703) / 58.8882;
   input3 = (input[3] - 1.39247) / 0.359494;
   input4 = (input[4] - 1.52167) / 0.372969;
   input5 = (input[5] - 1.12442) / 50.8091;
   input6 = (input[6] - 0.205599) / 7.17982;
   switch (index) {
      case 0:
         return neuron0x8ff6838();
      default:
         return 0.;
   }
}

double NNPerpElectronCorrectionPhi::neuron0x8ff5a80()
{
   return input0;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff5c38()
{
   return input1;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff5e38()
{
   return input2;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff6038()
{
   return input3;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff6238()
{
   return input4;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff6438()
{
   return input5;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff6638()
{
   return input6;
}

double NNPerpElectronCorrectionPhi::input0x8ff6960()
{
   double input = -0.250469;
   input += synapse0x8ff6b18();
   input += synapse0x8ff6b40();
   input += synapse0x8ff6b68();
   input += synapse0x8ff6b90();
   input += synapse0x8ff6bb8();
   input += synapse0x8ff6be0();
   input += synapse0x8ff6c08();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff6960()
{
   double input = input0x8ff6960();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff6c30()
{
   double input = 0.35421;
   input += synapse0x8ff6e30();
   input += synapse0x8ff6e58();
   input += synapse0x8ff6e80();
   input += synapse0x8ff6ea8();
   input += synapse0x8ff6ed0();
   input += synapse0x8ff6ef8();
   input += synapse0x8ff6f20();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff6c30()
{
   double input = input0x8ff6c30();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff6f48()
{
   double input = -3.12535;
   input += synapse0x8ff7148();
   input += synapse0x8ff7170();
   input += synapse0x8ff7198();
   input += synapse0x8ff71c0();
   input += synapse0x8ff71e8();
   input += synapse0x8ff7210();
   input += synapse0x8ff7238();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff6f48()
{
   double input = input0x8ff6f48();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff7260()
{
   double input = 0.155766;
   input += synapse0x8ff7418();
   input += synapse0x8ff7440();
   input += synapse0x8ff7468();
   input += synapse0x8ff7490();
   input += synapse0x8ff74b8();
   input += synapse0x8ff74e0();
   input += synapse0x8ff7508();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff7260()
{
   double input = input0x8ff7260();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff7530()
{
   double input = 0.618009;
   input += synapse0x8ff7730();
   input += synapse0x8ff7758();
   input += synapse0x8ff7780();
   input += synapse0x8ff77a8();
   input += synapse0x8ff77d0();
   input += synapse0x8fd5d28();
   input += synapse0x8fd5d50();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff7530()
{
   double input = input0x8ff7530();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff7900()
{
   double input = -0.469132;
   input += synapse0x8ff7b00();
   input += synapse0x8ff7b28();
   input += synapse0x8ff7b50();
   input += synapse0x8ff7b78();
   input += synapse0x8ff7ba0();
   input += synapse0x8ff7bc8();
   input += synapse0x8ff7bf0();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff7900()
{
   double input = input0x8ff7900();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff7c18()
{
   double input = -0.35116;
   input += synapse0x8ff7e30();
   input += synapse0x8ff7e58();
   input += synapse0x8ff7e80();
   input += synapse0x8ff7ea8();
   input += synapse0x8ff7ed0();
   input += synapse0x8ff7ef8();
   input += synapse0x8ff7f20();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff7c18()
{
   double input = input0x8ff7c18();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff7f48()
{
   double input = 0.202204;
   input += synapse0x8ff8160();
   input += synapse0x8ff8188();
   input += synapse0x8ff81b0();
   input += synapse0x8ff81d8();
   input += synapse0x8ff8200();
   input += synapse0x8ff8228();
   input += synapse0x8ff8250();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff7f48()
{
   double input = input0x8ff7f48();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff8278()
{
   double input = -0.177049;
   input += synapse0x8ff8490();
   input += synapse0x8ff84b8();
   input += synapse0x8ff84e0();
   input += synapse0x8ff8508();
   input += synapse0x8ff8530();
   input += synapse0x8ff8558();
   input += synapse0x8ff8580();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff8278()
{
   double input = input0x8ff8278();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff85a8()
{
   double input = 0.0532944;
   input += synapse0x8ff87c0();
   input += synapse0x8ff87e8();
   input += synapse0x8f18450();
   input += synapse0x8f180d8();
   input += synapse0x8ff77f8();
   input += synapse0x8ff7820();
   input += synapse0x8ff7848();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff85a8()
{
   double input = input0x8ff85a8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff8a18()
{
   double input = 1.66174;
   input += synapse0x8ff8c30();
   input += synapse0x8ff8c58();
   input += synapse0x8ff8c80();
   input += synapse0x8ff8ca8();
   input += synapse0x8ff8cd0();
   input += synapse0x8ff8cf8();
   input += synapse0x8ff8d20();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff8a18()
{
   double input = input0x8ff8a18();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff8d48()
{
   double input = -0.0359901;
   input += synapse0x8ff8f60();
   input += synapse0x8ff8f88();
   input += synapse0x8ff8fb0();
   input += synapse0x8ff8fd8();
   input += synapse0x8ff9000();
   input += synapse0x8ff9028();
   input += synapse0x8ff9050();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff8d48()
{
   double input = input0x8ff8d48();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff9078()
{
   double input = -0.175245;
   input += synapse0x8ff9290();
   input += synapse0x8ff92b8();
   input += synapse0x8ff92e0();
   input += synapse0x8ff9308();
   input += synapse0x8ff9330();
   input += synapse0x8ff9358();
   input += synapse0x8ff9380();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff9078()
{
   double input = input0x8ff9078();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff93a8()
{
   double input = -0.313099;
   input += synapse0x8ff95c0();
   input += synapse0x8ff95e8();
   input += synapse0x8ff9610();
   input += synapse0x8ff9638();
   input += synapse0x8ff9660();
   input += synapse0x8ff9688();
   input += synapse0x8ff96b0();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff93a8()
{
   double input = input0x8ff93a8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff96d8()
{
   double input = 0.0475619;
   input += synapse0x8ff98f0();
   input += synapse0x8ff9918();
   input += synapse0x8ff9940();
   input += synapse0x8ff9968();
   input += synapse0x8ff9990();
   input += synapse0x8ff99b8();
   input += synapse0x8ff99e0();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff96d8()
{
   double input = input0x8ff96d8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff9a08()
{
   double input = -0.63875;
   input += synapse0x8ff9c20();
   input += synapse0x8ff9cd0();
   input += synapse0x8ff9d80();
   input += synapse0x8ff9e30();
   input += synapse0x8ff9ee0();
   input += synapse0x8ff9f90();
   input += synapse0x8ffa040();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff9a08()
{
   double input = input0x8ff9a08();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ffa0f0()
{
   double input = -4.74851;
   input += synapse0x8ffa230();
   input += synapse0x8ffa258();
   input += synapse0x8ffa280();
   input += synapse0x8ffa2a8();
   input += synapse0x8ffa2d0();
   input += synapse0x8ffa2f8();
   input += synapse0x8ffa320();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ffa0f0()
{
   double input = input0x8ffa0f0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ffa348()
{
   double input = 0.316544;
   input += synapse0x8ffa488();
   input += synapse0x8ffa4b0();
   input += synapse0x8ffa4d8();
   input += synapse0x8ffa500();
   input += synapse0x8ffa528();
   input += synapse0x8ffa550();
   input += synapse0x8ffa578();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ffa348()
{
   double input = input0x8ffa348();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ffa5a0()
{
   double input = 0.404343;
   input += synapse0x8ffa770();
   input += synapse0x8ffa798();
   input += synapse0x8ffa7c0();
   input += synapse0x8ff7870();
   input += synapse0x8ff7898();
   input += synapse0x8ff78c0();
   input += synapse0x8fd5bc8();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ffa5a0()
{
   double input = input0x8ffa5a0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff8810()
{
   double input = 1.08241;
   input += synapse0x8fd5bf0();
   input += synapse0x8fd5c18();
   input += synapse0x8fd5c40();
   input += synapse0x8fd5c68();
   input += synapse0x8fd5c90();
   input += synapse0x8ffabf0();
   input += synapse0x8ffac18();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff8810()
{
   double input = input0x8ff8810();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ffac40()
{
   double input = -0.513102;
   input += synapse0x8ffae58();
   input += synapse0x8ffae80();
   input += synapse0x8ffaea8();
   input += synapse0x8ffaed0();
   input += synapse0x8ffaef8();
   input += synapse0x8ffaf20();
   input += synapse0x8ffaf48();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ffac40()
{
   double input = input0x8ffac40();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ffaf70()
{
   double input = 0.525623;
   input += synapse0x8ffb188();
   input += synapse0x8ffb1b0();
   input += synapse0x8ffb1d8();
   input += synapse0x8ffb200();
   input += synapse0x8ffb228();
   input += synapse0x8ffb250();
   input += synapse0x8ffb278();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ffaf70()
{
   double input = input0x8ffaf70();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ffb2a0()
{
   double input = -0.28877;
   input += synapse0x8ffb4b8();
   input += synapse0x8ffb4e0();
   input += synapse0x8ffb508();
   input += synapse0x8ffb530();
   input += synapse0x8ffb558();
   input += synapse0x8ffb580();
   input += synapse0x8ffb5a8();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ffb2a0()
{
   double input = input0x8ffb2a0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ffb5d0()
{
   double input = 0.677024;
   input += synapse0x8ffb7e8();
   input += synapse0x8ffb810();
   input += synapse0x8ffc7f8();
   input += synapse0x8ffc820();
   input += synapse0x8ffc848();
   input += synapse0x8ffc870();
   input += synapse0x8ffc898();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ffb5d0()
{
   double input = input0x8ffb5d0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ffc8c0()
{
   double input = 0.562226;
   input += synapse0x8ffcac0();
   input += synapse0x8ffcae8();
   input += synapse0x8ffcb10();
   input += synapse0x8ffcb38();
   input += synapse0x8ffcb60();
   input += synapse0x8ffcb88();
   input += synapse0x8ffcbb0();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ffc8c0()
{
   double input = input0x8ffc8c0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNPerpElectronCorrectionPhi::input0x8ff6838()
{
   double input = 0.423676;
   input += synapse0x8ffccb0();
   input += synapse0x8ffccd8();
   input += synapse0x8ffcd00();
   input += synapse0x8ffcd28();
   input += synapse0x8ffcd50();
   input += synapse0x8ffcd78();
   input += synapse0x8ffcda0();
   input += synapse0x8ffcdc8();
   input += synapse0x8ffcdf0();
   input += synapse0x8ffce18();
   input += synapse0x8ffce40();
   input += synapse0x8ffce68();
   input += synapse0x8ffce90();
   input += synapse0x8ffceb8();
   input += synapse0x8ffcee0();
   input += synapse0x8ffcf08();
   input += synapse0x8ffcfb8();
   input += synapse0x8ffcfe0();
   input += synapse0x8ffd008();
   input += synapse0x8ffd030();
   input += synapse0x8ffd058();
   input += synapse0x8ffd080();
   input += synapse0x8ffd0a8();
   input += synapse0x8ffd0d0();
   input += synapse0x8ffd0f8();
   return input;
}

double NNPerpElectronCorrectionPhi::neuron0x8ff6838()
{
   double input = input0x8ff6838();
   return (input * 1) + 0;
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6b18()
{
   return (neuron0x8ff5a80() * 0.264547);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6b40()
{
   return (neuron0x8ff5c38() * -0.270582);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6b68()
{
   return (neuron0x8ff5e38() * -0.418882);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6b90()
{
   return (neuron0x8ff6038() * -0.224347);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6bb8()
{
   return (neuron0x8ff6238() * -0.48481);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6be0()
{
   return (neuron0x8ff6438() * -0.349558);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6c08()
{
   return (neuron0x8ff6638() * -0.322771);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6e30()
{
   return (neuron0x8ff5a80() * 0.165076);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6e58()
{
   return (neuron0x8ff5c38() * 0.0586536);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6e80()
{
   return (neuron0x8ff5e38() * -0.173652);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6ea8()
{
   return (neuron0x8ff6038() * 0.0553175);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6ed0()
{
   return (neuron0x8ff6238() * -0.16932);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6ef8()
{
   return (neuron0x8ff6438() * 0.390624);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff6f20()
{
   return (neuron0x8ff6638() * 0.411833);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7148()
{
   return (neuron0x8ff5a80() * -2.25722);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7170()
{
   return (neuron0x8ff5c38() * -0.521693);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7198()
{
   return (neuron0x8ff5e38() * -0.483593);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff71c0()
{
   return (neuron0x8ff6038() * 0.0190116);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff71e8()
{
   return (neuron0x8ff6238() * 0.00456789);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7210()
{
   return (neuron0x8ff6438() * 0.460287);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7238()
{
   return (neuron0x8ff6638() * 0.0901264);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7418()
{
   return (neuron0x8ff5a80() * -0.140742);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7440()
{
   return (neuron0x8ff5c38() * 0.228997);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7468()
{
   return (neuron0x8ff5e38() * -0.127123);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7490()
{
   return (neuron0x8ff6038() * -0.217011);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff74b8()
{
   return (neuron0x8ff6238() * 0.184347);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff74e0()
{
   return (neuron0x8ff6438() * 0.18083);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7508()
{
   return (neuron0x8ff6638() * 0.519201);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7730()
{
   return (neuron0x8ff5a80() * -0.843147);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7758()
{
   return (neuron0x8ff5c38() * -0.238604);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7780()
{
   return (neuron0x8ff5e38() * -0.0773173);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff77a8()
{
   return (neuron0x8ff6038() * -0.192252);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff77d0()
{
   return (neuron0x8ff6238() * 0.11634);
}

double NNPerpElectronCorrectionPhi::synapse0x8fd5d28()
{
   return (neuron0x8ff6438() * 0.0230091);
}

double NNPerpElectronCorrectionPhi::synapse0x8fd5d50()
{
   return (neuron0x8ff6638() * -0.177814);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7b00()
{
   return (neuron0x8ff5a80() * -0.293168);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7b28()
{
   return (neuron0x8ff5c38() * 0.407079);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7b50()
{
   return (neuron0x8ff5e38() * -0.116428);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7b78()
{
   return (neuron0x8ff6038() * 0.176599);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7ba0()
{
   return (neuron0x8ff6238() * 0.475564);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7bc8()
{
   return (neuron0x8ff6438() * -0.195443);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7bf0()
{
   return (neuron0x8ff6638() * 0.327715);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7e30()
{
   return (neuron0x8ff5a80() * -0.135831);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7e58()
{
   return (neuron0x8ff5c38() * 0.474348);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7e80()
{
   return (neuron0x8ff5e38() * 0.146907);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7ea8()
{
   return (neuron0x8ff6038() * 0.638434);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7ed0()
{
   return (neuron0x8ff6238() * -0.202139);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7ef8()
{
   return (neuron0x8ff6438() * 0.196719);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7f20()
{
   return (neuron0x8ff6638() * 0.423678);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8160()
{
   return (neuron0x8ff5a80() * -0.151743);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8188()
{
   return (neuron0x8ff5c38() * -0.215462);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff81b0()
{
   return (neuron0x8ff5e38() * -0.43607);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff81d8()
{
   return (neuron0x8ff6038() * 0.0590603);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8200()
{
   return (neuron0x8ff6238() * 0.359867);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8228()
{
   return (neuron0x8ff6438() * -0.187819);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8250()
{
   return (neuron0x8ff6638() * -0.268283);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8490()
{
   return (neuron0x8ff5a80() * 0.258216);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff84b8()
{
   return (neuron0x8ff5c38() * 0.149939);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff84e0()
{
   return (neuron0x8ff5e38() * -0.193983);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8508()
{
   return (neuron0x8ff6038() * -0.0433588);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8530()
{
   return (neuron0x8ff6238() * 0.04684);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8558()
{
   return (neuron0x8ff6438() * 0.413147);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8580()
{
   return (neuron0x8ff6638() * -0.0948581);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff87c0()
{
   return (neuron0x8ff5a80() * 0.105667);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff87e8()
{
   return (neuron0x8ff5c38() * 0.157608);
}

double NNPerpElectronCorrectionPhi::synapse0x8f18450()
{
   return (neuron0x8ff5e38() * 0.193946);
}

double NNPerpElectronCorrectionPhi::synapse0x8f180d8()
{
   return (neuron0x8ff6038() * 0.167034);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff77f8()
{
   return (neuron0x8ff6238() * 0.0512739);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7820()
{
   return (neuron0x8ff6438() * 0.414528);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7848()
{
   return (neuron0x8ff6638() * -0.509122);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8c30()
{
   return (neuron0x8ff5a80() * 0.91872);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8c58()
{
   return (neuron0x8ff5c38() * 0.344125);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8c80()
{
   return (neuron0x8ff5e38() * -0.141061);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8ca8()
{
   return (neuron0x8ff6038() * -0.0215562);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8cd0()
{
   return (neuron0x8ff6238() * -0.0429456);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8cf8()
{
   return (neuron0x8ff6438() * 0.196441);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8d20()
{
   return (neuron0x8ff6638() * 0.511567);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8f60()
{
   return (neuron0x8ff5a80() * 0.428887);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8f88()
{
   return (neuron0x8ff5c38() * -0.398415);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8fb0()
{
   return (neuron0x8ff5e38() * -0.363066);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff8fd8()
{
   return (neuron0x8ff6038() * -0.284299);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9000()
{
   return (neuron0x8ff6238() * -0.198816);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9028()
{
   return (neuron0x8ff6438() * -0.384332);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9050()
{
   return (neuron0x8ff6638() * 0.179015);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9290()
{
   return (neuron0x8ff5a80() * 0.662225);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff92b8()
{
   return (neuron0x8ff5c38() * 0.433822);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff92e0()
{
   return (neuron0x8ff5e38() * 0.302619);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9308()
{
   return (neuron0x8ff6038() * -0.121172);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9330()
{
   return (neuron0x8ff6238() * 0.670406);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9358()
{
   return (neuron0x8ff6438() * 0.352969);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9380()
{
   return (neuron0x8ff6638() * 0.327859);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff95c0()
{
   return (neuron0x8ff5a80() * 0.236932);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff95e8()
{
   return (neuron0x8ff5c38() * 0.337095);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9610()
{
   return (neuron0x8ff5e38() * -0.104195);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9638()
{
   return (neuron0x8ff6038() * -0.339337);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9660()
{
   return (neuron0x8ff6238() * -0.27754);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9688()
{
   return (neuron0x8ff6438() * 0.150976);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff96b0()
{
   return (neuron0x8ff6638() * 0.204475);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff98f0()
{
   return (neuron0x8ff5a80() * 0.50283);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9918()
{
   return (neuron0x8ff5c38() * -0.521036);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9940()
{
   return (neuron0x8ff5e38() * -0.00528309);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9968()
{
   return (neuron0x8ff6038() * -0.00686033);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9990()
{
   return (neuron0x8ff6238() * -0.0665172);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff99b8()
{
   return (neuron0x8ff6438() * -0.376531);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff99e0()
{
   return (neuron0x8ff6638() * 0.181447);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9c20()
{
   return (neuron0x8ff5a80() * 0.79225);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9cd0()
{
   return (neuron0x8ff5c38() * 0.35276);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9d80()
{
   return (neuron0x8ff5e38() * 0.111809);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9e30()
{
   return (neuron0x8ff6038() * 0.224933);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9ee0()
{
   return (neuron0x8ff6238() * -0.102753);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff9f90()
{
   return (neuron0x8ff6438() * -0.19995);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa040()
{
   return (neuron0x8ff6638() * -0.257893);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa230()
{
   return (neuron0x8ff5a80() * -2.38555);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa258()
{
   return (neuron0x8ff5c38() * -0.31455);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa280()
{
   return (neuron0x8ff5e38() * -0.457664);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa2a8()
{
   return (neuron0x8ff6038() * 0.0317049);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa2d0()
{
   return (neuron0x8ff6238() * 0.0438826);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa2f8()
{
   return (neuron0x8ff6438() * -0.165625);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa320()
{
   return (neuron0x8ff6638() * 0.0905815);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa488()
{
   return (neuron0x8ff5a80() * 0.247182);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa4b0()
{
   return (neuron0x8ff5c38() * -0.196382);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa4d8()
{
   return (neuron0x8ff5e38() * -0.760486);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa500()
{
   return (neuron0x8ff6038() * -0.0985864);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa528()
{
   return (neuron0x8ff6238() * -0.393227);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa550()
{
   return (neuron0x8ff6438() * 0.324889);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa578()
{
   return (neuron0x8ff6638() * 1.25881);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa770()
{
   return (neuron0x8ff5a80() * 0.184275);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa798()
{
   return (neuron0x8ff5c38() * 0.135229);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffa7c0()
{
   return (neuron0x8ff5e38() * 0.303114);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7870()
{
   return (neuron0x8ff6038() * -0.0297477);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff7898()
{
   return (neuron0x8ff6238() * 0.112858);
}

double NNPerpElectronCorrectionPhi::synapse0x8ff78c0()
{
   return (neuron0x8ff6438() * -0.176244);
}

double NNPerpElectronCorrectionPhi::synapse0x8fd5bc8()
{
   return (neuron0x8ff6638() * 0.492128);
}

double NNPerpElectronCorrectionPhi::synapse0x8fd5bf0()
{
   return (neuron0x8ff5a80() * 0.535395);
}

double NNPerpElectronCorrectionPhi::synapse0x8fd5c18()
{
   return (neuron0x8ff5c38() * 0.0832037);
}

double NNPerpElectronCorrectionPhi::synapse0x8fd5c40()
{
   return (neuron0x8ff5e38() * 0.27735);
}

double NNPerpElectronCorrectionPhi::synapse0x8fd5c68()
{
   return (neuron0x8ff6038() * -0.145121);
}

double NNPerpElectronCorrectionPhi::synapse0x8fd5c90()
{
   return (neuron0x8ff6238() * 0.130861);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffabf0()
{
   return (neuron0x8ff6438() * 0.377669);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffac18()
{
   return (neuron0x8ff6638() * -0.149884);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffae58()
{
   return (neuron0x8ff5a80() * 0.509083);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffae80()
{
   return (neuron0x8ff5c38() * 0.276573);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffaea8()
{
   return (neuron0x8ff5e38() * -0.0314036);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffaed0()
{
   return (neuron0x8ff6038() * 0.109464);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffaef8()
{
   return (neuron0x8ff6238() * 0.320153);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffaf20()
{
   return (neuron0x8ff6438() * -0.482167);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffaf48()
{
   return (neuron0x8ff6638() * -0.336618);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb188()
{
   return (neuron0x8ff5a80() * -0.334644);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb1b0()
{
   return (neuron0x8ff5c38() * -0.561181);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb1d8()
{
   return (neuron0x8ff5e38() * 1.16003);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb200()
{
   return (neuron0x8ff6038() * 0.133992);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb228()
{
   return (neuron0x8ff6238() * -0.0709377);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb250()
{
   return (neuron0x8ff6438() * 0.367267);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb278()
{
   return (neuron0x8ff6638() * 0.401446);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb4b8()
{
   return (neuron0x8ff5a80() * 0.333269);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb4e0()
{
   return (neuron0x8ff5c38() * -0.2254);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb508()
{
   return (neuron0x8ff5e38() * -0.472896);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb530()
{
   return (neuron0x8ff6038() * 0.285036);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb558()
{
   return (neuron0x8ff6238() * 0.317844);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb580()
{
   return (neuron0x8ff6438() * -0.201352);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb5a8()
{
   return (neuron0x8ff6638() * -0.207159);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb7e8()
{
   return (neuron0x8ff5a80() * 0.120335);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffb810()
{
   return (neuron0x8ff5c38() * 0.0305482);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffc7f8()
{
   return (neuron0x8ff5e38() * -0.843669);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffc820()
{
   return (neuron0x8ff6038() * 0.0328194);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffc848()
{
   return (neuron0x8ff6238() * 0.160255);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffc870()
{
   return (neuron0x8ff6438() * 0.239141);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffc898()
{
   return (neuron0x8ff6638() * 0.334227);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcac0()
{
   return (neuron0x8ff5a80() * 0.567096);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcae8()
{
   return (neuron0x8ff5c38() * 0.444183);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcb10()
{
   return (neuron0x8ff5e38() * 0.374886);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcb38()
{
   return (neuron0x8ff6038() * 0.114085);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcb60()
{
   return (neuron0x8ff6238() * 0.292492);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcb88()
{
   return (neuron0x8ff6438() * -0.302497);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcbb0()
{
   return (neuron0x8ff6638() * -0.641808);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffccb0()
{
   return (neuron0x8ff6960() * -0.214184);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffccd8()
{
   return (neuron0x8ff6c30() * 0.146148);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcd00()
{
   return (neuron0x8ff6f48() * -1.22486);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcd28()
{
   return (neuron0x8ff7260() * 0.0329075);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcd50()
{
   return (neuron0x8ff7530() * 0.386833);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcd78()
{
   return (neuron0x8ff7900() * -0.420373);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcda0()
{
   return (neuron0x8ff7c18() * -0.210178);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcdc8()
{
   return (neuron0x8ff7f48() * 0.111957);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcdf0()
{
   return (neuron0x8ff8278() * 0.298295);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffce18()
{
   return (neuron0x8ff85a8() * 0.468894);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffce40()
{
   return (neuron0x8ff8a18() * -1.00447);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffce68()
{
   return (neuron0x8ff8d48() * -0.190438);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffce90()
{
   return (neuron0x8ff9078() * 0.28407);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffceb8()
{
   return (neuron0x8ff93a8() * -0.25236);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcee0()
{
   return (neuron0x8ff96d8() * -0.188957);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcf08()
{
   return (neuron0x8ff9a08() * 0.599568);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcfb8()
{
   return (neuron0x8ffa0f0() * 3.05899);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffcfe0()
{
   return (neuron0x8ffa348() * 0.493945);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffd008()
{
   return (neuron0x8ffa5a0() * -0.222564);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffd030()
{
   return (neuron0x8ff8810() * -0.203069);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffd058()
{
   return (neuron0x8ffac40() * 0.474269);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffd080()
{
   return (neuron0x8ffaf70() * 0.341495);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffd0a8()
{
   return (neuron0x8ffb2a0() * -0.472312);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffd0d0()
{
   return (neuron0x8ffb5d0() * 0.428969);
}

double NNPerpElectronCorrectionPhi::synapse0x8ffd0f8()
{
   return (neuron0x8ffc8c0() * -0.583872);
}

