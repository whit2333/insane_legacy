#include "NNPerpGammaXYCorrectionClusterDeltaX.h"
#include <cmath>

double NNPerpGammaXYCorrectionClusterDeltaX::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8) {
   input0 = (in0 - 2780.62)/1257.71;
   input1 = (in1 - -2.49912)/34.2565;
   input2 = (in2 - 1.20557)/59.5833;
   input3 = (in3 - 1.33643)/0.369571;
   input4 = (in4 - 1.45404)/0.378068;
   input5 = (in5 - 0.129143)/1.85876;
   input6 = (in6 - 0.234253)/1.48492;
   input7 = (in7 - 6.51988)/11.1759;
   input8 = (in8 - 4.52234)/8.10026;
   switch(index) {
     case 0:
         return neuron0x8cea818();
     default:
         return 0.;
   }
}

double NNPerpGammaXYCorrectionClusterDeltaX::Value(int index, double* input) {
   input0 = (input[0] - 2780.62)/1257.71;
   input1 = (input[1] - -2.49912)/34.2565;
   input2 = (input[2] - 1.20557)/59.5833;
   input3 = (input[3] - 1.33643)/0.369571;
   input4 = (input[4] - 1.45404)/0.378068;
   input5 = (input[5] - 0.129143)/1.85876;
   input6 = (input[6] - 0.234253)/1.48492;
   input7 = (input[7] - 6.51988)/11.1759;
   input8 = (input[8] - 4.52234)/8.10026;
   switch(index) {
     case 0:
         return neuron0x8cea818();
     default:
         return 0.;
   }
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cb12e8() {
   return input0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8c65818() {
   return input1;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8c65a18() {
   return input2;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8c65c18() {
   return input3;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8c65e30() {
   return input4;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd12c8() {
   return input5;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd14e0() {
   return input6;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd16f8() {
   return input7;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd1910() {
   return input8;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd1c68() {
   double input = 0.424336;
   input += synapse0x8bc6c58();
   input += synapse0x8c65fb8();
   input += synapse0x8cd1e20();
   input += synapse0x8cd1e48();
   input += synapse0x8cd1e70();
   input += synapse0x8cd1e98();
   input += synapse0x8cd1ec0();
   input += synapse0x8cd1ee8();
   input += synapse0x8cd1f10();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd1c68() {
   double input = input0x8cd1c68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd1f38() {
   double input = 0.703442;
   input += synapse0x8cd2138();
   input += synapse0x8cd2160();
   input += synapse0x8cd2188();
   input += synapse0x8cd21b0();
   input += synapse0x8cd21d8();
   input += synapse0x8cd2200();
   input += synapse0x8cd2228();
   input += synapse0x8cd2250();
   input += synapse0x8cd2300();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd1f38() {
   double input = input0x8cd1f38();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd2328() {
   double input = -0.908282;
   input += synapse0x8cd24e0();
   input += synapse0x8cd2508();
   input += synapse0x8cd2530();
   input += synapse0x8cd2558();
   input += synapse0x8cd2580();
   input += synapse0x8cd25a8();
   input += synapse0x8cd25d0();
   input += synapse0x8cd25f8();
   input += synapse0x8cd2620();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd2328() {
   double input = input0x8cd2328();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd2648() {
   double input = 1.39804;
   input += synapse0x8cd2848();
   input += synapse0x8cd2870();
   input += synapse0x8cd2898();
   input += synapse0x8cd28c0();
   input += synapse0x8cd28e8();
   input += synapse0x8cd2910();
   input += synapse0x8bc8468();
   input += synapse0x8cd2278();
   input += synapse0x8cd22a0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd2648() {
   double input = input0x8cd2648();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd2a40() {
   double input = -0.451348;
   input += synapse0x8cd2c40();
   input += synapse0x8cd2c68();
   input += synapse0x8cd2c90();
   input += synapse0x8cd2cb8();
   input += synapse0x8cd2ce0();
   input += synapse0x8cd2d08();
   input += synapse0x8cd2d30();
   input += synapse0x8cd2d58();
   input += synapse0x8cd2d80();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd2a40() {
   double input = input0x8cd2a40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd2da8() {
   double input = -0.280352;
   input += synapse0x8cd2fa8();
   input += synapse0x8cd2fd0();
   input += synapse0x8cd2ff8();
   input += synapse0x8cd3020();
   input += synapse0x8cd3048();
   input += synapse0x8cd3070();
   input += synapse0x8cd3098();
   input += synapse0x8cd30c0();
   input += synapse0x8cd30e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd2da8() {
   double input = input0x8cd2da8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd3110() {
   double input = -0.0981269;
   input += synapse0x8cd3310();
   input += synapse0x8cd3338();
   input += synapse0x8cd3360();
   input += synapse0x8cd3388();
   input += synapse0x8cd33b0();
   input += synapse0x8cd33d8();
   input += synapse0x8cd3400();
   input += synapse0x8cd3428();
   input += synapse0x8cd3450();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd3110() {
   double input = input0x8cd3110();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd3478() {
   double input = -0.369843;
   input += synapse0x8cd22c8();
   input += synapse0x8bfbd00();
   input += synapse0x8bfbd28();
   input += synapse0x8a11948();
   input += synapse0x8a11970();
   input += synapse0x8b0ee30();
   input += synapse0x8b0ee58();
   input += synapse0x8cb1500();
   input += synapse0x8cb1528();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd3478() {
   double input = input0x8cd3478();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd3908() {
   double input = -1.08344;
   input += synapse0x8cd3b08();
   input += synapse0x8cd3b30();
   input += synapse0x8cd3b58();
   input += synapse0x8cd3b80();
   input += synapse0x8cd3ba8();
   input += synapse0x8cd3bd0();
   input += synapse0x8cd3bf8();
   input += synapse0x8cd3c20();
   input += synapse0x8cd3c48();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd3908() {
   double input = input0x8cd3908();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd3c70() {
   double input = -0.133348;
   input += synapse0x8cd3e88();
   input += synapse0x8cd3eb0();
   input += synapse0x8cd3ed8();
   input += synapse0x8cd3f00();
   input += synapse0x8cd3f28();
   input += synapse0x8cd3f50();
   input += synapse0x8cd3f78();
   input += synapse0x8cd3fa0();
   input += synapse0x8cd3fc8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd3c70() {
   double input = input0x8cd3c70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd3ff0() {
   double input = -0.29946;
   input += synapse0x8cd4208();
   input += synapse0x8cd4230();
   input += synapse0x8cd4258();
   input += synapse0x8cd4280();
   input += synapse0x8cd42a8();
   input += synapse0x8cd42d0();
   input += synapse0x8cd42f8();
   input += synapse0x8cd4320();
   input += synapse0x8cd4348();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd3ff0() {
   double input = input0x8cd3ff0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd4370() {
   double input = -0.455688;
   input += synapse0x8cd4588();
   input += synapse0x8cd45b0();
   input += synapse0x8cd45d8();
   input += synapse0x8cd4600();
   input += synapse0x8cd4628();
   input += synapse0x8cd4650();
   input += synapse0x8cd4678();
   input += synapse0x8cd46a0();
   input += synapse0x8cd46c8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd4370() {
   double input = input0x8cd4370();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd46f0() {
   double input = -0.653143;
   input += synapse0x8cd4908();
   input += synapse0x8cd4930();
   input += synapse0x8cd4958();
   input += synapse0x8cd4980();
   input += synapse0x8cd49a8();
   input += synapse0x8cd49d0();
   input += synapse0x8cd49f8();
   input += synapse0x8cd4a20();
   input += synapse0x8cd4a48();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd46f0() {
   double input = input0x8cd46f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd4a70() {
   double input = 0.50282;
   input += synapse0x8cd4c88();
   input += synapse0x8cd4cb0();
   input += synapse0x8cd4cd8();
   input += synapse0x8cd4d00();
   input += synapse0x8cd4d28();
   input += synapse0x8cd4d50();
   input += synapse0x8cd4d78();
   input += synapse0x8cd4da0();
   input += synapse0x8cd4dc8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd4a70() {
   double input = input0x8cd4a70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd4df0() {
   double input = -0.12939;
   input += synapse0x8cd5008();
   input += synapse0x8cd5030();
   input += synapse0x8cd5058();
   input += synapse0x8cb1550();
   input += synapse0x8cb1578();
   input += synapse0x8cd2938();
   input += synapse0x8cd2960();
   input += synapse0x8cd2988();
   input += synapse0x8cd29b0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd4df0() {
   double input = input0x8cd4df0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd3700() {
   double input = 0.879624;
   input += synapse0x8cd38d0();
   input += synapse0x8cd5510();
   input += synapse0x8cd55c0();
   input += synapse0x8cd5670();
   input += synapse0x8cd5720();
   input += synapse0x8cd57d0();
   input += synapse0x8cd5880();
   input += synapse0x8cd5930();
   input += synapse0x8cd59e0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd3700() {
   double input = input0x8cd3700();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd5a90() {
   double input = -0.988048;
   input += synapse0x8cd5bd0();
   input += synapse0x8cd5bf8();
   input += synapse0x8cd5c20();
   input += synapse0x8cd5c48();
   input += synapse0x8cd5c70();
   input += synapse0x8cd5c98();
   input += synapse0x8cd5cc0();
   input += synapse0x8cd5ce8();
   input += synapse0x8cd5d10();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd5a90() {
   double input = input0x8cd5a90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd5d38() {
   double input = -0.0993979;
   input += synapse0x8cd5e78();
   input += synapse0x8cd5ea0();
   input += synapse0x8cd5ec8();
   input += synapse0x8cd5ef0();
   input += synapse0x8cd5f18();
   input += synapse0x8cd5f40();
   input += synapse0x8cd5f68();
   input += synapse0x8cd5f90();
   input += synapse0x8cd5fb8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd5d38() {
   double input = input0x8cd5d38();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd5fe0() {
   double input = 0.448502;
   input += synapse0x8cd6120();
   input += synapse0x8cd6148();
   input += synapse0x8cd6170();
   input += synapse0x8cd6198();
   input += synapse0x8cd61c0();
   input += synapse0x8cd61e8();
   input += synapse0x8cd6210();
   input += synapse0x8cd6238();
   input += synapse0x8cd6260();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd5fe0() {
   double input = input0x8cd5fe0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd6288() {
   double input = 0.561651;
   input += synapse0x8cd64a0();
   input += synapse0x8cd64c8();
   input += synapse0x8cd64f0();
   input += synapse0x8cd6518();
   input += synapse0x8cd6540();
   input += synapse0x8cd6568();
   input += synapse0x8cd6590();
   input += synapse0x8cd65b8();
   input += synapse0x8cd65e0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd6288() {
   double input = input0x8cd6288();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd6608() {
   double input = 0.891427;
   input += synapse0x8cd6820();
   input += synapse0x8cd6848();
   input += synapse0x8cd6870();
   input += synapse0x8cd6898();
   input += synapse0x8cd68c0();
   input += synapse0x8cd68e8();
   input += synapse0x8cd6910();
   input += synapse0x8cd6938();
   input += synapse0x8cd6960();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd6608() {
   double input = input0x8cd6608();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd6988() {
   double input = 0.280187;
   input += synapse0x8cd6ba0();
   input += synapse0x8cd6bc8();
   input += synapse0x8cd6bf0();
   input += synapse0x8cd6c18();
   input += synapse0x8cd6c40();
   input += synapse0x8cd6c68();
   input += synapse0x8cd6c90();
   input += synapse0x8cd6cb8();
   input += synapse0x8cd6ce0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd6988() {
   double input = input0x8cd6988();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd6d08() {
   double input = 0.245971;
   input += synapse0x8cd6f20();
   input += synapse0x8cd6f48();
   input += synapse0x8cd6f70();
   input += synapse0x8cd6f98();
   input += synapse0x8cd6fc0();
   input += synapse0x8cd6fe8();
   input += synapse0x8cd7010();
   input += synapse0x8cd7038();
   input += synapse0x8cd7060();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd6d08() {
   double input = input0x8cd6d08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd8040() {
   double input = -0.420277;
   input += synapse0x8cd3678();
   input += synapse0x8cd36a0();
   input += synapse0x8cd36c8();
   input += synapse0x8cd8348();
   input += synapse0x8cd8370();
   input += synapse0x8cd8398();
   input += synapse0x8cd83c0();
   input += synapse0x8cd83e8();
   input += synapse0x8cd8410();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd8040() {
   double input = input0x8cd8040();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd8438() {
   double input = -0.195235;
   input += synapse0x8cd8638();
   input += synapse0x8cd8660();
   input += synapse0x8cd8688();
   input += synapse0x8cd86b0();
   input += synapse0x8cd86d8();
   input += synapse0x8cd8700();
   input += synapse0x8cd8728();
   input += synapse0x8cd8750();
   input += synapse0x8cd8778();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd8438() {
   double input = input0x8cd8438();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd87a0() {
   double input = -0.935265;
   input += synapse0x8cd89a0();
   input += synapse0x8cd89c8();
   input += synapse0x8cd89f0();
   input += synapse0x8cd8a18();
   input += synapse0x8cd8a40();
   input += synapse0x8cd8a68();
   input += synapse0x8cd8a90();
   input += synapse0x8cd8ab8();
   input += synapse0x8cd8ae0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd87a0() {
   double input = input0x8cd87a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd8b08() {
   double input = 0.270996;
   input += synapse0x8cd8d08();
   input += synapse0x8cd8d30();
   input += synapse0x8cd8d58();
   input += synapse0x8cd8d80();
   input += synapse0x8cd8da8();
   input += synapse0x8cd8dd0();
   input += synapse0x8cd8df8();
   input += synapse0x8cd8e20();
   input += synapse0x8cd8e48();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd8b08() {
   double input = input0x8cd8b08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd8e70() {
   double input = 0.10804;
   input += synapse0x8cd9070();
   input += synapse0x8cd9098();
   input += synapse0x8cd90c0();
   input += synapse0x8cd90e8();
   input += synapse0x8cd9110();
   input += synapse0x8cd9138();
   input += synapse0x8cd9160();
   input += synapse0x8cd9188();
   input += synapse0x8cd91b0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd8e70() {
   double input = input0x8cd8e70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd91d8() {
   double input = 0.015722;
   input += synapse0x8cd93d8();
   input += synapse0x8cd9400();
   input += synapse0x8cd9428();
   input += synapse0x8cd9450();
   input += synapse0x8cd9478();
   input += synapse0x8cd5080();
   input += synapse0x8cd50a8();
   input += synapse0x8cd50d0();
   input += synapse0x8cd50f8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd91d8() {
   double input = input0x8cd91d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd5120() {
   double input = -0.131261;
   input += synapse0x8cd5338();
   input += synapse0x8cd5360();
   input += synapse0x8cd5388();
   input += synapse0x8cd53b0();
   input += synapse0x8cd53d8();
   input += synapse0x8cd5400();
   input += synapse0x8cd5428();
   input += synapse0x8cd5450();
   input += synapse0x8cd9ca8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd5120() {
   double input = input0x8cd5120();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd9cd0() {
   double input = -0.474431;
   input += synapse0x8cd9ed0();
   input += synapse0x8cd9ef8();
   input += synapse0x8cd9f20();
   input += synapse0x8cd9f48();
   input += synapse0x8cd9f70();
   input += synapse0x8cd9f98();
   input += synapse0x8cd9fc0();
   input += synapse0x8cd9fe8();
   input += synapse0x8cda010();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd9cd0() {
   double input = input0x8cd9cd0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cda038() {
   double input = 0.222357;
   input += synapse0x8cda238();
   input += synapse0x8cd5478();
   input += synapse0x8cd54a0();
   input += synapse0x8cd54c8();
   input += synapse0x8cd5538();
   input += synapse0x8cd5560();
   input += synapse0x8cd5588();
   input += synapse0x8cd55e8();
   input += synapse0x8cd5610();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cda038() {
   double input = input0x8cda038();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdaba8() {
   double input = 1.02488;
   input += synapse0x8cd5840();
   input += synapse0x8cd5638();
   input += synapse0x8cd56e0();
   input += synapse0x8cd5790();
   input += synapse0x8cd58a8();
   input += synapse0x8cd58d0();
   input += synapse0x8cd58f8();
   input += synapse0x8cd5958();
   input += synapse0x8cd5980();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdaba8() {
   double input = input0x8cdaba8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdacd0() {
   double input = 0.202787;
   input += synapse0x8cd59a8();
   input += synapse0x8cd5a50();
   input += synapse0x8cdae88();
   input += synapse0x8cdaeb0();
   input += synapse0x8cdaed8();
   input += synapse0x8cdaf00();
   input += synapse0x8cdaf28();
   input += synapse0x8cdaf50();
   input += synapse0x8cdaf78();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdacd0() {
   double input = input0x8cdacd0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdafa0() {
   double input = 0.0607967;
   input += synapse0x8cdb1a0();
   input += synapse0x8cdb1c8();
   input += synapse0x8cdb1f0();
   input += synapse0x8cdb218();
   input += synapse0x8cdb240();
   input += synapse0x8cdb268();
   input += synapse0x8cdb290();
   input += synapse0x8cdb2b8();
   input += synapse0x8cdb2e0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdafa0() {
   double input = input0x8cdafa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdb308() {
   double input = 0.310773;
   input += synapse0x8cdb508();
   input += synapse0x8cdb530();
   input += synapse0x8cdb558();
   input += synapse0x8cdb580();
   input += synapse0x8cdb5a8();
   input += synapse0x8cdb5d0();
   input += synapse0x8cdb5f8();
   input += synapse0x8cdb620();
   input += synapse0x8cdb648();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdb308() {
   double input = input0x8cdb308();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdb670() {
   double input = -0.729718;
   input += synapse0x8cdb870();
   input += synapse0x8cdb898();
   input += synapse0x8cdb8c0();
   input += synapse0x8cdb8e8();
   input += synapse0x8cdb910();
   input += synapse0x8cdb938();
   input += synapse0x8cdb960();
   input += synapse0x8cdb988();
   input += synapse0x8cdb9b0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdb670() {
   double input = input0x8cdb670();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdb9d8() {
   double input = 0.528856;
   input += synapse0x8cdbbd8();
   input += synapse0x8cdbc00();
   input += synapse0x8cdbc28();
   input += synapse0x8cdbc50();
   input += synapse0x8cdbc78();
   input += synapse0x8cdbca0();
   input += synapse0x8cdbcc8();
   input += synapse0x8cdbcf0();
   input += synapse0x8cdbd18();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdb9d8() {
   double input = input0x8cdb9d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdbd40() {
   double input = 0.721087;
   input += synapse0x8cdbf40();
   input += synapse0x8cdbf68();
   input += synapse0x8cdbf90();
   input += synapse0x8cdbfb8();
   input += synapse0x8cdbfe0();
   input += synapse0x8cdc008();
   input += synapse0x8cdc030();
   input += synapse0x8cdc058();
   input += synapse0x8cdc080();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdbd40() {
   double input = input0x8cdbd40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdc0a8() {
   double input = 0.387563;
   input += synapse0x8cdc2a8();
   input += synapse0x8cdc2d0();
   input += synapse0x8cdc2f8();
   input += synapse0x8cdc320();
   input += synapse0x8cdc348();
   input += synapse0x8cdc370();
   input += synapse0x8cdc398();
   input += synapse0x8cdc3c0();
   input += synapse0x8cdc3e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdc0a8() {
   double input = input0x8cdc0a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdc410() {
   double input = 1.64058;
   input += synapse0x8cdc628();
   input += synapse0x8cdc650();
   input += synapse0x8cdc678();
   input += synapse0x8cdc6a0();
   input += synapse0x8cdc6c8();
   input += synapse0x8cdc6f0();
   input += synapse0x8cdc718();
   input += synapse0x8cdc740();
   input += synapse0x8cdc768();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdc410() {
   double input = input0x8cdc410();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdc790() {
   double input = -0.369738;
   input += synapse0x8cdc9a8();
   input += synapse0x8cdc9d0();
   input += synapse0x8cdc9f8();
   input += synapse0x8cdca20();
   input += synapse0x8cdca48();
   input += synapse0x8cdca70();
   input += synapse0x8cdca98();
   input += synapse0x8cdcac0();
   input += synapse0x8cdcae8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdc790() {
   double input = input0x8cdc790();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdcb10() {
   double input = 0.620173;
   input += synapse0x8cdcd28();
   input += synapse0x8cdcd50();
   input += synapse0x8cdcd78();
   input += synapse0x8cdcda0();
   input += synapse0x8cdcdc8();
   input += synapse0x8cdcdf0();
   input += synapse0x8cdce18();
   input += synapse0x8cdce40();
   input += synapse0x8cdce68();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdcb10() {
   double input = input0x8cdcb10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdce90() {
   double input = -0.170758;
   input += synapse0x8cdd0a8();
   input += synapse0x8cdd0d0();
   input += synapse0x8cdd0f8();
   input += synapse0x8cdd120();
   input += synapse0x8cdd148();
   input += synapse0x8cdd170();
   input += synapse0x8cdd198();
   input += synapse0x8cdd1c0();
   input += synapse0x8cdd1e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdce90() {
   double input = input0x8cdce90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdd210() {
   double input = -1.05574;
   input += synapse0x8cdd428();
   input += synapse0x8cdd450();
   input += synapse0x8cdd478();
   input += synapse0x8cdd4a0();
   input += synapse0x8cdd4c8();
   input += synapse0x8cdd4f0();
   input += synapse0x8cdd518();
   input += synapse0x8cdd540();
   input += synapse0x8cdd568();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdd210() {
   double input = input0x8cdd210();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdd590() {
   double input = -0.0851962;
   input += synapse0x8cdd7a8();
   input += synapse0x8cdd7d0();
   input += synapse0x8cdd7f8();
   input += synapse0x8cdd820();
   input += synapse0x8cdd848();
   input += synapse0x8cdd870();
   input += synapse0x8cdd898();
   input += synapse0x8cdd8c0();
   input += synapse0x8cdd8e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdd590() {
   double input = input0x8cdd590();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdd910() {
   double input = -0.665256;
   input += synapse0x8cddb28();
   input += synapse0x8cddb50();
   input += synapse0x8cddb78();
   input += synapse0x8cddba0();
   input += synapse0x8cddbc8();
   input += synapse0x8cddbf0();
   input += synapse0x8cddc18();
   input += synapse0x8cddc40();
   input += synapse0x8cddc68();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdd910() {
   double input = input0x8cdd910();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cddc90() {
   double input = -0.106142;
   input += synapse0x8cddea8();
   input += synapse0x8cdded0();
   input += synapse0x8cddef8();
   input += synapse0x8cddf20();
   input += synapse0x8cddf48();
   input += synapse0x8cddf70();
   input += synapse0x8cddf98();
   input += synapse0x8cddfc0();
   input += synapse0x8cddfe8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cddc90() {
   double input = input0x8cddc90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cde010() {
   double input = -1.1484;
   input += synapse0x8cde228();
   input += synapse0x8cde250();
   input += synapse0x8cde278();
   input += synapse0x8cde2a0();
   input += synapse0x8cde2c8();
   input += synapse0x8cde2f0();
   input += synapse0x8cde318();
   input += synapse0x8cde340();
   input += synapse0x8cde368();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cde010() {
   double input = input0x8cde010();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cde390() {
   double input = 0.145298;
   input += synapse0x8cde5a8();
   input += synapse0x8cde5d0();
   input += synapse0x8cde5f8();
   input += synapse0x8cde620();
   input += synapse0x8cde648();
   input += synapse0x8cde670();
   input += synapse0x8cde698();
   input += synapse0x8cde6c0();
   input += synapse0x8cde6e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cde390() {
   double input = input0x8cde390();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cde710() {
   double input = -0.0222183;
   input += synapse0x8cde928();
   input += synapse0x8cde950();
   input += synapse0x8cde978();
   input += synapse0x8cde9a0();
   input += synapse0x8cde9c8();
   input += synapse0x8cde9f0();
   input += synapse0x8cdea18();
   input += synapse0x8cdea40();
   input += synapse0x8cdea68();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cde710() {
   double input = input0x8cde710();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdea90() {
   double input = -0.124366;
   input += synapse0x8cdeca8();
   input += synapse0x8cdecd0();
   input += synapse0x8cdecf8();
   input += synapse0x8cded20();
   input += synapse0x8cded48();
   input += synapse0x8cded70();
   input += synapse0x8cded98();
   input += synapse0x8cdedc0();
   input += synapse0x8cdede8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdea90() {
   double input = input0x8cdea90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdee10() {
   double input = -0.230311;
   input += synapse0x8cdf028();
   input += synapse0x8cdf050();
   input += synapse0x8cdf078();
   input += synapse0x8cdf0a0();
   input += synapse0x8cdf0c8();
   input += synapse0x8cdf0f0();
   input += synapse0x8cdf118();
   input += synapse0x8cdf140();
   input += synapse0x8cdf168();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdee10() {
   double input = input0x8cdee10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdf190() {
   double input = 1.02654;
   input += synapse0x8cdf3a8();
   input += synapse0x8cdf3d0();
   input += synapse0x8cdf3f8();
   input += synapse0x8cdf420();
   input += synapse0x8cdf448();
   input += synapse0x8cdf470();
   input += synapse0x8cdf498();
   input += synapse0x8cdf4c0();
   input += synapse0x8cdf4e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdf190() {
   double input = input0x8cdf190();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdf510() {
   double input = -0.163969;
   input += synapse0x8cdf728();
   input += synapse0x8cdf750();
   input += synapse0x8cdf778();
   input += synapse0x8cdf7a0();
   input += synapse0x8cdf7c8();
   input += synapse0x8cdf7f0();
   input += synapse0x8cdf818();
   input += synapse0x8cdf840();
   input += synapse0x8cdf868();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdf510() {
   double input = input0x8cdf510();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdf890() {
   double input = -0.112683;
   input += synapse0x8cd8240();
   input += synapse0x8cd8268();
   input += synapse0x8cd8290();
   input += synapse0x8cd82b8();
   input += synapse0x8cd82e0();
   input += synapse0x8cd8308();
   input += synapse0x8cdfcb0();
   input += synapse0x8cdfcd8();
   input += synapse0x8cdfd00();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdf890() {
   double input = input0x8cdf890();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cdfd28() {
   double input = 1.07538;
   input += synapse0x8cdff28();
   input += synapse0x8cdff50();
   input += synapse0x8cdff78();
   input += synapse0x8cdffa0();
   input += synapse0x8cdffc8();
   input += synapse0x8cdfff0();
   input += synapse0x8ce0018();
   input += synapse0x8ce0040();
   input += synapse0x8ce0068();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cdfd28() {
   double input = input0x8cdfd28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd94a0() {
   double input = 1.21563;
   input += synapse0x8cd96b8();
   input += synapse0x8cd96e0();
   input += synapse0x8cd9708();
   input += synapse0x8cd9730();
   input += synapse0x8cd9758();
   input += synapse0x8cd9780();
   input += synapse0x8cd97a8();
   input += synapse0x8cd97d0();
   input += synapse0x8cd97f8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd94a0() {
   double input = input0x8cd94a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cd9820() {
   double input = -1.2761;
   input += synapse0x8cd9a38();
   input += synapse0x8cd9a60();
   input += synapse0x8cd9a88();
   input += synapse0x8cd9ab0();
   input += synapse0x8cd9ad8();
   input += synapse0x8cd9b00();
   input += synapse0x8cd9b28();
   input += synapse0x8cd9b50();
   input += synapse0x8cd9b78();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cd9820() {
   double input = input0x8cd9820();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce1098() {
   double input = 0.833127;
   input += synapse0x8ce11c0();
   input += synapse0x8ce11e8();
   input += synapse0x8ce1210();
   input += synapse0x8ce1238();
   input += synapse0x8ce1260();
   input += synapse0x8ce1288();
   input += synapse0x8ce12b0();
   input += synapse0x8ce12d8();
   input += synapse0x8ce1300();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce1098() {
   double input = input0x8ce1098();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce1328() {
   double input = 0.491319;
   input += synapse0x8ce1528();
   input += synapse0x8ce1550();
   input += synapse0x8ce1578();
   input += synapse0x8ce15a0();
   input += synapse0x8ce15c8();
   input += synapse0x8ce15f0();
   input += synapse0x8ce1618();
   input += synapse0x8ce1640();
   input += synapse0x8ce1668();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce1328() {
   double input = input0x8ce1328();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce1690() {
   double input = -1.73119;
   input += synapse0x8ce18a8();
   input += synapse0x8ce18d0();
   input += synapse0x8ce18f8();
   input += synapse0x8ce1920();
   input += synapse0x8ce1948();
   input += synapse0x8ce1970();
   input += synapse0x8ce1998();
   input += synapse0x8ce19c0();
   input += synapse0x8ce19e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce1690() {
   double input = input0x8ce1690();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce1a10() {
   double input = -0.450449;
   input += synapse0x8ce1c28();
   input += synapse0x8ce1c50();
   input += synapse0x8ce1c78();
   input += synapse0x8ce1ca0();
   input += synapse0x8ce1cc8();
   input += synapse0x8ce1cf0();
   input += synapse0x8ce1d18();
   input += synapse0x8ce1d40();
   input += synapse0x8ce1d68();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce1a10() {
   double input = input0x8ce1a10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce1d90() {
   double input = 0.596763;
   input += synapse0x8ce1fa8();
   input += synapse0x8cda260();
   input += synapse0x8cda288();
   input += synapse0x8cda2b0();
   input += synapse0x8cda4e0();
   input += synapse0x8cda508();
   input += synapse0x8cda738();
   input += synapse0x8cda760();
   input += synapse0x8cda998();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce1d90() {
   double input = input0x8ce1d90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cda9c0() {
   double input = 0.299825;
   input += synapse0x8ce2c48();
   input += synapse0x8ce2c70();
   input += synapse0x8ce2c98();
   input += synapse0x8ce2cc0();
   input += synapse0x8ce2ce8();
   input += synapse0x8ce2d10();
   input += synapse0x8ce2d38();
   input += synapse0x8ce2d60();
   input += synapse0x8ce2d88();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cda9c0() {
   double input = input0x8cda9c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce2db0() {
   double input = -0.364146;
   input += synapse0x8ce2fb0();
   input += synapse0x8ce2fd8();
   input += synapse0x8ce3000();
   input += synapse0x8ce3028();
   input += synapse0x8ce3050();
   input += synapse0x8ce3078();
   input += synapse0x8ce30a0();
   input += synapse0x8ce30c8();
   input += synapse0x8ce30f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce2db0() {
   double input = input0x8ce2db0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce3118() {
   double input = 0.0851155;
   input += synapse0x8ce3330();
   input += synapse0x8ce3358();
   input += synapse0x8ce3380();
   input += synapse0x8ce33a8();
   input += synapse0x8ce33d0();
   input += synapse0x8ce33f8();
   input += synapse0x8ce3420();
   input += synapse0x8ce3448();
   input += synapse0x8ce3470();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce3118() {
   double input = input0x8ce3118();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce3498() {
   double input = -0.204331;
   input += synapse0x8ce36b0();
   input += synapse0x8ce36d8();
   input += synapse0x8ce3700();
   input += synapse0x8ce3728();
   input += synapse0x8ce3750();
   input += synapse0x8ce3778();
   input += synapse0x8ce37a0();
   input += synapse0x8ce37c8();
   input += synapse0x8ce37f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce3498() {
   double input = input0x8ce3498();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce3818() {
   double input = 0.156337;
   input += synapse0x8ce3a30();
   input += synapse0x8ce3a58();
   input += synapse0x8ce3a80();
   input += synapse0x8ce3aa8();
   input += synapse0x8ce3ad0();
   input += synapse0x8ce3af8();
   input += synapse0x8ce3b20();
   input += synapse0x8ce3b48();
   input += synapse0x8ce3b70();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce3818() {
   double input = input0x8ce3818();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce3b98() {
   double input = -0.113351;
   input += synapse0x8ce3db0();
   input += synapse0x8ce3dd8();
   input += synapse0x8ce3e00();
   input += synapse0x8ce3e28();
   input += synapse0x8ce3e50();
   input += synapse0x8ce3e78();
   input += synapse0x8ce3ea0();
   input += synapse0x8ce3ec8();
   input += synapse0x8ce3ef0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce3b98() {
   double input = input0x8ce3b98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce3f18() {
   double input = 0.612317;
   input += synapse0x8ce4130();
   input += synapse0x8ce4158();
   input += synapse0x8ce4180();
   input += synapse0x8ce41a8();
   input += synapse0x8ce41d0();
   input += synapse0x8ce41f8();
   input += synapse0x8ce4220();
   input += synapse0x8ce4248();
   input += synapse0x8ce4270();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce3f18() {
   double input = input0x8ce3f18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce4298() {
   double input = -0.224659;
   input += synapse0x8ce44b0();
   input += synapse0x8ce44d8();
   input += synapse0x8ce4500();
   input += synapse0x8ce4528();
   input += synapse0x8ce4550();
   input += synapse0x8ce4578();
   input += synapse0x8ce45a0();
   input += synapse0x8ce45c8();
   input += synapse0x8ce45f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce4298() {
   double input = input0x8ce4298();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce4618() {
   double input = 0.0569427;
   input += synapse0x8ce4830();
   input += synapse0x8ce4858();
   input += synapse0x8ce4880();
   input += synapse0x8ce48a8();
   input += synapse0x8ce48d0();
   input += synapse0x8ce48f8();
   input += synapse0x8ce4920();
   input += synapse0x8ce4948();
   input += synapse0x8ce4970();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce4618() {
   double input = input0x8ce4618();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce4998() {
   double input = 0.0616872;
   input += synapse0x8ce4bb0();
   input += synapse0x8ce4bd8();
   input += synapse0x8ce4c00();
   input += synapse0x8ce4c28();
   input += synapse0x8ce4c50();
   input += synapse0x8ce4c78();
   input += synapse0x8ce4ca0();
   input += synapse0x8ce4cc8();
   input += synapse0x8ce4cf0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce4998() {
   double input = input0x8ce4998();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce4d18() {
   double input = -0.212849;
   input += synapse0x8ce4f30();
   input += synapse0x8ce4f58();
   input += synapse0x8ce4f80();
   input += synapse0x8ce4fa8();
   input += synapse0x8ce4fd0();
   input += synapse0x8ce4ff8();
   input += synapse0x8ce5020();
   input += synapse0x8ce5048();
   input += synapse0x8ce5070();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce4d18() {
   double input = input0x8ce4d18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce5098() {
   double input = -0.147036;
   input += synapse0x8ce52b0();
   input += synapse0x8ce52d8();
   input += synapse0x8ce5300();
   input += synapse0x8ce5328();
   input += synapse0x8ce5350();
   input += synapse0x8ce5378();
   input += synapse0x8ce53a0();
   input += synapse0x8ce53c8();
   input += synapse0x8ce53f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce5098() {
   double input = input0x8ce5098();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce5418() {
   double input = 0.0415883;
   input += synapse0x8ce5630();
   input += synapse0x8ce5658();
   input += synapse0x8ce5680();
   input += synapse0x8ce56a8();
   input += synapse0x8ce56d0();
   input += synapse0x8ce56f8();
   input += synapse0x8ce5720();
   input += synapse0x8ce5748();
   input += synapse0x8ce5770();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce5418() {
   double input = input0x8ce5418();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce5798() {
   double input = 0.448023;
   input += synapse0x8ce59b0();
   input += synapse0x8ce59d8();
   input += synapse0x8ce5a00();
   input += synapse0x8ce5a28();
   input += synapse0x8ce5a50();
   input += synapse0x8ce5a78();
   input += synapse0x8ce5aa0();
   input += synapse0x8ce5ac8();
   input += synapse0x8ce5af0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce5798() {
   double input = input0x8ce5798();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce5b18() {
   double input = -0.639076;
   input += synapse0x8ce5d30();
   input += synapse0x8ce5d58();
   input += synapse0x8ce5d80();
   input += synapse0x8ce5da8();
   input += synapse0x8ce5dd0();
   input += synapse0x8ce5df8();
   input += synapse0x8ce5e20();
   input += synapse0x8ce5e48();
   input += synapse0x8ce5e70();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce5b18() {
   double input = input0x8ce5b18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce5e98() {
   double input = -0.543347;
   input += synapse0x8ce60b0();
   input += synapse0x8ce60d8();
   input += synapse0x8ce6100();
   input += synapse0x8ce6128();
   input += synapse0x8ce6150();
   input += synapse0x8ce6178();
   input += synapse0x8ce61a0();
   input += synapse0x8ce61c8();
   input += synapse0x8ce61f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce5e98() {
   double input = input0x8ce5e98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce6218() {
   double input = 0.0703095;
   input += synapse0x8ce6430();
   input += synapse0x8ce6458();
   input += synapse0x8ce6480();
   input += synapse0x8ce64a8();
   input += synapse0x8ce64d0();
   input += synapse0x8ce64f8();
   input += synapse0x8ce6520();
   input += synapse0x8ce6548();
   input += synapse0x8ce6570();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce6218() {
   double input = input0x8ce6218();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce6598() {
   double input = -0.0882632;
   input += synapse0x8ce67b0();
   input += synapse0x8ce67d8();
   input += synapse0x8ce6800();
   input += synapse0x8ce6828();
   input += synapse0x8ce6850();
   input += synapse0x8ce6878();
   input += synapse0x8ce68a0();
   input += synapse0x8ce68c8();
   input += synapse0x8ce68f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce6598() {
   double input = input0x8ce6598();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce6918() {
   double input = 0.743231;
   input += synapse0x8ce6b30();
   input += synapse0x8ce6b58();
   input += synapse0x8ce6b80();
   input += synapse0x8ce6ba8();
   input += synapse0x8ce6bd0();
   input += synapse0x8ce6bf8();
   input += synapse0x8ce6c20();
   input += synapse0x8ce6c48();
   input += synapse0x8ce6c70();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce6918() {
   double input = input0x8ce6918();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce6c98() {
   double input = -0.246768;
   input += synapse0x8ce6eb0();
   input += synapse0x8ce6ed8();
   input += synapse0x8ce6f00();
   input += synapse0x8ce6f28();
   input += synapse0x8ce6f50();
   input += synapse0x8ce6f78();
   input += synapse0x8ce6fa0();
   input += synapse0x8ce6fc8();
   input += synapse0x8ce6ff0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce6c98() {
   double input = input0x8ce6c98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce7018() {
   double input = 0.200248;
   input += synapse0x8ce7230();
   input += synapse0x8ce7258();
   input += synapse0x8ce7280();
   input += synapse0x8ce72a8();
   input += synapse0x8ce72d0();
   input += synapse0x8ce72f8();
   input += synapse0x8ce7320();
   input += synapse0x8ce7348();
   input += synapse0x8ce7370();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce7018() {
   double input = input0x8ce7018();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce7398() {
   double input = -0.00693498;
   input += synapse0x8ce75b0();
   input += synapse0x8ce75d8();
   input += synapse0x8ce7600();
   input += synapse0x8ce7628();
   input += synapse0x8ce7650();
   input += synapse0x8ce7678();
   input += synapse0x8ce76a0();
   input += synapse0x8ce76c8();
   input += synapse0x8ce76f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce7398() {
   double input = input0x8ce7398();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce7718() {
   double input = -1.1946;
   input += synapse0x8ce7930();
   input += synapse0x8ce7958();
   input += synapse0x8ce7980();
   input += synapse0x8ce79a8();
   input += synapse0x8ce79d0();
   input += synapse0x8ce79f8();
   input += synapse0x8ce7a20();
   input += synapse0x8ce7a48();
   input += synapse0x8ce7a70();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce7718() {
   double input = input0x8ce7718();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce7a98() {
   double input = 1.13587;
   input += synapse0x8ce7cb0();
   input += synapse0x8ce7cd8();
   input += synapse0x8ce7d00();
   input += synapse0x8ce7d28();
   input += synapse0x8ce7d50();
   input += synapse0x8ce7d78();
   input += synapse0x8ce7da0();
   input += synapse0x8ce7dc8();
   input += synapse0x8ce7df0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce7a98() {
   double input = input0x8ce7a98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce7e18() {
   double input = -0.510603;
   input += synapse0x8ce8030();
   input += synapse0x8ce8058();
   input += synapse0x8ce8080();
   input += synapse0x8ce80a8();
   input += synapse0x8ce80d0();
   input += synapse0x8ce80f8();
   input += synapse0x8ce8120();
   input += synapse0x8ce8148();
   input += synapse0x8ce8170();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce7e18() {
   double input = input0x8ce7e18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce8198() {
   double input = 0.157091;
   input += synapse0x8ce83b0();
   input += synapse0x8ce83d8();
   input += synapse0x8ce8400();
   input += synapse0x8ce8428();
   input += synapse0x8ce8450();
   input += synapse0x8ce8478();
   input += synapse0x8ce84a0();
   input += synapse0x8ce84c8();
   input += synapse0x8ce84f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce8198() {
   double input = input0x8ce8198();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce8518() {
   double input = -1.18577;
   input += synapse0x8ce8730();
   input += synapse0x8ce8758();
   input += synapse0x8ce8780();
   input += synapse0x8ce87a8();
   input += synapse0x8ce87d0();
   input += synapse0x8ce87f8();
   input += synapse0x8ce8820();
   input += synapse0x8ce8848();
   input += synapse0x8ce8870();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce8518() {
   double input = input0x8ce8518();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce8898() {
   double input = 0.291504;
   input += synapse0x8ce8ab0();
   input += synapse0x8ce8ad8();
   input += synapse0x8ce8b00();
   input += synapse0x8ce8b28();
   input += synapse0x8ce8b50();
   input += synapse0x8ce8b78();
   input += synapse0x8ce8ba0();
   input += synapse0x8ce8bc8();
   input += synapse0x8ce8bf0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce8898() {
   double input = input0x8ce8898();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce8c18() {
   double input = -0.0907635;
   input += synapse0x8ce8e30();
   input += synapse0x8ce8e58();
   input += synapse0x8ce8e80();
   input += synapse0x8ce8ea8();
   input += synapse0x8ce8ed0();
   input += synapse0x8ce8ef8();
   input += synapse0x8ce8f20();
   input += synapse0x8ce8f48();
   input += synapse0x8ce8f70();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce8c18() {
   double input = input0x8ce8c18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce8f98() {
   double input = 0.639669;
   input += synapse0x8ce91b0();
   input += synapse0x8ce91d8();
   input += synapse0x8ce9200();
   input += synapse0x8ce9228();
   input += synapse0x8ce9250();
   input += synapse0x8ce9278();
   input += synapse0x8ce92a0();
   input += synapse0x8ce92c8();
   input += synapse0x8ce92f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce8f98() {
   double input = input0x8ce8f98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce9318() {
   double input = 0.811742;
   input += synapse0x8ce9530();
   input += synapse0x8ce9558();
   input += synapse0x8ce9580();
   input += synapse0x8ce95a8();
   input += synapse0x8ce95d0();
   input += synapse0x8ce95f8();
   input += synapse0x8ce9620();
   input += synapse0x8ce9648();
   input += synapse0x8ce9670();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce9318() {
   double input = input0x8ce9318();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce9698() {
   double input = -0.403469;
   input += synapse0x8ce98b0();
   input += synapse0x8ce98d8();
   input += synapse0x8ce9900();
   input += synapse0x8ce9928();
   input += synapse0x8ce9950();
   input += synapse0x8ce9978();
   input += synapse0x8ce99a0();
   input += synapse0x8ce99c8();
   input += synapse0x8ce99f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce9698() {
   double input = input0x8ce9698();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce9a18() {
   double input = 0.0168543;
   input += synapse0x8ce9c30();
   input += synapse0x8ce9c58();
   input += synapse0x8ce9c80();
   input += synapse0x8ce9ca8();
   input += synapse0x8ce9cd0();
   input += synapse0x8ce9cf8();
   input += synapse0x8ce9d20();
   input += synapse0x8ce9d48();
   input += synapse0x8ce9d70();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce9a18() {
   double input = input0x8ce9a18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8ce9d98() {
   double input = 0.135621;
   input += synapse0x8ce9fb0();
   input += synapse0x8ce9fd8();
   input += synapse0x8cea000();
   input += synapse0x8cea028();
   input += synapse0x8cea050();
   input += synapse0x8cea078();
   input += synapse0x8cea0a0();
   input += synapse0x8cea0c8();
   input += synapse0x8cea0f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8ce9d98() {
   double input = input0x8ce9d98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cea118() {
   double input = 0.961169;
   input += synapse0x8cea330();
   input += synapse0x8cea358();
   input += synapse0x8cea380();
   input += synapse0x8cea3a8();
   input += synapse0x8cea3d0();
   input += synapse0x8cea3f8();
   input += synapse0x8cea420();
   input += synapse0x8cea448();
   input += synapse0x8cea470();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cea118() {
   double input = input0x8cea118();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cea498() {
   double input = -0.190874;
   input += synapse0x8cea6b0();
   input += synapse0x8cea6d8();
   input += synapse0x8cea700();
   input += synapse0x8cea728();
   input += synapse0x8cea750();
   input += synapse0x8cea778();
   input += synapse0x8cea7a0();
   input += synapse0x8cea7c8();
   input += synapse0x8cea7f0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cea498() {
   double input = input0x8cea498();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::input0x8cea818() {
   double input = -0.0213505;
   input += synapse0x8cea940();
   input += synapse0x8cea968();
   input += synapse0x8cea990();
   input += synapse0x8cea9b8();
   input += synapse0x8cea9e0();
   input += synapse0x8ceaa08();
   input += synapse0x8ceaa30();
   input += synapse0x8ceaa58();
   input += synapse0x8ceaa80();
   input += synapse0x8ceaaa8();
   input += synapse0x8ceaad0();
   input += synapse0x8ceaaf8();
   input += synapse0x8ceab20();
   input += synapse0x8ceab48();
   input += synapse0x8ceab70();
   input += synapse0x8ceab98();
   input += synapse0x8ceac48();
   input += synapse0x8ceac70();
   input += synapse0x8ceac98();
   input += synapse0x8ceacc0();
   input += synapse0x8ceace8();
   input += synapse0x8cead10();
   input += synapse0x8cead38();
   input += synapse0x8cead60();
   input += synapse0x8cead88();
   input += synapse0x8ceadb0();
   input += synapse0x8ceadd8();
   input += synapse0x8ceae00();
   input += synapse0x8ceae28();
   input += synapse0x8ceae50();
   input += synapse0x8ceae78();
   input += synapse0x8ceaea0();
   input += synapse0x8ceabc0();
   input += synapse0x8ceabe8();
   input += synapse0x8ceac10();
   input += synapse0x8ceafd0();
   input += synapse0x8cd1b80();
   input += synapse0x8ceaff8();
   input += synapse0x8ceb020();
   input += synapse0x8ceb048();
   input += synapse0x8ceb070();
   input += synapse0x8ceb098();
   input += synapse0x8ceb0c0();
   input += synapse0x8ceb0e8();
   input += synapse0x8ceb110();
   input += synapse0x8ceb138();
   input += synapse0x8ceb160();
   input += synapse0x8ceb188();
   input += synapse0x8ceb1b0();
   input += synapse0x8ceb1d8();
   input += synapse0x8ceb200();
   input += synapse0x8ceb228();
   input += synapse0x8ceb250();
   input += synapse0x8ceb278();
   input += synapse0x8ceb2a0();
   input += synapse0x8ceb2c8();
   input += synapse0x8ceb2f0();
   input += synapse0x8ceb318();
   input += synapse0x8ceb340();
   input += synapse0x8ceb368();
   input += synapse0x8ceb390();
   input += synapse0x8ceb3b8();
   input += synapse0x8ceb3e0();
   input += synapse0x8ceb408();
   input += synapse0x8ceaec8();
   input += synapse0x8ceaef0();
   input += synapse0x8ceaf18();
   input += synapse0x8ceaf40();
   input += synapse0x8ceaf68();
   input += synapse0x8ceaf90();
   input += synapse0x8ceb638();
   input += synapse0x8ceb660();
   input += synapse0x8ceb688();
   input += synapse0x8ceb6b0();
   input += synapse0x8ceb6d8();
   input += synapse0x8ceb700();
   input += synapse0x8ceb728();
   input += synapse0x8ceb750();
   input += synapse0x8ceb778();
   input += synapse0x8ceb7a0();
   input += synapse0x8ceb7c8();
   input += synapse0x8ceb7f0();
   input += synapse0x8ceb818();
   input += synapse0x8ceb840();
   input += synapse0x8ceb868();
   input += synapse0x8ceb890();
   input += synapse0x8ceb8b8();
   input += synapse0x8ceb8e0();
   input += synapse0x8ceb908();
   input += synapse0x8ceb930();
   input += synapse0x8ceb958();
   input += synapse0x8ceb980();
   input += synapse0x8ceb9a8();
   input += synapse0x8ceb9d0();
   input += synapse0x8ceb9f8();
   input += synapse0x8ceba20();
   input += synapse0x8ceba48();
   input += synapse0x8ceba70();
   input += synapse0x8ceba98();
   input += synapse0x8cebac0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaX::neuron0x8cea818() {
   double input = input0x8cea818();
   return (input * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8bc6c58() {
   return (neuron0x8cb12e8()*-0.156945);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8c65fb8() {
   return (neuron0x8c65818()*0.0212203);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd1e20() {
   return (neuron0x8c65a18()*-0.122468);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd1e48() {
   return (neuron0x8c65c18()*-0.262394);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd1e70() {
   return (neuron0x8c65e30()*0.583031);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd1e98() {
   return (neuron0x8cd12c8()*0.205798);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd1ec0() {
   return (neuron0x8cd14e0()*0.143659);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd1ee8() {
   return (neuron0x8cd16f8()*0.126079);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd1f10() {
   return (neuron0x8cd1910()*0.145493);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2138() {
   return (neuron0x8cb12e8()*-0.149932);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2160() {
   return (neuron0x8c65818()*-0.858914);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2188() {
   return (neuron0x8c65a18()*0.179108);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd21b0() {
   return (neuron0x8c65c18()*-0.23982);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd21d8() {
   return (neuron0x8c65e30()*0.149716);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2200() {
   return (neuron0x8cd12c8()*0.571218);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2228() {
   return (neuron0x8cd14e0()*0.545066);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2250() {
   return (neuron0x8cd16f8()*-0.655186);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2300() {
   return (neuron0x8cd1910()*-0.637116);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd24e0() {
   return (neuron0x8cb12e8()*0.0210891);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2508() {
   return (neuron0x8c65818()*0.118253);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2530() {
   return (neuron0x8c65a18()*0.139149);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2558() {
   return (neuron0x8c65c18()*-1.52716);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2580() {
   return (neuron0x8c65e30()*0.0315064);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd25a8() {
   return (neuron0x8cd12c8()*-1.48694);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd25d0() {
   return (neuron0x8cd14e0()*-0.048116);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd25f8() {
   return (neuron0x8cd16f8()*-2.10035);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2620() {
   return (neuron0x8cd1910()*0.053835);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2848() {
   return (neuron0x8cb12e8()*0.615135);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2870() {
   return (neuron0x8c65818()*-0.41835);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2898() {
   return (neuron0x8c65a18()*-0.186703);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd28c0() {
   return (neuron0x8c65c18()*-0.560852);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd28e8() {
   return (neuron0x8c65e30()*0.324616);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2910() {
   return (neuron0x8cd12c8()*1.39638);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8bc8468() {
   return (neuron0x8cd14e0()*0.584914);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2278() {
   return (neuron0x8cd16f8()*-0.579777);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd22a0() {
   return (neuron0x8cd1910()*0.583369);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2c40() {
   return (neuron0x8cb12e8()*-0.238971);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2c68() {
   return (neuron0x8c65818()*0.00768301);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2c90() {
   return (neuron0x8c65a18()*0.40134);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2cb8() {
   return (neuron0x8c65c18()*0.360527);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2ce0() {
   return (neuron0x8c65e30()*0.053889);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2d08() {
   return (neuron0x8cd12c8()*0.287327);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2d30() {
   return (neuron0x8cd14e0()*-0.155178);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2d58() {
   return (neuron0x8cd16f8()*0.575073);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2d80() {
   return (neuron0x8cd1910()*0.301241);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2fa8() {
   return (neuron0x8cb12e8()*-0.00772238);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2fd0() {
   return (neuron0x8c65818()*-0.102561);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2ff8() {
   return (neuron0x8c65a18()*0.270595);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3020() {
   return (neuron0x8c65c18()*-0.0449384);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3048() {
   return (neuron0x8c65e30()*0.547441);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3070() {
   return (neuron0x8cd12c8()*0.373989);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3098() {
   return (neuron0x8cd14e0()*-0.308841);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd30c0() {
   return (neuron0x8cd16f8()*-0.313552);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd30e8() {
   return (neuron0x8cd1910()*-0.539755);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3310() {
   return (neuron0x8cb12e8()*0.326431);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3338() {
   return (neuron0x8c65818()*0.196358);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3360() {
   return (neuron0x8c65a18()*-0.0715582);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3388() {
   return (neuron0x8c65c18()*-0.170441);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd33b0() {
   return (neuron0x8c65e30()*-0.107546);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd33d8() {
   return (neuron0x8cd12c8()*-0.266592);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3400() {
   return (neuron0x8cd14e0()*0.132292);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3428() {
   return (neuron0x8cd16f8()*-0.566305);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3450() {
   return (neuron0x8cd1910()*-0.17387);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd22c8() {
   return (neuron0x8cb12e8()*-0.0966286);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8bfbd00() {
   return (neuron0x8c65818()*0.686506);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8bfbd28() {
   return (neuron0x8c65a18()*-0.000967352);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8a11948() {
   return (neuron0x8c65c18()*-0.862156);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8a11970() {
   return (neuron0x8c65e30()*0.051211);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8b0ee30() {
   return (neuron0x8cd12c8()*-1.75463);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8b0ee58() {
   return (neuron0x8cd14e0()*-0.187879);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cb1500() {
   return (neuron0x8cd16f8()*-0.549459);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cb1528() {
   return (neuron0x8cd1910()*0.459955);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3b08() {
   return (neuron0x8cb12e8()*0.342741);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3b30() {
   return (neuron0x8c65818()*0.86279);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3b58() {
   return (neuron0x8c65a18()*0.64646);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3b80() {
   return (neuron0x8c65c18()*0.387761);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3ba8() {
   return (neuron0x8c65e30()*0.075372);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3bd0() {
   return (neuron0x8cd12c8()*-0.854459);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3bf8() {
   return (neuron0x8cd14e0()*0.363174);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3c20() {
   return (neuron0x8cd16f8()*-0.0795057);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3c48() {
   return (neuron0x8cd1910()*-0.28661);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3e88() {
   return (neuron0x8cb12e8()*-0.205877);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3eb0() {
   return (neuron0x8c65818()*0.467289);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3ed8() {
   return (neuron0x8c65a18()*-0.0125571);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3f00() {
   return (neuron0x8c65c18()*0.0900278);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3f28() {
   return (neuron0x8c65e30()*0.403273);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3f50() {
   return (neuron0x8cd12c8()*0.106547);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3f78() {
   return (neuron0x8cd14e0()*-0.356731);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3fa0() {
   return (neuron0x8cd16f8()*0.921845);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3fc8() {
   return (neuron0x8cd1910()*-0.273598);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4208() {
   return (neuron0x8cb12e8()*0.343627);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4230() {
   return (neuron0x8c65818()*0.0485629);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4258() {
   return (neuron0x8c65a18()*-0.0808446);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4280() {
   return (neuron0x8c65c18()*0.0197922);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd42a8() {
   return (neuron0x8c65e30()*-0.414384);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd42d0() {
   return (neuron0x8cd12c8()*-0.043239);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd42f8() {
   return (neuron0x8cd14e0()*0.0532683);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4320() {
   return (neuron0x8cd16f8()*0.517185);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4348() {
   return (neuron0x8cd1910()*0.22434);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4588() {
   return (neuron0x8cb12e8()*-0.0252278);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd45b0() {
   return (neuron0x8c65818()*-0.432636);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd45d8() {
   return (neuron0x8c65a18()*-0.209687);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4600() {
   return (neuron0x8c65c18()*0.994915);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4628() {
   return (neuron0x8c65e30()*0.0316632);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4650() {
   return (neuron0x8cd12c8()*0.653465);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4678() {
   return (neuron0x8cd14e0()*0.0665051);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd46a0() {
   return (neuron0x8cd16f8()*-1.19913);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd46c8() {
   return (neuron0x8cd1910()*-0.108264);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4908() {
   return (neuron0x8cb12e8()*-0.243607);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4930() {
   return (neuron0x8c65818()*0.698485);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4958() {
   return (neuron0x8c65a18()*-0.110685);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4980() {
   return (neuron0x8c65c18()*-0.351996);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd49a8() {
   return (neuron0x8c65e30()*-0.230496);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd49d0() {
   return (neuron0x8cd12c8()*-0.586876);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd49f8() {
   return (neuron0x8cd14e0()*-0.108868);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4a20() {
   return (neuron0x8cd16f8()*-0.525708);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4a48() {
   return (neuron0x8cd1910()*-0.0481987);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4c88() {
   return (neuron0x8cb12e8()*0.415501);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4cb0() {
   return (neuron0x8c65818()*0.174787);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4cd8() {
   return (neuron0x8c65a18()*0.214781);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4d00() {
   return (neuron0x8c65c18()*-0.192925);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4d28() {
   return (neuron0x8c65e30()*0.385162);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4d50() {
   return (neuron0x8cd12c8()*-0.120364);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4d78() {
   return (neuron0x8cd14e0()*-0.56983);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4da0() {
   return (neuron0x8cd16f8()*0.0763491);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd4dc8() {
   return (neuron0x8cd1910()*-0.281497);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5008() {
   return (neuron0x8cb12e8()*-0.475301);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5030() {
   return (neuron0x8c65818()*0.297218);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5058() {
   return (neuron0x8c65a18()*0.0529889);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cb1550() {
   return (neuron0x8c65c18()*0.0372365);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cb1578() {
   return (neuron0x8c65e30()*-0.208552);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2938() {
   return (neuron0x8cd12c8()*-0.160688);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2960() {
   return (neuron0x8cd14e0()*0.185095);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd2988() {
   return (neuron0x8cd16f8()*0.55062);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd29b0() {
   return (neuron0x8cd1910()*0.303293);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd38d0() {
   return (neuron0x8cb12e8()*0.19852);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5510() {
   return (neuron0x8c65818()*0.451106);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd55c0() {
   return (neuron0x8c65a18()*-0.117375);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5670() {
   return (neuron0x8c65c18()*0.364383);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5720() {
   return (neuron0x8c65e30()*-0.131657);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd57d0() {
   return (neuron0x8cd12c8()*-1.21136);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5880() {
   return (neuron0x8cd14e0()*-0.0653638);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5930() {
   return (neuron0x8cd16f8()*1.10147);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd59e0() {
   return (neuron0x8cd1910()*-0.101577);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5bd0() {
   return (neuron0x8cb12e8()*-0.152639);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5bf8() {
   return (neuron0x8c65818()*0.786723);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5c20() {
   return (neuron0x8c65a18()*-0.0198979);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5c48() {
   return (neuron0x8c65c18()*0.685859);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5c70() {
   return (neuron0x8c65e30()*-0.0636167);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5c98() {
   return (neuron0x8cd12c8()*-2.47145);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5cc0() {
   return (neuron0x8cd14e0()*0.0231484);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5ce8() {
   return (neuron0x8cd16f8()*-1.935);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5d10() {
   return (neuron0x8cd1910()*0.00377447);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5e78() {
   return (neuron0x8cb12e8()*0.2727);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5ea0() {
   return (neuron0x8c65818()*0.217741);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5ec8() {
   return (neuron0x8c65a18()*-0.157998);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5ef0() {
   return (neuron0x8c65c18()*-0.203796);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5f18() {
   return (neuron0x8c65e30()*0.0445994);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5f40() {
   return (neuron0x8cd12c8()*-0.123715);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5f68() {
   return (neuron0x8cd14e0()*0.018711);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5f90() {
   return (neuron0x8cd16f8()*0.521417);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5fb8() {
   return (neuron0x8cd1910()*-0.627381);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6120() {
   return (neuron0x8cb12e8()*0.154922);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6148() {
   return (neuron0x8c65818()*-0.437719);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6170() {
   return (neuron0x8c65a18()*0.00968412);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6198() {
   return (neuron0x8c65c18()*0.897265);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd61c0() {
   return (neuron0x8c65e30()*0.30583);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd61e8() {
   return (neuron0x8cd12c8()*-0.400349);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6210() {
   return (neuron0x8cd14e0()*0.42531);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6238() {
   return (neuron0x8cd16f8()*-0.274448);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6260() {
   return (neuron0x8cd1910()*-0.710831);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd64a0() {
   return (neuron0x8cb12e8()*-0.709436);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd64c8() {
   return (neuron0x8c65818()*0.152056);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd64f0() {
   return (neuron0x8c65a18()*0.360728);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6518() {
   return (neuron0x8c65c18()*-0.234542);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6540() {
   return (neuron0x8c65e30()*-0.101361);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6568() {
   return (neuron0x8cd12c8()*-0.45564);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6590() {
   return (neuron0x8cd14e0()*-0.0607588);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd65b8() {
   return (neuron0x8cd16f8()*0.440459);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd65e0() {
   return (neuron0x8cd1910()*0.364355);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6820() {
   return (neuron0x8cb12e8()*-0.103505);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6848() {
   return (neuron0x8c65818()*-0.190081);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6870() {
   return (neuron0x8c65a18()*1.01365);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6898() {
   return (neuron0x8c65c18()*0.130381);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd68c0() {
   return (neuron0x8c65e30()*0.414122);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd68e8() {
   return (neuron0x8cd12c8()*-0.248157);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6910() {
   return (neuron0x8cd14e0()*-0.395665);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6938() {
   return (neuron0x8cd16f8()*0.228448);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6960() {
   return (neuron0x8cd1910()*0.416156);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6ba0() {
   return (neuron0x8cb12e8()*0.000150145);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6bc8() {
   return (neuron0x8c65818()*-0.243186);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6bf0() {
   return (neuron0x8c65a18()*0.131592);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6c18() {
   return (neuron0x8c65c18()*-0.0711521);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6c40() {
   return (neuron0x8c65e30()*0.300832);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6c68() {
   return (neuron0x8cd12c8()*0.038759);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6c90() {
   return (neuron0x8cd14e0()*-0.0673934);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6cb8() {
   return (neuron0x8cd16f8()*0.165585);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6ce0() {
   return (neuron0x8cd1910()*-0.270335);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6f20() {
   return (neuron0x8cb12e8()*-0.440504);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6f48() {
   return (neuron0x8c65818()*-0.2005);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6f70() {
   return (neuron0x8c65a18()*0.447374);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6f98() {
   return (neuron0x8c65c18()*-0.499035);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6fc0() {
   return (neuron0x8c65e30()*0.288696);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd6fe8() {
   return (neuron0x8cd12c8()*-0.093974);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd7010() {
   return (neuron0x8cd14e0()*0.284757);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd7038() {
   return (neuron0x8cd16f8()*-0.212596);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd7060() {
   return (neuron0x8cd1910()*-0.331197);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd3678() {
   return (neuron0x8cb12e8()*-0.123528);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd36a0() {
   return (neuron0x8c65818()*-0.830324);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd36c8() {
   return (neuron0x8c65a18()*-0.687343);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8348() {
   return (neuron0x8c65c18()*-0.0679667);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8370() {
   return (neuron0x8c65e30()*0.361799);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8398() {
   return (neuron0x8cd12c8()*0.219769);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd83c0() {
   return (neuron0x8cd14e0()*-0.524142);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd83e8() {
   return (neuron0x8cd16f8()*-0.0077226);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8410() {
   return (neuron0x8cd1910()*0.43772);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8638() {
   return (neuron0x8cb12e8()*0.349113);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8660() {
   return (neuron0x8c65818()*0.251593);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8688() {
   return (neuron0x8c65a18()*-0.237286);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd86b0() {
   return (neuron0x8c65c18()*-0.260218);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd86d8() {
   return (neuron0x8c65e30()*0.479993);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8700() {
   return (neuron0x8cd12c8()*-0.275592);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8728() {
   return (neuron0x8cd14e0()*0.256437);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8750() {
   return (neuron0x8cd16f8()*-0.111019);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8778() {
   return (neuron0x8cd1910()*-0.515841);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd89a0() {
   return (neuron0x8cb12e8()*0.124673);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd89c8() {
   return (neuron0x8c65818()*-0.733767);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd89f0() {
   return (neuron0x8c65a18()*0.0288051);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8a18() {
   return (neuron0x8c65c18()*0.666626);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8a40() {
   return (neuron0x8c65e30()*0.0733088);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8a68() {
   return (neuron0x8cd12c8()*3.1515);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8a90() {
   return (neuron0x8cd14e0()*0.00783805);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8ab8() {
   return (neuron0x8cd16f8()*-1.99676);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8ae0() {
   return (neuron0x8cd1910()*-0.0140848);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8d08() {
   return (neuron0x8cb12e8()*0.42963);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8d30() {
   return (neuron0x8c65818()*0.450949);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8d58() {
   return (neuron0x8c65a18()*-0.631921);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8d80() {
   return (neuron0x8c65c18()*0.21086);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8da8() {
   return (neuron0x8c65e30()*-0.210719);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8dd0() {
   return (neuron0x8cd12c8()*0.299086);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8df8() {
   return (neuron0x8cd14e0()*-0.155652);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8e20() {
   return (neuron0x8cd16f8()*-0.373525);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8e48() {
   return (neuron0x8cd1910()*-0.211194);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9070() {
   return (neuron0x8cb12e8()*-0.14532);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9098() {
   return (neuron0x8c65818()*-0.0561603);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd90c0() {
   return (neuron0x8c65a18()*0.436811);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd90e8() {
   return (neuron0x8c65c18()*-0.257864);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9110() {
   return (neuron0x8c65e30()*-0.228703);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9138() {
   return (neuron0x8cd12c8()*-0.141943);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9160() {
   return (neuron0x8cd14e0()*-0.379284);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9188() {
   return (neuron0x8cd16f8()*-0.503463);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd91b0() {
   return (neuron0x8cd1910()*0.375928);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd93d8() {
   return (neuron0x8cb12e8()*-0.714324);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9400() {
   return (neuron0x8c65818()*-0.423735);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9428() {
   return (neuron0x8c65a18()*-0.469614);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9450() {
   return (neuron0x8c65c18()*-0.108365);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9478() {
   return (neuron0x8c65e30()*-0.28976);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5080() {
   return (neuron0x8cd12c8()*0.351403);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd50a8() {
   return (neuron0x8cd14e0()*0.185259);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd50d0() {
   return (neuron0x8cd16f8()*0.597654);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd50f8() {
   return (neuron0x8cd1910()*-0.0403421);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5338() {
   return (neuron0x8cb12e8()*-0.11873);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5360() {
   return (neuron0x8c65818()*0.0916638);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5388() {
   return (neuron0x8c65a18()*0.0743818);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd53b0() {
   return (neuron0x8c65c18()*0.489323);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd53d8() {
   return (neuron0x8c65e30()*0.250383);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5400() {
   return (neuron0x8cd12c8()*0.126452);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5428() {
   return (neuron0x8cd14e0()*0.183718);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5450() {
   return (neuron0x8cd16f8()*0.579321);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9ca8() {
   return (neuron0x8cd1910()*0.113039);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9ed0() {
   return (neuron0x8cb12e8()*-0.126731);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9ef8() {
   return (neuron0x8c65818()*-0.766545);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9f20() {
   return (neuron0x8c65a18()*-0.3148);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9f48() {
   return (neuron0x8c65c18()*-0.278439);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9f70() {
   return (neuron0x8c65e30()*0.228047);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9f98() {
   return (neuron0x8cd12c8()*-1.0838);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9fc0() {
   return (neuron0x8cd14e0()*0.260091);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9fe8() {
   return (neuron0x8cd16f8()*-1.13913);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda010() {
   return (neuron0x8cd1910()*-0.0232352);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda238() {
   return (neuron0x8cb12e8()*-0.453498);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5478() {
   return (neuron0x8c65818()*0.195499);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd54a0() {
   return (neuron0x8c65a18()*0.0393907);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd54c8() {
   return (neuron0x8c65c18()*0.545892);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5538() {
   return (neuron0x8c65e30()*-0.149977);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5560() {
   return (neuron0x8cd12c8()*0.284811);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5588() {
   return (neuron0x8cd14e0()*0.155892);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd55e8() {
   return (neuron0x8cd16f8()*0.23628);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5610() {
   return (neuron0x8cd1910()*0.0349009);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5840() {
   return (neuron0x8cb12e8()*-0.0483889);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5638() {
   return (neuron0x8c65818()*0.539516);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd56e0() {
   return (neuron0x8c65a18()*0.587426);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5790() {
   return (neuron0x8c65c18()*-0.348581);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd58a8() {
   return (neuron0x8c65e30()*0.00544204);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd58d0() {
   return (neuron0x8cd12c8()*0.45444);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd58f8() {
   return (neuron0x8cd14e0()*0.00281601);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5958() {
   return (neuron0x8cd16f8()*-0.451175);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5980() {
   return (neuron0x8cd1910()*0.0646807);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd59a8() {
   return (neuron0x8cb12e8()*0.150535);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd5a50() {
   return (neuron0x8c65818()*0.101814);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdae88() {
   return (neuron0x8c65a18()*0.285055);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdaeb0() {
   return (neuron0x8c65c18()*0.475623);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdaed8() {
   return (neuron0x8c65e30()*-0.485509);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdaf00() {
   return (neuron0x8cd12c8()*0.06542);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdaf28() {
   return (neuron0x8cd14e0()*-0.210669);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdaf50() {
   return (neuron0x8cd16f8()*0.472176);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdaf78() {
   return (neuron0x8cd1910()*0.45484);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb1a0() {
   return (neuron0x8cb12e8()*0.559094);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb1c8() {
   return (neuron0x8c65818()*-0.257002);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb1f0() {
   return (neuron0x8c65a18()*-0.208283);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb218() {
   return (neuron0x8c65c18()*0.238516);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb240() {
   return (neuron0x8c65e30()*0.318734);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb268() {
   return (neuron0x8cd12c8()*-0.538183);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb290() {
   return (neuron0x8cd14e0()*0.1735);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb2b8() {
   return (neuron0x8cd16f8()*0.0932985);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb2e0() {
   return (neuron0x8cd1910()*-0.0202123);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb508() {
   return (neuron0x8cb12e8()*-0.206528);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb530() {
   return (neuron0x8c65818()*-0.26873);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb558() {
   return (neuron0x8c65a18()*-0.1905);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb580() {
   return (neuron0x8c65c18()*0.536208);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb5a8() {
   return (neuron0x8c65e30()*-0.357342);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb5d0() {
   return (neuron0x8cd12c8()*-0.243094);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb5f8() {
   return (neuron0x8cd14e0()*0.263875);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb620() {
   return (neuron0x8cd16f8()*0.0869263);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb648() {
   return (neuron0x8cd1910()*-0.236455);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb870() {
   return (neuron0x8cb12e8()*0.0357295);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb898() {
   return (neuron0x8c65818()*0.402472);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb8c0() {
   return (neuron0x8c65a18()*-3.16769);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb8e8() {
   return (neuron0x8c65c18()*0.140143);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb910() {
   return (neuron0x8c65e30()*-0.133745);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb938() {
   return (neuron0x8cd12c8()*0.111864);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb960() {
   return (neuron0x8cd14e0()*-0.00652133);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb988() {
   return (neuron0x8cd16f8()*0.0909011);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdb9b0() {
   return (neuron0x8cd1910()*-0.172145);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbbd8() {
   return (neuron0x8cb12e8()*0.202353);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbc00() {
   return (neuron0x8c65818()*0.145741);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbc28() {
   return (neuron0x8c65a18()*0.0991719);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbc50() {
   return (neuron0x8c65c18()*-0.0294999);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbc78() {
   return (neuron0x8c65e30()*-0.365531);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbca0() {
   return (neuron0x8cd12c8()*0.495145);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbcc8() {
   return (neuron0x8cd14e0()*0.0189875);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbcf0() {
   return (neuron0x8cd16f8()*-0.417281);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbd18() {
   return (neuron0x8cd1910()*0.0490695);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbf40() {
   return (neuron0x8cb12e8()*-0.251769);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbf68() {
   return (neuron0x8c65818()*0.00579956);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbf90() {
   return (neuron0x8c65a18()*0.154752);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbfb8() {
   return (neuron0x8c65c18()*-0.541864);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdbfe0() {
   return (neuron0x8c65e30()*0.307751);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc008() {
   return (neuron0x8cd12c8()*0.40815);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc030() {
   return (neuron0x8cd14e0()*-0.291906);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc058() {
   return (neuron0x8cd16f8()*0.131872);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc080() {
   return (neuron0x8cd1910()*0.0993988);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc2a8() {
   return (neuron0x8cb12e8()*0.269856);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc2d0() {
   return (neuron0x8c65818()*-0.402664);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc2f8() {
   return (neuron0x8c65a18()*-0.450169);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc320() {
   return (neuron0x8c65c18()*-0.0929224);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc348() {
   return (neuron0x8c65e30()*0.0702468);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc370() {
   return (neuron0x8cd12c8()*-0.250474);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc398() {
   return (neuron0x8cd14e0()*0.140312);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc3c0() {
   return (neuron0x8cd16f8()*0.189451);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc3e8() {
   return (neuron0x8cd1910()*-0.372317);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc628() {
   return (neuron0x8cb12e8()*-0.0244972);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc650() {
   return (neuron0x8c65818()*0.0175423);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc678() {
   return (neuron0x8c65a18()*-9.29339);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc6a0() {
   return (neuron0x8c65c18()*0.0946685);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc6c8() {
   return (neuron0x8c65e30()*-0.17648);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc6f0() {
   return (neuron0x8cd12c8()*0.0348276);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc718() {
   return (neuron0x8cd14e0()*-0.129379);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc740() {
   return (neuron0x8cd16f8()*0.0426286);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc768() {
   return (neuron0x8cd1910()*-0.184908);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc9a8() {
   return (neuron0x8cb12e8()*0.309838);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc9d0() {
   return (neuron0x8c65818()*0.11615);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdc9f8() {
   return (neuron0x8c65a18()*-0.053562);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdca20() {
   return (neuron0x8c65c18()*0.0497474);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdca48() {
   return (neuron0x8c65e30()*0.103301);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdca70() {
   return (neuron0x8cd12c8()*0.042855);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdca98() {
   return (neuron0x8cd14e0()*0.106836);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdcac0() {
   return (neuron0x8cd16f8()*0.074127);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdcae8() {
   return (neuron0x8cd1910()*0.112135);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdcd28() {
   return (neuron0x8cb12e8()*-0.417754);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdcd50() {
   return (neuron0x8c65818()*0.215144);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdcd78() {
   return (neuron0x8c65a18()*0.0132108);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdcda0() {
   return (neuron0x8c65c18()*-0.216876);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdcdc8() {
   return (neuron0x8c65e30()*-0.434818);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdcdf0() {
   return (neuron0x8cd12c8()*-0.684935);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdce18() {
   return (neuron0x8cd14e0()*0.428138);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdce40() {
   return (neuron0x8cd16f8()*0.641247);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdce68() {
   return (neuron0x8cd1910()*-0.0109153);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd0a8() {
   return (neuron0x8cb12e8()*0.27579);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd0d0() {
   return (neuron0x8c65818()*0.101339);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd0f8() {
   return (neuron0x8c65a18()*0.784699);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd120() {
   return (neuron0x8c65c18()*0.760493);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd148() {
   return (neuron0x8c65e30()*-0.0194522);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd170() {
   return (neuron0x8cd12c8()*-0.315316);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd198() {
   return (neuron0x8cd14e0()*0.316144);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd1c0() {
   return (neuron0x8cd16f8()*-0.0962328);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd1e8() {
   return (neuron0x8cd1910()*-0.563015);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd428() {
   return (neuron0x8cb12e8()*0.272599);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd450() {
   return (neuron0x8c65818()*0.476534);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd478() {
   return (neuron0x8c65a18()*-0.706564);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd4a0() {
   return (neuron0x8c65c18()*0.35785);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd4c8() {
   return (neuron0x8c65e30()*-0.00861298);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd4f0() {
   return (neuron0x8cd12c8()*0.210386);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd518() {
   return (neuron0x8cd14e0()*0.0293623);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd540() {
   return (neuron0x8cd16f8()*-0.138661);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd568() {
   return (neuron0x8cd1910()*0.00846269);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd7a8() {
   return (neuron0x8cb12e8()*0.284862);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd7d0() {
   return (neuron0x8c65818()*0.535783);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd7f8() {
   return (neuron0x8c65a18()*-0.176652);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd820() {
   return (neuron0x8c65c18()*0.320798);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd848() {
   return (neuron0x8c65e30()*0.3296);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd870() {
   return (neuron0x8cd12c8()*1.75946);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd898() {
   return (neuron0x8cd14e0()*0.0194771);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd8c0() {
   return (neuron0x8cd16f8()*0.537092);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdd8e8() {
   return (neuron0x8cd1910()*0.275783);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddb28() {
   return (neuron0x8cb12e8()*-0.00912169);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddb50() {
   return (neuron0x8c65818()*0.858326);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddb78() {
   return (neuron0x8c65a18()*-0.15389);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddba0() {
   return (neuron0x8c65c18()*0.64145);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddbc8() {
   return (neuron0x8c65e30()*-0.0190805);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddbf0() {
   return (neuron0x8cd12c8()*-2.99566);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddc18() {
   return (neuron0x8cd14e0()*-0.0266261);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddc40() {
   return (neuron0x8cd16f8()*1.68843);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddc68() {
   return (neuron0x8cd1910()*0.0566099);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddea8() {
   return (neuron0x8cb12e8()*-0.156103);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdded0() {
   return (neuron0x8c65818()*-0.228606);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddef8() {
   return (neuron0x8c65a18()*-0.667855);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddf20() {
   return (neuron0x8c65c18()*0.257184);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddf48() {
   return (neuron0x8c65e30()*-0.164347);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddf70() {
   return (neuron0x8cd12c8()*-0.213894);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddf98() {
   return (neuron0x8cd14e0()*-0.286104);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddfc0() {
   return (neuron0x8cd16f8()*0.144307);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cddfe8() {
   return (neuron0x8cd1910()*-0.336565);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde228() {
   return (neuron0x8cb12e8()*0.276155);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde250() {
   return (neuron0x8c65818()*-0.781304);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde278() {
   return (neuron0x8c65a18()*0.212536);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde2a0() {
   return (neuron0x8c65c18()*-0.161995);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde2c8() {
   return (neuron0x8c65e30()*-0.0517081);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde2f0() {
   return (neuron0x8cd12c8()*-1.21259);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde318() {
   return (neuron0x8cd14e0()*-0.105394);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde340() {
   return (neuron0x8cd16f8()*-0.740219);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde368() {
   return (neuron0x8cd1910()*-0.0275229);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde5a8() {
   return (neuron0x8cb12e8()*-0.589199);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde5d0() {
   return (neuron0x8c65818()*0.2024);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde5f8() {
   return (neuron0x8c65a18()*-0.174648);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde620() {
   return (neuron0x8c65c18()*-0.325814);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde648() {
   return (neuron0x8c65e30()*0.0131979);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde670() {
   return (neuron0x8cd12c8()*0.218527);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde698() {
   return (neuron0x8cd14e0()*0.198368);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde6c0() {
   return (neuron0x8cd16f8()*-0.532096);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde6e8() {
   return (neuron0x8cd1910()*-0.095428);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde928() {
   return (neuron0x8cb12e8()*-0.0838565);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde950() {
   return (neuron0x8c65818()*1.37881);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde978() {
   return (neuron0x8c65a18()*-0.0259322);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde9a0() {
   return (neuron0x8c65c18()*0.166542);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde9c8() {
   return (neuron0x8c65e30()*-0.0962803);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cde9f0() {
   return (neuron0x8cd12c8()*-0.203313);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdea18() {
   return (neuron0x8cd14e0()*0.0451245);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdea40() {
   return (neuron0x8cd16f8()*-0.0521932);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdea68() {
   return (neuron0x8cd1910()*-0.00253625);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdeca8() {
   return (neuron0x8cb12e8()*-0.153387);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdecd0() {
   return (neuron0x8c65818()*0.0570268);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdecf8() {
   return (neuron0x8c65a18()*-0.389026);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cded20() {
   return (neuron0x8c65c18()*0.102302);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cded48() {
   return (neuron0x8c65e30()*0.535764);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cded70() {
   return (neuron0x8cd12c8()*-0.168175);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cded98() {
   return (neuron0x8cd14e0()*-0.597766);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdedc0() {
   return (neuron0x8cd16f8()*-0.580951);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdede8() {
   return (neuron0x8cd1910()*0.541046);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf028() {
   return (neuron0x8cb12e8()*0.175194);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf050() {
   return (neuron0x8c65818()*-0.0253335);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf078() {
   return (neuron0x8c65a18()*-0.32624);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf0a0() {
   return (neuron0x8c65c18()*-0.0325224);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf0c8() {
   return (neuron0x8c65e30()*-0.0253937);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf0f0() {
   return (neuron0x8cd12c8()*0.428864);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf118() {
   return (neuron0x8cd14e0()*-0.231947);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf140() {
   return (neuron0x8cd16f8()*0.317187);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf168() {
   return (neuron0x8cd1910()*-0.156238);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf3a8() {
   return (neuron0x8cb12e8()*-0.11059);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf3d0() {
   return (neuron0x8c65818()*-0.428394);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf3f8() {
   return (neuron0x8c65a18()*0.606738);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf420() {
   return (neuron0x8c65c18()*-0.156293);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf448() {
   return (neuron0x8c65e30()*-0.305103);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf470() {
   return (neuron0x8cd12c8()*-0.0720916);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf498() {
   return (neuron0x8cd14e0()*0.330008);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf4c0() {
   return (neuron0x8cd16f8()*-0.224122);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf4e8() {
   return (neuron0x8cd1910()*-0.283144);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf728() {
   return (neuron0x8cb12e8()*-0.174587);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf750() {
   return (neuron0x8c65818()*-0.263945);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf778() {
   return (neuron0x8c65a18()*-0.425295);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf7a0() {
   return (neuron0x8c65c18()*-0.303346);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf7c8() {
   return (neuron0x8c65e30()*0.0297961);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf7f0() {
   return (neuron0x8cd12c8()*0.788395);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf818() {
   return (neuron0x8cd14e0()*0.153604);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf840() {
   return (neuron0x8cd16f8()*-0.068108);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdf868() {
   return (neuron0x8cd1910()*-0.0266257);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8240() {
   return (neuron0x8cb12e8()*-0.0496773);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8268() {
   return (neuron0x8c65818()*-0.509689);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8290() {
   return (neuron0x8c65a18()*-1.19145);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd82b8() {
   return (neuron0x8c65c18()*-0.00639818);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd82e0() {
   return (neuron0x8c65e30()*-0.0224987);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd8308() {
   return (neuron0x8cd12c8()*0.446945);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdfcb0() {
   return (neuron0x8cd14e0()*-0.288047);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdfcd8() {
   return (neuron0x8cd16f8()*0.486448);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdfd00() {
   return (neuron0x8cd1910()*0.433808);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdff28() {
   return (neuron0x8cb12e8()*-0.0383051);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdff50() {
   return (neuron0x8c65818()*0.427563);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdff78() {
   return (neuron0x8c65a18()*0.0740928);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdffa0() {
   return (neuron0x8c65c18()*-0.371527);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdffc8() {
   return (neuron0x8c65e30()*0.528082);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cdfff0() {
   return (neuron0x8cd12c8()*0.599439);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce0018() {
   return (neuron0x8cd14e0()*0.531357);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce0040() {
   return (neuron0x8cd16f8()*0.121752);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce0068() {
   return (neuron0x8cd1910()*-0.259566);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd96b8() {
   return (neuron0x8cb12e8()*-0.268113);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd96e0() {
   return (neuron0x8c65818()*-0.86251);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9708() {
   return (neuron0x8c65a18()*-0.00664997);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9730() {
   return (neuron0x8c65c18()*-0.39467);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9758() {
   return (neuron0x8c65e30()*-0.122375);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9780() {
   return (neuron0x8cd12c8()*0.240513);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd97a8() {
   return (neuron0x8cd14e0()*-0.0104006);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd97d0() {
   return (neuron0x8cd16f8()*-1.20789);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd97f8() {
   return (neuron0x8cd1910()*-0.219314);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9a38() {
   return (neuron0x8cb12e8()*0.124152);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9a60() {
   return (neuron0x8c65818()*-0.245383);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9a88() {
   return (neuron0x8c65a18()*0.0658383);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9ab0() {
   return (neuron0x8c65c18()*-0.91588);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9ad8() {
   return (neuron0x8c65e30()*0.119003);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9b00() {
   return (neuron0x8cd12c8()*2.39257);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9b28() {
   return (neuron0x8cd14e0()*-0.0703799);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9b50() {
   return (neuron0x8cd16f8()*-1.21657);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd9b78() {
   return (neuron0x8cd1910()*0.0675887);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce11c0() {
   return (neuron0x8cb12e8()*-0.330895);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce11e8() {
   return (neuron0x8c65818()*-0.779442);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1210() {
   return (neuron0x8c65a18()*-0.133636);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1238() {
   return (neuron0x8c65c18()*1.10936);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1260() {
   return (neuron0x8c65e30()*0.0564373);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1288() {
   return (neuron0x8cd12c8()*-0.209613);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce12b0() {
   return (neuron0x8cd14e0()*0.0643532);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce12d8() {
   return (neuron0x8cd16f8()*-0.27979);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1300() {
   return (neuron0x8cd1910()*0.0597313);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1528() {
   return (neuron0x8cb12e8()*-0.164978);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1550() {
   return (neuron0x8c65818()*0.707152);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1578() {
   return (neuron0x8c65a18()*0.44533);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce15a0() {
   return (neuron0x8c65c18()*-0.467602);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce15c8() {
   return (neuron0x8c65e30()*-0.1608);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce15f0() {
   return (neuron0x8cd12c8()*-0.797283);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1618() {
   return (neuron0x8cd14e0()*-0.104077);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1640() {
   return (neuron0x8cd16f8()*0.170891);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1668() {
   return (neuron0x8cd1910()*-0.0896319);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce18a8() {
   return (neuron0x8cb12e8()*0.0777371);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce18d0() {
   return (neuron0x8c65818()*-0.334445);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce18f8() {
   return (neuron0x8c65a18()*-3.55026);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1920() {
   return (neuron0x8c65c18()*0.157896);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1948() {
   return (neuron0x8c65e30()*-0.246272);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1970() {
   return (neuron0x8cd12c8()*0.0233905);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1998() {
   return (neuron0x8cd14e0()*0.00478475);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce19c0() {
   return (neuron0x8cd16f8()*0.102253);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce19e8() {
   return (neuron0x8cd1910()*-0.223012);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1c28() {
   return (neuron0x8cb12e8()*-0.31359);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1c50() {
   return (neuron0x8c65818()*0.47864);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1c78() {
   return (neuron0x8c65a18()*0.612331);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1ca0() {
   return (neuron0x8c65c18()*-0.0889229);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1cc8() {
   return (neuron0x8c65e30()*0.154015);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1cf0() {
   return (neuron0x8cd12c8()*0.201891);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1d18() {
   return (neuron0x8cd14e0()*0.0941138);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1d40() {
   return (neuron0x8cd16f8()*-0.66684);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1d68() {
   return (neuron0x8cd1910()*0.102432);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce1fa8() {
   return (neuron0x8cb12e8()*0.0738032);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda260() {
   return (neuron0x8c65818()*0.12606);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda288() {
   return (neuron0x8c65a18()*-0.306734);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda2b0() {
   return (neuron0x8c65c18()*0.0688255);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda4e0() {
   return (neuron0x8c65e30()*-0.0977948);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda508() {
   return (neuron0x8cd12c8()*0.00817244);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda738() {
   return (neuron0x8cd14e0()*0.270865);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda760() {
   return (neuron0x8cd16f8()*-0.585339);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cda998() {
   return (neuron0x8cd1910()*-0.100471);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2c48() {
   return (neuron0x8cb12e8()*-0.34826);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2c70() {
   return (neuron0x8c65818()*-0.0148818);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2c98() {
   return (neuron0x8c65a18()*-0.28064);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2cc0() {
   return (neuron0x8c65c18()*-0.347261);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2ce8() {
   return (neuron0x8c65e30()*-0.254066);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2d10() {
   return (neuron0x8cd12c8()*-0.255215);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2d38() {
   return (neuron0x8cd14e0()*-0.0688646);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2d60() {
   return (neuron0x8cd16f8()*0.0908059);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2d88() {
   return (neuron0x8cd1910()*0.0101518);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2fb0() {
   return (neuron0x8cb12e8()*0.0786946);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce2fd8() {
   return (neuron0x8c65818()*-0.0849928);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3000() {
   return (neuron0x8c65a18()*0.468835);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3028() {
   return (neuron0x8c65c18()*0.286019);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3050() {
   return (neuron0x8c65e30()*0.309045);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3078() {
   return (neuron0x8cd12c8()*-0.156804);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce30a0() {
   return (neuron0x8cd14e0()*0.466751);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce30c8() {
   return (neuron0x8cd16f8()*0.0689725);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce30f0() {
   return (neuron0x8cd1910()*-0.182453);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3330() {
   return (neuron0x8cb12e8()*-0.264467);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3358() {
   return (neuron0x8c65818()*0.269443);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3380() {
   return (neuron0x8c65a18()*-0.23634);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce33a8() {
   return (neuron0x8c65c18()*0.152589);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce33d0() {
   return (neuron0x8c65e30()*0.260359);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce33f8() {
   return (neuron0x8cd12c8()*0.213642);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3420() {
   return (neuron0x8cd14e0()*-0.403903);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3448() {
   return (neuron0x8cd16f8()*-0.123515);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3470() {
   return (neuron0x8cd1910()*-0.330286);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce36b0() {
   return (neuron0x8cb12e8()*-0.0542167);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce36d8() {
   return (neuron0x8c65818()*-0.0512806);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3700() {
   return (neuron0x8c65a18()*-0.0608946);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3728() {
   return (neuron0x8c65c18()*-0.495625);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3750() {
   return (neuron0x8c65e30()*0.0744851);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3778() {
   return (neuron0x8cd12c8()*0.737466);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce37a0() {
   return (neuron0x8cd14e0()*-0.225017);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce37c8() {
   return (neuron0x8cd16f8()*-0.122553);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce37f0() {
   return (neuron0x8cd1910()*0.0123292);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3a30() {
   return (neuron0x8cb12e8()*0.073942);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3a58() {
   return (neuron0x8c65818()*-0.563138);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3a80() {
   return (neuron0x8c65a18()*-0.0046945);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3aa8() {
   return (neuron0x8c65c18()*-0.160054);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3ad0() {
   return (neuron0x8c65e30()*-0.156625);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3af8() {
   return (neuron0x8cd12c8()*-0.493556);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3b20() {
   return (neuron0x8cd14e0()*-0.614744);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3b48() {
   return (neuron0x8cd16f8()*-0.433315);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3b70() {
   return (neuron0x8cd1910()*-0.739133);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3db0() {
   return (neuron0x8cb12e8()*0.496419);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3dd8() {
   return (neuron0x8c65818()*-0.093664);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3e00() {
   return (neuron0x8c65a18()*0.42295);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3e28() {
   return (neuron0x8c65c18()*-0.0291732);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3e50() {
   return (neuron0x8c65e30()*0.369435);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3e78() {
   return (neuron0x8cd12c8()*-0.107139);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3ea0() {
   return (neuron0x8cd14e0()*0.52263);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3ec8() {
   return (neuron0x8cd16f8()*-0.661302);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce3ef0() {
   return (neuron0x8cd1910()*0.153872);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4130() {
   return (neuron0x8cb12e8()*-0.564211);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4158() {
   return (neuron0x8c65818()*0.498972);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4180() {
   return (neuron0x8c65a18()*0.140097);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce41a8() {
   return (neuron0x8c65c18()*-0.51069);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce41d0() {
   return (neuron0x8c65e30()*0.173669);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce41f8() {
   return (neuron0x8cd12c8()*-0.36879);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4220() {
   return (neuron0x8cd14e0()*0.00495718);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4248() {
   return (neuron0x8cd16f8()*-0.997083);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4270() {
   return (neuron0x8cd1910()*0.000427034);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce44b0() {
   return (neuron0x8cb12e8()*0.447841);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce44d8() {
   return (neuron0x8c65818()*0.223305);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4500() {
   return (neuron0x8c65a18()*0.723559);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4528() {
   return (neuron0x8c65c18()*0.18285);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4550() {
   return (neuron0x8c65e30()*0.0100064);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4578() {
   return (neuron0x8cd12c8()*-0.348799);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce45a0() {
   return (neuron0x8cd14e0()*-0.458805);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce45c8() {
   return (neuron0x8cd16f8()*0.497873);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce45f0() {
   return (neuron0x8cd1910()*0.0884785);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4830() {
   return (neuron0x8cb12e8()*0.15749);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4858() {
   return (neuron0x8c65818()*0.432045);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4880() {
   return (neuron0x8c65a18()*0.125485);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce48a8() {
   return (neuron0x8c65c18()*1.43635);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce48d0() {
   return (neuron0x8c65e30()*-0.139316);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce48f8() {
   return (neuron0x8cd12c8()*0.202139);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4920() {
   return (neuron0x8cd14e0()*-0.0909847);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4948() {
   return (neuron0x8cd16f8()*0.0665725);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4970() {
   return (neuron0x8cd1910()*0.196736);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4bb0() {
   return (neuron0x8cb12e8()*0.0823286);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4bd8() {
   return (neuron0x8c65818()*0.677823);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4c00() {
   return (neuron0x8c65a18()*0.0619127);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4c28() {
   return (neuron0x8c65c18()*-0.0989147);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4c50() {
   return (neuron0x8c65e30()*-0.308312);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4c78() {
   return (neuron0x8cd12c8()*0.123405);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4ca0() {
   return (neuron0x8cd14e0()*-1.03501);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4cc8() {
   return (neuron0x8cd16f8()*-0.191306);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4cf0() {
   return (neuron0x8cd1910()*0.791859);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4f30() {
   return (neuron0x8cb12e8()*0.117749);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4f58() {
   return (neuron0x8c65818()*0.197646);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4f80() {
   return (neuron0x8c65a18()*0.114748);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4fa8() {
   return (neuron0x8c65c18()*0.310239);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4fd0() {
   return (neuron0x8c65e30()*-0.226238);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce4ff8() {
   return (neuron0x8cd12c8()*-0.439813);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5020() {
   return (neuron0x8cd14e0()*0.0749494);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5048() {
   return (neuron0x8cd16f8()*-0.148822);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5070() {
   return (neuron0x8cd1910()*0.605855);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce52b0() {
   return (neuron0x8cb12e8()*0.215799);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce52d8() {
   return (neuron0x8c65818()*-0.790673);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5300() {
   return (neuron0x8c65a18()*0.273893);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5328() {
   return (neuron0x8c65c18()*1.43299);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5350() {
   return (neuron0x8c65e30()*0.00864867);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5378() {
   return (neuron0x8cd12c8()*-0.118828);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce53a0() {
   return (neuron0x8cd14e0()*-0.209494);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce53c8() {
   return (neuron0x8cd16f8()*-0.320689);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce53f0() {
   return (neuron0x8cd1910()*0.196771);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5630() {
   return (neuron0x8cb12e8()*0.207423);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5658() {
   return (neuron0x8c65818()*-0.215887);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5680() {
   return (neuron0x8c65a18()*0.15103);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce56a8() {
   return (neuron0x8c65c18()*0.0917527);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce56d0() {
   return (neuron0x8c65e30()*0.202756);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce56f8() {
   return (neuron0x8cd12c8()*0.670824);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5720() {
   return (neuron0x8cd14e0()*0.427515);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5748() {
   return (neuron0x8cd16f8()*-0.138605);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5770() {
   return (neuron0x8cd1910()*-0.241595);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce59b0() {
   return (neuron0x8cb12e8()*0.0781463);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce59d8() {
   return (neuron0x8c65818()*-0.333593);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5a00() {
   return (neuron0x8c65a18()*0.0133503);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5a28() {
   return (neuron0x8c65c18()*-0.142091);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5a50() {
   return (neuron0x8c65e30()*0.528008);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5a78() {
   return (neuron0x8cd12c8()*0.262862);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5aa0() {
   return (neuron0x8cd14e0()*0.0487759);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5ac8() {
   return (neuron0x8cd16f8()*-0.211965);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5af0() {
   return (neuron0x8cd1910()*-0.258825);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5d30() {
   return (neuron0x8cb12e8()*0.220967);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5d58() {
   return (neuron0x8c65818()*-0.685599);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5d80() {
   return (neuron0x8c65a18()*-0.154447);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5da8() {
   return (neuron0x8c65c18()*-1.39571);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5dd0() {
   return (neuron0x8c65e30()*-0.211512);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5df8() {
   return (neuron0x8cd12c8()*-0.122778);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5e20() {
   return (neuron0x8cd14e0()*0.0223668);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5e48() {
   return (neuron0x8cd16f8()*-0.15672);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce5e70() {
   return (neuron0x8cd1910()*0.18861);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce60b0() {
   return (neuron0x8cb12e8()*-0.0804298);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce60d8() {
   return (neuron0x8c65818()*-0.0771622);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6100() {
   return (neuron0x8c65a18()*-0.176903);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6128() {
   return (neuron0x8c65c18()*0.00457977);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6150() {
   return (neuron0x8c65e30()*0.433605);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6178() {
   return (neuron0x8cd12c8()*-0.85573);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce61a0() {
   return (neuron0x8cd14e0()*-0.0794521);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce61c8() {
   return (neuron0x8cd16f8()*0.295579);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce61f0() {
   return (neuron0x8cd1910()*-0.0214742);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6430() {
   return (neuron0x8cb12e8()*-0.257104);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6458() {
   return (neuron0x8c65818()*0.342663);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6480() {
   return (neuron0x8c65a18()*-0.297609);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce64a8() {
   return (neuron0x8c65c18()*-0.136259);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce64d0() {
   return (neuron0x8c65e30()*0.463675);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce64f8() {
   return (neuron0x8cd12c8()*0.169282);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6520() {
   return (neuron0x8cd14e0()*-0.309323);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6548() {
   return (neuron0x8cd16f8()*0.200125);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6570() {
   return (neuron0x8cd1910()*-0.0489678);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce67b0() {
   return (neuron0x8cb12e8()*0.165864);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce67d8() {
   return (neuron0x8c65818()*-0.292263);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6800() {
   return (neuron0x8c65a18()*-0.531174);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6828() {
   return (neuron0x8c65c18()*0.370349);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6850() {
   return (neuron0x8c65e30()*-0.330451);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6878() {
   return (neuron0x8cd12c8()*0.0257193);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce68a0() {
   return (neuron0x8cd14e0()*0.194033);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce68c8() {
   return (neuron0x8cd16f8()*0.187567);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce68f0() {
   return (neuron0x8cd1910()*-0.283112);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6b30() {
   return (neuron0x8cb12e8()*0.236465);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6b58() {
   return (neuron0x8c65818()*-0.419864);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6b80() {
   return (neuron0x8c65a18()*0.336557);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6ba8() {
   return (neuron0x8c65c18()*-0.663211);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6bd0() {
   return (neuron0x8c65e30()*-0.369515);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6bf8() {
   return (neuron0x8cd12c8()*0.488235);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6c20() {
   return (neuron0x8cd14e0()*0.104173);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6c48() {
   return (neuron0x8cd16f8()*0.20704);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6c70() {
   return (neuron0x8cd1910()*0.0731683);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6eb0() {
   return (neuron0x8cb12e8()*-0.538683);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6ed8() {
   return (neuron0x8c65818()*0.0647044);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6f00() {
   return (neuron0x8c65a18()*0.0595639);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6f28() {
   return (neuron0x8c65c18()*0.253103);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6f50() {
   return (neuron0x8c65e30()*0.180452);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6f78() {
   return (neuron0x8cd12c8()*-0.237895);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6fa0() {
   return (neuron0x8cd14e0()*-0.0303538);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6fc8() {
   return (neuron0x8cd16f8()*0.257057);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce6ff0() {
   return (neuron0x8cd1910()*0.245723);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7230() {
   return (neuron0x8cb12e8()*0.0137846);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7258() {
   return (neuron0x8c65818()*-0.0395851);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7280() {
   return (neuron0x8c65a18()*0.436687);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce72a8() {
   return (neuron0x8c65c18()*0.0272121);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce72d0() {
   return (neuron0x8c65e30()*0.09783);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce72f8() {
   return (neuron0x8cd12c8()*0.0251843);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7320() {
   return (neuron0x8cd14e0()*0.327051);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7348() {
   return (neuron0x8cd16f8()*-0.275844);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7370() {
   return (neuron0x8cd1910()*-0.00128642);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce75b0() {
   return (neuron0x8cb12e8()*-0.251186);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce75d8() {
   return (neuron0x8c65818()*0.139495);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7600() {
   return (neuron0x8c65a18()*-0.0145181);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7628() {
   return (neuron0x8c65c18()*-0.276033);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7650() {
   return (neuron0x8c65e30()*-0.347952);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7678() {
   return (neuron0x8cd12c8()*0.123545);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce76a0() {
   return (neuron0x8cd14e0()*-0.541067);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce76c8() {
   return (neuron0x8cd16f8()*-0.233936);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce76f0() {
   return (neuron0x8cd1910()*0.387537);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7930() {
   return (neuron0x8cb12e8()*0.223899);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7958() {
   return (neuron0x8c65818()*-0.855791);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7980() {
   return (neuron0x8c65a18()*-0.00553155);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce79a8() {
   return (neuron0x8c65c18()*0.0555924);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce79d0() {
   return (neuron0x8c65e30()*0.357216);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce79f8() {
   return (neuron0x8cd12c8()*-0.211142);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7a20() {
   return (neuron0x8cd14e0()*-0.0155453);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7a48() {
   return (neuron0x8cd16f8()*1.72081);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7a70() {
   return (neuron0x8cd1910()*0.105949);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7cb0() {
   return (neuron0x8cb12e8()*0.073599);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7cd8() {
   return (neuron0x8c65818()*0.718983);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7d00() {
   return (neuron0x8c65a18()*-0.0726786);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7d28() {
   return (neuron0x8c65c18()*-0.721308);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7d50() {
   return (neuron0x8c65e30()*0.0549585);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7d78() {
   return (neuron0x8cd12c8()*-1.90332);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7da0() {
   return (neuron0x8cd14e0()*-0.270302);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7dc8() {
   return (neuron0x8cd16f8()*0.833663);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce7df0() {
   return (neuron0x8cd1910()*0.0569717);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8030() {
   return (neuron0x8cb12e8()*0.168344);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8058() {
   return (neuron0x8c65818()*-0.517125);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8080() {
   return (neuron0x8c65a18()*0.774043);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce80a8() {
   return (neuron0x8c65c18()*0.142889);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce80d0() {
   return (neuron0x8c65e30()*-0.399128);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce80f8() {
   return (neuron0x8cd12c8()*0.150675);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8120() {
   return (neuron0x8cd14e0()*-0.273382);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8148() {
   return (neuron0x8cd16f8()*-0.25104);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8170() {
   return (neuron0x8cd1910()*0.0405206);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce83b0() {
   return (neuron0x8cb12e8()*-0.0738023);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce83d8() {
   return (neuron0x8c65818()*-0.406391);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8400() {
   return (neuron0x8c65a18()*0.536898);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8428() {
   return (neuron0x8c65c18()*-0.858307);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8450() {
   return (neuron0x8c65e30()*-0.0302106);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8478() {
   return (neuron0x8cd12c8()*-0.448192);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce84a0() {
   return (neuron0x8cd14e0()*-0.0797473);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce84c8() {
   return (neuron0x8cd16f8()*0.184215);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce84f0() {
   return (neuron0x8cd1910()*0.68812);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8730() {
   return (neuron0x8cb12e8()*0.0802864);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8758() {
   return (neuron0x8c65818()*-0.590254);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8780() {
   return (neuron0x8c65a18()*0.00569584);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce87a8() {
   return (neuron0x8c65c18()*0.520585);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce87d0() {
   return (neuron0x8c65e30()*0.0896733);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce87f8() {
   return (neuron0x8cd12c8()*2.25784);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8820() {
   return (neuron0x8cd14e0()*0.000492371);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8848() {
   return (neuron0x8cd16f8()*1.31663);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8870() {
   return (neuron0x8cd1910()*0.0330979);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8ab0() {
   return (neuron0x8cb12e8()*0.497928);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8ad8() {
   return (neuron0x8c65818()*-0.0462321);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8b00() {
   return (neuron0x8c65a18()*-0.303515);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8b28() {
   return (neuron0x8c65c18()*-0.272813);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8b50() {
   return (neuron0x8c65e30()*0.558713);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8b78() {
   return (neuron0x8cd12c8()*-0.0632442);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8ba0() {
   return (neuron0x8cd14e0()*0.168359);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8bc8() {
   return (neuron0x8cd16f8()*-0.559541);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8bf0() {
   return (neuron0x8cd1910()*0.207655);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8e30() {
   return (neuron0x8cb12e8()*0.255587);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8e58() {
   return (neuron0x8c65818()*0.525232);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8e80() {
   return (neuron0x8c65a18()*0.492097);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8ea8() {
   return (neuron0x8c65c18()*-0.560454);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8ed0() {
   return (neuron0x8c65e30()*0.0928788);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8ef8() {
   return (neuron0x8cd12c8()*0.207933);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8f20() {
   return (neuron0x8cd14e0()*0.29547);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8f48() {
   return (neuron0x8cd16f8()*-0.181921);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce8f70() {
   return (neuron0x8cd1910()*-0.143189);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce91b0() {
   return (neuron0x8cb12e8()*0.0878973);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce91d8() {
   return (neuron0x8c65818()*0.523588);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9200() {
   return (neuron0x8c65a18()*-0.344408);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9228() {
   return (neuron0x8c65c18()*-0.187012);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9250() {
   return (neuron0x8c65e30()*-0.408086);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9278() {
   return (neuron0x8cd12c8()*0.7247);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce92a0() {
   return (neuron0x8cd14e0()*-0.101094);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce92c8() {
   return (neuron0x8cd16f8()*0.582627);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce92f0() {
   return (neuron0x8cd1910()*0.186659);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9530() {
   return (neuron0x8cb12e8()*0.0942043);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9558() {
   return (neuron0x8c65818()*0.733068);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9580() {
   return (neuron0x8c65a18()*0.604711);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce95a8() {
   return (neuron0x8c65c18()*-0.343682);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce95d0() {
   return (neuron0x8c65e30()*0.210919);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce95f8() {
   return (neuron0x8cd12c8()*0.101547);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9620() {
   return (neuron0x8cd14e0()*-0.0517069);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9648() {
   return (neuron0x8cd16f8()*-0.563484);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9670() {
   return (neuron0x8cd1910()*-0.00376306);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce98b0() {
   return (neuron0x8cb12e8()*-0.0379042);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce98d8() {
   return (neuron0x8c65818()*0.0199703);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9900() {
   return (neuron0x8c65a18()*0.120189);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9928() {
   return (neuron0x8c65c18()*-0.268602);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9950() {
   return (neuron0x8c65e30()*0.482115);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9978() {
   return (neuron0x8cd12c8()*-0.151101);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce99a0() {
   return (neuron0x8cd14e0()*-0.0244933);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce99c8() {
   return (neuron0x8cd16f8()*-0.166647);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce99f0() {
   return (neuron0x8cd1910()*-0.42767);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9c30() {
   return (neuron0x8cb12e8()*0.0776072);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9c58() {
   return (neuron0x8c65818()*-0.353825);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9c80() {
   return (neuron0x8c65a18()*0.514709);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9ca8() {
   return (neuron0x8c65c18()*-0.0629282);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9cd0() {
   return (neuron0x8c65e30()*0.0892651);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9cf8() {
   return (neuron0x8cd12c8()*0.0432644);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9d20() {
   return (neuron0x8cd14e0()*-0.0169827);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9d48() {
   return (neuron0x8cd16f8()*-0.0935086);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9d70() {
   return (neuron0x8cd1910()*-0.308365);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9fb0() {
   return (neuron0x8cb12e8()*-0.011953);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ce9fd8() {
   return (neuron0x8c65818()*-0.43404);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea000() {
   return (neuron0x8c65a18()*-0.683087);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea028() {
   return (neuron0x8c65c18()*0.469719);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea050() {
   return (neuron0x8c65e30()*-0.388616);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea078() {
   return (neuron0x8cd12c8()*-0.152118);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea0a0() {
   return (neuron0x8cd14e0()*-0.15801);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea0c8() {
   return (neuron0x8cd16f8()*0.0956855);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea0f0() {
   return (neuron0x8cd1910()*-0.209361);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea330() {
   return (neuron0x8cb12e8()*-0.135216);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea358() {
   return (neuron0x8c65818()*-1.03171);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea380() {
   return (neuron0x8c65a18()*0.136074);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea3a8() {
   return (neuron0x8c65c18()*-0.600231);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea3d0() {
   return (neuron0x8c65e30()*0.00609757);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea3f8() {
   return (neuron0x8cd12c8()*-1.62327);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea420() {
   return (neuron0x8cd14e0()*0.0310737);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea448() {
   return (neuron0x8cd16f8()*0.354331);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea470() {
   return (neuron0x8cd1910()*-0.0642596);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea6b0() {
   return (neuron0x8cb12e8()*0.283711);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea6d8() {
   return (neuron0x8c65818()*0.35329);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea700() {
   return (neuron0x8c65a18()*-0.00785351);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea728() {
   return (neuron0x8c65c18()*0.589383);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea750() {
   return (neuron0x8c65e30()*0.0834932);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea778() {
   return (neuron0x8cd12c8()*-0.154528);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea7a0() {
   return (neuron0x8cd14e0()*0.146758);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea7c8() {
   return (neuron0x8cd16f8()*-0.112677);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea7f0() {
   return (neuron0x8cd1910()*-0.217076);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea940() {
   return (neuron0x8cd1c68()*-0.135017);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea968() {
   return (neuron0x8cd1f38()*0.0960082);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea990() {
   return (neuron0x8cd2328()*1.80401);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea9b8() {
   return (neuron0x8cd2648()*0.427251);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cea9e0() {
   return (neuron0x8cd2a40()*-0.036164);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaa08() {
   return (neuron0x8cd2da8()*0.200176);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaa30() {
   return (neuron0x8cd3110()*-0.203258);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaa58() {
   return (neuron0x8cd3478()*0.634469);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaa80() {
   return (neuron0x8cd3908()*-0.330187);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaaa8() {
   return (neuron0x8cd3c70()*-0.519795);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaad0() {
   return (neuron0x8cd3ff0()*0.134601);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaaf8() {
   return (neuron0x8cd4370()*0.760083);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceab20() {
   return (neuron0x8cd46f0()*0.85334);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceab48() {
   return (neuron0x8cd4a70()*0.233019);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceab70() {
   return (neuron0x8cd4df0()*-0.0514337);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceab98() {
   return (neuron0x8cd3700()*1.13568);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceac48() {
   return (neuron0x8cd5a90()*-1.0142);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceac70() {
   return (neuron0x8cd5d38()*-0.105905);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceac98() {
   return (neuron0x8cd5fe0()*0.378979);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceacc0() {
   return (neuron0x8cd6288()*-0.411034);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceace8() {
   return (neuron0x8cd6608()*0.426759);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cead10() {
   return (neuron0x8cd6988()*-0.0565627);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cead38() {
   return (neuron0x8cd6d08()*0.0400153);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cead60() {
   return (neuron0x8cd8040()*0.359236);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cead88() {
   return (neuron0x8cd8438()*-0.0410262);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceadb0() {
   return (neuron0x8cd87a0()*1.03704);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceadd8() {
   return (neuron0x8cd8b08()*0.342708);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceae00() {
   return (neuron0x8cd8e70()*0.242745);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceae28() {
   return (neuron0x8cd91d8()*0.437528);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceae50() {
   return (neuron0x8cd5120()*-0.284294);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceae78() {
   return (neuron0x8cd9cd0()*-0.722518);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaea0() {
   return (neuron0x8cda038()*-0.266552);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceabc0() {
   return (neuron0x8cdaba8()*-0.506705);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceabe8() {
   return (neuron0x8cdacd0()*-0.0591741);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceac10() {
   return (neuron0x8cdafa0()*0.386228);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceafd0() {
   return (neuron0x8cdb308()*0.0836562);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cd1b80() {
   return (neuron0x8cdb670()*-1.33141);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaff8() {
   return (neuron0x8cdb9d8()*-0.448335);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb020() {
   return (neuron0x8cdbd40()*-0.124617);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb048() {
   return (neuron0x8cdc0a8()*-0.153845);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb070() {
   return (neuron0x8cdc410()*1.01677);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb098() {
   return (neuron0x8cdc790()*-0.179583);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb0c0() {
   return (neuron0x8cdcb10()*0.35095);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb0e8() {
   return (neuron0x8cdce90()*0.38973);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb110() {
   return (neuron0x8cdd210()*-0.658168);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb138() {
   return (neuron0x8cdd590()*0.318217);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb160() {
   return (neuron0x8cdd910()*-1.42132);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb188() {
   return (neuron0x8cddc90()*-0.256989);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb1b0() {
   return (neuron0x8cde010()*-0.966963);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb1d8() {
   return (neuron0x8cde390()*-0.168091);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb200() {
   return (neuron0x8cde710()*-1.53908);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb228() {
   return (neuron0x8cdea90()*-0.193957);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb250() {
   return (neuron0x8cdee10()*-0.178578);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb278() {
   return (neuron0x8cdf190()*0.5781);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb2a0() {
   return (neuron0x8cdf510()*-0.358732);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb2c8() {
   return (neuron0x8cdf890()*-0.438028);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb2f0() {
   return (neuron0x8cdfd28()*0.227616);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb318() {
   return (neuron0x8cd94a0()*0.836877);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb340() {
   return (neuron0x8cd9820()*-1.51553);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb368() {
   return (neuron0x8ce1098()*0.823);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb390() {
   return (neuron0x8ce1328()*0.0184129);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb3b8() {
   return (neuron0x8ce1690()*1.02319);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb3e0() {
   return (neuron0x8ce1a10()*0.389694);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb408() {
   return (neuron0x8ce1d90()*0.072497);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaec8() {
   return (neuron0x8cda9c0()*0.223611);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaef0() {
   return (neuron0x8ce2db0()*-0.560922);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaf18() {
   return (neuron0x8ce3118()*0.108893);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaf40() {
   return (neuron0x8ce3498()*-0.262345);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaf68() {
   return (neuron0x8ce3818()*0.372019);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceaf90() {
   return (neuron0x8ce3b98()*0.193008);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb638() {
   return (neuron0x8ce3f18()*-0.704179);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb660() {
   return (neuron0x8ce4298()*0.109402);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb688() {
   return (neuron0x8ce4618()*-0.470074);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb6b0() {
   return (neuron0x8ce4998()*-0.399339);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb6d8() {
   return (neuron0x8ce4d18()*-0.346361);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb700() {
   return (neuron0x8ce5098()*0.713972);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb728() {
   return (neuron0x8ce5418()*-0.388501);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb750() {
   return (neuron0x8ce5798()*-0.237697);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb778() {
   return (neuron0x8ce5b18()*0.76978);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb7a0() {
   return (neuron0x8ce5e98()*0.725007);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb7c8() {
   return (neuron0x8ce6218()*-0.0552029);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb7f0() {
   return (neuron0x8ce6598()*-0.129579);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb818() {
   return (neuron0x8ce6918()*-0.372111);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb840() {
   return (neuron0x8ce6c98()*0.326497);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb868() {
   return (neuron0x8ce7018()*-0.0882556);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb890() {
   return (neuron0x8ce7398()*0.122109);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb8b8() {
   return (neuron0x8ce7718()*0.444555);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb8e0() {
   return (neuron0x8ce7a98()*-0.399049);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb908() {
   return (neuron0x8ce7e18()*-0.632018);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb930() {
   return (neuron0x8ce8198()*0.33804);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb958() {
   return (neuron0x8ce8518()*1.41714);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb980() {
   return (neuron0x8ce8898()*-0.368804);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb9a8() {
   return (neuron0x8ce8c18()*0.19164);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb9d0() {
   return (neuron0x8ce8f98()*0.469666);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceb9f8() {
   return (neuron0x8ce9318()*-0.400651);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceba20() {
   return (neuron0x8ce9698()*-0.0256448);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceba48() {
   return (neuron0x8ce9a18()*-0.460087);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceba70() {
   return (neuron0x8ce9d98()*-0.297566);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8ceba98() {
   return (neuron0x8cea118()*-0.797731);
}

double NNPerpGammaXYCorrectionClusterDeltaX::synapse0x8cebac0() {
   return (neuron0x8cea498()*-0.20323);
}

