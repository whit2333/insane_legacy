#include "NNPerpElectronAngleCorrectionDeltaTheta.h"
#include <cmath>

double NNPerpElectronAngleCorrectionDeltaTheta::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - 0.703042)/0.0942299;
   input1 = (in1 - -0.0193889)/0.272452;
   input2 = (in2 - -4.54411)/32.4866;
   input3 = (in3 - -4.42958)/59.3589;
   input4 = (in4 - 2147.14)/963.804;
   input5 = (in5 - 0.00290412)/0.75057;
   input6 = (in6 - 0.00347513)/0.749626;
   switch(index) {
     case 0:
         return neuron0x8c2ef10();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionDeltaTheta::Value(int index, double* input) {
   input0 = (input[0] - 0.703042)/0.0942299;
   input1 = (input[1] - -0.0193889)/0.272452;
   input2 = (input[2] - -4.54411)/32.4866;
   input3 = (input[3] - -4.42958)/59.3589;
   input4 = (input[4] - 2147.14)/963.804;
   input5 = (input[5] - 0.00290412)/0.75057;
   input6 = (input[6] - 0.00347513)/0.749626;
   switch(index) {
     case 0:
         return neuron0x8c2ef10();
     default:
         return 0.;
   }
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c08330() {
   return input0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c16988() {
   return input1;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c16ab0() {
   return input2;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c16bd8() {
   return input3;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c16d90() {
   return input4;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c16f90() {
   return input5;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c17190() {
   return input6;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c174d0() {
   double input = -0.448157;
   input += synapse0x8b9b670();
   input += synapse0x8b9b698();
   input += synapse0x8b2cb70();
   input += synapse0x8b9b648();
   input += synapse0x8c17688();
   input += synapse0x8c176b0();
   input += synapse0x8c176d8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c174d0() {
   double input = input0x8c174d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c17700() {
   double input = -0.341337;
   input += synapse0x8c17900();
   input += synapse0x8c17928();
   input += synapse0x8c17950();
   input += synapse0x8c17978();
   input += synapse0x8c179a0();
   input += synapse0x8c179c8();
   input += synapse0x8c179f0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c17700() {
   double input = input0x8c17700();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c17a18() {
   double input = -0.224849;
   input += synapse0x8c17c18();
   input += synapse0x8c17c40();
   input += synapse0x8c17c68();
   input += synapse0x8c17d18();
   input += synapse0x8c17d40();
   input += synapse0x8c17d68();
   input += synapse0x8c17d90();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c17a18() {
   double input = input0x8c17a18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c17db8() {
   double input = 0.0811896;
   input += synapse0x8c17f70();
   input += synapse0x8c17f98();
   input += synapse0x8c17fc0();
   input += synapse0x8c17fe8();
   input += synapse0x8c18010();
   input += synapse0x8c18038();
   input += synapse0x8c18060();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c17db8() {
   double input = input0x8c17db8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c18088() {
   double input = -0.112098;
   input += synapse0x8c182a0();
   input += synapse0x8c182c8();
   input += synapse0x8c182f0();
   input += synapse0x8c18318();
   input += synapse0x8c18340();
   input += synapse0x8c17c90();
   input += synapse0x8c17cb8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c18088() {
   double input = input0x8c18088();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c18470() {
   double input = -0.121473;
   input += synapse0x8c18670();
   input += synapse0x8c18698();
   input += synapse0x8c186c0();
   input += synapse0x8c186e8();
   input += synapse0x8c18710();
   input += synapse0x8c18738();
   input += synapse0x8c18760();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c18470() {
   double input = input0x8c18470();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c18788() {
   double input = 0.222996;
   input += synapse0x8c18988();
   input += synapse0x8c189b0();
   input += synapse0x8c189d8();
   input += synapse0x8c18a00();
   input += synapse0x8c18a28();
   input += synapse0x8c18a50();
   input += synapse0x8c18a78();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c18788() {
   double input = input0x8c18788();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c18aa0() {
   double input = -0.258024;
   input += synapse0x8c18cb8();
   input += synapse0x8c18ce0();
   input += synapse0x8c18d08();
   input += synapse0x8c18d30();
   input += synapse0x8c18d58();
   input += synapse0x8c18d80();
   input += synapse0x8c18da8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c18aa0() {
   double input = input0x8c18aa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c18dd0() {
   double input = -0.416119;
   input += synapse0x8c18fe8();
   input += synapse0x8c19010();
   input += synapse0x8c19038();
   input += synapse0x8c19060();
   input += synapse0x8c19088();
   input += synapse0x8c190b0();
   input += synapse0x8c190d8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c18dd0() {
   double input = input0x8c18dd0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c19100() {
   double input = -0.520093;
   input += synapse0x8c193a0();
   input += synapse0x8c193c8();
   input += synapse0x8b9b5c8();
   input += synapse0x8b2cb98();
   input += synapse0x8b31ad8();
   input += synapse0x8c18368();
   input += synapse0x8c18390();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c19100() {
   double input = input0x8c19100();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c195f8() {
   double input = -0.0253333;
   input += synapse0x8c18448();
   input += synapse0x8c19768();
   input += synapse0x8c19790();
   input += synapse0x8c197b8();
   input += synapse0x8c197e0();
   input += synapse0x8c19808();
   input += synapse0x8c19830();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c195f8() {
   double input = input0x8c195f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c19858() {
   double input = -0.332284;
   input += synapse0x8c19a58();
   input += synapse0x8c19a80();
   input += synapse0x8c19aa8();
   input += synapse0x8c19ad0();
   input += synapse0x8c19af8();
   input += synapse0x8c19b20();
   input += synapse0x8c19b48();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c19858() {
   double input = input0x8c19858();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c19b70() {
   double input = 0.0405221;
   input += synapse0x8c19d70();
   input += synapse0x8c19d98();
   input += synapse0x8c19dc0();
   input += synapse0x8c19de8();
   input += synapse0x8c19e10();
   input += synapse0x8c19e38();
   input += synapse0x8c19e60();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c19b70() {
   double input = input0x8c19b70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c19e88() {
   double input = -0.151964;
   input += synapse0x8c1a088();
   input += synapse0x8c1a0b0();
   input += synapse0x8c1a0d8();
   input += synapse0x8c1a100();
   input += synapse0x8c1a128();
   input += synapse0x8c1a150();
   input += synapse0x8c1a178();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c19e88() {
   double input = input0x8c19e88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1a1a0() {
   double input = -0.220003;
   input += synapse0x8c1a3a0();
   input += synapse0x8c1a3c8();
   input += synapse0x8c1a3f0();
   input += synapse0x8c1a418();
   input += synapse0x8c1a440();
   input += synapse0x8c1a468();
   input += synapse0x8c1a490();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1a1a0() {
   double input = input0x8c1a1a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1a4b8() {
   double input = 0.314406;
   input += synapse0x8c1a6b8();
   input += synapse0x8c1a768();
   input += synapse0x8c1a818();
   input += synapse0x8c1a8c8();
   input += synapse0x8c1a978();
   input += synapse0x8c1aa28();
   input += synapse0x8c1aad8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1a4b8() {
   double input = input0x8c1a4b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1ab88() {
   double input = -0.258611;
   input += synapse0x8c1acb0();
   input += synapse0x8c1acd8();
   input += synapse0x8c1ad00();
   input += synapse0x8c1ad28();
   input += synapse0x8c1ad50();
   input += synapse0x8c1ad78();
   input += synapse0x8c1ada0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1ab88() {
   double input = input0x8c1ab88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1adc8() {
   double input = 0.306862;
   input += synapse0x8c1aef0();
   input += synapse0x8c1af18();
   input += synapse0x8c1af40();
   input += synapse0x8c1af68();
   input += synapse0x8c1af90();
   input += synapse0x8c1afb8();
   input += synapse0x8c1afe0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1adc8() {
   double input = input0x8c1adc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1b008() {
   double input = -0.398297;
   input += synapse0x8c1b1c0();
   input += synapse0x8c1b1e8();
   input += synapse0x8c1b210();
   input += synapse0x8c193f0();
   input += synapse0x8c19418();
   input += synapse0x8c19440();
   input += synapse0x8c19468();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1b008() {
   double input = input0x8c1b008();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c19490() {
   double input = 0.28863;
   input += synapse0x8c195d0();
   input += synapse0x8c1b718();
   input += synapse0x8c1b740();
   input += synapse0x8c1b768();
   input += synapse0x8c1b790();
   input += synapse0x8c1b7b8();
   input += synapse0x8c1b7e0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c19490() {
   double input = input0x8c19490();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1b808() {
   double input = -0.00566516;
   input += synapse0x8c1ba08();
   input += synapse0x8c1ba30();
   input += synapse0x8c1ba58();
   input += synapse0x8c1ba80();
   input += synapse0x8c1baa8();
   input += synapse0x8c1bad0();
   input += synapse0x8c1baf8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1b808() {
   double input = input0x8c1b808();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1bb20() {
   double input = 0.164481;
   input += synapse0x8c1bd20();
   input += synapse0x8c1bd48();
   input += synapse0x8c1bd70();
   input += synapse0x8c1bd98();
   input += synapse0x8c1bdc0();
   input += synapse0x8c1bde8();
   input += synapse0x8c1be10();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1bb20() {
   double input = input0x8c1bb20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1be38() {
   double input = 0.114331;
   input += synapse0x8c1c050();
   input += synapse0x8c1c078();
   input += synapse0x8c1c0a0();
   input += synapse0x8c1c0c8();
   input += synapse0x8c1c0f0();
   input += synapse0x8c1c118();
   input += synapse0x8c1c140();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1be38() {
   double input = input0x8c1be38();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1c168() {
   double input = 0.0995882;
   input += synapse0x8c1c380();
   input += synapse0x8c1c3a8();
   input += synapse0x8c1c3d0();
   input += synapse0x8c1c3f8();
   input += synapse0x8c1c420();
   input += synapse0x8c1c448();
   input += synapse0x8c1c470();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1c168() {
   double input = input0x8c1c168();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1c498() {
   double input = -0.23008;
   input += synapse0x8c1c6b0();
   input += synapse0x8c1c6d8();
   input += synapse0x8c1c700();
   input += synapse0x8c1c728();
   input += synapse0x8c1c750();
   input += synapse0x8c1c778();
   input += synapse0x8c1c7a0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1c498() {
   double input = input0x8c1c498();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1c7c8() {
   double input = 0.507409;
   input += synapse0x8c19318();
   input += synapse0x8c19340();
   input += synapse0x8c19368();
   input += synapse0x8c1cae8();
   input += synapse0x8c1cb10();
   input += synapse0x8c1cb38();
   input += synapse0x8c1cb60();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1c7c8() {
   double input = input0x8c1c7c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1cb88() {
   double input = 0.353576;
   input += synapse0x8c1cda0();
   input += synapse0x8c1cdc8();
   input += synapse0x8c1cdf0();
   input += synapse0x8c1ce18();
   input += synapse0x8c1ce40();
   input += synapse0x8c1ce68();
   input += synapse0x8c1ce90();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1cb88() {
   double input = input0x8c1cb88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1ceb8() {
   double input = -0.459126;
   input += synapse0x8c1d0d0();
   input += synapse0x8c1d0f8();
   input += synapse0x8c1d120();
   input += synapse0x8c1d148();
   input += synapse0x8c1d170();
   input += synapse0x8c1d198();
   input += synapse0x8c1d1c0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1ceb8() {
   double input = input0x8c1ceb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1d1e8() {
   double input = 0.512069;
   input += synapse0x8c1d400();
   input += synapse0x8c1d428();
   input += synapse0x8c1d450();
   input += synapse0x8c1d478();
   input += synapse0x8c1d4a0();
   input += synapse0x8c1d4c8();
   input += synapse0x8c1d4f0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1d1e8() {
   double input = input0x8c1d1e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1d518() {
   double input = 0.169963;
   input += synapse0x8c1d730();
   input += synapse0x8c1d758();
   input += synapse0x8c1d780();
   input += synapse0x8c1d7a8();
   input += synapse0x8c1d7d0();
   input += synapse0x8c1d7f8();
   input += synapse0x8c1d820();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1d518() {
   double input = input0x8c1d518();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1d848() {
   double input = -0.354818;
   input += synapse0x8c1da60();
   input += synapse0x8c1da88();
   input += synapse0x8c1dab0();
   input += synapse0x8c1dad8();
   input += synapse0x8c1db00();
   input += synapse0x8c1db28();
   input += synapse0x8c1db50();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1d848() {
   double input = input0x8c1d848();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1db78() {
   double input = -0.477021;
   input += synapse0x8c1dd90();
   input += synapse0x8c1a6e0();
   input += synapse0x8c1a708();
   input += synapse0x8c1a730();
   input += synapse0x8c1a790();
   input += synapse0x8c1a7b8();
   input += synapse0x8c1a7e0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1db78() {
   double input = input0x8c1db78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1e4f0() {
   double input = -0.127296;
   input += synapse0x8c1a8a0();
   input += synapse0x8c1a9e8();
   input += synapse0x8c1a938();
   input += synapse0x8c1aa50();
   input += synapse0x8c1aa78();
   input += synapse0x8c1aaa0();
   input += synapse0x8c1ab00();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1e4f0() {
   double input = input0x8c1e4f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1e618() {
   double input = -0.0428899;
   input += synapse0x8c1e7d0();
   input += synapse0x8c1e7f8();
   input += synapse0x8c1e820();
   input += synapse0x8c1e848();
   input += synapse0x8c1e870();
   input += synapse0x8c1e898();
   input += synapse0x8c1e8c0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1e618() {
   double input = input0x8c1e618();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1e8e8() {
   double input = 0.125879;
   input += synapse0x8c1eae8();
   input += synapse0x8c1eb10();
   input += synapse0x8c1eb38();
   input += synapse0x8c1eb60();
   input += synapse0x8c1eb88();
   input += synapse0x8c1ebb0();
   input += synapse0x8c1ebd8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1e8e8() {
   double input = input0x8c1e8e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1ec00() {
   double input = -0.25806;
   input += synapse0x8c1ee00();
   input += synapse0x8c1ee28();
   input += synapse0x8c1ee50();
   input += synapse0x8c1ee78();
   input += synapse0x8c1eea0();
   input += synapse0x8c1eec8();
   input += synapse0x8c1eef0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1ec00() {
   double input = input0x8c1ec00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1ef18() {
   double input = 0.329767;
   input += synapse0x8c1f130();
   input += synapse0x8c1f158();
   input += synapse0x8c1f180();
   input += synapse0x8c1f1a8();
   input += synapse0x8c1f1d0();
   input += synapse0x8c1b238();
   input += synapse0x8c1b260();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1ef18() {
   double input = input0x8c1ef18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1b288() {
   double input = 0.0251025;
   input += synapse0x8c1b4a0();
   input += synapse0x8c1b4c8();
   input += synapse0x8c1b4f0();
   input += synapse0x8c1b518();
   input += synapse0x8c1b540();
   input += synapse0x8c1b568();
   input += synapse0x8c1b590();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1b288() {
   double input = input0x8c1b288();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1fa00() {
   double input = 0.221193;
   input += synapse0x8c1b618();
   input += synapse0x8c1fbb8();
   input += synapse0x8c1fbe0();
   input += synapse0x8c1fc08();
   input += synapse0x8c1fc30();
   input += synapse0x8c1fc58();
   input += synapse0x8c1fc80();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1fa00() {
   double input = input0x8c1fa00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1fca8() {
   double input = -0.109277;
   input += synapse0x8c1fec0();
   input += synapse0x8c1fee8();
   input += synapse0x8c1ff10();
   input += synapse0x8c1ff38();
   input += synapse0x8c1ff60();
   input += synapse0x8c1ff88();
   input += synapse0x8c1ffb0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1fca8() {
   double input = input0x8c1fca8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1ffd8() {
   double input = 0.047152;
   input += synapse0x8c201f0();
   input += synapse0x8c20218();
   input += synapse0x8c20240();
   input += synapse0x8c20268();
   input += synapse0x8c20290();
   input += synapse0x8c202b8();
   input += synapse0x8c202e0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1ffd8() {
   double input = input0x8c1ffd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c20308() {
   double input = -0.133631;
   input += synapse0x8c20520();
   input += synapse0x8c20548();
   input += synapse0x8c20570();
   input += synapse0x8c20598();
   input += synapse0x8c205c0();
   input += synapse0x8c205e8();
   input += synapse0x8c20610();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c20308() {
   double input = input0x8c20308();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c20638() {
   double input = -0.532521;
   input += synapse0x8c20850();
   input += synapse0x8c20878();
   input += synapse0x8c208a0();
   input += synapse0x8c208c8();
   input += synapse0x8c208f0();
   input += synapse0x8c20918();
   input += synapse0x8c20940();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c20638() {
   double input = input0x8c20638();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c20968() {
   double input = -0.143837;
   input += synapse0x8c20b80();
   input += synapse0x8c20ba8();
   input += synapse0x8c20bd0();
   input += synapse0x8c20bf8();
   input += synapse0x8c20c20();
   input += synapse0x8c20c48();
   input += synapse0x8c20c70();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c20968() {
   double input = input0x8c20968();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c20c98() {
   double input = 0.420183;
   input += synapse0x8c20eb0();
   input += synapse0x8c20ed8();
   input += synapse0x8c20f00();
   input += synapse0x8c20f28();
   input += synapse0x8c20f50();
   input += synapse0x8c20f78();
   input += synapse0x8c20fa0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c20c98() {
   double input = input0x8c20c98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c20fc8() {
   double input = -0.371804;
   input += synapse0x8c211e0();
   input += synapse0x8c21208();
   input += synapse0x8c21230();
   input += synapse0x8c21258();
   input += synapse0x8c21280();
   input += synapse0x8c212a8();
   input += synapse0x8c212d0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c20fc8() {
   double input = input0x8c20fc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c212f8() {
   double input = 0.0803493;
   input += synapse0x8c21510();
   input += synapse0x8c21538();
   input += synapse0x8c21560();
   input += synapse0x8c21588();
   input += synapse0x8c215b0();
   input += synapse0x8c215d8();
   input += synapse0x8c21600();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c212f8() {
   double input = input0x8c212f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c21628() {
   double input = 0.257396;
   input += synapse0x8c21840();
   input += synapse0x8c21868();
   input += synapse0x8c21890();
   input += synapse0x8c218b8();
   input += synapse0x8c218e0();
   input += synapse0x8c21908();
   input += synapse0x8c21930();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c21628() {
   double input = input0x8c21628();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c21958() {
   double input = 0.185251;
   input += synapse0x8c21b70();
   input += synapse0x8c21b98();
   input += synapse0x8c21bc0();
   input += synapse0x8c21be8();
   input += synapse0x8c21c10();
   input += synapse0x8c21c38();
   input += synapse0x8c21c60();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c21958() {
   double input = input0x8c21958();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c21c88() {
   double input = -0.314399;
   input += synapse0x8c21ea0();
   input += synapse0x8c21ec8();
   input += synapse0x8c21ef0();
   input += synapse0x8c21f18();
   input += synapse0x8c21f40();
   input += synapse0x8c21f68();
   input += synapse0x8c21f90();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c21c88() {
   double input = input0x8c21c88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c21fb8() {
   double input = -0.385611;
   input += synapse0x8c221d0();
   input += synapse0x8c221f8();
   input += synapse0x8c22220();
   input += synapse0x8c22248();
   input += synapse0x8c22270();
   input += synapse0x8c22298();
   input += synapse0x8c222c0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c21fb8() {
   double input = input0x8c21fb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c222e8() {
   double input = 0.0255453;
   input += synapse0x8c22500();
   input += synapse0x8c22528();
   input += synapse0x8c22550();
   input += synapse0x8c22578();
   input += synapse0x8c225a0();
   input += synapse0x8c225c8();
   input += synapse0x8c225f0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c222e8() {
   double input = input0x8c222e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c22618() {
   double input = 0.275879;
   input += synapse0x8c22830();
   input += synapse0x8c22858();
   input += synapse0x8c22880();
   input += synapse0x8c228a8();
   input += synapse0x8c228d0();
   input += synapse0x8c228f8();
   input += synapse0x8c22920();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c22618() {
   double input = input0x8c22618();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c22948() {
   double input = 0.261533;
   input += synapse0x8c22b60();
   input += synapse0x8c22b88();
   input += synapse0x8c22bb0();
   input += synapse0x8c22bd8();
   input += synapse0x8c22c00();
   input += synapse0x8c22c28();
   input += synapse0x8c22c50();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c22948() {
   double input = input0x8c22948();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c22c78() {
   double input = 0.285975;
   input += synapse0x8c22e90();
   input += synapse0x8c22eb8();
   input += synapse0x8c22ee0();
   input += synapse0x8c22f08();
   input += synapse0x8c22f30();
   input += synapse0x8c22f58();
   input += synapse0x8c22f80();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c22c78() {
   double input = input0x8c22c78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c22fa8() {
   double input = 0.0969061;
   input += synapse0x8c231c0();
   input += synapse0x8c231e8();
   input += synapse0x8c23210();
   input += synapse0x8c23238();
   input += synapse0x8c23260();
   input += synapse0x8c23288();
   input += synapse0x8c232b0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c22fa8() {
   double input = input0x8c22fa8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c232d8() {
   double input = 0.297296;
   input += synapse0x8c234f0();
   input += synapse0x8c23518();
   input += synapse0x8c23540();
   input += synapse0x8c23568();
   input += synapse0x8c23590();
   input += synapse0x8c235b8();
   input += synapse0x8c235e0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c232d8() {
   double input = input0x8c232d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c23608() {
   double input = 0.0192188;
   input += synapse0x8c1c9e0();
   input += synapse0x8c1ca08();
   input += synapse0x8c1ca30();
   input += synapse0x8c1ca58();
   input += synapse0x8c1ca80();
   input += synapse0x8c1caa8();
   input += synapse0x8c23a28();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c23608() {
   double input = input0x8c23608();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c23a50() {
   double input = -0.267709;
   input += synapse0x8c23c50();
   input += synapse0x8c23c78();
   input += synapse0x8c23ca0();
   input += synapse0x8c23cc8();
   input += synapse0x8c23cf0();
   input += synapse0x8c23d18();
   input += synapse0x8c23d40();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c23a50() {
   double input = input0x8c23a50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c23d68() {
   double input = 0.11493;
   input += synapse0x8c23f80();
   input += synapse0x8c23fa8();
   input += synapse0x8c23fd0();
   input += synapse0x8c23ff8();
   input += synapse0x8c24020();
   input += synapse0x8c24048();
   input += synapse0x8c24070();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c23d68() {
   double input = input0x8c23d68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c24098() {
   double input = -0.0165713;
   input += synapse0x8c242b0();
   input += synapse0x8c242d8();
   input += synapse0x8c24300();
   input += synapse0x8c24328();
   input += synapse0x8c24350();
   input += synapse0x8c24378();
   input += synapse0x8c243a0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c24098() {
   double input = input0x8c24098();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c243c8() {
   double input = -0.256308;
   input += synapse0x8c245e0();
   input += synapse0x8c24608();
   input += synapse0x8c24630();
   input += synapse0x8c24658();
   input += synapse0x8c24680();
   input += synapse0x8c246a8();
   input += synapse0x8c246d0();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c243c8() {
   double input = input0x8c243c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c246f8() {
   double input = -0.310716;
   input += synapse0x8c24910();
   input += synapse0x8c24938();
   input += synapse0x8c24960();
   input += synapse0x8c24988();
   input += synapse0x8c249b0();
   input += synapse0x8c249d8();
   input += synapse0x8c24a00();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c246f8() {
   double input = input0x8c246f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c24a28() {
   double input = -0.435447;
   input += synapse0x8c24c40();
   input += synapse0x8c1ddb8();
   input += synapse0x8c1dde0();
   input += synapse0x8c1de08();
   input += synapse0x8c1e038();
   input += synapse0x8c1e060();
   input += synapse0x8c1e290();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c24a28() {
   double input = input0x8c24a28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1e2b8() {
   double input = -0.0307845;
   input += synapse0x8c25690();
   input += synapse0x8c256b8();
   input += synapse0x8c256e0();
   input += synapse0x8c25708();
   input += synapse0x8c25730();
   input += synapse0x8c25758();
   input += synapse0x8c25780();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1e2b8() {
   double input = input0x8c1e2b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c257a8() {
   double input = 0.13667;
   input += synapse0x8c259a8();
   input += synapse0x8c259d0();
   input += synapse0x8c259f8();
   input += synapse0x8c25a20();
   input += synapse0x8c25a48();
   input += synapse0x8c25a70();
   input += synapse0x8c25a98();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c257a8() {
   double input = input0x8c257a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c25ac0() {
   double input = -0.0556352;
   input += synapse0x8c25cd8();
   input += synapse0x8c25d00();
   input += synapse0x8c25d28();
   input += synapse0x8c25d50();
   input += synapse0x8c25d78();
   input += synapse0x8c25da0();
   input += synapse0x8c25dc8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c25ac0() {
   double input = input0x8c25ac0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c25df0() {
   double input = 0.125215;
   input += synapse0x8c26008();
   input += synapse0x8c26030();
   input += synapse0x8c26058();
   input += synapse0x8c26080();
   input += synapse0x8c260a8();
   input += synapse0x8c260d0();
   input += synapse0x8c260f8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c25df0() {
   double input = input0x8c25df0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c26120() {
   double input = -0.175326;
   input += synapse0x8c26338();
   input += synapse0x8c26360();
   input += synapse0x8c26388();
   input += synapse0x8c263b0();
   input += synapse0x8c263d8();
   input += synapse0x8c26400();
   input += synapse0x8c26428();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c26120() {
   double input = input0x8c26120();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c26450() {
   double input = -0.358349;
   input += synapse0x8c26668();
   input += synapse0x8c26690();
   input += synapse0x8c266b8();
   input += synapse0x8c266e0();
   input += synapse0x8c26708();
   input += synapse0x8c26730();
   input += synapse0x8c26758();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c26450() {
   double input = input0x8c26450();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c26780() {
   double input = 0.123893;
   input += synapse0x8c26998();
   input += synapse0x8c269c0();
   input += synapse0x8c269e8();
   input += synapse0x8c26a10();
   input += synapse0x8c26a38();
   input += synapse0x8c26a60();
   input += synapse0x8c26a88();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c26780() {
   double input = input0x8c26780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c26ab0() {
   double input = -0.48653;
   input += synapse0x8c26cc8();
   input += synapse0x8c26cf0();
   input += synapse0x8c26d18();
   input += synapse0x8c26d40();
   input += synapse0x8c26d68();
   input += synapse0x8c26d90();
   input += synapse0x8c26db8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c26ab0() {
   double input = input0x8c26ab0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c26de0() {
   double input = 0.187314;
   input += synapse0x8c26ff8();
   input += synapse0x8c27020();
   input += synapse0x8c27048();
   input += synapse0x8c27070();
   input += synapse0x8c27098();
   input += synapse0x8c270c0();
   input += synapse0x8c270e8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c26de0() {
   double input = input0x8c26de0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c27110() {
   double input = 0.336312;
   input += synapse0x8c27328();
   input += synapse0x8c27350();
   input += synapse0x8c1f1f8();
   input += synapse0x8c1f220();
   input += synapse0x8c1f248();
   input += synapse0x8c1f270();
   input += synapse0x8c1f298();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c27110() {
   double input = input0x8c27110();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1f2c0() {
   double input = 0.242225;
   input += synapse0x8c1f4d8();
   input += synapse0x8c1f500();
   input += synapse0x8c1f528();
   input += synapse0x8c1f550();
   input += synapse0x8c1f578();
   input += synapse0x8c1f5a0();
   input += synapse0x8c1f5c8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1f2c0() {
   double input = input0x8c1f2c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c1f5f0() {
   double input = -0.171541;
   input += synapse0x8c1f808();
   input += synapse0x8c1f830();
   input += synapse0x8c1f858();
   input += synapse0x8c1f880();
   input += synapse0x8c1f8a8();
   input += synapse0x8c1f8d0();
   input += synapse0x8c1f8f8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c1f5f0() {
   double input = input0x8c1f5f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c28380() {
   double input = 0.046479;
   input += synapse0x8c1f9c8();
   input += synapse0x8c284f0();
   input += synapse0x8c28518();
   input += synapse0x8c28540();
   input += synapse0x8c28568();
   input += synapse0x8c28590();
   input += synapse0x8c285b8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c28380() {
   double input = input0x8c28380();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c285e0() {
   double input = -0.0456409;
   input += synapse0x8c287f8();
   input += synapse0x8c28820();
   input += synapse0x8c28848();
   input += synapse0x8c28870();
   input += synapse0x8c28898();
   input += synapse0x8c288c0();
   input += synapse0x8c288e8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c285e0() {
   double input = input0x8c285e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c28910() {
   double input = 0.391796;
   input += synapse0x8c28b28();
   input += synapse0x8c28b50();
   input += synapse0x8c28b78();
   input += synapse0x8c28ba0();
   input += synapse0x8c28bc8();
   input += synapse0x8c28bf0();
   input += synapse0x8c28c18();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c28910() {
   double input = input0x8c28910();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c28c40() {
   double input = -0.135384;
   input += synapse0x8c28e58();
   input += synapse0x8c28e80();
   input += synapse0x8c28ea8();
   input += synapse0x8c28ed0();
   input += synapse0x8c28ef8();
   input += synapse0x8c28f20();
   input += synapse0x8c28f48();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c28c40() {
   double input = input0x8c28c40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c28f70() {
   double input = -0.247908;
   input += synapse0x8c29188();
   input += synapse0x8c291b0();
   input += synapse0x8c291d8();
   input += synapse0x8c29200();
   input += synapse0x8c29228();
   input += synapse0x8c29250();
   input += synapse0x8c29278();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c28f70() {
   double input = input0x8c28f70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c292a0() {
   double input = 0.106371;
   input += synapse0x8c294b8();
   input += synapse0x8c294e0();
   input += synapse0x8c29508();
   input += synapse0x8c29530();
   input += synapse0x8c29558();
   input += synapse0x8c29580();
   input += synapse0x8c295a8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c292a0() {
   double input = input0x8c292a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c295d0() {
   double input = -0.0256574;
   input += synapse0x8c297e8();
   input += synapse0x8c29810();
   input += synapse0x8c29838();
   input += synapse0x8c29860();
   input += synapse0x8c29888();
   input += synapse0x8c298b0();
   input += synapse0x8c298d8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c295d0() {
   double input = input0x8c295d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c29900() {
   double input = -0.119068;
   input += synapse0x8c29b18();
   input += synapse0x8c29b40();
   input += synapse0x8c29b68();
   input += synapse0x8c29b90();
   input += synapse0x8c29bb8();
   input += synapse0x8c29be0();
   input += synapse0x8c29c08();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c29900() {
   double input = input0x8c29900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c29c30() {
   double input = -0.106063;
   input += synapse0x8c29e48();
   input += synapse0x8c29e70();
   input += synapse0x8c29e98();
   input += synapse0x8c29ec0();
   input += synapse0x8c29ee8();
   input += synapse0x8c29f10();
   input += synapse0x8c29f38();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c29c30() {
   double input = input0x8c29c30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c29f60() {
   double input = -0.244622;
   input += synapse0x8c2a178();
   input += synapse0x8c2a1a0();
   input += synapse0x8c2a1c8();
   input += synapse0x8c2a1f0();
   input += synapse0x8c2a218();
   input += synapse0x8c2a240();
   input += synapse0x8c2a268();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c29f60() {
   double input = input0x8c29f60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2a290() {
   double input = 0.49939;
   input += synapse0x8c2a4a8();
   input += synapse0x8c2a4d0();
   input += synapse0x8c2a4f8();
   input += synapse0x8c2a520();
   input += synapse0x8c2a548();
   input += synapse0x8c2a570();
   input += synapse0x8c2a598();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2a290() {
   double input = input0x8c2a290();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2a5c0() {
   double input = 0.210068;
   input += synapse0x8c2a7d8();
   input += synapse0x8c2a800();
   input += synapse0x8c2a828();
   input += synapse0x8c2a850();
   input += synapse0x8c2a878();
   input += synapse0x8c2a8a0();
   input += synapse0x8c2a8c8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2a5c0() {
   double input = input0x8c2a5c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2a8f0() {
   double input = 0.364394;
   input += synapse0x8c2ab08();
   input += synapse0x8c2ab30();
   input += synapse0x8c2ab58();
   input += synapse0x8c2ab80();
   input += synapse0x8c2aba8();
   input += synapse0x8c2abd0();
   input += synapse0x8c2abf8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2a8f0() {
   double input = input0x8c2a8f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2ac20() {
   double input = -0.417835;
   input += synapse0x8c2ae38();
   input += synapse0x8c2ae60();
   input += synapse0x8c2ae88();
   input += synapse0x8c2aeb0();
   input += synapse0x8c2aed8();
   input += synapse0x8c2af00();
   input += synapse0x8c2af28();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2ac20() {
   double input = input0x8c2ac20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2af50() {
   double input = -0.0122972;
   input += synapse0x8c2b168();
   input += synapse0x8c2b190();
   input += synapse0x8c2b1b8();
   input += synapse0x8c2b1e0();
   input += synapse0x8c2b208();
   input += synapse0x8c2b230();
   input += synapse0x8c2b258();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2af50() {
   double input = input0x8c2af50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2b280() {
   double input = 0.0458416;
   input += synapse0x8c2b498();
   input += synapse0x8c2b4c0();
   input += synapse0x8c2b4e8();
   input += synapse0x8c2b510();
   input += synapse0x8c2b538();
   input += synapse0x8c2b560();
   input += synapse0x8c2b588();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2b280() {
   double input = input0x8c2b280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2b5b0() {
   double input = -0.344742;
   input += synapse0x8c2b7c8();
   input += synapse0x8c2b7f0();
   input += synapse0x8c2b818();
   input += synapse0x8c2b840();
   input += synapse0x8c2b868();
   input += synapse0x8c2b890();
   input += synapse0x8c2b8b8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2b5b0() {
   double input = input0x8c2b5b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2b8e0() {
   double input = 0.279849;
   input += synapse0x8c2baf8();
   input += synapse0x8c2bb20();
   input += synapse0x8c2bb48();
   input += synapse0x8c2bb70();
   input += synapse0x8c2bb98();
   input += synapse0x8c2bbc0();
   input += synapse0x8c2bbe8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2b8e0() {
   double input = input0x8c2b8e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2bc10() {
   double input = 0.0177841;
   input += synapse0x8c2be28();
   input += synapse0x8c2be50();
   input += synapse0x8c2be78();
   input += synapse0x8c2bea0();
   input += synapse0x8c2bec8();
   input += synapse0x8c2bef0();
   input += synapse0x8c2bf18();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2bc10() {
   double input = input0x8c2bc10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2bf40() {
   double input = -0.0596337;
   input += synapse0x8c2c158();
   input += synapse0x8c2c180();
   input += synapse0x8c2c1a8();
   input += synapse0x8c2c1d0();
   input += synapse0x8c2c1f8();
   input += synapse0x8c2c220();
   input += synapse0x8c2c248();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2bf40() {
   double input = input0x8c2bf40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2c270() {
   double input = 0.0105148;
   input += synapse0x8c2c488();
   input += synapse0x8c2c4b0();
   input += synapse0x8c2c4d8();
   input += synapse0x8c2c500();
   input += synapse0x8c2c528();
   input += synapse0x8c2c550();
   input += synapse0x8c2c578();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2c270() {
   double input = input0x8c2c270();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2c5a0() {
   double input = -0.396247;
   input += synapse0x8c2c7b8();
   input += synapse0x8c2c7e0();
   input += synapse0x8c2c808();
   input += synapse0x8c2c830();
   input += synapse0x8c2c858();
   input += synapse0x8c2c880();
   input += synapse0x8c2c8a8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2c5a0() {
   double input = input0x8c2c5a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2c8d0() {
   double input = -0.390172;
   input += synapse0x8c2cae8();
   input += synapse0x8c2cb10();
   input += synapse0x8c2cb38();
   input += synapse0x8c2cb60();
   input += synapse0x8c2cb88();
   input += synapse0x8c2cbb0();
   input += synapse0x8c2cbd8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2c8d0() {
   double input = input0x8c2c8d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2cc00() {
   double input = 0.0023511;
   input += synapse0x8c2ce18();
   input += synapse0x8c2ce40();
   input += synapse0x8c2ce68();
   input += synapse0x8c2ce90();
   input += synapse0x8c2ceb8();
   input += synapse0x8c2cee0();
   input += synapse0x8c2cf08();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2cc00() {
   double input = input0x8c2cc00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2cf30() {
   double input = 0.0357217;
   input += synapse0x8c2d148();
   input += synapse0x8c2d170();
   input += synapse0x8c2d198();
   input += synapse0x8c2d1c0();
   input += synapse0x8c2d1e8();
   input += synapse0x8c2d210();
   input += synapse0x8c2d238();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2cf30() {
   double input = input0x8c2cf30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2d260() {
   double input = -0.198644;
   input += synapse0x8c2d478();
   input += synapse0x8c2d4a0();
   input += synapse0x8c2d4c8();
   input += synapse0x8c2d4f0();
   input += synapse0x8c2d518();
   input += synapse0x8c2d540();
   input += synapse0x8c2d568();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2d260() {
   double input = input0x8c2d260();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2d590() {
   double input = -0.416922;
   input += synapse0x8c2d7a8();
   input += synapse0x8c2d7d0();
   input += synapse0x8c2d7f8();
   input += synapse0x8c2d820();
   input += synapse0x8c2d848();
   input += synapse0x8c2d870();
   input += synapse0x8c2d898();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2d590() {
   double input = input0x8c2d590();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2d8c0() {
   double input = -0.392368;
   input += synapse0x8c2dad8();
   input += synapse0x8c2db00();
   input += synapse0x8c2db28();
   input += synapse0x8c2db50();
   input += synapse0x8c2db78();
   input += synapse0x8c2dba0();
   input += synapse0x8c2dbc8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2d8c0() {
   double input = input0x8c2d8c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2dbf0() {
   double input = -0.331608;
   input += synapse0x8c2de08();
   input += synapse0x8c2de30();
   input += synapse0x8c2de58();
   input += synapse0x8c2de80();
   input += synapse0x8c2dea8();
   input += synapse0x8c2ded0();
   input += synapse0x8c2def8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2dbf0() {
   double input = input0x8c2dbf0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2df20() {
   double input = -0.442975;
   input += synapse0x8c2e138();
   input += synapse0x8c2e160();
   input += synapse0x8c2e188();
   input += synapse0x8c2e1b0();
   input += synapse0x8c2e1d8();
   input += synapse0x8c2e200();
   input += synapse0x8c2e228();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2df20() {
   double input = input0x8c2df20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2e250() {
   double input = 0.180494;
   input += synapse0x8c2e468();
   input += synapse0x8c2e490();
   input += synapse0x8c2e4b8();
   input += synapse0x8c2e4e0();
   input += synapse0x8c2e508();
   input += synapse0x8c2e530();
   input += synapse0x8c2e558();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2e250() {
   double input = input0x8c2e250();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2e580() {
   double input = -0.438312;
   input += synapse0x8c2e798();
   input += synapse0x8c2e7c0();
   input += synapse0x8c2e7e8();
   input += synapse0x8c2e810();
   input += synapse0x8c2e838();
   input += synapse0x8c2e860();
   input += synapse0x8c2e888();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2e580() {
   double input = input0x8c2e580();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2e8b0() {
   double input = 0.0929062;
   input += synapse0x8c2eac8();
   input += synapse0x8c2eaf0();
   input += synapse0x8c2eb18();
   input += synapse0x8c2eb40();
   input += synapse0x8c2eb68();
   input += synapse0x8c2eb90();
   input += synapse0x8c2ebb8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2e8b0() {
   double input = input0x8c2e8b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2ebe0() {
   double input = -0.171355;
   input += synapse0x8c2edf8();
   input += synapse0x8c2ee20();
   input += synapse0x8c2ee48();
   input += synapse0x8c2ee70();
   input += synapse0x8c2ee98();
   input += synapse0x8c2eec0();
   input += synapse0x8c2eee8();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2ebe0() {
   double input = input0x8c2ebe0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::input0x8c2ef10() {
   double input = 0.160806;
   input += synapse0x8c2f038();
   input += synapse0x8c2f060();
   input += synapse0x8c2f088();
   input += synapse0x8c2f0b0();
   input += synapse0x8c2f0d8();
   input += synapse0x8c2f100();
   input += synapse0x8c2f128();
   input += synapse0x8c2f150();
   input += synapse0x8c2f178();
   input += synapse0x8c2f1a0();
   input += synapse0x8c2f1c8();
   input += synapse0x8c2f1f0();
   input += synapse0x8c2f218();
   input += synapse0x8c2f240();
   input += synapse0x8c2f268();
   input += synapse0x8c2f290();
   input += synapse0x8c2f340();
   input += synapse0x8c2f368();
   input += synapse0x8c2f390();
   input += synapse0x8c2f3b8();
   input += synapse0x8c2f3e0();
   input += synapse0x8c2f408();
   input += synapse0x8c2f430();
   input += synapse0x8c2f458();
   input += synapse0x8c2f480();
   input += synapse0x8c2f4a8();
   input += synapse0x8c2f4d0();
   input += synapse0x8c2f4f8();
   input += synapse0x8c2f520();
   input += synapse0x8c2f548();
   input += synapse0x8c2f570();
   input += synapse0x8c2f598();
   input += synapse0x8c2f2b8();
   input += synapse0x8c2f2e0();
   input += synapse0x8c2f308();
   input += synapse0x8c2f6c8();
   input += synapse0x8c2f6f0();
   input += synapse0x8c2f718();
   input += synapse0x8c2f740();
   input += synapse0x8c2f768();
   input += synapse0x8c2f790();
   input += synapse0x8c2f7b8();
   input += synapse0x8c2f7e0();
   input += synapse0x8c2f808();
   input += synapse0x8c2f830();
   input += synapse0x8c2f858();
   input += synapse0x8c2f880();
   input += synapse0x8c2f8a8();
   input += synapse0x8c2f8d0();
   input += synapse0x8c2f8f8();
   input += synapse0x8c2f920();
   input += synapse0x8c2f948();
   input += synapse0x8c2f970();
   input += synapse0x8c2f998();
   input += synapse0x8c2f9c0();
   input += synapse0x8c2f9e8();
   input += synapse0x8c2fa10();
   input += synapse0x8c2fa38();
   input += synapse0x8c2fa60();
   input += synapse0x8c2fa88();
   input += synapse0x8c2fab0();
   input += synapse0x8c2fad8();
   input += synapse0x8c2fb00();
   input += synapse0x8c2fb28();
   input += synapse0x8c173e8();
   input += synapse0x8c2f5c0();
   input += synapse0x8c2f5e8();
   input += synapse0x8c2f610();
   input += synapse0x8c2f638();
   input += synapse0x8c2f660();
   input += synapse0x8c2f688();
   input += synapse0x8c2fd58();
   input += synapse0x8c2fd80();
   input += synapse0x8c2fda8();
   input += synapse0x8c2fdd0();
   input += synapse0x8c2fdf8();
   input += synapse0x8c2fe20();
   input += synapse0x8c2fe48();
   input += synapse0x8c2fe70();
   input += synapse0x8c2fe98();
   input += synapse0x8c2fec0();
   input += synapse0x8c2fee8();
   input += synapse0x8c2ff10();
   input += synapse0x8c2ff38();
   input += synapse0x8c2ff60();
   input += synapse0x8c2ff88();
   input += synapse0x8c2ffb0();
   input += synapse0x8c2ffd8();
   input += synapse0x8c30000();
   input += synapse0x8c30028();
   input += synapse0x8c30050();
   input += synapse0x8c30078();
   input += synapse0x8c300a0();
   input += synapse0x8c300c8();
   input += synapse0x8c300f0();
   input += synapse0x8c30118();
   input += synapse0x8c30140();
   input += synapse0x8c30168();
   input += synapse0x8c30190();
   input += synapse0x8c301b8();
   input += synapse0x8c301e0();
   input += synapse0x8c30208();
   input += synapse0x8c30230();
   input += synapse0x8c30258();
   input += synapse0x8c30280();
   input += synapse0x8c302a8();
   input += synapse0x8c302d0();
   input += synapse0x8c302f8();
   input += synapse0x8c30320();
   input += synapse0x8c30348();
   return input;
}

double NNPerpElectronAngleCorrectionDeltaTheta::neuron0x8c2ef10() {
   double input = input0x8c2ef10();
   return (input * 1)+0;
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8b9b670() {
   return (neuron0x8c08330()*0.482206);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8b9b698() {
   return (neuron0x8c16988()*0.213272);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8b2cb70() {
   return (neuron0x8c16ab0()*0.0141186);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8b9b648() {
   return (neuron0x8c16bd8()*0.467968);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17688() {
   return (neuron0x8c16d90()*-0.218598);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c176b0() {
   return (neuron0x8c16f90()*-0.230871);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c176d8() {
   return (neuron0x8c17190()*0.469761);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17900() {
   return (neuron0x8c08330()*-0.18214);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17928() {
   return (neuron0x8c16988()*-0.305983);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17950() {
   return (neuron0x8c16ab0()*0.0295414);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17978() {
   return (neuron0x8c16bd8()*0.26663);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c179a0() {
   return (neuron0x8c16d90()*0.311218);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c179c8() {
   return (neuron0x8c16f90()*0.167349);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c179f0() {
   return (neuron0x8c17190()*0.17516);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17c18() {
   return (neuron0x8c08330()*-0.429371);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17c40() {
   return (neuron0x8c16988()*0.342825);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17c68() {
   return (neuron0x8c16ab0()*0.307284);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17d18() {
   return (neuron0x8c16bd8()*0.388638);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17d40() {
   return (neuron0x8c16d90()*0.0949514);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17d68() {
   return (neuron0x8c16f90()*0.378074);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17d90() {
   return (neuron0x8c17190()*0.319956);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17f70() {
   return (neuron0x8c08330()*-0.105114);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17f98() {
   return (neuron0x8c16988()*0.212115);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17fc0() {
   return (neuron0x8c16ab0()*0.27106);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17fe8() {
   return (neuron0x8c16bd8()*-0.035936);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18010() {
   return (neuron0x8c16d90()*0.141279);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18038() {
   return (neuron0x8c16f90()*-0.0446075);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18060() {
   return (neuron0x8c17190()*-0.387276);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c182a0() {
   return (neuron0x8c08330()*-0.33104);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c182c8() {
   return (neuron0x8c16988()*-0.00483071);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c182f0() {
   return (neuron0x8c16ab0()*0.223239);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18318() {
   return (neuron0x8c16bd8()*0.323312);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18340() {
   return (neuron0x8c16d90()*-0.42379);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17c90() {
   return (neuron0x8c16f90()*-0.0997058);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c17cb8() {
   return (neuron0x8c17190()*-0.104752);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18670() {
   return (neuron0x8c08330()*0.311681);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18698() {
   return (neuron0x8c16988()*0.13269);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c186c0() {
   return (neuron0x8c16ab0()*0.126833);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c186e8() {
   return (neuron0x8c16bd8()*0.182305);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18710() {
   return (neuron0x8c16d90()*-0.429142);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18738() {
   return (neuron0x8c16f90()*0.379645);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18760() {
   return (neuron0x8c17190()*-0.150719);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18988() {
   return (neuron0x8c08330()*-0.0120562);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c189b0() {
   return (neuron0x8c16988()*0.107565);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c189d8() {
   return (neuron0x8c16ab0()*-0.303106);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18a00() {
   return (neuron0x8c16bd8()*-0.398349);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18a28() {
   return (neuron0x8c16d90()*-0.146022);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18a50() {
   return (neuron0x8c16f90()*-0.110064);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18a78() {
   return (neuron0x8c17190()*0.0288773);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18cb8() {
   return (neuron0x8c08330()*-0.0251984);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18ce0() {
   return (neuron0x8c16988()*-0.221368);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18d08() {
   return (neuron0x8c16ab0()*0.353201);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18d30() {
   return (neuron0x8c16bd8()*-0.224076);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18d58() {
   return (neuron0x8c16d90()*0.26592);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18d80() {
   return (neuron0x8c16f90()*-0.334914);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18da8() {
   return (neuron0x8c17190()*-0.384557);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18fe8() {
   return (neuron0x8c08330()*-0.174505);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19010() {
   return (neuron0x8c16988()*-0.0509305);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19038() {
   return (neuron0x8c16ab0()*-0.422349);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19060() {
   return (neuron0x8c16bd8()*0.132598);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19088() {
   return (neuron0x8c16d90()*-0.286102);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c190b0() {
   return (neuron0x8c16f90()*0.469958);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c190d8() {
   return (neuron0x8c17190()*0.174298);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c193a0() {
   return (neuron0x8c08330()*-0.131058);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c193c8() {
   return (neuron0x8c16988()*-0.334437);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8b9b5c8() {
   return (neuron0x8c16ab0()*-0.108619);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8b2cb98() {
   return (neuron0x8c16bd8()*-0.360082);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8b31ad8() {
   return (neuron0x8c16d90()*0.491077);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18368() {
   return (neuron0x8c16f90()*0.217962);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18390() {
   return (neuron0x8c17190()*-0.138009);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c18448() {
   return (neuron0x8c08330()*0.113141);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19768() {
   return (neuron0x8c16988()*-0.199919);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19790() {
   return (neuron0x8c16ab0()*0.133181);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c197b8() {
   return (neuron0x8c16bd8()*-0.0674888);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c197e0() {
   return (neuron0x8c16d90()*0.0695464);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19808() {
   return (neuron0x8c16f90()*0.000970211);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19830() {
   return (neuron0x8c17190()*0.0692075);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19a58() {
   return (neuron0x8c08330()*-0.0364241);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19a80() {
   return (neuron0x8c16988()*0.280255);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19aa8() {
   return (neuron0x8c16ab0()*-0.348022);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19ad0() {
   return (neuron0x8c16bd8()*0.2538);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19af8() {
   return (neuron0x8c16d90()*0.417299);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19b20() {
   return (neuron0x8c16f90()*0.32033);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19b48() {
   return (neuron0x8c17190()*0.265089);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19d70() {
   return (neuron0x8c08330()*-0.176108);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19d98() {
   return (neuron0x8c16988()*0.159622);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19dc0() {
   return (neuron0x8c16ab0()*-0.134746);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19de8() {
   return (neuron0x8c16bd8()*0.0714101);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19e10() {
   return (neuron0x8c16d90()*-0.0818868);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19e38() {
   return (neuron0x8c16f90()*-0.43118);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19e60() {
   return (neuron0x8c17190()*0.0402776);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a088() {
   return (neuron0x8c08330()*-0.40973);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a0b0() {
   return (neuron0x8c16988()*-0.461565);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a0d8() {
   return (neuron0x8c16ab0()*-0.0386588);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a100() {
   return (neuron0x8c16bd8()*-0.200431);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a128() {
   return (neuron0x8c16d90()*-0.311216);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a150() {
   return (neuron0x8c16f90()*0.144587);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a178() {
   return (neuron0x8c17190()*-0.344851);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a3a0() {
   return (neuron0x8c08330()*-0.405308);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a3c8() {
   return (neuron0x8c16988()*-0.110502);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a3f0() {
   return (neuron0x8c16ab0()*0.32627);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a418() {
   return (neuron0x8c16bd8()*-0.318925);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a440() {
   return (neuron0x8c16d90()*0.221199);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a468() {
   return (neuron0x8c16f90()*0.350854);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a490() {
   return (neuron0x8c17190()*-0.134262);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a6b8() {
   return (neuron0x8c08330()*0.0305557);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a768() {
   return (neuron0x8c16988()*-0.219243);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a818() {
   return (neuron0x8c16ab0()*0.34606);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a8c8() {
   return (neuron0x8c16bd8()*-0.308593);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a978() {
   return (neuron0x8c16d90()*-0.150218);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1aa28() {
   return (neuron0x8c16f90()*-0.324975);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1aad8() {
   return (neuron0x8c17190()*-0.134036);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1acb0() {
   return (neuron0x8c08330()*-0.332648);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1acd8() {
   return (neuron0x8c16988()*-0.416235);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ad00() {
   return (neuron0x8c16ab0()*0.462396);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ad28() {
   return (neuron0x8c16bd8()*-0.121131);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ad50() {
   return (neuron0x8c16d90()*0.0157557);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ad78() {
   return (neuron0x8c16f90()*0.38586);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ada0() {
   return (neuron0x8c17190()*0.0622453);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1aef0() {
   return (neuron0x8c08330()*-0.337161);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1af18() {
   return (neuron0x8c16988()*-0.470479);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1af40() {
   return (neuron0x8c16ab0()*0.0830412);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1af68() {
   return (neuron0x8c16bd8()*-0.0366725);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1af90() {
   return (neuron0x8c16d90()*-0.00773691);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1afb8() {
   return (neuron0x8c16f90()*-0.0539963);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1afe0() {
   return (neuron0x8c17190()*-0.417431);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b1c0() {
   return (neuron0x8c08330()*-0.334476);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b1e8() {
   return (neuron0x8c16988()*-0.0771401);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b210() {
   return (neuron0x8c16ab0()*-0.375228);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c193f0() {
   return (neuron0x8c16bd8()*-0.181265);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19418() {
   return (neuron0x8c16d90()*-0.117859);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19440() {
   return (neuron0x8c16f90()*-0.279449);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19468() {
   return (neuron0x8c17190()*0.258024);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c195d0() {
   return (neuron0x8c08330()*-0.0956889);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b718() {
   return (neuron0x8c16988()*-0.235266);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b740() {
   return (neuron0x8c16ab0()*0.122792);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b768() {
   return (neuron0x8c16bd8()*-0.093595);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b790() {
   return (neuron0x8c16d90()*-0.148735);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b7b8() {
   return (neuron0x8c16f90()*-0.0467656);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b7e0() {
   return (neuron0x8c17190()*0.320749);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ba08() {
   return (neuron0x8c08330()*0.159387);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ba30() {
   return (neuron0x8c16988()*0.460498);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ba58() {
   return (neuron0x8c16ab0()*0.0950389);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ba80() {
   return (neuron0x8c16bd8()*-0.427589);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1baa8() {
   return (neuron0x8c16d90()*0.0617718);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1bad0() {
   return (neuron0x8c16f90()*-0.364177);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1baf8() {
   return (neuron0x8c17190()*0.214718);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1bd20() {
   return (neuron0x8c08330()*-0.25133);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1bd48() {
   return (neuron0x8c16988()*0.297682);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1bd70() {
   return (neuron0x8c16ab0()*-0.385105);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1bd98() {
   return (neuron0x8c16bd8()*-0.472652);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1bdc0() {
   return (neuron0x8c16d90()*0.392716);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1bde8() {
   return (neuron0x8c16f90()*-0.465783);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1be10() {
   return (neuron0x8c17190()*-0.286622);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c050() {
   return (neuron0x8c08330()*-0.444027);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c078() {
   return (neuron0x8c16988()*0.0947546);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c0a0() {
   return (neuron0x8c16ab0()*-0.387417);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c0c8() {
   return (neuron0x8c16bd8()*-0.190198);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c0f0() {
   return (neuron0x8c16d90()*0.255459);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c118() {
   return (neuron0x8c16f90()*-0.0236216);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c140() {
   return (neuron0x8c17190()*0.504217);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c380() {
   return (neuron0x8c08330()*-0.019182);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c3a8() {
   return (neuron0x8c16988()*0.397001);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c3d0() {
   return (neuron0x8c16ab0()*-0.426715);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c3f8() {
   return (neuron0x8c16bd8()*-0.0704431);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c420() {
   return (neuron0x8c16d90()*0.0756632);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c448() {
   return (neuron0x8c16f90()*-0.287946);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c470() {
   return (neuron0x8c17190()*-0.115393);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c6b0() {
   return (neuron0x8c08330()*0.225444);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c6d8() {
   return (neuron0x8c16988()*0.383654);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c700() {
   return (neuron0x8c16ab0()*0.381105);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c728() {
   return (neuron0x8c16bd8()*0.481838);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c750() {
   return (neuron0x8c16d90()*-0.211046);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c778() {
   return (neuron0x8c16f90()*-0.450489);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c7a0() {
   return (neuron0x8c17190()*-0.436956);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19318() {
   return (neuron0x8c08330()*0.0701221);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19340() {
   return (neuron0x8c16988()*-0.224878);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c19368() {
   return (neuron0x8c16ab0()*-0.0407499);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1cae8() {
   return (neuron0x8c16bd8()*-0.290444);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1cb10() {
   return (neuron0x8c16d90()*0.23638);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1cb38() {
   return (neuron0x8c16f90()*0.0879027);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1cb60() {
   return (neuron0x8c17190()*-0.352614);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1cda0() {
   return (neuron0x8c08330()*0.26807);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1cdc8() {
   return (neuron0x8c16988()*-0.122969);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1cdf0() {
   return (neuron0x8c16ab0()*0.0394775);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ce18() {
   return (neuron0x8c16bd8()*-0.326902);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ce40() {
   return (neuron0x8c16d90()*0.173411);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ce68() {
   return (neuron0x8c16f90()*-0.0802806);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ce90() {
   return (neuron0x8c17190()*0.113831);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d0d0() {
   return (neuron0x8c08330()*0.272396);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d0f8() {
   return (neuron0x8c16988()*-0.22131);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d120() {
   return (neuron0x8c16ab0()*-0.137888);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d148() {
   return (neuron0x8c16bd8()*-0.212807);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d170() {
   return (neuron0x8c16d90()*0.339114);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d198() {
   return (neuron0x8c16f90()*0.248779);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d1c0() {
   return (neuron0x8c17190()*0.0763702);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d400() {
   return (neuron0x8c08330()*0.0872431);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d428() {
   return (neuron0x8c16988()*-0.257452);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d450() {
   return (neuron0x8c16ab0()*0.0704897);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d478() {
   return (neuron0x8c16bd8()*-0.310433);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d4a0() {
   return (neuron0x8c16d90()*0.13807);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d4c8() {
   return (neuron0x8c16f90()*-0.331134);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d4f0() {
   return (neuron0x8c17190()*-0.35927);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d730() {
   return (neuron0x8c08330()*0.0676057);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d758() {
   return (neuron0x8c16988()*-0.26467);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d780() {
   return (neuron0x8c16ab0()*-0.222019);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d7a8() {
   return (neuron0x8c16bd8()*-0.128656);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d7d0() {
   return (neuron0x8c16d90()*0.476291);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d7f8() {
   return (neuron0x8c16f90()*-0.482359);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1d820() {
   return (neuron0x8c17190()*0.142252);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1da60() {
   return (neuron0x8c08330()*0.14512);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1da88() {
   return (neuron0x8c16988()*-0.142741);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1dab0() {
   return (neuron0x8c16ab0()*-0.0736284);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1dad8() {
   return (neuron0x8c16bd8()*0.252439);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1db00() {
   return (neuron0x8c16d90()*-0.169921);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1db28() {
   return (neuron0x8c16f90()*0.288279);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1db50() {
   return (neuron0x8c17190()*0.29434);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1dd90() {
   return (neuron0x8c08330()*0.15309);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a6e0() {
   return (neuron0x8c16988()*0.240358);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a708() {
   return (neuron0x8c16ab0()*-0.300871);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a730() {
   return (neuron0x8c16bd8()*0.212427);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a790() {
   return (neuron0x8c16d90()*-0.421429);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a7b8() {
   return (neuron0x8c16f90()*0.416127);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a7e0() {
   return (neuron0x8c17190()*0.438784);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a8a0() {
   return (neuron0x8c08330()*0.257695);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a9e8() {
   return (neuron0x8c16988()*-0.370676);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1a938() {
   return (neuron0x8c16ab0()*0.304351);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1aa50() {
   return (neuron0x8c16bd8()*-0.145635);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1aa78() {
   return (neuron0x8c16d90()*-0.0982181);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1aaa0() {
   return (neuron0x8c16f90()*-0.0205366);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ab00() {
   return (neuron0x8c17190()*0.424565);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e7d0() {
   return (neuron0x8c08330()*-0.0870847);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e7f8() {
   return (neuron0x8c16988()*-0.265777);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e820() {
   return (neuron0x8c16ab0()*-0.495249);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e848() {
   return (neuron0x8c16bd8()*0.160996);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e870() {
   return (neuron0x8c16d90()*0.480446);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e898() {
   return (neuron0x8c16f90()*-0.465666);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e8c0() {
   return (neuron0x8c17190()*0.222369);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1eae8() {
   return (neuron0x8c08330()*0.422472);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1eb10() {
   return (neuron0x8c16988()*0.297278);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1eb38() {
   return (neuron0x8c16ab0()*0.331481);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1eb60() {
   return (neuron0x8c16bd8()*-0.232256);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1eb88() {
   return (neuron0x8c16d90()*0.091219);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ebb0() {
   return (neuron0x8c16f90()*0.321254);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ebd8() {
   return (neuron0x8c17190()*-0.290694);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ee00() {
   return (neuron0x8c08330()*-0.298841);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ee28() {
   return (neuron0x8c16988()*-0.0839789);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ee50() {
   return (neuron0x8c16ab0()*-0.201312);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ee78() {
   return (neuron0x8c16bd8()*-0.432232);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1eea0() {
   return (neuron0x8c16d90()*0.154185);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1eec8() {
   return (neuron0x8c16f90()*0.352124);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1eef0() {
   return (neuron0x8c17190()*-0.324275);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f130() {
   return (neuron0x8c08330()*0.413353);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f158() {
   return (neuron0x8c16988()*0.387407);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f180() {
   return (neuron0x8c16ab0()*-0.0950877);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f1a8() {
   return (neuron0x8c16bd8()*0.418784);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f1d0() {
   return (neuron0x8c16d90()*0.177447);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b238() {
   return (neuron0x8c16f90()*0.269055);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b260() {
   return (neuron0x8c17190()*-0.286681);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b4a0() {
   return (neuron0x8c08330()*0.000867738);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b4c8() {
   return (neuron0x8c16988()*0.0598714);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b4f0() {
   return (neuron0x8c16ab0()*-0.241167);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b518() {
   return (neuron0x8c16bd8()*0.367456);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b540() {
   return (neuron0x8c16d90()*-0.543226);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b568() {
   return (neuron0x8c16f90()*0.250386);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b590() {
   return (neuron0x8c17190()*-0.185234);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1b618() {
   return (neuron0x8c08330()*0.0487701);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1fbb8() {
   return (neuron0x8c16988()*0.250623);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1fbe0() {
   return (neuron0x8c16ab0()*0.216661);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1fc08() {
   return (neuron0x8c16bd8()*0.381159);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1fc30() {
   return (neuron0x8c16d90()*0.0228107);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1fc58() {
   return (neuron0x8c16f90()*0.273664);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1fc80() {
   return (neuron0x8c17190()*-0.0325288);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1fec0() {
   return (neuron0x8c08330()*0.232005);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1fee8() {
   return (neuron0x8c16988()*0.391139);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ff10() {
   return (neuron0x8c16ab0()*0.288702);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ff38() {
   return (neuron0x8c16bd8()*-0.177798);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ff60() {
   return (neuron0x8c16d90()*-0.236765);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ff88() {
   return (neuron0x8c16f90()*-0.25871);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ffb0() {
   return (neuron0x8c17190()*-0.285332);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c201f0() {
   return (neuron0x8c08330()*-0.41445);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20218() {
   return (neuron0x8c16988()*-0.0798405);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20240() {
   return (neuron0x8c16ab0()*0.149378);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20268() {
   return (neuron0x8c16bd8()*-0.19156);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20290() {
   return (neuron0x8c16d90()*-0.057862);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c202b8() {
   return (neuron0x8c16f90()*0.0711501);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c202e0() {
   return (neuron0x8c17190()*0.260422);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20520() {
   return (neuron0x8c08330()*-0.121266);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20548() {
   return (neuron0x8c16988()*0.474359);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20570() {
   return (neuron0x8c16ab0()*0.0837664);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20598() {
   return (neuron0x8c16bd8()*0.05119);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c205c0() {
   return (neuron0x8c16d90()*0.33195);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c205e8() {
   return (neuron0x8c16f90()*-0.445198);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20610() {
   return (neuron0x8c17190()*0.418078);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20850() {
   return (neuron0x8c08330()*-0.254776);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20878() {
   return (neuron0x8c16988()*0.20838);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c208a0() {
   return (neuron0x8c16ab0()*-0.310126);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c208c8() {
   return (neuron0x8c16bd8()*0.109303);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c208f0() {
   return (neuron0x8c16d90()*-0.144026);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20918() {
   return (neuron0x8c16f90()*-0.15304);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20940() {
   return (neuron0x8c17190()*0.214908);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20b80() {
   return (neuron0x8c08330()*0.330462);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20ba8() {
   return (neuron0x8c16988()*0.439744);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20bd0() {
   return (neuron0x8c16ab0()*0.03096);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20bf8() {
   return (neuron0x8c16bd8()*-0.117266);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20c20() {
   return (neuron0x8c16d90()*-0.00711979);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20c48() {
   return (neuron0x8c16f90()*0.230089);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20c70() {
   return (neuron0x8c17190()*0.476814);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20eb0() {
   return (neuron0x8c08330()*0.49946);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20ed8() {
   return (neuron0x8c16988()*-0.346615);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20f00() {
   return (neuron0x8c16ab0()*0.013997);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20f28() {
   return (neuron0x8c16bd8()*0.281413);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20f50() {
   return (neuron0x8c16d90()*0.0466739);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20f78() {
   return (neuron0x8c16f90()*0.0870451);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c20fa0() {
   return (neuron0x8c17190()*-0.0836977);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c211e0() {
   return (neuron0x8c08330()*0.000497504);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21208() {
   return (neuron0x8c16988()*0.310214);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21230() {
   return (neuron0x8c16ab0()*-0.193084);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21258() {
   return (neuron0x8c16bd8()*0.439026);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21280() {
   return (neuron0x8c16d90()*0.28306);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c212a8() {
   return (neuron0x8c16f90()*-0.00969835);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c212d0() {
   return (neuron0x8c17190()*0.18686);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21510() {
   return (neuron0x8c08330()*-0.341721);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21538() {
   return (neuron0x8c16988()*-0.503082);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21560() {
   return (neuron0x8c16ab0()*0.327966);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21588() {
   return (neuron0x8c16bd8()*-0.204973);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c215b0() {
   return (neuron0x8c16d90()*0.242294);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c215d8() {
   return (neuron0x8c16f90()*0.256402);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21600() {
   return (neuron0x8c17190()*0.317697);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21840() {
   return (neuron0x8c08330()*-0.306128);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21868() {
   return (neuron0x8c16988()*-0.156823);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21890() {
   return (neuron0x8c16ab0()*-0.103564);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c218b8() {
   return (neuron0x8c16bd8()*0.0381597);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c218e0() {
   return (neuron0x8c16d90()*0.380113);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21908() {
   return (neuron0x8c16f90()*-0.175317);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21930() {
   return (neuron0x8c17190()*-0.298916);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21b70() {
   return (neuron0x8c08330()*-0.284033);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21b98() {
   return (neuron0x8c16988()*0.0778967);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21bc0() {
   return (neuron0x8c16ab0()*-0.0263021);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21be8() {
   return (neuron0x8c16bd8()*0.431546);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21c10() {
   return (neuron0x8c16d90()*0.0140213);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21c38() {
   return (neuron0x8c16f90()*-0.475555);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21c60() {
   return (neuron0x8c17190()*-0.467631);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21ea0() {
   return (neuron0x8c08330()*-0.280012);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21ec8() {
   return (neuron0x8c16988()*-0.441847);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21ef0() {
   return (neuron0x8c16ab0()*0.195451);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21f18() {
   return (neuron0x8c16bd8()*-0.00383674);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21f40() {
   return (neuron0x8c16d90()*-0.435088);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21f68() {
   return (neuron0x8c16f90()*-0.403293);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c21f90() {
   return (neuron0x8c17190()*0.211444);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c221d0() {
   return (neuron0x8c08330()*0.104615);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c221f8() {
   return (neuron0x8c16988()*0.120167);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22220() {
   return (neuron0x8c16ab0()*-0.237016);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22248() {
   return (neuron0x8c16bd8()*0.472405);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22270() {
   return (neuron0x8c16d90()*0.546297);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22298() {
   return (neuron0x8c16f90()*-0.295325);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c222c0() {
   return (neuron0x8c17190()*0.348145);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22500() {
   return (neuron0x8c08330()*0.230869);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22528() {
   return (neuron0x8c16988()*0.468338);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22550() {
   return (neuron0x8c16ab0()*0.34967);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22578() {
   return (neuron0x8c16bd8()*-0.102536);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c225a0() {
   return (neuron0x8c16d90()*-0.0501414);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c225c8() {
   return (neuron0x8c16f90()*-0.434349);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c225f0() {
   return (neuron0x8c17190()*0.431245);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22830() {
   return (neuron0x8c08330()*-0.254682);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22858() {
   return (neuron0x8c16988()*0.317985);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22880() {
   return (neuron0x8c16ab0()*0.358051);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c228a8() {
   return (neuron0x8c16bd8()*0.199217);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c228d0() {
   return (neuron0x8c16d90()*-0.446572);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c228f8() {
   return (neuron0x8c16f90()*0.143207);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22920() {
   return (neuron0x8c17190()*-0.37676);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22b60() {
   return (neuron0x8c08330()*0.269165);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22b88() {
   return (neuron0x8c16988()*0.160194);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22bb0() {
   return (neuron0x8c16ab0()*0.424223);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22bd8() {
   return (neuron0x8c16bd8()*-0.326956);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22c00() {
   return (neuron0x8c16d90()*-0.34499);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22c28() {
   return (neuron0x8c16f90()*0.423682);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22c50() {
   return (neuron0x8c17190()*-0.179125);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22e90() {
   return (neuron0x8c08330()*0.265844);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22eb8() {
   return (neuron0x8c16988()*0.359454);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22ee0() {
   return (neuron0x8c16ab0()*0.383887);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22f08() {
   return (neuron0x8c16bd8()*0.170743);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22f30() {
   return (neuron0x8c16d90()*0.202148);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22f58() {
   return (neuron0x8c16f90()*-0.461818);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c22f80() {
   return (neuron0x8c17190()*0.337865);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c231c0() {
   return (neuron0x8c08330()*0.164383);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c231e8() {
   return (neuron0x8c16988()*0.0168617);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23210() {
   return (neuron0x8c16ab0()*-0.0980093);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23238() {
   return (neuron0x8c16bd8()*0.0836877);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23260() {
   return (neuron0x8c16d90()*-0.0112441);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23288() {
   return (neuron0x8c16f90()*-0.155297);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c232b0() {
   return (neuron0x8c17190()*-0.344482);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c234f0() {
   return (neuron0x8c08330()*-0.342138);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23518() {
   return (neuron0x8c16988()*0.362174);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23540() {
   return (neuron0x8c16ab0()*0.0226352);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23568() {
   return (neuron0x8c16bd8()*0.0956536);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23590() {
   return (neuron0x8c16d90()*-0.0238092);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c235b8() {
   return (neuron0x8c16f90()*-0.393168);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c235e0() {
   return (neuron0x8c17190()*0.0312986);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1c9e0() {
   return (neuron0x8c08330()*0.0580922);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ca08() {
   return (neuron0x8c16988()*0.193326);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ca30() {
   return (neuron0x8c16ab0()*0.20225);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ca58() {
   return (neuron0x8c16bd8()*0.440844);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ca80() {
   return (neuron0x8c16d90()*-0.172131);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1caa8() {
   return (neuron0x8c16f90()*-0.153607);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23a28() {
   return (neuron0x8c17190()*-0.212909);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23c50() {
   return (neuron0x8c08330()*-0.186253);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23c78() {
   return (neuron0x8c16988()*0.0266231);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23ca0() {
   return (neuron0x8c16ab0()*0.27826);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23cc8() {
   return (neuron0x8c16bd8()*0.237341);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23cf0() {
   return (neuron0x8c16d90()*0.189732);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23d18() {
   return (neuron0x8c16f90()*-0.158885);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23d40() {
   return (neuron0x8c17190()*-0.470635);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23f80() {
   return (neuron0x8c08330()*-0.339411);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23fa8() {
   return (neuron0x8c16988()*0.325624);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23fd0() {
   return (neuron0x8c16ab0()*0.37656);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c23ff8() {
   return (neuron0x8c16bd8()*-0.0294976);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24020() {
   return (neuron0x8c16d90()*-0.293216);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24048() {
   return (neuron0x8c16f90()*0.133272);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24070() {
   return (neuron0x8c17190()*-0.221952);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c242b0() {
   return (neuron0x8c08330()*0.347654);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c242d8() {
   return (neuron0x8c16988()*-0.382815);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24300() {
   return (neuron0x8c16ab0()*0.293937);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24328() {
   return (neuron0x8c16bd8()*0.355665);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24350() {
   return (neuron0x8c16d90()*-0.408122);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24378() {
   return (neuron0x8c16f90()*0.23608);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c243a0() {
   return (neuron0x8c17190()*-0.283968);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c245e0() {
   return (neuron0x8c08330()*-0.336462);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24608() {
   return (neuron0x8c16988()*-0.478532);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24630() {
   return (neuron0x8c16ab0()*-0.449703);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24658() {
   return (neuron0x8c16bd8()*0.358073);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24680() {
   return (neuron0x8c16d90()*-0.258561);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c246a8() {
   return (neuron0x8c16f90()*-0.166756);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c246d0() {
   return (neuron0x8c17190()*0.263098);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24910() {
   return (neuron0x8c08330()*0.350029);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24938() {
   return (neuron0x8c16988()*-0.36941);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24960() {
   return (neuron0x8c16ab0()*0.206377);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24988() {
   return (neuron0x8c16bd8()*0.221497);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c249b0() {
   return (neuron0x8c16d90()*0.0824029);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c249d8() {
   return (neuron0x8c16f90()*0.0869514);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24a00() {
   return (neuron0x8c17190()*-0.294893);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c24c40() {
   return (neuron0x8c08330()*-0.162762);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1ddb8() {
   return (neuron0x8c16988()*-0.409056);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1dde0() {
   return (neuron0x8c16ab0()*0.151016);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1de08() {
   return (neuron0x8c16bd8()*0.00861675);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e038() {
   return (neuron0x8c16d90()*-0.218058);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e060() {
   return (neuron0x8c16f90()*0.0719887);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1e290() {
   return (neuron0x8c17190()*-0.30143);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25690() {
   return (neuron0x8c08330()*-0.241782);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c256b8() {
   return (neuron0x8c16988()*-0.430354);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c256e0() {
   return (neuron0x8c16ab0()*-0.206004);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25708() {
   return (neuron0x8c16bd8()*0.205949);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25730() {
   return (neuron0x8c16d90()*0.515794);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25758() {
   return (neuron0x8c16f90()*0.349559);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25780() {
   return (neuron0x8c17190()*0.04359);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c259a8() {
   return (neuron0x8c08330()*-0.00305774);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c259d0() {
   return (neuron0x8c16988()*0.04901);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c259f8() {
   return (neuron0x8c16ab0()*0.351685);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25a20() {
   return (neuron0x8c16bd8()*0.278832);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25a48() {
   return (neuron0x8c16d90()*-0.0770341);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25a70() {
   return (neuron0x8c16f90()*-0.39415);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25a98() {
   return (neuron0x8c17190()*0.0950875);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25cd8() {
   return (neuron0x8c08330()*-0.237244);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25d00() {
   return (neuron0x8c16988()*0.118011);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25d28() {
   return (neuron0x8c16ab0()*-0.192664);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25d50() {
   return (neuron0x8c16bd8()*0.425536);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25d78() {
   return (neuron0x8c16d90()*0.357525);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25da0() {
   return (neuron0x8c16f90()*0.154029);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c25dc8() {
   return (neuron0x8c17190()*-0.211654);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26008() {
   return (neuron0x8c08330()*-0.259962);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26030() {
   return (neuron0x8c16988()*0.404372);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26058() {
   return (neuron0x8c16ab0()*-0.151704);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26080() {
   return (neuron0x8c16bd8()*0.072749);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c260a8() {
   return (neuron0x8c16d90()*0.230232);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c260d0() {
   return (neuron0x8c16f90()*0.179132);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c260f8() {
   return (neuron0x8c17190()*-0.267708);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26338() {
   return (neuron0x8c08330()*-0.306764);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26360() {
   return (neuron0x8c16988()*-0.346359);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26388() {
   return (neuron0x8c16ab0()*0.168754);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c263b0() {
   return (neuron0x8c16bd8()*0.361478);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c263d8() {
   return (neuron0x8c16d90()*0.102646);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26400() {
   return (neuron0x8c16f90()*0.353053);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26428() {
   return (neuron0x8c17190()*-0.303007);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26668() {
   return (neuron0x8c08330()*0.202235);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26690() {
   return (neuron0x8c16988()*-0.1282);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c266b8() {
   return (neuron0x8c16ab0()*0.436538);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c266e0() {
   return (neuron0x8c16bd8()*-0.0458304);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26708() {
   return (neuron0x8c16d90()*-0.222665);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26730() {
   return (neuron0x8c16f90()*0.287058);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26758() {
   return (neuron0x8c17190()*-0.0901966);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26998() {
   return (neuron0x8c08330()*0.304486);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c269c0() {
   return (neuron0x8c16988()*-0.0178601);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c269e8() {
   return (neuron0x8c16ab0()*0.08431);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26a10() {
   return (neuron0x8c16bd8()*0.294615);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26a38() {
   return (neuron0x8c16d90()*0.225782);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26a60() {
   return (neuron0x8c16f90()*-0.436068);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26a88() {
   return (neuron0x8c17190()*0.270642);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26cc8() {
   return (neuron0x8c08330()*-0.391521);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26cf0() {
   return (neuron0x8c16988()*-0.394834);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26d18() {
   return (neuron0x8c16ab0()*0.41155);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26d40() {
   return (neuron0x8c16bd8()*0.541006);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26d68() {
   return (neuron0x8c16d90()*-0.316547);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26d90() {
   return (neuron0x8c16f90()*-0.0372263);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26db8() {
   return (neuron0x8c17190()*-0.096706);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c26ff8() {
   return (neuron0x8c08330()*-0.219116);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c27020() {
   return (neuron0x8c16988()*-0.105036);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c27048() {
   return (neuron0x8c16ab0()*-0.105087);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c27070() {
   return (neuron0x8c16bd8()*-0.131532);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c27098() {
   return (neuron0x8c16d90()*-0.132147);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c270c0() {
   return (neuron0x8c16f90()*-0.0266352);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c270e8() {
   return (neuron0x8c17190()*0.190861);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c27328() {
   return (neuron0x8c08330()*0.480266);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c27350() {
   return (neuron0x8c16988()*0.298271);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f1f8() {
   return (neuron0x8c16ab0()*-0.166403);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f220() {
   return (neuron0x8c16bd8()*0.413291);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f248() {
   return (neuron0x8c16d90()*0.213265);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f270() {
   return (neuron0x8c16f90()*0.0825465);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f298() {
   return (neuron0x8c17190()*-0.288863);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f4d8() {
   return (neuron0x8c08330()*0.0578031);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f500() {
   return (neuron0x8c16988()*0.109199);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f528() {
   return (neuron0x8c16ab0()*-0.358307);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f550() {
   return (neuron0x8c16bd8()*-0.45021);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f578() {
   return (neuron0x8c16d90()*-0.475013);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f5a0() {
   return (neuron0x8c16f90()*0.00744081);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f5c8() {
   return (neuron0x8c17190()*0.465017);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f808() {
   return (neuron0x8c08330()*0.182245);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f830() {
   return (neuron0x8c16988()*0.0839292);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f858() {
   return (neuron0x8c16ab0()*-0.107854);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f880() {
   return (neuron0x8c16bd8()*-0.243344);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f8a8() {
   return (neuron0x8c16d90()*-0.286577);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f8d0() {
   return (neuron0x8c16f90()*0.29237);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f8f8() {
   return (neuron0x8c17190()*0.438463);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c1f9c8() {
   return (neuron0x8c08330()*-0.152434);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c284f0() {
   return (neuron0x8c16988()*0.330786);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28518() {
   return (neuron0x8c16ab0()*0.330133);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28540() {
   return (neuron0x8c16bd8()*-0.157013);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28568() {
   return (neuron0x8c16d90()*-0.585257);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28590() {
   return (neuron0x8c16f90()*0.174144);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c285b8() {
   return (neuron0x8c17190()*0.458394);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c287f8() {
   return (neuron0x8c08330()*-0.261482);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28820() {
   return (neuron0x8c16988()*-0.332831);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28848() {
   return (neuron0x8c16ab0()*-0.162068);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28870() {
   return (neuron0x8c16bd8()*-0.0899121);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28898() {
   return (neuron0x8c16d90()*-0.379174);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c288c0() {
   return (neuron0x8c16f90()*0.422212);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c288e8() {
   return (neuron0x8c17190()*-0.295907);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28b28() {
   return (neuron0x8c08330()*-0.0446102);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28b50() {
   return (neuron0x8c16988()*0.183977);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28b78() {
   return (neuron0x8c16ab0()*0.433889);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28ba0() {
   return (neuron0x8c16bd8()*-0.271487);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28bc8() {
   return (neuron0x8c16d90()*-0.0746555);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28bf0() {
   return (neuron0x8c16f90()*-0.481548);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28c18() {
   return (neuron0x8c17190()*-0.0850313);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28e58() {
   return (neuron0x8c08330()*-0.238061);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28e80() {
   return (neuron0x8c16988()*0.332152);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28ea8() {
   return (neuron0x8c16ab0()*-0.238344);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28ed0() {
   return (neuron0x8c16bd8()*-0.124324);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28ef8() {
   return (neuron0x8c16d90()*0.309183);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28f20() {
   return (neuron0x8c16f90()*-0.288819);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c28f48() {
   return (neuron0x8c17190()*0.446076);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29188() {
   return (neuron0x8c08330()*0.121669);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c291b0() {
   return (neuron0x8c16988()*-0.249831);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c291d8() {
   return (neuron0x8c16ab0()*0.184187);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29200() {
   return (neuron0x8c16bd8()*-0.15757);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29228() {
   return (neuron0x8c16d90()*-0.400178);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29250() {
   return (neuron0x8c16f90()*0.00278012);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29278() {
   return (neuron0x8c17190()*-0.172395);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c294b8() {
   return (neuron0x8c08330()*0.375227);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c294e0() {
   return (neuron0x8c16988()*0.180675);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29508() {
   return (neuron0x8c16ab0()*-0.382635);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29530() {
   return (neuron0x8c16bd8()*-0.207697);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29558() {
   return (neuron0x8c16d90()*-0.219079);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29580() {
   return (neuron0x8c16f90()*0.00477268);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c295a8() {
   return (neuron0x8c17190()*0.202365);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c297e8() {
   return (neuron0x8c08330()*-0.225455);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29810() {
   return (neuron0x8c16988()*-0.0890747);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29838() {
   return (neuron0x8c16ab0()*0.321605);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29860() {
   return (neuron0x8c16bd8()*0.379959);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29888() {
   return (neuron0x8c16d90()*0.354428);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c298b0() {
   return (neuron0x8c16f90()*-0.0443163);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c298d8() {
   return (neuron0x8c17190()*-0.16234);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29b18() {
   return (neuron0x8c08330()*0.271753);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29b40() {
   return (neuron0x8c16988()*-0.283557);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29b68() {
   return (neuron0x8c16ab0()*-0.134379);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29b90() {
   return (neuron0x8c16bd8()*0.363479);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29bb8() {
   return (neuron0x8c16d90()*0.197608);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29be0() {
   return (neuron0x8c16f90()*-0.196898);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29c08() {
   return (neuron0x8c17190()*0.170941);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29e48() {
   return (neuron0x8c08330()*-0.0523908);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29e70() {
   return (neuron0x8c16988()*-0.416137);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29e98() {
   return (neuron0x8c16ab0()*0.345171);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29ec0() {
   return (neuron0x8c16bd8()*-0.121682);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29ee8() {
   return (neuron0x8c16d90()*0.171226);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29f10() {
   return (neuron0x8c16f90()*-0.0656591);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c29f38() {
   return (neuron0x8c17190()*0.0232041);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a178() {
   return (neuron0x8c08330()*-0.284286);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a1a0() {
   return (neuron0x8c16988()*0.097474);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a1c8() {
   return (neuron0x8c16ab0()*-0.0537061);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a1f0() {
   return (neuron0x8c16bd8()*0.388999);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a218() {
   return (neuron0x8c16d90()*-0.086289);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a240() {
   return (neuron0x8c16f90()*-0.419755);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a268() {
   return (neuron0x8c17190()*0.215175);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a4a8() {
   return (neuron0x8c08330()*0.0637512);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a4d0() {
   return (neuron0x8c16988()*0.224643);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a4f8() {
   return (neuron0x8c16ab0()*0.40158);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a520() {
   return (neuron0x8c16bd8()*0.183969);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a548() {
   return (neuron0x8c16d90()*0.470648);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a570() {
   return (neuron0x8c16f90()*0.402248);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a598() {
   return (neuron0x8c17190()*0.177914);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a7d8() {
   return (neuron0x8c08330()*0.120261);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a800() {
   return (neuron0x8c16988()*-0.0548938);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a828() {
   return (neuron0x8c16ab0()*-0.186533);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a850() {
   return (neuron0x8c16bd8()*-0.386059);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a878() {
   return (neuron0x8c16d90()*0.279782);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a8a0() {
   return (neuron0x8c16f90()*-0.329581);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2a8c8() {
   return (neuron0x8c17190()*0.153716);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ab08() {
   return (neuron0x8c08330()*-0.0975896);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ab30() {
   return (neuron0x8c16988()*0.050126);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ab58() {
   return (neuron0x8c16ab0()*-0.315107);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ab80() {
   return (neuron0x8c16bd8()*0.358148);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2aba8() {
   return (neuron0x8c16d90()*0.264851);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2abd0() {
   return (neuron0x8c16f90()*-0.38358);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2abf8() {
   return (neuron0x8c17190()*0.00692618);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ae38() {
   return (neuron0x8c08330()*0.349351);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ae60() {
   return (neuron0x8c16988()*-0.115774);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ae88() {
   return (neuron0x8c16ab0()*-0.0722588);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2aeb0() {
   return (neuron0x8c16bd8()*-0.343324);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2aed8() {
   return (neuron0x8c16d90()*-0.0317669);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2af00() {
   return (neuron0x8c16f90()*-0.270671);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2af28() {
   return (neuron0x8c17190()*-0.422063);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b168() {
   return (neuron0x8c08330()*0.0504573);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b190() {
   return (neuron0x8c16988()*0.164712);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b1b8() {
   return (neuron0x8c16ab0()*0.391748);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b1e0() {
   return (neuron0x8c16bd8()*-0.215216);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b208() {
   return (neuron0x8c16d90()*0.192275);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b230() {
   return (neuron0x8c16f90()*0.302562);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b258() {
   return (neuron0x8c17190()*0.460106);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b498() {
   return (neuron0x8c08330()*0.0649334);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b4c0() {
   return (neuron0x8c16988()*-0.345692);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b4e8() {
   return (neuron0x8c16ab0()*0.0670658);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b510() {
   return (neuron0x8c16bd8()*-0.0244036);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b538() {
   return (neuron0x8c16d90()*-0.263657);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b560() {
   return (neuron0x8c16f90()*-0.0879909);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b588() {
   return (neuron0x8c17190()*-0.246154);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b7c8() {
   return (neuron0x8c08330()*-0.0421952);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b7f0() {
   return (neuron0x8c16988()*0.267656);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b818() {
   return (neuron0x8c16ab0()*0.0461413);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b840() {
   return (neuron0x8c16bd8()*-0.053561);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b868() {
   return (neuron0x8c16d90()*-0.119707);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b890() {
   return (neuron0x8c16f90()*0.212343);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2b8b8() {
   return (neuron0x8c17190()*-0.472992);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2baf8() {
   return (neuron0x8c08330()*-0.322683);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bb20() {
   return (neuron0x8c16988()*-0.403535);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bb48() {
   return (neuron0x8c16ab0()*0.365605);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bb70() {
   return (neuron0x8c16bd8()*0.152214);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bb98() {
   return (neuron0x8c16d90()*-0.162294);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bbc0() {
   return (neuron0x8c16f90()*-0.071094);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bbe8() {
   return (neuron0x8c17190()*-0.0203523);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2be28() {
   return (neuron0x8c08330()*0.0617908);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2be50() {
   return (neuron0x8c16988()*-0.346151);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2be78() {
   return (neuron0x8c16ab0()*0.208704);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bea0() {
   return (neuron0x8c16bd8()*0.225036);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bec8() {
   return (neuron0x8c16d90()*-0.135373);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bef0() {
   return (neuron0x8c16f90()*0.247594);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2bf18() {
   return (neuron0x8c17190()*-0.239725);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c158() {
   return (neuron0x8c08330()*0.055815);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c180() {
   return (neuron0x8c16988()*-0.0396806);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c1a8() {
   return (neuron0x8c16ab0()*0.421933);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c1d0() {
   return (neuron0x8c16bd8()*0.207796);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c1f8() {
   return (neuron0x8c16d90()*-0.1914);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c220() {
   return (neuron0x8c16f90()*0.0644558);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c248() {
   return (neuron0x8c17190()*0.175901);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c488() {
   return (neuron0x8c08330()*-0.30016);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c4b0() {
   return (neuron0x8c16988()*0.248756);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c4d8() {
   return (neuron0x8c16ab0()*0.541414);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c500() {
   return (neuron0x8c16bd8()*-0.0768385);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c528() {
   return (neuron0x8c16d90()*0.302412);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c550() {
   return (neuron0x8c16f90()*0.39783);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c578() {
   return (neuron0x8c17190()*-0.405349);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c7b8() {
   return (neuron0x8c08330()*-0.343711);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c7e0() {
   return (neuron0x8c16988()*0.187188);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c808() {
   return (neuron0x8c16ab0()*-0.195875);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c830() {
   return (neuron0x8c16bd8()*0.040995);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c858() {
   return (neuron0x8c16d90()*-0.307385);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c880() {
   return (neuron0x8c16f90()*-0.231717);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2c8a8() {
   return (neuron0x8c17190()*0.237774);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cae8() {
   return (neuron0x8c08330()*-0.187613);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cb10() {
   return (neuron0x8c16988()*-0.325333);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cb38() {
   return (neuron0x8c16ab0()*0.0285649);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cb60() {
   return (neuron0x8c16bd8()*0.486645);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cb88() {
   return (neuron0x8c16d90()*-0.213822);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cbb0() {
   return (neuron0x8c16f90()*-0.219486);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cbd8() {
   return (neuron0x8c17190()*-0.294606);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ce18() {
   return (neuron0x8c08330()*-0.43496);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ce40() {
   return (neuron0x8c16988()*-0.281156);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ce68() {
   return (neuron0x8c16ab0()*0.473157);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ce90() {
   return (neuron0x8c16bd8()*0.192874);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ceb8() {
   return (neuron0x8c16d90()*-0.408034);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cee0() {
   return (neuron0x8c16f90()*0.206002);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2cf08() {
   return (neuron0x8c17190()*0.335864);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d148() {
   return (neuron0x8c08330()*0.347236);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d170() {
   return (neuron0x8c16988()*-0.275343);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d198() {
   return (neuron0x8c16ab0()*0.143133);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d1c0() {
   return (neuron0x8c16bd8()*-0.319367);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d1e8() {
   return (neuron0x8c16d90()*0.447037);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d210() {
   return (neuron0x8c16f90()*-0.169848);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d238() {
   return (neuron0x8c17190()*0.0288075);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d478() {
   return (neuron0x8c08330()*0.345667);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d4a0() {
   return (neuron0x8c16988()*-0.0970382);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d4c8() {
   return (neuron0x8c16ab0()*-0.129274);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d4f0() {
   return (neuron0x8c16bd8()*-0.358409);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d518() {
   return (neuron0x8c16d90()*-0.483397);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d540() {
   return (neuron0x8c16f90()*0.394175);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d568() {
   return (neuron0x8c17190()*0.0468008);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d7a8() {
   return (neuron0x8c08330()*-0.337069);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d7d0() {
   return (neuron0x8c16988()*0.323033);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d7f8() {
   return (neuron0x8c16ab0()*0.321832);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d820() {
   return (neuron0x8c16bd8()*0.37711);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d848() {
   return (neuron0x8c16d90()*0.218118);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d870() {
   return (neuron0x8c16f90()*-0.324297);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2d898() {
   return (neuron0x8c17190()*-0.373055);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2dad8() {
   return (neuron0x8c08330()*-0.39938);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2db00() {
   return (neuron0x8c16988()*0.425649);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2db28() {
   return (neuron0x8c16ab0()*0.263715);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2db50() {
   return (neuron0x8c16bd8()*0.139558);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2db78() {
   return (neuron0x8c16d90()*-0.254331);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2dba0() {
   return (neuron0x8c16f90()*0.189755);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2dbc8() {
   return (neuron0x8c17190()*-0.055678);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2de08() {
   return (neuron0x8c08330()*0.118928);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2de30() {
   return (neuron0x8c16988()*-0.00660915);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2de58() {
   return (neuron0x8c16ab0()*-0.592573);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2de80() {
   return (neuron0x8c16bd8()*0.451926);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2dea8() {
   return (neuron0x8c16d90()*-0.3607);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ded0() {
   return (neuron0x8c16f90()*-0.0370581);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2def8() {
   return (neuron0x8c17190()*0.199411);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e138() {
   return (neuron0x8c08330()*-0.144605);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e160() {
   return (neuron0x8c16988()*-0.368758);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e188() {
   return (neuron0x8c16ab0()*-0.126388);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e1b0() {
   return (neuron0x8c16bd8()*0.0192936);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e1d8() {
   return (neuron0x8c16d90()*0.364773);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e200() {
   return (neuron0x8c16f90()*-0.256498);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e228() {
   return (neuron0x8c17190()*-0.122055);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e468() {
   return (neuron0x8c08330()*-0.129056);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e490() {
   return (neuron0x8c16988()*0.31963);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e4b8() {
   return (neuron0x8c16ab0()*-0.213226);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e4e0() {
   return (neuron0x8c16bd8()*0.137143);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e508() {
   return (neuron0x8c16d90()*-0.0779827);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e530() {
   return (neuron0x8c16f90()*0.282961);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e558() {
   return (neuron0x8c17190()*0.460519);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e798() {
   return (neuron0x8c08330()*0.178209);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e7c0() {
   return (neuron0x8c16988()*0.164067);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e7e8() {
   return (neuron0x8c16ab0()*-0.332924);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e810() {
   return (neuron0x8c16bd8()*0.0961332);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e838() {
   return (neuron0x8c16d90()*-0.00564966);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e860() {
   return (neuron0x8c16f90()*0.0565583);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2e888() {
   return (neuron0x8c17190()*-0.46195);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2eac8() {
   return (neuron0x8c08330()*-0.199401);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2eaf0() {
   return (neuron0x8c16988()*0.143067);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2eb18() {
   return (neuron0x8c16ab0()*0.233228);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2eb40() {
   return (neuron0x8c16bd8()*0.233518);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2eb68() {
   return (neuron0x8c16d90()*-0.0280211);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2eb90() {
   return (neuron0x8c16f90()*-0.106376);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ebb8() {
   return (neuron0x8c17190()*0.186023);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2edf8() {
   return (neuron0x8c08330()*0.0656357);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ee20() {
   return (neuron0x8c16988()*0.00508182);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ee48() {
   return (neuron0x8c16ab0()*0.147532);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ee70() {
   return (neuron0x8c16bd8()*0.383741);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ee98() {
   return (neuron0x8c16d90()*0.179066);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2eec0() {
   return (neuron0x8c16f90()*-0.38873);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2eee8() {
   return (neuron0x8c17190()*-0.322291);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f038() {
   return (neuron0x8c174d0()*-0.109545);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f060() {
   return (neuron0x8c17700()*-0.112625);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f088() {
   return (neuron0x8c17a18()*-0.245367);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f0b0() {
   return (neuron0x8c17db8()*0.0505077);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f0d8() {
   return (neuron0x8c18088()*-0.263403);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f100() {
   return (neuron0x8c18470()*-0.0922599);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f128() {
   return (neuron0x8c18788()*0.535299);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f150() {
   return (neuron0x8c18aa0()*0.131822);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f178() {
   return (neuron0x8c18dd0()*0.0585835);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f1a0() {
   return (neuron0x8c19100()*0.264302);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f1c8() {
   return (neuron0x8c195f8()*-0.322064);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f1f0() {
   return (neuron0x8c19858()*0.067723);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f218() {
   return (neuron0x8c19b70()*-0.236614);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f240() {
   return (neuron0x8c19e88()*0.0306303);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f268() {
   return (neuron0x8c1a1a0()*0.0415242);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f290() {
   return (neuron0x8c1a4b8()*-0.0570559);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f340() {
   return (neuron0x8c1ab88()*-0.030817);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f368() {
   return (neuron0x8c1adc8()*-0.21979);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f390() {
   return (neuron0x8c1b008()*-0.301616);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f3b8() {
   return (neuron0x8c19490()*0.495003);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f3e0() {
   return (neuron0x8c1b808()*-0.149293);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f408() {
   return (neuron0x8c1bb20()*0.0483262);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f430() {
   return (neuron0x8c1be38()*0.101501);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f458() {
   return (neuron0x8c1c168()*-0.0634622);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f480() {
   return (neuron0x8c1c498()*-0.0352028);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f4a8() {
   return (neuron0x8c1c7c8()*-0.272024);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f4d0() {
   return (neuron0x8c1cb88()*-0.324238);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f4f8() {
   return (neuron0x8c1ceb8()*0.368836);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f520() {
   return (neuron0x8c1d1e8()*-0.204539);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f548() {
   return (neuron0x8c1d518()*-0.0167961);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f570() {
   return (neuron0x8c1d848()*-0.0186735);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f598() {
   return (neuron0x8c1db78()*-0.0221087);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f2b8() {
   return (neuron0x8c1e4f0()*-0.0867317);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f2e0() {
   return (neuron0x8c1e618()*0.144011);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f308() {
   return (neuron0x8c1e8e8()*0.0133266);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f6c8() {
   return (neuron0x8c1ec00()*-0.228197);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f6f0() {
   return (neuron0x8c1ef18()*-0.251847);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f718() {
   return (neuron0x8c1b288()*-0.291289);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f740() {
   return (neuron0x8c1fa00()*0.483869);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f768() {
   return (neuron0x8c1fca8()*0.218717);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f790() {
   return (neuron0x8c1ffd8()*-0.264758);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f7b8() {
   return (neuron0x8c20308()*-0.290182);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f7e0() {
   return (neuron0x8c20638()*0.381541);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f808() {
   return (neuron0x8c20968()*-0.133074);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f830() {
   return (neuron0x8c20c98()*0.0409053);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f858() {
   return (neuron0x8c20fc8()*-0.328183);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f880() {
   return (neuron0x8c212f8()*-0.0603093);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f8a8() {
   return (neuron0x8c21628()*-0.435551);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f8d0() {
   return (neuron0x8c21958()*-0.0284823);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f8f8() {
   return (neuron0x8c21c88()*-0.103454);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f920() {
   return (neuron0x8c21fb8()*0.125112);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f948() {
   return (neuron0x8c222e8()*0.0412412);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f970() {
   return (neuron0x8c22618()*-0.079049);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f998() {
   return (neuron0x8c22948()*0.0580041);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f9c0() {
   return (neuron0x8c22c78()*-0.218963);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f9e8() {
   return (neuron0x8c22fa8()*0.426633);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fa10() {
   return (neuron0x8c232d8()*0.214362);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fa38() {
   return (neuron0x8c23608()*0.168518);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fa60() {
   return (neuron0x8c23a50()*-0.117139);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fa88() {
   return (neuron0x8c23d68()*-0.279747);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fab0() {
   return (neuron0x8c24098()*0.282316);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fad8() {
   return (neuron0x8c243c8()*0.184838);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fb00() {
   return (neuron0x8c246f8()*0.315857);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fb28() {
   return (neuron0x8c24a28()*-0.383435);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c173e8() {
   return (neuron0x8c1e2b8()*0.20141);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f5c0() {
   return (neuron0x8c257a8()*0.493599);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f5e8() {
   return (neuron0x8c25ac0()*-0.280851);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f610() {
   return (neuron0x8c25df0()*0.168362);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f638() {
   return (neuron0x8c26120()*0.0760916);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f660() {
   return (neuron0x8c26450()*0.0241441);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2f688() {
   return (neuron0x8c26780()*-0.0668474);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fd58() {
   return (neuron0x8c26ab0()*0.196736);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fd80() {
   return (neuron0x8c26de0()*0.512949);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fda8() {
   return (neuron0x8c27110()*0.114755);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fdd0() {
   return (neuron0x8c1f2c0()*-0.158717);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fdf8() {
   return (neuron0x8c1f5f0()*-0.121808);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fe20() {
   return (neuron0x8c28380()*-0.209662);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fe48() {
   return (neuron0x8c285e0()*-0.281691);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fe70() {
   return (neuron0x8c28910()*0.0679378);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fe98() {
   return (neuron0x8c28c40()*0.130931);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fec0() {
   return (neuron0x8c28f70()*-0.170896);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2fee8() {
   return (neuron0x8c292a0()*-0.302179);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ff10() {
   return (neuron0x8c295d0()*-0.0977817);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ff38() {
   return (neuron0x8c29900()*-0.391014);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ff60() {
   return (neuron0x8c29c30()*-0.0640226);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ff88() {
   return (neuron0x8c29f60()*0.113154);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ffb0() {
   return (neuron0x8c2a290()*-0.0491937);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c2ffd8() {
   return (neuron0x8c2a5c0()*-0.0148219);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30000() {
   return (neuron0x8c2a8f0()*0.254176);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30028() {
   return (neuron0x8c2ac20()*-0.114673);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30050() {
   return (neuron0x8c2af50()*0.143811);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30078() {
   return (neuron0x8c2b280()*0.390796);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c300a0() {
   return (neuron0x8c2b5b0()*0.395119);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c300c8() {
   return (neuron0x8c2b8e0()*-0.286755);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c300f0() {
   return (neuron0x8c2bc10()*0.0157692);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30118() {
   return (neuron0x8c2bf40()*-0.264234);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30140() {
   return (neuron0x8c2c270()*-0.179298);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30168() {
   return (neuron0x8c2c5a0()*-0.113874);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30190() {
   return (neuron0x8c2c8d0()*0.355403);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c301b8() {
   return (neuron0x8c2cc00()*0.0957463);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c301e0() {
   return (neuron0x8c2cf30()*0.17934);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30208() {
   return (neuron0x8c2d260()*0.208482);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30230() {
   return (neuron0x8c2d590()*0.0685721);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30258() {
   return (neuron0x8c2d8c0()*0.516382);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30280() {
   return (neuron0x8c2dbf0()*-0.278742);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c302a8() {
   return (neuron0x8c2df20()*-0.0127838);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c302d0() {
   return (neuron0x8c2e250()*0.183669);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c302f8() {
   return (neuron0x8c2e580()*-0.326835);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30320() {
   return (neuron0x8c2e8b0()*0.315357);
}

double NNPerpElectronAngleCorrectionDeltaTheta::synapse0x8c30348() {
   return (neuron0x8c2ebe0()*-0.278475);
}

