#include "NNParaElectronAngleCorrectionBCPDirPhi.h"
#include <cmath>

double NNParaElectronAngleCorrectionBCPDirPhi::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10,double in11,double in12,double in13,double in14) {
   input0 = (in0 - 0.704494)/0.092045;
   input1 = (in1 - -0.00255061)/0.258907;
   input2 = (in2 - -3.42702)/31.587;
   input3 = (in3 - -0.528546)/56.5134;
   input4 = (in4 - 2626.33)/1277.16;
   input5 = (in5 - 0.00246648)/0.751318;
   input6 = (in6 - 0.00600458)/0.749842;
   input7 = (in7 - -3.92171)/32.3447;
   input8 = (in8 - -1.06857)/57.8975;
   input9 = (in9 - 1.37759)/0.329963;
   input10 = (in10 - 1.4804)/0.355224;
   input11 = (in11 - 0.149358)/1.47685;
   input12 = (in12 - 1.13264)/98.776;
   input13 = (in13 - 5.32237)/6.08756;
   input14 = (in14 - 1.24121)/301.843;
   switch(index) {
     case 0:
         return neuron0x935dfd0();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionBCPDirPhi::Value(int index, double* input) {
   input0 = (input[0] - 0.704494)/0.092045;
   input1 = (input[1] - -0.00255061)/0.258907;
   input2 = (input[2] - -3.42702)/31.587;
   input3 = (input[3] - -0.528546)/56.5134;
   input4 = (input[4] - 2626.33)/1277.16;
   input5 = (input[5] - 0.00246648)/0.751318;
   input6 = (input[6] - 0.00600458)/0.749842;
   input7 = (input[7] - -3.92171)/32.3447;
   input8 = (input[8] - -1.06857)/57.8975;
   input9 = (input[9] - 1.37759)/0.329963;
   input10 = (input[10] - 1.4804)/0.355224;
   input11 = (input[11] - 0.149358)/1.47685;
   input12 = (input[12] - 1.13264)/98.776;
   input13 = (input[13] - 5.32237)/6.08756;
   input14 = (input[14] - 1.24121)/301.843;
   switch(index) {
     case 0:
         return neuron0x935dfd0();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x933c098() {
   return input0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x933c240() {
   return input1;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934a980() {
   return input2;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934aaf0() {
   return input3;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934acf0() {
   return input4;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934af18() {
   return input5;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934b118() {
   return input6;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934b318() {
   return input7;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934b518() {
   return input8;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934b718() {
   return input9;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934b918() {
   return input10;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934bb18() {
   return input11;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934bd18() {
   return input12;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934bf30() {
   return input13;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934c148() {
   return input14;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934c488() {
   double input = -0.249839;
   input += synapse0x90044e8();
   input += synapse0x9041e98();
   input += synapse0x9041ec0();
   input += synapse0x9041ee8();
   input += synapse0x934c640();
   input += synapse0x934c668();
   input += synapse0x934c690();
   input += synapse0x934c6b8();
   input += synapse0x934c6e0();
   input += synapse0x934c708();
   input += synapse0x934c730();
   input += synapse0x934c758();
   input += synapse0x934c780();
   input += synapse0x934c7a8();
   input += synapse0x934c7d0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934c488() {
   double input = input0x934c488();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934c7f8() {
   double input = -0.177321;
   input += synapse0x934ca80();
   input += synapse0x934caa8();
   input += synapse0x934cb58();
   input += synapse0x934cb80();
   input += synapse0x934cba8();
   input += synapse0x934cbd0();
   input += synapse0x934cbf8();
   input += synapse0x934cc20();
   input += synapse0x934cc48();
   input += synapse0x934cc70();
   input += synapse0x934cc98();
   input += synapse0x934ccc0();
   input += synapse0x934cce8();
   input += synapse0x934cd10();
   input += synapse0x934cd38();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934c7f8() {
   double input = input0x934c7f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934cd60() {
   double input = -0.13443;
   input += synapse0x934ced0();
   input += synapse0x934cef8();
   input += synapse0x934cf20();
   input += synapse0x934cad0();
   input += synapse0x934caf8();
   input += synapse0x934cb20();
   input += synapse0x934d050();
   input += synapse0x934d078();
   input += synapse0x934d0a0();
   input += synapse0x934d0c8();
   input += synapse0x934d0f0();
   input += synapse0x934d118();
   input += synapse0x934d140();
   input += synapse0x934d168();
   input += synapse0x934d190();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934cd60() {
   double input = input0x934cd60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934d1b8() {
   double input = 0.468706;
   input += synapse0x934d3b8();
   input += synapse0x934d3e0();
   input += synapse0x934d408();
   input += synapse0x934d430();
   input += synapse0x934d458();
   input += synapse0x934d480();
   input += synapse0x934d4a8();
   input += synapse0x934d4d0();
   input += synapse0x934d4f8();
   input += synapse0x934d520();
   input += synapse0x934d548();
   input += synapse0x934d570();
   input += synapse0x934d598();
   input += synapse0x934d5c0();
   input += synapse0x934d5e8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934d1b8() {
   double input = input0x934d1b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934d610() {
   double input = 0.205491;
   input += synapse0x934d810();
   input += synapse0x934d838();
   input += synapse0x934d860();
   input += synapse0x934d888();
   input += synapse0x934d8b0();
   input += synapse0x9041df0();
   input += synapse0x933c008();
   input += synapse0x9041f10();
   input += synapse0x9041f38();
   input += synapse0x9041fc0();
   input += synapse0x9041fe8();
   input += synapse0x9042010();
   input += synapse0x9042038();
   input += synapse0x9042060();
   input += synapse0x934cf48();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934d610() {
   double input = input0x934d610();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934dae0() {
   double input = 0.0264425;
   input += synapse0x934dc08();
   input += synapse0x934dc30();
   input += synapse0x934dc58();
   input += synapse0x934dc80();
   input += synapse0x934dca8();
   input += synapse0x934dcd0();
   input += synapse0x934dcf8();
   input += synapse0x934dd20();
   input += synapse0x934dd48();
   input += synapse0x934dd70();
   input += synapse0x934dd98();
   input += synapse0x934ddc0();
   input += synapse0x934dde8();
   input += synapse0x934de10();
   input += synapse0x934de38();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934dae0() {
   double input = input0x934dae0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934de60() {
   double input = -0.00471424;
   input += synapse0x934e060();
   input += synapse0x934e088();
   input += synapse0x934e0b0();
   input += synapse0x934e0d8();
   input += synapse0x934e100();
   input += synapse0x934e128();
   input += synapse0x934e150();
   input += synapse0x934e178();
   input += synapse0x934e1a0();
   input += synapse0x934e1c8();
   input += synapse0x934e1f0();
   input += synapse0x934e218();
   input += synapse0x934e240();
   input += synapse0x934e268();
   input += synapse0x934e290();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934de60() {
   double input = input0x934de60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934e2b8() {
   double input = 0.440738;
   input += synapse0x934e4b8();
   input += synapse0x934e4e0();
   input += synapse0x934e508();
   input += synapse0x934e530();
   input += synapse0x934e558();
   input += synapse0x934e580();
   input += synapse0x934e5a8();
   input += synapse0x934e5d0();
   input += synapse0x934e5f8();
   input += synapse0x934e620();
   input += synapse0x934e648();
   input += synapse0x934e670();
   input += synapse0x934e698();
   input += synapse0x934e6c0();
   input += synapse0x934e6e8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934e2b8() {
   double input = input0x934e2b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934e710() {
   double input = 0.361116;
   input += synapse0x934e910();
   input += synapse0x934e938();
   input += synapse0x934e960();
   input += synapse0x934e988();
   input += synapse0x934e9b0();
   input += synapse0x934e9d8();
   input += synapse0x934ea00();
   input += synapse0x934ea28();
   input += synapse0x934ea50();
   input += synapse0x934d8d8();
   input += synapse0x934d900();
   input += synapse0x934d928();
   input += synapse0x934d950();
   input += synapse0x934d978();
   input += synapse0x934d9a0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934e710() {
   double input = input0x934e710();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934ee80() {
   double input = -0.470626;
   input += synapse0x934daa0();
   input += synapse0x934efa8();
   input += synapse0x934efd0();
   input += synapse0x934eff8();
   input += synapse0x934f020();
   input += synapse0x934f048();
   input += synapse0x934f070();
   input += synapse0x934f098();
   input += synapse0x934f0c0();
   input += synapse0x934f0e8();
   input += synapse0x934f110();
   input += synapse0x934f138();
   input += synapse0x934f160();
   input += synapse0x934f188();
   input += synapse0x934f1b0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934ee80() {
   double input = input0x934ee80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934f1d8() {
   double input = 0.0280269;
   input += synapse0x934f3d8();
   input += synapse0x934f400();
   input += synapse0x934f428();
   input += synapse0x934f450();
   input += synapse0x934f478();
   input += synapse0x934f4a0();
   input += synapse0x934f4c8();
   input += synapse0x934f4f0();
   input += synapse0x934f518();
   input += synapse0x934f540();
   input += synapse0x934f568();
   input += synapse0x934f590();
   input += synapse0x934f5b8();
   input += synapse0x934f5e0();
   input += synapse0x934f608();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934f1d8() {
   double input = input0x934f1d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934f630() {
   double input = -0.37259;
   input += synapse0x934f830();
   input += synapse0x934f858();
   input += synapse0x934f880();
   input += synapse0x934f8a8();
   input += synapse0x934f8d0();
   input += synapse0x934f8f8();
   input += synapse0x934f920();
   input += synapse0x934f948();
   input += synapse0x934f970();
   input += synapse0x934f998();
   input += synapse0x934f9c0();
   input += synapse0x934f9e8();
   input += synapse0x934fa10();
   input += synapse0x934fa38();
   input += synapse0x934fa60();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934f630() {
   double input = input0x934f630();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934fa88() {
   double input = 0.289025;
   input += synapse0x934fc88();
   input += synapse0x934fcb0();
   input += synapse0x934fcd8();
   input += synapse0x934fd00();
   input += synapse0x934fd28();
   input += synapse0x934fd50();
   input += synapse0x934fd78();
   input += synapse0x934fda0();
   input += synapse0x934fdc8();
   input += synapse0x934fdf0();
   input += synapse0x934fe18();
   input += synapse0x934fe40();
   input += synapse0x934fe68();
   input += synapse0x934fe90();
   input += synapse0x934feb8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934fa88() {
   double input = input0x934fa88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934fee0() {
   double input = 0.258815;
   input += synapse0x93500e0();
   input += synapse0x9350108();
   input += synapse0x9350130();
   input += synapse0x9350158();
   input += synapse0x9350180();
   input += synapse0x93501a8();
   input += synapse0x93501d0();
   input += synapse0x93501f8();
   input += synapse0x9350220();
   input += synapse0x9350248();
   input += synapse0x9350270();
   input += synapse0x9350298();
   input += synapse0x93502c0();
   input += synapse0x93502e8();
   input += synapse0x9350310();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934fee0() {
   double input = input0x934fee0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9350338() {
   double input = 0.0452179;
   input += synapse0x9350538();
   input += synapse0x9350560();
   input += synapse0x9350588();
   input += synapse0x93505b0();
   input += synapse0x93505d8();
   input += synapse0x9350600();
   input += synapse0x9350628();
   input += synapse0x9350650();
   input += synapse0x9350678();
   input += synapse0x93506a0();
   input += synapse0x93506c8();
   input += synapse0x93506f0();
   input += synapse0x9350718();
   input += synapse0x9350740();
   input += synapse0x9350768();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9350338() {
   double input = input0x9350338();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9350790() {
   double input = 0.208012;
   input += synapse0x9350990();
   input += synapse0x9350a40();
   input += synapse0x9350af0();
   input += synapse0x9350ba0();
   input += synapse0x9350c50();
   input += synapse0x9350d00();
   input += synapse0x9350db0();
   input += synapse0x9350e60();
   input += synapse0x9350f10();
   input += synapse0x9350fc0();
   input += synapse0x9351070();
   input += synapse0x9351120();
   input += synapse0x93511d0();
   input += synapse0x9351280();
   input += synapse0x9351330();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9350790() {
   double input = input0x9350790();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93513e0() {
   double input = -0.422621;
   input += synapse0x9351508();
   input += synapse0x9351530();
   input += synapse0x9351558();
   input += synapse0x9351580();
   input += synapse0x93515a8();
   input += synapse0x93515d0();
   input += synapse0x93515f8();
   input += synapse0x9351620();
   input += synapse0x9351648();
   input += synapse0x9351670();
   input += synapse0x9351698();
   input += synapse0x93516c0();
   input += synapse0x93516e8();
   input += synapse0x9351710();
   input += synapse0x9351738();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93513e0() {
   double input = input0x93513e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9351760() {
   double input = 0.207317;
   input += synapse0x934c9f8();
   input += synapse0x934ca20();
   input += synapse0x934ca48();
   input += synapse0x934b688();
   input += synapse0x934b488();
   input += synapse0x934b288();
   input += synapse0x934b088();
   input += synapse0x934ae88();
   input += synapse0x934ac60();
   input += synapse0x933c4b0();
   input += synapse0x933c3d8();
   input += synapse0x9004580();
   input += synapse0x90045a8();
   input += synapse0x934ea78();
   input += synapse0x934eaa0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9351760() {
   double input = input0x9351760();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x934eac8() {
   double input = -0.358297;
   input += synapse0x934ece0();
   input += synapse0x934ed08();
   input += synapse0x934ed30();
   input += synapse0x934ed58();
   input += synapse0x934ed80();
   input += synapse0x934eda8();
   input += synapse0x934edd0();
   input += synapse0x934edf8();
   input += synapse0x934ee20();
   input += synapse0x934ee48();
   input += synapse0x93521b0();
   input += synapse0x93521d8();
   input += synapse0x9352200();
   input += synapse0x9352228();
   input += synapse0x9352250();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x934eac8() {
   double input = input0x934eac8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9352278() {
   double input = 0.374913;
   input += synapse0x9352478();
   input += synapse0x93524a0();
   input += synapse0x93524c8();
   input += synapse0x93524f0();
   input += synapse0x9352518();
   input += synapse0x9352540();
   input += synapse0x9352568();
   input += synapse0x9352590();
   input += synapse0x93525b8();
   input += synapse0x93525e0();
   input += synapse0x9352608();
   input += synapse0x9352630();
   input += synapse0x9352658();
   input += synapse0x9352680();
   input += synapse0x93526a8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9352278() {
   double input = input0x9352278();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93526d0() {
   double input = -0.47697;
   input += synapse0x93528d0();
   input += synapse0x93528f8();
   input += synapse0x9352920();
   input += synapse0x9352948();
   input += synapse0x9352970();
   input += synapse0x9352998();
   input += synapse0x93529c0();
   input += synapse0x93529e8();
   input += synapse0x9352a10();
   input += synapse0x9352a38();
   input += synapse0x9352a60();
   input += synapse0x9352a88();
   input += synapse0x9352ab0();
   input += synapse0x9352ad8();
   input += synapse0x9352b00();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93526d0() {
   double input = input0x93526d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9352b28() {
   double input = -0.201552;
   input += synapse0x9352d28();
   input += synapse0x9352d50();
   input += synapse0x9352d78();
   input += synapse0x9352da0();
   input += synapse0x9352dc8();
   input += synapse0x9352df0();
   input += synapse0x9352e18();
   input += synapse0x9352e40();
   input += synapse0x9352e68();
   input += synapse0x9352e90();
   input += synapse0x9352eb8();
   input += synapse0x9352ee0();
   input += synapse0x9352f08();
   input += synapse0x9352f30();
   input += synapse0x9352f58();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9352b28() {
   double input = input0x9352b28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9352f80() {
   double input = 0.362768;
   input += synapse0x9353180();
   input += synapse0x93531a8();
   input += synapse0x93531d0();
   input += synapse0x93531f8();
   input += synapse0x9353220();
   input += synapse0x9353248();
   input += synapse0x9353270();
   input += synapse0x9353298();
   input += synapse0x93532c0();
   input += synapse0x93532e8();
   input += synapse0x9353310();
   input += synapse0x9353338();
   input += synapse0x9353360();
   input += synapse0x9353388();
   input += synapse0x93533b0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9352f80() {
   double input = input0x9352f80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93533d8() {
   double input = 0.419847;
   input += synapse0x93535d8();
   input += synapse0x9353600();
   input += synapse0x9353628();
   input += synapse0x9353650();
   input += synapse0x9353678();
   input += synapse0x93536a0();
   input += synapse0x93536c8();
   input += synapse0x93536f0();
   input += synapse0x9353718();
   input += synapse0x9353740();
   input += synapse0x9353768();
   input += synapse0x9353790();
   input += synapse0x93537b8();
   input += synapse0x93537e0();
   input += synapse0x9353808();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93533d8() {
   double input = input0x93533d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9353830() {
   double input = -0.423021;
   input += synapse0x9353a30();
   input += synapse0x9353a58();
   input += synapse0x9353a80();
   input += synapse0x9353aa8();
   input += synapse0x9353ad0();
   input += synapse0x9353af8();
   input += synapse0x9353b20();
   input += synapse0x9353b48();
   input += synapse0x9353b70();
   input += synapse0x9353b98();
   input += synapse0x9353bc0();
   input += synapse0x9353be8();
   input += synapse0x9353c10();
   input += synapse0x9353c38();
   input += synapse0x9353c60();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9353830() {
   double input = input0x9353830();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9353c88() {
   double input = -0.37308;
   input += synapse0x9353e88();
   input += synapse0x9353eb0();
   input += synapse0x9353ed8();
   input += synapse0x9353f00();
   input += synapse0x9353f28();
   input += synapse0x9353f50();
   input += synapse0x9353f78();
   input += synapse0x9353fa0();
   input += synapse0x9353fc8();
   input += synapse0x9353ff0();
   input += synapse0x9354018();
   input += synapse0x9354040();
   input += synapse0x9354068();
   input += synapse0x9354090();
   input += synapse0x93540b8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9353c88() {
   double input = input0x9353c88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93540e0() {
   double input = -0.184756;
   input += synapse0x93542e0();
   input += synapse0x9354308();
   input += synapse0x9354330();
   input += synapse0x9354358();
   input += synapse0x9354380();
   input += synapse0x93543a8();
   input += synapse0x93543d0();
   input += synapse0x93543f8();
   input += synapse0x9354420();
   input += synapse0x9354448();
   input += synapse0x9354470();
   input += synapse0x9354498();
   input += synapse0x93544c0();
   input += synapse0x93544e8();
   input += synapse0x9354510();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93540e0() {
   double input = input0x93540e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9354538() {
   double input = -0.17068;
   input += synapse0x9354750();
   input += synapse0x9354778();
   input += synapse0x93547a0();
   input += synapse0x93547c8();
   input += synapse0x93547f0();
   input += synapse0x9354818();
   input += synapse0x9354840();
   input += synapse0x9354868();
   input += synapse0x9354890();
   input += synapse0x93548b8();
   input += synapse0x93548e0();
   input += synapse0x9354908();
   input += synapse0x9354930();
   input += synapse0x9354958();
   input += synapse0x9354980();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9354538() {
   double input = input0x9354538();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93549a8() {
   double input = -0.314367;
   input += synapse0x9354bc0();
   input += synapse0x9354be8();
   input += synapse0x9354c10();
   input += synapse0x9354c38();
   input += synapse0x9354c60();
   input += synapse0x9354c88();
   input += synapse0x9354cb0();
   input += synapse0x9354cd8();
   input += synapse0x9354d00();
   input += synapse0x9354d28();
   input += synapse0x9354d50();
   input += synapse0x9354d78();
   input += synapse0x9354da0();
   input += synapse0x9354dc8();
   input += synapse0x9354df0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93549a8() {
   double input = input0x93549a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9354e18() {
   double input = 0.192764;
   input += synapse0x9355030();
   input += synapse0x9355058();
   input += synapse0x9355080();
   input += synapse0x93550a8();
   input += synapse0x93550d0();
   input += synapse0x93550f8();
   input += synapse0x9355120();
   input += synapse0x9355148();
   input += synapse0x9355170();
   input += synapse0x9355198();
   input += synapse0x93551c0();
   input += synapse0x93551e8();
   input += synapse0x9355210();
   input += synapse0x9355238();
   input += synapse0x9355260();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9354e18() {
   double input = input0x9354e18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9355288() {
   double input = 0.320557;
   input += synapse0x93554a0();
   input += synapse0x93554c8();
   input += synapse0x93554f0();
   input += synapse0x9355518();
   input += synapse0x9355540();
   input += synapse0x9355568();
   input += synapse0x9355590();
   input += synapse0x93555b8();
   input += synapse0x93555e0();
   input += synapse0x9355608();
   input += synapse0x9355630();
   input += synapse0x9355658();
   input += synapse0x9355680();
   input += synapse0x93556a8();
   input += synapse0x93556d0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9355288() {
   double input = input0x9355288();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93556f8() {
   double input = -0.0422743;
   input += synapse0x9355910();
   input += synapse0x93509b8();
   input += synapse0x93509e0();
   input += synapse0x9350a08();
   input += synapse0x9350a68();
   input += synapse0x9350a90();
   input += synapse0x9350ab8();
   input += synapse0x9350b18();
   input += synapse0x9350b40();
   input += synapse0x9350b68();
   input += synapse0x9350bc8();
   input += synapse0x9350bf0();
   input += synapse0x9350c18();
   input += synapse0x9350c78();
   input += synapse0x9350ca0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93556f8() {
   double input = input0x93556f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93568b0() {
   double input = 0.344511;
   input += synapse0x9350ed0();
   input += synapse0x9350d70();
   input += synapse0x9350e20();
   input += synapse0x9350f38();
   input += synapse0x9350f60();
   input += synapse0x9350f88();
   input += synapse0x9350fe8();
   input += synapse0x9351010();
   input += synapse0x9351038();
   input += synapse0x9351098();
   input += synapse0x93510c0();
   input += synapse0x93510e8();
   input += synapse0x9351148();
   input += synapse0x9351170();
   input += synapse0x9351198();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93568b0() {
   double input = input0x93568b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93569d8() {
   double input = 0.0161401;
   input += synapse0x93513a0();
   input += synapse0x9351240();
   input += synapse0x93512f0();
   input += synapse0x9356b00();
   input += synapse0x9356b28();
   input += synapse0x9356b50();
   input += synapse0x9356b78();
   input += synapse0x9356ba0();
   input += synapse0x9356bc8();
   input += synapse0x9356bf0();
   input += synapse0x9356c18();
   input += synapse0x9356c40();
   input += synapse0x9356c68();
   input += synapse0x9356c90();
   input += synapse0x9356cb8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93569d8() {
   double input = input0x93569d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9356ce0() {
   double input = 0.389441;
   input += synapse0x9356ee0();
   input += synapse0x9356f08();
   input += synapse0x9356f30();
   input += synapse0x93519a8();
   input += synapse0x93519d0();
   input += synapse0x93519f8();
   input += synapse0x9351a20();
   input += synapse0x9351a48();
   input += synapse0x9351a70();
   input += synapse0x9351a98();
   input += synapse0x9351ac0();
   input += synapse0x9351ae8();
   input += synapse0x9351b10();
   input += synapse0x9351b38();
   input += synapse0x9351b60();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9356ce0() {
   double input = input0x9356ce0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9351b88() {
   double input = 0.493897;
   input += synapse0x9351d88();
   input += synapse0x9351db0();
   input += synapse0x9351dd8();
   input += synapse0x9351e00();
   input += synapse0x9351e28();
   input += synapse0x9351e50();
   input += synapse0x9351e78();
   input += synapse0x9351ea0();
   input += synapse0x9351ec8();
   input += synapse0x9351ef0();
   input += synapse0x9351f18();
   input += synapse0x9351f40();
   input += synapse0x9351f68();
   input += synapse0x9351f90();
   input += synapse0x9351fb8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9351b88() {
   double input = input0x9351b88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9351fe0() {
   double input = -0.255311;
   input += synapse0x9357fa8();
   input += synapse0x9357fd0();
   input += synapse0x9357ff8();
   input += synapse0x9358020();
   input += synapse0x9358048();
   input += synapse0x9358070();
   input += synapse0x9358098();
   input += synapse0x93580c0();
   input += synapse0x93580e8();
   input += synapse0x9358110();
   input += synapse0x9358138();
   input += synapse0x9358160();
   input += synapse0x9358188();
   input += synapse0x93581b0();
   input += synapse0x93581d8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9351fe0() {
   double input = input0x9351fe0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9358200() {
   double input = -0.302196;
   input += synapse0x9358400();
   input += synapse0x9358428();
   input += synapse0x9358450();
   input += synapse0x9358478();
   input += synapse0x93584a0();
   input += synapse0x93584c8();
   input += synapse0x93584f0();
   input += synapse0x9358518();
   input += synapse0x9358540();
   input += synapse0x9358568();
   input += synapse0x9358590();
   input += synapse0x93585b8();
   input += synapse0x93585e0();
   input += synapse0x9358608();
   input += synapse0x9358630();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9358200() {
   double input = input0x9358200();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9358658() {
   double input = -0.29581;
   input += synapse0x9358858();
   input += synapse0x9358880();
   input += synapse0x93588a8();
   input += synapse0x93588d0();
   input += synapse0x93588f8();
   input += synapse0x9358920();
   input += synapse0x9358948();
   input += synapse0x9358970();
   input += synapse0x9358998();
   input += synapse0x93589c0();
   input += synapse0x93589e8();
   input += synapse0x9358a10();
   input += synapse0x9358a38();
   input += synapse0x9358a60();
   input += synapse0x9358a88();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9358658() {
   double input = input0x9358658();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9358ab0() {
   double input = 0.099381;
   input += synapse0x9358cb0();
   input += synapse0x9358cd8();
   input += synapse0x9358d00();
   input += synapse0x9358d28();
   input += synapse0x9358d50();
   input += synapse0x9358d78();
   input += synapse0x9358da0();
   input += synapse0x9358dc8();
   input += synapse0x9358df0();
   input += synapse0x9358e18();
   input += synapse0x9358e40();
   input += synapse0x9358e68();
   input += synapse0x9358e90();
   input += synapse0x9358eb8();
   input += synapse0x9358ee0();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9358ab0() {
   double input = input0x9358ab0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9358f08() {
   double input = -0.320821;
   input += synapse0x9359108();
   input += synapse0x9359130();
   input += synapse0x9359158();
   input += synapse0x9359180();
   input += synapse0x93591a8();
   input += synapse0x93591d0();
   input += synapse0x93591f8();
   input += synapse0x9359220();
   input += synapse0x9359248();
   input += synapse0x9359270();
   input += synapse0x9359298();
   input += synapse0x93592c0();
   input += synapse0x93592e8();
   input += synapse0x9359310();
   input += synapse0x9359338();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9358f08() {
   double input = input0x9358f08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9359360() {
   double input = -0.457316;
   input += synapse0x9359578();
   input += synapse0x93595a0();
   input += synapse0x93595c8();
   input += synapse0x93595f0();
   input += synapse0x9359618();
   input += synapse0x9359640();
   input += synapse0x9359668();
   input += synapse0x9359690();
   input += synapse0x93596b8();
   input += synapse0x93596e0();
   input += synapse0x9359708();
   input += synapse0x9359730();
   input += synapse0x9359758();
   input += synapse0x9359780();
   input += synapse0x93597a8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9359360() {
   double input = input0x9359360();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x93597d0() {
   double input = 0.235547;
   input += synapse0x93599e8();
   input += synapse0x9359a10();
   input += synapse0x9359a38();
   input += synapse0x9359a60();
   input += synapse0x9359a88();
   input += synapse0x9359ab0();
   input += synapse0x9359ad8();
   input += synapse0x9359b00();
   input += synapse0x9359b28();
   input += synapse0x9359b50();
   input += synapse0x9359b78();
   input += synapse0x9359ba0();
   input += synapse0x9359bc8();
   input += synapse0x9359bf0();
   input += synapse0x9359c18();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x93597d0() {
   double input = input0x93597d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x9359c40() {
   double input = 0.245108;
   input += synapse0x9359e58();
   input += synapse0x9359e80();
   input += synapse0x9359ea8();
   input += synapse0x9359ed0();
   input += synapse0x9359ef8();
   input += synapse0x9359f20();
   input += synapse0x9359f48();
   input += synapse0x9359f70();
   input += synapse0x9359f98();
   input += synapse0x9359fc0();
   input += synapse0x9359fe8();
   input += synapse0x935a010();
   input += synapse0x935a038();
   input += synapse0x935a060();
   input += synapse0x935a088();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x9359c40() {
   double input = input0x9359c40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935a0b0() {
   double input = -0.134412;
   input += synapse0x935a2c8();
   input += synapse0x935a2f0();
   input += synapse0x935a318();
   input += synapse0x935a340();
   input += synapse0x935a368();
   input += synapse0x935a390();
   input += synapse0x935a3b8();
   input += synapse0x935a3e0();
   input += synapse0x935a408();
   input += synapse0x935a430();
   input += synapse0x935a458();
   input += synapse0x935a480();
   input += synapse0x935a4a8();
   input += synapse0x935a4d0();
   input += synapse0x935a4f8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935a0b0() {
   double input = input0x935a0b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935a520() {
   double input = 0.167092;
   input += synapse0x935a738();
   input += synapse0x935a760();
   input += synapse0x935a788();
   input += synapse0x935a7b0();
   input += synapse0x935a7d8();
   input += synapse0x935a800();
   input += synapse0x935a828();
   input += synapse0x935a850();
   input += synapse0x935a878();
   input += synapse0x935a8a0();
   input += synapse0x935a8c8();
   input += synapse0x935a8f0();
   input += synapse0x935a918();
   input += synapse0x935a940();
   input += synapse0x935a968();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935a520() {
   double input = input0x935a520();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935a990() {
   double input = -0.310578;
   input += synapse0x935aba8();
   input += synapse0x935abd0();
   input += synapse0x935abf8();
   input += synapse0x935ac20();
   input += synapse0x935ac48();
   input += synapse0x935ac70();
   input += synapse0x935ac98();
   input += synapse0x935acc0();
   input += synapse0x935ace8();
   input += synapse0x935ad10();
   input += synapse0x935ad38();
   input += synapse0x935ad60();
   input += synapse0x935ad88();
   input += synapse0x935adb0();
   input += synapse0x935add8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935a990() {
   double input = input0x935a990();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935ae00() {
   double input = 0.360411;
   input += synapse0x935b018();
   input += synapse0x935b040();
   input += synapse0x935b068();
   input += synapse0x935b090();
   input += synapse0x935b0b8();
   input += synapse0x935b0e0();
   input += synapse0x935b108();
   input += synapse0x935b130();
   input += synapse0x935b158();
   input += synapse0x935b180();
   input += synapse0x935b1a8();
   input += synapse0x935b1d0();
   input += synapse0x935b1f8();
   input += synapse0x935b220();
   input += synapse0x935b248();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935ae00() {
   double input = input0x935ae00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935b270() {
   double input = -0.22938;
   input += synapse0x935b488();
   input += synapse0x935b4b0();
   input += synapse0x935b4d8();
   input += synapse0x935b500();
   input += synapse0x935b528();
   input += synapse0x935b550();
   input += synapse0x935b578();
   input += synapse0x935b5a0();
   input += synapse0x935b5c8();
   input += synapse0x935b5f0();
   input += synapse0x935b618();
   input += synapse0x935b640();
   input += synapse0x935b668();
   input += synapse0x935b690();
   input += synapse0x935b6b8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935b270() {
   double input = input0x935b270();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935b6e0() {
   double input = 0.179005;
   input += synapse0x93518a0();
   input += synapse0x93518c8();
   input += synapse0x93518f0();
   input += synapse0x9351918();
   input += synapse0x9351940();
   input += synapse0x9351968();
   input += synapse0x935bb00();
   input += synapse0x935bb28();
   input += synapse0x935bb50();
   input += synapse0x935bb78();
   input += synapse0x935bba0();
   input += synapse0x935bbc8();
   input += synapse0x935bbf0();
   input += synapse0x935bc18();
   input += synapse0x935bc40();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935b6e0() {
   double input = input0x935b6e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935bc68() {
   double input = -0.115939;
   input += synapse0x935be68();
   input += synapse0x935be90();
   input += synapse0x935beb8();
   input += synapse0x935bee0();
   input += synapse0x935bf08();
   input += synapse0x935bf30();
   input += synapse0x935bf58();
   input += synapse0x935bf80();
   input += synapse0x935bfa8();
   input += synapse0x935bfd0();
   input += synapse0x935bff8();
   input += synapse0x935c020();
   input += synapse0x935c048();
   input += synapse0x935c070();
   input += synapse0x935c098();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935bc68() {
   double input = input0x935bc68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935c0c0() {
   double input = -0.357854;
   input += synapse0x935c2d8();
   input += synapse0x935c300();
   input += synapse0x935c328();
   input += synapse0x935c350();
   input += synapse0x935c378();
   input += synapse0x935c3a0();
   input += synapse0x935c3c8();
   input += synapse0x935c3f0();
   input += synapse0x935c418();
   input += synapse0x935c440();
   input += synapse0x935c468();
   input += synapse0x935c490();
   input += synapse0x935c4b8();
   input += synapse0x935c4e0();
   input += synapse0x935c508();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935c0c0() {
   double input = input0x935c0c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935c530() {
   double input = -0.254306;
   input += synapse0x935c748();
   input += synapse0x935c770();
   input += synapse0x935c798();
   input += synapse0x935c7c0();
   input += synapse0x935c7e8();
   input += synapse0x935c810();
   input += synapse0x935c838();
   input += synapse0x935c860();
   input += synapse0x935c888();
   input += synapse0x935c8b0();
   input += synapse0x935c8d8();
   input += synapse0x935c900();
   input += synapse0x935c928();
   input += synapse0x935c950();
   input += synapse0x935c978();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935c530() {
   double input = input0x935c530();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935c9a0() {
   double input = -0.148422;
   input += synapse0x935cbb8();
   input += synapse0x935cbe0();
   input += synapse0x935cc08();
   input += synapse0x935cc30();
   input += synapse0x935cc58();
   input += synapse0x935cc80();
   input += synapse0x935cca8();
   input += synapse0x935ccd0();
   input += synapse0x935ccf8();
   input += synapse0x935cd20();
   input += synapse0x935cd48();
   input += synapse0x935cd70();
   input += synapse0x935cd98();
   input += synapse0x935cdc0();
   input += synapse0x935cde8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935c9a0() {
   double input = input0x935c9a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935ce10() {
   double input = -0.282908;
   input += synapse0x935d028();
   input += synapse0x935d050();
   input += synapse0x935d078();
   input += synapse0x935d0a0();
   input += synapse0x935d0c8();
   input += synapse0x935d0f0();
   input += synapse0x935d118();
   input += synapse0x935d140();
   input += synapse0x935d168();
   input += synapse0x935d190();
   input += synapse0x935d1b8();
   input += synapse0x935d1e0();
   input += synapse0x935d208();
   input += synapse0x935d230();
   input += synapse0x935d258();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935ce10() {
   double input = input0x935ce10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935d280() {
   double input = -0.0321163;
   input += synapse0x935d498();
   input += synapse0x935d4c0();
   input += synapse0x935d4e8();
   input += synapse0x935d510();
   input += synapse0x935d538();
   input += synapse0x935d560();
   input += synapse0x935d588();
   input += synapse0x935d5b0();
   input += synapse0x935d5d8();
   input += synapse0x935d600();
   input += synapse0x935d628();
   input += synapse0x935d650();
   input += synapse0x935d678();
   input += synapse0x935d6a0();
   input += synapse0x935d6c8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935d280() {
   double input = input0x935d280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935d6f0() {
   double input = -0.0491284;
   input += synapse0x935d908();
   input += synapse0x935d930();
   input += synapse0x935d958();
   input += synapse0x935d980();
   input += synapse0x935d9a8();
   input += synapse0x935d9d0();
   input += synapse0x935d9f8();
   input += synapse0x935da20();
   input += synapse0x935da48();
   input += synapse0x935da70();
   input += synapse0x935da98();
   input += synapse0x935dac0();
   input += synapse0x935dae8();
   input += synapse0x935db10();
   input += synapse0x935db38();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935d6f0() {
   double input = input0x935d6f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935db60() {
   double input = 0.379243;
   input += synapse0x935dd78();
   input += synapse0x935dda0();
   input += synapse0x935ddc8();
   input += synapse0x935ddf0();
   input += synapse0x935de18();
   input += synapse0x935de40();
   input += synapse0x935de68();
   input += synapse0x935de90();
   input += synapse0x935deb8();
   input += synapse0x935dee0();
   input += synapse0x935df08();
   input += synapse0x935df30();
   input += synapse0x935df58();
   input += synapse0x935df80();
   input += synapse0x935dfa8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935db60() {
   double input = input0x935db60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::input0x935dfd0() {
   double input = -0.142551;
   input += synapse0x935e0f8();
   input += synapse0x935e120();
   input += synapse0x935e148();
   input += synapse0x935e170();
   input += synapse0x935e198();
   input += synapse0x935e1c0();
   input += synapse0x935e1e8();
   input += synapse0x935e210();
   input += synapse0x935e238();
   input += synapse0x935e260();
   input += synapse0x935e288();
   input += synapse0x935e2b0();
   input += synapse0x935e2d8();
   input += synapse0x935e300();
   input += synapse0x935e328();
   input += synapse0x935e350();
   input += synapse0x935e400();
   input += synapse0x935e428();
   input += synapse0x935e450();
   input += synapse0x935e478();
   input += synapse0x935e4a0();
   input += synapse0x935e4c8();
   input += synapse0x935e4f0();
   input += synapse0x935e518();
   input += synapse0x935e540();
   input += synapse0x935e568();
   input += synapse0x935e590();
   input += synapse0x935e5b8();
   input += synapse0x935e5e0();
   input += synapse0x935e608();
   input += synapse0x935e630();
   input += synapse0x935e658();
   input += synapse0x935e378();
   input += synapse0x935e3a0();
   input += synapse0x935e3c8();
   input += synapse0x935e788();
   input += synapse0x935e7b0();
   input += synapse0x935e7d8();
   input += synapse0x935e800();
   input += synapse0x935e828();
   input += synapse0x935e850();
   input += synapse0x935e878();
   input += synapse0x935e8a0();
   input += synapse0x935e8c8();
   input += synapse0x935e8f0();
   input += synapse0x935e918();
   input += synapse0x935e940();
   input += synapse0x935e968();
   input += synapse0x935e990();
   input += synapse0x935e9b8();
   input += synapse0x935e9e0();
   input += synapse0x935ea08();
   input += synapse0x935ea30();
   input += synapse0x935ea58();
   input += synapse0x935ea80();
   input += synapse0x935eaa8();
   input += synapse0x935ead0();
   input += synapse0x935eaf8();
   return input;
}

double NNParaElectronAngleCorrectionBCPDirPhi::neuron0x935dfd0() {
   double input = input0x935dfd0();
   return (input * 1)+0;
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x90044e8() {
   return (neuron0x933c098()*0.180529);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9041e98() {
   return (neuron0x933c240()*0.215786);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9041ec0() {
   return (neuron0x934a980()*0.262364);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9041ee8() {
   return (neuron0x934aaf0()*-0.384207);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c640() {
   return (neuron0x934acf0()*0.00722813);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c668() {
   return (neuron0x934af18()*0.0861464);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c690() {
   return (neuron0x934b118()*-0.304117);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c6b8() {
   return (neuron0x934b318()*-0.0815085);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c6e0() {
   return (neuron0x934b518()*0.00638383);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c708() {
   return (neuron0x934b718()*-0.42914);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c730() {
   return (neuron0x934b918()*-0.182686);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c758() {
   return (neuron0x934bb18()*0.193915);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c780() {
   return (neuron0x934bd18()*-0.0490068);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c7a8() {
   return (neuron0x934bf30()*0.00612599);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c7d0() {
   return (neuron0x934c148()*-0.351203);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ca80() {
   return (neuron0x933c098()*0.227417);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934caa8() {
   return (neuron0x933c240()*0.13282);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cb58() {
   return (neuron0x934a980()*0.2577);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cb80() {
   return (neuron0x934aaf0()*0.207471);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cba8() {
   return (neuron0x934acf0()*0.333711);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cbd0() {
   return (neuron0x934af18()*0.165319);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cbf8() {
   return (neuron0x934b118()*-0.105272);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cc20() {
   return (neuron0x934b318()*-0.086816);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cc48() {
   return (neuron0x934b518()*-0.0409789);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cc70() {
   return (neuron0x934b718()*-0.288715);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cc98() {
   return (neuron0x934b918()*-0.509365);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ccc0() {
   return (neuron0x934bb18()*-0.309201);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cce8() {
   return (neuron0x934bd18()*0.189682);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cd10() {
   return (neuron0x934bf30()*0.304589);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cd38() {
   return (neuron0x934c148()*0.337302);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ced0() {
   return (neuron0x933c098()*-0.417738);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cef8() {
   return (neuron0x933c240()*-0.158131);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cf20() {
   return (neuron0x934a980()*0.296241);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cad0() {
   return (neuron0x934aaf0()*-0.522329);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934caf8() {
   return (neuron0x934acf0()*0.235106);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cb20() {
   return (neuron0x934af18()*0.0706805);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d050() {
   return (neuron0x934b118()*0.0590439);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d078() {
   return (neuron0x934b318()*0.409725);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d0a0() {
   return (neuron0x934b518()*-0.122661);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d0c8() {
   return (neuron0x934b718()*0.0297459);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d0f0() {
   return (neuron0x934b918()*-0.00391447);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d118() {
   return (neuron0x934bb18()*0.0313438);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d140() {
   return (neuron0x934bd18()*-0.00335164);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d168() {
   return (neuron0x934bf30()*0.0831027);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d190() {
   return (neuron0x934c148()*-0.098154);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d3b8() {
   return (neuron0x933c098()*0.226799);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d3e0() {
   return (neuron0x933c240()*-0.299805);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d408() {
   return (neuron0x934a980()*0.230381);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d430() {
   return (neuron0x934aaf0()*-0.133708);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d458() {
   return (neuron0x934acf0()*-0.000914491);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d480() {
   return (neuron0x934af18()*0.334864);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d4a8() {
   return (neuron0x934b118()*0.0452469);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d4d0() {
   return (neuron0x934b318()*-0.438507);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d4f8() {
   return (neuron0x934b518()*0.295466);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d520() {
   return (neuron0x934b718()*-0.443403);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d548() {
   return (neuron0x934b918()*-0.402488);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d570() {
   return (neuron0x934bb18()*0.373426);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d598() {
   return (neuron0x934bd18()*0.108321);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d5c0() {
   return (neuron0x934bf30()*-0.177048);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d5e8() {
   return (neuron0x934c148()*-0.395766);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d810() {
   return (neuron0x933c098()*0.293057);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d838() {
   return (neuron0x933c240()*-0.267782);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d860() {
   return (neuron0x934a980()*0.423902);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d888() {
   return (neuron0x934aaf0()*-0.239293);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d8b0() {
   return (neuron0x934acf0()*0.14686);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9041df0() {
   return (neuron0x934af18()*0.298726);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x933c008() {
   return (neuron0x934b118()*0.366447);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9041f10() {
   return (neuron0x934b318()*0.36127);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9041f38() {
   return (neuron0x934b518()*-0.499507);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9041fc0() {
   return (neuron0x934b718()*-0.0278366);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9041fe8() {
   return (neuron0x934b918()*-0.390297);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9042010() {
   return (neuron0x934bb18()*-0.390303);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9042038() {
   return (neuron0x934bd18()*-0.237298);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9042060() {
   return (neuron0x934bf30()*-0.246929);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934cf48() {
   return (neuron0x934c148()*-0.391108);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dc08() {
   return (neuron0x933c098()*-0.418794);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dc30() {
   return (neuron0x933c240()*-0.458406);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dc58() {
   return (neuron0x934a980()*-0.170767);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dc80() {
   return (neuron0x934aaf0()*0.393577);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dca8() {
   return (neuron0x934acf0()*0.183162);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dcd0() {
   return (neuron0x934af18()*0.0132988);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dcf8() {
   return (neuron0x934b118()*-0.124853);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dd20() {
   return (neuron0x934b318()*0.402047);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dd48() {
   return (neuron0x934b518()*-0.456252);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dd70() {
   return (neuron0x934b718()*0.102765);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dd98() {
   return (neuron0x934b918()*-0.309456);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ddc0() {
   return (neuron0x934bb18()*0.169738);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934dde8() {
   return (neuron0x934bd18()*-0.122048);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934de10() {
   return (neuron0x934bf30()*0.271804);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934de38() {
   return (neuron0x934c148()*-0.42277);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e060() {
   return (neuron0x933c098()*0.0355398);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e088() {
   return (neuron0x933c240()*-0.130627);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e0b0() {
   return (neuron0x934a980()*-0.210533);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e0d8() {
   return (neuron0x934aaf0()*0.401084);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e100() {
   return (neuron0x934acf0()*0.16676);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e128() {
   return (neuron0x934af18()*-0.127869);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e150() {
   return (neuron0x934b118()*0.0643369);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e178() {
   return (neuron0x934b318()*0.247656);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e1a0() {
   return (neuron0x934b518()*0.346053);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e1c8() {
   return (neuron0x934b718()*-0.526796);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e1f0() {
   return (neuron0x934b918()*0.249804);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e218() {
   return (neuron0x934bb18()*-0.00650232);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e240() {
   return (neuron0x934bd18()*0.00190596);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e268() {
   return (neuron0x934bf30()*-0.244189);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e290() {
   return (neuron0x934c148()*-0.47112);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e4b8() {
   return (neuron0x933c098()*-0.274478);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e4e0() {
   return (neuron0x933c240()*0.13176);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e508() {
   return (neuron0x934a980()*-0.383019);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e530() {
   return (neuron0x934aaf0()*0.0368424);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e558() {
   return (neuron0x934acf0()*0.0961217);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e580() {
   return (neuron0x934af18()*0.157537);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e5a8() {
   return (neuron0x934b118()*-0.231463);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e5d0() {
   return (neuron0x934b318()*-0.0241898);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e5f8() {
   return (neuron0x934b518()*-0.069332);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e620() {
   return (neuron0x934b718()*-0.258441);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e648() {
   return (neuron0x934b918()*-0.236272);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e670() {
   return (neuron0x934bb18()*0.154961);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e698() {
   return (neuron0x934bd18()*-0.261956);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e6c0() {
   return (neuron0x934bf30()*0.251162);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e6e8() {
   return (neuron0x934c148()*0.0811291);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e910() {
   return (neuron0x933c098()*0.0526084);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e938() {
   return (neuron0x933c240()*-0.478363);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e960() {
   return (neuron0x934a980()*0.168765);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e988() {
   return (neuron0x934aaf0()*-0.0374614);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e9b0() {
   return (neuron0x934acf0()*0.303565);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934e9d8() {
   return (neuron0x934af18()*-0.278398);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ea00() {
   return (neuron0x934b118()*-0.264263);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ea28() {
   return (neuron0x934b318()*0.0159576);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ea50() {
   return (neuron0x934b518()*-0.142217);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d8d8() {
   return (neuron0x934b718()*0.180363);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d900() {
   return (neuron0x934b918()*0.0247653);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d928() {
   return (neuron0x934bb18()*-0.0985723);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d950() {
   return (neuron0x934bd18()*-0.226451);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d978() {
   return (neuron0x934bf30()*0.195963);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934d9a0() {
   return (neuron0x934c148()*0.0114756);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934daa0() {
   return (neuron0x933c098()*0.0952311);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934efa8() {
   return (neuron0x933c240()*-0.0165683);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934efd0() {
   return (neuron0x934a980()*0.31418);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934eff8() {
   return (neuron0x934aaf0()*-0.0846384);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f020() {
   return (neuron0x934acf0()*-0.46116);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f048() {
   return (neuron0x934af18()*0.0248002);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f070() {
   return (neuron0x934b118()*0.106851);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f098() {
   return (neuron0x934b318()*-0.529368);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f0c0() {
   return (neuron0x934b518()*0.429674);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f0e8() {
   return (neuron0x934b718()*-0.0562128);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f110() {
   return (neuron0x934b918()*0.264703);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f138() {
   return (neuron0x934bb18()*-0.0757358);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f160() {
   return (neuron0x934bd18()*-0.379703);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f188() {
   return (neuron0x934bf30()*0.338748);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f1b0() {
   return (neuron0x934c148()*-0.432827);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f3d8() {
   return (neuron0x933c098()*-0.295112);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f400() {
   return (neuron0x933c240()*-0.384158);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f428() {
   return (neuron0x934a980()*-0.425026);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f450() {
   return (neuron0x934aaf0()*-0.0864546);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f478() {
   return (neuron0x934acf0()*-0.285603);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f4a0() {
   return (neuron0x934af18()*-0.0289641);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f4c8() {
   return (neuron0x934b118()*0.114886);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f4f0() {
   return (neuron0x934b318()*-0.00796751);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f518() {
   return (neuron0x934b518()*0.299469);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f540() {
   return (neuron0x934b718()*0.477062);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f568() {
   return (neuron0x934b918()*0.0892374);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f590() {
   return (neuron0x934bb18()*-0.304646);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f5b8() {
   return (neuron0x934bd18()*0.337259);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f5e0() {
   return (neuron0x934bf30()*-0.433955);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f608() {
   return (neuron0x934c148()*-0.0524999);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f830() {
   return (neuron0x933c098()*-0.144973);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f858() {
   return (neuron0x933c240()*0.247548);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f880() {
   return (neuron0x934a980()*-0.0353257);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f8a8() {
   return (neuron0x934aaf0()*-0.363493);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f8d0() {
   return (neuron0x934acf0()*-0.218551);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f8f8() {
   return (neuron0x934af18()*0.274193);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f920() {
   return (neuron0x934b118()*0.224348);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f948() {
   return (neuron0x934b318()*-0.216639);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f970() {
   return (neuron0x934b518()*0.269412);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f998() {
   return (neuron0x934b718()*-0.240138);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f9c0() {
   return (neuron0x934b918()*-0.0113933);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934f9e8() {
   return (neuron0x934bb18()*-0.219617);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fa10() {
   return (neuron0x934bd18()*-0.349677);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fa38() {
   return (neuron0x934bf30()*0.306382);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fa60() {
   return (neuron0x934c148()*0.082653);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fc88() {
   return (neuron0x933c098()*-0.269601);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fcb0() {
   return (neuron0x933c240()*0.40174);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fcd8() {
   return (neuron0x934a980()*0.0268374);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fd00() {
   return (neuron0x934aaf0()*0.286307);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fd28() {
   return (neuron0x934acf0()*0.146336);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fd50() {
   return (neuron0x934af18()*-0.325276);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fd78() {
   return (neuron0x934b118()*0.350373);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fda0() {
   return (neuron0x934b318()*-0.365653);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fdc8() {
   return (neuron0x934b518()*0.411836);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fdf0() {
   return (neuron0x934b718()*0.340762);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fe18() {
   return (neuron0x934b918()*0.283986);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fe40() {
   return (neuron0x934bb18()*0.337151);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fe68() {
   return (neuron0x934bd18()*-0.151171);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934fe90() {
   return (neuron0x934bf30()*0.290443);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934feb8() {
   return (neuron0x934c148()*-0.447825);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93500e0() {
   return (neuron0x933c098()*0.175581);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350108() {
   return (neuron0x933c240()*-0.266812);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350130() {
   return (neuron0x934a980()*-0.00411008);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350158() {
   return (neuron0x934aaf0()*0.410912);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350180() {
   return (neuron0x934acf0()*0.258616);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93501a8() {
   return (neuron0x934af18()*0.295888);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93501d0() {
   return (neuron0x934b118()*0.225803);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93501f8() {
   return (neuron0x934b318()*-0.0385065);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350220() {
   return (neuron0x934b518()*0.479523);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350248() {
   return (neuron0x934b718()*0.0185372);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350270() {
   return (neuron0x934b918()*0.193615);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350298() {
   return (neuron0x934bb18()*0.459807);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93502c0() {
   return (neuron0x934bd18()*0.398178);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93502e8() {
   return (neuron0x934bf30()*0.0973707);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350310() {
   return (neuron0x934c148()*0.246816);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350538() {
   return (neuron0x933c098()*0.0301195);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350560() {
   return (neuron0x933c240()*0.521112);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350588() {
   return (neuron0x934a980()*-0.0298291);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93505b0() {
   return (neuron0x934aaf0()*0.0851661);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93505d8() {
   return (neuron0x934acf0()*-0.0953881);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350600() {
   return (neuron0x934af18()*-0.245044);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350628() {
   return (neuron0x934b118()*0.170135);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350650() {
   return (neuron0x934b318()*-0.3074);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350678() {
   return (neuron0x934b518()*-0.375692);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93506a0() {
   return (neuron0x934b718()*-0.0827631);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93506c8() {
   return (neuron0x934b918()*-0.234282);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93506f0() {
   return (neuron0x934bb18()*-0.204652);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350718() {
   return (neuron0x934bd18()*0.448989);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350740() {
   return (neuron0x934bf30()*-0.333859);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350768() {
   return (neuron0x934c148()*-0.162747);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350990() {
   return (neuron0x933c098()*0.383043);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350a40() {
   return (neuron0x933c240()*0.116353);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350af0() {
   return (neuron0x934a980()*0.186646);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350ba0() {
   return (neuron0x934aaf0()*-0.238992);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350c50() {
   return (neuron0x934acf0()*0.285394);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350d00() {
   return (neuron0x934af18()*-0.0615042);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350db0() {
   return (neuron0x934b118()*0.291169);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350e60() {
   return (neuron0x934b318()*-0.0506513);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350f10() {
   return (neuron0x934b518()*0.120153);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350fc0() {
   return (neuron0x934b718()*-0.338202);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351070() {
   return (neuron0x934b918()*0.343587);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351120() {
   return (neuron0x934bb18()*0.491136);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93511d0() {
   return (neuron0x934bd18()*-0.22905);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351280() {
   return (neuron0x934bf30()*0.37227);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351330() {
   return (neuron0x934c148()*0.23868);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351508() {
   return (neuron0x933c098()*-0.360943);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351530() {
   return (neuron0x933c240()*0.0647456);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351558() {
   return (neuron0x934a980()*-0.0108162);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351580() {
   return (neuron0x934aaf0()*-0.380945);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93515a8() {
   return (neuron0x934acf0()*-0.207408);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93515d0() {
   return (neuron0x934af18()*-0.194522);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93515f8() {
   return (neuron0x934b118()*-0.370737);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351620() {
   return (neuron0x934b318()*0.013181);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351648() {
   return (neuron0x934b518()*-0.4197);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351670() {
   return (neuron0x934b718()*-0.517482);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351698() {
   return (neuron0x934b918()*-0.123147);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93516c0() {
   return (neuron0x934bb18()*-0.345351);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93516e8() {
   return (neuron0x934bd18()*-0.103013);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351710() {
   return (neuron0x934bf30()*0.33387);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351738() {
   return (neuron0x934c148()*-0.159371);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934c9f8() {
   return (neuron0x933c098()*0.342413);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ca20() {
   return (neuron0x933c240()*-0.307354);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ca48() {
   return (neuron0x934a980()*-0.15807);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934b688() {
   return (neuron0x934aaf0()*-0.423045);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934b488() {
   return (neuron0x934acf0()*-0.0723095);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934b288() {
   return (neuron0x934af18()*-0.294111);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934b088() {
   return (neuron0x934b118()*-0.00809521);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ae88() {
   return (neuron0x934b318()*-0.153047);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ac60() {
   return (neuron0x934b518()*0.254488);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x933c4b0() {
   return (neuron0x934b718()*0.2064);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x933c3d8() {
   return (neuron0x934b918()*0.293896);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9004580() {
   return (neuron0x934bb18()*0.203357);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x90045a8() {
   return (neuron0x934bd18()*0.143744);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ea78() {
   return (neuron0x934bf30()*-0.475935);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934eaa0() {
   return (neuron0x934c148()*0.450824);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ece0() {
   return (neuron0x933c098()*0.311107);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ed08() {
   return (neuron0x933c240()*-0.496507);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ed30() {
   return (neuron0x934a980()*-0.130246);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ed58() {
   return (neuron0x934aaf0()*-0.400558);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ed80() {
   return (neuron0x934acf0()*0.382212);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934eda8() {
   return (neuron0x934af18()*-0.371299);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934edd0() {
   return (neuron0x934b118()*0.422951);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934edf8() {
   return (neuron0x934b318()*0.377454);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ee20() {
   return (neuron0x934b518()*0.300138);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x934ee48() {
   return (neuron0x934b718()*-0.237758);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93521b0() {
   return (neuron0x934b918()*-0.156748);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93521d8() {
   return (neuron0x934bb18()*0.180448);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352200() {
   return (neuron0x934bd18()*0.1101);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352228() {
   return (neuron0x934bf30()*-0.276559);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352250() {
   return (neuron0x934c148()*-0.372708);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352478() {
   return (neuron0x933c098()*0.434443);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93524a0() {
   return (neuron0x933c240()*0.432854);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93524c8() {
   return (neuron0x934a980()*-0.362908);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93524f0() {
   return (neuron0x934aaf0()*0.0855432);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352518() {
   return (neuron0x934acf0()*-0.260693);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352540() {
   return (neuron0x934af18()*-0.0135999);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352568() {
   return (neuron0x934b118()*-0.101973);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352590() {
   return (neuron0x934b318()*0.164417);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93525b8() {
   return (neuron0x934b518()*-0.44838);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93525e0() {
   return (neuron0x934b718()*0.334334);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352608() {
   return (neuron0x934b918()*-0.23151);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352630() {
   return (neuron0x934bb18()*0.00249798);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352658() {
   return (neuron0x934bd18()*-0.438717);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352680() {
   return (neuron0x934bf30()*-0.24951);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93526a8() {
   return (neuron0x934c148()*-0.03726);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93528d0() {
   return (neuron0x933c098()*0.0945395);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93528f8() {
   return (neuron0x933c240()*-0.0617044);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352920() {
   return (neuron0x934a980()*0.0978735);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352948() {
   return (neuron0x934aaf0()*0.236216);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352970() {
   return (neuron0x934acf0()*-0.278729);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352998() {
   return (neuron0x934af18()*-0.440155);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93529c0() {
   return (neuron0x934b118()*-0.258821);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93529e8() {
   return (neuron0x934b318()*-0.125878);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352a10() {
   return (neuron0x934b518()*-0.240601);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352a38() {
   return (neuron0x934b718()*0.192118);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352a60() {
   return (neuron0x934b918()*-0.184823);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352a88() {
   return (neuron0x934bb18()*0.141647);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352ab0() {
   return (neuron0x934bd18()*-0.245597);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352ad8() {
   return (neuron0x934bf30()*0.138932);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352b00() {
   return (neuron0x934c148()*-0.0773998);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352d28() {
   return (neuron0x933c098()*0.0429244);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352d50() {
   return (neuron0x933c240()*0.0310111);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352d78() {
   return (neuron0x934a980()*0.450835);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352da0() {
   return (neuron0x934aaf0()*-0.328118);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352dc8() {
   return (neuron0x934acf0()*-0.224525);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352df0() {
   return (neuron0x934af18()*0.322556);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352e18() {
   return (neuron0x934b118()*-0.20923);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352e40() {
   return (neuron0x934b318()*-0.0830058);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352e68() {
   return (neuron0x934b518()*-0.375907);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352e90() {
   return (neuron0x934b718()*0.0471922);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352eb8() {
   return (neuron0x934b918()*-0.253606);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352ee0() {
   return (neuron0x934bb18()*-0.301056);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352f08() {
   return (neuron0x934bd18()*-0.451039);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352f30() {
   return (neuron0x934bf30()*0.467113);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9352f58() {
   return (neuron0x934c148()*-0.0248555);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353180() {
   return (neuron0x933c098()*-0.183562);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93531a8() {
   return (neuron0x933c240()*0.0314717);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93531d0() {
   return (neuron0x934a980()*-0.0967607);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93531f8() {
   return (neuron0x934aaf0()*0.0172718);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353220() {
   return (neuron0x934acf0()*-0.312692);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353248() {
   return (neuron0x934af18()*0.429575);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353270() {
   return (neuron0x934b118()*-0.242379);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353298() {
   return (neuron0x934b318()*-0.41403);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93532c0() {
   return (neuron0x934b518()*0.284443);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93532e8() {
   return (neuron0x934b718()*0.365061);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353310() {
   return (neuron0x934b918()*-0.233191);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353338() {
   return (neuron0x934bb18()*-0.1437);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353360() {
   return (neuron0x934bd18()*-0.293304);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353388() {
   return (neuron0x934bf30()*0.464484);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93533b0() {
   return (neuron0x934c148()*-0.177439);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93535d8() {
   return (neuron0x933c098()*-0.0526355);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353600() {
   return (neuron0x933c240()*0.113908);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353628() {
   return (neuron0x934a980()*-0.27706);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353650() {
   return (neuron0x934aaf0()*-0.288633);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353678() {
   return (neuron0x934acf0()*-0.356193);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93536a0() {
   return (neuron0x934af18()*0.531099);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93536c8() {
   return (neuron0x934b118()*-0.411904);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93536f0() {
   return (neuron0x934b318()*-0.46222);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353718() {
   return (neuron0x934b518()*-0.2549);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353740() {
   return (neuron0x934b718()*0.100905);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353768() {
   return (neuron0x934b918()*0.262742);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353790() {
   return (neuron0x934bb18()*-0.353105);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93537b8() {
   return (neuron0x934bd18()*-0.478104);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93537e0() {
   return (neuron0x934bf30()*0.0221688);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353808() {
   return (neuron0x934c148()*-0.0500445);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353a30() {
   return (neuron0x933c098()*-0.299163);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353a58() {
   return (neuron0x933c240()*0.159983);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353a80() {
   return (neuron0x934a980()*-0.434919);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353aa8() {
   return (neuron0x934aaf0()*0.356622);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353ad0() {
   return (neuron0x934acf0()*0.0491292);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353af8() {
   return (neuron0x934af18()*0.138791);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353b20() {
   return (neuron0x934b118()*-0.35628);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353b48() {
   return (neuron0x934b318()*0.193383);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353b70() {
   return (neuron0x934b518()*0.0351342);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353b98() {
   return (neuron0x934b718()*-0.107203);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353bc0() {
   return (neuron0x934b918()*-0.309967);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353be8() {
   return (neuron0x934bb18()*-0.390894);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353c10() {
   return (neuron0x934bd18()*-0.430978);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353c38() {
   return (neuron0x934bf30()*0.208772);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353c60() {
   return (neuron0x934c148()*-0.223115);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353e88() {
   return (neuron0x933c098()*0.11195);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353eb0() {
   return (neuron0x933c240()*-0.226504);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353ed8() {
   return (neuron0x934a980()*-0.209218);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353f00() {
   return (neuron0x934aaf0()*0.388034);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353f28() {
   return (neuron0x934acf0()*0.104186);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353f50() {
   return (neuron0x934af18()*-0.426393);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353f78() {
   return (neuron0x934b118()*-0.395767);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353fa0() {
   return (neuron0x934b318()*0.370084);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353fc8() {
   return (neuron0x934b518()*-0.159859);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9353ff0() {
   return (neuron0x934b718()*-0.153592);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354018() {
   return (neuron0x934b918()*-0.375329);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354040() {
   return (neuron0x934bb18()*-0.353835);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354068() {
   return (neuron0x934bd18()*0.0354961);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354090() {
   return (neuron0x934bf30()*-0.0550033);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93540b8() {
   return (neuron0x934c148()*0.453664);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93542e0() {
   return (neuron0x933c098()*-0.226238);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354308() {
   return (neuron0x933c240()*0.160391);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354330() {
   return (neuron0x934a980()*-0.0201773);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354358() {
   return (neuron0x934aaf0()*0.182016);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354380() {
   return (neuron0x934acf0()*-0.0322617);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93543a8() {
   return (neuron0x934af18()*-0.0722617);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93543d0() {
   return (neuron0x934b118()*-0.18164);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93543f8() {
   return (neuron0x934b318()*0.368702);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354420() {
   return (neuron0x934b518()*-0.0825142);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354448() {
   return (neuron0x934b718()*0.194882);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354470() {
   return (neuron0x934b918()*0.340779);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354498() {
   return (neuron0x934bb18()*-0.0602934);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93544c0() {
   return (neuron0x934bd18()*-0.0997187);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93544e8() {
   return (neuron0x934bf30()*0.0163686);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354510() {
   return (neuron0x934c148()*-0.337157);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354750() {
   return (neuron0x933c098()*-0.0369001);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354778() {
   return (neuron0x933c240()*-0.059951);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93547a0() {
   return (neuron0x934a980()*-0.0404746);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93547c8() {
   return (neuron0x934aaf0()*0.371938);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93547f0() {
   return (neuron0x934acf0()*-0.21263);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354818() {
   return (neuron0x934af18()*-0.329891);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354840() {
   return (neuron0x934b118()*0.0703496);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354868() {
   return (neuron0x934b318()*-0.30938);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354890() {
   return (neuron0x934b518()*0.435597);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93548b8() {
   return (neuron0x934b718()*0.0566589);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93548e0() {
   return (neuron0x934b918()*0.0934061);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354908() {
   return (neuron0x934bb18()*0.3624);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354930() {
   return (neuron0x934bd18()*-0.231297);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354958() {
   return (neuron0x934bf30()*0.30453);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354980() {
   return (neuron0x934c148()*0.158749);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354bc0() {
   return (neuron0x933c098()*0.0373792);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354be8() {
   return (neuron0x933c240()*0.478398);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354c10() {
   return (neuron0x934a980()*-0.187792);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354c38() {
   return (neuron0x934aaf0()*-0.0616849);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354c60() {
   return (neuron0x934acf0()*-0.0259433);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354c88() {
   return (neuron0x934af18()*0.361805);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354cb0() {
   return (neuron0x934b118()*-0.329251);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354cd8() {
   return (neuron0x934b318()*-0.195721);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354d00() {
   return (neuron0x934b518()*-0.06148);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354d28() {
   return (neuron0x934b718()*-0.014444);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354d50() {
   return (neuron0x934b918()*0.236688);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354d78() {
   return (neuron0x934bb18()*0.0971741);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354da0() {
   return (neuron0x934bd18()*0.206657);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354dc8() {
   return (neuron0x934bf30()*0.132902);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9354df0() {
   return (neuron0x934c148()*-0.162276);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355030() {
   return (neuron0x933c098()*0.269887);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355058() {
   return (neuron0x933c240()*0.160882);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355080() {
   return (neuron0x934a980()*-0.218772);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93550a8() {
   return (neuron0x934aaf0()*-0.169253);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93550d0() {
   return (neuron0x934acf0()*0.340803);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93550f8() {
   return (neuron0x934af18()*-0.438643);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355120() {
   return (neuron0x934b118()*0.423947);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355148() {
   return (neuron0x934b318()*0.0883204);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355170() {
   return (neuron0x934b518()*0.158539);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355198() {
   return (neuron0x934b718()*0.348287);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93551c0() {
   return (neuron0x934b918()*-0.305723);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93551e8() {
   return (neuron0x934bb18()*-0.381786);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355210() {
   return (neuron0x934bd18()*0.260063);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355238() {
   return (neuron0x934bf30()*0.0711719);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355260() {
   return (neuron0x934c148()*-0.0449974);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93554a0() {
   return (neuron0x933c098()*-0.0778318);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93554c8() {
   return (neuron0x933c240()*-0.0478316);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93554f0() {
   return (neuron0x934a980()*-0.15544);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355518() {
   return (neuron0x934aaf0()*0.0600634);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355540() {
   return (neuron0x934acf0()*0.461581);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355568() {
   return (neuron0x934af18()*0.422221);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355590() {
   return (neuron0x934b118()*0.322386);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93555b8() {
   return (neuron0x934b318()*0.0104266);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93555e0() {
   return (neuron0x934b518()*-0.170673);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355608() {
   return (neuron0x934b718()*0.334162);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355630() {
   return (neuron0x934b918()*0.460319);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355658() {
   return (neuron0x934bb18()*0.297961);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355680() {
   return (neuron0x934bd18()*-0.231571);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93556a8() {
   return (neuron0x934bf30()*0.125204);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93556d0() {
   return (neuron0x934c148()*-0.336222);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9355910() {
   return (neuron0x933c098()*-0.105328);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93509b8() {
   return (neuron0x933c240()*0.337821);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93509e0() {
   return (neuron0x934a980()*-0.292206);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350a08() {
   return (neuron0x934aaf0()*0.258927);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350a68() {
   return (neuron0x934acf0()*-0.26264);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350a90() {
   return (neuron0x934af18()*0.175962);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350ab8() {
   return (neuron0x934b118()*0.114946);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350b18() {
   return (neuron0x934b318()*0.10951);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350b40() {
   return (neuron0x934b518()*0.567353);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350b68() {
   return (neuron0x934b718()*0.0288975);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350bc8() {
   return (neuron0x934b918()*-0.312638);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350bf0() {
   return (neuron0x934bb18()*-0.222806);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350c18() {
   return (neuron0x934bd18()*-0.315118);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350c78() {
   return (neuron0x934bf30()*-0.402857);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350ca0() {
   return (neuron0x934c148()*0.0031468);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350ed0() {
   return (neuron0x933c098()*-0.22875);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350d70() {
   return (neuron0x933c240()*-0.102745);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350e20() {
   return (neuron0x934a980()*-0.316313);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350f38() {
   return (neuron0x934aaf0()*-0.317447);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350f60() {
   return (neuron0x934acf0()*0.0844555);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350f88() {
   return (neuron0x934af18()*-0.322593);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9350fe8() {
   return (neuron0x934b118()*-0.0990534);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351010() {
   return (neuron0x934b318()*0.378924);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351038() {
   return (neuron0x934b518()*-0.268534);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351098() {
   return (neuron0x934b718()*-0.0106181);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93510c0() {
   return (neuron0x934b918()*-0.0529245);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93510e8() {
   return (neuron0x934bb18()*0.138301);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351148() {
   return (neuron0x934bd18()*-0.349962);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351170() {
   return (neuron0x934bf30()*0.208988);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351198() {
   return (neuron0x934c148()*-0.295598);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93513a0() {
   return (neuron0x933c098()*-0.420503);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351240() {
   return (neuron0x933c240()*-0.462034);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93512f0() {
   return (neuron0x934a980()*0.081688);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356b00() {
   return (neuron0x934aaf0()*-0.0497102);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356b28() {
   return (neuron0x934acf0()*0.0709481);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356b50() {
   return (neuron0x934af18()*-0.395469);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356b78() {
   return (neuron0x934b118()*-0.0194354);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356ba0() {
   return (neuron0x934b318()*-0.118481);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356bc8() {
   return (neuron0x934b518()*-0.388536);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356bf0() {
   return (neuron0x934b718()*0.290733);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356c18() {
   return (neuron0x934b918()*0.0459732);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356c40() {
   return (neuron0x934bb18()*0.00146658);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356c68() {
   return (neuron0x934bd18()*-0.378875);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356c90() {
   return (neuron0x934bf30()*0.22772);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356cb8() {
   return (neuron0x934c148()*0.0091345);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356ee0() {
   return (neuron0x933c098()*0.499158);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356f08() {
   return (neuron0x933c240()*-0.238744);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9356f30() {
   return (neuron0x934a980()*-0.121294);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93519a8() {
   return (neuron0x934aaf0()*-0.324017);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93519d0() {
   return (neuron0x934acf0()*0.319692);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93519f8() {
   return (neuron0x934af18()*-0.0873588);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351a20() {
   return (neuron0x934b118()*-0.203967);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351a48() {
   return (neuron0x934b318()*-0.206984);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351a70() {
   return (neuron0x934b518()*-0.141182);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351a98() {
   return (neuron0x934b718()*0.18433);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351ac0() {
   return (neuron0x934b918()*0.436619);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351ae8() {
   return (neuron0x934bb18()*-0.195399);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351b10() {
   return (neuron0x934bd18()*-0.355072);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351b38() {
   return (neuron0x934bf30()*0.149971);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351b60() {
   return (neuron0x934c148()*0.433304);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351d88() {
   return (neuron0x933c098()*0.13576);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351db0() {
   return (neuron0x933c240()*-0.409525);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351dd8() {
   return (neuron0x934a980()*-0.297942);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351e00() {
   return (neuron0x934aaf0()*-0.297446);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351e28() {
   return (neuron0x934acf0()*-0.41506);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351e50() {
   return (neuron0x934af18()*0.175977);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351e78() {
   return (neuron0x934b118()*-0.278548);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351ea0() {
   return (neuron0x934b318()*-0.382384);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351ec8() {
   return (neuron0x934b518()*0.456205);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351ef0() {
   return (neuron0x934b718()*-0.167782);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351f18() {
   return (neuron0x934b918()*0.133437);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351f40() {
   return (neuron0x934bb18()*-0.29949);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351f68() {
   return (neuron0x934bd18()*-0.194482);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351f90() {
   return (neuron0x934bf30()*-0.484569);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351fb8() {
   return (neuron0x934c148()*-0.387542);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9357fa8() {
   return (neuron0x933c098()*0.0561263);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9357fd0() {
   return (neuron0x933c240()*-0.0294884);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9357ff8() {
   return (neuron0x934a980()*-0.416502);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358020() {
   return (neuron0x934aaf0()*-0.121873);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358048() {
   return (neuron0x934acf0()*-0.544395);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358070() {
   return (neuron0x934af18()*0.391648);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358098() {
   return (neuron0x934b118()*-0.267919);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93580c0() {
   return (neuron0x934b318()*-0.462618);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93580e8() {
   return (neuron0x934b518()*-0.429639);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358110() {
   return (neuron0x934b718()*0.234857);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358138() {
   return (neuron0x934b918()*-0.512328);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358160() {
   return (neuron0x934bb18()*-0.160424);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358188() {
   return (neuron0x934bd18()*-0.143724);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93581b0() {
   return (neuron0x934bf30()*0.303754);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93581d8() {
   return (neuron0x934c148()*0.44956);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358400() {
   return (neuron0x933c098()*-0.448975);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358428() {
   return (neuron0x933c240()*0.160598);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358450() {
   return (neuron0x934a980()*0.145349);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358478() {
   return (neuron0x934aaf0()*-0.373145);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93584a0() {
   return (neuron0x934acf0()*-0.340405);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93584c8() {
   return (neuron0x934af18()*-0.132164);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93584f0() {
   return (neuron0x934b118()*-0.47339);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358518() {
   return (neuron0x934b318()*-0.102715);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358540() {
   return (neuron0x934b518()*-0.415163);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358568() {
   return (neuron0x934b718()*-0.385692);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358590() {
   return (neuron0x934b918()*-0.0603282);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93585b8() {
   return (neuron0x934bb18()*-0.303935);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93585e0() {
   return (neuron0x934bd18()*0.0676901);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358608() {
   return (neuron0x934bf30()*0.417162);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358630() {
   return (neuron0x934c148()*-0.215293);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358858() {
   return (neuron0x933c098()*0.301824);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358880() {
   return (neuron0x933c240()*-0.322195);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93588a8() {
   return (neuron0x934a980()*0.358195);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93588d0() {
   return (neuron0x934aaf0()*-0.16909);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93588f8() {
   return (neuron0x934acf0()*0.0769306);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358920() {
   return (neuron0x934af18()*-0.124837);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358948() {
   return (neuron0x934b118()*0.130426);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358970() {
   return (neuron0x934b318()*0.289351);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358998() {
   return (neuron0x934b518()*-0.188893);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93589c0() {
   return (neuron0x934b718()*-0.123832);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93589e8() {
   return (neuron0x934b918()*0.477291);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358a10() {
   return (neuron0x934bb18()*0.41464);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358a38() {
   return (neuron0x934bd18()*-0.238979);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358a60() {
   return (neuron0x934bf30()*-0.050671);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358a88() {
   return (neuron0x934c148()*-0.492611);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358cb0() {
   return (neuron0x933c098()*0.410109);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358cd8() {
   return (neuron0x933c240()*0.0948768);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358d00() {
   return (neuron0x934a980()*-0.460092);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358d28() {
   return (neuron0x934aaf0()*0.477287);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358d50() {
   return (neuron0x934acf0()*0.233456);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358d78() {
   return (neuron0x934af18()*-0.307152);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358da0() {
   return (neuron0x934b118()*0.290531);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358dc8() {
   return (neuron0x934b318()*-0.00447595);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358df0() {
   return (neuron0x934b518()*-0.222281);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358e18() {
   return (neuron0x934b718()*-0.295306);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358e40() {
   return (neuron0x934b918()*0.42524);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358e68() {
   return (neuron0x934bb18()*0.417877);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358e90() {
   return (neuron0x934bd18()*0.00377009);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358eb8() {
   return (neuron0x934bf30()*0.347803);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9358ee0() {
   return (neuron0x934c148()*0.340124);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359108() {
   return (neuron0x933c098()*-0.369132);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359130() {
   return (neuron0x933c240()*0.344225);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359158() {
   return (neuron0x934a980()*-0.145157);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359180() {
   return (neuron0x934aaf0()*-0.221906);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93591a8() {
   return (neuron0x934acf0()*0.0846669);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93591d0() {
   return (neuron0x934af18()*0.268724);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93591f8() {
   return (neuron0x934b118()*-0.0929282);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359220() {
   return (neuron0x934b318()*0.0959817);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359248() {
   return (neuron0x934b518()*-0.141251);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359270() {
   return (neuron0x934b718()*-0.0368445);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359298() {
   return (neuron0x934b918()*-0.329304);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93592c0() {
   return (neuron0x934bb18()*0.0190193);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93592e8() {
   return (neuron0x934bd18()*-0.0691447);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359310() {
   return (neuron0x934bf30()*0.282541);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359338() {
   return (neuron0x934c148()*-0.344461);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359578() {
   return (neuron0x933c098()*0.178377);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93595a0() {
   return (neuron0x933c240()*-0.163852);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93595c8() {
   return (neuron0x934a980()*-0.0169897);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93595f0() {
   return (neuron0x934aaf0()*-0.0229111);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359618() {
   return (neuron0x934acf0()*-0.226339);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359640() {
   return (neuron0x934af18()*-0.352247);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359668() {
   return (neuron0x934b118()*0.0170645);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359690() {
   return (neuron0x934b318()*0.42747);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93596b8() {
   return (neuron0x934b518()*-0.0237016);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93596e0() {
   return (neuron0x934b718()*-0.243625);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359708() {
   return (neuron0x934b918()*0.0239701);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359730() {
   return (neuron0x934bb18()*0.101945);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359758() {
   return (neuron0x934bd18()*-0.466569);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359780() {
   return (neuron0x934bf30()*0.340303);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93597a8() {
   return (neuron0x934c148()*-0.402144);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93599e8() {
   return (neuron0x933c098()*0.412148);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359a10() {
   return (neuron0x933c240()*0.0643904);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359a38() {
   return (neuron0x934a980()*0.0887462);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359a60() {
   return (neuron0x934aaf0()*-0.206504);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359a88() {
   return (neuron0x934acf0()*-0.283669);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359ab0() {
   return (neuron0x934af18()*0.256566);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359ad8() {
   return (neuron0x934b118()*0.105962);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359b00() {
   return (neuron0x934b318()*-0.0938824);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359b28() {
   return (neuron0x934b518()*0.371606);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359b50() {
   return (neuron0x934b718()*0.00249927);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359b78() {
   return (neuron0x934b918()*-0.214134);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359ba0() {
   return (neuron0x934bb18()*0.218844);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359bc8() {
   return (neuron0x934bd18()*-0.474182);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359bf0() {
   return (neuron0x934bf30()*0.339812);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359c18() {
   return (neuron0x934c148()*0.476279);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359e58() {
   return (neuron0x933c098()*0.14905);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359e80() {
   return (neuron0x933c240()*-0.521711);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359ea8() {
   return (neuron0x934a980()*-0.233439);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359ed0() {
   return (neuron0x934aaf0()*0.0358513);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359ef8() {
   return (neuron0x934acf0()*-0.00301033);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359f20() {
   return (neuron0x934af18()*0.0505787);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359f48() {
   return (neuron0x934b118()*-0.11208);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359f70() {
   return (neuron0x934b318()*0.385026);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359f98() {
   return (neuron0x934b518()*0.196826);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359fc0() {
   return (neuron0x934b718()*0.251039);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9359fe8() {
   return (neuron0x934b918()*-0.0846194);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a010() {
   return (neuron0x934bb18()*0.398482);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a038() {
   return (neuron0x934bd18()*0.114022);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a060() {
   return (neuron0x934bf30()*0.0349681);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a088() {
   return (neuron0x934c148()*0.307426);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a2c8() {
   return (neuron0x933c098()*0.400316);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a2f0() {
   return (neuron0x933c240()*0.245323);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a318() {
   return (neuron0x934a980()*0.124039);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a340() {
   return (neuron0x934aaf0()*0.115706);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a368() {
   return (neuron0x934acf0()*-0.14224);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a390() {
   return (neuron0x934af18()*-0.340618);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a3b8() {
   return (neuron0x934b118()*0.335523);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a3e0() {
   return (neuron0x934b318()*0.165993);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a408() {
   return (neuron0x934b518()*-0.094108);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a430() {
   return (neuron0x934b718()*-0.341632);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a458() {
   return (neuron0x934b918()*-0.374008);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a480() {
   return (neuron0x934bb18()*0.116682);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a4a8() {
   return (neuron0x934bd18()*0.35263);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a4d0() {
   return (neuron0x934bf30()*0.0334597);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a4f8() {
   return (neuron0x934c148()*0.185533);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a738() {
   return (neuron0x933c098()*-0.259964);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a760() {
   return (neuron0x933c240()*-0.411033);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a788() {
   return (neuron0x934a980()*0.0303741);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a7b0() {
   return (neuron0x934aaf0()*0.398588);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a7d8() {
   return (neuron0x934acf0()*-0.167383);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a800() {
   return (neuron0x934af18()*-0.0982159);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a828() {
   return (neuron0x934b118()*0.314442);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a850() {
   return (neuron0x934b318()*0.323251);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a878() {
   return (neuron0x934b518()*-0.29285);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a8a0() {
   return (neuron0x934b718()*0.270722);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a8c8() {
   return (neuron0x934b918()*0.225983);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a8f0() {
   return (neuron0x934bb18()*0.296027);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a918() {
   return (neuron0x934bd18()*0.261112);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a940() {
   return (neuron0x934bf30()*0.250196);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935a968() {
   return (neuron0x934c148()*-0.456663);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935aba8() {
   return (neuron0x933c098()*0.0888975);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935abd0() {
   return (neuron0x933c240()*0.0675721);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935abf8() {
   return (neuron0x934a980()*0.0497716);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ac20() {
   return (neuron0x934aaf0()*0.180286);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ac48() {
   return (neuron0x934acf0()*0.0391094);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ac70() {
   return (neuron0x934af18()*0.0884815);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ac98() {
   return (neuron0x934b118()*0.0617586);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935acc0() {
   return (neuron0x934b318()*0.124824);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ace8() {
   return (neuron0x934b518()*0.0619742);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ad10() {
   return (neuron0x934b718()*0.373272);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ad38() {
   return (neuron0x934b918()*0.214854);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ad60() {
   return (neuron0x934bb18()*-0.0405228);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ad88() {
   return (neuron0x934bd18()*-0.448445);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935adb0() {
   return (neuron0x934bf30()*-0.139706);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935add8() {
   return (neuron0x934c148()*-0.0317944);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b018() {
   return (neuron0x933c098()*-0.34621);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b040() {
   return (neuron0x933c240()*-0.46991);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b068() {
   return (neuron0x934a980()*0.00419054);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b090() {
   return (neuron0x934aaf0()*0.109265);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b0b8() {
   return (neuron0x934acf0()*-0.102816);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b0e0() {
   return (neuron0x934af18()*-0.0619527);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b108() {
   return (neuron0x934b118()*0.27501);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b130() {
   return (neuron0x934b318()*0.291763);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b158() {
   return (neuron0x934b518()*0.483643);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b180() {
   return (neuron0x934b718()*0.362622);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b1a8() {
   return (neuron0x934b918()*0.349815);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b1d0() {
   return (neuron0x934bb18()*0.300265);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b1f8() {
   return (neuron0x934bd18()*0.114153);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b220() {
   return (neuron0x934bf30()*-0.305813);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b248() {
   return (neuron0x934c148()*-0.101045);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b488() {
   return (neuron0x933c098()*-0.347546);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b4b0() {
   return (neuron0x933c240()*-0.0272833);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b4d8() {
   return (neuron0x934a980()*0.268178);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b500() {
   return (neuron0x934aaf0()*-0.00561188);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b528() {
   return (neuron0x934acf0()*-0.378422);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b550() {
   return (neuron0x934af18()*0.306001);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b578() {
   return (neuron0x934b118()*-0.202331);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b5a0() {
   return (neuron0x934b318()*-0.0664083);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b5c8() {
   return (neuron0x934b518()*-0.514952);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b5f0() {
   return (neuron0x934b718()*0.137524);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b618() {
   return (neuron0x934b918()*-0.331127);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b640() {
   return (neuron0x934bb18()*-0.43524);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b668() {
   return (neuron0x934bd18()*0.48688);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b690() {
   return (neuron0x934bf30()*0.130239);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935b6b8() {
   return (neuron0x934c148()*-0.131655);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93518a0() {
   return (neuron0x933c098()*-0.253665);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93518c8() {
   return (neuron0x933c240()*-0.305318);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x93518f0() {
   return (neuron0x934a980()*-0.0065176);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351918() {
   return (neuron0x934aaf0()*-0.0733024);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351940() {
   return (neuron0x934acf0()*-0.188048);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x9351968() {
   return (neuron0x934af18()*-0.0234856);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bb00() {
   return (neuron0x934b118()*0.145056);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bb28() {
   return (neuron0x934b318()*0.197121);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bb50() {
   return (neuron0x934b518()*0.490123);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bb78() {
   return (neuron0x934b718()*0.423374);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bba0() {
   return (neuron0x934b918()*0.170335);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bbc8() {
   return (neuron0x934bb18()*-0.0914207);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bbf0() {
   return (neuron0x934bd18()*-0.479471);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bc18() {
   return (neuron0x934bf30()*-0.11601);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bc40() {
   return (neuron0x934c148()*-0.00417608);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935be68() {
   return (neuron0x933c098()*0.148905);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935be90() {
   return (neuron0x933c240()*-0.221396);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935beb8() {
   return (neuron0x934a980()*0.110584);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bee0() {
   return (neuron0x934aaf0()*-0.209536);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bf08() {
   return (neuron0x934acf0()*-0.204239);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bf30() {
   return (neuron0x934af18()*-0.239157);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bf58() {
   return (neuron0x934b118()*-0.0313924);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bf80() {
   return (neuron0x934b318()*-0.50852);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bfa8() {
   return (neuron0x934b518()*0.271963);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bfd0() {
   return (neuron0x934b718()*0.0879136);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935bff8() {
   return (neuron0x934b918()*-0.20143);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c020() {
   return (neuron0x934bb18()*-0.574043);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c048() {
   return (neuron0x934bd18()*-0.282365);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c070() {
   return (neuron0x934bf30()*-0.181523);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c098() {
   return (neuron0x934c148()*0.467721);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c2d8() {
   return (neuron0x933c098()*0.22643);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c300() {
   return (neuron0x933c240()*-0.388423);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c328() {
   return (neuron0x934a980()*-0.174154);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c350() {
   return (neuron0x934aaf0()*-0.0923618);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c378() {
   return (neuron0x934acf0()*0.295324);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c3a0() {
   return (neuron0x934af18()*-0.0458861);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c3c8() {
   return (neuron0x934b118()*0.0780991);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c3f0() {
   return (neuron0x934b318()*-0.0561666);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c418() {
   return (neuron0x934b518()*0.137319);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c440() {
   return (neuron0x934b718()*0.23902);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c468() {
   return (neuron0x934b918()*-0.347904);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c490() {
   return (neuron0x934bb18()*0.334244);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c4b8() {
   return (neuron0x934bd18()*0.0304648);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c4e0() {
   return (neuron0x934bf30()*0.296789);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c508() {
   return (neuron0x934c148()*-0.0895992);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c748() {
   return (neuron0x933c098()*-0.31168);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c770() {
   return (neuron0x933c240()*-0.233002);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c798() {
   return (neuron0x934a980()*-0.211792);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c7c0() {
   return (neuron0x934aaf0()*0.236768);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c7e8() {
   return (neuron0x934acf0()*0.174874);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c810() {
   return (neuron0x934af18()*0.280235);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c838() {
   return (neuron0x934b118()*-0.333297);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c860() {
   return (neuron0x934b318()*-0.0376815);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c888() {
   return (neuron0x934b518()*0.0675772);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c8b0() {
   return (neuron0x934b718()*-0.403256);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c8d8() {
   return (neuron0x934b918()*0.102911);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c900() {
   return (neuron0x934bb18()*0.137257);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c928() {
   return (neuron0x934bd18()*0.147796);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c950() {
   return (neuron0x934bf30()*0.056014);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935c978() {
   return (neuron0x934c148()*0.221455);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cbb8() {
   return (neuron0x933c098()*0.365148);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cbe0() {
   return (neuron0x933c240()*-0.296268);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cc08() {
   return (neuron0x934a980()*0.383188);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cc30() {
   return (neuron0x934aaf0()*0.183098);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cc58() {
   return (neuron0x934acf0()*-0.160227);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cc80() {
   return (neuron0x934af18()*0.194314);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cca8() {
   return (neuron0x934b118()*0.00895813);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ccd0() {
   return (neuron0x934b318()*0.452757);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ccf8() {
   return (neuron0x934b518()*0.0764178);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cd20() {
   return (neuron0x934b718()*0.0380948);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cd48() {
   return (neuron0x934b918()*0.276894);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cd70() {
   return (neuron0x934bb18()*0.257723);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cd98() {
   return (neuron0x934bd18()*0.424474);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cdc0() {
   return (neuron0x934bf30()*0.0344725);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935cde8() {
   return (neuron0x934c148()*-0.0775365);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d028() {
   return (neuron0x933c098()*-0.189985);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d050() {
   return (neuron0x933c240()*-0.19675);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d078() {
   return (neuron0x934a980()*-0.210685);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d0a0() {
   return (neuron0x934aaf0()*-0.22618);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d0c8() {
   return (neuron0x934acf0()*-0.449861);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d0f0() {
   return (neuron0x934af18()*0.368354);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d118() {
   return (neuron0x934b118()*-0.197136);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d140() {
   return (neuron0x934b318()*0.00381336);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d168() {
   return (neuron0x934b518()*-0.129493);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d190() {
   return (neuron0x934b718()*0.14426);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d1b8() {
   return (neuron0x934b918()*-0.391307);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d1e0() {
   return (neuron0x934bb18()*-0.406756);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d208() {
   return (neuron0x934bd18()*-0.176711);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d230() {
   return (neuron0x934bf30()*0.203524);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d258() {
   return (neuron0x934c148()*0.0245279);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d498() {
   return (neuron0x933c098()*-0.0861084);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d4c0() {
   return (neuron0x933c240()*-0.256596);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d4e8() {
   return (neuron0x934a980()*-0.185726);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d510() {
   return (neuron0x934aaf0()*-0.104832);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d538() {
   return (neuron0x934acf0()*0.0437232);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d560() {
   return (neuron0x934af18()*-0.355354);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d588() {
   return (neuron0x934b118()*-0.243591);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d5b0() {
   return (neuron0x934b318()*-0.298033);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d5d8() {
   return (neuron0x934b518()*-0.502172);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d600() {
   return (neuron0x934b718()*0.370101);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d628() {
   return (neuron0x934b918()*-0.279917);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d650() {
   return (neuron0x934bb18()*-0.0931929);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d678() {
   return (neuron0x934bd18()*-0.153456);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d6a0() {
   return (neuron0x934bf30()*-0.33547);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d6c8() {
   return (neuron0x934c148()*-0.456739);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d908() {
   return (neuron0x933c098()*-0.377457);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d930() {
   return (neuron0x933c240()*0.098311);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d958() {
   return (neuron0x934a980()*-0.00173808);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d980() {
   return (neuron0x934aaf0()*0.118097);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d9a8() {
   return (neuron0x934acf0()*0.117071);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d9d0() {
   return (neuron0x934af18()*0.343701);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935d9f8() {
   return (neuron0x934b118()*-0.0833623);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935da20() {
   return (neuron0x934b318()*0.182238);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935da48() {
   return (neuron0x934b518()*-0.176428);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935da70() {
   return (neuron0x934b718()*0.088666);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935da98() {
   return (neuron0x934b918()*-0.212502);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935dac0() {
   return (neuron0x934bb18()*-0.22502);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935dae8() {
   return (neuron0x934bd18()*-0.0181602);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935db10() {
   return (neuron0x934bf30()*-0.0774874);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935db38() {
   return (neuron0x934c148()*0.125845);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935dd78() {
   return (neuron0x933c098()*0.279322);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935dda0() {
   return (neuron0x933c240()*0.0779968);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ddc8() {
   return (neuron0x934a980()*0.131247);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ddf0() {
   return (neuron0x934aaf0()*0.213374);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935de18() {
   return (neuron0x934acf0()*-0.311259);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935de40() {
   return (neuron0x934af18()*-0.500136);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935de68() {
   return (neuron0x934b118()*0.195794);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935de90() {
   return (neuron0x934b318()*-0.0879249);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935deb8() {
   return (neuron0x934b518()*0.168205);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935dee0() {
   return (neuron0x934b718()*-0.238161);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935df08() {
   return (neuron0x934b918()*0.367884);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935df30() {
   return (neuron0x934bb18()*0.218968);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935df58() {
   return (neuron0x934bd18()*-0.370206);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935df80() {
   return (neuron0x934bf30()*-0.0100673);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935dfa8() {
   return (neuron0x934c148()*-0.255994);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e0f8() {
   return (neuron0x934c488()*-0.00196443);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e120() {
   return (neuron0x934c7f8()*0.00985259);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e148() {
   return (neuron0x934cd60()*0.0406136);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e170() {
   return (neuron0x934d1b8()*-0.00502889);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e198() {
   return (neuron0x934d610()*0.00369577);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e1c0() {
   return (neuron0x934dae0()*-0.0520335);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e1e8() {
   return (neuron0x934de60()*0.0353677);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e210() {
   return (neuron0x934e2b8()*0.0228778);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e238() {
   return (neuron0x934e710()*-0.0080941);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e260() {
   return (neuron0x934ee80()*-0.0182668);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e288() {
   return (neuron0x934f1d8()*0.00732239);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e2b0() {
   return (neuron0x934f630()*0.0189966);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e2d8() {
   return (neuron0x934fa88()*-0.0128574);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e300() {
   return (neuron0x934fee0()*0.0428953);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e328() {
   return (neuron0x9350338()*-0.0521082);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e350() {
   return (neuron0x9350790()*0.00882352);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e400() {
   return (neuron0x93513e0()*0.0164026);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e428() {
   return (neuron0x9351760()*0.00807342);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e450() {
   return (neuron0x934eac8()*-0.0228858);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e478() {
   return (neuron0x9352278()*0.01857);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e4a0() {
   return (neuron0x93526d0()*0.00947104);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e4c8() {
   return (neuron0x9352b28()*-0.0346966);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e4f0() {
   return (neuron0x9352f80()*-0.0207187);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e518() {
   return (neuron0x93533d8()*-0.00587218);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e540() {
   return (neuron0x9353830()*0.00573451);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e568() {
   return (neuron0x9353c88()*0.000263766);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e590() {
   return (neuron0x93540e0()*-0.0285419);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e5b8() {
   return (neuron0x9354538()*0.030154);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e5e0() {
   return (neuron0x93549a8()*-0.00198067);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e608() {
   return (neuron0x9354e18()*-0.00366608);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e630() {
   return (neuron0x9355288()*0.00147827);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e658() {
   return (neuron0x93556f8()*0.0244539);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e378() {
   return (neuron0x93568b0()*0.0593078);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e3a0() {
   return (neuron0x93569d8()*-0.0105456);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e3c8() {
   return (neuron0x9356ce0()*0.0129698);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e788() {
   return (neuron0x9351b88()*0.00907445);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e7b0() {
   return (neuron0x9351fe0()*0.0291119);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e7d8() {
   return (neuron0x9358200()*-0.0109192);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e800() {
   return (neuron0x9358658()*-0.00241767);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e828() {
   return (neuron0x9358ab0()*0.00305118);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e850() {
   return (neuron0x9358f08()*-0.00366714);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e878() {
   return (neuron0x9359360()*0.00186533);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e8a0() {
   return (neuron0x93597d0()*0.0133808);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e8c8() {
   return (neuron0x9359c40()*-0.0205784);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e8f0() {
   return (neuron0x935a0b0()*-0.00247255);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e918() {
   return (neuron0x935a520()*0.0422325);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e940() {
   return (neuron0x935a990()*0.0283918);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e968() {
   return (neuron0x935ae00()*0.00180318);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e990() {
   return (neuron0x935b270()*0.117568);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e9b8() {
   return (neuron0x935b6e0()*-0.0241991);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935e9e0() {
   return (neuron0x935bc68()*0.0551426);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ea08() {
   return (neuron0x935c0c0()*0.0284681);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ea30() {
   return (neuron0x935c530()*-0.0124426);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ea58() {
   return (neuron0x935c9a0()*0.00290639);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ea80() {
   return (neuron0x935ce10()*-0.10199);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935eaa8() {
   return (neuron0x935d280()*0.00536411);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935ead0() {
   return (neuron0x935d6f0()*0.0256183);
}

double NNParaElectronAngleCorrectionBCPDirPhi::synapse0x935eaf8() {
   return (neuron0x935db60()*-0.00365912);
}

