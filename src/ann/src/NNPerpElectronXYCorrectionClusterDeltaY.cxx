#include "NNPerpElectronXYCorrectionClusterDeltaY.h"
#include <cmath>

double NNPerpElectronXYCorrectionClusterDeltaY::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10) {
   input0 = (in0 - 1981.96)/1031.12;
   input1 = (in1 - -4.35365)/33.4012;
   input2 = (in2 - -6.95005)/60.2436;
   input3 = (in3 - 1.30476)/0.378059;
   input4 = (in4 - 1.40003)/0.392909;
   input5 = (in5 - 0.291103)/1.94696;
   input6 = (in6 - 0.288522)/1.77385;
   input7 = (in7 - 6.61681)/9.86309;
   input8 = (in8 - 5.27731)/9.73683;
   input9 = (in9 - -0.00558485)/0.751931;
   input10 = (in10 - 0.0179736)/0.743436;
   switch(index) {
     case 0:
         return neuron0x91205c8();
     default:
         return 0.;
   }
}

double NNPerpElectronXYCorrectionClusterDeltaY::Value(int index, double* input) {
   input0 = (input[0] - 1981.96)/1031.12;
   input1 = (input[1] - -4.35365)/33.4012;
   input2 = (input[2] - -6.95005)/60.2436;
   input3 = (input[3] - 1.30476)/0.378059;
   input4 = (input[4] - 1.40003)/0.392909;
   input5 = (input[5] - 0.291103)/1.94696;
   input6 = (input[6] - 0.288522)/1.77385;
   input7 = (input[7] - 6.61681)/9.86309;
   input8 = (input[8] - 5.27731)/9.73683;
   input9 = (input[9] - -0.00558485)/0.751931;
   input10 = (input[10] - 0.0179736)/0.743436;
   switch(index) {
     case 0:
         return neuron0x91205c8();
     default:
         return 0.;
   }
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x90f5bc0() {
   return input0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x90f5dc0() {
   return input1;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91043d0() {
   return input2;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91044f8() {
   return input3;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91046b0() {
   return input4;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91048b0() {
   return input5;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9104ab0() {
   return input6;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9104cc8() {
   return input7;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9104ee0() {
   return input8;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91050f8() {
   return input9;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91052f8() {
   return input10;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9105638() {
   double input = 0.657312;
   input += synapse0x8f34500();
   input += synapse0x91057f0();
   input += synapse0x9105818();
   input += synapse0x9105840();
   input += synapse0x9105868();
   input += synapse0x9105890();
   input += synapse0x91058b8();
   input += synapse0x91058e0();
   input += synapse0x9105908();
   input += synapse0x9105930();
   input += synapse0x9105958();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9105638() {
   double input = input0x9105638();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9105980() {
   double input = 1.08802;
   input += synapse0x9105b80();
   input += synapse0x9105ba8();
   input += synapse0x9105bd0();
   input += synapse0x9105bf8();
   input += synapse0x9105c20();
   input += synapse0x9105c48();
   input += synapse0x9105cf8();
   input += synapse0x9105d20();
   input += synapse0x9105d48();
   input += synapse0x9105d70();
   input += synapse0x9105d98();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9105980() {
   double input = input0x9105980();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9105dc0() {
   double input = -0.449713;
   input += synapse0x9105f78();
   input += synapse0x9105fa0();
   input += synapse0x9105fc8();
   input += synapse0x9105ff0();
   input += synapse0x9106018();
   input += synapse0x9106040();
   input += synapse0x9106068();
   input += synapse0x9106090();
   input += synapse0x91060b8();
   input += synapse0x91060e0();
   input += synapse0x9106108();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9105dc0() {
   double input = input0x9105dc0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9106238() {
   double input = 0.422742;
   input += synapse0x9105cb8();
   input += synapse0x91063f0();
   input += synapse0x9106418();
   input += synapse0x9106440();
   input += synapse0x9106468();
   input += synapse0x9106490();
   input += synapse0x91064b8();
   input += synapse0x91064e0();
   input += synapse0x9106508();
   input += synapse0x9106530();
   input += synapse0x9106558();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9106238() {
   double input = input0x9106238();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9106580() {
   double input = -0.240746;
   input += synapse0x9106780();
   input += synapse0x91067a8();
   input += synapse0x91067d0();
   input += synapse0x91067f8();
   input += synapse0x9106820();
   input += synapse0x9106848();
   input += synapse0x9106870();
   input += synapse0x9106898();
   input += synapse0x91068c0();
   input += synapse0x91068e8();
   input += synapse0x9106910();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9106580() {
   double input = input0x9106580();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9106938() {
   double input = 0.143381;
   input += synapse0x9106bc0();
   input += synapse0x9106be8();
   input += synapse0x9106c10();
   input += synapse0x9106c38();
   input += synapse0x9106c60();
   input += synapse0x9106c88();
   input += synapse0x9106cb0();
   input += synapse0x9106cd8();
   input += synapse0x9106d00();
   input += synapse0x9106d28();
   input += synapse0x8f34340();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9106938() {
   double input = input0x9106938();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9106f58() {
   double input = -0.337276;
   input += synapse0x8f344c8();
   input += synapse0x9106130();
   input += synapse0x9106158();
   input += synapse0x9106180();
   input += synapse0x91061a8();
   input += synapse0x91061d0();
   input += synapse0x91061f8();
   input += synapse0x9107080();
   input += synapse0x91070a8();
   input += synapse0x91070d0();
   input += synapse0x91070f8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9106f58() {
   double input = input0x9106f58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9107120() {
   double input = -0.537298;
   input += synapse0x9107320();
   input += synapse0x9107348();
   input += synapse0x9107370();
   input += synapse0x9107398();
   input += synapse0x91073c0();
   input += synapse0x91073e8();
   input += synapse0x9107410();
   input += synapse0x9107438();
   input += synapse0x9107460();
   input += synapse0x9107488();
   input += synapse0x91074b0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9107120() {
   double input = input0x9107120();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91074d8() {
   double input = 0.57093;
   input += synapse0x91076d8();
   input += synapse0x9107700();
   input += synapse0x9107728();
   input += synapse0x9107750();
   input += synapse0x9107778();
   input += synapse0x91077a0();
   input += synapse0x91077c8();
   input += synapse0x91077f0();
   input += synapse0x9107818();
   input += synapse0x9107840();
   input += synapse0x9107868();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91074d8() {
   double input = input0x91074d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9107890() {
   double input = 0.245168;
   input += synapse0x9107a90();
   input += synapse0x9107ab8();
   input += synapse0x9107ae0();
   input += synapse0x9107b08();
   input += synapse0x9107b30();
   input += synapse0x9107b58();
   input += synapse0x9107b80();
   input += synapse0x9107ba8();
   input += synapse0x9107bd0();
   input += synapse0x9107bf8();
   input += synapse0x9107c20();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9107890() {
   double input = input0x9107890();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9107c48() {
   double input = 0.43274;
   input += synapse0x9107e48();
   input += synapse0x9107e70();
   input += synapse0x9107e98();
   input += synapse0x9107ec0();
   input += synapse0x9107ee8();
   input += synapse0x9107f10();
   input += synapse0x9107f38();
   input += synapse0x9107f60();
   input += synapse0x9107f88();
   input += synapse0x9107fb0();
   input += synapse0x9107fd8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9107c48() {
   double input = input0x9107c48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9108000() {
   double input = -1.70805;
   input += synapse0x9108200();
   input += synapse0x9108228();
   input += synapse0x9108250();
   input += synapse0x9108278();
   input += synapse0x91082a0();
   input += synapse0x91082c8();
   input += synapse0x91082f0();
   input += synapse0x9108318();
   input += synapse0x9106d50();
   input += synapse0x9106d78();
   input += synapse0x9106da0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9108000() {
   double input = input0x9108000();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9106dc8() {
   double input = -0.561534;
   input += synapse0x91087d8();
   input += synapse0x9108800();
   input += synapse0x9108828();
   input += synapse0x9108850();
   input += synapse0x9108878();
   input += synapse0x91088a0();
   input += synapse0x91088c8();
   input += synapse0x91088f0();
   input += synapse0x9108918();
   input += synapse0x9108940();
   input += synapse0x9108968();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9106dc8() {
   double input = input0x9106dc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9108990() {
   double input = 0.344744;
   input += synapse0x9108b90();
   input += synapse0x9108bb8();
   input += synapse0x9108be0();
   input += synapse0x9108c08();
   input += synapse0x9108c30();
   input += synapse0x9108c58();
   input += synapse0x9108c80();
   input += synapse0x9108ca8();
   input += synapse0x9108cd0();
   input += synapse0x9108cf8();
   input += synapse0x9108d20();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9108990() {
   double input = input0x9108990();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9108d48() {
   double input = 0.865159;
   input += synapse0x9108f48();
   input += synapse0x9108f70();
   input += synapse0x9108f98();
   input += synapse0x9108fc0();
   input += synapse0x9108fe8();
   input += synapse0x9109010();
   input += synapse0x9109038();
   input += synapse0x9109060();
   input += synapse0x9109088();
   input += synapse0x91090b0();
   input += synapse0x91090d8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9108d48() {
   double input = input0x9108d48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9109100() {
   double input = -0.254413;
   input += synapse0x9109300();
   input += synapse0x91093b0();
   input += synapse0x9109460();
   input += synapse0x9109510();
   input += synapse0x91095c0();
   input += synapse0x9109670();
   input += synapse0x9109720();
   input += synapse0x91097d0();
   input += synapse0x9109880();
   input += synapse0x9109930();
   input += synapse0x91099e0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9109100() {
   double input = input0x9109100();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9109a90() {
   double input = -1.35073;
   input += synapse0x9109bb8();
   input += synapse0x9109be0();
   input += synapse0x9109c08();
   input += synapse0x9109c30();
   input += synapse0x9109c58();
   input += synapse0x9109c80();
   input += synapse0x9109ca8();
   input += synapse0x9109cd0();
   input += synapse0x9109cf8();
   input += synapse0x9109d20();
   input += synapse0x9109d48();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9109a90() {
   double input = input0x9109a90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9109d70() {
   double input = 0.713596;
   input += synapse0x9109e98();
   input += synapse0x9109ec0();
   input += synapse0x9109ee8();
   input += synapse0x9109f10();
   input += synapse0x9109f38();
   input += synapse0x9109f60();
   input += synapse0x9109f88();
   input += synapse0x9109fb0();
   input += synapse0x9109fd8();
   input += synapse0x910a000();
   input += synapse0x910a028();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9109d70() {
   double input = input0x9109d70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910a050() {
   double input = 0.400722;
   input += synapse0x910a178();
   input += synapse0x910a1a0();
   input += synapse0x910a1c8();
   input += synapse0x910a1f0();
   input += synapse0x910a218();
   input += synapse0x910a240();
   input += synapse0x910a268();
   input += synapse0x910a290();
   input += synapse0x910a2b8();
   input += synapse0x910a2e0();
   input += synapse0x910a308();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910a050() {
   double input = input0x910a050();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910a330() {
   double input = -0.449115;
   input += synapse0x910a4a0();
   input += synapse0x910a4c8();
   input += synapse0x910a4f0();
   input += synapse0x910a518();
   input += synapse0x910a540();
   input += synapse0x910a568();
   input += synapse0x910a590();
   input += synapse0x910a5b8();
   input += synapse0x910a5e0();
   input += synapse0x910a608();
   input += synapse0x910a630();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910a330() {
   double input = input0x910a330();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910a658() {
   double input = -1.33549;
   input += synapse0x910a858();
   input += synapse0x910a880();
   input += synapse0x910a8a8();
   input += synapse0x910a8d0();
   input += synapse0x910a8f8();
   input += synapse0x910a920();
   input += synapse0x910a948();
   input += synapse0x910a970();
   input += synapse0x910a998();
   input += synapse0x910a9c0();
   input += synapse0x910a9e8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910a658() {
   double input = input0x910a658();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910aa10() {
   double input = -0.157044;
   input += synapse0x9106b38();
   input += synapse0x9106b60();
   input += synapse0x9106b88();
   input += synapse0x910ad30();
   input += synapse0x910ad58();
   input += synapse0x910ad80();
   input += synapse0x910ada8();
   input += synapse0x910add0();
   input += synapse0x910adf8();
   input += synapse0x910ae20();
   input += synapse0x910ae48();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910aa10() {
   double input = input0x910aa10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910ae70() {
   double input = -0.381893;
   input += synapse0x910b088();
   input += synapse0x910b0b0();
   input += synapse0x910b0d8();
   input += synapse0x910b100();
   input += synapse0x910b128();
   input += synapse0x910b150();
   input += synapse0x910b178();
   input += synapse0x910b1a0();
   input += synapse0x910b1c8();
   input += synapse0x910b1f0();
   input += synapse0x910b218();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910ae70() {
   double input = input0x910ae70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910b240() {
   double input = -0.486924;
   input += synapse0x910b458();
   input += synapse0x910b480();
   input += synapse0x910b4a8();
   input += synapse0x910b4d0();
   input += synapse0x9108340();
   input += synapse0x9108368();
   input += synapse0x9108390();
   input += synapse0x91083b8();
   input += synapse0x91083e0();
   input += synapse0x9108408();
   input += synapse0x9108430();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910b240() {
   double input = input0x910b240();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9108458() {
   double input = 0.411441;
   input += synapse0x9108670();
   input += synapse0x9108698();
   input += synapse0x91086c0();
   input += synapse0x91086e8();
   input += synapse0x9108710();
   input += synapse0x910bd00();
   input += synapse0x910bd28();
   input += synapse0x910bd50();
   input += synapse0x910bd78();
   input += synapse0x910bda0();
   input += synapse0x910bdc8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9108458() {
   double input = input0x9108458();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910bdf0() {
   double input = -0.119995;
   input += synapse0x910c008();
   input += synapse0x910c030();
   input += synapse0x910c058();
   input += synapse0x910c080();
   input += synapse0x910c0a8();
   input += synapse0x910c0d0();
   input += synapse0x910c0f8();
   input += synapse0x910c120();
   input += synapse0x910c148();
   input += synapse0x910c170();
   input += synapse0x910c198();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910bdf0() {
   double input = input0x910bdf0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910c1c0() {
   double input = -0.729619;
   input += synapse0x910c3d8();
   input += synapse0x910c400();
   input += synapse0x910c428();
   input += synapse0x910c450();
   input += synapse0x910c478();
   input += synapse0x910c4a0();
   input += synapse0x910c4c8();
   input += synapse0x910c4f0();
   input += synapse0x910c518();
   input += synapse0x910c540();
   input += synapse0x910c568();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910c1c0() {
   double input = input0x910c1c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910c590() {
   double input = 0.289689;
   input += synapse0x910c7a8();
   input += synapse0x910c7d0();
   input += synapse0x910c7f8();
   input += synapse0x910c820();
   input += synapse0x910c848();
   input += synapse0x910c870();
   input += synapse0x910c898();
   input += synapse0x910c8c0();
   input += synapse0x910c8e8();
   input += synapse0x910c910();
   input += synapse0x910c938();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910c590() {
   double input = input0x910c590();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910c960() {
   double input = 0.500394;
   input += synapse0x910cb78();
   input += synapse0x910cba0();
   input += synapse0x910cbc8();
   input += synapse0x910cbf0();
   input += synapse0x910cc18();
   input += synapse0x910cc40();
   input += synapse0x910cc68();
   input += synapse0x910cc90();
   input += synapse0x910ccb8();
   input += synapse0x910cce0();
   input += synapse0x910cd08();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910c960() {
   double input = input0x910c960();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910cd30() {
   double input = 0.132273;
   input += synapse0x910cf48();
   input += synapse0x910cf70();
   input += synapse0x910cf98();
   input += synapse0x910cfc0();
   input += synapse0x910cfe8();
   input += synapse0x910d010();
   input += synapse0x910d038();
   input += synapse0x910d060();
   input += synapse0x910d088();
   input += synapse0x910d0b0();
   input += synapse0x910d0d8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910cd30() {
   double input = input0x910cd30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910d100() {
   double input = 0.063555;
   input += synapse0x910d318();
   input += synapse0x910d340();
   input += synapse0x910d368();
   input += synapse0x910d390();
   input += synapse0x910d3b8();
   input += synapse0x910d3e0();
   input += synapse0x910d408();
   input += synapse0x910d430();
   input += synapse0x910d458();
   input += synapse0x910d480();
   input += synapse0x910d4a8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910d100() {
   double input = input0x910d100();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910d4d0() {
   double input = 0.236844;
   input += synapse0x910d6e8();
   input += synapse0x9109328();
   input += synapse0x9109350();
   input += synapse0x9109378();
   input += synapse0x91093d8();
   input += synapse0x9109400();
   input += synapse0x9109428();
   input += synapse0x9109488();
   input += synapse0x91094b0();
   input += synapse0x91094d8();
   input += synapse0x9109538();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910d4d0() {
   double input = input0x910d4d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910e268() {
   double input = 0.605778;
   input += synapse0x91096e0();
   input += synapse0x9109630();
   input += synapse0x9109748();
   input += synapse0x9109770();
   input += synapse0x9109798();
   input += synapse0x91097f8();
   input += synapse0x9109820();
   input += synapse0x9109848();
   input += synapse0x91098a8();
   input += synapse0x91098d0();
   input += synapse0x91098f8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910e268() {
   double input = input0x910e268();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910e390() {
   double input = -0.185292;
   input += synapse0x91099a0();
   input += synapse0x9109a50();
   input += synapse0x910e500();
   input += synapse0x910e528();
   input += synapse0x910e550();
   input += synapse0x910e578();
   input += synapse0x910e5a0();
   input += synapse0x910e5c8();
   input += synapse0x910e5f0();
   input += synapse0x910e618();
   input += synapse0x910e640();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910e390() {
   double input = input0x910e390();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910e668() {
   double input = 0.337272;
   input += synapse0x910e868();
   input += synapse0x910e890();
   input += synapse0x910e8b8();
   input += synapse0x910e8e0();
   input += synapse0x910e908();
   input += synapse0x910e930();
   input += synapse0x910e958();
   input += synapse0x910e980();
   input += synapse0x910e9a8();
   input += synapse0x910e9d0();
   input += synapse0x910e9f8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910e668() {
   double input = input0x910e668();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910ea20() {
   double input = -0.245423;
   input += synapse0x910ec20();
   input += synapse0x910ec48();
   input += synapse0x910ec70();
   input += synapse0x910ec98();
   input += synapse0x910ecc0();
   input += synapse0x910ece8();
   input += synapse0x910ed10();
   input += synapse0x910ed38();
   input += synapse0x910ed60();
   input += synapse0x910ed88();
   input += synapse0x910edb0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910ea20() {
   double input = input0x910ea20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910edd8() {
   double input = -0.652514;
   input += synapse0x910efd8();
   input += synapse0x910f000();
   input += synapse0x910f028();
   input += synapse0x910f050();
   input += synapse0x910f078();
   input += synapse0x910f0a0();
   input += synapse0x910f0c8();
   input += synapse0x910f0f0();
   input += synapse0x910f118();
   input += synapse0x910f140();
   input += synapse0x910f168();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910edd8() {
   double input = input0x910edd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910f190() {
   double input = 0.608756;
   input += synapse0x910f3a8();
   input += synapse0x910f3d0();
   input += synapse0x910f3f8();
   input += synapse0x910f420();
   input += synapse0x910f448();
   input += synapse0x910f470();
   input += synapse0x910f498();
   input += synapse0x910f4c0();
   input += synapse0x910f4e8();
   input += synapse0x910f510();
   input += synapse0x910f538();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910f190() {
   double input = input0x910f190();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910f560() {
   double input = 0.687365;
   input += synapse0x910f778();
   input += synapse0x910f7a0();
   input += synapse0x910f7c8();
   input += synapse0x910f7f0();
   input += synapse0x910f818();
   input += synapse0x910f840();
   input += synapse0x910f868();
   input += synapse0x910f890();
   input += synapse0x910f8b8();
   input += synapse0x910f8e0();
   input += synapse0x910f908();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910f560() {
   double input = input0x910f560();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910f930() {
   double input = 0.0797884;
   input += synapse0x910fb48();
   input += synapse0x910fb70();
   input += synapse0x910fb98();
   input += synapse0x910fbc0();
   input += synapse0x910fbe8();
   input += synapse0x910fc10();
   input += synapse0x910fc38();
   input += synapse0x910fc60();
   input += synapse0x910fc88();
   input += synapse0x910fcb0();
   input += synapse0x910fcd8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910f930() {
   double input = input0x910f930();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910fd00() {
   double input = 0.668489;
   input += synapse0x910ff18();
   input += synapse0x910ff40();
   input += synapse0x910ff68();
   input += synapse0x910ff90();
   input += synapse0x910ffb8();
   input += synapse0x910ffe0();
   input += synapse0x9110008();
   input += synapse0x9110030();
   input += synapse0x9110058();
   input += synapse0x9110080();
   input += synapse0x91100a8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910fd00() {
   double input = input0x910fd00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91100d0() {
   double input = 0.214281;
   input += synapse0x91102e8();
   input += synapse0x9110310();
   input += synapse0x9110338();
   input += synapse0x9110360();
   input += synapse0x9110388();
   input += synapse0x91103b0();
   input += synapse0x91103d8();
   input += synapse0x9110400();
   input += synapse0x9110428();
   input += synapse0x9110450();
   input += synapse0x9110478();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91100d0() {
   double input = input0x91100d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91104a0() {
   double input = 0.0877047;
   input += synapse0x91106b8();
   input += synapse0x91106e0();
   input += synapse0x9110708();
   input += synapse0x9110730();
   input += synapse0x9110758();
   input += synapse0x9110780();
   input += synapse0x91107a8();
   input += synapse0x91107d0();
   input += synapse0x91107f8();
   input += synapse0x9110820();
   input += synapse0x9110848();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91104a0() {
   double input = input0x91104a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9110870() {
   double input = 1.00782;
   input += synapse0x9110a88();
   input += synapse0x9110ab0();
   input += synapse0x9110ad8();
   input += synapse0x9110b00();
   input += synapse0x9110b28();
   input += synapse0x9110b50();
   input += synapse0x9110b78();
   input += synapse0x9110ba0();
   input += synapse0x9110bc8();
   input += synapse0x9110bf0();
   input += synapse0x9110c18();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9110870() {
   double input = input0x9110870();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9110c40() {
   double input = -0.264244;
   input += synapse0x9110e58();
   input += synapse0x9110e80();
   input += synapse0x9110ea8();
   input += synapse0x9110ed0();
   input += synapse0x9110ef8();
   input += synapse0x9110f20();
   input += synapse0x9110f48();
   input += synapse0x9110f70();
   input += synapse0x9110f98();
   input += synapse0x9110fc0();
   input += synapse0x9110fe8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9110c40() {
   double input = input0x9110c40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9111010() {
   double input = 0.656782;
   input += synapse0x9111228();
   input += synapse0x9111250();
   input += synapse0x9111278();
   input += synapse0x91112a0();
   input += synapse0x91112c8();
   input += synapse0x91112f0();
   input += synapse0x9111318();
   input += synapse0x9111340();
   input += synapse0x9111368();
   input += synapse0x9111390();
   input += synapse0x91113b8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9111010() {
   double input = input0x9111010();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91113e0() {
   double input = 0.161642;
   input += synapse0x91115f8();
   input += synapse0x9111620();
   input += synapse0x9111648();
   input += synapse0x9111670();
   input += synapse0x9111698();
   input += synapse0x91116c0();
   input += synapse0x91116e8();
   input += synapse0x910b4f8();
   input += synapse0x910b520();
   input += synapse0x910b548();
   input += synapse0x910b570();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91113e0() {
   double input = input0x91113e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910b598() {
   double input = 0.429862;
   input += synapse0x910b7b0();
   input += synapse0x910b7d8();
   input += synapse0x910b800();
   input += synapse0x910b828();
   input += synapse0x910b850();
   input += synapse0x910b878();
   input += synapse0x910b8a0();
   input += synapse0x910b8c8();
   input += synapse0x910b8f0();
   input += synapse0x910b918();
   input += synapse0x910b940();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910b598() {
   double input = input0x910b598();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910b968() {
   double input = -0.390771;
   input += synapse0x910bb80();
   input += synapse0x910bba8();
   input += synapse0x910bbd0();
   input += synapse0x910bbf8();
   input += synapse0x910bc20();
   input += synapse0x910bc48();
   input += synapse0x910bc70();
   input += synapse0x910bc98();
   input += synapse0x910bcc0();
   input += synapse0x9112718();
   input += synapse0x9112740();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910b968() {
   double input = input0x910b968();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9112768() {
   double input = 0.0255623;
   input += synapse0x9112968();
   input += synapse0x9112990();
   input += synapse0x91129b8();
   input += synapse0x91129e0();
   input += synapse0x9112a08();
   input += synapse0x9112a30();
   input += synapse0x9112a58();
   input += synapse0x9112a80();
   input += synapse0x9112aa8();
   input += synapse0x9112ad0();
   input += synapse0x9112af8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9112768() {
   double input = input0x9112768();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9112b20() {
   double input = 0.371016;
   input += synapse0x9112d38();
   input += synapse0x9112d60();
   input += synapse0x9112d88();
   input += synapse0x9112db0();
   input += synapse0x9112dd8();
   input += synapse0x9112e00();
   input += synapse0x9112e28();
   input += synapse0x9112e50();
   input += synapse0x9112e78();
   input += synapse0x9112ea0();
   input += synapse0x9112ec8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9112b20() {
   double input = input0x9112b20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9112ef0() {
   double input = -0.579778;
   input += synapse0x9113108();
   input += synapse0x9113130();
   input += synapse0x9113158();
   input += synapse0x9113180();
   input += synapse0x91131a8();
   input += synapse0x91131d0();
   input += synapse0x91131f8();
   input += synapse0x9113220();
   input += synapse0x9113248();
   input += synapse0x9113270();
   input += synapse0x9113298();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9112ef0() {
   double input = input0x9112ef0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91132c0() {
   double input = -0.392311;
   input += synapse0x91134d8();
   input += synapse0x9113500();
   input += synapse0x9113528();
   input += synapse0x9113550();
   input += synapse0x9113578();
   input += synapse0x91135a0();
   input += synapse0x91135c8();
   input += synapse0x91135f0();
   input += synapse0x9113618();
   input += synapse0x9113640();
   input += synapse0x9113668();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91132c0() {
   double input = input0x91132c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9113690() {
   double input = 0.467092;
   input += synapse0x910ac28();
   input += synapse0x910ac50();
   input += synapse0x910ac78();
   input += synapse0x910aca0();
   input += synapse0x910acc8();
   input += synapse0x910acf0();
   input += synapse0x9113ab0();
   input += synapse0x9113ad8();
   input += synapse0x9113b00();
   input += synapse0x9113b28();
   input += synapse0x9113b50();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9113690() {
   double input = input0x9113690();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9113b78() {
   double input = 0.311573;
   input += synapse0x9113d78();
   input += synapse0x9113da0();
   input += synapse0x9113dc8();
   input += synapse0x9113df0();
   input += synapse0x9113e18();
   input += synapse0x9113e40();
   input += synapse0x9113e68();
   input += synapse0x9113e90();
   input += synapse0x9113eb8();
   input += synapse0x9113ee0();
   input += synapse0x9113f08();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9113b78() {
   double input = input0x9113b78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9113f30() {
   double input = 0.446404;
   input += synapse0x9114148();
   input += synapse0x9114170();
   input += synapse0x9114198();
   input += synapse0x91141c0();
   input += synapse0x91141e8();
   input += synapse0x9114210();
   input += synapse0x9114238();
   input += synapse0x9114260();
   input += synapse0x9114288();
   input += synapse0x91142b0();
   input += synapse0x91142d8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9113f30() {
   double input = input0x9113f30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9114300() {
   double input = -1.46551;
   input += synapse0x9114518();
   input += synapse0x9114540();
   input += synapse0x9114568();
   input += synapse0x9114590();
   input += synapse0x91145b8();
   input += synapse0x91145e0();
   input += synapse0x9114608();
   input += synapse0x9114630();
   input += synapse0x9114658();
   input += synapse0x9114680();
   input += synapse0x91146a8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9114300() {
   double input = input0x9114300();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91146d0() {
   double input = 0.256018;
   input += synapse0x91148e8();
   input += synapse0x9114910();
   input += synapse0x9114938();
   input += synapse0x9114960();
   input += synapse0x9114988();
   input += synapse0x91149b0();
   input += synapse0x91149d8();
   input += synapse0x9114a00();
   input += synapse0x9114a28();
   input += synapse0x9114a50();
   input += synapse0x9114a78();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91146d0() {
   double input = input0x91146d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9114aa0() {
   double input = -0.0820621;
   input += synapse0x9114cb8();
   input += synapse0x9114ce0();
   input += synapse0x9114d08();
   input += synapse0x9114d30();
   input += synapse0x9114d58();
   input += synapse0x9114d80();
   input += synapse0x9114da8();
   input += synapse0x9114dd0();
   input += synapse0x9114df8();
   input += synapse0x9114e20();
   input += synapse0x9114e48();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9114aa0() {
   double input = input0x9114aa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9114e70() {
   double input = -0.26703;
   input += synapse0x9115088();
   input += synapse0x91150b0();
   input += synapse0x91150d8();
   input += synapse0x9115100();
   input += synapse0x9115128();
   input += synapse0x9115150();
   input += synapse0x9115178();
   input += synapse0x91151a0();
   input += synapse0x91151c8();
   input += synapse0x91151f0();
   input += synapse0x9115218();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9114e70() {
   double input = input0x9114e70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9115240() {
   double input = 0.906901;
   input += synapse0x9115458();
   input += synapse0x9115480();
   input += synapse0x91154a8();
   input += synapse0x91154d0();
   input += synapse0x91154f8();
   input += synapse0x9115520();
   input += synapse0x9115548();
   input += synapse0x9115570();
   input += synapse0x9115598();
   input += synapse0x91155c0();
   input += synapse0x91155e8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9115240() {
   double input = input0x9115240();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9115610() {
   double input = -0.463885;
   input += synapse0x9115828();
   input += synapse0x9115850();
   input += synapse0x9115878();
   input += synapse0x91158a0();
   input += synapse0x91158c8();
   input += synapse0x91158f0();
   input += synapse0x9115918();
   input += synapse0x9115940();
   input += synapse0x9115968();
   input += synapse0x9115990();
   input += synapse0x91159b8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9115610() {
   double input = input0x9115610();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91159e0() {
   double input = -0.186364;
   input += synapse0x9115bf8();
   input += synapse0x9115c20();
   input += synapse0x9115c48();
   input += synapse0x9115c70();
   input += synapse0x9115c98();
   input += synapse0x9115cc0();
   input += synapse0x9115ce8();
   input += synapse0x9115d10();
   input += synapse0x9115d38();
   input += synapse0x9115d60();
   input += synapse0x9115d88();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91159e0() {
   double input = input0x91159e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9115db0() {
   double input = 0.0998949;
   input += synapse0x9115fc8();
   input += synapse0x910d710();
   input += synapse0x910d738();
   input += synapse0x910d760();
   input += synapse0x910d990();
   input += synapse0x910d9b8();
   input += synapse0x910dbe8();
   input += synapse0x910dc10();
   input += synapse0x910de48();
   input += synapse0x910de70();
   input += synapse0x910de98();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9115db0() {
   double input = input0x9115db0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x910e0c8() {
   double input = 0.284575;
   input += synapse0x9116eb8();
   input += synapse0x9116ee0();
   input += synapse0x9116f08();
   input += synapse0x9116f30();
   input += synapse0x9116f58();
   input += synapse0x9116f80();
   input += synapse0x9116fa8();
   input += synapse0x9116fd0();
   input += synapse0x9116ff8();
   input += synapse0x9117020();
   input += synapse0x9117048();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x910e0c8() {
   double input = input0x910e0c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9117070() {
   double input = 0.0487923;
   input += synapse0x9117270();
   input += synapse0x9117298();
   input += synapse0x91172c0();
   input += synapse0x91172e8();
   input += synapse0x9117310();
   input += synapse0x9117338();
   input += synapse0x9117360();
   input += synapse0x9117388();
   input += synapse0x91173b0();
   input += synapse0x91173d8();
   input += synapse0x9117400();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9117070() {
   double input = input0x9117070();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9117428() {
   double input = 0.716123;
   input += synapse0x9117640();
   input += synapse0x9117668();
   input += synapse0x9117690();
   input += synapse0x91176b8();
   input += synapse0x91176e0();
   input += synapse0x9117708();
   input += synapse0x9117730();
   input += synapse0x9117758();
   input += synapse0x9117780();
   input += synapse0x91177a8();
   input += synapse0x91177d0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9117428() {
   double input = input0x9117428();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91177f8() {
   double input = -0.0641172;
   input += synapse0x9117a10();
   input += synapse0x9117a38();
   input += synapse0x9117a60();
   input += synapse0x9117a88();
   input += synapse0x9117ab0();
   input += synapse0x9117ad8();
   input += synapse0x9117b00();
   input += synapse0x9117b28();
   input += synapse0x9117b50();
   input += synapse0x9117b78();
   input += synapse0x9117ba0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91177f8() {
   double input = input0x91177f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9117bc8() {
   double input = -2.23065;
   input += synapse0x9117de0();
   input += synapse0x9117e08();
   input += synapse0x9117e30();
   input += synapse0x9117e58();
   input += synapse0x9117e80();
   input += synapse0x9117ea8();
   input += synapse0x9117ed0();
   input += synapse0x9117ef8();
   input += synapse0x9117f20();
   input += synapse0x9117f48();
   input += synapse0x9117f70();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9117bc8() {
   double input = input0x9117bc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9117f98() {
   double input = 0.331474;
   input += synapse0x91181b0();
   input += synapse0x91181d8();
   input += synapse0x9118200();
   input += synapse0x9118228();
   input += synapse0x9118250();
   input += synapse0x9118278();
   input += synapse0x91182a0();
   input += synapse0x91182c8();
   input += synapse0x91182f0();
   input += synapse0x9118318();
   input += synapse0x9118340();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9117f98() {
   double input = input0x9117f98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9118368() {
   double input = 0.620947;
   input += synapse0x9118580();
   input += synapse0x91185a8();
   input += synapse0x91185d0();
   input += synapse0x91185f8();
   input += synapse0x9118620();
   input += synapse0x9118648();
   input += synapse0x9118670();
   input += synapse0x9118698();
   input += synapse0x91186c0();
   input += synapse0x91186e8();
   input += synapse0x9118710();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9118368() {
   double input = input0x9118368();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9118738() {
   double input = 0.402628;
   input += synapse0x9118950();
   input += synapse0x9118978();
   input += synapse0x91189a0();
   input += synapse0x91189c8();
   input += synapse0x91189f0();
   input += synapse0x9118a18();
   input += synapse0x9118a40();
   input += synapse0x9118a68();
   input += synapse0x9118a90();
   input += synapse0x9118ab8();
   input += synapse0x9118ae0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9118738() {
   double input = input0x9118738();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9118b08() {
   double input = -0.189534;
   input += synapse0x9118d20();
   input += synapse0x9118d48();
   input += synapse0x9118d70();
   input += synapse0x9118d98();
   input += synapse0x9118dc0();
   input += synapse0x9118de8();
   input += synapse0x9118e10();
   input += synapse0x9118e38();
   input += synapse0x9118e60();
   input += synapse0x9118e88();
   input += synapse0x9118eb0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9118b08() {
   double input = input0x9118b08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9118ed8() {
   double input = -0.215442;
   input += synapse0x91190f0();
   input += synapse0x9119118();
   input += synapse0x9119140();
   input += synapse0x9119168();
   input += synapse0x9119190();
   input += synapse0x91191b8();
   input += synapse0x91191e0();
   input += synapse0x9119208();
   input += synapse0x9119230();
   input += synapse0x9119258();
   input += synapse0x9119280();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9118ed8() {
   double input = input0x9118ed8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91192a8() {
   double input = 0.502232;
   input += synapse0x91194c0();
   input += synapse0x91194e8();
   input += synapse0x9119510();
   input += synapse0x9119538();
   input += synapse0x9119560();
   input += synapse0x9119588();
   input += synapse0x91195b0();
   input += synapse0x91195d8();
   input += synapse0x9119600();
   input += synapse0x9119628();
   input += synapse0x9119650();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91192a8() {
   double input = input0x91192a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9119678() {
   double input = -0.088331;
   input += synapse0x9119890();
   input += synapse0x91198b8();
   input += synapse0x91198e0();
   input += synapse0x9119908();
   input += synapse0x9119930();
   input += synapse0x9119958();
   input += synapse0x9119980();
   input += synapse0x91199a8();
   input += synapse0x91199d0();
   input += synapse0x91199f8();
   input += synapse0x9119a20();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9119678() {
   double input = input0x9119678();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9119a48() {
   double input = 0.813804;
   input += synapse0x9119c60();
   input += synapse0x9119c88();
   input += synapse0x9119cb0();
   input += synapse0x9119cd8();
   input += synapse0x9119d00();
   input += synapse0x9119d28();
   input += synapse0x9119d50();
   input += synapse0x9119d78();
   input += synapse0x9119da0();
   input += synapse0x9119dc8();
   input += synapse0x9119df0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9119a48() {
   double input = input0x9119a48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9119e18() {
   double input = 0.454883;
   input += synapse0x911a030();
   input += synapse0x911a058();
   input += synapse0x911a080();
   input += synapse0x911a0a8();
   input += synapse0x911a0d0();
   input += synapse0x911a0f8();
   input += synapse0x911a120();
   input += synapse0x911a148();
   input += synapse0x911a170();
   input += synapse0x911a198();
   input += synapse0x911a1c0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9119e18() {
   double input = input0x9119e18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911a1e8() {
   double input = 0.491549;
   input += synapse0x911a400();
   input += synapse0x911a428();
   input += synapse0x911a450();
   input += synapse0x911a478();
   input += synapse0x911a4a0();
   input += synapse0x911a4c8();
   input += synapse0x911a4f0();
   input += synapse0x911a518();
   input += synapse0x911a540();
   input += synapse0x911a568();
   input += synapse0x911a590();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911a1e8() {
   double input = input0x911a1e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911a5b8() {
   double input = 0.46289;
   input += synapse0x911a7d0();
   input += synapse0x911a7f8();
   input += synapse0x911a820();
   input += synapse0x911a848();
   input += synapse0x911a870();
   input += synapse0x911a898();
   input += synapse0x911a8c0();
   input += synapse0x911a8e8();
   input += synapse0x911a910();
   input += synapse0x911a938();
   input += synapse0x911a960();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911a5b8() {
   double input = input0x911a5b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911a988() {
   double input = -0.0640224;
   input += synapse0x911aba0();
   input += synapse0x911abc8();
   input += synapse0x911abf0();
   input += synapse0x911ac18();
   input += synapse0x911ac40();
   input += synapse0x911ac68();
   input += synapse0x911ac90();
   input += synapse0x911acb8();
   input += synapse0x911ace0();
   input += synapse0x911ad08();
   input += synapse0x911ad30();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911a988() {
   double input = input0x911a988();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911ad58() {
   double input = -0.260845;
   input += synapse0x911af70();
   input += synapse0x911af98();
   input += synapse0x911afc0();
   input += synapse0x911afe8();
   input += synapse0x911b010();
   input += synapse0x911b038();
   input += synapse0x911b060();
   input += synapse0x911b088();
   input += synapse0x911b0b0();
   input += synapse0x911b0d8();
   input += synapse0x911b100();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911ad58() {
   double input = input0x911ad58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911b128() {
   double input = 0.323132;
   input += synapse0x911b340();
   input += synapse0x911b368();
   input += synapse0x911b390();
   input += synapse0x911b3b8();
   input += synapse0x911b3e0();
   input += synapse0x911b408();
   input += synapse0x911b430();
   input += synapse0x911b458();
   input += synapse0x911b480();
   input += synapse0x911b4a8();
   input += synapse0x911b4d0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911b128() {
   double input = input0x911b128();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911b4f8() {
   double input = -0.0127516;
   input += synapse0x911b710();
   input += synapse0x911b738();
   input += synapse0x911b760();
   input += synapse0x911b788();
   input += synapse0x911b7b0();
   input += synapse0x911b7d8();
   input += synapse0x911b800();
   input += synapse0x911b828();
   input += synapse0x911b850();
   input += synapse0x911b878();
   input += synapse0x911b8a0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911b4f8() {
   double input = input0x911b4f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911b8c8() {
   double input = 0.870569;
   input += synapse0x911bae0();
   input += synapse0x911bb08();
   input += synapse0x911bb30();
   input += synapse0x911bb58();
   input += synapse0x911bb80();
   input += synapse0x911bba8();
   input += synapse0x911bbd0();
   input += synapse0x911bbf8();
   input += synapse0x911bc20();
   input += synapse0x911bc48();
   input += synapse0x911bc70();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911b8c8() {
   double input = input0x911b8c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911bc98() {
   double input = -0.024196;
   input += synapse0x911beb0();
   input += synapse0x911bed8();
   input += synapse0x911bf00();
   input += synapse0x911bf28();
   input += synapse0x911bf50();
   input += synapse0x911bf78();
   input += synapse0x911bfa0();
   input += synapse0x911bfc8();
   input += synapse0x911bff0();
   input += synapse0x911c018();
   input += synapse0x911c040();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911bc98() {
   double input = input0x911bc98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911c068() {
   double input = -0.161155;
   input += synapse0x911c280();
   input += synapse0x911c2a8();
   input += synapse0x911c2d0();
   input += synapse0x911c2f8();
   input += synapse0x911c320();
   input += synapse0x911c348();
   input += synapse0x911c370();
   input += synapse0x911c398();
   input += synapse0x911c3c0();
   input += synapse0x911c3e8();
   input += synapse0x911c410();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911c068() {
   double input = input0x911c068();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911c438() {
   double input = -0.889894;
   input += synapse0x911c650();
   input += synapse0x911c678();
   input += synapse0x911c6a0();
   input += synapse0x911c6c8();
   input += synapse0x911c6f0();
   input += synapse0x911c718();
   input += synapse0x911c740();
   input += synapse0x911c768();
   input += synapse0x911c790();
   input += synapse0x911c7b8();
   input += synapse0x911c7e0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911c438() {
   double input = input0x911c438();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911c808() {
   double input = -2.14494;
   input += synapse0x911ca20();
   input += synapse0x911ca48();
   input += synapse0x911ca70();
   input += synapse0x911ca98();
   input += synapse0x911cac0();
   input += synapse0x911cae8();
   input += synapse0x911cb10();
   input += synapse0x911cb38();
   input += synapse0x911cb60();
   input += synapse0x911cb88();
   input += synapse0x911cbb0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911c808() {
   double input = input0x911c808();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911cbd8() {
   double input = 0.210479;
   input += synapse0x911cdf0();
   input += synapse0x911ce18();
   input += synapse0x911ce40();
   input += synapse0x911ce68();
   input += synapse0x911ce90();
   input += synapse0x911ceb8();
   input += synapse0x911cee0();
   input += synapse0x911cf08();
   input += synapse0x911cf30();
   input += synapse0x911cf58();
   input += synapse0x911cf80();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911cbd8() {
   double input = input0x911cbd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911cfa8() {
   double input = 0.0264869;
   input += synapse0x911d1c0();
   input += synapse0x911d1e8();
   input += synapse0x911d210();
   input += synapse0x911d238();
   input += synapse0x911d260();
   input += synapse0x911d288();
   input += synapse0x911d2b0();
   input += synapse0x911d2d8();
   input += synapse0x911d300();
   input += synapse0x911d328();
   input += synapse0x911d350();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911cfa8() {
   double input = input0x911cfa8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911d378() {
   double input = 0.6054;
   input += synapse0x911d590();
   input += synapse0x911d5b8();
   input += synapse0x911d5e0();
   input += synapse0x911d608();
   input += synapse0x911d630();
   input += synapse0x911d658();
   input += synapse0x911d680();
   input += synapse0x911d6a8();
   input += synapse0x911d6d0();
   input += synapse0x911d6f8();
   input += synapse0x911d720();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911d378() {
   double input = input0x911d378();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911d748() {
   double input = 0.483637;
   input += synapse0x911d960();
   input += synapse0x911d988();
   input += synapse0x911d9b0();
   input += synapse0x911d9d8();
   input += synapse0x911da00();
   input += synapse0x911da28();
   input += synapse0x911da50();
   input += synapse0x911da78();
   input += synapse0x911daa0();
   input += synapse0x911dac8();
   input += synapse0x911daf0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911d748() {
   double input = input0x911d748();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911db18() {
   double input = 0.14257;
   input += synapse0x911dd30();
   input += synapse0x911dd58();
   input += synapse0x9111710();
   input += synapse0x9111738();
   input += synapse0x9111760();
   input += synapse0x9111788();
   input += synapse0x91117b0();
   input += synapse0x91117d8();
   input += synapse0x9111800();
   input += synapse0x9111828();
   input += synapse0x9111850();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911db18() {
   double input = input0x911db18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9111878() {
   double input = -0.37281;
   input += synapse0x9111a90();
   input += synapse0x9111ab8();
   input += synapse0x9111ae0();
   input += synapse0x9111b08();
   input += synapse0x9111b30();
   input += synapse0x9111b58();
   input += synapse0x9111b80();
   input += synapse0x9111ba8();
   input += synapse0x9111bd0();
   input += synapse0x9111bf8();
   input += synapse0x9111c20();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9111878() {
   double input = input0x9111878();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9111c48() {
   double input = -1.16776;
   input += synapse0x9111e60();
   input += synapse0x9111e88();
   input += synapse0x9111eb0();
   input += synapse0x9111ed8();
   input += synapse0x9111f00();
   input += synapse0x9111f28();
   input += synapse0x9111f50();
   input += synapse0x9111f78();
   input += synapse0x9111fa0();
   input += synapse0x9111fc8();
   input += synapse0x9111ff0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9111c48() {
   double input = input0x9111c48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x9112018() {
   double input = -0.478302;
   input += synapse0x9112230();
   input += synapse0x9112258();
   input += synapse0x9112280();
   input += synapse0x91122a8();
   input += synapse0x91122d0();
   input += synapse0x91122f8();
   input += synapse0x9112320();
   input += synapse0x9112348();
   input += synapse0x9112370();
   input += synapse0x9112398();
   input += synapse0x91123c0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x9112018() {
   double input = input0x9112018();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91123e8() {
   double input = 0.38438;
   input += synapse0x9112600();
   input += synapse0x9112628();
   input += synapse0x9112650();
   input += synapse0x9112678();
   input += synapse0x91126a0();
   input += synapse0x91126c8();
   input += synapse0x91126f0();
   input += synapse0x911fd88();
   input += synapse0x911fdb0();
   input += synapse0x911fdd8();
   input += synapse0x911fe00();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91123e8() {
   double input = input0x91123e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x911fe28() {
   double input = -0.18788;
   input += synapse0x9120040();
   input += synapse0x9120068();
   input += synapse0x9120090();
   input += synapse0x91200b8();
   input += synapse0x91200e0();
   input += synapse0x9120108();
   input += synapse0x9120130();
   input += synapse0x9120158();
   input += synapse0x9120180();
   input += synapse0x91201a8();
   input += synapse0x91201d0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x911fe28() {
   double input = input0x911fe28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91201f8() {
   double input = -0.0550987;
   input += synapse0x9120410();
   input += synapse0x9120438();
   input += synapse0x9120460();
   input += synapse0x9120488();
   input += synapse0x91204b0();
   input += synapse0x91204d8();
   input += synapse0x9120500();
   input += synapse0x9120528();
   input += synapse0x9120550();
   input += synapse0x9120578();
   input += synapse0x91205a0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91201f8() {
   double input = input0x91201f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::input0x91205c8() {
   double input = 0.777419;
   input += synapse0x91206f0();
   input += synapse0x9120718();
   input += synapse0x9120740();
   input += synapse0x9120768();
   input += synapse0x9120790();
   input += synapse0x91207b8();
   input += synapse0x91207e0();
   input += synapse0x9120808();
   input += synapse0x9120830();
   input += synapse0x9120858();
   input += synapse0x9120880();
   input += synapse0x91208a8();
   input += synapse0x91208d0();
   input += synapse0x91208f8();
   input += synapse0x9120920();
   input += synapse0x9120948();
   input += synapse0x91209f8();
   input += synapse0x9120a20();
   input += synapse0x9120a48();
   input += synapse0x9120a70();
   input += synapse0x9120a98();
   input += synapse0x9120ac0();
   input += synapse0x9120ae8();
   input += synapse0x9120b10();
   input += synapse0x9120b38();
   input += synapse0x9120b60();
   input += synapse0x9120b88();
   input += synapse0x9120bb0();
   input += synapse0x9120bd8();
   input += synapse0x9120c00();
   input += synapse0x9120c28();
   input += synapse0x9120c50();
   input += synapse0x9120970();
   input += synapse0x9120998();
   input += synapse0x91209c0();
   input += synapse0x9120d80();
   input += synapse0x9120da8();
   input += synapse0x9120dd0();
   input += synapse0x9120df8();
   input += synapse0x9120e20();
   input += synapse0x9120e48();
   input += synapse0x9120e70();
   input += synapse0x9120e98();
   input += synapse0x9120ec0();
   input += synapse0x9120ee8();
   input += synapse0x9120f10();
   input += synapse0x9120f38();
   input += synapse0x9120f60();
   input += synapse0x9120f88();
   input += synapse0x9120fb0();
   input += synapse0x9120fd8();
   input += synapse0x9121000();
   input += synapse0x9121028();
   input += synapse0x9121050();
   input += synapse0x9121078();
   input += synapse0x91210a0();
   input += synapse0x91210c8();
   input += synapse0x91210f0();
   input += synapse0x9121118();
   input += synapse0x9121140();
   input += synapse0x9121168();
   input += synapse0x9121190();
   input += synapse0x91211b8();
   input += synapse0x91211e0();
   input += synapse0x9105550();
   input += synapse0x9120c78();
   input += synapse0x9120ca0();
   input += synapse0x9120cc8();
   input += synapse0x9120cf0();
   input += synapse0x9120d18();
   input += synapse0x9120d40();
   input += synapse0x9121410();
   input += synapse0x9121438();
   input += synapse0x9121460();
   input += synapse0x9121488();
   input += synapse0x91214b0();
   input += synapse0x91214d8();
   input += synapse0x9121500();
   input += synapse0x9121528();
   input += synapse0x9121550();
   input += synapse0x9121578();
   input += synapse0x91215a0();
   input += synapse0x91215c8();
   input += synapse0x91215f0();
   input += synapse0x9121618();
   input += synapse0x9121640();
   input += synapse0x9121668();
   input += synapse0x9121690();
   input += synapse0x91216b8();
   input += synapse0x91216e0();
   input += synapse0x9121708();
   input += synapse0x9121730();
   input += synapse0x9121758();
   input += synapse0x9121780();
   input += synapse0x91217a8();
   input += synapse0x91217d0();
   input += synapse0x91217f8();
   input += synapse0x9121820();
   input += synapse0x9121848();
   input += synapse0x9121870();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaY::neuron0x91205c8() {
   double input = input0x91205c8();
   return (input * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x8f34500() {
   return (neuron0x90f5bc0()*0.270818);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91057f0() {
   return (neuron0x90f5dc0()*0.475287);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105818() {
   return (neuron0x91043d0()*-0.0531284);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105840() {
   return (neuron0x91044f8()*-0.509941);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105868() {
   return (neuron0x91046b0()*-0.0608595);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105890() {
   return (neuron0x91048b0()*0.0407829);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91058b8() {
   return (neuron0x9104ab0()*0.121349);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91058e0() {
   return (neuron0x9104cc8()*0.192787);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105908() {
   return (neuron0x9104ee0()*-0.869175);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105930() {
   return (neuron0x91050f8()*-0.124698);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105958() {
   return (neuron0x91052f8()*-0.415122);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105b80() {
   return (neuron0x90f5bc0()*0.115599);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105ba8() {
   return (neuron0x90f5dc0()*-0.225811);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105bd0() {
   return (neuron0x91043d0()*-0.363299);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105bf8() {
   return (neuron0x91044f8()*0.202221);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105c20() {
   return (neuron0x91046b0()*-1.08708);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105c48() {
   return (neuron0x91048b0()*-0.0352841);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105cf8() {
   return (neuron0x9104ab0()*2.25755);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105d20() {
   return (neuron0x9104cc8()*0.0106838);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105d48() {
   return (neuron0x9104ee0()*1.32555);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105d70() {
   return (neuron0x91050f8()*-0.200138);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105d98() {
   return (neuron0x91052f8()*-0.0193283);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105f78() {
   return (neuron0x90f5bc0()*-0.348154);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105fa0() {
   return (neuron0x90f5dc0()*-0.364391);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105fc8() {
   return (neuron0x91043d0()*-0.136977);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105ff0() {
   return (neuron0x91044f8()*0.197536);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106018() {
   return (neuron0x91046b0()*0.0269267);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106040() {
   return (neuron0x91048b0()*0.136485);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106068() {
   return (neuron0x9104ab0()*-0.185127);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106090() {
   return (neuron0x9104cc8()*0.391653);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91060b8() {
   return (neuron0x9104ee0()*0.668857);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91060e0() {
   return (neuron0x91050f8()*0.204985);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106108() {
   return (neuron0x91052f8()*-0.147616);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105cb8() {
   return (neuron0x90f5bc0()*0.225934);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91063f0() {
   return (neuron0x90f5dc0()*-0.243518);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106418() {
   return (neuron0x91043d0()*0.0982004);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106440() {
   return (neuron0x91044f8()*-0.502331);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106468() {
   return (neuron0x91046b0()*0.17075);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106490() {
   return (neuron0x91048b0()*-0.254157);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91064b8() {
   return (neuron0x9104ab0()*-0.305694);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91064e0() {
   return (neuron0x9104cc8()*-0.116132);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106508() {
   return (neuron0x9104ee0()*-0.131432);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106530() {
   return (neuron0x91050f8()*0.120349);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106558() {
   return (neuron0x91052f8()*0.100093);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106780() {
   return (neuron0x90f5bc0()*0.138512);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91067a8() {
   return (neuron0x90f5dc0()*0.0821716);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91067d0() {
   return (neuron0x91043d0()*0.8654);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91067f8() {
   return (neuron0x91044f8()*0.0255573);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106820() {
   return (neuron0x91046b0()*0.377839);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106848() {
   return (neuron0x91048b0()*0.282057);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106870() {
   return (neuron0x9104ab0()*-0.199339);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106898() {
   return (neuron0x9104cc8()*0.151535);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91068c0() {
   return (neuron0x9104ee0()*-1.82375);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91068e8() {
   return (neuron0x91050f8()*0.0935532);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106910() {
   return (neuron0x91052f8()*0.214757);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106bc0() {
   return (neuron0x90f5bc0()*-0.0416165);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106be8() {
   return (neuron0x90f5dc0()*0.495606);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106c10() {
   return (neuron0x91043d0()*-0.116782);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106c38() {
   return (neuron0x91044f8()*0.0199054);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106c60() {
   return (neuron0x91046b0()*-0.536102);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106c88() {
   return (neuron0x91048b0()*0.232683);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106cb0() {
   return (neuron0x9104ab0()*-0.144096);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106cd8() {
   return (neuron0x9104cc8()*-0.0237501);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106d00() {
   return (neuron0x9104ee0()*0.194155);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106d28() {
   return (neuron0x91050f8()*0.13583);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x8f34340() {
   return (neuron0x91052f8()*0.132371);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x8f344c8() {
   return (neuron0x90f5bc0()*0.0801314);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106130() {
   return (neuron0x90f5dc0()*0.309234);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106158() {
   return (neuron0x91043d0()*0.588396);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106180() {
   return (neuron0x91044f8()*0.0784323);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91061a8() {
   return (neuron0x91046b0()*-0.34855);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91061d0() {
   return (neuron0x91048b0()*0.247032);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91061f8() {
   return (neuron0x9104ab0()*0.058169);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107080() {
   return (neuron0x9104cc8()*0.421386);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91070a8() {
   return (neuron0x9104ee0()*0.360043);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91070d0() {
   return (neuron0x91050f8()*-0.552333);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91070f8() {
   return (neuron0x91052f8()*-0.0550669);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107320() {
   return (neuron0x90f5bc0()*0.0570762);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107348() {
   return (neuron0x90f5dc0()*0.0531032);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107370() {
   return (neuron0x91043d0()*-0.11585);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107398() {
   return (neuron0x91044f8()*0.160924);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91073c0() {
   return (neuron0x91046b0()*-0.241607);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91073e8() {
   return (neuron0x91048b0()*-0.394523);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107410() {
   return (neuron0x9104ab0()*0.486184);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107438() {
   return (neuron0x9104cc8()*-0.00516569);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107460() {
   return (neuron0x9104ee0()*0.251792);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107488() {
   return (neuron0x91050f8()*-0.0712603);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91074b0() {
   return (neuron0x91052f8()*-0.0517876);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91076d8() {
   return (neuron0x90f5bc0()*0.335417);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107700() {
   return (neuron0x90f5dc0()*0.472031);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107728() {
   return (neuron0x91043d0()*-0.115713);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107750() {
   return (neuron0x91044f8()*0.164355);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107778() {
   return (neuron0x91046b0()*-0.158173);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91077a0() {
   return (neuron0x91048b0()*0.684523);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91077c8() {
   return (neuron0x9104ab0()*0.057161);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91077f0() {
   return (neuron0x9104cc8()*0.506613);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107818() {
   return (neuron0x9104ee0()*-0.452864);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107840() {
   return (neuron0x91050f8()*-0.457625);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107868() {
   return (neuron0x91052f8()*1.14622);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107a90() {
   return (neuron0x90f5bc0()*-0.207452);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107ab8() {
   return (neuron0x90f5dc0()*-0.143142);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107ae0() {
   return (neuron0x91043d0()*0.452167);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107b08() {
   return (neuron0x91044f8()*0.148425);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107b30() {
   return (neuron0x91046b0()*-1.09837);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107b58() {
   return (neuron0x91048b0()*0.106527);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107b80() {
   return (neuron0x9104ab0()*-0.127637);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107ba8() {
   return (neuron0x9104cc8()*-0.0717695);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107bd0() {
   return (neuron0x9104ee0()*1.01319);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107bf8() {
   return (neuron0x91050f8()*-0.0554994);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107c20() {
   return (neuron0x91052f8()*-0.123342);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107e48() {
   return (neuron0x90f5bc0()*-0.524539);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107e70() {
   return (neuron0x90f5dc0()*0.105783);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107e98() {
   return (neuron0x91043d0()*-0.356018);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107ec0() {
   return (neuron0x91044f8()*0.647287);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107ee8() {
   return (neuron0x91046b0()*-0.303691);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107f10() {
   return (neuron0x91048b0()*0.0424492);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107f38() {
   return (neuron0x9104ab0()*-0.723037);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107f60() {
   return (neuron0x9104cc8()*0.607311);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107f88() {
   return (neuron0x9104ee0()*-0.487607);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107fb0() {
   return (neuron0x91050f8()*-0.269427);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9107fd8() {
   return (neuron0x91052f8()*-0.491213);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108200() {
   return (neuron0x90f5bc0()*0.198704);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108228() {
   return (neuron0x90f5dc0()*-0.413035);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108250() {
   return (neuron0x91043d0()*-1.0724);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108278() {
   return (neuron0x91044f8()*0.348871);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91082a0() {
   return (neuron0x91046b0()*-1.68414);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91082c8() {
   return (neuron0x91048b0()*0.315457);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91082f0() {
   return (neuron0x9104ab0()*-0.354547);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108318() {
   return (neuron0x9104cc8()*-0.249717);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106d50() {
   return (neuron0x9104ee0()*-0.298972);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106d78() {
   return (neuron0x91050f8()*0.28484);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106da0() {
   return (neuron0x91052f8()*0.789924);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91087d8() {
   return (neuron0x90f5bc0()*-0.253372);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108800() {
   return (neuron0x90f5dc0()*0.161867);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108828() {
   return (neuron0x91043d0()*-0.736303);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108850() {
   return (neuron0x91044f8()*0.283299);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108878() {
   return (neuron0x91046b0()*-0.475556);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91088a0() {
   return (neuron0x91048b0()*-0.228263);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91088c8() {
   return (neuron0x9104ab0()*1.31147);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91088f0() {
   return (neuron0x9104cc8()*-0.0999567);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108918() {
   return (neuron0x9104ee0()*-0.367343);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108940() {
   return (neuron0x91050f8()*0.631968);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108968() {
   return (neuron0x91052f8()*0.141169);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108b90() {
   return (neuron0x90f5bc0()*-0.212798);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108bb8() {
   return (neuron0x90f5dc0()*0.267088);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108be0() {
   return (neuron0x91043d0()*0.510689);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108c08() {
   return (neuron0x91044f8()*-0.517888);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108c30() {
   return (neuron0x91046b0()*-0.0926808);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108c58() {
   return (neuron0x91048b0()*-0.23537);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108c80() {
   return (neuron0x9104ab0()*-0.248825);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108ca8() {
   return (neuron0x9104cc8()*0.173573);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108cd0() {
   return (neuron0x9104ee0()*0.371416);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108cf8() {
   return (neuron0x91050f8()*0.154459);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108d20() {
   return (neuron0x91052f8()*-0.113037);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108f48() {
   return (neuron0x90f5bc0()*-0.158393);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108f70() {
   return (neuron0x90f5dc0()*0.0767508);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108f98() {
   return (neuron0x91043d0()*1.12982);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108fc0() {
   return (neuron0x91044f8()*-0.0383773);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108fe8() {
   return (neuron0x91046b0()*-0.320181);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109010() {
   return (neuron0x91048b0()*0.0149102);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109038() {
   return (neuron0x9104ab0()*-0.603365);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109060() {
   return (neuron0x9104cc8()*0.267754);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109088() {
   return (neuron0x9104ee0()*-1.18349);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91090b0() {
   return (neuron0x91050f8()*0.221652);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91090d8() {
   return (neuron0x91052f8()*-0.219117);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109300() {
   return (neuron0x90f5bc0()*0.364038);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91093b0() {
   return (neuron0x90f5dc0()*-0.289837);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109460() {
   return (neuron0x91043d0()*-0.381819);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109510() {
   return (neuron0x91044f8()*-0.379387);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91095c0() {
   return (neuron0x91046b0()*-0.0136699);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109670() {
   return (neuron0x91048b0()*0.0167797);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109720() {
   return (neuron0x9104ab0()*-0.144926);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91097d0() {
   return (neuron0x9104cc8()*-0.279466);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109880() {
   return (neuron0x9104ee0()*-0.452609);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109930() {
   return (neuron0x91050f8()*-0.214827);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91099e0() {
   return (neuron0x91052f8()*0.0297187);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109bb8() {
   return (neuron0x90f5bc0()*0.0726523);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109be0() {
   return (neuron0x90f5dc0()*0.0596992);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109c08() {
   return (neuron0x91043d0()*2.5198);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109c30() {
   return (neuron0x91044f8()*-0.316912);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109c58() {
   return (neuron0x91046b0()*0.75327);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109c80() {
   return (neuron0x91048b0()*-0.420115);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109ca8() {
   return (neuron0x9104ab0()*-2.22516);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109cd0() {
   return (neuron0x9104cc8()*-0.214758);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109cf8() {
   return (neuron0x9104ee0()*-0.286513);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109d20() {
   return (neuron0x91050f8()*0.242387);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109d48() {
   return (neuron0x91052f8()*-0.125721);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109e98() {
   return (neuron0x90f5bc0()*0.0605496);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109ec0() {
   return (neuron0x90f5dc0()*0.304707);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109ee8() {
   return (neuron0x91043d0()*0.485754);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109f10() {
   return (neuron0x91044f8()*-0.3198);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109f38() {
   return (neuron0x91046b0()*-0.217791);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109f60() {
   return (neuron0x91048b0()*-0.27282);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109f88() {
   return (neuron0x9104ab0()*0.163871);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109fb0() {
   return (neuron0x9104cc8()*-0.651641);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109fd8() {
   return (neuron0x9104ee0()*-1.10769);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a000() {
   return (neuron0x91050f8()*0.500221);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a028() {
   return (neuron0x91052f8()*0.28549);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a178() {
   return (neuron0x90f5bc0()*-0.204041);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a1a0() {
   return (neuron0x90f5dc0()*0.396421);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a1c8() {
   return (neuron0x91043d0()*0.00886778);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a1f0() {
   return (neuron0x91044f8()*0.227094);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a218() {
   return (neuron0x91046b0()*-0.0347881);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a240() {
   return (neuron0x91048b0()*0.389724);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a268() {
   return (neuron0x9104ab0()*0.112864);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a290() {
   return (neuron0x9104cc8()*0.110797);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a2b8() {
   return (neuron0x9104ee0()*0.133883);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a2e0() {
   return (neuron0x91050f8()*0.118354);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a308() {
   return (neuron0x91052f8()*-0.351555);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a4a0() {
   return (neuron0x90f5bc0()*-0.111933);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a4c8() {
   return (neuron0x90f5dc0()*0.143112);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a4f0() {
   return (neuron0x91043d0()*0.423798);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a518() {
   return (neuron0x91044f8()*-0.405171);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a540() {
   return (neuron0x91046b0()*-0.25549);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a568() {
   return (neuron0x91048b0()*-0.405545);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a590() {
   return (neuron0x9104ab0()*-0.0539621);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a5b8() {
   return (neuron0x9104cc8()*0.332535);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a5e0() {
   return (neuron0x9104ee0()*-0.169461);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a608() {
   return (neuron0x91050f8()*-0.0224005);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a630() {
   return (neuron0x91052f8()*-0.183838);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a858() {
   return (neuron0x90f5bc0()*-0.33536);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a880() {
   return (neuron0x90f5dc0()*-0.25331);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a8a8() {
   return (neuron0x91043d0()*0.437769);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a8d0() {
   return (neuron0x91044f8()*0.616799);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a8f8() {
   return (neuron0x91046b0()*0.898754);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a920() {
   return (neuron0x91048b0()*0.542086);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a948() {
   return (neuron0x9104ab0()*1.11593);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a970() {
   return (neuron0x9104cc8()*-0.180975);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a998() {
   return (neuron0x9104ee0()*0.0315485);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a9c0() {
   return (neuron0x91050f8()*-0.751141);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910a9e8() {
   return (neuron0x91052f8()*-0.0219521);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106b38() {
   return (neuron0x90f5bc0()*-0.27031);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106b60() {
   return (neuron0x90f5dc0()*0.414208);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9106b88() {
   return (neuron0x91043d0()*0.216955);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ad30() {
   return (neuron0x91044f8()*-0.122119);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ad58() {
   return (neuron0x91046b0()*0.465523);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ad80() {
   return (neuron0x91048b0()*-0.101768);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ada8() {
   return (neuron0x9104ab0()*0.354201);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910add0() {
   return (neuron0x9104cc8()*0.198649);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910adf8() {
   return (neuron0x9104ee0()*0.000947989);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ae20() {
   return (neuron0x91050f8()*0.252139);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ae48() {
   return (neuron0x91052f8()*0.374689);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b088() {
   return (neuron0x90f5bc0()*-0.24905);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b0b0() {
   return (neuron0x90f5dc0()*-0.150963);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b0d8() {
   return (neuron0x91043d0()*-0.200202);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b100() {
   return (neuron0x91044f8()*-0.21199);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b128() {
   return (neuron0x91046b0()*-0.122909);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b150() {
   return (neuron0x91048b0()*-0.389838);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b178() {
   return (neuron0x9104ab0()*0.366632);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b1a0() {
   return (neuron0x9104cc8()*0.287794);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b1c8() {
   return (neuron0x9104ee0()*0.051527);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b1f0() {
   return (neuron0x91050f8()*0.109897);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b218() {
   return (neuron0x91052f8()*0.455066);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b458() {
   return (neuron0x90f5bc0()*-0.465941);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b480() {
   return (neuron0x90f5dc0()*0.156176);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b4a8() {
   return (neuron0x91043d0()*-0.322573);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b4d0() {
   return (neuron0x91044f8()*0.674945);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108340() {
   return (neuron0x91046b0()*0.270732);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108368() {
   return (neuron0x91048b0()*0.468063);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108390() {
   return (neuron0x9104ab0()*0.647385);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91083b8() {
   return (neuron0x9104cc8()*-0.0995639);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91083e0() {
   return (neuron0x9104ee0()*0.286909);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108408() {
   return (neuron0x91050f8()*0.196703);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108430() {
   return (neuron0x91052f8()*0.193378);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108670() {
   return (neuron0x90f5bc0()*-0.00596287);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108698() {
   return (neuron0x90f5dc0()*-0.0811776);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91086c0() {
   return (neuron0x91043d0()*-0.501014);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91086e8() {
   return (neuron0x91044f8()*0.049501);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9108710() {
   return (neuron0x91046b0()*-1.05203);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bd00() {
   return (neuron0x91048b0()*0.578537);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bd28() {
   return (neuron0x9104ab0()*0.154309);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bd50() {
   return (neuron0x9104cc8()*-0.136616);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bd78() {
   return (neuron0x9104ee0()*1.19692);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bda0() {
   return (neuron0x91050f8()*0.0256808);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bdc8() {
   return (neuron0x91052f8()*-0.110182);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c008() {
   return (neuron0x90f5bc0()*0.139588);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c030() {
   return (neuron0x90f5dc0()*-0.484155);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c058() {
   return (neuron0x91043d0()*-0.0148564);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c080() {
   return (neuron0x91044f8()*0.269873);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c0a8() {
   return (neuron0x91046b0()*-0.460972);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c0d0() {
   return (neuron0x91048b0()*-0.530351);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c0f8() {
   return (neuron0x9104ab0()*-0.685192);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c120() {
   return (neuron0x9104cc8()*-0.409627);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c148() {
   return (neuron0x9104ee0()*-0.0468801);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c170() {
   return (neuron0x91050f8()*-0.0800057);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c198() {
   return (neuron0x91052f8()*0.559953);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c3d8() {
   return (neuron0x90f5bc0()*0.0203194);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c400() {
   return (neuron0x90f5dc0()*0.0540912);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c428() {
   return (neuron0x91043d0()*0.561095);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c450() {
   return (neuron0x91044f8()*0.208804);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c478() {
   return (neuron0x91046b0()*0.6738);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c4a0() {
   return (neuron0x91048b0()*0.987224);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c4c8() {
   return (neuron0x9104ab0()*-0.692828);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c4f0() {
   return (neuron0x9104cc8()*0.608322);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c518() {
   return (neuron0x9104ee0()*0.313575);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c540() {
   return (neuron0x91050f8()*0.347394);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c568() {
   return (neuron0x91052f8()*0.256489);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c7a8() {
   return (neuron0x90f5bc0()*0.113663);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c7d0() {
   return (neuron0x90f5dc0()*-0.379859);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c7f8() {
   return (neuron0x91043d0()*-0.287444);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c820() {
   return (neuron0x91044f8()*0.464488);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c848() {
   return (neuron0x91046b0()*-0.163318);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c870() {
   return (neuron0x91048b0()*-0.770234);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c898() {
   return (neuron0x9104ab0()*-0.448504);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c8c0() {
   return (neuron0x9104cc8()*0.0657741);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c8e8() {
   return (neuron0x9104ee0()*-0.500808);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c910() {
   return (neuron0x91050f8()*-0.526275);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910c938() {
   return (neuron0x91052f8()*-0.556094);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cb78() {
   return (neuron0x90f5bc0()*0.492661);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cba0() {
   return (neuron0x90f5dc0()*0.288557);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cbc8() {
   return (neuron0x91043d0()*-0.406656);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cbf0() {
   return (neuron0x91044f8()*-0.109442);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cc18() {
   return (neuron0x91046b0()*0.599284);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cc40() {
   return (neuron0x91048b0()*-0.452722);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cc68() {
   return (neuron0x9104ab0()*0.40732);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cc90() {
   return (neuron0x9104cc8()*-0.202138);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ccb8() {
   return (neuron0x9104ee0()*0.645163);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cce0() {
   return (neuron0x91050f8()*0.23795);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cd08() {
   return (neuron0x91052f8()*-0.81224);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cf48() {
   return (neuron0x90f5bc0()*-0.0687734);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cf70() {
   return (neuron0x90f5dc0()*0.331091);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cf98() {
   return (neuron0x91043d0()*-0.104292);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cfc0() {
   return (neuron0x91044f8()*0.297927);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910cfe8() {
   return (neuron0x91046b0()*0.484207);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d010() {
   return (neuron0x91048b0()*-0.215864);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d038() {
   return (neuron0x9104ab0()*-0.632764);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d060() {
   return (neuron0x9104cc8()*0.272504);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d088() {
   return (neuron0x9104ee0()*-0.603998);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d0b0() {
   return (neuron0x91050f8()*0.110087);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d0d8() {
   return (neuron0x91052f8()*-0.412752);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d318() {
   return (neuron0x90f5bc0()*-0.15146);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d340() {
   return (neuron0x90f5dc0()*0.1589);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d368() {
   return (neuron0x91043d0()*0.102893);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d390() {
   return (neuron0x91044f8()*0.476247);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d3b8() {
   return (neuron0x91046b0()*0.155654);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d3e0() {
   return (neuron0x91048b0()*-0.329623);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d408() {
   return (neuron0x9104ab0()*0.0846851);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d430() {
   return (neuron0x9104cc8()*0.0183875);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d458() {
   return (neuron0x9104ee0()*-0.0126371);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d480() {
   return (neuron0x91050f8()*0.61413);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d4a8() {
   return (neuron0x91052f8()*-0.225222);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d6e8() {
   return (neuron0x90f5bc0()*-0.360754);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109328() {
   return (neuron0x90f5dc0()*0.143538);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109350() {
   return (neuron0x91043d0()*-0.0497135);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109378() {
   return (neuron0x91044f8()*0.269855);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91093d8() {
   return (neuron0x91046b0()*0.137431);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109400() {
   return (neuron0x91048b0()*-0.252461);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109428() {
   return (neuron0x9104ab0()*0.0696946);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109488() {
   return (neuron0x9104cc8()*0.177683);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91094b0() {
   return (neuron0x9104ee0()*-0.507625);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91094d8() {
   return (neuron0x91050f8()*0.369011);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109538() {
   return (neuron0x91052f8()*0.127429);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91096e0() {
   return (neuron0x90f5bc0()*0.569677);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109630() {
   return (neuron0x90f5dc0()*-0.390311);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109748() {
   return (neuron0x91043d0()*0.172149);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109770() {
   return (neuron0x91044f8()*-0.148552);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109798() {
   return (neuron0x91046b0()*-0.490971);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91097f8() {
   return (neuron0x91048b0()*0.0939737);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109820() {
   return (neuron0x9104ab0()*1.11283);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109848() {
   return (neuron0x9104cc8()*0.416228);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91098a8() {
   return (neuron0x9104ee0()*0.0213968);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91098d0() {
   return (neuron0x91050f8()*0.012504);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91098f8() {
   return (neuron0x91052f8()*0.178773);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91099a0() {
   return (neuron0x90f5bc0()*-0.380433);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9109a50() {
   return (neuron0x90f5dc0()*-0.401455);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e500() {
   return (neuron0x91043d0()*-0.0287568);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e528() {
   return (neuron0x91044f8()*-0.141382);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e550() {
   return (neuron0x91046b0()*0.443456);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e578() {
   return (neuron0x91048b0()*-0.200386);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e5a0() {
   return (neuron0x9104ab0()*0.309865);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e5c8() {
   return (neuron0x9104cc8()*0.523821);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e5f0() {
   return (neuron0x9104ee0()*-0.257634);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e618() {
   return (neuron0x91050f8()*0.524141);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e640() {
   return (neuron0x91052f8()*-0.474907);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e868() {
   return (neuron0x90f5bc0()*0.508365);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e890() {
   return (neuron0x90f5dc0()*-0.22407);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e8b8() {
   return (neuron0x91043d0()*-0.273639);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e8e0() {
   return (neuron0x91044f8()*0.139299);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e908() {
   return (neuron0x91046b0()*-0.251426);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e930() {
   return (neuron0x91048b0()*0.0775886);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e958() {
   return (neuron0x9104ab0()*0.102821);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e980() {
   return (neuron0x9104cc8()*-0.228028);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e9a8() {
   return (neuron0x9104ee0()*0.303213);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e9d0() {
   return (neuron0x91050f8()*-0.52652);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910e9f8() {
   return (neuron0x91052f8()*-0.0632153);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ec20() {
   return (neuron0x90f5bc0()*0.285043);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ec48() {
   return (neuron0x90f5dc0()*0.142812);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ec70() {
   return (neuron0x91043d0()*-0.253437);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ec98() {
   return (neuron0x91044f8()*-0.0906898);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ecc0() {
   return (neuron0x91046b0()*0.0294612);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ece8() {
   return (neuron0x91048b0()*-0.107202);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ed10() {
   return (neuron0x9104ab0()*-0.19344);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ed38() {
   return (neuron0x9104cc8()*-0.282696);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ed60() {
   return (neuron0x9104ee0()*-0.201027);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ed88() {
   return (neuron0x91050f8()*-0.280597);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910edb0() {
   return (neuron0x91052f8()*-0.472648);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910efd8() {
   return (neuron0x90f5bc0()*0.222151);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f000() {
   return (neuron0x90f5dc0()*0.00551775);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f028() {
   return (neuron0x91043d0()*-0.61927);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f050() {
   return (neuron0x91044f8()*-0.265545);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f078() {
   return (neuron0x91046b0()*-0.361954);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f0a0() {
   return (neuron0x91048b0()*-0.404918);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f0c8() {
   return (neuron0x9104ab0()*-0.797681);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f0f0() {
   return (neuron0x9104cc8()*-0.165495);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f118() {
   return (neuron0x9104ee0()*1.02376);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f140() {
   return (neuron0x91050f8()*0.193211);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f168() {
   return (neuron0x91052f8()*0.00510557);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f3a8() {
   return (neuron0x90f5bc0()*0.0793886);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f3d0() {
   return (neuron0x90f5dc0()*0.0790634);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f3f8() {
   return (neuron0x91043d0()*1.45675);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f420() {
   return (neuron0x91044f8()*0.158716);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f448() {
   return (neuron0x91046b0()*-1.15945);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f470() {
   return (neuron0x91048b0()*0.216943);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f498() {
   return (neuron0x9104ab0()*-1.94168);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f4c0() {
   return (neuron0x9104cc8()*0.246294);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f4e8() {
   return (neuron0x9104ee0()*0.943374);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f510() {
   return (neuron0x91050f8()*0.00211792);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f538() {
   return (neuron0x91052f8()*0.0167545);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f778() {
   return (neuron0x90f5bc0()*0.597843);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f7a0() {
   return (neuron0x90f5dc0()*-0.611631);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f7c8() {
   return (neuron0x91043d0()*-0.563724);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f7f0() {
   return (neuron0x91044f8()*0.297621);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f818() {
   return (neuron0x91046b0()*-0.0647778);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f840() {
   return (neuron0x91048b0()*-0.569304);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f868() {
   return (neuron0x9104ab0()*0.210598);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f890() {
   return (neuron0x9104cc8()*0.550106);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f8b8() {
   return (neuron0x9104ee0()*0.412637);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f8e0() {
   return (neuron0x91050f8()*-0.576068);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910f908() {
   return (neuron0x91052f8()*0.0221105);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fb48() {
   return (neuron0x90f5bc0()*-0.253748);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fb70() {
   return (neuron0x90f5dc0()*-0.14425);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fb98() {
   return (neuron0x91043d0()*0.919326);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fbc0() {
   return (neuron0x91044f8()*-0.0372461);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fbe8() {
   return (neuron0x91046b0()*-1.18896);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fc10() {
   return (neuron0x91048b0()*0.239539);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fc38() {
   return (neuron0x9104ab0()*-0.111416);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fc60() {
   return (neuron0x9104cc8()*-0.263439);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fc88() {
   return (neuron0x9104ee0()*1.31511);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fcb0() {
   return (neuron0x91050f8()*-0.243268);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910fcd8() {
   return (neuron0x91052f8()*0.0186109);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ff18() {
   return (neuron0x90f5bc0()*0.137921);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ff40() {
   return (neuron0x90f5dc0()*0.431764);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ff68() {
   return (neuron0x91043d0()*-0.20862);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ff90() {
   return (neuron0x91044f8()*-0.0330673);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ffb8() {
   return (neuron0x91046b0()*-0.350075);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ffe0() {
   return (neuron0x91048b0()*-0.374318);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110008() {
   return (neuron0x9104ab0()*-0.00296841);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110030() {
   return (neuron0x9104cc8()*-0.0121631);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110058() {
   return (neuron0x9104ee0()*-1.00394);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110080() {
   return (neuron0x91050f8()*0.063289);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91100a8() {
   return (neuron0x91052f8()*-0.417643);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91102e8() {
   return (neuron0x90f5bc0()*0.221809);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110310() {
   return (neuron0x90f5dc0()*0.400517);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110338() {
   return (neuron0x91043d0()*-0.214119);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110360() {
   return (neuron0x91044f8()*0.339218);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110388() {
   return (neuron0x91046b0()*0.178301);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91103b0() {
   return (neuron0x91048b0()*0.344627);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91103d8() {
   return (neuron0x9104ab0()*0.0821543);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110400() {
   return (neuron0x9104cc8()*-0.273288);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110428() {
   return (neuron0x9104ee0()*0.237556);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110450() {
   return (neuron0x91050f8()*-0.33575);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110478() {
   return (neuron0x91052f8()*-0.329863);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91106b8() {
   return (neuron0x90f5bc0()*-0.488959);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91106e0() {
   return (neuron0x90f5dc0()*0.217026);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110708() {
   return (neuron0x91043d0()*-0.646115);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110730() {
   return (neuron0x91044f8()*-0.419993);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110758() {
   return (neuron0x91046b0()*0.284487);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110780() {
   return (neuron0x91048b0()*0.033292);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91107a8() {
   return (neuron0x9104ab0()*0.216792);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91107d0() {
   return (neuron0x9104cc8()*0.696951);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91107f8() {
   return (neuron0x9104ee0()*0.173947);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110820() {
   return (neuron0x91050f8()*-0.459426);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110848() {
   return (neuron0x91052f8()*-0.0744089);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110a88() {
   return (neuron0x90f5bc0()*0.298161);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110ab0() {
   return (neuron0x90f5dc0()*-0.0294151);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110ad8() {
   return (neuron0x91043d0()*-0.881344);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110b00() {
   return (neuron0x91044f8()*0.310018);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110b28() {
   return (neuron0x91046b0()*0.740813);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110b50() {
   return (neuron0x91048b0()*0.0926528);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110b78() {
   return (neuron0x9104ab0()*0.308191);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110ba0() {
   return (neuron0x9104cc8()*0.139062);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110bc8() {
   return (neuron0x9104ee0()*0.965215);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110bf0() {
   return (neuron0x91050f8()*-0.186905);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110c18() {
   return (neuron0x91052f8()*0.281818);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110e58() {
   return (neuron0x90f5bc0()*-0.25599);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110e80() {
   return (neuron0x90f5dc0()*-0.244989);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110ea8() {
   return (neuron0x91043d0()*-0.0240112);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110ed0() {
   return (neuron0x91044f8()*-0.106385);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110ef8() {
   return (neuron0x91046b0()*-0.227733);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110f20() {
   return (neuron0x91048b0()*0.126288);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110f48() {
   return (neuron0x9104ab0()*0.181302);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110f70() {
   return (neuron0x9104cc8()*-0.186149);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110f98() {
   return (neuron0x9104ee0()*-0.407186);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110fc0() {
   return (neuron0x91050f8()*-0.202099);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9110fe8() {
   return (neuron0x91052f8()*0.13651);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111228() {
   return (neuron0x90f5bc0()*-0.0969379);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111250() {
   return (neuron0x90f5dc0()*-0.484505);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111278() {
   return (neuron0x91043d0()*0.915604);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91112a0() {
   return (neuron0x91044f8()*0.148562);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91112c8() {
   return (neuron0x91046b0()*-0.397247);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91112f0() {
   return (neuron0x91048b0()*-0.654748);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111318() {
   return (neuron0x9104ab0()*-0.788639);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111340() {
   return (neuron0x9104cc8()*0.389436);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111368() {
   return (neuron0x9104ee0()*-0.297899);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111390() {
   return (neuron0x91050f8()*0.161218);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91113b8() {
   return (neuron0x91052f8()*0.531867);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91115f8() {
   return (neuron0x90f5bc0()*-0.29464);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111620() {
   return (neuron0x90f5dc0()*-0.358051);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111648() {
   return (neuron0x91043d0()*0.558062);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111670() {
   return (neuron0x91044f8()*-0.199629);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111698() {
   return (neuron0x91046b0()*0.357657);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91116c0() {
   return (neuron0x91048b0()*0.465188);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91116e8() {
   return (neuron0x9104ab0()*0.183445);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b4f8() {
   return (neuron0x9104cc8()*0.601631);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b520() {
   return (neuron0x9104ee0()*-0.709096);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b548() {
   return (neuron0x91050f8()*0.0753292);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b570() {
   return (neuron0x91052f8()*-0.398542);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b7b0() {
   return (neuron0x90f5bc0()*-0.0347677);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b7d8() {
   return (neuron0x90f5dc0()*-0.162521);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b800() {
   return (neuron0x91043d0()*0.701398);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b828() {
   return (neuron0x91044f8()*0.305043);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b850() {
   return (neuron0x91046b0()*0.0995221);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b878() {
   return (neuron0x91048b0()*0.177208);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b8a0() {
   return (neuron0x9104ab0()*-0.050837);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b8c8() {
   return (neuron0x9104cc8()*0.298654);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b8f0() {
   return (neuron0x9104ee0()*0.0576918);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b918() {
   return (neuron0x91050f8()*-0.144523);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910b940() {
   return (neuron0x91052f8()*0.106396);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bb80() {
   return (neuron0x90f5bc0()*0.252974);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bba8() {
   return (neuron0x90f5dc0()*0.0762913);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bbd0() {
   return (neuron0x91043d0()*-0.481039);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bbf8() {
   return (neuron0x91044f8()*-0.0917199);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bc20() {
   return (neuron0x91046b0()*0.0148935);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bc48() {
   return (neuron0x91048b0()*0.202773);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bc70() {
   return (neuron0x9104ab0()*-0.329601);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bc98() {
   return (neuron0x9104cc8()*0.00836785);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910bcc0() {
   return (neuron0x9104ee0()*-0.098808);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112718() {
   return (neuron0x91050f8()*0.172113);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112740() {
   return (neuron0x91052f8()*-0.587641);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112968() {
   return (neuron0x90f5bc0()*-0.0219149);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112990() {
   return (neuron0x90f5dc0()*-0.421419);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91129b8() {
   return (neuron0x91043d0()*0.36102);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91129e0() {
   return (neuron0x91044f8()*0.55159);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112a08() {
   return (neuron0x91046b0()*0.250685);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112a30() {
   return (neuron0x91048b0()*0.219838);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112a58() {
   return (neuron0x9104ab0()*-0.00975067);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112a80() {
   return (neuron0x9104cc8()*0.459316);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112aa8() {
   return (neuron0x9104ee0()*-1.38941);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112ad0() {
   return (neuron0x91050f8()*-0.265767);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112af8() {
   return (neuron0x91052f8()*-0.135289);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112d38() {
   return (neuron0x90f5bc0()*-0.0586472);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112d60() {
   return (neuron0x90f5dc0()*0.205951);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112d88() {
   return (neuron0x91043d0()*-0.0678143);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112db0() {
   return (neuron0x91044f8()*-0.232773);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112dd8() {
   return (neuron0x91046b0()*0.553039);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112e00() {
   return (neuron0x91048b0()*0.025927);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112e28() {
   return (neuron0x9104ab0()*0.78692);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112e50() {
   return (neuron0x9104cc8()*0.286434);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112e78() {
   return (neuron0x9104ee0()*-0.918816);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112ea0() {
   return (neuron0x91050f8()*-0.0211406);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112ec8() {
   return (neuron0x91052f8()*-0.509112);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113108() {
   return (neuron0x90f5bc0()*0.155899);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113130() {
   return (neuron0x90f5dc0()*-0.397064);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113158() {
   return (neuron0x91043d0()*-0.593355);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113180() {
   return (neuron0x91044f8()*0.148864);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91131a8() {
   return (neuron0x91046b0()*-0.80795);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91131d0() {
   return (neuron0x91048b0()*-0.0863462);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91131f8() {
   return (neuron0x9104ab0()*1.29762);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113220() {
   return (neuron0x9104cc8()*0.58141);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113248() {
   return (neuron0x9104ee0()*-0.558593);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113270() {
   return (neuron0x91050f8()*-0.140311);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113298() {
   return (neuron0x91052f8()*0.142349);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91134d8() {
   return (neuron0x90f5bc0()*0.176813);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113500() {
   return (neuron0x90f5dc0()*0.107292);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113528() {
   return (neuron0x91043d0()*0.0426532);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113550() {
   return (neuron0x91044f8()*0.337193);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113578() {
   return (neuron0x91046b0()*0.557198);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91135a0() {
   return (neuron0x91048b0()*-0.113681);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91135c8() {
   return (neuron0x9104ab0()*-0.500187);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91135f0() {
   return (neuron0x9104cc8()*0.0268123);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113618() {
   return (neuron0x9104ee0()*-0.224496);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113640() {
   return (neuron0x91050f8()*0.159989);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113668() {
   return (neuron0x91052f8()*0.229116);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ac28() {
   return (neuron0x90f5bc0()*0.0140562);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ac50() {
   return (neuron0x90f5dc0()*0.353933);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910ac78() {
   return (neuron0x91043d0()*-0.302201);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910aca0() {
   return (neuron0x91044f8()*-0.625578);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910acc8() {
   return (neuron0x91046b0()*0.255645);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910acf0() {
   return (neuron0x91048b0()*-0.0920482);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113ab0() {
   return (neuron0x9104ab0()*-0.344542);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113ad8() {
   return (neuron0x9104cc8()*-0.256491);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113b00() {
   return (neuron0x9104ee0()*-0.216041);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113b28() {
   return (neuron0x91050f8()*-0.132522);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113b50() {
   return (neuron0x91052f8()*0.528437);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113d78() {
   return (neuron0x90f5bc0()*0.619808);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113da0() {
   return (neuron0x90f5dc0()*-0.320849);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113dc8() {
   return (neuron0x91043d0()*-0.0256927);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113df0() {
   return (neuron0x91044f8()*-0.59103);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113e18() {
   return (neuron0x91046b0()*-0.34851);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113e40() {
   return (neuron0x91048b0()*-0.0467606);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113e68() {
   return (neuron0x9104ab0()*0.0348738);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113e90() {
   return (neuron0x9104cc8()*-0.212407);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113eb8() {
   return (neuron0x9104ee0()*-0.112928);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113ee0() {
   return (neuron0x91050f8()*-0.730698);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9113f08() {
   return (neuron0x91052f8()*0.565448);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114148() {
   return (neuron0x90f5bc0()*0.216062);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114170() {
   return (neuron0x90f5dc0()*0.0485779);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114198() {
   return (neuron0x91043d0()*0.666534);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91141c0() {
   return (neuron0x91044f8()*-0.275887);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91141e8() {
   return (neuron0x91046b0()*0.495937);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114210() {
   return (neuron0x91048b0()*-0.245752);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114238() {
   return (neuron0x9104ab0()*-0.646575);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114260() {
   return (neuron0x9104cc8()*-0.407111);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114288() {
   return (neuron0x9104ee0()*1.15212);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91142b0() {
   return (neuron0x91050f8()*0.194274);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91142d8() {
   return (neuron0x91052f8()*0.104702);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114518() {
   return (neuron0x90f5bc0()*-0.127143);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114540() {
   return (neuron0x90f5dc0()*-0.0456264);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114568() {
   return (neuron0x91043d0()*1.07894);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114590() {
   return (neuron0x91044f8()*0.0181658);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91145b8() {
   return (neuron0x91046b0()*0.72876);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91145e0() {
   return (neuron0x91048b0()*-0.0715817);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114608() {
   return (neuron0x9104ab0()*-0.582664);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114630() {
   return (neuron0x9104cc8()*0.132903);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114658() {
   return (neuron0x9104ee0()*2.37962);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114680() {
   return (neuron0x91050f8()*0.142611);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91146a8() {
   return (neuron0x91052f8()*0.0474301);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91148e8() {
   return (neuron0x90f5bc0()*0.240698);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114910() {
   return (neuron0x90f5dc0()*0.465069);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114938() {
   return (neuron0x91043d0()*-0.340205);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114960() {
   return (neuron0x91044f8()*-0.358787);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114988() {
   return (neuron0x91046b0()*-0.736404);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91149b0() {
   return (neuron0x91048b0()*-0.128054);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91149d8() {
   return (neuron0x9104ab0()*0.184099);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114a00() {
   return (neuron0x9104cc8()*0.297396);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114a28() {
   return (neuron0x9104ee0()*0.275063);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114a50() {
   return (neuron0x91050f8()*0.0516317);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114a78() {
   return (neuron0x91052f8()*0.407876);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114cb8() {
   return (neuron0x90f5bc0()*-0.0756166);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114ce0() {
   return (neuron0x90f5dc0()*0.220198);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114d08() {
   return (neuron0x91043d0()*0.0198939);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114d30() {
   return (neuron0x91044f8()*-0.0561676);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114d58() {
   return (neuron0x91046b0()*-0.25831);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114d80() {
   return (neuron0x91048b0()*-0.194114);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114da8() {
   return (neuron0x9104ab0()*0.194938);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114dd0() {
   return (neuron0x9104cc8()*-0.010484);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114df8() {
   return (neuron0x9104ee0()*0.724886);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114e20() {
   return (neuron0x91050f8()*-0.309873);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9114e48() {
   return (neuron0x91052f8()*0.125423);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115088() {
   return (neuron0x90f5bc0()*-0.185596);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91150b0() {
   return (neuron0x90f5dc0()*0.442765);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91150d8() {
   return (neuron0x91043d0()*-0.248278);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115100() {
   return (neuron0x91044f8()*-0.0316931);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115128() {
   return (neuron0x91046b0()*0.0250123);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115150() {
   return (neuron0x91048b0()*0.167334);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115178() {
   return (neuron0x9104ab0()*0.177752);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91151a0() {
   return (neuron0x9104cc8()*-0.212925);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91151c8() {
   return (neuron0x9104ee0()*0.0610455);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91151f0() {
   return (neuron0x91050f8()*0.0850776);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115218() {
   return (neuron0x91052f8()*0.236211);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115458() {
   return (neuron0x90f5bc0()*-0.627251);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115480() {
   return (neuron0x90f5dc0()*0.0227693);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91154a8() {
   return (neuron0x91043d0()*0.3801);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91154d0() {
   return (neuron0x91044f8()*-0.427468);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91154f8() {
   return (neuron0x91046b0()*-0.424003);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115520() {
   return (neuron0x91048b0()*0.381166);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115548() {
   return (neuron0x9104ab0()*-0.344289);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115570() {
   return (neuron0x9104cc8()*0.29642);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115598() {
   return (neuron0x9104ee0()*-0.124028);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91155c0() {
   return (neuron0x91050f8()*-0.0168398);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91155e8() {
   return (neuron0x91052f8()*0.190294);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115828() {
   return (neuron0x90f5bc0()*-0.66266);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115850() {
   return (neuron0x90f5dc0()*0.183875);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115878() {
   return (neuron0x91043d0()*0.47974);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91158a0() {
   return (neuron0x91044f8()*-0.134842);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91158c8() {
   return (neuron0x91046b0()*-0.400822);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91158f0() {
   return (neuron0x91048b0()*-0.765604);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115918() {
   return (neuron0x9104ab0()*0.150091);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115940() {
   return (neuron0x9104cc8()*-0.83796);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115968() {
   return (neuron0x9104ee0()*-0.38493);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115990() {
   return (neuron0x91050f8()*0.44851);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91159b8() {
   return (neuron0x91052f8()*-0.122187);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115bf8() {
   return (neuron0x90f5bc0()*0.0189089);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115c20() {
   return (neuron0x90f5dc0()*-0.0251438);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115c48() {
   return (neuron0x91043d0()*1.64856);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115c70() {
   return (neuron0x91044f8()*-0.286759);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115c98() {
   return (neuron0x91046b0()*-0.20448);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115cc0() {
   return (neuron0x91048b0()*0.256796);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115ce8() {
   return (neuron0x9104ab0()*-1.00445);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115d10() {
   return (neuron0x9104cc8()*-0.415413);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115d38() {
   return (neuron0x9104ee0()*0.627005);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115d60() {
   return (neuron0x91050f8()*-0.18753);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115d88() {
   return (neuron0x91052f8()*-0.0934497);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9115fc8() {
   return (neuron0x90f5bc0()*0.0293111);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d710() {
   return (neuron0x90f5dc0()*-0.466589);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d738() {
   return (neuron0x91043d0()*-0.109396);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d760() {
   return (neuron0x91044f8()*-0.332286);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d990() {
   return (neuron0x91046b0()*-0.0903937);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910d9b8() {
   return (neuron0x91048b0()*-0.0426246);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910dbe8() {
   return (neuron0x9104ab0()*-0.237096);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910dc10() {
   return (neuron0x9104cc8()*0.0641559);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910de48() {
   return (neuron0x9104ee0()*0.174691);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910de70() {
   return (neuron0x91050f8()*-0.56198);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x910de98() {
   return (neuron0x91052f8()*0.279031);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116eb8() {
   return (neuron0x90f5bc0()*-0.480394);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116ee0() {
   return (neuron0x90f5dc0()*-0.723749);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116f08() {
   return (neuron0x91043d0()*-0.314693);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116f30() {
   return (neuron0x91044f8()*0.639228);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116f58() {
   return (neuron0x91046b0()*-0.0972915);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116f80() {
   return (neuron0x91048b0()*-0.215467);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116fa8() {
   return (neuron0x9104ab0()*-0.0384711);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116fd0() {
   return (neuron0x9104cc8()*-0.357211);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9116ff8() {
   return (neuron0x9104ee0()*0.22521);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117020() {
   return (neuron0x91050f8()*-0.0359442);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117048() {
   return (neuron0x91052f8()*0.0827255);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117270() {
   return (neuron0x90f5bc0()*0.537413);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117298() {
   return (neuron0x90f5dc0()*0.207758);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91172c0() {
   return (neuron0x91043d0()*-0.29387);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91172e8() {
   return (neuron0x91044f8()*0.370233);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117310() {
   return (neuron0x91046b0()*-0.437798);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117338() {
   return (neuron0x91048b0()*-0.370081);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117360() {
   return (neuron0x9104ab0()*0.298494);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117388() {
   return (neuron0x9104cc8()*0.520495);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91173b0() {
   return (neuron0x9104ee0()*0.211227);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91173d8() {
   return (neuron0x91050f8()*0.0466059);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117400() {
   return (neuron0x91052f8()*0.00982059);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117640() {
   return (neuron0x90f5bc0()*-0.134809);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117668() {
   return (neuron0x90f5dc0()*-0.204988);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117690() {
   return (neuron0x91043d0()*-0.514054);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91176b8() {
   return (neuron0x91044f8()*0.333207);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91176e0() {
   return (neuron0x91046b0()*0.72423);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117708() {
   return (neuron0x91048b0()*0.196463);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117730() {
   return (neuron0x9104ab0()*0.387493);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117758() {
   return (neuron0x9104cc8()*0.297592);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117780() {
   return (neuron0x9104ee0()*-0.28611);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91177a8() {
   return (neuron0x91050f8()*0.429925);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91177d0() {
   return (neuron0x91052f8()*0.712152);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117a10() {
   return (neuron0x90f5bc0()*-0.129432);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117a38() {
   return (neuron0x90f5dc0()*-0.260882);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117a60() {
   return (neuron0x91043d0()*-0.103614);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117a88() {
   return (neuron0x91044f8()*-0.550672);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117ab0() {
   return (neuron0x91046b0()*0.0206855);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117ad8() {
   return (neuron0x91048b0()*0.0332463);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117b00() {
   return (neuron0x9104ab0()*-0.146338);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117b28() {
   return (neuron0x9104cc8()*-0.103124);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117b50() {
   return (neuron0x9104ee0()*0.0429719);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117b78() {
   return (neuron0x91050f8()*0.632534);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117ba0() {
   return (neuron0x91052f8()*0.435488);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117de0() {
   return (neuron0x90f5bc0()*-0.199984);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117e08() {
   return (neuron0x90f5dc0()*-0.00200052);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117e30() {
   return (neuron0x91043d0()*-1.73588);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117e58() {
   return (neuron0x91044f8()*0.0538755);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117e80() {
   return (neuron0x91046b0()*0.720964);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117ea8() {
   return (neuron0x91048b0()*0.276512);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117ed0() {
   return (neuron0x9104ab0()*-0.670721);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117ef8() {
   return (neuron0x9104cc8()*0.236921);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117f20() {
   return (neuron0x9104ee0()*1.52477);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117f48() {
   return (neuron0x91050f8()*0.160144);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9117f70() {
   return (neuron0x91052f8()*-0.0269928);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91181b0() {
   return (neuron0x90f5bc0()*0.565459);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91181d8() {
   return (neuron0x90f5dc0()*0.608011);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118200() {
   return (neuron0x91043d0()*-0.455023);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118228() {
   return (neuron0x91044f8()*0.255051);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118250() {
   return (neuron0x91046b0()*-0.144292);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118278() {
   return (neuron0x91048b0()*-0.624787);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91182a0() {
   return (neuron0x9104ab0()*0.323846);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91182c8() {
   return (neuron0x9104cc8()*0.74545);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91182f0() {
   return (neuron0x9104ee0()*0.202138);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118318() {
   return (neuron0x91050f8()*0.212019);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118340() {
   return (neuron0x91052f8()*-0.460797);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118580() {
   return (neuron0x90f5bc0()*0.394932);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91185a8() {
   return (neuron0x90f5dc0()*-0.512637);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91185d0() {
   return (neuron0x91043d0()*-0.647443);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91185f8() {
   return (neuron0x91044f8()*0.722533);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118620() {
   return (neuron0x91046b0()*-0.321249);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118648() {
   return (neuron0x91048b0()*0.386917);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118670() {
   return (neuron0x9104ab0()*0.22533);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118698() {
   return (neuron0x9104cc8()*-0.149422);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91186c0() {
   return (neuron0x9104ee0()*-1.20875);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91186e8() {
   return (neuron0x91050f8()*0.00467835);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118710() {
   return (neuron0x91052f8()*0.693546);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118950() {
   return (neuron0x90f5bc0()*-0.169325);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118978() {
   return (neuron0x90f5dc0()*-0.0160996);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91189a0() {
   return (neuron0x91043d0()*0.0490772);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91189c8() {
   return (neuron0x91044f8()*-0.207868);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91189f0() {
   return (neuron0x91046b0()*-0.521747);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118a18() {
   return (neuron0x91048b0()*-0.0611457);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118a40() {
   return (neuron0x9104ab0()*-0.602037);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118a68() {
   return (neuron0x9104cc8()*0.0969469);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118a90() {
   return (neuron0x9104ee0()*0.148916);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118ab8() {
   return (neuron0x91050f8()*0.354548);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118ae0() {
   return (neuron0x91052f8()*-0.253547);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118d20() {
   return (neuron0x90f5bc0()*0.231471);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118d48() {
   return (neuron0x90f5dc0()*0.385619);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118d70() {
   return (neuron0x91043d0()*-0.423355);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118d98() {
   return (neuron0x91044f8()*0.431391);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118dc0() {
   return (neuron0x91046b0()*0.10078);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118de8() {
   return (neuron0x91048b0()*-0.171203);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118e10() {
   return (neuron0x9104ab0()*0.200161);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118e38() {
   return (neuron0x9104cc8()*-0.503268);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118e60() {
   return (neuron0x9104ee0()*0.232328);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118e88() {
   return (neuron0x91050f8()*-0.427161);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9118eb0() {
   return (neuron0x91052f8()*-0.0492228);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91190f0() {
   return (neuron0x90f5bc0()*0.308803);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119118() {
   return (neuron0x90f5dc0()*0.312454);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119140() {
   return (neuron0x91043d0()*-0.147346);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119168() {
   return (neuron0x91044f8()*0.426417);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119190() {
   return (neuron0x91046b0()*-0.0831419);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91191b8() {
   return (neuron0x91048b0()*-0.335167);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91191e0() {
   return (neuron0x9104ab0()*-0.603089);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119208() {
   return (neuron0x9104cc8()*0.186589);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119230() {
   return (neuron0x9104ee0()*0.576692);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119258() {
   return (neuron0x91050f8()*-0.450956);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119280() {
   return (neuron0x91052f8()*-0.473637);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91194c0() {
   return (neuron0x90f5bc0()*-0.713453);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91194e8() {
   return (neuron0x90f5dc0()*-0.37858);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119510() {
   return (neuron0x91043d0()*-0.460554);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119538() {
   return (neuron0x91044f8()*0.311367);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119560() {
   return (neuron0x91046b0()*0.144834);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119588() {
   return (neuron0x91048b0()*-0.208437);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91195b0() {
   return (neuron0x9104ab0()*0.0268916);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91195d8() {
   return (neuron0x9104cc8()*0.0706703);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119600() {
   return (neuron0x9104ee0()*-0.864222);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119628() {
   return (neuron0x91050f8()*0.389842);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119650() {
   return (neuron0x91052f8()*0.270739);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119890() {
   return (neuron0x90f5bc0()*-0.499364);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91198b8() {
   return (neuron0x90f5dc0()*0.0179894);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91198e0() {
   return (neuron0x91043d0()*-0.131729);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119908() {
   return (neuron0x91044f8()*-0.311204);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119930() {
   return (neuron0x91046b0()*0.237569);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119958() {
   return (neuron0x91048b0()*0.463957);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119980() {
   return (neuron0x9104ab0()*-0.234975);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91199a8() {
   return (neuron0x9104cc8()*-0.000837939);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91199d0() {
   return (neuron0x9104ee0()*-0.263471);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91199f8() {
   return (neuron0x91050f8()*0.264849);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119a20() {
   return (neuron0x91052f8()*0.230745);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119c60() {
   return (neuron0x90f5bc0()*0.159443);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119c88() {
   return (neuron0x90f5dc0()*0.765402);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119cb0() {
   return (neuron0x91043d0()*0.125344);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119cd8() {
   return (neuron0x91044f8()*-0.286091);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119d00() {
   return (neuron0x91046b0()*0.821704);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119d28() {
   return (neuron0x91048b0()*0.914416);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119d50() {
   return (neuron0x9104ab0()*0.060969);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119d78() {
   return (neuron0x9104cc8()*-0.081775);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119da0() {
   return (neuron0x9104ee0()*0.604874);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119dc8() {
   return (neuron0x91050f8()*-0.218938);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9119df0() {
   return (neuron0x91052f8()*0.338316);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a030() {
   return (neuron0x90f5bc0()*0.163495);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a058() {
   return (neuron0x90f5dc0()*-0.416756);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a080() {
   return (neuron0x91043d0()*0.0270981);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a0a8() {
   return (neuron0x91044f8()*0.143528);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a0d0() {
   return (neuron0x91046b0()*0.278635);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a0f8() {
   return (neuron0x91048b0()*0.46282);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a120() {
   return (neuron0x9104ab0()*-0.391943);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a148() {
   return (neuron0x9104cc8()*-0.204531);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a170() {
   return (neuron0x9104ee0()*0.141717);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a198() {
   return (neuron0x91050f8()*0.134127);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a1c0() {
   return (neuron0x91052f8()*0.496449);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a400() {
   return (neuron0x90f5bc0()*0.583854);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a428() {
   return (neuron0x90f5dc0()*-0.301971);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a450() {
   return (neuron0x91043d0()*0.502649);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a478() {
   return (neuron0x91044f8()*0.0284175);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a4a0() {
   return (neuron0x91046b0()*0.0504084);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a4c8() {
   return (neuron0x91048b0()*0.0874194);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a4f0() {
   return (neuron0x9104ab0()*0.0686691);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a518() {
   return (neuron0x9104cc8()*-0.0233248);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a540() {
   return (neuron0x9104ee0()*-0.0584642);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a568() {
   return (neuron0x91050f8()*0.418249);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a590() {
   return (neuron0x91052f8()*-0.339223);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a7d0() {
   return (neuron0x90f5bc0()*0.302437);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a7f8() {
   return (neuron0x90f5dc0()*-0.00557187);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a820() {
   return (neuron0x91043d0()*0.454523);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a848() {
   return (neuron0x91044f8()*0.47704);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a870() {
   return (neuron0x91046b0()*-0.759552);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a898() {
   return (neuron0x91048b0()*-0.298795);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a8c0() {
   return (neuron0x9104ab0()*1.05222);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a8e8() {
   return (neuron0x9104cc8()*-0.0610174);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a910() {
   return (neuron0x9104ee0()*-0.142437);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a938() {
   return (neuron0x91050f8()*-0.355071);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911a960() {
   return (neuron0x91052f8()*-0.495701);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911aba0() {
   return (neuron0x90f5bc0()*0.328883);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911abc8() {
   return (neuron0x90f5dc0()*-0.204836);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911abf0() {
   return (neuron0x91043d0()*-1.20358);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ac18() {
   return (neuron0x91044f8()*0.0355477);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ac40() {
   return (neuron0x91046b0()*1.15763);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ac68() {
   return (neuron0x91048b0()*0.336425);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ac90() {
   return (neuron0x9104ab0()*-0.231257);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911acb8() {
   return (neuron0x9104cc8()*0.717976);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ace0() {
   return (neuron0x9104ee0()*-2.03593);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ad08() {
   return (neuron0x91050f8()*-0.314153);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ad30() {
   return (neuron0x91052f8()*0.137728);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911af70() {
   return (neuron0x90f5bc0()*0.0774263);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911af98() {
   return (neuron0x90f5dc0()*0.576306);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911afc0() {
   return (neuron0x91043d0()*0.0520792);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911afe8() {
   return (neuron0x91044f8()*0.340303);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b010() {
   return (neuron0x91046b0()*0.317495);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b038() {
   return (neuron0x91048b0()*0.421895);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b060() {
   return (neuron0x9104ab0()*0.342582);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b088() {
   return (neuron0x9104cc8()*0.260892);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b0b0() {
   return (neuron0x9104ee0()*-0.880411);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b0d8() {
   return (neuron0x91050f8()*-0.549855);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b100() {
   return (neuron0x91052f8()*0.2462);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b340() {
   return (neuron0x90f5bc0()*0.40294);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b368() {
   return (neuron0x90f5dc0()*0.214544);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b390() {
   return (neuron0x91043d0()*0.00288835);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b3b8() {
   return (neuron0x91044f8()*-0.0453675);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b3e0() {
   return (neuron0x91046b0()*-0.398722);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b408() {
   return (neuron0x91048b0()*-0.102241);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b430() {
   return (neuron0x9104ab0()*0.0453341);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b458() {
   return (neuron0x9104cc8()*0.07657);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b480() {
   return (neuron0x9104ee0()*-0.360895);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b4a8() {
   return (neuron0x91050f8()*0.228218);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b4d0() {
   return (neuron0x91052f8()*0.359611);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b710() {
   return (neuron0x90f5bc0()*0.210924);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b738() {
   return (neuron0x90f5dc0()*-0.0250092);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b760() {
   return (neuron0x91043d0()*0.729502);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b788() {
   return (neuron0x91044f8()*-0.087234);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b7b0() {
   return (neuron0x91046b0()*0.599045);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b7d8() {
   return (neuron0x91048b0()*-0.537696);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b800() {
   return (neuron0x9104ab0()*-1.37279);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b828() {
   return (neuron0x9104cc8()*0.034429);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b850() {
   return (neuron0x9104ee0()*0.286308);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b878() {
   return (neuron0x91050f8()*0.353861);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911b8a0() {
   return (neuron0x91052f8()*0.0867859);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bae0() {
   return (neuron0x90f5bc0()*-0.385755);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bb08() {
   return (neuron0x90f5dc0()*0.173698);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bb30() {
   return (neuron0x91043d0()*3.14505);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bb58() {
   return (neuron0x91044f8()*0.211904);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bb80() {
   return (neuron0x91046b0()*0.729573);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bba8() {
   return (neuron0x91048b0()*0.220577);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bbd0() {
   return (neuron0x9104ab0()*-0.273244);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bbf8() {
   return (neuron0x9104cc8()*0.182318);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bc20() {
   return (neuron0x9104ee0()*-0.813962);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bc48() {
   return (neuron0x91050f8()*0.0792025);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bc70() {
   return (neuron0x91052f8()*0.198192);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911beb0() {
   return (neuron0x90f5bc0()*0.238091);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bed8() {
   return (neuron0x90f5dc0()*-0.00657732);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bf00() {
   return (neuron0x91043d0()*0.343953);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bf28() {
   return (neuron0x91044f8()*0.0437287);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bf50() {
   return (neuron0x91046b0()*0.0509703);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bf78() {
   return (neuron0x91048b0()*0.576468);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bfa0() {
   return (neuron0x9104ab0()*-1.18217);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bfc8() {
   return (neuron0x9104cc8()*-0.0109143);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911bff0() {
   return (neuron0x9104ee0()*0.0396829);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c018() {
   return (neuron0x91050f8()*0.10571);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c040() {
   return (neuron0x91052f8()*-0.23877);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c280() {
   return (neuron0x90f5bc0()*0.0912596);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c2a8() {
   return (neuron0x90f5dc0()*0.0803457);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c2d0() {
   return (neuron0x91043d0()*-0.100725);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c2f8() {
   return (neuron0x91044f8()*0.731879);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c320() {
   return (neuron0x91046b0()*-0.4028);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c348() {
   return (neuron0x91048b0()*0.03425);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c370() {
   return (neuron0x9104ab0()*-0.370717);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c398() {
   return (neuron0x9104cc8()*0.295706);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c3c0() {
   return (neuron0x9104ee0()*1.20924);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c3e8() {
   return (neuron0x91050f8()*-0.600431);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c410() {
   return (neuron0x91052f8()*0.471511);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c650() {
   return (neuron0x90f5bc0()*0.253431);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c678() {
   return (neuron0x90f5dc0()*0.616188);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c6a0() {
   return (neuron0x91043d0()*-0.254838);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c6c8() {
   return (neuron0x91044f8()*0.178848);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c6f0() {
   return (neuron0x91046b0()*0.0827022);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c718() {
   return (neuron0x91048b0()*-0.222829);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c740() {
   return (neuron0x9104ab0()*-0.310869);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c768() {
   return (neuron0x9104cc8()*-0.0512279);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c790() {
   return (neuron0x9104ee0()*0.600825);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c7b8() {
   return (neuron0x91050f8()*0.396398);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911c7e0() {
   return (neuron0x91052f8()*-0.18429);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ca20() {
   return (neuron0x90f5bc0()*0.304554);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ca48() {
   return (neuron0x90f5dc0()*-0.145793);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ca70() {
   return (neuron0x91043d0()*-0.683977);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ca98() {
   return (neuron0x91044f8()*0.328324);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cac0() {
   return (neuron0x91046b0()*0.814892);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cae8() {
   return (neuron0x91048b0()*-0.144703);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cb10() {
   return (neuron0x9104ab0()*0.837491);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cb38() {
   return (neuron0x9104cc8()*0.205787);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cb60() {
   return (neuron0x9104ee0()*0.405971);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cb88() {
   return (neuron0x91050f8()*0.219122);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cbb0() {
   return (neuron0x91052f8()*0.0647498);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cdf0() {
   return (neuron0x90f5bc0()*-0.309084);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ce18() {
   return (neuron0x90f5dc0()*-0.280421);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ce40() {
   return (neuron0x91043d0()*0.0735849);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ce68() {
   return (neuron0x91044f8()*0.426378);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ce90() {
   return (neuron0x91046b0()*0.193293);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911ceb8() {
   return (neuron0x91048b0()*0.28526);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cee0() {
   return (neuron0x9104ab0()*-0.301627);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cf08() {
   return (neuron0x9104cc8()*-0.16406);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cf30() {
   return (neuron0x9104ee0()*0.182689);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cf58() {
   return (neuron0x91050f8()*0.39157);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911cf80() {
   return (neuron0x91052f8()*0.416573);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d1c0() {
   return (neuron0x90f5bc0()*-0.0193466);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d1e8() {
   return (neuron0x90f5dc0()*0.390184);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d210() {
   return (neuron0x91043d0()*0.16049);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d238() {
   return (neuron0x91044f8()*-0.220635);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d260() {
   return (neuron0x91046b0()*-0.0232587);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d288() {
   return (neuron0x91048b0()*-0.177653);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d2b0() {
   return (neuron0x9104ab0()*-0.305579);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d2d8() {
   return (neuron0x9104cc8()*-0.0527577);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d300() {
   return (neuron0x9104ee0()*0.141963);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d328() {
   return (neuron0x91050f8()*-0.159304);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d350() {
   return (neuron0x91052f8()*-0.146753);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d590() {
   return (neuron0x90f5bc0()*-0.0977693);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d5b8() {
   return (neuron0x90f5dc0()*0.113423);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d5e0() {
   return (neuron0x91043d0()*1.18426);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d608() {
   return (neuron0x91044f8()*0.471092);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d630() {
   return (neuron0x91046b0()*0.186324);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d658() {
   return (neuron0x91048b0()*0.147258);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d680() {
   return (neuron0x9104ab0()*0.748372);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d6a8() {
   return (neuron0x9104cc8()*0.183673);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d6d0() {
   return (neuron0x9104ee0()*0.795978);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d6f8() {
   return (neuron0x91050f8()*0.224211);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d720() {
   return (neuron0x91052f8()*-0.039667);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d960() {
   return (neuron0x90f5bc0()*-0.0750719);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d988() {
   return (neuron0x90f5dc0()*0.160673);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d9b0() {
   return (neuron0x91043d0()*1.09745);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911d9d8() {
   return (neuron0x91044f8()*-0.206578);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911da00() {
   return (neuron0x91046b0()*-0.626392);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911da28() {
   return (neuron0x91048b0()*-0.0473029);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911da50() {
   return (neuron0x9104ab0()*-3.32061);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911da78() {
   return (neuron0x9104cc8()*-0.401622);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911daa0() {
   return (neuron0x9104ee0()*-1.03279);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911dac8() {
   return (neuron0x91050f8()*0.0338889);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911daf0() {
   return (neuron0x91052f8()*-0.247067);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911dd30() {
   return (neuron0x90f5bc0()*-0.179264);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911dd58() {
   return (neuron0x90f5dc0()*0.534769);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111710() {
   return (neuron0x91043d0()*0.116131);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111738() {
   return (neuron0x91044f8()*0.131278);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111760() {
   return (neuron0x91046b0()*0.131425);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111788() {
   return (neuron0x91048b0()*0.363455);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91117b0() {
   return (neuron0x9104ab0()*-0.276376);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91117d8() {
   return (neuron0x9104cc8()*-0.395525);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111800() {
   return (neuron0x9104ee0()*0.199568);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111828() {
   return (neuron0x91050f8()*0.403445);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111850() {
   return (neuron0x91052f8()*0.0539389);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111a90() {
   return (neuron0x90f5bc0()*-0.266271);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111ab8() {
   return (neuron0x90f5dc0()*0.292641);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111ae0() {
   return (neuron0x91043d0()*0.0554642);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111b08() {
   return (neuron0x91044f8()*0.420009);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111b30() {
   return (neuron0x91046b0()*0.0222803);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111b58() {
   return (neuron0x91048b0()*-0.206536);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111b80() {
   return (neuron0x9104ab0()*-0.000429103);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111ba8() {
   return (neuron0x9104cc8()*0.443438);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111bd0() {
   return (neuron0x9104ee0()*0.628947);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111bf8() {
   return (neuron0x91050f8()*0.0228222);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111c20() {
   return (neuron0x91052f8()*-0.10149);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111e60() {
   return (neuron0x90f5bc0()*0.405809);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111e88() {
   return (neuron0x90f5dc0()*0.113998);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111eb0() {
   return (neuron0x91043d0()*0.949189);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111ed8() {
   return (neuron0x91044f8()*0.373169);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111f00() {
   return (neuron0x91046b0()*0.16514);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111f28() {
   return (neuron0x91048b0()*-0.711025);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111f50() {
   return (neuron0x9104ab0()*-0.190501);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111f78() {
   return (neuron0x9104cc8()*-0.477292);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111fa0() {
   return (neuron0x9104ee0()*0.635112);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111fc8() {
   return (neuron0x91050f8()*-0.285035);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9111ff0() {
   return (neuron0x91052f8()*-0.102593);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112230() {
   return (neuron0x90f5bc0()*0.122788);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112258() {
   return (neuron0x90f5dc0()*-0.486924);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112280() {
   return (neuron0x91043d0()*0.79691);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91122a8() {
   return (neuron0x91044f8()*0.176519);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91122d0() {
   return (neuron0x91046b0()*0.230725);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91122f8() {
   return (neuron0x91048b0()*-0.74339);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112320() {
   return (neuron0x9104ab0()*-0.771507);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112348() {
   return (neuron0x9104cc8()*0.65177);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112370() {
   return (neuron0x9104ee0()*-0.199365);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112398() {
   return (neuron0x91050f8()*-0.456052);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91123c0() {
   return (neuron0x91052f8()*-0.106959);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112600() {
   return (neuron0x90f5bc0()*-0.00533031);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112628() {
   return (neuron0x90f5dc0()*0.123195);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112650() {
   return (neuron0x91043d0()*0.171911);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9112678() {
   return (neuron0x91044f8()*0.308692);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91126a0() {
   return (neuron0x91046b0()*-0.272769);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91126c8() {
   return (neuron0x91048b0()*0.719615);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91126f0() {
   return (neuron0x9104ab0()*-0.556502);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911fd88() {
   return (neuron0x9104cc8()*-0.00505036);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911fdb0() {
   return (neuron0x9104ee0()*0.552029);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911fdd8() {
   return (neuron0x91050f8()*0.529763);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x911fe00() {
   return (neuron0x91052f8()*0.0549845);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120040() {
   return (neuron0x90f5bc0()*-0.307618);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120068() {
   return (neuron0x90f5dc0()*0.0992593);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120090() {
   return (neuron0x91043d0()*0.0853516);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91200b8() {
   return (neuron0x91044f8()*-0.275062);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91200e0() {
   return (neuron0x91046b0()*0.08872);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120108() {
   return (neuron0x91048b0()*0.430305);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120130() {
   return (neuron0x9104ab0()*-0.0885639);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120158() {
   return (neuron0x9104cc8()*0.179818);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120180() {
   return (neuron0x9104ee0()*0.14857);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91201a8() {
   return (neuron0x91050f8()*0.270047);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91201d0() {
   return (neuron0x91052f8()*-0.364055);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120410() {
   return (neuron0x90f5bc0()*0.198139);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120438() {
   return (neuron0x90f5dc0()*0.169981);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120460() {
   return (neuron0x91043d0()*-0.453077);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120488() {
   return (neuron0x91044f8()*-0.164538);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91204b0() {
   return (neuron0x91046b0()*-0.42965);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91204d8() {
   return (neuron0x91048b0()*-0.270396);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120500() {
   return (neuron0x9104ab0()*0.24385);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120528() {
   return (neuron0x9104cc8()*0.0801921);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120550() {
   return (neuron0x9104ee0()*-0.473548);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120578() {
   return (neuron0x91050f8()*0.225751);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91205a0() {
   return (neuron0x91052f8()*-0.298171);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91206f0() {
   return (neuron0x9105638()*0.62614);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120718() {
   return (neuron0x9105980()*0.747977);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120740() {
   return (neuron0x9105dc0()*0.40195);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120768() {
   return (neuron0x9106238()*0.0922419);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120790() {
   return (neuron0x9106580()*-0.674932);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91207b8() {
   return (neuron0x9106938()*0.242377);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91207e0() {
   return (neuron0x9106f58()*-0.36683);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120808() {
   return (neuron0x9107120()*-0.0290793);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120830() {
   return (neuron0x91074d8()*0.776878);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120858() {
   return (neuron0x9107890()*-0.187559);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120880() {
   return (neuron0x9107c48()*-0.607908);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91208a8() {
   return (neuron0x9108000()*0.588331);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91208d0() {
   return (neuron0x9106dc8()*-1.04598);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91208f8() {
   return (neuron0x9108990()*0.539795);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120920() {
   return (neuron0x9108d48()*-0.896008);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120948() {
   return (neuron0x9109100()*-0.375526);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91209f8() {
   return (neuron0x9109a90()*-0.947739);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120a20() {
   return (neuron0x9109d70()*-0.779788);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120a48() {
   return (neuron0x910a050()*-0.115458);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120a70() {
   return (neuron0x910a330()*0.129833);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120a98() {
   return (neuron0x910a658()*0.352506);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120ac0() {
   return (neuron0x910aa10()*0.201119);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120ae8() {
   return (neuron0x910ae70()*-0.00551974);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120b10() {
   return (neuron0x910b240()*-0.446831);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120b38() {
   return (neuron0x9108458()*0.444242);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120b60() {
   return (neuron0x910bdf0()*0.703639);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120b88() {
   return (neuron0x910c1c0()*-0.261708);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120bb0() {
   return (neuron0x910c590()*0.856102);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120bd8() {
   return (neuron0x910c960()*-0.74636);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120c00() {
   return (neuron0x910cd30()*0.80119);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120c28() {
   return (neuron0x910d100()*-0.341203);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120c50() {
   return (neuron0x910d4d0()*0.0422651);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120970() {
   return (neuron0x910e268()*0.580458);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120998() {
   return (neuron0x910e390()*0.492024);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91209c0() {
   return (neuron0x910e668()*-0.407149);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120d80() {
   return (neuron0x910ea20()*-0.258184);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120da8() {
   return (neuron0x910edd8()*0.0940615);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120dd0() {
   return (neuron0x910f190()*-1.09851);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120df8() {
   return (neuron0x910f560()*-0.783839);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120e20() {
   return (neuron0x910f930()*-0.56248);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120e48() {
   return (neuron0x910fd00()*0.63665);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120e70() {
   return (neuron0x91100d0()*-0.0256002);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120e98() {
   return (neuron0x91104a0()*0.694411);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120ec0() {
   return (neuron0x9110870()*-1.11953);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120ee8() {
   return (neuron0x9110c40()*-0.141664);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120f10() {
   return (neuron0x9111010()*-0.767516);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120f38() {
   return (neuron0x91113e0()*0.577169);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120f60() {
   return (neuron0x910b598()*-0.0820726);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120f88() {
   return (neuron0x910b968()*-0.350927);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120fb0() {
   return (neuron0x9112768()*-0.642502);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120fd8() {
   return (neuron0x9112b20()*-0.280928);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121000() {
   return (neuron0x9112ef0()*-0.903121);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121028() {
   return (neuron0x91132c0()*-0.166936);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121050() {
   return (neuron0x9113690()*-0.539641);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121078() {
   return (neuron0x9113b78()*0.730192);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91210a0() {
   return (neuron0x9113f30()*0.928015);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91210c8() {
   return (neuron0x9114300()*-1.74556);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91210f0() {
   return (neuron0x91146d0()*-0.359384);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121118() {
   return (neuron0x9114aa0()*-0.136067);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121140() {
   return (neuron0x9114e70()*0.212112);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121168() {
   return (neuron0x9115240()*-0.328242);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121190() {
   return (neuron0x9115610()*0.951781);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91211b8() {
   return (neuron0x91159e0()*-0.788649);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91211e0() {
   return (neuron0x9115db0()*0.239451);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9105550() {
   return (neuron0x910e0c8()*-0.57927);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120c78() {
   return (neuron0x9117070()*-0.121563);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120ca0() {
   return (neuron0x9117428()*-0.637933);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120cc8() {
   return (neuron0x91177f8()*-0.255761);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120cf0() {
   return (neuron0x9117bc8()*1.39474);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120d18() {
   return (neuron0x9117f98()*-0.552017);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9120d40() {
   return (neuron0x9118368()*0.930231);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121410() {
   return (neuron0x9118738()*-0.129435);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121438() {
   return (neuron0x9118b08()*-0.0298024);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121460() {
   return (neuron0x9118ed8()*0.721219);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121488() {
   return (neuron0x91192a8()*0.960194);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91214b0() {
   return (neuron0x9119678()*0.299953);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91214d8() {
   return (neuron0x9119a48()*-0.356338);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121500() {
   return (neuron0x9119e18()*-0.401407);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121528() {
   return (neuron0x911a1e8()*0.256948);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121550() {
   return (neuron0x911a5b8()*0.63362);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121578() {
   return (neuron0x911a988()*1.09143);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91215a0() {
   return (neuron0x911ad58()*-0.488567);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91215c8() {
   return (neuron0x911b128()*0.00375778);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91215f0() {
   return (neuron0x911b4f8()*1.15951);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121618() {
   return (neuron0x911b8c8()*-0.823151);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121640() {
   return (neuron0x911bc98()*0.602492);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121668() {
   return (neuron0x911c068()*0.806089);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121690() {
   return (neuron0x911c438()*0.714909);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91216b8() {
   return (neuron0x911c808()*1.0789);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91216e0() {
   return (neuron0x911cbd8()*-0.207965);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121708() {
   return (neuron0x911cfa8()*0.101213);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121730() {
   return (neuron0x911d378()*0.902557);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121758() {
   return (neuron0x911d748()*-0.996968);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121780() {
   return (neuron0x911db18()*0.330437);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91217a8() {
   return (neuron0x9111878()*0.388616);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91217d0() {
   return (neuron0x9111c48()*-0.833538);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x91217f8() {
   return (neuron0x9112018()*-0.432086);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121820() {
   return (neuron0x91123e8()*0.753547);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121848() {
   return (neuron0x911fe28()*-0.208393);
}

double NNPerpElectronXYCorrectionClusterDeltaY::synapse0x9121870() {
   return (neuron0x91201f8()*-0.0957188);
}

