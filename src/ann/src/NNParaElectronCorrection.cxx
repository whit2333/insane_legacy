#include "NNParaElectronCorrection.h"
#include <cmath>

double NNParaElectronCorrection::Value(int index, double in0, double in1, double in2, double in3, double in4, double in5, double in6)
{
   input0 = (in0 - 1347.4) / 904.749;
   input1 = (in1 - -8.30504) / 31.9453;
   input2 = (in2 - -5.53096) / 60.9397;
   input3 = (in3 - 1.66976) / 0.639722;
   input4 = (in4 - 1.69541) / 0.498174;
   input5 = (in5 - -1.047) / 320.123;
   input6 = (in6 - 48.6369) / 16711.1;
   switch (index) {
      case 0:
         return neuron0x8b83570();
      case 1:
         return neuron0x8b87e78();
      case 2:
         return neuron0x8b883f0();
      default:
         return 0.;
   }
}

double NNParaElectronCorrection::Value(int index, double* input)
{
   input0 = (input[0] - 1347.4) / 904.749;
   input1 = (input[1] - -8.30504) / 31.9453;
   input2 = (input[2] - -5.53096) / 60.9397;
   input3 = (input[3] - 1.66976) / 0.639722;
   input4 = (input[4] - 1.69541) / 0.498174;
   input5 = (input[5] - -1.047) / 320.123;
   input6 = (input[6] - 48.6369) / 16711.1;
   switch (index) {
      case 0:
         return neuron0x8b83570();
      case 1:
         return neuron0x8b87e78();
      case 2:
         return neuron0x8b883f0();
      default:
         return 0.;
   }
}

double NNParaElectronCorrection::neuron0x8b827d0()
{
   return input0;
}

double NNParaElectronCorrection::neuron0x8b82940()
{
   return input1;
}

double NNParaElectronCorrection::neuron0x8b82b40()
{
   return input2;
}

double NNParaElectronCorrection::neuron0x8b82d40()
{
   return input3;
}

double NNParaElectronCorrection::neuron0x8b82f40()
{
   return input4;
}

double NNParaElectronCorrection::neuron0x8b83140()
{
   return input5;
}

double NNParaElectronCorrection::neuron0x8b83358()
{
   return input6;
}

double NNParaElectronCorrection::input0x8b83698()
{
   double input = -85.5994;
   input += synapse0x8b83850();
   input += synapse0x8b83878();
   input += synapse0x8b838a0();
   input += synapse0x8b838c8();
   input += synapse0x8b838f0();
   input += synapse0x8b83918();
   input += synapse0x8b83940();
   return input;
}

double NNParaElectronCorrection::neuron0x8b83698()
{
   double input = input0x8b83698();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b83968()
{
   double input = -49.1302;
   input += synapse0x8b83b68();
   input += synapse0x8b83b90();
   input += synapse0x8b83bb8();
   input += synapse0x8b83be0();
   input += synapse0x8b83c08();
   input += synapse0x8b83c30();
   input += synapse0x8b83c58();
   return input;
}

double NNParaElectronCorrection::neuron0x8b83968()
{
   double input = input0x8b83968();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b83c80()
{
   double input = -206.074;
   input += synapse0x8b83e80();
   input += synapse0x8b83ea8();
   input += synapse0x8b83ed0();
   input += synapse0x8b83f80();
   input += synapse0x8b83fa8();
   input += synapse0x8b83fd0();
   input += synapse0x8b83ff8();
   return input;
}

double NNParaElectronCorrection::neuron0x8b83c80()
{
   double input = input0x8b83c80();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b84020()
{
   double input = -35.6699;
   input += synapse0x8b841d8();
   input += synapse0x8b84200();
   input += synapse0x8b84228();
   input += synapse0x8b84250();
   input += synapse0x8b84278();
   input += synapse0x8b842a0();
   input += synapse0x8b842c8();
   return input;
}

double NNParaElectronCorrection::neuron0x8b84020()
{
   double input = input0x8b84020();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b842f0()
{
   double input = -29.7881;
   input += synapse0x8b844f0();
   input += synapse0x8b84518();
   input += synapse0x8b84540();
   input += synapse0x8b84568();
   input += synapse0x8b84590();
   input += synapse0x8b83ef8();
   input += synapse0x8b83f20();
   return input;
}

double NNParaElectronCorrection::neuron0x8b842f0()
{
   double input = input0x8b842f0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b846c0()
{
   double input = -54.7442;
   input += synapse0x8b848c0();
   input += synapse0x8b848e8();
   input += synapse0x8b84910();
   input += synapse0x8b84938();
   input += synapse0x8b84960();
   input += synapse0x8b84988();
   input += synapse0x8b849b0();
   return input;
}

double NNParaElectronCorrection::neuron0x8b846c0()
{
   double input = input0x8b846c0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b849d8()
{
   double input = -117.775;
   input += synapse0x8b84bf0();
   input += synapse0x8b84c18();
   input += synapse0x8b84c40();
   input += synapse0x8b84c68();
   input += synapse0x8b84c90();
   input += synapse0x8b84cb8();
   input += synapse0x8b84ce0();
   return input;
}

double NNParaElectronCorrection::neuron0x8b849d8()
{
   double input = input0x8b849d8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b84d08()
{
   double input = -67.8566;
   input += synapse0x8b84f20();
   input += synapse0x8b84f48();
   input += synapse0x8b84f70();
   input += synapse0x8b84f98();
   input += synapse0x8b84fc0();
   input += synapse0x8b84fe8();
   input += synapse0x8b85010();
   return input;
}

double NNParaElectronCorrection::neuron0x8b84d08()
{
   double input = input0x8b84d08();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b85038()
{
   double input = -176.176;
   input += synapse0x8b85250();
   input += synapse0x8b85278();
   input += synapse0x8b852a0();
   input += synapse0x8b852c8();
   input += synapse0x8b852f0();
   input += synapse0x8b85318();
   input += synapse0x8b85340();
   return input;
}

double NNParaElectronCorrection::neuron0x8b85038()
{
   double input = input0x8b85038();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b85368()
{
   double input = -32.2295;
   input += synapse0x8b85608();
   input += synapse0x8b85630();
   input += synapse0x8b62460();
   input += synapse0x8b83f48();
   input += synapse0x8aff270();
   input += synapse0x8b63910();
   input += synapse0x8afec48();
   return input;
}

double NNParaElectronCorrection::neuron0x8b85368()
{
   double input = input0x8b85368();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b85860()
{
   double input = -38.07;
   input += synapse0x8b85a60();
   input += synapse0x8b85a88();
   input += synapse0x8b85ab0();
   input += synapse0x8b85ad8();
   input += synapse0x8b85b00();
   input += synapse0x8b85b28();
   input += synapse0x8b85b50();
   return input;
}

double NNParaElectronCorrection::neuron0x8b85860()
{
   double input = input0x8b85860();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b85b78()
{
   double input = -93.2474;
   input += synapse0x8b85d90();
   input += synapse0x8b85db8();
   input += synapse0x8b85de0();
   input += synapse0x8b85e08();
   input += synapse0x8b85e30();
   input += synapse0x8b85e58();
   input += synapse0x8b85e80();
   return input;
}

double NNParaElectronCorrection::neuron0x8b85b78()
{
   double input = input0x8b85b78();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b85ea8()
{
   double input = -73.0272;
   input += synapse0x8b860c0();
   input += synapse0x8b860e8();
   input += synapse0x8b86110();
   input += synapse0x8b86138();
   input += synapse0x8b86160();
   input += synapse0x8b86188();
   input += synapse0x8b861b0();
   return input;
}

double NNParaElectronCorrection::neuron0x8b85ea8()
{
   double input = input0x8b85ea8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b861d8()
{
   double input = -9.08834;
   input += synapse0x8b863f0();
   input += synapse0x8b86418();
   input += synapse0x8b86440();
   input += synapse0x8b86468();
   input += synapse0x8b86490();
   input += synapse0x8b864b8();
   input += synapse0x8b864e0();
   return input;
}

double NNParaElectronCorrection::neuron0x8b861d8()
{
   double input = input0x8b861d8();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b86508()
{
   double input = -139.804;
   input += synapse0x8b86720();
   input += synapse0x8b86748();
   input += synapse0x8b86770();
   input += synapse0x8b86798();
   input += synapse0x8b867c0();
   input += synapse0x8b867e8();
   input += synapse0x8b86810();
   return input;
}

double NNParaElectronCorrection::neuron0x8b86508()
{
   double input = input0x8b86508();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b86838()
{
   double input = -59.4151;
   input += synapse0x8b86a50();
   input += synapse0x8b86b00();
   input += synapse0x8b86bb0();
   input += synapse0x8b86c60();
   input += synapse0x8b86d10();
   input += synapse0x8b86dc0();
   input += synapse0x8b86e70();
   return input;
}

double NNParaElectronCorrection::neuron0x8b86838()
{
   double input = input0x8b86838();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b86f20()
{
   double input = -7.97303;
   input += synapse0x8b87060();
   input += synapse0x8b87088();
   input += synapse0x8b870b0();
   input += synapse0x8b870d8();
   input += synapse0x8b87100();
   input += synapse0x8b87128();
   input += synapse0x8b87150();
   return input;
}

double NNParaElectronCorrection::neuron0x8b86f20()
{
   double input = input0x8b86f20();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b87178()
{
   double input = -79.6942;
   input += synapse0x8b872b8();
   input += synapse0x8b872e0();
   input += synapse0x8b87308();
   input += synapse0x8b87330();
   input += synapse0x8b87358();
   input += synapse0x8b87380();
   input += synapse0x8b873a8();
   return input;
}

double NNParaElectronCorrection::neuron0x8b87178()
{
   double input = input0x8b87178();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b873d0()
{
   double input = -92.455;
   input += synapse0x8b875a0();
   input += synapse0x8b875c8();
   input += synapse0x8b875f0();
   input += synapse0x8aff340();
   input += synapse0x8aff368();
   input += synapse0x8b895f0();
   input += synapse0x8b89618();
   return input;
}

double NNParaElectronCorrection::neuron0x8b873d0()
{
   double input = input0x8b873d0();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b85658()
{
   double input = -52.833;
   input += synapse0x8b89688();
   input += synapse0x8b85828();
   input += synapse0x8b896b0();
   input += synapse0x8b845b8();
   input += synapse0x8b845e0();
   input += synapse0x8b84608();
   input += synapse0x8b84630();
   return input;
}

double NNParaElectronCorrection::neuron0x8b85658()
{
   double input = input0x8b85658();
   return ((input < -709. ? 0. : (1 / (1 + exp(-input)))) * 1) + 0;
}

double NNParaElectronCorrection::input0x8b83570()
{
   double input = 65.2728;
   input += synapse0x8b84698();
   input += synapse0x8b87af8();
   input += synapse0x8b87b20();
   input += synapse0x8b87b48();
   input += synapse0x8b87b70();
   input += synapse0x8b87b98();
   input += synapse0x8b87bc0();
   input += synapse0x8b87be8();
   input += synapse0x8b87c10();
   input += synapse0x8b87c38();
   input += synapse0x8b87c60();
   input += synapse0x8b87c88();
   input += synapse0x8b87cb0();
   input += synapse0x8b87cd8();
   input += synapse0x8b87d00();
   input += synapse0x8b87d28();
   input += synapse0x8b87dd8();
   input += synapse0x8b87e00();
   input += synapse0x8b87e28();
   input += synapse0x8b87e50();
   return input;
}

double NNParaElectronCorrection::neuron0x8b83570()
{
   double input = input0x8b83570();
   return (input * 1) + 0;
}

double NNParaElectronCorrection::input0x8b87e78()
{
   double input = -0.0161792;
   input += synapse0x8b88048();
   input += synapse0x8b88070();
   input += synapse0x8b88098();
   input += synapse0x8b880c0();
   input += synapse0x8b880e8();
   input += synapse0x8b88110();
   input += synapse0x8b88138();
   input += synapse0x8b88160();
   input += synapse0x8b88188();
   input += synapse0x8b881b0();
   input += synapse0x8b881d8();
   input += synapse0x8b88200();
   input += synapse0x8b88228();
   input += synapse0x8b88250();
   input += synapse0x8b88278();
   input += synapse0x8b882a0();
   input += synapse0x8b88350();
   input += synapse0x8b88378();
   input += synapse0x8b883a0();
   input += synapse0x8b883c8();
   return input;
}

double NNParaElectronCorrection::neuron0x8b87e78()
{
   double input = input0x8b87e78();
   return (input * 1) + 0;
}

double NNParaElectronCorrection::input0x8b883f0()
{
   double input = -0.00708852;
   input += synapse0x8b84658();
   input += synapse0x8b88560();
   input += synapse0x8b89780();
   input += synapse0x8b897a8();
   input += synapse0x8b897d0();
   input += synapse0x8b897f8();
   input += synapse0x8b89820();
   input += synapse0x8b89848();
   input += synapse0x8b89870();
   input += synapse0x8b89898();
   input += synapse0x8b898c0();
   input += synapse0x8b898e8();
   input += synapse0x8b89910();
   input += synapse0x8b89938();
   input += synapse0x8b89960();
   input += synapse0x8b89988();
   input += synapse0x8b89a38();
   input += synapse0x8b89a60();
   input += synapse0x8b89a88();
   input += synapse0x8b89ab0();
   return input;
}

double NNParaElectronCorrection::neuron0x8b883f0()
{
   double input = input0x8b883f0();
   return (input * 1) + 0;
}

double NNParaElectronCorrection::synapse0x8b83850()
{
   return (neuron0x8b827d0() * 0.24847);
}

double NNParaElectronCorrection::synapse0x8b83878()
{
   return (neuron0x8b82940() * -56.5969);
}

double NNParaElectronCorrection::synapse0x8b838a0()
{
   return (neuron0x8b82b40() * 0.875528);
}

double NNParaElectronCorrection::synapse0x8b838c8()
{
   return (neuron0x8b82d40() * 0.16092);
}

double NNParaElectronCorrection::synapse0x8b838f0()
{
   return (neuron0x8b82f40() * -0.787429);
}

double NNParaElectronCorrection::synapse0x8b83918()
{
   return (neuron0x8b83140() * -8.13934);
}

double NNParaElectronCorrection::synapse0x8b83940()
{
   return (neuron0x8b83358() * -0.224572);
}

double NNParaElectronCorrection::synapse0x8b83b68()
{
   return (neuron0x8b827d0() * 9.31224);
}

double NNParaElectronCorrection::synapse0x8b83b90()
{
   return (neuron0x8b82940() * -3.97424);
}

double NNParaElectronCorrection::synapse0x8b83bb8()
{
   return (neuron0x8b82b40() * -3.9846);
}

double NNParaElectronCorrection::synapse0x8b83be0()
{
   return (neuron0x8b82d40() * -5.22536);
}

double NNParaElectronCorrection::synapse0x8b83c08()
{
   return (neuron0x8b82f40() * 3.00837);
}

double NNParaElectronCorrection::synapse0x8b83c30()
{
   return (neuron0x8b83140() * -0.206433);
}

double NNParaElectronCorrection::synapse0x8b83c58()
{
   return (neuron0x8b83358() * 0.0347117);
}

double NNParaElectronCorrection::synapse0x8b83e80()
{
   return (neuron0x8b827d0() * -86.7421);
}

double NNParaElectronCorrection::synapse0x8b83ea8()
{
   return (neuron0x8b82940() * 15.5599);
}

double NNParaElectronCorrection::synapse0x8b83ed0()
{
   return (neuron0x8b82b40() * -27.4142);
}

double NNParaElectronCorrection::synapse0x8b83f80()
{
   return (neuron0x8b82d40() * 94.3678);
}

double NNParaElectronCorrection::synapse0x8b83fa8()
{
   return (neuron0x8b82f40() * 20.4225);
}

double NNParaElectronCorrection::synapse0x8b83fd0()
{
   return (neuron0x8b83140() * -0.0288459);
}

double NNParaElectronCorrection::synapse0x8b83ff8()
{
   return (neuron0x8b83358() * 1.03498);
}

double NNParaElectronCorrection::synapse0x8b841d8()
{
   return (neuron0x8b827d0() * -26.7089);
}

double NNParaElectronCorrection::synapse0x8b84200()
{
   return (neuron0x8b82940() * 6.31388);
}

double NNParaElectronCorrection::synapse0x8b84228()
{
   return (neuron0x8b82b40() * -3.31695);
}

double NNParaElectronCorrection::synapse0x8b84250()
{
   return (neuron0x8b82d40() * 5.21458);
}

double NNParaElectronCorrection::synapse0x8b84278()
{
   return (neuron0x8b82f40() * 0.0698993);
}

double NNParaElectronCorrection::synapse0x8b842a0()
{
   return (neuron0x8b83140() * 0.362907);
}

double NNParaElectronCorrection::synapse0x8b842c8()
{
   return (neuron0x8b83358() * -0.919472);
}

double NNParaElectronCorrection::synapse0x8b844f0()
{
   return (neuron0x8b827d0() * 2.14578);
}

double NNParaElectronCorrection::synapse0x8b84518()
{
   return (neuron0x8b82940() * 6.19401);
}

double NNParaElectronCorrection::synapse0x8b84540()
{
   return (neuron0x8b82b40() * -11.8982);
}

double NNParaElectronCorrection::synapse0x8b84568()
{
   return (neuron0x8b82d40() * -4.47337);
}

double NNParaElectronCorrection::synapse0x8b84590()
{
   return (neuron0x8b82f40() * -3.88926);
}

double NNParaElectronCorrection::synapse0x8b83ef8()
{
   return (neuron0x8b83140() * 0.0245926);
}

double NNParaElectronCorrection::synapse0x8b83f20()
{
   return (neuron0x8b83358() * 0.259115);
}

double NNParaElectronCorrection::synapse0x8b848c0()
{
   return (neuron0x8b827d0() * 5.90584);
}

double NNParaElectronCorrection::synapse0x8b848e8()
{
   return (neuron0x8b82940() * -1.63148);
}

double NNParaElectronCorrection::synapse0x8b84910()
{
   return (neuron0x8b82b40() * -20.6848);
}

double NNParaElectronCorrection::synapse0x8b84938()
{
   return (neuron0x8b82d40() * -16.1067);
}

double NNParaElectronCorrection::synapse0x8b84960()
{
   return (neuron0x8b82f40() * -6.92651);
}

double NNParaElectronCorrection::synapse0x8b84988()
{
   return (neuron0x8b83140() * 1.93071);
}

double NNParaElectronCorrection::synapse0x8b849b0()
{
   return (neuron0x8b83358() * 3.02456);
}

double NNParaElectronCorrection::synapse0x8b84bf0()
{
   return (neuron0x8b827d0() * 18.1071);
}

double NNParaElectronCorrection::synapse0x8b84c18()
{
   return (neuron0x8b82940() * 20.6448);
}

double NNParaElectronCorrection::synapse0x8b84c40()
{
   return (neuron0x8b82b40() * 13.5689);
}

double NNParaElectronCorrection::synapse0x8b84c68()
{
   return (neuron0x8b82d40() * 10.4654);
}

double NNParaElectronCorrection::synapse0x8b84c90()
{
   return (neuron0x8b82f40() * -9.22378);
}

double NNParaElectronCorrection::synapse0x8b84cb8()
{
   return (neuron0x8b83140() * -0.369833);
}

double NNParaElectronCorrection::synapse0x8b84ce0()
{
   return (neuron0x8b83358() * 0.537155);
}

double NNParaElectronCorrection::synapse0x8b84f20()
{
   return (neuron0x8b827d0() * -29.0891);
}

double NNParaElectronCorrection::synapse0x8b84f48()
{
   return (neuron0x8b82940() * 24.0311);
}

double NNParaElectronCorrection::synapse0x8b84f70()
{
   return (neuron0x8b82b40() * 6.75547);
}

double NNParaElectronCorrection::synapse0x8b84f98()
{
   return (neuron0x8b82d40() * -4.80195);
}

double NNParaElectronCorrection::synapse0x8b84fc0()
{
   return (neuron0x8b82f40() * -5.671);
}

double NNParaElectronCorrection::synapse0x8b84fe8()
{
   return (neuron0x8b83140() * -6.78549);
}

double NNParaElectronCorrection::synapse0x8b85010()
{
   return (neuron0x8b83358() * -6.51479);
}

double NNParaElectronCorrection::synapse0x8b85250()
{
   return (neuron0x8b827d0() * -27.8077);
}

double NNParaElectronCorrection::synapse0x8b85278()
{
   return (neuron0x8b82940() * -4.12753);
}

double NNParaElectronCorrection::synapse0x8b852a0()
{
   return (neuron0x8b82b40() * -24.3219);
}

double NNParaElectronCorrection::synapse0x8b852c8()
{
   return (neuron0x8b82d40() * 91.0617);
}

double NNParaElectronCorrection::synapse0x8b852f0()
{
   return (neuron0x8b82f40() * -12.4125);
}

double NNParaElectronCorrection::synapse0x8b85318()
{
   return (neuron0x8b83140() * -0.451352);
}

double NNParaElectronCorrection::synapse0x8b85340()
{
   return (neuron0x8b83358() * 1.21509);
}

double NNParaElectronCorrection::synapse0x8b85608()
{
   return (neuron0x8b827d0() * 5.17901);
}

double NNParaElectronCorrection::synapse0x8b85630()
{
   return (neuron0x8b82940() * -0.0610185);
}

double NNParaElectronCorrection::synapse0x8b62460()
{
   return (neuron0x8b82b40() * -5.15279);
}

double NNParaElectronCorrection::synapse0x8b83f48()
{
   return (neuron0x8b82d40() * 4.82906);
}

double NNParaElectronCorrection::synapse0x8aff270()
{
   return (neuron0x8b82f40() * 1.03334);
}

double NNParaElectronCorrection::synapse0x8b63910()
{
   return (neuron0x8b83140() * 0.773201);
}

double NNParaElectronCorrection::synapse0x8afec48()
{
   return (neuron0x8b83358() * -0.0453519);
}

double NNParaElectronCorrection::synapse0x8b85a60()
{
   return (neuron0x8b827d0() * 5.84425);
}

double NNParaElectronCorrection::synapse0x8b85a88()
{
   return (neuron0x8b82940() * -9.72978);
}

double NNParaElectronCorrection::synapse0x8b85ab0()
{
   return (neuron0x8b82b40() * -0.0417747);
}

double NNParaElectronCorrection::synapse0x8b85ad8()
{
   return (neuron0x8b82d40() * 3.9223);
}

double NNParaElectronCorrection::synapse0x8b85b00()
{
   return (neuron0x8b82f40() * -4.56555);
}

double NNParaElectronCorrection::synapse0x8b85b28()
{
   return (neuron0x8b83140() * 0.123957);
}

double NNParaElectronCorrection::synapse0x8b85b50()
{
   return (neuron0x8b83358() * 0.512018);
}

double NNParaElectronCorrection::synapse0x8b85d90()
{
   return (neuron0x8b827d0() * -57.666);
}

double NNParaElectronCorrection::synapse0x8b85db8()
{
   return (neuron0x8b82940() * 5.88465);
}

double NNParaElectronCorrection::synapse0x8b85de0()
{
   return (neuron0x8b82b40() * -12.3486);
}

double NNParaElectronCorrection::synapse0x8b85e08()
{
   return (neuron0x8b82d40() * 20.921);
}

double NNParaElectronCorrection::synapse0x8b85e30()
{
   return (neuron0x8b82f40() * -10.3673);
}

double NNParaElectronCorrection::synapse0x8b85e58()
{
   return (neuron0x8b83140() * -0.386748);
}

double NNParaElectronCorrection::synapse0x8b85e80()
{
   return (neuron0x8b83358() * 1.29024);
}

double NNParaElectronCorrection::synapse0x8b860c0()
{
   return (neuron0x8b827d0() * -59.7221);
}

double NNParaElectronCorrection::synapse0x8b860e8()
{
   return (neuron0x8b82940() * 3.19038);
}

double NNParaElectronCorrection::synapse0x8b86110()
{
   return (neuron0x8b82b40() * 1.56289);
}

double NNParaElectronCorrection::synapse0x8b86138()
{
   return (neuron0x8b82d40() * -4.41376);
}

double NNParaElectronCorrection::synapse0x8b86160()
{
   return (neuron0x8b82f40() * 2.24365);
}

double NNParaElectronCorrection::synapse0x8b86188()
{
   return (neuron0x8b83140() * -0.217164);
}

double NNParaElectronCorrection::synapse0x8b861b0()
{
   return (neuron0x8b83358() * 8.29077);
}

double NNParaElectronCorrection::synapse0x8b863f0()
{
   return (neuron0x8b827d0() * 3.56246);
}

double NNParaElectronCorrection::synapse0x8b86418()
{
   return (neuron0x8b82940() * -0.341348);
}

double NNParaElectronCorrection::synapse0x8b86440()
{
   return (neuron0x8b82b40() * 4.1215);
}

double NNParaElectronCorrection::synapse0x8b86468()
{
   return (neuron0x8b82d40() * 4.21686);
}

double NNParaElectronCorrection::synapse0x8b86490()
{
   return (neuron0x8b82f40() * 0.747484);
}

double NNParaElectronCorrection::synapse0x8b864b8()
{
   return (neuron0x8b83140() * 1.10117);
}

double NNParaElectronCorrection::synapse0x8b864e0()
{
   return (neuron0x8b83358() * -0.443866);
}

double NNParaElectronCorrection::synapse0x8b86720()
{
   return (neuron0x8b827d0() * -30.2641);
}

double NNParaElectronCorrection::synapse0x8b86748()
{
   return (neuron0x8b82940() * 12.8596);
}

double NNParaElectronCorrection::synapse0x8b86770()
{
   return (neuron0x8b82b40() * -38.4877);
}

double NNParaElectronCorrection::synapse0x8b86798()
{
   return (neuron0x8b82d40() * 48.5782);
}

double NNParaElectronCorrection::synapse0x8b867c0()
{
   return (neuron0x8b82f40() * -28.9166);
}

double NNParaElectronCorrection::synapse0x8b867e8()
{
   return (neuron0x8b83140() * -1.68384);
}

double NNParaElectronCorrection::synapse0x8b86810()
{
   return (neuron0x8b83358() * 1.18326);
}

double NNParaElectronCorrection::synapse0x8b86a50()
{
   return (neuron0x8b827d0() * -38.0881);
}

double NNParaElectronCorrection::synapse0x8b86b00()
{
   return (neuron0x8b82940() * 1.98582);
}

double NNParaElectronCorrection::synapse0x8b86bb0()
{
   return (neuron0x8b82b40() * -11.6497);
}

double NNParaElectronCorrection::synapse0x8b86c60()
{
   return (neuron0x8b82d40() * 9.50919);
}

double NNParaElectronCorrection::synapse0x8b86d10()
{
   return (neuron0x8b82f40() * -9.3806);
}

double NNParaElectronCorrection::synapse0x8b86dc0()
{
   return (neuron0x8b83140() * -0.0326879);
}

double NNParaElectronCorrection::synapse0x8b86e70()
{
   return (neuron0x8b83358() * 4.88406);
}

double NNParaElectronCorrection::synapse0x8b87060()
{
   return (neuron0x8b827d0() * 1.15694);
}

double NNParaElectronCorrection::synapse0x8b87088()
{
   return (neuron0x8b82940() * 0.514028);
}

double NNParaElectronCorrection::synapse0x8b870b0()
{
   return (neuron0x8b82b40() * 20.401);
}

double NNParaElectronCorrection::synapse0x8b870d8()
{
   return (neuron0x8b82d40() * -0.0918411);
}

double NNParaElectronCorrection::synapse0x8b87100()
{
   return (neuron0x8b82f40() * 2.04611);
}

double NNParaElectronCorrection::synapse0x8b87128()
{
   return (neuron0x8b83140() * 13.0027);
}

double NNParaElectronCorrection::synapse0x8b87150()
{
   return (neuron0x8b83358() * 1.9838);
}

double NNParaElectronCorrection::synapse0x8b872b8()
{
   return (neuron0x8b827d0() * -22.368);
}

double NNParaElectronCorrection::synapse0x8b872e0()
{
   return (neuron0x8b82940() * -3.40956);
}

double NNParaElectronCorrection::synapse0x8b87308()
{
   return (neuron0x8b82b40() * -11.8092);
}

double NNParaElectronCorrection::synapse0x8b87330()
{
   return (neuron0x8b82d40() * 30.1476);
}

double NNParaElectronCorrection::synapse0x8b87358()
{
   return (neuron0x8b82f40() * -13.0912);
}

double NNParaElectronCorrection::synapse0x8b87380()
{
   return (neuron0x8b83140() * -1.67466);
}

double NNParaElectronCorrection::synapse0x8b873a8()
{
   return (neuron0x8b83358() * 1.03626);
}

double NNParaElectronCorrection::synapse0x8b875a0()
{
   return (neuron0x8b827d0() * 7.41992);
}

double NNParaElectronCorrection::synapse0x8b875c8()
{
   return (neuron0x8b82940() * -3.11756);
}

double NNParaElectronCorrection::synapse0x8b875f0()
{
   return (neuron0x8b82b40() * -24.5535);
}

double NNParaElectronCorrection::synapse0x8aff340()
{
   return (neuron0x8b82d40() * 39.8461);
}

double NNParaElectronCorrection::synapse0x8aff368()
{
   return (neuron0x8b82f40() * -34.3119);
}

double NNParaElectronCorrection::synapse0x8b895f0()
{
   return (neuron0x8b83140() * 0.584721);
}

double NNParaElectronCorrection::synapse0x8b89618()
{
   return (neuron0x8b83358() * 0.446725);
}

double NNParaElectronCorrection::synapse0x8b89688()
{
   return (neuron0x8b827d0() * -44.9485);
}

double NNParaElectronCorrection::synapse0x8b85828()
{
   return (neuron0x8b82940() * 4.07216);
}

double NNParaElectronCorrection::synapse0x8b896b0()
{
   return (neuron0x8b82b40() * -8.0711);
}

double NNParaElectronCorrection::synapse0x8b845b8()
{
   return (neuron0x8b82d40() * 4.58351);
}

double NNParaElectronCorrection::synapse0x8b845e0()
{
   return (neuron0x8b82f40() * -0.314417);
}

double NNParaElectronCorrection::synapse0x8b84608()
{
   return (neuron0x8b83140() * 11.5438);
}

double NNParaElectronCorrection::synapse0x8b84630()
{
   return (neuron0x8b83358() * 6.3057);
}

double NNParaElectronCorrection::synapse0x8b84698()
{
   return (neuron0x8b83698() * 225.629);
}

double NNParaElectronCorrection::synapse0x8b87af8()
{
   return (neuron0x8b83968() * 193.536);
}

double NNParaElectronCorrection::synapse0x8b87b20()
{
   return (neuron0x8b83c80() * 213.162);
}

double NNParaElectronCorrection::synapse0x8b87b48()
{
   return (neuron0x8b84020() * 337.509);
}

double NNParaElectronCorrection::synapse0x8b87b70()
{
   return (neuron0x8b842f0() * 141.933);
}

double NNParaElectronCorrection::synapse0x8b87b98()
{
   return (neuron0x8b846c0() * 290.392);
}

double NNParaElectronCorrection::synapse0x8b87bc0()
{
   return (neuron0x8b849d8() * 172.548);
}

double NNParaElectronCorrection::synapse0x8b87be8()
{
   return (neuron0x8b84d08() * 249.923);
}

double NNParaElectronCorrection::synapse0x8b87c10()
{
   return (neuron0x8b85038() * 156.628);
}

double NNParaElectronCorrection::synapse0x8b87c38()
{
   return (neuron0x8b85368() * 236.595);
}

double NNParaElectronCorrection::synapse0x8b87c60()
{
   return (neuron0x8b85860() * 229.142);
}

double NNParaElectronCorrection::synapse0x8b87c88()
{
   return (neuron0x8b85b78() * 218.529);
}

double NNParaElectronCorrection::synapse0x8b87cb0()
{
   return (neuron0x8b85ea8() * 283.616);
}

double NNParaElectronCorrection::synapse0x8b87cd8()
{
   return (neuron0x8b861d8() * 174.766);
}

double NNParaElectronCorrection::synapse0x8b87d00()
{
   return (neuron0x8b86508() * 219.016);
}

double NNParaElectronCorrection::synapse0x8b87d28()
{
   return (neuron0x8b86838() * 269.564);
}

double NNParaElectronCorrection::synapse0x8b87dd8()
{
   return (neuron0x8b86f20() * 143.582);
}

double NNParaElectronCorrection::synapse0x8b87e00()
{
   return (neuron0x8b87178() * 263.792);
}

double NNParaElectronCorrection::synapse0x8b87e28()
{
   return (neuron0x8b873d0() * 271.833);
}

double NNParaElectronCorrection::synapse0x8b87e50()
{
   return (neuron0x8b85658() * 249.151);
}

double NNParaElectronCorrection::synapse0x8b88048()
{
   return (neuron0x8b83698() * -0.197398);
}

double NNParaElectronCorrection::synapse0x8b88070()
{
   return (neuron0x8b83968() * -0.423019);
}

double NNParaElectronCorrection::synapse0x8b88098()
{
   return (neuron0x8b83c80() * -0.330121);
}

double NNParaElectronCorrection::synapse0x8b880c0()
{
   return (neuron0x8b84020() * 0.147984);
}

double NNParaElectronCorrection::synapse0x8b880e8()
{
   return (neuron0x8b842f0() * -0.297162);
}

double NNParaElectronCorrection::synapse0x8b88110()
{
   return (neuron0x8b846c0() * -0.275702);
}

double NNParaElectronCorrection::synapse0x8b88138()
{
   return (neuron0x8b849d8() * -0.441611);
}

double NNParaElectronCorrection::synapse0x8b88160()
{
   return (neuron0x8b84d08() * -0.277616);
}

double NNParaElectronCorrection::synapse0x8b88188()
{
   return (neuron0x8b85038() * 0.275715);
}

double NNParaElectronCorrection::synapse0x8b881b0()
{
   return (neuron0x8b85368() * -0.89827);
}

double NNParaElectronCorrection::synapse0x8b881d8()
{
   return (neuron0x8b85860() * 0.0627429);
}

double NNParaElectronCorrection::synapse0x8b88200()
{
   return (neuron0x8b85b78() * 0.39812);
}

double NNParaElectronCorrection::synapse0x8b88228()
{
   return (neuron0x8b85ea8() * 0.00377253);
}

double NNParaElectronCorrection::synapse0x8b88250()
{
   return (neuron0x8b861d8() * -0.243454);
}

double NNParaElectronCorrection::synapse0x8b88278()
{
   return (neuron0x8b86508() * -0.0766082);
}

double NNParaElectronCorrection::synapse0x8b882a0()
{
   return (neuron0x8b86838() * -0.390965);
}

double NNParaElectronCorrection::synapse0x8b88350()
{
   return (neuron0x8b86f20() * 0.0794842);
}

double NNParaElectronCorrection::synapse0x8b88378()
{
   return (neuron0x8b87178() * -0.151038);
}

double NNParaElectronCorrection::synapse0x8b883a0()
{
   return (neuron0x8b873d0() * 0.229239);
}

double NNParaElectronCorrection::synapse0x8b883c8()
{
   return (neuron0x8b85658() * 0.0168293);
}

double NNParaElectronCorrection::synapse0x8b84658()
{
   return (neuron0x8b83698() * 0.212123);
}

double NNParaElectronCorrection::synapse0x8b88560()
{
   return (neuron0x8b83968() * 0.208406);
}

double NNParaElectronCorrection::synapse0x8b89780()
{
   return (neuron0x8b83c80() * -0.105474);
}

double NNParaElectronCorrection::synapse0x8b897a8()
{
   return (neuron0x8b84020() * -0.0627803);
}

double NNParaElectronCorrection::synapse0x8b897d0()
{
   return (neuron0x8b842f0() * 0.17008);
}

double NNParaElectronCorrection::synapse0x8b897f8()
{
   return (neuron0x8b846c0() * 0.10023);
}

double NNParaElectronCorrection::synapse0x8b89820()
{
   return (neuron0x8b849d8() * 0.000264875);
}

double NNParaElectronCorrection::synapse0x8b89848()
{
   return (neuron0x8b84d08() * -0.136269);
}

double NNParaElectronCorrection::synapse0x8b89870()
{
   return (neuron0x8b85038() * 0.368665);
}

double NNParaElectronCorrection::synapse0x8b89898()
{
   return (neuron0x8b85368() * -0.0776141);
}

double NNParaElectronCorrection::synapse0x8b898c0()
{
   return (neuron0x8b85860() * 0.418826);
}

double NNParaElectronCorrection::synapse0x8b898e8()
{
   return (neuron0x8b85b78() * 0.181055);
}

double NNParaElectronCorrection::synapse0x8b89910()
{
   return (neuron0x8b85ea8() * 0.243319);
}

double NNParaElectronCorrection::synapse0x8b89938()
{
   return (neuron0x8b861d8() * -0.0400135);
}

double NNParaElectronCorrection::synapse0x8b89960()
{
   return (neuron0x8b86508() * 0.201012);
}

double NNParaElectronCorrection::synapse0x8b89988()
{
   return (neuron0x8b86838() * -0.0506912);
}

double NNParaElectronCorrection::synapse0x8b89a38()
{
   return (neuron0x8b86f20() * -0.0237587);
}

double NNParaElectronCorrection::synapse0x8b89a60()
{
   return (neuron0x8b87178() * 0.258651);
}

double NNParaElectronCorrection::synapse0x8b89a88()
{
   return (neuron0x8b873d0() * -0.195361);
}

double NNParaElectronCorrection::synapse0x8b89ab0()
{
   return (neuron0x8b85658() * 0.035686);
}

