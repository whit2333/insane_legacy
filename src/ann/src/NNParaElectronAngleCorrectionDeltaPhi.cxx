#include "NNParaElectronAngleCorrectionDeltaPhi.h"
#include <cmath>

double NNParaElectronAngleCorrectionDeltaPhi::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - 0.707086)/0.0954631;
   input1 = (in1 - -0.00717654)/0.27641;
   input2 = (in2 - -3.34888)/32.7675;
   input3 = (in3 - -1.60046)/60.7523;
   input4 = (in4 - 2198.21)/971.743;
   input5 = (in5 - 0.00360118)/0.748592;
   input6 = (in6 - -0.0026409)/0.753729;
   switch(index) {
     case 0:
         return neuron0x9039fd8();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionDeltaPhi::Value(int index, double* input) {
   input0 = (input[0] - 0.707086)/0.0954631;
   input1 = (input[1] - -0.00717654)/0.27641;
   input2 = (input[2] - -3.34888)/32.7675;
   input3 = (input[3] - -1.60046)/60.7523;
   input4 = (input[4] - 2198.21)/971.743;
   input5 = (input[5] - 0.00360118)/0.748592;
   input6 = (input[6] - -0.0026409)/0.753729;
   switch(index) {
     case 0:
         return neuron0x9039fd8();
     default:
         return 0.;
   }
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x902af48() {
   return input0;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x902b128() {
   return input1;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90395b0() {
   return input2;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90397b0() {
   return input3;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90399b0() {
   return input4;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9039bd8() {
   return input5;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9039dd8() {
   return input6;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903a100() {
   double input = 0.486495;
   input += synapse0x8de3d38();
   input += synapse0x8dc3280();
   input += synapse0x8dc32c8();
   input += synapse0x8dc3398();
   input += synapse0x903a2b8();
   input += synapse0x903a2e0();
   input += synapse0x903a308();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903a100() {
   double input = input0x903a100();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903a330() {
   double input = 0.26861;
   input += synapse0x903a530();
   input += synapse0x903a558();
   input += synapse0x903a580();
   input += synapse0x903a5a8();
   input += synapse0x903a5d0();
   input += synapse0x903a5f8();
   input += synapse0x903a620();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903a330() {
   double input = input0x903a330();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903a648() {
   double input = -0.480323;
   input += synapse0x903a848();
   input += synapse0x903a870();
   input += synapse0x903a898();
   input += synapse0x903a948();
   input += synapse0x903a970();
   input += synapse0x903a998();
   input += synapse0x903a9c0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903a648() {
   double input = input0x903a648();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903a9e8() {
   double input = -0.436693;
   input += synapse0x903aba0();
   input += synapse0x903abc8();
   input += synapse0x903abf0();
   input += synapse0x903ac18();
   input += synapse0x903ac40();
   input += synapse0x903ac68();
   input += synapse0x903ac90();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903a9e8() {
   double input = input0x903a9e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903acb8() {
   double input = -0.171893;
   input += synapse0x903aed0();
   input += synapse0x903aef8();
   input += synapse0x903af20();
   input += synapse0x903af48();
   input += synapse0x903af70();
   input += synapse0x903a8c0();
   input += synapse0x903a8e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903acb8() {
   double input = input0x903acb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903b0a0() {
   double input = 0.404;
   input += synapse0x903b2a0();
   input += synapse0x903b2c8();
   input += synapse0x903b2f0();
   input += synapse0x903b318();
   input += synapse0x903b340();
   input += synapse0x903b368();
   input += synapse0x903b390();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903b0a0() {
   double input = input0x903b0a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903b3b8() {
   double input = 0.327805;
   input += synapse0x903b5b8();
   input += synapse0x903b5e0();
   input += synapse0x903b608();
   input += synapse0x903b630();
   input += synapse0x903b658();
   input += synapse0x903b680();
   input += synapse0x903b6a8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903b3b8() {
   double input = input0x903b3b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903b6d0() {
   double input = -1.02869;
   input += synapse0x903b8e8();
   input += synapse0x903b910();
   input += synapse0x903b938();
   input += synapse0x903b960();
   input += synapse0x903b988();
   input += synapse0x903b9b0();
   input += synapse0x903b9d8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903b6d0() {
   double input = input0x903b6d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903ba00() {
   double input = 0.2434;
   input += synapse0x903bc18();
   input += synapse0x903bc40();
   input += synapse0x903bc68();
   input += synapse0x903bc90();
   input += synapse0x903bcb8();
   input += synapse0x903bce0();
   input += synapse0x903bd08();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903ba00() {
   double input = input0x903ba00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903bd30() {
   double input = -0.517018;
   input += synapse0x903bfd0();
   input += synapse0x903bff8();
   input += synapse0x8dc31b8();
   input += synapse0x8dc33c0();
   input += synapse0x8dc2db0();
   input += synapse0x8de3cd8();
   input += synapse0x8de3d00();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903bd30() {
   double input = input0x903bd30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903c228() {
   double input = 0.128989;
   input += synapse0x903b070();
   input += synapse0x903c350();
   input += synapse0x903c378();
   input += synapse0x903c3a0();
   input += synapse0x903c3c8();
   input += synapse0x903c3f0();
   input += synapse0x903c418();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903c228() {
   double input = input0x903c228();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903c440() {
   double input = 0.34334;
   input += synapse0x903c640();
   input += synapse0x903c668();
   input += synapse0x903c690();
   input += synapse0x903c6b8();
   input += synapse0x903c6e0();
   input += synapse0x903c708();
   input += synapse0x903c730();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903c440() {
   double input = input0x903c440();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903c758() {
   double input = -0.164352;
   input += synapse0x903c958();
   input += synapse0x903c980();
   input += synapse0x903c9a8();
   input += synapse0x903c9d0();
   input += synapse0x903c9f8();
   input += synapse0x903ca20();
   input += synapse0x903ca48();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903c758() {
   double input = input0x903c758();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903ca70() {
   double input = 0.395933;
   input += synapse0x903cc70();
   input += synapse0x903cc98();
   input += synapse0x903ccc0();
   input += synapse0x903cce8();
   input += synapse0x903cd10();
   input += synapse0x903cd38();
   input += synapse0x903cd60();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903ca70() {
   double input = input0x903ca70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903cd88() {
   double input = -0.0503002;
   input += synapse0x903cf88();
   input += synapse0x903cfb0();
   input += synapse0x903cfd8();
   input += synapse0x903d000();
   input += synapse0x903d028();
   input += synapse0x903d050();
   input += synapse0x903d078();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903cd88() {
   double input = input0x903cd88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903d0a0() {
   double input = -0.106606;
   input += synapse0x903d2a0();
   input += synapse0x903d350();
   input += synapse0x903d400();
   input += synapse0x903d4b0();
   input += synapse0x903d560();
   input += synapse0x903d610();
   input += synapse0x903d6c0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903d0a0() {
   double input = input0x903d0a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903d770() {
   double input = 0.214811;
   input += synapse0x903d898();
   input += synapse0x903d8c0();
   input += synapse0x903d8e8();
   input += synapse0x903d910();
   input += synapse0x903d938();
   input += synapse0x903d960();
   input += synapse0x903d988();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903d770() {
   double input = input0x903d770();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903d9b0() {
   double input = -0.385777;
   input += synapse0x903dad8();
   input += synapse0x903db00();
   input += synapse0x903db28();
   input += synapse0x903db50();
   input += synapse0x903db78();
   input += synapse0x903dba0();
   input += synapse0x903dbc8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903d9b0() {
   double input = input0x903d9b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903dbf0() {
   double input = -0.299124;
   input += synapse0x903ddc0();
   input += synapse0x903dde8();
   input += synapse0x903de10();
   input += synapse0x903c020();
   input += synapse0x903c048();
   input += synapse0x903c070();
   input += synapse0x903c098();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903dbf0() {
   double input = input0x903dbf0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903c0c0() {
   double input = 0.0960758;
   input += synapse0x903c200();
   input += synapse0x903e318();
   input += synapse0x903e340();
   input += synapse0x903e368();
   input += synapse0x903e390();
   input += synapse0x903e3b8();
   input += synapse0x903e3e0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903c0c0() {
   double input = input0x903c0c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903e408() {
   double input = -0.29983;
   input += synapse0x903e620();
   input += synapse0x903e648();
   input += synapse0x903e670();
   input += synapse0x903e698();
   input += synapse0x903e6c0();
   input += synapse0x903e6e8();
   input += synapse0x903e710();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903e408() {
   double input = input0x903e408();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903e738() {
   double input = -0.0950885;
   input += synapse0x903e950();
   input += synapse0x903e978();
   input += synapse0x903e9a0();
   input += synapse0x903e9c8();
   input += synapse0x903e9f0();
   input += synapse0x903ea18();
   input += synapse0x903ea40();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903e738() {
   double input = input0x903e738();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903ea68() {
   double input = 0.431079;
   input += synapse0x903ec80();
   input += synapse0x903eca8();
   input += synapse0x903ecd0();
   input += synapse0x903ecf8();
   input += synapse0x903ed20();
   input += synapse0x903ed48();
   input += synapse0x903ed70();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903ea68() {
   double input = input0x903ea68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903ed98() {
   double input = -0.460462;
   input += synapse0x903efb0();
   input += synapse0x903efd8();
   input += synapse0x903f000();
   input += synapse0x903f028();
   input += synapse0x903f050();
   input += synapse0x903f078();
   input += synapse0x903f0a0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903ed98() {
   double input = input0x903ed98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903f0c8() {
   double input = 0.446959;
   input += synapse0x903f2e0();
   input += synapse0x903f308();
   input += synapse0x903f330();
   input += synapse0x903f358();
   input += synapse0x903f380();
   input += synapse0x903f3a8();
   input += synapse0x903f3d0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903f0c8() {
   double input = input0x903f0c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903f3f8() {
   double input = 0.271274;
   input += synapse0x903bf48();
   input += synapse0x903bf70();
   input += synapse0x903bf98();
   input += synapse0x903f718();
   input += synapse0x903f740();
   input += synapse0x903f768();
   input += synapse0x903f790();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903f3f8() {
   double input = input0x903f3f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903f7b8() {
   double input = -0.105124;
   input += synapse0x903f9d0();
   input += synapse0x903f9f8();
   input += synapse0x903fa20();
   input += synapse0x903fa48();
   input += synapse0x903fa70();
   input += synapse0x903fa98();
   input += synapse0x903fac0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903f7b8() {
   double input = input0x903f7b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903fae8() {
   double input = -0.205134;
   input += synapse0x903fd00();
   input += synapse0x903fd28();
   input += synapse0x903fd50();
   input += synapse0x903fd78();
   input += synapse0x903fda0();
   input += synapse0x903fdc8();
   input += synapse0x903fdf0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903fae8() {
   double input = input0x903fae8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903fe18() {
   double input = 0.0373017;
   input += synapse0x9040030();
   input += synapse0x9040058();
   input += synapse0x9040080();
   input += synapse0x90400a8();
   input += synapse0x90400d0();
   input += synapse0x90400f8();
   input += synapse0x9040120();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903fe18() {
   double input = input0x903fe18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9040148() {
   double input = -0.340417;
   input += synapse0x9040360();
   input += synapse0x9040388();
   input += synapse0x90403b0();
   input += synapse0x90403d8();
   input += synapse0x9040400();
   input += synapse0x9040428();
   input += synapse0x9040450();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9040148() {
   double input = input0x9040148();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9040478() {
   double input = 0.125943;
   input += synapse0x9040690();
   input += synapse0x90406b8();
   input += synapse0x90406e0();
   input += synapse0x9040708();
   input += synapse0x9040730();
   input += synapse0x9040758();
   input += synapse0x9040780();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9040478() {
   double input = input0x9040478();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90407a8() {
   double input = 1.5014;
   input += synapse0x90409c0();
   input += synapse0x903d2c8();
   input += synapse0x903d2f0();
   input += synapse0x903d318();
   input += synapse0x903d378();
   input += synapse0x903d3a0();
   input += synapse0x903d3c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90407a8() {
   double input = input0x90407a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9041120() {
   double input = 0.150242;
   input += synapse0x903d488();
   input += synapse0x903d5d0();
   input += synapse0x903d520();
   input += synapse0x903d638();
   input += synapse0x903d660();
   input += synapse0x903d688();
   input += synapse0x903d6e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9041120() {
   double input = input0x9041120();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9041248() {
   double input = -0.389987;
   input += synapse0x9041400();
   input += synapse0x9041428();
   input += synapse0x9041450();
   input += synapse0x9041478();
   input += synapse0x90414a0();
   input += synapse0x90414c8();
   input += synapse0x90414f0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9041248() {
   double input = input0x9041248();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9041518() {
   double input = 0.115428;
   input += synapse0x9041718();
   input += synapse0x9041740();
   input += synapse0x9041768();
   input += synapse0x9041790();
   input += synapse0x90417b8();
   input += synapse0x90417e0();
   input += synapse0x9041808();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9041518() {
   double input = input0x9041518();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9041830() {
   double input = 0.351038;
   input += synapse0x9041a30();
   input += synapse0x9041a58();
   input += synapse0x9041a80();
   input += synapse0x9041aa8();
   input += synapse0x9041ad0();
   input += synapse0x9041af8();
   input += synapse0x9041b20();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9041830() {
   double input = input0x9041830();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9041b48() {
   double input = 0.329621;
   input += synapse0x9041d60();
   input += synapse0x9041d88();
   input += synapse0x9041db0();
   input += synapse0x9041dd8();
   input += synapse0x9041e00();
   input += synapse0x903de38();
   input += synapse0x903de60();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9041b48() {
   double input = input0x9041b48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x903de88() {
   double input = 0.132685;
   input += synapse0x903e0a0();
   input += synapse0x903e0c8();
   input += synapse0x903e0f0();
   input += synapse0x903e118();
   input += synapse0x903e140();
   input += synapse0x903e168();
   input += synapse0x903e190();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x903de88() {
   double input = input0x903de88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9042630() {
   double input = 0.128758;
   input += synapse0x903e218();
   input += synapse0x90427e8();
   input += synapse0x9042810();
   input += synapse0x9042838();
   input += synapse0x9042860();
   input += synapse0x9042888();
   input += synapse0x90428b0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9042630() {
   double input = input0x9042630();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90428d8() {
   double input = 0.391669;
   input += synapse0x9042af0();
   input += synapse0x9042b18();
   input += synapse0x9042b40();
   input += synapse0x9042b68();
   input += synapse0x9042b90();
   input += synapse0x9042bb8();
   input += synapse0x9042be0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90428d8() {
   double input = input0x90428d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9042c08() {
   double input = -0.0542285;
   input += synapse0x9042e20();
   input += synapse0x9042e48();
   input += synapse0x9042e70();
   input += synapse0x9042e98();
   input += synapse0x9042ec0();
   input += synapse0x9042ee8();
   input += synapse0x9042f10();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9042c08() {
   double input = input0x9042c08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9042f38() {
   double input = 0.642271;
   input += synapse0x9043150();
   input += synapse0x9043178();
   input += synapse0x90431a0();
   input += synapse0x90431c8();
   input += synapse0x90431f0();
   input += synapse0x9043218();
   input += synapse0x9043240();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9042f38() {
   double input = input0x9042f38();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9043268() {
   double input = 0.106088;
   input += synapse0x9043480();
   input += synapse0x90434a8();
   input += synapse0x90434d0();
   input += synapse0x90434f8();
   input += synapse0x9043520();
   input += synapse0x9043548();
   input += synapse0x9043570();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9043268() {
   double input = input0x9043268();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9043598() {
   double input = -0.236907;
   input += synapse0x90437b0();
   input += synapse0x90437d8();
   input += synapse0x9043800();
   input += synapse0x9043828();
   input += synapse0x9043850();
   input += synapse0x9043878();
   input += synapse0x90438a0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9043598() {
   double input = input0x9043598();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90438c8() {
   double input = 0.267344;
   input += synapse0x9043ae0();
   input += synapse0x9043b08();
   input += synapse0x9043b30();
   input += synapse0x9043b58();
   input += synapse0x9043b80();
   input += synapse0x9043ba8();
   input += synapse0x9043bd0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90438c8() {
   double input = input0x90438c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9043bf8() {
   double input = 0.221435;
   input += synapse0x9043e10();
   input += synapse0x9043e38();
   input += synapse0x9043e60();
   input += synapse0x9043e88();
   input += synapse0x9043eb0();
   input += synapse0x9043ed8();
   input += synapse0x9043f00();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9043bf8() {
   double input = input0x9043bf8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9043f28() {
   double input = -0.573678;
   input += synapse0x9044140();
   input += synapse0x9044168();
   input += synapse0x9044190();
   input += synapse0x90441b8();
   input += synapse0x90441e0();
   input += synapse0x9044208();
   input += synapse0x9044230();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9043f28() {
   double input = input0x9043f28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9044258() {
   double input = 0.264415;
   input += synapse0x9044470();
   input += synapse0x9044498();
   input += synapse0x90444c0();
   input += synapse0x90444e8();
   input += synapse0x9044510();
   input += synapse0x9044538();
   input += synapse0x9044560();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9044258() {
   double input = input0x9044258();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9044588() {
   double input = 0.152783;
   input += synapse0x90447a0();
   input += synapse0x90447c8();
   input += synapse0x90447f0();
   input += synapse0x9044818();
   input += synapse0x9044840();
   input += synapse0x9044868();
   input += synapse0x9044890();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9044588() {
   double input = input0x9044588();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90448b8() {
   double input = 0.127646;
   input += synapse0x9044ad0();
   input += synapse0x9044af8();
   input += synapse0x9044b20();
   input += synapse0x9044b48();
   input += synapse0x9044b70();
   input += synapse0x9044b98();
   input += synapse0x9044bc0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90448b8() {
   double input = input0x90448b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9044be8() {
   double input = 0.0306442;
   input += synapse0x9044e00();
   input += synapse0x9044e28();
   input += synapse0x9044e50();
   input += synapse0x9044e78();
   input += synapse0x9044ea0();
   input += synapse0x9044ec8();
   input += synapse0x9044ef0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9044be8() {
   double input = input0x9044be8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9044f18() {
   double input = 0.216511;
   input += synapse0x9045130();
   input += synapse0x9045158();
   input += synapse0x9045180();
   input += synapse0x90451a8();
   input += synapse0x90451d0();
   input += synapse0x90451f8();
   input += synapse0x9045220();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9044f18() {
   double input = input0x9044f18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9045248() {
   double input = 0.468301;
   input += synapse0x9045460();
   input += synapse0x9045488();
   input += synapse0x90454b0();
   input += synapse0x90454d8();
   input += synapse0x9045500();
   input += synapse0x9045528();
   input += synapse0x9045550();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9045248() {
   double input = input0x9045248();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9045578() {
   double input = 0.141967;
   input += synapse0x9045790();
   input += synapse0x90457b8();
   input += synapse0x90457e0();
   input += synapse0x9045808();
   input += synapse0x9045830();
   input += synapse0x9045858();
   input += synapse0x9045880();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9045578() {
   double input = input0x9045578();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90458a8() {
   double input = -0.176991;
   input += synapse0x9045ac0();
   input += synapse0x9045ae8();
   input += synapse0x9045b10();
   input += synapse0x9045b38();
   input += synapse0x9045b60();
   input += synapse0x9045b88();
   input += synapse0x9045bb0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90458a8() {
   double input = input0x90458a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9045bd8() {
   double input = 0.118243;
   input += synapse0x9045df0();
   input += synapse0x9045e18();
   input += synapse0x9045e40();
   input += synapse0x9045e68();
   input += synapse0x9045e90();
   input += synapse0x9045eb8();
   input += synapse0x9045ee0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9045bd8() {
   double input = input0x9045bd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9045f08() {
   double input = -0.299551;
   input += synapse0x9046120();
   input += synapse0x9046148();
   input += synapse0x9046170();
   input += synapse0x9046198();
   input += synapse0x90461c0();
   input += synapse0x90461e8();
   input += synapse0x9046210();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9045f08() {
   double input = input0x9045f08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9046238() {
   double input = 0.0710443;
   input += synapse0x903f610();
   input += synapse0x903f638();
   input += synapse0x903f660();
   input += synapse0x903f688();
   input += synapse0x903f6b0();
   input += synapse0x903f6d8();
   input += synapse0x9046658();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9046238() {
   double input = input0x9046238();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9046680() {
   double input = 0.147719;
   input += synapse0x9046880();
   input += synapse0x90468a8();
   input += synapse0x90468d0();
   input += synapse0x90468f8();
   input += synapse0x9046920();
   input += synapse0x9046948();
   input += synapse0x9046970();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9046680() {
   double input = input0x9046680();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9046998() {
   double input = -0.101435;
   input += synapse0x9046bb0();
   input += synapse0x9046bd8();
   input += synapse0x9046c00();
   input += synapse0x9046c28();
   input += synapse0x9046c50();
   input += synapse0x9046c78();
   input += synapse0x9046ca0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9046998() {
   double input = input0x9046998();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9046cc8() {
   double input = -0.401464;
   input += synapse0x9046ee0();
   input += synapse0x9046f08();
   input += synapse0x9046f30();
   input += synapse0x9046f58();
   input += synapse0x9046f80();
   input += synapse0x9046fa8();
   input += synapse0x9046fd0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9046cc8() {
   double input = input0x9046cc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9046ff8() {
   double input = -0.567108;
   input += synapse0x9047210();
   input += synapse0x9047238();
   input += synapse0x9047260();
   input += synapse0x9047288();
   input += synapse0x90472b0();
   input += synapse0x90472d8();
   input += synapse0x9047300();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9046ff8() {
   double input = input0x9046ff8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9047328() {
   double input = 0.117769;
   input += synapse0x9047540();
   input += synapse0x9047568();
   input += synapse0x9047590();
   input += synapse0x90475b8();
   input += synapse0x90475e0();
   input += synapse0x9047608();
   input += synapse0x9047630();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9047328() {
   double input = input0x9047328();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9047658() {
   double input = 0.441874;
   input += synapse0x9047870();
   input += synapse0x90409e8();
   input += synapse0x9040a10();
   input += synapse0x9040a38();
   input += synapse0x9040c68();
   input += synapse0x9040c90();
   input += synapse0x9040ec0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9047658() {
   double input = input0x9047658();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9040ee8() {
   double input = -0.143192;
   input += synapse0x90482c0();
   input += synapse0x90482e8();
   input += synapse0x9048310();
   input += synapse0x9048338();
   input += synapse0x9048360();
   input += synapse0x9048388();
   input += synapse0x90483b0();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9040ee8() {
   double input = input0x9040ee8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90483d8() {
   double input = 0.0288871;
   input += synapse0x90485d8();
   input += synapse0x9048600();
   input += synapse0x9048628();
   input += synapse0x9048650();
   input += synapse0x9048678();
   input += synapse0x90486a0();
   input += synapse0x90486c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90483d8() {
   double input = input0x90483d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90486f0() {
   double input = 0.38077;
   input += synapse0x9048908();
   input += synapse0x9048930();
   input += synapse0x9048958();
   input += synapse0x9048980();
   input += synapse0x90489a8();
   input += synapse0x90489d0();
   input += synapse0x90489f8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90486f0() {
   double input = input0x90486f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9048a20() {
   double input = -0.0913814;
   input += synapse0x9048c38();
   input += synapse0x9048c60();
   input += synapse0x9048c88();
   input += synapse0x9048cb0();
   input += synapse0x9048cd8();
   input += synapse0x9048d00();
   input += synapse0x9048d28();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9048a20() {
   double input = input0x9048a20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9048d50() {
   double input = 0.066779;
   input += synapse0x9048f68();
   input += synapse0x9048f90();
   input += synapse0x9048fb8();
   input += synapse0x9048fe0();
   input += synapse0x9049008();
   input += synapse0x9049030();
   input += synapse0x9049058();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9048d50() {
   double input = input0x9048d50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9049080() {
   double input = 0.397284;
   input += synapse0x9049298();
   input += synapse0x90492c0();
   input += synapse0x90492e8();
   input += synapse0x9049310();
   input += synapse0x9049338();
   input += synapse0x9049360();
   input += synapse0x9049388();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9049080() {
   double input = input0x9049080();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90493b0() {
   double input = -0.146028;
   input += synapse0x90495c8();
   input += synapse0x90495f0();
   input += synapse0x9049618();
   input += synapse0x9049640();
   input += synapse0x9049668();
   input += synapse0x9049690();
   input += synapse0x90496b8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90493b0() {
   double input = input0x90493b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90496e0() {
   double input = 1.70887;
   input += synapse0x90498f8();
   input += synapse0x9049920();
   input += synapse0x9049948();
   input += synapse0x9049970();
   input += synapse0x9049998();
   input += synapse0x90499c0();
   input += synapse0x90499e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90496e0() {
   double input = input0x90496e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9049a10() {
   double input = 0.121766;
   input += synapse0x9049c28();
   input += synapse0x9049c50();
   input += synapse0x9049c78();
   input += synapse0x9049ca0();
   input += synapse0x9049cc8();
   input += synapse0x9049cf0();
   input += synapse0x9049d18();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9049a10() {
   double input = input0x9049a10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9049d40() {
   double input = 0.0290289;
   input += synapse0x9049f58();
   input += synapse0x9049f80();
   input += synapse0x9041e28();
   input += synapse0x9041e50();
   input += synapse0x9041e78();
   input += synapse0x9041ea0();
   input += synapse0x9041ec8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9049d40() {
   double input = input0x9049d40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9041ef0() {
   double input = -0.459922;
   input += synapse0x9042108();
   input += synapse0x9042130();
   input += synapse0x9042158();
   input += synapse0x9042180();
   input += synapse0x90421a8();
   input += synapse0x90421d0();
   input += synapse0x90421f8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9041ef0() {
   double input = input0x9041ef0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9042220() {
   double input = 0.251397;
   input += synapse0x9042438();
   input += synapse0x9042460();
   input += synapse0x9042488();
   input += synapse0x90424b0();
   input += synapse0x90424d8();
   input += synapse0x9042500();
   input += synapse0x9042528();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9042220() {
   double input = input0x9042220();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904afb0() {
   double input = -2.43944;
   input += synapse0x90425f8();
   input += synapse0x904b120();
   input += synapse0x904b148();
   input += synapse0x904b170();
   input += synapse0x904b198();
   input += synapse0x904b1c0();
   input += synapse0x904b1e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904afb0() {
   double input = input0x904afb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904b210() {
   double input = -0.454297;
   input += synapse0x904b428();
   input += synapse0x904b450();
   input += synapse0x904b478();
   input += synapse0x904b4a0();
   input += synapse0x904b4c8();
   input += synapse0x904b4f0();
   input += synapse0x904b518();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904b210() {
   double input = input0x904b210();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904b540() {
   double input = -0.29636;
   input += synapse0x904b758();
   input += synapse0x904b780();
   input += synapse0x904b7a8();
   input += synapse0x904b7d0();
   input += synapse0x904b7f8();
   input += synapse0x904b820();
   input += synapse0x904b848();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904b540() {
   double input = input0x904b540();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904b870() {
   double input = 0.477979;
   input += synapse0x904ba88();
   input += synapse0x904bab0();
   input += synapse0x904bad8();
   input += synapse0x904bb00();
   input += synapse0x904bb28();
   input += synapse0x904bb50();
   input += synapse0x904bb78();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904b870() {
   double input = input0x904b870();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904bba0() {
   double input = -0.101104;
   input += synapse0x904bdb8();
   input += synapse0x904bde0();
   input += synapse0x904be08();
   input += synapse0x904be30();
   input += synapse0x904be58();
   input += synapse0x904be80();
   input += synapse0x904bea8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904bba0() {
   double input = input0x904bba0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904bed0() {
   double input = 0.41555;
   input += synapse0x904c0e8();
   input += synapse0x904c110();
   input += synapse0x904c138();
   input += synapse0x904c160();
   input += synapse0x904c188();
   input += synapse0x904c1b0();
   input += synapse0x904c1d8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904bed0() {
   double input = input0x904bed0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904c200() {
   double input = 0.11067;
   input += synapse0x904c418();
   input += synapse0x904c440();
   input += synapse0x904c468();
   input += synapse0x904c490();
   input += synapse0x904c4b8();
   input += synapse0x904c4e0();
   input += synapse0x904c508();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904c200() {
   double input = input0x904c200();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904c530() {
   double input = -0.532319;
   input += synapse0x904c748();
   input += synapse0x904c770();
   input += synapse0x904c798();
   input += synapse0x904c7c0();
   input += synapse0x904c7e8();
   input += synapse0x904c810();
   input += synapse0x904c838();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904c530() {
   double input = input0x904c530();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904c860() {
   double input = 0.310329;
   input += synapse0x904ca78();
   input += synapse0x904caa0();
   input += synapse0x904cac8();
   input += synapse0x904caf0();
   input += synapse0x904cb18();
   input += synapse0x904cb40();
   input += synapse0x904cb68();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904c860() {
   double input = input0x904c860();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904cb90() {
   double input = -0.110463;
   input += synapse0x904cda8();
   input += synapse0x904cdd0();
   input += synapse0x904cdf8();
   input += synapse0x904ce20();
   input += synapse0x904ce48();
   input += synapse0x904ce70();
   input += synapse0x904ce98();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904cb90() {
   double input = input0x904cb90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904cec0() {
   double input = -0.0918208;
   input += synapse0x904d0d8();
   input += synapse0x904d100();
   input += synapse0x904d128();
   input += synapse0x904d150();
   input += synapse0x904d178();
   input += synapse0x904d1a0();
   input += synapse0x904d1c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904cec0() {
   double input = input0x904cec0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904d1f0() {
   double input = -0.426101;
   input += synapse0x904d408();
   input += synapse0x904d430();
   input += synapse0x904d458();
   input += synapse0x904d480();
   input += synapse0x904d4a8();
   input += synapse0x904d4d0();
   input += synapse0x904d4f8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904d1f0() {
   double input = input0x904d1f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904d520() {
   double input = 0.858678;
   input += synapse0x904d738();
   input += synapse0x904d760();
   input += synapse0x904d788();
   input += synapse0x904d7b0();
   input += synapse0x904d7d8();
   input += synapse0x904d800();
   input += synapse0x904d828();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904d520() {
   double input = input0x904d520();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904d850() {
   double input = 0.355946;
   input += synapse0x904da68();
   input += synapse0x904da90();
   input += synapse0x904dab8();
   input += synapse0x904dae0();
   input += synapse0x904db08();
   input += synapse0x904db30();
   input += synapse0x904db58();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904d850() {
   double input = input0x904d850();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904db80() {
   double input = -0.262397;
   input += synapse0x904dd98();
   input += synapse0x904ddc0();
   input += synapse0x904dde8();
   input += synapse0x904de10();
   input += synapse0x904de38();
   input += synapse0x904de60();
   input += synapse0x904de88();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904db80() {
   double input = input0x904db80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904deb0() {
   double input = 0.600792;
   input += synapse0x904e0c8();
   input += synapse0x904e0f0();
   input += synapse0x904e118();
   input += synapse0x904e140();
   input += synapse0x904e168();
   input += synapse0x904e190();
   input += synapse0x904e1b8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904deb0() {
   double input = input0x904deb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904e1e0() {
   double input = 0.114973;
   input += synapse0x904e3f8();
   input += synapse0x904e420();
   input += synapse0x904e448();
   input += synapse0x904e470();
   input += synapse0x904e498();
   input += synapse0x904e4c0();
   input += synapse0x904e4e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904e1e0() {
   double input = input0x904e1e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904e510() {
   double input = 0.137289;
   input += synapse0x904e728();
   input += synapse0x904e750();
   input += synapse0x904e778();
   input += synapse0x904e7a0();
   input += synapse0x904e7c8();
   input += synapse0x904e7f0();
   input += synapse0x904e818();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904e510() {
   double input = input0x904e510();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904e840() {
   double input = 0.252783;
   input += synapse0x904ea58();
   input += synapse0x904ea80();
   input += synapse0x904eaa8();
   input += synapse0x904ead0();
   input += synapse0x904eaf8();
   input += synapse0x904eb20();
   input += synapse0x904eb48();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904e840() {
   double input = input0x904e840();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904eb70() {
   double input = -0.176667;
   input += synapse0x904ed88();
   input += synapse0x904edb0();
   input += synapse0x904edd8();
   input += synapse0x904ee00();
   input += synapse0x904ee28();
   input += synapse0x904ee50();
   input += synapse0x904ee78();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904eb70() {
   double input = input0x904eb70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904eea0() {
   double input = -0.0199841;
   input += synapse0x904f0b8();
   input += synapse0x904f0e0();
   input += synapse0x904f108();
   input += synapse0x904f130();
   input += synapse0x904f158();
   input += synapse0x904f180();
   input += synapse0x904f1a8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904eea0() {
   double input = input0x904eea0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904f1d0() {
   double input = 0.163844;
   input += synapse0x904f3e8();
   input += synapse0x904f410();
   input += synapse0x904f438();
   input += synapse0x904f460();
   input += synapse0x904f488();
   input += synapse0x904f4b0();
   input += synapse0x904f4d8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904f1d0() {
   double input = input0x904f1d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904f500() {
   double input = 0.367234;
   input += synapse0x904f718();
   input += synapse0x904f740();
   input += synapse0x904f768();
   input += synapse0x904f790();
   input += synapse0x904f7b8();
   input += synapse0x904f7e0();
   input += synapse0x904f808();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904f500() {
   double input = input0x904f500();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904f830() {
   double input = -0.177327;
   input += synapse0x904fa48();
   input += synapse0x904fa70();
   input += synapse0x904fa98();
   input += synapse0x904fac0();
   input += synapse0x904fae8();
   input += synapse0x904fb10();
   input += synapse0x904fb38();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904f830() {
   double input = input0x904f830();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904fb60() {
   double input = 0.133957;
   input += synapse0x904fd78();
   input += synapse0x904fda0();
   input += synapse0x904fdc8();
   input += synapse0x904fdf0();
   input += synapse0x904fe18();
   input += synapse0x904fe40();
   input += synapse0x904fe68();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904fb60() {
   double input = input0x904fb60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x904fe90() {
   double input = 0.0443024;
   input += synapse0x90500a8();
   input += synapse0x90500d0();
   input += synapse0x90500f8();
   input += synapse0x9050120();
   input += synapse0x9050148();
   input += synapse0x9050170();
   input += synapse0x9050198();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x904fe90() {
   double input = input0x904fe90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90501c0() {
   double input = 0.670953;
   input += synapse0x90503d8();
   input += synapse0x9050400();
   input += synapse0x9050428();
   input += synapse0x9050450();
   input += synapse0x9050478();
   input += synapse0x90504a0();
   input += synapse0x90504c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90501c0() {
   double input = input0x90501c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90504f0() {
   double input = -0.422289;
   input += synapse0x9050708();
   input += synapse0x9050730();
   input += synapse0x9050758();
   input += synapse0x9050780();
   input += synapse0x90507a8();
   input += synapse0x90507d0();
   input += synapse0x90507f8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90504f0() {
   double input = input0x90504f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9050820() {
   double input = 0.0771036;
   input += synapse0x9050a38();
   input += synapse0x9050a60();
   input += synapse0x9050a88();
   input += synapse0x9050ab0();
   input += synapse0x9050ad8();
   input += synapse0x9050b00();
   input += synapse0x9050b28();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9050820() {
   double input = input0x9050820();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9050b50() {
   double input = -0.0682889;
   input += synapse0x9050d68();
   input += synapse0x9050d90();
   input += synapse0x9050db8();
   input += synapse0x9050de0();
   input += synapse0x9050e08();
   input += synapse0x9050e30();
   input += synapse0x9050e58();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9050b50() {
   double input = input0x9050b50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9050e80() {
   double input = -0.360689;
   input += synapse0x9051098();
   input += synapse0x90510c0();
   input += synapse0x90510e8();
   input += synapse0x9051110();
   input += synapse0x9051138();
   input += synapse0x9051160();
   input += synapse0x9051188();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9050e80() {
   double input = input0x9050e80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90511b0() {
   double input = -0.269524;
   input += synapse0x90513c8();
   input += synapse0x90513f0();
   input += synapse0x9051418();
   input += synapse0x9051440();
   input += synapse0x9051468();
   input += synapse0x9051490();
   input += synapse0x90514b8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90511b0() {
   double input = input0x90511b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90514e0() {
   double input = 0.0616153;
   input += synapse0x90516f8();
   input += synapse0x9051720();
   input += synapse0x9051748();
   input += synapse0x9051770();
   input += synapse0x9051798();
   input += synapse0x90517c0();
   input += synapse0x90517e8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90514e0() {
   double input = input0x90514e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9051810() {
   double input = -0.449709;
   input += synapse0x9051a28();
   input += synapse0x9051a50();
   input += synapse0x9051a78();
   input += synapse0x9051aa0();
   input += synapse0x9051ac8();
   input += synapse0x9051af0();
   input += synapse0x9051b18();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9051810() {
   double input = input0x9051810();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9051b40() {
   double input = 0.42947;
   input += synapse0x9051d58();
   input += synapse0x9051d80();
   input += synapse0x9051da8();
   input += synapse0x9051dd0();
   input += synapse0x9051df8();
   input += synapse0x9051e20();
   input += synapse0x9051e48();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9051b40() {
   double input = input0x9051b40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9051e70() {
   double input = 0.144411;
   input += synapse0x9052088();
   input += synapse0x90520b0();
   input += synapse0x90520d8();
   input += synapse0x9052100();
   input += synapse0x9052128();
   input += synapse0x9052150();
   input += synapse0x9052178();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9051e70() {
   double input = input0x9051e70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90521a0() {
   double input = -0.0282241;
   input += synapse0x90523b8();
   input += synapse0x90523e0();
   input += synapse0x9052408();
   input += synapse0x9052430();
   input += synapse0x9052458();
   input += synapse0x9052480();
   input += synapse0x90524a8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90521a0() {
   double input = input0x90521a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90524d0() {
   double input = 0.450077;
   input += synapse0x90526e8();
   input += synapse0x9052710();
   input += synapse0x9052738();
   input += synapse0x9052760();
   input += synapse0x9052788();
   input += synapse0x90527b0();
   input += synapse0x90527d8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90524d0() {
   double input = input0x90524d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9052800() {
   double input = 0.158345;
   input += synapse0x9052a18();
   input += synapse0x9052a40();
   input += synapse0x9052a68();
   input += synapse0x9052a90();
   input += synapse0x9052ab8();
   input += synapse0x9052ae0();
   input += synapse0x9052b08();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9052800() {
   double input = input0x9052800();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9052b30() {
   double input = -0.10013;
   input += synapse0x9052d48();
   input += synapse0x9052d70();
   input += synapse0x9052d98();
   input += synapse0x9052dc0();
   input += synapse0x9052de8();
   input += synapse0x9052e10();
   input += synapse0x9052e38();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9052b30() {
   double input = input0x9052b30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9052e60() {
   double input = -0.14764;
   input += synapse0x9053078();
   input += synapse0x90530a0();
   input += synapse0x90530c8();
   input += synapse0x90530f0();
   input += synapse0x9053118();
   input += synapse0x9053140();
   input += synapse0x9053168();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9052e60() {
   double input = input0x9052e60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9053190() {
   double input = -0.250426;
   input += synapse0x90533a8();
   input += synapse0x90533d0();
   input += synapse0x90533f8();
   input += synapse0x9053420();
   input += synapse0x9053448();
   input += synapse0x9053470();
   input += synapse0x9053498();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9053190() {
   double input = input0x9053190();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90534c0() {
   double input = 0.106572;
   input += synapse0x90536d8();
   input += synapse0x9053700();
   input += synapse0x9053728();
   input += synapse0x9053750();
   input += synapse0x9053778();
   input += synapse0x90537a0();
   input += synapse0x90537c8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90534c0() {
   double input = input0x90534c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x90537f0() {
   double input = -0.839837;
   input += synapse0x9053a08();
   input += synapse0x9053a30();
   input += synapse0x9053a58();
   input += synapse0x9053a80();
   input += synapse0x9053aa8();
   input += synapse0x9053ad0();
   input += synapse0x9053af8();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x90537f0() {
   double input = input0x90537f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::input0x9039fd8() {
   double input = 0.148709;
   input += synapse0x9053bf8();
   input += synapse0x9053c20();
   input += synapse0x9053c48();
   input += synapse0x9053c70();
   input += synapse0x9053c98();
   input += synapse0x9053cc0();
   input += synapse0x9053ce8();
   input += synapse0x9053d10();
   input += synapse0x9053d38();
   input += synapse0x9053d60();
   input += synapse0x9053d88();
   input += synapse0x9053db0();
   input += synapse0x9053dd8();
   input += synapse0x9053e00();
   input += synapse0x9053e28();
   input += synapse0x9053e50();
   input += synapse0x9053f00();
   input += synapse0x9053f28();
   input += synapse0x9053f50();
   input += synapse0x9053f78();
   input += synapse0x9053fa0();
   input += synapse0x9053fc8();
   input += synapse0x9053ff0();
   input += synapse0x9054018();
   input += synapse0x9054040();
   input += synapse0x9054068();
   input += synapse0x9054090();
   input += synapse0x90540b8();
   input += synapse0x90540e0();
   input += synapse0x9054108();
   input += synapse0x9054130();
   input += synapse0x9054158();
   input += synapse0x9053e78();
   input += synapse0x9053ea0();
   input += synapse0x9053ec8();
   input += synapse0x9054288();
   input += synapse0x90542b0();
   input += synapse0x90542d8();
   input += synapse0x9054300();
   input += synapse0x9054328();
   input += synapse0x9054350();
   input += synapse0x9054378();
   input += synapse0x90543a0();
   input += synapse0x90543c8();
   input += synapse0x90543f0();
   input += synapse0x9054418();
   input += synapse0x9054440();
   input += synapse0x9054468();
   input += synapse0x9054490();
   input += synapse0x90544b8();
   input += synapse0x90544e0();
   input += synapse0x9054508();
   input += synapse0x9054530();
   input += synapse0x9054558();
   input += synapse0x9054580();
   input += synapse0x90545a8();
   input += synapse0x90545d0();
   input += synapse0x90545f8();
   input += synapse0x9054620();
   input += synapse0x9054648();
   input += synapse0x9054670();
   input += synapse0x9054698();
   input += synapse0x90546c0();
   input += synapse0x90546e8();
   input += synapse0x9053b20();
   input += synapse0x9054180();
   input += synapse0x90541a8();
   input += synapse0x90541d0();
   input += synapse0x90541f8();
   input += synapse0x9054220();
   input += synapse0x9054248();
   input += synapse0x9054918();
   input += synapse0x9054940();
   input += synapse0x9054968();
   input += synapse0x9054990();
   input += synapse0x90549b8();
   input += synapse0x90549e0();
   input += synapse0x9054a08();
   input += synapse0x9054a30();
   input += synapse0x9054a58();
   input += synapse0x9054a80();
   input += synapse0x9054aa8();
   input += synapse0x9054ad0();
   input += synapse0x9054af8();
   input += synapse0x9054b20();
   input += synapse0x9054b48();
   input += synapse0x9054b70();
   input += synapse0x9054b98();
   input += synapse0x9054bc0();
   input += synapse0x9054be8();
   input += synapse0x9054c10();
   input += synapse0x9054c38();
   input += synapse0x9054c60();
   input += synapse0x9054c88();
   input += synapse0x9054cb0();
   input += synapse0x9054cd8();
   input += synapse0x9054d00();
   input += synapse0x9054d28();
   input += synapse0x9054d50();
   input += synapse0x9054d78();
   input += synapse0x9054da0();
   input += synapse0x9054dc8();
   input += synapse0x9054df0();
   input += synapse0x9054e18();
   input += synapse0x9054e40();
   input += synapse0x9054e68();
   input += synapse0x9054e90();
   input += synapse0x9054eb8();
   input += synapse0x9054ee0();
   input += synapse0x9054f08();
   input += synapse0x9054f30();
   input += synapse0x9054f58();
   input += synapse0x9054f80();
   input += synapse0x9054fa8();
   input += synapse0x9054fd0();
   input += synapse0x9054ff8();
   input += synapse0x9055020();
   input += synapse0x9055048();
   input += synapse0x9055070();
   input += synapse0x9055098();
   return input;
}

double NNParaElectronAngleCorrectionDeltaPhi::neuron0x9039fd8() {
   double input = input0x9039fd8();
   return (input * 1)+0;
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8de3d38() {
   return (neuron0x902af48()*-0.00416838);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8dc3280() {
   return (neuron0x902b128()*0.223736);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8dc32c8() {
   return (neuron0x90395b0()*0.0496311);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8dc3398() {
   return (neuron0x90397b0()*-0.101175);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a2b8() {
   return (neuron0x90399b0()*0.432571);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a2e0() {
   return (neuron0x9039bd8()*0.190892);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a308() {
   return (neuron0x9039dd8()*-0.121643);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a530() {
   return (neuron0x902af48()*0.042024);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a558() {
   return (neuron0x902b128()*-0.304785);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a580() {
   return (neuron0x90395b0()*0.0408125);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a5a8() {
   return (neuron0x90397b0()*0.0647383);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a5d0() {
   return (neuron0x90399b0()*-0.487274);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a5f8() {
   return (neuron0x9039bd8()*0.0776713);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a620() {
   return (neuron0x9039dd8()*0.200294);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a848() {
   return (neuron0x902af48()*-0.195347);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a870() {
   return (neuron0x902b128()*0.170544);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a898() {
   return (neuron0x90395b0()*0.21122);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a948() {
   return (neuron0x90397b0()*-0.030755);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a970() {
   return (neuron0x90399b0()*0.631893);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a998() {
   return (neuron0x9039bd8()*-0.0666876);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a9c0() {
   return (neuron0x9039dd8()*0.032719);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903aba0() {
   return (neuron0x902af48()*0.396441);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903abc8() {
   return (neuron0x902b128()*0.14422);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903abf0() {
   return (neuron0x90395b0()*-0.285213);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ac18() {
   return (neuron0x90397b0()*0.173144);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ac40() {
   return (neuron0x90399b0()*-0.0211658);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ac68() {
   return (neuron0x9039bd8()*-0.011974);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ac90() {
   return (neuron0x9039dd8()*0.35437);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903aed0() {
   return (neuron0x902af48()*0.181833);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903aef8() {
   return (neuron0x902b128()*-0.33635);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903af20() {
   return (neuron0x90395b0()*-0.0824757);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903af48() {
   return (neuron0x90397b0()*0.269253);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903af70() {
   return (neuron0x90399b0()*0.0397997);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a8c0() {
   return (neuron0x9039bd8()*0.384371);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903a8e8() {
   return (neuron0x9039dd8()*-0.368825);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b2a0() {
   return (neuron0x902af48()*-0.220417);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b2c8() {
   return (neuron0x902b128()*0.335104);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b2f0() {
   return (neuron0x90395b0()*0.0557392);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b318() {
   return (neuron0x90397b0()*-0.294694);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b340() {
   return (neuron0x90399b0()*0.0138994);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b368() {
   return (neuron0x9039bd8()*-0.184914);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b390() {
   return (neuron0x9039dd8()*0.0313735);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b5b8() {
   return (neuron0x902af48()*0.344359);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b5e0() {
   return (neuron0x902b128()*-0.39197);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b608() {
   return (neuron0x90395b0()*-0.0409987);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b630() {
   return (neuron0x90397b0()*0.133376);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b658() {
   return (neuron0x90399b0()*-0.0394155);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b680() {
   return (neuron0x9039bd8()*0.0841029);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b6a8() {
   return (neuron0x9039dd8()*-0.428865);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b8e8() {
   return (neuron0x902af48()*-0.145341);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b910() {
   return (neuron0x902b128()*0.179641);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b938() {
   return (neuron0x90395b0()*0.133929);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b960() {
   return (neuron0x90397b0()*-0.273139);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b988() {
   return (neuron0x90399b0()*-0.675812);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b9b0() {
   return (neuron0x9039bd8()*0.0100157);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b9d8() {
   return (neuron0x9039dd8()*-0.00302183);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bc18() {
   return (neuron0x902af48()*0.103417);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bc40() {
   return (neuron0x902b128()*0.0348056);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bc68() {
   return (neuron0x90395b0()*0.202292);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bc90() {
   return (neuron0x90397b0()*-0.00822806);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bcb8() {
   return (neuron0x90399b0()*-0.114099);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bce0() {
   return (neuron0x9039bd8()*0.317593);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bd08() {
   return (neuron0x9039dd8()*0.220269);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bfd0() {
   return (neuron0x902af48()*0.130556);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bff8() {
   return (neuron0x902b128()*-0.054777);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8dc31b8() {
   return (neuron0x90395b0()*0.214436);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8dc33c0() {
   return (neuron0x90397b0()*0.111291);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8dc2db0() {
   return (neuron0x90399b0()*-0.126801);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8de3cd8() {
   return (neuron0x9039bd8()*-0.15607);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x8de3d00() {
   return (neuron0x9039dd8()*-0.269932);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903b070() {
   return (neuron0x902af48()*0.0611817);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c350() {
   return (neuron0x902b128()*-0.250735);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c378() {
   return (neuron0x90395b0()*0.366);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c3a0() {
   return (neuron0x90397b0()*0.4101);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c3c8() {
   return (neuron0x90399b0()*0.0708204);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c3f0() {
   return (neuron0x9039bd8()*0.21961);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c418() {
   return (neuron0x9039dd8()*0.233149);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c640() {
   return (neuron0x902af48()*-0.167569);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c668() {
   return (neuron0x902b128()*-0.0895245);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c690() {
   return (neuron0x90395b0()*0.436714);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c6b8() {
   return (neuron0x90397b0()*0.373688);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c6e0() {
   return (neuron0x90399b0()*0.184094);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c708() {
   return (neuron0x9039bd8()*-0.253377);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c730() {
   return (neuron0x9039dd8()*-0.0126049);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c958() {
   return (neuron0x902af48()*0.296098);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c980() {
   return (neuron0x902b128()*-0.20231);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c9a8() {
   return (neuron0x90395b0()*-0.138747);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c9d0() {
   return (neuron0x90397b0()*-0.391562);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c9f8() {
   return (neuron0x90399b0()*-0.147499);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ca20() {
   return (neuron0x9039bd8()*0.0235741);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ca48() {
   return (neuron0x9039dd8()*0.138326);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cc70() {
   return (neuron0x902af48()*0.177124);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cc98() {
   return (neuron0x902b128()*0.301216);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ccc0() {
   return (neuron0x90395b0()*-0.246941);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cce8() {
   return (neuron0x90397b0()*0.0349551);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cd10() {
   return (neuron0x90399b0()*0.046973);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cd38() {
   return (neuron0x9039bd8()*0.240885);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cd60() {
   return (neuron0x9039dd8()*-0.337485);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cf88() {
   return (neuron0x902af48()*0.474357);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cfb0() {
   return (neuron0x902b128()*0.318958);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903cfd8() {
   return (neuron0x90395b0()*-0.116998);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d000() {
   return (neuron0x90397b0()*-0.293625);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d028() {
   return (neuron0x90399b0()*-0.0583589);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d050() {
   return (neuron0x9039bd8()*0.484809);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d078() {
   return (neuron0x9039dd8()*0.439172);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d2a0() {
   return (neuron0x902af48()*-0.0661661);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d350() {
   return (neuron0x902b128()*-0.437897);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d400() {
   return (neuron0x90395b0()*0.407165);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d4b0() {
   return (neuron0x90397b0()*-0.240594);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d560() {
   return (neuron0x90399b0()*-0.202685);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d610() {
   return (neuron0x9039bd8()*-0.259818);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d6c0() {
   return (neuron0x9039dd8()*-0.152542);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d898() {
   return (neuron0x902af48()*0.0930313);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d8c0() {
   return (neuron0x902b128()*0.077243);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d8e8() {
   return (neuron0x90395b0()*0.0658672);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d910() {
   return (neuron0x90397b0()*-0.444659);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d938() {
   return (neuron0x90399b0()*0.166673);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d960() {
   return (neuron0x9039bd8()*-0.284264);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d988() {
   return (neuron0x9039dd8()*-0.156955);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903dad8() {
   return (neuron0x902af48()*0.15947);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903db00() {
   return (neuron0x902b128()*0.144562);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903db28() {
   return (neuron0x90395b0()*-0.164213);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903db50() {
   return (neuron0x90397b0()*0.110242);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903db78() {
   return (neuron0x90399b0()*0.516754);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903dba0() {
   return (neuron0x9039bd8()*0.0602808);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903dbc8() {
   return (neuron0x9039dd8()*0.129239);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ddc0() {
   return (neuron0x902af48()*0.476266);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903dde8() {
   return (neuron0x902b128()*0.414945);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903de10() {
   return (neuron0x90395b0()*0.0951655);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c020() {
   return (neuron0x90397b0()*-0.169615);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c048() {
   return (neuron0x90399b0()*-0.374594);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c070() {
   return (neuron0x9039bd8()*0.23524);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c098() {
   return (neuron0x9039dd8()*-0.358594);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903c200() {
   return (neuron0x902af48()*0.315789);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e318() {
   return (neuron0x902b128()*-0.295506);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e340() {
   return (neuron0x90395b0()*0.336695);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e368() {
   return (neuron0x90397b0()*-0.287449);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e390() {
   return (neuron0x90399b0()*0.105588);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e3b8() {
   return (neuron0x9039bd8()*0.232193);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e3e0() {
   return (neuron0x9039dd8()*-0.288301);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e620() {
   return (neuron0x902af48()*0.35119);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e648() {
   return (neuron0x902b128()*-0.328753);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e670() {
   return (neuron0x90395b0()*-0.454234);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e698() {
   return (neuron0x90397b0()*0.0315455);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e6c0() {
   return (neuron0x90399b0()*-0.368771);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e6e8() {
   return (neuron0x9039bd8()*0.201357);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e710() {
   return (neuron0x9039dd8()*-0.0195248);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e950() {
   return (neuron0x902af48()*-0.217272);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e978() {
   return (neuron0x902b128()*-0.0631164);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e9a0() {
   return (neuron0x90395b0()*0.380247);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e9c8() {
   return (neuron0x90397b0()*0.111753);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e9f0() {
   return (neuron0x90399b0()*0.462539);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ea18() {
   return (neuron0x9039bd8()*0.283916);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ea40() {
   return (neuron0x9039dd8()*-0.188929);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ec80() {
   return (neuron0x902af48()*0.133672);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903eca8() {
   return (neuron0x902b128()*-0.152689);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ecd0() {
   return (neuron0x90395b0()*0.267334);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ecf8() {
   return (neuron0x90397b0()*-0.310912);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ed20() {
   return (neuron0x90399b0()*0.254454);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ed48() {
   return (neuron0x9039bd8()*0.238548);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903ed70() {
   return (neuron0x9039dd8()*-0.328979);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903efb0() {
   return (neuron0x902af48()*0.119756);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903efd8() {
   return (neuron0x902b128()*-0.267729);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f000() {
   return (neuron0x90395b0()*0.0328496);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f028() {
   return (neuron0x90397b0()*-0.270169);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f050() {
   return (neuron0x90399b0()*0.0732892);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f078() {
   return (neuron0x9039bd8()*-0.363643);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f0a0() {
   return (neuron0x9039dd8()*0.139804);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f2e0() {
   return (neuron0x902af48()*-0.0182518);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f308() {
   return (neuron0x902b128()*0.196991);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f330() {
   return (neuron0x90395b0()*0.0869708);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f358() {
   return (neuron0x90397b0()*0.152881);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f380() {
   return (neuron0x90399b0()*-0.620525);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f3a8() {
   return (neuron0x9039bd8()*0.14303);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f3d0() {
   return (neuron0x9039dd8()*-0.125448);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bf48() {
   return (neuron0x902af48()*-0.255492);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bf70() {
   return (neuron0x902b128()*-0.114114);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903bf98() {
   return (neuron0x90395b0()*-0.171774);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f718() {
   return (neuron0x90397b0()*0.223014);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f740() {
   return (neuron0x90399b0()*0.382511);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f768() {
   return (neuron0x9039bd8()*0.341583);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f790() {
   return (neuron0x9039dd8()*-0.37204);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f9d0() {
   return (neuron0x902af48()*-0.366726);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f9f8() {
   return (neuron0x902b128()*-0.301292);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fa20() {
   return (neuron0x90395b0()*-0.364445);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fa48() {
   return (neuron0x90397b0()*0.246479);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fa70() {
   return (neuron0x90399b0()*-0.306708);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fa98() {
   return (neuron0x9039bd8()*-0.186432);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fac0() {
   return (neuron0x9039dd8()*-0.260439);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fd00() {
   return (neuron0x902af48()*-0.109349);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fd28() {
   return (neuron0x902b128()*0.468346);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fd50() {
   return (neuron0x90395b0()*0.486914);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fd78() {
   return (neuron0x90397b0()*-0.261098);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fda0() {
   return (neuron0x90399b0()*0.331455);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fdc8() {
   return (neuron0x9039bd8()*-0.363177);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903fdf0() {
   return (neuron0x9039dd8()*0.184698);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040030() {
   return (neuron0x902af48()*-0.273396);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040058() {
   return (neuron0x902b128()*0.386201);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040080() {
   return (neuron0x90395b0()*0.0117538);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90400a8() {
   return (neuron0x90397b0()*0.337897);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90400d0() {
   return (neuron0x90399b0()*-0.534767);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90400f8() {
   return (neuron0x9039bd8()*-0.160836);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040120() {
   return (neuron0x9039dd8()*0.34935);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040360() {
   return (neuron0x902af48()*0.0747311);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040388() {
   return (neuron0x902b128()*-0.097778);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90403b0() {
   return (neuron0x90395b0()*-0.154442);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90403d8() {
   return (neuron0x90397b0()*0.204007);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040400() {
   return (neuron0x90399b0()*-0.35668);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040428() {
   return (neuron0x9039bd8()*-0.112193);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040450() {
   return (neuron0x9039dd8()*-0.179116);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040690() {
   return (neuron0x902af48()*-0.30111);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90406b8() {
   return (neuron0x902b128()*-0.227395);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90406e0() {
   return (neuron0x90395b0()*0.0764235);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040708() {
   return (neuron0x90397b0()*-0.334855);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040730() {
   return (neuron0x90399b0()*0.0729614);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040758() {
   return (neuron0x9039bd8()*-0.389778);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040780() {
   return (neuron0x9039dd8()*-0.185338);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90409c0() {
   return (neuron0x902af48()*-0.0391106);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d2c8() {
   return (neuron0x902b128()*0.0349697);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d2f0() {
   return (neuron0x90395b0()*0.0200608);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d318() {
   return (neuron0x90397b0()*-0.0910089);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d378() {
   return (neuron0x90399b0()*0.848198);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d3a0() {
   return (neuron0x9039bd8()*-0.00218788);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d3c8() {
   return (neuron0x9039dd8()*0.00108219);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d488() {
   return (neuron0x902af48()*-0.277651);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d5d0() {
   return (neuron0x902b128()*0.088565);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d520() {
   return (neuron0x90395b0()*0.219091);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d638() {
   return (neuron0x90397b0()*0.226285);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d660() {
   return (neuron0x90399b0()*0.292678);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d688() {
   return (neuron0x9039bd8()*0.0702776);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903d6e8() {
   return (neuron0x9039dd8()*0.168959);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041400() {
   return (neuron0x902af48()*0.383491);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041428() {
   return (neuron0x902b128()*-0.20811);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041450() {
   return (neuron0x90395b0()*-0.0916726);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041478() {
   return (neuron0x90397b0()*0.465735);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90414a0() {
   return (neuron0x90399b0()*0.344558);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90414c8() {
   return (neuron0x9039bd8()*-0.26524);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90414f0() {
   return (neuron0x9039dd8()*-0.347718);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041718() {
   return (neuron0x902af48()*-0.180176);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041740() {
   return (neuron0x902b128()*-0.146323);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041768() {
   return (neuron0x90395b0()*-0.0894617);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041790() {
   return (neuron0x90397b0()*-0.151319);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90417b8() {
   return (neuron0x90399b0()*0.248048);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90417e0() {
   return (neuron0x9039bd8()*-0.143503);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041808() {
   return (neuron0x9039dd8()*0.125952);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041a30() {
   return (neuron0x902af48()*-0.121472);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041a58() {
   return (neuron0x902b128()*0.280794);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041a80() {
   return (neuron0x90395b0()*0.176369);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041aa8() {
   return (neuron0x90397b0()*-0.154399);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041ad0() {
   return (neuron0x90399b0()*0.354868);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041af8() {
   return (neuron0x9039bd8()*0.0155717);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041b20() {
   return (neuron0x9039dd8()*0.0949381);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041d60() {
   return (neuron0x902af48()*0.449954);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041d88() {
   return (neuron0x902b128()*0.00426662);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041db0() {
   return (neuron0x90395b0()*-0.0913215);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041dd8() {
   return (neuron0x90397b0()*0.378251);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041e00() {
   return (neuron0x90399b0()*-0.143718);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903de38() {
   return (neuron0x9039bd8()*0.419788);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903de60() {
   return (neuron0x9039dd8()*-0.188696);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e0a0() {
   return (neuron0x902af48()*0.293552);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e0c8() {
   return (neuron0x902b128()*0.499837);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e0f0() {
   return (neuron0x90395b0()*-0.0399676);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e118() {
   return (neuron0x90397b0()*-0.28741);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e140() {
   return (neuron0x90399b0()*-0.151056);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e168() {
   return (neuron0x9039bd8()*-0.203315);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e190() {
   return (neuron0x9039dd8()*0.351938);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903e218() {
   return (neuron0x902af48()*0.393101);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90427e8() {
   return (neuron0x902b128()*-0.433781);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042810() {
   return (neuron0x90395b0()*0.0700129);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042838() {
   return (neuron0x90397b0()*0.372298);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042860() {
   return (neuron0x90399b0()*0.32006);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042888() {
   return (neuron0x9039bd8()*-0.338964);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90428b0() {
   return (neuron0x9039dd8()*0.148775);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042af0() {
   return (neuron0x902af48()*-0.352825);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042b18() {
   return (neuron0x902b128()*0.21085);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042b40() {
   return (neuron0x90395b0()*0.0942473);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042b68() {
   return (neuron0x90397b0()*0.0860445);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042b90() {
   return (neuron0x90399b0()*0.294308);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042bb8() {
   return (neuron0x9039bd8()*0.133022);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042be0() {
   return (neuron0x9039dd8()*-0.222781);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042e20() {
   return (neuron0x902af48()*0.124567);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042e48() {
   return (neuron0x902b128()*0.132821);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042e70() {
   return (neuron0x90395b0()*0.302248);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042e98() {
   return (neuron0x90397b0()*-0.421465);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042ec0() {
   return (neuron0x90399b0()*-0.0979774);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042ee8() {
   return (neuron0x9039bd8()*-0.316956);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042f10() {
   return (neuron0x9039dd8()*0.0244314);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043150() {
   return (neuron0x902af48()*-0.0963179);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043178() {
   return (neuron0x902b128()*-0.0443572);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90431a0() {
   return (neuron0x90395b0()*0.0869864);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90431c8() {
   return (neuron0x90397b0()*-0.258844);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90431f0() {
   return (neuron0x90399b0()*-0.626849);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043218() {
   return (neuron0x9039bd8()*-0.0101974);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043240() {
   return (neuron0x9039dd8()*0.023132);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043480() {
   return (neuron0x902af48()*-0.264535);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90434a8() {
   return (neuron0x902b128()*0.0450287);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90434d0() {
   return (neuron0x90395b0()*-0.491162);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90434f8() {
   return (neuron0x90397b0()*-0.0810383);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043520() {
   return (neuron0x90399b0()*-0.313508);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043548() {
   return (neuron0x9039bd8()*-0.20695);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043570() {
   return (neuron0x9039dd8()*0.457674);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90437b0() {
   return (neuron0x902af48()*0.0412954);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90437d8() {
   return (neuron0x902b128()*-0.188022);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043800() {
   return (neuron0x90395b0()*0.217585);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043828() {
   return (neuron0x90397b0()*0.175702);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043850() {
   return (neuron0x90399b0()*-0.0775272);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043878() {
   return (neuron0x9039bd8()*-0.0282103);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90438a0() {
   return (neuron0x9039dd8()*-0.132967);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043ae0() {
   return (neuron0x902af48()*-0.0636655);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043b08() {
   return (neuron0x902b128()*-0.447231);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043b30() {
   return (neuron0x90395b0()*-0.125889);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043b58() {
   return (neuron0x90397b0()*0.304599);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043b80() {
   return (neuron0x90399b0()*-0.0510431);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043ba8() {
   return (neuron0x9039bd8()*0.0595159);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043bd0() {
   return (neuron0x9039dd8()*0.202866);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043e10() {
   return (neuron0x902af48()*0.358098);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043e38() {
   return (neuron0x902b128()*-0.249143);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043e60() {
   return (neuron0x90395b0()*0.0793049);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043e88() {
   return (neuron0x90397b0()*-0.467169);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043eb0() {
   return (neuron0x90399b0()*0.379747);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043ed8() {
   return (neuron0x9039bd8()*0.127662);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9043f00() {
   return (neuron0x9039dd8()*-0.12319);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044140() {
   return (neuron0x902af48()*-0.0870576);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044168() {
   return (neuron0x902b128()*-0.181443);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044190() {
   return (neuron0x90395b0()*0.117311);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90441b8() {
   return (neuron0x90397b0()*0.28861);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90441e0() {
   return (neuron0x90399b0()*-0.570595);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044208() {
   return (neuron0x9039bd8()*-0.0211323);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044230() {
   return (neuron0x9039dd8()*0.0107105);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044470() {
   return (neuron0x902af48()*-0.105701);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044498() {
   return (neuron0x902b128()*-0.139803);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90444c0() {
   return (neuron0x90395b0()*-0.264139);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90444e8() {
   return (neuron0x90397b0()*-0.157693);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044510() {
   return (neuron0x90399b0()*-0.16374);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044538() {
   return (neuron0x9039bd8()*-0.153076);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044560() {
   return (neuron0x9039dd8()*0.275707);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90447a0() {
   return (neuron0x902af48()*-0.279546);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90447c8() {
   return (neuron0x902b128()*0.426129);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90447f0() {
   return (neuron0x90395b0()*-0.114985);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044818() {
   return (neuron0x90397b0()*0.311666);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044840() {
   return (neuron0x90399b0()*-0.260228);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044868() {
   return (neuron0x9039bd8()*0.0241303);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044890() {
   return (neuron0x9039dd8()*-0.19561);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044ad0() {
   return (neuron0x902af48()*0.463571);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044af8() {
   return (neuron0x902b128()*0.162933);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044b20() {
   return (neuron0x90395b0()*-0.0207406);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044b48() {
   return (neuron0x90397b0()*-0.112796);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044b70() {
   return (neuron0x90399b0()*0.457714);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044b98() {
   return (neuron0x9039bd8()*-0.204807);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044bc0() {
   return (neuron0x9039dd8()*-0.171539);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044e00() {
   return (neuron0x902af48()*0.200767);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044e28() {
   return (neuron0x902b128()*0.0620494);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044e50() {
   return (neuron0x90395b0()*0.454396);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044e78() {
   return (neuron0x90397b0()*0.232393);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044ea0() {
   return (neuron0x90399b0()*-0.258288);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044ec8() {
   return (neuron0x9039bd8()*0.327249);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9044ef0() {
   return (neuron0x9039dd8()*-0.441);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045130() {
   return (neuron0x902af48()*0.0401506);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045158() {
   return (neuron0x902b128()*-0.0534429);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045180() {
   return (neuron0x90395b0()*-0.231789);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90451a8() {
   return (neuron0x90397b0()*-0.531349);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90451d0() {
   return (neuron0x90399b0()*-0.350005);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90451f8() {
   return (neuron0x9039bd8()*-0.260327);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045220() {
   return (neuron0x9039dd8()*0.261928);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045460() {
   return (neuron0x902af48()*0.474213);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045488() {
   return (neuron0x902b128()*-0.2907);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90454b0() {
   return (neuron0x90395b0()*-0.375406);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90454d8() {
   return (neuron0x90397b0()*-0.236632);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045500() {
   return (neuron0x90399b0()*0.136297);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045528() {
   return (neuron0x9039bd8()*0.373243);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045550() {
   return (neuron0x9039dd8()*0.0273491);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045790() {
   return (neuron0x902af48()*-0.211801);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90457b8() {
   return (neuron0x902b128()*-0.186242);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90457e0() {
   return (neuron0x90395b0()*-0.453664);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045808() {
   return (neuron0x90397b0()*-0.247097);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045830() {
   return (neuron0x90399b0()*0.169116);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045858() {
   return (neuron0x9039bd8()*-0.0947727);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045880() {
   return (neuron0x9039dd8()*-0.294373);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045ac0() {
   return (neuron0x902af48()*0.0468044);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045ae8() {
   return (neuron0x902b128()*-0.351971);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045b10() {
   return (neuron0x90395b0()*-0.341486);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045b38() {
   return (neuron0x90397b0()*0.262687);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045b60() {
   return (neuron0x90399b0()*-0.015086);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045b88() {
   return (neuron0x9039bd8()*0.267032);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045bb0() {
   return (neuron0x9039dd8()*0.146576);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045df0() {
   return (neuron0x902af48()*0.167332);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045e18() {
   return (neuron0x902b128()*0.0152358);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045e40() {
   return (neuron0x90395b0()*-0.0547622);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045e68() {
   return (neuron0x90397b0()*-0.101596);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045e90() {
   return (neuron0x90399b0()*-0.643507);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045eb8() {
   return (neuron0x9039bd8()*0.0141708);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9045ee0() {
   return (neuron0x9039dd8()*-0.00793899);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046120() {
   return (neuron0x902af48()*0.423683);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046148() {
   return (neuron0x902b128()*-0.223379);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046170() {
   return (neuron0x90395b0()*-0.13532);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046198() {
   return (neuron0x90397b0()*0.0641146);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90461c0() {
   return (neuron0x90399b0()*-0.185491);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90461e8() {
   return (neuron0x9039bd8()*-0.358833);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046210() {
   return (neuron0x9039dd8()*0.44202);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f610() {
   return (neuron0x902af48()*0.301824);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f638() {
   return (neuron0x902b128()*-0.514129);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f660() {
   return (neuron0x90395b0()*0.253384);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f688() {
   return (neuron0x90397b0()*-0.395412);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f6b0() {
   return (neuron0x90399b0()*-0.294665);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x903f6d8() {
   return (neuron0x9039bd8()*0.256808);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046658() {
   return (neuron0x9039dd8()*0.487799);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046880() {
   return (neuron0x902af48()*0.352149);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90468a8() {
   return (neuron0x902b128()*0.497797);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90468d0() {
   return (neuron0x90395b0()*0.0388257);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90468f8() {
   return (neuron0x90397b0()*-0.262172);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046920() {
   return (neuron0x90399b0()*0.275209);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046948() {
   return (neuron0x9039bd8()*-0.134514);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046970() {
   return (neuron0x9039dd8()*0.210376);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046bb0() {
   return (neuron0x902af48()*0.306544);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046bd8() {
   return (neuron0x902b128()*-0.184011);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046c00() {
   return (neuron0x90395b0()*-0.329841);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046c28() {
   return (neuron0x90397b0()*-0.298897);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046c50() {
   return (neuron0x90399b0()*-0.226877);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046c78() {
   return (neuron0x9039bd8()*-0.219117);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046ca0() {
   return (neuron0x9039dd8()*0.437484);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046ee0() {
   return (neuron0x902af48()*-0.157208);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046f08() {
   return (neuron0x902b128()*-0.270808);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046f30() {
   return (neuron0x90395b0()*-0.272515);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046f58() {
   return (neuron0x90397b0()*-0.44419);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046f80() {
   return (neuron0x90399b0()*0.0512168);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046fa8() {
   return (neuron0x9039bd8()*0.0909207);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9046fd0() {
   return (neuron0x9039dd8()*-0.277374);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047210() {
   return (neuron0x902af48()*0.0137264);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047238() {
   return (neuron0x902b128()*-0.319764);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047260() {
   return (neuron0x90395b0()*-0.241551);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047288() {
   return (neuron0x90397b0()*0.215297);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90472b0() {
   return (neuron0x90399b0()*-0.293784);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90472d8() {
   return (neuron0x9039bd8()*0.284372);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047300() {
   return (neuron0x9039dd8()*0.191642);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047540() {
   return (neuron0x902af48()*-0.0953877);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047568() {
   return (neuron0x902b128()*0.1907);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047590() {
   return (neuron0x90395b0()*0.20205);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90475b8() {
   return (neuron0x90397b0()*0.229664);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90475e0() {
   return (neuron0x90399b0()*-0.106547);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047608() {
   return (neuron0x9039bd8()*-0.211884);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047630() {
   return (neuron0x9039dd8()*-0.259897);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9047870() {
   return (neuron0x902af48()*-0.363533);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90409e8() {
   return (neuron0x902b128()*-0.100035);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040a10() {
   return (neuron0x90395b0()*0.295711);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040a38() {
   return (neuron0x90397b0()*-0.30551);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040c68() {
   return (neuron0x90399b0()*0.462393);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040c90() {
   return (neuron0x9039bd8()*-0.0306335);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9040ec0() {
   return (neuron0x9039dd8()*-0.240728);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90482c0() {
   return (neuron0x902af48()*0.13804);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90482e8() {
   return (neuron0x902b128()*-0.327313);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048310() {
   return (neuron0x90395b0()*0.178885);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048338() {
   return (neuron0x90397b0()*-0.365923);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048360() {
   return (neuron0x90399b0()*0.566506);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048388() {
   return (neuron0x9039bd8()*0.0132511);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90483b0() {
   return (neuron0x9039dd8()*0.196987);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90485d8() {
   return (neuron0x902af48()*0.431207);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048600() {
   return (neuron0x902b128()*0.301474);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048628() {
   return (neuron0x90395b0()*-0.208906);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048650() {
   return (neuron0x90397b0()*-0.0194719);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048678() {
   return (neuron0x90399b0()*0.13876);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90486a0() {
   return (neuron0x9039bd8()*0.304219);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90486c8() {
   return (neuron0x9039dd8()*0.251044);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048908() {
   return (neuron0x902af48()*0.0873555);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048930() {
   return (neuron0x902b128()*0.0621293);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048958() {
   return (neuron0x90395b0()*-0.105902);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048980() {
   return (neuron0x90397b0()*0.115153);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90489a8() {
   return (neuron0x90399b0()*-0.0933605);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90489d0() {
   return (neuron0x9039bd8()*-0.399637);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90489f8() {
   return (neuron0x9039dd8()*0.445387);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048c38() {
   return (neuron0x902af48()*-0.0354651);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048c60() {
   return (neuron0x902b128()*0.40846);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048c88() {
   return (neuron0x90395b0()*0.142724);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048cb0() {
   return (neuron0x90397b0()*-0.390876);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048cd8() {
   return (neuron0x90399b0()*-0.154354);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048d00() {
   return (neuron0x9039bd8()*-0.0452174);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048d28() {
   return (neuron0x9039dd8()*-0.0937427);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048f68() {
   return (neuron0x902af48()*-0.229036);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048f90() {
   return (neuron0x902b128()*0.260382);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048fb8() {
   return (neuron0x90395b0()*-0.197378);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9048fe0() {
   return (neuron0x90397b0()*0.300563);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049008() {
   return (neuron0x90399b0()*0.207531);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049030() {
   return (neuron0x9039bd8()*-0.281912);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049058() {
   return (neuron0x9039dd8()*0.220866);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049298() {
   return (neuron0x902af48()*0.0616386);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90492c0() {
   return (neuron0x902b128()*-0.260093);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90492e8() {
   return (neuron0x90395b0()*0.0193726);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049310() {
   return (neuron0x90397b0()*-0.347778);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049338() {
   return (neuron0x90399b0()*0.193584);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049360() {
   return (neuron0x9039bd8()*-0.266241);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049388() {
   return (neuron0x9039dd8()*0.130489);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90495c8() {
   return (neuron0x902af48()*-0.0747332);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90495f0() {
   return (neuron0x902b128()*-0.243138);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049618() {
   return (neuron0x90395b0()*-0.155851);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049640() {
   return (neuron0x90397b0()*-0.307345);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049668() {
   return (neuron0x90399b0()*0.216516);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049690() {
   return (neuron0x9039bd8()*0.100657);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90496b8() {
   return (neuron0x9039dd8()*-0.238108);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90498f8() {
   return (neuron0x902af48()*-0.116062);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049920() {
   return (neuron0x902b128()*0.0866545);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049948() {
   return (neuron0x90395b0()*0.0825671);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049970() {
   return (neuron0x90397b0()*-0.227281);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049998() {
   return (neuron0x90399b0()*0.923815);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90499c0() {
   return (neuron0x9039bd8()*-0.00450867);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90499e8() {
   return (neuron0x9039dd8()*-0.00234752);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049c28() {
   return (neuron0x902af48()*-0.0788711);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049c50() {
   return (neuron0x902b128()*0.0103015);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049c78() {
   return (neuron0x90395b0()*-0.281391);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049ca0() {
   return (neuron0x90397b0()*-0.174194);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049cc8() {
   return (neuron0x90399b0()*-0.0851945);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049cf0() {
   return (neuron0x9039bd8()*0.31383);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049d18() {
   return (neuron0x9039dd8()*0.120983);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049f58() {
   return (neuron0x902af48()*-0.315134);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9049f80() {
   return (neuron0x902b128()*-0.173588);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041e28() {
   return (neuron0x90395b0()*0.399966);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041e50() {
   return (neuron0x90397b0()*-0.220643);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041e78() {
   return (neuron0x90399b0()*-0.397595);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041ea0() {
   return (neuron0x9039bd8()*-0.207462);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9041ec8() {
   return (neuron0x9039dd8()*0.0630907);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042108() {
   return (neuron0x902af48()*-0.399018);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042130() {
   return (neuron0x902b128()*0.387776);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042158() {
   return (neuron0x90395b0()*0.365883);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042180() {
   return (neuron0x90397b0()*-0.406683);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90421a8() {
   return (neuron0x90399b0()*0.0503864);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90421d0() {
   return (neuron0x9039bd8()*-0.467948);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90421f8() {
   return (neuron0x9039dd8()*0.211816);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042438() {
   return (neuron0x902af48()*-0.229402);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042460() {
   return (neuron0x902b128()*-0.0507907);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042488() {
   return (neuron0x90395b0()*0.290589);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90424b0() {
   return (neuron0x90397b0()*0.156437);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90424d8() {
   return (neuron0x90399b0()*-0.411436);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042500() {
   return (neuron0x9039bd8()*-0.192013);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9042528() {
   return (neuron0x9039dd8()*-0.386352);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90425f8() {
   return (neuron0x902af48()*0.112189);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b120() {
   return (neuron0x902b128()*-0.0219057);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b148() {
   return (neuron0x90395b0()*-0.0763737);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b170() {
   return (neuron0x90397b0()*0.0356059);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b198() {
   return (neuron0x90399b0()*-2.07086);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b1c0() {
   return (neuron0x9039bd8()*0.00279759);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b1e8() {
   return (neuron0x9039dd8()*-0.000723466);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b428() {
   return (neuron0x902af48()*-0.0623166);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b450() {
   return (neuron0x902b128()*-0.310996);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b478() {
   return (neuron0x90395b0()*0.395963);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b4a0() {
   return (neuron0x90397b0()*0.477741);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b4c8() {
   return (neuron0x90399b0()*-0.348099);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b4f0() {
   return (neuron0x9039bd8()*-0.212793);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b518() {
   return (neuron0x9039dd8()*0.179026);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b758() {
   return (neuron0x902af48()*-0.285839);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b780() {
   return (neuron0x902b128()*-0.209042);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b7a8() {
   return (neuron0x90395b0()*0.0831918);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b7d0() {
   return (neuron0x90397b0()*-0.318197);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b7f8() {
   return (neuron0x90399b0()*-0.30096);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b820() {
   return (neuron0x9039bd8()*0.0805691);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904b848() {
   return (neuron0x9039dd8()*0.108161);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ba88() {
   return (neuron0x902af48()*0.222464);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bab0() {
   return (neuron0x902b128()*0.345001);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bad8() {
   return (neuron0x90395b0()*0.0211674);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bb00() {
   return (neuron0x90397b0()*0.323966);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bb28() {
   return (neuron0x90399b0()*0.330769);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bb50() {
   return (neuron0x9039bd8()*0.184895);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bb78() {
   return (neuron0x9039dd8()*0.163264);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bdb8() {
   return (neuron0x902af48()*-0.0924925);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bde0() {
   return (neuron0x902b128()*0.0107842);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904be08() {
   return (neuron0x90395b0()*-0.0337437);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904be30() {
   return (neuron0x90397b0()*-0.426367);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904be58() {
   return (neuron0x90399b0()*-0.141047);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904be80() {
   return (neuron0x9039bd8()*-0.237421);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904bea8() {
   return (neuron0x9039dd8()*0.0803011);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c0e8() {
   return (neuron0x902af48()*-0.055337);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c110() {
   return (neuron0x902b128()*-0.144662);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c138() {
   return (neuron0x90395b0()*0.210531);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c160() {
   return (neuron0x90397b0()*0.161112);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c188() {
   return (neuron0x90399b0()*-0.0638564);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c1b0() {
   return (neuron0x9039bd8()*-0.374874);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c1d8() {
   return (neuron0x9039dd8()*-0.138561);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c418() {
   return (neuron0x902af48()*-0.332595);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c440() {
   return (neuron0x902b128()*0.0412558);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c468() {
   return (neuron0x90395b0()*-0.17354);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c490() {
   return (neuron0x90397b0()*-0.246723);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c4b8() {
   return (neuron0x90399b0()*0.00939467);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c4e0() {
   return (neuron0x9039bd8()*0.222705);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c508() {
   return (neuron0x9039dd8()*-0.481609);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c748() {
   return (neuron0x902af48()*0.0942617);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c770() {
   return (neuron0x902b128()*0.309222);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c798() {
   return (neuron0x90395b0()*0.0850683);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c7c0() {
   return (neuron0x90397b0()*-0.423918);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c7e8() {
   return (neuron0x90399b0()*-0.204841);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c810() {
   return (neuron0x9039bd8()*-0.182269);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904c838() {
   return (neuron0x9039dd8()*0.23418);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ca78() {
   return (neuron0x902af48()*0.0462389);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904caa0() {
   return (neuron0x902b128()*-0.456087);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904cac8() {
   return (neuron0x90395b0()*-0.11299);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904caf0() {
   return (neuron0x90397b0()*0.240191);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904cb18() {
   return (neuron0x90399b0()*-0.552187);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904cb40() {
   return (neuron0x9039bd8()*0.0498687);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904cb68() {
   return (neuron0x9039dd8()*-0.0661807);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904cda8() {
   return (neuron0x902af48()*-0.269593);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904cdd0() {
   return (neuron0x902b128()*0.0832554);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904cdf8() {
   return (neuron0x90395b0()*-0.230923);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ce20() {
   return (neuron0x90397b0()*-0.499697);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ce48() {
   return (neuron0x90399b0()*0.195654);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ce70() {
   return (neuron0x9039bd8()*-0.248276);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ce98() {
   return (neuron0x9039dd8()*0.0682419);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d0d8() {
   return (neuron0x902af48()*0.18037);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d100() {
   return (neuron0x902b128()*-0.0517638);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d128() {
   return (neuron0x90395b0()*0.21625);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d150() {
   return (neuron0x90397b0()*-0.453257);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d178() {
   return (neuron0x90399b0()*-0.408603);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d1a0() {
   return (neuron0x9039bd8()*0.279207);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d1c8() {
   return (neuron0x9039dd8()*-0.403993);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d408() {
   return (neuron0x902af48()*-0.038734);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d430() {
   return (neuron0x902b128()*-0.470534);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d458() {
   return (neuron0x90395b0()*0.280766);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d480() {
   return (neuron0x90397b0()*0.285828);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d4a8() {
   return (neuron0x90399b0()*-0.219525);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d4d0() {
   return (neuron0x9039bd8()*0.214197);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d4f8() {
   return (neuron0x9039dd8()*0.153963);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d738() {
   return (neuron0x902af48()*-0.21231);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d760() {
   return (neuron0x902b128()*-0.16362);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d788() {
   return (neuron0x90395b0()*0.196363);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d7b0() {
   return (neuron0x90397b0()*0.109448);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d7d8() {
   return (neuron0x90399b0()*0.643663);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d800() {
   return (neuron0x9039bd8()*0.00890226);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904d828() {
   return (neuron0x9039dd8()*0.00824541);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904da68() {
   return (neuron0x902af48()*0.324842);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904da90() {
   return (neuron0x902b128()*0.342715);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904dab8() {
   return (neuron0x90395b0()*-0.293931);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904dae0() {
   return (neuron0x90397b0()*0.0222739);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904db08() {
   return (neuron0x90399b0()*0.0372113);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904db30() {
   return (neuron0x9039bd8()*0.15273);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904db58() {
   return (neuron0x9039dd8()*0.00672919);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904dd98() {
   return (neuron0x902af48()*0.0183871);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ddc0() {
   return (neuron0x902b128()*0.250148);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904dde8() {
   return (neuron0x90395b0()*-0.332332);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904de10() {
   return (neuron0x90397b0()*0.0867464);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904de38() {
   return (neuron0x90399b0()*-0.0423932);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904de60() {
   return (neuron0x9039bd8()*-0.0037474);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904de88() {
   return (neuron0x9039dd8()*-0.251513);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e0c8() {
   return (neuron0x902af48()*-0.058758);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e0f0() {
   return (neuron0x902b128()*0.110374);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e118() {
   return (neuron0x90395b0()*0.0878128);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e140() {
   return (neuron0x90397b0()*0.114934);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e168() {
   return (neuron0x90399b0()*0.54304);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e190() {
   return (neuron0x9039bd8()*0.0268952);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e1b8() {
   return (neuron0x9039dd8()*-0.0302356);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e3f8() {
   return (neuron0x902af48()*-0.210938);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e420() {
   return (neuron0x902b128()*-0.211146);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e448() {
   return (neuron0x90395b0()*0.295282);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e470() {
   return (neuron0x90397b0()*0.156848);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e498() {
   return (neuron0x90399b0()*-0.0918452);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e4c0() {
   return (neuron0x9039bd8()*-0.0390482);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e4e8() {
   return (neuron0x9039dd8()*0.133987);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e728() {
   return (neuron0x902af48()*0.0361553);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e750() {
   return (neuron0x902b128()*0.021122);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e778() {
   return (neuron0x90395b0()*-0.112039);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e7a0() {
   return (neuron0x90397b0()*-0.432462);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e7c8() {
   return (neuron0x90399b0()*0.0688316);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e7f0() {
   return (neuron0x9039bd8()*-0.366579);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904e818() {
   return (neuron0x9039dd8()*0.170107);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ea58() {
   return (neuron0x902af48()*-0.252903);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ea80() {
   return (neuron0x902b128()*-0.323934);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904eaa8() {
   return (neuron0x90395b0()*0.100827);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ead0() {
   return (neuron0x90397b0()*0.236272);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904eaf8() {
   return (neuron0x90399b0()*-0.0218303);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904eb20() {
   return (neuron0x9039bd8()*0.2408);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904eb48() {
   return (neuron0x9039dd8()*0.113367);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ed88() {
   return (neuron0x902af48()*-0.337171);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904edb0() {
   return (neuron0x902b128()*0.227588);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904edd8() {
   return (neuron0x90395b0()*0.0456226);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ee00() {
   return (neuron0x90397b0()*-0.237454);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ee28() {
   return (neuron0x90399b0()*0.214558);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ee50() {
   return (neuron0x9039bd8()*0.0561856);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904ee78() {
   return (neuron0x9039dd8()*0.243163);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f0b8() {
   return (neuron0x902af48()*0.0636521);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f0e0() {
   return (neuron0x902b128()*0.101905);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f108() {
   return (neuron0x90395b0()*0.225003);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f130() {
   return (neuron0x90397b0()*0.370735);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f158() {
   return (neuron0x90399b0()*0.296069);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f180() {
   return (neuron0x9039bd8()*0.0278076);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f1a8() {
   return (neuron0x9039dd8()*0.169737);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f3e8() {
   return (neuron0x902af48()*-0.305307);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f410() {
   return (neuron0x902b128()*0.0895484);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f438() {
   return (neuron0x90395b0()*0.30189);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f460() {
   return (neuron0x90397b0()*-0.158024);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f488() {
   return (neuron0x90399b0()*-0.0860129);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f4b0() {
   return (neuron0x9039bd8()*-0.0491042);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f4d8() {
   return (neuron0x9039dd8()*-0.406217);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f718() {
   return (neuron0x902af48()*0.247818);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f740() {
   return (neuron0x902b128()*-0.432754);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f768() {
   return (neuron0x90395b0()*-0.0226278);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f790() {
   return (neuron0x90397b0()*-0.474025);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f7b8() {
   return (neuron0x90399b0()*0.029041);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f7e0() {
   return (neuron0x9039bd8()*-0.251952);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904f808() {
   return (neuron0x9039dd8()*0.155768);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fa48() {
   return (neuron0x902af48()*0.129272);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fa70() {
   return (neuron0x902b128()*-0.265503);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fa98() {
   return (neuron0x90395b0()*-0.0930003);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fac0() {
   return (neuron0x90397b0()*0.0806043);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fae8() {
   return (neuron0x90399b0()*-0.0801428);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fb10() {
   return (neuron0x9039bd8()*-0.304084);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fb38() {
   return (neuron0x9039dd8()*-0.332252);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fd78() {
   return (neuron0x902af48()*-0.0344181);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fda0() {
   return (neuron0x902b128()*0.358185);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fdc8() {
   return (neuron0x90395b0()*0.173591);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fdf0() {
   return (neuron0x90397b0()*-0.11572);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fe18() {
   return (neuron0x90399b0()*-0.0611224);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fe40() {
   return (neuron0x9039bd8()*0.192805);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x904fe68() {
   return (neuron0x9039dd8()*-0.220926);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90500a8() {
   return (neuron0x902af48()*-0.304264);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90500d0() {
   return (neuron0x902b128()*0.0871603);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90500f8() {
   return (neuron0x90395b0()*0.280011);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050120() {
   return (neuron0x90397b0()*-0.0919519);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050148() {
   return (neuron0x90399b0()*1.58679);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050170() {
   return (neuron0x9039bd8()*-0.0018552);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050198() {
   return (neuron0x9039dd8()*0.00480305);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90503d8() {
   return (neuron0x902af48()*-0.19276);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050400() {
   return (neuron0x902b128()*0.0707117);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050428() {
   return (neuron0x90395b0()*0.297785);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050450() {
   return (neuron0x90397b0()*0.182759);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050478() {
   return (neuron0x90399b0()*-0.649037);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90504a0() {
   return (neuron0x9039bd8()*-0.0769906);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90504c8() {
   return (neuron0x9039dd8()*0.0577698);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050708() {
   return (neuron0x902af48()*-0.0219492);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050730() {
   return (neuron0x902b128()*0.31869);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050758() {
   return (neuron0x90395b0()*0.429075);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050780() {
   return (neuron0x90397b0()*0.275867);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90507a8() {
   return (neuron0x90399b0()*0.109994);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90507d0() {
   return (neuron0x9039bd8()*0.426787);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90507f8() {
   return (neuron0x9039dd8()*-0.316599);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050a38() {
   return (neuron0x902af48()*0.39063);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050a60() {
   return (neuron0x902b128()*0.162614);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050a88() {
   return (neuron0x90395b0()*-0.00512762);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050ab0() {
   return (neuron0x90397b0()*-0.0606151);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050ad8() {
   return (neuron0x90399b0()*-0.0454849);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050b00() {
   return (neuron0x9039bd8()*-0.374241);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050b28() {
   return (neuron0x9039dd8()*0.0647878);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050d68() {
   return (neuron0x902af48()*0.195017);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050d90() {
   return (neuron0x902b128()*0.181613);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050db8() {
   return (neuron0x90395b0()*0.280401);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050de0() {
   return (neuron0x90397b0()*-0.282503);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050e08() {
   return (neuron0x90399b0()*0.26925);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050e30() {
   return (neuron0x9039bd8()*-0.424653);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9050e58() {
   return (neuron0x9039dd8()*-0.172661);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051098() {
   return (neuron0x902af48()*0.15869);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90510c0() {
   return (neuron0x902b128()*0.161431);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90510e8() {
   return (neuron0x90395b0()*0.273824);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051110() {
   return (neuron0x90397b0()*-0.432464);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051138() {
   return (neuron0x90399b0()*-0.0584333);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051160() {
   return (neuron0x9039bd8()*-0.35506);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051188() {
   return (neuron0x9039dd8()*0.111628);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90513c8() {
   return (neuron0x902af48()*-0.394229);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90513f0() {
   return (neuron0x902b128()*0.381741);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051418() {
   return (neuron0x90395b0()*-0.268583);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051440() {
   return (neuron0x90397b0()*0.467612);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051468() {
   return (neuron0x90399b0()*-0.196115);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051490() {
   return (neuron0x9039bd8()*-0.369921);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90514b8() {
   return (neuron0x9039dd8()*-0.0283627);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90516f8() {
   return (neuron0x902af48()*0.462983);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051720() {
   return (neuron0x902b128()*-0.276446);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051748() {
   return (neuron0x90395b0()*-0.398734);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051770() {
   return (neuron0x90397b0()*-0.135199);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051798() {
   return (neuron0x90399b0()*-0.00448741);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90517c0() {
   return (neuron0x9039bd8()*0.0222573);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90517e8() {
   return (neuron0x9039dd8()*0.00449248);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051a28() {
   return (neuron0x902af48()*0.199585);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051a50() {
   return (neuron0x902b128()*0.151869);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051a78() {
   return (neuron0x90395b0()*-0.0352133);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051aa0() {
   return (neuron0x90397b0()*0.272514);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051ac8() {
   return (neuron0x90399b0()*-0.318843);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051af0() {
   return (neuron0x9039bd8()*0.288058);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051b18() {
   return (neuron0x9039dd8()*-0.158864);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051d58() {
   return (neuron0x902af48()*-0.20653);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051d80() {
   return (neuron0x902b128()*-0.00518464);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051da8() {
   return (neuron0x90395b0()*-0.0765644);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051dd0() {
   return (neuron0x90397b0()*-0.035907);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051df8() {
   return (neuron0x90399b0()*-0.115372);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051e20() {
   return (neuron0x9039bd8()*0.410635);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9051e48() {
   return (neuron0x9039dd8()*-0.226833);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052088() {
   return (neuron0x902af48()*0.204221);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90520b0() {
   return (neuron0x902b128()*0.343584);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90520d8() {
   return (neuron0x90395b0()*0.0387248);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052100() {
   return (neuron0x90397b0()*-0.43709);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052128() {
   return (neuron0x90399b0()*-0.184062);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052150() {
   return (neuron0x9039bd8()*-0.0333857);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052178() {
   return (neuron0x9039dd8()*0.459288);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90523b8() {
   return (neuron0x902af48()*0.324852);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90523e0() {
   return (neuron0x902b128()*-0.183918);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052408() {
   return (neuron0x90395b0()*-0.200211);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052430() {
   return (neuron0x90397b0()*-0.0870668);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052458() {
   return (neuron0x90399b0()*0.407839);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052480() {
   return (neuron0x9039bd8()*0.433957);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90524a8() {
   return (neuron0x9039dd8()*0.312905);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90526e8() {
   return (neuron0x902af48()*0.129166);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052710() {
   return (neuron0x902b128()*-0.299061);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052738() {
   return (neuron0x90395b0()*-0.0241242);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052760() {
   return (neuron0x90397b0()*0.169222);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052788() {
   return (neuron0x90399b0()*-0.0244847);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90527b0() {
   return (neuron0x9039bd8()*-0.080827);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90527d8() {
   return (neuron0x9039dd8()*0.148962);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052a18() {
   return (neuron0x902af48()*-0.451162);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052a40() {
   return (neuron0x902b128()*-0.0580775);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052a68() {
   return (neuron0x90395b0()*-0.258468);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052a90() {
   return (neuron0x90397b0()*-0.216146);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052ab8() {
   return (neuron0x90399b0()*-0.0177103);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052ae0() {
   return (neuron0x9039bd8()*0.409668);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052b08() {
   return (neuron0x9039dd8()*-0.357196);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052d48() {
   return (neuron0x902af48()*0.136902);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052d70() {
   return (neuron0x902b128()*0.400717);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052d98() {
   return (neuron0x90395b0()*0.151894);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052dc0() {
   return (neuron0x90397b0()*-0.269411);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052de8() {
   return (neuron0x90399b0()*0.197555);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052e10() {
   return (neuron0x9039bd8()*0.269434);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9052e38() {
   return (neuron0x9039dd8()*0.0567654);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053078() {
   return (neuron0x902af48()*0.382421);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90530a0() {
   return (neuron0x902b128()*0.222323);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90530c8() {
   return (neuron0x90395b0()*0.0471141);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90530f0() {
   return (neuron0x90397b0()*-0.20284);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053118() {
   return (neuron0x90399b0()*0.139144);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053140() {
   return (neuron0x9039bd8()*-0.203727);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053168() {
   return (neuron0x9039dd8()*-0.313353);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90533a8() {
   return (neuron0x902af48()*0.141495);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90533d0() {
   return (neuron0x902b128()*-0.307797);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90533f8() {
   return (neuron0x90395b0()*0.39829);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053420() {
   return (neuron0x90397b0()*0.322886);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053448() {
   return (neuron0x90399b0()*-0.18416);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053470() {
   return (neuron0x9039bd8()*-0.266958);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053498() {
   return (neuron0x9039dd8()*-0.41759);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90536d8() {
   return (neuron0x902af48()*0.0636387);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053700() {
   return (neuron0x902b128()*-0.0857431);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053728() {
   return (neuron0x90395b0()*0.404944);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053750() {
   return (neuron0x90397b0()*0.450782);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053778() {
   return (neuron0x90399b0()*0.368224);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90537a0() {
   return (neuron0x9039bd8()*-0.242359);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90537c8() {
   return (neuron0x9039dd8()*-0.0428714);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053a08() {
   return (neuron0x902af48()*0.0040955);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053a30() {
   return (neuron0x902b128()*0.0739295);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053a58() {
   return (neuron0x90395b0()*-0.0289173);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053a80() {
   return (neuron0x90397b0()*-0.243068);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053aa8() {
   return (neuron0x90399b0()*-0.651589);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053ad0() {
   return (neuron0x9039bd8()*0.0137414);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053af8() {
   return (neuron0x9039dd8()*-0.0219193);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053bf8() {
   return (neuron0x903a100()*-0.226747);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053c20() {
   return (neuron0x903a330()*-0.340367);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053c48() {
   return (neuron0x903a648()*0.665473);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053c70() {
   return (neuron0x903a9e8()*-0.354251);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053c98() {
   return (neuron0x903acb8()*0.191091);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053cc0() {
   return (neuron0x903b0a0()*0.408705);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053ce8() {
   return (neuron0x903b3b8()*0.0574675);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053d10() {
   return (neuron0x903b6d0()*0.858962);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053d38() {
   return (neuron0x903ba00()*0.106499);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053d60() {
   return (neuron0x903bd30()*0.426404);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053d88() {
   return (neuron0x903c228()*-0.204995);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053db0() {
   return (neuron0x903c440()*-0.18099);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053dd8() {
   return (neuron0x903c758()*0.324083);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053e00() {
   return (neuron0x903ca70()*0.307488);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053e28() {
   return (neuron0x903cd88()*-0.0357046);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053e50() {
   return (neuron0x903d0a0()*0.0153388);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053f00() {
   return (neuron0x903d770()*-0.0396647);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053f28() {
   return (neuron0x903d9b0()*0.406776);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053f50() {
   return (neuron0x903dbf0()*-0.0747069);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053f78() {
   return (neuron0x903c0c0()*0.035302);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053fa0() {
   return (neuron0x903e408()*0.364617);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053fc8() {
   return (neuron0x903e738()*0.336026);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053ff0() {
   return (neuron0x903ea68()*-0.169144);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054018() {
   return (neuron0x903ed98()*-0.251423);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054040() {
   return (neuron0x903f0c8()*-0.380598);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054068() {
   return (neuron0x903f3f8()*0.256156);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054090() {
   return (neuron0x903f7b8()*-0.0736759);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90540b8() {
   return (neuron0x903fae8()*0.0449293);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90540e0() {
   return (neuron0x903fe18()*-0.0376195);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054108() {
   return (neuron0x9040148()*0.43382);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054130() {
   return (neuron0x9040478()*0.0504885);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054158() {
   return (neuron0x90407a8()*-1.31733);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053e78() {
   return (neuron0x9041120()*-0.261591);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053ea0() {
   return (neuron0x9041248()*0.064788);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053ec8() {
   return (neuron0x9041518()*-0.351666);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054288() {
   return (neuron0x9041830()*-0.252209);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90542b0() {
   return (neuron0x9041b48()*-0.147825);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90542d8() {
   return (neuron0x903de88()*0.0129795);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054300() {
   return (neuron0x9042630()*-0.044507);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054328() {
   return (neuron0x90428d8()*-0.0220779);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054350() {
   return (neuron0x9042c08()*-0.330014);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054378() {
   return (neuron0x9042f38()*-0.513046);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90543a0() {
   return (neuron0x9043268()*-0.0250995);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90543c8() {
   return (neuron0x9043598()*-0.0432225);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90543f0() {
   return (neuron0x90438c8()*0.405069);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054418() {
   return (neuron0x9043bf8()*0.055817);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054440() {
   return (neuron0x9043f28()*0.67331);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054468() {
   return (neuron0x9044258()*0.399073);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054490() {
   return (neuron0x9044588()*0.237693);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90544b8() {
   return (neuron0x90448b8()*0.0601648);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90544e0() {
   return (neuron0x9044be8()*0.00213878);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054508() {
   return (neuron0x9044f18()*-0.336325);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054530() {
   return (neuron0x9045248()*0.0367123);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054558() {
   return (neuron0x9045578()*0.0916882);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054580() {
   return (neuron0x90458a8()*-0.315686);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90545a8() {
   return (neuron0x9045bd8()*-0.483131);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90545d0() {
   return (neuron0x9045f08()*0.216077);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90545f8() {
   return (neuron0x9046238()*-0.00316555);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054620() {
   return (neuron0x9046680()*-0.290986);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054648() {
   return (neuron0x9046998()*0.149209);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054670() {
   return (neuron0x9046cc8()*-0.0478951);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054698() {
   return (neuron0x9046ff8()*0.167816);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90546c0() {
   return (neuron0x9047328()*0.278137);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90546e8() {
   return (neuron0x9047658()*-0.172316);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9053b20() {
   return (neuron0x9040ee8()*0.156813);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054180() {
   return (neuron0x90483d8()*0.261394);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90541a8() {
   return (neuron0x90486f0()*0.145541);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90541d0() {
   return (neuron0x9048a20()*0.507217);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90541f8() {
   return (neuron0x9048d50()*-0.00512209);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054220() {
   return (neuron0x9049080()*-0.278067);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054248() {
   return (neuron0x90493b0()*-0.0969138);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054918() {
   return (neuron0x90496e0()*-1.40439);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054940() {
   return (neuron0x9049a10()*0.396369);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054968() {
   return (neuron0x9049d40()*0.0728713);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054990() {
   return (neuron0x9041ef0()*0.0427167);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90549b8() {
   return (neuron0x9042220()*-0.118739);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x90549e0() {
   return (neuron0x904afb0()*-1.22433);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054a08() {
   return (neuron0x904b210()*0.162437);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054a30() {
   return (neuron0x904b540()*0.239944);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054a58() {
   return (neuron0x904b870()*-0.0964775);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054a80() {
   return (neuron0x904bba0()*0.391597);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054aa8() {
   return (neuron0x904bed0()*0.17321);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054ad0() {
   return (neuron0x904c200()*-0.11433);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054af8() {
   return (neuron0x904c530()*0.430117);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054b20() {
   return (neuron0x904c860()*-0.39049);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054b48() {
   return (neuron0x904cb90()*-0.26881);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054b70() {
   return (neuron0x904cec0()*0.00126113);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054b98() {
   return (neuron0x904d1f0()*0.173979);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054bc0() {
   return (neuron0x904d520()*-0.756203);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054be8() {
   return (neuron0x904d850()*0.128534);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054c10() {
   return (neuron0x904db80()*-0.158792);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054c38() {
   return (neuron0x904deb0()*-0.523865);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054c60() {
   return (neuron0x904e1e0()*0.475322);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054c88() {
   return (neuron0x904e510()*0.346555);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054cb0() {
   return (neuron0x904e840()*0.439965);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054cd8() {
   return (neuron0x904eb70()*0.0242036);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054d00() {
   return (neuron0x904eea0()*0.191134);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054d28() {
   return (neuron0x904f1d0()*0.100971);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054d50() {
   return (neuron0x904f500()*0.183768);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054d78() {
   return (neuron0x904f830()*-0.139519);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054da0() {
   return (neuron0x904fb60()*0.521381);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054dc8() {
   return (neuron0x904fe90()*0.821078);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054df0() {
   return (neuron0x90501c0()*-0.603239);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054e18() {
   return (neuron0x90504f0()*-0.00184343);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054e40() {
   return (neuron0x9050820()*0.183359);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054e68() {
   return (neuron0x9050b50()*0.151423);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054e90() {
   return (neuron0x9050e80()*0.336624);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054eb8() {
   return (neuron0x90511b0()*0.0179498);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054ee0() {
   return (neuron0x90514e0()*-0.123001);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054f08() {
   return (neuron0x9051810()*0.180414);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054f30() {
   return (neuron0x9051b40()*0.211601);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054f58() {
   return (neuron0x9051e70()*-0.0384745);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054f80() {
   return (neuron0x90521a0()*0.0288237);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054fa8() {
   return (neuron0x90524d0()*0.313216);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054fd0() {
   return (neuron0x9052800()*0.0511767);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9054ff8() {
   return (neuron0x9052b30()*-0.206413);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9055020() {
   return (neuron0x9052e60()*-0.162846);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9055048() {
   return (neuron0x9053190()*-0.0505147);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9055070() {
   return (neuron0x90534c0()*0.25305);
}

double NNParaElectronAngleCorrectionDeltaPhi::synapse0x9055098() {
   return (neuron0x90537f0()*0.837884);
}

