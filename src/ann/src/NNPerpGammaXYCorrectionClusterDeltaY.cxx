#include "NNPerpGammaXYCorrectionClusterDeltaY.h"
#include <cmath>

double NNPerpGammaXYCorrectionClusterDeltaY::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8) {
   input0 = (in0 - 2780.62)/1257.71;
   input1 = (in1 - -2.49912)/34.2565;
   input2 = (in2 - 1.20557)/59.5833;
   input3 = (in3 - 1.33643)/0.369571;
   input4 = (in4 - 1.45404)/0.378068;
   input5 = (in5 - 0.129143)/1.85876;
   input6 = (in6 - 0.234253)/1.48492;
   input7 = (in7 - 6.51988)/11.1759;
   input8 = (in8 - 4.52234)/8.10026;
   switch(index) {
     case 0:
         return neuron0x9ca5100();
     default:
         return 0.;
   }
}

double NNPerpGammaXYCorrectionClusterDeltaY::Value(int index, double* input) {
   input0 = (input[0] - 2780.62)/1257.71;
   input1 = (input[1] - -2.49912)/34.2565;
   input2 = (input[2] - 1.20557)/59.5833;
   input3 = (input[3] - 1.33643)/0.369571;
   input4 = (input[4] - 1.45404)/0.378068;
   input5 = (input[5] - 0.129143)/1.85876;
   input6 = (input[6] - 0.234253)/1.48492;
   input7 = (input[7] - 6.51988)/11.1759;
   input8 = (input[8] - 4.52234)/8.10026;
   switch(index) {
     case 0:
         return neuron0x9ca5100();
     default:
         return 0.;
   }
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c7dd70() {
   return input0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c7df70() {
   return input1;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c7e170() {
   return input2;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8c688() {
   return input3;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8c888() {
   return input4;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8ca88() {
   return input5;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8cc88() {
   return input6;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8ce88() {
   return input7;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8d088() {
   return input8;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8d3e0() {
   double input = -0.0570208;
   input += synapse0x8e77118();
   input += synapse0x9c8d598();
   input += synapse0x9c8d5c0();
   input += synapse0x9c8d5e8();
   input += synapse0x9c8d610();
   input += synapse0x9c8d638();
   input += synapse0x9c8d660();
   input += synapse0x9c8d688();
   input += synapse0x9c8d6b0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8d3e0() {
   double input = input0x9c8d3e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8d6d8() {
   double input = -0.079023;
   input += synapse0x9c8d8d8();
   input += synapse0x9c8d900();
   input += synapse0x9c8d928();
   input += synapse0x9c8d950();
   input += synapse0x9c8d978();
   input += synapse0x9c8d9a0();
   input += synapse0x9c8d9c8();
   input += synapse0x9c8d9f0();
   input += synapse0x9c8daa0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8d6d8() {
   double input = input0x9c8d6d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8dac8() {
   double input = -0.0473462;
   input += synapse0x9c8dc80();
   input += synapse0x9c8dca8();
   input += synapse0x9c8dcd0();
   input += synapse0x9c8dcf8();
   input += synapse0x9c8dd20();
   input += synapse0x9c8dd48();
   input += synapse0x9c8dd70();
   input += synapse0x9c8dd98();
   input += synapse0x9c8ddc0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8dac8() {
   double input = input0x9c8dac8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8dde8() {
   double input = -0.536226;
   input += synapse0x9c8dfe8();
   input += synapse0x9c8e010();
   input += synapse0x9c8e038();
   input += synapse0x9c8e060();
   input += synapse0x9c8e088();
   input += synapse0x9c8e0b0();
   input += synapse0x9c8da18();
   input += synapse0x9c8da40();
   input += synapse0x9c8da68();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8dde8() {
   double input = input0x9c8dde8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8e1e0() {
   double input = 0.717454;
   input += synapse0x9c8e3e0();
   input += synapse0x9c8e408();
   input += synapse0x9c8e430();
   input += synapse0x9c8e458();
   input += synapse0x9c8e480();
   input += synapse0x9c8e4a8();
   input += synapse0x9c8e4d0();
   input += synapse0x9c8e4f8();
   input += synapse0x9c8e520();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8e1e0() {
   double input = input0x9c8e1e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8e548() {
   double input = 0.0577285;
   input += synapse0x9c8e748();
   input += synapse0x9c8e770();
   input += synapse0x9c8e798();
   input += synapse0x9c8e7c0();
   input += synapse0x9c8e7e8();
   input += synapse0x9c8e810();
   input += synapse0x9c8e838();
   input += synapse0x9c8e860();
   input += synapse0x9c8e888();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8e548() {
   double input = input0x9c8e548();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8e8b0() {
   double input = -0.866455;
   input += synapse0x9c8eab0();
   input += synapse0x9c8ead8();
   input += synapse0x9c8eb00();
   input += synapse0x9c8eb28();
   input += synapse0x9c8eb50();
   input += synapse0x9c8eb78();
   input += synapse0x9c8eba0();
   input += synapse0x9c8ebc8();
   input += synapse0x9c8ebf0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8e8b0() {
   double input = input0x9c8e8b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8ec18() {
   double input = -0.56217;
   input += synapse0x9c8eea0();
   input += synapse0x9c8eec8();
   input += synapse0x8e770e0();
   input += synapse0x8e76eb8();
   input += synapse0x8e76a18();
   input += synapse0x8e76f88();
   input += synapse0x8e76fb0();
   input += synapse0x9c8e0d8();
   input += synapse0x9c8e100();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8ec18() {
   double input = input0x9c8ec18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8f0f8() {
   double input = 1.20722;
   input += synapse0x9c8e1b8();
   input += synapse0x9c8f268();
   input += synapse0x9c8f290();
   input += synapse0x9c8f2b8();
   input += synapse0x9c8f2e0();
   input += synapse0x9c8f308();
   input += synapse0x9c8f330();
   input += synapse0x9c8f358();
   input += synapse0x9c8f380();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8f0f8() {
   double input = input0x9c8f0f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8f3a8() {
   double input = -0.436402;
   input += synapse0x9c8f5a8();
   input += synapse0x9c8f5d0();
   input += synapse0x9c8f5f8();
   input += synapse0x9c8f620();
   input += synapse0x9c8f648();
   input += synapse0x9c8f670();
   input += synapse0x9c8f698();
   input += synapse0x9c8f6c0();
   input += synapse0x9c8f6e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8f3a8() {
   double input = input0x9c8f3a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8f710() {
   double input = -0.0670001;
   input += synapse0x9c8f910();
   input += synapse0x9c8f938();
   input += synapse0x9c8f960();
   input += synapse0x9c8f988();
   input += synapse0x9c8f9b0();
   input += synapse0x9c8f9d8();
   input += synapse0x9c8fa00();
   input += synapse0x9c8fa28();
   input += synapse0x9c8fa50();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8f710() {
   double input = input0x9c8f710();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8fa78() {
   double input = 3.50654;
   input += synapse0x9c8fc78();
   input += synapse0x9c8fca0();
   input += synapse0x9c8fcc8();
   input += synapse0x9c8fcf0();
   input += synapse0x9c8fd18();
   input += synapse0x9c8fd40();
   input += synapse0x9c8fd68();
   input += synapse0x9c8fd90();
   input += synapse0x9c8fdb8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8fa78() {
   double input = input0x9c8fa78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c8fde0() {
   double input = -0.113987;
   input += synapse0x9c8ffe0();
   input += synapse0x9c90008();
   input += synapse0x9c90030();
   input += synapse0x9c90058();
   input += synapse0x9c90080();
   input += synapse0x9c900a8();
   input += synapse0x9c900d0();
   input += synapse0x9c900f8();
   input += synapse0x9c90120();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c8fde0() {
   double input = input0x9c8fde0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c90148() {
   double input = 0.730853;
   input += synapse0x9c90348();
   input += synapse0x9c90370();
   input += synapse0x9c90398();
   input += synapse0x9c903c0();
   input += synapse0x9c903e8();
   input += synapse0x9c90410();
   input += synapse0x9c90438();
   input += synapse0x9c90460();
   input += synapse0x9c90488();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c90148() {
   double input = input0x9c90148();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c904b0() {
   double input = -0.207618;
   input += synapse0x9c906b0();
   input += synapse0x9c906d8();
   input += synapse0x9c90700();
   input += synapse0x9c8eef0();
   input += synapse0x9c8ef18();
   input += synapse0x9c8ef40();
   input += synapse0x9c8ef68();
   input += synapse0x9c8ef90();
   input += synapse0x9c8efb8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c904b0() {
   double input = input0x9c904b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c90b30() {
   double input = -0.738565;
   input += synapse0x9c8f0b8();
   input += synapse0x9c90ce0();
   input += synapse0x9c90d90();
   input += synapse0x9c90e40();
   input += synapse0x9c90ef0();
   input += synapse0x9c90fa0();
   input += synapse0x9c91050();
   input += synapse0x9c91100();
   input += synapse0x9c911b0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c90b30() {
   double input = input0x9c90b30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c91260() {
   double input = -0.225419;
   input += synapse0x9c91388();
   input += synapse0x9c913b0();
   input += synapse0x9c913d8();
   input += synapse0x9c91400();
   input += synapse0x9c91428();
   input += synapse0x9c91450();
   input += synapse0x9c91478();
   input += synapse0x9c914a0();
   input += synapse0x9c914c8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c91260() {
   double input = input0x9c91260();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c914f0() {
   double input = 0.495837;
   input += synapse0x9c91618();
   input += synapse0x9c91640();
   input += synapse0x9c91668();
   input += synapse0x9c91690();
   input += synapse0x9c916b8();
   input += synapse0x9c916e0();
   input += synapse0x9c91708();
   input += synapse0x9c91730();
   input += synapse0x9c91758();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c914f0() {
   double input = input0x9c914f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c91780() {
   double input = -0.198198;
   input += synapse0x9c918a8();
   input += synapse0x9c918d0();
   input += synapse0x9c918f8();
   input += synapse0x9c91920();
   input += synapse0x9c91948();
   input += synapse0x9c91970();
   input += synapse0x9c91998();
   input += synapse0x9c919c0();
   input += synapse0x9c919e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c91780() {
   double input = input0x9c91780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c91a10() {
   double input = 0.028422;
   input += synapse0x9c91c28();
   input += synapse0x9c91c50();
   input += synapse0x9c91c78();
   input += synapse0x9c91ca0();
   input += synapse0x9c91cc8();
   input += synapse0x9c91cf0();
   input += synapse0x9c91d18();
   input += synapse0x9c91d40();
   input += synapse0x9c91d68();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c91a10() {
   double input = input0x9c91a10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c91d90() {
   double input = 1.76271;
   input += synapse0x9c91fa8();
   input += synapse0x9c91fd0();
   input += synapse0x9c91ff8();
   input += synapse0x9c92020();
   input += synapse0x9c92048();
   input += synapse0x9c92070();
   input += synapse0x9c92098();
   input += synapse0x9c920c0();
   input += synapse0x9c920e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c91d90() {
   double input = input0x9c91d90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c92110() {
   double input = -0.629328;
   input += synapse0x9c92328();
   input += synapse0x9c92350();
   input += synapse0x9c92378();
   input += synapse0x9c923a0();
   input += synapse0x9c923c8();
   input += synapse0x9c923f0();
   input += synapse0x9c92418();
   input += synapse0x9c92440();
   input += synapse0x9c92468();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c92110() {
   double input = input0x9c92110();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c92490() {
   double input = 0.447183;
   input += synapse0x9c926a8();
   input += synapse0x9c926d0();
   input += synapse0x9c926f8();
   input += synapse0x9c92720();
   input += synapse0x9c92748();
   input += synapse0x9c92770();
   input += synapse0x9c92798();
   input += synapse0x9c927c0();
   input += synapse0x9c927e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c92490() {
   double input = input0x9c92490();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c92810() {
   double input = 1.14697;
   input += synapse0x9c8ee18();
   input += synapse0x9c8ee40();
   input += synapse0x9c8ee68();
   input += synapse0x9c92b30();
   input += synapse0x9c92b58();
   input += synapse0x9c92b80();
   input += synapse0x9c92ba8();
   input += synapse0x9c92bd0();
   input += synapse0x9c92bf8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c92810() {
   double input = input0x9c92810();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c92c20() {
   double input = 0.306162;
   input += synapse0x9c92e38();
   input += synapse0x9c92e60();
   input += synapse0x9c92e88();
   input += synapse0x9c92eb0();
   input += synapse0x9c92ed8();
   input += synapse0x9c92f00();
   input += synapse0x9c92f28();
   input += synapse0x9c92f50();
   input += synapse0x9c92f78();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c92c20() {
   double input = input0x9c92c20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c92fa0() {
   double input = 0.342927;
   input += synapse0x9c931b8();
   input += synapse0x9c931e0();
   input += synapse0x9c93208();
   input += synapse0x9c93230();
   input += synapse0x9c93258();
   input += synapse0x9c93280();
   input += synapse0x9c932a8();
   input += synapse0x9c932d0();
   input += synapse0x9c932f8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c92fa0() {
   double input = input0x9c92fa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c93320() {
   double input = -0.520597;
   input += synapse0x9c93538();
   input += synapse0x9c93560();
   input += synapse0x9c93588();
   input += synapse0x9c935b0();
   input += synapse0x9c935d8();
   input += synapse0x9c93600();
   input += synapse0x9c93628();
   input += synapse0x9c93650();
   input += synapse0x9c93678();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c93320() {
   double input = input0x9c93320();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c936a0() {
   double input = 0.495719;
   input += synapse0x9c938b8();
   input += synapse0x9c938e0();
   input += synapse0x9c93908();
   input += synapse0x9c93930();
   input += synapse0x9c93958();
   input += synapse0x9c93980();
   input += synapse0x9c939a8();
   input += synapse0x9c939d0();
   input += synapse0x9c939f8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c936a0() {
   double input = input0x9c936a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c93a20() {
   double input = 0.0385031;
   input += synapse0x9c93c38();
   input += synapse0x9c93c60();
   input += synapse0x9c93c88();
   input += synapse0x9c93cb0();
   input += synapse0x9c93cd8();
   input += synapse0x9c90728();
   input += synapse0x9c90750();
   input += synapse0x9c90778();
   input += synapse0x9c907a0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c93a20() {
   double input = input0x9c93a20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c907c8() {
   double input = 0.222838;
   input += synapse0x9c909e0();
   input += synapse0x9c90a08();
   input += synapse0x9c90a30();
   input += synapse0x9c90a58();
   input += synapse0x9c90a80();
   input += synapse0x9c90aa8();
   input += synapse0x9c90ad0();
   input += synapse0x9c90af8();
   input += synapse0x9c94508();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c907c8() {
   double input = input0x9c907c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c94530() {
   double input = -1.65106;
   input += synapse0x9c94748();
   input += synapse0x9c94770();
   input += synapse0x9c94798();
   input += synapse0x9c947c0();
   input += synapse0x9c947e8();
   input += synapse0x9c94810();
   input += synapse0x9c94838();
   input += synapse0x9c94860();
   input += synapse0x9c94888();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c94530() {
   double input = input0x9c94530();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c948b0() {
   double input = 0.259487;
   input += synapse0x9c94ac8();
   input += synapse0x9c90c58();
   input += synapse0x9c90c80();
   input += synapse0x9c90ca8();
   input += synapse0x9c90d08();
   input += synapse0x9c90d30();
   input += synapse0x9c90d58();
   input += synapse0x9c90db8();
   input += synapse0x9c90de0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c948b0() {
   double input = input0x9c948b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c95438() {
   double input = 1.4214;
   input += synapse0x9c91010();
   input += synapse0x9c90eb0();
   input += synapse0x9c90f60();
   input += synapse0x9c91078();
   input += synapse0x9c910a0();
   input += synapse0x9c910c8();
   input += synapse0x9c91128();
   input += synapse0x9c91150();
   input += synapse0x9c91178();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c95438() {
   double input = input0x9c95438();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c95560() {
   double input = -0.310547;
   input += synapse0x9c91220();
   input += synapse0x9c95718();
   input += synapse0x9c95740();
   input += synapse0x9c95768();
   input += synapse0x9c95790();
   input += synapse0x9c957b8();
   input += synapse0x9c957e0();
   input += synapse0x9c95808();
   input += synapse0x9c95830();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c95560() {
   double input = input0x9c95560();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c95858() {
   double input = 0.446135;
   input += synapse0x9c95a58();
   input += synapse0x9c95a80();
   input += synapse0x9c95aa8();
   input += synapse0x9c95ad0();
   input += synapse0x9c95af8();
   input += synapse0x9c95b20();
   input += synapse0x9c95b48();
   input += synapse0x9c95b70();
   input += synapse0x9c95b98();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c95858() {
   double input = input0x9c95858();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c95bc0() {
   double input = -0.291795;
   input += synapse0x9c95dc0();
   input += synapse0x9c95de8();
   input += synapse0x9c95e10();
   input += synapse0x9c95e38();
   input += synapse0x9c95e60();
   input += synapse0x9c95e88();
   input += synapse0x9c95eb0();
   input += synapse0x9c95ed8();
   input += synapse0x9c95f00();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c95bc0() {
   double input = input0x9c95bc0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c95f28() {
   double input = 0.0211209;
   input += synapse0x9c96128();
   input += synapse0x9c96150();
   input += synapse0x9c96178();
   input += synapse0x9c961a0();
   input += synapse0x9c961c8();
   input += synapse0x9c961f0();
   input += synapse0x9c96218();
   input += synapse0x9c96240();
   input += synapse0x9c96268();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c95f28() {
   double input = input0x9c95f28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c96290() {
   double input = -0.114329;
   input += synapse0x9c96490();
   input += synapse0x9c964b8();
   input += synapse0x9c964e0();
   input += synapse0x9c96508();
   input += synapse0x9c96530();
   input += synapse0x9c96558();
   input += synapse0x9c96580();
   input += synapse0x9c965a8();
   input += synapse0x9c965d0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c96290() {
   double input = input0x9c96290();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c965f8() {
   double input = -0.116327;
   input += synapse0x9c96810();
   input += synapse0x9c96838();
   input += synapse0x9c96860();
   input += synapse0x9c96888();
   input += synapse0x9c968b0();
   input += synapse0x9c968d8();
   input += synapse0x9c96900();
   input += synapse0x9c96928();
   input += synapse0x9c96950();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c965f8() {
   double input = input0x9c965f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c96978() {
   double input = -0.764849;
   input += synapse0x9c96b90();
   input += synapse0x9c96bb8();
   input += synapse0x9c96be0();
   input += synapse0x9c96c08();
   input += synapse0x9c96c30();
   input += synapse0x9c96c58();
   input += synapse0x9c96c80();
   input += synapse0x9c96ca8();
   input += synapse0x9c96cd0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c96978() {
   double input = input0x9c96978();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c96cf8() {
   double input = 0.798292;
   input += synapse0x9c96f10();
   input += synapse0x9c96f38();
   input += synapse0x9c96f60();
   input += synapse0x9c96f88();
   input += synapse0x9c96fb0();
   input += synapse0x9c96fd8();
   input += synapse0x9c97000();
   input += synapse0x9c97028();
   input += synapse0x9c97050();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c96cf8() {
   double input = input0x9c96cf8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c97078() {
   double input = -0.309308;
   input += synapse0x9c97290();
   input += synapse0x9c972b8();
   input += synapse0x9c972e0();
   input += synapse0x9c97308();
   input += synapse0x9c97330();
   input += synapse0x9c97358();
   input += synapse0x9c97380();
   input += synapse0x9c973a8();
   input += synapse0x9c973d0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c97078() {
   double input = input0x9c97078();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c973f8() {
   double input = -0.0752544;
   input += synapse0x9c97610();
   input += synapse0x9c97638();
   input += synapse0x9c97660();
   input += synapse0x9c97688();
   input += synapse0x9c976b0();
   input += synapse0x9c976d8();
   input += synapse0x9c97700();
   input += synapse0x9c97728();
   input += synapse0x9c97750();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c973f8() {
   double input = input0x9c973f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c97778() {
   double input = -0.392386;
   input += synapse0x9c97990();
   input += synapse0x9c979b8();
   input += synapse0x9c979e0();
   input += synapse0x9c97a08();
   input += synapse0x9c97a30();
   input += synapse0x9c97a58();
   input += synapse0x9c97a80();
   input += synapse0x9c97aa8();
   input += synapse0x9c97ad0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c97778() {
   double input = input0x9c97778();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c97af8() {
   double input = 0.194052;
   input += synapse0x9c97d10();
   input += synapse0x9c97d38();
   input += synapse0x9c97d60();
   input += synapse0x9c97d88();
   input += synapse0x9c97db0();
   input += synapse0x9c97dd8();
   input += synapse0x9c97e00();
   input += synapse0x9c97e28();
   input += synapse0x9c97e50();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c97af8() {
   double input = input0x9c97af8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c97e78() {
   double input = 0.11772;
   input += synapse0x9c98090();
   input += synapse0x9c980b8();
   input += synapse0x9c980e0();
   input += synapse0x9c98108();
   input += synapse0x9c98130();
   input += synapse0x9c98158();
   input += synapse0x9c98180();
   input += synapse0x9c981a8();
   input += synapse0x9c981d0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c97e78() {
   double input = input0x9c97e78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c981f8() {
   double input = 0.111258;
   input += synapse0x9c98410();
   input += synapse0x9c98438();
   input += synapse0x9c98460();
   input += synapse0x9c98488();
   input += synapse0x9c984b0();
   input += synapse0x9c984d8();
   input += synapse0x9c98500();
   input += synapse0x9c98528();
   input += synapse0x9c98550();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c981f8() {
   double input = input0x9c981f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c98578() {
   double input = 0.194503;
   input += synapse0x9c98790();
   input += synapse0x9c987b8();
   input += synapse0x9c987e0();
   input += synapse0x9c98808();
   input += synapse0x9c98830();
   input += synapse0x9c98858();
   input += synapse0x9c98880();
   input += synapse0x9c988a8();
   input += synapse0x9c988d0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c98578() {
   double input = input0x9c98578();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c988f8() {
   double input = -0.0234124;
   input += synapse0x9c98b10();
   input += synapse0x9c98b38();
   input += synapse0x9c98b60();
   input += synapse0x9c98b88();
   input += synapse0x9c98bb0();
   input += synapse0x9c98bd8();
   input += synapse0x9c98c00();
   input += synapse0x9c98c28();
   input += synapse0x9c98c50();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c988f8() {
   double input = input0x9c988f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c98c78() {
   double input = 0.351873;
   input += synapse0x9c98e90();
   input += synapse0x9c98eb8();
   input += synapse0x9c98ee0();
   input += synapse0x9c98f08();
   input += synapse0x9c98f30();
   input += synapse0x9c98f58();
   input += synapse0x9c98f80();
   input += synapse0x9c98fa8();
   input += synapse0x9c98fd0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c98c78() {
   double input = input0x9c98c78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c98ff8() {
   double input = -0.717095;
   input += synapse0x9c99210();
   input += synapse0x9c99238();
   input += synapse0x9c99260();
   input += synapse0x9c99288();
   input += synapse0x9c992b0();
   input += synapse0x9c992d8();
   input += synapse0x9c99300();
   input += synapse0x9c99328();
   input += synapse0x9c99350();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c98ff8() {
   double input = input0x9c98ff8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c99378() {
   double input = -0.154744;
   input += synapse0x9c99590();
   input += synapse0x9c995b8();
   input += synapse0x9c995e0();
   input += synapse0x9c99608();
   input += synapse0x9c99630();
   input += synapse0x9c99658();
   input += synapse0x9c99680();
   input += synapse0x9c996a8();
   input += synapse0x9c996d0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c99378() {
   double input = input0x9c99378();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c996f8() {
   double input = -0.521833;
   input += synapse0x9c99910();
   input += synapse0x9c99938();
   input += synapse0x9c99960();
   input += synapse0x9c99988();
   input += synapse0x9c999b0();
   input += synapse0x9c999d8();
   input += synapse0x9c99a00();
   input += synapse0x9c99a28();
   input += synapse0x9c99a50();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c996f8() {
   double input = input0x9c996f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c99a78() {
   double input = -0.609165;
   input += synapse0x9c99c90();
   input += synapse0x9c99cb8();
   input += synapse0x9c99ce0();
   input += synapse0x9c99d08();
   input += synapse0x9c99d30();
   input += synapse0x9c99d58();
   input += synapse0x9c99d80();
   input += synapse0x9c99da8();
   input += synapse0x9c99dd0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c99a78() {
   double input = input0x9c99a78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c99df8() {
   double input = 0.48002;
   input += synapse0x9c9a010();
   input += synapse0x9c9a038();
   input += synapse0x9c9a060();
   input += synapse0x9c9a088();
   input += synapse0x9c9a0b0();
   input += synapse0x9c9a0d8();
   input += synapse0x9c9a100();
   input += synapse0x9c9a128();
   input += synapse0x9c9a150();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c99df8() {
   double input = input0x9c99df8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9a178() {
   double input = 0.509838;
   input += synapse0x9c92a28();
   input += synapse0x9c92a50();
   input += synapse0x9c92a78();
   input += synapse0x9c92aa0();
   input += synapse0x9c92ac8();
   input += synapse0x9c92af0();
   input += synapse0x9c9a598();
   input += synapse0x9c9a5c0();
   input += synapse0x9c9a5e8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9a178() {
   double input = input0x9c9a178();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9a610() {
   double input = 0.0837651;
   input += synapse0x9c9a810();
   input += synapse0x9c9a838();
   input += synapse0x9c9a860();
   input += synapse0x9c9a888();
   input += synapse0x9c9a8b0();
   input += synapse0x9c9a8d8();
   input += synapse0x9c9a900();
   input += synapse0x9c9a928();
   input += synapse0x9c9a950();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9a610() {
   double input = input0x9c9a610();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c93d00() {
   double input = 0.196997;
   input += synapse0x9c93f18();
   input += synapse0x9c93f40();
   input += synapse0x9c93f68();
   input += synapse0x9c93f90();
   input += synapse0x9c93fb8();
   input += synapse0x9c93fe0();
   input += synapse0x9c94008();
   input += synapse0x9c94030();
   input += synapse0x9c94058();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c93d00() {
   double input = input0x9c93d00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c94080() {
   double input = -0.394087;
   input += synapse0x9c94298();
   input += synapse0x9c942c0();
   input += synapse0x9c942e8();
   input += synapse0x9c94310();
   input += synapse0x9c94338();
   input += synapse0x9c94360();
   input += synapse0x9c94388();
   input += synapse0x9c943b0();
   input += synapse0x9c943d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c94080() {
   double input = input0x9c94080();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9b980() {
   double input = 0.974909;
   input += synapse0x9c9baa8();
   input += synapse0x9c9bad0();
   input += synapse0x9c9baf8();
   input += synapse0x9c9bb20();
   input += synapse0x9c9bb48();
   input += synapse0x9c9bb70();
   input += synapse0x9c9bb98();
   input += synapse0x9c9bbc0();
   input += synapse0x9c9bbe8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9b980() {
   double input = input0x9c9b980();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9bc10() {
   double input = -0.320282;
   input += synapse0x9c9be10();
   input += synapse0x9c9be38();
   input += synapse0x9c9be60();
   input += synapse0x9c9be88();
   input += synapse0x9c9beb0();
   input += synapse0x9c9bed8();
   input += synapse0x9c9bf00();
   input += synapse0x9c9bf28();
   input += synapse0x9c9bf50();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9bc10() {
   double input = input0x9c9bc10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9bf78() {
   double input = -0.740458;
   input += synapse0x9c9c190();
   input += synapse0x9c9c1b8();
   input += synapse0x9c9c1e0();
   input += synapse0x9c9c208();
   input += synapse0x9c9c230();
   input += synapse0x9c9c258();
   input += synapse0x9c9c280();
   input += synapse0x9c9c2a8();
   input += synapse0x9c9c2d0();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9bf78() {
   double input = input0x9c9bf78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9c2f8() {
   double input = 0.145688;
   input += synapse0x9c9c510();
   input += synapse0x9c9c538();
   input += synapse0x9c9c560();
   input += synapse0x9c9c588();
   input += synapse0x9c9c5b0();
   input += synapse0x9c9c5d8();
   input += synapse0x9c9c600();
   input += synapse0x9c9c628();
   input += synapse0x9c9c650();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9c2f8() {
   double input = input0x9c9c2f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9c678() {
   double input = 0.647458;
   input += synapse0x9c9c890();
   input += synapse0x9c94af0();
   input += synapse0x9c94b18();
   input += synapse0x9c94b40();
   input += synapse0x9c94d70();
   input += synapse0x9c94d98();
   input += synapse0x9c94fc8();
   input += synapse0x9c94ff0();
   input += synapse0x9c95228();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9c678() {
   double input = input0x9c9c678();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c95250() {
   double input = -0.140593;
   input += synapse0x9c9d530();
   input += synapse0x9c9d558();
   input += synapse0x9c9d580();
   input += synapse0x9c9d5a8();
   input += synapse0x9c9d5d0();
   input += synapse0x9c9d5f8();
   input += synapse0x9c9d620();
   input += synapse0x9c9d648();
   input += synapse0x9c9d670();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c95250() {
   double input = input0x9c95250();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9d698() {
   double input = -0.096001;
   input += synapse0x9c9d898();
   input += synapse0x9c9d8c0();
   input += synapse0x9c9d8e8();
   input += synapse0x9c9d910();
   input += synapse0x9c9d938();
   input += synapse0x9c9d960();
   input += synapse0x9c9d988();
   input += synapse0x9c9d9b0();
   input += synapse0x9c9d9d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9d698() {
   double input = input0x9c9d698();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9da00() {
   double input = 0.157875;
   input += synapse0x9c9dc18();
   input += synapse0x9c9dc40();
   input += synapse0x9c9dc68();
   input += synapse0x9c9dc90();
   input += synapse0x9c9dcb8();
   input += synapse0x9c9dce0();
   input += synapse0x9c9dd08();
   input += synapse0x9c9dd30();
   input += synapse0x9c9dd58();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9da00() {
   double input = input0x9c9da00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9dd80() {
   double input = 0.581813;
   input += synapse0x9c9df98();
   input += synapse0x9c9dfc0();
   input += synapse0x9c9dfe8();
   input += synapse0x9c9e010();
   input += synapse0x9c9e038();
   input += synapse0x9c9e060();
   input += synapse0x9c9e088();
   input += synapse0x9c9e0b0();
   input += synapse0x9c9e0d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9dd80() {
   double input = input0x9c9dd80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9e100() {
   double input = 0.284578;
   input += synapse0x9c9e318();
   input += synapse0x9c9e340();
   input += synapse0x9c9e368();
   input += synapse0x9c9e390();
   input += synapse0x9c9e3b8();
   input += synapse0x9c9e3e0();
   input += synapse0x9c9e408();
   input += synapse0x9c9e430();
   input += synapse0x9c9e458();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9e100() {
   double input = input0x9c9e100();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9e480() {
   double input = -0.196178;
   input += synapse0x9c9e698();
   input += synapse0x9c9e6c0();
   input += synapse0x9c9e6e8();
   input += synapse0x9c9e710();
   input += synapse0x9c9e738();
   input += synapse0x9c9e760();
   input += synapse0x9c9e788();
   input += synapse0x9c9e7b0();
   input += synapse0x9c9e7d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9e480() {
   double input = input0x9c9e480();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9e800() {
   double input = -0.370773;
   input += synapse0x9c9ea18();
   input += synapse0x9c9ea40();
   input += synapse0x9c9ea68();
   input += synapse0x9c9ea90();
   input += synapse0x9c9eab8();
   input += synapse0x9c9eae0();
   input += synapse0x9c9eb08();
   input += synapse0x9c9eb30();
   input += synapse0x9c9eb58();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9e800() {
   double input = input0x9c9e800();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9eb80() {
   double input = -0.209877;
   input += synapse0x9c9ed98();
   input += synapse0x9c9edc0();
   input += synapse0x9c9ede8();
   input += synapse0x9c9ee10();
   input += synapse0x9c9ee38();
   input += synapse0x9c9ee60();
   input += synapse0x9c9ee88();
   input += synapse0x9c9eeb0();
   input += synapse0x9c9eed8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9eb80() {
   double input = input0x9c9eb80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9ef00() {
   double input = -0.486968;
   input += synapse0x9c9f118();
   input += synapse0x9c9f140();
   input += synapse0x9c9f168();
   input += synapse0x9c9f190();
   input += synapse0x9c9f1b8();
   input += synapse0x9c9f1e0();
   input += synapse0x9c9f208();
   input += synapse0x9c9f230();
   input += synapse0x9c9f258();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9ef00() {
   double input = input0x9c9ef00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9f280() {
   double input = 0.199119;
   input += synapse0x9c9f498();
   input += synapse0x9c9f4c0();
   input += synapse0x9c9f4e8();
   input += synapse0x9c9f510();
   input += synapse0x9c9f538();
   input += synapse0x9c9f560();
   input += synapse0x9c9f588();
   input += synapse0x9c9f5b0();
   input += synapse0x9c9f5d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9f280() {
   double input = input0x9c9f280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9f600() {
   double input = 1.21921;
   input += synapse0x9c9f818();
   input += synapse0x9c9f840();
   input += synapse0x9c9f868();
   input += synapse0x9c9f890();
   input += synapse0x9c9f8b8();
   input += synapse0x9c9f8e0();
   input += synapse0x9c9f908();
   input += synapse0x9c9f930();
   input += synapse0x9c9f958();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9f600() {
   double input = input0x9c9f600();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9f980() {
   double input = 1.27098;
   input += synapse0x9c9fb98();
   input += synapse0x9c9fbc0();
   input += synapse0x9c9fbe8();
   input += synapse0x9c9fc10();
   input += synapse0x9c9fc38();
   input += synapse0x9c9fc60();
   input += synapse0x9c9fc88();
   input += synapse0x9c9fcb0();
   input += synapse0x9c9fcd8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9f980() {
   double input = input0x9c9f980();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9c9fd00() {
   double input = 1.2759;
   input += synapse0x9c9ff18();
   input += synapse0x9c9ff40();
   input += synapse0x9c9ff68();
   input += synapse0x9c9ff90();
   input += synapse0x9c9ffb8();
   input += synapse0x9c9ffe0();
   input += synapse0x9ca0008();
   input += synapse0x9ca0030();
   input += synapse0x9ca0058();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9c9fd00() {
   double input = input0x9c9fd00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca0080() {
   double input = 0.953003;
   input += synapse0x9ca0298();
   input += synapse0x9ca02c0();
   input += synapse0x9ca02e8();
   input += synapse0x9ca0310();
   input += synapse0x9ca0338();
   input += synapse0x9ca0360();
   input += synapse0x9ca0388();
   input += synapse0x9ca03b0();
   input += synapse0x9ca03d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca0080() {
   double input = input0x9ca0080();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca0400() {
   double input = -0.292888;
   input += synapse0x9ca0618();
   input += synapse0x9ca0640();
   input += synapse0x9ca0668();
   input += synapse0x9ca0690();
   input += synapse0x9ca06b8();
   input += synapse0x9ca06e0();
   input += synapse0x9ca0708();
   input += synapse0x9ca0730();
   input += synapse0x9ca0758();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca0400() {
   double input = input0x9ca0400();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca0780() {
   double input = -0.350883;
   input += synapse0x9ca0998();
   input += synapse0x9ca09c0();
   input += synapse0x9ca09e8();
   input += synapse0x9ca0a10();
   input += synapse0x9ca0a38();
   input += synapse0x9ca0a60();
   input += synapse0x9ca0a88();
   input += synapse0x9ca0ab0();
   input += synapse0x9ca0ad8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca0780() {
   double input = input0x9ca0780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca0b00() {
   double input = 0.356041;
   input += synapse0x9ca0d18();
   input += synapse0x9ca0d40();
   input += synapse0x9ca0d68();
   input += synapse0x9ca0d90();
   input += synapse0x9ca0db8();
   input += synapse0x9ca0de0();
   input += synapse0x9ca0e08();
   input += synapse0x9ca0e30();
   input += synapse0x9ca0e58();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca0b00() {
   double input = input0x9ca0b00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca0e80() {
   double input = -0.561134;
   input += synapse0x9ca1098();
   input += synapse0x9ca10c0();
   input += synapse0x9ca10e8();
   input += synapse0x9ca1110();
   input += synapse0x9ca1138();
   input += synapse0x9ca1160();
   input += synapse0x9ca1188();
   input += synapse0x9ca11b0();
   input += synapse0x9ca11d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca0e80() {
   double input = input0x9ca0e80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca1200() {
   double input = 0.339424;
   input += synapse0x9ca1418();
   input += synapse0x9ca1440();
   input += synapse0x9ca1468();
   input += synapse0x9ca1490();
   input += synapse0x9ca14b8();
   input += synapse0x9ca14e0();
   input += synapse0x9ca1508();
   input += synapse0x9ca1530();
   input += synapse0x9ca1558();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca1200() {
   double input = input0x9ca1200();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca1580() {
   double input = 0.106368;
   input += synapse0x9ca1798();
   input += synapse0x9ca17c0();
   input += synapse0x9ca17e8();
   input += synapse0x9ca1810();
   input += synapse0x9ca1838();
   input += synapse0x9ca1860();
   input += synapse0x9ca1888();
   input += synapse0x9ca18b0();
   input += synapse0x9ca18d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca1580() {
   double input = input0x9ca1580();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca1900() {
   double input = 0.102199;
   input += synapse0x9ca1b18();
   input += synapse0x9ca1b40();
   input += synapse0x9ca1b68();
   input += synapse0x9ca1b90();
   input += synapse0x9ca1bb8();
   input += synapse0x9ca1be0();
   input += synapse0x9ca1c08();
   input += synapse0x9ca1c30();
   input += synapse0x9ca1c58();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca1900() {
   double input = input0x9ca1900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca1c80() {
   double input = -0.0257963;
   input += synapse0x9ca1e98();
   input += synapse0x9ca1ec0();
   input += synapse0x9ca1ee8();
   input += synapse0x9ca1f10();
   input += synapse0x9ca1f38();
   input += synapse0x9ca1f60();
   input += synapse0x9ca1f88();
   input += synapse0x9ca1fb0();
   input += synapse0x9ca1fd8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca1c80() {
   double input = input0x9ca1c80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca2000() {
   double input = 0.284446;
   input += synapse0x9ca2218();
   input += synapse0x9ca2240();
   input += synapse0x9ca2268();
   input += synapse0x9ca2290();
   input += synapse0x9ca22b8();
   input += synapse0x9ca22e0();
   input += synapse0x9ca2308();
   input += synapse0x9ca2330();
   input += synapse0x9ca2358();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca2000() {
   double input = input0x9ca2000();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca2380() {
   double input = -0.310042;
   input += synapse0x9ca2598();
   input += synapse0x9ca25c0();
   input += synapse0x9ca25e8();
   input += synapse0x9ca2610();
   input += synapse0x9ca2638();
   input += synapse0x9ca2660();
   input += synapse0x9ca2688();
   input += synapse0x9ca26b0();
   input += synapse0x9ca26d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca2380() {
   double input = input0x9ca2380();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca2700() {
   double input = -0.00690124;
   input += synapse0x9ca2918();
   input += synapse0x9ca2940();
   input += synapse0x9ca2968();
   input += synapse0x9ca2990();
   input += synapse0x9ca29b8();
   input += synapse0x9ca29e0();
   input += synapse0x9ca2a08();
   input += synapse0x9ca2a30();
   input += synapse0x9ca2a58();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca2700() {
   double input = input0x9ca2700();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca2a80() {
   double input = -0.073507;
   input += synapse0x9ca2c98();
   input += synapse0x9ca2cc0();
   input += synapse0x9ca2ce8();
   input += synapse0x9ca2d10();
   input += synapse0x9ca2d38();
   input += synapse0x9ca2d60();
   input += synapse0x9ca2d88();
   input += synapse0x9ca2db0();
   input += synapse0x9ca2dd8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca2a80() {
   double input = input0x9ca2a80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca2e00() {
   double input = 0.320883;
   input += synapse0x9ca3018();
   input += synapse0x9ca3040();
   input += synapse0x9ca3068();
   input += synapse0x9ca3090();
   input += synapse0x9ca30b8();
   input += synapse0x9ca30e0();
   input += synapse0x9ca3108();
   input += synapse0x9ca3130();
   input += synapse0x9ca3158();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca2e00() {
   double input = input0x9ca2e00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca3180() {
   double input = 0.482057;
   input += synapse0x9ca3398();
   input += synapse0x9ca33c0();
   input += synapse0x9ca33e8();
   input += synapse0x9ca3410();
   input += synapse0x9ca3438();
   input += synapse0x9ca3460();
   input += synapse0x9ca3488();
   input += synapse0x9ca34b0();
   input += synapse0x9ca34d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca3180() {
   double input = input0x9ca3180();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca3500() {
   double input = 0.0759089;
   input += synapse0x9ca3718();
   input += synapse0x9ca3740();
   input += synapse0x9ca3768();
   input += synapse0x9ca3790();
   input += synapse0x9ca37b8();
   input += synapse0x9ca37e0();
   input += synapse0x9ca3808();
   input += synapse0x9ca3830();
   input += synapse0x9ca3858();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca3500() {
   double input = input0x9ca3500();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca3880() {
   double input = 0.0976043;
   input += synapse0x9ca3a98();
   input += synapse0x9ca3ac0();
   input += synapse0x9ca3ae8();
   input += synapse0x9ca3b10();
   input += synapse0x9ca3b38();
   input += synapse0x9ca3b60();
   input += synapse0x9ca3b88();
   input += synapse0x9ca3bb0();
   input += synapse0x9ca3bd8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca3880() {
   double input = input0x9ca3880();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca3c00() {
   double input = 0.715046;
   input += synapse0x9ca3e18();
   input += synapse0x9ca3e40();
   input += synapse0x9ca3e68();
   input += synapse0x9ca3e90();
   input += synapse0x9ca3eb8();
   input += synapse0x9ca3ee0();
   input += synapse0x9ca3f08();
   input += synapse0x9ca3f30();
   input += synapse0x9ca3f58();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca3c00() {
   double input = input0x9ca3c00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca3f80() {
   double input = 0.22362;
   input += synapse0x9ca4198();
   input += synapse0x9ca41c0();
   input += synapse0x9ca41e8();
   input += synapse0x9ca4210();
   input += synapse0x9ca4238();
   input += synapse0x9ca4260();
   input += synapse0x9ca4288();
   input += synapse0x9ca42b0();
   input += synapse0x9ca42d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca3f80() {
   double input = input0x9ca3f80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca4300() {
   double input = -0.498713;
   input += synapse0x9ca4518();
   input += synapse0x9ca4540();
   input += synapse0x9ca4568();
   input += synapse0x9ca4590();
   input += synapse0x9ca45b8();
   input += synapse0x9ca45e0();
   input += synapse0x9ca4608();
   input += synapse0x9ca4630();
   input += synapse0x9ca4658();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca4300() {
   double input = input0x9ca4300();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca4680() {
   double input = 0.0513897;
   input += synapse0x9ca4898();
   input += synapse0x9ca48c0();
   input += synapse0x9ca48e8();
   input += synapse0x9ca4910();
   input += synapse0x9ca4938();
   input += synapse0x9ca4960();
   input += synapse0x9ca4988();
   input += synapse0x9ca49b0();
   input += synapse0x9ca49d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca4680() {
   double input = input0x9ca4680();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca4a00() {
   double input = 0.187817;
   input += synapse0x9ca4c18();
   input += synapse0x9ca4c40();
   input += synapse0x9ca4c68();
   input += synapse0x9ca4c90();
   input += synapse0x9ca4cb8();
   input += synapse0x9ca4ce0();
   input += synapse0x9ca4d08();
   input += synapse0x9ca4d30();
   input += synapse0x9ca4d58();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca4a00() {
   double input = input0x9ca4a00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca4d80() {
   double input = 0.345573;
   input += synapse0x9ca4f98();
   input += synapse0x9ca4fc0();
   input += synapse0x9ca4fe8();
   input += synapse0x9ca5010();
   input += synapse0x9ca5038();
   input += synapse0x9ca5060();
   input += synapse0x9ca5088();
   input += synapse0x9ca50b0();
   input += synapse0x9ca50d8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca4d80() {
   double input = input0x9ca4d80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::input0x9ca5100() {
   double input = 0.480044;
   input += synapse0x9ca5228();
   input += synapse0x9ca5250();
   input += synapse0x9ca5278();
   input += synapse0x9ca52a0();
   input += synapse0x9ca52c8();
   input += synapse0x9ca52f0();
   input += synapse0x9ca5318();
   input += synapse0x9ca5340();
   input += synapse0x9ca5368();
   input += synapse0x9ca5390();
   input += synapse0x9ca53b8();
   input += synapse0x9ca53e0();
   input += synapse0x9ca5408();
   input += synapse0x9ca5430();
   input += synapse0x9ca5458();
   input += synapse0x9ca5480();
   input += synapse0x9ca5530();
   input += synapse0x9ca5558();
   input += synapse0x9ca5580();
   input += synapse0x9ca55a8();
   input += synapse0x9ca55d0();
   input += synapse0x9ca55f8();
   input += synapse0x9ca5620();
   input += synapse0x9ca5648();
   input += synapse0x9ca5670();
   input += synapse0x9ca5698();
   input += synapse0x9ca56c0();
   input += synapse0x9ca56e8();
   input += synapse0x9ca5710();
   input += synapse0x9ca5738();
   input += synapse0x9ca5760();
   input += synapse0x9ca5788();
   input += synapse0x9ca54a8();
   input += synapse0x9ca54d0();
   input += synapse0x9ca54f8();
   input += synapse0x9ca58b8();
   input += synapse0x9ca58e0();
   input += synapse0x9ca5908();
   input += synapse0x9ca5930();
   input += synapse0x9ca5958();
   input += synapse0x9ca5980();
   input += synapse0x9ca59a8();
   input += synapse0x9ca59d0();
   input += synapse0x9ca59f8();
   input += synapse0x9ca5a20();
   input += synapse0x9ca5a48();
   input += synapse0x9ca5a70();
   input += synapse0x9ca5a98();
   input += synapse0x9ca5ac0();
   input += synapse0x9ca5ae8();
   input += synapse0x9ca5b10();
   input += synapse0x9ca5b38();
   input += synapse0x9ca5b60();
   input += synapse0x9ca5b88();
   input += synapse0x9ca5bb0();
   input += synapse0x9ca5bd8();
   input += synapse0x9ca5c00();
   input += synapse0x9ca5c28();
   input += synapse0x9ca5c50();
   input += synapse0x9ca5c78();
   input += synapse0x9ca5ca0();
   input += synapse0x9ca5cc8();
   input += synapse0x9ca5cf0();
   input += synapse0x9ca5d18();
   input += synapse0x9c8d2f8();
   input += synapse0x9ca57b0();
   input += synapse0x9ca57d8();
   input += synapse0x9ca5800();
   input += synapse0x9ca5828();
   input += synapse0x9ca5850();
   input += synapse0x9ca5878();
   input += synapse0x9ca5f48();
   input += synapse0x9ca5f70();
   input += synapse0x9ca5f98();
   input += synapse0x9ca5fc0();
   input += synapse0x9ca5fe8();
   input += synapse0x9ca6010();
   input += synapse0x9ca6038();
   input += synapse0x9ca6060();
   input += synapse0x9ca6088();
   input += synapse0x9ca60b0();
   input += synapse0x9ca60d8();
   input += synapse0x9ca6100();
   input += synapse0x9ca6128();
   input += synapse0x9ca6150();
   input += synapse0x9ca6178();
   input += synapse0x9ca61a0();
   input += synapse0x9ca61c8();
   input += synapse0x9ca61f0();
   input += synapse0x9ca6218();
   input += synapse0x9ca6240();
   input += synapse0x9ca6268();
   input += synapse0x9ca6290();
   input += synapse0x9ca62b8();
   input += synapse0x9ca62e0();
   input += synapse0x9ca6308();
   input += synapse0x9ca6330();
   input += synapse0x9ca6358();
   input += synapse0x9ca6380();
   input += synapse0x9ca63a8();
   return input;
}

double NNPerpGammaXYCorrectionClusterDeltaY::neuron0x9ca5100() {
   double input = input0x9ca5100();
   return (input * 1)+0;
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x8e77118() {
   return (neuron0x9c7dd70()*-0.193453);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d598() {
   return (neuron0x9c7df70()*0.211125);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d5c0() {
   return (neuron0x9c7e170()*0.053046);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d5e8() {
   return (neuron0x9c8c688()*-0.571367);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d610() {
   return (neuron0x9c8c888()*-0.200605);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d638() {
   return (neuron0x9c8ca88()*0.0544657);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d660() {
   return (neuron0x9c8cc88()*-0.113705);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d688() {
   return (neuron0x9c8ce88()*-0.318889);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d6b0() {
   return (neuron0x9c8d088()*0.67553);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d8d8() {
   return (neuron0x9c7dd70()*-0.165114);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d900() {
   return (neuron0x9c7df70()*-0.10791);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d928() {
   return (neuron0x9c7e170()*-0.40293);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d950() {
   return (neuron0x9c8c688()*-0.457006);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d978() {
   return (neuron0x9c8c888()*-0.766998);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d9a0() {
   return (neuron0x9c8ca88()*-0.19639);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d9c8() {
   return (neuron0x9c8cc88()*0.17108);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d9f0() {
   return (neuron0x9c8ce88()*-0.37406);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8daa0() {
   return (neuron0x9c8d088()*0.43151);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dc80() {
   return (neuron0x9c7dd70()*0.313223);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dca8() {
   return (neuron0x9c7df70()*0.255376);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dcd0() {
   return (neuron0x9c7e170()*0.44726);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dcf8() {
   return (neuron0x9c8c688()*0.168568);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dd20() {
   return (neuron0x9c8c888()*0.169631);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dd48() {
   return (neuron0x9c8ca88()*-0.499626);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dd70() {
   return (neuron0x9c8cc88()*0.135312);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dd98() {
   return (neuron0x9c8ce88()*0.0453629);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ddc0() {
   return (neuron0x9c8d088()*0.145196);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8dfe8() {
   return (neuron0x9c7dd70()*0.305964);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e010() {
   return (neuron0x9c7df70()*-0.322488);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e038() {
   return (neuron0x9c7e170()*-1.09042);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e060() {
   return (neuron0x9c8c688()*0.224164);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e088() {
   return (neuron0x9c8c888()*0.379365);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e0b0() {
   return (neuron0x9c8ca88()*0.107605);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8da18() {
   return (neuron0x9c8cc88()*0.920976);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8da40() {
   return (neuron0x9c8ce88()*0.144606);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8da68() {
   return (neuron0x9c8d088()*1.01873);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e3e0() {
   return (neuron0x9c7dd70()*-0.0341268);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e408() {
   return (neuron0x9c7df70()*-0.481746);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e430() {
   return (neuron0x9c7e170()*0.825985);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e458() {
   return (neuron0x9c8c688()*0.0996613);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e480() {
   return (neuron0x9c8c888()*-0.130137);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e4a8() {
   return (neuron0x9c8ca88()*0.0238276);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e4d0() {
   return (neuron0x9c8cc88()*-0.0880838);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e4f8() {
   return (neuron0x9c8ce88()*-0.0380699);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e520() {
   return (neuron0x9c8d088()*-0.0250996);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e748() {
   return (neuron0x9c7dd70()*0.17025);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e770() {
   return (neuron0x9c7df70()*-0.203599);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e798() {
   return (neuron0x9c7e170()*0.193412);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e7c0() {
   return (neuron0x9c8c688()*0.204662);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e7e8() {
   return (neuron0x9c8c888()*-0.0870452);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e810() {
   return (neuron0x9c8ca88()*-0.418718);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e838() {
   return (neuron0x9c8cc88()*0.306407);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e860() {
   return (neuron0x9c8ce88()*0.0131205);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e888() {
   return (neuron0x9c8d088()*-0.0962176);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eab0() {
   return (neuron0x9c7dd70()*0.154142);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ead8() {
   return (neuron0x9c7df70()*0.274641);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eb00() {
   return (neuron0x9c7e170()*-1.13478);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eb28() {
   return (neuron0x9c8c688()*0.091234);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eb50() {
   return (neuron0x9c8c888()*0.0280606);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eb78() {
   return (neuron0x9c8ca88()*0.0289241);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eba0() {
   return (neuron0x9c8cc88()*-0.315736);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ebc8() {
   return (neuron0x9c8ce88()*-0.0713913);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ebf0() {
   return (neuron0x9c8d088()*1.6038);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eea0() {
   return (neuron0x9c7dd70()*-0.090644);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eec8() {
   return (neuron0x9c7df70()*0.548753);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x8e770e0() {
   return (neuron0x9c7e170()*0.177833);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x8e76eb8() {
   return (neuron0x9c8c688()*-0.0868059);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x8e76a18() {
   return (neuron0x9c8c888()*-0.212747);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x8e76f88() {
   return (neuron0x9c8ca88()*0.418821);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x8e76fb0() {
   return (neuron0x9c8cc88()*0.129909);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e0d8() {
   return (neuron0x9c8ce88()*0.0933045);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e100() {
   return (neuron0x9c8d088()*0.37955);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8e1b8() {
   return (neuron0x9c7dd70()*-0.459226);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f268() {
   return (neuron0x9c7df70()*-0.0749415);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f290() {
   return (neuron0x9c7e170()*-1.02018);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f2b8() {
   return (neuron0x9c8c688()*-0.0953773);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f2e0() {
   return (neuron0x9c8c888()*-0.382223);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f308() {
   return (neuron0x9c8ca88()*0.0120998);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f330() {
   return (neuron0x9c8cc88()*0.194475);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f358() {
   return (neuron0x9c8ce88()*0.00245011);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f380() {
   return (neuron0x9c8d088()*-0.529283);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f5a8() {
   return (neuron0x9c7dd70()*0.351787);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f5d0() {
   return (neuron0x9c7df70()*-0.298841);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f5f8() {
   return (neuron0x9c7e170()*-0.314297);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f620() {
   return (neuron0x9c8c688()*-0.26058);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f648() {
   return (neuron0x9c8c888()*0.394066);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f670() {
   return (neuron0x9c8ca88()*-0.322794);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f698() {
   return (neuron0x9c8cc88()*0.594774);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f6c0() {
   return (neuron0x9c8ce88()*0.0573638);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f6e8() {
   return (neuron0x9c8d088()*-0.804049);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f910() {
   return (neuron0x9c7dd70()*0.328034);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f938() {
   return (neuron0x9c7df70()*-0.534921);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f960() {
   return (neuron0x9c7e170()*-0.079701);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f988() {
   return (neuron0x9c8c688()*0.00430099);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f9b0() {
   return (neuron0x9c8c888()*0.183005);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f9d8() {
   return (neuron0x9c8ca88()*-0.0427503);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fa00() {
   return (neuron0x9c8cc88()*0.412477);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fa28() {
   return (neuron0x9c8ce88()*0.131008);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fa50() {
   return (neuron0x9c8d088()*0.105445);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fc78() {
   return (neuron0x9c7dd70()*-0.409174);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fca0() {
   return (neuron0x9c7df70()*-0.239097);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fcc8() {
   return (neuron0x9c7e170()*1.04417);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fcf0() {
   return (neuron0x9c8c688()*-0.394107);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fd18() {
   return (neuron0x9c8c888()*-1.0805);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fd40() {
   return (neuron0x9c8ca88()*0.219325);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fd68() {
   return (neuron0x9c8cc88()*1.00434);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fd90() {
   return (neuron0x9c8ce88()*0.0615576);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8fdb8() {
   return (neuron0x9c8d088()*-1.7238);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ffe0() {
   return (neuron0x9c7dd70()*-0.0674325);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90008() {
   return (neuron0x9c7df70()*0.183275);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90030() {
   return (neuron0x9c7e170()*-0.122577);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90058() {
   return (neuron0x9c8c688()*-0.266217);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90080() {
   return (neuron0x9c8c888()*0.166886);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c900a8() {
   return (neuron0x9c8ca88()*0.364704);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c900d0() {
   return (neuron0x9c8cc88()*-0.477827);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c900f8() {
   return (neuron0x9c8ce88()*0.340693);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90120() {
   return (neuron0x9c8d088()*-0.208172);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90348() {
   return (neuron0x9c7dd70()*-0.0921348);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90370() {
   return (neuron0x9c7df70()*-0.0337663);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90398() {
   return (neuron0x9c7e170()*1.06174);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c903c0() {
   return (neuron0x9c8c688()*-0.0427819);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c903e8() {
   return (neuron0x9c8c888()*1.18315);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90410() {
   return (neuron0x9c8ca88()*0.0202058);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90438() {
   return (neuron0x9c8cc88()*-1.71786);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90460() {
   return (neuron0x9c8ce88()*-0.0170807);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90488() {
   return (neuron0x9c8d088()*1.80702);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c906b0() {
   return (neuron0x9c7dd70()*0.146405);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c906d8() {
   return (neuron0x9c7df70()*-0.265966);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90700() {
   return (neuron0x9c7e170()*0.148588);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8eef0() {
   return (neuron0x9c8c688()*-0.3695);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ef18() {
   return (neuron0x9c8c888()*-0.0340863);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ef40() {
   return (neuron0x9c8ca88()*-0.40867);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ef68() {
   return (neuron0x9c8cc88()*-0.314443);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ef90() {
   return (neuron0x9c8ce88()*-0.653713);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8efb8() {
   return (neuron0x9c8d088()*-0.685733);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8f0b8() {
   return (neuron0x9c7dd70()*-0.0552487);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90ce0() {
   return (neuron0x9c7df70()*-0.11603);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90d90() {
   return (neuron0x9c7e170()*1.54505);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90e40() {
   return (neuron0x9c8c688()*-0.0398996);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90ef0() {
   return (neuron0x9c8c888()*0.912988);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90fa0() {
   return (neuron0x9c8ca88()*-0.021683);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91050() {
   return (neuron0x9c8cc88()*0.457103);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91100() {
   return (neuron0x9c8ce88()*0.0689945);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c911b0() {
   return (neuron0x9c8d088()*-0.841031);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91388() {
   return (neuron0x9c7dd70()*0.016881);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c913b0() {
   return (neuron0x9c7df70()*-0.127213);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c913d8() {
   return (neuron0x9c7e170()*-0.229608);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91400() {
   return (neuron0x9c8c688()*-0.0161878);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91428() {
   return (neuron0x9c8c888()*-0.0476733);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91450() {
   return (neuron0x9c8ca88()*0.24206);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91478() {
   return (neuron0x9c8cc88()*0.497722);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c914a0() {
   return (neuron0x9c8ce88()*0.31182);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c914c8() {
   return (neuron0x9c8d088()*0.157137);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91618() {
   return (neuron0x9c7dd70()*-0.0237649);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91640() {
   return (neuron0x9c7df70()*-0.422165);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91668() {
   return (neuron0x9c7e170()*0.355014);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91690() {
   return (neuron0x9c8c688()*-0.0828335);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c916b8() {
   return (neuron0x9c8c888()*-0.285443);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c916e0() {
   return (neuron0x9c8ca88()*-0.37438);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91708() {
   return (neuron0x9c8cc88()*-0.190424);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91730() {
   return (neuron0x9c8ce88()*-0.260406);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91758() {
   return (neuron0x9c8d088()*0.0423417);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c918a8() {
   return (neuron0x9c7dd70()*-0.251524);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c918d0() {
   return (neuron0x9c7df70()*-0.00591519);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c918f8() {
   return (neuron0x9c7e170()*-0.0750589);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91920() {
   return (neuron0x9c8c688()*-0.108139);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91948() {
   return (neuron0x9c8c888()*0.417378);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91970() {
   return (neuron0x9c8ca88()*-0.218848);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91998() {
   return (neuron0x9c8cc88()*0.0450611);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c919c0() {
   return (neuron0x9c8ce88()*-0.0109188);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c919e8() {
   return (neuron0x9c8d088()*0.0232731);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91c28() {
   return (neuron0x9c7dd70()*-0.39871);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91c50() {
   return (neuron0x9c7df70()*0.426628);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91c78() {
   return (neuron0x9c7e170()*-0.580619);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91ca0() {
   return (neuron0x9c8c688()*0.326167);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91cc8() {
   return (neuron0x9c8c888()*-0.650112);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91cf0() {
   return (neuron0x9c8ca88()*-0.133542);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91d18() {
   return (neuron0x9c8cc88()*0.483254);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91d40() {
   return (neuron0x9c8ce88()*0.116555);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91d68() {
   return (neuron0x9c8d088()*0.541555);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91fa8() {
   return (neuron0x9c7dd70()*0.111327);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91fd0() {
   return (neuron0x9c7df70()*0.0178022);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91ff8() {
   return (neuron0x9c7e170()*-1.22381);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92020() {
   return (neuron0x9c8c688()*0.0723686);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92048() {
   return (neuron0x9c8c888()*0.775244);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92070() {
   return (neuron0x9c8ca88()*-0.0291616);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92098() {
   return (neuron0x9c8cc88()*2.12902);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c920c0() {
   return (neuron0x9c8ce88()*0.0728857);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c920e8() {
   return (neuron0x9c8d088()*1.8643);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92328() {
   return (neuron0x9c7dd70()*-0.0738932);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92350() {
   return (neuron0x9c7df70()*-0.0126559);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92378() {
   return (neuron0x9c7e170()*1.66575);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c923a0() {
   return (neuron0x9c8c688()*-0.0930565);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c923c8() {
   return (neuron0x9c8c888()*-0.137938);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c923f0() {
   return (neuron0x9c8ca88()*0.0308762);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92418() {
   return (neuron0x9c8cc88()*-2.28211);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92440() {
   return (neuron0x9c8ce88()*-0.0693353);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92468() {
   return (neuron0x9c8d088()*-1.94189);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c926a8() {
   return (neuron0x9c7dd70()*-0.121405);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c926d0() {
   return (neuron0x9c7df70()*0.0394408);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c926f8() {
   return (neuron0x9c7e170()*0.095486);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92720() {
   return (neuron0x9c8c688()*0.232286);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92748() {
   return (neuron0x9c8c888()*-0.305962);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92770() {
   return (neuron0x9c8ca88()*-0.251591);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92798() {
   return (neuron0x9c8cc88()*-0.139906);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c927c0() {
   return (neuron0x9c8ce88()*-0.442208);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c927e8() {
   return (neuron0x9c8d088()*0.400075);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ee18() {
   return (neuron0x9c7dd70()*0.0649966);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ee40() {
   return (neuron0x9c7df70()*-0.0220144);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8ee68() {
   return (neuron0x9c7e170()*0.379319);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92b30() {
   return (neuron0x9c8c688()*-0.130423);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92b58() {
   return (neuron0x9c8c888()*-1.06514);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92b80() {
   return (neuron0x9c8ca88()*0.117781);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92ba8() {
   return (neuron0x9c8cc88()*-1.13364);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92bd0() {
   return (neuron0x9c8ce88()*-0.480079);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92bf8() {
   return (neuron0x9c8d088()*0.252909);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92e38() {
   return (neuron0x9c7dd70()*-0.201087);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92e60() {
   return (neuron0x9c7df70()*-0.192958);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92e88() {
   return (neuron0x9c7e170()*0.572553);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92eb0() {
   return (neuron0x9c8c688()*0.297633);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92ed8() {
   return (neuron0x9c8c888()*0.519969);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92f00() {
   return (neuron0x9c8ca88()*-0.0332801);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92f28() {
   return (neuron0x9c8cc88()*0.120702);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92f50() {
   return (neuron0x9c8ce88()*-0.405497);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92f78() {
   return (neuron0x9c8d088()*-0.578287);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c931b8() {
   return (neuron0x9c7dd70()*-0.666327);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c931e0() {
   return (neuron0x9c7df70()*0.420443);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93208() {
   return (neuron0x9c7e170()*0.236971);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93230() {
   return (neuron0x9c8c688()*-0.00108718);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93258() {
   return (neuron0x9c8c888()*-0.00794571);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93280() {
   return (neuron0x9c8ca88()*0.362967);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c932a8() {
   return (neuron0x9c8cc88()*-0.159761);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c932d0() {
   return (neuron0x9c8ce88()*-0.104004);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c932f8() {
   return (neuron0x9c8d088()*0.326918);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93538() {
   return (neuron0x9c7dd70()*0.132225);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93560() {
   return (neuron0x9c7df70()*0.191445);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93588() {
   return (neuron0x9c7e170()*-0.310146);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c935b0() {
   return (neuron0x9c8c688()*0.188318);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c935d8() {
   return (neuron0x9c8c888()*0.420448);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93600() {
   return (neuron0x9c8ca88()*-0.107431);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93628() {
   return (neuron0x9c8cc88()*-0.493354);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93650() {
   return (neuron0x9c8ce88()*0.3022);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93678() {
   return (neuron0x9c8d088()*0.0434429);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c938b8() {
   return (neuron0x9c7dd70()*0.129124);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c938e0() {
   return (neuron0x9c7df70()*0.67164);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93908() {
   return (neuron0x9c7e170()*-0.208372);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93930() {
   return (neuron0x9c8c688()*0.117221);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93958() {
   return (neuron0x9c8c888()*-0.454434);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93980() {
   return (neuron0x9c8ca88()*-0.581468);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c939a8() {
   return (neuron0x9c8cc88()*0.150795);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c939d0() {
   return (neuron0x9c8ce88()*-0.00403355);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c939f8() {
   return (neuron0x9c8d088()*-0.833278);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93c38() {
   return (neuron0x9c7dd70()*-0.283616);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93c60() {
   return (neuron0x9c7df70()*-0.455078);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93c88() {
   return (neuron0x9c7e170()*-0.275858);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93cb0() {
   return (neuron0x9c8c688()*0.184189);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93cd8() {
   return (neuron0x9c8c888()*0.117012);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90728() {
   return (neuron0x9c8ca88()*0.0410834);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90750() {
   return (neuron0x9c8cc88()*-0.472966);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90778() {
   return (neuron0x9c8ce88()*-0.377387);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c907a0() {
   return (neuron0x9c8d088()*0.343546);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c909e0() {
   return (neuron0x9c7dd70()*-0.222062);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90a08() {
   return (neuron0x9c7df70()*-0.355895);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90a30() {
   return (neuron0x9c7e170()*0.155928);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90a58() {
   return (neuron0x9c8c688()*0.12691);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90a80() {
   return (neuron0x9c8c888()*-0.3945);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90aa8() {
   return (neuron0x9c8ca88()*0.0932754);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90ad0() {
   return (neuron0x9c8cc88()*0.340801);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90af8() {
   return (neuron0x9c8ce88()*-0.237083);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94508() {
   return (neuron0x9c8d088()*-0.258234);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94748() {
   return (neuron0x9c7dd70()*0.169266);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94770() {
   return (neuron0x9c7df70()*0.0602608);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94798() {
   return (neuron0x9c7e170()*0.953647);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c947c0() {
   return (neuron0x9c8c688()*-0.0744698);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c947e8() {
   return (neuron0x9c8c888()*0.652386);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94810() {
   return (neuron0x9c8ca88()*0.15458);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94838() {
   return (neuron0x9c8cc88()*-1.32963);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94860() {
   return (neuron0x9c8ce88()*0.00918455);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94888() {
   return (neuron0x9c8d088()*1.71765);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94ac8() {
   return (neuron0x9c7dd70()*-0.749049);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90c58() {
   return (neuron0x9c7df70()*0.387222);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90c80() {
   return (neuron0x9c7e170()*-0.0943169);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90ca8() {
   return (neuron0x9c8c688()*-0.155624);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90d08() {
   return (neuron0x9c8c888()*-0.500854);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90d30() {
   return (neuron0x9c8ca88()*-0.246884);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90d58() {
   return (neuron0x9c8cc88()*-0.513423);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90db8() {
   return (neuron0x9c8ce88()*0.12407);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90de0() {
   return (neuron0x9c8d088()*0.0555814);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91010() {
   return (neuron0x9c7dd70()*0.216587);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90eb0() {
   return (neuron0x9c7df70()*0.00956457);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c90f60() {
   return (neuron0x9c7e170()*-0.760976);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91078() {
   return (neuron0x9c8c688()*-0.0109882);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c910a0() {
   return (neuron0x9c8c888()*-1.0585);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c910c8() {
   return (neuron0x9c8ca88()*-0.146778);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91128() {
   return (neuron0x9c8cc88()*2.39919);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91150() {
   return (neuron0x9c8ce88()*0.0224127);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91178() {
   return (neuron0x9c8d088()*1.60673);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c91220() {
   return (neuron0x9c7dd70()*0.0381686);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95718() {
   return (neuron0x9c7df70()*-0.0107047);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95740() {
   return (neuron0x9c7e170()*0.212873);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95768() {
   return (neuron0x9c8c688()*0.0411583);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95790() {
   return (neuron0x9c8c888()*0.500327);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c957b8() {
   return (neuron0x9c8ca88()*-0.419408);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c957e0() {
   return (neuron0x9c8cc88()*-0.563292);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95808() {
   return (neuron0x9c8ce88()*0.35597);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95830() {
   return (neuron0x9c8d088()*-0.565339);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95a58() {
   return (neuron0x9c7dd70()*-0.52179);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95a80() {
   return (neuron0x9c7df70()*-0.0517645);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95aa8() {
   return (neuron0x9c7e170()*0.745769);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95ad0() {
   return (neuron0x9c8c688()*0.102726);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95af8() {
   return (neuron0x9c8c888()*0.26115);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95b20() {
   return (neuron0x9c8ca88()*-0.665682);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95b48() {
   return (neuron0x9c8cc88()*0.281389);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95b70() {
   return (neuron0x9c8ce88()*0.0956855);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95b98() {
   return (neuron0x9c8d088()*0.057772);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95dc0() {
   return (neuron0x9c7dd70()*0.304577);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95de8() {
   return (neuron0x9c7df70()*-0.392501);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95e10() {
   return (neuron0x9c7e170()*-0.0345431);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95e38() {
   return (neuron0x9c8c688()*0.00959632);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95e60() {
   return (neuron0x9c8c888()*-0.276561);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95e88() {
   return (neuron0x9c8ca88()*0.0659661);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95eb0() {
   return (neuron0x9c8cc88()*0.447523);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95ed8() {
   return (neuron0x9c8ce88()*0.0417753);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95f00() {
   return (neuron0x9c8d088()*0.164026);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96128() {
   return (neuron0x9c7dd70()*-0.152972);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96150() {
   return (neuron0x9c7df70()*-0.0754702);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96178() {
   return (neuron0x9c7e170()*-0.0977674);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c961a0() {
   return (neuron0x9c8c688()*-0.457883);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c961c8() {
   return (neuron0x9c8c888()*0.424528);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c961f0() {
   return (neuron0x9c8ca88()*-0.0805628);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96218() {
   return (neuron0x9c8cc88()*0.31308);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96240() {
   return (neuron0x9c8ce88()*0.041583);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96268() {
   return (neuron0x9c8d088()*-0.205291);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96490() {
   return (neuron0x9c7dd70()*-0.225217);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c964b8() {
   return (neuron0x9c7df70()*0.0312799);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c964e0() {
   return (neuron0x9c7e170()*0.131494);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96508() {
   return (neuron0x9c8c688()*0.0719934);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96530() {
   return (neuron0x9c8c888()*0.00469202);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96558() {
   return (neuron0x9c8ca88()*0.149386);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96580() {
   return (neuron0x9c8cc88()*-0.328955);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c965a8() {
   return (neuron0x9c8ce88()*0.501231);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c965d0() {
   return (neuron0x9c8d088()*-0.173792);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96810() {
   return (neuron0x9c7dd70()*0.299743);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96838() {
   return (neuron0x9c7df70()*0.131134);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96860() {
   return (neuron0x9c7e170()*-0.0484339);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96888() {
   return (neuron0x9c8c688()*-0.0976722);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c968b0() {
   return (neuron0x9c8c888()*0.58467);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c968d8() {
   return (neuron0x9c8ca88()*-0.570327);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96900() {
   return (neuron0x9c8cc88()*0.484991);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96928() {
   return (neuron0x9c8ce88()*-0.0977309);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96950() {
   return (neuron0x9c8d088()*-0.483182);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96b90() {
   return (neuron0x9c7dd70()*0.014012);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96bb8() {
   return (neuron0x9c7df70()*-0.0578865);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96be0() {
   return (neuron0x9c7e170()*1.41542);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96c08() {
   return (neuron0x9c8c688()*0.129592);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96c30() {
   return (neuron0x9c8c888()*0.417669);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96c58() {
   return (neuron0x9c8ca88()*-0.0724615);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96c80() {
   return (neuron0x9c8cc88()*-2.08102);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96ca8() {
   return (neuron0x9c8ce88()*0.0229695);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96cd0() {
   return (neuron0x9c8d088()*1.86805);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96f10() {
   return (neuron0x9c7dd70()*0.344248);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96f38() {
   return (neuron0x9c7df70()*0.0788194);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96f60() {
   return (neuron0x9c7e170()*-1.20647);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96f88() {
   return (neuron0x9c8c688()*0.557804);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96fb0() {
   return (neuron0x9c8c888()*0.363525);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c96fd8() {
   return (neuron0x9c8ca88()*-0.081494);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97000() {
   return (neuron0x9c8cc88()*-0.510245);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97028() {
   return (neuron0x9c8ce88()*0.203809);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97050() {
   return (neuron0x9c8d088()*-0.490766);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97290() {
   return (neuron0x9c7dd70()*-0.0273914);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c972b8() {
   return (neuron0x9c7df70()*-0.251177);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c972e0() {
   return (neuron0x9c7e170()*-0.0230366);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97308() {
   return (neuron0x9c8c688()*0.269233);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97330() {
   return (neuron0x9c8c888()*-0.0918376);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97358() {
   return (neuron0x9c8ca88()*-0.0979919);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97380() {
   return (neuron0x9c8cc88()*0.581633);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c973a8() {
   return (neuron0x9c8ce88()*0.220052);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c973d0() {
   return (neuron0x9c8d088()*0.0773121);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97610() {
   return (neuron0x9c7dd70()*-0.400249);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97638() {
   return (neuron0x9c7df70()*-0.412224);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97660() {
   return (neuron0x9c7e170()*0.00492937);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97688() {
   return (neuron0x9c8c688()*0.482698);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c976b0() {
   return (neuron0x9c8c888()*0.0128631);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c976d8() {
   return (neuron0x9c8ca88()*0.298342);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97700() {
   return (neuron0x9c8cc88()*-0.542589);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97728() {
   return (neuron0x9c8ce88()*-0.492155);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97750() {
   return (neuron0x9c8d088()*-0.275622);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97990() {
   return (neuron0x9c7dd70()*0.224251);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c979b8() {
   return (neuron0x9c7df70()*-0.293513);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c979e0() {
   return (neuron0x9c7e170()*-0.257786);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97a08() {
   return (neuron0x9c8c688()*0.261317);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97a30() {
   return (neuron0x9c8c888()*0.40745);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97a58() {
   return (neuron0x9c8ca88()*-0.106116);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97a80() {
   return (neuron0x9c8cc88()*-0.708519);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97aa8() {
   return (neuron0x9c8ce88()*-0.0833251);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97ad0() {
   return (neuron0x9c8d088()*-0.14686);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97d10() {
   return (neuron0x9c7dd70()*0.157159);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97d38() {
   return (neuron0x9c7df70()*0.0809755);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97d60() {
   return (neuron0x9c7e170()*1.01632);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97d88() {
   return (neuron0x9c8c688()*0.0716669);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97db0() {
   return (neuron0x9c8c888()*0.222986);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97dd8() {
   return (neuron0x9c8ca88()*-0.0591246);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97e00() {
   return (neuron0x9c8cc88()*1.16632);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97e28() {
   return (neuron0x9c8ce88()*0.0327654);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c97e50() {
   return (neuron0x9c8d088()*0.943511);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98090() {
   return (neuron0x9c7dd70()*-0.133301);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c980b8() {
   return (neuron0x9c7df70()*-0.916818);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c980e0() {
   return (neuron0x9c7e170()*-0.356188);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98108() {
   return (neuron0x9c8c688()*0.151583);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98130() {
   return (neuron0x9c8c888()*0.476374);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98158() {
   return (neuron0x9c8ca88()*0.00070455);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98180() {
   return (neuron0x9c8cc88()*-0.182256);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c981a8() {
   return (neuron0x9c8ce88()*-0.0517407);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c981d0() {
   return (neuron0x9c8d088()*0.510611);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98410() {
   return (neuron0x9c7dd70()*0.128247);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98438() {
   return (neuron0x9c7df70()*-0.122929);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98460() {
   return (neuron0x9c7e170()*0.0402527);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98488() {
   return (neuron0x9c8c688()*0.405307);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c984b0() {
   return (neuron0x9c8c888()*-0.0750718);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c984d8() {
   return (neuron0x9c8ca88()*0.141349);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98500() {
   return (neuron0x9c8cc88()*0.189349);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98528() {
   return (neuron0x9c8ce88()*0.10169);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98550() {
   return (neuron0x9c8d088()*-0.376171);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98790() {
   return (neuron0x9c7dd70()*0.0361712);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c987b8() {
   return (neuron0x9c7df70()*0.161746);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c987e0() {
   return (neuron0x9c7e170()*0.0618448);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98808() {
   return (neuron0x9c8c688()*-0.311441);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98830() {
   return (neuron0x9c8c888()*-0.497612);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98858() {
   return (neuron0x9c8ca88()*0.570399);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98880() {
   return (neuron0x9c8cc88()*-0.353757);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c988a8() {
   return (neuron0x9c8ce88()*-0.0884243);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c988d0() {
   return (neuron0x9c8d088()*0.651247);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98b10() {
   return (neuron0x9c7dd70()*0.378786);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98b38() {
   return (neuron0x9c7df70()*-0.281401);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98b60() {
   return (neuron0x9c7e170()*-0.0606148);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98b88() {
   return (neuron0x9c8c688()*-0.127464);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98bb0() {
   return (neuron0x9c8c888()*-0.232573);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98bd8() {
   return (neuron0x9c8ca88()*-0.248467);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98c00() {
   return (neuron0x9c8cc88()*-0.0178621);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98c28() {
   return (neuron0x9c8ce88()*0.0606731);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98c50() {
   return (neuron0x9c8d088()*-0.166144);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98e90() {
   return (neuron0x9c7dd70()*0.175709);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98eb8() {
   return (neuron0x9c7df70()*-0.0208455);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98ee0() {
   return (neuron0x9c7e170()*0.0945177);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98f08() {
   return (neuron0x9c8c688()*0.155119);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98f30() {
   return (neuron0x9c8c888()*0.326182);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98f58() {
   return (neuron0x9c8ca88()*-0.233505);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98f80() {
   return (neuron0x9c8cc88()*-0.0918925);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98fa8() {
   return (neuron0x9c8ce88()*-0.228517);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c98fd0() {
   return (neuron0x9c8d088()*-0.444733);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99210() {
   return (neuron0x9c7dd70()*0.471837);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99238() {
   return (neuron0x9c7df70()*0.128487);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99260() {
   return (neuron0x9c7e170()*-0.930537);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99288() {
   return (neuron0x9c8c688()*-0.467881);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c992b0() {
   return (neuron0x9c8c888()*0.281015);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c992d8() {
   return (neuron0x9c8ca88()*0.385162);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99300() {
   return (neuron0x9c8cc88()*0.736382);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99328() {
   return (neuron0x9c8ce88()*0.347459);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99350() {
   return (neuron0x9c8d088()*-0.16931);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99590() {
   return (neuron0x9c7dd70()*0.0651306);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c995b8() {
   return (neuron0x9c7df70()*-0.247172);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c995e0() {
   return (neuron0x9c7e170()*0.418265);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99608() {
   return (neuron0x9c8c688()*0.044021);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99630() {
   return (neuron0x9c8c888()*-0.373703);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99658() {
   return (neuron0x9c8ca88()*-0.326984);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99680() {
   return (neuron0x9c8cc88()*-0.238794);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c996a8() {
   return (neuron0x9c8ce88()*0.138376);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c996d0() {
   return (neuron0x9c8d088()*0.565576);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99910() {
   return (neuron0x9c7dd70()*0.191295);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99938() {
   return (neuron0x9c7df70()*-0.0301037);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99960() {
   return (neuron0x9c7e170()*0.268342);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99988() {
   return (neuron0x9c8c688()*0.17585);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c999b0() {
   return (neuron0x9c8c888()*-0.376491);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c999d8() {
   return (neuron0x9c8ca88()*-0.265736);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99a00() {
   return (neuron0x9c8cc88()*-0.493347);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99a28() {
   return (neuron0x9c8ce88()*0.415509);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99a50() {
   return (neuron0x9c8d088()*-0.00787559);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99c90() {
   return (neuron0x9c7dd70()*-0.453146);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99cb8() {
   return (neuron0x9c7df70()*-0.0582831);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99ce0() {
   return (neuron0x9c7e170()*-0.910218);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99d08() {
   return (neuron0x9c8c688()*0.0251238);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99d30() {
   return (neuron0x9c8c888()*-0.205029);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99d58() {
   return (neuron0x9c8ca88()*-0.233896);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99d80() {
   return (neuron0x9c8cc88()*-0.219422);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99da8() {
   return (neuron0x9c8ce88()*-0.22378);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c99dd0() {
   return (neuron0x9c8d088()*-0.91268);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a010() {
   return (neuron0x9c7dd70()*0.262121);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a038() {
   return (neuron0x9c7df70()*-0.409549);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a060() {
   return (neuron0x9c7e170()*0.374328);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a088() {
   return (neuron0x9c8c688()*-0.362775);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a0b0() {
   return (neuron0x9c8c888()*0.478518);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a0d8() {
   return (neuron0x9c8ca88()*-0.184462);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a100() {
   return (neuron0x9c8cc88()*0.368026);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a128() {
   return (neuron0x9c8ce88()*-0.228111);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a150() {
   return (neuron0x9c8d088()*-0.0756548);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92a28() {
   return (neuron0x9c7dd70()*-0.620156);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92a50() {
   return (neuron0x9c7df70()*-0.154855);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92a78() {
   return (neuron0x9c7e170()*0.958641);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92aa0() {
   return (neuron0x9c8c688()*-0.243888);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92ac8() {
   return (neuron0x9c8c888()*-0.204342);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c92af0() {
   return (neuron0x9c8ca88()*0.167432);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a598() {
   return (neuron0x9c8cc88()*0.866137);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a5c0() {
   return (neuron0x9c8ce88()*-0.193657);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a5e8() {
   return (neuron0x9c8d088()*-0.144414);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a810() {
   return (neuron0x9c7dd70()*0.328066);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a838() {
   return (neuron0x9c7df70()*0.272975);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a860() {
   return (neuron0x9c7e170()*0.0180469);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a888() {
   return (neuron0x9c8c688()*0.287728);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a8b0() {
   return (neuron0x9c8c888()*-0.405562);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a8d8() {
   return (neuron0x9c8ca88()*0.303994);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a900() {
   return (neuron0x9c8cc88()*-0.0419643);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a928() {
   return (neuron0x9c8ce88()*0.104958);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9a950() {
   return (neuron0x9c8d088()*0.305827);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93f18() {
   return (neuron0x9c7dd70()*0.211312);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93f40() {
   return (neuron0x9c7df70()*0.33346);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93f68() {
   return (neuron0x9c7e170()*0.330255);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93f90() {
   return (neuron0x9c8c688()*-0.374794);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93fb8() {
   return (neuron0x9c8c888()*-0.299353);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c93fe0() {
   return (neuron0x9c8ca88()*0.41733);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94008() {
   return (neuron0x9c8cc88()*-0.317792);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94030() {
   return (neuron0x9c8ce88()*0.308644);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94058() {
   return (neuron0x9c8d088()*0.350579);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94298() {
   return (neuron0x9c7dd70()*-0.284648);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c942c0() {
   return (neuron0x9c7df70()*0.325718);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c942e8() {
   return (neuron0x9c7e170()*-0.0899317);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94310() {
   return (neuron0x9c8c688()*-0.0268589);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94338() {
   return (neuron0x9c8c888()*-0.337463);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94360() {
   return (neuron0x9c8ca88()*0.495726);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94388() {
   return (neuron0x9c8cc88()*-0.0276809);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c943b0() {
   return (neuron0x9c8ce88()*-0.481253);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c943d8() {
   return (neuron0x9c8d088()*0.406178);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9baa8() {
   return (neuron0x9c7dd70()*0.421949);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bad0() {
   return (neuron0x9c7df70()*0.283266);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9baf8() {
   return (neuron0x9c7e170()*-0.56151);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bb20() {
   return (neuron0x9c8c688()*-0.00601399);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bb48() {
   return (neuron0x9c8c888()*0.205264);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bb70() {
   return (neuron0x9c8ca88()*-0.0957447);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bb98() {
   return (neuron0x9c8cc88()*0.192661);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bbc0() {
   return (neuron0x9c8ce88()*0.104252);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bbe8() {
   return (neuron0x9c8d088()*0.317904);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9be10() {
   return (neuron0x9c7dd70()*-0.424899);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9be38() {
   return (neuron0x9c7df70()*0.243715);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9be60() {
   return (neuron0x9c7e170()*0.0104612);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9be88() {
   return (neuron0x9c8c688()*-0.0620085);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9beb0() {
   return (neuron0x9c8c888()*-0.361496);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bed8() {
   return (neuron0x9c8ca88()*-0.160282);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bf00() {
   return (neuron0x9c8cc88()*0.110878);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bf28() {
   return (neuron0x9c8ce88()*-0.410645);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9bf50() {
   return (neuron0x9c8d088()*-0.463399);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c190() {
   return (neuron0x9c7dd70()*-0.239093);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c1b8() {
   return (neuron0x9c7df70()*0.295427);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c1e0() {
   return (neuron0x9c7e170()*-0.321035);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c208() {
   return (neuron0x9c8c688()*-0.324434);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c230() {
   return (neuron0x9c8c888()*-0.301491);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c258() {
   return (neuron0x9c8ca88()*-0.637154);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c280() {
   return (neuron0x9c8cc88()*-0.0516978);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c2a8() {
   return (neuron0x9c8ce88()*-0.498783);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c2d0() {
   return (neuron0x9c8d088()*0.098365);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c510() {
   return (neuron0x9c7dd70()*0.487656);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c538() {
   return (neuron0x9c7df70()*0.118516);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c560() {
   return (neuron0x9c7e170()*-0.049822);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c588() {
   return (neuron0x9c8c688()*0.0014066);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c5b0() {
   return (neuron0x9c8c888()*0.0983445);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c5d8() {
   return (neuron0x9c8ca88()*-0.0762761);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c600() {
   return (neuron0x9c8cc88()*0.000493087);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c628() {
   return (neuron0x9c8ce88()*0.287848);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c650() {
   return (neuron0x9c8d088()*0.431427);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9c890() {
   return (neuron0x9c7dd70()*0.451197);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94af0() {
   return (neuron0x9c7df70()*-0.123609);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94b18() {
   return (neuron0x9c7e170()*0.804359);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94b40() {
   return (neuron0x9c8c688()*0.40592);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94d70() {
   return (neuron0x9c8c888()*1.07267);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94d98() {
   return (neuron0x9c8ca88()*-0.0191323);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94fc8() {
   return (neuron0x9c8cc88()*0.301059);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c94ff0() {
   return (neuron0x9c8ce88()*0.414022);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c95228() {
   return (neuron0x9c8d088()*-0.0468443);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d530() {
   return (neuron0x9c7dd70()*-0.264148);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d558() {
   return (neuron0x9c7df70()*-0.60204);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d580() {
   return (neuron0x9c7e170()*0.141611);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d5a8() {
   return (neuron0x9c8c688()*0.245481);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d5d0() {
   return (neuron0x9c8c888()*0.223324);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d5f8() {
   return (neuron0x9c8ca88()*0.0453871);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d620() {
   return (neuron0x9c8cc88()*0.261913);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d648() {
   return (neuron0x9c8ce88()*-0.139865);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d670() {
   return (neuron0x9c8d088()*0.680188);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d898() {
   return (neuron0x9c7dd70()*-0.206479);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d8c0() {
   return (neuron0x9c7df70()*0.0255114);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d8e8() {
   return (neuron0x9c7e170()*0.371712);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d910() {
   return (neuron0x9c8c688()*0.423165);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d938() {
   return (neuron0x9c8c888()*0.137016);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d960() {
   return (neuron0x9c8ca88()*-0.0625752);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d988() {
   return (neuron0x9c8cc88()*-0.441215);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d9b0() {
   return (neuron0x9c8ce88()*0.129484);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9d9d8() {
   return (neuron0x9c8d088()*0.657379);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dc18() {
   return (neuron0x9c7dd70()*-0.266023);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dc40() {
   return (neuron0x9c7df70()*0.276409);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dc68() {
   return (neuron0x9c7e170()*1.0183);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dc90() {
   return (neuron0x9c8c688()*0.178557);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dcb8() {
   return (neuron0x9c8c888()*-1.32768);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dce0() {
   return (neuron0x9c8ca88()*-0.0961583);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dd08() {
   return (neuron0x9c8cc88()*0.387987);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dd30() {
   return (neuron0x9c8ce88()*-0.124238);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dd58() {
   return (neuron0x9c8d088()*0.588684);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9df98() {
   return (neuron0x9c7dd70()*0.0801388);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dfc0() {
   return (neuron0x9c7df70()*0.0393217);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9dfe8() {
   return (neuron0x9c7e170()*-1.33701);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e010() {
   return (neuron0x9c8c688()*0.0858547);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e038() {
   return (neuron0x9c8c888()*-1.74508);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e060() {
   return (neuron0x9c8ca88()*-0.0494165);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e088() {
   return (neuron0x9c8cc88()*-0.259651);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e0b0() {
   return (neuron0x9c8ce88()*0.0260734);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e0d8() {
   return (neuron0x9c8d088()*0.78775);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e318() {
   return (neuron0x9c7dd70()*0.0370378);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e340() {
   return (neuron0x9c7df70()*-0.26076);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e368() {
   return (neuron0x9c7e170()*0.265036);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e390() {
   return (neuron0x9c8c688()*-0.677982);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e3b8() {
   return (neuron0x9c8c888()*-0.750357);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e3e0() {
   return (neuron0x9c8ca88()*0.00799504);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e408() {
   return (neuron0x9c8cc88()*0.128624);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e430() {
   return (neuron0x9c8ce88()*0.532313);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e458() {
   return (neuron0x9c8d088()*0.258166);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e698() {
   return (neuron0x9c7dd70()*0.0220374);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e6c0() {
   return (neuron0x9c7df70()*0.0557664);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e6e8() {
   return (neuron0x9c7e170()*-0.133428);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e710() {
   return (neuron0x9c8c688()*-0.222838);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e738() {
   return (neuron0x9c8c888()*-0.66979);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e760() {
   return (neuron0x9c8ca88()*-0.254382);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e788() {
   return (neuron0x9c8cc88()*0.708427);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e7b0() {
   return (neuron0x9c8ce88()*0.083213);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9e7d8() {
   return (neuron0x9c8d088()*-0.338286);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ea18() {
   return (neuron0x9c7dd70()*0.0535796);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ea40() {
   return (neuron0x9c7df70()*0.398263);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ea68() {
   return (neuron0x9c7e170()*-0.373528);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ea90() {
   return (neuron0x9c8c688()*-0.46666);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9eab8() {
   return (neuron0x9c8c888()*0.105151);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9eae0() {
   return (neuron0x9c8ca88()*-0.587055);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9eb08() {
   return (neuron0x9c8cc88()*-0.48468);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9eb30() {
   return (neuron0x9c8ce88()*-0.184494);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9eb58() {
   return (neuron0x9c8d088()*0.184772);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ed98() {
   return (neuron0x9c7dd70()*0.0767039);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9edc0() {
   return (neuron0x9c7df70()*0.171236);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ede8() {
   return (neuron0x9c7e170()*0.119793);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ee10() {
   return (neuron0x9c8c688()*0.345578);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ee38() {
   return (neuron0x9c8c888()*0.150119);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ee60() {
   return (neuron0x9c8ca88()*-0.273139);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ee88() {
   return (neuron0x9c8cc88()*-0.227048);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9eeb0() {
   return (neuron0x9c8ce88()*-0.105321);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9eed8() {
   return (neuron0x9c8d088()*-0.238453);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f118() {
   return (neuron0x9c7dd70()*0.135654);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f140() {
   return (neuron0x9c7df70()*-0.0220324);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f168() {
   return (neuron0x9c7e170()*-1.50139);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f190() {
   return (neuron0x9c8c688()*0.0467952);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f1b8() {
   return (neuron0x9c8c888()*0.412225);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f1e0() {
   return (neuron0x9c8ca88()*-0.0400641);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f208() {
   return (neuron0x9c8cc88()*2.19003);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f230() {
   return (neuron0x9c8ce88()*-0.0328245);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f258() {
   return (neuron0x9c8d088()*-1.8773);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f498() {
   return (neuron0x9c7dd70()*0.211931);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f4c0() {
   return (neuron0x9c7df70()*-0.242574);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f4e8() {
   return (neuron0x9c7e170()*0.0687145);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f510() {
   return (neuron0x9c8c688()*0.243156);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f538() {
   return (neuron0x9c8c888()*-0.406176);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f560() {
   return (neuron0x9c8ca88()*0.346228);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f588() {
   return (neuron0x9c8cc88()*-0.138013);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f5b0() {
   return (neuron0x9c8ce88()*-0.0821034);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f5d8() {
   return (neuron0x9c8d088()*0.50009);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f818() {
   return (neuron0x9c7dd70()*-0.174526);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f840() {
   return (neuron0x9c7df70()*-0.0117351);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f868() {
   return (neuron0x9c7e170()*-1.26352);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f890() {
   return (neuron0x9c8c688()*-0.43437);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f8b8() {
   return (neuron0x9c8c888()*-0.0122964);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f8e0() {
   return (neuron0x9c8ca88()*0.0890235);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f908() {
   return (neuron0x9c8cc88()*-0.239411);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f930() {
   return (neuron0x9c8ce88()*-0.0841947);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9f958() {
   return (neuron0x9c8d088()*-1.42625);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fb98() {
   return (neuron0x9c7dd70()*-0.0279585);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fbc0() {
   return (neuron0x9c7df70()*0.18535);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fbe8() {
   return (neuron0x9c7e170()*2.11439);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fc10() {
   return (neuron0x9c8c688()*-0.0275592);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fc38() {
   return (neuron0x9c8c888()*0.506154);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fc60() {
   return (neuron0x9c8ca88()*0.031807);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fc88() {
   return (neuron0x9c8cc88()*0.0403266);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fcb0() {
   return (neuron0x9c8ce88()*-0.0121862);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9fcd8() {
   return (neuron0x9c8d088()*0.00454834);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ff18() {
   return (neuron0x9c7dd70()*-0.257406);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ff40() {
   return (neuron0x9c7df70()*0.0292269);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ff68() {
   return (neuron0x9c7e170()*-0.581079);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ff90() {
   return (neuron0x9c8c688()*-0.0649744);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ffb8() {
   return (neuron0x9c8c888()*0.568603);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c9ffe0() {
   return (neuron0x9c8ca88()*-0.0142565);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0008() {
   return (neuron0x9c8cc88()*0.860741);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0030() {
   return (neuron0x9c8ce88()*0.0407644);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0058() {
   return (neuron0x9c8d088()*-0.740527);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0298() {
   return (neuron0x9c7dd70()*-0.12834);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca02c0() {
   return (neuron0x9c7df70()*-0.00593339);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca02e8() {
   return (neuron0x9c7e170()*0.245978);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0310() {
   return (neuron0x9c8c688()*-0.435281);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0338() {
   return (neuron0x9c8c888()*0.733816);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0360() {
   return (neuron0x9c8ca88()*-0.216213);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0388() {
   return (neuron0x9c8cc88()*-0.773869);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca03b0() {
   return (neuron0x9c8ce88()*0.00694313);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca03d8() {
   return (neuron0x9c8d088()*0.114079);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0618() {
   return (neuron0x9c7dd70()*0.254046);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0640() {
   return (neuron0x9c7df70()*-0.301778);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0668() {
   return (neuron0x9c7e170()*-0.254491);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0690() {
   return (neuron0x9c8c688()*0.184163);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca06b8() {
   return (neuron0x9c8c888()*-0.0097678);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca06e0() {
   return (neuron0x9c8ca88()*0.258889);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0708() {
   return (neuron0x9c8cc88()*0.0236556);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0730() {
   return (neuron0x9c8ce88()*0.155793);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0758() {
   return (neuron0x9c8d088()*-0.388322);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0998() {
   return (neuron0x9c7dd70()*-0.520586);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca09c0() {
   return (neuron0x9c7df70()*0.0402238);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca09e8() {
   return (neuron0x9c7e170()*-0.952245);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0a10() {
   return (neuron0x9c8c688()*0.0343431);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0a38() {
   return (neuron0x9c8c888()*0.309346);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0a60() {
   return (neuron0x9c8ca88()*-0.282541);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0a88() {
   return (neuron0x9c8cc88()*0.533721);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0ab0() {
   return (neuron0x9c8ce88()*-0.110783);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0ad8() {
   return (neuron0x9c8d088()*0.0238195);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0d18() {
   return (neuron0x9c7dd70()*0.427224);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0d40() {
   return (neuron0x9c7df70()*0.34238);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0d68() {
   return (neuron0x9c7e170()*0.418484);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0d90() {
   return (neuron0x9c8c688()*0.246724);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0db8() {
   return (neuron0x9c8c888()*-0.00329863);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0de0() {
   return (neuron0x9c8ca88()*-0.101237);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0e08() {
   return (neuron0x9c8cc88()*-0.206194);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0e30() {
   return (neuron0x9c8ce88()*-0.481291);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca0e58() {
   return (neuron0x9c8d088()*0.297727);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1098() {
   return (neuron0x9c7dd70()*0.115864);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca10c0() {
   return (neuron0x9c7df70()*0.0390085);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca10e8() {
   return (neuron0x9c7e170()*-0.88298);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1110() {
   return (neuron0x9c8c688()*0.214113);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1138() {
   return (neuron0x9c8c888()*-0.624884);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1160() {
   return (neuron0x9c8ca88()*-0.0771895);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1188() {
   return (neuron0x9c8cc88()*0.549288);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca11b0() {
   return (neuron0x9c8ce88()*-0.0108603);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca11d8() {
   return (neuron0x9c8d088()*-0.709576);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1418() {
   return (neuron0x9c7dd70()*-0.472702);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1440() {
   return (neuron0x9c7df70()*-0.0681065);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1468() {
   return (neuron0x9c7e170()*-1.22425);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1490() {
   return (neuron0x9c8c688()*0.195975);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca14b8() {
   return (neuron0x9c8c888()*1.26793);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca14e0() {
   return (neuron0x9c8ca88()*0.00679942);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1508() {
   return (neuron0x9c8cc88()*-0.270711);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1530() {
   return (neuron0x9c8ce88()*0.116124);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1558() {
   return (neuron0x9c8d088()*-0.617467);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1798() {
   return (neuron0x9c7dd70()*-0.107794);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca17c0() {
   return (neuron0x9c7df70()*-0.561313);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca17e8() {
   return (neuron0x9c7e170()*-0.691054);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1810() {
   return (neuron0x9c8c688()*-0.453659);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1838() {
   return (neuron0x9c8c888()*-0.304258);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1860() {
   return (neuron0x9c8ca88()*-0.103043);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1888() {
   return (neuron0x9c8cc88()*0.00382349);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca18b0() {
   return (neuron0x9c8ce88()*0.103296);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca18d8() {
   return (neuron0x9c8d088()*0.0366916);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1b18() {
   return (neuron0x9c7dd70()*0.241332);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1b40() {
   return (neuron0x9c7df70()*0.168412);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1b68() {
   return (neuron0x9c7e170()*-2.10114);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1b90() {
   return (neuron0x9c8c688()*0.0342644);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1bb8() {
   return (neuron0x9c8c888()*-0.097223);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1be0() {
   return (neuron0x9c8ca88()*0.183288);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1c08() {
   return (neuron0x9c8cc88()*0.87413);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1c30() {
   return (neuron0x9c8ce88()*-0.124391);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1c58() {
   return (neuron0x9c8d088()*-0.141905);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1e98() {
   return (neuron0x9c7dd70()*0.521242);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1ec0() {
   return (neuron0x9c7df70()*0.362121);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1ee8() {
   return (neuron0x9c7e170()*-0.0809875);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1f10() {
   return (neuron0x9c8c688()*0.107325);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1f38() {
   return (neuron0x9c8c888()*-0.184927);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1f60() {
   return (neuron0x9c8ca88()*-0.00372018);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1f88() {
   return (neuron0x9c8cc88()*-0.0771311);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1fb0() {
   return (neuron0x9c8ce88()*-0.304379);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca1fd8() {
   return (neuron0x9c8d088()*0.0493274);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2218() {
   return (neuron0x9c7dd70()*0.112094);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2240() {
   return (neuron0x9c7df70()*-0.0353727);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2268() {
   return (neuron0x9c7e170()*0.864935);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2290() {
   return (neuron0x9c8c688()*-0.0418487);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca22b8() {
   return (neuron0x9c8c888()*-0.154947);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca22e0() {
   return (neuron0x9c8ca88()*0.570029);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2308() {
   return (neuron0x9c8cc88()*-1.14183);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2330() {
   return (neuron0x9c8ce88()*-0.23743);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2358() {
   return (neuron0x9c8d088()*0.63444);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2598() {
   return (neuron0x9c7dd70()*0.200694);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca25c0() {
   return (neuron0x9c7df70()*0.0948084);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca25e8() {
   return (neuron0x9c7e170()*-0.0134081);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2610() {
   return (neuron0x9c8c688()*0.164028);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2638() {
   return (neuron0x9c8c888()*-0.337742);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2660() {
   return (neuron0x9c8ca88()*0.583135);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2688() {
   return (neuron0x9c8cc88()*-0.469399);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca26b0() {
   return (neuron0x9c8ce88()*0.525465);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca26d8() {
   return (neuron0x9c8d088()*0.431821);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2918() {
   return (neuron0x9c7dd70()*-0.0223143);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2940() {
   return (neuron0x9c7df70()*0.216972);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2968() {
   return (neuron0x9c7e170()*-0.0593993);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2990() {
   return (neuron0x9c8c688()*0.269329);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca29b8() {
   return (neuron0x9c8c888()*-0.181301);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca29e0() {
   return (neuron0x9c8ca88()*0.479253);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2a08() {
   return (neuron0x9c8cc88()*-0.0127619);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2a30() {
   return (neuron0x9c8ce88()*-0.243082);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2a58() {
   return (neuron0x9c8d088()*-0.444538);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2c98() {
   return (neuron0x9c7dd70()*-0.480528);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2cc0() {
   return (neuron0x9c7df70()*0.387831);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2ce8() {
   return (neuron0x9c7e170()*0.331564);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2d10() {
   return (neuron0x9c8c688()*0.342016);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2d38() {
   return (neuron0x9c8c888()*0.104037);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2d60() {
   return (neuron0x9c8ca88()*-0.475249);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2d88() {
   return (neuron0x9c8cc88()*0.548435);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2db0() {
   return (neuron0x9c8ce88()*0.0295686);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca2dd8() {
   return (neuron0x9c8d088()*0.056003);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3018() {
   return (neuron0x9c7dd70()*-0.724365);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3040() {
   return (neuron0x9c7df70()*-0.131671);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3068() {
   return (neuron0x9c7e170()*0.735342);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3090() {
   return (neuron0x9c8c688()*-0.179805);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca30b8() {
   return (neuron0x9c8c888()*-0.37849);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca30e0() {
   return (neuron0x9c8ca88()*0.10132);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3108() {
   return (neuron0x9c8cc88()*0.1202);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3130() {
   return (neuron0x9c8ce88()*-0.184304);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3158() {
   return (neuron0x9c8d088()*0.00422728);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3398() {
   return (neuron0x9c7dd70()*-0.370799);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca33c0() {
   return (neuron0x9c7df70()*0.241756);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca33e8() {
   return (neuron0x9c7e170()*-0.184838);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3410() {
   return (neuron0x9c8c688()*-0.0259586);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3438() {
   return (neuron0x9c8c888()*-0.595316);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3460() {
   return (neuron0x9c8ca88()*0.0993513);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3488() {
   return (neuron0x9c8cc88()*1.0139);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca34b0() {
   return (neuron0x9c8ce88()*-0.182943);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca34d8() {
   return (neuron0x9c8d088()*0.692555);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3718() {
   return (neuron0x9c7dd70()*0.297827);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3740() {
   return (neuron0x9c7df70()*0.192735);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3768() {
   return (neuron0x9c7e170()*0.121113);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3790() {
   return (neuron0x9c8c688()*0.566562);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca37b8() {
   return (neuron0x9c8c888()*-0.229132);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca37e0() {
   return (neuron0x9c8ca88()*0.254667);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3808() {
   return (neuron0x9c8cc88()*0.220109);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3830() {
   return (neuron0x9c8ce88()*-0.0646209);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3858() {
   return (neuron0x9c8d088()*0.188405);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3a98() {
   return (neuron0x9c7dd70()*0.0273623);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3ac0() {
   return (neuron0x9c7df70()*0.506676);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3ae8() {
   return (neuron0x9c7e170()*-0.190689);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3b10() {
   return (neuron0x9c8c688()*-0.085023);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3b38() {
   return (neuron0x9c8c888()*-0.38335);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3b60() {
   return (neuron0x9c8ca88()*0.336665);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3b88() {
   return (neuron0x9c8cc88()*-0.15132);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3bb0() {
   return (neuron0x9c8ce88()*-0.28534);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3bd8() {
   return (neuron0x9c8d088()*0.146709);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3e18() {
   return (neuron0x9c7dd70()*-0.163844);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3e40() {
   return (neuron0x9c7df70()*-0.265216);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3e68() {
   return (neuron0x9c7e170()*0.181406);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3e90() {
   return (neuron0x9c8c688()*-0.213592);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3eb8() {
   return (neuron0x9c8c888()*-0.536007);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3ee0() {
   return (neuron0x9c8ca88()*0.0836895);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3f08() {
   return (neuron0x9c8cc88()*0.682711);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3f30() {
   return (neuron0x9c8ce88()*0.125301);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca3f58() {
   return (neuron0x9c8d088()*0.0107477);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4198() {
   return (neuron0x9c7dd70()*-0.246218);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca41c0() {
   return (neuron0x9c7df70()*0.508109);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca41e8() {
   return (neuron0x9c7e170()*-0.406294);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4210() {
   return (neuron0x9c8c688()*0.198186);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4238() {
   return (neuron0x9c8c888()*0.0990344);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4260() {
   return (neuron0x9c8ca88()*-0.575871);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4288() {
   return (neuron0x9c8cc88()*0.161489);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca42b0() {
   return (neuron0x9c8ce88()*-0.12443);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca42d8() {
   return (neuron0x9c8d088()*0.305623);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4518() {
   return (neuron0x9c7dd70()*0.480865);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4540() {
   return (neuron0x9c7df70()*0.0771585);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4568() {
   return (neuron0x9c7e170()*0.820074);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4590() {
   return (neuron0x9c8c688()*0.448723);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca45b8() {
   return (neuron0x9c8c888()*0.161058);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca45e0() {
   return (neuron0x9c8ca88()*-0.235525);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4608() {
   return (neuron0x9c8cc88()*1.13842);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4630() {
   return (neuron0x9c8ce88()*-0.245215);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4658() {
   return (neuron0x9c8d088()*-0.047065);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4898() {
   return (neuron0x9c7dd70()*0.0217559);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca48c0() {
   return (neuron0x9c7df70()*-0.433956);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca48e8() {
   return (neuron0x9c7e170()*0.428769);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4910() {
   return (neuron0x9c8c688()*0.470923);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4938() {
   return (neuron0x9c8c888()*0.382333);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4960() {
   return (neuron0x9c8ca88()*-0.104483);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4988() {
   return (neuron0x9c8cc88()*-0.0216587);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca49b0() {
   return (neuron0x9c8ce88()*0.0699344);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca49d8() {
   return (neuron0x9c8d088()*0.0770937);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4c18() {
   return (neuron0x9c7dd70()*0.433301);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4c40() {
   return (neuron0x9c7df70()*-0.174784);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4c68() {
   return (neuron0x9c7e170()*0.0642305);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4c90() {
   return (neuron0x9c8c688()*0.26421);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4cb8() {
   return (neuron0x9c8c888()*0.265446);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4ce0() {
   return (neuron0x9c8ca88()*0.375586);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4d08() {
   return (neuron0x9c8cc88()*-0.374471);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4d30() {
   return (neuron0x9c8ce88()*-0.123633);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4d58() {
   return (neuron0x9c8d088()*0.282089);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4f98() {
   return (neuron0x9c7dd70()*-0.583206);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4fc0() {
   return (neuron0x9c7df70()*0.31825);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca4fe8() {
   return (neuron0x9c7e170()*-0.322869);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5010() {
   return (neuron0x9c8c688()*0.286743);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5038() {
   return (neuron0x9c8c888()*-0.48959);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5060() {
   return (neuron0x9c8ca88()*0.296903);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5088() {
   return (neuron0x9c8cc88()*0.318881);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca50b0() {
   return (neuron0x9c8ce88()*-0.368339);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca50d8() {
   return (neuron0x9c8d088()*-0.133742);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5228() {
   return (neuron0x9c8d3e0()*0.33563);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5250() {
   return (neuron0x9c8d6d8()*-0.284405);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5278() {
   return (neuron0x9c8dac8()*-0.193078);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca52a0() {
   return (neuron0x9c8dde8()*0.836198);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca52c8() {
   return (neuron0x9c8e1e0()*-0.691775);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca52f0() {
   return (neuron0x9c8e548()*-0.177204);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5318() {
   return (neuron0x9c8e8b0()*0.835495);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5340() {
   return (neuron0x9c8ec18()*-0.289221);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5368() {
   return (neuron0x9c8f0f8()*1.18919);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5390() {
   return (neuron0x9c8f3a8()*0.224814);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca53b8() {
   return (neuron0x9c8f710()*-0.427667);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca53e0() {
   return (neuron0x9c8fa78()*-0.658852);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5408() {
   return (neuron0x9c8fde0()*0.234945);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5430() {
   return (neuron0x9c90148()*2.02517);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5458() {
   return (neuron0x9c904b0()*0.219697);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5480() {
   return (neuron0x9c90b30()*1.01372);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5530() {
   return (neuron0x9c91260()*-0.353801);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5558() {
   return (neuron0x9c914f0()*-0.248387);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5580() {
   return (neuron0x9c91780()*0.0171573);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca55a8() {
   return (neuron0x9c91a10()*0.228383);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca55d0() {
   return (neuron0x9c91d90()*-2.46632);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca55f8() {
   return (neuron0x9c92110()*-1.65937);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5620() {
   return (neuron0x9c92490()*-0.296872);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5648() {
   return (neuron0x9c92810()*-0.763191);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5670() {
   return (neuron0x9c92c20()*-0.133569);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5698() {
   return (neuron0x9c92fa0()*-0.297768);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca56c0() {
   return (neuron0x9c93320()*-0.173633);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca56e8() {
   return (neuron0x9c936a0()*0.781938);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5710() {
   return (neuron0x9c93a20()*0.0883597);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5738() {
   return (neuron0x9c907c8()*0.320596);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5760() {
   return (neuron0x9c94530()*-1.41805);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5788() {
   return (neuron0x9c948b0()*-0.283367);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca54a8() {
   return (neuron0x9c95438()*0.929916);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca54d0() {
   return (neuron0x9c95560()*0.292561);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca54f8() {
   return (neuron0x9c95858()*0.389666);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca58b8() {
   return (neuron0x9c95bc0()*-0.483615);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca58e0() {
   return (neuron0x9c95f28()*-0.336396);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5908() {
   return (neuron0x9c96290()*0.312657);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5930() {
   return (neuron0x9c965f8()*-0.172419);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5958() {
   return (neuron0x9c96978()*-1.15393);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5980() {
   return (neuron0x9c96cf8()*0.366662);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca59a8() {
   return (neuron0x9c97078()*-0.436026);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca59d0() {
   return (neuron0x9c973f8()*-0.310842);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca59f8() {
   return (neuron0x9c97778()*-0.394074);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5a20() {
   return (neuron0x9c97af8()*1.22118);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5a48() {
   return (neuron0x9c97e78()*0.661049);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5a70() {
   return (neuron0x9c981f8()*-0.0473684);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5a98() {
   return (neuron0x9c98578()*0.169969);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5ac0() {
   return (neuron0x9c988f8()*0.0873654);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5ae8() {
   return (neuron0x9c98c78()*0.207191);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5b10() {
   return (neuron0x9c98ff8()*0.0694948);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5b38() {
   return (neuron0x9c99378()*0.278789);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5b60() {
   return (neuron0x9c996f8()*0.554333);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5b88() {
   return (neuron0x9c99a78()*-0.746786);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5bb0() {
   return (neuron0x9c99df8()*0.168586);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5bd8() {
   return (neuron0x9c9a178()*0.594155);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5c00() {
   return (neuron0x9c9a610()*-0.0597678);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5c28() {
   return (neuron0x9c93d00()*-0.00163852);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5c50() {
   return (neuron0x9c94080()*0.0884727);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5c78() {
   return (neuron0x9c9b980()*-0.915451);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5ca0() {
   return (neuron0x9c9bc10()*0.334231);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5cc8() {
   return (neuron0x9c9bf78()*0.351275);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5cf0() {
   return (neuron0x9c9c2f8()*-0.358784);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5d18() {
   return (neuron0x9c9c678()*-0.446004);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9c8d2f8() {
   return (neuron0x9c95250()*-0.293377);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca57b0() {
   return (neuron0x9c9d698()*0.528097);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca57d8() {
   return (neuron0x9c9da00()*-0.341545);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5800() {
   return (neuron0x9c9dd80()*1.0054);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5828() {
   return (neuron0x9c9e100()*-0.400416);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5850() {
   return (neuron0x9c9e480()*-0.639635);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5878() {
   return (neuron0x9c9e800()*-0.284459);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5f48() {
   return (neuron0x9c9eb80()*-0.0618166);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5f70() {
   return (neuron0x9c9ef00()*1.21309);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5f98() {
   return (neuron0x9c9f280()*0.34426);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5fc0() {
   return (neuron0x9c9f600()*0.918032);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca5fe8() {
   return (neuron0x9c9f980()*-1.90506);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6010() {
   return (neuron0x9c9fd00()*-1.31066);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6038() {
   return (neuron0x9ca0080()*0.719848);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6060() {
   return (neuron0x9ca0400()*0.0676578);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6088() {
   return (neuron0x9ca0780()*0.867007);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca60b0() {
   return (neuron0x9ca0b00()*-0.0964711);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca60d8() {
   return (neuron0x9ca0e80()*-0.814585);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6100() {
   return (neuron0x9ca1200()*0.560686);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6128() {
   return (neuron0x9ca1580()*0.488849);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6150() {
   return (neuron0x9ca1900()*0.843573);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6178() {
   return (neuron0x9ca1c80()*0.199621);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca61a0() {
   return (neuron0x9ca2000()*-0.341697);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca61c8() {
   return (neuron0x9ca2380()*0.273114);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca61f0() {
   return (neuron0x9ca2700()*-0.0562302);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6218() {
   return (neuron0x9ca2a80()*-0.281987);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6240() {
   return (neuron0x9ca2e00()*-1.16157);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6268() {
   return (neuron0x9ca3180()*0.150648);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6290() {
   return (neuron0x9ca3500()*0.114869);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca62b8() {
   return (neuron0x9ca3880()*0.456792);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca62e0() {
   return (neuron0x9ca3c00()*0.291634);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6308() {
   return (neuron0x9ca3f80()*-0.464567);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6330() {
   return (neuron0x9ca4300()*0.494032);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6358() {
   return (neuron0x9ca4680()*-0.152225);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca6380() {
   return (neuron0x9ca4a00()*0.362228);
}

double NNPerpGammaXYCorrectionClusterDeltaY::synapse0x9ca63a8() {
   return (neuron0x9ca4d80()*0.249623);
}

