#include "NNParaGammaXYCorrectionClusterDeltaY.h"
#include <cmath>

double NNParaGammaXYCorrectionClusterDeltaY::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8) {
   input0 = (in0 - 2122.17)/956.309;
   input1 = (in1 - -4.21955)/33.6873;
   input2 = (in2 - 0.0276887)/60.1795;
   input3 = (in3 - 1.35737)/0.353415;
   input4 = (in4 - 1.46875)/0.364824;
   input5 = (in5 - 0.201504)/1.73176;
   input6 = (in6 - 0.253821)/1.35266;
   input7 = (in7 - 6.58314)/12.4187;
   input8 = (in8 - 4.54498)/7.86055;
   switch(index) {
     case 0:
         return neuron0x98d1600();
     default:
         return 0.;
   }
}

double NNParaGammaXYCorrectionClusterDeltaY::Value(int index, double* input) {
   input0 = (input[0] - 2122.17)/956.309;
   input1 = (input[1] - -4.21955)/33.6873;
   input2 = (input[2] - 0.0276887)/60.1795;
   input3 = (input[3] - 1.35737)/0.353415;
   input4 = (input[4] - 1.46875)/0.364824;
   input5 = (input[5] - 0.201504)/1.73176;
   input6 = (input[6] - 0.253821)/1.35266;
   input7 = (input[7] - 6.58314)/12.4187;
   input8 = (input[8] - 4.54498)/7.86055;
   switch(index) {
     case 0:
         return neuron0x98d1600();
     default:
         return 0.;
   }
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b7538() {
   return input0;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b76f0() {
   return input1;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b78f0() {
   return input2;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b7af0() {
   return input3;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b7cf0() {
   return input4;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b7ef0() {
   return input5;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b8108() {
   return input6;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b8320() {
   return input7;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b8538() {
   return input8;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98b8890() {
   double input = -0.414289;
   input += synapse0x8e76480();
   input += synapse0x98b8a48();
   input += synapse0x98b8a70();
   input += synapse0x98b8a98();
   input += synapse0x98b8ac0();
   input += synapse0x98b8ae8();
   input += synapse0x98b8b10();
   input += synapse0x98b8b38();
   input += synapse0x98b8b60();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b8890() {
   double input = input0x98b8890();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98b8b88() {
   double input = -0.177501;
   input += synapse0x98b8d88();
   input += synapse0x98b8db0();
   input += synapse0x98b8dd8();
   input += synapse0x98b8e00();
   input += synapse0x98b8e28();
   input += synapse0x98b8e50();
   input += synapse0x98b8e78();
   input += synapse0x98b8ea0();
   input += synapse0x98b8f50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b8b88() {
   double input = input0x98b8b88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98b8f78() {
   double input = -0.143028;
   input += synapse0x98b9130();
   input += synapse0x98b9158();
   input += synapse0x98b9180();
   input += synapse0x98b91a8();
   input += synapse0x98b91d0();
   input += synapse0x98b91f8();
   input += synapse0x98b9220();
   input += synapse0x98b9248();
   input += synapse0x98b9270();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b8f78() {
   double input = input0x98b8f78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98b9298() {
   double input = 0.650915;
   input += synapse0x98b9498();
   input += synapse0x98b94c0();
   input += synapse0x98b94e8();
   input += synapse0x98b9510();
   input += synapse0x98b9538();
   input += synapse0x98b9560();
   input += synapse0x98b8ec8();
   input += synapse0x98b8ef0();
   input += synapse0x98b8f18();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b9298() {
   double input = input0x98b9298();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98b9690() {
   double input = 0.93787;
   input += synapse0x98b9890();
   input += synapse0x98b98b8();
   input += synapse0x98b98e0();
   input += synapse0x98b9908();
   input += synapse0x98b9930();
   input += synapse0x98b9958();
   input += synapse0x98b9980();
   input += synapse0x98b99a8();
   input += synapse0x98b99d0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b9690() {
   double input = input0x98b9690();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98b99f8() {
   double input = 0.0737831;
   input += synapse0x98b9bf8();
   input += synapse0x98b9c20();
   input += synapse0x98b9c48();
   input += synapse0x98b9c70();
   input += synapse0x98b9c98();
   input += synapse0x98b9cc0();
   input += synapse0x98b9ce8();
   input += synapse0x98b9d10();
   input += synapse0x98b9d38();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b99f8() {
   double input = input0x98b99f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98b9d60() {
   double input = -0.269066;
   input += synapse0x98b9f60();
   input += synapse0x98b9f88();
   input += synapse0x98b9fb0();
   input += synapse0x98b9fd8();
   input += synapse0x98ba000();
   input += synapse0x98ba028();
   input += synapse0x98ba050();
   input += synapse0x98ba078();
   input += synapse0x98ba0a0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98b9d60() {
   double input = input0x98b9d60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ba0c8() {
   double input = 0.519598;
   input += synapse0x98ba350();
   input += synapse0x98ba378();
   input += synapse0x8e76448();
   input += synapse0x8e76220();
   input += synapse0x8e75d98();
   input += synapse0x8e762f0();
   input += synapse0x8e76318();
   input += synapse0x986bbb0();
   input += synapse0x986bbd8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ba0c8() {
   double input = input0x98ba0c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ba5a8() {
   double input = -0.583902;
   input += synapse0x98ba7c0();
   input += synapse0x98ba7e8();
   input += synapse0x98ba810();
   input += synapse0x98ba838();
   input += synapse0x98ba860();
   input += synapse0x98ba888();
   input += synapse0x98ba8b0();
   input += synapse0x98ba8d8();
   input += synapse0x98ba900();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ba5a8() {
   double input = input0x98ba5a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ba928() {
   double input = -1.22102;
   input += synapse0x98bab40();
   input += synapse0x98bab68();
   input += synapse0x98bab90();
   input += synapse0x98babb8();
   input += synapse0x98babe0();
   input += synapse0x98bac08();
   input += synapse0x98bac30();
   input += synapse0x98bac58();
   input += synapse0x98bac80();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ba928() {
   double input = input0x98ba928();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98baca8() {
   double input = -0.000696639;
   input += synapse0x98baec0();
   input += synapse0x98baee8();
   input += synapse0x98baf10();
   input += synapse0x98baf38();
   input += synapse0x98baf60();
   input += synapse0x98baf88();
   input += synapse0x98bafb0();
   input += synapse0x98bafd8();
   input += synapse0x98bb000();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98baca8() {
   double input = input0x98baca8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bb028() {
   double input = -0.156017;
   input += synapse0x98bb240();
   input += synapse0x98bb268();
   input += synapse0x98bb290();
   input += synapse0x98bb2b8();
   input += synapse0x98bb2e0();
   input += synapse0x98bb308();
   input += synapse0x98bb330();
   input += synapse0x98bb358();
   input += synapse0x98bb380();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bb028() {
   double input = input0x98bb028();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bb3a8() {
   double input = -0.27817;
   input += synapse0x98bb5c0();
   input += synapse0x98bb5e8();
   input += synapse0x98bb610();
   input += synapse0x98bb638();
   input += synapse0x98bb660();
   input += synapse0x98bb688();
   input += synapse0x98bb6b0();
   input += synapse0x98bb6d8();
   input += synapse0x98bb700();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bb3a8() {
   double input = input0x98bb3a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bb728() {
   double input = -0.167053;
   input += synapse0x98bb940();
   input += synapse0x98bb968();
   input += synapse0x98bb990();
   input += synapse0x98bb9b8();
   input += synapse0x98bb9e0();
   input += synapse0x98bba08();
   input += synapse0x98bba30();
   input += synapse0x98bba58();
   input += synapse0x98bba80();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bb728() {
   double input = input0x98bb728();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bbaa8() {
   double input = -0.947142;
   input += synapse0x98bbcc0();
   input += synapse0x98bbce8();
   input += synapse0x98bbd10();
   input += synapse0x98b9588();
   input += synapse0x98b95b0();
   input += synapse0x98b95d8();
   input += synapse0x98b9600();
   input += synapse0x98b9628();
   input += synapse0x98b9650();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bbaa8() {
   double input = input0x98bbaa8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ba3a0() {
   double input = 0.135588;
   input += synapse0x98bc140();
   input += synapse0x98bc1f0();
   input += synapse0x98bc2a0();
   input += synapse0x98bc350();
   input += synapse0x98bc400();
   input += synapse0x98bc4b0();
   input += synapse0x98bc560();
   input += synapse0x98bc610();
   input += synapse0x98bc6c0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ba3a0() {
   double input = input0x98ba3a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bc770() {
   double input = 0.669875;
   input += synapse0x98bc8b0();
   input += synapse0x98bc8d8();
   input += synapse0x98bc900();
   input += synapse0x98bc928();
   input += synapse0x98bc950();
   input += synapse0x98bc978();
   input += synapse0x98bc9a0();
   input += synapse0x98bc9c8();
   input += synapse0x98bc9f0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bc770() {
   double input = input0x98bc770();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bca18() {
   double input = 1.18851;
   input += synapse0x98bcb58();
   input += synapse0x98bcb80();
   input += synapse0x98bcba8();
   input += synapse0x98bcbd0();
   input += synapse0x98bcbf8();
   input += synapse0x98bcc20();
   input += synapse0x98bcc48();
   input += synapse0x98bcc70();
   input += synapse0x98bcc98();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bca18() {
   double input = input0x98bca18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bccc0() {
   double input = -0.0922386;
   input += synapse0x98bce00();
   input += synapse0x98bce28();
   input += synapse0x98bce50();
   input += synapse0x98bce78();
   input += synapse0x98bcea0();
   input += synapse0x98bcec8();
   input += synapse0x98bcef0();
   input += synapse0x98bcf18();
   input += synapse0x98bcf40();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bccc0() {
   double input = input0x98bccc0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bcf68() {
   double input = -0.424872;
   input += synapse0x98bd180();
   input += synapse0x98bd1a8();
   input += synapse0x98bd1d0();
   input += synapse0x98bd1f8();
   input += synapse0x98bd220();
   input += synapse0x98bd248();
   input += synapse0x98bd270();
   input += synapse0x98bd298();
   input += synapse0x98bd2c0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bcf68() {
   double input = input0x98bcf68();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98be3a8() {
   double input = -0.683152;
   input += synapse0x98be5a8();
   input += synapse0x98be5d0();
   input += synapse0x98be5f8();
   input += synapse0x98be620();
   input += synapse0x98be648();
   input += synapse0x98be670();
   input += synapse0x98be698();
   input += synapse0x98be6c0();
   input += synapse0x98be6e8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98be3a8() {
   double input = input0x98be3a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98be710() {
   double input = -0.249328;
   input += synapse0x98be910();
   input += synapse0x98be938();
   input += synapse0x98be960();
   input += synapse0x98be988();
   input += synapse0x98be9b0();
   input += synapse0x98be9d8();
   input += synapse0x98bea00();
   input += synapse0x98bea28();
   input += synapse0x98bea50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98be710() {
   double input = input0x98be710();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bea78() {
   double input = -0.336298;
   input += synapse0x98bec78();
   input += synapse0x98beca0();
   input += synapse0x98becc8();
   input += synapse0x98becf0();
   input += synapse0x98bed18();
   input += synapse0x98bed40();
   input += synapse0x98bed68();
   input += synapse0x98bed90();
   input += synapse0x98bedb8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bea78() {
   double input = input0x98bea78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bede0() {
   double input = 0.504413;
   input += synapse0x98ba2c8();
   input += synapse0x98ba2f0();
   input += synapse0x98ba318();
   input += synapse0x98bf0e8();
   input += synapse0x98bf110();
   input += synapse0x98bf138();
   input += synapse0x98bf160();
   input += synapse0x98bf188();
   input += synapse0x98bf1b0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bede0() {
   double input = input0x98bede0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bf1d8() {
   double input = 0.137256;
   input += synapse0x98bf3d8();
   input += synapse0x98bf400();
   input += synapse0x98bf428();
   input += synapse0x98bf450();
   input += synapse0x98bf478();
   input += synapse0x98bf4a0();
   input += synapse0x98bf4c8();
   input += synapse0x98bf4f0();
   input += synapse0x98bf518();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bf1d8() {
   double input = input0x98bf1d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bf540() {
   double input = -0.577945;
   input += synapse0x98bf740();
   input += synapse0x98bf768();
   input += synapse0x98bf790();
   input += synapse0x98bf7b8();
   input += synapse0x98bf7e0();
   input += synapse0x98bf808();
   input += synapse0x98bf830();
   input += synapse0x98bf858();
   input += synapse0x98bf880();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bf540() {
   double input = input0x98bf540();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bf8a8() {
   double input = 0.888474;
   input += synapse0x98bfaa8();
   input += synapse0x98bfad0();
   input += synapse0x98bfaf8();
   input += synapse0x98bfb20();
   input += synapse0x98bfb48();
   input += synapse0x98bfb70();
   input += synapse0x98bfb98();
   input += synapse0x98bfbc0();
   input += synapse0x98bfbe8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bf8a8() {
   double input = input0x98bf8a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bfc10() {
   double input = 0.797434;
   input += synapse0x98bfe10();
   input += synapse0x98bfe38();
   input += synapse0x98bfe60();
   input += synapse0x98bfe88();
   input += synapse0x98bfeb0();
   input += synapse0x98bfed8();
   input += synapse0x98bff00();
   input += synapse0x98bff28();
   input += synapse0x98bff50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bfc10() {
   double input = input0x98bfc10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bff78() {
   double input = -0.0477039;
   input += synapse0x98c0178();
   input += synapse0x98c01a0();
   input += synapse0x98c01c8();
   input += synapse0x98c01f0();
   input += synapse0x98c0218();
   input += synapse0x98bbd38();
   input += synapse0x98bbd60();
   input += synapse0x98bbd88();
   input += synapse0x98bbdb0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bff78() {
   double input = input0x98bff78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98bbdd8() {
   double input = 0.551415;
   input += synapse0x98bbff0();
   input += synapse0x98bc018();
   input += synapse0x98bc040();
   input += synapse0x98bc068();
   input += synapse0x98bc090();
   input += synapse0x98bc0b8();
   input += synapse0x98bc0e0();
   input += synapse0x98bc108();
   input += synapse0x98c0a48();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98bbdd8() {
   double input = input0x98bbdd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c0a70() {
   double input = -0.382425;
   input += synapse0x98c0c70();
   input += synapse0x98c0c98();
   input += synapse0x98c0cc0();
   input += synapse0x98c0ce8();
   input += synapse0x98c0d10();
   input += synapse0x98c0d38();
   input += synapse0x98c0d60();
   input += synapse0x98c0d88();
   input += synapse0x98c0db0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c0a70() {
   double input = input0x98c0a70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c0dd8() {
   double input = -0.0393054;
   input += synapse0x98c0fd8();
   input += synapse0x98bc168();
   input += synapse0x98bc190();
   input += synapse0x98bc1b8();
   input += synapse0x98bc218();
   input += synapse0x98bc240();
   input += synapse0x98bc268();
   input += synapse0x98bc2c8();
   input += synapse0x98bc2f0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c0dd8() {
   double input = input0x98c0dd8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c1948() {
   double input = -0.5804;
   input += synapse0x98bc520();
   input += synapse0x98bc318();
   input += synapse0x98bc3c0();
   input += synapse0x98bc470();
   input += synapse0x98bc588();
   input += synapse0x98bc5b0();
   input += synapse0x98bc5d8();
   input += synapse0x98bc638();
   input += synapse0x98bc660();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c1948() {
   double input = input0x98c1948();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c1a70() {
   double input = -0.230022;
   input += synapse0x98bc688();
   input += synapse0x98bc730();
   input += synapse0x98c1c28();
   input += synapse0x98c1c50();
   input += synapse0x98c1c78();
   input += synapse0x98c1ca0();
   input += synapse0x98c1cc8();
   input += synapse0x98c1cf0();
   input += synapse0x98c1d18();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c1a70() {
   double input = input0x98c1a70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c1d40() {
   double input = 0.399116;
   input += synapse0x98c1f40();
   input += synapse0x98c1f68();
   input += synapse0x98c1f90();
   input += synapse0x98c1fb8();
   input += synapse0x98c1fe0();
   input += synapse0x98c2008();
   input += synapse0x98c2030();
   input += synapse0x98c2058();
   input += synapse0x98c2080();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c1d40() {
   double input = input0x98c1d40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c20a8() {
   double input = -0.374622;
   input += synapse0x98c22a8();
   input += synapse0x98c22d0();
   input += synapse0x98c22f8();
   input += synapse0x98c2320();
   input += synapse0x98c2348();
   input += synapse0x98c2370();
   input += synapse0x98c2398();
   input += synapse0x98c23c0();
   input += synapse0x98c23e8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c20a8() {
   double input = input0x98c20a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c2410() {
   double input = 0.440963;
   input += synapse0x98c2610();
   input += synapse0x98c2638();
   input += synapse0x98c2660();
   input += synapse0x98c2688();
   input += synapse0x98c26b0();
   input += synapse0x98c26d8();
   input += synapse0x98c2700();
   input += synapse0x98c2728();
   input += synapse0x98c2750();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c2410() {
   double input = input0x98c2410();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c2778() {
   double input = -0.555774;
   input += synapse0x98c2990();
   input += synapse0x98c29b8();
   input += synapse0x98c29e0();
   input += synapse0x98c2a08();
   input += synapse0x98c2a30();
   input += synapse0x98c2a58();
   input += synapse0x98c2a80();
   input += synapse0x98c2aa8();
   input += synapse0x98c2ad0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c2778() {
   double input = input0x98c2778();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c2af8() {
   double input = -0.250993;
   input += synapse0x98c2d10();
   input += synapse0x98c2d38();
   input += synapse0x98c2d60();
   input += synapse0x98c2d88();
   input += synapse0x98c2db0();
   input += synapse0x98c2dd8();
   input += synapse0x98c2e00();
   input += synapse0x98c2e28();
   input += synapse0x98c2e50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c2af8() {
   double input = input0x98c2af8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c2e78() {
   double input = -0.521209;
   input += synapse0x98c3090();
   input += synapse0x98c30b8();
   input += synapse0x98c30e0();
   input += synapse0x98c3108();
   input += synapse0x98c3130();
   input += synapse0x98c3158();
   input += synapse0x98c3180();
   input += synapse0x98c31a8();
   input += synapse0x98c31d0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c2e78() {
   double input = input0x98c2e78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c31f8() {
   double input = 0.0717566;
   input += synapse0x98c3410();
   input += synapse0x98c3438();
   input += synapse0x98c3460();
   input += synapse0x98c3488();
   input += synapse0x98c34b0();
   input += synapse0x98c34d8();
   input += synapse0x98c3500();
   input += synapse0x98c3528();
   input += synapse0x98c3550();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c31f8() {
   double input = input0x98c31f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c3578() {
   double input = 0.117975;
   input += synapse0x98c3790();
   input += synapse0x98c37b8();
   input += synapse0x98c37e0();
   input += synapse0x98c3808();
   input += synapse0x98c3830();
   input += synapse0x98c3858();
   input += synapse0x98c3880();
   input += synapse0x98c38a8();
   input += synapse0x98c38d0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c3578() {
   double input = input0x98c3578();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c38f8() {
   double input = 0.293664;
   input += synapse0x98c3b10();
   input += synapse0x98c3b38();
   input += synapse0x98c3b60();
   input += synapse0x98c3b88();
   input += synapse0x98c3bb0();
   input += synapse0x98c3bd8();
   input += synapse0x98c3c00();
   input += synapse0x98c3c28();
   input += synapse0x98c3c50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c38f8() {
   double input = input0x98c38f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c3c78() {
   double input = 0.00418056;
   input += synapse0x98c3e90();
   input += synapse0x98c3eb8();
   input += synapse0x98c3ee0();
   input += synapse0x98c3f08();
   input += synapse0x98c3f30();
   input += synapse0x98c3f58();
   input += synapse0x98c3f80();
   input += synapse0x98c3fa8();
   input += synapse0x98c3fd0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c3c78() {
   double input = input0x98c3c78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c3ff8() {
   double input = 0.259192;
   input += synapse0x98c4210();
   input += synapse0x98c4238();
   input += synapse0x98c4260();
   input += synapse0x98c4288();
   input += synapse0x98c42b0();
   input += synapse0x98c42d8();
   input += synapse0x98c4300();
   input += synapse0x98c4328();
   input += synapse0x98c4350();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c3ff8() {
   double input = input0x98c3ff8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c4378() {
   double input = -0.554791;
   input += synapse0x98c4590();
   input += synapse0x98c45b8();
   input += synapse0x98c45e0();
   input += synapse0x98c4608();
   input += synapse0x98c4630();
   input += synapse0x98c4658();
   input += synapse0x98c4680();
   input += synapse0x98c46a8();
   input += synapse0x98c46d0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c4378() {
   double input = input0x98c4378();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c46f8() {
   double input = -0.660876;
   input += synapse0x98c4910();
   input += synapse0x98c4938();
   input += synapse0x98c4960();
   input += synapse0x98c4988();
   input += synapse0x98c49b0();
   input += synapse0x98c49d8();
   input += synapse0x98c4a00();
   input += synapse0x98c4a28();
   input += synapse0x98c4a50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c46f8() {
   double input = input0x98c46f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c4a78() {
   double input = 0.563147;
   input += synapse0x98c4c90();
   input += synapse0x98c4cb8();
   input += synapse0x98c4ce0();
   input += synapse0x98c4d08();
   input += synapse0x98c4d30();
   input += synapse0x98c4d58();
   input += synapse0x98c4d80();
   input += synapse0x98c4da8();
   input += synapse0x98c4dd0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c4a78() {
   double input = input0x98c4a78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c4df8() {
   double input = -0.272644;
   input += synapse0x98c5010();
   input += synapse0x98c5038();
   input += synapse0x98c5060();
   input += synapse0x98c5088();
   input += synapse0x98c50b0();
   input += synapse0x98c50d8();
   input += synapse0x98c5100();
   input += synapse0x98c5128();
   input += synapse0x98c5150();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c4df8() {
   double input = input0x98c4df8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c5178() {
   double input = -0.460286;
   input += synapse0x98c5390();
   input += synapse0x98c53b8();
   input += synapse0x98c53e0();
   input += synapse0x98c5408();
   input += synapse0x98c5430();
   input += synapse0x98c5458();
   input += synapse0x98c5480();
   input += synapse0x98c54a8();
   input += synapse0x98c54d0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c5178() {
   double input = input0x98c5178();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c54f8() {
   double input = -0.332559;
   input += synapse0x98c5710();
   input += synapse0x98c5738();
   input += synapse0x98c5760();
   input += synapse0x98c5788();
   input += synapse0x98c57b0();
   input += synapse0x98c57d8();
   input += synapse0x98c5800();
   input += synapse0x98c5828();
   input += synapse0x98c5850();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c54f8() {
   double input = input0x98c54f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c5878() {
   double input = 0.113704;
   input += synapse0x98c5a90();
   input += synapse0x98c5ab8();
   input += synapse0x98c5ae0();
   input += synapse0x98c5b08();
   input += synapse0x98c5b30();
   input += synapse0x98c5b58();
   input += synapse0x98c5b80();
   input += synapse0x98c5ba8();
   input += synapse0x98c5bd0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c5878() {
   double input = input0x98c5878();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c5bf8() {
   double input = -1.4599;
   input += synapse0x98c5e10();
   input += synapse0x98c5e38();
   input += synapse0x98c5e60();
   input += synapse0x98c5e88();
   input += synapse0x98c5eb0();
   input += synapse0x98c5ed8();
   input += synapse0x98c5f00();
   input += synapse0x98c5f28();
   input += synapse0x98c5f50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c5bf8() {
   double input = input0x98c5bf8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c5f78() {
   double input = -0.769884;
   input += synapse0x98c6190();
   input += synapse0x98c61b8();
   input += synapse0x98c61e0();
   input += synapse0x98c6208();
   input += synapse0x98c6230();
   input += synapse0x98c6258();
   input += synapse0x98c6280();
   input += synapse0x98c62a8();
   input += synapse0x98c62d0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c5f78() {
   double input = input0x98c5f78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c62f8() {
   double input = -0.117948;
   input += synapse0x98c6510();
   input += synapse0x98c6538();
   input += synapse0x98c6560();
   input += synapse0x98c6588();
   input += synapse0x98c65b0();
   input += synapse0x98c65d8();
   input += synapse0x98c6600();
   input += synapse0x98c6628();
   input += synapse0x98c6650();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c62f8() {
   double input = input0x98c62f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c6678() {
   double input = 0.285046;
   input += synapse0x98befe0();
   input += synapse0x98bf008();
   input += synapse0x98bf030();
   input += synapse0x98bf058();
   input += synapse0x98bf080();
   input += synapse0x98bf0a8();
   input += synapse0x98c6a98();
   input += synapse0x98c6ac0();
   input += synapse0x98c6ae8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c6678() {
   double input = input0x98c6678();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c6b10() {
   double input = -0.00711976;
   input += synapse0x98c6d10();
   input += synapse0x98c6d38();
   input += synapse0x98c6d60();
   input += synapse0x98c6d88();
   input += synapse0x98c6db0();
   input += synapse0x98c6dd8();
   input += synapse0x98c6e00();
   input += synapse0x98c6e28();
   input += synapse0x98c6e50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c6b10() {
   double input = input0x98c6b10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c0240() {
   double input = 0.316302;
   input += synapse0x98c0458();
   input += synapse0x98c0480();
   input += synapse0x98c04a8();
   input += synapse0x98c04d0();
   input += synapse0x98c04f8();
   input += synapse0x98c0520();
   input += synapse0x98c0548();
   input += synapse0x98c0570();
   input += synapse0x98c0598();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c0240() {
   double input = input0x98c0240();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c05c0() {
   double input = -0.251057;
   input += synapse0x98c07d8();
   input += synapse0x98c0800();
   input += synapse0x98c0828();
   input += synapse0x98c0850();
   input += synapse0x98c0878();
   input += synapse0x98c08a0();
   input += synapse0x98c08c8();
   input += synapse0x98c08f0();
   input += synapse0x98c0918();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c05c0() {
   double input = input0x98c05c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c7e80() {
   double input = -0.0527181;
   input += synapse0x98c7fa8();
   input += synapse0x98c7fd0();
   input += synapse0x98c7ff8();
   input += synapse0x98c8020();
   input += synapse0x98c8048();
   input += synapse0x98c8070();
   input += synapse0x98c8098();
   input += synapse0x98c80c0();
   input += synapse0x98c80e8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c7e80() {
   double input = input0x98c7e80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c8110() {
   double input = 0.281703;
   input += synapse0x98c8310();
   input += synapse0x98c8338();
   input += synapse0x98c8360();
   input += synapse0x98c8388();
   input += synapse0x98c83b0();
   input += synapse0x98c83d8();
   input += synapse0x98c8400();
   input += synapse0x98c8428();
   input += synapse0x98c8450();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c8110() {
   double input = input0x98c8110();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c8478() {
   double input = -0.138244;
   input += synapse0x98c8690();
   input += synapse0x98c86b8();
   input += synapse0x98c86e0();
   input += synapse0x98c8708();
   input += synapse0x98c8730();
   input += synapse0x98c8758();
   input += synapse0x98c8780();
   input += synapse0x98c87a8();
   input += synapse0x98c87d0();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c8478() {
   double input = input0x98c8478();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c87f8() {
   double input = -0.0758045;
   input += synapse0x98c8a10();
   input += synapse0x98c8a38();
   input += synapse0x98c8a60();
   input += synapse0x98c8a88();
   input += synapse0x98c8ab0();
   input += synapse0x98c8ad8();
   input += synapse0x98c8b00();
   input += synapse0x98c8b28();
   input += synapse0x98c8b50();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c87f8() {
   double input = input0x98c87f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c8b78() {
   double input = 0.0238339;
   input += synapse0x98c8d90();
   input += synapse0x98c1000();
   input += synapse0x98c1028();
   input += synapse0x98c1050();
   input += synapse0x98c1280();
   input += synapse0x98c12a8();
   input += synapse0x98c14d8();
   input += synapse0x98c1500();
   input += synapse0x98c1738();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c8b78() {
   double input = input0x98c8b78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c1760() {
   double input = -0.737296;
   input += synapse0x98c9a30();
   input += synapse0x98c9a58();
   input += synapse0x98c9a80();
   input += synapse0x98c9aa8();
   input += synapse0x98c9ad0();
   input += synapse0x98c9af8();
   input += synapse0x98c9b20();
   input += synapse0x98c9b48();
   input += synapse0x98c9b70();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c1760() {
   double input = input0x98c1760();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c9b98() {
   double input = 0.480733;
   input += synapse0x98c9d98();
   input += synapse0x98c9dc0();
   input += synapse0x98c9de8();
   input += synapse0x98c9e10();
   input += synapse0x98c9e38();
   input += synapse0x98c9e60();
   input += synapse0x98c9e88();
   input += synapse0x98c9eb0();
   input += synapse0x98c9ed8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c9b98() {
   double input = input0x98c9b98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98c9f00() {
   double input = 0.387775;
   input += synapse0x98ca118();
   input += synapse0x98ca140();
   input += synapse0x98ca168();
   input += synapse0x98ca190();
   input += synapse0x98ca1b8();
   input += synapse0x98ca1e0();
   input += synapse0x98ca208();
   input += synapse0x98ca230();
   input += synapse0x98ca258();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98c9f00() {
   double input = input0x98c9f00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ca280() {
   double input = -0.000490626;
   input += synapse0x98ca498();
   input += synapse0x98ca4c0();
   input += synapse0x98ca4e8();
   input += synapse0x98ca510();
   input += synapse0x98ca538();
   input += synapse0x98ca560();
   input += synapse0x98ca588();
   input += synapse0x98ca5b0();
   input += synapse0x98ca5d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ca280() {
   double input = input0x98ca280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ca600() {
   double input = 0.122311;
   input += synapse0x98ca818();
   input += synapse0x98ca840();
   input += synapse0x98ca868();
   input += synapse0x98ca890();
   input += synapse0x98ca8b8();
   input += synapse0x98ca8e0();
   input += synapse0x98ca908();
   input += synapse0x98ca930();
   input += synapse0x98ca958();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ca600() {
   double input = input0x98ca600();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ca980() {
   double input = -0.624489;
   input += synapse0x98cab98();
   input += synapse0x98cabc0();
   input += synapse0x98cabe8();
   input += synapse0x98cac10();
   input += synapse0x98cac38();
   input += synapse0x98cac60();
   input += synapse0x98cac88();
   input += synapse0x98cacb0();
   input += synapse0x98cacd8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ca980() {
   double input = input0x98ca980();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cad00() {
   double input = 0.6263;
   input += synapse0x98caf18();
   input += synapse0x98caf40();
   input += synapse0x98caf68();
   input += synapse0x98caf90();
   input += synapse0x98cafb8();
   input += synapse0x98cafe0();
   input += synapse0x98cb008();
   input += synapse0x98cb030();
   input += synapse0x98cb058();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cad00() {
   double input = input0x98cad00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cb080() {
   double input = 0.126184;
   input += synapse0x98cb298();
   input += synapse0x98cb2c0();
   input += synapse0x98cb2e8();
   input += synapse0x98cb310();
   input += synapse0x98cb338();
   input += synapse0x98cb360();
   input += synapse0x98cb388();
   input += synapse0x98cb3b0();
   input += synapse0x98cb3d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cb080() {
   double input = input0x98cb080();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cb400() {
   double input = -0.844379;
   input += synapse0x98cb618();
   input += synapse0x98cb640();
   input += synapse0x98cb668();
   input += synapse0x98cb690();
   input += synapse0x98cb6b8();
   input += synapse0x98cb6e0();
   input += synapse0x98cb708();
   input += synapse0x98cb730();
   input += synapse0x98cb758();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cb400() {
   double input = input0x98cb400();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cb780() {
   double input = 0.193219;
   input += synapse0x98cb998();
   input += synapse0x98cb9c0();
   input += synapse0x98cb9e8();
   input += synapse0x98cba10();
   input += synapse0x98cba38();
   input += synapse0x98cba60();
   input += synapse0x98cba88();
   input += synapse0x98cbab0();
   input += synapse0x98cbad8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cb780() {
   double input = input0x98cb780();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cbb00() {
   double input = 0.969039;
   input += synapse0x98cbd18();
   input += synapse0x98cbd40();
   input += synapse0x98cbd68();
   input += synapse0x98cbd90();
   input += synapse0x98cbdb8();
   input += synapse0x98cbde0();
   input += synapse0x98cbe08();
   input += synapse0x98cbe30();
   input += synapse0x98cbe58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cbb00() {
   double input = input0x98cbb00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cbe80() {
   double input = -0.0834131;
   input += synapse0x98cc098();
   input += synapse0x98cc0c0();
   input += synapse0x98cc0e8();
   input += synapse0x98cc110();
   input += synapse0x98cc138();
   input += synapse0x98cc160();
   input += synapse0x98cc188();
   input += synapse0x98cc1b0();
   input += synapse0x98cc1d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cbe80() {
   double input = input0x98cbe80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cc200() {
   double input = -1.97776;
   input += synapse0x98cc418();
   input += synapse0x98cc440();
   input += synapse0x98cc468();
   input += synapse0x98cc490();
   input += synapse0x98cc4b8();
   input += synapse0x98cc4e0();
   input += synapse0x98cc508();
   input += synapse0x98cc530();
   input += synapse0x98cc558();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cc200() {
   double input = input0x98cc200();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cc580() {
   double input = 0.762112;
   input += synapse0x98cc798();
   input += synapse0x98cc7c0();
   input += synapse0x98cc7e8();
   input += synapse0x98cc810();
   input += synapse0x98cc838();
   input += synapse0x98cc860();
   input += synapse0x98cc888();
   input += synapse0x98cc8b0();
   input += synapse0x98cc8d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cc580() {
   double input = input0x98cc580();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cc900() {
   double input = -0.0775525;
   input += synapse0x98ccb18();
   input += synapse0x98ccb40();
   input += synapse0x98ccb68();
   input += synapse0x98ccb90();
   input += synapse0x98ccbb8();
   input += synapse0x98ccbe0();
   input += synapse0x98ccc08();
   input += synapse0x98ccc30();
   input += synapse0x98ccc58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cc900() {
   double input = input0x98cc900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ccc80() {
   double input = -0.358697;
   input += synapse0x98cce98();
   input += synapse0x98ccec0();
   input += synapse0x98ccee8();
   input += synapse0x98ccf10();
   input += synapse0x98ccf38();
   input += synapse0x98ccf60();
   input += synapse0x98ccf88();
   input += synapse0x98ccfb0();
   input += synapse0x98ccfd8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ccc80() {
   double input = input0x98ccc80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cd000() {
   double input = 0.12129;
   input += synapse0x98cd218();
   input += synapse0x98cd240();
   input += synapse0x98cd268();
   input += synapse0x98cd290();
   input += synapse0x98cd2b8();
   input += synapse0x98cd2e0();
   input += synapse0x98cd308();
   input += synapse0x98cd330();
   input += synapse0x98cd358();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cd000() {
   double input = input0x98cd000();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cd380() {
   double input = -0.621311;
   input += synapse0x98cd598();
   input += synapse0x98cd5c0();
   input += synapse0x98cd5e8();
   input += synapse0x98cd610();
   input += synapse0x98cd638();
   input += synapse0x98cd660();
   input += synapse0x98cd688();
   input += synapse0x98cd6b0();
   input += synapse0x98cd6d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cd380() {
   double input = input0x98cd380();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cd700() {
   double input = -0.21926;
   input += synapse0x98cd918();
   input += synapse0x98cd940();
   input += synapse0x98cd968();
   input += synapse0x98cd990();
   input += synapse0x98cd9b8();
   input += synapse0x98cd9e0();
   input += synapse0x98cda08();
   input += synapse0x98cda30();
   input += synapse0x98cda58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cd700() {
   double input = input0x98cd700();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cda80() {
   double input = -0.126376;
   input += synapse0x98cdc98();
   input += synapse0x98cdcc0();
   input += synapse0x98cdce8();
   input += synapse0x98cdd10();
   input += synapse0x98cdd38();
   input += synapse0x98cdd60();
   input += synapse0x98cdd88();
   input += synapse0x98cddb0();
   input += synapse0x98cddd8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cda80() {
   double input = input0x98cda80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cde00() {
   double input = 1.45286;
   input += synapse0x98ce018();
   input += synapse0x98ce040();
   input += synapse0x98ce068();
   input += synapse0x98ce090();
   input += synapse0x98ce0b8();
   input += synapse0x98ce0e0();
   input += synapse0x98ce108();
   input += synapse0x98ce130();
   input += synapse0x98ce158();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cde00() {
   double input = input0x98cde00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ce180() {
   double input = -0.373792;
   input += synapse0x98ce398();
   input += synapse0x98ce3c0();
   input += synapse0x98ce3e8();
   input += synapse0x98ce410();
   input += synapse0x98ce438();
   input += synapse0x98ce460();
   input += synapse0x98ce488();
   input += synapse0x98ce4b0();
   input += synapse0x98ce4d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ce180() {
   double input = input0x98ce180();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ce500() {
   double input = 2.69904;
   input += synapse0x98ce718();
   input += synapse0x98ce740();
   input += synapse0x98ce768();
   input += synapse0x98ce790();
   input += synapse0x98ce7b8();
   input += synapse0x98ce7e0();
   input += synapse0x98ce808();
   input += synapse0x98ce830();
   input += synapse0x98ce858();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ce500() {
   double input = input0x98ce500();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98ce880() {
   double input = 0.435409;
   input += synapse0x98cea98();
   input += synapse0x98ceac0();
   input += synapse0x98ceae8();
   input += synapse0x98ceb10();
   input += synapse0x98ceb38();
   input += synapse0x98ceb60();
   input += synapse0x98ceb88();
   input += synapse0x98cebb0();
   input += synapse0x98cebd8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98ce880() {
   double input = input0x98ce880();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cec00() {
   double input = -0.113494;
   input += synapse0x98cee18();
   input += synapse0x98cee40();
   input += synapse0x98cee68();
   input += synapse0x98cee90();
   input += synapse0x98ceeb8();
   input += synapse0x98ceee0();
   input += synapse0x98cef08();
   input += synapse0x98cef30();
   input += synapse0x98cef58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cec00() {
   double input = input0x98cec00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cef80() {
   double input = -0.210713;
   input += synapse0x98cf198();
   input += synapse0x98cf1c0();
   input += synapse0x98cf1e8();
   input += synapse0x98cf210();
   input += synapse0x98cf238();
   input += synapse0x98cf260();
   input += synapse0x98cf288();
   input += synapse0x98cf2b0();
   input += synapse0x98cf2d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cef80() {
   double input = input0x98cef80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cf300() {
   double input = 0.138533;
   input += synapse0x98cf518();
   input += synapse0x98cf540();
   input += synapse0x98cf568();
   input += synapse0x98cf590();
   input += synapse0x98cf5b8();
   input += synapse0x98cf5e0();
   input += synapse0x98cf608();
   input += synapse0x98cf630();
   input += synapse0x98cf658();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cf300() {
   double input = input0x98cf300();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cf680() {
   double input = 0.39125;
   input += synapse0x98cf898();
   input += synapse0x98cf8c0();
   input += synapse0x98cf8e8();
   input += synapse0x98cf910();
   input += synapse0x98cf938();
   input += synapse0x98cf960();
   input += synapse0x98cf988();
   input += synapse0x98cf9b0();
   input += synapse0x98cf9d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cf680() {
   double input = input0x98cf680();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cfa00() {
   double input = -2.14833;
   input += synapse0x98cfc18();
   input += synapse0x98cfc40();
   input += synapse0x98cfc68();
   input += synapse0x98cfc90();
   input += synapse0x98cfcb8();
   input += synapse0x98cfce0();
   input += synapse0x98cfd08();
   input += synapse0x98cfd30();
   input += synapse0x98cfd58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cfa00() {
   double input = input0x98cfa00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98cfd80() {
   double input = -0.22345;
   input += synapse0x98cff98();
   input += synapse0x98cffc0();
   input += synapse0x98cffe8();
   input += synapse0x98d0010();
   input += synapse0x98d0038();
   input += synapse0x98d0060();
   input += synapse0x98d0088();
   input += synapse0x98d00b0();
   input += synapse0x98d00d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98cfd80() {
   double input = input0x98cfd80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98d0100() {
   double input = -0.0166643;
   input += synapse0x98d0318();
   input += synapse0x98d0340();
   input += synapse0x98d0368();
   input += synapse0x98d0390();
   input += synapse0x98d03b8();
   input += synapse0x98d03e0();
   input += synapse0x98d0408();
   input += synapse0x98d0430();
   input += synapse0x98d0458();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98d0100() {
   double input = input0x98d0100();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98d0480() {
   double input = 0.606319;
   input += synapse0x98d0698();
   input += synapse0x98d06c0();
   input += synapse0x98d06e8();
   input += synapse0x98d0710();
   input += synapse0x98d0738();
   input += synapse0x98d0760();
   input += synapse0x98d0788();
   input += synapse0x98d07b0();
   input += synapse0x98d07d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98d0480() {
   double input = input0x98d0480();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98d0800() {
   double input = -0.610746;
   input += synapse0x98d0a18();
   input += synapse0x98d0a40();
   input += synapse0x98d0a68();
   input += synapse0x98d0a90();
   input += synapse0x98d0ab8();
   input += synapse0x98d0ae0();
   input += synapse0x98d0b08();
   input += synapse0x98d0b30();
   input += synapse0x98d0b58();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98d0800() {
   double input = input0x98d0800();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98d0b80() {
   double input = 0.315714;
   input += synapse0x98d0d98();
   input += synapse0x98d0dc0();
   input += synapse0x98d0de8();
   input += synapse0x98d0e10();
   input += synapse0x98d0e38();
   input += synapse0x98d0e60();
   input += synapse0x98d0e88();
   input += synapse0x98d0eb0();
   input += synapse0x98d0ed8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98d0b80() {
   double input = input0x98d0b80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98d0f00() {
   double input = 1.56168;
   input += synapse0x98d1118();
   input += synapse0x98d1140();
   input += synapse0x98d1168();
   input += synapse0x98d1190();
   input += synapse0x98d11b8();
   input += synapse0x98d11e0();
   input += synapse0x98d1208();
   input += synapse0x98d1230();
   input += synapse0x98d1258();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98d0f00() {
   double input = input0x98d0f00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98d1280() {
   double input = -0.193566;
   input += synapse0x98d1498();
   input += synapse0x98d14c0();
   input += synapse0x98d14e8();
   input += synapse0x98d1510();
   input += synapse0x98d1538();
   input += synapse0x98d1560();
   input += synapse0x98d1588();
   input += synapse0x98d15b0();
   input += synapse0x98d15d8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98d1280() {
   double input = input0x98d1280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::input0x98d1600() {
   double input = 0.0862109;
   input += synapse0x98d1728();
   input += synapse0x98d1750();
   input += synapse0x98d1778();
   input += synapse0x98d17a0();
   input += synapse0x98d17c8();
   input += synapse0x98d17f0();
   input += synapse0x98d1818();
   input += synapse0x98d1840();
   input += synapse0x98d1868();
   input += synapse0x98d1890();
   input += synapse0x98d18b8();
   input += synapse0x98d18e0();
   input += synapse0x98d1908();
   input += synapse0x98d1930();
   input += synapse0x98d1958();
   input += synapse0x98d1980();
   input += synapse0x98d1a30();
   input += synapse0x98d1a58();
   input += synapse0x98d1a80();
   input += synapse0x98d1aa8();
   input += synapse0x98d1ad0();
   input += synapse0x98d1af8();
   input += synapse0x98d1b20();
   input += synapse0x98d1b48();
   input += synapse0x98d1b70();
   input += synapse0x98d1b98();
   input += synapse0x98d1bc0();
   input += synapse0x98d1be8();
   input += synapse0x98d1c10();
   input += synapse0x98d1c38();
   input += synapse0x98d1c60();
   input += synapse0x98d1c88();
   input += synapse0x98d19a8();
   input += synapse0x98d19d0();
   input += synapse0x98d19f8();
   input += synapse0x98d1db8();
   input += synapse0x98d1de0();
   input += synapse0x98d1e08();
   input += synapse0x98d1e30();
   input += synapse0x98d1e58();
   input += synapse0x98d1e80();
   input += synapse0x98d1ea8();
   input += synapse0x98d1ed0();
   input += synapse0x98d1ef8();
   input += synapse0x98d1f20();
   input += synapse0x98d1f48();
   input += synapse0x98d1f70();
   input += synapse0x98d1f98();
   input += synapse0x98d1fc0();
   input += synapse0x98d1fe8();
   input += synapse0x98d2010();
   input += synapse0x98d2038();
   input += synapse0x98d2060();
   input += synapse0x98d2088();
   input += synapse0x98d20b0();
   input += synapse0x98d20d8();
   input += synapse0x98d2100();
   input += synapse0x98d2128();
   input += synapse0x98d2150();
   input += synapse0x98d2178();
   input += synapse0x98d21a0();
   input += synapse0x98d21c8();
   input += synapse0x98d21f0();
   input += synapse0x98d2218();
   input += synapse0x98b87a8();
   input += synapse0x98d1cb0();
   input += synapse0x98d1cd8();
   input += synapse0x98d1d00();
   input += synapse0x98d1d28();
   input += synapse0x98d1d50();
   input += synapse0x98d1d78();
   input += synapse0x98d2448();
   input += synapse0x98d2470();
   input += synapse0x98d2498();
   input += synapse0x98d24c0();
   input += synapse0x98d24e8();
   input += synapse0x98d2510();
   input += synapse0x98d2538();
   input += synapse0x98d2560();
   input += synapse0x98d2588();
   input += synapse0x98d25b0();
   input += synapse0x98d25d8();
   input += synapse0x98d2600();
   input += synapse0x98d2628();
   input += synapse0x98d2650();
   input += synapse0x98d2678();
   input += synapse0x98d26a0();
   input += synapse0x98d26c8();
   input += synapse0x98d26f0();
   input += synapse0x98d2718();
   input += synapse0x98d2740();
   input += synapse0x98d2768();
   input += synapse0x98d2790();
   input += synapse0x98d27b8();
   input += synapse0x98d27e0();
   input += synapse0x98d2808();
   input += synapse0x98d2830();
   input += synapse0x98d2858();
   input += synapse0x98d2880();
   input += synapse0x98d28a8();
   return input;
}

double NNParaGammaXYCorrectionClusterDeltaY::neuron0x98d1600() {
   double input = input0x98d1600();
   return (input * 1)+0;
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x8e76480() {
   return (neuron0x98b7538()*0.0800631);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8a48() {
   return (neuron0x98b76f0()*0.0639326);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8a70() {
   return (neuron0x98b78f0()*-0.0864538);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8a98() {
   return (neuron0x98b7af0()*-0.184813);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8ac0() {
   return (neuron0x98b7cf0()*0.42909);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8ae8() {
   return (neuron0x98b7ef0()*0.214197);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8b10() {
   return (neuron0x98b8108()*0.646221);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8b38() {
   return (neuron0x98b8320()*-0.288898);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8b60() {
   return (neuron0x98b8538()*0.137278);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8d88() {
   return (neuron0x98b7538()*-0.317305);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8db0() {
   return (neuron0x98b76f0()*-0.262879);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8dd8() {
   return (neuron0x98b78f0()*0.0186338);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8e00() {
   return (neuron0x98b7af0()*0.278373);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8e28() {
   return (neuron0x98b7cf0()*-0.0507559);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8e50() {
   return (neuron0x98b7ef0()*0.358162);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8e78() {
   return (neuron0x98b8108()*0.297555);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8ea0() {
   return (neuron0x98b8320()*-0.165912);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8f50() {
   return (neuron0x98b8538()*-0.503174);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9130() {
   return (neuron0x98b7538()*-0.0246115);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9158() {
   return (neuron0x98b76f0()*0.125366);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9180() {
   return (neuron0x98b78f0()*-0.157201);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b91a8() {
   return (neuron0x98b7af0()*0.5249);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b91d0() {
   return (neuron0x98b7cf0()*-0.413077);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b91f8() {
   return (neuron0x98b7ef0()*-0.608254);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9220() {
   return (neuron0x98b8108()*-0.250768);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9248() {
   return (neuron0x98b8320()*-0.0416887);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9270() {
   return (neuron0x98b8538()*0.0759576);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9498() {
   return (neuron0x98b7538()*0.0866666);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b94c0() {
   return (neuron0x98b76f0()*0.251827);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b94e8() {
   return (neuron0x98b78f0()*-1.01021);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9510() {
   return (neuron0x98b7af0()*0.244768);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9538() {
   return (neuron0x98b7cf0()*0.587042);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9560() {
   return (neuron0x98b7ef0()*-0.0487662);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8ec8() {
   return (neuron0x98b8108()*1.27329);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8ef0() {
   return (neuron0x98b8320()*0.123856);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b8f18() {
   return (neuron0x98b8538()*1.01063);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9890() {
   return (neuron0x98b7538()*-0.113445);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b98b8() {
   return (neuron0x98b76f0()*-0.0824863);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b98e0() {
   return (neuron0x98b78f0()*1.29774);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9908() {
   return (neuron0x98b7af0()*-0.0282698);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9930() {
   return (neuron0x98b7cf0()*-1.01834);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9958() {
   return (neuron0x98b7ef0()*0.110266);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9980() {
   return (neuron0x98b8108()*-2.11269);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b99a8() {
   return (neuron0x98b8320()*-0.145373);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b99d0() {
   return (neuron0x98b8538()*1.03675);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9bf8() {
   return (neuron0x98b7538()*-0.119053);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9c20() {
   return (neuron0x98b76f0()*0.287677);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9c48() {
   return (neuron0x98b78f0()*-0.0756316);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9c70() {
   return (neuron0x98b7af0()*-0.167693);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9c98() {
   return (neuron0x98b7cf0()*0.00217505);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9cc0() {
   return (neuron0x98b7ef0()*0.0184993);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9ce8() {
   return (neuron0x98b8108()*0.111184);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9d10() {
   return (neuron0x98b8320()*0.113806);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9d38() {
   return (neuron0x98b8538()*0.652789);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9f60() {
   return (neuron0x98b7538()*-0.314939);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9f88() {
   return (neuron0x98b76f0()*-0.0972269);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9fb0() {
   return (neuron0x98b78f0()*0.0274408);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9fd8() {
   return (neuron0x98b7af0()*-0.454637);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba000() {
   return (neuron0x98b7cf0()*-0.245917);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba028() {
   return (neuron0x98b7ef0()*0.0898196);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba050() {
   return (neuron0x98b8108()*-0.638014);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba078() {
   return (neuron0x98b8320()*0.346302);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba0a0() {
   return (neuron0x98b8538()*-0.0659693);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba350() {
   return (neuron0x98b7538()*-0.00882684);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba378() {
   return (neuron0x98b76f0()*0.0285776);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x8e76448() {
   return (neuron0x98b78f0()*0.101221);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x8e76220() {
   return (neuron0x98b7af0()*0.472986);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x8e75d98() {
   return (neuron0x98b7cf0()*-0.69493);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x8e762f0() {
   return (neuron0x98b7ef0()*0.256334);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x8e76318() {
   return (neuron0x98b8108()*-0.0676941);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x986bbb0() {
   return (neuron0x98b8320()*-0.0616022);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x986bbd8() {
   return (neuron0x98b8538()*0.971427);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba7c0() {
   return (neuron0x98b7538()*0.0600256);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba7e8() {
   return (neuron0x98b76f0()*0.0326369);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba810() {
   return (neuron0x98b78f0()*0.373092);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba838() {
   return (neuron0x98b7af0()*-0.239256);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba860() {
   return (neuron0x98b7cf0()*-0.31611);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba888() {
   return (neuron0x98b7ef0()*-0.0209893);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba8b0() {
   return (neuron0x98b8108()*0.357607);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba8d8() {
   return (neuron0x98b8320()*0.080358);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba900() {
   return (neuron0x98b8538()*0.356472);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bab40() {
   return (neuron0x98b7538()*0.561926);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bab68() {
   return (neuron0x98b76f0()*-0.114218);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bab90() {
   return (neuron0x98b78f0()*1.33376);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98babb8() {
   return (neuron0x98b7af0()*0.135862);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98babe0() {
   return (neuron0x98b7cf0()*0.0305893);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bac08() {
   return (neuron0x98b7ef0()*-0.0563927);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bac30() {
   return (neuron0x98b8108()*-0.916156);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bac58() {
   return (neuron0x98b8320()*-0.0584684);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bac80() {
   return (neuron0x98b8538()*1.09602);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98baec0() {
   return (neuron0x98b7538()*-0.455304);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98baee8() {
   return (neuron0x98b76f0()*-0.553108);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98baf10() {
   return (neuron0x98b78f0()*0.107618);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98baf38() {
   return (neuron0x98b7af0()*-0.288574);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98baf60() {
   return (neuron0x98b7cf0()*-0.168914);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98baf88() {
   return (neuron0x98b7ef0()*-0.197078);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bafb0() {
   return (neuron0x98b8108()*-0.266059);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bafd8() {
   return (neuron0x98b8320()*0.0611205);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb000() {
   return (neuron0x98b8538()*0.0497074);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb240() {
   return (neuron0x98b7538()*0.122537);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb268() {
   return (neuron0x98b76f0()*0.179012);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb290() {
   return (neuron0x98b78f0()*-0.0303484);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb2b8() {
   return (neuron0x98b7af0()*0.12106);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb2e0() {
   return (neuron0x98b7cf0()*-0.363077);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb308() {
   return (neuron0x98b7ef0()*0.212985);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb330() {
   return (neuron0x98b8108()*-0.67766);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb358() {
   return (neuron0x98b8320()*0.451562);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb380() {
   return (neuron0x98b8538()*0.124036);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb5c0() {
   return (neuron0x98b7538()*0.238449);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb5e8() {
   return (neuron0x98b76f0()*-0.0442394);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb610() {
   return (neuron0x98b78f0()*-1.25207);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb638() {
   return (neuron0x98b7af0()*-0.267122);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb660() {
   return (neuron0x98b7cf0()*0.828653);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb688() {
   return (neuron0x98b7ef0()*-0.0962426);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb6b0() {
   return (neuron0x98b8108()*0.12674);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb6d8() {
   return (neuron0x98b8320()*-0.250341);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb700() {
   return (neuron0x98b8538()*-1.37576);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb940() {
   return (neuron0x98b7538()*0.420535);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb968() {
   return (neuron0x98b76f0()*-0.0723894);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb990() {
   return (neuron0x98b78f0()*0.0734954);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb9b8() {
   return (neuron0x98b7af0()*0.368049);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bb9e0() {
   return (neuron0x98b7cf0()*-0.0790342);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bba08() {
   return (neuron0x98b7ef0()*0.135398);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bba30() {
   return (neuron0x98b8108()*0.125034);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bba58() {
   return (neuron0x98b8320()*-0.156082);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bba80() {
   return (neuron0x98b8538()*-0.408768);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bbcc0() {
   return (neuron0x98b7538()*0.412194);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bbce8() {
   return (neuron0x98b76f0()*0.216858);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bbd10() {
   return (neuron0x98b78f0()*0.72703);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9588() {
   return (neuron0x98b7af0()*0.0964003);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b95b0() {
   return (neuron0x98b7cf0()*0.241748);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b95d8() {
   return (neuron0x98b7ef0()*0.0849218);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9600() {
   return (neuron0x98b8108()*-0.283762);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9628() {
   return (neuron0x98b8320()*-0.128627);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b9650() {
   return (neuron0x98b8538()*0.46411);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc140() {
   return (neuron0x98b7538()*0.185959);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc1f0() {
   return (neuron0x98b76f0()*-0.298138);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc2a0() {
   return (neuron0x98b78f0()*-0.252266);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc350() {
   return (neuron0x98b7af0()*0.271195);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc400() {
   return (neuron0x98b7cf0()*0.0694251);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc4b0() {
   return (neuron0x98b7ef0()*0.231306);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc560() {
   return (neuron0x98b8108()*0.32005);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc610() {
   return (neuron0x98b8320()*0.307981);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc6c0() {
   return (neuron0x98b8538()*0.122212);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc8b0() {
   return (neuron0x98b7538()*0.415491);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc8d8() {
   return (neuron0x98b76f0()*0.201935);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc900() {
   return (neuron0x98b78f0()*-0.791581);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc928() {
   return (neuron0x98b7af0()*0.196532);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc950() {
   return (neuron0x98b7cf0()*-1.00296);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc978() {
   return (neuron0x98b7ef0()*0.110417);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc9a0() {
   return (neuron0x98b8108()*1.05317);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc9c8() {
   return (neuron0x98b8320()*0.0970607);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc9f0() {
   return (neuron0x98b8538()*1.21399);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcb58() {
   return (neuron0x98b7538()*0.0694462);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcb80() {
   return (neuron0x98b76f0()*0.0123225);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcba8() {
   return (neuron0x98b78f0()*-1.70877);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcbd0() {
   return (neuron0x98b7af0()*-0.000446352);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcbf8() {
   return (neuron0x98b7cf0()*-0.327647);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcc20() {
   return (neuron0x98b7ef0()*0.0326991);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcc48() {
   return (neuron0x98b8108()*0.0548253);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcc70() {
   return (neuron0x98b8320()*-0.023585);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcc98() {
   return (neuron0x98b8538()*-1.46722);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bce00() {
   return (neuron0x98b7538()*-0.113952);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bce28() {
   return (neuron0x98b76f0()*-0.501219);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bce50() {
   return (neuron0x98b78f0()*0.226755);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bce78() {
   return (neuron0x98b7af0()*0.000165082);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcea0() {
   return (neuron0x98b7cf0()*-0.0764089);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcec8() {
   return (neuron0x98b7ef0()*-0.287217);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcef0() {
   return (neuron0x98b8108()*0.130652);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcf18() {
   return (neuron0x98b8320()*-0.290704);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bcf40() {
   return (neuron0x98b8538()*0.281876);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd180() {
   return (neuron0x98b7538()*0.638008);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd1a8() {
   return (neuron0x98b76f0()*-0.0900577);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd1d0() {
   return (neuron0x98b78f0()*-0.380133);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd1f8() {
   return (neuron0x98b7af0()*0.304593);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd220() {
   return (neuron0x98b7cf0()*0.312233);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd248() {
   return (neuron0x98b7ef0()*0.0121891);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd270() {
   return (neuron0x98b8108()*0.363364);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd298() {
   return (neuron0x98b8320()*-0.301152);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bd2c0() {
   return (neuron0x98b8538()*-0.494303);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be5a8() {
   return (neuron0x98b7538()*0.216299);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be5d0() {
   return (neuron0x98b76f0()*0.0416383);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be5f8() {
   return (neuron0x98b78f0()*-0.431731);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be620() {
   return (neuron0x98b7af0()*-0.103825);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be648() {
   return (neuron0x98b7cf0()*-1.09022);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be670() {
   return (neuron0x98b7ef0()*-0.00679453);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be698() {
   return (neuron0x98b8108()*1.48443);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be6c0() {
   return (neuron0x98b8320()*-0.1222);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be6e8() {
   return (neuron0x98b8538()*0.0529038);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be910() {
   return (neuron0x98b7538()*0.0504492);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be938() {
   return (neuron0x98b76f0()*0.406395);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be960() {
   return (neuron0x98b78f0()*-0.324057);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be988() {
   return (neuron0x98b7af0()*0.0662486);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be9b0() {
   return (neuron0x98b7cf0()*0.252835);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98be9d8() {
   return (neuron0x98b7ef0()*-0.0270627);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bea00() {
   return (neuron0x98b8108()*0.726718);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bea28() {
   return (neuron0x98b8320()*0.248775);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bea50() {
   return (neuron0x98b8538()*0.0759686);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bec78() {
   return (neuron0x98b7538()*0.224079);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98beca0() {
   return (neuron0x98b76f0()*-0.126958);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98becc8() {
   return (neuron0x98b78f0()*0.280186);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98becf0() {
   return (neuron0x98b7af0()*-0.0023675);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bed18() {
   return (neuron0x98b7cf0()*0.00639226);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bed40() {
   return (neuron0x98b7ef0()*0.48091);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bed68() {
   return (neuron0x98b8108()*-0.284851);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bed90() {
   return (neuron0x98b8320()*0.492755);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bedb8() {
   return (neuron0x98b8538()*-0.329257);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba2c8() {
   return (neuron0x98b7538()*0.0301904);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba2f0() {
   return (neuron0x98b76f0()*-0.342798);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ba318() {
   return (neuron0x98b78f0()*0.0344362);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf0e8() {
   return (neuron0x98b7af0()*-0.235884);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf110() {
   return (neuron0x98b7cf0()*0.273745);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf138() {
   return (neuron0x98b7ef0()*0.255129);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf160() {
   return (neuron0x98b8108()*0.167263);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf188() {
   return (neuron0x98b8320()*0.297372);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf1b0() {
   return (neuron0x98b8538()*-0.129119);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf3d8() {
   return (neuron0x98b7538()*0.120787);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf400() {
   return (neuron0x98b76f0()*0.0830675);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf428() {
   return (neuron0x98b78f0()*0.171328);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf450() {
   return (neuron0x98b7af0()*0.112736);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf478() {
   return (neuron0x98b7cf0()*0.210702);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf4a0() {
   return (neuron0x98b7ef0()*-0.234153);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf4c8() {
   return (neuron0x98b8108()*-0.0842833);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf4f0() {
   return (neuron0x98b8320()*0.388537);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf518() {
   return (neuron0x98b8538()*0.320665);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf740() {
   return (neuron0x98b7538()*0.211355);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf768() {
   return (neuron0x98b76f0()*-0.219589);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf790() {
   return (neuron0x98b78f0()*0.171403);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf7b8() {
   return (neuron0x98b7af0()*-0.379185);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf7e0() {
   return (neuron0x98b7cf0()*0.468025);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf808() {
   return (neuron0x98b7ef0()*0.0632916);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf830() {
   return (neuron0x98b8108()*0.431481);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf858() {
   return (neuron0x98b8320()*-0.308359);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf880() {
   return (neuron0x98b8538()*-0.392911);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfaa8() {
   return (neuron0x98b7538()*0.726377);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfad0() {
   return (neuron0x98b76f0()*0.332737);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfaf8() {
   return (neuron0x98b78f0()*1.00372);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfb20() {
   return (neuron0x98b7af0()*0.0374651);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfb48() {
   return (neuron0x98b7cf0()*-0.594);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfb70() {
   return (neuron0x98b7ef0()*-0.268984);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfb98() {
   return (neuron0x98b8108()*-0.784249);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfbc0() {
   return (neuron0x98b8320()*0.145046);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfbe8() {
   return (neuron0x98b8538()*-0.506201);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfe10() {
   return (neuron0x98b7538()*-0.135267);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfe38() {
   return (neuron0x98b76f0()*-0.117841);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfe60() {
   return (neuron0x98b78f0()*-1.42042);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfe88() {
   return (neuron0x98b7af0()*-0.147838);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfeb0() {
   return (neuron0x98b7cf0()*-0.358492);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bfed8() {
   return (neuron0x98b7ef0()*-0.0976047);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bff00() {
   return (neuron0x98b8108()*-0.769452);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bff28() {
   return (neuron0x98b8320()*0.139447);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bff50() {
   return (neuron0x98b8538()*1.02673);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0178() {
   return (neuron0x98b7538()*0.12389);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c01a0() {
   return (neuron0x98b76f0()*-0.0669233);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c01c8() {
   return (neuron0x98b78f0()*-0.112801);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c01f0() {
   return (neuron0x98b7af0()*-0.234299);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0218() {
   return (neuron0x98b7cf0()*0.216245);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bbd38() {
   return (neuron0x98b7ef0()*-0.474038);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bbd60() {
   return (neuron0x98b8108()*0.523518);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bbd88() {
   return (neuron0x98b8320()*-0.339823);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bbdb0() {
   return (neuron0x98b8538()*-0.459273);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bbff0() {
   return (neuron0x98b7538()*0.390396);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc018() {
   return (neuron0x98b76f0()*-0.238147);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc040() {
   return (neuron0x98b78f0()*1.30244);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc068() {
   return (neuron0x98b7af0()*-0.260541);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc090() {
   return (neuron0x98b7cf0()*-0.698018);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc0b8() {
   return (neuron0x98b7ef0()*-0.217186);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc0e0() {
   return (neuron0x98b8108()*-1.27813);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc108() {
   return (neuron0x98b8320()*-0.0785888);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0a48() {
   return (neuron0x98b8538()*1.32052);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0c70() {
   return (neuron0x98b7538()*0.431822);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0c98() {
   return (neuron0x98b76f0()*-0.389527);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0cc0() {
   return (neuron0x98b78f0()*-0.0730945);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0ce8() {
   return (neuron0x98b7af0()*0.358447);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0d10() {
   return (neuron0x98b7cf0()*0.531937);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0d38() {
   return (neuron0x98b7ef0()*-0.00645935);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0d60() {
   return (neuron0x98b8108()*0.408061);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0d88() {
   return (neuron0x98b8320()*-0.320551);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0db0() {
   return (neuron0x98b8538()*-0.430961);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0fd8() {
   return (neuron0x98b7538()*-0.132043);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc168() {
   return (neuron0x98b76f0()*-0.226145);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc190() {
   return (neuron0x98b78f0()*0.290073);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc1b8() {
   return (neuron0x98b7af0()*-0.47642);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc218() {
   return (neuron0x98b7cf0()*0.293385);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc240() {
   return (neuron0x98b7ef0()*0.355138);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc268() {
   return (neuron0x98b8108()*0.511602);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc2c8() {
   return (neuron0x98b8320()*0.541414);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc2f0() {
   return (neuron0x98b8538()*0.139558);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc520() {
   return (neuron0x98b7538()*0.403586);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc318() {
   return (neuron0x98b76f0()*0.614931);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc3c0() {
   return (neuron0x98b78f0()*-0.273168);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc470() {
   return (neuron0x98b7af0()*0.185152);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc588() {
   return (neuron0x98b7cf0()*0.568563);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc5b0() {
   return (neuron0x98b7ef0()*-0.238916);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc5d8() {
   return (neuron0x98b8108()*0.399012);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc638() {
   return (neuron0x98b8320()*-0.391284);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc660() {
   return (neuron0x98b8538()*-0.363989);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc688() {
   return (neuron0x98b7538()*0.075233);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bc730() {
   return (neuron0x98b76f0()*-0.382302);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1c28() {
   return (neuron0x98b78f0()*0.731018);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1c50() {
   return (neuron0x98b7af0()*-0.0987142);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1c78() {
   return (neuron0x98b7cf0()*-0.209327);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1ca0() {
   return (neuron0x98b7ef0()*-0.443272);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1cc8() {
   return (neuron0x98b8108()*-0.125793);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1cf0() {
   return (neuron0x98b8320()*-0.0852449);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1d18() {
   return (neuron0x98b8538()*0.237089);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1f40() {
   return (neuron0x98b7538()*-0.0486702);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1f68() {
   return (neuron0x98b76f0()*-0.47615);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1f90() {
   return (neuron0x98b78f0()*-0.240142);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1fb8() {
   return (neuron0x98b7af0()*-0.202968);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1fe0() {
   return (neuron0x98b7cf0()*0.273579);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2008() {
   return (neuron0x98b7ef0()*-0.31089);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2030() {
   return (neuron0x98b8108()*0.412536);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2058() {
   return (neuron0x98b8320()*-0.356779);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2080() {
   return (neuron0x98b8538()*-0.322024);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c22a8() {
   return (neuron0x98b7538()*0.436175);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c22d0() {
   return (neuron0x98b76f0()*0.439296);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c22f8() {
   return (neuron0x98b78f0()*-0.000425471);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2320() {
   return (neuron0x98b7af0()*-0.21061);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2348() {
   return (neuron0x98b7cf0()*-0.239241);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2370() {
   return (neuron0x98b7ef0()*0.114459);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2398() {
   return (neuron0x98b8108()*0.634508);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c23c0() {
   return (neuron0x98b8320()*-0.16427);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c23e8() {
   return (neuron0x98b8538()*-0.156372);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2610() {
   return (neuron0x98b7538()*0.31061);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2638() {
   return (neuron0x98b76f0()*-0.0195762);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2660() {
   return (neuron0x98b78f0()*0.329743);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2688() {
   return (neuron0x98b7af0()*0.187106);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c26b0() {
   return (neuron0x98b7cf0()*0.0358951);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c26d8() {
   return (neuron0x98b7ef0()*0.746337);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2700() {
   return (neuron0x98b8108()*0.360313);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2728() {
   return (neuron0x98b8320()*-0.292853);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2750() {
   return (neuron0x98b8538()*-0.738448);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2990() {
   return (neuron0x98b7538()*0.262077);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c29b8() {
   return (neuron0x98b76f0()*-0.463629);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c29e0() {
   return (neuron0x98b78f0()*-0.170233);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2a08() {
   return (neuron0x98b7af0()*0.307517);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2a30() {
   return (neuron0x98b7cf0()*0.430154);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2a58() {
   return (neuron0x98b7ef0()*-0.375448);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2a80() {
   return (neuron0x98b8108()*-0.319842);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2aa8() {
   return (neuron0x98b8320()*-0.277192);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2ad0() {
   return (neuron0x98b8538()*0.382204);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2d10() {
   return (neuron0x98b7538()*0.277021);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2d38() {
   return (neuron0x98b76f0()*0.222942);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2d60() {
   return (neuron0x98b78f0()*-0.800421);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2d88() {
   return (neuron0x98b7af0()*-0.204694);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2db0() {
   return (neuron0x98b7cf0()*0.188359);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2dd8() {
   return (neuron0x98b7ef0()*-0.449867);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2e00() {
   return (neuron0x98b8108()*0.292717);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2e28() {
   return (neuron0x98b8320()*-0.0433576);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c2e50() {
   return (neuron0x98b8538()*-0.900579);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3090() {
   return (neuron0x98b7538()*-0.3777);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c30b8() {
   return (neuron0x98b76f0()*-0.0116004);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c30e0() {
   return (neuron0x98b78f0()*-1.12999);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3108() {
   return (neuron0x98b7af0()*-0.365572);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3130() {
   return (neuron0x98b7cf0()*0.12661);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3158() {
   return (neuron0x98b7ef0()*0.189791);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3180() {
   return (neuron0x98b8108()*0.0449616);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c31a8() {
   return (neuron0x98b8320()*-0.178063);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c31d0() {
   return (neuron0x98b8538()*-0.425443);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3410() {
   return (neuron0x98b7538()*-0.305186);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3438() {
   return (neuron0x98b76f0()*-0.259637);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3460() {
   return (neuron0x98b78f0()*0.2457);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3488() {
   return (neuron0x98b7af0()*-0.386399);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c34b0() {
   return (neuron0x98b7cf0()*-0.164088);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c34d8() {
   return (neuron0x98b7ef0()*0.496221);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3500() {
   return (neuron0x98b8108()*-0.355387);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3528() {
   return (neuron0x98b8320()*-0.44425);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3550() {
   return (neuron0x98b8538()*0.653293);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3790() {
   return (neuron0x98b7538()*0.279851);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c37b8() {
   return (neuron0x98b76f0()*-0.338217);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c37e0() {
   return (neuron0x98b78f0()*-0.192709);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3808() {
   return (neuron0x98b7af0()*0.08273);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3830() {
   return (neuron0x98b7cf0()*0.250388);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3858() {
   return (neuron0x98b7ef0()*-0.165044);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3880() {
   return (neuron0x98b8108()*0.404824);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c38a8() {
   return (neuron0x98b8320()*-0.53824);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c38d0() {
   return (neuron0x98b8538()*0.267633);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3b10() {
   return (neuron0x98b7538()*-0.350454);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3b38() {
   return (neuron0x98b76f0()*-0.164977);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3b60() {
   return (neuron0x98b78f0()*0.178895);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3b88() {
   return (neuron0x98b7af0()*0.422516);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3bb0() {
   return (neuron0x98b7cf0()*0.310938);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3bd8() {
   return (neuron0x98b7ef0()*-0.0211816);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3c00() {
   return (neuron0x98b8108()*-0.0926341);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3c28() {
   return (neuron0x98b8320()*-0.495614);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3c50() {
   return (neuron0x98b8538()*-0.314788);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3e90() {
   return (neuron0x98b7538()*0.391008);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3eb8() {
   return (neuron0x98b76f0()*-0.510065);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3ee0() {
   return (neuron0x98b78f0()*0.335725);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3f08() {
   return (neuron0x98b7af0()*0.440673);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3f30() {
   return (neuron0x98b7cf0()*-0.00338095);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3f58() {
   return (neuron0x98b7ef0()*-0.0722157);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3f80() {
   return (neuron0x98b8108()*0.0569167);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3fa8() {
   return (neuron0x98b8320()*0.174939);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c3fd0() {
   return (neuron0x98b8538()*0.00591938);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4210() {
   return (neuron0x98b7538()*-0.196552);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4238() {
   return (neuron0x98b76f0()*-0.138285);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4260() {
   return (neuron0x98b78f0()*-0.505432);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4288() {
   return (neuron0x98b7af0()*-0.0732235);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c42b0() {
   return (neuron0x98b7cf0()*0.216313);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c42d8() {
   return (neuron0x98b7ef0()*0.0418426);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4300() {
   return (neuron0x98b8108()*0.801222);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4328() {
   return (neuron0x98b8320()*-0.0299973);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4350() {
   return (neuron0x98b8538()*0.601119);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4590() {
   return (neuron0x98b7538()*0.0646292);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c45b8() {
   return (neuron0x98b76f0()*-0.501786);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c45e0() {
   return (neuron0x98b78f0()*-0.134884);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4608() {
   return (neuron0x98b7af0()*0.332101);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4630() {
   return (neuron0x98b7cf0()*0.190549);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4658() {
   return (neuron0x98b7ef0()*0.0956211);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4680() {
   return (neuron0x98b8108()*-0.0859371);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c46a8() {
   return (neuron0x98b8320()*-0.030941);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c46d0() {
   return (neuron0x98b8538()*0.498482);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4910() {
   return (neuron0x98b7538()*0.208429);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4938() {
   return (neuron0x98b76f0()*1.04064);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4960() {
   return (neuron0x98b78f0()*0.53621);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4988() {
   return (neuron0x98b7af0()*-0.0775609);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c49b0() {
   return (neuron0x98b7cf0()*0.0969399);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c49d8() {
   return (neuron0x98b7ef0()*0.544298);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4a00() {
   return (neuron0x98b8108()*-0.0334503);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4a28() {
   return (neuron0x98b8320()*0.354957);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4a50() {
   return (neuron0x98b8538()*0.0603821);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4c90() {
   return (neuron0x98b7538()*-0.080033);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4cb8() {
   return (neuron0x98b76f0()*-0.553363);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4ce0() {
   return (neuron0x98b78f0()*0.0910808);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4d08() {
   return (neuron0x98b7af0()*-0.383105);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4d30() {
   return (neuron0x98b7cf0()*0.317335);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4d58() {
   return (neuron0x98b7ef0()*0.406389);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4d80() {
   return (neuron0x98b8108()*-0.239705);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4da8() {
   return (neuron0x98b8320()*-0.335396);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c4dd0() {
   return (neuron0x98b8538()*-0.539286);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5010() {
   return (neuron0x98b7538()*0.0241767);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5038() {
   return (neuron0x98b76f0()*0.233138);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5060() {
   return (neuron0x98b78f0()*0.1283);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5088() {
   return (neuron0x98b7af0()*0.452479);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c50b0() {
   return (neuron0x98b7cf0()*-0.520598);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c50d8() {
   return (neuron0x98b7ef0()*-0.0533456);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5100() {
   return (neuron0x98b8108()*0.195742);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5128() {
   return (neuron0x98b8320()*0.128951);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5150() {
   return (neuron0x98b8538()*0.370117);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5390() {
   return (neuron0x98b7538()*0.186639);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c53b8() {
   return (neuron0x98b76f0()*-0.0394773);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c53e0() {
   return (neuron0x98b78f0()*1.0513);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5408() {
   return (neuron0x98b7af0()*0.0208156);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5430() {
   return (neuron0x98b7cf0()*-0.920126);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5458() {
   return (neuron0x98b7ef0()*-0.573758);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5480() {
   return (neuron0x98b8108()*0.793967);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c54a8() {
   return (neuron0x98b8320()*0.221531);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c54d0() {
   return (neuron0x98b8538()*0.379043);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5710() {
   return (neuron0x98b7538()*0.179203);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5738() {
   return (neuron0x98b76f0()*-0.118114);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5760() {
   return (neuron0x98b78f0()*0.329143);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5788() {
   return (neuron0x98b7af0()*0.246774);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c57b0() {
   return (neuron0x98b7cf0()*-0.174779);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c57d8() {
   return (neuron0x98b7ef0()*-0.212842);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5800() {
   return (neuron0x98b8108()*-0.243826);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5828() {
   return (neuron0x98b8320()*-0.026907);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5850() {
   return (neuron0x98b8538()*0.415977);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5a90() {
   return (neuron0x98b7538()*0.192849);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5ab8() {
   return (neuron0x98b76f0()*0.126595);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5ae0() {
   return (neuron0x98b78f0()*-1.19178);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5b08() {
   return (neuron0x98b7af0()*0.181396);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5b30() {
   return (neuron0x98b7cf0()*-0.129611);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5b58() {
   return (neuron0x98b7ef0()*0.0122287);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5b80() {
   return (neuron0x98b8108()*2.00736);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5ba8() {
   return (neuron0x98b8320()*0.166968);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5bd0() {
   return (neuron0x98b8538()*-1.58836);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5e10() {
   return (neuron0x98b7538()*0.231654);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5e38() {
   return (neuron0x98b76f0()*0.181036);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5e60() {
   return (neuron0x98b78f0()*-1.14784);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5e88() {
   return (neuron0x98b7af0()*0.031382);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5eb0() {
   return (neuron0x98b7cf0()*0.778351);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5ed8() {
   return (neuron0x98b7ef0()*-0.0658599);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5f00() {
   return (neuron0x98b8108()*1.94682);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5f28() {
   return (neuron0x98b8320()*0.0231481);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c5f50() {
   return (neuron0x98b8538()*1.61389);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6190() {
   return (neuron0x98b7538()*0.0472437);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c61b8() {
   return (neuron0x98b76f0()*-0.146765);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c61e0() {
   return (neuron0x98b78f0()*-0.592963);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6208() {
   return (neuron0x98b7af0()*-0.173806);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6230() {
   return (neuron0x98b7cf0()*0.64079);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6258() {
   return (neuron0x98b7ef0()*0.290961);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6280() {
   return (neuron0x98b8108()*-1.3342);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c62a8() {
   return (neuron0x98b8320()*-0.244385);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c62d0() {
   return (neuron0x98b8538()*-0.570155);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6510() {
   return (neuron0x98b7538()*-0.0134099);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6538() {
   return (neuron0x98b76f0()*0.205049);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6560() {
   return (neuron0x98b78f0()*-2.01159);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6588() {
   return (neuron0x98b7af0()*-0.174024);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c65b0() {
   return (neuron0x98b7cf0()*-0.142169);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c65d8() {
   return (neuron0x98b7ef0()*-0.145573);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6600() {
   return (neuron0x98b8108()*0.332832);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6628() {
   return (neuron0x98b8320()*0.0214884);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6650() {
   return (neuron0x98b8538()*-0.141312);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98befe0() {
   return (neuron0x98b7538()*0.188122);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf008() {
   return (neuron0x98b76f0()*0.241938);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf030() {
   return (neuron0x98b78f0()*-0.225646);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf058() {
   return (neuron0x98b7af0()*-0.213237);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf080() {
   return (neuron0x98b7cf0()*-0.543902);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98bf0a8() {
   return (neuron0x98b7ef0()*0.00620769);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6a98() {
   return (neuron0x98b8108()*0.429929);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6ac0() {
   return (neuron0x98b8320()*-0.225567);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6ae8() {
   return (neuron0x98b8538()*0.51781);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6d10() {
   return (neuron0x98b7538()*0.131335);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6d38() {
   return (neuron0x98b76f0()*0.182447);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6d60() {
   return (neuron0x98b78f0()*0.434278);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6d88() {
   return (neuron0x98b7af0()*-0.291662);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6db0() {
   return (neuron0x98b7cf0()*0.0498856);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6dd8() {
   return (neuron0x98b7ef0()*0.10065);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6e00() {
   return (neuron0x98b8108()*-0.00101441);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6e28() {
   return (neuron0x98b8320()*0.147289);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c6e50() {
   return (neuron0x98b8538()*0.296457);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0458() {
   return (neuron0x98b7538()*-0.17971);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0480() {
   return (neuron0x98b76f0()*-0.632215);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c04a8() {
   return (neuron0x98b78f0()*0.359193);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c04d0() {
   return (neuron0x98b7af0()*0.0761103);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c04f8() {
   return (neuron0x98b7cf0()*0.110675);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0520() {
   return (neuron0x98b7ef0()*0.121622);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0548() {
   return (neuron0x98b8108()*-0.317221);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0570() {
   return (neuron0x98b8320()*0.630402);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0598() {
   return (neuron0x98b8538()*-0.0999932);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c07d8() {
   return (neuron0x98b7538()*0.125916);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0800() {
   return (neuron0x98b76f0()*0.0463815);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0828() {
   return (neuron0x98b78f0()*0.46226);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0850() {
   return (neuron0x98b7af0()*0.0760952);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0878() {
   return (neuron0x98b7cf0()*0.520337);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c08a0() {
   return (neuron0x98b7ef0()*-0.603506);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c08c8() {
   return (neuron0x98b8108()*-0.581883);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c08f0() {
   return (neuron0x98b8320()*0.173502);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c0918() {
   return (neuron0x98b8538()*-0.185791);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c7fa8() {
   return (neuron0x98b7538()*-0.494308);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c7fd0() {
   return (neuron0x98b76f0()*-0.334144);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c7ff8() {
   return (neuron0x98b78f0()*1.55251);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8020() {
   return (neuron0x98b7af0()*0.208264);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8048() {
   return (neuron0x98b7cf0()*-0.0221289);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8070() {
   return (neuron0x98b7ef0()*-0.110936);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8098() {
   return (neuron0x98b8108()*-0.0484301);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c80c0() {
   return (neuron0x98b8320()*-0.00451028);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c80e8() {
   return (neuron0x98b8538()*0.187076);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8310() {
   return (neuron0x98b7538()*-0.0160249);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8338() {
   return (neuron0x98b76f0()*0.327187);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8360() {
   return (neuron0x98b78f0()*-0.559024);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8388() {
   return (neuron0x98b7af0()*-0.298337);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c83b0() {
   return (neuron0x98b7cf0()*0.316961);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c83d8() {
   return (neuron0x98b7ef0()*-0.574119);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8400() {
   return (neuron0x98b8108()*-0.352871);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8428() {
   return (neuron0x98b8320()*0.137254);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8450() {
   return (neuron0x98b8538()*0.0661704);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8690() {
   return (neuron0x98b7538()*0.141419);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c86b8() {
   return (neuron0x98b76f0()*-0.239032);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c86e0() {
   return (neuron0x98b78f0()*0.234263);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8708() {
   return (neuron0x98b7af0()*0.0320217);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8730() {
   return (neuron0x98b7cf0()*-0.110949);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8758() {
   return (neuron0x98b7ef0()*0.0590527);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8780() {
   return (neuron0x98b8108()*-0.217382);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c87a8() {
   return (neuron0x98b8320()*-0.0416891);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c87d0() {
   return (neuron0x98b8538()*0.447767);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8a10() {
   return (neuron0x98b7538()*-0.483237);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8a38() {
   return (neuron0x98b76f0()*0.42349);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8a60() {
   return (neuron0x98b78f0()*0.636931);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8a88() {
   return (neuron0x98b7af0()*-0.222019);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8ab0() {
   return (neuron0x98b7cf0()*-0.266739);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8ad8() {
   return (neuron0x98b7ef0()*-0.175339);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8b00() {
   return (neuron0x98b8108()*-0.472812);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8b28() {
   return (neuron0x98b8320()*0.224778);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8b50() {
   return (neuron0x98b8538()*0.982208);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c8d90() {
   return (neuron0x98b7538()*-0.40237);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1000() {
   return (neuron0x98b76f0()*-0.169653);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1028() {
   return (neuron0x98b78f0()*-0.832918);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1050() {
   return (neuron0x98b7af0()*-0.503108);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1280() {
   return (neuron0x98b7cf0()*-1.28626);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c12a8() {
   return (neuron0x98b7ef0()*-0.0917628);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c14d8() {
   return (neuron0x98b8108()*-0.952059);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1500() {
   return (neuron0x98b8320()*-0.0577353);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c1738() {
   return (neuron0x98b8538()*0.629862);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9a30() {
   return (neuron0x98b7538()*-0.0374315);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9a58() {
   return (neuron0x98b76f0()*-0.00748411);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9a80() {
   return (neuron0x98b78f0()*0.969717);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9aa8() {
   return (neuron0x98b7af0()*0.179456);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9ad0() {
   return (neuron0x98b7cf0()*-0.602006);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9af8() {
   return (neuron0x98b7ef0()*-0.164495);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9b20() {
   return (neuron0x98b8108()*-1.08538);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9b48() {
   return (neuron0x98b8320()*-0.173576);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9b70() {
   return (neuron0x98b8538()*-0.801244);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9d98() {
   return (neuron0x98b7538()*0.228634);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9dc0() {
   return (neuron0x98b76f0()*0.0977318);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9de8() {
   return (neuron0x98b78f0()*-0.0926238);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9e10() {
   return (neuron0x98b7af0()*-0.272279);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9e38() {
   return (neuron0x98b7cf0()*0.324922);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9e60() {
   return (neuron0x98b7ef0()*0.260411);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9e88() {
   return (neuron0x98b8108()*0.149045);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9eb0() {
   return (neuron0x98b8320()*0.31197);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98c9ed8() {
   return (neuron0x98b8538()*-0.592543);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca118() {
   return (neuron0x98b7538()*-0.242402);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca140() {
   return (neuron0x98b76f0()*-0.318929);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca168() {
   return (neuron0x98b78f0()*0.31185);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca190() {
   return (neuron0x98b7af0()*0.304094);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca1b8() {
   return (neuron0x98b7cf0()*-0.230341);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca1e0() {
   return (neuron0x98b7ef0()*-0.629939);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca208() {
   return (neuron0x98b8108()*0.302517);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca230() {
   return (neuron0x98b8320()*-0.672953);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca258() {
   return (neuron0x98b8538()*0.368128);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca498() {
   return (neuron0x98b7538()*0.0918835);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca4c0() {
   return (neuron0x98b76f0()*0.389145);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca4e8() {
   return (neuron0x98b78f0()*0.756696);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca510() {
   return (neuron0x98b7af0()*0.684039);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca538() {
   return (neuron0x98b7cf0()*0.475782);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca560() {
   return (neuron0x98b7ef0()*-0.506583);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca588() {
   return (neuron0x98b8108()*-0.138634);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca5b0() {
   return (neuron0x98b8320()*-0.0818362);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca5d8() {
   return (neuron0x98b8538()*0.246736);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca818() {
   return (neuron0x98b7538()*-0.433232);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca840() {
   return (neuron0x98b76f0()*0.358223);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca868() {
   return (neuron0x98b78f0()*1.69796);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca890() {
   return (neuron0x98b7af0()*-0.0566994);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca8b8() {
   return (neuron0x98b7cf0()*0.26088);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca8e0() {
   return (neuron0x98b7ef0()*0.214877);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca908() {
   return (neuron0x98b8108()*-0.185912);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca930() {
   return (neuron0x98b8320()*0.165921);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ca958() {
   return (neuron0x98b8538()*-0.120914);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cab98() {
   return (neuron0x98b7538()*-0.10969);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cabc0() {
   return (neuron0x98b76f0()*0.0839283);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cabe8() {
   return (neuron0x98b78f0()*-0.128074);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cac10() {
   return (neuron0x98b7af0()*0.624124);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cac38() {
   return (neuron0x98b7cf0()*-0.00876576);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cac60() {
   return (neuron0x98b7ef0()*-0.123158);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cac88() {
   return (neuron0x98b8108()*-0.45492);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cacb0() {
   return (neuron0x98b8320()*-0.189449);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cacd8() {
   return (neuron0x98b8538()*0.811684);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98caf18() {
   return (neuron0x98b7538()*0.283517);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98caf40() {
   return (neuron0x98b76f0()*0.479224);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98caf68() {
   return (neuron0x98b78f0()*-0.581868);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98caf90() {
   return (neuron0x98b7af0()*-0.17255);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cafb8() {
   return (neuron0x98b7cf0()*-0.213887);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cafe0() {
   return (neuron0x98b7ef0()*0.183422);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb008() {
   return (neuron0x98b8108()*-0.302612);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb030() {
   return (neuron0x98b8320()*0.0816563);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb058() {
   return (neuron0x98b8538()*0.677095);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb298() {
   return (neuron0x98b7538()*0.125659);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb2c0() {
   return (neuron0x98b76f0()*-0.0265363);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb2e8() {
   return (neuron0x98b78f0()*0.00722694);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb310() {
   return (neuron0x98b7af0()*0.277663);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb338() {
   return (neuron0x98b7cf0()*0.20482);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb360() {
   return (neuron0x98b7ef0()*-0.387965);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb388() {
   return (neuron0x98b8108()*0.159567);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb3b0() {
   return (neuron0x98b8320()*-0.313754);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb3d8() {
   return (neuron0x98b8538()*-0.354306);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb618() {
   return (neuron0x98b7538()*0.078727);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb640() {
   return (neuron0x98b76f0()*0.301521);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb668() {
   return (neuron0x98b78f0()*-0.606687);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb690() {
   return (neuron0x98b7af0()*0.090175);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb6b8() {
   return (neuron0x98b7cf0()*-1.17145);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb6e0() {
   return (neuron0x98b7ef0()*-0.108692);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb708() {
   return (neuron0x98b8108()*-0.147222);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb730() {
   return (neuron0x98b8320()*0.37329);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb758() {
   return (neuron0x98b8538()*-0.23034);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb998() {
   return (neuron0x98b7538()*-0.0951455);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb9c0() {
   return (neuron0x98b76f0()*-0.318813);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cb9e8() {
   return (neuron0x98b78f0()*-0.0864611);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cba10() {
   return (neuron0x98b7af0()*-0.362612);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cba38() {
   return (neuron0x98b7cf0()*0.369354);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cba60() {
   return (neuron0x98b7ef0()*0.500754);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cba88() {
   return (neuron0x98b8108()*-0.296787);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbab0() {
   return (neuron0x98b8320()*0.614792);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbad8() {
   return (neuron0x98b8538()*0.0513839);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbd18() {
   return (neuron0x98b7538()*-0.441288);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbd40() {
   return (neuron0x98b76f0()*-0.0518326);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbd68() {
   return (neuron0x98b78f0()*-0.896867);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbd90() {
   return (neuron0x98b7af0()*0.136447);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbdb8() {
   return (neuron0x98b7cf0()*-1.14032);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbde0() {
   return (neuron0x98b7ef0()*0.0197685);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbe08() {
   return (neuron0x98b8108()*0.844252);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbe30() {
   return (neuron0x98b8320()*-0.0160842);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cbe58() {
   return (neuron0x98b8538()*0.615273);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc098() {
   return (neuron0x98b7538()*0.117633);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc0c0() {
   return (neuron0x98b76f0()*-0.015928);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc0e8() {
   return (neuron0x98b78f0()*-0.442552);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc110() {
   return (neuron0x98b7af0()*0.206693);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc138() {
   return (neuron0x98b7cf0()*-0.302925);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc160() {
   return (neuron0x98b7ef0()*-0.342598);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc188() {
   return (neuron0x98b8108()*0.295117);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc1b0() {
   return (neuron0x98b8320()*-0.143353);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc1d8() {
   return (neuron0x98b8538()*0.02827);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc418() {
   return (neuron0x98b7538()*0.22689);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc440() {
   return (neuron0x98b76f0()*-0.151438);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc468() {
   return (neuron0x98b78f0()*-1.73346);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc490() {
   return (neuron0x98b7af0()*0.130054);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc4b8() {
   return (neuron0x98b7cf0()*0.278875);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc4e0() {
   return (neuron0x98b7ef0()*-0.1273);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc508() {
   return (neuron0x98b8108()*0.0333193);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc530() {
   return (neuron0x98b8320()*0.00180055);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc558() {
   return (neuron0x98b8538()*1.14185);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc798() {
   return (neuron0x98b7538()*-0.623725);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc7c0() {
   return (neuron0x98b76f0()*0.180633);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc7e8() {
   return (neuron0x98b78f0()*0.175361);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc810() {
   return (neuron0x98b7af0()*-0.453215);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc838() {
   return (neuron0x98b7cf0()*0.0466112);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc860() {
   return (neuron0x98b7ef0()*-0.0956192);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc888() {
   return (neuron0x98b8108()*0.885758);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc8b0() {
   return (neuron0x98b8320()*0.0494863);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cc8d8() {
   return (neuron0x98b8538()*-0.730456);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccb18() {
   return (neuron0x98b7538()*0.0015295);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccb40() {
   return (neuron0x98b76f0()*0.18953);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccb68() {
   return (neuron0x98b78f0()*0.260903);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccb90() {
   return (neuron0x98b7af0()*0.101632);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccbb8() {
   return (neuron0x98b7cf0()*0.0513597);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccbe0() {
   return (neuron0x98b7ef0()*0.145771);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccc08() {
   return (neuron0x98b8108()*-0.28837);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccc30() {
   return (neuron0x98b8320()*0.0152368);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccc58() {
   return (neuron0x98b8538()*0.179653);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cce98() {
   return (neuron0x98b7538()*0.10205);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccec0() {
   return (neuron0x98b76f0()*0.481081);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccee8() {
   return (neuron0x98b78f0()*0.372943);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccf10() {
   return (neuron0x98b7af0()*0.468593);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccf38() {
   return (neuron0x98b7cf0()*-0.0946473);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccf60() {
   return (neuron0x98b7ef0()*-0.24389);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccf88() {
   return (neuron0x98b8108()*-0.0652167);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccfb0() {
   return (neuron0x98b8320()*0.0398889);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ccfd8() {
   return (neuron0x98b8538()*1.09158);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd218() {
   return (neuron0x98b7538()*-0.187142);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd240() {
   return (neuron0x98b76f0()*0.000881284);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd268() {
   return (neuron0x98b78f0()*-0.07478);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd290() {
   return (neuron0x98b7af0()*-0.324675);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd2b8() {
   return (neuron0x98b7cf0()*0.125806);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd2e0() {
   return (neuron0x98b7ef0()*-0.0110643);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd308() {
   return (neuron0x98b8108()*0.262163);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd330() {
   return (neuron0x98b8320()*-0.106709);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd358() {
   return (neuron0x98b8538()*0.462002);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd598() {
   return (neuron0x98b7538()*-0.0488773);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd5c0() {
   return (neuron0x98b76f0()*-0.371163);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd5e8() {
   return (neuron0x98b78f0()*0.583126);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd610() {
   return (neuron0x98b7af0()*-0.0245036);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd638() {
   return (neuron0x98b7cf0()*0.291949);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd660() {
   return (neuron0x98b7ef0()*0.91328);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd688() {
   return (neuron0x98b8108()*0.547387);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd6b0() {
   return (neuron0x98b8320()*0.441738);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd6d8() {
   return (neuron0x98b8538()*0.72706);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd918() {
   return (neuron0x98b7538()*0.0442314);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd940() {
   return (neuron0x98b76f0()*-0.0490369);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd968() {
   return (neuron0x98b78f0()*-2.04592);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd990() {
   return (neuron0x98b7af0()*0.231668);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd9b8() {
   return (neuron0x98b7cf0()*-0.249874);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cd9e0() {
   return (neuron0x98b7ef0()*0.169415);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cda08() {
   return (neuron0x98b8108()*0.672764);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cda30() {
   return (neuron0x98b8320()*0.0685514);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cda58() {
   return (neuron0x98b8538()*1.16546);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cdc98() {
   return (neuron0x98b7538()*-0.272959);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cdcc0() {
   return (neuron0x98b76f0()*0.334784);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cdce8() {
   return (neuron0x98b78f0()*0.169509);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cdd10() {
   return (neuron0x98b7af0()*0.0820308);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cdd38() {
   return (neuron0x98b7cf0()*-0.218085);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cdd60() {
   return (neuron0x98b7ef0()*-0.0803882);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cdd88() {
   return (neuron0x98b8108()*-0.368457);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cddb0() {
   return (neuron0x98b8320()*-0.107049);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cddd8() {
   return (neuron0x98b8538()*-0.109176);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce018() {
   return (neuron0x98b7538()*-0.321475);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce040() {
   return (neuron0x98b76f0()*-0.0148787);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce068() {
   return (neuron0x98b78f0()*-0.732919);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce090() {
   return (neuron0x98b7af0()*-0.180213);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce0b8() {
   return (neuron0x98b7cf0()*-0.932165);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce0e0() {
   return (neuron0x98b7ef0()*0.0796258);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce108() {
   return (neuron0x98b8108()*-1.08884);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce130() {
   return (neuron0x98b8320()*-0.140535);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce158() {
   return (neuron0x98b8538()*1.14934);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce398() {
   return (neuron0x98b7538()*-0.198582);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce3c0() {
   return (neuron0x98b76f0()*-0.0544562);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce3e8() {
   return (neuron0x98b78f0()*1.67804);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce410() {
   return (neuron0x98b7af0()*0.073151);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce438() {
   return (neuron0x98b7cf0()*-0.0203519);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce460() {
   return (neuron0x98b7ef0()*0.0801123);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce488() {
   return (neuron0x98b8108()*-2.62373);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce4b0() {
   return (neuron0x98b8320()*0.0084158);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce4d8() {
   return (neuron0x98b8538()*-1.81115);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce718() {
   return (neuron0x98b7538()*-0.223157);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce740() {
   return (neuron0x98b76f0()*-0.0578948);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce768() {
   return (neuron0x98b78f0()*-0.805496);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce790() {
   return (neuron0x98b7af0()*-0.0700423);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce7b8() {
   return (neuron0x98b7cf0()*-1.32463);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce7e0() {
   return (neuron0x98b7ef0()*-0.0779233);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce808() {
   return (neuron0x98b8108()*-0.591029);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce830() {
   return (neuron0x98b8320()*-0.129884);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ce858() {
   return (neuron0x98b8538()*-1.38529);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cea98() {
   return (neuron0x98b7538()*-0.0346832);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ceac0() {
   return (neuron0x98b76f0()*-0.039545);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ceae8() {
   return (neuron0x98b78f0()*0.439447);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ceb10() {
   return (neuron0x98b7af0()*0.352453);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ceb38() {
   return (neuron0x98b7cf0()*0.0590502);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ceb60() {
   return (neuron0x98b7ef0()*0.186839);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ceb88() {
   return (neuron0x98b8108()*0.308103);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cebb0() {
   return (neuron0x98b8320()*0.262632);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cebd8() {
   return (neuron0x98b8538()*0.338978);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cee18() {
   return (neuron0x98b7538()*-0.204919);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cee40() {
   return (neuron0x98b76f0()*-0.156223);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cee68() {
   return (neuron0x98b78f0()*1.56432);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cee90() {
   return (neuron0x98b7af0()*0.0410347);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ceeb8() {
   return (neuron0x98b7cf0()*-0.233542);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98ceee0() {
   return (neuron0x98b7ef0()*-0.0902163);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cef08() {
   return (neuron0x98b8108()*0.829327);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cef30() {
   return (neuron0x98b8320()*0.00239132);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cef58() {
   return (neuron0x98b8538()*-0.193959);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf198() {
   return (neuron0x98b7538()*0.0803324);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf1c0() {
   return (neuron0x98b76f0()*-0.112236);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf1e8() {
   return (neuron0x98b78f0()*0.389486);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf210() {
   return (neuron0x98b7af0()*0.254742);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf238() {
   return (neuron0x98b7cf0()*-0.483846);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf260() {
   return (neuron0x98b7ef0()*-0.252494);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf288() {
   return (neuron0x98b8108()*-0.631301);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf2b0() {
   return (neuron0x98b8320()*0.109094);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf2d8() {
   return (neuron0x98b8538()*-0.432842);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf518() {
   return (neuron0x98b7538()*-0.335587);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf540() {
   return (neuron0x98b76f0()*0.0870468);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf568() {
   return (neuron0x98b78f0()*0.0311664);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf590() {
   return (neuron0x98b7af0()*-0.25252);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf5b8() {
   return (neuron0x98b7cf0()*-0.136565);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf5e0() {
   return (neuron0x98b7ef0()*0.235662);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf608() {
   return (neuron0x98b8108()*0.375266);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf630() {
   return (neuron0x98b8320()*-0.147921);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf658() {
   return (neuron0x98b8538()*-0.439644);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf898() {
   return (neuron0x98b7538()*0.00993358);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf8c0() {
   return (neuron0x98b76f0()*-0.581812);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf8e8() {
   return (neuron0x98b78f0()*0.19004);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf910() {
   return (neuron0x98b7af0()*0.428753);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf938() {
   return (neuron0x98b7cf0()*-0.213533);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf960() {
   return (neuron0x98b7ef0()*-0.597788);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf988() {
   return (neuron0x98b8108()*-0.110979);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf9b0() {
   return (neuron0x98b8320()*0.286673);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cf9d8() {
   return (neuron0x98b8538()*-0.597026);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfc18() {
   return (neuron0x98b7538()*0.196032);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfc40() {
   return (neuron0x98b76f0()*0.120959);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfc68() {
   return (neuron0x98b78f0()*0.839939);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfc90() {
   return (neuron0x98b7af0()*0.111811);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfcb8() {
   return (neuron0x98b7cf0()*2.20737);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfce0() {
   return (neuron0x98b7ef0()*0.0205203);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfd08() {
   return (neuron0x98b8108()*1.14834);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfd30() {
   return (neuron0x98b8320()*0.106379);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cfd58() {
   return (neuron0x98b8538()*1.15416);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cff98() {
   return (neuron0x98b7538()*0.372736);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cffc0() {
   return (neuron0x98b76f0()*-0.149073);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98cffe8() {
   return (neuron0x98b78f0()*0.211176);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0010() {
   return (neuron0x98b7af0()*-0.516602);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0038() {
   return (neuron0x98b7cf0()*0.0568968);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0060() {
   return (neuron0x98b7ef0()*-0.447072);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0088() {
   return (neuron0x98b8108()*0.0772322);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d00b0() {
   return (neuron0x98b8320()*-0.0405155);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d00d8() {
   return (neuron0x98b8538()*0.299263);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0318() {
   return (neuron0x98b7538()*-0.193135);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0340() {
   return (neuron0x98b76f0()*0.360039);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0368() {
   return (neuron0x98b78f0()*-0.717195);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0390() {
   return (neuron0x98b7af0()*0.287069);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d03b8() {
   return (neuron0x98b7cf0()*-0.884884);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d03e0() {
   return (neuron0x98b7ef0()*-0.0220556);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0408() {
   return (neuron0x98b8108()*-0.438316);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0430() {
   return (neuron0x98b8320()*0.248202);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0458() {
   return (neuron0x98b8538()*0.790622);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0698() {
   return (neuron0x98b7538()*0.23006);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d06c0() {
   return (neuron0x98b76f0()*0.153678);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d06e8() {
   return (neuron0x98b78f0()*0.110976);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0710() {
   return (neuron0x98b7af0()*-0.0640525);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0738() {
   return (neuron0x98b7cf0()*-0.502974);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0760() {
   return (neuron0x98b7ef0()*0.688545);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0788() {
   return (neuron0x98b8108()*1.47287);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d07b0() {
   return (neuron0x98b8320()*0.270806);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d07d8() {
   return (neuron0x98b8538()*0.50934);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0a18() {
   return (neuron0x98b7538()*0.464688);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0a40() {
   return (neuron0x98b76f0()*0.153555);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0a68() {
   return (neuron0x98b78f0()*0.108728);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0a90() {
   return (neuron0x98b7af0()*-0.0284892);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0ab8() {
   return (neuron0x98b7cf0()*0.634073);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0ae0() {
   return (neuron0x98b7ef0()*-0.0550252);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0b08() {
   return (neuron0x98b8108()*0.427887);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0b30() {
   return (neuron0x98b8320()*-0.479768);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0b58() {
   return (neuron0x98b8538()*-0.212576);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0d98() {
   return (neuron0x98b7538()*-0.464542);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0dc0() {
   return (neuron0x98b76f0()*-0.133869);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0de8() {
   return (neuron0x98b78f0()*0.137276);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0e10() {
   return (neuron0x98b7af0()*0.169511);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0e38() {
   return (neuron0x98b7cf0()*-0.627929);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0e60() {
   return (neuron0x98b7ef0()*0.0209301);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0e88() {
   return (neuron0x98b8108()*-0.566275);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0eb0() {
   return (neuron0x98b8320()*-0.311751);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d0ed8() {
   return (neuron0x98b8538()*0.179864);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1118() {
   return (neuron0x98b7538()*-0.131435);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1140() {
   return (neuron0x98b76f0()*-0.100629);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1168() {
   return (neuron0x98b78f0()*1.18118);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1190() {
   return (neuron0x98b7af0()*-0.136266);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d11b8() {
   return (neuron0x98b7cf0()*0.614957);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d11e0() {
   return (neuron0x98b7ef0()*0.00847432);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1208() {
   return (neuron0x98b8108()*-1.94461);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1230() {
   return (neuron0x98b8320()*-0.0980054);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1258() {
   return (neuron0x98b8538()*1.49582);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1498() {
   return (neuron0x98b7538()*-0.236944);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d14c0() {
   return (neuron0x98b76f0()*-0.137721);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d14e8() {
   return (neuron0x98b78f0()*0.0541226);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1510() {
   return (neuron0x98b7af0()*-0.187915);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1538() {
   return (neuron0x98b7cf0()*-0.262507);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1560() {
   return (neuron0x98b7ef0()*0.167813);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1588() {
   return (neuron0x98b8108()*-0.0784288);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d15b0() {
   return (neuron0x98b8320()*0.280782);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d15d8() {
   return (neuron0x98b8538()*0.505967);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1728() {
   return (neuron0x98b8890()*-0.0634157);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1750() {
   return (neuron0x98b8b88()*-0.198618);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1778() {
   return (neuron0x98b8f78()*-0.383004);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d17a0() {
   return (neuron0x98b9298()*-1.50268);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d17c8() {
   return (neuron0x98b9690()*-1.01151);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d17f0() {
   return (neuron0x98b99f8()*0.137865);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1818() {
   return (neuron0x98b9d60()*0.288373);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1840() {
   return (neuron0x98ba0c8()*-0.663515);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1868() {
   return (neuron0x98ba5a8()*0.451297);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1890() {
   return (neuron0x98ba928()*-1.25865);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d18b8() {
   return (neuron0x98baca8()*-0.347194);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d18e0() {
   return (neuron0x98bb028()*0.331505);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1908() {
   return (neuron0x98bb3a8()*0.946131);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1930() {
   return (neuron0x98bb728()*0.00545195);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1958() {
   return (neuron0x98bbaa8()*-0.611266);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1980() {
   return (neuron0x98ba3a0()*-0.204913);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1a30() {
   return (neuron0x98bc770()*0.714601);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1a58() {
   return (neuron0x98bca18()*1.34087);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1a80() {
   return (neuron0x98bccc0()*-0.147829);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1aa8() {
   return (neuron0x98bcf68()*0.131349);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1ad0() {
   return (neuron0x98be3a8()*-1.3415);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1af8() {
   return (neuron0x98be710()*-0.552927);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1b20() {
   return (neuron0x98bea78()*-0.0536489);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1b48() {
   return (neuron0x98bede0()*0.0359545);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1b70() {
   return (neuron0x98bf1d8()*0.241002);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1b98() {
   return (neuron0x98bf540()*0.0269934);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1bc0() {
   return (neuron0x98bf8a8()*-0.627804);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1be8() {
   return (neuron0x98bfc10()*-0.637711);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1c10() {
   return (neuron0x98bff78()*-0.152223);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1c38() {
   return (neuron0x98bbdd8()*-0.629989);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1c60() {
   return (neuron0x98c0a70()*0.431815);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1c88() {
   return (neuron0x98c0dd8()*0.4771);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d19a8() {
   return (neuron0x98c1948()*0.413672);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d19d0() {
   return (neuron0x98c1a70()*0.677898);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d19f8() {
   return (neuron0x98c1d40()*-0.470245);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1db8() {
   return (neuron0x98c20a8()*-0.287012);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1de0() {
   return (neuron0x98c2410()*-0.564563);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1e08() {
   return (neuron0x98c2778()*-0.0518981);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1e30() {
   return (neuron0x98c2af8()*-0.792258);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1e58() {
   return (neuron0x98c2e78()*-0.888683);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1e80() {
   return (neuron0x98c31f8()*0.065968);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1ea8() {
   return (neuron0x98c3578()*-0.576719);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1ed0() {
   return (neuron0x98c38f8()*0.249523);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1ef8() {
   return (neuron0x98c3c78()*0.0692907);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1f20() {
   return (neuron0x98c3ff8()*-0.978127);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1f48() {
   return (neuron0x98c4378()*-0.349106);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1f70() {
   return (neuron0x98c46f8()*-0.673332);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1f98() {
   return (neuron0x98c4a78()*0.285491);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1fc0() {
   return (neuron0x98c4df8()*-0.0307853);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1fe8() {
   return (neuron0x98c5178()*-0.423506);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2010() {
   return (neuron0x98c54f8()*0.397144);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2038() {
   return (neuron0x98c5878()*1.01414);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2060() {
   return (neuron0x98c5bf8()*1.6551);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2088() {
   return (neuron0x98c5f78()*-0.763411);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d20b0() {
   return (neuron0x98c62f8()*0.99409);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d20d8() {
   return (neuron0x98c6678()*0.0450693);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2100() {
   return (neuron0x98c6b10()*0.317446);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2128() {
   return (neuron0x98c0240()*-0.306482);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2150() {
   return (neuron0x98c05c0()*-0.0741549);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2178() {
   return (neuron0x98c7e80()*-0.622693);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d21a0() {
   return (neuron0x98c8110()*-0.428972);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d21c8() {
   return (neuron0x98c8478()*0.0850601);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d21f0() {
   return (neuron0x98c87f8()*-0.33721);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2218() {
   return (neuron0x98c8b78()*0.232865);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98b87a8() {
   return (neuron0x98c1760()*1.20387);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1cb0() {
   return (neuron0x98c9b98()*-0.296878);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1cd8() {
   return (neuron0x98c9f00()*-0.383835);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1d00() {
   return (neuron0x98ca280()*-0.612616);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1d28() {
   return (neuron0x98ca600()*-0.477685);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1d50() {
   return (neuron0x98ca980()*0.571276);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d1d78() {
   return (neuron0x98cad00()*-0.529574);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2448() {
   return (neuron0x98cb080()*0.057362);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2470() {
   return (neuron0x98cb400()*0.711158);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2498() {
   return (neuron0x98cb780()*-0.582884);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d24c0() {
   return (neuron0x98cbb00()*0.934992);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d24e8() {
   return (neuron0x98cbe80()*-0.016649);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2510() {
   return (neuron0x98cc200()*1.20763);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2538() {
   return (neuron0x98cc580()*-0.499901);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2560() {
   return (neuron0x98cc900()*0.0450297);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2588() {
   return (neuron0x98ccc80()*-0.470398);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d25b0() {
   return (neuron0x98cd000()*0.132049);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d25d8() {
   return (neuron0x98cd380()*-0.447595);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2600() {
   return (neuron0x98cd700()*0.862031);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2628() {
   return (neuron0x98cda80()*0.42698);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2650() {
   return (neuron0x98cde00()*0.616284);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2678() {
   return (neuron0x98ce180()*-1.29671);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d26a0() {
   return (neuron0x98ce500()*1.32302);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d26c8() {
   return (neuron0x98ce880()*0.237329);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d26f0() {
   return (neuron0x98cec00()*1.06278);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2718() {
   return (neuron0x98cef80()*0.345222);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2740() {
   return (neuron0x98cf300()*-0.105468);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2768() {
   return (neuron0x98cf680()*-0.768055);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2790() {
   return (neuron0x98cfa00()*1.00857);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d27b8() {
   return (neuron0x98cfd80()*-0.0560782);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d27e0() {
   return (neuron0x98d0100()*0.155746);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2808() {
   return (neuron0x98d0480()*0.237947);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2830() {
   return (neuron0x98d0800()*0.344528);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2858() {
   return (neuron0x98d0b80()*-0.470037);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d2880() {
   return (neuron0x98d0f00()*2.3831);
}

double NNParaGammaXYCorrectionClusterDeltaY::synapse0x98d28a8() {
   return (neuron0x98d1280()*0.0287458);
}

