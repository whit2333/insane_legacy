#include "NNPerpPositronXYCorrectionClusterDeltaX.h"
#include <cmath>

double NNPerpPositronXYCorrectionClusterDeltaX::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8) {
   input0 = (in0 - 1674.66)/716.24;
   input1 = (in1 - -4.27309)/34.3691;
   input2 = (in2 - 9.67386)/60.444;
   input3 = (in3 - 1.3363)/0.381166;
   input4 = (in4 - 1.46325)/0.384923;
   input5 = (in5 - 0.114001)/10.7812;
   input6 = (in6 - 0.235447)/11.8782;
   input7 = (in7 - -2.34564)/849.298;
   input8 = (in8 - -0.871867)/680.621;
   switch(index) {
     case 0:
         return neuron0x9021ff0();
     default:
         return 0.;
   }
}

double NNPerpPositronXYCorrectionClusterDeltaX::Value(int index, double* input) {
   input0 = (input[0] - 1674.66)/716.24;
   input1 = (input[1] - -4.27309)/34.3691;
   input2 = (input[2] - 9.67386)/60.444;
   input3 = (input[3] - 1.3363)/0.381166;
   input4 = (input[4] - 1.46325)/0.384923;
   input5 = (input[5] - 0.114001)/10.7812;
   input6 = (input[6] - 0.235447)/11.8782;
   input7 = (input[7] - -2.34564)/849.298;
   input8 = (input[8] - -0.871867)/680.621;
   switch(index) {
     case 0:
         return neuron0x9021ff0();
     default:
         return 0.;
   }
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9001008() {
   return input0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9001178() {
   return input1;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9021178() {
   return input2;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9021378() {
   return input3;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9021578() {
   return input4;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9021790() {
   return input5;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x90219a8() {
   return input6;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9021bc0() {
   return input7;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9021dd8() {
   return input8;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9022118() {
   double input = 0.460129;
   input += synapse0x89b1ec8();
   input += synapse0x90222d0();
   input += synapse0x90222f8();
   input += synapse0x9022320();
   input += synapse0x9022348();
   input += synapse0x9022370();
   input += synapse0x9022398();
   input += synapse0x90223c0();
   input += synapse0x90223e8();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9022118() {
   double input = input0x9022118();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9022410() {
   double input = -3.15194;
   input += synapse0x9022610();
   input += synapse0x9022638();
   input += synapse0x9022660();
   input += synapse0x9022688();
   input += synapse0x90226b0();
   input += synapse0x90226d8();
   input += synapse0x9022700();
   input += synapse0x9022728();
   input += synapse0x90227d8();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9022410() {
   double input = input0x9022410();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9022800() {
   double input = -0.151628;
   input += synapse0x90229b8();
   input += synapse0x90229e0();
   input += synapse0x9022a08();
   input += synapse0x9022a30();
   input += synapse0x9022a58();
   input += synapse0x9022a80();
   input += synapse0x9022aa8();
   input += synapse0x9022ad0();
   input += synapse0x9022af8();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9022800() {
   double input = input0x9022800();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9022b20() {
   double input = -2.99782;
   input += synapse0x9022d20();
   input += synapse0x9022d48();
   input += synapse0x9022d70();
   input += synapse0x9022d98();
   input += synapse0x9022dc0();
   input += synapse0x9022de8();
   input += synapse0x9022750();
   input += synapse0x9022778();
   input += synapse0x90227a0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9022b20() {
   double input = input0x9022b20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9022f18() {
   double input = -4.15826;
   input += synapse0x9023118();
   input += synapse0x9023140();
   input += synapse0x9023168();
   input += synapse0x9023190();
   input += synapse0x90231b8();
   input += synapse0x90231e0();
   input += synapse0x9023208();
   input += synapse0x9023230();
   input += synapse0x9023258();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9022f18() {
   double input = input0x9022f18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9023280() {
   double input = 0.332206;
   input += synapse0x9023480();
   input += synapse0x90234a8();
   input += synapse0x90234d0();
   input += synapse0x90234f8();
   input += synapse0x9023520();
   input += synapse0x9023548();
   input += synapse0x9023570();
   input += synapse0x9023598();
   input += synapse0x90235c0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9023280() {
   double input = input0x9023280();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x90235e8() {
   double input = -0.76495;
   input += synapse0x90237e8();
   input += synapse0x9023810();
   input += synapse0x9023838();
   input += synapse0x9023860();
   input += synapse0x9023888();
   input += synapse0x90238b0();
   input += synapse0x90238d8();
   input += synapse0x9023900();
   input += synapse0x9023928();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x90235e8() {
   double input = input0x90235e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9023950() {
   double input = 0.81586;
   input += synapse0x9023bd8();
   input += synapse0x9023c00();
   input += synapse0x89b1d28();
   input += synapse0x8da7c90();
   input += synapse0x8e0a100();
   input += synapse0x9001378();
   input += synapse0x90013a0();
   input += synapse0x90013c8();
   input += synapse0x90013f0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9023950() {
   double input = input0x9023950();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9023e30() {
   double input = -3.41457;
   input += synapse0x9024030();
   input += synapse0x9024058();
   input += synapse0x9024080();
   input += synapse0x90240a8();
   input += synapse0x90240d0();
   input += synapse0x90240f8();
   input += synapse0x9024120();
   input += synapse0x9024148();
   input += synapse0x9024170();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9023e30() {
   double input = input0x9023e30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9024198() {
   double input = -0.851684;
   input += synapse0x90243b0();
   input += synapse0x90243d8();
   input += synapse0x9024400();
   input += synapse0x9024428();
   input += synapse0x9024450();
   input += synapse0x9024478();
   input += synapse0x90244a0();
   input += synapse0x90244c8();
   input += synapse0x90244f0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9024198() {
   double input = input0x9024198();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9024518() {
   double input = -0.48054;
   input += synapse0x9024730();
   input += synapse0x9024758();
   input += synapse0x9024780();
   input += synapse0x90247a8();
   input += synapse0x90247d0();
   input += synapse0x90247f8();
   input += synapse0x9024820();
   input += synapse0x9024848();
   input += synapse0x9024870();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9024518() {
   double input = input0x9024518();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9024898() {
   double input = 2.70247;
   input += synapse0x9024ab0();
   input += synapse0x9024ad8();
   input += synapse0x9024b00();
   input += synapse0x9024b28();
   input += synapse0x9024b50();
   input += synapse0x9024b78();
   input += synapse0x9024ba0();
   input += synapse0x9024bc8();
   input += synapse0x9024bf0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9024898() {
   double input = input0x9024898();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9024c18() {
   double input = 0.735795;
   input += synapse0x9024e30();
   input += synapse0x9024e58();
   input += synapse0x9024e80();
   input += synapse0x9024ea8();
   input += synapse0x9024ed0();
   input += synapse0x9024ef8();
   input += synapse0x9024f20();
   input += synapse0x9024f48();
   input += synapse0x9024f70();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9024c18() {
   double input = input0x9024c18();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9024f98() {
   double input = 2.32524;
   input += synapse0x90251b0();
   input += synapse0x90251d8();
   input += synapse0x9025200();
   input += synapse0x9025228();
   input += synapse0x9025250();
   input += synapse0x9025278();
   input += synapse0x90252a0();
   input += synapse0x90252c8();
   input += synapse0x90252f0();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9024f98() {
   double input = input0x9024f98();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9025318() {
   double input = -0.527272;
   input += synapse0x9025530();
   input += synapse0x9025558();
   input += synapse0x9025580();
   input += synapse0x9001418();
   input += synapse0x9001440();
   input += synapse0x9022e10();
   input += synapse0x9022e38();
   input += synapse0x9022e60();
   input += synapse0x9022e88();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9025318() {
   double input = input0x9025318();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9023c28() {
   double input = -0.653218;
   input += synapse0x9023df8();
   input += synapse0x9000f30();
   input += synapse0x9025a38();
   input += synapse0x9025ae8();
   input += synapse0x9025b98();
   input += synapse0x9025c48();
   input += synapse0x9025cf8();
   input += synapse0x9025da8();
   input += synapse0x9025e58();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9023c28() {
   double input = input0x9023c28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9025f08() {
   double input = 3.83943;
   input += synapse0x9026048();
   input += synapse0x9026070();
   input += synapse0x9026098();
   input += synapse0x90260c0();
   input += synapse0x90260e8();
   input += synapse0x9026110();
   input += synapse0x9026138();
   input += synapse0x9026160();
   input += synapse0x9026188();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9025f08() {
   double input = input0x9025f08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x90261b0() {
   double input = -0.105002;
   input += synapse0x90262f0();
   input += synapse0x9026318();
   input += synapse0x9026340();
   input += synapse0x9026368();
   input += synapse0x9026390();
   input += synapse0x90263b8();
   input += synapse0x90263e0();
   input += synapse0x9026408();
   input += synapse0x9026430();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x90261b0() {
   double input = input0x90261b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9026458() {
   double input = -0.353317;
   input += synapse0x9026598();
   input += synapse0x90265c0();
   input += synapse0x90265e8();
   input += synapse0x9026610();
   input += synapse0x9026638();
   input += synapse0x9026660();
   input += synapse0x9026688();
   input += synapse0x90266b0();
   input += synapse0x90266d8();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9026458() {
   double input = input0x9026458();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9026700() {
   double input = -0.249282;
   input += synapse0x9026918();
   input += synapse0x9026940();
   input += synapse0x9026968();
   input += synapse0x9026990();
   input += synapse0x90269b8();
   input += synapse0x90269e0();
   input += synapse0x9026a08();
   input += synapse0x9026a30();
   input += synapse0x9026a58();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9026700() {
   double input = input0x9026700();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::input0x9021ff0() {
   double input = -1.3262;
   input += synapse0x9026b58();
   input += synapse0x9026b80();
   input += synapse0x9026ba8();
   input += synapse0x9026bd0();
   input += synapse0x9026bf8();
   input += synapse0x9026c20();
   input += synapse0x9026c48();
   input += synapse0x9026c70();
   input += synapse0x9026c98();
   input += synapse0x9026cc0();
   input += synapse0x9026ce8();
   input += synapse0x9026d10();
   input += synapse0x9026d38();
   input += synapse0x9026d60();
   input += synapse0x9026d88();
   input += synapse0x9026db0();
   input += synapse0x9026e60();
   input += synapse0x9026e88();
   input += synapse0x9026eb0();
   input += synapse0x9026ed8();
   return input;
}

double NNPerpPositronXYCorrectionClusterDeltaX::neuron0x9021ff0() {
   double input = input0x9021ff0();
   return (input * 1)+0;
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x89b1ec8() {
   return (neuron0x9001008()*-0.354859);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90222d0() {
   return (neuron0x9001178()*0.258647);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90222f8() {
   return (neuron0x9021178()*0.0056141);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022320() {
   return (neuron0x9021378()*-0.558575);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022348() {
   return (neuron0x9021578()*-0.0267768);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022370() {
   return (neuron0x9021790()*3.02018);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022398() {
   return (neuron0x90219a8()*-0.751999);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90223c0() {
   return (neuron0x9021bc0()*1.53674);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90223e8() {
   return (neuron0x9021dd8()*0.100376);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022610() {
   return (neuron0x9001008()*0.242031);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022638() {
   return (neuron0x9001178()*-0.306053);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022660() {
   return (neuron0x9021178()*0.0676502);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022688() {
   return (neuron0x9021378()*1.98913);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90226b0() {
   return (neuron0x9021578()*-0.0323218);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90226d8() {
   return (neuron0x9021790()*-5.67679);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022700() {
   return (neuron0x90219a8()*-0.69572);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022728() {
   return (neuron0x9021bc0()*0.621063);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90227d8() {
   return (neuron0x9021dd8()*-0.0456561);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90229b8() {
   return (neuron0x9001008()*0.849792);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90229e0() {
   return (neuron0x9001178()*-1.018);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022a08() {
   return (neuron0x9021178()*-0.674904);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022a30() {
   return (neuron0x9021378()*-1.16475);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022a58() {
   return (neuron0x9021578()*0.0479702);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022a80() {
   return (neuron0x9021790()*2.8256);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022aa8() {
   return (neuron0x90219a8()*1.92077);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022ad0() {
   return (neuron0x9021bc0()*-1.55404);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022af8() {
   return (neuron0x9021dd8()*0.0234125);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022d20() {
   return (neuron0x9001008()*-2.1837);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022d48() {
   return (neuron0x9001178()*1.01706);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022d70() {
   return (neuron0x9021178()*-0.151492);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022d98() {
   return (neuron0x9021378()*-0.542357);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022dc0() {
   return (neuron0x9021578()*0.575672);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022de8() {
   return (neuron0x9021790()*-0.541597);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022750() {
   return (neuron0x90219a8()*3.32838);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022778() {
   return (neuron0x9021bc0()*1.46292);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90227a0() {
   return (neuron0x9021dd8()*-0.0641874);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023118() {
   return (neuron0x9001008()*0.432585);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023140() {
   return (neuron0x9001178()*0.501461);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023168() {
   return (neuron0x9021178()*-0.0565463);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023190() {
   return (neuron0x9021378()*2.59118);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90231b8() {
   return (neuron0x9021578()*0.0942913);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90231e0() {
   return (neuron0x9021790()*5.54787);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023208() {
   return (neuron0x90219a8()*0.0830788);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023230() {
   return (neuron0x9021bc0()*0.423691);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023258() {
   return (neuron0x9021dd8()*-0.139042);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023480() {
   return (neuron0x9001008()*-0.810045);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90234a8() {
   return (neuron0x9001178()*0.19748);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90234d0() {
   return (neuron0x9021178()*-0.0923827);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90234f8() {
   return (neuron0x9021378()*-2.39549);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023520() {
   return (neuron0x9021578()*0.0321682);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023548() {
   return (neuron0x9021790()*4.74264);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023570() {
   return (neuron0x90219a8()*0.728819);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023598() {
   return (neuron0x9021bc0()*0.430468);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90235c0() {
   return (neuron0x9021dd8()*0.0939817);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90237e8() {
   return (neuron0x9001008()*1.12557);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023810() {
   return (neuron0x9001178()*0.231402);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023838() {
   return (neuron0x9021178()*0.483359);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023860() {
   return (neuron0x9021378()*3.56784);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023888() {
   return (neuron0x9021578()*-0.0325622);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90238b0() {
   return (neuron0x9021790()*3.36971);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90238d8() {
   return (neuron0x90219a8()*-1.8257);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023900() {
   return (neuron0x9021bc0()*0.202808);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023928() {
   return (neuron0x9021dd8()*-0.570557);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023bd8() {
   return (neuron0x9001008()*0.403327);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023c00() {
   return (neuron0x9001178()*0.109722);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x89b1d28() {
   return (neuron0x9021178()*1.71351);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x8da7c90() {
   return (neuron0x9021378()*0.60921);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x8e0a100() {
   return (neuron0x9021578()*0.310957);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9001378() {
   return (neuron0x9021790()*-0.0931179);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90013a0() {
   return (neuron0x90219a8()*0.970382);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90013c8() {
   return (neuron0x9021bc0()*1.04691);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90013f0() {
   return (neuron0x9021dd8()*0.0467082);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024030() {
   return (neuron0x9001008()*-1.75554);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024058() {
   return (neuron0x9001178()*1.16423);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024080() {
   return (neuron0x9021178()*-0.0326709);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90240a8() {
   return (neuron0x9021378()*-0.46193);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90240d0() {
   return (neuron0x9021578()*0.931099);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90240f8() {
   return (neuron0x9021790()*0.115556);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024120() {
   return (neuron0x90219a8()*5.01515);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024148() {
   return (neuron0x9021bc0()*0.357814);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024170() {
   return (neuron0x9021dd8()*-0.203449);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90243b0() {
   return (neuron0x9001008()*0.428884);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90243d8() {
   return (neuron0x9001178()*0.932916);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024400() {
   return (neuron0x9021178()*0.0596368);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024428() {
   return (neuron0x9021378()*-0.629816);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024450() {
   return (neuron0x9021578()*0.521645);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024478() {
   return (neuron0x9021790()*-3.14263);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90244a0() {
   return (neuron0x90219a8()*0.910284);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90244c8() {
   return (neuron0x9021bc0()*0.217492);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90244f0() {
   return (neuron0x9021dd8()*0.349636);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024730() {
   return (neuron0x9001008()*-1.13948);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024758() {
   return (neuron0x9001178()*4.16096);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024780() {
   return (neuron0x9021178()*0.736821);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90247a8() {
   return (neuron0x9021378()*-2.75738);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90247d0() {
   return (neuron0x9021578()*0.860233);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90247f8() {
   return (neuron0x9021790()*-3.24944);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024820() {
   return (neuron0x90219a8()*-0.611278);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024848() {
   return (neuron0x9021bc0()*0.220368);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024870() {
   return (neuron0x9021dd8()*-0.134631);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024ab0() {
   return (neuron0x9001008()*1.2566);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024ad8() {
   return (neuron0x9001178()*-0.495697);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024b00() {
   return (neuron0x9021178()*-1.83151);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024b28() {
   return (neuron0x9021378()*0.0727493);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024b50() {
   return (neuron0x9021578()*-0.0947196);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024b78() {
   return (neuron0x9021790()*-0.0421139);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024ba0() {
   return (neuron0x90219a8()*-1.47362);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024bc8() {
   return (neuron0x9021bc0()*-0.0697993);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024bf0() {
   return (neuron0x9021dd8()*-0.609063);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024e30() {
   return (neuron0x9001008()*-0.228941);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024e58() {
   return (neuron0x9001178()*-0.729368);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024e80() {
   return (neuron0x9021178()*0.0408199);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024ea8() {
   return (neuron0x9021378()*-0.446197);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024ed0() {
   return (neuron0x9021578()*-0.0768475);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024ef8() {
   return (neuron0x9021790()*4.0384);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024f20() {
   return (neuron0x90219a8()*-2.05308);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024f48() {
   return (neuron0x9021bc0()*-1.6668);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9024f70() {
   return (neuron0x9021dd8()*-0.581296);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90251b0() {
   return (neuron0x9001008()*2.47722);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90251d8() {
   return (neuron0x9001178()*-0.698872);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025200() {
   return (neuron0x9021178()*-0.377737);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025228() {
   return (neuron0x9021378()*0.451692);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025250() {
   return (neuron0x9021578()*-0.0654426);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025278() {
   return (neuron0x9021790()*0.336738);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90252a0() {
   return (neuron0x90219a8()*-1.97361);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90252c8() {
   return (neuron0x9021bc0()*-0.294757);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90252f0() {
   return (neuron0x9021dd8()*0.641259);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025530() {
   return (neuron0x9001008()*-0.0232119);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025558() {
   return (neuron0x9001178()*0.0167542);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025580() {
   return (neuron0x9021178()*8.99589);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9001418() {
   return (neuron0x9021378()*-0.120005);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9001440() {
   return (neuron0x9021578()*-0.229293);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022e10() {
   return (neuron0x9021790()*-0.275688);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022e38() {
   return (neuron0x90219a8()*1.57431);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022e60() {
   return (neuron0x9021bc0()*-0.0836088);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9022e88() {
   return (neuron0x9021dd8()*-0.282481);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9023df8() {
   return (neuron0x9001008()*-0.591798);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9000f30() {
   return (neuron0x9001178()*3.37324);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025a38() {
   return (neuron0x9021178()*0.593326);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025ae8() {
   return (neuron0x9021378()*-2.19253);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025b98() {
   return (neuron0x9021578()*0.86092);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025c48() {
   return (neuron0x9021790()*-3.66048);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025cf8() {
   return (neuron0x90219a8()*-0.651312);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025da8() {
   return (neuron0x9021bc0()*-0.611011);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9025e58() {
   return (neuron0x9021dd8()*0.371595);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026048() {
   return (neuron0x9001008()*0.713189);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026070() {
   return (neuron0x9001178()*-0.45934);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026098() {
   return (neuron0x9021178()*-3.14236);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90260c0() {
   return (neuron0x9021378()*-0.253832);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90260e8() {
   return (neuron0x9021578()*-0.21239);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026110() {
   return (neuron0x9021790()*0.148286);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026138() {
   return (neuron0x90219a8()*-2.05824);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026160() {
   return (neuron0x9021bc0()*-1.23943);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026188() {
   return (neuron0x9021dd8()*0.0929492);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90262f0() {
   return (neuron0x9001008()*0.243099);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026318() {
   return (neuron0x9001178()*-0.0726667);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026340() {
   return (neuron0x9021178()*-0.776753);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026368() {
   return (neuron0x9021378()*-0.299292);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026390() {
   return (neuron0x9021578()*0.100742);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90263b8() {
   return (neuron0x9021790()*1.41685);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90263e0() {
   return (neuron0x90219a8()*0.761718);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026408() {
   return (neuron0x9021bc0()*0.197013);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026430() {
   return (neuron0x9021dd8()*-1.51458);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026598() {
   return (neuron0x9001008()*-0.155902);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90265c0() {
   return (neuron0x9001178()*-0.541705);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90265e8() {
   return (neuron0x9021178()*0.0184312);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026610() {
   return (neuron0x9021378()*0.893751);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026638() {
   return (neuron0x9021578()*-0.0288601);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026660() {
   return (neuron0x9021790()*7.97697);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026688() {
   return (neuron0x90219a8()*1.46995);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90266b0() {
   return (neuron0x9021bc0()*0.40313);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90266d8() {
   return (neuron0x9021dd8()*0.243354);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026918() {
   return (neuron0x9001008()*-0.782684);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026940() {
   return (neuron0x9001178()*1.33309);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026968() {
   return (neuron0x9021178()*0.737749);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026990() {
   return (neuron0x9021378()*1.73482);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90269b8() {
   return (neuron0x9021578()*0.164066);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x90269e0() {
   return (neuron0x9021790()*-3.30813);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026a08() {
   return (neuron0x90219a8()*-0.861658);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026a30() {
   return (neuron0x9021bc0()*-1.33129);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026a58() {
   return (neuron0x9021dd8()*0.0657782);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026b58() {
   return (neuron0x9022118()*-5.15687);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026b80() {
   return (neuron0x9022410()*-3.24003);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026ba8() {
   return (neuron0x9022800()*3.05976);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026bd0() {
   return (neuron0x9022b20()*4.98604);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026bf8() {
   return (neuron0x9022f18()*1.84185);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026c20() {
   return (neuron0x9023280()*1.26039);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026c48() {
   return (neuron0x90235e8()*0.377945);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026c70() {
   return (neuron0x9023950()*-1.9421);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026c98() {
   return (neuron0x9023e30()*-3.56111);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026cc0() {
   return (neuron0x9024198()*3.11411);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026ce8() {
   return (neuron0x9024518()*2.77783);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026d10() {
   return (neuron0x9024898()*-4.25632);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026d38() {
   return (neuron0x9024c18()*4.43969);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026d60() {
   return (neuron0x9024f98()*2.97369);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026d88() {
   return (neuron0x9025318()*-1.04912);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026db0() {
   return (neuron0x9023c28()*-3.72249);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026e60() {
   return (neuron0x9025f08()*2.9402);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026e88() {
   return (neuron0x90261b0()*-5.62756);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026eb0() {
   return (neuron0x9026458()*2.68147);
}

double NNPerpPositronXYCorrectionClusterDeltaX::synapse0x9026ed8() {
   return (neuron0x9026700()*1.80644);
}

