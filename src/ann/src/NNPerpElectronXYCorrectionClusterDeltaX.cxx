#include "NNPerpElectronXYCorrectionClusterDeltaX.h"
#include <cmath>

double NNPerpElectronXYCorrectionClusterDeltaX::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10) {
   input0 = (in0 - 1981.96)/1031.12;
   input1 = (in1 - -4.35365)/33.4012;
   input2 = (in2 - -6.95005)/60.2436;
   input3 = (in3 - 1.30476)/0.378059;
   input4 = (in4 - 1.40003)/0.392909;
   input5 = (in5 - 0.291103)/1.94696;
   input6 = (in6 - 0.288522)/1.77385;
   input7 = (in7 - 6.61681)/9.86309;
   input8 = (in8 - 5.27731)/9.73683;
   input9 = (in9 - -0.00558485)/0.751931;
   input10 = (in10 - 0.0179736)/0.743436;
   switch(index) {
     case 0:
         return neuron0x8d2b058();
     default:
         return 0.;
   }
}

double NNPerpElectronXYCorrectionClusterDeltaX::Value(int index, double* input) {
   input0 = (input[0] - 1981.96)/1031.12;
   input1 = (input[1] - -4.35365)/33.4012;
   input2 = (input[2] - -6.95005)/60.2436;
   input3 = (input[3] - 1.30476)/0.378059;
   input4 = (input[4] - 1.40003)/0.392909;
   input5 = (input[5] - 0.291103)/1.94696;
   input6 = (input[6] - 0.288522)/1.77385;
   input7 = (input[7] - 6.61681)/9.86309;
   input8 = (input[8] - 5.27731)/9.73683;
   input9 = (input[9] - -0.00558485)/0.751931;
   input10 = (input[10] - 0.0179736)/0.743436;
   switch(index) {
     case 0:
         return neuron0x8d2b058();
     default:
         return 0.;
   }
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8cedef8() {
   return input0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8cee0b0() {
   return input1;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8cee2b0() {
   return input2;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8cee4b0() {
   return input3;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0e3d8() {
   return input4;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0e5d8() {
   return input5;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0e7f0() {
   return input6;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0ea08() {
   return input7;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0ec20() {
   return input8;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0ee38() {
   return input9;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0f038() {
   return input10;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d0f378() {
   double input = 0.26582;
   input += synapse0x8c14588();
   input += synapse0x8d0f530();
   input += synapse0x8d0f558();
   input += synapse0x8d0f580();
   input += synapse0x8d0f5a8();
   input += synapse0x8d0f5d0();
   input += synapse0x8d0f5f8();
   input += synapse0x8d0f620();
   input += synapse0x8d0f648();
   input += synapse0x8d0f670();
   input += synapse0x8d0f698();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0f378() {
   double input = input0x8d0f378();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d0f6c0() {
   double input = 0.429135;
   input += synapse0x8d0f8c0();
   input += synapse0x8d0f8e8();
   input += synapse0x8d0f910();
   input += synapse0x8d0f938();
   input += synapse0x8d0f960();
   input += synapse0x8d0f988();
   input += synapse0x8d0fa38();
   input += synapse0x8d0fa60();
   input += synapse0x8d0fa88();
   input += synapse0x8d0fab0();
   input += synapse0x8d0fad8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0f6c0() {
   double input = input0x8d0f6c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d0fb00() {
   double input = 0.813226;
   input += synapse0x8d0fcb8();
   input += synapse0x8d0fce0();
   input += synapse0x8d0fd08();
   input += synapse0x8d0fd30();
   input += synapse0x8d0fd58();
   input += synapse0x8d0fd80();
   input += synapse0x8d0fda8();
   input += synapse0x8d0fdd0();
   input += synapse0x8d0fdf8();
   input += synapse0x8d0fe20();
   input += synapse0x8d0fe48();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0fb00() {
   double input = input0x8d0fb00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d0ff78() {
   double input = -0.498032;
   input += synapse0x8d10178();
   input += synapse0x8d101a0();
   input += synapse0x8d101c8();
   input += synapse0x8d101f0();
   input += synapse0x8d10218();
   input += synapse0x8d10240();
   input += synapse0x8d10268();
   input += synapse0x8d10290();
   input += synapse0x8d102b8();
   input += synapse0x8d102e0();
   input += synapse0x8d10308();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d0ff78() {
   double input = input0x8d0ff78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d10330() {
   double input = -0.306309;
   input += synapse0x8d10530();
   input += synapse0x8d10558();
   input += synapse0x8d10580();
   input += synapse0x8d105a8();
   input += synapse0x8d105d0();
   input += synapse0x8d105f8();
   input += synapse0x8d10620();
   input += synapse0x8d10648();
   input += synapse0x8d10670();
   input += synapse0x8d10698();
   input += synapse0x8d106c0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d10330() {
   double input = input0x8d10330();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d106e8() {
   double input = -0.606879;
   input += synapse0x8d108e8();
   input += synapse0x8d10910();
   input += synapse0x8d10938();
   input += synapse0x8d10960();
   input += synapse0x8d10988();
   input += synapse0x8d109b0();
   input += synapse0x8d109d8();
   input += synapse0x8d10a00();
   input += synapse0x8d10a28();
   input += synapse0x8d10a50();
   input += synapse0x8c144d0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d106e8() {
   double input = input0x8d106e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d10c80() {
   double input = -0.398606;
   input += synapse0x8d10e50();
   input += synapse0x8d10e78();
   input += synapse0x8d10ea0();
   input += synapse0x8d10ec8();
   input += synapse0x8d10ef0();
   input += synapse0x8d10f18();
   input += synapse0x8d10f40();
   input += synapse0x8d10f68();
   input += synapse0x8d10f90();
   input += synapse0x8d10fb8();
   input += synapse0x8d10fe0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d10c80() {
   double input = input0x8d10c80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d11008() {
   double input = 0.153187;
   input += synapse0x8d11220();
   input += synapse0x8d11248();
   input += synapse0x8d11270();
   input += synapse0x8d11298();
   input += synapse0x8d112c0();
   input += synapse0x8d112e8();
   input += synapse0x8d11310();
   input += synapse0x8d11338();
   input += synapse0x8d11360();
   input += synapse0x8d11388();
   input += synapse0x8d113b0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d11008() {
   double input = input0x8d11008();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d113d8() {
   double input = 0.655983;
   input += synapse0x8d115f0();
   input += synapse0x8d11618();
   input += synapse0x8d11640();
   input += synapse0x8d11668();
   input += synapse0x8d11690();
   input += synapse0x8d116b8();
   input += synapse0x8d116e0();
   input += synapse0x8d11708();
   input += synapse0x8d11730();
   input += synapse0x8d11758();
   input += synapse0x8d11780();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d113d8() {
   double input = input0x8d113d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d117a8() {
   double input = 0.352079;
   input += synapse0x8d119c0();
   input += synapse0x8d119e8();
   input += synapse0x8d11a10();
   input += synapse0x8d11a38();
   input += synapse0x8d11a60();
   input += synapse0x8d11a88();
   input += synapse0x8d11ab0();
   input += synapse0x8d11ad8();
   input += synapse0x8d11b00();
   input += synapse0x8d11b28();
   input += synapse0x8d11b50();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d117a8() {
   double input = input0x8d117a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d11b78() {
   double input = -0.174731;
   input += synapse0x8d11d90();
   input += synapse0x8d11db8();
   input += synapse0x8d11de0();
   input += synapse0x8d11e08();
   input += synapse0x8d11e30();
   input += synapse0x8d11e58();
   input += synapse0x8d11e80();
   input += synapse0x8d11ea8();
   input += synapse0x8d11ed0();
   input += synapse0x8d11ef8();
   input += synapse0x8d11f20();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d11b78() {
   double input = input0x8d11b78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d11f48() {
   double input = 0.0188157;
   input += synapse0x8d12160();
   input += synapse0x8d12188();
   input += synapse0x8d121b0();
   input += synapse0x8d121d8();
   input += synapse0x8d12200();
   input += synapse0x8d12228();
   input += synapse0x8d12250();
   input += synapse0x8d12278();
   input += synapse0x8b1f600();
   input += synapse0x8b1f628();
   input += synapse0x8c7d598();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d11f48() {
   double input = input0x8d11f48();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d10a78() {
   double input = -0.138323;
   input += synapse0x8d10c48();
   input += synapse0x8c14510();
   input += synapse0x8c14538();
   input += synapse0x8c14560();
   input += synapse0x8d0fe70();
   input += synapse0x8d0fe98();
   input += synapse0x8d0fec0();
   input += synapse0x8d0fee8();
   input += synapse0x8d0ff10();
   input += synapse0x8d0ff38();
   input += synapse0x8c7d460();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d10a78() {
   double input = input0x8d10a78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d126a8() {
   double input = 0.922894;
   input += synapse0x8d128a8();
   input += synapse0x8d128d0();
   input += synapse0x8d128f8();
   input += synapse0x8d12920();
   input += synapse0x8d12948();
   input += synapse0x8d12970();
   input += synapse0x8d12998();
   input += synapse0x8d129c0();
   input += synapse0x8d129e8();
   input += synapse0x8d12a10();
   input += synapse0x8d12a38();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d126a8() {
   double input = input0x8d126a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d12a60() {
   double input = 0.0093433;
   input += synapse0x8d12c78();
   input += synapse0x8d12ca0();
   input += synapse0x8d12cc8();
   input += synapse0x8d12cf0();
   input += synapse0x8d12d18();
   input += synapse0x8d12d40();
   input += synapse0x8d12d68();
   input += synapse0x8d12d90();
   input += synapse0x8d12db8();
   input += synapse0x8d12de0();
   input += synapse0x8d12e08();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d12a60() {
   double input = input0x8d12a60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d12e30() {
   double input = 0.430821;
   input += synapse0x8d13048();
   input += synapse0x8d130f8();
   input += synapse0x8d131a8();
   input += synapse0x8d13258();
   input += synapse0x8d13308();
   input += synapse0x8d133b8();
   input += synapse0x8d13468();
   input += synapse0x8d13518();
   input += synapse0x8d135c8();
   input += synapse0x8d13678();
   input += synapse0x8d13728();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d12e30() {
   double input = input0x8d12e30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d137d8() {
   double input = 0.0753037;
   input += synapse0x8d13918();
   input += synapse0x8d13940();
   input += synapse0x8d13968();
   input += synapse0x8d13990();
   input += synapse0x8d139b8();
   input += synapse0x8d139e0();
   input += synapse0x8d13a08();
   input += synapse0x8d13a30();
   input += synapse0x8d13a58();
   input += synapse0x8d13a80();
   input += synapse0x8d13aa8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d137d8() {
   double input = input0x8d137d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d13ad0() {
   double input = -0.222892;
   input += synapse0x8d13c10();
   input += synapse0x8d13c38();
   input += synapse0x8d13c60();
   input += synapse0x8d13c88();
   input += synapse0x8d13cb0();
   input += synapse0x8d13cd8();
   input += synapse0x8d13d00();
   input += synapse0x8d13d28();
   input += synapse0x8d13d50();
   input += synapse0x8d13d78();
   input += synapse0x8d13da0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d13ad0() {
   double input = input0x8d13ad0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d13dc8() {
   double input = -0.105065;
   input += synapse0x8d13f08();
   input += synapse0x8d13f30();
   input += synapse0x8d13f58();
   input += synapse0x8d13f80();
   input += synapse0x8d13fa8();
   input += synapse0x8d13fd0();
   input += synapse0x8d13ff8();
   input += synapse0x8d14020();
   input += synapse0x8d14048();
   input += synapse0x8d14070();
   input += synapse0x8d14098();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d13dc8() {
   double input = input0x8d13dc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d15150() {
   double input = 0.308352;
   input += synapse0x8c7d4d0();
   input += synapse0x8c7d4f8();
   input += synapse0x8c7d520();
   input += synapse0x8c7d548();
   input += synapse0x8d140c0();
   input += synapse0x8d140e8();
   input += synapse0x8d14110();
   input += synapse0x8d14138();
   input += synapse0x8d14160();
   input += synapse0x8cc8950();
   input += synapse0x8cc8978();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d15150() {
   double input = input0x8d15150();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d15278() {
   double input = 0.285486;
   input += synapse0x8cc8a30();
   input += synapse0x8d153e8();
   input += synapse0x8d15410();
   input += synapse0x8d15438();
   input += synapse0x8d15460();
   input += synapse0x8d15488();
   input += synapse0x8d154b0();
   input += synapse0x8d154d8();
   input += synapse0x8d15500();
   input += synapse0x8d15528();
   input += synapse0x8d15550();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d15278() {
   double input = input0x8d15278();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d15578() {
   double input = -0.171627;
   input += synapse0x8d0f9b0();
   input += synapse0x8d0f9d8();
   input += synapse0x8d0fa00();
   input += synapse0x8d15880();
   input += synapse0x8d158a8();
   input += synapse0x8d158d0();
   input += synapse0x8d158f8();
   input += synapse0x8d15920();
   input += synapse0x8d15948();
   input += synapse0x8d15970();
   input += synapse0x8d15998();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d15578() {
   double input = input0x8d15578();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d159c0() {
   double input = 0.260538;
   input += synapse0x8d15bc0();
   input += synapse0x8d15be8();
   input += synapse0x8d15c10();
   input += synapse0x8d15c38();
   input += synapse0x8d15c60();
   input += synapse0x8d15c88();
   input += synapse0x8d15cb0();
   input += synapse0x8d15cd8();
   input += synapse0x8d15d00();
   input += synapse0x8d15d28();
   input += synapse0x8d15d50();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d159c0() {
   double input = input0x8d159c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d15d78() {
   double input = -0.300006;
   input += synapse0x8d15f78();
   input += synapse0x8d15fa0();
   input += synapse0x8d15fc8();
   input += synapse0x8d15ff0();
   input += synapse0x8d122a0();
   input += synapse0x8d122c8();
   input += synapse0x8d122f0();
   input += synapse0x8d12318();
   input += synapse0x8d12340();
   input += synapse0x8d12368();
   input += synapse0x8d12390();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d15d78() {
   double input = input0x8d15d78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d123b8() {
   double input = 0.143805;
   input += synapse0x8d125d0();
   input += synapse0x8d125f8();
   input += synapse0x8d12620();
   input += synapse0x8d12648();
   input += synapse0x8d12670();
   input += synapse0x8d16820();
   input += synapse0x8d16848();
   input += synapse0x8d16870();
   input += synapse0x8d16898();
   input += synapse0x8d168c0();
   input += synapse0x8d168e8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d123b8() {
   double input = input0x8d123b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d16910() {
   double input = -0.185538;
   input += synapse0x8d16b10();
   input += synapse0x8d16b38();
   input += synapse0x8d16b60();
   input += synapse0x8d16b88();
   input += synapse0x8d16bb0();
   input += synapse0x8d16bd8();
   input += synapse0x8d16c00();
   input += synapse0x8d16c28();
   input += synapse0x8d16c50();
   input += synapse0x8d16c78();
   input += synapse0x8d16ca0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d16910() {
   double input = input0x8d16910();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d16cc8() {
   double input = 2.28069;
   input += synapse0x8d16ec8();
   input += synapse0x8d16ef0();
   input += synapse0x8d16f18();
   input += synapse0x8d16f40();
   input += synapse0x8d16f68();
   input += synapse0x8d16f90();
   input += synapse0x8d16fb8();
   input += synapse0x8d16fe0();
   input += synapse0x8d17008();
   input += synapse0x8d17030();
   input += synapse0x8d17058();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d16cc8() {
   double input = input0x8d16cc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d17080() {
   double input = -0.836626;
   input += synapse0x8d17280();
   input += synapse0x8d172a8();
   input += synapse0x8d172d0();
   input += synapse0x8d172f8();
   input += synapse0x8d17320();
   input += synapse0x8d17348();
   input += synapse0x8d17370();
   input += synapse0x8d17398();
   input += synapse0x8d173c0();
   input += synapse0x8d173e8();
   input += synapse0x8d17410();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d17080() {
   double input = input0x8d17080();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d17438() {
   double input = 0.27432;
   input += synapse0x8d17638();
   input += synapse0x8d17660();
   input += synapse0x8d17688();
   input += synapse0x8d176b0();
   input += synapse0x8d176d8();
   input += synapse0x8d17700();
   input += synapse0x8d17728();
   input += synapse0x8d17750();
   input += synapse0x8d17778();
   input += synapse0x8d177a0();
   input += synapse0x8d177c8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d17438() {
   double input = input0x8d17438();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d177f0() {
   double input = -0.0564277;
   input += synapse0x8d179f0();
   input += synapse0x8d17a18();
   input += synapse0x8d17a40();
   input += synapse0x8d17a68();
   input += synapse0x8d17a90();
   input += synapse0x8d17ab8();
   input += synapse0x8d17ae0();
   input += synapse0x8d17b08();
   input += synapse0x8d17b30();
   input += synapse0x8d17b58();
   input += synapse0x8d17b80();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d177f0() {
   double input = input0x8d177f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d17ba8() {
   double input = 0.381501;
   input += synapse0x8d17da8();
   input += synapse0x8d17dd0();
   input += synapse0x8d17df8();
   input += synapse0x8d17e20();
   input += synapse0x8d17e48();
   input += synapse0x8d17e70();
   input += synapse0x8d17e98();
   input += synapse0x8d17ec0();
   input += synapse0x8d17ee8();
   input += synapse0x8d17f10();
   input += synapse0x8d17f38();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d17ba8() {
   double input = input0x8d17ba8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d17f60() {
   double input = -0.358077;
   input += synapse0x8d18178();
   input += synapse0x8d13070();
   input += synapse0x8d13098();
   input += synapse0x8d130c0();
   input += synapse0x8d13120();
   input += synapse0x8d13148();
   input += synapse0x8d13170();
   input += synapse0x8d131d0();
   input += synapse0x8d131f8();
   input += synapse0x8d13220();
   input += synapse0x8d13280();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d17f60() {
   double input = input0x8d17f60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d18cf8() {
   double input = 0.288848;
   input += synapse0x8d13428();
   input += synapse0x8d13378();
   input += synapse0x8d13490();
   input += synapse0x8d134b8();
   input += synapse0x8d134e0();
   input += synapse0x8d13540();
   input += synapse0x8d13568();
   input += synapse0x8d13590();
   input += synapse0x8d135f0();
   input += synapse0x8d13618();
   input += synapse0x8d13640();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d18cf8() {
   double input = input0x8d18cf8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d18e20() {
   double input = -0.0310477;
   input += synapse0x8d136e8();
   input += synapse0x8d13798();
   input += synapse0x8d18f90();
   input += synapse0x8d18fb8();
   input += synapse0x8d18fe0();
   input += synapse0x8d19008();
   input += synapse0x8d19030();
   input += synapse0x8d19058();
   input += synapse0x8d19080();
   input += synapse0x8d190a8();
   input += synapse0x8d190d0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d18e20() {
   double input = input0x8d18e20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d190f8() {
   double input = 0.396946;
   input += synapse0x8d192f8();
   input += synapse0x8d19320();
   input += synapse0x8d19348();
   input += synapse0x8d19370();
   input += synapse0x8d19398();
   input += synapse0x8d193c0();
   input += synapse0x8d193e8();
   input += synapse0x8d19410();
   input += synapse0x8d19438();
   input += synapse0x8d19460();
   input += synapse0x8d19488();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d190f8() {
   double input = input0x8d190f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d194b0() {
   double input = 0.576655;
   input += synapse0x8d196b0();
   input += synapse0x8d196d8();
   input += synapse0x8d19700();
   input += synapse0x8d19728();
   input += synapse0x8d19750();
   input += synapse0x8d19778();
   input += synapse0x8d197a0();
   input += synapse0x8d197c8();
   input += synapse0x8d197f0();
   input += synapse0x8d19818();
   input += synapse0x8d19840();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d194b0() {
   double input = input0x8d194b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d19868() {
   double input = -0.48478;
   input += synapse0x8d19a68();
   input += synapse0x8d19a90();
   input += synapse0x8d19ab8();
   input += synapse0x8d19ae0();
   input += synapse0x8d19b08();
   input += synapse0x8d19b30();
   input += synapse0x8d19b58();
   input += synapse0x8d19b80();
   input += synapse0x8d19ba8();
   input += synapse0x8d19bd0();
   input += synapse0x8d19bf8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d19868() {
   double input = input0x8d19868();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d19c20() {
   double input = 1.91912;
   input += synapse0x8d19e38();
   input += synapse0x8d19e60();
   input += synapse0x8d19e88();
   input += synapse0x8d19eb0();
   input += synapse0x8d19ed8();
   input += synapse0x8d19f00();
   input += synapse0x8d19f28();
   input += synapse0x8d19f50();
   input += synapse0x8d19f78();
   input += synapse0x8d19fa0();
   input += synapse0x8d19fc8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d19c20() {
   double input = input0x8d19c20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d19ff0() {
   double input = 0.399105;
   input += synapse0x8d1a208();
   input += synapse0x8d1a230();
   input += synapse0x8d1a258();
   input += synapse0x8d1a280();
   input += synapse0x8d1a2a8();
   input += synapse0x8d1a2d0();
   input += synapse0x8d1a2f8();
   input += synapse0x8d1a320();
   input += synapse0x8d1a348();
   input += synapse0x8d1a370();
   input += synapse0x8d1a398();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d19ff0() {
   double input = input0x8d19ff0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1a3c0() {
   double input = -0.0639917;
   input += synapse0x8d1a5d8();
   input += synapse0x8d1a600();
   input += synapse0x8d1a628();
   input += synapse0x8d1a650();
   input += synapse0x8d1a678();
   input += synapse0x8d1a6a0();
   input += synapse0x8d1a6c8();
   input += synapse0x8d1a6f0();
   input += synapse0x8d1a718();
   input += synapse0x8d1a740();
   input += synapse0x8d1a768();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1a3c0() {
   double input = input0x8d1a3c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1a790() {
   double input = 0.725416;
   input += synapse0x8d1a9a8();
   input += synapse0x8d1a9d0();
   input += synapse0x8d1a9f8();
   input += synapse0x8d1aa20();
   input += synapse0x8d1aa48();
   input += synapse0x8d1aa70();
   input += synapse0x8d1aa98();
   input += synapse0x8d1aac0();
   input += synapse0x8d1aae8();
   input += synapse0x8d1ab10();
   input += synapse0x8d1ab38();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1a790() {
   double input = input0x8d1a790();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1ab60() {
   double input = -2.06875;
   input += synapse0x8d1ad78();
   input += synapse0x8d1ada0();
   input += synapse0x8d1adc8();
   input += synapse0x8d1adf0();
   input += synapse0x8d1ae18();
   input += synapse0x8d1ae40();
   input += synapse0x8d1ae68();
   input += synapse0x8d1ae90();
   input += synapse0x8d1aeb8();
   input += synapse0x8d1aee0();
   input += synapse0x8d1af08();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1ab60() {
   double input = input0x8d1ab60();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1af30() {
   double input = -0.329448;
   input += synapse0x8d1b148();
   input += synapse0x8d1b170();
   input += synapse0x8d1b198();
   input += synapse0x8d1b1c0();
   input += synapse0x8d1b1e8();
   input += synapse0x8d1b210();
   input += synapse0x8d1b238();
   input += synapse0x8d1b260();
   input += synapse0x8d1b288();
   input += synapse0x8d1b2b0();
   input += synapse0x8d1b2d8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1af30() {
   double input = input0x8d1af30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1b300() {
   double input = 0.298831;
   input += synapse0x8d1b518();
   input += synapse0x8d1b540();
   input += synapse0x8d1b568();
   input += synapse0x8d1b590();
   input += synapse0x8d1b5b8();
   input += synapse0x8d1b5e0();
   input += synapse0x8d1b608();
   input += synapse0x8d1b630();
   input += synapse0x8d1b658();
   input += synapse0x8d1b680();
   input += synapse0x8d1b6a8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1b300() {
   double input = input0x8d1b300();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1b6d0() {
   double input = 0.222353;
   input += synapse0x8d1b8e8();
   input += synapse0x8d1b910();
   input += synapse0x8d1b938();
   input += synapse0x8d1b960();
   input += synapse0x8d1b988();
   input += synapse0x8d1b9b0();
   input += synapse0x8d1b9d8();
   input += synapse0x8d1ba00();
   input += synapse0x8d1ba28();
   input += synapse0x8d1ba50();
   input += synapse0x8d1ba78();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1b6d0() {
   double input = input0x8d1b6d0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1baa0() {
   double input = 0.494071;
   input += synapse0x8d1bcb8();
   input += synapse0x8d1bce0();
   input += synapse0x8d1bd08();
   input += synapse0x8d1bd30();
   input += synapse0x8d1bd58();
   input += synapse0x8d1bd80();
   input += synapse0x8d1bda8();
   input += synapse0x8d1bdd0();
   input += synapse0x8d1bdf8();
   input += synapse0x8d1be20();
   input += synapse0x8d1be48();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1baa0() {
   double input = input0x8d1baa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1be70() {
   double input = -0.323503;
   input += synapse0x8d1c088();
   input += synapse0x8d1c0b0();
   input += synapse0x8d1c0d8();
   input += synapse0x8d1c100();
   input += synapse0x8d1c128();
   input += synapse0x8d1c150();
   input += synapse0x8d1c178();
   input += synapse0x8d16018();
   input += synapse0x8d16040();
   input += synapse0x8d16068();
   input += synapse0x8d16090();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1be70() {
   double input = input0x8d1be70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d160b8() {
   double input = 0.289618;
   input += synapse0x8d162d0();
   input += synapse0x8d162f8();
   input += synapse0x8d16320();
   input += synapse0x8d16348();
   input += synapse0x8d16370();
   input += synapse0x8d16398();
   input += synapse0x8d163c0();
   input += synapse0x8d163e8();
   input += synapse0x8d16410();
   input += synapse0x8d16438();
   input += synapse0x8d16460();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d160b8() {
   double input = input0x8d160b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d16488() {
   double input = -1.08471;
   input += synapse0x8d166a0();
   input += synapse0x8d166c8();
   input += synapse0x8d166f0();
   input += synapse0x8d16718();
   input += synapse0x8d16740();
   input += synapse0x8d16768();
   input += synapse0x8d16790();
   input += synapse0x8d167b8();
   input += synapse0x8d167e0();
   input += synapse0x8d1d1a8();
   input += synapse0x8d1d1d0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d16488() {
   double input = input0x8d16488();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1d1f8() {
   double input = -0.28198;
   input += synapse0x8d1d3f8();
   input += synapse0x8d1d420();
   input += synapse0x8d1d448();
   input += synapse0x8d1d470();
   input += synapse0x8d1d498();
   input += synapse0x8d1d4c0();
   input += synapse0x8d1d4e8();
   input += synapse0x8d1d510();
   input += synapse0x8d1d538();
   input += synapse0x8d1d560();
   input += synapse0x8d1d588();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1d1f8() {
   double input = input0x8d1d1f8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1d5b0() {
   double input = 0.260838;
   input += synapse0x8d1d7c8();
   input += synapse0x8d1d7f0();
   input += synapse0x8d1d818();
   input += synapse0x8d1d840();
   input += synapse0x8d1d868();
   input += synapse0x8d1d890();
   input += synapse0x8d1d8b8();
   input += synapse0x8d1d8e0();
   input += synapse0x8d1d908();
   input += synapse0x8d1d930();
   input += synapse0x8d1d958();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1d5b0() {
   double input = input0x8d1d5b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1d980() {
   double input = 0.712896;
   input += synapse0x8d1db98();
   input += synapse0x8d1dbc0();
   input += synapse0x8d1dbe8();
   input += synapse0x8d1dc10();
   input += synapse0x8d1dc38();
   input += synapse0x8d1dc60();
   input += synapse0x8d1dc88();
   input += synapse0x8d1dcb0();
   input += synapse0x8d1dcd8();
   input += synapse0x8d1dd00();
   input += synapse0x8d1dd28();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1d980() {
   double input = input0x8d1d980();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1dd50() {
   double input = -0.509633;
   input += synapse0x8d1df68();
   input += synapse0x8d1df90();
   input += synapse0x8d1dfb8();
   input += synapse0x8d1dfe0();
   input += synapse0x8d1e008();
   input += synapse0x8d1e030();
   input += synapse0x8d1e058();
   input += synapse0x8d1e080();
   input += synapse0x8d1e0a8();
   input += synapse0x8d1e0d0();
   input += synapse0x8d1e0f8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1dd50() {
   double input = input0x8d1dd50();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1e120() {
   double input = 0.552305;
   input += synapse0x8d15778();
   input += synapse0x8d157a0();
   input += synapse0x8d157c8();
   input += synapse0x8d157f0();
   input += synapse0x8d15818();
   input += synapse0x8d15840();
   input += synapse0x8d1e540();
   input += synapse0x8d1e568();
   input += synapse0x8d1e590();
   input += synapse0x8d1e5b8();
   input += synapse0x8d1e5e0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1e120() {
   double input = input0x8d1e120();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1e608() {
   double input = -0.622455;
   input += synapse0x8d1e808();
   input += synapse0x8d1e830();
   input += synapse0x8d1e858();
   input += synapse0x8d1e880();
   input += synapse0x8d1e8a8();
   input += synapse0x8d1e8d0();
   input += synapse0x8d1e8f8();
   input += synapse0x8d1e920();
   input += synapse0x8d1e948();
   input += synapse0x8d1e970();
   input += synapse0x8d1e998();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1e608() {
   double input = input0x8d1e608();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1e9c0() {
   double input = -0.864897;
   input += synapse0x8d1ebd8();
   input += synapse0x8d1ec00();
   input += synapse0x8d1ec28();
   input += synapse0x8d1ec50();
   input += synapse0x8d1ec78();
   input += synapse0x8d1eca0();
   input += synapse0x8d1ecc8();
   input += synapse0x8d1ecf0();
   input += synapse0x8d1ed18();
   input += synapse0x8d1ed40();
   input += synapse0x8d1ed68();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1e9c0() {
   double input = input0x8d1e9c0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1ed90() {
   double input = 0.115301;
   input += synapse0x8d1efa8();
   input += synapse0x8d1efd0();
   input += synapse0x8d1eff8();
   input += synapse0x8d1f020();
   input += synapse0x8d1f048();
   input += synapse0x8d1f070();
   input += synapse0x8d1f098();
   input += synapse0x8d1f0c0();
   input += synapse0x8d1f0e8();
   input += synapse0x8d1f110();
   input += synapse0x8d1f138();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1ed90() {
   double input = input0x8d1ed90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1f160() {
   double input = 0.800943;
   input += synapse0x8d1f378();
   input += synapse0x8d1f3a0();
   input += synapse0x8d1f3c8();
   input += synapse0x8d1f3f0();
   input += synapse0x8d1f418();
   input += synapse0x8d1f440();
   input += synapse0x8d1f468();
   input += synapse0x8d1f490();
   input += synapse0x8d1f4b8();
   input += synapse0x8d1f4e0();
   input += synapse0x8d1f508();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1f160() {
   double input = input0x8d1f160();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1f530() {
   double input = 0.0689624;
   input += synapse0x8d1f748();
   input += synapse0x8d1f770();
   input += synapse0x8d1f798();
   input += synapse0x8d1f7c0();
   input += synapse0x8d1f7e8();
   input += synapse0x8d1f810();
   input += synapse0x8d1f838();
   input += synapse0x8d1f860();
   input += synapse0x8d1f888();
   input += synapse0x8d1f8b0();
   input += synapse0x8d1f8d8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1f530() {
   double input = input0x8d1f530();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1f900() {
   double input = 0.406006;
   input += synapse0x8d1fb18();
   input += synapse0x8d1fb40();
   input += synapse0x8d1fb68();
   input += synapse0x8d1fb90();
   input += synapse0x8d1fbb8();
   input += synapse0x8d1fbe0();
   input += synapse0x8d1fc08();
   input += synapse0x8d1fc30();
   input += synapse0x8d1fc58();
   input += synapse0x8d1fc80();
   input += synapse0x8d1fca8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1f900() {
   double input = input0x8d1f900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1fcd0() {
   double input = -0.957795;
   input += synapse0x8d1fee8();
   input += synapse0x8d1ff10();
   input += synapse0x8d1ff38();
   input += synapse0x8d1ff60();
   input += synapse0x8d1ff88();
   input += synapse0x8d1ffb0();
   input += synapse0x8d1ffd8();
   input += synapse0x8d20000();
   input += synapse0x8d20028();
   input += synapse0x8d20050();
   input += synapse0x8d20078();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1fcd0() {
   double input = input0x8d1fcd0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d200a0() {
   double input = -0.722937;
   input += synapse0x8d202b8();
   input += synapse0x8d202e0();
   input += synapse0x8d20308();
   input += synapse0x8d20330();
   input += synapse0x8d20358();
   input += synapse0x8d20380();
   input += synapse0x8d203a8();
   input += synapse0x8d203d0();
   input += synapse0x8d203f8();
   input += synapse0x8d20420();
   input += synapse0x8d20448();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d200a0() {
   double input = input0x8d200a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d20470() {
   double input = -0.559826;
   input += synapse0x8d20688();
   input += synapse0x8d206b0();
   input += synapse0x8d206d8();
   input += synapse0x8d20700();
   input += synapse0x8d20728();
   input += synapse0x8d20750();
   input += synapse0x8d20778();
   input += synapse0x8d207a0();
   input += synapse0x8d207c8();
   input += synapse0x8d207f0();
   input += synapse0x8d20818();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d20470() {
   double input = input0x8d20470();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d20840() {
   double input = 0.248262;
   input += synapse0x8d20a58();
   input += synapse0x8d181a0();
   input += synapse0x8d181c8();
   input += synapse0x8d181f0();
   input += synapse0x8d18420();
   input += synapse0x8d18448();
   input += synapse0x8d18678();
   input += synapse0x8d186a0();
   input += synapse0x8d188d8();
   input += synapse0x8d18900();
   input += synapse0x8d18928();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d20840() {
   double input = input0x8d20840();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d18b58() {
   double input = -0.188163;
   input += synapse0x8d21948();
   input += synapse0x8d21970();
   input += synapse0x8d21998();
   input += synapse0x8d219c0();
   input += synapse0x8d219e8();
   input += synapse0x8d21a10();
   input += synapse0x8d21a38();
   input += synapse0x8d21a60();
   input += synapse0x8d21a88();
   input += synapse0x8d21ab0();
   input += synapse0x8d21ad8();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d18b58() {
   double input = input0x8d18b58();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d21b00() {
   double input = -0.296398;
   input += synapse0x8d21d00();
   input += synapse0x8d21d28();
   input += synapse0x8d21d50();
   input += synapse0x8d21d78();
   input += synapse0x8d21da0();
   input += synapse0x8d21dc8();
   input += synapse0x8d21df0();
   input += synapse0x8d21e18();
   input += synapse0x8d21e40();
   input += synapse0x8d21e68();
   input += synapse0x8d21e90();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d21b00() {
   double input = input0x8d21b00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d21eb8() {
   double input = -0.0243039;
   input += synapse0x8d220d0();
   input += synapse0x8d220f8();
   input += synapse0x8d22120();
   input += synapse0x8d22148();
   input += synapse0x8d22170();
   input += synapse0x8d22198();
   input += synapse0x8d221c0();
   input += synapse0x8d221e8();
   input += synapse0x8d22210();
   input += synapse0x8d22238();
   input += synapse0x8d22260();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d21eb8() {
   double input = input0x8d21eb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d22288() {
   double input = 0.32512;
   input += synapse0x8d224a0();
   input += synapse0x8d224c8();
   input += synapse0x8d224f0();
   input += synapse0x8d22518();
   input += synapse0x8d22540();
   input += synapse0x8d22568();
   input += synapse0x8d22590();
   input += synapse0x8d225b8();
   input += synapse0x8d225e0();
   input += synapse0x8d22608();
   input += synapse0x8d22630();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d22288() {
   double input = input0x8d22288();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d22658() {
   double input = 0.439295;
   input += synapse0x8d22870();
   input += synapse0x8d22898();
   input += synapse0x8d228c0();
   input += synapse0x8d228e8();
   input += synapse0x8d22910();
   input += synapse0x8d22938();
   input += synapse0x8d22960();
   input += synapse0x8d22988();
   input += synapse0x8d229b0();
   input += synapse0x8d229d8();
   input += synapse0x8d22a00();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d22658() {
   double input = input0x8d22658();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d22a28() {
   double input = 0.0401986;
   input += synapse0x8d22c40();
   input += synapse0x8d22c68();
   input += synapse0x8d22c90();
   input += synapse0x8d22cb8();
   input += synapse0x8d22ce0();
   input += synapse0x8d22d08();
   input += synapse0x8d22d30();
   input += synapse0x8d22d58();
   input += synapse0x8d22d80();
   input += synapse0x8d22da8();
   input += synapse0x8d22dd0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d22a28() {
   double input = input0x8d22a28();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d22df8() {
   double input = 0.139759;
   input += synapse0x8d23010();
   input += synapse0x8d23038();
   input += synapse0x8d23060();
   input += synapse0x8d23088();
   input += synapse0x8d230b0();
   input += synapse0x8d230d8();
   input += synapse0x8d23100();
   input += synapse0x8d23128();
   input += synapse0x8d23150();
   input += synapse0x8d23178();
   input += synapse0x8d231a0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d22df8() {
   double input = input0x8d22df8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d231c8() {
   double input = -0.29949;
   input += synapse0x8d233e0();
   input += synapse0x8d23408();
   input += synapse0x8d23430();
   input += synapse0x8d23458();
   input += synapse0x8d23480();
   input += synapse0x8d234a8();
   input += synapse0x8d234d0();
   input += synapse0x8d234f8();
   input += synapse0x8d23520();
   input += synapse0x8d23548();
   input += synapse0x8d23570();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d231c8() {
   double input = input0x8d231c8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d23598() {
   double input = -0.52706;
   input += synapse0x8d237b0();
   input += synapse0x8d237d8();
   input += synapse0x8d23800();
   input += synapse0x8d23828();
   input += synapse0x8d23850();
   input += synapse0x8d23878();
   input += synapse0x8d238a0();
   input += synapse0x8d238c8();
   input += synapse0x8d238f0();
   input += synapse0x8d23918();
   input += synapse0x8d23940();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d23598() {
   double input = input0x8d23598();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d23968() {
   double input = 0.376999;
   input += synapse0x8d23b80();
   input += synapse0x8d23ba8();
   input += synapse0x8d23bd0();
   input += synapse0x8d23bf8();
   input += synapse0x8d23c20();
   input += synapse0x8d23c48();
   input += synapse0x8d23c70();
   input += synapse0x8d23c98();
   input += synapse0x8d23cc0();
   input += synapse0x8d23ce8();
   input += synapse0x8d23d10();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d23968() {
   double input = input0x8d23968();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d23d38() {
   double input = 0.0246345;
   input += synapse0x8d23f50();
   input += synapse0x8d23f78();
   input += synapse0x8d23fa0();
   input += synapse0x8d23fc8();
   input += synapse0x8d23ff0();
   input += synapse0x8d24018();
   input += synapse0x8d24040();
   input += synapse0x8d24068();
   input += synapse0x8d24090();
   input += synapse0x8d240b8();
   input += synapse0x8d240e0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d23d38() {
   double input = input0x8d23d38();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d24108() {
   double input = 0.577502;
   input += synapse0x8d24320();
   input += synapse0x8d24348();
   input += synapse0x8d24370();
   input += synapse0x8d24398();
   input += synapse0x8d243c0();
   input += synapse0x8d243e8();
   input += synapse0x8d24410();
   input += synapse0x8d24438();
   input += synapse0x8d24460();
   input += synapse0x8d24488();
   input += synapse0x8d244b0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d24108() {
   double input = input0x8d24108();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d244d8() {
   double input = 0.322632;
   input += synapse0x8d246f0();
   input += synapse0x8d24718();
   input += synapse0x8d24740();
   input += synapse0x8d24768();
   input += synapse0x8d24790();
   input += synapse0x8d247b8();
   input += synapse0x8d247e0();
   input += synapse0x8d24808();
   input += synapse0x8d24830();
   input += synapse0x8d24858();
   input += synapse0x8d24880();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d244d8() {
   double input = input0x8d244d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d248a8() {
   double input = -0.473974;
   input += synapse0x8d24ac0();
   input += synapse0x8d24ae8();
   input += synapse0x8d24b10();
   input += synapse0x8d24b38();
   input += synapse0x8d24b60();
   input += synapse0x8d24b88();
   input += synapse0x8d24bb0();
   input += synapse0x8d24bd8();
   input += synapse0x8d24c00();
   input += synapse0x8d24c28();
   input += synapse0x8d24c50();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d248a8() {
   double input = input0x8d248a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d24c78() {
   double input = -0.977004;
   input += synapse0x8d24e90();
   input += synapse0x8d24eb8();
   input += synapse0x8d24ee0();
   input += synapse0x8d24f08();
   input += synapse0x8d24f30();
   input += synapse0x8d24f58();
   input += synapse0x8d24f80();
   input += synapse0x8d24fa8();
   input += synapse0x8d24fd0();
   input += synapse0x8d24ff8();
   input += synapse0x8d25020();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d24c78() {
   double input = input0x8d24c78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d25048() {
   double input = 0.722031;
   input += synapse0x8d25260();
   input += synapse0x8d25288();
   input += synapse0x8d252b0();
   input += synapse0x8d252d8();
   input += synapse0x8d25300();
   input += synapse0x8d25328();
   input += synapse0x8d25350();
   input += synapse0x8d25378();
   input += synapse0x8d253a0();
   input += synapse0x8d253c8();
   input += synapse0x8d253f0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d25048() {
   double input = input0x8d25048();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d25418() {
   double input = -0.21157;
   input += synapse0x8d25630();
   input += synapse0x8d25658();
   input += synapse0x8d25680();
   input += synapse0x8d256a8();
   input += synapse0x8d256d0();
   input += synapse0x8d256f8();
   input += synapse0x8d25720();
   input += synapse0x8d25748();
   input += synapse0x8d25770();
   input += synapse0x8d25798();
   input += synapse0x8d257c0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d25418() {
   double input = input0x8d25418();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d257e8() {
   double input = 0.863513;
   input += synapse0x8d25a00();
   input += synapse0x8d25a28();
   input += synapse0x8d25a50();
   input += synapse0x8d25a78();
   input += synapse0x8d25aa0();
   input += synapse0x8d25ac8();
   input += synapse0x8d25af0();
   input += synapse0x8d25b18();
   input += synapse0x8d25b40();
   input += synapse0x8d25b68();
   input += synapse0x8d25b90();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d257e8() {
   double input = input0x8d257e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d25bb8() {
   double input = -1.10299;
   input += synapse0x8d25dd0();
   input += synapse0x8d25df8();
   input += synapse0x8d25e20();
   input += synapse0x8d25e48();
   input += synapse0x8d25e70();
   input += synapse0x8d25e98();
   input += synapse0x8d25ec0();
   input += synapse0x8d25ee8();
   input += synapse0x8d25f10();
   input += synapse0x8d25f38();
   input += synapse0x8d25f60();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d25bb8() {
   double input = input0x8d25bb8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d25f88() {
   double input = -0.287025;
   input += synapse0x8d261a0();
   input += synapse0x8d261c8();
   input += synapse0x8d261f0();
   input += synapse0x8d26218();
   input += synapse0x8d26240();
   input += synapse0x8d26268();
   input += synapse0x8d26290();
   input += synapse0x8d262b8();
   input += synapse0x8d262e0();
   input += synapse0x8d26308();
   input += synapse0x8d26330();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d25f88() {
   double input = input0x8d25f88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d26358() {
   double input = 0.18213;
   input += synapse0x8d26570();
   input += synapse0x8d26598();
   input += synapse0x8d265c0();
   input += synapse0x8d265e8();
   input += synapse0x8d26610();
   input += synapse0x8d26638();
   input += synapse0x8d26660();
   input += synapse0x8d26688();
   input += synapse0x8d266b0();
   input += synapse0x8d266d8();
   input += synapse0x8d26700();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d26358() {
   double input = input0x8d26358();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d26728() {
   double input = 1.37042;
   input += synapse0x8d26940();
   input += synapse0x8d26968();
   input += synapse0x8d26990();
   input += synapse0x8d269b8();
   input += synapse0x8d269e0();
   input += synapse0x8d26a08();
   input += synapse0x8d26a30();
   input += synapse0x8d26a58();
   input += synapse0x8d26a80();
   input += synapse0x8d26aa8();
   input += synapse0x8d26ad0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d26728() {
   double input = input0x8d26728();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d26af8() {
   double input = 0.0849937;
   input += synapse0x8d26d10();
   input += synapse0x8d26d38();
   input += synapse0x8d26d60();
   input += synapse0x8d26d88();
   input += synapse0x8d26db0();
   input += synapse0x8d26dd8();
   input += synapse0x8d26e00();
   input += synapse0x8d26e28();
   input += synapse0x8d26e50();
   input += synapse0x8d26e78();
   input += synapse0x8d26ea0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d26af8() {
   double input = input0x8d26af8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d26ec8() {
   double input = 0.759149;
   input += synapse0x8d270e0();
   input += synapse0x8d27108();
   input += synapse0x8d27130();
   input += synapse0x8d27158();
   input += synapse0x8d27180();
   input += synapse0x8d271a8();
   input += synapse0x8d271d0();
   input += synapse0x8d271f8();
   input += synapse0x8d27220();
   input += synapse0x8d27248();
   input += synapse0x8d27270();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d26ec8() {
   double input = input0x8d26ec8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d27298() {
   double input = -0.260499;
   input += synapse0x8d274b0();
   input += synapse0x8d274d8();
   input += synapse0x8d27500();
   input += synapse0x8d27528();
   input += synapse0x8d27550();
   input += synapse0x8d27578();
   input += synapse0x8d275a0();
   input += synapse0x8d275c8();
   input += synapse0x8d275f0();
   input += synapse0x8d27618();
   input += synapse0x8d27640();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d27298() {
   double input = input0x8d27298();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d27668() {
   double input = -0.870733;
   input += synapse0x8d27880();
   input += synapse0x8d278a8();
   input += synapse0x8d278d0();
   input += synapse0x8d278f8();
   input += synapse0x8d27920();
   input += synapse0x8d27948();
   input += synapse0x8d27970();
   input += synapse0x8d27998();
   input += synapse0x8d279c0();
   input += synapse0x8d279e8();
   input += synapse0x8d27a10();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d27668() {
   double input = input0x8d27668();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d27a38() {
   double input = -0.0358374;
   input += synapse0x8d27c50();
   input += synapse0x8d27c78();
   input += synapse0x8d27ca0();
   input += synapse0x8d27cc8();
   input += synapse0x8d27cf0();
   input += synapse0x8d27d18();
   input += synapse0x8d27d40();
   input += synapse0x8d27d68();
   input += synapse0x8d27d90();
   input += synapse0x8d27db8();
   input += synapse0x8d27de0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d27a38() {
   double input = input0x8d27a38();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d27e08() {
   double input = -0.273031;
   input += synapse0x8d28020();
   input += synapse0x8d28048();
   input += synapse0x8d28070();
   input += synapse0x8d28098();
   input += synapse0x8d280c0();
   input += synapse0x8d280e8();
   input += synapse0x8d28110();
   input += synapse0x8d28138();
   input += synapse0x8d28160();
   input += synapse0x8d28188();
   input += synapse0x8d281b0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d27e08() {
   double input = input0x8d27e08();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d281d8() {
   double input = 0.557126;
   input += synapse0x8d283f0();
   input += synapse0x8d28418();
   input += synapse0x8d28440();
   input += synapse0x8d28468();
   input += synapse0x8d28490();
   input += synapse0x8d284b8();
   input += synapse0x8d284e0();
   input += synapse0x8d28508();
   input += synapse0x8d28530();
   input += synapse0x8d28558();
   input += synapse0x8d28580();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d281d8() {
   double input = input0x8d281d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d285a8() {
   double input = 0.0504387;
   input += synapse0x8d287c0();
   input += synapse0x8d287e8();
   input += synapse0x8d1c1a0();
   input += synapse0x8d1c1c8();
   input += synapse0x8d1c1f0();
   input += synapse0x8d1c218();
   input += synapse0x8d1c240();
   input += synapse0x8d1c268();
   input += synapse0x8d1c290();
   input += synapse0x8d1c2b8();
   input += synapse0x8d1c2e0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d285a8() {
   double input = input0x8d285a8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1c308() {
   double input = 0.063463;
   input += synapse0x8d1c520();
   input += synapse0x8d1c548();
   input += synapse0x8d1c570();
   input += synapse0x8d1c598();
   input += synapse0x8d1c5c0();
   input += synapse0x8d1c5e8();
   input += synapse0x8d1c610();
   input += synapse0x8d1c638();
   input += synapse0x8d1c660();
   input += synapse0x8d1c688();
   input += synapse0x8d1c6b0();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1c308() {
   double input = input0x8d1c308();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1c6d8() {
   double input = 0.160372;
   input += synapse0x8d1c8f0();
   input += synapse0x8d1c918();
   input += synapse0x8d1c940();
   input += synapse0x8d1c968();
   input += synapse0x8d1c990();
   input += synapse0x8d1c9b8();
   input += synapse0x8d1c9e0();
   input += synapse0x8d1ca08();
   input += synapse0x8d1ca30();
   input += synapse0x8d1ca58();
   input += synapse0x8d1ca80();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1c6d8() {
   double input = input0x8d1c6d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1caa8() {
   double input = 0.552926;
   input += synapse0x8d1ccc0();
   input += synapse0x8d1cce8();
   input += synapse0x8d1cd10();
   input += synapse0x8d1cd38();
   input += synapse0x8d1cd60();
   input += synapse0x8d1cd88();
   input += synapse0x8d1cdb0();
   input += synapse0x8d1cdd8();
   input += synapse0x8d1ce00();
   input += synapse0x8d1ce28();
   input += synapse0x8d1ce50();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1caa8() {
   double input = input0x8d1caa8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d1ce78() {
   double input = -0.0447025;
   input += synapse0x8d1d090();
   input += synapse0x8d1d0b8();
   input += synapse0x8d1d0e0();
   input += synapse0x8d1d108();
   input += synapse0x8d1d130();
   input += synapse0x8d1d158();
   input += synapse0x8d1d180();
   input += synapse0x8d2a818();
   input += synapse0x8d2a840();
   input += synapse0x8d2a868();
   input += synapse0x8d2a890();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d1ce78() {
   double input = input0x8d1ce78();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d2a8b8() {
   double input = -0.0551383;
   input += synapse0x8d2aad0();
   input += synapse0x8d2aaf8();
   input += synapse0x8d2ab20();
   input += synapse0x8d2ab48();
   input += synapse0x8d2ab70();
   input += synapse0x8d2ab98();
   input += synapse0x8d2abc0();
   input += synapse0x8d2abe8();
   input += synapse0x8d2ac10();
   input += synapse0x8d2ac38();
   input += synapse0x8d2ac60();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d2a8b8() {
   double input = input0x8d2a8b8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d2ac88() {
   double input = 0.11215;
   input += synapse0x8d2aea0();
   input += synapse0x8d2aec8();
   input += synapse0x8d2aef0();
   input += synapse0x8d2af18();
   input += synapse0x8d2af40();
   input += synapse0x8d2af68();
   input += synapse0x8d2af90();
   input += synapse0x8d2afb8();
   input += synapse0x8d2afe0();
   input += synapse0x8d2b008();
   input += synapse0x8d2b030();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d2ac88() {
   double input = input0x8d2ac88();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::input0x8d2b058() {
   double input = -0.149356;
   input += synapse0x8d2b180();
   input += synapse0x8d2b1a8();
   input += synapse0x8d2b1d0();
   input += synapse0x8d2b1f8();
   input += synapse0x8d2b220();
   input += synapse0x8d2b248();
   input += synapse0x8d2b270();
   input += synapse0x8d2b298();
   input += synapse0x8d2b2c0();
   input += synapse0x8d2b2e8();
   input += synapse0x8d2b310();
   input += synapse0x8d2b338();
   input += synapse0x8d2b360();
   input += synapse0x8d2b388();
   input += synapse0x8d2b3b0();
   input += synapse0x8d2b3d8();
   input += synapse0x8d2b488();
   input += synapse0x8d2b4b0();
   input += synapse0x8d2b4d8();
   input += synapse0x8d2b500();
   input += synapse0x8d2b528();
   input += synapse0x8d2b550();
   input += synapse0x8d2b578();
   input += synapse0x8d2b5a0();
   input += synapse0x8d2b5c8();
   input += synapse0x8d2b5f0();
   input += synapse0x8d2b618();
   input += synapse0x8d2b640();
   input += synapse0x8d2b668();
   input += synapse0x8d2b690();
   input += synapse0x8d2b6b8();
   input += synapse0x8d2b6e0();
   input += synapse0x8d2b400();
   input += synapse0x8d2b428();
   input += synapse0x8d2b450();
   input += synapse0x8d2b810();
   input += synapse0x8d2b838();
   input += synapse0x8d2b860();
   input += synapse0x8d2b888();
   input += synapse0x8d2b8b0();
   input += synapse0x8d2b8d8();
   input += synapse0x8d2b900();
   input += synapse0x8d2b928();
   input += synapse0x8d2b950();
   input += synapse0x8d2b978();
   input += synapse0x8d2b9a0();
   input += synapse0x8d2b9c8();
   input += synapse0x8d2b9f0();
   input += synapse0x8d2ba18();
   input += synapse0x8d2ba40();
   input += synapse0x8d2ba68();
   input += synapse0x8d2ba90();
   input += synapse0x8d2bab8();
   input += synapse0x8d2bae0();
   input += synapse0x8d2bb08();
   input += synapse0x8d2bb30();
   input += synapse0x8d2bb58();
   input += synapse0x8d2bb80();
   input += synapse0x8d2bba8();
   input += synapse0x8d2bbd0();
   input += synapse0x8d2bbf8();
   input += synapse0x8d2bc20();
   input += synapse0x8d2bc48();
   input += synapse0x8d2bc70();
   input += synapse0x8d0f290();
   input += synapse0x8d2b708();
   input += synapse0x8d2b730();
   input += synapse0x8d2b758();
   input += synapse0x8d2b780();
   input += synapse0x8d2b7a8();
   input += synapse0x8d2b7d0();
   input += synapse0x8d2bea0();
   input += synapse0x8d2bec8();
   input += synapse0x8d2bef0();
   input += synapse0x8d2bf18();
   input += synapse0x8d2bf40();
   input += synapse0x8d2bf68();
   input += synapse0x8d2bf90();
   input += synapse0x8d2bfb8();
   input += synapse0x8d2bfe0();
   input += synapse0x8d2c008();
   input += synapse0x8d2c030();
   input += synapse0x8d2c058();
   input += synapse0x8d2c080();
   input += synapse0x8d2c0a8();
   input += synapse0x8d2c0d0();
   input += synapse0x8d2c0f8();
   input += synapse0x8d2c120();
   input += synapse0x8d2c148();
   input += synapse0x8d2c170();
   input += synapse0x8d2c198();
   input += synapse0x8d2c1c0();
   input += synapse0x8d2c1e8();
   input += synapse0x8d2c210();
   input += synapse0x8d2c238();
   input += synapse0x8d2c260();
   input += synapse0x8d2c288();
   input += synapse0x8d2c2b0();
   input += synapse0x8d2c2d8();
   input += synapse0x8d2c300();
   return input;
}

double NNPerpElectronXYCorrectionClusterDeltaX::neuron0x8d2b058() {
   double input = input0x8d2b058();
   return (input * 1)+0;
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c14588() {
   return (neuron0x8cedef8()*-0.493149);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f530() {
   return (neuron0x8cee0b0()*0.1463);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f558() {
   return (neuron0x8cee2b0()*-0.0380693);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f580() {
   return (neuron0x8cee4b0()*0.186291);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f5a8() {
   return (neuron0x8d0e3d8()*0.107524);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f5d0() {
   return (neuron0x8d0e5d8()*-0.287234);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f5f8() {
   return (neuron0x8d0e7f0()*-0.00263924);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f620() {
   return (neuron0x8d0ea08()*0.451422);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f648() {
   return (neuron0x8d0ec20()*0.516496);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f670() {
   return (neuron0x8d0ee38()*-0.676835);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f698() {
   return (neuron0x8d0f038()*-0.535018);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f8c0() {
   return (neuron0x8cedef8()*-0.608312);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f8e8() {
   return (neuron0x8cee0b0()*0.50357);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f910() {
   return (neuron0x8cee2b0()*-0.448279);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f938() {
   return (neuron0x8cee4b0()*-0.0704211);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f960() {
   return (neuron0x8d0e3d8()*-0.135716);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f988() {
   return (neuron0x8d0e5d8()*-0.172611);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fa38() {
   return (neuron0x8d0e7f0()*-0.0226062);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fa60() {
   return (neuron0x8d0ea08()*-0.394056);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fa88() {
   return (neuron0x8d0ec20()*-0.145286);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fab0() {
   return (neuron0x8d0ee38()*-0.268575);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fad8() {
   return (neuron0x8d0f038()*-0.809541);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fcb8() {
   return (neuron0x8cedef8()*-0.309352);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fce0() {
   return (neuron0x8cee0b0()*0.0457199);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fd08() {
   return (neuron0x8cee2b0()*0.486556);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fd30() {
   return (neuron0x8cee4b0()*0.581158);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fd58() {
   return (neuron0x8d0e3d8()*-0.488076);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fd80() {
   return (neuron0x8d0e5d8()*-0.511884);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fda8() {
   return (neuron0x8d0e7f0()*-0.133908);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fdd0() {
   return (neuron0x8d0ea08()*-0.336441);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fdf8() {
   return (neuron0x8d0ec20()*-0.297046);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fe20() {
   return (neuron0x8d0ee38()*-0.14198);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fe48() {
   return (neuron0x8d0f038()*0.374017);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10178() {
   return (neuron0x8cedef8()*0.0296833);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d101a0() {
   return (neuron0x8cee0b0()*-0.734098);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d101c8() {
   return (neuron0x8cee2b0()*-0.262753);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d101f0() {
   return (neuron0x8cee4b0()*-0.770218);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10218() {
   return (neuron0x8d0e3d8()*-0.19388);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10240() {
   return (neuron0x8d0e5d8()*-0.851301);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10268() {
   return (neuron0x8d0e7f0()*0.40903);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10290() {
   return (neuron0x8d0ea08()*0.531805);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d102b8() {
   return (neuron0x8d0ec20()*-0.298138);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d102e0() {
   return (neuron0x8d0ee38()*0.0861429);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10308() {
   return (neuron0x8d0f038()*0.0973676);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10530() {
   return (neuron0x8cedef8()*0.222104);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10558() {
   return (neuron0x8cee0b0()*0.425818);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10580() {
   return (neuron0x8cee2b0()*-0.179929);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d105a8() {
   return (neuron0x8cee4b0()*-0.0640313);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d105d0() {
   return (neuron0x8d0e3d8()*0.402596);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d105f8() {
   return (neuron0x8d0e5d8()*-0.292968);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10620() {
   return (neuron0x8d0e7f0()*0.130407);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10648() {
   return (neuron0x8d0ea08()*0.37872);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10670() {
   return (neuron0x8d0ec20()*0.305373);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10698() {
   return (neuron0x8d0ee38()*0.0553501);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d106c0() {
   return (neuron0x8d0f038()*0.53691);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d108e8() {
   return (neuron0x8cedef8()*0.103434);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10910() {
   return (neuron0x8cee0b0()*0.0458486);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10938() {
   return (neuron0x8cee2b0()*-0.348139);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10960() {
   return (neuron0x8cee4b0()*-0.92863);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10988() {
   return (neuron0x8d0e3d8()*0.107946);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d109b0() {
   return (neuron0x8d0e5d8()*0.710441);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d109d8() {
   return (neuron0x8d0e7f0()*-0.567637);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10a00() {
   return (neuron0x8d0ea08()*-0.274889);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10a28() {
   return (neuron0x8d0ec20()*-0.62297);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10a50() {
   return (neuron0x8d0ee38()*-0.549128);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c144d0() {
   return (neuron0x8d0f038()*-0.882888);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10e50() {
   return (neuron0x8cedef8()*-0.180361);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10e78() {
   return (neuron0x8cee0b0()*0.501537);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10ea0() {
   return (neuron0x8cee2b0()*0.175309);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10ec8() {
   return (neuron0x8cee4b0()*0.131732);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10ef0() {
   return (neuron0x8d0e3d8()*-0.687291);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10f18() {
   return (neuron0x8d0e5d8()*-0.510609);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10f40() {
   return (neuron0x8d0e7f0()*-0.113542);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10f68() {
   return (neuron0x8d0ea08()*0.345427);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10f90() {
   return (neuron0x8d0ec20()*-0.350282);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10fb8() {
   return (neuron0x8d0ee38()*0.18989);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10fe0() {
   return (neuron0x8d0f038()*-0.0725805);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11220() {
   return (neuron0x8cedef8()*-0.0620419);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11248() {
   return (neuron0x8cee0b0()*-0.246308);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11270() {
   return (neuron0x8cee2b0()*-0.402535);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11298() {
   return (neuron0x8cee4b0()*0.163316);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d112c0() {
   return (neuron0x8d0e3d8()*0.371874);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d112e8() {
   return (neuron0x8d0e5d8()*-0.0333814);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11310() {
   return (neuron0x8d0e7f0()*-0.585903);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11338() {
   return (neuron0x8d0ea08()*0.479999);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11360() {
   return (neuron0x8d0ec20()*-0.651801);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11388() {
   return (neuron0x8d0ee38()*0.192026);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d113b0() {
   return (neuron0x8d0f038()*0.448253);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d115f0() {
   return (neuron0x8cedef8()*0.180618);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11618() {
   return (neuron0x8cee0b0()*-0.808476);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11640() {
   return (neuron0x8cee2b0()*1.11879);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11668() {
   return (neuron0x8cee4b0()*0.0603005);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11690() {
   return (neuron0x8d0e3d8()*0.0567265);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d116b8() {
   return (neuron0x8d0e5d8()*-0.032087);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d116e0() {
   return (neuron0x8d0e7f0()*0.191701);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11708() {
   return (neuron0x8d0ea08()*0.119507);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11730() {
   return (neuron0x8d0ec20()*-0.155764);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11758() {
   return (neuron0x8d0ee38()*-0.0737799);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11780() {
   return (neuron0x8d0f038()*0.216506);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d119c0() {
   return (neuron0x8cedef8()*-0.0561151);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d119e8() {
   return (neuron0x8cee0b0()*-0.250245);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11a10() {
   return (neuron0x8cee2b0()*0.456208);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11a38() {
   return (neuron0x8cee4b0()*-0.167839);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11a60() {
   return (neuron0x8d0e3d8()*0.616608);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11a88() {
   return (neuron0x8d0e5d8()*-0.0493134);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11ab0() {
   return (neuron0x8d0e7f0()*-0.112018);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11ad8() {
   return (neuron0x8d0ea08()*0.0142662);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11b00() {
   return (neuron0x8d0ec20()*-0.445674);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11b28() {
   return (neuron0x8d0ee38()*-0.792485);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11b50() {
   return (neuron0x8d0f038()*-0.167615);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11d90() {
   return (neuron0x8cedef8()*0.267632);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11db8() {
   return (neuron0x8cee0b0()*0.00704071);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11de0() {
   return (neuron0x8cee2b0()*-0.37023);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11e08() {
   return (neuron0x8cee4b0()*-0.009003);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11e30() {
   return (neuron0x8d0e3d8()*0.454838);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11e58() {
   return (neuron0x8d0e5d8()*-0.00590644);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11e80() {
   return (neuron0x8d0e7f0()*0.48128);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11ea8() {
   return (neuron0x8d0ea08()*-0.108834);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11ed0() {
   return (neuron0x8d0ec20()*-0.404343);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11ef8() {
   return (neuron0x8d0ee38()*0.312966);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d11f20() {
   return (neuron0x8d0f038()*0.0790644);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12160() {
   return (neuron0x8cedef8()*-0.174328);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12188() {
   return (neuron0x8cee0b0()*-1.04501);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d121b0() {
   return (neuron0x8cee2b0()*-0.146276);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d121d8() {
   return (neuron0x8cee4b0()*-0.0833748);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12200() {
   return (neuron0x8d0e3d8()*-0.373879);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12228() {
   return (neuron0x8d0e5d8()*-0.517723);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12250() {
   return (neuron0x8d0e7f0()*-0.292071);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12278() {
   return (neuron0x8d0ea08()*-0.079525);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8b1f600() {
   return (neuron0x8d0ec20()*-0.272248);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8b1f628() {
   return (neuron0x8d0ee38()*-0.799749);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c7d598() {
   return (neuron0x8d0f038()*-0.612022);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d10c48() {
   return (neuron0x8cedef8()*-0.222743);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c14510() {
   return (neuron0x8cee0b0()*-0.0704679);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c14538() {
   return (neuron0x8cee2b0()*-0.406707);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c14560() {
   return (neuron0x8cee4b0()*-0.226657);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fe70() {
   return (neuron0x8d0e3d8()*0.400535);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fe98() {
   return (neuron0x8d0e5d8()*0.353054);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fec0() {
   return (neuron0x8d0e7f0()*-0.248048);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fee8() {
   return (neuron0x8d0ea08()*-0.337098);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0ff10() {
   return (neuron0x8d0ec20()*-0.67045);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0ff38() {
   return (neuron0x8d0ee38()*-0.352253);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c7d460() {
   return (neuron0x8d0f038()*0.571524);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d128a8() {
   return (neuron0x8cedef8()*-0.139912);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d128d0() {
   return (neuron0x8cee0b0()*-0.19814);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d128f8() {
   return (neuron0x8cee2b0()*0.00836435);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12920() {
   return (neuron0x8cee4b0()*1.09743);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12948() {
   return (neuron0x8d0e3d8()*-0.476442);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12970() {
   return (neuron0x8d0e5d8()*-0.845143);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12998() {
   return (neuron0x8d0e7f0()*0.129763);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d129c0() {
   return (neuron0x8d0ea08()*-0.240328);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d129e8() {
   return (neuron0x8d0ec20()*-0.378119);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12a10() {
   return (neuron0x8d0ee38()*-0.479304);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12a38() {
   return (neuron0x8d0f038()*-0.0179261);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12c78() {
   return (neuron0x8cedef8()*0.0315579);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12ca0() {
   return (neuron0x8cee0b0()*-0.789053);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12cc8() {
   return (neuron0x8cee2b0()*-0.165332);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12cf0() {
   return (neuron0x8cee4b0()*0.379853);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12d18() {
   return (neuron0x8d0e3d8()*-0.201325);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12d40() {
   return (neuron0x8d0e5d8()*3.91996);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12d68() {
   return (neuron0x8d0e7f0()*0.184756);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12d90() {
   return (neuron0x8d0ea08()*0.279241);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12db8() {
   return (neuron0x8d0ec20()*-0.31311);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12de0() {
   return (neuron0x8d0ee38()*-0.249503);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12e08() {
   return (neuron0x8d0f038()*0.124375);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13048() {
   return (neuron0x8cedef8()*-0.0790094);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d130f8() {
   return (neuron0x8cee0b0()*1.36996);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d131a8() {
   return (neuron0x8cee2b0()*-0.694339);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13258() {
   return (neuron0x8cee4b0()*0.415638);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13308() {
   return (neuron0x8d0e3d8()*0.11865);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d133b8() {
   return (neuron0x8d0e5d8()*0.0566743);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13468() {
   return (neuron0x8d0e7f0()*0.312239);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13518() {
   return (neuron0x8d0ea08()*-0.0118876);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d135c8() {
   return (neuron0x8d0ec20()*-0.494304);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13678() {
   return (neuron0x8d0ee38()*0.373238);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13728() {
   return (neuron0x8d0f038()*0.471023);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13918() {
   return (neuron0x8cedef8()*0.214757);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13940() {
   return (neuron0x8cee0b0()*-0.189368);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13968() {
   return (neuron0x8cee2b0()*-0.147783);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13990() {
   return (neuron0x8cee4b0()*-0.344032);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d139b8() {
   return (neuron0x8d0e3d8()*0.313275);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d139e0() {
   return (neuron0x8d0e5d8()*0.0778521);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13a08() {
   return (neuron0x8d0e7f0()*0.159696);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13a30() {
   return (neuron0x8d0ea08()*0.366176);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13a58() {
   return (neuron0x8d0ec20()*0.369111);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13a80() {
   return (neuron0x8d0ee38()*-0.400558);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13aa8() {
   return (neuron0x8d0f038()*0.11913);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13c10() {
   return (neuron0x8cedef8()*-0.00807793);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13c38() {
   return (neuron0x8cee0b0()*0.397429);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13c60() {
   return (neuron0x8cee2b0()*0.0547628);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13c88() {
   return (neuron0x8cee4b0()*0.060133);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13cb0() {
   return (neuron0x8d0e3d8()*0.594585);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13cd8() {
   return (neuron0x8d0e5d8()*-0.279816);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13d00() {
   return (neuron0x8d0e7f0()*0.14533);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13d28() {
   return (neuron0x8d0ea08()*-0.276245);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13d50() {
   return (neuron0x8d0ec20()*-0.316313);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13d78() {
   return (neuron0x8d0ee38()*-0.441542);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13da0() {
   return (neuron0x8d0f038()*-0.134011);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13f08() {
   return (neuron0x8cedef8()*0.184045);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13f30() {
   return (neuron0x8cee0b0()*-0.294515);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13f58() {
   return (neuron0x8cee2b0()*-0.195736);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13f80() {
   return (neuron0x8cee4b0()*-1.31327);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13fa8() {
   return (neuron0x8d0e3d8()*-0.238159);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13fd0() {
   return (neuron0x8d0e5d8()*-0.490337);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13ff8() {
   return (neuron0x8d0e7f0()*-0.0866286);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d14020() {
   return (neuron0x8d0ea08()*-0.0321842);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d14048() {
   return (neuron0x8d0ec20()*-0.126322);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d14070() {
   return (neuron0x8d0ee38()*-0.188398);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d14098() {
   return (neuron0x8d0f038()*-0.346753);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c7d4d0() {
   return (neuron0x8cedef8()*-0.772694);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c7d4f8() {
   return (neuron0x8cee0b0()*-0.130857);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c7d520() {
   return (neuron0x8cee2b0()*0.406067);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8c7d548() {
   return (neuron0x8cee4b0()*-0.262912);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d140c0() {
   return (neuron0x8d0e3d8()*-0.346803);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d140e8() {
   return (neuron0x8d0e5d8()*-0.598375);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d14110() {
   return (neuron0x8d0e7f0()*0.107332);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d14138() {
   return (neuron0x8d0ea08()*0.376244);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d14160() {
   return (neuron0x8d0ec20()*0.435342);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8cc8950() {
   return (neuron0x8d0ee38()*-0.0866974);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8cc8978() {
   return (neuron0x8d0f038()*0.402395);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8cc8a30() {
   return (neuron0x8cedef8()*-0.32802);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d153e8() {
   return (neuron0x8cee0b0()*-0.33424);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15410() {
   return (neuron0x8cee2b0()*-0.758454);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15438() {
   return (neuron0x8cee4b0()*-0.0655602);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15460() {
   return (neuron0x8d0e3d8()*-0.183944);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15488() {
   return (neuron0x8d0e5d8()*0.457824);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d154b0() {
   return (neuron0x8d0e7f0()*0.602951);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d154d8() {
   return (neuron0x8d0ea08()*0.444714);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15500() {
   return (neuron0x8d0ec20()*0.392426);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15528() {
   return (neuron0x8d0ee38()*-0.620693);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15550() {
   return (neuron0x8d0f038()*0.567649);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f9b0() {
   return (neuron0x8cedef8()*0.0999219);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f9d8() {
   return (neuron0x8cee0b0()*0.338319);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0fa00() {
   return (neuron0x8cee2b0()*-0.253399);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15880() {
   return (neuron0x8cee4b0()*0.253537);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d158a8() {
   return (neuron0x8d0e3d8()*-0.0968216);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d158d0() {
   return (neuron0x8d0e5d8()*0.0522571);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d158f8() {
   return (neuron0x8d0e7f0()*0.237298);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15920() {
   return (neuron0x8d0ea08()*0.0874984);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15948() {
   return (neuron0x8d0ec20()*-0.137325);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15970() {
   return (neuron0x8d0ee38()*-0.275676);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15998() {
   return (neuron0x8d0f038()*0.0425634);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15bc0() {
   return (neuron0x8cedef8()*0.122069);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15be8() {
   return (neuron0x8cee0b0()*-0.0875832);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15c10() {
   return (neuron0x8cee2b0()*-0.0875419);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15c38() {
   return (neuron0x8cee4b0()*0.0223557);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15c60() {
   return (neuron0x8d0e3d8()*0.572608);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15c88() {
   return (neuron0x8d0e5d8()*-0.524584);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15cb0() {
   return (neuron0x8d0e7f0()*-0.182229);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15cd8() {
   return (neuron0x8d0ea08()*0.704338);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15d00() {
   return (neuron0x8d0ec20()*-0.69602);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15d28() {
   return (neuron0x8d0ee38()*-0.13423);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15d50() {
   return (neuron0x8d0f038()*0.312181);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15f78() {
   return (neuron0x8cedef8()*-0.412367);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15fa0() {
   return (neuron0x8cee0b0()*0.100086);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15fc8() {
   return (neuron0x8cee2b0()*-0.514004);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15ff0() {
   return (neuron0x8cee4b0()*-0.324449);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d122a0() {
   return (neuron0x8d0e3d8()*-0.163192);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d122c8() {
   return (neuron0x8d0e5d8()*0.567358);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d122f0() {
   return (neuron0x8d0e7f0()*-0.188974);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12318() {
   return (neuron0x8d0ea08()*-0.625542);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12340() {
   return (neuron0x8d0ec20()*0.349366);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12368() {
   return (neuron0x8d0ee38()*-0.211734);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12390() {
   return (neuron0x8d0f038()*-0.107995);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d125d0() {
   return (neuron0x8cedef8()*-0.306697);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d125f8() {
   return (neuron0x8cee0b0()*-0.503323);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12620() {
   return (neuron0x8cee2b0()*-0.0280747);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12648() {
   return (neuron0x8cee4b0()*-0.0591741);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d12670() {
   return (neuron0x8d0e3d8()*-0.389738);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16820() {
   return (neuron0x8d0e5d8()*-0.104176);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16848() {
   return (neuron0x8d0e7f0()*-0.455446);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16870() {
   return (neuron0x8d0ea08()*0.441791);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16898() {
   return (neuron0x8d0ec20()*-0.179429);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d168c0() {
   return (neuron0x8d0ee38()*0.128504);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d168e8() {
   return (neuron0x8d0f038()*-0.425365);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16b10() {
   return (neuron0x8cedef8()*0.117908);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16b38() {
   return (neuron0x8cee0b0()*0.212502);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16b60() {
   return (neuron0x8cee2b0()*-0.541899);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16b88() {
   return (neuron0x8cee4b0()*-0.428218);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16bb0() {
   return (neuron0x8d0e3d8()*-0.449348);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16bd8() {
   return (neuron0x8d0e5d8()*-0.256914);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16c00() {
   return (neuron0x8d0e7f0()*0.114517);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16c28() {
   return (neuron0x8d0ea08()*-0.186098);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16c50() {
   return (neuron0x8d0ec20()*-0.0384194);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16c78() {
   return (neuron0x8d0ee38()*0.24887);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16ca0() {
   return (neuron0x8d0f038()*0.15069);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16ec8() {
   return (neuron0x8cedef8()*0.412875);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16ef0() {
   return (neuron0x8cee0b0()*0.354415);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16f18() {
   return (neuron0x8cee2b0()*-0.0395731);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16f40() {
   return (neuron0x8cee4b0()*-1.19532);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16f68() {
   return (neuron0x8d0e3d8()*-0.442523);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16f90() {
   return (neuron0x8d0e5d8()*2.18713);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16fb8() {
   return (neuron0x8d0e7f0()*0.476687);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16fe0() {
   return (neuron0x8d0ea08()*-0.239956);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17008() {
   return (neuron0x8d0ec20()*-0.010841);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17030() {
   return (neuron0x8d0ee38()*0.190826);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17058() {
   return (neuron0x8d0f038()*0.457507);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17280() {
   return (neuron0x8cedef8()*-0.498825);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d172a8() {
   return (neuron0x8cee0b0()*0.18812);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d172d0() {
   return (neuron0x8cee2b0()*-0.0163394);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d172f8() {
   return (neuron0x8cee4b0()*-0.597133);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17320() {
   return (neuron0x8d0e3d8()*-0.0966476);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17348() {
   return (neuron0x8d0e5d8()*1.43324);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17370() {
   return (neuron0x8d0e7f0()*-0.068852);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17398() {
   return (neuron0x8d0ea08()*0.126839);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d173c0() {
   return (neuron0x8d0ec20()*-0.385308);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d173e8() {
   return (neuron0x8d0ee38()*0.0785253);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17410() {
   return (neuron0x8d0f038()*-0.0311201);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17638() {
   return (neuron0x8cedef8()*-0.15056);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17660() {
   return (neuron0x8cee0b0()*0.289749);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17688() {
   return (neuron0x8cee2b0()*0.693568);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d176b0() {
   return (neuron0x8cee4b0()*0.130601);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d176d8() {
   return (neuron0x8d0e3d8()*-0.0741267);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17700() {
   return (neuron0x8d0e5d8()*-0.0591469);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17728() {
   return (neuron0x8d0e7f0()*0.34706);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17750() {
   return (neuron0x8d0ea08()*0.177058);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17778() {
   return (neuron0x8d0ec20()*0.415453);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d177a0() {
   return (neuron0x8d0ee38()*0.51915);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d177c8() {
   return (neuron0x8d0f038()*-0.169429);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d179f0() {
   return (neuron0x8cedef8()*-0.672518);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17a18() {
   return (neuron0x8cee0b0()*0.166973);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17a40() {
   return (neuron0x8cee2b0()*-0.459676);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17a68() {
   return (neuron0x8cee4b0()*0.227821);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17a90() {
   return (neuron0x8d0e3d8()*-0.351475);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17ab8() {
   return (neuron0x8d0e5d8()*0.682903);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17ae0() {
   return (neuron0x8d0e7f0()*-0.883308);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17b08() {
   return (neuron0x8d0ea08()*0.232541);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17b30() {
   return (neuron0x8d0ec20()*-0.022521);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17b58() {
   return (neuron0x8d0ee38()*0.44917);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17b80() {
   return (neuron0x8d0f038()*-0.247892);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17da8() {
   return (neuron0x8cedef8()*-0.120828);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17dd0() {
   return (neuron0x8cee0b0()*-0.263026);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17df8() {
   return (neuron0x8cee2b0()*0.393378);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17e20() {
   return (neuron0x8cee4b0()*0.395135);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17e48() {
   return (neuron0x8d0e3d8()*0.208434);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17e70() {
   return (neuron0x8d0e5d8()*0.318068);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17e98() {
   return (neuron0x8d0e7f0()*-1.0565);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17ec0() {
   return (neuron0x8d0ea08()*-0.367109);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17ee8() {
   return (neuron0x8d0ec20()*-0.808464);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17f10() {
   return (neuron0x8d0ee38()*0.361677);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d17f38() {
   return (neuron0x8d0f038()*0.322792);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18178() {
   return (neuron0x8cedef8()*0.766632);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13070() {
   return (neuron0x8cee0b0()*0.639055);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13098() {
   return (neuron0x8cee2b0()*-0.268576);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d130c0() {
   return (neuron0x8cee4b0()*-0.354496);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13120() {
   return (neuron0x8d0e3d8()*0.00962775);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13148() {
   return (neuron0x8d0e5d8()*-0.0790421);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13170() {
   return (neuron0x8d0e7f0()*0.0421324);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d131d0() {
   return (neuron0x8d0ea08()*0.0522188);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d131f8() {
   return (neuron0x8d0ec20()*-0.331435);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13220() {
   return (neuron0x8d0ee38()*0.216462);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13280() {
   return (neuron0x8d0f038()*-0.25615);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13428() {
   return (neuron0x8cedef8()*0.467593);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13378() {
   return (neuron0x8cee0b0()*0.418972);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13490() {
   return (neuron0x8cee2b0()*-0.385496);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d134b8() {
   return (neuron0x8cee4b0()*-0.505691);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d134e0() {
   return (neuron0x8d0e3d8()*0.0768243);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13540() {
   return (neuron0x8d0e5d8()*-0.542846);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13568() {
   return (neuron0x8d0e7f0()*-0.343816);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13590() {
   return (neuron0x8d0ea08()*-0.759034);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d135f0() {
   return (neuron0x8d0ec20()*0.196353);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13618() {
   return (neuron0x8d0ee38()*-0.138987);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13640() {
   return (neuron0x8d0f038()*1.12797);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d136e8() {
   return (neuron0x8cedef8()*-0.282277);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d13798() {
   return (neuron0x8cee0b0()*-0.190103);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18f90() {
   return (neuron0x8cee2b0()*-0.284179);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18fb8() {
   return (neuron0x8cee4b0()*0.424893);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18fe0() {
   return (neuron0x8d0e3d8()*-0.407441);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19008() {
   return (neuron0x8d0e5d8()*-0.107967);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19030() {
   return (neuron0x8d0e7f0()*0.155214);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19058() {
   return (neuron0x8d0ea08()*0.404608);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19080() {
   return (neuron0x8d0ec20()*0.141702);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d190a8() {
   return (neuron0x8d0ee38()*-0.27917);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d190d0() {
   return (neuron0x8d0f038()*0.331895);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d192f8() {
   return (neuron0x8cedef8()*0.12453);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19320() {
   return (neuron0x8cee0b0()*-0.451859);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19348() {
   return (neuron0x8cee2b0()*0.533304);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19370() {
   return (neuron0x8cee4b0()*0.441696);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19398() {
   return (neuron0x8d0e3d8()*0.459211);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d193c0() {
   return (neuron0x8d0e5d8()*0.131433);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d193e8() {
   return (neuron0x8d0e7f0()*-0.0965776);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19410() {
   return (neuron0x8d0ea08()*-0.317792);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19438() {
   return (neuron0x8d0ec20()*-0.656086);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19460() {
   return (neuron0x8d0ee38()*-0.577718);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19488() {
   return (neuron0x8d0f038()*0.185069);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d196b0() {
   return (neuron0x8cedef8()*-0.71574);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d196d8() {
   return (neuron0x8cee0b0()*0.376903);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19700() {
   return (neuron0x8cee2b0()*-0.0544268);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19728() {
   return (neuron0x8cee4b0()*-0.262817);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19750() {
   return (neuron0x8d0e3d8()*-0.0652315);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19778() {
   return (neuron0x8d0e5d8()*0.0421637);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d197a0() {
   return (neuron0x8d0e7f0()*-0.00170582);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d197c8() {
   return (neuron0x8d0ea08()*-0.48948);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d197f0() {
   return (neuron0x8d0ec20()*-0.120203);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19818() {
   return (neuron0x8d0ee38()*0.142206);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19840() {
   return (neuron0x8d0f038()*0.208897);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19a68() {
   return (neuron0x8cedef8()*-0.00707931);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19a90() {
   return (neuron0x8cee0b0()*-0.54563);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19ab8() {
   return (neuron0x8cee2b0()*-0.0126856);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19ae0() {
   return (neuron0x8cee4b0()*0.31523);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19b08() {
   return (neuron0x8d0e3d8()*0.0929278);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19b30() {
   return (neuron0x8d0e5d8()*-0.425067);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19b58() {
   return (neuron0x8d0e7f0()*0.263107);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19b80() {
   return (neuron0x8d0ea08()*-0.323342);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19ba8() {
   return (neuron0x8d0ec20()*-0.311164);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19bd0() {
   return (neuron0x8d0ee38()*-0.0467666);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19bf8() {
   return (neuron0x8d0f038()*-0.0486803);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19e38() {
   return (neuron0x8cedef8()*-0.503783);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19e60() {
   return (neuron0x8cee0b0()*-1.43991);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19e88() {
   return (neuron0x8cee2b0()*-0.151557);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19eb0() {
   return (neuron0x8cee4b0()*-0.538392);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19ed8() {
   return (neuron0x8d0e3d8()*0.0623283);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19f00() {
   return (neuron0x8d0e5d8()*1.61878);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19f28() {
   return (neuron0x8d0e7f0()*-0.384215);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19f50() {
   return (neuron0x8d0ea08()*0.0987888);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19f78() {
   return (neuron0x8d0ec20()*-0.0915815);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19fa0() {
   return (neuron0x8d0ee38()*-0.223653);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d19fc8() {
   return (neuron0x8d0f038()*-0.0883928);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a208() {
   return (neuron0x8cedef8()*-0.150113);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a230() {
   return (neuron0x8cee0b0()*0.100169);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a258() {
   return (neuron0x8cee2b0()*0.229593);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a280() {
   return (neuron0x8cee4b0()*-0.18921);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a2a8() {
   return (neuron0x8d0e3d8()*-0.418115);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a2d0() {
   return (neuron0x8d0e5d8()*0.100617);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a2f8() {
   return (neuron0x8d0e7f0()*-0.0290777);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a320() {
   return (neuron0x8d0ea08()*-0.125974);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a348() {
   return (neuron0x8d0ec20()*0.0604188);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a370() {
   return (neuron0x8d0ee38()*-0.17101);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a398() {
   return (neuron0x8d0f038()*-0.557378);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a5d8() {
   return (neuron0x8cedef8()*0.0514097);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a600() {
   return (neuron0x8cee0b0()*0.414496);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a628() {
   return (neuron0x8cee2b0()*0.302663);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a650() {
   return (neuron0x8cee4b0()*-0.160165);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a678() {
   return (neuron0x8d0e3d8()*-0.193412);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a6a0() {
   return (neuron0x8d0e5d8()*-0.430189);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a6c8() {
   return (neuron0x8d0e7f0()*-0.118163);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a6f0() {
   return (neuron0x8d0ea08()*-0.3371);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a718() {
   return (neuron0x8d0ec20()*0.357703);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a740() {
   return (neuron0x8d0ee38()*-0.452694);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a768() {
   return (neuron0x8d0f038()*0.283544);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a9a8() {
   return (neuron0x8cedef8()*-0.443812);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a9d0() {
   return (neuron0x8cee0b0()*-0.416368);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1a9f8() {
   return (neuron0x8cee2b0()*0.487215);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1aa20() {
   return (neuron0x8cee4b0()*0.498165);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1aa48() {
   return (neuron0x8d0e3d8()*-0.553935);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1aa70() {
   return (neuron0x8d0e5d8()*-0.325001);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1aa98() {
   return (neuron0x8d0e7f0()*-0.383263);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1aac0() {
   return (neuron0x8d0ea08()*-1.1093);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1aae8() {
   return (neuron0x8d0ec20()*-0.0362869);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ab10() {
   return (neuron0x8d0ee38()*-0.171552);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ab38() {
   return (neuron0x8d0f038()*0.125225);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ad78() {
   return (neuron0x8cedef8()*-0.0226117);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ada0() {
   return (neuron0x8cee0b0()*-0.257177);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1adc8() {
   return (neuron0x8cee2b0()*6.17441);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1adf0() {
   return (neuron0x8cee4b0()*0.113001);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ae18() {
   return (neuron0x8d0e3d8()*0.0105123);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ae40() {
   return (neuron0x8d0e5d8()*-0.0631637);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ae68() {
   return (neuron0x8d0e7f0()*0.0904928);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ae90() {
   return (neuron0x8d0ea08()*0.51708);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1aeb8() {
   return (neuron0x8d0ec20()*-0.319707);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1aee0() {
   return (neuron0x8d0ee38()*0.0541861);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1af08() {
   return (neuron0x8d0f038()*-0.00457298);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b148() {
   return (neuron0x8cedef8()*-0.109938);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b170() {
   return (neuron0x8cee0b0()*-0.039084);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b198() {
   return (neuron0x8cee2b0()*0.542308);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b1c0() {
   return (neuron0x8cee4b0()*0.234293);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b1e8() {
   return (neuron0x8d0e3d8()*-0.477295);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b210() {
   return (neuron0x8d0e5d8()*-0.138718);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b238() {
   return (neuron0x8d0e7f0()*-0.00514207);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b260() {
   return (neuron0x8d0ea08()*-0.252166);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b288() {
   return (neuron0x8d0ec20()*0.130575);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b2b0() {
   return (neuron0x8d0ee38()*0.318399);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b2d8() {
   return (neuron0x8d0f038()*0.226852);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b518() {
   return (neuron0x8cedef8()*0.174384);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b540() {
   return (neuron0x8cee0b0()*0.717719);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b568() {
   return (neuron0x8cee2b0()*-0.330566);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b590() {
   return (neuron0x8cee4b0()*0.30472);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b5b8() {
   return (neuron0x8d0e3d8()*-0.43315);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b5e0() {
   return (neuron0x8d0e5d8()*-0.258682);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b608() {
   return (neuron0x8d0e7f0()*0.460523);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b630() {
   return (neuron0x8d0ea08()*-0.0275971);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b658() {
   return (neuron0x8d0ec20()*0.172318);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b680() {
   return (neuron0x8d0ee38()*0.155517);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b6a8() {
   return (neuron0x8d0f038()*-0.229086);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b8e8() {
   return (neuron0x8cedef8()*0.288658);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b910() {
   return (neuron0x8cee0b0()*-0.32915);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b938() {
   return (neuron0x8cee2b0()*-0.419541);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b960() {
   return (neuron0x8cee4b0()*-0.238391);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b988() {
   return (neuron0x8d0e3d8()*0.309778);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b9b0() {
   return (neuron0x8d0e5d8()*0.260957);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1b9d8() {
   return (neuron0x8d0e7f0()*-0.329911);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ba00() {
   return (neuron0x8d0ea08()*0.119622);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ba28() {
   return (neuron0x8d0ec20()*0.157719);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ba50() {
   return (neuron0x8d0ee38()*0.161008);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ba78() {
   return (neuron0x8d0f038()*-0.316812);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bcb8() {
   return (neuron0x8cedef8()*0.0186261);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bce0() {
   return (neuron0x8cee0b0()*-0.0800615);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bd08() {
   return (neuron0x8cee2b0()*0.333352);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bd30() {
   return (neuron0x8cee4b0()*0.40525);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bd58() {
   return (neuron0x8d0e3d8()*-0.00409041);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bd80() {
   return (neuron0x8d0e5d8()*-0.460156);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bda8() {
   return (neuron0x8d0e7f0()*0.0445075);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bdd0() {
   return (neuron0x8d0ea08()*0.265995);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1bdf8() {
   return (neuron0x8d0ec20()*0.230331);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1be20() {
   return (neuron0x8d0ee38()*-0.178635);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1be48() {
   return (neuron0x8d0f038()*-0.275033);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c088() {
   return (neuron0x8cedef8()*0.040298);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c0b0() {
   return (neuron0x8cee0b0()*-0.815233);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c0d8() {
   return (neuron0x8cee2b0()*-0.967916);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c100() {
   return (neuron0x8cee4b0()*-0.639721);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c128() {
   return (neuron0x8d0e3d8()*0.000383786);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c150() {
   return (neuron0x8d0e5d8()*-0.494969);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c178() {
   return (neuron0x8d0e7f0()*0.59265);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16018() {
   return (neuron0x8d0ea08()*-0.0028895);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16040() {
   return (neuron0x8d0ec20()*-0.427664);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16068() {
   return (neuron0x8d0ee38()*0.483823);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16090() {
   return (neuron0x8d0f038()*0.0161766);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d162d0() {
   return (neuron0x8cedef8()*-0.597646);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d162f8() {
   return (neuron0x8cee0b0()*-0.216278);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16320() {
   return (neuron0x8cee2b0()*-0.0129149);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16348() {
   return (neuron0x8cee4b0()*-0.314081);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16370() {
   return (neuron0x8d0e3d8()*0.368975);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16398() {
   return (neuron0x8d0e5d8()*-0.730981);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d163c0() {
   return (neuron0x8d0e7f0()*0.234913);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d163e8() {
   return (neuron0x8d0ea08()*0.192132);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16410() {
   return (neuron0x8d0ec20()*0.21638);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16438() {
   return (neuron0x8d0ee38()*0.467043);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16460() {
   return (neuron0x8d0f038()*-0.0454329);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d166a0() {
   return (neuron0x8cedef8()*-0.0975928);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d166c8() {
   return (neuron0x8cee0b0()*-0.473784);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d166f0() {
   return (neuron0x8cee2b0()*0.0153619);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16718() {
   return (neuron0x8cee4b0()*0.595064);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16740() {
   return (neuron0x8d0e3d8()*-0.138399);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16768() {
   return (neuron0x8d0e5d8()*-1.20419);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d16790() {
   return (neuron0x8d0e7f0()*-0.0542021);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d167b8() {
   return (neuron0x8d0ea08()*-0.401989);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d167e0() {
   return (neuron0x8d0ec20()*-0.573571);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d1a8() {
   return (neuron0x8d0ee38()*-0.263524);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d1d0() {
   return (neuron0x8d0f038()*0.0976151);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d3f8() {
   return (neuron0x8cedef8()*0.227977);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d420() {
   return (neuron0x8cee0b0()*-0.766993);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d448() {
   return (neuron0x8cee2b0()*0.119684);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d470() {
   return (neuron0x8cee4b0()*0.281219);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d498() {
   return (neuron0x8d0e3d8()*-0.277358);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d4c0() {
   return (neuron0x8d0e5d8()*-0.334445);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d4e8() {
   return (neuron0x8d0e7f0()*0.0686192);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d510() {
   return (neuron0x8d0ea08()*0.0435433);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d538() {
   return (neuron0x8d0ec20()*-0.353309);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d560() {
   return (neuron0x8d0ee38()*-0.232586);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d588() {
   return (neuron0x8d0f038()*0.784706);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d7c8() {
   return (neuron0x8cedef8()*0.448062);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d7f0() {
   return (neuron0x8cee0b0()*0.2308);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d818() {
   return (neuron0x8cee2b0()*0.60302);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d840() {
   return (neuron0x8cee4b0()*-0.296278);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d868() {
   return (neuron0x8d0e3d8()*0.0166472);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d890() {
   return (neuron0x8d0e5d8()*-0.191089);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d8b8() {
   return (neuron0x8d0e7f0()*0.241372);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d8e0() {
   return (neuron0x8d0ea08()*-0.0237458);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d908() {
   return (neuron0x8d0ec20()*-0.709908);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d930() {
   return (neuron0x8d0ee38()*0.533807);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d958() {
   return (neuron0x8d0f038()*0.855625);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1db98() {
   return (neuron0x8cedef8()*-0.237729);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dbc0() {
   return (neuron0x8cee0b0()*0.473593);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dbe8() {
   return (neuron0x8cee2b0()*0.0269316);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dc10() {
   return (neuron0x8cee4b0()*0.262304);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dc38() {
   return (neuron0x8d0e3d8()*0.0923673);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dc60() {
   return (neuron0x8d0e5d8()*0.208427);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dc88() {
   return (neuron0x8d0e7f0()*0.484499);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dcb0() {
   return (neuron0x8d0ea08()*0.770052);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dcd8() {
   return (neuron0x8d0ec20()*-0.392207);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dd00() {
   return (neuron0x8d0ee38()*-0.540818);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dd28() {
   return (neuron0x8d0f038()*-0.258837);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1df68() {
   return (neuron0x8cedef8()*0.221547);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1df90() {
   return (neuron0x8cee0b0()*0.438879);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dfb8() {
   return (neuron0x8cee2b0()*-0.734048);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1dfe0() {
   return (neuron0x8cee4b0()*-0.355418);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e008() {
   return (neuron0x8d0e3d8()*0.32182);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e030() {
   return (neuron0x8d0e5d8()*0.221525);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e058() {
   return (neuron0x8d0e7f0()*-0.181881);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e080() {
   return (neuron0x8d0ea08()*0.0832018);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e0a8() {
   return (neuron0x8d0ec20()*0.175735);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e0d0() {
   return (neuron0x8d0ee38()*0.139051);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e0f8() {
   return (neuron0x8d0f038()*0.642727);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15778() {
   return (neuron0x8cedef8()*0.143439);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d157a0() {
   return (neuron0x8cee0b0()*0.0787779);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d157c8() {
   return (neuron0x8cee2b0()*-0.446288);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d157f0() {
   return (neuron0x8cee4b0()*1.01784);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15818() {
   return (neuron0x8d0e3d8()*0.470078);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d15840() {
   return (neuron0x8d0e5d8()*0.305486);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e540() {
   return (neuron0x8d0e7f0()*-0.793965);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e568() {
   return (neuron0x8d0ea08()*0.113816);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e590() {
   return (neuron0x8d0ec20()*-0.348566);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e5b8() {
   return (neuron0x8d0ee38()*-0.0999337);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e5e0() {
   return (neuron0x8d0f038()*0.0896754);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e808() {
   return (neuron0x8cedef8()*0.552015);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e830() {
   return (neuron0x8cee0b0()*-0.28863);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e858() {
   return (neuron0x8cee2b0()*-0.0267902);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e880() {
   return (neuron0x8cee4b0()*0.427516);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e8a8() {
   return (neuron0x8d0e3d8()*-0.129714);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e8d0() {
   return (neuron0x8d0e5d8()*0.961665);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e8f8() {
   return (neuron0x8d0e7f0()*-0.0536974);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e920() {
   return (neuron0x8d0ea08()*-0.217213);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e948() {
   return (neuron0x8d0ec20()*-0.237984);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e970() {
   return (neuron0x8d0ee38()*0.250969);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1e998() {
   return (neuron0x8d0f038()*0.223369);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ebd8() {
   return (neuron0x8cedef8()*0.569808);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ec00() {
   return (neuron0x8cee0b0()*-0.868676);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ec28() {
   return (neuron0x8cee2b0()*-0.0935833);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ec50() {
   return (neuron0x8cee4b0()*0.845412);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ec78() {
   return (neuron0x8d0e3d8()*0.309801);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1eca0() {
   return (neuron0x8d0e5d8()*1.05608);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ecc8() {
   return (neuron0x8d0e7f0()*0.0137269);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ecf0() {
   return (neuron0x8d0ea08()*-0.639123);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ed18() {
   return (neuron0x8d0ec20()*-0.140992);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ed40() {
   return (neuron0x8d0ee38()*-0.511487);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ed68() {
   return (neuron0x8d0f038()*-0.130452);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1efa8() {
   return (neuron0x8cedef8()*0.198773);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1efd0() {
   return (neuron0x8cee0b0()*-0.163015);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1eff8() {
   return (neuron0x8cee2b0()*-0.572985);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f020() {
   return (neuron0x8cee4b0()*-0.38866);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f048() {
   return (neuron0x8d0e3d8()*0.645034);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f070() {
   return (neuron0x8d0e5d8()*0.224127);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f098() {
   return (neuron0x8d0e7f0()*0.233209);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f0c0() {
   return (neuron0x8d0ea08()*0.511649);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f0e8() {
   return (neuron0x8d0ec20()*-0.298533);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f110() {
   return (neuron0x8d0ee38()*-0.348012);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f138() {
   return (neuron0x8d0f038()*0.228576);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f378() {
   return (neuron0x8cedef8()*-0.0246575);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f3a0() {
   return (neuron0x8cee0b0()*0.0697548);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f3c8() {
   return (neuron0x8cee2b0()*-0.0655155);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f3f0() {
   return (neuron0x8cee4b0()*-1.13092);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f418() {
   return (neuron0x8d0e3d8()*0.13741);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f440() {
   return (neuron0x8d0e5d8()*0.600749);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f468() {
   return (neuron0x8d0e7f0()*-0.101579);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f490() {
   return (neuron0x8d0ea08()*-1.03181);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f4b8() {
   return (neuron0x8d0ec20()*-0.291943);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f4e0() {
   return (neuron0x8d0ee38()*-0.287564);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f508() {
   return (neuron0x8d0f038()*-0.33337);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f748() {
   return (neuron0x8cedef8()*0.0721847);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f770() {
   return (neuron0x8cee0b0()*-0.0812605);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f798() {
   return (neuron0x8cee2b0()*-0.289446);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f7c0() {
   return (neuron0x8cee4b0()*0.685848);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f7e8() {
   return (neuron0x8d0e3d8()*0.72842);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f810() {
   return (neuron0x8d0e5d8()*0.329129);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f838() {
   return (neuron0x8d0e7f0()*0.656763);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f860() {
   return (neuron0x8d0ea08()*-0.86338);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f888() {
   return (neuron0x8d0ec20()*-0.279331);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f8b0() {
   return (neuron0x8d0ee38()*0.0670895);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1f8d8() {
   return (neuron0x8d0f038()*-0.289448);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fb18() {
   return (neuron0x8cedef8()*-0.462048);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fb40() {
   return (neuron0x8cee0b0()*-0.0987235);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fb68() {
   return (neuron0x8cee2b0()*-0.961815);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fb90() {
   return (neuron0x8cee4b0()*-0.609521);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fbb8() {
   return (neuron0x8d0e3d8()*0.296711);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fbe0() {
   return (neuron0x8d0e5d8()*-0.572666);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fc08() {
   return (neuron0x8d0e7f0()*0.265055);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fc30() {
   return (neuron0x8d0ea08()*0.85786);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fc58() {
   return (neuron0x8d0ec20()*-0.139021);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fc80() {
   return (neuron0x8d0ee38()*0.380469);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fca8() {
   return (neuron0x8d0f038()*-0.195947);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1fee8() {
   return (neuron0x8cedef8()*0.0929081);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ff10() {
   return (neuron0x8cee0b0()*-0.765569);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ff38() {
   return (neuron0x8cee2b0()*-1.00484);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ff60() {
   return (neuron0x8cee4b0()*0.221683);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ff88() {
   return (neuron0x8d0e3d8()*-0.0864149);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ffb0() {
   return (neuron0x8d0e5d8()*0.200112);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ffd8() {
   return (neuron0x8d0e7f0()*-0.189745);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20000() {
   return (neuron0x8d0ea08()*0.385343);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20028() {
   return (neuron0x8d0ec20()*0.325307);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20050() {
   return (neuron0x8d0ee38()*0.027914);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20078() {
   return (neuron0x8d0f038()*0.123685);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d202b8() {
   return (neuron0x8cedef8()*0.286826);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d202e0() {
   return (neuron0x8cee0b0()*0.262368);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20308() {
   return (neuron0x8cee2b0()*-0.236014);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20330() {
   return (neuron0x8cee4b0()*0.864121);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20358() {
   return (neuron0x8d0e3d8()*0.631615);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20380() {
   return (neuron0x8d0e5d8()*-0.788492);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d203a8() {
   return (neuron0x8d0e7f0()*-0.606741);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d203d0() {
   return (neuron0x8d0ea08()*-0.548794);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d203f8() {
   return (neuron0x8d0ec20()*-0.270922);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20420() {
   return (neuron0x8d0ee38()*-0.0557999);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20448() {
   return (neuron0x8d0f038()*-0.560951);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20688() {
   return (neuron0x8cedef8()*0.652179);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d206b0() {
   return (neuron0x8cee0b0()*-0.312876);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d206d8() {
   return (neuron0x8cee2b0()*-0.623876);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20700() {
   return (neuron0x8cee4b0()*-0.0560139);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20728() {
   return (neuron0x8d0e3d8()*0.578514);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20750() {
   return (neuron0x8d0e5d8()*0.146612);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20778() {
   return (neuron0x8d0e7f0()*0.190245);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d207a0() {
   return (neuron0x8d0ea08()*-0.354699);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d207c8() {
   return (neuron0x8d0ec20()*-0.180464);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d207f0() {
   return (neuron0x8d0ee38()*-0.280999);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20818() {
   return (neuron0x8d0f038()*-0.220427);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d20a58() {
   return (neuron0x8cedef8()*0.126313);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d181a0() {
   return (neuron0x8cee0b0()*-0.0638124);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d181c8() {
   return (neuron0x8cee2b0()*-0.165075);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d181f0() {
   return (neuron0x8cee4b0()*0.00925831);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18420() {
   return (neuron0x8d0e3d8()*-0.353102);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18448() {
   return (neuron0x8d0e5d8()*-0.140163);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18678() {
   return (neuron0x8d0e7f0()*0.0789937);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d186a0() {
   return (neuron0x8d0ea08()*0.207387);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d188d8() {
   return (neuron0x8d0ec20()*0.333405);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18900() {
   return (neuron0x8d0ee38()*0.0465413);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d18928() {
   return (neuron0x8d0f038()*-0.323681);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21948() {
   return (neuron0x8cedef8()*-0.550263);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21970() {
   return (neuron0x8cee0b0()*0.32755);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21998() {
   return (neuron0x8cee2b0()*0.238568);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d219c0() {
   return (neuron0x8cee4b0()*0.14977);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d219e8() {
   return (neuron0x8d0e3d8()*-0.601074);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21a10() {
   return (neuron0x8d0e5d8()*0.354674);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21a38() {
   return (neuron0x8d0e7f0()*0.692294);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21a60() {
   return (neuron0x8d0ea08()*-0.0772802);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21a88() {
   return (neuron0x8d0ec20()*-0.772918);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21ab0() {
   return (neuron0x8d0ee38()*0.0759895);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21ad8() {
   return (neuron0x8d0f038()*0.0379629);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21d00() {
   return (neuron0x8cedef8()*-0.0946384);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21d28() {
   return (neuron0x8cee0b0()*-0.21018);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21d50() {
   return (neuron0x8cee2b0()*0.426778);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21d78() {
   return (neuron0x8cee4b0()*-0.300765);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21da0() {
   return (neuron0x8d0e3d8()*0.152001);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21dc8() {
   return (neuron0x8d0e5d8()*-0.195669);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21df0() {
   return (neuron0x8d0e7f0()*0.458082);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21e18() {
   return (neuron0x8d0ea08()*0.324515);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21e40() {
   return (neuron0x8d0ec20()*-0.163691);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21e68() {
   return (neuron0x8d0ee38()*-0.0649245);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d21e90() {
   return (neuron0x8d0f038()*-0.434626);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d220d0() {
   return (neuron0x8cedef8()*-0.296206);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d220f8() {
   return (neuron0x8cee0b0()*0.096313);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22120() {
   return (neuron0x8cee2b0()*0.0769751);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22148() {
   return (neuron0x8cee4b0()*-0.383627);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22170() {
   return (neuron0x8d0e3d8()*-0.251714);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22198() {
   return (neuron0x8d0e5d8()*0.137325);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d221c0() {
   return (neuron0x8d0e7f0()*0.072464);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d221e8() {
   return (neuron0x8d0ea08()*0.611484);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22210() {
   return (neuron0x8d0ec20()*-0.153408);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22238() {
   return (neuron0x8d0ee38()*0.14393);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22260() {
   return (neuron0x8d0f038()*0.276883);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d224a0() {
   return (neuron0x8cedef8()*0.185048);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d224c8() {
   return (neuron0x8cee0b0()*0.684741);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d224f0() {
   return (neuron0x8cee2b0()*0.30311);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22518() {
   return (neuron0x8cee4b0()*0.153618);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22540() {
   return (neuron0x8d0e3d8()*-0.180758);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22568() {
   return (neuron0x8d0e5d8()*-0.0156343);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22590() {
   return (neuron0x8d0e7f0()*-0.0447128);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d225b8() {
   return (neuron0x8d0ea08()*-0.140605);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d225e0() {
   return (neuron0x8d0ec20()*-0.0560313);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22608() {
   return (neuron0x8d0ee38()*0.0408011);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22630() {
   return (neuron0x8d0f038()*-0.113196);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22870() {
   return (neuron0x8cedef8()*0.30579);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22898() {
   return (neuron0x8cee0b0()*-1.29529);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d228c0() {
   return (neuron0x8cee2b0()*-0.271865);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d228e8() {
   return (neuron0x8cee4b0()*-0.225105);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22910() {
   return (neuron0x8d0e3d8()*0.0366289);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22938() {
   return (neuron0x8d0e5d8()*1.05976);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22960() {
   return (neuron0x8d0e7f0()*0.295224);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22988() {
   return (neuron0x8d0ea08()*-1.15934);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d229b0() {
   return (neuron0x8d0ec20()*0.523346);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d229d8() {
   return (neuron0x8d0ee38()*-0.108451);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22a00() {
   return (neuron0x8d0f038()*-0.199276);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22c40() {
   return (neuron0x8cedef8()*-0.145217);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22c68() {
   return (neuron0x8cee0b0()*-0.134063);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22c90() {
   return (neuron0x8cee2b0()*-0.090749);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22cb8() {
   return (neuron0x8cee4b0()*0.112393);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22ce0() {
   return (neuron0x8d0e3d8()*0.125561);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22d08() {
   return (neuron0x8d0e5d8()*0.155636);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22d30() {
   return (neuron0x8d0e7f0()*-0.043921);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22d58() {
   return (neuron0x8d0ea08()*-0.344647);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22d80() {
   return (neuron0x8d0ec20()*-0.175316);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22da8() {
   return (neuron0x8d0ee38()*-0.414233);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d22dd0() {
   return (neuron0x8d0f038()*0.33193);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23010() {
   return (neuron0x8cedef8()*0.0797225);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23038() {
   return (neuron0x8cee0b0()*0.685908);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23060() {
   return (neuron0x8cee2b0()*-0.320217);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23088() {
   return (neuron0x8cee4b0()*0.262861);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d230b0() {
   return (neuron0x8d0e3d8()*-0.201759);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d230d8() {
   return (neuron0x8d0e5d8()*-0.484701);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23100() {
   return (neuron0x8d0e7f0()*-0.760559);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23128() {
   return (neuron0x8d0ea08()*1.49295);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23150() {
   return (neuron0x8d0ec20()*0.46201);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23178() {
   return (neuron0x8d0ee38()*0.016424);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d231a0() {
   return (neuron0x8d0f038()*-0.0350573);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d233e0() {
   return (neuron0x8cedef8()*0.118624);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23408() {
   return (neuron0x8cee0b0()*0.369566);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23430() {
   return (neuron0x8cee2b0()*-0.70529);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23458() {
   return (neuron0x8cee4b0()*-0.173693);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23480() {
   return (neuron0x8d0e3d8()*0.173765);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d234a8() {
   return (neuron0x8d0e5d8()*0.0684824);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d234d0() {
   return (neuron0x8d0e7f0()*0.170135);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d234f8() {
   return (neuron0x8d0ea08()*0.0765934);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23520() {
   return (neuron0x8d0ec20()*0.47179);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23548() {
   return (neuron0x8d0ee38()*0.0661884);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23570() {
   return (neuron0x8d0f038()*0.188807);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d237b0() {
   return (neuron0x8cedef8()*-0.326128);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d237d8() {
   return (neuron0x8cee0b0()*0.355362);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23800() {
   return (neuron0x8cee2b0()*-0.54895);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23828() {
   return (neuron0x8cee4b0()*0.285536);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23850() {
   return (neuron0x8d0e3d8()*-0.440889);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23878() {
   return (neuron0x8d0e5d8()*0.38422);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d238a0() {
   return (neuron0x8d0e7f0()*0.0797637);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d238c8() {
   return (neuron0x8d0ea08()*0.0415279);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d238f0() {
   return (neuron0x8d0ec20()*0.541059);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23918() {
   return (neuron0x8d0ee38()*-0.236368);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23940() {
   return (neuron0x8d0f038()*0.126058);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23b80() {
   return (neuron0x8cedef8()*-0.331702);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23ba8() {
   return (neuron0x8cee0b0()*-0.516469);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23bd0() {
   return (neuron0x8cee2b0()*0.711469);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23bf8() {
   return (neuron0x8cee4b0()*0.15281);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23c20() {
   return (neuron0x8d0e3d8()*0.336625);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23c48() {
   return (neuron0x8d0e5d8()*0.168387);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23c70() {
   return (neuron0x8d0e7f0()*0.0835391);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23c98() {
   return (neuron0x8d0ea08()*0.397054);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23cc0() {
   return (neuron0x8d0ec20()*0.317882);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23ce8() {
   return (neuron0x8d0ee38()*0.0964688);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23d10() {
   return (neuron0x8d0f038()*0.0889199);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23f50() {
   return (neuron0x8cedef8()*-0.142487);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23f78() {
   return (neuron0x8cee0b0()*-0.310516);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23fa0() {
   return (neuron0x8cee2b0()*0.838816);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23fc8() {
   return (neuron0x8cee4b0()*-0.6422);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d23ff0() {
   return (neuron0x8d0e3d8()*-0.36428);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24018() {
   return (neuron0x8d0e5d8()*-0.315129);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24040() {
   return (neuron0x8d0e7f0()*-0.366236);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24068() {
   return (neuron0x8d0ea08()*0.112689);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24090() {
   return (neuron0x8d0ec20()*-0.0768659);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d240b8() {
   return (neuron0x8d0ee38()*0.19576);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d240e0() {
   return (neuron0x8d0f038()*-0.248034);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24320() {
   return (neuron0x8cedef8()*0.437537);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24348() {
   return (neuron0x8cee0b0()*0.0828267);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24370() {
   return (neuron0x8cee2b0()*0.594771);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24398() {
   return (neuron0x8cee4b0()*-0.775575);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d243c0() {
   return (neuron0x8d0e3d8()*0.536969);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d243e8() {
   return (neuron0x8d0e5d8()*-0.630214);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24410() {
   return (neuron0x8d0e7f0()*0.146366);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24438() {
   return (neuron0x8d0ea08()*1.24963);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24460() {
   return (neuron0x8d0ec20()*-0.266528);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24488() {
   return (neuron0x8d0ee38()*0.427225);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d244b0() {
   return (neuron0x8d0f038()*-0.139746);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d246f0() {
   return (neuron0x8cedef8()*0.0797582);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24718() {
   return (neuron0x8cee0b0()*0.394952);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24740() {
   return (neuron0x8cee2b0()*0.508275);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24768() {
   return (neuron0x8cee4b0()*-0.339142);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24790() {
   return (neuron0x8d0e3d8()*-0.0856692);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d247b8() {
   return (neuron0x8d0e5d8()*-0.676144);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d247e0() {
   return (neuron0x8d0e7f0()*0.114023);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24808() {
   return (neuron0x8d0ea08()*-1.01689);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24830() {
   return (neuron0x8d0ec20()*0.344784);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24858() {
   return (neuron0x8d0ee38()*0.383432);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24880() {
   return (neuron0x8d0f038()*1.07314);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24ac0() {
   return (neuron0x8cedef8()*-0.0539772);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24ae8() {
   return (neuron0x8cee0b0()*-0.42881);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24b10() {
   return (neuron0x8cee2b0()*-0.236211);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24b38() {
   return (neuron0x8cee4b0()*-0.578385);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24b60() {
   return (neuron0x8d0e3d8()*0.313606);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24b88() {
   return (neuron0x8d0e5d8()*-0.111408);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24bb0() {
   return (neuron0x8d0e7f0()*-0.0361137);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24bd8() {
   return (neuron0x8d0ea08()*1.27143);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24c00() {
   return (neuron0x8d0ec20()*-0.0369971);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24c28() {
   return (neuron0x8d0ee38()*0.0186944);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24c50() {
   return (neuron0x8d0f038()*-0.305908);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24e90() {
   return (neuron0x8cedef8()*-0.0178523);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24eb8() {
   return (neuron0x8cee0b0()*-0.587646);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24ee0() {
   return (neuron0x8cee2b0()*-0.222154);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24f08() {
   return (neuron0x8cee4b0()*1.01839);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24f30() {
   return (neuron0x8d0e3d8()*-0.221924);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24f58() {
   return (neuron0x8d0e5d8()*1.69811);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24f80() {
   return (neuron0x8d0e7f0()*-0.239338);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24fa8() {
   return (neuron0x8d0ea08()*-1.01644);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24fd0() {
   return (neuron0x8d0ec20()*0.157149);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d24ff8() {
   return (neuron0x8d0ee38()*0.354081);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25020() {
   return (neuron0x8d0f038()*0.223857);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25260() {
   return (neuron0x8cedef8()*0.244493);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25288() {
   return (neuron0x8cee0b0()*-0.574443);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d252b0() {
   return (neuron0x8cee2b0()*-0.189075);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d252d8() {
   return (neuron0x8cee4b0()*-0.614823);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25300() {
   return (neuron0x8d0e3d8()*0.0426253);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25328() {
   return (neuron0x8d0e5d8()*-1.79775);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25350() {
   return (neuron0x8d0e7f0()*0.412607);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25378() {
   return (neuron0x8d0ea08()*1.16324);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d253a0() {
   return (neuron0x8d0ec20()*0.670117);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d253c8() {
   return (neuron0x8d0ee38()*-0.122228);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d253f0() {
   return (neuron0x8d0f038()*0.0374833);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25630() {
   return (neuron0x8cedef8()*-0.0838249);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25658() {
   return (neuron0x8cee0b0()*-0.364043);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25680() {
   return (neuron0x8cee2b0()*-0.942201);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d256a8() {
   return (neuron0x8cee4b0()*-0.494207);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d256d0() {
   return (neuron0x8d0e3d8()*-0.094428);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d256f8() {
   return (neuron0x8d0e5d8()*-0.350271);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25720() {
   return (neuron0x8d0e7f0()*0.471269);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25748() {
   return (neuron0x8d0ea08()*0.364842);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25770() {
   return (neuron0x8d0ec20()*0.454128);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25798() {
   return (neuron0x8d0ee38()*-0.726461);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d257c0() {
   return (neuron0x8d0f038()*0.707461);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25a00() {
   return (neuron0x8cedef8()*-0.171671);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25a28() {
   return (neuron0x8cee0b0()*0.99005);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25a50() {
   return (neuron0x8cee2b0()*0.388406);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25a78() {
   return (neuron0x8cee4b0()*1.51119);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25aa0() {
   return (neuron0x8d0e3d8()*0.139069);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25ac8() {
   return (neuron0x8d0e5d8()*0.128948);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25af0() {
   return (neuron0x8d0e7f0()*-0.203311);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25b18() {
   return (neuron0x8d0ea08()*-0.507744);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25b40() {
   return (neuron0x8d0ec20()*0.38858);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25b68() {
   return (neuron0x8d0ee38()*0.0292951);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25b90() {
   return (neuron0x8d0f038()*0.0682284);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25dd0() {
   return (neuron0x8cedef8()*-0.0686146);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25df8() {
   return (neuron0x8cee0b0()*-0.229657);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25e20() {
   return (neuron0x8cee2b0()*-0.799605);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25e48() {
   return (neuron0x8cee4b0()*-0.900716);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25e70() {
   return (neuron0x8d0e3d8()*0.762411);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25e98() {
   return (neuron0x8d0e5d8()*-0.786398);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25ec0() {
   return (neuron0x8d0e7f0()*-0.432356);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25ee8() {
   return (neuron0x8d0ea08()*-0.20825);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25f10() {
   return (neuron0x8d0ec20()*-0.280737);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25f38() {
   return (neuron0x8d0ee38()*0.273916);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d25f60() {
   return (neuron0x8d0f038()*-0.432452);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d261a0() {
   return (neuron0x8cedef8()*0.368527);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d261c8() {
   return (neuron0x8cee0b0()*-0.500732);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d261f0() {
   return (neuron0x8cee2b0()*0.451597);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26218() {
   return (neuron0x8cee4b0()*-0.311319);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26240() {
   return (neuron0x8d0e3d8()*-0.235196);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26268() {
   return (neuron0x8d0e5d8()*-0.154672);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26290() {
   return (neuron0x8d0e7f0()*0.755083);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d262b8() {
   return (neuron0x8d0ea08()*-0.441702);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d262e0() {
   return (neuron0x8d0ec20()*0.32952);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26308() {
   return (neuron0x8d0ee38()*-0.00217686);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26330() {
   return (neuron0x8d0f038()*-0.229906);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26570() {
   return (neuron0x8cedef8()*-0.289447);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26598() {
   return (neuron0x8cee0b0()*0.0873884);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d265c0() {
   return (neuron0x8cee2b0()*-0.199996);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d265e8() {
   return (neuron0x8cee4b0()*0.0791582);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26610() {
   return (neuron0x8d0e3d8()*-0.313389);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26638() {
   return (neuron0x8d0e5d8()*-0.0548313);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26660() {
   return (neuron0x8d0e7f0()*0.137938);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26688() {
   return (neuron0x8d0ea08()*0.183462);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d266b0() {
   return (neuron0x8d0ec20()*0.358212);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d266d8() {
   return (neuron0x8d0ee38()*0.433321);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26700() {
   return (neuron0x8d0f038()*0.0789282);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26940() {
   return (neuron0x8cedef8()*-0.409546);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26968() {
   return (neuron0x8cee0b0()*-0.491016);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26990() {
   return (neuron0x8cee2b0()*0.0801298);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d269b8() {
   return (neuron0x8cee4b0()*-0.344928);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d269e0() {
   return (neuron0x8d0e3d8()*-0.190291);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26a08() {
   return (neuron0x8d0e5d8()*1.19251);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26a30() {
   return (neuron0x8d0e7f0()*-0.875953);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26a58() {
   return (neuron0x8d0ea08()*0.0911861);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26a80() {
   return (neuron0x8d0ec20()*-0.91219);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26aa8() {
   return (neuron0x8d0ee38()*0.0755533);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26ad0() {
   return (neuron0x8d0f038()*-0.416354);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26d10() {
   return (neuron0x8cedef8()*-0.179391);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26d38() {
   return (neuron0x8cee0b0()*-0.139984);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26d60() {
   return (neuron0x8cee2b0()*-0.0543757);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26d88() {
   return (neuron0x8cee4b0()*-0.073273);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26db0() {
   return (neuron0x8d0e3d8()*0.100536);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26dd8() {
   return (neuron0x8d0e5d8()*-0.750271);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26e00() {
   return (neuron0x8d0e7f0()*0.70727);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26e28() {
   return (neuron0x8d0ea08()*-0.115394);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26e50() {
   return (neuron0x8d0ec20()*0.834604);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26e78() {
   return (neuron0x8d0ee38()*0.150319);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d26ea0() {
   return (neuron0x8d0f038()*0.405997);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d270e0() {
   return (neuron0x8cedef8()*0.229329);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27108() {
   return (neuron0x8cee0b0()*0.180784);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27130() {
   return (neuron0x8cee2b0()*0.25249);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27158() {
   return (neuron0x8cee4b0()*-0.399683);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27180() {
   return (neuron0x8d0e3d8()*-0.136491);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d271a8() {
   return (neuron0x8d0e5d8()*-0.605124);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d271d0() {
   return (neuron0x8d0e7f0()*0.932628);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d271f8() {
   return (neuron0x8d0ea08()*0.246138);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27220() {
   return (neuron0x8d0ec20()*-0.0830327);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27248() {
   return (neuron0x8d0ee38()*-0.26241);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27270() {
   return (neuron0x8d0f038()*-0.614733);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d274b0() {
   return (neuron0x8cedef8()*0.176664);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d274d8() {
   return (neuron0x8cee0b0()*0.435624);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27500() {
   return (neuron0x8cee2b0()*-0.0166083);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27528() {
   return (neuron0x8cee4b0()*-0.228588);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27550() {
   return (neuron0x8d0e3d8()*-0.191258);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27578() {
   return (neuron0x8d0e5d8()*0.231101);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d275a0() {
   return (neuron0x8d0e7f0()*0.129393);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d275c8() {
   return (neuron0x8d0ea08()*0.95725);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d275f0() {
   return (neuron0x8d0ec20()*-0.134277);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27618() {
   return (neuron0x8d0ee38()*-0.284439);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27640() {
   return (neuron0x8d0f038()*-0.17126);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27880() {
   return (neuron0x8cedef8()*0.0530274);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d278a8() {
   return (neuron0x8cee0b0()*0.0815223);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d278d0() {
   return (neuron0x8cee2b0()*0.0434202);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d278f8() {
   return (neuron0x8cee4b0()*0.105644);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27920() {
   return (neuron0x8d0e3d8()*0.35391);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27948() {
   return (neuron0x8d0e5d8()*0.792284);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27970() {
   return (neuron0x8d0e7f0()*0.272147);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27998() {
   return (neuron0x8d0ea08()*-0.30154);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d279c0() {
   return (neuron0x8d0ec20()*0.335431);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d279e8() {
   return (neuron0x8d0ee38()*-0.350026);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27a10() {
   return (neuron0x8d0f038()*0.247856);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27c50() {
   return (neuron0x8cedef8()*-0.800631);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27c78() {
   return (neuron0x8cee0b0()*0.177815);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27ca0() {
   return (neuron0x8cee2b0()*-0.318842);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27cc8() {
   return (neuron0x8cee4b0()*-0.0106278);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27cf0() {
   return (neuron0x8d0e3d8()*0.106643);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27d18() {
   return (neuron0x8d0e5d8()*0.265777);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27d40() {
   return (neuron0x8d0e7f0()*-0.75924);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27d68() {
   return (neuron0x8d0ea08()*0.166328);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27d90() {
   return (neuron0x8d0ec20()*0.511043);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27db8() {
   return (neuron0x8d0ee38()*0.0258272);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d27de0() {
   return (neuron0x8d0f038()*0.207282);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28020() {
   return (neuron0x8cedef8()*0.0848817);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28048() {
   return (neuron0x8cee0b0()*-0.144712);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28070() {
   return (neuron0x8cee2b0()*-0.111165);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28098() {
   return (neuron0x8cee4b0()*0.388263);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d280c0() {
   return (neuron0x8d0e3d8()*-0.298779);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d280e8() {
   return (neuron0x8d0e5d8()*0.196761);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28110() {
   return (neuron0x8d0e7f0()*-0.475399);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28138() {
   return (neuron0x8d0ea08()*-0.0761784);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28160() {
   return (neuron0x8d0ec20()*0.149943);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28188() {
   return (neuron0x8d0ee38()*0.0447666);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d281b0() {
   return (neuron0x8d0f038()*-0.340049);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d283f0() {
   return (neuron0x8cedef8()*0.205198);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28418() {
   return (neuron0x8cee0b0()*0.183342);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28440() {
   return (neuron0x8cee2b0()*-0.422289);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28468() {
   return (neuron0x8cee4b0()*-0.122855);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28490() {
   return (neuron0x8d0e3d8()*-0.295853);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d284b8() {
   return (neuron0x8d0e5d8()*0.241946);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d284e0() {
   return (neuron0x8d0e7f0()*-0.188796);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28508() {
   return (neuron0x8d0ea08()*0.657831);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28530() {
   return (neuron0x8d0ec20()*0.455084);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28558() {
   return (neuron0x8d0ee38()*-0.051848);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d28580() {
   return (neuron0x8d0f038()*-0.196224);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d287c0() {
   return (neuron0x8cedef8()*-0.407214);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d287e8() {
   return (neuron0x8cee0b0()*-0.337364);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c1a0() {
   return (neuron0x8cee2b0()*0.170387);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c1c8() {
   return (neuron0x8cee4b0()*0.257232);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c1f0() {
   return (neuron0x8d0e3d8()*0.0401492);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c218() {
   return (neuron0x8d0e5d8()*0.517437);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c240() {
   return (neuron0x8d0e7f0()*0.659989);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c268() {
   return (neuron0x8d0ea08()*-0.228593);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c290() {
   return (neuron0x8d0ec20()*-0.537572);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c2b8() {
   return (neuron0x8d0ee38()*0.0261576);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c2e0() {
   return (neuron0x8d0f038()*-0.0372168);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c520() {
   return (neuron0x8cedef8()*-0.917367);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c548() {
   return (neuron0x8cee0b0()*-0.191372);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c570() {
   return (neuron0x8cee2b0()*0.641701);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c598() {
   return (neuron0x8cee4b0()*-0.536064);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c5c0() {
   return (neuron0x8d0e3d8()*-0.0570651);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c5e8() {
   return (neuron0x8d0e5d8()*0.196164);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c610() {
   return (neuron0x8d0e7f0()*-0.191637);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c638() {
   return (neuron0x8d0ea08()*0.462639);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c660() {
   return (neuron0x8d0ec20()*0.573182);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c688() {
   return (neuron0x8d0ee38()*-0.0634769);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c6b0() {
   return (neuron0x8d0f038()*0.613809);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c8f0() {
   return (neuron0x8cedef8()*-0.203395);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c918() {
   return (neuron0x8cee0b0()*-0.0142498);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c940() {
   return (neuron0x8cee2b0()*-0.0240281);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c968() {
   return (neuron0x8cee4b0()*-0.355378);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c990() {
   return (neuron0x8d0e3d8()*-0.541471);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c9b8() {
   return (neuron0x8d0e5d8()*-0.254094);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1c9e0() {
   return (neuron0x8d0e7f0()*-0.468822);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ca08() {
   return (neuron0x8d0ea08()*-0.453344);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ca30() {
   return (neuron0x8d0ec20()*-0.0593082);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ca58() {
   return (neuron0x8d0ee38()*-0.226175);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ca80() {
   return (neuron0x8d0f038()*0.220667);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ccc0() {
   return (neuron0x8cedef8()*-0.247336);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1cce8() {
   return (neuron0x8cee0b0()*-0.508581);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1cd10() {
   return (neuron0x8cee2b0()*-0.340447);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1cd38() {
   return (neuron0x8cee4b0()*1.17608);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1cd60() {
   return (neuron0x8d0e3d8()*0.411051);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1cd88() {
   return (neuron0x8d0e5d8()*-0.820734);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1cdb0() {
   return (neuron0x8d0e7f0()*-0.0805889);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1cdd8() {
   return (neuron0x8d0ea08()*-0.50722);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ce00() {
   return (neuron0x8d0ec20()*0.272546);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ce28() {
   return (neuron0x8d0ee38()*-0.0354496);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1ce50() {
   return (neuron0x8d0f038()*-0.0436362);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d090() {
   return (neuron0x8cedef8()*0.117074);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d0b8() {
   return (neuron0x8cee0b0()*-0.322438);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d0e0() {
   return (neuron0x8cee2b0()*0.226776);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d108() {
   return (neuron0x8cee4b0()*-0.138361);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d130() {
   return (neuron0x8d0e3d8()*0.321849);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d158() {
   return (neuron0x8d0e5d8()*0.597955);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d1d180() {
   return (neuron0x8d0e7f0()*0.151104);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2a818() {
   return (neuron0x8d0ea08()*-0.305803);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2a840() {
   return (neuron0x8d0ec20()*-0.15159);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2a868() {
   return (neuron0x8d0ee38()*-0.361996);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2a890() {
   return (neuron0x8d0f038()*0.0594987);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2aad0() {
   return (neuron0x8cedef8()*0.388869);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2aaf8() {
   return (neuron0x8cee0b0()*-0.556323);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ab20() {
   return (neuron0x8cee2b0()*-0.615938);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ab48() {
   return (neuron0x8cee4b0()*0.716732);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ab70() {
   return (neuron0x8d0e3d8()*-0.360237);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ab98() {
   return (neuron0x8d0e5d8()*0.784806);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2abc0() {
   return (neuron0x8d0e7f0()*0.276491);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2abe8() {
   return (neuron0x8d0ea08()*-0.4255);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ac10() {
   return (neuron0x8d0ec20()*0.501068);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ac38() {
   return (neuron0x8d0ee38()*-0.219464);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ac60() {
   return (neuron0x8d0f038()*-0.354246);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2aea0() {
   return (neuron0x8cedef8()*0.0438266);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2aec8() {
   return (neuron0x8cee0b0()*0.795071);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2aef0() {
   return (neuron0x8cee2b0()*-0.00988055);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2af18() {
   return (neuron0x8cee4b0()*-0.0386947);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2af40() {
   return (neuron0x8d0e3d8()*0.0582814);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2af68() {
   return (neuron0x8d0e5d8()*0.59025);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2af90() {
   return (neuron0x8d0e7f0()*0.0992989);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2afb8() {
   return (neuron0x8d0ea08()*0.218885);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2afe0() {
   return (neuron0x8d0ec20()*0.477817);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b008() {
   return (neuron0x8d0ee38()*0.155836);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b030() {
   return (neuron0x8d0f038()*0.566605);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b180() {
   return (neuron0x8d0f378()*0.273499);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b1a8() {
   return (neuron0x8d0f6c0()*-0.845804);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b1d0() {
   return (neuron0x8d0fb00()*0.0188102);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b1f8() {
   return (neuron0x8d0ff78()*0.0146441);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b220() {
   return (neuron0x8d10330()*-0.294546);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b248() {
   return (neuron0x8d106e8()*-0.939138);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b270() {
   return (neuron0x8d10c80()*0.538301);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b298() {
   return (neuron0x8d11008()*0.608908);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b2c0() {
   return (neuron0x8d113d8()*0.663346);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b2e8() {
   return (neuron0x8d117a8()*-0.472002);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b310() {
   return (neuron0x8d11b78()*-0.127878);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b338() {
   return (neuron0x8d11f48()*-0.741265);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b360() {
   return (neuron0x8d10a78()*0.422406);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b388() {
   return (neuron0x8d126a8()*0.755799);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b3b0() {
   return (neuron0x8d12a60()*0.867);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b3d8() {
   return (neuron0x8d12e30()*-0.552595);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b488() {
   return (neuron0x8d137d8()*0.0645174);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b4b0() {
   return (neuron0x8d13ad0()*0.221151);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b4d8() {
   return (neuron0x8d13dc8()*0.87298);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b500() {
   return (neuron0x8d15150()*-0.713286);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b528() {
   return (neuron0x8d15278()*-0.803837);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b550() {
   return (neuron0x8d15578()*-0.239531);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b578() {
   return (neuron0x8d159c0()*0.336367);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b5a0() {
   return (neuron0x8d15d78()*-0.736448);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b5c8() {
   return (neuron0x8d123b8()*-0.213162);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b5f0() {
   return (neuron0x8d16910()*-0.260906);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b618() {
   return (neuron0x8d16cc8()*0.691378);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b640() {
   return (neuron0x8d17080()*-0.715512);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b668() {
   return (neuron0x8d17438()*-0.148519);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b690() {
   return (neuron0x8d177f0()*-0.753756);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b6b8() {
   return (neuron0x8d17ba8()*0.654828);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b6e0() {
   return (neuron0x8d17f60()*-0.527176);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b400() {
   return (neuron0x8d18cf8()*0.595989);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b428() {
   return (neuron0x8d18e20()*-0.207015);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b450() {
   return (neuron0x8d190f8()*-0.54413);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b810() {
   return (neuron0x8d194b0()*-0.674257);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b838() {
   return (neuron0x8d19868()*-0.134061);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b860() {
   return (neuron0x8d19c20()*0.720342);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b888() {
   return (neuron0x8d19ff0()*0.146587);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b8b0() {
   return (neuron0x8d1a3c0()*0.401901);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b8d8() {
   return (neuron0x8d1a790()*0.220611);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b900() {
   return (neuron0x8d1ab60()*-0.891142);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b928() {
   return (neuron0x8d1af30()*0.246905);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b950() {
   return (neuron0x8d1b300()*-0.243366);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b978() {
   return (neuron0x8d1b6d0()*-0.00465828);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b9a0() {
   return (neuron0x8d1baa0()*0.212514);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b9c8() {
   return (neuron0x8d1be70()*0.756253);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b9f0() {
   return (neuron0x8d160b8()*-0.364717);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ba18() {
   return (neuron0x8d16488()*-0.0470747);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ba40() {
   return (neuron0x8d1d1f8()*-0.756178);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ba68() {
   return (neuron0x8d1d5b0()*-0.737966);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2ba90() {
   return (neuron0x8d1d980()*0.485192);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bab8() {
   return (neuron0x8d1dd50()*-0.339436);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bae0() {
   return (neuron0x8d1e120()*-0.565269);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bb08() {
   return (neuron0x8d1e608()*-0.00608389);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bb30() {
   return (neuron0x8d1e9c0()*0.321353);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bb58() {
   return (neuron0x8d1ed90()*-0.409597);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bb80() {
   return (neuron0x8d1f160()*0.933036);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bba8() {
   return (neuron0x8d1f530()*-0.346346);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bbd0() {
   return (neuron0x8d1f900()*-0.490802);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bbf8() {
   return (neuron0x8d1fcd0()*0.908216);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bc20() {
   return (neuron0x8d200a0()*-0.408804);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bc48() {
   return (neuron0x8d20470()*0.402714);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bc70() {
   return (neuron0x8d20840()*0.0910152);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d0f290() {
   return (neuron0x8d18b58()*0.633684);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b708() {
   return (neuron0x8d21b00()*0.0352336);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b730() {
   return (neuron0x8d21eb8()*0.0580377);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b758() {
   return (neuron0x8d22288()*-0.358155);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b780() {
   return (neuron0x8d22658()*0.88408);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b7a8() {
   return (neuron0x8d22a28()*0.103916);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2b7d0() {
   return (neuron0x8d22df8()*-0.627732);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bea0() {
   return (neuron0x8d231c8()*-0.517199);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bec8() {
   return (neuron0x8d23598()*0.460589);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bef0() {
   return (neuron0x8d23968()*0.468189);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bf18() {
   return (neuron0x8d23d38()*0.558873);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bf40() {
   return (neuron0x8d24108()*-0.455114);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bf68() {
   return (neuron0x8d244d8()*-0.749035);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bf90() {
   return (neuron0x8d248a8()*0.16228);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bfb8() {
   return (neuron0x8d24c78()*0.843168);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2bfe0() {
   return (neuron0x8d25048()*-0.808322);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c008() {
   return (neuron0x8d25418()*0.746613);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c030() {
   return (neuron0x8d257e8()*-0.508942);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c058() {
   return (neuron0x8d25bb8()*0.699377);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c080() {
   return (neuron0x8d25f88()*-0.546597);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c0a8() {
   return (neuron0x8d26358()*0.0207325);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c0d0() {
   return (neuron0x8d26728()*0.421631);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c0f8() {
   return (neuron0x8d26af8()*0.637581);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c120() {
   return (neuron0x8d26ec8()*0.918135);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c148() {
   return (neuron0x8d27298()*0.142653);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c170() {
   return (neuron0x8d27668()*-0.849912);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c198() {
   return (neuron0x8d27a38()*0.813761);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c1c0() {
   return (neuron0x8d27e08()*-0.0991203);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c1e8() {
   return (neuron0x8d281d8()*0.35648);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c210() {
   return (neuron0x8d285a8()*0.19682);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c238() {
   return (neuron0x8d1c308()*-0.257993);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c260() {
   return (neuron0x8d1c6d8()*-0.486292);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c288() {
   return (neuron0x8d1caa8()*0.732647);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c2b0() {
   return (neuron0x8d1ce78()*-0.333012);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c2d8() {
   return (neuron0x8d2a8b8()*-0.96057);
}

double NNPerpElectronXYCorrectionClusterDeltaX::synapse0x8d2c300() {
   return (neuron0x8d2ac88()*0.460737);
}

