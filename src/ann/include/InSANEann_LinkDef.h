#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;



#pragma link C++ class NNParaGammaXYCorrectionClusterDeltaX+;
#pragma link C++ class NNParaGammaXYCorrectionClusterDeltaY+;

#pragma link C++ class NNPerpGammaXYCorrectionClusterDeltaX+;
#pragma link C++ class NNPerpGammaXYCorrectionClusterDeltaY+;

//--------------- Electron
// Parallel
#pragma link C++ class NNParaElectronXYCorrectionClusterDeltaX+;
#pragma link C++ class NNParaElectronXYCorrectionClusterDeltaY+;

#pragma link C++ class NNParaElectronAngleCorrectionBCPDir+;
#pragma link C++ class NNParaElectronAngleCorrectionBCPDirTheta+;
#pragma link C++ class NNParaElectronAngleCorrectionBCPDirPhi+;

#pragma link C++ class NNParaElectronAngleCorrectionDeltaTheta+;
#pragma link C++ class NNParaElectronAngleCorrectionDeltaPhi+;

// Perpendicular
#pragma link C++ class NNPerpElectronXYCorrectionClusterDeltaX+;
#pragma link C++ class NNPerpElectronXYCorrectionClusterDeltaY+;

#pragma link C++ class NNPerpPositronXYCorrectionClusterDeltaX+;
#pragma link C++ class NNPerpPositronXYCorrectionClusterDeltaY+;

#pragma link C++ class NNPerpElectronAngleCorrectionDeltaTheta+;
#pragma link C++ class NNPerpElectronAngleCorrectionDeltaPhi+;


#endif

