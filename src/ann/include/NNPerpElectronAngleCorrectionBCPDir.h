#ifndef NNPerpElectronAngleCorrectionBCPDir_h
#define NNPerpElectronAngleCorrectionBCPDir_h

class NNPerpElectronAngleCorrectionBCPDir { 
public:
   NNPerpElectronAngleCorrectionBCPDir() {}
   virtual ~NNPerpElectronAngleCorrectionBCPDir() {}
   double Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6,double in7,double in8,double in9,double in10,double in11,double in12,double in13,double in14);
   double Value(int index, double* input);
private:
   double input0;
   double input1;
   double input2;
   double input3;
   double input4;
   double input5;
   double input6;
   double input7;
   double input8;
   double input9;
   double input10;
   double input11;
   double input12;
   double input13;
   double input14;
   double neuron0x8dc30e0();
   double neuron0x8dc3278();
   double neuron0x8dc34a0();
   double neuron0x8dc36a0();
   double neuron0x8dc38a0();
   double neuron0x8dc3ac8();
   double neuron0x8dc3cc8();
   double neuron0x8dc3ec8();
   double neuron0x8dc40c8();
   double neuron0x8dc42c8();
   double neuron0x8dc44c8();
   double neuron0x8dc46c8();
   double neuron0x8dc48c8();
   double neuron0x8dc4ae0();
   double neuron0x8dc4cf8();
   double input0x8dc5038();
   double neuron0x8dc5038();
   double input0x8dc53a8();
   double neuron0x8dc53a8();
   double input0x8dc5910();
   double neuron0x8dc5910();
   double input0x8dc5cc8();
   double neuron0x8dc5cc8();
   double input0x8dc6120();
   double neuron0x8dc6120();
   double input0x8dc65f0();
   double neuron0x8dc65f0();
   double input0x8dc6a60();
   double neuron0x8dc6a60();
   double input0x8dc6ed0();
   double neuron0x8dc6ed0();
   double input0x8dc7340();
   double neuron0x8dc7340();
   double input0x8dc63e8();
   double neuron0x8dc63e8();
   double input0x8dc7c58();
   double neuron0x8dc7c58();
   double input0x8dc80c8();
   double neuron0x8dc80c8();
   double input0x8dc8538();
   double neuron0x8dc8538();
   double input0x8dc89a8();
   double neuron0x8dc89a8();
   double input0x8dc9fe8();
   double neuron0x8dc9fe8();
   double input0x8dca3b0();
   double neuron0x8dca3b0();
   double input0x8dcb000();
   double neuron0x8dcb000();
   double input0x8dcb380();
   double neuron0x8dcb380();
   double input0x8dc7738();
   double neuron0x8dc7738();
   double input0x8dcbea8();
   double neuron0x8dcbea8();
   double input0x8dcc300();
   double neuron0x8dcc300();
   double input0x8dcc758();
   double neuron0x8dcc758();
   double input0x8dccbb0();
   double neuron0x8dccbb0();
   double input0x8dcd008();
   double neuron0x8dcd008();
   double input0x8dcd460();
   double neuron0x8dcd460();
   double input0x8dcd8b8();
   double neuron0x8dcd8b8();
   double input0x8dcdd10();
   double neuron0x8dcdd10();
   double input0x8dce168();
   double neuron0x8dce168();
   double input0x8dce5c0();
   double neuron0x8dce5c0();
   double input0x8dcea18();
   double neuron0x8dcea18();
   double input0x8dcee70();
   double neuron0x8dcee70();
   double input0x8dcf2c8();
   double neuron0x8dcf2c8();
   double input0x8dd0468();
   double neuron0x8dd0468();
   double input0x8dd0590();
   double neuron0x8dd0590();
   double input0x8dd0870();
   double neuron0x8dd0870();
   double input0x8dcb790();
   double neuron0x8dcb790();
   double input0x8dcbbe8();
   double neuron0x8dcbbe8();
   double input0x8dd1d90();
   double neuron0x8dd1d90();
   double input0x8dd2598();
   double neuron0x8dd2598();
   double synapse0x8d57638();
   double synapse0x8bee188();
   double synapse0x8bee1d0();
   double synapse0x8bee2a0();
   double synapse0x8dc51f0();
   double synapse0x8dc5218();
   double synapse0x8dc5240();
   double synapse0x8dc5268();
   double synapse0x8dc5290();
   double synapse0x8dc52b8();
   double synapse0x8dc52e0();
   double synapse0x8dc5308();
   double synapse0x8dc5330();
   double synapse0x8dc5358();
   double synapse0x8dc5380();
   double synapse0x8dc5630();
   double synapse0x8dc5658();
   double synapse0x8dc5708();
   double synapse0x8dc5730();
   double synapse0x8dc5758();
   double synapse0x8dc5780();
   double synapse0x8dc57a8();
   double synapse0x8dc57d0();
   double synapse0x8dc57f8();
   double synapse0x8dc5820();
   double synapse0x8dc5848();
   double synapse0x8dc5870();
   double synapse0x8dc5898();
   double synapse0x8dc58c0();
   double synapse0x8dc58e8();
   double synapse0x8dc5a80();
   double synapse0x8dc5aa8();
   double synapse0x8dc5ad0();
   double synapse0x8dc5680();
   double synapse0x8dc56a8();
   double synapse0x8dc56d0();
   double synapse0x8d57660();
   double synapse0x8d57688();
   double synapse0x8d576b0();
   double synapse0x8d576d8();
   double synapse0x8dc5c00();
   double synapse0x8dc5c28();
   double synapse0x8dc5c50();
   double synapse0x8dc5c78();
   double synapse0x8dc5ca0();
   double synapse0x8dc5ec8();
   double synapse0x8dc5ef0();
   double synapse0x8dc5f18();
   double synapse0x8dc5f40();
   double synapse0x8dc5f68();
   double synapse0x8dc5f90();
   double synapse0x8dc5fb8();
   double synapse0x8dc5fe0();
   double synapse0x8dc6008();
   double synapse0x8dc6030();
   double synapse0x8dc6058();
   double synapse0x8dc6080();
   double synapse0x8dc60a8();
   double synapse0x8dc60d0();
   double synapse0x8dc60f8();
   double synapse0x8dc6320();
   double synapse0x8dc6348();
   double synapse0x8dc6370();
   double synapse0x8dc6398();
   double synapse0x8dc63c0();
   double synapse0x8d57600();
   double synapse0x8bee2c8();
   double synapse0x8bee2f0();
   double synapse0x8bee318();
   double synapse0x8bee340();
   double synapse0x8bee3b8();
   double synapse0x8bee3e0();
   double synapse0x8bee408();
   double synapse0x8bee430();
   double synapse0x8bee458();
   double synapse0x8dc6808();
   double synapse0x8dc6830();
   double synapse0x8dc6858();
   double synapse0x8dc6880();
   double synapse0x8dc68a8();
   double synapse0x8dc68d0();
   double synapse0x8dc68f8();
   double synapse0x8dc6920();
   double synapse0x8dc6948();
   double synapse0x8dc6970();
   double synapse0x8dc6998();
   double synapse0x8dc69c0();
   double synapse0x8dc69e8();
   double synapse0x8dc6a10();
   double synapse0x8dc6a38();
   double synapse0x8dc6c78();
   double synapse0x8dc6ca0();
   double synapse0x8dc6cc8();
   double synapse0x8dc6cf0();
   double synapse0x8dc6d18();
   double synapse0x8dc6d40();
   double synapse0x8dc6d68();
   double synapse0x8dc6d90();
   double synapse0x8dc6db8();
   double synapse0x8dc6de0();
   double synapse0x8dc6e08();
   double synapse0x8dc6e30();
   double synapse0x8dc6e58();
   double synapse0x8dc6e80();
   double synapse0x8dc6ea8();
   double synapse0x8dc70e8();
   double synapse0x8dc7110();
   double synapse0x8dc7138();
   double synapse0x8dc7160();
   double synapse0x8dc7188();
   double synapse0x8dc71b0();
   double synapse0x8dc71d8();
   double synapse0x8dc7200();
   double synapse0x8dc7228();
   double synapse0x8dc7250();
   double synapse0x8dc7278();
   double synapse0x8dc72a0();
   double synapse0x8dc72c8();
   double synapse0x8dc72f0();
   double synapse0x8dc7318();
   double synapse0x8dc7558();
   double synapse0x8dc7580();
   double synapse0x8dc75a8();
   double synapse0x8dc75d0();
   double synapse0x8dc75f8();
   double synapse0x8dc7620();
   double synapse0x8dc7648();
   double synapse0x8dc7670();
   double synapse0x8dc7698();
   double synapse0x8dc9ea0();
   double synapse0x8dc9ec8();
   double synapse0x8dc9ef0();
   double synapse0x8dc9f18();
   double synapse0x8dc9f40();
   double synapse0x8dc9f68();
   double synapse0x8dc5b40();
   double synapse0x8dc5b68();
   double synapse0x8dc5b90();
   double synapse0x8dc5bb8();
   double synapse0x8dc65b8();
   double synapse0x8dc7ac8();
   double synapse0x8dc7af0();
   double synapse0x8dc7b18();
   double synapse0x8dc7b40();
   double synapse0x8dc7b68();
   double synapse0x8dc7b90();
   double synapse0x8dc7bb8();
   double synapse0x8dc7be0();
   double synapse0x8dc7c08();
   double synapse0x8dc7c30();
   double synapse0x8dc7e70();
   double synapse0x8dc7e98();
   double synapse0x8dc7ec0();
   double synapse0x8dc7ee8();
   double synapse0x8dc7f10();
   double synapse0x8dc7f38();
   double synapse0x8dc7f60();
   double synapse0x8dc7f88();
   double synapse0x8dc7fb0();
   double synapse0x8dc7fd8();
   double synapse0x8dc8000();
   double synapse0x8dc8028();
   double synapse0x8dc8050();
   double synapse0x8dc8078();
   double synapse0x8dc80a0();
   double synapse0x8dc82e0();
   double synapse0x8dc8308();
   double synapse0x8dc8330();
   double synapse0x8dc8358();
   double synapse0x8dc8380();
   double synapse0x8dc83a8();
   double synapse0x8dc83d0();
   double synapse0x8dc83f8();
   double synapse0x8dc8420();
   double synapse0x8dc8448();
   double synapse0x8dc8470();
   double synapse0x8dc8498();
   double synapse0x8dc84c0();
   double synapse0x8dc84e8();
   double synapse0x8dc8510();
   double synapse0x8dc8750();
   double synapse0x8dc8778();
   double synapse0x8dc87a0();
   double synapse0x8dc87c8();
   double synapse0x8dc87f0();
   double synapse0x8dc8818();
   double synapse0x8dc8840();
   double synapse0x8dc8868();
   double synapse0x8dc8890();
   double synapse0x8dc88b8();
   double synapse0x8dc88e0();
   double synapse0x8dc8908();
   double synapse0x8dc8930();
   double synapse0x8dc8958();
   double synapse0x8dc8980();
   double synapse0x8dc8bc0();
   double synapse0x8dc8be8();
   double synapse0x8dc8c10();
   double synapse0x8dc8c38();
   double synapse0x8dc8c60();
   double synapse0x8dc8c88();
   double synapse0x8dc8cb0();
   double synapse0x8dc8cd8();
   double synapse0x8dc8d00();
   double synapse0x8dc8d28();
   double synapse0x8dc8d50();
   double synapse0x8dc8d78();
   double synapse0x8dc8da0();
   double synapse0x8dc8dc8();
   double synapse0x8dc8df0();
   double synapse0x8dca158();
   double synapse0x8dca180();
   double synapse0x8dca1a8();
   double synapse0x8dca1d0();
   double synapse0x8dca1f8();
   double synapse0x8dca220();
   double synapse0x8dca248();
   double synapse0x8dca270();
   double synapse0x8dca298();
   double synapse0x8dca2c0();
   double synapse0x8dca2e8();
   double synapse0x8dca310();
   double synapse0x8dca338();
   double synapse0x8dca360();
   double synapse0x8dca388();
   double synapse0x8dca5b0();
   double synapse0x8dca660();
   double synapse0x8dca710();
   double synapse0x8dca7c0();
   double synapse0x8dca870();
   double synapse0x8dca920();
   double synapse0x8dca9d0();
   double synapse0x8dcaa80();
   double synapse0x8dcab30();
   double synapse0x8dcabe0();
   double synapse0x8dcac90();
   double synapse0x8dcad40();
   double synapse0x8dcadf0();
   double synapse0x8dcaea0();
   double synapse0x8dcaf50();
   double synapse0x8dcb128();
   double synapse0x8dcb150();
   double synapse0x8dcb178();
   double synapse0x8dcb1a0();
   double synapse0x8dcb1c8();
   double synapse0x8dcb1f0();
   double synapse0x8dcb218();
   double synapse0x8dcb240();
   double synapse0x8dcb268();
   double synapse0x8dcb290();
   double synapse0x8dcb2b8();
   double synapse0x8dcb2e0();
   double synapse0x8dcb308();
   double synapse0x8dcb330();
   double synapse0x8dcb358();
   double synapse0x8dc55a8();
   double synapse0x8dc55d0();
   double synapse0x8dc55f8();
   double synapse0x8dc4238();
   double synapse0x8dc4038();
   double synapse0x8dc3e38();
   double synapse0x8dc3c38();
   double synapse0x8dc3a38();
   double synapse0x8dc3810();
   double synapse0x8dc3610();
   double synapse0x8dc3410();
   double synapse0x8da31e8();
   double synapse0x8dc76c0();
   double synapse0x8dc76e8();
   double synapse0x8dc7710();
   double synapse0x8dc7950();
   double synapse0x8dc7978();
   double synapse0x8dc79a0();
   double synapse0x8dc79c8();
   double synapse0x8dc79f0();
   double synapse0x8dc7a18();
   double synapse0x8dc7a40();
   double synapse0x8dc7a68();
   double synapse0x8dc7a90();
   double synapse0x8dcbdb8();
   double synapse0x8dcbde0();
   double synapse0x8dcbe08();
   double synapse0x8dcbe30();
   double synapse0x8dcbe58();
   double synapse0x8dcbe80();
   double synapse0x8dcc0a8();
   double synapse0x8dcc0d0();
   double synapse0x8dcc0f8();
   double synapse0x8dcc120();
   double synapse0x8dcc148();
   double synapse0x8dcc170();
   double synapse0x8dcc198();
   double synapse0x8dcc1c0();
   double synapse0x8dcc1e8();
   double synapse0x8dcc210();
   double synapse0x8dcc238();
   double synapse0x8dcc260();
   double synapse0x8dcc288();
   double synapse0x8dcc2b0();
   double synapse0x8dcc2d8();
   double synapse0x8dcc500();
   double synapse0x8dcc528();
   double synapse0x8dcc550();
   double synapse0x8dcc578();
   double synapse0x8dcc5a0();
   double synapse0x8dcc5c8();
   double synapse0x8dcc5f0();
   double synapse0x8dcc618();
   double synapse0x8dcc640();
   double synapse0x8dcc668();
   double synapse0x8dcc690();
   double synapse0x8dcc6b8();
   double synapse0x8dcc6e0();
   double synapse0x8dcc708();
   double synapse0x8dcc730();
   double synapse0x8dcc958();
   double synapse0x8dcc980();
   double synapse0x8dcc9a8();
   double synapse0x8dcc9d0();
   double synapse0x8dcc9f8();
   double synapse0x8dcca20();
   double synapse0x8dcca48();
   double synapse0x8dcca70();
   double synapse0x8dcca98();
   double synapse0x8dccac0();
   double synapse0x8dccae8();
   double synapse0x8dccb10();
   double synapse0x8dccb38();
   double synapse0x8dccb60();
   double synapse0x8dccb88();
   double synapse0x8dccdb0();
   double synapse0x8dccdd8();
   double synapse0x8dcce00();
   double synapse0x8dcce28();
   double synapse0x8dcce50();
   double synapse0x8dcce78();
   double synapse0x8dccea0();
   double synapse0x8dccec8();
   double synapse0x8dccef0();
   double synapse0x8dccf18();
   double synapse0x8dccf40();
   double synapse0x8dccf68();
   double synapse0x8dccf90();
   double synapse0x8dccfb8();
   double synapse0x8dccfe0();
   double synapse0x8dcd208();
   double synapse0x8dcd230();
   double synapse0x8dcd258();
   double synapse0x8dcd280();
   double synapse0x8dcd2a8();
   double synapse0x8dcd2d0();
   double synapse0x8dcd2f8();
   double synapse0x8dcd320();
   double synapse0x8dcd348();
   double synapse0x8dcd370();
   double synapse0x8dcd398();
   double synapse0x8dcd3c0();
   double synapse0x8dcd3e8();
   double synapse0x8dcd410();
   double synapse0x8dcd438();
   double synapse0x8dcd660();
   double synapse0x8dcd688();
   double synapse0x8dcd6b0();
   double synapse0x8dcd6d8();
   double synapse0x8dcd700();
   double synapse0x8dcd728();
   double synapse0x8dcd750();
   double synapse0x8dcd778();
   double synapse0x8dcd7a0();
   double synapse0x8dcd7c8();
   double synapse0x8dcd7f0();
   double synapse0x8dcd818();
   double synapse0x8dcd840();
   double synapse0x8dcd868();
   double synapse0x8dcd890();
   double synapse0x8dcdab8();
   double synapse0x8dcdae0();
   double synapse0x8dcdb08();
   double synapse0x8dcdb30();
   double synapse0x8dcdb58();
   double synapse0x8dcdb80();
   double synapse0x8dcdba8();
   double synapse0x8dcdbd0();
   double synapse0x8dcdbf8();
   double synapse0x8dcdc20();
   double synapse0x8dcdc48();
   double synapse0x8dcdc70();
   double synapse0x8dcdc98();
   double synapse0x8dcdcc0();
   double synapse0x8dcdce8();
   double synapse0x8dcdf10();
   double synapse0x8dcdf38();
   double synapse0x8dcdf60();
   double synapse0x8dcdf88();
   double synapse0x8dcdfb0();
   double synapse0x8dcdfd8();
   double synapse0x8dce000();
   double synapse0x8dce028();
   double synapse0x8dce050();
   double synapse0x8dce078();
   double synapse0x8dce0a0();
   double synapse0x8dce0c8();
   double synapse0x8dce0f0();
   double synapse0x8dce118();
   double synapse0x8dce140();
   double synapse0x8dce368();
   double synapse0x8dce390();
   double synapse0x8dce3b8();
   double synapse0x8dce3e0();
   double synapse0x8dce408();
   double synapse0x8dce430();
   double synapse0x8dce458();
   double synapse0x8dce480();
   double synapse0x8dce4a8();
   double synapse0x8dce4d0();
   double synapse0x8dce4f8();
   double synapse0x8dce520();
   double synapse0x8dce548();
   double synapse0x8dce570();
   double synapse0x8dce598();
   double synapse0x8dce7c0();
   double synapse0x8dce7e8();
   double synapse0x8dce810();
   double synapse0x8dce838();
   double synapse0x8dce860();
   double synapse0x8dce888();
   double synapse0x8dce8b0();
   double synapse0x8dce8d8();
   double synapse0x8dce900();
   double synapse0x8dce928();
   double synapse0x8dce950();
   double synapse0x8dce978();
   double synapse0x8dce9a0();
   double synapse0x8dce9c8();
   double synapse0x8dce9f0();
   double synapse0x8dcec18();
   double synapse0x8dcec40();
   double synapse0x8dcec68();
   double synapse0x8dcec90();
   double synapse0x8dcecb8();
   double synapse0x8dcece0();
   double synapse0x8dced08();
   double synapse0x8dced30();
   double synapse0x8dced58();
   double synapse0x8dced80();
   double synapse0x8dceda8();
   double synapse0x8dcedd0();
   double synapse0x8dcedf8();
   double synapse0x8dcee20();
   double synapse0x8dcee48();
   double synapse0x8dcf070();
   double synapse0x8dcf098();
   double synapse0x8dcf0c0();
   double synapse0x8dcf0e8();
   double synapse0x8dcf110();
   double synapse0x8dcf138();
   double synapse0x8dcf160();
   double synapse0x8dcf188();
   double synapse0x8dcf1b0();
   double synapse0x8dcf1d8();
   double synapse0x8dcf200();
   double synapse0x8dcf228();
   double synapse0x8dcf250();
   double synapse0x8dcf278();
   double synapse0x8dcf2a0();
   double synapse0x8dcf4c8();
   double synapse0x8dca5d8();
   double synapse0x8dca600();
   double synapse0x8dca628();
   double synapse0x8dca688();
   double synapse0x8dca6b0();
   double synapse0x8dca6d8();
   double synapse0x8dca738();
   double synapse0x8dca760();
   double synapse0x8dca788();
   double synapse0x8dca7e8();
   double synapse0x8dca810();
   double synapse0x8dca838();
   double synapse0x8dca898();
   double synapse0x8dca8c0();
   double synapse0x8dcaaf0();
   double synapse0x8dca8e8();
   double synapse0x8dca990();
   double synapse0x8dcaa40();
   double synapse0x8dcab58();
   double synapse0x8dcab80();
   double synapse0x8dcaba8();
   double synapse0x8dcac08();
   double synapse0x8dcac30();
   double synapse0x8dcac58();
   double synapse0x8dcacb8();
   double synapse0x8dcace0();
   double synapse0x8dcad08();
   double synapse0x8dcad68();
   double synapse0x8dcad90();
   double synapse0x8dcafc0();
   double synapse0x8dcadb8();
   double synapse0x8dcae60();
   double synapse0x8dcaf10();
   double synapse0x8dd06b8();
   double synapse0x8dd06e0();
   double synapse0x8dd0708();
   double synapse0x8dd0730();
   double synapse0x8dd0758();
   double synapse0x8dd0780();
   double synapse0x8dd07a8();
   double synapse0x8dd07d0();
   double synapse0x8dd07f8();
   double synapse0x8dd0820();
   double synapse0x8dd0848();
   double synapse0x8dd0a70();
   double synapse0x8dd0a98();
   double synapse0x8dd0ac0();
   double synapse0x8dcb5b0();
   double synapse0x8dcb5d8();
   double synapse0x8dcb600();
   double synapse0x8dcb628();
   double synapse0x8dcb650();
   double synapse0x8dcb678();
   double synapse0x8dcb6a0();
   double synapse0x8dcb6c8();
   double synapse0x8dcb6f0();
   double synapse0x8dcb718();
   double synapse0x8dcb740();
   double synapse0x8dcb768();
   double synapse0x8dcb990();
   double synapse0x8dcb9b8();
   double synapse0x8dcb9e0();
   double synapse0x8dcba08();
   double synapse0x8dcba30();
   double synapse0x8dcba58();
   double synapse0x8dcba80();
   double synapse0x8dcbaa8();
   double synapse0x8dcbad0();
   double synapse0x8dcbaf8();
   double synapse0x8dcbb20();
   double synapse0x8dcbb48();
   double synapse0x8dcbb70();
   double synapse0x8dcbb98();
   double synapse0x8dcbbc0();
   double synapse0x8dd1b38();
   double synapse0x8dd1b60();
   double synapse0x8dd1b88();
   double synapse0x8dd1bb0();
   double synapse0x8dd1bd8();
   double synapse0x8dd1c00();
   double synapse0x8dd1c28();
   double synapse0x8dd1c50();
   double synapse0x8dd1c78();
   double synapse0x8dd1ca0();
   double synapse0x8dd1cc8();
   double synapse0x8dd1cf0();
   double synapse0x8dd1d18();
   double synapse0x8dd1d40();
   double synapse0x8dd1d68();
   double synapse0x8dd1eb8();
   double synapse0x8dd1ee0();
   double synapse0x8dd1f08();
   double synapse0x8dd1f30();
   double synapse0x8dd1f58();
   double synapse0x8dd1f80();
   double synapse0x8dd1fa8();
   double synapse0x8dd1fd0();
   double synapse0x8dd1ff8();
   double synapse0x8dd2020();
   double synapse0x8dd2048();
   double synapse0x8dd2070();
   double synapse0x8dd2098();
   double synapse0x8dd20c0();
   double synapse0x8dd20e8();
   double synapse0x8dd2110();
   double synapse0x8dd21c0();
   double synapse0x8dd21e8();
   double synapse0x8dd2210();
   double synapse0x8dd2238();
   double synapse0x8dd2260();
   double synapse0x8dd2288();
   double synapse0x8dd22b0();
   double synapse0x8dd22d8();
   double synapse0x8dd2300();
   double synapse0x8dd2328();
   double synapse0x8dd2350();
   double synapse0x8dd2378();
   double synapse0x8dd23a0();
   double synapse0x8dd23c8();
   double synapse0x8dd23f0();
   double synapse0x8dd2418();
   double synapse0x8dd2138();
   double synapse0x8dd2160();
   double synapse0x8dd2188();
   double synapse0x8dd2548();
   double synapse0x8dd2570();
   double synapse0x8dd2778();
   double synapse0x8dd27a0();
   double synapse0x8dd27c8();
   double synapse0x8dd27f0();
   double synapse0x8dd2818();
   double synapse0x8dd2840();
   double synapse0x8dd2868();
   double synapse0x8dd2890();
   double synapse0x8dd28b8();
   double synapse0x8dd28e0();
   double synapse0x8dd2908();
   double synapse0x8dd2930();
   double synapse0x8dd2958();
   double synapse0x8dd2980();
   double synapse0x8dd29a8();
   double synapse0x8dd29d0();
   double synapse0x8dd2a80();
   double synapse0x8dd2aa8();
   double synapse0x8dd2ad0();
   double synapse0x8dd2af8();
   double synapse0x8dd2b20();
   double synapse0x8dd2b48();
   double synapse0x8dd2b70();
   double synapse0x8dd2b98();
   double synapse0x8dd2bc0();
   double synapse0x8dd2be8();
   double synapse0x8dd2c10();
   double synapse0x8dd2c38();
   double synapse0x8dd2c60();
   double synapse0x8dd2c88();
   double synapse0x8dd2cb0();
   double synapse0x8dd2cd8();
   double synapse0x8dd29f8();
   double synapse0x8dd2a20();
   double synapse0x8dd2a48();
   double synapse0x8dd2e08();
   double synapse0x8dd2e30();
};

#endif // NNPerpElectronAngleCorrectionBCPDir_h

