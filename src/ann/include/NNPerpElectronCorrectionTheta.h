#ifndef NNPerpElectronCorrectionTheta_h
#define NNPerpElectronCorrectionTheta_h

class NNPerpElectronCorrectionTheta {
public:
   NNPerpElectronCorrectionTheta() {}
   virtual ~NNPerpElectronCorrectionTheta() {}
   double Value(int index, double in0, double in1, double in2, double in3, double in4, double in5, double in6);
   double Value(int index, double* input);
private:
   double input0;
   double input1;
   double input2;
   double input3;
   double input4;
   double input5;
   double input6;
   double neuron0x8df6d10();
   double neuron0x8df6ec8();
   double neuron0x8df70c8();
   double neuron0x8df72c8();
   double neuron0x8df74c8();
   double neuron0x8df76c8();
   double neuron0x8df78e0();
   double input0x8df7c20();
   double neuron0x8df7c20();
   double input0x8df7ef0();
   double neuron0x8df7ef0();
   double input0x8df8208();
   double neuron0x8df8208();
   double input0x8df85a8();
   double neuron0x8df85a8();
   double input0x8df8878();
   double neuron0x8df8878();
   double input0x8df8c48();
   double neuron0x8df8c48();
   double input0x8df8f60();
   double neuron0x8df8f60();
   double input0x8df9290();
   double neuron0x8df9290();
   double input0x8df95c0();
   double neuron0x8df95c0();
   double input0x8df98f0();
   double neuron0x8df98f0();
   double input0x8df9de8();
   double neuron0x8df9de8();
   double input0x8dfa100();
   double neuron0x8dfa100();
   double input0x8dfa430();
   double neuron0x8dfa430();
   double input0x8dfa760();
   double neuron0x8dfa760();
   double input0x8dfaa90();
   double neuron0x8dfaa90();
   double input0x8dfadc0();
   double neuron0x8dfadc0();
   double input0x8dfb4a8();
   double neuron0x8dfb4a8();
   double input0x8dfb700();
   double neuron0x8dfb700();
   double input0x8dfb958();
   double neuron0x8dfb958();
   double input0x8df9be0();
   double neuron0x8df9be0();
   double input0x8dfbfa8();
   double neuron0x8dfbfa8();
   double input0x8dfc2c0();
   double neuron0x8dfc2c0();
   double input0x8dfc5f0();
   double neuron0x8dfc5f0();
   double input0x8dfc920();
   double neuron0x8dfc920();
   double input0x8dfdc08();
   double neuron0x8dfdc08();
   double input0x8dfdf20();
   double neuron0x8dfdf20();
   double synapse0x8df7dd8();
   double synapse0x8df7e00();
   double synapse0x8df7e28();
   double synapse0x8df7e50();
   double synapse0x8df7e78();
   double synapse0x8df7ea0();
   double synapse0x8df7ec8();
   double synapse0x8df80f0();
   double synapse0x8df8118();
   double synapse0x8df8140();
   double synapse0x8df8168();
   double synapse0x8df8190();
   double synapse0x8df81b8();
   double synapse0x8df81e0();
   double synapse0x8df8408();
   double synapse0x8df8430();
   double synapse0x8df8458();
   double synapse0x8df8508();
   double synapse0x8df8530();
   double synapse0x8df8558();
   double synapse0x8df8580();
   double synapse0x8df8760();
   double synapse0x8df8788();
   double synapse0x8df87b0();
   double synapse0x8df87d8();
   double synapse0x8df8800();
   double synapse0x8df8828();
   double synapse0x8df8850();
   double synapse0x8df8a78();
   double synapse0x8df8aa0();
   double synapse0x8df8ac8();
   double synapse0x8df8af0();
   double synapse0x8df8b18();
   double synapse0x8d082b0();
   double synapse0x8df8480();
   double synapse0x8df8e48();
   double synapse0x8df8e70();
   double synapse0x8df8e98();
   double synapse0x8df8ec0();
   double synapse0x8df8ee8();
   double synapse0x8df8f10();
   double synapse0x8df8f38();
   double synapse0x8df9178();
   double synapse0x8df91a0();
   double synapse0x8df91c8();
   double synapse0x8df91f0();
   double synapse0x8df9218();
   double synapse0x8df9240();
   double synapse0x8df9268();
   double synapse0x8df94a8();
   double synapse0x8df94d0();
   double synapse0x8df94f8();
   double synapse0x8df9520();
   double synapse0x8df9548();
   double synapse0x8df9570();
   double synapse0x8df9598();
   double synapse0x8df97d8();
   double synapse0x8df9800();
   double synapse0x8df9828();
   double synapse0x8df9850();
   double synapse0x8df9878();
   double synapse0x8df98a0();
   double synapse0x8df98c8();
   double synapse0x8df9b90();
   double synapse0x8df9bb8();
   double synapse0x8d19540();
   double synapse0x8d195f0();
   double synapse0x8d191b8();
   double synapse0x8d08260();
   double synapse0x8d08288();
   double synapse0x8df9fe8();
   double synapse0x8dfa010();
   double synapse0x8dfa038();
   double synapse0x8dfa060();
   double synapse0x8dfa088();
   double synapse0x8dfa0b0();
   double synapse0x8dfa0d8();
   double synapse0x8dfa318();
   double synapse0x8dfa340();
   double synapse0x8dfa368();
   double synapse0x8dfa390();
   double synapse0x8dfa3b8();
   double synapse0x8dfa3e0();
   double synapse0x8dfa408();
   double synapse0x8dfa648();
   double synapse0x8dfa670();
   double synapse0x8dfa698();
   double synapse0x8dfa6c0();
   double synapse0x8dfa6e8();
   double synapse0x8dfa710();
   double synapse0x8dfa738();
   double synapse0x8dfa978();
   double synapse0x8dfa9a0();
   double synapse0x8dfa9c8();
   double synapse0x8dfa9f0();
   double synapse0x8dfaa18();
   double synapse0x8dfaa40();
   double synapse0x8dfaa68();
   double synapse0x8dfaca8();
   double synapse0x8dfacd0();
   double synapse0x8dfacf8();
   double synapse0x8dfad20();
   double synapse0x8dfad48();
   double synapse0x8dfad70();
   double synapse0x8dfad98();
   double synapse0x8dfafd8();
   double synapse0x8dfb088();
   double synapse0x8dfb138();
   double synapse0x8dfb1e8();
   double synapse0x8dfb298();
   double synapse0x8dfb348();
   double synapse0x8dfb3f8();
   double synapse0x8dfb5e8();
   double synapse0x8dfb610();
   double synapse0x8dfb638();
   double synapse0x8dfb660();
   double synapse0x8dfb688();
   double synapse0x8dfb6b0();
   double synapse0x8dfb6d8();
   double synapse0x8dfb840();
   double synapse0x8dfb868();
   double synapse0x8dfb890();
   double synapse0x8dfb8b8();
   double synapse0x8dfb8e0();
   double synapse0x8dfb908();
   double synapse0x8dfb930();
   double synapse0x8dfbb28();
   double synapse0x8dfbb50();
   double synapse0x8dfbb78();
   double synapse0x8df84a8();
   double synapse0x8df84d0();
   double synapse0x8dd6ee8();
   double synapse0x8dd6f10();
   double synapse0x8df9db0();
   double synapse0x8df8b40();
   double synapse0x8df8b68();
   double synapse0x8df8b90();
   double synapse0x8df8bb8();
   double synapse0x8df8be0();
   double synapse0x8df8c08();
   double synapse0x8dfc1a8();
   double synapse0x8dfc1d0();
   double synapse0x8dfc1f8();
   double synapse0x8dfc220();
   double synapse0x8dfc248();
   double synapse0x8dfc270();
   double synapse0x8dfc298();
   double synapse0x8dfc4d8();
   double synapse0x8dfc500();
   double synapse0x8dfc528();
   double synapse0x8dfc550();
   double synapse0x8dfc578();
   double synapse0x8dfc5a0();
   double synapse0x8dfc5c8();
   double synapse0x8dfc808();
   double synapse0x8dfc830();
   double synapse0x8dfc858();
   double synapse0x8dfc880();
   double synapse0x8dfc8a8();
   double synapse0x8dfc8d0();
   double synapse0x8dfc8f8();
   double synapse0x8dfcaa8();
   double synapse0x8dfdb18();
   double synapse0x8dfdb40();
   double synapse0x8dfdb68();
   double synapse0x8dfdb90();
   double synapse0x8dfdbb8();
   double synapse0x8dfdbe0();
   double synapse0x8dfde08();
   double synapse0x8dfde30();
   double synapse0x8dfde58();
   double synapse0x8dfde80();
   double synapse0x8dfdea8();
   double synapse0x8dfded0();
   double synapse0x8dfdef8();
   double synapse0x8df7bf8();
   double synapse0x8dfe048();
   double synapse0x8dfe070();
   double synapse0x8dfe098();
   double synapse0x8dfe0c0();
   double synapse0x8dfe0e8();
   double synapse0x8dfe110();
   double synapse0x8dfe138();
   double synapse0x8dfe160();
   double synapse0x8dfe188();
   double synapse0x8dfe1b0();
   double synapse0x8dfe1d8();
   double synapse0x8dfe200();
   double synapse0x8dfe228();
   double synapse0x8dfe250();
   double synapse0x8dfe278();
   double synapse0x8dfe328();
   double synapse0x8dfe350();
   double synapse0x8dfe378();
   double synapse0x8dfe3a0();
   double synapse0x8dfe3c8();
   double synapse0x8dfe3f0();
   double synapse0x8dfe418();
   double synapse0x8dfe440();
   double synapse0x8dfe468();
};

#endif // NNPerpElectronCorrectionTheta_h

