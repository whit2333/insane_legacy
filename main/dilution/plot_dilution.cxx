Int_t plot_dilution(Int_t num=1){

// W-bin, x-bin, counts Z=1, counts NH3(+Al), df, df-error, df w/rc, df(w/rc)-error 

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TSQLServer * db = dbman->GetMySQLConnection(); TSQLServer::Connect("mysql://quarks.temple.edu","sane","secret");
//   string line;
   const int N_rows = 8;
   double row[N_rows];
   int    pf_run_number = 72991;  
   double beam_energy = 4.7;
   double q_squared_bin = 2.222;
   const char * file = Form("dilution/updated_dfs/dfQ%dWx_%1.1f_para_%d_0.658_top.dat",(int)(q_squared_bin*1000),beam_energy,pf_run_number);
   std::cout << " FILE NAME: " << file << "\n";

   TString query_start = "\
   INSERT INTO`SANE`.`dilution_factors` ( \
		    `q_squared_bin` ,\
		     `beam_energy` ,\
		      `W_bin` ,\
		       `x_bin` ,\
		        `counts_proton` ,\
			 `counts_ammonia` ,\
			  `df` , \
			   `df_error` , \
			    `df_w_rc` , \
			     `df_w_rc_error`, `pf_run_number`\ 
		   ) \
	   VALUES ( ";
   TString query_values = "'2','','','','','','','','','','' ";
   TString query_end    = "  ); ";

   

   ifstream myfile (file);
   if (myfile.is_open()) {
     while ( myfile.good() )
     {
	query_values = "";
        query_values += Form("'%f','%f'",q_squared_bin,beam_energy);

	for(int i =0; i < N_rows ;i++) {
	   myfile >> row[i];
	   query_values += Form(",'%f'",row[0]);
//	   std::cout << i << "=" << row[i] << "\n";
	}
        query_values += Form(",'%d'",pf_run_number);
	if(db) {
           TString query = query_start + query_values + query_end ;
//	   std::cout << query.Data() << "\n"; 
	   db->Query(query.Data());
	}	
	
        //getline (myfile,line);
        //cout << line << endl;
     
     }
     myfile.close();
   }
   else cout << "Unable to open file"; 

   return 0;
}
