Int_t run_dilution_perp59(Int_t runNumber = 72761, Int_t aNumber = 508) {

   TFile * f = new TFile(Form("data/binned_asymmetries_perp59_%d.root",aNumber),"READ");
   TList * l = (TList*)gROOT->FindObject(Form("binned-asym-%d",runNumber));
   if(!l){
      f->ls();
      return -22;
   }

   gSystem->mkdir(Form("results/dilution/%d",aNumber));

   TMultiGraph * mg  = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   for(Int_t jj=0; jj<l->GetEntries(); jj++) {

      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)l->At(jj);

      TH1F * hD = asym->fDilutionVsx;
      TH1F * hA = asym->fAsymmetryVsx;

      TGraphErrors * gr = new TGraphErrors(hD);

      if( jj%5 == 4 ) {
         hA->SetLineColor(1);
         hA->SetMarkerColor(1);
      }

      gr->SetMarkerColor(hA->GetMarkerColor());
      gr->SetLineColor(hA->GetLineColor());


      if(jj<4) {
         gr->SetMarkerStyle(20);
         mg->Add(gr,"p");
      } else {

         if(jj<10){
            gr->SetMarkerStyle(22);
         }else {
            gr->SetMarkerStyle(23);
         }

         mg2->Add(gr,"p");
      }

   }

   TCanvas * c = new TCanvas();
   //c->Divide(1,2);

   //c->cd(1);
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(0.0,0.3);
   mg->Draw("a");

   c->SaveAs(Form("results/dilution/%d/run_dilution_perp59_%d.png",aNumber,runNumber));

   TCanvas * c2 = new TCanvas();
   mg2->Draw("a");
   mg2->GetYaxis()->SetRangeUser(0.0,0.3);
   mg2->Draw("a");

   c2->SaveAs(Form("results/dilution/%d/run_dilution_perp59_2_%d.png",aNumber,runNumber));

   return 0;
}
