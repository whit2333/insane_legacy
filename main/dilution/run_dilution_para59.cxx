Int_t run_dilution_para59(Int_t runNumber = 72925, Int_t aNumber = 2405) {

   TFile * f = new TFile(Form("data/binned_asymmetries_para59_%d.root",aNumber),"READ");
   TList * l = (TList*)gROOT->FindObject(Form("binned-asym-%d",runNumber));
   if(!l) return -22;

   gSystem->mkdir(Form("results/dilution/%d",aNumber));

   TMultiGraph * mg  = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg_W  = new TMultiGraph();
   TMultiGraph * mg2_W = new TMultiGraph();

   for(Int_t jj=0; jj<l->GetEntries(); jj++) {

      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)l->At(jj);

      // --------------------------------------
      // vs x
      TH1F * hD = asym->fDilutionVsx;
      TH1F * hA = asym->fAsymmetryVsx;

      TGraphErrors * gr = new TGraphErrors(hD);
      for( int j = gr->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt < 0.00001 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      if( jj%5 == 4 ) {
         hA->SetLineColor(1);
         hA->SetMarkerColor(1);
      }
      gr->SetMarkerColor(hA->GetMarkerColor());
      gr->SetLineColor(hA->GetLineColor());
      if(jj<4) {
         gr->SetMarkerStyle(20);
         mg->Add(gr,"p");
      } else {
         if(jj<10){
            gr->SetMarkerStyle(22);
         }else {
            gr->SetMarkerStyle(23);
         }
         mg2->Add(gr,"p");
      }

      // --------------------------------------
      // vs W
      TH1F * hD_W = asym->fDilutionVsW;
      TH1F * hA_W = asym->fAsymmetryVsW;

      gr = new TGraphErrors(hD_W);
      for( int j = gr->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt < 0.00001 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      if( jj%5 == 4 ) {
         hA->SetLineColor(1);
         hA->SetMarkerColor(1);
      }
      gr->SetMarkerColor(hA->GetMarkerColor());
      gr->SetLineColor(hA->GetLineColor());
      if(jj<4) {
         gr->SetMarkerStyle(20);
         mg_W->Add(gr,"p");
      } else {
         if(jj<10){
            gr->SetMarkerStyle(22);
         }else {
            gr->SetMarkerStyle(23);
         }
         mg2_W->Add(gr,"p");
      }
   }

   // -----------------------------------------------
   //
   TCanvas * c = new TCanvas();
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(0.0,0.3);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetXaxis()->SetTitle("x");

   c->SaveAs(Form("results/dilution/%d/run_dilution_para59_%d.png",aNumber,runNumber));
   c->SaveAs(Form("results/dilution/%d/run_dilution_para59_%d.pdf",aNumber,runNumber));

   // -----------------------------------------------
   //
   c = new TCanvas();
   mg_W->Draw("a");
   mg_W->GetYaxis()->SetRangeUser(0.0,0.3);
   mg_W->GetXaxis()->CenterTitle(true);
   mg_W->GetXaxis()->SetTitle("W (GeV)");

   c->SaveAs(Form("results/dilution/%d/run_dilution_para59_W_%d.png",aNumber,runNumber));
   c->SaveAs(Form("results/dilution/%d/run_dilution_para59_W_%d.pdf",aNumber,runNumber));

   // -----------------------------------------------
   //
   TCanvas * c2 = new TCanvas();
   mg2->Draw("a");
   mg2->GetYaxis()->SetRangeUser(0.0,0.3);
   mg2->GetXaxis()->CenterTitle(true);
   mg2->GetXaxis()->SetTitle("x");

   c2->SaveAs(Form("results/dilution/%d/run_dilution_para59_2_%d.png",aNumber,runNumber));
   c2->SaveAs(Form("results/dilution/%d/run_dilution_para59_2_%d.pdf",aNumber,runNumber));

   return 0;
}
