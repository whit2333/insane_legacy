/*!  Get the dilution factors for a run.
 *   This function builds InSANEDilutionFactors and creates an InSANEDilutionFunction object.
 */  
Int_t get_dilution(Int_t run_number=72999, Double_t q2_bin=4.066){

   /// Gets all data from a database. 
   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TSQLServer * db = dbman->GetMySQLConnection(); //TSQLServer::Connect("mysql://quarks.temple.edu","sane","secret");
   TString  sql = Form(" SELECT beam_energy,target_angle FROM SANE.run_info WHERE run_number=%d ", run_number);

   TSQLResult * res = 0;
   TSQLRow *    row = 0;
   double beam_energy = 0.0;
   int target_angle = 0;
   
   if(db) res = db->Query(sql.Data());
   if(res) {
   //   res->Print();
      if(res->GetRowCount() > 0) {
         row = res->Next();
	 beam_energy  = atof(row->GetField(0))/1000.0; 
	 target_angle = atoi(row->GetField(1)); 
	 std::cout << " Energy = " << atof(row->GetField(0))/1000.0 << "  target_angle=" << target_angle << "\n";
      } 
   }
   
   if(!res) { std::cout << " run not found \n" ; return(-1);} 
   int pf_run_number = 0;

/*   sql = Form(" SELECT run_number FROM SANE.packing_fractions WHERE first_run<=%d AND last_run>=%d ;",run_number,run_number);
   if(db) res = db->Query(sql.Data());
   std::cout << sql.Data() << "\n";
   if(res) {
      res->Print();
      if( row = res->Next() ) {
         for(int i = 0;i<res->GetFieldCount(); i++){
	    std::cout << res->GetFieldName(i) << " = " << row->GetField(i) << "\n";
	    pf_run_number = atoi(row->GetField(0));
	 }
      } 
   }
*/

   /// Get the dilution factors now that we know which beam energy, target angle, and Q2 bin we want. 
   ///  

   InSANEDilutionFunction * dilution_func = new InSANEDilutionFunction();
   dilution_func->fQ2_bin = q2_bin; 

   InSANEDilutionFactor * df = 0;

   //sql = Form(" SELECT df,df_error FROM SANE.dilution_factors WHERE ABS(beam_energy-%f)<0.2",beam_energy);
   if(q2_bin != 0.0) 
      sql = Form(" SELECT x_bin,W_bin,df,df_error FROM SANE.dilution_factors WHERE ABS(beam_energy-%f)<0.3 AND target_angle=%d AND q_squared_bin=%f", beam_energy,target_angle,q2_bin);
   else  sql = Form(" SELECT x_bin,W_bin,df,df_error FROM SANE.dilution_factors WHERE ABS(beam_energy-%f)<0.2 AND target_angle=%d ", beam_energy,target_angle);

   if(db){
      res = db->Query(sql.Data());
   //   if(res->GetRowCount() == 0) {
   //      sql = Form(" SELECT df,df_error FROM SANE.dilution_factors WHERE pf_run_number<%d ", run_number);
   //      res = db->Query(sql.Data());
   //   }
   }
   std::cout << sql.Data() << "\n";
   if(res) {
      dilution_func->SetNBins( res->GetRowCount() ) ;
      //res->Print();
      while( row = res->Next() ) {
         /// Create and set all the values for this kinematic point.
	 df = new InSANEDilutionFactor();
	 df->Setx(atof(row->GetField(0))); 
	 df->SetW(atof(row->GetField(1))); 
	 df->SetDilution(atof(row->GetField(2))); 
	 df->SetDilutionErr(atof(row->GetField(3))); 
         df->Dump();

	 dilution_func->Add(df);
         
	 for(int i = 0;i<res->GetFieldCount(); i++){
	    std::cout << res->GetFieldName(i) << " = " << row->GetField(i) << "  ";
	 }
         std::cout << "\n __________________________________________________________________________ \n";
      } 
   }

   dilution_func->FitGraphs();

   TCanvas * c = new TCanvas();
   c->Divide(3,1);
   c->cd(1);
   dilution_func->fGraph->SetMarkerStyle(20);
   dilution_func->fGraph->Draw("P");
   c->cd(2);
   dilution_func->fDfVsW->SetMarkerStyle(21);
   dilution_func->fDfVsW->Draw("AEP");
   c->cd(3);
   dilution_func->fDfVsx->SetMarkerStyle(22);
   dilution_func->fDfVsx->Draw("AEP");

   std::cout << " df(x=0.37) = " << dilution_func->GetXFitResult(0.37) << " \n";
   std::cout << " df(W=2.15)  = " << dilution_func->GetWFitResult(2.15) << " \n";

   c->SaveAs("results/target/get_diltion.png");

   return(0);

   ///////////////////////////////////////////////////
   const int N_rows = 8;
   double row[N_rows];
   int    pf_run_number = 72991;  
   double beam_energy = 4.7;
   double q_squared_bin = 2.222;
   const char * file = Form("dilution/updated_dfs/dfQ%dWx_%1.1f_para_%d_0.658_top.dat",(int)(q_squared_bin*1000),beam_energy,pf_run_number);
   std::cout << " FILE NAME: " << file << "\n";

   

   TString query_start = "\
   INSERT INTO`SANE`.`dilution_factors` ( \
		    `q_squared_bin` ,\
		     `beam_energy` ,\
		      `W_bin` ,\
		       `x_bin` ,\
		        `counts_proton` ,\
			 `counts_ammonia` ,\
			  `df` , \
			   `df_error` , \
			    `df_w_rc` , \
			     `df_w_rc_error`, `pf_run_number`\ 
		   ) \
	   VALUES ( ";
   TString query_values = "'2','','','','','','','','','','' ";
   TString query_end    = "  ); ";

   

   ifstream myfile (file);
   if (myfile.is_open()) {
     while ( myfile.good() )
     {
	query_values = "";
        query_values += Form("'%f','%f'",q_squared_bin,beam_energy);

	for(int i =0; i < N_rows ;i++) {
	   myfile >> row[i];
	   query_values += Form(",'%f'",row[0]);
//	   std::cout << i << "=" << row[i] << "\n";
	}
        query_values += Form(",'%d'",pf_run_number);
	if(db) {
           TString query = query_start + query_values + query_end ;
//	   std::cout << query.Data() << "\n"; 
	   db->Query(query.Data());
	}	
	
        //getline (myfile,line);
        //cout << line << endl;
     
     }
     myfile.close();
   }
   else cout << "Unable to open file"; 

   return 0;
}
