Int_t pi0_dilution(){

   UVAPolarizedAmmoniaTarget * target = new UVAPolarizedAmmoniaTarget();
   target->Print();


   TParticle * beam = new TParticle();
   TParticle * scat = new TParticle();
   beam->SetPdgCode(11);
   scat->SetPdgCode(111);
   beam->SetMomentum(0.0,0.0,5.9,5.9);



   InSANEDilutionFromTarget * dilution = new InSANEDilutionFromTarget();
   dilution->SetTarget(target);

   for(int i = 0 ; i<20; i++ ) {
      double theta_pi = 30.0*degree;
      double p_pi     = 0.5 + 0.1*double(i);
      double E_pi     = TMath::Sqrt(p_pi*p_pi + 0.135*0.135);
      scat->SetMomentum(p_pi*TMath::Sin(theta_pi),0.0,p_pi*TMath::Cos(theta_pi),E_pi);
      double d0 = dilution->GetDilution(beam,scat);
      scat->Print();
      std::cout << "dilution = " << d0 <<std::endl;
   }

   return 0;
}
