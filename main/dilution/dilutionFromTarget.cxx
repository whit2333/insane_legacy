void dilutionFromTarget(Int_t runNumber=72994) {

   double q2_bins[4] = {2.22,3.00,4.066  ,5.49};

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   InSANERun * run = runManager->GetCurrentRun();


//    InSANEFunctionManager->

   Double_t packingFraction = 0.65;

   InSANETargetMaterial * matH3 = new InSANETargetMaterial("H3","H3 in Ammonia",1,1);
   matH3->fLength          = 3.0;    //cm
   matH3->fDensity         = 2.0*0.0770; // g/cm3
   matH3->fIsPolarized     = true;
   matH3->fPackingFraction = packingFraction;

   InSANETargetMaterial * matN14 = new InSANETargetMaterial("14N","14N in Ammonia",7,14);
   matN14->fLength          = 3.0;    //cm
   matN14->fDensity         = 2.0*0.3576; // g/cm3
   matN14->fPackingFraction = packingFraction;

   InSANETargetMaterial * matHe = new InSANETargetMaterial("He","LHe around Ammonia beads",2,4);
   matHe->fLength          = 3.0;    //cm
   matHe->fDensity         = 0.1450; // g/cm3
   matHe->fPackingFraction = 1.0 - packingFraction;

   InSANETargetMaterial * matLHe = new InSANETargetMaterial("LHe","Liquid Helium",2,4);
   matLHe->fLength          = 0.50000*2.0;    //cm
   matLHe->fDensity         = 0.1450; // g/cm3

   InSANETargetMaterial * matAlEndcap = new InSANETargetMaterial("Al-endcap","Aluminum target endcp",13,27);
   matAlEndcap->fLength          = 2.0*0.00381 ;    //cm
   matAlEndcap->fDensity         = 2.7; // g/cm3

   InSANETargetMaterial * matAlTail = new InSANETargetMaterial("Al-tail","Aluminum target tail",13,27);
   matAlTail->fLength          = 2.0*0.01016  ;    //cm
   matAlTail->fDensity         = 2.7; // g/cm3

   InSANETargetMaterial * matAl4kShield = new InSANETargetMaterial("Al-4kshield","Aluminum target 4K shield",13,27);
   matAl4kShield->fLength          = 2.0*0.00254  ;    //cm
   matAl4kShield->fDensity         = 2.7; // g/cm3

   InSANETargetMaterial * matCu = new InSANETargetMaterial("Cu","NMR-Cu ",29,64);
   matCu->fLength          = 0.00050   ;    //cm
   matCu->fDensity         = 8.9600 ; // g/cm3

   InSANETargetMaterial * matNi = new InSANETargetMaterial("Ni","NMR-Ni  ",28,59);
   matCu->fLength          = 0.00022   ;    //cm
   matCu->fDensity         = 8.9020  ; // g/cm3

   /// UVA Polarized Target During SANE
   InSANETarget * target = new InSANETarget("UVATarget","UVA Polarized Target");
   target->AddMaterial(matH3);
   target->AddMaterial(matN14);
   target->AddMaterial(matLHe);
   target->AddMaterial(matHe);
   target->AddMaterial(matAlEndcap);
   target->AddMaterial(matAlTail);
   target->AddMaterial(matAl4kShield);
   target->AddMaterial(matCu);
   target->AddMaterial(matNi);

   TCanvas * c = new TCanvas();

   InSANEDilutionFromTarget * dilution = new InSANEDilutionFromTarget();
   dilution->fQ2_bin =q2_bins[0] ;
   dilution->SetTarget(target);

   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("");

   InSANEDilutionFunction * dfunc = 0;

   const int NPoints = 50;

   TGraph * g[4];
   TMultiGraph * mg = new TMultiGraph();
   for(int k= 0;k<4;k++){
   
      g[k] = new TGraph(NPoints);
      g[k]->SetNameTitle(Form("dilution-%.3f",q2_bins[k]),Form("dilution %.3f",q2_bins[k]));
      dilution->fQ2_bin = q2_bins[k];
      Double_t x = 0.2;
      for(int i = 0;i<NPoints;i++){
         x += 0.7/((double)NPoints);
         g[k]->SetPoint(i,x,dilution->GetDilution(x,dilution->fQ2_bin) );
         //std::cout << " x = " << x << ", dilution = " << dilution->GetDilution(x,dilution->fQ2_bin) << "\n";

      }

      g[k]->SetMarkerStyle(20);
      g[k]->SetMarkerSize(1.5);
      g[k]->SetMarkerColor(2000+k);
      g[k]->SetLineColor(2000+k);
      mg->Add(g[k],"lp");
      
      dfunc = 0;//  (InSANEDilutionFunction*)run->fDilution.FindObject(Form("df-%1.2f",q2_bins[k]));
      if(dfunc) if(dfunc->fDfVsx) {
          dfunc->fDfVsx->SetLineColor(2000+k);
          mg->Add(dfunc->fDfVsx,"p");
      }

      leg->AddEntry(g[k],g[k]->GetTitle(),"lp");
   }  

   mg->Draw("a");
   mg->SetMaximum(0.3);
   mg->Draw("a");
   leg->Draw();

   c->SaveAs("results/dilutionFromTarget.png");
   c->SaveAs("results/dilutionFromTarget.svg");
   c->SaveAs("results/dilutionFromTarget.pdf");
}

