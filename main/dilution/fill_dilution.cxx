Int_t fill_dilution(Int_t num=1){

// W-bin, x-bin, counts Z=1, counts NH3(+Al), df, df-error, df w/rc, df(w/rc)-error 


   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TSQLServer * db = dbman->GetMySQLConnection(); //TSQLServer::Connect("mysql://quarks.temple.edu","sane","secret");
//   string line;
   const int N_rows = 10;
   double row[N_rows];
   
   // For some reason in the file list only para->top and perp->bot ???
   double q2_bins[4] = {2.22,3.00,4.066  ,5.49};
   int runs[4] = {72991,72247,72959,72417};
   double pfs_used[4] = {0.658,0.682,0.625, 0.628}; 
   // Also para/perp alternate with the indicies starting with para...

for(int q = 0;q<4; q++) {
for(int k = 0;k<4; k++) {

   int    pf_run_number = runs[k] ;  
   double q_squared_bin = q2_bins[q];
   double beam_energy = 4.7;
   if(k>1) beam_energy = 5.9;
   TString file = "";
   int target_angle = 0;
   if( k%2 == 0 ) { // Para/top
      target_angle = 180;   
      file = Form("dilution/updated_dfs/dfQ2%dWx_%1.1f_para_%d_%1.3f_top.dat",
                  (int)(q_squared_bin*100),
		  beam_energy,
		  pf_run_number,
	          pfs_used[k]	 );
   } else { 
      target_angle = 80;   
      file = Form("dilution/updated_dfs/dfQ2%dWx_%1.1f_perp_%d_%1.3f_bot.dat",
                  (int)(q_squared_bin*100),
		  beam_energy,
		  pf_run_number,
	          pfs_used[k]	 );
   }


   std::cout << " FILE NAME: " << file.Data() << "\n";

   TString query_start = "\
   INSERT INTO`SANE`.`dilution_factors` ( \
`q_squared_bin` ,\
`beam_energy` ,\
`W_bin` ,\
`x_bin` ,\
`counts_proton` ,\
`counts_ammonia` ,`counts_proton_rc`,`counts_ammonia_rc`,\
`df` , \
`df_error` , \
`df_w_rc` , \
`df_w_rc_error`, `pf_run_number`,`target_angle`\ 
		   ) \
	   VALUES ( ";
   TString query_values = "'2','','','','','','','','','','' ";
   TString query_end    = "  ); ";

   

   ifstream myfile (file.Data());
   if (myfile.is_open()) {
     while ( myfile.good() )
     {
	query_values = "";
        query_values += Form("'%f','%f'",q_squared_bin,beam_energy);

	for(int i =0; i < N_rows ;i++) {
	   myfile >> row[i];
	   query_values += Form(",'%f'",row[i]);
           if( myfile.eof() ) break; 
//	   std::cout << i << "=" << row[i] << "\n";
	}
        if( myfile.eof() ) break; 
        query_values += Form(",'%d'",pf_run_number);
        query_values += Form(",'%d'",target_angle);
	if(db) {
           TString query = query_start + query_values + query_end ;
//	   std::cout << query.Data() << "\n"; 
	   db->Query(query.Data());
	}	
	
        //getline (myfile,line);
        //cout << line << endl;
     }
     myfile.close();
   }
   else cout << "Unable to open file \n"; 

}
}
   return 0;
}
