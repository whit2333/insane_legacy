Int_t calc_asym_dilution_perp47(Int_t runnumber = 72825, Int_t fileVersion = 5, Double_t E_min = 800.0) { 

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun        * run = rman->GetCurrentRun();
   InSANERunSummary  rSum =  run->GetRunSummary();

   InSANEFunctionManager * fman  = InSANEFunctionManager::GetManager();
   fman->SetBeamEnergy(4.7);

   // ------------------------------------------------
   // Create the target and dilution from target
   Double_t pf                      = run->fPackingFraction;
   if(pf>1.0) pf = pf/100.0;
   InSANEDilutionFromTarget * df    = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget("UVaTarget","UVa Pol. Target",pf);
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   // ------------------------------------------------
   // Open asymmetry file created by binned_asymmetries_perp47_luc_v4.cxx
   TFile * f = new TFile(Form("data/asym-%d/binned_asymmetries_perp47_%d.root",fileVersion,runnumber),"UPDATE");
   f->cd();

   // ------------------------------------------------
   // Get the asymmetries.
   TList *  fAsymmetries = (TList*)gROOT->FindObject(Form("binned-asym-%d",runnumber));
   if( !fAsymmetries ) return -1;

   // ------------------------------------------------
   // Calculate the asymmetries with new dilution configuration
   std::cout << " Calculating Asymmetries... \n";
   for(int j = 0;j<fAsymmetries->GetEntries();j++) {
      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      asy->SetRunNumber(runnumber);
      asy->SetRunSummary(&rSum);
      asy->SetDilutionFromTarget(df); // setting to zero uses mean df already calculated (very fast)
      asy->CalculateAsymmetries();
   }

   for(int j = 0;j<fAsymmetries->GetEntries();j++) {
      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      std::ofstream fileout(Form("plots/%d/binned_asym1-%d.dat",runnumber,j),std::ios_base::trunc );
      asy->PrintBinnedTableHead(fileout) ;
      asy->PrintBinnedTable(fileout) ;
      asy->Print();
      asy->PrintBinnedTableHead(std::cout) ;
      asy->PrintBinnedTable(std::cout) ;
   }
 
   rSum.Print();

   f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 

   return 0;
}
