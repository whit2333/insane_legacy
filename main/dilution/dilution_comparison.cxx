Int_t dilution_comparison(Int_t runNumber=72999) {

   double q2_bins[4] = {2.22,3.00,4.066  ,5.49};
   //double q2_bins_0[4] = {2.0,2.500,3.566  ,4.5};

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   InSANERun * run = runManager->GetCurrentRun();

   InSANEFunctionManager * fman  = InSANEFunctionManager::GetManager();
   //fman->SetBeamEnergy(5.89);
   fman->SetBeamEnergy(4.7);
   //fman->CreateSFs(6);
   //fman->CreatePolSFs(6);
   // ----------------------------------------------
   // Setup target and dilution 

   Double_t pf = run->fPackingFraction;

   //pf=0.5;
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget("UVaTarget","UVa Pol. Target",pf);
   targ->SetPackingFraction(pf);
   //targ->SetPackingFraction(0.3);

   InSANEDilutionFromTarget * dilution = new InSANEDilutionFromTarget();
   dilution->SetTarget(targ);
   dilution->fQ2_bin = q2_bins[0] ;

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TSQLServer * db = dbman->GetMySQLConnection(); 

   TList * dfuncList = new TList();

   // ---------------------------------------------
   InSANEDilutionFunction * dfunc = 0;
   const int NPoints = 30;
   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   TGraph * g[4];
   TMultiGraph * mg = new TMultiGraph();

   // loop over Q2 bins
   for(int k= 0;k<4;k++){

      g[k] = new TGraph(NPoints);
      g[k]->SetNameTitle(Form("dilution-%.3f",q2_bins[k]),Form("dilution %.3f",q2_bins[k]));
      dilution->fQ2_bin = q2_bins[k];
      Double_t x = 0.2;
      for(int i = 0;i<NPoints;i++){
         x += 0.7/((double)NPoints);
         Double_t df_0 = dilution->GetDilution(x,dilution->fQ2_bin);
         g[k]->SetPoint(i,x, df_0);
         Double_t y_frac = dilution->fQ2_bin/(2.0*(M_p/GeV)*x*(fman->GetBeamEnergy()));
         Double_t Eprime = InSANE::Kine::Eprime_xQ2y(x,dilution->fQ2_bin,y_frac); 
         Double_t theta   = InSANE::Kine::Theta_xQ2y(x,dilution->fQ2_bin,y_frac)/degree;
         std::cout << " x = " << x << ", dilution = " << df_0 << ", y = ";  
         std::cout << y_frac << ", Q2 = " << dilution->fQ2_bin  << ", th = " << theta << ", Ep = " << Eprime << std::endl; 


      }

      g[k]->SetMarkerStyle(20);
      g[k]->SetMarkerSize(1.3);
      g[k]->SetMarkerColor(2000+k);
      g[k]->SetLineColor(2000+k);
      mg->Add(g[k],"p");

      //dfunc =  (InSANEDilutionFunction*)run->fDilution.FindObject(Form("df-%1.2f",q2_bins[k]));
      //if(dfunc) if(dfunc->fDfVsx) {
      //   dfunc->fDfVsx->SetLineColor(2000+k);
      //   mg->Add(dfunc->fDfVsx,"p");
      //}
      leg->AddEntry(g[k],g[k]->GetTitle(),"lp");

      // -----------------
      InSANEDilutionFunction * dilution_func = new InSANEDilutionFunction();
      dilution_func->fQ2_bin = q2_bins[k]; 
      dfuncList->Add(dilution_func);

      //std::cout << run->GetBeamEnergy() << std::endl;
      //std::cout << run->GetTargetAngle() << std::endl;
      TString sql  = "SELECT x_bin,W_bin,df,df_error FROM dilution_factors WHERE ABS(beam_energy-";
              sql += run->GetBeamEnergy()/1000.0;
              sql += ")<0.3 AND target_angle=";
              sql += int(run->GetTargetAngle()/degree);
              sql += " AND ABS(q_squared_bin-";
              sql += q2_bins[k];
              sql += ")<0.1;";
      //std::cout << sql << std::endl;
      if(db){
         res = db->Query(sql.Data());
         if(res) {
            dilution_func->SetNBins( res->GetRowCount() ) ;
            dilution_func->InitGraph();
            //res->Print();
            while( row = res->Next() ) {
               /// Create and set all the values for this kinematic point.
               InSANEDilutionFactor * df = new InSANEDilutionFactor();
               df->Setx(atof(row->GetField(0))); 
               df->SetW(atof(row->GetField(1))); 
               df->SetDilution(atof(row->GetField(2))); 
               df->SetDilutionErr(atof(row->GetField(3))); 
               //df->Dump();
               dilution_func->Add(df);
            }

         }
         dilution_func->FitGraphs();
      }
   }  

   TCanvas * c = new TCanvas();
   leg->SetHeader(Form("run - %d",runNumber));
   mg->Draw("a");
   mg->SetMaximum(0.3);
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetYaxis()->SetRangeUser(0.0,0.3);
   mg->Draw("a");

   leg->Draw();

   for(int i=0;i<4;i++){
      InSANEDilutionFunction * adfunc = (InSANEDilutionFunction*)dfuncList->At(i);
      //adfunc->Dump();
      adfunc->fDfVsx->SetMarkerStyle(22);
      adfunc->fDfVsx->SetLineColor(2000+i);
      adfunc->fDfVsx->SetLineWidth(4);
      adfunc->fDfVsx->SetMarkerColor(2000+i);
      adfunc->fDfVsx->Draw("same,EP");
   }

   // ---------------------------------------------

   c->SaveAs(Form("plots/%d/dilution_comparison.png",runNumber));
   c->SaveAs(Form("plots/%d/dilution_comparison.svg",runNumber));
   c->SaveAs(Form("plots/%d/dilution_comparison.pdf",runNumber));

   return(0);
}

