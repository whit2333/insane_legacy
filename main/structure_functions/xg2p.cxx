/** Plots models and data for g2pWW at Qsq */
Int_t xg2p(Double_t Qsq = 1.0,Double_t QsqBinWidth=1.0){

   TCanvas * c = new TCanvas("g2pWW","g2pWW");
   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;
   // Create Legend
   TLegend * leg = new TLegend(0.6,0.5,0.875,0.875);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new LCWFPolarizedPartonDistributionFunctions);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new BBPolarizedPDFs);
   //pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg2pWW  = new TF1("xg2pWW"  , pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
                           xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");
   TF1 * xg2pWWA = new TF1("xg2pWWA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
                           xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");
   TF1 * xg2pWWB = new TF1("xg2pWWB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
                           xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");
   TF1 * xg2pWWC = new TF1("xg2pWWC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
                           xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");
   xg2pWW->SetParameter(0,Qsq);
   xg2pWWA->SetParameter(0,Qsq);
   xg2pWWB->SetParameter(0,Qsq);
   xg2pWWC->SetParameter(0,Qsq);

   xg2pWW->SetLineColor(1);
   xg2pWWA->SetLineColor(3);
   xg2pWWB->SetLineColor(kOrange -1);
   xg2pWWC->SetLineColor(kViolet-2);

   Int_t width = 3;
   xg2pWW->SetLineWidth(width);
   xg2pWWA->SetLineWidth(width);
   xg2pWWB->SetLineWidth(width);
   xg2pWWC->SetLineWidth(width);

   xg2pWW->SetLineStyle(1);
   xg2pWWA->SetLineStyle(1);
   xg2pWWB->SetLineStyle(1);
   xg2pWWC->SetLineStyle(1);

   xg2pWW->SetMaximum(1.5);
   xg2pWW->SetMinimum(-0.9);

   gr = new TGraph(xg2pWW->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(xg2pWWA->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(xg2pWWB->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(xg2pWWC->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");


   TF2 * fx = new TF2("fx","x*y");
   /// Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();
   /// Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);
   /// Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g2pWWQsq1 = new NucDBMeasurement("g2pQsq1",Form("g_{2WW}^{p} %s",Qsqbin->GetTitle()));

   TList * listOfMeas = manager->GetMeasurements("g2p");
   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g2pWWMeas = (NucDBMeasurement*)listOfMeas->At(i);
       gr = g2pWWMeas->BuildGraph();

       gr->SetMarkerStyle(20+i);
       gr->SetMarkerSize(1.6);
       gr->SetMarkerColor(kBlue-2-i);
       gr->SetLineColor(kBlue-2-i);
       gr->SetLineWidth(1);
       mg->Add(gr,"p");

       g2pWWQsq1->AddDataPoints(g2pWWMeas->FilterWithBin(Qsqbin));
       g2pWWQsq1->AddDataPoints(g2pWWMeas->FilterWithBin(Qsqbin));
       leg->AddEntry(gr,Form("%s %s",g2pWWMeas->GetExperimentName(),g2pWWMeas->GetTitle() ),"lp");

   }

   gr = g2pWWQsq1->BuildGraph();
   gr->SetMarkerColor(2);
   gr->SetLineColor(2);

   mg->Add(gr,"p");

   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.5,0.5);
   mg->Draw("a");

   /// Complete the Legend 

   leg->AddEntry(g2pWWQsq1->fGraph,Form("%s %s","All g2p data",g2pWWQsq1->GetTitle() ),"ep");
   leg->AddEntry(xg2pWW,Form("g2pWW %s ",pSFs->GetLabel()),"l");
   leg->AddEntry(xg2pWWA,Form("g2pWW %s ",pSFsA->GetLabel()),"l");
   leg->AddEntry(xg2pWWB,"g2pWW AAC ","l");
   leg->AddEntry(xg2pWWC,"g2pWW GS ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.25,0.85,Form("g_{2}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));

   c->SaveAs("results/structure_functions/xg2p.png");
   c->SaveAs("results/structure_functions/xg2p.pdf");
   return(0);
}
