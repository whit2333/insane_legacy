/** Plots models and data for g2nWW at Qsq */
Int_t g2nWW(Double_t Qsq = 5.0,Double_t QsqBinWidth=2.0){
	TCanvas * c = new TCanvas("g2nWW","g2nWW");
	/// Create Legend
	TLegend * leg = new TLegend(0.6,0.5,0.875,0.875);
	leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

	/// First Plot all the Polarized Structure Function Models! 
	InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
	pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
	InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
	pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
	InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
	pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
	InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
	pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);
	InSANEPolarizedStructureFunctionsFromPDFs * pSFsD = new InSANEPolarizedStructureFunctionsFromPDFs();
	pSFsD->SetPolarizedPDFs( new DSSVPolarizedPDFs);

	Int_t npar=1;
	Double_t xmin=0.01;
	Double_t xmax=1.0;
	TF1 * g2nWW = new TF1("g2nWW", pSFs, &InSANEPolarizedStructureFunctions::Evaluateg2nWW, 
			xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg2nWW");
	TF1 * g2nWWA = new TF1("g2nWWA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluateg2nWW, 
			xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg2nWW");
	TF1 * g2nWWB = new TF1("g2nWWB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluateg2nWW, 
			xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg2nWW");
	TF1 * g2nWWC = new TF1("g2nWWC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluateg2nWW, 
			xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg2nWW");
	TF1 * g2nWWD = new TF1("g2nWWD", pSFsC, &InSANEPolarizedStructureFunctions::Evaluateg2nWW, 
			xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg2nWW");

	g2nWW->SetParameter(0,Qsq);
	g2nWWA->SetParameter(0,Qsq);
	g2nWWB->SetParameter(0,Qsq);
	g2nWWC->SetParameter(0,Qsq);
	g2nWWD->SetParameter(0,Qsq);

	g2nWW->SetLineColor(1);
	g2nWWA->SetLineColor(3);
	g2nWWB->SetLineColor(kOrange -1);
	g2nWWC->SetLineColor(kViolet-2);
	g2nWWD->SetLineColor(kRed-2);

	Int_t width = 3;
	g2nWW->SetLineWidth(width);
	g2nWWA->SetLineWidth(width);
	g2nWWB->SetLineWidth(width);
	g2nWWC->SetLineWidth(width);
	g2nWWD->SetLineWidth(width);

	g2nWW->SetLineStyle(1);
	g2nWWA->SetLineStyle(1);
	g2nWWB->SetLineStyle(1);
	g2nWWC->SetLineStyle(1);
	g2nWWD->SetLineStyle(1);

	g2nWW->SetMaximum(0.9);
	g2nWW->SetMinimum(-0.9);

	/// Next,  Plot all the DATA
	NucDBManager * manager = NucDBManager::GetManager();
	/// Create a Bin in Q^2 for plotting a range of data on top  of all points
	NucDBBinnedVariable * Qsqbin = 
		new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
	Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
	Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);
	/// Create a new measurement which has all data points within the bin 
	NucDBMeasurement * g2nWWQsq1 = new NucDBMeasurement("g2nQsq1",Form("g_{2}^{WW,n} %s",Qsqbin->GetTitle()));

	TMultiGraph *Data = new TMultiGraph(); 

	Int_t NumOfDataPts=0; 
	TList * listOfMeas = manager->GetMeasurements("g2n");
	int N = listOfMeas->GetEntries(); 
	if(N>0){
		for(int i =0;i<N;i++){
			NucDBMeasurement *Meas = (NucDBMeasurement*)listOfMeas->At(i);
			NumOfDataPts = Meas->GetNDataPoints();
			if(NumOfDataPts>0){
				TGraphErrors *graph         = Meas->BuildGraph("x");
				graph->SetMarkerStyle(20+i);
				graph->SetMarkerSize(1.6);
				graph->SetMarkerColor(kBlue-2-i);
				graph->SetLineColor(kBlue-2-i);
				graph->SetLineWidth(1);
				Data->Add(graph); 
				g2nWWQsq1->AddDataPoints(Meas->FilterWithBin(Qsqbin));
				leg->AddEntry(graph,Form("%s %s",Meas->GetExperimentName(),Meas->GetTitle() ),"lp");
			}else{
				continue;
			}
		}
	}else{
		cout << "No data!" << endl;
	}

	TGraphErrors * graph2 = g2nWWQsq1->BuildGraph("x");
	graph2->SetMarkerStyle(20);
	graph2->SetMarkerColor(2);
	graph2->SetLineColor(2);
	graph2->SetMarkerSize(1.1);

	Data->Add(graph2); 

	/// Complete the Legend 
	leg->AddEntry(graph2,Form("%s %s","All g2n data",g2nWWQsq1->GetTitle() ),"ep");
	leg->AddEntry(g2nWW,"g2nWW BB ","l");
	leg->AddEntry(g2nWWA,"g2nWW DNS2005 ","l");
	// leg->AddEntry(g2nWWB,"g2nWW AAC ","l");
	leg->AddEntry(g2nWWC,"g2nWW GS ","l");
	leg->AddEntry(g2nWWD,"g2nWW DSSV ","l");

	TString Measurement = Form("g_{2}^{WW,n}");
	TString Title       = Form("%s(x,Q^{2} = %.0f GeV^{2})",Measurement.Data(),Qsq);
	TString xAxisTitle  = Form("x");
	TString yAxisTitle  = Form("%s",Measurement.Data());

	Data->Draw("AP");
	Data->SetTitle(Title);
	Data->GetXaxis()->SetTitle(xAxisTitle);
	Data->GetXaxis()->CenterTitle();
	Data->GetYaxis()->SetTitle(yAxisTitle);
	Data->GetYaxis()->CenterTitle();
	Data->Draw("AP");

	g2nWW->Draw("same");
	g2nWWA->Draw("same");
	// g2nWWB->Draw("same");
	g2nWWC->Draw("same");
	g2nWWD->Draw("same");

	leg->Draw();

        TString prefix = Form("./plots/structure_functions/");
        TString name   = Form("g2nWW");
        TString fn     = prefix + name; 

	c->SaveAs(fn+".png");
	c->SaveAs(fn+".pdf");

	return(0);
}
