/** Plots models and data for g1n at Qsq */
Int_t xg1n(Double_t Qsq = 5.0,Double_t QsqBinWidth=1.0){
   TCanvas * c = new TCanvas("xg1n","xg1n");
   /// Create Legend
   TLegend * leg = new TLegend(0.6,0.125,0.875,0.45);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   /// First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsD = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsD->SetPolarizedPDFs( new DSSVPolarizedPDFs );
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsE = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsE->SetPolarizedPDFs( new AvakianPolarizedPDFs );

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg1n = new TF1("xg1n", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1n");
   TF1 * xg1nA = new TF1("xg1nA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatexg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1n");
   TF1 * xg1nB = new TF1("xg1nB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatexg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1n");
   TF1 * xg1nC = new TF1("xg1nC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatexg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1n");
   TF1 * xg1nD = new TF1("xg1nD", pSFsD, &InSANEPolarizedStructureFunctions::Evaluatexg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1n");
   TF1 * xg1nE = new TF1("xg1nE", pSFsE, &InSANEPolarizedStructureFunctions::Evaluatexg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1n");

   xg1n->SetParameter(0,Qsq);
   xg1nA->SetParameter(0,Qsq);
   xg1nB->SetParameter(0,Qsq);
   xg1nC->SetParameter(0,Qsq);
   xg1nD->SetParameter(0,Qsq);
   xg1nE->SetParameter(0,Qsq);

   xg1n->SetLineColor(kBlack);
   xg1nA->SetLineColor(kRed);
   xg1nB->SetLineColor(kOrange -1);
   xg1nC->SetLineColor(kViolet-2);
   xg1nD->SetLineColor(kRed-2);
   xg1nE->SetLineColor(kGreen);

   Int_t width = 2;
   xg1n->SetLineWidth(width);
   xg1nA->SetLineWidth(width);
   xg1nB->SetLineWidth(width);
   xg1nC->SetLineWidth(width);
   xg1nD->SetLineWidth(width);
   xg1nE->SetLineWidth(width);

   xg1n->SetLineStyle(1);
   xg1nA->SetLineStyle(1);
   xg1nB->SetLineStyle(1);
   xg1nC->SetLineStyle(1);
   xg1nD->SetLineStyle(1);
   xg1nE->SetLineStyle(1);

   xg1nA->SetMinimum(-0.065);
   xg1nA->SetMaximum(0.05);

   /// Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();
   /// Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   /// Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g1nQsq1 = new NucDBMeasurement("g1nQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   TF2 * xf = new TF2("xf","x*y",0,1,-1,1);

   TMultiGraph *Data = new TMultiGraph(); 

   TList * listOfMeas = manager->GetMeasurements("g1n");
   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g1nMeas = (NucDBMeasurement*)listOfMeas->At(i);
       TGraphErrors * graph = g1nMeas->BuildGraph("x");
       graph->Apply(xf);
       graph->SetMarkerStyle(20+i);
       graph->SetMarkerSize(1.6);
       graph->SetMarkerColor(kBlue-2-i);
       graph->SetLineColor(kBlue-2-i);
       graph->SetLineWidth(1);

       Data->Add(graph); 
       g1nQsq1->AddDataPoints(g1nMeas->FilterWithBin(Qsqbin));

       // if(i==0) {
       //    graph->Draw("p");
       // }else{
       //    graph->Draw("p");
       //    g1nQsq1->AddDataPoints(g1nMeas->FilterWithBin(Qsqbin));
       // }

       leg->AddEntry(graph,Form("%s %s",g1nMeas->GetExperimentName(),g1nMeas->GetTitle() ),"p");

   }

   TGraphErrors * graph2 = g1nQsq1->BuildGraph("x");
   graph2->Apply(xf);
   graph2->SetMarkerStyle(20);
   graph2->SetMarkerColor(2);
   graph2->SetLineColor(2);
   graph2->SetMarkerSize(1.1);

   Data->Add(graph2); 

   TString Measurement = Form("xg_{1}^{n}");
   TString Title      = Form("%s(x,Q^{2} = %.0f GeV^{2})",Measurement.Data(),Qsq);
   TString xAxisTitle = Form("x");
   TString yAxisTitle = Form("%s",Measurement.Data());

   Data->Draw("AP");
   Data->SetTitle(Title);
   Data->GetXaxis()->SetTitle(xAxisTitle);
   Data->GetXaxis()->CenterTitle();
   Data->GetYaxis()->SetTitle(yAxisTitle);
   Data->GetYaxis()->CenterTitle();
   Data->Draw("AP");

   // draw the models 
   xg1n->Draw("same");
   xg1nA->Draw("same");
   xg1nB->Draw("same");
   xg1nC->Draw("same");
   xg1nD->Draw("same");
   xg1nE->Draw("same");

   /// Complete the Legend 
   leg->SetFillColor(kWhite); 
   //leg->AddEntry(g1nQsq1->fGraph,Form("%s %s","All g1n data",g1nQsq1->GetTitle() ),"ep");
   leg->AddEntry(xg1n,"BB ","l");
   leg->AddEntry(xg1nA,"DNS2005 ","l");
   leg->AddEntry(xg1nB,"AAC ","l");
   leg->AddEntry(xg1nC,"GS ","l");
   leg->AddEntry(xg1nD,"DSSV ","l");
   leg->AddEntry(xg1nE,"Avakian ","l");
   leg->Draw();

   // TLatex * t = new TLatex();
   // t->SetNDC();
   // t->SetTextFont(62);
   // t->SetTextColor(kBlack);
   // t->SetTextSize(0.04);
   // t->SetTextAlign(12); 
   // t->DrawLatex(0.55,0.85,Form("xg_{1}^{n}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));

   // c->SaveAs("./plots/structure_functions/xg1n.png");
   // c->SaveAs("./plots/structure_functions/xg1n.pdf");
   return(0);
}
