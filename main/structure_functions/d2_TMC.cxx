void d2_TMC(int aNumber = 55) {

  //auto sf0 = new InSANEPolarizedStructureFunctionsFromPDFs( new LCWFPolarizedPartonDistributionFunctions);
  //auto sf3 = new InSANEPolarizedStructureFunctionsFromPDFs( new GSPolarizedPDFs);
  //pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
  auto JAM_sf  = new InSANEPolarizedStructureFunctionsFromPDFs( new JAM15PolarizedPDFs);
  auto BB_sf   = new InSANEPolarizedStructureFunctionsFromPDFs( new BBPolarizedPDFs);
  auto DSSV_sf = new InSANEPolarizedStructureFunctionsFromPDFs( new DSSVPolarizedPDFs);
  auto STAT_sf = new InSANEPolarizedStructureFunctionsFromPDFs( new Stat2015PolarizedPDFs);

  std::vector<InSANEPolarizedStructureFunctionsFromPDFs*> sfs = {JAM_sf, BB_sf, DSSV_sf, STAT_sf};
  std::vector<TGraph*> grs_d2_WW;
  std::vector<TGraph*> grs_d2_Dp_Twist3;
  std::vector<TGraph*> grs_d2_3g2_Twist3;
  std::vector<TGraph*> grs_d2;
  std::vector<TGraph*> grs_d2_TMC;
  std::vector<TGraph*> grs_d2_Twist3;
  std::vector<TGraph*> grs_d2_Twist3_TMC;
  std::vector<TGraph*> grs_M23_TMC;

  std::vector<int> colors = {1,2,4,8,9,6};

  double x_min = 0.001;
  double x_max = 0.8;

  double Q2_min = 1.0;
  double Q2_max = 6.0;

  int    NQ2    = aNumber;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2-1);

  for(auto aSF : sfs) {
    grs_d2_WW.push_back(  new TGraph() );
    grs_d2_Dp_Twist3.push_back(  new TGraph() );
    grs_d2_3g2_Twist3.push_back(  new TGraph() );
    grs_d2.push_back(  new TGraph() );
    grs_d2_TMC.push_back(  new TGraph() );
    grs_d2_Twist3.push_back(  new TGraph() );
    grs_d2_Twist3_TMC.push_back(  new TGraph() );
    grs_M23_TMC.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    int iSF = 0;
    for(auto aSF : sfs) {

      auto pdfs = aSF->GetPolarizedPDFs();

      double d2_WW      = aSF->d2p_WW(Q2, x_min, x_max);
      grs_d2_WW[iSF]->SetPoint( iQ2, Q2, d2_WW);

      double d2_TMC      = aSF->d2p_tilde_TMC(Q2, x_min, x_max);
      grs_d2_TMC[iSF]->SetPoint(iQ2, Q2, d2_TMC);

      double d2      = aSF->d2p_tilde(Q2, x_min, x_max);
      grs_d2[iSF]->SetPoint(iQ2, Q2, d2);

      double d2_Dp_Twist3   = pdfs->Moment_Dp_Twist3(3, Q2, x_min, x_max);
      grs_d2_Dp_Twist3[iSF]->SetPoint(iQ2, Q2, d2_Dp_Twist3);

      double d2_3g2_Twist3   = 3.0*aSF->Moment_g2p_Twist3(3, Q2, x_min, x_max);
      grs_d2_3g2_Twist3[iSF]->SetPoint(iQ2, Q2, d2_3g2_Twist3);

      double d2_Twist3      = aSF->d2p_Twist3(Q2, x_min, x_max);
      grs_d2_Twist3[iSF]->SetPoint(iQ2, Q2, d2_Twist3);

      double d2_Twist3_TMC      = aSF->d2p_Twist3_TMC(Q2, x_min, x_max);
      grs_d2_Twist3_TMC[iSF]->SetPoint(iQ2, Q2, d2_Twist3_TMC);

      double M23_TMC      = aSF->M2n_TMC_p(3, Q2, x_min, x_max);
      grs_M23_TMC[iSF]->SetPoint(iQ2, Q2, M23_TMC);

      iSF++;
    }

  }

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.6,0.5,0.975,0.975);

  int iSF = 0;
  for(auto aSF : sfs) {

    grs_d2_WW[iSF]->SetLineColor(colors[iSF]);
    grs_d2_WW[iSF]->SetLineWidth(4);
    mg->Add(grs_d2_WW[iSF],"l");
    //leg->AddEntry(grs_d2_WW[iSF], aSF->GetLabel(), "l");

    grs_d2[iSF]->SetLineColor(colors[iSF]);
    grs_d2[iSF]->SetLineWidth(1);
    grs_d2[iSF]->SetLineStyle(1);
    mg->Add(grs_d2[iSF],"l");
    leg->AddEntry(grs_d2[iSF], Form("d2 %s",aSF->GetLabel()), "l");

    grs_d2_TMC[iSF]->SetLineColor(colors[iSF]);
    grs_d2_TMC[iSF]->SetLineWidth(2);
    grs_d2_TMC[iSF]->SetLineStyle(7);
    mg->Add(grs_d2_TMC[iSF],"l");
    leg->AddEntry(grs_d2_TMC[iSF], Form("d2_TMC %s",aSF->GetLabel()), "l");
    
    grs_d2_Twist3[iSF]->SetLineColor(colors[iSF]);
    grs_d2_Twist3[iSF]->SetLineWidth(2);
    grs_d2_Twist3[iSF]->SetLineStyle(2);
    mg->Add(      grs_d2_Twist3[iSF],"l");
    leg->AddEntry(grs_d2_Twist3[iSF], Form("d2(t3) %s",aSF->GetLabel()), "l");
    
    grs_d2_Dp_Twist3[iSF]->SetLineColor(colors[iSF]);
    grs_d2_Dp_Twist3[iSF]->SetLineWidth(2);
    grs_d2_Dp_Twist3[iSF]->SetLineStyle(3);
    mg->Add(grs_d2_Dp_Twist3[iSF],"l");
    if(iSF==0){
      leg->AddEntry(grs_d2_Dp_Twist3[iSF],Form("x^{2} D_{p}(x) %s",aSF->GetLabel()), "l");
    }

    grs_d2_3g2_Twist3[iSF]->SetLineColor(colors[iSF]);
    grs_d2_3g2_Twist3[iSF]->SetLineWidth(2);
    grs_d2_3g2_Twist3[iSF]->SetLineStyle(8);
    mg->Add(grs_d2_3g2_Twist3[iSF],"l");
    if(iSF==0){
      leg->AddEntry(grs_d2_3g2_Twist3[iSF],Form(" 3 x^{2} g_{2}(t3) %s",aSF->GetLabel()), "l");
    }

    grs_d2_Twist3_TMC[iSF]->SetLineColor(colors[iSF]);
    grs_d2_Twist3_TMC[iSF]->SetLineWidth(2);
    mg->Add(      grs_d2_Twist3_TMC[iSF],"l");
    leg->AddEntry(grs_d2_Twist3_TMC[iSF], Form("d2(t3)_TMC %s",aSF->GetLabel()), "l");
    
    grs_M23_TMC[iSF]->SetLineColor(colors[iSF]);
    grs_M23_TMC[iSF]->SetLineWidth(3);
    grs_M23_TMC[iSF]->SetLineStyle(10);
    mg->Add(      grs_M23_TMC[iSF],"l");
    leg->AddEntry(grs_M23_TMC[iSF], Form("M23_TMC %s",aSF->GetLabel()), "l");
    iSF++;
  }

  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(0.0,0.02);
  mg->GetXaxis()->SetLimits(1.0,6.0);
  leg->Draw();

  c->SaveAs(Form("results/structure_functions/d2_TMC_%d.png",aNumber));
  c->SaveAs(Form("results/structure_functions/d2_TMC_%d.pdf",aNumber));

}

