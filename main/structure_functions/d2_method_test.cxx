void d2_method_test(int aNumber = 0){

  std::string  file_name = "d2_test.txt";

  //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  auto   sf = new InSANEPolarizedStructureFunctionsFromPDFs();
  auto pdfs = new JAM15PolarizedPDFs(0);//aNumber);
  //auto pdfs = new StatisticalPolarizedPDFs();//aNumber);
  ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(pdfs);

  double x_min = 0.001;
  double x_max = 0.9;
  int    N     = 100;
  double dx    = (x_max-x_min)/double(N);

  //double d2p_tilde     = sf->d2p_tilde(Q2,x_min,x_max)    ;


  TMultiGraph * mg               = new TMultiGraph();
  TGraph      * gr_M23           = new TGraph();
  TGraph      * gr_M23_test      = new TGraph();
  TGraph      * gr_M23_Oscar     = new TGraph();
  TGraph      * gr_d2_tilde      = new TGraph();
  TGraph      * gr_d2_tilde_TMC  = new TGraph();
  TGraph      * gr_d2_g2_tw3     = new TGraph();
  TGraph      * gr_d2_tw3     = new TGraph();

  double Q2_min = 1.0;
  double Q2_max = 6.0;
  int    NQ2    = 12;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2-1);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    double Q2  = Q2_min + double(iQ2)*dQ2;

    double M23_TMC      = 2.0*sf->M2n_TMC_p(         3,Q2,x_min,x_max)    ;
    double M23_TMC_test = 2.0*sf->M2n_TMC_test_p(3,Q2,x_min,x_max) ;
    double d2_Oscar     = sf->d2_Oscar(Q2,x_min,x_max)    ;

    double d2_tilde_TMC = sf->d2p_tilde_TMC(Q2,x_min,x_max)    ;
    double d2_tilde     = sf->d2p_tilde    (Q2,x_min,x_max)    ;
    double d2_g2_tw3    = sf->Moment_g2p_Twist3(3,Q2,x_min,x_max)    ;
    double d2_tw3       = pdfs->Moment_Dp_Twist3(3,Q2,x_min,x_max)    ;

  double M25_TMC = sf->M2n_TMC_p(5,Q2,x_min,x_max) ;
  double M27_TMC = sf->M2n_TMC_p(7,Q2,x_min,x_max) ;
  double M29_TMC = sf->M2n_TMC_p(9,Q2,x_min,x_max) ;
  double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
  double I_nacht       = 2.0*(M23_TMC_p + 6.0*M25_TMC_p*y2 + 12.0*M27_TMC_p*y2*y2 + 20.0*M29_TMC_p*y2*y2*y2);
  double I_nacht_TMC   = 2.0*(M23_p     + 6.0*M25_p*y2     + 12.0*M27_p*y2*y2     + 20.0*M29_p*y2*y2*y2);
    gr_M23      ->SetPoint(iQ2, Q2, M23_TMC);
    gr_M23_test ->SetPoint(iQ2, Q2, M23_TMC_test);
    gr_M23_Oscar->SetPoint(iQ2, Q2, d2_Oscar    );

    //gr_d2_tilde_TMC->SetPoint( iQ2, Q2, d2_tilde_TMC);
    //gr_d2_tilde    ->SetPoint( iQ2, Q2, d2_tilde    );

    gr_d2_g2_tw3    ->SetPoint( iQ2, Q2, d2_g2_tw3    );
    gr_d2_tw3       ->SetPoint( iQ2, Q2, d2_tw3    );
  }

  gr_M23      ->SetLineColor(1);
  gr_M23_test ->SetLineColor(2);
  gr_M23_Oscar->SetLineColor(4);
  gr_M23      ->SetLineWidth(2);
  gr_M23_test ->SetLineWidth(2);
  gr_M23_Oscar->SetLineWidth(2);

  gr_d2_g2_tw3->SetLineColor(8);
  gr_d2_g2_tw3->SetLineWidth(2);
  gr_d2_tw3->SetLineColor(9);
  gr_d2_tw3->SetLineWidth(2);

  gr_d2_tilde_TMC->SetLineColor(1);
  gr_d2_tilde    ->SetLineColor(2);
  gr_d2_tilde_TMC->SetLineWidth(2);
  gr_d2_tilde    ->SetLineWidth(2);
  gr_d2_tilde_TMC->SetLineStyle(2);
  gr_d2_tilde    ->SetLineStyle(2);
  // -------------------------------------------------
  //
  TCanvas * c   = nullptr;
  TLegend * leg = nullptr;
  
  // ----------------------------------
  c   = new TCanvas();
  leg = new TLegend(0.6,0.7,0.99,0.99);
  mg->Add(gr_M23      ,      "l");
  mg->Add(gr_M23_test ,      "l");
  mg->Add(gr_M23_Oscar,      "l");
  //mg->Add(gr_d2_tilde_TMC,      "l");
  //mg->Add(gr_d2_tilde    ,      "l");
  mg->Add(gr_d2_g2_tw3   ,      "l");
  mg->Add(gr_d2_tw3   ,      "l");
  leg->AddEntry(gr_M23      ,  "2M_{2}^{3} g2(full) and g1(full)", "l");
  leg->AddEntry(gr_M23_test ,  "2M_{2}^{3} g2(full) and g1(#tau2 TMC only)", "l");
  leg->AddEntry(gr_M23_Oscar , "2M_{2}^{3} gT(full) and g1(#tau2 TMC only)", "l");
  leg->AddEntry(gr_d2_g2_tw3 , "#int x^{2} g_{2}^{#tau3}(x;M=0) dx", "l");
  leg->AddEntry(gr_d2_tw3 ,    "#int x^{2} D(x) dx", "l");
  leg->SetHeader(Form("%.3f < x < %.3f",x_min,x_max));//sf->GetLabel());
  mg->Draw("a");
  mg->GetXaxis()->SetTitle("Q^2");
  mg->GetYaxis()->SetRangeUser(0.0,0.015);
  mg->GetXaxis()->SetLimits(Q2_min,Q2_max);
  mg->Draw("a");
  leg->Draw();
  c->SaveAs(Form("results/structure_functions/d2_method_test_%d_0.png",aNumber));
  c->SaveAs(Form("results/structure_functions/d2_method_test_%d_0.pdf",aNumber));

}
