void g1_TMCs(){

  InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  auto   sf  = fman->CreatePolSFs(14);
  auto   sf_ref  = fman->CreatePolSFs(1);


  double Q2    = 2.0;
  double x_min = 0.05;
  double x_max = 0.95;
  int    N     = 50;
  double dx    = (x_max-x_min)/double(N);

  TMultiGraph * mg            = new TMultiGraph();
  TGraph      * gr            = new TGraph();
  TGraph      * gr_g1_ref     = new TGraph();
  TGraph      * gr_noTMC      = new TGraph();
  TGraph      * gr_g1_t3_TMC  = new TGraph();

  TMultiGraph * mg2             = new TMultiGraph();
  TGraph      * gr_WW           = new TGraph();
  TGraph      * gr_WW_TMC       = new TGraph();
  TGraph      * gr_delta_WW     = new TGraph();
  TGraph      * gr_g2_t3_noTMC  = new TGraph();
  TGraph      * gr_g2_t3_TMC    = new TGraph();
  TGraph      * gr_g2_t2_t3_TMC    = new TGraph();

  // -------------------------------------------------
  //
  gr->SetLineWidth(2);
  gr->SetLineColor(2);
  gr_noTMC->SetLineWidth(2);
  gr_noTMC->SetLineColor(1);
  gr_g1_t3_TMC->SetLineWidth(2);
  gr_g1_t3_TMC->SetLineColor(4);

  gr_WW_TMC->SetLineWidth(2);
  gr_WW_TMC->SetLineColor(2);
  gr_WW->SetLineWidth(2);
  gr_WW->SetLineColor(1);
  gr_delta_WW->SetLineWidth(2);
  gr_delta_WW->SetLineColor(4);
  gr_g2_t3_noTMC->SetLineWidth(2);
  gr_g2_t3_noTMC->SetLineColor(3);
  gr_g2_t3_TMC->SetLineWidth(2);
  gr_g2_t3_TMC->SetLineColor(7);
  gr_g2_t2_t3_TMC->SetLineWidth(2);
  gr_g2_t2_t3_TMC->SetLineColor(8);

  // -------------------------------------------------
  //
  for(unsigned int ix = 0; ix<N ; ix++) {

    double x = x_min + double(ix)*dx;

    double g1_t2_ref = sf_ref->g1p_Twist2(x,Q2);
    double g1_t2     = sf->g1p_Twist2(x,Q2);
    double g1_t2_TMC = sf->g1p_Twist2_TMC(x,Q2);
    double g1_t3_TMC = sf->g1p_Twist3_TMC(x,Q2);

    double g1_WW     = sf->g2pWW(x,Q2);
    double g1_WW_TMC = sf->g2pWW_TMC(x,Q2);
    double g2_t3     = sf->g2p_Twist3(x,Q2);
    double g2_t3_TMC = sf->g2p_Twist3_TMC(x,Q2);
    double g2_t2_TMC = sf->g2p_Twist2_TMC(x,Q2);

    gr_g1_ref->SetPoint( ix, x, x*g1_t2_ref );
    gr      ->SetPoint( ix, x, x*g1_t2_TMC );
    gr_noTMC->SetPoint( ix, x, x*g1_t2 );
    gr_g1_t3_TMC->SetPoint( ix, x, x*(g1_t3_TMC + g1_t2_TMC));

    gr_WW      ->SetPoint( ix, x, x*g1_WW );
    gr_WW_TMC  ->SetPoint( ix, x, x*g1_WW_TMC );
    gr_delta_WW->SetPoint( ix, x, x*g1_WW-x*g1_WW_TMC );

    gr_g2_t3_noTMC->SetPoint( ix, x, x*g2_t3 );
    gr_g2_t3_TMC  ->SetPoint( ix, x, x*g2_t3_TMC );
    gr_g2_t2_t3_TMC  ->SetPoint( ix, x, x*g2_t2_TMC );

  }

  // -------------------------------------------------
  //
  mg->Add(gr,      "l");
  mg->Add(gr_noTMC,"l");
  mg->Add(gr_g1_t3_TMC,"l");
  mg->Add(gr_g1_ref,"l");

  mg2->Add(gr_WW,    "l");
  mg2->Add(gr_WW_TMC,"l");
  mg2->Add(gr_delta_WW,"l");
  mg2->Add(gr_g2_t3_noTMC,"l");
  mg2->Add(gr_g2_t3_TMC,"l");
  mg2->Add(gr_g2_t2_t3_TMC,"l");

  // -------------------------------------------------
  //
  TCanvas * c   = nullptr;
  TLegend * leg = nullptr;
  
  // ----------------------------------
  c   = new TCanvas();
  leg = new TLegend(0.5,0.8,0.99,0.99);
  leg->AddEntry( gr_noTMC, "xg_{1}^{no TMC}", "l");
  leg->AddEntry( gr      , "xg_{1}^{TMC}"   , "l");
  leg->AddEntry( gr_g1_t3_TMC, "xg_{1}^{TMC+#tau3}"   , "l");
  leg->SetHeader(sf->GetLabel());
  mg->Draw("a");
  mg->GetXaxis()->SetTitle("x");
  mg->Draw("a");
  leg->Draw();
  c->SaveAs("results/structure_functions/g1_TMCs_0.png");
  c->SaveAs("results/structure_functions/g1_TMCs_0.pdf");

  // ----------------------------------
  c   = new TCanvas();
  leg = new TLegend(0.5,0.8,0.99,0.99);
  leg->AddEntry( gr_WW         , "xg_{2}^{WW}[g_{1}^{no TMC}]", "l");
  leg->AddEntry( gr_WW_TMC     , "xg_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  leg->AddEntry( gr_delta_WW   , "xg_{2}^{WW}[g_{1}^{no TMC}] - xg_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  leg->AddEntry( gr_g2_t3_noTMC, "xg_{2}^{#tau3}","l");
  leg->AddEntry( gr_g2_t3_TMC  , "xg_{2}^{TMC+#tau3}"   , "l");
  leg->AddEntry( gr_g2_t2_t3_TMC  , "xg_{2}^{TMC+#tau2+#tau3}"   , "l");
  mg2->Draw("a");
  mg2->GetXaxis()->SetTitle("x");
  mg2->Draw("a");
  leg->Draw();
  c->SaveAs("results/structure_functions/g1_TMCs_1.png");
  c->SaveAs("results/structure_functions/g1_TMCs_1.pdf");

  // ----------------------------------


}

