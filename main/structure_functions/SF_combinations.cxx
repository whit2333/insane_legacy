Int_t SF_combinations() {

   TCanvas * c = new TCanvas("F2p","F2p");
   c->Divide(2,2);

   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("F1p");

   // CTEQ
   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   // F1F209
   InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();

   //----------------------
   //  F1p
   c->cd(1);

   TF1 * F1pCTEQ = new TF1("F1pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(F1pCTEQ,"F1p CTEQ","l");
   F1pCTEQ->SetParameter(0,4.5);
   F1pCTEQ->SetLineColor(6);
   F1pCTEQ->Draw();

   //----------------------
   //  F2p
   c->cd(2);

   TF1 * F2pCTEQ = new TF1("F2pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(F2pCTEQ,"F2p CTEQ","l");
   F2pCTEQ->SetParameter(0,4.5);
   F2pCTEQ->SetLineColor(6);
   F2pCTEQ->Draw();

   TF1 * F2_f1f209 = new TF1("F2_f1f209", f1f2SFs, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(F2_f1f209,"F2p F1F209","l");
   F2_f1f209->SetParameter(0,4.5);
   F2_f1f209->SetLineColor(1);
   F2_f1f209->Draw("same");

   return(0);

   InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();
   TF1 * xF2pA = new TF1("xF1pA", f1f2SFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF2pA,"F1p F1F209","l");
   xF2pA->SetParameter(0,4.5);
   xF2pA->SetLineColor(1);
   xF2pA->Draw("same");

   InSANEStructureFunctionsFromPDFs * myBBS = new InSANEStructureFunctionsFromPDFs();
   myBBS->SetUnpolarizedPDFs(new BBSUnPolarizedPDFs());
   TF1 * xF2pB = new TF1("xF1pB", myBBS, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   xF2pB->SetLineColor(kGreen);
   xF2pB->Draw("same");
   leg->AddEntry(xF2pB,"F1p BBS","l");

   NMC95StructureFunctions * NMCSFs = new NMC95StructureFunctions();
   TF1 * xF2pNMC = new TF1("xF1pNMC", NMCSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF2pNMC,"F1p NMC95","l");
   xF2pNMC->SetLineColor(4);
   xF2pNMC->SetParameter(0,2.6);
   xF2pNMC->DrawCopy("same");


/*   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("MSTW2008nlo68cl");*/
/*   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("cteq4m");*/
/*
   InSANEStructureFunctions * lhaSFs = new LHAPDFStructureFunctions();

   TF1 * xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFType("cteq6m",1);
   xF2p->SetParameter(0,10.5);
   xF2p->SetLineColor(2);
   leg->AddEntry(xF2p,Form("F2p LHAPDF:%s at Q^2=10.5",lhaSFs->fLabel.Data()),"l");
   xF2p->DrawClone("same");

   xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");

   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFType("cteq6m",1);
   xF2p->SetParameter(0,4.5);
   xF2p->SetLineColor(3);
   leg->AddEntry(xF2p,Form("F2p LHAPDF:%s at Q^2=4.5",lhaSFs->fLabel.Data()),"l");
   xF2p->DrawClone("same");
*/

   /// Composite Structure FUnctions
   //LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   //TF1 * xF2pC = new TF1("xF2pC", sf, &InSANEStructureFunctions::EvaluateF2p, 
   //            0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   ////((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   //xF2pC->SetParameter(0,4.5);
   //xF2pC->SetLineColor(6);
   ////leg->AddEntry(xF2p,Form("Composite SFs:%s at Q^2=10.5",sf->fLabel.Data()),"l");
   //xF2pC->DrawClone("same");


//    xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF2p, 
//                0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
// 
//     ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("cteq6mE", LHAPDF::LHGRID );
//    xF2p->SetParameter(0,100.5);
//    xF2p->SetLineColor(7);
//    leg->AddEntry(xF2p,Form("F2p LHAPDF:%s",lhaSFs->fLabel.Data()),"l");
//    xF2p->DrawClone("same");



   leg->Draw();
//    lhaSFs->PlotSFs(4.5);


   //c->SaveAs(fn1);
   //c->SaveAs(fn2);

return(0);
}
