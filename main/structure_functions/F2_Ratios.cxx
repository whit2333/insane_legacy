Int_t F2_Ratios(Double_t Q2 = 5.0, Double_t dQ2 = 4.0) {

   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Qsq = new NucDBBinnedVariable("Qsquared",Form("Q^{2} %.1f<Q^{2}<%.1f",Q2-dQ2,Q2+dQ2));
   Qsq->SetBinMinimum(Q2-dQ2);
   Qsq->SetBinMaximum(Q2+dQ2);

   Double_t Q2_2  = 2.0;
   Double_t dQ2_2 = 1.0;
   NucDBBinnedVariable * Qsq2 = new NucDBBinnedVariable("Qsquared",Form("Q^{2} %.1f<Q^{2}<%.1f",Q2_2-dQ2_2,Q2_2+dQ2_2));
   Qsq2->SetBinMinimum(Q2_2-dQ2_2);
   Qsq2->SetBinMaximum(Q2_2+dQ2_2);

   TList * measurementsList = manager->GetMeasurements("F2COverF2D");
   NucDBMeasurement * F2Ratio = (NucDBMeasurement*)measurementsList->At(0);
   if(!F2Ratio) return -22;
   NucDBMeasurement * F2RatioAll = new NucDBMeasurement("F2RatioAll","F_{2}^{C}/F_{2}^{D} ");
   NucDBMeasurement * F2RatioQsq1 = new NucDBMeasurement("F2RatioQsq1",Form("F_{2}^{C}/F_{2}^{D} %s",Qsq->GetTitle()));
   NucDBMeasurement * F2RatioQsq2 = new NucDBMeasurement("F2RatioQsq2",Form("F_{2}^{C}/F_{2}^{D} %s",Qsq2->GetTitle()));

   TLegend * leg = new TLegend(0.6,0.7,0.89,0.89);
   leg->SetFillColor(0);
   leg->SetBorderSize(0);
   //leg->SetHeader("F1p");

   //TMultiGraph * mg = manager->GetMultiGraph("F1p","x");
   TMultiGraph * mg = new TMultiGraph();
   //leg->AddEntry(F1p->fGraph,Form("%s %s",F1p->GetExperimentName(),F1p->GetTitle() ),"ep");

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *aMeas = (NucDBMeasurement*)measurementsList->At(i);
      F2RatioQsq1->AddDataPoints(aMeas->FilterWithBin(Qsq));
      F2RatioQsq2->AddDataPoints(aMeas->FilterWithBin(Qsq2));
      F2RatioAll->AddDataPoints(aMeas->GetDataPoints());
   }

   TGraphErrors * graph0 = F2RatioAll->BuildGraph("x");
   graph0->SetMarkerColor(1);
   graph0->SetLineColor(1);
   mg->Add(graph0,"p");
   leg->AddEntry(graph0,Form("%s %s",F2Ratio->GetExperimentName(),F2RatioQsq2->GetTitle() ),"ep");

   TGraphErrors * graph1 = F2RatioQsq1->BuildGraph("x");
   graph1->SetMarkerColor(4);
   graph1->SetLineColor(4);
   mg->Add(graph1,"p");
   leg->AddEntry(graph1,Form("%s %s",F2Ratio->GetExperimentName(),F2RatioQsq1->GetTitle() ),"ep");

   TGraphErrors * graph2 = F2RatioQsq2->BuildGraph("x");
   graph2->SetMarkerColor(2);
   graph2->SetLineColor(2);
   mg->Add(graph2,"p");
   leg->AddEntry(graph2,Form("%s %s",F2Ratio->GetExperimentName(),F2RatioQsq2->GetTitle() ),"ep");


   // -----------------
   // CTEQ
   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   TF1 * xF1pCTEQ = new TF1("xF1pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF1p, 
                                        0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF1pCTEQ,"F1p CTEQ","l");
   xF1pCTEQ->SetParameter(0,Q2);
   xF1pCTEQ->SetLineColor(6);

   // -----------------
   // F1F209
   InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();
   TF1 * xF1pA = new TF1("xF1pA", f1f2SFs, &InSANEStructureFunctions::EvaluateF1p, 
                                  0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF1pA,"F1p F1F209","l");
   xF1pA->SetParameter(0,Q2);
   xF1pA->SetLineColor(1);

   // -----------------
   // Statistical 
   InSANEStructureFunctionsFromPDFs * myBBS = new InSANEStructureFunctionsFromPDFs();
   myBBS->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());
   TF1 * xF1pB = new TF1("xF1pB", myBBS, &InSANEStructureFunctions::EvaluateF1p, 
                                0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF1pB,"F1p BBS","l");
   xF1pB->SetParameter(0,Q2);
   xF1pB->SetLineColor(kGreen);

   // -----------------
   // NMC 
   NMC95StructureFunctions * NMCSFs = new NMC95StructureFunctions();
   TF1 * xF1pNMC = new TF1("xF1pNMC", NMCSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF1pNMC,"F1p NMC95","l");
   xF1pNMC->SetLineColor(4);
   xF1pNMC->SetParameter(0,Q2);

   // ----------------
   // Composite Structure FUnctions
   LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   TF1 * xF1pC = new TF1("xF1pC", sf, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   //((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   xF1pC->SetParameter(0,Q2);
   xF1pC->SetLineColor(7);
   leg->AddEntry(xF1pC,Form("Composite SFs:%s",sf->fLabel.Data()),"l");

   // EMC effect
   TF1 * emc = new TF1("emc", EMC_Effect, 0.005, 0.9, 1);
   emc->SetParameter(0 , 12.0);

   TF1 * F2n = new TF1("xF2d", sf, &InSANEStructureFunctions::EvaluateF2n, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2n");
   TF1 * F2d = new TF1("xF2d", sf, &InSANEStructureFunctions::EvaluateF2d, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2d");
   TF1 * F2He3 = new TF1("xF2He3", sf, &InSANEStructureFunctions::EvaluateF2Nuclear, 
               0, 1, 3,"InSANEStructureFunctions","EvaluateF2Nuclear");

   F2n->SetParameter(    0,2.0);
   F2d->SetParameter(    0,2.0);
   F2He3->SetParameter(  0,2.0);

   F2n->SetParameter(    1,6.0);
   F2d->SetParameter(    1,6.0);
   F2He3->SetParameter(  1,6.0);

   F2n->SetParameter(    2,12.0);
   F2d->SetParameter(    2,12.0);
   F2He3->SetParameter(  2,12.0);

   TH1F * hF2d   = F2d->DrawCopy("goff")->GetHistogram();
   TH1F * hF2He3 = F2He3->DrawCopy("goff")->GetHistogram();

   F2n->SetLineColor(8);
   F2d->SetLineColor(2);
   F2He3->SetLineColor(4);
   hF2He3->Divide(hF2d);
   hF2He3->SetLineColor(1);
   hF2He3->Scale(1.0/12.0);

   // -----------------------
   // 2D
   TF2 * xF2_xQ2 = new TF2("xF2_xQ2", sf, &InSANEStructureFunctions::Evaluate2DF2p, 
               0.05, 0.9, 0.2,4.0, 0,"InSANEStructureFunctions","Evaluate2DF1p");

   // --------------------------------------------------------
   TCanvas * c = new TCanvas("F2Ratio","F2Ratio");

   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle(true);
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(0.0,1.5);
   emc->Draw("same");
   hF2He3->Draw("same");
   //F2n->Draw("same");
   //F2d->Draw("same");
   //F2He3->Draw("same");
   //hF2He3->Draw();
   //emc->DrawCopy();

   c->SaveAs("results/structure_functions/F2_Ratios.png");

   // ----------------------------
   TCanvas * c2 = new TCanvas("F2Ratio2","F2Ratio2");
   c2->SetFrameFillColor(12);
   TMultiGraph * mg2 = new TMultiGraph();
   TGraph * gr = 0;

   TF1 * xF1 = new TF1("xF1", sf, &InSANEStructureFunctions::EvaluatexF1p, 
               0.05, 0.99, 1,"InSANEStructureFunctions","EvaluatexF1p");
   for(int i = 0;i<20;i++) {
      xF1->SetParameter(0,0.25+0.75*i);
      xF1->SetLineColor(40+i);
      gr = new TGraph(xF1->DrawCopy("goff")->GetHistogram());
      if(i%4==0) gr->SetTitle(Form("%.2f",0.25+0.2*i));
      mg2->Add(gr);
   }

   xF2_xQ2->SetNpx(200);
   xF2_xQ2->SetNpy(200);
   xF2_xQ2->Draw("colz");
   xF2_xQ2->GetXaxis()->CenterTitle(true);
   xF2_xQ2->GetYaxis()->CenterTitle(true);
   xF2_xQ2->GetXaxis()->SetTitle("x");
   xF2_xQ2->GetYaxis()->SetTitle("Q^{2}");
   c2->SaveAs("results/structure_functions/F2_Ratios_2.png");
   //------------------------------------------------------------


   TCanvas * c3 = new TCanvas("F2Ratio3","F2Ratio3");
   c3->SetFrameFillColor(12);
   mg2->Draw("a fb l3d");
   //mg2->GetXaxis()->CenterTitle(true);
   //mg2->GetYaxis()->CenterTitle(true);
   //mg2->GetXaxis()->SetTitle("x");
   //mg2->GetYaxis()->SetTitle("Q^{2}");

   c3->SaveAs("results/structure_functions/F2_Ratios_3.png");

   return 0;
}
