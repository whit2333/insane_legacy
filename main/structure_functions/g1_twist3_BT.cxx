void g1_twist3_BT(int aNumber = 3) {

  auto sf = new InSANEPolarizedStructureFunctionsFromPDFs( new JAM15PolarizedPDFs);
  //auto sf3 = new InSANEPolarizedStructureFunctionsFromPDFs( new GSPolarizedPDFs);
  //auto sf4 = new InSANEPolarizedStructureFunctionsFromPDFs( new DSSVPolarizedPDFs);
  //auto sf5 = new InSANEPolarizedStructureFunctionsFromPDFs( new Stat2015PolarizedPDFs);

  std::vector<TGraph*> grs_g1_BT;
  std::vector<TGraph*> grs_g1_t3_TMC;
  std::vector<TGraph*> grs_g1_t3;
  std::vector<TGraph*> grs_g1_diff;
  std::vector<TGraph*> grs_g1_diff2;
  std::vector<TGraph*> grs_D_diff;
  std::vector<int> colors = {1,2,4,6,8,9};

  double x_min = 0.05;
  double x_max = 0.9;
  int    N     = 20;
  double dx    = (x_max-x_min)/double(N-1);

  double Q2_min = 1.0;
  double Q2_max = 10.0;
  int    NQ2    = 4;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2-1);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    grs_g1_BT.push_back(  new TGraph() );
    grs_g1_t3.push_back(  new TGraph() );
    grs_g1_t3_TMC.push_back(  new TGraph() );
    grs_g1_diff.push_back(  new TGraph() );
    grs_g1_diff2.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    for (int ix = 0; ix < N; ix++) {

      double x = x_min + double(ix)*dx;

      auto pdfs = sf->GetPolarizedPDFs();

      double g1_BT      = pdfs->g1p_BT_Twist3(x, Q2);
      grs_g1_BT[iQ2]->SetPoint( ix, x, g1_BT);

      double g1_t3_TMC      = pdfs->g1p_Twist3_TMC(x, Q2);
      grs_g1_t3_TMC[iQ2]->SetPoint( ix, x, g1_t3_TMC);

      double g1_t3      = pdfs->g1p_Twist3(x, Q2);
      grs_g1_t3[iQ2]->SetPoint( ix, x, g1_t3);

      double g1_diff      = g1_t3_TMC-g1_BT;
      grs_g1_diff[iQ2]->SetPoint( ix, x, g1_diff);

      double g1_diff2      = 100.0*(g1_t3_TMC-g1_BT)/g1_t3_TMC;
      grs_g1_diff2[iQ2]->SetPoint( ix, x, g1_diff2);

      //double Dp      = pdfs->Dp_Twist3(x,Q2);
      //double Dp_BT   = pdfs->Dp_BT(x,Q2);
      //double D_diff  = Dp-Dp_BT;
      //grs_D_diff[iQ2]->SetPoint( ix, x, D_diff);

      //double d2_TMC      = aSF->d2p_tilde_TMC(Q2, x_min, x_max);
      //grs_d2_TMC[iSF]->SetPoint(iQ2, Q2, d2_TMC);

    }

    //---------------------------------------
    //
    auto pdf = sf->GetPolarizedPDFs();
    double x_avg = (x_min+x_max)/2.0;
    std::cout << "g_1^{t3    }(" << x_avg << ") = " << pdf->g1p_Twist3(    x_avg, Q2) << '\n';
    std::cout << "g_1^{t3 TMC}(" << x_avg << ") = " << pdf->g1p_Twist3_TMC(x_avg, Q2) << '\n';
    std::cout << "g_1^{t3 BT }(" << x_avg << ") = " << pdf->g1p_BT_Twist3( x_avg, Q2) << '\n';

  }

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.8,0.6,0.975,0.975);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_g1_diff[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_g1_diff[iQ2]->SetMarkerStyle(20);
    grs_g1_diff[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_g1_diff[iQ2]->SetLineWidth(2);
    grs_g1_diff[iQ2]->SetLineStyle(1+(iQ2/(colors.size())) );
    
    mg->Add(      grs_g1_diff[iQ2],"l");
    leg->AddEntry(grs_g1_diff[iQ2], Form("Q2=%.1f",Q2), "l");

  }

  mg->Draw("a");
  //mg->GetYaxis()->SetRangeUser(0.0,0.001);
  mg->GetXaxis()->SetLimits(0.001,1.0);
  mg->GetYaxis()->SetTitle("g_1(t3+TMC) - g_1(BT)");
  mg->GetXaxis()->SetTitle("x");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  leg->Draw();

  c->SaveAs(Form("results/structure_functions/g1_twist3_BT_%d.png",aNumber));
  c->SaveAs(Form("results/structure_functions/g1_twist3_BT_%d.pdf",aNumber));

  //---------------------------------------
  //
  c   = new TCanvas();
  mg  = new TMultiGraph();
  leg = new TLegend(0.8,0.6,0.975,0.975);
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_g1_diff2[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_g1_diff2[iQ2]->SetMarkerStyle(20);
    grs_g1_diff2[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_g1_diff2[iQ2]->SetLineWidth(2);
    grs_g1_diff2[iQ2]->SetLineStyle(1+(iQ2/(colors.size())) );
    
    mg->Add(      grs_g1_diff2[iQ2],"l");
    leg->AddEntry(grs_g1_diff2[iQ2], Form("Q2=%.1f",Q2), "l");

  }

  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(-100.0,100.0);
  mg->GetXaxis()->SetLimits(0.001,1.0);
  mg->GetYaxis()->SetTitle("difference [\%]");
  mg->GetXaxis()->SetTitle("x");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  leg->Draw();

  c->SaveAs(Form("results/structure_functions/g1_twist3_BT_percentDiff_%d.png",aNumber));
  c->SaveAs(Form("results/structure_functions/g1_twist3_BT_percentDiff_%d.pdf",aNumber));
}

