
Int_t A2pCompare(Double_t Q2 = 8.0) {

   Int_t paraFileVersion = 313;
   Int_t perpFileVersion = 313;

   // -----------------------------------------
   // SANE DATA
   TFile * fout = new TFile(Form("data/A1A2_59_noel_%d_%d.root",paraFileVersion,perpFileVersion),"READ"); 
   fout->cd();
   fout->ls();
   TList * fA2Asymmetries = (TList*) gROOT->FindObject(Form("A2-59-x-%d",0));
   if(!fA2Asymmetries) return -22;

   TList * fAsym59 = (TList*)gROOT->FindObject(Form("A2asym-59-x-%d",0)); 
   if(!fAsym59) return -24;

   TFile * fout2 = new TFile(Form("data/A1A2_47_noel_%d_%d.root",paraFileVersion,perpFileVersion),"READ"); 
   fout2->cd();
   fout2->ls();
   TList * fA2Asymmetries47 = (TList*) gROOT->FindObject(Form("A2-47-x-%d",0));
   if(!fA2Asymmetries47) return -23;

   TList * fAsym47 = (TList*)gROOT->FindObject(Form("A2asym-47-x-%d",0)); 
   if(!fAsym47) return -24;

   TMultiGraph * mg_sane   = new TMultiGraph();
   TMultiGraph * mg_sane_W = new TMultiGraph();

   // -----------------------------------------
   // 5.9 GeV data
   for(int jj =0;jj<4;jj++) {

      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)fAsym59->At(jj);
      TH1F * h1 = asym->fAsymmetryVsx;//(TH1F*)fA2Asymmetries->At(i);
      InSANEAveragedKinematics1D * avgKine = asym->fAvgKineVsx;

      Int_t   xBinmax = h1->GetNbinsX();
      Int_t   yBinmax = h1->GetNbinsY();
      Int_t   zBinmax = h1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      TGraphErrors * grVsW = new TGraphErrors( h1->GetNbinsX() ); 
      grVsW->SetMarkerStyle(20);
      grVsW->SetMarkerColor(h1->GetMarkerColor());
      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax+1; i++){
         for(Int_t j=0; j<= yBinmax+1; j++){
            for(Int_t k=0; k<= zBinmax+1; k++){

               bin   =h1->GetBin(i,j,k);
               h1->GetBinXYZ(bin,binx,biny,binz);

               Double_t A2 =  h1->GetBinContent(bin);
               Double_t eA2 =  h1->GetBinError(bin);

               Double_t W     = avgKine->fW->GetBinContent(bin);

               grVsW->SetPoint(i,W,A2);
               grVsW->SetPointError(i,0,eA2);

            }
         }
      }

      TGraphErrors * gr = 0;

      gr = grVsW;
      gr->SetMarkerStyle(20);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.8 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      mg_sane_W->Add(grVsW,"p");

      // graphs
      gr = new TGraphErrors( h1 ); 
      gr->SetMarkerStyle(20);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.8 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      mg_sane->Add(gr,"p");
   }

   // -----------------------------------------
   // 4.7 GeV data
   for(int jj =0;jj<4;jj++) {

      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)fAsym47->At(jj);
      TH1F * h1 = asym->fAsymmetryVsx;//(TH1F*)fA2Asymmetries->At(i);
      InSANEAveragedKinematics1D * avgKine = asym->fAvgKineVsx;

      Int_t   xBinmax = h1->GetNbinsX();
      Int_t   yBinmax = h1->GetNbinsY();
      Int_t   zBinmax = h1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      TGraphErrors * grVsW = new TGraphErrors( h1->GetNbinsX() ); 
      grVsW->SetMarkerStyle(20);
      grVsW->SetMarkerColor(h1->GetMarkerColor());
      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax+1; i++){
         for(Int_t j=0; j<= yBinmax+1; j++){
            for(Int_t k=0; k<= zBinmax+1; k++){

               bin   =h1->GetBin(i,j,k);
               h1->GetBinXYZ(bin,binx,biny,binz);

               Double_t A2 =  h1->GetBinContent(bin);
               Double_t eA2 =  h1->GetBinError(bin);

               Double_t W     = avgKine->fW->GetBinContent(bin);

               grVsW->SetPoint(i,W,A2);
               grVsW->SetPointError(i,0,eA2);

            }
         }
      }

      TGraphErrors * gr = 0;

      gr = grVsW;
      gr->SetMarkerStyle(21);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.8 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      mg_sane_W->Add(grVsW,"p");

      // graphs
      gr = new TGraphErrors( h1 ); 
      gr->SetMarkerStyle(21);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.8 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      mg_sane->Add(gr,"p");
   }
   // -----------------------------------------


   // -----------------------------------------

   //gStyle->SetPaperSize(10.,10.);
   gSystem->Load("libNucDB");

   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Qsq1 = new NucDBBinnedVariable("Qsquared",Form("Q^{2} %.1f<Q^{2}<%.1f",Q2-0.5,Q2+0.5));
   Qsq1->SetBinMinimum(Q2-0.5);
   Qsq1->SetBinMaximum(Q2+0.5);

   NucDBMeasurement * A2pQsq1 = new NucDBMeasurement("A2pQsq1",Form("A_{2}^{p} %s",Qsq1->GetTitle()));

   TLegend * leg = new TLegend(0.5, 0.7, 0.88, 0.9);
   //leg->SetHeader("A_{2}^{p} measurments");
   leg->SetFillStyle(0);
   leg->SetFillColor(0);
   leg->SetBorderSize(0);


   TList * measurementsList = manager->GetMeasurements("A2p");
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg_W = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement * g1p = (NucDBMeasurement*)measurementsList->At(i);
      TGraphErrors * graph = g1p->BuildGraph("x");
      //graph->SetLineColor(1);
      graph->SetMarkerStyle(20+i);
      leg->AddEntry(graph, Form("%s A2p", g1p->GetExperimentName()), "ep");
      mg->Add(graph,"p");

      A2pQsq1->AddDataPoints(g1p->FilterWithBin(Qsq1));

      TGraphErrors * graphW = g1p->BuildGraph("W");
      graphW->SetMarkerStyle(20+i);
      mg_W->Add(graphW,"p");

      TGraphErrors * grkine = g1p->BuildKinematicGraph("W","Qsquared");
      grkine->SetMarkerStyle(20+i);
      if(grkine) mg2->Add(grkine,"p");
   }

   TGraphErrors * graphW = A2pQsq1->BuildGraph("W");
   graphW->SetMarkerStyle(22);
   graphW->SetMarkerColor(2);
   graphW->SetMarkerSize(0.8);
   graphW->SetLineColor(2);
   mg_W->Add(graphW,"p");


   graphW = A2pQsq1->BuildGraph("x");
   graphW->SetMarkerStyle(22);
   graphW->SetMarkerColor(2);
   graphW->SetMarkerSize(0.8);
   graphW->SetLineColor(2);
   mg->Add(graphW,"p");

   leg->AddEntry(graphW,Form("World %s",A2pQsq1->GetTitle() ),"ep");

   // -----------------------------------------------------------------
   // Oscar's Fit
   TF1 * fOscar = new TF1("A2Oscar","[0]+[1]/x+[2]/[3]",1,7.0);
   fOscar->SetParameter(0,0.024);
   fOscar->SetParameter(1,0.131);
   fOscar->SetParameter(2,-0.042);
   fOscar->SetParameter(3,TMath::Sqrt(Q2)); // Q2

   // vs x
   TF1 * fOscar_x = new TF1("A2Oscarx","[0] + [1]/InSANE::Kine::W_xQsq(x,[3]) + [2]/[3]",0.0,1.0);
   fOscar_x->SetParameter(0,0.024);
   fOscar_x->SetParameter(1,0.131);
   fOscar_x->SetParameter(2,-0.042);
   fOscar_x->SetParameter(3,TMath::Sqrt(Q2)); // Q2
   fOscar_x->SetLineColor(2);

   // -----------------------------------------------------------------
   // Plots
   TLegend * leg2 = (TLegend*)leg->Clone();
   leg2->SetX1NDC(0.15);
   leg2->SetX2NDC(0.5);

   // ---------------------------------------------------------
   // A2p vs x
   TCanvas * c = new TCanvas();
   gPad->SetGridy(1);

   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.3,0.6);
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->Draw("a");

   fOscar_x->DrawCopy("same");

   fOscar_x->SetLineColor(8);
   fOscar_x->SetParameter(3,TMath::Sqrt(2)); // Q2
   fOscar_x->DrawCopy("same");

   leg2->Draw();

   c->SaveAs("results/structure_functions/A2pCompare.png");
   c->SaveAs("results/structure_functions/A2pCompare.pdf");

   // With sane data
   mg->Add(mg_sane);
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.3,0.6);
   mg->Draw("a");

   fOscar_x->SetLineColor(2);
   fOscar_x->SetParameter(3,TMath::Sqrt(Q2)); // Q2
   fOscar_x->DrawCopy("same");

   fOscar_x->SetLineColor(8);
   fOscar_x->SetParameter(3,TMath::Sqrt(2)); // Q2
   fOscar_x->DrawCopy("same");

   leg2->Draw();
   c->SaveAs("results/structure_functions/A2pCompareSANE.png");
   c->SaveAs("results/structure_functions/A2pCompareSANE.pdf");

   // ---------------------------------------------------------
   // Q2 vs x Kinematics
   TCanvas * c2 = new TCanvas();
   mg2->Draw("a");
   leg->Draw();

   c2->SaveAs("results/structure_functions/A2pCompareWQ2_2.png");
   c2->SaveAs("results/structure_functions/A2pCompareWQ2_2.pdf");

   // ---------------------------------------------------------
   // A2p vs W (calculated at the bin level) 
   TCanvas * c3 = new TCanvas();
   gPad->SetGridy(1);
   gPad->SetLogx(true);

   mg_W->Draw("a");
   mg_W->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg_W->GetXaxis()->SetLimits(0.9,7.0);

   fOscar->SetLineColor(2);
   fOscar->SetParameter(3,TMath::Sqrt(Q2)); // Q2
   fOscar->DrawCopy("same");

   fOscar->SetLineColor(8);
   fOscar->SetParameter(3,TMath::Sqrt(2)); // Q2
   fOscar->DrawCopy("same");

   leg->Draw();

   c3->SaveAs("results/structure_functions/A2pCompare_3.png");
   c3->SaveAs("results/structure_functions/A2pCompare_3.pdf");

   // With sane data
   mg_W->Add(mg_sane_W);
   mg_W->Draw("a");
   mg_W->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg_W->GetXaxis()->SetLimits(0.9,7.0);

   fOscar->SetLineColor(2);
   fOscar->SetParameter(3,TMath::Sqrt(Q2)); // Q2
   fOscar->DrawCopy("same");

   fOscar->SetLineColor(8);
   fOscar->SetParameter(3,TMath::Sqrt(2)); // Q2
   fOscar->DrawCopy("same");

   leg->Draw();

   c3->SaveAs("results/structure_functions/A2pCompareSANE_3.png");
   c3->SaveAs("results/structure_functions/A2pCompareSANE_3.pdf");

   gPad->SetLogx(false);
   mg_W->Add(mg_sane_W);
   mg_W->Draw("a");
   mg_W->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg_W->GetXaxis()->SetLimits(1.0,3.0);

   fOscar->SetLineColor(2);
   fOscar->SetParameter(3,TMath::Sqrt(Q2)); // Q2
   fOscar->DrawCopy("same");

   fOscar->SetLineColor(8);
   fOscar->SetParameter(3,TMath::Sqrt(2)); // Q2
   fOscar->DrawCopy("same");

   leg->Draw();

   c3->SaveAs("results/structure_functions/A2pCompareSANE_lowW_3.png");
   c3->SaveAs("results/structure_functions/A2pCompareSANE_lowW_3.pdf");

}
