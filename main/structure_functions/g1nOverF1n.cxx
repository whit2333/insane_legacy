/** Plots models and data for g1n at Qsq */
Int_t g1nOverF1n(Double_t Qsq = 5.0,Double_t QsqBinWidth=2.0){
   TCanvas * c = new TCanvas("g1n","g1n");
   /// Create Legend
   TLegend * leg = new TLegend(0.6,0.2,0.875,0.575);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   /// First Plot all the Polarized Structure Function Models! 
//    InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
//    pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
//    InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
//    pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
//    InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
//    pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
//    InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
//    pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);
// 
//    Int_t npar=1;
//    Double_t xmin=0.01;
//    Double_t xmax=1.0;
//    TF1 * xg1n = new TF1("xg1n", pSFs, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
//                xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
//    TF1 * xg1nA = new TF1("xg1nA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
//                xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
//    TF1 * xg1nB = new TF1("xg1nB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
//                xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
//    TF1 * xg1nC = new TF1("xg1nC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
//                xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
//    xg1n->SetParameter(0,Qsq);
//    xg1nA->SetParameter(0,Qsq);
//    xg1nB->SetParameter(0,Qsq);
//    xg1nC->SetParameter(0,Qsq);
// 
//    xg1n->SetLineColor(1);
//    xg1nA->SetLineColor(3);
//    xg1nB->SetLineColor(kOrange -1);
//    xg1nC->SetLineColor(kViolet-2);
// 
//    Int_t width = 3;
//    xg1n->SetLineWidth(width);
//    xg1nA->SetLineWidth(width);
//    xg1nB->SetLineWidth(width);
//    xg1nC->SetLineWidth(width);
// 
//    xg1n->SetLineStyle(1);
//    xg1nA->SetLineStyle(1);
//    xg1nB->SetLineStyle(1);
//    xg1nC->SetLineStyle(1);
// 
// 
//    xg1n->Draw();
//    xg1nA->Draw("same");
//    xg1nB->Draw("same");
//    xg1nC->Draw("same");
// 

   /// Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();
   /// Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);
   /// Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g1nQsq1 = new NucDBMeasurement("g1nQsq1",Form("g_{1}^{n} %s",Qsqbin->GetTitle()));

   TList * listOfMeas = manager->GetMeasurements("g1n/F1n");
   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g1nMeas = (NucDBMeasurement*)listOfMeas->At(i);
       g1nMeas->BuildGraph();

       g1nMeas->fGraph->SetMarkerStyle(20+i);
       g1nMeas->fGraph->SetMarkerSize(1.6);
       g1nMeas->fGraph->SetMarkerColor(kBlue-2-i);
       g1nMeas->fGraph->SetLineColor(kBlue-2-i);
       g1nMeas->fGraph->SetLineWidth(1);

       if(i==0) {
          g1nMeas->fGraph->Draw("ap");
          g1nQsq1->AddDataPoints(g1nMeas->FilterWithBin(Qsqbin));
       }else{
          g1nMeas->fGraph->Draw("p");
          g1nQsq1->AddDataPoints(g1nMeas->FilterWithBin(Qsqbin));
       }
       leg->AddEntry(g1nMeas->fGraph,Form("%s %s",g1nMeas->fExperiment.Data(),g1nMeas->GetTitle() ),"lp");

   }

   g1nQsq1->BuildGraph();
   g1nQsq1->fGraph->SetMarkerColor(2);
   g1nQsq1->fGraph->SetLineColor(2);
   g1nQsq1->fGraph->Draw("p");


   /// Complete the Legend 

   leg->AddEntry(g1nQsq1->fGraph,Form("%s %s","All g1n data",g1nQsq1->GetTitle() ),"ep");
//    leg->AddEntry(xg1n,"g1n BB ","l");
//    leg->AddEntry(xg1nA,"g1n DNS2005 ","l");
//    leg->AddEntry(xg1nB,"g1n AAC ","l");
//    leg->AddEntry(xg1nC,"g1n GS ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.25,0.35,Form("g_{1}^{n}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));


   c->SaveAs("results/structure_functions/g1nOverF1n.png");
   c->SaveAs("results/structure_functions/g1nOverF1n.ps");

   return(0);
}
