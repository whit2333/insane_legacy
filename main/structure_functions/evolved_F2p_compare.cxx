Int_t evolved_F2p_compare(Int_t aNumber = 1) {


   NucDBManager * dbman = NucDBManager::GetManager();

   NucDBBinnedVariable * Q2bin1  = new NucDBBinnedVariable("Qsquared","Q2bin",2,0.3);
   NucDBBinnedVariable * Q2bin2  = new NucDBBinnedVariable("Qsquared","Q2bin",90,60);
   NucDBBinnedVariable * Q2bin3  = new NucDBBinnedVariable("Qsquared","Q2bin",1.2e3,1.0e3);

   TList * all_meas =  dbman->GetMeasurements("F2p");
   all_meas->Print("v");

   TList * meas_low_Q2  = NucDB::FilterMeasurements(all_meas,Q2bin1);
   TList * meas_high_Q2 = NucDB::FilterMeasurements(all_meas,Q2bin2);
   TList * meas_Q2_3 = NucDB::FilterMeasurements(all_meas,Q2bin3);

   meas_Q2_3->Print("v");

   TLegend     * leg   = new TLegend(0.5,0.6,0.7,0.85);
   TLegend     * leg2  = new TLegend(0.7,0.6,0.85,0.85);
   TLegend     * leg3  = new TLegend(0.7,0.5,0.85,0.6);
   leg->SetBorderSize(0);
   leg2->SetBorderSize(0);
   leg3->SetBorderSize(0);

   TMultiGraph * mg1  = NucDB::CreateMultiGraph(meas_low_Q2, "x",1);
   TMultiGraph * mg2  = NucDB::CreateMultiGraph(meas_high_Q2,"x",2);
   TMultiGraph * mg3  = NucDB::CreateMultiGraph(meas_Q2_3,"x",4);
   NucDB::FillLegend(leg,meas_low_Q2,mg1);
   NucDB::FillLegend(leg2,meas_high_Q2,mg2);
   NucDB::FillLegend(leg3,meas_Q2_3,mg3);

   // ---------------------------------------------------
   //
   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CJ12UnpolarizedPDFs());
   TF1 * xF2pCTEQ = new TF1("xF2pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF2p, 
                                        0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   //leg->AddEntry(xF2pCTEQ,"CJ12","l");
   xF2pCTEQ->SetParameter(0,90);
   xF2pCTEQ->SetLineColor(1);
   //cteqSFs->SetUseR(false);
   //cteqSFs->CalculateTMCs(true);

   InSANEStructureFunctionsFromPDFs * sfA = new InSANEStructureFunctionsFromPDFs();
   sfA->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   TF1 * xF2pA = new TF1("xF2pA", sfA, &InSANEStructureFunctions::EvaluateF2p, 
                                0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(xF2pA,"CTEQ6","l");
   xF2pA->SetLineStyle(1);
   //sfA->SetUseR(false);
   //sfA->CalculateTMCs(true);

   InSANEStructureFunctionsFromPDFs * myBBS = new InSANEStructureFunctionsFromPDFs();
   myBBS->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());
   TF1 * xF2pB = new TF1("xF2pB", myBBS, &InSANEStructureFunctions::EvaluateF2p, 
                                0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(xF2pB,"Stat. Model","l");
   xF2pB->SetLineStyle(2);

   LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   TF1 * xF2pC = new TF1("xF2pC", sf, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   //((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   xF2pC->SetLineColor(7);


   // -----------------------------
   // 
   TCanvas * c = new TCanvas();

   gPad->SetLogx(true);
   //mg1->Add(mg3);
   mg1->Add(mg2);

   mg1->Draw("a");
   mg1->GetXaxis()->SetLimits(0.0005,1.0);
   mg1->GetXaxis()->CenterTitle(true);
   mg1->GetXaxis()->SetTitle("x");
   leg->Draw();
   leg2->Draw();

   // -----------------------------
   //xF2pA->SetParameter(0,1.2e3);
   //xF2pA->SetLineColor(4);
   //xF2pA->DrawCopy("same");

   //xF2pA->SetParameter(0,90);
   //xF2pA->SetLineColor(4);
   //xF2pA->DrawCopy("same");

   //xF2pA->SetParameter(0,2.0);
   //xF2pA->SetLineColor(4);
   //xF2pA->DrawCopy("same");

   // -----------------------------
   //xF2pB->SetParameter(0,1.2e3);
   //xF2pB->SetLineColor(2);
   //xF2pB->DrawCopy("same");

   xF2pB->SetParameter(0,90);
   xF2pB->SetLineColor(2);
   xF2pB->DrawCopy("same");

   xF2pB->SetParameter(0,2.0);
   xF2pB->SetLineColor(1);
   xF2pB->DrawCopy("same");

   // -----------------------------
   //xF2pCTEQ->SetParameter(0,90);
   //xF2pCTEQ->SetLineColor(2);
   //xF2pCTEQ->DrawCopy("same");

   //xF2pCTEQ->SetParameter(0,2.0);
   //xF2pCTEQ->SetLineColor(1);
   //xF2pCTEQ->DrawCopy("same");

   TLatex * t = new TLatex();
   t->SetTextSize(0.04);
   t->SetTextAlign(12);
   t->DrawLatex(0.001,1.6,"Q^{2} = 90 GeV^{2}");

   t->DrawLatex(0.002,0.25,"Q^{2} = 2 GeV^{2}");


   c->SaveAs(Form("results/structure_functions/evolved_F2p_compare_%d.png",aNumber));
   c->SaveAs(Form("results/structure_functions/evolved_F2p_compare_%d.pdf",aNumber));
   c->SaveAs(Form("results/structure_functions/evolved_F2p_compare_%d.svg",aNumber));

   return 0;
}
