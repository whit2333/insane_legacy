Int_t composite(){

   LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   TF1 * xF2p = new TF1("xF2p", sf, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   //((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   xF2p->SetParameter(0,10.5);
   xF2p->SetLineColor(2);
   //leg->AddEntry(xF2p,Form("Composite SFs:%s at Q^2=10.5",sf->fLabel.Data()),"l");
   xF2p->DrawClone();

   return(0);
}
