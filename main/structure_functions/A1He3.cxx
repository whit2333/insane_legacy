// Use the implementation in the InSANE code base to build the A1 asymmetry 

#include <cstdlib> 
#include <iostream> 
#include <fstream> 
#include "TMultiGraph.h"

void A1He3(){

	Double_t x,Q2; 

	// cout << "Enter x: ";
	// cin  >> x; 
	cout << "Enter Q2 (GeV): ";
	cin  >> Q2; 

        // Use DSSV as the polarized PDF model 
	DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs(); 
        InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs(); 
        DSSVSF->SetPolarizedPDFs(DSSV); 

        // Use CTEQ as the unpolarized PDF model 
        CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
        InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
        CTEQSF->SetUnpolarizedPDFs(CTEQ); 
 
        // Use NMC95 as the unpolarized SF model 
        NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
        F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

        // Asymmetry class: Use NMC
        InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
	Asym1->SetPolarizedSFs(DSSVSF);
	Asym1->SetUnpolarizedSFs(NMC95);  
        // Asymmetry class: Use F1F209 
        InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
	Asym2->SetPolarizedSFs(DSSVSF);
	Asym2->SetUnpolarizedSFs(F1F209);  
        // Asymmetry class: Use CTEQ  
        InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
	Asym3->SetPolarizedSFs(DSSVSF);
	Asym3->SetUnpolarizedSFs(CTEQSF);  

	Int_t npar = 1;
	Double_t xmin = 0.01;
	Double_t xmax = 1.00;
	Double_t xmin1 = 0.01;
	Double_t xmax1 = 1.00;
	Double_t xmin2 = 0.2;
	Double_t xmax2 = 1.00;
	Double_t xmin3 = 0.01;
	Double_t xmax3 = 1.00;
	Double_t ymin = -0.3;
	Double_t ymax =  0.3; 

        // Plot A1He3(x,Q2=const) 
        // WARNING! Plot is for CONSTANT Q2! 
        // we set Q2 for the evaluation of the function: &InSANEPolarizedStructureFunctions::Evaluateg1n
 	TF1 * Model1 = new TF1("Model1",Asym1,&InSANEAsymmetryBase::EvaluateA1He3,
 			xmin1, xmax1, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1He3");
 
 	TF1 * Model2 = new TF1("Model2",Asym2,&InSANEAsymmetryBase::EvaluateA1He3,
 			xmin2, xmax2, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1He3");
 
	TF1 * Model3 = new TF1("Model3",Asym3,&InSANEAsymmetryBase::EvaluateA1He3,
			xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1He3");

	TString Title;
	Int_t width = 3;
	
	Model1->SetParameter(0,Q2);
	Model1->SetLineColor(kMagenta);
	Model1->SetLineWidth(width);
	Model1->SetLineStyle(1);
	Model1->SetTitle(Title);
	Model1->GetYaxis()->SetRangeUser(ymin,ymax); 

	Model2->SetParameter(0,Q2);
	Model2->SetLineColor(kCyan);
	Model2->SetLineWidth(width);
	Model2->SetLineStyle(1);
	Model2->SetTitle(Title);
	Model2->GetYaxis()->SetRangeUser(ymin,ymax); 
	
	Model3->SetParameter(0,Q2);
	Model3->SetLineColor(kGreen);
	Model3->SetLineWidth(width);
	Model3->SetLineStyle(1);
	Model3->SetTitle(Title);
	Model3->GetYaxis()->SetRangeUser(ymin,ymax); 

        // Load in world data on A1He3 from NucDB  
	gSystem->Load("libNucDB");
	NucDBManager * manager = NucDBManager::GetManager();

	TLegend *leg = new TLegend(0.1, 0.7, 0.48, 0.9);
        leg->SetFillColor(kWhite); 

        TMultiGraph *MG = new TMultiGraph(); 

	TList * measurementsList = manager->GetMeasurements("A1He3");
	for (int i = 0; i < measurementsList->GetEntries(); i++) {
		NucDBMeasurement *A1He3World = (NucDBMeasurement*)measurementsList->At(i);
                // Get TGraphErrors object 
		TGraphErrors *graph        = A1He3World->BuildGraph("x");
		graph->SetMarkerStyle(20);
                // Set legend title 
		Title = Form("%s",A1He3World->GetExperimentName()); 
		leg->AddEntry(graph,Title,"p");
                // Add to TMultiGraph 
                MG->Add(graph); 
	}

        leg->AddEntry(Model1,"DSSV and NMC95 model","l");
        leg->AddEntry(Model2,"DSSV and F1F209 model","l");
        leg->AddEntry(Model3,"DSSV and CTEQ model","l");

	TCanvas * c = new TCanvas("c","A1He3",1000,800);
	c->SetFillColor(kWhite); 
        c->cd(); 

        TString Measurement = Form("A_{1}^{^{3}He}");
	TString GTitle      = Form("%s (x,Q^{2} = %.2f GeV^{2})",Measurement.Data(),Q2);
        TString xAxisTitle  = Form("x");
        TString yAxisTitle  = Form("%s",Measurement.Data());

        // Draw everything 
        MG->Draw("AP");
	MG->SetTitle(GTitle);
	MG->GetXaxis()->SetTitle(xAxisTitle);
	MG->GetXaxis()->CenterTitle();
	MG->GetYaxis()->SetTitle(yAxisTitle);
	MG->GetYaxis()->CenterTitle();
        MG->GetXaxis()->SetLimits(xmin1,xmax1); 
        MG->GetYaxis()->SetRangeUser(ymin,ymax); 
        MG->Draw("AP");
	Model1->Draw("same");
	Model2->Draw("same");
	Model3->Draw("same");
	leg->Draw();

}
