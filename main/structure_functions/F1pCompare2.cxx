/**  
 */
Int_t F1pCompare2(Double_t Q2 = 0.75, Double_t dQ2 = 0.5) {

   //gSystem->AddIncludePath("-I/usr/local/include/LHAPDF");
   //gSystem->Load("libLHAPDF.so");
   
   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Qsq = new NucDBBinnedVariable("Qsquared",Form("Q^{2} %.1f<Q^{2}<%.1f",Q2-dQ2,Q2+dQ2));
   Qsq->SetBinMinimum(Q2-dQ2);
   Qsq->SetBinMaximum(Q2+dQ2);

   Double_t Q2_2  = 2.0;
   Double_t dQ2_2 = 1.0;
   NucDBBinnedVariable * Qsq2 = new NucDBBinnedVariable("Qsquared",Form("Q^{2} %.1f<Q^{2}<%.1f",Q2_2-dQ2_2,Q2_2+dQ2_2));
   Qsq2->SetBinMinimum(Q2_2-dQ2_2);
   Qsq2->SetBinMaximum(Q2_2+dQ2_2);
   
   TList * measurementsList = manager->GetMeasurements("F1p");
   NucDBMeasurement * F1p = (NucDBMeasurement*)measurementsList->At(0);
   if(!F1p) return -22;
   NucDBMeasurement * F1pQsq1 = new NucDBMeasurement("F1pQsq1",Form("F_{1}^{p} %s",Qsq->GetTitle()));
   NucDBMeasurement * F1pQsq2 = new NucDBMeasurement("F1pQsq2",Form("F_{1}^{p} %s",Qsq2->GetTitle()));
   
   TLegend * leg = new TLegend(0.6,0.7,0.89,0.89);
   leg->SetFillColor(0);
   leg->SetBorderSize(0);
   //leg->SetHeader("F1p");

   //TMultiGraph * mg = manager->GetMultiGraph("F1p","x");
   TMultiGraph * mg = new TMultiGraph();
   //leg->AddEntry(F1p->fGraph,Form("%s %s",F1p->GetExperimentName(),F1p->GetTitle() ),"ep");

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *aMeas = (NucDBMeasurement*)measurementsList->At(i);
      F1pQsq1->AddDataPoints(aMeas->FilterWithBin(Qsq));
      F1pQsq2->AddDataPoints(aMeas->FilterWithBin(Qsq2));
   }

   TGraphErrors * graph1 = F1pQsq1->BuildGraph("x");
   graph1->SetMarkerColor(4);
   graph1->SetLineColor(4);
   mg->Add(graph1,"p");
   leg->AddEntry(graph1,Form("%s %s",F1p->GetExperimentName(),F1pQsq1->GetTitle() ),"ep");

   TGraphErrors * graph2 = F1pQsq2->BuildGraph("x");
   graph2->SetMarkerColor(2);
   graph2->SetLineColor(2);
   mg->Add(graph2,"p");
   //graph2->Draw("p");
   leg->AddEntry(graph2,Form("%s %s",F1p->GetExperimentName(),F1pQsq2->GetTitle() ),"ep");


   // -----------------
   // CTEQ
   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   TF1 * xF1pCTEQ = new TF1("xF1pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF1p, 
                                        0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF1pCTEQ,"F1p CTEQ","l");
   xF1pCTEQ->SetParameter(0,Q2);
   xF1pCTEQ->SetLineColor(6);

   // -----------------
   // F1F209
   InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();
   TF1 * xF1pA = new TF1("xF1pA", f1f2SFs, &InSANEStructureFunctions::EvaluateF1p, 
                                  0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF1pA,"F1p F1F209","l");
   xF1pA->SetParameter(0,Q2);
   xF1pA->SetLineColor(1);

   // -----------------
   // Statistical 
   InSANEStructureFunctionsFromPDFs * myBBS = new InSANEStructureFunctionsFromPDFs();
   myBBS->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());
   TF1 * xF1pB = new TF1("xF1pB", myBBS, &InSANEStructureFunctions::EvaluateF1p, 
                                0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF1pB,"F1p BBS","l");
   xF1pB->SetParameter(0,Q2);
   xF1pB->SetLineColor(kGreen);

   // -----------------
   // NMC 
   NMC95StructureFunctions * NMCSFs = new NMC95StructureFunctions();
   TF1 * xF1pNMC = new TF1("xF1pNMC", NMCSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF1pNMC,"F1p NMC95","l");
   xF1pNMC->SetLineColor(4);
   xF1pNMC->SetParameter(0,Q2);

   // ----------------
   // Composite Structure FUnctions
   LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   TF1 * xF1pC = new TF1("xF1pC", sf, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   //((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   xF1pC->SetParameter(0,Q2);
   xF1pC->SetLineColor(7);
   leg->AddEntry(xF1pC,Form("Composite SFs:%s",sf->fLabel.Data()),"l");
   // -----------------------

   TCanvas * c = new TCanvas("F1p","F1p");

   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle(true);
   mg->Draw("a");
   xF1pCTEQ->Draw("same");
   xF1pA->Draw("same");
   xF1pB->Draw("same");
   xF1pNMC->DrawCopy("same");
   xF1pC->Draw("same");

   //xF1pC->DrawClone("same");

   //xF1p = new TF1("xF1p", lhaSFs, &InSANEStructureFunctions::EvaluateF1p, 
   //            0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   //
   // ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("cteq6mE", LHAPDF::LHGRID );
   //xF1p->SetParameter(0,100.5);
   //xF1p->SetLineColor(7);
   //leg->AddEntry(xF1p,Form("F1p LHAPDF:%s",lhaSFs->fLabel.Data()),"l");
   //xF1p->DrawClone("same");

   TLatex latex;
   latex.DrawLatex(0.6,0.2,Form("#font[22]{Q^{2}=%.1f (GeV/c)^{2}}",Q2));

   leg->Draw();


   c->SaveAs("results/structure_functions/F1pCompare2.png");
   c->SaveAs("results/structure_functions/F1pCompare2.pdf");

   return(0);
}
