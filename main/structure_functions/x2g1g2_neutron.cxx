//#include "util/stat_error_graph.cxx"
//#include "asym/sane_data_bins.cxx"

//int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

Int_t x2g1g2_neutron(int aNumber = 0)
{

   // ------------------------
   //sane_data_bins();

   std::cout << "derppp" << std::endl;


   ////TGraphErrors * gr = 0;
   ////TGraphErrors * gr2 = 0;

   //TMultiGraph * mg = new TMultiGraph();
   ////TMultiGraph * mg2 = new TMultiGraph();
   ////TMultiGraph * mg3 = new TMultiGraph();
   ////TMultiGraph * mg4 = new TMultiGraph();

   ////TMultiGraph * mg_x2g1 = new TMultiGraph();
   ////TMultiGraph * mg_x2g2 = new TMultiGraph();

   ////TMultiGraph * mg_syst_err = new TMultiGraph();
   ////TMultiGraph * mg_syst_err2 = new TMultiGraph();
   ////TMultiGraph * mg_syst_err3= new TMultiGraph();
   ////TMultiGraph * mg_syst_err4 = new TMultiGraph();

   //TF2 * div2 = new TF2("xf","y/2.0",0,1,-1,1);
   //TF2 * div3 = new TF2("xf","y/3.0",0,1,-1,1);

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   //TLegend *leg_sane = new TLegend(0.65, 0.65, 0.89, 0.89);
   //leg_sane->SetFillColor(0);
   //leg_sane->SetFillStyle(0);
   //leg_sane->SetBorderSize(0);

   //// Function to take g1n --> x^2 g1n
   TF2 * xf = new TF2("xf","2.0*x*x*y",0,1,-1,1);
   TF2 * xf2 = new TF2("xf2","3.0*x*x*y",0,1,-1,1);
   TF2 * f2 = new TF2("f2","2.0*y",0,1,-1,1);
   TF2 * f3 = new TF2("f3","3.0*y",0,1,-1,1);

   TF2 * fTimes2 = new TF2("ftimes2","2.0*y",0,1,-1,1);
   TF2 * fTimes3 = new TF2("ftimes3","3.0*y",0,1,-1,1);
   TF2 * x2f = new TF2("x2f","x*x*y",0,1,-1,1);

   ////TList        * fAllx2g1Asym = new TList();
   ////TList        * fAllx2g2Asym = new TList();

   //// --------------------------------------------------------------------------
   //// Functions

   Double_t Q2 = 4.0;

   ////InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   ////pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);

   ////Int_t    npar = 1;
   ////Double_t xmin = 0.01;
   ////Double_t xmax = 1.00;
   ////Double_t yAxisMax = 0.07;
   ////Double_t yAxisMin =-0.07;

   ////TF1 * x2g2n = new TF1("x2g2n", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g2n, 
   ////            xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2n");

   ////TF1 * xg1nWW = new TF1("x2g1nWW", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1nWW, 
   ////            xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g1nWW");

   ////x2g2n->SetParameter(0,Q2);
   ////xg1nWW->SetParameter(0,Q2);


   //   std::cout << "derp" << std::endl;
   //// -------------------------------------------------------
   //// x2g2n 
   //Int_t width = 1;

   TLegend * leg0 = new TLegend(0.16, 0.60, 0.40, 0.88);
   leg0->SetFillColor(0);
   leg0->SetFillStyle(0);
   leg0->SetBorderSize(0);

   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Wbin          = new NucDBBinnedVariable("W","W",20.0,18.0);
   NucDBBinnedVariable * Qsqbin        = new NucDBBinnedVariable("Qsquared","Q^{2}");
   //NucDBMeasurement    * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   Qsqbin->SetBinMinimum(1.5);
   Qsqbin->SetBinMaximum(8.5);
   Qsqbin->SetMean((1.5+8.5)/2.0);
   Qsqbin->SetAverage((1.5+8.5)/2.0);
   Qsqbin->SetTitle(Form("%.1f<Q^{2}<%.1f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));

   TMultiGraph * MG         = new TMultiGraph();
   TMultiGraph * MG_data1   = new TMultiGraph();
   TMultiGraph * mg_CLAS_g1 = new TMultiGraph();

   TList            * measurementsList = manager->GetMeasurements("g1n");
   NucDBExperiment  * exp              = 0;
   NucDBMeasurement * ames             = 0;

   Int_t markers[] = {22,32,23,33,21,25,26,27};

   for (int i = 0; i < measurementsList->GetEntries(); i++) {

      std::cout << "i= " << i <<std::endl;

      NucDBMeasurement * measg1 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;

      plist = measg1->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg1->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      if(!strcmp(measg1->GetExperimentName(),"SANE") ){
         continue;
      }
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg1->BuildGraph("x");
      graph->Apply(x2f);
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      graph->SetMarkerSize(1.0);
   //   // Set legend title 
      leg->AddEntry(graph,Form("%s",measg1->GetExperimentName()),"p");
      leg0->AddEntry(graph,Form("%s",measg1->GetExperimentName()),"p");
      if(!strcmp(measg1->GetExperimentName(),"CLAS") ){
         mg_CLAS_g1->Add(graph,"p");
         continue;
      }

      // Add to TMultiGraph 
      MG_data1->Add(graph,"p"); 
      TGraphErrors * graph2 = new TGraphErrors(*graph);
      graph2->Apply(fTimes2);
      MG->Add(graph2,"p"); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }
   ////TGraphErrors *graph        = measg1_saneQ2->BuildGraph("x");
   ////graph->Apply(xf);
   ////graph->SetMarkerColor(1);
   ////graph->SetMarkerStyle(34);
   ////leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   ////MG->Add(graph,"p");
   ////leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   ////leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   ////leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   ////leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");
   //MG->Draw("A");
   //return 0;

   //// -------------------------------------------------------
   //// x2g1n 

   TLegend *leg2  = new TLegend(0.84, 0.15, 0.99, 0.85);
   leg2->SetFillColor(0);
   leg2->SetFillStyle(0);
   leg2->SetBorderSize(0);
   TLegend *leg02 = new TLegend(0.15, 0.65, 0.45, 0.88);
   leg02->SetFillColor(0);
   leg02->SetFillStyle(0);
   leg02->SetBorderSize(0);
   TMultiGraph *MG2 = new TMultiGraph(); 
   TMultiGraph *MG_data2 = new TMultiGraph(); 

   //NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));

   measurementsList->Clear();
   measurementsList =  manager->GetMeasurements("g2n");

   for (int i = 0; i < measurementsList->GetEntries(); i++) {

      NucDBMeasurement *measg2 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;

      if(!strcmp(measg2->GetExperimentName(),"SANE") ){
         continue;
      }

      plist = measg2->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg2->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      // Get TGraphErrors object 
      TGraphErrors *graph        = measg2->BuildGraph("x");
      graph->Apply(x2f);
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);

      leg2->AddEntry( graph,measg2->GetExperimentName(),"p");
      leg02->AddEntry(graph,measg2->GetExperimentName(),"p");

      MG_data2->Add(graph,"p"); 

      TGraphErrors * graph2 = new TGraphErrors(*graph);
      graph2->Apply(fTimes3);
      MG2->Add(graph2,"p"); 

   }

   // -------------------------------------------------------
   // Create graphs for ALL the models 
   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   //TMultiGraph * mg_models = new TMultiGraph();
   //std::vector<InSANEPolarizedStructureFunctions*> polSFlist;
   //InSANEPolarizedStructureFunctions * polSF = 0;
   //for(int i = 0; i< 8; i++ ){
   //   polSF = fman->CreatePolSFs(i);
   //   polSFlist.push_back(polSF);
   //}
   //polSF = fman->CreatePolSFs(12);
   //polSFlist.push_back(polSF);

   //Double_t model_Q2 = 2.0;
   //for(int i = 0; i<polSFlist.size(); i++) {
   //   InSANEPolarizedStructureFunctions * psf = polSFlist[i];
   //   TF1 * func2  = psf->GetFunction2();
   //   func2->SetLineColor(2);
   //   func2->SetLineWidth(1);
   //   func2->SetParameter(0,model_Q2);
   //   TGraph * grfun = new TGraph(func2->DrawCopy("goff")->GetHistogram());
   //   //grfun->Apply(f3);
   //   mg_models->Add(grfun,"l");
   //}
   //model_Q2 = 5.0;
   //for(int i = 0; i<polSFlist.size(); i++) {
   //   InSANEPolarizedStructureFunctions * psf = polSFlist[i];
   //   TF1 * func2  = psf->GetFunction2();
   //   func2->SetLineColor(4);
   //   func2->SetLineWidth(1);
   //   func2->SetParameter(0,model_Q2);
   //   TGraph * grfun = new TGraph(func2->DrawCopy("goff")->GetHistogram());
   //   //grfun->Apply(f3);
   //   mg_models->Add(grfun,"l");
   //}

   // ------------------------------------------------------------
   // g1 and g2 on same canvas 
   TCanvas     * c       = new TCanvas();
   TMultiGraph * mg_temp = new TMultiGraph();

   c->Divide(1,2);
   c->cd(1);
   ////gPad->SetGridy(true);

   mg_temp->Add(MG_data1);
   ////mg_temp->Add(mg);

   mg_temp->Draw("AP");
   mg_temp->SetTitle("");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->SetTitle("2x^{2}g_{1}^{p}");
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.03,0.1); 

   //TGraph * gr_x2g2n  = new TGraph( x2g2n->DrawCopy("goff")->GetHistogram());
   //gr_x2g2n->Apply(f2);
   ////gr_x2g2n->Draw("l");
   //mg_temp->Add(gr_x2g2n,"l");
   mg_temp->Draw("A");
   leg->Draw();

   // ---------------------------
   //
   c->cd(2);
   mg_temp = new TMultiGraph();
   ////gPad->SetGridy(true);

   ////mg_temp->Add(MG2);
   ////mg_temp->Add(mg2);
   mg_temp->Add(MG_data2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("");
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.2,0.15); 
   mg_temp->GetYaxis()->SetTitle("3x^{2}g_{2}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();

   //// Plot models
   ////TGraph * gr_x2g1n = new TGraph( xg1nWW->DrawCopy("goff")->GetHistogram());
   ////gr_x2g1n->Apply(f3);
   ////////gr_x2g1n->Draw("l");
   //////MG2->Add(gr_x2g1n,"l");
   //////MG2->Add(mg_models);

   ////mg_temp->Draw("a");
   leg2->Draw();

   mg_temp->Draw("a");
   c->Update();


   //c->SaveAs(Form("results/asymmetries/x2g1g2_combined2_%d.pdf",aNumber));
   //c->SaveAs(Form("results/asymmetries/x2g1g2_combined2_%d.png",aNumber));
   //c->SaveAs(Form("results/asymmetries/x2g1g2_combined2_%d.svg",aNumber));

   //// -----------------------------------------------------------------------
   //// d2p
   //TCanvas * cd2 = new TCanvas();

   //gPad->SetGridy(true);
   //mg3->Add(gr_x2g2n,"l");
   //mg3->Add(gr_x2g1n,"l");
   //mg3->Draw("a");

   //TH1 * h1_d2p = new TH1F( *((TH1F*)x2g2n->DrawCopy("goff")->GetHistogram()) ) ;
   //h1_d2p->Reset();
   //TH1 * h1_x2g2n = x2g2n->DrawCopy("goff")->GetHistogram();
   //h1_x2g2n->Scale(2.0);
   //TH1 * h1_x2g1n = xg1nWW->DrawCopy("goff")->GetHistogram();
   //h1_x2g1n->Scale(3.0);

   //h1_d2p->Add(h1_x2g2n);
   //h1_d2p->Add(h1_x2g1n);

   //TGraph * gr_d2p = new TGraph( h1_d2p );
   //gr_d2p->SetLineWidth(3);
   //mg3->Add(gr_d2p,"l");

   //mg3->GetXaxis()->SetLimits(0.0,1.0); 
   //mg3->GetYaxis()->SetRangeUser(-0.15,0.15); 
   //mg3->Draw("a");
   //cd2->Update();
   //cd2->SaveAs(Form("results/asymmetries/x2g1g2_combined2_d2_%d.png",aNumber));

   //// ----------------------------------------------------------------
   // Models
   c = new TCanvas();
   TLegend * legmod = new TLegend(0.40,0.7,0.60,0.88);
   legmod->SetFillColor(0);
   legmod->SetFillStyle(0);
   legmod->SetBorderSize(0);

   TList * list_of_models = new TList();
   TF1   * aModel         = 0;

   InSANEStructureFunctions          * sfs  = 0;
   InSANEPolarizedStructureFunctions * psfs = 0;

   // BBS
   sfs  = fman->CreateSFs(5);
   psfs = fman->CreatePolSFs(5);
   aModel = new TF1("xg1nbbs", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1n, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1n");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"BBS","l");

   // statistical
   sfs  = fman->CreateSFs(6);
   psfs = fman->CreatePolSFs(6);
   aModel = new TF1("xg1n", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1n, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1n");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"Stat 2015","l");

   // BB 
   psfs = fman->CreatePolSFs(3);
   aModel = new TF1("xg1nBB", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1n, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1n");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BB","l");


   // AAC
   psfs = fman->CreatePolSFs(2);
   aModel = new TF1("xg1naac", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1n, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1n");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"AAC","l");

   // LSS
   psfs = fman->CreatePolSFs(1);
   aModel = new TF1("xg1nlss", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1n, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1n");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"LSS2006","l");

   // DSSV
   psfs = fman->CreatePolSFs(0);
   aModel = new TF1("xg1nlssdssv", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1n, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1n");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"DSSV","l");

   TMultiGraph * mg_g1_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      TGraph * grmod = new TGraph( aModel->DrawCopy("goff")->GetHistogram());
      mg_g1_models->Add(grmod,"l");
   }

   //// --------------------------------------------------
   // g2 Models
   legmod2 = new TLegend(0.51,0.65,0.68,0.88);
   legmod2->SetFillColor(0);
   legmod2->SetFillStyle(0);
   legmod2->SetBorderSize(0);

   //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   list_of_models = new TList();
   //TF1 * aModel = 0;


   // BBS
   psfs = fman->CreatePolSFs(5);
   aModel = new TF1("xg2nbbs", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2nWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2nWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"BBS","l");

   // statistical
   psfs = fman->CreatePolSFs(13);
   aModel = new TF1("xg2n", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2nWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2nWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"Stat 2015","l");

   // BB 
   psfs = fman->CreatePolSFs(3);
   aModel = new TF1("xg2nBB", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2nWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2nWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"BB","l");


   // AAC
   psfs = fman->CreatePolSFs(2);
   aModel = new TF1("xg2naac", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2nWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2nWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"AAC","l");

   // LSS
   psfs = fman->CreatePolSFs(1);
   aModel = new TF1("xg2nlss", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2nWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2nWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"LSS2006","l");

   // DSSV
   psfs = fman->CreatePolSFs(0);
   aModel = new TF1("xg2nlssdssv", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2nWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2nWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"DSSV","l");

   TMultiGraph * mg_g2_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      TGraph * grmod = new TGraph( aModel->DrawCopy("goff")->GetHistogram());
      mg_g2_models->Add(grmod,"l");
   }


   ////---------------------------------------------------------------
   //// Nicer individual plots

   //// ---------------------------------
   // g1 with data
   TCanvas * c2 = new TCanvas();

   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data1);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{1}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.03,0.08); 

   leg0->Draw();

   //c2->SaveAs(Form("results/asymmetries/x2g1_existing_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g1_existing_%d.pdf",aNumber));

   // ---------------------------------
   // world data + clas
   mg_temp->Add(mg_CLAS_g1);

   c2->Update();
   //c2->SaveAs(Form("results/asymmetries/x2g1_existing_CLAS_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g1_existing_CLAS_%d.pdf",aNumber));

   // ---------------------------------
   // world dat + models
   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data1);
   mg_temp->Add(mg_g1_models);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{1}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.03,0.08); 

   legmod->Draw();
   leg0->Draw();

   c2->Update();
   //c2->SaveAs(Form("results/asymmetries/x2g1_existing_models_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g1_existing_models_%d.pdf",aNumber));


   //// ---------------------------
   //// world data + SANE data
   //mg_temp = new TMultiGraph();

   //mg_temp->Add(MG_data1);
   ////mg_temp->Add(mg_g1_models);
   //mg_temp->Add(mg_x2g1);
   //mg_temp->Add(mg_syst_err);

   //mg_temp->Draw("a");
   //mg_temp->SetTitle("x^{2}g_{1}^{p}");
   //mg_temp->GetXaxis()->SetTitle("x");
   //mg_temp->GetYaxis()->SetTitle("");
   //mg_temp->GetXaxis()->CenterTitle();
   //mg_temp->GetYaxis()->CenterTitle();
   //mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   //mg_temp->GetYaxis()->SetRangeUser(-0.03,0.08); 

   //leg0->Draw();
   //leg_sane->Draw();

   //c2->Update();
   //c2->SaveAs(Form("results/asymmetries/x2g1_combined2_sane_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g1_combined2_sane_%d.pdf",aNumber));

   //// ---------------------------
   //// world data + SANE data + models

   //mg_temp->Add(mg_g1_models);

   //legmod->Draw();

   //c2->Update();
   //c2->SaveAs(Form("results/asymmetries/x2g1_combined2_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g1_combined2_%d.pdf",aNumber));





   //// --------------------------------------------------------------
   //// g2   Nicer individual plots
   c2 = new TCanvas();

   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{2}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.1,0.15); 

   leg02->Draw();
   c2->Update();

   //c2->SaveAs(Form("results/asymmetries/x2g2_existing_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g2_existing_%d.pdf",aNumber));

   //// ------------------------
   ////
   mg_temp->Add(mg_g2_models);

   legmod->Draw();

   c2->Update();
   //c2->SaveAs(Form("results/asymmetries/x2g2_existing_models2_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g2_existing_models2_%d.pdf",aNumber));

   //// -----------------------------
   //// with SANE data
   //mg_temp = new TMultiGraph();

   //mg_temp->Add(MG_data2);
   //mg_temp->Add(mg_x2g2);
   //mg_temp->Add(mg_syst_err2);

   //mg_temp->Draw("a");
   //mg_temp->SetTitle("x^{2}g_{2}^{p}");
   //mg_temp->GetXaxis()->SetTitle("x");
   //mg_temp->GetYaxis()->SetTitle("");
   //mg_temp->GetXaxis()->CenterTitle();
   //mg_temp->GetYaxis()->CenterTitle();
   //mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   //mg_temp->GetYaxis()->SetRangeUser(-0.1,0.15); 

   //leg02->Draw();
   //leg_sane->Draw();

   //c2->Update();
   //c2->SaveAs(Form("results/asymmetries/x2g2_combined2_sane_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g2_combined2_sane_%d.pdf",aNumber));

   //// -----------------------------
   //// with SANE data

   //mg_temp->Add(mg_g2_models);

   //legmod->Draw();

   //c2->Update();
   //c2->SaveAs(Form("results/asymmetries/x2g2_combined2_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g2_combined2_%d.pdf",aNumber));

   return(0);
}
