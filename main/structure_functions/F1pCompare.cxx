/** First test of binned variable selection 
 */
Int_t F1pCompare() {

   //gSystem->AddIncludePath("-I/usr/local/include/LHAPDF");
   //gSystem->Load("libLHAPDF.so");

   TCanvas * c = new TCanvas("F1p","F1p");
   NucDBManager * manager = NucDBManager::GetManager();

   Double_t Q2 = 1.8;

   //NucDBBinnedVariable * Qsq = new NucDBBinnedVariable("Qsquared","Q^{2} 3.0<Q^{2}<4.0");
   //Qsq->SetBinMinimum(3.0);
   //Qsq->SetBinMaximum(4.0);
   //NucDBBinnedVariable * Qsq2 = new NucDBBinnedVariable("Qsquared","Q^{2} 4.0<Q^{2}<5.0");
   //Qsq2->SetBinMinimum(4.0);
   //Qsq2->SetBinMaximum(5.0);
   //NucDBMeasurement * F2p = manager->GetMeasurement("F1p");
   //NucDBMeasurement * F2pQsq1 = new NucDBMeasurement("F2pQsq1",Form("F_{2}^{p} %s",Qsq->GetTitle()));
   //NucDBMeasurement * F2pQsq2 = new NucDBMeasurement("F2pQsq2",Form("F_{2}^{p} %s",Qsq2->GetTitle()));


   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);
   TList * measurementsList =  manager->GetMeasurements("F1p");
   TMultiGraph * MG = new TMultiGraph();
   Int_t markers[] = {22,32,23,33,21,35,36,37};

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *aMeas = (NucDBMeasurement*)measurementsList->At(i);
      //TList * plist = aMeas->ApplyFilterWithBin(Qsqbin);
      //if(!plist) continue;
      //if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = aMeas->BuildGraph("x");
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerSize(1.3);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      // Set legend title 
      Title = Form("%s",aMeas->GetExperimentName()); 
      leg->AddEntry(graph,Title,"p");
      // Add to TMultiGraph 
      MG->Add(graph,"p"); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }

   leg->SetHeader("F1p");

   MG->Draw("a");
   MG->GetXaxis()->SetLimits(0.0,1.0);


   // --------------------------
   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   TF1 * xF2pCTEQ = new TF1("xF2pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF2pCTEQ,"F1p CTEQ","l");
   xF2pCTEQ->SetParameter(0,Q2);
   xF2pCTEQ->SetLineColor(6);

   // --------------------------
   InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();
   TF1 * xF2pA = new TF1("xF1pA", f1f2SFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF2pA,"F1p F1F209","l");
   xF2pA->SetParameter(0,Q2);
   xF2pA->SetLineColor(1);
   xF2pA->Draw("same");

   // --------------------------
   InSANEStructureFunctionsFromPDFs * myBBS = new InSANEStructureFunctionsFromPDFs();
   myBBS->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());
   TF1 * xF2pB = new TF1("xF1pB", myBBS, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   xF2pB->SetLineColor(kGreen);
   xF2pB->SetParameter(0,Q2);
   xF2pB->Draw("same");
   leg->AddEntry(xF2pB,"F1p BBS","l");

   // --------------------------
   NMC95StructureFunctions * NMCSFs = new NMC95StructureFunctions();
   TF1 * xF2pNMC = new TF1("xF1pNMC", NMCSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   leg->AddEntry(xF2pNMC,"F1p NMC95","l");
   xF2pNMC->SetLineColor(4);
   xF2pNMC->SetParameter(0,Q2);
   xF2pNMC->DrawCopy("same");

   // --------------------------
   LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   TF1 * xF2pC = new TF1("xF2pC", sf, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   //((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   xF2pC->SetParameter(0,Q2);
   xF2pC->SetLineColor(7);
   leg->AddEntry(xF2pC,Form("Composite SFs:%s",sf->GetLabel()),"l");
   xF2pC->DrawCopy("same");

   //LowQ2StructureFunctions * sfComp = new LowQ2StructureFunctions();
   //TF1 * xF1pC = new TF1("xF1pC", sfComp, &InSANEStructureFunctions::EvaluateF1p, 
   //             0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   //xF1pC->SetParameter(0,Q2);
   //xF1pC->SetLineColor(8);
   //leg->AddEntry(xF1pC,Form("Composite SFs:%s at Q^2=10.5",sfComp->GetLabel()),"l");
   //xF1pC->DrawClone("same");

   // --------------------------
   leg->Draw();

   c->SaveAs("results/structure_functions/F1pCompare.png");
   c->SaveAs("results/structure_functions/F1pCompare.pdf");

   return(0);
}
