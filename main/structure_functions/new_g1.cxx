#include <tuple>
//#include "SSFsFromVPACs.h"
//#include "SSFsFromPDFs.h"

void new_g1() {

  using namespace insane::physics;

  //insane::physics::SSFsFromPDFs<JAM_PPDFs> * sf = new insane::physics::SSFsFromPDFs<JAM_PPDFs>();
  //JAM_PPDFs ppdfs;
  //JAM_T3DFs t3dfs;
  //std::tuple<JAM_PPDFs, JAM_T3DFs, JAM_T4DFs> pdf_tuple;
  //std::tuple<JAM_PPDFs> pdf_tuple;
  //std::tuple<Stat2015_PPDFs> pdf_tuple2;

  //auto ssf = new SSFsFromVPACs<MAID_VPACs>();
  auto ssf = new SSFsFromPDFs<JAM_PPDFs, JAM_T3DFs>();
  //auto ssf = new CompositeSSFs();

  double Q2   = 1.0;
  auto   g1p  = std::function<double(double*,double*)>(
      [&](double *x, double*) {
      //return insane::physics::g1(pdf_tuple,Nuclei::p,x[0],Q2); 
      return x[0]*ssf->g1(x[0],Q2,Nuclei::p);
      }
      );

  auto   g1p2  = std::function<double(double*,double*)>(
      [&](double *x, double*) {
      //return insane::physics::g1(pdf_tuple2,Nuclei::p,x[0],Q2); 
      return x[0]*ssf->g1_TMC(x[0],Q2,Nuclei::p);
      //return x[0]*sf->g1(Nuclei::p,x[0],Q2); }
      }
      );
  TF1 *  f_g1p    = new TF1("g1p",g1p,0.0,1.0,0);
  TF1 *  f_g1p2   = new TF1("g1p2",g1p2,0.0,1.0,0);

  InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
  pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
  TF1 * xg1p = new TF1("g1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 0.02, 0.9, 1,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
  xg1p->SetParameter(0,Q2);

  TMultiGraph * mg = new TMultiGraph();
  TGraph * gr = nullptr;

  mg->Add( gr = new TGraph(f_g1p), "l");
  gr->SetLineColor(2);
  mg->Add( gr = new TGraph(f_g1p2), "l");
  gr->SetLineColor(4);
  //mg->Add( gr = new TGraph(xg1p), "l");
  //gr->SetLineColor(1);

  mg->Draw("a");
}
