Int_t xg1n_dssv(Double_t Qsq = 5.0,Double_t QsqBinWidth=1.0){
   TCanvas * c = new TCanvas("xg1n_dssv","xg1n_dssv");
   /// Create Legend

   /// First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsD = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsD->SetPolarizedPDFs( new DSSVPolarizedPDFs );

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;

   TF1 * xg1nD = new TF1("xg1nD", pSFsD, &InSANEPolarizedStructureFunctions::Evaluatexg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1n");

   xg1nD->SetParameter(0,Qsq);

   xg1nD->SetLineColor(kRed-2);

   Int_t width = 3;

   xg1nD->SetLineWidth(width);


   xg1nD->SetLineStyle(1);

   xg1nD->SetMinimum(-0.065);
   xg1nD->SetMaximum(0.05);

   xg1nD->Draw();

    std::cout << "ALL GOOD \n";
   return(0);
}
