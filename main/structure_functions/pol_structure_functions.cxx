/** Plots models and data for g1p at Qsq */
Int_t pol_structure_functions(Double_t Qsq = 5.0,Double_t QsqBinWidth=2.0){
   /// Create Legend
   TLegend * leg = new TLegend(0.6,0.5,0.875,0.875);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   /// First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg1p = new TF1("xg1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pA = new TF1("xg1pA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pB = new TF1("xg1pB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pC = new TF1("xg1pC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   xg1p->SetParameter(0,Qsq);
   xg1pA->SetParameter(0,Qsq);
   xg1pB->SetParameter(0,Qsq);
   xg1pC->SetParameter(0,Qsq);

   xg1p->SetLineColor(1);
   xg1pA->SetLineColor(3);
   xg1pB->SetLineColor(kOrange -1);
   xg1pC->SetLineColor(kViolet-2);

   Int_t width = 3;
   xg1p->SetLineWidth(width);
   xg1pA->SetLineWidth(width);
   xg1pB->SetLineWidth(width);
   xg1pC->SetLineWidth(width);

   xg1p->SetLineStyle(1);
   xg1pA->SetLineStyle(1);
   xg1pB->SetLineStyle(1);
   xg1pC->SetLineStyle(1);


   xg1p->Draw();
   xg1pA->Draw("same");
   xg1pB->Draw("same");
   xg1pC->Draw("same");


   /// Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();
   /// Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = new NucDBBinnedVariable(Form("Qsquared","Q^{2} %3f <Q^{2}<%3f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);
   /// Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g1pQsq1 = new NucDBMeasurement("g1pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   TList * listOfMeas = manager->GetMeasurements("g1p");
   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g1pMeas = (NucDBMeasurement*)listOfMeas->At(i);
       g1pMeas->BuildGraph();

       g1pMeas->fGraph->SetMarkerStyle(20+i);
       g1pMeas->fGraph->SetMarkerSize(1.6);
       g1pMeas->fGraph->SetMarkerColor(kBlue-2-i);
       g1pMeas->fGraph->SetLineColor(kBlue-2-i);
       g1pMeas->fGraph->SetLineWidth(1);

       if(i==0) {
          g1pMeas->fGraph->Draw("p");
          g1pQsq1->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin));
       }else{
          g1pMeas->fGraph->Draw("p");
          g1pQsq1->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin));
       }
       leg->AddEntry(g1pMeas->fGraph,Form("%s %s",g1pMeas->fExperiment.Data(),g1pMeas->GetTitle() ),"lp");

   }

   g1pQsq1->BuildGraph();
   g1pQsq1->fGraph->SetMarkerColor(2);
   g1pQsq1->fGraph->SetLineColor(2);
   g1pQsq1->fGraph->Draw("p");


   /// Complete the Legend 

   leg->AddEntry(g1pQsq1->fGraph,Form("%s %s","All g1p data",g1pQsq1->GetTitle() ),"ep");
   leg->AddEntry(xg1p,"g1p BB ","l");
   leg->AddEntry(xg1pA,"g1p DNS2005 ","l");
   leg->AddEntry(xg1pB,"g1p AAC ","l");
   leg->AddEntry(xg1pC,"g1p GS ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.25,0.85,Form("g_{1}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));


//    TCanvas * fCanvas = new TCanvas("polStructureFunc","Polarized Structure Functions",0,0,600,400);
//    xg1->Draw();
//    xg2->Draw("same");
//    xF1->Draw("same");
//    xF2->Draw("same");
//    xg1->SetMaximum(0.8);
//    xg1->SetMinimum(-0.1);
// 
//    TLegend * leg = new TLegend(0.7,0.7,0.95,0.95);
//    leg->SetHeader(Form("%s Polarized PDFs", " "));
//    leg->AddEntry(xg1,"xg_{1}^{p}(x)","l");
//    leg->AddEntry(xg2,"xg_{2}^{p}(x)","l");
//    leg->AddEntry(xF1,"xF_{1}^{p}(x)","l");
//    leg->AddEntry(xF2,"xF_{2}^{p}(x)","l");
// /*   leg->AddEntry(gStrangeVsX,gStrangeVsX->GetTitle(),"lp");*/
//    leg->Draw();

//    fCanvas->cd(0)->Update();
//    TLatex * t = new TLatex();
//    t->SetNDC();
//    t->SetTextFont(62);
//    t->SetTextColor(36);
//    t->SetTextSize(0.06);
//    t->SetTextAlign(12);
//    t->DrawLatex(0.05,0.95,Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));


   return(0);
}
