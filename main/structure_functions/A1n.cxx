// Use the implementation in the InSANE code base to build the A1 asymmetry 

#include <cstdlib> 
#include <iostream> 
#include <fstream> 
#include "TMultiGraph.h"

void A1n(){

	Double_t x,Q2; 

	cout << "Enter Q2 (GeV^2): ";
	cin  >> Q2; 

        // Use DSSV as the polarized PDF model 
        DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
        InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
        DSSVSF->SetPolarizedPDFs(DSSV);

        // Use CTEQ as the unpolarized PDF model 
        CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
        InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs();
        CTEQSF->SetUnpolarizedPDFs(CTEQ);

        // Asymmetry class: Use NMC
        InSANEAsymmetriesFromStructureFunctions *AsymSF = new InSANEAsymmetriesFromStructureFunctions(); 
	AsymSF->SetUnpolarizedSFs(CTEQSF);  
	AsymSF->SetPolarizedSFs(DSSVSF);

	Int_t npar = 1;
	Double_t xmin = 0.01;
	Double_t xmax = 1.00;
	Double_t ymin = -0.3;
	Double_t ymax =  0.3; 

        // Plot A1n(x,Q2=const) 
        // WARNING! Plot is for CONSTANT Q2! 
        // we set Q2 for the evaluation of the function: &InSANEPolarizedStructureFunctions::Evaluateg1n
	TF1 * Model = new TF1("Model",AsymSF,&InSANEAsymmetryBase::EvaluateA1n,
			xmin,xmax,npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1n");

	Model->SetParameter(0,Q2);
	Model->SetLineColor(kMagenta);

	Int_t width = 3;
	Model->SetLineWidth(width);
	Model->SetLineStyle(1);
	Model->GetYaxis()->SetRangeUser(ymin,ymax); 

        // Load in world data on A1n from NucDB  
	gSystem->Load("libNucDB");
	NucDBManager * manager = NucDBManager::GetManager();

	TLegend *leg = new TLegend(0.1, 0.7, 0.48, 0.9);
        leg->SetFillColor(kWhite); 

        TMultiGraph *MG = new TMultiGraph(); 

	TString Title;
	TList * measurementsList = manager->GetMeasurements("A1n");
	for (int i = 0; i < measurementsList->GetEntries(); i++) {
		NucDBMeasurement *WorldData = (NucDBMeasurement*)measurementsList->At(i);
                // Get TGraphErrors object 
		TGraphErrors *g        = WorldData->BuildGraph("x");
		g->SetMarkerStyle(20);
                // Set legend title 
		Title = Form("%s",WorldData->GetExperimentName()); 
		leg->AddEntry(g,Title,"p");
                // Add to TMultiGraph 
                MG->Add(g); 
	}

        leg->AddEntry(Model,"DSSV and CTEQ model","l");

	TCanvas * c = new TCanvas("c","A1n",1000,800);
	c->SetFillColor(kWhite); 
        c->cd(); 

        TString Measurement = Form("A_{1}^{n}");
	TString GTitle      = Form("%s (x,Q^{2} = %.2f GeV^{2})",Measurement.Data(),Q2);
        TString xAxisTitle  = Form("x");
        TString yAxisTitle  = Form("%s",Measurement.Data());

        // Draw everything 
        MG->Draw("AP");
	MG->SetTitle(GTitle);
	MG->GetXaxis()->SetTitle(xAxisTitle);
	MG->GetXaxis()->CenterTitle();
	MG->GetYaxis()->SetTitle(yAxisTitle);
	MG->GetYaxis()->CenterTitle();
        MG->GetXaxis()->SetLimits(xmin,xmax); 
        MG->Draw("AP");
	Model->Draw("same");
	leg->Draw();

}
