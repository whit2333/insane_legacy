void g1_g2_TMCs(){

  InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  auto   sf  = fman->CreatePolSFs(14);
  JAM15PolarizedPDFs  * pdfs = new JAM15PolarizedPDFs();

  double Q2    = 100.0;
  double x_min = 0.1;
  double x_max = 0.8;
  int    N     = 100;
  double dx    = (x_max-x_min)/double(N);

  TMultiGraph * mg            = new TMultiGraph();
  TGraph      * gr_g1         = new TGraph();
  TGraph      * gr_g1_noTMC   = new TGraph();
  TGraph      * gr_g1_t3_TMC  = new TGraph();
  TGraph      * gr_g1_t2_t3_TMC  = new TGraph();

  TGraph      * gr_Dp         = new TGraph();

  TGraph      * gr_d2_int_exp   = new TGraph();
  TGraph      * gr_d2_int_exp2   = new TGraph();
  TGraph      * gr_d2_int_ideal = new TGraph();
  TGraph      * gr_d2_nacht = new TGraph();
  TGraph      * gr_d2_int_WWsub = new TGraph();
  TGraph      * gr_d2_int_WWsub2 = new TGraph();

  TMultiGraph * mg2             = new TMultiGraph();
  TMultiGraph * mg3             = new TMultiGraph();
  TGraph      * gr_WW           = new TGraph();
  TGraph      * gr_WW_TMC       = new TGraph();
  TGraph      * gr_delta_WW     = new TGraph();
  TGraph      * gr_delta_WW_TMC = new TGraph();

  TGraph      * gr_g2_t3_noTMC  = new TGraph();
  TGraph      * gr_g2_t3_TMC    = new TGraph();
  TGraph      * gr_g2_t2_TMC    = new TGraph();
  TGraph      * gr_g2_t2_t3_TMC    = new TGraph();

  // -------------------------------------------------
  //
  gr_g1->SetLineWidth(2);
  gr_g1->SetLineColor(2);
  gr_g1_noTMC->SetLineWidth(2);
  gr_g1_noTMC->SetLineColor(1);
  gr_g1_t3_TMC->SetLineWidth(2);
  gr_g1_t3_TMC->SetLineColor(4);
  gr_g1_t2_t3_TMC->SetLineWidth(2);
  gr_g1_t2_t3_TMC->SetLineColor(8);

  gr_WW_TMC->SetLineWidth(2);
  gr_WW_TMC->SetLineColor(2);
  gr_WW->SetLineWidth(2);
  gr_WW->SetLineColor(1);
  gr_delta_WW->SetLineWidth(2);
  gr_delta_WW->SetLineColor(4);
  gr_g2_t3_noTMC->SetLineWidth(2);
  gr_g2_t3_noTMC->SetLineColor(3);
  gr_g2_t3_TMC->SetLineWidth(2);
  gr_g2_t3_TMC->SetLineColor(7);
  gr_g2_t2_TMC->SetLineWidth(2);
  gr_g2_t2_TMC->SetLineStyle(2);
  gr_g2_t2_TMC->SetLineColor(4);
  gr_g2_t2_t3_TMC->SetLineWidth(2);
  gr_g2_t2_t3_TMC->SetLineColor(8);
  gr_delta_WW_TMC->SetLineStyle(2);
  gr_delta_WW_TMC->SetLineWidth(2);
  gr_delta_WW_TMC->SetLineColor(2);
  
  gr_d2_int_exp->SetLineWidth(2);
  gr_d2_int_exp->SetLineColor(4);

  gr_d2_int_ideal->SetLineWidth(2);
  gr_d2_int_ideal->SetLineColor(8);

  gr_d2_int_exp2->SetLineWidth(2);
  gr_d2_int_exp2->SetLineColor(1);

  gr_d2_int_WWsub->SetLineWidth(2);
  gr_d2_int_WWsub->SetLineColor(3);
  gr_d2_int_WWsub->SetLineStyle(2);

  gr_d2_int_WWsub2->SetLineStyle(2);
  gr_d2_int_WWsub2->SetLineWidth(2);
  gr_d2_int_WWsub2->SetLineColor(7);

  // -------------------------------------------------
  //
  int ip = 0;
  for(unsigned int ix = 0; ix<N ; ix++) {

    double x = x_min + double(ix)*dx;

    double g1_t2     = sf->g1p_Twist2(x,Q2);
    double g1_t2_TMC = sf->g1p_Twist2_TMC(x,Q2);
    double g1_t3_TMC = sf->g1p_Twist3_TMC(x,Q2);
    double g1_t2_t3_TMC = sf->g1p_Twist3_TMC(x,Q2);

    double Dp        = pdfs->Dp_Twist3(x,Q2);
    double M2n_p_int = sf->M2nIntegrand_TMC_p(3,x,Q2);

    double g2_t2     = sf->g2p_Twist2(x,Q2);
    double g2_WW     = sf->g2pWW(x,Q2);
    double g2_WW_TMC = sf->g2pWW_TMC(x,Q2);
    double g2_WW_TMC_t3 = sf->g2pWW_TMC_t3(x,Q2);
    double g2_t3     = sf->g2p_Twist3(x,Q2);
    double g2_t3_TMC = sf->g2p_Twist3_TMC(x,Q2);
    double g2_t2_TMC = sf->g2p_Twist2_TMC(x,Q2);

    if( ip==0 ){
      gr_g1       ->SetPoint(    ip, x, 0.0);//x*x*g1_t2_TMC );
      gr_g1_noTMC ->SetPoint(    ip, x, 0.0);//x*x*g1_t2 );
      gr_g1_t3_TMC->SetPoint(    ip, x, 0.0);//x*x*(g1_t3_TMC));
      gr_g1_t2_t3_TMC->SetPoint( ip, x, 0.0);//x*x*(g1_t3_TMC + g1_t2_TMC));

      gr_WW      ->SetPoint(     ip, x, 0.0);//x*x*g2_WW );
      gr_WW_TMC  ->SetPoint(     ip, x, 0.0);//x*x*g2_WW_TMC );
      gr_delta_WW->SetPoint(     ip, x, 0.0);//x*x*(g2_WW-g2_WW_TMC));
      gr_delta_WW_TMC->SetPoint( ip, x, 0.0);//x*x*((g2_t2_TMC+g2_t3_TMC)-g2_WW_TMC));

      gr_g2_t3_noTMC ->SetPoint( ip, x, 0.0);//x*x*g2_t3 );
      gr_g2_t3_TMC   ->SetPoint( ip, x, 0.0);//x*x*g2_t3_TMC );
      gr_g2_t2_TMC   ->SetPoint( ip, x, 0.0);//x*x*g2_t2_TMC );
      gr_g2_t2_t3_TMC->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );

      gr_d2_int_exp->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_d2_int_exp2->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_d2_int_ideal->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_d2_nacht->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_d2_int_WWsub->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_d2_int_WWsub2->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );

      gr_Dp->SetPoint( ip, x, 0.0);

      ip++;
    }

    gr_g1       ->SetPoint(    ip, x, x*x*g1_t2_TMC );
    gr_g1_noTMC ->SetPoint(    ip, x, x*x*g1_t2 );
    gr_g1_t3_TMC->SetPoint(    ip, x, x*x*(g1_t3_TMC));
    gr_g1_t2_t3_TMC->SetPoint( ip, x, x*x*(g1_t3_TMC + g1_t2_TMC));

    gr_WW      ->SetPoint(     ip, x, x*x*g2_WW );
    gr_WW_TMC  ->SetPoint(     ip, x, x*x*g2_WW_TMC );
    gr_delta_WW->SetPoint(     ip, x, x*x*(g2_WW-g2_WW_TMC));
    gr_delta_WW_TMC->SetPoint( ip, x, 3.0*x*x*((g2_t2_TMC+g2_t3_TMC)-g2_WW_TMC));

    gr_g2_t3_noTMC ->SetPoint( ip, x, 3.0*x*x*g2_t3 );
    gr_g2_t3_TMC   ->SetPoint( ip, x, 3.0*x*x*g2_t3_TMC );
    gr_g2_t2_TMC   ->SetPoint( ip, x, x*x*g2_t2_TMC );
    gr_g2_t2_t3_TMC->SetPoint( ip, x, x*x*(g2_t2_TMC+g2_t3_TMC) );

    gr_d2_int_exp->SetPoint( ip, x,x*x*(2.0*(g1_t2_TMC+g1_t3_TMC) + 3.0*(g2_t2_TMC+g2_t3_TMC)) );
    gr_d2_int_exp2->SetPoint( ip, x,x*x*(2.0*(g1_t2_TMC) + 3.0*(g2_t2_TMC)) );
    gr_d2_int_ideal->SetPoint( ip, x, x*x*(2.0*(g1_t3_TMC) + 3.0*(g2_t3_TMC)));
    gr_d2_int_WWsub->SetPoint( ip, x, 3.0*x*x*((g2_t2_TMC+g2_t3_TMC)- g2_WW ));

    double  sub1 = (g2_t2_TMC+g2_t3_TMC) - g2_WW + g2_WW_TMC_t3;
    double  sub2 = sub1 - (g2_t2_TMC-g2_t2);
    gr_d2_int_WWsub2->SetPoint( ip, x, 3.0*x*x*( sub2 - (g2_t3_TMC-g2_t3)));

    gr_d2_nacht->SetPoint( ip, x, 2.0*M2n_p_int );

    gr_Dp->SetPoint( ip, x, x*x*Dp);

    ip++;

    if( ip==N+1 ){
      gr_g1       ->SetPoint(    ip, x, 0.0);//x*x*g1_t2_TMC );
      gr_g1_noTMC ->SetPoint(    ip, x, 0.0);//x*x*g1_t2 );
      gr_g1_t3_TMC->SetPoint(    ip, x, 0.0);//x*x*(g1_t3_TMC));
      gr_g1_t2_t3_TMC->SetPoint( ip, x, 0.0);//x*x*(g1_t3_TMC + g1_t2_TMC));

      gr_WW      ->SetPoint(     ip, x, 0.0);//x*x*g2_WW );
      gr_WW_TMC  ->SetPoint(     ip, x, 0.0);//x*x*g2_WW_TMC );
      gr_delta_WW->SetPoint(     ip, x, 0.0);//x*x*(g2_WW-g2_WW_TMC));
      gr_delta_WW_TMC->SetPoint( ip, x, 0.0);//x*x*((g2_t2_TMC+g2_t3_TMC)-g2_WW_TMC));

      gr_g2_t3_noTMC ->SetPoint( ip, x, 0.0);//x*x*g2_t3 );
      gr_g2_t3_TMC   ->SetPoint( ip, x, 0.0);//x*x*g2_t3_TMC );
      gr_g2_t2_TMC   ->SetPoint( ip, x, 0.0);//x*x*g2_t2_TMC );
      gr_g2_t2_t3_TMC->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );

      gr_d2_int_exp->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_Dp->SetPoint( ip, x, 0.0);
      gr_d2_nacht->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_d2_int_ideal->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_d2_int_WWsub->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      gr_d2_int_WWsub2->SetPoint( ip, x, 0.0);//x*x*(g2_t2_TMC+g2_t3_TMC) );
      ip++;
    }

  }

  double d2_exp       = gr_d2_int_exp->Integral()  ;
  double d2_exp2      = gr_d2_int_exp2->Integral()  ;
  double d2_exp_ideal = gr_d2_int_ideal->Integral()  ;
  double d2_ideal     = gr_g2_t3_noTMC->Integral()  ;
  double d2_TMC       = gr_g2_t3_TMC->Integral()    ;
  double d2_bad_TMC   = gr_delta_WW_TMC->Integral() ;
  double d2_nacht_TMC = gr_d2_nacht->Integral() ;

  double d2_WWsub     = gr_d2_int_WWsub->Integral()  ;
  double d2_WWsub2    = gr_d2_int_WWsub2->Integral()  ;

  std::cout << "2.0*d2_nacht_TMC  = " << d2_nacht_TMC   << std::endl;
  std::cout << " d2_int_WWsub     = " << d2_WWsub   << std::endl;
  std::cout << " d2_int_WWsub2    = " << d2_WWsub2   << std::endl;
  std::cout << " d2_exp     = " << d2_exp   << std::endl;
  std::cout << " d2_exp2    = " << d2_exp2   << std::endl;
  std::cout << " d2_expideal= " << d2_exp_ideal<< std::endl;
  std::cout << " d2_ideal   = " << d2_ideal   << std::endl;
  std::cout << " d2_TMC     = " << d2_TMC     << std::endl;
  std::cout << " d2_bad_TMC = " << d2_bad_TMC << std::endl;
  std::cout << " diff       :" << (d2_bad_TMC)/d2_ideal << std::endl;

  double M23_p     = sf->M2n_p(3,Q2,x_min,x_max)    ;
  double M23_TMC_p = sf->M2n_TMC_p(3,Q2,x_min,x_max) ;
  double d2p_tilde = sf->d2p_tilde(Q2,x_min,x_max)    ;

  std::cout << "2*M23_p      = " << 2.0*M23_p     << std::endl;
  std::cout << "2*M23_TMC_p  = " << 2.0*M23_TMC_p << std::endl;
  std::cout << "  d2p_tilde  = " << d2p_tilde << std::endl;

  double Dp_int = gr_Dp->Integral();
  std::cout << "   2*Dp_int  = " << 2.0*Dp_int << std::endl;

  // -------------------------------------------------
  //
  mg->Add(gr_g1,      "l");
  mg->Add(gr_g1_noTMC,"l");
  mg->Add(gr_g1_t3_TMC,"l");
  mg->Add(gr_g1_t2_t3_TMC,"l");

  mg2->Add(gr_WW,    "l");
  mg2->Add(gr_WW_TMC,"l");
  mg2->Add(gr_delta_WW,"l");
  mg2->Add(gr_delta_WW_TMC,"l");

  mg2->Add(gr_g2_t3_noTMC,"l");
  mg2->Add(gr_g2_t3_TMC,"l");
  mg2->Add(gr_g2_t2_TMC,"l");
  mg2->Add(gr_g2_t2_t3_TMC,"l");

  //mg3->Add(gr_delta_WW,"l");
  mg3->Add(gr_delta_WW_TMC,"l");
  mg3->Add(gr_g2_t3_noTMC,"l");
  mg3->Add(gr_g2_t3_TMC,"l");
  mg3->Add(gr_d2_nacht ,"l");
  mg3->Add(gr_d2_int_exp ,"l");
  mg3->Add(gr_d2_int_exp2 ,"l");
  mg3->Add(gr_d2_int_ideal ,"l");
  mg3->Add(gr_d2_int_WWsub ,"l");
  mg3->Add(gr_d2_int_WWsub2 ,"l");


  // -------------------------------------------------
  //
  TCanvas * c   = nullptr;
  TLegend * leg = nullptr;
  
  // ----------------------------------
  c   = new TCanvas();
  leg = new TLegend(0.7,0.8,0.99,0.99);
  leg->AddEntry( gr_g1_noTMC,     "xg_{1}^{no TMC}", "l");
  leg->AddEntry( gr_g1      ,     "xg_{1}^{TMC}"   , "l");
  leg->AddEntry( gr_g1_t3_TMC,    "xg_{1}^{TMC+#tau3}"   , "l");
  leg->AddEntry( gr_g1_t2_t3_TMC, "xg_{1}^{TMC+#tau2+#tau3}"   , "l");
  leg->SetHeader(sf->GetLabel());
  mg->Draw("a");
  mg->GetXaxis()->SetTitle("x");
  mg->Draw("a");
  leg->Draw();
  c->SaveAs("results/structure_functions/g1_g2_TMCs_0.png");
  c->SaveAs("results/structure_functions/g1_g2_TMCs_0.pdf");

  // ----------------------------------
  c   = new TCanvas();
  leg = new TLegend(0.01,0.2,0.3,0.5);
  leg->AddEntry( gr_WW         , "x^{2}g_{2}^{WW}[g_{1}^{no TMC}]", "l");
  leg->AddEntry( gr_WW_TMC     , "x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  leg->AddEntry( gr_delta_WW   , "x^{2}g_{2}^{WW}[g_{1}^{no TMC}] - x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  leg->AddEntry( gr_delta_WW_TMC, "x^{2}g_{2}^{TMC+#tau2+#tau3} - x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  leg->AddEntry( gr_g2_t3_noTMC, "x^{2}g_{2}^{#tau3}","l");
  leg->AddEntry( gr_g2_t3_TMC  , "x^{2}g_{2}^{TMC+#tau3}"   , "l");
  leg->AddEntry( gr_g2_t2_TMC  , "x^{2}g_{2}^{TMC+#tau2}"   , "l");
  leg->AddEntry( gr_g2_t2_t3_TMC  , "x^{2}g_{2}^{TMC+#tau2+#tau3}"   , "l");
  mg2->Draw("a");
  mg2->GetXaxis()->SetTitle("x");
  mg2->Draw("a");
  leg->Draw();
  c->SaveAs("results/structure_functions/g1_g2_TMCs_1.png");
  c->SaveAs("results/structure_functions/g1_g2_TMCs_1.pdf");


  // ----------------------------------
  c   = new TCanvas();
  leg = new TLegend(0.7,0.8,0.99,0.99);
  //leg->AddEntry( gr_delta_WW    , "x^{2}g_{2}^{WW}[g_{1}^{no TMC}] - x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  leg->AddEntry( gr_delta_WW_TMC, "x^{2}g_{2}^{TMC+#tau2+#tau3} - x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  leg->AddEntry( gr_g2_t3_noTMC , "x^{2}g_{2}^{#tau3}","l");
  leg->AddEntry( gr_g2_t3_TMC   , "x^{2}g_{2}^{TMC+#tau3}"   , "l");
  leg->AddEntry( gr_d2_nacht    , "M_2^{3} integrand"   , "l");
  leg->AddEntry( gr_d2_int_exp    , "d2 int exp"   , "l");
  leg->AddEntry( gr_d2_int_exp2    , "d2 int exp2"   , "l");
  leg->AddEntry( gr_d2_int_ideal    , "d2 int ideal" , "l");
  leg->AddEntry( gr_d2_int_WWsub    , "d2 int WWsub", "l");
  leg->AddEntry( gr_d2_int_WWsub2   , "d2 int WWsub2", "l");
  mg3->Draw("a");
  mg3->GetXaxis()->SetTitle("x");
  mg3->Draw("a");
  leg->Draw();
  c->SaveAs("results/structure_functions/g1_g2_TMCs_3.png");
  c->SaveAs("results/structure_functions/g1_g2_TMCs_3.pdf");
  // ----------------------------------


}

