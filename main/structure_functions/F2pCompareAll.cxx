// #include "TSystem.h"
// #include "TROOT.h"
// #include "NucDBManager.h"
// #include "NucDBMeasurement.h"
// #include "TLegend.h"
// #include "TF1.h"
//#include "InSANEStructureFunctions.h"
// #include "InSANEPDFs.h"
// #include "LHAPDF/LHAPDF.h"

/** First test of binned variable selection 
 *  
 *  Must be compiled by ACLIC
 */
Int_t F2pCompareAll() {

    gSystem->AddIncludePath("-I/usr/local/include/LHAPDF");
    gSystem->Load("libLHAPDF.so");
// 

   TCanvas * c = new TCanvas("F2p","F2p");

   NucDBManager * manager = NucDBManager::GetManager();

   // Qsq bin on slac data.
   NucDBBinnedVariable * Qsq = new NucDBBinnedVariable("Qsquared","Q^{2} 3.0<Q^{2}<4.0");
   Qsq->SetBinMinimum(3.0);
   Qsq->SetBinMaximum(4.0);
   /// OLDSLAC
   NucDBMeasurement * F2p = manager->GetExperiment("OLDSLAC")->GetMeasurement("F2p");

   // Qsq bin on BCDMS data
   NucDBBinnedVariable * Qsq2 = new NucDBBinnedVariable("Qsquared","Q^{2} 2.0<Q^{2}<10.0");
   Qsq2->SetBinMinimum(3.0);
   Qsq2->SetBinMaximum(7.0);
   /// BCDMS 
   NucDBMeasurement * F2pBCDMS = manager->GetExperiment("BCDMS")->GetMeasurement("F2p");

   /// EMC 
   NucDBMeasurement * F2pEMC = manager->GetExperiment("NMC")->GetMeasurement("F2p");

   /// ZEUS 
   NucDBMeasurement * F2pZEUS = manager->GetExperiment("ZEUS")->GetMeasurement("F2p");

   NucDBMeasurement * F2pQsq1 = new NucDBMeasurement("F2pQsq1",Form("F_{2}^{p} %s",Qsq->GetTitle()));
   NucDBMeasurement * F2pQsq2 = new NucDBMeasurement("F2pQsq2",Form("F_{2}^{p} %s",Qsq2->GetTitle()));
   NucDBMeasurement * F2pQsq3 = new NucDBMeasurement("F2pQsq2",Form("F_{2}^{p} %s",Qsq2->GetTitle()));
   NucDBMeasurement * F2pQsq4 = new NucDBMeasurement("F2pQsq2",Form("F_{2}^{p} %s",Qsq2->GetTitle()));

   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("F2p");

   TMultiGraph * mg = new TMultiGraph();  

   TGraphErrors * g1 = F2p->BuildGraph("x");
   mg->Add(g1,"p");
   TGraphErrors * g2 = F2pBCDMS->BuildGraph("x");
   mg->Add(g2,"p");
   TGraphErrors * g3 = F2pEMC->BuildGraph("x");
   mg->Add(g3,"p");
   TGraphErrors * g4 = F2pZEUS->BuildGraph("x");
   mg->Add(g4,"p");
   
   leg->AddEntry(g1,Form("%s %s",F2p->GetExperimentName(),F2p->GetTitle() ),"ep");
   leg->AddEntry(g2,Form("%s %s",F2pBCDMS->GetExperimentName(),F2pBCDMS->GetTitle() ),"ep");
   leg->AddEntry(g3,Form("%s %s",F2pEMC->GetExperimentName(),F2pEMC->GetTitle() ),"ep");
   leg->AddEntry(g4,Form("%s %s",F2pZEUS->GetExperimentName(),F2pZEUS->GetTitle() ),"ep");

   /// OLD SLAC with 4 < Q2 < 5 GeV^2
   F2pQsq1->AddDataPoints(F2p->FilterWithBin(Qsq));
   TGraphErrors * graph1 = F2pQsq1->BuildGraph("x");
   graph1->SetMarkerColor(4);
   graph1->SetLineColor(4);
   mg->Add(graph1,"p");//->Draw("p");
   leg->AddEntry(graph1,Form("%s %s",F2p->GetExperimentName(),F2pQsq1->GetTitle() ),"ep");

   /// BCDMS with 2 < Q2 < 10
   F2pQsq2->AddDataPoints(F2pBCDMS->FilterWithBin(Qsq2));
   TGraphErrors * graph2 = F2pQsq2->BuildGraph("x");
   graph2->SetMarkerColor(5);
   graph2->SetLineColor(5);
   mg->Add(graph2,"p");//->Draw("p");
   leg->AddEntry(graph2,Form("%s %s",F2pBCDMS->GetExperimentName(),F2pQsq2->GetTitle() ),"ep");

   /// EMC with 2 < Q2 < 10
   F2pQsq3->AddDataPoints(F2pEMC->FilterWithBin(Qsq2));
   TGraphErrors * graph3 = F2pQsq3->BuildGraph("x");
   graph3->SetMarkerColor(6);
   graph3->SetLineColor(6);
   mg->Add(graph3,"p");//->Draw("p");
   leg->AddEntry(graph3,Form("%s %s",F2pEMC->GetExperimentName(),F2pQsq2->GetTitle() ),"ep");

   /// EMC with 2 < Q2 < 10
   F2pQsq4->AddDataPoints(F2pZEUS->FilterWithBin(Qsq2));
   TGraphErrors * graph4 = F2pQsq4->BuildGraph("x");
   graph4->SetMarkerColor(7);
   graph4->SetLineColor(7);
   mg->Add(graph4,"p");//->Draw("p");
   leg->AddEntry(graph4,Form("%s %s",F2pZEUS->GetExperimentName(),F2pQsq2->GetTitle() ),"ep");

   mg->Draw("a"); 

//____________________________________________________________________

   /// Now plot the models functions and fits

   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   TF1 * xF2pCTEQ = new TF1("xF2pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(xF2pCTEQ,"F2p CTEQ","l");
   xF2pCTEQ->SetParameter(0,4.5);
   xF2pCTEQ->SetLineColor(6);
   xF2pCTEQ->Draw("same");


//    InSANEStructureFunctionsFromPDFs * lhaSFs = new InSANEStructureFunctionsFromPDFs();
//    lhaSFs->SetUnpolarizedPDFs(new LHAPDFUnpolarizedPDFs());

   InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();
   TF1 * xF2pA = new TF1("xF2pA", f1f2SFs, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(xF2pA,"F2p F1F209","l");
   xF2pA->SetParameter(0,4.5);
   xF2pA->SetLineColor(4);
   xF2pA->Draw("same");

/*   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("MSTW2008nlo68cl");*/
/*   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("cteq4m");*/
   InSANEStructureFunctions * lhaSFs = new LHAPDFStructureFunctions();

   TF1 * xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFType("cteq6m",1);
   xF2p->SetParameter(0,10.5);
   xF2p->SetLineColor(2);
   leg->AddEntry(xF2p,Form("F2p LHAPDF:%s at Q^2=10.5",lhaSFs->GetLabel()),"l");
   xF2p->DrawClone("same");

   xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");

   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFType("cteq6m",1);
   xF2p->SetParameter(0,4.5);
   xF2p->SetLineColor(3);
   leg->AddEntry(xF2p,Form("F2p LHAPDF:%s at Q^2=4.5",lhaSFs->GetLabel()),"l");
   xF2p->DrawClone("same");


   /// Composite Structure FUnctions
   LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   TF1 * xF2pC = new TF1("xF2pC", sf, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   //((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   xF2pC->SetParameter(0,4.5);
   xF2pC->SetLineColor(6);
   //leg->AddEntry(xF2p,Form("Composite SFs:%s at Q^2=10.5",sf->GetLabel()),"l");
   xF2pC->DrawClone("same");


//    xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF2p, 
//                0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
// 
//     ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("cteq6mE", LHAPDF::LHGRID );
//    xF2p->SetParameter(0,100.5);
//    xF2p->SetLineColor(7);
//    leg->AddEntry(xF2p,Form("F2p LHAPDF:%s",lhaSFs->GetLabel()),"l");
//    xF2p->DrawClone("same");



   leg->Draw();
//    lhaSFs->PlotSFs(4.5);


   TString prefix = Form("results/structure_functions/");
   TString name   = Form("F2p_compare"); 
   TString fn1    = prefix + name + Form(".png"); 
   TString fn2    = prefix + name + Form(".pdf"); 

   c->SaveAs(fn1);
   c->SaveAs(fn2);

return(0);
}
