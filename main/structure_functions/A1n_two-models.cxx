// Use the implementation in the InSANE code base to build the A1 asymmetry 

#include <cstdlib> 
#include <iostream> 
#include <fstream> 
#include "TMultiGraph.h"

void A1n_two_models(){

	Double_t x,Q2; 

	// cout << "Enter x: ";
	// cin  >> x; 
	cout << "Enter Q2 (GeV): ";
	cin  >> Q2; 

        // Use DSSV as the polarized PDF model 
	DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs(); 

        // Use NMC95 as the unpolarized SF model 
        NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
        // Use F1F209 as the unpolarized SF model 
        F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 
        // Use the modified version of F1F209 
	F1F209->UseModifiedModel('y'); 
	F1F209->SetBeamEnergy(5.89); 

        // Asymmetry class: Use NMC
        InSANEAsymmetriesFromStructureFunctions *AsymSF1 = new InSANEAsymmetriesFromStructureFunctions(); 
	AsymSF1->SetPolarizedPDFs(DSSV);
	AsymSF1->SetUnpolarizedSFs(NMC95);  
        // Asymmetry class: Use F1F209 
        InSANEAsymmetriesFromStructureFunctions *AsymSF2 = new InSANEAsymmetriesFromStructureFunctions(); 
	AsymSF2->SetPolarizedPDFs(DSSV);
	AsymSF2->SetUnpolarizedSFs(F1F209);  

	Int_t npar = 1;
	Double_t xmin = 0.01;
	Double_t xmax = 1.00;
	Double_t xmin1 = 0.01;
	Double_t xmax1 = 1.00;
	Double_t xmin2 = 0.2;
	Double_t xmax2 = 1.00;
	Double_t ymin = -0.3;
	Double_t ymax =  0.3; 

        // Plot A1n(x,Q2=const) 
        // WARNING! Plot is for CONSTANT Q2! 
        // we set Q2 for the evaluation of the function: &InSANEPolarizedStructureFunctions::Evaluateg1n
	TF1 * A1nModel1 = new TF1("A1nModel1",AsymSF1,&InSANEAsymmetryBase::EvaluateA1n,
			xmin1, xmax1, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1n");

	TF1 * A1nModel2 = new TF1("A1nModel2",AsymSF2,&InSANEAsymmetryBase::EvaluateA1n,
			xmin2, xmax2, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1n");


	A1nModel1->SetParameter(0,Q2);
	A1nModel1->SetLineColor(kMagenta);

	A1nModel2->SetParameter(0,Q2);
	A1nModel2->SetLineColor(kCyan+2);

	TString Title = Form("A_{1}^{^{3}He}(x, Q^{2} = %.2f GeV^{2})",Q2);

	Int_t width = 3;
	A1nModel1->SetLineWidth(width);
	A1nModel1->SetLineStyle(1);

	A1nModel2->SetLineWidth(width);
	A1nModel2->SetLineStyle(1);

	A1nModel1->SetTitle(Title);
	A1nModel1->GetYaxis()->SetRangeUser(ymin,ymax); 

	A1nModel2->SetTitle(Title);
	A1nModel2->GetYaxis()->SetRangeUser(ymin,ymax); 

        // Load in world data on A1n from NucDB  
	gSystem->Load("libNucDB");
	NucDBManager * manager = NucDBManager::GetManager();

	TLegend *leg = new TLegend(0.1, 0.7, 0.48, 0.9);
        leg->SetFillColor(kWhite); 

        TMultiGraph *MG = new TMultiGraph(); 

	TList * measurementsList = manager->GetMeasurements("A1n");
	for (int i = 0; i < measurementsList->GetEntries(); i++) {
		NucDBMeasurement *A1nWorld = (NucDBMeasurement*)measurementsList->At(i);
                // Get TGraphErrors object 
		TGraphErrors *graph        = A1nWorld->BuildGraph("x");
		graph->SetMarkerStyle(20);
                // Set legend title 
		Title = Form("%s",A1nWorld->GetExperimentName()); 
		leg->AddEntry(graph,Title,"p");
                // Add to TMultiGraph 
                MG->Add(graph); 
	}

        leg->AddEntry(A1nModel1,"DSSV and NMC95 model","l");
        leg->AddEntry(A1nModel2,"DSSV and F1F209 model","l");

	TCanvas * c = new TCanvas("c","A1n",1000,800);
	c->SetFillColor(kWhite); 
        c->cd(); 

        TString Measurement = Form("A_{1}^{n}");
	TString GTitle      = Form("%s (x,Q^{2} = %.2f GeV^{2})",Measurement.Data(),Q2);
        TString xAxisTitle  = Form("x");
        TString yAxisTitle  = Form("%s",Measurement.Data());

        // Draw everything 
        MG->Draw("AP");
	MG->SetTitle(GTitle);
	MG->GetXaxis()->SetTitle(xAxisTitle);
	MG->GetXaxis()->CenterTitle();
	MG->GetYaxis()->SetTitle(yAxisTitle);
	MG->GetYaxis()->CenterTitle();
        // MG->GetXaxis()->SetLimits(xmin,xmax); 
        MG->Draw("AP");
	A1nModel1->Draw("same");
	A1nModel2->Draw("same");
	leg->Draw();

}
