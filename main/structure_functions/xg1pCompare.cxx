/** Plots models and data for g1p at Qsq */
Int_t xg1pCompare(Double_t Qsq = 2.0,Double_t QsqBinWidth=1.0){
   TCanvas * c = new TCanvas("xg1pCompare","xg1pCompare");

   /// Create Legend
   TLegend * leg = new TLegend(0.6,0.5,0.875,0.875);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   /// First Plot all the Polarized Structure Function Models! 
   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;

   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   TF1 * xg1p = new TF1("xg1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");

   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   TF1 * xg1pA = new TF1("xg1pA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");

   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   TF1 * xg1pB = new TF1("xg1pB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");

   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);
   TF1 * xg1pC = new TF1("xg1pC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");

   xg1p->SetParameter(0,Qsq);
   xg1pA->SetParameter(0,Qsq);
   xg1pB->SetParameter(0,Qsq);
   xg1pC->SetParameter(0,Qsq);

   xg1p->SetLineColor(1);
   xg1pA->SetLineColor(3);
   xg1pB->SetLineColor(kOrange -1);
   xg1pC->SetLineColor(kViolet-2);

   Int_t width = 3;
   xg1p->SetLineWidth(width);
   xg1pA->SetLineWidth(width);
   xg1pB->SetLineWidth(width);
   xg1pC->SetLineWidth(width);

   xg1p->SetLineStyle(1);
   xg1pA->SetLineStyle(1);
   xg1pB->SetLineStyle(1);
   xg1pC->SetLineStyle(1);

   xg1pA->SetMaximum(0.1);
   xg1pA->SetMinimum(-0.03);

   xg1pA->Draw();
   xg1pB->Draw("same");
   xg1pC->Draw("same");
   xg1p->Draw("same");

   //__________________________________________________________________________
   /// Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   /// Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   Double_t Q2Min2 = 5.0;
   Double_t Q2Max2 = 15.0;
   NucDBBinnedVariable * Qsqbin2 = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Q2Min2,Q2Max2));
   Qsqbin2->SetBinMinimum(Q2Min2);
   Qsqbin2->SetBinMaximum(Q2Max2);

   /// Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g1pQsq1 = new NucDBMeasurement("g1pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   NucDBMeasurement * g1pQsq2 = new NucDBMeasurement("g1pQsq2",Form("g_{1}^{p} %s",Qsqbin2->GetTitle()));

   TF2 * xf = new TF2("xf","x*y",0,1,-1,1);

   TList * listOfMeas = manager->GetMeasurements("g1p");
   TMultiGraph * mg = new TMultiGraph();
   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g1pMeas = (NucDBMeasurement*)listOfMeas->At(i);
       TGraphErrors * graph = g1pMeas->BuildGraph("x");
       graph->Apply(xf);
       graph->SetMarkerStyle(20+i);
       graph->SetMarkerSize(1.6);
       graph->SetMarkerColor(kBlue-2-i);
       graph->SetLineColor(kBlue-2-i);
       graph->SetLineWidth(1);
       mg->Add(graph,"p");
       leg->AddEntry(graph,Form("%s %s",g1pMeas->GetExperimentName(),g1pMeas->GetTitle() ),"lp");

       g1pQsq1->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin));
       g1pQsq2->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin2));

   }

   mg->Draw("");
   TGraphErrors * graph1 = g1pQsq1->BuildGraph();
   graph1->Apply(xf);
   graph1->SetMarkerColor(2);
   graph1->SetLineColor(2);
   graph1->Draw("p");

   TGraphErrors * graph2 = g1pQsq2->BuildGraph();
   graph2->Apply(xf);
   graph2->SetMarkerColor(3);
   graph2->SetLineColor(3);
   graph2->Draw("p");


   /// Complete the Legend 

   leg->AddEntry(graph1,Form("%s %s","All g1p data",g1pQsq1->GetTitle() ),"ep");
   leg->AddEntry(graph2,Form("%s %s","All g1p data",g1pQsq2->GetTitle() ),"ep");
   leg->AddEntry(xg1p,"xg1p BB ","l");
   leg->AddEntry(xg1pA,"xg1p DNS2005 ","l");
   leg->AddEntry(xg1pB,"xg1p AAC ","l");
   leg->AddEntry(xg1pC,"xg1p GS ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.15,0.85,Form("xg_{1}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));


   c->SaveAs("results/structure_functions/xg1p.png");
   c->SaveAs("results/structure_functions/xg1p.ps");

   return(0);
}
