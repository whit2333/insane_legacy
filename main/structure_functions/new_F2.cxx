//#include "SFsFromPDFs.h"
#include "SFsFromVPACs.h"
#include <tuple>

void new_F2() {

  using namespace insane::physics;

  //CTEQ10_PDFs ppdfs;

  std::tuple<Stat2015_UPDFs> pdf_tuple;
  F1F209_SFs *  f1f209 = new F1F209_SFs();
  //CompositeSFs *  compsfs = new CompositeSFs();

  //auto vca = new MAID_VPACs();//VCSAsFromSFs<F1F209_SFs, SSFsFromPDFs<Stat2015_PPDFs>>();
  auto compsfs = new SFsFromVPACs<MAID_VPACs>();


  double Q2   = 1.0;
  auto   F2p  = std::function<double(double*,double*)>(
      [&](double *x, double*) {
      return insane::physics::F2(pdf_tuple,Nuclei::p,x[0],Q2); 
      //return x[0]*sf->g1(Nuclei::p,x[0],Q2); }
      }
      );
  TF1 *  f_F2p    = new TF1("g1p",F2p,0.2,1.0,0);
  auto   F2p_TMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) {
      //return insane::physics::F2_TMC(pdf_tuple,Nuclei::p,x[0],Q2); 
      return compsfs->F2(x[0],Q2,Nuclei::p);
      //return x[0]*sf->g1(Nuclei::p,x[0],Q2); }
      }
      );
  TF1 *  f_F2p_TMC    = new TF1("F1p_TMC",F2p_TMC,0.2,1.0,0);

  ////auto   g1p2  = std::function<double(double*,double*)>(
  ////    [&](double *x, double*) {
  ////    return x[0]*insane::physics::F2_TMC(pdf_tuple,Nuclei::p,x[0],Q2); 
  ////    //return x[0]*sf->g1(Nuclei::p,x[0],Q2); }
  ////    }
  ////    );
  ////TF1 *  f_g1p2   = new TF1("g1p2",g1p2,0.0,1.0,0);

  InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();
  TF1 * xF2pA = new TF1("xF2pA", f1f2SFs, &InSANEStructureFunctions::EvaluateF2p, 0.2, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
  //leg->AddEntry(xF2pA,"F2p F1F209","l");
  xF2pA->SetParameter(0,Q2);
  xF2pA->SetLineColor(2);

  // Statistical 
  InSANEStructureFunctionsFromPDFs * myBBS = new InSANEStructureFunctionsFromPDFs();
  myBBS->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());
  TF1 * xF2pB = new TF1("xF2pB", myBBS, &InSANEStructureFunctions::EvaluateF2p, 0.2, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
  xF2pB->SetParameter(0,Q2);
  xF2pB->SetLineColor(kGreen);

   // -----------------
   // CTEQ
   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   TF1 * xF2pCTEQ = new TF1("xF2pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF2p, 0.2, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   xF2pCTEQ->SetParameter(0,Q2);
   xF2pCTEQ->SetLineColor(8);

   NMC95StructureFunctions * NMCSFs = new NMC95StructureFunctions();
   TF1 * xF2pNMC = new TF1("xF2pNMC", NMCSFs, &InSANEStructureFunctions::EvaluateF2p, 0.2, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   xF2pNMC->SetLineColor(4);
   xF2pNMC->SetParameter(0,Q2);

  TMultiGraph * mg = new TMultiGraph();
  TGraph * gr = nullptr;

  mg->Add( gr = new TGraph(f_F2p), "l");
  gr->SetLineColor(1);
  mg->Add( gr = new TGraph(f_F2p_TMC), "l");
  gr->SetLineColor(1);
  gr->SetLineStyle(2);
  mg->Add( gr = new TGraph(xF2pA), "l");
  mg->Add( gr = new TGraph(xF2pB), "l");
  mg->Add( gr = new TGraph(xF2pNMC), "l");


  mg->Draw("a");
}
