void d2_WW() {

  auto sf0 = new InSANEPolarizedStructureFunctionsFromPDFs( new LCWFPolarizedPartonDistributionFunctions);
  auto sf1 = new InSANEPolarizedStructureFunctionsFromPDFs( new BBPolarizedPDFs);
  //pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
  auto sf2 = new InSANEPolarizedStructureFunctionsFromPDFs( new JAM15PolarizedPDFs);
  auto sf3 = new InSANEPolarizedStructureFunctionsFromPDFs( new GSPolarizedPDFs);
  auto sf4 = new InSANEPolarizedStructureFunctionsFromPDFs( new DSSVPolarizedPDFs);
  auto sf5 = new InSANEPolarizedStructureFunctionsFromPDFs( new Stat2015PolarizedPDFs);

  std::vector<InSANEPolarizedStructureFunctionsFromPDFs*> sfs = {sf0,sf1,sf2,sf5,sf4};
  std::vector<TGraph*> grs_d2_WW;
  std::vector<TGraph*> grs_d2_TMC;

  std::vector<int> colors = {1,2,4,6,8,9};

  double x_min = 0.001;
  double x_max = 0.9;
  int    N     = 100;
  double dx    = (x_max-x_min)/double(N);


  double Q2_min = 1.0;
  double Q2_max = 6.0;
  int    NQ2    = 60;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2);

  for(auto aSF : sfs) {
    grs_d2_WW.push_back(  new TGraph() );
    grs_d2_TMC.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  auto pdf = sf2->GetPolarizedPDFs();
  double x_avg = (x_min+x_max)/2.0;
  std::cout << "g_1^{t3    }(" << x_avg << ") = " << pdf->g1p_Twist3(    x_avg, 5.0) << '\n';
  std::cout << "g_1^{t3 TMC}(" << x_avg << ") = " << pdf->g1p_Twist3_TMC(x_avg, 5.0) << '\n';
  std::cout << "g_1^{t3 BT }(" << x_avg << ") = " << pdf->g1p_BT_Twist3( x_avg, 5.0) << '\n';
  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;


    int iSF = 0;
    for(auto aSF : sfs) {

      auto pdfs = aSF->GetPolarizedPDFs();

      double d2_WW      = aSF->d2p_WW(Q2, x_min, x_max);
      grs_d2_WW[iSF]->SetPoint( iQ2, Q2, d2_WW);


      //double d2_TMC      = aSF->d2p_tilde_TMC(Q2, x_min, x_max);
      //grs_d2_TMC[iSF]->SetPoint(iQ2, Q2, d2_TMC);

      iSF++;
    }

  }

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.6,0.5,0.875,0.875);

  int iSF = 0;
  for(auto aSF : sfs) {

    grs_d2_WW[iSF]->SetLineColor(colors[iSF]);
    grs_d2_WW[iSF]->SetLineWidth(2);
    mg->Add(grs_d2_WW[iSF],"l");
    leg->AddEntry(grs_d2_WW[iSF], aSF->GetLabel(), "l");

    //grs_d2_TMC[iSF]->SetLineColor(colors[iSF]);
    //grs_d2_TMC[iSF]->SetLineWidth(2);
    //grs_d2_TMC[iSF]->SetLineStyle(2);
    //mg->Add(grs_d2_TMC[iSF],"l");
    //leg->AddEntry(grs_d2_TMC[iSF], aSF->GetLabel(), "l");

    iSF++;
  }

  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(0.0,0.001);
  leg->Draw();

  c->SaveAs("results/structure_functions/d2_WW.png");
  c->SaveAs("results/structure_functions/d2_WW.pdf");

}

