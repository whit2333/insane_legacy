Int_t F2d(Double_t Qsq = 4.5 ) {


	TCanvas * c = new TCanvas("F2d","F2d");

	NucDBManager * manager = NucDBManager::GetManager();

	TString Measurement = Form("F_{2}^{d}");
	TString Title       = Form("%s(x,Q^{2} = %.0f GeV^{2})",Measurement.Data(),Qsq);
	TString xAxisTitle  = Form("x");
	TString yAxisTitle  = Form("%s",Measurement.Data());

        TString LegOption  = Form("p");

	// Grab the data 

	TLegend * leg = new TLegend(0.6,0.6,0.8,0.8);
	leg->SetFillColor(kWhite);

        TMultiGraph *G = new TMultiGraph(); 

	TList * listOfMeas = manager->GetMeasurements("F2d");
	int N = listOfMeas->GetEntries(); 
	for(int i=0;i<N;i++){
		NucDBMeasurement * Meas = (NucDBMeasurement*)listOfMeas->At(i);
		TGraphErrors * graph = Meas->BuildGraph("x");
		graph->SetMarkerStyle(20+i);
		graph->SetMarkerSize(0.9);
		graph->SetMarkerColor(kBlack);
		graph->SetLineColor(kBlack);
		graph->SetLineWidth(1);
		G->Add(graph);
		leg->AddEntry(graph,Form("%s %s",Meas->GetExperimentName(),Meas->GetTitle() ),LegOption);
	}

	// Choose the models

	// CTEQ  
	InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
	cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());

	TF1 * CTEQ = new TF1("CTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF2d, 
			     0, 1, 1,"InSANEStructureFunctions","EvaluateF2d");
	CTEQ->SetParameter(0,Qsq);
	CTEQ->SetLineColor(6);
	leg->AddEntry(CTEQ,"CTEQ (per nucleon)","l");

	// F1F209 
	InSANEStructureFunctions *f1f2SFs = new F1F209StructureFunctions();
	TF1 * F1F209 = new TF1("F1F209", f1f2SFs, &InSANEStructureFunctions::EvaluateF2d, 
			       0.2, 1, 1,"InSANEStructureFunctions","EvaluateF2d");
	F1F209->SetParameter(0,Qsq);
	F1F209->SetLineColor(kBlue);
	leg->AddEntry(F1F209,"F1F209 (per nucleon)","l");

        // F1F209 Quasi Elastic
	InSANEStructureFunctions *f1f2QESFs = new F1F209QuasiElasticStructureFunctions();
	TF1 * F1F209QE = new TF1("F1F209QE", f1f2QESFs, &InSANEStructureFunctions::EvaluateF2d, 
			       0.2, 1, 1,"InSANEStructureFunctions","EvaluateF2d");
	F1F209QE->SetParameter(0,Qsq);
	F1F209QE->SetLineColor(kRed);
	leg->AddEntry(F1F209QE,"F1F209 QE (per nucleon)","l");

	// NMC95
	InSANEStructureFunctions *NMCSFs = new NMC95StructureFunctions();
	TF1 * NMC95 = new TF1("F1F209", NMCSFs, &InSANEStructureFunctions::EvaluateF2d, 
			       0, 1, 1,"InSANEStructureFunctions","EvaluateF2d");
	NMC95->SetParameter(0,Qsq);
	NMC95->SetLineColor(kGreen);
	leg->AddEntry(NMC95,"NMC95 (per nucleon)","l");

        // ----------------------------------------------------------------
        gr = new TGraph( CTEQ->DrawCopy("goff")->GetHistogram() );
        G->Add(gr,"l");

        gr = new TGraph( F1F209->DrawCopy("goff")->GetHistogram() );
        G->Add(gr,"l");

        gr = new TGraph( F1F209QE->DrawCopy("goff")->GetHistogram() );
        G->Add(gr,"l");

        gr = new TGraph( NMC95->DrawCopy("goff")->GetHistogram() );
        G->Add(gr,"l");



        // ----------------------------------------------------------------
        // Draw the plot 	
	TString DrawOption = Form("AP");
	G->Draw(DrawOption);
	G->SetTitle(Title);
	G->GetXaxis()->SetTitle(xAxisTitle);
	G->GetXaxis()->CenterTitle();
	G->GetYaxis()->SetTitle(yAxisTitle);
	G->GetYaxis()->CenterTitle();
	G->Draw(DrawOption);

        TGraph * gr = 0;

        //gr = new TGraph( CTEQ->DrawCopy("goff")->GetHistogram() );
        //G->Add(gr,"l");
	//F1F209->Draw("same");
	//F1F209QE->Draw("same");
	//NMC95->Draw("same");

	leg->Draw();

	TString prefix = Form("./results/structure_functions/");
	TString name   = Form("F2d"); 
	TString fn1    = prefix + name + Form(".png"); 
	TString fn2    = prefix + name + Form(".pdf"); 

	c->SaveAs(fn1);
	c->SaveAs(fn2);

	return(0);
}
