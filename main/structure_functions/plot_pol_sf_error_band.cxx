Int_t plot_pol_sf_error_band(Double_t Q2 = 10.0, Int_t aNumber = 3){

   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Qsqbin = new NucDBBinnedVariable("Qsquared","Q^{2}>1 GeV^2");
   Qsqbin->SetBinMinimum(1.0);
   Qsqbin->SetBinMaximum(100.0);

   NucDBBinnedVariable * Wbin = new NucDBBinnedVariable("W","W>3 GeV");
   Wbin->SetBinMinimum(3.0);
   Wbin->SetBinMaximum(100.0);

   TList * listOfMeas = manager->GetMeasurements("g1p");
   TLegend * leg = new TLegend(0.7,0.7,0.89,0.89);

   TF2         * xf = new TF2("xf","x*y",0,1,-1,1);
   TMultiGraph * mg = new TMultiGraph();

   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g1pMeas = (NucDBMeasurement*)listOfMeas->At(i);
       g1pMeas->ApplyFilterWithBin(Qsqbin);
       g1pMeas->ApplyFilterWithBin(Wbin);
       if(g1pMeas->GetNDataPoints() == 0 ) continue;
       TGraphErrors * graph = g1pMeas->BuildGraph("x");
       graph->Apply(xf);
       graph->SetMarkerStyle(gNiceMarkerStyle[i]);
       //graph->SetMarkerSize(1.6);
       //graph->SetMarkerColor(kBlue-2-i);
       //graph->SetLineColor(kBlue-2-i);
       //graph->SetLineWidth(1);
       mg->Add(graph,"p");
       leg->AddEntry(graph,Form("%s %s",g1pMeas->GetExperimentName(),g1pMeas->GetTitle() ),"lp");

       //g1pQsq1->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin));
       //g1pQsq2->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin2));

   }

   InSANEFunctionManager             * fman = InSANEFunctionManager::GetManager();
   //fman->PrintNameLists();
   InSANEPolarizedStructureFunctions * pSFs = fman->CreatePolSFs(3);

   TH1F * hg1      = new TH1F("hg1",     "g1p",50,0.01,0.99);
   TH1F * hg1Err   = new TH1F("hg1Err",  "g1p",50,0.01,0.99);
   TH1F * hg2      = new TH1F("hg2",     "g2p",50,0.01,0.99);
   TH1F * hg2Err   = new TH1F("hg2Err",  "g2p",50,0.01,0.99);
   TH1F * hg2WW    = new TH1F("hg2WW",   "g2p",50,0.01,0.99);
   TH1F * hg2ErrWW = new TH1F("hg2ErrWW","g2p",50,0.01,0.99);

   pSFs->GetValues(   hg1,   Q2,InSANEStructureFunctionBase::kg1p);
   pSFs->GetErrorBand(hg1Err,Q2,InSANEStructureFunctionBase::kg1p);

   pSFs->GetValues(   hg2,   Q2,InSANEStructureFunctionBase::kg2p);
   pSFs->GetErrorBand(hg2Err,Q2,InSANEStructureFunctionBase::kg2p);

   pSFs->GetValues(   hg2WW,   Q2,InSANEStructureFunctionBase::kg2pWW);
   pSFs->GetErrorBand(hg2ErrWW,Q2,InSANEStructureFunctionBase::kg2pWW);

   gROOT->GetColor(41)->SetAlpha(0.7);
   gROOT->GetColor(40)->SetAlpha(0.7);

   hg1Err->SetFillColor(40);
   hg2Err->SetFillColor(41);
   hg2ErrWW->SetFillColor(41);

   THStack * hs = new THStack("errorBands","#Delta q Error Bands");
   hs->Add(hg1Err,"e3");
   hs->Add(hg1,"C hist");
   hs->Add(hg2Err,"e3");
   hs->Add(hg2,"C hist");
   hs->Add(hg2ErrWW,"e3");
   hs->Add(hg2WW,"C hist");

   TCanvas * c = new TCanvas();

   //hs2->Draw("nostack");
   mg->Draw("a");
   hs->Draw("nostack same");
   leg->Draw();

   c->SaveAs(Form("results/structure_functions/plot_pol_sf_error_band_%d.png",aNumber));

   return 0;
}
