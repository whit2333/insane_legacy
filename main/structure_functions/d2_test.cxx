void d2_test(double Q2 = 3.0){

  std::string  file_name = "d2_test.txt";

  auto   sf = new InSANEPolarizedStructureFunctionsFromPDFs();
  //auto pdfs = new StatisticalPolarizedPDFs();//aNumber);
  auto pdfs = new JAM15PolarizedPDFs(0);
  ((InSANEPolarizedStructureFunctionsFromPDFs*)sf)->SetPolarizedPDFs(pdfs);
  //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  //auto   sf  = fman->CreatePolSFs(14);
  //JAM15PolarizedPDFs  * pdfs = new JAM15PolarizedPDFs();

  double x_min = 0.001;
  double x_max = 0.99;
  int    N     = 20;
  double dx    = (x_max-x_min)/double(N);

  TMultiGraph * mg              = new TMultiGraph();
  TGraph      * gr_Dp           = new TGraph();

  TGraph      * gr_d2_CN          = new TGraph();
  TGraph      * gr_d2_CN_t2       = new TGraph();
  TGraph      * gr_d2_CN_t3       = new TGraph();
  TGraph      * gr_d2_t3          = new TGraph();
  TGraph      * gr_d2_g2_minus_WW = new TGraph();

  TGraph      * gr_d2_CN_TMC          = new TGraph();
  TGraph      * gr_d2_CN_t2_TMC       = new TGraph();
  TGraph      * gr_d2_CN_t3_TMC       = new TGraph();
  TGraph      * gr_d2_t3_TMC          = new TGraph();
  TGraph      * gr_d2_g2_minus_WW_TMC = new TGraph();

  TGraph      * gr_d2_nacht      = new TGraph();
  TGraph      * gr_d2_nacht_TMC  = new TGraph();

  TGraph      * gr_d2_int_ideal  = new TGraph();
  TGraph      * gr_d2_int_WWsub  = new TGraph();
  TGraph      * gr_d2_int_WWsub2 = new TGraph();

  TMultiGraph * mg2             = new TMultiGraph();
  TMultiGraph * mg3             = new TMultiGraph();

  TGraph      * gr_g2_t3_noTMC  = new TGraph();
  TGraph      * gr_g2_t3_TMC    = new TGraph();
  TGraph      * gr_g2_t2_TMC    = new TGraph();
  TGraph      * gr_g2_t2_t3_TMC    = new TGraph();

  // -------------------------------------------------
  //
  int ip = 0;
  for(unsigned int ix = 0; ix<N ; ix++) {

    double x = x_min + double(ix)*dx;

    double g1_t2        = sf->g1p_Twist2(x,Q2);
    double g1_t3        = sf->g1p_Twist3(x,Q2);
    double g1_t2_TMC    = sf->g1p_Twist2_TMC(x,Q2);
    double g1_t3_TMC    = sf->g1p_Twist3_TMC(x,Q2);
    double g1_t2_t3_TMC = g1_t2_TMC + g1_t3_TMC;

    double Dp            = pdfs->Dp_Twist3(x,Q2);
    double M2n_p_int_TMC = sf->M2nIntegrand_TMC_p(3,x,Q2);
    double M2n_p_int     = sf->M2nIntegrand_p(3,x,Q2);

    double g2_t2        = sf->g2p_Twist2(x,Q2);
    double g2_t3        = sf->g2p_Twist3(x,Q2);
    double g2_WW        = sf->g2pWW(x,Q2);
    double g2_t3_TMC    = sf->g2p_Twist3_TMC(x,Q2);
    double g2_t2_TMC    = sf->g2p_Twist2_TMC(x,Q2);
    double g2_WW_TMC    = sf->g2pWW_TMC(x,Q2);
    double g2_WW_TMC_t3 = sf->g2pWW_TMC_t3(x,Q2);

    double d2_CN_t2 = x*x*(2.0*g1_t2 + 3.0*g2_t2);
    double d2_CN_t3 = x*x*(2.0*g1_t3 + 3.0*g2_t3);
    double d2_tw3   = 3.0*x*x*g2_t3;
    double d2_g2_minus_WW = 3.0*x*x*(g2_t2 + g2_t3 - g2_WW);

    double d2_CN_t2_TMC = x*x*(2.0*g1_t2_TMC + 3.0*g2_t2_TMC);
    double d2_CN_t3_TMC = x*x*(2.0*g1_t3_TMC + 3.0*g2_t3_TMC);
    double d2_tw3_TMC   = 3.0*x*x*g2_t3_TMC;
    double d2_g2_minus_WW_TMC = 3.0*x*x*(g2_t2_TMC + g2_t3_TMC - g2_WW);

    if( ip==0 ){
      gr_Dp->SetPoint(ip,x,0.0);
      gr_d2_CN->SetPoint( ip, x, 0.0);
      gr_d2_CN_t2->SetPoint( ip, x, 0.0);
      gr_d2_CN_t3->SetPoint( ip, x, 0.0);
      gr_d2_t3->SetPoint( ip, x, 0.0);
      gr_d2_CN_TMC   ->SetPoint( ip, x, 0.0);
      gr_d2_CN_t2_TMC->SetPoint( ip, x, 0.0);
      gr_d2_CN_t3_TMC->SetPoint( ip, x, 0.0);
      gr_d2_t3_TMC   ->SetPoint( ip, x, 0.0);
      gr_d2_nacht    ->SetPoint( ip, x, 0.0);
      gr_d2_nacht_TMC->SetPoint( ip, x, 0.0);
      gr_d2_g2_minus_WW->SetPoint(     ip, x, 0.0);
      gr_d2_g2_minus_WW_TMC->SetPoint( ip, x, 0.0);
      ip++;
    }

    gr_Dp->SetPoint(ip,x,x*x*Dp);
    gr_d2_CN ->SetPoint(       ip, x, d2_CN_t2 + d2_CN_t3);
    gr_d2_CN_t2 ->SetPoint(    ip, x, d2_CN_t2           );
    gr_d2_CN_t3 ->SetPoint(    ip, x, d2_CN_t3           );
    gr_d2_t3->SetPoint(        ip, x, d2_tw3             );
    gr_d2_CN_TMC   ->SetPoint( ip, x, d2_CN_t2_TMC + d2_CN_t3_TMC);
    gr_d2_CN_t2_TMC->SetPoint( ip, x, d2_CN_t2_TMC           );
    gr_d2_CN_t3_TMC->SetPoint( ip, x, d2_CN_t3_TMC           );
    gr_d2_t3_TMC   ->SetPoint( ip, x, d2_tw3_TMC             );
    gr_d2_nacht    ->SetPoint( ip, x, M2n_p_int      );
    gr_d2_nacht_TMC->SetPoint( ip, x, M2n_p_int_TMC  );
    gr_d2_g2_minus_WW->SetPoint(     ip, x, d2_g2_minus_WW    );
    gr_d2_g2_minus_WW_TMC->SetPoint( ip, x, d2_g2_minus_WW_TMC);

    //double sub1 = (g2_t2_TMC+g2_t3_TMC) - g2_WW + g2_WW_TMC_t3;
    //double sub2 = sub1 - (g2_t2_TMC-g2_t2);
    //gr_d2_int_WWsub2->SetPoint( ip, x, 3.0*x*x*( sub2 - (g2_t3_TMC-g2_t3)));

    //gr_d2_nacht->SetPoint( ip, x, 2.0*M2n_p_int );
    //gr_Dp->SetPoint( ip, x, x*x*Dp);

    ip++;

    if( ip==N+1 ){
      gr_Dp->SetPoint(ip,x,0.0);
      gr_d2_CN->SetPoint( ip, x, 0.0);
      gr_d2_CN_t2->SetPoint( ip, x, 0.0);
      gr_d2_CN_t3->SetPoint( ip, x, 0.0);
      gr_d2_t3->SetPoint( ip, x, 0.0);
      gr_d2_CN_TMC   ->SetPoint( ip, x, 0.0);
      gr_d2_CN_t2_TMC->SetPoint( ip, x, 0.0);
      gr_d2_CN_t3_TMC->SetPoint( ip, x, 0.0);
      gr_d2_t3_TMC   ->SetPoint( ip, x, 0.0);
      gr_d2_nacht    ->SetPoint( ip, x, 0.0);
      gr_d2_nacht_TMC->SetPoint( ip, x, 0.0);
      gr_d2_g2_minus_WW->SetPoint( ip, x, 0.0);
      gr_d2_g2_minus_WW_TMC->SetPoint( ip, x, 0.0);
      ip++;
    }
  }

  double int_d2_Dp          = gr_Dp           ->Integral();

  double int_d2_CN          = gr_d2_CN        ->Integral();
  double int_d2_CN_t2       = gr_d2_CN_t2     ->Integral();
  double int_d2_CN_t3       = gr_d2_CN_t3     ->Integral();
  double int_d2_t3          = gr_d2_t3        ->Integral();
  double int_d2_g2_minus_WW = gr_d2_g2_minus_WW->Integral();

  double int_d2_CN_TMC          = gr_d2_CN_TMC    ->Integral();
  double int_d2_CN_t2_TMC       = gr_d2_CN_t2_TMC ->Integral();
  double int_d2_CN_t3_TMC       = gr_d2_CN_t3_TMC ->Integral();
  double int_d2_t3_TMC          = gr_d2_t3_TMC    ->Integral();
  double int_d2_g2_minus_WW_TMC = gr_d2_g2_minus_WW_TMC->Integral();

  double int_d2_nacht     = gr_d2_nacht    ->Integral();
  double int_d2_nacht_TMC = gr_d2_nacht_TMC->Integral();

  double M11_p     = sf->M1n_p(    1,Q2,x_min,x_max)    ;
  double M11_TMC_p = sf->M1n_TMC_p(1,Q2,x_min,x_max) ;

  double M23_p     = sf->M2n_p(    3,Q2,x_min,x_max)    ;
  double M25_p     = sf->M2n_p(    5,Q2,x_min,x_max)    ;
  double M27_p     = sf->M2n_p(    7,Q2,x_min,x_max)    ;
  double M29_p     = sf->M2n_p(    9,Q2,x_min,x_max)    ;

  double M23_TMC_p = sf->M2n_TMC_p(3,Q2,x_min,x_max) ;
  double M25_TMC_p = sf->M2n_TMC_p(5,Q2,x_min,x_max) ;
  double M27_TMC_p = sf->M2n_TMC_p(7,Q2,x_min,x_max) ;
  double M29_TMC_p = sf->M2n_TMC_p(9,Q2,x_min,x_max) ;

  double y2 = (M_p/GeV)*(M_p/GeV)/Q2;

  double I_nacht_TMC  = 2.0*(M23_TMC_p + 6.0*M25_TMC_p*y2 + 12.0*M27_TMC_p*y2*y2 + 20.0*M29_TMC_p*y2*y2*y2);
  double I_nacht      = 2.0*(M23_p     + 6.0*M25_p*y2     + 12.0*M27_p*y2*y2     + 20.0*M29_p*y2*y2*y2);

  double d2p_tilde     = sf->d2p_tilde(Q2,x_min,x_max)    ;
  double d2p_tilde_TMC = sf->d2p_tilde_TMC(Q2,x_min,x_max)    ;

  double M23_TMC_test_p = sf->M2n_TMC_test_p(3,Q2,x_min,x_max) ;
  double d2_Oscar       = sf->d2_Oscar(Q2,x_min,x_max)    ;

  std::ofstream output_file(file_name.c_str(), std::ofstream::out | std::ofstream::app);

  output_file << std::fixed;
  output_file << "                   " << std::endl;
  output_file << " Q2 : "              << Q2 << " GeV^2" << std::endl;
  output_file << " x ["                << std::setprecision(5) << x_min 
              << ", "                  << std::setprecision(5) << x_max 
              << "] "                  << std::endl;
  output_file << "    d2_CN        = " << std::setprecision(5) << int_d2_CN << '\n'
              << "                 = " << std::setprecision(5) << int_d2_CN_t2  
              << " (t2) + "            << std::setprecision(5) << int_d2_CN_t3  
              << " (t3)"               << std::endl;
  output_file << "    d2_CN_TMC    = " << std::setprecision(5) << int_d2_CN_TMC << '\n'
              << "                 = " << std::setprecision(5) << int_d2_CN_t2_TMC  
              << " (t2) + "            << std::setprecision(5) << int_d2_CN_t3_TMC  
              << " (t3)"               << std::endl;
  output_file << "   2*d2_D_p_t3   = " << std::setprecision(5) << 2.0*int_d2_Dp        << std::endl;
  output_file << "   d2_t3         = " << std::setprecision(5) << int_d2_t3        << std::endl;
  output_file << "   d2_t3_TMC     = " << std::setprecision(5) << int_d2_t3_TMC    << std::endl;
  output_file << "   d2    (g2-WW) = " << std::setprecision(5) << int_d2_g2_minus_WW        << std::endl;
  output_file << "   d2_TMC(g2-WW) = " << std::setprecision(5) << int_d2_g2_minus_WW_TMC    << std::endl;
  output_file << "   2*M23_p       = " << std::setprecision(5) << 2.0*M23_p     << std::endl;
  output_file << "   2*M23_TMC_p   = " << std::setprecision(5) << 2.0*M23_TMC_p << std::endl;
  output_file << "   d2_Oscar      = " << std::setprecision(5) << d2_Oscar << std::endl;
  output_file << "   2*M23_TMCtest = " << std::setprecision(5) << 2.0*M23_TMC_test_p << std::endl;
  output_file << "   2*M23/Oscar   = " << std::setprecision(5) << 2.0*M23_TMC_p/d2_Oscar << std::endl;
  output_file << "   2*M23/TMCtest = " << std::setprecision(5) << 2.0*M23_TMC_p/(2.0*M23_TMC_test_p) << std::endl;

  output_file << "   d3_nacht      = " << std::setprecision(5) << int_d2_nacht     << std::endl;
  output_file << "   d3_nacht_TMC  = " << std::setprecision(5) << int_d2_nacht_TMC << std::endl;
  output_file << "   d2p_I(no TMC) = " << std::setprecision(5) << d2p_tilde << std::endl;
  output_file << "   d2p_I(w/ TMC) = " << std::setprecision(5) << d2p_tilde_TMC << std::endl;
  output_file << "   I_Nacht       = " << std::setprecision(5) << I_nacht << std::endl;
  output_file << "                 = " << "2.0*(" 
              << std::setprecision(5) << M23_p << " + 6.0* "
              << std::setprecision(5) << M25_p << "*y2 + 12.0* " 
              << std::setprecision(5) << M27_p << "*y2*y2 + 20.0* " 
              << std::setprecision(5) << M29_p << "*y2*y2*y2 )" << std::endl;
  output_file << "   I_Nacht_TMC   = " << std::setprecision(5) << I_nacht_TMC << std::endl;
  output_file << "                 = " << "2.0*(" 
              << std::setprecision(5) << M23_TMC_p << " + 6.0* "
              << std::setprecision(5) << M25_TMC_p << "*y2 + 12.0* " 
              << std::setprecision(5) << M27_TMC_p << "*y2*y2 + 20.0* " 
              << std::setprecision(5) << M29_TMC_p << "*y2*y2*y2 )" << std::endl;
  output_file << "I_CN(TMC)/I_Nacht(noTMC) = " << std::setprecision(5) << d2p_tilde_TMC/I_nacht << std::endl;


  //output_file << "   2*M11_p       = " << std::setprecision(5) << 2.0*M11_p     << std::endl;
  //output_file << "   2*M11_TMC_p   = " << std::setprecision(5) << 2.0*M11_TMC_p << std::endl;

  output_file.close();


  // -------------------------------------------------
  //
  gr_d2_CN->SetLineWidth(2);
  gr_d2_CN->SetLineColor(4);

  gr_d2_t3->SetLineWidth(2);
  gr_d2_t3->SetLineColor(1);

  gr_d2_CN_t2->SetLineWidth(2);
  gr_d2_CN_t2->SetLineColor(2);

  gr_d2_CN_t3->SetLineWidth(2);
  gr_d2_CN_t3->SetLineColor(8);

  gr_d2_CN_TMC->SetLineWidth(2);
  gr_d2_CN_TMC->SetLineColor(4);
  gr_d2_CN_TMC->SetLineStyle(2);

  gr_d2_t3_TMC->SetLineWidth(2);
  gr_d2_t3_TMC->SetLineColor(1);
  gr_d2_t3_TMC->SetLineStyle(2);

  gr_d2_CN_t2_TMC->SetLineWidth(2);
  gr_d2_CN_t2_TMC->SetLineColor(2);
  gr_d2_CN_t2_TMC->SetLineStyle(2);

  gr_d2_CN_t3_TMC->SetLineWidth(2);
  gr_d2_CN_t3_TMC->SetLineColor(8);
  gr_d2_CN_t3_TMC->SetLineStyle(2);

  gr_d2_nacht->SetLineWidth(2);
  gr_d2_nacht->SetLineColor(1);

  gr_d2_nacht_TMC->SetLineStyle(2);
  gr_d2_nacht_TMC->SetLineWidth(2);
  gr_d2_nacht_TMC->SetLineColor(1);

  //gr_d2_int_exp2->SetLineWidth(2);
  //gr_d2_int_exp2->SetLineColor(1);

  //gr_d2_int_WWsub->SetLineWidth(2);
  //gr_d2_int_WWsub->SetLineColor(3);
  //gr_d2_int_WWsub->SetLineStyle(2);

  //gr_d2_int_WWsub2->SetLineStyle(2);
  //gr_d2_int_WWsub2->SetLineWidth(2);
  //gr_d2_int_WWsub2->SetLineColor(7);
  // -------------------------------------------------
  //

  // -------------------------------------------------
  //
  TCanvas * c   = nullptr;
  TLegend * leg = nullptr;
  
  // ----------------------------------
  c   = new TCanvas();
  leg = new TLegend(0.7,0.8,0.99,0.99);
  mg->Add(gr_d2_CN,         "l");
  mg->Add(gr_d2_CN_t2,      "l");
  mg->Add(gr_d2_t3,         "l");
  mg->Add(gr_d2_t3_TMC,     "l");
  mg->Add(gr_d2_CN_t3,      "l");
  mg->Add(gr_d2_nacht,      "l");
  mg->Add(gr_d2_nacht_TMC,  "l");
  leg->AddEntry( gr_d2_CN,     "d2 CN", "l");
  leg->AddEntry( gr_d2_CN_t2,  "d2 CN #tau2", "l");
  leg->AddEntry( gr_d2_CN_t3,  "d2 CN #tau3", "l");
  leg->AddEntry( gr_d2_t3,     "d2 = 3 x^{2} g_{2}", "l");
  leg->AddEntry( gr_d2_t3_TMC, "d2 TMC = 3 x^{2} g_{2}", "l");
  leg->SetHeader(sf->GetLabel());
  mg->Draw("a");
  mg->GetXaxis()->SetTitle("x");
  mg->Draw("a");
  leg->Draw();
  c->SaveAs("results/structure_functions/d2_test_0.png");
  c->SaveAs("results/structure_functions/d2_test_0.pdf");

  // ----------------------------------
  //c   = new TCanvas();
  //leg = new TLegend(0.01,0.2,0.3,0.5);
  //leg->AddEntry( gr_WW         , "x^{2}g_{2}^{WW}[g_{1}^{no TMC}]", "l");
  //leg->AddEntry( gr_WW_TMC     , "x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  //leg->AddEntry( gr_delta_WW   , "x^{2}g_{2}^{WW}[g_{1}^{no TMC}] - x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  //leg->AddEntry( gr_delta_WW_TMC, "x^{2}g_{2}^{TMC+#tau2+#tau3} - x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  //leg->AddEntry( gr_g2_t3_noTMC, "x^{2}g_{2}^{#tau3}","l");
  //leg->AddEntry( gr_g2_t3_TMC  , "x^{2}g_{2}^{TMC+#tau3}"   , "l");
  //leg->AddEntry( gr_g2_t2_TMC  , "x^{2}g_{2}^{TMC+#tau2}"   , "l");
  //leg->AddEntry( gr_g2_t2_t3_TMC  , "x^{2}g_{2}^{TMC+#tau2+#tau3}"   , "l");
  //mg2->Draw("a");
  //mg2->GetXaxis()->SetTitle("x");
  //mg2->Draw("a");
  //leg->Draw();
  //c->SaveAs("results/structure_functions/d2_test_1.png");
  //c->SaveAs("results/structure_functions/d2_test_1.pdf");


  //// ----------------------------------
  //c   = new TCanvas();
  //leg = new TLegend(0.7,0.8,0.99,0.99);
  ////leg->AddEntry( gr_delta_WW    , "x^{2}g_{2}^{WW}[g_{1}^{no TMC}] - x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  //leg->AddEntry( gr_delta_WW_TMC, "x^{2}g_{2}^{TMC+#tau2+#tau3} - x^{2}g_{2}^{WW}[g_{1}^{TMC}]"   , "l");
  //leg->AddEntry( gr_g2_t3_noTMC , "x^{2}g_{2}^{#tau3}","l");
  //leg->AddEntry( gr_g2_t3_TMC   , "x^{2}g_{2}^{TMC+#tau3}"   , "l");
  //leg->AddEntry( gr_d2_nacht    , "M_2^{3} integrand"   , "l");
  //leg->AddEntry( gr_d2_int_exp    , "d2 int exp"   , "l");
  //leg->AddEntry( gr_d2_int_exp2    , "d2 int exp2"   , "l");
  //leg->AddEntry( gr_d2_int_ideal    , "d2 int ideal" , "l");
  //leg->AddEntry( gr_d2_int_WWsub    , "d2 int WWsub", "l");
  //leg->AddEntry( gr_d2_int_WWsub2   , "d2 int WWsub2", "l");
  //mg3->Draw("a");
  //mg3->GetXaxis()->SetTitle("x");
  //mg3->Draw("a");
  //leg->Draw();
  //c->SaveAs("results/structure_functions/d2_test_3.png");
  //c->SaveAs("results/structure_functions/d2_test_3.pdf");
  //// ----------------------------------


}

