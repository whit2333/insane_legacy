/** Plots models and data for g1p at Qsq. */
Int_t xg1p(Double_t Qsq = 5.0,Double_t QsqBinWidth=3.0){

   TCanvas * c = new TCanvas("xg1p","xg1p");

   // Create Legend
   TLegend * leg = new TLegend(0.6,0.5,0.875,0.875);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   InSANEPolarizedStructureFunctionsFromPDFs *pSFsD  = new InSANEPolarizedStructureFunctionsFromPDFs(); 
   pSFsD->SetPolarizedPDFs( new StatisticalPolarizedPDFs); 

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg1p = new TF1("xg1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   TF1 * xg1pA = new TF1("xg1pA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   TF1 * xg1pB = new TF1("xg1pB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   TF1 * xg1pC = new TF1("xg1pC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   TF1 * xg1pD = new TF1("xg1pC", pSFsD, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");

   xg1p->SetParameter(0,Qsq);
   xg1pA->SetParameter(0,Qsq);
   xg1pB->SetParameter(0,Qsq);
   xg1pC->SetParameter(0,Qsq);
   xg1pD->SetParameter(0,Qsq);

   xg1p->SetLineColor(1);
   xg1pA->SetLineColor(3);
   xg1pB->SetLineColor(kOrange-1);
   xg1pC->SetLineColor(kCyan+1);
   xg1pD->SetLineColor(kMagenta);

   Int_t width = 3;
   xg1p->SetLineWidth(width);
   xg1pA->SetLineWidth(width);
   xg1pB->SetLineWidth(width);
   xg1pC->SetLineWidth(width);
   xg1pD->SetLineWidth(width);

   xg1p->SetLineStyle(1);
   xg1pA->SetLineStyle(1);
   xg1pB->SetLineStyle(1);
   xg1pC->SetLineStyle(1);
   xg1pD->SetLineStyle(1);

   xg1pA->SetMaximum(0.1);
   xg1pA->SetMinimum(-0.03);

   xg1pA->Draw();

   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   // Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   // Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g1pQsq1 = new NucDBMeasurement("g1pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   TF2 * xf = new TF2("xf","x*y",0,1,-1,1);
   TList * listOfMeas = manager->GetMeasurements("g1p");
   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g1pMeas = (NucDBMeasurement*)listOfMeas->At(i);
       TGraphErrors * graph = g1pMeas->BuildGraph("x");
       graph->Apply(xf);
       graph->SetMarkerStyle(20+i);
       graph->SetMarkerSize(1.6);
       graph->SetMarkerColor(kBlue-2-i);
       graph->SetLineColor(kBlue-2-i);
       graph->SetLineWidth(1);

       if(i==0) {
          g1pMeas->fGraph->Draw("p");
          g1pQsq1->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin));
       }else{
          g1pMeas->fGraph->Draw("p");
          g1pQsq1->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin));
       }
       leg->AddEntry(g1pMeas->fGraph,Form("%s %s",g1pMeas->GetExperimentName(),g1pMeas->GetTitle() ),"lp");

   }

   TGraphErrors * gr = g1pQsq1->BuildGraph();
   gr->Apply(xf);
   gr->SetMarkerColor(2);
   gr->SetLineColor(2);
   gr->Draw("p");

   xg1pB->Draw("same");
   xg1pC->Draw("same");
   xg1pD->Draw("same");
   xg1p->Draw("same");


   /// Complete the Legend 

   leg->AddEntry(g1pQsq1->fGraph,Form("%s %s","All g1p data",g1pQsq1->GetTitle() ),"ep");
   leg->AddEntry(xg1p,"xg1p BB ","l");
   leg->AddEntry(xg1pA,"xg1p DNS2005 ","l");
   leg->AddEntry(xg1pB,"xg1p AAC ","l");
   leg->AddEntry(xg1pC,"xg1p GS ","l");
   leg->AddEntry(xg1pD,"xg1p Stat ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.16,0.85,Form("xg_{1}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));


   c->SaveAs("results/structure_functions/xg1p.png");
   c->SaveAs("results/structure_functions/xg1p.ps");

   return(0);
}
