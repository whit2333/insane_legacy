Int_t g1p(Double_t Qsq = 1.1,Double_t QsqBinWidth=1.0){

   TCanvas * c = new TCanvas("g1p","g1p");
   // Create Legend
   TLegend * leg = new TLegend(0.6,0.5,0.875,0.875);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsD = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsD->SetPolarizedPDFs( new DSSVPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsE = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsE->SetPolarizedPDFs( new BBSPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromVCSAs * pSFsF = new InSANEPolarizedStructureFunctionsFromVCSAs();
   pSFsF->SetVCSAs( new MAIDVirtualComptonAsymmetries());

   InSANEPolarizedStructureFunctions * pSFsG = new LowQ2PolarizedStructureFunctions();

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg1p = new TF1("g1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pA = new TF1("g1pA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pB = new TF1("g1pB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pC = new TF1("g1pC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pD = new TF1("g1pD", pSFsD, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pE = new TF1("g1pE", pSFsE, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pF = new TF1("g1pF", pSFsF, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");
   TF1 * xg1pG = new TF1("g1pG", pSFsG, &InSANEPolarizedStructureFunctions::Evaluateg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1p");

   xg1p->SetParameter(0,Qsq);
   xg1pA->SetParameter(0,Qsq);
   xg1pB->SetParameter(0,Qsq);
   xg1pC->SetParameter(0,Qsq);
   xg1pD->SetParameter(0,Qsq);
   xg1pE->SetParameter(0,Qsq);
   xg1pF->SetParameter(0,Qsq);
   xg1pG->SetParameter(0,Qsq);

   xg1p->SetLineColor(kBlack);
   xg1pA->SetLineColor(kRed);
   xg1pB->SetLineColor(kOrange -1);
   xg1pC->SetLineColor(kViolet-2);
   xg1pD->SetLineColor(kBlue);
   xg1pE->SetLineColor(kGreen);
   xg1pF->SetLineColor(kGreen+1);
   xg1pG->SetLineColor(kBlue+1);

   Int_t width = 3;
   xg1p->SetLineWidth(width);
   xg1pA->SetLineWidth(width);
   xg1pB->SetLineWidth(width);
   xg1pC->SetLineWidth(width);
   xg1pD->SetLineWidth(width);
   xg1pE->SetLineWidth(width);
   xg1pF->SetLineWidth(width);
   xg1pG->SetLineWidth(width);

   xg1p->SetLineStyle(1);
   xg1pA->SetLineStyle(1);
   xg1pB->SetLineStyle(1);
   xg1pC->SetLineStyle(1);
   xg1pD->SetLineStyle(2);
   xg1pE->SetLineStyle(2);
   xg1pF->SetLineStyle(1);
   xg1pG->SetLineStyle(1);


   xg1p->Draw();
   xg1pA->Draw("same");
   xg1pB->Draw("same");
   xg1pC->Draw("same");
   xg1pD->Draw("same");
   xg1pE->Draw("same");
   xg1pF->Draw("same");
   xg1pG->Draw("same");

/*
   /// Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();
   /// Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);
   /// Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g1pQsq1 = new NucDBMeasurement("g1pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   TList * listOfMeas = manager->GetMeasurements("g1p");
   for(int i =0; i<listOfMeas->GetEntries();i++) {
//
       NucDBMeasurement * g1pMeas = (NucDBMeasurement*)listOfMeas->At(i);
       g1pMeas->BuildGraph();
//
       g1pMeas->fGraph->SetMarkerStyle(20+i);
       g1pMeas->fGraph->SetMarkerSize(1.6);
       g1pMeas->fGraph->SetMarkerColor(kBlue-2-i);
       g1pMeas->fGraph->SetLineColor(kBlue-2-i);
       g1pMeas->fGraph->SetLineWidth(1);
//
       if(i==0) {
          g1pMeas->fGraph->Draw("p");
          g1pQsq1->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin));
       }else{
          g1pMeas->fGraph->Draw("p");
          g1pQsq1->AddDataPoints(g1pMeas->FilterWithBin(Qsqbin));
       }
       leg->AddEntry(g1pMeas->fGraph,Form("%s %s",g1pMeas->fExperiment.Data(),g1pMeas->GetTitle() ),"lp");
//
   }

   g1pQsq1->BuildGraph();
   g1pQsq1->fGraph->SetMarkerColor(2);
   g1pQsq1->fGraph->SetLineColor(2);
   g1pQsq1->fGraph->Draw("p");

*/
   /// Complete the Legend 

//   leg->AddEntry(g1pQsq1->fGraph,Form("%s %s","All g1p data",g1pQsq1->GetTitle() ),"ep");
   leg->SetFillColor(10);
   leg->AddEntry(xg1p, "BB ","l");
   leg->AddEntry(xg1pA,"DNS2005 ","l");
   leg->AddEntry(xg1pB,"AAC ","l");
   leg->AddEntry(xg1pC,"GS ","l");
   leg->AddEntry(xg1pD,"DSSV ","l");
   leg->AddEntry(xg1pE,"BBS ","l");
   leg->AddEntry(xg1pF,"MAID07 ","l");
   leg->AddEntry(xg1pG,"MAID+DSSV ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.25,0.85,Form("g_{1}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));


   c->SaveAs("results/structure_functions/g1p.png");
   c->SaveAs("results/structure_functions/g1p.ps");

   return(0);
}
