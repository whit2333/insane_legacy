Int_t F2n() {

	Double_t Qsq; 

        cout << "Enter Q2 (GeV^2): ";
        cin  >> Qsq; 

	TString Measurement = Form("F_{2}^{n}");
	TString Title       = Form("%s(x,Q^{2} = %.0f GeV^{2})",Measurement.Data(),Qsq);
	TString xAxisTitle  = Form("x");
	TString yAxisTitle  = Form("%s",Measurement.Data());

	TCanvas * c = new TCanvas("F2n","F2n");

	NucDBManager * manager = NucDBManager::GetManager();

        TString LegOption  = Form("p");

	// Grab the data 

	TLegend * leg = new TLegend(0.6,0.6,0.8,0.8);
	leg->SetFillColor(kWhite);

        TMultiGraph *G = new TMultiGraph(); 

	TList * listOfMeas = manager->GetMeasurements("F2n");
	int N = listOfMeas->GetEntries(); 
	if(N==0){
		cout << "There are no data!" << endl;
	}else{
		for(int i=0;i<N;i++){
			NucDBMeasurement * Meas = (NucDBMeasurement*)listOfMeas->At(i);
			TGraphErrors * graph = Meas->BuildGraph("x");
			graph->SetMarkerStyle(20+i);
			graph->SetMarkerSize(0.9);
			graph->SetMarkerColor(kBlack);
			graph->SetLineColor(kBlack);
			graph->SetLineWidth(1);
			G->Add(graph);
			leg->AddEntry(graph,Form("%s %s",Meas->GetExperimentName(),Meas->GetTitle() ),LegOption);
		}
	}

	// Choose the models

	// CTEQ  
	InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
	cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());

	TF1 * xF2nCTEQ = new TF1("xF2nCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF2n, 
			0, 1, 1,"InSANEStructureFunctions","EvaluateF2n");
	xF2nCTEQ->SetParameter(0,4.5);
	leg->AddEntry(xF2nCTEQ,"F2n CTEQ","l");
	xF2nCTEQ->SetLineColor(6);

	// F1F209 
	InSANEStructureFunctions *f1f2SFs = new F1F209StructureFunctions();
	TF1 * xF2nA = new TF1("xF2nA", f1f2SFs, &InSANEStructureFunctions::EvaluateF2n, 
			0, 1, 1,"InSANEStructureFunctions","EvaluateF2n");
	xF2nA->SetParameter(0,4.5);
	xF2nA->SetLineColor(4);
	leg->AddEntry(xF2nA,"F2n F1F209","l");

        // NMC95 
       	NMC95StructureFunctions *nmcSFs = new NMC95StructureFunctions();
	TF1 * fnmc = new TF1("xF2nA", nmcSFs, &InSANEStructureFunctions::EvaluateF2n, 
			0, 1, 1,"InSANEStructureFunctions","EvaluateF2n");
	fnmc->SetParameter(0,4.5);
	fnmc->SetLineColor(kGreen);
	leg->AddEntry(fnmc,"F2n NMC95","l");

        // Draw the plot 	
	TString DrawOption = Form("AP");
	if(N>0){

		G->Draw(DrawOption);
		G->SetTitle(Title);
		G->GetXaxis()->SetTitle(xAxisTitle);
		G->GetXaxis()->CenterTitle();
		G->GetYaxis()->SetTitle(yAxisTitle);
		G->GetYaxis()->CenterTitle();
		G->Draw(DrawOption);
	}

	xF2nCTEQ->Draw("same");
	xF2nA->Draw("same");
	fnmc->Draw("same");

	leg->Draw();

	TString prefix = Form("./plots/structure_functions/");
	TString name   = Form("F2n"); 
	TString fn1    = prefix + name + Form(".png"); 
	TString fn2    = prefix + name + Form(".pdf"); 

	// c->SaveAs(fn1);
	// c->SaveAs(fn2);

	return(0);
}
