Int_t F2p_cteq() {

	Double_t Qsq; 

        cout << "Enter Q2 (GeV^2): ";
        cin  >> Qsq; 

	TCanvas * c = new TCanvas("F2p","F2p");

	NucDBManager * manager = NucDBManager::GetManager();

	TString Measurement = Form("F_{2}^{p}");
        TString LegOption  = Form("p");

	// Grab the data 

	TLegend * leg = new TLegend(0.6,0.6,0.8,0.8);
	leg->SetFillColor(kWhite);

        TMultiGraph *G = new TMultiGraph(); 

	TList * listOfMeas = manager->GetMeasurements("F2p");
	int N = listOfMeas->GetEntries(); 
	for(int i=0;i<N;i++){
		NucDBMeasurement * Meas = (NucDBMeasurement*)listOfMeas->At(i);
		TGraphErrors * graph = Meas->BuildGraph("x");
		graph->SetMarkerStyle(20+i);
		graph->SetMarkerSize(0.9);
		graph->SetMarkerColor(kBlack);
		graph->SetLineColor(kBlack);
		graph->SetLineWidth(1);
		G->Add(graph);
		leg->AddEntry(graph,Form("%s %s",Meas->GetExperimentName(),Meas->GetTitle() ),LegOption);
	}

	// Choose the models

	// CTEQ  
	InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
	cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());

	TF1 * xF2pCTEQ = new TF1("xF2pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF2p, 
			0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
	xF2pCTEQ->SetParameter(0,4.5);
	leg->AddEntry(xF2pCTEQ,"F2p CTEQ","l");
	xF2pCTEQ->SetLineColor(6);

	// F1F209 
	InSANEStructureFunctions *f1f2SFs = new F1F209StructureFunctions();
	TF1 * xF2pA = new TF1("xF2pA", f1f2SFs, &InSANEStructureFunctions::EvaluateF2p, 
			0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
	xF2pA->SetParameter(0,4.5);
	xF2pA->SetLineColor(kBlue);
	leg->AddEntry(xF2pA,"F2p F1F209","l");

	// NMC95
	InSANEStructureFunctions *NMCSFs = new NMC95StructureFunctions();
	TF1 * xF2pB = new TF1("xF2pA", NMCSFs, &InSANEStructureFunctions::EvaluateF2p, 
			0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
	xF2pB->SetParameter(0,4.5);
	xF2pB->SetLineColor(kGreen);
	leg->AddEntry(xF2pB,"F2p NMC95","l");

        // Draw the plot 	
	TString DrawOption = Form("AP");
	TString Title      = Form("%s(x,Q^{2} = %.0f GeV^{2})",Measurement.Data(),Qsq);
	TString xAxisTitle = Form("x");
	TString yAxisTitle = Form("%s",Measurement.Data());

	G->Draw(DrawOption);
	G->SetTitle(Title);
	G->GetXaxis()->SetTitle(xAxisTitle);
	G->GetXaxis()->CenterTitle();
	G->GetYaxis()->SetTitle(yAxisTitle);
	G->GetYaxis()->CenterTitle();
	G->Draw(DrawOption);

	xF2pCTEQ->Draw("same");
	xF2pA->Draw("same");
	xF2pB->Draw("same");

	leg->Draw();

	TString prefix = Form("./plots/structure_functions/");
	TString name   = Form("F2p"); 
	TString fn1    = prefix + name + Form(".png"); 
	TString fn2    = prefix + name + Form(".pdf"); 

	// c->SaveAs(fn1);
	// c->SaveAs(fn2);

	return(0);
}
