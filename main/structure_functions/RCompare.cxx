Int_t RCompare() {

   //gSystem->AddIncludePath("-I/usr/local/include/LHAPDF");
   //gSystem->Load("libLHAPDF.so");

   Double_t Q2 = 4.0;

   TCanvas * c = new TCanvas("F2p","F2p");
   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("R = #sigma_{L}/#sigma_{T}");

   NucDBBinnedVariable * Qsqbin = new NucDBBinnedVariable("Qsquared","Q^{2}");
   Qsqbin->SetBinMinimum(3.0);
   Qsqbin->SetBinMaximum(5.0);
   Qsqbin->SetMean(Q2);
   Qsqbin->SetAverage(Q2);

   // ----------------------------------------------------------------
   NucDBManager * manager = NucDBManager::GetManager();
   //TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);
   TList * measurementsList =  manager->GetMeasurements("R");
   TMultiGraph * MG = new TMultiGraph();
   Int_t markers[] = {22,32,23,33,21,35,36,37};

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *aMeas = (NucDBMeasurement*)measurementsList->At(i);
      //TList * plist = aMeas->FilterWithBin(Qsqbin);
      //if(!plist) continue;
      //if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = aMeas->BuildGraph("x");
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerSize(1.3);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      // Set legend title 
      //Title = "";//Form("%s",aMeas->GetExperimentName()); 
      //leg->AddEntry(graph,Title,"p");
      // Add to TMultiGraph 
      MG->Add(graph,"p"); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
      NucDBMeasurement *bMeas = aMeas->CreateMeasurementFilteredWithBin(Qsqbin);
      graph        = bMeas->BuildGraph("x");
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerSize(1.3);
      graph->SetMarkerColor(2);
      graph->SetLineColor(2);
      MG->Add(graph,"p"); 

   }

   //MG->Draw("a");
   //MG->GetXaxis()->SetLimits(0.0,1.0);


   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   TF1 * xF2pCTEQ = new TF1("xF2pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateR, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateR");
   leg->AddEntry(xF2pCTEQ,"CTEQ","l");
   xF2pCTEQ->SetParameter(0,Q2);
   xF2pCTEQ->SetLineColor(6);


//    InSANEStructureFunctionsFromPDFs * lhaSFs = new InSANEStructureFunctionsFromPDFs();
//    lhaSFs->SetUnpolarizedPDFs(new LHAPDFUnpolarizedPDFs());

   InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();
   TF1 * xF2pA = new TF1("xF1pA", f1f2SFs, &InSANEStructureFunctions::EvaluateR, 
               0.2, 1, 1,"InSANEStructureFunctions","EvaluateR");
   leg->AddEntry(xF2pA,"F1F209","l");
   xF2pA->SetParameter(0,Q2);
   xF2pA->SetLineColor(2);


   InSANEStructureFunctionsFromPDFs * myBBS = new InSANEStructureFunctionsFromPDFs();
   myBBS->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());
   myBBS->CalculateTMCs(true);
   //myBBS->SetUseR(true);
   TF1 * xF2pB = new TF1("xF1pB", myBBS, &InSANEStructureFunctions::EvaluateR, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateR");
   xF2pB->SetLineColor(kGreen);
   xF2pB->SetLineWidth(3);
   xF2pB->SetParameter(0,Q2);
   leg->AddEntry(xF2pB,"Statistical","l");

   NMC95StructureFunctions * NMCSFs = new NMC95StructureFunctions();
   TF1 * xF2pNMC = new TF1("xF1pNMC", NMCSFs, &InSANEStructureFunctions::EvaluateR, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateR");
   leg->AddEntry(xF2pNMC,"NMC95","l");
   xF2pNMC->SetLineColor(4);
   xF2pNMC->SetParameter(0,Q2);

   // ----------------------
   LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   TF1 * xF2pC = new TF1("xF2pC", sf, &InSANEStructureFunctions::EvaluateR, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateR");
   xF2pC->SetParameter(0,Q2);
   xF2pC->SetLineColor(1);
   xF2pC->SetLineWidth(4);
   //leg->AddEntry(xF2pC,Form("Composite SFs:%s",sf->fLabel.Data()),"l");

   TGraph * gr = 0;
   TMultiGraph * mg = new TMultiGraph();
   mg->Add(MG);
   gr = new TGraph(xF2pC->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(xF2pA->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(xF2pB->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(xF2pCTEQ->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(xF2pNMC->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   mg->Draw("a");
   //xF2pA->Draw("same");
   //xF2pB->Draw("same");
   //xF2pNMC->DrawCopy("same");

   TLatex * latex = new TLatex();
   latex->SetNDC();
   //latex->SetTextAlign(21);
   latex->DrawLatex(0.15,0.18,Form("Q^{2} = %.1f (GeV/c)^{2}",Q2));
/*   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("MSTW2008nlo68cl");*/
/*   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("cteq4m");*/
/*
   InSANEStructureFunctions * lhaSFs = new LHAPDFStructureFunctions();

   TF1 * xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");
   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFType("cteq6m",1);
   xF2p->SetParameter(0,10.5);
   xF2p->SetLineColor(2);
   leg->AddEntry(xF2p,Form("F2p LHAPDF:%s at Q^2=10.5",lhaSFs->fLabel.Data()),"l");
   xF2p->DrawClone("same");

   xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF1p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF1p");

   ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFType("cteq6m",1);
   xF2p->SetParameter(0,4.5);
   xF2p->SetLineColor(3);
   leg->AddEntry(xF2p,Form("F2p LHAPDF:%s at Q^2=4.5",lhaSFs->fLabel.Data()),"l");
   xF2p->DrawClone("same");
*/

   /// Composite Structure FUnctions
   //LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   //TF1 * xF2pC = new TF1("xF2pC", sf, &InSANEStructureFunctions::EvaluateF2p, 
   //            0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   ////((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   //xF2pC->SetParameter(0,4.5);
   //xF2pC->SetLineColor(6);
   ////leg->AddEntry(xF2p,Form("Composite SFs:%s at Q^2=10.5",sf->fLabel.Data()),"l");
   //xF2pC->DrawClone("same");


//    xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF2p, 
//                0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
// 
//     ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("cteq6mE", LHAPDF::LHGRID );
//    xF2p->SetParameter(0,100.5);
//    xF2p->SetLineColor(7);
//    leg->AddEntry(xF2p,Form("F2p LHAPDF:%s",lhaSFs->fLabel.Data()),"l");
//    xF2p->DrawClone("same");



   leg->Draw();
//    lhaSFs->PlotSFs(4.5);


   c->SaveAs("results/structure_functions/RCompare.pdf");
   c->SaveAs("results/structure_functions/RCompare.png");

return(0);
}
