/** Plots models and data for g1n at Qsq */
Int_t g1n(Double_t Qsq = 5.0,Double_t QsqBinWidth=1.0){
   TCanvas * c = new TCanvas("g1n","g1n");
   /// Create Legend
   TLegend * leg = new TLegend(0.6,0.125,0.875,0.45);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   /// First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsD = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsD->SetPolarizedPDFs( new DSSVPolarizedPDFs );

   InSANEPolarizedStructureFunctionsFromVCSAs * pSFsF = new InSANEPolarizedStructureFunctionsFromVCSAs();
   pSFsF->SetVCSAs( new MAIDVirtualComptonAsymmetries());

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * g1n = new TF1("g1n", pSFs, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
   TF1 * g1nA = new TF1("g1nA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
   TF1 * g1nB = new TF1("g1nB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
   TF1 * g1nC = new TF1("g1nC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
   TF1 * g1nD = new TF1("g1nD", pSFsD, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");
   TF1 * g1nF = new TF1("g1nF", pSFsF, &InSANEPolarizedStructureFunctions::Evaluateg1n, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluateg1n");

   g1n->SetParameter(0,Qsq);
   g1nA->SetParameter(0,Qsq);
   g1nB->SetParameter(0,Qsq);
   g1nC->SetParameter(0,Qsq);
   g1nD->SetParameter(0,Qsq);
   g1nF->SetParameter(0,Qsq);

   g1n->SetLineColor(1);
   g1nA->SetLineColor(3);
   g1nB->SetLineColor(kOrange -1);
   g1nC->SetLineColor(kViolet-2);
   g1nD->SetLineColor(kRed-2);
   g1nF->SetLineColor(kGreen+1);

   Int_t width = 3;
   g1n->SetLineWidth(width);
   g1nA->SetLineWidth(width);
   g1nB->SetLineWidth(width);
   g1nC->SetLineWidth(width);
   g1nD->SetLineWidth(width);
   g1nF->SetLineWidth(width);

   g1n->SetLineStyle(1);
   g1nA->SetLineStyle(1);
   g1nB->SetLineStyle(1);
   g1nC->SetLineStyle(1);
   g1nD->SetLineStyle(1);
   g1nF->SetLineWidth(1);

   g1nA->SetMinimum(-0.065);
   g1nA->SetMaximum(0.05);

   TString Measurement = Form("g_{1}^{n}");
   TString Title      = Form("%s(x,Q^{2} = %.0f GeV^{2})",Measurement.Data(),Qsq);
   TString xAxisTitle = Form("x");
   TString yAxisTitle = Form("%s",Measurement.Data());

   //Data->Draw("AP");
   //Data->SetTitle(Title);
   //Data->GetXaxis()->SetTitle(xAxisTitle);
   //Data->GetXaxis()->CenterTitle();
   //Data->GetYaxis()->SetTitle(yAxisTitle);
   //Data->GetYaxis()->CenterTitle();
   //Data->Draw("AP");

   // draw the models 
   // g1n->Draw("same");
   g1nA->Draw();
   g1nB->Draw("same");
   // g1nC->Draw("same");
   g1nD->Draw("same");
   g1nF->Draw("same");

   /// Complete the Legend 
   leg->SetFillColor(kWhite); 
   //leg->AddEntry(g1nQsq1->fGraph,Form("%s %s","All g1n data",g1nQsq1->GetTitle() ),"ep");
   // leg->AddEntry(g1n,"g1n BB ","l");
   // leg->AddEntry(g1nA,"g1n DNS2005 ","l");
   leg->AddEntry(g1nB,"g1n AAC ","l");
   // leg->AddEntry(g1nC,"g1n GS ","l");
   leg->AddEntry(g1nD,"g1n DSSV ","l");
   leg->AddEntry(g1nF,"MAID07 ","l");
   leg->Draw();

   c->SaveAs("results/structure_functions/g1n.png");
   c->SaveAs("results/structure_functions/g1n.pdf");

   return(0);
}
