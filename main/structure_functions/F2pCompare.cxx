/**  
 */
Int_t F2pCompare(Double_t Q2 = 4.0) {

   //gSystem->AddIncludePath("-I/usr/local/include/LHAPDF");
   //gSystem->Load("libLHAPDF.so");

   
   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Qsq = new NucDBBinnedVariable("Qsquared",Form("Q^{2} %.1f<Q^{2}<%.1f",Q2-0.5,Q2+0.5));
   Qsq->SetBinMinimum(Q2-0.5);
   Qsq->SetBinMaximum(Q2+0.5);
   NucDBBinnedVariable * Qsq2 = new NucDBBinnedVariable("Qsquared","Q^{2} 5.0<Q^{2}<8.0");
   Qsq2->SetBinMinimum(5.0);
   Qsq2->SetBinMaximum(8.0);
   
   NucDBMeasurement * F2p = manager->GetExperiment("OLDSLAC")->GetMeasurement("F2p");
   NucDBMeasurement * F2pQsq1 = new NucDBMeasurement("F2pQsq1",Form("F_{2}^{p} %s",Qsq->GetTitle()));
   NucDBMeasurement * F2pQsq2 = new NucDBMeasurement("F2pQsq2",Form("F_{2}^{p} %s",Qsq2->GetTitle()));
   
   TLegend * leg = new TLegend(0.6,0.7,0.89,0.89);
   leg->SetFillColor(0);
   leg->SetBorderSize(0);
   //leg->SetHeader("F2p");

   //TMultiGraph * mg = manager->GetMultiGraph("F2p","x");
   TMultiGraph * mg = new TMultiGraph();
   //leg->AddEntry(F2p->fGraph,Form("%s %s",F2p->GetExperimentName(),F2p->GetTitle() ),"ep");

   F2pQsq1->AddDataPoints(F2p->FilterWithBin(Qsq));
   TGraphErrors * graph1 = F2pQsq1->BuildGraph("x");
   graph1->SetMarkerColor(2);
   graph1->SetLineColor(2);
   leg->AddEntry(graph1,Form("%s %s",F2p->GetExperimentName(),F2pQsq1->GetTitle() ),"ep");

   F2pQsq2->AddDataPoints(F2p->FilterWithBin(Qsq2));
   TGraphErrors * graph2 = F2pQsq2->BuildGraph("x");
   graph2->SetMarkerColor(4);
   graph2->SetLineColor(4);
   //graph2->Draw("p");
   leg->AddEntry(graph2,Form("%s %s",F2p->GetExperimentName(),F2pQsq2->GetTitle() ),"ep");

   mg->Add(graph2,"p");
   mg->Add(graph1,"p");

   // -----------------
   // CTEQ
   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   TF1 * xF2pCTEQ = new TF1("xF2pCTEQ", cteqSFs, &InSANEStructureFunctions::EvaluateF2p, 
                                        0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(xF2pCTEQ,"F2p CTEQ","l");
   xF2pCTEQ->SetParameter(0,Q2);
   xF2pCTEQ->SetLineColor(6);

   // -----------------
   // F1F209
   InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();
   TF1 * xF2pA = new TF1("xF2pA", f1f2SFs, &InSANEStructureFunctions::EvaluateF2p, 
                                  0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(xF2pA,"F2p F1F209","l");
   xF2pA->SetParameter(0,Q2);
   xF2pA->SetLineColor(2);

   // -----------------
   // Statistical 
   InSANEStructureFunctionsFromPDFs * myBBS = new InSANEStructureFunctionsFromPDFs();
   myBBS->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());
   TF1 * xF2pB = new TF1("xF2pB", myBBS, &InSANEStructureFunctions::EvaluateF2p, 
                                0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(xF2pB,"F2p BBS","l");
   xF2pB->SetParameter(0,Q2);
   xF2pB->SetLineColor(kGreen);

   // -----------------
   // NMC 
   NMC95StructureFunctions * NMCSFs = new NMC95StructureFunctions();
   TF1 * xF2pNMC = new TF1("xF2pNMC", NMCSFs, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   leg->AddEntry(xF2pNMC,"F2p NMC95","l");
   xF2pNMC->SetLineColor(4);
   xF2pNMC->SetParameter(0,Q2);

   // ----------------
   // Composite Structure FUnctions
   LowQ2StructureFunctions * sf = new LowQ2StructureFunctions();
   TF1 * xF2pC = new TF1("xF2pC", sf, &InSANEStructureFunctions::EvaluateF2p, 
               0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   //((LowQ2StructureFunctions*)sf)->SetPDFType("cteq6m",1);
   xF2pC->SetParameter(0,Q2);
   xF2pC->SetLineColor(1);
   xF2pC->SetLineWidth(4);
   leg->AddEntry(xF2pC,Form("Composite SFs:%s",sf->fLabel.Data()),"l");
   // -----------------------

   TCanvas * c = new TCanvas("F2p","F2p");

   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetRangeUser(0.0,0.45);
   mg->Draw("a");
   xF2pC->Draw("same");
   xF2pCTEQ->Draw("same");
   xF2pA->Draw("same");
   xF2pB->Draw("same");
   xF2pNMC->DrawCopy("same");

   //xF2pC->DrawClone("same");

   //xF2p = new TF1("xF2p", lhaSFs, &InSANEStructureFunctions::EvaluateF2p, 
   //            0, 1, 1,"InSANEStructureFunctions","EvaluateF2p");
   //
   // ((LHAPDFStructureFunctions*)lhaSFs)->SetPDFDataSet("cteq6mE", LHAPDF::LHGRID );
   //xF2p->SetParameter(0,100.5);
   //xF2p->SetLineColor(7);
   //leg->AddEntry(xF2p,Form("F2p LHAPDF:%s",lhaSFs->fLabel.Data()),"l");
   //xF2p->DrawClone("same");

   TLatex latex;
   latex.DrawLatex(0.6,0.2,Form("#font[22]{Q^{2}=%.1f (GeV/c)^{2}}",Q2));

   leg->Draw();


   c->SaveAs("results/structure_functions/F2pCompare.png");
   c->SaveAs("results/structure_functions/F2pCompare.pdf");

   return(0);
}
