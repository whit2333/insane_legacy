Int_t g1p_F1p_A1p(Int_t aNumber = 15, Bool_t logx = false){

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   fman->Print();


   InSANEStructureFunctions          * SF_default  = fman->CreateSFs(1);//fman->GetStructureFunctions();
   InSANEPolarizedStructureFunctions * pSF_default = fman->CreatePolSFs(0);
   //InSANEPolarizedStructureFunctions * pSFsG = new LowQ2PolarizedStructureFunctions();
   InSANEVirtualComptonAsymmetries   * VCSA_default = new InSANEVirtualComptonAsymmetries();
   VCSA_default->SetUnpolarizedSFs(SF_default);
   VCSA_default->SetPolarizedSFs(pSF_default);

   // ---------------------------------------
   //
   TLegend * leg = new TLegend(0.6,0.5,0.875,0.875);
   //leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   // big legend in pad 4
   TLegend * leg4 = new TLegend(0.1,0.1,0.6,0.7);
   leg4->SetFillStyle(0);
   leg4->SetBorderSize(0);

   // -------------------
   // Functions
   Int_t    npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 0.90;

   std::vector<double> Q2values;
   Q2values.push_back(1.0);
   Q2values.push_back(1.5);
   Q2values.push_back(2.0);
   Q2values.push_back(3.0);
   Q2values.push_back(4.0);
   Q2values.push_back(10.0);
   Q2values.push_back(20.0);
   Q2values.push_back(200.0);

   TMultiGraph * mg_1 = new TMultiGraph();
   TMultiGraph * mg_2 = new TMultiGraph();
   TMultiGraph * mg_3 = new TMultiGraph();

   // --------------------
   // g1p
   TF1 * xg1p = new TF1("g1p", pSF_default, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
                    xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   xg1p->SetLineColor(kBlack);
   xg1p->SetLineWidth(2);
   xg1p->SetLineStyle(1);

   // --------------------
   // F1p
   TF1 * xF1p = new TF1("F1p", SF_default, &InSANEStructureFunctions::EvaluatexF1p, 
                    xmin, xmax, npar,"InSANEStructureFunctions","EvaluatexF1p");
   xF1p->SetLineColor(kBlack);
   xF1p->SetLineWidth(2);
   xF1p->SetLineStyle(1);

   // --------------------
   // A1p
   TF1 * A1p = new TF1("A1p",VCSA_default,&InSANE_VCSABase::EvaluateA1p,
         xmin,xmax,npar,"InSANE_VCSABase","EvaluateA1p");
   A1p->SetLineColor(1);
   A1p->SetLineWidth(2);

   // -------------------------------
   // Make graphas
   for(int i= 0; i< Q2values.size(); i++) {

      Double_t Qsq = Q2values[i];
      xF1p->SetParameter(0,Qsq);
      xg1p->SetParameter(0,Qsq);
      A1p->SetParameter(0,Qsq);
      TGraph * gr = 0;

      gr = new TGraph( xF1p->DrawCopy("goff")->GetHistogram() );
      gr->SetLineColor(gNiceColors[i]);
      mg_1->Add(gr,"l");

      gr = new TGraph( xg1p->DrawCopy("goff")->GetHistogram() );
      gr->SetLineColor(gNiceColors[i]);
      mg_2->Add(gr,"l");

      gr = new TGraph( A1p->DrawCopy("goff")->GetHistogram() );
      gr->SetLineColor(gNiceColors[i]);
      mg_3->Add(gr,"l");

      leg4->AddEntry(gr,Form("#LTQ^{2}#GT = %.1f GeV^{2}",Qsq),"l");
   }


   // -------------------------------------------------------------
   // 
   TCanvas * c = new TCanvas("g1p","g1p");
   c->Divide(2,2);

   TLatex * t = new TLatex();
   t->SetNDC();
   //t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 

   // ---------------------
   // F1p
   c->cd(1);
   gPad->SetLogx(logx);
   mg_1->Draw("a");
   mg_1->SetTitle("xF_{1}^{p}");
   mg_1->GetYaxis()->SetRangeUser(0.0,0.2);
   mg_1->GetXaxis()->SetLimits(xmin,1.0);
   mg_1->GetXaxis()->SetTitle("x");
   mg_1->GetXaxis()->CenterTitle(true);
   leg->AddEntry(xg1p,"default ","l");
   //leg->Draw();
   //t->DrawLatex(0.25,0.85,Form("g_{1}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));

   // ---------------------
   // g1p
   c->cd(2);
   gPad->SetLogx(logx);
   mg_2->Draw("a");
   mg_2->SetTitle("xg_{1}^{p}");
   mg_2->GetYaxis()->SetRangeUser(-0.05,0.11);
   mg_2->GetXaxis()->SetLimits(xmin,1.0);
   mg_2->GetXaxis()->SetTitle("x");
   mg_2->GetXaxis()->CenterTitle(true);

   // ---------------------
   // A1p
   c->cd(3);
   gPad->SetLogx(logx);
   mg_3->Draw("a");
   mg_3->SetTitle("A_{1}^{p}");
   mg_3->GetYaxis()->SetRangeUser(-0.5,1.0);
   mg_3->GetXaxis()->SetLimits(xmin,1.0);
   mg_3->GetXaxis()->SetTitle("x");
   mg_3->GetXaxis()->CenterTitle(true);

   c->cd(4);
   leg4->Draw();

   t->DrawLatex(0.2,0.85,pSF_default->GetTitle());
   t->DrawLatex(0.2,0.9,SF_default->GetTitle());

   c->SaveAs(Form("results/structure_functions/g1p_F1p_A1p_%d.png",aNumber));
   c->SaveAs(Form("results/structure_functions/g1p_F1p_A1p_%d.pdf",aNumber));

   return(0);
}
