void A2n(Double_t Q2 = 4.0){

	Double_t x ;

        // Use DSSV as the polarized PDF model 
	DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs(); 

        // Use NMC95 as the unpolarized SF model 
        NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
        F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

        // Asymmetry class: Use NMC
        InSANEAsymmetriesFromStructureFunctions *AsymSF1 = new InSANEAsymmetriesFromStructureFunctions(); 
	AsymSF1->SetPolarizedPDFs(DSSV);
	AsymSF1->SetUnpolarizedSFs(NMC95);  
        // Asymmetry class: Use F1F209 
        InSANEAsymmetriesFromStructureFunctions *AsymSF2 = new InSANEAsymmetriesFromStructureFunctions(); 
	AsymSF2->SetPolarizedPDFs(DSSV);
	AsymSF2->SetUnpolarizedSFs(F1F209);  

	Int_t npar = 1;
	Double_t xmin = 0.01;
	Double_t xmax = 1.00;
	Double_t xmin1 = 0.01;
	Double_t xmax1 = 1.00;
	Double_t xmin2 = 0.2;
	Double_t xmax2 = 1.00;
	Double_t ymin = -0.3;
	Double_t ymax =  0.3; 

        // Plot A2n(x,Q2=const) 
        // WARNING! Plot is for CONSTANT Q2! 
        // we set Q2 for the evaluation of the function: &InSANEPolarizedStructureFunctions::Evaluateg1n
	TF1 * Model1 = new TF1("Model1",AsymSF1,&InSANEAsymmetryBase::EvaluateA2n,
			xmin1, xmax1, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA2n");

	TF1 * Model2 = new TF1("Model2",AsymSF2,&InSANEAsymmetryBase::EvaluateA2n,
			xmin2, xmax2, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA2n");


	Model1->SetParameter(0,Q2);
	Model1->SetLineColor(kMagenta);

	Model2->SetParameter(0,Q2);
	Model2->SetLineColor(kCyan+2);

	TString Title;

	Int_t width = 3;
	Model1->SetLineWidth(width);
	Model1->SetLineStyle(1);

	Model2->SetLineWidth(width);
	Model2->SetLineStyle(1);

	Model1->SetTitle(Title);
	Model1->GetYaxis()->SetRangeUser(ymin,ymax); 

	Model2->SetTitle(Title);
	Model2->GetYaxis()->SetRangeUser(ymin,ymax); 

        // Load in world data on A2n from NucDB  
	gSystem->Load("libNucDB");
	NucDBManager * manager = NucDBManager::GetManager();

	TLegend *leg = new TLegend(0.1, 0.7, 0.48, 0.9);
        leg->SetFillColor(kWhite); 

        TMultiGraph *MG = new TMultiGraph(); 

	TList * measurementsList = manager->GetMeasurements("A2n");
	for (int i = 0; i < measurementsList->GetEntries(); i++) {
		NucDBMeasurement *A2nWorld = (NucDBMeasurement*)measurementsList->At(i);
                // Get TGraphErrors object 
		TGraphErrors *graph        = A2nWorld->BuildGraph("x");
		graph->SetMarkerStyle(20);
                // Set legend title 
		Title = Form("%s",A2nWorld->GetExperimentName()); 
		leg->AddEntry(graph,Title,"p");
                // Add to TMultiGraph 
                MG->Add(graph); 
	}

        leg->AddEntry(Model1,"DSSV and NMC95 model","l");
        leg->AddEntry(Model2,"DSSV and F1F209 model","l");

	TCanvas * c = new TCanvas("c","A2n",1000,800);
	c->SetFillColor(kWhite); 
        c->cd(); 

        TString Measurement = Form("A_{2}^{n}");
	TString GTitle      = Form("%s (x,Q^{2} = %.2f GeV^{2})",Measurement.Data(),Q2);
        TString xAxisTitle  = Form("x");
        TString yAxisTitle  = Form("%s",Measurement.Data());

        // Draw everything 
        MG->Draw("AP");
	MG->SetTitle(GTitle);
	MG->GetXaxis()->SetTitle(xAxisTitle);
	MG->GetXaxis()->CenterTitle();
	MG->GetYaxis()->SetTitle(yAxisTitle);
	MG->GetYaxis()->CenterTitle();
        // MG->GetXaxis()->SetLimits(xmin,xmax); 
        MG->Draw("AP");
	Model1->Draw("same");
	Model2->Draw("same");
	leg->Draw();

}
