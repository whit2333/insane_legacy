Int_t HT_models() {


   //TCanvas * c = new TCanvas("TMCs","TMCs");
   //c->Divide(1,2);

   Double_t Q20 = 1.0;

   InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());

   InSANEPolarizedStructureFunctionsFromPDFs * bbSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   //bbSFs->SetPolarizedPDFs(new StatisticalPolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new BBPolarizedPDFs());
   bbSFs->SetPolarizedPDFs(new DSSVPolarizedPDFs());

   //InSANEPolarizedStructureFunctionsFromPDFs * jamSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   WPolarizedEvolution<JAMPolarizedPDFs> * jamPDFs = new WPolarizedEvolution<JAMPolarizedPDFs>();
   InSANEPolarizedStructureFunctionsFromPDFs * jamSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   jamSFs->SetPolarizedPDFs(jamPDFs);
   jamPDFs->MakeGrid();
   jamPDFs->EvolveInit(Q20);
   jamPDFs->Evolve();

   TLegend * leg = new TLegend(0.7,0.65,0.9,0.9);
   // Functions
   // F1p w/ TMCs
   TF1 * xF1p_0 = new TF1("xF1p0", cteqSFs, &InSANEStructureFunctions::EvaluatexF1p, 
                                   0, 1, 1,"InSANEStructureFunctions","EvaluatexF1p");
   xF1p_0->SetParameter(0,5.0);
   xF1p_0->SetLineColor(1);

   // w/out TMCs
   TF1 * xF1p_1 = new TF1("xF1p1", cteqSFs, &InSANEStructureFunctionsFromPDFs::EvaluatexF1pNoTMC, 
               0, 1, 1,"InSANEStructureFunctionsFromPDFs","EvaluatexF1pNoTMC");
   xF1p_1->SetParameter(0,5.0);
   xF1p_1->SetLineColor(1);

   // g1p 
   TF1 * xg1p_0 = new TF1("xg1p0", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
                                 0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   xg1p_0->SetParameter(0,5.0);
   xg1p_0->SetLineColor(1);

   TF1 * xg1p_1 = new TF1("xg1p1", jamSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");
   xg1p_1->SetParameter(0,1.0);
   xg1p_1->SetLineColor(1);

   TF1 * xg1p_2 = new TF1("xg1p2", jamSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2pbar, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2pbar");
   xg1p_2->SetParameter(0,1.0);
   xg1p_2->SetLineColor(1);

   TF1 * xg1p_3 = new TF1("xg1p3", jamSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   xg1p_3->SetParameter(0,1.0);
   xg1p_3->SetLineColor(1);

   TF1 * xg1p_4 = new TF1("xg1p4", jamSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2pbar, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2pbar");
   xg1p_4->SetParameter(0,1.0);
   xg1p_4->SetLineColor(1);

   // -------------------------------
   // xF1
   TCanvas * c = new TCanvas("TMCs","TMCs");
   TMultiGraph * mg_F1  = new TMultiGraph();
   TGraph      * gr     = 0;
   
   // Q2 = 5.0
   xF1p_0->SetParameter(0,5.0);
   gr = new TGraph(xF1p_0->DrawCopy("goff")->GetHistogram());
   mg_F1->Add(gr,"l");

   xF1p_1->SetParameter(0,5.0);
   xF1p_1->SetLineStyle(2);
   gr = new TGraph(xF1p_1->DrawCopy("goff")->GetHistogram());
   mg_F1->Add(gr,"l");

   // Q2 = 2.0
   xF1p_0->SetParameter(0,2.0);
   xF1p_0->SetLineColor(2);
   gr = new TGraph(xF1p_0->DrawCopy("goff")->GetHistogram());
   mg_F1->Add(gr,"l");

   xF1p_1->SetParameter(0,2.0);
   xF1p_1->SetLineColor(2);
   xF1p_1->SetLineStyle(2);
   gr = new TGraph(xF1p_1->DrawCopy("goff")->GetHistogram());
   mg_F1->Add(gr,"l");

   // Q2 = 1.0
   xF1p_0->SetParameter(0,1.0);
   xF1p_0->SetLineColor(4);
   gr = new TGraph(xF1p_0->DrawCopy("goff")->GetHistogram());
   mg_F1->Add(gr,"l");

   xF1p_1->SetParameter(0,1.0);
   xF1p_1->SetLineStyle(2);
   xF1p_1->SetLineColor(4);
   gr = new TGraph(xF1p_1->DrawCopy("goff")->GetHistogram());
   mg_F1->Add(gr,"l");

   mg_F1->Draw("A");

   std::cout << "test" << std::endl;

   // -------------------------------
   // xg1
   TCanvas * c2 = new TCanvas("TMCsg1","TMCs g1");
   TMultiGraph * mg_g1  = new TMultiGraph();
   TGraph      * gr     = 0;
   
   // Reference

   // Q2 = 3.0
   xg1p_0->SetLineColor(2);
   xg1p_0->SetParameter(0,3.0);
   gr = new TGraph(xg1p_0->DrawCopy("goff")->GetHistogram());
   mg_g1->Add(gr,"l");
   leg->AddEntry(gr,"Q2=3, g2(REF)","l"); 

   // JAM evolved
   // Q2 = 5.0
   xg1p_1->SetParameter(0,3.0);
   xg1p_1->SetLineStyle(1);
   xg1p_1->SetLineColor(1);
   gr = new TGraph(xg1p_1->DrawCopy("goff")->GetHistogram());
   leg->AddEntry(gr,"Q2=3, g2(WW)","l"); 
   mg_g1->Add(gr,"l");

   xg1p_2->SetParameter(0,3.0);
   xg1p_2->SetLineStyle(2);
   xg1p_2->SetLineColor(1);
   gr = new TGraph(xg1p_2->DrawCopy("goff")->GetHistogram());
   mg_g1->Add(gr,"l");
   leg->AddEntry(gr,"Q2=3, g2(HT)","l"); 

   xg1p_3->SetParameter(0,3.0);
   xg1p_3->SetLineStyle(3);
   xg1p_3->SetLineColor(1);
   gr = new TGraph(xg1p_3->DrawCopy("goff")->GetHistogram());
   mg_g1->Add(gr,"l");
   leg->AddEntry(gr,"Q2=3, g2(LT+HT)","l"); 

   //xg1p_4->SetParameter(0,3.0);
   //xg1p_4->SetLineStyle(4);
   //xg1p_4->SetLineColor(1);
   //gr = new TGraph(xg1p_4->DrawCopy("goff")->GetHistogram());
   //mg_g1->Add(gr,"l");
   //leg->AddEntry(gr,"Q2 = 3 g1 +twist4","l"); 

   // Q2 = 1.0
   xg1p_0->SetParameter(0,1.0);
   xg1p_0->SetLineColor(3);
   gr = new TGraph(xg1p_0->DrawCopy("goff")->GetHistogram());
   mg_g1->Add(gr,"l");
   leg->AddEntry(gr,"Q2=1, g2(REF)","l"); 

   xg1p_1->SetParameter(0,1.0);
   xg1p_1->SetLineStyle(1);
   xg1p_1->SetLineColor(4);
   gr = new TGraph(xg1p_1->DrawCopy("goff")->GetHistogram());
   leg->AddEntry(gr,"Q2=1, g2(WW)","l"); 
   mg_g1->Add(gr,"l");

   xg1p_2->SetParameter(0,1.0);
   xg1p_2->SetLineStyle(2);
   xg1p_2->SetLineColor(4);
   gr = new TGraph(xg1p_2->DrawCopy("goff")->GetHistogram());
   mg_g1->Add(gr,"l");
   leg->AddEntry(gr,"Q2=1, g2(HT)","l"); 

   xg1p_3->SetParameter(0,1.0);
   xg1p_3->SetLineStyle(3);
   xg1p_3->SetLineColor(4);
   gr = new TGraph(xg1p_3->DrawCopy("goff")->GetHistogram());
   mg_g1->Add(gr,"l");
   leg->AddEntry(gr,"Q2=1, g2(LT+HT)","l"); 

   //xg1p_4->SetParameter(0,1.0);
   //xg1p_4->SetLineStyle(4);
   //xg1p_4->SetLineColor(4);
   //gr = new TGraph(xg1p_4->DrawCopy("goff")->GetHistogram());
   //mg_g1->Add(gr,"l");
   //leg->AddEntry(gr,"Q2 = 1 g1 +twist4","l"); 


   mg_g1->Draw("A");
   leg->Draw();

   c->SaveAs( "results/structure_functions/HT_models_F1.png");
   c->SaveAs( "results/structure_functions/HT_models_F1.pdf");
   c2->SaveAs("results/structure_functions/HT_models.png");
   c2->SaveAs("results/structure_functions/HT_models.pdf");

   return 0;
}
