#include <tuple>
//#include "SSFsFromVPACs.h"
//#include "SSFsFromPDFs.h"

void target_mass_corrections(const char * label = "JAM") {

  using namespace insane::physics;

  //insane::physics::SSFsFromPDFs<JAM_PPDFs> * sf = new insane::physics::SSFsFromPDFs<JAM_PPDFs>();
  //JAM_PPDFs ppdfs;
  JAM_T3DFs * t3dfs = new JAM_T3DFs();
  //std::tuple<JAM_PPDFs, JAM_T3DFs, JAM_T4DFs> pdf_tuple;
  //std::tuple<JAM_PPDFs> pdf_tuple;
  //std::tuple<Stat2015_PPDFs> pdf_tuple2;

  //auto ssf = new SSFsFromVPACs<MAID_VPACs>();
  //auto ssf = new SSFsFromPDFs<Stat2015_PPDFs>();
  auto ssf = new SSFsFromPDFs<JAM_PPDFs, JAM_T3DFs>();
  //auto ssf = new CompositeSSFs();

  double Q2   = 1.0;
  auto   g1p_noTMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) { return x[0]*ssf->g1(x[0],Q2,Nuclei::p); });
  auto   g1p_withTMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) { return x[0]*ssf->g1_TMC(x[0],Q2,Nuclei::p); });
  TF1 *  f_g1p_noTMC     = new TF1("g1p_noTMC  ", g1p_noTMC  ,0.0,1.0,0);
  TF1 *  f_g1p_withTMC   = new TF1("g1p_withTMC",g1p_withTMC,0.0,1.0,0);

  auto   g2p_noTMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) { return x[0]*ssf->g2(x[0],Q2,Nuclei::p); });
  auto   g2p_withTMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) { return x[0]*ssf->g2_TMC(x[0],Q2,Nuclei::p); });
  TF1 *  f_g2p_noTMC     = new TF1("g2p_noTMC  ", g2p_noTMC  ,0.0,1.0,0);
  TF1 *  f_g2p_withTMC   = new TF1("g2p_withTMC",g2p_withTMC,0.0,1.0,0);

  auto   g2p_t3_noTMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) { return x[0]*t3dfs->g2(x[0],Q2,Nuclei::p); });
  auto   g2p_t3_withTMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) { return x[0]*t3dfs->g2_TMC(x[0],Q2,Nuclei::p); });
  TF1 *  f_g2p_t3_noTMC     = new TF1("g2p_t3_noTMC  ", g2p_t3_noTMC  ,0.0,1.0,0);
  TF1 *  f_g2p_t3_withTMC   = new TF1("g2p_t3_withTMC", g2p_t3_withTMC,0.0,1.0,0);

  auto   g1p_t3_noTMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) { return x[0]*t3dfs->g1(x[0],Q2,Nuclei::p); });
  auto   g1p_t3_withTMC  = std::function<double(double*,double*)>(
      [&](double *x, double*) { return x[0]*t3dfs->g1_TMC(x[0],Q2,Nuclei::p); });
  TF1 *  f_g1p_t3_noTMC     = new TF1("g1p_t3_noTMC  ", g1p_t3_noTMC  ,0.0,1.0,0);
  TF1 *  f_g1p_t3_withTMC   = new TF1("g1p_t3_withTMC", g1p_t3_withTMC,0.0,1.0,0);

  // ----------------------------------
  //
  TCanvas     * c   = nullptr;
  TMultiGraph * mg  = nullptr;
  TLegend     * leg = nullptr;
  TGraph      * gr  = nullptr;

  // ----------------------------------
  //
  c = new TCanvas();
  c->Divide(1,2);

  c->cd(1);
  mg = new TMultiGraph();
  leg = new TLegend(0.8,0.8,0.99,0.99);
  mg->Add( gr = new TGraph(f_g1p_noTMC), "l");
  gr->SetLineColor(2);
  leg->AddEntry(gr,"g1 M=0","l");
  mg->Add( gr = new TGraph(f_g1p_withTMC), "l");
  gr->SetLineColor(4);
  leg->AddEntry(gr,"g1 M=M_{p}","l");
  //mg->Draw("a");
  //leg->Draw();
  //c->cd(3);
  //mg = new TMultiGraph();
  //leg = new TLegend(0.8,0.8,0.99,0.99);
  mg->Add( gr = new TGraph(f_g1p_t3_noTMC), "l");
  gr->SetLineColor(2);
  gr->SetLineStyle(2);
  leg->AddEntry(gr,"g1 twist-3 M=0","l");
  mg->Add( gr = new TGraph(f_g1p_t3_withTMC), "l");
  gr->SetLineColor(4);
  gr->SetLineStyle(2);
  leg->AddEntry(gr,"g1 twist-3 M=M_{p}","l");
  mg->Draw("a");
  leg->Draw();

  c->cd(2);
  mg = new TMultiGraph();
  leg = new TLegend(0.8,0.8,0.99,0.99);
  mg->Add( gr = new TGraph(f_g2p_noTMC), "l");
  gr->SetLineColor(2);
  leg->AddEntry(gr,"g2 M=0","l");
  mg->Add( gr = new TGraph(f_g2p_withTMC), "l");
  gr->SetLineColor(4);
  leg->AddEntry(gr,"g2 M=M_{p}","l");
  //mg->Draw("a");
  //leg->Draw();

  //c->cd(4);
  //mg = new TMultiGraph();
  //leg = new TLegend(0.8,0.8,0.99,0.99);
  mg->Add( gr = new TGraph(f_g2p_t3_noTMC), "l");
  gr->SetLineColor(2);
  gr->SetLineStyle(2);
  leg->AddEntry(gr,"g2 twist-3 M=0","l");
  mg->Add( gr = new TGraph(f_g2p_t3_withTMC), "l");
  gr->SetLineColor(4);
  gr->SetLineStyle(2);
  leg->AddEntry(gr,"g2 twist-3 M=M_{p}","l");
  mg->Draw("a");
  leg->Draw();

  c->SaveAs("results/structure_functions/target_mass_corrections.pdf");
  c->SaveAs("results/structure_functions/target_mass_corrections.png");


}
