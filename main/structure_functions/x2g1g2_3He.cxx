//#include "util/stat_error_graph.cxx"
//#include "asym/sane_data_bins.cxx"

//int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

Int_t x2g1g2_3He(int aNumber = 0)
{

   //sane_data_bins();

   // --------------------------------------------------------------------------
   // Functions
   // Function to take g1n --> x^2 g1n
   TF2 * xf = new TF2("xf","2.0*x*x*y",0,1,-1,1);
   TF2 * xf2 = new TF2("xf2","3.0*x*x*y",0,1,-1,1);
   TF2 * f2 = new TF2("f2","2.0*y",0,1,-1,1);
   TF2 * f3 = new TF2("f3","3.0*y",0,1,-1,1);

   TF2 * fTimes2 = new TF2("ftimes2","2.0*y",0,1,-1,1);
   TF2 * fTimes3 = new TF2("ftimes3","3.0*y",0,1,-1,1);
   TF2 * x2f = new TF2("x2f","x*x*y",0,1,-1,1);


   // -------------------------------------------------------
   //
   Double_t Q2 = 4.0;
   Int_t width = 1;

   TLegend * leg = new TLegend(0.16, 0.60, 0.40, 0.88);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);
   TLegend * leg0 = new TLegend(0.16, 0.60, 0.40, 0.88);
   leg0->SetFillColor(0);
   leg0->SetFillStyle(0);
   leg0->SetBorderSize(0);

   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Ebeam47       = new NucDBBinnedVariable("Ebeam","Ebeam",4.7,0.2);
   NucDBBinnedVariable * Ebeam59       = new NucDBBinnedVariable("Ebeam","Ebeam2",5.9,0.2);
   NucDBBinnedVariable * Wbin          = new NucDBBinnedVariable("W","W",20.0,18.0);
   NucDBBinnedVariable * Qsqbin        = new NucDBBinnedVariable("Qsquared","Q^{2}");
   Qsqbin->SetBinMinimum(1.5);
   Qsqbin->SetBinMaximum(18.5);
   Qsqbin->SetMean((1.5+18.5)/2.0);
   Qsqbin->SetAverage((1.5+18.5)/2.0);
   Qsqbin->SetTitle(Form("%.1f<Q^{2}<%.1f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));

   TMultiGraph * MG         = new TMultiGraph();
   TMultiGraph * MG_data1   = new TMultiGraph();
   TMultiGraph * mg_CLAS_g1 = new TMultiGraph();
   TMultiGraph * mg_d2n_g1 = new TMultiGraph();

   TList            * measurementsList = manager->GetMeasurements("g1He3");
   NucDBExperiment  * exp              = 0;
   NucDBMeasurement * ames             = 0;
   measurementsList->Print();

   Int_t markers[] = {21,22,23,32,33,21,25,26,27};

   for (int i = 0; i < measurementsList->GetEntries(); i++) {

      NucDBMeasurement * measg1 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;

      plist = measg1->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg1->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      if(!strcmp(measg1->GetExperimentName(),"SANE") ){
         continue;
      }
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg1->BuildGraph("x");
      graph->Apply(x2f);
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      graph->SetMarkerSize(1.5);
      if(!strcmp(measg1->GetExperimentName(),"CLAS") ){
         mg_CLAS_g1->Add(graph,"p");
         continue;
      }

      if(!strcmp(measg1->GetExperimentName(),"JLAB-E06014") ){
         //plist = measg1->ApplyFilterWithBin(Ebeam47);
         //if(!plist) continue;
         //if(plist->GetEntries() == 0 ) continue;
         TList * meas_by_beam = measg1->CreateMeasurementsWithUniqueBins("Ebeam");
         if(meas_by_beam->GetEntries()){

            for(int imeas= 0; imeas<meas_by_beam->GetEntries();imeas++){
               NucDBMeasurement * m0 = (NucDBMeasurement*)meas_by_beam->At(imeas);

               TGraphErrors * gr_47        = m0->BuildGraph("x");
               int acol = 2+imeas*2;
               gr_47->Apply(x2f);
               gr_47->SetMarkerColor(acol);
               gr_47->SetMarkerStyle(20);
               gr_47->SetLineColor(acol);
               gr_47->SetMarkerSize(1.5);
               mg_d2n_g1->Add(gr_47,"p");
               if(imeas==0){
                  leg0->AddEntry(gr_47,Form("%s - 4.7 GeV",measg1->GetExperimentName()),"p");
               }else {
                  leg0->AddEntry(gr_47,Form("%s - 5.9 GeV",measg1->GetExperimentName()),"p");
               }

               TGraphErrors * gr_syst = m0->BuildSystematicErrorBand("x",[](double x){ return x*x; },-0.008);
               gr_syst->SetMarkerColor(acol);
               gr_syst->SetLineColor(acol);
               gr_syst->SetFillColor(acol);
               gr_syst->SetFillStyle(3002+imeas);
               gr_syst->SetMarkerSize(1.5);
               mg_d2n_g1->Add(gr_syst,"e3");
            }
         }
         continue;
      }
      leg->AddEntry(graph,Form("%s",measg1->GetExperimentName()),"p");
      leg0->AddEntry(graph,Form("%s",measg1->GetExperimentName()),"p");

      // Add to TMultiGraph 
      MG_data1->Add(graph,"p"); 

      //TGraphErrors * graph2 = new TGraphErrors(*graph);
      //graph2->Apply(fTimes2);
      //graph2->SetMarkerSize(1.5);
      //MG->Add(graph2,"p"); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }
   ////TGraphErrors *graph        = measg1_saneQ2->BuildGraph("x");
   ////graph->Apply(xf);
   ////graph->SetMarkerColor(1);
   ////graph->SetMarkerStyle(34);
   ////leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   ////MG->Add(graph,"p");
   ////leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   ////leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   ////leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   ////leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");
   //MG->Draw("A");
   //return 0;

   //// -------------------------------------------------------
   //// x2g1n 

   TLegend *leg2  = new TLegend(0.84, 0.15, 0.99, 0.85);
   leg2->SetFillColor(0);
   leg2->SetFillStyle(0);
   leg2->SetBorderSize(0);
   TLegend *leg02 = new TLegend(0.15, 0.65, 0.42, 0.88);
   leg02->SetFillColor(0);
   leg02->SetFillStyle(0);
   leg02->SetBorderSize(0);
   TLegend *leg22 = new TLegend(0.15, 0.65, 0.42, 0.88);
   leg22->SetFillColor(0);
   leg22->SetFillStyle(0);
   leg22->SetBorderSize(0);

   TMultiGraph *MG2 = new TMultiGraph(); 
   TMultiGraph *MG_data2 = new TMultiGraph(); 
   TMultiGraph *mg_d2n_g2 = new TMultiGraph(); 

   //NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));

   measurementsList->Clear();
   measurementsList =  manager->GetMeasurements("g2He3");

   for (int i = 0; i < measurementsList->GetEntries(); i++) {

      NucDBMeasurement *measg2 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;

      if(!strcmp(measg2->GetExperimentName(),"SANE") ){
         continue;
      }

      plist = measg2->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg2->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      // Get TGraphErrors object 
      TGraphErrors *graph        = measg2->BuildGraph("x");
      graph->Apply(x2f);
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      graph->SetMarkerSize(1.5);

      if(!strcmp(measg2->GetExperimentName(),"JLAB-E06014") ){
         TList * meas_by_beam = measg2->CreateMeasurementsWithUniqueBins("Ebeam");
         if(meas_by_beam->GetEntries()){

            for(int imeas= 0; imeas<meas_by_beam->GetEntries();imeas++){
               NucDBMeasurement * m0 = (NucDBMeasurement*)meas_by_beam->At(imeas);

               TGraphErrors * gr_47        = m0->BuildGraph("x");
               int acol = 2+imeas*2;
               gr_47->Apply(x2f);
               gr_47->SetMarkerColor(acol);
               gr_47->SetMarkerStyle(20);
               gr_47->SetLineColor(acol);
               gr_47->SetMarkerSize(1.5);
               mg_d2n_g2->Add(gr_47,"p");
               if(imeas==0){
                  leg02->AddEntry(gr_47,Form("%s - 4.7 GeV",measg2->GetExperimentName()),"p");
               }else {
                  leg02->AddEntry(gr_47,Form("%s - 5.9 GeV",measg2->GetExperimentName()),"p");
               }

               TGraphErrors * gr_syst = m0->BuildSystematicErrorBand("x",[](double x){ return x*x; },-0.008);
               gr_syst->SetMarkerColor(acol);
               gr_syst->SetLineColor(acol);
               gr_syst->SetFillColor(acol);
               gr_syst->SetFillStyle(3002+imeas);
               gr_syst->SetMarkerSize(1.5);
               mg_d2n_g2->Add(gr_syst,"e3");
            }
         }
         //graph->SetMarkerColor(4);
         //graph->SetLineColor(4);
         //graph->SetMarkerStyle(20);
         //graph->SetMarkerSize(1.5);
         //mg_d2n_g2->Add(graph,"p");
         //leg02->AddEntry(graph,Form("%s",measg2->GetExperimentName()),"p");
         //TGraphErrors * gr_syst        = measg2->BuildSystematicErrorBand("x",[](double x){ return x*x; },-0.02);
         //gr_syst->SetMarkerColor(4);
         //gr_syst->SetLineColor(4);
         //gr_syst->SetFillColor(4);
         //gr_syst->SetFillStyle(3002);
         //gr_syst->SetMarkerSize(1.5);
         //mg_d2n_g2->Add(gr_syst,"e3");
         continue;
      }
      leg22->AddEntry( graph,measg2->GetExperimentName(),"p");
      leg2->AddEntry( graph,measg2->GetExperimentName(),"p");
      leg02->AddEntry(graph,measg2->GetExperimentName(),"p");

      MG_data2->Add(graph,"p"); 

      //TGraphErrors * graph2 = new TGraphErrors(*graph);
      //graph2->Apply(fTimes3);
      //graph2->SetMarkerSize(1.5);
      //MG2->Add(graph2,"p"); 

   }

   // -------------------------------------------------------
   // Create graphs for ALL the models 
   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();

   // ------------------------------------------------------------
   // g1 and g2 on same canvas 
   TCanvas     * c       = new TCanvas();
   TMultiGraph * mg_temp = new TMultiGraph();

   c->Divide(1,2);
   c->cd(1);
   ////gPad->SetGridy(true);

   mg_temp->Add(MG_data1);
   ////mg_temp->Add(mg);

   mg_temp->Draw("AP");
   mg_temp->SetTitle("");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->SetTitle("2x^{2}g_{1}^{p}");
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.03,0.1); 

   //TGraph * gr_x2g2n  = new TGraph( x2g2n->DrawCopy("goff")->GetHistogram());
   //gr_x2g2n->Apply(f2);
   ////gr_x2g2n->Draw("l");
   //mg_temp->Add(gr_x2g2n,"l");
   mg_temp->Draw("A");
   leg->Draw();

   // ---------------------------
   //
   c->cd(2);
   mg_temp = new TMultiGraph();
   ////gPad->SetGridy(true);

   ////mg_temp->Add(MG2);
   ////mg_temp->Add(mg2);
   mg_temp->Add(MG_data2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("");
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.2,0.15); 
   mg_temp->GetYaxis()->SetTitle("3x^{2}g_{2}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();

   //// Plot models
   ////TGraph * gr_x2g1n = new TGraph( xg1nWW->DrawCopy("goff")->GetHistogram());
   ////gr_x2g1n->Apply(f3);
   ////////gr_x2g1n->Draw("l");
   //////MG2->Add(gr_x2g1n,"l");
   //////MG2->Add(mg_models);

   ////mg_temp->Draw("a");
   leg22->Draw();

   mg_temp->Draw("a");
   c->Update();


   c->SaveAs(Form("results/structure_functions/x2g1g2_3He_%d.pdf",aNumber));
   c->SaveAs(Form("results/structure_functions/x2g1g2_3He_%d.png",aNumber));

   // ----------------------------------------------------------------
   // Models
   c = new TCanvas();
   TLegend * legmod = new TLegend(0.40,0.7,0.60,0.88);
   legmod->SetFillColor(0);
   legmod->SetFillStyle(0);
   legmod->SetBorderSize(0);

   TList * list_of_models = new TList();
   TF1   * aModel         = 0;

   InSANEStructureFunctions          * sfs  = 0;
   InSANEPolarizedStructureFunctions * psfs = 0;

   // BBS
   sfs  = fman->CreateSFs(5);
   psfs = fman->CreatePolSFs(5);
   aModel = new TF1("xg1nbbs", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"BBS","l");

   // statistical
   sfs  = fman->CreateSFs(6);
   psfs = fman->CreatePolSFs(6);
   aModel = new TF1("xg1n", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"Stat 2015","l");

   // BB 
   psfs = fman->CreatePolSFs(3);
   aModel = new TF1("xg1nBB", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BB","l");


   // AAC
   psfs = fman->CreatePolSFs(2);
   aModel = new TF1("xg1naac", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"AAC","l");

   // LSS
   psfs = fman->CreatePolSFs(1);
   aModel = new TF1("xg1nlss", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"LSS2006","l");

   // DSSV
   psfs = fman->CreatePolSFs(0);
   aModel = new TF1("xg1nlssdssv", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"DSSV","l");

   TMultiGraph * mg_g1_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      TGraph * grmod = new TGraph( aModel->DrawCopy("goff")->GetHistogram());
      mg_g1_models->Add(grmod,"l");
   }

   //// --------------------------------------------------
   // g2 Models
   legmod2 = new TLegend(0.51,0.65,0.68,0.88);
   legmod2->SetFillColor(0);
   legmod2->SetFillStyle(0);
   legmod2->SetBorderSize(0);

   //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   list_of_models = new TList();
   //TF1 * aModel = 0;


   // BBS
   psfs = fman->CreatePolSFs(5);
   aModel = new TF1("xg2nbbs", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"BBS","l");

   // statistical
   psfs = fman->CreatePolSFs(13);
   aModel = new TF1("xg2n", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2nWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2nWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"Stat 2015","l");

   // BB 
   psfs = fman->CreatePolSFs(3);
   aModel = new TF1("xg2nBB", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"BB","l");


   // AAC
   psfs = fman->CreatePolSFs(2);
   aModel = new TF1("xg2naac", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"AAC","l");

   // LSS
   psfs = fman->CreatePolSFs(1);
   aModel = new TF1("xg2nlss", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"LSS2006","l");

   // DSSV
   psfs = fman->CreatePolSFs(0);
   aModel = new TF1("xg2nlssdssv", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2He3, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2He3");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"DSSV","l");

   TMultiGraph * mg_g2_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      TGraph * grmod = new TGraph( aModel->DrawCopy("goff")->GetHistogram());
      mg_g2_models->Add(grmod,"l");
   }


   ////---------------------------------------------------------------
   //// Nicer individual plots

   double g1_min = -0.01;
   double g1_max =  0.01;
   double g2_min = -0.03;
   double g2_max =  0.03;

   //// ---------------------------------
   // g1 with data
   TCanvas * c2 = new TCanvas();

   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data1);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{1}^{^{3}He}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(g1_min,g1_max); 

   leg->Draw();

   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_1_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_1_%d.png",aNumber));


   // ---------------------------------
   // world data + clas

   mg_temp->Add(mg_d2n_g1);
   mg_temp->Draw("a");
   leg0->Draw();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_2_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_2_%d.png",aNumber));

   // ---------------------------------
   // world dat + models
   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data1);
   mg_temp->Add(mg_g1_models);
   mg_temp->Add(mg_d2n_g1);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{1}^{^{3}He}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(g1_min,g1_max); 

   legmod->Draw();
   leg0->Draw();

   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_3_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_3_%d.png",aNumber));


   // --------------------------------------------------------------
   // g2   Nicer individual plots

   c2 = new TCanvas();

   mg_temp = new TMultiGraph();
   mg_temp->Add(MG_data2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{2}^{^{3}He}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(g2_min,g2_max); 
   leg22->Draw();

   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_4_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_4_%d.png",aNumber));

   // ------------------------
   //
   mg_temp->Add(mg_g2_models);
   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_5_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_5_%d.png",aNumber));

   // ------------------------
   //
   mg_temp->Add(mg_d2n_g2);
   mg_temp->Draw("a");
   leg02->Draw();
   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_6_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_6_%d.png",aNumber));

   // ------------------------
   //
   //mg_temp->Add(mg_g2_models);
   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_7_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_7_%d.png",aNumber));


   // ------------------------
   //
   c2 = new TCanvas();

   mg_temp = new TMultiGraph();
   mg_temp->Add(MG_data2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{2}^{^{3}He}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(g2_min,g2_max); 

   leg22->Draw();
   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_8_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_8_%d.png",aNumber));

   mg_temp->Draw("a");
   mg_temp->Add(mg_d2n_g2);

   leg02->Draw();
   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_9_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_9_%d.png",aNumber));

   // ------------------------
   // g2 zommed
   c2 = new TCanvas();

   mg_temp = new TMultiGraph();
   mg_temp->Add(MG_data2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{2}^{^{3}He}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.01,0.01); 
   //mg_temp->GetYaxis()->SetRangeUser(g2_min,g2_max); 

   leg22->Draw();
   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_10_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_10_%d.png",aNumber));

   mg_temp->Draw("a");
   mg_temp->Add(mg_d2n_g2);

   leg02->Draw();
   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_11_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_11_%d.png",aNumber));

   // ---------------------------------
   mg_temp->Add(mg_g2_models);
   mg_temp->Add(mg_d2n_g2);

   legmod->Draw();
   leg02->Draw();
   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_12_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_12_%d.png",aNumber));

   // ---------------------------------
   // g1 agian
   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data1);
   //mg_temp->Add(mg_d2n_g1);
   mg_temp->Add(mg_g1_models);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{1}^{^{3}He}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(g1_min,g1_max); 

   legmod->Draw();
   leg->Draw();

   c2->Update();
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_13_%d.pdf",aNumber));
   c2->SaveAs(Form("results/structure_functions/x2g1g2_3He_13_%d.png",aNumber));
   //// -----------------------------
   //// with SANE data

   //mg_temp->Add(mg_g2_models);

   //legmod->Draw();

   //c2->Update();
   //c2->SaveAs(Form("results/asymmetries/x2g2_combined2_%d.png",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g2_combined2_%d.pdf",aNumber));

   return(0);
}
