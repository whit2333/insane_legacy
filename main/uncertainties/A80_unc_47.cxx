#include "util/stat_error_graph.cxx"

Int_t A80_unc_47(Int_t perpRunGroup = 4306, Int_t aNumber=4306)
{

   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager    * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs  = fman->GetStructureFunctions();

   const char * perpfile = Form("data/binned_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f1 = TFile::Open(perpfile,"UPDATE");
   f1->cd();
   TList * fperpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp47-%d",0));
   if(!fperpAsymmetries) return(-1);


   TList * systematics = new TList();

   TList * SystHists1 = new TList();

   TMultiGraph * mg = new TMultiGraph();

   TLegend *leg = new TLegend(0.17, 0.70, 0.40, 0.88);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   std::cout << " DERP \n";
   fperpAsymmetries->Print();


   std::vector<TMultiGraph*> mg_syst;

   std::vector<std::string> syst_names;

   // Loop over perpllel asymmetries Q2 bins
   for(int jj = 0;jj<fperpAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fperpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetMeasuredAsymmetryResult();

      TH1F * fAperp = &perpAsym->fAsymmetryVsx;

      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsx;

      perpAsym->fSystErrVsx.SetLineColor(fAperp->GetLineColor());

      SystHists1->Add(&perpAsym->fSystErrVsx);


      // systematics 
      if(jj<4) {
         for(int i=0; i< perpAsym->fSystematics_x.size() ; i++){
            if(jj==0) {
               mg_syst.push_back( new TMultiGraph());
               syst_names.push_back( perpAsym->fSystematics_x.at(i).GetName() );
            }

            TH1F * hsys = new TH1F(perpAsym->fSystematics_x.at(i).fRelativeUncertainties);
            //hsys->Multiply(fAperp);
            mg_syst[i]->Add(stat_error_graph(hsys,0.0),"e3");


         }
      }

      // ------------------------------------
      // A180
      TGraphErrors * gr = new TGraphErrors(fAperp);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = perpAsym->fSystErrVsx.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         Double_t eyt_sys = perpAsym->fSystErrVsx.GetBinContent(abin);
         if( yt == 0.0 || eyt_sys>1.0 || eyt>1.0 ) {
            gr->RemovePoint(j);
            perpAsym->fSystErrVsx.SetBinContent(abin,0.0);
            perpAsym->fSystErrVsx.SetBinError(abin,0.0);
            continue;
         }
      }
      if(jj==4) {
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
      }
      if(jj<4) {
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("#LTQ^{2}#GT=%.1f GeV^{2}",perpAsym->GetQ2()),"lp");
      }
       
   }

   // -----------------------------------------------------------------
   // A180

   TCanvas * c = new TCanvas();
   TMultiGraph *MG = new TMultiGraph(); 

   MG->Add(mg);
   MG->Draw("AP");
   MG->SetTitle("");//A_{180}^{p},  E= GeV");
   MG->GetXaxis()->SetTitle("x");
   MG->GetXaxis()->CenterTitle(true);
   MG->GetYaxis()->SetTitle("");
   MG->GetYaxis()->CenterTitle(true);
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.5,1.19); 
   MG->Draw("AP");

   TMultiGraph * mg_syst_err = new TMultiGraph();
   for(int i=0;i<SystHists1->GetEntries()&&i<4;i++){
      TH1 * hsys2 = (TH1*)SystHists1->At(i);
      mg_syst_err->Add(stat_error_graph(hsys2,-0.4),"e3");
   }
   MG->Add(mg_syst_err);

   TLatex l;
   l.SetTextSize(0.05);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(-15);
   //l.DrawLatex(0.05,0.2,"systematic errors #rightarrow");

   leg->Draw();

   c->SaveAs(Form("results/asymmetries/A80_unc_47_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A80_unc_47_%d.png",aNumber));

   // -----------------------------------------------------------------
   c = new TCanvas();
   c->Divide(1,6);
   for(int i = 0; i<6; i++) {
      c->cd(i+1);
      mg_syst[i]->Draw("AP");
      mg_syst[i]->SetTitle(syst_names[i].c_str());//A_{180}^{p},  E= GeV");
      mg_syst[i]->GetXaxis()->SetTitle("x");
      mg_syst[i]->GetXaxis()->CenterTitle(true);
      mg_syst[i]->GetYaxis()->SetTitle("");
      mg_syst[i]->GetYaxis()->CenterTitle(true);
      mg_syst[i]->GetXaxis()->SetLimits(0.0,1.0); 
      mg_syst[i]->GetYaxis()->SetRangeUser(-0.5,1.19); 
      mg_syst[i]->Draw("AP");
   }

   c->SaveAs(Form("results/asymmetries/A80_unc_47_2_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A80_unc_47_2_%d.png",aNumber));


   return(0);
}
