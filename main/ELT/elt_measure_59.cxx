#include "asym/sane_data_bins.cxx"
//int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

#include "util/d2_table.h"

Int_t elt_measure_59( Int_t aNumber = 8406 ) {

  using namespace TMath;
  Int_t paraFileVersion = aNumber;
  Int_t perpFileVersion = aNumber;
  Int_t para59Version   = aNumber;
  Int_t perp59Version   = aNumber;

  // ----------------------------------------------------------------
  // Model used to calculate  missing regions
  InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 
  InSANEFormFactors     * ffs  = fman->CreateFFs(4);
  TString    sfName    = "stat2015";
  InSANEPolarizedStructureFunctions * model_SFs            = fman->CreatePolSFs(6);
  auto best_sfs  = fman->CreateSFs(11);
  auto best_ssfs = fman->CreatePolSFs(1);

  sane_data_bins();

  // ----------------------------------------------------------------
  //
  TFile * f59 = new TFile(Form("data/g1g2_59_%d.root",aNumber),"READ");
  TList * fg1_comb_list = (TList*)gROOT->FindObject("g1_59_W");
  TList * fg2_comb_list = (TList*)gROOT->FindObject("g2_59_W");
  if(!fg1_comb_list) {std::cout << "-11" << std::endl; return -11;}
  if(!fg2_comb_list){ std::cout << "-12" << std::endl; return -12;}

  TFile * fA59 = new TFile(Form("data/A1A2_59_%d.root",aNumber),"READ");
  TList * fA1_comb_list = (TList*)gROOT->FindObject("A1_59_W");
  TList * fA2_comb_list = (TList*)gROOT->FindObject("A2_59_W");
  if(!fA1_comb_list) {std::cout << "-13" << std::endl; return -13;}
  if(!fA2_comb_list){ std::cout << "-14" << std::endl; return -14;}

  std::vector<Double_t>   bin_d2p_value    ;
  std::vector<Double_t>   bin_wd2p_exp     ;
  std::vector<Double_t>   bin_d2p_exp      ;
  std::vector<Double_t>   bin_err_d2p_exp  ;
  std::vector<Double_t>   bin_syserr_d2p_exp  ;
  std::vector<Double_t>   bin_d2p_exp2     ;
  std::vector<Double_t>   bin_wd2p_exp2    ;
  std::vector<Double_t>   bin_err_d2p_exp2 ;
  std::vector<Double_t>   bin_syserr_d2p_exp2 ;
  std::vector<Double_t>   bin_d2p_nach     ;
  std::vector<Double_t>   bin_err_d2p_nach ;
  std::vector<Double_t>   bin_syserr_d2p_nach ;
  std::vector<Double_t>   bin_d2p_OAR     ;
  std::vector<Double_t>   bin_err_d2p_OAR ;
  std::vector<Double_t>   bin_syserr_d2p_OAR ;
  std::vector<Int_t>      bin_iXbins;
  std::vector<Double_t>   bin_Q2 ;
  std::vector<Double_t>   bin_Q2min;
  std::vector<Double_t>   bin_Q2max;
  std::vector<Int_t>      bin_iQ2 ;
  std::vector<Double_t>   bin_mean_Q2 ;

  std::vector<Double_t> xLimits_0;
  std::vector<Double_t> xLimits_1;

  std::vector<d2_table_entry> d2_table;

   std::vector<TGraphErrors*>  d2_integrands;
   std::vector<TGraphErrors*>  d2_integrands_syst;

   // ------------------------------------------
   // Loop over Q2 bins
   for(int jj = 0;((jj<fg1_comb_list->GetEntries())&&(jj<5));jj++) {

      InSANEMeasuredAsymmetry         * fg1_asym        = (InSANEMeasuredAsymmetry * )(fg1_comb_list->At(jj));
      InSANEMeasuredAsymmetry         * fg2_asym        = (InSANEMeasuredAsymmetry * )(fg2_comb_list->At(jj));

      InSANEMeasuredAsymmetry         * fA1_asym        = (InSANEMeasuredAsymmetry * )(fA1_comb_list->At(jj));
      InSANEMeasuredAsymmetry         * fA2_asym        = (InSANEMeasuredAsymmetry * )(fA2_comb_list->At(jj));

      // kine is the same for g1 and g2
      InSANEAveragedKinematics1D      * kine            = &fg1_asym->fAvgKineVsW;

      TH1F * fg1   = &fg1_asym->fAsymmetryVsW;
      TH1F * fg2   = &fg2_asym->fAsymmetryVsW;
      TH1F * fA1   = &fA1_asym->fAsymmetryVsW;
      TH1F * fA2   = &fA2_asym->fAsymmetryVsW;

      //fg1_asym->MaskIntersection(fg1_asym->fMask_W,fg2_asym->fMask_W);
      TH1F * mask  = &fg1_asym->fMask_W;

      int lo_W_bin = mask->FindFirstBinAbove(0.1);
      if( lo_W_bin == -1 ) {
        std::cout << "Could not find bin above 0.1\n";
      }
      int hi_W_bin = mask->FindLastBinAbove(0.1);
      if( hi_W_bin == -1 ) {
        std::cout << "Could not find bin above 0.1\n";
      }

      d2_table_entry d2_point; 
      double W1 = mask->GetBinLowEdge(lo_W_bin);
      double W2 = mask->GetBinLowEdge(hi_W_bin+1);
      double Q2_1 = kine->fQ2.GetBinContent(lo_W_bin);
      double Q2_2 = kine->fQ2.GetBinContent(hi_W_bin);
      //double W1   = kine->fW.GetBinContent(lo_W_bin);
      //double W2   = kine->fW.GetBinContent(hi_W_bin);
      double x1   = InSANE::Kine::xBjorken_WQ2(W1,Q2_1);
      double x2   = InSANE::Kine::xBjorken_WQ2(W2,Q2_2);
      std::cout << " ===== " << jj << "=====\n";
      std::cout << lo_W_bin << "  W1: " << W1 << "  Q2_1: " << Q2_1 << "  x1: " << x1 << '\n';
      std::cout << hi_W_bin << "  W2: " << W2 << "  Q2_2: " << Q2_2 << "  x2: " << x2 << '\n';
      // 
      if(x2<x1) {
        xLimits_0.push_back(x2);
        xLimits_1.push_back(x1);
      } else {
        xLimits_0.push_back(x1);
        xLimits_1.push_back(x2);
      }
      d2_point.Q2_mean = fg1_asym->GetQ2();
      d2_point.Q2_min  = Q2_2;
      d2_point.Q2_max  = Q2_1;
      d2_point.W_mean  = (W2+W1)/2.0;
      d2_point.W_min   = W1;
      d2_point.W_max   = W2;
      d2_point.x_min   = x2;
      d2_point.x_max   = x1;

      TGraphErrors * gr_d2_nacht_integrand      = new TGraphErrors(1);
      TGraphErrors * gr_d2_nacht_integrand_syst = new TGraphErrors(1);
      d2_integrands.push_back(gr_d2_nacht_integrand);
      d2_integrands_syst.push_back(gr_d2_nacht_integrand_syst);

      TGraphErrors * gr_d2_OAR_integrand      = new TGraphErrors(1);
      TGraphErrors * gr_d2_OAR_integrand_syst = new TGraphErrors(1);
      d2_integrands.push_back(gr_d2_OAR_integrand);
      d2_integrands_syst.push_back(gr_d2_OAR_integrand_syst);

      bin_Q2.push_back(   fg1_asym->GetQ2());
      bin_Q2min.push_back(fg1_asym->fQ2Min);
      bin_Q2max.push_back(fg1_asym->fQ2Max);

      Double_t d2p_value       = 0.0;
      Double_t wd2p_exp        = 0.0;
      Double_t d2p_exp         = 0.0;
      Double_t err_d2p_exp     = 0.0;
      Double_t syserr_d2p_exp  = 0.0;
      Double_t d2p_exp2        = 0.0;
      Double_t wd2p_exp2       = 0.0;
      Double_t err_d2p_exp2    = 0.0;
      Double_t syserr_d2p_exp2 = 0.0;
      Double_t d2p_nach        = 0.0;
      Double_t err_d2p_nach    = 0.0;
      Double_t syserr_d2p_nach = 0.0;
      Double_t d2p_OAR         = 0.0;
      Double_t err_d2p_OAR     = 0.0;
      Double_t syserr_d2p_OAR  = 0.0;
      Int_t    iXbins          = 0;

      Int_t   xBinmax = fg1->GetNbinsX();
      Int_t   yBinmax = fg1->GetNbinsY();
      Int_t   zBinmax = fg1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      Int_t iQ2 = 0;
      Int_t mean_Q2 = 0.0;

      // ----------------------------------------------------------
      // 
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fg1->GetBin(i,j,k);

               Double_t g1      = fg1->GetBinContent(bin);
               Double_t g2      = fg2->GetBinContent(bin);
               Double_t eg1     = fg1->GetBinError(bin);
               Double_t eg2     = fg2->GetBinError(bin);
               Double_t eg1Syst = fg1_asym->fSystErrVsW.GetBinError(bin);
               Double_t eg2Syst = fg2_asym->fSystErrVsW.GetBinError(bin);

               Double_t A2      = fA2->GetBinContent(bin);
               Double_t eA2     = fA2->GetBinError(bin);
               Double_t eA2Syst = fA2_asym->fSystErrVsW.GetBinError(bin);

               if(  TMath::IsNaN(eg1Syst) || TMath::IsNaN(eg2Syst) ) {
                  eg1Syst  = 0.0;
                  eg2Syst  = 0.0;
               }

               Double_t W_low = kine->fW.GetBinLowEdge(bin);
               Double_t dW    = kine->fW.GetBinWidth(bin);
               Double_t W_hi  = W_low+dW;

               Double_t W     = kine->fW.GetBinContent(bin);
               Double_t x     = kine->fx.GetBinContent(bin);
               Double_t Q2    = kine->fQ2.GetBinContent(bin);
               Double_t M     = (M_p/GeV);
               Double_t xi    = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));

               Double_t x_hi  = InSANE::Kine::xBjorken_WQ2(W_low,Q2);
               Double_t x_low = InSANE::Kine::xBjorken_WQ2(W_low+dW,Q2);
               Double_t dx = x_hi - x_low;



               // -------------------------------------
               // ELT sum rule 
               // \int dx x (g1 +2 g2) =  \int dx x (2gT-g1) = 0
               Double_t coeff_OAR    = x;
               Double_t g1_coeff_OAR = -1.0;
               Double_t gT_coeff_OAR = 2.0;
               Double_t gamma        = TMath::Sqrt((2.0*x*M)*(2.0*x*M)/Q2);
               Double_t F1           = best_sfs->F1p(x,Q2);
               Double_t gT_OAR       = (F1/gamma)*A2;
               Double_t egT_OAR      = (F1/gamma)*eA2;
               Double_t egT_OARSyst  = (F1/gamma)*eA2Syst;
               Double_t g1_emp       = best_ssfs->g1p_TMC(x,Q2);
               Double_t eg1_emp      = 0.0;

               Double_t d2p_OAR_int      = coeff_OAR*(g1_coeff_OAR*g1_emp    + gT_coeff_OAR*gT_OAR);
               Double_t ed2p_OAR_int     = coeff_OAR*(g1_coeff_OAR*0.0       + gT_coeff_OAR*egT_OAR);
               Double_t ed2p_OAR_intSyst = coeff_OAR*(g1_coeff_OAR*eg1_emp   + gT_coeff_OAR*egT_OARSyst);


               // -------------------------------------
               // Nachtmann moments
               //Double_t dx    = kine->fx.GetBinWidth(bin);
               // for n = 3
               Double_t g1_coeff_nach       = 1.0*x;
               Double_t g2_coeff_nach       = 2.0*x;
               Double_t d2p_nach_int        = (g1_coeff_nach*g1 + g2_coeff_nach*g2);
               Double_t err_d2p_nach_int    = (g1_coeff_nach*eg1 + g2_coeff_nach*eg2);
               Double_t syserr_d2p_nach_int = (g1_coeff_nach*eg1Syst + g2_coeff_nach*eg2Syst)/2.0;

               Double_t x2g1     = x*g1;
               Double_t x2g2     = x*g2;
               Double_t ex2g1    = x*eg1;
               Double_t ex2g2    = x*eg2;
               Double_t ex2g1Syst = x*eg1Syst;
               Double_t ex2g2Syst = x*eg2Syst;

               Double_t d2_int_exp     = (2.0*x2g1 + 3.0*x2g2);
               Double_t ed2_int_exp    = TMath::Sqrt(TMath::Power(2.0*ex2g1,2.0) + TMath::Power(3.0*ex2g2,2.0));
               Double_t sysed2_int_exp = TMath::Sqrt(TMath::Power(2.0*ex2g1Syst,2.0) + TMath::Power(3.0*ex2g2Syst,2.0));

               Double_t g2WW_model      = model_SFs->x2g2pWW(x,Q2);
               Double_t g1_model        = model_SFs->g1p_Twist2(x,Q2);
               Double_t g1_twist3_model = model_SFs->g1p_Twist3(x,Q2);
               Double_t eg2WW_model     = ex2g1/(x*x);
               Double_t eg1_model       = ex2g1/(x*x);

               Double_t x2g2bar         = (2.0*x2g2 + x*(g2WW_model));
               Double_t x2g1bar         =  x2g1 - x*x*(g1_model+g1_twist3_model);

               Double_t ex2g2bar        = TMath::Sqrt(ex2g2*ex2g2 )*3.0;
               Double_t sysex2g2bar     = TMath::Sqrt(ex2g2Syst*ex2g2Syst + TMath::Power(x*x*eg2WW_model,2.0))*3.0;

               Double_t xg2bar          = 0;
               Double_t exg2bar         = 0;
               Double_t ex2g1bar        = 0.0;

               if(  TMath::IsNaN(x2g1) || TMath::IsNaN(x2g2)  ||  TMath::IsNaN(ex2g1) || TMath::IsNaN(ex2g2) ) {
                  x2g1bar  = 0.0;
                  x2g2bar  = 0.0;
                  xg2bar   = 0.0;
                  ex2g1bar = 0.0;
                  ex2g2bar = 0.0;
                  exg2bar  = 0.0;
               }

               if(jj<4) {

                  if(x > xLimits_0[jj] && x < xLimits_1[jj] ) {

                     if( (!TMath::IsNaN(d2_int_exp))  ){

                        if(  ( ed2_int_exp > 2.5)  ){
                           std::cout << "error: W " << x << "  "  << ex2g2bar << std::endl;
                           std::cout << " low : " << xLimits_0[jj] << " high : " << xLimits_1[jj] << std::endl;
                        } else {

                           //if(ed2_int_exp == 0.0 ) ed2_int_exp = 9999.0;
                           //std::cout << Q2 << std::endl;

                           d2p_exp          += (d2_int_exp*dx);
                           err_d2p_exp      += TMath::Power(ed2_int_exp*dx,2.0);
                           syserr_d2p_exp   += TMath::Power(sysed2_int_exp*dx,2.0);

                           d2p_exp2         += (x2g2bar*dx);
                           err_d2p_exp2     += TMath::Power(ex2g2bar*dx,2.0);
                           syserr_d2p_exp2  += TMath::Power(sysex2g2bar*dx,2.0);

                           mean_Q2 += Q2;
                           iQ2++;

                           d2p_nach        += (d2p_nach_int*dx);
                           err_d2p_nach    += TMath::Power(err_d2p_nach_int*dx,2.0);
                           syserr_d2p_nach += TMath::Power(syserr_d2p_nach_int*dx,2.0);

                           gr_d2_nacht_integrand->SetPoint(          iXbins, x,   d2p_nach_int);
                           gr_d2_nacht_integrand->SetPointError(     iXbins, 0.0, err_d2p_nach_int);
                           gr_d2_nacht_integrand_syst->SetPoint(     iXbins, x,   -0.5+syserr_d2p_nach_int);
                           gr_d2_nacht_integrand_syst->SetPointError(iXbins, 0.0, syserr_d2p_nach_int);

                           d2p_OAR        += (d2p_OAR_int*dx);
                           err_d2p_OAR    += TMath::Power(ed2p_OAR_int*dx,    2.0);
                           syserr_d2p_OAR += ed2p_OAR_intSyst*dx;

                           gr_d2_OAR_integrand->SetPoint(          iXbins, x,   d2p_OAR_int);
                           gr_d2_OAR_integrand->SetPointError(     iXbins, 0.0, ed2p_OAR_int);
                           gr_d2_OAR_integrand_syst->SetPoint(     iXbins, x,   -0.5+ed2p_OAR_intSyst);
                           gr_d2_OAR_integrand_syst->SetPointError(iXbins, 0.0, ed2p_OAR_intSyst);


                           iXbins++;
                           //std::cout << "error: x " << x << "  "  << ex2g2bar << std::endl;
                        }
                     }
                  }
               }

            } // bin loop
         }
      }

      // -------------------------------

      mean_Q2 = mean_Q2/double(iQ2);
      bin_mean_Q2.push_back(mean_Q2);

      d2p_nach        = d2p_nach;
      err_d2p_nach    = err_d2p_nach;
      syserr_d2p_nach = syserr_d2p_nach;

      d2p_OAR        = d2p_OAR;
      err_d2p_OAR    = err_d2p_OAR;
      syserr_d2p_OAR = syserr_d2p_OAR;

      d2_point.d2_Nacht_measured = d2p_nach;
      d2_point.d2_Nacht_stat_unc = TMath::Sqrt(err_d2p_nach);
      d2_point.d2_Nacht_syst_unc = TMath::Sqrt(syserr_d2p_nach)/double(iXbins);

      d2_point.d2_OAR_measured = d2p_OAR;
      d2_point.d2_OAR_stat_unc = TMath::Sqrt(err_d2p_OAR);
      d2_point.d2_OAR_syst_unc = syserr_d2p_OAR/double(iXbins);

      bin_d2p_value.push_back    ( d2p_value    );
      bin_wd2p_exp.push_back     ( wd2p_exp     );
      bin_d2p_exp.push_back      ( d2p_exp      );
      bin_err_d2p_exp.push_back  ( err_d2p_exp  );
      bin_syserr_d2p_exp.push_back  ( syserr_d2p_exp  );
      bin_d2p_exp2.push_back     ( d2p_exp2     );
      bin_wd2p_exp2.push_back    ( wd2p_exp2    );
      bin_err_d2p_exp2.push_back ( err_d2p_exp2 );
      bin_syserr_d2p_exp2.push_back ( syserr_d2p_exp2 );

      bin_d2p_nach.push_back     ( d2p_nach     );
      bin_err_d2p_nach.push_back ( err_d2p_nach );
      bin_syserr_d2p_nach.push_back ( syserr_d2p_nach );

      bin_d2p_OAR.push_back     ( d2p_OAR     );
      bin_err_d2p_OAR.push_back ( err_d2p_OAR );
      bin_syserr_d2p_OAR.push_back ( syserr_d2p_OAR );

      bin_iXbins.push_back       ( iXbins);

      d2_table.push_back(d2_point);

   }

   for(int i=0; i < bin_d2p_value.size() && i<fSANENBins; i++) {

      if( bin_iXbins[i] == 0 ) bin_iXbins[i] = 1;

      Double_t g1p_lowx_model  = model_SFs->Mellin_g1p(2,bin_Q2[i],0.01,xLimits_0[i]);
      Double_t g2p_lowx_model  = model_SFs->Mellin_g2p(2,bin_Q2[i],0.01,xLimits_0[i]);
      Double_t d2p_lowx_model  = g1p_lowx_model + 2.0*g2p_lowx_model;

      Double_t g1p_highx_model = model_SFs->d2p_tilde(bin_Q2[i],xLimits_1[i],0.99);
      Double_t g2p_highx_model = model_SFs->d2p_tilde(bin_Q2[i],xLimits_1[i],0.99);
      Double_t d2p_highx_model = g1p_highx_model + 2.0*g2p_highx_model;

      Double_t M23_lowx_model  = model_SFs->M2n_p(3,bin_Q2[i],0.01,xLimits_0[i]);
      Double_t M23_highx_model = model_SFs->M2n_p(3,bin_Q2[i],xLimits_1[i],0.99);
      Double_t d2p_tilde_lowx_model    = M23_lowx_model *2.0;
      Double_t d2p_tilde_highx_model   = M23_highx_model*2.0;
      Double_t d2p_tilde_elastic_model =  ffs->d2p_el_Nachtmann(bin_Q2[i])*2.0;

      bin_err_d2p_exp[i]     = TMath::Sqrt(bin_err_d2p_exp[i]  );
      bin_err_d2p_exp2[i]    = TMath::Sqrt(bin_err_d2p_exp2[i] );
      bin_err_d2p_nach[i]    = TMath::Sqrt(bin_err_d2p_nach[i] );
      bin_syserr_d2p_exp[i]  = TMath::Sqrt(bin_syserr_d2p_exp[i]  /double(bin_iXbins[i]));
      bin_syserr_d2p_exp2[i] = TMath::Sqrt(bin_syserr_d2p_exp2[i] /double(bin_iXbins[i]));
      bin_syserr_d2p_nach[i] = TMath::Sqrt(bin_syserr_d2p_nach[i] /double(bin_iXbins[i]));
 
      d2_table[i].d2_el_model   = d2p_tilde_elastic_model;
      d2_table[i].d2_low_x      = d2p_lowx_model;
      d2_table[i].d2_high_x     = d2p_highx_model;

   }

   // -----------------------------------------------------------------------
   //
   TCanvas * c = new TCanvas();
   TGraphErrors  * gr_nacht = new TGraphErrors();
   TMultiGraph * mg = new TMultiGraph();
   for(unsigned int ig = 0; ig<8; ig++) {
     d2_integrands[ig]->SetLineColor(2000+ig/2);
     d2_integrands[ig]->SetMarkerColor(2000+ig/2);
     d2_integrands[ig]->SetMarkerStyle(20);
     d2_integrands[ig]->SetMarkerStyle(20+4*(ig%2));
     d2_integrands[ig]->SetLineWidth(3);

     d2_integrands_syst[ig]->SetLineColor(2000+ig/2);
     d2_integrands_syst[ig]->SetMarkerColor(2000+ig/2);
     d2_integrands_syst[ig]->SetMarkerStyle(20);
     d2_integrands_syst[ig]->SetLineWidth(3);
     d2_integrands_syst[ig]->SetFillColorAlpha(2000+ig/2,50);
     d2_integrands_syst[ig]->SetFillStyle(3005+2*(ig%2));
     
     mg->Add(d2_integrands[ig],"ep");
     mg->Add(d2_integrands_syst[ig],"e3");

   }
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetYaxis()->SetRangeUser(-0.5,0.5);
   mg->Draw("a");
   c->SaveAs("results/d2_extract/elt_measure_59_0.png");
   c->SaveAs("results/d2_extract/elt_measure_59_0.pdf");

   //TFile fout("d2_table_out.root","UPDATE");

   // -------------------
   c = new TCanvas();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mgtemp = 0;
   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   for(unsigned int ig = 0; ig<4; ig++) {
     auto p =  d2_table[ig];
     mg2->Add( mgtemp = CreateGraphPoints(p, 2000+ig) );
     if(ig==0) {
       leg->AddEntry(mgtemp->GetListOfGraphs()->At(4),"measured","lp");
       leg->AddEntry(mgtemp->GetListOfGraphs()->At(0),"low x","lp");
       leg->AddEntry(mgtemp->GetListOfGraphs()->At(1),"high x","lp");
       leg->AddEntry(mgtemp->GetListOfGraphs()->At(6),"OAR","lp");
     }
   }

   auto pdfs = new JAM15PolarizedPDFs();
   auto sfs  = new InSANEPolarizedStructureFunctionsFromPDFs(pdfs);

   TGraph * gr_elastic = new TGraph();
   TGraph * gr_jam     = new TGraph();
   //for(int i= 0; i<60;i++) {
   //  double q2 = 0.5+0.1*double(i);
   //  gr_elastic->SetPoint(i,q2, 2.0*ffs->d2p_el_Nachtmann(q2));
   //  gr_jam    ->SetPoint(i,q2, 2.0*sfs->M2n_TMC_p(3,q2,0.1,0.8));
   //}

   //gr_elastic->SetLineColor(1);
   //gr_elastic->SetLineWidth(2);
   //mg2->Add(gr_elastic,"l");

   //gr_jam->SetLineColor(2);
   //gr_jam->SetLineWidth(2);
   //gr_jam->SetLineStyle(2);
   //mg2->Add(gr_jam,"l");

   mg2->Draw("a");
   mg2->GetYaxis()->SetRangeUser(-0.06,0.06);
   mg2->GetXaxis()->SetLimits(0.5,6.0);
   leg->Draw();
   c->SaveAs("results/d2_extract/elt_measure_59_1.png");
   c->SaveAs("results/d2_extract/elt_measure_59_1.pdf");

   return 0;
}
