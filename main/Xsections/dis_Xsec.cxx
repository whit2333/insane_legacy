#include "Math/Functor.h"
Int_t dis_Xsec() {

   Double_t beamEnergy = 5.9;

   /// 1
     F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
     fDiffXSec->SetBeamEnergy(beamEnergy);
     fDiffXSec->InitializePhaseSpaceVariables();
     InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
     /// all the following cross sections share the same phase space. 
     ps->ListVariables();
     InSANEPhaseSpaceSampler *  fF1F2EventSampler = new InSANEPhaseSpaceSampler(fDiffXSec);

   /// 2
     InSANEInclusiveMottXSec * fMottDiffXSec = new InSANEInclusiveMottXSec();
     fMottDiffXSec->SetPhaseSpace(ps);
     fMottDiffXSec->SetBeamEnergy(beamEnergy);
     InSANEPhaseSpaceSampler *  fMottEventSampler = new InSANEPhaseSpaceSampler(fMottDiffXSec);

   /// 3
     InSANEFlatInclusiveDiffXSec * fFlatDiffXSec = new InSANEFlatInclusiveDiffXSec();
     fFlatDiffXSec->SetPhaseSpace(ps);
     fFlatDiffXSec->SetBeamEnergy(beamEnergy);
     InSANEPhaseSpaceSampler *  fFlatEventSampler = new InSANEPhaseSpaceSampler(fFlatDiffXSec);

   /// 4
     QFSInclusiveDiffXSec * fQFSDiffXSec= new QFSInclusiveDiffXSec();
     fQFSDiffXSec->SetPhaseSpace(ps);
     fQFSDiffXSec->SetBeamEnergy(beamEnergy);
     fQFSDiffXSec->Configure();
     InSANEPhaseSpaceSampler *  fQFSEventSampler = new InSANEPhaseSpaceSampler(fQFSDiffXSec);

   /// 5
//      InSANEPolarizedDiffXSec * fPolXSec = new InSANEPolarizedDiffXSec();
//      fPolXSec->SetUnpolarizedCrossSection(fQFSDiffXSec);
//      fPolXSec->SetPolarizedPositiveCrossSection(fDiffXSec);
//      fPolXSec->SetPolarizedNegativeCrossSection(fDiffXSec);
//      fPolXSec->SetPhaseSpace(fPolarizedPhaseSpace);
//      fPolXSec->SetBeamEnergy(4.9);
//      fPolXSec->SetChargeAsymmetry(-0.2);
//      fPolXSec->Refresh();
//      InSANEPhaseSpaceSampler *  fPolSampler = new InSANEPhaseSpaceSampler(fPolXSec);


  TCanvas * c = new TCanvas("CrossSections","Cross Sections");
  c->Divide(2,2);
   TH2F * h1 = new TH2F("F1F2test","F1F2 Test",100,0.8,4.9,100,20,50);
   TH2F * h2 = new TH2F("FlatTest","Flat Test",100,0.8,4.9,100,20,50);
   TH2F * h3 = new TH2F("MottTest","Mott Test",100,4.8,5.0,100,20,50);
   TH2F * h4 = new TH2F("QFSTest","QFS Test",100,0.8,4.9,100,20,50);

   h1->GetXaxis()->SetTitle("Energy GeV");
   h1->GetYaxis()->SetTitle("#theta_{e'}");
   h2->GetXaxis()->SetTitle("Energy GeV");
   h2->GetYaxis()->SetTitle("#theta_{e'}");
   h3->GetXaxis()->SetTitle("Energy GeV");
   h3->GetYaxis()->SetTitle("#theta_{e'}");
   h4->GetXaxis()->SetTitle("Energy GeV");
   h4->GetYaxis()->SetTitle("#theta_{e'}");
   Double_t *res;
  for(int i = 0; i< 100000;i++) {
/*    if(i%500 == 0 ) std::cout << " Generating Event " << i << "\n";*/
    res = fF1F2EventSampler->GenerateEvent();
//       std::cout << " F1F2 Xsec = " << fDiffXSec->EvaluateXSec(res) << " \n";
    h1->Fill(res[0],res[1]*180.0/TMath::Pi());
    res = fMottEventSampler->GenerateEvent();
/*      std::cout << " Mott Xsec = " << fMottDiffXSec->EvaluateXSec(res) <<" \n";*/
    h3->Fill(fMottDiffXSec->GetEPrime(res[1]),res[1]*180.0/TMath::Pi());
    res = fFlatEventSampler->GenerateEvent();
//       std::cout << " Flat Xsec = " << fPolXSec->EvaluateXSec(res) <<" \n";
    h2->Fill(res[0],res[1]*180.0/TMath::Pi());

    res = fQFSEventSampler->GenerateEvent();
/*      std::cout << " QFS Xsec = " << fQFSDiffXSec->EvaluateXSec(res) <<" (pb?)\n";*/
    h4->Fill(res[0],res[1]*180.0/TMath::Pi());
  }
   c->cd(1);
   h1->Draw("COLZ");
   c->cd(2);
   h2->Draw("COLZ");
   c->cd(3);
   h3->Draw("COLZ");
   c->cd(4);
   h4->Draw("COLZ");

return(0);
}