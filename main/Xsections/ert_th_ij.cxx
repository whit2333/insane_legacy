

Int_t ert_th_ij(){

	Double_t beamEnergy  = 5.9;
	Double_t theta       = 45.0*degree;//29.0*TMath::Pi()/180.0;
	Double_t Eprime      = beamEnergy*(1.-0.869);

	InSANEPOLRADElasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADElasticTailDiffXSec();
	fDiffXSec4->SetBeamEnergy(beamEnergy);
	fDiffXSec4->GetPOLRAD()->SetTargetType(0);
	fDiffXSec4->GetPOLRAD()->SetHelicity(-1.);
	fDiffXSec4->InitializePhaseSpaceVariables();
	fDiffXSec4->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
	//ps4->ListVariables();
	Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e4 =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e4 =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e4) = Eprime;
	(*theta_e4) = theta;//45.0*TMath::Pi()/180.0;
	(*phi_e4) = 0.0*TMath::Pi()/180.0;
	fDiffXSec4->EvaluateCurrent();
	//fDiffXSec4->Print();
	//fDiffXSec4->PrintTable();

	Int_t    npar     = 2;
	Double_t Emin     = 0.5;
	Double_t Emax     = 4.5;
	Double_t minTheta = 0.0175*15.0;
	Double_t maxTheta = 0.0175*55.0;

	InSANEPOLRAD * polrad = fDiffXSec4->GetPOLRAD();
	Double_t tau_min = polrad->GetKinematics()->GetTau_A_min();
	Double_t tau_max = polrad->GetKinematics()->GetTau_A_max();

	// cout << "tau min = " << tau_min << endl;
	// cout << "tau max = " << tau_max << endl;
	// exit(1); 

	Double_t xbj = polrad->GetKinematics()->Getx();
	Double_t Q2  = polrad->GetKinematics()->GetQ2();
	Double_t Mp  = polrad->GetKinematics()->GetM();
	Double_t W2  = polrad->GetKinematics()->GetW2();
	Double_t nu  = polrad->GetKinematics()->Gety()*beamEnergy;
	Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
	Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
	Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
	Double_t tau_u = 0.67;
	Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

	//polrad->GetKinematics()->Print();
	std::cout << "tau_min = " << tau_min << "\n";
	std::cout << "tau_max = " << tau_max << "\n";
	std::cout << "Q2 = " << Q2 << "\n";
	std::cout << "W2 = " << W2 << "\n";
	std::cout << "Mp = " << Mp << "\n";
	std::cout << "xbj = " << xbj << "\n";
	std::cout << "tau_me_peak = " << tau_me_peak << "\n";
	std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
	std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
	std::cout << "t = " << t << "\n";

//           1           1
//           1           2
//           1           3
//           2           1
//           2           2
//           2           3
//           3           1
//           3           2
//           3           3
//           3           4
//           4           1
//           4           2
//           4           3
//           4           4
//           4           5
//           5           1
//           5           2
//           5           3
//           5           4
//           5           5
//           6           1
//           6           2
//           6           3
//           6           4
//           6           5
//           7           1
//           7           2
//           7           3
//           8           1
//           8           2
//           8           3
//           8           4

	vector<double> tau11,th11;
	vector<double> tau12,th12;
	vector<double> tau13,th13;

	vector<double> tau21,th21;
	vector<double> tau22,th22;
	vector<double> tau23,th23;

	vector<double> tau31,th31;
	vector<double> tau32,th32;
	vector<double> tau33,th33;
	vector<double> tau34,th34;

	vector<double> tau41,th41;
	vector<double> tau42,th42;
	vector<double> tau43,th43;
	vector<double> tau44,th44;

	vector<double> tau51,th51;
	vector<double> tau52,th52;
	vector<double> tau53,th53;
	vector<double> tau54,th54;
	vector<double> tau55,th55;

	vector<double> tau61,th61;
	vector<double> tau62,th62;
	vector<double> tau63,th63;
	vector<double> tau64,th64;
	vector<double> tau65,th65;

	vector<double> tau71,th71;
	vector<double> tau72,th72;
	vector<double> tau73,th73;

	vector<double> tau81,th81;
	vector<double> tau82,th82;
	vector<double> tau83,th83;
	vector<double> tau84,th84;

	ImportThData(1,1,tau11,th11);
	ImportThData(1,2,tau12,th12);
	ImportThData(1,3,tau13,th13);

	ImportThData(2,1,tau21,th21);
	ImportThData(2,2,tau22,th22);
	ImportThData(2,3,tau23,th23);

	ImportThData(3,1,tau31,th31);
	ImportThData(3,2,tau32,th32);
	ImportThData(3,3,tau33,th33);
	ImportThData(3,4,tau34,th34);

	ImportThData(4,1,tau41,th41);
	ImportThData(4,2,tau42,th42);
	ImportThData(4,3,tau43,th43);
	ImportThData(4,4,tau44,th44);

	ImportThData(5,1,tau51,th51);
	ImportThData(5,2,tau52,th52);
	ImportThData(5,3,tau53,th53);
	ImportThData(5,4,tau54,th54);
	ImportThData(5,5,tau55,th55);

	ImportThData(6,1,tau61,th61);
	ImportThData(6,2,tau62,th62);
	ImportThData(6,3,tau63,th63);
	ImportThData(6,4,tau64,th64);
	ImportThData(6,5,tau65,th65);

	ImportThData(7,1,tau71,th71);
	ImportThData(7,2,tau72,th72);
	ImportThData(7,3,tau73,th73);

	ImportThData(8,1,tau81,th81);
	ImportThData(8,2,tau82,th82);
	ImportThData(8,3,tau83,th83);
	ImportThData(8,4,tau84,th84);

	Int_t width = 2; 

	TGraph *g11      = GetTGraph(tau11,th11);
	TGraph *g12      = GetTGraph(tau12,th12);
	TGraph *g13      = GetTGraph(tau13,th13);

	TGraph *g21      = GetTGraph(tau21,th21);
	TGraph *g22      = GetTGraph(tau22,th22);
	TGraph *g23      = GetTGraph(tau23,th23);

	TGraph *g31      = GetTGraph(tau31,th31);
	TGraph *g32      = GetTGraph(tau32,th32);
	TGraph *g33      = GetTGraph(tau33,th33);
	TGraph *g34      = GetTGraph(tau34,th34);

	TGraph *g41      = GetTGraph(tau41,th41);
	TGraph *g42      = GetTGraph(tau42,th42);
	TGraph *g43      = GetTGraph(tau43,th43);
	TGraph *g44      = GetTGraph(tau44,th44);

	TGraph *g51      = GetTGraph(tau51,th51);
	TGraph *g52      = GetTGraph(tau52,th52);
	TGraph *g53      = GetTGraph(tau53,th53);
	TGraph *g54      = GetTGraph(tau54,th54);
	TGraph *g55      = GetTGraph(tau55,th55);

	TGraph *g61      = GetTGraph(tau61,th61);
	TGraph *g62      = GetTGraph(tau62,th62);
	TGraph *g63      = GetTGraph(tau63,th63);
	TGraph *g64      = GetTGraph(tau64,th64);
	TGraph *g65      = GetTGraph(tau65,th65);

	TGraph *g71      = GetTGraph(tau71,th71);
	TGraph *g72      = GetTGraph(tau72,th72);
	TGraph *g73      = GetTGraph(tau73,th73);

	TGraph *g81      = GetTGraph(tau81,th81);
	TGraph *g82      = GetTGraph(tau82,th82);
	TGraph *g83      = GetTGraph(tau83,th83);
	TGraph *g84      = GetTGraph(tau84,th84);

	g11->SetLineWidth(width);

	TF1 * T11 = new TF1("T11", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T11->SetParameter(0,1);
	T11->SetParameter(1,1);
	T11->SetNpx(5000);
	T11->SetLineColor(kMagenta);
	T11->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T12 = new TF1("T12", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T12->SetParameter(0,1);
	T12->SetParameter(1,2);
	T12->SetNpx(5000);
	T12->SetLineColor(kMagenta);
	T12->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T13 = new TF1("T13", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T13->SetParameter(0,1);
	T13->SetParameter(1,3);
	T13->SetNpx(5000);
	T13->SetLineColor(kMagenta);
	T13->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T21 = new TF1("T21", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T21->SetParameter(0,2);
	T21->SetParameter(1,1);
	T21->SetNpx(5000);
	T21->SetLineColor(kMagenta);
	T21->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T22 = new TF1("T22", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T22->SetParameter(0,2);
	T22->SetParameter(1,2);
	T22->SetNpx(5000);
	T22->SetLineColor(kMagenta);
	T22->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T23 = new TF1("T23", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T23->SetParameter(0,2);
	T23->SetParameter(1,3);
	T23->SetNpx(5000);
	T23->SetLineColor(kMagenta);
	T23->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T31 = new TF1("T31", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T31->SetParameter(0,3);
	T31->SetParameter(1,1);
	T31->SetNpx(5000);
	T31->SetLineColor(kMagenta);
	T31->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();
	// T311->DrawCopy();

	TF1 * T32 = new TF1("T32", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T32->SetParameter(0,3);
	T32->SetParameter(1,2);
	T32->SetNpx(5000);
	T32->SetLineColor(kMagenta);
	T32->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();
	// T321->DrawCopy();

	TF1 * T33 = new TF1("T33", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T33->SetParameter(0,3);
	T33->SetParameter(1,3);
	T33->SetNpx(5000);
	T33->SetLineColor(kMagenta);
	T33->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();
	// T331->DrawCopy();

	TF1 * T34 = new TF1("T34", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T34->SetParameter(0,3);
	T34->SetParameter(1,4);
	T34->SetNpx(5000);
	T34->SetLineColor(kMagenta);
	T34->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();
	// T341->DrawCopy();

	TF1 * T41 = new TF1("T41", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T41->SetParameter(0,4);
	T41->SetParameter(1,1);
	T41->SetNpx(5000);
	T41->SetLineColor(kMagenta);
	T41->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T42 = new TF1("T42", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T42->SetParameter(0,4);
	T42->SetParameter(1,2);
	T42->SetNpx(5000);
	T42->SetLineColor(kMagenta);
	T42->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T43 = new TF1("T43", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T43->SetParameter(0,4);
	T43->SetParameter(1,3);
	T43->SetNpx(5000);
	T43->SetLineColor(kMagenta);
	T43->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T44 = new TF1("T44", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T44->SetParameter(0,4);
	T44->SetParameter(1,4);
	T44->SetNpx(5000);
	T44->SetLineColor(kMagenta);
	T44->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T51 = new TF1("T51", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T51->SetParameter(0,5);
	T51->SetParameter(1,1);
	T51->SetNpx(5000);
	T51->SetLineColor(kMagenta);
	T51->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T52 = new TF1("T52", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T52->SetParameter(0,5);
	T52->SetParameter(1,2);
	T52->SetNpx(5000);
	T52->SetLineColor(kMagenta);
	T52->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T53 = new TF1("T53", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T53->SetParameter(0,5);
	T53->SetParameter(1,3);
	T53->SetNpx(5000);
	T53->SetLineColor(kMagenta);
	T53->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T54 = new TF1("T54", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T54->SetParameter(0,5);
	T54->SetParameter(1,4);
	T54->SetNpx(5000);
	T54->SetLineColor(kMagenta);
	T54->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T55 = new TF1("T55", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T55->SetParameter(0,5);
	T55->SetParameter(1,5);
	T55->SetNpx(5000);
	T55->SetLineColor(kMagenta);
	T55->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T61 = new TF1("T61", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T61->SetParameter(0,6);
	T61->SetParameter(1,1);
	T61->SetNpx(5000);
	T61->SetLineColor(kMagenta);
	T61->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T62 = new TF1("T62", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T62->SetParameter(0,6);
	T62->SetParameter(1,2);
	T62->SetNpx(5000);
	T62->SetLineColor(kMagenta);
	T62->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T63 = new TF1("T63", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T63->SetParameter(0,6);
	T63->SetParameter(1,3);
	T63->SetNpx(5000);
	T63->SetLineColor(kMagenta);
	T63->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T64 = new TF1("T64", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T64->SetParameter(0,6);
	T64->SetParameter(1,4);
	T64->SetNpx(5000);
	T64->SetLineColor(kMagenta);
	T64->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T65 = new TF1("T65", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T65->SetParameter(0,6);
	T65->SetParameter(1,5);
	T65->SetNpx(5000);
	T65->SetLineColor(kMagenta);
	T65->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T71 = new TF1("T71", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 3,"InSANEPOLRAD","THETAij_A_Plot");
	T71->SetParameter(0,7);
	T71->SetParameter(1,1);
	T71->SetNpx(5000);
	T71->SetLineColor(kMagenta);
	T71->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T72 = new TF1("T72", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T72->SetParameter(0,7);
	T72->SetParameter(1,2);
	T72->SetNpx(5000);
	T72->SetLineColor(kMagenta);
	T72->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T73 = new TF1("T73", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T73->SetParameter(0,7);
	T73->SetParameter(1,3);
	T73->SetNpx(5000);
	T73->SetLineColor(kMagenta);
	T73->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T81 = new TF1("T81", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T81->SetParameter(0,8);
	T81->SetParameter(1,1);
	T81->SetParameter(2,1);
	T81->SetNpx(5000);
	T81->SetLineColor(kMagenta);
	T81->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T82 = new TF1("T82", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T82->SetParameter(0,8);
	T82->SetParameter(1,2);
	T82->SetParameter(2,1);
	T82->SetNpx(5000);
	T82->SetLineColor(kMagenta);
	T82->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T83 = new TF1("T83", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T83->SetParameter(0,8);
	T83->SetParameter(1,3);
	T83->SetNpx(5000);
	T83->SetLineColor(kMagenta);
	T83->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TF1 * T84 = new TF1("T84", polrad, &InSANEPOLRAD::THETAij_A_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","THETAij_A_Plot");
	T84->SetParameter(0,8);
	T84->SetParameter(1,4);
	T84->SetNpx(5000);
	T84->SetLineColor(kMagenta);
	T84->SetLineWidth(width);
	fDiffXSec4->EvaluateCurrent();

	TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
	Leg->SetFillColor(kWhite); 
	Leg->AddEntry(g11 ,"Fortran","l");
	Leg->AddEntry(T11,"C++"    ,"l");

	TString DrawOption = Form("AP"); 
	TString xAxisTitle = Form("#tau");

	Double_t min = -1E+6; 
	Double_t max =  1E+6; 

	TCanvas *c1 = new TCanvas("c1","Theta i = 1 and 2",1000,800);
	c1->SetFillColor(kWhite);
	c1->Divide(3,2); 

        min = GetMin(th11);
        max = GetMax(th11);

	c1->cd(1); 
	g11->Draw(DrawOption);
	g11->SetTitle("#theta_{11}");
	g11->GetXaxis()->SetTitle(xAxisTitle);
	g11->GetXaxis()->CenterTitle();
        g11->GetYaxis()->SetRangeUser(min,max);
	g11->Draw(DrawOption);
	T11->Draw("same");
	Leg->Draw("same");
	c1->Update();

        min = GetMin(th12);
        max = GetMax(th12);

	c1->cd(2); 
	g12->Draw(DrawOption);
	g12->SetTitle("#theta_{12}");
	g12->GetXaxis()->SetTitle(xAxisTitle);
	g12->GetXaxis()->CenterTitle();
        g12->GetYaxis()->SetRangeUser(min,max);
	g12->Draw(DrawOption);
	T12->Draw("same");
	c1->Update();

        min = GetMin(th13);
        max = GetMax(th13);

	c1->cd(3); 
	g13->Draw(DrawOption);
	g13->SetTitle("#theta_{13}");
	g13->GetXaxis()->SetTitle(xAxisTitle);
	g13->GetXaxis()->CenterTitle();
        g13->GetYaxis()->SetRangeUser(min,max);
	g13->Draw(DrawOption);
	T13->Draw("same");
	c1->Update();

        min = GetMin(th21);
        max = GetMax(th21);

	c1->cd(4); 
	g21->Draw(DrawOption);
	g21->SetTitle("#theta_{21}");
	g21->GetXaxis()->SetTitle(xAxisTitle);
	g21->GetXaxis()->CenterTitle();
        g21->GetYaxis()->SetRangeUser(min,max);
	g21->Draw(DrawOption);
	T21->Draw("same");
	c1->Update();

        min = GetMin(th22);
        max = GetMax(th22);

	c1->cd(5); 
	g22->Draw(DrawOption);
	g22->SetTitle("#theta_{22}");
	g22->GetXaxis()->SetTitle(xAxisTitle);
	g22->GetXaxis()->CenterTitle();
        g22->GetYaxis()->SetRangeUser(min,max);
	g22->Draw(DrawOption);
	T22->Draw("same");
	c1->Update();

        min = GetMin(th23);
        max = GetMax(th23);

	c1->cd(6); 
	g23->Draw(DrawOption);
	g23->SetTitle("#theta_{23}");
	g23->GetXaxis()->SetTitle(xAxisTitle);
	g23->GetXaxis()->CenterTitle();
        g23->GetYaxis()->SetRangeUser(min,max);
	g23->Draw(DrawOption);
	T23->Draw("same");
	c1->Update();

	TCanvas *c2 = new TCanvas("c2","Theta i = 3 and 4",1000,800);
	c2->SetFillColor(kWhite);
	c2->Divide(4,2); 

        min = GetMin(th31);
        max = GetMax(th31);

	c2->cd(1); 
	g31->Draw(DrawOption);
	g31->SetTitle("#theta_{31}");
	g31->GetXaxis()->SetTitle(xAxisTitle);
	g31->GetXaxis()->CenterTitle();
        g31->GetYaxis()->SetRangeUser(min,max);
	g31->Draw(DrawOption);
	T31->Draw("same");
	Leg->Draw("same");
	c2->Update();

        min = GetMin(th32);
        max = GetMax(th32);

	c2->cd(2); 
	g32->Draw(DrawOption);
	g32->SetTitle("#theta_{32}");
	g32->GetXaxis()->SetTitle(xAxisTitle);
	g32->GetXaxis()->CenterTitle();
        g32->GetYaxis()->SetRangeUser(min,max);
	g32->Draw(DrawOption);
	T32->Draw("same");
	c2->Update();

        min = GetMin(th33);
        max = GetMax(th33);

	c2->cd(3); 
	g33->Draw(DrawOption);
	g33->SetTitle("#theta_{33}");
	g33->GetXaxis()->SetTitle(xAxisTitle);
	g33->GetXaxis()->CenterTitle();
        g33->GetYaxis()->SetRangeUser(min,max);
	g33->Draw(DrawOption);
	T33->Draw("same");
	c2->Update();

        min = GetMin(th34);
        max = GetMax(th34);

	c2->cd(4); 
	g34->Draw(DrawOption);
	g34->SetTitle("#theta_{34}");
	g34->GetXaxis()->SetTitle(xAxisTitle);
	g34->GetXaxis()->CenterTitle();
        g34->GetYaxis()->SetRangeUser(min,max);
	g34->Draw(DrawOption);
	T34->Draw("same");
	c2->Update();

        min = GetMin(th41);
        max = GetMax(th41);

	c2->cd(5); 
	g41->Draw(DrawOption);
	g41->SetTitle("#theta_{41}");
	g41->GetXaxis()->SetTitle(xAxisTitle);
	g41->GetXaxis()->CenterTitle();
        g41->GetYaxis()->SetRangeUser(min,max);
	g41->Draw(DrawOption);
	T41->Draw("same");
	c2->Update();

        min = GetMin(th42);
        max = GetMax(th42);

	c2->cd(6); 
	g42->Draw(DrawOption);
	g42->SetTitle("#theta_{42}");
	g42->GetXaxis()->SetTitle(xAxisTitle);
	g42->GetXaxis()->CenterTitle();
        g42->GetYaxis()->SetRangeUser(min,max);
	g42->Draw(DrawOption);
	T42->Draw("same");
	c2->Update();

        min = GetMin(th43);
        max = GetMax(th43);

	c2->cd(7); 
	g43->Draw(DrawOption);
	g43->SetTitle("#theta_{43}");
	g43->GetXaxis()->SetTitle(xAxisTitle);
	g43->GetXaxis()->CenterTitle();
        g43->GetYaxis()->SetRangeUser(min,max);
	g43->Draw(DrawOption);
	T43->Draw("same");
	c1->Update();

        min = GetMin(th44);
        max = GetMax(th44);

	c2->cd(8); 
	g44->Draw(DrawOption);
	g44->SetTitle("#theta_{44}");
	g44->GetXaxis()->SetTitle(xAxisTitle);
	g44->GetXaxis()->CenterTitle();
        g44->GetYaxis()->SetRangeUser(min,max);
	g44->Draw(DrawOption);
	T44->Draw("same");
	c2->Update();

	TCanvas *c3 = new TCanvas("c3","Theta i = 5 and 6",1000,800);
	c3->SetFillColor(kWhite);
	c3->Divide(5,2); 

        min = GetMin(th51);
        max = GetMax(th51);

	c3->cd(1); 
	g51->Draw(DrawOption);
	g51->SetTitle("#theta_{51}");
	g51->GetXaxis()->SetTitle(xAxisTitle);
	g51->GetXaxis()->CenterTitle();
        g51->GetYaxis()->SetRangeUser(min,max);
	g51->Draw(DrawOption);
	T51->Draw("same");
	Leg->Draw("same");
	c3->Update();

        min = GetMin(th52);
        max = GetMax(th52);

	c3->cd(2); 
	g52->Draw(DrawOption);
	g52->SetTitle("#theta_{52}");
	g52->GetXaxis()->SetTitle(xAxisTitle);
	g52->GetXaxis()->CenterTitle();
        g52->GetYaxis()->SetRangeUser(min,max);
	g52->Draw(DrawOption);
	T52->Draw("same");
	c3->Update();

        min = GetMin(th53);
        max = GetMax(th53);

	c3->cd(3); 
	g53->Draw(DrawOption);
	g53->SetTitle("#theta_{53}");
	g53->GetXaxis()->SetTitle(xAxisTitle);
	g53->GetXaxis()->CenterTitle();
        g53->GetYaxis()->SetRangeUser(min,max);
	g53->Draw(DrawOption);
	T53->Draw("same");
	c3->Update();

        min = GetMin(th54);
        max = GetMax(th54);

	c3->cd(4); 
	g54->Draw(DrawOption);
	g54->SetTitle("#theta_{54}");
	g54->GetXaxis()->SetTitle(xAxisTitle);
	g54->GetXaxis()->CenterTitle();
        g54->GetYaxis()->SetRangeUser(min,max);
	g54->Draw(DrawOption);
	T54->Draw("same");
	c3->Update();

        min = GetMin(th55);
        max = GetMax(th55);

	c3->cd(5); 
	g55->Draw(DrawOption);
	g55->SetTitle("#theta_{55}");
	g55->GetXaxis()->SetTitle(xAxisTitle);
	g55->GetXaxis()->CenterTitle();
        g55->GetYaxis()->SetRangeUser(min,max);
	g55->Draw(DrawOption);
	T55->Draw("same");
	c3->Update();

        min = GetMin(th61);
        max = GetMax(th61);

	c3->cd(6); 
	g61->Draw(DrawOption);
	g61->SetTitle("#theta_{61}");
	g61->GetXaxis()->SetTitle(xAxisTitle);
	g61->GetXaxis()->CenterTitle();
        g61->GetYaxis()->SetRangeUser(min,max);
	g61->Draw(DrawOption);
	T61->Draw("same");
	c3->Update();

        min = GetMin(th62);
        max = GetMax(th62);

	c3->cd(7); 
	g62->Draw(DrawOption);
	g62->SetTitle("#theta_{62}");
	g62->GetXaxis()->SetTitle(xAxisTitle);
	g62->GetXaxis()->CenterTitle();
        g62->GetYaxis()->SetRangeUser(min,max);
	g62->Draw(DrawOption);
	T62->Draw("same");
	c1->Update();

        min = GetMin(th63);
        max = GetMax(th63);

	c3->cd(8); 
	g63->Draw(DrawOption);
	g63->SetTitle("#theta_{63}");
	g63->GetXaxis()->SetTitle(xAxisTitle);
	g63->GetXaxis()->CenterTitle();
        g63->GetYaxis()->SetRangeUser(min,max);
	g63->Draw(DrawOption);
	T63->Draw("same");
	c3->Update();

        min = GetMin(th64);
        max = GetMax(th64);

	c3->cd(9); 
	g64->Draw(DrawOption);
	g64->SetTitle("#theta_{64}");
	g64->GetXaxis()->SetTitle(xAxisTitle);
	g64->GetXaxis()->CenterTitle();
        g64->GetYaxis()->SetRangeUser(min,max);
	g64->Draw(DrawOption);
	T64->Draw("same");
	c3->Update();

        min = GetMin(th65);
        max = GetMax(th65);

	c3->cd(10); 
	g65->Draw(DrawOption);
	g65->SetTitle("#theta_{65}");
	g65->GetXaxis()->SetTitle(xAxisTitle);
	g65->GetXaxis()->CenterTitle();
        g65->GetYaxis()->SetRangeUser(min,max);
	g65->Draw(DrawOption);
	T65->Draw("same");
	c3->Update();

	TCanvas *c4 = new TCanvas("c4","Theta i = 7 and 8",1000,800);
	c4->SetFillColor(kWhite);
	c4->Divide(4,2); 

        min = GetMin(th71);
        max = GetMax(th71);

	c4->cd(1); 
	g71->Draw(DrawOption);
	g71->SetTitle("#theta_{71}");
	g71->GetXaxis()->SetTitle(xAxisTitle);
	g71->GetXaxis()->CenterTitle();
	g71->GetYaxis()->SetRangeUser(min,max);
	g71->Draw(DrawOption);
	T71->Draw("same");
	c4->Update();

        min = GetMin(th72);
        max = GetMax(th72);

	c4->cd(2); 
	g72->Draw(DrawOption);
	g72->SetTitle("#theta_{72}");
	g72->GetXaxis()->SetTitle(xAxisTitle);
	g72->GetXaxis()->CenterTitle();
	g72->GetYaxis()->SetRangeUser(min,max);
	g72->Draw(DrawOption);
	T72->Draw("same");
	c4->Update();

        min = GetMin(th73);
        max = GetMax(th73);

	c4->cd(3); 
	g73->Draw(DrawOption);
	g73->SetTitle("#theta_{73}");
	g73->GetXaxis()->SetTitle(xAxisTitle);
	g73->GetXaxis()->CenterTitle();
	g73->GetYaxis()->SetRangeUser(min,max);
	g73->Draw(DrawOption);
	T73->Draw("same");
	c4->Update();

        min = GetMin(th81);
        max = GetMax(th81);

	c4->cd(4); 
	g81->Draw(DrawOption);
	g81->SetTitle("#theta_{81}");
	g81->GetXaxis()->SetTitle(xAxisTitle);
	g81->GetXaxis()->CenterTitle();
	g81->GetYaxis()->SetRangeUser(min,max);
	g81->Draw(DrawOption);
	T81->Draw("same");
	c4->Update();

        min = GetMin(th82);
        max = GetMax(th82);

	c4->cd(5); 
	g82->Draw(DrawOption);
	g82->SetTitle("#theta_{82}");
	g82->GetXaxis()->SetTitle(xAxisTitle);
	g82->GetXaxis()->CenterTitle();
	g82->GetYaxis()->SetRangeUser(min,max);
	g82->Draw(DrawOption);
	T82->Draw("same");
	c4->Update();

        min = GetMin(th83);
        max = GetMax(th83);

	c4->cd(6); 
	g83->Draw(DrawOption);
	g83->SetTitle("#theta_{83}");
	g83->GetXaxis()->SetTitle(xAxisTitle);
	g83->GetXaxis()->CenterTitle();
	g83->GetYaxis()->SetRangeUser(min,max);
	g83->Draw(DrawOption);
	T83->Draw("same");
	c4->Update();

        min = GetMin(th84);
        max = GetMax(th84);

	c4->cd(7); 
	g84->Draw(DrawOption);
	g84->SetTitle("#theta_{84}");
	g84->GetXaxis()->SetTitle(xAxisTitle);
	g84->GetXaxis()->CenterTitle();
	g84->GetYaxis()->SetRangeUser(min,max);
	g84->Draw(DrawOption);
	T84->Draw("same");
	c4->Update();

        c4->cd(8); 
	Leg->Draw("same");
	c4->Update();

	fDiffXSec4->GetPOLRAD()->Print();
	fDiffXSec4->GetPOLRAD()->GetKinematics()->Print();

	return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
	g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportThData(int i,int j,vector<double> &tau,vector<double> &th){

	double itau,ith;
	TString inpath = Form("./theta/ij/th_%d_%d.dat",i,j);

	ifstream infile;
	infile.open(inpath);
	if(infile.fail()){
		cout << "Cannot open the file: " << inpath << endl;
		exit(1);
	}else{
		while(!infile.eof()){
			infile >> itau >> ith;
			tau.push_back(itau);
			th.push_back(ith);
		}
		infile.close();
		tau.pop_back();
		th.pop_back();
	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
	min -= 1E+2; 
 	// if(min<-1E+2){
	// 	min -= 1E+4;
	// }else{
	// 	min -= 1E+2; 
        // } 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	max += 1E+2; 
	// if(max>1E+2){
	// 	max += 1E+4;
	// }else{
	// 	max += 1E+2; 
        // } 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}

