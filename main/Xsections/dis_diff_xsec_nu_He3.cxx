Int_t dis_diff_xsec_nu_He3(){

   Double_t beamEnergy =  5.0;
   Double_t theta      =  23.9*TMath::Pi()/180.0;
   Double_t Eprime     =  1.0;

   /// F1F209 cross section which includes a QRT (and possibly ERT?)
   F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetZ(2);
   fDiffXSec->SetA(1);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
   Double_t * energy_e           = ps->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e            = ps->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e              = ps->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e)                   = Eprime;
   (*theta_e)                    = theta;
   (*phi_e)                      = 0.0*TMath::Pi()/180.0;
   fDiffXSec->EvaluateCurrent();

   /// Born Cross section
   InSANEPOLRADBornDiffXSec * fDiffXSec2 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->GetPOLRAD()->SetTargetType(3);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps2          = fDiffXSec2->GetPhaseSpace();
   Double_t * energy_e2           = ps2->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e2            = ps2->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e2              = ps2->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e2)                   = Eprime;
   (*theta_e2)                    = theta;///45.0*TMath::Pi()/180.0;
   (*phi_e2)                      = 0.0*TMath::Pi()/180.0;
   fDiffXSec2->EvaluateCurrent();

   /// Elastic Radiative Tail
   InSANEPOLRADElasticTailDiffXSec * fDiffXSec3 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   fDiffXSec3->GetPOLRAD()->SetTargetType(3);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps3          = fDiffXSec3->GetPhaseSpace();
   Double_t * energy_e3           = ps3->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e3            = ps3->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e3              = ps3->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e3)                   = Eprime;
   (*theta_e3)                    = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e3)                      = 0.0*TMath::Pi()/180.0;
   fDiffXSec3->EvaluateCurrent();

   /// Quasielastic Radiative Tail
   InSANEPOLRADQuasiElasticTailDiffXSec * fDiffXSec6 = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   fDiffXSec6->SetBeamEnergy(beamEnergy);
   fDiffXSec6->GetPOLRAD()->SetTargetType(3);
   fDiffXSec6->InitializePhaseSpaceVariables();
   fDiffXSec6->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps6          = fDiffXSec3->GetPhaseSpace();
   Double_t * energy_e6           = ps6->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e6            = ps6->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e6              = ps6->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e6)                   = Eprime;
   (*theta_e6)                    = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e6)                      = 0.0*TMath::Pi()/180.0;
   fDiffXSec6->EvaluateCurrent();

   /// Inelastic Radiative Tail
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->GetPOLRAD()->SetTargetType(3);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4          = fDiffXSec4->GetPhaseSpace();
   Double_t * energy_e4           = ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4            = ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4              = ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4)                   = Eprime;
   (*theta_e4)                    = theta;
   (*phi_e4)                      = 0.0*TMath::Pi()/180.0;
   fDiffXSec4->EvaluateCurrent();

   //-------------------------------------------
   TCanvas * c = new TCanvas("dis_diff_xsec_nu_He3","Polrad 3He");

   // Parameters here are (theta,phi)
   Int_t    npar     = 2;
   Double_t nu_min   = 1.0;
   Double_t nu_max   = beamEnergy-1.0;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::PhotonEnergyDependentXSec, 
               nu_min, nu_max, npar,"InSANEInclusiveDiffXSec","PhotonEnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,0.0);
   sigmaE->SetLineColor(kRed);


   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::PhotonEnergyDependentXSec, 
               nu_min, nu_max, npar,"InSANEInclusiveDiffXSec","PhotonEnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);//30.0*TMath::Pi()/180.0);
   sigmaE2->SetParameter(1,0.0);
   sigmaE2->SetLineColor(kBlue);

   TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::PhotonEnergyDependentXSec, 
               nu_min, nu_max, npar,"InSANEInclusiveDiffXSec","PhotonEnergyDependentXSec");
   sigmaE3->SetParameter(0,theta);//30.0*TMath::Pi()/180.0);
   sigmaE3->SetParameter(1,0.0);
   sigmaE3->SetLineColor(kBlue-9);


   TF1 * sigmaE4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::PhotonEnergyDependentXSec, 
               nu_min, nu_max, npar,"InSANEInclusiveDiffXSec","PhotonEnergyDependentXSec");
   sigmaE4->SetParameter(0,theta);//30.0*TMath::Pi()/180.0);
   sigmaE4->SetParameter(1,0.0);
   sigmaE4->SetLineColor(kGreen-9);

   TF1 * sigmaE6 = new TF1("sigma6", fDiffXSec6, &InSANEInclusiveDiffXSec::PhotonEnergyDependentXSec, 
               nu_min, nu_max, npar,"InSANEInclusiveDiffXSec","PhotonEnergyDependentXSec");
   sigmaE6->SetParameter(0,theta);//30.0*TMath::Pi()/180.0);
   sigmaE6->SetParameter(1,0.0);
   sigmaE6->SetLineColor(kCyan-9);

   //-------------------------------------------

   sigmaE2->SetMinimum(1e-15);
   sigmaE2->SetMaximum(1e3);

   gPad->SetLogy(true);
   /*
   sigmaE->Draw();
   sigmaE->GetXaxis()->SetTitle("#nu");
   sigmaE->GetXaxis()->CenterTitle(true);
   sigmaE->GetYaxis()->SetTitle(fDiffXSec->GetLabel());
   sigmaE->GetYaxis()->CenterTitle(true);
   */
   sigmaE2->Draw();
   //sigmaE2->Draw("same");
   sigmaE3->Draw("same");
   sigmaE4->Draw("same");
   sigmaE6->Draw("same");

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);

   Leg->SetFillColor(kWhite);
   Leg->AddEntry(sigmaE,"F1F209","l");
   Leg->AddEntry(sigmaE2,"InSANE - POLRAD","l");
   Leg->AddEntry(sigmaE3,"ERT","l");
   Leg->AddEntry(sigmaE4,"IRT","l");
   Leg->AddEntry(sigmaE6,"QRT","l");
   Leg->Draw("same");

   c->SaveAs(Form("results/dis_diff_xsec_nu_He3.png"));

   return(0);
}
