{

   TFile * f = new TFile("testXSec.root","UPDATE");  

   InSANEEventGenerator * evGen = new InSANEEventGenerator();
     
   Double_t fBeamEnergy=5.9;

   /// Neutral Pion
   InSANEInclusiveWiserXSec * fDiffXSec1 = new InSANEInclusiveWiserXSec();
   fDiffXSec1->SetBeamEnergy(fBeamEnergy);
   fDiffXSec1->SetProductionParticleType(111);
   fDiffXSec1->InitPhaseSpace();
   fDiffXSec1->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler *  pi0EventSampler = new InSANEPhaseSpaceSampler(fDiffXSec1);
   evGen->AddSampler(pi0EventSampler);

   /// Positive Pion
   InSANEInclusiveWiserXSec * fDiffXSec2 = new InSANEInclusiveWiserXSec();
   fDiffXSec2->SetBeamEnergy(fBeamEnergy);
   fDiffXSec2->SetProductionParticleType(211);
   fDiffXSec2->InitPhaseSpace();
   fDiffXSec2->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler *  pi0EventSampler2 = new InSANEPhaseSpaceSampler(fDiffXSec2);
   evGen->AddSampler(pi0EventSampler2);

   /// Negative Pion
   InSANEInclusiveWiserXSec * fDiffXSec3 = new InSANEInclusiveWiserXSec();
   fDiffXSec3->SetBeamEnergy(fBeamEnergy);
   fDiffXSec3->SetProductionParticleType(-211);
   fDiffXSec3->InitPhaseSpace();
   fDiffXSec3->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler *  pi0EventSampler3 = new InSANEPhaseSpaceSampler(fDiffXSec3);
   evGen->AddSampler(pi0EventSampler3);

   F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
   fDiffXSec->SetBeamEnergy(fBeamEnergy);
   fDiffXSec->InitializePhaseSpaceVariables();
   //      InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace(); /// all the following cross sections share the same phase space. 
   //ps->ListVariables();
   InSANEPhaseSpaceSampler *  fF1F2EventSampler = new InSANEPhaseSpaceSampler(fDiffXSec);
   evGen->AddSampler(fF1F2EventSampler);

   evGen->CalculateTotalCrossSection();

     for(int i = 0;i<200;i++){
        TList * parts = evGen->GenerateEvent();
        parts->Print();
     }

     evGen->Print();
     evGen->Write();
}
