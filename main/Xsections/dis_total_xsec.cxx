#include "Math/Functor.h"
Int_t dis_total_xsec() {

   Double_t beamEnergy = 5.9;

   /// 1
     F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
     fDiffXSec->SetBeamEnergy(beamEnergy);
     InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
     ps->GetVariable("theta_e")->SetMinimum(0);
     ps->GetVariable("theta_e")->SetMaximum(TMath::Pi());
     ps->GetVariable("phi_e")->SetMinimum(-1.0*TMath::Pi());
     ps->GetVariable("phi_e")->SetMaximum(TMath::Pi());

     /// all the following cross sections share the same phase space. 
     ps->Print();
     
     std::cout << "total cross section is " << fDiffXSec->CalculateTotalCrossSection() << " mb\n";


     //    InSANEPhaseSpaceSampler *  fF1F2EventSampler = new InSANEPhaseSpaceSampler(fDiffXSec);
 /*
     /// 2
     InSANEInclusiveMottXSec * fMottDiffXSec = new InSANEInclusiveMottXSec();
     fMottDiffXSec->SetPhaseSpace(ps);
     fMottDiffXSec->SetBeamEnergy(beamEnergy);
     InSANEPhaseSpaceSampler *  fMottEventSampler = new InSANEPhaseSpaceSampler(fMottDiffXSec);

   /// 3
     InSANEFlatInclusiveDiffXSec * fFlatDiffXSec = new InSANEFlatInclusiveDiffXSec();
     fFlatDiffXSec->SetPhaseSpace(ps);
     fFlatDiffXSec->SetBeamEnergy(beamEnergy);
     InSANEPhaseSpaceSampler *  fFlatEventSampler = new InSANEPhaseSpaceSampler(fFlatDiffXSec);

   /// 4
     QFSInclusiveDiffXSec * fQFSDiffXSec= new QFSInclusiveDiffXSec();
     fQFSDiffXSec->SetPhaseSpace(ps);
     fQFSDiffXSec->SetBeamEnergy(beamEnergy);
     fQFSDiffXSec->Configure();
     InSANEPhaseSpaceSampler *  fQFSEventSampler = new InSANEPhaseSpaceSampler(fQFSDiffXSec);
*/
   /// 5
//      InSANEPolarizedDiffXSec * fPolXSec = new InSANEPolarizedDiffXSec();
//      fPolXSec->SetUnpolarizedCrossSection(fQFSDiffXSec);
//      fPolXSec->SetPolarizedPositiveCrossSection(fDiffXSec);
//      fPolXSec->SetPolarizedNegativeCrossSection(fDiffXSec);
//      fPolXSec->SetPhaseSpace(fPolarizedPhaseSpace);
//      fPolXSec->SetBeamEnergy(4.9);
//      fPolXSec->SetChargeAsymmetry(-0.2);
//      fPolXSec->Refresh();
//      InSANEPhaseSpaceSampler *  fPolSampler = new InSANEPhaseSpaceSampler(fPolXSec);

   return(0);
}
