/*! Beam-Target Asymmetry
 */
Int_t elastic_asym(){
   Double_t beamEnergy = 5.9;
   Double_t Mtarget = 0.938;

   Double_t theta_e   = TMath::Pi()*40.0/180.0;
   Double_t ePrime    = 0.0;

   InSANEBeamTargetAsymmetry * asym = new InSANEBeamTargetAsymmetry();
   asym->SetBeamEnergy(beamEnergy);
   asym->SetThetaPhiTarget(80.0*TMath::Pi()/180.0,0.0);

   const int Npoints = 30;

   TGraph * gr  = new TGraph(Npoints);  gr->SetTitle("A_{beam-target}");
   TGraph * gr1 = new TGraph(Npoints); gr1->SetTitle("A_{beam-target}");
   TGraph * gr2 = new TGraph(Npoints); gr2->SetTitle("A_{beam-target}");
   TGraph * gr3 = new TGraph(Npoints); gr3->SetTitle("A_{beam-target}");
   TGraph * gr4 = new TGraph(Npoints); gr4->SetTitle("A_{beam-target}");
   TGraph * gr5 = new TGraph(Npoints); gr5->SetTitle("A_{beam-target}");

   TCanvas * c = new TCanvas("cBeamTargetAsym","Beam-Target Asymmetry",1200,800); 
   c->Divide(3,2);

   for(int i = 0; i< Npoints;i++){

      ePrime = 0.5+(double)i*0.1;
      theta_e   = TMath::Pi()*40.0/180.0;

      asym->SetEPrimeThetaPhi(ePrime,theta_e,0.0);
      asym->Calculate();

      gr->SetPoint(i,asym->GetQsquared(),asym->GetAsymmetry());
      gr1->SetPoint(i,asym->GetTau(),asym->GetAsymmetry());
      gr2->SetPoint(i,ePrime,asym->GetAsymmetry());

//       asym->Print();

   }

   c->cd(1);
   gr->GetXaxis()->SetTitle("Q^{2}");
   gr->GetYaxis()->SetTitle("A");
   gr->Draw("alp");
   c->cd(2);
   gr1->GetXaxis()->SetTitle("#tau");
   gr1->GetYaxis()->SetTitle("A");
   gr1->Draw("alp");
   c->cd(3);
   gr2->GetXaxis()->SetTitle("k'");
   gr2->GetYaxis()->SetTitle("A");
   gr2->Draw("alp");

   for(int i = 0; i< Npoints;i++) {

      theta_e = (double)(i+1)*TMath::Pi()*2.0/180.0;
      ePrime = 1.0;

      asym->SetEPrimeThetaPhi(ePrime,theta_e,0.0);
      asym->Calculate();

      gr3->SetPoint(i,asym->GetQsquared(),asym->GetAsymmetry());
      gr4->SetPoint(i,asym->GetTheta(),asym->GetAsymmetry());
      gr5->SetPoint(i,asym->GetqAbs(),asym->GetAsymmetry());

//       asym->Print();

   }

   c->cd(4);
   gr3->GetXaxis()->SetTitle("Q^{2}*");
   gr3->GetYaxis()->SetTitle("A");
   gr3->Draw("alp");
   c->cd(5);
   gr4->GetXaxis()->SetTitle("#theta_{e'}");
   gr4->GetYaxis()->SetTitle("A");
   gr4->Draw("alp");
   c->cd(6);
   gr5->GetXaxis()->SetTitle("|q|");
   gr5->GetYaxis()->SetTitle("A");
   gr5->Draw("alp");

return(0);
}
