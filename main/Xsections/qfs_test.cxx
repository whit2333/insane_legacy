Int_t qfs_test() {

   InSANEInclusivePhaseSpace * fPhaseSpace = new InSANEInclusivePhaseSpace();

   InSANEPhaseSpaceVariable * varEnergy = new InSANEPhaseSpaceVariable();
      varEnergy = new InSANEPhaseSpaceVariable();
      varEnergy->fVariableName="Energy"; 
      varEnergy->fVariable="E#prime"; // ROOT string latex
      varEnergy->SetMinimum(0.8); //GeV
      varEnergy->SetMaximum(4.9); //GeV
      fPhaseSpace->AddVariable(varEnergy);

    InSANEPhaseSpaceVariable *   varTheta = new InSANEPhaseSpaceVariable();
      varTheta->fVariableName="theta"; 
      varTheta->fVariable="#theta"; // ROOT string latex
      varTheta->SetMinimum(20.0*TMath::Pi()/180.0); //
      varTheta->SetMaximum(50.0*TMath::Pi()/180.0); //
      fPhaseSpace->AddVariable(varTheta);

    InSANEPhaseSpaceVariable *   varPhi = new InSANEPhaseSpaceVariable();
      varPhi->fVariableName="phi"; 
      varPhi->fVariable="#phi"; // ROOT string latex
      varPhi->SetMinimum(-50.0*TMath::Pi()/180.0); //
      varPhi->SetMaximum(50.0*TMath::Pi()/180.0); //
      fPhaseSpace->AddVariable(varPhi);

     QFSInclusiveDiffXSec * fQFSDiffXSec= new QFSInclusiveDiffXSec();
     fQFSDiffXSec->SetPhaseSpace(fPhaseSpace);
     fQFSDiffXSec->SetBeamEnergy(4.9);
     fQFSDiffXSec->Configure();

fQFSDiffXSec->Plot();

     double x[3] = {1.0,40.0*TMath::Pi()/180.0, 0.0};
     
//      InSANEPhaseSpaceSampler *  fQFSEventSampler = new InSANEPhaseSpaceSampler(fQFSDiffXSec);

   std::cout << " sigma is " << fQFSDiffXSec->EvaluateXSec(x) << " pb. \n";

//    double RES = 0;
//    double Z=2;
//    double N=1;
//    double EBEAM = 5900.0;
//    double PCENTRAL = 1000.0;
//    double DELP = 0.1;
//    double THETA = 40.0*TMath::Pi()/180.0;
//    double DELTHETA = 10.0*TMath::Pi()/180.0;
//    qfs_(&RES,&Z,&N,&EBEAM,&PCENTRAL,&DELP,&THETA,&DELTHETA);

   return(0);
}