Int_t irt_R_integrand(){

   Double_t beamEnergy = 5.89;
   Double_t y          = 0.869; 
   Double_t Eprime     = beamEnergy*(1.- y);
   Double_t theta      = 45.0*degree;
   Double_t A = 1.; 
   Double_t Z = 1.; 

   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetTargetType(0);
   fDiffXSec4->SetA(A);
   fDiffXSec4->SetZ(Z);
   fDiffXSec4->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
   ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariableWithName("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4 =  ps4->GetVariableWithName("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4 =  ps4->GetVariableWithName("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4)  = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e4)    = 0.0*degree;
   //fDiffXSec4->EvaluateCurrent();

   InSANEPOLRAD * polrad = fDiffXSec4->GetPOLRAD(); 

   Double_t tau = 0.2;//polrad->GetKinematics()->GetTau_min();//-0.1;
   Double_t W2 = polrad->GetKinematics()->GetW2();
   Double_t Q2 = polrad->GetKinematics()->GetQ2();
   Double_t min = 0.0;//polrad->GetKinematics()->GetTau_min();
   Double_t max = (W2 - TMath::Power(M_p/GeV + 0.13,2.0))/(1+tau);// polrad->GetKinematics()->GetTau_max();

   std::cout << "Q2=" << Q2 << std::endl;
   std::cout << "tau=" << tau << std::endl;
   std::cout << "Rmax=" << max << std::endl;
   std::cout << " Q2 + Rmax * tau = " <<  Q2 + max * tau << std::endl; 
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   vector<double> tauIRT,RIRT,IRT; 
   vector<double> tauIRT1,RIRT1; 
   vector<double> tauIRT2,RIRT2; 
   vector<double> IRT11; 
   vector<double> IRT12; 
   vector<double> IRT13; 
   vector<double> IRT21; 
   vector<double> IRT22; 
   vector<double> IRT23; 
   //ImportERTData(RIRT,tauIRT,IRT); 
   ImportIRTData1(RIRT1,tauIRT1,IRT11,IRT12,IRT13); 
   ImportIRTData2(RIRT2,tauIRT2,IRT21,IRT22,IRT23); 

   TGraph *gIRT11 = GetTGraph(RIRT1,IRT11);
   TGraph *gIRT12 = GetTGraph(RIRT1,IRT12);
   TGraph *gIRT13 = GetTGraph(RIRT1,IRT13);
   TGraph *gIRT21 = GetTGraph(RIRT2,IRT21);
   TGraph *gIRT22 = GetTGraph(RIRT2,IRT22);
   TGraph *gIRT23 = GetTGraph(RIRT2,IRT23);

   TCanvas * c = new TCanvas("IRT_R_integrand","IRT_R_Integrand");
   c->Divide(3,2);

   c->cd(1);
   TMultiGraph * mg = new TMultiGraph();
   TF1 * irt_int_11 = new TF1("irt_int_12", polrad, &InSANEPOLRAD::IRT_R_Integrand1, 
                              min,max, 3,"InSANEPOLRAD","IRT_R_Integrand1");
   irt_int_11->SetParameter(0,tau);
   irt_int_11->SetParameter(1,1);
   irt_int_11->SetParameter(2,1);
   irt_int_11->SetNpx(5000);
   irt_int_11->SetLineColor(kMagenta);
   irt_int_11->SetTitle(Form("#tau = %1.2f",tau) );
   TGraph * gr = new TGraph(irt_int_11->DrawCopy("same")->GetHistogram());
   mg->Add(gr,"pl");
   //mg->Add(gIRT11);  
   //gIRT11->GetYaxis()->SetRangeUser(-10,100);
   //irt_int_11->Draw("same");
    
   //c->cd(2);
   //TF1 * irt_int_12 = new TF1("irt_int_12", polrad, &InSANEPOLRAD::IRT_R_Integrand_3, 
   //                           min,max, 3,"InSANEPOLRAD","IRT_R_Integrand_3");
   //irt_int_12->SetParameter(0,tau);
   //irt_int_12->SetParameter(1,1);
   //irt_int_12->SetParameter(2,2);
   //irt_int_12->SetNpx(5000);
   //irt_int_12->SetLineColor(kMagenta);
   //irt_int_12->DrawCopy();
   //gIRT12->Draw("pl,same");
   //gIRT12->GetYaxis()->SetRangeUser(-10,100);
   //irt_int_12->Draw("same");
   //
   //c->cd(3);
   //TF1 * irt_int_13 = new TF1("irt_int_13", polrad, &InSANEPOLRAD::IRT_R_Integrand2, 
   //                           min,max, 3,"InSANEPOLRAD","IRT_R_Integrand2");
   //irt_int_13->SetParameter(0,tau);
   //irt_int_13->SetParameter(1,1);
   //irt_int_13->SetParameter(2,3);
   //irt_int_13->SetNpx(5000);
   //irt_int_13->SetLineColor(kMagenta);
   //irt_int_13->DrawCopy();
   //gIRT13->Draw("pl,same");
   //gIRT13->GetYaxis()->SetRangeUser(-10,100);
   //irt_int_13->Draw("same");

   //c->cd(4);
   //TF1 * irt_int_21 = new TF1("irt_int_21", polrad, &InSANEPOLRAD::IRT_R_Integrand1, 
   //                           min,max, 3,"InSANEPOLRAD","IRT_R_Integrand1");
   //irt_int_21->SetParameter(0,tau);
   //irt_int_21->SetParameter(1,2);
   //irt_int_21->SetParameter(2,1);
   //irt_int_21->SetNpx(5000);
   //irt_int_21->SetLineColor(kMagenta);
   //irt_int_21->DrawCopy();
   //gIRT21->Draw("pl,same");
   //gIRT21->GetYaxis()->SetRangeUser(-10,100);
   //irt_int_21->Draw("same");
   // 
   //c->cd(5);
   //TF1 * irt_int_22 = new TF1("irt_int_22", polrad, &InSANEPOLRAD::IRT_R_Integrand2, 
   //                           min,max, 3,"InSANEPOLRAD","IRT_R_Integrand2");
   //irt_int_22->SetParameter(0,tau);
   //irt_int_22->SetParameter(1,2);
   //irt_int_22->SetParameter(2,2);
   //irt_int_22->SetNpx(5000);
   //irt_int_22->SetLineColor(kMagenta);
   //irt_int_22->DrawCopy();
   //gIRT22->Draw("pl,same");
   //gIRT22->GetYaxis()->SetRangeUser(-10,100);
   //irt_int_22->Draw("same");
   //
   //c->cd(6);
   //TF1 * irt_int_23 = new TF1("irt_int_23", polrad, &InSANEPOLRAD::IRT_R_Integrand2, 
   //                           min,max, 3,"InSANEPOLRAD","IRT_R_Integrand2");
   //irt_int_23->SetParameter(0,tau);
   //irt_int_23->SetParameter(1,2);
   //irt_int_23->SetParameter(2,3);
   //irt_int_23->SetNpx(5000);
   //irt_int_23->SetLineColor(kMagenta);
   //irt_int_23->DrawCopy();
   //gIRT23->Draw("pl,same");
   //gIRT23->GetYaxis()->SetRangeUser(-10,100);
   //irt_int_23->Draw("same");
   return(0);
}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

        const int N = x.size();
        TGraph *g = new TGraph(N,&x[0],&y[0]);
        g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack);

        return g;

}
//_______________________________________________________________
void ImportIRTData1(vector<double> &R,vector<double> &tau,vector<double> &irt1,vector<double> &irt2,vector<double> &irt3){

        double itau,iy1,iy2,iy3,iR;
        TString inpath = Form("./dump/dump_irt_r_int_i1_j123.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
			infile >> iR >> itau >> iy1 >> iy2 >> iy3;
			tau.push_back(itau);
			R.push_back(iR);
			irt1.push_back(iy1);
			irt2.push_back(iy2);
			irt3.push_back(iy3);
                }
                infile.close();
                tau.pop_back();
                R.pop_back();
                irt1.pop_back();
                irt2.pop_back();
                irt3.pop_back();
        }

}
//_______________________________________________________________
void ImportIRTData2(vector<double> &R,vector<double> &tau,vector<double> &irt1,vector<double> &irt2,vector<double> &irt3){

        double itau,iy1,iy2,iy3,iR;
        TString inpath = Form("./dump/dump_irt_r_int_i2_j123.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
			infile >> iR >> itau >> iy1 >> iy2 >> iy3;
			tau.push_back(itau);
			R.push_back(iR);
			irt1.push_back(iy1);
			irt2.push_back(iy2);
			irt3.push_back(iy3);
                }
                infile.close();
                tau.pop_back();
                R.pop_back();
                irt1.pop_back();
                irt2.pop_back();
                irt3.pop_back();
        }

}
//_______________________________________________________________
void ImportERTData(vector<double> &R,vector<double> &tau,vector<double> &ert){

        double itau,iy1,iR;
        TString inpath = Form("./dump/dump_irt_r_int.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
			infile >> iR >> itau >> iy1;
			tau.push_back(itau);
			R.push_back(iR);
			ert.push_back(iy1);
                }
                infile.close();
                tau.pop_back();
                ert.pop_back();
        }

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
 	if(min<-1E+2){
		min -= 1E+2;
	}else{
		min -= 1E+1; 
        } 

	return min; 
} 
//_______________________________________________________________
