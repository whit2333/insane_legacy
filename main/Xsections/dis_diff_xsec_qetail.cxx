#include <cstdlib>
#include <iostream> 
#include <fstream> 
#include "TString.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TGraphErrors.h"
#include "TLegend.h"
#include "TAxis.h"


Int_t dis_diff_xsec_qetail(){

	/// Test POLRAD against the results from ROSETAIL 
	/// for 3He at 45 deg. 

        Int_t Npoints = 500; 

	Int_t Pass = 0; 
	Double_t beamEnergy =  0;
	Double_t theta      =  45.*degree;
	Double_t Eprime     =  3.0;

	cout << "Enter beam pass (4 or 5): "; 
	cin  >> Pass; 

	vector<double> Ep,Tail; 

	ImportData(Pass,Ep,Tail); 

	int width = 2; 
	TGraph *gRosetail = GetTGraph(Ep,Tail); 
	gRosetail->SetMarkerStyle(20);  
	gRosetail->SetMarkerColor(kBlack); 
	gRosetail->SetLineWidth(width);  

	if(Pass==4) beamEnergy = 4.73; 
	if(Pass==5) beamEnergy = 5.89; 

	// Parameters for the plot are (theta,phi)
	Int_t    npar     = 2;
	Double_t E_min    = 0.50;
	Double_t E_max    = 1.8;

	InSANENucleus::NucleusType Target = InSANENucleus::k3He; 

	/// Form factor models (nucleus) 
	AmrounFormFactors *amrFF = new AmrounFormFactors();  
	MSWFormFactors *mswFF    = new MSWFormFactors();  
	/// Form factor models (nucleons) 
	/// Note that the Galster fit currently does GEn, and defaults to the dipole form otherwise. 
	GalsterFormFactors *galFF = new GalsterFormFactors(); 

	/// Quasi-Elastic Radiative Tail
	InSANEPOLRADQuasiElasticTailDiffXSec * fQETailFull = new  InSANEPOLRADQuasiElasticTailDiffXSec();
	fQETailFull->SetTargetType(Target); 
	fQETailFull->SetBeamEnergy(beamEnergy);
	fQETailFull->GetPOLRAD()->SetUnpolarized();
	fQETailFull->GetPOLRAD()->SetPOLRADTargetFormFactors(amrFF);  
	fQETailFull->GetPOLRAD()->SetPOLRADFormFactors(galFF); 
	fQETailFull->GetPOLRAD()->DoQEFullCalc(true);  
	fQETailFull->InitializePhaseSpaceVariables();
	fQETailFull->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps1 = fQETailFull->GetPhaseSpace();
	//ps->ListVariables();
	Double_t * energy_e1 =  ps1->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e1  =  ps1->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e1    =  ps1->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e1) = Eprime;
	(*theta_e1)  = theta;
	(*phi_e1)    = 0.0*degree;
	fQETailFull->EvaluateCurrent();

	/// Quasi-Elastic Radiative Tail (smeared) 
	InSANEPOLRADQuasiElasticTailDiffXSec * fQETailSmear = new  InSANEPOLRADQuasiElasticTailDiffXSec();
	fQETailSmear->SetTargetType(Target); 
	fQETailSmear->SetBeamEnergy(beamEnergy);
	fQETailSmear->GetPOLRAD()->SetUnpolarized();
	fQETailSmear->GetPOLRAD()->SetPOLRADTargetFormFactors(amrFF);  
	fQETailSmear->GetPOLRAD()->SetPOLRADFormFactors(galFF); 
	fQETailSmear->GetPOLRAD()->DoQEFullCalc(false);  
	fQETailSmear->InitializePhaseSpaceVariables();
	fQETailSmear->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps2 = fQETailSmear->GetPhaseSpace();
	//ps->ListVariables();
	Double_t * energy_e2 =  ps2->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e2  =  ps2->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e2    =  ps2->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e2) = Eprime;
	(*theta_e2)  = theta;
	(*phi_e2)    = 0.0*degree;
	fQETailSmear->EvaluateCurrent();

	TF1 *Full = new TF1("sigma", fQETailFull, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	Full->SetParameter(0,theta);
	Full->SetParameter(1,0.0);
	Full->SetLineColor(kMagenta);

	vector<double> vEp,vQEFull; 
        // FillVectors(E_min,E_max,Npoints,Full,vEp,vQEFull); 
        FillVectors(Ep,Full,vQEFull); 

        // vEp.clear();

	TF1 *Smear = new TF1("sigma", fQETailSmear, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	Smear->SetParameter(0,theta);
	Smear->SetParameter(1,0.0);
	Smear->SetLineColor(kRed);

	vector<double> vQESmear; 
        // FillVectors(E_min,E_max,Npoints,Smear,vEp,vQESmear); 
        FillVectors(Ep,Smear,vQESmear); 

        TGraph *gQESmear = GetTGraph(Ep,vQESmear); 
        gQESmear->SetLineColor(kMagenta); 
        gQESmear->SetLineWidth(width); 

        TGraph *gQEFull = GetTGraph(Ep,vQEFull); 
        gQEFull->SetLineColor(kRed); 
        gQEFull->SetLineWidth(width); 

	vector<double> exSmear,eySmear;
	vector<double> exFull,eyFull;
        CalculateRelativeErr(Ep,Tail,Ep,vQESmear,exSmear,eySmear);
        CalculateRelativeErr(Ep,Tail,Ep,vQEFull,exFull,eyFull);

        TGraph *gErrSmear = GetTGraph(exSmear,eySmear); 
        gErrSmear->SetLineColor(kMagenta); 
        gErrSmear->SetLineWidth(width); 

        TGraph *gErrFull = GetTGraph(exFull,eyFull); 
        gErrFull->SetLineColor(kRed); 
        gErrFull->SetLineWidth(width); 

        TMultiGraph *G = new TMultiGraph(); 
        G->Add(gRosetail,"c"); 
        G->Add(gQESmear ,"c"); 
        G->Add(gQEFull  ,"c"); 

        TMultiGraph *E = new TMultiGraph(); 
        E->Add(gErrSmear ,"c"); 
        E->Add(gErrFull  ,"c"); 

	TLegend *Leg = new TLegend(0.3567839,0.5472028,0.6319095,0.8583916);
	Leg->SetTextSize(0.03146853);
	Leg->SetFillColor(kWhite);
	Leg->AddEntry(gRosetail,"Rosetail (2p+n)"   ,"l");
	Leg->AddEntry(gQEFull  ,"C++ POLRAD (Full)" ,"l");
	Leg->AddEntry(gQESmear ,"C++ POLRAD (Smear)","l");

	TString Title      = Form("^{3}He Quasi-Elastic Tail (E_{s} = %.2f GeV)",beamEnergy);
	TString Title2     = Form("Error (Relative to ROSETAIL)");
	TString xAxisTitle = Form("E_{p} (GeV)");  
	TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)"); 
	
	TCanvas * c1 = new TCanvas("dis_diff_xsec","POLRAD QRT",1000,800);
	c1->SetFillColor(kWhite);
        c1->Divide(2,1); 
	c1->cd(); 

	c1->cd(1);
	gPad->SetLogy(false);
	G->Draw("A");
	G->SetTitle(Title);
	G->GetXaxis()->SetTitle(xAxisTitle);
	G->GetXaxis()->CenterTitle();
	G->GetYaxis()->SetTitle(yAxisTitle);
	G->GetYaxis()->CenterTitle();
	G->GetXaxis()->SetLimits(E_min,E_max);
	Leg->Draw("same");
        c1->Update();

	c1->cd(2);
	E->Draw("A");
	E->SetTitle(Title2);
	E->GetXaxis()->SetTitle(xAxisTitle);
	E->GetXaxis()->CenterTitle();
	E->GetYaxis()->SetTitle("Relative Error (%)");
	E->GetYaxis()->CenterTitle();
	E->GetXaxis()->SetLimits(E_min,E_max);
        c1->Update();

	return(0);
}
//______________________________________________________________________________
void FillVectors(double min,double max,int N,TF1 *f,vector<double> &x,vector<double> &y){

        // cout << "-----------------------------------" << endl;

        double ix=0,iy=0;
        double step = (max-min)/( (double)N );
        for(int i=0;i<N;i++){
                ix = min + ( (double)i )*step;
                iy = f->Eval(ix);
                // cout << "x = " << Form("%.3E",ix) << "\t"  
                //      << "y = " << Form("%.3E",iy) << endl;
                x.push_back(ix);
                y.push_back(iy);
        }

}
//______________________________________________________________________________
void FillVectors(vector<double> x,TF1 *f,vector<double> &y){

        cout << "Doing calculation..." << endl;

        const int N = x.size(); 
 
        double ix=0,iy=0;
        for(int i=0;i<N;i++){
                ix = x[i];
                iy = f->Eval(ix);
                // cout << "x = " << Form("%.3E",ix) << "\t"  
                //      << "y = " << Form("%.3E",iy) << endl;
                // x.push_back(ix);
                y.push_back(iy);
        }

        cout << "-->Done." << endl;

}
//______________________________________________________________________________
void CalculateRelativeErr(vector<double> x1,vector<double> y1,
                          vector<double> x2,vector<double> y2,
                          vector<double> &ex,vector<double> &ey){

	int N = x1.size();
	int M = x2.size();

	double num=0,den=0,arg=0;

	for(int i=0;i<N;i++){
		for(int j=0;j<M;j++){
			if( TMath::Abs(x1[i]-x2[j]) < 1E-4 ){
				num = TMath::Abs(y1[i] - y2[j]);
				den = y1[i]; 
				arg = 100.*num/den;
				ex.push_back(x1[i]);
				ey.push_back(arg);
			}
		}
	}


}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]);
	return g;

}
//______________________________________________________________________________
void ImportData(int Pass,vector<double> &Ep,vector<double> &Tail){

	vector<double> pEp,pT,nEp,nT; 

	ImportNucleonData(Pass,"P",pEp,pT);
	ImportNucleonData(Pass,"N",nEp,nT);

	int N = pEp.size();
	for(int i=0;i<N;i++){
		Ep.push_back( pEp[i] ); 
		Tail.push_back( 2.*pT[i] + nT[i] );
	}

}
//______________________________________________________________________________
void ImportNucleonData(int Pass,TString Target,vector<double> &Ep,vector<double> &Tail){

	TString inpath = Form("./dump/%d%sSA_eltail_unpl_int-only.out",Pass,Target.Data());
	double iEs,iEp,iNu,iW,iTail;

	ifstream infile;
	infile.open(inpath);
	if(infile.fail()){
		cout << "Cannot open the file: " << inpath << endl;
		exit(1);
	}else{
		while(!infile.eof()){
			infile >> iEs >> iEp >> iNu >> iW >> iTail; 
			Ep.push_back(iEp/1E+3); 
			Tail.push_back(iTail); 
		}
	}


}
