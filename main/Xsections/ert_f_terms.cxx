

Int_t ert_f_terms(){

   Double_t beamEnergy  = 5.9;
   Double_t theta       = 45.0*degree;//29.0*TMath::Pi()/180.0;
   Double_t Eprime      = beamEnergy*(1.-0.869);

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec1= new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->GetPOLRAD()->SetTargetType(0);
   fDiffXSec1->GetPOLRAD()->SetHelicity(-1.);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec1->GetPhaseSpace();
   //ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4 =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4 =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4) = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e4) = 0.0*TMath::Pi()/180.0;
   fDiffXSec1->EvaluateCurrent();
   //fDiffXSec1->Print();
   //fDiffXSec1->PrintTable();

   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   InSANEPOLRAD * polrad = fDiffXSec1->GetPOLRAD();
   Double_t tau_min = polrad->GetKinematics()->GetTau_A_min();
   Double_t tau_max = polrad->GetKinematics()->GetTau_A_max();

   // cout << "tau min = " << tau_min << endl;
   // cout << "tau max = " << tau_max << endl;
   // exit(1); 
 
   Double_t xbj = polrad->GetKinematics()->Getx();
   Double_t Q2  = polrad->GetKinematics()->GetQ2();
   Double_t Mp  = polrad->GetKinematics()->GetM();
   Double_t W2  = polrad->GetKinematics()->GetW2();
   Double_t nu  = polrad->GetKinematics()->Gety()*beamEnergy;
   Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
   Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
   Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
   Double_t tau_u = 0.67;
   Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

   //polrad->GetKinematics()->Print();
   std::cout << "tau_min = " << tau_min << "\n";
   std::cout << "tau_max = " << tau_max << "\n";
   std::cout << "Q2 = " << Q2 << "\n";
   std::cout << "W2 = " << W2 << "\n";
   std::cout << "Mp = " << Mp << "\n";
   std::cout << "xbj = " << xbj << "\n";
   std::cout << "tau_me_peak = " << tau_me_peak << "\n";
   std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
   std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
   std::cout << "t = " << t << "\n";

   vector<double> tauFxi,Fxi_d,Fxi_2minus,Fxi_2plus; 
   vector<double> tauFxieta,Fxieta_d,Fxieta_2minus,Fxieta_2plus; 
   vector<double> tauFeta,Feta,Feta_d,Feta_1plus,Feta_2minus,Feta_2plus; 
   vector<double> tauFetaeta,Fetaeta,Fetaeta_d,Fetaeta_1plus,Fetaeta_2minus,Fetaeta_2plus; 
   vector<double> tauF,F,F_d,F_1plus,F_2minus,F_2plus; 
   vector<double> tauFi,Fi,Fii; 

   ImportFiData(     tauFi     ,Fi,Fii); 
   ImportFData(      tauF      ,F,F_d,F_1plus,F_2minus,F_2plus); 
   ImportFxiData(    tauFxi    ,Fxi_d,Fxi_2minus,Fxi_2plus); 
   ImportFetaData(   tauFeta   ,Feta,Feta_d,Feta_1plus,Feta_2minus,Feta_2plus); 
   ImportFetaetaData(tauFetaeta,Fetaeta,Fetaeta_d,Fetaeta_1plus,Fetaeta_2minus,Fetaeta_2plus); 
   ImportFxietaData( tauFxieta ,Fxieta_d,Fxieta_2minus,Fxieta_2plus); 

   Int_t width = 2; 

   TGraph *gFi             = GetTGraph(tauFi,Fi);
   TGraph *gFii            = GetTGraph(tauFi,Fii);
   TGraph *gF              = GetTGraph(tauF,F);
   TGraph *gF_d            = GetTGraph(tauF,F_d);
   TGraph *gF_1plus        = GetTGraph(tauF,F_1plus); 
   TGraph *gF_2minus       = GetTGraph(tauF,F_2minus);
   TGraph *gF_2plus        = GetTGraph(tauF,F_2plus);
   TGraph *gFeta           = GetTGraph(tauFeta,Feta);
   TGraph *gFeta_d         = GetTGraph(tauFeta,Feta_d);
   TGraph *gFeta_1plus     = GetTGraph(tauFeta,Feta_1plus); 
   TGraph *gFeta_2minus    = GetTGraph(tauFeta,Feta_2minus);
   TGraph *gFeta_2plus     = GetTGraph(tauFeta,Feta_2plus);
   TGraph *gFxi_d          = GetTGraph(tauFxi,Fxi_d);
   TGraph *gFxi_2minus     = GetTGraph(tauFxi,Fxi_2minus);
   TGraph *gFxi_2plus      = GetTGraph(tauFxi,Fxi_2plus);
   TGraph *gFetaeta        = GetTGraph(tauFetaeta,Fetaeta);
   TGraph *gFetaeta_d      = GetTGraph(tauFetaeta,Fetaeta_d);
   TGraph *gFetaeta_1plus  = GetTGraph(tauFetaeta,Fetaeta_1plus); 
   TGraph *gFetaeta_2minus = GetTGraph(tauFetaeta,Fetaeta_2minus);
   TGraph *gFetaeta_2plus  = GetTGraph(tauFetaeta,Fetaeta_2plus);
   TGraph *gFxieta_d       = GetTGraph(tauFxieta,Fxieta_d);
   TGraph *gFxieta_2minus  = GetTGraph(tauFxieta,Fxieta_2minus);
   TGraph *gFxieta_2plus   = GetTGraph(tauFxieta,Fxieta_2plus);
     
   gF->SetLineWidth(width);

   TF1 * fFxieta_2minus = new TF1("fFxieta_2minus", polrad, &InSANEPOLRAD::Fxieta_2minus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fxieta_2minus_Plot");
   fFxieta_2minus->SetNpx(5000);
   fFxieta_2minus->SetLineColor(kMagenta);
   fFxieta_2minus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFxieta_2plus = new TF1("fFxieta_2plus", polrad, &InSANEPOLRAD::Fxieta_2plus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fxieta_2plus_Plot");
   fFxieta_2plus->SetNpx(5000);
   fFxieta_2plus->SetLineColor(kMagenta);
   fFxieta_2plus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFxieta_d = new TF1("fFxieta_d", polrad, &InSANEPOLRAD::Fxieta_d_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fxieta_d_Plot");
   fFxieta_d->SetNpx(5000);
   fFxieta_d->SetLineColor(kMagenta);
   fFxieta_d->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFxi_2minus = new TF1("fFxi_2minus", polrad, &InSANEPOLRAD::Fxi_2minus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fxi_2minus_Plot");
   fFxi_2minus->SetNpx(5000);
   fFxi_2minus->SetLineColor(kMagenta);
   fFxi_2minus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFxi_2plus = new TF1("fFxi_2plus", polrad, &InSANEPOLRAD::Fxi_2plus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fxi_2plus_Plot");
   fFxi_2plus->SetNpx(5000);
   fFxi_2plus->SetLineColor(kMagenta);
   fFxi_2plus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFxi_d = new TF1("fFxi_d", polrad, &InSANEPOLRAD::Fxi_d_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fxi_d_Plot");
   fFxi_d->SetNpx(5000);
   fFxi_d->SetLineColor(kMagenta);
   fFxi_d->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFi = new TF1("fFi", polrad, &InSANEPOLRAD::Fi_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fi_Plot");
   fFi->SetNpx(5000);
   fFi->SetLineColor(kMagenta);
   fFi->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFii = new TF1("fFii", polrad, &InSANEPOLRAD::Fii_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fii_Plot");
   fFii->SetNpx(5000);
   fFii->SetLineColor(kMagenta);
   fFii->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fF = new TF1("fF", polrad, &InSANEPOLRAD::F_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","F_Plot");
   fF->SetNpx(5000);
   fF->SetLineColor(kMagenta);
   fF->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fF_d = new TF1("fF_d", polrad, &InSANEPOLRAD::F_d_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","F_d_Plot");
   fF_d->SetNpx(5000);
   fF_d->SetLineColor(kMagenta);
   fF_d->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fF_2minus = new TF1("fF_2minus", polrad, &InSANEPOLRAD::F_2minus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","F_2minus_Plot");
   fF_2minus->SetNpx(5000);
   fF_2minus->SetLineColor(kMagenta);
   fF_2minus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fF_2plus = new TF1("fF_2plus", polrad, &InSANEPOLRAD::F_2plus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","F_2plus_Plot");
   fF_2plus->SetNpx(5000);
   fF_2plus->SetLineColor(kMagenta);
   fF_2plus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fF_1plus = new TF1("fF_1plus", polrad, &InSANEPOLRAD::F_1plus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","F_1plus_Plot");
   fF_1plus->SetNpx(5000);
   fF_1plus->SetLineColor(kMagenta);
   fF_1plus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFeta = new TF1("fFeta", polrad, &InSANEPOLRAD::Feta_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Feta_Plot");
   fFeta->SetNpx(5000);
   fFeta->SetLineColor(kMagenta);
   fFeta->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFeta_d = new TF1("fFeta_d", polrad, &InSANEPOLRAD::Feta_d_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Feta_d_Plot");
   fFeta_d->SetNpx(5000);
   fFeta_d->SetLineColor(kMagenta);
   fFeta_d->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFeta_2minus = new TF1("fFeta_2minus", polrad, &InSANEPOLRAD::Feta_2minus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Feta_2minus_Plot");
   fFeta_2minus->SetNpx(5000);
   fFeta_2minus->SetLineColor(kMagenta);
   fFeta_2minus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFeta_2plus = new TF1("fFeta_2plus", polrad, &InSANEPOLRAD::Feta_2plus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Feta_2plus_Plot");
   fFeta_2plus->SetNpx(5000);
   fFeta_2plus->SetLineColor(kMagenta);
   fFeta_2plus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFeta_1plus = new TF1("fFeta_1plus", polrad, &InSANEPOLRAD::Feta_1plus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Feta_1plus_Plot");
   fFeta_1plus->SetNpx(5000);
   fFeta_1plus->SetLineColor(kMagenta);
   fFeta_1plus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFetaeta = new TF1("fFetaeta", polrad, &InSANEPOLRAD::Fetaeta_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fetaeta_Plot");
   fFetaeta->SetNpx(5000);
   fFetaeta->SetLineColor(kMagenta);
   fFetaeta->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFetaeta_d = new TF1("fFetaeta_d", polrad, &InSANEPOLRAD::Fetaeta_d_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fetaeta_d_Plot");
   fFetaeta_d->SetNpx(5000);
   fFetaeta_d->SetLineColor(kMagenta);
   fFetaeta_d->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFetaeta_2minus = new TF1("fFetaeta_2minus", polrad, &InSANEPOLRAD::Fetaeta_2minus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fetaeta_2minus_Plot");
   fFetaeta_2minus->SetNpx(5000);
   fFetaeta_2minus->SetLineColor(kMagenta);
   fFetaeta_2minus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFetaeta_2plus = new TF1("fFetaeta_2plus", polrad, &InSANEPOLRAD::Fetaeta_2plus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fetaeta_2plus_Plot");
   fFetaeta_2plus->SetNpx(5000);
   fFetaeta_2plus->SetLineColor(kMagenta);
   fFetaeta_2plus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fFetaeta_1plus = new TF1("fFetaeta_1plus", polrad, &InSANEPOLRAD::Fetaeta_1plus_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Fetaeta_1plus_Plot");
   fFetaeta_1plus->SetNpx(5000);
   fFetaeta_1plus->SetLineColor(kMagenta);
   fFetaeta_1plus->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
   Leg->SetFillColor(kWhite); 
   Leg->AddEntry(gF_d,"Fortran","l");
   Leg->AddEntry(fF_d,"C++"    ,"l");

   Double_t min = -1E+05;
   Double_t max =  1E+05;

   TString DrawOption = Form("AP"); 
   TString xAxisTitle = Form("#tau");

   TCanvas *c1 = new TCanvas("c1","F Terms",1000,800);
   c1->SetFillColor(kWhite);
   c1->Divide(3,2); 

   min = GetMin(F_2minus);
   max = GetMax(F_2minus);

   c1->cd(1); 
   gF_2minus->Draw(DrawOption);
   gF_2minus->SetTitle("F_{2-}");
   gF_2minus->GetXaxis()->SetTitle(xAxisTitle);
   gF_2minus->GetXaxis()->CenterTitle();
   gF_2minus->GetYaxis()->SetRangeUser(min,max);
   gF_2minus->Draw(DrawOption);
   fF_2minus->Draw("same");
   c1->Update();

   min = GetMin(F_2plus);
   max = GetMax(F_2plus);

   c1->cd(2); 
   gF_2plus->Draw(DrawOption);
   gF_2plus->SetTitle("F_{2+}");
   gF_2plus->GetXaxis()->SetTitle(xAxisTitle);
   gF_2plus->GetXaxis()->CenterTitle();
   gF_2plus->GetYaxis()->SetRangeUser(min,max);
   gF_2plus->Draw(DrawOption);
   fF_2plus->Draw("same");
   c1->Update();

   min = GetMin(F_d);
   max = GetMax(F_d);

   c1->cd(3); 
   gF_d->Draw(DrawOption);
   gF_d->SetTitle("F_{d}");
   gF_d->GetXaxis()->SetTitle(xAxisTitle);
   gF_d->GetXaxis()->CenterTitle();
   gF_d->GetYaxis()->SetRangeUser(min,max);
   gF_d->Draw(DrawOption);
   fF_d->Draw("same");
   c1->Update();

   min = GetMin(F_1plus);
   max = GetMax(F_1plus);

   c1->cd(4); 
   gF_1plus->Draw(DrawOption);
   gF_1plus->SetTitle("F_{1+}");
   gF_1plus->GetXaxis()->SetTitle(xAxisTitle);
   gF_1plus->GetXaxis()->CenterTitle();
   gF_1plus->GetYaxis()->SetRangeUser(min,max);
   gF_1plus->Draw(DrawOption);
   fF_1plus->Draw("same");
   c1->Update();

   min = GetMin(F);
   max = GetMax(F);

   c1->cd(5); 
   gF->Draw(DrawOption);
   gF->SetTitle("F");
   gF->GetXaxis()->SetTitle(xAxisTitle);
   gF->GetXaxis()->CenterTitle();
   gF->GetYaxis()->SetRangeUser(min,max);
   gF->Draw(DrawOption);
   fF->Draw("same");
   c1->Update();

   c1->cd(6); 
   Leg->Draw();
   c1->Update();

   TCanvas *c2 = new TCanvas("c2","xi Terms",1000,800);
   c2->SetFillColor(kWhite);
   c2->Divide(2,2); 

   min = GetMin(Fxi_2minus);
   max = GetMax(Fxi_2minus);

   c2->cd(1); 
   gFxi_2minus->Draw(DrawOption);
   gFxi_2minus->SetTitle("F_{2-}^{#xi}");
   gFxi_2minus->GetXaxis()->SetTitle(xAxisTitle);
   gFxi_2minus->GetXaxis()->CenterTitle();
   gFxi_2minus->GetYaxis()->SetRangeUser(min,max);
   gFxi_2minus->Draw(DrawOption);
   fFxi_2minus->Draw("same");
   c2->Update();

   min = GetMin(Fxi_2plus);
   max = GetMax(Fxi_2plus);

   c2->cd(2); 
   gFxi_2plus->Draw(DrawOption);
   gFxi_2plus->SetTitle("F_{2+}^{#xi}");
   gFxi_2plus->GetXaxis()->SetTitle(xAxisTitle);
   gFxi_2plus->GetXaxis()->CenterTitle();
   gFxi_2plus->GetYaxis()->SetRangeUser(min,max);
   gFxi_2plus->Draw(DrawOption);
   fFxi_2plus->Draw("same");
   c2->Update();

   min = GetMin(Fxi_d);
   max = GetMax(Fxi_d);

   c2->cd(3); 
   gFxi_d->Draw(DrawOption);
   gFxi_d->SetTitle("F_{d}^{#xi}");
   gFxi_d->GetXaxis()->SetTitle(xAxisTitle);
   gFxi_d->GetXaxis()->CenterTitle();
   gFxi_d->GetYaxis()->SetRangeUser(min,max);
   gFxi_d->Draw(DrawOption);
   fFxi_d->Draw("same");
   c2->Update();

   c2->cd(4); 
   Leg->Draw();
   c2->Update();

   TCanvas *c3 = new TCanvas("c3","eta Terms",1000,800);
   c3->SetFillColor(kWhite);
   c3->Divide(3,2); 

   min = GetMin(Feta_2minus);
   max = GetMax(Feta_2minus);

   c3->cd(1); 
   gFeta_2minus->Draw(DrawOption);
   gFeta_2minus->SetTitle("F_{2-}^{#eta}");
   gFeta_2minus->GetXaxis()->SetTitle(xAxisTitle);
   gFeta_2minus->GetXaxis()->CenterTitle();
   gFeta_2minus->GetYaxis()->SetRangeUser(min,max);
   gFeta_2minus->Draw(DrawOption);
   fFeta_2minus->Draw("same");
   c3->Update();

   min = GetMin(Fetaeta_2plus);
   max = GetMax(Fetaeta_2plus);

   c3->cd(2); 
   gFeta_2plus->Draw(DrawOption);
   gFeta_2plus->SetTitle("F_{2+}^{#eta}");
   gFeta_2plus->GetXaxis()->SetTitle(xAxisTitle);
   gFeta_2plus->GetXaxis()->CenterTitle();
   gFeta_2plus->GetYaxis()->SetRangeUser(min,max);
   gFeta_2plus->Draw(DrawOption);
   fFeta_2plus->Draw("same");
   c3->Update();

   min = GetMin(Fetaeta_d);
   max = GetMax(Fetaeta_d);

   c3->cd(3); 
   gFeta_d->Draw(DrawOption);
   gFeta_d->SetTitle("F_{d}^{#eta}");
   gFeta_d->GetXaxis()->SetTitle(xAxisTitle);
   gFeta_d->GetXaxis()->CenterTitle();
   gFeta_d->GetYaxis()->SetRangeUser(min,max);
   gFeta_d->Draw(DrawOption);
   fFeta_d->Draw("same");
   c3->Update();

   min = GetMin(Fetaeta_1plus);
   max = GetMax(Fetaeta_1plus);

   c3->cd(4); 
   gFeta_1plus->Draw(DrawOption);
   gFeta_1plus->SetTitle("F_{1+}^{#eta}");
   gFeta_1plus->GetXaxis()->SetTitle(xAxisTitle);
   gFeta_1plus->GetXaxis()->CenterTitle();
   gFeta_1plus->GetYaxis()->SetRangeUser(min,max);
   gFeta_1plus->Draw(DrawOption);
   fFeta_1plus->Draw("same");
   c3->Update();

   min = GetMin(Feta);
   max = GetMax(Feta);

   c3->cd(5); 
   gFeta->Draw(DrawOption);
   gFeta->SetTitle("F^{#eta}");
   gFeta->GetXaxis()->SetTitle(xAxisTitle);
   gFeta->GetXaxis()->CenterTitle();
   gFeta->GetYaxis()->SetRangeUser(min,max);
   gFeta->Draw(DrawOption);
   fFeta->Draw("same");
   c3->Update();

   c3->cd(6); 
   Leg->Draw();
   c3->Update();

   TCanvas *c4 = new TCanvas("c4","eta eta Terms",1000,800);
   c4->SetFillColor(kWhite);
   c4->Divide(3,2); 

   min = GetMin(Fetaeta_2minus);
   max = GetMax(Fetaeta_2minus);

   c4->cd(1); 
   gFetaeta_2minus->Draw(DrawOption);
   gFetaeta_2minus->SetTitle("F_{2-}^{#eta#eta}");
   gFetaeta_2minus->GetXaxis()->SetTitle(xAxisTitle);
   gFetaeta_2minus->GetXaxis()->CenterTitle();
   gFetaeta_2minus->GetYaxis()->SetRangeUser(min,max);
   gFetaeta_2minus->Draw(DrawOption);
   fFetaeta_2minus->Draw("same");
   c4->Update();

   min = GetMin(Fetaeta_2plus);
   max = GetMax(Fetaeta_2plus);

   c4->cd(2); 
   gFetaeta_2plus->Draw(DrawOption);
   gFetaeta_2plus->SetTitle("F_{2+}^{#eta#eta}");
   gFetaeta_2plus->GetXaxis()->SetTitle(xAxisTitle);
   gFetaeta_2plus->GetXaxis()->CenterTitle();
   gFetaeta_2plus->GetYaxis()->SetRangeUser(min,max);
   gFetaeta_2plus->Draw(DrawOption);
   fFetaeta_2plus->Draw("same");
   c4->Update();

   min = GetMin(Fetaeta_d);
   max = GetMax(Fetaeta_d);

   c4->cd(3); 
   gFetaeta_d->Draw(DrawOption);
   gFetaeta_d->SetTitle("F_{d}^{#eta#eta}");
   gFetaeta_d->GetXaxis()->SetTitle(xAxisTitle);
   gFetaeta_d->GetXaxis()->CenterTitle();
   gFetaeta_d->GetYaxis()->SetRangeUser(min,max);
   gFetaeta_d->Draw(DrawOption);
   fFetaeta_d->Draw("same");
   c4->Update();

   min = GetMin(Fetaeta_1plus);
   max = GetMax(Fetaeta_1plus);

   c4->cd(4); 
   gFetaeta_1plus->Draw(DrawOption);
   gFetaeta_1plus->SetTitle("F_{1+}^{#eta#eta}");
   gFetaeta_1plus->GetXaxis()->SetTitle(xAxisTitle);
   gFetaeta_1plus->GetXaxis()->CenterTitle();
   gFetaeta_1plus->GetYaxis()->SetRangeUser(min,max);
   gFetaeta_1plus->Draw(DrawOption);
   fFetaeta_1plus->Draw("same");
   c4->Update();

   min = GetMin(Fetaeta);
   max = GetMax(Fetaeta);

   c4->cd(5); 
   gFetaeta->Draw(DrawOption);
   gFetaeta->SetTitle("F^{#eta#eta}");
   gFetaeta->GetXaxis()->SetTitle(xAxisTitle);
   gFetaeta->GetXaxis()->CenterTitle();
   gFetaeta->GetYaxis()->SetRangeUser(min,max);
   gFetaeta->Draw(DrawOption);
   fFetaeta->Draw("same");
   c4->Update();

   c4->cd(6); 
   Leg->Draw();
   c4->Update();

   TCanvas *c5 = new TCanvas("c5","xi eta Terms",1000,800);
   c5->SetFillColor(kWhite);
   c5->Divide(3,2); 

   min = GetMin(Fxieta_2minus);
   max = GetMax(Fxieta_2minus);

   c5->cd(1); 
   gFxieta_2minus->Draw(DrawOption);
   gFxieta_2minus->SetTitle("F_{2-}^{#xi#eta}");
   gFxieta_2minus->GetXaxis()->SetTitle(xAxisTitle);
   gFxieta_2minus->GetXaxis()->CenterTitle();
   gFxieta_2minus->GetYaxis()->SetRangeUser(min,max);
   gFxieta_2minus->Draw(DrawOption);
   fFxieta_2minus->Draw("same");
   c5->Update();

   min = GetMin(Fxieta_2plus);
   max = GetMax(Fxieta_2plus);

   c5->cd(2); 
   gFxieta_2plus->Draw(DrawOption);
   gFxieta_2plus->SetTitle("F_{2+}^{#xi#eta}");
   gFxieta_2plus->GetXaxis()->SetTitle(xAxisTitle);
   gFxieta_2plus->GetXaxis()->CenterTitle();
   gFxieta_2plus->GetYaxis()->SetRangeUser(min,max);
   gFxieta_2plus->Draw(DrawOption);
   fFxieta_2plus->Draw("same");
   c5->Update();

   min = GetMin(Fxieta_d);
   max = GetMax(Fxieta_d);

   c5->cd(3); 
   gFxieta_d->Draw(DrawOption);
   gFxieta_d->SetTitle("F_{d}^{#xi#eta}");
   gFxieta_d->GetXaxis()->SetTitle(xAxisTitle);
   gFxieta_d->GetXaxis()->CenterTitle();
   gFxieta_d->GetYaxis()->SetRangeUser(min,max);
   gFxieta_d->Draw(DrawOption);
   fFxieta_d->Draw("same");
   c5->Update();

   min = GetMin(Fi);
   max = GetMax(Fi);

   c5->cd(4); 
   gFi->Draw(DrawOption);
   gFi->SetTitle("F_{i}");
   gFi->GetXaxis()->SetTitle(xAxisTitle);
   gFi->GetXaxis()->CenterTitle();
   gFi->GetYaxis()->SetRangeUser(min,max);
   gFi->Draw(DrawOption);
   fFi->Draw("same");
   c5->Update();

   min = GetMin(Fii);
   max = GetMax(Fii);

   c5->cd(5); 
   gFii->Draw(DrawOption);
   gFii->SetTitle("F_{ii}");
   gFii->GetXaxis()->SetTitle(xAxisTitle);
   gFii->GetXaxis()->CenterTitle();
   gFii->GetYaxis()->SetRangeUser(min,max);
   gFii->Draw(DrawOption);
   fFii->Draw("same");
   c5->Update();

   c5->cd(6);
   Leg->Draw("same");
   c5->Update();

   fDiffXSec1->GetPOLRAD()->Print();
   fDiffXSec1->GetPOLRAD()->GetKinematics()->Print();

   return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportFData(vector<double> &tau,vector<double> &F,vector<double> &Fd,vector<double> &F1plus,vector<double> &F2minus,vector<double> &F2plus){

        double itau,iy1,iy2,iy3,iy4,iy5;
        TString inpath = Form("./dump/dump_f.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3 >> iy4 >> iy5;
                                tau.push_back(itau);
                                F.push_back(iy1);
                                Fd.push_back(iy2);
                                F1plus.push_back(iy3);
                                F2minus.push_back(iy4);
                                F2plus.push_back(iy5);
		}
                infile.close();
                tau.pop_back();
                F.pop_back();
                Fd.pop_back();
		F1plus.pop_back();
                F2minus.pop_back();
                F2plus.pop_back();
	}

}
//_______________________________________________________________
void ImportFxiData(vector<double> &tau,vector<double> &Fd,vector<double> &F2minus,vector<double> &F2plus){

        double itau,iy1,iy2,iy3,iy4,iy5;
        TString inpath = Form("./dump/dump_fxi.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3;
                                tau.push_back(itau);
                                Fd.push_back(iy1);
                                F2minus.push_back(iy2);
                                F2plus.push_back(iy3);
		}
                infile.close();
                tau.pop_back();
                Fd.pop_back();
                F2minus.pop_back();
                F2plus.pop_back();

	}

}
//_______________________________________________________________
void ImportFetaData(vector<double> &tau,vector<double> &F,vector<double> &Fd,vector<double> &F1plus,vector<double> &F2minus,vector<double> &F2plus){

        double itau,iy1,iy2,iy3,iy4,iy5;
        TString inpath = Form("./dump/dump_feta.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3 >> iy4 >> iy5;
                                tau.push_back(itau);
                                F.push_back(iy1);
                                Fd.push_back(iy2);
                                F1plus.push_back(iy3);
                                F2minus.push_back(iy4);
                                F2plus.push_back(iy5);
		}
                infile.close();
                tau.pop_back();
                F.pop_back();
                Fd.pop_back();
		F1plus.pop_back();
                F2minus.pop_back();
                F2plus.pop_back();

	}

}
//_______________________________________________________________
void ImportFetaetaData(vector<double> &tau,vector<double> &F,vector<double> &Fd,vector<double> &F1plus,vector<double> &F2minus,vector<double> &F2plus){

        double itau,iy1,iy2,iy3,iy4,iy5;
        TString inpath = Form("./dump/dump_fetaeta.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3 >> iy4 >> iy5;
                                tau.push_back(itau);
                                F.push_back(iy1);
                                Fd.push_back(iy2);
                                F1plus.push_back(iy3);
                                F2minus.push_back(iy4);
                                F2plus.push_back(iy5);
		}
                infile.close();
                tau.pop_back();
                F.pop_back();
                Fd.pop_back();
		F1plus.pop_back();
                F2minus.pop_back();
                F2plus.pop_back();

	}

}
//_______________________________________________________________
void ImportFxietaData(vector<double> &tau,vector<double> &Fd,vector<double> &F2minus,vector<double> &F2plus){

        double itau,iy1,iy2,iy3,iy4,iy5;
        TString inpath = Form("./dump/dump_fxieta.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3;
                                tau.push_back(itau);
                                Fd.push_back(iy1);
                                F2minus.push_back(iy2);
                                F2plus.push_back(iy3);
		}
                infile.close();
                tau.pop_back();
                Fd.pop_back();
                F2minus.pop_back();
                F2plus.pop_back();

	}

}
//_______________________________________________________________
void ImportFiData(vector<double> &tau,vector<double> &Fi,vector<double> &Fii){

        double itau,iy1,iy2,iy3,iy4,iy5;
        TString inpath = Form("./dump/dump_fii.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2;
                                tau.push_back(itau);
                                Fi.push_back(iy1);
                                Fii.push_back(iy2);
		}
                infile.close();
                tau.pop_back();
                Fi.pop_back();
                Fii.pop_back();

	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
	min -= 1E+2; 
 	// if(min<-1E+2){
	// 	min -= 1E+4;
	// }else{
	// 	min -= 1E+2; 
        // } 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	max += 1E+2; 
	// if(max>1E+2){
	// 	max += 1E+4;
	// }else{
	// 	max += 1E+2; 
        // } 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}


