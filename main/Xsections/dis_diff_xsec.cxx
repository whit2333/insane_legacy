/// A test script for comparing cross sections from F1F209
/// and POLRAD, which can give DIS Born, elastic radiative tail (ERT), 
/// quasi-elastic radiative tail (QRT) and the inelastic radiative tail (IRT) 

Int_t dis_diff_xsec(){

	/// Kinematics 
	Double_t DEG_TO_RAD = TMath::Pi()/180.0;
	Double_t beamEnergy = 5.9;
	Double_t Eprime     = 1.41; 
	Double_t theta      = 45.*DEG_TO_RAD;
	Double_t phi        = 0.*DEG_TO_RAD;  
	/// For plotting cross sections 
	Int_t    npar     = 2;
	Double_t Emin     = 0.5;
	Double_t Emax     = 2.2;
	Double_t minTheta = 15.0*DEG_TO_RAD;
	Double_t maxTheta = 55.0*DEG_TO_RAD;
	/// Target (use A>=2 to test QRT) 
        Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
	Double_t A          = 0;   
	Double_t Z          = 0;   

        TString Title,TargetName; 

        switch(TargetType){
		case 0: // proton 
			TargetName = Form("Proton");
			A = 1;
			Z = 1;
			break;
		case 1: // neutron 
			TargetName = Form("Neutron");
			A = 1;
			Z = 0;
			break;
		case 2: // deuteron 
			TargetName = Form("Deuteron");
			A = 2;
			Z = 1; 
			break;
		case 3: // Helium-3 
			TargetName = Form("^{3}He");
			A = 3;
			Z = 2; 
			break;
        }

        TString Title = Form("%s Cross Section (E_{beam} = %.2f GeV, #theta = %.1f#circ)",TargetName.Data(),beamEnergy,theta/DEG_TO_RAD);
        TString xAxisTitle = Form("E (GeV)");

	/// F1F209 Model 
	F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
	fDiffXSec->SetBeamEnergy(beamEnergy);
	fDiffXSec->SetA(A);
	fDiffXSec->SetZ(Z);
	fDiffXSec->InitializePhaseSpaceVariables();
	fDiffXSec->InitializeFinalStateParticles();

	InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
	ps->ListVariables();
	Double_t * energy_e =  ps->GetVariableWithName("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e =  ps->GetVariableWithName("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e =  ps->GetVariableWithName("phi_e")->GetCurrentValueAddress();
	(*energy_e) = Eprime; 
	(*theta_e)  = theta;
	(*phi_e)    = phi;
	//fDiffXSec->EvaluateCurrent();
	//fDiffXSec->Print();
	//fDiffXSec->PrintTable();

        /// DIS Born cross section 
	InSANEPOLRADBornDiffXSec * fDiffXSec2 = new  InSANEPOLRADBornDiffXSec();
	fDiffXSec2->SetBeamEnergy(beamEnergy);
	fDiffXSec2->SetA(A);
	fDiffXSec2->SetZ(Z);
        fDiffXSec2->SetTargetType(TargetType);
	fDiffXSec2->InitializePhaseSpaceVariables();
	fDiffXSec2->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps2 = fDiffXSec2->GetPhaseSpace();
	ps2->ListVariables();
	Double_t * energy_e2 =  ps2->GetVariableWithName("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e2  =  ps2->GetVariableWithName("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e2    =  ps2->GetVariableWithName("phi_e")->GetCurrentValueAddress();
	(*energy_e2) = Eprime;  
	(*theta_e2)  = theta;
	(*phi_e2)    = phi; 
	//fDiffXSec2->EvaluateCurrent();
	//fDiffXSec2->Print();
	//fDiffXSec2->PrintTable();

	/// Elastic tail 
	InSANEPOLRADElasticTailDiffXSec * fDiffXSec3 = new  InSANEPOLRADElasticTailDiffXSec();
	fDiffXSec3->SetBeamEnergy(beamEnergy);
	fDiffXSec3->SetA(A);
	fDiffXSec3->SetZ(Z);
        fDiffXSec3->SetTargetType(TargetType);
	fDiffXSec3->InitializePhaseSpaceVariables();
	fDiffXSec3->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps3 = fDiffXSec3->GetPhaseSpace();
	ps3->ListVariables();
	Double_t * energy_e3 =  ps3->GetVariableWithName("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e3 =   ps3->GetVariableWithName("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e3 =     ps3->GetVariableWithName("phi_e")->GetCurrentValueAddress();
	(*energy_e3) = Eprime; 
	(*theta_e3)  = theta;
	(*phi_e3)    = phi;
	//fDiffXSec3->EvaluateCurrent();
	//fDiffXSec3->Print();
	//fDiffXSec3->PrintTable();

	/// Quasi-elastic tail 
	InSANEPOLRADQuasiElasticTailDiffXSec * fDiffXSec4 = new  InSANEPOLRADQuasiElasticTailDiffXSec();
	fDiffXSec4->SetBeamEnergy(beamEnergy);
	fDiffXSec4->SetA(A);
	fDiffXSec4->SetZ(Z);
        fDiffXSec4->SetTargetType(TargetType);
	fDiffXSec4->InitializePhaseSpaceVariables();
	fDiffXSec4->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
	ps4->ListVariables();
	Double_t * energy_e4 =  ps4->GetVariableWithName("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e4  =  ps4->GetVariableWithName("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e4    =  ps4->GetVariableWithName("phi_e")->GetCurrentValueAddress();
	(*energy_e4) = Eprime; 
	(*theta_e4)  = theta; 
	(*phi_e4)    = phi; 
	//fDiffXSec4->EvaluateCurrent();
	//fDiffXSec4->Print();
	//fDiffXSec4->PrintTable();

	/// Inelastic tail 
	InSANEPOLRADInelasticTailDiffXSec * fDiffXSec5 = new  InSANEPOLRADInelasticTailDiffXSec();
	fDiffXSec5->SetBeamEnergy(beamEnergy);
	fDiffXSec5->SetA(A);
	fDiffXSec5->SetZ(Z);
        fDiffXSec5->SetTargetType(TargetType);
	fDiffXSec5->InitializePhaseSpaceVariables();
	fDiffXSec5->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps5 = fDiffXSec5->GetPhaseSpace();
	ps4->ListVariables();
	Double_t * energy_e5 =  ps5->GetVariableWithName("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e5  =  ps5->GetVariableWithName("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e5    =  ps5->GetVariableWithName("phi_e")->GetCurrentValueAddress();
	(*energy_e5) = Eprime;  
	(*theta_e5)  = theta; 
	(*phi_e5)    = phi;
	//fDiffXSec5->EvaluateCurrent();
	//fDiffXSec5->Print();
	//fDiffXSec5->PrintTable();

	TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	sigmaE->SetParameter(0,theta);
	sigmaE->SetParameter(1,phi);
	sigmaE->SetLineColor(kRed);
	sigmaE->Draw();
        sigmaE->SetTitle(Title);
	sigmaE->GetXaxis()->SetTitle(xAxisTitle);
	sigmaE->GetXaxis()->CenterTitle(true);
	sigmaE->GetYaxis()->SetTitle(fDiffXSec->GetLabel());
	sigmaE->GetYaxis()->CenterTitle(true);
	sigmaE->Draw();

	TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	sigmaE2->SetParameter(0,theta);
	sigmaE2->SetParameter(1,phi);
	sigmaE2->SetLineColor(kBlue);
	sigmaE2->Draw("same");
        sigmaE2->SetTitle(Title);
	sigmaE2->GetXaxis()->SetTitle(xAxisTitle);
	sigmaE2->GetXaxis()->CenterTitle(true);
	sigmaE2->GetYaxis()->SetTitle(fDiffXSec->GetLabel());
	sigmaE2->GetYaxis()->CenterTitle(true);
	sigmaE->Draw();
	sigmaE2->Draw("same");

	TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	sigmaE3->SetParameter(0,theta);
	sigmaE3->SetParameter(1,phi);
	sigmaE3->SetLineColor(kBlue-9);
	sigmaE3->Draw("same");
        sigmaE3->SetTitle(Title);
	sigmaE3->GetXaxis()->SetTitle(xAxisTitle);
	sigmaE3->GetXaxis()->CenterTitle(true);
	sigmaE3->GetYaxis()->SetTitle(fDiffXSec->GetLabel());
	sigmaE3->GetYaxis()->CenterTitle(true);

	TF1 * sigmaE4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	sigmaE4->SetParameter(0,theta);
	sigmaE4->SetParameter(1,phi);
	sigmaE4->SetLineColor(kGreen-9);
	//sigmaE4->Draw("same");
        //sigmaE4->SetTitle(Title);
	//sigmaE4->GetXaxis()->SetTitle(xAxisTitle);
	//sigmaE4->GetXaxis()->CenterTitle(true);
	//sigmaE4->GetYaxis()->SetTitle(fDiffXSec->GetLabel());
	//sigmaE4->GetYaxis()->CenterTitle(true);

	TF1 * sigmaE5 = new TF1("sigma5", fDiffXSec5, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	sigmaE5->SetParameter(0,theta);
	sigmaE5->SetParameter(1,phi);
	sigmaE5->SetLineColor(kMagenta);
	//sigmaE5->Draw("same");
        //sigmaE5->SetTitle(Title);
	//sigmaE5->GetXaxis()->SetTitle("E");
	//sigmaE5->GetXaxis()->CenterTitle(true);
	//sigmaE5->GetYaxis()->SetTitle(fDiffXSec->GetLabel());
	//sigmaE5->GetYaxis()->CenterTitle(true);

	sigmaE->SetMinimum(1e-3);

	// gPad->SetLogy(true);
	// sigmaE->Draw();
	// sigmaE2->Draw("same");
	// sigmaE3->Draw("same");
	// sigmaE4->Draw("same");
	// sigmaE5->Draw("same");

	TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
	Leg->SetFillColor(kWhite);
	Leg->AddEntry(sigmaE,"F1F209","l");
	Leg->AddEntry(sigmaE2,"InSANE: POLRAD","l");
	Leg->AddEntry(sigmaE3,"ERT","l");
	Leg->AddEntry(sigmaE4,"QRT","l");
	Leg->AddEntry(sigmaE5,"IRT","l");

	Leg->Draw("same");

	return(0);
}
