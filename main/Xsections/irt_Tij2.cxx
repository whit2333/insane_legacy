/*! Plots the T function for k=2.
 */
Int_t irt_Tij2(){

   Double_t beamEnergy  = 5.9;
   Double_t theta       = 45.0*degree;//29.0*TMath::Pi()/180.0;
   Double_t Eprime      = beamEnergy*(1.-0.869);

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->GetPOLRAD()->SetTargetType(0);
   fDiffXSec4->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4          = fDiffXSec4->GetPhaseSpace();
   Double_t * energy_e4           = ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4            = ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4              = ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4)                   = Eprime;
   (*theta_e4)                    = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e4)                      = 0.0*TMath::Pi()/180.0;
   fDiffXSec4->EvaluateCurrent();

   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   InSANEPOLRAD * polrad    = fDiffXSec4->GetPOLRAD();
   Double_t tau_min         = polrad->GetKinematics()->GetTau_A_min();
   Double_t tau_max         = polrad->GetKinematics()->GetTau_A_max();
   Double_t xbj             = polrad->GetKinematics()->Getx();
   Double_t Q2              = polrad->GetKinematics()->GetQ2();
   Double_t Mp              = polrad->GetKinematics()->GetM();
   Double_t W2              = polrad->GetKinematics()->GetW2();
   Double_t nu              = polrad->GetKinematics()->Gety()*beamEnergy;
   Double_t tau_me_peak     = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
   Double_t tau_mp_peak     = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
   Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
   Double_t tau_u           = 0.67;
   Double_t t               = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

   //polrad->GetKinematics()->Print();
   std::cout << "tau_min = "         << tau_min         << "\n";
   std::cout << "tau_max = "         << tau_max         << "\n";
   std::cout << "Q2 = "              << Q2              << "\n";
   std::cout << "W2 = "              << W2              << "\n";
   std::cout << "Mp = "              << Mp              << "\n";
   std::cout << "xbj = "             << xbj             << "\n";
   std::cout << "tau_me_peak = "     << tau_me_peak     << "\n";
   std::cout << "tau_mp_peak = "     << tau_mp_peak     << "\n";
   std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
   std::cout << "t = "               << t               << "\n";

   vector<double> tau422,th422;
   vector<double> tau432,th432;
   vector<double> tau442,th442;
   vector<double> tau452,th452;
   vector<double> tau412,th412;
   vector<double> tau622,th622;
   vector<double> tau632,th632;

   ImportThData(4,1,2,tau412,th412);
   ImportThData(4,2,2,tau422,th422);
   ImportThData(4,3,2,tau432,th432);
   ImportThData(4,4,2,tau442,th442);
   ImportThData(6,2,2,tau622,th622);
   ImportThData(6,3,2,tau632,th632);

   TGraph *g412      = GetTGraph(tau412,th412);
   TGraph *g422      = GetTGraph(tau422,th422);
   TGraph *g432      = GetTGraph(tau432,th432);
   TGraph *g442      = GetTGraph(tau442,th442);
   //TGraph *g452      = GetTGraph(tau452,th452);
   TGraph *g622      = GetTGraph(tau622,th622);
   TGraph *g632      = GetTGraph(tau632,th632);

   Int_t width = 2; 
   g422->SetLineWidth(width);

   TF1 * fT412 = new TF1("fT412", polrad, &InSANEPOLRAD::TijkPlot, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot");
   fT412->SetParameter(0,4);
   fT412->SetParameter(1,1);
   fT412->SetParameter(2,2);
   fT412->SetNpx(5000);
   fT412->SetLineColor(kMagenta);
   fT412->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT422 = new TF1("fT422", polrad, &InSANEPOLRAD::TijkPlot, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot");
   fT422->SetParameter(0,4);
   fT422->SetParameter(1,2);
   fT422->SetParameter(2,2);
   fT422->SetNpx(5000);
   fT422->SetLineColor(kMagenta);
   fT422->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT432 = new TF1("fT432", polrad, &InSANEPOLRAD::TijkPlot, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot");
   fT432->SetParameter(0,4);
   fT432->SetParameter(1,3);
   fT432->SetParameter(2,2);
   fT432->SetNpx(5000);
   fT432->SetLineColor(kMagenta);
   fT432->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT442 = new TF1("fT442", polrad, &InSANEPOLRAD::TijkPlot, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot");
   fT442->SetParameter(0,4);
   fT442->SetParameter(1,4);
   fT442->SetParameter(2,2);
   fT442->SetNpx(5000);
   fT442->SetLineColor(kMagenta);
   fT442->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT622 = new TF1("fT622", polrad, &InSANEPOLRAD::TijkPlot, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot");
   fT622->SetParameter(0,6);
   fT622->SetParameter(1,2);
   fT622->SetParameter(2,2);
   fT622->SetNpx(5000);
   fT622->SetLineColor(kMagenta);
   fT622->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT632 = new TF1("fT622", polrad, &InSANEPOLRAD::TijkPlot, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot");
   fT632->SetParameter(0,6);
   fT632->SetParameter(1,3);
   fT632->SetParameter(2,2);
   fT632->SetNpx(5000);
   fT632->SetLineColor(kMagenta);
   fT632->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
   Leg->SetFillColor(kWhite); 
   Leg->AddEntry(g422 ,"Fortran","l");
   Leg->AddEntry(fT422,"C++"    ,"l");

   TCanvas *c1 = new TCanvas("c1","Theta i = 4 and 6, k = 2");
   c1->SetFillColor(kWhite);
   c1->Divide(3,2); 

   TString DrawOption = Form("AP"); 
   TString xAxisTitle = Form("#tau");

   Double_t min = -0.2E+3; 
   Double_t max =  0.5E+3; 

   c1->cd(1); 
   g422->Draw(DrawOption);
   g422->SetTitle("#theta_{422}");
   g422->GetXaxis()->SetTitle(xAxisTitle);
   g422->GetXaxis()->CenterTitle();
   g422->GetYaxis()->SetRangeUser(min,max);
   g422->Draw(DrawOption);
   fT422->Draw("same");
   c1->Update();

   c1->cd(2); 
   g432->Draw(DrawOption);
   g432->SetTitle("#theta_{432}");
   g432->GetXaxis()->SetTitle(xAxisTitle);
   g432->GetXaxis()->CenterTitle();
   g432->GetYaxis()->SetRangeUser(min,max);
   g432->Draw(DrawOption);
   fT432->Draw("same");
   c1->Update();

   c1->cd(3); 
   g442->Draw(DrawOption);
   g442->SetTitle("#theta_{442}");
   g442->GetXaxis()->SetTitle(xAxisTitle);
   g442->GetXaxis()->CenterTitle();
   g442->GetYaxis()->SetRangeUser(-0.1E+2,0.1E+3);
   g442->Draw(DrawOption);
   fT442->Draw("same");
   c1->Update();

   c1->cd(4); 
   g622->Draw(DrawOption);
   g622->SetTitle("#theta_{622}");
   g622->GetXaxis()->SetTitle(xAxisTitle);
   g622->GetXaxis()->CenterTitle();
   g622->GetYaxis()->SetRangeUser(min,max);
   g622->Draw(DrawOption);
   fT622->Draw("same");
   c1->Update();

   c1->cd(5); 
   g632->Draw(DrawOption);
   g632->SetTitle("#theta_{632}");
   g632->GetXaxis()->SetTitle(xAxisTitle);
   g632->GetXaxis()->CenterTitle();
   g622->GetYaxis()->SetRangeUser(-0.2e+2,max);
   g632->GetYaxis()->SetRangeUser(min,max);
   g632->Draw(DrawOption);
   fT632->Draw("same");
   c1->Update();

   c1->cd(6); 
   g412->Draw(DrawOption);
   g412->SetTitle("#theta_{412}");
   g412->GetXaxis()->SetTitle(xAxisTitle);
   g412->GetXaxis()->CenterTitle();
   g412->GetYaxis()->SetRangeUser(min,max);
   g412->Draw(DrawOption);
   fT412->Draw("same");
   c1->Update();

   Leg->Draw("same");
   c1->Update();

   fDiffXSec4->GetPOLRAD()->Print();
   fDiffXSec4->GetPOLRAD()->GetKinematics()->Print();

   c1->SaveAs("comparison_plots/ert_tij2.pdf");
   //c1->SaveAs("comparison_plots/ert_tij2.ps");
   //c1->SaveAs("comparison_plots/ert_tij2.png");

   return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportThData(int i,int j,int k,vector<double> &tau,vector<double> &th){

        double itau,ith;
        TString inpath = Form("./theta/irt/ijk/th_%d_%d_%d.dat",i,j,k);

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> ith;
                                tau.push_back(itau);
                                th.push_back(ith);
		}
                infile.close();
                tau.pop_back();
                th.pop_back();
	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
	min -= 1E+2; 
 	// if(min<-1E+2){
	// 	min -= 1E+4;
	// }else{
	// 	min -= 1E+2; 
        // } 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	max += 1E+2; 
	// if(max>1E+2){
	// 	max += 1E+4;
	// }else{
	// 	max += 1E+2; 
        // } 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}


