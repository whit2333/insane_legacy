Int_t dis_diff_xsec2(){

   Double_t beamEnergy =  5.0;
   Double_t theta      =  23.9*degree;
   Double_t Eprime     =  3.0;

   // Parameters for the plot are (theta,phi)
   Int_t    npar     = 2;
   Double_t E_min    = 0.80;
   Double_t E_max    = 3.5;
   Double_t minTheta = 15*degree;
   Double_t maxTheta = 55*degree;

   Double_t A = 1; 
   Double_t Z = 1; 

   InSANENucleus::NucleusType Target = InSANENucleus::kProton; 

   /// F1F209 Unpolarized Born cross section 
   F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetA(A);
   fDiffXSec->SetZ(Z);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
   //ps->ListVariables();
   Double_t * energy_e =  ps->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e  =  ps->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e    =  ps->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e)  = Eprime;
   (*theta_e)   = theta;
   (*phi_e)     = 0.0*degree;
   fDiffXSec->EvaluateCurrent();
   //fDiffXSec->Print();
   //fDiffXSec->PrintTable();

   /// Unpolarized Born cross section
   InSANEPOLRADBornDiffXSec * fDiffXSec2 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->GetPOLRAD()->SetHelicity(0);       // try the Born case first...
   fDiffXSec2->SetTargetType(Target);
   // fDiffXSec2->SetA(A);
   // fDiffXSec2->SetZ(Z);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps2 = fDiffXSec2->GetPhaseSpace();
   ps2->ListVariables();
   Double_t * energy_e2 =  ps2->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e2  =  ps2->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e2    =  ps2->GetVariable("phi_e")->GetCurrentValueAddress();
   ps2->GetVariable("energy_e")->SetModified(true);
   ps2->GetVariable("theta_e") ->SetModified(true);
   ps2->GetVariable("phi_e")   ->SetModified(true);
   (*energy_e2) = Eprime;
   (*theta_e2)  = theta;
   (*phi_e2)    = 0.0*degree;
   fDiffXSec2->EvaluateCurrent();
   //fDiffXSec2->Print();
   //fDiffXSec2->PrintTable();

   /// [Polarized] Born cross section
   InSANEPOLRADBornDiffXSec * fDiffXSec3 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   // fDiffXSec3->GetPOLRAD()->SetHelicity(-1);       
   fDiffXSec3->SetTargetType(Target);
   // fDiffXSec3->SetA(A);
   // fDiffXSec3->SetZ(Z);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps3 = fDiffXSec2->GetPhaseSpace();
   //ps2->ListVariables();
   ps2->Print();
   Double_t * energy_e3 =  ps3->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e3  =  ps3->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e3    =  ps3->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e3) = Eprime;
   (*theta_e3)  = theta;
   (*phi_e3)    = 0.0*degree;
   std::cout << "Phase Space Error:\n" ; 
   fDiffXSec3->EvaluateCurrent();
   //fDiffXSec3->Print();
   //fDiffXSec3->PrintTable();

   /// [Polarized] Born cross section
   InSANEPOLRADBornDiffXSec * fDiffXSec4 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->GetPOLRAD()->SetHelicity(-1);       
   fDiffXSec4->SetTargetType(Target);
   // fDiffXSec4->SetA(A);
   // fDiffXSec4->SetZ(Z);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec2->GetPhaseSpace();
   //ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4  =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4    =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4)  = theta;
   (*phi_e4)    = 0.0*degree;
   std::cout << "Phase Space Error:\n" ; 
   fDiffXSec4->EvaluateCurrent();
   //fDiffXSec4->Print();
   //fDiffXSec4->PrintTable();

   /// Elastic Radiative Tail
   InSANEPOLRADElasticTailDiffXSec * fDiffXSec5 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec5->SetBeamEnergy(beamEnergy);
   fDiffXSec5->SetTargetType(Target);
   // fDiffXSec5->SetA(A);
   // fDiffXSec5->SetZ(Z);
   fDiffXSec5->InitializePhaseSpaceVariables();
   fDiffXSec5->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps5 = fDiffXSec3->GetPhaseSpace();
   //ps5->ListVariables();
   Double_t * energy_e5 =  ps5->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e5  =  ps5->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e5    =  ps5->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e5) = Eprime;
   (*theta_e5)  = theta;
   (*phi_e5)    = 0.0*degree;
   fDiffXSec5->EvaluateCurrent();
   //fDiffXSec5->Print();
   //fDiffXSec5->PrintTable();

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec6 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec6->SetBeamEnergy(beamEnergy);
   fDiffXSec6->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec6->SetTargetType(Target);
   // fDiffXSec6->SetA(A);
   // fDiffXSec6->SetZ(Z);
   fDiffXSec6->InitializePhaseSpaceVariables();
   fDiffXSec6->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps6 = fDiffXSec4->GetPhaseSpace();
   //ps6->ListVariables();
   Double_t * energy_e6 =  ps6->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e6  =  ps6->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e6    =  ps6->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e6) = Eprime;
   (*theta_e6)  = theta;
   (*phi_e6)    = 0.0*degree;
   fDiffXSec6->EvaluateCurrent();
   //fDiffXSec6->Print();
   //fDiffXSec6->PrintTable();

   //return(0);
   //-------------------------------------------
   TCanvas * c = new TCanvas("dis_diff_xsec_nu","polrad",800,600);

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,0.0);
   sigmaE->SetLineColor(kRed);


   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,0.0);
   sigmaE2->SetLineColor(kBlue);

   TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE3->SetParameter(0,theta);
   sigmaE3->SetParameter(1,0.0);
   sigmaE3->SetLineColor(kCyan+1);

   TF1 * sigmaE4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE4->SetParameter(0,theta);
   sigmaE4->SetParameter(1,0.0);
   sigmaE4->SetLineColor(kCyan-1);


   TF1 * sigmaE5 = new TF1("sigma5", fDiffXSec5, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max,  npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE5->SetParameter(0,theta);
   sigmaE5->SetParameter(1,0.0);
   sigmaE5->SetLineColor(kBlue-9);


   TF1 * sigmaE6 = new TF1("sigma6", fDiffXSec6, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE6->SetParameter(0,theta);
   sigmaE6->SetParameter(1,0.0);
   sigmaE6->SetLineColor(kGreen-9);

   //-------------------------------------------

   sigmaE->SetMinimum(1e-2);
   sigmaE->SetMaximum(1e3);


   gPad->SetLogy(false);
   sigmaE->Draw();
   sigmaE->GetXaxis()->SetTitle("E'");
   sigmaE->GetXaxis()->CenterTitle(true);
   sigmaE->GetYaxis()->SetTitle(fDiffXSec->GetLabel());
   sigmaE->GetYaxis()->CenterTitle(true);

   sigmaE2->Draw("same");
   sigmaE3->Draw("same");
   sigmaE4->Draw("same");
   sigmaE5->Draw("same");
   sigmaE6->Draw("same");

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);

   Leg->SetFillColor(kWhite);
   Leg->AddEntry(sigmaE ,"F1F209 Born Unpol"        ,"l");
   Leg->AddEntry(sigmaE2,"InSANE: POLRAD Born Unpol","l");
   Leg->AddEntry(sigmaE3,"InSANE: POLRAD Born (+)"  ,"l");
   Leg->AddEntry(sigmaE4,"InSANE: POLRAD Born (-)"  ,"l");
   Leg->AddEntry(sigmaE5,"ERT (+)"                  ,"l");
   Leg->AddEntry(sigmaE6,"ERT (-)"                  ,"l");
   Leg->Draw("same");

   return(0);
}
