

Int_t ert_b_and_c(){

   Double_t beamEnergy  = 5.9;
   Double_t theta       = 45.0*degree;//29.0*TMath::Pi()/180.0;
   Double_t Eprime      = beamEnergy*(1.-0.869);

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->GetPOLRAD()->SetTargetType(0);
   fDiffXSec4->GetPOLRAD()->SetHelicity(-1.);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
   //ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4 =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4 =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4) = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e4) = 0.0*TMath::Pi()/180.0;
   fDiffXSec4->EvaluateCurrent();
   //fDiffXSec4->Print();
   //fDiffXSec4->PrintTable();

   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   InSANEPOLRAD * polrad = fDiffXSec4->GetPOLRAD();
   Double_t tau_min = polrad->GetKinematics()->GetTau_min();
   Double_t tau_max = polrad->GetKinematics()->GetTau_max();

   // cout << "tau min = " << tau_min << endl;
   // cout << "tau max = " << tau_max << endl;
   // exit(1); 
 
   Double_t xbj = polrad->GetKinematics()->Getx();
   Double_t Q2  = polrad->GetKinematics()->GetQ2();
   Double_t Mp  = polrad->GetKinematics()->GetM();
   Double_t W2  = polrad->GetKinematics()->GetW2();
   Double_t nu  = polrad->GetKinematics()->Gety()*beamEnergy;
   Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
   Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
   Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
   Double_t tau_u = 0.67;
   Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

   //polrad->GetKinematics()->Print();
   std::cout << "tau_min = " << tau_min << "\n";
   std::cout << "tau_max = " << tau_max << "\n";
   std::cout << "Q2 = " << Q2 << "\n";
   std::cout << "W2 = " << W2 << "\n";
   std::cout << "Mp = " << Mp << "\n";
   std::cout << "xbj = " << xbj << "\n";
   std::cout << "tau_me_peak = " << tau_me_peak << "\n";
   std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
   std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
   std::cout << "t = " << t << "\n";

   vector<double> tauBC,B1,B2,C1,C2; 

   ImportBCData(tauBC,B1,B2,C1,C2); 

   Int_t width = 2; 

   TGraph *gB1       = GetTGraph(tauBC,B1); 
   TGraph *gB2       = GetTGraph(tauBC,B2); 
   TGraph *gC1       = GetTGraph(tauBC,C1); 
   TGraph *gC2       = GetTGraph(tauBC,C2); 

   gB1->SetLineWidth(width);

   TF1 * fB1 = new TF1("fB1", polrad, &InSANEPOLRAD::B1_A_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","B1_A_Plot");
   fB1->SetNpx(5000);
   fB1->SetLineColor(kMagenta);
   fB1->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fB2 = new TF1("fB2", polrad, &InSANEPOLRAD::B2_A_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","B2_A_Plot");
   fB2->SetNpx(5000);
   fB2->SetLineColor(kMagenta);
   fB2->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fC1 = new TF1("fC1", polrad, &InSANEPOLRAD::C1_A_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","C1_A_Plot");
   fC1->SetNpx(5000);
   fC1->SetLineColor(kMagenta);
   fC1->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fC2 = new TF1("fC2", polrad, &InSANEPOLRAD::C2_A_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","C2_A_Plot");
   fC2->SetNpx(5000);
   fC2->SetLineColor(kMagenta);
   fC2->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   Double_t min = -1E+05;
   Double_t max =  1E+05;

   TString DrawOption = Form("AP");
   TString xAxisTitle = Form("#tau");

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
   Leg->SetFillColor(kWhite); 
   Leg->AddEntry(gB1,"Fortran","l");
   Leg->AddEntry(fB1,"C++"    ,"l");

   TCanvas *c1 = new TCanvas("c1","B and C Terms",1000,800);
   c1->SetFillColor(kWhite);
   c1->Divide(2,2); 

   min = GetMin(B1);
   max = GetMax(B1);

   c1->cd(1); 
   gB1->Draw(DrawOption);
   gB1->SetTitle("B_{1}^{A}");
   gB1->GetXaxis()->SetTitle(xAxisTitle);
   gB1->GetXaxis()->CenterTitle();
   gB1->GetYaxis()->SetRangeUser(min,max);
   gB1->Draw(DrawOption);
   fB1->Draw("same");
   Leg->Draw("same");
   c1->Update();

   min = GetMin(B2);
   max = GetMax(B2);

   c1->cd(2); 
   gB2->Draw(DrawOption);
   gB2->SetTitle("B_{2}^{A}");
   gB2->GetXaxis()->SetTitle(xAxisTitle);
   gB2->GetXaxis()->CenterTitle();
   gB2->GetYaxis()->SetRangeUser(min,max);
   gB2->Draw(DrawOption);
   fB2->Draw("same");
   c1->Update();

   min = GetMin(C1);
   max = GetMax(C1);

   c1->cd(3); 
   gC1->Draw(DrawOption);
   gC1->SetTitle("C_{1}^{A}");
   gC1->GetXaxis()->SetTitle(xAxisTitle);
   gC1->GetXaxis()->CenterTitle();
   gC1->GetYaxis()->SetRangeUser(min,max);
   gC1->Draw(DrawOption);
   fC1->Draw("same");
   c1->Update();

   min = GetMin(C2);
   max = GetMax(C2);

   c1->cd(4); 
   gC2->Draw(DrawOption);
   gC2->SetTitle("C_{2}^{A}");
   gC2->GetXaxis()->SetTitle(xAxisTitle);
   gC2->GetXaxis()->CenterTitle();
   gC2->GetYaxis()->SetRangeUser(min,max);
   gC2->Draw(DrawOption);
   fC2->Draw("same");
   c1->Update();

   fDiffXSec4->GetPOLRAD()->Print();
   fDiffXSec4->GetPOLRAD()->GetKinematics()->Print();

   return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportBCData(vector<double> &tau,vector<double> &B1,vector<double> &B2,vector<double> &C1,vector<double> &C2){

        double itau,iy1,iy2,iy3,iy4;
        TString inpath = Form("./dump/dump_b_and_c.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3 >> iy4;
                                tau.push_back(itau);
                                B1.push_back(iy1);
                                B2.push_back(iy2);
                                C1.push_back(iy3);
                                C2.push_back(iy4);
		}
                infile.close();
                tau.pop_back();
                B1.pop_back();
                B2.pop_back();
		C1.pop_back();
                C2.pop_back();

	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
	min -= 1E+2; 
 	// if(min<-1E+2){
	// 	min -= 1E+4;
	// }else{
	// 	min -= 1E+2; 
        // } 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	max += 1E+2; 
	// if(max>1E+2){
	// 	max += 1E+4;
	// }else{
	// 	max += 1E+2; 
        // } 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}

