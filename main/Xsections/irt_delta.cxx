

Int_t irt_delta(){

   Double_t beamEnergy  = 5.9;
   Double_t theta       = 45.0*degree;
   Double_t Eprime      = beamEnergy*(1.-0.869);

   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec1= new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->GetPOLRAD()->SetTargetType(0);
   fDiffXSec1->GetPOLRAD()->SetHelicity(-1.);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps1 = fDiffXSec1->GetPhaseSpace();
   //ps1->ListVariables();
   Double_t * energy_e1 =  ps1->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e1  =  ps1->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e1    =  ps1->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e1) = Eprime;
   (*theta_e1)  = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e1)    = 0.0*degree;
   fDiffXSec1->EvaluateCurrent();
   //fDiffXSec1->Print();
   //fDiffXSec1->PrintTable();

   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   InSANEPOLRAD * polrad = fDiffXSec1->GetPOLRAD();
   Double_t tau_min = polrad->GetKinematics()->GetTau_min();
   Double_t tau_max = polrad->GetKinematics()->GetTau_max();

   // cout << "tau min = " << tau_min << endl;
   // cout << "tau max = " << tau_max << endl;
   // exit(1); 
 
   Double_t xbj = polrad->GetKinematics()->Getx();
   Double_t Q2  = polrad->GetKinematics()->GetQ2();
   Double_t Mp  = polrad->GetKinematics()->GetM();
   Double_t W2  = polrad->GetKinematics()->GetW2();
   Double_t nu  = polrad->GetKinematics()->Gety()*beamEnergy;
   Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
   Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
   Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
   Double_t tau_u = 0.67;
   Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

   //polrad->GetKinematics()->Print();
   std::cout << "tau_min = " << tau_min << "\n";
   std::cout << "tau_max = " << tau_max << "\n";
   std::cout << "Q2 = " << Q2 << "\n";
   std::cout << "W2 = " << W2 << "\n";
   std::cout << "Mp = " << Mp << "\n";
   std::cout << "xbj = " << xbj << "\n";
   std::cout << "tau_me_peak = " << tau_me_peak << "\n";
   std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
   std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
   std::cout << "t = " << t << "\n";

   vector<double> tau,DeltaVert,DeltaVacL,DeltaVacH,DeltaRIR,Sphi; 

   ImportDeltaData(tau,DeltaVert,DeltaVacL,DeltaVacH,DeltaRIR,Sphi); 

   Int_t width = 2; 

   TGraph *gDeltaVert = GetTGraph(tau,DeltaVert);
   TGraph *gDeltaVacL = GetTGraph(tau,DeltaVacL);
   TGraph *gDeltaVacH = GetTGraph(tau,DeltaVacH);
   TGraph *gDeltaRIR  = GetTGraph(tau,DeltaRIR);
   TGraph *gSphi      = GetTGraph(tau,Sphi);
  
   gDeltaVert->SetLineWidth(width);

   TF1 * fDeltaVert = new TF1("fDeltaVert", polrad, &InSANEPOLRAD::delta_vert_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","delta_vert_Plot");
   fDeltaVert->SetNpx(5000);
   fDeltaVert->SetLineColor(kMagenta);
   fDeltaVert->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fDeltaVacL = new TF1("fDeltaVacL", polrad, &InSANEPOLRAD::delta_vac_l_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","delta_vac_l_Plot");
   fDeltaVacL->SetNpx(5000);
   fDeltaVacL->SetLineColor(kMagenta);
   fDeltaVacL->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fDeltaVacH = new TF1("fDeltaVacH", polrad, &InSANEPOLRAD::delta_vac_h_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","delta_vac_h_Plot");
   fDeltaVacH->SetNpx(5000);
   fDeltaVacH->SetLineColor(kMagenta);
   fDeltaVacH->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fDeltaRIR = new TF1("fDeltaRIR", polrad, &InSANEPOLRAD::delta_R_IR_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","delta_R_IR_Plot");
   fDeltaRIR->SetNpx(5000);
   fDeltaRIR->SetLineColor(kMagenta);
   fDeltaRIR->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fSphi = new TF1("fSphi", polrad, &InSANEPOLRAD::s_phi_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","s_phi_Plot");
   fSphi->SetNpx(5000);
   fSphi->SetLineColor(kMagenta);
   fSphi->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
   Leg->SetFillColor(kWhite); 
   Leg->AddEntry(gDeltaVert,"Fortran","l");
   Leg->AddEntry(fDeltaVert,"C++"    ,"l");

   Double_t min = -3E+02;
   Double_t max =  3E+02;

   TString DrawOption = Form("AP"); 
   TString xAxisTitle = Form("#tau");

   TCanvas *c1 = new TCanvas("c1","F Terms",1000,800);
   c1->SetFillColor(kWhite);
   c1->Divide(3,2); 

   // min = GetMin(DeltaVert);
   // max = GetMax(DeltaVert);

   c1->cd(1); 
   gDeltaVert->Draw(DrawOption);
   gDeltaVert->SetTitle("#delta_{vert}");
   gDeltaVert->GetXaxis()->SetTitle(xAxisTitle);
   gDeltaVert->GetXaxis()->CenterTitle();
   gDeltaVert->GetYaxis()->SetRangeUser(min,max);
   gDeltaVert->Draw(DrawOption);
   fDeltaVert->Draw("same");
   c1->Update();

   // min = GetMin(DeltaVac);
   // max = GetMax(DeltaVac);

   c1->cd(2); 
   gDeltaVacL->Draw(DrawOption);
   gDeltaVacL->SetTitle("#delta_{vac}^{l}");
   gDeltaVacL->GetXaxis()->SetTitle(xAxisTitle);
   gDeltaVacL->GetXaxis()->CenterTitle();
   gDeltaVacL->GetYaxis()->SetRangeUser(min,max);
   gDeltaVacL->Draw(DrawOption);
   fDeltaVacL->Draw("same");
   c1->Update();

   c1->cd(3); 
   gDeltaVacH->Draw(DrawOption);
   gDeltaVacH->SetTitle("#delta_{vac}^{h}");
   gDeltaVacH->GetXaxis()->SetTitle(xAxisTitle);
   gDeltaVacH->GetXaxis()->CenterTitle();
   gDeltaVacH->GetYaxis()->SetRangeUser(min,max);
   gDeltaVacH->Draw(DrawOption);
   fDeltaVacH->Draw("same");
   c1->Update();

   // min = GetMin(DeltaRIR);
   // max = GetMax(DeltaRIR);

   c1->cd(4); 
   gDeltaRIR->Draw(DrawOption);
   gDeltaRIR->SetTitle("#delta_{R}^{IR}");
   gDeltaRIR->GetXaxis()->SetTitle(xAxisTitle);
   gDeltaRIR->GetXaxis()->CenterTitle();
   gDeltaRIR->GetYaxis()->SetRangeUser(min,max);
   gDeltaRIR->Draw(DrawOption);
   fDeltaRIR->Draw("same");
   c1->Update();

   c1->cd(5); 
   gSphi->Draw(DrawOption);
   gSphi->SetTitle("S_{#phi}");
   gSphi->GetXaxis()->SetTitle(xAxisTitle);
   gSphi->GetXaxis()->CenterTitle();
   gSphi->GetYaxis()->SetRangeUser(min,max);
   gSphi->Draw(DrawOption);
   fSphi->Draw("same");
   c1->Update();

   c1->cd(6); 
   Leg->Draw("same");
   c1->Update();

   fDiffXSec1->GetPOLRAD()->Print();
   fDiffXSec1->GetPOLRAD()->GetKinematics()->Print();

   return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportDeltaData(vector<double> &tau,vector<double> &DeltaVert,vector<double> &DeltaVacL,vector<double> &DeltaVacH,vector<double> &DeltaRIR,vector<double> &Sphi){

        double itau,iy1,iy2,iy3,iy4,iy5;
        TString inpath = Form("./dump/dump_delta.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3 >> iy4 >> iy5;
                                tau.push_back(itau);
                                DeltaVert.push_back(iy1);
                                DeltaVacL.push_back(iy2);
                                DeltaVacH.push_back(iy3);
                                DeltaRIR.push_back(iy4);
                                Sphi.push_back(iy5);
		}
                infile.close();
                tau.pop_back();
                DeltaVert.pop_back();
                DeltaVacL.pop_back();
                DeltaVacH.pop_back();
                DeltaRIR.pop_back();
                Sphi.pop_back();
	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
	min -= 1E+2; 
 	// if(min<-1E+2){
	// 	min -= 1E+4;
	// }else{
	// 	min -= 1E+2; 
        // } 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	max += 1E+2; 
	// if(max>1E+2){
	// 	max += 1E+4;
	// }else{
	// 	max += 1E+2; 
        // } 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}


