/*! Script showing how to create and event generator for double polarization cross sections.
 *
 */
Int_t grid_xsecs() {

   Double_t theta_target = 180.0*degree;
   Double_t fBeamEnergy = 5.9; 
   Double_t Emin = 0.3;
   Double_t Emax = 3.5;
   Double_t theta = 35.0*degree;
   Int_t npar = 2;
   Int_t npx = 50;
   Bool_t isLogY = false;//true; 
   TString grid_file = "grid/sigma_p_unpol_polrad_int_fixed.txt";

   //------------------
   //InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   //cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());

   InSANEStructureFunctionsFromPDFs * SFs = new InSANEStructureFunctionsFromPDFs();
   SFs->SetUnpolarizedPDFs(new BBSUnpolarizedPDFs());

   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs(new BBSPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new DSSVPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new BBPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new DNS2005PolarizedPDFs());

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();
   fm->SetPolarizedStructureFunctions(pSFs);
   //fm->SetStructureFunctions(SFs);

   //------------------

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);

   InSANEPhaseSpace * fPolarizedPhaseSpace = new InSANEPhaseSpace();
   InSANEPhaseSpace * fPhaseSpace          = new InSANEPhaseSpace();

   InSANEPhaseSpaceVariable * varEnergy = new InSANEPhaseSpaceVariable("Energy","E#prime"); 
   varEnergy->SetMinimum(Emin);         //GeV
   varEnergy->SetMaximum(Emax); //GeV
   varEnergy->SetVariableUnits("GeV");        //GeV
   fPolarizedPhaseSpace->AddVariable(varEnergy);
   fPhaseSpace->AddVariable(varEnergy);

   InSANEPhaseSpaceVariable *   varTheta = new InSANEPhaseSpaceVariable("theta","#theta");
   varTheta->SetMinimum(30.0*TMath::Pi()/180.0); //
   varTheta->SetMaximum(50.0*TMath::Pi()/180.0); //
   varTheta->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varTheta);
   fPhaseSpace->AddVariable(varTheta);

   InSANEPhaseSpaceVariable *   varPhi = new InSANEPhaseSpaceVariable("phi","#phi");
   varPhi->SetMinimum(-90.0*TMath::Pi()/180.0); //
   varPhi->SetMaximum(90.0*TMath::Pi()/180.0); //
   varPhi->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varPhi);
   fPhaseSpace->AddVariable(varPhi);

   InSANEDiscretePhaseSpaceVariable *   varHelicity = new InSANEDiscretePhaseSpaceVariable("helicity","#lambda");
   varHelicity->SetNumberOfValues(3); // ROOT string latex
   fPolarizedPhaseSpace->AddVariable(varHelicity);

   fPolarizedPhaseSpace->Refresh();

   varEnergy->Print();
   varTheta->Print();
   varPhi->Print();
   varHelicity->Print();

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();

   // --- Cross-sections ---
   // ---------------------- 
   // born unpolarized 
   //F1F209eInclusiveDiffXSec * fDiffXSec00 = new  F1F209eInclusiveDiffXSec();
   InSANEPOLRADBornDiffXSec * fDiffXSec00 = new  InSANEPOLRADBornDiffXSec();
   //InSANEPOLRADRadiatedDiffXSec * fDiffXSec00 = new  InSANEPOLRADRadiatedDiffXSec();
   fDiffXSec00->GetPOLRAD()->SetHelicity(0);
   fDiffXSec00->SetBeamEnergy(fBeamEnergy);
   fDiffXSec00->SetA(1);
   fDiffXSec00->SetZ(1);
   fDiffXSec00->SetPhaseSpace(fPhaseSpace);
   fDiffXSec00->Refresh();
   //ofstream fi("xsec_test.txt");
   //fDiffXSec00->PrintGrid(fi,20);


   // internal radiated unpolarized IRT 
   InSANEGridDiffXSec * fDiffXSec01 = new InSANEGridDiffXSec(grid_file); 
   fDiffXSec01->SetBeamEnergy(fBeamEnergy);
   fDiffXSec01->SetPhaseSpace(fPhaseSpace);
   fDiffXSec01->Refresh();

   // ---------------------- 

   fDiffXSec00->Print();
   fDiffXSec01->Print();
   //fDiffXSec02->Print();
   //fDiffXSec03->Print();
   //fDiffXSec10->Print();
   //fDiffXSec11->Print();
   //fDiffXSec12->Print();
   //fDiffXSec20->Print();
   //fDiffXSec21->Print();
   //fDiffXSec22->Print();

   // ---------------------- 
   // functions
 
   // ---------------------- 
   // Born unpolarized
   TF1 * sig00 = new TF1("sig00",fDiffXSec00,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig00->SetParameter(0,theta);
   sig00->SetParameter(1,0.0);
   sig00->SetNpx(npx);
   sig00->SetLineStyle(2);
   sig00->SetLineWidth(2);
   sig00->SetLineColor(kBlack);
   leg->AddEntry(sig00,"Born unpolarized","l");
   
   TF1 * sig01 = new TF1("sig01",fDiffXSec01,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig01->SetNpx(npx);
   sig01->SetParameter(0,theta);
   sig01->SetParameter(1,0.0);
   sig01->SetLineStyle(1);
   sig01->SetLineWidth(1);
   sig01->SetLineColor(2);
   leg->AddEntry(sig01,"Internally radiated (polrad) unpolarized IRT","l");

   // ---------------------- 
   TCanvas * c = new TCanvas("polXsecCompare","polXsecCompare");
   gPad->SetLogy(isLogY);

   sig00->Draw();
   TAxis * xAxis = sig00->GetXaxis(); 
   TAxis * yAxis = sig00->GetYaxis(); 

   xAxis->SetTitle("E'");
   xAxis->CenterTitle(true);
   yAxis->SetTitle("#frac{d#sigma}{dE'd#Omega} (nb/GeV/sr)");
   yAxis->CenterTitle(true);
   if(isLogY)yAxis->SetRangeUser(0.0001,100.0);

   sig00->Draw();
   sig01->Draw("same");

   leg->Draw();

   //c->SaveAs("results/polrad_xsecs.png");
   //c->SaveAs("results/polrad_xsecs.pdf");

   return(0);
}

