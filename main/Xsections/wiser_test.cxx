Int_t wiser_test(Int_t particle=211,Int_t canpad=1){

   TCanvas * c = (TCanvas*) gROOT->FindObject("wiserCanvas");
   if(!c){
      c = new TCanvas("wiserCanvas","Wiser cross sections");
      c->Divide(1,2);
   }

   c->cd(1);
   NucDBManager * manager = NucDBManager::GetManager();

   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("The Legend Title");

TList * measurementsList = manager->GetMeasurements("sig_p(pi+)");
for( int i=0; i< measurementsList->GetEntries();i++) {
   NucDBMeasurement * ameas = (NucDBMeasurement*)measurementsList->At(i);
   ameas->BuildGraph("z");
   leg->AddEntry(ameas->fGraph,Form("%s %s",ameas->fExperiment.Data(),ameas->GetName()),"ep");
   if(i==0)ameas->fGraph->Draw("ap");
   ameas->fGraph->Draw("p");
}

TList * measurementsList = manager->GetMeasurements("sig_p(pi-)");
for( int i=0; i< measurementsList->GetEntries();i++) {
   NucDBMeasurement * ameas = (NucDBMeasurement*)measurementsList->At(i);
   ameas->BuildGraph("z");
   leg->AddEntry(ameas->fGraph,Form("%s %s",ameas->fExperiment.Data(),ameas->GetName()),"ep");
   if(i==0)ameas->fGraph->Draw("p");
   ameas->fGraph->Draw("p");
}

TList * measurementsList = manager->GetMeasurements("sig_d(pi+)");
for( int i=0; i< measurementsList->GetEntries();i++) {
   NucDBMeasurement * ameas = (NucDBMeasurement*)measurementsList->At(i);
   ameas->BuildGraph("z");
   leg->AddEntry(ameas->fGraph,Form("%s %s",ameas->fExperiment.Data(),ameas->GetName()),"ep");
   if(i==0)ameas->fGraph->Draw("p");
   ameas->fGraph->Draw("p");
}

TList * measurementsList = manager->GetMeasurements("sig_d(pi-)");
for( int i=0; i< measurementsList->GetEntries();i++) {
   NucDBMeasurement * ameas = (NucDBMeasurement*)measurementsList->At(i);
   ameas->BuildGraph("z");
   leg->AddEntry(ameas->fGraph,Form("%s %s",ameas->fExperiment.Data(),ameas->GetName()),"ep");
   if(i==0)ameas->fGraph->Draw("p");
   ameas->fGraph->Draw("p");
}

   InSANEInclusiveWiserXSec * fDiffXSec = new InSANEInclusiveWiserXSec();
   fDiffXSec->SetBeamEnergy(5.9);
   fDiffXSec->SetProductionParticleType(particle);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace * fPhaseSpace = fDiffXSec->GetPhaseSpace();

//    fDiffXSec->SetPhaseSpace(fPhaseSpace);

   c->cd(2);

   Int_t npar = 5;
   TF1 * f1copy=0;
//    TF1 * xF1 = new TF1("wiser", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyFractionDepXSec, 
//                0.1,0.9, npar,"InSANEInclusiveDiffXSec","EnergyFractionDepXSec");
   Double_t Eprime = 2.0;//fPhaseSpace->GetVariable("energy_e")->GetCentralValue();
   Double_t thetaEprime = 0.1;//fPhaseSpace->GetVariable("theta_e")->GetCentralValue();
//    xF1->SetParameters(Eprime,thetaEprime,0.0,20.0*TMath::Pi()/180.0,0.0);
//    xF1->SetTitle(fDiffXSec->fPlotTitle.Data());
//    xF1->Draw();
//    std::cout << " Ecentral,thetaCentral = " << Eprime << "," << thetaEprime*180.0/TMath::Pi() << "\n";
   c->cd(2);

   npar = 5;
//    TF1 * sigElecProd = new TF1("electroWiser", fDiffXSec, &InSANEInclusiveWiserXSec::ElectroProductionXSec, 
//                0.1,0.9, npar,"InSANEInclusiveWiserXSec","ElectroProductionXSec");
// 
//    sigElecProd->SetParameters(Eprime,thetaEprime,0.0,30.0*TMath::Pi()/180.0,0.0);
//    sigElecProd->SetTitle(fDiffXSec->fPlotTitle.Data());
//    sigElecProd->Draw();

/*
   xF1->GetYaxis()->SetTitle(fDiffXSec->fPlotTitle.Data());
   TH1 * h1 = sigElecProd->GetHistogram();
   h1->Scale(1000000.0);
   h1->Draw("same");*/
/*
   xF1->SetParameters(30.0*TMath::Pi()/180.0,0.0);
   f1copy = xF1->DrawCopy("goff,same");
   f1copy->GetYaxis()->SetTitle(fDiffXSec->fPlotTitle.Data());
   f1copy->Draw("same");

   xF1->SetParameters(40.0*TMath::Pi()/180.0,0.0);
   f1copy = xF1->DrawCopy("goff,same");
   f1copy->GetYaxis()->SetTitle(fDiffXSec->fPlotTitle.Data());
   f1copy->Draw("same");

   xF1->SetParameters(50.0*TMath::Pi()/180.0,0.0);
   f1copy = xF1->DrawCopy("goff,same");
   f1copy->GetYaxis()->SetTitle(fDiffXSec->fPlotTitle.Data());
   f1copy->Draw("same");*/

//    xF1->GetYaxis()->SetTitle(fDiffXSec->fPlotTitle.Data());


//   fDiffXSec->Plot(30);

   c->cd(1);
   leg->Draw();
   c->Flush();
   c->SaveAs("results/pionPlus_production.png");

return(0);
}

