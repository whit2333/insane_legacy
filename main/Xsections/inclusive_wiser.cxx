{

   TCanvas * c = (TCanvas*) gROOT->FindObject("wiserCanvas");
   if(!c){
      c = new TCanvas("wiserCanvas","Inclusive Wiser cross sections");
      c->Divide(1,2);
   }

   c->cd(1);

   InSANEInclusiveWiserXSec * fDiffXSec = new InSANEInclusiveWiserXSec();
   fDiffXSec->SetBeamEnergy(5.9);
   fDiffXSec->SetProductionParticleType(111);
   fDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace * fPhaseSpace = fDiffXSec->GetPhaseSpace();

   c->cd(2);
   Int_t npar = 2;
   TF1 * incWiserXSec = new TF1("incWiser", fDiffXSec,
                                &InSANEInclusiveDiffXSec::MomentumDependentXSec, 
                                0.1,1.0, npar,
                                "InSANEInclusiveDiffXSec","MomentumDependentXSec");\

   incWiserXSec->SetParameters(30.0*TMath::Pi()/180.0,0.0);
   incWiserXSec->SetTitle(fDiffXSec->fPlotTitle.Data());
   incWiserXSec->DrawClone();

   fDiffXSec->SetProductionParticleType(211);
   incWiserXSec->SetLineColor(1);
   incWiserXSec->DrawClone("same");

   fDiffXSec->SetProductionParticleType(-211);
   incWiserXSec->SetLineColor(2);
   incWiserXSec->DrawClone("same");


}