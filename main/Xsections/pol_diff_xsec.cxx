
void pol_diff_xsec(){

        Int_t npar      = 2; 
        Double_t Ebeam  = 4.73;
        Double_t Eprime = 1.0;  
        Double_t EpMin  = 0.5; 
        Double_t EpMax  = 2.0; 
        Double_t theta  = 45.*degree;
        Double_t phi    = 0.*degree;

        cout << "Enter beam energy (GeV): "; 
        cin  >> Ebeam; 
        cout << "Enter Ep(min) (GeV): "; 
        cin  >> EpMin; 
        cout << "Enter Ep(max) (GeV): "; 
        cin  >> EpMax;

        Eprime = (EpMax+EpMin)/2.; 
 
        InSANENucleus::NucleusType Target = InSANENucleus::k3He; 
        // InSANENucleus::NucleusType Target = InSANENucleus::kProton; 

        Int_t width = 2; 

        DSSVPolarizedPDFs *DSSV      = new DSSVPolarizedPDFs(); 
        DNS2005PolarizedPDFs *DNS    = new DNS2005PolarizedPDFs(); 
        BBSPolarizedPDFs *BBSPol     = new BBSPolarizedPDFs(); 
        BBSPol->UseQ2Interpolation(); 

        // BBSUnpolarizedPDFs *BBSUnpol = new BBSUnpolarizedPDFs(); 
        // BBSUnpol->UseQ2Interpolation(); 

        // InSANEStructureFunctionsFromPDFs *UnpolSFs = new InSANEStructureFunctionsFromPDFs(); 
        // UnpolSFs->SetUnpolarizedPDFs(BBSUnpol);

        InSANEPolarizedStructureFunctionsFromPDFs *PolSFs = new InSANEPolarizedStructureFunctionsFromPDFs(); 
        PolSFs->SetPolarizedPDFs(BBSPol);

	InSANEPolarizedCrossSectionDifference *PXSPara = new InSANEPolarizedCrossSectionDifference();
	PXSPara->SetTargetType(Target);
        PXSPara->SetPolarizationType(1); // parallel 
        PXSPara->SetBeamEnergy(Ebeam); 
        PXSPara->SetPolarizedStructureFunctions(PolSFs);  
	PXSPara->InitializePhaseSpaceVariables();
	PXSPara->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps1 = PXSPara->GetPhaseSpace();
        ps1->Print();
	//ps->ListVariables();
	// Double_t * energy_e1 =  ps1->GetVariable("energy_e")->GetCurrentValueAddress();
	// Double_t * theta_e1  =  ps1->GetVariable("theta_e")->GetCurrentValueAddress();
	// Double_t * phi_e1    =  ps1->GetVariable("phi_e")->GetCurrentValueAddress();
	// (*energy_e1)  = Eprime;
	// (*theta_e1)   = theta;
	// (*phi_e1)     = phi;
	// PXSPara->EvaluateCurrent();

	InSANEPolarizedCrossSectionDifference *PXSPerp = new InSANEPolarizedCrossSectionDifference();
	PXSPerp->SetTargetType(Target);
        PXSPerp->SetPolarizationType(2); // perpendicular
        PXSPerp->SetBeamEnergy(Ebeam); 
        PXSPerp->SetPolarizedStructureFunctions(PolSFs);  
	PXSPerp->InitializePhaseSpaceVariables();
	PXSPerp->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps2 = PXSPerp->GetPhaseSpace();
        ps2->Print();
	//ps2->ListVariables();
	// Double_t * energy_e2 =  ps2->GetVariable("energy_e")->GetCurrentValueAddress();
	// Double_t * theta_e2  =  ps2->GetVariable("theta_e")->GetCurrentValueAddress();
	// Double_t * phi_e2    =  ps2->GetVariable("phi_e")->GetCurrentValueAddress();
	// (*energy_e2)  = Eprime;
	// (*theta_e2)   = theta;
	// (*phi_e2)     = phi;
	// PXSPerp->EvaluateCurrent();

        Double_t min = 0.; // -0.5; 
        Double_t max = 9.; //  0.5; 
	
        TF1 *Para = new TF1("PolXSDiffPara",PXSPara,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             EpMin,EpMax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
        Para->SetParameter(0,theta);
        Para->SetParameter(1,phi);
        Para->SetLineWidth(width);
        Para->SetLineColor(kBlue); 

        TF1 *Perp = new TF1("PolXSDiffPerp",PXSPerp,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             EpMin,EpMax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
        Perp->SetParameter(0,theta);
        Perp->SetParameter(1,phi);
        Perp->SetLineWidth(width);
        Perp->SetLineColor(kRed); 

        TLegend *L = new TLegend(0.6,0.6,0.8,0.8);
        L->SetFillColor(kWhite);  
        L->AddEntry(Para,"#Delta#sigma_{#parallel}","l"); 
        L->AddEntry(Perp,"#Delta#sigma_{#perp}"    ,"l"); 

        TString Title      = Form("Polarized Cross Section Differences (E = %.2f GeV, #theta = %.0f#circ)",Ebeam,theta/degree);
        TString xAxisTitle = Form("E' (GeV)");
        TString yAxisTitle = Form("#Delta#sigma (nb/GeV/sr)");

        TCanvas *c1 = new TCanvas("c1","Polarized Cross Section Differences",1000,800);
	c1->SetFillColor(kWhite);
 
        c1->cd(); 

        Para->Draw();
        Para->SetTitle(Title); 
	Para->GetXaxis()->SetTitle(xAxisTitle);
	Para->GetXaxis()->CenterTitle(true);
	Para->GetYaxis()->SetTitle(yAxisTitle);
	Para->GetYaxis()->CenterTitle(true);
	Para->GetYaxis()->SetRangeUser(min,max);
        Perp->Draw("same");
        L->Draw("same"); 

        c1->Update();
   
        double EP = 0.637164;
        double XS = Para->Eval(EP); 

        cout << EP << "\t" << XS << endl; 





}
