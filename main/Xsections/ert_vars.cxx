

Int_t ert_vars(){

   Double_t beamEnergy  = 5.9;
   Double_t theta       = 45.0*degree;//29.0*TMath::Pi()/180.0;
   Double_t Eprime      = beamEnergy*(1.-0.869);

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->GetPOLRAD()->SetTargetType(0);
   fDiffXSec4->GetPOLRAD()->SetHelicity(-1.);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
   //ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4 =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4 =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4) = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e4) = 0.0*TMath::Pi()/180.0;
   fDiffXSec4->EvaluateCurrent();
   //fDiffXSec4->Print();
   //fDiffXSec4->PrintTable();

   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   InSANEPOLRAD * polrad = fDiffXSec4->GetPOLRAD();
   Double_t tau_min = polrad->GetKinematics()->GetTau_A_min();
   Double_t tau_max = polrad->GetKinematics()->GetTau_A_max();

   // cout << "tau min = " << tau_min << endl;
   // cout << "tau max = " << tau_max << endl;
   // exit(1); 
 
   Double_t xbj = polrad->GetKinematics()->Getx();
   Double_t Q2  = polrad->GetKinematics()->GetQ2();
   Double_t Mp  = polrad->GetKinematics()->GetM();
   Double_t W2  = polrad->GetKinematics()->GetW2();
   Double_t nu  = polrad->GetKinematics()->Gety()*beamEnergy;
   Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
   Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
   Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
   Double_t tau_u = 0.67;
   Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

   //polrad->GetKinematics()->Print();
   std::cout << "tau_min = " << tau_min << "\n";
   std::cout << "tau_max = " << tau_max << "\n";
   std::cout << "Q2 = " << Q2 << "\n";
   std::cout << "W2 = " << W2 << "\n";
   std::cout << "Mp = " << Mp << "\n";
   std::cout << "xbj = " << xbj << "\n";
   std::cout << "tau_me_peak = " << tau_me_peak << "\n";
   std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
   std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
   std::cout << "t = " << t << "\n";

   vector<double> tausr,sxi,seta,rxi,reta; 
   vector<double> tauR_el,R_el; 

   ImportsrData(tausr,sxi,seta,rxi,reta); 
   ImportRelData(tauR_el,R_el); 

   Int_t width = 2; 

   TGraph *grxi  = GetTGraph(tausr,rxi);
   TGraph *greta = GetTGraph(tausr,reta);
   TGraph *gR_el = GetTGraph(tauR_el,R_el); 

   gR_el->SetLineWidth(width);

   TF1 * frxi = new TF1("frxi", polrad, &InSANEPOLRAD::rxi_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","rxi_Plot");
   frxi->SetNpx(5000);
   frxi->SetLineColor(kMagenta);
   frxi->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * freta = new TF1("freta", polrad, &InSANEPOLRAD::reta_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","reta_Plot");
   freta->SetNpx(5000);
   freta->SetLineColor(kMagenta);
   freta->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fR_el = new TF1("fR_el", polrad, &InSANEPOLRAD::Rel_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Rel_Plot");
   fR_el->SetNpx(5000);
   fR_el->SetLineColor(kMagenta);
   fR_el->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
   Leg->SetFillColor(kWhite); 
   Leg->AddEntry(gR_el,"Fortran","l");
   Leg->AddEntry(fR_el,"C++"    ,"l");

   Double_t min = -1E+05;
   Double_t max =  1E+05;

   TString DrawOption = Form("AP"); 
   TString xAxisTitle = Form("#tau");

   TCanvas *c1 = new TCanvas("c1","Miscellaneous Variables",1000,800);
   c1->SetFillColor(kWhite);
   c1->Divide(2,2); 

   min = GetMin(rxi);
   max = GetMax(rxi);

   c1->cd(1); 
   grxi->Draw(DrawOption);
   grxi->SetTitle("r_{#xi}");
   grxi->GetXaxis()->SetTitle(xAxisTitle);
   grxi->GetXaxis()->CenterTitle();
   grxi->GetYaxis()->SetRangeUser(min,max);
   grxi->Draw(DrawOption);
   frxi->Draw("same");
   c1->Update();

   min = GetMin(reta);
   max = GetMax(reta);

   c1->cd(2); 
   greta->Draw(DrawOption);
   greta->SetTitle("r_{#eta}");
   greta->GetXaxis()->SetTitle(xAxisTitle);
   greta->GetXaxis()->CenterTitle();
   greta->GetYaxis()->SetRangeUser(min,max);
   greta->Draw(DrawOption);
   freta->Draw("same");
   c1->Update();

   min = GetMin(R_el);
   max = GetMax(R_el);

   c1->cd(3); 
   gR_el->Draw(DrawOption); 
   gR_el->SetTitle("R_{el}");
   gR_el->GetXaxis()->SetTitle(xAxisTitle); 
   gR_el->GetXaxis()->CenterTitle(); 
   gR_el->GetYaxis()->SetRangeUser(min,max);
   gR_el->Draw(DrawOption);
   fR_el->Draw("same");  
   c1->Update();

   c1->cd(4); 
   Leg->Draw("same");
   c1->Update();

   fDiffXSec4->GetPOLRAD()->Print();
   fDiffXSec4->GetPOLRAD()->GetKinematics()->Print();

   return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportsrData(vector<double> &tau,vector<double> &sxi,vector<double> &seta,vector<double> &rxi,vector<double> &reta){

        double itau,iy1,iy2,iy3,iy4;
        TString inpath = Form("./dump/dump_s_and_r.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3 >> iy4;
                                tau.push_back(itau);
                                sxi.push_back(iy1);
                                seta.push_back(iy2);
                                rxi.push_back(iy3);
                                reta.push_back(iy4);
		}
                infile.close();
                tau.pop_back();
                sxi.pop_back();
                seta.pop_back();
		rxi.pop_back();
                reta.pop_back();

	}

}
//_______________________________________________________________
void ImportRelData(vector<double> &tau,vector<double> &rel){

        double itau,iy1;
        TString inpath = Form("./dump/dump_rel.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1;
                                tau.push_back(itau);
                                rel.push_back(iy1);
		}
                infile.close();
                tau.pop_back();
                rel.pop_back();
	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
 	if(min<-1E+2){
		min -= 1E+4;
	}else{
		min -= 1E+1; 
        } 
	// min -= 1E+2; 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	if(max>1E+2){
		max += 1E+4;
	}else{
		max += 1E+1; 
        } 
        // max += 1E+2; 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}

