
Int_t gfn=0; 

Int_t irt_fancy_sf(){

	Double_t beamEnergy   = 5.9;
	Double_t theta        = 45.0*degree;//29.0*TMath::Pi()/180.0;
	Double_t Eprime       = beamEnergy*(1.-0.869);
	Double_t Emin         = 0.5;
	Double_t Emax         = 4.5;
	Double_t minTheta     = 15.0*degree;
	Double_t maxTheta     = 55.0*degree;
	AMTFormFactors *amtFF            = new AMTFormFactors(); 
        NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
        F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

	Int_t Target          = 0;
	Double_t A            = 0;
	Double_t Z            = 0;

        Double_t R=0; 

	switch(Target){
		case 0: // proton 
			A = 1;
			Z = 1;
			break;
		case 1: // neutron 
			A = 1;
			Z = 0;
			break;
		case 2: // deuteron 
			A = 2;
			Z = 1;
			break;
		case 3: // helium-3 
			A = 3;
			Z = 2;
			break;
	}

	InSANEPOLRADInelasticTailDiffXSec * fDiffXSec1 = new  InSANEPOLRADInelasticTailDiffXSec();
	fDiffXSec1->SetBeamEnergy(beamEnergy);
	fDiffXSec1->GetPOLRAD()->SetTargetType(Target);
        fDiffXSec1->SetZ(Z); 
        fDiffXSec1->SetA(A); 
	fDiffXSec1->GetPOLRAD()->SetHelicity(-1.);
	fDiffXSec1->SetFormFactors(amtFF); 
        // fDiffXSec1->GetPOLRAD()->SetUSFs(NMC95); 
        fDiffXSec1->GetPOLRAD()->SetUSFs(F1F209); 
	fDiffXSec1->InitializePhaseSpaceVariables();
	fDiffXSec1->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps1 = fDiffXSec1->GetPhaseSpace();
	//ps1->ListVariables();
	Double_t * energy_e1 =  ps1->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e1  =  ps1->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e1    =  ps1->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e1) = Eprime;
	(*theta_e1)  = theta;//45.0*TMath::Pi()/180.0;
	(*phi_e1)    = 0.0*degree;
	fDiffXSec1->EvaluateCurrent();
	//fDiffXSec1->Print();
	//fDiffXSec1->PrintTable();

	InSANEPOLRAD * polrad = fDiffXSec1->GetPOLRAD();
	Double_t tau_min      = polrad->GetKinematics()->GetTau_min();
	Double_t tau_max      = polrad->GetKinematics()->GetTau_max();

	// cout << "tau min = " << tau_min << endl;
	// cout << "tau max = " << tau_max << endl;
	// exit(1); 

	Double_t xbj = polrad->GetKinematics()->Getx();
	Double_t Q2  = polrad->GetKinematics()->GetQ2();
	Double_t Mp  = polrad->GetKinematics()->GetM();
	Double_t W2  = polrad->GetKinematics()->GetW2();
	Double_t nu  = polrad->GetKinematics()->Gety()*beamEnergy;
	Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
	Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
	Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
	Double_t tau_u = 0.67;
	Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

	//polrad->GetKinematics()->Print();
	std::cout << "tau_min = " << tau_min << "\n";
	std::cout << "tau_max = " << tau_max << "\n";
	std::cout << "Q2 = " << Q2 << "\n";
	std::cout << "W2 = " << W2 << "\n";
	std::cout << "Mp = " << Mp << "\n";
	std::cout << "xbj = " << xbj << "\n";
	std::cout << "tau_me_peak = " << tau_me_peak << "\n";
	std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
	std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
	std::cout << "t = " << t << "\n";

	Int_t width = 2;

	vector<double> tauEps,eps; 
	vector<double> tauSF,sf1,sf2,sf3,sf4,sf5,sf6,sf7,sf8;
	vector<double> tauFF,GE,GM;  
	vector<double> tauT,T; 


        cout << "Enter file: \n 1: R = 0.1000 \n 2: R = 0.5661 \n 3: R = 0.0100 \n";
        cout << "Choice: ";
        cin  >> gfn;  

	ImportSFData(0,R,tauSF,sf1,sf2,sf3,sf4,sf5,sf6,sf7,sf8);
	tauSF.clear();
	ImportSFData(1,R,tauSF,sf1,sf2,sf3,sf4,sf5,sf6,sf7,sf8);
	// ImportEpsData(tauEps,eps);
	// ImporttData(tauT,T);

	TGraph *g1   = GetTGraph(tauSF,sf1);
	TGraph *g2   = GetTGraph(tauSF,sf2);
	TGraph *g3   = GetTGraph(tauSF,sf3);
	TGraph *g4   = GetTGraph(tauSF,sf4);
	TGraph *g5   = GetTGraph(tauSF,sf5);
	TGraph *g6   = GetTGraph(tauSF,sf6);
	TGraph *g7   = GetTGraph(tauSF,sf7);
	TGraph *g8   = GetTGraph(tauSF,sf8);
	// TGraph *gEps = GetTGraph(tauEps,eps);
	// TGraph *gT   = GetTGraph(tauT,T);

	g1->SetLineWidth(width);  

	// TF1 * fEps = new TF1("fEps", polrad, &InSANEPOLRAD::Eps_Plot, 
	// 		tau_min, tau_max, 0,"InSANEPOLRAD","Eps_Plot");
	// fEps->SetNpx(5000);
	// fEps->SetLineColor(kMagenta);
	// fEps->SetLineWidth(width);
	// fDiffXSec1->EvaluateCurrent();

	// TF1 * fT = new TF1("fT", polrad, &InSANEPOLRAD::t_Plot, 
	// 		tau_min, tau_max, 0,"InSANEPOLRAD","t_Plot");
	// fT->SetNpx(5000);
	// fT->SetLineColor(kMagenta);
	// fT->SetLineWidth(width);
	// fDiffXSec1->EvaluateCurrent();

	TF1 * f1 = new TF1("f1", polrad, &InSANEPOLRAD::FancyFInelastic_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","FancyFInelastic_Plot");
	f1->SetParameter(0,1);
	f1->SetParameter(1,R);
	f1->SetNpx(5000);
	f1->SetLineColor(kMagenta);
	f1->SetLineWidth(width);
	fDiffXSec1->EvaluateCurrent();

	TF1 * f2 = new TF1("f2", polrad, &InSANEPOLRAD::FancyFInelastic_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","FancyFInelastic_Plot");
	f2->SetParameter(0,2); 
	f2->SetParameter(1,R);
	f2->SetNpx(5000);
	f2->SetLineColor(kMagenta);
	f2->SetLineWidth(width);
	fDiffXSec1->EvaluateCurrent();

	TF1 * f3 = new TF1("f3", polrad, &InSANEPOLRAD::FancyFInelastic_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","FancyFInelastic_Plot");
	f3->SetParameter(0,3); 
	f3->SetParameter(1,R);
	f3->SetNpx(5000);
	f3->SetLineColor(kMagenta);
	f3->SetLineWidth(width);
	fDiffXSec1->EvaluateCurrent();

	TF1 * f4 = new TF1("f4", polrad, &InSANEPOLRAD::FancyFInelastic_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","FancyFInelastic_Plot");
	f4->SetParameter(0,4); 
	f4->SetParameter(1,R);
	f4->SetNpx(5000);
	f4->SetLineColor(kMagenta);
	f4->SetLineWidth(width);
	fDiffXSec1->EvaluateCurrent();

	TF1 * f5 = new TF1("f5", polrad, &InSANEPOLRAD::FancyFInelastic_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","FancyFInelastic_Plot");
	f5->SetParameter(0,5); 
	f5->SetParameter(1,R);
	f5->SetNpx(5000);
	f5->SetLineColor(kMagenta);
	f5->SetLineWidth(width);
	fDiffXSec1->EvaluateCurrent();

	TF1 * f6 = new TF1("f6", polrad, &InSANEPOLRAD::FancyFInelastic_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","FancyFInelastic_Plot");
	f6->SetParameter(0,6); 
	f6->SetParameter(1,R);
	f6->SetNpx(5000);
	f6->SetLineColor(kMagenta);
	f6->SetLineWidth(width);
	fDiffXSec1->EvaluateCurrent();

	TF1 * f7 = new TF1("f7", polrad, &InSANEPOLRAD::FancyFInelastic_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","FancyFInelastic_Plot");
	f7->SetParameter(0,7); 
	f7->SetParameter(1,R);
	f7->SetNpx(5000);
	f7->SetLineColor(kMagenta);
	f7->SetLineWidth(width);
	fDiffXSec1->EvaluateCurrent();

	TF1 * f8 = new TF1("f8", polrad, &InSANEPOLRAD::FancyFInelastic_Plot, 
			tau_min, tau_max, 2,"InSANEPOLRAD","FancyFInelastic_Plot");
	f8->SetParameter(0,8); 
	f8->SetParameter(1,R);
	f8->SetNpx(5000);
	f8->SetLineColor(kMagenta);
	f8->SetLineWidth(width);
	fDiffXSec1->EvaluateCurrent();

	TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
	Leg->SetFillColor(kWhite); 
	Leg->AddEntry(g1,"Fortran","l");
	Leg->AddEntry(f1,"C++"    ,"l");

	Double_t min = -5; 
	Double_t max =  5; 

	TString DrawOption = Form("AP"); 
	TString xAxisTitle = Form("#tau");

	TCanvas *c1 = new TCanvas("c1","Fancy SF",1000,800);
	c1->SetFillColor(kWhite);
	c1->Divide(4,2); 

	min = GetMin(sf1);
	max = GetMax(sf1);

	c1->cd(1); 
	g1->Draw(DrawOption);
	g1->SetTitle("#Jgothic_{1}");
	g1->GetXaxis()->SetTitle(xAxisTitle);
	g1->GetXaxis()->CenterTitle();
	g1->GetYaxis()->SetRangeUser(min,max);
	g1->Draw(DrawOption);
	f1->Draw("same");
	Leg->Draw("same");
	c1->Update();

	min = GetMin(sf2);
	max = GetMax(sf2);

	c1->cd(2); 
	g2->Draw(DrawOption);
	g2->SetTitle("#Jgothic_{2}");
	g2->GetXaxis()->SetTitle(xAxisTitle);
	g2->GetXaxis()->CenterTitle();
	g2->GetYaxis()->SetRangeUser(min,max);
	g2->Draw(DrawOption);
	f2->Draw("same");
	c1->Update();
	min = GetMin(sf3);
	max = GetMax(sf3);

	c1->cd(3); 
	g3->Draw(DrawOption);
	g3->SetTitle("#Jgothic_{3}");
	g3->GetXaxis()->SetTitle(xAxisTitle);
	g3->GetXaxis()->CenterTitle();
	g3->GetYaxis()->SetRangeUser(min,max);
	g3->Draw(DrawOption);
	f3->Draw("same");
	c1->Update();

	min = GetMin(sf4);
	max = GetMax(sf4);

	c1->cd(4); 
	g4->Draw(DrawOption);
	g4->SetTitle("#Jgothic_{4}");
	g4->GetXaxis()->SetTitle(xAxisTitle);
	g4->GetXaxis()->CenterTitle();
	g4->GetYaxis()->SetRangeUser(min,max);
	g4->Draw(DrawOption);
	f4->Draw("same");
	c1->Update();

	min = GetMin(sf5);
	max = GetMax(sf5);

	c1->cd(5); 
	g5->Draw(DrawOption);
	g5->SetTitle("#Jgothic_{5}");
	g5->GetXaxis()->SetTitle(xAxisTitle);
	g5->GetXaxis()->CenterTitle();
	g5->GetYaxis()->SetRangeUser(min,max);
	g5->Draw(DrawOption);
	f5->Draw("same");
	c1->Update();

	min = GetMin(sf6);
	max = GetMax(sf6);

	c1->cd(6); 
	g6->Draw(DrawOption);
	g6->SetTitle("#Jgothic_{6}");
	g6->GetXaxis()->SetTitle(xAxisTitle);
	g6->GetXaxis()->CenterTitle();
	g6->GetYaxis()->SetRangeUser(min,max);
	g6->Draw(DrawOption);
	f6->Draw("same");
	c1->Update();

	min = GetMin(sf7);
	max = GetMax(sf7);

	c1->cd(7); 
	g7->Draw(DrawOption);
	g7->SetTitle("#Jgothic_{7}");
	g7->GetXaxis()->SetTitle(xAxisTitle);
	g7->GetXaxis()->CenterTitle();
	g7->GetYaxis()->SetRangeUser(min,max);
	g7->Draw(DrawOption);
	f7->Draw("same");
	c1->Update();

	min = GetMin(sf8);
	max = GetMax(sf8);

	c1->cd(8); 
	g8->Draw(DrawOption);
	g8->SetTitle("#Jgothic_{8}");
	g8->GetXaxis()->SetTitle(xAxisTitle);
	g8->GetXaxis()->CenterTitle();
	g8->GetYaxis()->SetRangeUser(min,max);
	g8->Draw(DrawOption);
	f8->Draw("same");
	c1->Update();

	// TCanvas *c2 = new TCanvas("c2","Epsilon",1000,800);
	// c2->SetFillColor(kWhite);
	// // c2->Divide(2,1); 

	// min = GetMin(eps);
	// max = GetMax(eps);

	// c2->cd(); 
	// gEps->Draw(DrawOption);
	// gEps->SetTitle("#epsilon");
	// gEps->GetXaxis()->SetTitle(xAxisTitle);
	// gEps->GetXaxis()->CenterTitle();
	// gEps->GetYaxis()->SetRangeUser(min,max);
	// gEps->Draw(DrawOption);
	// fEps->Draw("same");
	// Leg->Draw("same");
	// c2->Update();

	// min = GetMin(T);
	// max = GetMax(T);

	// c2->cd(2); 
	// gT->Draw(DrawOption);
	// gT->SetTitle("t = Q^{2} + R#tau");
	// gT->GetXaxis()->SetTitle(xAxisTitle);
	// gT->GetXaxis()->CenterTitle();
	// gT->GetYaxis()->SetRangeUser(min,max);
	// gT->Draw(DrawOption);
	// fT->Draw("same");
	// c2->Update();

	fDiffXSec1->GetPOLRAD()->Print();
	fDiffXSec1->GetPOLRAD()->GetKinematics()->Print();

	return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
	g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportSFData(int type,double &R,vector<double> &tau,vector<double> &y1,vector<double> &y2,
		  vector<double> &y3,vector<double> &y4,vector<double> &y5,
		  vector<double> &y6,vector<double> &y7,vector<double> &y8){

	double itau,ir,iy1,iy2,iy3,iy4,iy5,iy6,iy7,iy8;
        TString sf;
 
        switch(type){
		case 0: 
			sf = Form("f1f2");
			break;
		case 1: 
			sf = Form("g1g2");
			break;
        } 
 
	TString inpath = Form("./dump/dump_irt_sf_%s_%d.dat",sf.Data(),gfn);

	ifstream infile;
	infile.open(inpath);
	if(infile.fail()){
		cout << "Cannot open the file: " << inpath << endl;
		exit(1);
	}else{
		cout << "Importing data from: " << inpath << "..." << endl;
		while(!infile.eof()){
			infile >> itau >> ir >> iy1 >> iy2 >> iy3 >> iy4 >> iy5 >> iy6 >> iy7 >> iy8;
                        // cout << Form("%.3E",itau) << "\t" 
                        //      << Form("%.3E",iy1 ) << "\t" 
                        //      << Form("%.3E",iy2 ) << "\t" 
                        //      << Form("%.3E",iy3 ) << "\t" 
                        //      << Form("%.3E",iy4 ) << "\t" 
                        //      << Form("%.3E",iy5 ) << "\t" 
                        //      << Form("%.3E",iy6 ) << "\t" 
                        //      << Form("%.3E",iy7 ) << "\t" 
                        //      << Form("%.3E",iy8 ) << endl;
			tau.push_back(itau);
			if(type==0){
				y1.push_back(iy1);
				y2.push_back(iy2);
			}else if(type==1){
				y3.push_back(iy3);
				y4.push_back(iy4);
				y5.push_back(iy5);
				y6.push_back(iy6);
				y7.push_back(iy7);
				y8.push_back(iy8);
			}
		}
		infile.close();
		tau.pop_back();
		if(type==0){
			y1.pop_back();
			y2.pop_back();
		}else if(type==1){
			y3.pop_back();
			y4.pop_back();
			y5.pop_back();
			y6.pop_back();
			y7.pop_back();
			y8.pop_back();
		}
	}

	R = ir; 

        cout << "CONSTANT R = " << ir << endl;  

}
//_______________________________________________________________
void ImportEpsData(vector<double> &tau,vector<double> &y1){

	double itau,iy1;
	TString inpath = Form("./dump/dump_irt_eps.dat");

	ifstream infile;
	infile.open(inpath);
	if(infile.fail()){
		cout << "Cannot open the file: " << inpath << endl;
		exit(1);
	}else{
		cout << "Importing data from: " << inpath << "..." << endl;
		while(!infile.eof()){
			infile >> itau >> iy1;
			tau.push_back(itau);
			y1.push_back(iy1);
		}
		infile.close();
		tau.pop_back();
		y1.pop_back();
	}

}
//_______________________________________________________________
void ImporttData(vector<double> &tau,vector<double> &y1){

	double itau,iy1;
	TString inpath = Form("./dump/dump_t_irt.dat");

	ifstream infile;
	infile.open(inpath);
	if(infile.fail()){
		cout << "Cannot open the file: " << inpath << endl;
		exit(1);
	}else{
		while(!infile.eof()){
			infile >> itau >> iy1;
			tau.push_back(itau);
			y1.push_back(iy1);
		}
		infile.close();
		tau.pop_back();
		y1.pop_back();
	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

	double min = (-1.)*GetSecondPeak(v);
	if(min<-1E+2){
		min -= 1E+2;
	}else{
		min -= 1E+0; 
	} 
	// min -= 1E+2; 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	if(max>1E+2){
		max += 1E+2;
	}else{
		max += 1E+0; 
	} 
	// max += 1E+2; 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 

	// first we get the highest peak 
	double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

	// now get the second highest peak 
	double sec_peak=0;
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}



