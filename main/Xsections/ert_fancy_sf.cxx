

Int_t ert_fancy_sf(){

   Double_t beamEnergy  = 5.9;
   Double_t theta       = 45.0*degree;//29.0*TMath::Pi()/180.0;
   Double_t Eprime      = beamEnergy*(1.-0.869);

   AMTFormFactors       *amtFF   = new AMTFormFactors(); 
   BilenkayaFormFactors *bilenFF = new BilenkayaFormFactors(); 

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec1 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->GetPOLRAD()->SetTargetType(0);
   fDiffXSec1->GetPOLRAD()->SetHelicity(-1.);
   fDiffXSec1->SetFormFactors(amtFF); 
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps1 = fDiffXSec1->GetPhaseSpace();
   //ps4->ListVariables();
   Double_t * energy_e1 =  ps1->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e1  =  ps1->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e1    =  ps1->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e1) = Eprime;
   (*theta_e1)  = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e1)    = 0.0*degree;
   fDiffXSec1->EvaluateCurrent();
   //fDiffXSec1->Print();
   //fDiffXSec1->PrintTable();

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec2 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->GetPOLRAD()->SetTargetType(0);
   fDiffXSec2->GetPOLRAD()->SetHelicity(-1.);
   fDiffXSec2->SetFormFactors(bilenFF); 
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps2 = fDiffXSec2->GetPhaseSpace();
   //ps2->ListVariables();
   Double_t * energy_e2 =  ps2->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e2  =  ps2->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e2    =  ps2->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e2) = Eprime;
   (*theta_e2)  = theta;
   (*phi_e2)    = 0.0*degree;
   fDiffXSec2->EvaluateCurrent();
   //fDiffXSec2->Print();
   //fDiffXSec2->PrintTable();

   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   InSANEPOLRAD * polrad1 = fDiffXSec1->GetPOLRAD();
   Double_t tau_min = polrad1->GetKinematics()->GetTau_A_min();
   Double_t tau_max = polrad1->GetKinematics()->GetTau_A_max();

   InSANEPOLRAD * polrad2 = fDiffXSec2->GetPOLRAD();

   // cout << "tau min = " << tau_min << endl;
   // cout << "tau max = " << tau_max << endl;
   // exit(1); 
 
   Double_t xbj = polrad1->GetKinematics()->Getx();
   Double_t Q2  = polrad1->GetKinematics()->GetQ2();
   Double_t Mp  = polrad1->GetKinematics()->GetM();
   Double_t W2  = polrad1->GetKinematics()->GetW2();
   Double_t nu  = polrad1->GetKinematics()->Gety()*beamEnergy;
   Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
   Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
   Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
   Double_t tau_u = 0.67;
   Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

   //polrad->GetKinematics()->Print();
   std::cout << "tau_min = " << tau_min << "\n";
   std::cout << "tau_max = " << tau_max << "\n";
   std::cout << "Q2 = " << Q2 << "\n";
   std::cout << "W2 = " << W2 << "\n";
   std::cout << "Mp = " << Mp << "\n";
   std::cout << "xbj = " << xbj << "\n";
   std::cout << "tau_me_peak = " << tau_me_peak << "\n";
   std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
   std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
   std::cout << "t = " << t << "\n";

   Int_t width = 2;
  
   vector<double> tauETA,eta; 
   vector<double> tauSF,sf1,sf2,sf3,sf4,sf5,sf6,sf7,sf8;
   vector<double> tauFF,GE,GM;  
   vector<double> taut,T; 

   ImportSFData(tauSF,sf1,sf2,sf3,sf4,sf5,sf6,sf7,sf8);
   ImportEtaData(tauETA,eta);
   ImporttData(taut,T);
   ImportFormFactorData(tauFF,GE,GM); 

   TGraph *g1     = GetTGraph(tauSF,sf1);
   TGraph *g2     = GetTGraph(tauSF,sf2);
   TGraph *g3     = GetTGraph(tauSF,sf3);
   TGraph *g4     = GetTGraph(tauSF,sf4);
   TGraph *g5     = GetTGraph(tauSF,sf5);
   TGraph *g6     = GetTGraph(tauSF,sf6);
   TGraph *g7     = GetTGraph(tauSF,sf7);
   TGraph *g8     = GetTGraph(tauSF,sf8);
   TGraph *gEta_A = GetTGraph(tauETA,eta);
   TGraph *gT     = GetTGraph(taut,T);
   TGraph *gGE    = GetTGraph(tauFF,GE);
   TGraph *gGM    = GetTGraph(tauFF,GM);
  
   g1->SetLineWidth(width);  

   TF1 * fEta_A = new TF1("fEta_A", polrad1, &InSANEPOLRAD::Eta_A_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","Eta_A_Plot");
   fEta_A->SetNpx(5000);
   fEta_A->SetLineColor(kMagenta);
   fEta_A->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fT = new TF1("fT", polrad1, &InSANEPOLRAD::t_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","t_Plot");
   fT->SetNpx(5000);
   fT->SetLineColor(kMagenta);
   fT->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * f1 = new TF1("f1", polrad1, &InSANEPOLRAD::FancyFElastic_1_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_1_Plot");
   f1->SetNpx(5000);
   f1->SetLineColor(kMagenta);
   f1->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * f2 = new TF1("f2", polrad1, &InSANEPOLRAD::FancyFElastic_2_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_2_Plot");
   f2->SetNpx(5000);
   f2->SetLineColor(kMagenta);
   f2->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * f3 = new TF1("f3", polrad1, &InSANEPOLRAD::FancyFElastic_3_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_3_Plot");
   f3->SetNpx(5000);
   f3->SetLineColor(kMagenta);
   f3->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * f4 = new TF1("f4", polrad1, &InSANEPOLRAD::FancyFElastic_4_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_4_Plot");
   f4->SetNpx(5000);
   f4->SetLineColor(kMagenta);
   f4->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * f5 = new TF1("f5", polrad1, &InSANEPOLRAD::FancyFElastic_5_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_5_Plot");
   f5->SetNpx(5000);
   f5->SetLineColor(kMagenta);
   f5->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * f6 = new TF1("f6", polrad1, &InSANEPOLRAD::FancyFElastic_6_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_6_Plot");
   f6->SetNpx(5000);
   f6->SetLineColor(kMagenta);
   f6->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * f7 = new TF1("f7", polrad1, &InSANEPOLRAD::FancyFElastic_7_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_7_Plot");
   f7->SetNpx(5000);
   f7->SetLineColor(kMagenta);
   f7->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * f8 = new TF1("f8", polrad1, &InSANEPOLRAD::FancyFElastic_8_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_8_Plot");
   f8->SetNpx(5000);
   f8->SetLineColor(kMagenta);
   f8->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * h1 = new TF1("h1", polrad2, &InSANEPOLRAD::FancyFElastic_1_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_1_Plot");
   h1->SetNpx(5000);
   h1->SetLineColor(kCyan);
   h1->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * h2 = new TF1("h2", polrad2, &InSANEPOLRAD::FancyFElastic_2_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_2_Plot");
   h2->SetNpx(5000);
   h2->SetLineColor(kCyan);
   h2->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * h3 = new TF1("h3", polrad2, &InSANEPOLRAD::FancyFElastic_3_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_3_Plot");
   h3->SetNpx(5000);
   h3->SetLineColor(kCyan);
   h3->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * h4 = new TF1("h4", polrad2, &InSANEPOLRAD::FancyFElastic_4_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_4_Plot");
   h4->SetNpx(5000);
   h4->SetLineColor(kCyan);
   h4->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * h5 = new TF1("h5", polrad2, &InSANEPOLRAD::FancyFElastic_5_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_5_Plot");
   h5->SetNpx(5000);
   h5->SetLineColor(kCyan);
   h5->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * h6 = new TF1("h6", polrad2, &InSANEPOLRAD::FancyFElastic_6_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_6_Plot");
   h6->SetNpx(5000);
   h6->SetLineColor(kCyan);
   h6->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * h7 = new TF1("h7", polrad2, &InSANEPOLRAD::FancyFElastic_7_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_7_Plot");
   h7->SetNpx(5000);
   h7->SetLineColor(kCyan);
   h7->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * h8 = new TF1("h8", polrad2, &InSANEPOLRAD::FancyFElastic_8_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","FancyFElastic_8_Plot");
   h8->SetNpx(5000);
   h8->SetLineColor(kCyan);
   h8->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * fGE = new TF1("fGE", polrad1, &InSANEPOLRAD::GE_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","GE_Plot");
   fGE->SetNpx(5000);
   fGE->SetLineColor(kMagenta);
   fGE->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * fGM = new TF1("fGM", polrad1, &InSANEPOLRAD::GM_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","GM_Plot");
   fGM->SetNpx(5000);
   fGM->SetLineColor(kMagenta);
   fGM->SetLineWidth(width);
   fDiffXSec1->EvaluateCurrent();

   TF1 * hGE = new TF1("hGE", polrad2, &InSANEPOLRAD::GE_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","GE_Plot");
   hGE->SetNpx(5000);
   hGE->SetLineColor(kCyan);
   hGE->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TF1 * hGM = new TF1("hGM", polrad2, &InSANEPOLRAD::GM_Plot, 
                tau_min, tau_max, 0,"InSANEPOLRAD","GM_Plot");
   hGM->SetNpx(5000);
   hGM->SetLineColor(kCyan);
   hGM->SetLineWidth(width);
   fDiffXSec2->EvaluateCurrent();

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
   Leg->SetFillColor(kWhite); 
   Leg->AddEntry(g1,"Fortran"          ,"l");
   Leg->AddEntry(f1,"C++"              ,"l");
   Leg->AddEntry(h1,"C++, Bilenkaya FF","l");

   TCanvas *c1 = new TCanvas("c1","Theta ijk",1000,800);
   c1->SetFillColor(kWhite);
   c1->Divide(4,2); 

   Double_t min = -5; 
   Double_t max =  5; 

   TString DrawOption = Form("AP"); 
   TString xAxisTitle = Form("#tau");

   min = GetMin(sf1);
   max = GetMax(sf1);

   c1->cd(1); 
   g1->Draw(DrawOption);
   g1->SetTitle("#Jgothic_{1}");
   g1->GetXaxis()->SetTitle(xAxisTitle);
   g1->GetXaxis()->CenterTitle();
   g1->GetYaxis()->SetRangeUser(min,max);
   g1->Draw(DrawOption);
   f1->Draw("same");
   h1->Draw("same");
   Leg->Draw("same");
   c1->Update();

   min = GetMin(sf2);
   max = GetMax(sf2);

   c1->cd(2); 
   g2->Draw(DrawOption);
   g2->SetTitle("#Jgothic_{2}");
   g2->GetXaxis()->SetTitle(xAxisTitle);
   g2->GetXaxis()->CenterTitle();
   g2->GetYaxis()->SetRangeUser(min,max);
   g2->Draw(DrawOption);
   f2->Draw("same");
   h2->Draw("same");
   c1->Update();

   min = GetMin(sf3);
   max = GetMax(sf3);

   c1->cd(3); 
   g3->Draw(DrawOption);
   g3->SetTitle("#Jgothic_{3}");
   g3->GetXaxis()->SetTitle(xAxisTitle);
   g3->GetXaxis()->CenterTitle();
   g3->GetYaxis()->SetRangeUser(min,max);
   g3->Draw(DrawOption);
   f3->Draw("same");
   h3->Draw("same");
   c1->Update();

   min = GetMin(sf4);
   max = GetMax(sf4);

   c1->cd(4); 
   g4->Draw(DrawOption);
   g4->SetTitle("#Jgothic_{4}");
   g4->GetXaxis()->SetTitle(xAxisTitle);
   g4->GetXaxis()->CenterTitle();
   g4->GetYaxis()->SetRangeUser(min,max);
   g4->Draw(DrawOption);
   f4->Draw("same");
   h4->Draw("same");
   c1->Update();

   min = GetMin(sf5);
   max = GetMax(sf5);

   c1->cd(5); 
   g5->Draw(DrawOption);
   g5->SetTitle("#Jgothic_{5}");
   g5->GetXaxis()->SetTitle(xAxisTitle);
   g5->GetXaxis()->CenterTitle();
   g5->GetYaxis()->SetRangeUser(min,max);
   g5->Draw(DrawOption);
   f5->Draw("same");
   h5->Draw("same");
   c1->Update();

   min = GetMin(sf6);
   max = GetMax(sf6);

   c1->cd(6); 
   g6->Draw(DrawOption);
   g6->SetTitle("#Jgothic_{6}");
   g6->GetXaxis()->SetTitle(xAxisTitle);
   g6->GetXaxis()->CenterTitle();
   g6->GetYaxis()->SetRangeUser(min,max);
   g6->Draw(DrawOption);
   f6->Draw("same");
   h6->Draw("same");
   c1->Update();

   min = GetMin(sf7);
   max = GetMax(sf7);

   c1->cd(7); 
   g7->Draw(DrawOption);
   g7->SetTitle("#Jgothic_{7}");
   g7->GetXaxis()->SetTitle(xAxisTitle);
   g7->GetXaxis()->CenterTitle();
   g7->GetYaxis()->SetRangeUser(min,max);
   g7->Draw(DrawOption);
   f7->Draw("same");
   h7->Draw("same");
   c1->Update();

   min = GetMin(sf8);
   max = GetMax(sf8);

   c1->cd(8); 
   g8->Draw(DrawOption);
   g8->SetTitle("#Jgothic_{8}");
   g8->GetXaxis()->SetTitle(xAxisTitle);
   g8->GetXaxis()->CenterTitle();
   g8->GetYaxis()->SetRangeUser(min,max);
   g8->Draw(DrawOption);
   f8->Draw("same");
   h8->Draw("same");
   c1->Update();

   TCanvas *c2 = new TCanvas("c2","Eta_A and t",1000,800);
   c2->SetFillColor(kWhite);
   c2->Divide(2,1); 

   min = GetMin(eta);
   max = GetMax(eta);

   c2->cd(1); 
   gEta_A->Draw(DrawOption);
   gEta_A->SetTitle("#eta_{A}");
   gEta_A->GetXaxis()->SetTitle(xAxisTitle);
   gEta_A->GetXaxis()->CenterTitle();
   gEta_A->GetYaxis()->SetRangeUser(min,max);
   gEta_A->Draw(DrawOption);
   fEta_A->Draw("same");
   Leg->Draw("same");
   c2->Update();

   min = GetMin(T);
   max = GetMax(T);

   c2->cd(2); 
   gT->Draw(DrawOption);
   gT->SetTitle("t = Q^{2} + R#tau");
   gT->GetXaxis()->SetTitle(xAxisTitle);
   gT->GetXaxis()->CenterTitle();
   gT->GetYaxis()->SetRangeUser(min,max);
   gT->Draw(DrawOption);
   fT->Draw("same");
   c2->Update();

   TCanvas *c3 = new TCanvas("c3","Form Factors",1000,800);
   c3->SetFillColor(kWhite);
   c3->Divide(2,1);

   min = GetMin(GE);
   max = GetMax(GE);

   c3->cd(1); 
   gGE->Draw(DrawOption);
   gGE->SetTitle("G_{E}");
   gGE->GetXaxis()->SetTitle(xAxisTitle);
   gGE->GetXaxis()->CenterTitle();
   gGE->GetYaxis()->SetRangeUser(min,max);
   gGE->Draw(DrawOption);
   fGE->Draw("same");
   hGE->Draw("same");
   Leg->Draw("same");
   c3->Update();

   min = GetMin(GM);
   max = GetMax(GM);

   c3->cd(2); 
   gGM->Draw(DrawOption);
   gGM->SetTitle("G_{M}");
   gGM->GetXaxis()->SetTitle(xAxisTitle);
   gGM->GetXaxis()->CenterTitle();
   gGM->GetYaxis()->SetRangeUser(min,max);
   gGM->Draw(DrawOption);
   fGM->Draw("same");
   hGM->Draw("same");
   c3->Update();

   fDiffXSec1->GetPOLRAD()->Print();
   fDiffXSec1->GetPOLRAD()->GetKinematics()->Print();

   return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportSFData(vector<double> &tau,vector<double> &y1,vector<double> &y2,
                  vector<double> &y3,vector<double> &y4,vector<double> &y5,
                  vector<double> &y6,vector<double> &y7,vector<double> &y8){

        double itau,iy1,iy2,iy3,iy4,iy5,iy6,iy7,iy8;
        TString inpath = Form("./dump/dump_fancy_el_sf.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2 >> iy3 >> iy4 >> iy5 >> iy6 >> iy7 >> iy8;
                                tau.push_back(itau);
                                y1.push_back(iy1);
                                y2.push_back(iy2);
                                y3.push_back(iy3);
                                y4.push_back(iy4);
                                y5.push_back(iy5);
                                y6.push_back(iy6);
                                y7.push_back(iy7);
                                y8.push_back(iy8);
		}
                infile.close();
                tau.pop_back();
		y1.pop_back();
		y2.pop_back();
		y3.pop_back();
		y4.pop_back();
		y5.pop_back();
		y6.pop_back();
		y7.pop_back();
		y8.pop_back();
	}

}
//_______________________________________________________________
void ImportEtaData(vector<double> &tau,vector<double> &y1){

        double itau,iy1;
        TString inpath = Form("./dump/dump_eta_a.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1;
                                tau.push_back(itau);
                                y1.push_back(iy1);
		}
                infile.close();
                tau.pop_back();
		y1.pop_back();
	}

}
//_______________________________________________________________
void ImporttData(vector<double> &tau,vector<double> &y1){

        double itau,iy1;
        TString inpath = Form("./dump/dump_t.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1;
                                tau.push_back(itau);
                                y1.push_back(iy1);
		}
                infile.close();
                tau.pop_back();
		y1.pop_back();
	}

}
//_______________________________________________________________
void ImportFormFactorData(vector<double> &tau,vector<double> &GE,vector<double> &GM){

        double itau,iy1,iy2;
        TString inpath = Form("./dump/dump_ge_gm.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> iy1 >> iy2;
                                tau.push_back(itau);
                                GE.push_back(iy1);
                                GM.push_back(iy2);
		}
                infile.close();
                tau.pop_back();
		GE.pop_back();
		GM.pop_back();
	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
 	if(min<-1E+2){
		min -= 1E+2;
	}else{
		min -= 1E+1; 
        } 
	// min -= 1E+2; 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	if(max>1E+2){
		max += 1E+2;
	}else{
		max += 1E+1; 
        } 
	// max += 1E+2; 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}



