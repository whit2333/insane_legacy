Int_t dis_diff_xsec_eltail(){

	/// Test POLRAD against the results from ROSETAIL 
	/// for 3He at 45 deg. 

	Int_t Pass = 0; 
	Double_t beamEnergy =  0;
	Double_t theta      =  45.*degree;
	Double_t Eprime     =  3.0;

	cout << "Enter beam pass (4 or 5): "; 
	cin  >> Pass; 

	if(Pass==4) beamEnergy = 4.73; 
	if(Pass==5) beamEnergy = 5.89; 

	vector<double> Ep,IntTail,IntExtTail; 

	ImportData(0,Pass,Ep,IntTail); 
	Ep.clear();
	ImportData(1,Pass,Ep,IntExtTail); 

	int width = 2; 
	TGraph *gInt = GetTGraph(Ep,IntTail); 
	gInt->SetMarkerStyle(20);  
	gInt->SetLineColor(kBlack); 
	gInt->SetLineWidth(width);  

	TGraph *gIntExt = GetTGraph(Ep,IntExtTail); 
	gIntExt->SetMarkerStyle(20);  
	gIntExt->SetLineColor(kCyan); 
	gIntExt->SetLineWidth(width);  

	TMultiGraph *gRosetail = new TMultiGraph(); 
	gRosetail->Add(gInt,"c");
	// gRosetail->Add(gIntExt,"c");

	// Parameters for the plot are (theta,phi)
	Int_t    npar     = 2;
	Double_t E_min    = 0.50;
	Double_t E_max    = 2.5;

	Double_t t_b = 0.002928;
	Double_t t_a = 0.0362;

	InSANENucleus::NucleusType Target = InSANENucleus::k3He; 

	/// Form factor models 
	AmrounFormFactors *intFF = new AmrounFormFactors();  
	MSWFormFactors *mswFF    = new MSWFormFactors();  

	/// Elastic Radiative Tail
	InSANEPOLRADElasticTailDiffXSec * FElTailINT = new  InSANEPOLRADElasticTailDiffXSec();
	FElTailINT->SetTargetType(Target); 
	FElTailINT->SetBeamEnergy(beamEnergy);
	FElTailINT->GetPOLRAD()->SetUnpolarized();
	FElTailINT->GetPOLRAD()->SetPOLRADTargetFormFactors(intFF);  
	FElTailINT->InitializePhaseSpaceVariables();
	FElTailINT->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps = FElTailINT->GetPhaseSpace();
	//ps->ListVariables();
	Double_t * energy_e5 =  ps->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e5  =  ps->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e5    =  ps->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e5) = Eprime;
	(*theta_e5)  = theta;
	(*phi_e5)    = 0.0*degree;
	FElTailINT->EvaluateCurrent();

	/// Elastic Radiative Tail (including external effects) 
	InSANEPOLRADElasticTailDiffXSec * fElTailEXT = new  InSANEPOLRADElasticTailDiffXSec();
	fElTailEXT->SetTargetType(Target); 
	fElTailEXT->SetBeamEnergy(beamEnergy);
	fElTailEXT->GetPOLRAD()->SetUnpolarized();
	fElTailEXT->GetPOLRAD()->SetPOLRADTargetFormFactors(intFF);  
	fElTailEXT->GetPOLRAD()->DoExternalTailCalc(true); 
	fElTailEXT->GetPOLRAD()->SetRadiationLengths(t_b,t_a); 
	fElTailEXT->InitializePhaseSpaceVariables();
	fElTailEXT->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps2 = fElTailEXT->GetPhaseSpace();
	//ps->ListVariables();
	Double_t * energy_e2 =  ps2->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e2  =  ps2->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e2    =  ps2->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e2) = Eprime;
	(*theta_e2)  = theta;
	(*phi_e2)    = 0.0*degree;
	fElTailEXT->EvaluateCurrent();

	TF1 *Int = new TF1("sigma", FElTailINT, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	Int->SetParameter(0,theta);
	Int->SetParameter(1,0.0);
	Int->SetLineColor(kRed);

	TF1 *ext = new TF1("sigma", fElTailEXT, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
			E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
	ext->SetParameter(0,theta);
	ext->SetParameter(1,0.0);
	ext->SetLineColor(kBlue);

	cout << "Calculating Relative Error of POLRAD Compared to ROSETAIL..." << endl;
        vector<double> diff,ratio;
        double arg=0,num=0,den=0;  
        const int N = Ep.size(); 
	for(int i=0;i<N;i++){
		num = IntTail[i] - Int->Eval(Ep[i]); 
		den = IntTail[i];
                arg = 100.*TMath::Abs(num/den);  
		diff.push_back(arg); 
		arg = (den - num)/den;
		ratio.push_back(arg); 
	} 
        cout << "--> Done. " << endl;

        TGraph *Diff = GetTGraph(Ep,diff);
        Diff->SetLineWidth(width);  

        TGraph *gRatio = GetTGraph(Ep,ratio);
        gRatio->SetLineWidth(width);  

	//-------------------------------------------
	TCanvas * c = new TCanvas("dis_diff_xsec_nu","polrad",1000,800);
        c->SetFillColor(kWhite); 
        c->Divide(3,1); 

	TString Title      = Form("^{3}He Elastic Tail (E_{s} = %.2f GeV)",beamEnergy);
	TString xAxisTitle = Form("E_{p} (GeV)");  
	TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)"); 

	double xMin = 0.5; 
	double xMax = 2.0; 
	double yMin = 1E-8; 
	double yMax = 1.0; 

	TLegend *Leg = new TLegend(0.3567839,0.5472028,0.6319095,0.8583916);
	Leg->SetTextSize(0.03146853);
	Leg->SetFillColor(kWhite);
	Leg->AddEntry(gInt   ,"ROSETAIL (Internal only)"  ,"l");
	// Leg->AddEntry(gIntExt,"Rosetail (Internal + external)"  ,"l");
	Leg->AddEntry(Int    ,"C++ POLRAD (Internal only)","l");
	// Leg->AddEntry(ext    ,"C++ POLRAD (Internal + external)","l");

        c->cd(1); 
	gPad->SetLogy(true);
	gRosetail->Draw("A");
	gRosetail->SetTitle(Title);
	gRosetail->GetXaxis()->SetTitle(xAxisTitle);
	gRosetail->GetXaxis()->CenterTitle();
	gRosetail->GetYaxis()->SetTitle(yAxisTitle);
	gRosetail->GetYaxis()->CenterTitle();
	gRosetail->GetXaxis()->SetLimits(xMin,xMax);
	gRosetail->GetYaxis()->SetRangeUser(yMin,yMax);
	Int->Draw("same");
	// ext->Draw("same");
	Leg->Draw("same");
        c->Update();
 
        c->cd(2); 
        gRatio->Draw("AC");
        gRatio->SetTitle("Ratio of POLRAD to ROSETAIL");
        gRatio->GetXaxis()->SetTitle(xAxisTitle); 
        gRatio->GetXaxis()->CenterTitle(); 
	gRatio->GetXaxis()->SetLimits(xMin,xMax);
	gRatio->GetYaxis()->SetRangeUser(0,100);
        c->Update();

        c->cd(3); 
        Diff->Draw("AC");
        Diff->SetTitle("Relative Error of POLRAD Compared to ROSETAIL");
        Diff->GetXaxis()->SetTitle(xAxisTitle); 
        Diff->GetXaxis()->CenterTitle(); 
	Diff->GetXaxis()->SetLimits(xMin,xMax);
	Diff->GetYaxis()->SetRangeUser(0,100);
        c->Update();
	c->SaveAs("./He3_eltail-test.pdf");


	return(0);
}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]);
	return g;

}
//______________________________________________________________________________
void ImportData(int Type,int Pass,vector<double> &Ep,vector<double> &Tail){

        TString inpath;
	switch(Type){
		case 0: 
			inpath = Form("./dump/%d_SA_eltail_unpl_int-only.out",Pass);
			break;
		case 1: 
			inpath = Form("./dump/%d_SA_eltail_unpl.out",Pass);
			break;
	}

	double iEs,iEp,iNu,iW,iTail;

	ifstream infile;
	infile.open(inpath);
	if(infile.fail()){
		cout << "Cannot open the file: " << inpath << endl;
		exit(1);
	}else{
		while(!infile.eof()){
			infile >> iEs >> iEp >> iNu >> iW >> iTail; 
                        Ep.push_back(iEp/1E+3); 
                        Tail.push_back(iTail); 
		}
	}


}
