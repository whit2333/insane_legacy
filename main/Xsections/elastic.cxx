Int_t elastic(){

   Double_t beamEnergy = 2.3;

   InSANEExclusiveMottXSec * fDiffXSec = new  InSANEExclusiveMottXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->InitializePhaseSpaceVariables();
   InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
   ps->ListVariables();
   InSANEPhaseSpaceSampler *  fExclusiveMottSampler = new InSANEPhaseSpaceSampler(fDiffXSec);

   InSANEepElasticDiffXSec * fDiffXSec2 = new  InSANEepElasticDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->InitializePhaseSpaceVariables();
   InSANEPhaseSpace *ps2 = fDiffXSec2->GetPhaseSpace();
   ps2->Print();
   InSANEPhaseSpaceSampler *  fElasticSampler = new InSANEPhaseSpaceSampler(fDiffXSec2);


//    fExclusiveMottSampler->Refresh();




   TH2F * h1 = new TH2F("ExclusiveMott","ExclusiveMott Test",100,0.8,4.0,100,20,60);
   TH2F * h2 = new TH2F("ExclusiveMott2","ExclusiveMott Test2",100,0.1,4.9,100,20,60);

   TH2F * h3 = new TH2F("ElasticXSec","ElasticXSec Test",100,0.5,4.0,100,20,60);
   TH2F * h4 = new TH2F("ElasticXSec2","ElasticXSec2 Test2",100,0.1,2.5,100,20,60);
   TH2F * hProtonPhiVsTheta = new TH2F("hProtonPhiVsTheta","Elastic Proton PhiVsTheta",100,20,60,100,100,260);
   TH2F * hElectronPhiVsTheta = new TH2F("hElectronPhiVsTheta","Elastic Electron PhiVsTheta",100,20,60,100,-50,50);

   h1->GetXaxis()->SetTitle("Energy GeV");
   h1->GetYaxis()->SetTitle("#theta_{e'}");

   Double_t *res;
   for(int i = 0; i< 1000;i++) {
    res = fExclusiveMottSampler->GenerateEvent();
// //       std::cout << " F1F2 Xsec = " << fDiffXSec->EvaluateXSec(res) << " \n";
    h1->Fill(res[0],res[1]*180.0/TMath::Pi());
    h2->Fill(res[3],res[4]*180.0/TMath::Pi());
    res = fElasticSampler->GenerateEvent();
//       std::cout << " F1F2 Xsec = " << fDiffXSec->EvaluateXSec(res) << " \n";
    h3->Fill(res[0],res[1]*180.0/TMath::Pi());
    h4->Fill(res[3],res[4]*180.0/TMath::Pi());
    hProtonPhiVsTheta->Fill(res[4]*180.0/TMath::Pi(),res[5]*180.0/TMath::Pi());
    hElectronPhiVsTheta->Fill(res[1]*180.0/TMath::Pi(),res[2]*180.0/TMath::Pi());
   }

   TCanvas * c = new TCanvas("CrossSections","Cross Sections");
   c->Divide(3,2);
   c->cd(1);
   h1->Draw("COLZ");
   c->cd(2);
   h2->Draw("COLZ");
   c->cd(3);
   hElectronPhiVsTheta->Draw("COLZ");
   c->cd(4);
   h3->Draw("COLZ");
   c->cd(5);
   h4->Draw("COLZ");
   c->cd(6);
   hProtonPhiVsTheta->Draw("COLZ");

return(0);
}