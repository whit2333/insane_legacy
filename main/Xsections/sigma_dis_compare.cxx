//Int_t sigma_dis_compare(const char * expname  = "SLAC-E140x"){
//Int_t sigma_dis_compare(const char * expname  = "SLAC-E140"){
//Int_t sigma_dis_compare(const char * expname  = "SLAC-E133"){
//Int_t sigma_dis_compare(const char * expname  = "SLAC-E49b"){
//Int_t sigma_dis_compare(const char * expname  = "SLAC-E61"){
Int_t sigma_dis_compare(const char * expname  = "SLAC-E87"){
//Int_t sigma_dis_compare(const char * expname  = "SLAC-NE11"){
//Int_t sigma_dis_compare(const char * expname  = "SLAC-E891"){
//Int_t sigma_dis_compare(const char * expname  = "SLAC-E8920"){
//Int_t sigma_dis_compare(const char * expname  = "JLAB-E00002"){
//Int_t sigma_dis_compare(const char * expname  = "JLAB-E99118"){
//Int_t sigma_dis_compare(const char * expname  = "JLAB-E94110"){

   NucDBManager * ndb = NucDBManager::GetManager();
   NucDBExperiment * exp = ndb->GetExperiment(expname);
   if(!exp) return( -1);
   NucDBMeasurement * sigma = exp->GetMeasurement("sigma");
   if(!sigma) return(-2);

   sigma->Print();
   /// Create a bins to filter the data
   NucDBBinnedVariable * EbeamBin = new NucDBBinnedVariable("E", "E 5-6 GeV");
   EbeamBin->SetBinValueSize(16,2*15.0);
   //EbeamBin->SetBinMinimum(5.0);
   //EbeamBin->SetBinMaximum(5.0);
   EbeamBin->Print();

   NucDBBinnedVariable * ThetaBin = new NucDBBinnedVariable("theta", "#theta 10-50 deg");
   ThetaBin->SetBinValueSize(50,2*48);

   NucDBMeasurement * sigma_1 = sigma->CreateMeasurementFilteredWithBin(EbeamBin);
   //sigma_1->Print();
   NucDBMeasurement * sigma_2 = sigma_1->CreateMeasurementFilteredWithBin(ThetaBin);
   //sigma_2->Print();
   /// Get values of beam energy and theta
   std::vector<Double_t>   beam_energy;
   std::vector<Double_t>   scat_angle;
   sigma_2->GetUniqueBinnedVariableValues("E",&beam_energy);
   sigma_2->GetUniqueBinnedVariableValues("theta",&scat_angle);

   if( beam_energy.size() == 0 || scat_angle.size() == 0 ) {
      std::cout << "No data to plot\n";
      return(-8);
   }

   TMultiGraph * mg = new TMultiGraph();

   /// Model cross section 
   Int_t TargetType    = 0;
   Int_t Z             = 1;
   Int_t A             = 0;
   Double_t phi        = 0*degree;
   Int_t    npar       = 2;
   Double_t Emin       = 0.5;
   Double_t Emax       = 5.1;
   Double_t minTheta   = 10*degree;
   Double_t maxTheta   = 50.0*degree;

   /// DIS Born cross section 
   InSANEPOLRADBornDiffXSec * fDiffXSec = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec->SetBeamEnergy(beam_energy[beam_energy.size()-1 ]);
   fDiffXSec->SetA(A);
   fDiffXSec->SetZ(Z);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->GetPOLRAD()->SetHelicity(0);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->GetPhaseSpace()->GetVariable("energy_e")->SetMaximum(20.0);
   TF1 * fSigma = new TF1(Form("fSigma%d",0), fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   fSigma->SetParameter(1,phi);
   fSigma->SetNpx(250);


   /// Create the canvas and plot 
   TCanvas * c = new TCanvas("compare","compare");
   c->SetLogy(true);
   TList  * xsec_hists = new TList();

   /// Create the data graphs first
   for(Int_t ie = 0;ie<beam_energy.size();ie++){

      /// create a new measurement for each unique beam energy
      NucDBBinnedVariable * Ebin = new NucDBBinnedVariable("E", Form("E %2.2f GeV",beam_energy[ie]));
      Ebin->SetBinValueSize(beam_energy[ie],0.01);
      NucDBMeasurement * sigma0 = sigma_2->CreateMeasurementFilteredWithBin(Ebin);
      scat_angle.clear();
      sigma0->GetUniqueBinnedVariableValues("theta",&scat_angle);

      TGraphErrors * gr1 = sigma0->BuildGraph("Eprime");
      gr1->SetLineColor(30+ie); 
      gr1->SetMarkerColor(30+ie);
      //gr1->Draw("ap");
      mg->Add(gr1,"p");
      std::cout << "Setting beam energy to : " << beam_energy[ie] << " GeV " << std::endl;

      fSigma->SetRange(Emin,beam_energy[ie]-0.3); 
      fDiffXSec->SetBeamEnergy(beam_energy[ie]);

      for(Int_t it = 0;it<scat_angle.size();it++) {
         fSigma->SetParameter(0,scat_angle[it]*degree);
         fSigma->SetLineColor(kRed-it);
         fSigma->Paint();
         //else fSigma->Draw("same");
         TH1 * h1 = fSigma->GetHistogram()->Clone(Form("h%d-%d",ie,it));
         xsec_hists->Add(h1);
         //xsec_hists->Add(fSigma->GetHistogram()->Clone(Form("h%d-%d",ie,it)));
      }
      //c->Update();
   }

   c->Clear();
   mg->Draw("a");
   xsec_hists->Print();
   
   for(int i = 0;i<xsec_hists->GetEntries();i++){
      TH1 * h1 = (TH1*)xsec_hists->At(i);//->Draw("same");
      //h1->GetListOfFunctions()->Print();
      if(i==0) h1->Draw("C same");
      else h1->Draw("C same");
   }


   c->SaveAs(Form("results/sigma_dis_compare_%s.png",expname));

   return(0);
}
