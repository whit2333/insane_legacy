Int_t dis_inclusive_e(){

   Double_t beamEnergy = 5.9;

   F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->InitializePhaseSpaceVariables();
   InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
   ps->ListVariables();

//    InSANEPolarizedDISAsymmetry * asym = new InSANEPolarizedDISAsymmetry();
//    asym->SetBeamEnergy(beamEnergy);
//    asym->SetThetaPhiTarget(180.0*TMath::Pi()/180.0,0.0);

   TCanvas * c = new TCanvas("DISCrossSection","Inclusive e- DIS SCross Section");
   c->Divide(3,2);

   Int_t npar=2;
   Double_t Emin=0.5;
   Double_t Emax=2.0;
   Double_t minTheta=0.0175*15.0;
   Double_t maxTheta=0.0175*55.0;

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,30.0*TMath::Pi()/180.0);
   sigmaE->SetParameter(1,0.0);

//    TF1 * Aenergy = new TF1("Aenergy", asym, &InSANEBeamTargetAsymmetry::EnergyDependentAsymmetry, 
//                Emin, Emax, npar,"InSANEBeamTargetAsymmetry","EnergyDependentAsymmetry");
//    Aenergy->SetParameter(0,30.0*TMath::Pi()/180.0);
//    Aenergy->SetParameter(1,0.0);

   TF1 * sigmaTheta = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
               minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigmaTheta->SetParameter(0,1.0);
   sigmaTheta->SetParameter(1,0.0);

//    TF1 * ATheta = new TF1("ATheta", asym, &InSANEBeamTargetAsymmetry::PolarAngleDependentAsymmetry, 
//                minTheta, maxTheta, npar,"InSANEBeamTargetAsymmetry","PolarAngleDependentAsymmetry");
//    ATheta->SetParameter(0,1.0);
//    ATheta->SetParameter(1,0.0);

   c->cd(1);
   sigmaE->SetTitle("Inclusive e^{-} Elastic Cross section");
   sigmaE->GetXaxis()->SetTitle("E' [GeV]");
   sigmaE->GetYaxis()->SetTitle("mb/GeV/sr");
   sigmaE->DrawCopy();

   c->cd(2);
   sigmaTheta->SetTitle("Inclusive e^{-} Elastic Cross section");
   sigmaTheta->GetXaxis()->SetTitle("#theta [rad]");
   sigmaTheta->GetYaxis()->SetTitle("mb/GeV/sr");
   sigmaTheta->DrawCopy();

//    c->cd(4);
//    Aenergy->SetTitle("Beam target asymmetry");
//    Aenergy->GetXaxis()->SetTitle("E' [GeV]");
//    Aenergy->GetYaxis()->SetTitle("A");
//    Aenergy->DrawCopy();
// 
//    c->cd(5);
//    ATheta->SetTitle("Beam target asymmetry");
//    ATheta->GetXaxis()->SetTitle("#theta [rad]");
//    ATheta->GetYaxis()->SetTitle("A");
//    ATheta->DrawCopy();
/*
   c->cd(3);
   TH1 * h1E = sigmaE->GetHistogram();
   TH1 * h2E = Aenergy->GetHistogram();
   h1E->Multiply(h2E);
   h1E->SetTitle("Asymmetry weighted cross section");
   h1E->DrawCopy();

   c->cd(6);
   TH1 * h1Theta = sigmaTheta->GetHistogram();
   TH1 * h2Theta = ATheta->GetHistogram();
   h1Theta->Multiply(h2Theta);
   h1Theta->SetTitle("Asymmetry weighted cross section");
   h1Theta->DrawCopy();*/


return(0);
}