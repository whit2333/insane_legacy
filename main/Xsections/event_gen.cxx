{
     Double_t beamEnergy = 5.9;

     F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
     fDiffXSec->SetBeamEnergy(beamEnergy);
     fDiffXSec->InitializePhaseSpaceVariables();
     InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace(); /// all the following cross sections share the same phase space. 
     ps->Print();
     InSANEPhaseSpaceSampler *  fF1F2EventSampler = new InSANEPhaseSpaceSampler(fDiffXSec);

// // Create the differential cross section to be used
//   InSANEInclusiveWiserXSec * WiserDiffXSec = new InSANEInclusiveWiserXSec();
//      WiserDiffXSec->SetBeamEnergy(4.9);
// // Set the cross section's phase space
//   WiserDiffXSec->SetPhaseSpace(fpi0PhaseSpace);
//      InSANEPhaseSpaceSampler *  fWiserEventSampler = new InSANEPhaseSpaceSampler(WiserDiffXSec);

     InSANEEventGenerator * eventGen = new InSANEEventGenerator();
     eventGen->AddSampler(fF1F2EventSampler);
//      eventGen->AddSampler(fWiserEventSampler);
     eventGen->Initialize();
     eventGen->CalculateTotalCrossSection();

     for(int i = 0;i<1000;i++){
        TList * parts = eventGen->GenerateEvent();
        parts->Print();
     }

     eventGen->Print();

}
