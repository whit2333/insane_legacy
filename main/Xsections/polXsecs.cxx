/*! Script showing how to create and event generator for double polarization cross sections.
 *
 */
Int_t polXsecs() {

   Double_t fBeamEnergy = 5.9;
   Double_t target_pol  = 0.6;
   Double_t beam_pol    = 0.2;
   Double_t Emin        = 1.0;
   Double_t Emax        = 3.5;
   Int_t    N_sample    = 10000;

   InSANEPhaseSpace * fPolarizedPhaseSpace = new InSANEPhaseSpace();
   InSANEPhaseSpace * fPhaseSpace          = new InSANEPhaseSpace();

   InSANEPhaseSpaceVariable * varEnergy = new InSANEPhaseSpaceVariable("Energy","E#prime"); 
   varEnergy->SetMinimum(Emin);         //GeV
   varEnergy->SetMaximum(Emax); //GeV
   varEnergy->SetVariableUnits("GeV");        //GeV
   fPolarizedPhaseSpace->AddVariable(varEnergy);
   fPhaseSpace->AddVariable(varEnergy);

   InSANEPhaseSpaceVariable *   varTheta = new InSANEPhaseSpaceVariable("theta","#theta");
   varTheta->SetMinimum(30.0*TMath::Pi()/180.0); //
   varTheta->SetMaximum(40.0*TMath::Pi()/180.0); //
   varTheta->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varTheta);
   fPhaseSpace->AddVariable(varTheta);

   InSANEPhaseSpaceVariable *   varPhi = new InSANEPhaseSpaceVariable("phi","#phi");
   varPhi->SetMinimum(-10.0*TMath::Pi()/180.0); //
   varPhi->SetMaximum(10.0*TMath::Pi()/180.0); //
   varPhi->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varPhi);
   fPhaseSpace->AddVariable(varPhi);

   InSANEDiscretePhaseSpaceVariable *   varHelicity = new InSANEDiscretePhaseSpaceVariable("helicity","#lambda");
   varHelicity->SetNumberOfValues(3); 
   fPolarizedPhaseSpace->AddVariable(varHelicity);

   varEnergy->Print();
   varTheta->Print();
   varPhi->Print();
   varHelicity->Print();

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();

   // --- Cross-sections ---
   // unpolarized 
   F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
   //InSANEInclusiveDiffXSec * fDiffXSec = new InSANEInclusiveDiffXSec();
   fDiffXSec->SetBeamEnergy(fBeamEnergy);
   fDiffXSec->SetA(1);
   fDiffXSec->SetZ(1);
   fDiffXSec->SetPhaseSpace(fPhaseSpace);
   fDiffXSec->Refresh();

   // polarized 
   PolarizedDISXSecParallelHelicity * fDiffXSec1 = new  PolarizedDISXSecParallelHelicity();
   fDiffXSec1->SetBeamEnergy(fBeamEnergy);
   fDiffXSec1->SetPhaseSpace(fPolarizedPhaseSpace);
   fDiffXSec1->Refresh();

   PolarizedDISXSecAntiParallelHelicity * fDiffXSec2 = new  PolarizedDISXSecAntiParallelHelicity();
   fDiffXSec2->SetBeamEnergy(fBeamEnergy);
   fDiffXSec2->SetPhaseSpace(fPolarizedPhaseSpace);
   fDiffXSec2->Refresh();

   // --- 

   // Set the cross section's phase space
   InSANEPolarizedDiffXSec * fPolXSec = new InSANEPolarizedDiffXSec();
   fPolXSec->SetUnpolarizedCrossSection(fDiffXSec);
   fPolXSec->SetPolarizedPositiveCrossSection(fDiffXSec1);
   fPolXSec->SetPolarizedNegativeCrossSection(fDiffXSec2);
   fPolXSec->SetPhaseSpace(fPolarizedPhaseSpace);
   fPolXSec->SetBeamEnergy(fBeamEnergy);
   fPolXSec->SetChargeAsymmetry(-0.002);
   fPolXSec->SetBeamPolarization(beam_pol);
   fPolXSec->SetTargetPolarization(target_pol);

   fPolXSec->Refresh();

   InSANEPhaseSpaceSampler *  fPolSampler = new InSANEPhaseSpaceSampler(fPolXSec);
   fPolSampler->SetFoamCells(2000);
   fPolSampler->SetFoamSample(10);
   fPolSampler->Refresh();

   fPolSampler->Print();
   fDiffXSec->Print();
   fDiffXSec1->Print();
   fDiffXSec2->Print();
   fPolXSec->Print();

   TCanvas * c = new TCanvas("CrossSections","Cross Sections");
   c->Divide(2,2);
   TH2F * h1 = new TH2F("F1F2test","F1F2 Test",100,0.8,4.9,100,20,50);
   TH2F * h4 = new TH2F("QFSTest","QFS Test",100,0.8,4.9,100,20,50);
   TH2F * hNeg = new TH2F("PolXSecNegative","Negative XSec",100,0.8,4.9,100,20,50);
   TH2F * hUnpol = new TH2F("PolXSecUnpolarized","Unpolarized XSec",100,0.8,4.9,100,20,50);
   TH2F * hPos = new TH2F("PolXSecPositive","Positive XSec",100,0.8,4.9,100,20,50);
   TH2F * hDiff = new TH2F("PolXSecDiff","Polarized XSec Diff",100,0.8,4.9,100,20,50);
   TH1F * hHelicity = new TH1F("Helicity","Helicity",5,-2,2);
   h1->GetXaxis()->SetTitle("Energy GeV");
   h1->GetYaxis()->SetTitle("#theta_{e'}");
   h4->GetXaxis()->SetTitle("Energy GeV");
   h4->GetYaxis()->SetTitle("#theta_{e'}");
   hNeg->GetXaxis()->SetTitle("Energy GeV");
   hNeg->GetYaxis()->SetTitle("#theta_{e'}");
   hUnpol->GetXaxis()->SetTitle("Energy GeV");
   hUnpol->GetYaxis()->SetTitle("#theta_{e'}");
   hPos->GetXaxis()->SetTitle("Energy GeV");
   hPos->GetYaxis()->SetTitle("#theta_{e'}");
   hDiff->GetXaxis()->SetTitle("Energy GeV");
   hDiff->GetYaxis()->SetTitle("#theta_{e'}");

   Double_t *res;
   for(int i = 0; i< N_sample;i++) {

      if(i%1000 == 0 ) std::cout << " Generating Event " << i << "\n";
      res = fPolSampler->GenerateEvent();

      Int_t hel = varHelicity->GetDiscreteVariable(res[3]) ;

      if(hel == 0) {hUnpol->Fill(res[0],res[1]*180.0/TMath::Pi());hel = 0;}
      if(hel == 1) {  hPos->Fill(res[0],res[1]*180.0/TMath::Pi());hel = 1;hHelicity->Fill(hel);}
      if(hel == 2) {  hNeg->Fill(res[0],res[1]*180.0/TMath::Pi());hel =-1;hHelicity->Fill(hel);}
//       if(i%500 == 0 ) std::cout << " helicity = " << varHelicity->GetDiscreteVariable(res[3]) 
//                                 << " and hel = " << hel << "\n";
//       
      hDiff->Fill(res[0],res[1]*180.0/TMath::Pi(),hel);
//       std::cout << " Xsec = " << fPolXSec->EvaluateXSec(res) << "  hel=" << hel <<" \n";

   }
   c->cd(1);
   hPos->Draw("COLZ");
   c->cd(2);
   hNeg->Draw("COLZ");
   c->cd(3);
   hUnpol->Draw("COLZ");
   c->cd(4);
   hDiff->Draw("COLZ");

   c->SaveAs("results/polXsecs.png");

   return(0);
}
