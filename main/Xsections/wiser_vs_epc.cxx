Int_t wiser_vs_epc(){

   InSANEInclusiveWiserXSec * fDiffXSec = new InSANEInclusiveWiserXSec();
   fDiffXSec->SetBeamEnergy(11.0);
   fDiffXSec->SetProductionParticleType(-211);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace * fPhaseSpace = fDiffXSec->GetPhaseSpace();

   fPhaseSpace->GetVariable("theta_pi")->SetMinimum(1.0*0.0175);
   fPhaseSpace->GetVariable("momentum_pi")->SetMinimum(0.1);

   Double_t * momentum =  fPhaseSpace->GetVariable("momentum_pi")->GetCurrentValueAddress();
   Double_t * theta =  fPhaseSpace->GetVariable("theta_pi")->GetCurrentValueAddress();
   (*momentum) = 1.8;
   (*theta) = 4.0*TMath::Pi()/180.0;
/*   fDiffXSec->Print();*/

// Z = 13, N = 14, E = 11 GeV, rad_len = 1.025 %, theta = 4 deg, E' = 1800 MeV;
// Z = 13, N = 14, E = 11 GeV, rad_len = 1.025 %, theta = 5.5 deg, E' = 2100
// MeV;
// Z = 13, N = 14, E = 11 GeV, rad_len = 1.025 %, theta = 8.0 deg, E' = 2400
// MeV;

   fDiffXSec->Print();

   (*momentum) = 1.8;
   (*theta) = 4.0*TMath::Pi()/180.0;
   fDiffXSec->PrintTable();

   (*momentum) = 2.1;
   (*theta) = 5.50*TMath::Pi()/180.0;
   fDiffXSec->PrintTable();

   (*momentum) = 2.4;
   (*theta) = 8.0*TMath::Pi()/180.0;
   fDiffXSec->PrintTable();


   /// EPCV 

   InSANEInclusiveEPCVXSec * fEPCVDiffXSec = new InSANEInclusiveEPCVXSec();
   fEPCVDiffXSec->SetBeamEnergy(11.0);
   fEPCVDiffXSec->SetProductionParticleType(211);
   fEPCVDiffXSec->InitializePhaseSpaceVariables();
   fEPCVDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace * fPhaseSpaceEPCV = fEPCVDiffXSec->GetPhaseSpace();

   fPhaseSpaceEPCV->GetVariable("theta_pi")->SetMinimum(1.0*0.0175);
   fPhaseSpaceEPCV->GetVariable("momentum_pi")->SetMinimum(0.1);

   Double_t * momentum2 =  fPhaseSpaceEPCV->GetVariable("momentum_pi")->GetCurrentValueAddress();
   Double_t * theta2 =  fPhaseSpaceEPCV->GetVariable("theta_pi")->GetCurrentValueAddress();
   (*momentum2) = 1.8;
   (*theta2) = 4.0*TMath::Pi()/180.0;

   fEPCVDiffXSec->Print();

   (*momentum2) = 1.8;
   (*theta2) = 4.0*TMath::Pi()/180.0;
   fEPCVDiffXSec->PrintTable();

   (*momentum2) = 2.1;
   (*theta2) = 5.50*TMath::Pi()/180.0;
   fEPCVDiffXSec->PrintTable();

   (*momentum2) = 2.4;
   (*theta2) = 8.0*TMath::Pi()/180.0;
   fEPCVDiffXSec->PrintTable();

   ///
   TCanvas * c1 = new TCanvas("WiserVsEpcv","Wiser vs EPCV");
   c1->Divide(1,2);

   c1->cd(1);
   Int_t npar = 2;
   TF1 * incWiserXSec = new TF1("incWiser", fDiffXSec,
                                &InSANEInclusiveDiffXSec::MomentumDependentXSec, 
                                0.1,4.0, npar,
                                "InSANEInclusiveDiffXSec","MomentumDependentXSec");

   fDiffXSec->SetProductionParticleType(111);
   incWiserXSec->SetParameters(5.50*TMath::Pi()/180.0,0.0);
   incWiserXSec->SetTitle(Form("%s Wiser",fDiffXSec->fPlotTitle.Data()));
   incWiserXSec->SetLineColor(1);
   incWiserXSec->DrawClone();

   fDiffXSec->SetProductionParticleType(211);
   incWiserXSec->SetLineColor(2);
   incWiserXSec->DrawClone("same");

   fDiffXSec->SetProductionParticleType(-211);
   incWiserXSec->SetLineColor(3);
   incWiserXSec->DrawClone("same");


   c1->cd(2);

   TF1 * incEPCVXSec = new TF1("incEPCV", fEPCVDiffXSec,
                                &InSANEInclusiveDiffXSec::MomentumDependentXSec, 
                                0.1,4.0, npar,
                                "InSANEInclusiveDiffXSec","MomentumDependentXSec");

   fEPCVDiffXSec->SetProductionParticleType(111);
   incEPCVXSec->SetParameters(5.50*TMath::Pi()/180.0,0.0);
   incEPCVXSec->SetTitle(Form("%s - EPC",fDiffXSec->fPlotTitle.Data()));
   incEPCVXSec->SetLineColor(1);
   incEPCVXSec->DrawClone();

   fEPCVDiffXSec->SetProductionParticleType(211);
   incEPCVXSec->SetLineColor(2);
   incEPCVXSec->DrawClone("same");

   fEPCVDiffXSec->SetProductionParticleType(-211);
   incEPCVXSec->SetLineColor(3);
   incEPCVXSec->DrawClone("same");

return(0);
}
