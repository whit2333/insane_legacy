/*!  An attempt to create an inclusive event generator which produces different particles.
    
    It samples different phase spaces, thus normalization becomes difficult.

 */
Int_t inclusive_all(){

   Double_t fBeamEnergy = 5.9; //GeV

   /// First create the pi0 cross section and phase-space sampler.
   InSANEInclusiveWiserXSec * pi0XSec = new InSANEInclusiveWiserXSec();
   pi0XSec->SetBeamEnergy(fBeamEnergy);
   pi0XSec->SetProductionParticleType(111);
   pi0XSec->InitializePhaseSpaceVariables();
   pi0XSec->InitializeFinalStateParticles();
/*   pi0XSec->SetProductionParticleType(111);*/
   InSANEPhaseSpaceSampler *  pi0PSSampler = new InSANEPhaseSpaceSampler(pi0XSec);

   /// Create an inclusive DIS cross section and sampler.
   F1F209eInclusiveDiffXSec * disXSec = new  F1F209eInclusiveDiffXSec();
   disXSec->SetBeamEnergy(fBeamEnergy);
   disXSec->InitializePhaseSpaceVariables();
   disXSec->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler *  disPSSampler = new InSANEPhaseSpaceSampler(disXSec);

/*   pi0XSec->Print();*/
//    disXSec->Print();

   InSANEEventGenerator * eventGen = new InSANEEventGenerator();
   eventGen->AddSampler(pi0PSSampler);
   eventGen->AddSampler(disPSSampler);
/*   eventGen->Print();*/
   eventGen->Initialize();
   eventGen->CalculateTotalCrossSection();
   eventGen->Print();
//    eventGen->Print();


   Int_t nElectrons =0;
   Int_t nPi0s = 0;
   TParticle * p = 0;
   for(int i = 0;i<10;i++){
      TList * parts = eventGen->GenerateEvent();
      parts->Print();
      p = (TParticle*)parts->At(0);
      std::cout << p << "\n";
      if(p)if(p->GetPdgCode() == 11) nElectrons++;
      if(p)if(p->GetPdgCode() == 111) nPi0s++;
   }
   eventGen->Print();

   std::cout << " Ratio of electrons to pi0s is " << ((double)nElectrons)/((double)nPi0s) << "\n";

   return(0);
}
