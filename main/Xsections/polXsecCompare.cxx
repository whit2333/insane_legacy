/*! Script showing how to create and event generator for double polarization cross sections.
 *
 */
Int_t polXsecCompare() {

   Double_t theta_target = 180.0*degree;
   Double_t fBeamEnergy = 5.9; 

   //InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();
   //cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
   //InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();
   //fm->SetStructureFunctions(cteqSFs);

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);

   InSANEPhaseSpace * fPolarizedPhaseSpace = new InSANEPhaseSpace();
   InSANEPhaseSpace * fPhaseSpace          = new InSANEPhaseSpace();

   Double_t Emin = 0.6;
   Double_t Emax = 2.4;

   InSANEPhaseSpaceVariable * varEnergy = new InSANEPhaseSpaceVariable("Energy","E#prime"); 
   varEnergy->SetMinimum(Emin);         //GeV
   varEnergy->SetMaximum(Emax); //GeV
   varEnergy->SetVariableUnits("GeV");        //GeV
   fPolarizedPhaseSpace->AddVariable(varEnergy);
   fPhaseSpace->AddVariable(varEnergy);

   InSANEPhaseSpaceVariable *   varTheta = new InSANEPhaseSpaceVariable("theta","#theta");
   varTheta->SetMinimum(20.0*TMath::Pi()/180.0); //
   varTheta->SetMaximum(50.0*TMath::Pi()/180.0); //
   varTheta->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varTheta);
   fPhaseSpace->AddVariable(varTheta);

   InSANEPhaseSpaceVariable *   varPhi = new InSANEPhaseSpaceVariable("phi","#phi");
   varPhi->SetMinimum(-10.0*TMath::Pi()/180.0); //
   varPhi->SetMaximum(10.0*TMath::Pi()/180.0); //
   varPhi->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varPhi);
   fPhaseSpace->AddVariable(varPhi);

   InSANEDiscretePhaseSpaceVariable *   varHelicity = new InSANEDiscretePhaseSpaceVariable("helicity","#lambda");
   varHelicity->SetNumberOfValues(3); // ROOT string latex
   fPolarizedPhaseSpace->AddVariable(varHelicity);

   fPolarizedPhaseSpace->Refresh();

   varEnergy->Print();
   varTheta->Print();
   varPhi->Print();
   varHelicity->Print();

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();

   // --- Cross-sections ---
  
   // born unpolarized 
   F1F209eInclusiveDiffXSec * fDiffXSec00 = new  F1F209eInclusiveDiffXSec();
   fDiffXSec00->SetBeamEnergy(fBeamEnergy);
   fDiffXSec00->SetA(1);
   fDiffXSec00->SetZ(1);
   fDiffXSec00->SetPhaseSpace(fPhaseSpace);
   fDiffXSec00->Refresh();
   ofstream fil("xsec_test.txt");
   //fDiffXSec00->PrintGrid(fil);

   // internal radiated unpolarized 
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec01 = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec01->GetPOLRAD()->SetHelicity(0);
   fDiffXSec01->SetBeamEnergy(fBeamEnergy);
   fDiffXSec01->SetA(1);
   fDiffXSec01->SetZ(1);
   fDiffXSec01->SetPhaseSpace(fPhaseSpace);
   fDiffXSec01->Refresh();

   // internal radiated unpolarized 
   InSANEPOLRADRadiatedDiffXSec * fDiffXSec02 = new  InSANEPOLRADRadiatedDiffXSec();
   fDiffXSec02->GetPOLRAD()->SetHelicity(0);
   fDiffXSec02->SetBeamEnergy(fBeamEnergy);
   fDiffXSec02->SetA(1);
   fDiffXSec02->SetZ(1);
   fDiffXSec02->SetPhaseSpace(fPhaseSpace);
   fDiffXSec02->Refresh();

   // born polarized + 
   PolarizedDISXSecParallelHelicity * fDiffXSec10 = new  PolarizedDISXSecParallelHelicity();
   fDiffXSec10->SetBeamEnergy(fBeamEnergy);
   fDiffXSec10->SetPhaseSpace(fPolarizedPhaseSpace);
   fDiffXSec10->SetTargetPolarizationAngle(theta_target);
   fDiffXSec10->Refresh();

   // internal radiated polarized + IRT
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec11 = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec11->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec11->SetBeamEnergy(fBeamEnergy);
   fDiffXSec11->SetA(1);
   fDiffXSec11->SetZ(1);
   fDiffXSec11->SetPhaseSpace(fPhaseSpace);
   fDiffXSec11->Refresh();

   // internal radiated polarized + 
   InSANEPOLRADRadiatedDiffXSec * fDiffXSec12 = new  InSANEPOLRADRadiatedDiffXSec();
   fDiffXSec12->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec12->SetBeamEnergy(fBeamEnergy);
   fDiffXSec12->SetA(1);
   fDiffXSec12->SetZ(1);
   fDiffXSec12->SetPhaseSpace(fPhaseSpace);
   fDiffXSec12->Refresh();

   // born polarized - 
   PolarizedDISXSecAntiParallelHelicity * fDiffXSec20 = new  PolarizedDISXSecAntiParallelHelicity();
   fDiffXSec20->SetBeamEnergy(fBeamEnergy);
   fDiffXSec20->SetPhaseSpace(fPolarizedPhaseSpace);
   fDiffXSec20->SetTargetPolarizationAngle(theta_target);
   fDiffXSec20->Refresh();

   // internal radiated polarized -
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec21 = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec21->GetPOLRAD()->SetHelicity(1);
   fDiffXSec21->SetBeamEnergy(fBeamEnergy);
   fDiffXSec21->SetA(1);
   fDiffXSec21->SetZ(1);
   fDiffXSec21->SetPhaseSpace(fPhaseSpace);
   fDiffXSec21->Refresh();

   // internal radiated polarized - 
   InSANEPOLRADRadiatedDiffXSec * fDiffXSec22 = new  InSANEPOLRADRadiatedDiffXSec();
   fDiffXSec22->GetPOLRAD()->SetHelicity(1);
   fDiffXSec22->SetBeamEnergy(fBeamEnergy);
   fDiffXSec22->SetA(1);
   fDiffXSec22->SetZ(1);
   fDiffXSec22->SetPhaseSpace(fPhaseSpace);
   fDiffXSec22->Refresh();

   // ---------------------- 

   fDiffXSec00->Print();
   fDiffXSec01->Print();
   fDiffXSec02->Print();
   fDiffXSec10->Print();
   fDiffXSec11->Print();
   fDiffXSec12->Print();
   fDiffXSec20->Print();
   fDiffXSec21->Print();
   fDiffXSec22->Print();

   // ---------------------- 
   // functions
 
   Double_t theta = 40.0*degree;
   Int_t npar = 2;
   Int_t npx = 50;

   // ---------------------- 
   // Born unpolarized
   TF1 * sig00 = new TF1("sig00",fDiffXSec00,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig00->SetParameter(0,theta);
   sig00->SetParameter(1,0.0);
   sig00->SetNpx(npx);
   sig00->SetLineWidth(2);
   sig00->SetLineStyle(2);
   sig00->SetLineColor(kBlack);
   leg->AddEntry(sig00,"Born unpolarized","l");
   
   // Internal radiated  unpolarized IRT
   TF1 * sig01 = new TF1("sig01",fDiffXSec01,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig01->SetNpx(npx);
   sig01->SetParameter(0,theta);
   sig01->SetParameter(1,0.0);
   sig01->SetLineStyle(3);
   sig01->SetLineWidth(2);
   sig01->SetLineColor(kBlack);
   leg->AddEntry(sig01,"Internally radiated (polrad) unpolarized IRT","l");

   // Internal radiated  unpolarized IRT
   TF1 * sig02 = new TF1("sig02",fDiffXSec02,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig02->SetNpx(npx);
   sig02->SetParameter(0,theta);
   sig02->SetParameter(1,0.0);
   sig02->SetLineStyle(1);
   sig02->SetLineWidth(2);
   sig02->SetLineColor(kBlack);
   leg->AddEntry(sig02,"Internally radiated (polrad) unpolarized","l");

   // ---------------------- 
   // born polarized + 
   TF1 * sig10 = new TF1("sig10",fDiffXSec10,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig10->SetParameter(0,theta);
   sig10->SetParameter(1,0.0);
   sig10->SetNpx(npx);
   sig10->SetLineWidth(2);
   sig10->SetLineStyle(2);
   sig10->SetLineColor(2);
   leg->AddEntry(sig10,"Born polarized parallel","l");

   // int radiated polarized + IRT
   TF1 * sig11 = new TF1("sig11",fDiffXSec11,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig11->SetParameter(0,theta);
   sig11->SetParameter(1,0.0);
   sig11->SetNpx(npx);
   sig11->SetLineWidth(2);
   sig11->SetLineStyle(3);
   sig11->SetLineColor(2);
   leg->AddEntry(sig11,"Int.Rad. (polrad) polarized parallel IRT","l");

   // Internal radiated  polarized +
   TF1 * sig12 = new TF1("sig12",fDiffXSec12,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig12->SetNpx(npx);
   sig12->SetParameter(0,theta);
   sig12->SetParameter(1,0.0);
   sig12->SetLineStyle(1);
   sig12->SetLineWidth(2);
   sig12->SetLineColor(2);
   leg->AddEntry(sig12,"Int.Rad. (polrad) polarized parallel","l");

   // ---------------------- 
   // born polarized - 
   TF1 * sig20 = new TF1("sig20",fDiffXSec20,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig20->SetParameter(0,theta);
   sig20->SetParameter(1,0.0);
   sig20->SetNpx(npx);
   sig20->SetLineWidth(2);
   sig20->SetLineStyle(2);
   sig20->SetLineColor(4);
   leg->AddEntry(sig20,"Born pol. anti-parallel","l");

   // int radiated polarized - IRT
   TF1 * sig21 = new TF1("sig21",fDiffXSec21,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig21->SetParameter(0,theta);
   sig21->SetParameter(1,0.0);
   sig21->SetNpx(npx);
   sig21->SetLineWidth(2);
   sig21->SetLineStyle(3);
   sig21->SetLineColor(4);
   leg->AddEntry(sig21,"Int.Rad. (polrad) pol. anti-parallel IRT","l");

   // Internal radiated  polarized -
   TF1 * sig22 = new TF1("sig22",fDiffXSec22,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
         Emin,Emax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sig22->SetNpx(npx);
   sig22->SetParameter(0,theta);
   sig22->SetParameter(1,0.0);
   sig22->SetLineStyle(1);
   sig22->SetLineWidth(2);
   sig22->SetLineColor(4);
   leg->AddEntry(sig22,"Int.Rad. (polrad) pol. anti-parallel","l");

   TCanvas * c = new TCanvas("polXsecCompare","polXsecCompare");

   sig02->Draw();

   sig02->GetXaxis()->SetTitle("E'");
   sig02->GetYaxis()->SetTitle("#frac{d#sigma}{dE'd#Omega} (nb/GeV/sr)");

   sig01->Draw("same");
   sig00->Draw("same");
   sig10->Draw("same");
   sig11->Draw("same");
   sig12->Draw("same");
   sig20->Draw("same");
   sig21->Draw("same");
   sig22->Draw("same");
   leg->Draw();

   c->SaveAs("results/polXsecCompare.png");
   c->SaveAs("results/polXsecCompare.pdf");

   return(0);
}

