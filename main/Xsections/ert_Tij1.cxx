

Int_t ert_Tij1(){

   Double_t beamEnergy  = 5.9;
   Double_t theta       = 45.0*degree;//29.0*TMath::Pi()/180.0;
   Double_t Eprime      = beamEnergy*(1.-0.869);

   InSANENucleus::NucleusType Target = InSANENucleus::kProton; 

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->GetPOLRAD()->SetTargetType(Target);
   fDiffXSec4->GetPOLRAD()->SetHelicity(-1.);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
   //ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4 =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4 =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4) = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e4) = 0.0*TMath::Pi()/180.0;
   fDiffXSec4->EvaluateCurrent();
   //fDiffXSec4->Print();
   //fDiffXSec4->PrintTable();

   Int_t    Npoints  = 1000;
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   InSANEPOLRAD * polrad = fDiffXSec4->GetPOLRAD();
   Double_t tau_min = polrad->GetKinematics()->GetTau_A_min();
   Double_t tau_max = polrad->GetKinematics()->GetTau_A_max();

   // cout << "tau min = " << tau_min << endl;
   // cout << "tau max = " << tau_max << endl;
   // exit(1); 
 
   Double_t xbj = polrad->GetKinematics()->Getx();
   Double_t Q2  = polrad->GetKinematics()->GetQ2();
   Double_t Mp  = polrad->GetKinematics()->GetM();
   Double_t W2  = polrad->GetKinematics()->GetW2();
   Double_t nu  = polrad->GetKinematics()->Gety()*beamEnergy;
   Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
   Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
   Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
   Double_t tau_u = 0.67;
   Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;

   //polrad->GetKinematics()->Print();
   std::cout << "tau_min = " << tau_min << "\n";
   std::cout << "tau_max = " << tau_max << "\n";
   std::cout << "Q2 = " << Q2 << "\n";
   std::cout << "W2 = " << W2 << "\n";
   std::cout << "Mp = " << Mp << "\n";
   std::cout << "xbj = " << xbj << "\n";
   std::cout << "tau_me_peak = " << tau_me_peak << "\n";
   std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
   std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
   std::cout << "t = " << t << "\n";


   vector<double> tau111,th111;
   vector<double> tau121,th121;
   vector<double> tau131,th131;

   vector<double> tau211,th211;
   vector<double> tau221,th221;
   vector<double> tau231,th231;

   vector<double> tau311,th311;
   vector<double> tau321,th321;
   vector<double> tau331,th331;
   vector<double> tau341,th341;

   vector<double> tau411,th411;
   vector<double> tau421,th421;
   vector<double> tau431,th431;
   vector<double> tau441,th441;

   vector<double> tau711,th711;
   vector<double> tau721,th721;
   vector<double> tau731,th731;

   vector<double> tau811,th811;
   vector<double> tau821,th821;
   vector<double> tau831,th831;

   ImportThData(1,1,1,tau111,th111);
   ImportThData(1,2,1,tau121,th121);
   ImportThData(1,3,1,tau131,th131);

   ImportThData(2,1,1,tau211,th211);
   ImportThData(2,2,1,tau221,th221);
   ImportThData(2,3,1,tau231,th231);

   ImportThData(3,1,1,tau311,th311);
   ImportThData(3,2,1,tau321,th321);
   ImportThData(3,3,1,tau331,th331);
   ImportThData(3,4,1,tau341,th341);

   ImportThData(4,1,1,tau411,th411);
   ImportThData(4,2,1,tau421,th421);
   ImportThData(4,3,1,tau431,th431);
   ImportThData(4,4,1,tau441,th441);

   ImportThData(7,1,1,tau711,th711);
   ImportThData(7,2,1,tau721,th721);
   ImportThData(7,3,1,tau731,th731);

   ImportThData(8,1,1,tau811,th811);
   ImportThData(8,2,1,tau821,th821);
   ImportThData(8,3,1,tau831,th831);

   Int_t width = 2; 

   TGraph *g111      = GetTGraph(tau111,th111);
   TGraph *g121      = GetTGraph(tau121,th121);
   TGraph *g131      = GetTGraph(tau131,th131);

   TGraph *g211      = GetTGraph(tau211,th211);
   TGraph *g221      = GetTGraph(tau221,th221);
   TGraph *g231      = GetTGraph(tau231,th231);

   TGraph *g311      = GetTGraph(tau311,th311);
   TGraph *g321      = GetTGraph(tau321,th321);
   TGraph *g331      = GetTGraph(tau331,th331);
   TGraph *g341      = GetTGraph(tau341,th341);

   TGraph *g411      = GetTGraph(tau411,th411);
   TGraph *g421      = GetTGraph(tau421,th421);
   TGraph *g431      = GetTGraph(tau431,th431);
   TGraph *g441      = GetTGraph(tau441,th441);

   TGraph *g711      = GetTGraph(tau711,th711);
   TGraph *g721      = GetTGraph(tau721,th721);
   TGraph *g731      = GetTGraph(tau731,th731);

   TGraph *g811      = GetTGraph(tau811,th811);
   TGraph *g821      = GetTGraph(tau821,th821);
   TGraph *g831      = GetTGraph(tau831,th831);

   g311->SetLineWidth(width);

   TF1 * fT111 = new TF1("fT111", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT111->SetParameter(0,1);
   fT111->SetParameter(1,1);
   fT111->SetParameter(2,1);
   fT111->SetNpx(Npoints);
   fT111->SetLineColor(kMagenta);
   fT111->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT121 = new TF1("fT121", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT121->SetParameter(0,1);
   fT121->SetParameter(1,2);
   fT121->SetParameter(2,1);
   fT121->SetNpx(Npoints);
   fT121->SetLineColor(kMagenta);
   fT121->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT131 = new TF1("fT131", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT131->SetParameter(0,1);
   fT131->SetParameter(1,3);
   fT131->SetParameter(2,1);
   fT131->SetNpx(Npoints);
   fT131->SetLineColor(kMagenta);
   fT131->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT211 = new TF1("fT211", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT211->SetParameter(0,2);
   fT211->SetParameter(1,1);
   fT211->SetParameter(2,1);
   fT211->SetNpx(Npoints);
   fT211->SetLineColor(kMagenta);
   fT211->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT221 = new TF1("fT221", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT221->SetParameter(0,2);
   fT221->SetParameter(1,2);
   fT221->SetParameter(2,1);
   fT221->SetNpx(Npoints);
   fT221->SetLineColor(kMagenta);
   fT221->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT231 = new TF1("fT231", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT231->SetParameter(0,2);
   fT231->SetParameter(1,3);
   fT231->SetParameter(2,1);
   fT231->SetNpx(Npoints);
   fT231->SetLineColor(kMagenta);
   fT231->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT311 = new TF1("fT311", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT311->SetParameter(0,3);
   fT311->SetParameter(1,1);
   fT311->SetParameter(2,1);
   fT311->SetNpx(Npoints);
   fT311->SetLineColor(kMagenta);
   fT311->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();
   // fT311->DrawCopy();

   TF1 * fT321 = new TF1("fT321", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT321->SetParameter(0,3);
   fT321->SetParameter(1,2);
   fT321->SetParameter(2,1);
   fT321->SetNpx(Npoints);
   fT321->SetLineColor(kMagenta);
   fT321->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();
   // fT321->DrawCopy();

   TF1 * fT331 = new TF1("fT331", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT331->SetParameter(0,3);
   fT331->SetParameter(1,3);
   fT331->SetParameter(2,1);
   fT331->SetNpx(Npoints);
   fT331->SetLineColor(kMagenta);
   fT331->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();
   // fT331->DrawCopy();

   TF1 * fT341 = new TF1("fT341", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT341->SetParameter(0,3);
   fT341->SetParameter(1,4);
   fT341->SetParameter(2,1);
   fT341->SetNpx(Npoints);
   fT341->SetLineColor(kMagenta);
   fT341->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();
   // fT341->DrawCopy();

   TF1 * fT411 = new TF1("fT411", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT411->SetParameter(0,4);
   fT411->SetParameter(1,1);
   fT411->SetParameter(2,1);
   fT411->SetNpx(Npoints);
   fT411->SetLineColor(kMagenta);
   fT411->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();
   // fT411->DrawCopy();

   TF1 * fT421 = new TF1("fT421", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT421->SetParameter(0,4);
   fT421->SetParameter(1,2);
   fT421->SetParameter(2,1);
   fT421->SetNpx(Npoints);
   fT421->SetLineColor(kMagenta);
   fT421->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();
   // fT421->DrawCopy();

   TF1 * fT431 = new TF1("fT431", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT431->SetParameter(0,4);
   fT431->SetParameter(1,3);
   fT431->SetParameter(2,1);
   fT431->SetNpx(Npoints);
   fT431->SetLineColor(kMagenta);
   fT431->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();
   // fT431->DrawCopy();

   TF1 * fT441 = new TF1("fT441", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT441->SetParameter(0,4);
   fT441->SetParameter(1,4);
   fT441->SetParameter(2,1);
   fT441->SetNpx(Npoints);
   fT441->SetLineColor(kMagenta);
   fT441->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();
   // fT441->DrawCopy();

   TF1 * fT711 = new TF1("fT711", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT711->SetParameter(0,7);
   fT711->SetParameter(1,1);
   fT711->SetParameter(2,1);
   fT711->SetNpx(Npoints);
   fT711->SetLineColor(kMagenta);
   fT711->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT721 = new TF1("fT721", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT721->SetParameter(0,7);
   fT721->SetParameter(1,2);
   fT721->SetParameter(2,1);
   fT721->SetNpx(Npoints);
   fT721->SetLineColor(kMagenta);
   fT721->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT731 = new TF1("fT731", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT731->SetParameter(0,7);
   fT731->SetParameter(1,3);
   fT731->SetParameter(2,1);
   fT731->SetNpx(Npoints);
   fT731->SetLineColor(kMagenta);
   fT731->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT811 = new TF1("fT811", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT811->SetParameter(0,8);
   fT811->SetParameter(1,1);
   fT811->SetParameter(2,1);
   fT811->SetNpx(Npoints);
   fT811->SetLineColor(kMagenta);
   fT811->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT821 = new TF1("fT221", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT821->SetParameter(0,8);
   fT821->SetParameter(1,2);
   fT821->SetParameter(2,1);
   fT821->SetNpx(Npoints);
   fT821->SetLineColor(kMagenta);
   fT821->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TF1 * fT831 = new TF1("fT831", polrad, &InSANEPOLRAD::TijkPlot_A, 
                tau_min, tau_max, 3,"InSANEPOLRAD","TijkPlot_A");
   fT831->SetParameter(0,8);
   fT831->SetParameter(1,3);
   fT831->SetParameter(2,1);
   fT831->SetNpx(Npoints);
   fT831->SetLineColor(kMagenta);
   fT831->SetLineWidth(width);
   fDiffXSec4->EvaluateCurrent();

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
   Leg->SetFillColor(kWhite); 
   Leg->AddEntry(g111 ,"Fortran","l");
   Leg->AddEntry(fT111,"C++"    ,"l");

   TCanvas *c1 = new TCanvas("c1","Theta i = 1, 2, k = 1",1000,800);
   c1->SetFillColor(kWhite);
   c1->Divide(3,2); 

   TString DrawOption = Form("AP"); 
   TString xAxisTitle = Form("#tau");

   Double_t min = -8E+6; 
   Double_t max =  8E+6; 

   c1->cd(1); 
   g111->Draw(DrawOption);
   g111->SetTitle("#theta_{111}");
   g111->GetXaxis()->SetTitle(xAxisTitle);
   g111->GetXaxis()->CenterTitle();
   g111->Draw(DrawOption);
   fT111->Draw("same");
   Leg->Draw("same");
   c1->Update();

   c1->cd(2); 
   g121->Draw(DrawOption);
   g121->SetTitle("#theta_{121}");
   g121->GetXaxis()->SetTitle(xAxisTitle);
   g121->GetXaxis()->CenterTitle();
   g121->Draw(DrawOption);
   fT121->Draw("same");
   c1->Update();

   c1->cd(3); 
   g131->Draw(DrawOption);
   g131->SetTitle("#theta_{131}");
   g131->GetXaxis()->SetTitle(xAxisTitle);
   g131->GetXaxis()->CenterTitle();
   g131->Draw(DrawOption);
   fT131->Draw("same");
   c1->Update();

   c1->cd(4); 
   g211->Draw(DrawOption);
   g211->SetTitle("#theta_{211}");
   g211->GetXaxis()->SetTitle(xAxisTitle);
   g211->GetXaxis()->CenterTitle();
   g211->Draw(DrawOption);
   fT211->Draw("same");
   c1->Update();

   c1->cd(5); 
   g221->Draw(DrawOption);
   g221->SetTitle("#theta_{221}");
   g221->GetXaxis()->SetTitle(xAxisTitle);
   g221->GetXaxis()->CenterTitle();
   g221->Draw(DrawOption);
   fT221->Draw("same");
   c1->Update();

   c1->cd(6); 
   g231->Draw(DrawOption);
   g231->SetTitle("#theta_{231}");
   g231->GetXaxis()->SetTitle(xAxisTitle);
   g231->GetXaxis()->CenterTitle();
   g231->Draw(DrawOption);
   fT231->Draw("same");
   c1->Update();
   c1->SaveAs("ert_Tij1_c1.pdf");
return(0);
   TCanvas *c2 = new TCanvas("c2","Theta i = 3 and 4, k = 1",1000,800);
   c2->SetFillColor(kWhite);
   c2->Divide(4,2); 

   c2->cd(1); 
   g311->Draw(DrawOption);
   g311->SetTitle("#theta_{311}");
   g311->GetXaxis()->SetTitle(xAxisTitle);
   g311->GetXaxis()->CenterTitle();
   g311->Draw(DrawOption);
   fT311->Draw("same");
   Leg->Draw("same");
   c2->Update();

   c2->cd(2); 
   g321->Draw(DrawOption);
   g321->SetTitle("#theta_{321}");
   g321->GetXaxis()->SetTitle(xAxisTitle);
   g321->GetXaxis()->CenterTitle();
   g321->Draw(DrawOption);
   fT321->Draw("same");
   c2->Update();

   c2->cd(3); 
   g331->Draw(DrawOption);
   g331->SetTitle("#theta_{331}");
   g331->GetXaxis()->SetTitle(xAxisTitle);
   g331->GetXaxis()->CenterTitle();
   g331->Draw(DrawOption);
   fT331->Draw("same");
   c2->Update();

   c2->cd(4); 
   g341->Draw(DrawOption);
   g341->SetTitle("#theta_{341}");
   g341->GetXaxis()->SetTitle(xAxisTitle);
   g341->GetXaxis()->CenterTitle();
   g341->Draw(DrawOption);
   fT341->Draw("same");
   c2->Update();

   c2->cd(5); 
   g411->Draw(DrawOption);
   g411->SetTitle("#theta_{411}");
   g411->GetXaxis()->SetTitle(xAxisTitle);
   g411->GetXaxis()->CenterTitle();
   g411->Draw(DrawOption);
   fT411->Draw("same");
   c2->Update();

   c2->cd(6); 
   g421->Draw(DrawOption);
   g421->SetTitle("#theta_{421}");
   g421->GetXaxis()->SetTitle(xAxisTitle);
   g421->GetXaxis()->CenterTitle();
   g421->Draw(DrawOption);
   fT421->Draw("same");
   c2->Update();

   c2->cd(7); 
   g431->Draw(DrawOption);
   g431->SetTitle("#theta_{431}");
   g431->GetXaxis()->SetTitle(xAxisTitle);
   g431->GetXaxis()->CenterTitle();
   g431->Draw(DrawOption);
   fT431->Draw("same");
   c1->Update();

   c2->cd(8); 
   g441->Draw(DrawOption);
   g441->SetTitle("#theta_{441}");
   g441->GetXaxis()->SetTitle(xAxisTitle);
   g441->GetXaxis()->CenterTitle();
   g441->Draw(DrawOption);
   fT441->Draw("same");
   c2->Update();
   c2->SaveAs("ert_Tij1_c2.pdf");

   TCanvas *c3 = new TCanvas("c3","Theta i = 7, 8, k = 1",1000,800);
   c3->SetFillColor(kWhite);
   c3->Divide(3,2); 

   c3->cd(1); 
   g711->Draw(DrawOption);
   g711->SetTitle("#theta_{711}");
   g711->GetXaxis()->SetTitle(xAxisTitle);
   g711->GetXaxis()->CenterTitle();
   g711->GetYaxis()->SetRangeUser(min,max);
   g711->Draw(DrawOption);
   fT711->Draw("same");
   Leg->Draw("same");
   c3->Update();

   c3->cd(2); 
   g721->Draw(DrawOption);
   g721->SetTitle("#theta_{721}");
   g721->GetXaxis()->SetTitle(xAxisTitle);
   g721->GetXaxis()->CenterTitle();
   g721->GetYaxis()->SetRangeUser(min,max);
   g721->Draw(DrawOption);
   fT721->Draw("same");
   c3->Update();

   c3->cd(3); 
   g731->Draw(DrawOption);
   g731->SetTitle("#theta_{731}");
   g731->GetXaxis()->SetTitle(xAxisTitle);
   g731->GetXaxis()->CenterTitle();
   g731->GetYaxis()->SetRangeUser(min,max);
   g731->Draw(DrawOption);
   fT731->Draw("same");
   c3->Update();

   c3->cd(4); 
   g811->Draw(DrawOption);
   g811->SetTitle("#theta_{811}");
   g811->GetXaxis()->SetTitle(xAxisTitle);
   g811->GetXaxis()->CenterTitle();
   g811->GetYaxis()->SetRangeUser(min,max);
   g811->Draw(DrawOption);
   fT811->Draw("same");
   c3->Update();

   c3->cd(5); 
   g821->Draw(DrawOption);
   g821->SetTitle("#theta_{821}");
   g821->GetXaxis()->SetTitle(xAxisTitle);
   g821->GetXaxis()->CenterTitle();
   g821->GetYaxis()->SetRangeUser(min,max);
   g821->Draw(DrawOption);
   fT821->Draw("same");
   c3->Update();

   c3->cd(6); 
   g831->Draw(DrawOption);
   g831->SetTitle("#theta_{831}");
   g831->GetXaxis()->SetTitle(xAxisTitle);
   g831->GetXaxis()->CenterTitle();
   g831->GetYaxis()->SetRangeUser(min,max);
   g831->Draw(DrawOption);
   fT831->Draw("same");
   c3->Update();
   c3->SaveAs("ert_Tij1_c3.pdf");

   fDiffXSec4->GetPOLRAD()->Print();
   fDiffXSec4->GetPOLRAD()->GetKinematics()->Print();

   return(0);

}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]); 
	g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack); 

	return g;

}
//_______________________________________________________________
void ImportThData(int i,int j,int k,vector<double> &tau,vector<double> &th){

        double itau,ith;
        TString inpath = Form("./theta/proton/ert/ijk/th_%d_%d_%d.dat",i,j,k);

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
                        infile >> itau >> ith;
                                tau.push_back(itau);
                                th.push_back(ith);
		}
                infile.close();
                tau.pop_back();
                th.pop_back();
	}

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
	min -= 1E+2; 
 	// if(min<-1E+2){
	// 	min -= 1E+4;
	// }else{
	// 	min -= 1E+2; 
        // } 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	max += 1E+2; 
	// if(max>1E+2){
	// 	max += 1E+4;
	// }else{
	// 	max += 1E+2; 
        // } 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}

