Int_t dis_diff_xsec_nucleus(){

   Double_t beamEnergy =  5.89;
   Double_t theta      =  45.*degree;
   Double_t Eprime     =  3.0;

   // Parameters for the plot are (theta,phi)
   Int_t    npar     = 2;
   Double_t E_min    = 0.50;
   Double_t E_max    = 2.5;

   InSANENucleus::NucleusType Target = InSANENucleus::k3He; 

   /// F1F209 Unpolarized Born cross section 
   F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
   fDiffXSec->SetTargetType(Target);
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
   //ps->ListVariables();
   Double_t * energy_e =  ps->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e  =  ps->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e    =  ps->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e)  = Eprime;
   (*theta_e)   = theta;
   (*phi_e)     = 0.0*degree;
   fDiffXSec->EvaluateCurrent();
   //fDiffXSec->Print();
   //fDiffXSec->PrintTable();

   /// Unpolarized Born cross section
   InSANEPOLRADBornDiffXSec * fDiffXSec2 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec2->SetTargetType(Target); 
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->GetPOLRAD()->SetHelicity(0);       // try the Born case first...
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps2 = fDiffXSec2->GetPhaseSpace();
   //ps2->ListVariables();
   Double_t * energy_e2 =  ps2->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e2  =  ps2->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e2    =  ps2->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e2) = Eprime;
   (*theta_e2)  = theta;
   (*phi_e2)    = 0.0*degree;
   fDiffXSec2->EvaluateCurrent();
   //fDiffXSec2->Print();
   //fDiffXSec2->PrintTable();

   /// [Polarized] Born cross section
   InSANEPOLRADBornDiffXSec * fDiffXSec3 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec3->SetTargetType(Target); 
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps3 = fDiffXSec3->GetPhaseSpace();
   //ps3->ListVariables();
   Double_t * energy_e3 =  ps3->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e3  =  ps3->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e3    =  ps3->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e3) = Eprime;
   (*theta_e3)  = theta;
   (*phi_e3)    = 0.0*degree;
   fDiffXSec3->EvaluateCurrent();
   //fDiffXSec3->Print();
   //fDiffXSec3->PrintTable();

   /// [Polarized] Born cross section
   InSANEPOLRADBornDiffXSec * fDiffXSec4 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec4->SetTargetType(Target); 
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->GetPOLRAD()->SetHelicity(-1);       
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
   //ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4  =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4    =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4)  = theta;
   (*phi_e4)    = 0.0*degree;
   fDiffXSec4->EvaluateCurrent();
   //fDiffXSec4->Print();
   //fDiffXSec4->PrintTable();

   /// Elastic Radiative Tail
   InSANEPOLRADElasticTailDiffXSec * fDiffXSec5 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec5->SetTargetType(Target); 
   fDiffXSec5->SetBeamEnergy(beamEnergy);
   fDiffXSec5->InitializePhaseSpaceVariables();
   fDiffXSec5->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps5 = fDiffXSec5->GetPhaseSpace();
   //ps5->ListVariables();
   Double_t * energy_e5 =  ps5->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e5  =  ps5->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e5    =  ps5->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e5) = Eprime;
   (*theta_e5)  = theta;
   (*phi_e5)    = 0.0*degree;
   fDiffXSec5->EvaluateCurrent();
   //fDiffXSec5->Print();
   //fDiffXSec5->PrintTable();

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec6 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec6->SetTargetType(Target); 
   fDiffXSec6->SetBeamEnergy(beamEnergy);
   fDiffXSec6->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec6->InitializePhaseSpaceVariables();
   fDiffXSec6->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps6 = fDiffXSec6->GetPhaseSpace();
   //ps6->ListVariables();
   Double_t * energy_e6 =  ps6->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e6  =  ps6->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e6    =  ps6->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e6) = Eprime;
   (*theta_e6)  = theta;
   (*phi_e6)    = 0.0*degree;
   fDiffXSec6->EvaluateCurrent();
   //fDiffXSec6->Print();
   //fDiffXSec6->PrintTable();

   /// Quasi-Elastic Radiative Tail
   InSANEPOLRADQuasiElasticTailDiffXSec * fDiffXSec7 = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   fDiffXSec7->SetTargetType(Target); 
   fDiffXSec7->SetBeamEnergy(beamEnergy);
   fDiffXSec7->GetPOLRAD()->SetHelicity(0);
   fDiffXSec7->InitializePhaseSpaceVariables();
   fDiffXSec7->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps7 = fDiffXSec7->GetPhaseSpace();
   //ps7->ListVariables();
   Double_t * energy_e7 =  ps7->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e7  =  ps7->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e7    =  ps7->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e7) = Eprime;
   (*theta_e7)  = theta;
   (*phi_e7)    = 0.0*degree;
   fDiffXSec7->EvaluateCurrent();

   InSANEPOLRADQuasiElasticTailDiffXSec * fDiffXSec8 = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   fDiffXSec8->SetTargetType(Target); 
   fDiffXSec8->SetBeamEnergy(beamEnergy);
   fDiffXSec8->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec8->InitializePhaseSpaceVariables();
   fDiffXSec8->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps8 = fDiffXSec8->GetPhaseSpace();
   //ps8->ListVariables();
   Double_t * energy_e8 =  ps8->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e8  =  ps8->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e8    =  ps8->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e8) = Eprime;
   (*theta_e8)  = theta;
   (*phi_e8)    = 0.0*degree;
   fDiffXSec8->EvaluateCurrent();

   InSANEPOLRADQuasiElasticTailDiffXSec * fDiffXSecQ = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   fDiffXSecQ->SetTargetType(Target); 
   fDiffXSecQ->SetBeamEnergy(beamEnergy);
   fDiffXSecQ->GetPOLRAD()->SetHelicity(0);
   fDiffXSecQ->GetPOLRAD()->DoQEFullCalc(true); 
   fDiffXSecQ->InitializePhaseSpaceVariables();
   fDiffXSecQ->InitializeFinalStateParticles();
   InSANEPhaseSpace *psQ = fDiffXSecQ->GetPhaseSpace();
   //ps8->ListVariables();
   Double_t * energy_eQ =  psQ->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_eQ  =  psQ->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_eQ    =  psQ->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_eQ) = Eprime;
   (*theta_eQ)  = theta;
   (*phi_eQ)    = 0.0*degree;
   fDiffXSecQ->EvaluateCurrent();

   /// Inelastic Radiative Tail
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec9 = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec9->SetTargetType(Target); 
   fDiffXSec9->SetBeamEnergy(beamEnergy);
   fDiffXSec9->InitializePhaseSpaceVariables();
   fDiffXSec9->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps9 = fDiffXSec9->GetPhaseSpace();
   //ps9->ListVariables();
   Double_t * energy_e9 =  ps9->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e9  =  ps9->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e9    =  ps9->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e9) = Eprime;
   (*theta_e9)  = theta;
   (*phi_e9)    = 0.0*degree;
   fDiffXSec9->EvaluateCurrent();

   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec10 = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec10->SetTargetType(Target); 
   fDiffXSec10->SetBeamEnergy(beamEnergy);
   fDiffXSec10->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec10->InitializePhaseSpaceVariables();
   fDiffXSec10->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps10 = fDiffXSec10->GetPhaseSpace();
   //ps8->ListVariables();
   Double_t * energy_e10 =  ps10->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e10  =  ps10->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e10    =  ps10->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e10) = Eprime;
   (*theta_e10)  = theta;
   (*phi_e10)    = 0.0*degree;
   fDiffXSec10->EvaluateCurrent();

   //return(0);
   //-------------------------------------------
   TCanvas * c = new TCanvas("dis_diff_xsec_nu","polrad",800,600);

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,0.0);
   sigmaE->SetLineColor(kRed);

   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,0.0);
   sigmaE2->SetLineColor(kCyan);

   TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE3->SetParameter(0,theta);
   sigmaE3->SetParameter(1,0.0);
   sigmaE3->SetLineColor(kCyan+1);

   TF1 * sigmaE4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE4->SetParameter(0,theta);
   sigmaE4->SetParameter(1,0.0);
   sigmaE4->SetLineColor(kCyan-1);

   TF1 * sigmaE5 = new TF1("sigma5", fDiffXSec5, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max,  npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE5->SetParameter(0,theta);
   sigmaE5->SetParameter(1,0.0);
   sigmaE5->SetLineColor(kAzure);

   TF1 * sigmaE6 = new TF1("sigma6", fDiffXSec6, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE6->SetParameter(0,theta);
   sigmaE6->SetParameter(1,0.0);
   sigmaE6->SetLineColor(kAzure+4);

   TF1 * sigmaE7 = new TF1("sigma7", fDiffXSec7, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max,  npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE7->SetParameter(0,theta);
   sigmaE7->SetParameter(1,0.0);
   sigmaE7->SetLineColor(kMagenta);

   TF1 * sigmaE8 = new TF1("sigma8", fDiffXSec8, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE8->SetParameter(0,theta);
   sigmaE8->SetParameter(1,0.0);
   sigmaE8->SetLineColor(kMagenta+4);

   TF1 * sigmaEQ = new TF1("sigmaQ", fDiffXSecQ, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaEQ->SetParameter(0,theta);
   sigmaEQ->SetParameter(1,0.0);
   sigmaEQ->SetLineColor(kMagenta-9);

   TF1 * sigmaE9 = new TF1("sigma9", fDiffXSec9, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max,  npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE9->SetParameter(0,theta);
   sigmaE9->SetParameter(1,0.0);
   sigmaE9->SetNpx(20);
   sigmaE9->SetLineColor(kGreen);

   TF1 * sigmaE10 = new TF1("sigma10", fDiffXSec10, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               E_min, E_max, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE10->SetParameter(0,theta);
   sigmaE10->SetParameter(1,0.0);
   sigmaE10->SetLineColor(kGreen+4);

   //-------------------------------------------

   sigmaE->SetMinimum(1e-7);
   sigmaE->SetMaximum(1e3);

   gPad->SetLogy(true);
   sigmaE->Draw();
   sigmaE->GetXaxis()->SetTitle("E'");
   sigmaE->GetXaxis()->CenterTitle(true);
   sigmaE->GetYaxis()->SetTitle(fDiffXSec->GetLabel());
   sigmaE->GetYaxis()->CenterTitle(true);

   // sigmaE2->Draw("same");
   // sigmaE3->Draw("same");
   // sigmaE4->Draw("same");
   // sigmaE5->Draw("same");
   // sigmaE6->Draw("same");
   sigmaE7->Draw("same");
   // sigmaE8->Draw("same");
   sigmaEQ->Draw("same");
   // sigmaE9->Draw("same");
   // sigmaE10->Draw("same");

   TLegend *Leg = new TLegend(0.8,0.75,0.95,0.95);

   Leg->SetFillColor(kWhite);
   Leg->AddEntry(sigmaE  ,"F1F209 Born Unpol"        ,"l");
   // Leg->AddEntry(sigmaE2 ,"InSANE: POLRAD Born Unpol","l");
   // Leg->AddEntry(sigmaE3 ,"InSANE: POLRAD Born (+)"  ,"l");
   // Leg->AddEntry(sigmaE4 ,"InSANE: POLRAD Born (-)"  ,"l");
   // Leg->AddEntry(sigmaE5 ,"ERT (+)"                  ,"l");
   // Leg->AddEntry(sigmaE6 ,"ERT (-)"                  ,"l");
   Leg->AddEntry(sigmaE7 ,"QRT (smear)"                  ,"l");
   // Leg->AddEntry(sigmaE8 ,"QRT (-)"                  ,"l");
   Leg->AddEntry(sigmaEQ ,"QRT (full)"                  ,"l");
   // Leg->AddEntry(sigmaE9 ,"IRT (+)"                  ,"l");
   // Leg->AddEntry(sigmaE10,"IRT (-)"                  ,"l");

   Leg->Draw("same");
   // c->SaveAs("../results/dis_diff_xsec_nucleus.png");

   return(0);
}
