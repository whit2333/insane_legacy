Int_t elastic_inclusive_e(){

   Double_t beamEnergy = 4.7;

   InSANEeInclusiveElasticDiffXSec * fDiffXSec = new  InSANEeInclusiveElasticDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->InitializePhaseSpaceVariables();
   InSANEPhaseSpace *ps = fDiffXSec->GetPhaseSpace();
   ps->ListVariables();

   InSANEBeamTargetAsymmetry * asym = new InSANEBeamTargetAsymmetry();
   asym->SetBeamEnergy(beamEnergy);
   asym->SetThetaPhiTarget(180.0*TMath::Pi()/180.0,0.0);


   TCanvas * c = new TCanvas("ElasticCrossSection","Inclusive e- Elastic Cross Section");
   c->Divide(3,2);


   Int_t npar=2;
   Double_t Emin=0.5;
   Double_t Emax=2.0;
   Double_t minTheta=0.0175*15.0;
   Double_t maxTheta=0.0175*55.0;

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
               Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,30.0*TMath::Pi()/180.0);
   sigmaE->SetParameter(1,0.0);

   TF1 * Aenergy = new TF1("Aenergy", asym, &InSANEBeamTargetAsymmetry::EnergyDependentAsymmetry, 
               Emin, Emax, npar,"InSANEBeamTargetAsymmetry","EnergyDependentAsymmetry");
   Aenergy->SetParameter(0,30.0*TMath::Pi()/180.0);
   Aenergy->SetParameter(1,0.0);

   TF1 * sigmaTheta = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
               minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigmaTheta->SetParameter(0,1.0);
   sigmaTheta->SetParameter(1,0.0);

   TF1 * ATheta = new TF1("ATheta", asym, &InSANEBeamTargetAsymmetry::PolarAngleDependentAsymmetry, 
               minTheta, maxTheta, npar,"InSANEBeamTargetAsymmetry","PolarAngleDependentAsymmetry");
   ATheta->SetParameter(0,1.0);
   ATheta->SetParameter(1,0.0);

   c->cd(1);
   sigmaE->SetTitle("Inclusive e^{-} Elastic Cross section");
   sigmaE->GetXaxis()->SetTitle("E' [GeV]");
   sigmaE->GetYaxis()->SetTitle("mb/GeV/sr");
   sigmaE->Draw();

   c->cd(2);
   sigmaTheta->SetTitle("Inclusive e^{-} Elastic Cross section");
   sigmaTheta->GetXaxis()->SetTitle("#theta [rad]");
   sigmaTheta->GetYaxis()->SetTitle("mb/GeV/sr");
   sigmaTheta->Draw();

   c->cd(4);
   Aenergy->SetTitle("Beam target asymmetry");
   Aenergy->GetXaxis()->SetTitle("E' [GeV]");
   Aenergy->GetYaxis()->SetTitle("A");
   Aenergy->Draw();

   c->cd(5);
   ATheta->SetTitle("Beam target asymmetry");
   ATheta->GetXaxis()->SetTitle("#theta [rad]");
   ATheta->GetYaxis()->SetTitle("A");
   ATheta->Draw();

   c->cd(3);
   TH1 * h1E = sigmaE->GetHistogram();
   TH1 * h2E = Aenergy->GetHistogram();
   h1E->Multiply(h2E);
   h1E->SetTitle("Asymmetry weighted cross section");

   h1E->DrawCopy();

   c->cd(6);
   TH1 * h1Theta = sigmaTheta->GetHistogram();
   TH1 * h2Theta = ATheta->GetHistogram();
   h1Theta->Multiply(h2Theta);
   h1Theta->SetTitle("Asymmetry weighted cross section");
   h1Theta->DrawCopy();
//   InSANEPhaseSpaceSampler *  fExclusiveMottSampler = new InSANEPhaseSpaceSampler(fDiffXSec);

//    InSANEepElasticDiffXSec * fDiffXSec2 = new  InSANEepElasticDiffXSec();
//    fDiffXSec2->SetBeamEnergy(beamEnergy);
//    fDiffXSec2->InitializePhaseSpaceVariables();
//    InSANEPhaseSpace *ps2 = fDiffXSec2->GetPhaseSpace();
//    ps2->Print();
//    InSANEPhaseSpaceSampler *  fElasticSampler = new InSANEPhaseSpaceSampler(fDiffXSec2);
// 
// 
// //    fExclusiveMottSampler->Refresh();
// 
// 
// 
// 
//    TH2F * h1 = new TH2F("ExclusiveMott","ExclusiveMott Test",100,0.8,4.0,100,20,60);
//    TH2F * h2 = new TH2F("ExclusiveMott2","ExclusiveMott Test2",100,0.1,4.9,100,20,60);
// 
//    TH2F * h3 = new TH2F("ElasticXSec","ElasticXSec Test",100,0.5,4.0,100,20,60);
//    TH2F * h4 = new TH2F("ElasticXSec2","ElasticXSec2 Test2",100,0.1,2.5,100,20,60);
//    TH2F * hProtonPhiVsTheta = new TH2F("hProtonPhiVsTheta","Elastic Proton PhiVsTheta",100,20,60,100,100,260);
//    TH2F * hElectronPhiVsTheta = new TH2F("hElectronPhiVsTheta","Elastic Electron PhiVsTheta",100,20,60,100,-50,50);
// 
//    h1->GetXaxis()->SetTitle("Energy GeV");
//    h1->GetYaxis()->SetTitle("#theta_{e'}");
// 
//    Double_t *res;
//    for(int i = 0; i< 1000;i++) {
//     res = fExclusiveMottSampler->GenerateEvent();
// // //       std::cout << " F1F2 Xsec = " << fDiffXSec->EvaluateXSec(res) << " \n";
//     h1->Fill(res[0],res[1]*180.0/TMath::Pi());
//     h2->Fill(res[3],res[4]*180.0/TMath::Pi());
//     res = fElasticSampler->GenerateEvent();
// //       std::cout << " F1F2 Xsec = " << fDiffXSec->EvaluateXSec(res) << " \n";
//     h3->Fill(res[0],res[1]*180.0/TMath::Pi());
//     h4->Fill(res[3],res[4]*180.0/TMath::Pi());
//     hProtonPhiVsTheta->Fill(res[4]*180.0/TMath::Pi(),res[5]*180.0/TMath::Pi());
//     hElectronPhiVsTheta->Fill(res[1]*180.0/TMath::Pi(),res[2]*180.0/TMath::Pi());
//    }
// 
//    TCanvas * c = new TCanvas("CrossSections","Cross Sections");
//    c->Divide(3,2);
//    c->cd(1);
//    h1->Draw("COLZ");
//    c->cd(2);
//    h2->Draw("COLZ");
//    c->cd(3);
//    hElectronPhiVsTheta->Draw("COLZ");
//    c->cd(4);
//    h3->Draw("COLZ");
//    c->cd(5);
//    h4->Draw("COLZ");
//    c->cd(6);
//    hProtonPhiVsTheta->Draw("COLZ");

return(0);
}