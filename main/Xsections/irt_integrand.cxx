Int_t irt_integrand(){

   Double_t beamEnergy = 5.89;
   Double_t y          = 0.869; 
   Double_t Eprime     = beamEnergy*(1.- y);
   Double_t theta      = 45.0*degree;

   Double_t A = 1.; 
   Double_t Z = 1.; 

   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetTargetType(0);
   fDiffXSec4->SetA(A);
   fDiffXSec4->SetZ(Z);
   fDiffXSec4->GetPOLRAD()->SetHelicity(0);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
   ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4 =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4 =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4)  = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e4)    = 0.0*degree;
   fDiffXSec4->EvaluateCurrent();

   InSANEPOLRAD * polrad = fDiffXSec4->GetPOLRAD(); 
   Double_t min = polrad->GetKinematics()->GetTau_min();
   Double_t max = polrad->GetKinematics()->GetTau_max();

   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   vector<double> tauIRT,IRT; 
   ImportERTData(tauIRT,IRT); 

   TGraph *gIRT = GetTGraph(tauIRT,IRT);

   TF1 * irt_int = new TF1("irt_int", polrad, &InSANEPOLRAD::IRT_Tau_Integrand2, 
                           min,max, 0,"InSANEPOLRAD","IRT_Tau_Integrand2");
   irt_int->SetNpx(500);
   irt_int->SetLineColor(kMagenta);
   gIRT->Draw("pla");
   gIRT->SetTitle("IRT #tau integrand - LEGENDRE 10");
   gIRT->GetXaxis()->SetTitle("#tau");
   gIRT->GetYaxis()->SetRangeUser(-10,100);
   irt_int->Draw("same");
    
   return(0);
}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

        const int N = x.size();
        TGraph *g = new TGraph(N,&x[0],&y[0]);
        g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack);

        return g;

}
//_______________________________________________________________
void ImportERTData(vector<double> &tau,vector<double> &ert){

        double itau,iy1;
        TString inpath = Form("./dump/dump_irt_int.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
			infile >> itau >> iy1;
			tau.push_back(itau);
			ert.push_back(iy1);
                }
                infile.close();
                tau.pop_back();
                ert.pop_back();
        }

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
 	if(min<-1E+2){
		min -= 1E+2;
	}else{
		min -= 1E+1; 
        } 

	return min; 
} 
//_______________________________________________________________
