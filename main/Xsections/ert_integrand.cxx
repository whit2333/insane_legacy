Int_t ert_integrand(){

   Double_t beamEnergy  = 20;
   Double_t theta       = 5.0*degree;//29.0*TMath::Pi()/180.0;
   Double_t y           = 0.869; 
   Double_t Eprime      = 4.0;//beamEnergy*(1.-y);

   InSANEPOLRADElasticTailDiffXSec * fDiffXSec4= new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->GetPOLRAD()->SetTargetType(0);
   fDiffXSec4->GetPOLRAD()->SetHelicity(0);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   InSANEPhaseSpace *ps4 = fDiffXSec4->GetPhaseSpace();
   //ps4->ListVariables();
   Double_t * energy_e4 =  ps4->GetVariable("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e4 =  ps4->GetVariable("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e4 =  ps4->GetVariable("phi_e")->GetCurrentValueAddress();
   (*energy_e4) = Eprime;
   (*theta_e4) = theta;//45.0*TMath::Pi()/180.0;
   (*phi_e4) = 0.0*TMath::Pi()/180.0;
   fDiffXSec4->EvaluateCurrent();
   //fDiffXSec4->Print();
   //fDiffXSec4->PrintTable();

   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 4.5;
   Double_t minTheta = 0.0175*15.0;
   Double_t maxTheta = 0.0175*55.0;

   InSANEPOLRAD * polrad = fDiffXSec4->GetPOLRAD();
   Double_t tau_min = polrad->GetKinematics()->GetTau_A_min();
   Double_t tau_max = polrad->GetKinematics()->GetTau_A_max();

   Double_t xbj = polrad->GetKinematics()->Getx();
   Double_t Q2  = polrad->GetKinematics()->GetQ2();
   Double_t S_xA   = polrad->GetKinematics()->GetS_xA();
   Double_t Mp  = polrad->GetKinematics()->GetM();
   Double_t W2  = polrad->GetKinematics()->GetW2();
   Double_t nu  = polrad->GetKinematics()->Gety()*beamEnergy;
   Double_t tau_me_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_e/GeV,2.0)-Q2)-1.0);
   Double_t tau_mp_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(Mp,2.0)-Q2)-1.0);
   Double_t tau_mdelta_peak = 1.0/((W2-Mp*Mp)/(TMath::Power(M_Delta/GeV,2.0)-Q2)-1.0);
   Double_t tau_u = 0.67;
   Double_t t = tau_u/(1.0+tau_u)*(W2-Mp*Mp) + Q2;
   Double_t Q2_over_S_xA = Q2/S_xA;
   //polrad->GetKinematics()->Print();
   std::cout << "tau_min = " << tau_min << "\n";
   std::cout << "tau_max = " << tau_max << "\n";
   std::cout << "Q2 = " << Q2 << "\n";
   std::cout << "Q2/Sx = " << Q2_over_S_xA << "\n";
   std::cout << "W2 = " << W2 << "\n";
   std::cout << "Mp = " << Mp << "\n";
   std::cout << "xbj = " << xbj << "\n";
   std::cout << "tau_me_peak = " << tau_me_peak << "\n";
   std::cout << "tau_mp_peak = " << tau_mp_peak << "\n";
   std::cout << "tau_mdelta_peak = " << tau_mdelta_peak << "\n";
   std::cout << "t = " << t << "\n";

   vector<double> tauERT,ERT; 
   ImportERTData(tauERT,ERT); 

   TGraph *gERT = GetTGraph(tauERT,ERT);

   TF1 * fERT = new TF1("fERT", polrad, &InSANEPOLRAD::ERT_Tau_Integrand2, 
               tau_min, tau_max, 0,"InSANEPOLRAD","ERT_Tau_Integrand2");
   fERT->SetNpx(5000);
   fERT->SetLineColor(kMagenta);
   fDiffXSec4->EvaluateCurrent();
   // fERT->DrawCopy();

   // TF1 * fERT_2 = new TF1("fERT_2", polrad, &InSANEPOLRAD::ERT_Tau_Integrand6, 
   //             tau_min, tau_max, 0,"InSANEPOLRAD","ERT_Tau_Integrand6");
   // fERT_2->SetNpx(5000);
   // fERT_2->SetLineColor(4);
   // fDiffXSec4->EvaluateCurrent();
   // fERT_2->DrawCopy("same");

   TCanvas * c = new TCanvas("ert_integrand","ert_integrand");
   c->SetFillColor(kWhite); 

   Double_t min = GetMin(ERT);
   Double_t max = GetMax(ERT);

   TString DrawOption = Form("AP"); 

   c->cd(); 
   //fERT->DrawCopy();
   //gERT->Draw(DrawOption);
   //gERT->SetTitle("ERT Integrand");
   //gERT->GetXaxis()->SetTitle("#tau");
   //gERT->GetXaxis()->CenterTitle();
   //gERT->GetYaxis()->SetRangeUser(min,max);
   //gERT->Draw(DrawOption);
   //fERT->Draw("same");
   c->Update();
 

   return(0);
}
//_______________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

        const int N = x.size();
        TGraph *g = new TGraph(N,&x[0],&y[0]);
        g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlack);

        return g;

}
//_______________________________________________________________
void ImportERTData(vector<double> &tau,vector<double> &ert){

        double itau,iy1;
        TString inpath = Form("./dump/dump_ert_int.dat");

        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                while(!infile.eof()){
			infile >> itau >> iy1;
			tau.push_back(itau);
			ert.push_back(iy1);
                }
                infile.close();
                tau.pop_back();
                ert.pop_back();
        }

}
//_______________________________________________________________
double GetMin(vector<double> v){

	// double min = 1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] < min) min = v[i];  
	// }

        double min = (-1.)*GetSecondPeak(v);
 	if(min<-1E+2){
		min -= 1E+2;
	}else{
		min -= 1E+1; 
        } 

	return min; 
} 
//_______________________________________________________________
double GetMax(vector<double> v){

	// double max = -1E+10; 
	// int N = v.size();
	// for(int i=0;i<N;i++){
	// 	if(v[i] > max) max = v[i];  
	// }

	double max = GetSecondPeak(v); 
	if(max>1E+2){
		max += 1E+2;
	}else{
		max += 1E+1; 
        } 

	return max; 

}
//_______________________________________________________________
double GetSecondPeak(vector<double> v){

	// get value of the *second* highest peak 
  
        // first we get the highest peak 
        double abs_v=0; 
	double max_peak=0; 
	int N = v.size();
	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]); 
		if( abs_v > max_peak) max_peak = abs_v;
	}

        // now get the second highest peak 
	double sec_peak=0;
 	for(int i=0;i<N;i++){
		abs_v = TMath::Abs(v[i]);
		if(i==0){
			if( (abs_v>0)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}else{
			if( (abs_v>sec_peak)&&(abs_v<max_peak) ) sec_peak = abs_v;
		}
	}

	return sec_peak;

}

