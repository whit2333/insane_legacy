Int_t sigma_LT(){

   Int_t TargetType    = 0;
   Int_t Z             = 1;
   Int_t A             = 1;
   Double_t beamEnergy = 3.118;
   Double_t Eprime     = 1.41;
   Double_t theta      = 12.05*degree;
   Double_t phi        = 0*degree;

   Int_t    npar       = 2;
   Double_t Emin       = 0.3;
   Double_t Emax       = 5.1;
   Double_t minTheta   = 15.0*degree;
   Double_t maxTheta   = 55.0*degree;

   TLegend * leg = new TLegend(0.1,0.7,0.3,0.9);
   TMultiGraph * mg = new TMultiGraph();

   //------------------
   InSANEStructureFunctionsFromPDFs * SFs = new InSANEStructureFunctionsFromPDFs();
   SFs->SetUnpolarizedPDFs(new BBSUnpolarizedPDFs());
   //SFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());

   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs(new BBSPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new DSSVPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new BBPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new DNS2005PolarizedPDFs());

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();
   //fm->SetPolarizedStructureFunctions(pSFs);
   //fm->SetStructureFunctions(SFs);

   //------------------
   // NucDB data from E94110
   NucDBManager * ndb = NucDBManager::GetManager();
   NucDBExperiment * e94110 = ndb->GetExperiment("JLAB-E94110");
   if(!e94110) return( -1);
   NucDBMeasurement * sigma = e94110->GetMeasurement("sigma");
   if(!sigma) return(-2);
   std::vector<Double_t> beam_energies;
   sigma->GetUniqueBinnedVariableValues("E",&beam_energies);
   std::cout << "E94110 beam energy variance = " << sigma->GetBinnedVariableVariance("E") << "\n";

   NucDBExperiment * e99118 = ndb->GetExperiment("JLAB-E99118");
   if(!e99118) return( -3);
   NucDBMeasurement * sigma_2 = e99118->GetMeasurement("sigma");
   if(!sigma_2) return(-4);
   sigma_2->Print();

   NucDBExperiment * e00002 = ndb->GetExperiment("JLAB-E00002");
   if(!e00002) return( -3);
   NucDBMeasurement * sigma_3 = e00002->GetMeasurement("sigma");
   if(!sigma_3) return(-4);

   // Create a bins to filter the data
   NucDBBinnedVariable * EbeamBin = new NucDBBinnedVariable("E", "E 5.3-5.9 GeV");
   EbeamBin->SetBinMinimum(2.5);
   EbeamBin->SetBinMaximum(3.9);
   NucDBBinnedVariable * ThetaBin = new NucDBBinnedVariable("theta", "0-20 deg");
   ThetaBin->SetBinMinimum(10.0);
   ThetaBin->SetBinMaximum(13.0);

   //--------------------------------
   // Create a new measurement
   // E94110
   NucDBMeasurement * sigma01 = sigma->CreateMeasurementFilteredWithBin(ThetaBin);
   NucDBMeasurement * sigma1 = new NucDBMeasurement("sigma1", Form("#sigma %s", EbeamBin->GetTitle()));
   sigma1->AddDataPoints(sigma01->FilterWithBin(EbeamBin) );
   //Double_t beamEnergy1 = sigma1->GetBinnedVariableMean("E");
   //std::cout << "beamEnergy1 = " << beamEnergy1 << "\n";
   TGraphErrors * gr1 = sigma1->BuildGraph("Eprime");
   gr1->SetLineColor(4009); 
   gr1->SetMarkerColor(4009);
   mg->Add(gr1,"ep");
   leg->AddEntry(gr1,"E94110","ep");
   std::vector<Double_t> beam_energies1;
   std::vector<Double_t> theta_settings1;
   sigma1->GetUniqueBinnedVariableValues("theta",&theta_settings1);
   sigma1->GetUniqueBinnedVariableValues("E",&beam_energies1);

   NucDBMeasurement * sigma02 = sigma_2->CreateMeasurementFilteredWithBin(ThetaBin);
   NucDBMeasurement * sigma2 = new NucDBMeasurement("sigma2", Form("#sigma %s", EbeamBin->GetTitle()));
   sigma2->AddDataPoints(sigma02->FilterWithBin(EbeamBin) );
   //std::cout << "beamEnergy2 = " <<  sigma2->GetBinnedVariableMean("E") << "\n";
   TGraphErrors * gr2 = sigma2->BuildGraph("Eprime");
   gr2->SetLineColor(4008); 
   gr2->SetMarkerColor(4008);
   mg->Add(gr2,"ep");
   leg->AddEntry(gr2,"E99118","ep");
   std::vector<Double_t> beam_energies2;
   std::vector<Double_t> theta_settings2;
   sigma2->GetUniqueBinnedVariableValues("theta",&theta_settings2);
   sigma2->GetUniqueBinnedVariableValues("E",&beam_energies2);
   
   NucDBMeasurement * sigma03 = sigma_3->CreateMeasurementFilteredWithBin(ThetaBin);
   NucDBMeasurement * sigma3 = sigma03->CreateMeasurementFilteredWithBin(EbeamBin);
   //new NucDBMeasurement("sigma3", Form("#sigma %s", EbeamBin->GetTitle()));
   //sigma3->AddDataPoints(sigma_3->FilterWithBin(EbeamBin) );
   //std::cout << "beamEnergy3 = " <<  sigma3->GetBinnedVariableMean("E") << "\n";
   //std::cout << "theta3 = " <<  sigma3->GetBinnedVariableMean("theta") << "\n";
   TGraphErrors * gr3 = sigma3->BuildGraph("Eprime");
   gr3->SetLineColor(4010); 
   gr3->SetMarkerColor(4010);
   mg->Add(gr3,"ep");
   leg->AddEntry(gr3,"E00002","ep");
   std::vector<Double_t> beam_energies3;
   std::vector<Double_t> theta_settings3;
   sigma3->GetUniqueBinnedVariableValues("theta",&theta_settings3);
   sigma3->GetUniqueBinnedVariableValues("E",&beam_energies3);



   TCanvas * c = new TCanvas("compare","compare");
   //c->SetLogy(true);
   //mg->Draw("a");
   //gr2->Draw("AP");
   //gr1->Draw("P");
   //gr3->Draw("P");

   // ------------------------------
   // Cross section models
   /// DIS Born cross section 
   InSANEPOLRADBornDiffXSec * fDiffXSec1 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetA(A);
   fDiffXSec1->SetZ(Z);
   fDiffXSec1->SetTargetType(TargetType);
   fDiffXSec1->GetPOLRAD()->SetHelicity(0);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();

   // MAID
   TString pion               = "pi0";
   TString nucleon            = "p";
   TString target             = "p";
   MAIDInclusiveDiffXSec *fDiffXSec2 = new MAIDInclusiveDiffXSec(pion,nucleon);
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetA(A);
   fDiffXSec2->SetZ(Z);
   fDiffXSec2->SetTargetType(TargetType);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();

   // MAID  
   pion               = "piplus";
   nucleon            = "n";
   target             = "p";
   MAIDInclusiveDiffXSec *fDiffXSec21 = new MAIDInclusiveDiffXSec(pion,nucleon);
   fDiffXSec21->SetBeamEnergy(beamEnergy);
   fDiffXSec21->SetA(A);
   fDiffXSec21->SetZ(Z);
   fDiffXSec21->SetTargetType(TargetType);
   fDiffXSec21->InitializePhaseSpaceVariables();
   fDiffXSec21->InitializeFinalStateParticles();
   fDiffXSec21->Refresh();
  
   // MAID e p ->e pi+ n -> e X
   pion               = "piplus";
   nucleon            = "p";
   target             = "p";
   MAIDInclusiveElectronDiffXSec *fDiffXSec3 = new MAIDInclusiveElectronDiffXSec(nucleon);
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   fDiffXSec3->SetA(A);
   fDiffXSec3->SetZ(Z);
   fDiffXSec3->SetTargetType(TargetType);

   // class needs to implement these:
   fDiffXSec3->fXSec0->SetBeamEnergy(beamEnergy);
   fDiffXSec3->fXSec0->SetA(A);
   fDiffXSec3->fXSec0->SetZ(Z);
   fDiffXSec3->fXSec0->SetTargetType(TargetType);

   fDiffXSec3->fXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec3->fXSec1->SetA(A);
   fDiffXSec3->fXSec1->SetZ(Z);
   fDiffXSec3->fXSec1->SetTargetType(TargetType);

   fDiffXSec3->fXSec0->InitializePhaseSpaceVariables();
   fDiffXSec3->fXSec1->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializePhaseSpaceVariables();


   fDiffXSec3->fXSec0->InitializeFinalStateParticles();
   fDiffXSec3->fXSec1->InitializeFinalStateParticles();
   fDiffXSec3->InitializeFinalStateParticles();
   fDiffXSec3->fXSec1->Refresh();
   fDiffXSec3->fXSec0->Refresh();
   fDiffXSec3->Refresh();


   //----------------------------
   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE1->SetParameter(0,theta);
   sigmaE1->SetParameter(1,phi);
   sigmaE1->SetLineColor(kCyan);
   //sigmaE1->SetNpx(250);

   //sigmaE1->SetLineColor(kCyan-4);
   //sigmaE1->SetParameter(0,theta2);
   //sigmaE1->DrawCopy("same");

   //sigmaE1->SetLineColor(kCyan-9);
   //sigmaE1->SetParameter(0,theta3);

   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetLineColor(kBlue);
   sigmaE2->SetNpx(100);
   
   TF1 * sigmaE21 = new TF1("sigma21", fDiffXSec21, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE21->SetParameter(0,theta);
   sigmaE21->SetParameter(1,phi);
   sigmaE21->SetLineStyle(2);
   sigmaE21->SetLineColor(kBlue);
   sigmaE21->SetNpx(100);

   //sigmaE2->SetLineColor(kBlue-4);
   //sigmaE2->SetParameter(0,theta);
   //sigmaE2->DrawCopy("same");

   //sigmaE2->SetLineColor(kBlue-9);
   //sigmaE2->SetParameter(0,theta3);

   TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE3->SetParameter(0,theta);
   sigmaE3->SetParameter(1,phi);
   sigmaE3->SetLineColor(kRed);
   sigmaE3->SetNpx(200);

   //sigmaE3->SetLineColor(kRed-4);
   //sigmaE3->SetParameter(0,theta);
   //sigmaE3->DrawCopy("same");

   //-------------------
   
   mg->Draw("a");
   sigmaE1->DrawCopy("same");
   sigmaE2->DrawCopy("same");
   sigmaE21->DrawCopy("same");
   sigmaE3->DrawCopy("same");

   leg->Draw();

   return(0);
}
