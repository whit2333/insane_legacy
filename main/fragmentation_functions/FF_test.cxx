Int_t FF_test(){

   const int N = 50;
   DSSFragmentationFunctions * ff = new DSSFragmentationFunctions();

   TGraph * gr0 = new TGraph(N);
   gr0->SetLineColor(1);
   TGraph * gr1 = new TGraph(N);
   gr1->SetLineColor(2);
   TGraph * gr2 = new TGraph(N);
   gr2->SetLineColor(4);

   double dz = 1.0/double(N);
   for(int i = 0; i<N; i++) {
      double z = (double(i)+0.5)*dz;
      gr0->SetPoint(i,z,z* ff->D_u_piplus(z,1.0));
      gr1->SetPoint(i,z,z* ff->D_dbar_piplus(z,1.0));
      gr2->SetPoint(i,z,z* ff->D_ubar_piplus(z,1.0));
   }


   TMultiGraph * mg = new TMultiGraph();
   mg->Add(gr0,"l");
   mg->Add(gr1,"l");
   mg->Add(gr2,"l");

   TLegend * leg = new TLegend(0.6,0.7,0.85,0.85);
   leg->AddEntry(gr0,"zD_{u}","l");
   leg->AddEntry(gr1,"zD_{#bar{d}}","l");
   leg->AddEntry(gr2,"zD_{#bar{u}}","l");

   TCanvas * c = new TCanvas();
   mg->Draw("a");
   leg->Draw();

   c->SaveAs("results/fragmentation_functions/FF_test.png");

   return 0;
}
