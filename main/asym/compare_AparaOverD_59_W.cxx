Int_t compare_AparaOverD_59_W(Int_t paraRunGroup = 23,Int_t perpRunGroup = 23,Int_t aNumber=23){

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/binned_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/binned_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);
   //fPerpAsymmetries->Print();

   std::cout << "DONE" << std::endl;
   TFile * fout = new TFile(Form("data/AparaOverD_59_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fAparaOverDAsymmetries = new TList();
   TList * fA2Asymmetries = new TList();

   TH1F * fAparaOverD = 0;
   TH1F * fA2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TCanvas * c = new TCanvas("cAparaOverD","Apara Over D");
   //c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();

      TH1F * fApara = paraAsym->fAsymmetryVsW;
      TH1F * fAperp = perpAsym->fAsymmetryVsW;

      InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsW; 

      fAparaOverD = new TH1F(*fApara);
      fA2 = new TH1F(*fAperp);

      fAparaOverD->SetNameTitle(Form("fAparaOverD-%d",jj),Form("Apara/D-%d",jj));
      fA2->SetNameTitle(Form("fA2-%d",jj),Form("A2-%d",jj));

      fAparaOverDAsymmetries->Add(fAparaOverD);
      fA2Asymmetries->Add(fA2);

      Int_t   xBinmax = fAparaOverD->GetNbinsX();
      Int_t   yBinmax = fAparaOverD->GetNbinsY();
      Int_t   zBinmax = fAparaOverD->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax; i++){
         for(Int_t j=0; j<= yBinmax; j++){
            for(Int_t k=0; k<= zBinmax; k++){

               bin   = fAparaOverD->GetBin(i,j,k);
               fAparaOverD->GetBinXYZ(bin,binx,biny,binz);

               Double_t W1     = paraKine->fW->GetBinContent(bin);
               Double_t x1     = paraKine->fx->GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi->GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2->GetBinContent(bin);
               Double_t W2     = perpKine->fW->GetBinContent(bin);
               Double_t x2     = perpKine->fx->GetBinContent(bin);
               Double_t phi2   = perpKine->fPhi->GetBinContent(bin);
               Double_t Q22    = perpKine->fQ2->GetBinContent(bin);
               Double_t W      = (W1+W2)/2.0;
               Double_t x      = (x1+x2)/2.0;
               Double_t phi    = (phi1+phi2)/2.0;
               Double_t Q2     = (Q21+Q22)/2.0;

               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               Double_t A180  = fApara->GetBinContent(bin);
               Double_t A80   = fAperp->GetBinContent(bin);

               Double_t eA180  = fApara->GetBinError(bin);
               Double_t eA80   = fAperp->GetBinError(bin);

               Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               Double_t R        = SFs->R(x,Q2);
               Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               Double_t cota     = 1.0/TMath::Tan(alpha);
               Double_t csca     = 1.0/TMath::Sin(alpha);

               Double_t c0 = 1.0/(1.0 + eta*xi);
               Double_t c11 = (1.0 + cota*chi)/D ;
               Double_t c12 = (csca*chi)/D ;
               Double_t c21 = (xi - cota*chi/eta)/D ;
               Double_t c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               Double_t A1 = A180*(1/D);
               //Double_t A1 = c0*(c11*A180 + c12*A80);
               Double_t A2 = 0.0;//c0*(c21*A180 + c22*A80);

               Double_t eA1 = eA180/D;
               //Double_t eA1 = TMath::Sqrt( TMath::Power(c0*c11*eA180,2.0) + TMath::Power(c0*c12*eA80,2.0));
               Double_t eA2 = 0.0;//TMath::Sqrt( TMath::Power(c0*c21*eA180,2.0) + TMath::Power(c0*c22*eA80,2.0));
               //eA2 = c0*(c21*eA180 + c22*eA80);
               //A1 = c0*(A180/D - eta*A80/d);
               //A2 = c0*(xi*A180/D + A80/d);

               if ( A1 != A1 ) A1 = 0;
               if ( A2 != A2 ) A2 = 0;

               //             std::cout << "theta = " << theta << "\n";
               //std::cout << "A1 = " << A1 << "\n";
               //std::cout << "A2 = " << A2 << "\n";
               //             
               fAparaOverD->SetBinContent(bin,A1);
               fA2->SetBinContent(bin,A2);
               fAparaOverD->SetBinError(bin,eA1);
               fA2->SetBinError(bin,eA2);

            }
         }
      }

      TH1 * h1 = fAparaOverD; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      TH1 * h2 = fA2; 
      gr2 = new TGraphErrors(h2);
      for( int j = gr2->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr2->RemovePoint(j);
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fAparaOverD->GetMarkerColor());
      gr->SetLineColor(fAparaOverD->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
      }
      if(jj!=4)mg->Add(gr,"p");
      leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");

      gr2->SetMarkerStyle(21);
      gr2->SetMarkerColor(fAparaOverD->GetMarkerColor());
      gr2->SetLineColor(fAparaOverD->GetMarkerColor());
      if(jj==4){
         gr2->SetMarkerColor(1);
         gr2->SetMarkerStyle(24);
         gr2->SetLineColor(1);
      }
      if(jj!=4)mg2->Add(gr2,"p");

   }

   Double_t Q2 = 4.0;

   fout->WriteObject(fAparaOverDAsymmetries,Form("AparaOverD-59-%d",paraRunGroup));//,TObject::kSingleKey); 

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);

   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  
   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin =  0.0;
   Double_t ymax =  1.0; 

   // --------------- A1 ----------------------
   //c->cd(1);
   //c->cd(0);
   TF2 * xf = new TF2("xf","-1.0*y",0,1,-1,1);

   TString Title;
   Int_t width = 1;

   // Load in world data on A1He3 from NucDB  
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   TMultiGraph *MG = new TMultiGraph(); 
   TList * measurementsList = manager->GetMeasurements("AparapOverD");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   // Q2 bin for filtering data
   NucDBBinnedVariable Q2Filter("Qsquared","Q^{2}");
   Q2Filter.SetBinMinimum(1.0);
   Q2Filter.SetBinMaximum(6.5);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1World = (NucDBMeasurement*)measurementsList->At(i);
      A1World->ApplyFilterWithBin(&Q2Filter);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1World->BuildGraph("W");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A1World->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
   }
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("A_{1}^{p}+#etaA_{2}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("W");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(1.0,3.0); 
   MG->GetYaxis()->SetRangeUser(-0.2,1.0); 
   MG->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   //c->cd(2);
   //TMultiGraph *MG2 = new TMultiGraph(); 
   //measurementsList = manager->GetMeasurements("A2p");
   //measurementsList->Print();
   //for (int i = 0; i < measurementsList->GetEntries(); i++) {
   //   NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
   //   A2pMeas->ApplyFilterWithBin(&Q2Filter);
   //   // Get TGraphErrors object 
   //   TGraphErrors *graph        = A2pMeas->BuildGraph("W");
   //   graph->SetMarkerStyle(27);
   //   // Set legend title 
   //   Title = Form("%s",A2pMeas->GetExperimentName()); 
   //   //leg->AddEntry(graph,Title,"lp");
   //   // Add to TMultiGraph 
   //   MG2->Add(graph,"p"); 
   //}
   //MG2->Add(mg2,"p");
   //MG2->SetTitle("Preliminary A_{2}^{p}, E=5.9 GeV");
   //MG2->Draw("a");
   //MG2->GetXaxis()->SetLimits(0.0,1.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.5,0.5); 
   //MG2->GetYaxis()->SetTitle("A_{2}^{p}");
   //MG2->GetXaxis()->SetTitle("W");
   //MG2->GetXaxis()->CenterTitle();
   //MG2->GetYaxis()->CenterTitle();
   //c->Update();

   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_W_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_W_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_W_%d.eps",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_W_%d.svg",aNumber));

   //fout->WriteObject(fA1Asymmetries,Form("A1-59-%d",0));//,TObject::kSingleKey);
   //fout->WriteObject(fA2Asymmetries,Form("A2-59-%d",0));//,TObject::kSingleKey);
   //fout->WriteObject(fParaAsymmetries,Form("ParaAsym-59-%d",0));
   //fout->WriteObject(fPerpAsymmetries,Form("PerpAsym-59-%d",0));

   return(0);
}
