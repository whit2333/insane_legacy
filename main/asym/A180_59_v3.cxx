#include "util/stat_error_graph.cxx"

Int_t A180_59_v3(Int_t paraRunGroup = 4306, Int_t aNumber=4306)
{

   Int_t paraFileVersion = paraRunGroup;

   Double_t E0    = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager    * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs  = fman->GetStructureFunctions();

   const char * parafile = Form("data/binned_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);


   TList * SystHists1 = new TList();

   TMultiGraph * mg = new TMultiGraph();

   TLegend *leg = new TLegend(0.17, 0.70, 0.40, 0.88);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   std::cout << " DERP \n";
   fParaAsymmetries->Print();

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetMeasuredAsymmetryResult();

      TH1F * fApara = &paraAsym->fAsymmetryVsx;

      InSANEAveragedKinematics1D * paraKine = &paraAsym->fAvgKineVsx;

      paraAsym->fSystErrVsx.SetLineColor(fApara->GetLineColor());

      SystHists1->Add(&paraAsym->fSystErrVsx);

      // ------------------------------------
      // A180
      TGraphErrors * gr = new TGraphErrors(fApara);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = paraAsym->fSystErrVsx.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         Double_t eyt_sys = paraAsym->fSystErrVsx.GetBinContent(abin);
         if( yt == 0.0 || eyt_sys>1.0 || eyt>1.0 ) {
            gr->RemovePoint(j);
            paraAsym->fSystErrVsx.SetBinContent(abin,0.0);
            paraAsym->fSystErrVsx.SetBinError(abin,0.0);
            continue;
         }
      }
      if(jj==4) {
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
      }
      if(jj<4) {
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("#LTQ^{2}#GT=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }
       
   }

   // -----------------------------------------------------------------
   // A180

   TCanvas * c = new TCanvas();
   TMultiGraph *MG = new TMultiGraph(); 

   MG->Add(mg);
   MG->Draw("AP");
   MG->SetTitle("");//A_{180}^{p},  E=5.9 GeV");
   MG->GetXaxis()->SetTitle("x");
   MG->GetXaxis()->CenterTitle(true);
   MG->GetYaxis()->SetTitle("");
   MG->GetYaxis()->CenterTitle(true);
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.5,1.19); 
   MG->Draw("AP");

   TMultiGraph * mg_syst_err = new TMultiGraph();
   for(int i=0;i<SystHists1->GetEntries()&&i<4;i++){
      TH1 * hsys2 = (TH1*)SystHists1->At(i);
      mg_syst_err->Add(stat_error_graph(hsys2,-0.4),"e3");
   }
   MG->Add(mg_syst_err);

   TLatex l;
   l.SetTextSize(0.05);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(-15);
   //l.DrawLatex(0.05,0.2,"systematic errors #rightarrow");

   leg->Draw();

   c->SaveAs(Form("results/asymmetries/A180_59_v3_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A180_59_v3_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/A180_59_v3_%d.svg",aNumber));


   return(0);
}
