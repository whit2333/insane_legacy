#include "util/stat_error_graph.cxx"

Int_t A80_59_v3(Int_t perpRunGroup = 4306, Int_t aNumber=4306)
{

   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager    * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs  = fman->GetStructureFunctions();

   const char * perpfile = Form("data/binned_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f1 = TFile::Open(perpfile,"UPDATE");
   f1->cd();
   f1->ls();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-1);


   TList * SystHists1 = new TList();

   TMultiGraph * mg = new TMultiGraph();

   TLegend *leg = new TLegend(0.17, 0.70, 0.40, 0.88);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   std::cout << " DERP \n";
   fPerpAsymmetries->Print();

   // Loop over asymmetries Q2 bins
   for(int jj = 0;jj<fPerpAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetMeasuredAsymmetryResult();

      TH1F * fAperp = &perpAsym->fAsymmetryVsx;

      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsx;

      perpAsym->fSystErrVsx.SetLineColor(fAperp->GetLineColor());

      SystHists1->Add(&perpAsym->fSystErrVsx);

      // ------------------------------------
      // A80
      TGraphErrors * gr = new TGraphErrors(fAperp);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = perpAsym->fSystErrVsx.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         Double_t eyt_sys = perpAsym->fSystErrVsx.GetBinContent(abin);
         if( yt == 0.0 || eyt_sys>2.0 || eyt>2.0 ) {
            gr->RemovePoint(j);
            perpAsym->fSystErrVsx.SetBinContent(abin,0.0);
            perpAsym->fSystErrVsx.SetBinError(abin,0.0);
            continue;
         }
      }
      if(jj==4) {
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
      }
      if(jj<4) {
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("#LTQ^{2}#GT=%.1f GeV^{2}",perpAsym->GetQ2()),"lp");
      }
       
   }

   // -----------------------------------------------------------------
   // A80

   TCanvas * c = new TCanvas();
   TMultiGraph *MG = new TMultiGraph(); 

   MG->Add(mg);
   MG->Draw("AP");
   MG->SetTitle("");//A_{80}^{p},  E=5.9 GeV");
   MG->GetXaxis()->SetTitle("x");
   MG->GetXaxis()->CenterTitle(true);
   MG->GetYaxis()->SetTitle("");
   MG->GetYaxis()->CenterTitle(true);
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.5,1.19); 
   MG->Draw("AP");

   TMultiGraph * mg_syst_err = new TMultiGraph();
   for(int i=0;i<SystHists1->GetEntries()&&i<4;i++){
      TH1 * hsys2 = (TH1*)SystHists1->At(i);
      mg_syst_err->Add(stat_error_graph(hsys2,-0.4),"e3");
   }
   MG->Add(mg_syst_err);

   TLatex l;
   l.SetTextSize(0.05);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(-15);
   //l.DrawLatex(0.05,0.2,"systematic errors #rightarrow");

   leg->Draw();

   c->SaveAs(Form("results/asymmetries/A80_59_v3_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A80_59_v3_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/A80_59_v3_%d.svg",aNumber));


   return(0);
}
