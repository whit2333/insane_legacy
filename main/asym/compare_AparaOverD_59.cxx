Int_t compare_AparaOverD_59(Int_t paraRunGroup = 1108,Int_t perpRunGroup = 1108,Int_t aNumber=1108){

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/binned_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/binned_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);
   //fPerpAsymmetries->Print();

   std::cout << "DONE" << std::endl;
   TFile * fout = new TFile(Form("data/AparaOverD_59_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fAparaOverDAsymmetries = new TList();
   TList * fA2Asymmetries = new TList();

   TH1F * fAparaOverD = 0;
   TH1F * fA2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TCanvas * c = new TCanvas("cAparaOverD","Apara Over D");
   //c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries() ;jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();

      TH1F * fApara = paraAsym->fAsymmetryVsx;
      TH1F * fAperp = perpAsym->fAsymmetryVsx;

      InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsx; 

      fAparaOverD = new TH1F(*fApara);
      fA2 = new TH1F(*fAperp);

      fAparaOverD->SetNameTitle(Form("fAparaOverD-%d",jj),Form("Apara/D-%d",jj));
      fA2->SetNameTitle(Form("fA2-%d",jj),Form("A2-%d",jj));

      fAparaOverDAsymmetries->Add(fAparaOverD);
      fA2Asymmetries->Add(fA2);

      Int_t   xBinmax = fAparaOverD->GetNbinsX();
      Int_t   yBinmax = fAparaOverD->GetNbinsY();
      Int_t   zBinmax = fAparaOverD->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax; i++){
         for(Int_t j=0; j<= yBinmax; j++){
            for(Int_t k=0; k<= zBinmax; k++){

               bin   = fAparaOverD->GetBin(i,j,k);
               fAparaOverD->GetBinXYZ(bin,binx,biny,binz);

               Double_t W1     = paraKine->fW->GetBinContent(bin);
               Double_t x1     = paraKine->fx->GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi->GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2->GetBinContent(bin);
               Double_t W2     = perpKine->fW->GetBinContent(bin);
               Double_t x2     = perpKine->fx->GetBinContent(bin);
               Double_t phi2   = perpKine->fPhi->GetBinContent(bin);
               Double_t Q22    = perpKine->fQ2->GetBinContent(bin);
               Double_t W      = (W1+W2)/2.0;
               Double_t x      = (x1+x2)/2.0;
               Double_t phi    = (phi1+phi2)/2.0;
               Double_t Q2     = (Q21+Q22)/2.0;

               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               Double_t A180  = fApara->GetBinContent(bin);
               Double_t A80   = fAperp->GetBinContent(bin);

               Double_t eA180  = fApara->GetBinError(bin);
               Double_t eA80   = fAperp->GetBinError(bin);

               Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               Double_t R        = SFs->R(x,Q2);
               Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               Double_t cota     = 1.0/TMath::Tan(alpha);
               Double_t csca     = 1.0/TMath::Sin(alpha);

               Double_t c0 = 1.0/(1.0 + eta*xi);
               Double_t c11 = (1.0 + cota*chi)/D ;
               Double_t c12 = (csca*chi)/D ;
               Double_t c21 = (xi - cota*chi/eta)/D ;
               Double_t c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               Double_t A1 = A180/D;
               //Double_t A1 = c0*(c11*A180 + c12*A80);
               Double_t A2 = 0.0;//c0*(c21*A180 + c22*A80);

               Double_t eA1 = eA180/D;
               //Double_t eA1 = TMath::Sqrt( TMath::Power(c0*c11*eA180,2.0) + TMath::Power(c0*c12*eA80,2.0));
               Double_t eA2 = 0.0;//TMath::Sqrt( TMath::Power(c0*c21*eA180,2.0) + TMath::Power(c0*c22*eA80,2.0));
               //eA2 = c0*(c21*eA180 + c22*eA80);
               //A1 = c0*(A180/D - eta*A80/d);
               //A2 = c0*(xi*A180/D + A80/d);

               if ( A1 != A1 ) A1 = 0;
               if ( A2 != A2 ) A2 = 0;

               //             std::cout << "theta = " << theta << "\n";
               //std::cout << "A1 = " << A1 << "\n";
               //std::cout << "A2 = " << A2 << "\n";
               //             
               fAparaOverD->SetBinContent(bin,A1);
               fA2->SetBinContent(bin,A2);
               fAparaOverD->SetBinError(bin,eA1);
               fA2->SetBinError(bin,eA2);

            }
         }
      }

      //------------------------------
      // Apara/D ~ A1
      TH1 * h1 = fAparaOverD; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }
      gr->SetMarkerColor(fAparaOverD->GetMarkerColor());
      gr->SetLineColor(fAparaOverD->GetMarkerColor());
      if(jj%5 == 4){
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(20);
         gr->SetLineColor(1);
      }
      if(jj<5) {
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      } else if(jj<10 && jj%5!=4) {
         gr->SetMarkerStyle(23);
         mg2->Add(gr,"p");
      } else if(jj<15 && jj%5!=4) {
         gr->SetMarkerStyle(22);
         mg2->Add(gr,"p");
      } else {
         gr->SetMarkerStyle(20+jj-15);
         mg3->Add(gr,"p");
      }

   }

   // -----------------------------------------------------------

   fout->WriteObject(fAparaOverDAsymmetries,Form("AparaOverD-59-%d",paraRunGroup));//,TObject::kSingleKey); 

   // -----------------------------------------------------------


   // ------------------ A1 ----------------------

   NucDBManager * manager = NucDBManager::GetManager();

   TMultiGraph *MG = new TMultiGraph(); 
   TMultiGraph *MG2 = new TMultiGraph(); 
   TList * measurementsList = manager->GetMeasurements("AparapOverD");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   // SLAC E155x
   //TList * g1overf1 = manager->GetMeasurements("g1pOverF1p");
   //measurementsList->Add(g1overf1);
   // HERMES

   exp = manager->GetExperiment("CLAS-g1"); 
   if(exp) ames = exp->GetMeasurement("g1pOverF1p");
   if(ames) measurementsList->Add(ames);


   // Q2 bin for filtering data
   NucDBBinnedVariable Q2Filter("Qsquared","Q^{2}");
   Q2Filter.SetBinMinimum(1.5);
   Q2Filter.SetBinMaximum(15.5);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1World = (NucDBMeasurement*)measurementsList->At(i);
      A1World->ApplyFilterWithBin(&Q2Filter);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1World->BuildGraph("x");
      graph->SetMarkerStyle(27);
      //graph->Apply(xf);
      // Set legend title 
      Title = Form("%s",A1World->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
      MG2->Add(graph); 
   }
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("A_{1}^{p}+#etaA_{2}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.2,1.0); 
   MG->Draw("AP");
   leg->Draw();

   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_%d.svg",aNumber));

   //------------------------------------------------------------

   MG2->Add(mg2);
   // Draw everything 
   MG2->Draw("AP");
   MG2->SetTitle(GTitle);
   MG2->GetXaxis()->SetTitle(xAxisTitle);
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->SetTitle(yAxisTitle);
   MG2->GetYaxis()->CenterTitle();
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   MG2->GetYaxis()->SetRangeUser(-0.2,1.0); 
   MG2->Draw("AP");
   leg->Draw();

   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_split_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_split_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_AparaOverD_59_split_%d.svg",aNumber));

   return(0);
}
