Int_t vs_ecut2(){

   const Int_t nEbins = 20; 
   Double_t Elow      = 500; 
   Double_t Ehigh     = 2500; 
   Double_t deltaE    = (Ehigh-Elow)/double(nEbins);

   if (gROOT->LoadMacro("util/read_data_table.cxx") != 0) {
      Error(weh, "Failed loading read_data_table.cxx in compiled mode.");
      return -1;
   }
 
   if (gROOT->LoadMacro("util/stat_funcs.cxx") != 0) {
      Error(weh, "Failed loading stat_funcs.cxx in compiled mode.");
      return -1;
   }

   std::vector<Double_t> x1;
   std::vector<Double_t> y1;
   std::vector<Double_t> ex1;
   std::vector<Double_t> ey1;

   for(int i = 0; i<nEbins; i++ ) {
      std::vector<std::vector<Double_t> > data;
      Int_t res = 0;
      res = read_data_table(Form("asym/whit/para_asym32-%d.dat",i), &data);

      Double_t mean,error;
      if( res!=0 ) return(2); 

      std::vector<Double_t>  * x  = &(data.at(10)) ;
      std::vector<Double_t>  * ex = &(data.at(11)) ;

      std::cout << "size x " << x->size() << std::endl ;
      res = get_weighted_mean( x, ex, mean,error);

      x1.push_back(Elow + double(i)*deltaE);
      y1.push_back(mean);
      ey1.push_back(error);
      ex1.push_back(0.0);

      //std::cout << " mean is " << mean << std::endl;
      //std::cout << " error is " << error << std::endl;
   }

   TCanvas * c = new TCanvas("test","test");
   TGraphErrors * g1 = new TGraphErrors(nEbins,&x1[0],&y1[0],&ex1[0],&ey1[0]);

   g1->SetMarkerStyle(20);
   g1->SetMarkerColor(kRed);
   g1->SetTitle("Asymmetry vs energy cut");
   g1->GetXaxis()->SetTitle("E_{low} (GeV)");
   g1->Draw("ap");

   g1->GetYaxis()->SetRangeUser(0.0,1.0);
   c->Update();

   c->SaveAs("results/vs_ecut2.png");
   return(0);
}

