Int_t combine_runs_perp47(Int_t fileVersion = 32, bool writeRun = false) {

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   TFile * f = new TFile(Form("data/binned_asymmetries_perp47_%d.root",fileVersion),"UPDATE");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");

   Int_t             nAsym          = 5;
   TList           * fCombinedAsyms = new TList();
   TLegend         * leg            = new TLegend(0.1,0.7,0.3,0.9);
   TMultiGraph     * mg             = new TMultiGraph();

   for( int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = new InSANEAveragedMeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   ofstream missingfile("missing_runs.txt",ios::app);
   //  Q2 bin.
   for(int j = 0; j< nAsym; j++) {

      //std::cout << " asym : " << j << std::endl; 
      InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(j);

      for(int i = 0;i<aman->fRunQueue->size() ;i++) {

         Int_t runnumber = aman->fRunQueue->at(i) ;
         //std::cout << " RUN : " << runnumber << "\n";

         TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
         if(!fAsymmetries){
            if(j==0) missingfile << runnumber << std::endl;
            std::cout << " Run, " << runnumber << ", not found!" << std::endl;;
            continue;
         }
         //if(fAsymmetries) fAsymmetries->Print();

         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));

         if(asy) if(Asym) Asym->Add(asy);
         fAsymmetries->Delete(); 
      }

      InSANEMeasuredAsymmetry * A_avg = Asym->Calculate();

      //--- Graph vs x ---
      A_avg->fAsymmetryVsx->SetMarkerStyle(24);
      TH1 * h1 = A_avg->fAsymmetryVsx;
      TGraphErrors * gr = new TGraphErrors(h1);
      Int_t icol = 1;
      Int_t istyle = 20;
      if(j>3) istyle = 24;
      if(gr) {
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(h1->GetMarkerColor());
         gr->SetLineColor(h1->GetLineColor());
         gr->SetTitle("A_{80} - 5.9 GeV");
      if(j==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerSize(1.0);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("Q^{2} = %d",i+3),"ep");
         // remove zero statistics data points.
         for( int k = gr->GetN() -1; k>=0 ; k--) {
            Double_t xt, yt;
            gr->GetPoint(k,xt,yt);
            if( yt == 0.0 ) { gr->RemovePoint(k); }
         }
      }

   }   


   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 

   mg->Draw("a");
   mg->SetTitle("A_{80}, E=5.9 GeV with Cherenkov ADC cut");
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetXaxis()->SetTitle("x");
   mg->GetYaxis()->SetTitle("Asymmetry");
   //mg->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg->GetYaxis()->SetRangeUser(-0.5,0.5);
   leg->Draw();
   c->Update();

   f->WriteObject(fCombinedAsyms,Form("combined-asym_perp47-%d",0));//,TObject::kSingleKey); 

   std::cout << "Test3" << std::endl;

   c->SaveAs(Form("results/combined-asym_perp47_%d.png",fileVersion));
   c->SaveAs(Form("results/combined-asym_perp47_%d.pdf",fileVersion));

   return(0);
}

