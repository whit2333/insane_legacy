Int_t combine_runs_para47(Int_t fileVersion = 10, bool writeRun = false) {

   TFile * f = new TFile(Form("data/binned_asymmetries_para47_%d.root",fileVersion),"UPDATE");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/para/4_7GeV/mark_NH3.txt");

   Int_t nAsym = 4;
   TList  * fCombinedAsyms = new TList();
   for( int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = new InSANEAveragedMeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   for(int i = 0;i<aman->fRunQueue->size() ;i++) {
      Int_t runnumber = aman->fRunQueue->at(i) ;
      std::cout << " RUN : " << runnumber << "\n";

      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      if(!fAsymmetries) continue;
      if(fAsymmetries) fAsymmetries->Print();

      //  Q2 bin.
      for(int j = 0; j<fAsymmetries->GetEntries(); j++) {

         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));
         if(j<nAsym){ 
            InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(j);

            if(asy) if(Asym) Asym->Add(asy);
         }
      }
   }   
  
   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 
   TLegend * leg = new TLegend(0.1,0.7,0.3,0.9);
   TMultiGraph * mg = new TMultiGraph();

   TList * fCombAsymProt = new TList();
   TList * fCombAsymRCS = new TList();

   for( int i = 0 ; i < fCombinedAsyms->GetEntries() ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(i);
      InSANEMeasuredAsymmetry * A_avg = Asym->Calculate();
      //A_avg->PrintBinnedTable(std::cout) ;
      //std::ofstream fileout(Form("asym/whit/binned/binned_asym%d.dat",i),std::ios_base::trunc );
      //A_avg->PrintBinnedTable(fileout) ;
      A_avg->fAsymmetryVsx->SetMarkerStyle(24);

      TH1 * h1 = A_avg->fAsymmetryVsx;
      TGraphErrors * gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      //TGraphErrors * gr = 0;
      //Int_t retres = hist_to_graph( A_avg->fAsymmetryVsx , gr);
      //if( !gr ) return(-5);
      Int_t icol = 1;
      Int_t istyle = 20;
      if(i>3) istyle = 24;
      if(gr) {
         gr->SetMarkerStyle(istyle);
         gr->SetMarkerColor(h1->GetMarkerColor());
         gr->SetLineColor(h1->GetLineColor());
         gr->SetTitle("A_{180}");
         //gr->SetTitle("A_{180} with Cherenkov ADC cut");
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("Q^{2} = %d",i+3),"ep");
      }
      Asym->GetAsymmetries()->Clear();
      if(i>3) fCombAsymProt->Add(Asym);
      else fCombAsymRCS->Add(Asym);
      Asym->Dump();
   }
   std::cout << "Test1" << std::endl;
   mg->Draw("a");
   mg->SetTitle("A_{80}, E=4.7 GeV with Cherenkov ADC cut");
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetXaxis()->SetTitle("x");
   mg->GetYaxis()->SetTitle("Asymmetry");
   //mg->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg->GetYaxis()->SetRangeUser(-0.9,0.9);

   leg->Draw();
   c->Update();

   std::cout << "Test2" << std::endl;
   fCombinedAsyms->Print(); 
   //f->WriteObject(fCombinedAsyms,Form("combined-asym_para47-%d",0));//,TObject::kSingleKey); 
   //f->WriteObject(fCombAsymProt,Form("combined-asym-Prot_para47-%d",0));//,TObject::kSingleKey); 
   f->WriteObject(fCombAsymRCS,Form("combined-asym-RCS_para47-%d",0));//,TObject::kSingleKey); 

   std::cout << "Test3" << std::endl;

   //TFile * f2 = new TFile("data/a180_graphs.root","UPDATE");
   //f2->WriteObject(mg,Form("asym_para47"));//,TObject::kSingleKey); 

   std::cout << "Test3" << std::endl;
   c->SaveAs(Form("results/combined-asym_para47_%d.png",fileVersion));
   c->SaveAs(Form("results/combined-asym_para47_%d.pdf",fileVersion));

   return(0);
}

