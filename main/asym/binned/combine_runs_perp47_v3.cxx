Int_t combine_runs_perp47_v3(Int_t fileVersion = 4306) {

   Double_t Ebeam = 4.7;
   Int_t    nAsym = 25;
   TString  missingFileName = Form("log/analysis/%d/missing_runs_perp47.txt",fileVersion);

   TFile * f = new TFile(Form("data/binned_asymmetries_perp47_%d.root",fileVersion),"UPDATE");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");
   //aman->fRunQueue->push_back(72825);
   //aman->fRunQueue->push_back(72824);

   TList  * fCombinedAsyms = new TList();
   TList  * fAAsyms        = new TList();
   fAAsyms->SetOwner(false);
   Int_t    nExpAsym = 0;

   for( int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = new InSANEAveragedMeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   ofstream missingfile(missingFileName.Data(),ios::app);
   bool first = true;

   for(int i = 0;i<aman->fRunQueue->size();i++) {

      Int_t runnumber = aman->fRunQueue->at(i) ;
      TFile frun(Form("data/asym-%d/binned_asymmetries_perp47_%d.root",fileVersion,runnumber),"READ");

      frun.cd();
      //std::cout << " RUN : " << runnumber << "\n";
   
      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      if(!fAsymmetries){
         missingfile << runnumber << std::endl;
         std::cout << " Run, " << runnumber << ", not found!" << std::endl;;
         continue;
      }
      f->cd();
      //f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 

      nExpAsym = fAsymmetries->GetEntries();

      //  For now ... just looking at the first Q2 bin.
      for(int j = 0; j<fAsymmetries->GetEntries() && j<fCombinedAsyms->GetEntries(); j++) {

         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));
         InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(j);

         //if(j==4) asy->Print();

         //if( !first ) {
         //   if( asy->fRunSummary.GetOverallSign() != Asym->GetMeasuredAsymmetryResult()->fRunSummary.GetOverallSign() ) 
         //      continue;
         //}

         if(asy && Asym ) {
            //std::cout << "Adding asym- " << j << std::endl;
            //asy->Dump();
            Asym->Add(*asy);
            first = false;
         }
      }
      if(fAsymmetries) fAsymmetries->Delete();
      //frun->Close();
   }   
  
   // ---------------------------------------------------------------------
   //std::cout << " nAsym = " << nAsym << ", nExpAsym = " << nExpAsym << std::endl;
   for( int i = nAsym; nExpAsym<i; i--) {
      fCombinedAsyms->RemoveAt(i-1);
   }
   //std::cout << fCombinedAsyms->GetEntries() << " to combine. " << std::endl;

   // ---------------------------------------------------------------------
   TH1::AddDirectory(kFALSE);
   for( int i = 0 ; i < fCombinedAsyms->GetEntries() ; i++) {

      InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(i);
      InSANEMeasuredAsymmetry  comb_asym = Asym->Calculate();
      //InSANECombinedAsymmetry         * A_avg = new InSANECombinedAsymmetry(comb_asym);

      fAAsyms->Add(Asym);
   }

   f->WriteObject(fAAsyms,Form("combined-asym_perp47-%d",0));//,TObject::kSingleKey); 

   f->Flush();
   f->Close();
   return(0);
}

