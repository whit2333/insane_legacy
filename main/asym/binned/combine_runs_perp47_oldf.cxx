Int_t combine_runs_perp47(Int_t fileVersion = 11, bool writeRun = false) {

   TFile * f = new TFile(Form("data/binned_asymmetries_perp47_%d.root",fileVersion),"UPDATE");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");

   TLegend          * leg = new TLegend(0.1,0.7,0.3,0.9);
   TMultiGraph       * mg = new TMultiGraph();
   Int_t            nAsym = 4;
   TList * fCombinedAsyms = new TList();
   for(int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = new InSANEAveragedMeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   for(int i = 0;i<aman->fRunQueue->size() ;i++) {
      Int_t runnumber = aman->fRunQueue->at(i) ;
      std::cout << " RUN : " << runnumber << "\n";

      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      if(!fAsymmetries) continue;
      if(fAsymmetries) fAsymmetries->Print();

      //  Q2 bin.
      for(int j = 0; j<fAsymmetries->GetEntries(); j++) {

         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));
         if(j<4){ 
            InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(j);

            if(asy) if(Asym) Asym->Add(asy);
         }
      }
   }   
  
   for( int i = 0 ; i < fCombinedAsyms->GetEntries() ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(i);
      InSANEMeasuredAsymmetry * A_avg = Asym->Calculate();
      A_avg->PrintBinnedTable(std::cout) ;
      std::ofstream fileout(Form("asym/whit/binned/binned_asym%d.dat",i),std::ios_base::trunc );
      A_avg->PrintBinnedTable(fileout) ;
      A_avg->fAsymmetryVsx->SetMarkerStyle(24);

      TH1 * h1 = A_avg->fAsymmetryVsx;
      TGraphErrors * gr = new TGraphErrors(h1);
      for( int j = 0; j<gr->GetN() ; j++) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
            gr->RemovePoint(j);
         }
      }
      for( int j = 0; j<gr->GetN() ; j++) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
            gr->RemovePoint(j);
         }
      }
      //for( int j = 0; j<gr->GetN() ; j++) {
      //   Double_t xt, yt;
      //   gr->GetPoint(j,xt,yt);
      //   if( yt < 1.0e-4 ) {
      //      std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
      //      gr->RemovePoint(j);
      //   }
      //}
      //for( int j = 0; j<gr->GetN() ; j++) {
      //   Double_t xt, yt;
      //   gr->GetPoint(j,xt,yt);
      //   if( yt < 1.0e-4 ) {
      //      std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
      //      gr->RemovePoint(j);
      //   }
      //}
      //for( int j = 0; j<gr->GetN() ; j++) {
      //   Double_t xt, yt;
      //   gr->GetPoint(j,xt,yt);
      //   if( yt < 1.0e-4 ) {
      //      std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
      //      gr->RemovePoint(j);
      //   }
      //}
      //for( int j = 0; j<gr->GetN() ; j++) {
      //   Double_t xt, yt;
      //   gr->GetPoint(j,xt,yt);
      //   if( yt < 1.0e-4 ) {
      //      std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
      //      gr->RemovePoint(j);
      //   }
      //}

      //TGraphErrors * gr = 0;
      //Int_t retres = hist_to_graph( A_avg->fAsymmetryVsx , gr);
      //if( !gr ) return(-5);
      Int_t icol = 1;
      if(gr) {
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(h1->GetMarkerColor());
         gr->SetLineColor(h1->GetLineColor());
         gr->SetTitle("A_{80}");
         //gr->SetTitle("A_{180} with Cherenkov ADC cut");
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("Q^{2} = %d",i+3),"ep");
      }
      //if(i==0) {
      //   A_avg->fAsymmetryVsx->Draw("E1");
      //   A_avg->fAsymmetryVsx->GetXaxis()->SetRangeUser(0.0,0.95);
      //   A_avg->fAsymmetryVsx->GetYaxis()->SetRangeUser(0.01,0.9);
      //   A_avg->fAsymmetryVsx->Draw("E1");
      //}
      //else A_avg->fAsymmetryVsx->Draw("same,E1");
      //A_avg->fAsymmetryVsx->SetTitle("A_{180} with Cherenkov ADC cut");
      ////A_avg->fAsymmetryVsx->SetRangeUser(0.1,0.9);
      //A_avg->fAsymmetryVsx->SetMaximum(0.9);
      //A_avg->fAsymmetryVsx->SetMinimum(0.01);
      //leg->AddEntry(A_avg->fAsymmetryVsx,Form("Q^{2} = %d",i+3),"ep");
      Asym->GetAsymmetries()->Clear();
   }
   std::cout << "Test1" << std::endl;

   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 
   mg->Draw("a");
   mg->SetTitle("A_{80}, E=5.9 GeV with Cherenkov ADC cut");
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetXaxis()->SetTitle("x");
   mg->GetYaxis()->SetTitle("Asymmetry");
   //mg->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg->GetYaxis()->SetRangeUser(-0.9,0.9);

   leg->Draw();
   c->Update();

   std::cout << "Test2" << std::endl;
   
   f->WriteObject(fCombinedAsyms,Form("combined-asym_perp47-%d",0));//,TObject::kSingleKey); 

   std::cout << "Test3" << std::endl;

   //TFile * f2 = new TFile("data/a180_graphs.root","UPDATE");
   //f2->WriteObject(mg,Form("asym_perp47"));//,TObject::kSingleKey); 

   std::cout << "Test3" << std::endl;
   c->SaveAs(Form("results/combined-asym_perp47_%d.png",fileVersion));
   c->SaveAs(Form("results/combined-asym_perp47_%d.pdf",fileVersion));

   return(0);
}

