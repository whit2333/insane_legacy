Int_t combine_runs_para47_test(Int_t fileVersion = 2, bool writeRun = false) {

   TFile * f = new TFile(Form("data/binned_asymmetries_para47_%d.root",fileVersion),"UPDATE");
   f->cd();
   f->ls();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->GetRunQueue()->push_back(72999);
   aman->GetRunQueue()->push_back(73005);
   //aman->BuildQueue2("lists/para/4_7GeV/mark_NH3.txt");

   Int_t  nAsym = 5;
   TList  * fCombinedAsyms = new TList();
   TList  * fAAsyms = new TList();
   for( int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = new InSANEAveragedMeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   ofstream missingfile("missing_runs.txt",ios::app);
   for(int i = 0;i<aman->fRunQueue->size();i++) {
      Int_t runnumber = aman->fRunQueue->at(i) ;
      std::cout << " RUN : " << runnumber << "\n";
      //f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
   
      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      if(!fAsymmetries){
         missingfile << runnumber << std::endl;
         std::cout << " Run, " << runnumber << ", not found!" << std::endl;;
         continue;
      }

      //  For now ... just looking at the first Q2 bin.
      for(int j = 0; j<fAsymmetries->GetEntries(); j++) {
         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));
         if(j<nAsym){ 
            InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(j);

            if(asy && Asym ) Asym->Add(asy);
         }
      }
   }   
  
   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 
   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);

   for( int i = 0 ; i < fCombinedAsyms->GetEntries() ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(i);
      InSANEMeasuredAsymmetry * A_avg = Asym->Calculate();
      //A_avg->PrintBinnedTable(std::cout) ;
      //std::ofstream fileout(Form("asym/whit/binned/binned_asym%d.dat",i),std::ios_base::trunc );
      //A_avg->PrintBinnedTable(fileout) ;
      if(i>3)A_avg->fAsymmetryVsx->SetMarkerStyle(24);
      if(i==0) {
         A_avg->fAsymmetryVsx->Draw("E1");
         A_avg->fAsymmetryVsx->GetXaxis()->SetRangeUser(0.0,0.95);
         A_avg->fAsymmetryVsx->GetYaxis()->SetRangeUser(0.01,0.9);
         A_avg->fAsymmetryVsx->Draw("E1");
      }
      else A_avg->fAsymmetryVsx->Draw("same,E1");
      A_avg->fAsymmetryVsx->SetTitle("A_{180} with Cherenkov ADC cut");
      //A_avg->fAsymmetryVsx->SetRangeUser(0.1,0.9);
      A_avg->fAsymmetryVsx->SetMaximum(0.9);
      A_avg->fAsymmetryVsx->SetMinimum(0.01);
      leg->AddEntry(A_avg->fAsymmetryVsx,Form("Q^{2} = %d",i+3),"ep");
      //Asym->GetAsymmetries()->Clear();
      //A_avg->fAvgKineVsx->Dump();
      fAAsyms->Add(A_avg);
   }
   leg->Draw();
   c->Update();

   
   f->WriteObject(fCombinedAsyms,Form("combined-asym_para47-%d",0));//,TObject::kSingleKey); 

   new TBrowser();
   c->SaveAs(Form("results/combined-asym_para47_%d.png",fileVersion));
   c->SaveAs(Form("results/combined-asym_para47_%d.pdf",fileVersion));

   //new TBrowser();
   return(0);
}

