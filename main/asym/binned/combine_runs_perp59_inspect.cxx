Int_t combine_runs_perp59_inspect(Int_t fileVersion = 32) {

   Double_t Ebeam = 5.9;
   Int_t    nAsym = 5;
   TString  missingFileName = Form("missing_runs_perp%d.txt",int(10*Ebeam));

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   TFile * f = new TFile(Form("data/binned_asymmetries_perp59_%d.root",fileVersion),"UPDATE");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   //aman->BuildQueue2("lists/perp/5_9GeV/positive_NH3.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/negative_NH3.txt");
   aman->BuildQueue2("lists/perp/5_9GeV/good_NH3.txt");

   TList  * fCombinedAsyms = new TList();
   TList  * fAAsyms        = new TList();

   for( int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = new InSANEAveragedMeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   ofstream missingfile(missingFileName.Data(),ios::app);

   TGraph       * gr_1 = 0;
   TGraphErrors * gr   = 0;
   TGraphErrors * gr2   = 0;
   TMultiGraph * mg_inspect     = new TMultiGraph();
   TMultiGraph * mg_inspect_sys = new TMultiGraph();
   std::vector<int> mg_runs;

   for(int i = 0;i<aman->fRunQueue->size();i++) {

      Int_t runnumber = aman->fRunQueue->at(i) ;
      //std::cout << " RUN : " << runnumber << "\n";
      //f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
   
      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      if(!fAsymmetries){
         missingfile << runnumber << std::endl;
         std::cout << " Run, " << runnumber << ", not found!" << std::endl;;
         continue;
      }

      //  For now ... just looking at the first Q2 bin.
      for(int j = 0; j<fAsymmetries->GetEntries() && j<fCombinedAsyms->GetEntries(); j++) {
         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
         //if(asy) asy->CalculateAsymmetries();
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));
         InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(j);
         if(asy && Asym ) {
            Asym->Add(asy);
            if(j==4){
               gr = new TGraphErrors(asy->fAsymmetryVsx);
               gr->SetMarkerStyle(20);
               gr->SetMarkerColor(2);
               gr->SetLineColor(2);
               // remove zero statistics data points.
               for( int k = gr->GetN() -1; k>=0 ; k--) {
                  Double_t xt, yt;
                  gr->GetPoint(k,xt,yt);
                  if( yt == 0.0 ) { 
                     gr->RemovePoint(k);
                     continue;
                  }
               }
               mg_inspect->Add(gr,"p");
               mg_runs.push_back(runnumber);

               gr_1 = new TGraph(asy->fSystErrVsx);
               for( int k = gr_1->GetN() -1; k>=0 ; k--) {
                  Double_t xt, yt;
                  gr_1->GetPoint(k,xt,yt);
                  if( yt == 0.0 ) { 
                     gr_1->RemovePoint(k);
                     continue;
                  }
               }
               TVectorD sysX(gr_1->GetN(),gr_1->GetX());
               TVectorD sysY(gr_1->GetN(),gr_1->GetY());
               sysY *= 0.5;
               TVectorD sysEY(gr_1->GetN(),gr_1->GetY());
               sysEY *= 0.5;
               TVectorD sysEX(gr_1->GetN());
               sysEX.Zero();
               TGraphErrors * grSys1 = new TGraphErrors(sysX,sysY,sysEX,sysEY);

               grSys1->SetFillStyle(3005);
               grSys1->SetMarkerColor(6);
               grSys1->SetMarkerColor(6);
               grSys1->SetMarkerStyle(20);
               grSys1->SetFillColor(6);
               grSys1->SetLineColor(6);
               mg_inspect_sys->Add(grSys1,"3");
            }
         }
      }
   }   
  
   // ---------------------------------------------------------------------

   TCanvas     * c   = new TCanvas("inspect","combine_runs");
   TLegend     * leg = new TLegend(0.85,0.75,0.99,0.95);
   leg->SetFillColor(0);
   TGraphErrors * gr1 = 0;
   TH1 * h1 = 0;

   // -------------------------------
   // Get the single large Q2 asymmetry
   if(fCombinedAsyms->GetEntries() < 5) return -1;
   InSANEAveragedMeasuredAsymmetry * Asym  = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(4);
   InSANEMeasuredAsymmetry         * A_avg = Asym->Calculate();
   TH1                             * h1    = A_avg->fAsymmetryVsx;
   TH1                             * hsys  = A_avg->fSystErrVsx;
   TGraphErrors                    * grAll = new TGraphErrors(h1);
   TGraph                          * grSys = new TGraph(hsys);
   // remove zero statistics data points.
   for( int k = grAll->GetN() -1; k>=0 ; k--) {
      Double_t xt, yt;
      grAll->GetPoint(k,xt,yt);
      if( yt == 0.0 ) { 
         grAll->RemovePoint(k); 
         continue;
      }
   }
   grAll->SetMarkerStyle(20);
   grAll->SetMarkerSize(1.0);
   grAll->SetMarkerColor(1);
   grAll->SetLineColor(1);
   grAll->SetTitle(Form("A_{180}, E=%.1fGeV",Ebeam));

   for( int k = grSys->GetN() -1; k>=0 ; k--) {
      Double_t xt, yt;
      grSys->GetPoint(k,xt,yt);
      if( yt == 0.0 ) { 
         grSys->RemovePoint(k); 
         continue;
      }
   }
   TVectorD sysX(grSys->GetN(),grSys->GetX());
   TVectorD sysY(grSys->GetN(),grSys->GetY());
   sysY *= 0.5;
   TVectorD sysEY(grSys->GetN(),grSys->GetY());
   sysEY *= 0.5;
   TVectorD sysEX(grSys->GetN());
   sysEX.Zero();
   TGraphErrors * grSysBand = new TGraphErrors(sysX,sysY,sysEX,sysEY);

   grSysBand->SetFillStyle(3004);
   grSysBand->SetMarkerColor(4);
   grSysBand->SetMarkerStyle(20);
   grSysBand->SetFillColor(4);
   grSysBand->SetLineColor(4);

   TLatex l;
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   TColor * col = new TColor(6666,0.0,0.0,0.0,"",0.8);
   l.SetTextColor(6666);
   l.SetTextSize(0.15);
   //l.SetTextAngle(45);
   TLatex * latex_run = 0;
   // -----------------------------------
   // Loop over individual runs
   TMultiGraph * mgAll = new TMultiGraph();
   mgAll->Add(grSysBand,"3");
   mgAll->Add(grAll,"ep");
   TList * mglist =  mg_inspect->GetListOfGraphs();
   if( mglist->GetEntries() != mg_runs.size()) std::cout << "error: list sizes do not match!" << std::endl;
   for(int i = 0; i<mglist->GetEntries();i++){
      if(latex_run) delete latex_run;
      latex_run=0;
      delete c;
      c   = new TCanvas("inspect","combine_runs");
      //c->Clear();
      leg->Clear();

      mgAll->Draw("a");
      mgAll->GetXaxis()->SetLimits(0.0,1.0);
      mgAll->GetYaxis()->SetRangeUser(-1.0,1.0);
      mgAll->GetXaxis()->SetTitle("x");
      mgAll->GetYaxis()->SetTitle("");
      mgAll->Draw("a");

      gr1 = (TGraphErrors*) mglist->At(i);
      gr2 = (TGraphErrors*) mg_inspect_sys->GetListOfGraphs()->At(i);
      leg->AddEntry(grAll,"All runs combined","ep");
      leg->AddEntry(gr1,Form("run - %d",mg_runs[i]),"ep");

      gr2->Draw("3");
      gr1->Draw("ep");
      leg->Draw();

      latex_run = l.DrawLatex(0.1,1.1,Form("%d",mg_runs[i]));
      c->Update();

      c->SaveAs(Form("results/asym_inspect/combined-asym_perp59_inspect_%d.gif+99",fileVersion));
      c->SaveAs(Form("results/asym_inspect/combined-asym_perp59_inspect_%d.png",mg_runs[i]));
   }
   c->SaveAs(Form("results/asym_inspect/combined-asym_perp59_inspect_%d.gif++",fileVersion));

   //
   //f->WriteObject(fCombinedAsyms,Form("combined-asym_perp59-%d",0));//,TObject::kSingleKey); 

   //c->SaveAs(Form("results/combined-asym_perp59_%d.png",fileVersion));
   //c->SaveAs(Form("results/combined-asym_perp59_%d.pdf",fileVersion));

   return(0);
}

