Int_t combine_runs_perp47_pi0(Int_t fileVersion = 160) {

   Double_t Ebeam = 4.7;
   Int_t    nAsym = 25;
   TString  missingFileName = Form("log/analysis/%d/missing_runs_perp47_pi0.txt",fileVersion);

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   TFile * f = new TFile(Form("data/binned_asymmetries_perp47_pi0_%d.root",fileVersion),"UPDATE");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/bottom_NH3.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/negative_NH3.txt");

   TList  * fCombinedAsyms = new TList();
   TList  * fAAsyms        = new TList();
   Int_t    nExpAsym = 0;

   for( int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedPi0MeasuredAsymmetry * Asym = new InSANEAveragedPi0MeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   ofstream missingfile(missingFileName.Data(),ios::app);

   for(int i = 0;i<aman->fRunQueue->size();i++) {

      Int_t runnumber = aman->fRunQueue->at(i) ;
      TFile * frun = new TFile(Form("data/asym-%d/binned_asymmetries_perp47_pi0_%d.root",fileVersion,runnumber),"READ");
      if(!frun) continue;
      frun->cd();

      std::cout << " RUN : " << runnumber << "\n";
      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("pi0-binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      //fAsymmetries->SetOwner(true);
      if(!fAsymmetries){
         missingfile << runnumber << std::endl;
         std::cout << " Run, " << runnumber << ", not found!" << std::endl;;
         continue;
      }
      f->cd();
      f->WriteObject(fAsymmetries,Form("pi0-binned-asym-%d",runnumber));//,TObject::kSingleKey); 

      nExpAsym = fAsymmetries->GetEntries();


      //  For now ... just looking at the first Q2 bin.
      for(int j = 0; j<fAsymmetries->GetEntries() && j<fCombinedAsyms->GetEntries(); j++) {
         InSANEPi0MeasuredAsymmetry * asy = ((InSANEPi0MeasuredAsymmetry*)(fAsymmetries->At(j)));
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));
         InSANEAveragedPi0MeasuredAsymmetry * Asym = (InSANEAveragedPi0MeasuredAsymmetry*)fCombinedAsyms->At(j);
         if(asy && Asym ) Asym->Add(asy);
      }
      if(fAsymmetries) fAsymmetries->Delete();
      frun->Close();
      //delete frun; 
   }   
   std::cout << " MADE IT \n";
  
   // ---------------------------------------------------------------------

   TLegend     * leg = new TLegend(0.85,0.75,0.99,0.95);
   leg->SetFillColor(0);
   TLegend     * leg2 = new TLegend(0.85,0.75,0.99,0.95);
   leg2->SetFillColor(0);

   TMultiGraph * mg  = new TMultiGraph();
   TMultiGraph * mg2  = new TMultiGraph();
   TMultiGraph * mg3  = new TMultiGraph();
   TMultiGraph * mg4  = new TMultiGraph();

   std::cout << " nAsym = " << nAsym << ", nExpAsym = " << nExpAsym << std::endl;
   for( int i = nAsym; nExpAsym<i; i--) {
      fCombinedAsyms->RemoveAt(i-1);
   }
   std::cout << fCombinedAsyms->GetEntries() << " to combine. " << std::endl;

   for( int i = 0 ; i < fCombinedAsyms->GetEntries() ; i++) {

      InSANEAveragedPi0MeasuredAsymmetry * Asym = (InSANEAveragedPi0MeasuredAsymmetry*)fCombinedAsyms->At(i);
      InSANEPi0MeasuredAsymmetry * A_avg = Asym->Calculate();

      TH1          * h1 = 0;
      TGraphErrors * gr = 0;
      // Asym vs E
      h1 = A_avg->fAsymmetryVsE;
      h1->SetMarkerColor(2000 + i%5);
      h1->SetLineColor(  2000 + i%5);
      if( i%5 == 4 ) {
         h1->SetMarkerColor( 1 );
         h1->SetLineColor(   1 );
      }
      gr = new TGraphErrors(h1);
      // remove zero statistics data points.
      for( int k = gr->GetN() -1; k>=0 ; k--) {
         Double_t xt, yt;
         gr->GetPoint(k,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(k); }
      }
      if(i<4) {
         mg->Add(gr,"ep");
         leg->AddEntry(gr,"pi0","ep");
      }

      // Asym vs Pt 
      h1 = A_avg->fAsymmetryVsPt;
      h1->SetMarkerColor(2000 + i%5);
      h1->SetLineColor(  2000 + i%5);
      if( i%5 == 4 ) {
         h1->SetMarkerColor( 1 );
         h1->SetLineColor(   1 );
      }
      gr = new TGraphErrors(h1);
      // remove zero statistics data points.
      for( int k = gr->GetN() -1; k>=0 ; k--) {
         Double_t xt, yt;
         gr->GetPoint(k,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(k); }
      }
      if(i<4) {
         mg2->Add(gr,"ep");
         //leg->AddEntry(gr,"pi0","ep");
      }

      // Asym vs Pt 
      h1 = A_avg->fAsymmetryVsTheta;
      h1->SetMarkerColor(2000 + i%5);
      h1->SetLineColor(  2000 + i%5);
      if( i%5 == 4 ) {
         h1->SetMarkerColor( 1 );
         h1->SetLineColor(   1 );
      }
      gr = new TGraphErrors(h1);
      // remove zero statistics data points.
      for( int k = gr->GetN() -1; k>=0 ; k--) {
         Double_t xt, yt;
         gr->GetPoint(k,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(k); }
      }
      if(i<4) {
         mg3->Add(gr,"ep");
         //leg->AddEntry(gr,"pi0","ep");
      }

      // Asym vs Pt 
      h1 = A_avg->fAsymmetryVsPhi;
      h1->SetMarkerColor(2000 + i%5);
      h1->SetLineColor(  2000 + i%5);
      if( i%5 == 4 ) {
         h1->SetMarkerColor( 1 );
         h1->SetLineColor(   1 );
      }
      gr = new TGraphErrors(h1);
      // remove zero statistics data points.
      for( int k = gr->GetN() -1; k>=0 ; k--) {
         Double_t xt, yt;
         gr->GetPoint(k,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(k); }
      }
      if(i<4) {
         mg4->Add(gr,"ep");
         //leg->AddEntry(gr,"pi0","ep");
      }


      fAAsyms->Add(A_avg);
   }

   // -----------------------------
   // 
   TCanvas     * c   = new TCanvas("combine_runs","combine_runs");
   c->Divide(2,2);

   c->cd(1);
   mg->SetTitle(Form("A_{80}, E=%.1fGeV",Ebeam));
   mg->Draw("a");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg->GetXaxis()->SetTitle("E_{#pi}");
   mg->GetYaxis()->SetTitle("");
   //leg->Draw();

   c->cd(2);
   mg2->SetTitle(Form("A_{80}, E=%.1fGeV",Ebeam));
   mg2->Draw("a");
   mg2->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg2->GetXaxis()->SetTitle("P_{t#pi}");
   mg2->GetYaxis()->SetTitle("");

   c->cd(3);
   mg3->SetTitle(Form("A_{80}, E=%.1fGeV",Ebeam));
   mg3->Draw("a");
   mg3->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg3->GetXaxis()->SetTitle("#theta_{#pi}");
   mg3->GetYaxis()->SetTitle("");

   c->cd(4);
   mg4->SetTitle(Form("A_{80}, E=%.1fGeV",Ebeam));
   mg4->Draw("a");
   mg4->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg4->GetXaxis()->SetTitle("#phi_{#pi}");
   mg4->GetYaxis()->SetTitle("");

   //TLatex l;
   //l.SetTextAlign(12);  //centered
   //l.SetTextFont(132);
   //TColor * col = new TColor(6666,0.0,0.0,0.0,"",0.5);
   //l.SetTextColor(6666);
   //l.SetTextSize(0.2);
   //l.SetTextAngle(45);
   //l.DrawLatex(0.1,-0.4,"Preliminary");
   
   
   f->WriteObject(fCombinedAsyms,Form("combined-asym_perp47_pi0-%d",0));//,TObject::kSingleKey); 

   c->SaveAs(Form("results/combined-asym_perp47_pi0_%d.png",fileVersion));
   c->SaveAs(Form("results/combined-asym_perp47_pi0_%d.pdf",fileVersion));

   return(0);
}

