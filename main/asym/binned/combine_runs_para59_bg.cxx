Int_t combine_runs_para59_bg(Int_t fileVersion = 34, bool writeRun = false) {

   TFile * f = new TFile(Form("data/binned_asymmetries_para59_bg_%d.root",fileVersion),"UPDATE");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/para/5_9GeV/mark_NH3.txt");

   Int_t  nAsym = 4;

   TList  * fCombinedAsyms = new TList();
   TList  * fAAsyms = new TList();
   for( int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = new InSANEAveragedMeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   ofstream missingfile("missing_runs.txt",ios::app);
   for(int i = 0;i<aman->fRunQueue->size();i++) {
      Int_t runnumber = aman->fRunQueue->at(i) ;
      //std::cout << " RUN : " << runnumber << "\n";
      //f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
   
      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      if(!fAsymmetries){
         missingfile << runnumber << std::endl;
         std::cout << " Run, " << runnumber << ", not found!" << std::endl;;
         continue;
      }
      //fAsymmetries->Print();

      //  For now ... just looking at the first Q2 bin.
      for(int j = 0; j<fAsymmetries->GetEntries(); j++) {
         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));
         if(j<nAsym){ 
            InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(j);

            if(asy && Asym ) Asym->Add(asy);
         }
      }
   }   
  
   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 
   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);

   TMultiGraph * mg = new TMultiGraph();
   for( int i = 0 ; i < fCombinedAsyms->GetEntries() ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(i);
      InSANEMeasuredAsymmetry * A_avg = Asym->Calculate();
      //A_avg->PrintBinnedTable(std::cout) ;
      //std::ofstream fileout(Form("asym/whit/binned/binned_asym%d.dat",i),std::ios_base::trunc );
      //A_avg->PrintBinnedTable(fileout) ;
      if(i>3)A_avg->fAsymmetryVsx->SetMarkerStyle(24);
      //if(i==0) {
      //   //A_avg->fAsymmetryVsx->Draw("E1");
      //   //A_avg->fAsymmetryVsx->GetXaxis()->SetRangeUser(0.0,1.0);
      //   //A_avg->fAsymmetryVsx->GetYaxis()->SetRangeUser(0.0,1.0);
      //}
      //else A_avg->fAsymmetryVsx->Draw("same,E1");
      TGraphErrors * gr = new TGraphErrors(A_avg->fAsymmetryVsx);
         for( int k = gr->GetN() -1; k>=0 ; k--) {
            Double_t xt, yt;
            gr->GetPoint(k,xt,yt);
            if( yt == 0.0 ) { gr->RemovePoint(k); }
         }
      mg->Add(gr,"p");
      //A_avg->fAsymmetryVsx->SetTitle("$A_{180}$, E=5.9 GeV ");
      //A_avg->fAsymmetryVsx->SetRangeUser(0.1,0.9);
      //A_avg->fAsymmetryVsx->SetMaximum(1.0);
      //A_avg->fAsymmetryVsx->SetMinimum(0.001);
      leg->AddEntry(A_avg->fAsymmetryVsx,Form("Q^{2} = %d",i+3),"ep");
      //Asym->GetAsymmetries()->Clear();
      //A_avg->fAvgKineVsx->Dump();
      fAAsyms->Add(A_avg);
   }
   mg->Draw("a");
   mg->SetMinimum(-0.5);
   mg->SetMaximum(1.0);
   mg->GetXaxis()->SetLimits(0.0,1.0);
   gPad->Modified();
   leg->Draw();

   //c->Update();
   gPad->Modified();
   gPad->SetGridy(true);

   
   f->WriteObject(fCombinedAsyms,Form("combined-asym_para59-%d",0));//,TObject::kSingleKey); 
   //f->WriteObject(fAAsyms,Form("some-asym_para59-%d",0));//,TObject::kSingleKey); 

   c->SaveAs(Form("results/combined-asym_para59_%d.png",fileVersion));
   c->SaveAs(Form("results/combined-asym_para59_%d.pdf",fileVersion));

   gPad->Print(Form("results/combined-asym_para59_%d.tex",fileVersion));

   //new TBrowser();
   return(0);
}

