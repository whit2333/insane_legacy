Int_t combine_runs_perp59_double_peak(Int_t fileVersion = 32) {

   Double_t Ebeam = 5.9;
   Int_t    nAsym = 5;
   TString  missingFileName = Form("missing_runs_perp%d.txt",int(10*Ebeam));

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   TFile * f = new TFile(Form("data/binned_asymmetries_perp59_double_peak_%d.root",fileVersion),"UPDATE");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   //aman->BuildQueue2("lists/perp/5_9GeV/negative_NH3.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/positive_NH3.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/bottom_NH3.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/top_NH3.txt");
   aman->BuildQueue2("lists/perp/5_9GeV/good_NH3.txt");

   TList  * fCombinedAsyms = new TList();
   TList  * fAAsyms        = new TList();

   for( int i = 0 ; i < nAsym ; i++) {
      InSANEAveragedMeasuredAsymmetry * Asym = new InSANEAveragedMeasuredAsymmetry();
      Asym->SetNameTitle(Form("combined-%d",i),Form("combined-%d",i));
      fCombinedAsyms->Add(Asym);
   }

   ofstream missingfile(missingFileName.Data(),ios::app);

   for(int i = 0;i<aman->fRunQueue->size();i++) {

      Int_t runnumber = aman->fRunQueue->at(i) ;
      //std::cout << " RUN : " << runnumber << "\n";
      //f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
   
      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      if(!fAsymmetries){
         missingfile << runnumber << std::endl;
         std::cout << " Run, " << runnumber << ", not found!" << std::endl;;
         continue;
      }

      //  For now ... just looking at the first Q2 bin.
      for(int j = 0; j<fAsymmetries->GetEntries() && j<fCombinedAsyms->GetEntries(); j++) {
         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
         asy->SetName(Form("%s-%d",asy->GetName(),runnumber));
         InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(j);
         if(asy && Asym ) Asym->Add(asy);
      }
   }   
  
   // ---------------------------------------------------------------------

   TCanvas     * c   = new TCanvas("combine_runs","combine_runs");
   TLegend     * leg = new TLegend(0.85,0.75,0.99,0.95);
   leg->SetFillColor(0);
   TMultiGraph * mg  = new TMultiGraph();

   for( int i = 0 ; i < fCombinedAsyms->GetEntries() ; i++) {

      InSANEAveragedMeasuredAsymmetry * Asym = (InSANEAveragedMeasuredAsymmetry*)fCombinedAsyms->At(i);
      InSANEMeasuredAsymmetry * A_avg = Asym->Calculate();

      //A_avg->PrintBinnedTable(std::cout) ;
      //std::ofstream fileout(Form("asym/whit/binned/binned_asym%d.dat",i),std::ios_base::trunc );
      //A_avg->PrintBinnedTable(fileout) ;

      TH1          * h1 = A_avg->fAsymmetryVsE;
      TGraphErrors * gr = new TGraphErrors(h1);

      // remove zero statistics data points.
      for( int k = gr->GetN() -1; k>=0 ; k--) {
         Double_t xt, yt;
         gr->GetPoint(k,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(k); }
      }

      if(i>3)gr->SetMarkerStyle(24);
      if(i==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerSize(1.0);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(i==4)mg->Add(gr,"ep");
      leg->AddEntry(gr,Form("Q^{2} = %.1f",A_avg->GetQ2()),"ep");

      fAAsyms->Add(A_avg);
   }

   mg->SetTitle(Form("Cherenkov double track A_{80}, E=%.1fGeV",Ebeam));
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.5,2.0);
   mg->GetYaxis()->SetRangeUser(-1.0,1.0);
   mg->GetXaxis()->SetTitle("x");
   mg->GetYaxis()->SetTitle("");
   mg->Draw("a");
   c->Update();
   leg->Draw();
   
   f->WriteObject(fCombinedAsyms,Form("combined-asym_perp59_double_peak-%d",0)); 

   c->SaveAs(Form("results/combined-asym_perp59_double_peak_%d.png",fileVersion));
   c->SaveAs(Form("results/combined-asym_perp59_double_peak_%d.pdf",fileVersion));

   return(0);
}

