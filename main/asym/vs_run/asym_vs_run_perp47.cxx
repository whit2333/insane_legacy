/*! Creates the plots vs Run Number
 */
Int_t asym_vs_run_perp47(Int_t some_number = 40){

//    gStyle->SetOptStat(0);
//    gROOT->SetStyle("Plain");
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   SANERunManager * rman = (SANERunManager *)InSANERunManager::GetRunManager();

   /// Sets up a canvas
/*   TCanvas *c1 = new TCanvas("asymCanvas","Raw Asymmetries",1200,400);*/
   // c1->SetFillColor(42);
/*   c1->SetGrid();*/
   //c1->Divide(1,2);

   InSANERunSeries * aSeries= new InSANERunSeries("Asym");

   InSANERunPolarization * beamPol = new InSANERunBeamPol("beamPol","Beam Polarization");
   beamPol->fMarkerColor = 8;
   beamPol->fMarkerStyle = 33;

   InSANERunPolarization * targTopPos = new InSANERunTargTopPosPol("topTargPosPol","Top Target Pos. Pol. ");
   targTopPos->fMarkerColor = 2;
   targTopPos->fMarkerStyle = 22;

   InSANERunPolarization * targTopNeg = new InSANERunTargTopNegPol("topTargNegPol","Top Target Neg. Pol. ");
   targTopNeg->fMarkerColor = 4;
   targTopNeg->fMarkerStyle = 22;

   InSANERunPolarization * targBotPos = new InSANERunTargBotPosPol("BottomTargPosPol","Bottom Target Pos. Pol. ");
   targBotPos->fMarkerColor = 2;
   targBotPos->fMarkerStyle = 23;

   InSANERunPolarization * targBotNeg = new InSANERunTargBotNegPol("BottomTargNegPol","Bottom Target Neg. Pol. ");
   targBotNeg->fMarkerColor = 4;
   targBotNeg->fMarkerStyle = 23;

   InSANERunAsymmetry * asym = new InSANERunAsymmetry();
   asym->fMarkerColor = 1;
   asym->fMarkerStyle = 20;
   asym->fMarkerSize = 1.5;

   aSeries->AddRunQuantity(beamPol);
   aSeries->AddRunQuantity(targTopPos);
   aSeries->AddRunQuantity(targTopNeg);
   aSeries->AddRunQuantity(targBotPos);
   aSeries->AddRunQuantity(targBotNeg);
   aSeries->AddRunQuantity(asym);

   /// Build up a run queue to be plotted
/*  aman->BuildQueue("lists/para/run_series1.txt");*/
/*  aman->BuildQueue("lists/para/all_para.txt");*/
/*  aman->BuildQueue("lists/para/all_para.txt");*/
   //aman->BuildQueue2("lists/para/5_9GeV/good_NH3_para59_1.txt");
   //aman->BuildQueue2("lists/para/5_9GeV/good_NH3_para59_2.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/para/4_7GeV/good_NH3.txt");
   aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/perp/4_7GeV/good_NH3_1.txt");

   Double_t avg_asym = 0.0;
   Double_t weighted_mean = 0.0;
   Double_t var_of_mean = 0.0;
   Double_t variance = 0.0;
   int k=0;

   TFile * f = new TFile("data/AsymVsRun.root","UPDATE");
 
   for(int i = 0;i<aman->fRunQueue->size();i++) {
      std::cout << " RUN : " <<  aman->fRunQueue->at(i) << "\n";
      rman->SetRun(aman->fRunQueue->at(i),-1,"Q");
      if( !( rman->GetCurrentRun()->fAnalysisPass < 1) ) {
        /// If the run has been fully analyzed then use it otherwise skip it out. 
        InSANERun * somerun = rman->GetCurrentRun();
        f->cd();

        if(somerun) {
           aSeries->AddRun(new InSANERun(*somerun) );

           asym->SetRun(somerun);
           if(asym->IsValidRun()) {
              avg_asym += asym->GetQuantity();
              variance += asym->GetQuantity()*asym->GetQuantity();
              weighted_mean += asym->GetQuantity()/(TMath::Power(asym->GetQuantityError(),2));
              var_of_mean += 1.0/TMath::Power(asym->GetQuantityError(),2);
              k++;
           }
           std::cout << k << " " << avg_asym << "\n";
        }
         
      }
/*      if(k>40) break;*/
   }

   avg_asym = avg_asym/float(k);
   variance = variance/float(k);
   variance = variance - avg_asym*avg_asym; 
   weighted_mean = weighted_mean/var_of_mean;
   var_of_mean = 1.0/var_of_mean;
   std::cout << "Average Asymmetry : " << avg_asym << "\n";
   std::cout << "variance : " << variance << "\n";
   std::cout << "Weighted Mean Asymmetry : " << weighted_mean << "\n";
   std::cout << "Variance of Mean : " << var_of_mean << "\n";

   aSeries->GenerateGraphs();

   TCanvas * c1 = aSeries->Draw();


   double x[] = {0, k+1};
   double y[] = {avg_asym,avg_asym };
   double ex[] = {0,0};
   double ey[] = {TMath::Sqrt(variance),TMath::Sqrt(variance)};
   TGraphErrors* ge = new TGraphErrors(2, x, y, ex, ey);
   ge->SetFillColor(4);
   ge->SetFillStyle(3004);
   ge->Draw("3,same");

   TF1 * f1 = new TF1("avg_asymmetry",Form("%f",avg_asym),0,73050);
   f1->Draw("same");
   TF1 * f1_p = new TF1("avg_asymmetry_p",Form("%f",avg_asym+TMath::Sqrt(variance)),0,73050);
   f1_p->Draw("same");
   TF1 * f1_m = new TF1("avg_asymmetry_m",Form("%f",avg_asym-TMath::Sqrt(variance)),0,73050);
   f1_m->Draw("same");

   TF1 * f2 = new TF1("weighted_mean_asymmetry",Form("%f",weighted_mean),0,73050);
   f2->SetLineColor(2);
   f2->Draw("same");
   TF1 * f3 = new TF1("upper_mean",Form("%f",weighted_mean+TMath::Sqrt(var_of_mean)),0,73050);
   f3->SetLineColor(2);
   f3->Draw("same");
   TF1 * f4 = new TF1("lower_mean",Form("%f",weighted_mean-TMath::Sqrt(var_of_mean)),0,73050);
   f4->SetLineColor(2);
   f4->Draw("same");

   c1->SaveAs(Form("results/asym_vs_run_perp47-%d.png",some_number));
   c1->SaveAs(Form("results/asym_vs_run_perp47-%d.pdf",some_number));
   c1->SaveAs(Form("results/asym_vs_run_perp47-%d.ps",some_number));

   return(0);
}
