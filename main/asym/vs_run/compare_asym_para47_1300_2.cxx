/*! Creates the plots vs Run Number
 */
Int_t compare_asym_para47_1300_2(Int_t some_number = 510){
   if (gROOT->LoadMacro("asym/vs_run/read_marks.cxx") != 0) {
      Error(weh, "Failed loading read_marks.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("asym/vs_run/read_marks2.cxx") != 0) {
      Error(weh, "Failed loading read_marks2.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("asym/vs_run/read_whits.cxx") != 0) {
      Error(weh, "Failed loading read_whits.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("util/stat_funcs.cxx") != 0) {
      Error(weh, "Failed loading util/stat_funcs.cxx in compiled mode.");
      return -1;
   }
   // --- Data from Mark
   TString mark_file1 = "asym/mark/para_47_900_run.dat";
   std::vector<std::vector<Double_t> > mark1;

   std::map<int,double> map_pt;
   std::map<int,double> map1;
   Int_t retval=read_marks2(mark_file1.Data(),&map_pt, 3);
   Int_t retval=read_marks2(mark_file1.Data(),&map1, 3);
   std::map<int,double>::iterator it; 
   it = map1.find(72999);
   //if(it) std::cout << "run:" << (*it).first << " found " <<std::endl;
   std::cout << "map has size " << map1.size() << std::endl;
   std::cout << "Pt[72999]" << map_pt[72999] << std::endl;

   Int_t retval=read_marks(mark_file1.Data(),&mark1);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << mark1.size() << std::endl;
   Int_t iRun = 0;
   Int_t iy  = 7;
   Int_t iey = 8;
   std::vector<Double_t> zeros;
   std::vector<Double_t> runCount;
   for(int i=0;i<(mark1.at(0)).size(); i++){
      zeros.push_back(0.0);
      runCount.push_back(i+1);
   }
   TGraphErrors * gAsym3 = new TGraphErrors( (mark1.at(0)).size(),&((mark1.at(iRun)).at(0)),&((mark1.at(iy)).at(0)),&zeros[0],&((mark1.at(iey)).at(0)));
   gAsym3->SetMarkerStyle(33);//filled diamond
   gAsym3->SetMarkerColor(kBlue);
   gAsym3->SetLineColor(kBlue);
   //gAsym3->Draw("alp");

   Double_t mean1,error1;
   get_weighted_mean(&(mark1.at(iy)),&(mark1.at(iey)), mean1, error1);
   Double_t avg1,var1;
   get_mean_variance(&(mark1.at(iy)),&(mark1.at(iey)), avg1, var1);


   // --- Data from Mark
   TString mark_file2 = "asym/mark/para_47_1300_run.dat";
   std::vector<std::vector<Double_t> > mark2;
   retval=read_marks(mark_file2.Data(),&mark2);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << mark2.size() << std::endl;
   iRun = 0;
   iy   = 7;
   iey  = 8;
   std::vector<Double_t> zeros;
   std::vector<Double_t> runCount;
   for(int i=0;i<(mark2.at(0)).size(); i++){
      zeros.push_back(0.0);
      runCount.push_back(i+1);
   }
   TGraphErrors * gAsym2 = new TGraphErrors( (mark2.at(0)).size(),&((mark2.at(iRun)).at(0)),&((mark2.at(iy)).at(0)),&zeros[0],&((mark2.at(iey)).at(0)));
   gAsym2->SetMarkerStyle(27);//open diamond
   gAsym2->SetMarkerColor(kBlue);
   gAsym2->SetLineColor(kBlue);
   //gAsym3->Draw("alp");
   
   Double_t mean2,error2;
   get_weighted_mean(&(mark2.at(iy)),&(mark2.at(iey)), mean2, error2);
   Double_t avg2,var2;
   get_mean_variance(&(mark2.at(iy)),&(mark2.at(iey)), avg2, var2);


   // --------------------------------


   // --- My data

   TString whit_file1 = "asym/whit/para_asym4-1.dat";
   std::vector<std::vector<Double_t> > whit1;
   retval=read_whits(whit_file1.Data(),&whit1);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit1.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW1;
   std::vector<Double_t> runCountW1;
   for(int i=0;i<(whit1.at(0)).size(); i++){
      zerosW1.push_back(0.0);
      runCountW1.push_back(i+1);
   }
   TGraphErrors * gAsymW1 = new TGraphErrors( (whit1.at(0)).size(),&((whit1.at(iRun)).at(0)),&((whit1.at(iy)).at(0)),&zerosW1[0],&((whit1.at(iey)).at(0)));
   gAsymW1->SetMarkerStyle(20);// open circle
   gAsymW1->SetMarkerColor(kRed);
   gAsymW1->SetLineColor(kRed);

   Double_t meanW1,errorW1;
   get_weighted_mean(&(whit1.at(iy)),&(whit1.at(iey)), meanW1, errorW1);
   Double_t avgW1,varW1;
   get_mean_variance(&(whit1.at(iy)),&(whit1.at(iey)), avgW1, varW1);

   // ------------ E > 1300 MeV
   TString whit_file2 = "asym/whit/para_asym4-2.dat";
   std::vector<std::vector<Double_t> > whit2;
   retval=read_whits(whit_file2.Data(),&whit2);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit2.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW2;
   std::vector<Double_t> runCountW2;
   for(int i=0;i<(whit2.at(0)).size(); i++){
      zerosW2.push_back(0.0);
      runCountW2.push_back(i+1); }
   TGraphErrors * gAsymW2 = new TGraphErrors( (whit2.at(0)).size(),&((whit2.at(iRun)).at(0)),&((whit2.at(iy)).at(0)),&zerosW2[0],&((whit2.at(iey)).at(0)));
   gAsymW2->SetMarkerStyle(24);
   gAsymW2->SetMarkerColor(kRed);
   gAsymW2->SetLineColor(kRed);
   
   Double_t meanW2,errorW2;
   get_weighted_mean(&(whit2.at(iy)),&(whit2.at(iey)), meanW2, errorW2);
   Double_t avgW2,varW2;
   get_mean_variance(&(whit2.at(iy)),&(whit2.at(iey)), avgW2, varW2);


   // ------------ E > 900 MeV with Cer ADC
   TString whit_file3 = "asym/whit/para_asym4-3.dat";
   std::vector<std::vector<Double_t> > whit3;
   retval=read_whits(whit_file3.Data(),&whit3);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit3.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW3;
   std::vector<Double_t> runCountW3;
   for(int i=0;i<(whit3.at(0)).size(); i++){
      zerosW3.push_back(0.0);
      runCountW3.push_back(i+1); }
   TGraphErrors * gAsymW3 = new TGraphErrors( (whit3.at(0)).size(),&((whit3.at(iRun)).at(0)),&((whit3.at(iy)).at(0)),&zerosW3[0],&((whit3.at(iey)).at(0)));
   gAsymW3->SetMarkerStyle(21);
   gAsymW3->SetMarkerColor(kGreen-6);
   gAsymW3->SetLineColor(kGreen-6);

   // ------------ E > 1300 MeV with Cer ADC
   TString whit_file4 = "asym/whit/para_asym4-4.dat";
   std::vector<std::vector<Double_t> > whit4;
   retval=read_whits(whit_file4.Data(),&whit4);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit4.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW4;
   std::vector<Double_t> runCountW4;
   for(int i=0;i<(whit4.at(0)).size(); i++){
      zerosW4.push_back(0.0);
      runCountW4.push_back(i+1); }
   TGraphErrors * gAsymW4 = new TGraphErrors( (whit4.at(0)).size(),&((whit4.at(iRun)).at(0)),&((whit4.at(iy)).at(0)),&zerosW4[0],&((whit4.at(iey)).at(0)));
   gAsymW4->SetMarkerStyle(21);
   gAsymW4->SetMarkerColor(kGreen-6);
   gAsymW4->SetLineColor(kGreen-6);
   
   Double_t meanW4,errorW4;
   get_weighted_mean(&(whit4.at(iy)),&(whit4.at(iey)), meanW4, errorW4);
   Double_t avgW4,varW4;
   get_mean_variance(&(whit4.at(iy)),&(whit4.at(iey)), avgW4, varW4);



   TCanvas * c = new TCanvas("glcompare_asym","compare_asym");

   // ----------
   TMultiGraph * mg = new TMultiGraph();
   mg->Add(gAsym2,"p");
   //mg->Add(gAsym3,"p");
   //mg->Add(gAsymW1,"p");
   mg->Add(gAsymW2,"p");
   //mg->Add(gAsymW3,"p");
   //mg->Add(gAsymW4,"p");
   mg->Draw("a");
   mg->GetXaxis()->SetRangeUser(72980,73045);
   c->Update();

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->AddEntry(gAsym2,"E>1300","lp");
   //leg->AddEntry(gAsym3,Form("E>900, A=%.3f #pm %.3f",mean1,error1),"lp");
   leg->AddEntry(gAsymW2,"E>1300","lp");
   //leg->AddEntry(gAsymW1,Form("E>900, A=%.3f #pm %.3f",meanW1,errorW1),"lp");
   //leg->AddEntry(gAsymW3,"E>900 w/ CER ADC cut","lp");
   //leg->AddEntry(gAsymW4,"E>1300 w/ CER ADC cut","lp");

   double x_m1[]  = {0,73050};
   double y_m1[]  = {mean2,mean2 };
   double ex_m1[] = {0,0};
   double ey_m1[] = {error2,error2};
   double ay_m1[]  = {avg2,avg2 };
   double vx_m1[] = {0,0};
   double vy_m1[] = {TMath::Sqrt(var2),TMath::Sqrt(var2)};
   // mean and variance
   Int_t icol1    = 4000;
   TColor *col1   = gROOT->GetColor(icol1);
   col1->SetAlpha(0.2);
   TGraphErrors* ge = new TGraphErrors(2, x_m1, ay_m1, vx_m1, vy_m1);
   ge->SetFillColor(icol1);
   ge->SetLineColor(icol1);
   ge->Draw("3,same");
   if(leg) leg->AddEntry(ge,Form("Jones #mu = %1.6f #pm %1.6f",avg2,TMath::Sqrt(var2)),"f");

   // weighted mean and error of the mean
   icol1    = 4001;
   col1   = gROOT->GetColor(icol1);
   col1->SetAlpha(0.3);
   TGraphErrors* ge2 = new TGraphErrors(2, x_m1, y_m1, ex_m1, ey_m1);
   ge2->SetFillColor(icol1);
   ge2->SetLineColor(icol1);
   ge2->Draw("3,same");
   if(leg) leg->AddEntry(ge2,Form("Jones #bar{#mu} = %1.6f #pm %1.6f",mean2,error2),"f");

   double x_w1[]  = {0,73050};
   double y_w1[]  = {meanW2,meanW2 };
   double ex_w1[] = {0,0};
   double ey_w1[] = {errorW2,errorW2};
   double ay_w1[]  = {avgW2,avgW2 };
   double vx_w1[] = {0,0};
   double vy_w1[] = {TMath::Sqrt(varW2),TMath::Sqrt(varW2)};
   // mean and variance
   icol1    = 4002;
   col1   = gROOT->GetColor(icol1);
   col1->SetAlpha(0.2);
   TGraphErrors* ge3 = new TGraphErrors(2, x_w1, ay_w1, vx_w1, vy_w1);
   ge3->SetFillColor(icol1);
   ge3->SetLineColor(icol1);
   ge3->Draw("3,same");
   if(leg) leg->AddEntry(ge3,Form("Armstrong #mu = %1.6f #pm %1.6f",avgW2,TMath::Sqrt(varW2)),"f");

   // weighted mean and error of the mean
   icol1    = 4003;
   col1   = gROOT->GetColor(icol1);
   col1->SetAlpha(0.3);
   TGraphErrors* ge4 = new TGraphErrors(2, x_w1, y_w1, ex_w1, ey_w1);
   ge4->SetFillColor(icol1);
   ge4->SetLineColor(icol1);
   ge4->Draw("3,same");
   if(leg) leg->AddEntry(ge4,Form("Armstrong #bar{#mu} = %1.6f #pm %1.6f",meanW2,errorW2),"f");

   leg->Draw();

   c->SaveAs(Form("results/compare_asym_para47_1300_%d.png",some_number));
   c->SaveAs(Form("results/compare_asym_para47_1300_%d.pdf",some_number));

   return(0);
}
