/*! Creates the plots vs Run Number
 */
Int_t compare_asym_para47_bcparts(Int_t some_number = 802){
   if (gROOT->LoadMacro("asym/vs_run/read_marks.cxx") != 0) {
      Error(weh, "Failed loading read_marks.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("asym/vs_run/read_marks2.cxx") != 0) {
      Error(weh, "Failed loading read_marks.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("asym/vs_run/read_whits.cxx") != 0) {
      Error(weh, "Failed loading read_whits.cxx in compiled mode.");
      return -1;
   }
   // --- Data from Mark
   TString mark_file1 = "asym/mark/para_47_900_run.dat";
   std::vector<std::vector<Double_t> > mark1;

   std::map<int,double> map_pt;
   std::map<int,double> map1;
   Int_t retval=read_marks2(mark_file1.Data(),&map_pt, 3);
   Int_t retval=read_marks2(mark_file1.Data(),&map1, 3);
   std::map<int,double>::iterator it; 
   it = map1.find(72999);
   //if(it) std::cout << "run:" << (*it).first << " found " <<std::endl;
   std::cout << "map has size " << map1.size() << std::endl;
   std::cout << "Pt[72999]" << map_pt[72999] << std::endl;

   Int_t retval=read_marks(mark_file1.Data(),&mark1);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << mark1.size() << std::endl;
   Int_t iRun = 0;
   Int_t iy  = 7;
   Int_t iey = 8;
   std::vector<Double_t> zeros;
   std::vector<Double_t> runCount;
   for(int i=0;i<(mark1.at(0)).size(); i++){
      zeros.push_back(0.0);
      runCount.push_back(i+1);
   }
   TGraphErrors * gAsym3 = new TGraphErrors( (mark1.at(0)).size(),&((mark1.at(iRun)).at(0)),&((mark1.at(iy)).at(0)),&zeros[0],&((mark1.at(iey)).at(0)));
   gAsym3->SetMarkerStyle(33);//filled diamond
   gAsym3->SetMarkerColor(kBlue);
   gAsym3->SetLineColor(kBlue);
   //gAsym3->Draw("alp");

   // --- Data from Mark
   TString mark_file2 = "asym/mark/para_47_1300_run.dat";
   std::vector<std::vector<Double_t> > mark2;
   retval=read_marks(mark_file2.Data(),&mark2);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << mark2.size() << std::endl;
   iRun = 0;
   iy   = 7;
   iey  = 8;
   std::vector<Double_t> zeros;
   std::vector<Double_t> runCount;
   for(int i=0;i<(mark2.at(0)).size(); i++){
      zeros.push_back(0.0);
      runCount.push_back(i+1);
   }
   TGraphErrors * gAsym2 = new TGraphErrors( (mark2.at(0)).size(),&((mark2.at(iRun)).at(0)),&((mark2.at(iy)).at(0)),&zeros[0],&((mark2.at(iey)).at(0)));
   gAsym2->SetMarkerStyle(27);//open diamond
   gAsym2->SetMarkerColor(kBlue);
   gAsym2->SetLineColor(kBlue);
   //gAsym3->Draw("alp");
   // --------------------------------


   // --- RCS, E > 900 Mev

   TString whit_file1 = "asym/whit/para_asym_rcs1.dat";
   std::vector<std::vector<Double_t> > whit1;
   retval=read_whits(whit_file1.Data(),&whit1);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit1.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW1;
   std::vector<Double_t> runCountW1;
   for(int i=0;i<(whit1.at(0)).size(); i++){
      zerosW1.push_back(0.0);
      runCountW1.push_back(i+1);
   }
   TGraphErrors * gAsymW1 = new TGraphErrors( (whit1.at(0)).size(),&((whit1.at(iRun)).at(0)),&((whit1.at(iy)).at(0)),&zerosW1[0],&((whit1.at(iey)).at(0)));
   gAsymW1->SetMarkerStyle(20);// open circle
   gAsymW1->SetMarkerColor(kRed);
   gAsymW1->SetLineColor(kRed);

   // ------------ E > 1300 MeV
   TString whit_file2 = "asym/whit/para_asym_rcs2.dat";
   std::vector<std::vector<Double_t> > whit2;
   retval=read_whits(whit_file2.Data(),&whit2);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit2.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW2;
   std::vector<Double_t> runCountW2;
   for(int i=0;i<(whit2.at(0)).size(); i++){
      zerosW2.push_back(0.0);
      runCountW2.push_back(i+1); }
   TGraphErrors * gAsymW2 = new TGraphErrors( (whit2.at(0)).size(),&((whit2.at(iRun)).at(0)),&((whit2.at(iy)).at(0)),&zerosW2[0],&((whit2.at(iey)).at(0)));
   gAsymW2->SetMarkerStyle(24);
   gAsymW2->SetMarkerColor(kRed);
   gAsymW2->SetLineColor(kRed);

   // ------------ Protvino, E > 900 MeV 
   TString whit_file3 = "asym/whit/para_asym_prot3.dat";
   std::vector<std::vector<Double_t> > whit3;
   retval=read_whits(whit_file3.Data(),&whit3);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit3.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW3;
   std::vector<Double_t> runCountW3;
   for(int i=0;i<(whit3.at(0)).size(); i++){
      zerosW3.push_back(0.0);
      runCountW3.push_back(i+1); }
   TGraphErrors * gAsymW3 = new TGraphErrors( (whit3.at(0)).size(),&((whit3.at(iRun)).at(0)),&((whit3.at(iy)).at(0)),&zerosW3[0],&((whit3.at(iey)).at(0)));
   gAsymW3->SetMarkerStyle(21);
   gAsymW3->SetMarkerColor(kGreen-6);
   gAsymW3->SetLineColor(kGreen-6);

   // ------------ E > 1300 MeV 
   TString whit_file4 = "asym/whit/para_asym_prot4.dat";
   std::vector<std::vector<Double_t> > whit4;
   retval=read_whits(whit_file4.Data(),&whit4);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit4.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW4;
   std::vector<Double_t> runCountW4;
   for(int i=0;i<(whit4.at(0)).size(); i++){
      zerosW4.push_back(0.0);
      runCountW4.push_back(i+1); }
   TGraphErrors * gAsymW4 = new TGraphErrors( (whit4.at(0)).size(),&((whit4.at(iRun)).at(0)),&((whit4.at(iy)).at(0)),&zerosW4[0],&((whit4.at(iey)).at(0)));
   gAsymW4->SetMarkerStyle(25);
   gAsymW4->SetMarkerColor(kGreen-6);
   gAsymW4->SetLineColor(kGreen-6);


   TCanvas * c = new TCanvas("compare_asym","compare_asym");

   // ----------
   TMultiGraph * mg = new TMultiGraph();
   mg->Add(gAsym2,"p");
   //mg->Add(gAsym3,"p");
   //mg->Add(gAsymW1,"p");
   mg->Add(gAsymW2,"p");
   //mg->Add(gAsymW3,"p");
   mg->Add(gAsymW4,"p");
   mg->Draw("a");
   mg->GetXaxis()->SetRangeUser(72980,73045);
   c->Update();

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   //leg->AddEntry(gAsym2,"E>1300","lp");
   leg->AddEntry(gAsym3,"E>900","lp");
   leg->AddEntry(gAsymW2,"RCS E>1300","lp");
   leg->AddEntry(gAsymW1,"RCS E>900","lp");
   leg->AddEntry(gAsymW3,"Protvino E>900 ","lp");
   leg->AddEntry(gAsymW4,"Protvino E>1300","lp");
   leg->Draw();

   c->SaveAs(Form("results/compare_asym_para47_bcparts_%d.png",some_number));
   c->SaveAs(Form("results/compare_asym_para47_bcparts_%d.pdf",some_number));

   return(0);
}
