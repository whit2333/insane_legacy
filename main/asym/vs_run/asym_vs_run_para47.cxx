/*! Creates the plots vs Run Number
 */
Int_t asym_vs_run_para47(Int_t some_number = 21){

   // --- Data from James
   std::ifstream infile("asym/james/ext_asym_standard.txt.split.4.7-para_asym_only.txt");
   std::vector<Double_t> runNumbers; 
   std::vector<Double_t> runNumbersErrors; 
   std::vector<Double_t> runAsymmetry; 
   std::vector<Double_t> runAsymmetryError; 
   char line[180];
   if ( !infile.is_open() ) {
      return(-1);
   } 
   infile.getline(line,180);
   Int_t rn;
   Int_t count=0;
   Double_t iA, iAe;
   while ( infile.good() ){
     infile >> rn >> iA >> iAe ; 
     count++;
     runNumbers.push_back(count+0.1);
     runNumbersErrors.push_back(0);
     runAsymmetry.push_back(iA);
     runAsymmetryError.push_back(iAe);
   }
   TGraphErrors * gAsym = new TGraphErrors(runNumbers.size(),&runNumbers[0],&runAsymmetry[0],&runNumbersErrors[0],&runAsymmetryError[0]);
   gAsym->SetMarkerStyle(29);
   gAsym->SetMarkerColor(kMagenta);
   gAsym->SetLineColor(kMagenta);
   //gAsym->Draw("alp");

   // --- Data from Mark
   std::ifstream infile("asym/mark/para_47_1300_run_asyms.dat");
   std::vector<Double_t> runNumbers2; 
   std::vector<Double_t> runNumbersErrors2; 
   std::vector<Double_t> runAsymmetry2; 
   std::vector<Double_t> runAsymmetryError2; 
   if ( !infile.is_open() ) {
      return(-1);
   } 
   rn    = 0;
   count =0;
   while ( infile.good() ){
     infile >> rn >> iA >> iAe ; 
     count++;
     runNumbers2.push_back(count+0.2);
     runNumbersErrors2.push_back(0);
     runAsymmetry2.push_back(iA);
     runAsymmetryError2.push_back(iAe);
   }
   TGraphErrors * gAsym2 = new TGraphErrors(runNumbers2.size(),&runNumbers2[0],&runAsymmetry2[0],&runNumbersErrors2[0],&runAsymmetryError2[0]);
   gAsym2->SetMarkerStyle(27);
   gAsym2->SetMarkerColor(kBlue-7);
   gAsym2->SetLineColor(kBlue-7);

   // --- Data from Mark
   std::ifstream infile("asym/mark/para_47_900_run_asyms.dat");
   std::vector<Double_t> runNumbers3; 
   std::vector<Double_t> runNumbersErrors3; 
   std::vector<Double_t> runAsymmetry3; 
   std::vector<Double_t> runAsymmetryError3; 
   if ( !infile.is_open() ) {
      return(-1);
   } 
   rn    = 0;
   count =0;
   while ( infile.good() ){
     infile >> rn >> iA >> iAe ; 
     count++;
     runNumbers3.push_back(count);
     runNumbersErrors3.push_back(0);
     runAsymmetry3.push_back(iA);
     runAsymmetryError3.push_back(iAe);
   }
   TGraphErrors * gAsym3 = new TGraphErrors(runNumbers3.size(),&runNumbers3[0],&runAsymmetry3[0],&runNumbersErrors3[0],&runAsymmetryError3[0]);
   gAsym3->SetMarkerStyle(33);
   gAsym3->SetMarkerColor(kBlue-7);
   gAsym3->SetLineColor(kBlue-7);
   // --------------------------------

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   SANERunManager * rman = (SANERunManager *)InSANERunManager::GetRunManager();

   /// Sets up a canvas
/*   TCanvas *c1 = new TCanvas("asymCanvas","Raw Asymmetries",1200,400);*/
   // c1->SetFillColor(42);
/*   c1->SetGrid();*/
   //c1->Divide(1,2);
   //TFile * f = new TFile("data/AsymVsRunTemp.root","RECREATE");

   InSANERunSeries * aSeries= new InSANERunSeries("Asym");

   InSANERunPolarization * beamPol = new InSANERunBeamPol("beamPol","Beam Polarization");
   beamPol->fMarkerColor = 8;
   beamPol->fMarkerStyle = 33;

   InSANERunPolarization * targTopPos = new InSANERunTargTopPosPol("topTargPosPol","Top Target Pos. Pol. ");
   targTopPos->fMarkerColor = 2;
   targTopPos->fMarkerStyle = 22;

   InSANERunPolarization * targTopNeg = new InSANERunTargTopNegPol("topTargNegPol","Top Target Neg. Pol. ");
   targTopNeg->fMarkerColor = 4;
   targTopNeg->fMarkerStyle = 22;

   InSANERunPolarization * targBotPos = new InSANERunTargBotPosPol("BottomTargPosPol","Bottom Target Pos. Pol. ");
   targBotPos->fMarkerColor = 2;
   targBotPos->fMarkerStyle = 23;

   InSANERunPolarization * targBotNeg = new InSANERunTargBotNegPol("BottomTargNegPol","Bottom Target Neg. Pol. ");
   targBotNeg->fMarkerColor = 4;
   targBotNeg->fMarkerStyle = 23;

   InSANERunAsymmetry * asym = new InSANERunAsymmetry();
   asym->fMarkerColor = 1;
   asym->fMarkerStyle = 20;
   asym->fMarkerSize = 1.5;

   InSANERawRunAsymmetry * asym1 = new InSANERawRunAsymmetry("run-asym1");
   asym1->SetNameTitle("runasym1","E>900");
   asym1->fMarkerColor = 3;
   asym1->fMarkerStyle = 20;
   asym1->fMarkerSize = 1.5;

   //aSeries->AddRunQuantity(beamPol);
   //aSeries->AddRunQuantity(targTopPos);
   //aSeries->AddRunQuantity(targTopNeg);
   //aSeries->AddRunQuantity(targBotPos);
   //aSeries->AddRunQuantity(targBotNeg);
   aSeries->AddRunQuantity(asym);
   aSeries->AddRunQuantity(asym1);

   /// Build up a run queue to be plotted
/*  aman->BuildQueue("lists/para/run_series1.txt");*/
/*  aman->BuildQueue("lists/para/all_para.txt");*/
/*  aman->BuildQueue("lists/para/all_para.txt");*/
   //aman->BuildQueue2("lists/para/5_9GeV/good_NH3_para59_1.txt");
   //aman->BuildQueue2("lists/para/5_9GeV/good_NH3_para59_2.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/good_NH3.txt");
   aman->BuildQueue2("lists/para/4_7GeV/good_NH3_2.txt");
   //aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/perp/4_7GeV/good_NH3_1.txt");

   Double_t avg_asym = 0.0;
   Double_t weighted_mean = 0.0;
   Double_t var_of_mean = 0.0;
   Double_t variance = 0.0;
   int k=0;
   int Nrun=0;

 
   for(int i = 0;i<aman->fRunQueue->size();i++) {
      std::cout << " RUN : " <<  aman->fRunQueue->at(i) << "\n";
      Nrun++;
      rman->SetRun(aman->fRunQueue->at(i),-1,"Q");
      if( !( rman->GetCurrentRun()->fAnalysisPass < 1) ) {
        /// If the run has been fully analyzed then use it otherwise skip it out. 
        InSANERun * somerun = rman->GetCurrentRun();

        if(somerun) {
           aSeries->AddRun(new InSANERun(*somerun) );
           //aSeries->AddRun(somerun);

           asym->SetRun(somerun);
           if(asym->IsValidRun()) {
              avg_asym += asym->GetQuantity();
              variance += asym->GetQuantity()*asym->GetQuantity();
              weighted_mean += asym->GetQuantity()/(TMath::Power(asym->GetQuantityError(),2));
              var_of_mean += 1.0/TMath::Power(asym->GetQuantityError(),2);
              k++;
           }
           std::cout << k << " " << avg_asym << "\n";
        }
         
      }
      rman->CloseRun();
   }
   //f->cd();
   //aSeries->Write("run-series");
   //f->Flush();

   avg_asym = avg_asym/float(k);
   variance = variance/float(k);
   variance = variance - avg_asym*avg_asym; 
   weighted_mean = weighted_mean/var_of_mean;
   var_of_mean = 1.0/var_of_mean;

   std::cout << "Average Asymmetry : " << avg_asym << "\n";
   std::cout << "variance : " << variance << "\n";
   std::cout << "Weighted Mean Asymmetry : " << weighted_mean << "\n";
   std::cout << "Variance of Mean : " << var_of_mean << "\n";

   std::cout << " Generating Graphs "  << std::endl;

   aSeries->GenerateGraphs();

   TLegend * leg = aSeries->fLegend;

   TCanvas * c1 =  new TCanvas("asym_vs_run_para47","asym_vs_run_para47");
   aSeries->fMultiGraph.SetMinimum(-1.0);
   aSeries->fMultiGraph.SetMaximum(1.0);
   aSeries->Draw();


   double x[] = {0,Nrun+1};
   double y[] = {avg_asym,avg_asym };
   double ex[] = {0,0};
   double ey[] = {TMath::Sqrt(variance),TMath::Sqrt(variance)};

   TGraphErrors* ge = new TGraphErrors(2, x, y, ex, ey);
   ge->SetFillColor(kBlue-10);
   ge->Draw("3,same");
   if(leg) leg->AddEntry(ge,Form("#sigma_{#mu} = %1.6f",var_of_mean),"f");

   double y2[] = {weighted_mean,weighted_mean };
   double ey2[] = {TMath::Sqrt(var_of_mean),TMath::Sqrt(var_of_mean)};

   TGraphErrors* ge2 = new TGraphErrors(2, x, y2, ex, ey2);
   ge2->SetFillColor(kRed-10);
   ge2->Draw("3,same");
   if(leg) leg->AddEntry(ge2,Form("#sigma = %1.3f",TMath::Sqrt(variance)),"f");


   //TF1 * f1 = new TF1("avg_asymmetry",Form("%f",avg_asym),0,73050);
   ////f1->Draw("same");
   //TF1 * f1_p = new TF1("avg_asymmetry_p",Form("%f",avg_asym+TMath::Sqrt(variance)),0,73050);
   ////f1_p->Draw("same");
   //TF1 * f1_m = new TF1("avg_asymmetry_m",Form("%f",avg_asym-TMath::Sqrt(variance)),0,73050);
   ////f1_m->Draw("same");

   TF1 * f2 = new TF1("weighted_mean_asymmetry",Form("%f",weighted_mean),0,73050);
   f2->SetLineColor(kRed);
   f2->Draw("same");

   //TF1 * f3 = new TF1("upper_mean",Form("%f",weighted_mean+TMath::Sqrt(var_of_mean)),0,73050);
   //f3->SetLineColor(kRed-9);
   ////f3->Draw("same");

   //TF1 * f4 = new TF1("lower_mean",Form("%f",weighted_mean-TMath::Sqrt(var_of_mean)),0,73050);
   //f4->SetLineColor(kRed-9);
   ////f4->Draw("same");

   if(leg) leg->AddEntry(f2,Form("#mu = %1.3f",weighted_mean),"l");
   if(leg) leg->AddEntry(gAsym,"UVa Asymmetry","lp");
   if(leg) leg->AddEntry(gAsym2,"Jones E > 1300 Asymmetry","lp");
   if(leg) leg->AddEntry(gAsym3,"Jones E > 900 Asymmetry","lp");

   aSeries->Draw();
   gAsym->Draw("p,same");
   gAsym2->Draw("p,same");
   gAsym3->Draw("p,same");

   c1->SaveAs(Form("results/asym_vs_run-%d.png",some_number));
   c1->SaveAs(Form("results/asym_vs_run-%d.pdf",some_number));
   c1->SaveAs(Form("results/asym_vs_run-%d.ps",some_number));

   return(0);
}
