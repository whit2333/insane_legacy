/*! Creates the plots vs Run Number
 */
Int_t compare_para47(Int_t some_number = 42){
   if (gROOT->LoadMacro("asym/vs_run/read_marks.cxx") != 0) {
      Error(weh, "Failed loading read_marks.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("asym/vs_run/read_whits.cxx") != 0) {
      Error(weh, "Failed loading read_whits.cxx in compiled mode.");
      return -1;
   }

   // --- Data from James
   std::ifstream infile("asym/james/ext_asym_standard.txt.split.4.7-para_asym_only.txt");
   std::vector<Double_t> runNumbers; 
   std::vector<Double_t> runNumbersErrors; 
   std::vector<Double_t> runAsymmetry; 
   std::vector<Double_t> runAsymmetryError; 
   char line[180];
   if ( !infile.is_open() ) {
      return(-1);
   } 
   infile.getline(line,180);
   Int_t rn;
   Int_t count=0;
   Double_t iA, iAe;
   while ( infile.good() ){
     infile >> rn >> iA >> iAe ; 
     count++;
     runNumbers.push_back(count+0.1);
     runNumbersErrors.push_back(0);
     runAsymmetry.push_back(iA);
     runAsymmetryError.push_back(iAe);
   }
   TGraphErrors * gAsym = new TGraphErrors(runNumbers.size(),&runNumbers[0],&runAsymmetry[0],&runNumbersErrors[0],&runAsymmetryError[0]);
   gAsym->SetMarkerStyle(29);
   gAsym->SetMarkerColor(kMagenta);
   gAsym->SetLineColor(kMagenta);
   //gAsym->Draw("alp");

   // --- Data from Mark
   std::ifstream infile("asym/mark/para_47_1300_run_asyms.dat");
   std::vector<Double_t> runNumbers2; 
   std::vector<Double_t> runNumbersErrors2; 
   std::vector<Double_t> runAsymmetry2; 
   std::vector<Double_t> runAsymmetryError2; 
   if ( !infile.is_open() ) {
      return(-1);
   } 
   rn    = 0;
   count =0;
   while ( infile.good() ){
     infile >> rn >> iA >> iAe ; 
     count++;
     runNumbers2.push_back(count+0.2);
     runNumbersErrors2.push_back(0);
     runAsymmetry2.push_back(iA);
     runAsymmetryError2.push_back(iAe);
   }
   TGraphErrors * gAsym2 = new TGraphErrors(runNumbers2.size(),&runNumbers2[0],&runAsymmetry2[0],&runNumbersErrors2[0],&runAsymmetryError2[0]);
   gAsym2->SetMarkerStyle(27);
   gAsym2->SetMarkerColor(kBlue-7);
   gAsym2->SetLineColor(kBlue-7);

   // --- Data from Mark
   std::ifstream infile("asym/mark/para_47_900_run_asyms.dat");
   std::vector<Double_t> runNumbers3; 
   std::vector<Double_t> runNumbersErrors3; 
   std::vector<Double_t> runAsymmetry3; 
   std::vector<Double_t> runAsymmetryError3; 
   if ( !infile.is_open() ) {
      return(-1);
   } 
   rn    = 0;
   count =0;
   while ( infile.good() ){
     infile >> rn >> iA >> iAe ; 
     count++;
     runNumbers3.push_back(count);
     runNumbersErrors3.push_back(0);
     runAsymmetry3.push_back(iA);
     runAsymmetryError3.push_back(iAe);
   }
   TGraphErrors * gAsym3 = new TGraphErrors(runNumbers3.size(),&runNumbers3[0],&runAsymmetry3[0],&runNumbersErrors3[0],&runAsymmetryError3[0]);
   gAsym3->SetMarkerStyle(33);
   gAsym3->SetMarkerColor(kBlue-7);
   gAsym3->SetLineColor(kBlue-7);
   // --------------------------------

   // --- My data

   TString whit_file1 = "asym/whit/para_asym1.dat";
   std::vector<std::vector<Double_t> > whit1;
   retval=read_whits(whit_file1.Data(),&whit1);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit1.size() << std::endl;
   Int_t iRun = 0;
   Int_t iy   = 10;
   Int_t iey  = 11;
   std::vector<Double_t> zerosW1;
   std::vector<Double_t> runCountW1;
   for(int i=0;i<(whit1.at(0)).size(); i++){
      zerosW1.push_back(0.0);
      runCountW1.push_back(i+1);
   }
   TGraphErrors * gAsymW1 = new TGraphErrors( (whit1.at(0)).size(),&(runCountW1.at(0)),&((whit1.at(iy)).at(0)),&zerosW1[0],&((whit1.at(iey)).at(0)));
   gAsymW1->SetMarkerStyle(24);
   gAsymW1->SetMarkerColor(kRed);
   gAsymW1->SetLineColor(kRed);

   TString whit_file2 = "asym/whit/para_asym2.dat";
   std::vector<std::vector<Double_t> > whit2;
   retval=read_whits(whit_file2.Data(),&whit2);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit2.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW2;
   std::vector<Double_t> runCountW2;
   for(int i=0;i<(whit2.at(0)).size(); i++){
      zerosW2.push_back(0.0);
      runCountW2.push_back(i+1); }
   TGraphErrors * gAsymW2 = new TGraphErrors( (whit2.at(0)).size(),&(runCountW2.at(0)),&((whit2.at(iy)).at(0)),&zerosW2[0],&((whit2.at(iey)).at(0)));
   gAsymW2->SetMarkerStyle(20);
   gAsymW2->SetMarkerColor(kRed);
   gAsymW2->SetLineColor(kRed);

   TMultiGraph * mg = new TMultiGraph();
   mg->Add(gAsym,"p");
   mg->Add(gAsym2,"p");
   mg->Add(gAsym3,"p");
   mg->Add(gAsymW1,"p");
   mg->Add(gAsymW2,"p");
   mg->Draw("a");

   return(0);
}
