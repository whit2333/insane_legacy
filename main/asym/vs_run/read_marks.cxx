Int_t read_marks(const char * file, std::vector<std::vector<Double_t>> * data, const Int_t nCols= 9  ) {

   std::ifstream infile(file);
   if ( !infile.is_open() ) {
      return(-1);
   }
   if(!data) return(-2);

   std::vector<Double_t> * cols[nCols];
   Double_t vals[nCols];
   for( int i = 0; i < nCols;i++) {
      cols[i] = new std::vector<Double_t>;
   }

   while ( infile.good() ) {
      for(int i = 0; i< nCols ; i++){
         infile >> vals[i];
         if(infile.eof()) break;
         cols[i]->push_back(vals[i]);
         std::cout << vals[i] << " ";
      }
         std::cout  << std::endl;
   }
   data->clear();
   for( int i = 0; i < nCols;i++) {
      //cols[i]->pop_back();
      //cols[i] = new std::vector<Double_t>;
      data->push_back(*(cols[i]));
      //std::cout << i << std::endl;
   }

    
   return(0);
}


