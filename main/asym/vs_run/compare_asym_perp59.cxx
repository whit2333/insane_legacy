/*! Creates the plots vs Run Number
 */
Int_t compare_asym_perp59(Int_t some_number = 81 ){

   if (gROOT->LoadMacro("asym/vs_run/read_whits.cxx") != 0) {
      Error(weh, "Failed loading read_whits.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("util/read_data_table_column_to_map.cxx") != 0) {
      Error(weh, "Failed loading util/read_data_table_column_to_map.cxx in compiled mode.");
      return -1;
   }
   //gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);
   Int_t iRun = 0;
   Int_t iy  = 7;
   Int_t iey = 8;

   // --- My data

   TString whit_file1 = "asym/whit/perp59_asym-1.dat";

   std::map<int,double> map_pt1;
   std::map<int,double> map_pb1;
   int retval = read_data_table_column_to_map(whit_file1.Data(),&map_pt1,8,17);
   retval     = read_data_table_column_to_map(whit_file1.Data(),&map_pb1,9,17);

   std::vector<std::vector<Double_t> > whit1;
   retval = read_whits(whit_file1.Data(),&whit1,17,71000,73050);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit1.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW1;
   std::vector<Double_t> runCountW1;
   std::vector<Double_t> ptW1;
   std::vector<Double_t> pbW1;
   for(int i=0;i<(whit1.at(0)).size(); i++){
      int runnum = whit1.at(iRun).at(i);
      ptW1.push_back( map_pt1[runnum]/4.0);
      pbW1.push_back( map_pb1[runnum]/4.0);
      zerosW1.push_back(0.0);
      //runCountW1.push_back(i+1);
      runCountW1.push_back(runnum);
   }
   TGraph * gPTW1 = new TGraph( (whit1.at(0)).size(), &runCountW1[0]/*((whit1.at(iRun)).at(0))*/, &ptW1[0] );
   gPTW1->SetMarkerStyle(25);
   gPTW1->SetMarkerColor(kMagenta);
   TGraph * gPBW1 = new TGraph( (whit1.at(0)).size(), &runCountW1[0]/*&((whit1.at(iRun)).at(0))*/, &pbW1[0] );
   gPBW1->SetMarkerStyle(26);
   gPBW1->SetMarkerColor(kGreen);

   TGraphErrors * gAsymW1 = new TGraphErrors( (whit1.at(0)).size(), &runCountW1[0]/*&((whit1.at(iRun)).at(0))*/, &((whit1.at(iy)).at(0)),&zerosW1[0],&((whit1.at(iey)).at(0)));
   gAsymW1->SetMarkerStyle(20);// open circle
   gAsymW1->SetMarkerColor(2000);
   gAsymW1->SetLineColor(2000);
   std::cout << " Data points: " << whit1.size() << std::endl;

   // ------------  
   TString whit_file2 = "asym/whit/perp59_asym-2.dat";

   std::map<int,double> map_x2;
   retval     = read_data_table_column_to_map(whit_file1.Data(),&map_x2,15,17);

   std::vector<std::vector<Double_t> > whit2;
   retval=read_whits(whit_file2.Data(),&whit2,17,71000,73050);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit2.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW2;
   std::vector<Double_t> runCountW2;
   std::vector<Double_t> xW2;
   for(int i=0;i<(whit2.at(0)).size(); i++){
      int runnum = whit1.at(iRun).at(i);
      xW2.push_back( map_x2[runnum]);
      whit2.at(0).at(i) += 0.1;
      zerosW2.push_back(0.0);
      //runCountW2.push_back(i+1+0.2);
      runCountW2.push_back(runnum+0.1);
   }

   TGraph * gxW2 = new TGraph( (whit2.at(0)).size(), &runCountW2[0]/*&((whit2.at(iRun)).at(0))*/, &xW2[0] );
   gxW2->SetMarkerStyle(33);
   gxW2->SetMarkerColor(1);

   TGraphErrors * gAsymW2 = new TGraphErrors( (whit2.at(0)).size(), &runCountW2[0]/*&((whit2.at(iRun)).at(0))*/,&((whit2.at(iy)).at(0)),&zerosW2[0],&((whit2.at(iey)).at(0)));
   gAsymW2->SetMarkerStyle(20);
   gAsymW2->SetMarkerColor(2001);
   gAsymW2->SetLineColor(2001);

   // ------------ 
   TString whit_file3 = "asym/whit/perp59_asym-3.dat";
   std::vector<std::vector<Double_t> > whit3;
   retval=read_whits(whit_file3.Data(),&whit3,17,71000,73050);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit3.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW3;
   std::vector<Double_t> runCountW3;
   for(int i=0;i<(whit3.at(0)).size(); i++){
      whit3.at(0).at(i) += 0.2;
      zerosW3.push_back(0.0);
      //runCountW3.push_back(i+1+0.4);
      runCountW3.push_back(whit3.at(0).at(i));
   }
   TGraphErrors * gAsymW3 = new TGraphErrors( (whit3.at(0)).size(), &runCountW3[0]/*&((whit3.at(iRun)).at(0))*/,&((whit3.at(iy)).at(0)),&zerosW3[0],&((whit3.at(iey)).at(0)));
   gAsymW3->SetMarkerStyle(20);
   gAsymW3->SetMarkerColor(2002);
   gAsymW3->SetLineColor(2002);

   // -----------
   TString whit_file4 = "asym/whit/perp59_asym-4.dat";
   std::vector<std::vector<Double_t> > whit4;
   retval=read_whits(whit_file4.Data(),&whit4,17,71000,73050);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << whit4.size() << std::endl;
   iRun = 0;
   iy   = 10;
   iey  = 11;
   std::vector<Double_t> zerosW4;
   std::vector<Double_t> runCountW4;
   for(int i=0;i<(whit4.at(0)).size(); i++){
      whit4.at(0).at(i) += 0.3;
      zerosW4.push_back(0.0);
      runCountW4.push_back(whit4.at(0).at(i)); 
      //runCountW4.push_back(i+1+0.6);
   }
   TGraphErrors * gAsymW4 = new TGraphErrors( (whit4.at(0)).size(), &runCountW4[0]/*&((whit4.at(iRun)).at(0))*/,&((whit4.at(iy)).at(0)),&zerosW4[0],&((whit4.at(iey)).at(0)));
   gAsymW4->SetMarkerStyle(20);
   gAsymW4->SetMarkerColor(2003);
   gAsymW4->SetLineColor(2003);

   //---------------------------------------------------
   TCanvas * c = new TCanvas("compare_asym","compare_asym");

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   //leg->AddEntry(gAsym2,"E>1300","lp");
   //leg->AddEntry(gAsym3,"E>900","lp");
   leg->AddEntry(gAsymW1,"E<700","lp");
   leg->AddEntry(gAsymW2,"E<900","lp");
   leg->AddEntry(gAsymW3,"E<1100 ","lp");
   leg->AddEntry(gAsymW4,"E<1300","lp");
   leg->AddEntry(gPTW1,"P_{target}","p");
   leg->AddEntry(gPBW1,"P_{beam}","p");
   leg->Draw();

   // ----------
   TMultiGraph * mg = new TMultiGraph();
   //mg->Add(gAsym2,"p");
   //mg->Add(gAsym3,"p");
   mg->Add(gAsymW1,"p");
   //mg->Add(gAsymW2,"p");
   //mg->Add(gAsymW3,"p");
   //mg->Add(gAsymW4,"p");
   mg->Add(gxW2,"p");
   mg->Add(gPTW1,"p");
   mg->Add(gPBW1,"p");

   mg->SetMaximum(0.40);
   mg->SetMinimum(-0.4);
   mg->Draw("a");

   //mg->GetXaxis()->SetRangeUser(72900,72980);
   mg->GetXaxis()->SetNoExponent(true);
   c->Update();

   c->SaveAs(Form("results/compare_asym_perp59_%d.png",some_number));
   c->SaveAs(Form("results/compare_asym_perp59_%d.pdf",some_number));

   return(0);
}
