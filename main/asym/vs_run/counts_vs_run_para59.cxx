/*! Creates the plots vs Run Number
 */
Int_t counts_vs_run_para59(Int_t some_number = 30){
   // --- Data from Mark
   TString mark_file1 = "asym/mark/para_59_900_run.dat";
   std::vector<std::vector<Double_t> > mark1;
   if (gROOT->LoadMacro("asym/vs_run/read_marks.cxx") != 0) {
      Error(weh, "Failed loading read_marks.cxx in compiled mode.");
      return -1;
   }
   Int_t retval=read_marks(mark_file1.Data(),&mark1);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << mark1.size() << std::endl;
   Int_t iRun = 0;
   Int_t iy  = 1;
   std::vector<Double_t> zeros;
   std::vector<Double_t> runCount;
   for(int i=0;i<(mark1.at(0)).size(); i++){
      zeros.push_back(0.0);
      runCount.push_back(i+1);
   }
   TGraph * gAsym3 = new TGraph( (mark1.at(0)).size(),&(runCount.at(0)),&((mark1.at(iy)).at(0)));//,&zeros[0],&runAsymmetryError3[0]);
   gAsym3->SetMarkerStyle(33);
   gAsym3->SetMarkerColor(kBlue-7);
   gAsym3->SetLineColor(kBlue-7);
   //gAsym3->Draw("alp");

   // --- Data from Mark
   TString mark_file2 = "asym/mark/para_59_1300_run.dat";
   std::vector<std::vector<Double_t> > mark2;
   retval=read_marks(mark_file2.Data(),&mark2);
   if( retval ) return( -10+retval);
   std::cout << " Data points: " << mark2.size() << std::endl;
   Int_t iRun = 0;
   Int_t iy  = 1;
   std::vector<Double_t> zeros;
   std::vector<Double_t> runCount;
   for(int i=0;i<(mark2.at(0)).size(); i++){
      zeros.push_back(0.0);
      runCount.push_back(i+1);
   }
   TGraph * gAsym2 = new TGraph( (mark2.at(0)).size(),&(runCount.at(0)),&((mark2.at(iy)).at(0)));//,&zeros[0],&runAsymmetryError3[0]);
   gAsym2->SetMarkerStyle(27);
   gAsym2->SetMarkerColor(kBlue-4);
   gAsym2->SetLineColor(kBlue-4);
   //gAsym3->Draw("alp");
   // --------------------------------

   // --------------------------------

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   SANERunManager * rman = (SANERunManager *)InSANERunManager::GetRunManager();

   /// Sets up a canvas
/*   TCanvas *c1 = new TCanvas("asymCanvas","Raw Asymmetries",1200,400);*/
   // c1->SetFillColor(42);
/*   c1->SetGrid();*/
   //c1->Divide(1,2);

   InSANERunSeries * aSeries= new InSANERunSeries("Asym");

   InSANERunPolarization * beamPol = new InSANERunBeamPol("beamPol","Beam Polarization");
   beamPol->fMarkerColor = 8;
   beamPol->fMarkerStyle = 33;

   InSANERunPolarization * targTopPos = new InSANERunTargTopPosPol("topTargPosPol","Top Target Pos. Pol. ");
   targTopPos->fMarkerColor = 2;
   targTopPos->fMarkerStyle = 22;

   InSANERunPolarization * targTopNeg = new InSANERunTargTopNegPol("topTargNegPol","Top Target Neg. Pol. ");
   targTopNeg->fMarkerColor = 4;
   targTopNeg->fMarkerStyle = 22;

   InSANERunPolarization * targBotPos = new InSANERunTargBotPosPol("BottomTargPosPol","Bottom Target Pos. Pol. ");
   targBotPos->fMarkerColor = 2;
   targBotPos->fMarkerStyle = 23;

   InSANERunPolarization * targBotNeg = new InSANERunTargBotNegPol("BottomTargNegPol","Bottom Target Neg. Pol. ");
   targBotNeg->fMarkerColor = 4;
   targBotNeg->fMarkerStyle = 23;

   InSANERunAsymmetry * asym = new InSANERunAsymmetry();
   asym->SetNameTitle("Asymmetry","Asymmetry");
   asym->fMarkerColor = 1;
   asym->fMarkerStyle = 20;
   asym->fMarkerSize = 1.5;

   InSANERawRunAsymmetryCounts * asym1 = new InSANERawRunAsymmetryCounts("run-asym1");
   asym1->SetNameTitle("runasym1","E>900");
   asym1->fMarkerColor = kGreen - 8;
   asym1->fMarkerStyle = 20;
   asym1->fMarkerSize = 1.3;

   InSANERawRunAsymmetryCounts * asym2 = new InSANERawRunAsymmetryCounts("run-asym2");
   asym2->SetNameTitle("runasym2","E>1300");
   asym2->fMarkerColor = kGreen - 4;
   asym2->fMarkerStyle = 21;
   asym2->fMarkerSize = 1.3;

   //aSeries->AddRunQuantity(beamPol);
   //aSeries->AddRunQuantity(targTopPos);
   //aSeries->AddRunQuantity(targTopNeg);
   //aSeries->AddRunQuantity(targBotPos);
   //aSeries->AddRunQuantity(targBotNeg);
   //aSeries->AddRunQuantity(asym);
   aSeries->AddRunQuantity(asym1);
   aSeries->AddRunQuantity(asym2);

   /// Build up a run queue to be plotted
/*  aman->BuildQueue("lists/para/run_series1.txt");*/
/*  aman->BuildQueue("lists/para/all_para.txt");*/
/*  aman->BuildQueue("lists/para/all_para.txt");*/
   aman->BuildQueue2("lists/para/5_9GeV/good_NH3_para59_1.txt");
   //aman->BuildQueue2("lists/para/5_9GeV/good_NH3_para59_2.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/para/4_7GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/perp/4_7GeV/good_NH3_1.txt");

   Double_t avg_asym = 0.0;
   Double_t weighted_mean = 0.0;
   Double_t var_of_mean = 0.0;
   Double_t variance = 0.0;
   int k=0;
   int Nrun=0;

   TFile * f = new TFile("data/AsymVsRun.root","UPDATE");
 
   for(int i = 0;i<aman->fRunQueue->size();i++) {
      std::cout << " RUN : " <<  aman->fRunQueue->at(i) << "\n";
      Nrun++;
      rman->SetRun(aman->fRunQueue->at(i),-1,"Q");
      if( !( rman->GetCurrentRun()->fAnalysisPass < 1) ) {
        /// If the run has been fully analyzed then use it otherwise skip it out. 
        InSANERun * somerun = rman->GetCurrentRun();
        f->cd();

        if(somerun) {
           aSeries->AddRun(new InSANERun(*somerun) );

           asym->SetRun(somerun);
           if(asym->IsValidRun()) {
              avg_asym += asym->GetQuantity();
              variance += asym->GetQuantity()*asym->GetQuantity();
              weighted_mean += asym->GetQuantity()/(TMath::Power(asym->GetQuantityError(),2));
              var_of_mean += 1.0/TMath::Power(asym->GetQuantityError(),2);
              k++;
           }
           std::cout << k << " " << avg_asym << "\n";
        }
         
      }
   }

   avg_asym = avg_asym/float(k);
   variance = variance/float(k);
   variance = variance - avg_asym*avg_asym; 
   weighted_mean = weighted_mean/var_of_mean;
   var_of_mean = 1.0/var_of_mean;
   std::cout << "Average Asymmetry : " << avg_asym << "\n";
   std::cout << "variance : " << variance << "\n";
   std::cout << "Weighted Mean Asymmetry : " << weighted_mean << "\n";
   std::cout << "Variance of Mean : " << var_of_mean << "\n";

   aSeries->GenerateGraphs();

   TCanvas * c1 =  new TCanvas("asym_vs_run_para47","asym_vs_run_para47");
   aSeries->fMultiGraph.Add(gAsym3,"p");
   aSeries->fMultiGraph.Add(gAsym2,"p");
   //aSeries->fMultiGraph.SetMinimum(-1.0);
   //aSeries->fMultiGraph.SetMaximum(1.0);
   aSeries->Draw();

   TLegend * leg = aSeries->fLegend;

   double x[] = {0,Nrun+1};
   double y[] = {avg_asym,avg_asym };
   double ex[] = {0,0};
   double ey[] = {TMath::Sqrt(variance),TMath::Sqrt(variance)};

   TGraphErrors* ge = new TGraphErrors(2, x, y, ex, ey);
   ge->SetFillColor(kBlue-12);
   ge->SetLineColor(kBlue-12);
   ge->Draw("3,same");
   if(leg) leg->AddEntry(ge,Form("#sigma_{#mu} = %1.6f",var_of_mean),"f");

   double y2[] = {weighted_mean,weighted_mean };
   double ey2[] = {TMath::Sqrt(var_of_mean),TMath::Sqrt(var_of_mean)};
   TGraphErrors* ge2 = new TGraphErrors(2, x, y2, ex, ey2);
   ge2->SetFillColor(kRed-10);
   ge2->SetLineColor(kRed-10);
   ge2->Draw("3,same");
   if(leg) leg->AddEntry(ge2,Form("#sigma = %1.3f",TMath::Sqrt(variance)),"f");

   TF1 * f2 = new TF1("weighted_mean_asymmetry",Form("%f",weighted_mean),0,73050);
   f2->SetLineColor(kRed);
   f2->Draw("same");


   if(leg) leg->AddEntry(f2,Form("#mu = %1.3f",weighted_mean),"l");
   if(leg) leg->AddEntry(gAsym2,"Jones E > 1300 Asymmetry","lp");
   if(leg) leg->AddEntry(gAsym3,"Jones E > 900 Asymmetry","lp");

   //gAsym2->Draw("p,same");
   aSeries->Draw();

   c1->SaveAs(Form("results/asym_vs_run-%d.png",some_number));
   c1->SaveAs(Form("results/asym_vs_run-%d.pdf",some_number));
   c1->SaveAs(Form("results/asym_vs_run-%d.ps",some_number));

   return(0);
}
