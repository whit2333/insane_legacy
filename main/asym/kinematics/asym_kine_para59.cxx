Int_t asym_kine_para59(Int_t runnumber = 72920/*4703*/, Int_t paraRunGroup = 242) {

   Int_t aNumber = 0;

   TFile * f = new TFile(Form("data/binned_asymmetries_para59_%d.root",paraRunGroup),"UPDATE");
   if(!f) return(-1);
   f->ls();
   TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey);
   if(!fAsymmetries) return(-2);
   if(fAsymmetries) fAsymmetries->Print();

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg1 = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   
   //  Q2 bin.
   Int_t istyle = 20;
   TLegend * leg = new TLegend(0.2,0.2,0.8,0.8);
   TString bigcalPart = "RCS";

   InSANEAveragedKinematics1D * kine;
   TString binAxis  = "x";
   TString binLabel = "x";

   for(int i = 0; i<fAsymmetries->GetEntries(); i++) {

      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(i)));
      if(!asy) continue;
      if(i>3) {
         istyle = 22; 
         bigcalPart = "Protvino";
      }
      //kine = asy->fAvgKineVsPhi;
      kine = asy->fAvgKineVsx;

      TH1F * h1 = kine->fE; 
      TGraphErrors * gr = new TGraphErrors(h1);
      gr->SetMarkerStyle(istyle);
      gr->SetLineWidth(1);
      gr->SetLineColor(asy->fAsymmetryVsx->GetMarkerColor());
      gr->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         //if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      mg->Add(gr,"ep");
      leg->AddEntry(gr,Form("%s, Q^{2} = %1.1f GeV^{2}",bigcalPart.Data(),asy->GetQ2()),"p");

      h1 = kine->fTheta; 
      TGraphErrors * gr1 = new TGraphErrors(h1);
      gr1->SetMarkerStyle(istyle);
      gr1->SetLineWidth(1);
      gr1->SetLineColor(asy->fAsymmetryVsx->GetMarkerColor());
      gr1->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      for( int j = gr1->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr1->GetPoint(j,xt,yt);
         //if( yt == 0.0 ) { gr1->RemovePoint(j); }
      }
      mg1->Add(gr1,"ep");
      kine->fPhi->Dump();
      h1 = kine->fPhi; 
      TGraphErrors * gr2 = new TGraphErrors(h1);
      gr2->SetMarkerStyle(istyle);
      gr2->SetLineWidth(1);
      gr2->SetLineColor(asy->fAsymmetryVsx->GetMarkerColor());
      gr2->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      for( int j = gr2->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         //if( yt == 0.0 ) { gr2->RemovePoint(j); }
      }
      mg2->Add(gr2,"ep");
   }


   TCanvas * c = new TCanvas("asym_kine_para59","asym_kine_para59");
   c->Divide(2,2);

   c->cd(1);
   mg->Draw("a");
   mg->GetXaxis()->SetTitle(binLabel);
   mg->GetXaxis()->CenterTitle(true);
   //mg->GetXaxis()->SetRangeUser(0.4,2.8);
   mg->GetYaxis()->SetTitle("E' [GeV]");
   mg->GetYaxis()->CenterTitle(true);
   mg->GetYaxis()->SetRangeUser(0.5,3.0);
   mg->Draw("a");

   c->cd(2);
   leg->Draw();

   c->cd(3);
   mg1->Draw("a");
   if(mg1->GetXaxis()){
      mg1->GetXaxis()->SetTitle(binLabel);
      mg1->GetXaxis()->CenterTitle(true);
      //mg1->GetXaxis()->SetRangeUser(0.4,2.8);
      mg1->GetYaxis()->SetTitle("Theta [rad]");
      mg1->GetYaxis()->CenterTitle(true);
      mg1->GetYaxis()->SetRangeUser(0.5,0.9);
      mg1->Draw("a");
   }

   c->cd(4);
   mg2->Draw("a");
   if(mg2->GetXaxis()){
      mg2->GetXaxis()->SetTitle(binLabel);
      mg2->GetXaxis()->CenterTitle(true);
      //mg2->GetXaxis()->SetRangeUser(0.4,2.8);
      mg2->GetYaxis()->SetTitle("Phi [rad]");
      mg2->GetYaxis()->CenterTitle(true);
      mg2->GetYaxis()->SetRangeUser(-0.25,0.7);
      mg2->Draw("a");
   }



   c->SaveAs(Form("results/asymmetries/asym_kine_para59_vs%s_%d.png",binAxis.Data(),runnumber));
   c->SaveAs(Form("results/asymmetries/asym_kine_para59_vs%s_%d.pdf",binAxis.Data(),runnumber));
   c->SaveAs(Form("results/asymmetries/asym_kine_para59_vs%s_%d.root",binAxis.Data(),runnumber));

   return(0);
}

