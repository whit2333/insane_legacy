Int_t asym_kine_59(Int_t paraRunGroup = 30, Int_t perpRunGroup = 30) {

   Int_t aNumber = 4;

   TFile * f = new TFile(Form("data/binned_asymmetries_perp59_%d.root",perpRunGroup),"UPDATE");
   if(!f) return(-1);
   f->cd();
   TList * fAsymmetries = (TList*) gROOT->FindObject(Form("combined-asym_perp59-%d",0));//,TObject::kSingleKey);
   if(!fAsymmetries) return(-2);
   
   TFile * f2 = new TFile(Form("data/binned_asymmetries_para59_%d.root",paraRunGroup),"UPDATE");
   if(!f2) return(-3);
   f2->cd();
   TList * fAsymmetries2 = (TList*) gROOT->FindObject(Form("combined-asym_para59-%d",0));//,TObject::kSingleKey);
   if(!fAsymmetries2) return(-4);

   //  parallel
   TMultiGraph * mg0_E1  = new TMultiGraph(); // E
   TMultiGraph * mg0_theta1 = new TMultiGraph(); // theta
   TMultiGraph * mg0_phi1 = new TMultiGraph(); // phi

   TMultiGraph * mg0_E2  = new TMultiGraph(); // E
   TMultiGraph * mg0_theta2 = new TMultiGraph(); // theta
   TMultiGraph * mg0_phi2 = new TMultiGraph(); // phi

   TMultiGraph * mg0_E3 = new TMultiGraph(); // E
   TMultiGraph * mg0_theta3 = new TMultiGraph(); // theta
   TMultiGraph * mg0_phi3 = new TMultiGraph(); // phi
   
   //  perp
   TMultiGraph * mg1_E1  = new TMultiGraph(); // E
   TMultiGraph * mg1_theta1 = new TMultiGraph(); // theta
   TMultiGraph * mg1_phi1 = new TMultiGraph(); // phi

   TMultiGraph * mg1_E2  = new TMultiGraph(); // E
   TMultiGraph * mg1_theta2 = new TMultiGraph(); // theta
   TMultiGraph * mg1_phi2 = new TMultiGraph(); // phi

   TMultiGraph * mg1_E3 = new TMultiGraph(); // E
   TMultiGraph * mg1_theta3 = new TMultiGraph(); // theta
   TMultiGraph * mg1_phi3 = new TMultiGraph(); // phi
   //  Q2 bin.
   for(int i = 0; i<fAsymmetries->GetEntries(); i++) {

   std::cout << " test " << std::endl;
      //SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      // Perp
      InSANEAveragedMeasuredAsymmetry * avg_asym = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries->At(i));
      SANEMeasuredAsymmetry * asy = (SANEMeasuredAsymmetry*)(avg_asym->GetAsymmetryResult());
      if(!asy) continue;

   std::cout << " test 2" << std::endl;
      // Para
      InSANEAveragedMeasuredAsymmetry * avg_asym2 = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries2->At(i));
      SANEMeasuredAsymmetry * asy2 = (SANEMeasuredAsymmetry*)(avg_asym2->GetAsymmetryResult());
      if(!asy2) continue;
   std::cout << " test 3" << std::endl;

      // Parallel
      TH1F * h1 = asy->fAvgKineVsx->fE;  
      TGraphErrors * gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      if(i<5) {
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_E1->Add(gr,"ep");
      } else if(i<10) {
         gr->SetMarkerStyle(32);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_E2->Add(gr,"ep");
      } else  {
         gr->SetMarkerStyle(26);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_E3->Add(gr,"ep");
      }

      h1 = asy->fAvgKineVsx->fTheta; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { 
            gr->RemovePoint(j);
         }
      }
      if(i<5) {
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_theta1->Add(gr,"ep");
      } else if(i<10) {
         gr->SetMarkerStyle(32);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_theta2->Add(gr,"ep");
      } else  {
         gr->SetMarkerStyle(26);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_theta3->Add(gr,"ep");
      }
      
      h1 = asy->fAvgKineVsx->fPhi; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      if(i<5) {
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_phi1->Add(gr,"ep");
      } else if(i<10) {
         gr->SetMarkerStyle(32);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_phi2->Add(gr,"ep");
      } else  {
         gr->SetMarkerStyle(26);
         gr->SetMarkerColor(i%5 + 2000);
         gr->SetLineColor(i%5 + 2000);
         mg0_phi3->Add(gr,"ep");
      }

      // Perpendicular
      //h1 = asy2->fAvgKineVsx->fE; 
      //gr = new TGraphErrors(h1);
      //for( int j = gr->GetN()-1; j>=0 ; j--) {
      //   Double_t xt, yt;
      //   gr->GetPoint(j,xt,yt);
      //   if( yt == 0.0 ) { gr->RemovePoint(j); }
      //}
      //gr->SetMarkerStyle(20);
      //gr->SetMarkerColor(asy2->fAsymmetryVsx->GetMarkerColor());
      //if(i<4)mg->Add(gr,"ep");
      //else mg3->Add(gr,"ep");

      //h1 = asy2->fAvgKineVsx->fTheta; 
      //gr1 = new TGraphErrors(h1);
      //for( int j = gr1->GetN()-1; j>=0 ; j--) {
      //   Double_t xt, yt;
      //   gr1->GetPoint(j,xt,yt);
      //   if( yt == 0.0 ) { gr1->RemovePoint(j); }
      //}
      //gr1->SetMarkerStyle(20);
      //gr1->SetMarkerColor(asy2->fAsymmetryVsx->GetMarkerColor());
      //if(i<4)mg1->Add(gr1,"ep");
      //else mg4->Add(gr1,"ep");
      //
      //h1 = asy2->fAvgKineVsx->fPhi; 
      //gr2 = new TGraphErrors(h1);
      //for( int j = gr2->GetN()-1; j>=0 ; j--) {
      //   Double_t xt, yt;
      //   gr2->GetPoint(j,xt,yt);
      //   if( yt == 0.0 ) { gr2->RemovePoint(j); }
      //}
      //gr2->SetMarkerStyle(20);
      //gr2->SetMarkerColor(asy2->fAsymmetryVsx->GetMarkerColor());
      //if(i<4)mg2->Add(gr2,"ep");
      //else mg5->Add(gr2,"ep");
   }


   TCanvas * c = new TCanvas("asym_kine_59","asym_kine_59");
   c->Divide(2,2);

   TMultiGraph * MG0 = new TMultiGraph();
   MG0->Add(mg0_E1);
   MG0->Add(mg0_E2);
   MG0->Add(mg0_E3);
   std::cout << " test 5" << std::endl;
   c->cd(1);
   MG0->Draw("a");
   MG0->SetTitle("E' vs x");
   MG0->GetXaxis()->SetTitle("x");
   MG0->GetYaxis()->SetTitle("E' [GeV]");
   MG0->GetXaxis()->SetRangeUser(0.0,1.0);
   MG0->GetYaxis()->SetRangeUser(0.5,3.0);

   std::cout << " test 5" << std::endl;
   TMultiGraph * MG1 = new TMultiGraph();
   MG1->Add(mg0_theta1);
   MG1->Add(mg0_theta2);
   MG1->Add(mg0_theta3);
   c->cd(2);
   MG1->Draw("a");
   MG1->SetTitle("#theta vs x");
   MG1->GetXaxis()->SetTitle("x");
   MG1->GetYaxis()->SetTitle("#theta_{e} [rad]");
   //MG1->GetXaxis()->SetLimits(0.0,1.0);
   MG1->GetYaxis()->SetRangeUser(0.2,1.0);

   std::cout << " test 5" << std::endl;
   TMultiGraph * MG2 = new TMultiGraph();
   MG2->Add(mg0_phi1);
   MG2->Add(mg0_phi2);
   MG2->Add(mg0_phi3);
   c->cd(3);
   MG2->Draw("a");
   MG2->SetTitle("#phi vs x");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetYaxis()->SetTitle("#phi_{e} [rad]");
   MG2->GetXaxis()->SetRangeUser(0.0,1.0);
   MG2->GetYaxis()->SetRangeUser(-0.8,0.8);

   //c->cd(4);
   //mg3->Draw("a");
   //mg3->SetTitle("Protvino");
   //mg3->GetXaxis()->SetTitle("x");
   //mg3->GetYaxis()->SetTitle("E' [GeV]");
   //mg3->GetXaxis()->SetRangeUser(0.0,1.0);
   //mg3->GetYaxis()->SetRangeUser(0.0,3.0);

   //c->cd(5);
   //mg4->Draw("a");
   //mg4->SetTitle("Protvino");
   //mg4->GetXaxis()->SetTitle("x");
   //mg4->GetYaxis()->SetTitle("#theta_{e} [rad]");
   //mg4->GetXaxis()->SetRangeUser(0.0,1.0);
   //mg4->GetYaxis()->SetRangeUser(0.0,1.0);

   //c->cd(6);
   //mg5->Draw("a");
   //mg5->SetTitle("Protvino");
   //mg5->GetXaxis()->SetTitle("x");
   //mg5->GetYaxis()->SetTitle("#phi_{e} [rad]");
   //mg5->GetXaxis()->SetRangeUser(0.0,1.0);
   //mg5->GetYaxis()->SetRangeUser(-0.2,0.1);

   c->SaveAs(Form("results/kinematics/asym_kine_59_%d.png",aNumber));
   c->SaveAs(Form("results/kinematics/asym_kine_59_%d.pdf",aNumber));

   return(0);
}
