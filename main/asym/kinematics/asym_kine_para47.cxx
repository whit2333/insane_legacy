Int_t asym_kine_para47(Int_t runnumber = 73001, Int_t paraRunGroup = 8) {
   Int_t aNumber = 0;

   TFile * f = new TFile(Form("data/binned_asymmetries_para47_%d.root",paraRunGroup),"UPDATE");
   if(!f) return(-1);
   f->ls();
   //TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey);
   TList * fAsymmetries = (TList*) gROOT->FindObject(Form("combined-asym_para47-%d",0));//,TObject::kSingleKey);
   if(!fAsymmetries) return(-2);
   if(fAsymmetries) fAsymmetries->Print();

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg1 = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   
   //  Q2 bin.
   for(int i = 0; i<fAsymmetries->GetEntries(); i++) {

      //SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(i)));
      InSANEAveragedMeasuredAsymmetry * avg_asym = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries->At(i));
      SANEMeasuredAsymmetry * asy = (SANEMeasuredAsymmetry*)(avg_asym->GetAsymmetryResult());
      if(!asy) continue;
      TH1D * h1 = asy->fEVsx; 
      TGraph * gr = new TGraph(h1);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      mg->Add(gr,"p");

      h1 = asy->fThetaVsx; 
      TGraph * gr1 = new TGraph(h1);
      for( int j = gr1->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr1->RemovePoint(j); }
      }
      gr1->SetMarkerStyle(20);
      gr1->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      mg1->Add(gr1,"p");
      
      h1 = asy->fPhiVsx; 
      TGraph * gr2 = new TGraph(h1);
      for( int j = gr2->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr2->RemovePoint(j); }
      }
      gr2->SetMarkerStyle(20);
      gr2->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      mg2->Add(gr2,"p");
   }


   TCanvas * c = new TCanvas("asym_kine_para47","asym_kine_para47");
   c->Divide(2,2);

   c->cd(1);
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("x");
   mg->GetYaxis()->SetTitle("E' [GeV]");
   mg->GetXaxis()->SetRangeUser(0.0,1.0);
   mg->GetYaxis()->SetRangeUser(0.0,3.0);

   c->cd(2);
   mg1->Draw("a");
   mg1->GetXaxis()->SetTitle("x");
   mg1->GetYaxis()->SetTitle("#theta_{e} [rad]");
   mg1->GetXaxis()->SetRangeUser(0.0,1.0);
   mg1->GetYaxis()->SetRangeUser(0.0,1.0);

   c->cd(3);
   mg2->Draw("a");
   mg2->GetXaxis()->SetTitle("x");
   mg2->GetYaxis()->SetTitle("#phi_{e} [rad]");
   mg2->GetXaxis()->SetRangeUser(0.0,1.0);
   mg2->GetYaxis()->SetRangeUser(-0.1,0.8);

   c->SaveAs(Form("results/asymmetries/asym_kine_para47_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/asym_kine_para47_%d.pdf",aNumber));


   return(0);
}
