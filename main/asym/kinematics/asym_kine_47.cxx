Int_t asym_kine_47(Int_t paraRunGroup = 30, Int_t perpRunGroup = 30) {

   Int_t aNumber = 4;

   TFile * f = new TFile(Form("data/binned_asymmetries_perp47_%d.root",perpRunGroup),"UPDATE");
   if(!f) return(-1);
   f->cd();
   TList * fAsymmetries = (TList*) gROOT->FindObject(Form("combined-asym_perp47-%d",0));//,TObject::kSingleKey);
   if(!fAsymmetries) return(-2);
   
   TFile * f2 = new TFile(Form("data/binned_asymmetries_para47_%d.root",paraRunGroup),"UPDATE");
   if(!f2) return(-3);
   f2->cd();
   TList * fAsymmetries2 = (TList*) gROOT->FindObject(Form("combined-asym_para47-%d",0));//,TObject::kSingleKey);
   if(!fAsymmetries2) return(-4);

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg1 = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TMultiGraph * mg3 = new TMultiGraph();
   TMultiGraph * mg4 = new TMultiGraph();
   TMultiGraph * mg5 = new TMultiGraph();
   
   //  Q2 bin.
   for(int i = 0; i<fAsymmetries->GetEntries(); i++) {

      //SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      // Perp
      InSANEAveragedMeasuredAsymmetry * avg_asym = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries->At(i));
      SANEMeasuredAsymmetry * asy = (SANEMeasuredAsymmetry*)(avg_asym->GetAsymmetryResult());
      if(!asy) continue;

      // Para
      InSANEAveragedMeasuredAsymmetry * avg_asym2 = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries2->At(i));
      SANEMeasuredAsymmetry * asy2 = (SANEMeasuredAsymmetry*)(avg_asym2->GetAsymmetryResult());
      if(!asy2) continue;

      // perp
      TH1F * h1 = asy->fAvgKineVsx->fE; 
      TGraphErrors * gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      gr->SetMarkerStyle(24);
      gr->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      if(i<4)mg->Add(gr,"ep");
      else mg3->Add(gr,"ep");

      h1 = asy->fAvgKineVsx->fTheta; 
      TGraphErrors * gr1 = new TGraphErrors(h1);
      for( int j = gr1->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { 
            gr1->RemovePoint(j);
         }
      }
      gr1->SetMarkerStyle(24);
      gr1->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      if(i<4)mg1->Add(gr1,"ep");
      else mg4->Add(gr1,"ep");
      
      h1 = asy->fAvgKineVsx->fPhi; 
      TGraphErrors * gr2 = new TGraphErrors(h1);
      for( int j = gr2->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr2->RemovePoint(j); }
      }
      gr2->SetMarkerStyle(24);
      gr2->SetMarkerColor(asy->fAsymmetryVsx->GetMarkerColor());
      if(i<4)mg2->Add(gr2,"ep");
      else mg5->Add(gr2,"ep");

      // para 
      h1 = asy2->fAvgKineVsx->fE; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(asy2->fAsymmetryVsx->GetMarkerColor());
      if(i<4)mg->Add(gr,"ep");
      else mg3->Add(gr,"ep");

      h1 = asy2->fAvgKineVsx->fTheta; 
      gr1 = new TGraphErrors(h1);
      for( int j = gr1->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr1->RemovePoint(j); }
      }
      gr1->SetMarkerStyle(20);
      gr1->SetMarkerColor(asy2->fAsymmetryVsx->GetMarkerColor());
      if(i<4)mg1->Add(gr1,"ep");
      else mg4->Add(gr1,"ep");
      
      h1 = asy2->fAvgKineVsx->fPhi; 
      gr2 = new TGraphErrors(h1);
      for( int j = gr2->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr2->RemovePoint(j); }
      }
      gr2->SetMarkerStyle(20);
      gr2->SetMarkerColor(asy2->fAsymmetryVsx->GetMarkerColor());
      if(i<4)mg2->Add(gr2,"ep");
      else mg5->Add(gr2,"ep");
   }


   TCanvas * c = new TCanvas("asym_kine_47","asym_kine_47");
   c->Divide(3,2);

   c->cd(1);
   mg->Draw("a");
   mg->SetTitle("");
   mg->GetXaxis()->SetTitle("x");
   mg->GetYaxis()->SetTitle("E' [GeV]");
   mg->GetXaxis()->SetRangeUser(0.0,1.0);
   mg->GetYaxis()->SetRangeUser(0.0,3.0);

   c->cd(2);
   mg1->Draw("a");
   mg1->SetTitle("");
   mg1->GetXaxis()->SetTitle("x");
   mg1->GetYaxis()->SetTitle("#theta_{e} [rad]");
   mg1->GetXaxis()->SetLimits(0.0,1.0);
   mg1->GetYaxis()->SetRangeUser(0.0,1.0);

   c->cd(3);
   mg2->Draw("a");
   mg2->SetTitle("");
   mg2->GetXaxis()->SetTitle("x");
   mg2->GetYaxis()->SetTitle("#phi_{e} [rad]");
   mg2->GetXaxis()->SetRangeUser(0.0,1.0);
   mg2->GetYaxis()->SetRangeUser(-0.3,0.8);

   c->cd(4);
   mg3->Draw("a");
   mg3->SetTitle("");
   mg3->GetXaxis()->SetTitle("x");
   mg3->GetYaxis()->SetTitle("E' [GeV]");
   mg3->GetXaxis()->SetRangeUser(0.0,1.0);
   mg3->GetYaxis()->SetRangeUser(0.0,3.0);

   c->cd(5);
   mg4->Draw("a");
   mg4->SetTitle("");
   mg4->GetXaxis()->SetTitle("x");
   mg4->GetYaxis()->SetTitle("#theta_{e} [rad]");
   mg4->GetXaxis()->SetRangeUser(0.0,1.0);
   mg4->GetYaxis()->SetRangeUser(0.0,1.0);

   c->cd(6);
   mg5->Draw("a");
   mg5->SetTitle("");
   mg5->GetXaxis()->SetTitle("x");
   mg5->GetYaxis()->SetTitle("#phi_{e} [rad]");
   mg5->GetXaxis()->SetRangeUser(0.0,1.0);
   mg5->GetYaxis()->SetRangeUser(-0.3,0.8);

   c->SaveAs(Form("results/kinematics/asym_kine_47_%d.png",aNumber));
   c->SaveAs(Form("results/kinematics/asym_kine_47_%d.pdf",aNumber));

   return(0);
}
