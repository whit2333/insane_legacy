Int_t coef_func(Int_t runNumber = 72999,Int_t groupNumber = 10) {

   TFile * f = new TFile(Form("data/binned_asymmetries_para59_%d.root",groupNumber),"UPDATE");
   if(!f) return(-1);

   TList * fAsymmetries = (TList*)gROOT->FindObject(Form("binned-asym-%d",runNumber));
   if(!fAsymmetries) return(-1);


   TMultiGraph * mgD = new TMultiGraph();
   TMultiGraph * mgChi = new TMultiGraph();
   TMultiGraph * mgEta = new TMultiGraph();
   TMultiGraph * mgXi = new TMultiGraph();

   TLegend * leg = new TLegend(0.1,0.1,0.9,0.9);

   InSANEAveragedKinematics1D * avgKine = 0;
   TString xAxis = "#theta";
   TString filelabel = "theta";
   //TString xAxis = "E'";

   for(int i = 0 ; i< fAsymmetries->GetEntries() ; i++){

      SANEMeasuredAsymmetry * asym = (SANEMeasuredAsymmetry*)(fAsymmetries->At(i));
      avgKine = asym->fAvgKineVsTheta;

      Int_t istyle = 20;
      if(i>=4) istyle = 24; 

      // --- D
      TGraph * gr = new TGraph(avgKine->fD);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      gr->SetMarkerStyle(istyle);
      gr->SetMarkerColor(asym->fAsymmetryVsx->GetMarkerColor());
      mgD->Add(gr,"p");
   
      // --- Chi 
      gr = new TGraph(avgKine->fChi);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      gr->SetMarkerStyle(istyle);
      gr->SetMarkerColor(asym->fAsymmetryVsx->GetMarkerColor());
      mgChi->Add(gr,"p");
   
      // --- Eta 
      gr = new TGraph(avgKine->fEta);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      gr->SetMarkerStyle(istyle);
      gr->SetMarkerColor(asym->fAsymmetryVsx->GetMarkerColor());
      mgEta->Add(gr,"p");
   
      // --- Chi 
      gr = new TGraph(avgKine->fXi);
      for( int j = gr->GetN()-1; j>=0 ; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) { gr->RemovePoint(j); }
      }
      gr->SetMarkerStyle(istyle);
      gr->SetMarkerColor(asym->fAsymmetryVsx->GetMarkerColor());
      mgXi->Add(gr,"p");
   }

   TCanvas * c = new TCanvas("coef_func","coef_func");
   c->Divide(2,2);

   c->cd(1);
   mgD->Draw("a");
   mgD->GetXaxis()->CenterTitle(true);
   mgD->GetYaxis()->CenterTitle(true);
   mgD->GetXaxis()->SetTitle(xAxis);
   mgD->GetYaxis()->SetTitle("D");
   mgD->Draw("a");

   c->cd(2);
   mgChi->Draw("a");
   mgChi->GetXaxis()->CenterTitle(true);
   mgChi->GetYaxis()->CenterTitle(true);
   mgChi->GetXaxis()->SetTitle(xAxis);
   mgChi->GetYaxis()->SetTitle("#chi");
   mgChi->Draw("a");

   c->cd(3);
   mgEta->Draw("a");
   mgEta->GetXaxis()->CenterTitle(true);
   mgEta->GetYaxis()->CenterTitle(true);
   mgEta->GetXaxis()->SetTitle(xAxis);
   mgEta->GetYaxis()->SetTitle("#eta");
   mgEta->Draw("a");

   c->cd(4);
   mgXi->Draw("a");
   mgXi->GetXaxis()->CenterTitle(true);
   mgXi->GetYaxis()->CenterTitle(true);
   mgXi->GetXaxis()->SetTitle(xAxis);
   mgXi->GetYaxis()->SetTitle("#xi");
   mgXi->Draw("a");

   
   c->Update();

   c->SaveAs(Form("results/coef_func_vs_%s.pdf",filelabel.Data()));
   c->SaveAs(Form("results/coef_func_vs_%s.png",filelabel.Data()));

   return (0);
}
