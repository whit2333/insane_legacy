Int_t compare_A1A2_59_W_x(Int_t paraRunGroup = 253,Int_t perpRunGroup = 253,Int_t aNumber=253){

   gROOT->LoadMacro("util/stat_funcs.cxx"); 

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t error_max = 0.9;
   Double_t E0        = 5.9;
   Double_t alpha     = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   const char * parafile = Form("data/binned_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/binned_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);

   TFile * fout = new TFile(Form("data/A1A2_59_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fA1Asymmetries = new TList();
   TList * fA2Asymmetries = new TList();

   TMultiGraph * mg_A1_vs_x = new TMultiGraph();
   TMultiGraph * mg_A2_vs_x = new TMultiGraph();

   TH1F         * fA1      = 0;
   TH1F         * fA2      = 0;
   TGraphErrors * gr       = 0;
   TGraphErrors * gr2      = 0;
   TMultiGraph  * mg       = new TMultiGraph();
   TMultiGraph  * mg2      = new TMultiGraph();
   TMultiGraph  * mgSplit  = new TMultiGraph();
   TMultiGraph  * mgSplit2 = new TMultiGraph();
   TLegend      * leg      = new TLegend(0.84, 0.15, 0.99, 0.85);
   TLegend      * leg2     = new TLegend(0.7, 0.7, 0.88, 0.88);
   leg2->SetBorderSize(0);
   leg2->SetFillStyle(0);
   leg2->SetFillColor(0);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();

      TH1F * fApara = paraAsym->fAsymmetryVsW;
      TH1F * fAperp = perpAsym->fAsymmetryVsW;

      InSANEAveragedKinematics1D * paraKine = paraAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsW; 

      fA1 = new TH1F(*fApara);
      fA2 = new TH1F(*fAperp);

      fA1->SetNameTitle(Form("fA1-%d",jj),Form("A1-%d",jj));
      fA2->SetNameTitle(Form("fA2-%d",jj),Form("A2-%d",jj));

      TGraphErrors * gr_A1_x = new TGraphErrors(fA1->GetNbinsX());
      TGraphErrors * gr_A2_x = new TGraphErrors(fA1->GetNbinsX());
      mg_A1_vs_x->Add(gr_A1_x,"ep");
      mg_A2_vs_x->Add(gr_A2_x,"ep");

      fA1Asymmetries->Add(fA1);
      fA2Asymmetries->Add(fA2);

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax; i++){
         for(Int_t j=0; j<= yBinmax; j++){
            for(Int_t k=0; k<= zBinmax; k++){

               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);

               Double_t W1     = paraKine->fW->GetBinContent(bin);
               Double_t x1     = paraKine->fx->GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi->GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2->GetBinContent(bin);
               Double_t W2     = perpKine->fW->GetBinContent(bin);
               Double_t x2     = perpKine->fx->GetBinContent(bin);
               Double_t phi2   = perpKine->fPhi->GetBinContent(bin);
               Double_t Q22    = perpKine->fQ2->GetBinContent(bin);
               Double_t W      = (W1+W2)/2.0;
               Double_t x      = (x1+x2)/2.0;
               Double_t phi    = (phi1+phi2)/2.0;
               Double_t Q2     = (Q21+Q22)/2.0;

               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               Double_t A180  = fApara->GetBinContent(bin);
               Double_t A80   = fAperp->GetBinContent(bin);

               Double_t eA180  = fApara->GetBinError(bin);
               Double_t eA80   = fAperp->GetBinError(bin);

               Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               Double_t R        = SFs->R(x,Q2);
               Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               Double_t cota     = 1.0/TMath::Tan(alpha);
               Double_t csca     = 1.0/TMath::Sin(alpha);

               Double_t c0 = 1.0/(1.0 + eta*xi);
               Double_t c11 = (1.0 + cota*chi)/D ;
               Double_t c12 = (csca*chi)/D ;
               Double_t c21 = (xi - cota*chi/eta)/D ;
               Double_t c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               //A1 = A180*(1/D);
               Double_t A1 = c0*(c11*A180 + c12*A80);
               Double_t A2 = c0*(c21*A180 + c22*A80);

               Double_t eA1 = TMath::Sqrt( TMath::Power(c0*c11*eA180,2.0) + TMath::Power(c0*c12*eA80,2.0));
               Double_t eA2 = TMath::Sqrt( TMath::Power(c0*c21*eA180,2.0) + TMath::Power(c0*c22*eA80,2.0));
               //eA2 = c0*(c21*eA180 + c22*eA80);
               //A1 = c0*(A180/D - eta*A80/d);
               //A2 = c0*(xi*A180/D + A80/d);

               if ( A1 != A1 ) A1 = 0;
               if ( A2 != A2 ) A2 = 0;

               //             std::cout << "theta = " << theta << "\n";
               //std::cout << "A1 = " << A1 << "\n";
               //std::cout << "A2 = " << A2 << "\n";
               //             
               fA1->SetBinContent(bin,A1);
               fA2->SetBinContent(bin,A2);
               fA1->SetBinError(bin,eA1);
               fA2->SetBinError(bin,eA2);

               gr_A1_x->SetPoint(i,x,A1);
               gr_A1_x->SetPointError(i,0.0,eA1);
               gr_A2_x->SetPoint(i,x,A2);
               gr_A2_x->SetPointError(i,0.0,eA2);

            }
         }
      }

      // ---------------------------------------
      // A1
      //TH1 * h1 = fA1; 
      gr = gr_A1_x;//new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.9 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      // A1
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(jj%5 + 2000);
      gr->SetLineColor(  jj%5 + 2000);
      if( jj%5 == 4 ){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if( jj<4 ){
         mg->Add(gr,"p");
      } else if( jj<10  ){
         gr->SetMarkerStyle(32);
         mgSplit->Add(gr,"p");
      } else { 
         gr->SetMarkerStyle(26);
         mgSplit->Add(gr,"p");
      }
      if( jj<4 ){
         leg->AddEntry(gr,  Form("SANE Q^{2}=%.1f (GeV/c)^{2}",paraAsym->GetQ2()),"lp");
         leg2->AddEntry(gr, Form("SANE Q^{2}=%.1f (GeV/c)^{2}",paraAsym->GetQ2()),"lp");
      }

      // ---------------------------------------
      // A2
      //h1 = fA2; 
      gr = gr_A2_x;//new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.7 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      // A2
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(jj%5 + 2000);
      gr->SetLineColor(  jj%5 + 2000);
      gr->SetMarkerSize(0.8);
      if( jj%5==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if( jj<4 ){
         mg2->Add(gr,"p");
      } else if( jj<10  ){
         gr->SetMarkerStyle(32);
         mgSplit2->Add(gr,"p");
      } else { 
         gr->SetMarkerStyle(26);
         mgSplit2->Add(gr,"p");
      }

   }

   Double_t Q2 = 4.0;

   //fout->WriteObject(fA1Asymmetries,Form("A1-para59-%d",paraRunGroup));//,TObject::kSingleKey); 


   // ----------------------------------------------------------------------
   TCanvas      * c        = new TCanvas("cA1A2_4pass","A1_para47");
   c->Divide(1,2);

   // --------------- A1 ----------------------
   c->cd(1);

   // Load in world data on A1He3 from NucDB  
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   // Q2 bin for filtering data
   NucDBBinnedVariable Q2Filter("Qsquared","Q^{2}");
   Q2Filter.SetBinMinimum(2.5);
   Q2Filter.SetBinMaximum(6.5);

   TMultiGraph * MG      = new TMultiGraph(); 
   TLegend     * leg3    = new TLegend(0.55, 0.7, 0.69, 0.88);
   leg3->SetBorderSize(0);
   leg3->SetFillStyle(0);
   leg3->SetFillColor(0);

   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   TList * measurementsList = manager->GetMeasurements("A1p");

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1World = (NucDBMeasurement*)measurementsList->At(i);
      A1World->ApplyFilterWithBin(&Q2Filter);
      if( !(strcmp(A1World->GetExperimentName(),"CLAS-E93009")) ){
         A1World->MergeDataPoints(4,"x",true);
      }
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1World->BuildGraph("x");
      if(graph->GetN() == 0) continue;
      //graph->SetMarkerStyle(33);
      graph->SetMarkerStyle(gNiceMarkerStyle[i]);
      graph->SetLineColor(1);
      graph->SetMarkerColor(1);
      graph->SetMarkerSize(0.9);
      // Set legend title 
      Title = Form("%s",A1World->GetExperimentName()); 
      //A1World->Print();
      // Add to TMultiGraph 
      leg->AddEntry(graph,Title,"lp");
      leg3->AddEntry(graph,Title,"lp");
      MG->Add(graph,"p"); 
   }

   TString Measurement = Form("A_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle("E=5.9 GeV");
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-1.0,1.0); 
   MG->Draw("AP");

   //--------------- A2 ---------------------
   c->cd(2);
   //gPad->SetGridx(1);
   //gPad->SetGridy(1);

   TMultiGraph *MG2 = new TMultiGraph(); 
   measurementsList = manager->GetMeasurements("A2p");
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
      A2pMeas->ApplyFilterWithBin(&Q2Filter);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A2pMeas->BuildGraph("x");
      if(graph->GetN() == 0) continue;
      //graph->SetMarkerStyle(27);
      graph->SetMarkerStyle(gNiceMarkerStyle[i]);
      graph->SetLineColor(1);
      graph->SetMarkerColor(1);
      graph->SetMarkerSize(0.9);
      // Set legend title 
      Title = Form("%s",A2pMeas->GetExperimentName()); 
      //A2pMeas->Print();
      if( !strcmp(A2pMeas->GetExperimentName(),"SLAC_E155x") )
         leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 
   }
   MG2->Add(mg2,"p");
   //MG2->SetTitle("Preliminary A_{2}^{p}, E=5.9 GeV");
   MG2->SetTitle("E=5.9 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   MG2->GetYaxis()->SetRangeUser(-1.0,1.0); 
   MG2->GetYaxis()->SetTitle("A_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   c->Update();

   c->cd(0);
   leg->Draw();


   c->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_%d.svg",aNumber));


   // ----------------------------------------------------------------
   // A1 alone
   TCanvas * c2 = new TCanvas();
   c2->cd(0);

   MG->GetYaxis()->SetRangeUser(-1.0,2.0); 
   MG->Draw("A");
   MG->SetTitle("");
   MG->Draw("A");

   leg2->Draw();
   leg3->Draw();

   c2->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_A1_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_A1_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_A1_%d.tex",aNumber));

   // ----------------------------------------------------------------
   // A2 alone
   TCanvas * c3 = new TCanvas();
   c3->cd(0);

   MG2->Draw("A");
   MG2->SetTitle("");
   MG2->Draw("A");

   c3->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_A2_%d.pdf",aNumber));
   c3->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_A2_%d.png",aNumber));
   c3->SaveAs(Form("results/asymmetries/compare_A1A2_59_W_x_A2_%d.tex",aNumber));

   return(0);
}
