Int_t compare_Apara_47_W(Int_t paraRunGroup = 898,Int_t perpRunGroup = 898,Int_t aNumber=898){

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   const char * parafile = Form("data/binned_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/binned_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);
   //fPerpAsymmetries->Print();

   std::cout << "DONE" << std::endl;
   TFile * fout = new TFile(Form("data/AparaOverD_47_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fAparaAsymmetries = new TList();
   TList * fAperpAsymmetries = new TList();

   TH1F * fApara = 0;
   TH1F * fAperp = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg  = new TLegend(0.84, 0.15, 0.99, 0.85);
   TLegend *leg2 = new TLegend(0.66, 0.6, 0.85, 0.85);
   leg2->SetBorderSize(0);
   leg2->SetFillStyle(0);
   leg2->SetFillColor(0);

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();

      TH1F * fApara = paraAsym->fAsymmetryVsW;
      TH1F * fAperp = perpAsym->fAsymmetryVsW;

      InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsW; 

      fApara = new TH1F(*fApara);
      fAperp = new TH1F(*fAperp);

      fApara->SetNameTitle(Form("fApara_47_%d",jj),Form("Apara_47_%d",jj));
      fAperp->SetNameTitle(Form("fAperp_47_%d",jj),Form("Aperp_47_%d",jj));

      fAparaAsymmetries->Add(fApara);
      fAperpAsymmetries->Add(fAperp);

      //----------------------------------------------------------------------

      // -------------------------
      // A_para
      gr = new TGraphErrors(fApara);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.55 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fApara->GetMarkerColor());
      gr->SetLineColor(fApara->GetMarkerColor());
      if(jj%5==4){
         gr->SetMarkerColor(1);
         //gr->SetMarkerStyle(24);
         gr->SetMarkerSize(0.8);
         gr->SetLineColor(1);
      }
      if(jj<4){
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
         leg2->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }
      if(jj==4){
         mg->Add(gr,"p");
         leg->AddEntry(gr,"SANE all Q^{2}","lp");
         leg2->AddEntry(gr,"SANE all Q^{2}","lp");
      }

      // -------------------------
      // A_perp
      gr = new TGraphErrors(fAperp);
      for( int j = gr->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.6 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fApara->GetMarkerColor());
      gr->SetLineColor(fApara->GetMarkerColor());
      if(jj%5==4){
         gr->SetMarkerColor(1);
         //gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
         gr->SetMarkerSize(0.8);
      }
      if(jj<5)mg2->Add(gr,"p");

   }


   // ----------------------------------------------------

   fout->WriteObject(fAparaAsymmetries,Form("Apara-47-W-%d",paraRunGroup));//,TObject::kSingleKey); 


   // ----------------------------------------------------
   TCanvas * c = new TCanvas("cApara","Apara");

   Double_t Q2 = 4.0;

   // --------------- A-para ----------------------

   // Load in world data on A1p from NucDB  
   NucDBManager * manager = NucDBManager::GetManager();

   TMultiGraph      * MG               = new TMultiGraph();
   TMultiGraph      * MG_data          = new TMultiGraph();
   TList            * measurementsList = manager->GetMeasurements("Aparap");
   NucDBExperiment  * exp              = 0;
   NucDBMeasurement * ames             = 0;

   // Q2 bin for filtering data
   NucDBBinnedVariable Q2Filter("Qsquared","Q^{2}");
   Q2Filter.SetBinMinimum(3.5);
   Q2Filter.SetBinMaximum(6.5);
   NucDBBinnedVariable WFilter("W","W");
   WFilter.SetBinMinimum(1.0);
   WFilter.SetBinMaximum(20.0);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1World = (NucDBMeasurement*)measurementsList->At(i);
      A1World->ApplyFilterWithBin(&Q2Filter);
      A1World->ApplyFilterWithBin(&WFilter);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1World->BuildGraph("W");
      graph->SetMarkerStyle(27+i);
      // Set legend title 
      Form("%s",A1World->GetExperimentName()); 
      leg->AddEntry(graph,Form("%s",A1World->GetExperimentName()),"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
      MG_data->Add(graph); 
   }

   MG->Draw("A");
   MG->SetTitle("E=4.7 GeV");
   MG->GetXaxis()->SetTitle("W (GeV)");
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle("A_{180}^{p}");
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(1.0,3.0); 
   MG->GetYaxis()->SetRangeUser(-0.2,1.5); 
   MG->Draw("AP");

   c->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_%d.cxx",aNumber));

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");

   leg->Draw();

   c->SaveAs(Form("results/asymmetries/compare_Apara_47_W_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_Apara_47_W_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_Apara_47_W_%d.cxx",aNumber));

   //----------------------------------------------------------
   // A_para alone
   TCanvas * c2 = new TCanvas();
   c2->cd(0);

   mg->Draw("A");
   mg->SetTitle("E=4.7 GeV");
   mg->GetXaxis()->SetTitle("W (GeV)");
   mg->GetXaxis()->CenterTitle();
   mg->GetYaxis()->SetTitle("A_{180}^{p}");
   mg->GetYaxis()->CenterTitle();
   mg->GetXaxis()->SetLimits(1.0,3.0); 
   mg->GetYaxis()->SetRangeUser(-0.2,1.5); 
   mg->Draw("A");

   leg2->Draw();

   c2->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_SANE_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_SANE_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_SANE_%d.cxx",aNumber));

   //----------------------------------------------------------
   // A_perp alone
   TCanvas * c3 = new TCanvas();
   c3->cd(0);

   mg2->Draw("A");
   mg2->SetTitle("E=4.7 GeV");
   mg2->GetXaxis()->SetTitle("W (GeV)");
   mg2->GetXaxis()->CenterTitle();
   mg2->GetYaxis()->SetTitle("A_{80}^{p}");
   mg2->GetYaxis()->CenterTitle();
   mg2->GetXaxis()->SetLimits(1.0,3.0); 
   mg2->GetYaxis()->SetRangeUser(-1.0,1.5); 
   mg2->Draw("A");

   leg2->Draw();

   c3->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_SANE_Aperp_%d.pdf",aNumber));
   c3->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_SANE_Aperp_%d.png",aNumber));
   c3->SaveAs(Form("results/asymmetries/compare_Apara_existing_47_W_SANE_Aperp_%d.cxx",aNumber));


   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   //c->cd(2);
   //TMultiGraph *MG2 = new TMultiGraph(); 
   //measurementsList = manager->GetMeasurements("A2p");
   //measurementsList->Print();
   //for (int i = 0; i < measurementsList->GetEntries(); i++) {
   //   NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
   //   A2pMeas->ApplyFilterWithBin(&Q2Filter);
   //   // Get TGraphErrors object 
   //   TGraphErrors *graph        = A2pMeas->BuildGraph("x");
   //   graph->SetMarkerStyle(27);
   //   // Set legend title 
   //   Title = Form("%s",A2pMeas->GetExperimentName()); 
   //   //leg->AddEntry(graph,Title,"lp");
   //   // Add to TMultiGraph 
   //   MG2->Add(graph,"p"); 
   //}
   //MG2->Add(mg2,"p");
   //MG2->SetTitle("Preliminary A_{2}^{p}, E=5.9 GeV");
   //MG2->Draw("a");
   //MG2->GetXaxis()->SetLimits(0.0,1.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.5,0.5); 
   //MG2->GetYaxis()->SetTitle("A_{2}^{p}");
   //MG2->GetXaxis()->SetTitle("x");
   //MG2->GetXaxis()->CenterTitle();
   //MG2->GetYaxis()->CenterTitle();
   //c->Update();

   //fout->WriteObject(fA1Asymmetries,Form("A1-47-%d",0));//,TObject::kSingleKey);
   //fout->WriteObject(fAperpAsymmetries,Form("A2-47-%d",0));//,TObject::kSingleKey);
   //fout->WriteObject(fParaAsymmetries,Form("ParaAsym-47-%d",0));
   //fout->WriteObject(fPerpAsymmetries,Form("PerpAsym-47-%d",0));

   return(0);
}
