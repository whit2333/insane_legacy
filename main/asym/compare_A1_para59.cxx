Int_t compare_A1_para59(Int_t paraRunGroup = 0,Int_t perpRunGroup = 0,Int_t aNumber=2){


   //f->WriteObject(fCombinedAsyms,Form("combined-asym_para47_2-%d",0));//,TObject::kSingleKey); 

   const char * parafile = "data/binned_asymmetries_para59_2.root";
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para59_2-%d",0));
   if(!fParaAsymmetries) return(-1);

   fParaAsymmetries->Print();
   //new TBrowser();
   //return(0);
   ////const char * perpfile = Form("data/Tracks/PerpTracks%d.root",perpRunGroup);;
   //TFile * f2 = TFile::Open(perpfile,"UPDATE");
   //f2->cd();
   //TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("asymmetries-%d",perpRunGroup));
   //if(!fPerpAsymmetries) return(-2);

   TFile * fout = new TFile(Form("data/A1_para59_%d_%d.root",paraRunGroup,perpRunGroup),"UPDATE");
   TList * fA1Asymmetries = new TList();
   //TList * fA2Asymmetries = new TList();

   Double_t eta,xi,chi,D,d,epsilon,R;
   Double_t Q2,W,x,y,phi,E0,Ep,theta,alpha;
   Double_t c0,c11,c12,c21,c22;
   Double_t A180,A80,A1,A2;
   E0 = 5.9;
   alpha = 80.0*TMath::Pi()/180.0;

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TGraphErrors * gr = 0;
   //TH3F * fA1 = 0;
   //TH3F * fA2 = 0;

   /// The asymmetry is calculated A = C1(A180*C2+A80*C3)
   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   //c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TLegend *leg = new TLegend(0.1, 0.15, 0.48, 0.4);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry*)(fParaAsymmetries->At(jj));
      paraAsymAverage->Print();
      InSANEMeasuredAsymmetry * paraAsym = paraAsymAverage->GetAsymmetryResult();

      //InSANEMeasuredAsymmetry * perpAsym = ((InSANEMeasuredAsymmetry*)(fPerpAsymmetries->At(jj)));

      TH1F * fApara = paraAsym->fAsymmetryVsx;
      //TH3F * fApara = paraAsym->fAsymmetryVsWxPhi;
      //TH3F * fAperp = perpAsym->fAsymmetryVsQ2xPhi;

      fA1 = new TH1F(*fApara);
      //fA1 = new TH3F(*fApara);
      fA1->SetNameTitle(Form("fA1-%d",jj),Form("A1-%d",jj));

      //fA2 = new TH3F(*fApara);
      //fA2->SetNameTitle(Form("fA2-%d",jj),Form("A2-%d",jj));

      fA1Asymmetries->Add(fA1);
      //fA2Asymmetries->Add(fA2);

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);
               //W     = fA1->GetXaxis()->GetBinCenter(binx);
               x     = fA1->GetXaxis()->GetBinCenter(binx);
               phi   = 0.0;//fA1->GetZaxis()->GetBinCenter(binz);
               Q2    = paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               //std::cout << Q2 << std::endl;
               y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               A180  = fApara->GetBinContent(bin);
               //std::cout << "A180 = " << A180 << std::endl;
               //A80   = fAperp->GetBinContent(bin);

               R        = InSANE::Kine::R1998(x,Q2);
               D        = InSANE::Kine::D(E0,Ep,theta,R);
               d        = InSANE::Kine::d(E0,Ep,theta,R);
               eta      = InSANE::Kine::Eta(E0,Ep,theta);
               xi       = InSANE::Kine::Xi(E0,Ep,theta);
               chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);

               c0 = 1.0/(1.0+eta*xi);
               c11 = (xi + (1.0/TMath::Tan(alpha))*chi)/D ;
               c12 = ( (1.0/TMath::Sin(alpha))*chi)/D ;
               c21 = (xi - (1.0/TMath::Tan(alpha))*chi/eta)/D ;
               c22 = (xi - (1.0/TMath::Tan(alpha))*chi/eta)/(D*(TMath::Cos(alpha)-TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               //std::cout << " c0*c11 - 1/D = " << c0*c11 - 1.0/D << std::endl;
               A1 = A180*(1/D);
               //A1 = c0*(c11*A180);// + c12*A80);
               //A2 = c0*(c21*A180 + c22*A80);

               if ( A1 != A1 ) A1 = 0;
               //if ( A2 != A2 ) A2 = 0;

               //             std::cout << "theta = " << theta << "\n";
               //             std::cout << "A1 = " << A1 << "\n";
               //             
               fA1->SetBinContent(bin,A1);
               //fA2->SetBinContent(bin,A2);

            }
         }
      }
      //       asy->fAsymmetryVsx->SetTitle("A vs x");
      //       asy->fAsymmetryVsx->SetMinimum(-2.0);
      //       asy->fAsymmetryVsx->SetMaximum(2.0);
      //       if(j==0)asy->fAsymmetryVsx->Draw("E1");
      //       else asy->fAsymmetryVsx->Draw("same,E1");
      //       if(j<4)leg->AddEntry(asy->fAsymmetryVsx,Form("<Q^{2}>=%1.1f",QsqAvg[j]),"lp");
      //TH1D* fA1_y = fA1->ProjectionY(Form("%s_py",fA1->GetName()));
      TH1 * h1 = fA1; 
      gr = new TGraphErrors(h1);
      for( int j = 0; j<gr->GetN() ; j++) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt < 1.0e-4 ) {
            std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
            gr->RemovePoint(j);
         }
      }
      for( int j = 0; j<gr->GetN() ; j++) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt < 1.0e-4 ) {
            std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
            gr->RemovePoint(j);
         }
      }
      for( int j = 0; j<gr->GetN() ; j++) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt < 1.0e-4 ) {
            std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
            gr->RemovePoint(j);
         }
      }
      for( int j = 0; j<gr->GetN() ; j++) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt < 1.0e-4 ) {
            std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
            gr->RemovePoint(j);
         }
      }
      for( int j = 0; j<gr->GetN() ; j++) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt < 1.0e-4 ) {
            std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
            gr->RemovePoint(j);
         }
      }
      for( int j = 0; j<gr->GetN() ; j++) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt < 1.0e-4 ) {
            std::cout << " removing point " << j << " = (" << xt << "," << yt << ")" << std::endl;
            gr->RemovePoint(j);
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      mg->Add(gr,"p");
      leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");

      //if(jj==0) fA1_y->Draw("E1,p");
      //else fA1_y->Draw("same,E1,p");

      //c->cd(2);
      //TH1D* fA2_y = fA2->ProjectionY(Form("%s_py",fA2->GetName()));
      //fA2_y->SetMarkerStyle(20);
      //fA2_y->SetMarkerColor(1+jj);
      //if(jj==0) fA2_y->Draw();
      //else fA2_y->Draw("same");

   }

   //c->cd(1);

   Q2 = 4.0;

   fout->WriteObject(fA1Asymmetries,Form("A1-para59-%d",paraRunGroup));//,TObject::kSingleKey); 
   //fout->WriteObject(fA2Asymmetries,Form("A2-%d",paraRunGroup));//,TObject::kSingleKey); 

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);
   
   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  
   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 

   TF1 * Model1 = new TF1("Model1",Asym1,&InSANEAsymmetryBase::EvaluateA1p,
         xmin1, xmax1, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model2 = new TF1("Model2",Asym2,&InSANEAsymmetryBase::EvaluateA1p,
         xmin2, xmax2, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model3 = new TF1("Model3",Asym3,&InSANEAsymmetryBase::EvaluateA1p,
         xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model4= new TF1("Model4",Asym4,&InSANEAsymmetryBase::EvaluateA1p,
         xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");

   TString Title;
   Int_t width = 1;

   Model1->SetParameter(0,Q2);
   Model1->SetLineColor(4);
   Model1->SetLineWidth(width);
   Model1->SetLineStyle(1);
   Model1->SetTitle(Title);
   Model1->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model4->SetParameter(0,Q2);
   Model4->SetLineColor(4);
   Model4->SetLineWidth(width);
   Model4->SetLineStyle(2);
   Model4->SetTitle(Title);
   Model4->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model2->SetParameter(0,Q2);
   Model2->SetLineColor(1);
   Model2->SetLineWidth(width);
   Model2->SetLineStyle(1);
   Model2->SetTitle(Title);
   Model2->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model3->SetParameter(0,Q2);
   Model3->SetLineColor(1);
   Model3->SetLineWidth(width);
   Model3->SetLineStyle(2);
   Model3->SetTitle(Title);
   Model3->GetYaxis()->SetRangeUser(ymin,ymax); 

   // Load in world data on A1He3 from NucDB  
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   TMultiGraph *MG = new TMultiGraph(); 

   TList * measurementsList = manager->GetMeasurements("A1p");
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1He3World = (NucDBMeasurement*)measurementsList->At(i);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1He3World->BuildGraph("x");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A1He3World->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
   }

   leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("A_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(ymin,ymax); 
   MG->Draw("AP");
   Model1->Draw("same");
   Model4->Draw("same");
   Model2->Draw("same");
   Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");
   c->Update();

   c->SaveAs(Form("results/asymmetries/compare_A1_para59_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_A1_para59_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_A1_para59_%d.svg",aNumber));

   //new TBrowser;
   return(0);
}
