Int_t compare_kinematics_47(Int_t paraRunGroup = 23,Int_t perpRunGroup = 23,Int_t aNumber=23){

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/binned_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/binned_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);


   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para47");
   c->Divide(5,2);
   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries()&&jj<5;jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();

      c->cd(jj+1);
      paraAsym->fAvgKineVsxQ2->fN->Draw("colz");

      c->cd(jj+6);
      perpAsym->fAvgKineVsxQ2->fN->Draw("colz");


   }


   c->SaveAs(Form("results/asymmetries/compare_kinematics_47_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_kinematics_47_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_kinematics_47_%d.svg",aNumber));


   return(0);
}
