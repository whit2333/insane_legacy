Int_t compare_g1g2_59(Int_t paraFileVersion = 4306,Int_t perpFileVersion = 4306,Int_t aNumber=4306){

   TFile * f = new TFile(Form("data/A1A2_59_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();

   TList * fA1Asymmetries = (TList*)gROOT->FindObject(Form("A1-59-%d",0));
   if(!fA1Asymmetries) return(-1);
   TList * fA2Asymmetries = (TList*)gROOT->FindObject(Form("A2-59-%d",0));
   if(!fA2Asymmetries) return(-2);
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("ParaAsym-59-%d",0));
   if(!fParaAsymmetries) return(-3);
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("PerpAsym-59-%d",0));
   if(!fPerpAsymmetries) return(-4);

   TList * fg1s = new TList();
   TList * fg2s = new TList();

   Double_t F1,eta,xi,chi,D,d,epsilon,R,cota,R_2;
   Double_t Q2,W,x,y,phi,E0,Ep,theta,alpha,gamma2,gamma;
   Double_t c0,c11,c12,c21,c22;
   Double_t A180,A80,A1,A2,eA1,eA2;
   Double_t g1,g2,eg1,eg2;
   E0 = 5.9;
   alpha = 80.0*TMath::Pi()/180.0;

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TH1F * fg1 = 0;
   TH1F * fg2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;
   //TH3F * fA1 = 0;
   //TH3F * fA2 = 0;
   InSANEStructureFunctions * SFs = new F1F209StructureFunctions();

   /// The asymmetry is calculated A = C1(A180*C2+A80*C3)
   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries->GetEntries();jj++) {

      fA1 = (TH1F*) fA1Asymmetries->At(jj);
      fA2 = (TH1F*) fA2Asymmetries->At(jj);

      fg1 = new TH1F(*fA1);
      fg1->Reset();
      fg1s->Add(fg1);
      fg2 = new TH1F(*fA2);
      fg2->Reset();
      fg2s->Add(fg2);

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry*)(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry * paraAsym = paraAsymAverage->GetMeasuredAsymmetryResult();
      //InSANECombinedAsymmetry * paraAsym = (InSANECombinedAsymmetry*)(fParaAsymmetries->At(jj));

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry*)(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry * perpAsym = perpAsymAverage->GetMeasuredAsymmetryResult();
      //InSANECombinedAsymmetry * perpAsym = (InSANECombinedAsymmetry*)(fPerpAsymmetries->At(jj));

      InSANEAveragedKinematics1D * paraKine = &paraAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsx; 

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);
               Double_t W1     = paraKine->fW.GetBinContent(bin);
               Double_t x1     = paraKine->fx.GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi.GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2.GetBinContent(bin);
               W     = W1;//
               x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
               phi   = phi1;//0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
               Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);


               A1   = fA1->GetBinContent(bin);
               A2   = fA2->GetBinContent(bin);

               eA1  = fA1->GetBinError(bin);
               eA2  = fA2->GetBinError(bin);

               R_2      = InSANE::Kine::R1998(x,Q2);
               R        = SFs->R(x,Q2);
               F1       = SFs->F1p(x,Q2);
               D        = InSANE::Kine::D(E0,Ep,theta,R);
               d        = InSANE::Kine::d(E0,Ep,theta,R);
               eta      = InSANE::Kine::Eta(E0,Ep,theta);
               xi       = InSANE::Kine::Xi(E0,Ep,theta);
               chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
               gamma    = TMath::Sqrt(gamma2);
               cota     = 1.0/TMath::Tan(alpha);
               csca     = 1.0/TMath::Sin(alpha);

               g1 = (F1/(1.0+gamma2))*(A1 + gamma*A2);
               g2 = (F1/(1.0+gamma2))*(A2/gamma - A1);

               //eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
               Double_t a1 = (F1/(1.0+gamma2));
               Double_t a2 = (F1/(1.0+gamma2))*gamma;
               eg1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //eg2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
               a1 = -1.0*(F1/(1.0+gamma2));
               a2 = (F1/(1.0+gamma2))/gamma;
               eg2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //c0 = 1.0/(1.0 + eta*xi);
               //c11 = (1.0 + cota*chi)/D ;
               //c12 = (csca*chi)/D ;
               //c21 = (xi - cota*chi/eta)/D ;
               //c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               //if( TMath::Abs(A80) > 0.0 ) {
               //   std::cout << "(E0,Ep,theta,phi) = (" << E0 << ","
               //                                        << Ep << ","
               //                                        << theta << ","
               //                                        << phi   << ")" << std::endl;  
               //   std::cout << " delta R      = " << R - R_2 << std::endl;
               //   std::cout << " A180         = " << A180 << std::endl;
               //   std::cout << " c0*c11 - 1/D = " << c0*c11 - 1.0/D << std::endl;
               //   std::cout << " D            = " << D << std::endl;
               //   std::cout << " 1/c11        = " << 1.0/c11 << std::endl;
               //   std::cout << " c0           = " << c0 << std::endl;
               //   std::cout << " c11          = " << c11 << std::endl;
               //   std::cout << " 1/D          = " << 1.0/D << std::endl;
               //   std::cout << " c12          = " << c12 << std::endl;
               //   std::cout << " -eta/d       = " << -eta/d << std::endl;
               //   std::cout << " c21          = " << c21 << std::endl;
               //   std::cout << " xi/D         = " <<  xi/D<< std::endl;
               //   std::cout << " c22          = " << c22 << std::endl;
               //   std::cout << " 1.0/d        = " << 1.0/d << std::endl;
               //   std::cout << " chi          = " << chi << std::endl;
               //   
               //}
               //             
               fg1->SetBinContent(bin,g1);
               fg2->SetBinContent(bin,g2);
               fg1->SetBinError(bin, eg1);
               fg2->SetBinError(bin, eg2);

            }
         }
      }
      //       asy->fAsymmetryVsx->SetTitle("A vs x");
      //       asy->fAsymmetryVsx->SetMinimum(-2.0);
      //       asy->fAsymmetryVsx->SetMaximum(2.0);
      //       if(j==0)asy->fAsymmetryVsx->Draw("E1");
      //       else asy->fAsymmetryVsx->Draw("same,E1");
      //       if(j<4)leg->AddEntry(asy->fAsymmetryVsx,Form("<Q^{2}>=%1.1f",QsqAvg[j]),"lp");
      //TH1D* fA1_y = fA1->ProjectionY(Form("%s_py",fA1->GetName()));
      TH1 * h1 = fg2; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }
      gr->SetMarkerStyle(21);
      gr->SetMarkerSize(0.6);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerSize(1.0);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5)mg2->Add(gr,"p");


      h1 = fg1; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.6);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerSize(1.0);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      //if(jj==0) fA1_y->Draw("E1,p");
      //else fA1_y->Draw("same,E1,p");

      //c->cd(2);
      //TH1D* fA2_y = fA2->ProjectionY(Form("%s_py",fA2->GetName()));
      //fA2_y->SetMarkerStyle(20);
      //fA2_y->SetMarkerColor(1+jj);
      //if(jj==0) fA2_y->Draw();
      //else fA2_y->Draw("same");

   }



   Q2 = 4.0;

   //fout->WriteObject(fA1Asymmetries,Form("A1-para59-%d",paraRunGroup));//,TObject::kSingleKey); 
   //fout->WriteObject(fA2Asymmetries,Form("A2-%d",paraRunGroup));//,TObject::kSingleKey); 

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);
   
   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  
   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 

   // --------------- A1 ----------------------
   c->cd(1);
   //TF1 * Model1 = new TF1("Model1",Asym1,&InSANEAsymmetryBase::EvaluateA1p,
   //      xmin1, xmax1, npar,"InSANEAsymmetriesFromStructureFunctions","Evaluateg1p");
   //TF1 * Model2 = new TF1("Model2",Asym2,&InSANEAsymmetryBase::EvaluateA1p,
   //      xmin2, xmax2, npar,"InSANEAsymmetriesFromStructureFunctions","Evaluateg1p");
   //TF1 * Model3 = new TF1("Model3",Asym3,&InSANEAsymmetryBase::EvaluateA1p,
   //      xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","Evaluateg1p");
   //TF1 * Model4= new TF1("Model4",Asym4,&InSANEAsymmetryBase::EvaluateA1p,
   //      xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","Evaluateg1p");

   Int_t width = 1;

   //Model1->SetParameter(0,Q2);
   //Model1->SetLineColor(4);
   //Model1->SetLineWidth(width);
   //Model1->SetLineStyle(1);
   //Model1->SetTitle(Title);
   //Model1->GetYaxis()->SetRangeUser(ymin,ymax); 

   //Model4->SetParameter(0,Q2);
   //Model4->SetLineColor(4);
   //Model4->SetLineWidth(width);
   //Model4->SetLineStyle(2);
   //Model4->SetTitle(Title);
   //Model4->GetYaxis()->SetRangeUser(ymin,ymax); 

   //Model2->SetParameter(0,Q2);
   //Model2->SetLineColor(1);
   //Model2->SetLineWidth(width);
   //Model2->SetLineStyle(1);
   //Model2->SetTitle(Title);
   //Model2->GetYaxis()->SetRangeUser(ymin,ymax); 

   //Model3->SetParameter(0,Q2);
   //Model3->SetLineColor(1);
   //Model3->SetLineWidth(width);
   //Model3->SetLineStyle(2);
   //Model3->SetTitle(Title);
   //Model3->GetYaxis()->SetRangeUser(ymin,ymax); 

   // Load in world data on A1He3 from NucDB  
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   TMultiGraph *MG = new TMultiGraph(); 
   TList * measurementsList = manager->GetMeasurements("g1p");
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1He3World = (NucDBMeasurement*)measurementsList->At(i);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1He3World->BuildGraph("x");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A1He3World->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
   }
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("g_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.1,ymax); 
   MG->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   c->cd(2);
   TMultiGraph *MG2 = new TMultiGraph(); 
   measurementsList = manager->GetMeasurements("g2p");
   //measurementsList->Print();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
      //A2pMeas->PrintData();
      // Get TGraphErrors object 
      TGraphErrors *graph        = A2pMeas->BuildGraph("x");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A2pMeas->GetExperimentName()); 
      //leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 
   }
   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary g_{2}^{p}, E=5.9 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   MG2->GetYaxis()->SetRangeUser(-0.95,0.95); 
   MG2->GetYaxis()->SetTitle("g_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   c->Update();

   c->SaveAs(Form("results/asymmetries/compare_g1g2_59_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_g1g2_59_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_g1g2_59_%d.svg",aNumber));

   //fout->WriteObject(fA1Asymmetries,Form("A1-59-%d",0));//,TObject::kSingleKey);
   //fout->WriteObject(fA2Asymmetries,Form("A2-59-%d",0));//,TObject::kSingleKey);

   return(0);
}
