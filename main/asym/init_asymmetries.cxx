#include "asym/sane_data_bins.cxx"

TList * init_asymmetries(InSANEDISEvent * dis, const InSANERunSummary & rSum, InSANEDilutionFromTarget * df){

//   if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
//      return 0;
//   }
   // Create the asymmetries to be calculated.
   // ------------------------------------------------
   TList *  fAsymmetries = new TList();
   //Double_t fSANEQsqMin[4] = {1.40,2.00,3.45,4.67};
   //Double_t fSANEQsqMax[4] = {2.00,3.45,4.67,7.31};
   //Double_t fSANEQsqMin[4] = {1.89,2.55,3.45,4.67};
   //Double_t fSANEQsqMax[4] = {2.55,3.45,4.67,6.31};
   //Double_t fSANEQsqAvg[4];
   SANEMeasuredAsymmetry * fAsymmetry = 0;

   // Asymmetries (0-3)
   // 4 Q2 bins
   for(int i=0;i<fSANENBins;i++){
      fAsymmetry              = new SANEMeasuredAsymmetry(i);
      fAsymmetry->fQ2Min      = fSANEQsqMin[i];
      fAsymmetry->fQ2Max      = fSANEQsqMax[i];
      fAsymmetry->fGroup      = 0;
      fAsymmetry->fDISEvent   = dis;
      fAsymmetry->fRunSummary = rSum;
      fAsymmetry->SetDilutionFromTarget(df);
      fAsymmetry->Initialize();
      fAsymmetries->Add(fAsymmetry);
      //fSANEQsqAvg[i] = (fSANEQsqMax[i]+fSANEQsqMin[i])/2.0;
   }
   // Asymmetry (4)
   // now one covering all Q2 bins
   fAsymmetry              = new SANEMeasuredAsymmetry(fAsymmetries->GetEntries());
   fAsymmetry->fQ2Min      = fSANEQsqMin[0];
   fAsymmetry->fQ2Max      = fSANEQsqMax[fSANENBins-1];
   fAsymmetry->fGroup      = 0;
   fAsymmetry->fDISEvent   = dis;
   fAsymmetry->fRunSummary = rSum;
   fAsymmetry->SetDilutionFromTarget(df);
   fAsymmetry->Initialize();
   fAsymmetries->Add(fAsymmetry);

   // ----------------------------------------------
   // Asymmetries (5-8)
   // RCS Group
   for(int i=0;i<fSANENBins;i++){
      fAsymmetry              = new SANEMeasuredAsymmetry(i+5);
      fAsymmetry->fQ2Min      = fSANEQsqMin[i];
      fAsymmetry->fQ2Max      = fSANEQsqMax[i];
      fAsymmetry->fGroup      = -1;
      fAsymmetry->fGroups.push_back(1);
      fAsymmetry->fDISEvent   = dis;
      fAsymmetry->fRunSummary = rSum;
      fAsymmetry->SetDilutionFromTarget(df);
      fAsymmetry->Initialize();
      fAsymmetries->Add(fAsymmetry);
      //fSANEQsqAvg[i] = (fSANEQsqMax[i]+fSANEQsqMin[i])/2.0;
   }
   // Asymmetry (9)
   // now one covering all Q2 bins
   fAsymmetry              = new SANEMeasuredAsymmetry(fAsymmetries->GetEntries());
   fAsymmetry->fQ2Min      = fSANEQsqMin[0];
   fAsymmetry->fQ2Max      = fSANEQsqMax[fSANENBins-1];
   fAsymmetry->fGroup      = -1;
   fAsymmetry->fGroups.push_back(1);
   fAsymmetry->fDISEvent   = dis;
   fAsymmetry->fRunSummary = rSum;
   fAsymmetry->SetDilutionFromTarget(df);
   fAsymmetry->Initialize();
   fAsymmetries->Add(fAsymmetry);

   // ----------------------------------------------
   // Protvino 
   // Asymmetries (10-13)
   for(int i=0;i<fSANENBins;i++){
      fAsymmetry              = new SANEMeasuredAsymmetry(i+10);
      fAsymmetry->fQ2Min      = fSANEQsqMin[i];
      fAsymmetry->fQ2Max      = fSANEQsqMax[i];
      fAsymmetry->fGroup      = -1;
      fAsymmetry->fGroups.push_back(2);
      fAsymmetry->fDISEvent   = dis;
      fAsymmetry->fRunSummary = rSum;
      fAsymmetry->SetDilutionFromTarget(df);
      fAsymmetry->Initialize();
      fAsymmetries->Add(fAsymmetry);
      //fSANEQsqAvg[i] = (fSANEQsqMax[i]+fSANEQsqMin[i])/2.0;
   }
   // Asymmetry (14)
   // now one covering all Q2 bins
   fAsymmetry              = new SANEMeasuredAsymmetry(fAsymmetries->GetEntries());
   fAsymmetry->fQ2Min      = fSANEQsqMin[0];
   fAsymmetry->fQ2Max      = fSANEQsqMax[fSANENBins-1];
   fAsymmetry->fGroup      = -1;
   fAsymmetry->fGroups.push_back(2);
   fAsymmetry->fDISEvent   = dis;
   fAsymmetry->fRunSummary = rSum;
   fAsymmetry->SetDilutionFromTarget(df);
   fAsymmetry->Initialize();
   fAsymmetries->Add(fAsymmetry);


   // ----------------------------------------------
   // now we split each section into two more groups 
   // but only use the full Q2 range
   // Asymmetries (15-18)
   for(int i=0; i<fSANENBins ; i++ ){ 
      fAsymmetry              = new SANEMeasuredAsymmetry(fAsymmetries->GetEntries());
      fAsymmetry->fQ2Min      = fSANEQsqMin[0];
      fAsymmetry->fQ2Max      = fSANEQsqMax[fSANENBins-1];
      fAsymmetry->fGroup      = -1;
      fAsymmetry->fGroups.push_back(35+i);
      fAsymmetry->fDISEvent   = dis;
      fAsymmetry->fRunSummary = rSum;
      fAsymmetry->SetDilutionFromTarget(df);
      fAsymmetry->Initialize();
      fAsymmetries->Add(fAsymmetry);
   }
   // ----------------------------------------------

   return fAsymmetries;
}
