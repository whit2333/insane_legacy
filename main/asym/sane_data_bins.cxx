// --------------------------------------------------------
// NucDB Q2 bins
TList  * fSANEQ2Bins    = 0;

// --------------------------------------------------------
// These arrays define the Q2 bins for the whole experiment
//const Int_t  fSANENBins = 12;
//Double_t fSANEQsqMin[fSANENBins] = {1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5};
//Double_t fSANEQsqMax[fSANENBins] = {1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.5};
//Double_t fSANEQsqAvg[fSANENBins] = {0.0 , 0.0 , 0.0 , 0.0,
//                           0.0 , 0.0 , 0.0 , 0.0,
//                           0.0 , 0.0 , 0.0 , 0.0};
const Int_t  fSANENBins = 4;
std::array<double,4> fSANEQsqMin = {0.5  , 2.0 , 2.70 , 3.4};                                                                           
std::array<double,4> fSANEQsqMax = {2.0  , 2.7 , 3.40 , 7.50};   
//std::array<double,4> fSANEQsqMin = {1.00 , 2.25 , 3.50 , 5.0};
//std::array<double,4> fSANEQsqMax = {2.25 , 3.50 , 5.00 , 7.50};
std::array<double,4> fSANEQsqAvg = {0.0 , 0.0 , 0.0 , 0.0};

Int_t sane_data_bins(){

   fSANEQ2Bins = new TList();
   fSANEQ2Bins->SetOwner(false);
   //Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   //Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   //Double_t QsqMin[4] = {1.40,2.00,3.45,4.67};
   //Double_t QsqMax[4] = {2.00,3.45,4.67,7.31};
   //Double_t QsqAvg[4];
   for(int i=0;i<fSANENBins;i++){
      nucdb::BinnedVariable * aQ2bin = new nucdb::BinnedVariable("Qsquared","Q^{2}");
      aQ2bin->SetBinMinimum(fSANEQsqMin[i]);
      aQ2bin->SetBinMaximum(fSANEQsqMax[i]);
      fSANEQsqAvg[i] = (fSANEQsqMax[i]+fSANEQsqMin[i])/2.0;
      aQ2bin->SetMean(fSANEQsqAvg[i]);
      aQ2bin->SetAverage(fSANEQsqAvg[i]);
      fSANEQ2Bins->Add(aQ2bin);
   }

   nucdb::BinnedVariable * aQ2bin = new nucdb::BinnedVariable("Qsquared","Q^{2}");
   aQ2bin->SetBinMinimum(2.0);
   aQ2bin->SetBinMaximum(7.0);
   aQ2bin->SetMean((7.0+2.0)/2.0);
   aQ2bin->SetAverage((7.0+2.0)/2.0);
   fSANEQ2Bins->Add(aQ2bin);

   aQ2bin = new nucdb::BinnedVariable("Qsquared","Q^{2}");
   aQ2bin->SetBinMinimum(1.5);
   aQ2bin->SetBinMaximum(8.5);
   aQ2bin->SetMean((1.5+8.5)/2.0);
   aQ2bin->SetAverage((1.5+8.5)/2.0);
   fSANEQ2Bins->Add(aQ2bin);

   aQ2bin = new nucdb::BinnedVariable("Qsquared","Q^{2}");
   aQ2bin->SetBinMinimum(3.0);
   aQ2bin->SetBinMaximum(8.5);
   aQ2bin->SetMean((3.0+8.5)/2.0);
   aQ2bin->SetAverage((3.0+8.5)/2.0);
   fSANEQ2Bins->Add(aQ2bin);

   return(0);
}
