Int_t browse_by_run(Int_t fileVersion = 30){

   gROOT->LoadMacro("radiative_corrections/init_xsections.cxx"); 

   //TFile * f = new TFile(Form("data/binned_asymmetries_perp47_%d.root",fileVersion),"UPDATE");
   TFile * f = new TFile(Form("data/binned_asymmetries_perp59_%d.root",fileVersion),"READ");
   //TFile * f = new TFile(Form("data/binned_asymmetries_para59_%d.root",fileVersion),"READ");
   f->cd();

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/perp/5_9GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/perp/5_9GeV/whits_run_list.txt");
   //aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/para/5_9GeV/good_NH3.txt");

   Int_t             nAsym          = 4;
   TList           * fCombinedAsyms = new TList();
   TLegend         * leg            = new TLegend(0.1,0.7,0.3,0.9);
   TMultiGraph     * mg             = new TMultiGraph();

   TCanvas * c = new TCanvas();

   for(int i = 0;i<aman->fRunQueue->size() ;i++) {

      Int_t runnumber = aman->fRunQueue->at(i) ;
      std::cout << " RUN : " << runnumber << "\n";

      TList * fAsymmetries = (TList*) gROOT->FindObject(Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 
      if(!fAsymmetries) continue;

      //  Q2 bin.
      for(int j = 0; j< fAsymmetries->GetEntries(); j++) {

         std::cout << " asym : " << j << std::endl; 
         SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));

         TH1F * h1 = asy->fAsymmetryVsE;
         //TH1F * h1 = asy->fAsymmetryVsW;
         //TH1F * h1 = asy->fAsymmetryVsx;
         TGraphErrors * gr = GetCleanGraph(h1); 
         gr->SetLineWidth(1);
         gr->SetMarkerSize(0.5);

         mg->Add(gr,"p");
         

      }

      mg->Draw("a");
      mg->GetHistogram()->SetTitle(Form("%d",runnumber));
      mg->SetMinimum(-3.0);
      mg->SetMaximum(3.0);
      gPad->Modified();
      //mg->Draw("a");

      c->SaveAs("results/asymmetries/browse_by_run_x.gif+99");
      //gPad->WaitPrimitive();
      //gSystem->Sleep(1000);

      mg->GetListOfGraphs()->Clear();
   }


   return(0);
}
