Int_t compare_x2g1g2_59(Int_t paraRunGroup = 4306,Int_t perpRunGroup = 4306,Int_t aNumber=4306){

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);
   TFile * f = new TFile(Form("data/A1A2_59_%d_%d.root",paraRunGroup,perpRunGroup),"UPDATE");
   f->cd();

   TList * fA1Asymmetries = (TList*)gROOT->FindObject(Form("A1-59-%d",0));
   if(!fA1Asymmetries) return(-1);
   TList * fA2Asymmetries = (TList*)gROOT->FindObject(Form("A2-59-%d",0));
   if(!fA2Asymmetries) return(-2);
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("ParaAsym-59-%d",0));
   if(!fParaAsymmetries) return(-3);
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("PerpAsym-59-%d",0));
   if(!fPerpAsymmetries) return(-4);

   std::cout << fA1Asymmetries->GetEntries() << " A1 entries\n";
   std::cout << fA2Asymmetries->GetEntries() << " A2 entries\n";
   std::cout << fParaAsymmetries->GetEntries() << " para entries\n";
   std::cout << fPerpAsymmetries->GetEntries() << " perp entries\n";

   //fA1Asymmetries->Print();
   //fA2Asymmetries->Print();

   TList * fg1s = new TList();
   TList * fg2s = new TList();
   TList * fd2s = new TList();

   //Double_t F1,eta,xi,chi,D,d,epsilon,R,cota,R_2;
   //Double_t Q2,W,x,y,phi,E0,Ep,theta,alpha,gamma2,gamma;
   //Double_t c0,c11,c12,c21,c22;
   //Double_t A180,A80,A1,A2,eA1,eA2;
   //Double_t g1,g2,eg1,eg2;
   //Double_t d2,ed2;
   Double_t E0 = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;
   TGraphErrors * gr3 = 0;

   InSANEStructureFunctions * SFs = new F1F209StructureFunctions();

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();
   TMultiGraph * mgSplit = new TMultiGraph();
   TMultiGraph * mgSplit2 = new TMultiGraph();
   TMultiGraph * mgSplit3 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries->GetEntries();jj++) {

      TH1F * fA1 = (TH1F*) fA1Asymmetries->At(jj);
      TH1F * fA2 = (TH1F*) fA2Asymmetries->At(jj);

      TH1F * fg1 = new TH1F(*fA1);
      fg1->Reset();
      fg1s->Add(fg1);
      TH1F * fg2 = new TH1F(*fA2);
      fg2->Reset();
      fg2s->Add(fg2);

      TH1F * fd2 = new TH1F(*fA1);
      fd2->Reset();
      fd2s->Add(fd2);

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry*)(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry * paraAsym = paraAsymAverage->GetMeasuredAsymmetryResult();
      //InSANECombinedAsymmetry * paraAsym = (InSANECombinedAsymmetry*)(fParaAsymmetries->At(jj));

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry*)(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry * perpAsym = perpAsymAverage->GetMeasuredAsymmetryResult();
      //InSANECombinedAsymmetry * perpAsym = (InSANECombinedAsymmetry*)(fPerpAsymmetries->At(jj));

      InSANEAveragedKinematics1D * paraKine = &paraAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsx; 

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   binx,biny,binz;


      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               int bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);
               //W     = fA1->GetXaxis()->GetBinCenter(binx);
               Double_t W1     = paraKine->fW.GetBinContent(bin);
               Double_t x1     = paraKine->fx.GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi.GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2.GetBinContent(bin);
               double x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
               double phi   = 0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
               double Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               double y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               double Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               double theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               double A1   = fA1->GetBinContent(bin);
               double A2   = fA2->GetBinContent(bin);

               double eA1  = fA1->GetBinError(bin);
               double eA2  = fA2->GetBinError(bin);

               double R_2      = InSANE::Kine::R1998(x,Q2);
               double R        = SFs->R(x,Q2);
               double F1       = SFs->F1p(x,Q2);
               double D        = InSANE::Kine::D(E0,Ep,theta,R);
               double d        = InSANE::Kine::d(E0,Ep,theta,R);
               double eta      = InSANE::Kine::Eta(E0,Ep,theta);
               double xi       = InSANE::Kine::Xi(E0,Ep,theta);
               double chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               double epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               double gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
               double gamma    = TMath::Sqrt(gamma2);
               double cota     = 1.0/TMath::Tan(alpha);
               double csca     = 1.0/TMath::Sin(alpha);

               double g1 = x*x*(F1/(1.0+gamma2))*(A1 + gamma*A2);
               double g2 = x*x*(F1/(1.0+gamma2))*(A2/gamma - A1);
               
               //eg1 = x*x*(F1/(1.0+gamma2))*(eA1 + gamma*eA2);
               //eg2 = x*x*(F1/(1.0+gamma2))*(eA2/gamma - eA1);
               double a1 = x*x*(F1/(1.0+gamma2));
               double a2 = x*x*(F1/(1.0+gamma2))*gamma;
               double eg1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               a1 = -1.0*x*x*(F1/(1.0+gamma2));
               a2 = x*x*(F1/(1.0+gamma2))/gamma;
               double eg2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));

               double d2 = x*x*(2.0*g1 + 3.0*g2);
               double ed2 = TMath::Sqrt(TMath::Power(x*x*(2.0*eg1),2.0) + TMath::Power(x*x*3.0*eg2,2.0));

               //c0 = 1.0/(1.0 + eta*xi);
               //c11 = (1.0 + cota*chi)/D ;
               //c12 = (csca*chi)/D ;
               //c21 = (xi - cota*chi/eta)/D ;
               //c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               //if( TMath::Abs(A80) > 0.0 ) {
               //   std::cout << "(E0,Ep,theta,phi) = (" << E0 << ","
               //                                        << Ep << ","
               //                                        << theta << ","
               //                                        << phi   << ")" << std::endl;  
               //std::cout << " delta R      = " << R - R_2 << std::endl;
               //std::cout << " A180         = " << A180 << std::endl;
               //std::cout << " c0*c11 - 1/D = " << c0*c11 - 1.0/D << std::endl;
               //std::cout << " D            = " << D << std::endl;
               //std::cout << " 1/c11        = " << 1.0/c11 << std::endl;
               //std::cout << " c0           = " << c0 << std::endl;
               //std::cout << " c11          = " << c11 << std::endl;
               //std::cout << " 1/D          = " << 1.0/D << std::endl;
               //std::cout << " c12          = " << c12 << std::endl;
               //std::cout << " -eta/d       = " << -eta/d << std::endl;
               //std::cout << " c21          = " << c21 << std::endl;
               //std::cout << " xi/D         = " <<  xi/D<< std::endl;
               //std::cout << " c22          = " << c22 << std::endl;
               //std::cout << " 1.0/d        = " << 1.0/d << std::endl;
               //std::cout << " chi          = " << chi << std::endl;
               //   
               //}
               //             
               fg1->SetBinContent(bin,g1);
               fg2->SetBinContent(bin,g2);
               fd2->SetBinContent(bin,d2);
               fg1->SetBinError(bin, eg1);
               fg2->SetBinError(bin, eg2);
               fd2->SetBinError(bin, ed2);

            }
         }
      }

      TH1 * h2 = fg2; 
      gr2 = new TGraphErrors(h2);
      for( int j = gr2->GetN()-1; j>=0; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr2->RemovePoint(j);
         }
      }

      TH1 * h1 = fg1; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      TH1 * h3 = fd2; 
      gr3 = new TGraphErrors(h3);
      for( int j = gr3->GetN()-1; j>=0; j--) {
         Double_t xt, yt;
         gr3->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr3->RemovePoint(j);
         }
      }


      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      if(jj%5==4){
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
         gr->SetMarkerStyle(24);
      }
      if(jj<5){

      }else if(jj<10) {
         gr->SetMarkerStyle(32);
      }else {
         gr->SetMarkerStyle(26);
      }
      if(jj<5){
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }else {
         mgSplit->Add(gr,"p");
      }
      gr2->SetMarkerStyle(21);
      gr2->SetMarkerSize(0.8);
      if(jj%5==4){
         gr2->SetMarkerStyle(24);
         gr2->SetMarkerSize(1.0);
         gr2->SetMarkerColor(1);
         gr2->SetLineColor(1);
      }
      if(jj>5) gr2->SetMarkerStyle(32);
      if(jj>10) gr2->SetMarkerStyle(26);
      if(jj<5)mg2->Add(gr2,"p");
      else mgSplit2->Add(gr2,"p");

      gr3->SetMarkerSize(0.8);
      gr3->SetMarkerStyle(21);
      if(jj%5==4){
         gr3->SetMarkerStyle(24);
         gr3->SetMarkerSize(1.0);
         gr3->SetMarkerColor(1);
         gr3->SetLineColor(1);
      }
      if(jj>5) gr3->SetMarkerStyle(32);
      if(jj>10) gr3->SetMarkerStyle(26);
      if(jj<5)mg3->Add(gr3,"p");
      else mgSplit3->Add(gr3,"p");

   }

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   c->Divide(1,2);

   Double_t Q2 = 4.0;

   //fout->WriteObject(fA1Asymmetries,Form("A1-para59-%d",paraRunGroup));//,TObject::kSingleKey); 
   //fout->WriteObject(fA2Asymmetries,Form("A2-%d",paraRunGroup));//,TObject::kSingleKey); 

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);
   
   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  
   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 

   // --------------- A1 ----------------------
   c->cd(1);
   TF1 * Model1 = new TF1("Model1",DSSVSF,&InSANEPolarizedStructureFunctions::Evaluatex2g1p,
         xmin1, xmax1, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   TF1 * Model2 = new TF1("Model2",DSSVSF,&InSANEPolarizedStructureFunctions::Evaluatex2g1p,
         xmin2, xmax2, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   TF1 * Model3 = new TF1("Model3",DSSVSF,&InSANEPolarizedStructureFunctions::Evaluatex2g2pWW,
         xmin3, xmax3, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * Model4= new TF1("Model4",DSSVSF,&InSANEPolarizedStructureFunctions::Evaluatex2g2pWW,
         xmin3, xmax3, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");

   Int_t width = 1;

   Model1->SetParameter(0,Q2);
   Model1->SetLineColor(4);
   Model1->SetLineWidth(width);
   Model1->SetLineStyle(1);
   Model1->SetTitle(Title);
   Model1->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model4->SetParameter(0,Q2);
   Model4->SetLineColor(4);
   Model4->SetLineWidth(width);
   Model4->SetLineStyle(2);
   Model4->SetTitle(Title);
   Model4->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model2->SetParameter(0,Q2);
   Model2->SetLineColor(1);
   Model2->SetLineWidth(width);
   Model2->SetLineStyle(1);
   Model2->SetTitle(Title);
   Model2->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model3->SetParameter(0,Q2);
   Model3->SetLineColor(1);
   Model3->SetLineWidth(width);
   Model3->SetLineStyle(2);
   Model3->SetTitle(Title);
   Model3->GetYaxis()->SetRangeUser(ymin,ymax); 

   // Load in world data on A1He3 from NucDB  
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   TF2 * x2_func  = new TF2("x2func","y*x*x",0,1,-1,1);
   TF2 * x2_func2 = new TF2("x2func","1.5*y*x*x",0,1,-1,1);

   // Q2 bin for filtering data
   NucDBBinnedVariable Q2Filter("Qsquared","Q^{2}");
   Q2Filter.SetBinMinimum(2.5);
   Q2Filter.SetBinMaximum(6.5);

   TMultiGraph *MG = new TMultiGraph(); 
   TList * measurementsList = manager->GetMeasurements("g1p");
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1World = (NucDBMeasurement*)measurementsList->At(i);
      A1World->ApplyFilterWithBin(&Q2Filter);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1World->BuildGraph("x");
      if(graph->GetN() == 0) continue;
      graph->Apply(x2_func);
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A1World->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
   }
   //leg->AddEntry(Model1,Form("DSSV model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("DSSV model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("x^{2}g_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.05,0.05); 
   MG->Draw("AP");
   //Model1->Draw("same");
   //Model2->Draw("same");
   //Model4->Draw("same");
   //Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   c->cd(2);
   TMultiGraph *MG2 = new TMultiGraph(); 
   measurementsList = manager->GetMeasurements("g2p");
   //measurementsList->Print();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
      A2pMeas->ApplyFilterWithBin(&Q2Filter);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A2pMeas->BuildGraph("x");
      if(graph->GetN() == 0) continue;
      graph->Apply(x2_func2);
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A2pMeas->GetExperimentName()); 
      //leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 
   }
   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary x^{2}g_{2}^{p}, E=5.9 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   MG2->GetYaxis()->SetRangeUser(-0.05,0.05); 
   MG2->GetYaxis()->SetTitle("x^{2}g_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   Model4->Draw("same");
   Model3->Draw("same");
   c->Update();

   //c->cd(3);
   //mg3->Draw("a");
   //mg3->GetXaxis()->SetLimits(0.0,1.0); 
   //mg3->GetYaxis()->SetRangeUser(-0.09,0.09); 
   //mg3->GetYaxis()->SetTitle("d_{2}^{p}");
   //mg3->GetXaxis()->SetTitle("x");
   //mg3->GetXaxis()->CenterTitle();
   //mg3->GetYaxis()->CenterTitle();
   //c->Update();

   c->SaveAs(Form("results/asymmetries/compare_x2g1g2_59_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_x2g1g2_59_%d.png",aNumber));

   // ----------------------------------------------
   TCanvas * c2 = new TCanvas("cA1A2_4pass2","A1_para592");
   c2->Divide(1,2);

   c2->cd(1);
   mgSplit->Draw("A");
   mgSplit->SetTitle(GTitle);
   mgSplit->GetXaxis()->SetTitle(xAxisTitle);
   mgSplit->GetXaxis()->CenterTitle();
   mgSplit->GetYaxis()->SetTitle(yAxisTitle);
   mgSplit->GetYaxis()->CenterTitle();
   mgSplit->GetXaxis()->SetLimits(0.0,1.0); 
   mgSplit->GetYaxis()->SetRangeUser(-0.05,0.05); 
   mgSplit->Draw("A");

   c2->cd(2);
   mgSplit2->Draw("A");
   mgSplit2->SetTitle(GTitle);
   mgSplit2->GetXaxis()->SetTitle(xAxisTitle);
   mgSplit2->GetXaxis()->CenterTitle();
   mgSplit2->GetYaxis()->SetTitle(yAxisTitle);
   mgSplit2->GetYaxis()->CenterTitle();
   mgSplit2->GetXaxis()->SetLimits(0.0,1.0); 
   mgSplit2->GetYaxis()->SetRangeUser(-0.05,0.05); 
   mgSplit2->Draw("A");

   c2->SaveAs(Form("results/asymmetries/compare_x2g1g2_59_split_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/compare_x2g1g2_59_split_%d.png",aNumber));

   //fout->WriteObject(fA1Asymmetries,Form("A1-59-%d",0));//,TObject::kSingleKey);
   //fout->WriteObject(fA2Asymmetries,Form("A2-59-%d",0));//,TObject::kSingleKey);

   return(0);
}
