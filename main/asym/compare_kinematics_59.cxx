Int_t compare_kinematics_59(Int_t paraRunGroup = 23,Int_t perpRunGroup = 23,Int_t aNumber=23){

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/binned_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/binned_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);


   TMultiGraph * mg1 = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();
   TMultiGraph * mg4 = new TMultiGraph();
   TGraph * gr = 0;
   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   c->Divide(2,2);
   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries()&&jj<4;jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();


      InSANEAveragedKinematics1D * paraKine = paraAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsx;

      gr = new TGraph(paraKine->fE);
      gr->SetLineColor( paraAsym->fAsymmetryVsx->GetLineColor());
      mg1->Add(gr,"l");

      gr = new TGraph(paraKine->fE);
      gr->SetLineColor( paraAsym->fAsymmetryVsx->GetLineColor());
      mg2->Add(gr,"l");

      gr = new TGraph(paraKine->fTheta);
      gr->SetLineColor( paraAsym->fAsymmetryVsx->GetLineColor());
      mg3->Add(gr,"l");

      gr = new TGraph(paraKine->fTheta);
      gr->SetLineColor( paraAsym->fAsymmetryVsx->GetLineColor());
      mg4->Add(gr,"l");

   }

   c->cd(1);
   mg1->Draw("a");

   c->cd(2);
   mg2->Draw("a");

   c->cd(3);
   mg3->Draw("a");

   c->cd(4);
   mg4->Draw("a");

   c->SaveAs(Form("results/asymmetries/compare_kinematics_59_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_kinematics_59_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_kinematics_59_%d.svg",aNumber));


   return(0);
}
