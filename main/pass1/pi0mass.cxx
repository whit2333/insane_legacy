Int_t pi0mass(Int_t runnumber=72994) {

   const char * detectorTree = "betaDetectors1";
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

   TTree * fPi0Tree = (TTree*)gROOT->FindObject("pi0results");
   if(!fPi0Tree) {printf("fPi0Tree tree not found\n"); return(-1); }

   TCanvas * c = new TCanvas("Pi0Mass","Pi0 Mass");
   fPi0Tree->Draw("fReconstruction.fAngle:fReconstruction.fMass","fReconstruction.fMass<300","colz");
   c->SaveAs(Form("plots/%d/pi0Mass.png",runnumber));

   TCanvas * c2 = new TCanvas("Pi0Mass2","Pi0 Mass2");
   fPi0Tree->Draw("fClusterPairs.fMass:(fClusterPairs.fCluster1.fCherenkovBestADCSum+fClusterPairs.fCluster2.fCherenkovBestADCSum)>>hCerVMass(20,0.5,6.5,20,0,300)",
         "fClusterPairs.fCluster1.fXMoment>-55&&fClusterPairs.fCluster2.fXMoment>-55","colz");
   c2->SaveAs(Form("plots/%d/pi0Mass_c2.png",runnumber));

   //fPi0Tree->StartViewer();


/*
  Pi0Event * fPi0Event = new Pi0Event();
    fPi0Tree->SetBranchAddress("pi0reconstruction",&fPi0Event);

  TFile*f = new TFile("data/rootfiles/pi0mass.root","UPDATE");

  f->cd();

  TTree * fpi0mass = (TTree*)gROOT->FindObject("pi0mass");

  if(!fpi0mass) {
    fpi0mass = new TTree("pi0mass","pi0 reconstruction accumulated");
    fpi0mass->Branch("pi0Event","Pi0Event",&fPi0Event);
  } else {
    fpi0mass->SetBranchAddress("pi0Event",&fPi0Event);
  }

  for(int i = 0; i< fPi0Tree->GetEntries()  ;i++) {
    fPi0Tree->GetEntry(i);
    if(1) {
      fpi0mass->Fill();
    }
  }

  f->Write();
// fpi0mass->StartViewer();*/

  return(0);
}
