/** The first pass root script.
 *  Run this in root in the `main` directory. 
 * 
 * @defgroup FirstPassScriptGroup The First Pass Analysis Script
 * @{
 *
 */
Int_t FirstPass(Int_t runNumber = 72994) {

   ///
   /// <h3>First Pass Analysis script</h3>
   ///
   //  - Define the analyzer for this pass
   //  - Create each detector used by the analysis and add it to the analyzer. Currently all the detectors are created in InSANEDetectorAnalysis
   //  - Define and setup each "Correction" and "Calculation".
   //  - Set each detector used by the analysis
   //  - Add the corrections and calculations 
   //  - Initialze and Process 
   /// 
   ///   \note Order Matters. It is the execution of the list from first to last. 
   /// 

   Bool_t    allowEventDisplay = false;
   const char * sourceTreeName = "betaDetectors0";
   const char * outputTreeName = "betaDetectors1";

   rman->SetRun(runNumber);

   /// 
   /// <h4>Trees created by this analysis pass</h4>
   ///  - Scalers1
   ///  - betaDetectors1 
   ///  - uncorrelatedPositions 
   ///  - Tracks 
   ///  - pi0reslults
   ///  - DISElectrons 
   /// 

   // -------------------------------------
   // Before running, clean up the file existing trees.
   //
   rman->GetScalerFile()->Delete( "Scalers1;*");
   rman->GetCurrentFile()->Delete("betaDetectors1;*");
   rman->GetCurrentFile()->Delete("Tracks;*");
   rman->GetCurrentFile()->Delete("pi0results;*");
   rman->GetCurrentFile()->Delete("3clusterPi0results;*");
   rman->GetCurrentFile()->Delete("cleanedEvents;*");
   rman->GetCurrentFile()->Delete("DISElectrons;*");

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   Int_t cal_series = bcgeo->GetLatestCalibrationSeries(runNumber);
   bcgeo->SetCalibrationSeries(cal_series);

   // ----------------------------------------------------------------------
   // - Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
   //
   std::cout << " + Creating SANEFirstPassAnalyzer\n";
   SANEFirstPassAnalyzer * analyzer = new SANEFirstPassAnalyzer(outputTreeName ,sourceTreeName);
   analyzer->SetNPrintUpdate(50000);
   aman->SetAnalyzer(analyzer);

   // ----------------------------------------------------------------------
   // Add analysis scripts to run at the end of processing.
   //
   analyzer->AddScript("pass1/bigcalCherenkov.cxx");
   analyzer->AddScript("pass1/pi0mass.cxx");
   //analyzer->AddScript("pass1/RunAppend.cxx");
   //analyzer->AddScript("pass1/luciteBarGeometry.cxx");
   //analyzer->AddScript("pass1/findMirrorEdges.cxx");
   analyzer->AddScript("pass1/cherenkov.cxx");
   //analyzer->AddScript("pass1/cherenkov2.cxx");
   //analyzer->AddScript("detectors/cherenkov/tracking_no_shower.cxx");
   //analyzer->AddScript("detectors/bigcal/bigcal_timing.cxx");
   //analyzer->AddScript("pass1/run_asymmetries.cxx");
   //analyzer->AddScript("detectors/hodoscope/hodoscope_hit_efficiency.cxx");
   //analyzer->AddScript("pass1/avg_dilution.cxx");

   // Filters and skimmers which may raise flags
   InSANESlowSkimmer * skimmer          = new InSANESlowSkimmer();
   InSANEBeamCurrentFilter * beamFilter = new InSANEBeamCurrentFilter();
   skimmer->AddFilter(beamFilter);
   skimmer->SetScalerEvent(analyzer->fScalers->fScalerEvent); 
   analyzer->SetSkimmer(skimmer);

   /// 
   /// BETA Detectors are currently created in InSANEDetectorAnalysis which is 
   /// inherited by SANEClusteringAnalysis which is inherited by
   /// SANEFirstPassAnalyzer.
   ///

   /// \todo This could be in the analyzer class?..
   analyzer->AddDetector( analyzer->fCherenkovDetector ); 
   analyzer->AddDetector( analyzer->fBigcalDetector ); 
   analyzer->AddDetector( analyzer->fHodoscopeDetector ); 
   analyzer->AddDetector( analyzer->fTrackerDetector ); 

   // ------------------------------------------------------
   // Corrections

   // Time walk corrections
   // Time walk correction \see time_walk.cxx which must be executed first
   std::cout << " + Creating BETATimeWalkCorrection\n"; 
   BETATimeWalkCorrection* timewalkcorr = new BETATimeWalkCorrection(analyzer);
   /*timewalkcorr->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );*/
   timewalkcorr->SetDetectors(analyzer);
   analyzer->AddCorrection(timewalkcorr);

   // ------------------------------------------------------
   // Calculations
   // Now trying to set the SetEvent on calculations ... where if it needs a cluster it must override BETACalculation
   // BETACalculation::SetEvent() which will include a call to the original and SetClusterEvent

   // Forward Tracker Calculations
   std::cout << " + Creating ForwardTrackerCalculation1\n"; 
   ForwardTrackerCalculation1 * t1;
   t1 = new ForwardTrackerCalculation1(analyzer);
   t1->SetDetectors(analyzer);
   analyzer->AddCalculation(t1);

   // Gas Cherenkov Calculations
   std::cout << " + Creating GasCherenkovCalculation1\n"; 
   GasCherenkovCalculation1 * g1;
   g1 = new GasCherenkovCalculation1(analyzer);
   g1->SetDetectors(analyzer);
   analyzer->AddCalculation(g1);

   // Lucite Hodoscope Calculations
   std::cout << " + Creating LuciteHodoscopeCalculation1\n"; 
   LuciteHodoscopeCalculation1 * l1;
   l1 = new LuciteHodoscopeCalculation1(analyzer);
   l1->SetDetectors(analyzer);
   analyzer->AddCalculation(l1);

   // Bigcal Calculations
   std::cout << " + Creating BigcalCalculation1\n"; 
   BigcalCalculation1 * bc1;
   bc1 = new BigcalCalculation1(analyzer);
   bc1->SetDetectors(analyzer);
   analyzer->AddCalculation(bc1);

   // Define the cluster processor
   std::cout << " + Creating LocalMaxClusterProcessor\n"; 
   LocalMaxClusterProcessor * clusterProcessor = 
      new LocalMaxClusterProcessor();

   // Perform clustering
   std::cout << " + Creating BigcalClusteringCalculation1\n"; 
   BigcalClusteringCalculation1 * b1 = new BigcalClusteringCalculation1(analyzer);
   b1->SetClusterProcessor(clusterProcessor);
   b1->SetDetectors(analyzer);
   analyzer->AddCalculation(b1);
   analyzer->SetClusterProcessor((BIGCALClusterProcessor*)b1->GetClusterProcessor());

   // Pi0 2-Cluster
   // - Adds to calibration matricies when a pi0 event is identified
   std::cout << " + Creating BigcalPi0Calaculations\n"; 
   BigcalPi0Calculations * b2 = new BigcalPi0Calculations(analyzer);
   b2->SetDetectors(analyzer);
   analyzer->AddCalculation(b2);

   // Pi0 3-Cluster
   // - Adds to calibration matricies when a pi0 event is identified
   std::cout << " + Creating Bigcal3ClusterPi0Calaculation\n"; 
   Bigcal3ClusterPi0Calculation * b3 = new Bigcal3ClusterPi0Calculation(analyzer);
   b3->SetDetectors(analyzer);
   analyzer->AddCalculation(b3);

   // Electron selection
   // Currently the raw helicity asymmetry is calculated here
   //std::cout << " + Creating SANEElectronSelector\n"; 
   //SANEElectronSelector * select;
   //select = new SANEElectronSelector(analyzer);
   //select->SetDetectors(analyzer);
   //analyzer->AddCalculation(select);

   // Asymmetry calculation requires SANEElectronSelector
   // Calculates the binned asymmetries
   //std::cout << " + Creating SANEAsymmetryCalculation1\n"; 
   //SANEAsymmetryCalculation1 * asym1;
   //asym1 = new SANEAsymmetryCalculation1(analyzer);
   //asym1->SetDetectors(analyzer);
   //asym1->fTrajectory = select->fTrajectory;
   //analyzer->AddCalculation(asym1);

   // Trajectories - lucite and tracker correllated (takes lots of time)
   //std::cout << " + Creating SANETrajectoryCalculation2\n"; 
   //SANETrajectoryCalculation2 * traj2 = new SANETrajectoryCalculation2(analyzer);
   //traj2->SetDetectors(analyzer);

   // Positions independently
   // (tracker, lucite, target (slow raster)
   std::cout << " + Creating SANETrajectoryCalculation3\n"; 
   SANETrajectoryCalculation3 * traj3 = new SANETrajectoryCalculation3(analyzer);
   traj3->SetDetectors(analyzer);
   analyzer->AddCalculation(traj3);

   // Electron selection
   // Currently the raw helicity asymmetry is calculated here
   std::cout << " + Creating SANEElasticElectronSelector\n"; 
   SANEElasticElectronSelector * elasticSelect;
   elasticSelect = new SANEElasticElectronSelector(analyzer);
   elasticSelect->SetDetectors(analyzer);
   analyzer->AddCalculation(elasticSelect);

   //Experimental DeadTime Calculation
   std::cout << " + Creating BETADeadTimeCalculation\n"; 
   BETADeadTimeCalculation * scaler1;
   scaler1 = new BETADeadTimeCalculation(analyzer);
   scaler1->SetDetectors(analyzer);
   analyzer->AddScalerCalculation(scaler1);

   // SANETrackCalculation1 is verymuch like SANEElectronSelector
   // 
   std::cout << " + Creating SANETrackCalculation1\n"; 
   SANETrackCalculation1 * track1;
   track1 = new SANETrackCalculation1(analyzer);
   track1->SetDetectors(analyzer);
   track1->fReconEvent = traj3->fReconEvent;
   analyzer->AddCalculation(track1);

   // Histograms
   std::cout << " + Creating BETAHistogramCalc1\n"; 
   BETAHistogramCalc1 * hist1 = new BETAHistogramCalc1(analyzer);
   hist1->SetDetectors(analyzer);
   analyzer->AddCalculation(hist1);
   //std::cout << " + Creating SANETrajectoryCalculation4\n"; 
   //SANETrajectoryCalculation4 * traj4 = new SANETrajectoryCalculation4(analyzer);
   //traj4->SetDetectors(analyzer);

   // should have some asymmetry calculation like:
   // 1- set the bit which determines whether to even count this event in a denominator
   // 2- set the address of the bit which determines which number, N1 or N2, to increment
   // 3- in the detector's event,  fGoodEvent is used to mean that all cuts for a good
   //    track in that detector are passed. 
   //   InSANEAsymmetry * helAsym = new InSANEAsymmetry();
   //     helAsym->SetTriggerBitAddress(&(beta1->fEvent->fGoodEvent);
   //   BETASubDetector * beta1 = new BETASubDetector(DetectorNumber);
   //   BETASubDetector * beta2 = new BETASubDetector();// this beta2 acceptance must be completely isolated from beta2
   //   // at first it is easy to just match to some other mirror, but once the mirrors are done 
   //   // they can be added to get better statisitics for each mirror
   //   InSANEAsymmetryDetector * cherenkov_efficiency_monitor_12 = new InSANEAsymmetryDetector(beta1,beta2);



   //  analyzer->AddCalculation(traj4);
   //  analyzer->AddCalculation(traj2);
   //  analyzer->AddCalculation(traj1);

   std::cout << " Initialize " << std::endl;
   analyzer->Initialize();

   std::cout << " Process " << std::endl;
   analyzer->Process();

   std::cout << " WRITING RUN OBJECT \n"; 
   //rman->WriteRun();
   std::cout << " FLUSHING AND WRITING FILE \n"; 
   rman->GetCurrentFile()->Flush();
   rman->GetCurrentFile()->Write();

   std::cout << " Deleting Analyzer ...  \n"; 

   delete analyzer;
   //delete b1;
   //delete l1;
   //delete g1;
   //if(gROOT->IsBatch())   gROOT->ProcessLine(".q");
   gSystem->Exit(0);
   return(0);
}

/** @} */

