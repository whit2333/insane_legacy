Int_t cherenkov_energy_dep(Int_t runNumber = 72994){

   rman->SetRun(runNumber);

   TList listOfEfficiencies;
   TEfficiency * cerEff[8][5];
   TEfficiency * cerEffVsEnergy[8];
   TEfficiency * cerEffVsNPE[8];
   TEfficiency * cerEffVsTDC[8];

   TEfficiency * cerEffVsXByMirror[8];
   TEfficiency * cerEffVsYByMirror[8];

   TEfficiency * cerEffVsX = new TEfficiency("cerEffVsX","cerEff Vs X",60,-60,60);
   TEfficiency * cerEffVsY = new TEfficiency("cerEffVsY","cerEff Vs Y",120,-120,120);


   for(int i=0;i<8;i++) {

      cerEffVsXByMirror[i] = new
         TEfficiency(Form("cerEffVsXByMirror-%d",i+1),Form("cerEffVsXByMirror-%d",i+1),60,-60,60);
      cerEffVsYByMirror[i] = new 
         TEfficiency(Form("cerEffVsYByMirror-%d",i+1),Form("cerEffVsYByMirror-%d",i+1),120,-120,120);

      cerEffVsEnergy[i] = new TEfficiency(Form("cer-%d-Vs Energy",i+1),Form("cer-%d-Vs Energy",i+1),50,500,3000);
      cerEffVsNPE[i] = new TEfficiency(Form("cer-%d-Vs NPE",i+1),Form("cer-%d-Vs NPE",i+1),50,0,50);
      cerEffVsTDC[i] = new TEfficiency(Form("cerEffVsTDC-%d",i+1),Form("cer-%d-Vs TDC",i+1),100,-50,50);

      for(int j=0;j<5;j++) {
         cerEff[i][j] = new TEfficiency(Form("cer-%d-%d",i+1,j),Form("cer-%d-%d",i+1,j),50,0,3);
      }
   }

//    for(int i = 0; i< 8; i++) listOfEfficiencies.Add(cerEff[i]);

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   TTree * t = events->fTree;
   if(!t) return(-1);

   GasCherenkovHit * gcHit = 0;
   BIGCALCluster *  aCluster = 0;
   Double_t step = 200.0;
   Double_t start = 800.0;

   Int_t max_events = 100000;

   for(Int_t ievent = 0; ievent<t->GetEntries();ievent++) {
      t->GetEntry(ievent);
      if(ievent%100000==0) printf(" Event %d\n",ievent);

      if(ievent>max_events) break;

      /// Loop over clusters
      if( events->TRIG->IsBETA2Event() )
      for(int j =0;j< events->CLUSTER->fClusters->GetEntries();j++) {
         aCluster  = (BIGCALCluster*)(*events->CLUSTER->fClusters)[j];



         /// First look at central hits in the mirror for "good clusters"
         for(int i=0;i<5;i++)
         if(aCluster->fIsGood /*&& aCluster->fNumberOfMirrors ==1 */) {
            if( aCluster->GetEnergy() > start+((double)i)*step &&
                aCluster->GetEnergy() < start+((double)(i+1))*step) {
/*               aCluster->Dump();*/
               cerEff[aCluster->fPrimaryMirror-1][i]->Fill(aCluster->fGoodCherenkovTDCHit,aCluster->fCherenkovBestADCSum);
            }
         }
         /// Plot good clusters
         if(aCluster->fIsGood /*&& aCluster->fNumberOfMirrors ==1*/ ) {
         
            cerEffVsX->Fill(TMath::Abs(aCluster->fCherenkovTDC)<15,aCluster->fXMoment);
            cerEffVsY->Fill(TMath::Abs(aCluster->fCherenkovTDC)<15,aCluster->fYMoment);
         }


         /// Plot good clusters
         if(aCluster->fIsGood /*&& aCluster->fNumberOfMirrors ==1*/ ) {
            cerEffVsXByMirror[aCluster->fPrimaryMirror-1]->Fill(TMath::Abs(aCluster->fCherenkovTDC)<15,aCluster->fXMoment);
            cerEffVsYByMirror[aCluster->fPrimaryMirror-1]->Fill(TMath::Abs(aCluster->fCherenkovTDC)<15,aCluster->fYMoment);

            cerEffVsEnergy[aCluster->fPrimaryMirror-1]->Fill(TMath::Abs(aCluster->fCherenkovTDC)<15,aCluster->GetEnergy() );
            cerEffVsNPE[aCluster->fPrimaryMirror-1]->Fill(TMath::Abs(aCluster->fCherenkovTDC)<15,aCluster->fCherenkovBestNPESum);

            cerEffVsTDC[aCluster->fPrimaryMirror-1]->Fill(aCluster->fGoodCherenkovTDCHit,aCluster->fCherenkovTDC);
         }
      }
   }

   TCanvas * c = new TCanvas();
   c->Divide(2,4);
   
   for(int i=0;i<8;i++) {
      c->cd(8-i);
      for(int j=0;j<5;j++){
         cerEff[i][j]->SetMarkerColor(j+1);
         cerEff[i][j]->SetLineColor(j+1);
         if(j==0) cerEff[i][j]->Draw();
         else cerEff[i][j]->Draw("same");
      }
   }

   TCanvas * c2 = new TCanvas();
   c2->Divide(2,4);
   for(int i=0;i<8;i++) {
      c2->cd(8-i);
      cerEffVsEnergy[i]->Draw();
   }

   TCanvas * c3 = new TCanvas();
   c3->Divide(2,4);
   for(int i=0;i<8;i++) {
      c3->cd(8-i);
      cerEffVsTDC[i]->Draw();
   }
   TCanvas * c4 = new TCanvas();
   c4->Divide(2,4);
   for(int i=0;i<8;i++) {
      c4->cd(8-i);
      cerEffVsNPE[i]->Draw();
   }

   TCanvas * c5 = new TCanvas();
   c5->Divide(2,1);
   c5->cd(1);
   cerEffVsX->Draw();
   c5->cd(2);
   cerEffVsY->Draw();

   TCanvas * c6 = new TCanvas();
   c6->Divide(2,4);
   for(int i=0;i<8;i++) {
      c6->cd(8-i);
      cerEffVsYByMirror[i]->Draw();
   }

   TCanvas * c7 = new TCanvas();
   c7->Divide(2,4);
   for(int i=0;i<8;i++) {
      c7->cd(8-i);
      cerEffVsXByMirror[i]->Draw();
   }

   c->SaveAs("results/cherenkov_energy_dep.png");
   c->SaveAs("results/cherenkov_energy_dep.pdf");
   c2->SaveAs("results/cherenkov_energy_dep2.png");
   c2->SaveAs("results/cherenkov_energy_dep2.pdf");
   c3->SaveAs("results/cherenkov_energy_dep3.png");
   c4->SaveAs("results/cherenkov_energy_dep4.png");
   c5->SaveAs("results/cherenkov_energy_dep5.png");
   c6->SaveAs("results/cherenkov_energy_dep6.png");

//    t->StartViewer();

  new TBrowser();

  return(0);
}