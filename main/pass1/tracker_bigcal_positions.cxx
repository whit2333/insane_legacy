/**  Tracker Positions w.r.t. Bigcal
 *
 *
 */
Int_t tracker_bigcal_positions(Int_t runnumber = 72995,Int_t nevents=100000){

   const char * detectorTree = "betaDetectors1";

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

   TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   if( !t ) {
      printf("Tree:%s \n    was NOT FOUND. Aborting... \n",detectorTree); 
      return(-1);
   }

   TCanvas * c = new TCanvas("tracker_bigcal_positions","Tracker Pos Calibration");
   c->Divide(2,2);

   /// First Draws lucite X  vs bigcal X.
   c->cd(1);
   t->Draw(
      "bigcalClusters.fXMoment[]:fTrackerPositionHits.fPositionVector.fX[]>>hbcXvstrackerX(100,-12,12,100,-60,60)",
       "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&bigcalClusters.fTotalE>1200",
      "colz",nevents);

   /// First Draws lucite Y  vs bigcal Y.
   c->cd(2);
   h2  = (TH2F*)gROOT->FindObject("hbcXvstrackerX");
   p1 = h2->ProfileX("hbcXvstrackerX_pfx");
//    p1->Fit(f1,"M,R,Q","goff");
//    p1->SetLineColor(i+1);
   p1->Draw();

   /// First Draws lucite Y  vs bigcal Y.
   c->cd(3);
   t->Draw(
      "bigcalClusters.fYMoment[]:fTrackerPositionHits.fPositionVector.fY[]>>hbcYvstrackerY(100,-22,22,100,-120,120)",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&bigcalClusters.fTotalE>1200",
      "colz",nevents);


   /// First Draws lucite Y  vs bigcal Y.
   c->cd(4);
   h2  = (TH2F*)gROOT->FindObject("hbcYvstrackerY");
   p1 = h2->ProfileX("hbcYvstrackerY_pfx");
//    p1->Fit(f1,"M,R,Q","goff");
//    p1->SetLineColor(i+1);
   p1->Draw();



   c->SaveAs(Form("plots/%d/tracker_bigcal_positions.png",runnumber));
   c->SaveAs(Form("plots/%d/tracker_bigcal_positions.ps",runnumber));


   return(0);
   
   
}

