/**  Fits the cherenkov ADC spectra for each cluster
 *
 *   This script does the following:
 * 
 *   - With a geometry correlated cluster,
 *      it plots the adc spectra with a two gaussian fit constrained so that they are at equal spacings
 *   - First it looks at the primary ADC for hits covering the "full mirror"
 *   - Fills the table cherenkov_performance with the fit results
 *   - Second it looks at the primary ADC for hits covering only the center of the mirror, thus the primary mirror
 *     recieves the full cone.
 *   - Similar fits with a corner mirror hit
 *
 *   When you want to use a specific run as say a default, say, run_number = -1, then do this in mysql:
 *   $ INSERT INTO cherenkov_performance (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) SELECT  -1,npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999;
 */
Int_t cherenkov(Int_t runnumber=72795) {

   return 0;
   const char * detectorTree = "betaDetectors1";
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
   if(!fClusterTree) {
      std::cout << "No Cluster Tree Found \n";
   }
   if(!fClusterTree) {printf("Clusters tree not found eh\n"); return(-1); }
   gROOT->cd();

   TH1F * cer[8];

   /// Fitting functions and parameters
   Double_t xMin = 800.0;
   Double_t xMax = 6000.0;
   TFitResultPtr fitResult;
   TF1 *f1 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+[3]*exp(-0.5*((x-2.0*[1])/[4])**2)", xMin,xMax);
   TF1 *e1 = new TF1("e1","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e2 = new TF1("e2","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   e1->SetLineColor(kBlue);
   e2->SetLineColor(kRed);

   TCanvas * c = new TCanvas("Cherenkov1","Cherenkov Primary Mirror ADC",500,800);
   c->Divide(2,4);

   TList * cerplots = new TList();

   /// Loop over each mirror
   for (int i=8;i>=1;i--) {

      c->cd(9-i); 
      if(rman->GetVerbosity()>1) printf("Fitting PMT %d",i);

      /// 1 electron peak amplitude
      f1->SetParameter(0,100);
      f1->SetParLimits(0,10,9999999);
      /// 1 electron peak mean
      f1->SetParameter(1,1800);
      f1->SetParLimits(1,900,6000);
      /// 1 electron peak width
      f1->SetParameter(2,500);
      f1->SetParLimits(2,0,5000);
      /// 2 electron peak amplitude
      f1->SetParameter(3,1);
      f1->SetParLimits(3,0,9999);
      /// 2 electron peak width
      f1->SetParameter(4,2000);
      f1->SetParLimits(4,400,5000);

      /// Plot the primary mirror ADC with a good TDC hit
      fClusterTree->Draw(
         Form("bigcalClusters.fCherenkovBestADC>>cer%d(100,3,6003)",i),
         Form("bigcalClusters.fPrimaryMirror[Iteration$]==%d&& \
               TMath::Abs(bigcalClusters.fCherenkovTDC[Iteration$])<10 && \
               bigcalClusters.fTotalE[Iteration$]>800 && \
               bigcalClusters.fTotalE[Iteration$]<3000&& \
               bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1 && \
               bigcalClusters.fIsGood[Iteration$]==1",i));
       cer[i-1] = (TH1F *) gROOT->FindObject(Form("cer%d",i));
       f1->SetParameter(1,cer[i-1]->GetMean(1));
       fitResult = cer[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);

       cerplots->Add(cer[i-1]);


      if(fitResult->IsValid() ){
         e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
         e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
         e1->DrawCopy("same");
         e2->DrawCopy("same");

         /// Filling the cherenkov_performance table with results
         InSANEDatabaseManager * db = InSANEDatabaseManager::GetManager();
//          TString SQLCOMMAND("UPDATE OR REPLACE cherenkov_performance set "); 
//          SQLCOMMAND +="`run_number`=";
//          SQLCOMMAND += runnumber;
//          SQLCOMMAND +=",`npe_1_amp`="; SQLCOMMAND += Form("%f",fitResult->Parameter(0));
//          SQLCOMMAND +=",`npe_1_width`="; SQLCOMMAND += Form("%f",fitResult->Parameter(2));
//          SQLCOMMAND +=",`npe_1_mean`="; SQLCOMMAND += Form("%f",fitResult->Parameter(1));
//          SQLCOMMAND +=",`npe_2_width`="; SQLCOMMAND += Form("%f",fitResult->Parameter(4));
//          SQLCOMMAND +=",`npe_2_amp`="; SQLCOMMAND += Form("%f",fitResult->Parameter(3));
//          SQLCOMMAND +=",`channel`=";  SQLCOMMAND += Form("%d",i);
         TString SQLCOMMAND("REPLACE INTO cherenkov_performance ("); 
         SQLCOMMAND +="`run_number`";
         SQLCOMMAND +=",`npe_1_amp`"; 
         SQLCOMMAND +=",`npe_1_width`"; 
         SQLCOMMAND +=",`npe_1_mean`"; 
         SQLCOMMAND +=",`npe_2_width`"; 
         SQLCOMMAND +=",`npe_2_amp`"; 
         SQLCOMMAND +=",`channel`) VALUES (";  
         SQLCOMMAND += runnumber;
         SQLCOMMAND +=",";
         SQLCOMMAND += Form("%f",fitResult->Parameter(0));
         SQLCOMMAND +=",";
         SQLCOMMAND += Form("%f",fitResult->Parameter(2));
         SQLCOMMAND +=",";
         SQLCOMMAND += Form("%f",fitResult->Parameter(1));
         SQLCOMMAND +=",";
         SQLCOMMAND += Form("%f",fitResult->Parameter(4));
         SQLCOMMAND +=",";
         SQLCOMMAND += Form("%f",fitResult->Parameter(3));
         SQLCOMMAND +=",";
         SQLCOMMAND += Form("%d",i);
         SQLCOMMAND +=") ;";
         if(db){
            std::cout << SQLCOMMAND << std::endl;
            db->ExecuteInsert(&SQLCOMMAND);
         } else { std::cout << " NO Database found! " << std::endl;}
      } // end good fit result

   } // end loop on mirrors
   c->SaveAs(Form("plots/%d/MirrorsWithTDCHit.png",runnumber));
   c->SaveAs(Form("plots/%d/MirrorsWithTDCHit.pdf",runnumber));
   
   /// Save the results
   /// This causes a crash for some reason.... Adding a Tlist to a TList from ascript?...
   //rman->GetCurrentRun()->fResults.Add(cerplots);
   


   TH1F * cer1Mir[8];
   TCanvas * c2 = new TCanvas("Cherenkov2","Gas Cherenkov Primary full cone events",500,800);
   c2->Divide(2,4);

   for (int i=8;i>=1;i--) {
      c2->cd(9-i);
      if(rman->GetVerbosity()>1) printf("Fitting PMT %d",i);
      /// 1 electron peak amplitude
      f1->SetParameter(0,100);
      f1->SetParLimits(0,10,9999999);
      /// 1 electron peak mean
      f1->SetParameter(1,1800);
      f1->SetParLimits(1,800,6000);
      /// 1 electron peak width
      f1->SetParameter(2,500);
      f1->SetParLimits(2,0,5000);
      /// 2 electron peak amplitude
      f1->SetParameter(3,1);
      f1->SetParLimits(3,0,9999);
      /// 2 electron peak width
      f1->SetParameter(4,2000);
      f1->SetParLimits(4,400,5000);

      /// Plot the primary mirror ADC where it is the only mirror
      fClusterTree->Draw(Form("bigcalClusters.fCherenkovBestADC[]>>cerSingle%d(100,3,5003)",i),
                       Form("bigcalClusters.fNumberOfMirrors[Iteration$]==1&&bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>800&&bigcalClusters.fTotalE[Iteration$]<6000&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1",i));
      cer1Mir[i-1] = (TH1F *) gROOT->FindObject(Form("cerSingle%d",i));
      f1->SetParameter(1,cer1Mir[i-1]->GetMean(1));
      fitResult = cer1Mir[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);
      if( fitResult->IsValid() ){
         e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
         e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
         e1->DrawCopy("same");
         e2->DrawCopy("same");
      } // end good fit result

   } // end loop on mirrors
   c2->SaveAs(Form("plots/%d/MirrorsWithTDCHitAndFullCone.png",runnumber));
   c2->SaveAs(Form("plots/%d/MirrorsWithTDCHitAndFullCone.pdf",runnumber));


   TH1F * cerMultiMir[8];
   TCanvas * c3 = new TCanvas("Cherenkov3","Gas Cherenkov split cone events",500,800);
   c3->Divide(2,4);

   for (int i=8;i>=1;i--) {
      c3->cd(9-i);
      if(rman->GetVerbosity()>1) printf("Fitting PMT %d",i);
      /// 1 electron peak amplitude
      f1->SetParameter(0,100);
      f1->SetParLimits(0,10,9999999);
      /// 1 electron peak mean
      f1->SetParameter(1,1800);
      f1->SetParLimits(1,800,6000);
      /// 1 electron peak width
      f1->SetParameter(2,500);
      f1->SetParLimits(2,0,5000);
      /// 2 electron peak amplitude
      f1->SetParameter(3,1);
      f1->SetParLimits(3,0,9999);
      /// 2 electron peak width
      f1->SetParameter(4,2000);
      f1->SetParLimits(4,400,5000);
      fClusterTree->Draw(Form("bigcalClusters.fCherenkovBestADC[]>>cerMulti%d(100,3,5003)",i),
                       Form("bigcalClusters.fNumberOfMirrors[Iteration$]>1&&bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<6000&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1",i));
      cerMultiMir[i-1] = (TH1F *) gROOT->FindObject(Form("cerMulti%d",i));
      f1->SetParameter(1,cerMultiMir[i-1]->GetMean(1));
      fitResult = cerMultiMir[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);
      if(fitResult->IsValid() ){
         e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
         e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
         e1->DrawCopy("same");
         e2->DrawCopy("same");
      } // end good fit result

   } // end loop on mirrors
   c3->SaveAs(Form("plots/%d/MirrorsWithTDCHitAndSplitCone.png",runnumber));
   c3->SaveAs(Form("plots/%d/MirrorsWithTDCHitAndSplitCone.pdf",runnumber));

   rman->GetCurrentFile()->cd("detectors/cherenkov");

   gDirectory->WriteObject(cerplots,"signalPeaks","SingleKey");
   //cerplots->Write("signalPeaks",TObject::kOverwrite);   
   rman->GetCurrentFile()->cd();

   std::cout << "cherenkov.cxx done!" << std::endl;
   //rman->WriteRun();
   return(0);
}
