/*!  Examines the cherenkov w.r.t. other detectors... trying to decrease background on TDC cut

 Attenuations from LED runs while ramping the magnet during perp:
  {1.02958, 0.976127, 0.553096, 0.863169, 0.982204, 0.831832, 0.604835, 0.800308}
 */
Int_t cherenkov5(Int_t runnumber=72999) {

   const char * detectorTree = "trackingPositions";
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
      if(!fClusterTree) {
      std::cout << "No Tree Found: " << detectorTree << "\n";
   }
   if(!fClusterTree) {printf("Clusters tree not found eh\n"); return(-1); }

   gROOT->cd();
   TH1F * cer[8];

   /// Fitting functions and parameters
   Double_t xMin = 200.0;
   Double_t xMax = 6000.0;
   TFitResultPtr fitResult;
   TF1 *f1 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+[3]*exp(-0.5*((x-2.0*[1])/[4])**2)+[5]*exp(-0.5*((x-[6])/[7])**2)", xMin,xMax);

// 1 electron peak amplitude
  f1->SetParameter(0,100);
  f1->SetParLimits(0,10,9999999);
// 1 electron peak mean
  f1->SetParameter(1,1800);
  f1->SetParLimits(1,800,3000);
// 1 electron peak width
  f1->SetParameter(2,500);
  f1->SetParLimits(2,0,5000);
// 2 electron peak amplitude
  f1->SetParameter(3,1);
  f1->SetParLimits(3,0,9999);
// 2 electron peak width
  f1->SetParameter(4,2000);
  f1->SetParLimits(4,400,5000);
// BG peak amplitude
  f1->SetParameter(5,100);
  f1->SetParLimits(5,1,99999);
//BG peak mean
  f1->SetParameter(6,600);
  f1->SetParLimits(6,100,1000);
// BG peak width
  f1->SetParameter(7,400);
  f1->SetParLimits(7,0,1000);

   TF1 *e1 = new TF1("e1","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e2 = new TF1("e2","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e3 = new TF1("e3","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   e1->SetLineColor(kBlue);
   e2->SetLineColor(kGreen);
   e3->SetLineColor(kBlack);
   e3->SetLineStyle(2);

   TCanvas * c = new TCanvas("cherenkov5","Gas Cherenkov script 5 ");
   c->Divide(4,4);
   TH1F * h1;

   /// Loop over each mirror
   for (int i=8;i>=1;i--) {

      c->cd((9-i)*2 -1);
      if(rman->GetVerbosity()>1) printf("PMT %d\n",i );

      ///___________________________________________________________________
      /// ADCs
      ///___________________________________________________________________

      ///
      fClusterTree->Draw(
         Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cer%d(100,0,4.0)",i),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror==%d&& \
               TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<50&&fNLucitePositions>0\
               &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("cer%d",i));
      h1->SetLineColor(1);
      h1->Draw();


      ///
      fClusterTree->Draw(
         Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cer2%d(100,0,4.0)",i),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror==%d&& \
               betaDetectors1.bigcalClusters.fIsGood &&\
               TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<50&&fNLucitePositions>0\
               &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("cer2%d",i));
      h1->SetLineColor(2000);
      h1->Draw("same");

      ///
      fClusterTree->Draw(
         Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cer3%d(100,0,4.0)",i),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror==%d&& \
               TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<50&& \
               fClosestLuciteMissDistance<40&&fNLucitePositions>0\
               &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600 ",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("cer3%d",i));
      h1->SetLineColor(2001);
      h1->Draw("same");


      ///
      fClusterTree->Draw(
         Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cer4%d(100,0,4.0)",i),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror==%d&& \
               TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<12&& \
               fClosestLuciteMissDistance<40&&fNLucitePositions>0\
               &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600 ",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("cer4%d",i));
      h1->SetLineColor(2002);
      h1->Draw("same");
/*
      ///
      fClusterTree->Draw(
         Form("fGasCherenkovEvent.fGasCherenkovTDCHits.fADC>>cer3%d(100,0,4000.0)",i),
         Form("fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fMirrorNumber==%d&& \
               TMath::Abs( fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign)<8&& \
               fNLucitePositions>2",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("cer3%d",i));
      h1->SetLineColor(2001);
      fitResult = h1->Fit(f1,"S","same");
      e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
      e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
      e3->SetParameters(fitResult->Parameter(5),fitResult->Parameter(6),fitResult->Parameter(7));
      e1->DrawCopy("same");
      e2->DrawCopy("same");
      e3->DrawCopy("same");
      h1->Draw("same");
*/

      ///___________________________________________________________________
      /// TDCS
      ///___________________________________________________________________
      c->cd((9-i)*2 );

      ///
      fClusterTree->Draw(
         Form("betaDetectors1.bigcalClusters.fCherenkovTDC>>certdc%d(100,-50,50)",i),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror==%d &&\
               TMath::Abs(betaDetectors1.bigcalClusters.fCherenkovTDC)<50&&fNLucitePositions>0\
               &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("certdc%d",i));
      h1->SetLineColor(1);
      h1->Draw();

      ///
      fClusterTree->Draw(
          Form("betaDetectors1.bigcalClusters.fCherenkovTDC>>certdc2%d(100,-50,50)",i),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror==%d &&\
               betaDetectors1.bigcalClusters.fIsGood &&\
               TMath::Abs(betaDetectors1.bigcalClusters.fCherenkovTDC)<50&&fNLucitePositions>0\
               &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("certdc2%d",i));
      h1->SetLineColor(2000);
      h1->Draw("same");

      ///
      fClusterTree->Draw(
          Form("betaDetectors1.bigcalClusters.fCherenkovTDC>>certdc3%d(100,-50,50)",i),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror==%d &&\
               TMath::Abs(betaDetectors1.bigcalClusters.fCherenkovTDC)<50&& \
               fClosestLuciteMissDistance<40&&fNLucitePositions>0\
               &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("certdc3%d",i));
      h1->SetLineColor(2001);
      h1->Draw("same");

      ///
      fClusterTree->Draw(
          Form("betaDetectors1.bigcalClusters.fCherenkovTDC>>certdc4%d(100,-50,50)",i),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror==%d &&\
               TMath::Abs(betaDetectors1.bigcalClusters.fCherenkovTDC)<12&& \
               fClosestLuciteMissDistance<40&&fNLucitePositions>0 \
               &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600",i),"goff");
      h1 = (TH1F *) gROOT->FindObject(Form("certdc4%d",i));
      h1->SetLineColor(2002);
      h1->Draw("same");




//     f1->SetParameter(1,cer[i-1]->GetMean(1));
//     fitResult = cer[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);
// if(fitResult->IsValid() ){
//     e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
//     e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
//     e1->DrawCopy("same");
//     e2->DrawCopy("same");
//     
// /// Filling the cherenkov_performance table with results
//    TSQLServer * db = SANEDatabaseManager::GetManager()->dbServer;
//    TString SQLCOMMAND("REPLACE into cherenkov_performance set "); 
//    SQLCOMMAND +="`run_number`=";
//    SQLCOMMAND += runnumber;
//    SQLCOMMAND +=",`npe_1_amp`="; SQLCOMMAND += Form("%f",fitResult->Parameter(0));
//    SQLCOMMAND +=",`npe_1_width`="; SQLCOMMAND += Form("%f",fitResult->Parameter(2));
//    SQLCOMMAND +=",`npe_1_mean`="; SQLCOMMAND += Form("%f",fitResult->Parameter(1));
//    SQLCOMMAND +=",`npe_2_width`="; SQLCOMMAND += Form("%f",fitResult->Parameter(4));
//    SQLCOMMAND +=",`npe_2_amp`="; SQLCOMMAND += Form("%f",fitResult->Parameter(3));
//    SQLCOMMAND +=",`channel`=";  SQLCOMMAND += Form("%d",i);
// 
//    if(db) db->Query(SQLCOMMAND.Data());
// // }
// 
//    }
  
   }
   c->SaveAs(Form("plots/%d/Cherenkov5.png",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov5.pdf",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov5.svg",runnumber));

   //fClusterTree->StartViewer();

//   c->SaveAs(Form("plots/%d/MirrorsWithTDCHit.jpg",runnumber));
// 
//   TH1F * cer1Mir[8];
//   TCanvas * c2 = new TCanvas("Cherenkov2","Gas Cherenkov 2",500,800);
//   c2->Divide(2,4);
// 
//    for (int i=8;i>=1;i--) {
//       c2->cd(9-i);printf("Fitting PMT %d",i);
// // 1 electron peak amplitude
//   f1->SetParameter(0,100);
//   f1->SetParLimits(0,10,9999999);
// // 1 electron peak mean
//   f1->SetParameter(1,1800);
//   f1->SetParLimits(1,800,6000);
// // 1 electron peak width
//   f1->SetParameter(2,500);
//   f1->SetParLimits(2,0,5000);
// // 2 electron peak amplitude
//   f1->SetParameter(3,1);
//   f1->SetParLimits(3,0,9999);
// // 2 electron peak width
//   f1->SetParameter(4,2000);
//   f1->SetParLimits(4,400,5000);
//     fClusterTree->Draw(Form("bigcalClusters.fCherenkovBestADCSum[]>>cerSingle%d(100,3,6303)",i),
//                        Form("bigcalClusters.fNumberOfMirrors[Iteration$]==1&&bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<6000&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1",i));
//     cer1Mir[i-1] = (TH1F *) gROOT->FindObject(Form("cerSingle%d",i));
//     f1->SetParameter(1,cer1Mir[i-1]->GetMean(1));
//     fitResult = cer1Mir[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);
// if(fitResult->IsValid() ){
//     e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
//     e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
//     e1->DrawCopy("same");
//     e2->DrawCopy("same");
// 
//     
//      }
// }
//   c2->SaveAs(Form("plots/%d/MirrorsWithTDCHitAndFullCone.jpg",runnumber));
// 
// 
//   TH1F * cerMultiMir[8];
// 
//   TCanvas * c3 = new TCanvas("Cherenkov3","Gas Cherenkov 3",500,800);
//   c3->Divide(2,4);
// 
//    for (int i=8;i>=1;i--) {
//       c3->cd(9-i);printf("Fitting PMT %d",i);
// // 1 electron peak amplitude
//   f1->SetParameter(0,100);
//   f1->SetParLimits(0,10,9999999);
// // 1 electron peak mean
//   f1->SetParameter(1,1800);
//   f1->SetParLimits(1,800,6000);
// // 1 electron peak width
//   f1->SetParameter(2,500);
//   f1->SetParLimits(2,0,5000);
// // 2 electron peak amplitude
//   f1->SetParameter(3,1);
//   f1->SetParLimits(3,0,9999);
// // 2 electron peak width
//   f1->SetParameter(4,2000);
//   f1->SetParLimits(4,400,5000);
//       fClusterTree->Draw(Form("bigcalClusters.fCherenkovBestADCSum[]>>cerMulti%d(100,3,6303)",i),
//                        Form("bigcalClusters.fNumberOfMirrors[Iteration$]>1&&bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<6000&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1",i));
//     cerMultiMir[i-1] = (TH1F *) gROOT->FindObject(Form("cerMulti%d",i));
//     f1->SetParameter(1,cerMultiMir[i-1]->GetMean(1));
//     fitResult = cerMultiMir[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);
//  if(fitResult->IsValid() ){
//    e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
//     e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
//     e1->DrawCopy("same");
//     e2->DrawCopy("same");
//     }
//    }
//   c3->SaveAs(Form("plots/%d/MirrorsWithTDCHitAndSplitCone.jpg",runnumber));

}
