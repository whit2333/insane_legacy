/** Lucite Bar Geometry w.r.t. BigCal
 *
 *  - Creates new Calibration object 
 *  - Draw lucite positions X(diff) vs Y(bar).
 *  - Draw the Luicte X position vs the Cluster X moment
 *  - Writes to file the new calibration
 *
 * Should be called from a MakePlots() for a calculation.
 * \todo clean up calculation -> MakePlot() Analysis connection
 *
 * This script gets the calibrated positions for the hodoscope
 * from cluster information. 
 * 
 */
Int_t lucite1(Int_t runnumber = 72795) {

   const char * detectorTree = "betaDetectors1";

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

   TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   if( !t ) {
      printf("Tree:%s \n    was NOT FOUND. Aborting... \n",detectorTree); 
      return(-1);
   }

   gROOT->cd();

   TCanvas * c = new TCanvas("lucite1","Lucite Hodoscope 1");
   c->Divide(1,2);
   TH1F * h1;

   t->StartViewer();

   c->cd(1);
   t->Draw("fLucitePositionHits.fTDCSum>>luc1(100,-1000,1000)","","");

   c->cd(2);
   t->Draw("fLucitePositionHits.fTDCDifference>>luc2(100,-1000,1000)","","");
   //t->Draw("fLucitePositionHits.fTDCSum[]>>luc2(100,-1000,1000)","bigcalClusters.fCherenkovBestADCSum[]>0.4","same");

   c->SaveAs(Form("plots/%d/lucite1.png",runnumber));
   c->SaveAs(Form("plots/%d/lucite1.pdf",runnumber));
   c->SaveAs(Form("plots/%d/lucite1.svg",runnumber));


  return(0);
}
