/*!  Examines the cherenkov w.r.t. other detectors... trying to decrease background on TDC cut

 Attenuations from LED runs while ramping the magnet during perp:
  {1.02958, 0.976127, 0.553096, 0.863169, 0.982204, 0.831832, 0.604835, 0.800308}
 */
Int_t cherenkov18(Int_t runnumber=72999, Double_t Emin = 1300.0) {

   //if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   //else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   //rman->GetCurrentFile()->cd();
   TFile * f = new TFile(Form("data/rootfiles/InSANE%d.-1.root",runnumber),"READ");

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   //TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   TTree * t = events->fTree;
   if(!t) {
      std::cout << "No Tree Found: " << detectorTree << "\n";
      return(-10);
   }

   const char * trackingTree = "trackingPositions";
   TTree * t2 = (TTree*)gROOT->FindObject(trackingTree);
   if(!t2) {
      std::cout << "No Tree Found: " << trackingTree << "\n";
      return(-11);
   }
   t2->BuildIndex("fRunNumber","fEventNumber");
   t->AddFriend(t2);

   InSANEReconstructedEvent * fRecon = new InSANEReconstructedEvent();
   t2->SetBranchAddress("uncorrelatedPositions",&fRecon);
   //InSANEDISTrajectory * fTrajectory = new InSANEDISTrajectory();
   //t->SetBranchAddress("trajectory",&fTrajectory);
   InSANETriggerEvent  * fTriggerEvent = new InSANETriggerEvent();
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   InSANEHitPosition * lucHit = 0;
   BIGCALCluster     * cluster = 0;
   bool goodLucite            = false;
   int  goodClusterN          = 0;
   TH1F * h1 = 0;

   TList * fHistograms      = new TList();
   TList * fADCHistograms   = new TList();
   TList * fTDCHistograms   = new TList();
   TH1F  * hCerAdcOdd       = new TH1F("cerodd","Spherical Mirrors",100,0,5.0);
   TH1F  * hCerAdcEven      = new TH1F("cereven","Toroidal Mirrors",100,0,5.0);
   TH1F  * hCerAdcOddNoLuc  = new TH1F("ceroddnoluc","Spherical Mirrors",100,0,5.0);
   TH1F  * hCerAdcEvenNoLuc = new TH1F("cerevennoluc","Toroidal Mirrors",100,0,5.0);
   for(int i = 0;i<8;i++){
      h1 = new TH1F(Form("ceradc%d",i+1),Form("ADC %d",i+1),100,0,5.0);
      fADCHistograms->Add(h1);
      h1 = new TH1F(Form("certdc%d",i+1),Form("TDC %d",i+1),100,-200,200);
      fTDCHistograms->Add(h1);
   }
   fHistograms->Add(hCerAdcOdd);
   fHistograms->Add(hCerAdcEven);
   fHistograms->Add(hCerAdcEvenNoLuc);
   fHistograms->Add(hCerAdcOddNoLuc);


   Int_t fEntries = t->GetEntries();
   for(Int_t ievent = 0; ievent <fEntries /*&& ievent < 100000*/ ; ievent++) {

      if(ievent%10000 == 0) std::cout << ievent << "/" << fEntries << "\n";
      t->GetEntry(ievent);
      Int_t bytes = t2->GetEntryWithIndex(fTriggerEvent->fRunNumber,fTriggerEvent->fEventNumber);
      if(bytes <= 0) continue;
      //std::cout << bytes << std::endl;
      //std::cout << fTriggerEvent->fEventNumber  << " vs " << fRecon->fEventNumber << "\n";

      if( fTriggerEvent->IsBETA2Event() ) 
         if( fRecon->fNTrajectories > 0 ){ 

            // Determine if there is a good lucite hit
            goodLucite = false;

            for(int i = 0; i< fRecon->fNLucitePositions; i++){
               lucHit = (InSANEHitPosition*)(*(fRecon->fLucitePositions))[i];

               if( TMath::Abs(lucHit->fPositionUncertainty.Y() ) < 10 ) {
               if( TMath::Abs(lucHit->fTiming ) < 250 ) {
                  goodLucite = true;
                  goodClusterN = lucHit->fClusterNumber; 
                  break;
               }
               }

            }
            if( goodLucite ) {

               cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[goodClusterN];
               if( cluster->fIsGood )
                  if( cluster->fTotalE<5000 && cluster->fTotalE > Emin ) 
                     if( cluster->fNumberOfMirrors == 1 ) 
                     if( TMath::Abs(cluster->fCherenkovTDC) < 8 ) {

                        if(cluster->fPrimaryMirror%2==0)
                           hCerAdcEven->Fill(cluster->fCherenkovBestADCSum);
                        
                        if(cluster->fPrimaryMirror%2==1)
                           hCerAdcOdd->Fill(cluster->fCherenkovBestADCSum);
                        
                        h1 = (TH1F*)fADCHistograms->At(cluster->fPrimaryMirror-1);
                        h1->Fill(cluster->fCherenkovBestADCSum);
                        h1 = (TH1F*)fTDCHistograms->At(cluster->fPrimaryMirror-1);
                        h1->Fill(cluster->fCherenkovTDC);

                     }
            }
            for(int j=0;j<events->CLUSTER->fClusters->GetEntries();j++) {
               cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[j];
               if( cluster->fIsGood )
                   if( TMath::Abs(cluster->fCherenkovTDC) < 8 ) 
                    if( cluster->fTotalE<5000 && cluster->fTotalE > Emin ) 
                     if( cluster->fNumberOfMirrors == 1 ) 
                     {

                        if(cluster->fPrimaryMirror%2==0)
                           hCerAdcEvenNoLuc->Fill(cluster->fCherenkovBestADCSum);
                        
                        if(cluster->fPrimaryMirror%2==1)
                           hCerAdcOddNoLuc->Fill(cluster->fCherenkovBestADCSum);
                        
                        //h1 = (TH1F*)fADCHistograms->At(cluster->fPrimaryMirror-1);
                        //h1->Fill(cluster->fCherenkovBestADCSum);
                        //h1 = (TH1F*)fTDCHistograms->At(cluster->fPrimaryMirror-1);
                        //h1->Fill(cluster->fCherenkovTDC);

                     }
            }


         }

   }

   gROOT->cd();

   /// Fitting functions and parameters
   Double_t xMin = 0.5;
   Double_t xMax = 4.0;
   TFitResultPtr fitResult;
   TF1 *f1 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+[3]*exp(-0.5*((x-2.0*[1])/[4])**2)+[5]*exp(-0.5*((x-[6])/[7])**2)", xMin,xMax);

   TF1 *f2 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+\
         [3]*exp(-0.5*((x-2.0*[1])/[4])**2)+\
         [5]*exp(-0.5*((x-3.0*[1])/[6])**2)", xMin,xMax);

   // 1 electron peak amplitude
   f1->SetParameter(0,100);
   f1->SetParLimits(0,0.10,9999999);
   // 1 electron peak mean
   f1->SetParameter(1,1.0);
   f1->SetParLimits(1,0.5,2.0);
   // 1 electron peak width
   f1->SetParameter(2,0.500);
   f1->SetParLimits(2,0,9999);
   // 2 electron peak amplitude
   f1->SetParameter(3,1);
   f1->SetParLimits(3,0,9999);
   // 2 electron peak width
   f1->SetParameter(4,2.000);
   f1->SetParLimits(4,0.400,5.000);
   // BG peak amplitude
   f1->SetParameter(5,100);
   f1->SetParLimits(5,0,99999);
   //BG peak mean
   f1->SetParameter(6,0.600);
   f1->SetParLimits(6,100,1000);
   // BG peak width
   f1->SetParameter(7,0.3);
   f1->SetParLimits(7,0,1000);

   // 1 electron peak amplitude
   f2->SetParameter(0,100);
   f2->SetParLimits(0,0.10,9999999);
   // 1 electron peak mean
   f2->SetParameter(1,1.0);
   f2->SetParLimits(1,0.5,2.0);
   // 1 electron peak width
   f2->SetParameter(2,0.500);
   f2->SetParLimits(2,0,9999);
   // 2 electron peak amplitude
   f2->SetParameter(3,1);
   f2->SetParLimits(3,0,9999);
   // 2 electron peak width
   f2->SetParameter(4,2.000);
   f2->SetParLimits(4,0.400,5.000);
   // 3 peak amplitude
   f2->SetParameter(5,1);
   f2->SetParLimits(5,0,99999);
   // 3 peak width
   f2->SetParameter(6,0.7);
   f2->SetParLimits(6,0,3000);

   TF1 *e1 = new TF1("e1","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e2 = new TF1("e2","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e3 = new TF1("e3","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e4 = new TF1("e4","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   e1->SetLineColor(kBlue);
   e2->SetLineColor(kGreen);
   e3->SetLineColor(kBlack);
   e3->SetLineStyle(2);
   e4->SetLineColor(2000);
   e4->SetLineStyle(2);

   TCanvas * c = new TCanvas("cherenkov8","Gas Cherenkov script 8 ");
   c->Divide(1,2);

   ///___________________________________________________________________
   /// ADCs
   ///___________________________________________________________________
   ///
   c->cd(1);
   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
 
   //t->Draw(
   //   Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerEven(100,0,5.0)"),
   //   Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==0&& \
   //         TMath::Abs(betaDetectors1.bigcalClusters.fCherenkovTDC)<4 \
   //         && fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0 \
   //         && bigcalClusters.fIsGood \
   //         &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>1200"),"goff");
   //h1 = (TH1F *) gROOT->FindObject(Form("cerEven"));
   h1 = hCerAdcEven;
   h1->SetLineColor(1);
   h1->Draw("E1");
   fitResult = h1->Fit(f1,"S,R","same");
   e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
   e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
   e3->SetParameters(fitResult->Parameter(5),fitResult->Parameter(6),fitResult->Parameter(7));
/*   e4->SetParameters(fitResult->Parameter(5),3.0*fitResult->Parameter(1),fitResult->Parameter(6));*/
   e1->DrawCopy("same");
   e2->DrawCopy("same");
   e3->DrawCopy("same");
/*   e4->DrawCopy("same");*/
/*   h1->SetMarkerStyle(20);*/
   h1->SetTitle("Toroidal Mirrors");
   h1->Draw("same");
   hCerAdcEvenNoLuc->SetLineColor(2);
   hCerAdcEvenNoLuc->Draw("same");

   for(int iPmt = 2; iPmt <=8 ;iPmt+=2 _){
      h1 = (TH1F*)fADCHistograms->At(iPmt-1);
      h1->SetLineColor(4007+iPmt/2);
      leg->AddEntry(h1,Form("PMT %d",iPmt),"l");
      h1->Draw("same");
   }
   leg->Draw();



   c->cd(2);
   TLegend * leg2 = new TLegend(0.7,0.7,0.9,0.9);
   //t->Draw(
   //   Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerOdd(100,0,5.0)"),
   //   Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==1&& \
   //         TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<10 \
   //         && bigcalClusters.fIsGood \
   //         &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>1000 "),"goff");
   //h1 = (TH1F *) gROOT->FindObject(Form("cerOdd"));
   h1 = hCerAdcOdd;
   h1->SetLineColor(1);
   h1->Draw("E1");
   fitResult = h1->Fit(f1,"S,R","same");
   e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
   e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
   e3->SetParameters(fitResult->Parameter(5),fitResult->Parameter(6),fitResult->Parameter(7));
/*   e4->SetParameters(fitResult->Parameter(5),3.0*fitResult->Parameter(1),fitResult->Parameter(6));*/
   e1->DrawCopy("same");
   e2->DrawCopy("same");
   e3->DrawCopy("same");
/*   e4->DrawCopy("same");*/
/*   h1->SetMarkerStyle(20);*/
   h1->SetTitle("Spherical Mirrors");
   h1->Draw("same");
   hCerAdcOddNoLuc->SetLineColor(2);
   hCerAdcOddNoLuc->Draw("same");

   for(int iPmt = 1; iPmt <=8 ;iPmt+=2 _){
      h1 = (TH1F*)fADCHistograms->At(iPmt-1);
      h1->SetLineColor(8008+iPmt/2);
      leg2->AddEntry(h1,Form("PMT %d",iPmt),"l");
      h1->Draw("same");
   }
   leg2->Draw();

   c->SaveAs(Form("plots/%d/Cherenkov18.png",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov18.pdf",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov18.eps",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov18.tex",runnumber));


   TFile * outfile = new TFile(Form("detectors/cherenkov/rootfiles/cherenkov18_%d_%d.root",runnumber,int(Emin)),"UPDATE");
   outfile->mkdir(Form("run%d-%d",runnumber,int(Emin)));
   outfile->cd(Form("run%d-%d",runnumber,int(Emin)));
   fADCHistograms->Write();//"ADCHistograms");
   fTDCHistograms->Write();//"TDCHistograms");
   fHistograms->Write();//"Histograms");


   return(0);
}
