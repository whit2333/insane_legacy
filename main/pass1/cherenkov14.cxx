/*!  Examines the cherenkov w.r.t. other detectors... trying to decrease background on TDC cut

 Attenuations from LED runs while ramping the magnet during perp:
  {1.02958, 0.976127, 0.553096, 0.863169, 0.982204, 0.831832, 0.604835, 0.800308}
 */
Int_t cherenkov14(Int_t runnumber=72999) {

   const char * detectorTree = "trackingPositions";
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
      if(!fClusterTree) {
      std::cout << "No Tree Found: " << detectorTree << "\n";
   }
   if(!fClusterTree) {printf("Clusters tree not found eh\n"); return(-1); }

   gROOT->cd();
   TH1F * cer[8];

   /// Fitting functions and parameters
   Double_t xMin = 0.4;
   Double_t xMax = 5.0;
   TFitResultPtr fitResult;
   TF1 *f1 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+\
                                     [3]*exp(-0.5*((x-2.0*[1])/[4])**2)+\
                                     [5]*exp(-0.5*((x-[6])/[7])**2)", xMin,xMax);
   TF1 *f3 = new TF1("cherenkovADC3","[0]*exp(-0.5*((x-[1])/[2])**2)+\
                                     [3]*exp(-0.5*((x-2.0*[1])/[4])**2)", xMin,xMax);

   TF1 *f2 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+\
                                     [3]*exp(-0.5*((x-2.0*[1])/[4])**2)+\
                                     [5]*exp(-0.5*((x-3.0*[1])/[6])**2)", xMin,xMax);

// 1 electron peak amplitude
  f1->SetParameter(0,100);
  f1->SetParLimits(0,0.10,9999999);
// 1 electron peak mean
  f1->SetParameter(1,1.0);
  f1->SetParLimits(1,0.8,1.5);
// 1 electron peak width
  f1->SetParameter(2,0.500);
  f1->SetParLimits(2,0,9999);
// 2 electron peak amplitude
  f1->SetParameter(3,1);
  f1->SetParLimits(3,0,9999);
// 2 electron peak width
  f1->SetParameter(4,2.000);
  f1->SetParLimits(4,0.400,5.000);
// BG peak amplitude
  f1->SetParameter(5,100);
  f1->SetParLimits(5,0,99999);
//BG peak mean
  f1->SetParameter(6,0.600);
  f1->SetParLimits(6,100,1000);
// BG peak width
  f1->SetParameter(7,0.3);
  f1->SetParLimits(7,0,1000);

// 1 electron peak amplitude
  f3->SetParameter(0,100);
  f3->SetParLimits(0,0.10,9999999);
// 1 electron peak mean
  f3->SetParameter(1,1.0);
  f3->SetParLimits(1,0.8,1.5);
// 1 electron peak width
  f3->SetParameter(2,0.500);
  f3->SetParLimits(2,0,9999);
// 2 electron peak amplitude
  f3->SetParameter(3,1);
  f3->SetParLimits(3,0,9999);
// 2 electron peak width
  f3->SetParameter(4,2.000);
  f3->SetParLimits(4,0.400,5.000);


// 1 electron peak amplitude
  f2->SetParameter(0,100);
  f2->SetParLimits(0,0.10,9999999);
// 1 electron peak mean
  f2->SetParameter(1,1.0);
  f2->SetParLimits(1,0.8,1.5);
// 1 electron peak width
  f2->SetParameter(2,0.500);
  f2->SetParLimits(2,0,9999);
// 2 electron peak amplitude
  f2->SetParameter(3,1);
  f2->SetParLimits(3,0,9999);
// 2 electron peak width
  f2->SetParameter(4,2.000);
  f2->SetParLimits(4,0.400,5.000);
// 3 peak amplitude
  f2->SetParameter(5,1);
  f2->SetParLimits(5,0,99999);
// 3 peak width
  f2->SetParameter(6,0.7);
  f2->SetParLimits(6,0,3000);

   TF1 *e1 = new TF1("e1","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e2 = new TF1("e2","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e3 = new TF1("e3","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e4 = new TF1("e4","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   e1->SetLineColor(kBlue);
   e2->SetLineColor(kGreen);
   e3->SetLineColor(kBlack);
   e3->SetLineStyle(2);
   e4->SetLineColor(2000);
   e4->SetLineStyle(2);

   TCanvas * c = new TCanvas("cherenkov14","Gas Cherenkov script 14");
   //c->Divide(2,2);
   TH1F * h1;

   ///___________________________________________________________________
   /// ADCs
   ///___________________________________________________________________
   ///
   //c->cd(1);


//    fClusterTree->Draw(
//       Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerEven(100,0,5.0)"),
//       Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==0&& \
//             betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600 "),"goff");
      fClusterTree->Draw(
         Form("fGasCherenkovEvent.fGasCherenkovTDCHits.fADCAlign>>cerEven(100,0,2.0)"),
         Form("fGasCherenkovEvent.fGasCherenkovTDCHits.fMirrorNumber%2==0&&TMath::Abs( fGasCherenkovEvent.fGasCherenkovTDCHits.fTDCAlign)<8"),"goff");
   h1 = (TH1F *) gROOT->FindObject(Form("cerEven"));
   h1->SetLineColor(1);
   h1->Draw("");


   fClusterTree->Draw(
      Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerEven2(100,0,5.0)"),
      Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==0&& \
            TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<12&& \
            betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600 "),"goff");
   h1 = (TH1F *) gROOT->FindObject(Form("cerEven2"));
   h1->SetLineColor(2001);
   h1->Draw("SAME");


/*
   ///
   fClusterTree->Draw(
      Form("betaDetectors1.bigcalClusters.fCherenkovTDC>>certdcodd0(100,-100,100)"),
      Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==1&& \
            fClosestLuciteMissDistance<40&&fNLucitePositions>0 &&\
            betaDetectors1.bigcalClusters.fGoodLuciteTDCHit==1\
            &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600 "),"goff");
   h1 = (TH1F *) gROOT->FindObject(Form("certdcodd0"));
   h1->SetLineColor(1);
   h1->Draw();


   ///
   fClusterTree->Draw(
      Form("betaDetectors1.bigcalClusters.fCherenkovTDC>>certdcodd1(100,-100,100)"),
      Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==1&& \
            TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<12&& \
            fClosestLuciteMissDistance<40&&fNLucitePositions>0 &&\
            betaDetectors1.bigcalClusters.fGoodLuciteTDCHit==1\
            &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600 "),"goff");
   h1 = (TH1F *) gROOT->FindObject(Form("certdcodd1"));
   h1->SetLineColor(2001);
   h1->Draw("same");

   ///
   fClusterTree->Draw(
      Form("betaDetectors1.bigcalClusters.fCherenkovTDC>>certdcodd2(100,-100,100)"),
      Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==1&& \
            TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC+30)<12&& \
            fClosestLuciteMissDistance<40&&fNLucitePositions>0 &&\
            betaDetectors1.bigcalClusters.fGoodLuciteTDCHit==1\
            &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600 "),"goff");
   h1 = (TH1F *) gROOT->FindObject(Form("certdcodd2"));
   h1->SetLineColor(2002);
   h1->Draw("same");


   ///
   fClusterTree->Draw(
      Form("betaDetectors1.bigcalClusters.fCherenkovTDC>>certdcodd3(100,-100,100)"),
      Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==1&& \
            TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC-30)<12&& \
            fClosestLuciteMissDistance<40&&fNLucitePositions>0 &&\
            betaDetectors1.bigcalClusters.fGoodLuciteTDCHit==1\
            &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>600 "),"goff");
   h1 = (TH1F *) gROOT->FindObject(Form("certdcodd3"));
   h1->SetLineColor(2003);
   h1->Draw("same");
*/

   c->SaveAs(Form("plots/%d/Cherenkov14.png",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov14.pdf",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov14.svg",runnumber));

}
