/** Clustering
 */
Int_t clustering5(Int_t runnumber = 72999) {

   const char * detectorTree = "betaDetectors1";

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

   TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   if( !t ) {
      printf("Tree:%s \n    was NOT FOUND. Aborting... \n",detectorTree); 
      return(-1);
   }

   gROOT->cd();

   TCanvas * c = new TCanvas("clustering5","Clustering 5");
   c->Divide(2,2);
   TH1F * h1;
   TH2F * h2;
   TProfile * p1;

   c->cd(1);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYSkewness:bigcalClusters.fjPeak>>clust1(56,1,57,50,-5,5)",
           "bigcalClusters.fIsGood&&bigcalClusters.fiPeak!=1&&\
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5&&\
            fNClusters>1&&\
            betaDetectors1.bigcalClusters.fTotalE<5000&&\
            betaDetectors1.bigcalClusters.fTotalE>600 ","goff,colz");
   h2 = (TH2F*)gROOT->FindObject("clust1");
   h2->SetTitle("Y-skew Vs. jPeak");
   h2->Draw("colz");
   p1 = h2->ProfileX("clust1_pfx");
   p1->SetLineColor(2000);
   p1->Draw("same");

   c->cd(2);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>clust2(32,1,33,56,1,57)",
           "bigcalClusters.fIsGood&&bigcalClusters.fiPeak!=1&&\
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5&&\
            fNClusters>1&&\
            betaDetectors1.bigcalClusters.fTotalE<5000&&\
            betaDetectors1.bigcalClusters.fTotalE>600 ","goff,colz");
   h1 = (TH1F*)gROOT->FindObject("clust2");
   h1->SetLineColor(2000);
   h1->Draw("colz");


   c->cd(3);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYSkewness>>clust3(50,-5,5)",
           "bigcalClusters.fIsGood&&bigcalClusters.fiPeak!=1&&\
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5&&\
            fNClusters>1&&\
            betaDetectors1.bigcalClusters.fTotalE<5000&&\
            betaDetectors1.bigcalClusters.fTotalE>600 ","goff,colz");
   h1 = (TH1F*)gROOT->FindObject("clust3");
   h1->SetLineColor(2001);
   h1->Draw();

   t->Draw("bigcalClusters.fYSkewness>>clust32(50,-5,5)",
           "bigcalClusters.fIsGood&&bigcalClusters.fiPeak!=1&&\
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>2.0 &&\
            fNClusters>1&&\
            betaDetectors1.bigcalClusters.fTotalE<5000&&\
            betaDetectors1.bigcalClusters.fTotalE>600 ","goff,colz");
   h1 = (TH1F*)gROOT->FindObject("clust32");
   h1->SetLineColor(2000);
   h1->Draw("same");

   c->cd(4);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYSkewness:bigcalClusters.fiPeak>>clust4(32,1,33,50,-5,5)",
           "bigcalClusters.fIsGood&&bigcalClusters.fiPeak!=1&&\
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5&&\
            fNClusters>1&&\
            betaDetectors1.bigcalClusters.fTotalE<5000&&\
            betaDetectors1.bigcalClusters.fTotalE>600 ","goff,colz");
   h1 = (TH1F*)gROOT->FindObject("clust4");
   h1->SetLineColor(2000);
   h1->Draw("colz");
//    t->Draw("bigcalClusters.fYMoment>>clust2(60,-120,120)","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","goff");
//    h1 = (TH1F*)gROOT->FindObject("clust2");
//    h1->SetLineColor(2001);
//    h1->Draw("same");
// 
//    t->Draw("bigcalClusters.fYMoment>>clust3(60,-120,120)",
//            "bigcalClusters.fIsGood&& \
//             bigcalClusters.fCherenkovBestADCSum>0.4 &&\
//             bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
//    h1 = (TH1F*)gROOT->FindObject("clust3");
//    h1->SetLineColor(2002);
//    h1->Draw("same");
// 
//    t->Draw("bigcalClusters.fYMoment>>clust4(60,-120,120)",
//            "bigcalClusters.fIsGood&& \
//             TMath::Abs(bigcalClusters.fCherenkovTDC)<12 && \
//             bigcalClusters.fCherenkovBestADCSum>0.4 &&\
//             bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
//    h1 = (TH1F*)gROOT->FindObject("clust4");
//    h1->SetLineColor(2003);
//    h1->Draw("same");

/*

   c->cd(2);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYStdDeviation>>clustY21(50,0,5)","bigcalClusters.fIsGood","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY21");
   h1->SetLineColor(2000);
   h1->Draw();

   t->Draw("bigcalClusters.fYStdDeviation>>clustY22(50,0,5)","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY22");
   h1->SetLineColor(2001);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYStdDeviation>>clustY23(50,0,5)",
           "bigcalClusters.fIsGood&& \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY23");
   h1->SetLineColor(2002);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYStdDeviation>>clustY24(50,0,5)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<12 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY24");
   h1->SetLineColor(2003);
   h1->Draw("same");

   c->cd(3);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYSkewness>>clustY31rcs(60,-5,5)","bigcalClusters.fIsGood&&bigcalClusters.fYMoment>0","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY31rcs");
   h1->SetLineColor(2000);
   h1->Draw();

   t->Draw("bigcalClusters.fYSkewness>>clustY32rcs(60,-5,5)",
           "bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4&&bigcalClusters.fYMoment>0","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY32rcs");
   h1->SetLineColor(2001);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYSkewness>>clustY33rcs(60,-5,5)",
           "bigcalClusters.fIsGood&& \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5&&bigcalClusters.fYMoment>0"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY33rcs");
   h1->SetLineColor(2002);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYSkewness>>clustY34rcs(60,-5,5)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<12 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5&&bigcalClusters.fYMoment>0"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY34rcs");
   h1->SetLineColor(2003);
   h1->Draw("same");


   c->cd(4);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYSkewness>>clustY31(60,-5,5)","bigcalClusters.fIsGood&&bigcalClusters.fYMoment<0","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY31");
   h1->SetLineColor(2000);
   h1->Draw();

   t->Draw("bigcalClusters.fYSkewness>>clustY32(60,-5,5)","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4&&bigcalClusters.fYMoment<0","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY32");
   h1->SetLineColor(2001);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYSkewness>>clustY33(60,-5,5)",
           "bigcalClusters.fIsGood&& \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5&&bigcalClusters.fYMoment<0"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY33");
   h1->SetLineColor(2002);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYSkewness>>clustY34(60,-5,5)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<12 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5&&bigcalClusters.fYMoment<0"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY34");
   h1->SetLineColor(2003);
   h1->Draw("same");

//    c->cd(3);
//    t->Draw("bigcalClusters.fY2Moment","bigcalClusters.fIsGood","");
//    t->Draw("bigcalClusters.fY2Moment","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","same");
// 
//    c->cd(4);
//    t->Draw("bigcalClusters.fY3Moment","bigcalClusters.fIsGood","");
//    t->Draw("bigcalClusters.fY3Moment","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","same");
// */

   c->SaveAs(Form("plots/%d/clustering5.png",runnumber));
   c->SaveAs(Form("plots/%d/clustering5.pdf",runnumber));
   c->SaveAs(Form("plots/%d/clustering5.svg",runnumber));


  return(0);
}
