Int_t merge_elastics(Int_t runGroup = 3){

   /// Build up the queue 
   aman->BuildQueue(Form("lists/para/run_series_%d.txt",runGroup));

   /// Get the chains for run queue
   TChain * chain =  aman->BuildChain("elastics");
//    TChain *  =  aman->BuildChain("3clusterPi0results");

   /// Merge chain into single tree
   TFile * f = new TFile(Form("data/elastics%d.root",runGroup),"RECREATE");
   f->cd();
   chain->Merge(f,32000,"keep");
   std::cout << " elastics Merge Complete. \n";

   f->Flush();

   delete chain;

   new TBrowser;

   return(0);
}
