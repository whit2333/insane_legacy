/** Clustering
 */
Int_t clustering1(Int_t runnumber = 72995) {

   const char * detectorTree = "betaDetectors1";

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

   TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   if( !t ) {
      printf("Tree:%s \n    was NOT FOUND. Aborting... \n",detectorTree); 
      return(-1);
   }

   gROOT->cd();

   TCanvas * c = new TCanvas("clustering1","Clustering 1");
   c->Divide(2,2);
   TH1F * h1;


   c->cd(1);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYMoment>>clust1(60,-120,120)","bigcalClusters.fIsGood","goff");
   h1 = (TH1F*)gROOT->FindObject("clust1");
   h1->SetLineColor(2000);
   h1->Draw();

   t->Draw("bigcalClusters.fYMoment>>clust2(60,-120,120)","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","goff");
   h1 = (TH1F*)gROOT->FindObject("clust2");
   h1->SetLineColor(2001);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYMoment>>clust3(60,-120,120)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clust3");
   h1->SetLineColor(2002);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYMoment>>clust4(60,-120,120)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>2.0"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clust4");
   h1->SetLineColor(2003);
   h1->Draw("same");



   c->cd(2);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYStdDeviation>>clustY21(50,0,5)","bigcalClusters.fIsGood","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY21");
   h1->SetLineColor(2000);
   h1->Draw();

   t->Draw("bigcalClusters.fYStdDeviation>>clustY22(50,0,5)","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY22");
   h1->SetLineColor(2001);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYStdDeviation>>clustY23(50,0,5)",
           "bigcalClusters.fIsGood&& \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY23");
   h1->SetLineColor(2002);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYStdDeviation>>clustY24(50,0,5)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<12 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY24");
   h1->SetLineColor(2003);
   h1->Draw("same");

   c->cd(3);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fXStdDeviation>>clustX21(50,0,5)","bigcalClusters.fIsGood","goff");
   h1 = (TH1F*)gROOT->FindObject("clustX21");
   h1->SetLineColor(2000);
   h1->Draw();

   t->Draw("bigcalClusters.fXStdDeviation>>clustX22(50,0,5)","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","goff");
   h1 = (TH1F*)gROOT->FindObject("clustX22");
   h1->SetLineColor(2001);
   h1->Draw("same");

   t->Draw("bigcalClusters.fXStdDeviation>>clustX23(50,0,5)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustX23");
   h1->SetLineColor(2002);
   h1->Draw("same");

   t->Draw("bigcalClusters.fXStdDeviation>>clustX24(50,0,5)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<10 && \
            bigcalClusters.fCherenkovBestADCSum>2.0"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustX24");
   h1->SetLineColor(2003);
   h1->Draw("same");


   c->cd(4);
//    t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","colz");
   t->Draw("bigcalClusters.fYSkewness>>clustY31(60,-5,5)","bigcalClusters.fIsGood","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY31");
   h1->SetLineColor(2000);
   h1->Draw();

   t->Draw("bigcalClusters.fYSkewness>>clustY32(60,-5,5)","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","goff");
   h1 = (TH1F*)gROOT->FindObject("clustY32");
   h1->SetLineColor(2001);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYSkewness>>clustY33(60,-5,5)",
           "bigcalClusters.fIsGood&& \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY33");
   h1->SetLineColor(2002);
   h1->Draw("same");

   t->Draw("bigcalClusters.fYSkewness>>clustY34(60,-5,5)",
           "bigcalClusters.fIsGood&& \
            TMath::Abs(bigcalClusters.fCherenkovTDC)<12 && \
            bigcalClusters.fCherenkovBestADCSum>0.4 &&\
            bigcalClusters.fCherenkovBestADCSum<1.5"  ,"goff");
   h1 = (TH1F*)gROOT->FindObject("clustY34");
   h1->SetLineColor(2003);
   h1->Draw("same");

//    c->cd(3);
//    t->Draw("bigcalClusters.fY2Moment","bigcalClusters.fIsGood","");
//    t->Draw("bigcalClusters.fY2Moment","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","same");
// 
//    c->cd(4);
//    t->Draw("bigcalClusters.fY3Moment","bigcalClusters.fIsGood","");
//    t->Draw("bigcalClusters.fY3Moment","bigcalClusters.fIsGood&&bigcalClusters.fCherenkovBestADCSum>0.4","same");
// 

   c->SaveAs(Form("plots/%d/clustering1.png",runnumber));
   c->SaveAs(Form("plots/%d/clustering1.pdf",runnumber));
   c->SaveAs(Form("plots/%d/clustering1.svg",runnumber));


  return(0);
}
