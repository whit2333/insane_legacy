Int_t binned_asymmetries_para59_nocer(Int_t runnumber = 72961, Int_t fileVersion = 15, Double_t E_min = 1300.0 ) {

   bool writeRun = false;
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun * run = rman->GetCurrentRun();
   InSANERunSummary rSum =  run->GetRunSummary();

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   bcgeo->LoadBadBlocks(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber));

   TTree * t = (TTree*) gROOT->FindObject("Tracks"); 
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);
   //SANEEvents * events = new SANEEvents("betaDetectors1");
   //events->fTree->BuildIndex("fRunNumber","fEventNumber");

   Int_t ev      = 0;
   Int_t runnum  = 0;
   Int_t stdcut  = 0;
   TH1F * h1     = 0;
   TH2F * h2     = 0;

   TTree * selectt = new TTree("cuts","The cuts");
   selectt->Branch("ev",&ev,"ev/I");
   selectt->Branch("runnum",&runnum,"runnum/I");
   selectt->Branch("stdcut",&stdcut,"stdcut/I");
   bool passed = false;

   ///std::cout << " Found the tree!\n";

   Double_t theta_range[2] = {0.4,1.0};
   Double_t phi_range[2] = {-0.6,1.0};
   Double_t energy_range[2] = {500,4500};

   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
         20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
         20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy = new TEfficiency("effEnergy","Cut Efficiency vs E;E;#epsilon",
         20,energy_range[0],energy_range[1]);

   // ------------------------------------------------
   // Create the asymmetries to be calculated.
   // ------------------------------------------------
   TList *  fAsymmetries = new TList();
   Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t QsqAvg[4];
   for(int i=0;i<4;i++){
      fAsymmetry = new SANEMeasuredAsymmetry(i);
      fAsymmetry->fQ2Min = QsqMin[i];
      fAsymmetry->fQ2Max = QsqMax[i];
      fAsymmetry->fGroup = 0;
      fAsymmetry->fBeamEnergy = 4.737;
      fAsymmetry->fDISEvent = dis;
      fAsymmetry->fRunSummary = rSum;//SetDirectory(f);
      fAsymmetry->Initialize();
      fAsymmetries->Add(fAsymmetry);
      QsqAvg[i] = (QsqMax[i]+QsqMin[i])/2.0;
   }

   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      t->GetEntry(iEvent);
      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;

      dis->fRunNumber         = traj->fRunNumber;
      runnum                  = traj->fRunNumber;
      fDisEvent->fEventNumber = traj->fEventNumber;
      ev                      = traj->fEventNumber;
      stdcut                  = 0;
      dis->fHelicity          = traj->fHelicity;
      dis->fx                 = traj->GetBjorkenX();
      dis->fW                 = TMath::Sqrt(traj->GetInvariantMassSquared())/1000.0;
      dis->fQ2                = traj->GetQSquared()/1000000.0;
      dis->fTheta             = traj->GetTheta();
      dis->fPhi               = traj->GetPhi();
      dis->fEnergy            = traj->GetEnergy()/1000.0;
      dis->fBeamEnergy        = traj->fBeamEnergy/1000.0;
      //dis->fGroup             = 0;//traj->fSubDetector;
      //dis->fIsGood            = false;
      //dis->fIsGood            = traj->fIsGood;
      Int_t ibl = bcgeo->GetBlockNumber(traj->fCluster.fiPeak,traj->fCluster.fjPeak);

      if( dis->fW > 1.0 )
         if( trig->IsBETA2Event() )   
            if( !traj->fIsNoisyChannel)
            if( traj->fIsGood)
            //if( traj->fNCherenkovElectrons > 0.4 && traj->fNCherenkovElectrons < 1.5 )
            if( !(bcgeo->IsBadBlock(ibl)) )
               if( traj->fEnergy > E_min )
                  if(TMath::Abs(traj->fCherenkovTDC) < 10) {
                     passed = true;
                     hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
                     stdcut = 1;
                     for(int j = 0; j<fAsymmetries->GetEntries();j++) {
                        ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
                     }

                  }
      effPhi->Fill(passed,traj->fPhi);
      effTheta->Fill(passed,traj->fTheta);
      effEnergy->Fill(passed,traj->fEnergy);

      selectt->Fill();

      //       if( TMath::Abs( traj->fPhi ) < 0.4 ) 
      //       if( trig->IsBETA2Event() )   
      //       if( TMath::Abs(traj->fCherenkovTDC) < 10 ) 
      //       if( traj->fIsGood ) 
      //       //if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 2.0 )
      //       {
      //           dis->fHelicity = traj->fHelicity;
      //           dis->fx        = traj->GetBjorkenX();
      //           dis->fW        = TMath::Sqrt(traj->GetInvariantMassSquared()/1.0e6);
      //           dis->fQ2       = traj->GetQSquared()/1.0e6;
      // 
      //           if(dis->fW > 1.0 ) { 
      //              if( traj->fEnergy > 900 ){
      //                 asym1->CountEvent();
      //                 if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
      //                    asym3->CountEvent();
      //              }
      //              if( traj->fEnergy > 1300 ) asym2->CountEvent();
      //           }
      // 
      //       }

   }

   //t->StartViewer();

   //Double_t x_1  = asym1->fxHist->GetMean();// asym1->fxHist->GetBinCenter(asym1->fxHist->GetMaximumBin());
   //Double_t Q2_1  = asym1->fQ2Hist->GetMean();//GetBinCenter(asym1->fQ2Hist->GetMaximumBin());
   //Double_t x_2  = asym2->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   //Double_t Q2_2  = asym2->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());
   //Double_t x_3  = asym3->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   //Double_t Q2_3  = asym3->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());

   Double_t pf = run->fPackingFraction;
   InSANEDilutionFromTarget * df = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget();
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   //    asym1->SetRunNumber(runnumber);
   //    asym2->SetRunNumber(runnumber);
   //    asym3->SetRunNumber(runnumber);
   //    asym1->SetRunSummary(&rSum);
   //    asym2->SetRunSummary(&rSum);
   //    asym3->SetRunSummary(&rSum);
   // 
   //    asym1->SetDilution(df->GetDilution(x_1,Q2_1));
   //    asym2->SetDilution(df->GetDilution(x_2,Q2_2));
   //    asym3->SetDilution(df->GetDilution(x_3,Q2_3));
   // 
   //    asym1->Calculate();
   //    asym2->Calculate();
   //    asym3->Calculate();
   // 
   //    asym1->Print();
   //    asym2->Print();
   //    asym3->Print();

   std::cout << " Calculating Asymmetries... \n";
   for(int j = 0;j<fAsymmetries->GetEntries();j++) {
      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      asy->SetDilutionFromTarget(df);
      Double_t x_mean = asy->fNPlusVsx->GetMean();
      asy->SetDilution(df->GetDilution(x_mean,asy->GetQ2()) );
      asy->SetRunNumber(runnumber);
      asy->SetRunSummary(&rSum);
      asy->CalculateAsymmetries();
   }

   for(int j = 0;j<fAsymmetries->GetEntries();j++) {
      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      std::ofstream fileout(Form("plots/%d/binned_asym1-%d.dat",runnumber,j),std::ios_base::trunc );
      asy->PrintBinnedTable(fileout) ;
      asy->Print();
      asy->PrintBinnedTable(std::cout) ;
   }


   //std::ofstream fileout1("asym/whit/para_asym1.dat",std::ios_base::app );
   //std::ofstream fileout2("asym/whit/para_asym2.dat",std::ios_base::app );
   //std::ofstream fileout3("asym/whit/para_asym3.dat",std::ios_base::app );
   //std::ofstream fileout4("asym/whit/para_asym4.dat",std::ios_base::app );

   //asym1->PrintTable(fileout1);
   //asym2->PrintTable(fileout2);
   //asym3->PrintTable(fileout3);
   //asym4->PrintTable(fileout4);
   //
   //run->fResults.Add(asym1);
   //run->fResults.Add(asym2);
   //run->fResults.Add(asym3);
   TFile * f = new TFile(Form("data/binned_asymmetries_para59_%d.root",fileVersion),"UPDATE");
   f->cd();
   f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 


   // ------------------------------------------------------------
   // Plot asymmetries
   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 
   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   TMultiGraph * mg = new TMultiGraph();
   for( int i = 0 ; i < fAsymmetries->GetEntries() ; i++) {
      SANEMeasuredAsymmetry * Asym = (SANEMeasuredAsymmetry*)fAsymmetries->At(i);
      if(i>3)Asym->fAsymmetryVsx->SetMarkerStyle(24);
      TGraphErrors * gr = new TGraphErrors(Asym->fAsymmetryVsx);
         for( int k = gr->GetN() -1; k>=0 ; k--) {
            Double_t xt, yt;
            gr->GetPoint(k,xt,yt);
            if( yt == 0.0 ) { gr->RemovePoint(k); }
         }
      mg->Add(gr,"p");
      leg->AddEntry(Asym->fAsymmetryVsx,Form("Q^{2} = %d",i+3),"ep");
   }
   mg->Draw("a");
   mg->SetMinimum(-0.5);
   mg->SetMaximum(1.0);
   mg->GetXaxis()->SetLimits(0.0,1.0);
   gPad->Modified();
   leg->Draw();
   //c->Update();
   gPad->Modified();
   c->SaveAs(Form("plots/%d/binned_asymmetries_para59%d.png",runnumber,fileVersion));

   if(writeRun) rman->WriteRun();
   //if(writeRun) gROOT->ProcessLine(".q");

   //delete events;
   delete traj;
   delete trig;
   delete dis;

   return(0);
}
