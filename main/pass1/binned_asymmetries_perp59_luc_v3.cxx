Int_t binned_asymmetries_perp59_luc_v3(Int_t runnumber = 72425 , Int_t fileVersion = 159, Double_t E_min  = 1300.0) {

   if( gROOT->LoadMacro("asym/init_asymmetries.cxx") )  {
      Error(weh, "Failed loading asym/init_asymmetries.cxx in compiled mode.");
      return 1;
   }

   //bool writeRun = false;
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun        * run = rman->GetCurrentRun();
   InSANERunSummary  rSum =  run->GetRunSummary();

   InSANEFunctionManager * fman  = InSANEFunctionManager::GetManager();
   fman->SetBeamEnergy(5.9);
   //fman->CreateSFs(6);
   //fman->CreatePolSFs(6);

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   // Still need this?
   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   bcgeo->LoadBadBlocks(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber));

   // ----------------------------------------------------------
   // Trees
   TTree * t = (TTree*) gROOT->FindObject("Tracks"); 
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);

   const char * trackingTree = "trackingPositions";
   TTree * t2 = (TTree*)gROOT->FindObject(trackingTree);
   if(!t2) {
      std::cout << "No Tree Found: " << trackingTree << "\n";
      return(-11);
   }
   t2->BuildIndex("fRunNumber","fEventNumber");
   t->AddFriend(t2);

   // ----------------------------------------------------------
   // Event classes
   InSANEReconstructedEvent * fRecon = new InSANEReconstructedEvent();
   t2->SetBranchAddress("uncorrelatedPositions",&fRecon);

   InSANEHitPosition * lucHit  = 0;
   BIGCALCluster     * cluster = 0;
   bool goodLucite             = false;
   int  goodClusterN           = 0;

   Int_t ev      = 0;
   Int_t runnum  = 0;
   Int_t stdcut  = 0;
   TH1F * h1     = 0;
   TH2F * h2     = 0;

   //TTree * selectt = new TTree("cuts","The cuts");
   //selectt->Branch("ev",&ev,"ev/I");
   //selectt->Branch("runnum",&runnum,"runnum/I");
   //selectt->Branch("stdcut",&stdcut,"stdcut/I");
   bool passed = false;


   //Double_t theta_range[2] = {0.4,1.0};
   //Double_t phi_range[2] = {-0.6,1.0};
   //Double_t energy_range[2] = {500,4500};

   //TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
   //      100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   //TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
   //      100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   //TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
   //      20,phi_range[0],phi_range[1]);
   //TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
   //      20,theta_range[0],theta_range[1]);
   //TEfficiency* effEnergy = new TEfficiency("effEnergy","Cut Efficiency vs E;E;#epsilon",
   //      20,energy_range[0],energy_range[1]);

   // ------------------------------------------------
   // Create the target and dilution from target
   Double_t pf                      = run->fPackingFraction;
   if(pf>1.0) pf = pf/100.0;
   InSANEDilutionFromTarget * df    = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget("UVaTarget","UVa Pol. Target",pf);
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   // ------------------------------------------------
   // Create the asymmetries to be calculated.
   TList *  fAsymmetries = init_asymmetries(dis,rSum,df);
   Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t QsqAvg[4];
   for(int i=0;i<4;i++){
      QsqAvg[i] = (QsqMax[i]+QsqMin[i])/2.0;
   }

   // -------------------------------------------------------------
   // Event Loop
   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      t->GetEntry(iEvent);
      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      Int_t bytes = t2->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      if(bytes <= 0) continue;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;

      dis->fRunNumber         = traj->fRunNumber;
      runnum                  = traj->fRunNumber;
      fDisEvent->fEventNumber = traj->fEventNumber;
      ev                      = traj->fEventNumber;
      stdcut                  = 0;
      dis->fHelicity          = traj->fHelicity;
      dis->fx                 = traj->GetBjorkenX();
      dis->fW                 = TMath::Sqrt(traj->GetInvariantMassSquared())/1000.0;
      dis->fQ2                = traj->GetQSquared()/1000000.0;
      dis->fTheta             = traj->fTheta;
      dis->fPhi               = traj->fPhi;
      dis->fEnergy            = traj->fEnergy/1000.0;
      dis->fBeamEnergy        = traj->fBeamEnergy/1000.0;
      dis->fGroup             = traj->fSubDetector;
      //dis->fIsGood            = false;
      //dis->fIsGood            = traj->fIsGood;
      Int_t ibl = bcgeo->GetBlockNumber(traj->fCluster.fiPeak,traj->fCluster.fjPeak);

      if( dis->fW > 1.0 )
         if( trig->IsBETA2Event() )   
            if( !traj->fIsNoisyChannel)
               if( traj->fIsGood) 
                  if( traj->fNCherenkovElectrons > 0.4 && traj->fNCherenkovElectrons < 1.6 )
                     if( !(bcgeo->IsBadBlock(ibl)) )
                        if( traj->fEnergy > E_min ) {
                           goodLucite = false;
                           if( fRecon->fNTrajectories > 0 ){ 

                              // Determine if there is a good lucite hit

                              for(int i = 0; i< fRecon->fNLucitePositions; i++){
                                 lucHit = (InSANEHitPosition*)(*(fRecon->fLucitePositions))[i];

                                 if( TMath::Abs(lucHit->fPositionUncertainty.Y() ) < 10 ) {
                                    if( TMath::Abs(lucHit->fTiming ) < 250 ) {
                                       goodLucite = true;
                                       goodClusterN = lucHit->fClusterNumber; 
                                       break;
                                    }
                                 }

                              }
                           }
                           if(goodLucite){

                              // Different timing cuts depending on the run
                              // Here the timing is set by bigcal
                              if( runnumber <= 72488 ){
                                 if( (TMath::Abs(traj->fCluster.fClusterTime1) < 3 ) || (TMath::Abs(traj->fCluster.fClusterTime2) < 3 ) ) 
                                    if(TMath::Abs(traj->fCherenkovTDC) < 200){ 
                                       passed = true;
                                       //hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
                                       stdcut = 1;
                                       for(int j = 0; j<fAsymmetries->GetEntries();j++) {
                                          ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
                                       }
                                    }
                                 // Here the timing is set by the cherenkov 
                              } else if( runnumber > 72488 ) {
                                 if(TMath::Abs(traj->fCherenkovTDC) < 10){ 
                                    passed = true;
                                    //hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
                                    stdcut = 1;
                                    for(int j = 0; j<fAsymmetries->GetEntries();j++) {
                                       ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
                                    }
                                 }
                              }
                           }
                        }
      //effPhi->Fill(passed,traj->fPhi);
      //effTheta->Fill(passed,traj->fTheta);
      //effEnergy->Fill(passed,traj->fEnergy);

      //selectt->Fill();

   }

   //selectt->Write();

   std::cout << " Calculating Asymmetries... \n";
   for(int j = 0;j<fAsymmetries->GetEntries();j++) {
      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      Double_t x_mean = asy->fNPlusVsx->GetMean();
      asy->SetDilution(df->GetDilution(x_mean,asy->GetQ2()) );
      asy->SetRunNumber(runnumber);
      asy->SetRunSummary(&rSum);
      asy->CalculateAsymmetries();
   }

   for(int j = 0;j<fAsymmetries->GetEntries();j++) {
      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      std::ofstream fileout(Form("plots/%d/binned_asym1-%d.dat",runnumber,j),std::ios_base::trunc );
      asy->PrintBinnedTable(fileout) ;
      asy->Print();
      asy->PrintBinnedTable(std::cout) ;
   }

   TFile * f = new TFile(Form("data/asym-%d/binned_asymmetries_perp59_%d.root",fileVersion,runnumber),"UPDATE");
   f->cd();
   f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 

   //if(writeRun) rman->WriteRun();

   //delete events;
   delete traj;
   delete trig;
   delete dis;

   return(0);
}
