Int_t run_asymmetries_perp5(Int_t runnumber = 72425, bool writeRun = false) {

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun * run = rman->GetCurrentRun();
   InSANERunSummary rSum =  run->GetRunSummary();
   
   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   TTree * t = (TTree*) gROOT->FindObject("Tracks"); 
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);
   //SANEEvents * events = new SANEEvents("betaDetectors1");
   //events->fTree->BuildIndex("fRunNumber","fEventNumber");
  
   // Create the asymmetries to be calculated.
   InSANERawAsymmetry * asym1 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym1");
   if(asym1) {
      run->fResults.Remove(asym1);
      delete asym1;
      asym1 = 0;
   }
   asym1 = new InSANERawAsymmetry();
   asym1->SetNameTitle("run-asym1","run asym E>900"); 

   InSANERawAsymmetry * asym2 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym2");
   if(asym2) {
      run->fResults.Remove(asym2);
      delete asym2;
      asym2 = 0;
   }
   asym2 = new InSANERawAsymmetry();
   asym2->SetNameTitle("run-asym2","run asym E>1300"); 

   InSANERawAsymmetry * asym3 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym3");
   if(asym3) {
      run->fResults.Remove(asym3);
      delete asym3;
      asym3 = 0;
   }
   asym3 = new InSANERawAsymmetry();
   asym3->SetNameTitle("run-asym3","run asym E>900 with Cer ADC"); 

   InSANERawAsymmetry * asym4 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym4");
   if(asym4) {
      run->fResults.Remove(asym4);
      delete asym4;
      asym4 = 0;
   }
   asym4 = new InSANERawAsymmetry();
   asym4->SetNameTitle("run-asym4","run asym E>1300 with Cer ADC"); 

   InSANERawAsymmetry * asym5 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym5");
   if(asym5) {
      run->fResults.Remove(asym5);
      delete asym5;
      asym5 = 0;
   }
   asym5 = new InSANERawAsymmetry();
   asym5->SetNameTitle("run-asym5","run asym E>1300 with Cer ADC"); 

   InSANERawAsymmetry * asym6 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym6");
   if(asym6) {
      run->fResults.Remove(asym6);
      delete asym6;
      asym6 = 0;
   }
   asym6 = new InSANERawAsymmetry();
   asym6->SetNameTitle("run-asym6","run asym E>1300 with Cer ADC"); 

   asym1->SetDISEvent(dis);
   asym2->SetDISEvent(dis);
   asym3->SetDISEvent(dis);
   asym4->SetDISEvent(dis);
   asym5->SetDISEvent(dis);
   asym6->SetDISEvent(dis);

   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      t->GetEntry(iEvent);
      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;
      //if( TMath::Abs( traj->fPhi ) < 0.4 ) 
      if( traj->fCluster.GetYmoment() < 100.0 )
      if( traj->fNClusters == 1 )
      if( trig->IsBETA2Event() )   
      if( TMath::Abs(traj->fCherenkovTDC) < 150 ) 
      if( (TMath::Abs(traj->fCluster.fClusterTime1) < 4 ) || (TMath::Abs(traj->fCluster.fClusterTime2) < 4 ) ) 
      //if( traj->fIsGood ) 
      //if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
      {
          dis->fHelicity = traj->fHelicity;
          dis->fx        = traj->GetBjorkenX();
          dis->fW        = TMath::Sqrt(traj->GetInvariantMassSquared()/1.0e6);
          dis->fQ2       = traj->GetQSquared()/1.0e6;

          if(dis->fW > 1.0 ) { 
             if( traj->fCluster.GetCorrectedEnergy() > 500 )
             if( traj->fCluster.GetCorrectedEnergy() < 800 ){
                asym1->CountEvent();
                if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
                   asym2->CountEvent();
             }
             if( traj->fCluster.GetCorrectedEnergy() > 800 )
             if( traj->fCluster.GetCorrectedEnergy() < 1000 ) {
                asym3->CountEvent();
                if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
                   asym4->CountEvent();
             }
             if( traj->fCluster.GetCorrectedEnergy() > 1000 )
             if( traj->fCluster.GetCorrectedEnergy() < 1500 ) {
                asym5->CountEvent();
                if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
                   asym6->CountEvent();
             }
          }

      }

   }

   //t->StartViewer();

   Double_t x_1  = asym1->fxHist->GetMean();// asym1->fxHist->GetBinCenter(asym1->fxHist->GetMaximumBin());
   Double_t Q2_1  = asym1->fQ2Hist->GetMean();//GetBinCenter(asym1->fQ2Hist->GetMaximumBin());
   Double_t x_2  = asym2->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   Double_t Q2_2  = asym2->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());
   Double_t x_3  = asym3->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   Double_t Q2_3  = asym3->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());
   Double_t x_4  = asym4->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   Double_t Q2_4 = asym4->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());
   Double_t x_5  = asym3->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   Double_t Q2_5  = asym3->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());
   Double_t x_6  = asym5->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   Double_t Q2_6 = asym6->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());

   Double_t pf = run->fPackingFraction;
   InSANEDilutionFromTarget * df = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget();
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   asym1->SetRunNumber(runnumber);
   asym2->SetRunNumber(runnumber);
   asym3->SetRunNumber(runnumber);
   asym4->SetRunNumber(runnumber);
   asym5->SetRunNumber(runnumber);
   asym6->SetRunNumber(runnumber);
   
   asym1->SetRunSummary(&rSum);
   asym2->SetRunSummary(&rSum);
   asym3->SetRunSummary(&rSum);
   asym4->SetRunSummary(&rSum);
   asym5->SetRunSummary(&rSum);
   asym6->SetRunSummary(&rSum);


   asym1->SetDilution(df->GetDilution(x_1,Q2_1));
   asym2->SetDilution(df->GetDilution(x_2,Q2_2));
   asym3->SetDilution(df->GetDilution(x_3,Q2_3));
   asym4->SetDilution(df->GetDilution(x_4,Q2_4));
   asym5->SetDilution(df->GetDilution(x_5,Q2_5));
   asym6->SetDilution(df->GetDilution(x_6,Q2_6));

   asym1->Calculate();
   asym2->Calculate();
   asym3->Calculate();
   asym4->Calculate();
   asym5->Calculate();
   asym6->Calculate();

   asym1->Print();
   asym2->Print();
   asym3->Print();
   asym4->Print();
   asym5->Print();
   asym6->Print();
   
   std::ofstream fileout1("asym/whit/perp_asym5-1.dat",std::ios_base::app );
   std::ofstream fileout2("asym/whit/perp_asym5-2.dat",std::ios_base::app );
   std::ofstream fileout3("asym/whit/perp_asym5-3.dat",std::ios_base::app );
   std::ofstream fileout4("asym/whit/perp_asym5-4.dat",std::ios_base::app );
   std::ofstream fileout5("asym/whit/perp_asym5-5.dat",std::ios_base::app );
   std::ofstream fileout6("asym/whit/perp_asym5-6.dat",std::ios_base::app );

   asym1->PrintTable(fileout1);
   asym2->PrintTable(fileout2);
   asym3->PrintTable(fileout3);
   asym4->PrintTable(fileout4);
   asym5->PrintTable(fileout5);
   asym6->PrintTable(fileout6);
   
   run->fResults.Add(asym1);
   run->fResults.Add(asym2);
   run->fResults.Add(asym3);
   run->fResults.Add(asym4);
   run->fResults.Add(asym5);
   run->fResults.Add(asym6);

   if(writeRun) rman->WriteRun();
   //if(writeRun) gROOT->ProcessLine(".q");

   //delete events;
   delete traj;
   delete trig;
   delete dis;

   return(0);
}
