Int_t run_asymmetries4(Int_t runnumber = 73017, bool writeRun = true) {
   if (gROOT->LoadMacro("asym/vs_run/read_marks2.cxx") != 0) {
      Error(weh, "Failed loading read_marks.cxx in compiled mode.");
      return -1;
   }
   TString mark_file1 = "asym/mark/para_all_900_run.dat";
   TString mark_file2 = "asym/mark/para_all_1300_run.dat";
   std::map<int,double> map_N;
   std::map<int,double> map_AN;
   std::map<int,double> map_A;
   std::map<int,double> map_pt;
   std::map<int,double> map_pb;
   std::map<int,double> map_df;
   std::map<int,double> map_df2;
   Int_t retval = read_marks2(mark_file1.Data(),&map_pt, 3);
   retval       = read_marks2(mark_file2.Data(),&map_pb, 4);
   retval       = read_marks2(mark_file1.Data(),&map_df, 2);
   retval       = read_marks2(mark_file2.Data(),&map_df2, 2);

   retval       = read_marks2(mark_file2.Data(),&map_A, 7);
   retval       = read_marks2(mark_file2.Data(),&map_AN, 5);
   retval       = read_marks2(mark_file2.Data(),&map_N, 1);

   Double_t Am     = -1.0*map_A[runnumber]*(map_df[runnumber]*map_pb[runnumber]*map_pt[runnumber]);
   Double_t AN     = map_AN[runnumber];
   Double_t Ntot   = map_N[runnumber];
   Double_t X1norm = (-1.0 + AN - Am *(1.0 + AN))/((-1.0 + Am)*(1.0+AN));
   Double_t n1     = 0.5*(1.0+AN)*Ntot;
   Double_t n2     = 0.5*(1.0-AN)*Ntot;

   std::cout << "The charge and livetime ratio is " << X1norm << std::endl;
   std::cout << n1 << " , " << n2 << std::endl;
   std::cout << "Pt[" << runnumber << "] = " << map_pt[runnumber] << std::endl;
   std::cout << "df[" << runnumber << "] = " << map_df[runnumber] << std::endl;
   std::cout << "df2[" << runnumber << "] = " << map_df2[runnumber] << std::endl;

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun * run = rman->GetCurrentRun();
   InSANERunSummary rSum =  run->GetRunSummary();
   rSum.SetPt(map_pt[runnumber]);
   
   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   TTree * t = (TTree*) gROOT->FindObject("Tracks"); 
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);
   //SANEEvents * events = new SANEEvents("betaDetectors1");
   //events->fTree->BuildIndex("fRunNumber","fEventNumber");
  
   // Create the asymmetries to be calculated.
   InSANERawAsymmetry * asym1 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym4-1");
   if(asym1) {
      run->fResults.Remove(asym1);
      delete asym1;
      asym1 = 0;
   }
   asym1 = new InSANERawAsymmetry();
   asym1->SetNameTitle("run-asym4-1","run asym E>900"); 

   InSANERawAsymmetry * asym2 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym4-2");
   if(asym2) {
      run->fResults.Remove(asym2);
      delete asym2;
      asym2 = 0;
   }
   asym2 = new InSANERawAsymmetry();
   asym2->SetNameTitle("run-asym4-2","run asym E>1300"); 

   InSANERawAsymmetry * asym3 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym4-3");
   if(asym3) {
      run->fResults.Remove(asym3);
      delete asym3;
      asym3 = 0;
   }
   asym3 = new InSANERawAsymmetry();
   asym3->SetNameTitle("run-asym4-3","run asym E>900 with Cer ADC"); 

   InSANERawAsymmetry * asym4 = (InSANERawAsymmetry*) run->fResults.FindObject("run-asym4-4");
   if(asym4) {
      run->fResults.Remove(asym4);
      delete asym4;
      asym4 = 0;
   }
   asym4 = new InSANERawAsymmetry();
   asym4->SetNameTitle("run-asym4-4","run asym E>1300 with Cer ADC"); 

   asym1->SetDISEvent(dis);
   asym2->SetDISEvent(dis);
   asym3->SetDISEvent(dis);
   asym4->SetDISEvent(dis);

   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      t->GetEntry(iEvent);
      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;
      //if( TMath::Abs( traj->fPhi ) < 0.4 ) 
      if( trig->IsBETA2Event() )   
      if( TMath::Abs(traj->fCherenkovTDC) < 10 ) 
      if( traj->fIsGood ) 
      if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 2.0 )
      {
          dis->fHelicity = traj->fHelicity;
          dis->fx        = traj->GetBjorkenX();
          dis->fW        = TMath::Sqrt(traj->GetInvariantMassSquared()/1.0e6);
          dis->fQ2       = traj->GetQSquared()/1.0e6;

          if(dis->fW > 1.0 ) { 
             if( traj->fCluster.GetCorrectedEnergy() > 900 ){
                asym1->CountEvent();
                if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
                   asym3->CountEvent();
             }
             if( traj->fCluster.GetCorrectedEnergy() > 1300 ) {
                asym2->CountEvent();
                if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
                   asym4->CountEvent();
             }
          }

      }

   }

   //t->StartViewer();

   Double_t x_1  = asym1->fxHist->GetMean();// asym1->fxHist->GetBinCenter(asym1->fxHist->GetMaximumBin());
   Double_t Q2_1  = asym1->fQ2Hist->GetMean();//GetBinCenter(asym1->fQ2Hist->GetMaximumBin());
   Double_t x_2  = asym2->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   Double_t Q2_2  = asym2->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());
   Double_t x_3  = asym3->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   Double_t Q2_3  = asym3->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());
   Double_t x_4  = asym4->fxHist->GetMean();//asym2->fxHist->GetBinCenter(asym2->fxHist->GetMaximumBin());
   Double_t Q2_4 = asym4->fQ2Hist->GetMean();//GetBinCenter(asym2->fQ2Hist->GetMaximumBin());

   Double_t pf = run->fPackingFraction;
   InSANEDilutionFromTarget * df = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget();
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   asym1->SetRunNumber(runnumber);
   asym2->SetRunNumber(runnumber);
   asym3->SetRunNumber(runnumber);
   asym4->SetRunNumber(runnumber);
   asym1->SetRunSummary(&rSum);
   asym2->SetRunSummary(&rSum);
   asym3->SetRunSummary(&rSum);
   asym4->SetRunSummary(&rSum);


   asym1->SetDilution(df->GetDilution(x_1,Q2_1));
   asym2->SetDilution(df->GetDilution(x_2,Q2_2));
   asym3->SetDilution(df->GetDilution(x_3,Q2_3));
   asym4->SetDilution(df->GetDilution(x_4,Q2_4));

   asym1->Calculate();
   asym2->Calculate();
   asym3->Calculate();
   asym4->Calculate();

   asym1->Print();
   asym2->Print();
   asym3->Print();
   asym4->Print();
   
   std::ofstream fileout1("asym/whit/para_asym4-1.dat",std::ios_base::app );
   std::ofstream fileout2("asym/whit/para_asym4-2.dat",std::ios_base::app );
   std::ofstream fileout3("asym/whit/para_asym4-3.dat",std::ios_base::app );
   std::ofstream fileout4("asym/whit/para_asym4-4.dat",std::ios_base::app );

   asym1->PrintTable(fileout1);
   asym2->PrintTable(fileout2);
   asym3->PrintTable(fileout3);
   asym4->PrintTable(fileout4);
   
   run->fResults.Add(asym1);
   run->fResults.Add(asym2);
   run->fResults.Add(asym3);
   run->fResults.Add(asym4);

   if(writeRun) rman->WriteRun();
   //if(writeRun) gROOT->ProcessLine(".q");

   //delete events;
   delete traj;
   delete trig;
   delete dis;

   return(0);
}
