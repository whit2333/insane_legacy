Int_t binned_asymmetries_perp59_split(Int_t runnumber = 72424,Int_t fileVersion = 17, Double_t E_min  = 1000.0) {

   bool writeRun = false;
   Int_t runGroup = 1;

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun * run = rman->GetCurrentRun();
   InSANERunSummary rSum =  run->GetRunSummary();

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   TTree * t = (TTree*) gROOT->FindObject("Tracks"); 
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);
   //SANEEvents * events = new SANEEvents("betaDetectors1");
   //events->fTree->BuildIndex("fRunNumber","fEventNumber");

   Int_t ev      = 0;
   Int_t runnum  = 0;
   Int_t stdcut  = 0;
   TH1F * h1     = 0;
   TH2F * h2     = 0;

   TTree * selectt = new TTree("cuts","The cuts");
   selectt->Branch("ev",&ev,"ev/I");
   selectt->Branch("runnum",&runnum,"runnum/I");
   selectt->Branch("stdcut",&stdcut,"stdcut/I");
   bool passed = false;

   ///std::cout << " Found the tree!\n";

   Double_t theta_range[2] = {0.4,1.0};
   Double_t phi_range[2] = {-0.6,1.0};
   Double_t energy_range[2] = {500,4500};

   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
         20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
         20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy = new TEfficiency("effEnergy","Cut Efficiency vs E;E;#epsilon",
         20,energy_range[0],energy_range[1]);

   // ------------------------------------------------
   // Create the asymmetries to be calculated.
   // ------------------------------------------------
   TList *  fAsymmetries = new TList();
   Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t QsqAvg[4];
   //RCS
   for(int i=0;i<4;i++){
      fAsymmetry = new SANEMeasuredAsymmetry(i);
      fAsymmetry->fQ2Min = QsqMin[i];
      fAsymmetry->fQ2Max = QsqMax[i];
      fAsymmetry->fGroup = 0;
      fAsymmetry->fBeamEnergy = 5.9;
      fAsymmetry->fDISEvent = dis;
      fAsymmetry->fRunSummary = rSum;//SetDirectory(f);
      fAsymmetry->Initialize();
      fAsymmetries->Add(fAsymmetry);
      QsqAvg[i] = (QsqMax[i]+QsqMin[i])/2.0;
   }
   //Protvino
   for(int i=0;i<4;i++){
      fAsymmetry = new SANEMeasuredAsymmetry(i+4);
      fAsymmetry->fQ2Min = QsqMin[i];
      fAsymmetry->fQ2Max = QsqMax[i];
      fAsymmetry->fGroup = 0;
      fAsymmetry->fBeamEnergy = 5.9;
      fAsymmetry->fDISEvent = dis;
      fAsymmetry->fRunSummary = rSum;//SetDirectory(f);
      fAsymmetry->Initialize();
      fAsymmetries->Add(fAsymmetry);
      QsqAvg[i] = (QsqMax[i]+QsqMin[i])/2.0;
   }

   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      t->GetEntry(iEvent);
      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;

      dis->fRunNumber         = traj->fRunNumber;
      runnum                  = traj->fRunNumber;
      fDisEvent->fEventNumber = traj->fEventNumber;
      ev                      = traj->fEventNumber;
      stdcut                  = 0;
      dis->fHelicity          = traj->fHelicity;
      dis->fx                 = traj->GetBjorkenX();
      dis->fW                 = TMath::Sqrt(traj->GetInvariantMassSquared())/1000.0;
      dis->fQ2                = traj->GetQSquared()/1000000.0;
      dis->fTheta             = traj->GetTheta();
      dis->fPhi               = traj->GetPhi();
      dis->fEnergy            = traj->GetEnergy()/1000.0;
      dis->fBeamEnergy        = traj->fBeamEnergy/1000.0;
      //dis->fGroup             = 0;//traj->fSubDetector;
      //dis->fIsGood            = false;
      //dis->fIsGood            = traj->fIsGood;
      if( dis->fW > 1.0 )
         if( trig->IsBETA2Event() )   
            if( !traj->fIsNoisyChannel)
               if( traj->fIsGood) 
                  if( traj->fNCherenkovElectrons > 0.6 && traj->fNCherenkovElectrons < 1.5 )
                     if( traj->fEnergy > E_min ) {

                        // Different timing cuts depending on the run
                        // Here the timing is set by bigcal
                        if( runnumber <= 72488 ){
                           if( (TMath::Abs(traj->fCluster.fClusterTime1) < 3 ) || (TMath::Abs(traj->fCluster.fClusterTime2) < 3 ) ) 
                              if(TMath::Abs(traj->fCherenkovTDC) < 200){ 
                                 passed = true;
                                 hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
                                 stdcut = 1;
                                 if(traj->fCluster->fjPeak > 32){
                                    for(int j = 0; j<4;j++) {
                                       ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
                                    }
                                 }
                                 // protvino 
                                 if(traj->fCluster->fjPeak < 33){
                                    for(int j = 4; j<8;j++) {
                                       ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
                                    }
                                 }
                              }
                           // Here the timing is set by the cherenkov 
                        } else if( runnumber > 72488 ) {
                           if(TMath::Abs(traj->fCherenkovTDC) < 10){ 
                              passed = true;
                              hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
                              stdcut = 1;
                              if(traj->fCluster->fjPeak > 32){
                                 for(int j = 0; j<4;j++) {
                                    ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
                                 }
                              }
                              // protvino 
                              if(traj->fCluster->fjPeak < 33){
                                 for(int j = 4; j<8;j++) {
                                    ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
                                 }
                              }
                           }
                        }
                     }

      //if( dis->fW > 1.0 )
      //   if( trig->IsBETA2Event() )   
      //      if( !traj->fIsNoisyChannel)
      //      if( traj->fIsGood)
      //      if( (TMath::Abs(traj->fCluster.fClusterTime1) < 15 ) || (TMath::Abs(traj->fCluster.fClusterTime2) < 15 ) ) 
      //      if( traj->fNCherenkovElectrons > 0.4 && traj->fNCherenkovElectrons < 1.5 )
      //         if( traj->fEnergy > 1000 )
      //            if(TMath::Abs(traj->fCherenkovTDC) < 50) {
      //               passed = true;
      //               hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
      //               stdcut = 1;
      //               // rcs
      //               if(traj->fCluster->fjPeak > 32){
      //                  for(int j = 0; j<4;j++) {
      //                     ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
      //                  }
      //               }
      //               // protvino 
      //               if(traj->fCluster->fjPeak < 33){
      //                  for(int j = 4; j<8;j++) {
      //                     ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
      //                  }
      //               }

      //            }
      effPhi->Fill(passed,traj->fPhi);
      effTheta->Fill(passed,traj->fTheta);
      effEnergy->Fill(passed,traj->fEnergy);

      selectt->Fill();

   }

   Double_t pf = run->fPackingFraction;
   InSANEDilutionFromTarget * df = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget();
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   std::cout << " Calculating Asymmetries... \n";
   for(int j = 0;j<fAsymmetries->GetEntries();j++) {
      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      asy->SetDilutionFromTarget(df);
      Double_t x_mean = asy->fNPlusVsx->GetMean();
      asy->SetDilution(df->GetDilution(x_mean,asy->GetQ2()) );
      asy->SetRunNumber(runnumber);
      asy->SetRunSummary(&rSum);
      asy->CalculateAsymmetries();
   }

   for(int j = 0;j<fAsymmetries->GetEntries();j++) {
      SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
      std::ofstream fileout(Form("plots/%d/binned_asym1-%d.dat",runnumber,j),std::ios_base::trunc );
      asy->PrintBinnedTable(fileout) ;
      asy->Print();
      asy->PrintBinnedTable(std::cout) ;
   }


   TFile * f = new TFile(Form("data/binned_asymmetries_perp59_%d.root",fileVersion),"UPDATE");
   f->cd();
   f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 

   if(writeRun) rman->WriteRun();

   delete traj;
   delete trig;
   delete dis;

   return(0);
}
