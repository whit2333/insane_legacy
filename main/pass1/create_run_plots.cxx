Int_t create_run_plots(){

//    gStyle->SetOptStat(0);
//    gROOT->SetStyle("Plain");
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   SANERunManager * rman = (SANERunManager *)InSANERunManager::GetRunManager();

   /// Sets up a canvas
/*   TCanvas *c1 = new TCanvas("asymCanvas","Raw Asymmetries",1200,400);*/
   // c1->SetFillColor(42);
/*   c1->SetGrid();*/
   //c1->Divide(1,2);

   InSANERunSeries * aSeries= new InSANERunSeries("Asym");

   InSANERunPolarization * beamPol = new InSANERunBeamPol("beamPol","Beam Polarization");
   beamPol->fMarkerColor = 8;
   beamPol->fMarkerStyle = 33;

   InSANERunPolarization * targTopPos = new InSANERunTargTopPosPol("topTargPosPol","Top Target Pos. Pol. ");
   targTopPos->fMarkerColor = 2;
   targTopPos->fMarkerStyle = 22;

   InSANERunPolarization * targTopNeg = new InSANERunTargTopNegPol("topTargNegPol","Top Target Neg. Pol. ");
   targTopNeg->fMarkerColor = 4;
   targTopNeg->fMarkerStyle = 22;

   InSANERunPolarization * targBotPos = new InSANERunTargBotPosPol("BottomTargPosPol","Bottom Target Pos. Pol. ");
   targBotPos->fMarkerColor = 2;
   targBotPos->fMarkerStyle = 23;

   InSANERunPolarization * targBotNeg = new InSANERunTargBotNegPol("BottomTargNegPol","Bottom Target Neg. Pol. ");
   targBotNeg->fMarkerColor = 4;
   targBotNeg->fMarkerStyle = 23;

   InSANERunAsymmetry * asym = new InSANERunAsymmetry();
   asym->fMarkerColor = 1;
   asym->fMarkerStyle = 20;
   asym->fMarkerSize = 1.5;

   aSeries->AddRunQuantity(beamPol);
   aSeries->AddRunQuantity(targTopPos);
   aSeries->AddRunQuantity(targTopNeg);
   aSeries->AddRunQuantity(targBotPos);
   aSeries->AddRunQuantity(targBotNeg);
   aSeries->AddRunQuantity(asym);

   /// Build up a run queue to be plotted
/*  aman->BuildQueue("lists/para/run_series1.txt");*/
/*  aman->BuildQueue("lists/para/all_para.txt");*/
  aman->BuildQueue("lists/para/all_para.txt");
/*   aman->QueueUpParallel();*/
//aman->QueueUpPerpendicular(); 
   //aman->QueueUpPerpendicularNH3Runs();
//   aman->QueueUp5PassRuns();
/*   aman->QueueUpTestRuns();*/
/*   aman->BuildQueue("lists/temp.list");*/
//    aman->BuildQueue("replaying_list.txt");
//    aman->GetRunQueue()->push_back(72994);
//    aman->GetRunQueue()->push_back(72995);
//    aman->GetRunQueue()->push_back(72999);
//    aman->GetRunQueue()->push_back(72933);
//    aman->GetRunQueue()->push_back(73005);
//    aman->GetRunQueue()->push_back(72934);
//    aman->GetRunQueue()->push_back(72991);
//    aman->GetRunQueue()->push_back(72991);
//    aman->GetRunQueue()->push_back(72990);
//    aman->GetRunQueue()->push_back(73041);
//    aman->GetRunQueue()->push_back(73040);
//    aman->GetRunQueue()->push_back(73023);
//    aman->GetRunQueue()->push_back(73022);
//    aman->GetRunQueue()->push_back(73038);


   int k=0;

   for(int i = 0;i<aman->fRunQueue->size();i++) {
      rman->SetRun(aman->fRunQueue->at(i));
      if( !( rman->GetCurrentRun()->fAnalysisPass < 1) ) {
        /// If the run has been fully analyzed then use it otherwise skip it out. 
        InSANERun * somerun = rman->GetCurrentRun();
        gROOT->cd();
        if(somerun)
           aSeries->AddRun(new InSANERun(*somerun) );
        k++;
        
      }
/*      if(k>40) break;*/
   }

   aSeries->GenerateGraphs();
   TCanvas * c1 = aSeries->Draw();

//    std::vector<double> configChangeX;
//    std::vector<double> configBeam;
//       TString query = " SELECT Start_run,gpbeam FROM exp_configuration; ";
//       TSQLResult * res = SANEDatabaseManager::GetManager()->dbServer->Query(query.Data());
//       TSQLRow * row;
//       for(int i = 0 ;i< res->GetRowCount();i++ ){
//          row = res->Next();
//          std::cout << row->GetField(0) << "    " << row->GetField(1) << "\n";
//          configChangeX.push_back(atoi(row->GetField(0)) );
//          configBeam.push_back(atof(row->GetField(1))/100.0 );
//       }
//    TVectorD configx;
//    configx.Use(configChangeX.size(), &configChangeX[0]);
// 
//    TVectorD beamEnergy;
//    beamEnergy.Use(configBeam.size(), &configBeam[0]);
// 
//    TGraph *  grBeam = new TGraph(configx,beamEnergy);
//    grBeam->SetMarkerColor(3);
//    grBeam->SetLineColor(2);
//    grBeam->SetLineWidth(3);
//    grBeam->SetMarkerSize(1.6);
//    grBeam->SetMarkerStyle(29);
//
//    std::vector<double> detChangeX;
//    std::vector<double> detChangeY;
// 
//       query = " SELECT first_run FROM detector_changes; ";
//       /*TSQLResult * */res = SANEDatabaseManager::GetManager()->dbServer->Query(query.Data());
// //       TSQLRow * row;
//       for(int i = 0 ;i< res->GetRowCount();i++ ){
//          row = res->Next();
//          std::cout << row->GetField(0) <<  "\n";
//          detChangeX.push_back(atoi(row->GetField(0)) );
//          detChangeY.push_back(0.1);
//       }
//    TVectorD detX;
//    detX.Use(detChangeX.size(), &detChangeX[0]);
// 
//    TVectorD detY;
//    detY.Use(detChangeY.size(), &detChangeY[0]);
// 

//    TGraph * grDet= new TGraph(detX,detY);
//    grDet->SetMarkerColor(6);
//    grDet->SetLineColor(2);
//    grDet->SetLineWidth(3);
//    grDet->SetMarkerSize(1.3);
//    grDet->SetMarkerStyle(34);



//    TLegend * leg = new TLegend(0.2,0.75,0.5,0.99);
//    leg->SetHeader("Asymmetry vs Run");
// 
//    TMultiGraph *mg = new TMultiGraph();
//    TGraphErrors * aGraph;
//    TListIter grIter1(topPositiveRuns.GenerateGraphs());
//    int isFirst=1;
//    if(topPositiveRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter1.Next() ) ) {
//       mg->Add(aGraph,"P");
//       if(isFirst==1)leg->AddEntry(aGraph,topPositiveRuns.fLegend,"p");
//       isFirst=0;
//    }
//    TListIter grIter2(bottomPositiveRuns.GenerateGraphs());
//    isFirst=1;
//    if(bottomPositiveRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter2.Next() ) ) {
//       mg->Add(aGraph,"P");
//       if(isFirst==1)leg->AddEntry(aGraph,bottomPositiveRuns.fLegend,"p");
//       isFirst=0;
//    }
//    TListIter grIter3(topNegativeRuns.GenerateGraphs());
//    isFirst=1;
//    if(topNegativeRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter3.Next() ) ) {
//       mg->Add(aGraph,"P");
//       if(isFirst==1)leg->AddEntry(aGraph,topNegativeRuns.fLegend,"p");
//       isFirst=0;
//    }
//    TListIter grIter4(bottomNegativeRuns.GenerateGraphs());
//    if(bottomNegativeRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter4.Next() ) ) {
//       mg->Add(aGraph,"P");
//       if(isFirst==1)leg->AddEntry(aGraph,bottomNegativeRuns.fLegend,"p");
//        isFirst=0;
//   }
//    TListIter grIter5(unpolarizedRuns.GenerateGraphs());
//    isFirst=1;
//    if(unpolarizedRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter5.Next() ) ) {
//       mg->Add(aGraph,"P");
//       if(isFirst==1)leg->AddEntry(aGraph,unpolarizedRuns.fLegend,"p");
//       isFirst=0;
//    }
//    TF1 * zero = new TF1("f1","0",72000,73500);
//    zero->SetLineColor(1);
//    zero->SetLineWidth(1);
// 
// 
//    mg->Add(grBeam,"P");
//    mg->Add(grDet,"P");
//    leg->AddEntry(grDet,"Detector Changes","p");
//    leg->AddEntry(grBeam,"Config Changes","p");
// 
//    //   c1->cd(1);
//    mg->SetMaximum(0.15);
//    mg->SetMinimum(-0.1);
//    mg->Draw("a");
//    mg->SetMaximum(0.15);
//    mg->SetMinimum(-0.1);
//    zero->Draw("same");
//    leg->Draw();

//    leg->SetHeader("Asymmetry vs Run");
//    leg->AddEntry(grA,"Asymmetries with errors","lep");
// /*   leg->AddEntry("","Function abs(#frac{sin(x)}{x})","l");*/
//    leg->AddEntry(grPBT,"Pb*Pt/10","p");

//    leg->Draw();


   c1->SaveAs("results/runseriestest.png");
   c1->SaveAs("results/runseriestest.ps");
   return(0);
}
