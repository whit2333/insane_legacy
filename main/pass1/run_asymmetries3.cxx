Int_t run_asymmetries3(Int_t runnumber = 73017, bool writeRun = true) {

   const Int_t nEbins = 20;
   Double_t Elow      = 500;
   Double_t Ehigh     = 2500;
   Double_t deltaE    = (Ehigh-Elow)/double(nEbins);

   if (gROOT->LoadMacro("asym/vs_run/read_marks2.cxx") != 0) {
      Error(weh, "Failed loading read_marks.cxx in compiled mode.");
      return -1;
   }
   TString mark_file1 = "asym/mark/para_all_900_run.dat";
   TString mark_file2 = "asym/mark/para_all_1300_run.dat";
   std::map<int,double> map_N;
   std::map<int,double> map_AN;
   std::map<int,double> map_A;
   std::map<int,double> map_pt;
   std::map<int,double> map_pb;
   std::map<int,double> map_df;
   std::map<int,double> map_df2;
   Int_t retval = read_marks2(mark_file1.Data(),&map_pt, 3);
   retval       = read_marks2(mark_file2.Data(),&map_pb, 4);
   retval       = read_marks2(mark_file1.Data(),&map_df, 2);
   retval       = read_marks2(mark_file2.Data(),&map_df2, 2);

   retval       = read_marks2(mark_file2.Data(),&map_A, 7);
   retval       = read_marks2(mark_file2.Data(),&map_AN, 5);
   retval       = read_marks2(mark_file2.Data(),&map_N, 1);

   Double_t Am     = -1.0*map_A[runnumber]*(map_df[runnumber]*map_pb[runnumber]*map_pt[runnumber]);
   Double_t AN     = map_AN[runnumber];
   Double_t Ntot   = map_N[runnumber];
   Double_t X1norm = (-1.0 + AN - Am *(1.0 + AN))/((-1.0 + Am)*(1.0+AN));
   Double_t n1     = 0.5*(1.0+AN)*Ntot;
   Double_t n2     = 0.5*(1.0-AN)*Ntot;

   std::cout << "The charge and livetime ratio is " << X1norm << std::endl;
   std::cout << n1 << " , " << n2 << std::endl;
   std::cout << "Pt[" << runnumber << "] = " << map_pt[runnumber] << std::endl;
   std::cout << "df[" << runnumber << "] = " << map_df[runnumber] << std::endl;
   std::cout << "df2[" << runnumber << "] = " << map_df2[runnumber] << std::endl;

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun * run = rman->GetCurrentRun();
   InSANERunSummary rSum =  run->GetRunSummary();
   rSum.SetPt(map_pt[runnumber]);
   
   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   TTree * t = (TTree*) gROOT->FindObject("Tracks"); 
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);
   //SANEEvents * events = new SANEEvents("betaDetectors1");
   //events->fTree->BuildIndex("fRunNumber","fEventNumber");
 
   InSANERawAsymmetry * asym = 0;
   TList * fAsymmetries = new TList();
   for(int i = 0; i<nEbins; i++) {
      Int_t ELabel = Elow + i*deltaE;
      asym =  new InSANERawAsymmetry();
      asym->SetNameTitle(Form("run-asym32-%d",ELabel),Form("run asym E>%d",ELabel) ); 
      asym->SetDISEvent(dis);
      fAsymmetries->Add(asym);
   }

   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      t->GetEntry(iEvent);
      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;
      //if( TMath::Abs( traj->fPhi ) < 0.4 ) 
      if( trig->IsBETA2Event() )   
         if( TMath::Abs(traj->fCherenkovTDC) < 10 ) 
            if( traj->fIsGood ) 
               if(traj->fCluster.GetCorrectedEnergy() > Elow && traj->fCluster.GetCorrectedEnergy() < Ehigh )
                 if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 2.0 )
               {
                  Int_t iAsym = (traj->fCluster.GetCorrectedEnergy() - Elow);
                  iAsym = iAsym/100;

                  if(iAsym >= 0 && iAsym < fAsymmetries->GetEntries() ) {
                     asym = (InSANERawAsymmetry*) fAsymmetries->At(iAsym); 

                     dis->fHelicity = traj->fHelicity;
                     dis->fx        = traj->GetBjorkenX();
                     dis->fW        = TMath::Sqrt(traj->GetInvariantMassSquared()/1.0e6);
                     dis->fQ2       = traj->GetQSquared()/1.0e6;

                     if(dis->fW > 1.0 ) { 
                        asym->CountEvent();
                        //   if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
                        //      asym4->CountEvent();
                     }

                  }
               }

   }


   Double_t pf = run->fPackingFraction;
   InSANEDilutionFromTarget * df = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget();
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   for(int i = 0; i<nEbins; i++ ){
      asym = (InSANERawAsymmetry*)fAsymmetries->At(i);
      asym->SetRunNumber(runnumber);
      asym->SetRunSummary(&rSum);
      Double_t x  = asym->fxHist->GetMean();// asym1->fxHist->GetBinCenter(asym1->fxHist->GetMaximumBin());
      Double_t Q2  = asym->fQ2Hist->GetMean();//GetBinCenter(asym1->fQ2Hist->GetMaximumBin());
      asym->SetDilution(df->GetDilution(x,Q2));
      asym->Calculate();
      asym->Print();
      std::ofstream fileout(Form("asym/whit/para_asym32-%d.dat",i),std::ios_base::app );
      asym->PrintTable(fileout);
   }


   //run->fResults.Add(asym4);
   //if(writeRun) rman->WriteRun();
   //if(writeRun) gROOT->ProcessLine(".q");

   //delete events;
   delete traj;
   delete trig;
   delete dis;

   return(0);
}
