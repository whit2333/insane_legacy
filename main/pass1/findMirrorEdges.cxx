/** Finds the mirror edges of the Gas Cherenkov
*/
Int_t findMirrorEdges(Int_t runnumber=72999) {

   std::cout << " THIS SCRIPT NEEDS FIXED!!!!\n";

   TFile * f = new TFile(Form("data/rootfiles/FirstPass%d.root",runnumber),"UPDATE");
   if(!f) { printf("file not found. \n"); return(-1); }
   TH2F * h2 = (TH2F*)gROOT->FindObject("bigcal_clusters_4npec_2mir_goodtiming");
   if(!h2) {printf("histogram not found\n"); return(-1); }
   //h2->Draw();	
   TFitResultPtr r;
   //Double_t par1 = r->Parameter(1);
   // Double_t par2  = r->Parameter(2);
   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TString SQLCOMMAND(""); 

   TH2F * pairs[10];
   TH1D * mirrorEdge[10];
   Double_t roughVertLocations[8];
   Double_t roughHorizontalLocations[12];
   Double_t roughVertLocations2[6];

   roughVertLocations[0]=-110.0;
   roughVertLocations[1]=-80.0;

   roughVertLocations[2]=-45.0;
   roughVertLocations[3]=-20.0;

   roughVertLocations[4]= 20.0;
   roughVertLocations[5]= 45.0;

   roughVertLocations[6]= 80.0;
   roughVertLocations[7]= 110.0;

   TCanvas * c = new TCanvas("MirrorEdges","Mirror Edges",800,800);
   c->Divide(1,4);
   TF1 *f1 = new TF1("f1", "gaus", -10,10);
   c->cd(1)->Divide(4,1);
   c->cd(2)->Divide(4,1);


   for(int i = 0;i<4;i++) {
      pairs[i] = (TH2F*) h2->Clone(Form("pair%d",i));
      ((TH2F*)pairs[i])->GetYaxis()->SetRangeUser(roughVertLocations[i*2],roughVertLocations[i*2+1]);
      c->cd(1)->cd(i+1);
      h2->Draw();
      pairs[i]->Draw("same,colz");
      c->cd(2)->cd(i+1);
      mirrorEdge[i] = pairs[i]->ProjectionX(Form("mirrorpair%d",i));
      mirrorEdge[i]->Rebin();
      r = mirrorEdge[i]->Fit("f1","R,S,Q");
      if(r->IsValid()){
         SQLCOMMAND ="UPDATE OR REPLACE into cherenkov_mirror_edges set `run_number`=";
         SQLCOMMAND += runnumber;

         SQLCOMMAND +=",`edge_number`="; 
         SQLCOMMAND +=i;

         SQLCOMMAND +=",`edge_orientation`="; 
         SQLCOMMAND +=2;

         SQLCOMMAND +=",`mirror_1`="; 
         SQLCOMMAND +=1;

         SQLCOMMAND +=",`mirror_2`="; 
         SQLCOMMAND +=2;

         SQLCOMMAND +=",`location`="; 
         SQLCOMMAND +=r->Parameter(1);

         SQLCOMMAND +=",`width`="; 
         SQLCOMMAND +=r->Parameter(2);

         SQLCOMMAND +=",`chi_squared`="; 
         SQLCOMMAND +=r->Chi2();

         SQLCOMMAND +=" ;"; 

         // if(db) db->Query(SQLCOMMAND.Data());
      }
   }

   roughHorizontalLocations[0] = -60.0;
   roughHorizontalLocations[1] = -15.0;
   roughHorizontalLocations[2] = 15.0;
   roughHorizontalLocations[3] = 60.0;

   roughHorizontalLocations[4] = -60.0;
   roughHorizontalLocations[5] = -15.0;
   roughHorizontalLocations[6] = 15.0;
   roughHorizontalLocations[7] = 60.0;

   roughHorizontalLocations[8] = -60.0;
   roughHorizontalLocations[9] = -15.0;
   roughHorizontalLocations[10] = 15.0;
   roughHorizontalLocations[11] = 60.0;


   roughVertLocations2[0]=-80.0;

   roughVertLocations2[1]=-50.0;
   roughVertLocations2[2]=-20.0;

   roughVertLocations2[3]= 20.0;
   roughVertLocations2[4]= 50.0;

   roughVertLocations2[5]= 80.0;

   c->cd(3)->Divide(6,1);
   c->cd(4)->Divide(6,1);

   for(int i = 0;i<6;i++) {
      pairs[i+4] = (TH2F*) h2->Clone(Form("pair%d",i+4));
      ((TH2F*)pairs[i+4])->GetXaxis()->SetRangeUser(roughHorizontalLocations[i*2],roughHorizontalLocations[i*2+1]);
      ((TH2F*)pairs[i+4])->GetYaxis()->SetRangeUser(roughVertLocations2[(i/2)*2],roughVertLocations2[(i/2)*2+1]);
      c->cd(3)->cd(i+1);
      h2->Draw();
      pairs[i+4]->Draw("same,colz");
      c->cd(4)->cd(i+1);
      mirrorEdge[i+4] = pairs[i+4]->ProjectionY(Form("mirrorpair%d",i+4));
      mirrorEdge[i+4]->Rebin();
      r=mirrorEdge[i+4]->Fit("f1","S,Q","",roughVertLocations2[(i/2)*2],roughVertLocations2[(i/2)*2+1]);

      if(r->IsValid()) 
      { SQLCOMMAND ="REPLACE into cherenkov_mirror_edges set `run_number`=";
         SQLCOMMAND += runnumber;

         SQLCOMMAND +=",`edge_number`="; 
         SQLCOMMAND +=i+4;

         SQLCOMMAND +=",`edge_orientation`="; 
         SQLCOMMAND +=2;

         SQLCOMMAND +=",`mirror_1`="; 
         SQLCOMMAND +=1;

         SQLCOMMAND +=",`mirror_2`="; 
         SQLCOMMAND +=2;

         SQLCOMMAND +=",`location`="; 
         SQLCOMMAND +=r->Parameter(1);

         SQLCOMMAND +=",`width`="; 
         SQLCOMMAND +=r->Parameter(2);

         SQLCOMMAND +=",`chi_squared`="; 
         SQLCOMMAND +=r->Chi2();

         SQLCOMMAND +=" ;";  
         //if(db) db->Query(SQLCOMMAND.Data());
      }
   }
   c->SaveAs(Form("plots/%d/cherenkovMirrorEdges.pdf",runnumber));
   c->SaveAs(Form("plots/%d/cherenkovMirrorEdges.png",runnumber));
   c->SaveAs(Form("plots/%d/cherenkovMirrorEdges.ps",runnumber));


   return(0);
}

