/*!  Examines the cherenkov w.r.t. other detectors... trying to decrease background on TDC cut

 Attenuations from LED runs while ramping the magnet during perp:
  {1.02958, 0.976127, 0.553096, 0.863169, 0.982204, 0.831832, 0.604835, 0.800308}
 */
Int_t cherenkov8(Int_t runnumber=72795) {

   load_style("MultiSquarePlot");
   gROOT->SetStyle("MultiSquarePlot");

   const char * detectorTree = "betaDetectors1";
   //const char * detectorTree = "trackingPositions";
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
   if(!fClusterTree) {
      std::cout << "No Tree Found: " << detectorTree << "\n";
      return(-10);
   }
   if(!fClusterTree) {printf("Clusters tree not found eh\n"); return(-1); }

   gROOT->cd();
   TH1F * cer[8];

   /// Fitting functions and parameters
   Double_t xMin = 0.5;
   Double_t xMax = 4.0;
   TFitResultPtr fitResult;
   TF1 *f1 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+[3]*exp(-0.5*((x-2.0*[1])/[4])**2)+[5]*exp(-0.5*((x-[6])/[7])**2)", xMin,xMax);

   TF1 *f2 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+\
         [3]*exp(-0.5*((x-2.0*[1])/[4])**2)+\
         [5]*exp(-0.5*((x-3.0*[1])/[6])**2)", xMin,xMax);

   // 1 electron peak amplitude
   f1->SetParameter(0,100);
   f1->SetParLimits(0,0.10,9999999);
   // 1 electron peak mean
   f1->SetParameter(1,1.0);
   f1->SetParLimits(1,0.5,2.0);
   // 1 electron peak width
   f1->SetParameter(2,0.500);
   f1->SetParLimits(2,0,9999);
   // 2 electron peak amplitude
   f1->SetParameter(3,1);
   f1->SetParLimits(3,0,9999);
   // 2 electron peak width
   f1->SetParameter(4,2.000);
   f1->SetParLimits(4,0.400,5.000);
   // BG peak amplitude
   f1->SetParameter(5,100);
   f1->SetParLimits(5,0,99999);
   //BG peak mean
   f1->SetParameter(6,0.600);
   f1->SetParLimits(6,100,1000);
   // BG peak width
   f1->SetParameter(7,0.3);
   f1->SetParLimits(7,0,1000);

   // 1 electron peak amplitude
   f2->SetParameter(0,100);
   f2->SetParLimits(0,0.10,9999999);
   // 1 electron peak mean
   f2->SetParameter(1,1.0);
   f2->SetParLimits(1,0.5,2.0);
   // 1 electron peak width
   f2->SetParameter(2,0.500);
   f2->SetParLimits(2,0,9999);
   // 2 electron peak amplitude
   f2->SetParameter(3,1);
   f2->SetParLimits(3,0,9999);
   // 2 electron peak width
   f2->SetParameter(4,2.000);
   f2->SetParLimits(4,0.400,5.000);
   // 3 peak amplitude
   f2->SetParameter(5,1);
   f2->SetParLimits(5,0,99999);
   // 3 peak width
   f2->SetParameter(6,0.7);
   f2->SetParLimits(6,0,3000);

   TF1 *e1 = new TF1("e1","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e2 = new TF1("e2","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e3 = new TF1("e3","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e4 = new TF1("e4","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   e1->SetLineColor(kBlack);
   e1->SetLineStyle(2);

   e2->SetLineColor(kBlack);
   e2->SetLineStyle(3);

   e3->SetLineColor(kBlack);
   e3->SetLineStyle(2);

   e4->SetLineColor(2000);
   e4->SetLineStyle(2);

   TCanvas * c = new TCanvas("cherenkov8","Gas Cherenkov script 8 ");
   //c->Divide(1,2);
   c->Divide(1,2,0.00001,0.00001);
   TH1F * h1;

   ///___________________________________________________________________
   /// ADCs
   ///___________________________________________________________________
   ///
   c->cd(1)->SetBottomMargin(0.00001);
   gPad->SetLogy(true);

   fClusterTree->Draw(
         Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerEven(100,0,4.0)"),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==0&& \
            TMath::Abs(betaDetectors1.bigcalClusters.fCherenkovTDC)<17 \
            && bigcalClusters.fIsGood \
            && fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0 \
            && betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>800"),"goff");
   //            &&fClosestLuciteMissDistance<40&&fNLucitePositions>0
            //&& fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0 

   h1 = (TH1F *) gROOT->FindObject(Form("cerEven"));
   h1->SetLineColor(1);
   f1->SetLineColor(1);
   h1->Draw("E1");
   fitResult = h1->Fit(f1,"S,R","same");

   // 
   e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
   e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
   e3->SetParameters(fitResult->Parameter(5),fitResult->Parameter(6),fitResult->Parameter(7));
   /*   e4->SetParameters(fitResult->Parameter(5),3.0*fitResult->Parameter(1),fitResult->Parameter(6));*/
   e1->DrawCopy("same");
   e2->DrawCopy("same");
   //e3->DrawCopy("same");
   /*   e4->DrawCopy("same");*/
   /*   h1->SetMarkerStyle(20);*/
   h1->SetTitle("");
   h1->Draw("same");


   //-------------------------------------------------
   c->cd(2)->SetTopMargin(0.00001);
   gPad->SetLogy(true);
   fClusterTree->Draw(
         Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerOdd(100,0,4.0)"),
         Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==1&& \
            TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<17 \
            && bigcalClusters.fIsGood \
            && fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0 \
            && betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>800 "),"goff");
   //            &&fClosestLuciteMissDistance<40&&fNLucitePositions>0

   h1 = (TH1F *) gROOT->FindObject(Form("cerOdd"));
   h1->SetLineColor(1);
   h1->Draw("E1");
   h1->GetXaxis()->SetTitle("ADC");
   h1->GetXaxis()->CenterTitle(true);
   fitResult = h1->Fit(f1,"S,R","same");
   e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
   e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
   e3->SetParameters(fitResult->Parameter(5),fitResult->Parameter(6),fitResult->Parameter(7));
   /*   e4->SetParameters(fitResult->Parameter(5),3.0*fitResult->Parameter(1),fitResult->Parameter(6));*/
   e1->DrawCopy("same");
   e2->DrawCopy("same");
   //e3->DrawCopy("same");
   /*   e4->DrawCopy("same");*/
   /*   h1->SetMarkerStyle(20);*/
   h1->SetTitle("");
   h1->Draw("same");

   //fClusterTree->Draw(
   //      Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerOdd2(100,0,5.0)"),
   //      Form("betaDetectors1.bigcalClusters.fPrimaryMirror==1&& \
   //         TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<10 \
   //         && bigcalClusters.fIsGood \
   //         &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>1100 "),"goff");

   //h1 = (TH1F *) gROOT->FindObject(Form("cerOdd2"));
   //h1->SetLineColor(2);
   //h1->Draw("same");

   //fClusterTree->Draw(
   //      Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerOdd3(100,0,5.0)"),
   //      Form("betaDetectors1.bigcalClusters.fPrimaryMirror==3&& \
   //         TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<10 \
   //         && bigcalClusters.fIsGood \
   //         &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>1100 "),"goff");

   //h1 = (TH1F *) gROOT->FindObject(Form("cerOdd3"));
   //h1->SetLineColor(4);
   //h1->Draw("same");

   //fClusterTree->Draw(
   //      Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerOdd5(100,0,5.0)"),
   //      Form("betaDetectors1.bigcalClusters.fPrimaryMirror==5&& \
   //         TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<10 \
   //         && bigcalClusters.fIsGood \
   //         &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>1100 "),"goff");

   //h1 = (TH1F *) gROOT->FindObject(Form("cerOdd5"));
   //h1->SetLineColor(6);
   //h1->Draw("same");

   //fClusterTree->Draw(
   //      Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cerOdd7(100,0,5.0)"),
   //      Form("betaDetectors1.bigcalClusters.fPrimaryMirror==7&& \
   //         TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<10 \
   //         && bigcalClusters.fIsGood \
   //         &&betaDetectors1.bigcalClusters.fTotalE<5000&&betaDetectors1.bigcalClusters.fTotalE>1100 "),"goff");

   //h1 = (TH1F *) gROOT->FindObject(Form("cerOdd7"));
   //h1->SetLineColor(7);
   //h1->Draw("same");

   c->SaveAs(Form("plots/%d/Cherenkov8_log.png",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov8_log.pdf",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov8_log.eps",runnumber));
   c->SaveAs(Form("plots/%d/Cherenkov8_log.tex",runnumber));

   //c->SaveAs(Form("plots/%d/Cherenkov8.png",runnumber));
   //c->SaveAs(Form("plots/%d/Cherenkov8.pdf",runnumber));
   //c->SaveAs(Form("plots/%d/Cherenkov8.eps",runnumber));
   //c->SaveAs(Form("plots/%d/Cherenkov8.tex",runnumber));

}
