Int_t RunAppend(Int_t runnumber=0) {
  InSANERun * aRun = 0;

  const char * saneFileName = "data/results/sane.root";
  TFile * fout = new TFile(saneFileName,"UPDATE");
  fout->cd();
  TTree * tout = (TTree*)gROOT->FindObject("saneRuns");
  if(!tout) {
    tout = new TTree("saneRuns", "Sane Runs");
    tout->Branch("runs","InSANERun",&aRun);
  } else {
    tout->SetBranchAddress("runs",&aRun);
  }
  aRun = rman->GetCurrentRun();
  tout->Fill();

  tout->Write();
  fout->Write();
  fout->Close();
  
rman->GetCurrentFile()->cd();

  return(0);
}