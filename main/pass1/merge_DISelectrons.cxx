Int_t merge_DISelectrons(Int_t runGroup = 2){

   /// Build up the queue 
   aman->BuildQueue(Form("lists/para/run_series_%d.txt",runGroup));

   /// Get the chains for run queue
   TChain * electronsChain =  aman->BuildChain("DISElectrons");
//TChain *  =  aman->BuildChain("3clusterPi0results");

   /// Merge chain into single tree
   TFile * f = new TFile(Form("data/DISelectrons%d.root",runGroup),"RECREATE");
   f->cd();
   electronsChain->Merge(f,32000,"keep");
   std::cout << " electronsChain Merge Complete. \n";

   f->Flush();

   delete electronsChain;

   new TBrowser;

   return(0);
}
