/*!  Examines the cherenkov w.r.t. other detectors... trying to decrease background on TDC cut

 Attenuations from LED runs while ramping the magnet during perp:
  {1.02958, 0.976127, 0.553096, 0.863169, 0.982204, 0.831832, 0.604835, 0.800308}
 */
Int_t lucite2(Int_t runnumber=72994) {

   const char * detectorTree = "trackingPositions";
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
      if(!fClusterTree) {
      std::cout << "No Tree Found: " << detectorTree << "\n";
   }
   if(!fClusterTree) {printf("Clusters tree not found eh\n"); return(-1); }

   gROOT->cd();
   TH1F * cer[8];

   /// Fitting functions and parameters
   Double_t xMin = 0.3;
   Double_t xMax = 5.0;
   TFitResultPtr fitResult;
   TF1 *f1 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+\
                                     [3]*exp(-0.5*((x-2.0*[1])/[4])**2)+\
                                     [5]*exp(-0.5*((x-[6])/[7])**2)", xMin,xMax);
   TF1 *f3 = new TF1("cherenkovADC3","[0]*exp(-0.5*((x-[1])/[2])**2)+\
                                     [3]*exp(-0.5*((x-2.0*[1])/[4])**2)", xMin,xMax);

   TF1 *f2 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+\
                                     [3]*exp(-0.5*((x-2.0*[1])/[4])**2)+\
                                     [5]*exp(-0.5*((x-3.0*[1])/[6])**2)", xMin,xMax);

// 1 electron peak amplitude
  f1->SetParameter(0,100);
  f1->SetParLimits(0,0.10,9999999);
// 1 electron peak mean
  f1->SetParameter(1,1.0);
  f1->SetParLimits(1,0.5,2.0);
// 1 electron peak width
  f1->SetParameter(2,0.500);
  f1->SetParLimits(2,0,9999);
// 2 electron peak amplitude
  f1->SetParameter(3,1);
  f1->SetParLimits(3,0,9999);
// 2 electron peak width
  f1->SetParameter(4,2.000);
  f1->SetParLimits(4,0.400,5.000);
// BG peak amplitude
  f1->SetParameter(5,100);
  f1->SetParLimits(5,0,99999);
//BG peak mean
  f1->SetParameter(6,0.600);
  f1->SetParLimits(6,100,1000);
// BG peak width
  f1->SetParameter(7,0.3);
  f1->SetParLimits(7,0,1000);

// 1 electron peak amplitude
  f3->SetParameter(0,100);
  f3->SetParLimits(0,0.10,9999999);
// 1 electron peak mean
  f3->SetParameter(1,1.0);
  f3->SetParLimits(1,0.5,2.0);
// 1 electron peak width
  f3->SetParameter(2,0.500);
  f3->SetParLimits(2,0,9999);
// 2 electron peak amplitude
  f3->SetParameter(3,1);
  f3->SetParLimits(3,0,9999);
// 2 electron peak width
  f3->SetParameter(4,2.000);
  f3->SetParLimits(4,0.400,5.000);


// 1 electron peak amplitude
  f2->SetParameter(0,100);
  f2->SetParLimits(0,0.10,9999999);
// 1 electron peak mean
  f2->SetParameter(1,1.0);
  f2->SetParLimits(1,0.5,2.0);
// 1 electron peak width
  f2->SetParameter(2,0.500);
  f2->SetParLimits(2,0,9999);
// 2 electron peak amplitude
  f2->SetParameter(3,1);
  f2->SetParLimits(3,0,9999);
// 2 electron peak width
  f2->SetParameter(4,2.000);
  f2->SetParLimits(4,0.400,5.000);
// 3 peak amplitude
  f2->SetParameter(5,1);
  f2->SetParLimits(5,0,99999);
// 3 peak width
  f2->SetParameter(6,0.7);
  f2->SetParLimits(6,0,3000);

   TF1 *e1 = new TF1("e1","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e2 = new TF1("e2","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e3 = new TF1("e3","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e4 = new TF1("e4","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   e1->SetLineColor(kBlue);
   e2->SetLineColor(kGreen);
   e3->SetLineColor(kBlack);
   e3->SetLineStyle(2);
   e4->SetLineColor(2000);
   e4->SetLineStyle(2);

   TCanvas * c = new TCanvas("cherenkov12","Gas Cherenkov script 12");
   c->Divide(2,2);
   TH1F * h1;

   ///___________________________________________________________________
   /// ADCs
   ///___________________________________________________________________
   ///
   c->cd(1);

   fClusterTree->Draw(
      Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cer1(100,0,5.0)"),
      Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==0&& \
            TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<20&& \
            fNLucitePositions>0 && betaDetectors1.bigcalClusters.fIsGood &&\
            betaDetectors1.bigcalClusters.fTotalE<1000&&\
            betaDetectors1.bigcalClusters.fTotalE>600 "),"goff");
   h1 = (TH1F *) gROOT->FindObject(Form("cer1"));
   h1->SetLineColor(1);
   h1->GetXaxis()->SetTitle("e^{#pm} Count");
   h1->Draw();
   fitResult = h1->Fit(f2,"S,R","same");
   e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
   e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
/*   e3->SetParameters(fitResult->Parameter(5),fitResult->Parameter(6),fitResult->Parameter(7));*/
   e4->SetParameters(fitResult->Parameter(5),3.0*fitResult->Parameter(1),fitResult->Parameter(6));
   e1->DrawCopy("same");
   e2->DrawCopy("same");
/*   e3->DrawCopy("same");*/
   e4->DrawCopy("same");
/*   h1->SetMarkerStyle(20);*/
   h1->SetTitle("Toroidal Mirrors");
   h1->Draw("same");

   c->cd(2);
fClusterTree->StartViewer();
//    fClusterTree->Draw(
//       Form("betaDetectors1.bigcalClusters.fCherenkovBestADCSum>>cer1(100,0,5.0)"),
//       Form("betaDetectors1.bigcalClusters.fPrimaryMirror%2==0&& \
//             TMath::Abs( betaDetectors1.bigcalClusters.fCherenkovTDC)<20&& \
//             fNLucitePositions>0 && betaDetectors1.bigcalClusters.fIsGood &&\
//             betaDetectors1.bigcalClusters.fTotalE<1000&&\
//             betaDetectors1.bigcalClusters.fTotalE>600 "),"goff");


   c->SaveAs(Form("plots/%d/lucite2.png",runnumber));
   c->SaveAs(Form("plots/%d/lucite2.pdf",runnumber));
   c->SaveAs(Form("plots/%d/lucite2.svg",runnumber));

}
