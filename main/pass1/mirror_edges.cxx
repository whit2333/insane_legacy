/** Finds the mirror edges of the Gas Cherenkov
*/
Int_t mirror_edges(Int_t runnumber=72913) {

   rman->SetRun(runnumber);
   //TFile * f = new TFile(Form("data/rootfiles/FirstPass%d.root",runnumber),"UPDATE");
   //if(!f) { printf("file not found. \n"); return(-1); }
   //TH2F * h2 = (TH2F*)gROOT->FindObject("bigcal_clusters_4npec_2mir_goodtiming");
   //if(!h2) {printf("histogram not found\n"); return(-1); }

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);

   TCanvas  * c = new TCanvas("mirror_edges","mirror_edges",600,800);

   events->fTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>hMirrorEdges(30,-60,60,60,-120,120)",
                       "bigcalClusters.fNumberOfGoodMirrors==2&&bigcalClusters.fIsGood","colz");
   TH2F * h2 = (TH2F*)gROOT->FindObject("hMirrorEdges");
   if(h2){
      h2->SetTitle("Cherenkov Mirror Edges");
      h2->GetXaxis()->SetTitle("x (cm)");
      h2->GetYaxis()->SetTitle("y (cm)");
      h2->GetXaxis()->CenterTitle(true);
      h2->GetYaxis()->CenterTitle(true);
      h2->SetFillColor(1);
      h2->Draw("box");
   }

   c->SaveAs(Form("plots/%d/mirror_edges.pdf",runnumber));
   c->SaveAs(Form("plots/%d/mirror_edges.png",runnumber));
   c->SaveAs(Form("plots/%d/mirror_edges.ps",runnumber));


   return(0);
}

