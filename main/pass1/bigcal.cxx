Int_t bigcal(Int_t runnumber=72949,Int_t isACalibration=0) {

   const char * detectorTree = "betaDetectors1";

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
   if(!fClusterTree) {
      std::cout << "x- Error " << detectorTree <<  " Tree NOT Found \n";
   }
   if(!fClusterTree) {printf("Tree not found eh\n"); return(-1); }

   TH1 * h1 = 0;
   TH2 * h2 = 0;
   TFile * f = new TFile(Form("data/rootfiles/FirstPass%d.root",runnumber),"UPDATE");
   TCanvas * c = new TCanvas("Cherenkov","Gas Cherenkov",500,800);

   // -------------------------------------------------------------------
   fClusterTree->Draw("bigcalClusters.fTotalE>>bcE0(100,0,3000)",
                     "bigcalClusters.fCherenkovBestADCSum>0.3");
   h1 = (TH1 *) gROOT->FindObject("bcE0");
   h1->SetTitle("BigCal energy");
   c->SaveAs(Form("plots/%d/bigcal_1.png",runnumber));

   // -------------------------------------------------------------------
   fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fTotalE>>bcEvsY0(100,0,3000,120,-120,120)",
                     "bigcalClusters.fCherenkovBestADCSum>0.5","goff");
   h2 = (TH2 *) gROOT->FindObject("bcEvsY0");
   h2->SetTitle("BigCal energy");
   h2->Draw("colz");
   c->SaveAs(Form("plots/%d/bigcal_2.png",runnumber));

   // -------------------------------------------------------------------
   fClusterTree->Draw("bigcalClusters.fXMoment:bigcalClusters.fTotalE>>bcEvsX0(100,0,3000,55,-55,55)",
                     "bigcalClusters.fCherenkovBestADCSum>0.5","goff");
   h2 = (TH2 *) gROOT->FindObject("bcEvsX0");
   h2->SetTitle("BigCal energy");
   h2->Draw("colz");
   c->SaveAs(Form("plots/%d/bigcal_3.png",runnumber));

   // -------------------------------------------------------------------
   fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bcYvsX0(55,-55,55,100,-100,100)",
                     "bigcalClusters.fCherenkovBestADCSum>0.5&&bigcalClusters.fTotalE>1100","goff");
   h2 = (TH2 *) gROOT->FindObject("bcYvsX0");
   h2->SetTitle("E>1100");
   h2->Draw("colz");
   c->SaveAs(Form("plots/%d/bigcal_4.png",runnumber));

   return 0;

   /// Cherenkov number of Photoelectrons when only one mirror is required 
   /// geometrically to have the entire cone focused on 1 PMT.
   /// And at least 1 good TDC hit
   fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>cherenkov_npe_1mir_goodhit(200,1,40)",
                     "bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fGoodCherenkovTDCHit==1&&bigcalClusters.fTotalE>1000");
   TH1 * cherenkov_npe_1mir_goodhit = (TH1 *) gROOT->FindObject("cherenkov_npe_1mir_goodhit");
   cherenkov_npe_1mir_goodhit->SetTitle("Single Mirror Clusters with good hit");
   c->SaveAs(Form("plots/%d/cherenkov_npe_1mir_goodhit.jpg",runnumber));

   /// Cherenkov number of Photoelectrons when only one mirror is required 
   /// geometrically to have the entire cone focused on 1 PMT.
   /// with good timing (meaning all cherenkov hits have a tdc hit)
   fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>cherenkov_npe_1mir_goodtiming(200,1,40)",
                     "bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fGoodCherenkovTiming==1&&bigcalClusters.fTotalE>1000");
   TH1 * cherenkov_npe_1mir_goodtiming = (TH1 *) gROOT->FindObject("cherenkov_npe_1mir_goodtiming");
   cherenkov_npe_1mir_goodtiming->SetTitle("Single Mirror Clusters with good timing");
   c->SaveAs(Form("plots/%d/cherenkov_npe_1mir_goodtiming.jpg",runnumber));
   TFitResultPtr r = cherenkov_npe_1mir_goodtiming->Fit("gaus","S,E,M","",5,28);
// if(r){
// Double_t par1 = r->Parameter(1);
// Double_t par2  = r->Parameter(2);
// TSQLServer * db = SANEDatabaseManager::GetManager()->dbServer;
// TString SQLCOMMAND("REPLACE into cherenkov_performance set "); 
// SQLCOMMAND +="`run_number`=";
// SQLCOMMAND += runnumber;
// SQLCOMMAND +=",`npe_1_mean`="; 
// SQLCOMMAND += Form("%f",par1);
// SQLCOMMAND +=",`npe_1_width`="; 
// SQLCOMMAND += Form("%f",par2);
// SQLCOMMAND +=",`channel`=-1 ; "; 
// if(db) db->Query(SQLCOMMAND.Data());
// }
   c->SaveAs(Form("plots/%d/cherenkov_npe_1mir_goodtimingfit.jpg",runnumber));

   /// Cherenkov number of Photoelectrons when the cone is split into 2 mirrors.
   fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>cherenkov_npe_2mir(200,1,40)",
                      "bigcalClusters.fNumberOfMirrors==2&&bigcalClusters.fTotalE>1000");
   TH1 * cherenkov_npe_2mir = (TH1 *) gROOT->FindObject("cherenkov_npe_2mir");
   cherenkov_npe_2mir->SetTitle("2 Mirror Clusters");
   c->SaveAs(Form("plots/%d/cherenkov_npe_2mir.jpg",runnumber));
  
   /// Cherenkov number of Photoelectrons when the cone is split into 2 mirrors.
   /// And at least 1 good TDC hit
   fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>cherenkov_npe_2mir_goodhit(200,1,40)",
                      "bigcalClusters.fNumberOfMirrors==2&&bigcalClusters.fTotalE>1000&&bigcalClusters.fGoodCherenkovTiming==1");
   TH1 * cherenkov_npe_2mir_goodhit = (TH1 *) gROOT->FindObject("cherenkov_npe_2mir_goodhit");
   cherenkov_npe_2mir_goodhit->SetTitle("2 Mirror Clusters with one good tdc hit");
   c->SaveAs(Form("plots/%d/cherenkov_npe_2mir_goodhit.jpg",runnumber));


   /// Cherenkov number of Photoelectrons when the cone is split into 2 mirrors.
   /// with good timing (meaning all cherenkov hits have a tdc hit)
   fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>cherenkov_npe_2mir_goodtiming(200,1,40)",
                      "bigcalClusters.fNumberOfMirrors==2&&bigcalClusters.fTotalE>1000&&bigcalClusters.fGoodCherenkovTiming==1");
   TH1 * cherenkov_npe_2mir_goodtiming = (TH1 *) gROOT->FindObject("cherenkov_npe_2mir_goodtiming");
   cherenkov_npe_2mir_goodtiming->SetTitle("2 Mirror Clusters with good timing");
   c->SaveAs(Form("plots/%d/cherenkov_npe_2mir_goodtiming.jpg",runnumber));

///////////////////////////////////////////////////////

// Cherenkov number of Photoelectrons when the cone is split into 4 mirrors.
  fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>cherenkov_npe_4mir(200,1,40)",
                     "bigcalClusters.fNumberOfMirrors==4&&bigcalClusters.fTotalE>1000","colz");
  TH1 * cherenkov_npe_4mir = (TH1 *) gROOT->FindObject("cherenkov_npe_4mir");
  cherenkov_npe_4mir->SetTitle("4 Mirror Clusters");
  c->SaveAs(Form("plots/%d/cherenkov_npe_4mir.jpg",runnumber));

// Cherenkov number of Photoelectrons when the cone is split into 4 mirrors.
// with good timing (meaning all cherenkov hits have a tdc hit)
  fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>cherenkov_npe_4mir_goodtiming(200,1,40)",
                     "bigcalClusters.fNumberOfMirrors==4&&bigcalClusters.fTotalE>1000&&bigcalClusters.fGoodCherenkovTiming==1","colz");
  TH1 * cherenkov_npe_4mir_goodtiming = (TH1 *) gROOT->FindObject("cherenkov_npe_4mir_goodtiming");
  cherenkov_npe_4mir_goodtiming->SetTitle("4 Mirror Clusters with good timing");
  c->SaveAs(Form("plots/%d/cherenkov_npe_4mir_goodtiming.jpg",runnumber));

// Cherenkov number of Photoelectrons when the cone is split into 4 mirrors.
// with good timing (meaning all cherenkov hits have a tdc hit)
  fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>cherenkov_npe_4mir_goodhit(200,1,40)",
                     "bigcalClusters.fNumberOfMirrors==4&&bigcalClusters.fTotalE>1000&&bigcalClusters.fGoodCherenkovTDCHit==1","colz");
  TH1 * cherenkov_npe_4mir_goodhit = (TH1 *) gROOT->FindObject("cherenkov_npe_4mir_goodhit");
  cherenkov_npe_4mir_goodhit->SetTitle("4 Mirror Clusters with a good hit");
  c->SaveAs(Form("plots/%d/cherenkov_npe_4mir_goodhit.jpg",runnumber));


////////////// BIGCAL CLUSTERS //////////////////////////

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec(130,-65,65,120,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4&&bigcalClusters.fTotalE>1000","colz");
  TH2 * bigcal_clusters_4npec = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec");
  bigcal_clusters_4npec->SetTitle("Single Mirror Clusters with 4 or more PE");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
//  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec.jpg",runnumber));

///////////////////////////////////////////////////////////////////////////////////

// Bigcal cluster positions given that there is a 8 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 1 mirror.
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_8npec_1mir(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>8&& bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000","colz");
  TH2 * bigcal_clusters_8npec_1mir = (TH2 *) gROOT->FindObject("bigcal_clusters_8npec_1mir");
  bigcal_clusters_8npec_1mir->SetTitle("Single Mirror Clusters with 4 with 8 or more PE");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
 // c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_1mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_8npec_1mir.jpg",runnumber));

// Bigcal cluster positions given that there is a 8 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 2 mirrors.
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_8npec_2mir(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>8&& bigcalClusters.fNumberOfMirrors==2&&bigcalClusters.fTotalE>1000","colz");
  TH2 * bigcal_clusters_8npec_2mir = (TH2 *) gROOT->FindObject("bigcal_clusters_8npec_2mir");
  bigcal_clusters_8npec_2mir->SetTitle("2 Mirror Clusters with 8 or more PE");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
 // c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_1mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_8npec_2mir.jpg",runnumber));

////////////////////////////////////////////////////

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 1 mirror.
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_1mir(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4&& bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000","colz");
  TH2 * bigcal_clusters_4npec_1mir = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_1mir");
  bigcal_clusters_4npec_1mir->SetTitle("Single Mirror Clusters with 4 or more PE");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
 // c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_1mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_1mir.jpg",runnumber));
  
// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 2 mirrors.
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_2mir(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4 && bigcalClusters.fNumberOfMirrors==2&&bigcalClusters.fTotalE>1000","colz");
  TH2 * bigcal_clusters_4npec_2mir = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_2mir");
  bigcal_clusters_4npec_2mir->SetTitle("2 Mirror Clusters with 4 or more PE");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
//  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_2mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_2mir.jpg",runnumber));

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 4 mirror2.
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_4mir(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4 && bigcalClusters.fNumberOfMirrors==4&&bigcalClusters.fTotalE>1000","colz");
  TH2 * bigcal_clusters_4npec_4mir = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_4mir");
  bigcal_clusters_4npec_4mir->SetTitle("4 Mirror Clusters with 4 or more PE");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
 // c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_4mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_4mir.jpg",runnumber));

///////////////////////////////////////////////////////////////

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 1 mirror.
// with good timing (meaning all cherenkov hits have a tdc hit)
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_1mir_goodtiming(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4&& bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fTotalE>1000&&bigcalClusters.fGoodCherenkovTiming==1","colz");
  TH2 * bigcal_clusters_4npec_1mir_goodtiming = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_1mir_goodtiming");
  bigcal_clusters_4npec_1mir_goodtiming->SetTitle("Single Mirror Clusters with 4 or more PE with good timing");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
 // c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_1mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_1mir_goodtiming.jpg",runnumber));
  
// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 2 mirror2.
// with good timing (meaning all cherenkov hits have a tdc hit)
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_2mir_goodtiming(100,-65,65,200,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>3 && bigcalClusters.fNumberOfGoodMirrors>=2&&bigcalClusters.fTotalE>900","colz");//&&bigcalClusters.fGoodCherenkovTiming==1
  TH2 * bigcal_clusters_4npec_2mir_goodtiming = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_2mir_goodtiming");
  bigcal_clusters_4npec_2mir_goodtiming->SetTitle("2 Mirror Clusters with 4 or more PE with good timing");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
//  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_2mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_2mir_goodtiming.jpg",runnumber));

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 4 mirror2.
// with good timing (meaning all cherenkov hits have a tdc hit)
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_4mir_goodtiming(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4 && bigcalClusters.fNumberOfMirrors==4&&bigcalClusters.fTotalE>1000&&bigcalClusters.fGoodCherenkovTiming==1","colz");
  TH2 * bigcal_clusters_4npec_4mir_goodtiming = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_4mir_goodtiming");
  bigcal_clusters_4npec_4mir_goodtiming->SetTitle("4 Mirror Clusters with 4 or more PE with good timing");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
 // c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_4mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_4mir_goodtiming.jpg",runnumber));

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 2 mirrors.
// with at least 1 good tdc hit
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_2mir_goodhit(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4 && bigcalClusters.fNumberOfMirrors==2&&bigcalClusters.fTotalE>1000&&bigcalClusters.fGoodCherenkovTDCHit==1","colz");
  TH2 * bigcal_clusters_4npec_2mir_goodhit = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_2mir_goodhit");
  bigcal_clusters_4npec_2mir_goodhit->SetTitle("2 Mirror Clusters with 4 or more PE with 1 good tdc hit");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
//  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_2mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_2mir_goodhit.jpg",runnumber));

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the entire Cherenkov cone falls on 4 mirrors.
// with at least 1 good tdc hit
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_4mir_goodhit(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4 && bigcalClusters.fNumberOfMirrors==4&&bigcalClusters.fTotalE>1000&&bigcalClusters.fGoodCherenkovTDCHit==1","colz");
  TH2 * bigcal_clusters_4npec_4mir_goodhit = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_4mir_goodhit");
  bigcal_clusters_4npec_4mir_goodhit->SetTitle("4 Mirror Clusters with 4 or more PE with 1 good tdc hit");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
 // c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_4mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_4mir_goodhit.jpg",runnumber));

// Bigcal cluster positions given that there is a 4 photo electron cut on the cluster AND that the cone is split...
// with at least 1 good tdc hit
  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>bigcal_clusters_4npec_2to4mir_2goodhit(200,-65,65,300,-120,120)",
                     "bigcalClusters.fCherenkovBestNPESum>4&& bigcalClusters.fNumberOfMirrors>=2&&bigcalClusters.fTotalE>1000&&bigcalClusters.fNumberOfGoodMirrors>=2","colz");
  TH2 * bigcal_clusters_4npec_2to4mir_2goodhit = (TH2 *) gROOT->FindObject("bigcal_clusters_4npec_2to4mir_2goodhit");
  bigcal_clusters_4npec_2to4mir_2goodhit->SetTitle("Split Cherenkov Cone Clusters with 4 or more PE, gt2 good tdc hit");
  //bc_1_clust->Fit("gaus","E,M","",5,28);
 // c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_4mir.ps",runnumber));
  c->SaveAs(Form("plots/%d/bigcal_clusters_4npec_2to4mir_2goodhit.jpg",runnumber));





f->Flush();
f->Write();
f->Close();
return(0);
}
