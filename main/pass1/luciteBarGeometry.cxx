/** Lucite Bar Geometry w.r.t. BigCal
 *
 *  - Creates new Calibration object 
 *  - Draw lucite positions X(diff) vs Y(bar).
 *  - Draw the Luicte X position vs the Cluster X moment
 *  - Writes to file the new calibration
 *
 * Should be called from a MakePlots() for a calculation.
 * \todo clean up calculation -> MakePlot() Analysis connection
 *
 * This script gets the calibrated positions for the hodoscope
 * from cluster information. 
 * 
 */
Int_t luciteBarGeometry(Int_t runnumber = 72994,Int_t nevents=500000) {

   const char * detectorTree = "betaDetectors1";

   bool fast = false;

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

   TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   if( !t ) {
      printf("Tree:%s \n    was NOT FOUND. Aborting... \n",detectorTree); 
      return(-1);
   }

   TCut cTrigger = "triggerEvent.fCodaType==5&&triggerEvent.IsNeutralPionEvent()==0&&bigcalClusters.fIsGood";
   TCut cPi0Trigger = "triggerEvent.IsNeutralPionEvent()==1"
   TCut cLuciteBar = "fLuciteHodoscopeEvent.fLucitePositionHits.fPositionVector.fY==1";
   TCut cCherenkovTDCHit = "bigcalClusters.fGoodCherenkovTDCHit==1";

   rman->GetCurrentFile()->cd("calibrations/hodoscope");

//    TDirectory * dir = rman->GetCurrentFile()->GetDirectory("calibrations/hodoscope");
//    if(dir) {
//       dir->Delete("hbcluc*");
//    }

   InSANEDetectorCalibration * lucitePositionCalibrations =  
      new InSANEDetectorCalibration( Form("lucPosCal%d",runnumber),
                                     "Lucite Hodoscope-Bigcal Pos Calibration" );

   TF1 *f1;
   TH2F * h2;
   TH1F * h1;
   TProfile * p1;
   THStack * hsLucDiffHists = new THStack("hsLucDiffHists","hsLucDiffHists");
   THStack * hsLucDiffHistsRealign = new THStack("hsLucDiffHistsRealign","hsLucDiffHistsRealign");
   THStack * hsLucDiffProfilefit = new THStack("hsLucDiffHistsRealign","hsLucDiffHistsRealign");

   TCanvas * c = new TCanvas("hodoscopePositionCal","Hodoscope Pos Calibration",800,800);
   c->Divide(3,3);

   /// First Draws lucite positions X vs Y.
   c->cd(1);
   t->Draw("fLuciteHodoscopeEvent.fLucitePositionHits.fBarNumber:fLuciteHodoscopeEvent.fLucitePositionHits.fTDCDifference>>hluc(100,-100,300,56,1,29)",
      cTrigger ,
      "colz",nevents);


   /// Second Draws the Luicte X position vs the Cluster X moment 
   c->cd(2);
   t->Draw("bigcalClusters.fXMoment:fLuciteHodoscopeEvent.fLucitePositionHits.fTDCDifference>>pos(100,-100,300, 100,-60,60)",
      cTrigger,
      "colz,goff",nevents);
   h2  = (TH2F*)gROOT->FindObject("pos");
   h2->SetMaximum(100);
   h2->Draw("colz");
 
   /// Third For each bar, draw in two separate histograms, Lucite Y position   
   /// Get the location of peak. It is usually on the left side and falls for gits within a few 
   /// hundred ns (units? ask hovannes or go back to analyzer )
   /// Subtract the peak from the difference and draw ... 
   /// \todo Need to look at run-averaged peak locations. Do they vary much? no exept prior to ~72500

   InSANECalibration * aCalib = 0;
   TString options = "";
   TParameter<double> * aPar = 0;
   TSpectrum * spectrum = 0;

   for(int i = 0;i<28;i++){

      aCalib = new InSANECalibration(Form("luc-BigCal-Bar%d-Position",i+1),
                                     Form("Lucite-Bigcal Position Cal - %d",i+1) );
      aCalib->SetChannel(i+1);

      /// Add this bar's calibration to the list
      lucitePositionCalibrations->AddCalibration(aCalib);

      /// create the fitting function saved in the calibration
      f1 = aCalib->GetFunction();
      f1->SetRange(0,150);
//new TF1(Form("fLucPosCalib%d",i+1), "[0] +[1]*x ", 0,100);
//      lucitePositionCalibration->fCalibrations->Add(f1);

      TCut cLuciteBar =
         Form("fLuciteHodoscopeEvent.fLucitePositionHits.fBarNumber==%d",i+1);

      /// draw tdc difference (x-position) 
      c->cd(3);
      if(i!=0) options="same";
      else options="";

      t->Draw(
         Form("fLuciteHodoscopeEvent.fLucitePositionHits.fTDCDifference>>hluc%d(100,-100,300)",i+1),
         cTrigger && cLuciteBar ,"goff",
         nevents);
      h1  = (TH1F*)gROOT->FindObject(Form("hluc%d",i+1));
      h1->SetLineColor(i+1);
      //h1->SetFillColor(i+1);
      //h1->SetFillStyle(1001);
      
         hsLucDiffHists->Add(h1);
      if(!fast){
         hsLucDiffHists->Draw("nostack");
      }

      /// Search for the left most peak (sometimes there is a larg peak near the "middle"
      /// I suppose this is due to inefficient optical transport from the far side
      /// Searchin for max two peaks of which the second must be at least half the 
      /// size of the first. 
      spectrum = new TSpectrum(2); 
      Int_t Npeaks = spectrum->Search(h1,/* Double_t sigma =*/ 3,"nobackground", /*threshold =*/ 0.5);
      Float_t *xpos;
      Float_t *ypos;
      xpos = spectrum->GetPositionX();
      ypos = spectrum->GetPositionY();
      Double_t bestLocation = xpos[0];
      if(Npeaks>1) if(xpos[0]>xpos[1]) bestLocation=xpos[1] ;

      aPar = new TParameter<double>("lucDiffPeakLocation",bestLocation);
      aCalib->AddParameter(aPar);

      //      lucitePositionCalibration->fConstants->push_back( h1->GetBinCenter(h1->GetMaximumBin()));

//       std::cout << "Lucite Bar, " << i+1 
//                 << ", has its maximum time difference at " 
// 	        <<  aPar->GetVal()
// 	        <<  " ns (need to check unit). \n";
      /// Draw  the corrected difference (second order alignment...
      c->cd(4);
      t->Draw(
         Form("fLuciteHodoscopeEvent.fLucitePositionHits.fTDCDifference-%e >>hlucCorrected%d(100,-100,300)", aPar->GetVal(),i+1),
         cTrigger && cLuciteBar,
         "goff",
         nevents);
      h1  = (TH1F*)gROOT->FindObject(Form("hlucCorrected%d",i+1));
      h1->SetLineColor(i+1);
      //h1->SetFillColor(i+1);
      //h1->SetFillStyle(1001);
      hsLucDiffHistsRealign->Add(h1);
      if(!fast){
         hsLucDiffHistsRealign->Draw("nostack");
      }

      /// Create the plot of lucite x vs bigcal-x 
    if(!fast){
      c->cd(5);
      t->Draw(
         Form("fLuciteHodoscopeEvent.fLucitePositionHits.fBarNumber:(fLuciteHodoscopeEvent.fLucitePositionHits.fTDCDifference - %e)>>hlucCorVsPosVert%d(100,-100,300,30,0,30)",
         aPar->GetVal(),i+1),
         cTrigger && cLuciteBar && cCherenkovTDCHit,
         "goff",
         nevents);
      h1 = (TH1F*)gROOT->FindObject(Form("hlucCorVsPosVert%d",i+1));
      h1->Draw(Form("col,%s",options.Data()));

      c->cd(6);
         t->Draw(Form("bigcalClusters.fXMoment:fLuciteHodoscopeEvent.fLucitePositionHits.fTDCDifference - %e >>hlucCorVsBigcalPi0%d(100,-100,300,100,-80,80)", aPar->GetVal(),i+1),
         cPi0Trigger && cLuciteBar && cCherenkovTDCHit,
         Form("col,%s",options.Data()),
         nevents);
    }

      c->cd(7);
      t->Draw(
         Form("bigcalClusters.fXMoment:fLuciteHodoscopeEvent.fLucitePositionHits.fTDCDifference-%e>>hbcluc%d(100,-100,300,100,-60,60)",aPar->GetVal(),i+1),
         cTrigger && cLuciteBar && cCherenkovTDCHit,
         Form("goff"),
         nevents);
      h2  = (TH2F*)gROOT->FindObject(Form("hbcluc%d",i+1));
      p1 = h2->ProfileX(Form("hbcluc%d_pfx",i+1));
      p1->Fit(f1,"M,R,Q","goff");

      p1->SetLineColor(i+1);
      hsLucDiffProfilefit->Add(p1);
    if(!fast){
      hsLucDiffProfilefit->Draw("nostack");
    }
      c->cd(8);
    if(!fast){
      f1->DrawClone(options.Data());
    }
//       t->Draw(
//          Form("bigcalClusters.fXMoment:fLuciteHodoscopeEvent.fLucitePositionHits.fTDCDifference-%e>>hbcluc%d(100,-100,300,100,-60,60)",aPar->GetVal(),i+1),
//          cTrigger && cLuciteBar && cCherenkovTDCHit,
//          Form("goff") ,
//          nevents);
//       h2  = (TH2F*)gROOT->FindObject(Form("hbcluc%d",i+1));
//       p1 = h2->ProfileX(Form("hbcluc%d_pfx",i+1));
//       p1->Fit(f1,"R,Q","goff");
// 
//       p1->SetLineColor(i+1);
//       hsLucDiffProfilefit->Add(p1);
//       hsLucDiffProfilefit->Draw("nostack");

/*      p1->DrawClone(options.Data());*/

    if(!fast){
      c->Update();
    }
    
   }
/*   Double_t maxValue = h1->GetBinContent(h1->GetMaximumBin());*/
//    c->cd(8);
//    t->Draw(Form("fLuciteHodoscopeEvent.fLucitePositionHits[].fPositionVector.fY:fLuciteHodoscopeEvent.fLucitePositionHits[Iteration$].fPositionVector.fX-%e>>hLucXY(100,-100,300,56,1,29)",
// 			   lucitePositionCalibration->fConstants->at(i)),
//       cTrigger && cCherenkovTDCHit,
//       "colz");
 //   lucitePositionCalibration->Write("lucitePositionCalibration",kOverwrite);


   TFile * f = new TFile("data/HodoscopeCalibrations.root","UPDATE");
   f->cd();

   lucitePositionCalibrations->Write(Form("lucitePositionCalibration%d",runnumber),TObject::kWriteDelete);


   c->SaveAs(Form("plots/%d/lucitePositions.jpg",runnumber));

   std::cout << " - Tree " << t->GetName() << " had " << t->GetEntries() << " entries.\n";


//   c->SaveAs(Form("plots/%d/lucite_bar_geo.png",runnumber));
//   c->SaveAs(Form("plots/%d/lucite_bar_geo.ps",runnumber));


  return(0);
}
