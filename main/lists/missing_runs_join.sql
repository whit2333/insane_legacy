Select all_runs.run_number,all_runs.target_typ FROM all_runs
LEFT JOIN run_info ON all_runs.run_number=run_info.run_number
WHERE all_runs.run_number NOT IN (SELECT run_number FROM  run_info)
UNION
Select all_runs.run_number,all_runs.target_typ FROM all_runs
RIGHT JOIN run_info ON all_runs.run_number=run_info.run_number
WHERE run_info.run_number NOT IN (SELECT run_number FROM  all_runs);
