-- selects the good 5.9 GEV

.mode list
.separator " "

CREATE TEMP VIEW a_good_list AS 
SELECT analysis_run_list.run_number, beam_pol_per_run.beam_energy 
FROM analysis_run_list 
JOIN beam_pol_per_run 
ON analysis_run_list.run_number = beam_pol_per_run.run_number AND analysis_run_list.NH3=1 AND analysis_run_list.overall=1 ;

CREATE TEMP VIEW a_better_list AS 
SELECT a_good_list.run_number, a_good_list.beam_energy, run_info.target_angle 
FROM a_good_list 
JOIN run_info 
ON a_good_list.run_number = run_info.run_number;

select * FROM a_better_list 
WHERE target_angle BETWEEN 170.0 AND 190.0 AND beam_energy BETWEEN 5500 AND 6100;

--AND beam_pol_per_run.beam_energy > 5500 AND beam_pol_per_run.beam_energy < 6100 AND analysis_run_list.overall=1 AND analysis_run_list.NH3=1;

