Int_t simulated_cherenkov(Int_t runNumber =  5000 ){

   gROOT->LoadMacro("background/eplus_eminus_ratio.cxx"); 
   // function of theta,E' (deg,GeV)
   TF2 * R_ratio = GetR_PlusMinus();

   //load_style("SingleSquarePlot");
   //gROOT->SetStyle("SingleSquarePlot");

   for(int i = 0;i<100;i++){
      aman->fRunQueue->push_back(runNumber+i);
   }
   //aman->fRunQueue->push_back(1002);
   //aman->fRunQueue->push_back(1003);
   //aman->fRunQueue->push_back(1004);
   //aman->fRunQueue->push_back(1005);
   //aman->fRunQueue->push_back(1006);
   //aman->fRunQueue->push_back(1007);
   //aman->fRunQueue->push_back(1008);
   //aman->fRunQueue->push_back(1009);

   //aman->fRunQueue->push_back(1020);
   //aman->fRunQueue->push_back(1021);
   //aman->fRunQueue->push_back(1022);
   //aman->fRunQueue->push_back(1023);
   //aman->fRunQueue->push_back(1024);
   //aman->fRunQueue->push_back(1025);
   //aman->fRunQueue->push_back(1026);
   //aman->fRunQueue->push_back(1027);
   //aman->fRunQueue->push_back(1028);
   //aman->fRunQueue->push_back(1029);
   //aman->fRunQueue->push_back(1030);
   //aman->fRunQueue->push_back(1031);
   //aman->fRunQueue->push_back(1032);
   //aman->fRunQueue->push_back(1033);
   //aman->fRunQueue->push_back(1034);
   //aman->fRunQueue->push_back(1035);
   //aman->fRunQueue->push_back(1036);
   //aman->fRunQueue->push_back(1037);
   //aman->fRunQueue->push_back(1038);
   //aman->fRunQueue->push_back(1039);
   // new electronPion with Oscar's fit


   //  old allNH3 event gen
   //aman->fRunQueue->push_back(954);
   //aman->fRunQueue->push_back(955);
   //aman->fRunQueue->push_back(956);
   //aman->fRunQueue->push_back(957);
   //aman->fRunQueue->push_back(958);
   //aman->fRunQueue->push_back(959);
   //aman->fRunQueue->push_back(960);
   //aman->fRunQueue->push_back(961);
   //aman->fRunQueue->push_back(962);
   //aman->fRunQueue->push_back(963);
   //aman->fRunQueue->push_back(964);
   //aman->fRunQueue->push_back(965);
   //aman->fRunQueue->push_back(966);
   //aman->fRunQueue->push_back(967);
   //aman->fRunQueue->push_back(968);
   //aman->fRunQueue->push_back(969);

   // 
   //aman->fRunQueue->push_back(901);
   //aman->fRunQueue->push_back(902);
   //aman->fRunQueue->push_back(903);
   //aman->fRunQueue->push_back(952);
   //aman->fRunQueue->push_back(6052);
   //aman->fRunQueue->push_back(6056);
   //aman->fRunQueue->push_back(6057);

   TChain * ch1 = aman->BuildChain("Tracks");
   TChain * ch2 = aman->BuildChain("betaDetectors1");

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetBranches(ch2);
   events->SetClusterBranches(ch2);
   ch2->BuildIndex("fRunNumber","fEventNumber");

   BETAG4MonteCarloEvent * mcevent  = (BETAG4MonteCarloEvent*)(events->MC);
   InSANEParticle * thrownEvent     = new InSANEParticle();
   
   // Histograms
   TH2F * hNThrownVNTrackerplane = new TH2F("hNThrownVNTrackerplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   TH2F * hNThrownVNBigcalplane  = new TH2F("hNThrownVNBigcalplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   TH2F * hRaster                = new TH2F("hRaster","Raster;x;y",100,-2.5,2.5,100,-2.5,2.5);

   // Event Loop.
   Int_t nevents = ch2->GetEntries();
   std::cout << " chain has " << nevents << "\n";
   //for(int ievent = 0; ievent < nevents ; ievent++){
   //   thrownEvent = (InSANEParticle*)(*(events->MC->fThrownParticles))[0];      
   //   hRaster->Fill(thrownEvent->Vx(),thrownEvent->Vy());
   //   hNThrownVNTrackerplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fTrackerPlaneHits->GetEntries());
   //   hNThrownVNBigcalplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fBigcalPlaneHits->GetEntries());
   //}// Event loop end 

   TCanvas * c = new TCanvas("particles_thrown","thrown_particle_distribution");
   TH1F * h1 = 0;
   TH2F * h2 = 0;

   c->Divide(3,3);
   c->cd(1);
   ch2->Draw("fNumberOfParticlesThrown:fThrownParticles.fPdgCode","","box");

   c->cd(2);
   ch2->Draw("fThrownParticles.fVy:fThrownParticles.fVx","","colz");

   c->cd(3);
   ch2->Draw("fThrownParticles.fVy:fThrownParticles.fVz","","colz");

   c->cd(4);
   ch2->Draw("bigcalClusters.fCherenkovBestADCSum>>cerADC(100,0,5)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25","");

   c->cd(5);
   ch2->Draw("bigcalClusters.fCherenkovTDC>>cerTDC(100,-100,100)","","");

   c->cd(6);
   ch2->Draw("bigcalClusters.fCherenkovTDC","","");

   c->cd(7);
   ch2->Draw("bigcalClusters.fPrimaryMirror:bigcalClusters.fCherenkovBestADCSum>>cerADCvsMir(100,0,5,10,0,10)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25","colz");


   c->SaveAs(Form("results/detectors/cherenkov/simulated_cherenkov_%d.png",runNumber));

   TCanvas * c2 = new TCanvas("cerADC","cerADC");
   c2->cd();
   //gPad->SetLogy(true);
   ch2->Draw("bigcalClusters.fCherenkovBestADCSum>>cerADC2(100,0,4)","","goff");
   ch2->Draw("bigcalClusters.fCherenkovBestADCSum>>cerADC3(100,0,4)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25","goff");
   ch2->Draw("bigcalClusters.fCherenkovBestADCSum>>cerADC4(100,0,4)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25&&fThrownParticles.fPdgCode==111","goff");
   ch2->Draw("bigcalClusters.fCherenkovBestADCSum>>cerADC5(100,0,4)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25&&fThrownParticles.fPdgCode==11","goff");
   ch2->Draw("bigcalClusters.fCherenkovBestADCSum>>cerADC6(100,0,4)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25&&fThrownParticles.fPdgCode==111&&fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0","goff");

   ch2->Draw("(bigcalClusters.fDeltaE+bigcalClusters.fTotalE)/1000>>cerEnergy( 40,0.5,2)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25&&fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0","goff");
   ch2->Draw("(bigcalClusters.fDeltaE+bigcalClusters.fTotalE)/1000>>cerEnergy0(40,0.5,2)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25&&fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0&&bigcalClusters.fCherenkovBestADCSum>0.5&&bigcalClusters.fCherenkovBestADCSum<1.5","goff");
   ch2->Draw("(bigcalClusters.fDeltaE+bigcalClusters.fTotalE)/1000>>cerEnergy1(40,0.5,2)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25&&fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0&&fThrownParticles.fPdgCode==11","goff");
   ch2->Draw("(bigcalClusters.fDeltaE+bigcalClusters.fTotalE)/1000>>cerEnergy2(40,0.5,2)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25&&fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0&&fThrownParticles.fPdgCode==111","goff");
   ch2->Draw("(bigcalClusters.fDeltaE+bigcalClusters.fTotalE)/1000>>cerEnergy3(40,0.5,2)","TMath::Abs(bigcalClusters.fCherenkovTDC)<25&&fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0&&fThrownParticles.fPdgCode==111&&bigcalClusters.fCherenkovBestADCSum>0.5&&bigcalClusters.fCherenkovBestADCSum<1.5","goff");


   // ----------------------------------------------------
   // Cherenkov ADC plot

   h1 = (TH1F*)gROOT->FindObject("cerADC3");
   if(h1) {
      h1->Draw();
      h1->SetTitle("Simulated #check{C}herenkov Response");
      h1->GetXaxis()->CenterTitle(true);
      h1->GetXaxis()->SetTitle("ADC #propto number of tracks");
      h1->Draw();
   }
   h1 = (TH1F*)gROOT->FindObject("cerADC4");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
   }
   h1 = (TH1F*)gROOT->FindObject("cerADC6");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
   }


   c2->SaveAs(Form("results/detectors/cherenkov/simulated_cherenkov2_%d.png",runNumber));
   c2->SaveAs(Form("results/detectors/cherenkov/simulated_cherenkov2_%d.pdf",runNumber));

   // ----------------------------------------------------
   TCanvas * c3 = new TCanvas("cerADC2","cerEnergy");

   h1 = (TH1F*)gROOT->FindObject("cerEnergy2");
   TH1F * hpion = (TH1F*)h1->Clone();

   h1 = (TH1F*)gROOT->FindObject("cerEnergy3");
   TH1F * hpion2 = (TH1F*)h1->Clone();

   //c3->Divide(1,2);
   //c3->cd(1);

   TLegend * leg  = new TLegend(0.6,0.6,0.88,0.88);

   h1 = (TH1F*)gROOT->FindObject("cerEnergy0");
   TH1F * helectron0 = (TH1F*)h1->Clone();
   helectron0->Sumw2(true);

   TH1F * hRatio = (TH1F*)h1->Clone();
   TH1F * hrRatio = (TH1F*)h1->Clone();
   hRatio->Sumw2(true);
   hrRatio->Sumw2(true);
   TH1 * histAx =  0;

   h1 = (TH1F*)gROOT->FindObject("cerEnergy");
   if(h1) {
      h1->Sumw2(true);
      hpion->Sumw2(true);
      hpion->Divide(h1);
      hpion->SetLineColor(4);

      histAx = hpion->DrawCopy("hist,C");
      hpion->DrawCopy("same,E1");

      hpion2->Sumw2(true);
      hpion2->Divide(helectron0);
      hpion2->SetLineColor(2);
      hpion2->DrawCopy("hist,same,C");
      hpion2->DrawCopy("same,E1");


      //TH1F * hpionTimes2 = (TH1F*)hpion2->Clone();
      //hpionTimes2->Scale(2.0);
      //hpionTimes2->SetLineColor(6);
      //hpionTimes2->Draw("same");

      leg->AddEntry(hpion,"No Window cut","l");
      leg->AddEntry(hpion2,"Window cut","l");
      //leg->AddEntry(hpionTimes2,"Window cut x2","l");
   }
   histAx->GetYaxis()->SetRangeUser(0.0,1.5);
   histAx->SetTitle("Pion events/all");
   histAx->GetXaxis()->CenterTitle(true);
   histAx->GetXaxis()->SetTitle("Energy (GeV)");

   hRatio->Reset();
   hrRatio->Reset();
   Double_t theta_avg = 38.0;
   Int_t xBinmax = hRatio->GetNbinsX();
   Int_t yBinmax = hRatio->GetNbinsY();
   Int_t zBinmax = hRatio->GetNbinsZ();
   Int_t bin     = 0;

   // -------------------------------------------------
   // Loop over E bins
   for(Int_t i=0; i<= xBinmax; i++){
      bin   = hRatio->GetBin(i);
      Double_t Eprime = hRatio->GetBinCenter(bin);
      Double_t Rratio = R_ratio->Eval(theta_avg,Eprime);
      Double_t rratio = Rratio/(1.0+Rratio);
      Double_t R_HC = 0.06/0.028;
      Double_t R_oscar = 2.0*R_HC*Rratio*(1.0+Rratio*(2.0*R_HC-1.0));
      hRatio->SetBinContent(bin,Rratio);
      hrRatio->SetBinContent(bin,rratio);
   }
   hRatio->Draw("same,C");
   hrRatio->SetLineColor(8);
   hrRatio->Draw("same,C");
   leg->AddEntry(hRatio,"R=e^{pair}/e^{total} model - #theta=38","l");
   leg->AddEntry(hrRatio,"r=e^{pair}/e^{DIS} ","l");

   leg->Draw();

   c3->SaveAs(Form("results/detectors/cherenkov/simulated_cherenkov3_%d.png",runNumber));
   c3->SaveAs(Form("results/detectors/cherenkov/simulated_cherenkov3_%d.pdf",runNumber));

   // ----------------------------------------------------
   TCanvas * c4 = new TCanvas("cerADC4","cerEnergy");
   TLegend * leg4  = new TLegend(0.6,0.6,0.88,0.88);
   gPad->SetLogy(true);
   TH1 * histAxis4 = 0;

   h1 = (TH1F*)gROOT->FindObject("cerEnergy");
   if(h1) {
      histAxis4 = h1->DrawCopy("E1");
      h1->DrawCopy("same,C");
      leg4->AddEntry(h1,"no window cut","l");
      h1->SetTitle("");
   }
   histAxis4->GetYaxis()->SetRangeUser(0.5,1000);
   histAxis4->SetTitle("");
   histAxis4->GetXaxis()->CenterTitle(true);
   histAxis4->GetXaxis()->SetTitle("Energy (GeV)");

   h1 = (TH1F*)gROOT->FindObject("cerEnergy0");
   if(h1) {
      h1->SetLineColor(2);
      h1->DrawCopy("same,E1");
      h1->Draw("same,C");
      leg4->AddEntry(h1,"window cut","l");
   }
   h1 = (TH1F*)gROOT->FindObject("cerEnergy1");
   if(h1) {
      h1->SetLineColor(4);
      h1->DrawCopy("same,E1");
      h1->Draw("same,C");
      leg4->AddEntry(h1,"e- no window","l");
   }
   h1 = (TH1F*)gROOT->FindObject("cerEnergy2");
   if(h1) {
      h1->SetLineColor(6);
      h1->DrawCopy("same,E1");
      h1->Draw("same,C");
      leg4->AddEntry(h1,"#pi no window","l");
   }
   h1 = (TH1F*)gROOT->FindObject("cerEnergy3");
   if(h1) {
      h1->SetLineColor(8);
      h1->DrawCopy("same,E1");
      h1->Draw("same,C");
      leg4->AddEntry(h1,"#pi with window cut","l");
   }

   leg4->Draw();

   c4->SaveAs(Form("results/detectors/cherenkov/simulated_cherenkov4_%d.png",runNumber));
   c4->SaveAs(Form("results/detectors/cherenkov/simulated_cherenkov4_%d.pdf",runNumber));

   TFile * fout = new TFile("data/electron_pion_ratio.root","UPDATE");

   hpion2->Write("Replus_eminus");

   fout->Flush();


   return(0);
}

