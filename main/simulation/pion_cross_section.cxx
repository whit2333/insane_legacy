Int_t pion_cross_section(Int_t runnumber = 6050, Int_t fileVersion = 16, Double_t E_min = 1000.0 ) {

   // Simulated runs with allNH3 event generator
   //aman->fRunQueue->push_back(100);
   aman->fRunQueue->push_back(101);
   aman->fRunQueue->push_back(102);
   aman->fRunQueue->push_back(103);
   aman->fRunQueue->push_back(104);
   aman->fRunQueue->push_back(105);
   aman->fRunQueue->push_back(106);
   aman->fRunQueue->push_back(107);
   aman->fRunQueue->push_back(108);
   aman->fRunQueue->push_back(109);
   aman->fRunQueue->push_back(110);
   aman->fRunQueue->push_back(111);
   aman->fRunQueue->push_back(112);
   aman->fRunQueue->push_back(113);
   aman->fRunQueue->push_back(114);
   aman->fRunQueue->push_back(115);
   aman->fRunQueue->push_back(116);
   aman->fRunQueue->push_back(117);
   aman->fRunQueue->push_back(118);
   aman->fRunQueue->push_back(119);
   aman->fRunQueue->push_back(6050);
   //aman->fRunQueue->push_back(6052);
   //aman->fRunQueue->push_back(6056);
   //aman->fRunQueue->push_back(6057);

   TChain * ch1 = aman->BuildChain("Tracks");
   TChain * ch2 = aman->BuildChain("betaDetectors1");

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   bcgeo->LoadBadBlocks(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber));

   ch1->SetBranchAddress("trajectory",&traj);
   ch1->SetBranchAddress("triggerEvent",&trig);

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetBranches(ch2);
   events->SetClusterBranches(ch2);
   ch2->BuildIndex("fRunNumber","fEventNumber");

   Bool_t   passed          = false;  // passes detector cuts
   Bool_t   isDISEvent      = false;  // is DIS electron event
   Double_t theta_range[2]  = {25.0,55.0};   //Degrees
   Double_t phi_range[2]    = {-70.0,70.0};
   Double_t energy_range[2] = {0.500,2.000}; //GeV


   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
         40,phi_range[0],phi_range[1]);
   TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
         40,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy = new TEfficiency("effEnergy","f_{bg} Vs E;E;f_{bg}",
         40,energy_range[0],energy_range[1]);

   TEfficiency* fbgThrownEnergy = new TEfficiency("fbgThrownEnergy","f_{bg} Vs E thrown;E;f_{bg}",
         40,energy_range[0],energy_range[1]);

   TEfficiency* DetEffEnergy = new TEfficiency("DetEffEnergy","Detector Efficiency vs E;E;#epsilon",
         40,energy_range[0],energy_range[1]);
   TEfficiency* DetEffEnergyTheta = new TEfficiency("DetEffEnergyTheta","Detector Efficiency vs E and Theta;E;#theta",
         40,energy_range[0],energy_range[1],
         40,theta_range[0],theta_range[1]);

   TEfficiency* DetEffThetaPhi = new TEfficiency("DetEffThetaPhi","Detector Efficiency vs #theta and #phi;#theta;#phi",
         40,theta_range[0],theta_range[1],40,phi_range[0],phi_range[1]);
   TEfficiency* DetEffThetaPhi2 = new TEfficiency("DetEffThetaPhi2","Detector Efficiency vs #theta and #phi in BigCal Coords;#theta;#phi",
         40,theta_range[0],theta_range[1],40,phi_range[0],phi_range[1]);

   TH1F* hEnergy1 = new TH1F("hEnergy1","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy2 = new TH1F("hEnergy2","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy3 = new TH1F("hEnergy3","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy4 = new TH1F("hEnergy4","E;E;#epsilon", 40,energy_range[0],energy_range[1]);

   // ------------------------------------------------

   Int_t nEvents = ch1->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      ch1->GetEntry(iEvent);
      ch2->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "run  :       " << trig->fRunNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;
      dis->fRunNumber         = traj->fRunNumber;
      runnum                  = traj->fRunNumber;
      fDisEvent->fEventNumber = traj->fEventNumber;
      ev                      = traj->fEventNumber;
      stdcut                  = 0;
      dis->fHelicity          = traj->fHelicity;
      dis->fx                 = traj->GetBjorkenX();
      dis->fW                 = TMath::Sqrt(traj->GetInvariantMassSquared())/1000.0;
      dis->fQ2                = traj->GetQSquared()/1000000.0;
      dis->fTheta             = traj->GetTheta();
      dis->fPhi               = traj->GetPhi();
      dis->fEnergy            = traj->GetEnergy()/1000.0;
      dis->fBeamEnergy        = traj->fBeamEnergy/1000.0;
      //dis->fGroup             = 0;//traj->fSubDetector;
      //dis->fIsGood            = false;
      //dis->fIsGood            = traj->fIsGood;
      Int_t ibl = bcgeo->GetBlockNumber(traj->fCluster.fiPeak,traj->fCluster.fjPeak);

      passed                      = false;
      isDISEvent                  = false;
      InSANEParticle * thrownPart = (InSANEParticle*)(*(events->MC->fThrownParticles))[0];

      if(thrownPart->GetPdgCode() == 11) isDISEvent = true; 

      if( TMath::Abs(traj->fCluster->fYMoment) <100 )
      if( TMath::Abs(traj->fCluster->fXMoment) <52 ){
      if( events->CLUSTER->fNClusters == 2)
      if( trig->IsPi0Event() )   {
         hEnergy3->Fill(dis->fEnergy);
         //if( !traj->fIsNoisyChannel)
         //if( traj->fIsGood)
         //if( dis->fW > 1.0 )
         //if( traj->fNCherenkovElectrons > 0.1 && traj->fNCherenkovElectrons < 1.5 )
         if( traj->fNCherenkovElectrons < 0.3 )
            //if( !(bcgeo->IsBadBlock(ibl)) )
            //if( traj->fEnergy > E_min )
            //if(TMath::Abs(traj->fCherenkovTDC) < 20) 
            if(true){
               passed = true;
               hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
               if(thrownPart->GetPdgCode() == 11) hEnergy1->Fill(dis->fEnergy);
               else if(thrownPart->GetPdgCode() == 111) hEnergy2->Fill(dis->fEnergy);
               hEnergy4->Fill(dis->fEnergy);
            } 
      }

      hPhiVsTheta1->Fill(dis->fTheta/degree,dis->fPhi/degree);
      effPhi->Fill(passed,traj->fPhi/degree);
      effTheta->Fill(passed,traj->fTheta/degree);
      effEnergy->Fill(passed,dis->fEnergy);
      fbgThrownEnergy->Fill(passed,thrownPart->Energy());

      if(isDISEvent) {
         DetEffThetaPhi->Fill(passed,thrownPart->Theta()/degree,thrownPart->Phi()/degree);
         DetEffEnergy->Fill(passed,thrownPart->Energy());
         DetEffEnergyTheta->Fill(passed,thrownPart->Energy(),thrownPart->Theta()/degree);

         DetEffThetaPhi2->Fill(passed,traj->fCluster.GetTheta()/degree,traj->fCluster.GetPhi()/degree);
      }
      }

   }


   // ------------------------------------------------------------
   // Plot asymmetries
   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 
   c->Divide(2,2);

   c->cd(1);
   hEnergy1->SetLineWidth(2);
   hEnergy2->SetLineWidth(2);
   hEnergy3->SetLineWidth(2);
   hEnergy1->SetLineColor(4);
   hEnergy2->SetLineColor(2);
   hEnergy4->SetLineColor(kGreen-4);

   hEnergy3->Draw();
   hEnergy2->DrawCopy("same");
   hEnergy1->DrawCopy("same");
   hEnergy4->Draw("same");
   TLegend * leg1 = new TLegend(0.5,0.6,0.9,0.9);
   leg1->AddEntry(hEnergy3,"All Events with BETA2","l");
   leg1->AddEntry(hEnergy1,"Pion Events passing cuts","l");
   leg1->AddEntry(hEnergy2,"DIS  Events passing cuts","l");
   leg1->AddEntry(hEnergy4,"All  Events passing cuts","l");
   leg1->Draw();

   c->cd(2);
   hPhiVsTheta1->Draw("colz");

   c->cd(3);
   hEnergy2->Divide(hEnergy1);
   hEnergy2->Draw();
   //effEnergy->Draw();
   //fbgThrownEnergy->SetLineColor(2);
   //fbgThrownEnergy->Draw("same");

   c->cd(4);
   //DetEffEnergyTheta->Draw("colz");
   //DetEffThetaPhi->Draw("colz");
   DetEffThetaPhi2->Draw("colz");

   c->SaveAs(Form("results/pion_cross_section_%d.png",fileVersion));

   //if(writeRun) rman->WriteRun();
   ////if(writeRun) gROOT->ProcessLine(".q");

   ////delete events;
   //delete traj;
   //delete trig;
   //delete dis;

   ch2->StartViewer();

   return(0);
}
