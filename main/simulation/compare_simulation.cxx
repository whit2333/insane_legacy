Int_t compare_simulation(Int_t runNumber = 72923, Int_t simnumber = 4628 ){

   TString clusterCuts = "bigcalClusters.fIsGood && bigcalClusters.fTotalE > 800 && bigcalClusters.fCherenkovBestADCSum > 0.3";
   TString clusterCuts1 = "bigcalClusters.fIsGood && bigcalClusters.fTotalE > 800 && bigcalClusters.fCherenkovBestADCSum > 0.3";
   TString clusterCuts2 = "bigcalClusters.fIsGood && bigcalClusters.fTotalE > 1100 && bigcalClusters.fCherenkovBestADCSum > 0.3";
   TString clusterCuts3 = "bigcalClusters.fIsGood && bigcalClusters.fTotalE > 1100 && bigcalClusters.fCherenkovBestADCSum > 0.3";

   rman->SetRun(runNumber);
   InSANERun * run = rman->GetCurrentRun();

   SANEEvents * events              = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   BETAG4MonteCarloEvent * mcevent  = (BETAG4MonteCarloEvent*)(events->MC);
   BETAG4MonteCarloThrownParticle * thrownEvent = new BETAG4MonteCarloThrownParticle();
   TTree * t                        = events->fTree;
   
   // Histograms
   //TH2F * hNThrownVNTrackerplane = new TH2F("hNThrownVNTrackerplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   //TH2F * hNThrownVNBigcalplane = new TH2F("hNThrownVNBigcalplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   //TH2F * hRaster = new TH2F("hRaster","Raster;x;y",100,-2.5,2.5,100,-2.5,2.5);
   /// Event Loop.
   //Int_t nevents = t->GetEntries();
   //std::cout << " Tree has " << nevents << "\n";
   //for(int ievent = 0; ievent < nevents ; ievent++){
   //   thrownEvent = (BETAG4MonteCarloThrownParticle*)(*(events->MC->fThrownParticles))[0];      
   //   hRaster->Fill(thrownEvent->Vx(),thrownEvent->Vy());
   //   hNThrownVNTrackerplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fTrackerPlaneHits->GetEntries());
   //   hNThrownVNBigcalplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fBigcalPlaneHits->GetEntries());
   //}// Event loop end 

   TCanvas * c = new TCanvas("particles_thrown","compare_simulation");
   TH1F * h1 = 0;
   TH2F * h2 = 0;

   Double_t dataMax = 0.0;
   Int_t    scaleBin = 0;
   Double_t simMax = 0.0;
   Double_t scale = 0.0;

   Double_t dataMax2 = 0.0;
   Int_t    scaleBin2 = 0;
   Double_t simMax2 = 0.0;
   Double_t scale2 = 0.0;

   gROOT->cd();

   TLegend * leg1 = new TLegend(0.7,0.7,0.9,0.9);
   TLegend * leg2 = new TLegend(0.7,0.7,0.9,0.9);

   c->Divide(3,2);
   c->cd(1);
   t->Draw("bigcalClusters.GetCorrectedEnergy()>>bcClusterE(50,0,3000)",clusterCuts1.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterE");
   if(h1) {
      scaleBin = h1->FindBin(1100);
      //scaleBin = h1->GetMaximumBin();
      dataMax = h1->GetBinContent(scaleBin);
      h1->SetLineColor(kBlue);
      h1->Draw();
      leg1->AddEntry(h1,"data","l");
   }
   t->Draw("bigcalClusters.GetCorrectedEnergy()>>bcClusterEd2(50,0,3000)",clusterCuts2.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterEd2");
   if(h1) {
      scaleBin2 = h1->FindBin(1300);
      //scaleBin = h1->GetMaximumBin();
      dataMax2 = h1->GetBinContent(scaleBin2);
      h1->SetLineColor(kRed);
      h1->Draw("same");
      leg1->AddEntry(h1,"data - E>1100","l");
   }

   c->cd(2);

   t->Draw("bigcalClusters.GetTheta()/0.0175>>bcClusterTheta(100,20,60)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterTheta");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
      //leg5->AddEntry(h1,"cluster with E>500","l");
   }

   c->cd(3);

   t->Draw("bigcalClusters.GetPhi()/0.0175>>bcClusterPhi(100,-90,90)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterPhi");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
      //leg5->AddEntry(h1,"cluster with E>500","l");
   }

   c->cd(4);
   t->Draw("bigcalClusters.fXStdDeviation>>bcXstdDev(100,0,3)",clusterCuts.Data(),"goff");
   t->Draw("bigcalClusters.fYStdDeviation>>bcYstdDev(100,0,3)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcXstdDev");
   if(h1) {
      h1->SetLineColor(kRed);
      h1->Draw();
      leg2->AddEntry(h1,"cluster #sigma_{x}","l");
   }
   h1 = (TH1F*)gROOT->FindObject("bcYstdDev");
   if(h1) {
      h1->SetLineColor(kBlue);
      h1->Draw("same");
      leg2->AddEntry(h1,"cluster #sigma_{y}","l");
   }
   
   c->cd(5);
   t->Draw("bigcalClusters.fXSkewness>>bcXskew(100,-9,9)",clusterCuts.Data(),"goff");
   t->Draw("bigcalClusters.fYSkewness>>bcYskew(100,-9,9)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcXskew");
   if(h1) {
      h1->SetLineColor(kRed);
      h1->Draw();
   }
   h1 = (TH1F*)gROOT->FindObject("bcYskew");
   if(h1) {
      h1->SetLineColor(kBlue);
      h1->Draw("same");
   }



   /// Plot simulations
   rman->SetRun(simnumber);
   SANEEvents * events2              = new SANEEvents("betaDetectors1");
   events2->SetClusterBranches(events2->fTree);
   t = events2->fTree;

   c->cd(1);
   t->Draw("bigcalClusters.GetCorrectedEnergy()>>bcClusterE2(50,0,3000)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterE2");
   if(h1) {
      simMax = h1->GetBinContent(scaleBin);
      scale = dataMax/simMax ;
      h1->Scale(scale);
      h1->SetLineColor(kBlue-9);
      h1->Draw("same");
      leg1->AddEntry(h1,"simulation","l");
   }
   t->Draw("bigcalClusters.GetCorrectedEnergy()>>bcClusterEs2(50,0,3000)",clusterCuts2.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterEs2");
   if(h1) {
      simMax2 = h1->GetBinContent(scaleBin2);
      scale2 = dataMax2/simMax2 ;
      h1->Scale(scale2);
      h1->SetLineColor(kRed-9);
      h1->Draw("same");
      leg1->AddEntry(h1,"simulation - E>1300","l");
   }
   leg1->Draw();

   c->cd(2);

   t->Draw("bigcalClusters.GetTheta()/0.0175>>bcClusterTheta2(100,20,60)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterTheta2");
   if(h1) {
      h1->Scale(scale);
      h1->SetLineColor(2);
      h1->Draw("same");
      //leg5->AddEntry(h1,"cluster with E>500","l");
   }

   c->cd(3);

   t->Draw("bigcalClusters.GetPhi()/0.0175>>bcClusterPhi2(100,-90,90)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterPhi2");
   if(h1) {
      h1->Scale(scale);
      h1->SetLineColor(2);
      h1->Draw("same");
      //leg5->AddEntry(h1,"cluster with E>500","l");
   }

   c->cd(4);
   t->Draw("bigcalClusters.fXStdDeviation>>bcXstdDev2(100,0,3)",clusterCuts.Data(),"goff");
   t->Draw("bigcalClusters.fYStdDeviation>>bcYstdDev2(100,0,3)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcXstdDev2");
   if(h1) {
      h1->Scale(scale);
      h1->SetLineColor(kRed-9);
      h1->Draw("same");
      leg2->AddEntry(h1,"sim - cluster #sigma_{x}","l");
   }
   h1 = (TH1F*)gROOT->FindObject("bcYstdDev2");
   if(h1) {
      h1->Scale(scale);
      h1->SetLineColor(kBlue-9);
      h1->Draw("same");
      leg2->AddEntry(h1,"sim - cluster #sigma_{y}","l");
   }
   leg2->Draw(); 

   c->cd(5);
   t->Draw("bigcalClusters.fXSkewness>>bcXskew2(100,-9,9)",clusterCuts.Data(),"goff");
   t->Draw("bigcalClusters.fYSkewness>>bcYskew2(100,-9,9)",clusterCuts.Data(),"goff");
   h1 = (TH1F*)gROOT->FindObject("bcXskew2");
   if(h1) {
      h1->Scale(scale);
      h1->SetLineColor(kRed-9);
      h1->Draw("same");
   }
   h1 = (TH1F*)gROOT->FindObject("bcYskew2");
   if(h1) {
      h1->Scale(scale);
      h1->SetLineColor(kBlue-9);
      h1->Draw("same");
   }

   std::cout << "scaleBin = " << scaleBin << std::endl;
   std::cout << "dataMax  = " << dataMax << std::endl;
   std::cout << "simMax   = " << simMax << std::endl;
   std::cout << "scale    = " << scale << std::endl;

   t->StartViewer();
   
   c->SaveAs(Form("results/compare_simulation%d.png",runNumber));

   return(0);
}

