Int_t f_pairs_para59_data(Int_t runnumber = 72927, Int_t fileVersion = 16, Double_t E_min = 1000.0 ) {

   rman->SetRun(runnumber);

   Double_t Qtotal = rman->GetCurrentRun()->fTotalQPlus; 
   Qtotal         += rman->GetCurrentRun()->fTotalQMinus;
   Double_t LiveTime = rman->GetCurrentRun()->fLiveTime; 
   
   // Simulated runs with allNH3 event generator
   aman->fRunQueue->push_back(runnumber);

   TChain * ch1 = aman->BuildChain("Tracks");
   TChain * ch2 = aman->BuildChain("betaDetectors1");

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   bcgeo->LoadBadBlocks(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber));

   ch1->SetBranchAddress("trajectory",&traj);
   ch1->SetBranchAddress("triggerEvent",&trig);

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetBranches(ch2);
   events->SetClusterBranches(ch2);
   ch2->BuildIndex("fRunNumber","fEventNumber");

   Bool_t   passed          = false;  // passes detector cuts
   Bool_t   isDISEvent      = false;  // is DIS electron event
   Double_t theta_range[2]  = {25.0,55.0};   //Degrees
   Double_t phi_range[2]    = {-70.0,70.0};
   Double_t energy_range[2] = {0.500,2.500}; //GeV

   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
         40,phi_range[0],phi_range[1]);
   TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
         40,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy = new TEfficiency("effEnergy","f_{bg} Vs E;E;f_{bg}",
         40,energy_range[0],energy_range[1]);

   TEfficiency* fbgThrownEnergy = new TEfficiency("fbgThrownEnergy","f_{bg} Vs E thrown;E;f_{bg}",
         40,energy_range[0],energy_range[1]);

   TEfficiency* DetEffEnergy = new TEfficiency("DetEffEnergy","Detector Efficiency vs E;E;#epsilon",
         40,energy_range[0],energy_range[1]);
   TEfficiency* DetEffEnergyTheta = new TEfficiency("DetEffEnergyTheta","Detector Efficiency vs E and Theta;E;#theta",
         40,energy_range[0],energy_range[1],
         40,theta_range[0],theta_range[1]);

   TEfficiency* DetEffThetaPhi = new TEfficiency("DetEffThetaPhi","Detector Efficiency vs #theta and #phi;#theta;#phi",
         40,theta_range[0],theta_range[1],40,phi_range[0],phi_range[1]);
   TEfficiency* DetEffThetaPhi2 = new TEfficiency("DetEffThetaPhi2","Detector Efficiency vs #theta and #phi in BigCal Coords;#theta;#phi",
         40,theta_range[0],theta_range[1],40,phi_range[0],phi_range[1]);

   TH1F* hEnergy1 = new TH1F("hEnergy1","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy2 = new TH1F("hEnergy2","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy3 = new TH1F("hEnergy3","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy4 = new TH1F("hEnergy4","E;E;#epsilon", 40,energy_range[0],energy_range[1]);

   TH1F* hGammaE1 = new TH1F("hGammaE1","Gamma;E;", 40,energy_range[0],energy_range[1]);
   TH1F* hElectronE1 = new TH1F("hElectronE1","Electron;E;", 40,energy_range[0],energy_range[1]);
   TH1F* hPionE1 = new TH1F("hPionE1","Pion;E;", 40,energy_range[0],energy_range[1]);

   TH1F* hTheta1 = new TH1F("hTheta1","theta;#theta;#epsilon", 40,theta_range[0],theta_range[1]);
   TH1F* hTheta2 = new TH1F("hTheta2","theta;#theta;#epsilon", 40,theta_range[0],theta_range[1]);

   TH1F* hPhi1 = new TH1F("hPhi1","phi;#phi;", 40,phi_range[0],phi_range[1]);
   TH1F* hPhi2 = new TH1F("hPhi2","phi;#phi;", 40,phi_range[0],phi_range[1]);
   // ------------------------------------------------

   Int_t nEvents = ch1->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      ch1->GetEntry(iEvent);
      ch2->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "run  :       " << trig->fRunNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;
      dis->fRunNumber         = traj->fRunNumber;
      runnum                  = traj->fRunNumber;
      fDisEvent->fEventNumber = traj->fEventNumber;
      ev                      = traj->fEventNumber;
      stdcut                  = 0;
      dis->fHelicity          = traj->fHelicity;
      dis->fx                 = traj->GetBjorkenX();
      dis->fW                 = TMath::Sqrt(traj->GetInvariantMassSquared())/1000.0;
      dis->fQ2                = traj->GetQSquared()/1000000.0;
      dis->fTheta             = traj->GetTheta();
      dis->fPhi               = traj->GetPhi();
      dis->fEnergy            = traj->GetEnergy()/1000.0;
      dis->fBeamEnergy        = traj->fBeamEnergy/1000.0;
      //dis->fGroup             = 0;//traj->fSubDetector;
      //dis->fIsGood            = false;
      //dis->fIsGood            = traj->fIsGood;
      Int_t ibl = bcgeo->GetBlockNumber(traj->fCluster.fiPeak,traj->fCluster.fjPeak);

      passed                      = false;
      isDISEvent                  = false;
      //InSANEParticle * thrownPart = (InSANEParticle*)(*(events->MC->fThrownParticles))[0];
      //if(thrownPart->GetPdgCode() == 11) isDISEvent = true; 

      //if( TMath::Abs(traj->fCluster->fYMoment -30) <30 )
      if( TMath::Abs(traj->fCluster->fYMoment) <100 )
         if( TMath::Abs(traj->fCluster->fXMoment) <52 ){
            if( events->CLUSTER->fNClusters == 1)
               if( trig->IsBETA2Event() )   {
                  hEnergy3->Fill(dis->fEnergy);
                  //if( !traj->fIsNoisyChannel) if( traj->fIsGood) if( dis->fW > 1.0 ){
                  if(true){
                     //if( !(bcgeo->IsBadBlock(ibl)) )
                     //if( traj->fEnergy > E_min )
                     if( traj->fNCherenkovElectrons > 0.3 && traj->fNCherenkovElectrons < 1.5 )
                        if(TMath::Abs(traj->fCherenkovTDC) < 20) {
                           passed = true;
                           hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
                           //if(thrownPart->GetPdgCode() == 11) hEnergy1->Fill(dis->fEnergy);
                           //else hEnergy2->Fill(dis->fEnergy);
                           hEnergy4->Fill(dis->fEnergy);
                           hElectronE1->Fill(dis->fEnergy);
                           hTheta1->Fill(dis->fTheta/degree);
                           hPhi1->Fill(dis->fPhi/degree);
                        } 
                     //if( traj->fNCherenkovElectrons < 0.1 )
                     //   if(TMath::Abs(traj->fCherenkovTDC) > 30) {
                     //      hGammaE1->Fill(dis->fEnergy);
                     //      hTheta2->Fill(dis->fTheta/degree);
                     //      hPhi2->Fill(dis->fPhi/degree);
                     //   }

                  }
               }

            hPhiVsTheta1->Fill(dis->fTheta/degree,dis->fPhi/degree);
            effPhi->Fill(passed,traj->fPhi/degree);
            effTheta->Fill(passed,traj->fTheta/degree);
            effEnergy->Fill(passed,dis->fEnergy);

            if( events->CLUSTER->fNClusters == 2)
               if( trig->IsBETA2Event() || trig->IsPi0Event() )   {
                  if( traj->fNCherenkovElectrons < 0.1 )
                     if(TMath::Abs(traj->fCherenkovTDC) > 20) {
                        hGammaE1->Fill(dis->fEnergy);
                        hTheta2->Fill(dis->fTheta/degree);
                        hPhi2->Fill(dis->fPhi/degree);
                     }
               }

         }

   }


   // ------------------------------------------------------------
   // Plot asymmetries
   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 
   c->Divide(3,2);

   c->cd(1);
   hEnergy1->SetLineWidth(2);
   hEnergy2->SetLineWidth(2);
   hEnergy3->SetLineWidth(2);
   hEnergy4->SetLineWidth(2);
   hEnergy1->SetLineColor(4);
   hEnergy2->SetLineColor(2);
   hEnergy4->SetLineColor(kGreen-4);

   hEnergy3->Draw();
   hEnergy2->Draw("same");
   hEnergy1->Draw("same");
   hEnergy4->Draw("same");
   TLegend * leg1 = new TLegend(0.5,0.6,0.9,0.9);
   leg1->AddEntry(hEnergy3,"All Events with BETA2","l");
   leg1->AddEntry(hEnergy1,"Pion Events passing cuts","l");
   leg1->AddEntry(hEnergy2,"DIS  Events passing cuts","l");
   leg1->AddEntry(hEnergy4,"All  Events passing cuts","l");
   leg1->Draw();

   c->cd(2);
   hPhiVsTheta1->Draw("colz");

   c->cd(3);
   effEnergy->Draw();
   fbgThrownEnergy->SetLineColor(2);
   fbgThrownEnergy->Draw("same");

   c->cd(4);
   gPad->SetLogy(true);
   hGammaE1->SetLineColor(2);
   hElectronE1->SetLineColor(1);
   hGammaE1->Scale(1.0/(LiveTime*Qtotal));
   hElectronE1->Scale(1.0/(LiveTime*Qtotal));
   hElectronE1->Draw();
   hElectronE1->SetMinimum(0.5);
   hGammaE1->Draw("same");
   //DetEffEnergyTheta->Draw("colz");
   //DetEffThetaPhi->Draw("colz");
   //DetEffThetaPhi2->Draw("colz");

   c->cd(5);
   gPad->SetLogy(true);
   hTheta2->SetLineColor(2);
   hTheta1->SetLineColor(1);
   hTheta2->Scale(1.0/(LiveTime*Qtotal));
   hTheta1->Scale(1.0/(LiveTime*Qtotal));
   hTheta1->Draw();
   hTheta1->SetMinimum(0.5);
   hTheta2->Draw("same");

   c->cd(6);
   gPad->SetLogy(true);
   hPhi2->SetLineColor(2);
   hPhi1->SetLineColor(1);
   hPhi2->Scale(1.0/(LiveTime*Qtotal));
   hPhi1->Scale(1.0/(LiveTime*Qtotal));
   hPhi1->Draw();
   hPhi1->SetMinimum(0.5);
   hPhi2->Draw("same");

   c->SaveAs(Form("results/f_pairs_para59_data_%d.png",fileVersion));

   //if(writeRun) rman->WriteRun();
   ////if(writeRun) gROOT->ProcessLine(".q");

   ////delete events;
   //delete traj;
   //delete trig;
   //delete dis;

   //ch2->StartViewer();

   return(0);
}
