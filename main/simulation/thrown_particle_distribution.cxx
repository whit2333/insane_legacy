Int_t thrown_particle_distribution(Int_t runNumber = 9400 ){

   rman->SetRun(runNumber);

   InSANERun * run = rman->GetCurrentRun();

   SANEEvents * events              = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   BETAG4MonteCarloEvent * mcevent  = (BETAG4MonteCarloEvent*)(events->MC);
   InSANEParticle * thrownEvent = new InSANEParticle();
   TTree * t                        = events->fTree;
   

   // Histograms
   TH2F * hNThrownVNTrackerplane = new TH2F("hNThrownVNTrackerplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   TH2F * hNThrownVNBigcalplane = new TH2F("hNThrownVNBigcalplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   TH2F * hRaster = new TH2F("hRaster","Raster;x;y",100,-2.5,2.5,100,-2.5,2.5);

   /// Event Loop.
   Int_t nevents = t->GetEntries();
   std::cout << " Tree has " << nevents << "\n";
   for(int ievent = 0; ievent < nevents ; ievent++){

      thrownEvent = (InSANEParticle*)(*(events->MC->fThrownParticles))[0];      
      hRaster->Fill(thrownEvent->Vx(),thrownEvent->Vy());
      hNThrownVNTrackerplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fTrackerPlaneHits->GetEntries());
      hNThrownVNBigcalplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fBigcalPlaneHits->GetEntries());

   }// Event loop end 

   TCanvas * c = new TCanvas("particles_thrown","thrown_particle_distribution");
   TH1F * h1 = 0;
   TH2F * h2 = 0;

   c->Divide(3,4);
   c->cd(1);
   t->Draw("fNumberOfParticlesThrown:fThrownParticles.fPdgCode","","box");

   c->cd(2);
   t->Draw("fThrownParticles.fVy:fThrownParticles.fVx","","colz");

   c->cd(3);
   t->Draw("fThrownParticles.fVy:fThrownParticles.fVz","","colz");

   c->cd(4);
   TLegend * leg4 = new TLegend(0.7,0.7,0.9,0.9);
   gPad->SetLogy(true);
   t->Draw("fThrownParticles.fE*1000>>thrownE(100,0,3500)","","goff");
   h1 = (TH1F*)gROOT->FindObject("thrownE");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
      leg4->AddEntry(h1,"thrown","l");
   }

   t->Draw("fBigcalPlaneHits.fEnergy>>bcHitsE(100,0,3500)","","goff");
   h1 = (TH1F*)gROOT->FindObject("bcHitsE");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
      leg4->AddEntry(h1,"bigcal Plane","l");
   }

   t->Draw("fTrackerPlaneHits.fEnergy>>tkHitsE(100,0,3500)","","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHitsE");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
      leg4->AddEntry(h1,"tracker Plane","l");
   }
   t->Draw("bigcalClusters.fTotalE>>bcClusterE(100,0,3500)","","goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterE");
   if(h1) {
      h1->SetLineColor(3);
      h1->Draw("same");
      leg4->AddEntry(h1,"cluster","l");
   }
   t->Draw("bigcalClusters.GetCorrectedEnergy()>>bcClusterE2(100,0,3500)","","goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterE2");
   if(h1) {
      h1->SetLineColor(kRed - 9);
      h1->Draw("same");
      leg4->AddEntry(h1,"cluster corrected","l");
   }
   leg4->Draw();

   c->cd(5);
   gPad->SetLogy(true);
   TLegend * leg5 = new TLegend(0.7,0.7,0.9,0.9);
   t->Draw("fThrownParticles.Theta()/0.0175>>thrownTheta(100,20,60)","","goff");
   h1 = (TH1F*)gROOT->FindObject("thrownTheta");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
      leg5->AddEntry(h1,"thrown","l");
   }

   t->Draw("fBigcalPlaneHits.fPosition.Theta()/0.0175>>bcHitsTheta(100,20,60)","fBigcalPlaneHits.fEnergy>500","goff");
   h1 = (TH1F*)gROOT->FindObject("bcHitsTheta");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
      leg5->AddEntry(h1,"bigcal Plane with E>500","l");
   }

   t->Draw("bigcalClusters.GetTheta()/0.0175>>bcClusterTheta(100,20,60)","bigcalClusters.fTotalE>500","goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterTheta");
   if(h1) {
      h1->SetLineColor(kMagenta);
      h1->Draw("same");
      leg5->AddEntry(h1,"cluster with E>500","l");
   }

   t->Draw("fTrackerPlaneHits.fPosition.Theta()/0.0175>>tkHitsTheta(100,20,60)","","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHitsTheta");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
      leg5->AddEntry(h1,"tracker Plane","l");
   }
   t->Draw("fTrackerPlaneHits.fPosition.Theta()/0.0175>>tkHitsTheta2(100,20,60)","fTrackerPlaneHits.fEnergy>100","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHitsTheta2");
   if(h1) {
      h1->SetLineColor(4);
      h1->SetFillStyle(3004);
      h1->SetFillColor(4);
      h1->Draw("same");
      leg5->AddEntry(h1,"tracker Plane E>100","l");
   }
   t->Draw("fTrackerPlaneHits.fPosition.Theta()/0.0175>>tkHitsTheta3(100,20,60)","fTrackerPlaneHits.fEnergy>400","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHitsTheta3");
   if(h1) {
      h1->SetLineColor(4);
      h1->SetFillStyle(3005);
      h1->SetFillColor(4);
      h1->Draw("same");
      leg5->AddEntry(h1,"tracker Plane E>400","l");
   }

   t->Draw("fTrackerPlane2Hits.fPosition.Theta()/0.0175>>tkHits2Theta(100,20,60)","","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHits2Theta");
   if(h1) {
      h1->SetLineColor(3);
      h1->Draw("same");
      leg5->AddEntry(h1,"tracker Plane -2","l");
   }
   t->Draw("fTrackerPlane2Hits.fPosition.Theta()/0.0175>>tkHits2Theta2(100,20,60)","fTrackerPlane2Hits.fEnergy>100","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHits2Theta2");
   if(h1) {
      h1->SetLineColor(3);
      h1->SetFillStyle(3004);
      h1->SetFillColor(3);
      h1->Draw("same");
      leg5->AddEntry(h1,"tracker Plane -2 E>100","l");
   }
   t->Draw("fTrackerPlane2Hits.fPosition.Theta()/0.0175>>tkHits2Theta3(100,20,60)","fTrackerPlane2Hits.fEnergy>400","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHits2Theta3");
   if(h1) {
      h1->SetLineColor(3);
      h1->SetFillStyle(3005);
      h1->SetFillColor(3);
      h1->Draw("same");
      leg5->AddEntry(h1,"tracker Plane -2 E>400","l");
   }
   leg5->Draw();

   c->cd(6);
   gPad->SetLogy(true);
   t->Draw("TMath::ATan2(TMath::Sin(fThrownParticles.Phi()),TMath::Cos(fThrownParticles.Phi()))/0.0175>>thrownPhi(200,-90,90)","","goff");
//   t->Draw("fThrownParticles.Phi()/0.0175>>thrownPhi(200,-90,390)","","goff");
   h1 = (TH1F*)gROOT->FindObject("thrownPhi");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
   }

   t->Draw("bigcalClusters.GetPhi()/0.0175>>bcClusterPhi(100,-90,90)","bigcalClusters.fTotalE>500","goff");
   h1 = (TH1F*)gROOT->FindObject("bcClusterPhi");
   if(h1) {
      h1->SetLineColor(kMagenta);
      h1->Draw("same");
      //leg5->AddEntry(h1,"cluster with E>500","l");
   }

   t->Draw("fBigcalPlaneHits.fPosition.Phi()/0.0175>>bcHitsPhi(200,-90,90)","","goff");
   h1 = (TH1F*)gROOT->FindObject("bcHitsPhi");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   t->Draw("fBigcalPlaneHits.fPosition.Phi()/0.0175>>bcHitsPhi2(200,-90,90)","fBigcalPlaneHits.fEnergy>21","goff");
   h1 = (TH1F*)gROOT->FindObject("bcHitsPhi2");
   if(h1) {
      //h1->SetFillStyle(3001);
      //h1->SetFillColor(2);
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   t->Draw("fTrackerPlaneHits.fPosition.Phi()/0.0175>>tkHits2Phi3(100,-90,90)","fTrackerPlane2Hits.fEnergy>400","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHits2Phi3");
   if(h1) {
      h1->SetLineColor(3);
      //h1->SetFillStyle(3005);
      //h1->SetFillColor(3);
      h1->Draw("same");
      //leg5->AddEntry(h1,"tracker Plane -2 E>400","l");
   }


   c->cd(7);
   t->Draw("bigcalClusters.fXStdDeviation>>bcXstdDev(100,0,3)","bigcalClusters.fTotalE>500","");
   t->Draw("bigcalClusters.fYStdDeviation>>bcYstdDev(100,0,3)","bigcalClusters.fTotalE>500","");
   h1 = (TH1F*)gROOT->FindObject("bcXstdDev");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw();
   }
   h1 = (TH1F*)gROOT->FindObject("bcYstdDev");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
   }
   
   c->cd(8);
   t->Draw("bigcalClusters.fXSkewness>>bcXskew(100,-9,9)","bigcalClusters.fTotalE>500","");
   t->Draw("bigcalClusters.fYSkewness>>bcYskew(100,-9,9)","bigcalClusters.fTotalE>500","");
   h1 = (TH1F*)gROOT->FindObject("bcXskew");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw();
   }
   h1 = (TH1F*)gROOT->FindObject("bcYskew");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
   }
//    hNThrownVNBigcalplane->Draw("colz");
//    //t->Draw("fTrackerPlaneHits.fLocalPosition.fY:fTrackerPlaneHits.fLocalPosition.fX","","colz"); 
// 
//    c->cd(8);
//    t->Draw("fBigcalPlaneHits.fLocalPosition.fY:fBigcalPlaneHits.fLocalPosition.fX","","colz"); 
// 
    c->cd(9);
    t->Draw("bigcalClusters.fNNonZeroBlocks","bigcalClusters.fTotalE>500"); 
// 
    c->cd(10);
   gPad->SetLogy(true);
   t->Draw("fThrownParticles.fE>>helectronsEThrown(100,0,5.0)","fThrownParticles.fPdgCode==11","goff");
   h1 = (TH1F*)gROOT->FindObject("helectronsEThrown");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
   }
   t->Draw("fThrownParticles.fE>>hpionsEThrown(100,0,5.0)","fThrownParticles.fPdgCode==111","goff");
   h1 = (TH1F*)gROOT->FindObject("hpionsEThrown");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   //c->SaveAs(Form("plots/%d/thrown_particle_distribution.png",runNumber));
   c->SaveAs(Form("plots/%d/thrown_particle_distribution.png",runNumber));
   c->SaveAs(Form("plots/%d/thrown_particle_distribution.pdf",runNumber));

   //t->StartViewer();

   return(0);
}

