Int_t f_pairs_para59(Int_t runnumber = 6050, Int_t fileVersion = 21, Double_t E_min = 1000.0 ) {

   gROOT->LoadMacro("background/eplus_eminus_ratio.cxx"); 

   // function of theta,E' (deg,GeV)
   TF2 * R_ratio = GetR_PlusMinus();

   // Simulated runs with allNH3 event generator
   //aman->fRunQueue->push_back(300);
   //aman->fRunQueue->push_back(321);
   //aman->fRunQueue->push_back(322);
   //aman->fRunQueue->push_back(323);
   //aman->fRunQueue->push_back(324);
   //aman->fRunQueue->push_back(325);
   //aman->fRunQueue->push_back(326);
   //aman->fRunQueue->push_back(327);
   //aman->fRunQueue->push_back(328);
   //aman->fRunQueue->push_back(329);

   aman->BuildQueue2("lists/para/MC/allNH3_1.txt");

   TChain * ch1 = aman->BuildChain("Tracks");
   TChain * ch2 = aman->BuildChain("betaDetectors1");

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   bcgeo->LoadBadBlocks(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber));

   ch1->SetBranchAddress("trajectory",&traj);
   ch1->SetBranchAddress("triggerEvent",&trig);

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetBranches(ch2);
   events->SetClusterBranches(ch2);
   ch2->BuildIndex("fRunNumber","fEventNumber");

   Bool_t   passed          = false;  // passes detector cuts
   Bool_t   isDISEvent      = false;  // is DIS electron event
   Double_t theta_range[2]  = {25.0,55.0};   //Degrees
   Double_t phi_range[2]    = {-70.0,70.0};
   Double_t energy_range[2] = {0.500,2.500}; //GeV


   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta3 = new TH2F("hPhiVsTheta3","Phi vs Theta with cuts;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
         40,phi_range[0],phi_range[1]);
   TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
         40,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy = new TEfficiency("effEnergy","f_{bg} Vs E;E;f_{bg}",
         40,energy_range[0],energy_range[1]);

   TEfficiency* fbgThrownEnergy = new TEfficiency("fbgThrownEnergy","f_{bg} Vs E thrown;E;f_{bg}",
         40,energy_range[0],energy_range[1]);

   TEfficiency* DetEffEnergy = new TEfficiency("DetEffEnergy","Detector Efficiency vs E;E;#epsilon",
         40,energy_range[0],energy_range[1]);
   TEfficiency* DetEffEnergyTheta = new TEfficiency("DetEffEnergyTheta","Detector Efficiency vs E and Theta;E;#theta",
         40,energy_range[0],energy_range[1],
         40,theta_range[0],theta_range[1]);

   TEfficiency* DetEffThetaPhi = new TEfficiency("DetEffThetaPhi","Detector Efficiency vs #theta and #phi;#theta;#phi",
         40,theta_range[0],theta_range[1],40,phi_range[0],phi_range[1]);
   TEfficiency* DetEffThetaPhi2 = new TEfficiency("DetEffThetaPhi2","Detector Efficiency vs #theta and #phi in BigCal Coords;#theta;#phi",
         40,theta_range[0],theta_range[1],40,phi_range[0],phi_range[1]);

   TH1F* hEnergy1 = new TH1F("hEnergy1","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy2 = new TH1F("hEnergy2","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy3 = new TH1F("hEnergy3","E;E;#epsilon", 40,energy_range[0],energy_range[1]);
   TH1F* hEnergy4 = new TH1F("hEnergy4","E;E;#epsilon", 40,energy_range[0],energy_range[1]);

   TH1F* hTheta1 = new TH1F("hTheta1","theta;#theta;#epsilon", 40,theta_range[0],theta_range[1]);
   TH1F* hTheta2 = new TH1F("hTheta2","theta;#theta;#epsilon", 40,theta_range[0],theta_range[1]);

   TH1F* hPhi1 = new TH1F("hPhi1","phi;#phi;", 40,phi_range[0],phi_range[1]);
   TH1F* hPhi2 = new TH1F("hPhi2","phi;#phi;", 40,phi_range[0],phi_range[1]);

   TH1F* hGammaE1 = new TH1F("hGammaE1","Gamma;E;", 40,energy_range[0],energy_range[1]);
   TH1F* hElectronE1 = new TH1F("hElectronE1","Electron;E;", 40,energy_range[0],energy_range[1]);
   TH1F* hPionE1 = new TH1F("hPionE1","Pion;E;", 40,energy_range[0],energy_range[1]);

   TH1F* hGTheta1 = new TH1F("hTheta1","theta;#theta;#epsilon", 40,theta_range[0],theta_range[1]);
   TH1F* hGTheta2 = new TH1F("hTheta2","theta;#theta;#epsilon", 40,theta_range[0],theta_range[1]);

   TH1F* hGPhi1 = new TH1F("hPhi1","phi;#phi;", 40,phi_range[0],phi_range[1]);
   TH1F* hGPhi2 = new TH1F("hPhi2","phi;#phi;", 40,phi_range[0],phi_range[1]);

   // ------------------------------------------------
   Int_t nEvents = ch2->GetEntries();
   //for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){
   //   ch2->GetEntry(iEvent);
   //   if(events->TRIG->fEventNumber < 1001) continue;

   //   InSANEParticle * thrownPart = (InSANEParticle*)(*(events->MC->fThrownParticles))[0];
   //   Double_t thrownPhi = thrownPart->Phi();
   //   if(thrownPhi > TMath::Pi()) thrownPhi = thrownPhi - 2.0*TMath::Pi();

   //   hPhiVsTheta3->Fill(thrownPart->Theta()/degree,thrownPhi/degree);
   //}

   // ------------------------------------------------
   nEvents = ch1->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      ch1->GetEntry(iEvent);
      ch2->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      if(trig->fEventNumber < 1001) continue;
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "run  :       " << trig->fRunNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;
      dis->fRunNumber         = traj->fRunNumber;
      runnum                  = traj->fRunNumber;
      fDisEvent->fEventNumber = traj->fEventNumber;
      ev                      = traj->fEventNumber;
      stdcut                  = 0;
      dis->fHelicity          = traj->fHelicity;
      dis->fx                 = traj->GetBjorkenX();
      dis->fW                 = TMath::Sqrt(traj->GetInvariantMassSquared())/1000.0;
      dis->fQ2                = traj->GetQSquared()/1000000.0;
      dis->fTheta             = traj->GetTheta();
      dis->fPhi               = traj->GetPhi();
      dis->fEnergy            = traj->GetEnergy()/1000.0;
      dis->fBeamEnergy        = traj->fBeamEnergy/1000.0;
      //dis->fGroup             = 0;//traj->fSubDetector;
      //dis->fIsGood            = false;
      //dis->fIsGood            = traj->fIsGood;
      Int_t ibl = bcgeo->GetBlockNumber(traj->fCluster.fiPeak,traj->fCluster.fjPeak);

      passed                      = false;
      isDISEvent                  = false;
      InSANEParticle * thrownPart = (InSANEParticle*)(*(events->MC->fThrownParticles))[0];
      Double_t thrownPhi = thrownPart->Phi();
      if(thrownPhi > TMath::Pi()) thrownPhi = thrownPhi - 2.0*TMath::Pi();

      //hPhiVsTheta3->Fill(thrownPart->Theta()/degree,thrownPhi/degree);

      if(thrownPart->GetPdgCode() == 11) isDISEvent = true; 

      //if( TMath::Abs(traj->fCluster->fYMoment -30) <30 )
      if( TMath::Abs(traj->fCluster->fYMoment) <100 )
         if( TMath::Abs(traj->fCluster->fXMoment) <52 ){
            if( events->CLUSTER->fNClusters == 1) if( trig->IsBETA2Event() )   {
               hEnergy3->Fill(dis->fEnergy);
               //if( !traj->fIsNoisyChannel)
               //if( traj->fIsGood)
               //if( dis->fW > 1.0 )
               if( traj->fNCherenkovElectrons > 0.1 && traj->fNCherenkovElectrons < 1.5 )
                  if( traj->fNCherenkovElectrons > 0.3 )
                     //if( !(bcgeo->IsBadBlock(ibl)) )
                     //if( traj->fEnergy > E_min )
                     if(TMath::Abs(traj->fCherenkovTDC) < 20) 
                        if(true){
                           passed = true;
                           hPhiVsTheta2->Fill(traj->fTheta,traj->fPhi);
                           if(thrownPart->GetPdgCode() == 11){
                              hEnergy1->Fill(dis->fEnergy);
                              hTheta1->Fill(dis->fTheta/degree);
                              hPhi1->Fill(dis->fPhi/degree);
                           } else {
                              hEnergy2->Fill(dis->fEnergy);
                              hTheta2->Fill(dis->fTheta/degree);
                              hPhi2->Fill(dis->fPhi/degree);
                           }
                           hEnergy4->Fill(dis->fEnergy);
                        } 

               //if( traj->fEnergy > E_min )
               if( traj->fNCherenkovElectrons > 0.3 && traj->fNCherenkovElectrons < 1.5 )
                  if(TMath::Abs(traj->fCherenkovTDC) < 20) {
                     hElectronE1->Fill(dis->fEnergy);
                     hGTheta1->Fill(dis->fTheta/degree);
                     hGPhi1->Fill(dis->fPhi/degree);
                  } 
               //if( traj->fNCherenkovElectrons < 0.1 )
               //   if(TMath::Abs(traj->fCherenkovTDC) > 30) {
               //      hGammaE1->Fill(dis->fEnergy);
               //      hGTheta2->Fill(dis->fTheta/degree);
               //      hGPhi2->Fill(dis->fPhi/degree);
               //   }
            }

            hPhiVsTheta1->Fill(dis->fTheta/degree,dis->fPhi/degree);
            effPhi->Fill(passed,traj->fPhi/degree);
            effTheta->Fill(passed,traj->fTheta/degree);
            effEnergy->Fill(passed,dis->fEnergy);
            fbgThrownEnergy->Fill(passed,thrownPart->Energy());

            if( events->CLUSTER->fNClusters == 2)
               if( trig->IsBETA2Event() || trig->IsPi0Event() )   {
                  if( traj->fNCherenkovElectrons < 0.1 )
                     if(TMath::Abs(traj->fCherenkovTDC) > 30) {
                        hGammaE1->Fill(dis->fEnergy);
                        hGTheta2->Fill(dis->fTheta/degree);
                        hGPhi2->Fill(dis->fPhi/degree);
                     }
               }

            if(isDISEvent) {
               DetEffThetaPhi->Fill(passed,thrownPart->Theta()/degree,thrownPhi/degree);
               DetEffEnergy->Fill(passed,thrownPart->Energy());
               DetEffEnergyTheta->Fill(passed,thrownPart->Energy(),thrownPart->Theta()/degree);

               DetEffThetaPhi2->Fill(passed,traj->fCluster.GetTheta()/degree,traj->fCluster.GetPhi()/degree);
            }
         }

   }


   // ------------------------------------------------------------
   // Plot asymmetries
   TCanvas * c = new TCanvas("combine_runs","combine_runs"); 
   c->Divide(4,2);

   c->cd(1);
   //DetEffEnergyTheta->Draw("colz");
   //DetEffThetaPhi->Draw("colz");
   DetEffThetaPhi2->Draw("colz");

   c->cd(2);
   hPhiVsTheta3->Draw("box");
   hPhiVsTheta1->Draw("colz,same");

   c->cd(3);
   gPad->SetLogy(true);
   hEnergy1->SetLineWidth(2);
   hEnergy2->SetLineWidth(2);
   hEnergy3->SetLineWidth(2);
   hEnergy1->SetLineColor(4);
   hEnergy2->SetLineColor(2);
   hEnergy4->SetLineColor(kGreen-4);
   hEnergy3->Draw();
   hEnergy2->Draw("same");
   hEnergy1->Draw("same");
   hEnergy4->Draw("same");
   TLegend * leg1 = new TLegend(0.5,0.6,0.9,0.9);
   leg1->AddEntry(hEnergy3,"All Events with BETA2","l");
   leg1->AddEntry(hEnergy1,"DIS Events passing cuts","l");
   leg1->AddEntry(hEnergy2,"Pion  Events passing cuts","l");
   leg1->AddEntry(hEnergy4,"All  Events passing cuts","l");
   leg1->Draw();

   c->cd(4);
   TH1F * hf_bg = (TH1F*)hEnergy2->Clone();
   hf_bg->Divide(hEnergy1);
   hf_bg->Scale(0.5);
   hf_bg->Draw();

   TLegend * leg = new TLegend(0.35,0.6,0.9,0.9);
   int Npts     = hEnergy1->GetXaxis()->GetNbins();
   TGraph * g0  = new TGraph(Npts);
   TGraph * g1  = new TGraph(Npts);
   TGraph * g2  = new TGraph(Npts);
   TGraph * g3  = new TGraph(Npts);
   for(int i = 0; i<Npts;i++){
      double Eprime = hEnergy1->GetXaxis()->GetBinCenter(i);
      g0->SetPoint(i,Eprime,R_ratio->Eval(25.0,Eprime));
      g1->SetPoint(i,Eprime,R_ratio->Eval(30.0,Eprime));
      g2->SetPoint(i,Eprime,R_ratio->Eval(40.0,Eprime));
      g3->SetPoint(i,Eprime,R_ratio->Eval(50.0,Eprime));
   }
   g0->SetLineColor(kGreen-4);
   g0->SetLineWidth(2);
   g1->SetLineColor(2);
   g1->SetLineWidth(2);
   g2->SetLineColor(1);
   g2->SetLineWidth(2);
   g3->SetLineColor(4);
   g3->SetLineWidth(2);
   leg->AddEntry(g0,"Fersch #theta=25 deg","l");
   leg->AddEntry(g1,"Fersch #theta=30 deg","l");
   leg->AddEntry(g2,"Fersch #theta=40 deg","l");
   leg->AddEntry(g3,"Fersch #theta=50 deg","l");
   g0->Draw("l");
   g1->Draw("l");
   g2->Draw("l");
   g3->Draw("l");
   leg->Draw();

   //effEnergy->Draw();
   //fbgThrownEnergy->SetLineColor(2);
   //fbgThrownEnergy->Draw("same");


   c->cd(5);
   hTheta1->SetLineWidth(2);
   hTheta2->SetLineWidth(2);
   hTheta1->SetLineColor(4);
   hTheta2->SetLineColor(2);
   hTheta1->Draw();
   hTheta2->Draw("same");

   c->cd(6);
   TH1F * hf_bg2 = (TH1F*)hTheta2->Clone();
   hf_bg2->Divide(hTheta1);
   hf_bg2->Draw();

   c->cd(7);
   hPhi1->SetLineWidth(2);
   hPhi2->SetLineWidth(2);
   hPhi1->SetLineColor(4);
   hPhi2->SetLineColor(2);
   hPhi1->Draw();
   hPhi2->Draw("same");

   c->cd(8);
   TH1F * hf_bg3 = (TH1F*)hPhi2->Clone();
   hf_bg3->Divide(hPhi1);
   hf_bg3->Draw();

   c->SaveAs(Form("results/f_pairs_para59_%d.png",fileVersion));
   c->SaveAs(Form("results/f_pairs_para59_%d.pdf",fileVersion));
   c->SaveAs(Form("results/f_pairs_para59_%d.eps",fileVersion));

   //if(writeRun) rman->WriteRun();
   ////if(writeRun) gROOT->ProcessLine(".q");

   ////delete events;
   //delete traj;
   //delete trig;
   //delete dis;
   // 23.0889 seconds per 2M Events at 100nA
   Double_t Qtotal = 23.0889.0*100.0/(1000.0) ; // converts to uC
   Double_t LiveTime = 1.0;

   TCanvas * c2 = new TCanvas("rates","rates"); 
   c2->Divide(3,2);

   c2->cd(4);
   gPad->SetLogy(true);
   hGammaE1->SetLineColor(2);
   hElectronE1->SetLineColor(1);
   hGammaE1->Scale(1.0/(LiveTime*Qtotal));
   hElectronE1->Scale(1.0/(LiveTime*Qtotal));
   hElectronE1->Draw();
   hElectronE1->SetMinimum(0.5);
   hGammaE1->Draw("same");

   c2->cd(5);
   gPad->SetLogy(true);
   hGTheta2->SetLineColor(2);
   hGTheta1->SetLineColor(1);
   hGTheta2->Scale(1.0/(LiveTime*Qtotal));
   hGTheta1->Scale(1.0/(LiveTime*Qtotal));
   hGTheta1->Draw();
   hGTheta1->SetMinimum(0.5);
   hGTheta2->Draw("same");

   c2->cd(6);
   gPad->SetLogy(true);
   hGPhi2->SetLineColor(2);
   hGPhi1->SetLineColor(1);
   hGPhi2->Scale(1.0/(LiveTime*Qtotal));
   hGPhi1->Scale(1.0/(LiveTime*Qtotal));
   hGPhi1->Draw();
   hGPhi1->SetMinimum(0.5);
   hGPhi2->Draw("same");

   c2->SaveAs(Form("results/f_pairs_para59_rates_%d.png",fileVersion));

   //ch2->StartViewer();

   return(0);
}
