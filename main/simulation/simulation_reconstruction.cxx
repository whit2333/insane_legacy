Int_t simulation_reconstruction(Int_t runNumber = 9400 ){

   rman->SetRun(runNumber);

   InSANERun * run = rman->GetCurrentRun();

   SANEEvents * events              = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   BETAG4MonteCarloEvent * mcevent  = (BETAG4MonteCarloEvent*)(events->MC);
   InSANEParticle * thrownEvent = new InSANEParticle();
   TTree * t                        = events->fTree;
   ANNClusterRecon fANNClusterRecon;
   BIGCALCluster * aCluster = 0;
   BIGCALCluster cluster1;

   // Histograms
   TH1F * hEnergyThrown  = new TH1F("Ethrown","Energy Thrown",100,0,4000);
   TH1F * hEnergyRecon1   = new TH1F("Erecon","Energy Recon",100,0,4000);
   TH1F * hEnergyRecon2   = new TH1F("Erecon2","Energy Recon",100,0,4000);
   TH1F * hEnergyRecon3   = new TH1F("Erecon3","Energy Recon",100,0,4000);

   TH2F * hThetaPhiRecon0   = new TH2F("thetaphiRecon0","thetaphi",100,20,60,100,-80,80);
   TH2F * hThetaPhiRecon1   = new TH2F("thetaphiRecon1","thetaphi",100,20,60,100,-80,80);
   TH2F * hThetaPhiRecon2   = new TH2F("thetaphiRecon2","thetaphi",100,20,60,100,-80,80);
   TH2F * hThetaPhiRecon3   = new TH2F("thetaphiRecon3","thetaphi",100,20,60,100,-80,80);

   TH1F * hDeltaRecon  = new TH1F("DeltaE","#Delta E",100,-1000,1000);

   // Event Loop.
   Int_t nevents = t->GetEntries();
   std::cout << " Tree has " << nevents << "\n";
   for(int ievent = 0; ievent < nevents ; ievent++){

      t->GetEntry(ievent);
      fANNClusterRecon.SetBeamEvent(events->BEAM);

      thrownEvent = (InSANEParticle*)(*(events->MC->fThrownParticles))[0];      
      hEnergyThrown->Fill(thrownEvent->Energy()*1000.0);

      hThetaPhiRecon0->Fill(thrownEvent->Theta()/degree,thrownEvent->Phi()/degree);

      for(int i=0;i<events->CLUSTER->fClusters->GetEntries();i++){
         aCluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[i];
         fANNClusterRecon.SetCluster(*aCluster);
         cluster1 = fANNClusterRecon.AsPhoton();
         hEnergyRecon1->Fill(cluster1.GetEnergy());
         hEnergyRecon2->Fill(cluster1.GetCorrectedEnergy());
         hDeltaRecon->Fill(thrownEvent->Energy()*1000.0 - cluster1.GetCorrectedEnergy());
         hThetaPhiRecon1->Fill(thrownEvent->Theta()/degree,thrownEvent->Phi()/degree);
         if( TMath::Abs(aCluster->fCherenkovTDC) <100 ) {
            hEnergyRecon3->Fill(cluster1.GetCorrectedEnergy());
            hThetaPhiRecon3->Fill(thrownEvent->Theta()/degree,thrownEvent->Phi()/degree);
         }
      }

   }// Event loop end 


   TCanvas * c = new TCanvas("simulation_reconstruction","simulation_reconstruction");
   c->Divide(2,1);

   c->cd(1);
   hEnergyThrown->Draw();
   hEnergyRecon1->SetLineColor(2);
   hEnergyRecon1->Draw("same");
   hEnergyRecon2->SetLineColor(4);
   hEnergyRecon2->Draw("same");
   hEnergyRecon3->SetLineColor(8);
   hEnergyRecon3->Draw("same");

   c->cd(2);
   hDeltaRecon->Draw();

   c->SaveAs(Form("plots/%d/simulation_reconstruction.png",runNumber));
   c->SaveAs(Form("plots/%d/simulation_reconstruction.pdf",runNumber));

   c = new TCanvas("simulation_reconstruction","simulation_reconstruction");
   hThetaPhiRecon0->Draw("colz");
   c->SaveAs(Form("plots/%d/simulation_reconstruction_0.png",runNumber));
   c->SaveAs(Form("plots/%d/simulation_reconstruction_0.pdf",runNumber));

   c = new TCanvas("simulation_reconstruction","simulation_reconstruction");
   hThetaPhiRecon1->Draw("colz");
   c->SaveAs(Form("plots/%d/simulation_reconstruction_1.png",runNumber));
   c->SaveAs(Form("plots/%d/simulation_reconstruction_1.pdf",runNumber));

   return(0);
}

