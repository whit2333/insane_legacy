Int_t twodim_background_para47(Int_t aNumber =  20 ){

   //load_style("SingleSquarePlot");
   //gROOT->SetStyle("SingleSquarePlot");


   // -----------------------------------------------------
   // para 4.7 with radiative tail
   //for(int i=1080;i<1099;i++) {
   //   aman->fRunQueue->push_back(i);
   //}
   //for(int i=1229;i<1265;i++) {
   //   aman->fRunQueue->push_back(i);
   //}
   //for(int i=1400;i<1499;i++) {
   //   aman->fRunQueue->push_back(i);
   //}
   for(int i=10100;i<10200;i++) {
      aman->fRunQueue->push_back(i);
   }

   // -----------------------------------------------------


   TChain * ch1 = aman->BuildChain("Tracks");
   TChain * ch2 = aman->BuildChain("betaDetectors1");

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetBranches(ch2);
   events->SetClusterBranches(ch2);
   ch1->BuildIndex("triggerEvent.fRunNumber","triggerEvent.fEventNumber");
   ch2->BuildIndex("fRunNumber","fEventNumber");
   ch1->AddFriend(ch2);

   BETAG4MonteCarloEvent * mcevent  = (BETAG4MonteCarloEvent*)(events->MC);
   InSANEParticle * thrownEvent     = new InSANEParticle();
   
   // Histograms
   //TH2F * hNThrownVNTrackerplane = new TH2F("hNThrownVNTrackerplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   //TH2F * hNThrownVNBigcalplane  = new TH2F("hNThrownVNBigcalplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   //TH2F * hRaster                = new TH2F("hRaster","Raster;x;y",100,-2.5,2.5,100,-2.5,2.5);

   // Event Loop.
   Int_t nevents = ch2->GetEntries();
   std::cout << " chain has " << nevents << "\n";

   //for(int ievent = 0; ievent < nevents ; ievent++){
   //   thrownEvent = (InSANEParticle*)(*(events->MC->fThrownParticles))[0];      
   //   hRaster->Fill(thrownEvent->Vx(),thrownEvent->Vy());
   //   hNThrownVNTrackerplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fTrackerPlaneHits->GetEntries());
   //   hNThrownVNBigcalplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fBigcalPlaneHits->GetEntries());
   //}// Event loop end 


   TCanvas * c = new TCanvas("particles_thrown","thrown_particle_distribution");
   TH1F * h1 = 0;
   TH2F * h2 = 0;

   c->Divide(2,2); 
   // --------------------------------
   // Cuts
   TCut c0        = "TMath::Abs(fCluster.fCherenkovTDC)<25";//&&fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0";
   TCut c1        = "TMath::Abs(fCluster.fCherenkovTDC)<25&&betaDetectors1.fLuciteHodoscopeEvent.fNumberOfTimedTDCHits>0";
   TCut pionEvent = "betaDetectors1.fThrownParticles.fPdgCode==111";
   TCut cerWindow = "fCluster.fCherenkovBestADCSum>0.5&&fCluster.fCherenkovBestADCSum<1.5";
   TCut theta0    = Form("TMath::Abs(track.fTheta/%f-35)<5",degree);
   TCut theta1    = Form("TMath::Abs(track.fTheta/%f-40)<5",degree);
   TCut theta2    = Form("TMath::Abs(track.fTheta/%f-45)<5",degree);
   TCut theta3    = Form("TMath::Abs(track.fTheta/%f-50)<5",degree);
   TCut theta4    = Form("TMath::Abs(track.fTheta/%f-55)<5",degree);

   // ------
   c->cd(1);
   ch1->Draw(Form("track.fTheta/%f:track.fEnergy/1000.0>>hThetaVsEnergy0(20,0.5,2.0,20,32,52)",degree), c1&&cerWindow,"COLZ");

   //ch1->Draw("track.fEnergy/1000.0>>hEnergy0(20,0.5,2.0)", theta0&&c1&&cerWindow,"goff");
   //ch1->Draw("track.fEnergy/1000.0>>hEnergy1(20,0.5,2.0)", theta1&&c1&&cerWindow,"goff");
   //ch1->Draw("track.fEnergy/1000.0>>hEnergy2(20,0.5,2.0)", theta2&&c1&&cerWindow,"goff");
   //ch1->Draw("track.fEnergy/1000.0>>hEnergy3(20,0.5,2.0)", theta3&&c1&&cerWindow,"goff");
   //ch1->Draw("track.fEnergy/1000.0>>hEnergy4(20,0.5,2.0)", theta4&&c1&&cerWindow,"goff");

   // ------
   c->cd(2);
   ch1->Draw(Form("track.fTheta/%f:track.fEnergy/1000.0>>hThetaVsEnergy1(20,0.5,2.0,20,32,52)",degree), c1&&cerWindow&&pionEvent,"COLZ");

   //ch1->Draw("track.fEnergy/1000.0>>hEnergy0Pion(20,0.5,2.0)", theta0&&c1&&cerWindow&&pionEvent,"goff");
   //ch1->Draw("track.fEnergy/1000.0>>hEnergy1Pion(20,0.5,2.0)", theta1&&c1&&cerWindow&&pionEvent,"goff");
   //ch1->Draw("track.fEnergy/1000.0>>hEnergy2Pion(20,0.5,2.0)", theta2&&c1&&cerWindow&&pionEvent,"goff");
   //ch1->Draw("track.fEnergy/1000.0>>hEnergy3Pion(20,0.5,2.0)", theta3&&c1&&cerWindow&&pionEvent,"goff");
   //ch1->Draw("track.fEnergy/1000.0>>hEnergy4Pion(20,0.5,2.0)", theta4&&c1&&cerWindow&&pionEvent,"goff");

   // ------
   c->cd(3);
   TH2F * hPionOverAll = 0;
   TH2F * hAll         = 0;
   h2 = (TH2F*)gROOT->FindObject("hThetaVsEnergy1");
   if(h2) {
      hPionOverAll = (TH2F*)h2->Clone();
      hPionOverAll->Sumw2(true);
   }
   h2 = (TH2F*)gROOT->FindObject("hThetaVsEnergy0");
   if(h2) {
      hAll = (TH2F*)h2->Clone();
      hAll->Sumw2(true);
   }
   hPionOverAll->Divide(hAll);
   hPionOverAll->DrawCopy("COLZ");

   // ------
   c->cd(4);
   hPionOverAll->DrawCopy("LEGO E");

   // --------------------------------
   c->SaveAs(Form("results/background/twodim_background_para47_%d.png",aNumber));
   c->SaveAs(Form("results/background/twodim_background_para47_%d.pdf",aNumber));

   // --------------------------------

   // --------------------------------
   // 2D Fit 
   TF2 *func  = 0;
   TF2 *f2    = new TF2("RPlusMinus2_para47","TMath::Exp([0]+[1]*x+[2]*y)/(1.0+TMath::Exp([0]+[1]*x+[2]*y))",0.5,2.0,30,55);


   TCanvas * c2 = new TCanvas();
   c2->Divide(2,2); 

   // Fit histogram
   hPionOverAll->Fit(f2,"N","goff");
   f2->SetContour(10);

   // -------
   c2->cd(1);
   hPionOverAll->DrawCopy("box");
   f2->DrawCopy("cont1 same");

   // -------
   c2->cd(2);
   func = (TF2*)f2->Clone();
   func->SetMinimum(0.0);
   func->SetMaximum(1.4);
   func->Draw("surf2");
   //h2->GetZaxis()->SetRangeUser(0.0,2.0);
   //h2->Draw("surf2");
   //h2->GetZaxis()->SetRangeUser(0.0,2.0);

   h2 = (TH2F*)hPionOverAll->DrawCopy("SAME,LEGO,E");
   h2->GetZaxis()->SetRangeUser(0.0,1.4);
   //f2->SetRange(0.5,32.0,0.0,2.0,52,2.0);
   //func->GetZaxis()->SetRangeUser(0.0,2.0);
   //f2->Draw("surf3");
   c2->Update();



   // --------------------------------
   c2->SaveAs(Form("results/background/twodim_background_para47_2_%d.png",aNumber));
   c2->SaveAs(Form("results/background/twodim_background_para47_2_%d.pdf",aNumber));


   TFile * fout = new TFile("data/electron_pion_ratio.root","UPDATE");

   h2->Write("hR_2D_para47");
   f2->Write("fR_2D_para47");

   //ch1->StartViewer();
   return 0;
}

