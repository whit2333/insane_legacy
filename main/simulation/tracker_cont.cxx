Int_t tracker_cont(Int_t runNumber = 4214 ){

   rman->SetRun(runNumber);

   InSANERun * run = rman->GetCurrentRun();

   SANEEvents * events              = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);

   BETAG4MonteCarloEvent * mcevent  = (BETAG4MonteCarloEvent*)(events->MC);
   BIGCALCluster * cluster = 0;
   InSANEFakePlaneHit * planehit = 0;
   TTree * t                        = events->fTree;
   TH1F * h1 = 0;
   TH2F * h2 = 0;

   TTree * t2 = (TTree*)gROOT->FindObject("thrownEvents");
   if(!t2) return(-1);

   /// Histograms
   Double_t fRadsPerDeg  = TMath::Pi()/180.0;
   Double_t fPhi_min     = -90.0;
   Double_t fPhi_max     = 90.0;
   Double_t fE_min       = 100.0; //MeV
   Double_t fE_max       = 2100.0;
   Double_t fTheta_min   = 20.0; 
   Double_t fTheta_max   = 60.0;

   TH2F * hNThrownVNTrackerplane = new TH2F("hNThrownVNTrackerplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   TH2F * hNThrownVNBigcalplane = new TH2F("hNThrownVNBigcalplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);

   TH1F * hPIDs1    = new TH1F("hPIDs1","hPIDs1",100,-220,220);
   TH1F * hPIDs2    = new TH1F("hPIDs2","hPIDs2",100,-220,220);
   TH1F * hPIDs3    = new TH1F("hPIDs3","hPIDs3",100,-220,220);
   TH1F * hPIDs4    = new TH1F("hPIDs4","hPIDs4",100,-220,220);

   TH1F * hTheta1   = new TH1F("hTheta1","hTheta1",50,fTheta_min,fTheta_max);
   TH1F * hTheta2   = new TH1F("hTheta2","hTheta2",50,fTheta_min,fTheta_max);
   TH1F * hTheta3   = new TH1F("hTheta3","hTheta3",50,fTheta_min,fTheta_max);
   TH1F * hTheta4   = new TH1F("hTheta4","hTheta4",50,fTheta_min,fTheta_max);

   TH1F * hEnergy1  = new TH1F("hEnergy1","hEnergy1",50,fE_min,fE_max);
   TH1F * hEnergy2  = new TH1F("hEnergy2","hEnergy2",50,fE_min,fE_max);
   TH1F * hEnergy3  = new TH1F("hEnergy3","hEnergy3",50,fE_min,fE_max);
   TH1F * hEnergy4  = new TH1F("hEnergy4","hEnergy4",50,fE_min,fE_max);


   TH1F * hPhi1     = new TH1F("hPhi1","hPhi1",50,fPhi_min,fPhi_max);
   TH1F * hPhi2     = new TH1F("hPhi2","hPhi2",50,fPhi_min,fPhi_max);
   TH1F * hPhi3     = new TH1F("hPhi3","hPhi3",50,fPhi_min,fPhi_max);
   TH1F * hPhi4     = new TH1F("hPhi4","hPhi4",50,fPhi_min,fPhi_max);


   TH2F * hThetaVPhi1   = new TH2F("hThetaVPhi1","hThetaVPhi1",50,fTheta_min,fTheta_max,50,fPhi_min,fPhi_max);
   TH2F * hThetaVPhi2   = new TH2F("hThetaVPhi2","hThetaVPhi2",50,fTheta_min,fTheta_max,50,fPhi_min,fPhi_max);
   TH2F * hThetaVPhi3   = new TH2F("hThetaVPhi3","hThetaVPhi3",50,fTheta_min,fTheta_max,50,fPhi_min,fPhi_max);
   TH2F * hThetaVPhi4   = new TH2F("hThetaVPhi4","hThetaVPhi4",50,fTheta_min,fTheta_max,50,fPhi_min,fPhi_max);

   TH2F * hThetaVEnergy1   = new TH2F("hThetaVEnergy1","hThetaVEnergy1",50,fTheta_min,fTheta_max,50,fE_min,fE_max);

   /// Event Loop.
   Int_t nevents = t->GetEntries();
   std::cout << " Tree has " << nevents << "\n";
   for(int ievent = 0; ievent < nevents ; ievent++){

      if(ievent%10000 == 0) std::cout << ievent << "/" << nevents << "\n";

      events->fTree->GetEntry(ievent);

      for(int j = 0; j< events->MC->fTrackerPlane2Hits->GetEntries();j++ ){
	 planehit = (InSANEFakePlaneHit*)(*(events->MC->fTrackerPlane2Hits))[j];

	 hPIDs1->Fill(planehit->fPID);
	 hThetaVPhi1->Fill(planehit->fTheta/fRadsPerDeg,planehit->fPhi/fRadsPerDeg);
	 hTheta1->Fill(planehit->fTheta/fRadsPerDeg);
	 hPhi1->Fill(planehit->fPhi/fRadsPerDeg);
	 hEnergy1->Fill(planehit->fEnergy);
	 hThetaVEnergy1->Fill(planehit->fTheta/fRadsPerDeg,planehit->fEnergy);
      }

      for(int j = 0; j< events->MC->fTrackerPlaneHits->GetEntries();j++ ){
	 planehit = (InSANEFakePlaneHit*)(*(events->MC->fTrackerPlaneHits))[j];

	 hPIDs3->Fill(planehit->fPID);
	 hThetaVPhi3->Fill(planehit->fTheta/fRadsPerDeg,planehit->fPhi/fRadsPerDeg);
	 hTheta3->Fill(planehit->fTheta/fRadsPerDeg);
	 hPhi3->Fill(planehit->fPhi/fRadsPerDeg);
	 hEnergy3->Fill(planehit->fEnergy);
      }


      if( events->TRIG->IsBETAEvent() ) {

	 for(int i = 0; i< events->CLUSTER->fClusters->GetEntries();i++) {
	    cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[i];

            /// Cherenkov Cut
	    if(cluster->fCherenkovBestADCSum > 0.2) {
               
               /// POST Tracker Plane
	       for(int j = 0; j< events->MC->fTrackerPlane2Hits->GetEntries();j++ ){
	          planehit = (InSANEFakePlaneHit*)(*(events->MC->fTrackerPlane2Hits))[j];

		  hPIDs2->Fill(planehit->fPID);
		  hThetaVPhi2->Fill(planehit->fTheta/fRadsPerDeg,planehit->fPhi/fRadsPerDeg);
		  hTheta2->Fill(planehit->fTheta/fRadsPerDeg);
		  hPhi2->Fill(planehit->fPhi/fRadsPerDeg);
		  hEnergy2->Fill(planehit->fEnergy);

		  if(cluster->fCherenkovBestADCSum < 1.5 && cluster->fCherenkovBestADCSum > 0.5) {
		     hPIDs4->Fill(planehit->fPID);
		     hThetaVPhi4->Fill(planehit->fTheta/fRadsPerDeg,planehit->fPhi/fRadsPerDeg);
		     hTheta4->Fill(planehit->fTheta/fRadsPerDeg);
		     hPhi4->Fill(planehit->fPhi/fRadsPerDeg);
		     hEnergy4->Fill(planehit->fEnergy);
		  } // Tight cherenkov cut 

	       } // Tracker plane 2 loop

	    }// Cherenkov cut

	 }

      }
      hNThrownVNTrackerplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fTrackerPlaneHits->GetEntries());
      hNThrownVNBigcalplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fBigcalPlaneHits->GetEntries());

   }// Event loop end 



   TCanvas * c = new TCanvas("tracker_cont","tracker_cont");
   c->Divide(3,3);
   c->cd(1);
   gPad->SetLogy(true);

   hPIDs1->SetLineColor(1);
   hPIDs1->SetMinimum(100);
   hPIDs1->Draw();

   hPIDs2->SetLineColor(2);
   hPIDs2->Draw("same");

   hPIDs3->SetLineColor(3);
   hPIDs3->Draw("same");

   hPIDs4->SetLineColor(4);
   hPIDs4->Draw("same");

   TLegend *    leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("The Legend Title");

   leg->AddEntry(hPIDs3,"PRE-tracker plane","l");
   leg->AddEntry(hPIDs1,"POST-tracker plane","l");
   leg->AddEntry(hPIDs2,"POST-tracker plane Loose #check{C} Cut","l");
   leg->AddEntry(hPIDs4,"POST-tracker plane Tight #check{C} Cut","l");

   leg->Draw();


   c->cd(2);
   hThetaVEnergy1->Draw("colz");

   c->cd(3);
   TH1F * hThetaVPhiEff1  = hThetaVPhi2->Clone("hEnergyEff1");
   hThetaVPhiEff1->Divide(hThetaVPhi1);
   hThetaVPhiEff1->Draw("colz");
   //hThetaVPhi2->Draw("colz");

   c->cd(4);
   gPad->SetLogy(true);

   hEnergy1->SetLineColor(1);
   hEnergy1->SetMinimum(10);
   hEnergy1->Draw();

   hEnergy2->SetLineColor(2);
   hEnergy2->Draw("same");

   hEnergy3->SetLineColor(3);
   hEnergy3->Draw("same");

   hEnergy4->SetLineColor(4);
   hEnergy4->Draw("same");

   c->cd(5);
   gPad->SetLogy(true);

   hTheta1->SetLineColor(1);
   hTheta1->Draw();

   hTheta2->SetLineColor(2);
   hTheta2->Draw("same");

   hTheta3->SetLineColor(3);
   hTheta3->Draw("same");

   hTheta4->SetLineColor(4);
   hTheta4->Draw("same");

   c->cd(6);
   gPad->SetLogy(true);

   hPhi1->SetLineColor(1);
   hPhi1->Draw();

   hPhi2->SetLineColor(2);
   hPhi2->Draw("same");

   hPhi3->SetLineColor(3);
   hPhi3->Draw("same");

   hPhi4->SetLineColor(4);
   hPhi4->Draw("same");

   c->cd(7);

   TH1F * hEnergyEff1  = hEnergy2->Clone("hEnergyEff1");
   hEnergyEff1->Divide(hEnergy1);
   hEnergyEff1->SetLineColor(2);
   hEnergyEff1->Draw();

   TH1F * hEnergyEff2  = hEnergy4->Clone("hEnergyEff2");
   hEnergyEff2->Divide(hEnergy1);
   hEnergyEff2->SetLineColor(4);
   hEnergyEff2->Draw("same");

   c->cd(8);

   TH1F * hThetaEff1  = hTheta2->Clone("hThetaEff1");
   hThetaEff1->Divide(hTheta1);
   hThetaEff1->SetLineColor(2);
   hThetaEff1->Draw();

   TH1F * hThetaEff2  = hTheta4->Clone("hThetaEff2");
   hThetaEff2->Divide(hTheta1);
   hThetaEff2->SetLineColor(4);
   hThetaEff2->Draw("same");


   c->cd(9);

   TH1F * hPhiEff1  = hPhi2->Clone("hPhiEff1");
   hPhiEff1->Divide(hPhi1);
   hPhiEff1->SetLineColor(2);
   hPhiEff1->Draw();

   TH1F * hPhiEff2  = hPhi4->Clone("hPhiEff2");
   hPhiEff2->Divide(hPhi1);
   hPhiEff2->SetLineColor(4);
   hPhiEff2->Draw("same");


   c->SaveAs(Form("plots/%d/tracker_cont.png",runNumber));


   return(0);
}

