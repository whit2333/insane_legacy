Int_t tracker_planes(Int_t runNumber = 4214 ){

   rman->SetRun(runNumber);

   InSANERun * run = rman->GetCurrentRun();

   SANEEvents * events              = new SANEEvents("betaDetectors");
   BETAG4MonteCarloEvent * mcevent  = (BETAG4MonteCarloEvent*)(events->MC);
   TTree * t                        = events->fTree;

   // Histograms
   TH2F * hNThrownVNTrackerplane = new TH2F("hNThrownVNTrackerplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);
   TH2F * hNThrownVNBigcalplane = new TH2F("hNThrownVNBigcalplane","hNThrownVNTrackerplane", 10,0,10, 10,0,10);


   /// Event Loop.
/*
   Int_t nevents = t->GetEntries();
   std::cout << " Tree has " << nevents << "\n";
   for(int ievent = 0; ievent < nevents ; ievent++){

      hNThrownVNTrackerplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fTrackerPlaneHits->GetEntries());
      hNThrownVNBigcalplane->Fill(events->MC->fThrownParticles->GetEntries(), events->MC->fBigcalPlaneHits->GetEntries());

   }// Event loop end 
*/
   TCanvas * c = new TCanvas("particles_thrown","thrown_particle_distribution");
   TH1F * h1 = 0;
   TH2F * h2 = 0;

//   c->Divide(3,3);
//   c->cd(1);

   t->Draw("fTrackerPlane2Hits.fPID>>tk2PID(100,-220,220)","","goff");
   h1 = (TH1F*)gROOT->FindObject("tk2PID");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw();
   }

   t->Draw("fTrackerPlaneHits.fPID>>tk1PID(100,-220,220)","","goff");
   h1 = (TH1F*)gROOT->FindObject("tk1PID");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   t->Draw("fTrackerPlane2Hits.fPID>>tk3PID(100,-220,220)","fTrackerPlane2Hits->fEnergy>21","goff");
   h1 = (TH1F*)gROOT->FindObject("tk3PID");
   if(h1) {
      h1->SetLineColor(3);
      h1->Draw("same");
   }

   t->Draw("fTrackerPlane2Hits.fPID>>tk4PID(100,-220,220)","fTrackerPlane2Hits->fEnergy>200","goff");
   h1 = (TH1F*)gROOT->FindObject("tk4PID");
   if(h1) {
      h1->SetLineColor(6);
      h1->Draw("same");
   }

   
   return(0);

   c->cd(2);
   t->Draw("fThrownParticles.fVy:fThrownParticles.fVx","","colz");

   c->cd(3);
   t->Draw("fThrownParticles.fVy:fThrownParticles.fVz","","colz");

   c->cd(4);
   gPad->SetLogy(true);
   t->Draw("fThrownParticles.fE*1000>>thrownE(100,0,4000)","","goff");
   h1 = (TH1F*)gROOT->FindObject("thrownE");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
   }

   t->Draw("fBigcalPlaneHits.fEnergy>>bcHitsE(100,0,4000)","","goff");
   h1 = (TH1F*)gROOT->FindObject("bcHitsE");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   t->Draw("fTrackerPlaneHits.fEnergy>>tkHitsE(100,0,4000)","","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHitsE");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
   }

   c->cd(5);
   gPad->SetLogy(true);
   t->Draw("fThrownParticles.Theta()/0.0175>>thrownTheta(100,0,90)","","goff");
   h1 = (TH1F*)gROOT->FindObject("thrownTheta");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
   }

   t->Draw("fBigcalPlaneHits.fPosition.Theta()/0.0175>>bcHitsTheta(100,0,90)","","goff");
   h1 = (TH1F*)gROOT->FindObject("bcHitsTheta");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   t->Draw("fTrackerPlaneHits.fPosition.Theta()/0.0175>>tkHitsTheta(100,0,90)","","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHitsTheta");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
   }

   c->cd(6);
   gPad->SetLogy(true);
   t->Draw("TMath::ATan2(TMath::Sin(fThrownParticles.Phi()),TMath::Cos(fThrownParticles.Phi()))/0.0175>>thrownPhi(200,-90,90)","","goff");
//   t->Draw("fThrownParticles.Phi()/0.0175>>thrownPhi(200,-90,390)","","goff");
   h1 = (TH1F*)gROOT->FindObject("thrownPhi");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
   }

   t->Draw("fBigcalPlaneHits.fPosition.Phi()/0.0175>>bcHitsPhi(200,-90,90)","","goff");
   h1 = (TH1F*)gROOT->FindObject("bcHitsPhi");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   t->Draw("fBigcalPlaneHits.fPosition.Phi()/0.0175>>bcHitsPhi2(200,-90,90)","fBigcalPlaneHits.fEnergy>21","goff");
   h1 = (TH1F*)gROOT->FindObject("bcHitsPhi2");
   if(h1) {
      h1->SetFillStyle(3001);
      h1->SetFillColor(2);
      h1->SetLineColor(2);
      h1->Draw("same");
   }


   t->Draw("fTrackerPlaneHits.fPosition.Phi()/0.0175>>tkHitsPhi(200,-90,90)","","goff");
   h1 = (TH1F*)gROOT->FindObject("tkHitsPhi");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
   }

   c->cd(7);
   hNThrownVNBigcalplane->Draw("colz");
   //t->Draw("fTrackerPlaneHits.fLocalPosition.fY:fTrackerPlaneHits.fLocalPosition.fX","","colz"); 

   c->cd(8);
   t->Draw("fBigcalPlaneHits.fLocalPosition.fY:fBigcalPlaneHits.fLocalPosition.fX","","colz"); 

   c->cd(9);
   t->Draw("fTrackerPlaneHits.fLocalPosition.fY:fTrackerPlaneHits.fLocalPosition.fX","","colz"); 



   //hNThrownVNTrackerplane->Draw("colz");

   t->StartViewer();

   return(0);
}

