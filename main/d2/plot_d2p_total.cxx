class  d2_alphas {
   public:
      InSANEStrongCouplingConstant  coupling;
      Double_t d20;
      Double_t Q20;
      double operator() (double *x, double *p) {
         double as_0 = coupling(Q20);
         double as_1 = coupling(x[0]);
         double Nc = 3.0;
         double Nf = 3.0;
         double b = (11.0/3.0)*Nc-(2.0/3.0)*Nf;
         double NS_exponent = (-1.0/b)*(3.0*Nc-(Nc-1.0/Nc)/6.0);
         double S_exponent = (-1.0/b)*(3.0*Nc-(Nc-1.0/Nc)/6.0 + (2.0/3.0)*Nf);
         return( d20*TMath::Power(as_0/as_1,S_exponent) );
      }
};
class  d2p_elastic {
   public:
      InSANEFormFactors                 * ffs;
      double operator() (double *x, double *p) { return( ffs->d2p_el(x[0])); }
};

class  d2n_elastic {
   public:
      InSANEFormFactors                 * ffs;
      double operator() (double *x, double *p) { return( ffs->d2n_el(x[0])); }
};

class  d2p_inelastic {
   public:
      InSANEPolarizedStructureFunctions * psfs;
      double operator() (double *x, double *p) { return(  psfs->d2p_tilde(x[0],p[0],p[1])); }
};

class  d2n_inelastic {
   public:
      InSANEPolarizedStructureFunctions * psfs;
      double operator() (double *x, double *p) { return(  psfs->d2n_tilde(x[0],p[0],p[1])); }
};

class  d2p_total {
   public:
      InSANEStrongCouplingConstant  coupling;
      Double_t d20;
      Double_t Q20;
      double evolved_twist3_ME(double *x, double *p) {
         double as_0 = coupling(Q20);
         double as_1 = coupling(x[0]);
         double Nc = 3.0;
         double Nf = 3.0;
         double b = (11.0/3.0)*Nc-(2.0/3.0)*Nf;
         double NS_exponent = (-1.0/b)*(3.0*Nc-(Nc-1.0/Nc)/6.0);
         double S_exponent = (-1.0/b)*(3.0*Nc-(Nc-1.0/Nc)/6.0 + (2.0/3.0)*Nf);
         return( d20*TMath::Power(as_0/as_1,S_exponent) );
      }
      d2_alphas                         * pqcd;
      InSANEPolarizedStructureFunctions * psfs; 
      InSANEFormFactors                 * ffs;
      double operator() (double *x, double *p) { return( ffs->d2p_el(x[0]) + evolved_twist3_ME(x,p) ) ; }//psfs->d2p_tilde(x[0],p[0],p[1])); }
};

class  d2n_total {
   public:
      InSANEPolarizedStructureFunctions * psfs;
      InSANEFormFactors                 * ffs;
      double operator() (double *x, double *p) { return( ffs->d2n_el(x[0]) + psfs->d2n_tilde(x[0],p[0],p[1])); }
};

Int_t plot_d2p_total(Int_t aNumber = 3216) {

   NucDBManager * dbman = NucDBManager::GetManager();
   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 

   d2p_elastic   * fobj_p_el   = new d2p_elastic();
   d2n_elastic   * fobj_n_el   = new d2n_elastic();
   d2p_inelastic * fobj_p_inel = new d2p_inelastic();
   d2n_inelastic * fobj_n_inel = new d2n_inelastic();
   d2p_total     * fobj_p_tot  = new d2p_total();
   d2n_total     * fobj_n_tot  = new d2n_total();

   // ------------------
   //
   InSANEPolarizedStructureFunctions * psfs = fman->CreatePolSFs(6);
   InSANEFormFactors                 * ffs  = fman->CreateFFs(4);
   fobj_p_el->ffs    = ffs;
   fobj_n_el->ffs    = ffs;
   fobj_p_inel->psfs = psfs;
   fobj_n_inel->psfs = psfs;
   fobj_p_tot->ffs   = ffs;
   fobj_n_tot->ffs   = ffs;
   fobj_p_tot->psfs  = psfs;
   fobj_n_tot->psfs  = psfs;

   // ------------------
   //
   TF1           * d2p_el   = new TF1("d2p_el",  fobj_p_el  , 0.1, 10,0, "d2p_elastic");
   TF1           * d2n_el   = new TF1("d2n_el",  fobj_n_el  , 0.1, 10,0, "d2n_elastic");
   TF1           * d2p_inel = new TF1("d2p_inel",fobj_p_inel, 0.1, 10,2, "d2p_inelastic");
   TF1           * d2n_inel = new TF1("d2n_inel",fobj_n_inel, 0.1, 10,2, "d2n_inelastic");
   TF1           * d2p_tot  = new TF1("d2p_tot", fobj_p_tot , 0.1, 10,2, "d2p_total");
   TF1           * d2n_tot  = new TF1("d2n_tot", fobj_n_tot , 0.1, 10,2, "d2n_total");
   d2p_inel->SetParameters(0.01,0.9);
   d2n_inel->SetParameters(0.01,0.9);
   d2p_tot->SetParameters(0.01,0.9);
   d2n_tot->SetParameters(0.01,0.9);

   // -------------------------------------------------
   // Legends
   TLegend * leg = new TLegend(0.54,0.65,0.88,0.88);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   TLegend * leg0 = new TLegend(0.60,0.65,0.88,0.88);
   leg0->SetFillStyle(0);
   leg0->SetBorderSize(0);

   TLegend * leg2 = new TLegend(0.4,0.7,0.60,0.88);
   leg2->SetFillStyle(0);
   leg2->SetBorderSize(0);

   // -------------------------------------------------
   //
   double x1,y1;

   d2p_tot->SetLineColor(1);
   d2p_tot->SetLineStyle(1);

   d2p_el->SetLineColor(1);
   d2p_el->SetLineStyle(2);

   d2p_inel->SetLineColor(1);
   d2p_inel->SetLineStyle(3);

   // --------------------------------
   // Calculated curves
   TGraphErrors * gr = 0;
   TMultiGraph  * mg_curves = new TMultiGraph();

   TGraph * gr_el_curve = new TGraph(d2p_el->DrawCopy("goff")->GetHistogram());
   mg_curves->Add(gr_el_curve,"l");
   leg0->AddEntry(gr_el_curve,"elastic","l");

   TGraph * gr_inel_curve = new TGraph(d2p_inel->DrawCopy("goff")->GetHistogram());
   mg_curves->Add(gr_inel_curve,"l");
   leg0->AddEntry(gr_inel_curve,"inelastic","l");

   TGraph * gr_tot_curve = new TGraph(d2p_tot->DrawCopy("goff")->GetHistogram());
   mg_curves->Add(gr_tot_curve,"l");
   leg0->AddEntry(gr_tot_curve,"total","l");

   // ---------------------------------------------
   // Lattice
   TMultiGraph * mg_calc  = new TMultiGraph();
   TList    * calc_meas = dbman->GetMeasurementCalculations("d2p");
   NucDBMeasurement * lattice_meas = NucDB::FindExperiment("Gockeler",calc_meas);
   TGraphErrors     * gr_lattice   = lattice_meas->BuildGraph("Qsquared");
   gr_lattice->SetMarkerStyle(24);
   gr_lattice->SetLineWidth(2);
   mg_calc->Add(gr_lattice,"p");
   //NucDB::FillLegend(leg,calc_meas,mg_calc);
   leg->AddEntry(gr_lattice,Form("Lattice - %s",lattice_meas->GetExperimentName()),"lp");

   // ---------------------------------------------
   // published d2
   // RSS
   TMultiGraph * mg_existing    = new TMultiGraph();
   TList            * meas_list = 0;
   meas_list                    = dbman->GetMeasurements("d2p I");
   //TList            * meas      = dbman->GetMeasurements("d2p");
   NucDBMeasurement * rss_meas  = NucDB::FindExperiment("RSS",meas_list);
   TGraphErrors     * gr_rss    = rss_meas->BuildGraph("Qsquared");
   gr_rss->SetMarkerStyle(21);
   gr_rss->SetLineColor(1);
   gr_rss->SetMarkerColor(1);
   mg_existing->Add(gr_rss,"ep");
   leg->AddEntry(gr_rss,rss_meas->GetExperimentName(),"lp");
   gr_rss    = rss_meas->BuildGraph("Qsquared",true);
   gr_rss->SetMarkerStyle(1);
   gr_rss->SetLineColor(1);
   gr_rss->SetMarkerColor(1);
   mg_existing->Add(gr_rss,"[]");

   // SLAC
   meas_list                    = dbman->GetMeasurements("d2p");
   NucDBMeasurement * slac_meas = NucDB::FindExperiment("SLAC",meas_list);
   TGraphErrors     * gr_slac   = slac_meas->BuildGraph("Qsquared");
   gr_slac->SetMarkerStyle(25);
   gr_slac->SetLineColor(1);
   mg_existing->Add(gr_slac,"ep");
   leg->AddEntry(gr_slac,"SLAC E155x+E155+E143","lp");
   gr_slac    = slac_meas->BuildGraph("Qsquared",true);
   gr_slac->SetMarkerStyle(1);
   gr_slac->SetLineColor(1);
   gr_slac->SetMarkerColor(1);
   mg_existing->Add(gr_slac,"[]");

   // ---------------------------------------------
   // pQCD evolved slac  point
   d2_alphas  * d2as = new d2_alphas();
   gr_slac->GetPoint(0,x1,y1);
   d2as->d20 = y1;
   d2as->Q20 = x1;
   TF1 * d2_evolve  = new TF1("d2_evolve", d2as , 1.0, 10,0, "d2_alphas");
   fobj_p_tot->d20 = y1;
   fobj_p_tot->Q20 = x1;

   // ---------------------------------------------
   // g1 analysis
   NucDBMeasurement * osipenko = NucDB::FindExperiment("Osipenko",calc_meas);
   if( osipenko ) {
      //osipenko->Print("data");
      TGraph * gr_sys = osipenko->BuildGraph("Qsquared",true);
      gr_sys->SetFillColorAlpha(kGreen,0.1);
      //mg->Add(gr_sys,"e3");
      //TLegendEntry * leg_entry = (TLegendEntry*) leg->GetListOfPrimitives()->At(0);
      //leg_entry->SetFillColorAlpha(kGreen,0.1);
      //leg_entry->SetOption("lpf");
      //leg_entry->SetObject(gr_sys);
   }

   // ---------------------------------------------
   //
   TLatex latex;
   latex.SetNDC(true);
   //latex.SetTextColorAlpha(1,0.2);
   latex.SetTextSize(0.15);



   // ---------------------------------------------
   //
   TCanvas     * c  = new TCanvas();
   TMultiGraph * mg = new TMultiGraph();

   mg->Add(mg_calc);
   //mg->Add(mg_curves);
   mg->Add(mg_existing);

   mg->Draw("a");
   mg->GetXaxis()->SetLimits(1.0,10.0);
   mg->GetYaxis()->SetRangeUser(-0.03,0.045);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetXaxis()->SetTitle("Q^{2} (GeV^{2})");

   leg->Draw();
   leg2->Draw();


   c->SaveAs(Form("results/d2/plot_d2p_total_0_%d.png",aNumber));
   c->SaveAs(Form("results/d2/plot_d2p_total_0_%d.pdf",aNumber));


   //NucDBExperiment * exp = dbman->GetExperiment("SANE");
   //NucDBMeasurement * d2p_sane = exp->GetMeasurement("d2p");
   //if(!d2p_sane) { 
   //   d2p_sane = new NucDBMeasurement("d2p","d2p");
   //}

   // ---------------------------------------------
   // SANE BETA
   TMultiGraph      * mg_sane   = new TMultiGraph();
   NucDBMeasurement * sane_meas = dbman->GetExperiment("SANE")->GetMeasurement("d2p all");
   TGraphErrors     * gr_sane   = sane_meas->BuildGraph("Qsquared");
   gr_sane->SetMarkerStyle(20);
   gr_sane->SetMarkerColor(2);
   gr_sane->SetLineColor(2);
   mg_sane->Add(gr_sane,"ep");
   leg->AddEntry(gr_sane,sane_meas->GetExperimentName(),"lp");

   //gr_sane   = sane_meas->BuildGraph("Qsquared",true);
   //gr_sane->SetMarkerStyle(20);
   //gr_sane->SetMarkerColor(2);
   //gr_sane->SetLineColor(2);
   //mg_sane->Add(gr_sane,"e3");

   gr_sane   = sane_meas->BuildSystematicErrorBand("Qsquared",-0.03);
   gr_sane->SetFillColorAlpha(2,0.5);
   mg_sane->Add(gr_sane,"e3");

   // -------------------------------
   // HMS 
   TMultiGraph      * mg_hms   = new TMultiGraph();
   NucDBMeasurement * hms_meas = dbman->GetExperiment("SANE")->GetMeasurement("d2p all kang");
   TGraphErrors     * gr_hms   = hms_meas->BuildGraph("Qsquared");
   gr_hms->SetMarkerStyle(24);
   gr_hms->SetMarkerColor(2);
   gr_hms->SetLineColor(2);
   //mg_hms->Add(gr_hms,"ep");
   //leg->AddEntry(gr_hms,hms_meas->GetExperimentName(),"lp");
   gr_hms   = hms_meas->BuildGraph("Qsquared",true); // systematic error
   gr_hms->SetMarkerStyle(24);
   gr_hms->SetMarkerColor(2);
   gr_hms->SetLineColor(2);
   //mg_hms->Add(gr_hms,"[]");

   //hms_meas      = dbman->GetMeasurements("d2pbar kang");
   //if(hms_meas) {
   //   mg_hms      = NucDB::CreateMultiGraph(hms_meas,"Qsquared");
   //   NucDB::FillLegend(leg2,hms_meas,mg_hms);
   //   mg->Add(mg_hms);
   //}

   //------------------------
   //
   c   = new TCanvas();
   mg  = new TMultiGraph();
   mg->Add(mg_calc);
   mg->Add(mg_existing);
   mg->Add(mg_hms);
   mg->Add(mg_sane);
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(1.0,10.0);
   mg->GetYaxis()->SetRangeUser(-0.03,0.045);
   mg->GetXaxis()->SetTitle("Q^{2} (GeV^{2})");
   mg->GetXaxis()->CenterTitle(true);

   leg->Draw();
   leg2->Draw();

   //d2p_total->Draw("same");
   ////d2n_total->Draw("same");
   //d2p_el->Draw("same");
   ////d2n_el->Draw("same");

   latex.DrawLatex(0.2,0.2,"Preliminary");

   c->Update();
   c->SaveAs(Form("results/d2/plot_d2p_total_1_%d.png",aNumber));
   c->SaveAs(Form("results/d2/plot_d2p_total_1_%d.pdf",aNumber));


   //------------------------
   //
   d2_evolve->SetLineColor(1);
   d2_evolve->SetLineWidth(1);
   d2_evolve->SetLineStyle(2);
   d2_evolve->Draw("same");

   d2p_el->SetLineColor(1);
   d2p_el->SetLineWidth(1);
   d2p_el->SetLineStyle(3);
   d2p_el->Draw("same");

   d2p_tot->SetLineColor(1);
   d2p_tot->SetLineWidth(1);
   d2p_tot->SetLineStyle(1);
   d2p_tot->Draw("same");

   leg->AddEntry(d2_evolve,"pQCD - Shuryak and Vainshtein","l");
   leg->AddEntry(d2p_el,"elastic","l");
   leg->AddEntry(d2p_tot,"elastic + pQCD","l");

   latex.DrawLatex(0.2,0.2,"Preliminary");
   c->Update();
   c->SaveAs(Form("results/d2/plot_d2p_total_2_%d.png",aNumber));
   c->SaveAs(Form("results/d2/plot_d2p_total_2_%d.pdf",aNumber));

   return 0;
}
