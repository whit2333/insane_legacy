Int_t make_tables(Int_t aNumber = 3206){

   NucDBManager          * dbman = NucDBManager::GetManager();
   //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 
   NucDBMeasurement * meas = 0;

   NucDBExperiment * exp = dbman->GetExperiment("SANE");
   exp->ListMeasurements(); 
   
   // Full Nachtmann moment
   meas = exp->GetMeasurement("d2p Nachtmann");
   meas->Print("data");

   std::ofstream out_file(Form("results/d2/nachtmann_%d.txt",aNumber));
   meas->PrintTable(out_file);
   out_file.close();

   // Full Nachtmann moment
   meas = exp->GetMeasurement("d2p all");
   meas->Print("data");

   out_file.open(Form("results/d2/cn_%d.txt",aNumber));
   meas->PrintTable(out_file);

   return 0;
}
