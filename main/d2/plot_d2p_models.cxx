Int_t plot_d2p_models(Int_t aNumber = 0) {


   NucDBManager          * dbman = NucDBManager::GetManager();
   InSANEFunctionManager * fman  = InSANEFunctionManager::GetManager(); 

   TList    * calc_meas = dbman->GetMeasurementCalculations("d2p");
   TMultiGraph * mg = NucDB::CreateMultiGraph(calc_meas,"Qsquared");

   //NucDB::FillLegend(leg,calc_meas,mg_calc);
   //NucDBMeasurement * lattice_meas = NucDB::FindExperiment("Gockeler",calc_meas);

   // Take off of sum rules and models form Ji and Filippone
   // SR     -0.00647176530439 0.00358962184161 
   // SR     -0.00346820417161 0.00615363744277 
   // SR     -0.00636052229947 0.00307681872138 
   // Model:  0.0175906393081  0.000769204680346 
   // Model:  0.00598066813634 0.00230761404104 
   // Model:  0.00976021705952 0.00153840936069 

   // ----------------------------
   // Sum Rules
   TGraphErrors * sum_rules = new TGraphErrors(3);
   sum_rules->SetPoint(     0, 1.0, -0.00647176530439);
   sum_rules->SetPointError(0, 0.0,  0.00358962184161);
   sum_rules->SetPoint(     1, 1.2, -0.00346820417161);
   sum_rules->SetPointError(1, 0.0,  0.00615363744277);
   sum_rules->SetPoint(     2, 1.4, -0.00636052229947);
   sum_rules->SetPointError(2, 0.0,  0.00307681872138);

   sum_rules->SetMarkerColor(8);
   sum_rules->SetLineColor(8);
   sum_rules->SetMarkerStyle(26);

   // ----------------------------
   // 
   TCanvas * c = new TCanvas();


   mg->Add(sum_rules,"p");
   mg->Draw("a");

   c->SaveAs(Form("results/d2/plot_d2p_models_%d.png",aNumber));
   c->SaveAs(Form("results/d2/plot_d2p_models_%d.pdf",aNumber));

   return 0;
}
