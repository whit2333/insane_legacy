#include <iostream>
#include <vector>
#include "TMath.h"
using namespace std;

int interpolation(){

   using namespace std;
   vector<double>                                      x;
   vector<double>                                      xd;
   vector<vector<double> >                             y;
   vector<vector<vector<vector<vector<double> > > > >  c_5;
   vector<vector<vector<vector<double> > > >           c_4;
   vector<vector<vector<double> > >                    c_3;
   vector<vector<double> >                             c_2;
   vector<double>                                      c_1;
   double                                              c_0;
   const int N = 5;
   x.resize(N);
   xd.resize(N);
   y.resize(N);
   for(int i=0;i<N;i++){
      x[i] = 0.5;
      y[i].resize(2);
      y[i][0] = 0.0; 
      y[i][1] = 1.0; 
   }

   c_5.resize(5); 
   c_4.resize(4); 
   c_3.resize(3); 
   c_2.resize(2); 
   c_1.resize(1); 
   for(int i=0;i<2;i++){
      c_5[i].resize(2); 
      c_4[i].resize(2); 
      c_3[i].resize(2); 
      c_2[i].resize(2); 
      for(int j=0;j<2;j++){
         c_5[i][j].resize(2); 
         c_4[i][j].resize(2); 
         c_3[i][j].resize(2); 
         for(int k=0;k<2;k++){
            c_5[i][j][k].resize(2); 
            c_4[i][j][k].resize(2); 
            for(int l=0;l<2;l++){
               c_5[i][j][k][l].resize(2);
               for(int m=0;m<2;m++){
                  //double res = TMath::Sqrt(double(i)+double(j)+double(k)+double(l)+double(m));
                  double res = 0.0; 
                  if( i==1 )
                     if( j==1 )
                        if( k==1 )
                           if( l==1 )
                              if( m==1 ) {
                                 res = 1.0;
                                 std::cout << "made it " << std::endl;
                              }
                  c_5[i][j][k][l][m] = res;//double(m); // f(x[]) = m  
               }
            }
         }
      }
   }

   for(int i=0;i<N;i++)  {
      xd[i] = (x[i]-y[i][0])/(y[i][1]-y[i][0]);
      std::cout << "xd[" << i << "] = " << xd[i] << std::endl;
   }

   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         for(int k=0;k<2;k++){
            for(int l=0;l<2;l++){
               c_4[i][j][k][l] = c_5[i][j][k][l][0]*(1.0-xd[4]) + c_5[i][j][k][l][1]*xd[4] ;
               std::cout << "c_4[" << i << j << k << l << "] = " << c_4[i][j][k][l] << std::endl;
            }
         }
      }
   }

   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         for(int k=0;k<2;k++){
            c_3[i][j][k] = c_4[i][j][k][0]*(1.0-xd[3]) + c_4[i][j][k][1]*xd[3] ;
            std::cout << "c_3[" << i << j << k << "] = " << c_3[i][j][k] << std::endl;
         }
      }
   }

   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         c_2[i][j] = c_3[i][j][0]*(1.0-xd[2]) + c_3[i][j][1]*xd[2] ;
         std::cout << "c_2[" << i << j << "] = " << c_2[i][j] << std::endl;
      }
   }

   for(int i=0;i<2;i++){
      c_1[i] = c_2[i][0]*(1.0-xd[1]) + c_2[i][1]*xd[1] ;
      std::cout << "c_1[" << i  << "] = " << c_1[i] << std::endl;
   }

   c_0 = c_1[0]*(1.0-xd[0]) + c_1[1]*xd[0] ;
      
   std::cout << "result is " << c_0 << std::endl;

   return(0);
}
