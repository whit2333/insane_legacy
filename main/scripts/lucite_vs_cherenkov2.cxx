Int_t lucite_vs_cherenkov2(Int_t runnumber=72994) {

  const char * detectorTree = "betaDetectors1";
  const char * trajTree = "betaTrajectories";
  const char * clusterTree = "Clusters";

// Set the run number 
   rman->SetRun(runnumber);
// Find the trees above
   TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   if( !t ) { 
      printf("Tree:%s \n    was NOT FOUND. Aborting... \n",detectorTree); 
      return(-1);
   }

   TString draw = "fLuciteHodoscopeEvent.fHits->";

// Setup analysis
//    InSANEClusterAnalysis * clusterAnalysis = new InSANEClusterAnalysis( clusterTree );
//    clusterAnalysis->SetClusterTree(ct);
//    ct->BuildIndex("fRunNumber","fEventNumber");
//    t->AddFriend(ct);
//
//
// Useful pointers
// Create sane events for detector tree
   SANEEvents * events = new SANEEvents( detectorTree );
   bcEvent = (events->BETA->fBigcalEvent);
   Int_t fNTrajectories;
   Int_t fEventNumber;
   TClonesArray * trajectories = new TClonesArray("InSANETrajectory",1);
// Setup detectors

   // BETA
   fBETADetector  = new BETADetectorPackage();
   fBETADetector->SetEventAddress(events->BETA);

   // Bigcal
   BigcalDetector * bcdet = new BigcalDetector(runnumber);
   bcdet->SetEventAddress( bcEvent );
   fBETADetector->fBigcalDetector  = bcdet;   /* bcdet->SetClusterEventAddress( clusterAnalysis->fClusterEvent );*/

   // Gas Cherenkov
   GasCherenkovDetector * gcdet = new GasCherenkovDetector(runnumber);
   gcdet->SetEventAddress( (GasCherenkovEvent*)events->BETA->fGasCherenkovEvent );
   fBETADetector->fCherenkovDetector  = gcdet;

   // Lucite Hodoscope
   LuciteHodoscopeDetector * lhdet = new LuciteHodoscopeDetector(runnumber);
   lhdet->SetEventAddress( (LuciteHodoscopeEvent*)events->BETA->fLuciteHodoscopeEvent );
   fBETADetector->fHodoscopeDetector  = lhdet;

// Setup Display

   BETAEventDisplay * display = new BETAEventDisplay();
   display->fWaitPrimitive=1;

   display->fBigcalHists->Add(bcdet->fEventADCHist);
   display->fBigcalHists->Add(bcdet->fEventHist);
   display->fBigcalHists->Add(bcdet->fEventHistWithThreshold);
   display->fBigcalHists->Add(bcdet->fEventTimingHist);
   display->fBigcalHists->Add(bcdet->fEventTrigTimingHist);
   display->fBigcalHists->Add(bcdet->fChargedPiClustersHist);//fElectronClustersHists
   display->fBigcalHists->Add(bcdet->fEventTimingHistGood);

   display->fLuciteHists->Add(lhdet->fLuciteHitDisplay);
   display->fLuciteHists->Add(lhdet->fLuciteGoodHitDisplay);
   display->fLuciteHists->Add(lhdet->fEventDisplayTimedHitDifferences);
   display->fLuciteHists->Add(lhdet->fEventDisplayTimedHitSums);
   display->fLuciteHists->Add(fBETADetector->fEventLuciteTimeSum);

   display->fCherenkovHists->Add(gcdet->fCerHits);
   display->fCherenkovHists->Add(gcdet->fCerHitsGood);
   display->fCherenkovHists->Add(lhdet->fRunDisplayTimedHitDifferences);
   display->fCherenkovHists->Add(lhdet->fRunDisplayTimedHitSums);
   display->fCherenkovHists->Add(fBETADetector->fRunLuciteTimeSum);

   trajt->BuildIndex("fRunNumber","fEventNumber");
   trajt->SetBranchAddress("fTrajectories",&trajectories);
   trajt->SetBranchAddress("fNTrajectories",&fNTrajectories);
   trajt->SetBranchAddress("fEventNumber",&fEventNumber);
   t->AddFriend(trajt);
//    ct->AddFriend(trajt);

  gROOT->ProcessLine(Form(".! mkdir plots/%d/eventdisplay",runnumber));

// Loop through events
   for(int i = 0; i < t->GetEntries() && i < 5000 ;i++) {
      std::cout << "event " << i << "\n";
      t->GetEntry(i);
      fBETADetector->SetReferenceTDC(events->TRIG->fTriggerTDC);
      gcdet->ProcessEvent();
      bcdet->ProcessEvent();
      lhdet->ProcessEvent();
      fBETADetector->ProcessEvent();

if( gcdet->fCherenkovHit && events->BETA->fBigcalEvent->fTotalEnergyDeposited > 10000.0 ) {

    std::cout << "fBigcalEvent->fTotalEnergyDeposited = " << events->BETA->fBigcalEvent->fTotalEnergyDeposited << "\n";
      display->UpdateDisplay();

      display->fCanvas->SaveAs(Form("plots/%d/eventdisplay/event%d.png",runnumber,i));
   }
      bcdet->ClearEvent();
      lhdet->ClearEvent();
      gcdet->ClearEvent();
      fBETADetector->ClearEvent();

   }


//   TCanvas * c = new TCanvas("LuciteVsCherenkov","Lucite VS Cherenkov",800,600);
//    c->Divide(2,2);
//    c->cd(1);
//    t->Draw("fGasCherenkovEvent->fHits->fTDC>>cer1tdc(250,-5000,0)",
//             "fGasCherenkovEvent->fHits->fLevel==1&&fGasCherenkovEvent->fHits->fMirrorNumber==2");
//    c->cd(2);
//    t->Draw("fLuciteHodoscopeEvent->fHits->fTDC>>luc1tdc(250,-5000,0)",
//             "fLuciteHodoscopeEvent->fHits->fLevel==1&&fLuciteHodoscopeEvent->fHits->fPMTNumber==2");
//    c->cd(3);
//    t->Draw("fGasCherenkovEvent->fHits[Iteration$]->fTDC:fLuciteHodoscopeEvent->fHits[Iteration1$]->fTDC>>lucVsCerTdc(250,-5000,0,250,-5000,0)",
//             "fLuciteHodoscopeEvent->fHits->fLevel==1&&fLuciteHodoscopeEvent->fHits->fPMTNumber==2&&fGasCherenkovEvent->fHits->fLevel==1","colz");
//    c->cd(4);
// //    t->Draw("fLuciteHodoscopeEvent->fHits->fTDC>>luc1tdc(250,-5000,0)",
// //             "fLuciteHodoscopeEvent->fHits->fLevel==1&&fLuciteHodoscopeEvent->fHits->fPMTNumber==2");

   trajt->StartViewer();

   return(0);
}