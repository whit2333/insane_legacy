/**  First Pass Analysis script
 * 
 */
Int_t clustering(Int_t runNumber = 72994) {
  gROOT->SetBatch(kTRUE);

  const char * sourceTreeName = "betaDetectors0";
  const char * outputTreeName = "betaDetectors1";

// First open the run
   rman->SetRun(runNumber);

  if ( !rman->fCurrentRun) {printf("Please use SetRun(#) first"); return(-1);}

// Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
  SANEFirstPassAnalyzer * analyzer = new SANEFirstPassAnalyzer(outputTreeName ,sourceTreeName);

//   GasCherenkovCalculation1 * g1;
//   g1 = new GasCherenkovCalculation1();
//   g1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//   g1->SetDetectors(analyzer);
// // 
//   LuciteHodoscopeCalculation1 * l1;
//   l1 = new LuciteHodoscopeCalculation1();
//   l1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//   l1->SetDetectors(analyzer);

  BigcalClusteringCalculation1 * b1 = new BigcalClusteringCalculation1();
  BigcalClusteringCalculation1 * b2 = new BigcalClusteringCalculation1();

//   BigcalPi0Calaculations * b2 = new BigcalPi0Calaculations();
//   SANETrajectoryCalculation1 * traj1 = new SANETrajectoryCalculation1();
//   traj1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//   traj1->SetDetectors(analyzer);

  LocalMaxClusterProcessor * localMaxClusterProcessor = new LocalMaxClusterProcessor();
  KMeansClusterProcessor * kmeansClusterProcessor = new KMeansClusterProcessor();

  TTree * t1 = new TTree("ClustersB","Cluster Data");
/*  TTree * t2 = new TTree("ClustersC","Cluster Data");*/
  analyzer->SetClusterTree( t1 );

  b1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
  b1->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
  b1->SetClusterProcessor((BIGCALClusterProcessor*)kmeansClusterProcessor);

//   b2->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//   b2->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
//   b2->SetClusterProcessor(kmeansClusterProcessor); // not need for b2
// 

  analyzer->SetClusterProcessor((BIGCALClusterProcessor*)b1->GetClusterProcessor());

/*  b2->SetDetectors(analyzer);*/
  b1->SetDetectors(analyzer);

  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fCherenkovDetector); 
  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fBigcalDetector); 
  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fHodoscopeDetector); 
  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fTrackerDetector); 

//   analyzer->AddCalculation(g1);
//   analyzer->AddCalculation(l1);
  analyzer->AddCalculation(b1);
/*  analyzer->AddCalculation(b2);*/
//   analyzer->AddCalculation(traj1);

  analyzer->Initialize();

  analyzer->Process();


  delete analyzer;
  delete b1;
/*  delete b2;*/
//   delete l1;
//   delete g1;
  new TBrowser;
/// \todo this should go to a higher level
//  CreateNewFile();
/*   return(0);*/
//    
/*gROOT->ProcessLine(".q");*/
}
