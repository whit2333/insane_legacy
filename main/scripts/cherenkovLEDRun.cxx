/**  Analyzes a SANE Cherenkov LED RUn
 */
Int_t cherenkovLEDRun(Int_t runnumber=72137,Int_t isACalibration=0) {

//   TFile * f = new TFile(Form("data/rootfiles/InSANE%d.*.root",runnumber),"READ");
//   if(!f) { printf("file not found. \n"); return(-1); }
  TTree * t = (TTree*)gROOT->FindObject("betaDetectorsZerothPass");
  if(!t) {printf("tree not found\n"); return(-1); }

  TFile*f = new TFile(Form("data/rootfiles/LED%d.root",runnumber),"UPDATE");
     gROOT->SetBatch(kTRUE);
TSQLServer * db = SANEDatabaseManager::GetManager()->dbServer;
TString SQLCOMMAND(""); 

  TCanvas * c = new TCanvas("cherenkovLED","Cherenkov LED",400,800);
  c->Divide(2,4);
  TH1F * aMirrorHist;
  TFitResultPtr fitResult;
  TF1 *f1 = new TF1("f1", "gaus", 0,6000);
  f1->SetParameter(0,1000);
  f1->SetParameter(1,500);
  f1->SetParameter(2,300);

  for(int i = 0; i<8;i++) {
    c->cd(i+1);
    t->Draw(Form("betaDetectorEvent.fGasCherenkovEvent->fHits->fADC>>mirror%dhist(100,5,2005)",i+1),Form(
            "betaDetectorEvent.fGasCherenkovEvent->fHits->fLevel==0&&betaDetectorEvent.fGasCherenkovEvent->fHits->fMirrorNumber==%d",i+1),"",1000000000,  1001);
    aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist",i+1));
    f1->SetParameter(1,aMirrorHist->GetMean(1));

    fitResult = aMirrorHist->Fit("gaus","M,E,S");

 SQLCOMMAND ="REPLACE into cherenkov_calibrations set `run_number`=";
 SQLCOMMAND += runnumber;

 SQLCOMMAND +=",pmt_number="; 
 SQLCOMMAND +=i+1;

 SQLCOMMAND +=",value="; 
 SQLCOMMAND +=fitResult->Parameter(1);

 SQLCOMMAND +=",calibration_type="; 
 SQLCOMMAND +="'relative'";

 SQLCOMMAND +=" ;"; 
 if(db) db->Query(SQLCOMMAND.Data());

    aMirrorHist->SetTitle(Form("Cherenkov Mirror %d",i+1));
    aMirrorHist->Draw();


  }

  c->SaveAs(Form("plots/%d/CherenkovLED.ps",runnumber));
  c->SaveAs(Form("plots/%d/CherenkovLED.jpg",runnumber));

//     gROOT->SetBatch(kFALSE);


f->Write();
f->Close();
//Double_t par1 = r->Parameter(1);


return(0);
}
