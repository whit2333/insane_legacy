Int_t pi0EventDisplay(Int_t runnumber = 72994) {

  rman->SetRun(runnumber);

  TTree * t = (TTree*)gROOT->FindObject("betaDetectors1");
   if( !t ) {
     printf("Tree:%s \n       was NOT FOUND. Aborting... \n","betaDetectorsZerothPass"); 
     return(-1);
   }
  TTree * t = (TTree*)gROOT->FindObject("betaDetectors1");
//   TTree * ct = (TTree*)gROOT->FindObject("Clusters");
//   ct->BuildIndex();
//   t->AddFriend(ct);

   BigcalEvent * bcEvent = 0; 

   SANEEvents * events = new SANEEvents("betaDetectors1");
   bcEvent = (events->BETA->fBigcalEvent);

   BigcalDetector * bcdet = new BigcalDetector(runnumber);
   bcdet->SetEventAddress( bcEvent );

   Pi0EventDisplay * display = new Pi0EventDisplay();
   bcdet->fEventADCHist->SetOption("colz");
   display->fDataHists->Add(bcdet->fEventADCHist);

   for(int i = 1000; i < t->GetEntries() && i < 2000 ;i++) {
      std::cout << "event " << i << "\n";
      t->GetEntry(i);
      bcdet->ProcessEvent();
      display->UpdateDisplay();
      bcdet->ClearEvent();
   }
  t->Draw(">>all","fBigcalEvent->fEventNumber>1000");
  TEventList *elist = (TEventList*)gDirectory->Get("all");
  t->SetEventList(elist);

   
/*     */
/*  t->SetBranchAddress("fBigcalEvent",&bcEvent) */

//   t->Draw(">>all",Form("fBigcalEvent->fRunNumber==%d",runnumber));
//   TEventList *elist = (TEventList*)gDirectory->Get("all");
//   t->SetEventList(elist);
//   t->Draw("fBigcalEvent->fEventNumber");



//   t->GetEntry(elist->GetIndex(40));

  t->StartViewer();

  


// Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
//   SANEFirstPassAnalyzer * analyzer = new SANEFirstPassAnalyzer();
// 
// // Get the Pi0 Event display
//   Pi0EventDisplay * anEventDisplay ;
// 
//   analyzer->InSANEAnalyzer::fEventDisplay  = new Pi0EventDisplay();
// 
//   InSANETriggerEvent * aTriggerEvent = analyzer->InSANEDetectorAnalysis::fEvents->TRIG;
// // Set the trigger event for selecting the events to display
//   analyzer->InSANEAnalyzer::fEventDisplay->SetTriggerEvent( aTriggerEvent );
// // Add histograms to event display
//   analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fBigcalDetector->fEventTimingHist);
//   analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fBigcalDetector->fEventTrigTimingHist);
//   analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fCherenkovDetector->fCerHits);
//   analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fTrackerDetector->fEventHitsY1);
//   analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fTrackerDetector->fEventHitsY2);
//   analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fTrackerDetector->fEventHitsX);
//   analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(analyzer->SANEClusteringAnalysis::fBigcalDetector->fEventADCHist);
// 
// 
// // Output
//   analyzer->SetOutputTree( new TTree("Clusters","Cluster Data") );
// 
// ///////////////////////
// // Analysis Calculations
// 
// // Gas Cherenkov
//   GasCherenkovCalculation1 * g1 = new GasCherenkovCalculation1();
//     g1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//     g1->SetDetectors(analyzer);
// // Lucite Hodoscope
//   LuciteHodoscopeCalculation1 * l1 = new LuciteHodoscopeCalculation1();
//     l1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//     l1->SetDetectors(analyzer);
// // Bigcal Clustering
//   BigcalClusteringCalculation1 * b1 = new BigcalClusteringCalculation1();
//   LocalMaxClusterProcessor * clusterProcessor = new LocalMaxClusterProcessor();
// 
// 
// // Bigcal Pi0
//   BigcalPi0Calaculations * b2 = new BigcalPi0Calaculations(new TTree("pi0results","pi0 Reconstruction"));
// 
//     b1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//     b1->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
//     b1->SetClusterProcessor(clusterProcessor);
//     b2->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//     b2->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
// //     b2->SetClusterProcessor(clusterProcessor); // not need for b2
// 
//   b1->SetDetectors(analyzer);
//   b2->SetDetectors(analyzer);
// 
//   analyzer->SetClusterProcessor((BIGCALClusterProcessor*)b1->GetClusterProcessor());
// 
//   analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fCherenkovDetector); 
//   analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fBigcalDetector); 
//   analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fTrackerDetector); 
// 
//   analyzer->AddCalculation(g1);
//   analyzer->AddCalculation(l1);
//   analyzer->AddCalculation(b1);
//   analyzer->AddCalculation(b2);
// 
// 
// 
//   analyzer->InSANEAnalyzer::fEventDisplay->AddDisplayDatum("Trigger ",aTriggerEvent,1);
//   analyzer->InSANEAnalyzer::fEventDisplay->AddDisplayDatum("Pi0mass ",b2->fPi0Event,1);
// 
//   analyzer->InSANEAnalyzer::fEventDisplay->fDataHists->Add(clusterProcessor->GetSourceHistogram());
// 
// 
//   analyzer->Initialize(t);
//   analyzer->Process();
// 
//   delete analyzer;
//   delete b1;
//   delete b2;
//   delete l1;
//   delete g1;

   return(0);
}
