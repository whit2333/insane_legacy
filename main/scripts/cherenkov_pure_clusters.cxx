Int_t cherenkov_pure_clusters() {
   TChain chain("data/rootfiles/InSANE72994.*.root/Clusters","ClustersChain");
   chain.Add("data/rootfiles/InSANE72999.*.root/Clusters");
   chain.Add("data/rootfiles/InSANE72995.*.root/Clusters");
   chain.Add("data/rootfiles/InSANE73005.*.root/Clusters");
   chain.Add("data/rootfiles/InSANE73031.*.root/Clusters");

   TCut cClusterEnergy= "bigcalClusters.fTotalE[Iteration$] > 1500";

   TCut cCherenkov1 = "bigcalClusters.fNumberOfMirrors[Iteration$]==1";
   TCut cCherenkovMirror = "bigcalClusters.fPrimaryMirror[Iteration$]==8";

   TCut cCherenkovTDC = "TMath::Abs(bigcalClusters.fCherenkovTDC[Iteration$]) < 10";

   TCut cTrigger = "bigcalClusters.fType[Iteration$]==2";

   std::cout << "Chain has " << chain.GetEntries() << " entries.\n";

   TCanvas * c = new TCanvas("cherenkov","cherenkov");
   c->Divide(2,2);
   c->cd(1);
   chain.Draw("bigcalClusters.fCherenkovBestNPESum>>hCerPure(100,0,40)",cCherenkov1 && cTrigger && cClusterEnergy && cCherenkovMirror ,"");
   c->cd(2);
   chain.Draw("bigcalClusters.fCherenkovTDC>>hCerPureTDC(100,-10,10)",cCherenkov1 && cTrigger && cClusterEnergy && cCherenkovMirror  ,"");
   c->cd(3);
   chain.Draw("bigcalClusters.fCherenkovBestNPESum>>hCerPureWithTDCCut(100,0,40)",cCherenkov1 && cTrigger && cClusterEnergy && cCherenkovMirror &&cCherenkovTDC );
   c->cd(4);
   chain.Draw("bigcalClusters.fCherenkovTDC>>hCerPureTDCWithTDCCut(100,-10,10)",cCherenkov1 && cTrigger && cClusterEnergy && cCherenkovMirror&&cCherenkovTDC  ,"");
   std::cout << "Chain has " << chain.GetEntries() << " entries.\n";


   c->SaveAs("results/efficiencies/cerEff.clustE1.5GeV.png");

return(0);
}