Int_t scalersToTree(Int_t runnumber=72999) {

  // This script takes the text file and fills a simiple tree with all the events
  TFile * f = new TFile(Form("data/rootfiles/scalers%d.root",runnumber),"RECREATE");

  Long64_t fScalers[582];
  //Long64_t fScalers[352];
  Long64_t fRunTime;
  Long64_t fEventNumber;

  f->cd();
  TTree *T = new TTree("saneScalers","data from ascii txt file");
  Long64_t  nlines =  T->ReadFile( Form("InSANEOUT/scalers%d.txt",runnumber),
                           "fEventNumber/L:fRunTime/L:fScalers[582]/L");
  /// Added scalers at the end ( 170-230) becuase they are needed, spec 174,
  /// which is a 1MHz clock.
//                           "fEventNumber/L:fRunTime/L:fScalers[351]");
  //T->StartViewer();
  
  T->FlushBaskets();
  f->Write();
/*  f->Write();*/
  f->Close();

  // O
  rman->SetRun(runnumber);
  rman->GetCurrentFile()->Delete("Scalers;*");

  TFile * sf = rman->GetScalerFile();
  if(sf) sf->Delete("Scalers;*");
  TTree * scalerT = 0;
  sf->GetObject("saneScalers",scalerT);

  if(!scalerT) std::cout << "NO SCALER TREE!!\n";

  std::cout << " o Scaler tree has " << scalerT->GetEntries() << " events\n";
  AnalyzerScalersToInSANE * sel = new AnalyzerScalersToInSANE();
  std::cout << " o Processing AnalyzerScalersToInSANE !!\n";
  scalerT->Process(sel);

  return(0);
}
