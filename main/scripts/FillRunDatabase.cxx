Int_t FillRunDatabase() {
// This script is call from mainly from AnalyzerToInSANE through the InSANERun::FillDB()

  InSANERun * aRun = rman->GetCurrentRun();
 
    InSANEDatabaseManager * dbmanager = (InSANEDatabaseManager *)SANEDatabaseManager::GetManager();
    TString sqlDB("USE SANE; "); 
    dbmanager->ExecuteQuery(&sqlDB);
// 
    TDatime replayDateTime;
    TString sqlReplayeDatime( replayDateTime.AsSQLString() );
    TString aStartDateTime( aRun->start_datetime.AsSQLString() );
    TString aEndDateTime( aRun->end_datetime.AsSQLString() );


   TString SQLCOMMAND("INSERT INTO run_info SET ");
// 
  SQLCOMMAND +="Run_number=" ; 
  SQLCOMMAND +=aRun->fRunNumber ; 

  SQLCOMMAND +=",replay_date='" ; 
  SQLCOMMAND +=sqlReplayeDatime(0,10)  ; 
  SQLCOMMAND +="',replay_time='" ; 
  SQLCOMMAND +=sqlReplayeDatime(11,18)  ; 

  SQLCOMMAND +="',start_date='" ; 
  SQLCOMMAND +=aStartDateTime(0,10)  ; 
  SQLCOMMAND +="',start_time='" ; 
  SQLCOMMAND +=aStartDateTime(11,18)  ; 

  SQLCOMMAND +="',end_date='" ; 
  SQLCOMMAND +=aEndDateTime(0,10)  ; 
  SQLCOMMAND +="',end_time='" ; 
  SQLCOMMAND +=aEndDateTime(11,18)  ; 

  SQLCOMMAND +="',number_of_events=" ; 
  SQLCOMMAND +=aRun->fNEvents;
  SQLCOMMAND +=",number_of_coin_events=" ; 
  SQLCOMMAND +=aRun->fNCoinEvents;
  SQLCOMMAND +=",number_of_beta2_events=" ; 
  SQLCOMMAND +=aRun->fNBeta2Events;
  SQLCOMMAND +=",number_of_beta1_events=" ; 
  SQLCOMMAND +=aRun->fNBeta1Events;
  SQLCOMMAND +=",number_of_pi0_events=" ; 
  SQLCOMMAND +=aRun->fNPi0Events;
  SQLCOMMAND +=",number_of_hms_events=" ; 
  SQLCOMMAND +=aRun->fNHmsEvents;
  SQLCOMMAND +=",number_of_LED_events=" ; 
  SQLCOMMAND +=aRun->fNLedEvents;
//   SQLCOMMAND +=",number_of_Ped_events=" ; 
//   SQLCOMMAND +=aRun->fNPedEvents;

  SQLCOMMAND +="  ON DUPLICATE KEY UPDATE " ; 
  SQLCOMMAND +="Run_number=" ; 
  SQLCOMMAND +=aRun->fRunNumber ; 

  SQLCOMMAND +=",replay_date='" ; 
  SQLCOMMAND +=sqlReplayeDatime(0,10)  ; 
  SQLCOMMAND +="',replay_time='" ; 
  SQLCOMMAND +=sqlReplayeDatime(11,18)  ; 

  SQLCOMMAND +="',start_date='" ; 
  SQLCOMMAND +=aStartDateTime(0,10)  ; 
  SQLCOMMAND +="',start_time='" ; 
  SQLCOMMAND +=aStartDateTime(11,18)  ; 

  SQLCOMMAND +="',end_date='" ; 
  SQLCOMMAND +=aEndDateTime(0,10)  ; 
  SQLCOMMAND +="',end_time='" ; 
  SQLCOMMAND +=aEndDateTime(11,18)  ; 

  SQLCOMMAND +="',number_of_events=" ; 
  SQLCOMMAND +=aRun->fNEvents;
  SQLCOMMAND +=",number_of_coin_events=" ; 
  SQLCOMMAND +=aRun->fNCoinEvents;
  SQLCOMMAND +=",number_of_beta2_events=" ; 
  SQLCOMMAND +=aRun->fNBeta2Events;
  SQLCOMMAND +=",number_of_beta1_events=" ; 
  SQLCOMMAND +=aRun->fNBeta1Events;
  SQLCOMMAND +=",number_of_pi0_events=" ; 
  SQLCOMMAND +=aRun->fNPi0Events;
  SQLCOMMAND +=",number_of_hms_events=" ; 
  SQLCOMMAND +=aRun->fNHmsEvents;
  SQLCOMMAND +=",number_of_LED_events=" ; 
  SQLCOMMAND +=aRun->fNLedEvents;
  SQLCOMMAND +=" ;" ; 
// 
  std::cout << SQLCOMMAND.Data() << "\n";
  TSQLResult * aResult = dbmanager->ExecuteQuery(&SQLCOMMAND);

//  gROOT->ProcessLine(".x scripts/RunAppend.cxx ");

  return(0);
}
