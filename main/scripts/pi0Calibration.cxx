Int_t pi0Calibration(Int_t runNumber=72994) {
  // This script executes a pi0 calibration for a whole run.
//   gROOT->SetBatch(kTRUE);

  gROOT->ProcessLine(".except ");

  std::vector<Int_t> fRunList;
  fRunList.push_back(runNumber);
//   fRunList.push_back(72999);
//   fRunList.push_back(73001);

  const char * sourceTreeName = "Clusters";
  const char * outputTreeName = "Pi0CalibEigen";

  BigcalPi0Calibrator * calibrator = 0;
  Pi0Calibration * cal1 = 0;

  for (std::vector<Int_t>::iterator it = fRunList.begin(); it!=fRunList.end(); ++it) {
    cout << *it << endl;
    rman->SetRun(*it);
    TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
    if( !t ) {
       printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName); 
       return(-1);
    }
/// First run initialization
    if(it == fRunList.begin()) {
       calibrator = new BigcalPi0Calibrator( outputTreeName, sourceTreeName );
       std::cout << " first run\n";
       cal1 = new Pi0Calibration();
       cal1->fjMax=31+4;
       cal1->fjMin=31-4;
       cal1->fiMin=5;
       cal1->fiMax=12;
       cal1->SetBlockList();
       calibrator->AddCalculation( cal1 );
    }
    calibrator->fInputTree = t;
    calibrator->SetClusterTree(t);
    cal1->SetClusterEvent(calibrator->fClusterEvent);
    if(it == fRunList.begin()) {
       calibrator->Initialize(t);
    }
  calibrator->Process();

  }


/*
  t->Draw(">>pi0List","bigcalClusterEvent.fNClusters==2","entrylist");
  TEntryList *pi0List = (TEntryList*)gDirectory->Get("pi0List");
  t->SetEntryList(pi0List);*/





  cal1->ExecuteCalibration();

//   cal1->ViewStatus();

return(0);
}