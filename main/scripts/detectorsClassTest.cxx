{
// Detectors used
std::cout << "=== Creating detector configurations for run 72994 ===\n";
  ForwardTrackerDetector *    tracker   = new ForwardTrackerDetector(72994);
  GasCherenkovDetector *      cherenkov = new GasCherenkovDetector(72994);
  LuciteHodoscopeDetector *   hodoscope = new LuciteHodoscopeDetector(72994);
  BigcalDetector *            bigcal    = new BigcalDetector(72994);

std::cout << "Tracker has "
          << tracker->GetNumberOfPedestals() << " pedestals and " 
          << tracker->GetNumberOfTDCs() << " TDCs. \n";

std::cout << "Cherenkov has "
          << cherenkov->GetNumberOfPedestals() << " pedestals and " 
          << cherenkov->GetNumberOfTDCs() << " TDCs. \n";

std::cout << "Hodoscope has "
          << hodoscope->GetNumberOfPedestals() << " pedestals and " 
          << hodoscope->GetNumberOfTDCs() << " TDCs. \n";

std::cout << "Bigcal has "
          << bigcal->GetNumberOfPedestals() << " pedestals and " 
          << bigcal->GetNumberOfTDCs() << " TDCs. \n";


}