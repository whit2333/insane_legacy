Int_t plot_bigcal_calibrations(Int_t runNumber = 72994) {

  rman->SetRun(runNumber);

BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();

TH2F * calibCoeffHist = new TH2F("BigcalCalibCoeff","Bigcal Calibration Coefficients",32,1,33,56,1,57);
  calibCoeffHist->Reset();
    for(int j = 1 ; j<=56 ; j++) {
  for(int i = 1 ; i<=32 ; i++) {
      if( !(i>=30 && j>=33) ) {
        calibCoeffHist->Fill(i,j,bcgeo->GetCalibrationCoefficient(i,j));
        std::cout << "Block("<< i << "," << j << ") coeff: " << bcgeo->GetCalibrationCoefficient(i,j) << std::endl;
      }
    }
  }
  calibCoeffHist->SetMaximum(3.0);
  calibCoeffHist->Draw("colz");

//   bcgeo->PrintCalibrationCoefficients();

/*
  const char * sourceTreeName = "Clusters";
  const char * outputTreeName = "Pi0CalibEigen";


  TTree * tSource = (TTree*)gROOT->FindObject(sourceTreeName);
  if( !tSource ) {
     printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName); 
     return(-1);
  }

rman->fCurrentFile->cd();

  BigcalPi0Calibrator * calibrator = new BigcalPi0Calibrator(outputTreeName,sourceTreeName);
  calibrator->SetClusterTree(tSource);

  Pi0Calibration * cal1 = new Pi0Calibration();
  cal1->SetClusterEvent(calibrator->fClusterEvent);

  calibrator->AddCalculation( cal1 );

  calibrator->Initialize(tSource);
  calibrator->Process();*/
//   TH1D * fLHist = new TH1D("fLHist","L Vector",1744,1,1745);
// fLHist->FillN(1744,cal1->fBlocks,(Double_t*)cal1->fL.GetMatrixArray());
// fLHist->Draw();

/*
  t->Draw(">>pi0List","bigcalClusterEvent.fNClusters==2","entrylist");
  TEntryList *pi0List = (TEntryList*)gDirectory->Get("pi0List");
  tSource->SetEntryList(pi0List);*/


//   ROOT::Math::RichardsonDerivator rd;
//   rd.SetFunction(photonE);
// 
//   std::cout << "Derivative of function inheriting from "
//             << "IGenFunction f(x) = 2 + 3x + 4x^2 at x = 2" << std::endl;
//   std::cout << "First Derivative:   " << rd.Derivative1(x0) << std::endl;
//   std::cout << "Second Derivative:  " << rd.Derivative2(x0) << std::endl;
//   std::cout << "Third Derivative:   " << rd.Derivative3(x0) << std::endl;


}

