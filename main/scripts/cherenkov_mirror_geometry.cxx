/** Finds the chenkov mirror correlations  with hodoscope bars
 */
Int_t cherenkov_mirror_geometry(Int_t runNumber=72994){

   rman->SetRun(runNumber);



////// HISTOGRAMS ///////
TH1F * cer_adc_hist[12];
TH1F * cer_tdc_hist[12];

TH1F * cer_adc_hist_wIndTDCCut[12];
TH1F * cer_tdc_hist_wIndTDCCut[12];

TH1F * cer_adc_hist_wSumTDCCut[12];
TH1F * cer_tdc_hist_wSumTDCCut[12];

TH1F * cer_adc_hist_wIndTDCCut_wSumThreshCut[12];
TH1F * cer_tdc_hist_wIndTDCCut_wSumThreshCut[12];

TH1F * cer_adc_hist_wSumTDCCut_wSumThreshCut[12];
TH1F * cer_tdc_hist_wSumTDCCut_wSumThreshCut[12];

TH1F * cer_adc_hist_wIndTDCCut_wIndThreshCut[12];
TH1F * cer_tdc_hist_wIndTDCCut_wIndThreshCut[12];

TH1F * cer_adc_hist_wSumTDCCut_wIndThreshCut[12];
TH1F * cer_tdc_hist_wSumTDCCut_wIndThreshCut[12];

for(Int_t i=0;i<12;i++) {
     cer_adc_hist[i] = new TH1F(Form("cer_adc_hist_%d",i),Form("Cherenkov ADC %d",i), 100,-1000,3000);
     cer_adc_hist_wIndTDCCut[i] = new TH1F(Form("cer_adc_hist_%d_wIndTDCCut",i),Form("Cherenkov ADC %d w/ Indiv. TDC cuts",i), 100,-1000, 3000);
     cer_tdc_hist[i] = new TH1F(Form("cer_tdc_hist_%d",i),Form("Cherenkov TDC %d",i), 100,-3000,-1000);
     cer_tdc_hist_wIndTDCCut[i] = new TH1F(Form("cer_tdc_hist_%d_wIndTDCCut",i),Form("Cherenkov TDC %d w/ Indiv. TDC cuts",i), 100,-3000, -1000);
}

  TClonesArray * cerHits = new TClonesArray ("GasCherenkovHit",20) ;
  cerHits = aBETAEvent->fGasCherenkovEvent->fHits ;
  cerHits->ExpandCreateFast (1) ; 
  GasCherenkovHit * aCerHit = new GasCherenkovHit();
//// Analysis Flags /////
bool odd_ceradc_hit = false;

///// CUT PARAMETERS /////
int tdclow = -2750;
int tdchigh = -1000;
int k,kk; // associated with tdc loops
int tempTDC;
int tempADC;
int tempMirrorNumber;

///// EVENT LOOP  //////
Int_t nevent = inputTree->GetEntries();
cout << "\n" << nevent << " ENTRIES \n";
   for (int iEVENT=0;iEVENT<nevent;iEVENT++) 
    {
    inputTree->GetEvent(iEVENT);

   if ( aBETAEvent->gen_event_type==5 && aBETAEvent->gen_event_trigtype[3]   ) {


///// CHERENKOV //////
// Begin by checking and printing obvious stuff
odd_ceradc_hit = false;
//    cout <<  aBETAEvent->fGasCherenkovEvent->fNumberOfHits << " HITS \n";

// loop over cherenkov hits for event
for (kk=0; kk< aBETAEvent->fGasCherenkovEvent->fNumberOfHits;kk++) 
  {
    aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
/*    cout <<  aCerHit->fADC << " ADC \n";
    cout <<  aCerHit->fTDC << " TDC \n";
    cout <<  aCerHit->fMirrorNumber << " MirrorNumber \n";
    cout <<  aCerHit->fPMTNumber[0] << " PMTNumber \n";
*/  
    cer_adc_hist[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);

    if( aCerHit->fTDC > tdclow && aCerHit->fTDC < tdchigh) 
      { //passes very wide tdc cut 
//if(odd_ceradc_hit) printf("cer_num[k] - 1 =%d\n",tempMirrorNumber);
      cer_adc_hist_wIndTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);
      cer_tdc_hist_wIndTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDC);
      }

  } // Trigger type 
} // End loop over Cherenkov signals
    

    } // END OF EVENT LOOP

//;outputTree->Write();
//outputTree->StartViewer();
//newFile->Write();

TCanvas * c = new TCanvas();
c->Divide(4,3);
for(i=0;i<12;i++) {
c->cd(i+1);
cer_adc_hist_wIndTDCCut[i]->Draw();
}

TCanvas * c2 = new TCanvas();
c2->Divide(4,3);
for(i=0;i<12;i++) {
c2->cd(i+1);
cer_tdc_hist_wIndTDCCut[i]->Draw();
}


}
