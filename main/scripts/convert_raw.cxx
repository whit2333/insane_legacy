Int_t convert_raw(Int_t runnum = 72999) {

   if(rman->GetVerbosity() > 0) std::cout << " o Starting convert_raw\n";

   rman->SetRun(runnum);

   Int_t result = 0;

   result = rman->ConvertRawData();

   if(rman->GetVerbosity() > 0) std::cout << " o rman->ConvertRawData() Finished with " << result << "\n";

   result = rman->CalculatePedestals();

   if(rman->GetVerbosity() > 0) std::cout << " o rman->CalculatePedestals() Finished with " << result << "\n";

   result = rman->CalculateTiming();

   if(rman->GetVerbosity() > 0) std::cout << " o rman->CalculateTiming() Finished with " << result << "\n";

   rman->WriteRun();

   gROOT->ProcessLine(".q");
   return(0);
}


