/*!  Builds the Parallel neural network
     From the simulation run number, this script builds a NN 
     class called NNPerpendicular. 
 */
Int_t BuildParaNN(Int_t runnumber=1015/*923*/) {

   rman->SetRun(runnumber);


   SANEEvents * events = new SANEEvents("betaDetectors1");
/*   events->SetClusterBranches(events->fTree);*/
//    InSANEClusterAnalysis * clusterAnalysis = new InSANEClusterAnalysis("Clusters");
// Build index for each tree and make them friends
//    clusterAnalysis->SetClusterTree((TTree*)gROOT->FindObject("Clusters"));
//    events->fTree->BuildIndex("betaDetectorEvent.fRunNumber","betaDetectorEvent.fEventNumber");
//    clusterAnalysis->fClusterTree->BuildIndex("bigcalClusterEvent.fRunNumber","bigcalClusterEvent.fEventNumber");
//    events->fTree->AddFriend("Clusters");

// Create a tree for training the Neural network
   Double_t energy,energy_cluster;
   Double_t theta,phi;
   Double_t x_cluster,y_cluster; 
   Double_t xsigma_cluster,ysigma_cluster;
   Double_t xskew_cluster,yskew_cluster;

   Double_t delta_energy,delta_theta,delta_phi;

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloThrownParticle * thrown = 0;
   InSANEFakePlaneHit * bigcalPlane = 0;

   TTree* nnt = new TTree("nnt","training tree");
    nnt->Branch("energy", &energy, "energy/D");
    nnt->Branch("theta", &theta, "theta/D");
    nnt->Branch("phi", &phi, "phi/D");
    nnt->Branch("delta_energy", &delta_energy, "delta_energy/D");
    nnt->Branch("delta_theta", &delta_theta, "delta_theta/D");
    nnt->Branch("delta_phi", &delta_phi, "delta_phi/D");
    nnt->Branch("energy_cluster", &energy_cluster, "energy_cluster/D");
    nnt->Branch("x_cluster", &x_cluster, "x_cluster/D");
    nnt->Branch("y_cluster", &y_cluster, "y_cluster/D");
    nnt->Branch("xsigma_cluster", &xsigma_cluster, "xsigma_cluster/D");
    nnt->Branch("ysigma_cluster", &ysigma_cluster, "ysigma_cluster/D");
    nnt->Branch("xskew_cluster", &xskew_cluster, "xskew_cluster/D");
    nnt->Branch("yskew_cluster", &yskew_cluster, "yskew_cluster/D");


   for (int i = 0;i< events->fTree->GetEntries();i++ ) if(i>=1000){

     events->fTree->GetEntry(i);
//      clusterAnalysis->fClusterTree->GetEntryWithIndex(events->BETA->fRunNumber,events->BETA->fEventNumber);

     if( events->CLUSTER->fClusters->GetEntries() > 0 && events->MC->fThrownParticles->GetEntries() > 0){


// std::cout << "got here !!! \n";

     thrown = (BETAG4MonteCarloThrownParticle*)(*(events->MC->fThrownParticles))[0];
     aCluster  = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[0];

     energy_cluster = aCluster->fTotalE;
     x_cluster = aCluster->GetXmoment();
     y_cluster = aCluster->GetYmoment();
     xsigma_cluster = aCluster->GetXStdDeviation();
     ysigma_cluster = aCluster->GetYStdDeviation();
     xskew_cluster = aCluster->GetXSkewness();
     yskew_cluster = aCluster->GetYSkewness();


     theta = thrown->fTheta;
     phi = thrown->fPhi;
     energy = thrown->fEnergy;

     delta_energy  = energy - energy_cluster;
     delta_phi     = phi - aCluster->GetPhi();
     delta_theta   = theta - aCluster->GetTheta();



//     std::cout << " energy " << energy << " cluster_energy " << energy_cluster << "\n";

     nnt->Fill();
     }
   }


   nnt->StartViewer();
   

// Maximum Block energy
///////////
  if (!gROOT->GetClass("TMultiLayerPerceptron")) {
     gSystem->Load("libMLP");
  }

   Int_t ntrain=500;
   Int_t nHidden = 4;
/////////// Here is the network:

/*    mlp =   new TMultiLayerPerceptron (Form("@energy_cluster,@x_cluster,@y_cluster,@xsigma_cluster,@ysigma_cluster,@xskew_cluster,@yskew_cluster:%d:energy,theta,phi",nHidden),nnt);*/

    mlp =   new TMultiLayerPerceptron (Form("@energy_cluster,@x_cluster,@y_cluster,@xsigma_cluster,@ysigma_cluster,@xskew_cluster,@yskew_cluster:%d:delta_energy,delta_theta,delta_phi",nHidden),nnt);

    mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);
    mlp->Train(ntrain, "text,graph,update=10");
  mlp->Export("NNParallelOUT");
  TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(2,2);
   TMLPAnalyzer ana(mlp);
   // Initialisation
   ana.GatherInformations();
   // output to the console
   ana.CheckNetwork();
   mlpa_canvas->cd(1);
   // shows how each variable influences the network
   ana.DrawDInputs();
   mlpa_canvas->cd(2);
   // shows the network structure
   mlp->Draw();
   mlpa_canvas->cd(3);
   // draws the resulting network
   ana.DrawNetwork(0,"energy>1","");
   mlpa_canvas->cd(4);
// OBSERVATION :
// NN does better without the maximum block positions (xy_max)
// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 



return(0);
}