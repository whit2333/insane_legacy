/*! Script used to gather tidbits of run info.

   This is where the most tedious data gather should occur...

 */
Int_t GatherRunInfo() {

   std::cout << " o Executing GatherRunInfo.cxx \n";

   InSANERun * aRun = rman->GetCurrentRun();
   Int_t runnumber  = aRun->GetRunNumber();


   InSANEDatabaseManager * DBM = InSANEDatabaseManager::GetManager();
   const char * fDatabase = "SANE.db";
   const char * fTable    = "run_info";
   InSANERunReportDatum * dat =0;

   dat = rman->GetRunReport()->GetDatum("iron_plate");
   if(dat){
      if( runnumber >= 72099 ) dat->fValue="1";
      else dat->fValue="0";
   }
   //    const char * fTable1 = "qe_polsource";
//    TString query1(
//         Form("SELECT qe FROM %s WHERE measured_datetime<'%s' ORDER BY measured_datetime DESC LIMIT 1 ;\n",
//            fTable1, 
//            aRun->fStartDatetime.AsSQLString() ) );
//    dat = rman->GetRunReport()->GetDatum("pol_source_qe");
//    std::cout << query1 << "\n";
//    if(dat){
//       dat->GatherInfo(&query1);
//       aRun->fQuantumEfficiency = atof(dat->fValue.Data());
//    }
//   std::cout << "query1 complete \n";
// OLD
//    stmt = serv->Statement(query1.Data(), 100);
//    if(stmt->Process()) {
//       stmt->StoreResult();
// // display info about selected field
// /*      cout << "NumFields = " << stmt->GetNumFields() << endl;*/
// //       for (int n=0;n<stmt->GetNumFields();n++) {
// //          std::cout << "Field " << n << "  = " << stmt->GetFieldName(n) << endl;
// //       }
//       while (stmt->NextResultRow()) {
// //              std::cout << stmt->GetDouble(0);
// //              std::cout << " ";
// //              std::cout <<  stmt->GetString(1);
// //              std::cout << "\n";
//              aRun->fQuantumEfficiency = stmt->GetDouble(0);
//              rman->GetRunReport()->SetValueOf("pol_source_qe",stmt->GetString(0));
//       }
//    }

//    const char * fTable2 = "beam_pass_number";
//    TString query2(Form(
//       "SELECT pass_number FROM %s WHERE pass_change_datetime<'%s' ORDER BY pass_change_datetime DESC LIMIT 1 ;\n",
//       fTable2,  aRun->fStartDatetime.AsSQLString() ));
//    dat = rman->GetRunReport()->GetDatum("beam_pass");
//    if(dat){
//       dat->GatherInfo(&query2);
// 
//       aRun->fBeamPassNumber = atoi(dat->fValue.Data());
//    }
//   std::cout << "query2 complete \n";

// OLD
//    stmt = serv->Statement(query2.Data(), 100);
//    if(stmt->Process()) {
//       stmt->StoreResult();
// // display info about selected field
// //       cout << "NumFields = " << stmt->GetNumFields() << endl;
// //       for (int n=0;n<stmt->GetNumFields();n++) {
// //          std::cout << "Field " << n << "  = " << stmt->GetFieldName(n) << endl;
// //       }
//       while (stmt->NextResultRow()) {
// //              std::cout << stmt->GetDouble(0);
// //              std::cout << " ";
// //              std::cout <<  stmt->GetString(1);
// //              std::cout << "\n";
//              aRun->fBeamPassNumber = stmt->GetDouble(0);
//              rman->GetRunReport()->SetValueOf("beam_pass",stmt->GetString(0));
//       }
//    }

   const char * fTable3 = "all_runs";
/*   TString query3(Form("SELECT ORIENTATIO,TARGET_TYP,TARGET_POS,POL_START,POL_STOP,COMP_DEAD_,PS1,PS2,PS3,PS4,PS5 FROM %s WHERE RUN_NUMBER=%d",
      fTable3, aRun->fRunNumber));*/
   TString query3(Form("SELECT ORIENTATIO FROM %s WHERE RUN_NUMBER=%d ;",
      fTable3, aRun->fRunNumber));
   dat = rman->GetRunReport()->GetDatum("target_sign");
   if(dat){
          if( !(dat->GatherInfo(&query3)) )
             dat->SetUpdated(true);

   }
   query3 = Form("SELECT TARGET_TYP FROM %s WHERE RUN_NUMBER=%d  ;",
      fTable3, aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("target_type");
   if(dat){
          if( !(dat->GatherInfo(&query3)) )
             dat->SetUpdated(true);

   }
   query3 = Form("SELECT TARGET_POS FROM %s WHERE RUN_NUMBER=%d  ;",
      fTable3, aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("target_cup");
   if(dat){
         dat->GatherInfo(&query3);
   }
   query3 = Form("SELECT COMP_DEAD_ FROM %s WHERE RUN_NUMBER=%d  ;",
      fTable3, aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("dead_time");
   if(dat){
       dat->GatherInfo(&query3);
   }
   query3 = Form("SELECT PS1 FROM %s WHERE RUN_NUMBER=%d ;",
      fTable3, aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("PS1");
   if(dat){
       dat->GatherInfo(&query3);
   }
   query3 = Form("SELECT PS2 FROM %s WHERE RUN_NUMBER=%d ;",
      fTable3, aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("PS2");
   if(dat){
       dat->GatherInfo(&query3);
   }
   query3 = Form("SELECT PS3 FROM %s WHERE RUN_NUMBER=%d ;",
      fTable3, aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("PS3");
   if(dat){
       dat->GatherInfo(&query3);
   }
   query3 = Form("SELECT PS4 FROM %s WHERE RUN_NUMBER=%d ;",
      fTable3, aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("PS4");
   if(dat){
       dat->GatherInfo(&query3);
   }
   query3 = Form("SELECT PS5 FROM %s WHERE RUN_NUMBER=%d ;",
      fTable3, aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("PS5");
   if(dat){
       dat->GatherInfo(&query3);
   }
//    query3 = Form("SELECT PS6 FROM %s WHERE RUN_NUMBER=%d ;",
//       fTable3, aRun->fRunNumber);
//    dat = rman->GetRunReport()->GetDatum("PS6");
//    if(dat){
//        dat->GatherInfo(&query3);
//    }
//   std::cout << "query3 complete \n";

// /*   std::cout << query3 << "\n";*/
//    stmt = serv->Statement(query3.Data(), 100);
//    if(stmt->Process()) {
//       stmt->StoreResult();
// // display info about selected field
// //       cout << "NumFields = " << stmt->GetNumFields() << endl;
// //       for (int n=0;n<stmt->GetNumFields();n++) {
// //          std::cout << "Field " << n << "  = " << stmt->GetFieldName(n) << endl;
// //       }
//       while (stmt->NextResultRow()) {
// //              std::cout << stmt->GetString(0);
// //              std::cout << " ";
// //              std::cout <<  stmt->GetString(1);
// //              std::cout << " ";
// //              std::cout <<  stmt->GetString(2);
// //              std::cout << "\n";
//              rman->GetRunReport()->SetValueOf("target_sign",stmt->GetString(0));
//              rman->GetRunReport()->SetValueOf("target_type",stmt->GetString(1));
//              rman->GetRunReport()->SetValueOf("target_cup",stmt->GetString(2));
//              rman->GetRunReport()->SetValueOf("dead_time",stmt->GetString(5));
// 
//       }
//    }

   const char * fTable4 = "exp_configuration";
TString query4( 
        Form( "SELECT sane_field_angle_theta FROM %s WHERE End_run >= %d AND Start_run <= %d",
       fTable4, aRun->fRunNumber,aRun->fRunNumber) );
   dat = rman->GetRunReport()->GetDatum("target_angle");
   if(dat){
      dat->GatherInfo(&query4);
      dat->SetUpdated(true);
   }
//   std::cout << "query4 complete \n";

/*   std::cout << query4 << "\n";*/
//    stmt = serv->Statement(query4.Data(), 100);
//    if(stmt->Process()) {
//       stmt->StoreResult();
// //display info about selected field
// //       cout << "NumFields = " << stmt->GetNumFields() << endl;
// //       for (int n=0;n<stmt->GetNumFields();n++) {
// //          std::cout << "Field " << n << "  = " << stmt->GetFieldName(n) << endl;
// //       }
//       while (stmt->NextResultRow()) {
// //              std::cout << stmt->GetString(0);
// //              std::cout << "\n";
//              rman->GetRunReport()->SetValueOf("target_angle",stmt->GetString(0));
//              aRun->fTargetAngle = stmt->GetDouble(0);
//       }
//    }



   const char * fTable5 = "target_material_runlist";
// TString query5( 
//         Form( "SELECT Cup_used,Insert_used,Pol_sign FROM %s WHERE End_run >= %d && Start_run <= %d",
//        fTable5, aRun->fRunNumber,aRun->fRunNumber) );
   TString query5( 
        Form( "SELECT Cup_used FROM %s WHERE End_run >= %d AND Start_run <= %d",
       fTable5, aRun->fRunNumber,aRun->fRunNumber) );
   dat = rman->GetRunReport()->GetDatum("target_cup");
   if(dat){
      dat->fValue = "None";
      if( !(dat->GatherInfo(&query5)))
         dat->SetUpdated(true);


   }
   query5 =
        Form( "SELECT Insert_used FROM %s WHERE End_run >= %d AND Start_run <= %d",
       fTable5, aRun->fRunNumber,aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("target_insert");
   if(dat){
      if( !(dat->GatherInfo(&query5)) )
         dat->SetUpdated(true);
   }
   query5 =
        Form( "SELECT Pol_sign FROM %s WHERE End_run >= %d AND Start_run <= %d",
       fTable5, aRun->fRunNumber,aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("target_pol_sign");
   if(dat){
      dat->fValue = "Unpolarized";
      if( !(dat->GatherInfo(&query5)) )
         dat->SetUpdated(true);

   }
//   std::cout << "query5 complete \n";

// /*   std::cout << query5 << "\n";*/
//    stmt = serv->Statement(query5.Data(), 100);
// 
//              rman->GetRunReport()->SetValueOf("target_pol_sign","Unpolarized");
//              rman->GetRunReport()->SetValueOf("target_cup","None");
// 
//    if(stmt->Process()) {
//       stmt->StoreResult();
// // display info about selected field
// //       cout << "NumFields = " << stmt->GetNumFields() << endl;
// //       for (int n=0;n<stmt->GetNumFields();n++) {
// //          std::cout << "Field " << n << "  = " << stmt->GetFieldName(n) << endl;
// //       }
//       while (stmt->NextResultRow()) {
// //              std::cout << stmt->GetString(0);
// //              std::cout << " ";
// //              std::cout <<  stmt->GetString(1);
// //              std::cout << " ";
// //              std::cout <<  stmt->GetString(2);
// //              std::cout << "\n";
//              rman->GetRunReport()->SetValueOf("target_pol_sign",stmt->GetString(2));
//              rman->GetRunReport()->SetValueOf("target_insert",stmt->GetString(1));
//              rman->GetRunReport()->SetValueOf("target_cup",stmt->GetString(0));
//       }
//    }


   const char * fTable6 = "target_pol_final";
//    TString query6( Form( "SELECT online,offline FROM %s WHERE run_number = %d", fTable6 , aRun->fRunNumber ) );
   TString query6 =
        Form( "SELECT online FROM %s WHERE run_number = %d", fTable6 , aRun->fRunNumber );
   dat = rman->GetRunReport()->GetDatum("target_online_pol");
   if(dat){
      dat->GatherInfo(&query6);
      dat->SetUpdated(true);
   }
   query6 =
        Form( "SELECT offline FROM %s WHERE run_number = %d", fTable6 , aRun->fRunNumber );
   dat = rman->GetRunReport()->GetDatum("target_offline_pol");
   if(dat){
      dat->GatherInfo(&query6);
      dat->SetUpdated(true);
   }
   

   const char * fTable7 = "beam_pol_per_run";
   TString query7 =
        Form( "SELECT qe FROM %s WHERE run_number=%d", fTable7 , aRun->fRunNumber );
   dat = rman->GetRunReport()->GetDatum("pol_source_qe");
   if(dat){
        dat->GatherInfo(&query7);
      dat->SetUpdated(true);
   }
   query7 =
        Form( "SELECT wien_angle FROM %s WHERE run_number=%d", fTable7 , aRun->fRunNumber );
   dat = rman->GetRunReport()->GetDatum("wien_angle");
   if(dat){
        dat->GatherInfo(&query7);
      dat->SetUpdated(true);
   }
   query7 =
        Form( "SELECT npass FROM %s WHERE run_number=%d", fTable7 , aRun->fRunNumber );
   dat = rman->GetRunReport()->GetDatum("beam_pass");
   if(dat){
        dat->GatherInfo(&query7);
      dat->SetUpdated(true);
   }
   query7 =
        Form( "SELECT beam_energy FROM %s WHERE run_number=%d", fTable7 , aRun->fRunNumber );
   dat = rman->GetRunReport()->GetDatum("beam_energy");
   if(dat){
      dat->GatherInfo(&query7);
      dat->SetUpdated(true);
   }
   query7 =
        Form( "SELECT hwplate FROM %s WHERE run_number=%d", fTable7 , aRun->fRunNumber );
   dat = rman->GetRunReport()->GetDatum("halfwave_plate");
   if(dat){
      dat->GatherInfo(&query7);
      dat->SetUpdated(true);
   }


   const char * fTable8 = "packing_fractions";
   TString query8 =
        Form( "SELECT packing_fraction FROM %s WHERE first_run<=%d AND last_run>=%d", fTable8 , aRun->fRunNumber , aRun->fRunNumber);
   dat = rman->GetRunReport()->GetDatum("target_packing_fraction");
   if(dat){
      dat->GatherInfo(&query8);
      Double_t pf = atof(dat->fValue.Data());
      if(pf > 1.0) {
         dat->fValue = Form("%f",pf/100.0);
         //std::cout << " " << dat->fValue.Data() << std::endl;
      }
      dat->SetUpdated(true);
      //std::cout << dat->fValue.Data() << std::endl;
   }

   std::cout << " done with GatherRunInfo " << std::endl;

   return(0);
}
