/**  Zeroth Pass Analysis script
 *
 *  The Steps of a good analyzer pass script
 *
 *  - Input and output trees
 * 
 *  - Define the analyzer for this pass
 *
 *  - Define and setup each "Correction" and "Calculation".
 *  
 *  - Set each detector used by the analysis
 *  
 *  - Add the corrections and calculations 
 *    \note Order Matters. It is the execution of the list from first to last. 
 *  
 *  - Initialze and Process 
 * 
 */
Int_t ZerothPass(Int_t runNumber = 72995) {

  gROOT->SetBatch(kTRUE);

  const char * sourceTreeName = "betaDetectors";
  const char * outputTreeName = "betaDetectors0";

  // First open the run
  rman->SetRun(runNumber);
  if (!rman->fCurrentRun ) {printf("Please use SetRun(#) first"); return(-1);}
/*
  rman->CalculatePedestals();

  rman->CalculateTiming();*/

  SANEZerothPassCorrector * corrector;
  corrector = new SANEZerothPassCorrector( outputTreeName , sourceTreeName );

  BETAPedestalCorrection * b1;
  b1 = new BETAPedestalCorrection(corrector);
/*  b1->SetEvent( corrector->GetEvents() );*/
  b1->SetDetectors( corrector );

  BETATimingCorrection * b2;
  b2 = new BETATimingCorrection(corrector);
/*  b2->SetEvent( corrector->GetEvents() );*/
  b2->SetDetectors( corrector );

  BETAEventCounterCorrection * b3;
  b3 = new BETAEventCounterCorrection(corrector);
/*  b3->SetEvent( corrector->GetEvents() );*/
  b3->SetDetectors( corrector );


  SANEScalerCalculation * scaler1;
  scaler1 = new SANEScalerCalculation(corrector);
  scaler1->SetDetectors(corrector);


  corrector->AddCorrection( b1 );
  corrector->AddCorrection( b2 );
  corrector->AddCorrection( b3 );

  corrector->AddScalerCalculation( scaler1 );

/// \todo This could be in the analyzer class?..
  corrector->AddDetector(corrector->InSANEDetectorAnalysis::fCherenkovDetector); 
  corrector->AddDetector(corrector->InSANEDetectorAnalysis::fBigcalDetector); 
  corrector->AddDetector(corrector->InSANEDetectorAnalysis::fHodoscopeDetector); 
  corrector->AddDetector(corrector->InSANEDetectorAnalysis::fTrackerDetector); 


  corrector->Initialize();

//   corrector->ApplyCorrections();
  corrector->Process();
  rman->fCurrentFile->Flush();
  rman->fCurrentFile->Write();

//   delete b1;
//   delete corrector;

  gROOT->SetBatch(kFALSE);
  gROOT->ProcessLine(".q");
  return(0);
}

