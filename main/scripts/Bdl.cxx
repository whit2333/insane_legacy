{
const int N=40;
Double_t x[N];
Double_t y[N];

//TGraph * g = new TGraph(N,x,y);
TCanvas * c = new TCanvas("bdlpar","Parallel Bdl",800,350);
UVAOxfordMagneticField * f = new UVAOxfordMagneticField();
Double_t res;
for(int i = 0;i<N;i++) {
printf("Theta = %f degrees\n",30.0+i*0.5);
res = f->IntegrateBdl(30.0+i*0.5,0);
x[i]=30.0+i*0.5;
y[i]=res;

}

TGraph * g = new TGraph(N,x,y);
g->SetTitle("#intB.dl");
g->GetXaxis()->SetTitle("Theta (degrees)");
g->GetYaxis()->SetTitle("(Tesla*cm)");
g->Draw("AC*");

c->SaveAs("plots/Bdl.png");

}
