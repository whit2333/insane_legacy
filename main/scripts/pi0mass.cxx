Int_t pi0mass(Int_t runnumber=72994) {
  
  const char * detectorTree = "betaDetectors1";
  if(!rman->IsRunSet() ) rman->SetRun(runnumber,1);
  else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

  TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
  TTree * fPi0Tree = (TTree*)gROOT->FindObject("pi0results");
  if(!fClusterTree) {
    fClusterTree = (TTree*)gROOT->FindObject("Clusters");
    fPi0Tree = (TTree*)gROOT->FindObject("pi0results");
  }

  if(!fClusterTree) {printf("tree not found\n"); return(-1); }
  if(!fPi0Tree) {printf("fPi0Tree tree not found\n"); return(-1); }
/*
  Pi0Event * fPi0Event = new Pi0Event();
    fPi0Tree->SetBranchAddress("pi0reconstruction",&fPi0Event);

  TFile*f = new TFile("data/rootfiles/pi0mass.root","UPDATE");

  f->cd();

  TTree * fpi0mass = (TTree*)gROOT->FindObject("pi0mass");

  if(!fpi0mass) {
    fpi0mass = new TTree("pi0mass","pi0 reconstruction accumulated");
    fpi0mass->Branch("pi0Event","Pi0Event",&fPi0Event);
  } else {
    fpi0mass->SetBranchAddress("pi0Event",&fPi0Event);
  }

  for(int i = 0; i< fPi0Tree->GetEntries()  ;i++) {
    fPi0Tree->GetEntry(i);
    if(1) {
      fpi0mass->Fill();
    }
  }

  f->Write();
// fpi0mass->StartViewer();*/

  return(0);
}
