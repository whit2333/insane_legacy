Int_t CreateNNTree() {

   const char * input1 = "betaDetectors";
   const char * input2 = "Clusters";

   rman->SetRun(885);

   TTree * t = (TTree*)gROOT->FindObject(input1);
   if( !t ) {  printf("Tree:%s \n       was NOT FOUND. Aborting... \n",input1);   return(-1); }
   TTree * t1 = t;

//    t = (TTree*)gROOT->FindObject(input2);
//    if( !t ) {  printf("Tree:%s \n       was NOT FOUND. Aborting... \n",input2);   return(-1); }
//    TTree * t2 = t;


TCanvas * c1 = new TCanvas("cEnergy"," Energy Canvas ");
   c1->Divide(2,2);
   t1->Draw(">>bcSingleHits","@fBigcalPlaneHits.size()==1","entrylist");
   TEntryList *bcSingleHits = (TEntryList*)gDirectory->Get("bcSingleHits");
   t1->SetEntryList(bcSingleHits);

   c1->cd(1);
   t1->Draw("fBigcalPlaneHits.fEnergy","");
   c1->cd(2);
   t1->Draw("fTrackerPlaneHits.fEnergy","");
   c1->cd(3);
   t1->Draw("fBigcalPlaneHits.fEnergy-fThrownParticles.fEnergy","");
   c1->cd(4);
   t1->Draw("fThrownParticles.fEnergy","");

return(0);
}