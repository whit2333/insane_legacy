Int_t read_grep(Int_t runNumber = 72994) {

  ifstream file(Form("scalers/gen%d.txt",runNumber));
  stringstream  ins; // Declare an input string stream.
  string line;
  Int_t var;
  Double_t dump;
  InSANERun * aRun = new InSANERun();
  if( ! file.is_open() ) return(-1);

  string genRunnumber = "Run #: ";
  string genTime = "Time of run = ";
  string genBeamE="E_beam     = ";
  string genFirstEvent ="first event = ";
  string genLastEvent = "last event  = ";
  string genPS1 = " PS1: ";
  string genPS2 = " PS2: ";
  string genPS3 = " PS3: ";
  string genPS4 = " PS4: ";
  string genPS5 = " PS5: ";
  string genPS6 = " PS6: ";
  string genBCM1QHplus = " no BCM threshold cut BCM1 charge gated by H+: ";
  string genBCM1QHminus = " no BCM threshold cut BCM1 charge gated by H-: ";
  string genBCM1QA = " no BCM threshold cut BCM1 charge asymmetry : ";
  string genBCM2QHplus = " no BCM threshold cut BCM2 charge gated by H+: ";
  string genBCM2QHminus = " no BCM threshold cut BCM2 charge gated by H-: ";
  string genBCM2QA = " no BCM threshold cut BCM2 charge asymmetry :   ";
// //   string genTotalChargeSt="SANE_HMS_ANGLE_THETA = ";
  
  int found;
  int t=0;
  int runnumber;
  double tester;
  while( ! file.eof() ) {
    t++;
    getline(file,line);
    ins.str("");
    found=line.find(genBCM2QA);
    if (found!=string::npos) {
      ins << line.substr(genBCM2QA.size());
      ins >> tester; //(double)aRun->fBCM2ChargeAsymmetry; 
/*      aRun->fBCM2ChargeAsymmetry = atof(line.substr(genBCM2QA.size()));*/
      std::cout << " found something " << tester << "\n";;
      std::cout << line;
      std::cout << line.substr(genBCM2QA.size());
      ins.str("");
    }
    found=line.find(genBCM2QHminus);
    if (found!=string::npos) {
      ins << line.substr(genBCM2QHminus.size());
      ins >> aRun->fBCM2GatedMinusCharge; ;
      ins.str("");
    }
    found=line.find(genBCM2QHplus);
    if (found!=string::npos) {
      ins << line.substr(genBCM2QHplus.size());
      ins >> aRun->fBCM2GatedPlusCharge; ;
      ins.str("");
    }
    found=line.find(genBCM1QA);
    if (found!=string::npos) {
      ins << line.substr(genBCM1QA.size());
      ins >> aRun->fBCM1ChargeAsymmetry; ;
      ins.str("");
    }
    found=line.find(genBCM1QHminus);
    if (found!=string::npos) {
      ins << line.substr(genBCM1QHminus.size());
      ins >> aRun->fBCM1GatedMinusCharge; ;
      ins.str("");
    }
    found=line.find(genBCM1QHplus);
    if (found!=string::npos) {
      ins << line.substr(genBCM1QHplus.size());
      ins >> aRun->fBCM1GatedPlusCharge; ;
      ins.str("");
    }
    found=line.find(genPS6);
    if (found!=string::npos) {
      ins << line.substr(genPS6.size());
      ins >> aRun->fPreScale[5]; ;
      ins.str("");
    }
    found=line.find(genPS5);
    if (found!=string::npos) {
      ins << line.substr(genPS5.size());
      ins >> aRun->fPreScale[4]; ;
      ins.str("");
    }
    found=line.find(genPS4);
    if (found!=string::npos) {
      ins << line.substr(genPS4.size());
      ins >> aRun->fPreScale[3]; ;
      ins.str("");
    }

    found=line.find(genPS3);
    if (found!=string::npos) {
      ins << line.substr(genPS3.size());
      ins >> aRun->fPreScale[2]; ;
      ins.str("");
    }
    found=line.find(genPS2);
    if (found!=string::npos) {
      ins << line.substr(genPS2.size());
      ins >> aRun->fPreScale[1]; ;
      ins.str("");
    }
    found=line.find(genPS1);
    if (found!=string::npos) {
      ins << line.substr(genPS1.size());
      ins >> aRun->fPreScale[0]; ;
      ins.str("");
    }

    found=line.find(genLastEvent);
    if (found!=string::npos) {
      ins << line.substr(genLastEvent.size());
      ins >> aRun->fLastEvent; ;
      ins.str("");
    }

    found=line.find(genFirstEvent);
    if (found!=string::npos) {
      ins << line.substr(genFirstEvent.size());
      ins >> aRun->fFirstEvent; ;
      ins.str("");
    }

    found=line.find(genBeamE);
    if (found!=string::npos) {
      ins << line.substr(genBeamE.size());
      ins >> aRun->fBeamEnergy; ;
      ins.str("");
    }

    found=line.find(genTime);
    if (found!=string::npos) {
      ins << line.substr(genTime.size());
      ins >> aRun->fDuration; ;
      ins.str("");
    }

    found=line.find(genRunnumber);
    if (found!=string::npos) {
      ins << line.substr(genRunnumber.size());
      ins >> runnumber ;
      ins.str("");
    }

  }
//     file >> aRun->fRunNumber;  
//     file.ignore(300,'\n');
//     file >> aRun->fDuration;  
//     file.ignore(300,'\n');
//     file >> aRun->fFirstEvent;  
//     file.ignore(300,'\n');
//     file >> aRun->fLastEvent;  
//     file.ignore(300,'\n');
//     file >> aRun->fPreScale[0];  
//     file.ignore(300,'\n');
//     file >> aRun->fPreScale[1];  
//     file.ignore(300,'\n');
//     file >> aRun->fPreScale[2];  
//     file.ignore(300,'\n');
//     file >> aRun->fPreScale[3];  
//     file.ignore(300,'\n');
//     file >> aRun->fPreScale[4];  
//     file.ignore(300,'\n');
//     file >> aRun->fPreScale[5];  
//     file.ignore(300,'\n');
//     file >> aRun->fNHmsEvents;  
//     file.ignore(300,'\n');
//     file >> aRun->fNBeta1Events;  
//     file.ignore(300,'\n');
//     file >> aRun->fNPi0Events;  
//     file.ignore(300,'\n');
//     file >> aRun->fNBeta2Events;  
//     file.ignore(300,'\n');
//     file >> aRun->fNCoinEvents;  
//     file.ignore(300,'\n');
//     file >> dump;  
//     file.ignore(300,'\n');
//     file >> aRun->fNEvents; 


  aRun->Print();
}