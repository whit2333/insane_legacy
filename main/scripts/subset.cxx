{
    vector<int> runs;

 runs.push_back(72994);
 runs.push_back(72623);
 runs.push_back(72825);

// runs.push_back(72995);
// runs.push_back(72996);
// runs.push_back(72997);
// runs.push_back(72998);
// runs.push_back(72999);
// runs.push_back(73000);
// runs.push_back(73001);
// runs.push_back(73002);
// runs.push_back(73003);
// runs.push_back(73004);
// runs.push_back(73005);
// runs.push_back(73006);
// runs.push_back(73007);
// runs.push_back(73008);
// runs.push_back(73009);
// runs.push_back(73010);
// runs.push_back(73011);

SANERunManager * rm = SANERunManager::GetRunManager();
BETAPedestalAnalysis * ped;
BETATimingAnalysis * tdc;
SANELuciteHodoscopeAnalysis * luc;
SANEGasCherenkovAnalysis * cer;

for(int i=0; i<runs.size();i++) {
printf("\n Analyzing run %d \n", runs[i]);
rm->SetRun(runs[i]);
rm->ConvertRawData();
printf("\n Analyzing peds \n", runs[i]);

ped = new BETAPedestalAnalysis(runs[i]);
ped->AnalyzeRun();
ped->FitHistograms();
ped->CreatePlots();
delete ped;
printf("\n Analyzing timing\n", runs[i]);
tdc = new BETATimingAnalysis(runs[i]);
tdc->AnalyzeRun();
tdc->FitHistograms();
tdc->CreatePlots();
delete tdc;

printf("\n Analyzing Lucite Hodoscope\n", runs[i]);
luc = new SANELuciteHodoscopeAnalysis(runs[i]);
luc->AnalyzeRun();
luc->Visualize();
delete luc;

printf("\n Analyzing Gas Cherenkov\n", runs[i]);
cer = new SANEGasCherenkovAnalysis(runs[i]);
cer->AnalyzeRun();
cer->Visualize();
delete cer;

}

}
