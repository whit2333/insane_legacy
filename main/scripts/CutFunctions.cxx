#include "InSANEEvent.h"
#include "BETAEvent.h"

void* CutFunctions; 

 Bool_t CutFunctions(InSANEEvent * anEvent) {
Bool_t res = kFALSE;

  if(!anEvent) { return(kFALSE); }
  else {
  BETAEvent * aBetaEvent = (BETAEvent *)anEvent;
  if( aBetaEvent->gen_event_type==5 && aBetaEvent->gen_event_trigtype[3]) res = kTRUE;
  }
return res;
}

