/*! Event display for detectors

  
 */
Int_t event_display(Int_t runnumber = 72825) {

  const char * detectorTree = "betaDetectors1";
   rman->SetRun(runnumber);

// Useful pointers
   BigcalEvent * bcEvent = 0; 
   InSANEClusterEvent * clusterEvent = 0; 

// Create sane events for detector tree
   SANEEvents * events = new SANEEvents( detectorTree );
   bcEvent = (events->BETA->fBigcalEvent);

// Setup analysis
   InSANEClusterAnalysis * clusterAnalysis = new InSANEClusterAnalysis( clusterTree );
   clusterAnalysis->SetClusterTree(ct);
   ct->BuildIndex("fRunNumber","fEventNumber");
   t->AddFriend(ct);

// Setup detectors
   BigcalDetector * bcdet = new BigcalDetector(runnumber);
   bcdet->SetEventAddress( bcEvent );
   bcdet->SetClusterEventAddress( clusterAnalysis->fClusterEvent );
   GasCherenkovDetector * gcdet = new GasCherenkovDetector(runnumber);
   gcdet->SetEventAddress( (GasCherenkovEvent*)events->BETA->fGasCherenkovEvent );
   LuciteHodoscopeDetector * lhdet = new LuciteHodoscopeDetector(runnumber);
   lhdet->SetEventAddress( (LuciteHodoscopeEvent*)events->BETA->fLuciteHodoscopeEvent );

// Setup Display
   Pi0EventDisplay * display = new Pi0EventDisplay();
   display->fDataHists->Add(bcdet->fEventADCHist);
   display->fDataHists->Add(bcdet->fEventTimingHist);
   display->fDataHists->Add(gcdet->fCerHits);
   display->fDataHists->Add(lhdet->fLuciteHitDisplay);
   display->fDataHists->Add(bcdet->fEventTrigTimingHist);
   display->fDataHists->Add(bcdet->fElectronClustersHist);
   display->fDataHists->Add(bcdet->fChargedPiClustersHist);
   display->fWaitPrimitive=0;

// Loop through events
   for(int i = 1000; i < t->GetEntries() && i < 2000 ;i++) {
      std::cout << "event " << i << "\n";
      t->GetEntry(i);

      gcdet->ProcessEvent();
      bcdet->ProcessEvent();
      lhdet->ProcessEvent();

      display->UpdateDisplay();

      bcdet->ClearEvent();
      lhdet->ClearEvent();
      gcdet->ClearEvent();
   }
//   t->Draw(">>all","fBigcalEvent->fEventNumber>1000");
//   TEventList *elist = (TEventList*)gDirectory->Get("all");
//   t->SetEventList(elist);



}

