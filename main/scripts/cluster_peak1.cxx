int cluster_peak_1(int runNumber=72616) {

// initialize objects used in analysis
  BETAEvent * event = new BETAEvent();
  BIGCALClusterProcessor * clusterProcessor = new BIGCALClusterProcessor();


// input data
  TChain * chain = new TChain("betaDetectors"); // name of tree
    chain->Add(Form("data/rootfiles/SANE%d*root",runNumber));
    chain->SetBranchAddress("betaDetectorEvent",&event);

  std::cout << chain->GetEntries() << " Entries " << "in " 
    << ((TChain * )chain)->GetNtrees() << " trees.\n" ;

// output data

  TFile * clustFile = new TFile(Form("data/rootfiles/clusters.1.%d.root",runNumber
),"RECREATE");
  TTree * clusterTree = new TTree("bigcalClusters","Clusters Tree");

// Analysis objects (after opening outputfile)
  BIGCALCluster * aCluster = new BIGCALCluster();
  TClonesArray * bcClusters;

  bcClusters = new TClonesArray ("BIGCALCluster", 2) ;
  bcClusters->ExpandCreateFast (1) ; 
          clusterTree->Branch ("bigcalClusterEvent", &bcClusters) ;
/// \todo consider putting this at a tree level, i.e., set up two trees one cluster one betaDetectors
          clusterTree->Branch ("betaDetectorEvent", &event) ;
          clusterTree->AutoSave();
    clusterProcessor->SetEventAddress(event->fBigcalEvent);


// Cuts
  TH2F * bc_cluster_src; // This is actually created by the cluster processer. 
// We just need a pointer.. ignore lines below 
// = new TH2F("bc_cluster_src","Bigcal for clustering",33,0,33,57,0,57);
// We pass this histogram to the cluster processor which finds clusters in some way using it.
//  // CUTS
//   gROOT->ProcessLine(".L scripts/CutFunctions.cxx++");
//   InSANECut * aCut  = new InSANECut("bigcalTrigger","Cut on event type 5 and bigcal trigger bit");
//   aCut->SetEventAddress( (InSANEEvent *)event);
//   aCut->SetCutFunction( CutFunctions  );
//   aCut->Describe();

// Event Loop
  Int_t entries = chain->GetEntriesFast();
  for(Int_t i_event=1000;i_event<1200;i_event++) {
      if(i_event%100==0) cout << "Event " << i_event << "\n";
      chain->GetEntry(i_event);
     if ( event->gen_event_type==5 && event->gen_event_trigtype[3]   ) {
//      if( aCut->ApplyCut() ) {
//      cout << "event " << i_event << "\n";      
      clusterProcessor->ProcessEvent(bcClusters);// Fills this TClonesArray with cluster objects
//      clusterProcessor->fSourceHistogram->Draw("colz");
//      gPad->WaitPrimitive();
      clusterTree->Fill(); // Done with clustering.
//      clusterProcessor->Clear();
      bcClusters->Clear();
    }// event type end
  } // Event loop ending

clusterTree->Write();

clusterTree->Draw("bigcalClusterEvent.fYStdDeviation:bigcalClusterEvent.fXStdDeviation","bigcalClusterEvent.fTotalE > 500000","colz");
clusterTree->Draw("bigcalClusterEvent.fYStdDeviation:bigcalClusterEvent.fTotalE","bigcalClusterEvent.fTotalE > 500000","colz");
clusterTree->Draw("bigcalClusterEvent.fTotalE:bigcalClusterEvent.fXStdDeviation","bigcalClusterEvent.fTotalE > 500000","colz");

clusterTree->StartViewer();
}

//////////////////////////

Int_t Analyze_clust_peak1(Int_t runNumber=72616){
//gROOT->ProcessLine(".x create_shared_libs.cxx");

  BIGCALCluster * aCluster = new BIGCALCluster();
  TClonesArray * bcClusters;
  bcClusters = new TClonesArray ("BIGCALCluster", 2) ;
  bcClusters->ExpandCreateFast (1) ; 


TFile * oldFile =new TFile(Form("data/rootfiles/clusters%d.root",runNumber));

TTree * inputTree = (TTree *)gROOT->FindObject("bigcalClusters");

if(!inputTree) { std::out << "\n Error: No tree \n"; return(-1);}

TBranch * b = inputTree->GetBranch("bigcalClusterEvent");
b->SetAddress( &bcClusters );

//TFile * newFile = new TFile("EAE_rootfiles/EAEtest.root","RECREATE","A test of EAE");
//TTree * outputTree = new TTree("EAEfirst","A first test");
//TTree * outputTree = inputTree->CloneTree(0);
//outputTree->Branch("EAEbranch","ExpAnalysisEvent",&event );

////// HISTOGRAMS ///////
// TH1F * cer_adc_hist[12];
// TH1F * cer_tdc_hist[12];
// 
// TH1F * cer_adc_hist_wIndTDCCut[12];
// TH1F * cer_tdc_hist_wIndTDCCut[12];
// 
// TH1F * cer_adc_hist_wSumTDCCut[12];
// TH1F * cer_tdc_hist_wSumTDCCut[12];
// 
// TH1F * cer_adc_hist_wIndTDCCut_wSumThreshCut[12];
// TH1F * cer_tdc_hist_wIndTDCCut_wSumThreshCut[12];
// 
// TH1F * cer_adc_hist_wSumTDCCut_wSumThreshCut[12];
// TH1F * cer_tdc_hist_wSumTDCCut_wSumThreshCut[12];
// 
// TH1F * cer_adc_hist_wIndTDCCut_wIndThreshCut[12];
// TH1F * cer_tdc_hist_wIndTDCCut_wIndThreshCut[12];
// 
// TH1F * cer_adc_hist_wSumTDCCut_wIndThreshCut[12];
// TH1F * cer_tdc_hist_wSumTDCCut_wIndThreshCut[12];

// for(Int_t i=0;i<12;i++) {
//      cer_adc_hist[i] = new TH1F(Form("cer_adc_hist_%d",i),Form("Cherenkov ADC %d",i), 100,-1000,3000);
//      cer_adc_hist_wIndTDCCut[i] = new TH1F(Form("cer_adc_hist_%d_wIndTDCCut",i),Form("Cherenkov ADC %d w/ Indiv. TDC cuts",i), 100,-1000, 3000);
//      cer_tdc_hist[i] = new TH1F(Form("cer_tdc_hist_%d",i),Form("Cherenkov TDC %d",i), 100,-3000,-1000);
//      cer_tdc_hist_wIndTDCCut[i] = new TH1F(Form("cer_tdc_hist_%d_wIndTDCCut",i),Form("Cherenkov TDC %d w/ Indiv. TDC cuts",i), 100,-3000, -1000);
// }
/*
  TClonesArray * cerHits = new TClonesArray ("GasCherenkovHit",20) ;
  cerHits = aBETAEvent->fGasCherenkovEvent->fHits ;
  cerHits->ExpandCreateFast (1) ; 
  GasCherenkovHit * aCerHit = new GasCherenkovHit();*/
//// Analysis Flags /////
// bool odd_ceradc_hit = false;
/*
///// CUT PARAMETERS /////
int tdclow = -2750;
int tdchigh = -1000;
int k,kk; // associated with tdc loops
int tempTDC;
int tempADC;
int tempMirrorNumber;*/

// Event Loop
  Int_t nevent = inputTree->GetEntries();
  cout << "\n" << nevent << " ENTRIES \n";
  for (int iEVENT=0;iEVENT<nevent;iEVENT++) 
    {
    inputTree->GetEvent(iEVENT);
///// CHERENKOV //////
// Begin by checking and printing obvious stuff
//odd_ceradc_hit = false;
//    cout <<  aBETAEvent->fGasCherenkovEvent->fNumberOfHits << " HITS \n";

// loop over cherenkov hits for event
for (kk=0; kk< aBETAEvent->fGasCherenkovEvent->fNumberOfHits;kk++) 
  {
    aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
/*    cout <<  aCerHit->fADC << " ADC \n";
    cout <<  aCerHit->fTDC << " TDC \n";
    cout <<  aCerHit->fMirrorNumber << " MirrorNumber \n";
    cout <<  aCerHit->fPMTNumber[0] << " PMTNumber \n";
*/  
    cer_adc_hist[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);

    if( aCerHit->fTDC > tdclow && aCerHit->fTDC < tdchigh) 
      { //passes very wide tdc cut 
//if(odd_ceradc_hit) printf("cer_num[k] - 1 =%d\n",tempMirrorNumber);
      cer_adc_hist_wIndTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fADC);
      cer_tdc_hist_wIndTDCCut[aCerHit->fMirrorNumber-1]->Fill(aCerHit->fTDC);
      }
} // End loop over Cherenkov signals
    

    } // END OF EVENT LOOP

//;outputTree->Write();
//outputTree->StartViewer();
//newFile->Write();

TCanvas * c = new TCanvas();
c->Divide(4,3);
for(i=0;i<12;i++) {
c->cd(i+1);
cer_adc_hist_wIndTDCCut[i]->Draw();
}

TCanvas * c2 = new TCanvas();
c2->Divide(4,3);
for(i=0;i<12;i++) {
c2->cd(i+1);
cer_tdc_hist_wIndTDCCut[i]->Draw();
}


}


