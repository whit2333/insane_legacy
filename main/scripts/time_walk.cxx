/**  BETA Time walk fitting procedure 
 *
 *  Currently only cherenkov has been implemented
 *
 *  \todo Bigcal
 */
Int_t time_walk(Int_t runNumber = 72994) {

/// This controls whether or not to draw all histograms
/// or just the ones that are used in fitting
   Bool_t drawAllPlots = false;

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();

   InSANERun * run = runManager->fCurrentRun;

   runManager->GetCurrentFile()->cd();

   TTree* t = (TTree*) (gROOT->FindObject("betaDetectors0"));
   if(t){
   
   runManager->GetCurrentFile()->cd();
/*   TDirectory *twDIR = runManager->GetCurrentFile()->mkdir("timewalk");*/
   runManager->GetCurrentFile()->cd("timewalk");
/*   twDIR->cd();*/
   THStack * hs = new THStack("hs","test stacked histograms");
   TH1F * h = 0;
   TH2F * h2 = 0;
   TVirtualPad * pad;
   TCanvas * c = new TCanvas("timewalk","Gas Cherenkov",500,800);
   c->Divide(2,4);
   TCanvas * c2 = new TCanvas("Cherenkov","Gas Cherenkov",500,800);
/*   c2->Divide(2,4);*/
/* pad1->GetFrame()->SetFillColor(42);
   pad1->GetFrame()->SetBorderMode(-1);
   pad1->GetFrame()->SetBorderSize(5);*/
    std::cout << "Plotting PMT\n";

   TF1 *ftimewalk = 0;

 for (int i=8;i>=1;i=i-1) {
     ftimewalk = new TF1(Form("cer%dtimewalk",i),"[0]+x*[1]+x*x*[2]+x*x*x*[3]+x*x*x*x*[4]",450,4000);
     run->fCherenkovTimeWalkFits.Add(ftimewalk);
/*      pad = c->cd(0);*/
//       gPad->GetFrame()->SetFillColor(42);
      c->cd(9-i);
      std::cout <<  " ... " << i << "\n" ;
      TString hName = Form("cer%dTimewalk",i);
      TString pName = Form("cer%dTimewalkProfile",i);
      TString h0Name = Form("cer%dtdc",i);
      TString draw0 = "fGasCherenkovEvent.fHits[].fTDCAlign>>" + h0Name + "(100,-50,50)";
      TString draw = "fGasCherenkovEvent.fHits[].fTDCAlign:fGasCherenkovEvent.fHits[Iteration$].fADC>>" + hName + "(100,0,3000,100,-50,50)";

/*      TString draw = "fGasCherenkovEvent.fHits[].fTDCAlign:fGasCherenkovEvent.fHits[Iteration$].fADC>>" + hName + "(100,0,3000,100,-50,50)";*/
      TCut cEventType =
         "triggerEvent.IsBETAEvent()";
      TCut cCherenkovTDCHit = Form(
         "fGasCherenkovEvent.fHits[Iteration$].fLevel==%d",1);
      TCut cCherenkovNum = Form(
         "fGasCherenkovEvent.fHits[Iteration$].fMirrorNumber==%d",i);
      TCut cTDCRange =
         "TMath::Abs(fGasCherenkovEvent.fHits[Iteration$].fTDCAlign)<100";
      TCut cADC =
         "TMath::Abs(fGasCherenkovEvent.fHits[Iteration$].fADC>500)";

      t->Draw(draw0.Data(),cEventType + cCherenkovTDCHit + cCherenkovNum + cTDCRange + cADC,"goff"); 
      h = (TH1F*) gROOT->FindObject(h0Name.Data());
       hs->Add(h);

      t->Draw(draw.Data(),cEventType + cCherenkovTDCHit + cCherenkovNum + cTDCRange + cADC,"goff"); 

      // retrieve the histogram
      h2 = (TH2F*) gROOT->FindObject(hName.Data());
//       h2->Draw("colz");
//       h2->SetMarkerSize(3.0);
//       h2->SetLineWidth(3);
//       h2->Draw("ap");

/*      h->SetMarkerStyle(22);*/
//       if(i%2==0) h->SetLineColor(kRed-i+2);
//       else h->SetLineColor(kBlue-i+2);
/*      h->SetFillColor(kRed-i);*/
//       hs->Add(h);
      TProfile * p = h2->ProfileX(pName.Data());
      p->Fit(ftimewalk,"R,Q");


/// time walk corrected
      c2->cd(9-i);
      std::cout <<  " ... " << i << "\n" ;
      TString h1Name = Form("cer%dTimewalkCorrected",i);

      draw = 
         "fGasCherenkovEvent.fHits[].fTDCAlign - ";
         draw = draw+ Form("((%e)",ftimewalk->GetParameter(0)); 
         draw = draw+ Form("+(%e)*fGasCherenkovEvent.fHits[Iteration$].fADC",ftimewalk->GetParameter(1)) ;
         draw = draw+ Form("+(%e)*fGasCherenkovEvent.fHits[Iteration$].fADC*fGasCherenkovEvent.fHits[Iteration$].fADC",ftimewalk->GetParameter(2)) ;
         draw = draw+ Form( "+(%e)*fGasCherenkovEvent.fHits[Iteration$].fADC*fGasCherenkovEvent.fHits[Iteration$].fADC*fGasCherenkovEvent.fHits[Iteration$].fADC"
         ,ftimewalk->GetParameter(3));
         draw = draw+ Form( "+(%e)*fGasCherenkovEvent.fHits[Iteration$].fADC*fGasCherenkovEvent.fHits[Iteration$].fADC*fGasCherenkovEvent.fHits[Iteration$].fADC*fGasCherenkovEvent.fHits[Iteration$].fADC)"
         ,ftimewalk->GetParameter(4));
         draw = draw+  ">>" + h1Name + "(100,-50,50)";

//          std::cout << draw.Data() << std::endl;
//          std::cout << draw0.Data() << std::endl;
/*      TString draw = "fGasCherenkovEvent.fHits[].fTDCAlign:fGasCherenkovEvent.fHits[Iteration$].fADC>>" + hName + "(100,0,3000,100,-50,50)";*/
//       TCut cEventType =
//          "triggerEvent.IsBETAEvent()";
//       TCut cCherenkovTDCHit = Form(
//          "fGasCherenkovEvent.fHits[Iteration$].fLevel==%d",1);
//       TCut cCherenkovNum = Form(
//          "fGasCherenkovEvent.fHits[Iteration$].fMirrorNumber==%d",i);
//       TCut cTDCRange =
//          "TMath::Abs(fGasCherenkovEvent.fHits[Iteration$].fTDCAlign)<100";
      t->Draw(draw.Data(),cEventType + cCherenkovTDCHit + cCherenkovNum + cTDCRange + cADC,"goff"); 

      // retrieve the histogram
      h = (TH1F*) gROOT->FindObject(h1Name.Data());
//       c->cd(2);
//       TProfile * p2 = h2->ProfileX("pasdfkj");
//       p2->Draw("");

//       h2->Draw("colz");
//       h2->SetMarkerSize(3.0);
      h->SetLineWidth(3);
//       h2->Draw("ap");

/*      h->SetMarkerStyle(22);*/
      if(i%2==0) h->SetLineColor(kGreen-i+2);
      else h->SetLineColor(kYellow-i+2);
/*      h->SetFillColor(kRed-i);*/
      hs->Add(h);
//       TProfile * p = h->ProfileX(pName.Data());
//       p->Fit(ftimewalk,"R");
   }
      std::cout <<  " - Done with time_walk.cxx.\n ";
c2->cd(0);
   hs->Draw("nostack");

 }
     else std::cout << "x- Null tree in time_walk.cxx\n";


   runManager->GetCurrentFile()->cd();
//    t->StartViewer();

  return(0);
}
 
