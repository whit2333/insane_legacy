/**  Second Pass Analysis script
 *
 *  The Primary job here is to complete the data members for the event class
 *  CleanedBETAEvent
 *
 *  The Steps of a good analyzer pass script
 *
 *  - Name input and output trees
 *  - Define the analyzer for this pass. \note This does not yet create the trees! This happens at initialize
 *  - Create each detector used by the analysis and add it to the analyzer. 
 *    Currently all the detectors are created in InSANEDetectorAnalysis
 *  - Define and setup each "Correction" and "Calculation".
 *  - Set each detector used by the analysis
 *  - Add the corrections and calculations 
 *    \note Order Matters. It is the execution of the list from first to last. 
 *  - Initialze and Process 
 * 
 */
Int_t SecondPass(Int_t runNumber = 72995) {
  gROOT->SetBatch(kTRUE);

  const char * sourceTreeName = "betaDetectors1";
  const char * outputTreeName = "betaDetectors2";

// Clean up the file
   rman->SetRun(runNumber);
   rman->fCurrentFile->Delete("betaDetectors2;*");
   rman->fCurrentFile->Delete("cleanedEvents;*");

// Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
   std::cout << " + Creating SANESecondPassAnalyzer\n"; 
   SANESecondPassAnalyzer* analyzer = 
     new SANESecondPassAnalyzer(outputTreeName ,sourceTreeName);

//_______________ DETECTORS _____________//
// Should Create Detectors here or before creating the analyzer?...

  analyzer->AddDetector(analyzer->fCherenkovDetector); 
  analyzer->AddDetector(analyzer->fBigcalDetector); 
  analyzer->AddDetector(analyzer->fHodoscopeDetector); 
  analyzer->AddDetector(analyzer->fTrackerDetector); 


//___________BETA DETECTORS Ind. Calculations_________//

// Lucite Hodoscope Calculations
std::cout << "Creating LuciteHodoscopeCalculation1\n"; 
  LuciteHodoscopeCalculation3 * l3;
  l3 = new LuciteHodoscopeCalculation3(analyzer);
//  l1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
  l3->SetDetectors(analyzer);

// Cleaned BETA Events
  SANEElectronSelector2 * cleaned = new SANEElectronSelector2(analyzer);
  cleaned->SetDetectors(analyzer);

// compares hodoscope and other detectors
// std::cout << "Creating LuciteHodoscopeCalculation2\n"; 
//   LuciteHodoscopeCalculation2 * l2;
//   l2 = new LuciteHodoscopeCalculation2(analyzer);
// //  l2->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//   l2->SetDetectors(analyzer);

// Bigcal Calculations 
// std::cout << "Creating BigcalCalculation1\n"; 
//   BigcalCalculation1 * bc1;
//   bc1 = new BigcalCalculation1(analyzer);
// //  bc1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
//   bc1->SetDetectors(analyzer);


// The cluster tree
/*  TTree * tout = new TTree("Clusters","Cluster Data");*/
//   analyzer->SetClusterTree( tout );


// Define the cluster processor 
// std::cout << "Creating LocalMaxClusterProcessor\n"; 
//   LocalMaxClusterProcessor * clusterProcessor = 
//      new LocalMaxClusterProcessor();
// // - Performs clustering
// std::cout << "Creating BigcalClusteringCalculation1\n"; 
//   BigcalClusteringCalculation1 * b1 = new BigcalClusteringCalculation1(analyzer);
// //   b1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
// //   b1->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
//   b1->SetClusterProcessor(clusterProcessor);
//   b1->SetDetectors(analyzer);

// Pi0 Calibrations
// - Adds to calibration matricies when a pi0 event is identified
// std::cout << "Creating BigcalPi0Calaculations\n"; 
//   BigcalPi0Calaculations * b2 = new BigcalPi0Calaculations(analyzer);
// //   b2->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );
// //   b2->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);
//   b2->SetDetectors(analyzer);

// std::cout << "Creating SANETrajectoryCalculation1\n"; 
//   SANETrajectoryCalculation1 * traj1 = new SANETrajectoryCalculation1(analyzer);
// /*  traj1->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );*/
//   traj1->SetDetectors(analyzer);
// 
//   analyzer->SetClusterProcessor((BIGCALClusterProcessor*)b1->GetClusterProcessor());
// 

// Electron selection
// Currently the raw helicity asymmetry is calculated here
// std::cout << "Creating SANEElectronSelector\n"; 
//   SANEElectronSelector * select;
//   select = new SANEElectronSelector(analyzer);
// /*  select->SetEvent( analyzer->InSANEDetectorAnalysis::fEvents );*/
//   select->SetDetectors(analyzer);
// /*  select->SetClusterEvent( analyzer->SANEClusteringAnalysis::fClusterEvent);*/
// //   select->SetClusterTree( (TTree*)gROOT->FindObject(outputTreeName) );

// should have some asymmetry calculation like:
  // 1- set the bit which determines whether to even count this event in a denominator
  // 2- set the address of the bit which determines which number, N1 or N2, to increment
  // 3- in the detector's event,  fGoodEvent is used to mean that all cuts for a good
  //    track in that detector are passed. 
//   InSANEAsymmetry * helAsym = new InSANEAsymmetry();
//     helAsym->SetTriggerBitAddress(&(beta1->fEvent->fGoodEvent);
// 
//   BETASubDetector * beta1 = new BETASubDetector(DetectorNumber);
//   BETASubDetector * beta2 = new BETASubDetector();// this beta2 acceptance must be completely isolated from beta2
//   // at first it is easy to just match to some other mirror, but once the mirrors are done 
//   // they can be added to get better statisitics for each mirror
//   InSANEAsymmetryDetector * cherenkov_efficiency_monitor_12 = new InSANEAsymmetryDetector(beta1,beta2);
//   

/// \todo This could be in the analyzer class?..
  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fCherenkovDetector); 
  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fBigcalDetector); 
  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fHodoscopeDetector); 
  analyzer->AddDetector(analyzer->SANEClusteringAnalysis::fTrackerDetector); 

//____________ Add Corrections  _______________//
/// Time walk correction \see time_walk.cxx which must be executed first
  //analyzer->AddCorrection(timewalkcorr);

//____________ Add Calculations _______________//
// Add the calculations to the analyzer (order matters)
   analyzer->AddCalculation(l3);
   analyzer->AddCalculation(cleaned);
//   analyzer->AddCalculation(l1);
//   analyzer->AddCalculation(bc1);
//   analyzer->AddCalculation(b1);
//   analyzer->AddCalculation(b2);
//   analyzer->AddCalculation(traj1);
//   analyzer->AddCalculation(l2);
//   analyzer->AddCalculation(select);


//____________ Initialize and Process _______________//

  analyzer->Initialize();

  analyzer->Process();

// is this needed?
  rman->WriteRun();

// should delete everything properly ... but... we are quiting
  delete analyzer;
//   delete b1;
//   delete l1;
//   delete g1;

  if(gROOT->IsBatch())   gROOT->ProcessLine(".q");
  else return(0);
}
