Int_t liveTime(Int_t runNumber = 72999) {

   TH2F * h2 = 0;
   TH2F * h2 = 0;
   TGraph * g1 = 0;
   TProfile * px;
   TProfile * py;

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->fCurrentFile->cd();

   TTree * t = (TTree*)gROOT->FindObject("Scalers");

   TCanvas * c = new TCanvas("beamMonotor","Beam Monitor",1200,800);
   c->Divide(1,4);
//   TCanvas * c2 = new TCanvas("beamCurrent","Beam Current",900,240);

   t->Draw("saneScalerEvent.fLiveTime:saneScalerEvent.fTime>>liveTime","","goff");
   t->Draw("saneScalerEvent.BeamCurrentMonitor1():saneScalerEvent.fTime>>bcm2","","goff");

   c->cd(1);
   h2 = (TH2F*)gROOT->FindObject("liveTime");
   h2->SetTitle("Live Time");
   px = h2->ProfileX();
   px->GetYaxis()->SetTitle("liveTime");
   px->GetXaxis()->SetTitle("seconds");
   px->SetTitle("Live Time");
   px->SetLineColor(46);
   px->SetMarkerColor(9);
   px->SetMarkerStyle(33);
   px->SetMarkerSize(1.1);
   px->DrawClone();

   c->cd(2);
   h2 = (TH2F*)gROOT->FindObject("bcm2");
   h2->SetTitle("Beam Current Monitor 2");
   px = h2->ProfileX();
   px->GetYaxis()->SetTitle("nA");
   px->GetXaxis()->SetTitle("seconds");
   px->SetTitle("Beam Current Monitor 2");
   px->SetLineColor(38);
   px->SetMarkerColor(2);
   px->SetMarkerStyle(23);
   px->SetMarkerSize(1.1);
   px->DrawClone("lp");

   c->cd(3);
   t->Draw("saneScalerEvent.fLiveTime:saneScalerEvent.fTime","","colz");
   c->cd(4);
   t->Draw("saneScalerEvent.fLiveTime>>lt");
//    h1 = (TH1F*)gROOT->FindObject("lt");
//    h1->Draw();

   c->SaveAs(Form("plots/%d/LiveTime.png",runNumber));
//    c2->SaveAs(Form("plots/%d/BeamCurrent.png",runNumber));

//    c->cd(4);
//    h2->Draw("colz");



}