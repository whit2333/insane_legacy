Int_t clusters1(Int_t runnumber=72828) {

  TH1 * cer[8];
  rman->SetRun(runnumber);

  TTree * fClusterTree = (TTree*)gROOT->FindObject("Clusters");
  if(!fClusterTree) {
  std::cout << "No Cluster Tree Found \n";
//  new TFile(Form("data/rootfiles/InSANE%d.root",runnumber),"READ");
/*  rman->SetRun(runnumber,1);*/
  }

  if(!fClusterTree) {printf("Clusters tree not found eh\n"); return(-1); }

  TCanvas * c = new TCanvas("Cherenkov","Gas Cherenkov",500,800);

  c->Divide(2,4);


for(int i = 0;i<8;i++) {

  c->cd(8-i);
  gPad->SetLogy(1);

  gStyle->SetHistLineColor(kBlack);

  gStyle->SetHistFillColor(kRed);
  fClusterTree->UseCurrentStyle();
  fClusterTree->Draw(
  Form("bigcalClusters.fCherenkovBestNPESum[]>>cer%d(100,0,40)",i+1),
  Form("bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1",i+1)
 ,"");
  cer[i] = (TH1 *) gROOT->FindObject(Form("cer%d",i+1));
  cer[i]->SetMinimum(1.0);
  
  gStyle->SetHistLineColor(kBlack);
  gStyle->SetHistFillColor(kBlue);
  fClusterTree->UseCurrentStyle();
  fClusterTree->Draw(
  Form("bigcalClusters.fCherenkovBestNPESum[]",i+1),
  Form("bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1&&Iteration$==0",i+1)
 ,"same");

  gStyle->SetHistLineColor(kBlack);
  gStyle->SetHistFillColor(kYellow);
  fClusterTree->UseCurrentStyle();
  fClusterTree->Draw(
  Form("bigcalClusters.fCherenkovBestNPESum[]",i+1),
  Form("bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1&&Iteration$==1",i+1)
 ,"same");

  gStyle->SetHistLineColor(kBlack);
  gStyle->SetHistFillColor(kGreen);
  fClusterTree->UseCurrentStyle();
  fClusterTree->Draw(
  Form("bigcalClusters.fCherenkovBestNPESum[]",i+1),
  Form("bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1&&Iteration$==2",i+1)
 ,"same");

  gStyle->SetHistLineColor(kBlack);
  gStyle->SetHistFillColor(kCyan);
  fClusterTree->UseCurrentStyle();
  fClusterTree->Draw(
  Form("bigcalClusters.fCherenkovBestNPESum[]",i+1),
  Form("bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1&&Iteration$==3",i+1)
 ,"same");

}

  c->SaveAs(Form("plots/perp4.7/cer_nclust_decomp_%d.ps",runnumber));
  c->SaveAs(Form("plots/perp4.7/cer_nclust_decomp_%d.jpg",runnumber));

return(0);
}
