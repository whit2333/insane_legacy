/**  Fits the cherenkov ADC spectra for each cluster
 *
 *   This script does the following:
 *   
 *   - With a geometry cut s.t. the Cherenkov cone is hitting one mirror,
 *      it plots the adc spectra with a two gaussian fit constrained so that they are at equal spacings
 *   - Fills the table cherenkov_performance with the fit results
 *   - Similar fits with a double mirror hit
 *   - Similar fits with a corner mirror hit
 */
Int_t cherenkov(Int_t runnumber=72999) {

  const char * detectorTree = "betaDetectors1";

  if(!rman->IsRunSet() ) rman->SetRun(runnumber);
  else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

  rman->GetCurrentFile()->cd();

  TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
     if(!fClusterTree) {
     std::cout << "No Cluster Tree Found \n";
  }

  if(!fClusterTree) {printf("Clusters tree not found eh\n"); return(-1); }

  gROOT->cd();
  TH1F * cer[8];

// Fitting functions and parameters
  Double_t xMin = 600.0;
  Double_t xMax = 6000.0;
  TFitResultPtr fitResult;
  TF1 *f1 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+[3]*exp(-0.5*((x-2.0*[1])/[4])**2)", xMin,xMax);
  TF1 *e1 = new TF1("e1","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
  TF1 *e2 = new TF1("e2","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
  e1->SetLineColor(kBlue);
  e2->SetLineColor(kGreen);

  TCanvas * c = new TCanvas("Cherenkov","Gas Cherenkov 1 ",500,800);
  c->Divide(2,4);

// Loop over each mirror
   for (int i=8;i>=1;i--) {

      c->cd(9-i); printf("Fitting PMT %d",i);

// 1 electron peak amplitude
      f1->SetParameter(0,100);
      f1->SetParLimits(0,10,9999999);
// 1 electron peak mean
      f1->SetParameter(1,1800);
      f1->SetParLimits(1,800,6000);
// 1 electron peak width
      f1->SetParameter(2,500);
      f1->SetParLimits(2,0,5000);
// 2 electron peak amplitude
      f1->SetParameter(3,1);
      f1->SetParLimits(3,0,9999);
// 2 electron peak width
      f1->SetParameter(4,2000);
      f1->SetParLimits(4,400,5000);

      fClusterTree->Draw(
         Form("bigcalClusters.fCherenkovBestADCSum[Iteration$]>>cer%d(100,3,6303)",i),
         Form("bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<6000&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1",i));
    cer[i-1] = (TH1F *) gROOT->FindObject(Form("cer%d",i));
    f1->SetParameter(1,cer[i-1]->GetMean(1));
    fitResult = cer[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);
if(fitResult->IsValid() ){
    e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
    e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
    e1->DrawCopy("same");
    e2->DrawCopy("same");
    
/// Filling the cherenkov_performance table with results
   TSQLServer * db = SANEDatabaseManager::GetManager()->dbServer;
   TString SQLCOMMAND("REPLACE into cherenkov_performance set "); 
   SQLCOMMAND +="`run_number`=";
   SQLCOMMAND += runnumber;
   SQLCOMMAND +=",`npe_1_amp`="; SQLCOMMAND += Form("%f",fitResult->Parameter(0));
   SQLCOMMAND +=",`npe_1_width`="; SQLCOMMAND += Form("%f",fitResult->Parameter(2));
   SQLCOMMAND +=",`npe_1_mean`="; SQLCOMMAND += Form("%f",fitResult->Parameter(1));
   SQLCOMMAND +=",`npe_2_width`="; SQLCOMMAND += Form("%f",fitResult->Parameter(4));
   SQLCOMMAND +=",`npe_2_amp`="; SQLCOMMAND += Form("%f",fitResult->Parameter(3));
   SQLCOMMAND +=",`channel`=";  SQLCOMMAND += Form("%d",i);

   if(db) db->Query(SQLCOMMAND.Data());
// }

   }

   
}

  c->SaveAs(Form("plots/%d/MirrorsWithTDCHit.jpg",runnumber));

  TH1F * cer1Mir[8];
  TCanvas * c2 = new TCanvas("Cherenkov2","Gas Cherenkov 2",500,800);
  c2->Divide(2,4);

   for (int i=8;i>=1;i--) {
      c2->cd(9-i);printf("Fitting PMT %d",i);
// 1 electron peak amplitude
  f1->SetParameter(0,100);
  f1->SetParLimits(0,10,9999999);
// 1 electron peak mean
  f1->SetParameter(1,1800);
  f1->SetParLimits(1,800,6000);
// 1 electron peak width
  f1->SetParameter(2,500);
  f1->SetParLimits(2,0,5000);
// 2 electron peak amplitude
  f1->SetParameter(3,1);
  f1->SetParLimits(3,0,9999);
// 2 electron peak width
  f1->SetParameter(4,2000);
  f1->SetParLimits(4,400,5000);
    fClusterTree->Draw(Form("bigcalClusters.fCherenkovBestADCSum[]>>cerSingle%d(100,3,6303)",i),
                       Form("bigcalClusters.fNumberOfMirrors[Iteration$]==1&&bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<6000&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1",i));
    cer1Mir[i-1] = (TH1F *) gROOT->FindObject(Form("cerSingle%d",i));
    f1->SetParameter(1,cer1Mir[i-1]->GetMean(1));
    fitResult = cer1Mir[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);
if(fitResult->IsValid() ){
    e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
    e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
    e1->DrawCopy("same");
    e2->DrawCopy("same");

    
     }
}
  c2->SaveAs(Form("plots/%d/MirrorsWithTDCHitAndFullCone.jpg",runnumber));


  TH1F * cerMultiMir[8];

  TCanvas * c3 = new TCanvas("Cherenkov3","Gas Cherenkov 3",500,800);
  c3->Divide(2,4);

   for (int i=8;i>=1;i--) {
      c3->cd(9-i);printf("Fitting PMT %d",i);
// 1 electron peak amplitude
  f1->SetParameter(0,100);
  f1->SetParLimits(0,10,9999999);
// 1 electron peak mean
  f1->SetParameter(1,1800);
  f1->SetParLimits(1,800,6000);
// 1 electron peak width
  f1->SetParameter(2,500);
  f1->SetParLimits(2,0,5000);
// 2 electron peak amplitude
  f1->SetParameter(3,1);
  f1->SetParLimits(3,0,9999);
// 2 electron peak width
  f1->SetParameter(4,2000);
  f1->SetParLimits(4,400,5000);
      fClusterTree->Draw(Form("bigcalClusters.fCherenkovBestADCSum[]>>cerMulti%d(100,3,6303)",i),
                       Form("bigcalClusters.fNumberOfMirrors[Iteration$]>1&&bigcalClusters.fPrimaryMirror[Iteration$]==%d&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<6000&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1",i));
    cerMultiMir[i-1] = (TH1F *) gROOT->FindObject(Form("cerMulti%d",i));
    f1->SetParameter(1,cerMultiMir[i-1]->GetMean(1));
    fitResult = cerMultiMir[i-1]->Fit(f1,"E,S,B,Q","",xMin,xMax);
 if(fitResult->IsValid() ){
   e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
    e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
    e1->DrawCopy("same");
    e2->DrawCopy("same");
    }
   }
  c3->SaveAs(Form("plots/%d/MirrorsWithTDCHitAndSplitCone.jpg",runnumber));

//_____________________________________________________________________
//   c->cd(1);
//     fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum[Iteration$]",
//                        "bigcalClusters.fPrimaryMirror[Iteration$]==8&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","same");
//   c->cd(2);
//     fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum[Iteration$]",
//                        "bigcalClusters.fPrimaryMirror[Iteration$]==7&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","same");
// 
//   c->cd(3);
//     fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum[Iteration$]",
//                        "bigcalClusters.fPrimaryMirror[Iteration$]==6&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","same");
// 
//   c->cd(4);
//     fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum[Iteration$]",
//                        "bigcalClusters.fPrimaryMirror[Iteration$]==5&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","same");
// 
//   c->cd(5);
//     fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum[Iteration$]",
//                        "bigcalClusters.fPrimaryMirror[Iteration$]==4&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","same");
// 
//   c->cd(6);
//     fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum[Iteration$]",
//                        "bigcalClusters.fPrimaryMirror[Iteration$]==3&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","same");
// 
//   c->cd(7);
//     fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum[Iteration$]",
//                        "bigcalClusters.fPrimaryMirror[Iteration$]==2&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","same");
// 
//   c->cd(8);
//     fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum[Iteration$]",
//                        "bigcalClusters.fPrimaryMirror[Iteration$]==1&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","same");
// 
/*
  TCanvas * c2 = new TCanvas("Bigcal","Bigcal windows",500,800);
  c2->Divide(2,4);
  c2->cd(1);
    fClusterTree->Draw("bigcalClusters.fYMoment[Iteration$]:bigcalClusters.fXMoment[Iteration$]>>bc8(60,-65,65,120,-120,120)",
                       "bigcalClusters.fPrimaryMirror[Iteration$]==8&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","COLZ");
  TH1 * bc8 = (TH1 *) gROOT->FindObject("bc8");

  c2->cd(2);
    fClusterTree->Draw("bigcalClusters.fYMoment[Iteration$]:bigcalClusters.fXMoment[Iteration$]>>bc7(60,-65,65,120,-120,120)",
                       "bigcalClusters.fPrimaryMirror[Iteration$]==7&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE<4700&&bigcalClusters.fGoodCherenkovTDCHit==1","COLZ");
  TH1 * bc7 = (TH1 *) gROOT->FindObject("bc7");

  c2->cd(3);
    fClusterTree->Draw("bigcalClusters.fYMoment[Iteration$]:bigcalClusters.fXMoment[Iteration$]>>bc6(60,-65,65,120,-120,120)",
                       "bigcalClusters.fPrimaryMirror[Iteration$]==6&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","COLZ");
  TH1 * bc6 = (TH1 *) gROOT->FindObject("bc6");

  c2->cd(4);
    fClusterTree->Draw("bigcalClusters.fYMoment[Iteration$]:bigcalClusters.fXMoment[Iteration$]>>bc5(60,-65,65,120,-120,120)",
                       "bigcalClusters.fPrimaryMirror[Iteration$]==5&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","COLZ");
  TH1 * bc5 = (TH1 *) gROOT->FindObject("bc5");

  c2->cd(5);
    fClusterTree->Draw("bigcalClusters.fYMoment[Iteration$]:bigcalClusters.fXMoment[Iteration$]>>bc4(60,-65,65,120,-120,120)",
                       "bigcalClusters.fPrimaryMirror[Iteration$]==4&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","COLZ");
  TH1 * bc4 = (TH1 *) gROOT->FindObject("bc4");

  c2->cd(6);
    fClusterTree->Draw("bigcalClusters.fYMoment[Iteration$]:bigcalClusters.fXMoment[Iteration$]>>bc3(60,-65,65,120,-120,120)",
                       "bigcalClusters.fPrimaryMirror[Iteration$]==3&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","COLZ");
  TH1 * bc3 = (TH1 *) gROOT->FindObject("bc3");

  c2->cd(7);
    fClusterTree->Draw("bigcalClusters.fYMoment[Iteration$]:bigcalClusters.fXMoment[Iteration$]>>bc2(60,-65,65,120,-120,120)",
                       "bigcalClusters.fPrimaryMirror[Iteration$]==2&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","COLZ");
  TH1 * bc2 = (TH1 *) gROOT->FindObject("bc2");

  c2->cd(8);
    fClusterTree->Draw("bigcalClusters.fYMoment[Iteration$]:bigcalClusters.fXMoment[Iteration$]>>bc1(60,-65,65,120,-120,120)",
                       "bigcalClusters.fPrimaryMirror[Iteration$]==1&&bigcalClusters.fTotalE[Iteration$]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1","COLZ");
  TH1 * bc1 = (TH1 *) gROOT->FindObject("bc1");
    c2->SaveAs(Form("plots/%d/MirrorsWisthSingleTDCHitBigcal.jpg",runnumber));*/

}
