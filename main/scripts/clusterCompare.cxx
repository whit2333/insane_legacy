Int_t SecondPass(Int_t runNumber = 72994) {

  const char * sourceTreeName = "betaDetectors1";

  const char * outputA = "ClustersA";

  rman->SetRun(runNumber);

  TTree * fClusterTree = (TTree*)rman->GetCurrentFile()->Get("Clusters");

  TCanvas * c = new TCanvas("SomeAnalysis","Some Analysis", 800,600);

  c->Divide(2,2);

c->cd(1);
 fClusterTree->Draw("bigcalClusters.fYMoment>>cy(200,-110,110)","","");c->cd(2);
c->cd(2);
  fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum>>npe(120,1,41)","","");
c->cd(3);
  fClusterTree->Draw("bigcalClusters.fTotalE>>cE(100,500,5500)","");
c->cd(4);
  fClusterTree->Draw("bigcalClusters.fXMoment>>cx(100,-60,60)","","");

//&&bigcalClusters.fYStdDeviation>0.1
//&&bigcalClusters.fYStdDeviation>0.1

  fClusterTree->Draw(">>GoodeClusterList","bigcalClusters.fGoodCherenkov==1","entrylist");

  TEntryList *fGoodeClusterList = (TEntryList*)gDirectory->Get("GoodeClusterList");

  fClusterTree->SetEntryList(fGoodeClusterList);

/*  fClusterTree->StartViewer();*/

c->cd(1);
 fClusterTree->Draw("bigcalClusters.fYMoment","","same");
c->cd(2);
  fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum","","same");
/*  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","same");*/
c->cd(3);
  fClusterTree->Draw("bigcalClusters.fTotalE","","same");
c->cd(4);
  fClusterTree->Draw("bigcalClusters.fXMoment","","same");

c->cd(1);
 fClusterTree->Draw("bigcalClusters.fYMoment","bigcalClusterEvent.fNClusters==1","same");
c->cd(2);
  fClusterTree->Draw("bigcalClusters.fCherenkovBestNPESum","bigcalClusterEvent.fNClusters==1","same");
/*  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment","","same");*/
c->cd(3);
  fClusterTree->Draw("bigcalClusters.fTotalE","bigcalClusterEvent.fNClusters==1","same");
c->cd(4);
  fClusterTree->Draw("bigcalClusters.fXMoment","bigcalClusterEvent.fNClusters==1","same");

  fClusterTree->Draw(">>bestList","bigcalClusterEvent.fNClusters==1","entrylist");
  TEntryList *fbestList = (TEntryList*)gDirectory->Get("bestList");
  fClusterTree->SetEntryList(fbestList);

   new TCanvas("ParallelCoords","ParallelCoords");

   fClusterTree->Draw(   "bigcalClusters.fCherenkovBestNPESum:bigcalClusters.fYSkewness:bigcalClusters.fYStdDeviation:bigcalClusters.fYMoment:bigcalClusters.fTotalE","","para");




/// PARALLEL Coordinates Plots

   TParallelCoord* para = (TParallelCoord*)gPad->GetListOfPrimitives()->FindObject("ParaCoord");
   TParallelCoordVar* npe = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fCherenkovBestNPESum");
   npe->AddRange(new TParallelCoordRange(npe,5,30));



//    TParallelCoordVar* sigx = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fXStdDeviation");
//    sigx->AddRange(new TParallelCoordRange(sigx,0,7.0));

   TParallelCoordVar* sigy = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fYStdDeviation");
   sigy->AddRange(new TParallelCoordRange(sigy,0,7.0));
   TParallelCoordVar* ypos = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fYMoment");
   ypos->AddRange(new TParallelCoordRange(ypos,-106,106.0));

//     TParallelCoordVar* skewx = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fXSkewness");
//    skewx->AddRange(new TParallelCoordRange(skewx,-6,7.0));

    TParallelCoordVar* skewy = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fYSkewness");
   skewy->AddRange(new TParallelCoordRange(skewy,-6,7.0));

   TParallelCoordVar* energy = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fTotalE");
   energy->AddRange(new TParallelCoordRange(energy,1000.0,6000.0));

   para->AddSelection("bigcalClusters.fGoodCherenkov");
   para->GetCurrentSelection()->SetLineColor(kViolet);


return(0);
}
