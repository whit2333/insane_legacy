/** Lucite Bar Geometry
 *  
 * Should be called from a MakePlots() for a calculation. 
 * \todo clean up calculation -> MakePlot() Analysis connection
 *
 * This script gets the calibrated positions for the hodoscope
 * from cluster information. 
 * 
 */
Int_t luciteBarGeometry(Int_t runnumber = 72995,Int_t nevents=100000) {

  const char * detectorTree = "betaDetectors1";

  if(!rman->IsRunSet() ) rman->SetRun(runnumber);
  else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
/*   rman->SetRun(runnumber);*/
   
   TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   if( !t ) { 
      printf("Tree:%s \n    was NOT FOUND. Aborting... \n",detectorTree); 
      return(-1);
   }

//   Double_t maxBinValues[28];

    TCut cTrigger = "triggerEvent.fCodaType==5&&triggerEvent.IsNeutralPionEvent()==0";
    TCut cPi0Trigger = "triggerEvent.IsNeutralPionEvent()==1"
    TCut cLuciteBar = "fLuciteHodoscopeEvent.fPositionHits[Iteration$].fPositionVector.fY==1";
    TCut cCherenkovTDCHit = "bigcalClusters.fGoodCherenkovTDCHit==1";

   rman->fCurrentFile->cd("calibrations/hodoscope");


   InSANEDetectorCalibration * lucitePositionCalibration =  new InSANEDetectorCalibration();

   lucitePositionCalibration->SetNameTitle("lucPosCal","Luc Hodoscope Bigcal Pos Calibration");
   lucitePositionCalibration->fCalibrations->Clear();

   TF1 *f1;
   TH2F * h2;
   TH1F * h1;
   TProfile * p1;


   TCanvas * c = new TCanvas("hodoscopePositionCal","Hodoscope Pos Calibration",800,900);
   c->Divide(3,3);

   // First Draws lucite positions X vs Y. 
   c->cd(1);
   t->Draw("fLuciteHodoscopeEvent.fPositionHits[].fPositionVector.fY:fLuciteHodoscopeEvent.fPositionHits[Iteration$].fPositionVector.fX>>hluc(100,-500,500,56,1,29)",
      cTrigger ,
      "colz");

   // Second Draws  the Luicte X position vs the Cluster X moment 
   c->cd(2);
   t->Draw("bigcalClusters.fXMoment:fLuciteHodoscopeEvent.fPositionHits.fPositionVector.fX>>pos(100,-500,500, 100,-60,60)",
      cTrigger,
      "colz,goff");
   h2  = (TH2F*)gROOT->FindObject("pos");
   h2->SetMaximum(100);
   h2->Draw("colz");
 
   // Third For each bar, draw in two separate histograms, Lucite Y position   
   // Get the location of peak. It is usually on the left side and falls for gits within a few 
   // hundred ns (units? ask hovannes or go back to analyzer )
   // Subtract the peak from the difference and draw ... 
   // \todo Need to look at run-averaged peak locations. Do they vary much?
   // 
   TString options = "";
   for(int i = 0;i<28;i++){
// 
      f1 = new TF1(Form("fLucPosCalib%d",i+1), "[0] +[1]*x ", 0,150);

      lucitePositionCalibration->fCalibrations->Add(f1);
      TCut cLuciteBar = Form("fLuciteHodoscopeEvent.fPositionHits[Iteration$].fPositionVector.fY==%d",i+1);

// draw tdc difference (x-position) 
      c->cd(3);
      if(i!=0) options="same";
      t->Draw(Form("fLuciteHodoscopeEvent.fPositionHits[].fPositionVector.fX>>hluc%d(100,-500,500)",i+1),
           cTrigger && cLuciteBar,options.Data());
      h1  = (TH1F*)gROOT->FindObject(Form("hluc%d",i+1));

      lucitePositionCalibration->fConstants->push_back( h1->GetBinCenter(h1->GetMaximumBin()));
         std::cout << "Lucite Bar, " << i+1 << ", has its maximum time difference at " 
		   <<  lucitePositionCalibration->fConstants->at(i) << " ns (need to check unit). \n";

   c->cd(4);
   t->Draw(Form("fLuciteHodoscopeEvent.fPositionHits[].fPositionVector.fX - %e >>hlucCorrected%d(100,-500,500)",
	lucitePositionCalibration->fConstants->at(i),i+1),cTrigger && cLuciteBar,options.Data());

   c->cd(5);
   t->Draw(Form("bigcalClusters.fXMoment:fLuciteHodoscopeEvent.fPositionHits[].fPositionVector.fX - %e >>hlucCorVsBigcal%d(100,-100,300,100,-80,80)",
			   lucitePositionCalibration->fConstants->at(i),
			   i+1),cTrigger && cLuciteBar,Form("colz"));

   c->cd(6);
   t->Draw(Form(
    "bigcalClusters.fXMoment:fLuciteHodoscopeEvent.fPositionHits[].fPositionVector.fX - %e >>hlucCorVsBigcalPi0%d(100,-300,300,100,-80,80)",
			   lucitePositionCalibration->fConstants->at(i),
                i+1),
     cPi0Trigger && cLuciteBar,
     Form("colz,%s",options.Data()));

   c->cd(7);
   t->Draw(Form(
    "bigcalClusters.fXMoment:fLuciteHodoscopeEvent.fPositionHits[].fPositionVector.fX-%e>>hbcluc%d(100,-100,300,100,-80,80)",
			   lucitePositionCalibration->fConstants->at(i),
                i+1),
         cTrigger && cLuciteBar ,
         Form("goff,colz") );
      h2  = (TH2F*)gROOT->FindObject(Form("hbcluc%d",i+1));
      p1 = h2->ProfileX(Form("hbcluc%d_pfx",i+1));
      p1->Fit(f1,"R,Q","goff");
      p1->DrawClone(options.Data());


   }
/*   Double_t maxValue = h1->GetBinContent(h1->GetMaximumBin());*/
//    c->cd(8);
//    t->Draw(Form("fLuciteHodoscopeEvent.fPositionHits[].fPositionVector.fY:fLuciteHodoscopeEvent.fPositionHits[Iteration$].fPositionVector.fX-%e>>hLucXY(100,-100,300,56,1,29)",
// 			   lucitePositionCalibration->fConstants->at(i)),
//       cTrigger && cCherenkovTDCHit,
//       "colz");
 //   lucitePositionCalibration->Write("lucitePositionCalibration",kOverwrite);
    lucitePositionCalibration->Write("lucitePositionCalibration",TObject::kWriteDelete);
      
      
   c->SaveAs(Form("plots/%d/lucitePositions.jpg",runnumber));

   std::cout << "tree has " << t->GetEntries() << " entries.\n";

   //t->StartViewer();

   //TBrowser * b = new TBrowser();

//    c->SaveAs("results/efficiencies/cerEff.clustE1.5GeV.png");

//    TTree * ct = (TTree*)gROOT->FindObject(clusterTree);
//    if( !ct ) { 
//       printf("Tree:%s \n    was NOT FOUND. Aborting... \n",clusterTree); 
//       return(-1);
//    }

// Useful pointers
   BigcalEvent * bcEvent = 0; 
   InSANEClusterEvent * clusterEvent = 0; 

// Create sane events for detector tree
/*   SANEEvents * events = new SANEEvents( detectorTree );*/
//    bcEvent = (events->BETA->fBigcalEvent);

// Setup analysis
//    InSANEClusterAnalysis * clusterAnalysis = new InSANEClusterAnalysis( clusterTree );
//    clusterAnalysis->SetClusterTree(ct);
//    ct->BuildIndex("fRunNumber","fEventNumber");
//    t->AddFriend(ct);

// Setup detectors
//    BigcalDetector * bcdet = new BigcalDetector(runnumber);
//    bcdet->SetEventAddress( bcEvent );
//    bcdet->SetClusterEventAddress( clusterAnalysis->fClusterEvent );
//    GasCherenkovDetector * gcdet = new GasCherenkovDetector(runnumber);
//    gcdet->SetEventAddress( (GasCherenkovEvent*)events->BETA->fGasCherenkovEvent );
//    LuciteHodoscopeDetector * lhdet = new LuciteHodoscopeDetector(runnumber);
//    lhdet->SetEventAddress( (LuciteHodoscopeEvent*)events->BETA->fLuciteHodoscopeEvent );

// Setup Display
//    Pi0EventDisplay * display = new Pi0EventDisplay();
//    display->fDataHists->Add(bcdet->fEventADCHist);
//    display->fDataHists->Add(bcdet->fEventTimingHist);
//    display->fDataHists->Add(gcdet->fCerHits);
//    display->fDataHists->Add(lhdet->fLuciteHitDisplay);
//    display->fDataHists->Add(bcdet->fEventTrigTimingHist);
//    display->fDataHists->Add(bcdet->fElectronClustersHist);
//    display->fDataHists->Add(bcdet->fChargedPiClustersHist);
//    display->fWaitPrimitive=0;

// Loop through events
//    for(int i = 1000; i < t->GetEntries() && i < 2000 ;i++) {
//       std::cout << "event " << i << "\n";
//       t->GetEntry(i);
// 
//       gcdet->ProcessEvent();
//       bcdet->ProcessEvent();
//       lhdet->ProcessEvent();
// 
//       display->UpdateDisplay();
// 
//       bcdet->ClearEvent();
//       lhdet->ClearEvent();
//       gcdet->ClearEvent();
//    }
// t->Draw(">>all",
//         "triggerEvent->IsBETAEvent()==1&&triggerEvent->fEventNumber>1000");
//   TEventList *elist = (TEventList*)gDirectory->Get("all");
//   t->SetEventList(elist);

//   TCanvas * c = new TCanvas("Lucite","Lucite Hodoscope",500,800);
//     c->Divide(2,2);
//     c->cd(1);
//     t->Draw("betaDetectorEvent.fGasCherenkovEvent->fHits[]->fTDC>>cer1tdc",
//             "betaDetectorEvent.fGasCherenkovEvent->fHits[Iteration$]->fLevel==1&&betaDetectorEvent.fGasCherenkovEvent->fHits[Iteration$]->fMirrorNumber==2");
//     t->StartViewer();
/*
  t->Draw(">>all","triggerEvent->IsBETAEvent()==1&&triggerEvent->fEventNumber>1000&&bigcalClusters.fTotalE[]>1000&&bigcalClusters.fTotalE[Iteration$]<4700&&bigcalClusters.fCherenkovBestNPESum[Iteration$]>6");
  TEventList *elist = (TEventList*)gDirectory->Get("all");
  t->SetEventList(elist);


    t->Draw(Form("bigcalClusters.fjPeak[]:betaDetectorEvent.fLuciteHodoscopeEvent->fHits[]->fRow>>luc%d",1),
                       Form("(betaDetectorEvent.fLuciteHodoscopeEvent->fHits[Iteration$]->fPMTNumber-1)%2==%d&&betaDetectorEvent.fLuciteHodoscopeEvent->fHits[Iteration$]->fLevel==1&&bigcalClusters.fGoodDoubleLuciteTDCHit==1",0),"colz");
//&&betaDetectorEvent.fLuciteHodoscopeEvent->fHits[Iteration$]->fPassesTiming[2]==1
//    c->cd(i*2+2);
    c->cd(2);
    t->Draw(Form("bigcalClusters.fjPeak:betaDetectorEvent.fLuciteHodoscopeEvent->fHits[]->fRow>>luc%d",2),
                       Form("(betaDetectorEvent.fLuciteHodoscopeEvent->fHits[Iteration$]->fPMTNumber-1)%2==%d&&betaDetectorEvent.fLuciteHodoscopeEvent->fHits[Iteration$]->fLevel==1&&bigcalClusters.fGoodDoubleLuciteTDCHit==1",1),"colz");

    c->cd(3);
    t->Draw(Form("bigcalClusters.fjPeak[]"),
                       Form("bigcalClusters.fGoodLuciteTDCHit[Iteration$]==1&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1"),"");
    c->cd(4);
    t->Draw(Form("bigcalClusters.fjPeak[]"),
                       Form("bigcalClusters.fGoodDoubleLuciteTDCHit[Iteration$]==1&&bigcalClusters.fGoodCherenkovTDCHit[Iteration$]==1"),"");

   c->SaveAs(Form("plots/%d/lucite_bar_geo.png",runnumber));
   c->SaveAs(Form("plots/%d/lucite_bar_geo.ps",runnumber));*/


  return(0);
}
