Int_t cherenkov_pure() {

TChain chain("data/rootfiles/InSANE72994.*.root/betaDetectors1","betaDetectors1Chain");
chain.Add("data/rootfiles/InSANE72999.*.root/betaDetectors1");
chain.Add("data/rootfiles/InSANE72994.*.root/betaDetectors1");

TCut cBigcal1= "fBigcalEvent.fTotalEnergyDeposited > 2000";

TCut cCherenkov1 = "fGasCherenkovEvent.fHits[Iteration$].fLevel==0";
TCut cCherenkovMirror = "fGasCherenkovEvent.fHits[Iteration$].fMirrorNumber==1";
TCut cCherenkov2 = "fGasCherenkovEvent.fNumberOfTDCHits==3";

TCut cCherenkovTDC = "TMath::Abs(fGasCherenkovEvent.fHits[Iteration$].fTDCAlign) < 10";

TCut cTrigger = "triggerEvent.IsBETAEvent()";

TCanvas * c = new TCanvas("cherenkov","cherenkov");
c->Divide(1,2);
c->cd(1);
chain.Draw("fGasCherenkovEvent.fHits[].fADCAlign>>hCerPure(100,0,3)",cCherenkov1 && cTrigger && cBigcal1 && cCherenkovMirror );
c->cd(2);
chain.Draw("fGasCherenkovEvent.fHits[].fADCAlign>>h2CerPure(100,0,3)",cCherenkov1 && cTrigger && cBigcal1 && cCherenkovMirror && cCherenkov2,"");


std::cout << "Chain has " << chain.GetEntries() << " entries.\n";


return(0);
}