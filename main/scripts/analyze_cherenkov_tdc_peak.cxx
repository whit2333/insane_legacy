#include <iostream.h>

// Lorenzian Peak function

Double_t lorentzianPeak(Double_t *x, Double_t *par) {
return (0.5*par[0]*par[1]/TMath::Pi()) /
TMath::Max( 1.e-10,
(x[0]-par[2])*(x[0]-par[2]) + .25*par[1]*par[1]
);
}

int analyze_cherenkov_tdc_peak(int runNumber = 72994 ){
gROOT->Reset();

BETAEvent * anEvent = new BETAEvent();// event->aBETAEvent;

TFile * oldFile =new TFile(Form("data/rootfiles/SANE%d.root",runNumber));

/*TChain * ch = new TChain("betaDetectors");
ch->Add(Form("data/BErootfiles/BE%d*.root",runNumber));
//ch->Print();
printf(" filename %s with %d trees " , Form("data/BE_rootfiles/rawBE%d*.root",runNumber),ch->GetNtrees());
//TChain * t = &ch;
//TFile * file = new TFile(fname);
*/
ch = (TTree *)gROOT->FindObject("betaDetectors");

/*
if(ch) {
  TBranch *branch  = ch.GetBranch("anEvent");
   branch->SetAddress(&event);
      } // 
if(branch) printf("branch good \n");
*/

 ch->SetBranchAddress("betaDetectorEvent",&anEvent);
int i=0;


TFile * newFile =new TFile(Form("data/analysis_rootfiles/cherenkov_tdc%d.root",runNumber),"RECREATE");

////// HISTOGRAMS ///////
TH1F * cer_tdc_hist[12];
TH1F * cer_tdc_hist_wTotalEcut[12];
TH2F * cer_tdc_vs_adc_hist[12];

//TH1F * missingChannel = new TH1F("missingADCChannel","Missing ADC Channel Count event_type=5", 15,0,15);
//TH1F * nADChits = new TH1F("nADChits","Number of ADC hitsi event_type=5", 15,0,15);
//TH1F *  cer_adc_hist_wIndTDCCut[i] = new TH1F(Form("cer_adc_hist_%d_wIndTDCCut",i),Form("Cherenkov ADC %d w/ Indiv. TDC cuts",i), 100,-1000, 3000);

for(i=0;i<12;i++) {
     cer_tdc_hist[i] = new TH1F(Form("cer_tdc_hist_%d",i),Form("Cherenkov TDC %d",i), 200,-3000,-1200);
     cer_tdc_hist_wTotalEcut[i] = new TH1F(Form("cer_tdc_hist_wTotalEcut_%d",i),Form("Cherenkov TDC vs ADC %d",i),  200,-3000,-1200);
     cer_tdc_vs_adc_hist[i] = new TH2F(Form("cer_tdc_vs_adc_hist_%d",i),Form("Cherenkov TDC %d",i), 200,-100,1800,  200,-3000,-1200);
}

///// Analysis Flags /////
bool odd_ceradc_hit = false;

///// CUT PARAMETERS /////
int tdclow = -2750;
int tdchigh = -1300;
int k; // associated with tdc loops
int j;
int tempTDC;
int tempADC;
int unusedADCChannel;
int tempMirrorNumber;
bool adchit[15];
int adchitcount[15];
int hitsPerChan[15];
bool hms_trig;     // gen_event_trigtype[0]
bool beta1_trig;   // gen_event_trigtype[1]
bool pi0_trig;     // gen_event_trigtype[2]
bool beta2_trig;   // gen_event_trigtype[3]
bool coin_trig;    // gen_event_trigtype[4]
bool hms_cosmic_trig;    // gen_event_trigtype[5]
bool LED_trig;    // gen_event_trigtype[6]
bool ped_trig;    // gen_event_trigtype[7]
int sumOf8;

double geocut_point_x[5];
double geocut_point_y[5];
double geocut_slope[6];

geocut_point_x[0]=0.0;
geocut_point_x[1]=1.0;
geocut_point_x[2]=0.0;
geocut_point_x[3]=0.50;
geocut_point_x[4]=0.0;

geocut_point_y[0]=-59.0;
geocut_point_y[1]=-58.9;
geocut_point_y[2]=0.0;
geocut_point_y[3]=59.0;
geocut_point_y[4]=59.1;

geocut_slope[0]=-50.0;
geocut_slope[1]=-0.020;
geocut_slope[2]=0.0;
geocut_slope[3]=0.20;
geocut_slope[4]=50.0;
geocut_slope[5]=0.0;

////////////////////////
///// EVENT LOOP  //////
////////////////////////
Int_t nevent = ch->GetEntries();
GasCherenkovHit * cerHit = new GasCherenkovHit();

  TClonesArray * cerHits = new TClonesArray ("GasCherenkovHit",20) ;
  cerHits = anEvent->fGasCherenkovEvent->fHits ;
  cerHits->ExpandCreateFast (1) ; 
  GasCherenkovHit * aCerHit = new GasCherenkovHit();

cout << "\n" << nevent << " ENTRIES \n";
   for (int iEVENT=0;(iEVENT<nevent && iEVENT<100000) ;iEVENT++) 
    {
if( iEVENT%10000 == 0 ) printf(" At event %d \n",iEVENT);
    ch->GetEvent(iEVENT);   
   if ( anEvent->gen_event_type==5 && anEvent->gen_event_trigtype[3]   ) {

        ///// CHERENKOV //////

for (kk=0; kk< anEvent->fGasCherenkovEvent->fNumberOfHits;kk++) 
  {
    aCerHit = (GasCherenkovHit*)(*cerHits)[kk] ;
/*    cout <<  aCerHit->fADC << " ADC \n";
    cout <<  aCerHit->fTDC << " TDC \n";
    cout <<  aCerHit->fMirrorNumber << " MirrorNumber \n";
    cout <<  aCerHit->fPMTNumber[0] << " PMTNumber \n";
*/ 
      cer_tdc_hist[aCerHit->fMirrorNumber - 1]->Fill(aCerHit->fADC);
      cer_tdc_vs_adc_hist[aCerHit->fMirrorNumber -1 ]->Fill(aCerHit->fADC,aCerHit->fTDC);
    }
  

    } // END of trigger cut
    } // END OF EVENT LOOP

/*
 TF1 * timewalk = new TF1("timewalk", "[0]+[1]*x+[2]*x*x",0,1000);
// Find Peak(s) in TDC spectrum.
for(i=0;i<12;i++) {
 TFitResult r = cer_tdc_vs_adc_hist[i]->Profile()->Fit(timewalk,"E","S,goff" , -100,1000)
cout << "chi squared" << r->Chi2() << " \n ";
}
*/

 TF1 * timewalk = new TF1("timewalk", "[0]+[1]*x+[2]*x*x",0,1000);
 TF1 *lorentzian = new TF1("lorentzian",lorentzianPeak,-2500,-1000,3);

TSpectrum * s = new TSpectrum(1);
Double_t  peak[12];
Double_t  peakwidth[12];
Double_t r[12][3];

TCanvas * c = new TCanvas("Individuals");
c->Divide(8,2);

for(i=0;i<8;i++) {
  c->cd(i+1);
    s->Search(cer_tdc_hist[i],10," ",0.05);
    peak[i]= s->GetPositionX()[0];
lorentzian->SetParameter(0,3000); 
lorentzian->SetParameter(2,peak[i]); 
lorentzian->SetParameter(1,60); 
    cer_tdc_hist[i]->Fit(lorentzian,"E,M","S",peak[i]-100,peak[i]+100);    cer_tdc_hist[i]->Draw();
peakwidth[i]=lorentzian->GetParameter(1);

  c->cd(8+i+1);
//cer_tdc_vs_adc_hist[i]->Draw("colz");
   cer_tdc_vs_adc_hist[i]->ProfileX()->Fit(timewalk,"E","S,goff" , -50,1500);
r[i][0]=timewalk->GetParameter(0);
r[i][1]=timewalk->GetParameter(1);
r[i][2]=timewalk->GetParameter(2);
//cout << "chi squared" << r->Chi2() << " \n ";
//cer_tdc_vs_adc_hist[i]->ProfileX()->Draw();
}

TString plotDirector("tdc_peak_plots/");
TString plotFileName(Form("Individual_tdcs_%d.pdf",runNumber));
TString plotFileNamejpg(Form("Individual_tdcs_%d.jpg",runNumber));
TString plotFileName2(Form("Sum_tdcs_%d.pdf",runNumber));
TString plotFileName2jpg(Form("Sum_tdcs_%d.jpg",runNumber));

c->SaveAs( plotDirector+plotFileName );
c->SaveAs( plotDirector+plotFileNamejpg );

TCanvas * c2 = new TCanvas("Sums");
c2->Divide(4,2);
for(i=8;i<12;i++) {
  c2->cd(i+1-8);
  cer_tdc_hist[i]->Draw();
s->Search(cer_tdc_hist[i],10," ",0.05);
if(s->GetNPeaks() > 0) peak[i]= s->GetPositionX()[0];
  c2->cd(4+i+1 -8);
//  cer_tdc_vs_adc_hist[i]->Draw("colz");
 cer_tdc_vs_adc_hist[i]->ProfileX()->Fit(timewalk,"E","S goff" , -100,1000);

r[i][0]=timewalk->GetParameter(0);
r[i][1]=timewalk->GetParameter(1);
r[i][2]=timewalk->GetParameter(2);
//cout << "chi squared" << r[i].Chi2() << " \n ";
//cer_tdc_vs_adc_hist[i]->ProfileX()->Draw();
}

c2->SaveAs( plotDirector+plotFileName2 );
c2->SaveAs( plotDirector+plotFileName2jpg );

s->Print();

  TSQLServer * db = TSQLServer::Connect("mysql://localhost/SANE", "sane", "secret");
  TSQLResult * res;


TString SQLq("Insert into cherenkov_timing_parameters set "); // dont forget the extra space
// at the end

SQLq +=" run_number=" ; // Dont add space here
SQLq +=runNumber;
SQLq +=", number_of_events=";
SQLq +=nevent;
//  starting Date and time 
  TDatime * dt = new TDatime();
  TString datetime( dt->AsSQLString() );
  TString date(datetime(0,10));
  TString time(datetime(11,8));

SQLq += ", date_calculated='";
SQLq += date.Data(); 
SQLq += "', time_calculated='";
SQLq += time.Data();
SQLq += "', filename_1='<a href=\"http://quarks.temple.edu/~whit/SANE/databases/cherenkov_timing_plots/";
SQLq += plotFileNamejpg.Data();
SQLq += "\"> Individual plots </a> ";
SQLq += " <a href=\"http://quarks.temple.edu/~whit/SANE/databases/cherenkov_timing_plots/";
SQLq += plotFileName.Data();
SQLq += "\"> [pdf] </a> ' ";

SQLq += ", filename_2='<a href=\"http://quarks.temple.edu/~whit/SANE/databases/cherenkov_timing_plots/";
SQLq += plotFileName2jpg.Data();
SQLq += "\"> Sum plots </a> ";
SQLq += " <a href=\"http://quarks.temple.edu/~whit/SANE/databases/cherenkov_timing_plots/";
SQLq += plotFileName2.Data();
SQLq += "\"> [pdf] </a> ' ";
for(i=0;i<12;i++) {
  SQLq += ", cer_tdc_peak_";
  SQLq += i;
  SQLq += "=";
  SQLq += (int)floor(peak[i]);
  SQLq += ", cer_tdc_width_";
  SQLq += i;
  SQLq += "=";
  SQLq += (int)floor(peakwidth[i]);
  for(j=0;j<3;j++) {
    SQLq += Form(", cer_timewalk_%d_par_%d=%f",i,j,r[i][j]);
  }
} 
// Fill MySQL table
   res = db->Query(SQLq.Data());
   cout << SQLq.Data() << "\n";
   db->Close();
   newFile->Write();

gROOT->ProcessLine(".q");

}
