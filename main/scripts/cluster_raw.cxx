int cluster_raw(int runNumber=72994) {
   
  BETAEvent * event = new BETAEvent();

  TChain * chain = new TChain("betaDetectors"); // name of tree
  chain->Add(Form("data/rootfiles/SANE%d*root",runNumber));
  chain->SetBranchAddress("betaDetectorEvent",&event);

trigEvent = new InSANETriggerEvent();
//hmsEvent = new TFile(Form("data/rootfiles/hms%s.root",option.Data()),"RECREATE");
  chain->SetBranchAddress("triggerEventData",&trigEvent);



  std::cout << chain->GetEntries() << " Entries " << "in " 
    << ((TChain * )chain)->GetNtrees() << " trees.\n" ;
  

  TFile * clustFile = new TFile(Form("data/rootfiles/clusters%d.root",runNumber
),"RECREATE");
// New output tree of clusters
  BIGCALCluster * aCluster = new BIGCALCluster();
  TClonesArray * bcClusters = new TClonesArray ("BIGCALCluster", 2) ;
  bcClusters->ExpandCreateFast (1) ; 
  TTree * clusterTree = new TTree("bigcalClusters","Clusters Tree");
          clusterTree->Branch ("bigcalClusterEvent", &bcClusters) ;
          clusterTree->AutoSave();

 
  TH2F * bc_cluster_src; // This is actually created by the cluster processer. 
// We just need a pointer.. ignore lines below 
// = new TH2F("bc_cluster_src","Bigcal for clustering",33,0,33,57,0,57);
// We pass this histogram to the cluster processor which finds clusters in some way using it.

  BIGCALClusterProcessor * clusterProcessor = new BIGCALClusterProcessor();
  clusterProcessor->SetEventAddress(event->fBigcalEvent);

  Int_t entries = chain->GetEntriesFast();

//  // CUTS
//   gROOT->ProcessLine(".L scripts/CutFunctions.cxx++");
//   InSANECut * aCut  = new InSANECut("bigcalTrigger","Cut on event type 5 and bigcal trigger bit");
//   aCut->SetEventAddress( (InSANEEvent *)event);
//   aCut->SetCutFunction( CutFunctions  );
//   aCut->Describe();

  for(Int_t i_event=0;i_event<entries;i_event++) {
      if(i_event%1000==0) cout << "Event " << i_event << "\n";
      chain->GetEntry(i_event);
     if ( trigEvent->gen_event_type==5 && trigEvent->gen_event_trigtype[3]   ) {
//      if( aCut->ApplyCut() ) {
//      cout << "event " << i_event << "\n";      
      clusterProcessor->ProcessEvent(bcClusters);// Fills this TClonesArray with cluster objects
      clusterTree->Fill(); // Done with clustering.
//      clusterProcessor->Clear();
      bcClusters->Clear();
    }// event type end
  } // Event loop ending

clusterTree->Write();

}

