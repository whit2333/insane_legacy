Int_t analyze_peds_tdcs(Int_t runnum = 0) {

  rman->SetRun(runnum);
  rman->CalculatePedestals();
  rman->CalculateTiming();

  rman->WriteRun();


  gROOT->ProcessLine(".q");
}
