/**  
 */
Int_t ElectronSelection(Int_t runNumber = 72994) {
   gROOT->SetBatch(kTRUE);

   const char * sourceTreeName = "Clusters";
   const char * outputTreeName1 = "Electrons";
   const char * outputTreeName2 = "NotElectrons";
// First open the run
   rman->SetRun(runNumber);

   InSANEDISTrajectory * electronEvent = 0;
   Pi0Event * pi0Event =0 ;

   if ( !rman->fCurrentRun) { printf("Please use SetRun(#) first"); return(-1); }

// Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
/*  for (std::vector<Int_t>::iterator it = fRunList.begin(); it!=fRunList.end(); ++it) {*/
/*    cout << *it << endl;*/
/*    rman->SetRun(*it);*/

    TTree * t = (TTree*)gROOT->FindObject(sourceTreeName);
    if( !t ) {
       printf("Tree:%s \n       was NOT FOUND. Aborting... \n",sourceTreeName); 
       return(-1);
    }

  SANEFinalAnalyzer * analyzer = new SANEFinalAnalyzer();
  SANEElectronSelector * electronSel = 0;
  electronSel = new SANEElectronSelector( outputTreeName, sourceTreeName );
  electronSel->SetClusterTree(new TTree(outputTreeName1,"Electrons"));

  analyzer->AddCalculation(electronSel);

/*    if(it == fRunList.begin()) {
       calibrator = new BigcalPi0Calibrator( outputTreeName, sourceTreeName );
       std::cout << " first run\n";
       cal1 = new Pi0Calibration();
       cal1->fjMax=31+4;
       cal1->fjMin=31-4;
       cal1->fiMin=5;
       cal1->fiMax=12;
       cal1->SetBlockList();
       calibrator->AddCalculation( cal1 );
    }
    calibrator->fInputTree = t;
    calibrator->SetClusterTree(t);
    cal1->SetClusterEvent(calibrator->fClusterEvent);*/
/*    if(it == fRunList.begin()) {*/
/*       calibrator->Initialize(t);*/
// 
//    }
//   calibrator->Process();


/// \todo this should go to a higher level
//  CreateNewFile();
/*   return(0);*/
//    
gROOT->ProcessLine(".q");
}
