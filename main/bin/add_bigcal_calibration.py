#!/usr/bin/env python

# adds calibratios to database from PARAM file
import numpy as np
import pylab as plt
import matplotlib.mlab as mlab
import sys
import time
import datetime
#import MySQLdb
import math
import sqlite3
import os


#conn = MySQLdb.connect (host = "localhost",
                        #user = "sane",
                        #passwd = "secret",
                        #db = "SANE")
dbfile = os.environ['InSANE_DB_PATH']+'/'+'SANE.db'
print dbfile
conn = sqlite3.connect(dbfile)
cursor = conn.cursor ()

cursor.execute (" SELECT series FROM `bigcal_calibrations` WHERE 1 ORDER BY id DESC LIMIT 1 ")
row = cursor.fetchone ()
seriesnumber = int(row[0]) + 1
print "Adding new calibration series :", seriesnumber

protCoef = []
rcsCoef = []
BigcalCoef = []


filename = sys.argv[1]
searchfile = open(filename, "r")
nextline = searchfile.readline() # keep it one line ahead of searchfile
Ncoef=0
for line in searchfile:
	noreturnline=line.rstrip(" ,\n")
	noreturnline=noreturnline.lstrip(" ")
	if noreturnline == "bigcal_rcs_gain_cor =":
		break
	else:
		values=noreturnline.split(" , ")
		for coef in values:
			protCoef.append(coef)
			BigcalCoef.append(coef)
#			print float(coef)
			Ncoef=Ncoef+1


for line in searchfile:
	noreturnline=line.rstrip(" ,\n")
	noreturnline=noreturnline.lstrip(" ")
	values=noreturnline.split(" , ")
#	print values
	for coef in values:
		rcsCoef.append(coef)
		BigcalCoef.append(coef)
#		print float(coef)
		Ncoef=Ncoef+1
		
print "number of coefficients is " + str(Ncoef)

sqlstart="UPDATE INTO bigcal_calibrations SET "

blocknumber=1

for cval in BigcalCoef:
	sqlcmd=sqlstart + "block_number=" + str(blocknumber) + ",coeff=" + cval + ",series=" + str(seriesnumber)
	cursor.execute (sqlcmd)

	blocknumber=blocknumber+1



#row = cursor.fetchone ()
#print "server version:", row[0]
cursor.close ()
conn.close ()