#!/usr/bin/env python

#import sqlite3
import os
import sys
from InSANEHallCExtractor import *

runnumber = sys.argv[1]

#print "Filling database for Run " + str(runnumber)

epics = InSANEEpicsExtractor(runnumber)
epics.InitializeQuantities()
epics.ExtractValues()
epics.CalculateAverageValues()
epics.FillDatabase()
del epics

epicsbeam = InSANEEpicsBeamExtractor(runnumber)
epicsbeam.InitializeQuantities()
epicsbeam.ExtractValues()
epicsbeam.CalculateAverageValues()
epicsbeam.FillDatabase()
del epicsbeam

epicshms = InSANEEpicsHMSExtractor(runnumber)
epicshms.InitializeQuantities()
epicshms.ExtractValues()
epicshms.CalculateAverageValues()
epicshms.FillDatabase()
del epicshms

epicstarget = InSANEEpicsTargetExtractor(runnumber)
epicstarget.InitializeQuantities()
epicstarget.ExtractValues()
epicstarget.CalculateAverageValues()
epicstarget.FillDatabase()
del epicstarget

#ROOT.gROOT.Macro( os.path.expanduser( '~/rootlogon.C' ) )
#aman = ROOT.SANEAnalysisManager.GetAnalysisManager()
#rman = ROOT.SANERunManager.GetRunManager()

#import rootlogon
#import hallctools
#import ROOT
#import numpy as n
#import ROOT, os
#rman.SetRun(72994)


