#! /bin/bash

function prompt_run_number {
runNumber=`zenity --entry --text "Please enter a run number " --entry-text "72994" --title "Run Number"`
if [ $runNumber == 0 ]
then
echo "oops $runNumber"
else
echo $runNumber
fi
#Xdialog --inputbox "Enter a run number:" 8 40 2>runNumber
}

function print_run_stats {
  if [ -z $1 ] 
  then
    runNumber=`prompt_run_number`
  else
    runNumber=$1
  fi


#declare -a fields
aCMD="select column_name from information_schema.columns where table_name='runs_on_disk';"
fields=(`mysql SANE -u sane -psecret -N -e"${aCMD}"`)

#declare -a rows
aCMD="Select * from runs_on_disk where run_number=${runNumber} "
rows=(`mysql SANE -u sane -psecret -N -e"${aCMD}"`)

numCols=${#fields[*]}
echo "$numCols"
echo "${#rows[*]}"

echo ${fields[*]}
echo ${rows[*]}

i=0
while [ "$i" -lt "$numCols" ]
do
let "i++"
echo "${fields[$i]} : ${rows[$i]}"
done

#for (( i = 0 ; i < $numCols  ; i++ ))
#do
#echo "${fields[$i]}  ${rows[$i]}"
#printf "%s : " ${fields[$i]}; printf "%s\n" ${rows[$i]}
#done

  if [ $isInteractive == 0 ]
  then
  if [ $rows == 0 ] 
  then
    echo " No run found in table: run_info "
    echo " Search other sources? " 
    Xdialog --title "Message"  --yesno "Are you having\ fun?" 6 25 2>ans
    if [ !$ans ] 
    then
      print_run_info_from_all_tables 

    fi
fi
fi
}

function print_help {
      echo " Usage:" >&2
      echo "  -i         Starts interactive" >&2
      echo "  -r #Run#   Sets run number" >&2
      echo "  -h         This help" >&2s
}

function print_run_info_from_all_tables {
aTable="runs_on_disk"
    echo "Info from table ${aTable}"
    aCMD="Select * from ${aTable} where run_number=${runNumber}"
    rows=`mysql SANE -u sane -psecret -N -e"${aCMD}"`
    echo $rows
    echo " "
aTable="exp_config_changes"
    echo "Info from table ${aTable}"
    aCMD="Select * from ${aTable} where start_run<=${runNumber} and end_run>=${runNumber}"
    rows=`mysql SANE -u sane -psecret -N -e"${aCMD}"`
    echo $rows
    echo " "
aTable="exp_configuration"
    echo "Info from table ${aTable}"
    aCMD="Select * from ${aTable} where start_run<=${runNumber} and end_run>=${runNumber}"
    rows=`mysql SANE -u sane -psecret -N -e"${aCMD}"`
    echo $rows
    
    }

