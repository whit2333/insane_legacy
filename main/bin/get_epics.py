#!/usr/bin/env python
#usage: get_epics_quantity [run_number]

import numpy as np
import pylab as plt
import matplotlib.mlab as mlab
import sys
import time
import datetime
#import MySQLdb
import math
import sqlite3
import os


#conn = MySQLdb.connect (host = "localhost",
                        #user = "sane",
                        #passwd = "secret",
                        #db = "SANE")
dbfile = os.environ['InSANE_DB_PATH']+'/'+'SANE.db'
print dbfile
conn = sqlite3.connect(dbfile)
#conn = MySQLdb.connect (host = "localhost",
                        #user = "sane",
                        #passwd = "secret",
                        #db = "SANE")
cursor = conn.cursor ()

class EDT(datetime.tzinfo):
    """Concrete imp of tzinfo for a datetime timezone

    EDT
    """
    def utcoffset(self,dt):
        return timedelta(hours=4,minutes=0) 
    def tzname(self,dt):
        return "EDT"
    def dst(self,dt):
        return timedelta(0)

def get_epics_datetime(epicsDateTimeLine):
    """ Converts the EPICS date and time to a datetime

    """
    epicsDatetimeFMT = '%a %b %d %H:%M:%S %Z %Y '
    dt = datetime.datetime.now()
    if epicsDateTimeLine == "":
        print "null timestamp provided!"
    else :
        dt = dt.strptime(epicsDateTimeLine,epicsDatetimeFMT)
        return(dt )


# tab at end is there to remove finding random printing of variables
# since the value is separetd by a tab
quantities = [" epics event # ", "epicsdatetime", "hcptPT_Encoder","hcptPolarization","ibcm1","ibcm2","HC:Bigcalth1",
             "HC:Bigcalth2","HC:Bigcalth3","HC:Bigcalth4","HC:SCerth1","HC:SCerth2","HALLC:p","EHCSR_XIPEAK","EHCSR_YIPEAK",
             "MBSY3C_energy"]
epicsdata =  [ [] for i in range(len(quantities)) ]

beamNames =  ["MBSY3C_energy","EHCSR_XIPEAK","EHCSR_YIPEAK","ibcm1","ibcm2",
              "EHCFR_INITSPPAT","EHCFR_SPWidth","EHCFR_SPIpeakraw.VAL",
              "IGL1I00DI24_24M","IGL1I00OD16_16",]
beamTitles = ["Beam Energy","Slow Raster X Magnet Current (RMS)","Slow Raster Y Magnet Current (RMS)","BCM 1","BCM 2",
              "Raster pattern (0=Lissajous, 1=Spiral?)","Fast Raster Spiral Radius","Fast Raster Magnet setting?",
              "1/2 Wave Plate retracted","1/2 Wave Plate Retracted Control"]
beamValues =  [ [] for i in range(len(beamNames)) ]

bpmNames = ["IPM3C01","IPM3C01.XPOS","IPM3C01.YPOS",
             "IPM3C02","IPM3C02.XPOS","IPM3C02.YPOS",
             "IPM3C03","IPM3C03.XPOS","IPM3C03.YPOS",
             "IPM3C04","IPM3C04.XPOS","IPM3C04.YPOS",
             "IPM3C05","IPM3C05.XPOS","IPM3C05.YPOS",
             "IPM3C06","IPM3C06.XPOS","IPM3C06.YPOS",
             "IPM3C07","IPM3C07.XPOS","IPM3C07.YPOS",
             "IPM3C08","IPM3C08.XPOS","IPM3C08.YPOS",
             "IPM3C09","IPM3C09.XPOS","IPM3C09.YPOS",
             "IPM3C10","IPM3C10.XPOS","IPM3C10.YPOS",
             "IPM3C11","IPM3C11.XPOS","IPM3C11.YPOS",
             "IPM3C12","IPM3C12.XPOS","IPM3C12.YPOS",
             "IPM3C13","IPM3C13.XPOS","IPM3C13.YPOS",
             "IPM3C14","IPM3C14.XPOS","IPM3C14.YPOS",
             "IPM3C15","IPM3C15.XPOS","IPM3C15.YPOS",
             "IPM3C16","IPM3C16.XPOS","IPM3C16.YPOS",
             "IPM3C17","IPM3C17.XPOS","IPM3C17.YPOS",
             "IPM3C18","IPM3C18.XPOS","IPM3C18.YPOS"
             "IPM3C19","IPM3C19.XPOS","IPM3C19.YPOS",
             "IPM3C20","IPM3C20.XPOS","IPM3C20.YPOS",
             "IPM3C03","IPM3C03.XPOS","IPM3C03.YPOS",
             "IPM3C04","IPM3C04.XPOS","IPM3C04.YPOS",
             "IPM3C05","IPM3C05.XPOS","IPM3C05.YPOS",
             "IPM3C06","IPM3C06.XPOS","IPM3C06.YPOS"]
bpmValues =  [ [] for i in range(len(bpmNames)) ]

secondsIntoRun = []

runnumber = sys.argv[1]
filename = "scalers/epics"+str(runnumber)+".txt"
results = []

searchfile = open(filename, "r")
searchfile2 = open(filename, "r")
nextline = searchfile2.readline() # keep it one line ahead of searchfile

for line in searchfile:  
    nextline = searchfile2.readline()
    for j, quantity in enumerate(quantities):
        if quantity in line:
            print line
            if j==0: 
                epicsdata[j].append( float(line.split()[3]))
                nextline.lstrip()
                print nextline
                eventtime = get_epics_datetime(nextline.lstrip())
                print eventtime
                epicsdata[1].append( eventtime)
                secondsIntoRun.append( (eventtime-epicsdata[1][0]).seconds )
		print str((eventtime-epicsdata[1][0]).seconds) + "seconds"
               # return the file position to its original location
            else: epicsdata[j].append( float(line.split()[1]))
    for j, quantity in enumerate(bpmNames):
        if quantity in line:
            bpmValues[j].append( float(line.split()[1]))
    for j, quantity in enumerate(beamNames):
        if quantity in line:
            beamValues[j].append( float(line.split()[1]))

searchfile.close()
searchfile2.close()

print "Length of lists"
for i in range(4):
    print len(epicsdata[i])

beamCurrentAvg = (sum(epicsdata[4]) + sum(epicsdata[5]))/(float(len(epicsdata[5])) + float(len(epicsdata[4])))
beamEnergy = epicsdata[15][0]
beamEnergyAvg = sum(epicsdata[15])/float(len(epicsdata[5]))
startdate = epicsdata[1][0].strftime('%Y-%m-%d')
starttime = epicsdata[1][0].strftime('%H:%M:%S')
enddate   = epicsdata[1][len(epicsdata[1])-1].strftime('%Y-%m-%d')
endtime   = epicsdata[1][len(epicsdata[1])-1].strftime('%H:%M:%S')


print "Average beam current is " + str(beamCurrentAvg)
print "Beam Energy is " + str(beamEnergy)
print "Average Beam Energy is " + str(beamEnergyAvg)
# Note MySQL datetime format is YYYY-MM-DD HH:MM:SS    
print "Start Date and time are " + startdate + " " + starttime
print "End Date and time are " + enddate + " " + endtime


columns = {"Run_number": runnumber,
           "start_date": "'"+startdate+"'" ,
	   "start_time": "'"+starttime+"'",
	   "end_date": "'"+enddate+"'",
	   "end_time": "'"+endtime+"'",
	   "beam_energy":beamEnergy,
	   "beam_current": beamCurrentAvg,
	   "halfwave_plate": int(beamValues[8][0]),
	   "target_field": epicsdata[3][0] }

sqlcmd="REPLACE INTO run_info SET "
#cursor.execute ("""
         #UPDATE animal SET name = %s
         #WHERE name = %s
       #""", ("snake", "turtle"))
   #print "Number of rows updated: %d" % cursor.rowcount

for n, v in columns.iteritems():
    sqlcmd = sqlcmd + n + "=" + str(v) +", "
sqlcmd = sqlcmd.strip(", ")
sqlcmd = sqlcmd + " ; "
print sqlcmd

cursor.execute (sqlcmd)
#row = cursor.fetchone ()
#print "server version:", row[0]
cursor.close ()
conn.close ()

#box = dict(facecolor='yellow', pad=5, alpha=0.2)
#fig = plt.figure()
#fig.subplots_adjust(left=0.2, wspace=0.6)
#labelx = -0.3  # axes coords

##ax1 = fig.add_subplot(221)
##ax1.set_title('Bigcal Thresholds')
##ax1.plot(secondsIntoRun,epicsdata[6],'bo',
         ##secondsIntoRun,epicsdata[7],'ro',
	 ##secondsIntoRun,epicsdata[8],'go',
	 ##secondsIntoRun,epicsdata[9],'mo')
#ax1 = fig.add_subplot(221)
#ax1.set_title('Raster magnet current')
#ax1.plot(epicsdata[0],epicsdata[13],'go')

#ax3 = fig.add_subplot(223)
#ax3.set_title('Raster magnet current')
#ax3.plot(epicsdata[0],epicsdata[14],'mo')

#ax2 = fig.add_subplot(222)
#ax2.set_title('bcm vs event#')
#ax2.plot(epicsdata[0],epicsdata[4],'bo',epicsdata[0],epicsdata[5],'ro')
#ax2.set_ylabel('bcm', bbox=box)
##ax2.yaxis.set_label_coords(labelx, 0.5)
##ax2.set_ylim(0, 2000)

#ax4 = fig.add_subplot(224)
#ax4.plot(epicsdata[0],secondsIntoRun,'bo')
#ax4.set_ylabel('seconds into run', bbox=box)
##ax4.yaxis.set_label_coords(labelx, 0.5)


#box = dict(facecolor='yellow', pad=5, alpha=0.2)
#fig2 = plt.figure()
#fig2.subplots_adjust(left=0.2, wspace=0.6)

#ax1 = fig2.add_subplot(221)
#ax1.set_title('BPM')
#ax1.plot(bpmValues[1],bpmValues[2],'g+',
         #bpmValues[4],bpmValues[5],'b+',
         #bpmValues[7],bpmValues[8],'r+',
         #bpmValues[10],bpmValues[11],'c+',
         #bpmValues[13],bpmValues[14],'y+',
         #bpmValues[16],bpmValues[17],'g+',
         #bpmValues[19],bpmValues[20],'b+',
         #bpmValues[22],bpmValues[23],'r+',
         #bpmValues[25],bpmValues[26],'c+',
         #bpmValues[28],bpmValues[29],'y+',
         #bpmValues[31],bpmValues[32],'g+',
         #bpmValues[34],bpmValues[35],'b+',
         #bpmValues[37],bpmValues[38],'r+',
         #bpmValues[40],bpmValues[41],'c+',
         #bpmValues[43],bpmValues[44],'y+')
#ax6 = fig2.add_subplot(222)
#ax6.set_title('BPM')
#ax6.plot(bpmValues[0],'g+',
         #bpmValues[3],'b+',
         #bpmValues[6],'r+',
         #bpmValues[9],'c+',
         #bpmValues[12],'y+',
         #bpmValues[15],'g+',
         #bpmValues[18],'b+',
         #bpmValues[21],'r+',
         #bpmValues[24],'c+',
         #bpmValues[27],'y+',
         #bpmValues[30],'g+',
	 #bpmValues[33],'b+',
	 #bpmValues[36],'r+',
	 #bpmValues[39],'c+',
	 #bpmValues[42],'y+')
#plt.show()


#plt.plot(epicsdata[3],epicsdata[4][0:len(epicsdata[3])],'ro')
#plt.plot()
#plt.show()
#plt.plot(epicsdata[0],epicsdata[4],'bo',epicsdata[0],epicsdata[3],'ro')
#plt.show()

## epics.dictionary
## This file will contain a list of all EPICS variables (except
## for High Voltage which are to be recorded in coda.
## The format 5 blankspace separated columns, namely:
##
##	Epics signal name
##	Comma separated list of run types for which signal is to be read
##		all = all runtypes except moller
##               allm = all runtypes including moller
##               sosonly,coin = Sos or Coincidence
##	How often signal is to be read (seconds).  Several comma separated periods may
##		be listed so that a signal gets recorded at several rates.
##	The expert for this signal.  (Either who put it in this file, or
##		who bests understands what it is doing here.)
##	The title, meaning or comment of the signal
##
## All fields except for the Comment must have no blank space in them.
##
## contributor codes:
##       saw    = Stephen Wood
##	danagu = Samuel Danagoulian 
##	maclach = Glen MacLaclan
##	smithg = Greg Smith
##	gueye = Paul Gueye
##       miyoshi = Toshinobu Miyoshi
##       mizuki = Mizuki Sumihama
##Signal        Runtype  Period  Person  Comment
##What are these?
#IBC0L02Current	allm	30	maclach
##IBC0R07AVG	allm	30	maclach


## Don't mess with below.  It is just to keep emacs happy.
## Local Variables:
## mode: text
## fill-column: 120
## End:

