#include "util/hist_to_graph.cxx"
#include "util/syst_error_band.cxx"

void A2_vs_W(int aNumber = 0) {

  int sf_num  = 1; 
  int ssf_num = 0;
  InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  auto sfs    = fman->CreateSFs(sf_num);
  auto ssfs   = fman->CreatePolSFs(ssf_num);

  //auto sfs   = new F1F209StructureFunctions();
  auto vca = new InSANEVirtualComptonAsymmetries(sfs,ssfs);

  int ana_number = 8409;

  TFile * fA59 = new TFile(Form("data/A1A2_59_%d.root",ana_number),"READ");
  TList * fA2_list = (TList*)gROOT->FindObject("A2_59_W");
  if(!fA2_list){ std::cout << "-14" << std::endl; return -14;}

  TFile * fA47 = new TFile(Form("data/A1A2_47_%d.root",ana_number),"READ");
  TList * fA2_47_list = (TList*)gROOT->FindObject("A2_47_W");
  if(!fA2_47_list){ std::cout << "-14" << std::endl; return -14;}

  TMultiGraph * mg_sane_data      = new TMultiGraph();
  TMultiGraph * mg_sane_data_syst = new TMultiGraph();

  for(int jj = 0;jj<4;jj++) {

    InSANEMeasuredAsymmetry * fA2_59_asym = (InSANEMeasuredAsymmetry * )(fA2_list->At(jj));
    InSANEMeasuredAsymmetry * fA2_47_asym = (InSANEMeasuredAsymmetry * )(fA2_47_list->At(jj));
    TH1F * fA2_59   = &fA2_59_asym->fAsymmetryVsW;
    TH1F * fA2_47   = &fA2_47_asym->fAsymmetryVsW;

    TGraphErrors * gr = hist_to_graph(fA2_59);
    mg_sane_data->Add( gr , "ep");

    gr = syst_error_band(&fA2_59_asym->fSystErrVsW, -1.0,0.3);
    mg_sane_data_syst->Add( gr , "e3");

    gr = hist_to_graph(fA2_47);
    mg_sane_data->Add( gr , "ep");

    gr = syst_error_band(&fA2_47_asym->fSystErrVsW, -1.0,0.3);
    mg_sane_data_syst->Add( gr , "e3");

  }

    std::vector<int> colors = {1,2,4,6,8,7};

  double W_min = 1.00;
  double W_max = 3.0;
  int    N     = 80;
  double dW    = (W_max-W_min)/double(N);

  double Q2_min = 2.0;
  double Q2_max = 3.0;
  int    NQ2    = 6;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2-1);

  std::vector<TGraph*> grs_A2;
  std::vector<TGraph*> grs_A2_TMC;
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    grs_A2.push_back(  new TGraph() );
    grs_A2_TMC.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    for (int iW = 0; iW < N; iW++) {

      double W = W_min + double(iW)*dW;
      double x = InSANE::Kine::xBjorken_WQsq(W,Q2);

      double A2      = vca->A2p(x, Q2);
      grs_A2[iQ2]->SetPoint( iW, W, A2);

      double A2_TMC      = vca->A2p_TMC(x, Q2);
      grs_A2_TMC[iQ2]->SetPoint( iW, W, A2_TMC);
    }
  }

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.8,0.6,0.975,0.975);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_A2[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetMarkerStyle(20);
    grs_A2[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetLineWidth(2);
    grs_A2[iQ2]->SetLineStyle(1+(iQ2/(colors.size())) );

    grs_A2_TMC[iQ2]->SetLineColor(colors[(iQ2)%(colors.size())]);
    grs_A2_TMC[iQ2]->SetMarkerStyle(20);
    grs_A2_TMC[iQ2]->SetMarkerColor(colors[(iQ2)%(colors.size())]);
    grs_A2_TMC[iQ2]->SetLineWidth(2);
    grs_A2_TMC[iQ2]->SetLineStyle(2+(iQ2/(colors.size())) );
    
    mg->Add(      grs_A2[iQ2],"l");
    leg->AddEntry(grs_A2[iQ2], Form("Q2=%.1f",Q2), "l");
    mg->Add(      grs_A2_TMC[iQ2],"l");
    leg->AddEntry(grs_A2_TMC[iQ2], Form("TMC Q2=%.1f",Q2), "l");

  }

  mg->Add(mg_sane_data);
  mg->Add(mg_sane_data_syst);
  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(-1.0,1.0);
  mg->GetXaxis()->SetLimits(W_min,W_max);
  mg->GetYaxis()->SetTitle("A_{2}");
  mg->GetXaxis()->SetTitle("W");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  leg->Draw();

  TLatex lt;
  lt.SetTextAlign(12);
  lt.SetTextSize(0.04);
  lt.DrawLatex(1.2,0.9,Form("%s / %s",sfs->GetLabel(), ssfs->GetLabel()));

  c->SaveAs(Form("results/sane_plots/A2_vs_W_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/sane_plots/A2_vs_W_%d_%d.pdf",sf_num,ssf_num));


}
