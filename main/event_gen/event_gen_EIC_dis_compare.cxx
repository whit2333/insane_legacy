Int_t event_gen_EIC_dis_compare(){
   
  InSANEFunctionManager::GetManager()->CreateSFs(2);
  Double_t fBeamEnergy = 6.0;
  Double_t Emin = 2.9;
  Double_t Emax = 4.2;

  double k0 = 10.0; // GeV
  double M0 = 0.938; // GeV
  double P0 = 60.0; // GeV/n
  double P  = TMath::Sqrt(P*P + M0*M0); // GeV/u
  double s  = M0*M0 + 2.0*k0*(P0+P);
  double k0_Rest = (s-M0*M0)/(2.0*M0);
  std::cout << " P       = " << P << std::endl;
  std::cout << " s       = " << s << std::endl;
  std::cout << " k0_Rest = " << k0_Rest << std::endl;

   // Event Generator
   Double_t       LH2_density   = 0.07085; //g/cm^3
   Double_t       target_length = 5.0;
   InSANETarget * target        = new InSANETarget("LH2 Target","LH2 Target for CLAS12");
   {
      InSANETargetMaterial * matLH2 = new InSANETargetMaterial("LH2","LH2",1,1);
      matLH2->fLength          = target_length;    //cm
      matLH2->fDensity         = LH2_density; // g/cm3
      matLH2->fZposition       = 0.0;     //cm
      target->AddMaterial(matLH2);
   }
   InSANETargetEventGenerator * eventGen = new InSANETargetEventGenerator("LH2 event generator");
   eventGen->SetTarget(target);
   //InSANEEventGenerator * eventGen = new InSANEEventGenerator("DIS-event-gen","DIS Event Generator");

   // Cross-section
   //InSANECompositeDiffXSec * disXSec = new InSANECompositeDiffXSec();
   InSANEInclusiveBornDISXSec * disXSec = new InSANEInclusiveBornDISXSec();
   disXSec->SetTargetNucleus( InSANENucleus(1,1) );
   //F1F209eInclusiveDiffXSec * disXSec = new F1F209eInclusiveDiffXSec();
   disXSec->SetBeamEnergy(fBeamEnergy);
   disXSec->InitializePhaseSpaceVariables();
   disXSec->InitializeFinalStateParticles();

   // Phase-space Sampler
   InSANEPhaseSpaceSampler * disPSSampler = new InSANEPhaseSpaceSampler(disXSec);
   disPSSampler->SetFoamCells(1000);
   disPSSampler->SetFoamSample(1000);
   disPSSampler->SetFoamBins(36);
   disPSSampler->SetFoamChat(1);

   // Add the event generator.
   eventGen->AddSampler(disPSSampler);
   eventGen->Initialize();

   double theta = 20.0*CLHEP::degree;
   double phi = 0.0*CLHEP::degree;
   double delta_theta = 0.1*CLHEP::degree;
   double delta_phi = 1.0*CLHEP::degree;
   int    nbins = 20;
   double dE = (Emax-Emin)/double(nbins);
   double theta1 = theta-delta_theta;
   double theta2 =  theta+delta_theta;

   TGraph * gr = new TGraph(20);
   TGraph * grN = new TGraph(20);
   //for(int i = 0; i<nbins; i++) {

   //   double p0       = Emin+dE*(0.5+double(i));
   //   double delta_p  = dE*0.5;
   //   double dEdOmgea =  2.0*dE*delta_phi*(TMath::Cos(theta1)- TMath::Cos(theta2));
   //   disXSec->GetPhaseSpace()->GetVariableWithName("energy")->SetRange( p0-delta_p, p0+delta_p );
   //   disXSec->GetPhaseSpace()->GetVariableWithName("theta")->SetRange( theta1,  theta2  );
   //   disXSec->GetPhaseSpace()->GetVariableWithName("phi")->SetRange( phi-delta_phi, phi+delta_phi );
   //   eventGen->Refresh();
   //   double sigma = eventGen->CalculateTotalCrossSection()/dEdOmgea;
   //   gr->SetPoint(i,p0, sigma);

   //}

   double p0       = (Emin+Emax)/2.0;
   double delta_p  = (Emax-Emin)/2.0;
   double dEdOmgea = 4.0*delta_p*delta_phi*(TMath::Cos(theta1)- TMath::Cos(theta2));
   disXSec->GetPhaseSpace()->GetVariableWithName("energy")->SetRange( p0-delta_p, p0+delta_p );
   disXSec->GetPhaseSpace()->GetVariableWithName("theta")->SetRange( theta-delta_theta, theta+delta_theta );
   disXSec->GetPhaseSpace()->GetVariableWithName("phi")->SetRange( phi-delta_phi, phi+delta_phi );
   eventGen->Refresh();
   double sigmaTot = eventGen->CalculateTotalCrossSection()/dEdOmgea;

   int nbins2 = 500;
   TH1F * h1 = new TH1F("Ncount","Ncount",nbins2,Emin,Emax);
   int nevents = 1000000;
   for(int iev = 0; iev<nevents; iev++){
      eventGen->GenerateEvent();
      InSANEParticle * part = eventGen->fEG_Event.GetParticle(0);
      if(part) {
         h1->Fill(part->Energy());
         //part->Print();
      }
   }
   //h1->Scale(1.0/(double(nevents)));

   double time_simulated = eventGen->GetSimulatedTime(nevents, 100.0e-9);
   InSANELuminosity lumi( eventGen->GetTarget(),100.0e-9);
   double lum            = lumi.CalculateLuminosity();

   h1->Scale(1.0e33/(lum*time_simulated));
   h1->Scale(1.0/(dEdOmgea));
   h1->Scale(double(nbins2));
   //h1->Scale(dE/(delta_p));

   eventGen->PrintXSecSummary();
   std::cout << " Beam Energy    : " << fBeamEnergy    << " GeV" << std::endl;
   std::cout << " Nevents        : " << nevents        << std::endl;
   std::cout << " Simulated time : " << time_simulated << " s" << std::endl;
   std::cout << " Luminosity     : " << lum << " " << std::endl;
   lumi.Print();

   //eventGen->Print(log_file);
   //eventGen->GetTarget()->Print(log_file);
   //eventGen->PrintXSecSummary(log_file);
   //log_file << " Beam Energy    : " << fBeamEnergy    << " GeV" << std::endl;
   //log_file << " Nevents        : " << Nevents        << std::endl;
   //log_file << " Simulated time : " << time_simulated << " s" << std::endl;
   //log_file << " Occup. Norm    : " << occupancy_normalization << " events" << std::endl;
   //lumi.Print(log_file);

   //TFile * f = new TFile("event_gen/test.root","RECREATE"); 
   //TParticle * part = new TParticle();
   //TTree * t = new TTree("EventGenTest","Event generator DIS Test");
   //t->Branch("partThrown","TParticle",&part);

   //eventGen->Print();
   //for(int i = 0;i<2000;i++){
   //   TList * parts = eventGen->GenerateEvent();
   //   if(parts->GetEntries() > 0){
   //      part = (TParticle*) parts->At(0);
   //      t->Fill();
   //   }
   //   //parts->Print();
   //}

   TLegend*      leg = new TLegend(0.55,0.7,0.85,0.9);
   TCanvas*      c   = new TCanvas();
   TMultiGraph*  mg  = new TMultiGraph();
   TGraph*       gr0 = nullptr;
   TGraphErrors* egr= nullptr;

   c->cd(0);


   h1->SetLineColor(4);
   mg->Add(egr = new TGraphErrors(h1),"ep");
   leg->AddEntry(egr, "MC normalized","ep");

   gr->SetMarkerStyle(20);
   //mg->Add(gr,"p");
   leg->AddEntry(gr,"Integrated differential XS","p");

   //InSANEInclusiveBornDISXSec * xsec0 = new InSANEInclusiveBornDISXSec();
   //xsec0->InitializePhaseSpaceVariables();
   //xsec0->InitializeFinalStateParticles();
   //plot_xsec->UsePhaseSpace(false);
   //disXSec->UsePhaseSpace(false);
   disXSec->SetIncludeJacobian(false);
   TF1 * sigma0 = new TF1("sigma0", disXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             Emin, Emax , 2,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0->SetParameter(0,theta);
   sigma0->SetParameter(1,phi);
   sigma0->SetLineColor(1);
   sigma0->SetNpx(200);
   auto copy0 = new TGraph(sigma0);
   mg->Add(copy0, "l");
   leg->AddEntry(copy0,"central XS ","l");

   sigma0->SetParameter(0,theta-delta_theta);
   sigma0->SetParameter(1,phi);
   sigma0->SetLineColor(2);
   sigma0->SetNpx(200);
   auto copy1 = new TGraph(sigma0);
   mg->Add(copy1, "l");
   leg->AddEntry(copy1,"#theta = 19.9^{#circ}","l");

   sigma0->SetParameter(0,theta+delta_theta);
   sigma0->SetParameter(1,phi);
   sigma0->SetLineColor(4);
   sigma0->SetNpx(200);
   auto copy2 = new TGraph(sigma0);
   mg->Add(copy2, "l");
   leg->AddEntry(copy2,"#theta = 20.1^{#circ}","l");
   //TH1 * hf = sigma0->GetHistogram();
   //hf->Scale(1.0/TMath::Sin(theta));
   //hf->Draw();
   //c->Update();

   //TF1 * f1 = new TF1("f", plot_xsec, &InSANEInclusiveDiffXSec::EnergyDependentXSec,1.0,3.9,2,"InSANEInclusiveDiffXSec","EnergyDependentXSec");   // create TF1 class.
   //f1->SetParameter(0,theta);
   //f1->SetParameter(1,phi);
   ////t->StartViewer();

   mg->Draw("a");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetXaxis()->SetTitle("E^{'} [GeV]");
   mg->GetYaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("#frac{d^{2}#sigma}{dE'd#Omega} [nb/GeV/sr]");
   mg->Draw("a");
   leg->Draw();
   TLatex lt;
   lt.DrawLatex(2.9,6.0,"E_{0} = 6 GeV");
   lt.DrawLatex(2.9,4.0,"#theta = 20^{#circ}");
   lt.DrawLatex(2.9,2.0,Form("#DeltaE'#Delta#Omega = %.2f MeV-msr",dEdOmgea*1000*1000));


   c->SaveAs("results/event_gen/event_gen_EIC_dis_compare.pdf");


   return 0;
}
