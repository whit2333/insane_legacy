// Define some simple structures
struct DIS_KINE{
  Float_t s,x,Q2,y,W;
};

int event_gen_dis_EIC_jose(int Nevents = 1e6)
{
  InSANEFunctionManager::GetManager()->CreateSFs(2);
  using namespace insane::physics;
  using namespace insane::physics::eic;
  double k0      = 10.0; // GeV
  double M0      = M_p/GeV; // GeV
  double P0      = 60.0 ;// GeV/u
  double P       = TMath::Sqrt(P0*P0 - M0*M0); // GeV/u
  double s       = M0*M0 + 2.0*k0*(P0+P);
  double k0_Rest = (s-M0*M0)/(2.0*M0);

  std::cout << " P       = " << P << std::endl;
  std::cout << " s       = " << s << std::endl;
  std::cout << " k0_Rest = " << k0_Rest << std::endl;

  InSANENucleus target_nucleus(1,1);
  //InSANENucleus recoil_nucleus = target_nucleus - InSANENucleus::Proton();
  //std::cout << " Recoiil Nucleus :\n";
  std::cout << " -----------------\n";
  target_nucleus.Print();
  std::cout << " -----------------\n";

  //InSANETarget*         target = new InSANETarget("deuterium gas target","deuterium gas target for CLAS12");
  //InSANETargetMaterial* matD   = new InSANETargetMaterial("D","D",1,2);
  double target_length = 0.1;
  double a_noUnit     = 2.003;
  double pre_noUnit   = 3.0;
  double tempe_noUnit = 298.;
  double pressure     = 3.0*atmosphere;
  double temperature  = 298.*kelvin;
  double D_density  = (a_noUnit*pre_noUnit)/(0.0821*tempe_noUnit)*kg/m3; //0.164*kg/m3 at 1 atm;

  //matD->fLength          = target_length;    //cm
  //matD->fDensity         = D_density/(g/cm3); // g/cm3
  //matD->fZposition       = 0.0;     //cm
  //std::cout << " DENSITY = " << matD->fDensity << std::endl;
  //target->AddMaterial(matD);

  // -----------------------------------------------------------------
  // Event Generator
  InSANEEventGenerator * eventGen = new InSANEEventGenerator("DIS-event-gen","DIS Event Generator");
  //eventGen->SetTarget(target);
  //eventGen->fSlowRasterSize = 0.0;
  //eventGen->SetBeamEnergy(k0_Rest);

  // Cross-section
  //EICIncoherentDISXSec * disXSec = new EICIncoherentDISXSec();
  //InSANECompositeDiffXSec * disXSec = new InSANECompositeDiffXSec();
  EICInclusiveBornDISXSec * disXSec = new EICInclusiveBornDISXSec();
  disXSec->SetBeamEnergy(k0);
  disXSec->SetIonBeamEnergy(P0);
  disXSec->SetTargetNucleus( target_nucleus );
  //disXSec->SetRecoilNucleus( recoil_nucleus  );
  disXSec->InitializePhaseSpaceVariables();
  disXSec->InitializeFinalStateParticles();
  disXSec->GetPhaseSpace()->GetVariable(0)->SetMinimum(0.5);
  disXSec->GetPhaseSpace()->GetVariable(0)->SetMaximum(k0_Rest);
  disXSec->GetPhaseSpace()->GetVariable(1)->SetMinimum(5.0*degree);
  disXSec->GetPhaseSpace()->GetVariable(1)->SetMaximum(180.0*degree);
  disXSec->GetPhaseSpace()->GetVariable(2)->SetMinimum(-180.0*degree);
  disXSec->GetPhaseSpace()->GetVariable(2)->SetMaximum(180.0*degree);
  //return 0;

  // Phase-space Sampler
  InSANEPhaseSpaceSampler * disPSSampler = new InSANEPhaseSpaceSampler(disXSec);
  disPSSampler->SetFoamCells(3000);
  disPSSampler->SetFoamSample(1000);
  disPSSampler->SetFoamBins(300);
  disPSSampler->SetFoamChat(1);

  // Add the event generator.
  eventGen->AddSampler(disPSSampler);

  eventGen->Initialize();

  double sigma_tot = eventGen->CalculateTotalCrossSection()*1.0e-9*1.0e-24;
  double N_tot     = double(Nevents);
  double L_Delta_t = N_tot/sigma_tot;
  double t_year    = 1.0e7;
  double L0        = 1.0e33;
  double Y_i       = 100.0;
  double sigma_i   = sigma_tot*Y_i/N_tot;
  double R_i       = sigma_i*L0*t_year;

  eventGen->Print();

  std::cout << " t_year    = " << t_year    << " [s]"   << std::endl;
  std::cout << " sigma_tot = " << sigma_tot << " [cm^2]"   << std::endl;
  std::cout << " N_tot     = " << N_tot     << "  "   << std::endl;
  std::cout << " L_Delta_t = " << L_Delta_t << " [1/cm^2]"   << std::endl;
  std::cout << " L0        = " << L0        << " [1/cm^2-s]"   << std::endl;
  std::cout << " Y_i       = " << Y_i       << " "   << std::endl;
  std::cout << " sigma_i   = " << sigma_i   << " [cm^2]"   << std::endl;
  std::cout << " R_i       = " << R_i       << " [per year]" << std::endl;

  TFile*           f    = new TFile("results/event_gen/event_gen_dis_EIC_jose.root","RECREATE"); 
  InSANEParticle*  part = new InSANEParticle();
  DIS_KINE*        kine = new DIS_KINE();
  TTree*           t    = new TTree("EventGenTest","Event generator DIS Test");
  t->Branch("partThrown","InSANEParticle",&part);
  t->Branch("disKine","DIS_KINE",&kine);

  std::vector<double> log_x_bins;
  double x_min    = 0.0001;
  double n_x_bins = 100;
  double delta_x_0  =  TMath::Log10(x_min);
  double delta_x    = -delta_x_0/double(n_x_bins);
  for(int i=0;i<=n_x_bins;i++){
    log_x_bins.push_back(TMath::Power(10.0,delta_x_0 + double(i)*delta_x));
  }
  std::vector<double> log_Q2_bins;
  double Q2_min    = 0.5;
  double Q2_max    = 100.0;
  double n_Q2_bins = 100;
  double delta_Q2_min  =  TMath::Log10(Q2_min);
  double delta_Q2_max  =  TMath::Log10(Q2_max);
  double delta_Q2    = (delta_Q2_max-delta_Q2_min)/double(n_Q2_bins);
  for(int i=0;i<=n_Q2_bins;i++){
    log_Q2_bins.push_back(TMath::Power(10.0,delta_Q2_min + double(i)*delta_Q2));
  }

  //TH1F * h1 = new TH1F("hEnergy1","", n_x_bins, log_x_bins.data());
  //TH1F * h2 = new TH1F("hEnergy2","",100,0,100);
  //TH1F * h3 = new TH1F("hEnergy3","",100,0,100);
  //TH2F * hQ2Vsx  = new TH2F("hQ2Vsx","",n_x_bins, log_x_bins.data(), n_Q2_bins, log_Q2_bins.data());
  //TH2F * hQ2Vsx2 = new TH2F("hQ2Vsx2","",20, 0.0001, 1, 20 , 0,20.0);
  //TH1F * hx  = new TH1F("hx","", n_x_bins, log_x_bins.data());
  //TH1F * hQ2 = new TH1F("hQ2","",n_Q2_bins, log_Q2_bins.data());
  //TH1F * hx2 = new TH1F("hx2","",20,0,1);

  TLorentzVector k1_4 = {0,0, k0,k0};
  TLorentzVector P0_4 = {0,0, -P,P0};
  TLorentzVector k2_4 = {0,0, k0,k0};


  for(int i = 0;i<Nevents;i++){

    if( i%1000000 == 0 ) {
      std::cout << " Event " << i << std::endl;
    }
    TList * parts = eventGen->GenerateEvent();
    auto vars = disXSec->DependentVariables();

    if(parts->GetEntries() > 0){

      part = (InSANEParticle*) parts->At(0);
      //h1->Fill(part->Energy());
      part->Momentum(k2_4);
      //part->Print();

      TLorentzVector q  = k1_4 - k2_4;
      double         Q2 = -1.0*q*q;
      double         x  = Q2/(2.0*(P0_4*q));
      //std::cout << " x = " << x << "\n";
      //std::cout << " Q2= " << Q2 << "\n";
      kine->s  = s;
      kine->x  = x;
      kine->Q2 = Q2;
      kine->y  = (P0_4*q)/(P0_4*k1_4);
      kine->W  = TMath::Sqrt( (P0_4+q)*(P0_4+q) );

      //std::cout << vars[6] << std::endl;

      t->Fill();
    }
    //parts->Print();
  }

  f->Write();
  f->Close();

  //TCanvas * c = new TCanvas();
  //c->Divide(2,3);
  //c->cd(1);
  //gPad->SetLogy(true);
  //h1->SetLineColor(2);
  //h1->Draw();
  ////h2->Draw();
  //c->cd(2);
  //gPad->SetLogy(true);
  //gPad->SetLogx(true);
  //hQ2->Draw();
  //hQ2->GetYaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");

  //c->cd(3);
  //gPad->SetLogy(true);
  //hx->Draw();
  //hx2->SetLineColor(2);
  //hx2->Draw("same");

  //c->cd(4);
  //gPad->SetLogx(true);
  //gPad->SetLogy(true);
  //hQ2Vsx->Draw("colz");

  //c->cd(5);
  //gPad->SetLogy(true);
  //hx2->SetLineColor(2);
  //hx2->Draw();

  //c->cd(6);
  //gPad->SetLogz(true);
  //hQ2Vsx2->Draw("colz");

  //c = new TCanvas();
  //gPad->SetRightMargin(0.2);
  //
  //double RateFactor       = L0*t_year*(sigma_tot/N_tot);
  //gPad->SetLogz(true);
  //hQ2Vsx2->Scale(RateFactor);
  //hQ2Vsx2->Draw("colz");
  //hQ2Vsx2->GetXaxis()->CenterTitle(true);
  //hQ2Vsx2->GetYaxis()->CenterTitle(true);
  //hQ2Vsx2->GetXaxis()->SetTitle("x");
  //hQ2Vsx2->GetYaxis()->SetTitle("Q^{2} [Gev^{2}/c^{2}]");
  //hQ2Vsx2->GetZaxis()->SetTitle("Events per year");
  //c->SaveAs("results/event_gen/event_gen_dis_EIC.pdf");
  ////TF1 * f1 = new TF1("f",disXSec,&InSANEInclusiveDiffXSec::EnergyDependentXSec,1.0,5.9,2,"InSANEInclusiveDiffXSec","EnergyDependentXSec");   // create TF1 class.
  ////f1->SetParameter(0,20*degree);
  ////f1->SetParameter(1,0);
  ////f1->Draw();
  ////t->StartViewer();
  return(0);
}
