Int_t oscar_pi0(){
   Int_t NEvents = 1e6;

   Double_t fBeamEnergy = 5.90; //GeV
   InSANEPhaseSpaceVariable * psvar = 0;

   // First create the pi0 cross section and phase-space sampler.
   OARPionElectroDiffXSec * pi0XSec = new OARPionElectroDiffXSec();
   pi0XSec->SetBeamEnergy(fBeamEnergy);
   //pi0XSec->SetProductionParticleType(111);
   pi0XSec->InitializePhaseSpaceVariables();
   psvar = pi0XSec->GetPhaseSpace()->GetVariableWithName("energy");
   if(psvar){
      psvar->SetMinimum(0.15);
      psvar->SetMaximum(2.0);
   }
   psvar = pi0XSec->GetPhaseSpace()->GetVariableWithName("theta");
   if(psvar){
      psvar->SetMinimum(0.5*degree);
      psvar->SetMaximum(90.0*degree);
   }
   psvar = pi0XSec->GetPhaseSpace()->GetVariableWithName("phi");
   if(psvar){
      psvar->SetMinimum(-180.0*degree);
      psvar->SetMaximum(180.0*degree);
   }
   pi0XSec->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler *  pi0PSSampler = new InSANEPhaseSpaceSampler(pi0XSec);

   pi0XSec->Print();

   InSANEEventGenerator * eventGen = new InSANEEventGenerator("pi0EventGen","wiser pi0 event generator");
   eventGen->AddSampler(pi0PSSampler);
   eventGen->Print();
   eventGen->Initialize();
   //eventGen->CalculateTotalCrossSection();
   eventGen->Print();
   //eventGen->Print();

   TFile * egFile = new TFile("results/event_gen/oscar_pi0.root","UPDATE");

   TH1F * hEnergy = new TH1F("hEnergy"           , "Energy;E_{#pi} (GeV)"         , 100 , 0.0    , 2);
   TH1F * hTheta  = new TH1F("hTheta"            , "#theta_{#pi};#theta_{#pi} (deg)"         , 100 , 0    , 90);
   TH1F * hPhi    = new TH1F("hPhi"              , "#phi_{#pi};#phi_{#pi} (deg)"           , 100 , 0 , 360);
   TH2F * hEnergyTheta = new TH2F("hEnergyTheta" , ";E_{#pi} (GeV);#theta_{#pi} (deg)" , 100 , 0.0    , 2     , 100 , 0 , 90);
   TH1F * hMomentum = new TH1F("hMomentum"           , "Momentum;P_{#pi} (GeV/c)"         , 100 , 0.0    , 2);
   TH2F * hMomentumTheta = new TH2F("hMomentumTheta" , ";P_{#pi} (GeV/c);#theta_{#pi} (deg)" , 100 , 0.0    , 2     , 100 , 0 , 90);

   Int_t       nElectrons = 0;
   Int_t       nPi0s      = 0;
   TParticle * p          = 0;

   for(int i = 0; i<NEvents; i++){
      TList * parts = eventGen->GenerateEvent();
      //parts->Print();
      p = (TParticle*)parts->At(0);
      if(p) {
         hEnergy->Fill(p->Energy());
         hTheta->Fill(p->Theta()/degree);
         hPhi->Fill(p->Phi()/degree);
         hEnergyTheta->Fill(p->Energy(),p->Theta()/degree);
         hMomentum->Fill(TMath::Sqrt(p->Energy()*p->Energy()-M_pi0*M_pi0/(GeV*GeV)));
         hMomentumTheta->Fill(TMath::Sqrt(p->Energy()*p->Energy()-M_pi0*M_pi0/(GeV*GeV)),p->Theta()/degree);
         if(p->GetPdgCode() == 11) nElectrons++;
         if(p->GetPdgCode() == 111) nPi0s++;
      }

   }

   TCanvas * c = new TCanvas();
   c->Divide(2,2);

   c->cd(1);
   hMomentum->Draw();

   c->cd(2);
   hTheta->Draw();

   c->cd(3);
   hMomentumTheta->Draw("colz");

   c->cd(4);
   hPhi->Draw();

   c->SaveAs("results/event_gen/oscar_pi0_2.png");

   ofstream ofile("results/event_gen/oscar_pi0.txt");
   eventGen->Print(ofile);

   hEnergy       ->SetDirectory(egFile);
   hTheta        ->SetDirectory(egFile);
   hPhi          ->SetDirectory(egFile);
   hEnergyTheta  ->SetDirectory(egFile);
   hMomentum     ->SetDirectory(egFile);
   hMomentumTheta->SetDirectory(egFile);

   egFile->Write();

   return(0);
}
