Int_t event_gen_lund_format(){

   Int_t Nevents = 1000;
   
   Double_t fBeamEnergy = 3.0;

   // Event Generator
   InSANEEventGenerator * eventGen = new InSANEEventGenerator("DIS-event-gen","DIS Event Generator");

   // Cross-section

   //F1F209eInclusiveDiffXSec * disXSec = new F1F209eInclusiveDiffXSec();
   InSANEInclusiveBornDISXSec * disXSec = new InSANEInclusiveBornDISXSec();
   disXSec->SetBeamEnergy(fBeamEnergy);
   disXSec->SetTargetNucleus(InSANENucleus::Proton());
   disXSec->InitializePhaseSpaceVariables();
   disXSec->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler * disPSSampler = new InSANEPhaseSpaceSampler(disXSec);
   eventGen->AddSampler(disPSSampler);

   // ep elastic (exclusive)
   InSANEepElasticDiffXSec * elasticXSec = new InSANEepElasticDiffXSec();
   elasticXSec->SetBeamEnergy(fBeamEnergy);
   elasticXSec->SetTargetNucleus(InSANENucleus::Proton());
   elasticXSec->InitializePhaseSpaceVariables();
   elasticXSec->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler * elasticPSSampler = new InSANEPhaseSpaceSampler(elasticXSec);
   eventGen->AddSampler(elasticPSSampler);

   InclusiveElectroProductionXSec * pion_electro_xsec = new InclusiveElectroProductionXSec();
   pion_electro_xsec->SetBeamEnergy(fBeamEnergy);
   pion_electro_xsec->InitializePhaseSpaceVariables();
   pion_electro_xsec->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler *  pionElectroPSSampler = new InSANEPhaseSpaceSampler(pion_electro_xsec);
   eventGen->AddSampler(pionElectroPSSampler);

   InclusiveElectroProductionXSec * pion_electro_xsec1 = new InclusiveElectroProductionXSec();
   pion_electro_xsec1->SetBeamEnergy(fBeamEnergy);
   pion_electro_xsec1->SetProductionParticleType(211);
   pion_electro_xsec1->InitializePhaseSpaceVariables();
   pion_electro_xsec1->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler *  pionElectroPSSampler1 = new InSANEPhaseSpaceSampler(pion_electro_xsec1);
   eventGen->AddSampler(pionElectroPSSampler1);

   InclusiveElectroProductionXSec * pion_electro_xsec2 = new InclusiveElectroProductionXSec();
   pion_electro_xsec2->SetBeamEnergy(fBeamEnergy);
   pion_electro_xsec2->SetProductionParticleType(-211);
   pion_electro_xsec2->InitializePhaseSpaceVariables();
   pion_electro_xsec2->InitializeFinalStateParticles();
   InSANEPhaseSpaceSampler *  pionElectroPSSampler2 = new InSANEPhaseSpaceSampler(pion_electro_xsec2);
   eventGen->AddSampler(pionElectroPSSampler2);

   //elasticXSec->Refresh();
   //elasticXSec->EvaluateCurrent();
   //elasticXSec->GetPhaseSpace()->Print();
   //elasticXSec->GetPhaseSpace()->GetVariable(0)->Dump();
   //elasticXSec->GetPhaseSpace()->GetVariable(1)->Dump();
   //elasticXSec->GetPhaseSpace()->GetVariable(2)->Dump();
   //elasticPSSampler->Refresh();

   eventGen->Initialize();
   eventGen->CalculateTotalCrossSection();

   //TFile * f = new TFile("event_gen/test.root","RECREATE"); 
   //TParticle * part = new TParticle();
   //TTree * t = new TTree("EventGenTest","Event generator DIS Test");
   //t->Branch("partThrown","TParticle",&part);

   eventGen->Print();

   std::ofstream evout("results/event_gen/event_gen_lund_format.txt");

   for(int i = 0;i<Nevents;i++){
      //std::cout << " Event " << i << "\n";
      TList * parts = eventGen->GenerateEvent();
      eventGen->LundFormat(evout);
      //if(parts->GetEntries() > 1){
      //   part = (TParticle*) parts->At(0);
      //   std::cout << " P     = " << part->P() << "\n";
      //   std::cout << " theta = " << part->Theta()/degree << "\n";
      //   std::cout << " phi   = " << part->Phi()/degree << "\n";
      //   part = (TParticle*) parts->At(1);
      //   std::cout << " P     = " << part->P() << "\n";
      //   std::cout << " theta = " << part->Theta()/degree << "\n";
      //   std::cout << " phi   = " << part->Phi()/degree << "\n";
      //   //t->Fill();
      //}
      //parts->Print();
   }
   return(0);
}
