/*! Event generator test for wiser.

 */ 
Int_t event_gen_wiser(){
   
   Double_t fBeamEnergy = 5.9;

   // Event Generator
   InSANEEventGenerator * eventGen = new InSANEEventGenerator("DIS-event-gen","DIS Event Generator");

   //---------------------
   // Cross-sections
   //---------------------
   //F1F209eInclusiveDiffXSec * disXSec = new F1F209eInclusiveDiffXSec();
   //disXSec->SetBeamEnergy(fBeamEnergy);
   //disXSec->InitializeFinalStateParticles();
   //disXSec->InitializePhaseSpaceVariables();

   InSANEInclusiveWiserXSec * WiserDiffXSec = new InSANEInclusiveWiserXSec();
   WiserDiffXSec->SetBeamEnergy(fBeamEnergy);
   WiserDiffXSec->InitializeFinalStateParticles();
   WiserDiffXSec->InitializePhaseSpaceVariables();

   //---------------------
   // Phase-space Samplers
   //---------------------
   //InSANEPhaseSpaceSampler *  disPSSampler = new InSANEPhaseSpaceSampler(disXSec);
   InSANEPhaseSpaceSampler *  WiserPSSampler = new InSANEPhaseSpaceSampler(WiserDiffXSec);


   // Add the event generator.
   //eventGen->AddSampler(disPSSampler);
   eventGen->AddSampler(WiserPSSampler);

   eventGen->Initialize();
   eventGen->CalculateTotalCrossSection();


   TFile * f = new TFile("event_gen/test.root","RECREATE"); 
   TParticle * part = new TParticle();
   TTree * t = new TTree("EventGenTest","Event generator DIS Test");
   t->Branch("partThrown","TParticle",&part);
   Float_t theta, phi;
   t->Branch("theta",&theta,"theta/F");
   t->Branch("phi",&phi,"phi/F");

   TVector3 p;

   eventGen->Print();
   for(int i = 0;i<2000;i++){
      TList * parts = eventGen->GenerateEvent();
      if(parts->GetEntries() > 0){
         part = (TParticle*) parts->At(0);
         p.SetXYZ(part->Px(),part->Py(),part->Pz());
         theta = p.Theta();
         phi = p.Phi();

         t->Fill();
      }
      //parts->Print();
   }
   TF1 * f1 = new TF1("f",WiserDiffXSec,&InSANEInclusiveDiffXSec::EnergyDependentXSec,1.0,5.9,2,"InSANEInclusiveDiffXSec","EnergyDependentXSec");   // create TF1 class.
   f1->SetParameter(0,20*degree);
   f1->SetParameter(1,0);
   //f1->Draw();
   t->StartViewer();
   return(0);
}
