/*! Event generator test for wiser.

 */ 
Int_t event_gen_dvcs(){

   using namespace InSANE::physics;
   
   Double_t fBeamEnergy = 5.9;

   // Event Generator
   InSANEEventGenerator * eventGen = new InSANEEventGenerator("DIS-event-gen","DIS Event Generator");

   //---------------------
   // Cross-sections
   //---------------------
   //F1F209eInclusiveDiffXSec * disXSec = new F1F209eInclusiveDiffXSec();
   //disXSec->SetBeamEnergy(fBeamEnergy);
   //disXSec->InitializeFinalStateParticles();
   //disXSec->InitializePhaseSpaceVariables();

   //DVCSDiffXSec * xsec0 = new DVCSDiffXSec();
   CoherentDVCSXSec * xsec0 = new CoherentDVCSXSec();
   xsec0->SetBeamEnergy(fBeamEnergy);
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();

   std::cout << "derp\n";
   //---------------------
   // Phase-space Samplers
   //---------------------
   //InSANEPhaseSpaceSampler *  disPSSampler = new InSANEPhaseSpaceSampler(disXSec);
   InSANEPhaseSpaceSampler *  ps_samp0 = new InSANEPhaseSpaceSampler(xsec0);


   // Add the event generator.
   //eventGen->AddSampler(disPSSampler);
   eventGen->AddSampler(ps_samp0);

   eventGen->Initialize();
   eventGen->CalculateTotalCrossSection();


   TFile * f = new TFile("data/event_gen_dvcs.root","RECREATE"); 
   TParticle * part0 = 0;
   TParticle * part1 = 0;
   TParticle * part2 = 0;
   TTree * t = new TTree("EventGenTest","Event generator DVCS Test");
   t->Branch("partThrown0","TParticle",&part0);
   t->Branch("partThrown1","TParticle",&part1);
   t->Branch("partThrown2","TParticle",&part2);
   Float_t theta, phi;
   t->Branch("theta",&theta,"theta/F");
   t->Branch("phi",&phi,"phi/F");

   TH1F * hEnergy0 = new TH1F("hEnergy0","Energy e'", 100,0,5.0);

   TVector3 p;

   eventGen->Print();
  
   for(int i = 0;i<5000;i++){
      TList * parts = eventGen->GenerateEvent();
      if(parts->GetEntries() > 0){
         part0 = (TParticle*) parts->At(0);
         part1 = (TParticle*) parts->At(1);
         part2 = (TParticle*) parts->At(2);

         p.SetXYZ(part0->Px(),part0->Py(),part0->Pz());
         theta = p.Theta();
         phi = p.Phi();

         hEnergy0->Fill(part0->Energy());

         t->Fill();
      }
      //parts->Print();
   }
   //TF1 * f1 = new TF1("f",WiserDiffXSec,&InSANEInclusiveDiffXSec::EnergyDependentXSec,1.0,5.9,2,"InSANEInclusiveDiffXSec","EnergyDependentXSec");   // create TF1 class.
   //f1->SetParameter(0,20*degree);
   //f1->SetParameter(1,0);
   //f1->Draw();
   hEnergy0->Draw();
   t->StartViewer();
   return(0);
}
