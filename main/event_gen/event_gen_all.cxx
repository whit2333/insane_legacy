/*! Event generator test for many cross sections.
Includes:
 - Unpolarized electron DIS
 - Wiser

 */ 
Int_t event_gen_all(){
   
   Double_t fBeamEnergy = 5.9;
   Double_t target_pol  = 0.6;
   Double_t beam_pol    = 0.2;
   Double_t Emin        = 1.0;
   Double_t Emax        = 3.5;
   Int_t    N_sample    = 10000;

   // Event Generator
   InSANEEventGenerator * eventGen = new InSANEEventGenerator("event-gen","Event Generator");

   //----------------------------------
   // Electron Phase Space
   //----------------------------------
   InSANEPhaseSpace * fPolarizedPhaseSpace = new InSANEPhaseSpace();
   InSANEPhaseSpace * fPhaseSpace          = new InSANEPhaseSpace();

   InSANEPhaseSpaceVariable * varEnergy = new InSANEPhaseSpaceVariable("Energy","E#prime"); 
   varEnergy->SetMinimum(Emin);         //GeV
   varEnergy->SetMaximum(Emax); //GeV
   varEnergy->SetVariableUnits("GeV");        //GeV
   fPolarizedPhaseSpace->AddVariable(varEnergy);
   fPhaseSpace->AddVariable(varEnergy);

   InSANEPhaseSpaceVariable *   varTheta = new InSANEPhaseSpaceVariable("theta","#theta");
   varTheta->SetMinimum(10.0*TMath::Pi()/180.0); //
   varTheta->SetMaximum(50.0*TMath::Pi()/180.0); //
   varTheta->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varTheta);
   fPhaseSpace->AddVariable(varTheta);

   InSANEPhaseSpaceVariable *   varPhi = new InSANEPhaseSpaceVariable("phi","#phi");
   varPhi->SetMinimum(-90.0*TMath::Pi()/180.0); //
   varPhi->SetMaximum(90.0*TMath::Pi()/180.0); //
   varPhi->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varPhi);
   fPhaseSpace->AddVariable(varPhi);

   InSANEDiscretePhaseSpaceVariable *   varHelicity = new InSANEDiscretePhaseSpaceVariable("helicity","#lambda");
   varHelicity->SetNumberOfValues(3); 
   fPolarizedPhaseSpace->AddVariable(varHelicity);


   //---------------------
   // Cross-sections
   //---------------------

   // - Build up electron DIS x-sec
   //  - unpolarized 
   F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
   fDiffXSec->SetBeamEnergy(fBeamEnergy);
   fDiffXSec->SetA(1);
   fDiffXSec->SetZ(1);
   fDiffXSec->SetPhaseSpace(fPhaseSpace);
   fDiffXSec->Refresh();
   //  - polarized 
   PolarizedDISXSecParallelHelicity * fDiffXSec1 = new  PolarizedDISXSecParallelHelicity();
   fDiffXSec1->SetBeamEnergy(fBeamEnergy);
   fDiffXSec1->SetPhaseSpace(fPolarizedPhaseSpace);
   fDiffXSec1->Refresh();
   PolarizedDISXSecAntiParallelHelicity * fDiffXSec2 = new  PolarizedDISXSecAntiParallelHelicity();
   fDiffXSec2->SetBeamEnergy(fBeamEnergy);
   fDiffXSec2->SetPhaseSpace(fPolarizedPhaseSpace);
   fDiffXSec2->Refresh();
   //  - Add cross sections to total cross section
   InSANEPolarizedDiffXSec * fPolXSec = new InSANEPolarizedDiffXSec();
   fPolXSec->SetUnpolarizedCrossSection(fDiffXSec);
   fPolXSec->SetPolarizedPositiveCrossSection(fDiffXSec1);
   fPolXSec->SetPolarizedNegativeCrossSection(fDiffXSec2);
   fPolXSec->SetPhaseSpace(fPolarizedPhaseSpace);
   fPolXSec->SetBeamEnergy(fBeamEnergy);
   fPolXSec->SetChargeAsymmetry(-0.002);
   fPolXSec->SetBeamPolarization(beam_pol);
   fPolXSec->SetTargetPolarization(target_pol);
   fPolXSec->Refresh();

   //F1F209eInclusiveDiffXSec * disXSec = new F1F209eInclusiveDiffXSec();
   //disXSec->InitializeFinalStateParticles();
   //disXSec->InitializePhaseSpaceVariables();

   std::cout << "pions..." << std::endl;
   InSANEInclusiveWiserXSec * WiserDiffXSec = new InSANEInclusiveWiserXSec();
   WiserDiffXSec->SetProductionParticleType(111); // pi0
   WiserDiffXSec->InitializePhaseSpaceVariables();
   WiserDiffXSec->InitializeFinalStateParticles();

   InSANEInclusiveWiserXSec * WiserDiffXSec2 = new InSANEInclusiveWiserXSec();
   WiserDiffXSec2->SetProductionParticleType(211); // pi+
   WiserDiffXSec2->InitializePhaseSpaceVariables();
   WiserDiffXSec2->InitializeFinalStateParticles();

   InSANEInclusiveWiserXSec * WiserDiffXSec3 = new InSANEInclusiveWiserXSec();
   WiserDiffXSec3->SetProductionParticleType(-211); // pi-
   WiserDiffXSec3->InitializePhaseSpaceVariables();
   WiserDiffXSec3->InitializeFinalStateParticles();

   //---------------------
   // Phase-space Samplers
   //---------------------
   std::cout << "samplers..." << std::endl;
   InSANEPhaseSpaceSampler *  disPSSampler = new InSANEPhaseSpaceSampler(fPolXSec);
   disPSSampler->SetFoamSample(10);
   InSANEPhaseSpaceSampler *  WiserPSSampler = new InSANEPhaseSpaceSampler(WiserDiffXSec);
   WiserPSSampler->SetFoamSample(10);
   InSANEPhaseSpaceSampler *  WiserPSSampler2 = new InSANEPhaseSpaceSampler(WiserDiffXSec2);
   WiserPSSampler2->SetFoamSample(10);
   InSANEPhaseSpaceSampler *  WiserPSSampler3 = new InSANEPhaseSpaceSampler(WiserDiffXSec3);
   WiserPSSampler3->SetFoamSample(10);


   // Add the event generator.
   eventGen->AddSampler(disPSSampler);
   //eventGen->AddSampler(WiserPSSampler);
   //eventGen->AddSampler(WiserPSSampler2);
   //eventGen->AddSampler(WiserPSSampler3);

   std::cout << "samplers..." << std::endl;

   eventGen->SetBeamEnergy(fBeamEnergy);
   eventGen->Refresh();

   eventGen->Initialize();
   eventGen->CalculateTotalCrossSection();


   TFile * f = new TFile("data/test_event_gen_all.root","RECREATE"); 
   InSANEParticle * part = new InSANEParticle();
   TTree * t = new TTree("EventGenTest","Event generator DIS Test");
   t->Branch("partThrown","InSANEParticle",&part);

   eventGen->Print();
   for(int i = 0;i<N_sample;i++){
      TList * parts = eventGen->GenerateEvent();
      if(parts->GetEntries() > 0){
         part = (InSANEParticle*) parts->At(0);
         t->Fill();
      }
      //parts->Print();
   }
   //TF1 * f1 = new TF1("f",disXSec,&InSANEInclusiveDiffXSec::EnergyDependentXSec,1.0,5.9,2,"InSANEInclusiveDiffXSec","EnergyDependentXSec");   // create TF1 class.
   //f1->SetParameter(0,20*degree);
   //f1->SetParameter(1,0);
   //f1->Draw();
   t->StartViewer();
   return(0);
}
