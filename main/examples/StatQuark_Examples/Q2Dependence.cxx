// Investigate Q2 dependence of the StatisticalQuark model 

void Q2Dependence(){

	StatisticalQuarkFits *Model = new StatisticalQuarkFits();
	Model->UseQ2Interpolation();

	// interpolation model 
	vector<Double_t> x,U,D,S,G; 
	vector<Double_t> UBAR,DBAR,SBAR; 

	Double_t Qsq[5] = {3.,4.,5.,6.,7.}; // GeV^2
	Double_t xmin = 5E-4; 
        Double_t xmax = 1.0;
	Double_t dx   = 1E-4;
	Double_t ix   = xmin;

        int width = 2; 

        TMultiGraph *mU = new TMultiGraph(); 
        TMultiGraph *mD = new TMultiGraph(); 
        TMultiGraph *mS = new TMultiGraph(); 
        TMultiGraph *mG = new TMultiGraph(); 

	TLegend *qleg = new TLegend(0.6,0.7,0.8,0.9);
	qleg->SetFillColor(kWhite);

	int color;
	for(int i=0;i<5;i++){
		if(i<4)  color = i+1; 
		if(i==4) color = i+2;
		while(ix < xmax ){
			// interpolation model 
			U.push_back( ix*Model->GetUQuarkInterp(ix,Qsq[i]) ); 
			D.push_back( ix*Model->GetDQuarkInterp(ix,Qsq[i]) ); 
			S.push_back( ix*Model->GetSQuarkInterp(ix,Qsq[i]) ); 
			G.push_back( (ix/10.)*Model->GetGluonInterp(ix,Qsq[i]) ); 

			x.push_back(ix);
			//      cout << ix << " " << myStatisticalQuark->GetFermiDiracFunc("u",1,ix,Qsq) << endl;
			//      ix = xmax;
			ix += dx;
		}
		ix = xmin;
                TGraph *u = GetTGraph(x,U);
                u->SetLineWidth(width); 
                u->SetLineColor(color); 
                TGraph *d = GetTGraph(x,D);
                d->SetLineWidth(width); 
                d->SetLineColor(color);
                TGraph *s = GetTGraph(x,S);
		s->SetLineWidth(width); 
                s->SetLineColor(color);
                TGraph *g = GetTGraph(x,G);
		g->SetLineWidth(width); 
                g->SetLineColor(color);
                mU->Add(u,"c"); 
                mD->Add(d,"c"); 
                mS->Add(s,"c"); 
                mG->Add(g,"c"); 

		qleg->AddEntry(u,Form("Q^{2} = %.0f GeV^{2}",Qsq[i]),"l");

		x.clear();
		U.clear();
		D.clear();
		S.clear();
		G.clear();
	}

        TString xAxisTitle = Form("x");

	TCanvas *c1 = new TCanvas("c1","Q2 Dependence",1000,800);
	c1->SetFillColor(kWhite); 
        c1->Divide(2,2); 

	c1->cd(1);
	mU->Draw("A");
	mU->SetTitle("u Quark");
	mU->GetXaxis()->SetTitle(xAxisTitle);
	mU->GetXaxis()->CenterTitle();
	mU->GetYaxis()->SetTitle("xu(x,Q^{2})");
	mU->GetYaxis()->CenterTitle();
	qleg->Draw("same");
	c1->Update();

	c1->cd(2);
	mD->Draw("A");
	mD->SetTitle("d Quark");
	mD->GetXaxis()->SetTitle(xAxisTitle);
	mD->GetXaxis()->CenterTitle();
	mD->GetYaxis()->SetTitle("xd(x,Q^{2})");
	mD->GetYaxis()->CenterTitle();
	c1->Update();

	c1->cd(3);
	mS->Draw("A");
	mS->SetTitle("s Quark");
	mS->GetXaxis()->SetTitle(xAxisTitle);
	mS->GetXaxis()->CenterTitle();
	mS->GetYaxis()->SetTitle("xs(x,Q^{2})");
	mS->GetYaxis()->CenterTitle();
	c1->Update();

	c1->cd(4);
	mG->Draw("A");
	mG->SetTitle("Gluon/10");
	mG->GetXaxis()->SetTitle(xAxisTitle);
	mG->GetXaxis()->CenterTitle();
	mG->GetYaxis()->SetTitle("xg(x,Q^{2})");
	mG->GetYaxis()->CenterTitle();
	c1->Update();


}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){
	const int N = x.size(); 
	TGraph *g = new TGraph(N,&x[0],&y[0]);
	return g;
}
