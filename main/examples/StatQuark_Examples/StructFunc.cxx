//Plots F2p,F2n, F2n/F2p and (F2p - F2n)
//from CTEQ,NMC95 and Statistical models
#include <vector>

Int_t StructFunc() {

   Double_t xmin = 0.00001; Double_t xmax = 1.0;
   Double_t dx = 0.001; Double_t iQ2 = 4.0;
   Double_t ix = xmin;

   vector<Double_t> F2p_Statistical,F2p_CTEQ,F2p_F1F209,F2p_NMC95;
   vector<Double_t> F2n_Statistical,F2n_CTEQ,F2n_F1F209,F2n_NMC95;
   vector<Double_t> X,Q2;
   //RF2 = F2n/F2p, dF2 = F2p - F2n
   vector<Double_t> RF2_Statistical,RF2_CTEQ,RF2_F1F209,RF2_NMC95;
   vector<Double_t> dF2_Statistical,dF2_CTEQ,dF2_F1F209,dF2_NMC95;

  InSANEStructureFunctionsFromPDFs * cteqSFs = new InSANEStructureFunctionsFromPDFs();   
  cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());

  InSANEStructureFunctions * f1f2SFs = new F1F209StructureFunctions();  

  InSANEStructureFunctionsFromPDFs * bbsSFs = new InSANEStructureFunctionsFromPDFs();
  bbsSFs->SetUnpolarizedPDFs(new StatisticalUnpolarizedPDFs());

  NMC95StructureFunctions * nmcSFs = new NMC95StructureFunctions();

  while(ix < xmax)
    {
      F2p_CTEQ.push_back(cteqSFs->F2p(ix,iQ2));
      F2n_CTEQ.push_back(cteqSFs->F2n(ix,iQ2));
      F2p_Statistical.push_back(bbsSFs->F2p(ix,iQ2));
      F2n_Statistical.push_back(bbsSFs->F2n(ix,iQ2));
      F2p_F1F209.push_back(f1f2SFs->F2p(ix,iQ2));
      F2n_F1F209.push_back(f1f2SFs->F2n(ix,iQ2));
      F2p_NMC95.push_back(nmcSFs->F2p(ix,iQ2));
      F2n_NMC95.push_back(nmcSFs->F2n(ix,iQ2));
      X.push_back(ix); Q2.push_back(iQ2);
      ix+=dx;
   }
   for(Int_t i=0; i<X.size();i++)
     {
       RF2_CTEQ.push_back(F2n_CTEQ[i]/F2p_CTEQ[i]);
       dF2_CTEQ.push_back(F2p_CTEQ[i] - F2n_CTEQ[i]);
       RF2_Statistical.push_back(F2n_Statistical[i]/F2p_Statistical[i]);
       dF2_Statistical.push_back(F2p_Statistical[i] - F2n_Statistical[i]);
       RF2_NMC95.push_back(F2n_NMC95[i]/F2p_NMC95[i]);
       dF2_NMC95.push_back(F2p_NMC95[i] - F2n_NMC95[i]);

//       cout << F2p_CTEQ[i] << " " << F2p_Statistical[i] << " " << F2p_F1F209[i] << " " << F2p_NMC95[i] << endl;
     }
  
     TCanvas *c1 = new TCanvas("F2","F2");
     c1->Divide(2,1);
     //--F2p
     TGraph *gr_F2pCTEQ = new TGraph(X.size(),&X[0],&F2p_CTEQ[0]);
     gr_F2pCTEQ->SetLineColor(kBlack);

     TGraph *gr_F2pStatistical = new TGraph(X.size(),&X[0],&F2p_Statistical[0]);
     gr_F2pStatistical->SetLineColor(kBlue);

     TGraph *gr_F2pNMC95 = new TGraph(X.size(),&X[0],&F2p_NMC95[0]);
     gr_F2pNMC95->SetLineColor(kRed);
     //--F2n
     TGraph *gr_F2nCTEQ = new TGraph(X.size(),&X[0],&F2n_CTEQ[0]);
     gr_F2nCTEQ->SetLineColor(kBlack);
     
     TGraph *gr_F2nStatistical = new TGraph(X.size(),&X[0],&F2n_Statistical[0]);
     gr_F2nStatistical->SetLineColor(kBlue);

     TGraph *gr_F2nNMC95 = new TGraph(X.size(),&X[0],&F2n_NMC95[0]);
     gr_F2nNMC95->SetLineColor(kRed);

     TLegend *leg01 = new TLegend(0.6,0.7,0.8,0.9);
     leg01->SetFillColor(10);
     leg01->SetLineColor(10);
     //--F2p
     TMultiGraph *mg_F2p = new TMultiGraph();
     c1->cd(1);
     mg_F2p->SetTitle("F_{2}^{p} Q^{2} = 4.0 GeV^{2}");
     mg_F2p->Add(gr_F2pCTEQ,"c"); leg01->AddEntry(gr_F2pCTEQ,"CTEQ","l");
     mg_F2p->Add(gr_F2pStatistical,"c"); leg01->AddEntry(gr_F2pStatistical,"Statistical","l");
     mg_F2p->Add(gr_F2pNMC95,"c"); leg01->AddEntry(gr_F2pNMC95,"NMC95","l");
     mg_F2p->Draw("a");
     leg01->Draw("same");
     gPad->SetLogx(1);
     mg_F2p->GetXaxis()->SetTitle("x");
     mg_F2p->GetXaxis()->CenterTitle();
     mg_F2p->GetXaxis()->SetLimits(10e-6,1.0);
     mg_F2p->GetYaxis()->SetTitle("F_{2}^{p}");
     mg_F2p->GetYaxis()->CenterTitle();
     mg_F2p->GetYaxis()->SetRangeUser(0.0,2.0);  
     c1->Update();
     //--F2n
     TMultiGraph *mg_F2n = new TMultiGraph();
     c1->cd(2);
     mg_F2n->SetTitle("F_{2}^{n} Q^{2} = 4.0 GeV^{2}");
     mg_F2n->Add(gr_F2nCTEQ,"c");// leg01->AddEntry(gr_F2nCTEQ,"CTEQ","l");
     mg_F2n->Add(gr_F2nStatistical,"c"); //leg01->AddEntry(gr_F2nStatistical,"Statistical","l");
     mg_F2n->Add(gr_F2nNMC95,"c"); //leg01->AddEntry(gr_F2nNMC95,"NMC95","l");
     mg_F2n->Draw("a");
     gPad->SetLogx(1);
     mg_F2n->GetXaxis()->SetTitle("x");
     mg_F2n->GetXaxis()->CenterTitle();
     mg_F2n->GetXaxis()->SetLimits(10e-6,1.0);
     mg_F2n->GetYaxis()->SetTitle("F_{2}^{n}");
     mg_F2n->GetYaxis()->CenterTitle();
     mg_F2n->GetYaxis()->SetRangeUser(0.0,2.0);  
     c1->Update();

     //-RF2
     TCanvas *c2 = new TCanvas("F2n/F2p","F2n/F2p");

     TLegend *leg02 = new TLegend(0.6,0.7,0.8,0.9);
     leg02->SetFillColor(10);
     leg02->SetLineColor(10);

     TGraph *gr_RF2Statistical = new TGraph(X.size(),&X[0],&RF2_Statistical[0]);
     gr_RF2Statistical->SetLineColor(kBlue);

     TGraph *gr_RF2CTEQ = new TGraph(X.size(),&X[0],&RF2_CTEQ[0]);
     gr_RF2CTEQ->SetLineColor(kBlack);

     TGraph *gr_RF2NMC95 = new TGraph(X.size(),&X[0],&RF2_NMC95[0]);
     gr_RF2NMC95->SetLineColor(kRed);

     TMultiGraph *mg_RF2 = new TMultiGraph();
     c2->cd(1);
     mg_RF2->SetTitle("F_{2}^{n}/F_{2}^{p}, Q^{2} = 4.0 GeV^{2}");
     mg_RF2->Add(gr_RF2CTEQ,"c"); leg02->AddEntry(gr_RF2CTEQ,"CTEQ","l");
     mg_RF2->Add(gr_RF2Statistical,"c"); leg02->AddEntry(gr_RF2Statistical,"Statistical","l");
     mg_RF2->Add(gr_RF2NMC95,"c"); leg02->AddEntry(gr_RF2NMC95,"NMC95","l");
     mg_RF2->Draw("a");
     leg02->Draw("same");
     gPad->SetLogx(1);
     mg_RF2->GetXaxis()->SetTitle("x");
     mg_RF2->GetXaxis()->CenterTitle();
     mg_RF2->GetXaxis()->SetLimits(10e-6,1.0);
     mg_RF2->GetYaxis()->SetTitle("F_{2}^{n}/F_{2}^{p}");
     mg_RF2->GetYaxis()->CenterTitle();
     mg_RF2->GetYaxis()->SetRangeUser(0.3,1.1);  
     c2->Update();
     //--dF2
     TCanvas *c3 = new TCanvas("dF2p","dF2");

     TLegend *leg03 = new TLegend(0.6,0.7,0.8,0.9);
     leg03->SetFillColor(10);
     leg03->SetLineColor(10);

     TGraph *gr_dF2Statistical = new TGraph(X.size(),&X[0],&dF2_Statistical[0]);
     gr_dF2Statistical->SetLineColor(kBlue);

     TGraph *gr_dF2CTEQ = new TGraph(X.size(),&X[0],&dF2_CTEQ[0]);
     gr_dF2CTEQ->SetLineColor(kBlack);

     TGraph *gr_dF2NMC95 = new TGraph(X.size(),&X[0],&dF2_NMC95[0]);
     gr_dF2NMC95->SetLineColor(kRed);

     TMultiGraph *mg_dF2 = new TMultiGraph();
     c3->cd(1);
     mg_dF2->SetTitle("F_{2}^{p} - F_{2}^{n}, Q^{2} = 4.0 GeV^{2}");
     mg_dF2->Add(gr_dF2CTEQ,"c"); leg03->AddEntry(gr_dF2CTEQ,"CTEQ","l");
     mg_dF2->Add(gr_dF2Statistical,"c"); leg03->AddEntry(gr_dF2Statistical,"Statistical","l");
     mg_dF2->Add(gr_dF2NMC95,"c"); leg03->AddEntry(gr_dF2NMC95,"NMC95","l");
     mg_dF2->Draw("a");
     gPad->SetLogx(1);
     leg03->Draw("same");
     mg_dF2->GetXaxis()->SetTitle("x");
     mg_dF2->GetXaxis()->CenterTitle();
     mg_dF2->GetXaxis()->SetLimits(10e-3,1.0);
     mg_dF2->GetYaxis()->SetTitle("F_{2}^{p} - F_{2}^{n}");
     mg_dF2->GetYaxis()->CenterTitle();
     mg_dF2->GetYaxis()->SetRangeUser(0.0,0.151);  
     c3->Update();

return(0);
}
