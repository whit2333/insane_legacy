//Plots unpolarized parton distributions using StatisticalQuarkFits
#include <vector>
//#include "StatisticalQuarkFits.h"
void PlotPartons()
{

	StatisticalQuarkFits *myStatisticalQuark       = new StatisticalQuarkFits();

	StatisticalQuarkFits *myStatisticalQuarkInterp = new StatisticalQuarkFits();
	myStatisticalQuarkInterp->UseQ2Interpolation();

	//  cout << myStatisticalQuark->GetFermiDiracFunc("u",1,0.0001,4.0) << endl;

	//--quarks
	vector<Double_t> quplus,quminus;
	vector<Double_t> qdplus,qdminus;
	vector<Double_t> d,u;
	//-anti-quarks
	vector<Double_t> qubarplus,qubarminus;
	vector<Double_t> qdbarplus,qdbarminus;
	vector<Double_t> dbar,ubar;
	//--Gluon
	vector<Double_t> gluon;

	// interpolation model 
	vector<Double_t> U,D,S,G; 
	vector<Double_t> UBAR,DBAR,SBAR; 

	Double_t Qsq = 4.0; //GeV^2
	Double_t xmin = 0.0001; Double_t xmax = 1.0;
	Double_t dx = 0.0001;
	Double_t ix = xmin;
	vector<Double_t> x;

	while(ix < xmax )
	{
		//--Quarks 
		quplus.push_back(  myStatisticalQuark->GetUQuark(1,ix,Qsq) );
		quminus.push_back( myStatisticalQuark->GetUQuark(-1,ix,Qsq) );
		qdplus.push_back(  myStatisticalQuark->GetDQuark(1,ix,Qsq) );
		qdminus.push_back( myStatisticalQuark->GetDQuark(-1,ix,Qsq) );      

		//--Anti-Quarks 
		qubarplus.push_back(  myStatisticalQuark->GetAntiUQuark( 1,ix,Qsq) );
		qubarminus.push_back( myStatisticalQuark->GetAntiUQuark(-1,ix,Qsq) );
		qdbarplus.push_back(  myStatisticalQuark->GetAntiDQuark( 1,ix,Qsq) );
		qdbarminus.push_back( myStatisticalQuark->GetAntiDQuark(-1,ix,Qsq) );

		//--Gluon
		gluon.push_back( myStatisticalQuark->GetGluon(ix,Qsq)*(ix/10.0) );      

                // interpolation model 
                U.push_back( ix*myStatisticalQuarkInterp->GetUQuarkInterp(ix,Qsq) ); 
                D.push_back( ix*myStatisticalQuarkInterp->GetDQuarkInterp(ix,Qsq) ); 
                S.push_back( ix*myStatisticalQuarkInterp->GetSQuarkInterp(ix,Qsq) ); 
                UBAR.push_back( ix*myStatisticalQuarkInterp->GetAntiUQuarkInterp(ix,Qsq) ); 
                DBAR.push_back( ix*myStatisticalQuarkInterp->GetAntiDQuarkInterp(ix,Qsq) ); 
                SBAR.push_back( ix*myStatisticalQuarkInterp->GetAntiSQuarkInterp(ix,Qsq) ); 
                G.push_back( (ix/10.)*myStatisticalQuarkInterp->GetGluonInterp(ix,Qsq) ); 

		x.push_back(ix);
		//      cout << ix << " " << myStatisticalQuark->GetFermiDiracFunc("u",1,ix,Qsq) << endl;
		//      ix = xmax;
		ix += dx;
	}

	for(Int_t i=0; i<x.size();i++){
		u.push_back( x[i]*(quplus[i] + quminus[i]) );
		d.push_back( x[i]*(qdplus[i] + qdminus[i]) );
		ubar.push_back( x[i]*(qubarplus[i] + qubarminus[i]) );
		dbar.push_back( x[i]*(qdbarplus[i] + qdbarminus[i]) );

	}

	TGraph *ggluon = new TGraph(x.size(),&x[0],&gluon[0]);
	ggluon->SetLineColor(kGreen+2);
	//--Quarks
	TGraph *gu = new TGraph(x.size(),&x[0],&u[0]);

	TGraph *gd = new TGraph(x.size(),&x[0],&d[0]);
	gd->SetLineStyle(2);

	TGraph *gdbar = new TGraph(x.size(),&x[0],&dbar[0]);
	gdbar->SetLineStyle(3);
	gdbar->SetLineColor(kBlue);

	TGraph *gubar = new TGraph(x.size(),&x[0],&ubar[0]);
	gubar->SetLineStyle(4);
	gubar->SetLineColor(kBlue);

        TGraph *gU    = GetTGraph(x,U); 
        
	TGraph *gD    = GetTGraph(x,D); 
        gD->SetLineStyle(2); 
        
        TGraph *gUBAR = GetTGraph(x,UBAR); 
	gUBAR->SetLineStyle(4);
	gUBAR->SetLineColor(kCyan);

        TGraph *gDBAR = GetTGraph(x,DBAR); 
        gDBAR->SetLineStyle(3); 
        gDBAR->SetLineColor(kCyan); 

        TGraph *gG    = GetTGraph(x,G);
        gG->SetLineColor(kGreen);  

	TLegend *qleg = new TLegend(0.6,0.7,0.8,0.9);
	qleg->SetLineColor(10);
	qleg->SetFillColor(10);
	qleg->AddEntry(ggluon,"gluon/10"          ,"l");
	qleg->AddEntry(gu    ,"u"                 ,"l");
	qleg->AddEntry(gubar ,"ubar"              ,"l");
	qleg->AddEntry(gdbar ,"dbar"              ,"l");
	qleg->AddEntry(gd    ,"d"                 ,"l");
	qleg->AddEntry(gG    ,"gluon/10 (interp.)","l");
	qleg->AddEntry(gU    ,"u (interp.)"       ,"l");
	qleg->AddEntry(gUBAR ,"ubar (interp.)"    ,"l");
	qleg->AddEntry(gDBAR ,"dbar (interp.)"    ,"l");
	qleg->AddEntry(gD    ,"d (interp.)"       ,"l");

	TCanvas *c1 = new TCanvas();
	c1->cd(1);

	TMultiGraph *mg = new TMultiGraph();
	mg->Add(ggluon,"c"); 
	mg->Add(gu    ,"c"); 
	mg->Add(gubar ,"c"); 
	mg->Add(gdbar ,"c"); 
	mg->Add(gd    ,"c"); 
	mg->Add(gG    ,"c"); 
	mg->Add(gU    ,"c"); 
	mg->Add(gUBAR ,"c"); 
	mg->Add(gDBAR ,"c"); 
	mg->Add(gD    ,"c"); 
	mg->Draw("a");
	qleg->Draw("same");
	mg->GetYaxis()->SetTitle("xf(x,Q_{0}^{2})");
	mg->GetYaxis()->CenterTitle();
	//  mg->GetYaxis()->SetRangeUser(0.0,0.5);
	mg->GetXaxis()->SetTitle("x");
	mg->GetXaxis()->CenterTitle();
	mg->GetXaxis()->SetLimits(xmin,xmax);
	gPad->SetLogx(1);
	c1->Update();
}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){
	const int N = x.size(); 
	TGraph *g = new TGraph(N,&x[0],&y[0]);
	return g;
}
