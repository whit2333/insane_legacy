//Plots strange quark quantities for using StatisticalQuarkFits
#include <vector>
//#include "StatisticalQuarkFits.h"
void PlotStrangeQuark()
{

  StatisticalQuarkFits *myStatisticalQuark = new StatisticalQuarkFits();

  //--quarks
  vector<Double_t> qsplus,qsminus;
  vector<Double_t> sq,ds;
  //-anti-quarks
  vector<Double_t> qsbarplus,qsbarminus;
  vector<Double_t> sbar,dsbar;
  vector<Double_t> dssbar; //s - sbar

  Double_t Qsq = 4.0; //GeV^2
  Double_t xmin = 0.0001; Double_t xmax = 1.0;
  Double_t dx = 0.0001;
  Double_t ix = xmin;
  vector<Double_t> x;
  
  while(ix < xmax )
    {
      //--Quarks 
      qsplus.push_back(  myStatisticalQuark->GetSQuark(1,ix,Qsq) );
      qsminus.push_back( myStatisticalQuark->GetSQuark(-1,ix,Qsq) );
      
      //--Anti-Quarks 
      qsbarplus.push_back(  myStatisticalQuark->GetAntiSQuark( 1,ix,Qsq) );
      qsbarminus.push_back( myStatisticalQuark->GetAntiSQuark(-1,ix,Qsq) );

      x.push_back(ix);
//      cout << ix << " " << myStatisticalQuark->GetFermiDiracFunc("u",1,ix,Qsq) << endl;
//      ix = xmax;
      ix += dx;
    }
  for(Int_t i=0; i<x.size();i++)
    {
      sq.push_back( x[i]*(qsplus[i] + qsminus[i]) );
      sbar.push_back( x[i]*(qsbarplus[i] + qsbarminus[i]) );
      ds.push_back( x[i]*(qsplus[i] - qsminus[i])*10.0 );
      dsbar.push_back( x[i]*(qsbarplus[i] - qsbarminus[i])*10.0 );
      dssbar.push_back( (sq[i] - sbar[i])/x[i] );
    }
 
  //--Quarks
  TGraph *gs = new TGraph(x.size(),&x[0],&sq[0]);

  TGraph *gsbar = new TGraph(x.size(),&x[0],&sbar[0]);
  gsbar->SetLineStyle(2);

  TGraph *gdsbar = new TGraph(x.size(),&x[0],&dsbar[0]);
  gdsbar->SetLineStyle(2);
  gdsbar->SetLineColor(kBlue);

  TGraph *gds = new TGraph(x.size(),&x[0],&ds[0]);
//  gubar->SetLineStyle(2);
  gdsbar->SetLineColor(kBlue);
      
  TLegend *qleg = new TLegend(0.6,0.7,0.8,0.9);
  qleg->SetLineColor(10);
  qleg->SetFillColor(10);
  
  TLine *l0 = new TLine(xmin,0,xmax,0);

  TCanvas *c1 = new TCanvas();
  c1->cd(1);
  
  TMultiGraph *mg = new TMultiGraph();
  mg->Add(gs,"c"); qleg->AddEntry(gs,"xs","l");
  mg->Add(gsbar,"c"); qleg->AddEntry(gsbar,"xsbar","l");
  mg->Add(gdsbar,"c"); qleg->AddEntry(gdsbar,"x#Deltasbar (x10)","l");
  mg->Add(gds,"c"); qleg->AddEntry(gds,"x#Deltas (x10)","l");
  mg->Draw("a");
  l0->Draw("lsame");
  qleg->Draw("same");
  mg->GetYaxis()->SetTitle("xf(x,Q_{0}^{2})");
  mg->GetYaxis()->CenterTitle();
  mg->GetYaxis()->SetRangeUser(-0.2,0.5);
  mg->GetXaxis()->SetTitle("x");
  mg->GetXaxis()->CenterTitle();
  mg->GetXaxis()->SetLimits(xmin,xmax);
  gPad->SetLogx(1);
  c1->Update();

  TCanvas *c2 = new TCanvas();
  c2->cd(1);
  
  TGraph *gdssbar = new TGraph(x.size(),&x[0],&dssbar[0]);
  gdssbar->SetTitle("");
  gdssbar->Draw("ac");  
  l0->Draw("lsame");
  gdssbar->GetXaxis()->SetTitle("x");
  gdssbar->GetXaxis()->CenterTitle();
  gdssbar->GetXaxis()->SetLimits(xmin,xmax);
  gdssbar->GetYaxis()->SetTitle("s - sbar");
  gdssbar->GetYaxis()->CenterTitle();
  gdssbar->GetYaxis()->SetLimits(-0.02,0.03);
  gPad->SetLogx(1);
  c2->Update();
}
