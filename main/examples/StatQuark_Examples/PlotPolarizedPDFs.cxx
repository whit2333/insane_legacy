
void PlotPolarizedPDFs(){


   StatisticalPolarizedPDFs *stat = new StatisticalPolarizedPDFs(); 

   int NPTS = 1000; 
   double xMin = 1E-4; 
   double xMax = 1.;
   double yMin = -0.1; 
   double yMax = 0.3; 
   double delta = (xMax-xMin)/( (double)NPTS ); 

   vector<double> x,u,d,ubar,dbar,str,c,g; 

   double arg = 0; 
   double Q2 = 20.;
   double ix=0;  
   for(int i=0;i<NPTS;i++){
      ix  = xMin + ( (double)i )*delta;
      arg = ix*stat->Deltau(ix,Q2); 
      u.push_back(arg);  
      arg = ix*stat->Deltad(ix,Q2); 
      d.push_back(arg);
      arg = ix*stat->DeltauBar(ix,Q2); 
      ubar.push_back(arg);
      arg = ix*stat->DeltadBar(ix,Q2); 
      dbar.push_back(arg);
      arg = ix*stat->Deltas(ix,Q2); 
      str.push_back(5.*arg);
      // arg = ix*stat->Deltag(ix,Q2); 
      // g.push_back(10.*arg);
      // arg = ix*stat->Deltac(ix,Q2); 
      // c.push_back(20.*arg);
      x.push_back(ix); 
   } 

   TGraph *gu    = GetTGraph(x,u);  
   TGraph *gubar = GetTGraph(x,ubar);  
   TGraph *gd    = GetTGraph(x,d);  
   TGraph *gdbar = GetTGraph(x,dbar);  
   TGraph *gs    = GetTGraph(x,str);  
   // TGraph *gg    = GetTGraph(x,g);  
   // TGraph *gc    = GetTGraph(x,c);  

   SetGraphParameters(gu   ,20,kBlack,2,1);
   SetGraphParameters(gubar,20,kBlack,2,2);
   SetGraphParameters(gd   ,20,kBlack,2,9);
   SetGraphParameters(gdbar,20,kBlack,2,3);
   SetGraphParameters(gs   ,20,kBlack,2,7);
   // SetGraphParameters(gg   ,20,kBlack,2,6);
   // SetGraphParameters(gc   ,20,kBlack,2,7);

   TMultiGraph *PPDF = new TMultiGraph(); 
   PPDF->Add(gu   ,"c"); 
   PPDF->Add(gubar,"c"); 
   PPDF->Add(gd   ,"c"); 
   PPDF->Add(gdbar,"c"); 
   PPDF->Add(gs   ,"c"); 
   // PPDF->Add(gg   ,"c"); 
   // PPDF->Add(gc   ,"c"); 

   TLegend *L = new TLegend(0.6,0.6,0.8,0.8); 
   L->SetFillColor(kWhite); 
   L->AddEntry(gu   ,"u"      ,"l"); 
   L->AddEntry(gubar,"#bar{u}","l"); 
   L->AddEntry(gd   ,"d"      ,"l"); 
   L->AddEntry(gdbar,"#bar{d}","l"); 
   L->AddEntry(gs   ,"5s"     ,"l"); 
   // L->AddEntry(gg   ,"10g"    ,"l"); 
   // L->AddEntry(gc   ,"20c"    ,"l"); 

   TLine *xAxisLine = new TLine(xMin,0.,xMax,0.); 
 
   TString Option     = Form("A"); 
   TString Title      = Form("x#Deltaf(x,Q^{2} = %.0f GeV^{2})",Q2);
   TString xAxisTitle = Form("x");

   TCanvas *c1 = new TCanvas("c1","x*delta_f",1000,800);
   c1->SetFillColor(kWhite);

   c1->cd();
   gPad->SetLogx(1);   

   PPDF->Draw(Option);
   PPDF->SetTitle(Title);
   PPDF->GetXaxis()->SetTitle(xAxisTitle);
   PPDF->GetXaxis()->CenterTitle();
   PPDF->GetXaxis()->SetLimits(xMin,xMax); 
   PPDF->GetYaxis()->SetRangeUser(yMin,yMax); 
   PPDF->Draw(Option); 
   xAxisLine->Draw("same"); 
   L->Draw("same"); 
   c1->Update();


}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){
   const int N = x.size();
   TGraph *g = new TGraph(N,&x[0],&y[0]);
   return g;
}
//______________________________________________________________________________
void SetGraphParameters(TGraph *g,int Marker,int Color,int width=2,int Line=1){

   g->SetMarkerStyle(Marker);
   g->SetMarkerColor(Color);
   g->SetLineColor(Color);
   g->SetLineWidth(width);
   g->SetLineStyle(Line); 

}
