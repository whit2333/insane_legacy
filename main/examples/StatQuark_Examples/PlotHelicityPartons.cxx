//Plots helicity dependent quark dist using StatisticalQuarkFits
#include <vector>
void PlotHelicityPartons()
{

  StatisticalQuarkFits *myStatisticalQuark = new StatisticalQuarkFits();
//  cout << myStatisticalQuark->GetFermiDiracFunc("u",1,0.0001,4.0) << endl;
  

  //--quarks
  vector<Double_t> quplus,quminus;
  vector<Double_t> qdplus,qdminus;
  //-anti-quarks
  vector<Double_t> qubarplus,qubarminus;
  vector<Double_t> qdbarplus,qdbarminus;

  Double_t Qsq = 4.0; //GeV^2
  Double_t xmin = 0.0001; Double_t xmax = 1.0;
  Double_t dx = 0.0001;
  Double_t ix = xmin;
  vector<Double_t> x;
  
  while(ix < xmax )
    {
      //--Quarks 
      quplus.push_back(  ix*myStatisticalQuark->GetUQuark(1,ix,Qsq) );
      quminus.push_back( ix*myStatisticalQuark->GetUQuark(-1,ix,Qsq) );
      qdplus.push_back(  ix*myStatisticalQuark->GetDQuark(1,ix,Qsq) );
      qdminus.push_back( ix*myStatisticalQuark->GetDQuark(-1,ix,Qsq) );      
      //--Anti-Quarks 
      qubarplus.push_back(  ix*myStatisticalQuark->GetAntiUQuark( 1,ix,Qsq) );
      qubarminus.push_back( ix*myStatisticalQuark->GetAntiUQuark(-1,ix,Qsq) );
      qdbarplus.push_back(  ix*myStatisticalQuark->GetAntiDQuark( 1,ix,Qsq) );
      qdbarminus.push_back( ix*myStatisticalQuark->GetAntiDQuark(-1,ix,Qsq) );

      x.push_back(ix);
//      cout << ix << " " << myStatisticalQuark->GetFermiDiracFunc("u",1,ix,Qsq) << endl;
//      ix = xmax;
      ix += dx;
    }
 
  //--Quarks
  TGraph *gFuplus = new TGraph(x.size(),&x[0],&quplus[0]);

  TGraph *gFuminus = new TGraph(x.size(),&x[0],&quminus[0]);
  gFuminus->SetLineStyle(2);
  gFuminus->SetLineColor(kBlue);

  TGraph *gFdplus = new TGraph(x.size(),&x[0],&qdplus[0]);
  gFdplus->SetLineStyle(3);

  TGraph *gFdminus = new TGraph(x.size(),&x[0],&qdminus[0]);
  gFdminus->SetLineStyle(4);
  gFdminus->SetLineColor(kBlue);
      
  TLegend *qleg = new TLegend(0.6,0.7,0.8,0.9);
  qleg->SetLineColor(10);
  qleg->SetFillColor(10);
  
  TCanvas *c1 = new TCanvas();
  c1->cd(1);
  
  TMultiGraph *mg = new TMultiGraph();
  mg->Add(gFuplus,"c"); qleg->AddEntry(gFuplus,"u^{+}","l");
  mg->Add(gFuminus,"c"); qleg->AddEntry(gFuminus,"u^{-}","l");
  mg->Add(gFdplus,"c"); qleg->AddEntry(gFdplus,"d^{+}","l");
  mg->Add(gFdminus,"c"); qleg->AddEntry(gFdminus,"d^{-}","l");
  mg->Draw("a");
  qleg->Draw("same");
  mg->GetYaxis()->SetTitle("xf(x,Q_{0}^{2})");
  mg->GetYaxis()->CenterTitle();
//  mg->GetYaxis()->SetRangeUser(0.0,0.5);
  mg->GetXaxis()->SetTitle("x");
  mg->GetXaxis()->CenterTitle();
  mg->GetXaxis()->SetLimits(xmin,xmax);
  gPad->SetLogx(1);
  c1->Update();

  //--Anti-Quarks
  TGraph *gFubarplus = new TGraph(x.size(),&x[0],&qubarplus[0]);

  TGraph *gFubarminus = new TGraph(x.size(),&x[0],&qubarminus[0]);
  gFubarminus->SetLineStyle(2);
  gFubarminus->SetLineColor(kBlue);

  TGraph *gFdbarplus = new TGraph(x.size(),&x[0],&qdbarplus[0]);
  gFdbarplus->SetLineStyle(3);

  TGraph *gFdbarminus = new TGraph(x.size(),&x[0],&qdbarminus[0]);
  gFdbarminus->SetLineStyle(4);
  gFdbarminus->SetLineColor(kBlue);

  TLegend *qbarleg = new TLegend(0.6,0.7,0.8,0.9);
  qbarleg->SetLineColor(10);
  qbarleg->SetFillColor(10);

  TCanvas *c2 = new TCanvas();
  c2->cd(1);

  TMultiGraph *mgbar = new TMultiGraph();
  mgbar->Add(gFubarplus,"c"); qbarleg->AddEntry(gFubarplus,"ubar^{+}","l");
  mgbar->Add(gFubarminus,"c"); qbarleg->AddEntry(gFubarminus,"ubar^{-}","l");
  mgbar->Add(gFdbarplus,"c"); qbarleg->AddEntry(gFdbarplus," dbar^{+}","l");
  mgbar->Add(gFdbarminus,"c"); qbarleg->AddEntry(gFdbarminus,"dbar^{-}","l");
  mgbar->Draw("a");
  qbarleg->Draw("same");
  mgbar->GetYaxis()->SetTitle("xf(x,Q_{0}^{2})");
  mgbar->GetYaxis()->CenterTitle();
//  mgbar->GetYaxis()->SetRangeUser(0.0,0.5);
  mgbar->GetXaxis()->SetTitle("x");
  mgbar->GetXaxis()->CenterTitle();
  mgbar->GetXaxis()->SetLimits(xmin,xmax);
  gPad->SetLogx(1);
  c2->Update();

}
