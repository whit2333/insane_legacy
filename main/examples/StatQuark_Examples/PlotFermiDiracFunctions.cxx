//Plots Fermi-Dirac functions for quarks and anti-quarks based on StatisticalQuark fits
#include <vector>
#include "StatisticalQuarkFits.h"
void PlotFermiDiracFunctions()
{

  StatisticalQuarkFits *myStatisticalQuark = new StatisticalQuarkFits();
//  cout << myStatisticalQuark->GetFermiDiracFunc("u",1,0.0001,4.0) << endl;
  

  //--quarks
  vector<Double_t> Fuplus,Fuminus;
  vector<Double_t> Fdplus,Fdminus;
  //-anti-quarks
  vector<Double_t> Fubarplus,Fubarminus;
  vector<Double_t> Fdbarplus,Fdbarminus;

  Double_t Qsq = 4.0; //GeV^2
  Double_t xmin = 0.0001; Double_t xmax = 1.0;
  Double_t dx = 0.0001;
  Double_t ix = xmin;
  vector<Double_t> x;
  
  while(ix < xmax )
    {
      //--Quarks 
      Fuplus.push_back( myStatisticalQuark->GetFermiDiracFunc("u",1,ix,Qsq) );
      Fuminus.push_back( myStatisticalQuark->GetFermiDiracFunc("u",-1,ix,Qsq) );
      Fdplus.push_back( myStatisticalQuark->GetFermiDiracFunc("d",1,ix,Qsq) );
      Fdminus.push_back( myStatisticalQuark->GetFermiDiracFunc("d",-1,ix,Qsq) );      
      //--Anti-Quarks 
      Fubarplus.push_back( myStatisticalQuark->GetFermiDiracFunc("ubar",1,ix,Qsq) );
      Fubarminus.push_back( myStatisticalQuark->GetFermiDiracFunc("ubar",-1,ix,Qsq) );
      Fdbarplus.push_back( myStatisticalQuark->GetFermiDiracFunc("dbar",1,ix,Qsq) );
      Fdbarminus.push_back( myStatisticalQuark->GetFermiDiracFunc("dbar",-1,ix,Qsq) );

      x.push_back(ix);
//      cout << ix << " " << myStatisticalQuark->GetFermiDiracFunc("u",1,ix,Qsq) << endl;
//      ix = xmax;
      ix += dx;
    }
 
  //--Quarks
  TGraph *gFuplus = new TGraph(x.size(),&x[0],&Fuplus[0]);

  TGraph *gFuminus = new TGraph(x.size(),&x[0],&Fuminus[0]);
  gFuminus->SetLineStyle(2);
  gFuminus->SetLineColor(kBlue);

  TGraph *gFdplus = new TGraph(x.size(),&x[0],&Fdplus[0]);
  gFdplus->SetLineStyle(3);

  TGraph *gFdminus = new TGraph(x.size(),&x[0],&Fdminus[0]);
  gFdminus->SetLineStyle(4);
  gFdminus->SetLineColor(kBlue);
      
  TLegend *qleg = new TLegend(0.6,0.7,0.8,0.9);
  qleg->SetLineColor(10);
  qleg->SetFillColor(10);
  
  TCanvas *c1 = new TCanvas();
  c1->cd(1);
  
  TMultiGraph *mg = new TMultiGraph();
  mg->Add(gFuplus,"c"); qleg->AddEntry(gFuplus,"F^{+}_{u}","l");
  mg->Add(gFuminus,"c"); qleg->AddEntry(gFuminus,"F^{-}_{u}","l");
  mg->Add(gFdplus,"c"); qleg->AddEntry(gFdplus,"F^{+}_{d}","l");
  mg->Add(gFdminus,"c"); qleg->AddEntry(gFdminus,"F^{-}_{d}","l");
  mg->Draw("a");
  qleg->Draw("same");
  mg->GetYaxis()->SetTitle("Fermi-Dirac Functions");
  mg->GetYaxis()->CenterTitle();
  mg->GetYaxis()->SetRangeUser(0.0,0.5);
  mg->GetXaxis()->SetTitle("x");
  mg->GetXaxis()->CenterTitle();
  mg->GetXaxis()->SetLimits(xmin,xmax);
  gPad->SetLogx(1);
  c1->Update();

  //--Anti-Quarks
  TGraph *gFubarplus = new TGraph(x.size(),&x[0],&Fubarplus[0]);

  TGraph *gFubarminus = new TGraph(x.size(),&x[0],&Fubarminus[0]);
  gFubarminus->SetLineStyle(2);
  gFubarminus->SetLineColor(kBlue);

  TGraph *gFdbarplus = new TGraph(x.size(),&x[0],&Fdbarplus[0]);
  gFdbarplus->SetLineStyle(3);

  TGraph *gFdbarminus = new TGraph(x.size(),&x[0],&Fdbarminus[0]);
  gFdbarminus->SetLineStyle(4);
  gFdbarminus->SetLineColor(kBlue);

  TLegend *qbarleg = new TLegend(0.6,0.7,0.8,0.9);
  qbarleg->SetLineColor(10);
  qbarleg->SetFillColor(10);

  TCanvas *c2 = new TCanvas();
  c2->cd(1);

  TMultiGraph *mgbar = new TMultiGraph();
  mgbar->Add(gFubarplus,"c"); qbarleg->AddEntry(gFubarplus,"F^{+}_{ubar}","l");
  mgbar->Add(gFubarminus,"c"); qbarleg->AddEntry(gFubarminus,"F^{-}_{ubar}","l");
  mgbar->Add(gFdbarplus,"c"); qbarleg->AddEntry(gFdbarplus,"F^{+}_{dbar}","l");
  mgbar->Add(gFdbarminus,"c"); qbarleg->AddEntry(gFdbarminus,"F^{-}_{dbar}","l");
  mgbar->Draw("a");
  qbarleg->Draw("same");
  mgbar->GetYaxis()->SetTitle("Fermi-Dirac Functions");
  mgbar->GetYaxis()->CenterTitle();
  mgbar->GetYaxis()->SetRangeUser(0.0,0.5);
  mgbar->GetXaxis()->SetTitle("x");
  mgbar->GetXaxis()->CenterTitle();
  mgbar->GetXaxis()->SetLimits(xmin,xmax);
  gPad->SetLogx(1);
  c2->Update();

}
