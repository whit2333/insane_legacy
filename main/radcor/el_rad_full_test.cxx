// WARNING: run this script as: root rad_test.cxx | tee -a dump.dat
//          to see the output in a file. 

#include <cstdlib> 
#include <iostream> 

TString gTargetName;
Double_t gCONV = 1E+3; 

Int_t el_rad_full_test(){

        Int_t MC    = 0;    // 0 = Sample mean; 1 = Accept/reject (not working yet); 2 = Importance sampling   
        Int_t Bin   = 100;   
<<<<<<< HEAD
        Int_t order = 8;
=======
        Int_t order = 6;
>>>>>>> 31c0eaee8a1cf5b4114121292fab8b0e3c06d385
        Int_t N     = Int_t( TMath::Power(10,order) );

        Double_t Es;
        Double_t RadLen[2]; 
        RadLen[0] = 0.002928; 
        RadLen[1] = 0.0362; 

        Double_t Ebeam  = 0.; 
	Double_t Eprime = 1.0;
        Double_t theta  = 0.*degree;  
        Double_t phi    = 0.*degree;  

	Double_t EpMin  = 0.; 
	Double_t EpMax  = 0.; 
	Double_t EP     = EpMin;

        Double_t ZEff  = 19.5260;
        Double_t AEff  = 40.4370;
        Double_t X0Eff = 19.5232;

	Int_t Pass = Int_t(Ebeam); 

        Int_t Target,Units; 
        //cout << "Enter target (0 = proton, 1 = 3He): "; 
        //cin  >> Target;
        Target = 1;
        //cout << "Set units (0 = GeV, 1 = MeV): ";
        //cin  >> Units; 
        Units = 0;

        theta = 45.0*degree;  
        std::cout << " Theta = " << theta/degree << " degrees. " << std::endl;
	if(Target==0){
		cout << "Enter beam pass (1, 5 or 20): ";
		cin  >> Pass;
		theta = 5.0*degree;  
                if(Pass==1)  Ebeam = 1.0;  
                if(Pass==5)  Ebeam = 5.0;  
                if(Pass==20) Ebeam = 20.0;  
                EpMax = 19.000; 
	}else if(Target==1){
		//cout << "Enter beam pass (4 or 5): ";
		//cin  >> Pass; 
                Pass = 5;
                if(Pass==4)  Ebeam = 4.73;  
                if(Pass==5)  Ebeam = 5.89; 
                EpMax = 2.000; 
	} 

        switch(Units){
           case 1: // convert to MeV 
              Ebeam *= gCONV;
              EpMax *= gCONV;  
              EpUnits = Form(" MeV");
              XSUnits = Form(" pb/MeV/sr");
              break;
           default:
              EpUnits = Form(" GeV");
              XSUnits = Form(" nb/GeV/sr");
        }

        vector<double> rEp,rXS; 
        ImportRosetailData(Target,Pass,rEp,rXS); 

	int width = 2;
        TGraph *gROSETAIL = GetTGraph(rEp,rXS); 
	gROSETAIL->SetLineColor(kBlack);
	gROSETAIL->SetMarkerColor(kBlack);
	gROSETAIL->SetMarkerStyle(20);
        gROSETAIL->SetLineWidth(width); 

        InSANENucleus::NucleusType Proton = InSANENucleus::kProton; 
        InSANENucleus::NucleusType He3    = InSANENucleus::k3He;
 
        // Unpolarized structure functions 
        F1F209StructureFunctions *F1F209  = new F1F209StructureFunctions(); 
        // Form factors 
        AmrounFormFactors *amrounFF       = new AmrounFormFactors(); 
        AMTFormFactors *amtFF             = new AMTFormFactors(); 
        InSANEDipoleFormFactors *Dipole   = new InSANEDipoleFormFactors(); 

	InSANERADCORRadiatedDiffXSec *sig = new InSANERADCORRadiatedDiffXSec();
        sig->fRADCOR->DoElastic(true); 
        sig->fRADCOR->SetUnits(Units);   
        sig->fRADCOR->UseInternal();  
        sig->fRADCOR->UseExternal();  
        sig->fRADCOR->SetNumberOfMCEvents(N);
        sig->fRADCOR->SetNumberOfMCBins(Bin);
        sig->fRADCOR->SetMonteCarloIntegrationMethod(MC);  
        sig->SetPolarizationType(0);         // unpolarized 
	sig->SetThreshold(0);                // 0 = elastic; 1 = QE; 2 = pion threshold 
        sig->SetMultiplePhoton(1);           // use multiple photon correction 
	// sig->SetVerbosity(5); 
	if(Target==0){
		sig->fRADCOR->SetTargetType(Proton);             
                sig->fRADCOR->SetFormFactors(Dipole); 
        }else if(Target==1){
		sig->fRADCOR->SetTargetType(He3);              
		sig->fRADCOR->SetTargetFormFactors(amrounFF); 
        } 
        sig->SetBeamEnergy(Ebeam); 
	sig->fRADCOR->SetRadiationLengths(RadLen); 
        sig->fRADCOR->SetEffectiveRadiationLength(X0Eff);
        sig->fRADCOR->SetEffectiveZ(ZEff);
        sig->fRADCOR->SetEffectiveA(AEff);
        sig->InitializePhaseSpaceVariables();
        sig->InitializeFinalStateParticles();
	sig->UseApprox('n');
	sig->fRADCOR->InitializeROOTFile(); 
        InSANEPhaseSpace *ps1 = sig->GetPhaseSpace();
        ps1->GetVariable(0)->SetMaximum(EpMax); 
        ps1->Print(); 
        //ps->ListVariables();
        // Double_t * energy_e1 = ps1->GetVariable("energy_e")->GetCurrentValueAddress();
        // Double_t * theta_e1  = ps1->GetVariable("theta_e")->GetCurrentValueAddress();
        // Double_t * phi_e1    = ps1->GetVariable("phi_e")->GetCurrentValueAddress();
        // (*energy_e1)         = Eprime;
        // (*theta_e1)          = theta;
        // (*phi_e1)            = phi;
        // sig->EvaluateCurrent();
 
	gTargetName = sig->fRADCOR->GetTargetName(); 
 
        vector<Double_t> Ep,XSApprox,XSExact,Err; 

        Double_t par[3]; 
        Double_t sig_approx=0,sig_exact=0;

        TStopwatch *Watch1 = new TStopwatch();  
        TStopwatch *Watch2 = new TStopwatch();  
        Watch2->Start(); 
	Double_t end_time=0;
	int RN = rEp.size();
	double num=0,den=0,arg=0,EP=0;
	for(int i=0;i<1;i++){
		Watch1->Start();
                if(Units==0){
                   EP     = rEp[i];
                }else if(Units==1){
                   EP     = rEp[i]*gCONV;
                }
		par[0] = EP; 
		par[1] = theta;
		par[2] = phi;
		sig_exact = sig->EvaluateXSec(par);
		end_time = Watch1->RealTime();  
                num = TMath::Abs(rXS[i]-sig_exact);
                den = TMath::Abs(rXS[i]+sig_exact); 
                if(den!=0){
                   arg = 100.*num/den;
                }else{
                   arg = 0.;
                }
                Err.push_back(arg);  
		// sig_approx = sig->fRADCOR->sigma_b();  
		// cout << "--> Done." << endl;
		// sig->UseApprox('y'); 
		// sig_approx = sig->EvaluateXSec(par);  
		// cout << "--> Done." << endl;
		cout << "Ep = "         << Form("%.3f",EP)        << EpUnits << "\t" 
		     << "rose  =  "     << Form("%.3E",rXS[i])    << XSUnits << "\t" 
		     << "radcor  =  "   << Form("%.3E",sig_exact) << XSUnits << "\t" 
		     << "pct diff  =  " << Form("%.3f",arg)       << " %"    << "\t" 
		     << "time =  "      << Form("%.3f",end_time)  << " s"    << endl; 
		// << "sig_approx =  " << Form("%.3f",sig_approx) << " nb/GeV/sr" << endl;
		// cout << "------------------------------------------------------------" << endl;
		Ep.push_back(EP); 
		// XSApprox.push_back(sig_approx); 
		XSExact.push_back(sig_exact); 
	}

        return 0; 

        double time = Watch2->RealTime();
        TString Units = Form("s");
        if( (time>minute)&&(time<hour) ){
		time *= 1./minute;
		Units = Form("min.");
        }else if(time>hour){
		time *= 1./hour;
		Units = Form("hr");
        } 
        cout << "Total elapsed time: " << time << " " << Units << endl;

        sig->fRADCOR->WriteTheROOTFile(); 

	// exit(1);

	TString TargetName;
        if(Target==0) TargetName = Form("Proton");
        if(Target==1) TargetName = Form("^{3}He");

        TString Title      = Form("%s Radiated Cross Section (E_{s} = %.2f GeV, #theta = %.0f#circ)",TargetName.Data(),Es,theta/degree);
        TString xAxisTitle = Form("E_{p} (GeV)");
        TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");
      
        TGraph *gRADCOR = GetTGraph(Ep,XSExact); 
	gRADCOR->SetLineColor(kRed);
	gRADCOR->SetMarkerColor(kRed);
	gRADCOR->SetMarkerStyle(20);
        gRADCOR->SetLineWidth(width); 

        TGraph *gErr = GetTGraph(Ep,Err); 
	gErr->SetLineColor(kRed);
	gErr->SetMarkerColor(kRed);
	gErr->SetMarkerStyle(20);
        gErr->SetLineWidth(width); 

        TMultiGraph *G = new TMultiGraph(); 
        G->Add(gRADCOR  ,"c");
        G->Add(gROSETAIL,"c");

        TMultiGraph *E = new TMultiGraph(); 
        E->Add(gErr  ,"c");

        TLegend *L = new TLegend(0.6,0.6,0.8,0.8); 
        L->AddEntry(gRADCOR  ,Form("RADCOR++ (N_{MC} = 10^{%d})",order),"l");
        L->AddEntry(gROSETAIL,Form("ROSETAIL"),"l");

        TString Option = Form("A");

        TCanvas *c1 = new TCanvas("c1","RADCOR Internal + External Tail Test (Exact)",1000,800);
	c1->SetFillColor(kWhite);
        c1->Divide(2,1); 

        c1->cd(1); 
        G->Draw(Option);
	G->SetTitle(Title);
        G->GetXaxis()->SetTitle(xAxisTitle);
        G->GetXaxis()->CenterTitle();
        G->GetYaxis()->SetTitle(yAxisTitle);
        G->GetYaxis()->CenterTitle();
        G->Draw(Option);
        c1->Update();

        c1->cd(2); 
        E->Draw(Option);
	E->SetTitle("Percent Difference");
        E->GetXaxis()->SetTitle(xAxisTitle);
        E->GetXaxis()->CenterTitle();
        E->GetYaxis()->SetTitle("Percent Difference (%)");
        E->GetYaxis()->CenterTitle();
        E->Draw(Option);
        c1->Update();

        WriteToFile(Target,Pass,MC,order,Ep,XSExact); 
        
	return(0);
}
//______________________________________________________________________________
void WriteToFile(Int_t Target,Int_t Pass,Int_t MC,Int_t order,vector<double> Ep,vector<double> XS){

	int N = Ep.size(); 
	TString mc_name,targname;
	switch(MC){
		case 0: 
			mc_name = Form("sm-mc");
			break;
		case 1: 
			mc_name = Form("ar-mc");
			break;
		case 2:
			mc_name = Form("is-mc");
			break;
	}

        if(Target==0) targname = Form("prot");
        if(Target==1) targname = Form("he3");

	TString outpath = Form("./output/MC/%s_eltail_full-exact_%d_%s_%d-pass.dat",targname.Data(),order,mc_name.Data(),Pass);

	ofstream outfile;
	outfile.open(outpath);
	if(outfile.fail()){
		cout << "Cannot open the file: " << outpath << endl;
		exit(1);
	}else{
		for(int i=0;i<N;i++){
			outfile << Form("%.3E",Ep[i]) << "\t" << Form("%.3E",XS[i]) << endl;
		}
		outfile.close();
		cout << "The data has been written to the file: " << outpath << endl;
        }

} 
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]);
	return g;  

}
//______________________________________________________________________________
void ImportRosetailData(int Target,int Pass,vector<double> &Ep,vector<double> &Tail){

        double iEs,iEp,iNu,iW,iT;

        TString inpath;
        if(Target==0) inpath = Form("./output/rosetail/Mo-T_eltail_unpl_%d-pass.out",Pass);
        if(Target==1) inpath = Form("./output/rosetail/%d_SA_eltail_unpl.out",Pass);
        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                cout << "Opening the file: " << inpath << endl;
                while(!infile.eof()){
                        infile >> iEs >> iEp >> iNu >> iW >> iT;
                        Ep.push_back(iEp/gCONV);
                        Tail.push_back(iT);
                }
                infile.close();
                Ep.pop_back();
                Tail.pop_back();
        }

}

