#include <cstdlib> 
#include <iostream> 

Int_t inel_rad_full_test(){

        // MC integration options 
	// MC = method
        // 0  = Sample mean
	// 1  = Accept/reject (not working yet)
	// 2  = Importance sampling (not working yet)
        Int_t MC    = 0;    
        // number of MC bins (for method 1 and 2 above)  
        Int_t Bin   = 100;    
        // How many events? 
        Int_t order = 4;
        Int_t N     = Int_t( TMath::Power(10,order) );

        Double_t Es;
        Double_t RadLen[2]; 
        RadLen[0] = 0.002928; 
        RadLen[1] = 0.0362; 

        Double_t Ebeam  = 0.; 
	Double_t Eprime = 1.0;
        Double_t theta  = 0.*degree;  
        Double_t phi    = 0.*degree;  

	Double_t EpMin  = 0.; 
	Double_t EpMax  = 0.; 
	Double_t EP     = EpMin;

        Double_t ZEff  = 19.5260;
        Double_t AEff  = 40.4370;
        Double_t X0Eff = 19.5232;

        Int_t Thr        = 2; // 0 = elastic, 1 = QE, 2 = pion threshold 
        Int_t Pol        = 0; // 0 = unpolarized, 1 = parallel, 2 = perpendicular
        Bool_t IsElastic = false;   

	Int_t Pass = Int_t(Ebeam); 

        Int_t Target; 
        cout << "Enter target (0 = proton, 1 = 3He): "; 
        cin  >> Target;
	if(Target==0){
		cout << "Enter beam pass (1, 5 or 20): ";
		cin  >> Pass;
		theta = 5.0*degree;  
                if(Pass==1)  Ebeam = 1.0;  
                if(Pass==5)  Ebeam = 5.0;  
                if(Pass==20) Ebeam = 20.0;  
	}else if(Target==1){
		cout << "Enter beam pass (4 or 5): ";
		cin  >> Pass; 
		theta = 45.0*degree;  
                if(Pass==4)  Ebeam = 4.73;  
                if(Pass==5)  Ebeam = 5.89;  
	} 

        vector<double> rEp,rXS; 
        ImportRosetailData(Target,Pass,rEp,rXS); 

        InSANENucleus::NucleusType Proton = InSANENucleus::kProton; 
        InSANENucleus::NucleusType He3    = InSANENucleus::k3He;
 
        // Unpolarized structure functions 
        F1F209StructureFunctions *F1F209  = new F1F209StructureFunctions(); 
        // Form factors 
        AmrounFormFactors *amrounFF       = new AmrounFormFactors(); 
        AMTFormFactors *amtFF             = new AMTFormFactors(); 
        InSANEDipoleFormFactors *Dipole   = new InSANEDipoleFormFactors(); 

	InSANERADCORRadiatedDiffXSec *sig_exact = new InSANERADCORRadiatedDiffXSec();
        sig_exact->fRADCOR->DoElastic(IsElastic);  
        sig_exact->fRADCOR->SetNumberOfMCEvents(N);
        sig_exact->fRADCOR->SetNumberOfMCBins(Bin);
        sig_exact->fRADCOR->SetMonteCarloIntegrationMethod(MC);  
        sig_exact->fRADCOR->SetPolarization(Pol);          
	sig_exact->fRADCOR->SetThreshold(Thr);                 
        sig_exact->fRADCOR->UseMultiplePhoton();
	// sig_exact->SetVerbosity(5); 
	if(Target==0){
		sig_exact->fRADCOR->SetTargetType(Proton);             
		sig_exact->fRADCOR->SetFormFactors(Dipole); 
	}else if(Target==1){
		sig_exact->fRADCOR->SetTargetType(He3);             
		sig_exact->fRADCOR->SetFormFactors(amrounFF); 
	} 
        sig_exact->SetBeamEnergy(Ebeam); 
	sig_exact->fRADCOR->SetRadiationLengths(RadLen); 
        sig_exact->fRADCOR->SetEffectiveRadiationLength(X0Eff);
        sig_exact->fRADCOR->SetEffectiveZ(ZEff);
        sig_exact->fRADCOR->SetEffectiveA(AEff);
        sig_exact->InitializePhaseSpaceVariables();
        sig_exact->InitializeFinalStateParticles();
	sig_exact->UseApprox('n');
	sig_exact->fRADCOR->InitializeROOTFile(); 
        InSANEPhaseSpace *ps1 = sig_exact->GetPhaseSpace();
        ps1->GetVariable("energy_e")->SetVariableMaxima(25.0); 
        // ps1->Print(); 
        // ps1->ListVariables();
        Double_t * energy_e1 = ps1->GetVariable("energy_e")->GetCurrentValueAddress();
        Double_t * theta_e1  = ps1->GetVariable("theta_e")->GetCurrentValueAddress();
        Double_t * phi_e1    = ps1->GetVariable("phi_e")->GetCurrentValueAddress();
        (*energy_e1)         = Eprime;
        (*theta_e1)          = theta;
        (*phi_e1)            = phi;
        // sig_exact->EvaluateCurrent();
 
	InSANERADCORRadiatedDiffXSec *sig_approx = new InSANERADCORRadiatedDiffXSec();
        sig_approx->fRADCOR->DoElastic(IsElastic);  
        sig_approx->fRADCOR->SetNumberOfMCEvents(N);
        sig_approx->fRADCOR->SetNumberOfMCBins(Bin);
        sig_approx->fRADCOR->SetMonteCarloIntegrationMethod(MC);  
        sig_approx->fRADCOR->SetPolarization(Pol);         // unpolarized 
	sig_approx->fRADCOR->SetThreshold(Thr);           
        sig_approx->fRADCOR->UseMultiplePhoton();
 	// sig_exact->SetVerbosity(3); 
	if(Target==0){
		sig_approx->fRADCOR->SetTargetType(Proton);             
		sig_approx->fRADCOR->SetFormFactors(Dipole); 
	}else if(Target==1){
		sig_approx->fRADCOR->SetTargetType(He3);             
		sig_approx->fRADCOR->SetFormFactors(amrounFF); 
	} 
        sig_approx->SetBeamEnergy(Ebeam); 
	sig_approx->fRADCOR->SetRadiationLengths(RadLen); 
        sig_approx->fRADCOR->SetEffectiveRadiationLength(X0Eff);
        sig_approx->fRADCOR->SetEffectiveZ(ZEff);
        sig_approx->fRADCOR->SetEffectiveA(AEff);
        sig_approx->InitializePhaseSpaceVariables();
        sig_approx->InitializeFinalStateParticles();
	sig_approx->UseApprox('y'); 
        InSANEPhaseSpace *ps2 = sig_approx->GetPhaseSpace();
        ps2->GetVariable("energy_e")->SetVariableMaxima(25.0); 
        // ps2->Print(); 
        // ps2->ListVariables();
        Double_t * energy_e2 = ps2->GetVariable("energy_e")->GetCurrentValueAddress();
        Double_t * theta_e2  = ps2->GetVariable("theta_e")->GetCurrentValueAddress();
        Double_t * phi_e2    = ps2->GetVariable("phi_e")->GetCurrentValueAddress();
        (*energy_e2)         = Eprime;
        (*theta_e2)          = theta;
        (*phi_e2)            = phi;
        // sig_approx->EvaluateCurrent();
 
        vector<Double_t> Ep,XSApprox,XSExact,Err; 

        Double_t par[3]; 
        Double_t sig_app=0,sig_ex=0;

        TStopwatch *Watch1 = new TStopwatch();  
        TStopwatch *Watch2 = new TStopwatch();  
        Watch2->Start(); 
	Double_t end_time=0;
	int RN = rEp.size();
	double num=0,den=0,arg=0;
	for(int i=0;i<1;i++){
		Watch1->Start();
		par[0]   = rEp[i]; 
		par[1]   = theta;
		par[2]   = phi;
		sig_app  = sig_approx->EvaluateXSec(par);
		sig_ex   = sig_exact->EvaluateXSec(par);
		end_time = Watch1->RealTime();  
                num = TMath::Abs(sig_app-sig_ex);
                den = TMath::Abs(sig_app+sig_ex); 
                arg = 100.*num/den; 
                Err.push_back(arg);  
		// sig_approx = sig->fRADCOR->sigma_b();  
		// cout << "--> Done." << endl;
		// sig->UseApprox('y'); 
		// sig_approx = sig->EvaluateXSec(par);  
		// cout << "--> Done." << endl;
		cout << "Ep = "         << Form("%.3f",rEp[i])    << " GeV"       << "\t" 
		     << "peak =  "      << Form("%.3E",sig_app)   << " nb/GeV/sr" << "\t" 
		     << "exact  =  "    << Form("%.3E",sig_ex)    << " nb/GeV/sr" << "\t" 
		     << "pct diff  =  " << Form("%.3f",arg)       << " %"         << "\t" 
		     << "time =  "      << Form("%.3f",end_time)  << " s"         << endl; 
		// << "sig_approx =  " << Form("%.3f",sig_approx) << " nb/GeV/sr" << endl;
		// cout << "------------------------------------------------------------" << endl;
		Ep.push_back(rEp[i]); 
		XSApprox.push_back(sig_app); 
		XSExact.push_back(sig_ex); 
	}

        double time = Watch2->RealTime();
        TString Units = Form("s");
        if( (time>minute)&&(time<hour) ){
		time *= 1./minute;
		Units = Form("min.");
        }else if(time>hour){
		time *= 1./hour;
		Units = Form("hr");
        } 
        cout << "Total elapsed time: " << time << " " << Units << endl;

        sig_exact->fRADCOR->WriteTheROOTFile();

	exit(1);

	TString TargetName;
        if(Target==0) TargetName = Form("Proton");
        if(Target==1) TargetName = Form("^{3}He");

        TString Title      = Form("%s Radiated Cross Section (E_{s} = %.2f GeV, #theta = %.0f#circ)",TargetName.Data(),Es,theta/degree);
        TString xAxisTitle = Form("E_{p} (GeV)");
        TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");
     
        int width = 2; 
        TGraph *gExact = GetTGraph(Ep,XSExact); 
	gExact->SetLineColor(kRed);
	gExact->SetMarkerColor(kRed);
	gExact->SetMarkerStyle(20);
        gExact->SetLineWidth(width); 

        TGraph *gApprox = GetTGraph(Ep,XSExact); 
	gApprox->SetLineColor(kBlue);
	gApprox->SetMarkerColor(kBlue);
	gApprox->SetMarkerStyle(20);
        gApprox->SetLineWidth(width); 

        TGraph *gErr = GetTGraph(Ep,Err); 
	gErr->SetLineColor(kRed);
	gErr->SetMarkerColor(kRed);
	gErr->SetMarkerStyle(20);
        gErr->SetLineWidth(width); 

        TMultiGraph *G = new TMultiGraph(); 
        G->Add(gExact ,"c");
        G->Add(gApprox,"c");

        TMultiGraph *E = new TMultiGraph(); 
        E->Add(gErr  ,"c");

        TLegend *L = new TLegend(0.6,0.6,0.8,0.8); 
        L->AddEntry(gExact ,Form("Exact (N_{MC} = 10^{%d})",order),"l");
        L->AddEntry(gApprox,Form("Peaking Approx.")               ,"l");

        TString Option = Form("A");

        TCanvas *c1 = new TCanvas("c1","RADCOR Internal + External Tail Test (Exact)",1000,800);
	c1->SetFillColor(kWhite);
        c1->Divide(2,1); 

        c1->cd(1); 
        G->Draw(Option);
	G->SetTitle(Title);
        G->GetXaxis()->SetTitle(xAxisTitle);
        G->GetXaxis()->CenterTitle();
        G->GetYaxis()->SetTitle(yAxisTitle);
        G->GetYaxis()->CenterTitle();
        G->Draw(Option);
        c1->Update();

        c1->cd(2); 
        E->Draw(Option);
	E->SetTitle("Percent Difference");
        E->GetXaxis()->SetTitle(xAxisTitle);
        E->GetXaxis()->CenterTitle();
        E->GetYaxis()->SetTitle("Percent Difference (%)");
        E->GetYaxis()->CenterTitle();
        E->Draw(Option);
        c1->Update();

        WriteToFile(Target,Pass,MC,order,Ep,XSExact); 
        
	return(0);
}
//______________________________________________________________________________
void WriteToFile(Int_t Target,Int_t Pass,Int_t MC,Int_t order,vector<double> Ep,vector<double> XS){

	int N = Ep.size(); 
	TString mc_name,targname;
	switch(MC){
		case 0: 
			mc_name = Form("sm-mc");
			break;
		case 1: 
			mc_name = Form("ar-mc");
			break;
		case 2:
			mc_name = Form("is-mc");
			break;
	}

        if(Target==0) targname = Form("prot");
        if(Target==1) targname = Form("he3");

	TString outpath = Form("./output/MC/%s_intail_full-exact_%d_%s_%d-pass.dat",targname.Data(),order,mc_name.Data(),Pass);

	ofstream outfile;
	outfile.open(outpath);
	if(outfile.fail()){
		cout << "Cannot open the file: " << outpath << endl;
		exit(1);
	}else{
		for(int i=0;i<N;i++){
			outfile << Form("%.3E",Ep[i]) << "\t" << Form("%.3E",XS[i]) << endl;
		}
		outfile.close();
		cout << "The data has been written to the file: " << outpath << endl;
        }

} 
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]);
	return g;  

}
//______________________________________________________________________________
void ImportRosetailData(int Target,int Pass,vector<double> &Ep,vector<double> &Tail){

        double CONV = 1E+3;
        double iEs,iEp,iNu,iW,iT;

        TString inpath;
        if(Target==0) inpath = Form("./output/rosetail/Mo-T_eltail_unpl_%d-pass.out",Pass);
        if(Target==1) inpath = Form("./output/rosetail/%d_SA_eltail_unpl.out",Pass);
        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                cout << "Opening the file: " << inpath << endl;
                while(!infile.eof()){
                        infile >> iEs >> iEp >> iNu >> iW >> iT;
                        Ep.push_back(iEp/CONV);
                        Tail.push_back(iT);
                }
                infile.close();
                Ep.pop_back();
                Tail.pop_back();
        }

}

