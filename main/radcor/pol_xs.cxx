// WARNING: run this script as: root rad_test.cxx | tee -a dump.dat
//          to see the output in a file. 

#include <cstdlib> 
#include <iostream> 

Int_t pol_xs(){

        Double_t Es,th; 
        Double_t RadLen[3]; 
 
        Int_t npar        = 2; 
        Double_t Ebeam    = 5.89;
        Double_t Eprime   = 1.0;  
        Double_t EpMin    = 0.5; 
        Double_t EpMax    = 2.0;
        Double_t theta    = 45.*degree;
        Double_t phi      = 0.*degree;

        /// Parameters 
        Int_t width     = 2; 
        Int_t Verbosity = 0; 
        Int_t MP        = 1;    // multiple photons 
        Int_t Threshold = 1;    // 0 = elastic; 1 = QE; 2 = pion threshold 
        char Choice     = 'y';  // for peaking approximation 
       
        RadLen[0] = 0.002928; 
        RadLen[1] = 0.0362; 

        InSANENucleus::NucleusType Target = InSANENucleus::k3He; 
 
        /// Polarized PDFs
        DSSVPolarizedPDFs *DSSV   = new DSSVPolarizedPDFs(); 
        DNS2005PolarizedPDFs *DNS = new DNS2005PolarizedPDFs(); 
        /// Polarized structure functions 
        InSANEPolarizedStructureFunctionsFromPDFs *PolSFs = new InSANEPolarizedStructureFunctionsFromPDFs(); 
        PolSFs->SetPolarizedPDFs(DNS);

        /// Cross section objects 
	InSANEPolarizedCrossSectionDifference *PXSPara = new InSANEPolarizedCrossSectionDifference();
        PXSPara->SetPolarizationType(1); // parallel 
	PXSPara->SetTargetType(Target);
        PXSPara->SetBeamEnergy(Ebeam); 
        PXSPara->SetPolarizedStructureFunctions(PolSFs);  
	PXSPara->InitializePhaseSpaceVariables();
	PXSPara->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps1 = PXSPara->GetPhaseSpace();
	ps1->Print();
	Double_t * energy_e1 = ps1->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e1  = ps1->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e1    = ps1->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e1)         = Eprime;
	(*theta_e1)          = theta;
	(*phi_e1)            = phi;
	PXSPara->EvaluateCurrent();

	InSANEPolarizedCrossSectionDifference *PXSPerp = new InSANEPolarizedCrossSectionDifference();
        PXSPerp->SetPolarizationType(2); // perpendicular
	PXSPerp->SetTargetType(Target);
        PXSPerp->SetBeamEnergy(Ebeam); 
        PXSPerp->SetPolarizedStructureFunctions(PolSFs);  
	PXSPerp->InitializePhaseSpaceVariables();
	PXSPerp->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps2 = PXSPerp->GetPhaseSpace();
	ps2->Print();
	Double_t * energy_e2 = ps2->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e2  = ps2->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e2    = ps2->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e2)         = Eprime;
	(*theta_e2)          = theta;
	(*phi_e2)            = phi;
	PXSPerp->EvaluateCurrent();
        //PXSPerp->Print();

        /// radiated cross sections (internal only) 
	InSANERADCORRadiatedDiffXSec *sig_rad_para_int = new InSANERADCORRadiatedDiffXSec();
        sig_rad_para_int->SetPolarizedCrossSectionDifference(PXSPara); 
        sig_rad_para_int->SetPolarizationType(1);         // parallel 
        sig_rad_para_int->UseApprox(Choice); 
	sig_rad_para_int->SetThreshold(Threshold);         
        sig_rad_para_int->SetMultiplePhoton(MP);           // use multiple photon correction 
	sig_rad_para_int->SetVerbosity(Verbosity); 
	sig_rad_para_int->fRADCOR->SetTargetType(Target);              
        sig_rad_para_int->SetBeamEnergy(Ebeam); 
	sig_rad_para_int->InitializePhaseSpaceVariables();
	sig_rad_para_int->InitializeFinalStateParticles();
        sig_rad_para_int->Print();
	InSANEPhaseSpace *ps3 = sig_rad_para_int->GetPhaseSpace();
	ps3->Print();

	Double_t * energy_e3 = ps3->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e3  = ps3->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e3    = ps3->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e3)         = Eprime;
	(*theta_e3)          = theta;
	(*phi_e3)            = phi;
	sig_rad_para_int->EvaluateCurrent();

	InSANERADCORRadiatedDiffXSec *sig_rad_perp_int = new InSANERADCORRadiatedDiffXSec();
        sig_rad_perp_int->SetPolarizedCrossSectionDifference(PXSPerp); 
        sig_rad_perp_int->SetPolarizationType(2);         // perpendicular 
        sig_rad_perp_int->UseApprox(Choice); 
	sig_rad_perp_int->SetThreshold(Threshold);         
        sig_rad_perp_int->SetMultiplePhoton(MP);           // use multiple photon correction 
	sig_rad_perp_int->SetVerbosity(Verbosity); 
	sig_rad_perp_int->fRADCOR->SetTargetType(Target);               
        sig_rad_perp_int->SetBeamEnergy(Ebeam);  
	sig_rad_perp_int->InitializePhaseSpaceVariables();
	sig_rad_perp_int->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps4 = sig_rad_perp_int->GetPhaseSpace();
	//ps->ListVariables();
	Double_t * energy_e4 = ps4->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e4  = ps4->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e4    = ps4->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e4)         = Eprime;
	(*theta_e4)          = theta;
	(*phi_e4)            = phi;
	sig_rad_perp_int->EvaluateCurrent();

        // cout << "phi = " << (*phi_e3) << endl;
        // cout << "phi = " << (*phi_e4) << endl;

        // exit(1); 

	Double_t ZERO[2] = {0.,0.};
	TString TargetName = sig_rad_para_int->fRADCOR->GetTargetName(); 
	sig_rad_para_int->fRADCOR->SetRadiationLengths(ZERO); 
	sig_rad_perp_int->fRADCOR->SetRadiationLengths(ZERO); 
 
	sig_rad_para_int->fRADCOR->Print();

        /// radiated cross sections (int + ext) 
	InSANERADCORRadiatedDiffXSec *sig_rad_para_full = new InSANERADCORRadiatedDiffXSec();
        sig_rad_para_full->SetPolarizedCrossSectionDifference(PXSPara); 
        sig_rad_para_full->SetPolarizationType(1);         // parallel 
        sig_rad_para_full->UseApprox(Choice); 
	sig_rad_para_full->SetThreshold(Threshold);         
        sig_rad_para_full->SetMultiplePhoton(MP);           // use multiple photon correction 
	sig_rad_para_full->SetVerbosity(Verbosity); 
	sig_rad_para_full->fRADCOR->SetTargetType(Target);              
        sig_rad_para_full->SetBeamEnergy(Ebeam); 
	sig_rad_para_full->InitializePhaseSpaceVariables();
	sig_rad_para_full->InitializeFinalStateParticles();
        sig_rad_para_full->Print();
	InSANEPhaseSpace *ps5 = sig_rad_para_full->GetPhaseSpace();
	ps3->Print();

	Double_t * energy_e5 = ps5->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e5  = ps5->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e5    = ps5->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e5)         = Eprime;
	(*theta_e5)          = theta;
	(*phi_e5)            = phi;
	sig_rad_para_full->EvaluateCurrent();

	InSANERADCORRadiatedDiffXSec *sig_rad_perp_full = new InSANERADCORRadiatedDiffXSec();
        sig_rad_perp_full->SetPolarizedCrossSectionDifference(PXSPerp); 
        sig_rad_perp_full->SetPolarizationType(2);         // perpendicular 
        sig_rad_perp_full->UseApprox(Choice); 
	sig_rad_perp_full->SetThreshold(Threshold);         
        sig_rad_perp_full->SetMultiplePhoton(MP);           // use multiple photon correction 
	sig_rad_perp_full->SetVerbosity(Verbosity); 
	sig_rad_perp_full->fRADCOR->SetTargetType(Target);               
        sig_rad_perp_full->SetBeamEnergy(Ebeam);  
	sig_rad_perp_full->InitializePhaseSpaceVariables();
	sig_rad_perp_full->InitializeFinalStateParticles();
	InSANEPhaseSpace *ps6 = sig_rad_perp_full->GetPhaseSpace();
	//ps->ListVariables();
	Double_t * energy_e6 = ps6->GetVariable("energy_e")->GetCurrentValueAddress();
	Double_t * theta_e6  = ps6->GetVariable("theta_e")->GetCurrentValueAddress();
	Double_t * phi_e6    = ps6->GetVariable("phi_e")->GetCurrentValueAddress();
	(*energy_e6)         = Eprime;
	(*theta_e6)          = theta;
	(*phi_e6)            = phi;
	sig_rad_perp_full->EvaluateCurrent();

	sig_rad_para_full->fRADCOR->SetRadiationLengths(RadLen); 
	sig_rad_perp_full->fRADCOR->SetRadiationLengths(RadLen); 

        /// TF1 objects 	
        TF1 *ParaBorn = new TF1("PolXSDiffPara",PXSPara,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             EpMin,EpMax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
        ParaBorn->SetParameter(0,theta);
        ParaBorn->SetParameter(1,phi);
        ParaBorn->SetLineWidth(width);
        ParaBorn->SetLineColor(kBlack); 

        TF1 *PerpBorn = new TF1("PolXSDiffPerp",PXSPerp,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             EpMin,EpMax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
        PerpBorn->SetParameter(0,theta);
        PerpBorn->SetParameter(1,phi);
        PerpBorn->SetLineWidth(width);
        PerpBorn->SetLineColor(kBlack); 

        TF1 *ParaRadInt = new TF1("PolXSDiffParaRad",sig_rad_para_int,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             EpMin,EpMax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
        ParaRadInt->SetParameter(0,theta);
        ParaRadInt->SetParameter(1,phi);
        ParaRadInt->SetLineWidth(width);
        ParaRadInt->SetLineColor(kBlue); 

        TF1 *PerpRadInt = new TF1("PolXSDiffPerpRad",sig_rad_perp_int,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             EpMin,EpMax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
        PerpRadInt->SetParameter(0,theta);
        PerpRadInt->SetParameter(1,phi);
        PerpRadInt->SetLineWidth(width);
        PerpRadInt->SetLineColor(kBlue); 

        TF1 *ParaRadFull = new TF1("PolXSDiffParaRad",sig_rad_para_full,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             EpMin,EpMax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
        ParaRadFull->SetParameter(0,theta);
        ParaRadFull->SetParameter(1,phi);
        ParaRadFull->SetLineWidth(width);
        ParaRadFull->SetLineColor(kRed); 

        TF1 *PerpRadFull = new TF1("PolXSDiffPerpRad",sig_rad_perp_full,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             EpMin,EpMax,npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
        PerpRadFull->SetParameter(0,theta);
        PerpRadFull->SetParameter(1,phi);
        PerpRadFull->SetLineWidth(width);
        PerpRadFull->SetLineColor(kRed); 

        TLegend *L = new TLegend(0.6,0.6,0.8,0.8);
        L->AddEntry(ParaBorn   ,"Born"                          ,"l"); 
        L->AddEntry(ParaRadInt ,"Radiated (internal only)"      ,"l"); 
        L->AddEntry(ParaRadFull,"Radiated (internal + external)","l"); 

        TString Details   = Form("DSSV, RADCOR");
        TString ParaTitle = Form("#Delta#sigma_{#parallel} (%s)",Details.Data());
        TString PerpTitle = Form("#Delta#sigma_{#perp} (%s)",Details.Data());

        TCanvas *c1 = new TCanvas("c1","Polarized Cross Section Differences",1000,800);
	c1->SetFillColor(kWhite);
        c1->Divide(2,1); 

	Double_t yMin = -1; 
	Double_t yMax =  1; 

        c1->cd(1); 
        ParaBorn->Draw();
        ParaBorn->SetTitle(ParaTitle);
	ParaBorn->GetXaxis()->SetTitle("E'");
	ParaBorn->GetXaxis()->CenterTitle(true);
	ParaBorn->GetYaxis()->SetTitle(PXSPara->GetLabel());
	ParaBorn->GetYaxis()->CenterTitle(true);
        ParaBorn->GetYaxis()->SetRangeUser(-0.6,0.2); 
	ParaRadInt->Draw("same");
	ParaRadFull->Draw("same");
        L->Draw("same");

        c1->cd(2); 
        PerpBorn->Draw();
        PerpBorn->SetTitle(PerpTitle);
	PerpBorn->GetXaxis()->SetTitle("E'");
	PerpBorn->GetXaxis()->CenterTitle(true);
	PerpBorn->GetYaxis()->SetTitle(PXSPerp->GetLabel());
	PerpBorn->GetYaxis()->CenterTitle(true);
        PerpBorn->GetYaxis()->SetRangeUser(-0.05,0.05); 
	PerpRadInt->Draw("same");
	PerpRadFull->Draw("same");
        c1->Update();

}

