// WARNING: run this script as: root rad_test.cxx | tee -a dump.dat
//          to see the output in a file. 

#include <cstdlib> 
#include <iostream> 

Double_t hbarc2  = 0.389379E+06; // (hbar*c)^2 in units of GeV^2*nb 
TString gTargetName;

Int_t exact_and_peaking(){

        Double_t Es,th; 
        int Target = 3; // 3He

        Double_t RadLen[3]; 
  
	InSANERADCORInternalUnpolarizedDiffXSec *sig_int = new InSANERADCORInternalUnpolarizedDiffXSec();
	// sig_int->SetThreshold(1);                // QE threshold 
        sig_int->SetMultiplePhoton(1);           // use multiple photon correction 
	sig_int->SetVerbosity(0); 
	sig_int->fRADCOR->SetTargetType(Target);               

	InSANERADCORRadiatedUnpolarizedDiffXSec *sig_rad = new InSANERADCORRadiatedUnpolarizedDiffXSec();
        sig_rad->UseApprox('y'); 
	sig_rad->SetThreshold(1);                // QE threshold 
        sig_rad->SetMultiplePhoton(1);           // use multiple photon correction 
	sig_rad->SetVerbosity(0); 
	sig_rad->fRADCOR->SetTargetType(Target);               

        GetInput(Es,th,RadLen); 

	TargetName = sig_rad->fRADCOR->GetTargetName(); 
	// sig->fRADCOR->SetScatteringAngle(gth); 
	sig_int->fRADCOR->SetRadiationLengths(RadLen); 
	sig_rad->fRADCOR->SetRadiationLengths(RadLen); 
 
	//sig_rad->fRADCOR->Print();

        vector<Double_t> Ep,XSApprox,XSExact; 

        Int_t N = 40; 

	Double_t EpMin  = 0.600;
	Double_t EpMax  = 2.000; 
	Double_t EP     = EpMin;
	Double_t deltap = 0.01;

        Double_t par[3]; 
        Double_t sig_approx=0,sig_exact=0;

	while(EP<=EpMax){
		par[0] = Es;
		par[1] = EP; 
		par[2] = th;
		sig_exact  = hbarc2*sig_int->EvaluateXSec(par);  
		sig_approx = sig_rad->EvaluateXSec(par);  
		cout << "Ep = "           << Form("%.2f",EP)          << "\t" 
		     << "sig_exact  =  "  << Form("%.3f",sig_exact)   << " nb/GeV/sr" << "\t" 
		     << "sig_approx  =  " << Form("%.3f",sig_approx)  << " nb/GeV/sr" << endl;
		Ep.push_back(EP); 
		XSApprox.push_back(sig_approx); 
		XSExact.push_back(sig_exact); 
		EP     += deltap;
	}

        PlotAll(Es,th,Ep,XSApprox,XSExact); 
        
	return(0);
}
//______________________________________________________________________________
void Plot(Double_t Es,Double_t th,vector<Double_t> Ep,vector<Double_t> XS){

        TString Title      = Form("%s Internally-Radiated Tail (E_{s} = %.2f GeV, #theta = %.0f#circ)",gTargetName.Data(),Es,th);
        TString xAxisTitle = Form("E_{p} (GeV)");
        TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");
       
        const Int_t NN = Ep.size(); 
        TGraph *g = new TGraph(NN,&Ep[0],&XS[0]); 
	g->SetMarkerColor(kBlack);
	g->SetMarkerStyle(20);

        TCanvas *c1 = new TCanvas("c1","RADCOR Internal Tail Test (Exact)",1000,800);
	c1->SetFillColor(kWhite);

        c1->cd(); 
        g->Draw("AP");
	g->SetTitle(Title);
        g->GetXaxis()->SetTitle(xAxisTitle);
        g->GetXaxis()->CenterTitle();
        g->GetYaxis()->SetTitle(yAxisTitle);
        g->GetYaxis()->CenterTitle();
        g->Draw("AP");
        c1->Update();


}
//______________________________________________________________________________
void PlotAll(Double_t Es,Double_t th,vector<Double_t> Ep,vector<Double_t> XSA,vector<Double_t> XSE){

	// XSA = approx, XSE = exact 

        TString Title      = Form("%s Internally-Radiated Cross Section (E_{s} = %.2f GeV, #theta = %.0f#circ)",gTargetName.Data(),Es,th/degree);
        TString xAxisTitle = Form("E_{p} (GeV)");
        TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");
       
	int width = 2;

        const Int_t NN = Ep.size(); 
        TGraph *approx = new TGraph(NN,&Ep[0],&XSA[0]); 
	approx->SetMarkerColor(kBlue);
	approx->SetLineColor(kBlue);
	approx->SetMarkerStyle(20);
	approx->SetLineWidth(width);

        TGraph *exact = new TGraph(NN,&Ep[0],&XSE[0]); 
	exact->SetMarkerColor(kRed);
	exact->SetLineColor(kRed);
	exact->SetMarkerStyle(20);
	exact->SetLineWidth(width);

        TMultiGraph *G = new TMultiGraph();
	G->Add(approx,"c");
	G->Add(exact ,"c");

        TLegend *L = new TLegend(0.6,0.6,0.8,0.8);
        L->SetFillColor(kWhite); 
        L->AddEntry(approx,"Energy Peaking Approximation","l"); 
        L->AddEntry(exact ,"Exact"                       ,"l"); 

        TCanvas *c1 = new TCanvas("c1","RADCOR Internal Tail Test (Exact)",1000,800);
	c1->SetFillColor(kWhite);

	TString DrawOption = Form("AC");

        c1->cd(); 
        G->Draw(DrawOption);
	G->SetTitle(Title);
        G->GetXaxis()->SetTitle(xAxisTitle);
        G->GetXaxis()->CenterTitle();
        G->GetYaxis()->SetTitle(yAxisTitle);
        G->GetYaxis()->CenterTitle();
        G->Draw(DrawOption);
	L->Draw("same");
        c1->Update();


}
//______________________________________________________________________________
void GetInput(Double_t &Es,Double_t &th,Double_t *RadLen){

        Es = 5.89; 
        th = 45*degree; 
        RadLen[0] = 0.; // 0.002928; 
        RadLen[1] = 0.; // 0.0362; 

        // cout << "Enter beam energy (GeV): "; 
        // cin  >> gEs; 
        // cout << "Enter scattering angle (deg): "; 
        // cin  >> gth;
        // cout << "Enter radiation length before scattering (#X0): ";
        // cin  >> RadLen[0];  
        // cout << "Enter radiation length after scattering (#X0): ";
        // cin  >> RadLen[1];

}

