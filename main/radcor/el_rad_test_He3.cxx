// WARNING: run this script as: root rad_test.cxx | tee -a dump.dat
//          to see the output in a file. 

#include <cstdlib> 
#include <iostream> 

int fDebug=1; 
TString gTargetName;

Int_t el_rad_test_He3(){

        Int_t order = 4;
        Int_t MC    = Int_t( TMath::Power(10,order) );

        Double_t Es;
        Double_t RadLen[2]; 
        RadLen[0] = 0.002928; 
        RadLen[1] = 0.0362; 

        Double_t Ebeam  = 0.; 
	Double_t Eprime = 0.;
        Double_t theta  = 45.*degree;  
        Double_t phi    = 0.*degree;  

        Int_t Pass      = 0.;  
 	Double_t EpMin  = 0.; 
        Double_t EpMax  = 0.; 

        cout << "Enter beam pass (4 or 5): "; 
        cin  >> Pass; 

        switch(Pass){
		case 4: 
			Ebeam  = 4.73; 
			Eprime = 1.0; 
			EpMin  = 0.55;
			EpMax  = 2.00;
			break;
		case 5: 
			Ebeam  = 5.89; 
			Eprime = 1.0; 
			EpMin  = 0.55; 
			EpMax  = 2.00; 
			break; 
        } 
       
	vector<double> rEp,rXS; 

        ImportRosetailData(Pass,rEp,rXS); 

	int width = 2;
	TGraph *gRosetail = GetTGraph(rEp,rXS);        
        gRosetail->SetMarkerStyle(20); 
        gRosetail->SetMarkerColor(kBlack);  
        gRosetail->SetLineColor(kBlack);  
        gRosetail->SetLineWidth(width);  

        InSANENucleus::NucleusType Target = InSANENucleus::k3He; 

        F1F209eInclusiveDiffXSec *XS      = new F1F209eInclusiveDiffXSec(); 
        XS->UseModifiedModel('y'); 

        /// form factors 
        AMTFormFactors *AMT             = new AMTFormFactors(); 
        InSANEDipoleFormFactors *Dipole = new InSANEDipoleFormFactors(); 
        AmrounFormFactors *Amroun       = new AmrounFormFactors(); 
        
        /// structure functions 
        F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 
        NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
        CTEQ6UnpolarizedPDFs *CTEQ       = new CTEQ6UnpolarizedPDFs();

	InSANEStructureFunctionsFromPDFs *UnpolSFs = new InSANEStructureFunctionsFromPDFs();
	UnpolSFs->SetUnpolarizedPDFs(CTEQ);  

        InSANERADCOR *RC1 = new InSANERADCOR();
        InSANERADCOR *RC2 = new InSANERADCOR();
        RC1->DoElastic(true); 
        RC2->DoElastic(true); 

        RC1->UseInternal();
        RC1->UseExternal(false); 
        RC2->UseInternal();
        RC2->UseExternal(false); 

        // RC1->SetVerbosity(5); 
        // RC2->SetVerbosity(5); 

        RC1->SetNumberOfMCEvents(MC);
        RC2->SetNumberOfMCEvents(MC);

        RC1->SetUnpolarizedCrossSection(XS); 
        RC2->SetUnpolarizedCrossSection(XS); 

        RC1->SetTargetType(Target); 
        RC2->SetTargetType(Target); 

	RC1->SetTargetFormFactors(Amroun);
	RC1->SetUnpolarizedStructureFunctions(F1F209); 
	RC2->SetTargetFormFactors(Amroun);
	RC2->SetUnpolarizedStructureFunctions(F1F209); 

        RC1->SetKinematicsForExactInternal(Ebeam,Eprime,theta);
        RC2->SetKinematicsForExactInternal(Ebeam,Eprime,theta);

        // RC2->InitializeROOTFile(); 

        Double_t COSTHK     = -0.5;     // limits are -1 to 1  
        Double_t costhk_min = -1; 
        Double_t costhk_max =  1; 

        cout << "For integrand plot: "  << endl; 
	cout << "Es = " << Ebeam        << endl;
	cout << "Ep = " << Eprime       << endl;
	cout << "th = " << theta/degree << endl;

	Int_t Npoints = 50000;
        Int_t width   = 2;  
 
 	TF1 * fExIntCosThk = new TF1("fExIntCosThk",RC1, &InSANERADCOR::InternalIntegrand_Plot,
			costhk_min, costhk_max,4,"InSANERADCOR","InternalIntegrand_Plot");
        fExIntCosThk->SetParameter(0,Ebeam); 
        fExIntCosThk->SetParameter(1,Eprime); 
        fExIntCosThk->SetParameter(2,theta); 
        fExIntCosThk->SetParameter(3,phi); 
	fExIntCosThk->SetNpx(Npoints);
	fExIntCosThk->SetLineColor(kMagenta);
	fExIntCosThk->SetLineWidth(width);
 	
        cout << "Calculating relative error of RADCOR w.r.t. ROSETAIL..." << endl;
        const int N = rEp.size();
        double num=0,den=0,arg=0,rc=0,rose=0;
        vector<double> RC,Err;
        double end_time=0;
        TStopwatch *Watch1 = new TStopwatch();
        TStopwatch *Watch2 = new TStopwatch();
        Watch2->Start();
        for(int i=0;i<N;i++){
                Watch1->Start();
                rose     = rXS[i];
                rc       = RC2->Exact(Ebeam,rEp[i],theta);
                end_time = Watch1->RealTime();
                num      = TMath::Abs(rose-rc);
                den      = rose;
                arg      = 100.*num/den;
                if(fDebug==1){
                        cout << "Ep = "     << Form("%.3f",rEp[i])      << "\t"
                             << "rose = "   << Form("%.3E",rose)        << "\t"
                             << "radcor = " << Form("%.3E",rc )         << "\t"
                             << "err = "    << Form("%.3f",arg)         << "\t"
                             << "time = "   << Form("%.3f s",end_time)  << endl;
                }
                RC.push_back(rc);
                Err.push_back(arg);
        }
        double end_time_full = Watch2->RealTime();
        TString Units = "s"; 
        if( (end_time_full>=minute)&&(end_time_full<hour) ){
		end_time_full *= (1./minute); 
		Units = "min.";
        }else if(end_time_full>=hour){
		end_time_full *= (1./hour); 
		Units = "hr.";
        }
        cout << "--> Done." << endl;
        cout << "Total time: " << end_time_full << " " << Units << endl;

	// RC2->WriteTheROOTFile();

	// exit(1); 

	cout << "Plotting..." << endl;

        TGraph *gRADCOR = GetTGraph(rEp,RC); 
	gRADCOR->SetLineColor(kMagenta); 
	gRADCOR->SetLineWidth(width); 

        TGraph *gError = GetTGraph(rEp,Err); 
	gError->SetLineColor(kBlack); 
	gError->SetLineWidth(width); 

        TMultiGraph *G = new TMultiGraph();
        G->Add(gRADCOR  ,"c"); 
        G->Add(gRosetail,"c"); 

        TLegend *L = new TLegend(0.6,0.6,0.8,0.8);
	L->AddEntry(fExIntCosThk,Form("RADCOR++ (N_{MC} = 10^{%d})",order),"l");
	L->AddEntry(gRosetail   ,"ROSETAIL","l");

	TCanvas *c1 = new TCanvas("c1","cos(thk) Integrand",700,500);
	c1->SetFillColor(kWhite); 

        TString Title      = Form("cos #theta_{k} Integrand (E_{s} = %.2f GeV, E_{p} = %.2f GeV, #theta = %.0f#circ)",Ebeam,Eprime,theta/degree);
	TString xAxisTitle = Form("E_{p} (GeV)");
	TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");

	c1->cd();
        gPad->SetLogy(true); 
        fExIntCosThk->Draw(); 
        fExIntCosThk->SetTitle(Title);
        fExIntCosThk->GetXaxis()->SetTitle("cos #theta_{k}");
	c1->Update(); 

	TCanvas *c2 = new TCanvas("c2","Elastic Tail",1000,800);
	c2->SetFillColor(kWhite); 
        c2->Divide(2,1); 
 
        double yMin = 1e-5; 
        double yMax = 10; 
        
	Title = Form("Elastic Tail (E_{s} = %.2f GeV, #theta = %.0f#circ)",Ebeam,theta/degree);

	c2->cd(1);
        gPad->SetLogy(true); 
        G->Draw("A");
        G->SetTitle(Title); 
        G->GetXaxis()->SetTitle(xAxisTitle);
        G->GetXaxis()->CenterTitle();
	G->GetYaxis()->SetTitle(yAxisTitle);
        G->GetYaxis()->CenterTitle();
        G->GetYaxis()->SetRangeUser(yMin,yMax);
        // fExIntXS->Draw("same");
        L->Draw("same");
	c2->Update(); 

        c2->cd(2); 
	gError->Draw("AC");
	gError->SetTitle("Relative Error of RADCOR w.r.t. ROSETAIL");
	gError->GetXaxis()->SetTitle(xAxisTitle);
	gError->GetXaxis()->CenterTitle();
	gError->GetYaxis()->SetTitle("#Delta#sigma/#sigma_{rose} (%)");
        gError->GetYaxis()->CenterTitle();
	gError->GetYaxis()->SetRangeUser(0,100); 
        c2->Update();

        TString outpath = Form("./output/MC/he3_eltail_exact_%d_sm-mc_%d-pass.dat",order,Pass);
        PrintToFile(outpath,rEp,RC);

	return(0);
}
//______________________________________________________________________________
void PrintToFile(TString outpath,vector<double> x,vector<double> y){

        const int N = x.size();
        ofstream outfile;
        outfile.open(outpath);
        if(outfile.fail()){
                cout << "Cannot open the file: " << outpath << endl;
                exit(1);
        }else{
                for(int i=0;i<N;i++){
                        outfile << Form("%.3E",x[i]) << "\t"
                                << Form("%.3E",y[i]) << endl;
                }
                outfile.close();
                cout << "Data has been written to the file: " << outpath << endl;
        }

}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

        const int N = x.size();
        TGraph *g = new TGraph(N,&x[0],&y[0]);
        return g;

}
//______________________________________________________________________________
void ImportRosetailData(int Pass,vector<double> &Ep,vector<double> &Tail){

        double CONV = 1E+3;
        double iEs,iEp,iNu,iW,iT;

        TString inpath = Form("./output/rosetail/%d_SA_eltail_unpl_int-only.out",Pass);
        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                cout << "Opening the file: " << inpath << endl;
                while(!infile.eof()){
                        infile >> iEs >> iEp >> iNu >> iW >> iT;
                        Ep.push_back(iEp/CONV);
                        Tail.push_back(iT);
                }
                infile.close();
                Ep.pop_back();
                Tail.pop_back();
        }

}


