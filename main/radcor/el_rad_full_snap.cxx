// WARNING: run this script as: root rad_test.cxx | tee -a dump.dat
//          to see the output in a file. 

#include <cstdlib> 
#include <iostream> 

TString gTargetName;

Int_t el_rad_full_snap(){

	Int_t Npoints = 500;
        Int_t width   = 2; 
	Double_t delta = 0.01;  

        Double_t RadLen[2]; 
        RadLen[0] = 0.002928; 
        RadLen[1] = 0.0362; 

	Double_t ZEff  = 19.5260; 
	Double_t AEff  = 40.4370; 
	Double_t X0Eff = 19.5232; 

        Double_t Es=0,Ep=0;
	Double_t EsPrime=0,EpPrime=0;

        Double_t T_min  = 0.; 
	Double_t T_max  = RadLen[0] + RadLen[1]; 
	Double_t t_test = RadLen[0]; 

        Double_t Ebeam  = 0.; 
	Double_t Eprime = 0.;
        Double_t theta  = 5.*degree;  
        Double_t phi    = 0.*degree;  

        Int_t Pass      = 0.;  
 	Double_t EpMin  = 0.; 
        Double_t EpMax  = 0.; 
 	Double_t EsMin  = 0.; 
        Double_t EsMax  = 0.;
        Double_t delta_s= 0.;  
        Double_t delta_p= 0.;  

        cout << "Enter beam pass (1, 5 or 20): "; 
        // cin  >> Pass; 

	Pass = 20; 

	cout << "pass = " << Pass << endl;

        switch(Pass){
		case 1: 
			Ebeam  = 1.0; 
			Eprime = 0.5; 
			break;
		case 5: 
			Ebeam  = 5.0; 
			Eprime = 2.0; 
			break; 
		case 20: 
			Ebeam  = 20.0; 
			Eprime = 6.0; 
			break;
        } 
      
        InSANENucleus::NucleusType Target = InSANENucleus::kProton; 

        F1F209eInclusiveDiffXSec *XS      = new F1F209eInclusiveDiffXSec(); 
        XS->UseModifiedModel('y'); 

        /// form factors 
        AMTFormFactors *AMT             = new AMTFormFactors(); 
        InSANEDipoleFormFactors *Dipole = new InSANEDipoleFormFactors(); 
        AmrounFormFactors *Amroun       = new AmrounFormFactors(); 
        
        /// structure functions 
        NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
        CTEQ6UnpolarizedPDFs *CTEQ       = new CTEQ6UnpolarizedPDFs();

	InSANEStructureFunctionsFromPDFs *UnpolSFs = new InSANEStructureFunctionsFromPDFs();
	UnpolSFs->SetUnpolarizedPDFs(CTEQ);  

        InSANERADCOR *RC1 = new InSANERADCOR();
        RC1->DoElastic(true); 

        // RC1->SetVerbosity(4); 

	cout << "Proton target" << endl;
	RC1->SetFormFactors(Dipole);
	RC1->SetUnpolarizedStructureFunctions(NMC95); 

        RC1->SetUnpolarizedCrossSection(XS); 
        RC1->SetTargetType(Target); 
        RC1->SetRadiationLengths(RadLen);

        RC1->SetEffectiveRadiationLength(X0Eff); 
        RC1->SetEffectiveZ(ZEff); 
        RC1->SetEffectiveA(AEff); 
 
        Es = Ebeam  - RC1->Delta(Ebeam,RadLen[0]); 
        Ep = Eprime + RC1->Delta(Eprime,RadLen[1]); 

        /// Set kinematics
        /// In the exact case, we calculate the delta_{s,p} terms in the method!  
        RC1->SetKinematicsForExactInternal(Ebeam,Eprime,theta);
        RC1->SetKinematicsForExactExternal(Ebeam,Eprime,theta);

        // limits on Es 
	EsMin       = RC1->GetEsMin(Ep,theta);
        EsMax       = Es; 
	EsPrime     = EsMin + 0.01*(EsMax-EsMin);   
        // limits on Ep 
        EpMin       = Ep; 
        EpMax       = RC1->GetEpMax(EsPrime,theta); 
        EpPrime     = EpMin + 0.01*(EpMax-EpMin);

        Double_t COSTHK     = -0.5;     // limits are -1 to 1  
        Double_t costhk_min = -1; 
        Double_t costhk_max =  1; 

        if(EsMin>EsMax){
		cout << "Es limits are wrong!" << endl;
		cout << "EsMin = " << EsMin << endl;
		cout << "EsMax = " << EsMax << endl;
		exit(1);
        }
        if(EpMin>EpMax){
		cout << "Ep limits are wrong!" << endl;
		cout << "EpMin = " << EpMin << endl;
		cout << "EpMax = " << EpMax << endl;
		exit(1);
        }
        if(Eprime>Ebeam){
		cout << "Eprime greater than Ebeam!" << endl;
		cout << "Ep = " << Eprime << endl;
		cout << "Es = " << Ebeam << endl;
		exit(1);
        }
	if(EpMax>EsPrime){
		cout << "EpMax greater than EsPrime!" << endl;
		cout << "EpMax = " << EpMax << endl;
		cout << "EsPrime = " << EsPrime << endl;
		exit(1);
	}

	cout << "Es  = " << Es           << endl;
	cout << "Es' = " << EsPrime      << endl;
	cout << "Ep  = " << Ep           << endl;
	cout << "Ep' = " << EpPrime      << endl;
	cout << "th  = " << theta/degree << endl;

        cout << "Integration limits: " << endl;
        cout << "EsMin = " << EsMin << endl;
        cout << "EsMax = " << EsMax << endl;
        cout << "EpMin = " << EpMin << endl;
        cout << "EpMax = " << EpMax << endl;
        cout << "TMin = "  << T_min << endl;
        cout << "TMax = "  << T_max << endl;


 	TF1 * fExact = new TF1("fExact",RC1, &InSANERADCOR::TExactIntegrand_Snapshot_Plot,
			T_min,T_max,5,"InSANERADCOR","TExactIntegrand_Snapshot_Plot");
        fExact->SetParameter(0,Es); 
        fExact->SetParameter(1,EsPrime); 
        fExact->SetParameter(2,Ep); 
        fExact->SetParameter(3,EpPrime); 
        fExact->SetParameter(4,theta); 
	fExact->SetNpx(Npoints);
	fExact->SetLineColor(kMagenta);
	fExact->SetLineWidth(width);

 	TF1 * fIe1 = new TF1("fIe1",RC1, &InSANERADCOR::Ie_new_2_Type1_Plot,
			EsMin,EsMax,5,"InSANERADCOR","Ie_new_2_Type1_Plot");
        fIe1->SetParameter(0,Es); 
        fIe1->SetParameter(1,t_test); 
	fIe1->SetNpx(Npoints);
	fIe1->SetLineColor(kMagenta);
	fIe1->SetLineWidth(width);

 	TF1 * fIe2 = new TF1("fIe2",RC1, &InSANERADCOR::Ie_new_2_Type2_Plot,
			EpMin,EpMax,5,"InSANERADCOR","Ie_new_2_Type2_Plot");
        fIe2->SetParameter(0,Ep); 
        fIe2->SetParameter(1,T_max-t_test); 
	fIe2->SetNpx(Npoints);
	fIe2->SetLineColor(kMagenta);
	fIe2->SetLineWidth(width);

        vector<double> vt,vInteg; 
        FillVectors(T_min,T_max,Npoints,fExact,vt,vInteg);

        vector<double> vEs,vIeEs;
        FillVectors(EsMin,EsMax,Npoints,fIe1,vEs,vIeEs);
 
        vector<double> vEp,vIeEp; 
        FillVectors(EpMin,EpMax,Npoints,fIe2,vEp,vIeEp); 

        TGraph *gExact = GetTGraph(vt,vInteg);
        gExact->SetLineWidth(width); 
        gExact->SetLineColor(kMagenta);  

        TGraph *gIeEs = GetTGraph(vEs,vIeEs);
        gIeEs->SetLineWidth(width); 
        gIeEs->SetLineColor(kMagenta);  

        TGraph *gIeEp = GetTGraph(vEp,vIeEp);
        gIeEp->SetLineWidth(width); 
        gIeEp->SetLineColor(kMagenta);  

	TCanvas *c1 = new TCanvas("c1","Exact Integrands",1200,800);
	c1->SetFillColor(kWhite); 
        c1->Divide(3,1); 

        double yMin = 1e-5; 
        double yMax = 1e+10; 

        TString Title1 = Form("Exact Ext. t Integrand (E_{s} = %.2f GeV, E_{p} = %.2f GeV, #theta = %.0f#circ)",Es,Ep,theta/degree);
        TString Title2 = Form("I_{e}(E_{s} = %.2f GeV, E_{s}',t = %.3E )",Es,t_test);
        TString Title3 = Form("I_{e}(E_{p}', E_{p} = %.2f GeV,t = %.3E )",Ep,T_max-t_test);

	c1->cd(1);
        gPad->SetLogy(true); 
        gExact->Draw("AC"); 
        gExact->SetTitle(Title1);
        gExact->GetXaxis()->SetTitle("t (#X_{0})");
        gExact->GetXaxis()->CenterTitle();
        gExact->GetYaxis()->SetRangeUser(yMin,yMax);
	c1->Update(); 

	c1->cd(2);
        gPad->SetLogy(true); 
        gIeEs->Draw("AC"); 
        gIeEs->SetTitle(Title2);
        gIeEs->GetXaxis()->SetTitle("E_{s}'");
        gIeEs->GetXaxis()->CenterTitle();
        gIeEs->GetYaxis()->SetRangeUser(yMin,yMax);
	c1->Update(); 

	c1->cd(3);
        gPad->SetLogy(true); 
        gIeEp->Draw("AC"); 
        gIeEp->SetTitle(Title3);
        gIeEp->GetXaxis()->SetTitle("E_{p}'");
        gIeEp->GetXaxis()->CenterTitle();
        gIeEp->GetYaxis()->SetRangeUser(yMin,yMax);
	c1->Update(); 

	return(0);
}
//______________________________________________________________________________
void FillVectors(double min,double max,int N,TF1 *f,vector<double> &x,vector<double> &y){

	// cout << "-----------------------------------" << endl;

	double ix=0,iy=0;
	double step = (max-min)/( (double)N ); 
	for(int i=0;i<N;i++){
		ix = min + ( (double)i )*step; 
		iy = f->Eval(ix);
		// cout << "x = " << Form("%.3E",ix) << "\t"  
                //      << "y = " << Form("%.3E",iy) << endl;
		x.push_back(ix);  
		y.push_back(iy);  
	}

}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

        const int N = x.size();
        TGraph *g = new TGraph(N,&x[0],&y[0]);
        return g;

}
//______________________________________________________________________________
void ImportMoTsaiData(int Pass,vector<double> &Ep,vector<double> &Tail){

        double CONV = 1E+3;
        double iEp,iW,iT;

        TString inpath = Form("./output/MoTsai/elastic/MoTsai_%d-pass.dat",Pass);
        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                cout << "Opening the file: " << inpath << endl;
                while(!infile.eof()){
                        infile >> iEp >> iW >> iT;
                        Ep.push_back(iEp/CONV);
                        Tail.push_back(iT);
                }
                infile.close();
                Ep.pop_back();
                Tail.pop_back();
        }

}
//______________________________________________________________________________
void ImportRosetailData(int Pass,vector<double> &Ep,vector<double> &Tail){

        double CONV = 1E+3;
        double iEs,iEp,iNu,iW,iT;

        TString inpath = Form("./output/rosetail/Mo-T_eltail_unpl_%d-pass.out",Pass);
        ifstream infile;
        infile.open(inpath);
        if(infile.fail()){
                cout << "Cannot open the file: " << inpath << endl;
                exit(1);
        }else{
                cout << "Opening the file: " << inpath << endl;
                while(!infile.eof()){
                        infile >> iEs >> iEp >> iNu >> iW >> iT;
                        Ep.push_back(iEp/CONV);
                        Tail.push_back(iT);
                }
                infile.close();
                Ep.pop_back();
                Tail.pop_back();
        }

}

