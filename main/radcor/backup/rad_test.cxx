// WARNING: run this script as: root rad_test.cxx | tee -a dump.dat
//          to see the output in a file. 

#include <cstdlib> 
#include <iostream> 

Double_t hbarc2  = 0.389379E+06; // (hbar*c)^2 in units of GeV^2*nb 
TString gTargetName;

Int_t rad_test(){

        Double_t Es,th; 
        int Target = 3; // 3He

        Double_t RadLen[3]; 
  
	InSANERADCORRadiatedUnpolarizedDiffXSec *sig = new InSANERADCORRadiatedUnpolarizedDiffXSec();
	sig->SetThreshold(1);                // QE threshold 
        sig->SetMultiplePhoton(1);           // use multiple photon correction 
	sig->SetVerbosity(4); 
	sig->fRADCOR->SetTargetType(Target);               

        GetInput(Es,th,RadLen); 

	sig->fRADCOR->SetRadiationLengths(RadLen); 

	TargetName = sig->fRADCOR->GetTargetName(); 
 
	sig->fRADCOR->Print();

        vector<Double_t> Ep,XSApprox,XSExact; 

        Int_t N = 40; 

	Double_t EpMin  = 0.600;
	Double_t EpMax  = 2.000; 
	Double_t EP     = EpMin;
	Double_t deltap = 0.01;

        Double_t par[3]; 
        Double_t sig_approx=0,sig_exact=0;

	while(EP<=EpMax){
		par[0] = Es;
		par[1] = EP; 
		par[2] = th;
		sig->UseApprox('n'); 
		sig_exact = sig->EvaluateXSec(par);  
		cout << "--> Done." << endl;
		sig->UseApprox('y'); 
		sig_approx = sig->EvaluateXSec(par);  
		cout << "--> Done." << endl;
		cout << "Ep = "          << Form("%.2f",EP)         << "\t" 
		     << "sig_exact  =  " << Form("%.3f",sig_exact)  << " nb/GeV/sr" << "\t" 
		     << "sig_approx =  " << Form("%.3f",sig_approx) << " nb/GeV/sr" << endl;
		cout << "------------------------------------------------------------" << endl;
		Ep.push_back(EP); 
		XSApprox.push_back(sig_approx); 
		XSExact.push_back(sig_exact); 
		EP     += deltap;
	}

        Plot(Es,th,Ep,XSApprox,XSExact); 

        
	return(0);
}
//______________________________________________________________________________
void Plot(Double_t Es,Double_t th,vector<Double_t> Ep,vector<Double_t> XS){

        TString Title      = Form("%s Internally-Radiated Tail (E_{s} = %.2f GeV, #theta = %.0f#circ)",gTargetName.Data(),Es,th);
        TString xAxisTitle = Form("E_{p} (GeV)");
        TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");
       
        const Int_t NN = Ep.size(); 
        TGraph *g = new TGraph(NN,&Ep[0],&XS[0]); 
	g->SetMarkerColor(kBlack);
	g->SetMarkerStyle(20);

        TCanvas *c1 = new TCanvas("c1","RADCOR Internal Tail Test (Exact)",1000,800);
	c1->SetFillColor(kWhite);

        c1->cd(); 
        g->Draw("AP");
	g->SetTitle(Title);
        g->GetXaxis()->SetTitle(xAxisTitle);
        g->GetXaxis()->CenterTitle();
        g->GetYaxis()->SetTitle(yAxisTitle);
        g->GetYaxis()->CenterTitle();
        g->Draw("AP");
        c1->Update();


}
//______________________________________________________________________________
void PlotAll(Double_t Es,Double_t th,vector<Double_t> Ep,vector<Double_t> XSA,vector<Double_t> XSE){

	// XSA = approx, XSE = exact 

        TString Title      = Form("%s Internally-Radiated Tail (E_{s} = %.2f GeV, #theta = %.0f#circ)",TargetName.Data(),Es,th);
        TString xAxisTitle = Form("E_{p} (GeV)");
        TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");
       
        const Int_t NN = N; 
        TGraph *g = new TGraph(NN,&Ep[0],&XSIntTail[0]); 
	g->SetMarkerColor(kBlack);
	g->SetMarkerStyle(20);

        TCanvas *c1 = new TCanvas("c1","RADCOR Internal Tail Test (Exact)",1000,800);
	c1->SetFillColor(kWhite);

        c1->cd(); 
        g->Draw("AP");
	g->SetTitle(Title);
        g->GetXaxis()->SetTitle(xAxisTitle);
        g->GetXaxis()->CenterTitle();
        g->GetYaxis()->SetTitle(yAxisTitle);
        g->GetYaxis()->CenterTitle();
        g->Draw("AP");
        c1->Update();


}
//______________________________________________________________________________
void GetInput(Double_t &Es,Double_t &th,Double_t *RadLen){

        Es = 5.89; 
        th = 45*degree; 
        RadLen[0] = 0.002928; 
        RadLen[1] = 0.0362; 

        // cout << "Enter beam energy (GeV): "; 
        // cin  >> gEs; 
        // cout << "Enter scattering angle (deg): "; 
        // cin  >> gth;
        // cout << "Enter radiation length before scattering (#X0): ";
        // cin  >> RadLen[0];  
        // cout << "Enter radiation length after scattering (#X0): ";
        // cin  >> RadLen[1];

}

