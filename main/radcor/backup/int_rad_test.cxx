// WARNING: run this script as: root rad_test.cxx | tee -a dump.dat
//          to see the output in a file. 

#include <cstdlib> 
#include <iostream> 

Double_t hbarc2  = 0.389379E+06; // (hbar*c)^2 in units of GeV^2*nb 
// Double_t hbarc2  = 3.89376E-04; // (hbar*c)^2 in units of GeV^2*b 

TString gTargetName;

Int_t int_rad_test(){

        int Target = 3; // 3He

	Double_t Es,th;
        Double_t RadLen[2] = {0.,0.}; 
  
	InSANERADCORInternalUnpolarizedDiffXSec *sig = new InSANERADCORInternalUnpolarizedDiffXSec();
        sig->UseMultiPhoton('y'); 
	sig->SetVerbosity(0); 
	sig->fRADCOR->SetTargetType(Target);               
	
	gTargetName = sig->fRADCOR->GetTargetName(); 

        GetInput(Es,th,RadLen); 
	
	sig->fRADCOR->SetRadiationLengths(RadLen); 
 
	sig->fRADCOR->Print();

        vector<Double_t> Ep,XS; 

        Double_t EpMin  = 0.60; 
        Double_t EpMax  = 2.00; 
	Double_t EP     = EpMin;
	Double_t deltap = 0.01;

        Double_t par[3]; 
        Double_t sig_int_exact=0; 
        while(EP<=EpMax){ 
                par[0] = Es;
                par[1] = EP; 
                par[2] = th;
                sig_int_exact = sig->EvaluateXSec(par);  
		// cout << "Ep = "              << Form("%.2f",EP)                    << "\t" 
                //      << "sig_int_exact  =  " << Form("%.3f",sig_int_exact*hbarc2)  << " nb/sr" << endl;
                Ep.push_back(EP); 
                XS.push_back(sig_int_exact*hbarc2); 
		EP    += deltap;
	}

        Plot(Es,th,Ep,XS); 
        
	return(0);
}
//______________________________________________________________________________
void Plot(Double_t Es,Double_t th,vector<Double_t> Ep,vector<Double_t> XS){

	// XSA = approx, XSE = exact 

        TString Title      = Form("%s Internally-Radiated Tail (Exact) [E_{s} = %.2f GeV, #theta = %.0f#circ]",gTargetName.Data(),Es,th/degree);
        TString xAxisTitle = Form("E_{p} (GeV)");
        TString yAxisTitle = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");
       
        const Int_t NN = Ep.size(); 
        int width = 2;
        TGraph *g = new TGraph(NN,&Ep[0],&XS[0]); 
	g->SetMarkerColor(kBlack);
	g->SetLineColor(kBlack);
	g->SetMarkerStyle(20);
        g->SetLineWidth(width);

        TCanvas *c1 = new TCanvas("c1","RADCOR Internal Tail Test (Exact)",1000,800);
	c1->SetFillColor(kWhite);

	TString DrawOption = "AC"; 

        c1->cd(); 
        g->Draw(DrawOption);
	g->SetTitle(Title);
        g->GetXaxis()->SetTitle(xAxisTitle);
        g->GetXaxis()->CenterTitle();
        g->GetYaxis()->SetTitle(yAxisTitle);
        g->GetYaxis()->CenterTitle();
        g->Draw(DrawOption);
        c1->Update();


}
//______________________________________________________________________________
void GetInput(Double_t &Es,Double_t &th,Double_t *RadLen){

        Es = 5.89;
        th = 45.*degree; 
        RadLen[0] = 0.; // 0.002928; 
        RadLen[1] = 0.; // 0.0362; 
 
        // cout << "Enter beam energy (GeV): "; 
        // cin  >> gEs; 
        // cout << "Enter scattering angle (deg): "; 
        // cin  >> gth;
        // cout << "Enter radiation length before scattering (#X0): "; 
        // cin  >> RadLen[0];
        // cout << "Enter radiation length after scattering (#X0): "; 
        // cin  >> RadLen[1];
}

