// WARNING: run this script as: root rad_test.cxx | tee -a dump.dat
//          to see the output in a file. 

#include <cstdlib> 
#include <iostream> 

TString gTargetName;

Int_t exact_int_test(){

        Double_t Es;
        Double_t RadLen[2]; 
        RadLen[0] = 0.002928; 
        RadLen[1] = 0.0362; 

        Double_t Ebeam  = 20.0; 
	Double_t Eprime = 6.0;
        Double_t theta  = 5.*degree;  
        Double_t phi    = 0.*degree;  

        InSANENucleus::NucleusType Target = InSANENucleus::kProton; 

        F1F209eInclusiveDiffXSec *XS      = new F1F209eInclusiveDiffXSec(); 
        XS->SetTargetType(Target); 
        XS->UseModifiedModel('y'); 

        /// form factors 
        AMTFormFactors *AMT             = new AMTFormFactors(); 
        InSANEDipoleFormFactors *Dipole = new InSANEDipoleFormFactors(); 
        AmrounFormFactors *Amroun       = new AmrounFormFactors(); 
        
        /// structure functions 
        F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 
        NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
        CTEQ6UnpolarizedPDFs *CTEQ       = new CTEQ6UnpolarizedPDFs();

	InSANEStructureFunctionsFromPDFs *UnpolSFs = new InSANEStructureFunctionsFromPDFs();
	UnpolSFs->SetUnpolarizedPDFs(CTEQ);  

        InSANERADCOR *RC = new InSANERADCOR();
        // RC->DoElastic(true); 

        if(Target==InSANENucleus::kProton){
		cout << "Proton target" << endl;
		RC->SetFormFactors(Dipole);
		RC->SetUnpolarizedStructureFunctions(NMC95); 
        }else if(Target==InSANENucleus::k3He){
		cout << "3He target" << endl;
		RC->SetTargetFormFactors(Amroun);
		RC->SetUnpolarizedStructureFunctions(F1F209); 
        }
        RC->SetUnpolarizedCrossSection(XS); 
        RC->SetTargetType(Target); 
        RC->SetKinematicsForExactInternal(Ebeam,Eprime,theta);

        Double_t COSTHK    = -0.5;     // limits are -1 to 1  
        Double_t omega_min = 0.; 
        Double_t costhk_min = -1; 
        Double_t costhk_max =  1; 

        Double_t omega_max = RC->CalculateOmegaMax(COSTHK); 

	cout << "Es = " << Ebeam << endl;
	cout << "Ep = " << Eprime << endl;
	cout << "th = " << theta/degree << endl;

        cout << "omega_min = " << omega_min << endl;
        cout << "omega_max = " << omega_max << endl;

	Int_t Npoints = 50000;
        Int_t width   = 2;  
 
	TF1 * fExIntOmega = new TF1("fExIntegrand",RC, &InSANERADCOR::InternalIntegrand_omega_Inelastic_Plot,
			omega_min, omega_max, 1,"InSANERADCOR","InternalIntegrand_omega_Inelastic_Plot");
	fExIntOmega->SetParameter(0,COSTHK);
	fExIntOmega->SetNpx(Npoints);
	fExIntOmega->SetLineColor(kMagenta);
	fExIntOmega->SetLineWidth(width);

 	TF1 * fExIntCosThk = new TF1("fExIntCosThk",RC, &InSANERADCOR::InternalIntegrand_Plot,
			costhk_min, costhk_max,0,"InSANERADCOR","InternalIntegrand_Plot");
	fExIntCosThk->SetNpx(Npoints);
	fExIntCosThk->SetLineColor(kMagenta);
	fExIntCosThk->SetLineWidth(width);

        TLegend *L = new TLegend(0.6,0.6,0.8,0.8);
	L->AddEntry(fExIntCosThk,Form("E_{s} = %.2f GeV, E_{p} = %.2f GeV, #theta = %.0f#circ",Ebeam,Eprime,theta/degree),"l");

	TCanvas *c1 = new TCanvas("c1","omega integrand",1000,800);
        c1->SetFillColor(kWhite); 
	// c1->Divide(2,1);

        TString Title = Form("#omega Integrand");

	// c1->cd(1);
        // gPad->SetLogy(true); 
        // fExIntOmega->Draw(); 
        // fExIntOmega->SetTitle(Title);
        // fExIntOmega->GetXaxis()->SetTitle("#omega (GeV)");
	// c1->Update(); 
        
        Title = Form("cos #theta_{k} Integrand");

	c1->cd(2);
        gPad->SetLogy(true); 
        fExIntCosThk->Draw(); 
        fExIntCosThk->SetTitle(Title);
        fExIntCosThk->GetXaxis()->SetTitle("cos #theta_{k}");
	c1->Update(); 

	return(0);
}

