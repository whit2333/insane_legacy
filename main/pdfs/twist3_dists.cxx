void twist3_dists() {

   insane::physics::JAM_T3DFs  * jam  = new insane::physics::JAM_T3DFs();
   insane::physics::LCWF_T3DFs * lcwf = new insane::physics::LCWF_T3DFs();

   double Q2   = 2.0;
   auto   udist = std::function<double(double*,double*)>(
       [&](double *x, double*) { return jam->Get(insane::physics::kUP,x[0],Q2); }
       );
   auto   ddist = std::function<double(double*,double*)>(
       [&](double *x, double*) { return jam->Get(insane::physics::kDOWN,x[0],Q2); }
       );
   auto   udist2 = std::function<double(double*,double*)>(
       [&](double *x, double*) { return lcwf->Get(insane::physics::kUP,x[0],Q2); }
       );
   auto   ddist2 = std::function<double(double*,double*)>(
       [&](double *x, double*) { return lcwf->Get(insane::physics::kDOWN,x[0],Q2); }
       );

   TF1 *  fu    = new TF1("jamT3u",udist,0.0,1.0,0);
   TF1 *  fd    = new TF1("jamT3d",ddist,0.0,1.0,0);
   TF1 *  fu2    = new TF1("lcwfT3u",udist2,0.0,1.0,0);
   TF1 *  fd2    = new TF1("lcwfT3d",ddist2,0.0,1.0,0);

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = nullptr;

   mg->Add( gr = new TGraph(fu), "l");

   mg->Add( gr = new TGraph(fd), "l");
   gr->SetLineColor(4);

   mg->Add( gr = new TGraph(fu2), "l");
   gr->SetLineStyle(2);

   mg->Add( gr = new TGraph(fd2), "l");
   gr->SetLineStyle(2);
   gr->SetLineColor(4);

   mg->Draw("a");
}
