void higher_twist_pdfs(double Q2 = 1.0){

  JAM15PolarizedPDFs  * pdfs = new JAM15PolarizedPDFs();

  double x_min = 0.001;
  double x_max = 0.99;
  int    N     = 100;
  double dx    = (x_max-x_min)/double(N);

  TGraph      * gr_Du           = new TGraph();
  TGraph      * gr_Dd           = new TGraph();
  TGraph      * gr_Dp           = new TGraph();
  TGraph      * gr_Hp           = new TGraph();
  TGraph      * gr_Hn           = new TGraph();

  for(unsigned int ix = 0; ix<N ; ix++) {

    double x = x_min + double(ix)*dx;

    gr_Du->SetPoint(ix, x, x*pdfs->D_u(x,Q2));
    gr_Dd->SetPoint(ix, x, x*pdfs->D_d(x,Q2));
    gr_Dp->SetPoint(ix, x, x*pdfs->Dp_Twist3(x,Q2));
    gr_Hp->SetPoint(ix, x, x*pdfs->Hp_Twist4(x,Q2));
    gr_Hn->SetPoint(ix, x, x*pdfs->Hn_Twist4(x,Q2));
  }

  // -------------------------------------------------
  //
  TCanvas * c   = nullptr;
  TLegend * leg = nullptr;
  
  // ----------------------------------
  TMultiGraph * mg              = new TMultiGraph();
  gr_Du->SetLineWidth(2);
  gr_Dd->SetLineWidth(2);
  gr_Dp->SetLineWidth(2);

  gr_Du->SetLineColor(1);
  gr_Dd->SetLineColor(2);
  gr_Dp->SetLineColor(4);

  gr_Hp->SetLineWidth(2);
  gr_Hn->SetLineWidth(2);

  gr_Hp->SetLineStyle(2);
  gr_Hn->SetLineStyle(2);

  gr_Hp->SetLineColor(1);
  gr_Hn->SetLineColor(2);

  // ----------------------------------
  c   = new TCanvas();
  leg = new TLegend(0.7,0.8,0.99,0.99);
  leg->AddEntry(gr_Du, "xD_{u}", "l");
  leg->AddEntry(gr_Dd, "xD_{d}", "l");
  leg->AddEntry(gr_Dp, "xD_{p}", "l");
  leg->AddEntry(gr_Hp, "xH_{p}", "l");
  leg->AddEntry(gr_Hn, "xH_{n}", "l");
  mg->Add(gr_Du, "l");
  mg->Add(gr_Dd, "l");
  mg->Add(gr_Dp, "l");
  mg->Add(gr_Hp, "l");
  mg->Add(gr_Hn, "l");
  mg->Draw("a");
  mg->SetTitle("Twist 3");
  mg->GetXaxis()->SetTitle("D(x)");
  mg->GetYaxis()->SetTitle("x");
  mg->GetXaxis()->CenterTitle(true);
  mg->GetYaxis()->CenterTitle(true);
  gPad->SetLogx(true);
  mg->Draw("a");
  leg->Draw();
  c->SaveAs("results/pdfs/higher_twist_pdfs_0.png");
  c->SaveAs("results/pdfs/higher_twist_pdfs_0.pdf");

}

