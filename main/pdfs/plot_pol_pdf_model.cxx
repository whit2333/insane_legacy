Int_t plot_pol_pdf_model(Double_t Q2 = 1.0, Int_t aNumber = 14) {

   InSANEFunctionManager             * fman = InSANEFunctionManager::GetManager();
   fman->PrintNameLists();
   InSANEPolarizedStructureFunctions * pSFs = fman->CreatePolSFs(aNumber);
   InSANEPolarizedStructureFunctionsFromPDFs * sf = dynamic_cast<InSANEPolarizedStructureFunctionsFromPDFs*>(pSFs);
   if(!sf) return -1;

   InSANEPolarizedPartonDistributionFunctions * pdfs = sf->GetPolarizedPDFs();

   // -------------------------
   gROOT->GetColor(40)->SetAlpha(0.7);
   gROOT->GetColor(41)->SetAlpha(0.7);
   gROOT->GetColor(42)->SetAlpha(0.7);
   gROOT->GetColor(43)->SetAlpha(0.7);
   gROOT->GetColor(44)->SetAlpha(0.7);
   gROOT->GetColor(45)->SetAlpha(0.7);
   gROOT->GetColor(46)->SetAlpha(0.7);
   gROOT->GetColor(47)->SetAlpha(0.7);
   gROOT->GetColor(48)->SetAlpha(0.7);

   Int_t nBins = 100;

   TH1F * hDelta_u      = new TH1F("hDelta_u",    "#Delta u",nBins,0.001,0.99);
   TH1F * hDelta_u_err  = new TH1F("hDelta_u_err","#Delta u",nBins,0.001,0.99);
   pdfs->GetValues(   hDelta_u,    Q2,InSANEPDFBase::kUP);
   pdfs->GetErrorBand(hDelta_u_err,Q2,InSANEPDFBase::kUP);

   TH1F * hDelta_d      = new TH1F("hDelta_d",    "#Delta d",nBins,0.001,0.99);
   TH1F * hDelta_d_err  = new TH1F("hDelta_d_err","#Delta d",nBins,0.001,0.99);
   pdfs->GetValues(   hDelta_d,    Q2,InSANEPDFBase::kDOWN);
   pdfs->GetErrorBand(hDelta_d_err,Q2,InSANEPDFBase::kDOWN);

   TH1F * hDelta_ubar      = new TH1F("hDelta_ubar",    "#Delta #bar{u}",nBins,0.001,0.99);
   TH1F * hDelta_ubar_err  = new TH1F("hDelta_ubar_err","#Delta #bar{u}",nBins,0.001,0.99);
   pdfs->GetValues(   hDelta_ubar,    Q2,InSANEPDFBase::kANTIUP);
   pdfs->GetErrorBand(hDelta_ubar_err,Q2,InSANEPDFBase::kANTIUP);

   TH1F * hDelta_dbar      = new TH1F("hDelta_dbar",    "#Delta #bar{d}",nBins,0.001,0.99);
   TH1F * hDelta_dbar_err  = new TH1F("hDelta_dbar_err","#Delta #bar{d}",nBins,0.001,0.99);
   pdfs->GetValues(   hDelta_dbar,    Q2,InSANEPDFBase::kANTIDOWN);
   pdfs->GetErrorBand(hDelta_dbar_err,Q2,InSANEPDFBase::kANTIDOWN);

   TH1F * hDelta_g      = new TH1F("hDelta_g",    "#Delta g",nBins,0.001,0.99);
   TH1F * hDelta_g_err  = new TH1F("hDelta_g_err","#Delta g",nBins,0.001,0.99);
   pdfs->GetValues(   hDelta_g,    Q2,InSANEPDFBase::kGLUON);
   pdfs->GetErrorBand(hDelta_g_err,Q2,InSANEPDFBase::kGLUON);

   // -------------------------

   TCanvas * c   = new TCanvas();
   TLegend * leg = new TLegend(0.6,0.7,0.87,0.87);
   THStack * hs  = new THStack("errorBands",pdfs->GetLabel());

   //gPad->SetLogx(true);

   // Delta g
   hDelta_g_err->SetFillColor(40);
   hs->Add(hDelta_g_err,"e3");
   hs->Add(hDelta_g,"C hist");

   // Delta u
   hDelta_u_err->SetFillColor(41);
   hs->Add(hDelta_u_err,"e3");
   hs->Add(hDelta_u,"C hist");

   // Delta d
   hDelta_d_err->SetFillColor(42);
   hs->Add(hDelta_d_err,"e3");
   hs->Add(hDelta_d,"C hist");

   // Delta ubar
   hDelta_ubar_err->SetFillColor(46);
   hs->Add(hDelta_ubar_err,"e3");
   hs->Add(hDelta_ubar,"C hist");


   // Delta d
   hDelta_dbar_err->SetFillColor(47);
   hs->Add(hDelta_dbar_err,"e3");
   hs->Add(hDelta_dbar,"C hist");


   leg->SetNColumns(2);
   leg->AddEntry(hDelta_u_err,"#Deltau","LF");
   leg->AddEntry(hDelta_ubar_err,"#Delta#bar{u}","LF");
   leg->AddEntry(hDelta_d_err,"#Deltad","LF");
   leg->AddEntry(hDelta_dbar_err,"#Delta#bar{d}","LF");
   leg->AddEntry(hDelta_g_err,"#Deltag","LF");

   //TLatex * t = new TLatex();
   //t->SetNDC();
   //t->SetTextFont(62);
   //t->SetTextColor(36);
   //t->SetTextSize(0.06);
   //t->SetTextAlign(12);

   hs->Draw("nostack");
   hs->SetMaximum(0.8);
   hs->SetMinimum(-0.2);
   //hs->GetHistogram()->GetYaxis()->SetRangeUser(-0.2,0.7);
   hs->GetXaxis()->SetTitle("x");
   hs->GetXaxis()->CenterTitle(true);
   leg->Draw();

   c->SaveAs(Form("results/pdfs/plot_pol_pdf_model_%d.png",aNumber));

   return(0);
}
