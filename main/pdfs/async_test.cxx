#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <future>
#include <thread>
#include <chrono>
#include "TF1.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "JAM_T3DFs.h"
#include "LCWF_T3DFs.h"

using namespace std;

void async_test() {

   double Q20   = 2.0;

   auto f = std::async(launch::async,
                       [=](double Q2){
                         insane::physics::JAM_T3DFs  * jam  = new insane::physics::JAM_T3DFs();
                         auto udist = std::function<double(double*,double*)>(
                           [=](double *x, double*) { return jam->Get(insane::physics::kUP,x[0],Q2); }
                           );
                         TF1 *  fu    = new TF1("jamT3u",udist,0.0,1.0,0);
                         std::cout << " f " << std::endl;
                         //fu->Dump();
                         return new TGraph(fu);
                       },
                       Q20);

   auto f2 = std::async(launch::async,
                        [=](double Q2){
                          insane::physics::JAM_T3DFs  * jam  = new insane::physics::JAM_T3DFs();
                          auto   ddist = std::function<double(double*,double*)>(
                            [&](double *x, double*) { return jam->Get(insane::physics::kDOWN,x[0],Q2); }
                            );
                          TF1 *  fd    = new TF1("jamT3d",ddist,0.0,1.0,0);
                          //fd->Dump();
                          std::cout << " f2 " << std::endl;
                          return new TGraph(fd);
                        },
                        Q20);

   auto f3 = std::async(launch::async,
                        [=](double Q2){
                          insane::physics::LCWF_T3DFs * lcwf = new insane::physics::LCWF_T3DFs();
                          auto   udist2 = std::function<double(double*,double*)>(
                            [&](double *x, double*) { return lcwf->Get(insane::physics::kUP,x[0],Q2); }
                            );
                          TF1 *  fu2   = new TF1("lcwfT3u",udist2,0.0,1.0,0);
                          std::cout << " f3 " << std::endl;
                          return new TGraph(fu2);
                        },
                        Q20);

   auto f4 = std::async(launch::async,
                        [=](double Q2){
                          insane::physics::LCWF_T3DFs * lcwf = new insane::physics::LCWF_T3DFs();
                          auto   ddist2 = std::function<double(double*,double*)>(
                            [&](double *x, double*) { return lcwf->Get(insane::physics::kDOWN,x[0],Q2); }
                            );
                          TF1 *  fd2   = new TF1("lcwfT3d",ddist2,0.0,1.0,0);
                          //fd2->Dump();
                          std::cout << " f4 " << std::endl;
                          return new TGraph(fd2);
                        },
                        Q20);

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = nullptr;

   mg->Add( gr = f.get(), "l");

   mg->Add( gr = f2.get(), "l");
   gr->SetLineColor(4);

   mg->Add( gr = f3.get(), "l");
   gr->SetLineStyle(2);

   mg->Add( gr =  f4.get(), "l");

   gr->SetLineStyle(2);
   gr->SetLineColor(4);

   mg->Draw("a");
}
