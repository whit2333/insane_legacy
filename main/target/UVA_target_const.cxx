Int_t UVA_target_const(){
   Double_t packingFraction = 0.6;

   InSANETargetMaterial * matH3 = new InSANETargetMaterial("H3","H3 in Ammonia",1,1);
   matH3->fLength          = 3.0;    //cm
   matH3->fDensity         = 2.0*0.0770; // g/cm3
   matH3->fIsPolarized     = true;
   matH3->fPackingFraction = packingFraction;

   InSANETargetMaterial * matN14 = new InSANETargetMaterial("14N","14N in Ammonia",7,14);
   matN14->fLength          = 3.0;    //cm
   matN14->fDensity         = 2.0*0.3576; // g/cm3
   matN14->fPackingFraction = packingFraction;

   InSANETargetMaterial * matHe = new InSANETargetMaterial("He","LHe around Ammonia beads",2,4);
   matHe->fLength          = 3.0;    //cm
   matHe->fDensity         = 0.1450; // g/cm3
   matHe->fPackingFraction = 1.0 - packingFraction;

   InSANETargetMaterial * matLHe = new InSANETargetMaterial("LHe","Liquid Helium",2,4);
   matLHe->fLength          = 0.50000*2.0;    //cm
   matLHe->fDensity         = 0.1450; // g/cm3

   InSANETargetMaterial * matAlEndcap = new InSANETargetMaterial("Al-endcap","Aluminum target endcp",13,27);
   matAlEndcap->fLength          = 2.0*0.00381 ;    //cm
   matAlEndcap->fDensity         = 2.7; // g/cm3

   InSANETargetMaterial * matAlTail = new InSANETargetMaterial("Al-tail","Aluminum target tail",13,27);
   matAlTail->fLength          = 2.0*0.01016  ;    //cm
   matAlTail->fDensity         = 2.7; // g/cm3

   InSANETargetMaterial * matAl4kShield = new InSANETargetMaterial("Al-4kshield","Aluminum target 4K shield",13,27);
   matAl4kShield->fLength          = 2.0*0.00254  ;    //cm
   matAl4kShield->fDensity         = 2.7; // g/cm3

   InSANETargetMaterial * matCu = new InSANETargetMaterial("Cu","NMR-Cu ",29,64);
   matCu->fLength          = 0.00050   ;    //cm
   matCu->fDensity         = 8.9600 ; // g/cm3

   InSANETargetMaterial * matNi = new InSANETargetMaterial("Ni","NMR-Ni  ",28,59);
   matCu->fLength          = 0.00022   ;    //cm
   matCu->fDensity         = 8.9020  ; // g/cm3

   /// UVA Polarized Target During SANE
   InSANETarget * target = new InSANETarget("UVATarget","UVA Polarized Target");
   target->AddMaterial(matH3);
   target->AddMaterial(matN14);
   target->AddMaterial(matLHe);
   target->AddMaterial(matHe);
   target->AddMaterial(matAlEndcap);
   target->AddMaterial(matAlTail);
   target->AddMaterial(matAl4kShield);
   target->AddMaterial(matCu);
   target->AddMaterial(matNi);

//    target->Print();

   Double_t current = 117.0e-9; // nA
   InSANELuminosity * luminosity = new InSANELuminosity(target,current);
   
   luminosity->Print();
   std::cout << " Luminosity Per Nucleon = " << luminosity->CalculateLuminosityPerNucleon() << " cm2/s\n";

   return(0);
}

