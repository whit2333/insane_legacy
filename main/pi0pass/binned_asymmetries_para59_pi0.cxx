Int_t binned_asymmetries_para59_pi0(Int_t runnumber = 72999, Int_t fileVersion = 2008, Double_t E_min = 500.0) {

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun        * run = rman->GetCurrentRun();
   InSANERunSummary  rSum =  run->GetRunSummary();

   InSANEFunctionManager * fman  = InSANEFunctionManager::GetManager();
   fman->SetBeamEnergy(5.9);

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   //bcgeo->LoadBadBlocks(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber));

   // ----------------------------------------------------------
   // Trees
   rman->GetCurrentFile()->cd();
   TTree * t = (TTree*) gROOT->FindObject("pi0results");
   if(!t) return(-1);
   Long64_t entries = t->GetEntries();
   std::cout << entries << " entries " << std::endl;

   Pi0Event * pi0event             = new Pi0Event();
   t->SetBranchAddress("pi0reconstruction",&pi0event);

   TClonesArray         * clusters = 0;
   InSANEReconstruction * recon    = 0;
   Pi0ClusterPair       * pair     = 0;

   // ----------------------------------------------------------
   // Event classes

   // ------------------------------------------------
   // Create the target and dilution from target
   Double_t pf                      = run->fPackingFraction;
   if(pf>1.0) pf = pf/100.0;
   InSANEDilutionFromTarget * df    = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget("UVaTarget","UVa Pol. Target",pf);
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   // ------------------------------------------------
   // Create the asymmetries to be calculated.
   TList * fAsymmetries = new TList();
   InSANEPi0MeasuredAsymmetry * asym = new InSANEPi0MeasuredAsymmetry();
   asym->SetRunSummary(&rSum);
   asym->Initialize();
   fAsymmetries->Add(asym);

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   dbman->CloseConnections();
   // -------------------------------------------------------------
   // Event Loop
   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      if(iEvent%1000 == 0) {
         std::cout << "\r" << iEvent << "     " ;
         std::cout.flush();
      }
      t->GetEntry(iEvent);

      for(int iPair = 0; iPair < pi0event->fClusterPairs->GetEntries(); iPair++) {

         pair = (Pi0ClusterPair*)(*(pi0event->fClusterPairs))[iPair];
         asym->fPi0ClusterPair = pair;
         //asym->fHelicity = pair->fHelicity;


         //if( pair->fCluster1.GetEnergy() > E_min )
         //if( pair->fCluster2.GetEnergy() > E_min )
         if( TMath::Abs(pair->fMass-135.0) <  30.0 ) {
         //pair->Dump();

            asym->CountEvent();

         }

      }

   }

   std::cout << " Calculating Asymmetry " << std::endl;
   asym->CalculateAsymmetries();

   //for(int j = 0;j<fAsymmetries->GetEntries();j++) {
   //   SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
   //   std::ofstream fileout(Form("plots/%d/binned_asym1-%d.dat",runnumber,j),std::ios_base::trunc );
   //   asy->PrintBinnedTableHead(fileout) ;
   //   asy->PrintBinnedTable(fileout) ;
   //   asy->Print();
   //   asy->PrintBinnedTableHead(std::cout) ;
   //   asy->PrintBinnedTable(std::cout) ;
   //}


   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   c->cd(1);
   asym->fAsymmetryVsE->Draw();
   asym->fAsymmetryVsE->GetYaxis()->SetRangeUser(-5.0,5.0);

   c->cd(2);
   asym->fAsymmetryVsPt->Draw();
   asym->fAsymmetryVsPt->GetYaxis()->SetRangeUser(-5.0,5.0);

   c->cd(3);
   asym->fAsymmetryVsTheta->Draw();
   asym->fAsymmetryVsTheta->GetYaxis()->SetRangeUser(-5.0,5.0);

   c->cd(4);
   asym->fAsymmetryVsPhi->Draw();
   asym->fAsymmetryVsPhi->GetYaxis()->SetRangeUser(-5.0,5.0);

   c->SaveAs("results/asymmetries/pi0/pi0test.png");

   TFile * f = new TFile(Form("data/asym-%d/binned_asymmetries_para59_pi0_%d.root",fileVersion,runnumber),"UPDATE");
   f->cd();
   f->WriteObject(fAsymmetries,Form("pi0-binned-asym-%d",runnumber));//,TObject::kSingleKey); 

   return(0);
}
