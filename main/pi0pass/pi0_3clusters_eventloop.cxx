Int_t pi0_3clusters_eventloop() {

   TFile * f = new TFile("data/pi03cluster.root","READ");
   f->cd();
   /// Get new trees 
   TTree * pitree = (TTree*) gROOT->FindObject("pi0results");
   TTree * pi3tree = (TTree*) gROOT->FindObject("3clusterPi0results");




   if( !pitree  ) { std::cout << "pitree not found\n"; return(-1); }
   if( !pi3tree  ) { std::cout << "pi3tree not found\n"; return(-1); }


   /// Create the entry list which removes all the hot channel events  

   TCanvas * c = new TCanvas("pi0AndBetaCanvas","Pi0 3 Cluster",1200,700);
   c->Divide(4,2);
   TH1 * h1 = 0;
   TH2 * h2 = 0;

   /// First get a reference histogram where the two photon pi0 mass is...
   TString pairXYcut = "(fClusterPairs.fCluster1.fYMoment<10||fClusterPairs.fCluster1.fYMoment>25)&&(fClusterPairs.fCluster2.fYMoment<10||fClusterPairs.fCluster2.fYMoment>25)&&(TMath::Abs(fClusterPairs.fCluster1.fXMoment)<50)&&(TMath::Abs(fClusterPairs.fCluster2.fXMoment)<50)&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood";

   TString aCut = "(fClusters.fYMoment<10||fClusters.fYMoment>25)&&(TMath::Abs(fClusters.fXMoment)<50)&&fClusters.fIsGood";
   TFile * f2 = new TFile("data/pi03clusterKEPT.root","RECREATE");

   /// Pi0 mass
   c->cd(1);

   pitree->Draw("fReconstruction.fMass>>pi0Mass(100,0,300)", 
           Form("%s",pairXYcut.Data()),
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass");
   h1->SetFillColor(1);
   h1->SetTitle("#pi^{0} Mass");
//    h1->Draw();

   TH1F * hMassWithGoodPositions = new TH1F("hMassWithGoodPositions","Mass (no cer gamma)",100,0,300);
   TH1F * hMassWithGoodGammaStar = new TH1F("hMassWithGoodGammaStar","Mass (good GammaStar)",100,0,300);


   TH1F * hMassWithGoodMasses = new TH1F("hMassWithGoodMasses","Mass (no cer gamma)",100,0,300);
   TH1F * hMassWithGoodPositionsAndGammaMass = new TH1F("hMassWithGoodPositionsAndGammaMass","Mass (no cer gamma)",100,0,300);


   TH1F * hPositronCer = new TH1F("hPositronCer","Positron Cer",50,0,4);
   TH1F * hElectronCer = new TH1F("hElectronCer","Electron Cer",50,0,4);

   TH2F * hElectronVPositronEnergy = new TH2F("hElectronVPositronEnergy","Electron Cer",50,0,4000,50,0,4000);
   TH2F * hGammaMassVPositronEnergy = new TH2F("hGammaMassVPositronEnergy","Electron Cer",50,0,4000,50,0,1000);
   TH2F * hGammaMassVElectronEnergy = new TH2F("hGammaMassVElectronEnergy","Electron Cer",50,0,4000,50,0,1000);



   TH1F * hMassWithGoodGammaHit = new TH1F("hMassWithGoodGammaHit","Mass (no cer gamma)",100,0,300);
   TH1F * hMassWithGoodHits = new TH1F("hMassWithGoodHits","Mass (no cer gamma, cer pair)",100,0,300);
   TH1F * hMassWithGoodBlocks = new TH1F("hMassWithGoodBlocks","Mass (good blocks)",100,0,300);
   TH1F * hMassWithGoodHitsAndBlocks = new TH1F("hMassWithGoodHitsAndBlocks","Mass (goodblocks,no cer gamma, cer pair)",100,0,300);

   TH1F * hNGoodMassesPerEvent = new TH1F("hNGoodMassesPerEvent","NGoodMasses",10,0,10);


   TH2F * hElectronPosition = new TH2F("hElectronPosition","Electron position",60,-60,60,120,-120,120);
   TH2F * hPositironPosition= new TH2F("hPositironPosition","Positron position",60,-60,60,120,-120,120);
   TH2F * hGammaPosition = new TH2F("hGammaPosition","Gamma position",60,-60,60,120,-120,120);

   TH1F * hGammaStarMass = new TH1F("hGammaStarMass","Gamma mass",100,0,300);
   TH2F * hGammaStarMassVsPi0Momentum = new TH2F("hGammaStarMassVsPi0Momentum","#gamma mass Vs #pi momentum",50,0,500,50,0,4000);

   TH2F * hElectronEnergyVMass = new TH2F("hElectronEnergyVMass","E1 vs #pi mass",100,0,300,50,0,2000);
   TH2F * hPositronEnergyVMass = new TH2F("hPositronEnergyVMass","E2 vs #pi mass",100,0,300,50,0,2000);
   TH2F * hGammaEnergyVMass = new TH2F("hGammaEnergyVMass","nu vs #pi mass",100,0,300,50,0,2000);

   TH2F * hElectronEnergyVPosition = new TH2F("hElectronEnergyVPosition","E1 vs y",50,-120,120,50,0,2000);

   Pi03ClusterEvent * event3 = new Pi03ClusterEvent();
   TClonesArray *  pairs = event3->fClusterPairs;
   TClonesArray *  clusters = event3->fClusters;    /// For each cluster there is a reconstruction
   TClonesArray *  reconstructions = event3->fReconstruction;
   InSANEReconstruction * aRecon=0;
   Pi0ClusterPair * aPair=0;
   BIGCALCluster * aCluster=0;
   Pi0Event * event2 = new Pi0Event();
   pi3tree->SetBranchAddress("pi03ClusterEvents",&event3);

   TTree * pi3treeOUT = new TTree("good3ClusterEvents","good3ClusterEvents");
//    pi3treeOUT->Branch("goodPi3Event","Pi03ClusterEvent",&event3);
   pi3treeOUT->Branch("goodPi3Recon","InSANEReconstruction",&aRecon);
   pi3treeOUT->Branch("goodPi3Pair","Pi0ClusterPair",&aPair);
   pi3treeOUT->Branch("goodPi3Gamma","BIGCALCluster",&aCluster);


   for(int i =0; i<pi3tree->GetEntries();i++){
      pi3tree->GetEntry(i);
      int NGoodMasses = 0;
      int best = 0;
      double prevbest = 0;
      for(int j=0; j<clusters->GetEntries();j++ ) {
         aCluster = (BIGCALCluster*)(*clusters)[j];
         aRecon  = (InSANEReconstruction*)(*reconstructions)[j];
         aPair = (Pi0ClusterPair*)(*pairs)[aRecon->fNumber];

         if( TMath::Abs(aRecon->fMass-130.0)<200.0 ) {
            NGoodMasses++;
            if(NGoodMasses == 1 ){
               best=j;
               prevbest =  TMath::Abs(aRecon->fMass-130.0);
            } else if(NGoodMasses > 1 ){
             if ( TMath::Abs(aRecon->fMass-130.0) < prevbest ) {
               best=j;
               prevbest =  TMath::Abs(aRecon->fMass-130.0);
              }
            }
          }
       }

      /*for(int j=0; j<clusters->GetEntries();j++ )*/
        if(NGoodMasses>0) {
         aCluster = (BIGCALCluster*)(*clusters)[best];
         aRecon  = (InSANEReconstruction*)(*reconstructions)[best];
         aPair = (Pi0ClusterPair*)(*pairs)[aRecon->fNumber];


        /// This cut removes the noisy row in bigcal
        if( (aCluster->fjPeak != 32) && (aPair->fCluster1->fjPeak != 32) && (aPair->fCluster2->fjPeak != 32) )
        if( (aCluster->fjPeak != 33) && (aPair->fCluster1->fjPeak != 33) && (aPair->fCluster2->fjPeak != 33) )
        if( (aCluster->fjPeak < 31         || aCluster->fjPeak > 37) 
        &&  (aPair->fCluster1->fjPeak < 31 || aPair->fCluster1->fjPeak > 37)
        &&  (aPair->fCluster2->fjPeak < 31 || aPair->fCluster2->fjPeak  > 37) )
        if( !(aCluster->fiPeak == 10 && aCluster->fjPeak == 42) )
        if( !(aCluster->fiPeak == 18 && aCluster->fjPeak == 43) )
        if( !(aCluster->fiPeak == 20 && aCluster->fjPeak == 46) )
        if( !(aCluster->fiPeak == 21 && aCluster->fjPeak == 43) )
        if( !(aCluster->fiPeak == 25 && aCluster->fjPeak == 49) )
        if( !(aCluster->fiPeak == 20 && aCluster->fjPeak == 47) )

        if( !(aPair->fCluster1->fiPeak == 10 && aPair->fCluster1->fjPeak == 42) )
        if( !(aPair->fCluster1->fiPeak == 18 && aPair->fCluster1->fjPeak == 43) )
        if( !(aPair->fCluster1->fiPeak == 20 && aPair->fCluster1->fjPeak == 46) )
        if( !(aPair->fCluster1->fiPeak == 21 && aPair->fCluster1->fjPeak == 43) )
        if( !(aPair->fCluster1->fiPeak == 25 && aPair->fCluster1->fjPeak == 49) )
        if( !(aPair->fCluster1->fiPeak == 20 && aPair->fCluster1->fjPeak == 47) )

        if( !(aPair->fCluster2->fiPeak == 10 && aPair->fCluster2->fjPeak == 42) )
        if( !(aPair->fCluster2->fiPeak == 18 && aPair->fCluster2->fjPeak == 43) )
        if( !(aPair->fCluster2->fiPeak == 20 && aPair->fCluster2->fjPeak == 46) )
        if( !(aPair->fCluster2->fiPeak == 21 && aPair->fCluster2->fjPeak == 43) )
        if( !(aPair->fCluster2->fiPeak == 25 && aPair->fCluster2->fjPeak == 49) )
        if( !(aPair->fCluster2->fiPeak == 20 && aPair->fCluster2->fjPeak == 47) )

        {
           /// Slightly virtual photon and not big opening see mathematica notebook
           /// Removed Edges make
/*           if(NGoodMasses <=1) */
           if( aRecon->fEnergy < 5000.0 
             && aRecon->fNu < 400.0
              && (aRecon->fE1 > 1100.0 || aRecon->fE2 > 1100.0 ) 
              && (aRecon->fE1 > 600.0 && aRecon->fE2 > 600.0 ) )
           if(  /* aPair->fMass < 500.0 &&*/ aPair->fAngle*0.0175 <15.0 && aRecon->fAngle*0.0175 < 15.0
              && TMath::Abs(aPair->fCluster1->fYMoment)<105.0
              && TMath::Abs(aPair->fCluster2->fYMoment)<105.0 
              && TMath::Abs(aCluster->fYMoment ) < 105.0 
              && TMath::Abs(aPair->fCluster1->fXMoment)<45.0
              && TMath::Abs(aPair->fCluster2->fXMoment)<45.0 
              && TMath::Abs(aCluster->fXMoment) < 45.0  )   
              {
              hMassWithGoodGammaStar->Fill(aRecon->fMass);
              hPositronEnergyVMass->Fill(aRecon->fMass,aRecon->fE2);
              hElectronEnergyVMass->Fill(aRecon->fMass,aRecon->fE1);
              hGammaEnergyVMass->Fill(aRecon->fMass,aRecon->fNu);
              hGammaStarMass->Fill(aPair->fMass);
              hElectronEnergyVPosition->Fill(aCluster->fYMoment,aRecon->fNu);

              hElectronPosition->Fill(aPair->fCluster1->fXMoment,aPair->fCluster1->fYMoment);
              hPositironPosition->Fill(aPair->fCluster2->fXMoment,aPair->fCluster2->fYMoment);
              hGammaPosition->Fill(aCluster->fXMoment,aCluster->fYMoment);

              hElectronCer->Fill(aPair->fCluster1->fCherenkovBestADCSum);
              hPositronCer->Fill(aPair->fCluster2->fCherenkovBestADCSum);
              
              pi3treeOUT->Fill();
              

           }

        /// Look at good good blocks only
        if(/*aRecon->fNu > 800.0 && aRecon->fE2 > 500.0 && aRecon->fE1 > 800.0 &&*/  aCluster->fIsGood && aPair->fCluster1->fIsGood && aPair->fCluster2->fIsGood) {
           hMassWithGoodBlocks->Fill( aRecon->fMass);
           /// Gamma* mass with only good blocks

           /// From the plots below we find that the electron is in the bottom and the positron is in the top
           if( TMath::Abs(aRecon->fMass-135.0)<20.0 ){

              hElectronVPositronEnergy->Fill(aRecon->fE1,aRecon->fE2);
              hGammaMassVPositronEnergy->Fill(aRecon->fE2,aPair->fMass);
              hGammaMassVElectronEnergy->Fill(aRecon->fE1,aPair->fMass);
              hGammaStarMassVsPi0Momentum->Fill(aPair->fMass,aRecon->fMomentum);
           }
           /// 
           if(aPair->fCluster2->fYMoment>0.0 && aPair->fCluster1->fYMoment<20.0 ){
              hMassWithGoodPositions->Fill(aRecon->fMass);

              if(aPair->fMass < 50.0) hMassWithGoodPositionsAndGammaMass->Fill(aRecon->fMass);
           }


         /// Cherenkov related cuts
//         if(aCluster->fCherenkovBestADCSum < 0.3) {
           hMassWithGoodGammaHit->Fill( aRecon->fMass);
            if(aPair->fCluster1->fCherenkovBestADCSum > 0.5) if(aPair->fCluster2->fCherenkovBestADCSum > 0.5) {
               hMassWithGoodHits->Fill( aRecon->fMass);
               if(aCluster->fIsGood && aPair->fCluster1->fIsGood && aPair->fCluster2->fIsGood) {
                 hMassWithGoodHitsAndBlocks->Fill( aRecon->fMass);
               }
            }
//         }
        } /// END good blocks


         } /// j=32 remoeal
      } /// end loop on clusters/recons

      hNGoodMassesPerEvent->Fill(NGoodMasses);
      if(NGoodMasses==1)hMassWithGoodMasses->Fill(aRecon->fMass);
   } /// end event loop

   c->cd(1);
   h1->Scale(0.02);
   h1->Draw();
   hMassWithGoodGammaStar->SetFillColor(4);
   hMassWithGoodGammaStar->SetLineColor(4);
   hMassWithGoodGammaStar->Draw("same");

//    hMassWithGoodGammaHit->SetFillColor(2);
//    hMassWithGoodGammaHit->Draw();
//    hMassWithGoodHits->SetFillColor(3);
//    hMassWithGoodHits->Draw("same");
//    hMassWithGoodPositions->SetFillColor(4);
//    hMassWithGoodPositions->Draw("same");
//    hMassWithGoodPositionsAndGammaMass->SetFillColor(5);
//    hMassWithGoodPositionsAndGammaMass->Draw("same");
   c->cd(2);
   hGammaStarMass->SetFillColor(3);
   hGammaStarMass->Draw();

   c->cd(3);
//    hMassWithGoodBlocks->SetFillColor(4);
//    hMassWithGoodBlocks->Draw();
   hPositronCer->Draw();
   hElectronCer->SetLineColor(4);
   hElectronCer->Draw("same");
   c->cd(4);
   hMassWithGoodGammaStar->SetFillColor(4);
   hMassWithGoodGammaStar->Draw();

   hMassWithGoodHitsAndBlocks->SetFillColor(5);
   hMassWithGoodHitsAndBlocks->Draw("same");

   c->cd(5);
   hPositironPosition->SetMarkerStyle(20);
   hPositironPosition->SetMarkerColor(2);
   hPositironPosition->SetMarkerSize(0.4);
   hPositironPosition->Draw("");
   hElectronPosition->SetMarkerStyle(20);
   hElectronPosition->SetMarkerColor(4);
   hElectronPosition->SetMarkerSize(0.4);
   hElectronPosition->Draw("same");
   hGammaPosition->SetMarkerStyle(20);
   hGammaPosition->SetMarkerColor(3);
   hGammaPosition->SetMarkerSize(0.4);
   hGammaPosition->Draw("same");

   //hGammaStarMassVsPi0Momentum->Draw("colz");
   c->cd(6);
   hElectronEnergyVMass->Draw("colz");

   c->cd(7);
/*   hElectronEnergyVPosition->Draw("colz");*/
   hPositronEnergyVMass->Draw("colz");

   c->cd(8);
   hGammaEnergyVMass->Draw("colz");


//   hGammaMassVPositronEnergy->Draw("colz");


   pi3treeOUT->StartViewer();

   c->SaveAs("results/pi0_3clusters.png");
   c->SaveAs("results/pi0_3clusters.pdf");

   return(0);
}
