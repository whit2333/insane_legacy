/*!
 */
Int_t energy_dependence(Int_t number=10) {
 
//   Create the pi0 chain 
   TChain chain("pi0results");
   while(aman->fRunQueue->size()>0) {
       chain.Add(Form("data/rootfiles/InSANE%d.0.root",aman->fRunQueue->back()));
       aman->fRunQueue->pop_back();
   }

   TCanvas * c = new TCanvas("pi0Canvas","pi0 canvas ");
   c->Divide(2,2);
   c->cd(1);
   chain.Draw("fNTracks:fReconstruction.fMass","fReconstruction[Iteration$].fMass<300.0","colz");
   c->cd(2);
   chain.Draw("fReconstruction.fPi0Momentum.Theta()*180.0/TMath::Pi():fReconstruction.fMass","fReconstruction.fMass<300","colz");
   c->cd(3);
   chain.Draw("fReconstruction.fAngle*180.0/TMath::Pi():fReconstruction[Iteration$].fPi0Momentum.Mag()","fReconstruction[Iteration$].fMass<300&&fReconstruction[Iteration$].fPi0Momentum.Mag()<6000&&fNTracks<5","colz");
   c->cd(4);
//   chain.Draw("fReconstruction.fAngle*180.0/TMath::Pi():fNTracks>>openingAngle1","fReconstruction.fMass<300","box,goff");
//   TH2F * h2 = (TH2F*)gROOT->FindObject("openingAngle1");
//   chain.Draw("fReconstruction[Iteration$].fMass:fReconstruction[].fAngle*180.0/TMath::Pi():fReconstruction[Iteration$].fMass","fReconstruction[Iteration$].fMass<300","ISO");
//   chain.Draw("fReconstruction.fPi0Momentum.Theta()*180.0/TMath::Pi():fReconstruction[Iteration$].fPi0Momentum.Mag()>>hAngleVsP(100,0,6000,100,30,50)",
//		   "fNTracks<5","colz");
   chain.Draw("fReconstruction.fPi0Momentum.Theta()*180.0/TMath::Pi():fReconstruction[Iteration$].fPi0Momentum.Phi()*180.0/TMath::Pi()>>hAngleVsP(100,-50,50,100,30,50)",
		   "fNTracks<5","colz");
   c->cd(4);
//   if(h2)h2->Draw("box,same");
return(0);
}
