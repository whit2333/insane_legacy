Int_t merge_chain(){

   /// Build up the queue 

//    aman->GetRunQueue()->push_back(72930);
//    aman->GetRunQueue()->push_back(72999);
//    aman->GetRunQueue()->push_back(72994);
/*   aman->GetRunQueue()->push_back(73003);*/
//    aman->GetRunQueue()->push_back(7300);
//    aman->GetRunQueue()->push_back(72919);
//    aman->GetRunQueue()->push_back(72921);
//    aman->GetRunQueue()->push_back(72922);
//    aman->GetRunQueue()->push_back(72925);
//    aman->GetRunQueue()->push_back(72926);


// running:
//    aman->GetRunQueue()->push_back(72915);
//    aman->GetRunQueue()->push_back(72925);
//    aman->GetRunQueue()->push_back(72926);

// old:
//    aman->GetRunQueue()->push_back(72931);
//    aman->GetRunQueue()->push_back(72932);

   /// Get the chains for run queue
   TChain * pichain =  aman->BuildChain("pi0results");
   TChain * pi3chain =  aman->BuildChain("3clusterPi0results");

   /// Merge chain into single tree
   TFile * f = new TFile("data/pi0events.root","RECREATE");
   f->cd();
   pichain->Merge(f,32000,"keep");
   std::cout << " pichain Merge Complete. \n";

   pi3chain->Merge(f,32000,"keep");
   std::cout << " pi3chain Merge Complete. \n";
//    detchain->Merge(f,32000,"keep,fast");
//    std::cout << " detchain Merge Complete. \n";


   f->Flush();

   delete pichain;
   delete pi3chain;

   return(0);
}
