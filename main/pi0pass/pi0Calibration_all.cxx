// Quadratic background function
Double_t background(Double_t *x, Double_t *par) {
      return par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
}

Double_t background1(Double_t *x, Double_t *par) {
      return par[0];
}

// Lorenzian Peak function
Double_t lorentzianPeak(Double_t *x, Double_t *par) {
     return (0.5*par[0]*par[1]/TMath::Pi()) / 
            TMath::Max( 1.e-10,(x[0]-par[2])*(x[0]-par[2]) 
                     + .25*par[1]*par[1]);
}

// Sum of background and peak function
Double_t fitFunction(Double_t *x, Double_t *par) {
     return background1(x,par) + lorentzianPeak(x,&par[1]);
}
//______________________________________________________________________________
Int_t pi0Calibration_all( Int_t aNumber = 1000, Int_t sector = -1) {

   //load_style("MultiSquarePlot");
   //gROOT->SetStyle("MultiSquarePlot");
   //gStyle->SetPalette(55);

   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();

   //aman->BuildQueue2("lists/para/5_9GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/para/4_7GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/para/none_NH3.txt");
   
   //aman->BuildQueue2("temp_para.txt");
   //aman->BuildQueue2("temp_perp2.txt");
   //aman->BuildQueue2("pi0_calib_runs.txt");
   aman->BuildQueue2("temp_pi0.txt");
   
   rman->SetRun(aman->fRunQueue->at(0));

   Int_t iPrint    = 50000;
   Int_t NCalib    = 20;
   Int_t NEventMax = 5e5;
   Int_t firstRun  = 72000;
   Int_t lastRun   = 73100;   
   Int_t series_0  = rman->GetCurrentRun()->fBigcalCalibrationSeries;  // series that data was replayed with
   Int_t series    = geocalc->GetLatestCalibrationSeries();

   Double_t E_photon_min    = 500.0;
   Double_t clusterTimeCut  = 20.0;
   Double_t clusterTimePeak = -40;
   Double_t mass_cut        = 60.0;
   Double_t mass_cut_end    = 8.0;
   Double_t mass_center     = 135.0;
   Double_t epsilon_start   = 0.10;
   Double_t epsilon_end     = 0.01;

   Int_t start_fit = 0;
   Double_t hist_maxEnergy = 300;

   Double_t mass_cut_step   = -1.0*(mass_cut - mass_cut_end)/float(NCalib);
   Double_t epsilon_step    = -1.0*(epsilon_start - epsilon_end)/float(NCalib);

   geocalc->SetCalibrationSeries(series_0);

   TChain * t = 0;
   t = aman->BuildChain("pi0results");
   if(!t) return(-1);
   Long64_t entries = t->GetEntries();
   std::cout << entries << " entries " << std::endl;

   Pi0Event * pi0event             = new Pi0Event();
   t->SetBranchAddress("pi0reconstruction",&pi0event);
   TClonesArray         * clusters = 0;
   InSANEReconstruction * recon    = 0;
   Pi0ClusterPair       * pair     = 0;

   const char * sourceTreeName = "betaDetectors1";
   const char * outputTreeName = "Pi0CalibEigen";

   TList * hists = new TList();
   THStack * hs  = new THStack("hs",Form("Calibration %d",aNumber));

   Pi0Calibration * cal1       = new Pi0Calibration();
   cal1->fIsPerp               = false;
   cal1->fEpsilonFraction      = epsilon_start;
   cal1->fEigenValueThreshold  = 1.0e-6;


   std::cout << " Calibrating sector " << sector << std::endl;
   switch( sector ) {
      case 0 : 
         // Bottom 9 rows
         cal1->InitActiveBlocks( 1,  1, 32,  9); 
         cal1->InitBlocks(       1,  1, 32,  9); 
         break;
      case 1 : 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  10, 32,  19);
         cal1->InitBlocks(       1,  10, 32,  19);
         break;
      case 2 : 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  20, 32,  29);
         cal1->InitBlocks(       1,  20, 32,  29);
         break;
      case 3 : 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  30, 32,  39);
         cal1->InitBlocks(       1,  30, 32,  39);
         break;
      case 4 : 
         // next 8 rows
         cal1->InitActiveBlocks( 1,  40, 30,  47);
         cal1->InitBlocks(       1,  40, 30,  47);
         break;
      case 5 : 
         // next 8 rows
         cal1->InitActiveBlocks( 1,  48, 30,  56);
         cal1->InitBlocks(       1,  48, 30,  56);
         break;
      case 6 : 
         // Bottom 9 rows
         cal1->InitActiveBlocks( 1,  1, 32,  9); 
         cal1->InitBlocks(       1,  1, 32,  9); 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  10, 32,  19,false);
         cal1->InitBlocks(       1,  10, 32,  19,false);
         break;
      case 7 : 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  20, 32,  29); // 
         cal1->InitBlocks(       1,  20, 32,  29); // 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  30, 32,  39,false); // 
         cal1->InitBlocks(       1,  30, 32,  39,false); // 
         break;
      case 8 : 
         // next 8 rows
         cal1->InitActiveBlocks( 1,  40, 30,  47); // 
         cal1->InitBlocks(       1,  40, 30,  47); // 
         // next 8 rows
         cal1->InitActiveBlocks( 1,  48, 30,  56,false); // 
         cal1->InitBlocks(       1,  48, 30,  56,false); // 
         break;
      case 9 : 
         // Bottom - Protvino
         cal1->InitActiveBlocks(1,1,32,32);
         cal1->InitBlocks(1,1,32,32); // 
         break;
      case 10 : 
         // Top - RCS
         cal1->InitActiveBlocks(1,33,30,56);
         cal1->InitBlocks(1,33,30,56); // 
         break;

      case 11:
         // Protvino Vertical Strip only
         cal1->ClearBlocks();
         for(   int j = 5; j<=32; j++ ) {
            for(int i = 5; i<=26; i++ ) {
               if( geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;
      case 12:
         // RCS Vertical Strip only
         cal1->ClearBlocks();
         for(   int j =33; j<=51; j++ ) {
            for(int i = 5; i<=25; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 13:
         // left part of Horizontal Strip
         cal1->ClearBlocks();
         // Protvino part
         for(   int j =22; j<=32; j++ ) {
            for(int i = 1; i<=16; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=41; j++ ) {
            for(int i = 1; i<=15; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 14:
         // right part of Horizontal Strip
         cal1->ClearBlocks();
         // Protvino part
         for(   int j =22; j<=32; j++ ) {
            for(int i = 17; i<=32; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=41; j++ ) {
            for(int i = 16; i<=30; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 15:
         // Full Protvino cross
         cal1->ClearBlocks();
         for(   int j = 1; j<=32; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               if( geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         //mass_cut        = 200;
         //mass_cut_step   = -1.0*(mass_cut - mass_cut_end)/float(NCalib);
         break;

      case 16:
         // Full RCS cross 
         cal1->ClearBlocks();
         for(   int j =33; j<=56; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         //mass_cut        = 200;
         //mass_cut_step   = -1.0*(mass_cut - mass_cut_end)/float(NCalib);
         break;

      case 17:
         // Full Horizontal Strip
         cal1->ClearBlocks();
         // Protvino part
         for(   int j =22; j<=32; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=41; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         //mass_cut        = 200;
         //mass_cut_step   = -1.0*(mass_cut - mass_cut_end)/float(NCalib);
         break;

      case 18:
         // All corners with entire detector active. 
         cal1->ClearBlocks();
         // Protvino part
         for(   int j = 1; j<=32; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               cal1->AddActiveBlock(i,j);
               if( !(geocalc->IsBlockInCross1(i,j)) ){
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=56; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               cal1->AddActiveBlock(i,j);
               if( !(geocalc->IsBlockInCross1(i,j)) ){
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      default : 
         // Bottom
         //cal1->InitActiveBlocks(1,1,32,32);
         //cal1->InitBlocks(1,1,32,32); // 
         // Top
         cal1->InitActiveBlocks(1,33,30,42);
         cal1->InitBlocks(1,33,30,42); // 
         // Middle
         //jcal1->InitActiveBlocks(1,20,32,40);
         //jcal1->InitBlocks(1,20,32,40); // 
         break;
   }

   cal1->Initialize();
   cal1->LoadEpsilonFromSeries(series_0,series);
   std::cout << "Initializing epsilon values with the difference between " << series_0 << " and " << series << std::endl;

   // --------------------------------------
   //
   TCanvas * c = new TCanvas("pi0MassIterations","Pi0 mass iterations");
   c->Divide(2,2);
   c->cd(2)->Divide(1,2);
   c->cd(4)->Divide(1,2);

   TCanvas * c2 = new TCanvas("c2","Pi0 mass iterations");
   c2->Divide(2,1);

   TCanvas * c5 = new TCanvas("Pi0diagnostics","Pi0 cuts");
   c5->Divide(2,2);

   Int_t iPeak1 = 0;
   Int_t jPeak1 = 0;
   Int_t iPeak2 = 0;
   Int_t jPeak2 = 0;

   // --------------------------------------
   // Fit function
   // create a TF1 with the range from 0 to 3 and 6 parameters
   TF1 *fitFcn = new TF1("fitFcn",fitFunction,0,300,4);
   fitFcn->SetNpx(200);
   fitFcn->SetLineWidth(2);
   fitFcn->SetLineColor(kMagenta);

   // first try without starting values for the parameters
   // This defaults to 1 for each param. 
   // this results in an ok fit for the polynomial function
   // however the non-linear part (lorenzian) does not 
   // respond well.

   fitFcn->SetParameter(0,10); // p0
   fitFcn->SetParLimits(0,0,100);
   fitFcn->SetParameter(1,100); // p0
   fitFcn->SetParLimits(1,0,1.0e6);
   fitFcn->SetParameter(2,10); // width
   fitFcn->SetParLimits(2,0,1.0e2);
   fitFcn->SetParameter(3,135);   // peak
   fitFcn->SetParLimits(3,100,190);
   //// second try: set start values for some parameters
   //fitFcn->SetParameter(4,0.2); // width
   //fitFcn->SetParameter(5,1);   // peak
   //histo->Fit("fitFcn","V+","ep");
   
   // close the mysql connetion before going into calibration/event loop
   dbman->CloseConnections();

   Int_t i_chain_event = 6000;
   // --------------------------------------
   // Calibration loop
   for(int iCalib = 0; iCalib<NCalib; iCalib++){


      std::cout << " Mass cut is " << mass_cut << " MeV" << std::endl;
      std::cout << " centered at " << mass_center << std::endl;
      cal1->Reset();

      //if(iCalib>1 && iCalib%10 == 0) {
      //    cal1->fEpsilonFraction = cal1->fEpsilonFraction/2.0;
      //}
      cal1->fEpsilonFraction += epsilon_step;
      std::cout << " Epsilon Fraction is " << cal1->fEpsilonFraction << std::endl;

      TH2F * AnglevsMassHist = new TH2F(Form("AnglevsMass-%d" , iCalib) , Form("AnglevsMass-%d" , iCalib) , 100 , 50 , hist_maxEnergy   , 40 , 3 , 13);
      TH2F * YvsMassHist     = new TH2F(Form("YvsMass-%d"     , iCalib) , Form("YvsMass-%d"     , iCalib) , 100 , 50 , hist_maxEnergy   , 56 , 1 , 57);
      TH2F * YvsMassHist2    = new TH2F(Form("YvsMass2-%d"    , iCalib) , Form("YvsMass-%d"     , iCalib) , 100 , 50 , hist_maxEnergy   , 56 , 1 , 57);
      TH2F * XvsMassHist     = new TH2F(Form("XvsMass-%d"     , iCalib) , Form("Mass vs x-%d"   , iCalib) , 100 , 50 , hist_maxEnergy   , 32 , 1 , 33);
      TH2F * XvsMassHist2    = new TH2F(Form("XvsM2-%d"       , iCalib) , Form("Mass vs x-%d"   , iCalib) , 100 , 50 , hist_maxEnergy   , 32 , 1 , 33);
      TH1F * massHist        = new TH1F(Form("mass-%d"        , iCalib) , Form("mass-%d"        , iCalib) , 100 , 50 , hist_maxEnergy);

      TH1F * timeHist0        = new TH1F(Form("time0-%d"        , iCalib) , Form("time-%d"        , iCalib) , 100 , -100 , 100);
      TH1F * timeHist1        = new TH1F(Form("time1-%d"        , iCalib) , Form("time-%d"        , iCalib) , 100 , -100 , 100);
      TH1F * timeHist2        = new TH1F(Form("time2-%d"        , iCalib) , Form("time-%d"        , iCalib) , 100 , -100 , 100);
      TH1F * timeHist3        = new TH1F(Form("time3-%d"        , iCalib) , Form("time-%d"        , iCalib) , 100 , -100 , 100);
      TH1F * timeHist4        = new TH1F(Form("time4-%d"        , iCalib) , Form("time-%d"        , iCalib) , 100 , -100 , 100);
      TH1F * timeHist5        = new TH1F(Form("time5-%d"        , iCalib) , Form("time-%d"        , iCalib) , 100 , -100 , 100);

      TH2F * XvsY0           = new TH2F(Form("XvsY0-%d"       , iCalib) , Form("Y vs x-%d"   , iCalib)    ,  32 , 1 , 33, 56 , 1 , 57);
      //TH1F * clusterTiming1        = new TH1F(Form("clustertiming1-%d",iCalib),Form("cluster time 1-%d",iCalib),100,-150,150);
      //TH1F * clusterTiming2        = new TH1F(Form("clustertiming2-%d",iCalib),Form("cluster time 2-%d",iCalib),100,-150,150);

      hists->Add(massHist);
      hs->Add(   massHist);
      massHist->SetLineWidth(2);
      massHist->SetLineColor(1);

      Long64_t i_good_events = 0;

      for(int ievent = 0 ; ievent < entries && ievent < NEventMax ; ievent++){

         Int_t bytes =  t->GetEntry(i_chain_event);
         if(ievent%iPrint == 0) {
            //std::cout << bytes << " bytes read" << std::endl;
            std::cout << "Event: " << i_chain_event << ", " << ievent << ", " << i_good_events << std::endl;
            std::cout << t->GetFile()->GetName() << std::endl;

         }

         i_chain_event++;
         if( i_chain_event >= entries ) i_chain_event = 0;

         for(int i = 0; i< pi0event->fClusterPairs->GetEntries() ; i++){
            pair  = (Pi0ClusterPair*)(*(pi0event->fClusterPairs))[i];
            //recon = (InSANEReconstruction*)(*(pi0event->fReconstruction))[i];

            iPeak1 = pair->fCluster1.fiPeak;
            jPeak1 = pair->fCluster1.fjPeak;
            iPeak2 = pair->fCluster2.fiPeak;
            jPeak2 = pair->fCluster2.fjPeak;

            // The cluster centers must belong to separte trigger groups.
            if( geocalc->GetTriggerGroup(iPeak1,jPeak1) == geocalc->GetTriggerGroup(iPeak2,jPeak2) ) continue;

            if( (pair->fCluster1.GetCorrectedEnergy() > E_photon_min) && (pair->fCluster2.GetCorrectedEnergy() > E_photon_min )) 
            if( pair->fAngle > 4.0*degree) 
               if( pair->fCluster1.fIsGood && pair->fCluster2.fIsGood)
               if( pair->fCluster1.fCherenkovBestADCSum < 0.1 && pair->fCluster2.fCherenkovBestADCSum < 0.1 )
               { 
                  if( !(geocalc->IsBlockOnPerimeter(iPeak1,jPeak1))   )
                  if( !(geocalc->IsBlockOnPerimeter(iPeak2,jPeak2))   )
                  {
                     if( pair->fCluster1.fNNonZeroBlocks > 4)
                        if( pair->fCluster2.fNNonZeroBlocks > 4)
                           if( cal1->IsActivePair(pair) )
                              //if( cal1->IsPairInList(pair) )
                              //if( !(cal1->IsBlockOnListPerimeter(iPeak1,jPeak1)) )
                              //if( !(cal1->IsBlockOnListPerimeter(iPeak2,jPeak2)) )
                           {


                              cal1->SetClusterPair(pair);
                              if( TMath::Abs(cal1->CalculateMass() - mass_center) < mass_cut) { 

                              timeHist0->Fill(pair->fCluster1.fClusterTime1);
                              timeHist1->Fill(pair->fCluster2.fClusterTime1);
                              timeHist2->Fill(pair->fCluster1.fClusterTime2);
                              timeHist3->Fill(pair->fCluster2.fClusterTime2);

               if( TMath::Abs(pair->fCluster1.fClusterTime1-clusterTimePeak) < clusterTimeCut || TMath::Abs(pair->fCluster1.fClusterTime2-clusterTimePeak) < clusterTimeCut ) 
               if( TMath::Abs(pair->fCluster2.fClusterTime1-clusterTimePeak) < clusterTimeCut || TMath::Abs(pair->fCluster2.fClusterTime2-clusterTimePeak) < clusterTimeCut )  {

                              timeHist4->Fill(pair->fCluster1.fClusterTime1);
                              timeHist4->Fill(pair->fCluster1.fClusterTime2);
                              timeHist5->Fill(pair->fCluster2.fClusterTime1);
                              timeHist5->Fill(pair->fCluster2.fClusterTime2);
                                 //clusterTiming1->Fill(pair->fCluster1.fClusterTime1);
                                 //clusterTiming2->Fill(pair->fCluster2.fClusterTime1);
                                 //std::cout << " mass = " << recon->fMass << std::endl;
                                 //pair->fMass = recon->fMass;
                                 i_good_events++;
                                 cal1->SetClusterPair(pair);
                                 cal1->BuildMatricies();
                                 massHist->Fill(cal1->fLastMass);
                                 YvsMassHist->Fill( cal1->fLastMass, jPeak1);
                                 YvsMassHist2->Fill(cal1->fLastMass, jPeak2);
                                 XvsMassHist->Fill( cal1->fLastMass, iPeak1);
                                 XvsMassHist2->Fill(cal1->fLastMass, iPeak2);
                                 XvsY0->Fill(iPeak1,jPeak1);
                                 XvsY0->Fill(iPeak2,jPeak2);
                                 AnglevsMassHist->Fill(cal1->fLastMass, pair->fAngle/degree);
               }
                              }
                           }
                  }
               }

         } // pair loop
      } // event loop

      c->cd(1); 

      if( iCalib > start_fit ) { 
         massHist->Fit(fitFcn,"E,W","",mass_center-mass_cut, mass_center+mass_cut);
         mass_center = fitFcn->GetParameter(3);
      }

      hs->Draw("nostack");

      c->cd(2)->cd(1);
      YvsMassHist->Draw("colz");
      c->cd(2)->cd(2);
      YvsMassHist2->Draw("colz");

      c->cd(4)->cd(1);
      XvsMassHist->Draw("colz");
      c->cd(4)->cd(2);
      XvsMassHist2->Draw("colz");

      c->cd(3);
      AnglevsMassHist->Draw("colz");

      c->Update();
      c->SaveAs(Form("results/pi0calibration/pi0Calibration_all_%d_%d.png",aNumber,iCalib));
      c->SaveAs(Form("results/pi0calibration/pi0Calibration_all_%d_%d.pdf",aNumber,iCalib));

      cal1->Print();
      cal1->ExecuteCalibration();
      cal1->ViewStatus();

      cal1->fCanvas->SaveAs(Form("results/pi0calibration/pi0Calibration_all_%d_c1_%d.png",aNumber,iCalib));
      cal1->fCanvas->SaveAs(Form("results/pi0calibration/pi0Calibration_all_%d_c1_%d.pdf",aNumber,iCalib));

      c2->cd(1);
      gPad->SetLogy(true);
      timeHist0->Draw();
      timeHist1->SetLineColor(2);
      timeHist2->SetLineColor(4);
      timeHist3->SetLineColor(8);
      timeHist1->Draw("same");
      timeHist2->Draw("same");
      timeHist3->Draw("same");

      //XvsY0->Draw("colz");

      c2->cd(2);
      gPad->SetLogy(true);
      timeHist4->Draw();
      timeHist5->SetLineColor(2);
      timeHist5->Draw("same");

      c2->SaveAs(Form("results/pi0calibration/pi0Calibration_all_%d_c2_%d.png",aNumber,iCalib));

      massHist->SetLineWidth(1);
      massHist->SetLineColor(iCalib+1);

      // Shrink the mass cut 
      mass_cut += mass_cut_step;
      TF1 * ff = massHist->GetFunction("fitFcn");
      if(ff) ff->SetLineWidth(1);

      //c5->cd(1);
      //clusterTiming1->Draw();
      //c5->cd(2);
      //clusterTiming2->Draw();
      //c5->SaveAs(Form("results/pi0calibration/pi0Calibration_all_%d_cuts_%d.png",aNumber,iCalib));
   }

   // Set the series to the last one so the coefficients can be updated.
   //geocalc->SetCalibrationSeries(series);
   Int_t newSeries = cal1->CreateNewCalibrationSeries(firstRun,lastRun);
   std::cout << " Created new calibration series, " << newSeries;
   std::cout << ", for runs " << firstRun << "-" << lastRun << std::endl;

   return(0);
}

