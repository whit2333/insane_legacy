// Quadratic background function
Double_t background(Double_t *x, Double_t *par) {
      return par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
}

Double_t background2(Double_t *x, Double_t *par) {
      return par[0] + par[1]*x[0];
}
Double_t background1(Double_t *x, Double_t *par) {
      return par[0];
}

// Lorenzian Peak function
Double_t lorentzianPeak(Double_t *x, Double_t *par) {
     return (0.5*par[0]*par[1]/TMath::Pi()) / 
            TMath::Max( 1.e-10,(x[0]-par[2])*(x[0]-par[2]) 
                     + .25*par[1]*par[1]);
}

// Sum of background and peak function
Double_t fitFunction(Double_t *x, Double_t *par) {
     return lorentzianPeak(x,&par[0]) + background2(x,&par[3]) ;
}
//______________________________________________________________________________
Int_t pi0CalibrationCompare_swap( Int_t aNumber = 2000, Int_t sector = -1) {

   //load_style("MultiSquarePlot");
   //gROOT->SetStyle("MultiSquarePlot");

   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();

   Int_t iPrint    = 50000;
   Int_t NCalib    = 1; //
   Int_t NEventMax = 5e6;

   aman->BuildQueue2("pi0_calib_runs.txt");

   rman->SetRun(aman->fRunQueue->at(0));

   //Int_t firstRun  = 72000;
   //Int_t lastRun   = 72900;   //aman->fRunQueue->at(aman->fRunQueue->size()-1);
   //Int_t series_0  = geocalc->GetLatestCalibrationSeries(72424);//rman->GetCurrentRun()->fBigcalCalibrationSeries;//93;//37;//geocalc->GetLatestCalibrationSeries(firstRun);
   //Int_t series    = geocalc->GetLatestCalibrationSeries(72424);

   Int_t firstRun  = 72001;
   Int_t lastRun   = 73100;   //aman->fRunQueue->at(aman->fRunQueue->size()-1);
   Int_t series_0  = geocalc->GetLatestCalibrationSeries(aman->fRunQueue->at(aman->fRunQueue->size()-1));
   Int_t series    = geocalc->GetLatestCalibrationSeries(72913);

   Double_t clusterTimeCut  = 60.0;
   Double_t clusterTimePeak = 0.0;
   Double_t mass_cut        = 150;//35.0;
   Double_t mass_cut_end    = 150;//35.0;
   Double_t mass_center     = 135.0;
   Double_t mass_cut_step   = -1.0*(mass_cut - mass_cut_end)/float(NCalib);
   Double_t E_photon_min    = 200.0;

   std::cout << " Data replayed with series  : " << series_0 << std::endl;
   std::cout << " Initializing epsilons with : " << series << std::endl;
   geocalc->SetCalibrationSeries(series_0);

   TChain * t = 0;
   t = aman->BuildChain("pi0results");
   if(!t) return(-1);
   Long64_t entries = t->GetEntries();
   std::cout << entries << " entries " << std::endl;

   Pi0Event * pi0event             = new Pi0Event();
   t->SetBranchAddress("pi0reconstruction",&pi0event);
   TClonesArray         * clusters = 0;
   InSANEReconstruction * recon    = 0;
   Pi0ClusterPair       * pair     = 0;

   TFile * pi0OutputFile = new TFile("data/pi0/AllCombinedPi0.root","UPDATE");
   TTree * outputTree    = new TTree("pi0Compare","pi0 combined events");
   outputTree->Branch("pi0Event","Pi0Event",&pi0event);

   const char * sourceTreeName = "betaDetectors1";
   const char * outputTreeName = "Pi0CalibEigen";

   TList * hists = new TList();
   THStack * hs  = new THStack("hs","test stacked histograms");

   // ----------------------------
   // Calibrator 
   Pi0Calibration * cal1       = new Pi0Calibration();
   cal1->fIsPerp               = false;
   cal1->fEigenValueThreshold  = 1.0e5;
   cal1->fEpsilonFraction      = 0.01;

   std::cout << " Calibrating sector " << sector << std::endl;
   switch( sector ) {
      case 0 : 
         // Bottom 9 rows
         cal1->InitActiveBlocks( 1,  1, 32,  9); 
         cal1->InitBlocks(       1,  1, 32,  9); 
         break;
      case 1 : 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  10, 32,  19);
         cal1->InitBlocks(       1,  10, 32,  19);
         break;
      case 2 : 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  20, 32,  29);
         cal1->InitBlocks(       1,  20, 32,  29);
         break;
      case 3 : 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  30, 32,  39);
         cal1->InitBlocks(       1,  30, 32,  39);
         break;
      case 4 : 
         // next 8 rows
         cal1->InitActiveBlocks( 1,  40, 30,  47);
         cal1->InitBlocks(       1,  40, 30,  47);
         break;
      case 5 : 
         // next 8 rows
         cal1->InitActiveBlocks( 1,  48, 30,  56);
         cal1->InitBlocks(       1,  48, 30,  56);
         break;
      case 6 : 
         // Bottom 9 rows
         cal1->InitActiveBlocks( 1,  1, 32,  9); 
         cal1->InitBlocks(       1,  1, 32,  9); 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  10, 32,  19,false);
         cal1->InitBlocks(       1,  10, 32,  19,false);
         break;
      case 7 : 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  20, 32,  29); // 
         cal1->InitBlocks(       1,  20, 32,  29); // 
         // next 10 rows
         cal1->InitActiveBlocks( 1,  30, 32,  39,false); // 
         cal1->InitBlocks(       1,  30, 32,  39,false); // 
         break;
      case 8 : 
         // next 8 rows
         cal1->InitActiveBlocks( 1,  40, 30,  47); // 
         cal1->InitBlocks(       1,  40, 30,  47); // 
         // next 8 rows
         cal1->InitActiveBlocks( 1,  48, 30,  56,false); // 
         cal1->InitBlocks(       1,  48, 30,  56,false); // 
         break;
      case 9 : 
         // Bottom - Protvino
         cal1->InitActiveBlocks(1,1,32,32);
         cal1->InitBlocks(1,1,32,32); // 
         break;
      case 10 : 
         // Top - RCS
         cal1->InitActiveBlocks(1,33,30,56);
         cal1->InitBlocks(1,33,30,56); // 
         break;
      case 11:
         // Protvino Vertical Strip only
         cal1->ClearBlocks();
         for(   int j = 1; j<=21; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               if( geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;
      case 12:
         // RCS Vertical Strip only
         cal1->ClearBlocks();
         for(   int j =42; j<=56; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 13:
         // left part of Horizontal Strip
         cal1->ClearBlocks();
         // Protvino part
         for(   int j =22; j<=32; j++ ) {
            for(int i = 1; i<=16; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=41; j++ ) {
            for(int i = 1; i<=15; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 14:
         // right part of Horizontal Strip
         cal1->ClearBlocks();
         // Protvino part
         for(   int j =22; j<=32; j++ ) {
            for(int i = 17; i<=32; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=41; j++ ) {
            for(int i = 16; i<=30; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 15:
         // Full Protvino cross
         cal1->ClearBlocks();
         for(   int j = 1; j<=32; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               if( geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 16:
         // Full RCS cross 
         cal1->ClearBlocks();
         for(   int j =33; j<=56; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 17:
         // Full Horizontal Strip
         cal1->ClearBlocks();
         // Protvino part
         for(   int j =22; j<=32; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=41; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               if(geocalc->IsBlockInCross1(i,j) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 18:
         // All corners with entire detector active. 
         cal1->ClearBlocks();
         // Protvino part
         for(   int j = 1; j<=32; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               cal1->AddActiveBlock(i,j);
               if( !(geocalc->IsBlockInCross1(i,j)) ){
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=56; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               cal1->AddActiveBlock(i,j);
               if( !(geocalc->IsBlockInCross1(i,j)) ){
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 19:
         // All corners with entire detector active. 
         cal1->ClearBlocks();
         // Protvino part
         for(   int j = 1; j<=32; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               cal1->AddActiveBlock(i,j);
               if( (geocalc->IsBlockInCross3(i,j)) ){
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =33; j<=56; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               cal1->AddActiveBlock(i,j);
               if( (geocalc->IsBlockInCross3(i,j)) ){
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;


      case 20:
         // top middle and bottom middle for ets. 
         cal1->ClearBlocks();
         // Protvino part
         for(   int j = 1; j<=22; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               cal1->AddActiveBlock(i,j);
               if( (geocalc->IsBlockInCross3(i,j)) ){
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =42; j<=56; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               cal1->AddActiveBlock(i,j);
               if( (geocalc->IsBlockInCross3(i,j)) ){
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 21:
         // top middle and bottom middle for ets. 
         cal1->ClearBlocks();
         // Protvino part
         for(   int j = 1; j<=22; j++ ) {
            for(int i = 1; i<=32; i++ ) {
               if( (geocalc->IsBlockInCross1(i,j)) ){
               cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =42; j<=56; j++ ) {
            for(int i = 1; i<=30; i++ ) {
               if( (geocalc->IsBlockInCross1(i,j)) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      case 22:
         // top left and bottom left for ets. 
         cal1->ClearBlocks();
         // Protvino part
         for(   int j = 1; j<=20; j++ ) {
            for(int i = 1; i<=16; i++ ) {
               if( (geocalc->IsBlockInCross1(i,j)) ){
               cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         // RCS part
         for(   int j =42; j<=56; j++ ) {
            for(int i = 16; i<=30; i++ ) {
               if( (geocalc->IsBlockInCross1(i,j)) ){
                  cal1->AddActiveBlock(i,j);
                  cal1->AddBlock(i,j);
               }
            }
         }
         break;

      default : 
         cal1->ClearBlocks();
         // Bottom
         //cal1->InitActiveBlocks(1,1,32,32);
         //cal1->InitBlocks(1,1,32,32); // 
         // Top
         //cal1->InitActiveBlocks(1,33,30,42);
         //cal1->InitBlocks(1,33,30,42); // 
         // Middle
         //jcal1->InitActiveBlocks(1,20,32,40);
         //jcal1->InitBlocks(1,20,32,40); // 
         // Entire calorimeter 
         cal1->InitActiveBlocks(1,1,32,56);
         cal1->InitBlocks(1,1,32,56);
         break;
   }

   cal1->Initialize();
   cal1->LoadEpsilonFromSeries(series_0,series);

   // --------------------------------------
   //
   TCanvas * c = new TCanvas("pi0MassIterations","Pi0 mass iterations");
   c->Divide(2,2);
   //c->cd(2)->Divide(1,2);
   //c->cd(4)->Divide(1,2);

   TCanvas * c3 = new TCanvas("massVsE","Pi0 mass v E");
   c3->Divide(2,3);

   TCanvas * c4 = new TCanvas("etaMass","eta Mass");

   TCanvas * c5 = new TCanvas("Pi0diagnostics","Pi0 cuts");
   c5->Divide(2,2);

   Int_t iPeak1 = 0;
   Int_t jPeak1 = 0;
   Int_t iPeak2 = 0;
   Int_t jPeak2 = 0;

   // --------------------------------------
   // Fit function
   // create a TF1 with the range from 0 to 3 and 6 parameters
   TF1 *fitFcn = new TF1("fitFcn",fitFunction,0,300,5);
   fitFcn->SetNpx(200);
   fitFcn->SetLineWidth(2);
   fitFcn->SetLineColor(kMagenta);

   // first try without starting values for the parameters
   // This defaults to 1 for each param. 
   // this results in an ok fit for the polynomial function
   // however the non-linear part (lorenzian) does not 
   // respond well.

   fitFcn->SetParameter(0,100); // p0
   fitFcn->SetParLimits(0,0,1.0e6);
   fitFcn->SetParameter(1,10); // width
   fitFcn->SetParLimits(1,0,1.0e2);
   fitFcn->SetParameter(2,135);   // peak
   fitFcn->SetParLimits(2,110,160);
   fitFcn->SetParameter(3,10); // p0
   fitFcn->SetParLimits(3,0,100);
   fitFcn->SetParameter(4,0);
   //fitFcn->SetParLimits(4,0,10);
   //fitFcn->SetParameter(5,0);
   //fitFcn->SetParLimits(5,0,100);
   
   // close the mysql connetion before going into calibration/event loop
   dbman->CloseConnections();

   TFile * fHists = new TFile(Form("results/pi0calibration/pi0CalibrationCompare_para_%d.root",aNumber),"UPDATE");

   // --------------------------------------
   // Calibration loop
   for(int iCalib = 0; iCalib<NCalib; iCalib++){

      std::cout << " Mass cut is " << mass_cut << " MeV" << std::endl;
      std::cout << " centered at " << mass_center << std::endl;
      cal1->Reset();

      fHists->cd();
      TH2F * AnglevsMassHist = new TH2F(Form("AnglevsMass-%d",iCalib),Form("%d AnglevsMass-%d",aNumber,iCalib),100,50,200,40,3,13);
      TH2F * etaAnglevsMassHist = new TH2F(Form("etaAnglevsMass-%d",iCalib),Form("%d angle Vs #eta-%d",aNumber,iCalib),100,200,1000,40,20,40);

      TH2F * YvsMassHist     = new TH2F(Form("YvsMass-%d",iCalib),Form("%d YvsMass-%d",  aNumber,iCalib),100,50,200,56,1,57);
      TH2F * YvsMassHist2    = new TH2F(Form("YvsMass2-%d",iCalib),Form("%d YvsMass-%d", aNumber,iCalib),100,50,200,56,1,57);
      TH2F * XvsMassHist     = new TH2F(Form("XvsMass-%d",iCalib),Form("%d Mass vs x-%d",aNumber,iCalib),100,50,200,32,1,33);
      TH2F * XvsMassHist2    = new TH2F(Form("XvsM2-%d",iCalib),Form("%d Mass vs x-%d",  aNumber,iCalib),100,50,200,32,1,33);

      TH2F * etaYvsMassHist     = new TH2F(Form("etaYvsMass-%d"  , iCalib) , Form("%d #eta YvsMass-%d"  ,aNumber , iCalib) , 100 , 200 , 1000 , 56 , 1 , 57);
      TH2F * etaYvsMassHist2    = new TH2F(Form("etaYvsMass2-%d" , iCalib) , Form("%d #eta YvsMass-%d"  ,aNumber , iCalib) , 100 , 200 , 1000 , 56 , 1 , 57);
      TH2F * etaXvsMassHist     = new TH2F(Form("etaXvsMass-%d"  , iCalib) , Form("%d #eta Mass vs x-%d",aNumber , iCalib) , 100 , 200 , 1000 , 32 , 1 , 33);
      TH2F * etaXvsMassHist2    = new TH2F(Form("etaXvsM2-%d"    , iCalib) , Form("%d #eta Mass vs x-%d",aNumber , iCalib) , 100 , 200 , 1000 , 32 , 1 , 33);

      TH2F * EvsMassHist     = new TH2F(Form("EvsMass-%d",iCalib),Form("%d Energy1 vs Mass %d",aNumber,iCalib),200,50,850,50,0,3000);
      TH2F * EvsMassHist2    = new TH2F(Form("EvsM2-%d",iCalib),  Form("%d Energy2 vs Mass %d",aNumber,iCalib),200,50,850,50,0,3000);
      TH2F * DeltaEvsMassHist     = new TH2F(Form("DeltaEvsMass-%d",iCalib),Form("%d Delta Energy1 vs Mass %d",aNumber, iCalib),200,50,850,50,0,1000);
      TH2F * DeltaEvsMassHist2    = new TH2F(Form("DeltaEvsM2-%d",iCalib),  Form("%d Delta Energy2 vs Mass %d",aNumber, iCalib),200,50,850,50,0,1000);
      TH2F * EvsDelta     = new TH2F(Form("EVsDelta-%d",iCalib),  Form("%d Energy1 vs Delta %d",aNumber,iCalib),50,0,3000,200,0,1000);
      TH2F * EvsDelta2    = new TH2F(Form("EVsDelta2-%d",iCalib), Form("%d Energy2 vs Delta %d",aNumber,iCalib),50,0,3000,200,0,1000);

      TH1F * massHist        = new TH1F(Form("mass-%d",iCalib),Form("%d mass-%d",aNumber, iCalib),100,50,200);
      TH1F * pi0MassHist     = new TH1F("pi0MassHist", "#pi^{0} mass",200,0,500);
      TH1F * etaMassHist     = new TH1F("etaMassHist", "#eta mass",200,50,850);
      TH1F * etaMassHist2    = new TH1F("etaMassHist2","#eta mass",200,50,850);
      TH1F * etaMassHist3    = new TH1F("etaMassHist3","#eta mass",200,50,850);

      TH1F * clusterTiming1        = new TH1F(Form("clustertiming1-%d",iCalib),Form("cluster time 1-%d",iCalib),100,-150,150);
      TH1F * clusterTiming2        = new TH1F(Form("clustertiming2-%d",iCalib),Form("cluster time 2-%d",iCalib),100,-150,150);
      TH1F * clusterTiming3        = new TH1F(Form("clustertiming3-%d",iCalib),Form("cluster time 1-%d",iCalib),100,-150,150);
      TH1F * clusterTiming4        = new TH1F(Form("clustertiming4-%d",iCalib),Form("cluster time 2-%d",iCalib),100,-150,150);
      
      hists->Add(massHist);
      hs->Add(   massHist);
      massHist->SetLineWidth(2);
      massHist->SetLineColor(1);

      for(int ievent = 0 ; ievent < entries && ievent < NEventMax ; ievent++){

         if(ievent%iPrint == 0) std::cout << "Event: " << ievent << std::endl;
         Int_t bytes =  t->GetEntry(ievent);
         //std::cout << bytes << " bytes read" << std::endl;
         for(int i = 0; i< pi0event->fClusterPairs->GetEntries() ; i++){
            pair  = (Pi0ClusterPair*)(*(pi0event->fClusterPairs))[i];
            //recon = (InSANEReconstruction*)(*(pi0event->fReconstruction))[i];

            iPeak1 = pair->fCluster1.fiPeak;
            jPeak1 = pair->fCluster1.fjPeak;
            iPeak2 = pair->fCluster2.fiPeak;
            jPeak2 = pair->fCluster2.fjPeak;

            clusterTiming1->Fill(pair->fCluster1.fClusterTime1);
            clusterTiming2->Fill(pair->fCluster2.fClusterTime1);

            // The cluster centers must belong to separte trigger groups.
            if( geocalc->GetTriggerGroup(iPeak1,jPeak1) == geocalc->GetTriggerGroup(iPeak2,jPeak2) ) continue;

            if( (pair->fCluster1.GetCorrectedEnergy() > E_photon_min) && (pair->fCluster2.GetCorrectedEnergy() > E_photon_min ) ) 
            if( pair->fAngle > 4.0*degree) 
            //if(TMath::Abs(pair->fMass - mass_center) < mass_cut) 
               //if( TMath::Abs(pair->fCluster1.fClusterTime1-clusterTimePeak) < clusterTimeCut || TMath::Abs(pair->fCluster1.fClusterTime2-clusterTimePeak) < clusterTimeCut ) 
               //if( TMath::Abs(pair->fCluster2.fClusterTime1-clusterTimePeak) < clusterTimeCut || TMath::Abs(pair->fCluster2.fClusterTime2-clusterTimePeak) < clusterTimeCut ) 
               //if( pair->fCluster1.fIsGood && pair->fCluster2.fIsGood)
               if( pair->fCluster1.fCherenkovBestADCSum < 0.2 && pair->fCluster2.fCherenkovBestADCSum < 0.2 )
               { 
                  //if( !( (jPeak1 == 6)  &&  (iPeak1 == 18) )   )
                  //if( !( (jPeak2 == 6)  &&  (iPeak2 == 18) )   )
                  //if( !( (jPeak1 == 26) &&  (iPeak1 == 17) )   )
                  //if( !( (jPeak2 == 26) &&  (iPeak2 == 17) )   )
                  //if( !( (jPeak1 == 3) &&  (iPeak1 == 17) )   )
                  //if( !( (jPeak2 == 3) &&  (iPeak2 == 17) )   )
                  //if( !( (jPeak1 == 47) &&  (iPeak1 == 16) )   )
                  //if( !( (jPeak2 == 47) &&  (iPeak2 == 16) )   )
                  //if( !( (jPeak1 == 9 ) &&  (iPeak1 == 17) )   )
                  //if( !( (jPeak2 == 9 ) &&  (iPeak2 == 17) )   )
                  //if( !( (jPeak1 == 8 ) &&  (iPeak1 == 17) )   )
                  //if( !( (jPeak2 == 8 ) &&  (iPeak2 == 17) )   )
                  //if( !( (jPeak1 == 7 ) &&  (iPeak1 == 17) )   )
                  //if( !( (jPeak2 == 7 ) &&  (iPeak2 == 17) )   )
                  //if( !( (jPeak1 == 8 ) &&  (iPeak1 ==  8) )   )
                  //if( !( (jPeak2 == 8 ) &&  (iPeak2 ==  8) )   )
                  {

                     if( pair->fCluster1.fNNonZeroBlocks >= 3)
                        if( pair->fCluster2.fNNonZeroBlocks >= 3)
                           if( cal1->IsActivePair(pair) )
                              //if( cal1->IsPairInList(pair) )
                              //if( !(cal1->IsBlockOnListPerimeter(iPeak1,jPeak1)) )
                              //if( !(cal1->IsBlockOnListPerimeter(iPeak2,jPeak2)) )
                           {
                              outputTree->Fill();
                              cal1->SetClusterPair(pair);
                              //if( TMath::Abs(cal1->CalculateMass() - mass_center) < mass_cut) { 
                              clusterTiming3->Fill(pair->fCluster1.fClusterTime1);
                              clusterTiming4->Fill(pair->fCluster2.fClusterTime1);
                              //std::cout << " mass = " << recon->fMass << std::endl;
                              //pair->fMass = recon->fMass;
                              cal1->SetClusterPair(pair);
                              cal1->BuildMatricies();
                              massHist->Fill(cal1->fLastMass);
                              etaMassHist->Fill(cal1->fLastMass);
                              pi0MassHist->Fill(cal1->fLastMass);

                              etaYvsMassHist->Fill( cal1->fLastMass, jPeak1);
                              etaYvsMassHist2->Fill(cal1->fLastMass, jPeak2);
                              etaXvsMassHist->Fill( cal1->fLastMass, iPeak1);
                              etaXvsMassHist2->Fill(cal1->fLastMass, iPeak2);

                              YvsMassHist->Fill( cal1->fLastMass, jPeak1);
                              YvsMassHist2->Fill(cal1->fLastMass, jPeak2);
                              XvsMassHist->Fill( cal1->fLastMass, iPeak1);
                              XvsMassHist2->Fill(cal1->fLastMass, iPeak2);

                              EvsMassHist->Fill( cal1->fLastMass, pair->fCluster1.GetCorrectedEnergy());
                              EvsMassHist2->Fill(cal1->fLastMass, pair->fCluster2.GetCorrectedEnergy());

                              DeltaEvsMassHist->Fill( cal1->fLastMass, pair->fCluster1.fDeltaE);
                              DeltaEvsMassHist2->Fill(cal1->fLastMass, pair->fCluster2.fDeltaE);

                              EvsDelta->Fill( pair->fCluster1.fTotalE, pair->fCluster1.fDeltaE);
                              EvsDelta2->Fill(pair->fCluster2.fTotalE, pair->fCluster2.fDeltaE);

                              AnglevsMassHist->Fill(cal1->fLastMass, pair->fAngle/degree);
                              etaAnglevsMassHist->Fill(cal1->fLastMass, pair->fAngle/degree);
                              //}
                           }
                  }
               //}
            }
         }
      }


      // -----------------------
      // Main mass plots
      c->cd(1); 
      massHist->Fit(fitFcn,"E,W","",mass_center-mass_cut, mass_center+mass_cut);
      mass_center = fitFcn->GetParameter(2);
      hs->Draw("nostack");

      //c->cd(2)->cd(1);
      //YvsMassHist->Draw("colz");
      //c->cd(2)->cd(2);
      //YvsMassHist2->Draw("colz");
      //c->cd(4)->cd(1);
      //XvsMassHist->Draw("colz");
      //c->cd(4)->cd(2);
      //XvsMassHist2->Draw("colz");
      
      c->cd(2);
      etaMassHist->Draw();

      c->cd(3);
      AnglevsMassHist->Draw("colz");

      c->cd(4);
      etaAnglevsMassHist->Draw("colz");

      c->Update();
      c->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_%d.png",aNumber,iCalib));
      c->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_%d.pdf",aNumber,iCalib));

      // ----------------------------------
      c3->cd(1);
      gPad->SetLogz(true);
      EvsMassHist->Draw("colz");
      c3->cd(2);
      gPad->SetLogz(true);
      EvsMassHist2->Draw("colz");
      c3->cd(3);
      EvsDelta->Draw("colz");
      c3->cd(4);
      EvsDelta2->Draw("colz");
      c3->cd(5);
      DeltaEvsMassHist->Draw("colz");
      c3->cd(6);
      DeltaEvsMassHist2->Draw("colz");
      c3->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_c3_%d.png",aNumber,iCalib));
      c3->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_c3_%d.pdf",aNumber,iCalib));

      // ----------------------------------
      // Pi0 mass
      c4->cd(0);
      c4->Clear();
      pi0MassHist->Draw();
      pi0MassHist->GetXaxis()->CenterTitle(true);
      pi0MassHist->GetXaxis()->SetTitle("M_{#gamma #gamma} GeV");
      c4->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_pi0_%d.png",aNumber,iCalib));
      c4->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_pi0_%d.pdf",aNumber,iCalib));

      c4->Clear();
      c4->Divide(2,2);
      c4->Update();
      c4->cd(1);
      YvsMassHist->Draw("colz");
      c4->cd(2);
      YvsMassHist2->Draw("colz");
      c4->cd(3);
      XvsMassHist->Draw("colz");
      c4->cd(4);
      XvsMassHist2->Draw("colz");
      c4->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_pi02_%d.png",aNumber,iCalib));
      c4->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_pi02_%d.pdf",aNumber,iCalib));

      // ----------------------------------
      // Eta mass
      c4->cd(0);
      c4->Clear();
      c4->Update();
      etaMassHist->Draw();
      c4->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_eta_%d.png",aNumber,iCalib));
      c4->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_eta_%d.pdf",aNumber,iCalib));

      c4->Clear();
      c4->Divide(2,2);
      c4->Update();
      c4->cd(1);
      etaYvsMassHist->Draw("colz");
      c4->cd(2);
      etaYvsMassHist2->Draw("colz");
      c4->cd(3);
      etaXvsMassHist->Draw("colz");
      c4->cd(4);
      etaXvsMassHist2->Draw("colz");
      c4->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_eta2_%d.png",aNumber,iCalib));
      c4->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_eta2_%d.pdf",aNumber,iCalib));

      cal1->Print();
      //cal1->ExecuteCalibration();
      cal1->ViewStatus();

      cal1->fCanvas->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_c1_%d.png",aNumber,iCalib));
      cal1->fCanvas->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_c1_%d.pdf",aNumber,iCalib));
      

      // ------------------------
      // cuts
      c5->Clear();
      c5->Divide(2,2);
      c5->cd(1);
      clusterTiming1->Draw();
      clusterTiming3->SetLineColor(2);
      clusterTiming3->Draw("same");
      c5->cd(2);
      clusterTiming2->Draw();
      clusterTiming4->SetLineColor(2);
      clusterTiming4->Draw("same");
      c5->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare_swap_%d_cuts_%d.png",aNumber,iCalib));

      massHist->SetLineWidth(1);
      massHist->SetLineColor(iCalib+1);

      // Shrink the mass cut 
      mass_cut += mass_cut_step;
      TF1 * ff = massHist->GetFunction("fitFcn");
      if(ff) ff->SetLineWidth(1);

   }
   fHists->Write();

   pi0OutputFile->Flush();
   //Int_t newSeries = cal1->CreateNewCalibrationSeries(firstRun,lastRun);
   //std::cout << " Created new calibration series, " << newSeries;
   //std::cout << ", for runs " << firstRun << "-" << lastRun << std::endl;

   return(0);
}

