Int_t pi0Calibration() {

   Int_t iPrint = 1000;
   Int_t NCalib = 50;
   //std::vector<Int_t> fRunList;
   //fRunList.push_back(runNumber);
   aman->fRunQueue->push_back(72913);
   aman->fRunQueue->push_back(72921);
   aman->fRunQueue->push_back(72922);
   aman->fRunQueue->push_back(72923);
   aman->fRunQueue->push_back(72924);
   aman->fRunQueue->push_back(72925);
   aman->fRunQueue->push_back(72926);
   aman->fRunQueue->push_back(72927);
   aman->fRunQueue->push_back(72928);
   aman->fRunQueue->push_back(72929);
   aman->fRunQueue->push_back(72930);
   aman->fRunQueue->push_back(72931);
   aman->fRunQueue->push_back(72932);
   aman->fRunQueue->push_back(72933);
   aman->fRunQueue->push_back(72934);
   aman->fRunQueue->push_back(72935);

   TChain * t = 0;
   //t = (TTree*)gROOT->FindObject("pi0results");
   t = aman->BuildChain("pi0results");
   if(!t) return(-1);

   Pi0Event * pi0event        = new Pi0Event();
   t->SetBranchAddress("pi0reconstruction",&pi0event);
   TClonesArray         * clusters = 0;
   InSANEReconstruction * recon    = 0;
   Pi0ClusterPair       * pair     = 0;

   const char * sourceTreeName = "betaDetectors1";
   const char * outputTreeName = "Pi0CalibEigen";

   TList * hists = new TList();
   THStack * hs = new THStack("hs","test stacked histograms");

   Pi0Calibration      * cal1       = new Pi0Calibration();
   //cal1->InitBlocks(5,15,15,40);
   cal1->Initialize();

   Long64_t entries = t->GetEntries();
   std::cout << entries << " entries " << std::endl;

   TCanvas * c = new TCanvas("pi0MassCalib","Pi0 mass iterations");

   for(int iCalib = 0; iCalib<NCalib; iCalib++){

      cal1->Reset();

      TH1F * massHist = new TH1F(Form("mass-%d",iCalib),Form("mass-%d",iCalib),100,50,250);
      hists->Add(massHist);
      hs->Add(massHist);
      massHist->SetLineColor(iCalib+1);


      for(int ievent = 0 ; ievent < entries; ievent++){

         if(ievent%iPrint == 0) std::cout << "Event: " << ievent << std::endl;
         Int_t bytes =  t->GetEntry(ievent);
         //std::cout << bytes << " bytes read" << std::endl;
         for(int i = 0; i< pi0event->fClusterPairs->GetEntries() ; i++){
            pair  = (Pi0ClusterPair*)(*(pi0event->fClusterPairs))[i];
            recon = (InSANEReconstruction*)(*(pi0event->fReconstruction))[i];
            if(TMath::Abs(recon->fMass - 135.0) < 100) {
               if( pair->fCluster1.fIsGood && pair->fCluster2.fIsGood) {
               if( !( (pair->fCluster1.fjPeak == 11) && ( pair->fCluster1.fiPeak == 17 || pair->fCluster1.fiPeak == 18) )  )
               if( !( (pair->fCluster2.fjPeak == 11) && ( pair->fCluster2.fiPeak == 17 || pair->fCluster2.fiPeak == 18) )  ){
                  if( !( (pair->fCluster1.fjPeak == 34) && ( (TMath::Abs(pair->fCluster1.fiPeak - 3) < 2) || (TMath::Abs(pair->fCluster1.fiPeak - 27) < 6)) ) )
                  if( !( (pair->fCluster2.fjPeak == 34) && ( (TMath::Abs(pair->fCluster2.fiPeak - 3) < 2) || (TMath::Abs(pair->fCluster2.fiPeak - 27) < 6)) ) ){
                     //std::cout << " mass = " << recon->fMass << std::endl;
                     pair->fMass = recon->fMass;
                     cal1->SetClusterPair(pair);
                     cal1->BuildMatricies();
                     massHist->Fill(cal1->fLastMass);
                  }
               }
               }
            }
         }
      }

      cal1->Print();
      cal1->ExecuteCalibration();
      hs->Draw("nostack");
      c->Update();
      c->SaveAs(Form("results/pi0Calibration_%d.png",iCalib));
      c->SaveAs(Form("results/pi0Calibration_%d.pdf",iCalib));
      cal1->ViewStatus();
      cal1->fCanvas->SaveAs(Form("results/pi0Calibration_c1_%d.png",iCalib));
      //cal1->fCanvas2->SaveAs(Form("results/pi0Calibration_c2_%d.png",iCalib));
      //cal1->fCanvas3->SaveAs(Form("results/pi0Calibration_c3_%d.png",iCalib));

   }


   return(0);
}

