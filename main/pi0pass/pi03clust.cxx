Int_t pi03clust() {
   /// Build up the queue 
   aman->GetRunQueue()->push_back(72912);
   aman->GetRunQueue()->push_back(72920);
//   aman->GetRunQueue()->push_back(72992);
//    aman->GetRunQueue()->push_back(72994);

   /// Get the chains for run queue
   TChain * pichain =  aman->BuildChain("pi0results");
   TChain * pi3chain =  aman->BuildChain("3clusterPi0results");
   TChain * detchain =  aman->BuildChain("betaDetectors1");

   /// Merge chain into single tree
   TFile * f = new TFile("data/pi03cluster.root","RECREATE");
   f->cd();
   pichain->Merge(f,32000,"keep");
   std::cout << " pichain Merge Complete. \n";

   pi3chain->Merge(f,32000,"keep");
   std::cout << " pi3chain Merge Complete. \n";


   detchain->Merge(f,32000,"keep,fast");
   std::cout << " detchain Merge Complete. \n";

   f->Flush();
   delete pichain;
   delete detchain;

   /// Get new trees 
   TTree * pitree = (TTree*) gROOT->FindObject("pi0results");
   TTree * pi3tree = (TTree*) gROOT->FindObject("3clusterPi0results");
   TTree * dettree = (TTree*) gROOT->FindObject("betaDetectors1");
   if( !pitree  ) { std::cout << "pitree not found\n"; return(-1); }
   if( !dettree  ) { std::cout << "dettree not found\n"; return(-1); }

   /// Cuts out HOT channels ... do you still need this? is this set by bigcal good clust???
   TString pairsCut("betaDetectors1.bigcalClusters.fIsGood==1");

   /// Build the index on ..
   pitree->BuildIndex("pi0reconstruction.fRunNumber","pi0reconstruction.fEventNumber");
   dettree->BuildIndex("fRunNumber","fEventNumber");
   pitree->AddFriend(dettree);

   /// Create the entry list which removes all the hot channel events  
   pitree->Draw(">>noHotChannels", pairsCut.Data(), "entrylist");
   TEntryList *elist = (TEntryList*)gDirectory->Get("noHotChannels");

   /// Use entry list
   pitree->SetEntryList(elist);

   TCanvas * c = new TCanvas("pi0AndBetaCanvas","Pi0 3 Cluster",1200,700);
   c->Divide(4,2);
   TH1 * h1 = 0;
   TH2 * h2 = 0;

   /// Gas Cherenkov TDC 
   c->cd(1);
   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fTDCAlign>>hCerTDC(100,-300,1000)", 
           "",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hCerTDC");
   h1->SetFillColor(1);
   h1->SetTitle("Cherenkov TDC");
   h1->Draw();

   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fTDCAlign>>hCerTDCWcut(100,-300,1000)", 
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign)<30",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hCerTDCWcut");
   h1->SetLineColor(2);
   h1->SetFillColor(2);
   h1->Draw("same");
   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fTDCAlign>>hCerTDCW3clust(100,-300,1000)",
      "betaDetectors1.bigcalClusterEvent.fNClusters==3","goff");
   h1 = (TH1*) gROOT->FindObject("hCerTDCW3clust");
   h1->SetFillColor(4);
   h1->SetLineColor(4);
   h1->Draw("same");
   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerTDCWadccut(100,-300,1000)",
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fADCAlign-2.0)<0.5","goff");
   h1 = (TH1*) gROOT->FindObject("hCerTDCWadccut");
   h1->SetFillColor(3);
   h1->SetLineColor(3);
   h1->Draw("same");

   /// Gas Cherenkov ADC 
   c->cd(2);
   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits.fADCAlign>>hCerADC",
      "fReconstruction[].fPi0Momentum.Mag()<4000",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hCerADC");
   h1->SetFillColor(1);
   h1->SetTitle("Gas Cherenkov e^{#pm} number");
   h1->Draw();
   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerADCWcut",
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign-0)<30","goff");
   h1 = (TH1*) gROOT->FindObject("hCerADCWcut");
   h1->SetFillColor(2);
   h1->SetLineColor(2);
   h1->Draw("same");
   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerADCWadccut",
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fADCAlign-0.8)<0.45&&TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign-300)<100","goff");
   h1 = (TH1*) gROOT->FindObject("hCerADCWadccut");
   h1->SetFillColor(3);
   h1->SetLineColor(3);
//   h1->Draw("same");
   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerADCW3clust",
      "betaDetectors1.bigcalClusterEvent.fNClusters==3","goff");
   h1 = (TH1*) gROOT->FindObject("hCerADCW3clust");
   h1->SetFillColor(4);
   h1->SetLineColor(4);
   h1->Draw("same");
   pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerADCWadcFATcut",
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign-300)<100","goff");
   h1 = (TH1*) gROOT->FindObject("hCerADCWadcFATcut");
   h1->SetFillColor(5);
   h1->SetLineColor(5);
//   h1->Draw("same");

   /// BigCal TDCs
   c->cd(3);
   pitree->Draw("betaDetectors1.fBigcalEvent.fBigcalTimingGroupHits[].fTDCAlign>>hBigcalTDC(300,-600,600)",
      "",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hBigcalTDC");
   h1->SetFillColor(1);
   h1->SetTitle("BigCal TDC");
   h1->Draw();
   pitree->Draw("betaDetectors1.fBigcalEvent.fBigcalTimingGroupHits[].fTDCAlign>>hBigcal3ClustTDC(300,-600,600)",
      "betaDetectors1.bigcalClusterEvent.fNClusters==3",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hBigcal3ClustTDC");
   h1->SetLineColor(2);
   h1->SetTitle("BigCal TDC");
   h1->Draw("same");
   dettree->Draw("betaDetectors1.fBigcalEvent.fBigcalTimingGroupHits[].fTDCAlign>>hBigcalAllTDC(300,-600,600)",
      "",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hBigcalAllTDC");
   h1->SetLineColor(4);
   h1->Scale(0.01);
   h1->SetTitle("BigCal TDC");
   h1->Draw("same");

   /// Pi0 mass
   c->cd(8);
   pitree->Draw("fReconstruction.fMass>>hPi0Mass", 
           "",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hPi0Mass");
   h1->SetTitle("#pi_{0} Mass");
   h1->Draw();



  pi3tree->StartViewer();

   /// Pi0 reconstructed theta vs phi
/*   c->cd(1);
   tv__tree->Draw(
      "fReconstruction.fPi0Momentum.Phi()/0.017:fReconstruction.fPi0Momentum.Theta()/0.017>>hPi0PhiTheta",pairsCut.Data(),"colz,goff");
   h1 = (TH1*)gROOT->FindObject("hPi0PhiTheta");
   if(h1)h1->SetTitle("#pi0 #phi Vs #theta");
   if(h1)h1->Draw("colz");
*/
   /// pion cluster pair's y2-y1 VS x2-x1
/*   c->cd(2);
   tv__tree->Draw("fClusterPairs.fCluster2.fYMoment-fClusterPairs.fCluster1.fYMoment:fClusterPairs.fCluster2.fXMoment-fClusterPairs.fCluster1.fXMoment>>hPi0xydiff"
   ,pairsCut.Data(),"colz,goff");
   h1 = (TH1*)gROOT->FindObject("hPi0xydiff");
   if(h1)h1->SetTitle("#pi0 #Delta_{x} Vs #Delta_{y}");
   if(h1)h1->Draw("colz");
*/
   /// pion cluster pairs Delta_y VS reconstructed pion Momentum
/*   c->cd(3);
   tv__tree->Draw("fClusterPairs.fCluster2.fYMoment-fClusterPairs.fCluster1.fYMoment:fReconstruction.fPi0Momentum.Theta()/0.017>>hPi0ydiffTheta",
   pairsCut.Data(),"colz,goff");
   h1 = (TH1*)gROOT->FindObject("hPi0ydiffTheta");
   if(h1)h1->SetTitle("#pi0 #Delta_{y} Vs #theta");
   if(h1)h1->Draw("colz");
*/
   /// pion momentum VS theta
/*   c->cd(4);
   tv__tree->Draw("betaDetectors1.bigcalClusters[].fjPeak:betaDetectors1.bigcalClusters[Iteration$].fiPeak>>hBCij" ,
     "fReconstruction.fPi0Momentum.Mag()<6000&&betaDetectors1.bigcalClusters[Iteration$].fIsGood&&!(betaDetectors1.bigcalClusters.fjPeak==34)&&!(betaDetectors1.bigcalClusters.fjPeak==56)&&!(betaDetectors1.bigcalClusters.fjPeak>=34&&betaDetectors1.bigcalClusters.fiPeak==30)&&!(betaDetectors1.bigcalClusters.fjPeak<34&&betaDetectors1.bigcalClusters.fiPeak==32)&&!(betaDetectors1.bigcalClusters.fjPeak==1)&&!(betaDetectors1.bigcalClusters.fiPeak==1)",
     "colz,goff");
   h1 = (TH1*)gROOT->FindObject("hBCij");
   if(h1)h1->SetTitle("j_{peak} vs i_{peak}");
   if(h1)h1->Draw("colz");
*/



/*   tv__tree->Draw(">>yplus",
     pairsCut.Data(),
     "entrylist");
   TEntryList *elist = (TEntryList*)gDirectory->Get("yplus");
   tv__tree->SetEntryList(elist);
//      tree->Draw("py");
*/


   /// GC TDC hits with ADC cut 
/*
   /// 
   c->cd(7);
   tv__tree->Draw("fReconstruction[].fPi0Momentum.Mag()>>hPi0P", 
           "fReconstruction[Iteration$].fPi0Momentum.Mag()<4000",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hPi0P");
   h1->SetTitle("#pi_{0} Momentum");
   h1->Draw();
   tv__tree->Draw("fReconstruction.fPi0Momentum.Mag()>>hPi0PWcut", 
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits.fTDCAlign-0)<30&&fReconstruction.fPi0Momentum.Mag()<4000","goff");
   h1 = (TH1*) gROOT->FindObject("hPi0PWcut");
   h1->SetFillColor(2);
   h1->SetLineColor(2);
   h1->Draw("same");

   /// 

//    tv__tree->Draw("fReconstruction.fPi0Momentum.Mag()-betaDetectors1.monteCarloEvent.fThrownParticles.P()*1000.0>>hDelPP", 
//            "",
//       "goff");
//    h1 = (TH1*) gROOT->FindObject("hDelPP");
//    h1->Draw("");

   printf("%s\n",pairsCut.Data());
// tv__tree->Draw("fReconstruction.fPi0Momentum.Mag()-betaDetectors1.monteCarloEvent.fThrownParticles.P()*1000.0>>hDelPPWcut", 
//            "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits.fTDCAlign-0)<30&&fReconstruction.fPi0Momentum.Mag()<4000",
//       "goff");
//    h1 = (TH1*) gROOT->FindObject("hDelPPWcut");
//    h1->SetFillColor(2);
//    h1->SetLineColor(2);
//    h1->Draw("same");
*/
    c->SaveAs("results/pi03clust.png");
    c->SaveAs("results/pi03clust.ps");
//   c->SaveAs(Form("plots/%d/pi0Rejection.png",runNumber));
//   c->SaveAs(Form("plots/%d/pi0Rejection.ps",runNumber));
}

