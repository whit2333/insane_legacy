// Quadratic background function
Double_t background(Double_t *x, Double_t *par) {
      return par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
}

Double_t background1(Double_t *x, Double_t *par) {
      return par[0];
}

// Lorenzian Peak function
Double_t lorentzianPeak(Double_t *x, Double_t *par) {
     return (0.5*par[0]*par[1]/TMath::Pi()) / 
            TMath::Max( 1.e-10,(x[0]-par[2])*(x[0]-par[2]) 
                     + .25*par[1]*par[1]);
}

// Sum of background and peak function
Double_t fitFunction(Double_t *x, Double_t *par) {
     return background1(x,par) + lorentzianPeak(x,&par[1]);
}

Int_t pi0CalibrationCompare(Int_t aNumber=206) {

   // create a TF1 with the range from 0 to 3 and 6 parameters
   TF1 *fitFcn = new TF1("fitFcn",fitFunction,0,300,4);
   fitFcn->SetNpx(500);
   fitFcn->SetLineWidth(2);
   fitFcn->SetLineColor(4);

   // first try without starting values for the parameters
   // This defaults to 1 for each param. 
   // this results in an ok fit for the polynomial function
   // however the non-linear part (lorenzian) does not 
   // respond well.
   fitFcn->SetParameters(0.1,0.1,0.1,0.1);
   //histo->Fit("fitFcn","0");

   fitFcn->SetParameter(0,10); // p0
   fitFcn->SetParameter(1,100); // p0
   fitFcn->SetParameter(2,10); // width
   fitFcn->SetParameter(3,135);   // peak
   //// second try: set start values for some parameters
   //fitFcn->SetParameter(4,0.2); // width
   //fitFcn->SetParameter(5,1);   // peak
   //histo->Fit("fitFcn","V+","ep");

   Int_t iPrint = 10000;
   Int_t NCalib = 1;
   Int_t NEventsMax = 10000000;
   //std::vector<Int_t> fRunList;
   //fRunList.push_back(runNumber);


   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();

   //aman->BuildQueue2("lists/perp/5_9GeV/good_NH3.txt");
   aman->BuildQueue2("lists/para/5_9GeV/good_NH3.txt");
   //aman->BuildQueue2("lists/para/4_7GeV/good_NH3.txt");

   if(aman->fRunQueue->size() ==0) return -10;
   Int_t run_number_cal = 72500;//72913;//aman->fRunQueue->at(0);
   std::cout << " Using run " << run_number_cal << " to get the calibration series" << std::endl;
   Int_t series = geocalc->GetLatestCalibrationSeries(run_number_cal);
   geocalc->SetCalibrationSeries(series);

   TChain * t = 0;
   t = aman->BuildChain("pi0results");
   if(!t) return(-1);

   Pi0Event * pi0event        = new Pi0Event();
   t->SetBranchAddress("pi0reconstruction",&pi0event);
   TClonesArray         * clusters = 0;
   InSANEReconstruction * recon    = 0;
   Pi0ClusterPair       * pair     = 0;

   const char * sourceTreeName = "betaDetectors1";
   const char * outputTreeName = "Pi0CalibEigen";

   TList * hists = new TList();
   THStack * hs = new THStack("hs","test stacked histograms");

   Pi0Calibration      * cal1       = new Pi0Calibration();
   //cal1->InitBlocks(2,2,31,31);
   cal1->InitBlocks(1,1,32,56);
   cal1->Initialize();

   //cal1->LoadEpsilonFromSeries(series-1,series);
   cal1->LoadEpsilonFromSeries(1,series);

   Double_t clusterTimeCut = 60.0;
   Double_t mass_cut = 99.0;
   Double_t mass_cut_step = 0.0;//(10.0 - mass_cut)/float(NCalib);
   //mass_cut = 10.0;
   Long64_t entries = t->GetEntries();
   std::cout << entries << " entries " << std::endl;

   TLegend * leg = new TLegend(0.6,0.6,0.89,0.89);
   TCanvas * c = new TCanvas("pi0MassIterations","Pi0 mass iterations");
   c->Divide(2,2);

   Int_t iPeak1 = 0;
   Int_t jPeak1 = 0;
   Int_t iPeak2 = 0;
   Int_t jPeak2 = 0;

   for(int iCalib = 0; iCalib<NCalib; iCalib++){

      cal1->Reset();

      TH2F * AnglevsMassHist = new TH2F(Form("AnglevsMass-%d",iCalib),Form("AnglevsMass-%d",iCalib),100,50,250,40,0,20);
      TH2F * YvsMassHist     = new TH2F(Form("YvsMass-%d",iCalib),Form("YvsMass-%d",iCalib),100,50,250,56,1,57);
      TH2F * XvsMassHist     = new TH2F(Form("XvsMass-%d",iCalib),Form("XvsMass-%d",iCalib),100,50,250,32,1,33);
      TH1F * massHist        = new TH1F(Form("mass-%d",iCalib),Form("mass-%d",iCalib),100,50,250);
      TH1F * massHist0   = new TH1F(Form("massInit-%d",iCalib),Form("mass-%d",iCalib),100,50,250);
      hists->Add(massHist0);
      hists->Add(massHist);
      hs->Add(   massHist0);
      hs->Add(   massHist);
      massHist0->SetLineWidth(3);
      massHist0->SetLineColor(2);
      massHist->SetLineWidth(2);
      massHist->SetLineColor(1);
      leg->AddEntry(massHist0,"Old calibration","l");
      leg->AddEntry(massHist,"New calibration","l");

      for(int ievent = 0 ; ievent < entries && ievent < NEventsMax; ievent++){

         if(ievent%iPrint == 0) std::cout << "Event: " << ievent << std::endl;
         Int_t bytes =  t->GetEntry(ievent);
         //std::cout << bytes << " bytes read" << std::endl;
         for(int i = 0; i< pi0event->fClusterPairs->GetEntries() ; i++){
            pair  = (Pi0ClusterPair*)(*(pi0event->fClusterPairs))[i];
            //recon = (InSANEReconstruction*)(*(pi0event->fReconstruction))[i];

            iPeak1 = pair->fCluster1.fiPeak;
            jPeak1 = pair->fCluster1.fjPeak;
            iPeak2 = pair->fCluster2.fiPeak;
            jPeak2 = pair->fCluster2.fjPeak;

            if( pair->fAngle > 4.0*degree) 
            //if(TMath::Abs(pair->fMass - mass_center) < mass_cut) 
               if( TMath::Abs(pair->fCluster1.fClusterTime1) < clusterTimeCut || TMath::Abs(pair->fCluster1.fClusterTime2) < clusterTimeCut ) 
               if( TMath::Abs(pair->fCluster2.fClusterTime1) < clusterTimeCut || TMath::Abs(pair->fCluster2.fClusterTime2) < clusterTimeCut ) 
               if( pair->fCluster1.fIsGood && pair->fCluster2.fIsGood)
               if( pair->fCluster1.fCherenkovBestADCSum < 0.2 && pair->fCluster2.fCherenkovBestADCSum < 0.2 )
               { 

            //if( pair->fAngle > 4.0*degree) 
            //if(TMath::Abs(recon->fMass - 130.0) < 130) {
            //if(TMath::Abs(recon->fMass - 135.0) < mass_cut) 
            //   if( TMath::Abs(pair->fCluster1.fClusterTime1) < clusterTimeCut || TMath::Abs(pair->fCluster1.fClusterTime2) < clusterTimeCut ) 
            //   if( TMath::Abs(pair->fCluster2.fClusterTime1) < clusterTimeCut || TMath::Abs(pair->fCluster2.fClusterTime2) < clusterTimeCut ) 
            //   if( pair->fCluster1.fIsGood && pair->fCluster2.fIsGood) {
               if( !( (jPeak1 == 46) &&  (iPeak1 == 16) )   )
               if( !( (jPeak2 == 46) &&  (iPeak2 == 16) )   )
               if( !( (jPeak1 == 46) &&  (iPeak1 == 6) )   )
               if( !( (jPeak2 == 46) &&  (iPeak2 == 6) )   )
               if( !( (jPeak1 == 48) &&  (iPeak1 == 7) )   )
               if( !( (jPeak2 == 48) &&  (iPeak2 == 7) )   )
               if( !( (jPeak1 == 35) &&  (iPeak1 == 6) )   )
               if( !( (jPeak2 == 35) &&  (iPeak2 == 6) )   )
               if( !( (jPeak1 == 38) &&  (iPeak1 == 15) )   )
               if( !( (jPeak2 == 38) &&  (iPeak2 == 15) )   )
               if( !( (jPeak1 == 36) &&  (iPeak1 == 14) )   )
               if( !( (jPeak2 == 36) &&  (iPeak2 == 14) )   )
               if( !( (jPeak1 == 48) &&  (iPeak1 == 12) )   )
               if( !( (jPeak2 == 48) &&  (iPeak2 == 12) )   )
               if( !( (jPeak1 == 53) &&  (iPeak1 == 9) )   )
               if( !( (jPeak2 == 53) &&  (iPeak2 == 9) )   )
               if( !( (jPeak1 == 51) &&  (iPeak1 == 16) )   )
               if( !( (jPeak2 == 51) &&  (iPeak2 == 16) )   )
               if( !( (jPeak1 == 8 ) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 8 ) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 21) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 21) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 29) &&  (iPeak1 == 3) )   )
               if( !( (jPeak2 == 29) &&  (iPeak2 == 3) )   )
               //if( !( (jPeak1 == 34) ||  (jPeak2 == 34) )   )
               if( !( (jPeak1 == 29) &&  (iPeak1 == 15) )   )
               if( !( (jPeak2 == 29) &&  (iPeak2 == 15) )   )
               if( !( (jPeak1 == 26) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 26) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 16) &&  (iPeak1 == 18) )   )
               if( !( (jPeak2 == 16) &&  (iPeak2 == 18) )   )
               if( !( (jPeak1 == 25) &&  (iPeak1 == 11) )   )
               if( !( (jPeak2 == 25) &&  (iPeak2 == 11) )   )
               if( !( (jPeak1 == 34) &&  (iPeak1 == 13) )   )
               if( !( (jPeak2 == 34) &&  (iPeak2 == 13) )   )
               if( !( (jPeak1 == 42) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 42) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 45) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 45) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 47) &&  (iPeak1 == 16) )   )
               if( !( (jPeak2 == 47) &&  (iPeak2 == 16) )   )
               if( !( (jPeak1 == 5 ) &&  (iPeak1 == 30) )   )
               if( !( (jPeak2 == 5 ) &&  (iPeak2 == 30) )   )
               if( !( (jPeak1 == 11) && ( iPeak1 == 17 || iPeak1 == 18) )  )
               if( !( (jPeak2 == 11) && ( iPeak2 == 17 || iPeak2 == 18) )  ){
                  if( !( (jPeak1 == 34) && ( (TMath::Abs(iPeak1 - 4) < 4) || (TMath::Abs(iPeak1 - 27) < 6)) ) )
                  if( !( (jPeak2 == 34) && ( (TMath::Abs(iPeak2 - 4) < 4) || (TMath::Abs(iPeak2 - 27) < 6)) ) ){

                     if( pair->fCluster1.fNNonZeroBlocks > 3)
                     if( pair->fCluster2.fNNonZeroBlocks > 3)
                     if( cal1->IsPairInList(pair) )
                     if( !(cal1->IsBlockOnListPerimeter(pair->fCluster1.fiPeak,pair->fCluster1.fjPeak)) )
                     if( !(cal1->IsBlockOnListPerimeter(pair->fCluster2.fiPeak,pair->fCluster2.fjPeak)) ){
                        //std::cout << " mass = " << recon->fMass << std::endl;
                        cal1->SetClusterPair(pair);
                        cal1->BuildMatricies();
                        cal1->CalculateMass();
                        massHist->Fill(cal1->fLastMass);
                        massHist0->Fill(pair->fMass);
                        YvsMassHist->Fill(cal1->fLastMass, pair->fCluster1.fjPeak);
                        XvsMassHist->Fill(cal1->fLastMass, pair->fCluster1.fiPeak);
                        AnglevsMassHist->Fill(cal1->fLastMass, pair->fAngle/degree);
                     }
                  }
               }
            }
         }
      }

      c->cd(1); 

      massHist->Fit(fitFcn,"E","",60,220);
      hs->Draw("nostack");
      leg->Draw();

      c->cd(2);
      YvsMassHist->Draw("colz");

      c->cd(3);
      AnglevsMassHist->Draw("colz");

      c->cd(4);
      XvsMassHist->Draw("colz");

      c->Update();
      c->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare-%d_%d.png",aNumber,iCalib));
      c->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare-%d_%d.pdf",aNumber,iCalib));
      c->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare-%d_%d.ps",aNumber,iCalib));

      cal1->Print();
      //cal1->ExecuteCalibration();


      //c->SaveAs(Form("results/pi0Calibration_%d.pdf",iCalib));
      cal1->ViewStatus();
      cal1->fCanvas->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare-%d_c1_%d.png",aNumber,iCalib));
      cal1->fCanvas->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare-%d_c1_%d.pdf",aNumber,iCalib));
      cal1->fCanvas->SaveAs(Form("results/pi0calibration/pi0CalibrationCompare-%d_c1_%d.ps",aNumber,iCalib));
      //cal1->fCanvas2->SaveAs(Form("results/pi0Calibration2_c2_%d.png",iCalib));
      //cal1->fCanvas3->SaveAs(Form("results/pi0Calibration2_c3_%d.png",iCalib));
      massHist->SetLineWidth(1);
      massHist->SetLineColor(iCalib+1);

   }

   //cal1->CreateNewCalibrationSeries(72913,72935);

   return(0);
}

