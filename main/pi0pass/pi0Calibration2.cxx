// Quadratic background function
Double_t background(Double_t *x, Double_t *par) {
      return par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
}

Double_t background1(Double_t *x, Double_t *par) {
      return par[0];
}

// Lorenzian Peak function
Double_t lorentzianPeak(Double_t *x, Double_t *par) {
     return (0.5*par[0]*par[1]/TMath::Pi()) / 
            TMath::Max( 1.e-10,(x[0]-par[2])*(x[0]-par[2]) 
                     + .25*par[1]*par[1]);
}

// Sum of background and peak function
Double_t fitFunction(Double_t *x, Double_t *par) {
     return background1(x,par) + lorentzianPeak(x,&par[1]);
}
//______________________________________________________________________________
Int_t pi0Calibration2( Int_t aNumber = 130) {

   load_style("MultiSquarePlot");
   gROOT->SetStyle("MultiSquarePlot");

   // create a TF1 with the range from 0 to 3 and 6 parameters
   TF1 *fitFcn = new TF1("fitFcn",fitFunction,0,300,4);
   fitFcn->SetNpx(500);
   fitFcn->SetLineWidth(2);
   fitFcn->SetLineColor(kMagenta);

   // first try without starting values for the parameters
   // This defaults to 1 for each param. 
   // this results in an ok fit for the polynomial function
   // however the non-linear part (lorenzian) does not 
   // respond well.
   fitFcn->SetParameters(0.1,0.1,0.1,0.1);
   //histo->Fit("fitFcn","0");

   fitFcn->SetParameter(0,10); // p0
   fitFcn->SetParLimits(0,0,100);
   fitFcn->SetParameter(1,100); // p0
   fitFcn->SetParLimits(1,0,1.0e6);
   fitFcn->SetParameter(2,10); // width
   fitFcn->SetParLimits(2,0,1.0e2);
   fitFcn->SetParameter(3,135);   // peak
   fitFcn->SetParLimits(3,110,160);
   //// second try: set start values for some parameters
   //fitFcn->SetParameter(4,0.2); // width
   //fitFcn->SetParameter(5,1);   // peak
   //histo->Fit("fitFcn","V+","ep");

   Int_t iPrint = 10000;
   Int_t NCalib = 5;
   Int_t NEventMax = 10000000;

   aman->BuildQueue2("lists/para/4_7GeV/good_NH3.txt");

   aman->fRunQueue->push_back(72915);
   aman->fRunQueue->push_back(72916);
   aman->fRunQueue->push_back(72917);
   aman->fRunQueue->push_back(72918);
   aman->fRunQueue->push_back(72919);
   aman->fRunQueue->push_back(72920);
   aman->fRunQueue->push_back(72921);
   aman->fRunQueue->push_back(72922);
   aman->fRunQueue->push_back(72923);
   aman->fRunQueue->push_back(72924);
   aman->fRunQueue->push_back(72925);
   aman->fRunQueue->push_back(72926);
   aman->fRunQueue->push_back(72927);
   aman->fRunQueue->push_back(72928);
   aman->fRunQueue->push_back(72929);
   aman->fRunQueue->push_back(72930);
   aman->fRunQueue->push_back(72931);
   aman->fRunQueue->push_back(72932);
   aman->fRunQueue->push_back(72933);
   aman->fRunQueue->push_back(72934);
   aman->fRunQueue->push_back(72935);
   aman->fRunQueue->push_back(72936);
   aman->fRunQueue->push_back(72942);
   aman->fRunQueue->push_back(72943);
   aman->fRunQueue->push_back(72944);
   aman->fRunQueue->push_back(72945);
   aman->fRunQueue->push_back(72946);
   aman->fRunQueue->push_back(72947);
   aman->fRunQueue->push_back(72948);
   aman->fRunQueue->push_back(72949);
   aman->fRunQueue->push_back(72950);
   aman->fRunQueue->push_back(72957);
   aman->fRunQueue->push_back(72959);
   aman->fRunQueue->push_back(72961);

   Int_t firstRun = 72913;
   Int_t lastRun  = 73050;//aman->fRunQueue->at(aman->fRunQueue->size()-1);

   BIGCALGeometryCalculator * geocalc = BIGCALGeometryCalculator::GetCalculator();
   Int_t                      series  = geocalc->GetLatestCalibrationSeries(72913);

   geocalc->SetCalibrationSeries(series);

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();

   TChain * t = 0;
   t = aman->BuildChain("pi0results");
   if(!t) return(-1);

   Pi0Event * pi0event        = new Pi0Event();
   t->SetBranchAddress("pi0reconstruction",&pi0event);
   TClonesArray         * clusters = 0;
   InSANEReconstruction * recon    = 0;
   Pi0ClusterPair       * pair     = 0;

   const char * sourceTreeName = "betaDetectors1";
   const char * outputTreeName = "Pi0CalibEigen";

   TList * hists = new TList();
   THStack * hs = new THStack("hs","test stacked histograms");

   Pi0Calibration      * cal1       = new Pi0Calibration();
   cal1->fEigenValueThreshold = 1.0e5;//1000000.0;

   //cal1->InitBlocks(2,2,31,31);
   //cal1->InitBlocks(3,25,28,44);
   //cal1->InitBlocks(3,20,29,40);
   //cal1->InitBlocks(3,2,30,23);
   //cal1->InitBlocks(1,1,20,23);
   
   //cal1->InitBlocks(12,1,32,23);  // lower left corner
  
   //cal1->InitBlocks(1,35,21,56);  // upper left corner
   //cal1->InitBlocks(10,35,30,56); // upper right corner
 
   //cal1->InitBlocks(1,40,30,56);   // top 
  
   // Top Middle
   //cal1->InitActiveBlocks(1,38,20,53);
   //cal1->InitBlocks(3,38,28,54); // 
   //cal1->InitBlocks(21,43,25,47); // 
   //cal1->InitBlocks(9,43,14,47); // 
   //cal1->InitBlocks(21,49,25,55); // 
   //cal1->InitBlocks(8,49,13,55); // 
   //cal1->InitBlocks(3,49,10,54); // 
   //cal1->InitBlocks(2,48,11,55); // 
   //cal1->InitBlocks(21,49,24,54); // 
   //cal1->InitBlocks(2,48,11,55); // 
   //cal1->InitBlocks(2,42,11,49); // 
   //cal1->InitBlocks(3,38,28,54); // 116
   
   // top far left
   //cal1->InitActiveBlocks(1,38,20,53);
   ////cal1->InitBlocks(1,40,2,53); //
   ////cal1->InitBlocks(1,40,4,53); //
   //cal1->InitBlocks(1,40,6,53); //
   
   // top far right
   //cal1->InitActiveBlocks(10,38,30,53);
   ////cal1->InitBlocks(29,40,30,53); //
   ////cal1->InitBlocks(27,40,30,53); //
   //cal1->InitBlocks(26,40,30,53); //

   // top top
   //cal1->InitActiveBlocks(3,38,27,56);
   ////cal1->InitBlocks(29,40,30,53); //
   //cal1->InitBlocks(3,50,27,56); //

   //cal1->InitBlocks(1,35,28,54);   // top left middle
   
   //cal1->InitBlocks(1,33,30,56);   // RCS 
   
   //cal1->InitBlocks(1,40,30,50); // slightly lower 

   //cal1->InitBlocks(1,40,30,56); // larger top 

   // Sector 1
   cal1->InitBlocks(3,3,30,16);
   cal1->InitActiveBlocks(3,3,30,16);
   
   // Sector 2
   //cal1->InitBlocks(3,8,30,15);

   // Sector 3
   //cal1->InitBlocks(3,13,30,20);

   // Sector 4
   //cal1->InitBlocks(3,18,30,25);

   cal1->Initialize();
   cal1->LoadEpsilonFromSeries(1,series);
   //cal1->LoadEpsilonFromSeries(series-1,series);

   Double_t clusterTimeCut = 60.0;
   Double_t mass_cut       = 45.0;
   Double_t mass_cut_end   = 30.0;
   Double_t mass_center    = 140.0;
   Double_t mass_cut_step  = -1.0*(mass_cut - mass_cut_end)/float(NCalib);
   //mass_cut = 10.0;
   Long64_t entries = t->GetEntries();
   std::cout << entries << " entries " << std::endl;

   TCanvas * c5 = new TCanvas("Pi0diagnostics","Pi0 cuts");
   c5->Divide(2,2);

   TCanvas * c = new TCanvas("pi0MassIterations","Pi0 mass iterations");
   c->Divide(2,2);
   c->cd(2)->Divide(1,2);
   c->cd(4)->Divide(1,2);
   
   // close the mysql connetion before going into calibration/event loop
   dbman->CloseConnections();

   Int_t iPeak1 = 0;
   Int_t jPeak1 = 0;
   Int_t iPeak2 = 0;
   Int_t jPeak2 = 0;

   // Calibration loop
   for(int iCalib = 0; iCalib<NCalib; iCalib++){

      std::cout << " Mass cut is " << mass_cut << " MeV" << std::endl;
      std::cout << " centered at " << mass_center << std::endl;
      cal1->Reset();

      TH2F * AnglevsMassHist = new TH2F(Form("AnglevsMass-%d",iCalib),Form("AnglevsMass-%d",iCalib),100,50,250,40,3,13);
      TH2F * YvsMassHist     = new TH2F(Form("YvsMass-%d",iCalib),Form("YvsMass-%d",iCalib),100,50,250,56,1,57);
      TH2F * YvsMassHist2    = new TH2F(Form("YvsMass2-%d",iCalib),Form("YvsMass-%d",iCalib),100,50,250,56,1,57);
      TH2F * XvsMassHist     = new TH2F(Form("XvsMass-%d",iCalib),Form("Mass vs x-%d",iCalib),100,50,250,32,1,33);
      TH2F * XvsMassHist2    = new TH2F(Form("XvsM2-%d",iCalib),Form("Mass vs x-%d",iCalib),100,50,250,32,1,33);
      TH1F * massHist        = new TH1F(Form("mass-%d",iCalib),Form("mass-%d",iCalib),100,50,250);
      //TH1F * clusterTiming1        = new TH1F(Form("clustertiming1-%d",iCalib),Form("cluster time 1-%d",iCalib),100,-150,150);
      //TH1F * clusterTiming2        = new TH1F(Form("clustertiming2-%d",iCalib),Form("cluster time 2-%d",iCalib),100,-150,150);
      hists->Add(massHist);
      hs->Add(   massHist);
      massHist->SetLineWidth(2);
      massHist->SetLineColor(1);

      for(int ievent = 0 ; ievent < entries && ievent < NEventMax ; ievent++){

         if(ievent%iPrint == 0) std::cout << "Event: " << ievent << std::endl;
         Int_t bytes =  t->GetEntry(ievent);
         //std::cout << bytes << " bytes read" << std::endl;
         for(int i = 0; i< pi0event->fClusterPairs->GetEntries() ; i++){
            pair  = (Pi0ClusterPair*)(*(pi0event->fClusterPairs))[i];
            //recon = (InSANEReconstruction*)(*(pi0event->fReconstruction))[i];

            iPeak1 = pair->fCluster1.fiPeak;
            jPeak1 = pair->fCluster1.fjPeak;
            iPeak2 = pair->fCluster2.fiPeak;
            jPeak2 = pair->fCluster2.fjPeak;

            if( pair->fAngle > 4.0*degree) 
            //if(TMath::Abs(pair->fMass - mass_center) < mass_cut) 
               if( TMath::Abs(pair->fCluster1.fClusterTime1) < clusterTimeCut || TMath::Abs(pair->fCluster1.fClusterTime2) < clusterTimeCut ) 
               if( TMath::Abs(pair->fCluster2.fClusterTime1) < clusterTimeCut || TMath::Abs(pair->fCluster2.fClusterTime2) < clusterTimeCut ) 
               //if( pair->fCluster1.fIsGood && pair->fCluster2.fIsGood)
               if( pair->fCluster1.fCherenkovBestADCSum < 0.2 && pair->fCluster2.fCherenkovBestADCSum < 0.2 )
               { 
               if( !( (jPeak1 == 46) &&  (iPeak1 == 16) )   )
               if( !( (jPeak2 == 46) &&  (iPeak2 == 16) )   )
               if( !( (jPeak1 == 46) &&  (iPeak1 == 6) )   )
               if( !( (jPeak2 == 46) &&  (iPeak2 == 6) )   )
               if( !( (jPeak1 == 48) &&  (iPeak1 == 7) )   )
               if( !( (jPeak2 == 48) &&  (iPeak2 == 7) )   )
               if( !( (jPeak1 == 35) &&  (iPeak1 == 6) )   )
               if( !( (jPeak2 == 35) &&  (iPeak2 == 6) )   )
               if( !( (jPeak1 == 38) &&  (iPeak1 == 15) )   )
               if( !( (jPeak2 == 38) &&  (iPeak2 == 15) )   )
               if( !( (jPeak1 == 36) &&  (iPeak1 == 14) )   )
               if( !( (jPeak2 == 36) &&  (iPeak2 == 14) )   )
               if( !( (jPeak1 == 48) &&  (iPeak1 == 12) )   )
               if( !( (jPeak2 == 48) &&  (iPeak2 == 12) )   )
               if( !( (jPeak1 == 53) &&  (iPeak1 == 9) )   )
               if( !( (jPeak2 == 53) &&  (iPeak2 == 9) )   )
               if( !( (jPeak1 == 51) &&  (iPeak1 == 16) )   )
               if( !( (jPeak2 == 51) &&  (iPeak2 == 16) )   )
               if( !( (jPeak1 == 8 ) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 8 ) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 21) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 21) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 29) &&  (iPeak1 == 3) )   )
               if( !( (jPeak2 == 29) &&  (iPeak2 == 3) )   )
               if( !( (jPeak1 == 34) ||  (jPeak2 == 34) )   )
               if( !( (jPeak1 == 29) &&  (iPeak1 == 15) )   )
               if( !( (jPeak2 == 29) &&  (iPeak2 == 15) )   )
               if( !( (jPeak1 == 26) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 26) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 16) &&  (iPeak1 == 18) )   )
               if( !( (jPeak2 == 16) &&  (iPeak2 == 18) )   )
               if( !( (jPeak1 == 25) &&  (iPeak1 == 11) )   )
               if( !( (jPeak2 == 25) &&  (iPeak2 == 11) )   )
               if( !( (jPeak1 == 34) &&  (iPeak1 == 13) )   )
               if( !( (jPeak2 == 34) &&  (iPeak2 == 13) )   )
               if( !( (jPeak1 == 42) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 42) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 45) &&  (iPeak1 == 17) )   )
               if( !( (jPeak2 == 45) &&  (iPeak2 == 17) )   )
               if( !( (jPeak1 == 47) &&  (iPeak1 == 16) )   )
               if( !( (jPeak2 == 47) &&  (iPeak2 == 16) )   )
               if( !( (jPeak1 == 5 ) &&  (iPeak1 == 30) )   )
               if( !( (jPeak2 == 5 ) &&  (iPeak2 == 30) )   )
               if( !( (jPeak1 == 11) && ( iPeak1 == 17 || iPeak1 == 18) )  )
               if( !( (jPeak2 == 11) && ( iPeak2 == 17 || iPeak2 == 18) )  ){
                  if( !( (jPeak1 == 34) && ( (TMath::Abs(iPeak1 - 4) < 4) || (TMath::Abs(iPeak1 - 27) < 6)) ) )
                  if( !( (jPeak2 == 34) && ( (TMath::Abs(iPeak2 - 4) < 4) || (TMath::Abs(iPeak2 - 27) < 6)) ) ){
                     // the next two lines are here because we want to cut on NNonzeroblocks and the copy ctor of the cluster wasn't set right.
                     //pair->fCluster1.CalculateMoments();
                     //pair->fCluster2.CalculateMoments();
                     if( pair->fCluster1.fNNonZeroBlocks > 3)
                     if( pair->fCluster2.fNNonZeroBlocks > 3)
                     if( cal1->IsActivePair(pair) )
                     //if( cal1->IsPairInList(pair) )
                     //if( !(cal1->IsBlockOnListPerimeter(iPeak1,jPeak1)) )
                     //if( !(cal1->IsBlockOnListPerimeter(iPeak2,jPeak2)) )
                     {
                        cal1->SetClusterPair(pair);
                     if( TMath::Abs(cal1->CalculateMass() - mass_center) < mass_cut) { 
                        //clusterTiming1->Fill(pair->fCluster1.fClusterTime1);
                        //clusterTiming2->Fill(pair->fCluster2.fClusterTime1);
                        //std::cout << " mass = " << recon->fMass << std::endl;
                        //pair->fMass = recon->fMass;
                        cal1->SetClusterPair(pair);
                        cal1->BuildMatricies();
                        massHist->Fill(cal1->fLastMass);
                        YvsMassHist->Fill( cal1->fLastMass, jPeak1);
                        YvsMassHist2->Fill(cal1->fLastMass, jPeak2);
                        XvsMassHist->Fill( cal1->fLastMass, iPeak1);
                        XvsMassHist2->Fill(cal1->fLastMass, iPeak2);
                        AnglevsMassHist->Fill(cal1->fLastMass, pair->fAngle/degree);
                     }
                     }
                  }
               }
            }
         }
      }

      c->cd(1); 

      massHist->Fit(fitFcn,"E,W","",mass_center-mass_cut, mass_center+mass_cut);
      //mass_center = fitFcn->GetParameter(3);

      hs->Draw("nostack");

      c->cd(2)->cd(1);
      YvsMassHist->Draw("colz");
      c->cd(2)->cd(2);
      YvsMassHist2->Draw("colz");

      c->cd(4)->cd(1);
      XvsMassHist->Draw("colz");
      c->cd(4)->cd(2);
      XvsMassHist2->Draw("colz");

      c->cd(3);
      AnglevsMassHist->Draw("colz");

      c->Update();
      c->SaveAs(Form("results/pi0calibration/pi0Calibration_%d_%d.png",aNumber,iCalib));

      cal1->Print();
      cal1->ExecuteCalibration();

      //c->SaveAs(Form("results/pi0Calibration_%d.pdf",iCalib));
      cal1->ViewStatus();
      cal1->fCanvas->SaveAs(Form("results/pi0calibration/pi0Calibration_%d_c1_%d.png",aNumber,iCalib));
      //cal1->fCanvas2->SaveAs(Form("results/pi0calibration/pi0Calibration2_c2_%d.png",iCalib));
      //cal1->fCanvas3->SaveAs(Form("results/pi0calibration/pi0Calibration2_c3_%d.png",iCalib));
      massHist->SetLineWidth(1);
      massHist->SetLineColor(iCalib+1);

      // Shrink the mass cut 
      //mass_cut += mass_cut_step;
      TF1 * ff = massHist->GetFunction("fitFcn");
      if(ff) ff->SetLineWidth(1);

      //c5->cd(1);
      //clusterTiming1->Draw();
      //c5->cd(2);
      //clusterTiming2->Draw();
      //c5->SaveAs(Form("results/pi0calibration/pi0Calibration_%d_cuts_%d.png",aNumber,iCalib));
   }

   Int_t newSeries = cal1->CreateNewCalibrationSeries(firstRun,lastRun);
   std::cout << " Created new calibration series, " << newSeries;
   std::cout << ", for runs " << firstRun << "-" << lastRun << std::endl;

   return(0);
}

