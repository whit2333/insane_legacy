Int_t pi0_3clusters() {

   TFile * f = new TFile("data/pi03cluster.root","READ");
   f->cd();
   /// Get new trees 
   TTree * pitree = (TTree*) gROOT->FindObject("pi0results");
   TTree * pi3tree = (TTree*) gROOT->FindObject("3clusterPi0results");

   if( !pitree  ) { std::cout << "pitree not found\n"; return(-1); }
   if( !pi3tree  ) { std::cout << "pi3tree not found\n"; return(-1); }

   /// Create the entry list which removes all the hot channel events  

   TCanvas * c = new TCanvas("pi0AndBetaCanvas","Pi0 3 Cluster",1200,700);
   c->Divide(4,2);
   TH1 * h1 = 0;
   TH2 * h2 = 0;

   TString pairXYcut = "(fClusterPairs.fCluster1.fYMoment<10||fClusterPairs.fCluster1.fYMoment>25)&&(fClusterPairs.fCluster2.fYMoment<10||fClusterPairs.fCluster2.fYMoment>25)&&(TMath::Abs(fClusterPairs.fCluster1.fXMoment)<50)&&(TMath::Abs(fClusterPairs.fCluster2.fXMoment)<50)&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood";

   TString aCut = "(fClusters.fYMoment<10||fClusters.fYMoment>25)&&(TMath::Abs(fClusters.fXMoment)<50)&&fClusters.fIsGood";

   /// Pi0 mass
   c->cd(1);

   pitree->Draw("fReconstruction.fMass>>pi0Mass(50,0,500)", 
           Form("%s",pairXYcut.Data()),
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass");
   h1->SetFillColor(1);
   h1->SetTitle("#pi^{0} Mass");
   h1->Draw();

//    pitree->Draw("fReconstruction.fMass>>pi0Mass(50,0,1000)", 
//            "",
//       "goff");
//    h1 = (TH1*) gROOT->FindObject("pi0Mass");
//    h1->SetFillColor(1);
//    h1->SetTitle("#pi^{0} Mass");
//    h1->Draw();

   pi3tree->Draw("fReconstruction.fMass>>pi0Mass3ClusterYCutscaled(50,0,500)", 
           "(fClusters.fYMoment<10||fClusters.fYMoment>25)&&fClusters.fIsGood&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood",
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass3ClusterYCutscaled");
   h1->SetLineColor(3);
   h1->Scale(1.0);
   h1->Draw("same");

   /// 3 cluster pi0 mass
   c->cd(2);
   pi3tree->Draw("fReconstruction.fMass>>pi0Mass3Cluster(50,0,500)", 
           "fClusters.fIsGood&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood",
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass3Cluster");
   h1->SetFillColor(2);
   h1->SetTitle("#pi^{0} Mass");
   h1->Draw();

   pi3tree->Draw("fReconstruction.fMass>>pi0Mass3ClusterYCut(50,0,500)", 
           "(fClusters.fYMoment<10||fClusters.fYMoment>25)&&fClusters.fIsGood&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood",
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass3ClusterYCut");
   h1->SetFillColor(3);
   h1->Draw("same");


   pi3tree->Draw("fReconstruction.fMass>>pi0Mass3ClusterGammaCut(50,0,500)",
           "fReconstruction.fMomentum>1500&&fClusters.fIsGood&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood",
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass3ClusterGammaCut");
   h1->SetFillColor(4);
   h1->Draw("same");



   pi3tree->Draw("fReconstruction.fMass>>pi0Mass3ClusterYPairCut(50,0,500)", 
           "fNumberOfPairs<10&&(fClusters.fYMoment<10||fClusters.fYMoment>25)&&fClusters.fIsGood&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood",
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass3ClusterYPairCut");
   h1->SetFillColor(5);
   h1->Draw("same");

   pi3tree->Draw("fReconstruction.fMass>>pi0Mass3ClusterXYCts(50,0,500)", 
           Form("%s&&%s",aCut.Data(),pairXYcut.Data()),
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass3ClusterXYCts");
   h1->SetFillColor(7);
   h1->Draw("same");
   
   pi3tree->Draw("fReconstruction.fMass>>pi0Mass3ClusterAngleCt(50,0,500)", 
/*           Form("fReconstruction.fAngle/0.0175<15&&%s&&%s",aCut.Data(),pairXYcut.Data()),*/
           Form("fReconstruction.fMomentum>1500&&%s&&%s",aCut.Data(),pairXYcut.Data()),
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Mass3ClusterAngleCt");
   h1->SetFillColor(6);
   h1->Draw("same");

//    pi3tree->Draw("fReconstruction.fMass>>pi0Mass3ClusterXYCts(50,0,500)", 
//            Form("%s&&%s",aCut.Data(),pairXYcut.Data()),
//       "goff");
//    h1 = (TH1*) gROOT->FindObject("pi0Mass3ClusterXYCts");
//    h1->SetFillColor(6);
//    h1->Draw("same");



   /// gamma mass vs pi0 mass
   c->cd(3);
   pi3tree->Draw("fClusterPairs.fMass:fReconstruction.fMass>>pi0MassVsGammaMass3Cluster(100,0,500,100,0,500)", 
           "(fClusters.fYMoment<10||fClusters.fYMoment>25)&&fClusters.fIsGood&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood",
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0MassVsGammaMass3Cluster");
   h1->SetFillColor(2);
   h1->SetTitle("#gamma* Mass Vs #pi^{0} Mass");
   h1->GetXaxis()->SetTitle("#pi^{0} Mass");
   h1->GetYaxis()->SetTitle("#gamma* Mass");
   h1->Draw("colz");

   /// gamma mass vs pi0 mass
   c->cd(4);
   pi3tree->Draw("fClusterPairs.fMass:fReconstruction.fMomentum>>pi0MassVsGammaMass3Cluster2(50,0,4500,50,0,1000)", 
           "(fClusters.fYMoment<10||fClusters.fYMoment>25)&&fClusters.fIsGood&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood&&TMath::Abs(fReconstruction.fMass-130)<25",
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0MassVsGammaMass3Cluster2");
   h1->SetFillColor(2);
   h1->SetTitle("#gamma* Mass Vs #pi^{0} Mass");
   h1->GetXaxis()->SetTitle("#pi^{0} Mass");
   h1->GetYaxis()->SetTitle("#gamma* Mass");
   h1->Draw("colz");


   /// Pi0 Angle
   c->cd(5);
   pi3tree->Draw("fClusterPairs[0].fMass:fClusterPairs[4].fMass>>pi0MassVsGammaMass3Cluster3(50,0,1000,50,0,1000)", 
           "fNumberOfPairs<10&&(fClusters.fYMoment<10||fClusters.fYMoment>25)&&fClusters.fIsGood&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood&&TMath::Abs(fReconstruction.fMass-130)<25",
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0MassVsGammaMass3Cluster3");
   h1->SetFillColor(2);
   h1->SetTitle("#gamma* Mass Vs #pi^{0} Mass");
   h1->GetXaxis()->SetTitle("#pi^{0} Mass");
   h1->GetYaxis()->SetTitle("#gamma* Mass");
   h1->Draw("colz");


//    pitree->Draw("fReconstruction.fAngle/0.0175:fReconstruction.fMass>>pi03AngleVMass(50,0,500,50,0,30)",
//            Form("%s&&fReconstruction.fAngle/0.0175>13",pairXYcut.Data()),
//       "goff");
//    h1 = (TH1*) gROOT->FindObject("pi03AngleVMass");
// //    h1->SetLineColor(2);
// //    h1->SetFillColor(2);
//    h1->Draw("colz");

   /// Pi0 Angle
   c->cd(6);
/*   pitree->Draw("fReconstruction.fPhi/0.0175:fReconstruction.fTheta/0.0175>>pi03AngleVMass2",*/
   pitree->Draw("fClusterPairs.fCluster2.fYMoment:fClusterPairs.fCluster2.fXMoment>>pi03AngleVMass2",
/*   pitree->Draw("fClusterPairs.fCluster2.fYMoment:fReconstruction.fAngle/0.0175>>pi03AngleVMass2",*/
           Form("%s&&fReconstruction.fAngle/0.0175<7",pairXYcut.Data()),
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi03AngleVMass2");
//    h1->SetLineColor(2);
//    h1->SetFillColor(2);
   h1->Draw("colz");
//    pi3tree->Draw("fClusters.fYMoment:fClusters.fXMoment>>Clust2XY", 
//            Form("%s&&%s",aCut.Data(),pairXYcut.Data()),
//       "goff");
//    h1 = (TH1*) gROOT->FindObject("Clust2XY");
// //    h1->SetLineColor(2);
// //    h1->SetFillColor(2);
//    h1->Draw("colz");

   /// Pi0 Angle
   c->cd(7);

   pi3tree->Draw("fReconstruction.fPhi/0.0175:fReconstruction.fTheta/0.0175>>pi0Angles(50,0,80,50,-30,30)", Form("%s&&%s",aCut.Data(),pairXYcut.Data()),
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Angles");
//    h1->SetLineColor(2);
//    h1->SetFillColor(2);
   h1->Draw("colz");


   /// Pi0 Angle
   c->cd(8);

   pi3tree->Draw("fReconstruction.fAngle/0.0175:fClusterPairs.fAngle/0.0175>>pi0Angle", 
            Form("%s&&%s",aCut.Data(),pairXYcut.Data()),
      "goff");
   h1 = (TH1*) gROOT->FindObject("pi0Angle");
//    h1->SetLineColor(2);
//    h1->SetFillColor(2);
   h1->Draw("colz");


// pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fTDCAlign>>hCerTDCW3clust(100,-300,1000)",
//       "betaDetectors1.bigcalClusterEvent.fNClusters==3","goff");
//    h1 = (TH1*) gROOT->FindObject("hCerTDCW3clust");
//    h1->SetFillColor(4);
//    h1->SetLineColor(4);
//    h1->Draw("same");
//    pitree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerTDCWadccut(100,-300,1000)",
//       "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fADCAlign-2.0)<0.5","goff");
//    h1 = (TH1*) gROOT->FindObject("hCerTDCWadccut");
//    h1->SetFillColor(3);
//    h1->SetLineColor(3);
//    h1->Draw("same");
// 



//    pi3tree->StartViewer();

//    c->SaveAs("results/pi0_3clusters.png");
//    c->SaveAs("results/pi0_3clusters.pdf");

}

