Int_t pi0mass2() {
   //load_style("MultiSquarePlot");
   //gROOT->SetStyle("MultiSquarePlot");

   Int_t aNumber = 2;

   //std::vector<Int_t> fRunList;
   //fRunList.push_back(runNumber);
   //aman->fRunQueue->push_back(502);
   //aman->fRunQueue->push_back(72913);
   aman->fRunQueue->push_back(72999);
   //aman->fRunQueue->push_back(72780);

   aman->fRunQueue->push_back(72920);
   aman->fRunQueue->push_back(72921);
   aman->fRunQueue->push_back(72922);
   aman->fRunQueue->push_back(72923);
   aman->fRunQueue->push_back(72924);
   aman->fRunQueue->push_back(72925);
   aman->fRunQueue->push_back(72926);
   aman->fRunQueue->push_back(72927);
   aman->fRunQueue->push_back(72928);
   aman->fRunQueue->push_back(72929);
   aman->fRunQueue->push_back(72930);
   aman->fRunQueue->push_back(72931);
   aman->fRunQueue->push_back(72932);
   aman->fRunQueue->push_back(72933);
   aman->fRunQueue->push_back(72934);
   aman->fRunQueue->push_back(72935);
   aman->fRunQueue->push_back(72936);
   aman->fRunQueue->push_back(72942);
   aman->fRunQueue->push_back(72943);
   aman->fRunQueue->push_back(72944);
   aman->fRunQueue->push_back(72945);
   aman->fRunQueue->push_back(72946);
   aman->fRunQueue->push_back(72947);
   aman->fRunQueue->push_back(72948);
   aman->fRunQueue->push_back(72949);

   //aman->fRunQueue->push_back(72921);
   //aman->fRunQueue->push_back(72922);
   //aman->fRunQueue->push_back(72923);
   //aman->fRunQueue->push_back(72924);
   //aman->fRunQueue->push_back(72925);
   //aman->fRunQueue->push_back(72926);
   //aman->fRunQueue->push_back(72927);
   //aman->fRunQueue->push_back(72928);
   //aman->fRunQueue->push_back(72929);

   //aman->fRunQueue->push_back(72930);
   //aman->fRunQueue->push_back(72931);
   //aman->fRunQueue->push_back(72932);
   //aman->fRunQueue->push_back(72933);
   //aman->fRunQueue->push_back(72934);
   //aman->fRunQueue->push_back(72935);

   TChain * t = 0;
   //t = (TTree*)gROOT->FindObject("pi0results");
   t = aman->BuildChain("pi0results");
   if(!t) return(-1);

   Pi0Event * pi0event        = new Pi0Event();
   t->SetBranchAddress("pi0reconstruction",&pi0event);
   TClonesArray         * clusters = 0;
   InSANEReconstruction * recon    = 0;
   Pi0ClusterPair       * pair     = 0;

   TList * hists = new TList();
   THStack * hs = new THStack("hs","test stacked histograms");

   Long64_t entries = t->GetEntries();
   std::cout << entries << " entries " << std::endl;

   TH1F * cerSumHist = new TH1F("cerSumHist","cer sum",20,0,6);
   TH1F * cerSumHist2 = new TH1F("cerSumHist2","cer sum2",20,0,6);

   TH1F * massHist = new TH1F("mass1","mass1",50,50,250);
   TH1F * mass2Hist = new TH1F("mass2","mass2",40,0,800);
   TH1F * mass3Hist = new TH1F("mass3","mass3",40,0,800);
   TH1F * mass4Hist = new TH1F("mass4","mass4",40,0,800);

   TH2F * massVSD = new TH2F("massVSD","massVSD",40,0,800,50,0,150);

   TH1F * E2Hist = new TH1F("E2Hist","E2",50,1500,3000);

   TH2F * massVSx1 = new TH2F("massVSx1","massVSx1",100,50,650,60,-60,60);
   TH2F * massVSx2 = new TH2F("massVSx2","massVSx2",100,50,650,60,-60,60);

   TH2F * massVSy1 = new TH2F("massVSy1","massVSy1",100,50,350,120,-120,120);
   TH2F * massVSy2 = new TH2F("massVSy2","massVSy2",100,50,350,120,-120,120);

   TH2F * clustxVSy1 = new TH2F("clustxVSy1","clustxVSy1",70,-70,70,120,-120,120);
   TH2F * clustxVSy2 = new TH2F("clustxVSy2","clustxVSy2",70,-70,70,120,-120,120);

   TH2F * angleVSy1 = new TH2F("angleVSy1","Angle VS y1",40,0,20,120,-120,120);
   TH2F * angleVSx1 = new TH2F("angleVSx1","Angle VS x1",40,0,20,120,-120,120);

   for(int ievent = 0 ; ievent < entries; ievent++){

      Int_t bytes =  t->GetEntry(ievent);
      //std::cout << bytes << " bytes read" << std::endl;
      for(int i = 0; i< pi0event->fClusterPairs->GetEntries() ; i++){
         pair  = (Pi0ClusterPair*)(*(pi0event->fClusterPairs))[i];
         recon = (InSANEReconstruction*)(*(pi0event->fReconstruction))[i];

         if( TMath::Abs(recon->fE1 - 2000.0) < 100.0  ) 
            if(TMath::Abs( pair->fAngle -16.0*degree) < 1.0*degree) {
               E2Hist->Fill(recon->fE2);
               mass3Hist->Fill(pair->fMass);
               massVSD->Fill(pair->fMass,recon->fClusterDistance);
               if(TMath::Abs(recon->fE2 - 2000.0) < 500.0) mass4Hist->Fill(pair->fMass);
            }
            cerSumHist->Fill( pair->fCluster1.fCherenkovBestADCSum + pair->fCluster2.fCherenkovBestADCSum);

            if( TMath::Abs(pair->fMass-135.0)<20.0 )
               cerSumHist2->Fill( pair->fCluster1.fCherenkovBestADCSum + pair->fCluster2.fCherenkovBestADCSum);

         //if(pair->fCluster1.fXMoment>-55 && pair->fCluster2.fXMoment>-55 )
         if( pair->fCluster1.fIsGood && pair->fCluster2.fIsGood) {

               if( !( (pair->fCluster1.fjPeak == 34) ||  (pair->fCluster2.fjPeak == 34) )   )
               if( !( (pair->fCluster1.fjPeak == 29) &&  (pair->fCluster1.fiPeak == 15) )   )
               if( !( (pair->fCluster2.fjPeak == 29) &&  (pair->fCluster2.fiPeak == 15) )   )
               if( !( (pair->fCluster1.fjPeak == 26) &&  (pair->fCluster1.fiPeak == 17) )   )
               if( !( (pair->fCluster2.fjPeak == 26) &&  (pair->fCluster2.fiPeak == 17) )   )
               if( !( (pair->fCluster1.fjPeak == 16) &&  (pair->fCluster1.fiPeak == 18) )   )
               if( !( (pair->fCluster2.fjPeak == 16) &&  (pair->fCluster2.fiPeak == 18) )   )
               if( !( (pair->fCluster1.fjPeak == 25) &&  (pair->fCluster1.fiPeak == 11) )   )
               if( !( (pair->fCluster2.fjPeak == 25) &&  (pair->fCluster2.fiPeak == 11) )   )
               if( !( (pair->fCluster1.fjPeak == 34) &&  (pair->fCluster1.fiPeak == 13) )   )
               if( !( (pair->fCluster2.fjPeak == 34) &&  (pair->fCluster2.fiPeak == 13) )   )
               if( !( (pair->fCluster1.fjPeak == 42) &&  (pair->fCluster1.fiPeak == 17) )   )
               if( !( (pair->fCluster2.fjPeak == 42) &&  (pair->fCluster2.fiPeak == 17) )   )
               if( !( (pair->fCluster1.fjPeak == 45) &&  (pair->fCluster1.fiPeak == 17) )   )
               if( !( (pair->fCluster2.fjPeak == 45) &&  (pair->fCluster2.fiPeak == 17) )   )
               if( !( (pair->fCluster1.fjPeak == 47) &&  (pair->fCluster1.fiPeak == 16) )   )
               if( !( (pair->fCluster2.fjPeak == 47) &&  (pair->fCluster2.fiPeak == 16) )   )
               if( !( (pair->fCluster1.fjPeak == 5) &&  (pair->fCluster1.fiPeak == 30) )   )
               if( !( (pair->fCluster2.fjPeak == 5) &&  (pair->fCluster2.fiPeak == 30) )   )
               if( !( (pair->fCluster1.fjPeak == 11) && ( pair->fCluster1.fiPeak == 17 || pair->fCluster1.fiPeak == 18) )  )
               if( !( (pair->fCluster2.fjPeak == 11) && ( pair->fCluster2.fiPeak == 17 || pair->fCluster2.fiPeak == 18) )  ){
                  if( !( (pair->fCluster1.fjPeak == 34) && ( (TMath::Abs(pair->fCluster1.fiPeak - 4) < 4) || (TMath::Abs(pair->fCluster1.fiPeak - 27) < 6)) ) )
                     if( !( (pair->fCluster2.fjPeak == 34) && ( (TMath::Abs(pair->fCluster2.fiPeak - 4) < 4) || (TMath::Abs(pair->fCluster2.fiPeak - 27) < 6)) ) ){


                        massHist->Fill( pair->fMass);
                        mass2Hist->Fill(pair->fMass);
                        massVSx1->Fill( pair->fMass,pair->fCluster1.GetXmoment());
                        massVSx2->Fill( pair->fMass,pair->fCluster2.GetXmoment());
                        massVSy1->Fill( pair->fMass,pair->fCluster1.GetYmoment());
                        massVSy2->Fill( pair->fMass,pair->fCluster2.GetYmoment());
                        if(pair->fCluster1.GetYmoment()<-20){
                           clustxVSy1->Fill( pair->fCluster1.GetXmoment(),pair->fCluster1.GetYmoment());
                           clustxVSy2->Fill( pair->fCluster2.GetXmoment(),pair->fCluster2.GetYmoment());
                        }
                        angleVSy1->Fill(pair->fAngle/degree,pair->fCluster1.GetYmoment());
                        angleVSx1->Fill(pair->fAngle/degree,pair->fCluster1.GetXmoment());
                     }
               }
               }
      }
   }

   TCanvas * c = new TCanvas("pi0MassCalib","Pi0 mass iterations");
   c->Divide(2,3);

   c->cd(1);
   massHist->Draw();
   //clustxVSy1->Draw("colz");
   //angleVSy1->Draw("colz");

   c->cd(2);
   gPad->SetLogy(true);
   mass3Hist->SetLineColor(2);
   mass3Hist->Draw();
   mass4Hist->SetLineColor(4);
   mass4Hist->Draw("same");
   mass2Hist->Draw("same");
   //clustxVSy2->Draw("colz");
   //angleVSx1->Draw("colz");

   c->cd(3);
   gPad->SetLogy(true);
   cerSumHist->Draw();
   cerSumHist2->SetLineColor(2);
   cerSumHist2->Draw("same");
   //massVSx1->SetMarkerColor(1);
   //massVSx2->SetMarkerColor(2);
   //massVSx1->Draw("colz");

   c->cd(4);
   E2Hist->Draw();
   //massVSy1->SetMarkerColor(1);
   //massVSy2->SetMarkerColor(2);
   //massVSy1->Draw("colz");

   c->cd(5);
   massVSD->Draw("colz");

   c->cd(6);
   massVSy2->Draw("colz");


   c->SaveAs(Form("results/pi0mass2_%d.png",aNumber));
   c->SaveAs(Form("results/pi0mass2_%d.pdf",aNumber));



   return(0);
}

