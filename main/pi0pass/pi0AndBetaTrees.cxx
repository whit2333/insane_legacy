Int_t pi0AndBetaTrees(Int_t runNumber = 72995) {
   TCanvas * c = new TCanvas("pi0AndBetaCanvas","Pi0 and Beta Friends",1200,700);
   c->Divide(4,2);   
   c->cd(1);
  rman->SetRun(runNumber);
/*   rman->SetRun(72999);*/
/*   rman->SetRun(72995);*/
   TTree * pitree = (TTree*) gROOT->FindObject("pi0results");
   TTree * dettree = (TTree*) gROOT->FindObject("betaDetectors1");
   pitree->BuildIndex("pi0reconstruction.fRunNumber","pi0reconstruction.fEventNumber");
   dettree->BuildIndex("fRunNumber","fEventNumber");
   pitree->AddFriend(dettree);
        gROOT->ProcessLine(".L treeviewers/pi0_viewer.C");
   treeviewer();
        tv__tree = pitree;

   TH1 * h1 = 0;

//    TString pairsCut("fReconstruction.fPi0Momentum.Mag()<4000&&\
// !(fClusterPairs.fCluster2.fiPeak==1||fClusterPairs.fCluster2.fjPeak==1)&&\
// !(fClusterPairs.fCluster2.fjPeak==1||fClusterPairs.fCluster1.fjPeak==1)&&\
// !(fClusterPairs.fCluster2.fiPeak==1||fClusterPairs.fCluster1.fiPeak==1)&&\
// !(fClusterPairs.fCluster2.fiPeak==30||fClusterPairs.fCluster1.fiPeak==30)&&\
// !(fClusterPairs.fCluster2.fiPeak==32||fClusterPairs.fCluster1.fiPeak==32)&&\
// !(fClusterPairs.fCluster1.fiPeak==1||fClusterPairs.fCluster1.fjPeak==1)&&\
// !(fClusterPairs.fCluster2.fjPeak==4||fClusterPairs.fCluster1.fjPeak==4)&&\
// !(fClusterPairs.fCluster2.fjPeak==5||fClusterPairs.fCluster1.fjPeak==5)&&\
// !(fClusterPairs.fCluster2.fjPeak==34||fClusterPairs.fCluster1.fjPeak==34)&&\
// !(fClusterPairs.fCluster2.fjPeak==56||fClusterPairs.fCluster1.fjPeak==56)&&\
// !((fClusterPairs.fCluster2.fiPeak==23&&fClusterPairs.fCluster2.fjPeak==28)||(fClusterPairs.fCluster1.fiPeak==23&&fClusterPairs.fCluster1.fjPeak==28))&&\
// !((fClusterPairs.fCluster2.fiPeak==3&&fClusterPairs.fCluster2.fjPeak==54)||(fClusterPairs.fCluster1.fiPeak==3&&fClusterPairs.fCluster1.fjPeak==54))&&\
// !((fClusterPairs.fCluster2.fiPeak==3&&fClusterPairs.fCluster2.fjPeak==52)||(fClusterPairs.fCluster1.fiPeak==3&&fClusterPairs.fCluster1.fjPeak==52))&&\
// !((fClusterPairs.fCluster2.fiPeak==16&&fClusterPairs.fCluster2.fjPeak==55)||(fClusterPairs.fCluster1.fiPeak==16&&fClusterPairs.fCluster1.fjPeak==55))&&\
// !((fClusterPairs.fCluster2.fiPeak==16&&fClusterPairs.fCluster2.fjPeak==54)||(fClusterPairs.fCluster1.fiPeak==16&&fClusterPairs.fCluster1.fjPeak==54))&&\
// !((fClusterPairs.fCluster2.fiPeak==2&&fClusterPairs.fCluster2.fjPeak==49)||(fClusterPairs.fCluster1.fiPeak==2&&fClusterPairs.fCluster1.fjPeak==49))&&\
// !((fClusterPairs.fCluster2.fiPeak==18&&fClusterPairs.fCluster2.fjPeak==8)||(fClusterPairs.fCluster1.fiPeak==18&&fClusterPairs.fCluster1.fjPeak==8))");


TString pairsCut( 
   "fReconstruction.fPi0Momentum.Mag()<6000&&fClusterPairs.fCluster1.fIsGood&&fClusterPairs.fCluster2.fIsGood&&!(fClusterPairs.fCluster2.fjPeak==34||fClusterPairs.fCluster1.fjPeak==34)&&!(fClusterPairs.fCluster2.fjPeak==56||fClusterPairs.fCluster1.fjPeak==56)&&!((fClusterPairs.fCluster1.fjPeak>=34&&fClusterPairs.fCluster1.fiPeak==30)||(fClusterPairs.fCluster2.fjPeak>=34&&fClusterPairs.fCluster2.fiPeak==30))&&!((fClusterPairs.fCluster1.fjPeak<34&&fClusterPairs.fCluster1.fiPeak==32)||(fClusterPairs.fCluster2.fjPeak<34&&fClusterPairs.fCluster2.fiPeak==32))&&!(fClusterPairs.fCluster2.fjPeak==1||fClusterPairs.fCluster1.fjPeak==1)&&!(fClusterPairs.fCluster2.fiPeak==1||fClusterPairs.fCluster1.fiPeak==1)");
/// High rate channels
///  ix   iy
///x  1    1
///x  23   28
///x  ..   34
///x  16   55
///x  1    28
///x  3    54
///x  3    52
///x  18   8
///x  16   54
///x  2    49
///  6    49
///  19   9



   /// Pi0 reconstructed theta vs phi
   c->cd(1);
   tv__tree->Draw(
      "fReconstruction.fPi0Momentum.Phi()/0.017:fReconstruction.fPi0Momentum.Theta()/0.017>>hPi0PhiTheta",pairsCut.Data(),"colz,goff");
   h1 = (TH1*)gROOT->FindObject("hPi0PhiTheta");
   if(h1)h1->SetTitle("#pi0 #phi Vs #theta");
   if(h1)h1->Draw("colz");
   /// pion cluster pair's y2-y1 VS x2-x1
   c->cd(2);
   tv__tree->Draw("fClusterPairs.fCluster2.fYMoment-fClusterPairs.fCluster1.fYMoment:fClusterPairs.fCluster2.fXMoment-fClusterPairs.fCluster1.fXMoment>>hPi0xydiff"
   ,/*pairsCut.Data()*/"fReconstruction.fPi0Momentum.Mag()<6000&&betaDetectors1.bigcalClusters[Iteration$].fIsGood&&!(betaDetectors1.bigcalClusters.fjPeak==34)&&!(betaDetectors1.bigcalClusters.fjPeak==56)&&!(betaDetectors1.bigcalClusters.fjPeak>=34&&betaDetectors1.bigcalClusters.fiPeak==30)&&!(betaDetectors1.bigcalClusters.fjPeak<34&&betaDetectors1.bigcalClusters.fiPeak==32)&&!(betaDetectors1.bigcalClusters.fjPeak==1)&&!(betaDetectors1.bigcalClusters.fiPeak==1)","colz,goff");
   h1 = (TH1*)gROOT->FindObject("hPi0xydiff");
   if(h1)h1->SetTitle("#pi0 #Delta_{x} Vs #Delta_{y}");
   if(h1)h1->Draw("colz");

   /// pion cluster pairs Delta_y VS reconstructed pion Momentum
   c->cd(3);
   tv__tree->Draw("fClusterPairs.fCluster2.fYMoment-fClusterPairs.fCluster1.fYMoment:fReconstruction.fPi0Momentum.Theta()/0.017>>hPi0ydiffTheta",
   pairsCut.Data(),"colz,goff");
   h1 = (TH1*)gROOT->FindObject("hPi0ydiffTheta");
   if(h1)h1->SetTitle("#pi0 #Delta_{y} Vs #theta");
   if(h1)h1->Draw("colz");

   /// pion momentum VS theta
   c->cd(4);
   tv__tree->Draw("betaDetectors1.bigcalClusters[].fjPeak:betaDetectors1.bigcalClusters[Iteration$].fiPeak>>hBCij" ,
     "fReconstruction.fPi0Momentum.Mag()<6000&&betaDetectors1.bigcalClusters[Iteration$].fIsGood&&!(betaDetectors1.bigcalClusters.fjPeak==34)&&!(betaDetectors1.bigcalClusters.fjPeak==56)&&!(betaDetectors1.bigcalClusters.fjPeak>=34&&betaDetectors1.bigcalClusters.fiPeak==30)&&!(betaDetectors1.bigcalClusters.fjPeak<34&&betaDetectors1.bigcalClusters.fiPeak==32)&&!(betaDetectors1.bigcalClusters.fjPeak==1)&&!(betaDetectors1.bigcalClusters.fiPeak==1)",
     "colz,goff");
   h1 = (TH1*)gROOT->FindObject("hBCij");
   if(h1)h1->SetTitle("j_{peak} vs i_{peak}");
   if(h1)h1->Draw("colz");
   tv__tree->Draw(">>yplus",
     pairsCut.Data(),
     "entrylist");
   TEntryList *elist = (TEntryList*)gDirectory->Get("yplus");
   tv__tree->SetEntryList(elist);
//      tree->Draw("py");

   /// GC TDC hits with cut 
   c->cd(5);
   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fTDCAlign>>hCerTDC(100,-300,1000)", 
           "",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hCerTDC");
   h1->SetFillColor(1);
   h1->SetTitle("Cherenkov TDC");
   h1->Draw();

   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fTDCAlign>>hCerTDCWcut(100,-300,1000)", 
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign)<30",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hCerTDCWcut");
   h1->SetLineColor(2);
   h1->SetFillColor(2);
   h1->Draw("same");
   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fTDCAlign>>hCerTDCW3clust(100,-300,1000)",
      "betaDetectors1.bigcalClusterEvent.fNClusters==3","goff");
   h1 = (TH1*) gROOT->FindObject("hCerTDCW3clust");
   h1->SetFillColor(4);
   h1->SetLineColor(4);
   h1->Draw("same");
   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerTDCWadccut(100,-300,1000)",
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fADCAlign-2.0)<0.5","goff");
   h1 = (TH1*) gROOT->FindObject("hCerTDCWadccut");
   h1->SetFillColor(3);
   h1->SetLineColor(3);
   h1->Draw("same");


   /// GC TDC hits with ADC cut 
   c->cd(6);
   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits.fADCAlign>>hCerADC",
      "fReconstruction[].fPi0Momentum.Mag()<4000",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hCerADC");
   h1->SetFillColor(1);
   h1->SetTitle("Gas Cherenkov e^{#pm} number");
   h1->Draw();
   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerADCWcut",
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign-0)<30","goff");
   h1 = (TH1*) gROOT->FindObject("hCerADCWcut");
   h1->SetFillColor(2);
   h1->SetLineColor(2);
   h1->Draw("same");
   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerADCWadccut",
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fADCAlign-0.8)<0.45&&TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign-300)<100","goff");
   h1 = (TH1*) gROOT->FindObject("hCerADCWadccut");
   h1->SetFillColor(3);
   h1->SetLineColor(3);
//   h1->Draw("same");
   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerADCW3clust",
      "betaDetectors1.bigcalClusterEvent.fNClusters==3","goff");
   h1 = (TH1*) gROOT->FindObject("hCerADCW3clust");
   h1->SetFillColor(4);
   h1->SetLineColor(4);
   h1->Draw("same");
   tv__tree->Draw("betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[].fADCAlign>>hCerADCWadcFATcut",
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits[Iteration$].fTDCAlign-300)<100","goff");
   h1 = (TH1*) gROOT->FindObject("hCerADCWadcFATcut");
   h1->SetFillColor(5);
   h1->SetLineColor(5);
//   h1->Draw("same");

   /// 
   c->cd(7);
   tv__tree->Draw("fReconstruction[].fPi0Momentum.Mag()>>hPi0P", 
           "fReconstruction[Iteration$].fPi0Momentum.Mag()<4000",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hPi0P");
   h1->SetTitle("#pi_{0} Momentum");
   h1->Draw();
   tv__tree->Draw("fReconstruction.fPi0Momentum.Mag()>>hPi0PWcut", 
      "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits.fTDCAlign-0)<30&&fReconstruction.fPi0Momentum.Mag()<4000","goff");
   h1 = (TH1*) gROOT->FindObject("hPi0PWcut");
   h1->SetFillColor(2);
   h1->SetLineColor(2);
   h1->Draw("same");

   /// 
   c->cd(8);

   tv__tree->Draw("fReconstruction.fMass>>hPi0Mass", 
           "",
      "goff");
   h1 = (TH1*) gROOT->FindObject("hPi0Mass");
   h1->SetTitle("#pi_{0} Mass");
   h1->Draw();
//    tv__tree->Draw("fReconstruction.fPi0Momentum.Mag()-betaDetectors1.monteCarloEvent.fThrownParticles.P()*1000.0>>hDelPP", 
//            "",
//       "goff");
//    h1 = (TH1*) gROOT->FindObject("hDelPP");
//    h1->Draw("");

   printf("%s\n",pairsCut.Data());
// tv__tree->Draw("fReconstruction.fPi0Momentum.Mag()-betaDetectors1.monteCarloEvent.fThrownParticles.P()*1000.0>>hDelPPWcut", 
//            "TMath::Abs(betaDetectors1.fGasCherenkovEvent.fGasCherenkovTDCHits.fTDCAlign-0)<30&&fReconstruction.fPi0Momentum.Mag()<4000",
//       "goff");
//    h1 = (TH1*) gROOT->FindObject("hDelPPWcut");
//    h1->SetFillColor(2);
//    h1->SetLineColor(2);
//    h1->Draw("same");

//    c->SaveAs("results/pi0Rejection.png");
//    c->SaveAs("results/pi0Rejection.ps");
   c->SaveAs(Form("plots/%d/pi0Rejection.png",runNumber));
   c->SaveAs(Form("plots/%d/pi0Rejection.ps",runNumber));
}

