Int_t etamass2(Int_t aNumber = 5){

   load_style("MultiSquarePlot");
   gROOT->SetStyle("MultiSquarePlot");

   Double_t Eprime       = 2000.0; //MeV
   Double_t Meta         = 547; //MeV
   Double_t theta        = TMath::ACos(1.0 - Meta*Meta/(2.0*Eprime*Eprime));
   Double_t Delta_Eprime = 1000.0;
   Double_t Delta_theta  = 40.0*degree;
   Int_t    NEventsMax   = 1e7;
   Int_t    iPrint       = 10000;

   std::cout <<  "Pi0 reconstruction symmetric with photon energy : " << Eprime << " +- " << Delta_Eprime << " MeV" << std::endl;
   std::cout <<  "Opening angle of : " << theta/degree << " +- " << Delta_theta/degree << " degrees" << std::endl;

   aman->BuildQueue2("lists/perp/5_9GeV/good_NH3.txt");
   aman->BuildQueue2("lists/perp/4_7GeV/good_NH3.txt");
   aman->BuildQueue2("lists/para/5_9GeV/good_NH3.txt");
   aman->BuildQueue2("lists/para/4_7GeV/good_NH3.txt");
   //std::vector<Int_t> fRunList;
   //fRunList.push_back(runNumber);
   //aman->fRunQueue->push_back(502);
   //aman->fRunQueue->push_back(72913);
   //aman->fRunQueue->push_back(72999);
   //aman->fRunQueue->push_back(72780);

   TChain * t = 0;
   //t = (TTree*)gROOT->FindObject("pi0results");
   t = aman->BuildChain("pi0results");
   if(!t) return(-1);

   Pi0Event * pi0event        = new Pi0Event();
   t->SetBranchAddress("pi0reconstruction",&pi0event);
   TClonesArray         * clusters = 0;
   InSANEReconstruction * recon    = 0;
   Pi0ClusterPair       * pair     = 0;

   TList * hists = new TList();
   THStack * hs = new THStack("hs","test stacked histograms");

   Long64_t entries = t->GetEntries();
   std::cout << entries << " entries " << std::endl;

   TH1F * cerSumHist = new TH1F("cerSumHist","cer sum",20,0,6);
   TH1F * cerSumHist2 = new TH1F("cerSumHist2","cer sum2",20,0,6);

   TH1F * massHist  = new TH1F("mass1","mass1",50,250,850);
   TH1F * mass2Hist = new TH1F("mass2","mass2",50,250,850);
   TH1F * mass3Hist = new TH1F("mass3","mass3",50,250,850);
   TH1F * mass4Hist = new TH1F("mass4","mass4",50,250,850);

   TH2F * massVSD = new TH2F("massVSD","massVSD",50,250,850,60,0,220);

   TH1F * E2Hist = new TH1F("E2Hist","E2",200,500,3000);
   TH1F * E2Hist2 = new TH1F("E2Hist2","E2",200,500,3000);

   TH2F * massVSx1 = new TH2F("massVSx1","massVSx1",50,250,850,20,-60,60);
   TH2F * massVSx2 = new TH2F("massVSx2","massVSx2",50,250,850,20,-60,60);

   TH2F * massVSy1 = new TH2F("massVSy1","massVSy1",50,250,850,30,-120,120);
   TH2F * massVSy2 = new TH2F("massVSy2","massVSy2",50,250,850,30,-120,120);

   TH2F * E2VSy1 = new TH2F("E2VSy1","E2 VS y1",100,500,2000,30,-120,120);
   TH2F * E2VSy2 = new TH2F("E2VSy2","E2 VS y2",100,500,2000,30,-120,120);

   TH2F * clustxVSy1 = new TH2F("clustxVSy1","clustxVSy1",32,1,33,56,1,57);
   TH2F * clustxVSy2 = new TH2F("clustxVSy2","clustxVSy2",32,1,33,56,1,57);

   TH2F * angleVSy1 = new TH2F("angleVSy1","Angle VS y1",40,0,20,120,-120,120);
   TH2F * angleVSx1 = new TH2F("angleVSx1","Angle VS x1",40,0,20,120,-120,120);

   for(int ievent = 0 ; ievent < entries && ievent < NEventsMax; ievent++){

      if(ievent%iPrint == 0) std::cout << "Event: " << ievent << std::endl;
      Int_t bytes =  t->GetEntry(ievent);

      //std::cout << bytes << " bytes read" << std::endl;
      for(int i = 0; i< pi0event->fClusterPairs->GetEntries() ; i++){
         pair  = (Pi0ClusterPair*)(*(pi0event->fClusterPairs))[i];
         recon = (InSANEReconstruction*)(*(pi0event->fReconstruction))[i];

         if( pair->fCluster1.fIsGood && pair->fCluster2.fIsGood) 
         if( TMath::Abs(recon->fE1 - Eprime) < Delta_Eprime  ) 
            if(TMath::Abs( pair->fAngle - theta) < Delta_theta) {

               E2Hist->Fill(recon->fE2);
               mass3Hist->Fill(pair->fMass);
               //if( TMath::Abs(recon->fE2 - Eprime) < Delta_Eprime )
               if(( pair->fCluster1.fCherenkovBestADCSum + pair->fCluster2.fCherenkovBestADCSum) < 0.4 )
               //if( recon->fClusterDistance > 80.0 )
               {
                  E2Hist2->Fill(recon->fE2);
                  mass4Hist->Fill(  pair->fMass );
                  massVSD->Fill(    pair->fMass,recon->fClusterDistance  );
                  massVSx1->Fill(   pair->fMass,pair->fCluster1.GetXmoment());
                  massVSx2->Fill(   pair->fMass,pair->fCluster2.GetXmoment());
                  massVSy1->Fill(   pair->fMass,pair->fCluster1.GetYmoment());
                  massVSy2->Fill(   pair->fMass,pair->fCluster2.GetYmoment());
                  E2VSy1->Fill(     recon->fE2, pair->fCluster1.GetYmoment());
                  E2VSy2->Fill(     recon->fE2, pair->fCluster2.GetYmoment());
                  clustxVSy1->Fill( pair->fCluster1.fiPeak,pair->fCluster1.fjPeak);
                  clustxVSy2->Fill( pair->fCluster2.fiPeak,pair->fCluster2.fjPeak);
               }
            }
            cerSumHist->Fill( pair->fCluster1.fCherenkovBestADCSum + pair->fCluster2.fCherenkovBestADCSum);

            if( TMath::Abs(pair->fMass-135.0)<20.0 )
               cerSumHist2->Fill( pair->fCluster1.fCherenkovBestADCSum + pair->fCluster2.fCherenkovBestADCSum);

      }
   }

   TCanvas * c = new TCanvas("pi0MassCalib","Pi0 mass iterations");
   c->Divide(3,3);

   c->cd(1);
   massVSD->Draw("colz");
   //massHist->Draw();
   //clustxVSy1->Draw("colz");
   //angleVSy1->Draw("colz");

   c->cd(2);
   //gPad->SetLogy(true);
   mass3Hist->SetLineColor(2);
   mass3Hist->Draw();
   mass4Hist->SetLineColor(4);
   mass4Hist->Draw("same");
   mass2Hist->Draw("same");
   //clustxVSy2->Draw("colz");
   //angleVSx1->Draw("colz");

   c->cd(3);
   //gPad->SetLogy(true);
   E2Hist->Draw();
   E2Hist2->SetLineColor(2);
   E2Hist2->Draw("same");
   //cerSumHist->Draw();
   //cerSumHist2->SetLineColor(2);
   //cerSumHist2->Draw("same");
   //massVSx1->SetMarkerColor(1);
   //massVSx2->SetMarkerColor(2);
   //massVSx1->Draw("colz");

   c->cd(4);
   clustxVSy2->Draw("colz");
   //massVSy1->SetMarkerColor(1);
   //massVSy2->SetMarkerColor(2);
   //massVSy1->Draw("colz");

   c->cd(5);
   massVSy1->Draw("colz");
   //E2VSy1->Draw("colz");

   c->cd(6);
   massVSy2->Draw("colz");
   //E2VSy2->Draw("colz");

   c->cd(7);
   clustxVSy1->Draw("colz");

   c->cd(8);
   massVSx1->Draw("colz");

   c->cd(9);
   massVSx2->Draw("colz");


   c->SaveAs(Form("results/pi0calibration/etamass2-%d_%d.png",int(Eprime),aNumber));
   c->SaveAs(Form("results/pi0calibration/etamass2-%d_%d.pdf",int(Eprime),aNumber));



   return(0);
}

