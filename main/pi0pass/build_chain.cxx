/*! Adds the events to the tree
 */
Int_t build_chain(Int_t number=10) {

// 	aman->QueueUpTestRuns();;
aman->GetRunQueue()->push_back(72995);
aman->GetRunQueue()->push_back(72994);
//   Create the pi0 chain
   TChain chain("pi0results");
   while(aman->fRunQueue->size()>0) {
       chain.Add(Form("data/rootfiles/InSANE%d.0.root",aman->fRunQueue->back()));
       aman->fRunQueue->pop_back();
   }
//    Pi0Event * fPi0Event = new Pi0Event();
//    fPi0Tree->SetBranchAddress("pi0reconstruction",&fPi0Event);

   TCanvas * c = new TCanvas("pi0Canvas","pi0 canvas ");
   c->Divide(2,2);
   c->cd(1);
   chain.Draw("fNTracks:fReconstruction.fMass","fReconstruction[Iteration$].fMass<300.0","colz");
   c->cd(2);
   chain.Draw("fReconstruction.fAngle:fReconstruction.fMass","fReconstruction.fMass<300","colz");

return(0);
}
