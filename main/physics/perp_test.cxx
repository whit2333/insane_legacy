Int_t perp_test(Int_t runGroup = 0) {

   aman->BuildQueue("lists/perp/test_perp.txt");

   InSANEMeasuredAsymmetry * asym = 0;
   THStack * hs = new THStack("hists","stack");
   THStack * hsplus = new THStack("histsplus","stack Plus");
   TH1F * hists[8];  
   InSANEMeasuredAsymmetry * Asymmetries[16];
   TFile * f = new TFile("data/perp_test.root","RECREATE");
   f->cd();
   TList * list =  0; //new TList();
   std::vector<Int_t> * runqueue = aman->GetRunQueue();
   list = new TList();

   for(int i = 0; i < runqueue->size() ; i++) {

      std::cout << " Start of run " << runqueue->at(i) << "\n";
      rman->SetRun(runqueue->at(i));
      TObjArray * asymmetries = (TObjArray*)((&(rman->GetCurrentRun()->fAsymmetries)));

      for(int j = 0; j<asymmetries->GetEntries() ; j++) {

         asym = (InSANEMeasuredAsymmetry*) asymmetries->At(j);
         asym->CalculateAsymmetries();

         if(i==0) {
            Asymmetries[j] = new InSANEMeasuredAsymmetry(*asym);
            list->Add(Asymmetries[j]);
         }
         else     *(Asymmetries[j]) += *asym;

         Asymmetries[j]->fAsymmetryVsx->SetTitle("Physics Asymmetry");
         Asymmetries[j]->fAsymmetryVsW->SetTitle("Physics Asymmetry");

//          Asymmetries[j]->SetDirectory(f);

      }

      std::cout << " Done with run " << runqueue->at(i) << "\n";
   }
   f->WriteObject(list,Form("run-%d-asymmetries",1));//,TObject::kSingleKey); 

   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   for(int i = 0; i<4 ; i++) {
      c->cd(i+1);
      Asymmetries[i]->fAsymmetryVsW->SetLineColor(2000);
      Asymmetries[i]->fAsymmetryVsW->SetMarkerColor(2000);
      Asymmetries[i]->fAsymmetryVsW->SetMaximum(2.0);
      Asymmetries[i]->fAsymmetryVsW->SetMinimum(-2.0);
      Asymmetries[i]->fAsymmetryVsW->Draw("E1");
      Asymmetries[i+4]->fAsymmetryVsW->SetLineColor(2001);
      Asymmetries[i+4]->fAsymmetryVsW->SetMarkerColor(2001);
      Asymmetries[i+4]->fAsymmetryVsW->Draw("E1,same");
   }

   TCanvas * c1 = new TCanvas();
   c1->Divide(2,2);
   for(int i = 0; i<4 ; i++) {
      c1->cd(i+1);
      Asymmetries[i]->fAsymmetryVsx->SetLineColor(2000);
      Asymmetries[i]->fAsymmetryVsx->SetMarkerColor(2000);
      Asymmetries[i]->fAsymmetryVsx->SetMaximum(2.0);
      Asymmetries[i]->fAsymmetryVsx->SetMinimum(-2.0);
      Asymmetries[i]->fAsymmetryVsx->Draw("E1");
      Asymmetries[i+4]->fAsymmetryVsx->SetLineColor(2001);
      Asymmetries[i+4]->fAsymmetryVsx->SetMarkerColor(2001);
      Asymmetries[i+4]->fAsymmetryVsx->Draw("E1,same");
   }

   c1->SaveAs("results/perp_test_asym_x.pdf");
   c1->SaveAs("results/perp_test_asym_x.png");
   c1->SaveAs("results/perp_test_asym_x.svg");
   c->SaveAs("results/perp_test_asym_W.pdf");
   c->SaveAs("results/perp_test_asym_W.png");
   c->SaveAs("results/perp_test_asym_W.svg");

   return(0);
}
