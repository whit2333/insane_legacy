Int_t para_plot(Int_t runGroup = 2) {

   aman->BuildQueue(Form("lists/para/run_series_%d.txt",runGroup));

   InSANEMeasuredAsymmetry * asym = 0;
   THStack * hs = new THStack("hists","stack");
   THStack * hsplus = new THStack("histsplus","stack Plus");
   TH1F * hists[16];  
   InSANEMeasuredAsymmetry * Asymmetries[16];


   TFile * f = new TFile(Form("data/para_%d.root",runGroup),"RECREATE");

   std::vector<Int_t> * runqueue = aman->GetRunQueue();
   TList *  list = new TList();

   Int_t nRuns = 0;
   std::vector<Int_t> nonNH3Runs;

   for(int i = 0; i<runqueue->size() ; i++) {
      
      std::cout << " Start of run " << runqueue->at(i) << "\n";
      rman->SetRun(runqueue->at(i));

      if(rman->GetCurrentRun()->fTarget != InSANERun::kNH3 ){
          nonNH3Runs.push_back(runqueue->at(i));
          continue;
      }
      f->cd();
      TObjArray * asymmetries = (TObjArray*)((&(rman->GetCurrentRun()->fAsymmetries)));

      for(int j = 0; j<asymmetries->GetEntries() ; j++) {
         asym = (InSANEMeasuredAsymmetry*) asymmetries->At(j);
         asym->CalculateAsymmetries();
         //asym->SetDirectory(f);

         if(nRuns==0) {
            Asymmetries[j] = new InSANEMeasuredAsymmetry(*asym);
            list->Add(Asymmetries[j]);
         }
         else     *(Asymmetries[j]) += *asym;

         Asymmetries[j]->fAsymmetryVsx->SetTitle("Asymmetry");
         Asymmetries[j]->fAsymmetryVsW->SetTitle("Asymmetry");

         std::cout << " done with asym:" << j << "\n";

         nRuns++;
      }

      std::cout << " Done with run " << runqueue->at(i) << "\n";
   }

   f->WriteObject(list,Form("run-%d-asymmetries",runGroup));//,TObject::kSingleKey); 

   std::cout << nRuns << " NH3 Runs \n";


   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   for(int i = 0; i<4 ; i++) {
      c->cd(i+1);
      Asymmetries[i]->fAsymmetryVsW->SetLineColor(2000);
      Asymmetries[i]->fAsymmetryVsW->SetMarkerColor(2000);
      Asymmetries[i]->fAsymmetryVsW->SetMaximum(2.0);
      Asymmetries[i]->fAsymmetryVsW->SetMinimum(-2.0);
      Asymmetries[i]->fAsymmetryVsW->Draw("E1");
      Asymmetries[i+4]->fAsymmetryVsW->SetLineColor(2001);
      Asymmetries[i+4]->fAsymmetryVsW->SetMarkerColor(2001);
      Asymmetries[i+4]->fAsymmetryVsW->Draw("E1,same");
   }

   TCanvas * c1 = new TCanvas();
   c1->Divide(2,2);
   for(int i = 0; i<4 ; i++) {
      c1->cd(i+1);
      Asymmetries[i]->fAsymmetryVsx->SetLineColor(2000);
      Asymmetries[i]->fAsymmetryVsx->SetMarkerColor(2000);
      Asymmetries[i]->fAsymmetryVsx->SetMaximum(2.0);
      Asymmetries[i]->fAsymmetryVsx->SetMinimum(-2.0);
      Asymmetries[i]->fAsymmetryVsx->Draw("E1");
      Asymmetries[i+4]->fAsymmetryVsx->SetLineColor(2001);
      Asymmetries[i+4]->fAsymmetryVsx->SetMarkerColor(2001);
      Asymmetries[i+4]->fAsymmetryVsx->Draw("E1,same");
   }

   c1->SaveAs(Form("results/para_%d_asym_x.pdf",runGroup));
   c1->SaveAs(Form("results/para_%d_asym_x.png",runGroup));
   c1->SaveAs(Form("results/para_%d_asym_x.svg",runGroup));
   c->SaveAs(Form("results/para_%d_asym_W.pdf",runGroup));
   c->SaveAs(Form("results/para_%d_asym_W.png",runGroup));
   c->SaveAs(Form("results/para_%d_asym_W.svg",runGroup));

   return(0);
}
