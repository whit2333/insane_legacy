Int_t perp_1(Int_t runGroup = 1) {

   aman->BuildQueue(Form("lists/perp/run_series_%d.txt",runGroup));
//   aman->BuYildQueue("lists/para/test_para.txt");

   InSANEMeasuredAsymmetry * asym = 0;
   THStack * hs = new THStack("hists","stack");
   THStack * hsplus = new THStack("histsplus","stack Plus");
   TH1F * hists[8];  
   InSANEMeasuredAsymmetry * Asymmetries[16];
   TFile * f = new TFile("data/perp_test.root","RECREATE");

   TList * list =  0; //new TList();
   std::vector<Int_t> * runqueue = aman->GetRunQueue();
   list = new TList();

//   for(int i = 0; i < 2 ; i++) {
   for(int i = 0; i < runqueue->size() ; i++) {
      std::cout << " Start of run " << runqueue->at(i) << "\n";

      rman->SetRun(runqueue->at(i));
      f->cd();
      //TObjArray * asymmetries = (TObjArray*)((TObjArray*)(&(rman->GetCurrentRun()->fAsymmetries)))->Clone();
      TObjArray * asymmetries = (TObjArray*)((&(rman->GetCurrentRun()->fAsymmetries)));
      //if(asymmetries) asymmetries->Print();

      for(int j = 0; j<asymmetries->GetEntries() ; j++) {
         asym = (InSANEMeasuredAsymmetry*) asymmetries->At(j);
         //asym->SetDirectory(f);
	 //asym->Dump();
         asym->CalculateAsymmetries();
//         asym->Print();   
//	 if(j==0) asym->fAsymmetryVsx->Draw();
         asym->fAsymmetryVsx->SetLineColor(j+1);

         if(i==0) {
            Asymmetries[j] = new InSANEMeasuredAsymmetry(*asym);
	    list->Add(Asymmetries[j]);
         }
	 else     *(Asymmetries[j]) += *asym;
         //Asymmetries[j]->Dump();
//	 hists[j] = asym->fAsymmetryVsx;
//	 hs->Add(asym->fAsymmetryVsx);
//         hsplus->Add(asym->fNPlusVsx);

	 Asymmetries[j]->SetDirectory(f);
/*	 Asymmetries[j]->Dump();*/
      }

      std::cout << " Done with run " << runqueue->at(i) << "\n";
   }
   f->WriteObject(list,Form("run-%d-asymmetries",1));//,TObject::kSingleKey); 


//    TCanvas * c = new TCanvas();
//    c->Divide(2,2);
//    for(int i = 0; i<4 ; i++) {
//       c->cd(i+1);
//       Asymmetries[i]->fAsymmetryVsx->Draw("E1");
//    }
   //   hs->Draw("nostack,E1");
//   new TCanvas();
//   hsplus->Draw("nostack");
   return(0);
}
