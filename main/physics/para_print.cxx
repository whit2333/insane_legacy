Int_t para_print(Int_t runGroup = 1) {

   aman->BuildQueue(Form("lists/para/run_series_%d.txt",runGroup));

   InSANEMeasuredAsymmetry * asym = 0;
   THStack * hs = new THStack("hists","stack");
   THStack * hsplus = new THStack("histsplus","stack Plus");
   TH1F * hists[16];  
   InSANEMeasuredAsymmetry * Asymmetries[16];
   TFile * f = new TFile(Form("data/para_%d.root",runGroup),"RECREATE");

   std::vector<Int_t> * runqueue = aman->GetRunQueue();
   TList * list = new TList();

   ofstream file("printtabletest.txt");

   for(int i = 0;i<3 && i<runqueue->size() ; i++) {
      std::cout << " Start of run " << runqueue->at(i) << "\n";

      rman->SetRun(runqueue->at(i));
      f->cd();
      
      TObjArray * asymmetries = (TObjArray*)((&(rman->GetCurrentRun()->fAsymmetries)));

      for(int j = 0; j<asymmetries->GetEntries() ; j++) {
         asym = (InSANEMeasuredAsymmetry*) asymmetries->At(j);

         asym->CalculateAsymmetries();
         asym->SetDirectory(f);

	 //asym->PrintTable(file);

         if(i==0) {
            Asymmetries[j] = new InSANEMeasuredAsymmetry(*asym);
            list->Add(Asymmetries[j]);
         }
         else     *(Asymmetries[j]) += *asym;

//         Asymmetries[j]->fAsymmetryVsx->SetTitle("Physics Asymmetry");
//         Asymmetries[j]->fAsymmetryVsW->SetTitle("Physics Asymmetry");

//         Asymmetries[j]->SetDirectory(f);

      }

//      f->WriteObject(list,Form("run-%d-asymmetries",runqueue->at(i)));//,TObject::kSingleKey); 

      std::cout << " Done with run " << runqueue->at(i) << "\n";
   }
//    f->WriteObject(list,Form("run-%d-asymmetries",1));//,TObject::kSingleKey); 

   Asymmetries[0]->PrintTableHead(file);
   for(int i = 0; i<8 ; i++) {
      Asymmetries[i]->PrintTable(file);;
   }

/*

   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   for(int i = 0; i<4 ; i++) {
      c->cd(i+1);
      Asymmetries[i]->fAsymmetryVsW->SetLineColor(2000);
      Asymmetries[i]->fAsymmetryVsW->SetMarkerColor(2000);
      Asymmetries[i]->fAsymmetryVsW->SetMaximum(2.0);
      Asymmetries[i]->fAsymmetryVsW->SetMinimum(-2.0);
      Asymmetries[i]->fAsymmetryVsW->Draw("E1");
      Asymmetries[i+4]->fAsymmetryVsW->SetLineColor(2001);
      Asymmetries[i+4]->fAsymmetryVsW->SetMarkerColor(2001);
      Asymmetries[i+4]->fAsymmetryVsW->Draw("E1,same");
   }

   TCanvas * c1 = new TCanvas();
   c1->Divide(2,2);
   for(int i = 0; i<4 ; i++) {
      c1->cd(i+1);
      Asymmetries[i]->fAsymmetryVsx->SetLineColor(2000);
      Asymmetries[i]->fAsymmetryVsx->SetMarkerColor(2000);
      Asymmetries[i]->fAsymmetryVsx->SetMaximum(2.0);
      Asymmetries[i]->fAsymmetryVsx->SetMinimum(-2.0);
      Asymmetries[i]->fAsymmetryVsx->Draw("E1");
      Asymmetries[i+4]->fAsymmetryVsx->SetLineColor(2001);
      Asymmetries[i+4]->fAsymmetryVsx->SetMarkerColor(2001);
      Asymmetries[i+4]->fAsymmetryVsx->Draw("E1,same");
   }

   c1->SaveAs(Form("results/para_%d_asym_x.pdf",runGroup));
   c1->SaveAs(Form("results/para_%d_asym_x.png",runGroup));
   c1->SaveAs(Form("results/para_%d_asym_x.svg",runGroup));
   c->SaveAs(Form("results/para_%d_asym_W.pdf",runGroup));
   c->SaveAs(Form("results/para_%d_asym_W.png",runGroup));
   c->SaveAs(Form("results/para_%d_asym_W.svg",runGroup));
*/
   return(0);
}
