Int_t perp_plot(Int_t runGroup = 21) {

   aman->BuildQueue(Form("lists/perp/run_series_%d.txt",runGroup));
   //aman->BuildQueue("lists/para/test_para.txt");

   InSANEMeasuredAsymmetry * asym = 0;
   THStack * hs = new THStack("hists","stack");
   THStack * hsplus = new THStack("histsplus","stack Plus");
   TH1F * hists[16];  
   InSANEMeasuredAsymmetry * Asymmetries[16];
   TFile * f = new TFile(Form("data/perp_%d.root",runGroup),"RECREATE");

   TList * list =  0; //new TList();
   std::vector<Int_t> * runqueue = aman->GetRunQueue();
   list = new TList();

   for(int i = 0; i < runqueue->size() ; i++) {
      std::cout << " Start of run " << runqueue->at(i) << "\n";

      rman->SetRun(runqueue->at(i));
      if( rman->GetCurrentRun()->fTarget == InSANERun::kNH3 ) {
      f->cd();

      TObjArray * asymmetries = (TObjArray*)((&(rman->GetCurrentRun()->fAsymmetries)));

      for(int j = 0; j<asymmetries->GetEntries() ; j++) {
         asym = (InSANEMeasuredAsymmetry*) asymmetries->At(j);
	 //asym->Dump();
         asym->CalculateAsymmetries();
         asym->SetDirectory(f);
         asym->Print();
//	 if(j==0) asym->fAsymmetryVsx->Draw();
         asym->fAsymmetryVsx->SetLineColor(j+1);

         if(i==0) {
            Asymmetries[j] = new InSANEMeasuredAsymmetry(*asym);
            list->Add(Asymmetries[j]);
         }
	 else     *(Asymmetries[j]) += *asym;

//          list->Add(asym);

         //Asymmetries[j]->Dump();
//	 hists[j] = asym->fAsymmetryVsx;
//	 hs->Add(asym->fAsymmetryVsx);
//         hsplus->Add(asym->fNPlusVsx);

	 Asymmetries[j]->SetDirectory(f);

/*	 Asymmetries[j]->Dump();*/
      }

      f->WriteObject(list,Form("run-%d-asymmetries",runqueue->at(i)));//,TObject::kSingleKey); 

      std::cout << " Done with run " << runqueue->at(i) << "\n";
   }
   }
//    f->WriteObject(list,Form("run-%d-asymmetries",1));//,TObject::kSingleKey); 


   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   for(int i = 0; i<4 ; i++) {
      c->cd(i+1);
      Asymmetries[i]->fAsymmetryVsW->SetLineColor(4);
      Asymmetries[i]->fAsymmetryVsW->Draw("E1");
      Asymmetries[i+4]->fAsymmetryVsW->SetLineColor(2);
      Asymmetries[i+4]->fAsymmetryVsW->Draw("E1,same");
   }

   TCanvas * c1 = new TCanvas();
   c1->Divide(2,2);
   for(int i = 0; i<4 ; i++) {
      c1->cd(i+1);
      Asymmetries[i]->fAsymmetryVsx->SetLineColor(4);
      Asymmetries[i]->fAsymmetryVsx->Draw("E1");

      Asymmetries[i+4]->fAsymmetryVsx->SetLineColor(2);
      Asymmetries[i+4]->fAsymmetryVsx->Draw("E1,same");
   }

   return(0);
}
