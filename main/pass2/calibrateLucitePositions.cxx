/** Lucite Bar Geometry
 *  
 * Should be called from a MakePlots() for a calculation. 
 * \todo clean up calculation -> MakePlot() Analysis connection
 *
 * This script gets the calibrated positions for the hodoscope
 * from cluster information. 
 * 
 */
Int_t calibrateLucitePositions(Int_t runnumber = 72994,Int_t nevents=100000) {

  const char * detectorTree = "betaDetectors2";

  if(!rman->IsRunSet() ) rman->SetRun(runnumber);
  else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
/*   rman->SetRun(runnumber);*/
   
   TTree * t = (TTree*)gROOT->FindObject(detectorTree);
   if( !t ) { 
      printf("Tree:%s \n    was NOT FOUND. Aborting... \n",detectorTree); 
      return(-1);
   }


    TCut cTrigger = "triggerEvent.IsBETAEvent()==1";
    TCut cPi0Trigger = "triggerEvent.IsNeutralPionEvent()==1";
    /* TCut cLuciteBar = "fLuciteHodoscopeEvent.fPositionHits[Iteration$].fPositionVector.fY==1";*/
    TCut cCherenkovTDCHit = "bigcalClusters.fGoodCherenkovTDCHit==1";
    TCut cLuciteTDCHit = "bigcalClusters.fGoodLuciteTDCHit==1";

   rman->GetCurrentFile()->cd("calibrations/hodoscope");



//    InSANEDetectorCalibration * lucitePositionCalibration =  new InSANEDetectorCalibration();
//    lucitePositionCalibration->SetNameTitle("LuciteXposToBigcalXpos","Lucite to bigcal x position calibration");
//    lucitePositionCalibration->fCalibrations->Clear();

   TF1 *f1;
   TH2F * h2;
   TH1F * h1;
   TProfile * p1;


   TCanvas * c = new TCanvas("hodoscopePositionCal","Hodoscope Pos Calibration",800,900);
   c->Divide(3,1);


   // First Draws lucite positions X vs Y. 
   c->cd(1);
   t->Draw("fLuciteHodoscopeEvent.fPositionHits[].fBarNumber:fLuciteHodoscopeEvent.fPositionHits[Iteration$].fPositionVector.fX>>hluc(50,-60,60,28,1,29)",
      cTrigger && cCherenkovTDCHit && cLuciteTDCHit ,
      "colz",nevents);

   // Second Draws  the Luicte X position vs the Cluster X moment 
   c->cd(2);
   t->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>pos(60,-60,60, 100,-120,120)",
      cTrigger && cCherenkovTDCHit && cLuciteTDCHit ,
      "colz",nevents);

   c->cd(3);
   t->Draw("fLuciteHodoscopeEvent.fPositionHits[].fBarNumber:fLuciteHodoscopeEvent.fPositionHits[Iteration$].fPositionVector.fX-bigcalClusters.fXMoment>>bcLucDiff(100,-60,60,28,1,29)",
      cTrigger && cCherenkovTDCHit && cLuciteTDCHit ,
      "colz",nevents);

   return(0);
}
