Int_t betaDetectorsOverview(Int_t run= 72994){

   rman->SetRun(run);

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors2");
   if(!t) {
      printf(" NO TREE\n");
      return(-1);
   }
   TCanvas * c = new TCanvas("betaDet2","betaDet2",700,700);

   c->Divide(1,6);

   c->cd(1);
   t->Draw("fGasCherenkovEvent.fGasCherenkovTDCHits.fTDCAlign>>cerTDCAlign(150,-150,150)","","");
   
   c->cd(2);
   t->Draw("fGasCherenkovEvent.fGasCherenkovTDCHits.fADCAlign>>cerADCAlign(300,0,3)","","");

   c->cd(3);
   t->Draw("fGasCherenkovEvent.fGasCherenkovTDCHits.fNPE>>cerNPE(300,0,40)","","");

   c->cd(4);
   t->Draw("fForwardTrackerEvent.fTrackerPositionHits.fTDCSum>>trackerTDCSum(300,-150,150)","","");

   return(0);
}
