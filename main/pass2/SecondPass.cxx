/*! \page secondpass Second Pass documentation page
   The "second pass" uses SANESecondPassAnalyzer to make a few corrections and run a few calculations.

    \section sp0 Input and Output 
   Using SANEFirstPassAnalyzer, it opens the first pass tree "betaDetectors1" 

    \section sp1 Description
    
   
   \subsection sp2 Corrections

   \subsection sp3 Calculations
     - LuciteHodoscopeCalculation2
     - SANEElectronSelector2 : creates tree of cleanedBetaEvents

  \section sp4 Bugs

  \section sp5 Future Improvements

  - 
*/


/**  Second Pass Analysis script
 *
 *  The Primary job here is to complete the data members for the event class
 *  CleanedBETAEvent
 *
 *  The Steps of a good analyzer pass script
 *
 *  - Name input and output trees
 *  - Define the analyzer for this pass. \note This does not yet create the trees! This happens at initialize
 *  - Create each detector used by the analysis and add it to the analyzer. 
 *    Currently all the detectors are created in InSANEDetectorAnalysis
 *  - Define and setup each "Correction" and "Calculation".
 *  - Set each detector used by the analysis
 *  - Add the corrections and calculations 
 *    \note Order Matters. It is the execution of the list from first to last. 
 *  - Initialze and Process 
 * 
 */
Int_t SecondPass(Int_t runNumber = 72995) {
  gROOT->SetBatch(kTRUE);

  Bool_t allowEventDisplay = false;
  const char * sourceTreeName = "betaDetectors1";
  const char * outputTreeName = "betaDetectors2";

  if(!rman->IsRunSet() ) rman->SetRun(runNumber);
  else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;

   /// Clean up the file
   //rman->GetCurrentFile()->Delete("betaDetectors2;*");
   //rman->GetCurrentFile()->Delete("cleanedEvents;*");
   //rman->GetCurrentFile()->Delete("betaTrajectories;*");

   /// Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
   std::cout << " + Creating SANESecondPassAnalyzer\n"; 
   SANESecondPassAnalyzer* analyzer = 
     new SANESecondPassAnalyzer(outputTreeName ,sourceTreeName);
   aman->SetAnalyzer(analyzer);



//_______________ DETECTORS _____________//
  analyzer->AddDetector(analyzer->fCherenkovDetector);
  analyzer->AddDetector(analyzer->fBigcalDetector);
  analyzer->AddDetector(analyzer->fHodoscopeDetector);
  analyzer->AddDetector(analyzer->fTrackerDetector);

//___________BETA DETECTORS Ind. Calculations_________//

//____________ Add Corrections  _______________//
/// Time walk correction \see time_walk.cxx which must be executed first
//analyzer->AddCorrection(timewalkcorr);

//____________ Add Calculations _______________//
/// SANETrackCalculation1 is verymuch like SANEElectronSelector
/// 
  std::cout << " + Creating SANETrackCalculation2\n";
  SANETrackCalculation2 * track2;
  track2 = new SANETrackCalculation2(analyzer);
  track2->SetDetectors(analyzer);
  //track1->fReconEvent = traj3->fReconEvent; // now done in SANETrackCalculation2::Initialize()


// Add the calculations to the analyzer (order matters)
   analyzer->AddCalculation(track2);

//____________ Initialize and Process _______________//

   analyzer->Initialize();

   analyzer->Process();

   // is this needed?
   rman->WriteRun();

   delete analyzer;
   
   std::cout << " - Finished SecondPass.cxx\n";

  //if(gROOT->IsBatch())   gROOT->ProcessLine(".q");
  return(0);
}
