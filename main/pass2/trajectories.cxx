Int_t trajectories(Int_t runNumber=72950){
   rman->SetRun(runNumber);

   const char * treeName = "betaTrajectories";

   TTree * t = (TTree*)gROOT->FindObject(treeName);
   if(!t){ std::cout << " TREE NOT FOUND : " << treeName << "\n"; return(-1);}
   TCanvas * c = new TCanvas("ctrajectories","Trajectories",1200,800);
   c->Divide(3,2);

   c->cd(1);
   t->Draw("@fTrajectories.size()","@fTrajectories.size()>0","");

   c->cd(2);
   t->Draw("fTrajectories.fNCherenkovElectrons","@fTrajectories.size()>0","");
 
   c->cd(3);
   t->Draw("@fPositions.size()","","");

   c->cd(4);
   t->Draw("fPositions.fMissDistance","","");

   c->cd(5);
   t->Draw("fPositions.fCluster[].GetXmoment()","","");



   TH1F * h1 = new TH1F("xpos","Luc X pos",100,-100,100);
   TH1F * h2 = new TH1F("ypos","Luc Y pos",100,-100,100);
   TH1F * h3 = new TH1F("zpos","Luc Z pos",100,-100,100);
  /* 
   TClonesArray * trajectories = new TClonesArray("InSANETrajectories",1);
   InSANETrajectory * aTraj = new InSANETrajectory();
   TVector3 * aVec = new TVector3();
   t->SetBranchAddress("fTrajectories",&trajectories);

   for(int i = 0;i<t->GetEntries();i++){
      t->GetEntry(i);
      for(int j = 0; j<trajectories->GetEntries();j++){
         aTraj = (InSANETrajectory*)(*trajectories)[j];
     	 for(int k = 0;k<aTraj->fPositions->GetEntries();k++){
            aVec = (TVector3*)(* aTraj->fPositions)[k];
            h1->Fill(aVec->X());
            h2->Fill(aVec->Y());
            h3->Fill(aVec->Z());
            //h1->Fill(aVec->X());
            //h1->Fill(aVec->X());
	 }
      }
   }
   c->cd(4); h1->Draw();
   c->cd(5); h2->Draw();
   c->cd(6); h3->Draw();
*/



   //t->Draw("fTrajectories.fPositions.fX","@fTrajectories.size()>0","");
   t->StartViewer();
   return(0);
}
