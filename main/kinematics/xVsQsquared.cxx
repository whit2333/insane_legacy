Int_t xVsQsquared(){

   Int_t nSamp = 1000;
   Double_t fxMin = 0.0;
   Double_t fxMax = 0.95;
   Double_t fQ2Min = 0.10;
   Double_t fQ2Max = 10.0;
   Double_t fWMin = 1.0;
   Double_t fWMax = 5.0;

   TF1 * f1 = new TF1("f1","InSANE::Kine::Q2_xW(x,1.2)",fxMin,fxMax);
   TF1 * f2 = new TF1("f2","InSANE::Kine::Q2_xW(x,2.0)",fxMin,fxMax);
   TF1 * f3 = new TF1("f3","InSANE::Kine::Q2_xW(x,3.0)",fxMin,fxMax);
   f1->SetLineWidth(2);
   f2->SetLineWidth(2);
   f3->SetLineWidth(2);

   TH2F * h2 = 0;
   InSANEAcceptance * acc = 0;
   TLegend * leg = new TLegend(0.75,0.7,0.9,0.9);
   TLegend * leg2 = new TLegend(0.6,0.7,0.73,0.9);

   /// SLAC E143
   acc = new InSANEAcceptance("SLACE143","SLAC E143");
   acc->SetBeamEnergy(9.7);//GeV
   acc->SetEnergyMax(8.0);
   acc->SetEnergyMin(22.0);
   acc->SetThetaMax(5.0*TMath::Pi()/180.0);
   acc->SetThetaMin(4.0*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC143_1 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC143_1->SetLineColor(2003);
   leg->AddEntry(hSLAC143_1,"SLAC E143 ","f");
   //leg->AddEntry(hSLAC143_1,"SLAC E143 9.7GeV","f");

   acc->SetBeamEnergy(16.2 );//GeV
   acc->Initialize();
   TH2F * hSLAC143_2 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC143_2->SetLineColor(2003);

   acc->SetBeamEnergy(29.1);//GeV
   acc->Initialize();
   TH2F * hSLAC143_3 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC143_3->SetLineColor(2003);
   //leg->AddEntry(hSLAC143_2,"SLAC E143 29.1GeV","f");

   /// CLAS
   acc = new InSANEAcceptance("CLAS1","CLAS");
   acc->SetBeamEnergy(2.6);//GeV
   acc->SetEnergyMax(0.50);
   acc->SetEnergyMin(2.0);
   acc->SetThetaMax(20*TMath::Pi()/180.0);
   acc->SetThetaMin(50*TMath::Pi()/180.0);

   acc->Initialize();
   TH2F * hCLAS_1 = acc->KinematicCoverage_xQ2(nSamp);
   hCLAS_1->SetLineColor(2001);
   leg->AddEntry(hCLAS_1,"CLAS","f");

   /// SLAC E155
   acc = new InSANEAcceptance("SLACE155","SLAC E155");
   acc->SetBeamEnergy(48.35);//GeV
   acc->SetEnergyMin(10.0);
   acc->SetEnergyMax(44.0);
   acc->SetThetaMax(3.0*TMath::Pi()/180.0);
   acc->SetThetaMin(2.50*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC155_1 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155_1->SetLineColor(6);
   leg->AddEntry(hSLAC155_1,"SLAC E155","f");

   acc->SetEnergyMin(10.0);
   acc->SetEnergyMax(39.0);
   acc->SetThetaMax(5.25*TMath::Pi()/180.0);
   acc->SetThetaMin(5.75*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC155_2 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155_2->SetLineColor(6);

   acc->SetEnergyMin(6.0);
   acc->SetEnergyMax(20.0);
   acc->SetThetaMax(10.25*TMath::Pi()/180.0);
   acc->SetThetaMin(10.75*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC155_3 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155_3->SetLineColor(6);

   /// SLAC E155x
   acc = new InSANEAcceptance("SLACE155x","SLAC E155x");
   acc->SetBeamEnergy(29.1);//GeV
   acc->SetEnergyMin(10.0);
   acc->SetEnergyMax(44.0);
   acc->SetThetaMax(3.0*TMath::Pi()/180.0);
   acc->SetThetaMin(2.50*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC155x_1 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155x_1->SetLineColor(4006);
   leg->AddEntry(hSLAC155x_1,"SLAC E155x","f");

   acc->SetEnergyMin(10.0);
   acc->SetEnergyMax(39.0);
   acc->SetThetaMax(5.25*TMath::Pi()/180.0);
   acc->SetThetaMin(5.75*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC155x_2 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155x_2->SetLineColor(4006);

   acc->SetEnergyMin(6.0);
   acc->SetEnergyMax(20.0);
   acc->SetThetaMax(10.25*TMath::Pi()/180.0);
   acc->SetThetaMin(10.75*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC155x_3 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155x_3->SetLineColor(4006);

   acc->SetBeamEnergy(32.3);//GeV
   acc->SetEnergyMin(10.0);
   acc->SetEnergyMax(44.0);
   acc->SetThetaMax(3.0*TMath::Pi()/180.0);
   acc->SetThetaMin(2.50*TMath::Pi()/180.0);
   TH2F * hSLAC155x_4 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155x_4->SetLineColor(4006);

   acc->SetEnergyMin(10.0);
   acc->SetEnergyMax(39.0);
   acc->SetThetaMax(5.25*TMath::Pi()/180.0);
   acc->SetThetaMin(5.75*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC155x_5 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155x_5->SetLineColor(4006);

   acc->SetEnergyMin(6.0);
   acc->SetEnergyMax(20.0);
   acc->SetThetaMax(10.25*TMath::Pi()/180.0);
   acc->SetThetaMin(10.75*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSLAC155x_6 = acc->KinematicCoverage_xQ2(nSamp);
   hSLAC155x_6->SetLineColor(4006);

   /// SANE Kinematics
   acc = new InSANEAcceptance("SANE4Pass","SANE 4 Pass");
   acc->SetBeamEnergy(4.7);
   acc->SetEnergyMin(0.6);
   acc->SetEnergyMax(3.0);
   acc->SetThetaMin(33.0*TMath::Pi()/180.0);
   acc->SetThetaMax(47.0*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSANE4Pass = acc->KinematicCoverage_xQ2(nSamp);
   hSANE4Pass->SetLineColor(2);
   leg->AddEntry(hSANE4Pass,"SANE 4.7 GeV","f");

   acc = new InSANEAcceptance("SANE5Pass","SANE 5 Pass");
   acc->SetBeamEnergy(5.9);
   acc->SetEnergyMin(0.6);
   acc->SetEnergyMax(3.0);
   acc->SetThetaMin(33.0*TMath::Pi()/180.0);
   acc->SetThetaMax(47.0*TMath::Pi()/180.0);
   acc->Initialize();
   TH2F * hSANE5Pass = acc->KinematicCoverage_xQ2(nSamp);
   hSANE5Pass->SetLineColor(4);
   leg->AddEntry(hSANE5Pass,"SANE 5.9 GeV","f");

   leg2->AddEntry(f1,"W=1.2 GeV","l");
   leg2->AddEntry(f2,"W=2.0 GeV","l");
   leg2->AddEntry(f3,"W=3.0 GeV","l");

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   TCanvas * c = new TCanvas("xVsQsquared","x vs Q2");
   //c->Divide(1,2);

   //c->cd(1);
   //gPad->SetLogx(true);
   //gPad->SetLogy(true);

   f1->SetLineColor(1);
   gr = new TGraph(f1->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   //f1->Draw();
   //f1->SetMaximum(fQ2Max);
   //f1->SetMinimum(0);

   f2->SetLineColor(2);
   gr = new TGraph(f2->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   f3->SetLineColor(4);
   gr = new TGraph(f3->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(fQ2Min,fQ2Max);

   hSANE4Pass->Draw("box,same");
   hSANE5Pass->Draw("box,same");
   
   hSLAC143_1->Draw("box,same");
   hSLAC143_2->Draw("box,same");
   hSLAC143_3->Draw("box,same");

   hSLAC155_1->Draw("box,same");
   hSLAC155_2->Draw("box,same");
   hSLAC155_3->Draw("box,same");

   hSLAC155x_1->Draw("box,same");
   hSLAC155x_2->Draw("box,same");
   hSLAC155x_3->Draw("box,same");
   hSLAC155x_4->Draw("box,same");
   hSLAC155x_5->Draw("box,same");
   hSLAC155x_6->Draw("box,same");


   hCLAS_1->Draw("box,same");

   leg->Draw();
   leg2->Draw();

   //c->cd(2);
   //hSLAC143_1->Draw("box");
   //hSLAC143_2->Draw("box,same");

   c->SaveAs("results/kinematics/xVsQsquared_test.png");
   c->SaveAs("results/kinematics/xVsQsquared_test.pdf");

   return(0);
}
