/*!
Q^2_lo	Q^2	Q^2_hi
1.89	2.22	2.55
2.55	3.00	3.45
3.45	4.06	4.67
4.67	5.49	6.31

 */
Int_t binning(Double_t Qsq = 4.06,Double_t QsqBinWidth=0.66) {

   TCanvas * c = new TCanvas("xg1p","xg1p");
   c->cd(0);
   /// Create Legend
   TLegend * leg = new TLegend(0.6,0.5,0.875,0.875);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));



   /// First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg1p = new TF1("xg1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");

   xg1p->SetParameter(0,Qsq);
   xg1p->SetLineColor(1);

   Int_t width = 3;
   xg1p->SetLineWidth(width);
   xg1p->SetLineStyle(1);

   xg1p->Draw();

/*   TF1 * W_xAxis = new TF1("f1","-x+0.0*[0]",xmin,xmax);*/
   TF1 * W_xAxis = new TF1("f1","InSANE::Kine::W_xQsq(x,[0])",xmin,xmax);
   W_xAxis->SetParameter(0,Qsq);
   W_xAxis->SetLineColor(2);
//    W_xAxis->Draw();

   TGaxis *axis = new TGaxis(0.0,xg1p->GetMaximum(),
         1.0,xg1p->GetMaximum(),"f1");//0,rightmax,510,"+L");
   axis->SetLineColor(kBlue);
   axis->SetLabelColor(kBlue);
   axis->SetLabelSize(0.03);
   axis->SetTitleSize(0.02);
   axis->SetTitleOffset(1.2);

   axis->Draw();



   return(0);
}