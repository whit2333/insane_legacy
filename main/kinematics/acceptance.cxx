Int_t acceptance(){

   InSANEAcceptance * acc = new InSANEAcceptance("SLACE143","SLAC E143");
   acc->SetBeamEnergy(21.0);//GeV
   acc->SetEnergyMax(12.0);
   acc->SetEnergyMin(7.0);
   acc->SetThetaMax(5.0*TMath::Pi()/180.0);
   acc->SetThetaMin(4.0*TMath::Pi()/180.0);

   acc->Initialize();

   TH2F * h2 = acc->KinematicCoverage_xQ2();
   h2->SetLineColor(2); 
   h2->Draw("box");
   return(0);
}
