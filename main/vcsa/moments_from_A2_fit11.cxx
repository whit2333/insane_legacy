void moments_from_A2_fit11(int anumber = 1145) {

  using namespace insane::physics;

  int Wmin       = 1199;
  int n_params   = 9;
  int n_t3_params   = 5;
  
  typedef  insane::physics::Stat2015_PPDFs T2PDF  ;
  std::string pdf_names =  "Stat2015";
  //typedef  insane::physics::JAM_PPDFs T2PDF;
  //std::string pdf_names =  "JAM";

  //typedef  wevolve::SimpleFit_T3DFs3   T3FIT;  // 700 series
  //typedef  wevolve::SimpleFit_T3DFs   T3FIT;   // 800 series
  //typedef  wevolve::SimpleFit_T3DFs2   T3FIT;    // 900 series
  typedef  wevolve::SimpleFit_T3DFs4   T3FIT;    // 900 series
  pdf_names += "_F4";

  // ------------------------------------------------
  
  std::string dir_name  = Form("/home/whit/work/SANE/results/A2_fit/Wmin_%d_%dpars", Wmin, n_params);

  TMatrixDSym* cov  = nullptr;
  TMatrixDSym* corr = nullptr;
  std::vector<double>* result_pars = nullptr;
  std::map<std::string,double>* result_values = nullptr;
  //result_values["Wmin"] = W_min_data;
  //result_values["Q2min"] = Q20-Q2width;
  //result_values["E"] = Q20-Q2width;
  //result_values["Prob"] = result.Prob();
  //result_values["Ndf"] = result.Ndf();
  //result_values["Chi2"] = result.Chi2();

  TFile * f_result = new TFile(Form("%s/A2_fit_%d.root",dir_name.c_str(),anumber),"UPDATE");
  f_result->cd();
  f_result->GetObject("cov_matrix" , cov         );
  f_result->GetObject("corr_matrix", corr        );
  f_result->GetObject("parameters" , result_pars );
  f_result->GetObject("result_values" , result_values );
  std::cout << "Prob : " << (*result_values)["Prob"] << std::endl;

  for(auto val : (*result_values)) {
    std::cout << val.first << " : " << val.second << std::endl;
  }

  double Chi2 = (*result_values)["MinFcnValue"];
  int    Ndf  = (*result_values)["Ndf"];  ;

  std::vector<double> parameters;
  for(int ip = 0; ip < n_t3_params; ++ip){
    parameters.push_back((*result_pars)[ip]);
  }
  for(auto apr: parameters) {
    std::cout << apr << ", "; 
  }
  std::cout << std::endl;

  // ---------------------------------------------------------------
  
  auto ssfs = new SSFsFromPDFs<T2PDF,T3FIT>();
  std::get<T3FIT>(ssfs->fDFs).SetParams(parameters,true);
  for(auto apr: std::get<T3FIT>(ssfs->fDFs).GetParams()) {
    std::cout << apr << ", "; 
  }
  std::cout << std::endl;

  TGraph * d2_gr              = new TGraph();
  TGraph * d2_TMC_gr          = new TGraph();
  TGraph * M23_gr             = new TGraph();
  TGraph * M23_TMC_gr         = new TGraph();
  TGraph * g2bar_3_gr         = new TGraph();
  TGraph * g2bar_TMC_gr       = new TGraph();
  TGraph * g2bar_minus_M23_gr = new TGraph();

  double Q2_min = 0.8;
  double Q2_max = 5.5;
  double x_min  = 0.05;
  double x_max  = 0.8;
  int Npoints  = 10;

  std::vector<double> vars = {0.0};

  std::function<double(const double*, const double*)> func_M23 = [&](const double * x, const double * p){
      std::vector<double> pars_temp(n_t3_params,0.0);
      for(int ipars = 0 ; ipars < n_t3_params; ipars++) { pars_temp[ipars] = p[ipars]; }
      std::get<T3FIT>(ssfs->fDFs).SetParams(pars_temp);
      double Q2__  = x[0];
      double xmax__ = InSANE::Kine::xBjorken_WQsq(0.938+0.139,Q2__);
      //double xbj = InSANE::Kine::xBjorken_WQ2(W,Qsq);
      double res = ssfs->M2n_TMC_p(3,  Q2__, x_min, xmax__)*2.0;
      return res;
      };

  TH1  * hd2_integrand = new TH1F("d2_integrand","d2 integrand", 10, Q2_min, Q2_max);
  //TH1  * ci_hist = NucDB::GetConfidenceIntervals(*cov, Chi2, Ndf, 0.95, func_M23, 1, parameters, hd2_integrand, 0, vars);
  std::cout << " CI hist done\n";

  std::get<T3FIT>(ssfs->fDFs).SetParams(parameters);

  // ---------------------------------------------------------------
  //
  AMTFormFactors * ffs = new AMTFormFactors();
  //
  auto g1 = [&](double x, double Q2){return ssfs->g1p_TMC(x,Q2);};
  auto g2 = [&](double x, double Q2){return ssfs->g2p_TMC(x,Q2);};

  auto M_g1 = [&](int n, double Q2){
    return insane::physics::CN_moment([&](double x){return g1(x,Q2);},n,x_min,x_max)+ffs->g1p_el(Q2);
  };
  auto M_g2 = [&](int n, double Q2){
    return insane::physics::CN_moment([&](double x){return g2(x,Q2);},n,x_min,x_max)+ffs->g2p_el(Q2);
  };
  auto M_g1_el = [&](int n, double Q2){
    return ffs->g1p_el(Q2);
  };
  auto M_g2_el = [&](int n, double Q2){
    return ffs->g2p_el(Q2);
  };

  auto M_0_g1 = [&](double Q2){return M_g1(0,Q2);};
  auto M_1_g1 = [&](double Q2){return M_g1(1,Q2);};
  auto M_2_g1 = [&](double Q2){return M_g1(2,Q2);};
  auto M_3_g1 = [&](double Q2){return M_g1(3,Q2);};
  auto M_4_g1 = [&](double Q2){return M_g1(4,Q2);};
  auto M_el_g1= [&](double Q2){return M_g1_el(0,Q2);};

  auto M_0_g2 = [&](double Q2){return M_g2(0,Q2);};
  auto M_1_g2 = [&](double Q2){return M_g2(1,Q2);};
  auto M_2_g2 = [&](double Q2){return M_g2(2,Q2);};
  auto M_3_g2 = [&](double Q2){return M_g2(3,Q2);};
  auto M_4_g2 = [&](double Q2){return M_g2(4,Q2);};
  auto M_el_g2= [&](double Q2){return M_g2_el(0,Q2);};

  auto d2_CN = [&](double Q2){return 2.0*M_g1(2,Q2) + 2.0*(3.0/2.0)*M_g2(2,Q2);};
  auto d4_CN = [&](double Q2){return 2.0*M_g1(4,Q2) + 2.0*(5.0/4.0)*M_g2(4,Q2);};
  auto d6_CN = [&](double Q2){return 2.0*M_g1(6,Q2) + 2.0*(7.0/6.0)*M_g2(6,Q2);};
  auto d8_CN = [&](double Q2){return 2.0*M_g1(8,Q2) + 2.0*(9.0/8.0)*M_g2(8,Q2);};

  auto ELT_SR  = [&](double Q2){return M_g1(1,Q2) + 2.0*M_g2(1,Q2);};
  auto BC_SR   = [&](double Q2){return M_g2(0,Q2);};
  auto mu_2    = [&](double Q2){
    return insane::physics::CN_moment([&](double x){return std::get<T2PDF>(ssfs->fDFs).g1p_Twist2_TMC(x,Q2);},
                                      0,
                                      x_min,
                                      x_max );
  };
  auto Gamma_1    = [&](double Q2){
    return insane::physics::CN_moment([&](double x){return ssfs->g1p_TMC(x,Q2);},
                                      0,
                                      x_min,
                                      x_max );
      //+ ffs->g1p_el(Q2);
  };
  auto Delta_Gamma  = [&](double Q2){return Gamma_1(Q2)-mu_2(Q2);};

  auto f2 = [&](double Q2){
    double c0     = (M_p/GeV)*(M_p/GeV)/(Q2*9.0);
    double c1     = -4.0*(M_p/GeV)*(M_p/GeV)/(Q2*9.0);
    double a2     = M_2_g1(Q2);
    double d2     = d2_CN(Q2);
    double Gamma1 = Gamma_1(Q2);
    double mu2    = mu_2(Q2);
    return( (Gamma1-mu2-c0*(a2+4.0*d2))/c1 );
  };

  auto f2_overQ = [&](double OverQ2){
    double Q2     = 1.0/OverQ2;
    double c0     = (M_p/GeV)*(M_p/GeV)/(Q2*9.0);
    double c1     = -4.0*(M_p/GeV)*(M_p/GeV)/(Q2*9.0);
    double a2     = M_2_g1(Q2);
    double d2     = d2_CN(Q2);
    double Gamma1 = Gamma_1(Q2);
    double mu2    = mu_2(Q2);
    return( (Gamma1-mu2-c0*(a2+4.0*d2)) );
  };

  std::vector<std::pair<std::function<double(double)>, std::string>> moments = {
    {M_0_g1, "g_{1} n=0"}, {M_0_g2, "g_{2} n=0"}, 
    {M_1_g1, "g_{1} n=1"}, {M_1_g2, "g_{2} n=1"}, 
    {M_2_g1, "g_{1} n=2"}, {M_2_g2, "g_{2} n=2"}, 
    {M_3_g1, "g_{1} n=3"}, {M_3_g2, "g_{2} n=3"},
    {M_4_g1, "g_{1} n=4"}, {M_4_g2, "g_{2} n=4"},
    {M_el_g1, "g_{1} elastic"}, {M_el_g2, "g_{2} elastic"}
  };

  TMultiGraph* moments_mg = new TMultiGraph();
  int i_mom = 0;
  for(auto f : moments) {
    std::cout << i_mom << std::endl;
    auto fcn = [&](double* x, double* p){return (f.first)(x[0]);};
    auto a_moment = new TF1("moment",fcn, Q2_min, Q2_max,0);
    a_moment->SetNpx(Npoints);
    auto gr = new TGraph(a_moment); gr->SetTitle(f.second.data());
    gr->SetLineWidth(3);
    if(i_mom%2==0) { gr->SetLineStyle(2);}
    gr->Print();
    moments_mg->Add(gr,"c");
    i_mom++;
  }

  // ---------------------------------------------------------------
  //
  std::vector<std::pair<std::function<double(double)>, std::string>> SRs = {
    {ELT_SR, "ELT SR"}, {BC_SR, "BC SR"}, 
    {Gamma_1, "Gamma_1"},
    {Delta_Gamma, "Delta_Gamma"}, {mu_2, "mu_2"},
    {d2_CN, "d2_CN"},
    {d4_CN, "d4_CN"},
    {d6_CN, "d6_CN"},
    {d8_CN, "d8_CN"},
    {f2, "f2"},

  };
  TMultiGraph* SR_mg  = new TMultiGraph();
  i_mom = 0;
  for(auto f : SRs) {
    std::cout << i_mom << std::endl;
    auto fcn = [&](double* x, double* p){return (f.first)(x[0]);};
    auto a_moment = new TF1("moment",fcn, Q2_min, Q2_max,0);
    a_moment->SetNpx(Npoints);
    auto gr = new TGraph(a_moment); gr->SetTitle(f.second.data());
    gr->SetLineWidth(3);
    if(i_mom%2==0) { gr->SetLineStyle(2);}
    gr->Print();
    SR_mg->Add(gr,"c");
    i_mom++;
  }

  // ---------------------------------------------------------------
  //
  std::vector<std::pair<std::function<double(double)>, std::string>> MEs = {
    {f2_overQ, "f2"},
  };
  TMultiGraph* ME_mg  = new TMultiGraph();
  i_mom = 0;
  for(auto f : MEs) {
    std::cout << i_mom << std::endl;
    auto fcn = [&](double* x, double* p){return (f.first)(x[0]);};
    auto a_moment = new TF1("moment",fcn,1.0/Q2_max , 1.0/Q2_min,0);
    a_moment->SetNpx(Npoints);
    auto gr = new TGraph(a_moment); gr->SetTitle(f.second.data());
    gr->SetLineWidth(3);
    if(i_mom%2==0) { gr->SetLineStyle(2);}
    gr->Print();
    ME_mg->Add(gr,"c");
    i_mom++;
  }

  // ----------------------------------------
  //
  TCanvas*     c   = new TCanvas();
  gStyle->SetOptTitle(kFALSE);
  //gStyle->SetPalette(kSolar);

  moments_mg->Draw("a PLC");
  auto leg = gPad->BuildLegend();
  leg->SetNColumns(2);

  gSystem->mkdir("results/vcsa/moments_from_A2_fit11",true);
  c->SaveAs("results/vcsa/moments_from_A2_fit11_0.pdf");

  // ----------------------------------------
  //
  c   = new TCanvas();
  SR_mg->Draw("a PLC");
  auto SR_leg = gPad->BuildLegend();
  SR_leg->SetNColumns(2);

  c->SaveAs("results/vcsa/moments_from_A2_fit11_1.pdf");

  // ----------------------------------------
  //
  c   = new TCanvas();
  ME_mg->Draw("a PLC");
  auto ME_leg = gPad->BuildLegend();
  ME_leg->SetNColumns(2);

  c->SaveAs("results/vcsa/moments_from_A2_fit11_2.pdf");

  //d2_gr     ->SetLineColor(1);
  //d2_TMC_gr ->SetLineColor(1);
  //d2_TMC_gr ->SetLineStyle(2);
  //M23_gr    ->SetLineColor(2);
  //M23_TMC_gr->SetLineColor(2);
  //M23_TMC_gr->SetLineStyle(2);
  //g2bar_3_gr->SetLineColor(4);
  //g2bar_3_gr->SetLineStyle(1);
  //g2bar_TMC_gr->SetLineColor(4);
  //g2bar_TMC_gr->SetLineStyle(2);

  //d2_gr     ->SetLineWidth(2);
  //d2_TMC_gr ->SetLineWidth(2);
  //M23_gr    ->SetLineWidth(2);
  //M23_TMC_gr->SetLineWidth(2);
  //g2bar_3_gr->SetLineWidth(2);
  //g2bar_TMC_gr->SetLineWidth(2);

  ////auto         * ci_gr = new TGraphErrors(ci_hist);
  ////ci_gr->SetFillColorAlpha( kRed, 0.35 );
  ////ci_gr->SetLineColor( kBlue );
  ////ci_gr->SetLineWidth( 3 );
  ////mg->Add(ci_gr,"le3");

  //leg->AddEntry(d2_gr,"I(M=0)","l");
  //leg->AddEntry(d2_TMC_gr,"I","l");
  //leg->AddEntry(M23_gr,"2 M_{2}^{3}(M=0)","l");
  //leg->AddEntry(M23_TMC_gr,"2 M_{2}^{3}","l");
  //leg->AddEntry(g2bar_3_gr,"3 #bar{g}_{2}(M=0)","l");
  //leg->AddEntry(g2bar_TMC_gr,"3 #bar{g}_{2}","l");

  //mg->Add(d2_gr,      "l");
  //mg->Add(d2_TMC_gr,      "l");
  //mg->Add(M23_gr,     "l");
  //mg->Add(M23_TMC_gr, "l");
  //mg->Add(g2bar_3_gr, "l");
  //mg->Add(g2bar_TMC_gr, "l");

  //mg->Draw("a");
  //mg->GetXaxis()->CenterTitle(true);
  //mg->GetXaxis()->SetTitle("Q^{2}");
  //mg->GetXaxis()->SetLimits(0.5,5.5);
  //mg->GetYaxis()->CenterTitle(true);
  //mg->GetYaxis()->SetTitle("");
  //mg->GetYaxis()->SetRangeUser(-0.011,0.023);
  //mg->Draw("a");
  //leg->Draw();

  //gSystem->mkdir("results/d2_from_A2_fit4",true);
  //c->SaveAs(Form("results/d2_from_A2_fit4/d2_Wmin%d_%dpars_%d_%s.pdf",Wmin,n_params,anumber,pdf_names.c_str())); 

  //// --------------
  //mg->Add(g2bar_minus_M23_gr, "l");
  //leg->AddEntry(g2bar_minus_M23_gr,"3 #bar{g}_{2} - 2M_{2}^{3}","l");
  //mg->Draw("a");
  //leg->Draw();
  //c->SaveAs(Form("results/d2_from_A2_fit4/d2_Wmin%d_%dpars_%d_%s_2.pdf",Wmin,n_params,anumber,pdf_names.c_str())); 

  //// ----------------------------------------
  ////
  //TFile * f = new TFile(Form("results/d2_from_A2_fit4_%d.root",anumber),"UPDATE");
  //f->cd();
  //mg->Write("mg");
  //d2_gr     ->Write("d2");
  //d2_TMC_gr ->Write("d2");
  //M23_gr    ->Write("M23");
  //M23_TMC_gr->Write("M23_TMC");
  //g2bar_3_gr->Write("3g2bar");
  //g2bar_TMC_gr->Write("3g2bar_TMC");
  ////ci_gr->Write("CI_M23_TMC");
  //f->Close();

}

