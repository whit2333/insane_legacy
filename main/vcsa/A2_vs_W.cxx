#include "util/get_A2_fit.cxx"

void A2_vs_W(int aNumber = 0) {

  int sf_num  = 1;
  int ssf_num = 14;
  InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  auto sfs    = fman->CreateSFs(sf_num);
  auto ssfs   = fman->CreatePolSFs(ssf_num);

  //auto sfs   = new F1F209StructureFunctions();
  auto vca = new InSANEVirtualComptonAsymmetries(sfs,ssfs);
  InSANEVirtualComptonAsymmetries * vca_fit = get_A2_fit(); 

  std::vector<int> colors = {1,2,4,6,8,7};

  double W_min = 1.00;
  double W_max = 8.0;
  int    N     = 20;
  double dW    = (W_max-W_min)/double(N);

  double Q2_min = 1.0;
  double Q2_max = 5.0;
  int    NQ2    = 2;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2-1);

  std::vector<TGraph*> grs_A2;
  std::vector<TGraph*> grs_A2_TMC;
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    grs_A2.push_back(  new TGraph() );
    grs_A2_TMC.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    for (int iW = 0; iW < N; iW++) {

      double W = W_min + double(iW)*dW;
      double x = InSANE::Kine::xBjorken_WQsq(W,Q2);

      double A2      = vca->A2p(x, Q2);
      grs_A2[iQ2]->SetPoint( iW, W, A2);

      double A2_TMC      = vca_fit->A2p(x, Q2);
      grs_A2_TMC[iQ2]->SetPoint( iW, W, A2_TMC);
    }
  }

  //---------------------------------------
  //
  auto ndbman  = NucDBManager::GetManager();
  auto A2_meas = ndbman->GetAllMeasurements("A2p");

  Double_t Q2      = 5.0;
  Double_t Q2width = 4.0;
  Double_t W       = 6.0;
  Double_t Wwidth  = 5.0;

  NucDBBinnedVariable Q2_bin         ("Qsquared", "Q^{2}", Q2 , Q2width);
  NucDBBinnedVariable W_bin          ("W"       , "W"    , W  , Wwidth );
  NucDBBinnedVariable theta_bin      ("theta"   , "theta", 4.5, 4.0    );
  NucDBBinnedVariable theta_10deg_bin("theta"   , "theta", 10 , 1.0    );

  NucDB::ApplyFilterOnList(&Q2_bin, NucDB::ToList(A2_meas));
  NucDB::ApplyFilterOnList(&W_bin , NucDB::ToList(A2_meas));

  auto sane_meas = NucDB::FindExperiment("SANE", NucDB::ToList(A2_meas));
  std::vector<NucDBBinnedVariable> selection_bins = {
    NucDBBinnedVariable("W",       "W",  0.0,  0.3),
    NucDBBinnedVariable("Qsquared","Q2", 0.0,  0.6),
    NucDBBinnedVariable("x",       "x",  0.0,  0.1)
  };
  sane_meas->MergeDataPoints(5, selection_bins, true);

  auto mg_A2_data = NucDB::CreateMultiGraph(A2_meas,"W");
  TLegend     * leg2 = new TLegend(0.8,0.2,0.975,0.975);
  NucDB::FillLegend(leg2, A2_meas,mg_A2_data);

  auto mg_kine_data = NucDB::CreateMultiKineGraph(A2_meas,"W","Qsquared");

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.8,0.6,0.975,0.975);

  mg->Add(mg_A2_data);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_A2[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetMarkerStyle(20);
    grs_A2[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetLineWidth(2);
    grs_A2[iQ2]->SetLineStyle(1+(iQ2/(colors.size())) );

    grs_A2_TMC[iQ2]->SetLineColor(colors[(iQ2)%(colors.size())]);
    grs_A2_TMC[iQ2]->SetMarkerStyle(20);
    grs_A2_TMC[iQ2]->SetMarkerColor(colors[(iQ2)%(colors.size())]);
    grs_A2_TMC[iQ2]->SetLineWidth(2);
    grs_A2_TMC[iQ2]->SetLineStyle(2+(iQ2/(colors.size())) );
    
    mg->Add(      grs_A2[iQ2],"l");
    leg->AddEntry(grs_A2[iQ2], Form("Q2=%.1f",Q2), "l");
    mg->Add(      grs_A2_TMC[iQ2],"l");
    leg->AddEntry(grs_A2_TMC[iQ2], Form("TMC Q2=%.1f",Q2), "l");

  }

  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(-0.1,0.5);
  mg->GetXaxis()->SetLimits(W_min,W_max);
  mg->GetYaxis()->SetTitle("A_{2}");
  mg->GetXaxis()->SetTitle("W");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  leg->Draw();
  leg2->Draw();

  TLatex lt;
  lt.SetTextAlign(12);
  lt.SetTextSize(0.04);
  lt.DrawLatex(1.2,0.4,Form("%s / %s",sfs->GetLabel(), ssfs->GetLabel()));

  c->SaveAs(Form("results/vcsa/A2_vs_W_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/A2_vs_W_%d_%d.pdf",sf_num,ssf_num));

  gPad->SetLogx(true);

  c->Update();

  c->SaveAs(Form("results/vcsa/A2_vs_Wlog_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/A2_vs_Wlog_%d_%d.pdf",sf_num,ssf_num));

  c = new TCanvas();
  mg_kine_data->Draw("a");
  mg_kine_data->GetXaxis()->SetLimits(W_min,W_max);
  mg_kine_data->GetYaxis()->SetTitle("Q^{2}");
  mg_kine_data->GetXaxis()->SetTitle("W");
  mg_kine_data->GetYaxis()->CenterTitle(true);
  mg_kine_data->GetXaxis()->CenterTitle(true);
  mg_kine_data->Draw("a");
  c->SaveAs(Form("results/vcsa/Q2_vs_W_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/Q2_vs_W_%d_%d.pdf",sf_num,ssf_num));

  //c = new TCanvas();
  //c->Divide(2,2);


}
