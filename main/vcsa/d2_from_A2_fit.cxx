//#include "SSFsFromPDFs.h"
//#include <tuple>
//#include "QCDNUMBase.h"
//#include "InputModel.h"
//#include "Twist3Evolution.h"
//#include "Twist3Evolution.h"
//#include "VirtualComptonScatteringAsymmetries.h"
//#include "VCSAsFromPDFs.h"
//#include "VCSAsFromSFs.h"
//#include "NucDBUtil.h"

void d2_from_A2_fit(){

  using namespace insane::physics;

  //Minimizer is Minuit2 / Migrad
  //MinFCN                    =      301.143
  //NDf                       =          188
  double Chi2 = 301.143;
  int    Ndf  = 188;
  //std::vector<double> parameters = {
  //  //0.0318802, -0.0360833 //Wmin 1200 2pars Stat2015
  //  //-0.0753599, 0.346594, -0.420844, 0.13794 // Wmin_1200 4pars CTEQ10_JAM
  //  //-0.0496557, 0.195166, -0.160137          // Wmin_1200_3pars
  //  //-0.0588276, 0.214449, -0.16091             // Wmin 1500 3pars CTEQ10_JAM
  //  //0.0299766, -0.3365, 0.828886, -0.537601  // Wmin 1500 4pars
  //  //-0.0140583, 0.0220481                    // Wmin 1750 2pars
  //  //-0.0214605, 0.0512038, -0.0248646        // Wmin 1750 3pars
  //  //0.034062, -0.317633, 0.690649, -0.418935 // Wmin_1750 4pars
  //  //-0.00361692, 0.0244204, -0.013724        // Wmin_1800 3pars Stat2015
  //  //0.000179323, 0.00884835                  // Wmin 1800 2pars Stat2015
  //  //-0.0190164, 0.0315319                    // Wmin_2000 2pars
  //  //  0.15023, -0.163802 
  //  -0.0414316, -2.54855, 28.0978, -100.233, 162.751
  //};
  int Wmin              = 1300;
  std::string pdf_names = "CTEQ10_JAM";
  int n_params          = 6;//parameters.size();

//std::vector<double> data = {2.0364e-05 , -3.576e-05 , -3.1681e-06,  1.9335e-05,
  //                             -3.576e-05,  9.9752e-05, -5.1472e-05, -1.8214e-05,
  //                            -3.1681e-06, -5.1472e-05,  0.00015097, -9.9632e-05,
  //                             1.9335e-05, -1.8214e-05, -9.9632e-05,  0.00011007};
  //TMatrixDSym cov(n_params, &data[0]);
  //std::cout << cov(0,0) << std::endl;
  //std::cout << cov(1,0) << std::endl;
  //std::cout << cov(0,1) << std::endl;

  // ------------------------------------------
  //
  //auto vcsa = new VCSAsFromPDFs<std::tuple<CTEQ10_UPDFs>, std::tuple<JAM_PPDFs,wevolve::SimpleFit_T3DFs>>();
  //std::get<wevolve::SimpleFit_T3DFs>(vcsa->fPPDFs).SetParams(parameters);

  // ------------------------------------------
  //
  //t3dfs->SetParams(parameters);
  //const auto tups =  std::make_tuple(ppdfs,t3dfs);
  //auto ssfs = new SSFsFromPDFs<Stat2015_PPDFs, wevolve::SimpleFit_T3DFs>();
  auto ssfs = new SSFsFromPDFs<JAM_PPDFs, wevolve::SimpleFit_T3DFs>();
  //std::get<wevolve::SimpleFit_T3DFs>(ssfs->fDFs).SetParams(parameters);
  //std::get<1>(ssfs->fDFs);//.SetParams(parameters);

  //auto d2_integrand = [&](double* x, double* p){
  //    std::vector<double> pars_temp(n_params,0.0);
  //    for(int ipars = 0 ; ipars < n_params; ipars++) { pars_temp[ipars] = p[ipars]; }
  //    //std::get<wevolve::SimpleFit_T3DFs>(ssfs->fDFs).SetParams(pars_temp);
  //    //t3dfs->SetParams(parameters);
  //    double xbj = x[0];
  //    double Qsq = x[1];
  //    //double xbj = InSANE::Kine::xBjorken_WQ2(W,Qsq);
  //    double g1v = ppdfs->g1p_Twist2(xbj,Qsq);
  //    double g2v = 0.0;//insane::physics::g2(std::make_tuple(ppdfs,t3dfs));
  //    return 2.0*g1v+3.0*g2v;
  //};

  //TF1 * f_d2_integrand = new TF1("d2_int",d2_integrand,0.1,0.9,0);
  ////f_d2_integrand->Draw();
  //
  TGraph * d2_gr = new TGraph();
  TGraph * M23_gr = new TGraph();
  TGraph * M23_TMC_gr = new TGraph();
  TGraph * g2bar_3_gr = new TGraph();
  
  for(int i = 0; i<20; i++) {
    double Qsq         = 0.5 + double(i)*0.20;
    double d2_val      = ssfs->d2p_tilde(Qsq,0.01,0.999);
    double M23_val     = 0.0;//ssfs->M2n_p(3,Qsq,0.1,0.8)*2.0;
    double M23_TMC_val = ssfs->M2n_TMC_p(3,Qsq,0.01,0.999)*2.0;
    double g2bar_3     = 0.0;//std::get<wevolve::SimpleFit_T3DFs>(ssfs->fDFs).d2p(Qsq,0.1,0.8);

    d2_gr->SetPoint(i,Qsq,d2_val);
    M23_gr->SetPoint(i,Qsq,M23_val);
    M23_TMC_gr->SetPoint(i,Qsq,M23_TMC_val);
    g2bar_3_gr->SetPoint(i,Qsq,g2bar_3);
  } 


  //TGraph * gr = new TGraph(f_d2_integrand);

  TCanvas * c  = new TCanvas();

  TMultiGraph * mg = new TMultiGraph();

  d2_gr ->SetLineColor(1);
  M23_gr->SetLineColor(2);
  M23_TMC_gr->SetLineColor(2);
  M23_TMC_gr->SetLineStyle(2);
  g2bar_3_gr->SetLineColor(4);
  g2bar_3_gr->SetLineStyle(2);

  d2_gr ->SetLineWidth(2);
  M23_gr->SetLineWidth(2);
  M23_TMC_gr->SetLineWidth(2);
  g2bar_3_gr->SetLineWidth(2);

  mg->Add(d2_gr,"l");
  mg->Add(M23_gr,"l");
  mg->Add(M23_TMC_gr,"l");
  mg->Add(g2bar_3_gr,"l");

  mg->Draw("a");


  gSystem->mkdir("results/d2_from_A2_fit",true);

  c->SaveAs(Form("results/d2_from_A2_fit/d2_Wmin%d_%dpars_%s.pdf",Wmin,n_params,pdf_names.c_str())); 

}
