void A2_with_twist3(int aNumber = 0) {

  int sf_num  = 1;
  int ssf_num = 6;
  InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  auto sfs    = fman->CreateSFs(sf_num);
  auto ssfs   = fman->CreatePolSFs(ssf_num);
  InSANEPolarizedPartonDistributionFunctions * ppdfs = nullptr;
  if( dynamic_cast<InSANEPolarizedStructureFunctionsFromPDFs*>(ssfs) ){
     ppdfs = dynamic_cast<InSANEPolarizedStructureFunctionsFromPDFs*>(ssfs)->GetPolarizedPDFs();
  }
  if( !ppdfs ) return -123;

  //auto ppdfs = new JAM15PolarizedPDFs();
  //auto ssfs  = new InSANEPolarizedStructureFunctionsFromPDFs(ppdfs);
  //auto ppdfs  = new Stat2015PolarizedPDFs();

  ////auto pdfs  = new Stat2015UnpolarizedPDFs();
  //auto pdfs  = new CTEQ10UnpolarizedPDFs();

  //auto sfs   = new InSANEStructureFunctionsFromPDFs(pdfs);
  ////auto sfs   = new F1F209StructureFunctions();
  WEvolvedg2 * g2model = new WEvolvedg2();
  g2model->SetQCDNUMFunc1(g2model);
  g2model->SetFunc(4);
  g2model->SetType(4);
  g2model->SetPolarizedPDFs(ppdfs);
  g2model->MakeGrid();
  g2model->EvolveInit(1.0);
  g2model->Evolve();

  auto vca = new InSANEVirtualComptonAsymmetries();
  vca->SetUnpolarizedSFs(sfs );
  vca->SetPolarizedSFs(  ssfs);

  auto vca2 = new InSANEVirtualComptonAsymmetries();
  vca2->SetUnpolarizedSFs(sfs );
  vca2->SetPolarizedSFs(  g2model);

  std::vector<int> colors = {1,2,4,6,8,9};

  double x_min = 0.05;
  double x_max = 0.95;
  int    N     = 10;
  double dx    = (x_max-x_min)/double(N);

  double Q2_min = 1.0;
  double Q2_max = 6.0;
  int    NQ2    = 2;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2-1);

  std::vector<TGraph*> grs_A1;
  std::vector<TGraph*> grs_A1_TMC;
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    grs_A1.push_back(  new TGraph() );
    grs_A1_TMC.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    for (int ix = 0; ix < N; ix++) {

      double x = x_min + double(ix)*dx;

      double A1      = vca->A2p(x, Q2);
      grs_A1[iQ2]->SetPoint( ix, x, A1);

      double A1_TMC      = vca2->A2p(x, Q2);
      grs_A1_TMC[iQ2]->SetPoint( ix, x, A1_TMC);
    }
  }
  
  //---------------------------------------
  //
  auto ndbman  = NucDBManager::GetManager();
  auto A2_meas = ndbman->GetAllMeasurements("A2p");

  Double_t Q2      = 2.0;
  Double_t Q2width = 0.4;
  Double_t W       = 6.0;
  Double_t Wwidth  = 4.0;

  NucDBBinnedVariable Q2_bin         ("Qsquared", "Q^{2}", Q2 , Q2width);
  NucDBBinnedVariable W_bin          ("W"       , "W"    , W  , Wwidth );
  NucDBBinnedVariable theta_bin      ("theta"   , "theta", 4.5, 4.0    );
  NucDBBinnedVariable theta_10deg_bin("theta"   , "theta", 10 , 1.0    );

  NucDB::ApplyFilterOnList(&Q2_bin, NucDB::ToList(A2_meas));
  NucDB::ApplyFilterOnList(&W_bin , NucDB::ToList(A2_meas));

  auto sane_meas = NucDB::FindExperiment("SANE", NucDB::ToList(A2_meas));
  std::vector<NucDBBinnedVariable> selection_bins = {
    NucDBBinnedVariable("W",       "W",  0.0,  0.05),
    NucDBBinnedVariable("Qsquared","Q2", 0.0,  0.4),
    NucDBBinnedVariable("x",       "x", 0.0,  0.1)
  };
  sane_meas->MergeDataPoints(4, selection_bins, true);

  auto mg_A2_data = NucDB::CreateMultiGraph(A2_meas,"x");
  TLegend     * leg2 = new TLegend(0.8,0.2,0.975,0.975);
  NucDB::FillLegend(leg2, A2_meas,mg_A2_data);


  //for(auto m: A2_meas ) {
  //  auto gr = m->Build
  //}


  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.8,0.6,0.975,0.975);
  gSystem->mkdir("results/vcsa_with_twist3", true);

  mg->Add(mg_A2_data);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_A1[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A1[iQ2]->SetMarkerStyle(20);
    grs_A1[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A1[iQ2]->SetLineWidth(2);
    grs_A1[iQ2]->SetLineStyle(1+(iQ2/(colors.size())) );
    mg->Add(      grs_A1[iQ2],"l");
    leg->AddEntry(grs_A1[iQ2], Form("Q2=%.1f",Q2), "l");

    grs_A1_TMC[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A1_TMC[iQ2]->SetMarkerStyle(20);
    grs_A1_TMC[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A1_TMC[iQ2]->SetLineWidth(2);
    grs_A1_TMC[iQ2]->SetLineStyle(2+(iQ2/(colors.size())) );
    mg->Add(      grs_A1_TMC[iQ2],"l");
    leg->AddEntry(grs_A1_TMC[iQ2], Form("w/ twist3 Q2=%.1f",Q2), "l");

  }
  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(-1.0,1.0);
  mg->GetXaxis()->SetLimits(0.001,1.0);
  mg->GetYaxis()->SetTitle("A_{2}");
  mg->GetXaxis()->SetTitle("x");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  leg->Draw();
  leg2->Draw();

  TLatex lt;
  lt.SetTextAlign(12);
  lt.SetTextSize(0.04);
  lt.DrawLatex(0.1,0.9,Form("%s / %s",sfs->GetLabel(), ssfs->GetLabel()));

  c->SaveAs(Form("results/vcsa_with_twist3/A2_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa_with_twist3/A2_%d_%d.pdf",sf_num,ssf_num));

}
