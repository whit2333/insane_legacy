#include "util/get_A2_fit.cxx"
#include "QCDNUMBase.h"
#include "InputModel.h"
#include "Twist3Evolution.h"
#include "VCSAsFromPDFs.h"
#include "Fit_T3DFs.h"

//#include "VirtualComptonScatteringAsymmetries.h"
//#include "VCSAsFromPDFs.h"
#include "VCSAsFromSFs.h"

void A2_vs_W_2(int aNumber = 0) {
  using namespace insane::physics;

  int sf_num  = 3;
  int ssf_num = 3;
  //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 
  //auto sfs    = fman->CreateSFs(sf_num);
  //auto ssfs   = fman->CreatePolSFs(ssf_num);
  //InSANEPolarizedStructureFunctions * psfs = fman->CreatePolSFs(ssf_num);
  //InSANEStructureFunctions          * sfs = fman->CreateSFs(sf_num);

   // ---------------------------------------
   // Soffer bound
  //auto vcsa = new VCSAsFromPDFs<std::tuple<CTEQ10_UPDFs>, std::tuple<JAM_PPDFs,JAM_T3DFs>>();
  //auto vcsa = new VCSAsFromPDFs<Stat2015_UPDFs, Stat2015_PPDFs>();
  auto vcsa0 = new MAID_VCSAs();//new VCSAsFromSFs<NMC95_SFs, SSFsFromPDFs<Stat2015_PPDFs>>();
  //auto vcsa  = new VCSAsFromSFs<F1F209_SFs, CompositeSSFs>();
  auto vcsa = new VCSAsFromPDFs<std::tuple<CTEQ10_UPDFs>, std::tuple<JAM_PPDFs,wevolve::Fit_T3DFs>>();
  std::get<wevolve::Fit_T3DFs>(vcsa->fPPDFs).SetParams({0.0105388 ,-0.363684 , -2.74007 , -4.39931 , -2.07681});
  //auto vcsa = new VCSAsFromSFs<CompositeSFs, SSFsFromPDFs<Stat2015_PPDFs>>();


  //auto ssf = new CompositeSSFs();
  //InSANEVirtualComptonAsymmetries   * vcsa = new InSANEVirtualComptonAsymmetries(); 

  //auto sfs   = new F1F209StructureFunctions();
  //auto vca = new InSANEVirtualComptonAsymmetries(sfs,ssfs);
  InSANEVirtualComptonAsymmetries * vca_fit = get_A2_fit(); 

  std::vector<int> colors = {2,4,6,8,9,3,1,kCyan+2};

  double W_min = 1.00;
  double W_min_2 = 1.250;
  double W_max = 10.0;
  int    N     = 200;
  double dW    = (W_max-W_min)/double(N);
  double dW2   = (W_max-W_min_2)/double(N);

  double Q2_min = 1.0;
  double Q2_max = 8.0;
  int    NQ2    = 4;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2-1);

  std::vector<TGraph*> grs_A2;
  std::vector<TGraph*> grs_A2_TMC;
  std::vector<TGraph*> grs_A2_bound_plus;
  std::vector<TGraph*> grs_A2_bound_minus;
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    grs_A2.push_back(  new TGraph() );
    grs_A2_TMC.push_back(  new TGraph() );
    grs_A2_bound_plus .push_back(  new TGraph() );
    grs_A2_bound_minus.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    for (int iW = 0; iW < N; iW++) {

      double W = W_min_2 + double(iW)*dW2;
      double x = InSANE::Kine::xBjorken_WQsq(W,Q2);

      //double A2      = vcsa->A2p(x,Q2);//vca->A2p(x, Q2);
      double A2      = vcsa->A2(Nuclei::p,x,Q2);//vca->A2p(x, Q2);
      grs_A2[iQ2]->SetPoint( iW, W, A2);

      //double A2_TMC      = vca_fit->A2p(x, Q2);
      double A2_TMC     = vcsa0->A2(Nuclei::p,x,Q2);//vca->A2p(x, Q2);
      grs_A2_TMC[iQ2]->SetPoint( iW, W, A2_TMC);

      double bound1  = vcsa->A2_PositivityBound(Nuclei::p,x,Q2);
      grs_A2_bound_plus[iQ2]->SetPoint(  iW, W, bound1);
      grs_A2_bound_minus[iQ2]->SetPoint( iW, W, -1.0*bound1);

    }
  }

  //---------------------------------------
  //
  auto ndbman  = NucDBManager::GetManager();
  auto A2_meas = ndbman->GetAllMeasurements("A2p");

  auto A1_meas   = ndbman->GetAllMeasurements("A1p");
  auto merged_A1 = NucDB::Merge(A1_meas);
  merged_A1->TransformDataPoints([&](NucDBDataPoint* point){
      double x  = point->GetBinVariable("x")->GetMean();
      double Q2 = point->GetBinVariable("Qsquared")->GetMean();
      double R  = InSANE::Kine::R1998(x,Q2);
      double A1 = point->GetValue();
      double eA1 = point->GetError();
      double bound = TMath::Sqrt(TMath::Abs(R*(1.0+A1)/2.0));
      double ebound = TMath::Sqrt(TMath::Abs(R*(1.0+eA1)/2.0));
      point->SetValue(bound);
      point->SetStatError(ebound/4.0);
      });

  NucDB::SetColors(A2_meas,colors);
  NucDB::SetLineColor(A2_meas,1);

  Double_t Q2      = 6.0;
  Double_t Q2width = 5.0;
  Double_t W       = 6.0;
  Double_t Wwidth  = 5.0;

  NucDBBinnedVariable Q2_bin         ("Qsquared", "Q^{2}", Q2 , Q2width);
  NucDBBinnedVariable W_bin          ("W"       , "W"    , W  , Wwidth );
  NucDBBinnedVariable theta_bin      ("theta"   , "theta", 4.5, 4.0    );
  NucDBBinnedVariable theta_10deg_bin("theta"   , "theta", 10 , 1.0    );

  NucDB::ApplyFilterOnList(&Q2_bin, NucDB::ToList(A2_meas));
  NucDB::ApplyFilterOnList(&W_bin , NucDB::ToList(A2_meas));

  auto sane_meas = NucDB::FindExperiment("SANE", NucDB::ToList(A2_meas));
  std::vector<NucDBBinnedVariable> selection_bins = {
    NucDBBinnedVariable("W",       "W",  0.0,  0.08 ),
    NucDBBinnedVariable("Qsquared","Q2", 0.0,  0.2 ),
    NucDBBinnedVariable("x",       "x",  0.0,  0.05)
  };
  sane_meas->MergeDataPoints(7, selection_bins, true);

  auto hms_meas = NucDB::FindExperiment("SANE_HMS1", NucDB::ToList(A2_meas));
  std::vector<NucDBBinnedVariable> selection_bins2 = {
    NucDBBinnedVariable("W",       "W",  0.0,  0.025),
    NucDBBinnedVariable("Qsquared","Q2", 0.0,  0.1  ),
    NucDBBinnedVariable("x",       "x",  0.0,  0.05 )
  };
  //hms_meas->MergeDataPoints(3, selection_bins2, true);
  hms_meas->SetMarkerStyle(kFullSquare);

  auto mg_A2_data = NucDB::CreateMultiGraph(A2_meas,"W");
  TLegend     * leg2 = new TLegend(0.85,0.6,0.999,0.999);
  NucDB::FillLegend(leg2, A2_meas,mg_A2_data);

  auto mg_kine_data = NucDB::CreateMultiKineGraph(A2_meas,"W","Qsquared");

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.8,0.6,0.975,0.975);

  mg->Add(mg_A2_data);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_A2[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetMarkerStyle(20);
    grs_A2[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetLineWidth(2);
    grs_A2[iQ2]->SetLineStyle(2);

    grs_A2_TMC[iQ2]->SetLineColor(colors[(iQ2)%(colors.size())]);
    grs_A2_TMC[iQ2]->SetMarkerStyle(20);
    grs_A2_TMC[iQ2]->SetMarkerColor(colors[(iQ2)%(colors.size())]);
    grs_A2_TMC[iQ2]->SetLineWidth(2);
    grs_A2_TMC[iQ2]->SetLineStyle(1);
    
    mg->Add(      grs_A2[iQ2],"l");
    mg->Add(      grs_A2_TMC[iQ2],"l");
    leg->AddEntry(grs_A2[iQ2], Form("Q2=%.1f",Q2), "l");
    leg->AddEntry(grs_A2_TMC[iQ2], Form("TMC Q2=%.1f",Q2), "l");
    if(iQ2 == 0) {
    leg2->AddEntry(grs_A2[iQ2],     "Composite", "l");
    leg2->AddEntry(grs_A2_TMC[iQ2], "MAID"     , "l");
    }

  }

  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(-1.0,1.0);
  mg->GetXaxis()->SetLimits(W_min,W_max);
  mg->GetYaxis()->SetTitle("A_{2}");
  mg->GetXaxis()->SetTitle("W");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  //leg->Draw();
  leg2->Draw();

  TLatex lt;
  lt.SetTextAlign(12);
  lt.SetTextSize(0.04);
  //lt.DrawLatex(1.2,0.4,Form("%s / %s",sfs->GetLabel(), ssfs->GetLabel()));

  c->SaveAs(Form("results/vcsa/A2_vs_W_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/A2_vs_W_%d_%d.pdf",sf_num,ssf_num));

  gPad->SetLogx(true);

  c->Update();

  c->SaveAs(Form("results/vcsa/A2_vs_Wlog_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/A2_vs_Wlog_%d_%d.pdf",sf_num,ssf_num));

  c = new TCanvas();
  mg_kine_data->Draw("a");
  mg_kine_data->GetXaxis()->SetLimits(W_min,W_max);
  mg_kine_data->GetYaxis()->SetTitle("Q^{2}");
  mg_kine_data->GetXaxis()->SetTitle("W");
  mg_kine_data->GetYaxis()->CenterTitle(true);
  mg_kine_data->GetXaxis()->CenterTitle(true);
  mg_kine_data->Draw("a");
  c->SaveAs(Form("results/vcsa/Q2_vs_W_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/Q2_vs_W_%d_%d.pdf",sf_num,ssf_num));

  //---------------------------------------
  //
  c = new TCanvas("Q2dep","Q2dep",900,1600);
  gStyle->SetPadBorderMode(0);
  gStyle->SetFrameBorderMode(0);
  c->Divide(1,NQ2, 1.0e-5, 1.0e-5);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    c->cd(iQ2+1);
    if( (iQ2!=NQ2-1) ) gPad->SetBottomMargin(1.0e-3);
    if( (iQ2 > 0)  ) gPad->SetTopMargin(1.0e-3);

    double Q2  = Q2_min + double(iQ2)*dQ2;

    auto soff1 = grs_A2_bound_plus[iQ2];
    auto soff2 = grs_A2_bound_minus[iQ2];

    soff1->SetLineWidth(2);
    soff2->SetLineWidth(2);
    soff1->SetLineColor(1);
    soff2->SetLineColor(1);
    soff1->SetLineStyle(2);
    soff2->SetLineStyle(2);

    NucDBBinnedVariable aQ2_bin  ("Qsquared", "Q^{2}", Q2 , dQ2/2.0);
    auto Q2_meas_points = NucDB::FilterMeasurements(A2_meas, &aQ2_bin);
    auto mg_temp = NucDB::CreateMultiGraph(Q2_meas_points,"W");

    auto temp_A1 = merged_A1->NewMeasurementWithFilter(&aQ2_bin);
    temp_A1->MergeDataPoints(4, "W", true);
    auto gr_data_bound = temp_A1->BuildGraph("W");
    TF2  f1_invert("f1_invert","-1.0*y",0,10,-10,10);
    auto gr_data_bound_low = new TGraphErrors(*gr_data_bound);
    gr_data_bound->Sort();
    gr_data_bound_low->Apply(&f1_invert);
    gr_data_bound->SetFillColor(4);
    gr_data_bound->SetFillStyle(3010);
    gr_data_bound_low->Sort();
    gr_data_bound_low->SetFillColor(4);
    gr_data_bound_low->SetFillStyle(3010);
    mg_temp->Add(gr_data_bound,"e3");
    mg_temp->Add(gr_data_bound_low,"e3");

    mg_temp->Add(grs_A2[iQ2],"l");
    mg_temp->Add(grs_A2_TMC[iQ2],"l");
    mg_temp->Add(soff1, "l");
    mg_temp->Add(soff2,"l");
    mg_temp->Draw("a");
    mg_temp->GetYaxis()->SetTitle(Form("%.2f < Q^{2} < %.2f",Q2-dQ2/2.0, Q2+dQ2/2.0));
    mg_temp->GetYaxis()->SetRangeUser(-1.0,1.0);
    mg_temp->GetXaxis()->SetLimits(W_min,W_max);
    //mg_temp->GetYaxis()->SetTitle("A_{2}");
    mg_temp->GetXaxis()->SetTitle("W");
    mg_temp->GetYaxis()->CenterTitle(true);
    mg_temp->GetXaxis()->CenterTitle(true);
    mg_temp->GetXaxis()->SetMoreLogLabels(true);
    gPad->SetLogx(true);
    mg_temp->Draw("a");

  }
  c->cd(0);
  leg2->Draw();
  c->SaveAs(Form("results/vcsa/A2_vs_W_Q2_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/A2_vs_W_Q2_%d_%d.pdf",sf_num,ssf_num));

}
