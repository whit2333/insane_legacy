#include "InSANEMathFunc.h"
#include "NucDBUtil.h"

void d2_integrand_from_A2_fit11(int anumber = 1145) {

  //using namespace insane::physics;

  int Wmin        = 1199;
  int n_params    = 9;
  int n_t3_params = 5;

  std::string pdf_names =  "Stat1015";
  typedef  insane::physics::Stat2015_PPDFs T2PDF  ;
  //std::string pdf_names =  "JAM";
  //typedef  insane::physics::JAM_PPDFs T2PDF  ;
  
  //typedef  wevolve::SimpleFit_T3DFs3   T3FIT;  // 700 series
  //typedef  wevolve::SimpleFit_T3DFs   T3FIT;   // 800 series
  typedef  wevolve::SimpleFit_T3DFs4   T3FIT;    // 900 series
  pdf_names += "_F4";

  double Qsq   = 2.5;
  double x_min = 0.005;
  double x_max = InSANE::Kine::xBjorken_WQsq(0.938+0.139,Qsq);
  std::cout << " x_max = " << x_max << std::endl;

  // -------------------------------------------------------
  
  std::string dir_name  = Form("/home/whit/work/SANE/results/A2_fit/Wmin_%d_%dpars", Wmin, n_params);
  TFile*      f_result  = new TFile(Form("%s/A2_fit_%d.root",dir_name.c_str(),anumber),"READ");

  TMatrixDSym* cov  = nullptr;
  TMatrixDSym* corr = nullptr;
  std::vector<double>* result_pars = nullptr;
  std::map<std::string,double>* result_values = nullptr;

  f_result->cd();
  f_result->GetObject("cov_matrix" , cov         );
  f_result->GetObject("corr_matrix", corr        );
  f_result->GetObject("parameters" , result_pars );
  f_result->GetObject("result_values" , result_values );

  double Chi2 = (*result_values)["MinFcnValue"];
  int    Ndf  = (*result_values)["Ndf"];  ;

  std::vector<double> parameters;
  for(int ip = 0; ip < n_t3_params; ++ip){
    parameters.push_back((*result_pars)[ip]);
  }
  
  auto ssfs = new insane::physics::SSFsFromPDFs<T2PDF, T3FIT>();
  std::get<T3FIT>(ssfs->fDFs).SetParams(parameters);
  //auto sfs  = new SFsFromPDFs<Stat2015_UPDFs>();
  //std::get<1>(ssfs->fDFs);//.SetParams(parameters);

  TGraph * I_gr               = new TGraph();
  TGraph * I_TMC_gr           = new TGraph();
  TGraph * M23_gr             = new TGraph();
  TGraph * M23_TMC_gr         = new TGraph();
  TGraph * g2bar_3_gr         = new TGraph();
  TGraph * g2bar_TMC_gr       = new TGraph();


  std::function<double(const double*, const double*)> func_I = [&](const double * x, const double * p){
      std::vector<double> pars_temp(n_t3_params,0.0);
      for(int ipars = 0 ; ipars < n_t3_params; ipars++) { pars_temp[ipars] = p[ipars]; }
      std::get<T3FIT>(ssfs->fDFs).SetParams(pars_temp);
      double xbj = x[0];
      double Q2__ = x[1];
      //double xbj = InSANE::Kine::xBjorken_WQ2(W,Qsq);
      double res = xbj*xbj*(2.0*ssfs->g1p( xbj, Q2__) + 3.0*ssfs->g2p( xbj,Q2__));
      return res;
      };

  std::function<double(const double*, const double*)> func_M23 = [&](const double * x, const double * p){
      std::vector<double> pars_temp(n_t3_params,0.0);
      for(int ipars = 0 ; ipars < n_t3_params; ipars++) { pars_temp[ipars] = p[ipars]; }
      std::get<T3FIT>(ssfs->fDFs).SetParams(pars_temp);
      double xbj   = x[0];
      double Q2__  = x[1];
      //double xbj = InSANE::Kine::xBjorken_WQ2(W,Qsq);
      double res = ssfs->M2nIntegrand_TMC_p(3, xbj, Q2__)*2.0;
      return res;
      };

  TH1  * hd2_integrand = new TH1F("d2_integrand","d2 integrand", 30,x_min,x_max);
  std::vector<double> vars = {0.0, Qsq};
  //TH1  * ci_hist = NucDB::GetConfidenceIntervals(*cov, Chi2, Ndf, 0.95, func_M23, 2, parameters, hd2_integrand, 0, vars);

  std::get<T3FIT>(ssfs->fDFs).SetParams(parameters);
  
  int    Npoints  = 30;
  double dx       = (x_max-x_min)/double(Npoints-1);

  for(int i = 0; i<Npoints; i++) {

    std::cout << " i : " << i << std::endl;
    double x           = x_min + double(i)*dx;

    double I_val       = x*x*(2.0*ssfs->g1p( x, Qsq) + 3.0*ssfs->g2p( x, Qsq));
    double I_TMC_val   = x*x*(2.0*ssfs->g1p_TMC( x, Qsq) + 3.0*ssfs->g2p_TMC( x, Qsq));
    double M23_val     = ssfs->M2nIntegrand_p(    3, x, Qsq)*2.0;
    double M23_TMC_val = ssfs->M2nIntegrand_TMC_p(3, x, Qsq)*2.0;
    double g2bar_3     = 3.0*x*x*std::get<T3FIT>(ssfs->fDFs).g2p_Twist3(x, Qsq);
    double g2bar_3_TMC = 3.0*x*x*std::get<T3FIT>(ssfs->fDFs).g2p_Twist3_TMC(x, Qsq);

    std::cout << "I_val       " <<  I_val       << std::endl;
    std::cout << "I_TMC_val   " <<  I_TMC_val   << std::endl;
    std::cout << "M23_val     " <<  M23_val     << std::endl;
    std::cout << "M23_TMC_val " <<  M23_TMC_val << std::endl;
    std::cout << "g2bar_3     " <<  g2bar_3     << std::endl;
    std::cout << "g2bar_3_TMC " <<  g2bar_3_TMC << std::endl;


    I_gr      ->SetPoint(i, x, I_val);
    I_TMC_gr  ->SetPoint(i, x, I_TMC_val);
    M23_gr    ->SetPoint(i, x, M23_val);
    M23_TMC_gr->SetPoint(i, x, M23_TMC_val);
    g2bar_3_gr->SetPoint(i, x, g2bar_3);
    g2bar_TMC_gr->SetPoint(i, x, g2bar_3_TMC);

  } 

  // ----------------------------------------
  TCanvas*     c   = new TCanvas();
  TMultiGraph* mg  = new TMultiGraph();
  TLegend*     leg = new TLegend(0.17,0.60,0.44,0.87);
  leg->SetMargin(0.25);
  leg->SetHeader(Form("Q^{2} = %.2f ",Qsq),"C");
  leg->SetEntrySeparation(0.35);

  I_gr      ->SetLineColor(1);
  I_TMC_gr  ->SetLineColor(1);
  M23_gr    ->SetLineColor(2);
  M23_TMC_gr->SetLineColor(2);
  I_TMC_gr  ->SetLineStyle(2);
  M23_TMC_gr->SetLineStyle(2);
  g2bar_3_gr->SetLineColor(4);
  g2bar_3_gr->SetLineStyle(1);
  g2bar_TMC_gr->SetLineColor(4);
  g2bar_TMC_gr->SetLineStyle(2);

  I_gr      ->SetLineWidth(2);
  I_TMC_gr  ->SetLineWidth(2);
  M23_gr    ->SetLineWidth(2);
  M23_TMC_gr->SetLineWidth(2);
  g2bar_3_gr->SetLineWidth(2);
  g2bar_TMC_gr->SetLineWidth(2);

  //auto         * ci_gr = new TGraphErrors(ci_hist);
  //ci_gr->SetFillColorAlpha( kRed, 0.35 );
  //ci_gr->SetLineColor( kBlue );
  //ci_gr->SetLineWidth( 3 );
  //mg->Add(ci_gr,"le3");

  leg->AddEntry(I_gr,"I(M=0)","l");
  leg->AddEntry(I_TMC_gr,"I","l");
  leg->AddEntry(M23_gr,"2 M_{2}^{3}(M=0)","l");
  leg->AddEntry(M23_TMC_gr,"2 M_{2}^{3}","l");
  leg->AddEntry(g2bar_3_gr,"3 x^{2} #bar{g}_{2}(M=0) ","l");
  leg->AddEntry(g2bar_TMC_gr,"3 x^{2} #bar{g}_{2} ","l");

  mg->Add(I_gr,       "l");
  mg->Add(I_TMC_gr,       "l");
  mg->Add(M23_gr,     "l");
  mg->Add(M23_TMC_gr, "l");
  mg->Add(g2bar_3_gr, "l");
  mg->Add(g2bar_TMC_gr, "l");

  mg->Draw("a");
  mg->GetXaxis()->CenterTitle(true);
  mg->GetXaxis()->SetTitle("x");
  mg->GetXaxis()->SetLimits(0.0,1.0);
  mg->GetYaxis()->CenterTitle(true);
  mg->GetYaxis()->SetTitle("");
  mg->GetYaxis()->SetRangeUser(-0.09,0.09);
  mg->Draw("a");
  leg->Draw();

  gSystem->mkdir("results/d2_integrand_from_A2_fit4",true);
  c->SaveAs(Form("results/d2_integrand_from_A2_fit4/d2_Wmin%d_%dpars_%d_%s.pdf",Wmin,n_params,anumber,pdf_names.c_str())); 

  // --------------
  //mg->Add(g2bar_minus_M23_gr, "l");
  //leg->AddEntry(g2bar_minus_M23_gr,"3 #bar{g}_{2} - 2M_{2}^{3}","l");
  //mg->Draw("a");
  //leg->Draw();
  //c->SaveAs(Form("results/d2_integrand_from_A2_fit/d2_Wmin%d_%dpars_%s_2.pdf",Wmin,n_params,pdf_names.c_str())); 

  // ----------------------------------------
  //
  TFile * f = new TFile(Form("results/d2_integrand_from_A2_fit4_%d.root",anumber),"UPDATE");
  f->cd();
  mg->Write("mg");
  I_gr      ->Write("I");
  M23_gr    ->Write("M23");
  M23_TMC_gr->Write("M23_TMC");
  //g2bar_3_gr->Write("3g2bar");
  f->Close();

}

