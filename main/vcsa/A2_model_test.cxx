//#include "VirtualComptonScatteringAsymmetries.h"
//#include "VCSAsFromPDFs.h"
#include "QCDNUMBase.h"
#include "InputModel.h"
#include "Twist3Evolution.h"
#include "VCSAsFromPDFs.h"
#include "Fit_T3DFs.h"

void A2_model_test() {
  using namespace insane::physics;
//0.0105388
//-0.363684
// -2.74007
// -4.39931
// -2.07681

  auto vca = new VCSAsFromPDFs<std::tuple<CTEQ10_UPDFs>, std::tuple<JAM_PPDFs,wevolve::Fit_T3DFs>>();
  std::get<wevolve::Fit_T3DFs>(vca->fPPDFs).SetParams({0.0105388 ,-0.363684 , -2.74007 , -4.39931 , -2.07681});

  std::vector<int> colors = {1,2,4,6,8,9};

  double x_min = 0.05;
  double x_max = 0.95;
  int    N     = 50;
  double dx    = (x_max-x_min)/double(N);

  double Q2_min = 1.0;
  double Q2_max = 8.0;
  int    NQ2    = 2;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2);

  std::vector<TGraph*> grs_A2;
  std::vector<TGraph*> grs_A2_TMC;
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    grs_A2.push_back(  new TGraph() );
    grs_A2_TMC.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    for (int ix = 0; ix < N; ix++) {

      double x = x_min + double(ix)*dx;

      //double A2      = vca->A2p(x, Q2);// insane::physics::A2_SFs(sf1,sf2,insane::physics::Nuclei::p,x,Q2);
      double A2      = vca->A2(Nuclei::p,x, Q2);// insane::physics::A2_SFs(sf1,sf2,insane::physics::Nuclei::p,x,Q2);
      grs_A2[iQ2]->SetPoint( ix, x, A2);

      double A2_TMC      = vca->A2p(x, Q2);
      //double A2_TMC      = insane::physics::A2(tup1,tup2,insane::physics::Nuclei::p,x,Q2);
      grs_A2_TMC[iQ2]->SetPoint( ix, x, A2_TMC);
    }
  }

  auto ndbman  = NucDBManager::GetManager();
  auto A2_meas = ndbman->GetAllMeasurements("A2p");

  auto mg_A2_data = NucDB::CreateMultiGraph(A2_meas,"x");
  TLegend     * leg2 = new TLegend(0.85,0.6,0.999,0.999);
  NucDB::FillLegend(leg2, A2_meas,mg_A2_data);

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.8,0.6,0.975,0.975);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_A2[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetMarkerStyle(20);
    grs_A2[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetLineWidth(2);
    grs_A2[iQ2]->SetLineStyle(1+(iQ2/(colors.size())) );
    mg->Add(      grs_A2[iQ2],"l");
    leg->AddEntry(grs_A2[iQ2], Form("Q2=%.1f",Q2), "l");

    grs_A2_TMC[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A2_TMC[iQ2]->SetMarkerStyle(20);
    grs_A2_TMC[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A2_TMC[iQ2]->SetLineWidth(2);
    grs_A2_TMC[iQ2]->SetLineStyle(2+(iQ2/(colors.size())) );
    mg->Add(      grs_A2_TMC[iQ2],"l");
    leg->AddEntry(grs_A2_TMC[iQ2], Form("TMC Q2=%.1f",Q2), "l");

  }
  mg->Add(mg_A2_data);
  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(-1.0,1.4);
  mg->GetXaxis()->SetLimits(0.001,1.0);
  mg->GetYaxis()->SetTitle("A_{1}");
  mg->GetXaxis()->SetTitle("x");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  leg->Draw();

  TLatex lt;
  lt.SetTextAlign(12);
  lt.SetTextSize(0.04);
  //lt.DrawLatex(0.1,1.0,Form("%s / %s",sfs->GetLabel(), ssfs->GetLabel()));

  c->SaveAs(Form("results/vcsa/A2_model_test_%d_%d.png",0,0));
  c->SaveAs(Form("results/vcsa/A2_model_test_%d_%d.pdf",0,0));
}
