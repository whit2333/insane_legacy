void A2(int aNumber = 0) {

  int sf_num  = 1;
  int ssf_num = 14;
  InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  auto sfs    = fman->CreateSFs(sf_num);
  auto ssfs   = fman->CreatePolSFs(ssf_num);

  //auto sfs   = new F1F209StructureFunctions();
  auto vca = new InSANEVirtualComptonAsymmetries(sfs,ssfs);

  std::vector<int> colors = {1,2,4,6,8,9};

  double x_min = 0.05;
  double x_max = 0.95;
  int    N     = 50;
  double dx    = (x_max-x_min)/double(N);

  double Q2_min = 1.0;
  double Q2_max = 3.0;
  int    NQ2    = 5;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2-1);

  std::vector<TGraph*> grs_A2;
  std::vector<TGraph*> grs_A2_TMC;
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    grs_A2.push_back(  new TGraph() );
    grs_A2_TMC.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    for (int ix = 0; ix < N; ix++) {

      double x = x_min + double(ix)*dx;

      double A2      = vca->A2p(x, Q2);
      grs_A2[iQ2]->SetPoint( ix, x, A2);

      double A2_TMC      = vca->A2p_TMC(x, Q2);
      grs_A2_TMC[iQ2]->SetPoint( ix, x, A2_TMC);
    }
  }

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.8,0.6,0.975,0.975);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_A2[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetMarkerStyle(20);
    grs_A2[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A2[iQ2]->SetLineWidth(2);
    grs_A2[iQ2]->SetLineStyle(1+(iQ2/(colors.size())) );

    grs_A2_TMC[iQ2]->SetLineColor(colors[(iQ2)%(colors.size())]);
    grs_A2_TMC[iQ2]->SetMarkerStyle(20);
    grs_A2_TMC[iQ2]->SetMarkerColor(colors[(iQ2)%(colors.size())]);
    grs_A2_TMC[iQ2]->SetLineWidth(2);
    grs_A2_TMC[iQ2]->SetLineStyle(2+(iQ2/(colors.size())) );
    
    mg->Add(      grs_A2[iQ2],"l");
    leg->AddEntry(grs_A2[iQ2], Form("Q2=%.1f",Q2), "l");
    mg->Add(      grs_A2_TMC[iQ2],"l");
    leg->AddEntry(grs_A2_TMC[iQ2], Form("TMC Q2=%.1f",Q2), "l");

  }

  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(-0.5,0.5);
  mg->GetXaxis()->SetLimits(0.001,1.0);
  mg->GetYaxis()->SetTitle("A_{2}");
  mg->GetXaxis()->SetTitle("x");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  leg->Draw();

  TLatex lt;
  lt.SetTextAlign(12);
  lt.SetTextSize(0.04);
  lt.DrawLatex(0.1,0.4,Form("%s / %s",sfs->GetLabel(), ssfs->GetLabel()));

  c->SaveAs(Form("results/vcsa/A2_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/A2_%d_%d.pdf",sf_num,ssf_num));


}
