void rss_fit_W(){

  using namespace insane::physics;

  auto RSS_fits = new InSANE::physics::RSSAsymmetryFits();
  auto maid_fit = new MAID_VCSAs();

  auto frss = [&] (double *x, double *p){
    return RSS_fits->A2_W(x[0], p[0]);
  };
  auto fmaid = [&] (double *x, double *p){
    double xbj = InSANE::Kine::xBjorken_WQsq(x[0],p[0]);
    return maid_fit->A2(Nuclei::p, xbj, p[0]);
  };

  TF1 * f1 = new TF1("f1", frss, 1, 2.3, 1);
  f1->SetParameter(0,1.0);
  f1->SetNpx(500);

  TF1 * f2 = new TF1("f2", fmaid, 1, 2.3, 1);
  f2->SetParameter(0,1.0);
  f2->SetNpx(500);

  TCanvas*      c   = nullptr;
  TMultiGraph*  mg  = nullptr;
  TGraph*       gr  = nullptr;
  TLegend*      leg = nullptr;

  c   = new TCanvas();
  mg  = new TMultiGraph();
  leg = new TLegend();

  // --------------------------
  f1->SetParameter(0, 0.8);
  f1->SetLineColor(1);
  gr = new TGraph(f1);
  mg->Add(gr,"l");

  // --------------------------
  f1->SetParameter(0, 1.3);
  f1->SetLineColor(2);
  gr = new TGraph(f1);
  mg->Add(gr,"l");

  // --------------------------
  f1->SetParameter(0, 1.8);
  f1->SetLineColor(4);
  gr = new TGraph(f1);
  mg->Add(gr,"l");

  // --------------------------
  f1->SetParameter(0, 2.3);
  f1->SetLineColor(8);
  gr = new TGraph(f1);
  mg->Add(gr,"l");

  // --------------------------
  f2->SetParameter(0, 0.8);
  f2->SetLineColor(1);
  f2->SetLineStyle(2);
  gr = new TGraph(f2);
  mg->Add(gr,"l");

  // --------------------------
  f2->SetParameter(0, 1.3);
  f2->SetLineColor(2);
  gr = new TGraph(f2);
  mg->Add(gr,"l");

  // --------------------------
  f2->SetParameter(0, 1.8);
  f2->SetLineColor(4);
  gr = new TGraph(f2);
  mg->Add(gr,"l");

  // --------------------------
  f2->SetParameter(0, 2.3);
  f2->SetLineColor(8);
  gr = new TGraph(f2);
  mg->Add(gr,"l");

  mg->Draw("a");
}
