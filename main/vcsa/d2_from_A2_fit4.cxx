void d2_from_A2_fit4(int anumber = 9045) {

  using namespace insane::physics;

  int Wmin       = 1500;
  int n_params   = 5;
  
  typedef  insane::physics::Stat2015_PPDFs T2PDF  ;
  std::string pdf_names =  "Stat2015";
  //typedef  insane::physics::JAM_PPDFs T2PDF;
  //std::string pdf_names =  "JAM";

  //typedef  wevolve::SimpleFit_T3DFs3   T3FIT;  // 700 series
  //typedef  wevolve::SimpleFit_T3DFs   T3FIT;   // 800 series
  //typedef  wevolve::SimpleFit_T3DFs2   T3FIT;    // 900 series
  typedef  wevolve::SimpleFit_T3DFs4   T3FIT;    // 900 series
  pdf_names += "_F4";

  // ------------------------------------------------
  
  std::string dir_name  = Form("/home/whit/work/SANE/results/A2_fit/Wmin_%d_%dpars", Wmin, n_params);

  TMatrixDSym* cov  = nullptr;
  TMatrixDSym* corr = nullptr;
  std::vector<double>* result_pars = nullptr;
  std::map<std::string,double>* result_values = nullptr;
  //result_values["Wmin"] = W_min_data;
  //result_values["Q2min"] = Q20-Q2width;
  //result_values["E"] = Q20-Q2width;
  //result_values["Prob"] = result.Prob();
  //result_values["Ndf"] = result.Ndf();
  //result_values["Chi2"] = result.Chi2();
 

  TFile * f_result = new TFile(Form("%s/A2_fit_%d.root",dir_name.c_str(),anumber),"UPDATE");
  f_result->cd();
  f_result->GetObject("cov_matrix" , cov         );
  f_result->GetObject("corr_matrix", corr        );
  f_result->GetObject("parameters" , result_pars );
  f_result->GetObject("result_values" , result_values );
  std::cout << "Prob : " << (*result_values)["Prob"] << std::endl;

  for(auto val : (*result_values)) {
    std::cout << val.first << " : " << val.second << std::endl;
  }

  double Chi2 = (*result_values)["MinFcnValue"];
  int    Ndf  = (*result_values)["Ndf"];  ;

  std::vector<double> parameters = *result_pars;
  for(auto apr: parameters) {
    std::cout << apr << ", "; 
  }
  std::cout << std::endl;
  
  auto ssfs = new SSFsFromPDFs<T2PDF,T3FIT>();
  std::cout << "setting  params\n";
  std::get<T3FIT>(ssfs->fDFs).SetParams(parameters,true);
  //std::get<1>(ssfs->fDFs);//.SetParams(parameters);
  //
  std::cout << "getting  params\n";
  
  for(auto apr: std::get<T3FIT>(ssfs->fDFs).GetParams()) {
    std::cout << apr << ", "; 
  }
  std::cout << std::endl;

  TGraph * d2_gr              = new TGraph();
  TGraph * d2_TMC_gr          = new TGraph();
  TGraph * M23_gr             = new TGraph();
  TGraph * M23_TMC_gr         = new TGraph();
  TGraph * g2bar_3_gr         = new TGraph();
  TGraph * g2bar_TMC_gr       = new TGraph();
  TGraph * g2bar_minus_M23_gr = new TGraph();

  double Q2_min = 0.5;
  double Q2_max = 5.5;
  double x_min  = 0.05;
  double x_max  = 0.95;
  std::vector<double> vars = {0.0};

  std::function<double(const double*, const double*)> func_M23 = [&](const double * x, const double * p){
      std::vector<double> pars_temp(n_params,0.0);
      for(int ipars = 0 ; ipars < n_params; ipars++) { pars_temp[ipars] = p[ipars]; }
      std::get<T3FIT>(ssfs->fDFs).SetParams(pars_temp);
      double Q2__  = x[0];
      double xmax__ = InSANE::Kine::xBjorken_WQsq(0.938+0.139,Q2__);
      //double xbj = InSANE::Kine::xBjorken_WQ2(W,Qsq);
      double res = ssfs->M2n_TMC_p(3,  Q2__, x_min, xmax__)*2.0;
      return res;
      };

  int Npoints  = 20;
  TH1  * hd2_integrand = new TH1F("d2_integrand","d2 integrand", 10, Q2_min, Q2_max);
  TH1  * ci_hist = NucDB::GetConfidenceIntervals(*cov, Chi2, Ndf, 0.95, func_M23, 1, parameters, hd2_integrand, 0, vars);
  std::cout << " CI hist done\n";

  std::get<T3FIT>(ssfs->fDFs).SetParams(parameters);
  
  for(int i = 0; i<Npoints; i++) {

    std::cout << " i : " << i << std::endl;
    double Qsq         = 0.5 + double(i)*0.055*(100.0/double(Npoints));

    x_max = InSANE::Kine::xBjorken_WQsq(0.938+0.139,Qsq);
    std::cout << "Q2: "<< Qsq << ", x_max = " << x_max << std::endl;

    double d2_val      = ssfs->d2p_tilde(    Qsq, x_min, x_max);
    double d2_TMC_val  = ssfs->d2p_tilde_TMC(Qsq, x_min, x_max);
    double M23_val     = ssfs->M2n_p(    3,  Qsq, x_min, x_max)*2.0;
    double M23_TMC_val = ssfs->M2n_TMC_p(3,  Qsq, x_min, x_max)*2.0;
    double g2bar_3     = std::get<T3FIT>(ssfs->fDFs).d2p(Qsq, x_min, x_max);
    double g2bar_3_TMC = std::get<T3FIT>(ssfs->fDFs).d2p_TMC(Qsq, x_min, x_max);

    d2_gr     ->SetPoint(i, Qsq, d2_val);
    d2_TMC_gr ->SetPoint(i, Qsq, d2_TMC_val);
    M23_gr    ->SetPoint(i, Qsq, M23_val);
    M23_TMC_gr->SetPoint(i, Qsq, M23_TMC_val);
    g2bar_3_gr->SetPoint(i, Qsq, g2bar_3);
    g2bar_TMC_gr->SetPoint(i, Qsq, g2bar_3_TMC);


    g2bar_minus_M23_gr->SetPoint(i, Qsq, g2bar_3 - M23_TMC_val);

  } 

  // ----------------------------------------
  TCanvas*     c   = new TCanvas();
  TMultiGraph* mg  = new TMultiGraph();
  TLegend*     leg = new TLegend(0.55,0.60,0.82,0.87);
  leg->SetMargin(0.25);
  leg->SetHeader(Form("%.2f < x < %.2f",x_min,x_max),"C");
  leg->SetEntrySeparation(0.35);

  d2_gr     ->SetLineColor(1);
  d2_TMC_gr ->SetLineColor(1);
  d2_TMC_gr ->SetLineStyle(2);
  M23_gr    ->SetLineColor(2);
  M23_TMC_gr->SetLineColor(2);
  M23_TMC_gr->SetLineStyle(2);
  g2bar_3_gr->SetLineColor(4);
  g2bar_3_gr->SetLineStyle(1);
  g2bar_TMC_gr->SetLineColor(4);
  g2bar_TMC_gr->SetLineStyle(2);

  d2_gr     ->SetLineWidth(2);
  d2_TMC_gr ->SetLineWidth(2);
  M23_gr    ->SetLineWidth(2);
  M23_TMC_gr->SetLineWidth(2);
  g2bar_3_gr->SetLineWidth(2);
  g2bar_TMC_gr->SetLineWidth(2);

  auto         * ci_gr = new TGraphErrors(ci_hist);
  ci_gr->SetFillColorAlpha( kRed, 0.35 );
  ci_gr->SetLineColor( kBlue );
  ci_gr->SetLineWidth( 3 );
  mg->Add(ci_gr,"le3");

  leg->AddEntry(d2_gr,"I(M=0)","l");
  leg->AddEntry(d2_TMC_gr,"I","l");
  leg->AddEntry(M23_gr,"2 M_{2}^{3}(M=0)","l");
  leg->AddEntry(M23_TMC_gr,"2 M_{2}^{3}","l");
  leg->AddEntry(g2bar_3_gr,"3 #bar{g}_{2}(M=0)","l");
  leg->AddEntry(g2bar_TMC_gr,"3 #bar{g}_{2}","l");

  mg->Add(d2_gr,      "l");
  mg->Add(d2_TMC_gr,      "l");
  mg->Add(M23_gr,     "l");
  mg->Add(M23_TMC_gr, "l");
  mg->Add(g2bar_3_gr, "l");
  mg->Add(g2bar_TMC_gr, "l");

  mg->Draw("a");
  mg->GetXaxis()->CenterTitle(true);
  mg->GetXaxis()->SetTitle("Q^{2}");
  mg->GetXaxis()->SetLimits(0.5,5.5);
  mg->GetYaxis()->CenterTitle(true);
  mg->GetYaxis()->SetTitle("");
  mg->GetYaxis()->SetRangeUser(-0.011,0.023);
  mg->Draw("a");
  leg->Draw();

  gSystem->mkdir("results/d2_from_A2_fit4",true);
  c->SaveAs(Form("results/d2_from_A2_fit4/d2_Wmin%d_%dpars_%d_%s.pdf",Wmin,n_params,anumber,pdf_names.c_str())); 

  // --------------
  mg->Add(g2bar_minus_M23_gr, "l");
  leg->AddEntry(g2bar_minus_M23_gr,"3 #bar{g}_{2} - 2M_{2}^{3}","l");
  mg->Draw("a");
  leg->Draw();
  c->SaveAs(Form("results/d2_from_A2_fit4/d2_Wmin%d_%dpars_%d_%s_2.pdf",Wmin,n_params,anumber,pdf_names.c_str())); 

  // ----------------------------------------
  //
  TFile * f = new TFile(Form("results/d2_from_A2_fit4_%d.root",anumber),"UPDATE");
  f->cd();
  mg->Write("mg");
  d2_gr     ->Write("d2");
  d2_TMC_gr ->Write("d2");
  M23_gr    ->Write("M23");
  M23_TMC_gr->Write("M23_TMC");
  g2bar_3_gr->Write("3g2bar");
  g2bar_TMC_gr->Write("3g2bar_TMC");
  ci_gr->Write("CI_M23_TMC");
  f->Close();

}

