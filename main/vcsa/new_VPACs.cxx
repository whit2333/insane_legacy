//#include "VCSAsFromPDFs.h"
//#include "VCSAsFromSFs.h"

void new_VPACs(int aNumber = 0) {
  using namespace insane::physics;

  int sf_num  = 1;
  int ssf_num = 14;
  //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
  //auto sfs    = fman->CreateSFs(sf_num);
  //auto ssfs   = fman->CreatePolSFs(ssf_num);

  //auto ppdfs = new JAM15PolarizedPDFs();
  //auto ssfs  = new InSANEPolarizedStructureFunctionsFromPDFs(ppdfs);

  ////auto pdfs  = new Stat2015UnpolarizedPDFs();
  //auto pdfs  = new CTEQ10UnpolarizedPDFs();

  //auto sfs   = new InSANEStructureFunctionsFromPDFs(pdfs);
  ////auto sfs   = new F1F209StructureFunctions();
  
  //auto vca = new VCSAsFromPDFs<Stat2015_UPDFs, Stat2015_PPDFs>();
  //auto vca = new VCSAsFromPDFs<std::tuple<CTEQ10_UPDFs>, std::tuple<JAM_PPDFs>>();
  auto vca = new MAID_VPACs();//VCSAsFromSFs<F1F209_SFs, SSFsFromPDFs<Stat2015_PPDFs>>();
  //auto vca = new VCSAsFromSFs<SFsFromPDFs<Stat2015_UPDFs>, SSFsFromPDFs<Stat2015_PPDFs>>();
  //vca->Dump();

  //auto sf1 = SFsFromPDFs<Stat2015_UPDFs>();
  //auto sf2 = SSFsFromPDFs<Stat2015_PPDFs>();


  std::vector<int> colors = {1,2,4,6,8,9};

  double x_min = 0.05;
  double x_max = 0.95;
  int    N     = 50;
  double dx    = (x_max-x_min)/double(N);

  double Q2_min = 0.5;
  double Q2_max = 3.0;
  int    NQ2    = 5;
  double dQ2    = (Q2_max-Q2_min)/double(NQ2);

  std::vector<TGraph*> grs_A1;
  std::vector<TGraph*> grs_A1_TMC;
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {
    grs_A1.push_back(  new TGraph() );
    grs_A1_TMC.push_back(  new TGraph() );
  }

  //---------------------------------------
  //
  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    for (int ix = 0; ix < N; ix++) {

      double x = x_min + double(ix)*dx;

      //double A1      = vca->A2p(x, Q2);// insane::physics::A1_SFs(sf1,sf2,insane::physics::Nuclei::p,x,Q2);
      vca->CalculateProton(x, Q2);
      double A1      = vca->Sig_LTp();// insane::physics::A1_SFs(sf1,sf2,insane::physics::Nuclei::p,x,Q2);
      grs_A1[iQ2]->SetPoint( ix, x, A1);

      double A1_TMC      = vca->Sig_LT();;
      //double A1_TMC      = insane::physics::A1(tup1,tup2,insane::physics::Nuclei::p,x,Q2);
      grs_A1_TMC[iQ2]->SetPoint( ix, x, A1_TMC);
    }
  }

  //---------------------------------------
  //
  TCanvas     * c   = new TCanvas();
  TMultiGraph * mg  = new TMultiGraph();
  TLegend     * leg = new TLegend(0.8,0.6,0.975,0.975);

  for (int iQ2 = 0; iQ2 < NQ2; iQ2++) {

    double Q2  = Q2_min + double(iQ2)*dQ2;

    grs_A1[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A1[iQ2]->SetMarkerStyle(20);
    grs_A1[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A1[iQ2]->SetLineWidth(2);
    grs_A1[iQ2]->SetLineStyle(1+(iQ2/(colors.size())) );
    mg->Add(      grs_A1[iQ2],"l");
    leg->AddEntry(grs_A1[iQ2], Form("Q2=%.1f",Q2), "l");

    grs_A1_TMC[iQ2]->SetLineColor(colors[iQ2%(colors.size())]);
    grs_A1_TMC[iQ2]->SetMarkerStyle(20);
    grs_A1_TMC[iQ2]->SetMarkerColor(colors[iQ2%(colors.size())]);
    grs_A1_TMC[iQ2]->SetLineWidth(2);
    grs_A1_TMC[iQ2]->SetLineStyle(2+(iQ2/(colors.size())) );
    mg->Add(      grs_A1_TMC[iQ2],"l");
    leg->AddEntry(grs_A1_TMC[iQ2], Form("TMC Q2=%.1f",Q2), "l");

  }
  mg->Draw("a");
  mg->GetYaxis()->SetRangeUser(0.0,1.4);
  mg->GetXaxis()->SetLimits(0.001,1.0);
  mg->GetYaxis()->SetTitle("A_{1}");
  mg->GetXaxis()->SetTitle("x");
  mg->GetYaxis()->CenterTitle(true);
  mg->GetXaxis()->CenterTitle(true);
  mg->Draw("a");
  leg->Draw();

  TLatex lt;
  lt.SetTextAlign(12);
  lt.SetTextSize(0.04);
  //lt.DrawLatex(0.1,1.0,Form("%s / %s",sfs->GetLabel(), ssfs->GetLabel()));

  c->SaveAs(Form("results/vcsa/new_A1_%d_%d.png",sf_num,ssf_num));
  c->SaveAs(Form("results/vcsa/new_A1_%d_%d.pdf",sf_num,ssf_num));

}
