void rss_fit(){

  using namespace insane::physics;

  auto RSS_fits = new InSANE::physics::RSSAsymmetryFits2();
  auto params = RSS_fits->GetA2Amplitudes();
  for(int i = 0 ; i<4; i++){
    params[i] = 0.5*params[i];
  }

  //auto maid_fit = new MAID_VCSAs();
  auto maid_fit = new InSANE::physics::RSSAsymmetryFits2();
  maid_fit->SetA2Amplitudes(params);

  auto frss = [&] (double *x, double *p){
    return RSS_fits->A2_x(x[0], p[0]);
  };
  auto fmaid = [&] (double *x, double *p){
    return maid_fit->A2_x( x[0], p[0]);
    //return maid_fit->A2(Nuclei::p, x[0], p[0]);
  };

  TF1 * f1 = new TF1("f1", frss, 0, 1, 1);
  f1->SetParameter(0,1.0);
  f1->SetNpx(500);

  TF1 * f2 = new TF1("f2", fmaid, 0.01, 0.99, 1);
  f2->SetParameter(0,1.0);
  f2->SetNpx(500);

  TCanvas*      c   = nullptr;
  TMultiGraph*  mg  = nullptr;
  TGraph*       gr  = nullptr;
  TLegend*      leg = nullptr;

  c   = new TCanvas();
  mg  = new TMultiGraph();
  leg = new TLegend();

  // --------------------------
  f1->SetParameter(0, 0.8);
  f1->SetLineColor(1);
  gr = new TGraph(f1);
  mg->Add(gr,"l");

  // --------------------------
  f1->SetParameter(0, 1.3);
  f1->SetLineColor(2);
  gr = new TGraph(f1);
  mg->Add(gr,"l");

  // --------------------------
  f1->SetParameter(0, 1.8);
  f1->SetLineColor(4);
  gr = new TGraph(f1);
  mg->Add(gr,"l");

  // --------------------------
  f1->SetParameter(0, 2.3);
  f1->SetLineColor(8);
  gr = new TGraph(f1);
  mg->Add(gr,"l");

  // --------------------------
  f2->SetParameter(0, 0.8);
  f2->SetLineColor(1);
  f2->SetLineStyle(2);
  gr = new TGraph(f2);
  mg->Add(gr,"l");

  // --------------------------
  f2->SetParameter(0, 1.3);
  f2->SetLineColor(2);
  gr = new TGraph(f2);
  mg->Add(gr,"l");

  // --------------------------
  f2->SetParameter(0, 1.8);
  f2->SetLineColor(4);
  gr = new TGraph(f2);
  mg->Add(gr,"l");

  // --------------------------
  f2->SetParameter(0, 2.3);
  f2->SetLineColor(8);
  gr = new TGraph(f2);
  mg->Add(gr,"l");

  mg->Draw("a");
}
