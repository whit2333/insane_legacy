void d2_from_A2_fit2(int anumber = 19) {

  using namespace insane::physics;

  double Chi2 = 301.143;
  int    Ndf  = 188;

  int Wmin              = 1800;
  std::string pdf_names =  "JAM_Fit3";
  //typedef  insane::physics::Stat2015_PPDFs T2PDF  ;
  typedef  insane::physics::JAM_PPDFs T2PDF;
  //typedef  wevolve::SimpleFit_T3DFs   T3FIT;
  typedef  wevolve::SimpleFit_T3DFs3   T3FIT;
  std::vector<double> parameters = {
    //0.359988, -11.0374, 87.7535, -304.583, 543.811, -515.785 // 604 Wmin=1600
    //0.391982, -12.2933, 108.323, -431.253, 898.229, -1009.79 // 607 Wmin=1800
    //0.362093, -11.9828  , 107.121  , -428.724  , 894.346  , -1005.46  // 609 Wmin=1900
    -0.0482366 , 0.0925829 , -0.127698 
  };

  int n_params          = parameters.size();
  
  auto ssfs = new SSFsFromPDFs<T2PDF,T3FIT>();
  std::get<T3FIT>(ssfs->fDFs).SetParams(parameters);
  //std::get<1>(ssfs->fDFs);//.SetParams(parameters);

  TGraph * d2_gr              = new TGraph();
  TGraph * d2_TMC_gr          = new TGraph();
  TGraph * M23_gr             = new TGraph();
  TGraph * M23_TMC_gr         = new TGraph();
  TGraph * g2bar_3_gr         = new TGraph();
  TGraph * g2bar_TMC_gr       = new TGraph();
  TGraph * g2bar_minus_M23_gr = new TGraph();

  double x_min = 0.05;
  double x_max = 0.95;
  
  int Npoints  = 30;
  for(int i = 0; i<Npoints; i++) {

    std::cout << " i : " << i << std::endl;
    double Qsq         = 0.5 + double(i)*0.055*(100.0/double(Npoints));
    double d2_val      = ssfs->d2p_tilde(    Qsq, x_min, x_max);
    double d2_TMC_val  = ssfs->d2p_tilde_TMC(Qsq, x_min, x_max);
    double M23_val     = ssfs->M2n_p(    3,  Qsq, x_min, x_max)*2.0;
    double M23_TMC_val = ssfs->M2n_TMC_p(3,  Qsq, x_min, x_max)*2.0;
    double g2bar_3     = std::get<T3FIT>(ssfs->fDFs).d2p(Qsq, x_min, x_max);
    double g2bar_3_TMC = std::get<T3FIT>(ssfs->fDFs).d2p_TMC(Qsq, x_min, x_max);

    d2_gr     ->SetPoint(i, Qsq, d2_val);
    d2_TMC_gr ->SetPoint(i, Qsq, d2_TMC_val);
    M23_gr    ->SetPoint(i, Qsq, M23_val);
    M23_TMC_gr->SetPoint(i, Qsq, M23_TMC_val);
    g2bar_3_gr->SetPoint(i, Qsq, g2bar_3);
    g2bar_TMC_gr->SetPoint(i, Qsq, g2bar_3_TMC);


    g2bar_minus_M23_gr->SetPoint(i, Qsq, g2bar_3 - M23_TMC_val);

  } 

  // ----------------------------------------
  TCanvas*     c   = new TCanvas();
  TMultiGraph* mg  = new TMultiGraph();
  TLegend*     leg = new TLegend(0.55,0.60,0.82,0.87);
  leg->SetMargin(0.25);
  leg->SetHeader(Form("%.2f < x < %.2f",x_min,x_max),"C");
  leg->SetEntrySeparation(0.35);

  d2_gr     ->SetLineColor(1);
  d2_TMC_gr ->SetLineColor(1);
  d2_TMC_gr ->SetLineStyle(2);
  M23_gr    ->SetLineColor(2);
  M23_TMC_gr->SetLineColor(2);
  M23_TMC_gr->SetLineStyle(2);
  g2bar_3_gr->SetLineColor(4);
  g2bar_3_gr->SetLineStyle(1);
  g2bar_TMC_gr->SetLineColor(4);
  g2bar_TMC_gr->SetLineStyle(2);

  d2_gr     ->SetLineWidth(2);
  d2_TMC_gr ->SetLineWidth(2);
  M23_gr    ->SetLineWidth(2);
  M23_TMC_gr->SetLineWidth(2);
  g2bar_3_gr->SetLineWidth(2);
  g2bar_TMC_gr->SetLineWidth(2);

  leg->AddEntry(d2_gr,"I(M=0)","l");
  leg->AddEntry(d2_TMC_gr,"I","l");
  leg->AddEntry(M23_gr,"2 M_{2}^{3}(M=0)","l");
  leg->AddEntry(M23_TMC_gr,"2 M_{2}^{3}","l");
  leg->AddEntry(g2bar_3_gr,"3 #bar{g}_{2}(M=1)","l");
  leg->AddEntry(g2bar_TMC_gr,"3 #bar{g}_{2}","l");

  mg->Add(d2_gr,      "l");
  mg->Add(d2_TMC_gr,      "l");
  mg->Add(M23_gr,     "l");
  mg->Add(M23_TMC_gr, "l");
  mg->Add(g2bar_3_gr, "l");
  mg->Add(g2bar_TMC_gr, "l");

  mg->Draw("a");
  mg->GetXaxis()->CenterTitle(true);
  mg->GetXaxis()->SetTitle("Q^{2}");
  mg->GetXaxis()->SetLimits(0.5,5.5);
  mg->GetYaxis()->CenterTitle(true);
  mg->GetYaxis()->SetTitle("");
  mg->GetYaxis()->SetRangeUser(-0.011,0.023);
  mg->Draw("a");
  leg->Draw();

  gSystem->mkdir("results/d2_from_A2_fit2",true);
  c->SaveAs(Form("results/d2_from_A2_fit2/d2_Wmin%d_%dpars_%s.pdf",Wmin,n_params,pdf_names.c_str())); 

  // --------------
  mg->Add(g2bar_minus_M23_gr, "l");
  leg->AddEntry(g2bar_minus_M23_gr,"3 #bar{g}_{2} - 2M_{2}^{3}","l");
  mg->Draw("a");
  leg->Draw();
  c->SaveAs(Form("results/d2_from_A2_fit2/d2_Wmin%d_%dpars_%s_2.pdf",Wmin,n_params,pdf_names.c_str())); 

  // ----------------------------------------
  //
  TFile * f = new TFile(Form("results/d2_from_A2_fit2_%d.root",anumber),"UPDATE");
  f->cd();
  mg->Write("mg");
  d2_gr     ->Write("d2");
  d2_TMC_gr ->Write("d2");
  M23_gr    ->Write("M23");
  M23_TMC_gr->Write("M23_TMC");
  g2bar_3_gr->Write("3g2bar");
  g2bar_TMC_gr->Write("3g2bar_TMC");
  f->Close();

}

