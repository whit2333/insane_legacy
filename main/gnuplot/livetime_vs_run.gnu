#!/usr/bin/gnuplot
#set term dumb
set term png
set output "results/livetime_vs_run_2.png"

set xlabel "run number"
set ylabel "live time"
set pointsize 2
#set style fill transparent solid 0.2 noborder
set yrange [0.75:1.0]
#set xrange [72200:73050]
f(x) = 0.92
tau  = 0.0009
#fit f(x) "results/livetime_fit_results1.txt" using 2:3 via tau 
plot "results/livetime_fit_results1.txt" using 1:3 with points pt 6,\
     "results/livetime_fit_results2.txt" using 1:3 with points pt 5,\
     "results/livetime_fit_results3.txt" using 1:3 with points pt 4

