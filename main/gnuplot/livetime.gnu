#!/usr/bin/gnuplot
#set term dumb

set term png  
set output "results/livetime_vs_rate_2.png"

set xlabel "counts per 2s"
set ylabel "live time"
set pointsize 2
#set style fill transparent solid 0.2 noborder
#set xrange [0:3200]
set yrange [0.75:1.0]
f(x) = 1.0 - tau*x
tau  = 0.0009
fit f(x) "results/livetime_fit_results1.txt" using 2:3 via tau 
plot "results/livetime_fit_results1.txt" using 2:3 with points pt 6,\
     "results/livetime_fit_results2.txt" using 2:3 with points pt 5,\
     "results/livetime_fit_results3.txt" using 2:3 with points pt 4,f(x)

