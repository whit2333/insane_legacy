#!/usr/bin/gnuplot
#set term dumb
set term png
set output "results/livetime_slope_vs_run_3.png"

set xlabel "run number"
set ylabel "live time slope"
set pointsize 2
#set style fill transparent solid 0.2 noborder
#set yrange [0.8:1.0]
#set xrange [72200:73050]
#f(x) = 0.92
tau  = 0.0009
#fit f(x) "results/livetime_fit_results5.txt" using 2:3 via tau 
plot "results/livetime_fit_results1.txt" using 1:4 with points pt 6,\
     "results/livetime_fit_results2.txt" using 1:4 with points pt 5,\
     "results/livetime_fit_results3.txt" using 1:4 with points pt 4

