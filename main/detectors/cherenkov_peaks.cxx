Int_t cherenkov_peaks(Int_t runNumber = 72988) {

   Double_t max = 0.0;
//    if(!rman->IsRunSet() ) rman->SetRun(runNumber);
//    else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
//    rman->fCurrentFile->cd();
/*   aman->BuildQueue("replaying_list.txt");*/
   aman->BuildQueue("lists/para/test_para.txt");

   TH1F * h1[8];
   TH1F * h2[8];
   TH1F * h3[8];
   TGraph * gr[8];

   for(int j=0;j<8;j++) {
      h1[j] = new TH1F(Form("Cherenkov1electron%d",j+1),Form("Cherenkov single electron peak mirror %d",j+1),100,0,4000);
      h2[j] = new TH1F(Form("Cherenkov1electron%dwidth",j+1),Form("Cherenkov single electron peak width - mirror %d",j+1),100,0,4000);
      h3[j] = new TH1F(Form("Cherenkov2electron%dwidth",j+1),Form("Cherenkov doulbe electron peak width - mirror %d",j+1),100,0,4000);
      h2[j]->SetLineColor(2);
      h3[j]->SetLineColor(4);
      gr[j] = new TGraph(aman->fRunQueue->size());
   }

   for(int i = 0;i<aman->fRunQueue->size();i++) {
      rman->SetRun(aman->fRunQueue->at(i));
      rman->fCurrentFile->cd();
      GasCherenkovDetector cherenkovDetector;
      cherenkovDetector.GetADCAlignmentCalibrations(rman->fCurrentRunNumber);
      for(int j=0;j<8;j++) {
         h1[j]->Fill(cherenkovDetector.fCherenkovAlignmentCoef[j]);
         h2[j]->Fill(cherenkovDetector.fCherenkovSingleElectronWidth[j]);
         h3[j]->Fill(cherenkovDetector.fCherenkovDoubleElectronWidth[j]);
         gr[j]->SetPoint(i,rman->fCurrentRunNumber,cherenkovDetector.fCherenkovAlignmentCoef[j]);
      }
   }

   std::cout << " Creating Multi Graph \n";
   TMultiGraph * mg = new TMultiGraph();

   TCanvas * c = new TCanvas("CherenkovPeaks", "cherenkov Peaks", 500,800);
   c->Divide(2,4);
   TLegend * leg2 = new TLegend(0.0,0.8,0.3,1.0);
   leg2->SetHeader("Cherenkov peaks vs Run");

   for(int j=0;j<8;j++) {
      c->cd(8-j);
      h1[j]->Draw();
      h2[j]->Draw("same");
      h3[j]->Draw("same");
      gr[j]->SetMarkerStyle(20+j);
      gr[j]->SetMarkerSize(1.3);
      gr[j]->SetMarkerColor(1+j);
      leg2->AddEntry(gr[j],Form("Cherenkov Mirror %d",j+1),"p");
      mg->Add(gr[j],"p");
   }

   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("Cherenkov ");
   leg->AddEntry(h1[0],"single electron peak","l");
   leg->AddEntry(h2[0],"single electron peak width","l");
   leg->AddEntry(h3[0],"double electron peak width","l");
   leg->Draw();

   TCanvas * c2 = new TCanvas("GraphCherenkovPeaks", "Graph of Cherenkov Peaks ", 800,400);
   c2->cd(0);
   mg->Draw("a");
   leg2->Draw();
return(0);
}
