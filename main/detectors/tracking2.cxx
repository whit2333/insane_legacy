/*!  Track reconstruction with BigCal


   TODO:
   - Do the y cut first then look at the X distribution ( curretnly done the other way around)
   - Look at Pi0 events w.r.t. reconstructed tracks (no shower)


 */
Int_t tracking2(Int_t runNumber=72999) {

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->fCurrentFile->cd();

   const char * treeName = "trackingPositions";
   TTree * t = (TTree*)gROOT->FindObject(treeName);
   if(!t){ std::cout << " TREE NOT FOUND : " << treeName << "\n"; return(-1);}

   InSANEReconstructedEvent * aRecon = new InSANEReconstructedEvent();
   BIGCALCluster * aCluster= 0;
   t->SetBranchAddress("uncorrelatedPositions",&aRecon);
   SANEEvents * ev = new SANEEvents("betaDetectors1");
   ev->SetClusterBranches(ev->fTree);

   t->AddFriend(ev->fTree);

   TDirectory * cerPerformanceDir=0;
   TFile * cerFile = new TFile("data/tracking2.root","UPDATE");
   if( cerFile ) {
      if( !(cerFile->cd(Form("cherenkov_energy_dep%d",runNumber)) ) ) {
         cerPerformanceDir = cerFile->mkdir(Form("cherenkov_energy_dep%d",runNumber)) ;
         if( !(cerPerformanceDir->cd()) ) printf(" dir Error\n");
      } else {
         cerPerformanceDir = gDirectory;
      }
   }

   Double_t trackerLuciteYSlope = 5.0;
   const Int_t NEBins = 4;
   /// HISTOGRAMS
   TH1F * hCerNPESingle[8];
   TH1F * hCerNPESum[8];
   TH1F * hCerSignalSum[8];
   TH1F * hCerSignalSingle[8];

   TH1F * hCerNPESingleEdep[8][NEBins];
   TH1F * hCerNPESumEdep[8][NEBins];
   TH1F * hCerSignalSumEdep[8][NEBins];
   TH1F * hCerSignalSingleEdep[8][NEBins];

   TEfficiency * hCerSignalEfficiencyVsEnergy[8];
   TEfficiency * hCerSignalEfficiencyVsY[8];
   TEfficiency * hCerSignalEfficiencyVsX[8];

   for(int i = 0; i<8;i++) {

      hCerNPESum[i] = new TH1F(Form("Cer-%d-NPE Sum",i+1),Form("Cer -%d- NPE Sum",i+1),50,1,45);
      hCerNPESingle[i] = new TH1F(Form("Cer-%d-NPE",i+1),Form("Cer -%d- NPE",i+1),50,1,45);      hCerSignalSum[i] = new TH1F(Form("Cer-%d-Sum",i+1),Form("Cer -%d- Sum",i+1),50,0.02,4);
      hCerSignalSingle[i] = new TH1F(Form("Cer-%d-",i+1),Form("Cer -%d-",i+1),50,0.02,4);

      for(int k = 0; k<NEBins;k++) {
         hCerNPESumEdep[i][k] = new TH1F(Form("Cer-%d-NPE-Sum_%d",i+1,k),Form("Cer -%d- NPE Sum _%d",i+1,k),50,1,45);
         hCerNPESingleEdep[i][k] = new TH1F(Form("Cer-%d-NPE_%d",i+1,k),Form("Cer -%d- NPE _%d",i+1,k),50,1,45);      
         hCerSignalSumEdep[i][k] = new TH1F(Form("Cer-%d-Sum_%d",i+1,k),Form("Cer -%d- Sum _%d",i+1,k),50,0.02,4);
         hCerSignalSingleEdep[i][k] = new TH1F(Form("Cer-%d-_%d",i+1,k),Form("Cer -%d- _%d",i+1,k),50,0.02,4);
      }

      hCerSignalEfficiencyVsEnergy[i] = new TEfficiency(Form("Cer-%d-EffVsEnergy",i+1),Form("Cer -%d- EffVsEnergy",i+1),50,500,4500);
      hCerSignalEfficiencyVsY[i] = new TEfficiency(Form("Cer-%d-EffVsY",i+1),Form("Cer -%d- EffVsY",i+1),60,-120,120);
      hCerSignalEfficiencyVsX[i] = new TEfficiency(Form("Cer-%d-EffVsX",i+1),Form("Cer -%d- EffVsX",i+1),60,-120,120);
   }


   TH2F * hBigCalXYEdep[NEBins];
   TH2F * hBigCalYvsMissEdep[NEBins];

   for(int k = 0; k<NEBins;k++) {
      hBigCalXYEdep[k] = new TH2F(Form("hBigCalXYEdep_%d",k),Form("hBigCalXYEdep_%d",k),60,-60,60,60,-120,120);
      hBigCalYvsMissEdep[k] = new TH2F(Form("hBigCalYvsMissEdep_%d",k),Form("hBigCalYvsMissEdep_%d",k),30,-5,5,30,-120,120);
   }


   TEfficiency * hAllCerEfficiencyVsEnergy = new TEfficiency("hAllCerEfficiencyVsEnergy","All Cer EffVsEnergy",50,500,4500);
   TEfficiency * hAllCerEfficiencyVsY = new TEfficiency("hAllCerEfficiencyVsY","All Cer EffVsY",60,-120,120);
   TEfficiency * hAllCerEfficiencyVsX = new TEfficiency("hAllCerEfficiencyVsX","All Cer EffVsX",60,-60,60);

   TEfficiency * hAllCerEfficiencyVsEnergy2 = new TEfficiency("hAllCerEfficiencyVsEnergy2","All Cer EffVsEnergy E>1000",50,500,4500);
   TEfficiency * hAllCerEfficiencyVsY2 = new TEfficiency("hAllCerEfficiencyVsY2","All Cer EffVsY E>1000",60,-120,120);
   TEfficiency * hAllCerEfficiencyVsX2 = new TEfficiency("hAllCerEfficiencyVsX2","All Cer EffVsX E>1000",60,-60,60);

   TCut beta = "triggerEvent.IsBETAEvent()";
   TCut trackLucHits = "fNLucitePositions>0&&fNTrackerPositions>0";
   TCut trackYCor = "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0"

   TCut E1 = "bigcalClusters.GetEnergy()>500&&bigcalClusters.GetEnergy()<900";
   TCut E2 = "bigcalClusters.GetEnergy()>900&&bigcalClusters.GetEnergy()<1200";
   TCut E3 = "bigcalClusters.GetEnergy()>1200&&bigcalClusters.GetEnergy()<1400";
   TCut E4 = "bigcalClusters.GetEnergy()>1400&&bigcalClusters.GetEnergy()<5900";


   InSANETrajectory * aTrajectory                 = 0;
   LuciteHodoscopePositionHit * aLucPositionHit   = 0;
   LuciteHodoscopePositionHit * bLucPositionHit   = 0;
   BIGCALCluster * aCluster                       = 0;
   BIGCALCluster * bCluster                       = 0;
   ForwardTrackerPositionHit * aTrackerPosHit     = 0;
   ForwardTrackerPositionHit * bTrackerPosHit     = 0;
   InSANEHitPosition * aPos                       = 0;
   InSANEHitPosition * bPos                       = 0;

   TClonesArray * lucPos         = aRecon->fLucitePositions;
   TClonesArray * trackerPos     = aRecon->fTrackerPositions;
   TClonesArray * lucHitPos      = aRecon->fLuciteHitPos;
   TClonesArray * trackerHitPos  = aRecon->fTrackerHitPos;

   Double_t Elow[4]  = {500,800,1100,1300};
   Double_t Ehigh[4] = {800,1000,1300,5900};

   BETADetectorPackage * BetaDet = new BETADetectorPackage();

   /// Event Loop
   Int_t Nentries = t->GetEntries();
   std::cout << "Entries " << Nentries << "\n";

   bool cerTDCcut = false;
   bool cerADCcut = false;
   Double_t miss  = 0.0;

   for(Int_t IEVENT=0;IEVENT < Nentries /*&& IEVENT<80000*/;IEVENT++) {

      t->GetEntry(IEVENT);
      if(IEVENT%5000==0)std::cout << " event  " << IEVENT<< "\n";

      /// Require lucite
      if( aRecon->fNLucitePositions > 0 ){
//       aRecon->Print();

      ev->fTree->GetEntryWithIndex(aRecon->fRunNumber,aRecon->fEventNumber);

         /// Require only one cluster
         if(ev->TRIG->IsBETAEvent())
         if( ev->CLUSTER->fClusters->GetEntries() == 1 ) {

            aCluster = (BIGCALCluster*)(*ev->CLUSTER->fClusters)[0];

            cerTDCcut = TMath::Abs(aCluster->fCherenkovTDC)<25;
            cerADCcut = /*aCluster->fCherenkovBestADCSum<2.2&&*/aCluster->fCherenkovBestADCSum>0.15;

            if(aCluster->fIsGood) if(aCluster->fNumberOfMirrors==1) {
               hCerNPESum[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestNPESum);
               hCerNPESingle[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestNPEChannel);
               hCerSignalSum[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestADCSum);
               hCerSignalSingle[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestADCChannel);

            }

            if(aCluster->fIsGood && cerADCcut && cerTDCcut) {
               for(int ii=0; ii<NEBins ;ii++) if(aCluster->GetEnergy() > Elow[ii] && aCluster->GetEnergy() < Ehigh[ii] ){
               
                  for(int il = 0;il<lucPos->GetEntries();il++){
                     bPos = (InSANEHitPosition*)(*lucPos)[il];
                     for(int it = 0;it<trackerPos->GetEntries();it++){
                        aPos = (InSANEHitPosition*)(*trackerPos)[it];

                        miss = ((aPos->fPosition) - (BetaDet->fTrackerDetector->GetSeedingHitPosition(bPos->fPosition) )).Y();
                        hBigCalYvsMissEdep[ii]->Fill(miss,aCluster->fYMoment);

                     }
                  }
                  hBigCalXYEdep[ii]->Fill(aCluster->fXMoment,aCluster->fYMoment); 
                  hCerNPESumEdep[aCluster->fPrimaryMirror-1][ii]->Fill(aCluster->fCherenkovBestNPESum);
                  hCerNPESingleEdep[aCluster->fPrimaryMirror-1][ii]->Fill(aCluster->fCherenkovBestNPEChannel);
                  hCerSignalSumEdep[aCluster->fPrimaryMirror-1][ii]->Fill(aCluster->fCherenkovBestADCSum);
                  hCerSignalSingleEdep[aCluster->fPrimaryMirror-1][ii]->Fill(aCluster->fCherenkovBestADCChannel);
               }
            }


//             hAllCerEfficiencyVsEnergy->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
//             hAllCerEfficiencyVsY->Fill(cerTDCcut,aCluster->fYMoment);
//             hAllCerEfficiencyVsX->Fill(cerTDCcut,aCluster->fXMoment);

//             if( (aCluster->fTotalE + aCluster->fDeltaE) > 1000.0) {
// //                hAllCerEfficiencyVsEnergy2->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
// //                hAllCerEfficiencyVsY2->Fill(cerTDCcut,aCluster->fYMoment);
// //                hAllCerEfficiencyVsX2->Fill(cerTDCcut,aCluster->fXMoment);
//             }
         }
      }
   }

   /// Canvas and simple draws
   TCanvas * c1 = new TCanvas("Tracking1","Tracking1 No cherenkov ADC");
   c1->Divide(2,4);
   for(int i = 0; i<8;i++) {
      c1->cd(8-i);
      hCerNPESingle[i]->SetLineColor(1);
//       hCerNPESingle[i]->SetLineWidth(1);
      hCerNPESingle[i]->SetFillColor(1);
      hCerNPESingle[i]->SetFillStyle(3001);
      hCerNPESingle[i]->Draw();
   }

   c1->SaveAs(Form("plots/%d/tracking2_1.png",runNumber));
   c1->SaveAs(Form("plots/%d/tracking2_1.pdf",runNumber));

   TCanvas * c2 = new TCanvas("CerEDep","Cherenkov E Dep");
   c2->Divide(2,4);
   THStack * hstacks[8];
   for(int i = 0; i<8;i++) {
      c2->cd(8-i);
      hstacks[i] = new THStack("MirrorEDep","Mirror E Dependence");
      for(int ii=0;ii<NEBins;ii++) {
      
         hCerNPESingleEdep[i][ii]->SetLineColor(1);
         hCerNPESingleEdep[i][ii]->SetLineWidth(1);
         hCerNPESingleEdep[i][ii]->SetFillColor(2+ii);
         hCerNPESingleEdep[i][ii]->SetFillStyle(3001+ii);
//       if(ii==0)hCerNPESingleEdep[i][ii]->Draw();
//       else hCerNPESingleEdep[i][ii]->Draw("same");
         hstacks[i]->Add(hCerNPESingleEdep[i][ii]);
      }
      hstacks[i]->Draw("nostack");
   }

   c2->SaveAs(Form("plots/%d/tracking2_2.png",runNumber));
   c2->SaveAs(Form("plots/%d/tracking2_2.pdf",runNumber));


   TCanvas * c3 = new TCanvas("BigcalEDep2","Cherenkov E Dep");
   c3->Divide(2,2);
   for(int i = 0; i<NEBins;i++) {
      c3->cd(i+1);
      hBigCalXYEdep[i]->Draw("col");
   }

   c3->SaveAs(Form("plots/%d/tracking2_3.png",runNumber));
   c3->SaveAs(Form("plots/%d/tracking2_3.pdf",runNumber));


   TCanvas * c4 = new TCanvas("hBigCalYvsMissEdep","bigcal Y vs luc miss  E Dep");
   c4->Divide(2,2);
   for(int i = 0; i<NEBins;i++) {
      c4->cd(i+1);
      hBigCalYvsMissEdep[i]->Draw("col");
   }

   c4->SaveAs(Form("plots/%d/tracking2_4.png",runNumber));
   c4->SaveAs(Form("plots/%d/tracking2_4.pdf",runNumber));

   t->StartViewer();

//     canvas->SaveAs(Form("plots/%d/tracking1_1.png",runNumber));    canvas->SaveAs(Form("plots/%d/tracking1_1.pdf",runNumber));
//     c2->SaveAs(Form("plots/%d/tracking1_2.png",runNumber));
//     c2->SaveAs(Form("plots/%d/tracking1_2.pdf",runNumber));
//     c3->SaveAs(Form("plots/%d/tracking1_3.png",runNumber));
//     c3->SaveAs(Form("plots/%d/tracking1_3.pdf",runNumber));
//     c4->SaveAs(Form("plots/%d/tracking1_4.png",runNumber));
//     c4->SaveAs(Form("plots/%d/tracking1_4.pdf",runNumber));
// 
//     c5->SaveAs(Form("plots/%d/tracking1_5.png",runNumber));
//     c5->SaveAs(Form("plots/%d/tracking1_5.pdf",runNumber));
// 
//     c6->SaveAs(Form("plots/%d/tracking1_6.png",runNumber));
//     c6->SaveAs(Form("plots/%d/tracking1_6.pdf",runNumber));
// 

   return(0);
}
