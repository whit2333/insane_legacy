Int_t hms_elastics(Int_t aNumber = 3) {

   //gROOT->LoadMacro("$HOME/.root/load_style.cxx");
   //load_style("MultiSquarePlot"); 
   //gROOT->SetStyle("MultiSquarePlot");

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/elastic_temp.txt");  	
   //aman->BuildQueue2("lists/anusha_elastic_runs.txt");  	
   //aman->fRunQueue->push_back(72515);
   //aman->fRunQueue->push_back(72516);
   //aman->fRunQueue->push_back(72517);

   TChain * t =  new TChain("hmsElastic");// aman->BuildChain("hmsElastics");
   for(int i = 0; i< aman->fRunQueue->size();i++){
      std::cout << aman->fRunQueue->at(i) << std::endl;;
      t->Add(Form("data/rootfiles/hms%d.root",aman->fRunQueue->at(i)));
   }

   TChain * dt = aman->BuildChain("betaDetectors1");
   if(!dt) return -1;
   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetBranches(dt);
   events->SetClusterBranches(dt);
   dt->BuildIndex("fRunNumber","fEventNumber");


   Int_t           fRunNumber;
   Int_t           fEventNumber;
   t->SetBranchAddress("fRunNumber",&fRunNumber);
   t->SetBranchAddress("fEventNumber",&fEventNumber);
   // Declaration of leaf types
   Float_t         hcer_npe;
   Float_t         hsp;
   Float_t         hse;
   Float_t         charge;
   Float_t         hsdelta;
   Float_t         hstheta;
   Float_t         hsphi;
   Float_t         Fpp_w;
   Float_t         hszbeam;
   Float_t         hsdedx1;
   Float_t         hsbeta;
   Float_t         hsshtrk;
   Float_t         hsprtrk;
   Float_t         hsxfp;
   Float_t         hsyfp;
   Float_t         hsxpfp;
   Float_t         hsypfp;
   Float_t         hsxtar;
   Float_t         hsytar;
   Float_t         hsxptar;
   Float_t         hsyptar;
   Float_t         hstartme;
   Float_t         hsfptime;
   Float_t         eventid;
   Float_t         ev_type;
   Float_t         frast_y;
   Float_t         frast_x;
   Float_t         raw_srx;
   Float_t         raw_sry;
   Float_t         srast_y;
   Float_t         srast_x;
   Float_t         helicite;
   Float_t         betantrk;
   Float_t         dctothit;
   Float_t         cleantrk;
   Float_t         foundtrk;
   Float_t         sctothit;
   Float_t         scallhit;
   Float_t         scshould;
   Float_t         ch1hit;
   Float_t         ch2hit;
   Float_t         caletot;
   Float_t         dpel_bet;
   Float_t         dpel_hms;
   Float_t         X_hms;
   Float_t         Y_hms;
   Float_t         E_hms;
   Float_t         E_hms_th;
   Float_t         x_clust;
   Float_t         y_clust;
   Float_t         e_clust;
   Float_t         e_beta;    // electron energy calculated assuming elastic event (E' = E/(1 + 2E sin(theta/2) ))
   Float_t         hsp_beta;  // proton momentum from elastic electron beta
   Float_t         the_beta;  // proton angle from elastic electron  beta
   Float_t         hsp_hms;   // proton momentum from proton angle in hms
   Float_t         bthe_hms;  // electron theta from proton angle in hms 
   Float_t         btheta;    // best cluster theta angle in beta
   Float_t         bphi;      // best cluster phi angle in beta
   Float_t         Nn_delx;
   Float_t         Nn_dely;
   Float_t         Nn_dele;
   Float_t         xcal_b0;   // Expected bigcal position from HMS
   Float_t         ycal_b0;   // Expected bigcal position from HMS
   Float_t         xdiff_sh;  // some corrections I don't understand
   Float_t         ydiff_sh;  //
   Float_t         E_hms_me;  // HMS Eprime in MeV
   Float_t         nclust;
   Float_t         rawnclus;
   Float_t         hstubs;
   Float_t         de_hms;
   Float_t         W_etheta;
   Float_t         W_htheta;
   Float_t         Q2_pang;
   Float_t         etheta;
   Float_t         ephi;
   Float_t         T_trgbig;
   Float_t         T_trgbet;
   Float_t         Coin_tim;
   Float_t         inpoly;
   Float_t         Cer_ttes;
   Float_t         polarizn;
   Float_t         half_plt;
   Float_t         pp_hthet;
   Float_t         Q2_ehms;
   Float_t         qtheta;
   Float_t         qphi;

   t->SetBranchAddress("hcer_npe" , &hcer_npe); 
   t->SetBranchAddress("hsp"      , &hsp     ); 
   t->SetBranchAddress("hse"      , &hse     ); 
   t->SetBranchAddress("charge"   , &charge  ); 
   t->SetBranchAddress("hsdelta"  , &hsdelta ); 
   t->SetBranchAddress("hstheta"  , &hstheta ); 
   t->SetBranchAddress("hsphi"    , &hsphi   ); 
   t->SetBranchAddress("Fpp_w"    , &Fpp_w   ); 
   t->SetBranchAddress("hszbeam"  , &hszbeam ); 
   t->SetBranchAddress("hsdedx1"  , &hsdedx1 ); 
   t->SetBranchAddress("hsbeta"   , &hsbeta  ); 
   t->SetBranchAddress("hsshtrk"  , &hsshtrk ); 
   t->SetBranchAddress("hsprtrk"  , &hsprtrk ); 
   t->SetBranchAddress("hsxfp"    , &hsxfp   ); 
   t->SetBranchAddress("hsyfp"    , &hsyfp   ); 
   t->SetBranchAddress("hsxpfp"   , &hsxpfp  ); 
   t->SetBranchAddress("hsypfp"   , &hsypfp  ); 
   t->SetBranchAddress("hsxtar"   , &hsxtar  ); 
   t->SetBranchAddress("hsytar"   , &hsytar  ); 
   t->SetBranchAddress("hsxptar"  , &hsxptar ); 
   t->SetBranchAddress("hsyptar"  , &hsyptar ); 
   t->SetBranchAddress("hstartme" , &hstartme); 
   t->SetBranchAddress("hsfptime" , &hsfptime); 
   t->SetBranchAddress("eventid"  , &eventid ); 
   t->SetBranchAddress("ev_type"  , &ev_type ); 
   t->SetBranchAddress("frast_y"  , &frast_y ); 
   t->SetBranchAddress("frast_x"  , &frast_x ); 
   t->SetBranchAddress("raw_srx"  , &raw_srx ); 
   t->SetBranchAddress("raw_sry"  , &raw_sry ); 
   t->SetBranchAddress("srast_y"  , &srast_y ); 
   t->SetBranchAddress("srast_x"  , &srast_x ); 
   t->SetBranchAddress("helicite" , &helicite); 
   t->SetBranchAddress("betantrk" , &betantrk); 
   t->SetBranchAddress("dctothit" , &dctothit); 
   t->SetBranchAddress("cleantrk" , &cleantrk); 
   t->SetBranchAddress("foundtrk" , &foundtrk); 
   t->SetBranchAddress("sctothit" , &sctothit); 
   t->SetBranchAddress("scallhit" , &scallhit); 
   t->SetBranchAddress("scshould" , &scshould); 
   t->SetBranchAddress("ch1hit"   , &ch1hit  ); 
   t->SetBranchAddress("ch2hit"   , &ch2hit  ); 
   t->SetBranchAddress("caletot"  , &caletot ); 
   t->SetBranchAddress("dpel_bet" , &dpel_bet); 
   t->SetBranchAddress("dpel_hms" , &dpel_hms); 
   t->SetBranchAddress("X_hms"    , &X_hms   ); 
   t->SetBranchAddress("Y_hms"    , &Y_hms   ); 
   t->SetBranchAddress("E_hms"    , &E_hms   ); 
   t->SetBranchAddress("E_hms_th" , &E_hms_th); 
   t->SetBranchAddress("x_clust"  , &x_clust ); 
   t->SetBranchAddress("y_clust"  , &y_clust ); 
   t->SetBranchAddress("e_clust"  , &e_clust ); 
   t->SetBranchAddress("e_beta"   , &e_beta  );
   t->SetBranchAddress("hsp_beta" , &hsp_beta); 
   t->SetBranchAddress("the_beta" , &the_beta); 
   t->SetBranchAddress("hsp_hms"  , &hsp_hms ); 
   t->SetBranchAddress("bthe_hms" , &bthe_hms); 
   t->SetBranchAddress("btheta"   , &btheta  ); 
   t->SetBranchAddress("bphi"     , &bphi    ); 
   t->SetBranchAddress("Nn_delx"  , &Nn_delx ); 
   t->SetBranchAddress("Nn_dely"  , &Nn_dely ); 
   t->SetBranchAddress("Nn_dele"  , &Nn_dele ); 
   t->SetBranchAddress("xcal_b0"  , &xcal_b0 ); 
   t->SetBranchAddress("ycal_b0"  , &ycal_b0 ); 
   t->SetBranchAddress("xdiff_sh" , &xdiff_sh); 
   t->SetBranchAddress("ydiff_sh" , &ydiff_sh); 
   t->SetBranchAddress("E_hms_me" , &E_hms_me); 
   t->SetBranchAddress("nclust"   , &nclust  ); 
   t->SetBranchAddress("rawnclus" , &rawnclus); 
   t->SetBranchAddress("hstubs"   , &hstubs  ); 
   t->SetBranchAddress("de_hms"   , &de_hms  ); 
   t->SetBranchAddress("W_etheta" , &W_etheta); 
   t->SetBranchAddress("W_htheta" , &W_htheta); 
   t->SetBranchAddress("Q2_pang"  , &Q2_pang ); 
   t->SetBranchAddress("etheta"   , &etheta  ); 
   t->SetBranchAddress("ephi"     , &ephi    ); 
   t->SetBranchAddress("T_trgbig" , &T_trgbig); 
   t->SetBranchAddress("T_trgbet" , &T_trgbet); 
   t->SetBranchAddress("Coin_tim" , &Coin_tim); 
   t->SetBranchAddress("inpoly"   , &inpoly  ); 
   t->SetBranchAddress("Cer_ttes" , &Cer_ttes); 
   t->SetBranchAddress("polarizn" , &polarizn); 
   t->SetBranchAddress("half_plt" , &half_plt); 
   t->SetBranchAddress("pp_hthet" , &pp_hthet); 
   t->SetBranchAddress("Q2_ehms"  , &Q2_ehms ); 
   t->SetBranchAddress("qtheta"   , &qtheta  ); 
   t->SetBranchAddress("qphi"     , &qphi    ); 

   TFile * beta_coin_file = new TFile("data/beta_elastic_coin.root","UPDATE");
   beta_coin_file->cd();

   TTree * beta_coin_tree = dt->CloneTree(0);
   
   TH1F * hW_p = new TH1F("hW_p","W from p",100,0.5,2.5);
   TH1F * hW_e = new TH1F("hW_e","W from e",100,0.5,2.5);
   TH1F * hW_c = new TH1F("hW_c","W from cluster",100,0.5,2.5);

   TH1F * hTheta_e = new TH1F("hTheta_e","Theta e",50,15,55);
   TH1F * hPhi_e   = new TH1F("hPhi_e",  "Phi e",70,-180,360);
   TH1F * hTheta_hs = new TH1F("hTheta_hs","Theta e",50,15,55);
   TH1F * hPhi_hs   = new TH1F("hPhi_hs",  "Phi e",70,-180,360);
   TH1F * hTheta_b = new TH1F("hTheta_b","Theta b",50,15,55);
   TH1F * hPhi_b   = new TH1F("hPhi_b",  "Phi b",70,-180,360);

   TH1F * hEnergy_clust1 = new TH1F("hEnergy_clust1","E' from cluster",100,0.5,5.9);
   TH1F * hEnergy_hs = new TH1F("hEnergy_hs","E' from cluster",100,0.5,5.9);
   TH1F * hDeltaEnergy = new TH1F("hDeltaEnergy","E' from cluster",100,-0.9,0.9);

   TH1F * hDeltaX_clust1 = new TH1F("hDeltaX_clust1","#Delta X cluster",60,-60,60);
   TH1F * hDeltaX_clust2 = new TH1F("hDeltaX_clust2","#Delta X cluster",60,-60,60);

   TH1F * hDeltaY_clust1 = new TH1F("hDeltaY_clust1","#Delta Y cluster",120,-120,120);
   TH1F * hDeltaY_clust2 = new TH1F("hDeltaY_clust2","#Delta Y cluster",120,-120,120);

   TH1F * hDeltaTheta_e = new TH1F("hDeltaTheta_e","#Delta #theta e",50,-15,15);
   TH1F * hDeltaPhi_e   = new TH1F("hDeltaPhi_e",  "#Delta #phi e",90,-180,180);

   TH1F * hTheta_c = new TH1F("hTheta_c","Theta clust",50,25,55);
   TH1F * hPhi_c   = new TH1F("hPhi_c",  "Phi clust",70,-180,360);

   TH2F * hXY1 = new TH2F("hXY1","Y vs X",60,-60,60,120,-120,120);
   TH2F * hXY2 = new TH2F("hXY2","Y vs X",60,-60,60,120,-120,120);

   BIGCALCluster * aClust = 0;


   // -------------------------------------------------------
   // Event Loop
   for(Int_t ievent = 0; ievent < t->GetEntries(); ievent++){

      t->GetEntry(ievent);
      //std::cout << fRunNumber << " - " << fEventNumber << std::endl;

      if( hse<5 && hcer_npe < 2.0 )
        if( TMath::Abs(hstheta-0.39)<0.2 )
           if( TMath::Abs(dpel_bet)<3 ) 
           if( TMath::Abs(hszbeam)<3.0 ) 
              if( TMath::Abs(X_hms)<70 ) {
                 hW_e->Fill(W_etheta);
                 hW_p->Fill(W_htheta);
                 if( TMath::Abs(W_htheta-0.938) < 0.4 ) {

      Int_t bytes_read = dt->GetEntryWithIndex(fRunNumber,fEventNumber);
      if( bytes_read == 0 ) {
         std::cout << "run-event: " << fRunNumber << "-" << fEventNumber << " not found in beta chain. " << std::endl;
         continue;
      }
      beta_coin_tree->Fill();
      std::cout << events->TRIG->fRunNumber << " - event: " << events->TRIG->fEventNumber << std::endl;
      for(int k = 0; k< events->CLUSTER->fClusters->GetEntries(); k++){
         aClust = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[k];
         if(aClust->fIsGood){

            Double_t xclust  = (aClust->GetXmoment() + aClust->fDeltaX);
            Double_t yclust  = (aClust->GetYmoment() + aClust->fDeltaY);
            Double_t theta_clust  = (aClust->GetTheta() + aClust->fDeltaTheta);
            Double_t phi_clust    = (aClust->GetPhi() + aClust->fDeltaPhi);
            Double_t energy_clust = aClust->GetCorrectedEnergy()/1000.0;
            Double_t thetap = hstheta;
            Double_t E0 = 5.89;
            Double_t energy_from_Eproton = E0 + M_p/GeV - ((M_p/GeV)*(2*E0*(M_p/GeV) + TMath::Power((M_p/GeV),2) + 
                             TMath::Power(E0,2)*(1 + TMath::Power(TMath::Cos(thetap),2))))/
                   ((E0 + (M_p/GeV) - E0*TMath::Cos(thetap))*(E0 + (M_p/GeV) + E0*TMath::Cos(thetap)));
            if(energy_clust>2.0) {

            hEnergy_clust1->Fill(energy_clust);
            hEnergy_hs->Fill(hse);

            hDeltaX_clust1->Fill(xclust-X_hms);
            hDeltaX_clust2->Fill(x_clust-X_hms);
            hDeltaY_clust1->Fill(yclust-X_hms);
            hDeltaY_clust2->Fill(y_clust-X_hms);

            hDeltaEnergy->Fill(energy_clust - energy_from_Eproton);

            //std::cout << aClust->fDeltaTheta/degree << ", " << aClust->fDeltaPhi/degree << ", " << aClust->fDeltaE << std::endl;
            //aClust->Dump();
            //std::cout << " beam: " << events->BEAM->fBeamEnergy << std::endl;
            Double_t W_clust      = InSANE::Kine::W_EEprimeTheta(5.89,energy_clust,theta_clust); 

            hTheta_c->Fill(theta_clust/degree);
            hPhi_c->Fill(phi_clust/degree);
            hW_c->Fill(W_clust);

            hDeltaTheta_e->Fill((theta_clust - etheta)/degree);
            hDeltaPhi_e->Fill((phi_clust - ephi)/degree);
            }
         }
      }
                    hXY1->Fill(X_hms,Y_hms);
                    hTheta_e->Fill(etheta/degree);
                    hPhi_e->Fill(ephi/degree);
                    hTheta_b->Fill(btheta/degree);
                    hPhi_b->Fill(bphi/degree);
                    hTheta_hs->Fill(hstheta/degree);
                    hPhi_hs->Fill(hsphi/degree);
                 }
              }
   }

   beta_coin_tree->FlushBaskets();
   beta_coin_file->Flush();

   TCanvas * c = new TCanvas("HMSElastics","HMS-BETA Elastics");
   c->Divide(3,3);
   TLegend * leg = 0;

   c->cd(1);
   hW_p->SetLineColor(2);
   hW_c->SetLineColor(1);
   hW_c->SetLineWidth(2);
   hW_p->Draw();
   hW_e->Draw("same");
   hW_c->Draw("same");

   leg = new TLegend(0.7,0.7,0.88,0.88);
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->AddEntry(hW_e,"W e","l");
   leg->AddEntry(hW_p,"W p","l");
   leg->AddEntry(hW_c,"W cluster","l");
   leg->Draw();


   c->cd(2);
   hXY1->Draw("colz");

   c->cd(4);
   hTheta_b->SetLineColor(2);
   hTheta_hs->SetLineColor(4);
   hTheta_c->SetLineColor(1);
   hTheta_c->SetLineWidth(2);
   hTheta_hs->Draw();
   hTheta_e->Draw("same");
   hTheta_b->Draw("same");
   hTheta_c->Draw("same");

   leg = new TLegend(0.7,0.7,0.88,0.88);
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->AddEntry(hTheta_e,"#theta e","l");
   leg->AddEntry(hTheta_b,"#theta b?","l");
   leg->AddEntry(hTheta_hs,"#theta HMS","l");
   leg->AddEntry(hTheta_c,"#theta Cluster","l");
   leg->Draw();

   c->cd(5);
   hPhi_e->Draw();
   hPhi_b->SetLineColor(2);
   hPhi_b->Draw("same");
   hPhi_hs->SetLineColor(4);
   hPhi_hs->Draw("same");
   hPhi_c->SetLineColor(1);
   hPhi_c->SetLineWidth(2);
   hPhi_c->Draw("same");

   leg = new TLegend(0.7,0.7,0.88,0.88);
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->AddEntry(hPhi_e, "#phi e","l");
   leg->AddEntry(hPhi_b, "#phi b?","l");
   leg->AddEntry(hPhi_hs,"#phi HMS","l");
   leg->AddEntry(hPhi_c, "#phi Cluster","l");
   leg->Draw();

   c->cd(7);
   hDeltaTheta_e->SetLineColor(1);
   hDeltaTheta_e->Draw();

   c->cd(8);
   hDeltaPhi_e->SetLineColor(1);
   hDeltaPhi_e->Draw();

   c->cd(3);
   hDeltaX_clust1->Draw();
   hDeltaX_clust2->SetLineColor(2);
   hDeltaX_clust2->Draw("same");

   c->cd(6);
   hDeltaY_clust1->Draw();
   hDeltaY_clust2->SetLineColor(2);
   hDeltaY_clust2->Draw("same");

   c->cd(9); 
   hDeltaEnergy->Draw();
   //hEnergy_clust1->Draw();
   //hEnergy_hs->SetLineColor(2);
   //hEnergy_hs->Draw("same");

   beta_coin_file->Write();
   c->SaveAs(Form("results/hms_elastics_%d.png",aNumber));

   //dt->StartViewer();

   return 0;
}
