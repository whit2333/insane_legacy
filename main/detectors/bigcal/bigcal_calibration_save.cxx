Int_t bigcal_calibration_save(Int_t series = 2){

   InSANEDetectorCalibration * calib = 0;
   InSANECalibration * chanCalib     = 0;
   TParameter<double>  * aPar        = 0;;

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   bcgeo->SetCalibrationSeries(series);

   TFile * f              = new TFile("data/BigCalCalibrations.root","UPDATE");
   const char * calibname = Form("BigCal-Calibration-%d",series);
   calib                  = new InSANEDetectorCalibration(calibname,"BigCal Energy Calibration");

   for(int i=0;i<1744;i++){
      chanCalib = new InSANECalibration(Form("BigCal-block-%d",i+1),Form("BigCal-block-%d",i+1));

      aPar      = new TParameter<double>(Form("BigCal-coef-%d",i+1),bcgeo->bc_cc[i]);
      //      aPar->SetVal(bcgeo->bc_cc[i]);
      chanCalib->AddParameter(aPar);
      chanCalib->GetFunctionsi()->Clear();
      chanCalib->SetChannel(i+1);
      calib->AddCalibration(chanCalib);
   }

   calib->Write(Form("BigCal-Calibration-%d",series), TObject::kWriteDelete);

   return(0);
}

