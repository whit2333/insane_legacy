Int_t gain_match(Int_t runNumber = 72948) {

   Int_t NEvents_max = 6e6;

   rman->SetRun(runNumber);
   SANEEvents * events = new SANEEvents("betaDetectors1");

   TTree * t = events->fTree;
   if(!t) return -1;
   Int_t nEntries = t->GetEntries();
   
   // ------------------------
   // Histograms
   TList * fHistograms = new TList();
   //std::vector<TH1F*> fHists;
   for(int i = 0; i< 1744 ; i++ ){
      TH1F * hist = new TH1F(Form("BCADC%d",i+1),Form("Bigcal ADC %d",i+1),100,0,3000);
      fHistograms->Add(hist);
      //fHists.push_back(hist);
   }

   // ------------------------
   // Event loop
   BigcalEvent * bcEvent = 0; 
   TH1F        * h1      = 0;

   for(int iEvent = 0 ; iEvent < nEntries && iEvent < NEvents_max; iEvent++) {

      if(iEvent%10000==0) {
         std::cout << iEvent << std::endl;
      }

      t->GetEntry(iEvent);
      if( events->TRIG->IsBETA2Event() ) {

         bcEvent = events->BETA->fBigcalEvent;

      for(int j = 0; j < events->BETA->fBigcalEvent->fBigcalHits->GetEntries(); j++){

            BigcalHit *  ahit = (BigcalHit*)(*(events->BETA->fBigcalEvent->fBigcalHits))[j];

            //h1 = fHists[ahit->fBlockNumber-1];
            h1 = (TH1F*)fHistograms->At(ahit->fBlockNumber-1);
            if(h1) {
               h1->Fill(ahit->fADC);
            }

         }
      }
   }

   TCanvas * c = new TCanvas();
   gPad->SetLogy(true);
   h1 =  (TH1F*)fHistograms->At(32*7+12);
   h1->Draw();
   c->SaveAs("results/detectors/bigcal/gain_match.png");

   TFile * fout = new TFile(Form("data/bigcal/gain_match%d.root",runNumber),"UPDATE");
   fout->WriteObject(fHistograms,"histograms");

   return 0;
}
