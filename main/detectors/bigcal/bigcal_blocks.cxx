Int_t bigcal_blocks(Int_t runnumber = 601) {
   rman->SetRun(runnumber);

   SANEEvents * events = new SANEEvents("betaDetectors1");
   if(!events->fTree) return(-1);
   events->SetClusterBranches(events->fTree);
   TTree * t = events->fTree;

   TCanvas * c = new TCanvas("bigcal_blocks","bigcal_blocks",600,600);
   c->Divide(2,2);

   c->cd(1);
   t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>BCcluster0(32,1,33,56,1,57)","","colz");

   c->cd(2);
   t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>BCcluster1(32,1,33,56,1,57)","bigcalClusters.fIsNoisyChannel","colz");

   c->cd(3);
   t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>BCcluster2(32,1,33,56,1,57)","!bigcalClusters.fIsNoisyChannel","colz");

   c->cd(4);
   t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>BCcluster4(32,1,33,56,1,57)","bigcalClusters.fIsGood","colz");

   c->SaveAs(Form("plots/%d/bigcal_blocks.png",runnumber));

   //t->StartViewer();
   return(0);
}
