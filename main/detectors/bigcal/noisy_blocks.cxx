Int_t noisy_blocks(Int_t runnumber = 72425,Int_t col = 12) {
   Double_t sensitivity = 4.0; // ratio peak/mean value which is then flagged
   rman->SetRun(runnumber);

   SANEEvents * events = new SANEEvents("betaDetectors1");
   if(!events->fTree) return(-1);
   events->SetClusterBranches(events->fTree);
   TTree * t = events->fTree;

   TCanvas * c = new TCanvas("bigcal_blocks","bigcal_blocks",600,600);
   TH1D *h1 = 0;
   TH1D *h1 = 0;
   //c->Divide(2,2);

   TCut columncut = "triggerEvent.IsBETA2Event()";
   //TCut columncut = Form("triggerEvent.IsBETA2Event()&&bigcalClusters.fiPeak==%d",col);
   TCut goodcut = "!bigcalClusters.fIsNoisyChannel";
   TCut goodcut2 = "bigcalClusters.fIsGood";
   TCut cer       = "TMath::Abs(bigcalClusters.fCherenkovTDC)<5";
   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   //c->cd(1);
   //t->Draw("bigcalClusters.fjPeak>>BCcluster2(56,1,57)",/*goodcut&&*/columncut,"goff",500000);
   
   // --------------------------------------------------------------
   t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>BCcluster2D(32,1,33,56,1,57)",/*goodcut&&*/columncut,"goff",500000);
   h2 = (TH2D*)gROOT->FindObject("BCcluster2D");

   for(int icol = 1;icol<=32;icol++){
      leg->SetHeader(Form("column %d",icol));
      h1 = h2->ProjectionY(Form("COL%d_py",icol),icol,icol);
      if(h1){
         leg->AddEntry(h1,"not noisy","l");
         h1->SetLineColor(2);
         h1->SetFillStyle(3004);
         h1->SetFillColor(2);
         std::cout << h1->GetMean(1)<<std::endl;;
         std::cout << h1->GetMean(2)<<std::endl;;
         Double_t mean = h1->GetMean(2);
         Double_t mean2 = 0;
         for(int i = 0; i<h1->GetNbinsX() ; i++){
            Double_t yi = h1->GetBinContent(i);
            mean2 += yi;
         }
         std::ofstream outfile(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber),std::ios_base::app);
         if(icol < 31) mean2 = mean2/float(56);
         else mean2 = mean2/float(32);
         for(int i = 0; i<h1->GetNbinsX() ; i++){
            Double_t yi = h1->GetBinContent(i);
            if(yi/mean2 > sensitivity ) {
               std::cout << icol << " " << i << " is bad" << std::endl;
               int blocknum = bcgeo->GetBlockNumber(icol,i);
               if(blocknum != -1) 
                  outfile << blocknum << std::endl;
            }
         }
         h1->Draw();
         std::cout << " mean is " << mean2 << std::endl;
      }
      //t->Draw("bigcalClusters.fjPeak>>BCcluster0(56,1,57)",columncut,"same");
      //h1 = (TH1F*)gROOT->FindObject("BCcluster0");
      //   leg->AddEntry(h1,"no cuts","l");
      //t->Draw("bigcalClusters.fjPeak>>BCcluster3(56,1,57)",cer&&columncut,"goff");
      //h1 = (TH1F*)gROOT->FindObject("BCcluster3");
      //if(h1){
      //   leg->AddEntry(h1,"cer ","l");
      //   h1->SetLineColor(3);
      //   h1->SetLineWidth(2);
      //   h1->Draw("same");
      //}
      //t->Draw("bigcalClusters.fjPeak>>BCcluster4(56,1,57)",goodcut&&cer&&columncut,"goff");
      //h1 = (TH1F*)gROOT->FindObject("BCcluster4");
      //if(h1){
      //   leg->AddEntry(h1,"not noisy, cer ","l");
      //   h1->SetLineColor(4);
      //   h1->Draw("same");
      //}
      //t->Draw("bigcalClusters.fjPeak>>BCcluster5(56,1,57)",goodcut&&goodcut2&&cer&&columncut,"goff");
      //h1 = (TH1F*)gROOT->FindObject("BCcluster5");
      //if(h1){
      //   leg->AddEntry(h1,"not noisy,good, cer ","l");
      //   h1->SetLineColor(6);
      //   h1->Draw("same");
      //}

      //jleg->Draw();
      gPad->Modified(true);

      //c->cd(2);
      //t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>BCcluster1(32,1,33,56,1,57)","bigcalClusters.fIsNoisyChannel","colz");

      //c->cd(3);
      //t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>BCcluster2(32,1,33,56,1,57)","!bigcalClusters.fIsNoisyChannel","colz");

      //c->cd(4);
      //t->Draw("bigcalClusters.fjPeak:bigcalClusters.fiPeak>>BCcluster4(32,1,33,56,1,57)","bigcalClusters.fIsGood","colz");

      c->SaveAs(Form("plots/%d/noisy_blocks_col%d.png",runnumber,icol));
   }

   //t->StartViewer();
   return(0);
}
