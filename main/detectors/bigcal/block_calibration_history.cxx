Int_t block_calibration_history(Int_t ib = 27, Int_t jb = 34){

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   InSANEDatabaseManager    * dbman = InSANEDatabaseManager::GetManager();

   Int_t        blockNumber = bcgeo->GetBlockNumber(ib,jb);
   TSQLServer * conn        = dbman->GetMySQLConnection();
   TSQLResult * res         = 0;
   TSQLRow    * row         = 0;

   std::vector<double> calib_series;
   std::vector<double> calib_coeff;

   TString  sql = "SELECT coeff,series FROM bigcal_calibrations WHERE block_number=";
   sql += Form("%d",blockNumber);
   sql += " ; ";
   if(conn){
      res = conn->Query(sql.Data());
      if(res){
         int nrows = res->GetRowCount();
         for(int i = 0; i< nrows; i++){
            row = res->Next();
            calib_coeff.push_back(atof(row->GetField(0)));
            calib_series.push_back(atof(row->GetField(1)));
            delete row;
         } 
         delete res;
      }

   }

   TCanvas * c = new TCanvas("block_calibration_history","block");
   TGraph * gr = 0;

   gr = new TGraph(calib_series.size(),&calib_series[0],&calib_coeff[0]);
   gr->SetMarkerStyle(20);
   gr->Draw("alp");


   c->SaveAs(Form("results/bigcal_calibration/block_calibration_history%d_%d_%d.png",blockNumber,ib,jb));

   return 0;
}
