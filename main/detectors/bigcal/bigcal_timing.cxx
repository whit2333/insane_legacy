/*!  Examine Bigcal Timing

       - 

 */
Int_t bigcal_timing(Int_t runnumber = 72999) {

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();
   
   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   TTree * t = events->fTree;
   if(!t) return(-1);



   TCanvas * c1 = new TCanvas("BigcalTiming","Bigcal Timing groups");
   c1->Divide(2,2);
   
   c1->cd(1);
   t->Draw("fBigcalTimingGroupHits.fTDCRow:fBigcalTimingGroupHits.fTDC","","col");
   
   c1->cd(2);
   t->Draw("fBigcalTimingGroupHits.fTDCRow:fBigcalTimingGroupHits.fTDCAlign",
           "TMath::Abs(fBigcalTimingGroupHits.fTDCAlign+20)<50",
           "col");

   c1->cd(3);
   t->Draw("fBigcalTimingGroupHits.fTDCGroup:fBigcalTimingGroupHits.fTDCAlign",
           "TMath::Abs(fBigcalTimingGroupHits.fTDCAlign)<100",
           "col");

   c1->cd(4);
   t->Draw("fBigcalTimingGroupHits.fTDCRow:fBigcalTimingGroupHits.fTDCGroup","","col");
   
   //t->StartViewer();


   c1->SaveAs(Form("plots/%d/bigcal_timing1.png",runnumber));
   c1->SaveAs(Form("plots/%d/bigcal_timing1.pdf",runnumber));


   TCanvas * c2 = new TCanvas("BigcalTriggerGroups","Bigcal Trigger groups");
   c2->Divide(2,2);
   
   c2->cd(1);
   t->Draw("fBigcalTriggerGroupHits.fTDCRow:fBigcalTriggerGroupHits.fTDC","","col");
   
   c2->cd(2);
   t->Draw("fBigcalTriggerGroupHits.fTDCRow:fBigcalTriggerGroupHits.fTDCAlign","","col");
   
   c2->cd(3);
   t->Draw("fBigcalTriggerGroupHits.fTDCRow:fBigcalTriggerGroupHits.fTDCGroup","","col");
   
   c2->cd(4);
   t->Draw("fBigcalTriggerGroupHits.fTDCAlign","","");
   
   //t->StartViewer();


   c1->SaveAs(Form("plots/%d/bigcal_timing1.png",runnumber));
   c1->SaveAs(Form("plots/%d/bigcal_timing1.pdf",runnumber));
   c2->SaveAs(Form("plots/%d/bigcal_timing2.png",runnumber));
   c2->SaveAs(Form("plots/%d/bigcal_timing2.pdf",runnumber));


   return(0);
}
