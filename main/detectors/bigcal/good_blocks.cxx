Int_t good_blocks(Int_t runnumber = 72425 , Int_t fileVersion = 11, Double_t E_min  = 1200.0) {

   bool writeRun = false;
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun * run = rman->GetCurrentRun();
   InSANERunSummary rSum =  run->GetRunSummary();

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   bcgeo->LoadBadBlocks(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber));

   TTree * t = (TTree*) gROOT->FindObject("Tracks"); 
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);
   //SANEEvents * events = new SANEEvents("betaDetectors1");
   //events->fTree->BuildIndex("fRunNumber","fEventNumber");

   Int_t ev      = 0;
   Int_t runnum  = 0;
   Int_t stdcut  = 0;
   TH1F * h1     = 0;
   TH2F * h2     = 0;

   TTree * selectt = new TTree("cuts","The cuts");
   selectt->Branch("ev",&ev,"ev/I");
   selectt->Branch("runnum",&runnum,"runnum/I");
   selectt->Branch("stdcut",&stdcut,"stdcut/I");
   bool passed = false;

   ///std::cout << " Found the tree!\n";

   Double_t theta_range[2] = {0.4,1.0};
   Double_t phi_range[2] = {-0.6,1.0};
   Double_t energy_range[2] = {500,4500};

   TH2F * hBCYvsBCX = new TH2F("hBCYvsBCX","good blocks;x_{clust};y_{clust}", 32,1,33, 56, 1 ,57);
   TH2F * hBCYvsx = new TH2F("hBCYvsx","good blocks;x;y_{clust}", 100,0,1, 56, 1 ,57);
   TH2F * hBCXvsx = new TH2F("hBCXvsx","good blocsk;x;x_{clust}", 100,0,1, 32, 1 ,33 );
   TH2F * hBCXvsx2 = new TH2F("hBCXvsx2","good blocsk;x_{clust};x", 32, 1 ,33 , 100,0,1);

   Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t QsqAvg[4];
   for(int i=0;i<4;i++){
      QsqAvg[i] = (QsqMax[i]+QsqMin[i])/2.0;
   }

   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      t->GetEntry(iEvent);
      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;

      dis->fRunNumber         = traj->fRunNumber;
      runnum                  = traj->fRunNumber;
      fDisEvent->fEventNumber = traj->fEventNumber;
      ev                      = traj->fEventNumber;
      stdcut                  = 0;
      dis->fHelicity          = traj->fHelicity;
      dis->fx                 = traj->GetBjorkenX();
      dis->fW                 = TMath::Sqrt(traj->GetInvariantMassSquared())/1000.0;
      dis->fQ2                = traj->GetQSquared()/1000000.0;
      dis->fTheta             = traj->GetTheta();
      dis->fPhi               = traj->GetPhi();
      dis->fEnergy            = traj->GetEnergy()/1000.0;
      dis->fBeamEnergy        = traj->fBeamEnergy/1000.0;
      //dis->fGroup             = 0;//traj->fSubDetector;
      //dis->fIsGood            = false;
      //dis->fIsGood            = traj->fIsGood;
      Int_t ibl = bcgeo->GetBlockNumber(traj->fCluster.fiPeak,traj->fCluster.fjPeak);

      if( dis->fW > 1.0 )
         if( trig->IsBETA2Event() )   
            if( !traj->fIsNoisyChannel)
               if( traj->fIsGood) 
                  if( traj->fNCherenkovElectrons > 0.6 && traj->fNCherenkovElectrons < 1.5 )
                  if( !(bcgeo->IsBadBlock(ibl)) )
                     if( traj->fEnergy > E_min ) {

                        // Different timing cuts depending on the run
                        // Here the timing is set by bigcal
                        if( runnumber <= 72488 ){
                           if( (TMath::Abs(traj->fCluster.fClusterTime1) < 3 ) || (TMath::Abs(traj->fCluster.fClusterTime2) < 3 ) ) 
                              if(TMath::Abs(traj->fCherenkovTDC) < 200){ 

                                 hBCXvsx->Fill(dis->fx,traj->fCluster.fiPeak);
                                 hBCYvsx->Fill(dis->fx,traj->fCluster.fjPeak);
                                 hBCXvsx2->Fill(traj->fCluster.fiPeak,dis->fx);
                                 hBCYvsBCX->Fill(traj->fCluster.fiPeak,traj->fCluster.fjPeak);

                              }
                           // Here the timing is set by the cherenkov 
                        } else if( runnumber > 72488 ) {
                           if(TMath::Abs(traj->fCherenkovTDC) < 10){ 

                              hBCXvsx->Fill(dis->fx,traj->fCluster.fiPeak);
                              hBCYvsx->Fill(dis->fx,traj->fCluster.fjPeak);
                              hBCXvsx2->Fill(traj->fCluster.fiPeak,dis->fx);
                              hBCYvsBCX->Fill(traj->fCluster.fiPeak,traj->fCluster.fjPeak);

                           }
                        }
                     }
   }

   TCanvas * c = new TCanvas("good_blocks","good_blocks");
   c->Divide(2,2);

   c->cd(1);
   hBCYvsx->Draw("colz");

   c->cd(2);
   hBCYvsBCX->Draw("colz");

   c->cd(3);
   hBCXvsx->Draw("colz");

   c->cd(4);
   hBCXvsx2->Draw("colz");

   c->SaveAs(Form("plots/%d/good_blocks.png",runnumber));
   return(0);
}
