/*! Alignment correction for track "miss distance".
    Since the "miss distance" is calculated by drawing a straight line from
    the cluster to the target and projected onto the detector (either
    hodoscope or tracker) using no cross calibration. It just uses the 
    detector geometry information. This script finds the correction necessary
    to remove any position dependence.

    \todo See if this correction can be minimized/understood by making more
    detailed geometry corrections.   
  
 */ 
Int_t track_align4(Int_t runNumber=72995) {

	rman->SetRun(runNumber);

	TTree * t = (TTree *) gROOT->FindObject("Tracks");
	if(!t) return(-1);
	TH1F * h1 = 0;
	TH2F * h2 = 0;

	TProfile * p1 = 0;
	
	TF1 * fx = new TF1("xtrack_raster_correction","[0]+[1]*x",-100,100);
        fx->SetParameter(0,0.0);
        fx->SetParameter(1,0.0);
        TF1 * fy = new TF1("ytrack_raster_correction","[0]+[1]*x",-100,100);
        fy->SetParameter(0,0.5); //0.5
        fy->SetParameter(1,1.0); //0.9
        TF1 * fz = new TF1("ztrack_raster_correction","[0]+[1]*x",-100,100);
        fz->SetParameter(0,0.0);
        fz->SetParameter(1,0.0);
        TF1 * fy2 = new TF1("ytrack_align","[0]+[1]*x",-100,100);
	
	TFitResultPtr XfitResult;
	TFitResultPtr YfitResult;
	TFitResultPtr ZfitResult;

	TCanvas * c = new TCanvas("track_align4","track_align4");
	c->Divide(3,3);
	
	c->cd(1);
        t->Draw("fTrackerMiss.fY:fTargetPosition.fX>>hTrackerMissVTargX(100,-2,2,100,-2,2)",
	        "fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(2);
        t->Draw("fTrackerMiss.fY:fTargetPosition.fY>>hTrackerMissVTargY(100,-2,2,100,-2,2)",
         	"fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");
	c->cd(3);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY:fTargetPosition.fY>>hTrackerMissVTargYCorrected(100,-2,2,100,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1)),
	        "fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5",
		"colz");
// 	c->cd(3);
//         t->Draw("fTrackerMiss.fZ:fTargetPosition.fZ>>hTrackerMissVbcZ(50,230,280,100,-5,5)",
// 		"fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

// 	c->cd(4);
//         t->Draw("fTrackerMiss.fX:fPosition0.fX>>hTrackerMissVbcX(50,160,260,100,-5,5)",
// 	        "fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(4);
        t->Draw(Form("fTrackerMiss.fY:fPosition0.fY>>hTrackerMissVbcY1(50,-120,120,100,-3,3)"),
         	"fEnergy>1000.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(5);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY:fPosition0.fY>>hTrackerMissVbcY(100,-120,120,60,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1)),
         	"fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");
        c->cd(6);
        h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcY");
	if(h2){
		p1 = h2->ProfileX();
	        if(p1) {
			p1->Draw();
		        YfitResult = p1->Fit(fy2,"S,E,M","same",-60,60);
			fy2->SetFitResult(*(YfitResult.Get()));
		}
	}

	c->cd(7);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYCorrected1(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5&&TMath::Abs(fPosition0.fY)<50",
		"colz");

	c->cd(8);
/*	gPad->SetLogz(true);*/
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYCorrected2(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>1.5&&fNCherenkovElectrons<2.5",
		"colz");

	c->cd(9);
/*	gPad->SetLogz(true);*/
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYCorrected3(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel",
		"colz");


   TF1 * fit = new TF1("doublegaus","gaus(0)+gaus(3)",-2,2);
   fit->SetParameter(0,800);
   fit->SetParameter(1,0.1);
   fit->SetParLimits(1,0.05,0.3);
   fit->SetParameter(2,0.3);
   fit->SetParameter(3,800);
   fit->SetParameter(4,-0.5);
   fit->SetParLimits(4,-0.6,-0.49);
   fit->SetParameter(5,0.05);
   fit->SetParLimits(5,0.001,0.2);

   t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  >>hMissWithEnergyCut(100,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1) ),
		Form("!fIsNoisyChannel&&fNCherenkovElectrons>1.5&&fNCherenkovElectrons<2.5\
                     &&fEnergy>%d &&fEnergy<%d ",600,1200),
		"goff");
   t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  >>hMissWithEnergyCut00(100,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1) ),
		Form("!fIsNoisyChannel&&fNCherenkovElectrons>1.5&&fNCherenkovElectrons<2.5\
                     &&fEnergy>%d &&fEnergy<%d &&fLuciteMissDistance<20",600,1200),
		"goff");

   for(int i = 0;i<6;i++){
/*	gPad->SetLogz(true);*/
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  >>hMissWithEnergyCut%d(100,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1) ,i),
		Form("!fIsNoisyChannel&&fNCherenkovElectrons>1.5&&fNCherenkovElectrons<2.5\
                     &&fEnergy>%d &&fEnergy<%d",600+200*i,600+200*(i+1)),
		"goff");
        
   }

   TCanvas * c2 = new TCanvas("track_align4_2","track_align4_2");
   c2->Divide(2,2);
   c2->cd(3);
   t->Draw(Form("fLuciteMissDistance>>lucitemiss(100,0,200)" ),
		Form("!fIsNoisyChannel&&fNCherenkovElectrons>1.5&&fNCherenkovElectrons<2.5\
                     &&fEnergy>%d &&fEnergy<%d ",600,1200),
		"");
   c2->cd(1);
   h1 = (TH1F*) gROOT->FindObject("hMissWithEnergyCut");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw();
/*      h1->Fit(fit,"S,E,M","same",-1.1,1.1);*/
   }
   h1 = (TH1F*) gROOT->FindObject("hMissWithEnergyCut00");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw("same");
/*      h1->Fit(fit,"S,E,M","same",-1.1,1.1);*/
   }

   c2->cd(2);
   h1 = (TH1F*) gROOT->FindObject("hMissWithEnergyCut1");
   if(h1) {
      h1->SetLineColor(2);
      h1->Draw();
   }
   h1 = (TH1F*) gROOT->FindObject("hMissWithEnergyCut0");
   if(h1) {
      h1->SetLineColor(1);
      h1->Draw("same");
   }
   h1 = (TH1F*) gROOT->FindObject("hMissWithEnergyCut2");
   if(h1) {
      h1->SetLineColor(3);
      h1->Draw("same");
   }
   h1 = (TH1F*) gROOT->FindObject("hMissWithEnergyCut3");
   if(h1) {
      h1->SetLineColor(4);
      h1->Draw("same");
   }
   h1 = (TH1F*) gROOT->FindObject("hMissWithEnergyCut4");
   if(h1) {
      h1->SetLineColor(6);
      h1->Draw("same");
   }
   h1 = (TH1F*) gROOT->FindObject("hMissWithEnergyCut5");
   if(h1) {
      h1->SetLineColor(7);
      h1->Draw("same");
   }
// 	c->cd(12);//         t->Draw(Form("fTrackerMiss.Mag():fEnergy>>hTrackerMissVbcEMagCorrected(100,0,2000,50,-5,5)",ZfitResult->Parameter(0),ZfitResult->Parameter(1)),
// 		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5",
// 		"colz");

// 	rman->GetCurrentFile()->cd("calibrations/tracker");
// 	
// 	InSANECalibration * xCal = new InSANECalibration("x_track_align","x track align");
// 	xCal->Clear();
// 	xCal->AddFunction(fx);
//         h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcXCorrected");
// 	xCal->AddHistogram(h2);
// 
// 	InSANECalibration * yCal = new InSANECalibration("y_track_align","y track align");
// 	yCal->Clear();
// 	yCal->AddFunction(fy);
//         h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcYCorrected");
// 	yCal->AddHistogram(h2);
// 
// 	InSANECalibration * zCal = new InSANECalibration("z_track_align","z track align");
// 	zCal->Clear();
// 	zCal->AddFunction(fz);
//         h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcZCorrected");
// 	zCal->AddHistogram(h2);
// 
// 
// 	xCal->Print();
// 	yCal->Print();
// 	zCal->Print();
// 
//         xCal->Write(xCal->GetName(), TObject::kWriteDelete);
//         yCal->Write(yCal->GetName(), TObject::kWriteDelete);
//         zCal->Write(zCal->GetName(), TObject::kWriteDelete);

	c->SaveAs(Form("plots/%d/track_align4.png",runNumber));
	c2->SaveAs(Form("plots/%d/track_align4_2.png",runNumber));

	return(0);
}
