/*! Alignment correction for track "miss distance".
    Since the "miss distance" is calculated by drawing a straight line from
    the cluster to the target and projected onto the detector (either
    hodoscope or tracker) using no cross calibration. It just uses the 
    detector geometry information. This script finds the correction necessary
    to remove any position dependence.

    \todo See if this correction can be minimized/understood by making more
    detailed geometry corrections.   
  
 */ 
Int_t track_align5(Int_t runNumber=72995) {

	rman->SetRun(runNumber);

	TTree * t = (TTree *) gROOT->FindObject("Tracks");
	if(!t) return(-1);
	TH1F * h1 = 0;
	TH2F * h2 = 0;

	TProfile * p1 = 0;
	
	TF1 * fx = new TF1("xtrack_raster_correction","[0]+[1]*x",-100,100);
        fx->SetParameter(0,0.0);
        fx->SetParameter(1,0.0);
        TF1 * fy = new TF1("ytrack_raster_correction","[0]+[1]*x",-100,100);
        fy->SetParameter(0,0.3); //0.5
        fy->SetParameter(1,1.0); //0.9
        TF1 * fz = new TF1("ztrack_raster_correction","[0]+[1]*x",-100,100);
        fz->SetParameter(0,0.0);
        fz->SetParameter(1,0.0);
        TF1 * fy2 = new TF1("ytrack_align","[0]+[1]*x",-100,100);
	
	TFitResultPtr XfitResult;
	TFitResultPtr YfitResult;
	TFitResultPtr ZfitResult;

	TCanvas * c = new TCanvas("track_align5","track_align5");
	c->Divide(3,3);
	
	c->cd(1);
        t->Draw("fTrackerMiss.fY:fTargetPosition.fX>>hTrackerMissVTargX(100,-2,2,100,-2,2)",
	        "fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(2);
        t->Draw("fTrackerMiss.fY:fTargetPosition.fY>>hTrackerMissVTargY(100,-2,2,100,-2,2)",
         	"fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");
	c->cd(3);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY:fTargetPosition.fY>>hTrackerMissVTargYCorrected(100,-2,2,100,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1)),
	        "fEnergy>1400.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5",
		"colz");
//         t->Draw(Form("fEnergy:fPosition0.fY>>hTrackerMissVTargYCorrected(100,-120,120,100,0,2000)",
//         	     fy->GetParameter(0),fy->GetParameter(1)),
// 	        Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY<-0.3&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5",
//         	     fy->GetParameter(0),fy->GetParameter(1)),
// 		"colz");
// 	c->cd(3);
//         t->Draw("fTrackerMiss.fZ:fTargetPosition.fZ>>hTrackerMissVbcZ(50,230,280,100,-5,5)",
// 		"fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

// 	c->cd(4);
//         t->Draw("fTrackerMiss.fX:fPosition0.fX>>hTrackerMissVbcX(50,160,260,100,-5,5)",
// 	        "fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(4);
        t->Draw(Form("fTrackerMiss.fY:fPosition0.fY>>hTrackerMissVbcY1(50,-120,120,100,-3,3)"),
         	"fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(5);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY:fPosition0.fY>>hTrackerMissVbcY(100,-120,120,60,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1)),
         	"fEnergy>1200.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");
        c->cd(6);
        h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcY");
	if(h2){
		p1 = h2->ProfileX();
	        if(p1) {
			p1->Draw();
		        YfitResult = p1->Fit(fy2,"S,E,M","same",-60,60);
			fy2->SetFitResult(*(YfitResult.Get()));
		}
	}

	c->cd(7);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYCorrected1(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5&&TMath::Abs(fPosition0.fY)<50",
		"colz");

	c->cd(8);
/*	gPad->SetLogz(true);*/
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYCorrected2(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>1.5&&fNCherenkovElectrons<2.5",
		"colz");

	c->cd(9);
/*	gPad->SetLogz(true);*/
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYCorrected3(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel",
		"colz");

   TCanvas * c2 = new TCanvas("tarcker_align5_2","tarcker_align5_2");
   c2->Divide(3,2);

	c2->cd(1);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYDEP1(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&TMath::Abs(fPosition0.fY-90)<20",
		"colz");

	c2->cd(2);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYDEP2(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&TMath::Abs(fPosition0.fY-50)<20",
		"colz");

	c2->cd(3);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYDEP3(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&TMath::Abs(fPosition0.fY-10)<20",
		"colz");

	c2->cd(4);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYDEP4(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&TMath::Abs(fPosition0.fY+25)<30",
		"colz");


	c2->cd(5);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYDEP5(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&TMath::Abs(fPosition0.fY+45)<70",
		"colz");

	c2->cd(6);
// 	gPad->SetLogz(true);

        t->Draw(Form("fTrackerMiss.fY-%e-%e*fTargetPosition.fY \
                      -%e-%e*fPosition0.fY  :fEnergy>>hTrackerMissVbcEYDEP6(50,400,2000,50,-2,2)",
        	     fy->GetParameter(0),fy->GetParameter(1), fy2->GetParameter(0),fy2->GetParameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&TMath::Abs(fPosition0.fY-70)<40",
		"colz");

	c->SaveAs(Form("plots/%d/track_align5.png",runNumber));
	c2->SaveAs(Form("plots/%d/track_align5_2.png",runNumber));

	return(0);
}
