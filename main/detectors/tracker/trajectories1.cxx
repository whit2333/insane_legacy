Int_t trajectories1(Int_t runNumber=72999) {

	rman->SetRun(runNumber);

	TTree * t = (TTree *) gROOT->FindObject("trackingPositions");
	if(!t) return(-1);
	TH1F * h1 = 0;
	TH2F * h2 = 0;
	TProfile * p1 = 0;
	
	TF1 * fx = new TF1("xtrack_alignment","[0]+[1]*x",-100,100);
        TF1 * fy = new TF1("ytrack_alignment","[0]+[1]*x",-100,100);
        TF1 * fz = new TF1("ztrack_alignment","[0]+[1]*x",-100,100);
	
	TFitResultPtr XfitResult;
	TFitResultPtr YfitResult;
	TFitResultPtr ZfitResult;

	TCanvas * c = new TCanvas("trajectories1","trajectories1");
	c->Divide(3,3);
	
	c->cd(1);
        t->Draw("fTrackerPositions.fPositionUncertainty.fY:fTargetPositions.fPosition.fY>>trajY(100,-2,2,100,-2,2)","","colz");
	c->cd(2);
        t->Draw("fTrackerPositions.fPositionUncertainty.fX:fTargetPositions.fPosition.fX>>trajX(100,-2,2,100,-2,2)","","colz");
	c->cd(4);
        t->Draw("fTrackerPositions.fPositionUncertainty.fY:fTargetPositions.fPosition.fX>>trajY2(100,-2,2,100,-2,2)","","colz");
	c->cd(5);
        t->Draw("fTrackerPositions.fPositionUncertainty.fX:fTargetPositions.fPosition.fY>>trajX2(100,-2,2,100,-2,2)","","colz");


    return(0);
}