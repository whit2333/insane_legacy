/*! Simulation study the effect with and without the tracker by throwing positrons.

 - Run 4025 - noTracker6 - 500k uniform e+ 
 - Run 4026 - withTracker6 - 500k uniform e+

 */
Int_t with_without_positron(){

   Int_t run_with    = 4062;
   Int_t run_without = 4059;

   //rman->SetRun(4004);
   rman->SetRun(run_with);
   TTree * t = (TTree*) gROOT->FindObject("betaDetectors1");
   if(!t) return(-1);
   
   gROOT->cd();

   TH1F * h1 =0;

   TCanvas * c = new TCanvas("with_without_positron","With and Without Tracker - Positrons");
   c->Divide(2,2);

   c->cd(1);
   gPad->SetLogy(true);
   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer0(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer0");
   if(h1){
      h1->SetTitle("Photo-electron SUM");   
      h1->SetLineColor(2000);
      h1->SetFillColor(2000);
      h1->SetFillStyle(3001);
      h1->Draw("");
   }


   c->cd(2);
   gPad->SetLogy(true);
   t->Draw("bigcalClusters.fCherenkovBestNPEChannel>>cer1(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer1");
   if(h1){
      h1->SetTitle("Single Channel Photo-electron");   
      h1->SetLineColor(2000);
      h1->SetFillColor(2000);
      h1->SetFillStyle(3002);
      h1->Draw("");
   }

   c->cd(3);
/*   gPad->SetLogy(true);*/
   t->Draw("bigcalClusters.fTotalE:bigcalClusters.fCherenkovBestNPESum>>cer2(25,2,52,50,500,4500)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h2 = (TH2F*) gROOT->FindObject("cer2");
   if(h2){
      h2->SetTitle("Photo-electron SUM");   
      h2->SetLineColor(2000);
/*      h2->SetFillColor(2000);*/
/*      h2->SetFillStyle(3003);*/
      h2->Draw("box");
   }
   
   c->cd(4);
   gPad->SetLogy(true);
   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer4(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100&&\
	    TMath::Abs(bigcalClusters.fCherenkovTDC)<12","goff");
   h1 = (TH1F*) gROOT->FindObject("cer4");
   if(h1){
      h1->SetTitle("Photo-electron SUM");   
      h1->SetLineColor(2000);
      h1->SetFillColor(2000);
      h1->SetFillStyle(3003);
      h1->Draw("");
   }

   /// Without the tracker
   //rman->SetRun(4005);
   rman->SetRun(run_without);
   TTree * t = (TTree*) gROOT->FindObject("betaDetectors1");
   if(!t) return(-1);
   gROOT->cd();

   c->cd(1);
   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer20(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer20");
   if(h1){
      h1->SetLineColor(2001);
      h1->Draw("same");
   }

   c->cd(2);
   t->Draw("bigcalClusters.fCherenkovBestNPEChannel>>cer21(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer21");
   if(h1){
      h1->SetLineColor(2001);
      h1->Draw("same");
   }

   c->cd(3);
   t->Draw("bigcalClusters.fTotalE:bigcalClusters.fCherenkovBestNPESum>>cer22(25,2,52,50,500,4500)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h2 = (TH2F*) gROOT->FindObject("cer22");
   if(h2){
      h2->SetTitle("Photo-electron SUM");   
      h2->SetLineColor(2001);
/*      h2->SetFillColor(2001);*/
/*      h2->SetFillStyle(3003);*/
      h2->Draw("box,same");
   }


   c->cd(4);
   gPad->SetLogy(true);
   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer24(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100&&\
	    TMath::Abs(bigcalClusters.fCherenkovTDC)<12","goff");
   h1 = (TH1F*) gROOT->FindObject("cer24");
   if(h1){
      h1->SetTitle("Photo-electron SUM");   
      h1->SetLineColor(2001);
      //h1->SetFillColor(2000);
      //h1->SetFillStyle(3003);
      h1->Draw("same");
   }

   c->SaveAs("results/with_without_positron.png");


//    t->StartViewer();


}



