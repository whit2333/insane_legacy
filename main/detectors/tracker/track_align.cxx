/*! Alignment correction for track "miss distance".
    Since the "miss distance" is calculated by drawing a straight line from
    the cluster to the target and projected onto the detector (either
    hodoscope or tracker) using no cross calibration. It just uses the 
    detector geometry information. This script finds the correction necessary
    to remove any position dependence.

    \todo See if this correction can be minimized/understood by making more
    detailed geometry corrections.   
  
 */
Int_t track_align(Int_t runNumber=1579) {

	rman->SetRun(runNumber);

	TTree * t = (TTree *) gROOT->FindObject("Tracks");
	if(!t) return(-1);
	TH1F * h1 = 0;
	TH2F * h2 = 0;
	TProfile * p1 = 0;

	TF1 * fx = new TF1("xtrack_alignment","[0]+[1]*x",-100,100);
        TF1 * fy = new TF1("ytrack_alignment","[0]+[1]*x",-100,100);
        TF1 * fz = new TF1("ztrack_alignment","[0]+[1]*x",-100,100);
	
        
	TFitResultPtr XfitResult;
	TFitResultPtr YfitResult;
	TFitResultPtr ZfitResult;


	TCanvas * c = new TCanvas("track_align","track_align");
	c->Divide(3,3);
	
	c->cd(1);
        t->Draw("fTrackerMiss.fX:fPosition0.fX>>hTrackerMissVbcX(50,160,260,100,-5,5)",
	        "fEnergy>1000.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(2);
        t->Draw("fTrackerMiss.fY:fPosition0.fY>>hTrackerMissVbcY(50,-120,120,100,-5,5)",
         	"fEnergy>1000.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(3);
        t->Draw("fTrackerMiss.fZ:fPosition0.fZ>>hTrackerMissVbcZ(50,230,280,100,-5,5)",
		"fEnergy>1000.0&&!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5","colz");

	c->cd(4);
        h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcX");
	if(h2){
		p1 = h2->ProfileX();
	        if(p1) {
			p1->Draw();
		        XfitResult = p1->Fit(fx,"S,E,M","same",180,220);
			fx->SetFitResult(*(ROOT::Fit::FitResult*)(XfitResult.Get()));
		}
	}

        c->cd(5);
        h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcY");
	if(h2){
		p1 = h2->ProfileX();
	        if(p1) {
			p1->Draw();
		        YfitResult = p1->Fit(fy,"S,E,M","same",-40,40);
			fy->SetFitResult(*(YfitResult.Get()));
		}
	}
        c->cd(6);
        h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcZ");
	if(h2){
		p1 = h2->ProfileX();
	        if(p1) {
			p1->Draw();
		        ZfitResult = p1->Fit(fz,"S,E,M","same",200,300);
			fz->SetFitResult(*(ZfitResult.Get()));
		}
	}

	c->cd(7);
        t->Draw(Form("fTrackerMiss.fX-%e-%e*fPosition0.fX:fPosition0.fX>>hTrackerMissVbcXCorrected(50,160,260,50,-4,4)",XfitResult->Parameter(0),XfitResult->Parameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5",
		"colz");

	c->cd(8);
        t->Draw(Form("fTrackerMiss.fY-%e-%e*fPosition0.fY:fPosition0.fY>>hTrackerMissVbcYCorrected(50,-120,120,50,-4,4)",
        	     YfitResult->Parameter(0),YfitResult->Parameter(1)),
	        "!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5",
		"colz");

	c->cd(9);
        t->Draw(Form("fTrackerMiss.fZ-%e-%e*fPosition0.fZ:fPosition0.fZ>>hTrackerMissVbcZCorrected(50,200,300,50,-4,4)",ZfitResult->Parameter(0),ZfitResult->Parameter(1)),
		"!fIsNoisyChannel&&fNCherenkovElectrons>0.5&&fNCherenkovElectrons<1.5",
		"colz");

	rman->GetCurrentFile()->cd("calibrations/tracker");
	
	InSANECalibration * xCal = new InSANECalibration("x_track_align","x track align");
	xCal->Clear();
	xCal->AddFunction(fx);
        h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcXCorrected");
	xCal->AddHistogram(h2);

	InSANECalibration * yCal = new InSANECalibration("y_track_align","y track align");
	yCal->Clear();
	yCal->AddFunction(fy);
        h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcYCorrected");
	yCal->AddHistogram(h2);

	InSANECalibration * zCal = new InSANECalibration("z_track_align","z track align");
	zCal->Clear();
	zCal->AddFunction(fz);
        h2 = (TH2F*) gROOT->FindObject("hTrackerMissVbcZCorrected");
	zCal->AddHistogram(h2);


	xCal->Print();
	yCal->Print();
	zCal->Print();

        xCal->Write(xCal->GetName(), TObject::kWriteDelete);
        yCal->Write(yCal->GetName(), TObject::kWriteDelete);
        zCal->Write(zCal->GetName(), TObject::kWriteDelete);

	c->SaveAs(Form("plots/%d/track_align.png",runNumber));

	return(0);
}
