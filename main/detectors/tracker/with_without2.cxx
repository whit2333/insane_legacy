Int_t with_without2(){

   rman->SetRun(4022);
   TTree * t = (TTree*) gROOT->FindObject("betaDetectors1");
   if(!t) return(-1);
   
   gROOT->cd();

   TH1F * h1 =0;

   TCanvas * c = new TCanvas("with_without_tracker2","With and Without Tracker 2");

   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer0(100,2,52)","","goff");
   h1 = (TH1F*) gROOT->FindObject("cer0");
   if(h1){
      h1->SetLineColor(1);
      h1->SetFillColor(1);
      h1->SetFillStyle(3003);
      h1->Draw("");
   }

   t->Draw("bigcalClusters.fCherenkovBestNPEChannel>>cer1(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer1");
   if(h1){
      h1->SetLineColor(2001);
      h1->SetFillColor(2001);
      h1->SetFillStyle(3004);
      h1->Draw("same");
   }

   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer2(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer2");
   if(h1){
      h1->SetLineColor(2000);
      h1->SetFillColor(2000);
      h1->SetFillStyle(3005);
      h1->Draw("same");
   }
/*

   rman->SetRun(1506);
   TTree * t = (TTree*) gROOT->FindObject("betaDetectors1");
   if(!t) return(-1);
   gROOT->cd();

   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer20(100,2,52)","","goff");
   h1 = (TH1F*) gROOT->FindObject("cer20");
   if(h1){
      h1->SetLineColor(2000);
      h1->Draw("same");
   }

   t->Draw("bigcalClusters.fCherenkovBestNPEChannel>>cer21(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer21");
   if(h1){
      h1->SetLineColor(2001);
      h1->Draw("same");
   }

   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer22(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer22");
   if(h1){
      h1->SetLineColor(2000);
      h1->Draw("same");
   }*/



//    t->StartViewer();


}



