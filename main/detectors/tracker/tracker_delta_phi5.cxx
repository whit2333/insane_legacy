Int_t tracker_delta_phi5(Int_t runNumber = 72994) {

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   if(!events->fTree) return(-1);

   Int_t nevents = 300000;

   InSANEReconstructedEvent * fReconEvent = new InSANEReconstructedEvent();

   const char * treeName2 = "trackingPositions";
   TTree * t2 = (TTree*)gROOT->FindObject(treeName2);
   if(!t2){ std::cout << " TREE NOT FOUND : " << treeName2 << "\n"; return(-1);}
   if( t2->BuildIndex("fRunNumber","fEventNumber") < 0 )
      std::cout << "Failed to build branch on " << treeName2 << "\n";
   t2->AddFriend(events->fTree);
   t2->SetBranchAddress("uncorrelatedPositions",&fReconEvent);

   rman->GetCurrentFile()->cd("detectors/tracker");

   Double_t phimin = -5.0;
   Double_t phimax = 5.0;

   TH1F * hDeltaPhi      = new TH1F("TrackerDeltaPhi","Tracker Delta Phi E<1GeV", 100,phimin,phimax);
   TH2F * hTimingVsDeltaPhi      = new TH2F("hTimingVsDeltaPhi","Timing Vs DeltaPhi ", 100,phimin,phimax,100,-400,400);
   TH2F * hDeltaPhiVsBCY      = new TH2F("hDeltaPhiVsBCY","DeltaPhi Vs BigcalY ",120,-120,120,100,phimin,phimax);
   TH2F * hDeltaPhiVsBCj      = new TH2F("hDeltaPhiVsBCj","DeltaPhi Vs Bigcalj ",56,1,57,100,phimin,phimax);
   TH1F * hDeltaPhi2     = new TH1F("TrackerDeltaPhi2","Tracker Delta Phi 2 E<1GeV", 100,phimin,phimax);
   TH1F * hDeltaPhiAll   = new TH1F("TrackerDeltaPhiAll","Tracker Delta Phi All Energies", 100,phimin,phimax);
   TH1F * hDeltaPhi3     = new TH1F("TrackerDeltaPhi3","Tracker Delta Phi E>1.2GeV", 100,phimin,phimax);

   TH1F * hDeltaPhi_1pos      = new TH1F("TrackerDeltaPhi_1pos","Tracker_1pos Delta Phi E<1GeV", 100,phimin,phimax);
   TH2F * hTimingVsDeltaPhi_1pos      = new TH2F("hTimingVsDeltaPhi_1pos","Timing Vs DeltaPhi ", 100,phimin,phimax,100,-400,400);
   TH2F * hDeltaPhiVsBCY_1pos       = new TH2F("hDeltaPhiVsBCY_1pos ","DeltaPhi_1pos  Vs BigcalY ",120,-120,120, 100,phimin,phimax);
   TH2F * hDeltaPhiVsBCj_1pos      = new TH2F("hDeltaPhiVsBCj_1pos","DeltaPhi_1pos Vs Bigcalj ",56,1,57,100,phimin,phimax);
   TH1F * hDeltaPhi2_1pos     = new TH1F("TrackerDeltaPhi2_1pos","Tracker_1pos Delta Phi 2 E<1GeV", 100,phimin,phimax);
   TH1F * hDeltaPhiAll_1pos   = new TH1F("TrackerDeltaPhiAll_1pos","Tracker_1pos Delta Phi All Energies", 100,phimin,phimax);
   TH1F * hDeltaPhi3_1pos     = new TH1F("TrackerDeltaPhi3_1pos","Tracker_1pos Delta Phi E>1.2GeV", 100,phimin,phimax);

   TH1F * hDeltaPhi_2pos      = new TH1F("TrackerDeltaPhi_2pos","Tracker_2pos Delta Phi E<1GeV", 100,phimin,phimax);
   TH2F * hTimingVsDeltaPhi_2pos      = new TH2F("hTimingVsDeltaPhi_2pos","Timing Vs DeltaPhi ", 100,phimin,phimax,100,-400,400);
   TH2F * hDeltaPhiVsBCY_2pos       = new TH2F("hDeltaPhiVsBCY_2pos ","DeltaPhi_2pos  Vs BigcalY ",120,-120,120, 100,phimin,phimax);
   TH2F * hDeltaPhiVsBCj_2pos      = new TH2F("hDeltaPhiVsBCj_2pos","DeltaPhi_2pos Vs Bigcalj ",56,1,57,100,phimin,phimax);
   TH1F * hDeltaPhi2_2pos     = new TH1F("TrackerDeltaPhi2_2pos","Tracker_2pos Delta Phi 2 E<1GeV", 100,phimin,phimax);
   TH1F * hDeltaPhiAll_2pos   = new TH1F("TrackerDeltaPhiAll_2pos","Tracker_2pos Delta Phi All Energies", 100,phimin,phimax);
   TH1F * hDeltaPhi3_2pos     = new TH1F("TrackerDeltaPhi3_2pos","Tracker_2pos Delta Phi E>1.2GeV", 100,phimin,phimax);


   rman->GetCurrentFile()->cd();

   Double_t RadPerDegree = 0.017453293;

   BIGCALCluster * cluster = 0;
   ForwardTrackerPositionHit * tkpos = 0;
   InSANEHitPosition *  hitpos = 0;

   for(int i = 0 ; i< t2->GetEntries()  ; i++){
      if( i%10000 == 0) std::cout << " Entry " << i << "\n";
      t2->GetEntry(i);

      events->fTree->GetEntryWithIndex(fReconEvent->fRunNumber,fReconEvent->fEventNumber);

      if( events->TRIG->IsBETA2Event() ) {
         /// Loop over clusters....
         for(int ic = 0; ic < events->CLUSTER->fClusters->GetEntries() ;ic++){
            cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[ic];

            /// Check that the cluster channel is not a noisy one!
            if( !(cluster->fIsNoisyChannel) && cluster->fIsGood) 
            if( cluster->fCherenkovBestADCSum > 0.5 && cluster->fCherenkovBestADCSum < 2.5 )
            if( fReconEvent->fClosestLuciteMissDistance < 50.0)
            {

               /// Tracker double hit positions.
               for(int it = 0; it < fReconEvent->fTrackerPositions->GetEntries(); it++) {
                  hitpos = (InSANEHitPosition *)(*(fReconEvent->fTrackerPositions))[it];

                  hDeltaPhiAll->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                  hTimingVsDeltaPhi->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree ,hitpos->fTiming);
                  hDeltaPhiVsBCY->Fill(cluster->fYMoment, (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );

                  /// Tracker double hit positions with low energy which should show separation.
                  if( cluster->GetEnergy() < 1000 &&  cluster->GetEnergy() > 800 ){
                     hDeltaPhi->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                     hDeltaPhi2->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                     hDeltaPhiVsBCj->Fill(cluster->fjPeak, (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );

                  }
                  if(  cluster->GetEnergy() > 1200 &&  cluster->GetEnergy() < 1400  ){
                     hDeltaPhi3->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                  }

               } // tracker double hit loop

               /// Tracker Y1 hit positions.
               for(int it = 0; it < fReconEvent->fTrackerY1Positions->GetEntries(); it++) {
                  hitpos = (InSANEHitPosition *)(*(fReconEvent->fTrackerY1Positions))[it];

                  hDeltaPhiAll_1pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                  hTimingVsDeltaPhi_1pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree ,hitpos->fTiming);
                  hDeltaPhiVsBCY_1pos->Fill(cluster->fYMoment, (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                  hDeltaPhiVsBCj_1pos->Fill(cluster->fjPeak, (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );


                  /// Tracker double hit positions with low energy which should show separation.
                  if( cluster->GetEnergy() < 1000 &&  cluster->GetEnergy() > 800 ){
                     hDeltaPhi_1pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                     hDeltaPhi2_1pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                     hDeltaPhiVsBCj_1pos->Fill(cluster->fjPeak, (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );

                  }
                  if(  cluster->GetEnergy() > 1200 &&  cluster->GetEnergy() < 1400  ){
                     hDeltaPhi3_1pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                  }
               } // tracker Y2 hit loop

               /// Tracker Y1 hit positions.
               for(int it = 0; it < fReconEvent->fTrackerY2Positions->GetEntries(); it++) {
                  hitpos = (InSANEHitPosition *)(*(fReconEvent->fTrackerY2Positions))[it];

                  hDeltaPhiAll_2pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                  hTimingVsDeltaPhi_2pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree ,hitpos->fTiming);
                  hDeltaPhiVsBCY_2pos->Fill(cluster->fYMoment, (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                  hDeltaPhiVsBCj_2pos->Fill(cluster->fjPeak, (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );

                  /// Tracker double hit positions with low energy which should show separation.
                  if( cluster->GetEnergy() < 1000 &&  cluster->GetEnergy() > 800 ){
                     hDeltaPhi_2pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                     hDeltaPhi2_2pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );
                     hDeltaPhiVsBCj_2pos->Fill(cluster->fjPeak, (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );

                  }
                  if(  cluster->GetEnergy() > 1200 &&  cluster->GetEnergy() < 1400  ){
                     hDeltaPhi3_2pos->Fill( (hitpos->fPosition.Phi() - hitpos->fSeedingPosition.Phi())/RadPerDegree );

                  }
               } // tracker Y1 hit loop

            } // not noisy cluster
         } // cluster loop
      } // is BETA2 event
   } // event loop


   TCanvas * c = new TCanvas("tracker_delta_phi","tracker_delta_phi");
   c->Divide(2,3);

   c->cd(1);
   hDeltaPhiAll->SetLineColor(1);
   hDeltaPhiAll->Draw();

   hDeltaPhi2->SetLineColor(2);
   hDeltaPhi2->Draw("same");

   hDeltaPhi3->SetLineColor(4);
   hDeltaPhi3->Draw("same");

   c->cd(2);
   hDeltaPhiVsBCj->Draw("colz");


   c->cd(3);
   hDeltaPhiAll_1pos->SetLineColor(1);
   hDeltaPhiAll_1pos->Draw();

   hDeltaPhi2_1pos->SetLineColor(2);
   hDeltaPhi2_1pos->Draw("same");

   hDeltaPhi3_1pos->SetLineColor(4);
   hDeltaPhi3_1pos->Draw("same");

   c->cd(4);
   hDeltaPhiVsBCj_1pos->Draw("colz");


   c->cd(5);
   hDeltaPhiAll_2pos->SetLineColor(1);
   hDeltaPhiAll_2pos->Draw();

   hDeltaPhi2_2pos->SetLineColor(2);
   hDeltaPhi2_2pos->Draw("same");

   hDeltaPhi3_2pos->SetLineColor(4);
   hDeltaPhi3_2pos->Draw("same");

   c->cd(6);
   hDeltaPhiVsBCj_2pos->Draw("colz");
/*   hDeltaPhi3_2pos->DrawCopy("same");*/
//    c->cd(3);
//    hDeltaPhi3->Draw();

//    t2->StartViewer();

   c->SaveAs(Form("plots/%d/tracker_delta_phi5.png",runNumber));

   return(0);

}
