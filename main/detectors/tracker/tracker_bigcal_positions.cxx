Int_t tracker_bigcal_positions(Int_t runNumber = 72999) {

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   if(!events->fTree) return(-1);

   Int_t nevents = 300000;

   const char * treeName2 = "trackingPositions";
   TTree * t2 = (TTree*)gROOT->FindObject(treeName2);
   if(!t2){ std::cout << " TREE NOT FOUND : " << treeName2 << "\n"; return(-1);}
   if( t2->BuildIndex("fRunNumber","fEventNumber") < 0 )
      std::cout << "Failed to build branch on " << treeName2 << "\n";
   t2->AddFriend(events->fTree);


   rman->GetCurrentFile()->cd("detectors/tracker");
   TH2F * hists1[128];
   TH1F * hists1_1[128];
   TH2F * hists2[128];
   TH1F * hists2_1[128];
   TH2F * hists3[68];
   TH1F * hists3_1[68];
   TH2F * hists4[68];
   TH1F * hists4_1[68];
   for(int i = 0;i<128;i++){
      hists1[i] = new TH2F(Form("TrackerY1RowBigcalXY-%d",i+1),Form("TrackerY1BigcalXY-%d",i+1),32,1,33,56,1,57);
      hists1_1[i] = new TH1F(Form("TrackerY1RowBigcalY-%d",i+1),Form("TrackerY1BigcalY-%d",i+1),56,1,57);
      hists2[i] = new TH2F(Form("TrackerY2RowBigcalXY-%d",i+1),Form("TrackerY2BigcalXY-%d",i+1),32,1,33,56,1,57);
      hists2_1[i] = new TH1F(Form("TrackerY2RowBigcalY-%d",i+1),Form("TrackerY2BigcalY-%d",i+1),56,1,57);
   }
   for(int i = 0;i<68;i++){
      hists3[i] = new TH2F(Form("TrackerX1RowBigcalXY-%d",i+1),Form("TrackerXRowBigcalXY-%d",i+1),32,1,33,56,1,57);
      hists3_1[i] = new TH1F(Form("TrackerX1RowBigcalX-%d",i+1),Form("TrackerXRowBigcalX-%d",i+1),32,1,33);
      hists4[i] = new TH2F(Form("TrackerX2RowBigcalXY-%d",i+1),Form("TrackerXRowBigcalXY-%d",i+1),32,1,33,56,1,57);
      hists4_1[i] = new TH1F(Form("TrackerX2RowBigcalX-%d",i+1),Form("TrackerXRowBigcalX-%d",i+1),32,1,33);
   }
   rman->GetCurrentFile()->cd();
   
   BIGCALCluster * cluster = 0;
   ForwardTrackerPositionHit * tkpos = 0;

   for(int i = 0 ; i<events->fTree->GetEntries() ; i++){
      if( i%10000 == 0) std::cout << " Entry " << i << "\n";
      events->fTree->GetEntry(i);

      if( events->TRIG->IsBETA2Event() ) {
      
         for(int it = 0; it <  events->BETA->fForwardTrackerEvent->fTrackerPositionHits->GetEntries() ;it++)
         for(int ic = 0; ic < events->CLUSTER->fClusters->GetEntries() ;ic++){
/*            std::cout << events->CLUSTER->fClusters->GetEntries() << "\n";*/
            cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[ic];
            if( !(cluster->fIsNoisyChannel) && cluster->fTotalE > 1200 ){
               tkpos = (ForwardTrackerPositionHit*)(*(events->BETA->fForwardTrackerEvent->fTrackerPositionHits))[it];
            
               if(TMath::Abs(tkpos->fTDCSum) <300) {
                  if(tkpos->fScintLayer ==1) hists1[tkpos->fYRow-1]->Fill(cluster->fiPeak,cluster->fjPeak);
                  if(tkpos->fScintLayer ==1) hists1_1[tkpos->fYRow-1]->Fill(cluster->fjPeak);
                  if(tkpos->fScintLayer ==2) hists2[tkpos->fYRow-1]->Fill(cluster->fiPeak,cluster->fjPeak);
                  if(tkpos->fScintLayer ==2) hists2_1[tkpos->fYRow-1]->Fill(cluster->fjPeak);
                  if(tkpos->fScintLayer ==1) hists3[tkpos->fXRow-1]->Fill(cluster->fiPeak,cluster->fjPeak);
                  if(tkpos->fScintLayer ==1) hists3_1[tkpos->fXRow-1]->Fill(cluster->fiPeak);
                  if(tkpos->fScintLayer ==2) hists4[tkpos->fXRow-1]->Fill(cluster->fiPeak,cluster->fjPeak);
                  if(tkpos->fScintLayer ==2) hists4_1[tkpos->fXRow-1]->Fill(cluster->fiPeak);
               }
            }
         
         }
      
      }
   
   
   }


    TCanvas * c1 = new TCanvas("tracker_bigcal_positions","tracker_bigcal_positions");
    Int_t histcount = 0;
    c1->Divide(5,5);
    do{
       for(int i = 0;i<25;i++){
          c1->cd(i+1);
          if(histcount >= 128) break;
          hists1_1[histcount]->Draw();
          histcount++;
       }
       c1->SaveAs(Form("plots/%d/tracker_bigcal_positions_1_%d.png",runNumber,histcount));
    } while ( histcount < 128 ) ;


    TCanvas * c1 = new TCanvas("tracker_bigcal_positions_1","tracker_bigcal_positions");
    Int_t histcount = 0;
    c1->Divide(5,5);
    do{
       for(int i = 0;i<25;i++){
          c1->cd(i+1);
          if(histcount >= 128) break;
          hists1[histcount]->Draw("colz");
          histcount++;
       }
       c1->SaveAs(Form("plots/%d/tracker_bigcal_XY_positions_1_%d.png",runNumber,histcount));
    } while ( histcount < 128 ) ;


    TCanvas * c1 = new TCanvas("tracker_bigcal_positions","tracker_bigcal_positions");
    Int_t histcount = 0;
    c1->Divide(5,5);
    do{
       for(int i = 0;i<25;i++){
          c1->cd(i+1);
          if(histcount >= 128) break;
          hists2_1[histcount]->Draw();
          histcount++;
       }
       c1->SaveAs(Form("plots/%d/tracker_bigcal_positions_2_%d.png",runNumber,histcount));
    } while ( histcount < 128 ) ;


    TCanvas * c1 = new TCanvas("tracker_bigcal_XY_positions","tracker_bigcal_positions");
    Int_t histcount = 0;
    c1->Divide(5,5);
    do{
       for(int i = 0;i<25;i++){
          c1->cd(i+1);
          if(histcount >= 128) break;
          hists2[histcount]->Draw("colz");
          histcount++;
       }
       c1->SaveAs(Form("plots/%d/tracker_bigcal_XY_positions_2_%d.png",runNumber,histcount));
    } while ( histcount < 128 ) ;




//    c1->SaveAs(Form("plots/%d/tracker_hit_efficiency.png",runNumber));
//    c1->SaveAs(Form("plots/%d/tracker_hit_efficiency.pdf",runNumber));
// 
//    c2->SaveAs(Form("plots/%d/tracker_hit_efficiency2.png",runNumber));
//    c2->SaveAs(Form("plots/%d/tracker_hit_efficiency2.pdf",runNumber));
// 
//    c3->SaveAs(Form("plots/%d/tracker_hit_efficiency3.png",runNumber));
//    c3->SaveAs(Form("plots/%d/tracker_hit_efficiency3.pdf",runNumber));

   return(0);

}
