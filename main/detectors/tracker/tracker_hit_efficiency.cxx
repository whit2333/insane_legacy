Int_t tracker_hit_efficiency(Int_t runNumber = 72990) {

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;

   SANEEvents * events = new SANEEvents("betaDetectors1");
   if(!events->fTree) return(-1);

   Int_t nevents = 300000;

   const char * treeName2 = "trackingPositions";
   TTree * t2 = (TTree*)gROOT->FindObject(treeName2);
   if(!t2){ std::cout << " TREE NOT FOUND : " << treeName2 << "\n"; return(-1);}
   if( t2->BuildIndex("fRunNumber","fEventNumber") < 0 )
      std::cout << "Failed to build branch on " << treeName2 << "\n";

   t2->AddFriend(events->fTree);

   rman->GetCurrentFile()->cd("detectors/tracker");

   TCanvas * c1 = new TCanvas("tracker_hit_efficiency","Tracker Hit Eff");
   c1->Divide(2,2);

   /// Tracker  with at least 1 hit with
   ///  - Timed Cherenkov hit
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fYMoment>>hTotalEvents(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
   TH1F * hTotal = (TH1F*)gROOT->FindObject("hTotalEvents");
   events->fTree->Draw("bigcalClusters.fYMoment>>hPassedEvents(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fTrackerHits.fTDCAlign)<200&&fTrackerHits.fScintLayer==1",
           "goff",nevents);
   TH1F * hPassed = (TH1F*)gROOT->FindObject("hPassedEvents");
   c1->cd(1);
   TEfficiency * luc1HitEffWithCerBigcal = new TEfficiency(*hPassed,*hTotal);
   luc1HitEffWithCerBigcal->SetTitle("Tracker (y1) at least 1 timed hit vs Bigcal Y ");
   luc1HitEffWithCerBigcal->Draw();


   /// Tracker with at least 1 double hit with
   ///  - Timed Cherenkov hit
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fYMoment>>hTotalEvents2(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
   TH1F * hTotal2 = (TH1F*)gROOT->FindObject("hTotalEvents2");
   events->fTree->Draw("bigcalClusters.fYMoment>>hPassedEvents2(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fTrackerPositionHits.fTDCSum)<200&&fTrackerPositionHits.fScintLayer==1",
           "goff",nevents);
   TH1F * hPassed2 = (TH1F*)gROOT->FindObject("hPassedEvents2");
   c1->cd(2);
   TEfficiency * luc2HitEffWithCerBigcal = new TEfficiency(*hPassed2,*hTotal2);
   luc2HitEffWithCerBigcal->SetTitle("Tracker (x1+y1) at least 1 timed hit vs Bigcal Y ");
   luc2HitEffWithCerBigcal->Draw();

   /// Lucite Hodoscope with at least 1 hit with
   ///  - Timed Cherenkov hit
   ///  - Cherenkov ADC cut (0.2,2.2)
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fYMoment>>hTotalEvents3(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
   TH1F * hTotal3 = (TH1F*)gROOT->FindObject("hTotalEvents3");
   events->fTree->Draw("bigcalClusters.fYMoment>>hPassedEvents3(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fTrackerHits.fTDCAlign)<200&&fTrackerHits.fScintLayer==2",
           "goff",nevents);
   TH1F * hPassed3 = (TH1F*)gROOT->FindObject("hPassedEvents3");
   c1->cd(3);
   TEfficiency * luc3HitEffWithCerBigcal = new TEfficiency(*hPassed3,*hTotal3);
   luc3HitEffWithCerBigcal->SetTitle("Tracker (y2) 1 timed hit vs Bigcal Y ");
   luc3HitEffWithCerBigcal->Draw();


   /// Lucite Hodoscope with at least 1 double hit with
   ///  - Timed Cherenkov hit
   ///  - Cherenkov ADC cut (0.2,2.2)
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fYMoment>>hTotalEvents4(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
   TH1F * hTotal4 = (TH1F*)gROOT->FindObject("hTotalEvents4");
   events->fTree->Draw("bigcalClusters.fYMoment>>hPassedEvents4(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fTrackerHits.fTDCAlign)<200&&fTrackerHits.fScintLayer==0",
           "goff",nevents);
   TH1F * hPassed4 = (TH1F*)gROOT->FindObject("hPassedEvents4");
   c1->cd(4);
   TEfficiency * luc4HitEffWithCerBigcal = new TEfficiency(*hPassed4,*hTotal4);
   luc4HitEffWithCerBigcal->SetTitle("Tracker (x1)  hit vs Bigcal Y ");
   luc4HitEffWithCerBigcal->Draw();




   /// Vs bigcla X position

   TCanvas * c2 = new TCanvas("tracker_hit_efficiency2","Tracker Hit Eff vs x");
   c2->Divide(2,2);

   /// Lucite Hodoscope with at least 1 hit with
   ///  - Timed Cherenkov hit
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fXMoment>>hTotalEventsx(120,-60,60)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
   TH1F * hTotalx = (TH1F*)gROOT->FindObject("hTotalEventsx");
   events->fTree->Draw("bigcalClusters.fXMoment>>hPassedEventsx(120,-60,60)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fLuciteHits.fTDCAlign)<200",
           "goff",nevents);
   TH1F * hPassedx = (TH1F*)gROOT->FindObject("hPassedEventsx");
   c2->cd(1);
   TEfficiency * luc1HitEffWithCerBigcalx = new TEfficiency(*hPassedx,*hTotalx);
   luc1HitEffWithCerBigcalx->SetTitle("Lucite at least 1 timed hit vs Bigcal X ");
   luc1HitEffWithCerBigcalx->Draw();


   /// Lucite Hodoscope with at least 1 double hit with
   ///  - Timed Cherenkov hit
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fXMoment>>hTotalEvents2x(120,-60,60)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
   TH1F * hTotal2x = (TH1F*)gROOT->FindObject("hTotalEvents2x");
   events->fTree->Draw("bigcalClusters.fXMoment>>hPassedEvents2x(120,-60,60)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fLucitePositionHits.fTDCSum)<200",
           "goff",nevents);
   TH1F * hPassed2x = (TH1F*)gROOT->FindObject("hPassedEvents2x");
   c2->cd(2);
   TEfficiency * luc2HitEffWithCerBigcalx = new TEfficiency(*hPassed2x,*hTotal2x);
   luc2HitEffWithCerBigcalx->SetTitle("Lucite at least 1 double hit vs Bigcal X ");
   luc2HitEffWithCerBigcalx->Draw();


/*
   /// Lucite Hodoscope with at least 1 hit with
   ///  - Timed Cherenkov hit
   ///  - Cherenkov ADC cut (0.2,2.2)
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fXMoment>>hTotalEvents3x(120,-60,60)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&bigcalClusters.fCherenkovBestADCSum>0.2&&bigcalClusters.fCherenkovBestADCSum<2.2",
           "goff",nevents);
   TH1F * hTotal3x = (TH1F*)gROOT->FindObject("hTotalEvents3x");
   events->fTree->Draw("bigcalClusters.fXMoment>>hPassedEvents3x(120,-60,60)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fLuciteHits.fTDCAlign)<200&&bigcalClusters.fCherenkovBestADCSum>0.2&&bigcalClusters.fCherenkovBestADCSum<2.2",
           "goff",nevents);
   TH1F * hPassed3x = (TH1F*)gROOT->FindObject("hPassedEvents3x");
   c2->cd(3);
   TEfficiency * luc3HitEffWithCerBigcalx = new TEfficiency(*hPassed3x,*hTotal3x);
   luc3HitEffWithCerBigcalx->SetTitle("At least 1 timed hit vs Bigcal X w/ Cer window (0.2,2.2)");
   luc3HitEffWithCerBigcalx->Draw();


   /// Lucite Hodoscope with at least 1 double hit with
   ///  - Timed Cherenkov hit
   ///  - Cherenkov ADC cut (0.2,2.2)
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fXMoment>>hTotalEvents4x(120,-60,60)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&bigcalClusters.fCherenkovBestADCSum>0.2&&bigcalClusters.fCherenkovBestADCSum<2.2",
           "goff",nevents);
   TH1F * hTotal4x = (TH1F*)gROOT->FindObject("hTotalEvents4x");
   events->fTree->Draw("bigcalClusters.fXMoment>>hPassedEvents4x(120,-60,60)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fLucitePositionHits.fTDCSum)<200&&bigcalClusters.fCherenkovBestADCSum>0.2&&bigcalClusters.fCherenkovBestADCSum<2.2",
           "goff",nevents);
   TH1F * hPassed4x = (TH1F*)gROOT->FindObject("hPassedEvents4x");
   c2->cd(4);
   TEfficiency * luc4HitEffWithCerBigcalx = new TEfficiency(*hPassed4x,*hTotal4x);
   luc4HitEffWithCerBigcalx->SetTitle("At least 1 double hit vs Bigcal X w/ Cer window (0.2,2.2)");
   luc4HitEffWithCerBigcalx->Draw();
*/





   /// Geometry matching... 


/*
   TCanvas * c3 = new TCanvas("hodoscope_hit_efficiency3","Hodoscope Hit Eff  with geometry");
   c3->Divide(2,2);

   /// Lucite Hodoscope with at least 1 hit with
   ///  - Timed Cherenkov hit
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fYMoment>>hTotalEventsGeo(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
//&&bigcalClusters.IsGoodHodoscopeBar(fLuciteHodoscopeEvent.fLuciteHits.fRow)
   TH1F * hTotalGeo = (TH1F*)gROOT->FindObject("hTotalEventsGeo");
   events->fTree->Draw("bigcalClusters.fYMoment>>hPassedEventsGeo(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fLuciteHodoscopeEvent.fLuciteHits.fRow*2 - bigcalClusters.fjPeak)<4&&TMath::Abs(fLuciteHits.fTDCAlign)<200",
           "goff",nevents);
   TH1F * hPassedGeo = (TH1F*)gROOT->FindObject("hPassedEventsGeo");
   c3->cd(1);
   TEfficiency * luc1HitEffWithCerBigcalGeo = new TEfficiency(*hPassedGeo,*hTotalGeo);
   luc1HitEffWithCerBigcalGeo->SetTitle("1 timed/Geo hit  vs Bigcal Y ");
   luc1HitEffWithCerBigcalGeo->Draw();


   /// Lucite Hodoscope with at least 1 double hit with
   ///  - Timed Cherenkov hit
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fYMoment>>hTotalEvents2Geo(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
   TH1F * hTotal2Geo = (TH1F*)gROOT->FindObject("hTotalEvents2Geo");
   events->fTree->Draw("bigcalClusters.fYMoment>>hPassedEvents2Geo(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fLuciteHodoscopeEvent.fLuciteHits.fRow*2 - bigcalClusters.fjPeak)<4&&TMath::Abs(fLucitePositionHits.fTDCSum)<200",
           "goff",nevents);
   TH1F * hPassed2Geo = (TH1F*)gROOT->FindObject("hPassedEvents2Geo");
   c3->cd(2);
   TEfficiency * luc2HitEffWithCerBigcalGeo = new TEfficiency(*hPassed2Geo,*hTotal2Geo);
   luc2HitEffWithCerBigcalGeo->SetTitle("1 double timed/Geo  vs Bigcal Y ");
   luc2HitEffWithCerBigcalGeo->Draw();

   /// Lucite Hodoscope with at least 1 hit with
   ///  - Timed Cherenkov hit
   ///  - Cherenkov ADC cut (0.2,2.2)
   ///  - Bigcal E > 1 GeV
   events->fTree->Draw("bigcalClusters.fYMoment>>hTotalEvents3Geo(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000",
           "goff",nevents);
   TH1F * hTotal3Geo = (TH1F*)gROOT->FindObject("hTotalEvents3Geo");
   events->fTree->Draw("bigcalClusters.fYMoment>>hPassedEvents3Geo(120,-120,120)",
           "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fLuciteHodoscopeEvent.fLucitePositionHits.fBarNumber*2 - bigcalClusters.fjPeak)<4&&TMath::Abs(fLucitePositionHits.fTDCSum)<200",
           "goff",nevents);
   TH1F * hPassed3Geo = (TH1F*)gROOT->FindObject("hPassedEvents3Geo");
   c3->cd(3);
   TEfficiency * luc3HitEffWithCerBigcalGeo = new TEfficiency(*hPassed3Geo,*hTotal3Geo);
   luc3HitEffWithCerBigcalGeo->SetTitle("1 double timed/Geo  vs Bigcal Y ");
   luc3HitEffWithCerBigcalGeo->Draw();*/

// 
//    /// Lucite Hodoscope with at least 1 double hit with
//    ///  - Timed Cherenkov hit
//    ///  - Cherenkov ADC cut (0.2,2.2)
//    ///  - Bigcal E > 1 GeV
//    events->fTree->Draw("bigcalClusters.fYMoment>>hTotalEvents4(120,-120,120)",
//            "triggerEvent.IsBETAEvent()&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&bigcalClusters.fCherenkovBestADCSum>0.2&&bigcalClusters.fCherenkovBestADCSum<2.2",
//            "goff");
//    TH1F * hTotal4 = (TH1F*)gROOT->FindObject("hTotalEvents4");
//    events->fTree->Draw("bigcalClusters.fYMoment>>hPassedEvents4(120,-120,120)",
//            "triggerEvent.IsBETAEvent()&&TMath::Abs(bigcalClusters.fCherenkovTDC)<15&&bigcalClusters.fTotalE>1000&&TMath::Abs(fLucitePositionHits.fTDCSum)<200&&bigcalClusters.fCherenkovBestADCSum>0.2&&bigcalClusters.fCherenkovBestADCSum<2.2",
//            "goff");
//    TH1F * hPassed4 = (TH1F*)gROOT->FindObject("hPassedEvents4");
//    c1->cd(4);
//    TEfficiency * luc4HitEffWithCerBigcal = new TEfficiency(*hPassed4,*hTotal4);
//    luc4HitEffWithCerBigcal->SetTitle("At least 1 double hit vs Bigcal Y w/ Cer window (0.2,2.2)");
//    luc4HitEffWithCerBigcal->Draw();
//
//



   events->fTree->StartViewer();



   c1->SaveAs(Form("plots/%d/tracker_hit_efficiency.png",runNumber));
   c1->SaveAs(Form("plots/%d/tracker_hit_efficiency.pdf",runNumber));

   c2->SaveAs(Form("plots/%d/tracker_hit_efficiency2.png",runNumber));
   c2->SaveAs(Form("plots/%d/tracker_hit_efficiency2.pdf",runNumber));

   c3->SaveAs(Form("plots/%d/tracker_hit_efficiency3.png",runNumber));
   c3->SaveAs(Form("plots/%d/tracker_hit_efficiency3.pdf",runNumber));

   return(0);

}
