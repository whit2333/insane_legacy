/*! Tracker Histogram Event Display.

 */
//__________________________________________________________________________________

TCanvas * canvas = 0;
TList *   trackerHistograms = 0;
TH2F *    hTracker1 = 0;// new TH2F("hTracker1","Tracker 1Pos Y vs X",100,-12,12,100,-22,22);
TH2F *    hTracker2 = 0;//  new TH2F("hTracker2","Tracker 2Pos Y vs X",100,-12,12,100,-22,22);
TH2F *    hTrackerY1 = 0;// new TH2F("hTrackerY1","Tracker-Y1 Y vs X",100,-12,12,100,-22,22);
TH2F *    hTrackerY2 = 0;//new TH2F("hTrackerY2","Tracker-Y2 Y vs X",100,-12,12,100,-22,22);
TH1F *    hTrackerTDC = 0;
TH2F *    hTrackerTDC2 = 0;
TClonesArray * ft1Pos = 0;// events->BETA->fForwardTrackerEvent->fTrackerPositionHits;
TClonesArray * ft2Pos = 0;//events->BETA->fForwardTrackerEvent->fTracker2PositionHits;
TClonesArray * ftY1Pos = 0;//events->BETA->fForwardTrackerEvent->fTrackerY1PositionHits;
TClonesArray * ftY2Pos = 0;//events->BETA->fForwardTrackerEvent->fTrackerY2PositionHits;
ForwardTrackerPositionHit * aPosHit = 0;
//__________________________________________________________________________________

Int_t tracker_event_update();
Int_t tracker_event_clear(); 
Int_t tracker_event_init(SANEEvents * events, bool nocanvas = 0,TCanvas * c = 0);
Bool_t tracker_event_process(SANEEvents * events);
//__________________________________________________________________________________

Int_t tracker_event_update(){
	if(!trackerHistograms) return(-1);
	if(canvas) {
	       	for( int i = 0; i<trackerHistograms->GetEntries() ; i++) {
			canvas->cd(i+1);
			((TH2F*)trackerHistograms->At(i))->Draw("colz");
		}
 	canvas->cd(trackerHistograms->GetEntries()+1);
	hTrackerTDC->Draw();	
 	canvas->cd(trackerHistograms->GetEntries()+2);
	hTrackerTDC2->Draw("box");	
	canvas->Update();
	}
	//gSystem->Sleep(500);
//	gPad->WaitPrimitive();
	return(0);
}
//__________________________________________________________________________________

Int_t tracker_event_clear() {
	for( int i = 0; i<trackerHistograms->GetEntries() ; i++) {
		((TH2F*)trackerHistograms->At(i))->Reset();
	}
	return(0);
}
//__________________________________________________________________________________

Int_t tracker_event_init(SANEEvents * events, bool nocanvas ,TCanvas * c ){
	/// Histograms
        trackerHistograms = new TList();
	hTracker1 = new TH2F("hTracker1","Tracker 1Pos Y vs X",80,-12,12,100,-22,22);
	hTracker1->SetDirectory(0);
        hTracker2 = new TH2F("hTracker2","Tracker 2Pos Y vs X",80,-12,12,100,0,22);
	hTracker2->SetDirectory(0);
        hTrackerY1 = new TH2F("hTrackerY1","Tracker-Y1 Y vs X",80,-12,12,100,-22,22);
	hTrackerY1->SetDirectory(0);
        hTrackerY2 = new TH2F("hTrackerY2","Tracker-Y2 Y vs X",80,-12,12,100,-22,22);
	hTrackerY2->SetDirectory(0);
        hTrackerTDC = new TH1F("hTrackerTDC","Tracker-TDC",100,-200,200);
	hTrackerTDC->SetDirectory(0);
        hTrackerTDC2 = new TH2F("hTrackerTDC","Tracker-TDC",100,-200,200,20,100,120);
	hTrackerTDC2->SetDirectory(0);
	trackerHistograms->Add(hTracker1);
	trackerHistograms->Add(hTracker2);
	trackerHistograms->Add(hTrackerY1);
	trackerHistograms->Add(hTrackerY2);

	//TCanvas * c = new TCanvas("TrackerEventDisplay","Tracker Event Display");
   if(!nocanvas && !c){
      canvas = new TCanvas("TrackerEventDisplay","Tracker Event Display");
      canvas->Divide(3,2);
   } else if(c) {
      canvas = c;
      canvas->Divide(2,2);
   } else { 
      canvas =  0;
   }

	ft1Pos = events->BETA->fForwardTrackerEvent->fTrackerPositionHits;
	ft2Pos = events->BETA->fForwardTrackerEvent->fTracker2PositionHits;
	ftY1Pos = events->BETA->fForwardTrackerEvent->fTrackerY1PositionHits;
	ftY2Pos = events->BETA->fForwardTrackerEvent->fTrackerY2PositionHits;

	return(0);
}
//__________________________________________________________________________________

Bool_t tracker_event_process(SANEEvents * events){
	if( !(events->TRIG->IsBETAEvent()) ) return false;

	Bool_t up = false;

	for( Int_t j = 0; j < ft1Pos->GetEntries();j++){
		aPosHit = (ForwardTrackerPositionHit*)(*ft1Pos)[j];
		hTracker1->Fill(aPosHit->fPositionVector.X(),aPosHit->fPositionVector.Y());
		up=true;
		hTrackerTDC2->Fill(aPosHit->fTDCSum,aPosHit->fYRow);;

	}
	for( Int_t j = 0; j < ft2Pos->GetEntries();j++){
		aPosHit = (ForwardTrackerPositionHit*)(*ft2Pos)[j];
		hTracker2->Fill(aPosHit->fPositionVector.X(),aPosHit->fPositionVector.Y());
		hTrackerTDC->Fill(aPosHit->fTDCSum);
	}
	for( Int_t j = 0; j < ftY1Pos->GetEntries();j++){
		aPosHit = (ForwardTrackerPositionHit*)(*ftY1Pos)[j];
		hTrackerY1->Fill(aPosHit->fPositionVector.X(),aPosHit->fPositionVector.Y());
	}
	for( Int_t j = 0; j < ftY2Pos->GetEntries();j++){
		aPosHit = (ForwardTrackerPositionHit*)(*ftY2Pos)[j];
		hTrackerY2->Fill(aPosHit->fPositionVector.X(),aPosHit->fPositionVector.Y());
	}




	events->BETA->fForwardTrackerEvent->Print();

	
	
	return(up);
}
//__________________________________________________________________________________

Int_t tracker_event_display2(Int_t runNumber = 72995){

	rman->SetRun(runNumber);

	SANEEvents * ev = new SANEEvents("betaDetectors1");
	if(!ev->fTree)return(-1);
	TTree * t = ev->fTree;
	Int_t nevents = t->GetEntries();
	Int_t nstart = 0;
	std::cout << " Enter starting event : ";
	std::cin >> nstart ;
	std::cout << " \n";

	tracker_event_init(ev);
	Bool_t update = false;
	for(Int_t i = nstart ; i<nevents; i++){
		//if(i%1000==0) 
		//	std::cout << " Entry : " << i << std::endl;

		t->GetEntry(i);
		
		update = tracker_event_process(ev);

	        if(update){
		       ev->TRIG->Print();
		       tracker_event_update();
	               canvas->SaveAs(Form("plots/%d/event_display/tracker_event_%d.png",runNumber,ev->fEventNumber) );	       
		       tracker_event_clear();
	       }

	}

	return(0);
}
//__________________________________________________________________________________

