Int_t with_without(){

   rman->SetRun(4004);
   TTree * t = (TTree*) gROOT->FindObject("betaDetectors1");
   if(!t) return(-1);
   
   gROOT->cd();

   TH1F * h1 =0;

   TCanvas * c = new TCanvas("with_without_tracker","With and Without Tracker");
   c->Divide(2,2);

   c->cd(1);
/*   gPad->SetLogy(true);*/
   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer0(100,2,52)","","goff");
   h1 = (TH1F*) gROOT->FindObject("cer0");
   if(h1){
      h1->SetLineColor(1);
      h1->SetFillColor(1);
      h1->SetFillStyle(3001);
      h1->Draw("");
   }


   c->cd(2);
/*   gPad->SetLogy(true);*/
   t->Draw("bigcalClusters.fCherenkovBestNPEChannel>>cer1(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer1");
   if(h1){
      h1->SetLineColor(2001);
      h1->SetFillColor(2001);
      h1->SetFillStyle(3002);
      h1->Draw("");
   }

   c->cd(3);
   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer2(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer2");
   if(h1){
      h1->SetLineColor(2000);
      h1->SetFillColor(2000);
      h1->SetFillStyle(3003);
      h1->Draw("");
   }

   /// With the tracker
   rman->SetRun(4005);
   TTree * t = (TTree*) gROOT->FindObject("betaDetectors1");
   if(!t) return(-1);
   gROOT->cd();

   c->cd(1);
   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer20(100,2,52)","","goff");
   h1 = (TH1F*) gROOT->FindObject("cer20");
   if(h1){
      h1->SetLineColor(2000);
      h1->Draw("same");
   }

   c->cd(2);
   t->Draw("bigcalClusters.fCherenkovBestNPEChannel>>cer21(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer21");
   if(h1){
      h1->SetLineColor(2002);
      h1->Draw("same");
   }

   c->cd(3);
   t->Draw("bigcalClusters.fCherenkovBestNPESum>>cer22(100,2,52)",
           "TMath::Abs(bigcalClusters.fXMoment)<50&&\
            TMath::Abs(bigcalClusters.fYMoment)<100","goff");
   h1 = (TH1F*) gROOT->FindObject("cer22");
   if(h1){
      h1->SetLineColor(2001);
      h1->Draw("same");
   }



//    t->StartViewer();


}



