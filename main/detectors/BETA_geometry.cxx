Int_t BETA_geometry(){


   ForwardTrackerGeometryCalculator *  ftgeo = ForwardTrackerGeometryCalculator::GetCalculator();
   GasCherenkovGeometryCalculator *    gcgeo = GasCherenkovGeometryCalculator::GetCalculator();
   LuciteGeometryCalculator *          lhgeo = LuciteGeometryCalculator::GetCalculator();
   BIGCALGeometryCalculator *          bcgeo = BIGCALGeometryCalculator::GetCalculator();


   ftgeo->Print();
   gcgeo->Print();
   lhgeo->Print();
   bcgeo->Print();

   return(0);
}
