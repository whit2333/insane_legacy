Int_t tracker_rates(Int_t runNumber = 72999) {

   TH1 * h1 = 0;
   TH2F * h2 = 0;
   TGraph * g1 = 0;
   TProfile * px;
   TProfile * py;
   Double_t max = 0.0;
   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->fCurrentFile->cd();

   TTree * t = (TTree*)gROOT->FindObject("Scalers");

   TCanvas * c = new TCanvas("trackerRates","tracker Rates",1200,1000);
/*   c->Divide(1,2);*/
   c->cd(1);
   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   for(int i = 1;i<=31;i++) {
/*      t->Draw(Form("saneScalerEvent.fTrackerScalers[%d]:saneScalerEvent.fTime>>bc%d",i-1,i),"","goff");*/
      t->Draw(Form("saneScalerEvent.GetTrackerRate(%d):saneScalerEvent.fTime>>bc%d",i-1,i),"","goff");
/*      c->cd(i);*/
      h2 = (TH2F*)gROOT->FindObject(Form("bc%d",i) );
      h2->SetTitle(Form("Tracker scaler %d",i));
      px = h2->ProfileX();
      px->GetYaxis()->SetTitle("rate");
      px->GetXaxis()->SetTitle("seconds");
      px->SetTitle(Form("Tracker scaler %d",i));
      px->SetLineColor(37+ (i%15) );
      px->SetMarkerColor(kRed*(i%2)+kBlue*((i+1)%2)); //37+i);
      px->SetMarkerStyle(19+(i%15));
      px->SetMarkerSize(1.2);
      if(i==1)h1 = px->DrawCopy("lp");
      else h1 =  px->DrawCopy("lpsame");
      if(h1->GetMaximum() > max) {
         max = h1->GetMaximum() +500000;
         h1->SetMaximum(max);
      }
/*      leg->AddEntry(h1,Form("BigCal %d scaler",i));*/
   }
//    leg->Draw();
//    max = 0;
//    c->cd(2);
//    TLegend * leg2 = new TLegend(0.1,0.7,0.48,0.9);
//    for(int i = 211;i<=223;i++) {
// /*      t->Draw(Form("saneScalerEvent.GetBigcalRate(%d):saneScalerEvent.fTime>>cer%d",i-1,i),"","goff");*/
//       t->Draw(Form("saneScalerEvent.fBigcalScalers[%d]:saneScalerEvent.fTime>>bc%d",i-1,i),"","goff");
// /*      c->cd(i);*/
//       h2 = (TH2F*)gROOT->FindObject(Form("bc%d",i) );
//       h2->SetTitle(Form("BigCal scaler %d",i));
//       px = h2->ProfileX();
//       px->GetYaxis()->SetTitle("rate");
//       px->GetXaxis()->SetTitle("seconds");
//       px->SetTitle(Form("BigCal scaler %d",i));
//       px->SetLineColor(35+(i%15)-8);
//       px->SetMarkerColor(kRed*((i-8)%2)+kBlue*((i-8+1)%2)+i-8); //37+i);
//       px->SetMarkerStyle(18+(i%15)-8);
//       px->SetMarkerSize(1.2);
//       if(i==211)h1 = px->DrawCopy("lp");
//       else h1 =  px->DrawCopy("lpsame");
//       if(h1->GetMaximum() > max) {
//          max = h1->GetMaximum() + 5000;
//          h1->SetMaximum(max);
//       }
//       leg2->AddEntry(h1,Form("BigCal %d scaler",i));
//    }
//   leg2->Draw();

   c->SaveAs(Form("plots/%d/TrackerRates.png",runNumber));


return(0);
}