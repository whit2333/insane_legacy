{
   TFile * f = new TFile("data/CalibTest.root","UPDATE");
   Int_t version=0;
   InSANEDetectorCalibration * detCalib = new InSANEDetectorCalibration("TestDetectorCalibration","Test Detector Calibration");
   InSANECalibration * aCalib = 0;
   TParameter<double> * aParam = 0;

   
int i = 0;
   for(int i=0; i<10;i++){
      aCalib = new InSANECalibration(Form("acal%d",i),Form("A Calibration %d",i));
      aCalib->fChannel = i;
      for(int j=0; j<5; j++){
         aParam = new TParameter<double>(Form("par%d_%d",j,i),gRandom->Uniform(0,1));
	 aCalib->AddParameter(aParam);
      }
//      aCalib->fFunction =new TF1(Form("line%d",i),"x*[0]+x*x*[1]",-1,1);
      aCalib->GetFunction()->SetParameters( ((TParameter<double>*)aCalib->GetParameter(0))->GetVal(),
                                           ((TParameter<double>*)aCalib->GetParameter(3))->GetVal());
      detCalib->AddCalibration(aCalib);
   }

//   aCalib->Write(Form("%s%d",aCalib->GetName(),version),TObject::kWriteDelete);

   detCalib->Write(Form("%s%d",detCalib->GetName(),version),TObject::kWriteDelete);

//   new TBrowser();
}
