Int_t rates_cherenkov1(Int_t runNumber = 72582) {

   TH1 * h1 = 0;
   TH2F * h2 = 0;
   TGraph * g1 = 0;
   TProfile * px;
   TProfile * py;
   Double_t max = 0.0;
   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fRunNumber;
   rman->GetScalerFile()->cd();


   TTree * t = (TTree*)gROOT->FindObject("Scalers1");
   if(!t) return(-1);
   TCanvas * c = new TCanvas("cherenkovRates","Cherenkov Rates",1200,1000);
   c->Divide(1,2);
   c->cd(1);
   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   for(int i = 1;i<=8;i++) {
      t->Draw(Form("saneScalerEvent.GetCherenkovRate(%d):saneScalerEvent.fTime>>cer%d",i-1,i),"","goff");
/*      c->cd(i);*/
      h2 = (TH2F*)gROOT->FindObject(Form("cer%d",i) );
      h2->SetTitle(Form("Cherenkov scaler %d",i));
      px = h2->ProfileX();
      px->GetYaxis()->SetTitle("rate");
      px->GetXaxis()->SetTitle("seconds");
      px->SetTitle(Form("Cherenkov scaler %d",i));
      px->SetLineColor(37+i);
      px->SetMarkerColor(kRed*(i%2)+kBlue*((i+1)%2)+i); //37+i);
      px->SetMarkerStyle(19+i);
      px->SetMarkerSize(1.2);
      if(i==1)h1 = px->DrawCopy("lp");
      else h1 =  px->DrawCopy("lpsame");
      if(h1->GetMaximum() > max) {
         max = h1->GetMaximum()+ 200000;
         h1->SetMaximum(max);
      }
      leg->AddEntry(h1,Form("cer %d scaler",i));
   }
   leg->Draw();
   max = 0;
   c->cd(2);
   TLegend * leg2 = new TLegend(0.1,0.7,0.48,0.9);
   for(int i = 9;i<=12;i++) {
      t->Draw(Form("saneScalerEvent.GetCherenkovRate(%d):saneScalerEvent.fTime>>cer%d",i-1,i),"","goff");
/*      c->cd(i);*/
      h2 = (TH2F*)gROOT->FindObject(Form("cer%d",i) );
      h2->SetTitle(Form("Cherenkov scaler %d",i));
      px = h2->ProfileX();
      px->GetYaxis()->SetTitle("rate");
      px->GetXaxis()->SetTitle("seconds");
      px->SetTitle(Form("Cherenkov scaler %d",i));
      px->SetLineColor(35+i-8);
      px->SetMarkerColor(kRed*((i-8)%2)+kBlue*((i-8+1)%2)+i-8); //37+i);
      px->SetMarkerStyle(18+i-8);
      px->SetMarkerSize(1.2);
      if(i==9)h1 = px->DrawCopy("lp");
      else h1 =  px->DrawCopy("lpsame");
      if(h1->GetMaximum() > max) {
         max = h1->GetMaximum() + 200000;
         h1->SetMaximum(max);
      }
      leg2->AddEntry(h1,Form("cer %d scaler",i));
   }
  leg2->Draw();

   c->SaveAs(Form("plots/%d/rates_cherenkov1.png",runNumber));


return(0);
}
