Int_t cherenkov_timing(){
   InSANEDatabaseManager * dbManager= SANEDatabaseManager::GetManager();
   TSQLServer * db = dbManager->dbServer;


   TFile * cerFile = new TFile("data/cherenkov_timing.root","UPDATE");

   TCanvas * c = new TCanvas("cherenkovTiming","Cherenkov TDC Peaks",1200,800);
   TH1F * h1;
   TH1F * h2;
   TH2F * h3;
   THStack * hs = new THStack("ceriteTimings","Lucite Timings");
   Double_t max = 0;


   /// Look at individual PMTs of cherenkov
   TH2F  * cerTDCWidthvsRunAll  = new TH2F("cerTDCWidthvsRunAll","cerTDCWidthvsRunAll",200,72200,73050,100,-100, 600);
   TH2F  * cerTDCPeakvsRunAll  = new TH2F("cerTDCPeakvsRunAll","cerTDCPeakvsRunAll",200,72200,73050,100,-2500, -1400);

   for(int i = 0;i<8;i++) {

      TString SQLCOMMAND(
         Form("SELECT run_number,tdc_mean,tdc_width FROM %s.detector_timing where tdc_number=%d AND detector='cherenkov'", dbManager->GetDatabaseName(),i) ); 
      SQLCOMMAND +=" ;";

      h1 = new TH1F(Form("cerTDC%d",i),Form("Lucite %d TDC",i),100,-2200, -1400);
      h2 = new TH1F(Form("cerTDCWidth%d",i),Form("Lucite %d TDC width",i),50,0, 1000);
      h3 = new TH2F(Form("ccerTDCWidthvsRun%d",i),Form("Lucite %d TDC width vs Run",i),200,72200,73050,100,-2000, -1600);
//       h3 = new TH2F(Form("ccerTDCWidthvsPeak%d",i),Form("Lucite %d TDC width vs ped",i),100,0,1000,50,0,100);

      if(db) {
         TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
         TSQLRow * row;
         while(row = res->Next()) {
         cerTDCPeakvsRunAll->Fill(atof(row->GetField(0)),atof(row->GetField(1)));
         cerTDCWidthvsRunAll->Fill(atof(row->GetField(0)),atof(row->GetField(2)));
         h1->Fill(atof(row->GetField(1)) );
         h2->Fill(atof(row->GetField(2)) );
         h3->Fill(atof(row->GetField(0)),atof(row->GetField(1)));
/*         h2->Fill(atoi(rosw->GetField(0)),atof(row->GetField(1)) );*/
/*         std::cout << " mean " << atof(row->GetField(1)) << "\n";*/
         }
      }
      h1->SetLineColor(i);
      h2->SetLineColor(1);
//       if(h1->GetMaximum() > max){max = h1->GetMaximum(); h1->SetMaximum(max+5); }
//       if(i==0) h1->Draw();
//       else h1->Draw("same");
      hs->Add(h1);
//       h1->Draw();
//       h2->Draw("same");
//        h3->Draw("colz");
   }

   /// Look at individual PMTs of cherenkov
   TH2F  * cerTDCWidthvsRunSums  = new TH2F("cerTDCWidthvsRunSums","cerTDCWidthvsRunSums",200,72200,73050,100,-100, 600);
   TH2F  * cerTDCPeakvsRunSums  = new TH2F("cerTDCPeakvsRunSums","cerTDCPeakvsRunSums",200,72200,73050,100,-2200, -1400);

   for(int i = 8;i<12;i++) {

      TString SQLCOMMAND(
         Form("SELECT run_number,tdc_mean,tdc_width FROM %s.detector_timing where tdc_number=%d AND detector='cherenkov'", dbManager->GetDatabaseName(),i) ); 
      SQLCOMMAND +=" ;";

      h1 = new TH1F(Form("cerTDC%d",i),Form("Lucite %d TDC",i),100,-2200, -1400);
      h2 = new TH1F(Form("cerTDCWidth%d",i),Form("Lucite %d TDC width",i),50,0, 1000);
      h3 = new TH2F(Form("ccerTDCWidthvsRun%d",i),Form("Lucite %d TDC width vs Run",i),200,72200,73050,100,-2000, -1600);
//       h3 = new TH2F(Form("ccerTDCWidthvsPeak%d",i),Form("Lucite %d TDC width vs ped",i),100,0,1000,50,0,100);

      if(db) {
         TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
         TSQLRow * row;
         while(row = res->Next()) {
         cerTDCPeakvsRunSums->Fill(atof(row->GetField(0)),atof(row->GetField(1)));
         cerTDCWidthvsRunSums->Fill(atof(row->GetField(0)),atof(row->GetField(2)));
         h1->Fill(atof(row->GetField(1)) );
         h2->Fill(atof(row->GetField(2)) );
         h3->Fill(atof(row->GetField(0)),atof(row->GetField(1)));
/*         h2->Fill(atoi(rosw->GetField(0)),atof(row->GetField(1)) );*/
/*         std::cout << " mean " << atof(row->GetField(1)) << "\n";*/
         }
      }
      h1->SetLineColor(i);
      h2->SetLineColor(1);
//       if(h1->GetMaximum() > max){max = h1->GetMaximum(); h1->SetMaximum(max+5); }
//       if(i==0) h1->Draw();
//       else h1->Draw("same");
      hs->Add(h1);
//       h1->Draw();
//       h2->Draw("same");
//        h3->Draw("colz");
   }


   c->Divide(2,2);
//    c->cd(1);
//    hs->Draw("nostack");
   c->cd(1);
   cerTDCPeakvsRunAll->Draw("colz");
   c->cd(3);
   cerTDCWidthvsRunAll->Draw("colz");

   c->cd(2);
   cerTDCPeakvsRunSums->Draw("colz");
   c->cd(4);
   cerTDCWidthvsRunSums->Draw("colz");

//    c->cd(3);
//    cerTDCDiffPeakvsRunAll->Draw("colz");
//    c->cd(6);
//    cerTDCDiffWidthvsRunAll->Draw("colz");


   
   return(0);

}