/*! Cherenkov Histogram Event Display.

 */
//__________________________________________________________________________________

TCanvas * fCherenkovEventCanvas = 0;
TList *   cherenkovHistograms = 0;
TH2F *    hCherenkov1 = 0;// new TH2F("hCherenkov1","Cherenkov 1Pos Y vs X",100,-12,12,100,-22,22);
TH2F *    hCherenkov2 = 0;//  new TH2F("hCherenkov2","Cherenkov 2Pos Y vs X",100,-12,12,100,-22,22);
TH2F *    hCherenkovY1 = 0;// new TH2F("hCherenkovY1","Cherenkov-Y1 Y vs X",100,-12,12,100,-22,22);
TH2F *    hCherenkovY2 = 0;//new TH2F("hCherenkovY2","Cherenkov-Y2 Y vs X",100,-12,12,100,-22,22);
TClonesArray * lh1Pos = 0;// events->BETA->fForwardCherenkovEvent->fCherenkovPositionHits;
TClonesArray * lh2Pos = 0;//events->BETA->fForwardCherenkovEvent->fCherenkov2PositionHits;
TClonesArray * lhY1Pos = 0;//events->BETA->fForwardCherenkovEvent->fCherenkovY1PositionHits;
TClonesArray * lhY2Pos = 0;//events->BETA->fForwardCherenkovEvent->fCherenkovY2PositionHits;
//__________________________________________________________________________________

Int_t  cherenkov_event_update();
Int_t  cherenkov_event_clear(); 
Int_t  cherenkov_event_init(SANEEvents * events, bool nocanvas = 0,TCanvas * c = 0);
Bool_t cherenkov_event_process(SANEEvents * events);
//__________________________________________________________________________________

Int_t cherenkov_event_update(){
	if(!cherenkovHistograms) return(-1);
	if(fCherenkovEventCanvas) {
	       	for( int i = 0; i<cherenkovHistograms->GetEntries() ; i++) {
			fCherenkovEventCanvas->cd(i+1);
			((TH2F*)cherenkovHistograms->At(i))->Draw("colz");
		}
	fCherenkovEventCanvas->Update();
	}
	//gSystem->Sleep(500);
//	gPad->WaitPrimitive();
	return(0);
}
//__________________________________________________________________________________

Int_t cherenkov_event_clear() {
	for( int i = 0; i<cherenkovHistograms->GetEntries() ; i++) {
		((TH2F*)cherenkovHistograms->At(i))->Reset();
	}
	return(0);
}
//__________________________________________________________________________________

Int_t cherenkov_event_init(SANEEvents * events, bool nocanvas ,TCanvas * c ){
	/// Histograms
        cherenkovHistograms = new TList();
	hCherenkov1 = new TH2F("hCherenkov1","Cherenkov Pos Y vs X",50,-50,50,60,-120,120);
	hCherenkov1->SetDirectory(0);
        hCherenkov2 = new TH2F("hCherenkov2","Cherenkov 2Pos Y vs X",80,-12,12,100,-22,22);
	hCherenkov2->SetDirectory(0);
        hCherenkovY1 = new TH2F("hCherenkovY1","Cherenkov-Y1 Y vs X",80,-12,12,100,-22,22);
	hCherenkovY1->SetDirectory(0);
        hCherenkovY2 = new TH2F("hCherenkovY2","Cherenkov-Y2 Y vs X",80,-12,12,100,-22,22);
	hCherenkovY2->SetDirectory(0);
	cherenkovHistograms->Add(hCherenkov1);
	cherenkovHistograms->Add(hCherenkov2);
	cherenkovHistograms->Add(hCherenkovY1);
	cherenkovHistograms->Add(hCherenkovY2);

	//TCanvas * c = new TCanvas("CherenkovEventDisplay","Cherenkov Event Display");
   if(!nocanvas && !c){
      fCherenkovEventCanvas = new TCanvas("CherenkovEventDisplay","Cherenkov Event Display");
      fCherenkovEventCanvas->Divide(2,2);
   } else if(c) {
      fCherenkovEventCanvas = c;
      fCherenkovEventCanvas->Divide(2,2);
   } else { 
      fCherenkovEventCanvas =  0;
   }

	lh1Pos = events->BETA->fCherenkovHodoscopeEvent->fCherenkovPositionHits;
// 	lh2Pos = events->BETA->fForwardCherenkovEvent->fCherenkov2PositionHits;
// 	lhY1Pos = events->BETA->fForwardCherenkovEvent->fCherenkovY1PositionHits;
// 	lhY2Pos = events->BETA->fForwardCherenkovEvent->fCherenkovY2PositionHits;

	return(0);
}
//__________________________________________________________________________________

Bool_t cherenkov_event_process(SANEEvents * events){
	if( !(events->TRIG->IsBETAEvent()) ) return false;
    CherenkovHodoscopePositionHit * aPosHit = 0;

	Bool_t up = false;

	for( Int_t j = 0; j < lh1Pos->GetEntries();j++){
		aPosHit = (CherenkovHodoscopePositionHit*)(*lh1Pos)[j];
		hCherenkov1->Fill(aPosHit->fPositionVector.X(),aPosHit->fPositionVector.Y());
		up=true;
	}
// 	for( Int_t j = 0; j < ft2Pos->GetEntries();j++){
// 		aPosHit = (ForwardCherenkovPositionHit*)(*ft2Pos)[j];
// 		hCherenkov2->Fill(aPosHit->fPositionVector.X(),aPosHit->fPositionVector.Y());
// 	}
// 	for( Int_t j = 0; j < ftY1Pos->GetEntries();j++){
// 		aPosHit = (ForwardCherenkovPositionHit*)(*ftY1Pos)[j];
// 		hCherenkovY1->Fill(aPosHit->fPositionVector.X(),aPosHit->fPositionVector.Y());
// 	}
// 	for( Int_t j = 0; j < ftY2Pos->GetEntries();j++){
// 		aPosHit = (ForwardCherenkovPositionHit*)(*ftY2Pos)[j];
// 		hCherenkovY2->Fill(aPosHit->fPositionVector.X(),aPosHit->fPositionVector.Y());
// 	}
        std::cout << "update " << up << "\n";
	return(up);
}
//__________________________________________________________________________________

Int_t cherenkov_event_display1(Int_t runNumber = 72995){

	rman->SetRun(runNumber);

	SANEEvents * ev = new SANEEvents("betaDetectors1");
	if(!ev->fTree)return(-1);
	TTree * t = ev->fTree;
	Int_t nevents = t->GetEntries();
	Int_t nstart = 0;
	std::cout << " Enter starting event : ";
	std::cin >> nstart ;
	std::cout << " \n";

	cherenkov_event_init(ev);
	Bool_t update = false;
	for(Int_t i = nstart ; i<nevents; i++){
		//if(i%1000==0) 
		//	std::cout << " Entry : " << i << std::endl;

		t->GetEntry(i);
		
		update = cherenkov_event_process(ev);

	        if(update){
		       ev->TRIG->Print();
		       cherenkov_event_update();
	               fCherenkovEventCanvas->SaveAs(Form("plots/%d/event_display/cherenkov_event_%d.png",runNumber,ev->fEventNumber) );	       
		       cherenkov_event_clear();
	       }

	}

	return(0);
}
//__________________________________________________________________________________

