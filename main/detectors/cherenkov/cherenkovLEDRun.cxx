/*!  Analyzes a SANE Cherenkov LED Run.
 
 - 72137
 - 72101 through 72138 are Cherenkov LED runs while ramping
 - 71992 through 72015 are Cherenkov LED runs. https://hallcweb.jlab.org/hclog/0901_archive/090126200932.html.
   However PMT 5 problems persist.
 - 72023 through 72047 "cherenkov test" 
 - 72063 through 72067 "cherenkov test" 
 - 72101 through 72138 Tests with iron plate 

 */
Int_t cherenkovLEDRun(Int_t runNumber=72137,Int_t isACalibration=0) {

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if(!t) {printf("betaDetectors0 tree not found\n"); return(-1); }

   TFile*f = new TFile(Form("data/rootfiles/LED%d.root",runNumber),"UPDATE");
//     gROOT->SetBatch(kTRUE);
  //TSQLServer * db = SANEDatabaseManager::GetManager()->dbServer;
  //TString SQLCOMMAND(""); 

  TCanvas * c = new TCanvas("cherenkovLED","Cherenkov LED",400,800);
  c->Divide(2,4);
  TH1F * aMirrorHist;
  TFitResultPtr fitResult;
  TF1 *f1 = new TF1("f1", "gaus", 0,6000);
  f1->SetParameter(0,1000);
  f1->SetParameter(1,500);
  f1->SetParameter(2,300);

  Double_t means[8];

  for(int i = 0; i<8;i++) {
    c->cd(i+1);
    t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(100,5,2005)",i+1),Form(
            "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d",i+1),"",1000000000,  1001);
    aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist",i+1));
    if(aMirrorHist){
       f1->SetParameter(1,aMirrorHist->GetMean(1));
       fitResult = aMirrorHist->Fit("gaus","M,E,S");
       means[i] = fitResult->Parameter(1);
/*
 SQLCOMMAND ="REPLACE into cherenkov_calibrations set `run_number`=";
 SQLCOMMAND += runnumber;

 SQLCOMMAND +=",pmt_number="; 
 SQLCOMMAND +=i+1;

 SQLCOMMAND +=",value="; 
 SQLCOMMAND +=fitResult->Parameter(1);

 SQLCOMMAND +=",calibration_type="; 
 SQLCOMMAND +="'relative'";

 SQLCOMMAND +=" ;"; 
 if(db) db->Query(SQLCOMMAND.Data());
*/
    aMirrorHist->SetTitle(Form("Cherenkov Mirror %d",i+1));
    aMirrorHist->Draw();
     }

  }

  c->SaveAs(Form("plots/%d/CherenkovLED.ps",runNumber));
  c->SaveAs(Form("plots/%d/CherenkovLED.jpg",runNumber));

//     gROOT->SetBatch(kFALSE);

  std::cout << " { ";
  for(int i = 0;i<8; i++){
     std::cout << means[i] << ",";
  }
  std::cout << " } \n";

f->Write();


return(0);
}
