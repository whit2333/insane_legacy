#INSERT INTO cherenkov_performance 
#        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
#SELECT  -2,npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999;

DELETE FROM cherenkov_performance where run_number=-2 limit 8;

INSERT INTO cherenkov_performance 
        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
SELECT  -2,1.0*npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999 AND channel=1;

INSERT INTO cherenkov_performance 
        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
SELECT  -2,1.0*npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999 AND channel=2;

INSERT INTO cherenkov_performance 
        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
SELECT  -2,0.7*npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999 AND channel=3;

INSERT INTO cherenkov_performance 
        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
SELECT  -2,1.0*npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999 AND channel=4;

INSERT INTO cherenkov_performance 
        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
SELECT  -2,0.9*npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999 AND channel=5;

INSERT INTO cherenkov_performance 
        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
SELECT  -2,1.0*npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999 AND channel=6;

INSERT INTO cherenkov_performance 
        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
SELECT  -2,0.7*npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999 AND channel=7;

INSERT INTO cherenkov_performance 
        (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
SELECT  -2,1.0*npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel FROM cherenkov_performance WHERE run_number=72999 AND channel=8;

#INSERT INTO cherenkov_performance (run_number, npe_1_mean,npe_1_width,npe_1_amp,npe_2_width,npe_2_amp,channel) 
#       VALUES (-3, 1800,500,100,3600,100,10, 1);
