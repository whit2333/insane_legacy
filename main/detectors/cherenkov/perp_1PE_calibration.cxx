/*!  The calibration coefficients below are from
     https://hallcweb.jlab.org/hclog/0812_archive/081220133641.html
     correct for lowered gains during perpendicular runs
 */
Int_t perp_1PE_calibration() {
   
   TFile * f = new TFile("data/CherenkovCalibrations.root","UPDATE");
   const char * calibname = "PerpCherenkov1PECalib";
   InSANEDetectorCalibration * cer1PECalibration =
      new InSANEDetectorCalibration(calibname,"Perpendicular Cherenkov 1PE Calibration");

   InSANECalibration * cerpmt[8];
   TParameter<double> * cer[8];

   cer[0] = new TParameter<double>("1PEpeak",106.184*0.95);
   cer[1] = new TParameter<double>("1PEpeak",104.39*0.95);
   cer[2] = new TParameter<double>("1PEpeak",98.5437*0.55);
   cer[3] = new TParameter<double>("1PEpeak",100.831*0.85);
   cer[4] = new TParameter<double>("1PEpeak",99.8849*0.85);
   cer[5] = new TParameter<double>("1PEpeak",102.702*0.80);
   cer[6] = new TParameter<double>("1PEpeak",102.49*0.60);
   cer[7] = new TParameter<double>("1PEpeak",95.8557*0.83);

   for(int i=0;i<8;i++){
      cerpmt[i] = new InSANECalibration(Form("PMT-%d-1PE",i+1),Form("PMT-%d-1PE",i+1));
      cerpmt[i]->Clear();
      cerpmt[i]->AddParameter(cer[i]);
      cerpmt[i]->SetChannel(i+1);
      cer1PECalibration->AddCalibration(cerpmt[i]);
   }

   cer1PECalibration->Write(calibname, TObject::kWriteDelete);

   return(0);
}
