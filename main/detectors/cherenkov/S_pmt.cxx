/**
 * NIMA 451 (2000) 623}637
 */
class  PMTResponse {
   protected:
      Int_t fNmax;
      Double_t fA,fN0,fP0,fP1,fpe,fxp,fsigp,fx0,fsig0,fx1,fsig1,fmu;
      Double_t fgN;
      TF1 * fFit_low_light;
      TF1 * fFit;

   public:
      PMTResponse(){
         fA = 10.0;
         //fN0;fP0,fP1,fpe,fxp,fsigp,fx0,fsig0,fx1,fsig1,fmu;
         fFit_low_light = 0;
         fFit = 0;
         fNmax = 20;
      }
      ~PMTResponse(){
      }

      Int_t fNmax;
      Double_t fA,fN0,fP0,fP1,fpe,fxp,fsigp,fx0,fsig0,fx1,fsig1,fmu;
      Double_t fgN;

      TF1 * fFit_low_light;
      TF1 * fFit;

      /** Functor  not used.
       */
      double operator() (double *x, double *p) {
         // function implementation using class data members
         return(0);
      }

      /** Sets all the internal parameters.*/
      void SetParameters(double *p){
         fN0   = p[0];
         fP0   = p[1];
         fP1   = p[2];
         fA    = p[3]; //slope of exponential part
         fpe   = p[4]; // fraction of ents under the exponential
         fxp   = p[5]; // ped
         fsigp = p[6]; // ped
         fx0   = p[7]; // spe
         fsig0 = p[8]; // spe
         fmu   = p[9];
         fx1   = p[10];
         fsig1 = p[11];
         fgN  = (1.0+TMath::Erf(fx0/(TMath::Sqrt(2.0)*fsig0)))/2.0;
      }

      void SetParameters2(double *p){
         fN0   = p[0];
         fP0   = p[1];
         fA    = p[2]; //slope of exponential part
         fpe   = p[3]; // fraction of ents under the exponential
         fxp   = p[4]; // ped
         fsigp = p[5]; // ped
         fx0   = p[6]; // spe
         fsig0 = p[7]; // spe
         fmu   = p[8];
         fx1   = fx0;//p[10];
         fsig1 = fsig0;//p[11];
         fP1   = TMath::Poisson(1.0,fmu);//p[2];
         fgN  = (1.0+TMath::Erf(fx0/(TMath::Sqrt(2.0)*fsig0)))/2.0;
      }

      /** PMT response fit function with 12 parameters.
       */
      double PMT_fit(double *x, double *p){
         Double_t res  = 0;
         Double_t xx   = x[0];

         fN0   = p[0];
         fP0   = p[1];
         fP1   = p[2];
         fA    = p[3]; //slope of exponential part
         fpe   = p[4]; // fraction of ents under the exponential
         fxp   = p[5]; // ped
         fsigp = p[6]; // ped
         fx0   = p[7]; // spe
         fsig0 = p[8]; // spe
         fmu   = p[9];
         fx1   = p[10];
         fsig1 = p[11];

         Double_t Mpars[] = {fmu,fxp,fx1,fsig1};

         Double_t Noise   = TMath::Gaus(xx,fxp,fsigp,kTRUE);
         Double_t SER     = Ser_pmt(x,&p[3]);
         Double_t M       = M_pmt(x,Mpars);

         res = fN0*(fP0*Noise + fP1*SER + M);
         return(res);
      }

      /** PMT response fit function with 9 parameters.
       */
      double PMT_fit2(double *x, double *p){
         Double_t res  = 0;
         Double_t xx   = x[0];

         fN0   = p[0];
         fP0   = p[1];
         fA    = p[2]; //slope of exponential part
         fpe   = p[3]; // fraction of ents under the exponential
         fxp   = p[4]; // ped
         fsigp = p[5]; // ped
         fx0   = p[6]; // spe
         fsig0 = p[7]; // spe
         fmu   = p[8];
         fx1   = fx0;//p[10];
         fsig1 = fsig0;//p[11];
         fP1   = TMath::Poisson(1.0,fmu);//p[2];

         Double_t Mpars[] = {fmu,fxp,fx1,fsig1};

         Double_t Noise   = TMath::Gaus(xx,fxp,fsigp,kTRUE);
         Double_t SER     = Ser_pmt(x,&p[2]);
         Double_t M       = M_pmt(x,Mpars);

         res = fN0*(fP0*Noise + fP1*SER + M);
         return(res);
      }


      /** Following the NIM paper http://users.lngs.infn.it/~ianni/PECounting-paper.pdf
       *
       * Single electron response
       */
      double Ser_pmt(double *x, double *p){
         Double_t res = 0;
         Double_t xx   = x[0];
         Double_t A    = p[0]; //slope of exponential part
         Double_t pe   = p[1]; // fraction of ents under the exponential
         Double_t xp   = p[2]; // ped
         Double_t sigp = p[3]; // ped
         Double_t x0   = p[4]; // spe
         Double_t sig0 = p[5]; // spe

         Double_t pedexp = (pe/(2.0*A))*TMath::Exp((sigp*sigp-2.0*A*(xx-xp))/(2.0*A*A))*(1.0+TMath::Erf((A*xp-sigp*sigp)/(TMath::Sqrt(2.0)*A*sigp)));
         fgN  = (1.0+TMath::Erf(x0/(TMath::Sqrt(2.0)*sig0)))/2.0;
         Double_t spegaus = ((1.0-pe)/fgN)*TMath::Gaus(xx,x0+xp,sig0,kTRUE);
         res = pedexp + spegaus;
         if(xx < fxp) res = 0;
         return(res);
      }

      /** Single electron response includes exponential bg and one pe peak. */
      double Ser_exp(double *x, double *p){
         Double_t res = 0;
         Double_t xx   = x[0];
         Double_t A    = p[0]; //slope of exponential part
         Double_t pe   = p[1]; // fraction of ents under the exponential
         Double_t xp   = p[2]; // ped
         Double_t sigp = p[3]; // ped
         Double_t x0   = p[4]; // spe
         Double_t sig0 = p[5]; // spe

         double pedexp = (pe/(2.0*A))*TMath::Exp((sigp*sigp-2.0*A*(xx-xp))/(2.0*A*A))*(1.0+TMath::Erf((A*xp-sigp*sigp)/(TMath::Sqrt(2.0)*A*sigp)));
         if(xx < xp) pedexp = 0;
         return(pedexp);
      }

      /** PMT response for n>2.
       *
       * @param p[0] mu
       * @param p[1] xp
       * @param p[2] x1
       * @param p[3] sig1
       */
      double M_pmt(double *x, double *p){
         double res  = 0;
         Double_t xx   = x[0];
         Double_t mu   = p[0];
         Double_t xp   = p[1];
         Double_t x1   = p[2];
         Double_t sig1 = p[3];

         for(int i = 2; i<=fNmax; i++){
            double n = (double)i;
            res += TMath::Poisson(n,fmu)*TMath::Gaus(xx,n*fx1+fxp,TMath::Sqrt(n)*fsig1,kTRUE);
         }
         if(xx<xp) res = 0;
         return(res);
      }


      /** Light light version of PMT_fit with 10 parameters.
       * 
       */
      double PMT_fit_low_light(double *x, double *p){
         Double_t res  = 0;
         Double_t xx   = x[0];
         fN0   = p[0];
         fP0   = p[1];
         fP1   = p[2];
         fA    = p[3]; //slope of exponential part
         fpe   = p[4]; // fraction of ents under the exponential
         fxp   = p[5]; // ped
         fsigp = p[6]; // ped
         fx0   = p[7]; // spe
         fsig0 = p[8]; // spe
         fmu   = p[9];
         fx1   = (1.0-fpe)*fx0+fpe*fA;
         fsig1 = (1.0-fpe)*(fsig0*fsig0 + fx0*fx0) + 2.0*fpe*fA*fA - fx1*fx1;

         Double_t Mpars[] = {fmu,fxp,fx1,fsig1};

         Double_t Noise   = TMath::Gaus(xx,fxp,fsigp,kTRUE);
         Double_t SER     = Ser_pmt(x,&p[3]);
         Double_t M       = M_pmt(x,Mpars);

         res = fN0*(fP0*Noise + fP1*SER + M);
         return(res);
      }

      TF1 * GetPeakFunction(Int_t n = 1){
         TF1 *f1 = 0;
         if(n==0) {
            f1 = new TF1(Form("pmt-peak-%d",n),"[0]*TMath::Gaus(x,[1],[2],1)",-25,2000);
            f1->SetParameter(0,fN0*fP0);
            f1->SetParameter(1,fxp);
            f1->SetParameter(2,fsigp);
         } else if(n==1) {
            f1 = new TF1(Form("pmt-peak-%d",n),"[0]*TMath::Gaus(x,[1],[2],1)",-25,2000);
            f1->SetParameter(0,fN0*fP1*((1.0-fpe)/fgN));
            f1->SetParameter(1,fx0+fxp);
            f1->SetParameter(2,fsig0);
         } else {
            f1 = new TF1(Form("pmt-peak-%d",n),"[0]*TMath::Gaus(x,[1],[2],1)",-25,2000);
            f1->SetParameter(0,fN0*TMath::Poisson(n,fmu));
            f1->SetParameter(1,n*fx1+fxp);
            f1->SetParameter(2,TMath::Sqrt(n)*fsig1);
         } 
         return(f1);
      }

      TF1 * GetExpFunction(){
         TF1 *f1 = new TF1("pmt-exp",this,&PMTResponse::Ser_exp,0,2000,6,"PMTResponse","Ser_exp");
            f1->SetParameter(0,fA);
            f1->SetParameter(1,fpe);
            f1->SetParameter(2,fxp);
            f1->SetParameter(3,fsigp);
            f1->SetParameter(4,fx0);
            f1->SetParameter(5,fsig0);
         return(f1);
      }

};


