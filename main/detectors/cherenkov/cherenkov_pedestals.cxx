{
   InSANEDatabaseManager * dbManager= SANEDatabaseManager::GetManager();
   TSQLServer * db = dbManager->dbServer;

   TCanvas * c = new TCanvas("cerPeaksVsRun","Cherenkov Peaks",1200,1000);
   c->Divide(2,4);
   TH1F * h1;
   TH1F * h2;
   TH2F * h3;

   Double_t max = 0;
   for(int i = 1;i<9;i++) {
   c->cd(i);


      TString SQLCOMMAND( 
         Form("SELECT run_number,ped_mean,ped_width FROM %s.detector_pedestals where ped_number=%d", dbManager->GetDatabaseName(),i-1) ); 
      SQLCOMMAND +=" ;";

      h1 = new TH1F(Form("cerPed%d",i),Form("Cherenkov %d pedestal",i),50,0, 1000);
      h2 = new TH1F(Form("cerPedWidth%d",i),Form("Cherenkov %d pedestal width",i),50,0, 1000);
      h3 = new TH2F(Form("cerWidthvsPed%d",i),Form("Cherenkov %d ped width vs ped",i),100,0,1000,50,0,100);

      if(db) {
         TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
         TSQLRow * row;
         while(row = res->Next()) {
         h1->Fill(atof(row->GetField(1)) );
         h2->Fill(atof(row->GetField(2)) );
         h3->Fill(atof(row->GetField(1)),atof(row->GetField(2)));
/*         h2->Fill(atoi(row->GetField(0)),atof(row->GetField(1)) );*/
/*         std::cout << " mean " << atof(row->GetField(1)) << "\n";*/
         }
      }
      h1->SetLineColor(i);
      h2->SetLineColor(1);
      if(h1->GetMaximum() > max){max = h1->GetMaximum(); h1->SetMaximum(max+5); }
//       if(i==1) h1->Draw();
//       else h1->Draw("same");
//       h1->Draw();
//       h2->Draw("same");
       h3->Draw("colz");
   }
//    c->cd(2);
//    h2->Draw();

   return(0);
}

}