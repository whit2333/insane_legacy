Int_t cherenkov_performance(Int_t runNumber=72996) {

   rman->SetRun(runNumber);

   const char * treeName = "betaTrajectories";

   TTree * t = (TTree*)gROOT->FindObject(treeName);
   if(!t){ std::cout << " TREE NOT FOUND : " << treeName << "\n"; return(-1);}

   InSANEReconstructedEvent * aRecon = new InSANEReconstructedEvent();
   BIGCALCluster * aCluster= 0;
   t->SetBranchAddress("fReconEvent",&aRecon);

   TDirectory * cerPerformanceDir=0;
   TFile * cerFile = new TFile("data/cherenkov_performance.root","UPDATE");
   if( cerFile ) {
      if( !(cerFile->cd(Form("cerPerformance%d",runNumber)) ) ) {
         cerPerformanceDir = cerFile->mkdir(Form("cerPerformance%d",runNumber)) ;
         if( !(cerPerformanceDir->cd()) ) printf(" dir Error\n");
      } else {
         cerPerformanceDir = gDirectory;
      }
   }

   TH1F * hCerNPESingle[8];
   TH1F * hCerNPESum[8];
   TH1F * hCerSignalSum[8];
   TH1F * hCerSignalSingle[8];
   TEfficiency * hCerSignalEfficiencyVsEnergy[8];
   TEfficiency * hCerSignalEfficiencyVsY[8];
   TEfficiency * hCerSignalEfficiencyVsX[8];

   for(int i = 0; i<8;i++) {
      hCerNPESum[i] = new TH1F(Form("Cer-%d-NPE Sum",i+1),Form("Cer -%d- NPE Sum",i+1),200,1,45);
      hCerNPESingle[i] = new TH1F(Form("Cer-%d-NPE",i+1),Form("Cer -%d- NPE",i+1),200,1,45);      hCerSignalSum[i] = new TH1F(Form("Cer-%d-Sum",i+1),Form("Cer -%d- Sum",i+1),200,0.02,4);
      hCerSignalSingle[i] = new TH1F(Form("Cer-%d-",i+1),Form("Cer -%d-",i+1),200,0.02,4);

      hCerSignalEfficiencyVsEnergy[i] = new TEfficiency(Form("Cer-%d-EffVsEnergy",i+1),Form("Cer -%d- EffVsEnergy",i+1),50,500,4500);
      hCerSignalEfficiencyVsY[i] = new TEfficiency(Form("Cer-%d-EffVsY",i+1),Form("Cer -%d- EffVsY",i+1),120,-120,120);
      hCerSignalEfficiencyVsX[i] = new TEfficiency(Form("Cer-%d-EffVsX",i+1),Form("Cer -%d- EffVsX",i+1),120,-120,120);
   }

   TEfficiency * hAllCerEfficiencyVsEnergy = new TEfficiency("hAllCerEfficiencyVsEnergy","All Cer EffVsEnergy",50,500,4500);
   TEfficiency * hAllCerEfficiencyVsY = new TEfficiency("hAllCerEfficiencyVsY","All Cer EffVsY",120,-120,120);
   TEfficiency * hAllCerEfficiencyVsX = new TEfficiency("hAllCerEfficiencyVsX","All Cer EffVsX",120,-60,60);


   TEfficiency * hAllCerEfficiencyVsEnergy2 = new TEfficiency("hAllCerEfficiencyVsEnergy2","All Cer EffVsEnergy E>1000",50,500,4500);
   TEfficiency * hAllCerEfficiencyVsY2 = new TEfficiency("hAllCerEfficiencyVsY2","All Cer EffVsY E>1000",120,-120,120);
   TEfficiency * hAllCerEfficiencyVsX2 = new TEfficiency("hAllCerEfficiencyVsX2","All Cer EffVsX E>1000",120,-60,60);

   Int_t Nentries = t->GetEntries();
   for(Int_t IEVENT=0;IEVENT < Nentries;IEVENT++) {

      t->GetEntry(IEVENT);

      /// Require lucite
      if( aRecon->fNLucitePositions > 0 /*&& aRecon->fNTrackerPositions > 0*/ ){

         /// Require only one cluster
         if( aRecon->fNBigcalClusters == 1 ) {
            aCluster = (BIGCALCluster*)(*aRecon->fBigcalClusters)[0];

            bool cerTDCcut = TMath::Abs(aCluster->fCherenkovTDC)<10;

            hCerNPESum[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestNPESum);
            hCerNPESingle[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestNPEChannel);
            hCerSignalSum[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestADCSum);
            hCerSignalSingle[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestADCChannel);

            hCerSignalEfficiencyVsEnergy[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
            hCerSignalEfficiencyVsY[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fYMoment);
            hCerSignalEfficiencyVsX[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fXMoment);

            hAllCerEfficiencyVsEnergy->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
            hAllCerEfficiencyVsY->Fill(cerTDCcut,aCluster->fYMoment);
            hAllCerEfficiencyVsX->Fill(cerTDCcut,aCluster->fXMoment);

            if( (aCluster->fTotalE + aCluster->fDeltaE) > 1000.0) {
               hAllCerEfficiencyVsEnergy2->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
               hAllCerEfficiencyVsY2->Fill(cerTDCcut,aCluster->fYMoment);
               hAllCerEfficiencyVsX2->Fill(cerTDCcut,aCluster->fXMoment);
            }

         }

      }

   }

   TCanvas * c = new TCanvas();
   c->Divide(2,4);
   /// Draw (but don't display) the efficiencies
   for(int i = 0; i<8;i++) {
      c->cd(8-i);
      hCerNPESingle[i]->SetLineColor(1);
//       hCerNPESingle[i]->SetLineWidth(1);
      hCerNPESingle[i]->SetFillColor(1);
      hCerNPESingle[i]->SetFillStyle(3001);
      hCerNPESingle[i]->Draw();

//       hCerSignalEfficiencyVsEnergy[i]->Draw();
//       hCerSignalEfficiencyVsY[i]->Draw("same");
//       hCerSignalEfficiencyVsX[i]->Draw("same");
   }

   TCanvas * cAlign = new TCanvas();
   cAlign->Divide(2,4);
   /// Draw (but don't display) the efficiencies
   for(int i = 0; i<8;i++) {
      cAlign->cd(8-i);
      hCerSignalSum[i]->SetLineColor(1);
//       hCerNPESingle[i]->SetLineWidth(1);
      hCerSignalSum[i]->SetFillColor(1);
      hCerSignalSum[i]->SetFillStyle(1001);
      hCerSignalSum[i]->Draw();

//       hCerSignalEfficiencyVsEnergy[i]->Draw();
//       hCerSignalEfficiencyVsY[i]->Draw("same");
//       hCerSignalEfficiencyVsX[i]->Draw("same");
   }


   TLegend * leg = new TLegend(0.0,0.8,0.2,0.99);
   leg->SetHeader("Cherenkov");

   THStack * hs = new THStack("cherenkovNPE","Cherenkov NPE");

//    TCanvas * c1 = new TCanvas();
// /*   c1->Divide(2,4);*/
//    /// Draw (but don't display) the efficiencies
//    for(int i = 0; i<8;i++) {
// /*      c1->cd(8-i);*/
//       hCerNPESingle[i]->SetLineColor(1);
//       hCerNPESingle[i]->SetLineWidth(1);
//       hCerNPESingle[i]->SetFillColor(38+i);
//       hCerNPESingle[i]->SetMarkerColor(38+i);
//       hCerNPESingle[i]->SetMarkerStyle(21);
//       hs->Add(hCerNPESingle[i]);
//       if(i==0) hCerNPESingle[i]->Draw();
//       else hCerNPESingle[i]->Draw("same");
//       leg->AddEntry(hCerNPESingle[i],Form("Mirror %d",i+1),"l");
// 
//    }
//    leg->Draw();
// 
//    new TCanvas();
//    hs->Draw("nostack,p,l");
//    leg->Draw();
// 
// 
//    TCanvas * c2 = new TCanvas();
//    hAllCerEfficiencyVsEnergy->();
//    TCanvas * c3 = new TCanvas();
//    hAllCerEfficiencyVsY->Draw();
//    TCanvas * c4 = new TCanvas();
//    hAllCerEfficiencyVsX->Draw();
//    new TCanvas();
//    hAllCerEfficiencyVsEnergy2->Draw();
//    new TCanvas();
//    hAllCerEfficiencyVsY2->Draw();
//    new TCanvas();
//    hAllCerEfficiencyVsX2->Draw();

//    new TBrowser;


   TCanvas * cerNPEcanvas = new TCanvas();
   cerNPEcanvas->Divide(2,4);
   /// Draw (but don't display) the efficiencies
   for(int i = 0; i<8;i++) {
      cerNPEcanvas->cd(8-i);
      hCerNPESum[i]->SetLineColor(1);
//       hCerNPESum[i]->SetLineWidth(1);
      hCerNPESum[i]->SetFillColor(1);
      hCerNPESum[i]->SetFillStyle(3002);
      hCerNPESum[i]->Draw();
//       hCerNPESum[i]->s(38+i);
//       hCerNPESum[i]->SetMarkerStyle(21);
/*      hs->Add(hCerNPESum[i]);*/
//       if(i==0) hCerNPESingle[i]->Draw();
//       else hCerNPESingle[i]->Draw("same");
//       leg->AddEntry(hCerNPESingle[i],Form("Mirror %d",i+1),"l");

   }



   return(0);
}
