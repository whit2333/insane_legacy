/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650

 */
Int_t performance_by_mirror(Int_t runNumber = 703, Int_t mirrorNumber= 6)
{
   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   SANEEvents * events = new SANEEvents("betaDetectors1");
   if(events->fTree) events->SetClusterBranches(events->fTree);
   else{ std::cout << " TREE NOT FOUND : asdfasdf"  << "\n"; return(-1);}

//    const char * treeName = "trackingDetectors";
// 
//    TTree * t = (TTree*)gROOT->FindObject(treeName);
//    if(!t){ std::cout << " TREE NOT FOUND : " << treeName << "\n"; return(-1);}
// 
//    InSANEReconstructedEvent * aRecon = new InSANEReconstructedEvent();
//    BIGCALCluster * aCluster= 0;
//    t->SetBranchAddress("trackingReconEvent",&aRecon);
//    t->AddFriend(events->fTree);

   TDirectory * cerPerformanceDir=0;
   TFile * cerFile = new TFile("data/cherenkov_performance_by_mirror.root","UPDATE");
   if( cerFile ) {
      if( !(cerFile->cd(Form("cerPerformance%d",runNumber)) ) ) {
         cerPerformanceDir = cerFile->mkdir(Form("cerPerformance%d",runNumber)) ;
         if( !(cerPerformanceDir->cd()) ) printf(" dir Error\n");
      } else {
         cerPerformanceDir = gDirectory;
      }
   }

   TCanvas * c = new TCanvas("performance_by_mirror", "performance_by_mirror");
   //c->Divide(2, 4);

   TH1F * aMirrorHist;
   TFitResultPtr fitResult;

   InSANEPMTResponse * fptr = new InSANEPMTResponse();  // create the user function class
   fptr->SetMaxNPE(40);
   TF1 * f1 = new TF1("f1", fptr, &InSANEPMTResponse::PMT_fit, -10, 2000, 12, "InSANEPMTResponse", "PMT_fit");
         f1->SetParName(0, "N0");
         f1->SetParName(1, "P0");
         f1->SetParName(2, "P1");
         f1->SetParName(3, "A");
         f1->SetParName(4, "pe");
         f1->SetParName(5, "xp");
         f1->SetParName(6, "sigp");
         f1->SetParName(7, "x0");
         f1->SetParName(8, "sig0");
         f1->SetParName(9, "mu");
         f1->SetParName(10, "x1");
         f1->SetParName(11, "sig1");

            gPad->SetLogy(1);
      events->fTree->Draw(Form("bigcalClusters.fCherenkovBestADC>>mirror%dhist(200,0,4000)", mirrorNumber), 
                          Form(
                 "bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fPrimaryMirror==%d", mirrorNumber),
              "");
      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist", mirrorNumber));
      if (aMirrorHist) {
         std::cout << " Fitting Mirror " << mirrorNumber << "\n";

         f1->SetParameter(0, 2.0e3);                 // N0
         f1->SetParameter(1, 0.001); // P0
         f1->SetParLimits(1, 0.0, 1.0);
         f1->SetParameter(2, TMath::Poisson(1, 18)); // P1
         f1->SetParLimits(2, 0.0, 1.0);
         f1->SetParameter(3, 30);                 // A
         f1->SetParLimits(3, 0.0, 500.0);
         f1->SetParameter(4, 0.002);                  // pe
         f1->SetParLimits(4, 0.0, 1.0);
         f1->FixParameter(5,0.0);                  // xp
         f1->SetParLimits(5,-10.0,10.0);
         f1->FixParameter(6, 2.5);                  // sigp
         f1->SetParLimits(6,0.5,5.0);                  // sigp
         f1->FixParameter(7, 100.0);                 // x0
         f1->SetParLimits(7,80.0,150.0);                  // x0
         f1->FixParameter(8, 30.0);                 // sig0
         f1->SetParLimits(8,10.0,100.0);                   // sig0
         f1->SetParameter(9, 20.0);                  // mu
         f1->SetParLimits(9, 0.0, 30.0);                 // mu
         f1->SetParameter(10, 100.0);                   // x1
         f1->SetParLimits(10, 70.0, 120.0);                 // x1
         f1->SetParameter(11, 30.0);                  // sig1
         f1->SetParLimits(11, 10.0, 60.0);                 // sig1

         fitResult = aMirrorHist->Fit(f1, "S", "", 30, 6000);
         fptr->SetParameters(f1->GetParameters());

         aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->SetTitle("channels");
         aMirrorHist->GetXaxis()->CenterTitle(1);
         aMirrorHist->SetMaximum(aMirrorHist->GetMaximum());
         aMirrorHist->SetFillColor(kYellow-9);
         aMirrorHist->SetLineWidth(1);
         aMirrorHist->Draw();
         f1->DrawClone("same");

         /// Draw the fit components
         Int_t linestyle = 2;

         TF1 *fpexp = fptr->GetExpFunction();
         fpexp->SetLineColor(4);
         fpexp->SetLineStyle(linestyle);
         fpexp->DrawClone("same");

         TF1 * fp0 = fptr->GetPeakFunction(0);
         fp0->SetLineColor(4);
         fp0->SetLineStyle(linestyle);
         fp0->SetLineWidth(2);
         fp0->DrawClone("same");

         for (int np = 1; np < 25; np++) {
            TF1 * fp1 = fptr->GetPeakFunction(np);
            fp1->SetLineWidth(1);
            if (np == 1)fp1->SetLineColor(4);
            else fp1->SetLineColor(2);
            fp1->SetLineStyle(2);
           fp1->DrawClone("same");
         }

      }

      aMirrorHist->Write();


   c->SaveAs(Form("plots/%d/performanc_by_mirror_%d.pdf", runNumber,mirrorNumber));
   c->SaveAs(Form("plots/%d/performanc_by_mirror_%d.png", runNumber,mirrorNumber));
   c->SaveAs(Form("plots/%d/performanc_by_mirror_%d.svg", runNumber,mirrorNumber));

//     gROOT->SetBatch(kFALSE);


// f->Write();


   return(0);
}
