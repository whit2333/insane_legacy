/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008 
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650  

 */
Int_t OnePE_calibration_run4(Int_t runNumber=71805,Int_t isACalibration=0) {
   gStyle->SetLabelSize(0.08,"XYZ");
   gStyle->SetTitleSize(0.08,"XYZ");
   //   gStyle->SetTitleOffset(
   gStyle->SetPadTopMargin(0.01);
   gStyle->SetPadBottomMargin(0.18);
   gStyle->SetPadLeftMargin(0.09);
   gStyle->SetPadRightMargin(0.09);

   if (gROOT->LoadMacro("detectors/cherenkov/S_pmt.cxx") != 0) {
      Error(weh, "Failed loading detectors/cherenkov/S_pmt.cxx in compiled mode.");
      return -1;
   }

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if(!t) {printf("betaDetectors0 tree not found\n"); return(-1); }

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLED%d.root",runNumber),"UPDATE");

   TCanvas * c = new TCanvas("cherenkovLED","Cherenkov LED",800,200*2*1.62);
   c->Divide(2,4);

   TH1F * aMirrorHist;
   TFitResultPtr fitResult;

   //TF1 *f1 = new TF1("f1", "gaus", 0,6000);
   //TF1 *f1 = new TF1("Spmt",S_pmt,0,1000,5);
   PMTResponse * fptr = new PMTResponse();  // create the user function class
   TF1 * f1 = new TF1("f1",fptr,&PMTResponse::PMT_fit,-10,400,12,"PMTResponse","PMT_fit");  


   f1->SetParName(0,"N0");
   f1->SetParName(1,"P0");
   f1->SetParName(2,"P1");
   f1->SetParName(3,"A");
   f1->SetParName(4,"pe");
   f1->SetParName(5,"xp");
   f1->SetParName(6,"sigp");
   f1->SetParName(7,"x0");
   f1->SetParName(8,"sig0");
   f1->SetParName(9,"mu");
   f1->SetParName(10,"x1");
   f1->SetParName(11,"sig1");

   for(int i = 1; i<3;i++) {
      c->cd(8 - i);
      gPad->SetLogy(1);
      t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(100,-25,275)",i+1),Form(
                   "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d",i+1),
                   "",
                   1000000
                   1001);
      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist",i+1));
      if(aMirrorHist){

         std::cout << " Fitting Mirror " << i+1 << "\n";

   f1->SetParameter(0,2.0e5);                  // N0
   f1->SetParameter(1,TMath::Poisson(0,0.01) );  // P0
   f1->SetParLimits(1,0.0,1.0);
   f1->SetParameter(2,TMath::Poisson(1,0.01));   // P1
   f1->SetParLimits(2,0.0,1.0);
   f1->SetParameter(3,100.0);                  // A 
   f1->SetParLimits(3,1.0,1000.0);
   f1->SetParameter(4,0.5);                   // pe
   f1->SetParLimits(4,0.0,1.0);
   f1->SetParameter(5,0.0);                   // xp
   f1->SetParLimits(5,-20.0,30.0);
   f1->SetParameter(6,2.0);                   // sigp
   f1->SetParLimits(6,0.0,7.0);
   f1->SetParameter(7,90.0);                  // x0
   f1->SetParLimits(7,70.0,150.0);                  // x0
   f1->SetParameter(8,30.0);                  // sig0
   f1->SetParLimits(8,10.0,100.0);                   // sig0
   f1->SetParameter(9,0.01);                   // mu
   f1->SetParLimits(9,0.0,0.5);                   // mu
   f1->SetParameter(10, 90.0);                   // x1
   f1->SetParLimits(10,70.0,150.0);                   // x1
   f1->SetParameter(11,30.0);                   // sig1
   f1->SetParLimits(11,10.0,100.0);                   // sig1

         Double_t P0 = aMirrorHist->Integral(0,13)/((double)(aMirrorHist->GetEntries()));
         f1->FixParameter(1,P0 );  // P0

         //f1->SetParameter(1,aMirrorHist->GetMean(1));
         //aMirrorHist->Fit(f1,"M,E");
         fitResult = aMirrorHist->Fit(f1,"E,S","",-20,275);

         fptr->SetParameters(f1->GetParameters());

         //aMirrorHist->SetTitle(Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->SetTitle("channels");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->CenterTitle(1);
         aMirrorHist->Draw();

   TF1 * fpexp = fptr->GetExpFunction();
   fpexp->SetLineColor(3);
         TF1 * fp0 = fptr->GetPeakFunction(0);
         fp0->SetLineColor(6);
         TF1 * fp1 = fptr->GetPeakFunction(1);
         fp1->SetLineColor(2);
         TF1 * fp2 = fptr->GetPeakFunction(2);
         fp2->SetLineColor(4);
    fpexp->Draw("same");
         fp0->Draw("same");
         fp1->Draw("same");
         fp2->Draw("same");
      }
   }

   c->SaveAs(Form("plots/%d/CherenkovLED.pdf",runNumber));
  c->SaveAs(Form("plots/%d/CherenkovLED.png",runNumber));
  c->SaveAs(Form("plots/%d/CherenkovLED.svg",runNumber));

//     gROOT->SetBatch(kFALSE);


// f->Write();


return(0);
}
