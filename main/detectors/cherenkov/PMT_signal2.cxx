Int_t PMT_signal2(Int_t runNumber=71805,Int_t isACalibration=0) {

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if(!t) {printf("betaDetectors0 tree not found\n"); return(-1); }

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLED%d.root",runNumber),"UPDATE");

//    if (gROOT->LoadMacro("detectors/cherenkov/S_pmt.cxx") != 0) {
//       Error(weh, "Failed loading detectors/cherenkov/S_pmt.cxx in compiled mode.");
//       return -1;
//    }
   f->cd();
   
   Double_t P0  = 0;
   
   for(int i = 2; i<3;i++) {
      //c->cd(8 - i);
      //gPad->SetLogy(1);
      t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(100,-25,275)",i+1),Form(
                   "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d",i+1),
                   "",
                   1000000
                   1001);
      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist",i+1));
      if(aMirrorHist){
         std::cout << " Fitting Mirror " << i+1 << "\n";
         P0 = aMirrorHist->Integral(0,13)/((double)(aMirrorHist->GetEntries()));
      }
   }

 // create TF1 class.
//   TF1 *f1 = new TF1("Spmt",PMT_fit_low_light,0,400,10);

   InSANEPMTResponse * fptr = new InSANEPMTResponse();  // create the user function class
   TF1 * f1 = new TF1("f1",fptr,&InSANEPMTResponse::PMT_fit,-20,400,12,"InSANEPMTResponse","PMT_fit");  
   f1->SetParameter(0,2.0e5);                  // N0
   f1->SetParameter(1,P0);//TMath::Poisson(0,0.05) );  // P0
   f1->SetParameter(2,TMath::Poisson(1,0.01));   // P1
   f1->SetParameter(3,35.0);                  // A 
   f1->SetParameter(4,0.5);                   // pe
   f1->SetParLimits(4,0.0,1.0);
   f1->SetParameter(5,0.0);                   // xp
   f1->SetParLimits(5,-20.0,30.0);
   f1->SetParameter(6,2.5);                   // sigp
   f1->SetParLimits(6,0.0,10.0);
   f1->SetParameter(7,100.0);                  // x0
   f1->SetParLimits(7,30.0,150.0);                  // x0
   f1->SetParameter(8,35.0);                  // sig0
   f1->SetParameter(9,0.03);                   // mu
   f1->SetParLimits(9,0.0,5.0);                   // mu
   f1->SetParameter(10, 100.0);                   // x1
   f1->SetParLimits(10,30.0,200.0);                   // x1
   f1->SetParameter(11,28.0);                   // sig1
   f1->SetParLimits(11,10.0,200.0);                   // sig1

   f1->SetParName(0,"N0");
   f1->SetParName(1,"P0");
   f1->SetParName(2,"P1");
   f1->SetParName(3,"A");
   f1->SetParName(4,"pe");
   f1->SetParName(5,"xp");
   f1->SetParName(6,"sigp");
   f1->SetParName(7,"x0");
   f1->SetParName(8,"sig0");
   f1->SetParName(9,"mu");
   f1->SetParName(10,"x1");
   f1->SetParName(11,"sig1");

   f1->Draw("same");

   fptr->SetParameters(f1->GetParameters());

   gPad->SetLogy(1);

   TF1 * fp0 = fptr->GetPeakFunction(0);
   fp0->SetLineColor(6);

   TF1 * fp1 = fptr->GetPeakFunction(1);
   fp1->SetLineColor(2);

   TF1 * fp2 = fptr->GetPeakFunction(2);
   fp2->SetLineColor(4);

   TF1 * fpexp = fptr->GetExpFunction();
   fpexp->SetLineColor(3);

   fp0->Draw("same");
   fp1->Draw("same");
   fp2->Draw("same");
   fpexp->Draw("same");

   f1->Print();

   f->Write();

   return(0);
}
