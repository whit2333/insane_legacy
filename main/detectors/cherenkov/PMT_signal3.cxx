Int_t PMT_signal3(Int_t runNumber=71805,Int_t isACalibration=0) {

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if(!t) {printf("betaDetectors0 tree not found\n"); return(-1); }

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLED%d.root",runNumber),"UPDATE");

//    if (gROOT->LoadMacro("detectors/cherenkov/S_pmt.cxx") != 0) {
//       Error(weh, "Failed loading detectors/cherenkov/S_pmt.cxx in compiled mode.");
//       return -1;
//    }
   f->cd();
   
   Double_t P0  = 0;
   
   for(int i = 2; i<3;i++) {
      //c->cd(8 - i);
      //gPad->SetLogy(1);
      t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(100,-25,275)",i+1),Form(
                   "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d",i+1),
                   "",
                   1000000
                   1001);
      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist",i+1));
      if(aMirrorHist){
         std::cout << " Fitting Mirror " << i+1 << "\n";
         P0 = aMirrorHist->Integral(0,13)/((double)(aMirrorHist->GetEntries()));
      }
   }
   InSANEPMTResponse * fptr = new InSANEPMTResponse();  // create the user function class

   TF1 * f2 = new TF1("f2",fptr,&InSANEPMTResponse::SER_0,-10,400,6,"InSANEPMTResponse","SER_0");
   f2->SetParameter(0,2.0); //A
   f2->SetParameter(1,0.1); //pe
   f2->SetParameter(2,0.0); //xp
   f2->SetParameter(3,2.5); //sigp
   f2->SetParameter(4,100.0); //x0
   f2->SetParameter(5,40.0); //sig0
   f2->Draw();
   return(0);

   TF1 * f1 = new TF1("f1",fptr,&InSANEPMTResponse::PMT_fit3,-10,400,9,"InSANEPMTResponse","PMT_fit3");  

   f1->SetParName(0,"N0");
   f1->SetParName(1,"P0");
   f1->SetParName(2,"A");
   f1->SetParName(3,"pe");
   f1->SetParName(4,"xp");
   f1->SetParName(5,"sigp");
   f1->SetParName(6,"x0");
   f1->SetParName(7,"sig0");
   f1->SetParName(8,"mu");

   f1->SetParameter(0,2.0e5);                  // N0
   f1->SetParameter(1,TMath::Poisson(0,0.01) );  // P0
   f1->SetParLimits(1,0.0,1.0);
   f1->SetParameter(2,10.0);                  // A 
   f1->SetParLimits(2,0.0,100.0);
   f1->SetParameter(3,0.5);                   // pe
   f1->SetParLimits(3,0.0,1.0);
   f1->SetParameter(4,0.0);                   // xp
   f1->SetParLimits(4,-20.0,30.0);
   f1->SetParameter(5,2.5);                   // sigp
   f1->SetParLimits(5,0.0,7.0);
   f1->SetParameter(6,90.0);                  // x0
   f1->SetParLimits(6,70.0,150.0);                  // x0
   f1->SetParameter(7,30.0);                  // sig0
   f1->SetParLimits(7,10.0,100.0);                   // sig0
   f1->SetParameter(8,0.01);                   // mu
   f1->SetParLimits(8,0.0,0.5);                   // mu

   f2->Draw("same");

   fptr->SetParameters(f1->GetParameters());

   gPad->SetLogy(1);


   Int_t linestyle = 2;

         TF1 *fpexp = new TF1("pmt-exp",fptr,&InSANEPMTResponse::Ser_exp,-100,2000,7,"InSANEPMTResponse","Ser_exp");
            fpexp->SetParameter(0,f1->GetParameters()[2]);
            fpexp->SetParameter(1,f1->GetParameters()[3]);
            fpexp->SetParameter(2,f1->GetParameters()[4]);//fxp);
            fpexp->SetParameter(3,f1->GetParameters()[5]);//fsigp);
            fpexp->SetParameter(4,f1->GetParameters()[6]);//fx0);
            fpexp->SetParameter(5,f1->GetParameters()[7]);//fsig0);
            fpexp->SetParameter(6,TMath::Poisson(1.0,f1->GetParameters()[8])*f1->GetParameters()[0]);//N0);
   fpexp->SetLineColor(3);
/*   fpexp->SetLineStyle(linestyle);*/
   fpexp->SetLineWidth(2);
   fpexp->DrawClone("same");

   TF1 * fp0 = fptr->GetPeakFunction(0);
   fp0->SetLineColor(4);
   fp0->SetLineStyle(linestyle);
   fp0->SetLineWidth(2);
      fp0->DrawClone("same");

//    TF1 * fpexp = fptr->GetExpFunction();
//    fpexp->SetLineColor(3);
//    //fpexp->Print();

   for(int np = 1;np<15;np++){ 
      TF1 * fp1 = fptr->GetPeakFunction(np);
      fp1->SetLineWidth(1);
      if(np==1)fp1->SetLineColor(4);
      else fp1->SetLineColor(2);
      fp1->SetLineStyle(2);
      fp1->DrawClone("same");
   }
   f->Write();

   return(0);
}
