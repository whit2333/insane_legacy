Int_t cherenkov_tdc_cut(Int_t runNumber=72256, Int_t mirror = 6) {

   rman->SetRun(runNumber);

   const char * treeName0 = "betaDetectors0";
   TTree * t0 = (TTree*)gROOT->FindObject(treeName0);
   if(!t0){ std::cout << " TREE NOT FOUND : " << treeName0 << "\n"; return(-1);}

   const char * treeName = "betaDetectors1";
   TTree * t = (TTree*)gROOT->FindObject(treeName);
   if(!t){ std::cout << " TREE NOT FOUND : " << treeName << "\n"; return(-1);}

   TCanvas * c = new TCanvas("cherenkov_td_cut",Form("Gas Chernekov TDC Cut for mirror %d" , mirror) );
   c->Divide(2,1);

   Float_t tdc_cut_width = 6.0;
   Double_t zero_timewalk = 0;
   TSpectrum * spectrum = new TSpectrum(5);
   Float_t * peaks;
//_____________________________________________________________________________

   c->cd(1);
   t0->Draw("fGasCherenkovHits.fTDCAlign>>hcer60(100,-50,50)",
            Form("triggerEvent.IsBETA2Event()&&fGasCherenkovHits.fChannel==%d",mirror),
            "goff",100000);
   TH1F * h1 = (TH1F*) gROOT->FindObject("hcer60");
   if(h1) { h1->SetLineColor(2); h1->Draw(); }

   t->Draw("fGasCherenkovTDCHits.fTDCAlign>>hcer61(100,-50,50)",
            Form("triggerEvent.IsBETA2Event()&&fGasCherenkovTDCHits.fChannel==%d",mirror),
           "goff",100000);
   TH1F * h2 = (TH1F*) gROOT->FindObject("hcer61");
   spectrum->Search(h2,2,"goff",0.05);
   peaks = spectrum->GetPositionX();
   zero_timewalk = peaks[0];//h2->GetMean();// h2->GetBinCenter(h2->GetMaximumBin());
   std::cout << " zero_timewalk = " << zero_timewalk << "\n";
   if(h2) { h2->SetLineColor(4); h2->Draw("same"); }

   t->Draw(Form("fGasCherenkovTDCHits.fTDCAlign-%f>>hcer62(100,-50,50)",zero_timewalk),
            Form("TMath::Abs(fGasCherenkovTDCHits.fTDCAlign-%f)<%f&&triggerEvent.IsBETA2Event()&&fGasCherenkovTDCHits.fChannel==%d",zero_timewalk,tdc_cut_width,mirror),
           "goff",100000);
   h2 = (TH1F*) gROOT->FindObject("hcer62");
   if(h2) { h2->SetLineColor(1); h2->Draw("same"); }

//_____________________________________________________________________________

   c->cd(2);
   t0->Draw("fGasCherenkovHits.fADC>>hcerADC60(100,0,4000)",
            Form("triggerEvent.IsBETA2Event()&&fGasCherenkovHits.fChannel==%d",mirror),
            "goff",100000);
   TH1F * h3 = (TH1F*) gROOT->FindObject("hcerADC60");
   if(h3) { h3->SetLineColor(2); h3->Draw(); }

   t->Draw("fGasCherenkovHits.fADC>>hcerADC61(100,0,4000)",
            Form("TMath::Abs(fGasCherenkovHits.fTDCAlign)>50&&triggerEvent.IsBETA2Event()&&fGasCherenkovHits.fChannel==%d",zero_timewalk,mirror),
           "goff",100000);
   TH1F * h4 = (TH1F*) gROOT->FindObject("hcerADC61");
   if(h4) { h4->SetLineColor(4); h4->Draw("same"); }

   t->Draw("fGasCherenkovTDCHits.fADC>>hcerADC62(100,0,4000)",
            Form("TMath::Abs(fGasCherenkovTDCHits.fTDCAlign-%f)<5&&triggerEvent.IsBETA2Event()&&fGasCherenkovTDCHits.fChannel==%d",zero_timewalk,mirror),
           "goff",100000);
   TH1F * h5 = (TH1F*) gROOT->FindObject("hcerADC62");
   if(h5) { h5->SetLineColor(1); h5->Draw("same"); }

//_____________________________________________________________________________


   return(0);
}
