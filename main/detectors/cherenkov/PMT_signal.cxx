Int_t PMT_signal(){

   if (gROOT->LoadMacro("detectors/cherenkov/S_pmt.cxx") != 0) {
      Error(weh, "Failed loading detectors/cherenkov/S_pmt.cxx in compiled mode.");
      return -1;
   }

   Double_t x = 100.0;
   Double_t par[] = {1.0,0.0,100.0,40.0,1.0};

 // create TF1 class.
//   TF1 *f1 = new TF1("Spmt",PMT_fit_low_light,0,400,10);
   InSANEPMTResponse * fptr = new InSANEPMTResponse();  // create the user function class
   TF1 * f1 = new TF1("f2",fptr,&InSANEPMTResponse::PMT_fit2,-10,400,9,"InSANEPMTResponse","PMT_fit2");  


   f1->SetParName(0,"N0");
   f1->SetParName(1,"P0");
   f1->SetParName(2,"A");
   f1->SetParName(3,"pe");
   f1->SetParName(4,"xp");
   f1->SetParName(5,"sigp");
   f1->SetParName(6,"x0");
   f1->SetParName(7,"sig0");
   f1->SetParName(8,"mu");


   f1->SetParameter(0,2.0e5);                  // N0
   f1->SetParameter(1,TMath::Poisson(0,0.01) );  // P0
   f1->SetParLimits(1,0.0,1.0);
   f1->SetParameter(2,10.0);                  // A 
   f1->SetParLimits(2,0.0,100.0);
   f1->SetParameter(3,0.5);                   // pe
   f1->SetParLimits(3,0.0,1.0);
   f1->SetParameter(4,0.0);                   // xp
   f1->SetParLimits(4,-20.0,30.0);
   f1->SetParameter(5,2.5);                   // sigp
   f1->SetParLimits(5,0.0,7.0);
   f1->SetParameter(6,90.0);                  // x0
   f1->SetParLimits(6,70.0,150.0);                  // x0
   f1->SetParameter(7,30.0);                  // sig0
   f1->SetParLimits(7,10.0,100.0);                   // sig0
   f1->SetParameter(8,0.01);                   // mu
   f1->SetParLimits(8,0.0,0.5);                   // mu
   f1->Draw();

   fptr->SetParameters(f1->GetParameters());

   gPad->SetLogy(1);

   TF1 * fp0 = fptr->GetPeakFunction(0);
   fp0->SetLineColor(6);

   TF1 * fp1 = fptr->GetPeakFunction(1);
   fp1->SetLineColor(2);

   TF1 * fp2 = fptr->GetPeakFunction(2);
   fp2->SetLineColor(4);

   TF1 * fpexp = fptr->GetExpFunction();
   fpexp->SetLineColor(3);

   fp0->Draw("same");
   fp1->Draw("same");
   fp2->Draw("same");
   fpexp->Draw("same");

   f1->Print();

   return(0);
}
