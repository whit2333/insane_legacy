Int_t rates_cherenkov2(Int_t runNumber = 72995) {

   TH1 * h1 = 0;
   TH2F * h2 = 0;
   TGraph * g1 = 0;
   TProfile * px;
   TProfile * py;
   Double_t max = 0.0;
   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fRunNumber;
   rman->GetScalerFile()->cd();


   TTree * t = (TTree*)gROOT->FindObject("Scalers1");
   if(!t) return(-1);

   TCanvas * c = new TCanvas("cherenkovRates","Cherenkov Rates");
   c->Divide(1,2);
   c->cd(1);
   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   for(int i = 1;i<=8;i++) {
      t->Draw(Form("saneScalerEvent.GetCherenkovRate(%d)>>cer%d",i-1,i),"","goff");
/*      c->cd(i);*/
      h1 = (TH1F*)gROOT->FindObject(Form("cer%d",i) );
      h1->SetTitle(Form("Cherenkov scaler %d",i));
      h1->GetXaxis()->SetTitle("rate");
      h1->SetLineColor(37+i);
      h1->SetMarkerColor(kRed*(i%2)+kBlue*((i+1)%2)+i); //37+i);
      h1->SetMarkerStyle(19+i);
      h1->SetMarkerSize(1.2);
      if(i==1)h1 = h1->DrawCopy("lp");
      else h1 =  h1->DrawCopy("lpsame");
      if(h1->GetMaximum() > max) {
         max = h1->GetMaximum();
         h1->SetMaximum(max);
      }
      leg->AddEntry(h1,Form("cer %d scaler",i));
   }
   leg->Draw();
   max = 0;
   c->cd(2);
   TLegend * leg2 = new TLegend(0.1,0.7,0.48,0.9);
   for(int i = 9;i<=12;i++) {
      t->Draw(Form("saneScalerEvent.GetCherenkovRate(%d)>>cer%d",i-1,i),"","goff");
/*      c->cd(i);*/
      h1 = (TH1F*)gROOT->FindObject(Form("cer%d",i) );
      h1->SetTitle(Form("Cherenkov scaler %d",i));
      h1->GetXaxis()->SetTitle("rate");
      h1->SetLineColor(35+i-8);
      h1->SetMarkerColor(kRed*((i-8)%2)+kBlue*((i-8+1)%2)+i-8); //37+i);
      h1->SetMarkerStyle(18+i-8);
      h1->SetMarkerSize(1.2);
      if(i==9)h1 = h1->DrawCopy("lp");
      else h1 =  h1->DrawCopy("lpsame");
      if(h1->GetMaximum() > max) {
         max = h1->GetMaximum() ;
         h1->SetMaximum(max);
      }
      leg2->AddEntry(h1,Form("cer %d scaler",i));
   }
  leg2->Draw();

   c->SaveAs(Form("plots/%d/rates_cherenkov2.png",runNumber));


return(0);
}
