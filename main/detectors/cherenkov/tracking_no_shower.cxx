/*!  Track reconstruction with BigCal


   TODO:
   - Do the y cut first then look at the X distribution ( curretnly done the other way around)
   - Look at Pi0 events w.r.t. reconstructed tracks (no shower)
   - 

 */
Int_t tracking_no_shower(Int_t runNumber=72671) {

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   const char * treeName = "trackingDetectors";
   TTree * t = (TTree*)gROOT->FindObject(treeName);
   if(!t){ std::cout << " TREE NOT FOUND : " << treeName << "\n"; return(-1);}

   InSANEReconstructedEvent * aRecon = new InSANEReconstructedEvent();
   BIGCALCluster * aCluster= 0;
   t->SetBranchAddress("trackingReconEvent",&aRecon);
   SANEEvents * ev = new SANEEvents("betaDetectors1");

   t->AddFriend(ev->fTree);


   TDirectory * cerPerformanceDir=0;
   TFile * cerFile = new TFile("data/tracking_no_shower.root","UPDATE");
   if( cerFile ) {
      if( !(cerFile->cd(Form("noshower%d",runNumber)) ) ) {
         cerPerformanceDir = cerFile->mkdir(Form("noshower%d",runNumber)) ;
         if( !(cerPerformanceDir->cd()) ) printf(" dir Error\n");
      } else {
         cerPerformanceDir = gDirectory;
      }
   }

   Double_t trackerLuciteYSlope = 5.0;

   /// HISTOGRAMS
   TH1F * hCerNPESingle[8];
   TH1F * hCerNPESum[8];
   TH1F * hCerSignalSum[8];
   TH1F * hCerSignalSingle[8];
   TEfficiency * hCerSignalEfficiencyVsEnergy[8];
   TEfficiency * hCerSignalEfficiencyVsY[8];
   TEfficiency * hCerSignalEfficiencyVsX[8];

//    for(int i = 0; i<8;i++) {
//       hCerNPESum[i] = new TH1F(Form("Cer-%d-NPE Sum",i+1),Form("Cer -%d- NPE Sum",i+1),200,1,45);
//       hCerNPESingle[i] = new TH1F(Form("Cer-%d-NPE",i+1),Form("Cer -%d- NPE",i+1),200,1,45);      hCerSignalSum[i] = new TH1F(Form("Cer-%d-Sum",i+1),Form("Cer -%d- Sum",i+1),200,0.02,4);
//       hCerSignalSingle[i] = new TH1F(Form("Cer-%d-",i+1),Form("Cer -%d-",i+1),200,0.02,4);
// 
//       hCerSignalEfficiencyVsEnergy[i] = new TEfficiency(Form("Cer-%d-EffVsEnergy",i+1),Form("Cer -%d- EffVsEnergy",i+1),50,500,4500);
//       hCerSignalEfficiencyVsY[i] = new TEfficiency(Form("Cer-%d-EffVsY",i+1),Form("Cer -%d- EffVsY",i+1),120,-120,120);
//       hCerSignalEfficiencyVsX[i] = new TEfficiency(Form("Cer-%d-EffVsX",i+1),Form("Cer -%d- EffVsX",i+1),120,-120,120);
//    }
//    TEfficiency * hAllCerEfficiencyVsEnergy = new TEfficiency("hAllCerEfficiencyVsEnergy","All Cer EffVsEnergy",50,500,4500);
//    TEfficiency * hAllCerEfficiencyVsY = new TEfficiency("hAllCerEfficiencyVsY","All Cer EffVsY",120,-120,120);
//    TEfficiency * hAllCerEfficiencyVsX = new TEfficiency("hAllCerEfficiencyVsX","All Cer EffVsX",120,-60,60);
// 
//    TEfficiency * hAllCerEfficiencyVsEnergy2 = new TEfficiency("hAllCerEfficiencyVsEnergy2","All Cer EffVsEnergy E>1000",50,500,4500);
//    TEfficiency * hAllCerEfficiencyVsY2 = new TEfficiency("hAllCerEfficiencyVsY2","All Cer EffVsY E>1000",120,-120,120);
//    TEfficiency * hAllCerEfficiencyVsX2 = new TEfficiency("hAllCerEfficiencyVsX2","All Cer EffVsX E>1000",120,-60,60);




   /// Canvas and simple draws
   TCanvas * canvas = new TCanvas("Hodoscope","Lucite Hodoscope");
   canvas->Divide(2,2);

   /// Examine TDC Diff's peak subtraction... is it good? Yes
//    canvas->cd(1);
//    ev->fTree->Draw("fLucitePositionHits.fBarNumber:fLucitePositionHits.fTDCDifference",
//       "TMath::Abs(fLucitePositionHits.fTDCDifference)<250",
//       "col");

   /// Look at the Reconstructed Positions
   canvas->cd(1);
   ev->fTree->Draw("fLucitePositionHits.fPositionVector.fY:fLucitePositionHits.fPositionVector.fX",
      "TMath::Abs(fLucitePositionHits.fTDCDifference-60)<200",
      "col");

   /// Lucite Positions with a Tracker Hit
   canvas->cd(2);
   t->Draw("fLucitePositions.fPosition.fY:fLucitePositions.fPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fX-150)<100&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   /// the lucite "Seeding hit" for position comparison... if geometry is calculated/measured correctly it should be the same as the lucite position 
   canvas->cd(3);
   t->Draw("fLucitePositions.fSeedingPosition.fY:fLucitePositions.fSeedingPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fX-150)<100&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   canvas->cd(4);
   t->Draw("fLucitePositions.fPositionUncertainty.fY:fLucitePositions.fPositionUncertainty.fX",
      "TMath::Abs(fLucitePositions.fPosition.fX-150)<100&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");





   TCanvas * c2 = new TCanvas("Tracker","Forward Tracker");
   c2->Divide(2,2);

   /// Look at the Reconstructed Positions
   c2->cd(1);
   ev->fTree->Draw("fTrackerPositionHits.fPositionVector.fY:fTrackerPositionHits.fPositionVector.fX",
      "",
      "col");

   /// Tracker Positions
   c2->cd(2);
   t->Draw("fTrackerPositions.fPosition.fY:fTrackerPositions.fPosition.fX",
      "fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   c2->cd(3);
   t->Draw("fTrackerPositions.fSeedingPosition.fY:fTrackerPositions.fSeedingPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fX-150)<100&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   /// Look at the Reconstructed Positions
   c2->cd(4);
   t->Draw("fTrackerPositions.fPositionUncertainty.fY:fTrackerPositions.fPositionUncertainty.fX",
      "fNLucitePositions>0&&fNTrackerPositions>0",
      "col");




   TCanvas * c3 = new TCanvas("TrackerAndHodoscope","Tracker and Hodoscope");
   c3->Divide(2,2);

   /// Look at the Reconstructed Positions
   c3->cd(1);
   t->Draw("fTrackerPositions.fPosition.fX:fLucitePositions.fPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fX-150)<100&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   /// Look at the Reconstructed Positions
   c3->cd(2);
   t->Draw("fTrackerPositions.fPosition.fY:fLucitePositions.fPosition.fY",
      "fNLucitePositions>0",
      "col");
/*TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<8.0&&&&fNTrackerPositions>0&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2*/
   /// Look at the Reconstructed Positions
   c3->cd(3);
   t->Draw("fTrackerPositions.fPosition.fX:fTrackerPositions.fSeedingPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&& TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   /// Look at the Reconstructed Positions
   c3->cd(4);
   t->Draw("fTrackerPositions.fPosition.fY:fLucitePositions.fPosition.fY",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   TCanvas * c4 = new TCanvas("CherenkovTrackingOnly","Cherenkov with Tracker and Hodoscope only");
   c4->Divide(2,2);

   c4->cd(1);
   t->Draw("fTrackerPositions.fPosition.fY:bigcalClusters.fCherenkovBestADCSum",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2",
      "col");

   c4->cd(2);
   t->Draw("fLucitePositions.fPosition.fY:bigcalClusters.fCherenkovBestADCSum",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2",
      "col");

   c4->cd(3);
   t->Draw("fTrackerPositions.fPosition.fY:fTrackerPositions.fPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2",
      "col");

   c4->cd(4);
   t->Draw("fTrackerPositions.fSeedingPosition.fY:fTrackerPositions.fSeedingPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2",
      "col");

   TCanvas * c5 = new TCanvas("CherenkovTrackingOnly2","Cherenkov with Tracker and Hodoscope only");
   c5->Divide(2,2);

   c5->cd(1);
   t->Draw("fTrackerPositions.fPosition.fY:bigcalClusters.fCherenkovBestADCSum",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2&&bigcalClusters.fTotalE>1300",
      "col");

   c5->cd(2);
   t->Draw("fLucitePositions.fPosition.fY:bigcalClusters.fCherenkovBestADCSum",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2&&bigcalClusters.fTotalE>1300",
      "col");

   c5->cd(3);
   t->Draw("fTrackerPositions.fPosition.fY:fTrackerPositions.fPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX-150)<100&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2&&bigcalClusters.fTotalE>1300",
      "col");

   c5->cd(4);
   t->Draw("fTrackerPositions.fSeedingPosition.fY:fTrackerPositions.fSeedingPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX-150)<100&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2&&bigcalClusters.fTotalE>1300",
      "col");


   TCanvas * c6 = new TCanvas("CherenkovLuciteOnly","Cherenkov with Hodoscope only");
   c6->Divide(2,2);
/*
   c6->cd(1);
   t->Draw("fTrackerPositions.fPosition.fY:bigcalClusters.fCherenkovBestADCSum",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2",
      "col");*/

   c6->cd(2);
   t->Draw("fLucitePositions.fPosition.fY:bigcalClusters.fCherenkovBestADCSum",
      "fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2",
      "col");
/*
   c4->cd(3);
   t->Draw("fTrackerPositions.fPosition.fY:fTrackerPositions.fPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2",
      "col");

   c4->cd(4);
   t->Draw("fTrackerPositions.fSeedingPosition.fY:fTrackerPositions.fSeedingPosition.fX",
      "TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fX-2)<5&&TMath::Abs(fTrackerPositions.fPositionUncertainty.fY)<5&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fCherenkovBestADCSum-2)<2",
      "col");s*/

//    t->StartViewer();

   InSANETrajectory * aTrajectory                 = 0;
   LuciteHodoscopePositionHit * aLucPositionHit   = 0;
   LuciteHodoscopePositionHit * bLucPositionHit   = 0;
   BIGCALCluster * aCluster                       = 0;
   BIGCALCluster * bCluster                       = 0;
   ForwardTrackerPositionHit * aTrackerPosHit     = 0;
   ForwardTrackerPositionHit * bTrackerPosHit     = 0;
   InSANEHitPosition * aPos                       = 0;
   InSANEHitPosition * bPos                       = 0;

   TClonesArray * lucPos         = aRecon->fLucitePositions;
   TClonesArray * trackerPos     = aRecon->fTrackerPositions;
   TClonesArray * lucHitPos      = aRecon->fLuciteHitPos;
   TClonesArray * trackerHitPos  = aRecon->fTrackerHitPos;

   /// Event Loop
/*   Int_t Nentries = t->GetEntries();*/
/*
   for(Int_t IEVENT=0;IEVENT < Nentries;IEVENT++) {

      t->GetEntry(IEVENT);
      ev->fTree->GetEntryWithIndex(aRecon->fRunNumber,aRecon->fEventNumber);

      /// Require lucite
      if( aRecon->fNLucitePositions > 0 ){

         ///
         

         /// Require only one cluster
//          if( aRecon->fNBigcalClusters == 1 ) {
//             aCluster = (BIGCALCluster*)(*aRecon->fBigcalClusters)[0];
//            bool cerTDCcut = TMath::Abs(aCluster->fCherenkovTDC)<10;

            hCerNPESum[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestNPESum);
            hCerNPESingle[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestNPEChannel);
            hCerSignalSum[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestADCSum);
            hCerSignalSingle[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestADCChannel);

            hCerSignalEfficiencyVsEnergy[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
            hCerSignalEfficiencyVsY[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fYMoment);
            hCerSignalEfficiencyVsX[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fXMoment);

            hAllCerEfficiencyVsEnergy->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
            hAllCerEfficiencyVsY->Fill(cerTDCcut,aCluster->fYMoment);
            hAllCerEfficiencyVsX->Fill(cerTDCcut,aCluster->fXMoment);

            if( (aCluster->fTotalE + aCluster->fDeltaE) > 1000.0) {
               hAllCerEfficiencyVsEnergy2->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
               hAllCerEfficiencyVsY2->Fill(cerTDCcut,aCluster->fYMoment);
               hAllCerEfficiencyVsX2->Fill(cerTDCcut,aCluster->fXMoment);
            }
         }
      }
   }
*/
    canvas->SaveAs(Form("plots/%d/tracking_no_shower1.png",runNumber));    canvas->SaveAs(Form("plots/%d/tracking_no_shower1.pdf",runNumber));
    c2->SaveAs(Form("plots/%d/tracking_no_shower2.png",runNumber));
    c2->SaveAs(Form("plots/%d/tracking_no_shower2.pdf",runNumber));
    c3->SaveAs(Form("plots/%d/tracking_no_shower3.png",runNumber));
    c3->SaveAs(Form("plots/%d/tracking_no_shower3.pdf",runNumber));
    c4->SaveAs(Form("plots/%d/tracking_no_shower4.png",runNumber));
    c4->SaveAs(Form("plots/%d/tracking_no_shower4.pdf",runNumber));

    c5->SaveAs(Form("plots/%d/tracking_no_shower5.png",runNumber));
    c5->SaveAs(Form("plots/%d/tracking_no_shower5.pdf",runNumber));

   return(0);
}
