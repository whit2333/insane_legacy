Int_t cherenkov_perp18(Double_t Emin = 1100){

   TFile * outfile = new TFile("detectors/cherenkov/rootfiles/cherenkov_perp18_3.root","UPDATE");
   TList * fHistograms = new TList();
   TList * fADCHistograms = new TList();
   TList * fTDCHistograms = new TList();
   TH1F * h1 = 0;
   TH1F * hcerOdd = 0;
   TH1F * hcerEven = 0;
   TH1F * hcerOddNoLuc = 0;
   TH1F * hcerEvenNoLuc = 0;

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/cherenkov18_runs.txt");

   Double_t cer_lo = 0.5;
   Double_t cer_hi = 1.5;

   //std::vector<Int_t> runs;
   //runs.push_back(72692);
   //runs.push_back(72796);
   //runs.push_back(72795);
   //runs.push_back(72770);
   //runs.push_back(72715);
   //runs.push_back(72792);
   //runs.push_back(72795);
   //runs.push_back(72796);
   //runs.push_back(72797);
   //runs.push_back(72798);
   //runs.push_back(72799);
   //runs.push_back(72703);
   //runs.push_back(72704);
   //runs.push_back(72708);
   //runs.push_back(72702);
   //72795 72796 72770 72692 72715 72792 72795 72796 72797 72798 72799 72703 72704 72708 72702
   std::vector<Int_t> & runs = *(aman->fRunQueue);

   for (std::vector<int>::iterator it = runs.begin() ; it != runs.end(); ++it){
      outfile->cd(Form("run%d-%d",*it,int(Emin)));
      for(int i = 0;i<8;i++){
         gDirectory->GetObject(Form("ceradc%d",i+1),h1);
         //h1 = new TH1F(Form("ceradc%d",i+1),Form("ADC %d",i+1),100,0,5.0);
         if(h1) fADCHistograms->Add(h1);
         //h1 = new TH1F(Form("certdc%d",i+1),Form("TDC %d",i+1),100,-200,200);
         //fTDCHistograms->Add(h1);
      }
     
      if(!hcerOdd){
         gDirectory->GetObject("cerodd",hcerOdd);
      } else {
         gDirectory->GetObject("cerodd",h1);
         if(h1) hcerOdd->Add(h1);//fHistograms->Add(hcerOdd);
      }
      if(!hcerEven){
         gDirectory->GetObject("cereven",hcerEven);
      } else {
         gDirectory->GetObject("cereven",h1);
         if(h1) hcerEven->Add(h1);//fHistograms->Add(hcerEven);
      }
      if(!hcerOddNoLuc){
         gDirectory->GetObject("ceroddnoluc",hcerOddNoLuc);
      } else {
         gDirectory->GetObject("ceroddnoluc",h1);
         if(h1) hcerOddNoLuc->Add(h1);
      }
      if(!hcerEvenNoLuc){
         gDirectory->GetObject("cerevennoluc",hcerEvenNoLuc);
      } else {
         gDirectory->GetObject("cerevennoluc",h1);
         if(h1) hcerEvenNoLuc->Add(h1);
      }

   }

   // ----------------------------------------
   Double_t xMin = 0.02;
   Double_t xMax = 4.0;
   TFitResultPtr fitResult;
   TF1 *f1 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+[3]*exp(-0.5*((x-2.0*[1])/[4])**2)+[5]*exp(-0.5*((x-[6])/[7])**2)", xMin,xMax);

   TF1 *f2 = new TF1("cherenkovADC","[0]*exp(-0.5*((x-[1])/[2])**2)+\
         [3]*exp(-0.5*((x-2.0*[1])/[4])**2)+\
         [5]*exp(-0.5*((x-3.0*[1])/[6])**2)", xMin,xMax);

   // 1 electron peak amplitude
   f1->SetParameter(0,100);
   f1->SetParLimits(0,1000,99999999);
   // 1 electron peak mean
   f1->SetParameter(1,0.9);
   f1->SetParLimits(1,0.75,2.0);
   // 1 electron peak width
   f1->SetParameter(2,0.700);
   f1->SetParLimits(2,0,9999);
   // 2 electron peak amplitude
   f1->SetParameter(3,20);
   f1->SetParLimits(3,0,9999999);
   // 2 electron peak width
   f1->SetParameter(4,2.800);
   f1->SetParLimits(4,0.400,5.000);
   // BG peak amplitude
   //f1->SetParameter(5,100);
   //f1->SetParLimits(5,0,99999);
   f1->FixParameter(5,0.0);
   //BG peak mean
   f1->SetParameter(6,0.600);
   f1->SetParLimits(6,100,1000);
   // BG peak width
   f1->SetParameter(7,0.3);
   f1->SetParLimits(7,0,1000);

   // 1 electron peak amplitude
   f2->SetParameter(0,100);
   f2->SetParLimits(0,0.10,9999999);
   // 1 electron peak mean
   f2->SetParameter(1,1.0);
   f2->SetParLimits(1,0.5,2.0);
   // 1 electron peak width
   f2->SetParameter(2,0.500);
   f2->SetParLimits(2,0,9999);
   // 2 electron peak amplitude
   f2->SetParameter(3,1);
   f2->SetParLimits(3,0,9999);
   // 2 electron peak width
   f2->SetParameter(4,2.000);
   f2->SetParLimits(4,0.400,5.000);
   // 3 peak amplitude
   f2->SetParameter(5,1);
   f2->SetParLimits(5,0,99999);
   // 3 peak width
   f2->SetParameter(6,0.7);
   f2->SetParLimits(6,0,3000);

   TF1 *e1 = new TF1("e1","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e2 = new TF1("e2","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e3 = new TF1("e3","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   TF1 *e4 = new TF1("e4","[0]*exp(-0.5*((x-[1])/[2])**2)", xMin,xMax);
   e1->SetLineColor(kBlue);
   e2->SetLineColor(kGreen);
   e3->SetLineColor(kBlack);
   e3->SetLineStyle(2);
   e4->SetLineColor(2000);
   e4->SetLineStyle(2);

   // ----------------------------------------
   // Create differences
   TH1F * hcerEvenDiff = new TH1F(*hcerEvenNoLuc);
   hcerEvenDiff->Add(hcerEven,-1.0);
   TH1F * hcerOddDiff = new TH1F(*hcerOddNoLuc);
   hcerOddDiff->Add(hcerOdd,-1.0);
   // ----------------------------------------

   TCanvas * c = new TCanvas("cherenkov_perp18","cherenkov_perp18");
   c->Divide(1,2,0,0,0);

   c->cd(1);
   gPad->SetBottomMargin(0.0001);
   gPad->DrawFrame(0,0,1,100);
   hcerEven->Draw("E1");
   h1 = hcerEven;
   //h1->Rebin();
   fitResult = h1->Fit(f1,"S,R","same");
   e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
   e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
   e3->SetParameters(fitResult->Parameter(5),fitResult->Parameter(6),fitResult->Parameter(7));
   /*e4->SetParameters(fitResult->Parameter(5),3.0*fitResult->Parameter(1),fitResult->Parameter(6));*/
   e1->DrawCopy("same");
   e2->DrawCopy("same");
   e3->DrawCopy("same");

   Double_t single_track_0 = e1->Integral(0.0,4.0);
   Double_t single_track_1 = e1->Integral(cer_lo,cer_hi);
   Double_t single_track_2 = e1->Integral(0.0 ,cer_lo);
   Double_t single_track_3 = e1->Integral(cer_hi,4.0);
   Double_t double_track_0 = e2->Integral(0.0,4.0);
   Double_t double_track_1 = e2->Integral(cer_lo,cer_hi);
   Double_t double_track_2 = e2->Integral(cer_hi,4.0);
   Double_t double_contamination1 = double_track_1/single_track_1;
   Double_t double_eff1 =  1.0 - double_track_1/double_track_0;

   Double_t ratio1 = e2->Eval(2.0*fitResult->Parameter(1))/e1->Eval(fitResult->Parameter(1));
   /*e4->DrawCopy("same");*/
   /*h1->SetMarkerStyle(20);*/
   hcerEvenNoLuc->SetLineColor(2);
   hcerEvenNoLuc->Draw("same");
   hcerEvenDiff->SetLineColor(2);
   hcerEvenDiff->SetLineWidth(2);
   hcerEvenDiff->Draw("same");

   // 1 electron peak amplitude
   f1->SetParameter(0,100);
   f1->SetParLimits(0,0.10,9999999);
   // 1 electron peak mean
   f1->SetParameter(1,1.0);
   f1->SetParLimits(1,0.75,2.0);
   // 1 electron peak width
   f1->SetParameter(2,0.800);
   f1->SetParLimits(2,0,9999);
   // 2 electron peak amplitude
   f1->SetParameter(3,1);
   f1->SetParLimits(3,0,9999);
   // 2 electron peak width
   f1->SetParameter(4,2.000);
   f1->SetParLimits(4,0.400,5.000);
   // BG peak amplitude
   //f1->SetParameter(5,100);
   //f1->SetParLimits(5,0,99999);
   f1->FixParameter(5,0.0);
   //BG peak mean
   f1->SetParameter(6,0.600);
   f1->SetParLimits(6,100,1000);
   // BG peak width
   f1->SetParameter(7,0.3);
   f1->SetParLimits(7,0,1000);

   // ----------------------------------------------------------------
   c->cd(2);
   gPad->SetTopMargin(0.0001);
   gPad->DrawFrame(0,0,1,100);
   hcerOdd->Draw("E1");
   h1 = hcerOdd;
   if( Emin > 1600 ){
      //h1.Add(hcerOddDiff,-1.0);
      h1->Rebin();
      hcerOddNoLuc->Rebin();
      hcerOddDiff->Rebin();
      f1->SetMinimum(0.5);
   }
   fitResult = h1->Fit(f1,"S,R","same");
   e1->SetParameters(fitResult->Parameter(0),fitResult->Parameter(1),fitResult->Parameter(2));
   e2->SetParameters(fitResult->Parameter(3),2.0*fitResult->Parameter(1),fitResult->Parameter(4));
   e3->SetParameters(fitResult->Parameter(5),fitResult->Parameter(6),fitResult->Parameter(7));
   e1->DrawCopy("same");
   e2->DrawCopy("same");
   e3->DrawCopy("same");
   Double_t ratio2 = e2->Eval(2.0*fitResult->Parameter(1))/e1->Eval(fitResult->Parameter(1));

   Double_t single2_track_0        = e1->Integral(0.0,4.0);
   Double_t single2_track_1        = e1->Integral(cer_lo,cer_hi);
   Double_t single2_track_2        = e1->Integral(0.0 ,cer_lo);
   Double_t single2_track_3        = e1->Integral(cer_hi,4.0);
   Double_t double2_track_0        = e2->Integral(0.0,4.0);
   Double_t double2_track_1        = e2->Integral(cer_lo,cer_hi);
   Double_t double2_track_2        = e2->Integral(cer_hi,4.0);
   Double_t double_contamination2 = double2_track_1/single2_track_1;
   Double_t double_eff2           = 1.0 - double2_track_1/double2_track_0;

   hcerOddNoLuc->SetLineColor(2);
   hcerOddNoLuc->Draw("same");
   hcerOddDiff->SetLineColor(2);
   hcerOddDiff->SetLineWidth(2);
   hcerOddDiff->Draw("same");

   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);
   latex->DrawLatex(0.7,0.6,Form("E_{min}=%4.0fMeV",Emin));
   latex->DrawLatex(0.7,0.5,Form("Ratio  =%0.5f",ratio2));

   c->cd(1);
   latex->DrawLatex(0.7,0.6,Form("E_{min}=%4.0fMeV",Emin));
   latex->DrawLatex(0.7,0.5,Form("Ratio  =%0.5f",ratio1));

   c->SaveAs(Form("results/detectors/cherenkov/cherenkov_perp18_%d.png",int(Emin)));
   c->SaveAs(Form("results/detectors/cherenkov/cherenkov_perp18_%d.pdf",int(Emin)));
   c->SaveAs(Form("results/detectors/cherenkov/cherenkov_perp18_%d.eps",int(Emin)));

   std::ofstream datafile("results/detectors/cherenkov/cherenkov_perp18.txt",std::ios_base::app);
   datafile << Emin << " " << ratio1 << " " << ratio2 << std::endl;
   
   std::ofstream datafile2("results/detectors/cherenkov/cherenkov_perp18_cuts.txt",std::ios_base::app);
   datafile2 << Emin << " " << double_contamination1 << " " << double_eff1
                     << " " << double_contamination2 << " " << double_eff2 << std::endl;

   return 0;
}
