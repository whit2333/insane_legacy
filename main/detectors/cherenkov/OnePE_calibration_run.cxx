/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008 
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650  
 */
Int_t OnePE_calibration_run(Int_t runNumber=71805,Int_t isACalibration=0) {

   gStyle->SetLabelSize(0.08,"XYZ");
   gStyle->SetTitleSize(0.08,"XYZ");
   
   //   gStyle->SetTitleOffset(
   gStyle->SetPadTopMargin(0.01);
   gStyle->SetPadBottomMargin(0.18);
   gStyle->SetPadLeftMargin(0.09);
   gStyle->SetPadRightMargin(0.09);


   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if(!t) {printf("betaDetectors0 tree not found\n"); return(-1); }

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLED%d.root",runNumber),"UPDATE");
//     gROOT->SetBatch(kTRUE);
  //TSQLServer * db = SANEDatabaseManager::GetManager()->dbServer;
  //TString SQLCOMMAND(""); 


 
   TCanvas * c = new TCanvas("cherenkovLED","Cherenkov LED",800,200*2*1.62);
  c->Divide(2,4);

  TH1F * aMirrorHist;
  TFitResultPtr fitResult;
  TF1 *f1 = new TF1("f1", "gaus", 0,6000);
  f1->SetParameter(0,1000);
  f1->SetParameter(1,100);
  f1->SetParameter(2,50);

  for(int i = 0; i<8;i++) {
    c->cd(8 - i);
    t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(75,10,310)",i+1),Form(
            "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d",i+1),"",1000000,  1001);
    aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist",i+1));
    if(aMirrorHist){
       f1->SetParameter(1,aMirrorHist->GetMean(1));
       fitResult = aMirrorHist->Fit("gaus","M,E,S","",40,300);

/*
 SQLCOMMAND ="REPLACE into cherenkov_calibrations set `run_number`=";
 SQLCOMMAND += runnumber;

 SQLCOMMAND +=",pmt_number="; 
 SQLCOMMAND +=i+1;

 SQLCOMMAND +=",value="; 
 SQLCOMMAND +=fitResult->Parameter(1);

 SQLCOMMAND +=",calibration_type="; 
 SQLCOMMAND +="'relative'";

 SQLCOMMAND +=" ;"; 
 if(db) db->Query(SQLCOMMAND.Data());
*/
    //aMirrorHist->SetTitle(Form("Cherenkov Mirror %d",i+1));
    aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
    aMirrorHist->GetXaxis()->SetTitle("channels");//Form("Cherenkov Mirror %d",i+1));
    aMirrorHist->GetXaxis()->CenterTitle(1);
    aMirrorHist->Draw();
     }

  }

  c->SaveAs(Form("plots/%d/CherenkovLED.pdf",runNumber));
  c->SaveAs(Form("plots/%d/CherenkovLED.png",runNumber));
  c->SaveAs(Form("plots/%d/CherenkovLED.svg",runNumber));

//     gROOT->SetBatch(kFALSE);


// f->Write();


return(0);
}
