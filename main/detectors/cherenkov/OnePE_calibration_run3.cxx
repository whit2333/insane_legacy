/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008 
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650  

 */
Int_t OnePE_calibration_run3(Int_t runNumber=71805,Int_t isACalibration=0) {
   gStyle->SetLabelSize(0.08,"XYZ");
   gStyle->SetTitleSize(0.08,"XYZ");
   //   gStyle->SetTitleOffset(
   gStyle->SetPadTopMargin(0.01);
   gStyle->SetPadBottomMargin(0.18);
   gStyle->SetPadLeftMargin(0.09);
   gStyle->SetPadRightMargin(0.09);
   
   if (gROOT->LoadMacro("detectors/cherenkov/S_pmt.cxx") != 0) {
      Error(weh, "Failed loading detectors/cherenkov/S_pmt.cxx in compiled mode.");
      return -1;
   }

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if(!t) {printf("betaDetectors0 tree not found\n"); return(-1); }

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLED%d.root",runNumber),"UPDATE");
   //gROOT->SetBatch(kTRUE);
   //TSQLServer * db = SANEDatabaseManager::GetManager()->dbServer;
   //TString SQLCOMMAND(""); 

   TCanvas * c = new TCanvas("cherenkovLED","Cherenkov LED",800,200*2*1.62);
   c->Divide(2,4);

   TH1F * aMirrorHist;
   TFitResultPtr fitResult;

   //TF1 *f1 = new TF1("f1", "gaus", 0,6000);
   TF1 *f1 = new TF1("Spmt",S_pmt,0,1000,5);
   //f1->SetParNames("constant","coefficient");
   
   f1->SetParameter(0,1);
   f1->SetParLimits(0,0,10);

   f1->SetParameter(1,0);
   f1->SetParLimits(1,-10,10);

   f1->SetParameter(2,100);
   f1->SetParLimits(2,40,120);

   f1->SetParameter(3,50);
   f1->SetParLimits(3,5,60);

   f1->SetParameter(4,1000);


  for(int i = 0; i<1;i++) {
    c->cd(8 - i);
    gPad->SetLogy(1);
    t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(100,-25,975)",i+1),Form(
            "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d",i+1),"",1000000,  1001);
    aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist",i+1));
    if(aMirrorHist){
       //f1->SetParameter(1,aMirrorHist->GetMean(1));
       //aMirrorHist->Fit(f1,"M,E");
       fitResult = aMirrorHist->Fit(f1,"M,E,S","",-25,900);

    //aMirrorHist->SetTitle(Form("Cherenkov Mirror %d",i+1));
    aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
    aMirrorHist->GetXaxis()->SetTitle("channels");//Form("Cherenkov Mirror %d",i+1));
    aMirrorHist->GetXaxis()->CenterTitle(1);
    aMirrorHist->Draw();
     }

  }

  c->cd(1);
  f1->Draw();
  c->SaveAs(Form("plots/%d/CherenkovLED.pdf",runNumber));
  c->SaveAs(Form("plots/%d/CherenkovLED.png",runNumber));
  c->SaveAs(Form("plots/%d/CherenkovLED.svg",runNumber));

//     gROOT->SetBatch(kFALSE);


// f->Write();


return(0);
}
