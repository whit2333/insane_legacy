Int_t cherenkov_sums(Int_t runNumber=72988) {

   //load_style("MultiSquarePlot");
   //gROOT->SetStyle("MultiSquarePlot");

   Double_t max = 16000;
   //Double_t max = 26000;
   rman->SetRun(runNumber);

   const char * treeName = "betaDetectors1";
   TTree * t = (TTree*)gROOT->FindObject(treeName);
   if(!t){ std::cout << " TREE NOT FOUND : " << treeName << "\n"; return(-1);}

   TCanvas * c = new TCanvas("cherenkov_sums", "cherenkov_sums");
   //c->Divide(2,1);


//_____________________________________________________________________________

   //c->cd(1);
   t->Draw("fGasCherenkovHits.fADCAlign>>hcersum(100,0,4.000)",
            "triggerEvent.IsBETA2Event()&&fGasCherenkovHits.fChannel==9&&TMath::Abs(fGasCherenkovHits.fTDCAlign)<10",
            "goff");
   TH1F * h1 = (TH1F*) gROOT->FindObject("hcersum");
   if(h1) {
      h1->SetLineColor(1);
      h1->SetTitle("Analog and Software Sums");
      h1->SetMaximum(max);
      h1->Draw("");
      h1->GetXaxis()->SetTitle("ADC");
      h1->GetXaxis()->CenterTitle(true);
   }

   t->Draw("bigcalClusters.fCherenkovBestADCSum>>hcerbest(100,0,4.000)",
            "triggerEvent.IsBETA2Event()",
            "goff");
   TH1F * h1 = (TH1F*) gROOT->FindObject("hcerbest");
   if(h1) {
      h1->SetLineColor(1); 
      h1->SetFillStyle(3004);
      h1->SetFillColor(1);
      h1->DrawCopy("same");
   }

   t->Draw("bigcalClusters.fCherenkovBestADCSum>>hcerbest2(100,0,4.000)",
            "triggerEvent.IsBETA2Event()&&TMath::Abs(bigcalClusters.fCherenkovTDC)<10",
            "goff");
   TH1F * h1 = (TH1F*) gROOT->FindObject("hcerbest2");
   if(h1) {
      h1->SetLineColor(1);
      h1->SetFillColor(1);
      h1->SetFillStyle(3005);
      h1->DrawCopy("same");
   }

   c->SaveAs(Form("plots/%d/cherenkov_sums.png",runNumber));
   c->SaveAs(Form("plots/%d/cherenkov_sums.eps",runNumber));
   c->SaveAs(Form("plots/%d/cherenkov_sums.pdf",runNumber));
   c->SaveAs(Form("plots/%d/cherenkov_sums.tex",runNumber));

// t->StartViewer();

   return(0);
}
