/*! Fit to single photoelectron peak

   Uses 9 parameter fit. 

 - 71805
 - 71800 through 71807 - late Dec 2008
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650

 */
Int_t OnePE_calibration_combined6(Int_t aNumber = 0)
{

   bool isLogPlot = true;//false;
   gStyle->SetLabelSize(0.10, "XYZ");
   gStyle->SetTitleSize(0.09, "XYZ");

   gStyle->SetPadTopMargin(0.10);
   gStyle->SetPadBottomMargin(0.19);
   gStyle->SetPadLeftMargin(0.09);
   gStyle->SetPadRightMargin(0.01);

   aman->GetRunQueue()->push_back(71803);
   aman->GetRunQueue()->push_back(71804);
   aman->GetRunQueue()->push_back(71805);
   aman->GetRunQueue()->push_back(71806);
   aman->GetRunQueue()->push_back(71989);
   TChain * t = aman->BuildChain("betaDetectors0");

   //SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   //if (!(runManager->IsRunSet())) runManager->SetRun(runNumber);
   //else runNumber = runManager->fRunNumber;
   //runManager->GetCurrentFile()->cd();
   //InSANERun * run = runManager->GetCurrentRun();
   //TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");

   if (!t) { printf("betaDetectors0 tree not found\n"); return(-1);}

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLEDCombined%d.root", aNumber), "RECREATE");
   
   TCanvas * c = new TCanvas("cherenkovLED", "Cherenkov LED", 1000, 240 * 2 * 1.62);
   c->Divide(2, 4);

   TH1F               * aMirrorHist;
   TFitResultPtr        fitResult;

   /// Model of PMT response 
   InSANEPMTResponse2  * fptr = new InSANEPMTResponse2();
/*   fptr->SetNIntPoints(100);*/
   TF1                * f1   = new TF1("f2", fptr,
                                     &InSANEPMTResponse2::PMT_fit,
                                     -10, 400, 9,
                                     "InSANEPMTResponse2", "PMT_fit");

   f1->SetParName(0, "N0");
   f1->SetParName(1, "P0");
   f1->SetParName(2, "A");
   f1->SetParName(3, "pe");
   f1->SetParName(4, "xp");
   f1->SetParName(5, "sigp");
   f1->SetParName(6, "x0");
   f1->SetParName(7, "sig0");
   f1->SetParName(8, "mu");

   std::ofstream fitFile(Form("results/detectors/cherenkov/calibration/OnePE_calibration_combined6_fit_%d.txt",aNumber),std::ios_base::trunc);

   for (int i = 7; i >=0; i--) {
      c->cd(8 - i);
      gPad->SetLogy(isLogPlot);
      t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(150,-20,280)", i + 1), 
              Form("fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d", i + 1),
             "");

      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist", i + 1));

      c->Update();
      if (aMirrorHist) {
         std::cout << "\n Fitting Mirror " << i + 1 << "\n";

         f1->SetParameter(0, 2.0e5);                   // N0
         f1->SetParameter(1,0.98);// TMath::Poisson(0, 0.05)); // P0
         f1->SetParLimits(1, 0.95, 0.999999);           // P0
         f1->SetParameter(2, 20.0);                   // A
         f1->SetParLimits(2, 20, 500.0);              // A
         f1->SetParameter(3, 0.5);                     // pe
         f1->SetParLimits(3, 0.2, 0.7);              // pe
         f1->SetParameter(4,                           // xp
            aMirrorHist->GetBinCenter(aMirrorHist->GetMaximumBin()) ); // xp
         f1->SetParLimits(4, -20.0, 30.0);             // xp
         f1->SetParameter(5, 2.5);                     // sigp
         f1->SetParLimits(5, 0.0, 7.0);                // sigp
         f1->SetParameter(6, 90.0);                    // x0
         f1->SetParLimits(6, 70.0, 150.0);             // x0
         f1->SetParameter(7, 30.0);                    // sig0
         f1->SetParLimits(7, 10.0, 100.0);             // sig0
         f1->SetParameter(8, 0.01);                   // mu
         f1->SetParLimits(8, 0.000001, 0.05);                // mu

         Double_t p0bin = aMirrorHist->GetMaximumBin() + 15;
         Double_t P0 = aMirrorHist->Integral(0, p0bin) / ((double)(aMirrorHist->GetEntries()));
         Double_t lowerlimit = aMirrorHist->GetBinCenter(aMirrorHist->GetMaximumBin()) - 20.0;

         //f1->SetParameter(1, P0);  // P0
         f1->FixParameter(1, P0);  // P0
         //f1->SetParameter(8, -1.0*TMath::Log(P0) );  // P0
         f1->FixParameter(8, -1.0*TMath::Log(P0) );  // P0

         std::cout << "   ped upper limit : " << aMirrorHist->GetBinCenter(p0bin) << std::endl;
         std::cout << "   ped lower limit : " << lowerlimit << std::endl;
         std::cout << "   initial P0 : " << P0 << std::endl;

         fitResult = aMirrorHist->Fit(f1, "S", "goff", lowerlimit, 275);
         //fitResult = aMirrorHist->Fit(f1, "E,S", "", lowerlimit, 275);
         //f1->SetLineColor(kCyan);


         aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->SetTitle("channels");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetYaxis()->SetTitle("counts");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->CenterTitle(1);
         aMirrorHist->GetYaxis()->CenterTitle(1);
         if(isLogPlot)  aMirrorHist->SetMinimum(0.5);
         if(!isLogPlot) aMirrorHist->SetMaximum(45.0);
         aMirrorHist->SetLineWidth(1);
         aMirrorHist->Draw();
         f1->SetRange(lowerlimit,275);
         f1->SetLineWidth(1);
         f1->DrawClone("same");

         fptr->SetParameters(f1->GetParameters());

         TF1 *fpexp = fptr->GetExpFunction();
         fpexp->SetRange(lowerlimit,275);
         fpexp->SetLineWidth(1);
         //fpexp->SetLineColor(1);
         fpexp->SetLineStyle(2);

         TF1 * fp0 = fptr->GetPeakFunction(0);
         fp0->SetRange(lowerlimit,275);
         fp0->SetLineStyle(3);
         fp0->SetLineWidth(1);
         //fp0->SetLineColor(2);

         TF1 * fp1 = fptr->GetPeakFunction(1);
         fp1->SetRange(lowerlimit,275);
         fp1->SetLineWidth(1);
         //fp1->SetLineColor(4);
         fp1->SetLineStyle(1);

         TF1 * fp2 = fptr->GetPeakFunction(2);
         fp2->SetRange(lowerlimit,275);
         fp2->SetLineWidth(1);
         //fp2->SetLineColor(4);
         fp2->SetLineStyle(1);

         fpexp->DrawClone("same");
         fp0->DrawClone("same");
         fp1->DrawClone("same");
         fp2->DrawClone("same");

         fitFile << i + 1 << " " ; 
         for(int j = 0; j<9; j++) {
            fitFile << f1->GetParameter(j) << " " ; 
         }
         fitFile << std::endl;
      }
   }

   c->SaveAs(Form("results/detectors/cherenkov/OnePE_calibration_combined6_%d.pdf", aNumber));
   c->SaveAs(Form("results/detectors/cherenkov/OnePE_calibration_combined6_%d.png", aNumber));
   c->SaveAs(Form("results/detectors/cherenkov/OnePE_calibration_combined6_%d.svg", aNumber));
//     gROOT->SetBatch(kFALSE);


   f->Write();


   return(0);
}
