/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650

 */
Int_t LED_fit1(Int_t runNumber = 71835, Int_t isACalibration = 0)
{
   gStyle->SetLabelSize(0.08, "XYZ");
   gStyle->SetTitleSize(0.08, "XYZ");
   //   gStyle->SetTitleOffset(
   gStyle->SetPadTopMargin(0.01);
   gStyle->SetPadBottomMargin(0.18);
   gStyle->SetPadLeftMargin(0.09);
   gStyle->SetPadRightMargin(0.09);

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if (!(runManager->IsRunSet())) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if (!t) {
      printf("betaDetectors0 tree not found\n");
      return(-1);
   }

   Double_t * cer_pars[8];

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLED%d.root", runNumber), "UPDATE");

   TCanvas * c = new TCanvas("cherenkovLED", "Cherenkov LED", 800, 200 * 2 * 1.62);
   c->Divide(2, 4);

   TH1F * aMirrorHist;
   TFitResultPtr fitResult;

   InSANEPMTResponse * fptr = new InSANEPMTResponse();  // create the user function class
   TF1 * f1 = new TF1("f1", fptr, &InSANEPMTResponse::PMT_fit, -10, 2000, 12, "InSANEPMTResponse", "PMT_fit");
         f1->SetParName(0, "N0");
         f1->SetParName(1, "P0");
         f1->SetParName(2, "P1");
         f1->SetParName(3, "A");
         f1->SetParName(4, "pe");
         f1->SetParName(5, "xp");
         f1->SetParName(6, "sigp");
         f1->SetParName(7, "x0");
         f1->SetParName(8, "sig0");
         f1->SetParName(9, "mu");
         f1->SetParName(10, "x1");
         f1->SetParName(11, "sig1");

   for (int i = 0; i < 8; i++) {
      c->cd(8 - i);
      /*      gPad->SetLogy(1);*/
      t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(200,-25,1375)", i + 1), Form(
                 "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d", i + 1),
              "",
              1000000
              1001);
      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist", i + 1));
      if (aMirrorHist) {
         std::cout << " Fitting Mirror " << i + 1 << "\n";

         f1->SetParameter(0, 2.0e3);                 // N0
         f1->SetParameter(1, 0.0001); // P0
         f1->SetParLimits(1, 0.0, 1.0);
         f1->SetParameter(2, TMath::Poisson(1, 5)); // P1
         f1->SetParLimits(2, 0.0, 1.0);
         f1->SetParameter(3, 30);                 // A
         f1->SetParLimits(3, 0.0, 500.0);
         f1->SetParameter(4, 0.002);                  // pe
         f1->SetParLimits(4, 0.0, 1.0);
         f1->FixParameter(5,0.0);                  // xp
         f1->SetParLimits(5,-10.0,10.0);
         f1->FixParameter(6, 2.5);                  // sigp
         f1->SetParLimits(6,0.5,5.0);                  // sigp
         f1->FixParameter(7, 100.0);                 // x0
         f1->SetParLimits(7,80.0,150.0);                  // x0
         f1->FixParameter(8, 30.0);                 // sig0
         f1->SetParLimits(8,10.0,100.0);                   // sig0
         f1->SetParameter(9, 3.0);                  // mu
         f1->SetParLimits(9, 0.0, 20.0);                 // mu
         f1->SetParameter(10, 100.0);                   // x1
         f1->SetParLimits(10, 70.0, 120.0);                 // x1
         f1->SetParameter(11, 30.0);                  // sig1
         f1->SetParLimits(11, 10.0, 60.0);                 // sig1

         fitResult = aMirrorHist->Fit(f1, "S", "", 30, 1575);
         fptr->SetParameters(f1->GetParameters());

         aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->SetTitle("channels");
         aMirrorHist->GetXaxis()->CenterTitle(1);
         aMirrorHist->SetMaximum(aMirrorHist->GetMaximum());
         aMirrorHist->Draw();
         f1->DrawClone("same");

         /// Draw the fit components
         Int_t linestyle = 2;
         TF1 *fpexp = fptr->GetExpFunction();
         fpexp->SetLineColor(3);
         //fpexp->SetLineStyle(linestyle);
         fpexp->SetLineWidth(2);
         fpexp->DrawClone("same");

         TF1 *fpexp = fptr->GetExpFunction();
         fpexp->SetLineColor(3);
         fpexp->DrawClone("same");

         TF1 * fp0 = fptr->GetPeakFunction(0);
         fp0->SetLineColor(4);
         fp0->SetLineStyle(linestyle);
         fp0->SetLineWidth(2);
         fp0->DrawClone("same");

         for (int np = 1; np < 12; np++) {
            TF1 * fp1 = fptr->GetPeakFunction(np);
            fp1->SetLineWidth(1);
            if (np == 1)fp1->SetLineColor(4);
            else fp1->SetLineColor(2);
            fp1->SetLineStyle(2);
           fp1->DrawClone("same");
         }

      }
   }

   c->SaveAs(Form("plots/%d/LED_fit1.pdf", runNumber));
   c->SaveAs(Form("plots/%d/LED_fit1.png", runNumber));
   c->SaveAs(Form("plots/%d/LED_fit1.svg", runNumber));

//     gROOT->SetBatch(kFALSE);


// f->Write();


   return(0);
}
