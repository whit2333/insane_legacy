/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650

 */
Int_t performance_by_mirror4(Int_t runNumber = 72994, Int_t mirrorNumber= 8) {

   if (gROOT->LoadMacro("util/read_data_table_column_to_map.cxx") != 0) {
      Error(weh, "Failed loading read_data_table_column_to_map.cxx in compiled mode.");
      return -1;
   }

   //gStyle->SetLabelSize(0.08, "XYZ");
   //gStyle->SetTitleSize(0.08, "XYZ");
   ////   gStyle->SetTitleOffset(
   //gStyle->SetPadTopMargin(0.01);
   //gStyle->SetPadBottomMargin(0.18);
   //gStyle->SetPadLeftMargin(0.09);
   //gStyle->SetPadRightMargin(0.09);

   Int_t n_calib_cols = 11;
   Int_t res = 0;
   //TString fname = Form("results/detectors/cherenkov/calibration/OnePE_calibration_run6_71805.txt") ;
   TString fname = Form("results/detectors/cherenkov/calibration/LED_fit5_6.txt") ;

   // A = 3
   std::map<int,double> map_A;
   res = read_data_table_column_to_map(fname.Data(), &map_A, 2, n_calib_cols); 
   if(res) return(res);

   // pe = 4
   std::map<int,double> map_pe;
   res = read_data_table_column_to_map(fname.Data(), &map_pe, 3, n_calib_cols); 
   if(res) return(res);

   // xp = 5
   std::map<int,double> map_xp;
   res = read_data_table_column_to_map(fname.Data(), &map_xp, 4, n_calib_cols); 
   if(res) return(res);

   // sigp = 6
   std::map<int,double> map_sigp;
   res = read_data_table_column_to_map(fname.Data(), &map_sigp, 5, n_calib_cols); 
   if(res) return(res);

   // x0 = 7 
   std::map<int,double> map_x0;
   res = read_data_table_column_to_map(fname.Data(), &map_x0, 6, n_calib_cols); 
   if(res) return(res);

   // sig0 = 8 
   std::map<int,double> map_sig0;
   res = read_data_table_column_to_map(fname.Data(), &map_sig0, 7, n_calib_cols); 
   if(res) return(res);

   // x1 = 10 
   std::map<int,double> map_x1;
   res = read_data_table_column_to_map(fname.Data(), &map_x1, 9, n_calib_cols); 
   if(res) return(res);
   std::cout << map_x1[1] << std::endl;

   // sig1 = 11 
   std::map<int,double> map_sig1;
   res = read_data_table_column_to_map(fname.Data(), &map_sig1, 10, n_calib_cols); 
   if(res) return(res);

   std::cout << " Done reading parameters" << std::endl;

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   SANEEvents * events = new SANEEvents("betaDetectors1");
   if(events->fTree) events->SetClusterBranches(events->fTree);
   else{ std::cout << " TREE NOT FOUND : asdfasdf"  << "\n"; return(-1);}

   TDirectory * cerPerformanceDir=0;
   TFile * cerFile = new TFile("data/cherenkov_performance_by_mirror.root","UPDATE");
   if( cerFile ) {
      if( !(cerFile->cd(Form("cerPerformance%d",runNumber)) ) ) {
         cerPerformanceDir = cerFile->mkdir(Form("cerPerformance%d",runNumber)) ;
         if( !(cerPerformanceDir->cd()) ) printf(" dir Error\n");
      } else {
         cerPerformanceDir = gDirectory;
      }
   }

   cerFile->cd();

   TCanvas * c = new TCanvas("performance_by_mirror4", "performance_by_mirror4");
   //c->Divide(2, 4);

   TH1F * aMirrorHist;
   TFitResultPtr fitResult;

   InSANEPMTResponse4 * fptr = new InSANEPMTResponse4();  // create the user function class
   fptr->SetMaxNPE(60);
   TF1 * f1 = new TF1("f1", fptr, &InSANEPMTResponse4::PMT_fit, -10, 6000, 13, "InSANEPMTResponse4", "PMT_fit");
         f1->SetParName(0, "N0");
         f1->SetParName(1, "P_{0}");
         f1->SetParName(2, "P_{1}");
         f1->SetParName(3, "A");
         f1->SetParName(4, "p_{E}");
         f1->SetParName(5, "x_{p}");
         f1->SetParName(6, "#sigma_{p}");
         f1->SetParName(7, "x_{0}");
         f1->SetParName(8, "#sigma_{0}");
         f1->SetParName(9, "#mu");
         f1->SetParName(10, "x_{1}");
         f1->SetParName(11, "#sigma_{1}");
         f1->SetParName(12, "A_{2}");

            gPad->SetLogy(1);
      events->fTree->Draw(Form("bigcalClusters.fCherenkovBestADC>>mirror%dhist(100,0,6000)", mirrorNumber), 
                          Form(
                 "TMath::Abs(bigcalClusters.fCherenkovTDC)<20&&bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fPrimaryMirror==%d", mirrorNumber),
              "");
      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist", mirrorNumber));
      if (aMirrorHist) {
         std::cout << " Fitting Mirror " << mirrorNumber << "\n";

         f1->SetParameter(0, 2.0e3);                 // N0
         f1->SetParameter(1, 0.001);                 // P0
         f1->SetParLimits(1, 0.0, 1.0);
         f1->SetParameter(2, TMath::Poisson(1, 18)); // P1
         f1->SetParLimits(2, 0.0, 1.0);
         f1->FixParameter(3, map_A[mirrorNumber]);            // A
         f1->FixParameter(4, map_pe[mirrorNumber]);           // pe
         f1->FixParameter(5, map_xp[mirrorNumber]);           // xp
         f1->FixParameter(6, map_sigp[mirrorNumber]);         // sigp
         f1->FixParameter(7, map_x0[mirrorNumber]);           // x0
         f1->FixParameter(8, map_sig0[mirrorNumber]);         // sig0
         f1->SetParameter(9, 20.0);                  // mu
         f1->SetParLimits(9, 0.0, 40.0);             // mu
         f1->FixParameter(10, map_x1[mirrorNumber]);          // x1
         f1->FixParameter(11, map_sig1[mirrorNumber]);        // sig1
         f1->SetParameter(12, 0.01);                 // A2
         f1->SetParLimits(12, 0.0, 1.0);             // A2
         fitResult = aMirrorHist->Fit(f1, "S", "", 800, 6000);
         fptr->SetParameters(f1->GetParameters());

         aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",mirrorNumber));
         aMirrorHist->GetXaxis()->SetTitle("channels");
         aMirrorHist->GetXaxis()->CenterTitle(1);
         aMirrorHist->SetMaximum(aMirrorHist->GetMaximum());
         aMirrorHist->SetFillColor(kYellow-9);
         aMirrorHist->SetLineWidth(1);
         aMirrorHist->Draw();
         f1->DrawClone("same");

         /// Draw the fit components
         Int_t linestyle = 2;

         TF1 *fpexp = fptr->GetExpFunction();
         fpexp->SetLineColor(4);
         fpexp->SetLineStyle(linestyle);
         fpexp->DrawClone("same");

         TF1 * fp0 = fptr->GetPeakFunction(0);
         fp0->SetLineColor(4);
         fp0->SetLineStyle(linestyle);
         fp0->SetLineWidth(2);
         fp0->DrawClone("same");

         for (int np = 1; np < 35; np++) {
            TF1 * fp1 = fptr->GetPeakFunction(np);
            fp1->SetLineWidth(1);
            if (np == 1)fp1->SetLineColor(4);
            else fp1->SetLineColor(2);
            fp1->SetLineStyle(2);
           fp1->DrawClone("same");
         }

         for (int np = 5; np < 50; np++) {
            TF1 * fp1 = fptr->GetDoublePeakFunction(np);
            fp1->SetLineWidth(1);
            fp1->SetLineColor(4);
            fp1->SetLineStyle(2);
            fp1->SetRange(100,8000);
            fp1->DrawClone("same");
         }

      }

      aMirrorHist->Write();


   c->SaveAs(Form("plots/%d/performanc_by_mirror4_%d.pdf", runNumber,mirrorNumber));
   c->SaveAs(Form("plots/%d/performanc_by_mirror4_%d.png", runNumber,mirrorNumber));
   c->SaveAs(Form("plots/%d/performanc_by_mirror4_%d.svg", runNumber,mirrorNumber));

//     gROOT->SetBatch(kFALSE);


// f->Write();


   return(0);
}
