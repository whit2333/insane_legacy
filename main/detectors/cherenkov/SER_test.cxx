Int_t SER_test(){

   InSANEPMTResponse * fptr = new InSANEPMTResponse();  // create the user function class

   TF1 * f2 = new TF1("f2",fptr,&InSANEPMTResponse::SER,-20,400,6,"InSANEPMTResponse","SER");
   f2->SetParameter(0,2.0); //A
   f2->SetParameter(1,0.1); //pe
   f2->SetParameter(2,0.0); //xp
   f2->SetParameter(3,2.5); //sigp
   f2->SetParameter(4,100.0); //x0
   f2->SetParameter(5,40.0); //sig0
   f2->DrawClone();
   //fptr->SetNIntPoints(50);
   f2->SetLineColor(4);
   f2->DrawClone("same");

   
   fptr->SetNIntPoints(100);
   TF1 * f1 = new TF1("f1",fptr,&InSANEPMTResponse::SER,-20,400,6,"InSANEPMTResponse","SER");
   f1->SetParameter(0,2.0); //A
   f1->SetParameter(1,0.5); //pe
   f1->SetParameter(2,0.0); //xp
   f1->SetParameter(3,2.5); //sigp
   f1->SetParameter(4,100.0); //x0
   f1->SetParameter(5,40.0); //sig0   f1->SetLineColor(2);
   f1->SetLineColor(2);
   f1->DrawClone("same");

   TF1 * f3 = new TF1("f3",fptr,&InSANEPMTResponse::f_N_wrap,-20,400,7,"InSANEPMTResponse","f_N_wrap");
   f3->SetParameter(0,2.0); //A
   f3->SetParameter(1,0.1); //pe
   f3->SetParameter(2,0.0); //xp
   f3->SetParameter(3,2.5); //sigp
   f3->SetParameter(4,100.0); //x0
   f3->SetParameter(5,40.0); //sig0
   f3->SetParameter(6,3);   //N
   //fptr->SetNIntPoints(200);
   f3->SetLineColor(kGreen-5);
   f3->DrawClone("same");

   std::cout << f2->Integral(-20,400) << "\n";

   return(0);
}

