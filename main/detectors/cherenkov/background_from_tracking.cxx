/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650

 */
Int_t background_from_tracking(Int_t runNumber = 72999, Int_t mirrorNumber= 2)
{
   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   SANEEvents * events = new SANEEvents("betaDetectors1");
   if(events->fTree) {
      events->SetClusterBranches(events->fTree);
      events->fTree->BuildIndex("fRunNumber","fEventNumber");
   } else{ std::cout << " TREE NOT FOUND : betaDetectors1"  << "\n"; return(-1);}

   TTree * t = (TTree*)gROOT->FindObject("Tracks");
   if(!t) return(-2);
   t->AddFriend(events->fTree);

   TH1F * aMirrorHist;
   TFitResultPtr fitResult;
   InSANEDISTrajectory * fTrajectory = new InSANEDISTrajectory();
   t->SetBranchAddress("trajectory",&fTrajectory);
   InSANETriggerEvent  * fTriggerEvent = new InSANETriggerEvent();
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   /// Create the output file/folder
   TDirectory * cerPerformanceDir=0;
   TFile * cerFile = new TFile("data/cherenkov_performance_by_mirror.root","UPDATE");
   if( cerFile ) {
      if( !(cerFile->cd(Form("cerPerformance%d",runNumber)) ) ) {
         cerPerformanceDir = cerFile->mkdir(Form("cerPerformance%d",runNumber)) ;
         if( !(cerPerformanceDir->cd()) ) printf(" dir Error\n");
      } else {
         cerPerformanceDir = gDirectory;
      }
   }

   /// Histograms
   Int_t tdc_min = -1000;
   Int_t tdc_max = 1000;
   TH1F * hCerADCs = new TH1F("hCerADC","hCerADC",100,0,6000);
   TH1F * hCerTDCs = new TH1F("hCerTDC","hCerTDC",100,tdc_min,tdc_max);

   TH1F * hCerADCs0 = new TH1F("hCerADC0","hCerADC0",100,0,6000);
   TH1F * hCerTDCs0 = new TH1F("hCerTDC0","hCerTDC0",100,tdc_min,tdc_max);

   TH1F * hCerADCs2 = new TH1F("hCerADC2","hCerADC2",100,0,6000);
   TH1F * hCerTDCs2 = new TH1F("hCerTDC2","hCerTDC2",100,tdc_min,tdc_max);

   TH1F * hCerADCs3 = new TH1F("hCerADC3","hCerADC3",100,0,6000);
   TH1F * hCerTDCs3 = new TH1F("hCerTDC3","hCerTDC3",100,tdc_min,tdc_max);

   TH1F * hCerADCs4 = new TH1F("hCerADC4","hCerADC4",100,0,6000);
   TH1F * hCerTDCs4 = new TH1F("hCerTDC4","hCerTDC4",100,tdc_min,tdc_max);

   BIGCALCluster * aClust = 0;

   /// EVENT LOOP
   Int_t fEntries = t->GetEntries();
   for(Int_t ievent = 0; ievent <fEntries  ; ievent++) {

      if(ievent%10000 == 0) std::cout << ievent << "/" << fEntries << "\n";
      t->GetEntry(ievent);
      events->fTree->GetEntryWithIndex(fTriggerEvent->fRunNumber,fTriggerEvent->fEventNumber);
      //std::cout << fTriggerEvent->fEventNumber  << " vs " << events->TRIG->fEventNumber << "\n";

      if(fTriggerEvent->fEventNumber == events->TRIG->fEventNumber){

      aClust = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[fTrajectory->fClusterNumber];

      if(aClust->fPrimaryMirror == mirrorNumber) {
         hCerADCs->Fill(aClust->fCherenkovBestADC);
         hCerTDCs->Fill(aClust->fCherenkovTDC);

         if(aClust->fNumberOfMirrors == 1){
            hCerADCs2->Fill(aClust->fCherenkovBestADC);
            hCerTDCs2->Fill(aClust->fCherenkovTDC);

            if(TMath::Abs(aClust->fCherenkovTDC) < 200){
               hCerADCs0->Fill(aClust->fCherenkovBestADC);
               hCerTDCs0->Fill(aClust->fCherenkovTDC);
            }

            if(TMath::Abs(aClust->fCherenkovTDC) < 20){
               hCerADCs3->Fill(aClust->fCherenkovBestADC);
               hCerTDCs3->Fill(aClust->fCherenkovTDC);
            }

            if(TMath::Abs(aClust->fCherenkovTDC) < 500){
               hCerADCs4->Fill(aClust->fCherenkovBestADC);
               hCerTDCs4->Fill(aClust->fCherenkovTDC);
            }
         }
      }
      }
   }



   TCanvas * c = new TCanvas("background_from_tracking", "background_from_tracking");
   c->Divide(2, 1);
   c->cd(1);
   gPad->SetLogy(1);
   hCerADCs->Draw();

   hCerADCs2->SetFillColor(kYellow-9);
   hCerADCs2->Draw("same");

   hCerTDCs4->SetFillColor(kRed-9);
   hCerTDCs4->Draw("same");

   hCerADCs0->SetFillColor(kBlue-9);
   hCerADCs0->Draw("same");
   hCerADCs3->SetFillColor(kGreen-9);
   hCerADCs3->Draw("same");

   c->cd(2);
   gPad->SetLogy(1);
   hCerTDCs->Draw();
   hCerTDCs2->SetFillColor(kYellow-9);
   hCerTDCs2->Draw("same");

   hCerTDCs4->SetFillColor(kRed-9);
   hCerTDCs4->Draw("same");

   hCerTDCs0->SetFillColor(kBlue-9);
   hCerTDCs0->Draw("same");

   hCerTDCs3->SetFillColor(kGreen-9);
   hCerTDCs3->Draw("same");

   c->SaveAs(Form("plots/%d/background_from_tracking_%d.pdf", runNumber,mirrorNumber));
   c->SaveAs(Form("plots/%d/background_from_tracking_%d.png", runNumber,mirrorNumber));
   c->SaveAs(Form("plots/%d/background_from_tracking_%d.svg", runNumber,mirrorNumber));

   return(0);


   InSANEPMTResponse4 * fptr = new InSANEPMTResponse4();  // create the user function class
   fptr->SetMaxNPE(60);
   TF1 * f1 = new TF1("f1", fptr, &InSANEPMTResponse4::PMT_fit, -10, 6000, 13, "InSANEPMTResponse4", "PMT_fit");
         f1->SetParName(0, "N0");
         f1->SetParName(1, "P0");
         f1->SetParName(2, "P1");
         f1->SetParName(3, "A");
         f1->SetParName(4, "pe");
         f1->SetParName(5, "xp");
         f1->SetParName(6, "sigp");
         f1->SetParName(7, "x0");
         f1->SetParName(8, "sig0");
         f1->SetParName(9, "mu");
         f1->SetParName(10, "x1");
         f1->SetParName(11, "sig1");
         f1->SetParName(12, "A2");

            gPad->SetLogy(1);
      events->fTree->Draw(Form("bigcalClusters.fCherenkovBestADC>>mirror%dhist(200,0,6000)", mirrorNumber), 
                          Form(
                 "TMath::Abs(bigcalClusters.fCherenkovTDC)<20&&bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fPrimaryMirror==%d", mirrorNumber),
              "");

      events->fTree->Draw(Form("bigcalClusters.fCherenkovTDC>>mirror%dhist(200,0,6000)", mirrorNumber), 
                          Form(
                 "TMath::Abs(bigcalClusters.fCherenkovTDC)<20&&bigcalClusters.fNumberOfMirrors==1&&bigcalClusters.fPrimaryMirror==%d", mirrorNumber),
              "");

      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist", mirrorNumber));
      if (aMirrorHist) {
         std::cout << " Fitting Mirror " << mirrorNumber << "\n";

         f1->SetParameter(0, 2.0e3);                 // N0
         f1->SetParameter(1, 0.001); // P0
         f1->SetParLimits(1, 0.0, 1.0);
         f1->SetParameter(2, TMath::Poisson(1, 18)); // P1
         f1->SetParLimits(2, 0.0, 1.0);
         f1->SetParameter(3, 30);                 // A
         f1->SetParLimits(3, 0.0, 500.0);
         f1->SetParameter(4, 0.002);                  // pe
         f1->SetParLimits(4, 0.0, 1.0);
         f1->FixParameter(5,0.0);                  // xp
         f1->SetParLimits(5,-10.0,10.0);
         f1->FixParameter(6, 2.5);                  // sigp
         f1->SetParLimits(6,0.5,5.0);                  // sigp
         f1->FixParameter(7, 100.0);                 // x0
         f1->SetParLimits(7,80.0,150.0);                  // x0
         f1->FixParameter(8, 30.0);                 // sig0
         f1->SetParLimits(8,10.0,100.0);                   // sig0
         f1->SetParameter(9, 20.0);                  // mu
         f1->SetParLimits(9, 0.0, 30.0);                 // mu
         f1->SetParameter(10, 100.0);                   // x1
         f1->SetParLimits(10, 70.0, 120.0);                 // x1
         f1->SetParameter(11, 30.0);                  // sig1
         f1->SetParLimits(11, 10.0, 60.0);                 // sig1
         f1->SetParameter(12, 0.01);                  // sig1
         f1->SetParLimits(12, 0.0, 1.0);                 // sig1
         fitResult = aMirrorHist->Fit(f1, "S", "", 800, 6000);
         fptr->SetParameters(f1->GetParameters());

         aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->SetTitle("channels");
         aMirrorHist->GetXaxis()->CenterTitle(1);
         aMirrorHist->SetMaximum(aMirrorHist->GetMaximum());
         aMirrorHist->SetFillColor(kYellow-9);
         aMirrorHist->SetLineWidth(1);
         aMirrorHist->Draw();
         f1->DrawClone("same");

         /// Draw the fit components
         Int_t linestyle = 2;

         TF1 *fpexp = fptr->GetExpFunction();
         fpexp->SetLineColor(4);
         fpexp->SetLineStyle(linestyle);
         fpexp->DrawClone("same");

         TF1 * fp0 = fptr->GetPeakFunction(0);
         fp0->SetLineColor(4);
         fp0->SetLineStyle(linestyle);
         fp0->SetLineWidth(2);
         fp0->DrawClone("same");

         for (int np = 1; np < 35; np++) {
            TF1 * fp1 = fptr->GetPeakFunction(np);
            fp1->SetLineWidth(1);
            if (np == 1)fp1->SetLineColor(4);
            else fp1->SetLineColor(2);
            fp1->SetLineStyle(2);
           fp1->DrawClone("same");
         }

         for (int np = 5; np < 50; np++) {
            TF1 * fp1 = fptr->GetDoublePeakFunction(np);
            fp1->SetLineWidth(1);
            fp1->SetLineColor(4);
            fp1->SetLineStyle(2);
            fp1->DrawClone("same");
         }

      }

      aMirrorHist->Write();


   c->SaveAs(Form("plots/%d/performanc_by_mirror5_%d.pdf", runNumber,mirrorNumber));
   c->SaveAs(Form("plots/%d/performanc_by_mirror5_%d.png", runNumber,mirrorNumber));
   c->SaveAs(Form("plots/%d/performanc_by_mirror5_%d.svg", runNumber,mirrorNumber));

//     gROOT->SetBatch(kFALSE);


// f->Write();


   return(0);
}
