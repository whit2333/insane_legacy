/** Finds the mirror edges of the Gas Cherenkov
 */
Int_t mirror_edges(Int_t runnumber=72994) {

   const char * detectorTree = "betaDetectors1";
   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   TTree * fClusterTree = (TTree*)gROOT->FindObject(detectorTree);
      if(!fClusterTree) {
      std::cout << "No Cluster Tree Found \n";
   }
   if(!fClusterTree) {printf("Clusters tree not found eh\n"); return(-1); }

   TCanvas * c = new TCanvas("mirror_edges","mirror_edges",600,800);

  fClusterTree->Draw("bigcalClusters.fYMoment:bigcalClusters.fXMoment>>cer_edges(50,-60,60,60,-110,110)",
                     "bigcalClusters.fCherenkovBestADCSum>0.4 &&bigcalClusters.fNumberOfGoodMirrors>=2 &&bigcalClusters.fTotalE>600 && \
                      !(bigcalClusters.fIsNoisyChannel)","colz");//&&bigcalClusters.fGoodCherenkovTiming

   TH2F * h2 = (TH2F*)gROOT->FindObject("cer_edges");
   if(h2)h2->SetTitle("#check{C}erenkov Mirror Edges");
   if(h2) {
      h2->SetMaximum(h2->GetMaximum()*0.6); 
      h2->Draw("col");
   }
          
   c->SaveAs(Form("plots/%d/mirror_edges.png",runnumber));

   return(0);
}

