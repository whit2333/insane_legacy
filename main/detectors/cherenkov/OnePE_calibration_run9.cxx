/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650

 */
Int_t OnePE_calibration_run9(Int_t runNumber = 71805, Int_t isACalibration = 0)
{
   gStyle->SetLabelSize(0.08, "XYZ");
   gStyle->SetTitleSize(0.08, "XYZ");
   //   gStyle->SetTitleOffset(
   gStyle->SetPadTopMargin(0.05);
   gStyle->SetPadBottomMargin(0.18);
   gStyle->SetPadLeftMargin(0.09);
   gStyle->SetPadRightMargin(0.09);

   std::vector< vector<double> > cer_pars;
   cer_pars.resize(8);
   for (int i = 0; i < 8 ; ++i) {
      cer_pars[i].resize(15);
   }

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if (!(runManager->IsRunSet())) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if (!t) {
      printf("betaDetectors0 tree not found\n");
      return(-1);
   }

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLED%d.root", runNumber), "UPDATE");

   TCanvas * c = new TCanvas("cOnePE_calibration_run9", "OnePE_calibration_run9", 800, 200 * 2 * 1.62);
   c->Divide(2, 4);

   TH1F * aMirrorHist;
   TFitResultPtr fitResult;

   InSANEPMTResponse * fptr = new InSANEPMTResponse();  // create the user function class
   TF1 * f1 = new TF1("f2", fptr, &InSANEPMTResponse::PMT_fit3, -10, 400, 9, "InSANEPMTResponse", "PMT_fit3");
   f1->SetParName(0, "N0");
   f1->SetParName(1, "P0");
   f1->SetParName(2, "A");
   f1->SetParName(3, "pe");
   f1->SetParName(4, "xp");
   f1->SetParName(5, "sigp");
   f1->SetParName(6, "x0");
   f1->SetParName(7, "sig0");
   f1->SetParName(8, "mu");

   for (int i = 0; i < 1; i++) {
      c->cd(8 - i);
      gPad->SetLogy(1);
      t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(100,-20,280)", i + 1), Form(
                 "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d", i + 1),
              "",
              1000000,
              1001);
      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist", i + 1));
      if (aMirrorHist) {

         std::cout << " Fitting Mirror " << i + 1 << "\n";

         f1->SetParameter(0, 2.0e5);                 // N0
         f1->SetParameter(1, TMath::Poisson(0, 0.01)); // P0
         f1->SetParLimits(1, 0.0, 1.0);
         f1->SetParameter(2, 2.0);                 // A
         f1->SetParLimits(2, 0.0, 100.0);
         f1->SetParameter(3, 0.5);                  // pe
         f1->SetParLimits(3, 0.0, 1.0);
         f1->SetParameter(4, aMirrorHist->GetBinCenter(aMirrorHist->GetMaximumBin()));                  // xp
         f1->SetParLimits(4, -20.0, 30.0);
         f1->SetParameter(5, 2.5);                  // sigp
         f1->SetParLimits(5, 0.0, 7.0);
         f1->SetParameter(6, 100.0);                 // x0
         f1->SetParLimits(6, 70.0, 150.0);                // x0
         f1->SetParameter(7, 35.0);                 // sig0
         f1->SetParLimits(7, 10.0, 100.0);                 // sig0
         f1->SetParameter(8, 0.02);                  // mu
         f1->SetParLimits(8, 0.0, 5.0);                 // mu
//    f1->SetParameter(10, 90.0);                   // x1
//    f1->SetParLimits(10,70.0,150.0);                   // x1
//    f1->SetParameter(11,30.0);                   // sig1
//    f1->SetParLimits(11,10.0,100.0);                   // sig1

         Double_t P0 = aMirrorHist->Integral(0, 24) / ((double)(aMirrorHist->GetEntries()));
         f1->FixParameter(1, P0);  // P0
//          f1->SetParameter(1,P0 );  // P0

         Double_t lowerlimit = aMirrorHist->GetBinCenter(aMirrorHist->GetMaximumBin()) - 8.0;
         //f1->SetParameter(1,aMirrorHist->GetMean(1));
         //aMirrorHist->Fit(f1,"M,E");
         std::cout << "Limit = " << lowerlimit << "\n";
         fitResult = aMirrorHist->Fit(f1, "V,E,M,S", "", lowerlimit, 275);
         double * fitparameters = f1->GetParameters();
         fptr->SetParameters2(f1->GetParameters());

         for (int jj = 0; jj < 9; jj++) {
            cer_pars[i][jj] = fitparameters[jj];
         }

         //aMirrorHist->SetTitle(Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->SetTitle("channels");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->CenterTitle(1);
         aMirrorHist->Draw();

         f1->DrawCopy("same");
         Int_t linestyle = 2;
         /*   TF1 * fpbg = fptr->GetExpFunction();*/
         TF1 *fpexp = new TF1("pmt-exp", fptr, &InSANEPMTResponse::Ser_exp, -20, 2000, 7, "InSANEPMTResponse", "Ser_exp");
         fpexp->SetParameter(0, f1->GetParameters()[2]);
         fpexp->SetParameter(1, f1->GetParameters()[3]);
         fpexp->SetParameter(2, f1->GetParameters()[4]); //fxp);
         fpexp->SetParameter(3, f1->GetParameters()[5]); //fsigp);
         fpexp->SetParameter(4, f1->GetParameters()[6]); //fx0);
         fpexp->SetParameter(5, f1->GetParameters()[7]); //fsig0);
         fpexp->SetParameter(6, TMath::Poisson(1.0, f1->GetParameters()[8])*f1->GetParameters()[0]); //N0);
         fpexp->SetLineColor(3);
         fpexp->SetLineStyle(linestyle);
         fpexp->SetLineWidth(2);

         TF1 * fp0 = fptr->GetPeakFunction(0);
         fp0->SetLineColor(4);
         fp0->SetLineStyle(linestyle);
         fp0->SetLineWidth(2);

         TF1 * fp1 = fptr->GetPeakFunction(1);
         fp1->SetLineColor(4);
         fp1->SetLineStyle(linestyle);
         fp1->SetLineWidth(2);
         //fp1->Print();
         /*
            TF1 * fp2 = fptr->GetPeakFunction(2);
            fp2->SetLineColor(4);
            fp2->SetLineStyle(3);
         //    fp2->Print();

            TF1 * fp3 = fptr->GetPeakFunction(3);
            fp3->SetLineColor(4);
            fp3->SetLineStyle(3);
         //    fp3->Print();*/

         fpexp->DrawCopy("same");
         fp0->DrawCopy("same");
         fp1->DrawCopy("same");

      }
   }

   c->SaveAs(Form("plots/%d/OnePE_calibration_run9.pdf", runNumber));
   c->SaveAs(Form("plots/%d/OnePE_calibration_run9.png", runNumber));
   c->SaveAs(Form("plots/%d/OnePE_calibration_run9.svg", runNumber));


   for (int i = 0; i < 8; i++) {
      std::cout << "Double_t cerp" << i << "[] = {";
      for (int j = 0; j < 12; j++) {
         if (j != 0) std::cout << ",";
         std::cout << std::setw(10) << cer_pars[i][j];
      }
      std::cout << "}; cer_pars[" << i << "] = cerp" << i << ";\n";
   }


   return(0);
}
