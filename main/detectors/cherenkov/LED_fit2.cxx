/*!  Analyzes a SANE Cherenkov LED Run.

 - 71805
 - 71800 through 71807 - late Dec 2008
 - 71833
 - 71835  - not 1PE setting
 - 72775
 - 71650

 */
Int_t LED_fit2(Int_t runNumber = 71835, Int_t isACalibration = 0)
{
   gStyle->SetLabelSize(0.08, "XYZ");
   gStyle->SetTitleSize(0.08, "XYZ");
   //   gStyle->SetTitleOffset(
   gStyle->SetPadTopMargin(0.01);
   gStyle->SetPadBottomMargin(0.18);
   gStyle->SetPadLeftMargin(0.09);
   gStyle->SetPadRightMargin(0.09);


   /// output from script: One_PE_calibration7.cxx
   Double_t * cer_pars[8];
// Double_t cerp0[] = {347251,0.999894,154.292,0.876354,-3.81256,2.87862,101.094,44.5074,0.0402355,0,0,0}; cer_pars[0] = cerp0;
// Double_t cerp1[] = {363930,0.950172,67.1132,0.592257,0.651103,2.40246,98.9189,27.3955,0.0127819,0,0,0}; cer_pars[1] = cerp1;
// Double_t cerp2[] = {343656,0.999974,90.594,0.521196,0.642951,2.33078,102.743,33.6818,0.0187019,0,0,0}; cer_pars[2] = cerp2;
// Double_t cerp3[] = {344041,1,61.6979,0.559765,0.128887,2.2509,109.303,31.0684,0.0284384,0,0,0}; cer_pars[3] = cerp3;
// Double_t cerp4[] = {347554,0.999363,19.9996,0.32841,0.0153856,2.22314,98.5503,31.5075,0.0145697,0,0,0}; cer_pars[4] = cerp4;
// Double_t cerp5[] = {342580,0.999904,67.8167,0.43639,1.44026,2.3545,103.147,31.1602,0.0246862,0,0,0}; cer_pars[5] = cerp5;
// Double_t cerp6[] = {346134,0.999897,95.6649,0.768915,-1.10746,2.32434,104.807,33.7352,0.0191336,0,0,0}; cer_pars[6] = cerp6;
// Double_t cerp7[] = {346028,0.999455,62.3922,0.538858,-0.00743529,2.22495,95.4106,29.6958,0.0189016,0,0,0}; cer_pars[7] = cerp7;
//

   Double_t cerp0[] = {    345414,  0.992751,       100,  0.724137,      -3.5,   2.80186,   100.602,   50.7525, 0.0214257,         0,         0,         0};
   cer_pars[0] = cerp0;
   Double_t cerp1[] = {    265154,  0.990478,   3.08999,  0.972267,      -0.5,   2.10676,   92.6853,   35.9216,   1.00019,         0,         0,         0};
   cer_pars[1] = cerp1;
   Double_t cerp2[] = {    347070,  0.986728,   5.25786,  0.444108,  0.626717,   2.31703,   95.3423,   44.1634, 0.0236339,         0,         0,         0};
   cer_pars[2] = cerp2;
   Double_t cerp3[] = {    346709,  0.981971,    3.6038,  0.736722, 0.0864471,   2.21128,   96.0987,   42.5232,    4.1437,         0,         0,         0};
   cer_pars[3] = cerp3;
   Double_t cerp4[] = {    305472,  0.989261,   2.61894,  0.969255, -0.500062,   2.19784,   96.0935,   33.9467,  0.999997,         0,         0,         0};
   cer_pars[4] = cerp4;
   Double_t cerp5[] = {    346207,  0.980603,    4.7201,  0.438835,   1.40809,   2.33699,    94.525,   39.8982,         5,         0,         0,         0};
   cer_pars[5] = cerp5;
   Double_t cerp6[] = {    333909,  0.991896,    2.2144,  0.978101,  -1.17028,   2.34158,   90.3457,   51.5279,  0.999992,         0,         0,         0};
   cer_pars[6] = cerp6;
   Double_t cerp7[] = {    343518,  0.987542,   2.78048,  0.908652, -0.0796861,   2.16014,   87.2007,   36.8518,   3.19993,         0,         0,         0};
   cer_pars[7] = cerp7;




   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if (!(runManager->IsRunSet())) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();

   TTree * t = (TTree*)gROOT->FindObject("betaDetectors0");
   if (!t) {
      printf("betaDetectors0 tree not found\n");
      return(-1);
   }

   TFile * f = new TFile(Form("data/rootfiles/cherenkovLED%d.root", runNumber), "UPDATE");

   TCanvas * c = new TCanvas("LED_fit2", "LED_fit2", 800, 200 * 2 * 1.62);
   c->Divide(2, 4);

   TH1F * aMirrorHist;
   TFitResultPtr fitResult;

   //TF1 *f1 = new TF1("f1", "gaus", 0,6000);
   //TF1 *f1 = new TF1("Spmt",S_pmt,0,1000,5);
   InSANEPMTResponse2 * fptr = new InSANEPMTResponse2();  // create the user function class
   TF1 * f1 = new TF1("f1", fptr, &InSANEPMTResponse2::PMT_fit, -10, 2000, 9, "InSANEPMTResponse2", "PMT_fit");


   for (int i = 0; i < 8; i++) {
      c->cd(8 - i);
      /*      gPad->SetLogy(1);*/
      t->Draw(Form("fGasCherenkovEvent.fGasCherenkovHits.fADC>>mirror%dhist(200,-25,1375)", i + 1), Form(
                 "fGasCherenkovEvent.fGasCherenkovHits.fLevel==0&&fGasCherenkovEvent.fGasCherenkovHits.fMirrorNumber==%d", i + 1),
              "",
              1000000
              1001);
      aMirrorHist = (TH1F *) gROOT->FindObject(Form("mirror%dhist", i + 1));
      if (aMirrorHist) {
         std::cout << " Fitting Mirror " << i + 1 << "\n";

         f1->SetParameter(0, 2.0e5);                 // N0
         f1->SetParameter(1, 0.001);  // P0
         f1->SetParLimits(1, 0.0, 1.0);
         f1->SetParameter(2, cer_pars[i][2]);                 // A
         //f1->SetParLimits(3,0.0,1000.0);
         f1->SetParameter(3, 0.1);                  // pe
         f1->SetParLimits(3, 0.0, 1.0);
         f1->FixParameter(4, cer_pars[i][4]);                  // xp
         //f1->SetParLimits(5,-20.0,30.0);
         f1->FixParameter(5, cer_pars[i][5]);                  // sigp
         f1->FixParameter(6, cer_pars[i][6]);                 // x0
         //f1->SetParLimits(7,30.0,150.0);                  // x0
         f1->SetParameter(7, cer_pars[i][7]);                 // sig0
         f1->SetParLimits(7, cer_pars[i][7] * 0.5, cer_pars[i][7] * 1.3);             // sig0
         f1->SetParameter(8, 5.0);                  // mu
         f1->SetParLimits(8, 0.0, 15.0);                 // mu
//    f1->SetParameter(10, 100.0);                   // x1
//    f1->SetParLimits(10,30.0,150.0);                   // x1
//    f1->SetParameter(11,30.0);                   // sig1
//    f1->SetParLimits(11,10.0,100.0);                   // sig1

         f1->SetParName(0, "N0");
         f1->SetParName(1, "P0");
         /*   f1->SetParName(2,"P1");*/
         f1->SetParName(2, "A");
         f1->SetParName(3, "pe");
         f1->SetParName(4, "xp");
         f1->SetParName(5, "sigp");
         f1->SetParName(6, "x0");
         f1->SetParName(7, "sig0");
         f1->SetParName(8, "mu");
//    f1->SetParName(10,"x1");
//    f1->SetParName(11,"sig1");
         //f1->SetParameter(1,aMirrorHist->GetMean(1));
         //aMirrorHist->Fit(f1,"M,E");
         fitResult = aMirrorHist->Fit(f1, "E,M,S", "", 10, 1375);

         fptr->SetParameters(f1->GetParameters());

         //aMirrorHist->SetTitle(Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->SetTitle("");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->SetTitle("channels");//Form("Cherenkov Mirror %d",i+1));
         aMirrorHist->GetXaxis()->CenterTitle(1);
         aMirrorHist->SetMaximum(aMirrorHist->GetMaximum());
         aMirrorHist->Draw();

         f1->DrawClone("same");

         Int_t linestyle = 2;

         TF1 * fpexp = fptr->GetExpFunction();
         fpexp->SetLineColor(3);
         /*   fpexp->SetLineStyle(linestyle);*/
         fpexp->SetLineWidth(2);
         fpexp->DrawClone("same");


         TF1 * fp0 = fptr->GetPeakFunction(0);
         fp0->SetLineColor(4);
         fp0->SetLineStyle(linestyle);
         fp0->SetLineWidth(2);
         fp0->DrawClone("same");

//    TF1 * fpexp = fptr->GetExpFunction();
//    fpexp->SetLineColor(3);
//    //fpexp->Print();

         for (int np = 1; np < 15; np++) {
            TF1 * fp1 = fptr->GetPeakFunction(np);
            fp1->SetLineWidth(1);
            if (np == 1)fp1->SetLineColor(4);
            else fp1->SetLineColor(2);
            fp1->SetLineStyle(2);
            fp1->DrawClone("same");
         }
         //fp1->Print();
      }
   }

   c->SaveAs(Form("plots/%d/LED_fit2.pdf", runNumber));
   c->SaveAs(Form("plots/%d/LED_fit2.png", runNumber));
   c->SaveAs(Form("plots/%d/LED_fit2.svg", runNumber));

//     gROOT->SetBatch(kFALSE);


// f->Write();


   return(0);
}
