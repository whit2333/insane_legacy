Int_t deadtime(Int_t runNumber = 73001) {

   rman->SetRun(runNumber);

   rman->GetScalerFile()->cd();
   const char * treeName0 = "Scalers1";
   TTree * t0 = (TTree*)gROOT->FindObject(treeName0);
   if(!t0){ std::cout << " TREE NOT FOUND : " << treeName0 << "\n"; return(-1);}

   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   c->cd(1);
   t0->Draw("fBETA2Counter.fLiveTime:fTime","","colz" );
   c->cd(2);
   t0->Draw("fBETA2Counter.fPositiveHelicityLiveTime:fTime","","colz" );
   c->cd(3);
   t0->Draw("fBETA2Counter.fLiveTime:fEventNumber","","colz" );
/*   t0->Draw("fBETA2Counter.fPositiveHelicityLiveTime:fTime","","colz" );*/
   c->cd(4);
   t0->Draw("fEventNumber:fTime","","colz" );


   return(0);
}