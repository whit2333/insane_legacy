/*!  Track reconstruction with BigCal


   TODO:
   - Do the y cut first then look at the X distribution ( curretnly done the other way around)
   - Look at Pi0 events w.r.t. reconstructed tracks (no shower)
   - 

 */
Int_t tracking1(Int_t runNumber=72994) {

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->fCurrentFile->cd();

   const char * treeName = "trackingPositions";
   TTree * t = (TTree*)gROOT->FindObject(treeName);
   if(!t){ std::cout << " TREE NOT FOUND : " << treeName << "\n"; return(-1);}

   InSANEReconstructedEvent * aRecon = new InSANEReconstructedEvent();
   BIGCALCluster * aCluster= 0;
   t->SetBranchAddress("uncorrelatedPositions",&aRecon);
   SANEEvents * ev = new SANEEvents("betaDetectors1");

   t->AddFriend(ev->fTree);

//    TDirectory * cerPerformanceDir=0;
//    TFile * cerFile = new TFile("data/tracking1.root","UPDATE");
//    if( cerFile ) {
//       if( !(cerFile->cd(Form("noshower%d",runNumber)) ) ) {
//          cerPerformanceDir = cerFile->mkdir(Form("noshower%d",runNumber)) ;
//          if( !(cerPerformanceDir->cd()) ) printf(" dir Error\n");
//       } else {
//          cerPerformanceDir = gDirectory;
//       }
//    }

   Double_t trackerLuciteYSlope = 5.0;

   /// HISTOGRAMS
   TH1F * hCerNPESingle[8];
   TH1F * hCerNPESum[8];
   TH1F * hCerSignalSum[8];
   TH1F * hCerSignalSingle[8];
   TEfficiency * hCerSignalEfficiencyVsEnergy[8];
   TEfficiency * hCerSignalEfficiencyVsY[8];
   TEfficiency * hCerSignalEfficiencyVsX[8];

//    for(int i = 0; i<8;i++) {
//       hCerNPESum[i] = new TH1F(Form("Cer-%d-NPE Sum",i+1),Form("Cer -%d- NPE Sum",i+1),200,1,45);
//       hCerNPESingle[i] = new TH1F(Form("Cer-%d-NPE",i+1),Form("Cer -%d- NPE",i+1),200,1,45);      hCerSignalSum[i] = new TH1F(Form("Cer-%d-Sum",i+1),Form("Cer -%d- Sum",i+1),200,0.02,4);
//       hCerSignalSingle[i] = new TH1F(Form("Cer-%d-",i+1),Form("Cer -%d-",i+1),200,0.02,4);
// 
//       hCerSignalEfficiencyVsEnergy[i] = new TEfficiency(Form("Cer-%d-EffVsEnergy",i+1),Form("Cer -%d- EffVsEnergy",i+1),50,500,4500);
//       hCerSignalEfficiencyVsY[i] = new TEfficiency(Form("Cer-%d-EffVsY",i+1),Form("Cer -%d- EffVsY",i+1),120,-120,120);
//       hCerSignalEfficiencyVsX[i] = new TEfficiency(Form("Cer-%d-EffVsX",i+1),Form("Cer -%d- EffVsX",i+1),120,-120,120);
//    }
//    TEfficiency * hAllCerEfficiencyVsEnergy = new TEfficiency("hAllCerEfficiencyVsEnergy","All Cer EffVsEnergy",50,500,4500);
//    TEfficiency * hAllCerEfficiencyVsY = new TEfficiency("hAllCerEfficiencyVsY","All Cer EffVsY",120,-120,120);
//    TEfficiency * hAllCerEfficiencyVsX = new TEfficiency("hAllCerEfficiencyVsX","All Cer EffVsX",120,-60,60);
// 
//    TEfficiency * hAllCerEfficiencyVsEnergy2 = new TEfficiency("hAllCerEfficiencyVsEnergy2","All Cer EffVsEnergy E>1000",50,500,4500);
//    TEfficiency * hAllCerEfficiencyVsY2 = new TEfficiency("hAllCerEfficiencyVsY2","All Cer EffVsY E>1000",120,-120,120);
//    TEfficiency * hAllCerEfficiencyVsX2 = new TEfficiency("hAllCerEfficiencyVsX2","All Cer EffVsX E>1000",120,-60,60);


   /// Canvas and simple draws
   TCanvas * canvas = new TCanvas("Tracking1","Tracking1 No cherenkov ADC");
   canvas->Divide(2,2);

   /// Look at the Reconstructed Positions
   canvas->cd(1);
   t->Draw("fTrackerPositions.fPosition.fX:fLucitePositions.fPosition.fX",
      "triggerEvent.IsBETAEvent()&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   /// Look at the Reconstructed Positions
   canvas->cd(2);
   t->Draw("fTrackerPositions.fPosition.fY:fLucitePositions.fPosition.fY",
      "triggerEvent.IsBETAEvent()&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");

   /// Look at the Reconstructed Positions
   canvas->cd(3);
   t->Draw("fTrackerPositions.fPosition.fX:fLucitePositions.fPosition.fX",
      "triggerEvent.IsBETAEvent()&&TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&fNLucitePositions>0&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");
   canvas->cd(4);
   t->Draw("fTrackerPositions.fPosition.fY:fLucitePositions.fPosition.fY",
      "triggerEvent.IsBETAEvent()&&TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&fNLucitePositions>0&&fNTrackerPositions>0",
      "col");


   /// With Cherenkov ADC

   TCanvas * c2 = new TCanvas("Tracking2","Tracking2 Cherenkov ADC");
   c2->Divide(2,2);

   /// Look at the Reconstructed Positions
   c2->cd(1);
   t->Draw("fTrackerPositions.fPosition.fX:fLucitePositions.fPosition.fX",
      "triggerEvent.IsBETAEvent()&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8",
      "col");

  /// 
   c2->cd(2);
   t->Draw("fTrackerPositions.fPosition.fY:fLucitePositions.fPosition.fY",
      "triggerEvent.IsBETAEvent()&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8",
      "col");
/*TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<8.0&&&&fNTrackerPositions>0&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2*/
   /// 
   c2->cd(3);
   t->Draw("fTrackerPositions.fPosition.fX:fLucitePositions.fPosition.fX",
      "triggerEvent.IsBETAEvent()&&TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8",
      "col");
   c2->cd(4);
   t->Draw("fTrackerPositions.fPosition.fY:fLucitePositions.fPosition.fY",
      "triggerEvent.IsBETAEvent()&&TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8",
      "col");


   /// 

   TCanvas * c3 = new TCanvas("Tracking3","Tracking3 With mono-energetic");
   c3->Divide(2,2);

   /// Look at the Reconstructed Positions
   c3->cd(1);
   t->Draw("fTrackerPositions.fPosition.fX:fLucitePositions.fPosition.fX",
      "triggerEvent.IsBETAEvent()&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "col");

   /// 
   c3->cd(2);
   t->Draw("fTrackerPositions.fPosition.fY:fLucitePositions.fPosition.fY",
      "triggerEvent.IsBETAEvent()&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "col");
/*TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<8.0&&&&fNTrackerPositions>0&&TMath::Abs(fLucitePositions.fPosition.fX*0.225-0.5-fTrackerPositions.fPosition.fX)<2*/
   /// 
   c3->cd(3);
   t->Draw("fTrackerPositions.fPosition.fX:fLucitePositions.fPosition.fX",
      "triggerEvent.IsBETAEvent()&&TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "col");
   c3->cd(4);
   t->Draw("fTrackerPositions.fPosition.fY:fLucitePositions.fPosition.fY",
      "triggerEvent.IsBETAEvent()&&TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "col");



   /// 

   TCanvas * c4 = new TCanvas("Tracking4","Tracking4 With mono-energetic");
   c4->Divide(2,2);

   /// Look at the Reconstructed Positions
   c4->cd(1);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYCerADC",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8",
      "goff");
   TH1F * hBigcalYCerADC = (TH1F*)gROOT->FindObject("hBigcalYCerADC");
   hBigcalYCerADC->SetTitle("Bigcal Y with ADC Window");
   hBigcalYCerADC->Draw();

   /// 
   c4->cd(2);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "goff");
   TH1F * hBigcalYMonoenergetic = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic");
   hBigcalYMonoenergetic->SetTitle("Bigcal Y with 800 < E < 1200");
   hBigcalYMonoenergetic->Draw();

   /// 
   c4->cd(3);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergeticCerADC",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "goff");
   TH1F * hBigcalYMonoenergeticCerADC = (TH1F*)gROOT->FindObject("hBigcalYMonoenergeticCerADC");
   hBigcalYMonoenergeticCerADC->SetTitle("800 < E < 1200 And Cer Window");
   hBigcalYMonoenergeticCerADC->Draw();

   /// 
   c4->cd(4);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergeticCerADCTracking",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "goff");
   TH1F * hBigcalYMonoenergeticCerADCTracking = (TH1F*)gROOT->FindObject("hBigcalYMonoenergeticCerADCTracking");
   hBigcalYMonoenergeticCerADCTracking->SetTitle("800 < E < 1200, Cer Window, Tracking Y correlation");
   hBigcalYMonoenergeticCerADCTracking->Draw();




   /// 

   TCanvas * c5 = new TCanvas("Tracking5","Tracking5 With mono-energetic");
   c5->Divide(2,3);

   /// Look at the Reconstructed Positions
   c5->cd(1);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYCerADC",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8",
      "goff");
   TH1F * hBigcalYCerADC = (TH1F*)gROOT->FindObject("hBigcalYCerADC");
   hBigcalYCerADC->SetTitle("Bigcal Y with ADC Window");
   hBigcalYCerADC->Draw();

   /// 
   c5->cd(2);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "goff");
   TH1F * hBigcalYMonoenergetic = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic");
   hBigcalYMonoenergetic->SetTitle("Bigcal Y with 800 < E < 1200");
   hBigcalYMonoenergetic->Draw();

   /// 
   c5->cd(3);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergeticCerADC",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "goff");
   TH1F * hBigcalYMonoenergeticCerADC = (TH1F*)gROOT->FindObject("hBigcalYMonoenergeticCerADC");
   hBigcalYMonoenergeticCerADC->SetTitle("800 < E < 1200 And Cer Window");
   hBigcalYMonoenergeticCerADC->Draw();

   /// 
   c5->cd(4);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergeticCerADCTracking",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&TMath::Abs(fLucitePositions.fPosition.fY/5.0-fTrackerPositions.fPosition.fY)<4.0&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200",
      "goff");
   TH1F * hBigcalYMonoenergeticCerADCTracking = (TH1F*)gROOT->FindObject("hBigcalYMonoenergeticCerADCTracking");
   hBigcalYMonoenergeticCerADCTracking->SetTitle("800 < E < 1200, Cer Window, Tracking Y correlation");
   hBigcalYMonoenergeticCerADCTracking->Draw();

   /// 
   c5->cd(5);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergeticCerADCTDC",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<200&&TMath::Abs(bigcalClusters.fCherenkovTDC)<20",
      "goff");
   TH1F * hBigcalYMonoenergeticCerADCTDC = (TH1F*)gROOT->FindObject("hBigcalYMonoenergeticCerADCTDC");
   hBigcalYMonoenergeticCerADCTDC->SetTitle("800 < E < 1200 And Cer Window and TDC");
   hBigcalYMonoenergeticCerADCTDC->Draw();
//    t->StartViewer();



   /// 
   c5->cd(6);
   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic2CerADC",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-700)<200",
      "goff");
   TH1F * hBigcalYMonoenergetic2CerADC = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic2CerADC");
   hBigcalYMonoenergetic2CerADC->SetTitle("500 < E < 900 And Cer Window ");
   hBigcalYMonoenergetic2CerADC->Draw();





   TCanvas * c6 = new TCanvas("Tracking6","Tracking6 With different energies");
   c6->Divide(1,2);

   THStack * hStack = new THStack("energyDep","BigCal  Y position for increasing energies");
   c6->cd(1);

   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic2CerADC1",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-600)<100",
      "goff");
   TH1F * hBigcalYMonoenergetic2CerADC1 = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic2CerADC1");
   hBigcalYMonoenergetic2CerADC1->SetTitle("500 < E < 700 And Cer Window ");
   hBigcalYMonoenergetic2CerADC1->SetLineColor(1);
/*   hStack->Add(hBigcalYMonoenergetic2CerADC1);*/
   hBigcalYMonoenergetic2CerADC1->Draw();

   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic2CerADC2",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-800)<100",
      "goff");
   TH1F * hBigcalYMonoenergetic2CerADC2 = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic2CerADC2");
   hBigcalYMonoenergetic2CerADC2->SetTitle("700 < E < 900 And Cer Window ");
   hBigcalYMonoenergetic2CerADC2->SetLineColor(2);
   hStack->Add(hBigcalYMonoenergetic2CerADC2);
   hBigcalYMonoenergetic2CerADC2->Draw("same");

   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic2CerADC3",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1000)<100",
      "goff");
   TH1F * hBigcalYMonoenergetic2CerADC3 = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic2CerADC3");
   hBigcalYMonoenergetic2CerADC3->SetTitle("900 < E < 1100 And Cer Window ");
   hBigcalYMonoenergetic2CerADC3->SetLineColor(38);
   hStack->Add(hBigcalYMonoenergetic2CerADC3);
   hBigcalYMonoenergetic2CerADC3->Draw("same");

   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic2CerADC4",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1200)<100",
      "goff");
   TH1F * hBigcalYMonoenergetic2CerADC4 = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic2CerADC4");
   hBigcalYMonoenergetic2CerADC4->SetTitle("1100 < E < 1300 And Cer Window ");
   hBigcalYMonoenergetic2CerADC4->SetLineColor(40);
   hStack->Add(hBigcalYMonoenergetic2CerADC4);
   hBigcalYMonoenergetic2CerADC4->Draw("same");

   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic2CerADC5",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1400)<100",
      "goff");
   TH1F * hBigcalYMonoenergetic2CerADC5 = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic2CerADC5");
   hBigcalYMonoenergetic2CerADC5->SetTitle("1300 < E < 1500 And Cer Window ");
   hBigcalYMonoenergetic2CerADC5->SetLineColor(41);
   hStack->Add(hBigcalYMonoenergetic2CerADC5);
//   hBigcalYMonoenergetic2CerADC5->Draw("same");

   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic2CerADC6",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1600)<100",
      "goff");
   TH1F * hBigcalYMonoenergetic2CerADC6 = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic2CerADC6");
   hBigcalYMonoenergetic2CerADC6->SetTitle("1500 < E < 1700 And Cer Window ");
   hBigcalYMonoenergetic2CerADC6->SetLineColor(42);
   hStack->Add(hBigcalYMonoenergetic2CerADC6);
//   hBigcalYMonoenergetic2CerADC6->Draw("same");

   t->Draw("bigcalClusters.fYMoment>>hBigcalYMonoenergetic2CerADC7",
      "triggerEvent.IsBETAEvent()&&bigcalClusters.fIsGood&&fNLucitePositions>0&&fNTrackerPositions>0&&bigcalClusters.fCherenkovBestADCSum>0.3&&bigcalClusters.fCherenkovBestADCSum<1.8&&TMath::Abs(bigcalClusters.fTotalE-1800)<100",
      "goff");
   TH1F * hBigcalYMonoenergetic2CerADC7 = (TH1F*)gROOT->FindObject("hBigcalYMonoenergetic2CerADC7");
   hBigcalYMonoenergetic2CerADC7->SetTitle("1700 < E < 1900 And Cer Window ");
   hBigcalYMonoenergetic2CerADC7->SetLineColor(43);
   hStack->Add(hBigcalYMonoenergetic2CerADC7);
//   hBigcalYMonoenergetic2CerADC6->Draw("same");




   c6->cd(2);

   hStack->Draw();


   InSANETrajectory * aTrajectory                 = 0;
   LuciteHodoscopePositionHit * aLucPositionHit   = 0;
   LuciteHodoscopePositionHit * bLucPositionHit   = 0;
   BIGCALCluster * aCluster                       = 0;
   BIGCALCluster * bCluster                       = 0;
   ForwardTrackerPositionHit * aTrackerPosHit     = 0;
   ForwardTrackerPositionHit * bTrackerPosHit     = 0;
   InSANEHitPosition * aPos                       = 0;
   InSANEHitPosition * bPos                       = 0;

   TClonesArray * lucPos         = aRecon->fLucitePositions;
   TClonesArray * trackerPos     = aRecon->fTrackerPositions;
   TClonesArray * lucHitPos      = aRecon->fLuciteHitPos;
   TClonesArray * trackerHitPos  = aRecon->fTrackerHitPos;

   /// Event Loop
/*   Int_t Nentries = t->GetEntries();*/
/*
   for(Int_t IEVENT=0;IEVENT < Nentries;IEVENT++) {

      t->GetEntry(IEVENT);
      ev->fTree->GetEntryWithIndex(aRecon->fRunNumber,aRecon->fEventNumber);

      /// Require lucite
      if( aRecon->fNLucitePositions > 0 ){

         ///
         

         /// Require only one cluster
//          if( aRecon->fNBigcalClusters == 1 ) {
//             aCluster = (BIGCALCluster*)(*aRecon->fBigcalClusters)[0];
//            bool cerTDCcut = TMath::Abs(aCluster->fCherenkovTDC)<10;

            hCerNPESum[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestNPESum);
            hCerNPESingle[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestNPEChannel);
            hCerSignalSum[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestADCSum);
            hCerSignalSingle[aCluster->fPrimaryMirror-1]->Fill(aCluster->fCherenkovBestADCChannel);

            hCerSignalEfficiencyVsEnergy[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
            hCerSignalEfficiencyVsY[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fYMoment);
            hCerSignalEfficiencyVsX[aCluster->fPrimaryMirror-1]->Fill(cerTDCcut,aCluster->fXMoment);

            hAllCerEfficiencyVsEnergy->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
            hAllCerEfficiencyVsY->Fill(cerTDCcut,aCluster->fYMoment);
            hAllCerEfficiencyVsX->Fill(cerTDCcut,aCluster->fXMoment);

            if( (aCluster->fTotalE + aCluster->fDeltaE) > 1000.0) {
               hAllCerEfficiencyVsEnergy2->Fill(cerTDCcut,aCluster->fTotalE+aCluster->fDeltaE);
               hAllCerEfficiencyVsY2->Fill(cerTDCcut,aCluster->fYMoment);
               hAllCerEfficiencyVsX2->Fill(cerTDCcut,aCluster->fXMoment);
            }
         }
      }
   }
*/
    canvas->SaveAs(Form("plots/%d/tracking1_1.png",runNumber));    canvas->SaveAs(Form("plots/%d/tracking1_1.pdf",runNumber));
    c2->SaveAs(Form("plots/%d/tracking1_2.png",runNumber));
    c2->SaveAs(Form("plots/%d/tracking1_2.pdf",runNumber));
    c3->SaveAs(Form("plots/%d/tracking1_3.png",runNumber));
    c3->SaveAs(Form("plots/%d/tracking1_3.pdf",runNumber));
    c4->SaveAs(Form("plots/%d/tracking1_4.png",runNumber));
    c4->SaveAs(Form("plots/%d/tracking1_4.pdf",runNumber));

    c5->SaveAs(Form("plots/%d/tracking1_5.png",runNumber));
    c5->SaveAs(Form("plots/%d/tracking1_5.pdf",runNumber));

    c6->SaveAs(Form("plots/%d/tracking1_6.png",runNumber));
    c6->SaveAs(Form("plots/%d/tracking1_6.pdf",runNumber));


   return(0);
}
