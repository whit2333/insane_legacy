{
   Int_t version=0;
   TFile * f = new TFile("data/CalibTest.root","UPDATE");
   InSANEDetectorCalibration * detCalib = 0;
   InSANECalibration * aCalib = 0;

//   f->GetObject(Form("acal%d%d",version,i),aCalib);
 
   f->GetObject(Form("TestDetectorCalibration%d",version),detCalib);
   detCalib->Print();
   detCalib->fCalibrations->Print();
   for(int i=0;i<detCalib->fCalibrations.GetEntries();i++){
      aCalib = (InSANECalibration*)detCalib->GetCalibration(i);
      aCalib->fParameters.Print();
      aCalib->GetFunction()->Print(); 
      if(i==0) aCalib->GetFunction()->DrawCopy();
      else aCalib->GetFunction()->DrawCopy("same");
   }   


f->GetListOfKeys()->Print();

}
