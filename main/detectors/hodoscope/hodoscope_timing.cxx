/** Hodoscope timing 
 *
 *
 */
Int_t hodoscope_timing(){

   InSANEDatabaseManager * dbManager= InSANEDatabaseManager::GetManager();
   TSQLServer * db = dbManager->GetMySQLConnection();

   TFile * cerFile = new TFile("data/hodoscope_timing.root","UPDATE");

   TCanvas * c = new TCanvas("hodoscopeTiming","Lucite HodoscopePeaks",1200,800);
   TH1F * h1;
   TH1F * h2;
   TH2F * h3;
   THStack * hs = new THStack("luciteTimings","Lucite Timings");
   Double_t max = 0;


   /// Look at individual PMTs of hodoscopes
   TH2F  * lucTDCWidthvsRunAll  = new TH2F("lucTDCWidthvsRunAll","lucTDCWidthvsRunAll",200,72200,73050,100,-100, 600);
   TH2F  * lucTDCPeakvsRunAll  = new TH2F("lucTDCPeakvsRunAll","lucTDCPeakvsRunAll",200,72200,73050,100,-2200, -1400);

   for(int i = 0;i<56;i++) {

      TString SQLCOMMAND(
         Form("SELECT run_number,tdc_mean,tdc_width FROM %s.detector_timing where tdc_number=%d AND detector='hodoscope'", dbManager->GetDatabaseName(),i) ); 
      SQLCOMMAND +=" ;";

      h1 = new TH1F(Form("lucTDC%d",i),Form("Lucite %d TDC",i),100,-2200, -1400);
      h2 = new TH1F(Form("lucTDCWidth%d",i),Form("Lucite %d TDC width",i),50,0, 1000);
      h3 = new TH2F(Form("clucTDCWidthvsRun%d",i),Form("Lucite %d TDC width vs Run",i),200,72200,73050,100,-2000, -1600);
//       h3 = new TH2F(Form("clucTDCWidthvsPeak%d",i),Form("Lucite %d TDC width vs ped",i),100,0,1000,50,0,100);

      if(db) {
         TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
         TSQLRow * row;
         while(row = res->Next()) {
         lucTDCPeakvsRunAll->Fill(atof(row->GetField(0)),atof(row->GetField(1)));
         lucTDCWidthvsRunAll->Fill(atof(row->GetField(0)),atof(row->GetField(2)));
         h1->Fill(atof(row->GetField(1)) );
         h2->Fill(atof(row->GetField(2)) );
         h3->Fill(atof(row->GetField(0)),atof(row->GetField(1)));

         }
      }
      h1->SetLineColor(i);
      h2->SetLineColor(1);
      hs->Add(h1);
   }

   /// Look at the Peak in the TDC Sums
   TH2F  * lucTDCSumWidthvsRunAll  = new TH2F("lucTDCSumWidthvsRunAll","lucTDCSumWidthvsRunAll",200,72200,73050,100,-100, 600);

   TH2F  * lucTDCSumPeakvsRunAll  = new TH2F("lucTDCSumPeakvsRunAll","lucTDCSumPeakvsRunAll",200,72200,73050,100,-4400,-2800);

   for(int i = 0;i<28;i++) {
//    c->cd(i);
      TString SQLCOMMAND(
         Form("SELECT run_number,tdc_mean,tdc_width FROM %s.detector_timing where tdc_number=%d AND detector='hodoscopeSums'", dbManager->GetDatabaseName(),i) ); 
      SQLCOMMAND +=" ;";

      h4 = new TH2F(Form("clucTDCSumPeakvsRun%d",i),Form("Lucite %d TDC Sum Peak vs Run",i),200,72000,73050,100,-4000, -3200);

      if(db) {
         TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
         TSQLRow * row;
         while(row = res->Next()) {
         lucTDCSumWidthvsRunAll->Fill(atof(row->GetField(0)),atof(row->GetField(2)));
         lucTDCSumPeakvsRunAll->Fill(atof(row->GetField(0)),atof(row->GetField(1)));
         h4->Fill(atof(row->GetField(0)),atof(row->GetField(1)));
/*         h2->Fill(atoi(rosw->GetField(0)),atof(row->GetField(1)) );*/
/*         std::cout << " mean " << atof(row->GetsField(1)) << "\n";*/
         }
      }
   }
   /// Look at the Peak in the TDC Diffs
   TH2F  * lucTDCDiffWidthvsRunAll  = new TH2F("lucTDCDiffWidthvsRunAll","lucTDCDiffWidthvsRunAll",200,72200,73050,100,-100, 600);

   TH2F  * lucTDCDiffPeakvsRunAll  = new TH2F("lucTDCDiffPeakvsRunAll","lucTDCDiffPeakvsRunAll",200,72200,73050,100,-500,500);

   for(int i = 0;i<28;i++) {
      TString SQLCOMMAND(
         Form("SELECT run_number,tdc_mean,tdc_width FROM %s.detector_timing where tdc_number=%d AND detector='hodoscopeDiffs'", dbManager->GetDatabaseName(),i) ); 
      SQLCOMMAND +=" ;";
      h5 = new TH2F(Form("lucTDCDiffPeakvsRun%d",i),Form("Lucite %d TDC Diff Peak vs Run",i),200,72000,73050,100,-500, 500);

      if(db) {
         TSQLResult * res = db->Query(SQLCOMMAND.Data()) ;
         TSQLRow * row;
         while(row = res->Next()) {
         lucTDCDiffWidthvsRunAll->Fill(atof(row->GetField(0)),atof(row->GetField(2)));
         lucTDCDiffPeakvsRunAll->Fill(atof(row->GetField(0)),atof(row->GetField(1)));            h5->Fill(atof(row->GetField(0)),atof(row->GetField(1)));
         }
      }
   }


   c->Divide(3,2);
//    c->cd(1);
//    hs->Draw("nostack");
   c->cd(1);
   lucTDCPeakvsRunAll->Draw("colz");
   c->cd(4);
   lucTDCWidthvsRunAll->Draw("colz");

   c->cd(2);
   lucTDCSumPeakvsRunAll->Draw("colz");
   c->cd(5);
   lucTDCSumWidthvsRunAll->Draw("colz");

   c->cd(3);
   lucTDCDiffPeakvsRunAll->Draw("colz");
   c->cd(6);
   lucTDCDiffWidthvsRunAll->Draw("colz");


   
   return(0);

}
