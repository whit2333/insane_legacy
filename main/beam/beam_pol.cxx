/**
 *
 */
Int_t beam_pol(){

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();

   TSQLServer * con = dbman->GetMySQLConnection();
   if(!con) return -1;

   TSQLResult * res = 0;
   TSQLRow    * row = 0;

   TString sql = "SELECT run_number-72000,beam_energy/1000.0,npass,npass*10,beam_pol,(hwplate-0.5)*100.0 FROM beam_pol_per_run WHERE run_number>72000";

   std::vector<double> runs;
   std::vector<double> beam_energies;
   std::vector<double> pass_numbers;
   std::vector<double> pass10;
   std::vector<double> beam_pols;
   std::vector<double> hwplate;
   res = con->Query(sql);
   if(res){
      while(row = res->Next()){
         runs.push_back(atof(row->GetField(0)));
         beam_energies.push_back(atof(row->GetField(1)));
         pass_numbers.push_back(atof(row->GetField(2)));
         pass10.push_back(atof(row->GetField(3)));
         beam_pols.push_back(atof(row->GetField(4)));
         hwplate.push_back(atof(row->GetField(5)));
         delete row;
         row = 0;
      }
   }
   delete res;


   TGraph * gr = 0;
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TCanvas * c = new TCanvas();
   c->Divide(1,2);

   c->cd(1);
   gr = new TGraph(runs.size(),&runs[0],&beam_energies[0]);
   mg->Add(gr,"p");

   gr = new TGraph(runs.size(),&runs[0],&pass_numbers[0]);
   gr->SetMarkerColor(2);
   mg->Add(gr,"p");

   mg->Draw("a");
   
   c->cd(2);

   gr = new TGraph(runs.size(),&runs[0],&beam_pols[0]);
   mg2->Add(gr,"p");

   gr = new TGraph(runs.size(),&runs[0],&pass10[0]);
   gr->SetMarkerColor(2);
   mg2->Add(gr,"p");

   gr = new TGraph(runs.size(),&runs[0],&hwplate[0]);
   gr->SetMarkerColor(4);
   mg2->Add(gr,"p");

   mg2->Draw("a");

   c->SaveAs("results/detectors/beam_pol.png");

   return 0;
}
