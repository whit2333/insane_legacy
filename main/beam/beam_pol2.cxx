/**
 *
 */
Int_t beam_pol2(){

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();

   TSQLServer * con = dbman->GetMySQLConnection();
   if(!con) return -1;

   TSQLResult * res = 0;
   TSQLRow    * row = 0;

   TString sql = "SELECT run_number-72000,beam_energy/1000.0,beam_pass*1.0,beam_pass*10.0,beam_pol,(halfwave_plate-0.5)*100.0,wien_angle FROM run_info WHERE run_number>72000 AND beam_energy IS NOT NULL ORDER BY run_number";

   std::vector<double> runs;
   std::vector<double> beam_energies;
   std::vector<double> pass_numbers;
   std::vector<double> pass10;
   std::vector<double> beam_pols;
   std::vector<double> hwplate;
   std::vector<double> wien;
   res = con->Query(sql);
   if(res){
      while(row = res->Next()){
         //row->Print();
         runs.push_back(atof(row->GetField(0)));
         beam_energies.push_back(atof(row->GetField(1)));
         pass_numbers.push_back(atof(row->GetField(2)));
         pass10.push_back(atof(row->GetField(3)));
         beam_pols.push_back(atof(row->GetField(4)));
         hwplate.push_back(atof(row->GetField(5)));
         wien.push_back(atof(row->GetField(6)));
         delete row;
         row = 0;
      }
   }
   delete res;

   //-----
   // positive polarizations
   sql = "SELECT run_number-72000,beam_energy/1000.0,beam_pass*1.0,beam_pass*10.0,beam_pol,(halfwave_plate-0.5)*100.0,wien_angle ";
   sql += "FROM run_info WHERE run_number>72000 AND beam_pol>40.0 AND beam_energy IS NOT NULL ORDER BY run_number";

   std::vector<double> runs2;
   std::vector<double> beam_energies2;
   std::vector<double> pass_numbers2;
   std::vector<double> pass102;
   std::vector<double> beam_pols2;
   std::vector<double> hwplate2;
   std::vector<double> wien2;
   res = con->Query(sql);
   if(res){
      while(row = res->Next()){
         runs2.push_back(atof(row->GetField(0)));
         beam_energies2.push_back(atof(row->GetField(1)));
         pass_numbers2.push_back(atof(row->GetField(2)));
         pass102.push_back(atof(row->GetField(3)));
         beam_pols2.push_back(atof(row->GetField(4)));
         hwplate2.push_back(atof(row->GetField(5)));
         wien2.push_back(atof(row->GetField(6)));
         delete row;
         row = 0;
      }
   }
   delete res;

   //-----
   // negative polarizations
   sql = "SELECT run_number-72000,beam_energy/1000.0,beam_pass*1.0,beam_pass*10.0,beam_pol,(halfwave_plate-0.5)*100.0,wien_angle ";
   sql += "FROM run_info WHERE run_number>72000 AND beam_pol<-40.0 AND beam_energy IS NOT NULL ORDER BY run_number";

   std::vector<double> runs3;
   std::vector<double> beam_energies3;
   std::vector<double> pass_numbers3;
   std::vector<double> pass103;
   std::vector<double> beam_pols3;
   std::vector<double> hwplate3;
   std::vector<double> wien3;
   res = con->Query(sql);
   if(res){
      while(row = res->Next()){
         runs3.push_back(atof(row->GetField(0)));
         beam_energies3.push_back(atof(row->GetField(1)));
         pass_numbers3.push_back(atof(row->GetField(2)));
         pass103.push_back(atof(row->GetField(3)));
         beam_pols3.push_back(TMath::Abs(atof(row->GetField(4))));
         hwplate3.push_back(atof(row->GetField(5)));
         wien3.push_back(atof(row->GetField(6)));
         delete row;
         row = 0;
      }
   }
   delete res;

   TGraph * gr = 0;
   TMultiGraph * mg = new TMultiGraph();

   TCanvas * c = new TCanvas();
   //c->Divide(1,2);
   //c->cd(1);
   gr = new TGraph(runs.size(),&runs[0],&beam_energies[0]);
   gr->SetMarkerStyle(8);
   gr->SetMarkerSize(0.6);
   gr->SetMarkerColor(1);
   mg->Add(gr,"p");

   //gr = new TGraph(runs2.size(),&runs2[0],&beam_energies2[0]);
   //gr->SetMarkerStyle(8);
   //gr->SetMarkerSize(0.6);
   //gr->SetMarkerColor(2);
   //mg->Add(gr,"p");

   //gr = new TGraph(runs3.size(),&runs3[0],&beam_energies3[0]);
   //gr->SetMarkerStyle(8);
   //gr->SetMarkerSize(0.6);
   //gr->SetMarkerColor(4);
   //mg->Add(gr,"p");

   //gr = new TGraph(runs.size(),&runs[0],&pass_numbers[0]);
   //gr->SetMarkerColor(2);
   //gr->SetMarkerStyle(6);
   //mg->Add(gr,"p");

   mg->Draw("A");
   mg->GetYaxis()->SetTitle("Beam Energy (GeV)");
   mg->GetYaxis()->CenterTitle(true);
   mg->GetXaxis()->SetTitle("Run Number - 72000");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetRangeUser(4.0,6.0);

   c->SaveAs("results/detectors/beam_pol2_c1.png");
   c->SaveAs("results/detectors/beam_pol2_c1.pdf");
   
   // -------------------------------------
   TCanvas * c2 = new TCanvas();
   gPad->SetGrid(1,1);
   TMultiGraph * mg2 = new TMultiGraph();

   //gr = new TGraph(runs.size(),&runs[0],&beam_pols[0]);
   //gr->SetMarkerStyle(6);
   //mg2->Add(gr,"p");

   gr = new TGraph(runs2.size(),&runs2[0],&beam_pols2[0]);
   gr->SetMarkerStyle(8);
   gr->SetMarkerSize(0.6);
   gr->SetMarkerColor(2);
   mg2->Add(gr,"p");

   gr = new TGraph(runs3.size(),&runs3[0],&beam_pols3[0]);
   gr->SetMarkerStyle(8);
   gr->SetMarkerSize(0.6);
   gr->SetMarkerColor(4);
   mg2->Add(gr,"p");

   mg2->Draw("a");
   mg2->GetYaxis()->SetTitle("Polarization (\%)");
   mg2->GetYaxis()->CenterTitle(true);
   mg2->GetXaxis()->SetTitle("Run Number - 72000");
   mg2->GetXaxis()->CenterTitle(true);
   mg2->GetYaxis()->SetRangeUser(0.1,100);

   c2->SaveAs("results/detectors/beam_pol2_c2.png");
   c2->SaveAs("results/detectors/beam_pol2_c2.pdf");

   // -------------------------------------
   TCanvas * c3 = new TCanvas();
   TMultiGraph * mg3 = new TMultiGraph();

   gr = new TGraph(runs.size(),&runs[0],&pass10[0]);
   gr->SetMarkerStyle(6);
   gr->SetMarkerColor(2);
   mg3->Add(gr,"p");

   gr = new TGraph(runs.size(),&runs[0],&hwplate[0]);
   gr->SetMarkerStyle(6);
   gr->SetMarkerColor(4);
   mg3->Add(gr,"p");

   gr = new TGraph(runs.size(),&runs[0],&wien[0]);
   gr->SetMarkerStyle(6);
   gr->SetMarkerColor(kGreen-4);
   mg3->Add(gr,"p");

   mg3->Draw("a");
   c3->SaveAs("results/detectors/beam_pol2_c3.png");

   return 0;
}
