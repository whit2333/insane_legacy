/// Test the form factor classes for the proton 

void FormFactors_p(){

        Double_t Es    = 5.89; 
        Double_t th    = 45.*degree; 
        Double_t SIN   = TMath::Sin(th/2.); 
        Double_t SIN2  = SIN*SIN; 
        Double_t MT    = 3.*M_p/GeV;  

        Double_t EpMin = 0.00200;  
        Double_t EpMax = Es/(1. + (2.*Es/MT)*SIN2);  
        Double_t Ep    = EpMin;
        Double_t step  = 0.001; 
        Double_t Q2=0,amtGE=0,amtGM=0,bilGE=0,bilGM=0,dipGE=0,dipGM=0,bosGE,bosGM; 

	AMTFormFactors          *amtFF = new AMTFormFactors(); 
	BilenkayaFormFactors    *bilFF = new BilenkayaFormFactors(); 
	InSANEDipoleFormFactors *dipFF = new InSANEDipoleFormFactors();
        BostedFormFactors       *bosFF = new BostedFormFactors();  

        vector<Double_t> vEp,vQ2,aGE,aGM,bGE,bGM,dGE,dGM,bbGE,bbGM; 

        while(Ep<=EpMax){
		Q2    = InSANE::Kine::Qsquared(Es,Ep,th);
		amtGE = amtFF->GEp(Q2);
		amtGM = amtFF->GMp(Q2);
		bilGE = bilFF->GEp(Q2);
		bilGM = bilFF->GMp(Q2);
		dipGE = dipFF->GEp(Q2);
		dipGM = dipFF->GMp(Q2);
		bosGE = bosFF->GEp(Q2);
		bosGM = bosFF->GMp(Q2);
                vEp.push_back(Ep); 
                vQ2.push_back(Q2); 
                aGE.push_back(amtGE);
                aGM.push_back(amtGM);
                bGE.push_back(bilGE);
                bGM.push_back(bilGM);
                dGE.push_back(dipGE);
                dGM.push_back(dipGM);
                bbGE.push_back(bosGE);
                bbGM.push_back(bosGM);
                // cout << "Ep = " << Form("%.3f",Ep) << "\t"
                //      << "Q2 = " << Form("%.3f",Q2) << "\t" 
                //      << "GE = " << Form("%.3E",GE) << "\t"
                //      << "GM = " << Form("%.3E",GM) << endl;  
		Ep += step;
        }

	const int N = vEp.size();

        Int_t width = 2; 

        TGraph *aQ2GE = new TGraph(N,&vQ2[0],&aGE[0]);
	aQ2GE->SetMarkerStyle(20);
        aQ2GE->SetMarkerColor(kBlack); 
        aQ2GE->SetLineColor(kBlack); 
        aQ2GE->SetLineWidth(width); 
 
        TGraph *aQ2GM = new TGraph(N,&vQ2[0],&aGM[0]);
	aQ2GM->SetMarkerStyle(20);
        aQ2GM->SetMarkerColor(kBlack); 
        aQ2GM->SetLineColor(kBlack); 
        aQ2GM->SetLineWidth(width); 
 
        TGraph *bQ2GE = new TGraph(N,&vQ2[0],&bGE[0]);
	bQ2GE->SetMarkerStyle(20);
        bQ2GE->SetMarkerColor(kRed); 
        bQ2GE->SetLineColor(kRed); 
        bQ2GE->SetLineWidth(width); 
 
        TGraph *bQ2GM = new TGraph(N,&vQ2[0],&bGM[0]);
	bQ2GM->SetMarkerStyle(20);
        bQ2GM->SetMarkerColor(kRed); 
        bQ2GM->SetLineColor(kRed); 
        bQ2GM->SetLineWidth(width); 
 
        TGraph *dQ2GE = new TGraph(N,&vQ2[0],&dGE[0]);
	dQ2GE->SetMarkerStyle(20);
        dQ2GE->SetMarkerColor(kBlue); 
        dQ2GE->SetLineColor(kBlue); 
        dQ2GE->SetLineWidth(width); 
 
        TGraph *dQ2GM = new TGraph(N,&vQ2[0],&dGM[0]);
	dQ2GM->SetMarkerStyle(20);
        dQ2GM->SetMarkerColor(kBlue); 
        dQ2GM->SetLineColor(kBlue); 
        dQ2GM->SetLineWidth(width); 
 
        TGraph *bosQ2GE = new TGraph(N,&vQ2[0],&bbGE[0]);
	bosQ2GE->SetMarkerStyle(20);
        bosQ2GE->SetMarkerColor(kCyan+2); 
        bosQ2GE->SetLineColor(kCyan+2); 
        bosQ2GE->SetLineWidth(width); 
 
        TGraph *bosQ2GM = new TGraph(N,&vQ2[0],&bbGM[0]);
	bosQ2GM->SetMarkerStyle(20);
        bosQ2GM->SetMarkerColor(kCyan+2); 
        bosQ2GM->SetLineColor(kCyan+2); 
        bosQ2GM->SetLineWidth(width); 
 
        TMultiGraph *GE = new TMultiGraph(); 
        GE->Add(aQ2GE ,"l"); 
        GE->Add(bQ2GE ,"l"); 
        GE->Add(dQ2GE ,"l"); 
        GE->Add(bosQ2GE,"l"); 

        TMultiGraph *GM = new TMultiGraph(); 
	GM->Add(aQ2GM ,"l"); 
        GM->Add(bQ2GM ,"l"); 
        GM->Add(dQ2GM ,"l"); 
        GM->Add(bosQ2GM,"l"); 
 
        TString xAxisTitle  = Form("Q^{2} (GeV^{2})");
        TString yAxisTitle1 = Form("Proton G_{E}"); 
        TString yAxisTitle2 = Form("Proton G_{M}"); 

        TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8);
        Leg->AddEntry(aQ2GE ,"AMT Model"      ,"l");
        Leg->AddEntry(bQ2GE ,"Bilenkaya Model","l");
        Leg->AddEntry(dQ2GE ,"Dipole Model"   ,"l");
        Leg->AddEntry(bosQ2GE,"Bosted Model"   ,"l");

        TCanvas *c1 = new TCanvas("c1","Proton Form Factors",1000,800);
	c1->SetFillColor(kWhite);  
        c1->Divide(2,1); 

        c1->cd(1);
	gPad->SetLogy(1);
        GE->Draw("A");
        GE->SetTitle(yAxisTitle1); 
	GE->GetXaxis()->SetTitle(xAxisTitle); 
	GE->GetXaxis()->CenterTitle(); 
        GE->Draw("A");
        Leg->Draw("same");
        c1->Update(); 

        c1->cd(2);
	gPad->SetLogy(1);
        GM->Draw("A");
        GM->SetTitle(yAxisTitle2); 
	GM->GetXaxis()->SetTitle(xAxisTitle); 
	GM->GetXaxis()->CenterTitle(); 
        GM->Draw("A");
        c1->Update(); 


}
