/// Test the form factor classes 

void FormFactors_He3(){

        Int_t    N     = 4000; 
        Double_t Q2Min = 0.0; 
        Double_t Q2Max = 12.0; 
        Double_t dQ2   = (Q2Max-Q2Min)/( (double)N ); 
        Double_t Q2=Q2Min;
        Double_t GE=0,GM=0; 

	MSWFormFactors *msw     = new MSWFormFactors(); 
	AmrounFormFactors *amr1 = new AmrounFormFactors(); 
	AmrounFormFactors *amr2 = new AmrounFormFactors(); 
        amr2->UseFortran(true); 

	vector<Double_t> aQ2,aGE,aGM;

        ImportData(aQ2,aGE,aGM);

        vector<Double_t> vEp,vGE,vGM,vQ2; 
        vector<Double_t> vGEamr1,vGMamr1; 
        vector<Double_t> vGEamr2,vGMamr2; 

        for(int i=0;i<N;i++){
		Q2 = Q2Min + ( (double)i )*dQ2; 
		GE = msw->GEHe3(Q2);
		GM = msw->GMHe3(Q2);
                vQ2.push_back(Q2); 
                vGE.push_back(GE);
                vGM.push_back(GM);
		GE = amr1->GEHe3(Q2);
		GM = amr1->GMHe3(Q2);
                vGEamr1.push_back(GE);
                vGMamr1.push_back(GM);
		GE = amr2->GEHe3(Q2);
		GM = amr2->GMHe3(Q2);
                vGEamr2.push_back(GE);
                vGMamr2.push_back(GM);
        }

	const int M = vQ2.size();

	int width = 2;
 
        const int FORT = aQ2.size();
        TGraph *aQ2GE = new TGraph(FORT,&aQ2[0],&aGE[0]);
	aQ2GE->SetMarkerStyle(20);
        aQ2GE->SetMarkerColor(kBlack); 
        aQ2GE->SetLineColor(kBlack); 
        aQ2GE->SetLineWidth(width); 
 
        TGraph *aQ2GM = new TGraph(FORT,&aQ2[0],&aGM[0]);
	aQ2GM->SetMarkerStyle(20);
        aQ2GM->SetMarkerColor(kBlack); 
        aQ2GM->SetLineColor(kBlack); 
        aQ2GM->SetLineWidth(width); 
      
        TGraph *Q2GE = new TGraph(M,&vQ2[0],&vGE[0]);
	Q2GE->SetMarkerStyle(20);
        Q2GE->SetMarkerColor(kMagenta); 
        Q2GE->SetLineColor(kMagenta); 
        Q2GE->SetLineWidth(width); 
 
        TGraph *Q2GM = new TGraph(M,&vQ2[0],&vGM[0]);
	Q2GM->SetMarkerStyle(20);
        Q2GM->SetMarkerColor(kMagenta); 
        Q2GM->SetLineColor(kMagenta); 
        Q2GM->SetLineWidth(width); 
 
        TGraph *Q2GEamr1 = new TGraph(M,&vQ2[0],&vGEamr1[0]);
	Q2GEamr1->SetMarkerStyle(20);
        Q2GEamr1->SetMarkerColor(kRed); 
        Q2GEamr1->SetLineColor(kRed); 
        Q2GEamr1->SetLineWidth(width); 

        TGraph *Q2GMamr1 = new TGraph(M,&vQ2[0],&vGMamr1[0]);
	Q2GMamr1->SetMarkerStyle(20);
        Q2GMamr1->SetMarkerColor(kRed); 
        Q2GMamr1->SetLineColor(kRed); 
        Q2GMamr1->SetLineWidth(width); 

        TGraph *Q2GEamr2 = new TGraph(N,&vQ2[0],&vGEamr2[0]);
	Q2GEamr2->SetMarkerStyle(20);
        Q2GEamr2->SetMarkerColor(kBlue); 
        Q2GEamr2->SetLineColor(kBlue); 
        Q2GEamr2->SetLineWidth(width); 

        TGraph *Q2GMamr2 = new TGraph(M,&vQ2[0],&vGMamr2[0]);
	Q2GMamr2->SetMarkerStyle(20);
        Q2GMamr2->SetMarkerColor(kBlue); 
        Q2GMamr2->SetLineColor(kBlue); 
        Q2GMamr2->SetLineWidth(width); 

        TMultiGraph *gQ2GE = new TMultiGraph();
        gQ2GE->Add(Q2GE,"c");
        gQ2GE->Add(Q2GEamr1,"c");
        gQ2GE->Add(Q2GEamr2,"c");
        gQ2GE->Add(aQ2GE,"c");

        TMultiGraph *gQ2GM = new TMultiGraph();
        gQ2GM->Add(Q2GM,"c");
        gQ2GM->Add(Q2GMamr1,"c");
        gQ2GM->Add(Q2GMamr2,"c");
        gQ2GM->Add(aQ2GM,"c");

        TString xAxisTitle1 = Form("E_{p} (GeV)");
        TString xAxisTitle2 = Form("Q^{2} (GeV^{2})");

        TString yAxisTitle1 = Form("G_{E}"); 
        TString yAxisTitle2 = Form("G_{M}"); 

        TLegend *L = new TLegend(0.6,0.6,0.8,0.8);
        L->SetFillColor(kWhite);
        L->AddEntry(Q2GE    ,"MSW"                            ,"l");
        L->AddEntry(Q2GEamr1,"Amroun (C++)"                   ,"l");
        L->AddEntry(Q2GEamr2,"Amroun (fortran)"               ,"l");
        L->AddEntry(aQ2GE   ,"Amroun (fortran, from rosetail)","l");

        TCanvas *c1 = new TCanvas("c1","3He Form Factors",1000,800);
	c1->SetFillColor(kWhite);  
        c1->Divide(2,1); 

        c1->cd(1);
	gPad->SetLogy(0);
        gQ2GE->Draw("A");
        gQ2GE->SetTitle(yAxisTitle1); 
	gQ2GE->GetXaxis()->SetTitle(xAxisTitle2); 
	gQ2GE->GetXaxis()->CenterTitle(); 
        L->Draw("same");
        c1->Update(); 

        c1->cd(2);
	gPad->SetLogy(0);
        gQ2GM->Draw("A");
        gQ2GM->SetTitle(yAxisTitle2); 
	gQ2GM->GetXaxis()->SetTitle(xAxisTitle2); 
	gQ2GM->GetXaxis()->CenterTitle(); 
        c1->Update(); 



}
//______________________________________________________________________________
void ImportData(vector<double> &Q2,vector<double> &GE,vector<double> &GM){

        double iQ2,iGE,iGM; 
	TString inpath = Form("./dump/amroun_fortran.dat");
	ifstream infile;
	infile.open(inpath);
	if(infile.fail()){
		cout << "Cannot open file: " << inpath << endl;
		exit(1);
	}else{
		while(!infile.eof()){
			infile >> iQ2 >> iGE >> iGM; 
			Q2.push_back(iQ2); 
			GE.push_back(iGE); 
			GM.push_back(iGM); 
		}
                Q2.pop_back();
                GE.pop_back();
                GM.pop_back();
	}


}
