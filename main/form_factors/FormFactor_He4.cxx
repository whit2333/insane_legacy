int FormFactor_He4()
{

   //InSANEDipoleFormFactors * dipole_ff = new InSANEDipoleFormFactors();
   InSANEDipoleFormFactors dipole_ff;
   auto lFCHe4 = [&](double* xs, double *ps) {
      double Q2 = xs[0];
      return( TMath::Abs(dipole_ff.FCHe4(Q2)) );
   };
   TF1 * FCHe4 = new TF1("FCHe4", lFCHe4, 0.0,4.0,0);

   // --------------------------------

   TCanvas     * c   = 0;//new TCanvas();
   TGraph      * gr  = 0;
   TLegend     * leg = 0;
   TMultiGraph * mg  = 0;

   leg = new TLegend(0.7,0.7,0.89,0.89);
   mg  = new TMultiGraph();
   c   = new TCanvas();
   c->cd(0);
   gPad->SetLogy(true);
   FCHe4->SetLineColor(1);
   FCHe4->SetLineWidth(2);
   gr = new TGraph( FCHe4->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"^{4}He","l");

   mg->Draw("a");
   mg->GetYaxis()->SetTitle("F_{C}^{4He}");
   mg->GetXaxis()->SetTitle("Q^{2} [GeV^{2}]");
   leg->Draw();
   c->SaveAs("results/form_factors/FormFactor_He4.pdf");

   return 0;
}

