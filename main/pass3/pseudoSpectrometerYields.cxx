Int_t pseudoSpectrometerYields(Int_t runNumber = 72995) {


   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;

   rman->GetCurrentFile()->cd("pseudoSpectrometers");

   TCanvas * c = new TCanvas("pseudoSpecs","pseudo specs");
//    c->Divide(2,4);
   TString drawOpt="box";

   for(int i = 1;i<=8;i++) {

      const char * treeName = Form("pseudoSpec%d",i);

      TTree * t = (TTree*) gROOT->FindObject(treeName);
      if(t) {
          if(i!=1) drawOpt="box same ";
/*         c->cd(9-i);*/
// t->Draw(Form("InSANEDISTrajectory.GetQSquared():InSANEDISTrajectory.GetBjorkenX()>>spec%dxVsQ(20,0.0,1.0,10,500000,5500000)",i),"","COLZ");
/*            t->Draw(Form("InSANEDISTrajectory.fPhi:InSANEDISTrajectory.fTheta>>spec%dxVsQ",i),"",drawOpt.Data());*/
         t->Draw(Form("InSANEDISTrajectory.fPhi*180.0/TMath::Pi():InSANEDISTrajectory.fTheta*180.0/TMath::Pi() >>spec%dxVsQ(100,20,50,100,-120,120)",i),"","goff");
         TH2F*h2 = (TH2F*) gROOT->FindObject(Form("spec%dxVsQ",i));
         h2->Draw(drawOpt.Data());
      }
   }
/*
   gr1->SetLineColor(1);
   gr1->SetLineWidth(2);
   gr1->SetMarkerColor(1);
   gr1->SetMarkerSize(2);
   gr1->SetMarkerStyle(20);
   gr1->SetTitle("Asymmetry");
   gStyle->SetEndErrorSize(5) ;
   gr1->GetXaxis()->SetTitle("Run Number");
   gr1->GetYaxis()->SetTitle("A");
   gr1->Draw("ap");

   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("Asymmetry vs Run");
   leg->AddEntry(gr1,"Asymmetries with errors","lep");*/
//   leg->AddEntry("","Function abs(#frac{sin(x)}{x})","l");
//    leg->AddEntry(grPBT,"Pb*Pt/10","p");
//    leg->AddEntry(grDet,"Detector Changes","p");
//    leg->AddEntry(grBeam,"Config Changes","p");
//   leg->Draw();


return(0);
}
