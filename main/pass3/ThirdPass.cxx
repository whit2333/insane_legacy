/**  Third Pass Analysis script
 *
 *
 *  The Steps of a good analyzer pass script
 *  - Name input and output trees
 *  - Define the analyzer for this pass. \note This does not yet create the trees! This happens at initialize
 *  - Create each detector used by the analysis and add it to the analyzer. 
 *    Currently all the detectors are created in InSANEDetectorAnalysis
 *  - Define and setup each "Correction" and "Calculation".
 *  - Set each detector used by the analysis
 *  - Add the corrections and calculations 
 *    \note Order Matters. It is the execution of the list from first to last. 
 *  - Initialze and Process 
 * 
 */
Int_t ThirdPass(Int_t runnumber = 72995) {
  gROOT->SetBatch(kTRUE);

  const char * sourceTreeName = "DISElectrons";
  const char * outputTreeName = "trajectories3";

// Set the run if its not set
  if(!rman->IsRunSet() ) rman->SetRun(runnumber);
  else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;

// Clean up the file
   rman->GetCurrentFile()->Delete("trajectories3;*");

// Create First Pass "analyzer", to which, "analyzes" by executing the calculations added.
   std::cout << " + Creating SANETrajectoryAnalyzer\n"; 
   SANETrajectoryAnalyzer * analyzer = 
     new SANETrajectoryAnalyzer(outputTreeName ,sourceTreeName);

//_______________ DETECTORS _____________//
// Should Create Detectors here or before creating the analyzer?...
//   analyzer->AddDetector(analyzer->fCherenkovDetector); 
//   analyzer->AddDetector(analyzer->fBigcalDetector); 
//   analyzer->AddDetector(analyzer->fHodoscopeDetector); 
//   analyzer->AddDetector(analyzer->fTrackerDetector); 

   std::cout << " + Creating BETAPseudoSpectrometer(s) \n"; 
/*   BETAPseudoSpectrometer * PseudoSpectrometer[8];*/

//___________BETA DETECTORS Ind. Calculations_________//

std::cout << " + Creating BETAPseudoSpectrometersCalc\n"; 
   BETAPseudoSpectrometersCalc * pseudoSpecCalc[32];
  for(int i = 1;i<=analyzer->fPseudoSpectrometers->size();i++) { 
/*     PseudoSpectrometer[i-1] = new BETAPseudoSpectrometer(i);*/
     /// Add the detectors to the "detectors list"
     analyzer->AddDetector( analyzer->fPseudoSpectrometers->at(i-1) );

     pseudoSpecCalc[i-1] = new BETAPseudoSpectrometersCalc(analyzer);
     /// set the spectrometer detector used for this new calculation (also sets events which were created in analysis/analyzer class)
     pseudoSpecCalc[i-1]->SetSpectrometerDetector(analyzer->fPseudoSpectrometers->at(i-1) );
     /// Add the calculation to the "Calculations list"
     analyzer->AddCalculation(pseudoSpecCalc[i-1]);
  }

/*   BETAPseudoSpectrometersCalc * pseudoSpecCalc = new BETAPseudoSpectrometersCalc(analyzer);*/
//    pseudoSpecCalc->SetDetectors(analyzer);


// should have some asymmetry calculation like:
  // 1- set the bit which determines whether to even count this event in a denominator
  // 2- set the address of the bit which determines which number, N1 or N2, to increment
  // 3- in the detector's event,  fGoodEvent is used to mean that all cuts for a good
  //    track in that detector are passed. 
//   InSANEAsymmetry * helAsym = new InSANEAsymmetry();
//     helAsym->SetTriggerBitAddress(&(beta1->fEvent->fGoodEvent);
//   BETASubDetector * beta1 = new BETASubDetector(DetectorNumber);
//   BETASubDetector * beta2 = new BETASubDetector();// this beta2 acceptance must be completely isolated from beta2
//   // at first it is easy to just match to some other mirror, but once the mirrors are done 
//   // they can be added to get better statisitics for each mirror
//   InSANEAsymmetryDetector * cherenkov_efficiency_monitor_12 = new InSANEAsymmetryDetector(beta1,beta2);
//   

//____________ Add Corrections  _______________//
/// Time walk correction \see time_walk.cxx which must be executed first
  //analyzer->AddCorrection(timewalkcorr);

//____________ Add Calculations _______________//
// Add the calculations to the analyzer (order matters)

//    analyzer->AddCalculation(pseudoSpecCalc);

//____________ Initialize and Process _______________//
  analyzer->Initialize();
  analyzer->Process();

// is this needed?
  rman->WriteRun();

// should delete everything properly ... but... we are quiting
  delete analyzer;
//   delete b1;
//   delete l1;
//   delete g1;

  if(gROOT->IsBatch())   gROOT->ProcessLine(".q");
  else return(0);
}
