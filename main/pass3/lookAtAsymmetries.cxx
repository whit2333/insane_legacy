Int_t lookAtAsymmetries(Int_t runNumber = 72995) {

  if(!rman->IsRunSet() ) rman->SetRun(runNumber);
  else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;

  TClonesArray asymmetries = *(rman->GetCurrentRun()->fAsymmetries);

  TCanvas * c = new TCanvas("pseudoSpecs","pseudo spectrometers");

   TGraphErrors * gr1 = new TGraphErrors(8);
   TGraphErrors * gr2 = new TGraphErrors(8);
   TGraphErrors * gr3 = new TGraphErrors(8);
   Double_t Pt = rman->GetCurrentRun()->fTargetOfflinePolarization/100.0;
   Double_t Pb = rman->GetCurrentRun()->fBeamPolarization/100.0;
   InSANEAsymmetry * anAsym;
   for(int i =0; i<8;i++ ){
       anAsym = (InSANEAsymmetry * )asymmetries[i];
       gr1->SetPoint(i, i+1,  anAsym->GetRawAsymmetry() );
       gr1->SetPointError(i,0.0,anAsym->GetStatisticalError() );
   }
   for(int i =0; i<8;i++ ){
       anAsym = (InSANEAsymmetry * ) asymmetries[i+8];
       gr2->SetPoint(i,i+1.0+0.2,anAsym->GetRawAsymmetry() );
       gr2->SetPointError(i,0.0,anAsym->GetStatisticalError() );
   }
   for(int i =0; i<8;i++ ){
       anAsym = (InSANEAsymmetry * ) asymmetries[i+16];
       gr3->SetPoint(i,i+1.0+0.4,anAsym->GetRawAsymmetry() );
       gr3->SetPointError(i,0.0,anAsym->GetStatisticalError() );
   }

   TMultiGraph * mg = new TMultiGraph();
   gr1->SetLineColor(1);
   gr1->SetLineWidth(2);
   gr1->SetMarkerColor(1);
   gr1->SetMarkerSize(2);
   gr1->SetMarkerStyle(20);
   gr1->SetTitle("Asymmetry");
   gStyle->SetEndErrorSize(5) ;
   gr1->GetXaxis()->SetTitle("(pseudo)Spectrometer Number");
   gr1->GetYaxis()->SetTitle("A");
   gr1->SetMaximum(0.07);
   gr1->SetMinimum(-0.07);


//   gr1->Draw("ap");
   mg->Add(gr1,"p");


   gr2->SetLineColor(4);
   gr2->SetLineWidth(2);
   gr2->SetMarkerColor(4);
   gr2->SetMarkerSize(2);
   gr2->SetMarkerStyle(20);
   gr2->SetTitle("Asymmetry");
   gStyle->SetEndErrorSize(5) ;
   gr2->GetXaxis()->SetTitle("(pseudo)Spectrometer Number");
   gr2->GetYaxis()->SetTitle("A");
   //gr1->Draw("ap");
   gr2->SetMaximum(0.07);
   gr2->SetMinimum(-0.07);
   mg->Add(gr2,"p");

   gr3->SetLineColor(2);
   gr3->SetLineWidth(2);
   gr3->SetMarkerColor(2);
   gr3->SetMarkerSize(2);
   gr3->SetMarkerStyle(20);
   gr3->SetTitle("Asymmetry");
   gStyle->SetEndErrorSize(5) ;
   gr3->GetXaxis()->SetTitle("(pseudo)Spectrometer Number");
   gr3->GetYaxis()->SetTitle("A");
   gr3->SetMaximum(0.07);
   gr3->SetMinimum(-0.07);
//   gr1->Draw("ap");
   mg->Add(gr3,"p");
   mg->SetMaximum(0.1);
   mg->SetMinimum(-0.1);
/*   mg->GetXaxis()->SetLimits(-0.07,0.07);*/
   mg->Draw("a");
   TLegend * leg = new TLegend(0.1,0.1,0.5,0.2);
   leg->SetHeader("Asymmetry vs (pseudo)Spectrometer");
   leg->AddEntry(gr1,"E(central)=1.0 [GeV]#delta=0.2","lep");
   leg->AddEntry(gr2,"E(central)=0.8 [GeV]#delta=0.2","lep");
   leg->AddEntry(gr3,"E(central)=1.4 [GeV] #delta=0.4","lep");
//   leg->AddEntry("","Function abs(#frac{sin(x)}{x})","l");
//    leg->AddEntry(grPBT,"Pb*Pt/10","p");
//    leg->AddEntry(grDet,"Detector Changes","p");
//    leg->AddEntry(grBeam,"Config Changes","p");
   leg->Draw();

  c->SaveAs(Form("plots/%d/pseduSpecAsym.png",runNumber));
  c->SaveAs(Form("plots/%d/pseduSpecAsym.ps",runNumber));

return 0;
}
