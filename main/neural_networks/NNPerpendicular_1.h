#ifndef NNPerpendicular_h
#define NNPerpendicular_h

class NNPerpendicular { 
public:
   NNPerpendicular() {}
   ~NNPerpendicular() {}
   double Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6);
   double Value(int index, double* input);
private:
   double input0;
   double input1;
   double input2;
   double input3;
   double input4;
   double input5;
   double input6;
   double neuron0xeb12a00();
   double neuron0xbba92d8();
   double neuron0xbba94d0();
   double neuron0xbba9738();
   double neuron0xbba9938();
   double neuron0xbba9b38();
   double neuron0xaaafb60();
   double input0xaab0138();
   double neuron0xaab0138();
   double input0xaab03d8();
   double neuron0xaab03d8();
   double input0xaab06e8();
   double neuron0xaab06e8();
   double input0xaab0a80();
   double neuron0xaab0a80();
   double input0xeb128c8();
   double neuron0xeb128c8();
   double input0xaaafd38();
   double neuron0xaaafd38();
   double input0xaab0e30();
   double neuron0xaab0e30();
   double synapse0xd207828();
   double synapse0xaab02e8();
   double synapse0xaab0310();
   double synapse0xaab0338();
   double synapse0xaab0360();
   double synapse0xaab0388();
   double synapse0xaab03b0();
   double synapse0xaab05d0();
   double synapse0xaab05f8();
   double synapse0xaab0620();
   double synapse0xaab0648();
   double synapse0xaab0670();
   double synapse0xaab0698();
   double synapse0xaab06c0();
   double synapse0xaab08e0();
   double synapse0xaab0908();
   double synapse0xaab0930();
   double synapse0xaab09e0();
   double synapse0xaab0a08();
   double synapse0xaab0a30();
   double synapse0xaab0a58();
   double synapse0xaab0c30();
   double synapse0xaab0c58();
   double synapse0xaab0c80();
   double synapse0xaab0ca8();
   double synapse0xaab0cd0();
   double synapse0xaab0cf8();
   double synapse0xaab0d20();
   double synapse0xaab0d90();
   double synapse0xaab0db8();
   double synapse0xaab0de0();
   double synapse0xaab0e08();
   double synapse0xaaaff10();
   double synapse0xeb129c8();
   double synapse0xaab0958();
   double synapse0xaab0980();
   double synapse0xaab1008();
   double synapse0xaab1030();
   double synapse0xaab1058();
   double synapse0xaab1080();
};

#endif // NNPerpendicular_h

