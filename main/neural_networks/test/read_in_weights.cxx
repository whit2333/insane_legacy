int read_in_weights(const char * weightfile = "NNParapositronCorrections.txt") {
   int number = 1;
   const char * networkName = "paraPositronCorrections";
   const char * networkType = "positronCorrections";
   const char * networkClassName = Form("NNPara%s",networkType);

   if (!gROOT->GetClass("TMultiLayerPerceptron")) {
      gSystem->Load("libMLP");
   }

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName),"READ");
   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));

   if(!nnt) {return(-1);} 

   TMultiLayerPerceptron * mlp = new TMultiLayerPerceptron();
   
   mlp->LoadWeights(weightfile);

   mlp->SetData(nnt);

   std::cout << " read in weights \n";

   mlp->Draw();

   return(0);
}
