/*!  Builds the  neural network
 */
Int_t TrainNN_positron_ID(Int_t number=12) {
   const char * networkName = "Para59PositronID";
   const char * networkType = "PositronID";
   const char * networkClassName = Form("NNPara59%s",networkType);  

   Bool_t plotInputDataFirst = true; 

   if (!gROOT->GetClass("TMultiLayerPerceptron")) {
      gSystem->Load("libMLP");
   }

   Int_t ntrain  = 1000;
   Int_t nHidden = 8;
   Int_t nHidden2 = 3;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName),"READ");
   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   ANNDisElectronEvent * event = new ANNDisElectronEvent();
   nnt->SetBranchAddress("NNeEvents",&event);
   const char * inputLayout = event->GetMLPInputNeurons() ;

   const int fgNInputNeurons = event->GetNIDInputNeurons();
   std::cout << " network has " << fgNInputNeurons << "\n";
   std::cout << " inputs: " << inputLayout << "\n";

   if(plotInputDataFirst){
      TCanvas* c = new TCanvas("NNinputCanvas","Network analysis",800,600);
      c->Divide(3,2);
      c->cd(1);
      nnt->Draw("fYCluster:fXCluster>>clusterXY(200,-60,60,200,-120,120)","","colz",1000000000,1001);
      c->cd(2);
      nnt->Draw("fYClusterSigma:fXClusterSigma>>sigmaXY(200,0,5,200,0,5)","","colz",1000000000,1001);
      c->cd(3);
      nnt->Draw("fYClusterSkew:fXClusterSkew>>skewXY(200,-15,15,200,-15,15)","","colz",1000000000,1001);
      c->cd(4);
      nnt->Draw("fDelta_Energy:fClusterEnergy>>deltaenergyXY(200,0,5900,200,0,2000)","","colz",1000000000,1001);
      c->cd(5);
      nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fClusterEnergy>>deltathetaXY(200,0,5900,200,-20,20)","","colz",1000000000,1001);
      c->cd(6);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fClusterEnergy>>deltaphiXY(200,0,5900,200,-20,50)","","colz",1000000000,1001);
   }
   
   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",800,600);
   mlpa_canvas->Divide(3,2);
   mlpa_canvas->cd(5);
   if(nHidden2==0)  mlp = new TMultiLayerPerceptron ( Form(
                              "%s:%d:fBackground",
                              inputLayout,nHidden),nnt);
   else  mlp = new TMultiLayerPerceptron ( Form(
                           "%s:%d:%d:fBackground",
                           inputLayout,nHidden,nHidden2),nnt);
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kStochastic); // sucks
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kSteepestDescent);
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kBatch);
   mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???

   mlp->Train(ntrain, "text,graph,current,update=10"); // add graph to string for plot
   mlp->Export(networkClassName);

//   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",800,600);
//   mlpa_canvas->Divide(2,2);
   TMLPAnalyzer ana(mlp);
   // Initialisation
   ana.GatherInformations();
   // output to the console
   ana.CheckNetwork();
   mlpa_canvas->cd(1);
   // shows how each variable influences the network
   ana.DrawDInputs();
   mlpa_canvas->cd(2);
   // shows the network structure
   mlp->Draw();
   mlpa_canvas->cd(3);
   // draws the resulting network
   ana.DrawNetwork(0,"fBackground==0","fBackground==1");
   mlpa_canvas->cd(4);
   gPad->SetLogy(1);
//   ana.DrawTruthDeviations();

   ANNDisElectronEvent * event = new ANNDisElectronEvent();
   nnt->SetBranchAddress("NNeEvents",&event);
   TH1F *bg = new TH1F("bgh", "NN output", 50, -.5, 1.5);
   TH1F *sig = new TH1F("sigh", "NN output", 50, -.5, 1.5);
   bg->SetDirectory(0);
   sig->SetDirectory(0);
   Double_t * params = new Double_t[fgNInputNeurons];
   for (int i = 0; i < nnt->GetEntries(); i++) {
      nnt->GetEntry(i);
      // "fClusterEnergy,fXCluster,fYCluster,fXClusterSigma,fYClusterSigma,fXClusterSkew,fYClusterSkew,fGoodCherenkovHit,fCherenkovADC:%d:3:fBackground"
      event->GetMLPInputNeuronArray(params);
      //std::cout << " " << params[0] << "   " << event->fBackground << " \n";
      if(event->fBackground)
         bg->Fill(mlp->Evaluate(0,params));
      if(!(event->fBackground))
         sig->Fill(mlp->Evaluate(0,params));
   }
   bg->SetLineColor(kBlue);
   bg->SetFillStyle(3008);   bg->SetFillColor(kBlue);
   sig->SetLineColor(kRed);
   sig->SetFillStyle(3003); sig->SetFillColor(kRed);
   bg->SetStats(0);
   sig->SetStats(0);
   bg->Draw();
   sig->Draw("same");
   TLegend *legend = new TLegend(.75, .80, .95, .95);
   legend->AddEntry(bg, " Background events ");
   legend->AddEntry(sig, " DIS e- ");
   legend->Draw();


   // OBSERVATION :
   // NN does better without the maximum block positions (xy_max)
   // Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 
   
   // For two hidden layer networks, it seems that having a small number of second layer neurons
   // produces good results

   mlpa_canvas->Update();

   mlpa_canvas->SaveAs(Form("results/neural_networks/%s%d_%d.png",networkClassName,nHidden,nHidden2));
   mlpa_canvas->SaveAs(Form("results/neural_networks/%s%d_%d.pdf",networkClassName,nHidden,nHidden2));
   mlpa_canvas->SaveAs(Form("results/neural_networks/%s%d_%d.svg",networkClassName,nHidden,nHidden2));


return(0);
}
