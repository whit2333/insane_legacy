/*! Builds a chain of the simulation runs for ANN training.
 
    Fills a tree of ANNDisElectronEvent5 events, which include the following inputs
    - Cluster Energy
    - X and Y cluster positions
    - X and Y sigma
    - X and Y skewness
    - X and Y kurtosis
    The (possible) outputs:
    - fClusterDeltaX
    - fClusterDeltaY
    - fTrueEnergy
    - fTruePhi
    - fTrueTheta
    - signal/background

    Requirements for good electron event:
    - pid thrown = e-
    - 0 < DeltaEnergy < 800 MeV
    - phi > 2 degrees (this avoids bremsstrahlung photons from target being treated as e-)

 */
Int_t build_chain_electron_full_correction(Int_t number=250) {

   const char * networkName = "ParaElectronFullCorrection";
   const char * signalParticle = "e-";

   /// Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("lists/para/MC/mc_list_1.txt");  	

   /// Get chain of runs
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster                  * aCluster    = new BIGCALCluster;
   BETAG4MonteCarloEvent          * MCevent     = events->MC;
   BETAG4MonteCarloThrownParticle * thrown      = new BETAG4MonteCarloThrownParticle();
   InSANEFakePlaneHit             * bigcalPlane = 0;

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   ANNDisElectronEvent6 * disEvent = new ANNDisElectronEvent6();
   disEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"UPDATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNDisElectronEvent6", &disEvent);
   //aChain->StartViewer();
   //return(0);
   std::cout << " Chain has " << aChain->GetEntries() << ".\n";
   for (int i = 0;i< aChain->GetEntries();i++ ) {
      if( i%1000 == 0 ) {
         std::cout << "\r" << i ;
         std::cout.flush();
      }
      aChain->GetEntry(i);
      if( clusters->GetEntries() > 0 ){
         //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
         // only 1 thrown particle !!!
         thrown = (BETAG4MonteCarloThrownParticle*)(*thrownParticles)[0];

         for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {
            aCluster  = (BIGCALCluster*)(*clusters)[iClust];
            //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
            //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
            //for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
               //bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];

               TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
               disEvent->SetEventValues(aCluster); 

               disEvent->fDelta_Energy = thrown->Energy() - aCluster->GetEnergy();
               disEvent->fDelta_Theta  = p.Theta() - aCluster->GetTheta();
               disEvent->fDelta_Phi    = p.Phi()   - aCluster->GetPhi();
               disEvent->fTrueEnergy = thrown->Energy();
               disEvent->fTrueTheta  = p.Theta();
               disEvent->fTruePhi    = p.Phi() ;

               disEvent->fXClusterKurt = aCluster->fXKurtosis;
               disEvent->fYClusterKurt = aCluster->fYKurtosis;

               //disEvent->fClusterDeltaX = bigcalPlane->fLocalPosition.X() - aCluster->fXMoment;
               //disEvent->fClusterDeltaY = bigcalPlane->fLocalPosition.Y() - aCluster->fYMoment;
               disEvent->fNClusters = clusters->GetEntries();

               disEvent->fRasterX    = thrown->Vx();
               disEvent->fRasterY    = thrown->Vy();

               if( TMath::Abs(thrown->fMomentum4Vector.E()*1000.0 - bigcalPlane->fEnergy) < 40.0 )
                  if( disEvent->fDelta_Energy < 800.0 && disEvent->fDelta_Energy > 0.0 )
                     if( disEvent->fDelta_Phi/0.0175 > 2.0)
                        if( bigcalPlane->fPID == signalPID)
                           if( thrown->GetPdgCode() == signalPID ){
                              disEvent->fBackground = false;
                              nnt->Fill();
                           }

            } // bigcal plane loop
         } // cluster loop

      }
   }
   std::cout << "\n";
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   //nnt->StartViewer();
   gROOT->ProcessLine(".q");
   return(0);
}
