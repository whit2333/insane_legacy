/*! Networks to correct for 

 */
Int_t build_chain_positron_anglecorrection(Int_t number=701) {

   const char * networkName = "ParaPositronAngleCorrection";
   const char * signalParticle = "e+";

   /// Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   //aman->BuildQueue2("neural_networks/para/para_uniform_all.txt");  	
   aman->BuildQueue2("lists/para/MC/mc_list_1.txt");  	
   //aman->GetRunQueue()->push_back(4422); // 1405 -- 10k with min energy increased to 400MeV

   /// Get chain of runs
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloEvent * MCevent = events->MC;
   InSANEParticle * thrown = new InSANEParticle();
   InSANEFakePlaneHit * bigcalPlane = 0; 

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   TVector3 vertex;
   TVector3 p_naive;
   TVector3 p_thrown;

   ANNElectronThruFieldEvent2 * nnEvent = new ANNElectronThruFieldEvent2();
   nnEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"UPDATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNElectronThruFieldEvent2", &nnEvent);
   //aChain->StartViewer();
   //return(0);
   std::cout << " Chain has " << aChain->GetEntries() << ".\n";
   for (int i = 0;i< aChain->GetEntries() ;i++ ) {
      if( i%1000 == 0 ) {
         std::cout << "\r" << i ;
         std::cout.flush();
      }
      aChain->GetEntry(i);
      if( clusters->GetEntries() > 0 ){
         //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
         // only 1 thrown particle !!!
         thrown = (InSANEParticle*)(*thrownParticles)[0];

         for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {
            aCluster  = (BIGCALCluster*)(*clusters)[iClust];
            //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
            //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
            for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
               bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];

               p_thrown.SetXYZ(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
               //nnEvent->SetEventValues(aCluster); 

               //nnEvent->fXCluster = aCluster->GetXmoment();
               //nnEvent->fYCluster = aCluster->GetYmoment();

               //nnEvent->fXClusterSigma = aCluster->GetXStdDeviation();
               //nnEvent->fYClusterSigma = aCluster->GetYStdDeviation();

               //nnEvent->fXClusterSkew = aCluster->fXSkewness;
               //nnEvent->fYClusterSkew = aCluster->fYSkewness;

               //nnEvent->fXClusterKurt = aCluster->fXKurtosis;
               //nnEvent->fYClusterKurt = aCluster->fYKurtosis;


               nnEvent->fTrueEnergy             = thrown->Energy()*1000.0;
               nnEvent->fTrueTheta              = p_thrown.Theta();
               nnEvent->fTruePhi                = p_thrown.Phi() ;

               nnEvent->fRasterX                = thrown->Vx();
               nnEvent->fRasterY                = thrown->Vy();

               vertex.SetXYZ(thrown->Vx(),thrown->Vy(),thrown->Vz());
               p_naive = bigcalPlane->fPosition - vertex; 

               nnEvent->fDelta_Energy           = thrown->Energy()*1000.0 - bigcalPlane->fEnergy;
               nnEvent->fDelta_Theta            = p_thrown.Theta() - p_naive.Theta();//bigcalPlane->fPosition.Theta();//aCluster->GetTheta();
               nnEvent->fDelta_Phi              = p_thrown.Phi()   - p_naive.Phi();//bigcalPlane->fPosition.Phi();//aCluster->GetPhi();

               nnEvent->fBigcalPlaneTheta       = p_naive.Theta();//bigcalPlane->fPosition.Theta();
               nnEvent->fBigcalPlanePhi         = p_naive.Phi();//bigcalPlane->fPosition.Phi();

               nnEvent->fBigcalPlaneDeltaPTheta = bigcalPlane->fMomentum.Theta() - p_naive.Theta();//bigcalPlane->fPosition.Theta();
               nnEvent->fBigcalPlaneDeltaPPhi   = bigcalPlane->fMomentum.Phi()   - p_naive.Phi();//bigcalPlane->fPosition.Phi();

               nnEvent->fBigcalPlaneX           = bigcalPlane->fLocalPosition.X();
               nnEvent->fBigcalPlaneY           = bigcalPlane->fLocalPosition.Y();
               nnEvent->fBigcalPlaneEnergy      = bigcalPlane->fEnergy;

               if( TMath::Abs(nnEvent->fBigcalPlaneY) < 102 )
               if( TMath::Abs(nnEvent->fBigcalPlaneX) < 55 )
               if( nnEvent->fDelta_Energy < 20.0 && nnEvent->fDelta_Energy > 0.0 )
               if( bigcalPlane->fPID == signalPID)
                  if( thrown->GetPdgCode() == signalPID ){
                     //nnEvent->fBackground = false;
                     nnt->Fill();
                  }
            } // bigcal plane loop
         } // cluster loop

      }
   }
   std::cout << "\n";
   //nnt->FlushBaskets();
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   //nnt->StartViewer();
   gROOT->ProcessLine(".q");
   return(0);
}
