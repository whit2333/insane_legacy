/*!  Builds the  neural network
 */
Int_t TrainNNeCorrections(Int_t number=10) {
   const char * networkName = "Para59ElectronCorrection";
   const char * networkType = "ElectronCorrection";
   const char * networkClassName = Form("NNPara59%s",networkType);  

   Bool_t plotInputDataFirst = true; 

   if (!gROOT->GetClass("TMultiLayerPerceptron")) {
      gSystem->Load("libMLP");
   }

   Int_t ntrain  = 100;
   Int_t nHidden = 20;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName),"READ");
   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   ANNDisElectronEvent * event = new ANNDisElectronEvent();
   nnt->SetBranchAddress("NNeEvents",&event);

   if(plotInputDataFirst){
      TCanvas* c = new TCanvas("NNinputCanvas",Form("Network Analysis %s",networkType),1200,800);
      c->Divide(3,2);
      c->cd(1);
      nnt->Draw("fYCluster:fXCluster>>clusterXY(200,-60,60,200,-120,120)","","colz",1000000000,1001);
      c->cd(2);
      nnt->Draw("fYClusterSigma:fXClusterSigma>>sigmaXY(200,0,5,200,0,5)","","colz",1000000000,1001);
      c->cd(3);
      nnt->Draw("fYClusterSkew:fXClusterSkew>>skewXY(200,-15,15,200,-15,15)","","colz",1000000000,1001);
      c->cd(4);
      nnt->Draw("fDelta_Energy:fClusterEnergy>>deltaenergyXY(200,0,5000,200,-200,1000)","","colz",1000000000,1001);
      c->cd(5);
      nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fClusterEnergy>>deltathetaXY(200,0,5000,200,-10,10)","","colz",1000000000,1001);
      c->cd(6);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fClusterEnergy>>deltaphiXY(200,0,5000,200,-20,20)","","colz",1000000000,1001);
      c->SaveAs(Form("results/neural_networks/NNInputData%s%d.png",networkName,nHidden));
      c->SaveAs(Form("results/neural_networks/NNInputData%s%d.pdf",networkName,nHidden));
   }

   TCanvas* mlpa_canvas = new TCanvas("ANNtraining",Form("Traning Results for %s",networkType),1200,800);

   mlpa_canvas->Divide(3,2);
   mlpa_canvas->cd(3);
   const Int_t fgNInputNeurons = event->GetNCorrectionInputNeurons();

   //    std::cout << " input string has " << fgNInputNeurons << " neurons : \n";
   //    std::cout << event->GetCorrectionMLPInputNeurons() << "\n";
   mlp = new TMultiLayerPerceptron(Form("%s:%d:fDelta_Energy,fDelta_Theta,fDelta_Phi",
         event->GetCorrectionMLPInputNeurons(),nHidden) ,nnt);
   //   mlp->SetLearningMethod(TMultiLayerPerceptron::kStochastic); // sucks
   //   mlp->SetLearningMethod(TMultiLayerPerceptron::kSteepestDescent);
   //   mlp->SetLearningMethod(TMultiLayerPerceptron::kBatch);
   mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???

   mlp->Train(ntrain, "text,graph,current,update=10"); // add graph to string for plot
   mlp->Export(networkClassName);

   TMLPAnalyzer ana(mlp);
   // Initialisation
   ana.GatherInformations();
   // output to the console
   ana.CheckNetwork();
   mlpa_canvas->cd(1);
   // shows how each variable influences the network
   ana.DrawDInputs();
   mlpa_canvas->cd(2);
   // shows the network structure
   mlp->Draw();


   /// Histograms to check NN
   TH2F *energies = new TH2F("energy_diff_vs_energy", "Energy Diff (NN - Thrown) ", 200, 0.0, 3000.0,200,-1000.0 , 1000.0);

   TH1F *energy_diff = new TH1F("energy diff", "Energy Diff (NN - Thrown) ", 200, -1000.0, 1000.0);
   TH1F *theta_diff  = new TH1F("theta diff", "#theta Diff (NN - Thrown) ", 200, -3.0, 3.0);
   TH1F *phi_diff    = new TH1F("phi diff", "#phi Diff (NN - Thrown) ", 200, -3.0, 3.0);

   TH1F *energy_cor = new TH1F("energy cor", "Energy correction diff (NN - Thrown) ", 200, -500.0, 500.0);
   TH1F *theta_cor  = new TH1F("thetacor", "#theta correction diff (NN - Thrown) ", 200, -10.0, 10.0);
   TH1F *phi_cor    = new TH1F("phicor", "#phi correction diff ", 200, -10.0, 10.0);

   energies->SetDirectory(0);
   energy_diff->SetDirectory(0);
   theta_diff->SetDirectory(0);
   phi_diff->SetDirectory(0);

   TH1F *bg = new TH1F("bgh", "NN output", 50, -.5, 1.5);
   TH1F *sig = new TH1F("sigh", "NN output", 50, -.5, 1.5);
   bg->SetDirectory(0);
   sig->SetDirectory(0);



   Double_t * params = new Double_t[fgNInputNeurons];


   /// Loop over tree and get the corrections
   for (int i = 0; i < nnt->GetEntries(); i++) {
      nnt->GetEntry(i);

      event->GetCorrectionMLPInputNeuronArray(params);

      Double_t delta_energy_NN = mlp->Evaluate(0,params);
      Double_t delta_theta_NN = mlp->Evaluate(1,params);
      Double_t delta_phi_NN = mlp->Evaluate(2,params);

//       energy_cor->Fill(delta_energy_NN);
//       theta_cor->Fill(delta_theta_NN);
//       phi_cor->Fill(delta_phi_NN);

//      std::cout << " delta: energy = " << delta_energy_NN << " , theta = " << delta_theta_NN << " , phi = " << delta_phi_NN << "\n";

      Double_t cEnergy = event->fClusterEnergy + delta_energy_NN ;
      Double_t cTheta =  delta_theta_NN ;
      Double_t cPhi = delta_phi_NN ;

      sig->Fill(event->fSignal);
      bg->Fill(event->fBackground);

      energy_cor->Fill( cEnergy  - event->fTrueEnergy );
      theta_cor->Fill( (cTheta  - event->fDelta_Theta)/0.0175 );
      phi_cor->Fill( (cPhi  - event->fDelta_Phi)/0.0175 );

      energies->Fill( event->fTrueEnergy, cEnergy - event->fTrueEnergy  );

      //std::cout << " " << params[0] << "   " << event->fBackground << " \n";
   }
   bg->SetLineColor(kBlue);
   bg->SetFillStyle(3008);   bg->SetFillColor(kBlue);
   sig->SetLineColor(kRed);
   sig->SetFillStyle(3003); sig->SetFillColor(kRed);
   bg->SetStats(0);
   sig->SetStats(0);


   mlpa_canvas->cd(4);
   energy_diff->Draw();

   mlpa_canvas->cd(5);
   theta_cor->Draw();

   mlpa_canvas->cd(6);
   phi_cor->Draw();


//    TLegend *legend = new TLegend(.75, .80, .95, .95);
//    legend->AddEntry(bg, " Background events ");
//    legend->AddEntry(sig, " DIS e- ");
//    legend->Draw();
//    mlpa_canvas->cd(5);
//    energy_cor->Draw();

/// OBSERVATION :
/// NN does better without the maximum block positions (xy_max)
/// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

   mlpa_canvas->Update();

   mlpa_canvas->SaveAs(Form("results/neural_networks/%s%d.png",networkName,nHidden));
   mlpa_canvas->SaveAs(Form("results/neural_networks/%s%d.pdf",networkName,nHidden));


return(0);
}
