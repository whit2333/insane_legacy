/*!   Adds the events to the tree
 */
Int_t build_chain_positron(Int_t number=7) {

   const char * networkName = "ParaPositronCorrection";
   const char * signalParticle = "e+";

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   //aman->BuildQueue("neural_networks/para/para_positrons.txt");  	
//    aman->GetRunQueue()->push_back(1363);
//    aman->GetRunQueue()->push_back(1364);
/*   aman->GetRunQueue()->push_back(1368);*/
/*   aman->GetRunQueue()->push_back(1372);*/
//    aman->GetRunQueue()->push_back(1384);
//    aman->GetRunQueue()->push_back(1385);
//    aman->GetRunQueue()->push_back(1388); // simulating 1388 100k
//    aman->GetRunQueue()->push_back(1389);// simulating 1389 10k -- fixed random number generator
//    aman->GetRunQueue()->push_back(1390);// simulating 1390 10k -- fixed random number generator
//    aman->GetRunQueue()->push_back(1393);// simulating 1393 10k 
//    aman->GetRunQueue()->push_back(1391);// simulating 1391 100k -- fixed random number generator

   aman->GetRunQueue()->push_back(1395);// simulating 1395 10k -- shrank pedestals and tdc peaks gausian by factor of 2
   aman->GetRunQueue()->push_back(1396);// simulating 1396 10k -- shrank pedestals and tdc peaks gausian by factor of 2

  aman->GetRunQueue()->push_back(1397);// simulating 1397 100k -- shrank pedestals and tdc peaks gausian by factor of 2
  aman->GetRunQueue()->push_back(1398);// simulating 1398 100k -- shrank pedestals and tdc peaks gausian by factor of 2
  aman->GetRunQueue()->push_back(1399);// simulating 1399 100k -- shrank pedestals and tdc peaks gausian by factor of 2

//    aman->GetRunQueue()->push_back(1394);// simulating 1394 10k --- corrupt?



   int Npositrons = 0;

   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();

   std::cout << "\n Building Neural Network ... \n";\
   std::cout << "  -  " << networkName <<  " \n";
   std::cout << "  - Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloEvent * MCevent = events->MC;
   BETAG4MonteCarloThrownParticle * thrown = new BETAG4MonteCarloThrownParticle();

   BIGCALGeometryCalculator * bcGeo = BIGCALGeometryCalculator::GetCalculator();

   InSANEFakePlaneHit * bigcalPlane = 0; 

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   ANNDisElectronEvent * disEvent = new ANNDisElectronEvent();
   disEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"RECREATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNDisElectronEvent", &disEvent);
   //aChain->StartViewer();

   std::cout << " chain has " << aChain->GetEntries() << " entries\n";


   bool isEventFilled = false;

   int Ncheck = aChain->GetEntries()/10;
   int iCheck = 0;
   for (int i = 0;i< aChain->GetEntries();i++ ) {
      aChain->GetEntry(i);
      isEventFilled = false;

     if (i%Ncheck == 0 ){iCheck++; printf("  ... %d\% \n",iCheck*10); }
   

     //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
      if( clusters->GetEntries() > 0 ){
     //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
     // only 1 thrown particle !!!
	 thrown = (BETAG4MonteCarloThrownParticle*)(*thrownParticles)[0];
 
      for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {   
	 aCluster  = (BIGCALCluster*)(*clusters)[iClust];
	 //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));

         if(!bcGeo->IsBlockOnPerimeter(aCluster->fiPeak,aCluster->fjPeak) )
         for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
            bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];


	 TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
	 disEvent->SetEventValues(aCluster); 

         disEvent->fDelta_Energy = bigcalPlane->fEnergy - aCluster->GetEnergy();
         disEvent->fDelta_Theta  = p.Theta() - aCluster->GetTheta();
         disEvent->fDelta_Phi    = p.Phi()   - aCluster->GetPhi();
         disEvent->fTrueEnergy = bigcalPlane->fEnergy;
	 disEvent->fTrueTheta  = p.Theta();
	 disEvent->fTruePhi    = p.Phi() ;
         disEvent->fNClusters = clusters->GetEntries();


         if( bigcalPlane->fPID == signalPID) Npositrons++;

         
	    if( !isEventFilled )
	    if( disEvent->fDelta_Energy < 400.0)
	    if( disEvent->fClusterEnergy > 300.0)
	    if( disEvent->fDelta_Phi/0.0175 < -2.0)
         if( bigcalPlane->fPID == signalPID)
	 if( thrown->GetPdgCode() == signalPID ){
/*            std::cout << "PID = " << signalPID << " was thrown\n";*/
            disEvent->fSignal = true;
            disEvent->fBackground = false;
            nnt->Fill();
            isEventFilled =true;
	 }
        }
      } 
      }
   }

   nnt->FlushBaskets();
   //nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   std::cout << " Input  Events        : " << aChain->GetEntries() << std::endl;
   std::cout << " N-positrons at plane : " << Npositrons <<  std::endl;
   std::cout << " Output Events        : " << nnt->GetEntries() << std::endl;


   return(0);
}
