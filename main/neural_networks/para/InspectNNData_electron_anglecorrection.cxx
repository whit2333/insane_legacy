/*!  Builds the  neural network.

 */
Int_t InspectNNData_electron_anglecorrection(Int_t number=706) {
   
   if (!gROOT->GetClass("TMultiLayerPerceptron")) { gSystem->Load("libMLP"); }

   const char * networkName      = "ParaElectronAngleCorrection";
   const char * networkType      = "ElectronAngleCorrection";
   const char * networkClassName = Form("NNPara%s",networkType);
   TString networkClassName2     = networkClassName;

   Int_t ntrain              = 100;
   Int_t nHidden             = 30;
   Int_t nHidden2            = 0;
   Bool_t plotInputDataFirst = true;

   ANNElectronThruFieldEvent2 * event = new ANNElectronThruFieldEvent2();
   const Int_t fgNInputNeurons = event->GetNInputNeurons();

   std::cout << "-----------------------------------" << std::endl;
   std::cout << "Network Class Name : " << networkClassName2.Data() << std::endl;
   std::cout << "Input string has " << event->GetNInputNeurons() << " neurons " << std::endl;
   std::cout << event->GetMLPInputNeurons() << std::endl;

   TMultiLayerPerceptron * mlp = 0;
   TMultiLayerPerceptron * mlp_energy = 0;
   TMultiLayerPerceptron * mlp_theta = 0;
   TMultiLayerPerceptron * mlp_phi = 0;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName),"READ");

   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   nnt->SetBranchAddress("NNeEvents",&event);

   TCanvas * c = 0;

   if(plotInputDataFirst){
      c = new TCanvas("NNinputCanvas","Network analysis");
      c->Divide(4,2);
      c->cd(1);
      nnt->Draw("fBigcalPlaneY:fBigcalPlaneX>>clusterXY(200,-100,100,200,-150,150)","","colz");
      c->cd(2);
      nnt->Draw("fBigcalPlanePhi*180.0/TMath::Pi():fBigcalPlaneTheta*180.0/TMath::Pi()>>bcThetaPhi(200,20,60,200,-50,50)","","colz");
      c->cd(3);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fDelta_Theta*180.0/TMath::Pi()>>skewXY(200,-15,15,200,0,20)","","colz");
      c->cd(4);
      nnt->Draw("fBigcalPlaneX:fDelta_Phi*180.0/TMath::Pi()>>rasterYvsDTheta(200,0,30,100,-150,150)","","colz");
      //nnt->Draw("fRasterY:fDelta_Phi*180.0/TMath::Pi()>>rasterYvsDTheta(200,0,30,100,-5,5)","","colz");
      //nnt->Draw("fDelta_Energy:fBigcalPlaneEnergy>>deltaenergyXY(200,0,5900,200,0,1000)","","colz");
      c->cd(5);
      nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fBigcalPlaneEnergy>>deltathetaXY(200,0,5900,200,-20,20)","","colz");
      c->cd(6);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fBigcalPlaneEnergy>>deltaphiXY(200,0,5900,200,-20,50)","","colz");
      c->cd(7);
      nnt->Draw("fDelta_Energy:fDelta_Theta*180.0/TMath::Pi()>>deltaenergyEEVsDTheta(200,-15,15,200,0,1000)","","colz");
      c->cd(8);
      nnt->Draw("fBigcalPlaneY:fDelta_Theta*180.0/TMath::Pi()>>bigcalXvsDTheta(100,-15,15,100,-150,150)","","colz");
      //nnt->Draw("fDelta_Energy:fDelta_Phi*180.0/TMath::Pi()>>EEVsDPhi(200,-20,20,200,0,1000)","","colz");
   }
   if(c)  c->SaveAs(Form("results/neural_networks/para/InspectNN_electron_anglecorrection-%d.png",number));


   return 0;

return(0);
}
