/*!  Builds the  neural network
 */
Int_t profile_positrons(Int_t number=5) {
   const char * networkName = "ParaPositronCorrections";
   const char * networkType = "PositronCorrections";
   const char * networkClassName = Form("NNPara%s",networkType);  

   Bool_t plotInputDataFirst = true; 

   if (!gROOT->GetClass("TMultiLayerPerceptron")) {
      gSystem->Load("libMLP");
   }

   Int_t ntrain  = 100;
   Int_t nHidden = 41;

   TH2F * h2 = 0;
   TProfile * prof = 0;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName),"READ");
   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   ANNDisElectronEvent * event = new ANNDisElectronEvent();
   nnt->SetBranchAddress("NNeEvents",&event);

   if(plotInputDataFirst){
      TCanvas* c = new TCanvas("NNinputCanvas","Network analysis",800,600);
      c->Divide(4,2);
      c->cd(1);
      nnt->Draw("fYCluster:fXCluster>>clusterXY(200,-60,60,200,-120,120)","","colz",1000000000,1001);
      c->cd(2);
      nnt->Draw("fYClusterSigma:fXClusterSigma>>sigmaXY(200,0,5,200,0,5)","","colz",1000000000,1001);
      c->cd(3);
      nnt->Draw("fYClusterSkew:fXClusterSkew>>skewXY(200,-15,15,200,-15,15)","","colz",1000000000,1001);
      c->cd(4);
      nnt->Draw("fDelta_Energy:fClusterEnergy>>deltaenergyXY(200,0,5900,200,-500,2000)","","colz,goff",1000000000,1001);
      h2 = (TH2F*) gROOT->FindObject("deltaenergyXY");
      prof = h2->ProfileX();
      prof->Draw();
      c->cd(5);
      nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fClusterEnergy>>deltathetaXY(200,0,5900,200,-20,20)","","colz,goff",1000000000,1001);
      h2 = (TH2F*) gROOT->FindObject("deltathetaXY");
      prof = h2->ProfileX();
      prof->Draw();
      c->cd(6);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fClusterEnergy>>deltaphiXY(200,0,5900,200,-20,50)","","colz,goff",1000000000,1001);
      h2 = (TH2F*) gROOT->FindObject("deltaphiXY");
      prof = h2->ProfileX();
      prof->Draw();
      c->cd(7);
      nnt->Draw("fDelta_Energy:fDelta_Theta*180.0/TMath::Pi()>>deltaenergyEEVsDTheta(200,-2,8,200,0,1000)","","colz",1000000000,1001);
      c->cd(8);
      nnt->Draw("fDelta_Energy:fDelta_Phi*180.0/TMath::Pi()>>EEVsDPhi(200,-20,0,200,0,1000)","","colz",1000000000,1001);

   }
/*   
   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",800,600);
   mlpa_canvas->Divide(3,3);
   mlpa_canvas->cd(3);

   const Int_t fgNInputNeurons = event->GetNCorrectionInputNeurons();

   std::cout << " input string has " << fgNInputNeurons << " neurons : \n";
   std::cout << event->GetCorrectionMLPInputNeuronsNormalized() << "\n";
   mlp = new TMultiLayerPerceptron(Form(
      "%s:%d:fDelta_Energy,fDelta_Theta,fDelta_Phi", event->GetCorrectionMLPInputNeuronsNormalized(),nHidden ),nnt);

   //   mlp->SetLearningMethod(TMultiLayerPerceptron::kStochastic); // sucks
   //   mlp->SetLearningMethod(TMultiLayerPerceptron::kSteepestDescent);
   //   mlp->SetLearningMethod(TMultiLayerPerceptron::kBatch);
   mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???

   mlp->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   mlp->Export(networkClassName);
   mlp->DumpWeights(Form("%s.txt",networkClassName));

   //   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",800,600);
   //   mlpa_canvas->Divide(2,2);
   TMLPAnalyzer ana(mlp);
   // Initialisation
   ana.GatherInformations();
   // output to the console
   ana.CheckNetwork();
   mlpa_canvas->cd(1);
   // shows how each variable influences the network
   ana.DrawDInputs();
   mlpa_canvas->cd(2);
   // shows the network structure
   mlp->Draw();
   mlpa_canvas->cd(4);

   TH1F *bg = new TH1F("bgh", "NN output", 50, -.5, 1.5);
   TH1F *sig = new TH1F("sigh", "NN output", 50, -.5, 1.5);
   bg->SetDirectory(0);
   sig->SetDirectory(0);

   TH2F *energies = new TH2F("energy_diff_vs_energy", "Energy Diff (NN - Thrown) ", 200, 0.0, 3000.0,200,-1000.0 , 1000.0);
   energies->SetDirectory(0);


   /// differences 
   TH1F *energy_cor = new TH1F("energy cor", "Energy  (NN - Thrown) ", 200, -500.0, 500.0);
   TH1F *theta_cor = new TH1F("theta cor", "#theta  (NN - Thrown) ", 200, -10.0, 10.0);
   TH1F *phi_cor = new TH1F("phi cor", "#phi  (NN - Thrown) ", 200, -30.0, 30.0);
   /// differences predicted
   TH1F *energy_diff = new TH1F("energy diff", "#Delta Energy  test ", 200, -20.0, 1000.0);
   TH1F *theta_diff = new TH1F("theta diff", "#Delta #theta test  ", 200, -10.0, 10.0);
   TH1F *phi_diff = new TH1F("phi diff", "#Delta #phi  test", 200, -30.0, 30.0);

   energy_diff->SetDirectory(0);
   theta_diff->SetDirectory(0);
   phi_diff->SetDirectory(0);

   Double_t * params = new Double_t[fgNInputNeurons];

   for (int i = 0; i < nnt->GetEntries(); i++) {
      nnt->GetEntry(i);
      // "fClusterEnergy,fXCluster,fYCluster,fXClusterSigma,fYClusterSigma,fXClusterSkew,fYClusterSkew,fGoodCherenkovHit,fCherenkovADC:%d:3:fBackground"
      event->GetCorrectionMLPInputNeuronArray(params);

   //  std::cout << " delta: energy = " << delta_energy_NN << " , theta = " << delta_theta_NN << " , phi = " << delta_phi_NN << "\n";

      Double_t delta_energy_NN = mlp->Evaluate(0,params);
      Double_t delta_theta_NN  = mlp->Evaluate(1,params);
      Double_t delta_phi_NN    = mlp->Evaluate(2,params);

      Double_t cEnergy = event->fClusterEnergy + delta_energy_NN ;
      Double_t cTheta  = delta_theta_NN ;
      Double_t cPhi    = delta_phi_NN ;

      sig->Fill(event->fSignal);
      bg->Fill(event->fBackground);

      energy_cor->Fill( cEnergy  - event->fTrueEnergy );
      theta_cor->Fill( (cTheta  - event->fDelta_Theta)/0.0175 );
      phi_cor->Fill( (cPhi  - event->fDelta_Phi)/0.0175 );

      energy_diff->Fill( delta_energy_NN );
      theta_diff->Fill( (delta_theta_NN)/0.0175 );
      phi_diff->Fill( (delta_phi_NN)/0.0175 );



   }

   


   bg->SetLineColor(kBlue);
   bg->SetFillStyle(3008);   bg->SetFillColor(kBlue);
   sig->SetLineColor(kRed);
   sig->SetFillStyle(3003); sig->SetFillColor(kRed);
   bg->SetStats(0);
   sig->SetStats(0);


   mlpa_canvas->cd(4);
   energy_cor->Draw();
   mlpa_canvas->cd(7);
   energy_diff->SetLineColor(2);
   energy_diff->Draw();

   mlpa_canvas->cd(5);
   theta_cor->Draw();
   mlpa_canvas->cd(8);
   theta_diff->SetLineColor(2);
   theta_diff->Draw();

   mlpa_canvas->cd(6);
   phi_cor->Draw();
   mlpa_canvas->cd(9);
   phi_diff->SetLineColor(2);
   phi_diff->Draw();

// OBSERVATION :
// NN does better without the maximum block positions (xy_max)
// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

mlpa_canvas->Update();

   mlpa_canvas->SaveAs(Form("results/neural_networks/%s%d.png",networkClassName,nHidden));
   mlpa_canvas->SaveAs(Form("results/neural_networks/%s%d.pdf",networkClassName,nHidden));

   std::cout << mlp->GetStructure().Data() << std::endl;*/

return(0);
}
