/*!  Builds the  neural network
 */
Int_t TrainNN_electron_ID4(Int_t number=45) {
   if(!gROOT->GetClass("TMultiLayerPerceptron"))  gSystem->Load("libMLP");

   /// Define the network 
   Int_t ntrain    = 500;
   Int_t nHidden   = 11;
   Int_t nHidden2  = 3;
   Int_t nOutput   = 1;
   Bool_t plotInputDataFirst = true; 
   Int_t trialN    = 1;

   /// Class names and such
   const char * networkType = "ElectronID4";
   const char * networkName = Form("Para59%s",networkType);
   const char * networkClassName = Form("NNPara59%sT%d",networkType,trialN);
   TString fname = Form("results/neural_networks/%s_%d-%d_%d_trial-%d",
		        networkClassName, nHidden, nHidden2, ntrain,trialN);

   /// Open the file with the network name and get the tree by "number"
   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName),"READ");
   if(!file){ std::cerr << "Error: could not open file.\n"; return(-1);}
   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   if(!nnt){std::cerr << "Error: nnt tree not found.\n"; return(-1);}
   std::cout << " Training Tree has " << nnt->GetEntries() << " entries.\n";
   
   /// ANN Event class used for input and training tree building. 
   ANNDisElectronEvent3 * event = new ANNDisElectronEvent3();
   nnt->SetBranchAddress("NNeEvents",&event);
   TString   fInputLayout      = event->GetMLPInputNeurons() ;
   const int fgNInputNeurons = event->GetNIDInputNeurons();
   std::cout << "Network the neuron structure : " 
	     << fgNInputNeurons << " <> " 
	     << nHidden << " <> " 
	     << nHidden2 << " <> "
	     << nOutput  << std::endl;


   if(plotInputDataFirst){
	   std::cout << " Plotting input data... ";
      TCanvas* c = new TCanvas("NNinputCanvas","Network analysis");
      c->Divide(3,2);
      c->cd(1);
      nnt->Draw("fYCluster:fXCluster>>clusterXY(200,-60,60,200,-120,120)","","colz",1000000000,1001);
      c->cd(2);
      nnt->Draw("fTrackerDeltaY:fTrackerDeltaX>>trackerDeltaXY(100,-5,5,100,-5,5)","","colz");
      c->cd(3);
      nnt->Draw("fTrackerDeltaY:fBackground>>trackerDeltaXY2(2,0,2,100,-5,5)","fClusterEnergy<1000","colz");
      c->cd(4);
      nnt->Draw("fTrackerDeltaX:fClusterEnergy>>deltaenergyXY(100,0,3000,200,-10,10)","","colz",1000000000,1001);
      c->cd(5);
      nnt->Draw("fTrackerDeltaY:fClusterEnergy>>deltathetaXY(100,0,3000,200,-10,10)","","colz",1000000000,1001);
      c->cd(6);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fClusterEnergy>>deltaphiXY(200,0,5900,200,-20,50)","","colz",1000000000,1001);
      c->SaveAs(Form("%s_InputData.png",fname.Data() ) );
      std::cout << " done.\n";
   }
   
   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(3,2);
   mlpa_canvas->cd(5);
   if(nHidden2==0)  mlp = new TMultiLayerPerceptron ( Form(
                              "%s:%d:fBackground",
                              fInputLayout.Data(),nHidden),nnt);
   else  mlp = new TMultiLayerPerceptron ( Form(
                           "%s:%d:%d:fBackground",
                           fInputLayout.Data(),nHidden,nHidden2),nnt);
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kStochastic); // sucks
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kSteepestDescent);
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kBatch);
   mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???

   mlp->Train(ntrain, "text,graph,current,update=10"); // add graph to string for plot
   mlp->Export(networkClassName);

//   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",800,600);
//   mlpa_canvas->Divide(2,2);
   TMLPAnalyzer ana(mlp);
   // Initialisation
   ana.GatherInformations();
   // output to the console
   ana.CheckNetwork();
   mlpa_canvas->cd(1);
   // shows how each variable influences the network
   ana.DrawDInputs();
   mlpa_canvas->cd(2);
   // shows the network structure
   mlp->Draw();
   mlpa_canvas->cd(3);
   // draws the resulting network
   ana.DrawNetwork(0,"fBackground==0","fBackground==1");
   mlpa_canvas->cd(4);
   gPad->SetLogy(1);
//   ana.DrawTruthDeviations();

  // ANNDisElectronEvent2 * event = new ANNDisElectronEvent2();
   nnt->SetBranchAddress("NNeEvents",&event);
   TH1F *bg = new TH1F("bgh", "NN output", 50, -.5, 1.5);
   TH1F *sig = new TH1F("sigh", "NN output", 50, -.5, 1.5);
   TH1F *bg2 = new TH1F("bgh2", "NN output", 50, -.5, 1.5);
   TH1F *sig2 = new TH1F("sigh2", "NN output", 50, -.5, 1.5);
   bg->SetDirectory(0);
   bg2->SetDirectory(0);
   sig->SetDirectory(0);
   sig2->SetDirectory(0);
   TH1F *bg3 = new TH1F("bgh3", "NN output", 50, -.5, 1.5);
   TH1F *sig3 = new TH1F("sigh3", "NN output", 50, -.5, 1.5);
   bg3->SetDirectory(0);
   sig3->SetDirectory(0);
   Double_t * params = new Double_t[fgNInputNeurons];
   for (int i = 0; i < nnt->GetEntries(); i++) {
      nnt->GetEntry(i);
      // "fClusterEnergy,fXCluster,fYCluster,fXClusterSigma,fYClusterSigma,fXClusterSkew,fYClusterSkew,fGoodCherenkovHit,fCherenkovADC:%d:3:fBackground"
      event->GetMLPInputNeuronArray(params);
   //   std::cout << " " << params[0] << "   " << event->fBackground << " \n";
      if(event->fBackground)
         bg->Fill(mlp->Evaluate(0,params));
      if(!(event->fBackground))
         sig->Fill(mlp->Evaluate(0,params));

      if(event->fClusterEnergy<800){
         if(event->fBackground)
            bg2->Fill(mlp->Evaluate(0,params));
         if(!(event->fBackground))
            sig2->Fill(mlp->Evaluate(0,params));
      }
      if(event->fClusterEnergy>800 && event->fClusterEnergy<1200){
         if(event->fBackground)
            bg3->Fill(mlp->Evaluate(0,params));
         if(!(event->fBackground))
            sig3->Fill(mlp->Evaluate(0,params));
      }
   }
   bg->SetLineColor(kBlue);
   bg->SetFillStyle(3008);   bg->SetFillColor(kBlue);
   sig->SetLineColor(kRed);
   sig->SetFillStyle(3003); sig->SetFillColor(kRed);
   bg->SetStats(0);
   sig->SetStats(0);
   bg->Draw();
   sig->Draw("same");
   TLegend *legend = new TLegend(.75, .80, .95, .95);
   legend->AddEntry(bg, " Background events ");
   legend->AddEntry(sig, " DIS e- ");
   legend->Draw();

   mlpa_canvas->cd(6);
   gPad->SetLogy(1);
   bg2->SetLineColor(3000);
   //bg2->SetFillStyle(3008);   bg2->SetFillColor(3000);
   sig2->SetLineColor(3005);
   //sig2->SetFillStyle(3003); sig2->SetFillColor(3001);
   bg2->SetStats(0);
   sig2->SetStats(0);
   bg2->Draw();
   sig2->Draw("same");

   bg3->SetLineColor(kGreen);
   //bg3->SetFillStyle(3008);   bg3->SetFillColor(kGreen);
   sig3->SetLineColor(kOrange);
   //sig3->SetFillStyle(3003); sig3->SetFillColor(kOrange);
   bg3->SetStats(0);
   sig3->SetStats(0);
   bg3->Draw("same");
   sig3->Draw("same");
   TLegend *legend2 = new TLegend(.75, .80, .95, .95);
   legend2->AddEntry(bg2, " Background events E<1000 ");
   legend2->AddEntry(sig2, " DIS e- E<1000 ");
   legend2->AddEntry(bg3, " Background events E>1000 && E<1500 ");
   legend2->AddEntry(sig3, " DIS e- E>1000 && E<1500 ");
   legend2->Draw();


   /// Plot resulting energy dependence on another canvas
   TCanvas * c2 = new TCanvas("NN_edep","Network Energy Dependence");
   //gPad->SetLogy(1);
   bg->DrawCopy();
   sig->DrawCopy("same");
   bg2->DrawCopy("same");
   sig2->DrawCopy("same");
   bg3->DrawCopy("same");
   sig3->DrawCopy("same");
   TLegend *legend3 = new TLegend(.75, .80, .95, .95);
   legend3->AddEntry(bg, " Background events  ");
   legend3->AddEntry(sig, " DIS e- ");
   legend3->AddEntry(bg2, " Background events E<800 ");
   legend3->AddEntry(sig2, " DIS e- E<800 ");
   legend3->AddEntry(bg3, " Background events E>800 && E<1200 ");
   legend3->AddEntry(sig3, " DIS e- E>800 && E<1200 ");
   legend3->Draw();

   c2->SaveAs(Form("%s_Edep.png",fname.Data() ) );

   // OBSERVATION :
   // NN does better without the maximum block positions (xy_max)
   // Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

   mlpa_canvas->Update();
   
   mlpa_canvas->SaveAs(Form("%s.svg",fname.Data() ) );
   mlpa_canvas->SaveAs(Form("%s.png",fname.Data() ) );
   mlpa_canvas->SaveAs(Form("%s.pdf",fname.Data() ) );


return(0);
}
