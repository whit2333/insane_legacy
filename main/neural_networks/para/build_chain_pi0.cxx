/*!   Adds the events to the tree; 
 */
Int_t build_chain_pi0(Int_t number=10) {
   const char * networkName = "para59pi0";
   const char * signalParticle = "pi0";

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue("neural_networks/para/para5.9_betag4_runs.txt");  	

   TChain * piChain = aman->BuildChain("pi0results");
   TChain * aChain = aman->BuildChain("betaDetectors1");

   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   aChain->BuildIndex("fRunNumber","fEventNumber");
   piChain->AddFriend("betaDetectors1");

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

  // if(fOutTree) fOutTree->Branch("pi0reconstruction","Pi0Event",&fPi0Event);

   BIGCALCluster * aCluster = new BIGCALCluster;
   Pi0ClusterPair * aPair = new Pi0ClusterPair();
   BETAG4MonteCarloEvent * MCevent = events->MC;
   BETAG4MonteCarloThrownParticle * thrown = new BETAG4MonteCarloThrownParticle();
   InSANEFakePlaneHit * bigcalPlane = new InSANEFakePlaneHit(); 
   Pi0Event * pi0Event = new Pi0Event();
   InSANEReconstruction * recon = new InSANEReconstruction();
   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   piChain->SetBranchAddress("pi0reconstruction",&pi0Event);

   ANNPi0TwoGammaEvent * piEvent = new ANNPi0TwoGammaEvent();
   piEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"RECREATE");
   TTree * nnt = 0;
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNpi0Events", "ANNPi0TwoGammaEvent", &piEvent);
   //piChain->StartViewer();

   for (int i = 0;i< piChain->GetEntries();i++ ) {
      if( i%1000 == 0 )std::cout << i << " \n";
      piChain->GetEntry(i);
      Int_t res = aChain->GetEntryWithIndex(pi0Event->fRunNumber,pi0Event->fEventNumber);
      //std::cout << res << " ... checking index by event number ... " <<  pi0Event->fEventNumber << "  vs  " <<  events->fEventNumber << " diff \n";
      if( pi0Event->fClusterPairs->GetEntries() > 0) { 
         //std::cout << " " << pi0Event->fClusterPairs->GetEntries() << " pairs \n";
         if( bigcalPlaneHits->GetEntries() > 1 ){
            //std::cout << " " << bigcalPlaneHits->GetEntries() << " bigcal plane hits \n";
            /// only 1 thrown particle !!!
	    thrown = (BETAG4MonteCarloThrownParticle*)(*thrownParticles)[0];
	    for(int iPair = 0; iPair < pi0Event->fClusterPairs->GetEntries(); iPair++) {   
               aPair = (Pi0ClusterPair*)(*pi0Event->fClusterPairs)[iPair];
               recon = (InSANEReconstruction*)(*pi0Event->fReconstruction)[iPair];
	       piEvent->fClusterPair = aPair;
	       //for(int iHit = 0; iHit < bigcalPlaneHits->GetEntries(); iHit++) {   
               //aCluster  = (BIGCALCluster*)(*clusters)[iClust];
	       //bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iHit];
	       TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
               /*piEvent->fDelta_Energy = thrown->Pt()*1000.0 - aCluster->GetEnergy();
               piEvent->fDelta_Theta = p.Theta() - aCluster->GetTheta();
               piEvent->fDelta_Phi =  p.Phi() - aCluster->GetPhi();
               piEvent->fTrueEnergy = thrown->Pt()*1000.0;
	       piEvent->fTrueTheta  = p.Theta();
	       piEvent->fTruePhi    = p.Phi();*/
	       piEvent->fClustersMass = recon->fMass;
	       piEvent->fClustersOpeningAngle = recon->fAngle;
	       piEvent->fEGamma1 = recon->fE1;
	       piEvent->fEGamma2 = recon->fE2;
               piEvent->fNClusters = clusters->GetEntries();
	       if( thrown->GetPdgCode() == signalPID ) piEvent->fSignal = true;
	       else disEvent->fSignal = false;
  	       nnt->Fill();
            }
         } 
      }
   }

   nnt->FlushBaskets();
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   nnt->StartViewer();
   
   return(0);
}
