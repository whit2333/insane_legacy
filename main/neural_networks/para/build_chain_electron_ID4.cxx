/*! Builds a chain of the simulation runs for ANN training.

    Uses all to identify electrons

 */
Int_t build_chain_electron_ID4(Int_t number=45) {

   const char * networkName = "Para59ElectronID4";
   const char * signalParticle = "e-";

   /// Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue("neural_networks/para/para_uniform_all.txt");  	
   //aman->BuildQueue("neural_networks/para/para_uniform_wTracker.txt");  	
   //aman->BuildQueue("neural_networks/para/para_uniform_100k.txt");  	

   /// Get The chain
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );
   aChain->BuildIndex("fRunNumber","fEventNumber");

   /// Get the Tracks chain
   TChain * t = aman->BuildChain("Tracks");
   if(!t) return(-1);
   InSANEDISTrajectory * fTrajectory = new InSANEDISTrajectory();
   t->BuildIndex("triggerEvent.fRunNumber","triggerEvent.fEventNumber");
   t->SetBranchAddress("trajectory",&fTrajectory);

   /// Get electron pdg code
   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle, " << signalParticle << ", has a Pdg code of " << signalPID << "\n";

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloEvent * MCevent = events->MC;
   BETAG4MonteCarloThrownParticle * thrown = new BETAG4MonteCarloThrownParticle();
   InSANEFakePlaneHit * bigcalPlane = 0; 

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   ANNDisElectronEvent3 * disEvent = new ANNDisElectronEvent3();
   disEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"UPDATE");
   NNFile->Print();
   gROOT->pwd();
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   NNFile->cd();
   nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   if(nnt) nnt->Delete("all");// delete frojm memory and disk.
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNDisElectronEvent3", &disEvent);
 
 
   Int_t entrynum = 0;
   Int_t byteread = 0;
   std::cout << " Tracks Chain has " << t->GetEntries() << ".\n";
   for (int i = 0;i< t->GetEntries();i++ ) {
      if( i%1000 == 0 )std::cout << i << " \n";
      t->GetEntry(i);

      entrynum = aChain->GetEntryNumberWithIndex(fTrajectory->fRunNumber,fTrajectory->fEventNumber);
      byteread = aChain->GetEntry(entrynum);
//      std::cout << "           run = " << events->fRunNumber << " , event = " << events->fEventNumber << "\n";
//      std::cout << "trajectory run = " << fTrajectory->fRunNumber << " , event = " << fTrajectory->fEventNumber << "\n";

      if(!(byteread>0) ) continue;

      if( clusters->GetEntries() > 0 ){

      thrown = (BETAG4MonteCarloThrownParticle*)(*thrownParticles)[0];

      /// Correct for bad geometries...
      disEvent->fTrackerDeltaX = fTrajectory->fTrackerMiss.X() -(  2.96041+(6.29484e-05*fTrajectory->fPosition0.X()) );
      disEvent->fTrackerDeltaY = fTrajectory->fTrackerMiss.Y() -( 0.0498615+(-0.285813*fTrajectory->fPosition0.Y()) );

      for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {   
	 aCluster  = (BIGCALCluster*)(*clusters)[iClust];
	 //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));

	 TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
	 disEvent->SetEventValues(aCluster); 
         disEvent->fDelta_Energy = thrown->P()*1000.0 - aCluster->GetEnergy();
         disEvent->fDelta_Theta = p.Theta() - aCluster->GetTheta();
         disEvent->fDelta_Phi =  p.Phi() - aCluster->GetPhi();
         disEvent->fTrueEnergy = thrown->P()*1000.0;
	 disEvent->fTrueTheta  = p.Theta();
	 disEvent->fTruePhi    = p.Phi();
         //disEvent->fNClusters = clusters->GetEntries();
	 if( thrown->GetPdgCode() == signalPID ) disEvent->fBackground = false;
	 else disEvent->fBackground = true;
         disEvent->fSignal = !disEvent->fBackground;

	 if(aCluster->GetEnergy() < 1000.0 )nnt->Fill();
      } 
      }
   }

   NNFile->cd();
   //nnt->Write();
   nnt->FlushBaskets();

   //NNFile->Flush();
   NNFile->Write();

//  nnt->StartViewer();
   
   return(0);
}
