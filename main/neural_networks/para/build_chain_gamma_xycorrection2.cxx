/*! Builds a chain of the simulation runs for ANN training.
 
    Fills a tree of ANNGammaEvent events, which include the following inputs
    - Cluster Energy
    - X and Y cluster positions
    - X and Y sigma
    - X and Y skewness
    - X and Y kurtosis
    The (possible) outputs:
    - fClusterDeltaX
    - fClusterDeltaY
    - fTrueEnergy
    - fTruePhi
    - fTrueTheta
    - signal/background

    Requirements for good gamma event:
    - pid thrown = gamma 
    - 0 < DeltaEnergy < 800 MeV
    - phi > 2 degrees (this avoids bremsstrahlung photons from target being treated as e-)

 */
Int_t build_chain_gamma_xycorrection2(Int_t number=16) {

   const char * networkName = "ParaGammaEXYCorrection";
   const char * signalParticle = "gamma";

   /// Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   //aman->BuildQueue2("lists/para/MC/mc_list_1.txt");  	
   //aman->fRunQueue->push_back(2061);
   //aman->fRunQueue->push_back(2062);
   //aman->fRunQueue->push_back(502);
   //aman->fRunQueue->push_back(600);
   //aman->fRunQueue->push_back(601);
   //aman->fRunQueue->push_back(800);
   //aman->fRunQueue->push_back(821);
   //aman->fRunQueue->push_back(824);
   //aman->fRunQueue->push_back(825);
   //aman->fRunQueue->push_back(826);
   //aman->fRunQueue->push_back(827);
   //aman->fRunQueue->push_back(828);
   //aman->fRunQueue->push_back(829);
   //aman->fRunQueue->push_back(830);

   //aman->fRunQueue->push_back(6072);
   //aman->fRunQueue->push_back(832);
   //aman->fRunQueue->push_back(833);
   //aman->fRunQueue->push_back(834);
   for(int i = 3100;i<=3120; i++) {
      aman->fRunQueue->push_back(i);
   }

   /// Get chain of runs
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster                  * aCluster    = new BIGCALCluster;
   BETAG4MonteCarloEvent          * MCevent     = events->MC;
   InSANEParticle * thrown      = new InSANEParticle();
   InSANEFakePlaneHit             * bigcalPlane = 0;

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   ANNGammaEvent * annEvent = new ANNGammaEvent();
   annEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"UPDATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNgammaEvents", "ANNGammaEvent", &annEvent);
   //aChain->StartViewer();
   //return(0);
   std::cout << " Chain has " << aChain->GetEntries() << ".\n";
   for (int i = 0;i< aChain->GetEntries();i++ ) {
      if( i%1000 == 0 ) {
         std::cout << "\r" << i ;
         std::cout.flush();
      }
      aChain->GetEntry(i);
      if( clusters->GetEntries() > 0 ){
         //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
         // only 1 thrown particle !!!
         thrown = (InSANEParticle*)(*thrownParticles)[0];

         for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {
            aCluster  = (BIGCALCluster*)(*clusters)[iClust];
            //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
            //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
            for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
               bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];

               //TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
               annEvent->SetEventValues(aCluster); 

               annEvent->fDelta_Energy = bigcalPlane->fEnergy - aCluster->GetEnergy();
               annEvent->fDelta_Theta  = bigcalPlane->fTheta - aCluster->GetTheta();
               annEvent->fDelta_Phi    = bigcalPlane->fPhi   - aCluster->GetPhi();
               annEvent->fTrueEnergy = bigcalPlane->fEnergy;
               annEvent->fTrueTheta  = bigcalPlane->fTheta;
               annEvent->fTruePhi    = bigcalPlane->fPhi ;

               annEvent->fXClusterKurt = aCluster->fXKurtosis;
               annEvent->fYClusterKurt = aCluster->fYKurtosis;

               annEvent->fClusterDeltaX = bigcalPlane->fLocalPosition.X() - aCluster->fXMoment;
               annEvent->fClusterDeltaY = bigcalPlane->fLocalPosition.Y() - aCluster->fYMoment;
               annEvent->fNClusters = clusters->GetEntries();

               //annEvent->fRasterX    = thrown->Vx();
               //annEvent->fRasterY    = thrown->Vy();


               if( bigcalPlane->fEnergy > 400.0 )
                  if( ( (thrown->GetPdgCode() == signalPID) && (TMath::Abs(thrown->Energy()*1000.0 - annEvent->fTrueEnergy) < 10.0 ) )
                        || (thrown->GetPdgCode() != signalPID) )
                     if( annEvent->fDelta_Energy < 800.0 && annEvent->fDelta_Energy > 0.0 )
                     if( TMath::Abs(annEvent->fDelta_Phi/degree  ) < 1.0)
                     if( TMath::Abs(annEvent->fDelta_Theta/degree) < 0.5)
                        if( bigcalPlane->fPID == signalPID)
                           //if( thrown->GetPdgCode() == signalPID )
                           {
                              //std::cout << " -----------------------------" << std::endl;
                              //std::cout << " E clust   : " << aCluster->GetEnergy() << std::endl;
                              //std::cout << " E plane   : " << bigcalPlane->fEnergy << std::endl;
                              //std::cout << " E thrown  : " << thrown->Energy()*1000.0 << std::endl;
                              annEvent->fBackground = false;
                              nnt->Fill();
                           }

            } // bigcal plane loop
         } // cluster loop

      }
   }
   std::cout << "\n";
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   //nnt->StartViewer();
   //gROOT->ProcessLine(".q");
   return(0);
}

