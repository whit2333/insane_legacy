/*! Builds a chain of the simulation runs for ANN training.
 
    Fills a tree of ANNDisElectronEvent5 events, which include the following inputs
    - Cluster Energy
    - X and Y cluster positions
    - X and Y sigma
    - X and Y skewness
    - X and Y kurtosis
    The (possible) outputs:
    - fClusterDeltaX
    - fClusterDeltaY
    - fTrueEnergy
    - fTruePhi
    - fTrueTheta
    - signal/background

    Requirements for good electron event:
    - pid thrown = e-
    - 0 < DeltaEnergy < 800 MeV
    - phi > 2 degrees (this avoids bremsstrahlung photons from target being treated as e-)

 */
Int_t build_chain_electron_xycorrection_v2(Int_t number=402) {

   const char * networkName = "PerpElectronXYCorrection";
   const char * signalParticle = "e-";

   // Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   //aman->BuildQueue2("lists/Perp/MC/mc_list_2.txt");  	
   //aman->fRunQueue->push_back(30042);
   //aman->fRunQueue->push_back(30043);

   for(int i=3300;i<3400;i++) {
      aman->fRunQueue->push_back(i);
   }


   // Get chain of runs
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster                  * aCluster    = new BIGCALCluster;
   BETAG4MonteCarloEvent          * MCevent     = events->MC;
   InSANEParticle * thrown         = new InSANEParticle();
   InSANEFakePlaneHit             * bigcalPlane = 0;

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   ANNDisElectronEvent6 * disEvent = new ANNDisElectronEvent6();
   disEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"UPDATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNDisElectronEvent6", &disEvent);
   //aChain->StartViewer();
   //return(0);
   std::cout << " Chain has " << aChain->GetEntries() << ".\n";
   for (int i = 0;i< aChain->GetEntries() && i< 50000;i++ ) {
      if( i%1000 == 0 ) {
         std::cout << "\r" << i ;
         std::cout.flush();
      }
      aChain->GetEntry(i);
      if( clusters->GetEntries() > 0 ){
         //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
         // only 1 thrown particle !!!
         thrown = (InSANEParticle*)(*thrownParticles)[0];
         //thrown->Dump();

         for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {
            aCluster  = (BIGCALCluster*)(*clusters)[iClust];

            for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
               bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];


               Double_t delta_en = bigcalPlane->fEnergy - aCluster->GetEnergy();
               // coefficient to scale the cluser energy so that it matches the actual energy
               Double_t  a_E =  bigcalPlane->fEnergy/aCluster->GetEnergy();

               // This shouldn't change the various moments.
               aCluster->fTotalE = aCluster->fTotalE*a_E;

               disEvent->SetEventValues(aCluster); 

               disEvent->fDelta_Energy = bigcalPlane->fEnergy - aCluster->GetEnergy();
               disEvent->fDelta_Theta  = bigcalPlane->fTheta - aCluster->GetTheta();
               disEvent->fDelta_Phi    = bigcalPlane->fPhi   - aCluster->GetPhi();

               disEvent->fTrueEnergy = bigcalPlane->fEnergy;
               disEvent->fTrueTheta  = bigcalPlane->fTheta;
               disEvent->fTruePhi    = bigcalPlane->fPhi ;

               disEvent->fXClusterKurt = aCluster->fXKurtosis;
               disEvent->fYClusterKurt = aCluster->fYKurtosis;

               disEvent->fClusterDeltaX = bigcalPlane->fLocalPosition.X() - aCluster->fXMoment;
               disEvent->fClusterDeltaY = bigcalPlane->fLocalPosition.Y() - aCluster->fYMoment;
               disEvent->fNClusters     = clusters->GetEntries();

               disEvent->fRasterX    = thrown->Vx();
               disEvent->fRasterY    = thrown->Vy();

               TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
               double delta_Phi    = p.Phi()   - aCluster->GetPhi();
               //disEvent->SetEventValues(aCluster); 

               //disEvent->fDelta_Energy = bigcalPlane->fEnergy*1000.0 - aCluster->GetEnergy();
               //disEvent->fDelta_Theta  = p.Theta() - aCluster->GetTheta();

               //disEvent->fTrueEnergy = bigcalPlane->fEnergy*1000.0;
               //disEvent->fTrueTheta  = p.Theta();
               //disEvent->fTruePhi    = p.Phi() ;

               //disEvent->fXClusterKurt = aCluster->fXKurtosis;
               //disEvent->fYClusterKurt = aCluster->fYKurtosis;

               //disEvent->fClusterDeltaX = bigcalPlane->fLocalPosition.X() - aCluster->fXMoment;
               //disEvent->fClusterDeltaY = bigcalPlane->fLocalPosition.Y() - aCluster->fYMoment;
               //disEvent->fNClusters = clusters->GetEntries();


               //std::cout << "cluster Energy = " << aCluster->fTotalE           << " MeV " << std::endl;
               //std::cout << "plane   Energy = " << bigcalPlane->fEnergy << " MeV " << std::endl;
               //std::cout << "thrown  Energy = " << thrown->Energy()*1000.0     << " MeV " << std::endl;
               //std::cout << " -----------------------------" << std::endl;

               if( bigcalPlane->fEnergy > 400.0 )
                  if(  (thrown->GetPdgCode() == signalPID) &&
                       (TMath::Abs(thrown->Energy()*1000.0 - disEvent->fTrueEnergy) < 40.0 ))
                     if( delta_en < 1000.0 && delta_en > 0.0 )
                     //if( delta_Phi/degree  > 0.0)
                     //if( TMath::Abs(disEvent->fDelta_Theta/degree) < 0.5)
                        if( bigcalPlane->fPID == signalPID)
                           //if( thrown->GetPdgCode() == signalPID )
                           {
               //std::cout << " -----------------------------" << std::endl;
               //std::cout << " E clust   : " << aCluster->GetEnergy() << std::endl;
               //std::cout << " E plane   : " << bigcalPlane->fEnergy << std::endl;
               //std::cout << " E thrown  : " << thrown->Energy()*1000.0 << std::endl;

               //if( TMath::Abs(thrown->Energy()*1000.0 - bigcalPlane->fEnergy) < 400.0 )
               //   if( disEvent->fDelta_Energy < 1000.0 /*&& disEvent->fDelta_Energy > 0.0*/ )
               //      if( disEvent->fDelta_Phi/0.0175 > 2.0)
               //         if( bigcalPlane->fPID == signalPID)
               //            if( thrown->GetPdgCode() == signalPID ){

                              disEvent->fBackground = false;
                              nnt->Fill();
                           }

            } // bigcal plane loop
         } // cluster loop

      }
   }
   std::cout << "\n";
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   std::cout << "Entries : " << nnt->GetEntries() << std::endl;
   //nnt->StartViewer();
   //gROOT->ProcessLine(".q");
   return(0);
}

