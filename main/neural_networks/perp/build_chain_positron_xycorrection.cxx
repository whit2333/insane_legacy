/*! Builds a chain of the simulation runs for ANN training.

    Uses all to identify electrons

 */
Int_t build_chain_positron_xycorrection(Int_t number=515) {

   const char * networkName = "Perp59PositronXYCorrection";
   const char * signalParticle = "e+";

   /// Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("neural_networks/perp/perp_uniform_all.txt");  	
//    aman->GetRunQueue()->push_back(4422); // 1405 -- 10k with min energy increased to 400MeV

   /// Get chain of runs
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloEvent * MCevent = events->MC;
   BETAG4MonteCarloThrownParticle * thrown = new BETAG4MonteCarloThrownParticle();
   InSANEFakePlaneHit * bigcalPlane = 0; 

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   ANNDisElectronEvent5 * disEvent = new ANNDisElectronEvent5();
   disEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"UPDATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNDisElectronEvent5", &disEvent);
   //aChain->StartViewer();

   std::cout << " Chain has " << aChain->GetEntries() << ".\n";
   for (int i = 0;i< aChain->GetEntries();i++ ) {
      if( i%1000 == 0 ) {
          std::cout << "\r" << i ;
          std::cout.flush();
      }
      aChain->GetEntry(i);
      if( clusters->GetEntries() > 0 ){
     //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
     // only 1 thrown particle !!!
	 thrown = (BETAG4MonteCarloThrownParticle*)(*thrownParticles)[0];
 
      for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {
	 aCluster  = (BIGCALCluster*)(*clusters)[iClust];
	 //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
	 //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
         for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
            bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];

	 TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
	 disEvent->SetEventValues(aCluster); 

         disEvent->fDelta_Energy = bigcalPlane->fEnergy - aCluster->GetEnergy();
         disEvent->fDelta_Theta  = p.Theta() - aCluster->GetTheta();
         disEvent->fDelta_Phi    = p.Phi()   - aCluster->GetPhi();
         disEvent->fTrueEnergy = bigcalPlane->fEnergy;
	 disEvent->fTrueTheta  = p.Theta();
	 disEvent->fTruePhi    = p.Phi() ;
	 disEvent->fClusterDeltaX = bigcalPlane->fLocalPosition.X() - aCluster->fXMoment;
         disEvent->fClusterDeltaY = bigcalPlane->fLocalPosition.Y() - aCluster->fYMoment;

         disEvent->fXClusterKurt = aCluster->fXKurtosis;
         disEvent->fYClusterKurt = aCluster->fYKurtosis;


         //          disEvent->fDelta_Energy = thrown->P()*1000.0 - aCluster->GetEnergy();
//          disEvent->fDelta_Theta = p.Theta() - aCluster->GetTheta();
//          disEvent->fDelta_Phi =  p.Phi() - aCluster->GetPhi();
//          disEvent->fTrueEnergy = thrown->P()*1000.0;
// 	 disEvent->fTrueTheta  = p.Theta();
// 	 disEvent->fTruePhi    = p.Phi();
         disEvent->fNClusters = clusters->GetEntries();

/*         if(disEvent->fDelta_Energy < 2000.0)*/
/*         if(disEvent->fDelta_Phi > 4.0*0.017)*/
   
	    if( disEvent->fDelta_Energy < 800.0 && disEvent->fDelta_Energy > 0.0 )
	    //if( disEvent->fDelta_Phi/0.0175 > 2.0)
            if( bigcalPlane->fPID == signalPID)
            if( thrown->GetPdgCode() == signalPID ){
	       disEvent->fBackground = false;
               nnt->Fill();
	    }

         } // bigcal plane loop
      } // cluster loop

   }
   }
   std::cout << "\n";
   std::cout << nnt->GetEntries() << " entries in new tree\n";
   nnt->FlushBaskets();
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   //nnt->StartViewer();
   gROOT->ProcessLine(".q");
   return(0);
}
