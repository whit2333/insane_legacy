/*!  Builds the  neural network for correcting the momentum direction at bigcal.
 */
Int_t TrainNN_electron_BCPDir3(Int_t number=650) {

   TString networkName = "PerpElectronAngleCorrection";
   TString networkType = "ElectronAngleCorrection";
   TString networkClassName = Form("NNPerp%s",networkType.Data());  
   TString networkClassName2 = networkClassName;

   Int_t ntrain   = 2000;
   Int_t nHidden  = 59;
   Int_t nHidden2 = 0;
   Bool_t plotInputDataFirst = true; 

   ANNElectronThruFieldEvent3 * event = new ANNElectronThruFieldEvent3();
   const Int_t fgNInputNeurons = event->GetNInputNeurons();

   std::cout << " Network Class Name " << networkClassName2.Data() << " .\n";
   std::cout << " Input string has " << fgNInputNeurons << " neurons : \n";
   std::cout << event->GetMLPInputNeurons() << "\n";

   if (!gROOT->GetClass("TMultiLayerPerceptron")) {
      gSystem->Load("libMLP");
   }

   TMultiLayerPerceptron * mlp = 0;
   TMultiLayerPerceptron * mlp_bcpdir = 0;
   TMultiLayerPerceptron * mlp_bcpdir2 = 0;
   TMultiLayerPerceptron * mlp_theta = 0;
   TMultiLayerPerceptron * mlp_phi = 0;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName.Data()),"READ");

   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   nnt->SetBranchAddress("NNeEvents",&event);
   std::cout << " Tree has " << nnt->GetEntries() << " entries." << std::endl;

   TCanvas * c = 0;

   if(plotInputDataFirst){
      c = new TCanvas("NNinputCanvas","Network analysis");
      c->Divide(4,3);
      c->cd(1);
      nnt->Draw("fBigcalPlaneY:fBigcalPlaneX>>clusterXY(200,-100,100,200,-150,150)","","colz");
      c->cd(2);
      nnt->Draw("fBigcalPlanePhi*180.0/TMath::Pi():fBigcalPlaneTheta*180.0/TMath::Pi()>>bcThetaPhi(200,20,60,200,-50,50)","","colz");
      c->cd(3);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fDelta_Theta*180.0/TMath::Pi()>>skewXY(200,-15,15,200,-20,20)","","colz");
      c->cd(4);
      nnt->Draw("fDelta_Energy:fBigcalPlaneDeltaPPhi/0.0175>>deltaenergyXY(200,-1,1,200,0,1000)","","colz");
      c->cd(5);
      nnt->Draw("fBigcalPlaneDeltaPTheta*180.0/TMath::Pi():fBigcalPlaneEnergy>>deltathetaXY(200,0,5900,200,-1,1)","","colz");
      c->cd(6);
      nnt->Draw("fBigcalPlaneDeltaPPhi*180.0/TMath::Pi():fBigcalPlaneEnergy>>deltaphiXY(200,0,5900,200,-1,1)","","colz");
      c->cd(7);
      nnt->Draw("fTrueEnergy:fBigcalPlaneDeltaPPhi*180.0/TMath::Pi()>>deltaenergyEEVsDTheta(200,-1,1,200,0,4000)","","colz");
      c->cd(8);
      nnt->Draw("fDelta_Energy:fDelta_Phi*180.0/TMath::Pi()>>EEVsDPhi(200,-20,20,200,0,1000)","","colz");
      c->cd(9);
      nnt->Draw("fRasterX:fBigcalPlaneDeltaPPhi*180.0/TMath::Pi()>>RastXVsDPhi(200,-0.5,0.5,100,-2,2)","","colz");
      c->cd(10);
      nnt->Draw("fRasterY:fBigcalPlaneDeltaPPhi*180.0/TMath::Pi()>>RastYVsDPhi(200,-0.5,0.5,100,-2,2)","","colz");
      c->cd(11);
      nnt->Draw("fRasterX:fBigcalPlaneDeltaPTheta*180.0/TMath::Pi()>>RastXVsDTheta(200,-0.5,0.5,100,-2,2)","","colz");
      c->cd(12);
      nnt->Draw("fRasterY:fBigcalPlaneDeltaPTheta*180.0/TMath::Pi()>>RastYVsDTheta(200,-0.5,0.5,100,-2,2)","","colz");
   }
   nnt->StartViewer();
   //return(0);

   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(3,3);
   mlpa_canvas->cd(1)->Divide(3,1);
   mlpa_canvas->cd(2)->Divide(3,1);
   mlpa_canvas->cd(3)->Divide(3,1);

   TCanvas * c1 = new TCanvas("c1","BCPDir Network");
   c1->Divide(2,3);

   TCanvas * c2 = new TCanvas("c2","BCP Phi Network");
   c2->Divide(2,2);

   //TCanvas * c3 = new TCanvas("c3","Phi Network");
   //c3->Divide(2,2);

   /// Momentum Direction at BigCal Plane 
   mlpa_canvas->cd(1)->cd(3);
   c1->cd(1);
   gPad->SetLogy(true);
   if( nHidden2 < 1 ){
      mlp_bcpdir = new TMultiLayerPerceptron(Form(
         "%s:%d:fBigcalPlaneDeltaPTheta",
         event->GetMLPInputNeurons(),
         nHidden  ),nnt);
   } else {
      mlp_bcpdir = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fBigcalPlaneDeltaPTheta",
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2  ),nnt);
   }
   mlp_bcpdir->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   mlp_bcpdir->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName2.Data() << "BCPDirTheta (.txt,.cxx,.h) will be created " << "\n\n";
   mlp_bcpdir->Export(Form("neural_networks/networks/%sBCPDirTheta",networkClassName.Data()));
   mlp_bcpdir->DumpWeights(Form("neural_networks/networks/%sBCPDirTheta.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_energy(mlp_bcpdir);
   ana_energy.GatherInformations();
   ana_energy.CheckNetwork();
   //mlpa_canvas->cd(1)->cd(1);
   c1->cd(2);
   ana_energy.DrawDInputs();
   //mlpa_canvas->cd(1)->cd(2);
   c1->cd(3);
   mlp_bcpdir->Draw();

   c2->cd(1);
   gPad->SetLogy(true);
   if( nHidden2 < 1 ){
      mlp_bcpdir2 = new TMultiLayerPerceptron(Form(
         "%s:%d:fBigcalPlaneDeltaPPhi",
         event->GetMLPInputNeurons(),
         nHidden  ),nnt);
   } else {
      mlp_bcpdir2 = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fBigcalPlaneDeltaPPhi",
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2  ),nnt);
   }
   mlp_bcpdir2->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   mlp_bcpdir2->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName2.Data() << "BCPDirPhi (.txt,.cxx,.h) will be created " << "\n\n";
   mlp_bcpdir2->Export(Form("neural_networks/networks/%sBCPDirPhi",networkClassName.Data()));
   mlp_bcpdir2->DumpWeights(Form("neural_networks/networks/%sBCPDirPhi.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_phi(mlp_bcpdir2);
   ana_phi.GatherInformations();
   ana_phi.CheckNetwork();
   //mlpa_canvas->cd(1)->cd(1);
   c2->cd(2);
   ana_phi.DrawDInputs();
   //mlpa_canvas->cd(1)->cd(2);
   c2->cd(3);
   mlp_bcpdir2->Draw();


   ///
   TH1F * hEnergyDiff          = new TH1F("hEnergyDiff", "Energy  (NN - Thrown) ", 100, -200.0, 1000.0);
   TH1F * hDeltaThetaDiff      = new TH1F("hDeltaThetaDiff", "#theta  (NN - Thrown) ", 100, -1.0, 1.0);
   TH1F * hDeltaPhiDiff        = new TH1F("hDeltaPhiDiff", "#phi  (NN - Thrown) ;#Delta #phi [deg]", 100, -2.0, 2.0);
   /// input
   TH1F * hEnergyInput         = new TH1F("hEnergyInput", "#Delta Energy  (Thrown);#Delta E [MeV]", 100, -200.0, 1000.0);
   TH1F * hDeltaThetaInput     = new TH1F("hDeltaThetaInput", "#Delta #theta  (Thrown); #Delta #theta [deg] ", 100, -1.0, 1.0);
   TH1F * hDeltaPhiInput       = new TH1F("y_input", "#Delta #phi  (Thrown) ;#Delta #phi [deg]", 100, -2.0, 2.0);
   /// ouput predicted
   TH1F * hEnergyOutput        = new TH1F("hEnergyOutput", "#Delta Energy  (NN) ", 100, -200.0, 1000.0);
   TH1F * hDeltaThetaOutput    = new TH1F("hDeltaThetaOutput", "#Delta #theta (NN) ", 100, -1.0, 1.0);
   TH1F * hDeltaPhiOutput      = new TH1F("hDeltaPhiOutput", "#Delta #phi  (NN);#Delta #phi [deg]", 100, -2.0, 2.0);

   TH2F * hEnergyOutputVsY     = new TH2F("hEnergyOutputVsY", " EnergyOutput Vs Y", 100, -200.0, 1000.0,200,-150.0,150.0);
   TH2F * hDeltaThetaOutputVsY = new TH2F("hDeltaThetaOutputVsY", " ThetaOutput Vs Y", 100, -1.0, 1.0,200,-150.0,150.0);
   TH2F * hDeltaPhiOutputVsY   = new TH2F("hDeltaPhiOutputVsY", " ThetaOutput Vs Y", 100, -1.0, 5.0,200,-150.0,150.0);


   Double_t * params = new Double_t[fgNInputNeurons];
   Double_t radtodegree = 180.0/TMath::Pi();

   /// Loop over tree and get the corrections
   for (int i = 0; i < nnt->GetEntries(); i++) {

      nnt->GetEntry(i);
      event->GetMLPInputNeuronArray(params);

      Double_t delta_energy_NN    = 0;//mlp_bcpdir->Evaluate(0,params);
      Double_t delta_theta_NN     = mlp_bcpdir->Evaluate(0,params);
      Double_t delta_phi_NN       = mlp_bcpdir2->Evaluate(0,params);

      Double_t cEnergy     = event->fBigcalPlaneEnergy + delta_energy_NN ;
      Double_t cTheta      = event->fBigcalPlaneTheta + delta_theta_NN ;
      Double_t cPhi        = event->fBigcalPlanePhi + delta_phi_NN ;
      Double_t cX          = event->fBigcalPlaneX;
      Double_t cY          = event->fBigcalPlaneY;

      hEnergyDiff->Fill( delta_energy_NN  - event->fDelta_Energy );
      hDeltaThetaDiff->Fill( ( delta_theta_NN  - event->fBigcalPlaneDeltaPTheta)*radtodegree );
      hDeltaPhiDiff->Fill( ( delta_phi_NN  - event->fBigcalPlaneDeltaPPhi)*radtodegree );

      hEnergyInput->Fill(  event->fDelta_Energy );
      hDeltaThetaInput->Fill( ( event->fBigcalPlaneDeltaPTheta)*radtodegree);
      hDeltaPhiInput->Fill( ( event->fBigcalPlaneDeltaPPhi)*radtodegree );

      hEnergyOutput->Fill(  delta_energy_NN );
      hDeltaThetaOutput->Fill(       delta_theta_NN*radtodegree );
      hDeltaPhiOutput->Fill(       delta_phi_NN*radtodegree );

      hEnergyOutputVsY->Fill( delta_energy_NN ,cY);
      hDeltaThetaOutputVsY->Fill( delta_theta_NN *radtodegree ,cY);
      hDeltaPhiOutputVsY->Fill( delta_phi_NN*radtodegree  ,cY);

   }

   //mlpa_canvas->cd(4);

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);

   c1->cd(5);
   gPad->SetLogy(true);
   hDeltaThetaDiff->SetLineColor(1);
   hDeltaThetaInput->SetLineColor(2);
   hDeltaThetaOutput->SetLineColor(4);
   hDeltaThetaOutput->Draw();
   hDeltaThetaDiff->Draw("same");
   hDeltaThetaInput->Draw("same");
   leg->AddEntry(hDeltaThetaInput,"input","l");
   leg->AddEntry(hDeltaThetaOutput,"output","l");
   leg->AddEntry(hDeltaThetaDiff,"difference","l");
   leg->Draw();
   c1->cd(6);
   gPad->SetLogy(true);
   hDeltaPhiDiff->SetLineColor(1);
   hDeltaPhiInput->SetLineColor(2);
   hDeltaPhiOutput->SetLineColor(4);
   hDeltaPhiOutput->Draw();
   hDeltaPhiDiff->Draw("same");
   hDeltaPhiInput->Draw("same");
   leg->Draw();

   mlpa_canvas->cd(7);
   hEnergyOutputVsY->Draw("colz");


   mlpa_canvas->cd(9);
   hDeltaPhiOutputVsY->Draw("colz");

// OBSERVATION :
// NN does better without the maximum block positions (xy_max)
// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

   mlpa_canvas->Update();
   mlpa_canvas->SaveAs(Form("results/neural_networks/perp/%s-%d-%d_%d.png",networkClassName2.Data(),nHidden,nHidden2,number));
   mlpa_canvas->SaveAs(Form("results/neural_networks/perp/%s-%d-%d_%d.pdf",networkClassName2.Data(),nHidden,nHidden2,number));
   if(c)c->SaveAs(Form("results/neural_networks/perp/%s-input_%d.png",networkClassName2.Data(),number));
   if(c1)c1->SaveAs(Form("results/neural_networks/perp/%sBCPDir-%d-%d-c1_%d.png",networkClassName2.Data(),nHidden,nHidden2,number));
   if(c1)c1->SaveAs(Form("results/neural_networks/perp/%sBCPDir-%d-%d-c1_%d.pdf",networkClassName2.Data(),nHidden,nHidden2,number));

   return(0);
}
