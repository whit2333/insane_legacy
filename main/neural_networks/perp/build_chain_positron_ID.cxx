/*! Builds a chain of the simulation runs for ANN training.

    Uses all to identify electrons

 */
Int_t build_chain_positron_ID(Int_t number=503) {

   const char * networkName = "Perp59PositronID";
   const char * signalParticle = "e+";

   /// Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   aman->BuildQueue2("neural_networks/perp/perp_uniform_all.txt");  	
//    aman->GetRunQueue()->push_back(4422); // 1405 -- 10k with min energy increased to 400MeV

   /// 
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloEvent * MCevent = events->MC;
   BETAG4MonteCarloThrownParticle * thrown = new BETAG4MonteCarloThrownParticle();
   InSANEFakePlaneHit * bigcalPlane = 0; 

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   ANNDisElectronEvent * disEvent = new ANNDisElectronEvent();
   disEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"RECREATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNDisElectronEvent", &disEvent);
   //aChain->StartViewer();

   std::cout << " Chain has " << aChain->GetEntries() << ".\n";
   for (int i = 0;i< aChain->GetEntries();i++ ) {
      if( i%1000 == 0 )std::cout << i << " \n";
      aChain->GetEntry(i);
      if( clusters->GetEntries() > 0 ){
     //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
     // only 1 thrown particle !!!
	 thrown = (BETAG4MonteCarloThrownParticle*)(*thrownParticles)[0];
 
      for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {   
	 aCluster  = (BIGCALCluster*)(*clusters)[iClust];
	 //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));

	 TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
	 disEvent->SetEventValues(aCluster); 
         disEvent->fDelta_Energy = thrown->P()*1000.0 - aCluster->GetEnergy();
         disEvent->fDelta_Theta = p.Theta() - aCluster->GetTheta();
         disEvent->fDelta_Phi =  p.Phi() - aCluster->GetPhi();
         disEvent->fTrueEnergy = thrown->P()*1000.0;
	 disEvent->fTrueTheta  = p.Theta();
	 disEvent->fTruePhi    = p.Phi();
         disEvent->fNClusters = clusters->GetEntries();
	 if( thrown->GetPdgCode() == signalPID ) disEvent->fBackground = false;
	 else disEvent->fBackground = true;
         nnt->Fill();
      } 
      }
   }

//    nnt->FlushBaskets();
//    nnt->Write();
//    NNFile->Flush();
//    NNFile->Write();

   //nnt->StartViewer();
   
   return(0);
}
