/*!  Builds the  neural network to get the bigcal plane position and energy
     as corrections to the cluster position and energy.

 */
Int_t TrainNN_electron_xycorrection4(Int_t number=450) {

   if (!gROOT->GetClass("TMultiLayerPerceptron")) gSystem->Load("libMLP");

   TString networkName       = "PerpElectronXYCorrection";
   TString networkType       = "ElectronXYCorrection";
   TString networkClassName  = Form("NNPerp%s",networkType.Data());
   TString networkClassName2 = networkClassName;

   Int_t ntrain              = 500; 
   Int_t nHidden             = 50;
   Int_t nHidden2            = 0;
   Bool_t plotInputDataFirst = true;

   ANNDisElectronEvent6 * event = new ANNDisElectronEvent6();
   const Int_t fgNInputNeurons  = event->GetNInputNeurons();

   std::cout << "-----------------------------------" << std::endl;
   std::cout << "Network Class Name : " << networkClassName2.Data() << std::endl;
   std::cout << "Input string has " << event->GetNInputNeurons() << " neurons " << std::endl;
   std::cout << event->GetMLPInputNeurons() << std::endl;

   TMultiLayerPerceptron * mlp        = 0;
   TMultiLayerPerceptron * mlp_energy = 0;
   TMultiLayerPerceptron * mlp_x      = 0;
   TMultiLayerPerceptron * mlp_y      = 0;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName.Data()),"READ");

   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   nnt->SetBranchAddress("NNeEvents",&event);

   TCanvas * c = 0;
   if(plotInputDataFirst){
      c = new TCanvas("NNinputCanvas","Network analysis");
      c->Divide(4,3);
      c->cd(1);
      nnt->Draw("fYCluster:fXCluster>>clusterXY(100,-65,65,120,-120,120)","","colz");
      c->cd(2);
      nnt->Draw("fYClusterSigma:fXClusterSigma>>sigmaXY(100,0,3,100,0,3)","","colz");
      c->cd(3);
      nnt->Draw("fYClusterSkew:fXClusterSkew>>skewXY(100,-15,15,100,-15,15)","","colz");
      c->cd(4);
      nnt->Draw("fYClusterKurt:fXClusterKurt>>kurtXY(100,-3,15,100,-3,15)","","colz");
      c->cd(5);
      nnt->Draw("fClusterDeltaY:fClusterDeltaX>>deltaXY(100,-5,5,100,-5,5)","","colz");
      c->cd(6);
      nnt->Draw("fDelta_Energy:fClusterEnergy>>deltaenergyXY(100,0,5900,100,0,1000)","","colz");
      c->cd(7);
      nnt->Draw("fYClusterKurt:fClusterEnergy>>ykurtVSenergy(100,0,5900,100,-3,15)","","colz");
      c->cd(8);
      nnt->Draw("fXClusterKurt:fClusterEnergy>>xkurtVSenergy(100,0,5900,100,-3,15)","","colz");
      c->cd(9);
      nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fClusterEnergy>>deltathetaXY(100,0,5900,100,-6,6)","","colz");
      c->cd(10);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fClusterEnergy>>deltaphiXY(100,0,5900,100,-30,30)","","colz");
      c->cd(11);
      nnt->Draw("fDelta_Energy:fDelta_Theta*180.0/TMath::Pi()>>deltaenergyEEVsDTheta(100,-6,6,100,0,1000)","","colz");
      c->cd(12);
      //nnt->Draw("fDelta_Energy:fDelta_Phi*180.0/TMath::Pi()>>EEVsDPhi(100,-30,30,100,0,1000)","","colz");
      nnt->Draw("fDelta_Energy:fRasterY>>EEVsRasterX(100,-3,3,100,0,1000)","","colz");
   }
   //return(0);
   TCanvas * c1 = new TCanvas("c1","Energy Network");
   c1->Divide(2,2);
   TCanvas * c2 = new TCanvas("c2","X Network");
   c2->Divide(2,2);
   TCanvas * c3 = new TCanvas("c3","Y Network");
   c3->Divide(2,2);

   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(3,3);
   //mlpa_canvas->cd(1)->Divide(3,1);
   //mlpa_canvas->cd(2)->Divide(3,1);
   //mlpa_canvas->cd(3)->Divide(3,1);

   /// Energy Correction 
   //mlpa_canvas->cd(1)->cd(3);
   c1->cd(1);
   if( nHidden2 < 1 ){
      mlp_energy = new TMultiLayerPerceptron(Form(
         "%s:%d:fDelta_Energy",
         event->GetMLPInputNeurons(),
         nHidden  ),nnt);
   } else {
      mlp_energy = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fDelta_Energy",
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2  ),nnt);
   }
   mlp_energy->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   std::cout << " Training Energy Network...." << std::endl;
   mlp_energy->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   // output the results
   std::cout << networkClassName2.Data() << "Energy (.txt,.cxx,.h) will be created " << "\n";
   mlp_energy->Export(Form("neural_networks/networks/%sEnergy",networkClassName.Data()));
   mlp_energy->DumpWeights(Form("neural_networks/networks/%sEnergy.txt",networkClassName2.Data()));
   // analyze the network
   TMLPAnalyzer ana_energy(mlp_energy);
   ana_energy.GatherInformations();
   ana_energy.CheckNetwork();
   //mlpa_canvas->cd(1)->cd(1);
   c1->cd(2);
   ana_energy.DrawDInputs();
   // Annoying fix for white colored line.
   THStack * hs = (THStack*)gROOT->FindObject("differences");
   if( hs ) {
      TList * hl = hs->GetHists();
      if(hl->GetEntries()>9) {
         TH1 * hist = (TH1*)hl->At(9);
         if(hist)
            hist->SetLineColor(kRed -7);  
      }
   } else { std::cout << " could not find THStack named differences.\n"; }
   
   //mlpa_canvas->cd(1)->cd(2);
   c1->cd(3);
   mlp_energy->Draw();
   c1->Update();

   /// Cluster X Correction
   //mlpa_canvas->cd(2)->cd(3);
   c2->cd(1);
   if( nHidden2 < 1 ){
      mlp_x = new TMultiLayerPerceptron(Form(
         "%s:%d:fClusterDeltaX", 
         event->GetMLPInputNeurons(),
         nHidden ),nnt);
   } else {
      mlp_x = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fClusterDeltaX", 
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2 ),nnt);
   }
   mlp_x->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   std::cout << " Training ClusterDeltaX Network...." << std::endl;
   mlp_x->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName << "ClusterDeltaX (.txt,.cxx,.h) will be created " << "\n";
   mlp_x->Export(Form("neural_networks/networks/%sClusterDeltaX",networkClassName2.Data()));
   mlp_x->DumpWeights(Form("neural_networks/networks/%sClusterDeltaX.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_x(mlp_x);
   ana_x.GatherInformations();
   ana_x.CheckNetwork();
   //mlpa_canvas->cd(2)->cd(1);
   c2->cd(2);
   ana_x.DrawDInputs();
   // Annoying fix for white colored line.
   hs = (THStack*)gROOT->FindObject("differences");
   if( hs ) {
      TList * hl = hs->GetHists();
      if(hl->GetEntries()>9) {
         TH1 * hist = (TH1*)hl->At(9);
         if(hist)
            hist->SetLineColor(kRed -7);  
      }
   } else { std::cout << " could not find THStack named differences.\n"; }
   
   //mlpa_canvas->cd(2)->cd(2);
   c2->cd(3);
   mlp_x->Draw();

   /// Cluster Y Correction
   //mlpa_canvas->cd(3)->cd(3);
   c3->cd(1);
   if( nHidden2 < 1 ){
      mlp_y = new TMultiLayerPerceptron(Form(
         "%s:%d:fClusterDeltaY", 
         event->GetMLPInputNeurons(),
         nHidden ),nnt);
   } else {
      mlp_y = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fClusterDeltaY", 
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2 ),nnt);
   }
   mlp_y->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   std::cout << " Training ClusterDeltaY Network...." << std::endl;
   mlp_y->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName2.Data() << "ClusterDeltaY (.txt,.cxx,.h) will be created " << "\n";
   mlp_y->Export(Form("neural_networks/networks/%sClusterDeltaY",networkClassName2.Data()));
   mlp_y->DumpWeights(Form("neural_networks/networks/%sClusterDeltaY.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_y(mlp_y);
   ana_y.GatherInformations();
   ana_y.CheckNetwork();
   //mlpa_canvas->cd(3)->cd(1);
   c3->cd(2);
   ana_y.DrawDInputs();
   // Annoying fix for white colored line.
   hs = (THStack*)gROOT->FindObject("differences");
   if( hs ) {
      TList * hl = hs->GetHists();
      if(hl->GetEntries()>9) {
         TH1 * hist = (TH1*)hl->At(9);
         if(hist)
            hist->SetLineColor(kRed -7);  
      }
   } else { std::cout << " could not find THStack named differences.\n"; }
   
   //mlpa_canvas->cd(3)->cd(2);
   c3->cd(3);
   mlp_y->Draw();


   /// Energy+Theta+Phi Corrections
   //mlp = new TMultiLayerPerceptron(Form(
   //    "%s:%d:fDelta_Energy,fDelta_Theta,fDelta_Phi", event->GetCorrectionMLPInputNeurons(),nHidden ),nnt);
   //mlp->SetLearningMethod(TMultiLayerPerceptron::kStochastic); // sucks
   //mlp->SetLearningMethod(TMultiLayerPerceptron::kSteepestDescent);
   //mlp->SetLearningMethod(TMultiLayerPerceptron::kBatch);
   //mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   //
   //mlp->Train(ntrain, "text,graph,current,update=10"); // add graph to string for plot
   //mlp->Export(networkClassName);
   //TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",800,600);
   //mlpa_canvas->Divide(2,2);
   //TMLPAnalyzer ana(mlp);
   //ana.GatherInformations();
   //ana.CheckNetwork();
   //mlpa_canvas->cd(1);
   //ana.DrawDInputs();
   //mlpa_canvas->cd(2);
   //mlp->Draw();
   // draws the resulting network
   //ana.DrawNetwork(0,"fBackground==0","fBackground==1");
   //mlpa_canvas->cd(5);
   //ana.DrawNetwork(1,"fBackground==0","fBackground==1");
   //mlpa_canvas->cd(6);

   TFile * resultFile = new TFile(Form("data/anns/electronXYcorrection%d.root",number),"UPDATE");
   resultFile->cd();
   TTree * resultTree = new TTree("NNelectronXYcorr","NN electron XYE correction training results"); 
   Double_t nnOut_DeltaE;
   Double_t nnOut_DeltaX;
   Double_t nnOut_DeltaY;
   Double_t nnDiff_DeltaE;
   Double_t nnDiff_DeltaX;
   Double_t nnDiff_DeltaY;
   resultTree->Branch("nnOut_DeltaE",&nnOut_DeltaE,"nnOut_DeltaE/D");
   resultTree->Branch("nnOut_DeltaX",&nnOut_DeltaX,"nnOut_DeltaX/D");
   resultTree->Branch("nnOut_DeltaY",&nnOut_DeltaY,"nnOut_DeltaY/D");
   resultTree->Branch("nnDiff_DeltaE",&nnDiff_DeltaE,"nnDiff_DeltaE/D");
   resultTree->Branch("nnDiff_DeltaX",&nnDiff_DeltaX,"nnDiff_DeltaX/D");
   resultTree->Branch("nnDiff_DeltaY",&nnDiff_DeltaY,"nnDiff_DeltaY/D");


   /// Histograms to check NN
   TH1F *bg = new TH1F("bgh", "NN output", 50, -.5, 1.5);
   TH1F *sig = new TH1F("sigh", "NN output", 50, -.5, 1.5);
   bg->SetDirectory(0);
   sig->SetDirectory(0);

   TH2F *energies = new TH2F("energy_diff_vs_energy", "Energy Diff (NN - Thrown) ", 200, 0.0, 3000.0,200,-1000.0 , 1000.0);
   energies->SetDirectory(0);

   /// differences 
   TH1F *energy_cor = new TH1F("energy_cor", "Energy  (NN - Thrown) ", 100, -400.0, 1000.0);
   TH1F *x_cor = new TH1F("x_cor", "x  (NN - Thrown) ", 100, -5.0, 5.0);
   TH1F *y_cor = new TH1F("y_cor", "y  (NN - Thrown) ", 100, -5.0, 5.0);
   /// input  
   TH1F *energy_input = new TH1F("energy_input", "#Delta Energy  (Thrown) ", 100, -400.0, 1000.0);
   TH1F *x_input = new TH1F("x_input", "#Delta x  (Thrown) ", 100, -5.0, 5.0);
   TH1F *y_input = new TH1F("y_input", "#Delta y  (Thrown) ", 100, -5.0, 5.0);
   /// ouput predicted
   TH1F *energy_diff = new TH1F("energy_diff", "#Delta Energy  (NN) ", 100, -400.0, 1000.0);
   TH1F *x_diff = new TH1F("x_diff", "#Delta x (NN) ", 100, -5.0, 5.0);
   TH1F *y_diff = new TH1F("y_diff", "#Delta y  (NN)", 100, -5.0, 5.0);
   /// differences input vs some variable
   TH2F *energy_in = new TH2F("energy_in", "#Delta Energy  test ", 100, 0.0, 3000.0, 100, -200.0, 1000.0);
   TH2F *theta_in = new TH2F("x_in", "#Delta x test  ",            100, 0.0, 3000.0, 100, -5.0, 5.0);
   TH2F *phi_in = new TH2F("y_in", "#Delta y  test",               100, 0.0, 3000.0, 100, -5.0, 5.0);

   /// differences vs Energy
   TH2F * energyDiffVsEnergy = new TH2F("energyDiffVsEnergy", "#Delta Energy  vs Energy;E [MeV];#Delta E", 100, 200, 3000.0, 100, -400.0, 600.0);
   TH2F * xDiffVsEnergy = new TH2F("xDiffVsEnergy", "#delta x  vs Energy;E [MeV];#delta x", 100, 200, 3000.0, 100, -3.0, 3.0);
   TH2F * yDiffVsEnergy = new TH2F("yDiffVsEnergy", "#delta y  vs Energy;E [MeV];#delta y", 100, 200, 3000.0, 100, -3.0, 3.0);
   /// differences vs X 
   TH2F * energyDiffVsX = new TH2F("energyDiffVsX", "#Delta Energy  vs X;x;#Delta E", 100, -120.0,120.0, 100, -400.0, 600.0);
   TH2F * xDiffVsX      = new TH2F("xDiffVsX", "#delta x  vs X; x;#delta x",          100, -120.0,120.0, 100, -3.0, 3.0);
   TH2F * yDiffVsX      = new TH2F("yDiffVsX", "#delta y  vs X; x;#delta y",          100, -120.0,120.0, 100, -3.0, 3.0);
   /// differences vs X 
   TH2F * energyDiffVsY = new TH2F("energyDiffVsY", "#Delta Energy  vs Y;y;#Delta E", 100, -120.0,120.0, 100, -400.0, 600.0);
   TH2F * xDiffVsY      = new TH2F("xDiffVsY", "#delta x  vs Y; y;#delta x",          100, -120.0,120.0, 100, -3.0, 3.0);
   TH2F * yDiffVsY      = new TH2F("yDiffVsY", "#delta y  vs Y; y;#delta y",          100, -120.0,120.0, 100, -3.0, 3.0);

   Double_t * params = new Double_t[fgNInputNeurons];

   /// Loop over tree and get the corrections
   for (int i = 0; i < nnt->GetEntries(); i++) {

      nnt->GetEntry(i);
      event->GetMLPInputNeuronArray(params);

      /// Extract values
      //std::cout << " delta: energy = " << delta_energy_NN 
      //          << " , theta = " << delta_theta_NN 
      //          << " , phi = " << delta_phi_NN << "\n";

      //Double_t delta_energy_NN = mlp->Evaluate(0,params);
      //Double_t delta_theta_NN  = mlp->Evaluate(1,params);
      //Double_t delta_phi_NN    = mlp->Evaluate(2,params);

      Double_t delta_energy_NN = mlp_energy->Evaluate(0,params);
      Double_t delta_x_NN      = mlp_x->Evaluate(0,params);
      Double_t delta_y_NN      = mlp_y->Evaluate(0,params);

      Double_t cEnergy = event->fClusterEnergy + delta_energy_NN ;
      Double_t cX      = event->fXCluster + delta_x_NN ;
      Double_t cY      = event->fYCluster + delta_y_NN ;

      sig->Fill(event->fSignal);
      bg->Fill(event->fBackground);

      // The network output error
      energy_diff->Fill( delta_energy_NN  - event->fDelta_Energy );
      x_diff->Fill( ( delta_x_NN  - event->fClusterDeltaX) );
      y_diff->Fill( ( delta_y_NN  - event->fClusterDeltaY) );

      // The training input 
      energy_input->Fill(  event->fDelta_Energy );
      x_input->Fill( ( event->fClusterDeltaX) );
      y_input->Fill( ( event->fClusterDeltaY) );

      // The network output
      energy_cor->Fill(  delta_energy_NN );
      x_cor->Fill(       delta_x_NN );
      y_cor->Fill(       delta_y_NN );

      energy_in->Fill( event->fClusterEnergy, delta_energy_NN );
      x_in->Fill(      event->fClusterEnergy, delta_x_NN );
      y_in->Fill(      event->fClusterEnergy, delta_y_NN );
      
      nnOut_DeltaE = delta_energy_NN;
      nnOut_DeltaX = delta_x_NN;
      nnOut_DeltaY = delta_y_NN;
      nnDiff_DeltaE = delta_energy_NN - event->fDelta_Energy;
      nnDiff_DeltaX = delta_x_NN - event->fClusterDeltaX;
      nnDiff_DeltaY = delta_y_NN - event->fClusterDeltaY;

      energyDiffVsEnergy->Fill( event->fClusterEnergy, nnDiff_DeltaE);
      xDiffVsEnergy->Fill(      event->fClusterEnergy, nnDiff_DeltaX);
      yDiffVsEnergy->Fill(      event->fClusterEnergy, nnDiff_DeltaY);

      energyDiffVsX->Fill( event->fXCluster, nnDiff_DeltaE);
      xDiffVsX->Fill(      event->fXCluster, nnDiff_DeltaX);
      yDiffVsX->Fill(      event->fXCluster, nnDiff_DeltaY);

      energyDiffVsY->Fill( event->fYCluster, nnDiff_DeltaE);
      xDiffVsY->Fill(      event->fYCluster, nnDiff_DeltaX);
      yDiffVsY->Fill(      event->fYCluster, nnDiff_DeltaY);

   }

   THStack * hs1 = new THStack();
   THStack * hs2 = new THStack();
   THStack * hs3 = new THStack();

   TLegend * leg = new TLegend(0.1,0.7,0.29,0.9);
   

   //mlpa_canvas->cd(4);
   c1->cd(4);
   gPad->SetLogy(true);
   energy_diff->SetLineColor(2);
   energy_input->SetLineColor(4);
   hs1->Add(energy_cor); 
   hs1->Add(energy_input); 
   hs1->Add(energy_diff);
   hs1->Draw("nostack");
   leg->AddEntry(energy_cor,"corrected output","l"); 
   leg->AddEntry(energy_input,"input","l"); 
   leg->AddEntry(energy_diff,"output-input","l"); 
   leg->Draw();

   mlpa_canvas->cd(1);
   energyDiffVsEnergy->Draw("colz");

   mlpa_canvas->cd(4);
   energyDiffVsX->Draw("colz");

   mlpa_canvas->cd(7);
   energyDiffVsY->Draw("colz");

   //mlpa_canvas->cd(7);
   //energy_in->Draw("colz");


   //mlpa_canvas->cd(5);
   c2->cd(4);
   gPad->SetLogy(true);
   x_diff->SetLineColor(2);
   x_input->SetLineColor(4);
   hs2->Add(x_cor);
   hs2->Add(x_input);
   hs2->Add(x_diff);
   //x_cor->Draw();
   //x_input->Draw("same");
   //x_diff->Draw("same");
   hs2->Draw("nostack");
   leg->Draw();
   
   mlpa_canvas->cd(2);
   xDiffVsEnergy->Draw("colz");

   mlpa_canvas->cd(5);
   xDiffVsX->Draw("colz");

   mlpa_canvas->cd(8);
   xDiffVsY->Draw("colz");

   //mlpa_canvas->cd(8);
   //x_in->Draw("colz");


   //mlpa_canvas->cd(6);
   c3->cd(4);
   gPad->SetLogy(true);
   y_diff->SetLineColor(2);
   y_input->SetLineColor(4);
   hs3->Add(y_cor);
   hs3->Add(y_input);
   hs3->Add(y_diff);
   //y_cor->Draw();
   //y_input->Draw("same");
   //y_diff->Draw("same");
   hs3->Draw("nostack");
   leg->Draw();
   
   mlpa_canvas->cd(3);
   yDiffVsEnergy->Draw("colz");

   mlpa_canvas->cd(6);
   yDiffVsX->Draw("colz");

   mlpa_canvas->cd(9);
   yDiffVsY->Draw("colz");

   //mlpa_canvas->cd(9);
   //y_in->Draw("colz");

// OBSERVATION :
// NN does better without the maximum block positions (xy_max)
// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

   resultFile->Write();


   mlpa_canvas->Update();
   mlpa_canvas->SaveAs(Form("results/neural_networks/perp/%s-%d-%d_%d.png",networkClassName2.Data(),nHidden,nHidden2,number));
   mlpa_canvas->SaveAs(Form("results/neural_networks/perp/%s-%d-%d_%d.pdf",networkClassName2.Data(),nHidden,nHidden2,number));
   if(c)  c->SaveAs(Form("results/neural_networks/perp/%s-input_%d.png",   networkClassName2.Data(),number));
   if(c1)c1->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-c1_%d.png",networkClassName2.Data(),nHidden,nHidden2,number));
   if(c2)c2->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-c2_%d.png",networkClassName2.Data(),nHidden,nHidden2,number));
   if(c3)c3->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-c3_%d.png",networkClassName2.Data(),nHidden,nHidden2,number));

   return(0);
}
