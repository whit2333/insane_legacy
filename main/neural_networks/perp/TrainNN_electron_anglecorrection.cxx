/*!  Builds the  neural network
 */
Int_t TrainNN_electron_anglecorrection(Int_t number=602) {
   const char * networkName = "Perp59ElectronAngleCorrection";
   const char * networkType = "ElectronAngleCorrection";
   const char * networkClassName = Form("NNPerp%s",networkType);  
   TString networkClassName2 = networkClassName;

   Int_t ntrain   = 200;
   Int_t nHidden  = 14;
   Int_t nHidden2 = 0;
   Bool_t plotInputDataFirst = true; 

   ANNElectronThruFieldEvent * event = new ANNElectronThruFieldEvent();
   const Int_t fgNInputNeurons = event->GetMLPInputNeurons();

   std::cout << " Network Class Name " << networkClassName2.Data() << " .\n";
   std::cout << "\n Input string has " << fgNInputNeurons << " neurons : \n";
   std::cout << event->GetMLPInputNeurons() << "\n";

   if (!gROOT->GetClass("TMultiLayerPerceptron")) {
      gSystem->Load("libMLP");
   }

   TMultiLayerPerceptron * mlp = 0;
   TMultiLayerPerceptron * mlp_energy = 0;
   TMultiLayerPerceptron * mlp_theta = 0;
   TMultiLayerPerceptron * mlp_phi = 0;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName),"READ");

   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   nnt->SetBranchAddress("NNeEvents",&event);

   TCanvas * c = 0;

   if(plotInputDataFirst){
      c = new TCanvas("NNinputCanvas","Network analysis");
      c->Divide(4,2);
      c->cd(1);
      nnt->Draw("fBigcalPlaneY:fBigcalPlaneX>>clusterXY(200,-100,100,200,-150,150)","","colz");
      c->cd(2);
      nnt->Draw("fBigcalPlanePhi*180.0/TMath::Pi():fBigcalPlaneTheta*180.0/TMath::Pi()>>bcThetaPhi(200,20,60,200,-50,50)","","colz");
      c->cd(3);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fDelta_Theta*180.0/TMath::Pi()>>skewXY(200,-15,15,200,-20,20)","","colz");
      c->cd(4);
      nnt->Draw("fDelta_Energy:fBigcalPlaneEnergy>>deltaenergyXY(200,0,5900,200,0,1000)","","colz");
      c->cd(5);
      nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fBigcalPlaneEnergy>>deltathetaXY(200,0,5900,200,-20,20)","","colz");
      c->cd(6);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fBigcalPlaneEnergy>>deltaphiXY(200,0,5900,200,-20,50)","","colz");
      c->cd(7);
      nnt->Draw("fDelta_Energy:fDelta_Theta*180.0/TMath::Pi()>>deltaenergyEEVsDTheta(200,-15,15,200,0,1000)","","colz");
      c->cd(8);
      nnt->Draw("fDelta_Energy:fDelta_Phi*180.0/TMath::Pi()>>EEVsDPhi(200,-20,20,200,0,1000)","","colz");
   }

   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(3,3);
   mlpa_canvas->cd(1)->Divide(3,1);
   mlpa_canvas->cd(2)->Divide(3,1);
   mlpa_canvas->cd(3)->Divide(3,1);

   TCanvas * c1 = new TCanvas("c1","Energy Network");
   c1->Divide(2,2);

   TCanvas * c2 = new TCanvas("c2","Theta Network");
   c2->Divide(2,2);

   TCanvas * c3 = new TCanvas("c3","Phi Network");
   c3->Divide(2,2);

   /// Energy 
   mlpa_canvas->cd(1)->cd(3);
   c1->cd(1);
   if( nHidden2 < 1 ){
      mlp_energy = new TMultiLayerPerceptron(Form(
         "%s:%d:fDelta_Energy",
         event->GetMLPInputNeurons(),
         nHidden  ),nnt);
   } else {
      mlp_energy = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fDelta_Energy",
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2  ),nnt);
   }
   mlp_energy->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   mlp_energy->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName2.Data() << "DeltaEnergy (.txt,.cxx,.h) will be created " << "\n\n";
   mlp_energy->Export(Form("neural_networks/networks/%sDeltaEnergy",networkClassName));
   mlp_energy->DumpWeights(Form("neural_networks/networks/%sDeltaEnergy.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_energy(mlp_energy);
   ana_energy.GatherInformations();
   ana_energy.CheckNetwork();
   //mlpa_canvas->cd(1)->cd(1);
   c1->cd(2);
   ana_energy.DrawDInputs();
   //mlpa_canvas->cd(1)->cd(2);
   c1->cd(3);
   mlp_energy->Draw();

   /// Theta
   //mlpa_canvas->cd(2)->cd(3);
   c2->cd(1);
   if( nHidden2 < 1 ){
      mlp_theta = new TMultiLayerPerceptron(Form(
         "%s:%d:fDelta_Theta", 
         event->GetMLPInputNeurons(),
         nHidden ),nnt);
   } else {
      mlp_theta = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fDelta_Theta", 
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2 ),nnt);
   }
   mlp_theta->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   mlp_theta->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName << "DeltaTheta (.txt,.cxx,.h) will be created " << "\n\n";
   mlp_theta->Export(Form("neural_networks/networks/%sDeltaTheta",networkClassName2.Data()));
   mlp_theta->DumpWeights(Form("neural_networks/networks/%sDeltaTheta.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_x(mlp_theta);
   ana_x.GatherInformations();
   ana_x.CheckNetwork();
   //mlpa_canvas->cd(2)->cd(1);
   c2->cd(2);
   ana_x.DrawDInputs();
   //mlpa_canvas->cd(2)->cd(2);
   c2->cd(3);
   mlp_theta->Draw();

   /// Cluster Y Correction
   //mlpa_canvas->cd(3)->cd(3);
   c3->cd(1);
   if( nHidden2 < 1 ){
      mlp_phi = new TMultiLayerPerceptron(Form(
         "%s:%d:fDelta_Phi", 
         event->GetMLPInputNeurons(),
         nHidden ),nnt);
   } else {
      mlp_phi = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fDelta_Phi", 
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2 ),nnt);
   }
   mlp_phi->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   mlp_phi->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName2.Data() << "DeltaPhi (.txt,.cxx,.h) will be created " << "\n\n";
   mlp_phi->Export(Form("neural_networks/networks/%sDeltaPhi",networkClassName2.Data()));
   mlp_phi->DumpWeights(Form("neural_networks/networks/%sDeltaPhi.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_y(mlp_phi);
   ana_y.GatherInformations();
   ana_y.CheckNetwork();
   //mlpa_canvas->cd(3)->cd(1);
   c3->cd(2);
   ana_y.DrawDInputs();
   //mlpa_canvas->cd(3)->cd(2);
   c3->cd(3);
   mlp_phi->Draw();


   /// Energy+Theta+Phi Corrections
//    mlp = new TMultiLayerPerceptron(Form(
//       "%s:%d:fDelta_Energy,fDelta_Theta,fDelta_Phi", event->GetCorrectionMLPInputNeurons(),nHidden ),nnt);
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kStochastic); // sucks
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kSteepestDescent);
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kBatch);
//    mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
// 
//    mlp->Train(ntrain, "text,graph,current,update=10"); // add graph to string for plot
//    mlp->Export(networkClassName);
//   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",800,600);
//   mlpa_canvas->Divide(2,2);
//    TMLPAnalyzer ana(mlp);
//    ana.GatherInformations();
//    ana.CheckNetwork();
//    mlpa_canvas->cd(1);
//    ana.DrawDInputs();
//    mlpa_canvas->cd(2);
//    mlp->Draw();
   // draws the resulting network
//    ana.DrawNetwork(0,"fBackground==0","fBackground==1");
//    mlpa_canvas->cd(5);
//    ana.DrawNetwork(1,"fBackground==0","fBackground==1");
//    mlpa_canvas->cd(6);


//    TH2F *energies = new TH2F("energy_diff_vs_energy", "Energy Diff (NN - Thrown) ", 200, 0.0, 3000.0,200,-1000.0 , 1000.0);
//    energies->SetDirectory(0);



   ///
   TH1F * hEnergyDiff = new TH1F("hEnergyDiff", "Energy  (NN - Thrown) ", 100, -200.0, 1000.0);
   TH1F * hDeltaThetaDiff = new TH1F("hDeltaThetaDiff", "#theta  (NN - Thrown) ", 100, -5.0, 5.0);
   TH1F * hDeltaPhiDiff = new TH1F("hDeltaPhiDiff", "#phi  (NN - Thrown) ", 100, -5.0, 20.0);
   /// input  
   TH1F * hEnergyInput = new TH1F("hEnergyInput", "#Delta Energy  (Thrown) ", 100, -200.0, 1000.0);
   TH1F * hDeltaThetaInput = new TH1F("hDeltaThetaInput", "#Delta #theta  (Thrown) ", 100, -5.0, 5.0);
   TH1F * hDeltaPhiInput = new TH1F("y_input", "#Delta #phi  (Thrown) ", 100, -5.0, 20.0);
   /// ouput predicted
   TH1F * hEnergyOutput = new TH1F("hEnergyOutput", "#Delta Energy  (NN) ", 100, -200.0, 1000.0);
   TH1F * hDeltaThetaOutput = new TH1F("hDeltaThetaOutput", "#Delta #theta (NN) ", 100, -5.0, 5.0);
   TH1F * hDeltaPhiOutput = new TH1F("hDeltaPhiOutput", "#Delta #phi  (NN)", 100, -5.0, 20.0);

   TH2F * hEnergyOutputVsY = new TH2F("hEnergyOutputVsY", " EnergyOutput Vs Y", 100, -200.0, 1000.0,200,-150.0,150.0);
   TH2F * hDeltaThetaOutputVsY = new TH2F("hDeltaThetaOutputVsY", " ThetaOutput Vs Y", 100, -5.0, 5.0,200,-150.0,150.0);
   TH2F * hDeltaPhiOutputVsY = new TH2F("hDeltaPhiOutputVsY", " ThetaOutput Vs Y", 100, -5.0, 20.0,200,-150.0,150.0);


   Double_t * params = new Double_t[fgNInputNeurons];
   Double_t radtodegree = 180.0/TMath::Pi();

   /// Loop over tree and get the corrections
   for (int i = 0; i < nnt->GetEntries(); i++) {

      nnt->GetEntry(i);
      event->GetMLPInputNeuronArray(params);

      /// Extract values
      //std::cout << " delta: energy = " << delta_energy_NN 
      //          << " , theta = " << delta_theta_NN 
      //          << " , phi = " << delta_phi_NN << "\n";

      //Double_t delta_energy_NN = mlp->Evaluate(0,params);
      //Double_t delta_theta_NN  = mlp->Evaluate(1,params);
      //Double_t delta_phi_NN    = mlp->Evaluate(2,params);

//       Double_t energy_NN    = mlp_energy->Evaluate(0,params);
//       Double_t theta_NN     = mlp_theta->Evaluate(0,params);
//       Double_t phi_NN       = mlp_phi->Evaluate(0,params);

      Double_t delta_energy_NN    = mlp_energy->Evaluate(0,params);
      Double_t delta_theta_NN     = mlp_theta->Evaluate(0,params);
      Double_t delta_phi_NN       = mlp_phi->Evaluate(0,params);

      Double_t cEnergy     = event->fBigcalPlaneEnergy + delta_energy_NN ;
      Double_t cTheta      = event->fBigcalPlaneTheta + delta_theta_NN ;
      Double_t cPhi        = event->fBigcalPlanePhi + delta_phi_NN ;
      Double_t cX          = event->fBigcalPlaneX;
      Double_t cY          = event->fBigcalPlaneY;

      hEnergyDiff->Fill( delta_energy_NN  - event->fDelta_Energy );
      hDeltaThetaDiff->Fill( ( delta_theta_NN  - event->fDelta_Theta)*radtodegree );
      hDeltaPhiDiff->Fill( ( delta_phi_NN  - event->fDelta_Phi)*radtodegree );

      hEnergyInput->Fill(  event->fDelta_Energy );
      hDeltaThetaInput->Fill( ( event->fDelta_Theta)*radtodegree);
      hDeltaPhiInput->Fill( ( event->fDelta_Phi)*radtodegree );

      hEnergyOutput->Fill(  delta_energy_NN );
      hDeltaThetaOutput->Fill(       delta_theta_NN*radtodegree );
      hDeltaPhiOutput->Fill(       delta_phi_NN*radtodegree );

      hEnergyOutputVsY->Fill( delta_energy_NN ,cY);
      hDeltaThetaOutputVsY->Fill( delta_theta_NN *radtodegree ,cY);
      hDeltaPhiOutputVsY->Fill( delta_phi_NN*radtodegree  ,cY);

   }

   //mlpa_canvas->cd(4);
   c1->cd(4);
   hEnergyDiff->SetLineColor(1);
   hEnergyInput->SetLineColor(2);
   hEnergyOutput->SetLineColor(4);
   hEnergyDiff->Draw();
   hEnergyInput->Draw("same");
   hEnergyOutput->Draw("same");

   mlpa_canvas->cd(7);
   hEnergyOutputVsY->Draw("colz");


   //mlpa_canvas->cd(5);
   c2->cd(4);
   hDeltaThetaDiff->SetLineColor(1);
   hDeltaThetaInput->SetLineColor(2);
   hDeltaThetaOutput->SetLineColor(4);
   hDeltaThetaDiff->Draw();
   hDeltaThetaInput->Draw("same");
   hDeltaThetaOutput->Draw("same");

   mlpa_canvas->cd(8);
   hDeltaThetaOutputVsY->Draw("colz");


   //mlpa_canvas->cd(6);
   c3->cd(4);
   hDeltaPhiDiff->SetLineColor(1);
   hDeltaPhiInput->SetLineColor(2);
   hDeltaPhiOutput->SetLineColor(4);
   hDeltaPhiDiff->Draw();
   hDeltaPhiInput->Draw("same");
   hDeltaPhiOutput->Draw("same");

   mlpa_canvas->cd(9);
   hDeltaPhiOutputVsY->Draw("colz");

// OBSERVATION :
// NN does better without the maximum block positions (xy_max)
// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

   mlpa_canvas->Update();
   mlpa_canvas->SaveAs(Form("results/neural_networks/%s-%d-%d.png",networkClassName2.Data(),nHidden,nHidden2));
   mlpa_canvas->SaveAs(Form("results/neural_networks/%s-%d-%d.pdf",networkClassName2.Data(),nHidden,nHidden2));
   if(c)c->SaveAs(Form("results/neural_networks/%s-input.png",networkClassName2.Data()));
   if(c1)c1->SaveAs(Form("results/neural_networks/%s-%d-%d-c1.png",networkClassName2.Data(),nHidden,nHidden2));
   if(c1)c1->SaveAs(Form("results/neural_networks/%s-%d-%d-c1.pdf",networkClassName2.Data(),nHidden,nHidden2));
   if(c2)c2->SaveAs(Form("results/neural_networks/%s-%d-%d-c2.png",networkClassName2.Data(),nHidden,nHidden2));
   if(c2)c2->SaveAs(Form("results/neural_networks/%s-%d-%d-c2.pdf",networkClassName2.Data(),nHidden,nHidden2));
   if(c3)c3->SaveAs(Form("results/neural_networks/%s-%d-%d-c3.png",networkClassName2.Data(),nHidden,nHidden2));
   if(c3)c3->SaveAs(Form("results/neural_networks/%s-%d-%d-c3.pdf",networkClassName2.Data(),nHidden,nHidden2));

return(0);
}
