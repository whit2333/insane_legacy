/*! Networks to correct for 

 */
Int_t build_chain_electron_anglecorrection_v2(Int_t number=757) {

   const char * networkName = "PerpElectronAngleCorrection";
   const char * signalParticle = "e-";

   // Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();

   for(int i=3300;i<3400;i++) {
      aman->fRunQueue->push_back(i);
   }

   // Get chain of runs
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloEvent * MCevent = events->MC;
   InSANEParticle * thrown = 0;
   InSANEFakePlaneHit * bigcalPlane = 0; 

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   TVector3 vertex;
   TVector3 p_naive;

   ANNElectronThruFieldEvent2 * nnEvent = new ANNElectronThruFieldEvent2();
   nnEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"UPDATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNElectronThruFieldEvent2", &nnEvent);

   std::cout << " Chain has " << aChain->GetEntries() << ".\n";

   Int_t nkeep = 0;
   for (int i = 0;i< aChain->GetEntries() ;i++ ) {

      if( i%1000 == 0 ) {
         std::cout << "\r" << i  << " " << nkeep;
         std::cout.flush();
      }
      aChain->GetEntry(i);
      if( clusters->GetEntries() > 0 ){

         // only 1 thrown particle
         thrown = (InSANEParticle*)(*thrownParticles)[0];

         // no clusters involved
         //for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {
         //   aCluster  = (BIGCALCluster*)(*clusters)[iClust];
               //nnEvent->SetEventValues(aCluster); 

            for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
               bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];

               // Only keep events that don't loose too much energy

               TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);

               nnEvent->fTrueEnergy             = thrown->Energy()*1000.0;
               nnEvent->fTrueTheta              = p.Theta();
               nnEvent->fTruePhi                = p.Phi() ;

               nnEvent->fRasterX                = thrown->Vx();
               nnEvent->fRasterY                = thrown->Vy();

               vertex.SetXYZ(thrown->Vx(),thrown->Vy(),0.0);//thrown->Vz());
               p_naive = bigcalPlane->fPosition - vertex; 

               nnEvent->fDelta_Energy           = thrown->Energy()*1000.0 - bigcalPlane->fEnergy;
               nnEvent->fDelta_Theta            = p.Theta() - p_naive.Theta();
               nnEvent->fDelta_Phi              = p.Phi()   - p_naive.Phi();

               nnEvent->fBigcalPlaneTheta       = p_naive.Theta();
               nnEvent->fBigcalPlanePhi         = p_naive.Phi();

               nnEvent->fBigcalPlaneDeltaPTheta = bigcalPlane->fMomentum.Theta() - p_naive.Theta();
               nnEvent->fBigcalPlaneDeltaPPhi   = bigcalPlane->fMomentum.Phi()   - p_naive.Phi();

               nnEvent->fBigcalPlaneX           = bigcalPlane->fLocalPosition.X();
               nnEvent->fBigcalPlaneY           = bigcalPlane->fLocalPosition.Y();
               nnEvent->fBigcalPlaneEnergy      = bigcalPlane->fEnergy;

               //if( TMath::Abs(nnEvent->fBigcalPlaneY) < 125 )
               //if( TMath::Abs(nnEvent->fBigcalPlaneX) < 65 )
               //if( nnEvent->fDelta_Energy < 30.0 && nnEvent->fDelta_Energy > 0.0 )
               if( TMath::Abs(thrown->Energy()*1000.0 - bigcalPlane->fEnergy) < 500 )
               if( bigcalPlane->fPID == signalPID)
                  if( thrown->GetPdgCode() == signalPID ){
                     //nnEvent->fBackground = false;
                     nnt->Fill();
                     nkeep++;
                  }
            } // bigcal plane loop
         //} // cluster loop

      }
   }
   std::cout << "\n";
   //nnt->FlushBaskets();
   std::cout << nnt->GetEntries() << " events recorded" << std::endl;
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   //nnt->StartViewer();
   gROOT->ProcessLine(".q");
   return(0);
}
