/*!  Builds the  neural network to get the bigcal plane position 
     as corrections to the cluster position and energy.
     Note the energy has been normalized to be the correct particle energy
 */
Int_t TrainNN_gamma_xycorrection_v3(Int_t number = 654) {

   if (!gROOT->GetClass("TMultiLayerPerceptron")) 
      gSystem->Load("libMLP");

   TString networkName       = "PerpGammaXYCorrection";
   TString networkType       = "GammaXYCorrection";
   TString networkClassName  = "NNPerp";
   networkClassName         += networkType;

   Int_t ntrain              = 100;
   Int_t nHidden             = 100;
   Int_t nHidden2            = 0;
   Bool_t plotInputDataFirst = true;

   // Event class 
   ANNGammaEvent  * annEvent        = new ANNGammaEvent();
   Int_t            fgNInputNeurons = annEvent->GetNInputNeurons();
   TString          inputNeuronStr  = annEvent->GetMLPInputNeurons();
   TString          inputLayer      = annEvent->GetMLPInputNeurons();

   //-------------------------------------------------------------------------------

   std::cout << "-----------------------------------------------------------" << std::endl;
   std::cout << "Network Class Name         : " << networkClassName.Data() << std::endl;
   std::cout << "N input neurons            : " << annEvent->GetNInputNeurons() <<  std::endl;
   std::cout << "Input neurons              : " << std::endl;
   std::cout << "   " << annEvent->GetMLPInputNeurons() <<  std::endl;
   std::cout << "Training iterations        : " << ntrain <<  std::endl;
   std::cout << "N hidden layer neurons     : " << nHidden+nHidden2 <<  std::endl;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName.Data()),"READ");
   TTree * nnt  = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   nnt->SetBranchAddress("NNgammaEvents",&annEvent);

   TCanvas * c = 0;
   if(plotInputDataFirst){
      c = new TCanvas("NNinputCanvas","Network analysis");
      c->Divide(4,3);
      c->cd(1);
      nnt->Draw("fYCluster:fXCluster>>clusterXY(100,-65,65,120,-120,120)","","colz");
      c->cd(2);
      nnt->Draw("fYClusterSigma:fXClusterSigma>>sigmaXY(100,0,3,100,0,3)","","colz");
      c->cd(3);
      nnt->Draw("fYClusterSkew:fXClusterSkew>>skewXY(100,-15,15,100,-15,15)","","colz");
      c->cd(4);
      nnt->Draw("fYCluster:fClusterEnergy>>YvsdeltaE(100,0,4000,60,-120,120)","","colz");
      c->cd(5);
      nnt->Draw("fClusterDeltaY:fClusterDeltaX>>deltaXY(100,-5,5,100,-5,5)","","colz");
      c->cd(6);
      nnt->Draw("fXCluster:fClusterEnergy>>deltaenergyXY(100,0,4000,60,-60,60)","","colz");
      c->cd(7);
      nnt->Draw("fYClusterKurt:fClusterEnergy>>ykurtVSenergy(100,0,5900,100,-3,15)","","colz");
      c->cd(8);
      nnt->Draw("fXClusterKurt:fClusterEnergy>>xkurtVSenergy(100,0,5900,100,-3,15)","","colz");
      c->cd(9);
      nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fClusterEnergy>>deltathetaXY(100,0,5900,100,-5,5)","","colz");
      c->cd(10);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fClusterEnergy>>deltaphiXY(100,0,5900,100,-5,5)","","colz");
      c->cd(11);
      nnt->Draw("fDelta_Energy:fDelta_Theta*180.0/TMath::Pi()>>deltaenergyEEVsDTheta(100,-6,6,100,-500,1000)","","colz");
      c->cd(12);
      //nnt->Draw("fDelta_Energy:fDelta_Phi*180.0/TMath::Pi()>>EEVsDPhi(100,-30,30,100,0,1000)","","colz");
      //nnt->Draw("fDelta_Energy:fRasterY>>EEVsRasterX(100,-3,3,100,0,1000)","","colz");
   }
   if(c)  c->SaveAs(Form("results/neural_networks/perp/%s-input_%d.png",   networkClassName.Data(),number));

   TCanvas * cnet = new TCanvas();

   // -----------------------------------------------------------------
   // X Correction

   TCanvas * c2 = new TCanvas("c2","x Network");
   c2->Divide(2,2);

   // Define the network
   TString Network2 = "";
   if( nHidden2 < 1 ){
      // only one hidden layer 
      Network2 =  Form(
         "%s:%d:fClusterDeltaX", 
         inputLayer.Data(),
         nHidden );
   } else {
      Network2 = Form(
         "%s:%d:%d:fClusterDeltaX", 
         annEvent->GetMLPInputNeurons(),
         nHidden,
         nHidden2 );
   }

   //std::cout << "Input neurons              : " << std::endl;
   std::cout << "ClusterDeltaX network      : " << std::endl;
   std::cout << "   " << Network2.Data() << std::endl;

   TMultiLayerPerceptron * mlp_x = 0;
   mlp_x = new TMultiLayerPerceptron(Network2.Data(),nnt);
   mlp_x->SetLearningMethod(TMultiLayerPerceptron::kBFGS);

   std::cout << "Training ClusterDeltaX Network..." << std::endl;

   c2->cd(1);
   mlp_x->Train(ntrain, "text,current,graph,update=5"); // add graph to string for plot
   TMultiGraph * mg2 = new TMultiGraph();
   mg2->Add(mlp_x->GetResidual()); // did this use to work? Need to patch root to get this
   mg2->Draw("A");

   std::cout << "neural_networks/networks/" << networkClassName << "ClusterDeltaX (.txt,.cxx,.h) will be created " << "\n";
   mlp_x->Export(     Form("neural_networks/networks/%sClusterDeltaX",    networkClassName.Data()));
   mlp_x->DumpWeights(Form("neural_networks/networks/%sClusterDeltaX.txt",networkClassName.Data()));

   TMLPAnalyzer ana_x(mlp_x);
   ana_x.GatherInformations();
   ana_x.CheckNetwork();

   cnet->cd(0);
   ana_x.DrawDInputs();
   // Annoying fix for white colored line.
   hs = (THStack*)gROOT->FindObject("differences");
   if( hs ) {
      TList * hl = hs->GetHists();
      for(int kk=0;kk<hl->GetEntries();kk++){
      //if(hl->GetEntries()>9) {
         if(kk+1==5){
            //yellow
            TH1 * hist = (TH1*)hl->At(kk);
            if(hist) hist->SetLineColor(kYellow -2);  
         }
         if(kk+1==10){
            //white
            TH1 * hist = (TH1*)hl->At(kk);
            if(hist) hist->SetLineColor(kRed -7);  
         }
      }
   } else { std::cout << " could not find THStack named differences.\n"; }
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Xnetwork_D_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Xnetwork_D_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));

   c2->cd(2);
   ana_x.DrawDInputs();
   // Annoying fix for white colored line.
   hs = (THStack*)gROOT->FindObject("differences");
   if( hs ) {
      TList * hl = hs->GetHists();
      for(int kk=0;kk<hl->GetEntries();kk++){
      //if(hl->GetEntries()>9) {
         if(kk+1==5){
            //yellow
            TH1 * hist = (TH1*)hl->At(kk);
            if(hist) hist->SetLineColor(kYellow -2);  
         }
         if(kk+1==10){
            //white
            TH1 * hist = (TH1*)hl->At(kk);
            if(hist) hist->SetLineColor(kRed -7);  
         }
      }
   } else { std::cout << " could not find THStack named differences.\n"; }
   
   cnet = new TCanvas();
   cnet->cd();
   gPad->SetLogy(false);
   mlp_x->Draw();
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Xnetwork_geo_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Xnetwork_geo_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));

   c2->cd(3);
   mlp_x->Draw();

   // -----------------------------------------------------------------
   // Y Correction

   TCanvas * c3 = new TCanvas("c3","y Network");
   c3->Divide(2,2);

   // Define the network
   TString Network3 = "";
   if( nHidden2 < 1 ){
      // only one hidden layer 
      Network3 =  Form(
         "%s:%d:fClusterDeltaY", 
         inputLayer.Data(),
         nHidden );
   } else {
      Network3 = Form(
         "%s:%d:%d:fClusterDeltaY", 
         annEvent->GetMLPInputNeurons(),
         nHidden,
         nHidden2 );
   }

   //std::cout << "Input neurons              : " << std::endl;
   std::cout << "ClusterDeltaY network      : " << std::endl;
   std::cout << "   " << Network3.Data() << std::endl;

   TMultiLayerPerceptron * mlp_y = 0;
   mlp_y = new TMultiLayerPerceptron(Network3.Data(),nnt);
   mlp_y->SetLearningMethod(TMultiLayerPerceptron::kBFGS);

   std::cout << "Training ClusterDeltaY Network..." << std::endl;

   c3->cd(1);
   mlp_y->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   TMultiGraph * mg3 = new TMultiGraph();
   mg3->Add(mlp_y->GetResidual());
   mg3->Draw("A");

   std::cout << networkClassName.Data() << "ClusterDeltaY (.txt,.cxx,.h) will be created " << "\n";
   mlp_y->Export(Form("neural_networks/networks/%sClusterDeltaY",networkClassName.Data()));
   mlp_y->DumpWeights(Form("neural_networks/networks/%sClusterDeltaY.txt",networkClassName.Data()));

   TMLPAnalyzer ana_y(mlp_y);
   ana_y.GatherInformations();
   ana_y.CheckNetwork();
   
   cnet = new TCanvas();
   cnet->cd();
   ana_y.DrawDInputs();
   // Annoying fix for white colored line.
   // Annoying fix for white colored line.
   hs = (THStack*)gROOT->FindObject("differences");
   hs = (THStack*)gROOT->FindObject("differences");
   if( hs ) {
      TList * hl = hs->GetHists();
      for(int kk=0;kk<hl->GetEntries();kk++){
      //if(hl->GetEntries()>9) {
         if(kk+1==5){
            //yellow
            TH1 * hist = (TH1*)hl->At(kk);
            if(hist) hist->SetLineColor(kYellow -2);  
         }
         if(kk+1==10){
            //white
            TH1 * hist = (TH1*)hl->At(kk);
            if(hist) hist->SetLineColor(kRed -7);  
         }
      }
   } else { std::cout << " could not find THStack named differences.\n"; }
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Ynetwork_D_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Ynetwork_D_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));
   c3->cd(2);
   ana_y.DrawDInputs();
   // Annoying fix for white colored line.
   hs = (THStack*)gROOT->FindObject("differences");
   if( hs ) {
      TList * hl = hs->GetHists();
      for(int kk=0;kk<hl->GetEntries();kk++){
      //if(hl->GetEntries()>9) {
         if(kk+1==5){
            //yellow
            TH1 * hist = (TH1*)hl->At(kk);
            if(hist) hist->SetLineColor(kYellow -2);  
         }
         if(kk+1==10){
            //white
            TH1 * hist = (TH1*)hl->At(kk);
            if(hist) hist->SetLineColor(kRed -7);  
         }
      }
   } else { std::cout << " could not find THStack named differences.\n"; }
   
   cnet = new TCanvas();
   cnet->cd();
   gPad->SetLogy(false);
   mlp_y->Draw();
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Ynetwork_geo_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Ynetwork_geo_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));

   c3->cd(3);
   mlp_y->Draw();

   // ----------------------------------------------------------------------
   // 

   TFile * resultFile = new TFile(Form("data/anns/PerpGammaXYcorrection%d-%d.root",number,nHidden),"UPDATE");
   resultFile->cd();
   TTree * resultTree = new TTree("NNgammaXYcorr","NN gamma XYE correction training results"); 
   Double_t nnOut_DeltaE;
   Double_t nnOut_DeltaX;
   Double_t nnOut_DeltaY;
   Double_t nnDiff_DeltaE;
   Double_t nnDiff_DeltaX;
   Double_t nnDiff_DeltaY;
   resultTree->Branch("nnOut_DeltaE",&nnOut_DeltaE,"nnOut_DeltaE/D");
   resultTree->Branch("nnOut_DeltaX",&nnOut_DeltaX,"nnOut_DeltaX/D");
   resultTree->Branch("nnOut_DeltaY",&nnOut_DeltaY,"nnOut_DeltaY/D");
   resultTree->Branch("nnDiff_DeltaE",&nnDiff_DeltaE,"nnDiff_DeltaE/D");
   resultTree->Branch("nnDiff_DeltaX",&nnDiff_DeltaX,"nnDiff_DeltaX/D");
   resultTree->Branch("nnDiff_DeltaY",&nnDiff_DeltaY,"nnDiff_DeltaY/D");

   // Histograms to check NN
   TH1F *bg = new TH1F("bgh", "NN output", 50, -.5, 1.5);
   TH1F *sig = new TH1F("sigh", "NN output", 50, -.5, 1.5);
   //bg->SetDirectory(0);
   //sig->SetDirectory(0);

   TH2F *energies = new TH2F("energy_diff_vs_energy", "Energy Diff (NN - Thrown) ", 200, 0.0, 3000.0,200,-1000.0 , 1000.0);
   //energies->SetDirectory(0);

   // differences 
   TH1F *energy_cor = new TH1F("energy_cor", "Energy  (NN - Thrown) ", 100, -400.0, 1000.0);
   TH1F *x_cor = new TH1F("x_cor", "x  (NN - Thrown) ", 100, -5.0, 5.0);
   TH1F *y_cor = new TH1F("y_cor", "y  (NN - Thrown) ", 100, -5.0, 5.0);
   /// input  
   TH1F *energy_input = new TH1F("energy_input", "#Delta Energy  (Thrown) ", 100, -400.0, 1000.0);
   TH1F *x_input = new TH1F("x_input", "#Delta x  (Thrown) ", 100, -5.0, 5.0);
   TH1F *y_input = new TH1F("y_input", "#Delta y  (Thrown) ", 100, -5.0, 5.0);
   /// ouput predicted
   TH1F *energy_diff = new TH1F("energy_diff", "#Delta Energy  (NN) ", 100, -400.0, 1000.0);
   TH1F *x_diff = new TH1F("x_diff", "#Delta x (NN) ", 100, -5.0, 5.0);
   TH1F *y_diff = new TH1F("y_diff", "#Delta y  (NN)", 100, -5.0, 5.0);
   /// differences input vs some variable
   TH2F *energy_in = new TH2F("energy_in", "#Delta Energy  test ", 100, 0.0, 3000.0, 100, -200.0, 1000.0);
   TH2F *theta_in = new TH2F("x_in", "#Delta x test  ",            100, 0.0, 3000.0, 100, -5.0, 5.0);
   TH2F *phi_in = new TH2F("y_in", "#Delta y  test",               100, 0.0, 3000.0, 100, -5.0, 5.0);

   /// differences vs Energy
   TH2F * energyDiffVsEnergy = new TH2F("energyDiffVsEnergy", "#Delta Energy  vs Energy;E [MeV];#Delta E", 100, 200, 3000.0, 100, -400.0, 600.0);
   TH2F * xDiffVsEnergy = new TH2F("xDiffVsEnergy", "#delta x  vs Energy;E [MeV];#delta x", 100, 200, 3000.0, 100, -3.0, 3.0);
   TH2F * yDiffVsEnergy = new TH2F("yDiffVsEnergy", "#delta y  vs Energy;E [MeV];#delta y", 100, 200, 3000.0, 100, -3.0, 3.0);
   /// differences vs X 
   TH2F * energyDiffVsX = new TH2F("energyDiffVsX", "#Delta Energy  vs X;x;#Delta E", 100, -120.0,120.0, 100, -400.0, 600.0);
   TH2F * xDiffVsX      = new TH2F("xDiffVsX", "#delta x  vs X; x;#delta x",          100, -120.0,120.0, 100, -3.0, 3.0);
   TH2F * yDiffVsX      = new TH2F("yDiffVsX", "#delta y  vs X; x;#delta y",          100, -120.0,120.0, 100, -3.0, 3.0);
   /// differences vs X 
   TH2F * energyDiffVsY = new TH2F("energyDiffVsY", "#Delta Energy  vs Y;y;#Delta E", 100, -120.0,120.0, 100, -400.0, 600.0);
   TH2F * xDiffVsY      = new TH2F("xDiffVsY", "#delta x  vs Y; y;#delta x",          100, -120.0,120.0, 100, -3.0, 3.0);
   TH2F * yDiffVsY      = new TH2F("yDiffVsY", "#delta y  vs Y; y;#delta y",          100, -120.0,120.0, 100, -3.0, 3.0);

   Double_t * params = new Double_t[fgNInputNeurons];

   // Loop over tree and get the corrections
   for (int i = 0; i < nnt->GetEntries(); i++) {

      nnt->GetEntry(i);
      annEvent->GetMLPInputNeuronArray(params);

      /// Extract values
      //std::cout << " delta: energy = " << delta_energy_NN 
      //          << " , theta = " << delta_theta_NN 
      //          << " , phi = " << delta_phi_NN << "\n";

      //Double_t delta_energy_NN = mlp->Evaluate(0,params);
      //Double_t delta_theta_NN  = mlp->Evaluate(1,params);
      //Double_t delta_phi_NN    = mlp->Evaluate(2,params);

      Double_t delta_energy_NN = 0.0;//mlp_energy->Evaluate(0,params);
      Double_t delta_x_NN      = mlp_x->Evaluate(0,params);
      Double_t delta_y_NN      = mlp_y->Evaluate(0,params);

      Double_t cEnergy = annEvent->fClusterEnergy + delta_energy_NN ;
      Double_t cX      = annEvent->fXCluster + delta_x_NN ;
      Double_t cY      = annEvent->fYCluster + delta_y_NN ;

      sig->Fill(annEvent->fSignal);
      bg->Fill( annEvent->fBackground);

      // The network output error
      energy_diff->Fill( delta_energy_NN  - annEvent->fDelta_Energy );
      x_diff->Fill( ( delta_x_NN  - annEvent->fClusterDeltaX) );
      y_diff->Fill( ( delta_y_NN  - annEvent->fClusterDeltaY) );

      // The training input 
      energy_input->Fill(  annEvent->fDelta_Energy );
      x_input->Fill( (     annEvent->fClusterDeltaX) );
      y_input->Fill( (     annEvent->fClusterDeltaY) );

      // The network output
      energy_cor->Fill(  delta_energy_NN );
      x_cor->Fill(       delta_x_NN );
      y_cor->Fill(       delta_y_NN );

      energy_in->Fill( annEvent->fClusterEnergy, delta_energy_NN );
      x_in->Fill(      annEvent->fClusterEnergy, delta_x_NN );
      y_in->Fill(      annEvent->fClusterEnergy, delta_y_NN );
      
      nnOut_DeltaE = delta_energy_NN;
      nnOut_DeltaX = delta_x_NN;
      nnOut_DeltaY = delta_y_NN;
      nnDiff_DeltaE = delta_energy_NN - annEvent->fDelta_Energy;
      nnDiff_DeltaX = delta_x_NN - annEvent->fClusterDeltaX;
      nnDiff_DeltaY = delta_y_NN - annEvent->fClusterDeltaY;

      energyDiffVsEnergy->Fill( annEvent->fClusterEnergy, nnDiff_DeltaE);
      xDiffVsEnergy->Fill(      annEvent->fClusterEnergy, nnDiff_DeltaX);
      yDiffVsEnergy->Fill(      annEvent->fClusterEnergy, nnDiff_DeltaY);

      energyDiffVsX->Fill( annEvent->fXCluster, nnDiff_DeltaE);
      xDiffVsX->Fill(      annEvent->fXCluster, nnDiff_DeltaX);
      yDiffVsX->Fill(      annEvent->fXCluster, nnDiff_DeltaY);

      energyDiffVsY->Fill( annEvent->fYCluster, nnDiff_DeltaE);
      xDiffVsY->Fill(      annEvent->fYCluster, nnDiff_DeltaX);
      yDiffVsY->Fill(      annEvent->fYCluster, nnDiff_DeltaY);

   }

   THStack * hs1 = new THStack();
   THStack * hs2 = new THStack();
   THStack * hs3 = new THStack();

   TLegend * leg = new TLegend(0.7,0.75,0.87,0.87);
   
   // -----------------
   // output test
   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(3,3);
   //mlpa_canvas->cd(1)->Divide(3,1);
   //mlpa_canvas->cd(2)->Divide(3,1);
   //mlpa_canvas->cd(3)->Divide(3,1);

   mlpa_canvas->cd(1);
   energyDiffVsEnergy->Draw("colz");

   mlpa_canvas->cd(4);
   energyDiffVsX->Draw("colz");

   mlpa_canvas->cd(7);
   energyDiffVsY->Draw("colz");

   mlpa_canvas->cd(2);
   xDiffVsEnergy->Draw("colz");

   mlpa_canvas->cd(5);
   xDiffVsX->Draw("colz");

   mlpa_canvas->cd(8);
   xDiffVsY->Draw("colz");

   mlpa_canvas->cd(3);
   yDiffVsEnergy->Draw("colz");

   mlpa_canvas->cd(6);
   yDiffVsX->Draw("colz");

   mlpa_canvas->cd(9);
   yDiffVsY->Draw("colz");

   // -----------------

   //mlpa_canvas->cd(4);
   //c1->cd(4);
   //gPad->SetLogy(true);
   //energy_diff->SetLineColor(2);
   //energy_input->SetLineColor(4);
   //hs1->Add(energy_cor); 
   //hs1->Add(energy_input); 
   //hs1->Add(energy_diff);
   //hs1->Draw("nostack");
   //leg->AddEntry(energy_cor,"corrected output","l"); 
   //leg->AddEntry(energy_input,"input","l"); 
   //leg->AddEntry(energy_diff,"output-input","l"); 
   //leg->Draw();

   c2->cd(4);
   gPad->SetLogy(true);
   x_diff->SetLineColor(2);
   x_input->SetLineColor(4);
   leg->AddEntry(x_cor,"corrected output","l"); 
   leg->AddEntry(x_input,"input","l"); 
   leg->AddEntry(x_diff,"output-input","l"); 
   hs2->Add(x_cor);
   hs2->Add(x_input);
   hs2->Add(x_diff);
   //x_cor->Draw();
   //x_input->Draw("same");
   //x_diff->Draw("same");
   hs2->Draw("nostack");
   leg->Draw();
   
   c3->cd(4);
   gPad->SetLogy(true);
   y_diff->SetLineColor(2);
   y_input->SetLineColor(4);
   hs3->Add(y_cor);
   hs3->Add(y_input);
   hs3->Add(y_diff);
   //y_cor->Draw();
   //y_input->Draw("same");
   //y_diff->Draw("same");
   hs3->Draw("nostack");
   leg->Draw();

   

   // OBSERVATION :
   // NN does better without the maximum block positions (xy_max)
   // Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

   mlpa_canvas->Update();
   mlpa_canvas->SaveAs(Form("results/neural_networks/perp/%s-%d-%d_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   mlpa_canvas->SaveAs(Form("results/neural_networks/perp/%s-%d-%d_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));

   if(c2)c2->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-c2_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   if(c3)c3->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-c3_%d.png",networkClassName.Data(),nHidden,nHidden2,number));

   // ------------------------------
   cnet = new TCanvas();
   cnet->cd();
   gPad->SetLogy(true);
   hs2->Draw("nostack");
   hs2->GetXaxis()->CenterTitle(true);
   hs2->GetXaxis()->SetTitle("#delta_{x}");
   leg->Draw();
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Xnetwork_res_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Xnetwork_res_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));
   
   cnet = new TCanvas();
   cnet->cd();
   gPad->SetLogy(true);
   hs3->Draw("nostack");
   hs3->GetXaxis()->CenterTitle(true);
   hs3->GetXaxis()->SetTitle("#delta_{y}");
   leg->Draw();
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Ynetwork_res_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Ynetwork_res_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));

   cnet = new TCanvas();
   gPad->SetLogy(false);
   mg2->Draw("a");
   mg2->GetXaxis()->CenterTitle(true);
   mg2->GetYaxis()->CenterTitle(true);
   mg2->GetXaxis()->SetTitle("Training Iteration");
   mg2->GetYaxis()->SetTitle("Residual");
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Xnetwork_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Xnetwork_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));

   mg3->Draw("a");
   mg3->GetXaxis()->CenterTitle(true);
   mg3->GetYaxis()->CenterTitle(true);
   mg3->GetXaxis()->SetTitle("Training Iteration");
   mg3->GetYaxis()->SetTitle("Residual");
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Ynetwork_%d.png",networkClassName.Data(),nHidden,nHidden2,number));
   cnet->SaveAs(Form("results/neural_networks/perp/%s-%d-%d-Ynetwork_%d.pdf",networkClassName.Data(),nHidden,nHidden2,number));

   resultFile->Write();
   return(0);

}
