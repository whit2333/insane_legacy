/*!   Adds the events to the tree; 
 */
Int_t build_chain_gamma(Int_t number=1) {
   const char * networkName = "PerpGammaCorrection";
   const char * signalParticle = "gamma";

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();

    aman->GetRunQueue()->push_back(140asdf6);

   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloEvent * MCevent = events->MC;
   BETAG4MonteCarloThrownParticle * thrown = new BETAG4MonteCarloThrownParticle();
   InSANEFakePlaneHit * bigcalPlane = 0; 

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   ANNDisElectronEvent * disEvent = new ANNDisElectronEvent();
   disEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"RECREATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNDisElectronEvent", &disEvent);
   //aChain->StartViewer();

   std::cout << " chain has " << aChain->GetEntries() << " entries\n";

   for (int i = 0;i< aChain->GetEntries();i++ ) {
      aChain->GetEntry(i);
     //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
      if( clusters->GetEntries() > 0 ){
     //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
     // only 1 thrown particle !!!
	 thrown = (BETAG4MonteCarloThrownParticle*)(*thrownParticles)[0];

      for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {   
	 aCluster  = (BIGCALCluster*)(*clusters)[iClust];
	 //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
	 // Clear last event
	 disEvent->Clear();

         for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
            bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];

	 TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
	 disEvent->SetEventValues(aCluster); 
         disEvent->fDelta_Energy = bigcalPlane->fEnergy - aCluster->GetEnergy();
         disEvent->fDelta_Theta  = p.Theta() - aCluster->GetTheta();
         disEvent->fDelta_Phi    = p.Phi()   - aCluster->GetPhi();
         disEvent->fTrueEnergy = bigcalPlane->fEnergy;
	 disEvent->fTrueTheta  = p.Theta();
	 disEvent->fTruePhi    = p.Phi() ;
         disEvent->fNClusters = clusters->GetEntries();


	    if( disEvent->fDelta_Energy < 400.0)
	    if( TMath::Abs(disEvent->fDelta_Phi/0.0175 ) < 2.0)
	    if( bigcalPlane->fPID == signalPID)
	    if( thrown->GetPdgCode() == signalPID )
	    // if( disEvent->fDelta_Phi > 5.0*0.017 )
	    {
/*            std::cout << "PID = " << signalPID << " was thrown\n";*/
               disEvent->fBackground = false;
               nnt->Fill();
	    }
	 }
      } 
      }
   }

   nnt->FlushBaskets();
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   /*nnt->StartViewer();*/

   return(0);
}
