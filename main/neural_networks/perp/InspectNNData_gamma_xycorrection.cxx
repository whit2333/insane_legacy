/*!  Builds the  neural network to get the bigcal plane position and energy
     as corrections to the cluster position and energy.

 */
Int_t InspectNNData_gamma_xycorrection(Int_t number=650) {

   if (!gROOT->GetClass("TMultiLayerPerceptron")) gSystem->Load("libMLP");

   TString networkName       = "PerpGammaXYCorrection";
   TString networkType       = "GammaXYCorrection";
   TString networkClassName  = "NNPerp";
   networkClassName += networkType;

   TH1F * h1 = 0;
   TH2F * h2 = 0;

   ANNGammaEvent * event = new ANNGammaEvent();
   const Int_t fgNInputNeurons  = event->GetNInputNeurons();

   std::cout << "-----------------------------------" << std::endl;
   std::cout << "Network Class Name : " << networkClassName.Data() << std::endl;
   std::cout << "Input string has " << event->GetNInputNeurons() << " neurons " << std::endl;
   std::cout << event->GetMLPInputNeurons() << std::endl;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName.Data()),"READ");
   TTree * nnt  = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   nnt->SetBranchAddress("NNgammaEvents",&event);
   std::cout << "Entries : " << nnt->GetEntries() << std::endl;;

   // ---------------------------------------------------------------
   TCanvas * c = new TCanvas("NNinputCanvas","Network analysis");
   c->Divide(4,3);
   c->cd(1);
   nnt->Draw("fYCluster:fXCluster>>clusterXY(100,-65,65,120,-120,120)","","colz");
   c->cd(2);
   nnt->Draw("fYClusterSigma:fXClusterSigma>>sigmaXY(100,0,3,100,0,3)","","colz");
   c->cd(3);
   nnt->Draw("fYClusterSkew:fXClusterSkew>>skewXY(100,-15,15,100,-15,15)","","colz");
   c->cd(4);
   //nnt->Draw("fYClusterKurt:fXClusterKurt>>kurtXY(100,-3,15,100,-3,15)","","colz");
   nnt->Draw("fYCluster:fClusterEnergy>>YvsdeltaE(100,0,4000,60,-120,120)","","colz");
   c->cd(5);
   nnt->Draw("fClusterDeltaY:fClusterDeltaX>>deltaXY(100,-5,5,100,-5,5)","","colz");
   c->cd(6);
   nnt->Draw("fXCluster:fClusterEnergy>>deltaenergyXY(100,0,4000,50,-60,60)","","colz");
   c->cd(7);
   nnt->Draw("fYClusterKurt:fXClusterKurt>>ykurtVSenergy(32,1,33,56,1,57)","","colz");
   c->cd(8);
   nnt->Draw("fXClusterKurt:fClusterEnergy>>xkurtVSenergy(100,0,5900,100,-3,15)","","colz");
   c->cd(9);
   nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fClusterEnergy>>deltathetaXY(100,0,5900,100,-6,6)","","colz");
   c->cd(10);
   nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fClusterEnergy>>deltaphiXY(100,0,5900,100,-30,30)","","colz");
   c->cd(11);
   nnt->Draw("fYCluster:fClusterDeltaY>>YvsdeltaE4(100,-5,5,60,-120,120)","","colz");
   c->cd(12);
   //nnt->Draw("fDelta_Energy:fDelta_Phi*180.0/TMath::Pi()>>EEVsDPhi(100,-30,30,100,0,1000)","","colz");
   //nnt->Draw("fDelta_Energy:fRasterY>>EEVsRasterX(100,-3,3,100,0,1000)","","colz");
   nnt->Draw("fYCluster:fClusterDeltaX>>YvsdeltaE3(100,-5,5,60,-120,120)","","colz");

   c->SaveAs(Form("results/neural_networks/perp/InspectNNData_gamma_xycorrection_0_%s-input_%d.png",   networkClassName.Data(),number));

   // ---------------------------------------------------------------
   TCanvas * c1 = new TCanvas("c1","Energy Network");
   c1->Divide(2,2);

   c1->cd(1);
   nnt->Draw("fTrueEnergy>>hE0(100,100,4100)","","goff");
   h1 = (TH1F*)gROOT->FindObject("hE0");
   if(h1){
      h1->Draw();
      h1->GetXaxis()->SetTitle("E (MeV)");
      h1->GetXaxis()->CenterTitle(true);
   }
   nnt->Draw("fClusterEnergy>>hE1(100,100,4100)","","goff");
   h1 = (TH1F*)gROOT->FindObject("hE1");
   if(h1){
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   c1->cd(2);
   nnt->Draw("fTrueEnergy>>hE3(100,100,4100)","fYCluster>20","goff");
   h1 = (TH1F*)gROOT->FindObject("hE3");
   if(h1){
      h1->Draw();
      h1->GetXaxis()->SetTitle("E (MeV)");
      h1->GetXaxis()->CenterTitle(true);
   }
   nnt->Draw("fClusterEnergy>>hE4(100,100,4100)","fYCluster>20","goff");
   h1 = (TH1F*)gROOT->FindObject("hE4");
   if(h1){
      h1->SetLineColor(2);
      h1->Draw("same");
   }

   c1->cd(4);
   nnt->Draw("fTrueEnergy>>hE5(100,100,4100)","fYCluster<20","goff");
   h1 = (TH1F*)gROOT->FindObject("hE5");
   if(h1){
      h1->Draw();
      h1->GetXaxis()->SetTitle("E (MeV)");
      h1->GetXaxis()->CenterTitle(true);
   }
   nnt->Draw("fClusterEnergy>>hE6(100,100,4100)","fYCluster<20","goff");
   h1 = (TH1F*)gROOT->FindObject("hE6");
   if(h1){
      h1->SetLineColor(2);
      h1->Draw("same");
   }
   c1->SaveAs(Form("results/neural_networks/perp/InspectNNData_gamma_xycorrection_1_%s-input_%d.png",   networkClassName.Data(),number));

   // ---------------------------------------------------------------
   // 
   TCanvas * c2 = new TCanvas("c2","X Network");
   c2->Divide(2,2);

   c2->cd(1);
   nnt->Draw("fYCluster:fDelta_Energy>>hYvsDeltaE(50,-500,1000,60,-120,120)","","colz");

   c2->cd(3);
   nnt->Draw("fDelta_Energy:fClusterEnergy>>hDeltaEVsE(100,0,4000,50,-1000,1000)","","colz");

   c2->cd(2);
   nnt->Draw("fDelta_Energy:fClusterEnergy>>hDeltaEVsE1(100,0,4000,50,-1000,1000)","fYCluster>20","colz");

   c2->cd(4);
   nnt->Draw("fDelta_Energy:fClusterEnergy>>hDeltaEVsE2(100,0,4000,50,-1000,1000)","fYCluster<20","colz");

   c2->SaveAs(Form("results/neural_networks/perp/InspectNNData_gamma_xycorrection_2_%s-input_%d.png",   networkClassName.Data(),number));

   // ---------------------------------------------------------------
   // 
   TCanvas * c3 = new TCanvas("c3","X Network");
   c3->Divide(2,2);

   c3->cd(1);
   nnt->Draw("fYCluster:fClusterDeltaX>>hYvsDeltaX(50,-10,10,60,-120,120)","","colz");

   c3->cd(3);
   nnt->Draw("fYCluster:fClusterDeltaY>>hYvsDeltaY0(50,-10,10,60,-120,120)","","colz");

   c3->cd(2);
   nnt->Draw("fYCluster:fClusterDeltaX>>hYvsDeltaX1(50,-10,10,60,-120,120)","","colz");

   c3->cd(4);
   nnt->Draw("fDelta_Energy:fClusterDeltaY>>hYvsDeltaX2(50,-10,10,50,-1000,1000)","","colz");

   c3->SaveAs(Form("results/neural_networks/perp/InspectNNData_gamma_xycorrection_3_%s-input_%d.png",   networkClassName.Data(),number));


   return(0);
}
