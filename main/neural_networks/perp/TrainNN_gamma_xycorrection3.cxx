/*!  Builds the  neural network
 */
Int_t TrainNN_gamma_xycorrection3(Int_t number=516) {
   const char * networkName = "Perp59GammaXYCorrection";
   const char * networkType = "GammaXYCorrection";
   const char * networkClassName = Form("NNPerp%s",networkType);  
   TString networkClassName2 = networkClassName;

   Int_t ntrain   = 400;
   Int_t nHidden  = 20;
   Int_t nHidden2 = 0;
   Bool_t plotInputDataFirst = true; 

   ANNDisElectronEvent5 * event = new ANNDisElectronEvent5();
   const Int_t fgNInputNeurons = event->GetMLPInputNeurons();

   std::cout << " Network Class Name " << networkClassName2.Data() << " .\n";
   std::cout << "\n Input string has " << fgNInputNeurons << " neurons : \n";
   std::cout << event->GetMLPInputNeurons() << "\n";

   if (!gROOT->GetClass("TMultiLayerPerceptron")) {
      gSystem->Load("libMLP");
   }

   TMultiLayerPerceptron * mlp = 0;
   TMultiLayerPerceptron * mlp_energy = 0;
   TMultiLayerPerceptron * mlp_x = 0;
   TMultiLayerPerceptron * mlp_y = 0;

   TFile * file = new TFile(Form("data/anns/NN%s.root",networkName),"READ");

   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));
   nnt->SetBranchAddress("NNeEvents",&event);

   TCanvas * c = 0;

   if(plotInputDataFirst){
      c = new TCanvas("NNinputCanvas","Network analysis");
      c->Divide(4,2);
      c->cd(1);
      nnt->Draw("fYCluster:fXCluster>>clusterXY(200,-60,60,200,-120,120)","","colz",1000000000,1001);
      c->cd(2);
      nnt->Draw("fYClusterSigma:fXClusterSigma>>sigmaXY(200,0,5,200,0,5)","","colz",1000000000,1001);
      c->cd(3);
      nnt->Draw("fClusterDeltaY:fClusterDeltaX>>skewXY(200,-15,15,200,-15,15)","","colz",1000000000,1001);
      c->cd(4);
      nnt->Draw("fDelta_Energy:fClusterEnergy>>deltaenergyXY(200,0,5900,200,-500,2000)","","colz",1000000000,1001);
      c->cd(5);
      nnt->Draw("fDelta_Theta*180.0/TMath::Pi():fClusterEnergy>>deltathetaXY(200,0,5900,200,-20,20)","","colz",1000000000,1001);
      c->cd(6);
      nnt->Draw("fDelta_Phi*180.0/TMath::Pi():fClusterEnergy>>deltaphiXY(200,0,5900,200,-20,50)","","colz",1000000000,1001);
      c->cd(7);
      nnt->Draw("fDelta_Energy:fDelta_Theta*180.0/TMath::Pi()>>deltaenergyEEVsDTheta(200,-2,8,200,0,1000)","","colz",1000000000,1001);
      c->cd(8);
      nnt->Draw("fDelta_Energy:fDelta_Phi*180.0/TMath::Pi()>>EEVsDPhi(200,-20,0,200,0,1000)","","colz",1000000000,1001);
   }

   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(3,3);
   mlpa_canvas->cd(1)->Divide(3,1);
   mlpa_canvas->cd(2)->Divide(3,1);
   mlpa_canvas->cd(3)->Divide(3,1);

   /// Energy Correction 
   mlpa_canvas->cd(1)->cd(3);
   if( nHidden2 < 1 ){
      mlp_energy = new TMultiLayerPerceptron(Form(
         "%s:%d:fDelta_Energy",
         event->GetMLPInputNeurons(),
         nHidden  ),nnt);
   } else {
      mlp_energy = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fDelta_Energy",
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2  ),nnt);
   }
   mlp_energy->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   mlp_energy->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName2.Data() << "Energy (.txt,.cxx,.h) will be created " << "\n\n";
   mlp_energy->Export(Form("%sEnergy",networkClassName));
   mlp_energy->DumpWeights(Form("%sEnergy.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_energy(mlp_energy);
   ana_energy.GatherInformations();
   ana_energy.CheckNetwork();
   mlpa_canvas->cd(1)->cd(1);
   ana_energy.DrawDInputs();
   mlpa_canvas->cd(1)->cd(2);
   mlp_energy->Draw();

   /// Cluster X Correction
   mlpa_canvas->cd(2)->cd(3);
   if( nHidden2 < 1 ){
      mlp_x = new TMultiLayerPerceptron(Form(
         "%s:%d:fClusterDeltaX", 
         event->GetMLPInputNeurons(),
         nHidden ),nnt);
   } else {
      mlp_x = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fClusterDeltaX", 
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2 ),nnt);
   }
   mlp_x->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   mlp_x->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName << "ClusterDeltaX (.txt,.cxx,.h) will be created " << "\n\n";
   mlp_x->Export(Form("%sClusterDeltaX",networkClassName2.Data()));
   mlp_x->DumpWeights(Form("%sClusterDeltaX.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_x(mlp_x);
   ana_x.GatherInformations();
   ana_x.CheckNetwork();
   mlpa_canvas->cd(2)->cd(1);
   ana_x.DrawDInputs();
   mlpa_canvas->cd(2)->cd(2);
   mlp_x->Draw();

   /// Cluster Y Correction
   mlpa_canvas->cd(3)->cd(3);
   if( nHidden2 < 1 ){
      mlp_y = new TMultiLayerPerceptron(Form(
         "%s:%d:fClusterDeltaY", 
         event->GetMLPInputNeurons(),
         nHidden ),nnt);
   } else {
      mlp_y = new TMultiLayerPerceptron(Form(
         "%s:%d:%d:fClusterDeltaY", 
         event->GetMLPInputNeurons(),
         nHidden,
         nHidden2 ),nnt);
   }
   mlp_y->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
   mlp_y->Train(ntrain, "text,current,graph,update=10"); // add graph to string for plot
   std::cout << networkClassName2.Data() << "ClusterDeltaY (.txt,.cxx,.h) will be created " << "\n\n";
   mlp_y->Export(Form("%sClusterDeltaY",networkClassName2.Data()));
   mlp_y->DumpWeights(Form("%sClusterDeltaY.txt",networkClassName2.Data()));
   TMLPAnalyzer ana_y(mlp_y);
   ana_y.GatherInformations();
   ana_y.CheckNetwork();
   mlpa_canvas->cd(3)->cd(1);
   ana_y.DrawDInputs();
   mlpa_canvas->cd(3)->cd(2);
   mlp_y->Draw();


   /// Energy+Theta+Phi Corrections
//    mlp = new TMultiLayerPerceptron(Form(
//       "%s:%d:fDelta_Energy,fDelta_Theta,fDelta_Phi", event->GetCorrectionMLPInputNeurons(),nHidden ),nnt);
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kStochastic); // sucks
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kSteepestDescent);
//   mlp->SetLearningMethod(TMultiLayerPerceptron::kBatch);
//    mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);  // best???
// 
//    mlp->Train(ntrain, "text,graph,current,update=10"); // add graph to string for plot
//    mlp->Export(networkClassName);
//   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",800,600);
//   mlpa_canvas->Divide(2,2);
//    TMLPAnalyzer ana(mlp);
//    ana.GatherInformations();
//    ana.CheckNetwork();
//    mlpa_canvas->cd(1);
//    ana.DrawDInputs();
//    mlpa_canvas->cd(2);
//    mlp->Draw();
   // draws the resulting network
//    ana.DrawNetwork(0,"fBackground==0","fBackground==1");
//    mlpa_canvas->cd(5);
//    ana.DrawNetwork(1,"fBackground==0","fBackground==1");
//    mlpa_canvas->cd(6);


   /// Histograms to check NN
   TH1F *bg = new TH1F("bgh", "NN output", 50, -.5, 1.5);
   TH1F *sig = new TH1F("sigh", "NN output", 50, -.5, 1.5);
   bg->SetDirectory(0);
   sig->SetDirectory(0);

   TH2F *energies = new TH2F("energy_diff_vs_energy", "Energy Diff (NN - Thrown) ", 200, 0.0, 3000.0,200,-1000.0 , 1000.0);
   energies->SetDirectory(0);

   /// differences 
   TH1F *energy_cor = new TH1F("energy_cor", "Energy  (NN - Thrown) ", 100, -200.0, 1000.0);
   TH1F *x_cor = new TH1F("x_cor", "x  (NN - Thrown) ", 100, -5.0, 5.0);
   TH1F *y_cor = new TH1F("y_cor", "y  (NN - Thrown) ", 100, -5.0, 5.0);
   /// input  
   TH1F *energy_input = new TH1F("energy_input", "#Delta Energy  (Thrown) ", 100, -200.0, 1000.0);
   TH1F *x_input = new TH1F("x_input", "#Delta x  (Thrown) ", 100, -5.0, 5.0);
   TH1F *y_input = new TH1F("y_input", "#Delta y  (Thrown) ", 100, -5.0, 5.0);
   /// ouput predicted
   TH1F *energy_diff = new TH1F("energy_diff", "#Delta Energy  (NN) ", 100, -200.0, 1000.0);
   TH1F *x_diff = new TH1F("x_diff", "#Delta x (NN) ", 100, -5.0, 5.0);
   TH1F *y_diff = new TH1F("y_diff", "#Delta y  (NN)", 100, -5.0, 5.0);
   /// differences input vs some variable
   TH2F *energy_in = new TH2F("energy_in", "#Delta Energy  test ", 100, 0.0, 3000.0, 100, -200.0, 1000.0);
   TH2F *theta_in = new TH2F("x_in", "#Delta x test  ",            100, 0.0, 3000.0, 100, -5.0, 5.0);
   TH2F *phi_in = new TH2F("y_in", "#Delta y  test",               100, 0.0, 3000.0, 100, -5.0, 5.0);

   energy_diff->SetDirectory(0);
   x_diff->SetDirectory(0);
   y_diff->SetDirectory(0);

   Double_t * params = new Double_t[fgNInputNeurons];

   /// Loop over tree and get the corrections
   for (int i = 0; i < nnt->GetEntries(); i++) {

      nnt->GetEntry(i);
      event->GetMLPInputNeuronArray(params);

      /// Extract values
      //std::cout << " delta: energy = " << delta_energy_NN 
      //          << " , theta = " << delta_theta_NN 
      //          << " , phi = " << delta_phi_NN << "\n";

      //Double_t delta_energy_NN = mlp->Evaluate(0,params);
      //Double_t delta_theta_NN  = mlp->Evaluate(1,params);
      //Double_t delta_phi_NN    = mlp->Evaluate(2,params);

      Double_t delta_energy_NN = mlp_energy->Evaluate(0,params);
      Double_t delta_x_NN      = mlp_x->Evaluate(0,params);
      Double_t delta_y_NN      = mlp_y->Evaluate(0,params);

      Double_t cEnergy = event->fClusterEnergy + delta_energy_NN ;
      Double_t cX      = event->fXCluster + delta_x_NN ;
      Double_t cY      = event->fYCluster + delta_y_NN ;

      sig->Fill(event->fSignal);
      bg->Fill(event->fBackground);

      energy_cor->Fill( delta_energy_NN  - event->fDelta_Energy );
      x_cor->Fill( ( delta_x_NN  - event->fClusterDeltaX) );
      y_cor->Fill( ( delta_y_NN  - event->fClusterDeltaY) );

      energy_input->Fill(  event->fDelta_Energy );
      x_input->Fill( ( event->fClusterDeltaX) );
      y_input->Fill( ( event->fClusterDeltaY) );

      energy_diff->Fill(  delta_energy_NN );
      x_diff->Fill(       delta_x_NN );
      y_diff->Fill(       delta_y_NN );

      energy_in->Fill( event->fClusterEnergy, delta_energy_NN );
      x_in->Fill(      event->fClusterEnergy, delta_x_NN );
      y_in->Fill(      event->fClusterEnergy, delta_y_NN );

   }


   bg->SetLineColor(kBlue);
   bg->SetFillStyle(3008);   bg->SetFillColor(kBlue);
   sig->SetLineColor(kRed);
   sig->SetFillStyle(3003); sig->SetFillColor(kRed);
   bg->SetStats(0);
   sig->SetStats(0);


   mlpa_canvas->cd(4);
   energy_diff->SetLineColor(2);
   energy_input->SetLineColor(4);
   energy_cor->Draw();
   energy_input->Draw("same");
   energy_diff->Draw("same");

   mlpa_canvas->cd(7);
   energy_in->Draw("colz");


   mlpa_canvas->cd(5);
   x_diff->SetLineColor(2);
   x_input->SetLineColor(4);
   x_cor->Draw();
   x_input->Draw("same");
   x_diff->Draw("same");

   mlpa_canvas->cd(8);
   x_in->Draw("colz");


   mlpa_canvas->cd(6);
   y_diff->SetLineColor(2);
   y_input->SetLineColor(4);
   y_cor->Draw();
   y_input->Draw("same");
   y_diff->Draw("same");

   mlpa_canvas->cd(9);
   y_in->Draw("colz");

// OBSERVATION :
// NN does better without the maximum block positions (xy_max)
// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

   mlpa_canvas->Update();
   mlpa_canvas->SaveAs(Form("results/neural_networks/%s-%d-%d.png",networkClassName2.Data(),nHidden,nHidden2));
   mlpa_canvas->SaveAs(Form("results/neural_networks/%s-%d-%d.pdf",networkClassName2.Data(),nHidden,nHidden2));
   if(c)c->SaveAs(Form("results/neural_networks/%s-input.png",networkClassName2.Data()));
   
return(0);
}
