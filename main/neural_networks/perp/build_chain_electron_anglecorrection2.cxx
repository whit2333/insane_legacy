/*! Networks to correct for 

 */
Int_t build_chain_electron_anglecorrection2(Int_t number=640) {

   const char * networkName = "PerpElectronAngleCorrection";
   const char * signalParticle = "e-";

   /// Build up queue
   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   //aman->BuildQueue2("neural_networks/para/para_uniform_all.txt");  	
   aman->BuildQueue2("lists/perp/MC/mc_list_100.txt");  	
   //aman->GetRunQueue()->push_back(4422); // 1405 -- 10k with min energy increased to 400MeV

   /// Get chain of runs
   TChain * aChain = aman->BuildChain("betaDetectors1");
   SANEEvents * events = new SANEEvents(aChain);
   events->SetClusterBranches( aChain );

   TParticlePDG * part = TDatabasePDG::Instance()->GetParticle(signalParticle);
   Int_t signalPID = part->PdgCode();
   std::cout << " Particle " << signalParticle << " has Pdg code " << signalPID << "\n";

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloEvent * MCevent = events->MC;
   BETAG4MonteCarloThrownParticle * thrown = new BETAG4MonteCarloThrownParticle();
   InSANEFakePlaneHit * bigcalPlane = 0; 

   TClonesArray * bigcalPlaneHits = events->MC->fBigcalPlaneHits;
   TClonesArray * thrownParticles = events->MC->fThrownParticles;
   TClonesArray * clusters = events->CLUSTER->fClusters;

   TVector3 vertex;
   TVector3 p_naive;

   ANNElectronThruFieldEvent2 * nnEvent = new ANNElectronThruFieldEvent2();
   nnEvent->Clear();

   TFile * NNFile = new TFile(Form("data/anns/NN%s.root",networkName),"UPDATE");
   TTree * nnt = 0;
   std::cout << "Creating nnt Tree \n";
   nnt = new TTree(Form("nnt%d",number),"training tree");
   nnt->Branch("NNeEvents", "ANNElectronThruFieldEvent2", &nnEvent);
   //aChain->StartViewer();
   //return(0);
   std::cout << " Chain has " << aChain->GetEntries() << ".\n";
   for (int i = 0;i< aChain->GetEntries() ;i++ ) {
      if( i%1000 == 0 ) {
         std::cout << "\r" << i ;
         std::cout.flush();
      }
      aChain->GetEntry(i);
      if( clusters->GetEntries() > 0 ){
         //    std::cout << " " << clusters->GetEntries() << " bigcal clusters \n";
         // only 1 thrown particle !!!
         thrown = (BETAG4MonteCarloThrownParticle*)(*thrownParticles)[0];

         for(int iClust = 0; iClust < clusters->GetEntries(); iClust++) {
            aCluster  = (BIGCALCluster*)(*clusters)[iClust];
            //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
            //bigcalPlane = (InSANEFakePlaneHit*)(*(events->MC->fBigcalPlaneHits));
            for(int iPlane = 0; iPlane < bigcalPlaneHits->GetEntries(); iPlane++) {
               bigcalPlane = (InSANEFakePlaneHit*)(*bigcalPlaneHits)[iPlane];

               TVector3 p(thrown->Px()*1000.0,thrown->Py()*1000.0,thrown->Pz()*1000.0);
               //nnEvent->SetEventValues(aCluster); 

               nnEvent->fTrueEnergy             = thrown->Energy()*1000.0;
               nnEvent->fTrueTheta              = p.Theta();
               nnEvent->fTruePhi                = p.Phi() ;

               nnEvent->fRasterX                = thrown->Vx();
               nnEvent->fRasterY                = thrown->Vy();

               vertex.SetXYZ(thrown->Vx(),thrown->Vy(),0.0);//thrown->Vz());
               p_naive = bigcalPlane->fPosition - vertex; 

               nnEvent->fDelta_Energy           = thrown->Energy()*1000.0 - bigcalPlane->fEnergy;
               nnEvent->fDelta_Theta            = p.Theta() - p_naive.Theta();//bigcalPlane->fPosition.Theta();//aCluster->GetTheta();
               nnEvent->fDelta_Phi              = p.Phi()   - p_naive.Phi();//bigcalPlane->fPosition.Phi();//aCluster->GetPhi();

               nnEvent->fBigcalPlaneTheta       = p_naive.Theta();//bigcalPlane->fPosition.Theta();
               nnEvent->fBigcalPlanePhi         = p_naive.Phi();//bigcalPlane->fPosition.Phi();

               nnEvent->fBigcalPlaneDeltaPTheta = bigcalPlane->fMomentum.Theta() - p_naive.Theta();//bigcalPlane->fPosition.Theta();
               nnEvent->fBigcalPlaneDeltaPPhi   = bigcalPlane->fMomentum.Phi()   - p_naive.Phi();//bigcalPlane->fPosition.Phi();

               nnEvent->fBigcalPlaneX           = bigcalPlane->fLocalPosition.X();
               nnEvent->fBigcalPlaneY           = bigcalPlane->fLocalPosition.Y();
               nnEvent->fBigcalPlaneEnergy      = bigcalPlane->fEnergy;

               if( TMath::Abs(nnEvent->fBigcalPlaneY) < 102 )
               if( TMath::Abs(nnEvent->fBigcalPlaneX) < 55 )
               if( nnEvent->fDelta_Energy < 30.0 && nnEvent->fDelta_Energy > 0.0 )
               if( bigcalPlane->fPID == signalPID)
                  if( thrown->GetPdgCode() == signalPID ){
                     //nnEvent->fBackground = false;
                     nnt->Fill();
                  }
            } // bigcal plane loop
         } // cluster loop

      }
   }
   std::cout << "\n";
   //nnt->FlushBaskets();
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

   //nnt->StartViewer();
   gROOT->ProcessLine(".q");
   return(0);
}
