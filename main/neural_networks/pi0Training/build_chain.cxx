/*!   Adds the events to the tree; 
 */
Int_t build_chain(Int_t number=10) {
 
//   Create the chain as above
   TChain chain("betaDetectors1");
   chain.Add("data/rootfiles/InSANE1266.0.root");
   chain.Add("data/rootfiles/InSANE1267.0.root");

   SANEEvents * events = new SANEEvents(&chain);
   events->SetClusterBranches(&chain);
   
   // Create a tree for training the Neural network
   Double_t energy,energy_cluster;
   Double_t theta,phi;
   Double_t x_cluster,y_cluster; 
   Double_t xsigma_cluster,ysigma_cluster;
   Double_t xskew_cluster,yskew_cluster;
   Double_t delta_energy,delta_theta,delta_phi;

   BIGCALCluster * aCluster = new BIGCALCluster;
   BETAG4MonteCarloThrownParticle * thrown = 0;
   InSANEFakePlaneHit * bigcalPlane = 0;

   TFile * NNFile = new TFile("data/NNfile.root","UPDATE");
   TTree * nnt = 0;

/*   nnt = (TTree *)NNFile->FindObject(Form("nnt%d",number));*/
/*   NNFile->F(Form("nnt"),nnt);*/
/*   if(!nnt) {*/
      std::cout << "Creating nnt Tree \n";
      nnt = new TTree(Form("nnt%d",number),"training tree");
      nnt->Branch("energy", &energy, "energy/D");
      nnt->Branch("theta", &theta, "theta/D");
      nnt->Branch("phi", &phi, "phi/D");
      nnt->Branch("delta_energy", &delta_energy, "delta_energy/D");
      nnt->Branch("delta_theta", &delta_theta, "delta_theta/D");
      nnt->Branch("delta_phi", &delta_phi, "delta_phi/D");
      nnt->Branch("energy_cluster", &energy_cluster, "energy_cluster/D");
      nnt->Branch("x_cluster", &x_cluster, "x_cluster/D");
      nnt->Branch("y_cluster", &y_cluster, "y_cluster/D");
      nnt->Branch("xsigma_cluster", &xsigma_cluster, "xsigma_cluster/D");
      nnt->Branch("ysigma_cluster", &ysigma_cluster, "ysigma_cluster/D");
      nnt->Branch("xskew_cluster", &xskew_cluster, "xskew_cluster/D");
      nnt->Branch("yskew_cluster", &yskew_cluster, "yskew_cluster/D");
 /*  } else {
    nnt->SetBranchAddress("energy", &energy);
    nnt->SetBranchAddress("theta", &theta);
    nnt->SetBranchAddress("phi", &phi);
    nnt->SetBranchAddress("delta_energy", &delta_energy);
    nnt->SetBranchAddress("delta_theta", &delta_theta);
    nnt->SetBranchAddress("delta_phi", &delta_phi);
    nnt->SetBranchAddress("energy_cluster", &energy_cluster);
    nnt->SetBranchAddress("x_cluster", &x_cluster);
    nnt->SetBranchAddress("y_cluster", &y_cluster);
    nnt->SetBranchAddress("xsigma_cluster", &xsigma_cluster);
    nnt->SetBranchAddress("ysigma_cluster", &ysigma_cluster);
    nnt->SetBranchAddress("xskew_cluster", &xskew_cluster);
    nnt->SetBranchAddress("yskew_cluster", &yskew_cluster);
   }*/

   for (int i = 0;i< chain.GetEntries();i++ ) if(i>=1001){
/*	   std::cout << "entry : " << i << "\n";*/
     chain.GetEntry(i);
//      clusterAnalysis->fClusterTree->GetEntryWithIndex(events->BETA->fRunNumber,events->BETA->fEventNumber);
/*     std::cout << "Test\n";*/
     if( events->CLUSTER->fClusters->GetEntries() > 0 && events->MC->fThrownParticles->GetEntries() > 0){


// std::cout << "got here !!! \n";

     thrown = (BETAG4MonteCarloThrownParticle*)(*(events->MC->fThrownParticles))[0];
     aCluster  = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[0];

     energy_cluster = aCluster->fTotalE;
     x_cluster = aCluster->GetXmoment();
     y_cluster = aCluster->GetYmoment();
     xsigma_cluster = aCluster->GetXStdDeviation();
     ysigma_cluster = aCluster->GetYStdDeviation();
     xskew_cluster = aCluster->GetXSkewness();
     yskew_cluster = aCluster->GetYSkewness();


     theta = thrown->fTheta;
     phi = thrown->fPhi;
     energy = thrown->fEnergy;

     delta_energy  = energy - energy_cluster;
     delta_phi     = phi - aCluster->GetPhi();
     delta_theta   = theta - aCluster->GetTheta();

//     std::cout << " energy " << energy << " cluster_energy " << energy_cluster << "\n";

     nnt->Fill();
     }
   }
   nnt->FlushBaskets();
   nnt->Write();
   NNFile->Flush();
   NNFile->Write();

//   nnt->StartViewer();
   

// Maximum Block energy
///////////
/*  if (!gROOT->GetClass("TMultiLayerPerceptron")) {
     gSystem->Load("libMLP");
  }

   Int_t ntrain=500;
   Int_t nHidden = 4;
/////////// Here is the network:
*/
/*    mlp =   new TMultiLayerPerceptron (Form("@energy_cluster,@x_cluster,@y_cluster,@xsigma_cluster,@ysigma_cluster,@xskew_cluster,@yskew_cluster:%d:energy,theta,phi",nHidden),nnt);*/
/*
    mlp =   new TMultiLayerPerceptron (Form("@energy_cluster,@x_cluster,@y_cluster,@xsigma_cluster,@ysigma_cluster,@xskew_cluster,@yskew_cluster:%d:delta_energy,delta_theta,delta_phi",nHidden),nnt);

    mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);
    mlp->Train(ntrain, "text,graph,update=10");
  mlp->Export("NNParallel");
  TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(2,2);
   TMLPAnalyzer ana(mlp);
   // Initialisation
   ana.GatherInformations();
   // output to the console
   ana.CheckNetwork();
   mlpa_canvas->cd(1);
   // shows how each variable influences the network
   ana.DrawDInputs();
   mlpa_canvas->cd(2);
   // shows the network structure
   mlp->Draw();
   mlpa_canvas->cd(3);
   // draws the resulting network
   ana.DrawNetwork(0,"energy>1","");
   mlpa_canvas->cd(4);
// OBSERVATION :
// NN does better without the maximum block positions (xy_max)
// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

*/


return(0);
}
