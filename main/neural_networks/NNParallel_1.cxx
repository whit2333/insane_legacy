#include "NNParallel.h"
#include <cmath>

double NNParallel::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - 2421.01)/1095.44;
   input1 = (in1 - -4.26397)/34.9728;
   input2 = (in2 - -0.518658)/61.7638;
   input3 = (in3 - 1.57616)/0.425325;
   input4 = (in4 - 1.73153)/0.449503;
   input5 = (in5 - -20.6014)/3275.03;
   input6 = (in6 - 3.42352)/572.564;
   switch(index) {
     case 0:
         return neuron0xe0ed0d0();
     case 1:
         return neuron0xce907c8();
     case 2:
         return neuron0xce8b5b8();
     default:
         return 0.;
   }
}

double NNParallel::Value(int index, double* input) {
   input0 = (input[0] - 2421.01)/1095.44;
   input1 = (input[1] - -4.26397)/34.9728;
   input2 = (input[2] - -0.518658)/61.7638;
   input3 = (input[3] - 1.57616)/0.425325;
   input4 = (input[4] - 1.73153)/0.449503;
   input5 = (input[5] - -20.6014)/3275.03;
   input6 = (input[6] - 3.42352)/572.564;
   switch(index) {
     case 0:
         return neuron0xe0ed0d0();
     case 1:
         return neuron0xce907c8();
     case 2:
         return neuron0xce8b5b8();
     default:
         return 0.;
   }
}

double NNParallel::neuron0xb57b748() {
   return input0;
}

double NNParallel::neuron0xe0ecce0() {
   return input1;
}

double NNParallel::neuron0xe0eced8() {
   return input2;
}

double NNParallel::neuron0xe3bac28() {
   return input3;
}

double NNParallel::neuron0xe3bae28() {
   return input4;
}

double NNParallel::neuron0xe3bb028() {
   return input5;
}

double NNParallel::neuron0xe3bb228() {
   return input6;
}

double NNParallel::input0xce90bc8() {
   double input = -873.639;
   input += synapse0xe3bb350();
   input += synapse0xb5e5a30();
   input += synapse0xce90d30();
   input += synapse0xce90d58();
   input += synapse0xce90d80();
   input += synapse0xce90da8();
   input += synapse0xce90dd0();
   return input;
}

double NNParallel::neuron0xce90bc8() {
   double input = input0xce90bc8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParallel::input0xce90df8() {
   double input = -4.74855;
   input += synapse0xce90ff0();
   input += synapse0xce91018();
   input += synapse0xce91040();
   input += synapse0xce91068();
   input += synapse0xce91090();
   input += synapse0xce910b8();
   input += synapse0xce910e0();
   return input;
}

double NNParallel::neuron0xce90df8() {
   double input = input0xce90df8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParallel::input0xce91108() {
   double input = -3.23855;
   input += synapse0xce91300();
   input += synapse0xce91328();
   input += synapse0xce91350();
   input += synapse0xce91400();
   input += synapse0xce91428();
   input += synapse0xce91450();
   input += synapse0xce91478();
   return input;
}

double NNParallel::neuron0xce91108() {
   double input = input0xce91108();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParallel::input0xce914a0() {
   double input = -8.37318;
   input += synapse0xce91650();
   input += synapse0xce91678();
   input += synapse0xce916a0();
   input += synapse0xce916c8();
   input += synapse0xce916f0();
   input += synapse0xce91718();
   input += synapse0xce91740();
   return input;
}

double NNParallel::neuron0xce914a0() {
   double input = input0xce914a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNParallel::input0xe0ed0d0() {
   double input = 121.497;
   input += synapse0xce917f8();
   input += synapse0xe0ed240();
   input += synapse0xb57cad0();
   input += synapse0xce8b590();
   return input;
}

double NNParallel::neuron0xe0ed0d0() {
   double input = input0xe0ed0d0();
   return (input * 1)+0;
}

double NNParallel::input0xce907c8() {
   double input = -0.0507168;
   input += synapse0xce909a0();
   input += synapse0xce91378();
   input += synapse0xce913a0();
   input += synapse0xce913c8();
   return input;
}

double NNParallel::neuron0xce907c8() {
   double input = input0xce907c8();
   return (input * 1)+0;
}

double NNParallel::input0xce8b5b8() {
   double input = -0.22908;
   input += synapse0xce8b790();
   input += synapse0xce8b7b8();
   input += synapse0xce8b7e0();
   input += synapse0xce8b808();
   return input;
}

double NNParallel::neuron0xce8b5b8() {
   double input = input0xce8b5b8();
   return (input * 1)+0;
}

double NNParallel::synapse0xe3bb350() {
   return (neuron0xb57b748()*-519.865);
}

double NNParallel::synapse0xb5e5a30() {
   return (neuron0xe0ecce0()*15.351);
}

double NNParallel::synapse0xce90d30() {
   return (neuron0xe0eced8()*-20.8307);
}

double NNParallel::synapse0xce90d58() {
   return (neuron0xe3bac28()*-4.26715);
}

double NNParallel::synapse0xce90d80() {
   return (neuron0xe3bae28()*12.3986);
}

double NNParallel::synapse0xce90da8() {
   return (neuron0xe3bb028()*1110.51);
}

double NNParallel::synapse0xce90dd0() {
   return (neuron0xe3bb228()*-13.1966);
}

double NNParallel::synapse0xce90ff0() {
   return (neuron0xb57b748()*0.356772);
}

double NNParallel::synapse0xce91018() {
   return (neuron0xe0ecce0()*-0.18148);
}

double NNParallel::synapse0xce91040() {
   return (neuron0xe0eced8()*-0.128791);
}

double NNParallel::synapse0xce91068() {
   return (neuron0xe3bac28()*-3.01506);
}

double NNParallel::synapse0xce91090() {
   return (neuron0xe3bae28()*0.250723);
}

double NNParallel::synapse0xce910b8() {
   return (neuron0xe3bb028()*-102.411);
}

double NNParallel::synapse0xce910e0() {
   return (neuron0xe3bb228()*-0.816609);
}

double NNParallel::synapse0xce91300() {
   return (neuron0xb57b748()*0.484267);
}

double NNParallel::synapse0xce91328() {
   return (neuron0xe0ecce0()*-0.0211071);
}

double NNParallel::synapse0xce91350() {
   return (neuron0xe0eced8()*1.6757);
}

double NNParallel::synapse0xce91400() {
   return (neuron0xe3bac28()*0.611193);
}

double NNParallel::synapse0xce91428() {
   return (neuron0xe3bae28()*0.158946);
}

double NNParallel::synapse0xce91450() {
   return (neuron0xe3bb028()*0.434442);
}

double NNParallel::synapse0xce91478() {
   return (neuron0xe3bb228()*-134.179);
}

double NNParallel::synapse0xce91650() {
   return (neuron0xb57b748()*0.358511);
}

double NNParallel::synapse0xce91678() {
   return (neuron0xe0ecce0()*0.167243);
}

double NNParallel::synapse0xce916a0() {
   return (neuron0xe0eced8()*-0.850697);
}

double NNParallel::synapse0xce916c8() {
   return (neuron0xe3bac28()*-0.629559);
}

double NNParallel::synapse0xce916f0() {
   return (neuron0xe3bae28()*-3.72245);
}

double NNParallel::synapse0xce91718() {
   return (neuron0xe3bb028()*2.28578);
}

double NNParallel::synapse0xce91740() {
   return (neuron0xe3bb228()*37.2213);
}

double NNParallel::synapse0xce917f8() {
   return (neuron0xce90bc8()*951.502);
}

double NNParallel::synapse0xe0ed240() {
   return (neuron0xce90df8()*679.37);
}

double NNParallel::synapse0xb57cad0() {
   return (neuron0xce91108()*667.363);
}

double NNParallel::synapse0xce8b590() {
   return (neuron0xce914a0()*816.628);
}

double NNParallel::synapse0xce909a0() {
   return (neuron0xce90bc8()*0.17445);
}

double NNParallel::synapse0xce91378() {
   return (neuron0xce90df8()*-0.157534);
}

double NNParallel::synapse0xce913a0() {
   return (neuron0xce91108()*-0.0244264);
}

double NNParallel::synapse0xce913c8() {
   return (neuron0xce914a0()*0.540406);
}

double NNParallel::synapse0xce8b790() {
   return (neuron0xce90bc8()*-0.903323);
}

double NNParallel::synapse0xce8b7b8() {
   return (neuron0xce90df8()*3.04728);
}

double NNParallel::synapse0xce8b7e0() {
   return (neuron0xce91108()*0.88196);
}

double NNParallel::synapse0xce8b808() {
   return (neuron0xce914a0()*-1.96302);
}

