#include "NNPerpendicular.h"
#include <cmath>

double NNPerpendicular::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - 1202.05)/361.077;
   input1 = (in1 - -33.7924)/24.1779;
   input2 = (in2 - 29.2984)/51.556;
   input3 = (in3 - 1.48301)/0.510415;
   input4 = (in4 - 1.65903)/0.522318;
   input5 = (in5 - 20.9423)/14016.1;
   input6 = (in6 - -61.8685)/36714.5;
   switch(index) {
     case 0:
         return neuron0xeb128c8();
     case 1:
         return neuron0xaaafd38();
     case 2:
         return neuron0xaab0e30();
     default:
         return 0.;
   }
}

double NNPerpendicular::Value(int index, double* input) {
   input0 = (input[0] - 1202.05)/361.077;
   input1 = (input[1] - -33.7924)/24.1779;
   input2 = (input[2] - 29.2984)/51.556;
   input3 = (input[3] - 1.48301)/0.510415;
   input4 = (input[4] - 1.65903)/0.522318;
   input5 = (input[5] - 20.9423)/14016.1;
   input6 = (input[6] - -61.8685)/36714.5;
   switch(index) {
     case 0:
         return neuron0xeb128c8();
     case 1:
         return neuron0xaaafd38();
     case 2:
         return neuron0xaab0e30();
     default:
         return 0.;
   }
}

double NNPerpendicular::neuron0xeb12a00() {
   return input0;
}

double NNPerpendicular::neuron0xbba92d8() {
   return input1;
}

double NNPerpendicular::neuron0xbba94d0() {
   return input2;
}

double NNPerpendicular::neuron0xbba9738() {
   return input3;
}

double NNPerpendicular::neuron0xbba9938() {
   return input4;
}

double NNPerpendicular::neuron0xbba9b38() {
   return input5;
}

double NNPerpendicular::neuron0xaaafb60() {
   return input6;
}

double NNPerpendicular::input0xaab0138() {
   double input = 11.8365;
   input += synapse0xd207828();
   input += synapse0xaab02e8();
   input += synapse0xaab0310();
   input += synapse0xaab0338();
   input += synapse0xaab0360();
   input += synapse0xaab0388();
   input += synapse0xaab03b0();
   return input;
}

double NNPerpendicular::neuron0xaab0138() {
   double input = input0xaab0138();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpendicular::input0xaab03d8() {
   double input = -11.7951;
   input += synapse0xaab05d0();
   input += synapse0xaab05f8();
   input += synapse0xaab0620();
   input += synapse0xaab0648();
   input += synapse0xaab0670();
   input += synapse0xaab0698();
   input += synapse0xaab06c0();
   return input;
}

double NNPerpendicular::neuron0xaab03d8() {
   double input = input0xaab03d8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpendicular::input0xaab06e8() {
   double input = -6.8543;
   input += synapse0xaab08e0();
   input += synapse0xaab0908();
   input += synapse0xaab0930();
   input += synapse0xaab09e0();
   input += synapse0xaab0a08();
   input += synapse0xaab0a30();
   input += synapse0xaab0a58();
   return input;
}

double NNPerpendicular::neuron0xaab06e8() {
   double input = input0xaab06e8();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpendicular::input0xaab0a80() {
   double input = -11.6444;
   input += synapse0xaab0c30();
   input += synapse0xaab0c58();
   input += synapse0xaab0c80();
   input += synapse0xaab0ca8();
   input += synapse0xaab0cd0();
   input += synapse0xaab0cf8();
   input += synapse0xaab0d20();
   return input;
}

double NNPerpendicular::neuron0xaab0a80() {
   double input = input0xaab0a80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double NNPerpendicular::input0xeb128c8() {
   double input = 83.4188;
   input += synapse0xaab0d90();
   input += synapse0xaab0db8();
   input += synapse0xaab0de0();
   input += synapse0xaab0e08();
   return input;
}

double NNPerpendicular::neuron0xeb128c8() {
   double input = input0xeb128c8();
   return (input * 1)+0;
}

double NNPerpendicular::input0xaaafd38() {
   double input = 0.13931;
   input += synapse0xaaaff10();
   input += synapse0xeb129c8();
   input += synapse0xaab0958();
   input += synapse0xaab0980();
   return input;
}

double NNPerpendicular::neuron0xaaafd38() {
   double input = input0xaaafd38();
   return (input * 1)+0;
}

double NNPerpendicular::input0xaab0e30() {
   double input = -0.278089;
   input += synapse0xaab1008();
   input += synapse0xaab1030();
   input += synapse0xaab1058();
   input += synapse0xaab1080();
   return input;
}

double NNPerpendicular::neuron0xaab0e30() {
   double input = input0xaab0e30();
   return (input * 1)+0;
}

double NNPerpendicular::synapse0xd207828() {
   return (neuron0xeb12a00()*0.566656);
}

double NNPerpendicular::synapse0xaab02e8() {
   return (neuron0xbba92d8()*-0.585503);
}

double NNPerpendicular::synapse0xaab0310() {
   return (neuron0xbba94d0()*38.9591);
}

double NNPerpendicular::synapse0xaab0338() {
   return (neuron0xbba9738()*0.204344);
}

double NNPerpendicular::synapse0xaab0360() {
   return (neuron0xbba9938()*0.408961);
}

double NNPerpendicular::synapse0xaab0388() {
   return (neuron0xbba9b38()*3.07974);
}

double NNPerpendicular::synapse0xaab03b0() {
   return (neuron0xaaafb60()*-20.7435);
}

double NNPerpendicular::synapse0xaab05d0() {
   return (neuron0xeb12a00()*-4.51213);
}

double NNPerpendicular::synapse0xaab05f8() {
   return (neuron0xbba92d8()*-0.17606);
}

double NNPerpendicular::synapse0xaab0620() {
   return (neuron0xbba94d0()*-0.275176);
}

double NNPerpendicular::synapse0xaab0648() {
   return (neuron0xbba9738()*0.264566);
}

double NNPerpendicular::synapse0xaab0670() {
   return (neuron0xbba9938()*-0.00340511);
}

double NNPerpendicular::synapse0xaab0698() {
   return (neuron0xbba9b38()*-2.27271);
}

double NNPerpendicular::synapse0xaab06c0() {
   return (neuron0xaaafb60()*0.0812773);
}

double NNPerpendicular::synapse0xaab08e0() {
   return (neuron0xeb12a00()*-4.74899);
}

double NNPerpendicular::synapse0xaab0908() {
   return (neuron0xbba92d8()*-0.110563);
}

double NNPerpendicular::synapse0xaab0930() {
   return (neuron0xbba94d0()*-0.354284);
}

double NNPerpendicular::synapse0xaab09e0() {
   return (neuron0xbba9738()*0.07822);
}

double NNPerpendicular::synapse0xaab0a08() {
   return (neuron0xbba9938()*-0.217634);
}

double NNPerpendicular::synapse0xaab0a30() {
   return (neuron0xbba9b38()*-1.37344);
}

double NNPerpendicular::synapse0xaab0a58() {
   return (neuron0xaaafb60()*-0.0781207);
}

double NNPerpendicular::synapse0xaab0c30() {
   return (neuron0xeb12a00()*0.116334);
}

double NNPerpendicular::synapse0xaab0c58() {
   return (neuron0xbba92d8()*-10.2564);
}

double NNPerpendicular::synapse0xaab0c80() {
   return (neuron0xbba94d0()*0.184412);
}

double NNPerpendicular::synapse0xaab0ca8() {
   return (neuron0xbba9738()*-0.478282);
}

double NNPerpendicular::synapse0xaab0cd0() {
   return (neuron0xbba9938()*-0.0122126);
}

double NNPerpendicular::synapse0xaab0cf8() {
   return (neuron0xbba9b38()*62.016);
}

double NNPerpendicular::synapse0xaab0d20() {
   return (neuron0xaaafb60()*-3.23899);
}

double NNPerpendicular::synapse0xaab0d90() {
   return (neuron0xaab0138()*132.47);
}

double NNPerpendicular::synapse0xaab0db8() {
   return (neuron0xaab03d8()*688.673);
}

double NNPerpendicular::synapse0xaab0de0() {
   return (neuron0xaab06e8()*622.051);
}

double NNPerpendicular::synapse0xaab0e08() {
   return (neuron0xaab0a80()*640.573);
}

double NNPerpendicular::synapse0xaaaff10() {
   return (neuron0xaab0138()*0.0400335);
}

double NNPerpendicular::synapse0xeb129c8() {
   return (neuron0xaab03d8()*-0.099956);
}

double NNPerpendicular::synapse0xaab0958() {
   return (neuron0xaab06e8()*-0.707366);
}

double NNPerpendicular::synapse0xaab0980() {
   return (neuron0xaab0a80()*-0.0139248);
}

double NNPerpendicular::synapse0xaab1008() {
   return (neuron0xaab0138()*-0.445127);
}

double NNPerpendicular::synapse0xaab1030() {
   return (neuron0xaab03d8()*0.176791);
}

double NNPerpendicular::synapse0xaab1058() {
   return (neuron0xaab06e8()*0.34529);
}

double NNPerpendicular::synapse0xaab1080() {
   return (neuron0xaab0a80()*-0.0439084);
}

