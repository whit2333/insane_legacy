/*!  Builds the  neural network
 */
Int_t BuildNeuralNetwork(Int_t number=8) {
   if (!gROOT->GetClass("TMultiLayerPerceptron")) {
      gSystem->Load("libMLP");
   }

   Int_t ntrain  = 50;
   Int_t nHidden = 8;

   TFile * file = new TFile("data/NNfile.root","READ");
   TTree * nnt = (TTree*)gROOT->FindObject(Form("nnt%d",number));

   TCanvas* c = new TCanvas("NNinputCanvas","Network analysis");
   c->Divide(3,2);
   c->cd(1);
   nnt->Draw("y_cluster:x_cluster>>clusterXY(200,-60,60,200,-120,120)","","colz",1000000000,1001);
   c->cd(2);
   nnt->Draw("ysigma_cluster:xsigma_cluster>>sigmaXY(200,0,5,200,0,5)","","colz",1000000000,1001);
   c->cd(3);
   nnt->Draw("yskew_cluster:xskew_cluster>>skewXY(200,-15,15,200,-15,15)","","colz",1000000000,1001);
   c->cd(4);
   nnt->Draw("delta_energy:energy_cluster>>deltaenergyXY(200,0,5900,200,0,2000)","","colz",1000000000,1001);
   c->cd(5);
   nnt->Draw("delta_theta*180.0/TMath::Pi():energy_cluster>>deltathetaXY(200,0,5900,200,-20,20)","","colz",1000000000,1001);
   c->cd(6);
   nnt->Draw("delta_phi*180.0/TMath::Pi():energy_cluster>>deltaphiXY(200,0,5900,200,-20,50)","","colz",1000000000,1001);
/*    mlp =   new TMultiLayerPerceptron (Form("@energy_cluster,@x_cluster,@y_cluster,@xsigma_cluster,@ysigma_cluster,@xskew_cluster,@yskew_cluster:%d:energy,theta,phi",nHidden),nnt);*/

   mlp = new TMultiLayerPerceptron ( Form(
      "energy_cluster,x_cluster,y_cluster,xsigma_cluster,ysigma_cluster,xskew_cluster,yskew_cluster:%d:delta_energy,delta_theta,delta_phi" ,nHidden),nnt);

   mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);
   mlp->Train(ntrain, "text,graph,update=10");
   mlp->Export("NNtemp");

   TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis");
   mlpa_canvas->Divide(2,2);
   TMLPAnalyzer ana(mlp);
   // Initialisation
   ana.GatherInformations();
   // output to the console
   ana.CheckNetwork();
   mlpa_canvas->cd(1);
   // shows how each variable influences the network
   ana.DrawDInputs();
   mlpa_canvas->cd(2);
   // shows the network structure
   mlp->Draw();
   mlpa_canvas->cd(3);
   // draws the resulting network
   ana.DrawNetwork(1,"x_cluster>-59","x_cluster<-59");
   mlpa_canvas->cd(4);
   ana.DrawTruthDeviations();
// OBSERVATION :
// NN does better without the maximum block positions (xy_max)
// Also 10 seems to be the sweet spot for the number of neurons in a single hidden layer 

mlpa_canvas->Update();


return(0);
}