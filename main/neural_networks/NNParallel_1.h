#ifndef NNParallel_h
#define NNParallel_h

class NNParallel { 
public:
   NNParallel() {}
   ~NNParallel() {}
   double Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6);
   double Value(int index, double* input);
private:
   double input0;
   double input1;
   double input2;
   double input3;
   double input4;
   double input5;
   double input6;
   double neuron0xb57b748();
   double neuron0xe0ecce0();
   double neuron0xe0eced8();
   double neuron0xe3bac28();
   double neuron0xe3bae28();
   double neuron0xe3bb028();
   double neuron0xe3bb228();
   double input0xce90bc8();
   double neuron0xce90bc8();
   double input0xce90df8();
   double neuron0xce90df8();
   double input0xce91108();
   double neuron0xce91108();
   double input0xce914a0();
   double neuron0xce914a0();
   double input0xe0ed0d0();
   double neuron0xe0ed0d0();
   double input0xce907c8();
   double neuron0xce907c8();
   double input0xce8b5b8();
   double neuron0xce8b5b8();
   double synapse0xe3bb350();
   double synapse0xb5e5a30();
   double synapse0xce90d30();
   double synapse0xce90d58();
   double synapse0xce90d80();
   double synapse0xce90da8();
   double synapse0xce90dd0();
   double synapse0xce90ff0();
   double synapse0xce91018();
   double synapse0xce91040();
   double synapse0xce91068();
   double synapse0xce91090();
   double synapse0xce910b8();
   double synapse0xce910e0();
   double synapse0xce91300();
   double synapse0xce91328();
   double synapse0xce91350();
   double synapse0xce91400();
   double synapse0xce91428();
   double synapse0xce91450();
   double synapse0xce91478();
   double synapse0xce91650();
   double synapse0xce91678();
   double synapse0xce916a0();
   double synapse0xce916c8();
   double synapse0xce916f0();
   double synapse0xce91718();
   double synapse0xce91740();
   double synapse0xce917f8();
   double synapse0xe0ed240();
   double synapse0xb57cad0();
   double synapse0xce8b590();
   double synapse0xce909a0();
   double synapse0xce91378();
   double synapse0xce913a0();
   double synapse0xce913c8();
   double synapse0xce8b790();
   double synapse0xce8b7b8();
   double synapse0xce8b7e0();
   double synapse0xce8b808();
};

#endif // NNParallel_h

