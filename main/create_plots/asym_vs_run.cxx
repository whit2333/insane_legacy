/*! Creates the plots vs Run Number

  Asymmetry vs run number 

  Adds to two TMultiGraph objects 
 */
Int_t asym_vs_run(){

   /// Sets up a canvas
   TCanvas *c1 = new TCanvas("c1","A Simple Graph Example",200,10,700,500);
  // c1->SetFillColor(42);
   c1->SetGrid();
   c1->Divide(1,2);

   /// The vectors which will be used to hold the values pulled from each run
   std::vector<double> xvals;
   std::vector<double> Avals;
   std::vector<double> PTvals;
   std::vector<double> PBvals;
   std::vector<double> PBTvals;
   std::vector<double> APhysvals;
   std::vector<double> NEventsvals;

   aman->BuildQueue2("lists/para/5_9GeV/good_NH3_para59_1.txt");

  for(int i = 0;i<aman->fRunQueue->size();i++) {
    rman->SetRun(aman->fRunQueue->at(i),0,"Q");
    if(rman->fCurrentRun->fNEvents > 100000 && TMath::Abs(rman->fCurrentRun->fTargetOfflinePolarization)>10.0 && rman->fCurrentRun->fAnalysisPass >= 1 ) {
    xvals.push_back(aman->fRunQueue->at(i));
    Avals.push_back(rman->fCurrentRun->fTotalAsymmetry);
    PTvals.push_back(rman->fCurrentRun->fStartingPolarization/100.0);
    PBvals.push_back(rman->fCurrentRun->fBeamPolarization/100.0);
    PBTvals.push_back(PTvals.back()*PBvals.back());
    NEventsvals.push_back(rman->fCurrentRun->fNBeta2Events);
    APhysvals.push_back(( double)rman->fCurrentRun->fTotalAsymmetry/PBTvals.back() /*/( ((double) rman->fCurrentRun->fTargetOfflinePolarization) /100.0) */);
    if( rman->fCurrentRun->fStartingPolarization == 0.0 ) std::cout << "yousuck\n";
    }
  }
//    double * xv;
//    double * xy;
// memcpy( xv, &xv[0], sizeof( int ) * myVector.size() );

   Int_t N = xvals.size();

   TVectorD x;
   x.Use(N, &xvals[0]);

   TVectorD a;
   a.Use(N, &Avals[0]);

   TVectorD aphys;
   aphys.Use(N, &APhysvals[0]);

   TVectorD pt;
   pt.Use(N, &PTvals[0]);

   TVectorD pb;
   pb.Use(N, &PBvals[0]);

   TVectorD pbt;
   pbt.Use(N, &PBTvals[0]);
   
   TVectorD nevents;
   nevents.Use(N, &NEventsvals[0]);
 
 
   TMultiGraph *mg = new TMultiGraph();

   grA = new TGraph(x,aphys);
   grA->SetLineColor(2);
   grA->SetLineWidth(4);
   grA->SetMarkerColor(2);
   grA->SetMarkerSize(1.5);
   grA->SetMarkerStyle(21);
   grA->SetTitle("NEvents vs Run number");
   grA->GetXaxis()->SetTitle("Run Number");
   grA->GetYaxis()->SetTitle("events");
//grA->Draw("AP");

   grPT = new TGraph(x,pbt);
   grPT->SetMarkerColor(1);
   grPT->SetLineColor(2);
   grPT->SetLineWidth(3);
   grPT->SetMarkerSize(1.5);
   grPT->SetMarkerStyle(23);

   grNEvents = new TGraph(x,nevents);
   grNEvents->SetMarkerColor(1);
   grNEvents->SetLineColor(2);
   grNEvents->SetLineWidth(3);
   grNEvents->SetMarkerSize(1.5);
   grNEvents->SetMarkerStyle(23);

   mg->Add(grA,"P");
   mg->Add(grPT,"P");

   c1->cd(1);
   mg->Draw("a");

   c1->cd(2);
   grNEvents->Draw("ap");

   c1->SaveAs("results/asym_vs_run.ps");
   c1->SaveAs("results/asym_vs_run.png");

}
