Int_t asym_carbon(){


  TCanvas *c1 = new TCanvas("c1","A Simple Graph Example",200,10,700,500);
  // c1->SetFillColor(42);
   c1->SetGrid();
   c1->Divide(1,2);

   std::vector<double> xvals;
   std::vector<double> Avals;
   std::vector<double> APhysvals;
   std::vector<double> PTvals;
   std::vector<double> PBvals;
   std::vector<double> PBTvals;
   std::vector<double> NEventsvals;

//   aman->QueueUpCH2Runs();

  aman->fRunQueue->push_back(72995);
  aman->fRunQueue->push_back(73040);
  aman->fRunQueue->push_back(73039);
  aman->fRunQueue->push_back(73038);
  aman->fRunQueue->push_back(73037);
  aman->fRunQueue->push_back(73036);
  aman->fRunQueue->push_back(73035);
  aman->fRunQueue->push_back(73034);
  aman->fRunQueue->push_back(73033);

  for(int i = 0;i<aman->fRunQueue->size();i++) {
    rman->SetRun( aman->fRunQueue->at(i) );
   
    if(rman->fCurrentRun->IsValid() ) {
/*    fAsymVsRun->Fill(aman->fRunQueue->at(i), rman->fCurrentRun->fNEvents);*/
    xvals.push_back(aman->fRunQueue->at(i));
    Avals.push_back(rman->fCurrentRun->fTotalAsymmetry);
    PTvals.push_back(rman->fCurrentRun->fStartingPolarization/1000.0);
    PBvals.push_back(rman->fCurrentRun->fEndingPolarization/1000.0);
    PBTvals.push_back(rman->fCurrentRun->fTotalAsymmetry/1000.0);
    NEventsvals.push_back(rman->fCurrentRun->fNBeta2Events);
    APhysvals.push_back(rman->fCurrentRun->fTotalAsymmetry/rman->fCurrentRun->fStartingPolarization);
    }
  }
//    double * xv;
//    double * xy;
// memcpy( xv, &xv[0], sizeof( int ) * myVector.size() );

   Int_t N = xvals.size();

   TVectorD x;
   x.Use(N, &xvals[0]);

   TVectorD a;
   a.Use(N, &Avals[0]);

   TVectorD aphys;
   aphys.Use(N, &APhysvals[0]);

   TVectorD pt;
   pt.Use(N, &PTvals[0]);

   TVectorD pb;
   pb.Use(N, &PBvals[0]);

   TVectorD pbt;
   pbt.Use(N, &PBTvals[0]);
   
   TVectorD nevents;
   nevents.Use(N, &NEventsvals[0]);
 
 
   TMultiGraph *mg = new TMultiGraph();

   grA = new TGraph(x,a);
   grA->SetLineColor(2);
   grA->SetLineWidth(4);
   grA->SetMarkerColor(4);
   grA->SetMarkerSize(1.5);
   grA->SetMarkerStyle(21);
   grA->SetTitle("NEvents vs Run number");
   grA->GetXaxis()->SetTitle("Run Number");
   grA->GetYaxis()->SetTitle("events");
//grA->Draw("AP");

   grPT = new TGraph(x,pt);
   grPT->SetMarkerColor(1);
   grPT->SetLineColor(2);
   grPT->SetLineWidth(3);
   grPT->SetMarkerSize(1.5);
   grPT->SetMarkerStyle(23);

   grNEvents = new TGraph(x,nevents);
   grNEvents->SetMarkerColor(1);
   grNEvents->SetLineColor(2);
   grNEvents->SetLineWidth(3);
   grNEvents->SetMarkerSize(1.5);
   grNEvents->SetMarkerStyle(23);

   mg->Add(grA,"P");
   mg->Add(grPT,"P");

   c1->cd(1);
   mg->Draw("a");

   c1->cd(2);
   grNEvents->Draw("ap");


   c1->SaveAs("results/asym_carbon.ps");
   c1->SaveAs("results/asym_carbon.png");
   // fAsymVsRun->Draw();

}
