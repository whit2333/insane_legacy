/*! Creates the plots vs Run Number

  Asymmetry vs run number 
  Adds to two TMultiGraph objects 
 */
#include "InSANERun.h"
#include "TGraphErrors.h"
#include "TList.h"
#include <vector>
#include "TVectorT.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TF1.h"
#include "TString.h"
#include "TSQLResult.h"
#include "TSQLRow.h"
#include "SANEDatabaseManager.h"
#include "InSANERunManager.h"
#include "SANEAnalysisManager.h"

class RunAsymmetrySeries {
public :
   RunAsymmetrySeries(const char * legtext = " " ) ;
   
   ~RunAsymmetrySeries() {
   }

   /** Extracts all data from run necessary for plotting  */
   Int_t AddRun(InSANERun * aRun);

public:

   Int_t fMarkerStyle;
   Int_t fMarkerColor;
   Int_t fNDataPoints;
public: 

   /** creates a list of TGraph objects after adding all runs */
   TList * GenerateGraphs();

   TGraphErrors * fGraph;
   TList fListOfGraphs;
   const char * fLegend;
   std::vector<double> runNumbers;
   std::vector<double> rawAsymmetries;
   std::vector<double> targetPolarizations;
   std::vector<double> beamPolarizations;
   std::vector<double> pbptProducts;
   std::vector<double> physicsAsymmetries;
   std::vector<double> numberOfEventsInRun;
   std::vector<double> statErrors;
   std::vector<double> runErrors;

ClassDef(RunAsymmetrySeries,1)
};
//___________________________________________________________________//

   ClassImp(RunAsymmetrySeries)
//___________________________________________________________________//

   RunAsymmetrySeries::RunAsymmetrySeries(const char * legtext){
      fLegend = legtext;
      fMarkerStyle=20;
      fMarkerColor=2;
      fNDataPoints=0;
   }
//___________________________________________________________________//

   Int_t RunAsymmetrySeries::AddRun(InSANERun * aRun){
      Double_t N1,N2,Pb,Pt;

      Pt = aRun->fTargetOfflinePolarization/100.0;
      Pb = aRun->fBeamPolarization/100.0;
      runErrors.push_back(0.0);
      runNumbers.push_back(aRun->fRunNumber);
      rawAsymmetries.push_back(aRun->fTotalAsymmetry);

         N1 = (double) aRun->fTotalNPlus ;
         N2 = (double) aRun->fTotalNMinus ;

         targetPolarizations.push_back(aRun->fStartingPolarization/1000.0);
         beamPolarizations.push_back(aRun->fBeamPolarization/1000.0);

    /// see mathematica notebook on errors assuming 5% error on beam and target.... for now 
    statErrors.push_back(
    TMath::Sqrt(((0.0025000000000000005*TMath::Power(N1,3) - 
         0.0025000000000000005*TMath::Power(N1,2)*N2 - 
         0.0025000000000000005*N1*TMath::Power(N2,2) + 
         0.0025000000000000005*TMath::Power(N2,3))*TMath::Power(Pb,2) + 
      (0.0025000000000000005*TMath::Power(N1,3) - 
         0.0025000000000000005*TMath::Power(N1,2)*N2 + 
         0.0025000000000000005*TMath::Power(N2,3) + 
         N1*N2*(-0.0025000000000000005*N2 + 4.*TMath::Power(Pb,2)))*
       TMath::Power(Pt,2))/(TMath::Power(N1 + N2,3)*TMath::Power(Pb,4)*TMath::Power(Pt,4))) );

       pbptProducts.push_back(Pb*Pt/10.0);
       numberOfEventsInRun.push_back(aRun->fNBeta2Events);
       physicsAsymmetries.push_back(( double)aRun->fTotalAsymmetry /(Pb*Pt));
      fNDataPoints++;
      return(0);
   }
//___________________________________________________________________//

   TList * RunAsymmetrySeries::GenerateGraphs(){
      Int_t N = runNumbers.size();

      TVectorD x;
      x.Use(N, &runNumbers[0]);
      TVectorD xerr;
      xerr.Use(N, &runErrors[0]);
      TVectorD a;
      a.Use(N, &rawAsymmetries[0]);
      TVectorD aerr;
      aerr.Use(N, &statErrors[0]);
      TVectorD aphys;
      aphys.Use(N, &physicsAsymmetries[0]);
      TVectorD pt;
      pt.Use(N, &targetPolarizations[0]);
      TVectorD pb;
      pb.Use(N, &beamPolarizations[0]);
      TVectorD pbt;
      pbt.Use(N, &pbptProducts[0]);
      TVectorD nevents;
      nevents.Use(N, &numberOfEventsInRun[0]);

      fGraph = new TGraphErrors(x,aphys,xerr,aerr);
      fGraph->SetLineColor(fMarkerColor);
      fGraph->SetLineWidth(2);
      fGraph->SetMarkerColor(fMarkerColor);
      fGraph->SetMarkerSize(1.2);
      fGraph->SetMarkerStyle(fMarkerStyle);
      fGraph->SetTitle(" ");
      fGraph->GetXaxis()->SetTitle("Run Number");
      fGraph->GetYaxis()->SetTitle(" ");
      fListOfGraphs.Add(fGraph);


      TGraph * aGraph = new TGraph(x,pbt);
      aGraph->SetMarkerColor(1);
      aGraph->SetLineColor(2);
      aGraph->SetLineWidth(3);
      aGraph->SetMarkerSize(1.3);
      aGraph->SetMarkerStyle(23);
      aGraph->SetTitle(" ");
      aGraph->GetXaxis()->SetTitle("Run Number");
      aGraph->GetYaxis()->SetTitle(" ");
      fListOfGraphs.Add(aGraph);


      return(&fListOfGraphs);
   }
//___________________________________________________________________//


Int_t create_run_plots(){

   gStyle->SetOptStat(0);
   gROOT->SetStyle("Plain");

   SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();
   SANERunManager * rman = (SANERunManager *)InSANERunManager::GetRunManager();

   /// Sets up a canvas
   TCanvas *c1 = new TCanvas("asymCanvas","Raw Asymmetries",1200,400);
   // c1->SetFillColor(42);
   c1->SetGrid();
   //c1->Divide(1,2);


   RunAsymmetrySeries topPositiveRuns("Top,positive polarization");
   topPositiveRuns.fMarkerColor = 4;
   topPositiveRuns.fMarkerStyle = 20;

   RunAsymmetrySeries bottomPositiveRuns("Bottom,positive polarization");
   bottomPositiveRuns.fMarkerColor = 4;
   bottomPositiveRuns.fMarkerStyle = 24;

   RunAsymmetrySeries topNegativeRuns("Top,negative polarization");
   topNegativeRuns.fMarkerColor = 2;
   topNegativeRuns.fMarkerStyle = 20;

   RunAsymmetrySeries bottomNegativeRuns("Bottom,negative polarization");
   bottomNegativeRuns.fMarkerColor = 2;
   bottomNegativeRuns.fMarkerStyle = 24;

   RunAsymmetrySeries unpolarizedRuns("unpolarized");
   unpolarizedRuns.fMarkerColor = 3;
   unpolarizedRuns.fMarkerStyle = 21;


   /// The vectors which will be used to hold the values pulled from each run
   std::vector<double> runNumbers;

   /// Build up a run queue to be plotted
   //aman->QueueUpPerpendicular(); 
   //aman->QueueUpPerpendicularNH3Runs();
   //aman->QueueUpParallel();
   //aman->QueueUpTestRuns();
   //aman->BuildQueue("run_list.txt",2);
   aman->BuildQueue("replaying_list.txt");

   /// Convenient data holders
   Double_t N1,N2,Pb,Pt;

   for(int i = 0;i<aman->fRunQueue->size();i++) {

      rman->SetRun(aman->fRunQueue->at(i));

      /// If the run has been fully analyzed then use it otherwise skip it out. 
      if(rman->GetCurrentRun())
      if(rman->GetCurrentRun()->fAnalysisPass >= 3  && rman->fCurrentRun->IsValid() ) {

         if(rman->GetCurrentRun()->fTargetCup == InSANERun::kTOP ) {
            if(rman->GetCurrentRun()->fTargetPolarizationSign == InSANERun::kPOSITIVE )
               topPositiveRuns.AddRun(rman->GetCurrentRun() );
            if(rman->GetCurrentRun()->fTargetPolarizationSign == InSANERun::kNEGATIVE)
               topNegativeRuns.AddRun(rman->GetCurrentRun() );
         }

         if(rman->GetCurrentRun()->fTargetCup == InSANERun::kBOTTOM ){
            if(rman->GetCurrentRun()->fTargetPolarizationSign == InSANERun::kPOSITIVE )
               topPositiveRuns.AddRun(rman->GetCurrentRun() );
            if(rman->GetCurrentRun()->fTargetPolarizationSign == InSANERun::kNEGATIVE)
               topNegativeRuns.AddRun(rman->GetCurrentRun() );
         }

         if(rman->GetCurrentRun()->fTargetPolarizationSign == InSANERun::kUNPOLARIZED )
            unpolarizedRuns.AddRun(rman->GetCurrentRun() );
    }
  }

///////////
   std::vector<double> configChangeX;
   std::vector<double> configBeam;
      TString query = " SELECT Start_run,gpbeam FROM exp_configuration; ";
      TSQLResult * res = SANEDatabaseManager::GetManager()->dbServer->Query(query.Data());
      TSQLRow * row;
      for(int i = 0 ;i< res->GetRowCount();i++ ){
         row = res->Next();
         std::cout << row->GetField(0) << "    " << row->GetField(1) << "\n";
         configChangeX.push_back(atoi(row->GetField(0)) );
         configBeam.push_back(atof(row->GetField(1))/100.0 );
      }
   TVectorD configx;
   configx.Use(configChangeX.size(), &configChangeX[0]);

   TVectorD beamEnergy;
   beamEnergy.Use(configBeam.size(), &configBeam[0]);

   TGraph *  grBeam = new TGraph(configx,beamEnergy);
   grBeam->SetMarkerColor(3);
   grBeam->SetLineColor(2);
   grBeam->SetLineWidth(3);
   grBeam->SetMarkerSize(1.6);
   grBeam->SetMarkerStyle(29);

///////////

///////////
   std::vector<double> detChangeX;
   std::vector<double> detChangeY;

      query = " SELECT first_run FROM detector_changes; ";
      /*TSQLResult * */res = SANEDatabaseManager::GetManager()->dbServer->Query(query.Data());
//       TSQLRow * row;
      for(int i = 0 ;i< res->GetRowCount();i++ ){
         row = res->Next();
         std::cout << row->GetField(0) <<  "\n";
         detChangeX.push_back(atoi(row->GetField(0)) );
         detChangeY.push_back(0.1);
      }
   TVectorD detX;
   detX.Use(detChangeX.size(), &detChangeX[0]);

   TVectorD detY;
   detY.Use(detChangeY.size(), &detChangeY[0]);


   TGraph * grDet= new TGraph(detX,detY);
   grDet->SetMarkerColor(6);
   grDet->SetLineColor(2);
   grDet->SetLineWidth(3);
   grDet->SetMarkerSize(1.3);
   grDet->SetMarkerStyle(34);
///////////


   TLegend * leg = new TLegend(0.2,0.75,0.5,0.99);
   leg->SetHeader("Asymmetry vs Run");

   TMultiGraph *mg = new TMultiGraph();
   TGraphErrors * aGraph;
   TListIter grIter1(topPositiveRuns.GenerateGraphs());
   int isFirst=1;
   if(topPositiveRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter1.Next() ) ) {
      mg->Add(aGraph,"P");
      if(isFirst==1)leg->AddEntry(aGraph,topPositiveRuns.fLegend,"p");
      isFirst=0;
   }
   TListIter grIter2(bottomPositiveRuns.GenerateGraphs());
   isFirst=1;
   if(bottomPositiveRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter2.Next() ) ) {
      mg->Add(aGraph,"P");
      if(isFirst==1)leg->AddEntry(aGraph,bottomPositiveRuns.fLegend,"p");
      isFirst=0;
   }
   TListIter grIter3(topNegativeRuns.GenerateGraphs());
   isFirst=1;
   if(topNegativeRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter3.Next() ) ) {
      mg->Add(aGraph,"P");
      if(isFirst==1)leg->AddEntry(aGraph,topNegativeRuns.fLegend,"p");
      isFirst=0;
   }
   TListIter grIter4(bottomNegativeRuns.GenerateGraphs());
   if(bottomNegativeRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter4.Next() ) ) {
      mg->Add(aGraph,"P");
      if(isFirst==1)leg->AddEntry(aGraph,bottomNegativeRuns.fLegend,"p");
       isFirst=0;
  }
   TListIter grIter5(unpolarizedRuns.GenerateGraphs());
   isFirst=1;
   if(unpolarizedRuns.fNDataPoints>0)while ( (aGraph = (TGraphErrors*)grIter5.Next() ) ) {
      mg->Add(aGraph,"P");
      if(isFirst==1)leg->AddEntry(aGraph,unpolarizedRuns.fLegend,"p");
      isFirst=0;
   }
   TF1 * zero = new TF1("f1","0",72000,73500);
   zero->SetLineColor(1);
   zero->SetLineWidth(1);


   mg->Add(grBeam,"P");
   mg->Add(grDet,"P");
   leg->AddEntry(grDet,"Detector Changes","p");
   leg->AddEntry(grBeam,"Config Changes","p");

   //   c1->cd(1);
   mg->SetMaximum(0.15);
   mg->SetMinimum(-0.1);
   mg->Draw("a");
   mg->SetMaximum(0.15);
   mg->SetMinimum(-0.1);
   zero->Draw("same");
   leg->Draw();

//    leg->SetHeader("Asymmetry vs Run");
//    leg->AddEntry(grA,"Asymmetries with errors","lep");
// /*   leg->AddEntry("","Function abs(#frac{sin(x)}{x})","l");*/
//    leg->AddEntry(grPBT,"Pb*Pt/10","p");

//    leg->Draw();

/*
   c1->SaveAs("results/asym_vs_run.ps");
   c1->SaveAs("results/asym_vs_run.png");*/
   return(0);
}
