Int_t quantity_vs_time() {

rman->SetRun(73005);

TTree * t = (TTree*)gROOT->FindObject("Scalers");
TCanvas * c = new TCanvas("chargeAsym","ChargeAsym");
c->Divide(2,2);
c->cd(1);
t->Draw("saneScalerEvent.BCM1ChargeAsymmetry()");
c->cd(2);
t->Draw("saneScalerEvent.BCM2ChargeAsymmetry()");
c->cd(3);
t->Draw("saneScalerEvent.BCM1ChargeAsymmetry():saneScalerEvent.TimeLapsed()","saneScalerEvent.TimeLapsed()>1.8","COLZ");
c->cd(4);
t->Draw("saneScalerEvent.BeamCurrentAverage()");
c->SaveAs("results/scalers/ChargeAsym.png");
}