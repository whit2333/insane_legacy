void cluster_types(Int_t runNumber =72994) {
   TCanvas *c1 = new TCanvas("c1");
   rman->SetRun(runNumber,1);
   TTree *T = (TTree*)rman->fCurrentFile->Get("Clusters");
   T->Draw(   "bigcalClusters.fCherenkovBestNPESum:bigcalClusters.fYSkewness:bigcalClusters.fYStdDeviation:bigcalClusters.fYMoment:bigcalClusterEvent.fNClusters:bigcalClusters.fTotalE","","para");

   TParallelCoord* para = (TParallelCoord*)gPad->GetListOfPrimitives()->FindObject("ParaCoord");

   TParallelCoordVar* npe = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fCherenkovBestNPESum");
   npe->AddRange(new TParallelCoordRange(npe,5,30));



//    TParallelCoordVar* sigx = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fXStdDeviation");
//    sigx->AddRange(new TParallelCoordRange(sigx,0,7.0));

   TParallelCoordVar* sigy = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fYStdDeviation");
   sigy->AddRange(new TParallelCoordRange(sigy,0,7.0));
   TParallelCoordVar* ypos = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fYMoment");
   ypos->AddRange(new TParallelCoordRange(ypos,-106,106.0));

//     TParallelCoordVar* skewx = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fXSkewness");
//    skewx->AddRange(new TParallelCoordRange(skewx,-6,7.0));

    TParallelCoordVar* skewy = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fYSkewness");
   skewy->AddRange(new TParallelCoordRange(skewy,-6,7.0));

   TParallelCoordVar* energy = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fTotalE");
   energy->AddRange(new TParallelCoordRange(energy,1000.0,6000.0));

   para->AddSelection("bigcalClusters.fGoodCherenkov");
   para->GetCurrentSelection()->SetLineColor(kViolet);

}


//    TParallelCoordVar* nclust = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusterEvent.fNClusters");
//    TParallelCoordVar* cer = (TParallelCoordVar*)para->GetVarList()->FindObject("bigcalClusters.fCherenkovBestNPESum");


//    nclust->SetName("NClusters");
//    cer->SetName("CerNPE");
//    energy->SetName("Energy");
//    sigx->SetName("#sigma_x");
//    sigy->SetName("#sigma_y");
//    skewx->SetName("#gamma_x");
//    skewy->SetName("#gamma_y");