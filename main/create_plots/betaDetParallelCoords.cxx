void betaDetParallelCoords(Int_t runNumber =72994) {
   TCanvas *c1 = new TCanvas("c1");
   rman->SetRun(runNumber,1);
   TTree *T = (TTree*)rman->fCurrentFile->Get("betaDetectors1");
   T->Draw(   "betaDetectorEvent.fGasCherenkovEvent->fHits->fADC:betaDetectorEvent.fGasCherenkovEvent->fNumberOfHits","betaDetectorEvent.fGasCherenkovEvent->fHits->fLevel==0","para");

   TParallelCoord* para = (TParallelCoord*)gPad->GetListOfPrimitives()->FindObject("ParaCoord");

   TParallelCoordVar* adc = (TParallelCoordVar*)para->GetVarList()->FindObject("betaDetectorEvent.fGasCherenkovEvent->fHits->fADC");
   adc->AddRange(new TParallelCoordRange(adc,0,3000));

   TParallelCoordVar* nhit = (TParallelCoordVar*)para->GetVarList()->FindObject("betaDetectorEvent.fGasCherenkovEvent->fNumberOfHits");
   nhit->AddRange(new TParallelCoordRange(nhit,0,30));

//    para->AddSelection("bigcalClusters.fGoodCherenkov");
//    para->GetCurrentSelection()->SetLineColor(kViolet);

}
