Int_t quantity_vs_run(const char * quantity = "beam_pol") {

   TCanvas *c1 = new TCanvas("run_plots","Run Plots",200,10,700,500);
  // c1->SetFillColor(42);
   c1->SetGrid();

   std::vector<double> xvals;
   std::vector<double> yvals;

/*   aman->QueueUpParallel();*/
   aman->QueueUpTestRuns();

   for(int i = 0;i<aman->fRunQueue->size();i++) {
      rman->SetRun(aman->fRunQueue->at(i));
      xvals.push_back(aman->fRunQueue->at(i));
      yvals.push_back(atof(rman->GetRunReport()->GetValueOf(quantity)));
   }

   Int_t N = xvals.size();

   TVectorD x;
   x.Use(N, &xvals[0]);

   TVectorD y;
   y.Use(N, &yvals[0]);

   TGraph * grA;
   grA = new TGraph(x,y);
   grA->SetLineColor(2);
   grA->SetLineWidth(4);
   grA->SetMarkerColor(4);
   grA->SetMarkerSize(1.5);
   grA->SetMarkerStyle(21);
   grA->SetTitle(Form("%s vs Run number",quantity));
   grA->GetXaxis()->SetTitle("Run Number");
   grA->GetYaxis()->SetTitle(quantity);
   grA->Draw("ap");
}