/*! Creates the plots vs Run Number

  Asymmetry vs run number 
  Adds to two TMultiGraph objects 
 */
Int_t create_date_plots(){

   gStyle->SetOptStat(0);
   gROOT->SetStyle("Plain");
   /// Sets up a canvas
   TCanvas *c1 = new TCanvas("asymCanvas","Raw Asymmetries",1200,400);
   // c1->SetFillColor(42);
   c1->SetGrid();
//    c1->Divide(1,2);

   /// The vectors which will be used to hold the values pulled from each run
   std::vector<double> xvals;
   std::vector<double> Avals;
   std::vector<double> PTvals;
   std::vector<double> PBvals;
   std::vector<double> PBTvals;
   std::vector<double> APhysvals;
   std::vector<double> NEventsvals;
   std::vector<double> AErrors;
   std::vector<double> xErrors;
//    aman->QueueUpPerpendicular();
// 
//    aman->QueueUpPerpendicularNH3Runs();
/*   aman->QueueUpParallel();*/
//    aman->QueueUpTestRuns();
   aman->BuildQueue("replaying_list.txt");
//    aman->BuildQueue("run_list.txt",2);

   Double_t N1,N2;
   Double_t Pb,Pt;

  for(int i = 0;i<aman->fRunQueue->size();i++) {
    rman->SetRun(aman->fRunQueue->at(i));

    Pt = rman->fCurrentRun->fTargetOfflinePolarization/100.0;
    Pb = rman->fCurrentRun->fBeamPolarization/100.0;

    if(rman->fCurrentRun->IsValid() && rman->fCurrentRun->fAnalysisPass >= 1 /*&& Pt>0.00001 && Pb > 0.000001*/) {
    xErrors.push_back(0.0);
    xvals.push_back(aman->fRunQueue->at(i));
    Avals.push_back(rman->fCurrentRun->fTotalAsymmetry);

    N1 = (double) rman->GetCurrentRun()->fTotalNPlus ;
    N2 = (double) rman->GetCurrentRun()->fTotalNMinus ;

    PTvals.push_back(rman->fCurrentRun->fStartingPolarization/1000.0);
    PBvals.push_back(rman->fCurrentRun->fBeamPolarization/1000.0);


// see notebook on errors assuming 5% error on beam and target.... for now 
    AErrors.push_back(
    TMath::Sqrt(((0.0025000000000000005*TMath::Power(N1,3) - 
         0.0025000000000000005*TMath::Power(N1,2)*N2 - 
         0.0025000000000000005*N1*TMath::Power(N2,2) + 
         0.0025000000000000005*TMath::Power(N2,3))*TMath::Power(Pb,2) + 
      (0.0025000000000000005*TMath::Power(N1,3) - 
         0.0025000000000000005*TMath::Power(N1,2)*N2 + 
         0.0025000000000000005*TMath::Power(N2,3) + 
         N1*N2*(-0.0025000000000000005*N2 + 4.*TMath::Power(Pb,2)))*
       TMath::Power(Pt,2))/(TMath::Power(N1 + N2,3)*TMath::Power(Pb,4)*TMath::Power(Pt,4))) );

    PBTvals.push_back(Pb*Pt/10.0);
    NEventsvals.push_back(rman->fCurrentRun->fNBeta2Events);
    APhysvals.push_back(( double)rman->fCurrentRun->fTotalAsymmetry /(Pb*Pt));
    if( rman->fCurrentRun->fStartingPolarization == 0.0 ) std::cout << "yousuck\n";
    }
  }
//    double * xv;
//    double * xy;
// memcpy( xv, &xv[0], sizeof( int ) * myVector.size() );


///////////
   std::vector<double> configChangeX;
   std::vector<double> configBeam;
      TString query = " SELECT Start_run,gpbeam FROM exp_configuration; ";
      TSQLResult * res = SANEDatabaseManager::GetManager()->dbServer->Query(query.Data());
      TSQLRow * row;
      for(int i = 0 ;i< res->GetRowCount();i++ ){
         row = res->Next();
         std::cout << row->GetField(0) << "    " << row->GetField(1) << "\n";
         configChangeX.push_back(atoi(row->GetField(0)) );
         configBeam.push_back(atof(row->GetField(1))/100.0 );
      }
   TVectorD configx;
   configx.Use(configChangeX.size(), &configChangeX[0]);

   TVectorD beamEnergy;
   beamEnergy.Use(configBeam.size(), &configBeam[0]);
///////////


///////////
   std::vector<double> detChangeX;
   std::vector<double> detChangeY;

      TString query = " SELECT first_run FROM detector_changes; ";
      TSQLResult * res = SANEDatabaseManager::GetManager()->dbServer->Query(query.Data());
      TSQLRow * row;
      for(int i = 0 ;i< res->GetRowCount();i++ ){
         row = res->Next();
         std::cout << row->GetField(0) <<  "\n";
         detChangeX.push_back(atoi(row->GetField(0)) );
         detChangeY.push_back(0.1);
      }
   TVectorD detX;
   detX.Use(detChangeX.size(), &detChangeX[0]);

   TVectorD detY;
   detY.Use(detChangeY.size(), &detChangeY[0]);
///////////



   Int_t N = xvals.size();

   TVectorD x;
   x.Use(N, &xvals[0]);

   TVectorD configx;
   configx.Use(configChangeX.size(), &configChangeX[0]);

   TVectorD beamEnergy;
   beamEnergy.Use(configBeam.size(), &configBeam[0]);

   TVectorD xerr;
   xerr.Use(N, &xErrors[0]);

   TVectorD a;
   a.Use(N, &Avals[0]);

   TVectorD aerr;
   aerr.Use(N, &AErrors[0]);

   TVectorD aphys;
   aphys.Use(N, &APhysvals[0]);

   TVectorD pt;
   pt.Use(N, &PTvals[0]);

   TVectorD pb;
   pb.Use(N, &PBvals[0]);

   TVectorD pbt;
   pbt.Use(N, &PBTvals[0]);
   
   TVectorD nevents;
   nevents.Use(N, &NEventsvals[0]);
 
 
   TMultiGraph *mg = new TMultiGraph();

   TGraphErrors * grA = new TGraphErrors(x,aphys,xerr,aerr);
   grA->SetLineColor(2);
   grA->SetLineWidth(2);
   grA->SetMarkerColor(4);
   grA->SetMarkerSize(1.1);
   grA->SetMarkerStyle(21);
   grA->SetTitle("NEvents vs Run number");
   grA->GetXaxis()->SetTitle("Run Number");
   grA->GetYaxis()->SetTitle("events");
//grA->Draw("AP");

   grBeam = new TGraph(configx,beamEnergy);
   grBeam->SetMarkerColor(3);
   grBeam->SetLineColor(2);
   grBeam->SetLineWidth(3);
   grBeam->SetMarkerSize(1.6);
   grBeam->SetMarkerStyle(29);

   grDet= new TGraph(detX,detY);
   grDet->SetMarkerColor(6);
   grDet->SetLineColor(2);
   grDet->SetLineWidth(3);
   grDet->SetMarkerSize(1.3);
   grDet->SetMarkerStyle(34);

   grPT = new TGraph(x,pt);
   grPT->SetMarkerColor(1);
   grPT->SetLineColor(2);
   grPT->SetLineWidth(3);
   grPT->SetMarkerSize(1.5);
   grPT->SetMarkerStyle(23);

   grPB = new TGraph(x,pb);
   grPB->SetMarkerColor(3);
   grPB->SetLineColor(6);
   grPB->SetLineWidth(1);
   grPB->SetMarkerSize(1.5);
   grPB->SetMarkerStyle(24);

   grPBT = new TGraph(x,pbt);
   grPBT->SetMarkerColor(1);
   grPBT->SetLineColor(2);
   grPBT->SetLineWidth(3);
   grPBT->SetMarkerSize(1.3);
   grPBT->SetMarkerStyle(23);


   grNEvents = new TGraph(x,nevents);
   grNEvents->SetMarkerColor(1);
   grNEvents->SetLineColor(2);
   grNEvents->SetLineWidth(3);
   grNEvents->SetMarkerSize(1.5);
   grNEvents->SetMarkerStyle(23);

   mg->Add(grA,"P");
   mg->Add(grPBT,"P");
   mg->Add(grBeam,"P");
   mg->Add(grDet,"P");
//    mg->Add(grPT,"P");

   TF1 * zero = new TF1("f1","0",72000,73500);
   zero->SetLineColor(1);
   zero->SetLineWidth(1);

//   c1->cd(1);
   mg->SetMaximum(0.2);
   mg->SetMinimum(-0.1);
   mg->Draw("a");
   mg->SetMaximum(0.2);
   mg->SetMinimum(-0.1);
   zero->Draw("same");
/*
   c1->cd(2);
   grNEvents->Draw("ap");*/


  TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("Asymmetry vs Run");
   leg->AddEntry(grA,"Asymmetries with errors","lep");
/*   leg->AddEntry("","Function abs(#frac{sin(x)}{x})","l");*/
   leg->AddEntry(grPBT,"Pb*Pt/10","p");
   leg->AddEntry(grDet,"Detector Changes","p");
   leg->AddEntry(grBeam,"Config Changes","p");
   leg->Draw();


//    c1->SaveAs("results/asym_vs_run.ps");
   c1->SaveAs("results/asym_vs_run.png");

}
