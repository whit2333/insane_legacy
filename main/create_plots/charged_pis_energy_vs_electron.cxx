Int_t charged_pis_energy_vs_electron(
   Int_t ePlusRunNumber = 981,
   Int_t eMinusRunNumber = 980,
   Int_t piPlusRunNumber = 983,
   Int_t piMinusRunNumber = 982      ) {

  const char * treename = "betaDetectors";

  rman->SetRun(piPlusRunNumber);

  TTree* t = (TTree*) gROOT->FindObject(treename);

gROOT->cd();


TCanvas * c = new TCanvas("piPlus","pi plus canvas");
c->Divide(2,2);
c->cd(1);
  t->Draw("Sum$(fBigcalEvent.fHits[].fEnergy):monteCarloEvent.fThrownParticles[0].fEnergy>>bcEdepVSEthrown(100,0,7000,100,0,7000)",
   "monteCarloEvent.fThrownParticles[0].fPID==211&&fBigcalEvent.fHits[Iteration$].fEnergy>0&&fBigcalEvent.fHits[Iteration$].fLevel==0","colz",100000,1001);
c->cd(3);

  TProfile * p = (TProfile*) (((TH2F*)gROOT->FindObject("bcEdepVSEthrown"))->ProfileX("bcEdepVSEthrown_pfx"));
  p->SetTitle("Sum of hit energys Vs BC Plane Energy");
  p->DrawClone();
// TProfile * p = gROOT->FindObject("bcEdepVSEbcplane_pfx"); 
c->cd(2);
  t->Draw("fBigcalEvent.fTotalEnergyDeposited:monteCarloEvent.fBigcalPlaneHits[].fEnergy>>bcEdepVSEbcplane(100,0,7000,100,0,7000)",
          "monteCarloEvent.fBigcalPlaneHits[Iteration$].fPID==211","colz",100000,1001);
c->cd(4);

  TProfile * p2 = (TProfile*)(((TH2F*)gROOT->FindObject("bcEdepVSEbcplane"))->ProfileX("bcEdepVSEbcplane_pfx"));
p2->SetTitle("BCEvent Total Deposited Vs BC Plane Energy");
  p2->DrawClone();

   c->SaveAs("results/montecarlo/piPlusEdeposited.png");
   c->SaveAs("results/montecarlo/piPlusEdeposited.eps");

/*  rman->SetRun(967);*/
  rman->SetRun(ePlusRunNumber);
  TTree* t2 = (TTree*) gROOT->FindObject(treename);
gROOT->cd();

/*TCanvas * c2=c;*/
TCanvas * c2 = new TCanvas("eMinus","electron canvas");
c2->Divide(2,2);
c2->cd(1);
  t2->Draw("Sum$(fBigcalEvent.fHits.fADC):monteCarloEvent.fThrownParticles[0].fEnergy>>h1(100,0,7000,100,0,7000)",
          "monteCarloEvent.fThrownParticles[0].fPID==-11&&fBigcalEvent.fHits[Iteration$].fEnergy>0&&fBigcalEvent.fHits[Iteration$].fLevel==0&&fBigcalPlaneHits.fEnergy-fThrownParticles.fEnergy>-100","colz",100000,1001);
c2->cd(3);

TProfile * p3 = (TProfile*) (((TH2F*)gROOT->FindObject("h1"))->ProfileX("h1_pfx"));
   p3->SetMarkerStyle(20);
   p3->SetFillColor(2);
   p3->SetLineColor(2);
   p3->SetTitle("Sum of hit energys Vs BC Plane Energy");
   p3->SetMarkerColor(2);
  p3->DrawClone("");
// TProfile * p = gROOT->FindObject("bcEdepVSEbcplane_pfx"); 
c2->cd(2);
  t2->Draw("Sum$(fBigcalEvent.fHits.fADC):monteCarloEvent.fThrownParticles[0].fEnergy>>h2(100,0,7000,100,0,7000)",
          "monteCarloEvent.fBigcalPlaneHits[Iteration$].fPID==11&&fBigcalEvent.fTotalEnergyDeposited>0&&fBigcalPlaneHits.fEnergy-fThrownParticles.fEnergy>-100","colz",100000,1001);
c2->cd(4);

TProfile * p4 = (TProfile*)(((TH2F*)gROOT->FindObject("h2"))->ProfileX("h2_pfx"));
   p4->SetMarkerStyle(20);
   p4->SetLineColor(2);
   p4->SetFillColor(2);
   p4->SetMarkerColor(2);
   p4->SetTitle("BCEvent Total Deposited Vs BC Plane Energy");
  p4->DrawClone("");
   c2->SaveAs("results/montecarlo/ePlusEdeposited.png");
   c2->SaveAs("results/montecarlo/ePlusEdeposited.eps");

   TCanvas * c3 = new TCanvas("pionelectron","piPlus Vx ePlus ");
//   c3->Divide(2,1);
//   c3->cd(1);
   p3->Draw("");
//   c3->cd(1);
   p->Draw("same");
   c3->SaveAs("results/montecarlo/piPlusVSePlus2.png");
   c3->SaveAs("results/montecarlo/piPlusVSePlus2.eps");
   
   TCanvas * c4 = new TCanvas("pionelectron2","piPlus Vx ePlus 2 ",900,800);
   p4->Draw("");
   p2->Draw("same");
   c4->SaveAs("results/montecarlo/piPlusVSePlus.png");
   c4->SaveAs("results/montecarlo/piPlusVSePlus.eps");
  t2->StartViewer();

}