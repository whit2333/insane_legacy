Int_t energy_comparisons( Int_t runNumber = 1004, Int_t aPID = -211 ) {

  // e is 11
  // pi is 211
  // mu is 13
  // gamma is 22
  // kaon is 321
  // pi0 is 111
   const char * treename = "betaDetectors1";

   rman->SetRun(runNumber);
   TTree* t = (TTree*) gROOT->FindObject(treename);
   gROOT->cd();

   TCanvas * c = new TCanvas("piPlus","pi plus canvas");
   c->Divide(2,3);

   c->cd(1);
   t->Draw(
   "fBigcalEvent.fHits[].fADC:monteCarloEvent.fThrownParticles[0].fEnergy>>bcEdepVSEthrown(100,0,7000,100,0,7000)",
   Form("monteCarloEvent.fThrownParticles[0].fPID==%d&&fBigcalEvent.fHits[Iteration$].fEnergy>5&&fBigcalEvent.fHits[Iteration$].fLevel==0",aPID),
   "colz",100000,1001);

   c->cd(3);
   TProfile * p = (TProfile*) (((TH2F*)gROOT->FindObject("bcEdepVSEthrown"))->ProfileX("bcEdepVSEthrown_pfx"));
   p->SetTitle("Sum of hit energys Vs BC Plane Energy");
   p->DrawClone();
// TProfile * p = gROOT->FindObject("bcEdepVSEbcplane_pfx"); 

   c->cd(2);
   t->Draw(
   "fBigcalEvent.fTotalEnergyDeposited:monteCarloEvent.fBigcalPlaneHits[].fEnergy>>bcEdepVSEbcplane(100,0,7000,100,0,7000)",
   Form("monteCarloEvent.fBigcalPlaneHits[Iteration$].fPID==%d",aPID),
   "colz",100000,1001);

   c->cd(4);
   TProfile * p2 =
      (TProfile*)(((TH2F*)gROOT->FindObject("bcEdepVSEbcplane"))->ProfileX("bcEdepVSEbcplane_pfx"));
   p2->SetTitle("BCEvent Total Deposited Vs BC Plane Energy");
   p2->DrawClone();


   c->cd(5);
   t->Draw(
   "fBigcalEvent.fTotalEnergyDeposited:bigcalClusters.fTotalE>>bcclusterVs(100,0,7000,100,0,7000)",
   Form("monteCarloEvent.fBigcalPlaneHits[Iteration$].fPID==%d",aPID),
   "colz",100000,1001);

   c->cd(6);
   t->Draw(
   "monteCarloEvent.fThrownParticles[0].fEnergy:bigcalClusters.fTotalE>>bcclusterVs2(100,0,7000,100,0,7000)",
   Form("monteCarloEvent.fBigcalPlaneHits[Iteration$].fPID==%d",aPID),
   "colz",100000,1001);


   c->SaveAs("results/montecarlo/energy_comp.png");
   c->SaveAs("results/montecarlo/energy_comp.eps");


   t->StartViewer();
}
