Int_t track_efficiencies4(Int_t runGroup = 1, bool newmerge = true) {

   if (gROOT->LoadMacro("util/merge_run_list.cxx") != 0) {
      Error(weh, "Failed loading util/merge_run_list.cxx in compiled mode.");
      return -1;
   }

   const char * runlist = "lists/para/test_para.txt";
   const char * outfile = Form("data/Tracks/ParaTracks%d.root",runGroup);;

   if( newmerge )
      if( merge_run_list(runlist,outfile,"Tracks") ) { return(-1);}

   TFile * f = TFile::Open(outfile,"UPDATE");
   f->cd();
   TTree * t = (TTree*)gROOT->FindObject("Tracks");
   if(!t) return(-2);

   TH1F * h1 = 0;
   TH2F * h2 = 0;

   Double_t theta_range[2] = {0.4*180.0/TMath::Pi(),1.0*180.0/TMath::Pi()};
   Double_t phi_range[2] = {-0.6*180.0/TMath::Pi(),1.0*180.0/TMath::Pi()};
   Double_t energy_range[2] = {500,4500};

   TH2F * hPhiVsLucMiss = new TH2F("hPhiVsLucMiss","Phi vs LucMiss;#delta Lucite;#phi",
                                  100,0,50,100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTrackerMiss = new TH2F("hPhiVsTrackerMiss","Phi vs LucMiss;#delta Lucite;#phi",
                                  100,-15,15,100,phi_range[0],phi_range[1]);
   TH2F * hTrackerMissVsPhi = new TH2F("hTrackerMissVsPhi","Tracker miss vs #phi;#delta tracker;#phi",
                                  100,phi_range[0],phi_range[1],100,-15,15);
   TH2F * hTrackerMissVsEnergy = new TH2F("hTrackerMissVsEnergy","Tracker miss vs #phi;#delta tracker;#phi",
                                  20,energy_range[0],energy_range[1],100,-5,5);
   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta3 = new TH2F("hPhiVsTheta3","Phi vs Theta with tracker cuts;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   TEfficiency* effPhi3   = new TEfficiency("effPhi3","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta3 = new TEfficiency("effTheta3","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy3 = new TEfficiency("effEnergy3","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);


   TEfficiency* effPhi4   = new TEfficiency("effPhi4","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta4 = new TEfficiency("effTheta4","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy4 = new TEfficiency("effEnergy4","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi5   = new TEfficiency("effPhi5","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta5 = new TEfficiency("effTheta5","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy5 = new TEfficiency("effEnergy5","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi6   = new TEfficiency("effPhi6","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta6 = new TEfficiency("effTheta6","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy6 = new TEfficiency("effEnergy6","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi2   = new TEfficiency("effPhi2","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta2 = new TEfficiency("effTheta2","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy2 = new TEfficiency("effEnergy2","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy = new TEfficiency("effEnergy","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi8   = new TEfficiency("effPhi8","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta8 = new TEfficiency("effTheta8","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy8 = new TEfficiency("effEnergy8","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi9   = new TEfficiency("effPhi9","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta9 = new TEfficiency("effTheta9","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy9 = new TEfficiency("effEnergy9","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy9 = new TEfficiency("effBCy9","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx9 = new TEfficiency("effBCx9","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effPhi42   = new TEfficiency("effPhi42","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta42 = new TEfficiency("effTheta42","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy42 = new TEfficiency("effEnergy42","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy42 = new TEfficiency("effBCy42","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx42 = new TEfficiency("effBCx42","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effPhi38   = new TEfficiency("effPhi38","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta38 = new TEfficiency("effTheta38","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy38 = new TEfficiency("effEnergy38","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy38 = new TEfficiency("effBCy38","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx38 = new TEfficiency("effBCx38","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effPhi30   = new TEfficiency("effPhi30","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta30 = new TEfficiency("effTheta30","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy30 = new TEfficiency("effEnergy30","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy30 = new TEfficiency("effBCy30","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx30 = new TEfficiency("effBCx30","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effBCyVsEnergy = new TEfficiency("effBCyVsEnergy","Cut Efficiency, Energy Vs Y_{bigcal} ;Y_{bigcal};E",
                                           120,-120,120, 20,500,2500);
   TEfficiency* effBCy7 = new TEfficiency("effBCy","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx7 = new TEfficiency("effBCx","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);
   TEfficiency* effPhi7   = new TEfficiency("effPhi7","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1],20,energy_range[0],energy_range[1]);
   TEfficiency* effTheta7 = new TEfficiency("effTheta7","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1],20,energy_range[0],energy_range[1]);
   TEfficiency* effEnergy7 = new TEfficiency("effEnergy7","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effBCyVsBCx = new TEfficiency("effBCyVsBCx","Cut Efficiency, Y_{bigcal} Vs X_{bigcal} ;X_{bigcal};Y_{bigcal}",
                                           60,160,260, 60,-120,120);
   TEfficiency* effTrackerYVsTrackerX = new TEfficiency("effTrackerYVsTrackerX","Cut Efficiency, Y_{tracker} Vs X_{tracker} ;X_{tracker};Y_{tracker}",
                                           40,24,42,50,-20,20);
   TEfficiency* effPhiVsTheta = new TEfficiency("effPhiVsTheta","Cut Efficiency, #phi Vs #theta ;#theta;#phi",
                                           50,theta_range[0],theta_range[1],50,phi_range[0],phi_range[1]);

   TEfficiency* effBCyVsEnergy2 = new TEfficiency("effBCyVsEnergy2","Cut Efficiency, Energy Vs Y_{bigcal} ;Y_{bigcal};E",
                                           20,500,4000,60,-120,120);

   TEfficiency* effBCxVsEnergy2 = new TEfficiency("effBCxVsEnergy2","Cut Efficiency, Energy Vs X_{bigcal} ;X_{bigcal};E",
                                           20,500,4000,60,160,260);


   InSANEDISTrajectory * fTrajectory = new InSANEDISTrajectory();
   t->SetBranchAddress("trajectory",&fTrajectory);
   InSANETriggerEvent  * fTriggerEvent = new InSANETriggerEvent();
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   /// Create the Asymmetries for each Q^2 bin.
   Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t CerADCCut[3] = {0.5,1.5,2.5};

   Int_t fEntries =  t->GetEntries();
   /// Event Loop
   for(Int_t ievent = 0; ievent < fEntries; ievent++) {

      if(ievent%10000 == 0) std::cout << ievent << "/" << fEntries << "\n";
      t->GetEntry(ievent);
      bool passed = false;
      bool passed1 = false;
      bool passed2 = false;
      bool passed3 = false;
      bool passed4 = false;
      bool passed5 = false;
      bool passed7 = false;
      bool passed8 = false;
      bool passed9 = false;
      bool passed42 = false;
      bool passed30 = false;

      hPhiVsTheta1->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());
      //hPhiVsLucMiss->Fill(fTrajectory->fLuciteMissDistance,fTrajectory->fPhi*180.0/TMath::Pi());

      /// Standard Cuts.
      if(!fTrajectory->fIsNoisyChannel)
      if(fTrajectory->fIsGood) 
      if(fTriggerEvent->IsBETA2Event() ){

      hPhiVsLucMiss->Fill(fTrajectory->fLuciteMissDistance,fTrajectory->fPhi*180.0/TMath::Pi());
      hPhiVsTrackerMiss->Fill(fTrajectory->fTrackerMiss.Y(),fTrajectory->fPhi*180.0/TMath::Pi());
      hTrackerMissVsPhi->Fill(fTrajectory->fPhi*180.0/TMath::Pi(),fTrajectory->fTrackerMiss.Y());
      hTrackerMissVsEnergy->Fill(fTrajectory->fEnergy,fTrajectory->fTrackerMiss.Y());

      if(fTrajectory->fNCherenkovElectrons > 0.5 && fTrajectory->fNCherenkovElectrons < 1.5){
         passed42 = true;
      }

      if( fTrajectory->fEnergy > 600.0 ) {
         passed1 = true;
      }

      if(TMath::Abs(fTrajectory->fCherenkovTDC-347) < 20) {
         passed30 = true;
      }

      if(TMath::Abs(fTrajectory->fCherenkovTDC) < 10) {
         passed9 = true;

      if(fTrajectory->fNCherenkovElectrons > 0.5 && fTrajectory->fNCherenkovElectrons < 1.5) {

         passed5 = true;
         
         if( TMath::Abs(fTrajectory->fTrackerMiss.Y()) < 1.5 ) {
            passed = true;
            passed2 = true;
            hPhiVsTheta3->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());
         } // tracker cut

         if( TMath::Abs(fTrajectory->fLuciteMissDistance) < 25.0 ) { 
            passed3 = true;
            passed4 = true;
            hPhiVsTheta3->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());
         } // tracker cut

         hPhiVsTheta2->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());

         if( fTrajectory->fEnergy > 800.0 ) {
            passed7 = true;
         }
         effPhi7->Fill(passed,fTrajectory->fPhi*180.0/TMath::Pi(),fTrajectory->fEnergy);
         effTheta7->Fill(passed,fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fEnergy);
         effEnergy7->Fill(passed,fTrajectory->fEnergy);
         effBCy7->Fill(passed,fTrajectory->fPosition0.Y());
         effBCx7->Fill(passed,fTrajectory->fPosition0.X());

         //effBCyVsEnergy->Fill(passed,fTrajectory->fPosition0.Y(),fTrajectory->fEnergy);
         effBCyVsEnergy2->Fill(passed,fTrajectory->fEnergy,fTrajectory->fPosition0.Y());
         effBCxVsEnergy2->Fill(passed,fTrajectory->fEnergy,fTrajectory->fPosition0.X());
         effBCyVsBCx->Fill(passed ,fTrajectory->fPosition0.X(),fTrajectory->fPosition0.Y());
         effPhiVsTheta->Fill(passed ,fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());
         effTrackerYVsTrackerX->Fill(passed ,fTrajectory->fTrackerPosition.X(),fTrajectory->fTrackerPosition.Y());

         effPhi->Fill(passed,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta->Fill(passed,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy->Fill(passed,fTrajectory->fEnergy);

         effPhi3->Fill(passed3,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta3->Fill(passed3,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy3->Fill(passed3,fTrajectory->fEnergy);

         effPhi6->Fill(passed3 && passed,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta6->Fill(passed3 && passed,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy6->Fill(passed3 && passed,fTrajectory->fEnergy);

      } // end cherenkov TDC cut
      } // end cherenkov ADC cut

      if( TMath::Abs(fTrajectory->fTrackerMiss.Y()) < 2.2 ) { 
         passed8 = true;
      } // tracker cut

      effPhi9->Fill(passed9,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta9->Fill(passed9,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy9->Fill(passed9,fTrajectory->fEnergy);
      effBCy9->Fill(passed9,fTrajectory->fPosition0.Y());
      effBCx9->Fill(passed9,fTrajectory->fPosition0.X());

      effPhi8->Fill(passed5,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta8->Fill(passed5,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy8->Fill(passed5,fTrajectory->fEnergy);

      effPhi5->Fill(passed5,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta5->Fill(passed5,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy5->Fill(passed5,fTrajectory->fEnergy);

      effPhi2->Fill(passed2,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta2->Fill(passed2,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy2->Fill(passed2,fTrajectory->fEnergy);

      effPhi4->Fill(passed4,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta4->Fill(passed4,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy4->Fill(passed4,fTrajectory->fEnergy);

      effPhi42->Fill(passed42,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta42->Fill(passed42,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy42->Fill(passed42,fTrajectory->fEnergy);
      effBCy42->Fill(passed42,fTrajectory->fPosition0.Y());
      effBCx42->Fill(passed42,fTrajectory->fPosition0.X());

      effPhi30->Fill(passed30,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta30->Fill(passed30,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy30->Fill(passed30,fTrajectory->fEnergy);
      effBCy30->Fill(passed30,fTrajectory->fPosition0.Y());
      effBCx30->Fill(passed30,fTrajectory->fPosition0.X());

      effPhi38->Fill(passed42 && passed9 ,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta38->Fill(passed42 && passed9,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy38->Fill(passed42 && passed9,fTrajectory->fEnergy);
      effBCy38->Fill(passed42 && passed9,fTrajectory->fPosition0.Y());
      effBCx38->Fill(passed42 && passed9,fTrajectory->fPosition0.X());

      } // end bad channel cuts



   } // end event loop



   TCanvas * c = new TCanvas("track_efficiencies4","track_efficiencies4");
   c->Divide(3,3);

   TLegend * leg = new TLegend(0.76,0.15,0.999,0.999);
   leg->SetHeader("Efficiencies");
   //leg->SetNColumns(2);

   c->cd(1);
   TMultiGraph *mg = new TMultiGraph();
   effPhi->Paint();
   if(effPhi->GetPaintedGraph() ) mg->Add(effPhi->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi->GetPaintedGraph(),"N_{#check{C}+T}/N_{#checkC}","lep");


   effPhi2->SetLineColor(2);
   effPhi2->Paint();
   if(effPhi2->GetPaintedGraph() ) mg->Add(effPhi2->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi2->GetPaintedGraph(),"N_{#check{C}+T}/N_{0}","lep");

   effPhi3->SetLineColor(3);
   effPhi3->Paint();
   if(effPhi3->GetPaintedGraph() ) mg->Add(effPhi3->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi3->GetPaintedGraph(),"N_{#check{C}+L}/N_{#check{C}}","lep");

   effPhi4->SetLineColor(4);
   effPhi4->Paint();
   if(effPhi4->GetPaintedGraph() ) mg->Add(effPhi4->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi4->GetPaintedGraph(),"N_{#check{C}+L}/N_{0}","lep");

   effPhi5->SetLineColor(6);
   effPhi5->Paint();
   if(effPhi5->GetPaintedGraph() ) mg->Add(effPhi5->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi5->GetPaintedGraph(),"N_{#check{C}+T+L}/N_{0}","lep");

   effPhi6->SetLineColor(7);
   effPhi6->Paint();
   if(effPhi6->GetPaintedGraph() ) mg->Add(effPhi6->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi6->GetPaintedGraph(),"N_{#check{C}+T+L}/N_{#check{C}}","lep");

   effPhi8->SetLineColor(8);
   effPhi8->Paint();
   if(effPhi8->GetPaintedGraph() ) mg->Add(effPhi8->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi8->GetPaintedGraph(),"N_{T}/N_{0}","lep");

   effPhi9->SetLineColor(9);
   effPhi9->Paint();
   if(effPhi9->GetPaintedGraph() ) mg->Add(effPhi9->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi9->GetPaintedGraph(),"N_{#slash{#check{C}}TDC}/N_{0}","lep");

   effPhi38->SetLineColor(38);
   effPhi38->Paint();
   if(effPhi38->GetPaintedGraph() ) mg->Add(effPhi38->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi38->GetPaintedGraph(),"N_{#slash{#check{C}}ADC}/N_{0}","lep");

   effPhi42->SetLineColor(42);
   effPhi42->Paint();
   if(effPhi42->GetPaintedGraph() ) mg->Add(effPhi42->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi42->GetPaintedGraph(),"N_{#slash{#check{C}}ADC}/N_{0}","lep");

   mg->Draw("A");
   mg->GetXaxis()->SetTitle("#phi");
   mg->GetYaxis()->SetTitle("#epsilon");
   mg->Draw("A");


   c->cd(2);
   TMultiGraph *mg2 = new TMultiGraph();

   effTheta->Paint();
   if(effTheta->GetPaintedGraph() ) mg2->Add(effTheta->GetPaintedGraph(),"P");

   effTheta2->SetLineColor(2);
   effTheta2->Paint();
   if(effTheta2->GetPaintedGraph() ) mg2->Add(effTheta2->GetPaintedGraph(),"P");

   effTheta3->SetLineColor(3);
   effTheta3->Paint();
   if(effTheta3->GetPaintedGraph() ) mg2->Add(effTheta3->GetPaintedGraph(),"P");

   effTheta4->SetLineColor(4);
   effTheta4->Paint();
   if(effTheta4->GetPaintedGraph() ) mg2->Add(effTheta4->GetPaintedGraph(),"P");

   effTheta5->SetLineColor(6);
   effTheta5->Paint();
   if(effTheta5->GetPaintedGraph() ) mg2->Add(effTheta5->GetPaintedGraph(),"P");

   effTheta6->SetLineColor(7);
   effTheta6->Paint();
   if(effTheta6->GetPaintedGraph() ) mg2->Add(effTheta6->GetPaintedGraph(),"P");

   mg2->Draw("A");
   mg2->GetXaxis()->SetTitle("#theta");
   mg2->GetYaxis()->SetTitle("#epsilon");
   mg2->Draw("A");

   c->cd(3);
   TMultiGraph *mg3 = new TMultiGraph();

   effEnergy->Paint();
   if(effEnergy->GetPaintedGraph() ) mg3->Add(effEnergy->GetPaintedGraph(),"P");

   effEnergy2->SetLineColor(2);
   effEnergy2->Paint();
   if(effEnergy2->GetPaintedGraph() ) mg3->Add(effEnergy2->GetPaintedGraph(),"P");

   effEnergy3->SetLineColor(3);
   effEnergy3->Paint();
   if(effEnergy3->GetPaintedGraph() ) mg3->Add(effEnergy3->GetPaintedGraph(),"P");

   effEnergy4->SetLineColor(4);
   effEnergy4->Paint();
   if(effEnergy4->GetPaintedGraph() ) mg3->Add(effEnergy4->GetPaintedGraph(),"P");

   effEnergy5->SetLineColor(6);
   effEnergy5->Paint();
   if(effEnergy5->GetPaintedGraph() ) mg3->Add(effEnergy5->GetPaintedGraph(),"P");

   effEnergy6->SetLineColor(7);
   effEnergy6->Paint();
   if(effEnergy6->GetPaintedGraph() ) mg3->Add(effEnergy6->GetPaintedGraph(),"P");

   mg3->Draw("A");
   mg3->GetXaxis()->SetTitle("E'");
   mg3->GetYaxis()->SetTitle("#epsilon");
   mg3->Draw("A");
   leg->Draw();

   c->cd(4);
   //hPhiVsLucMiss->Draw("colz");
   effTrackerYVsTrackerX->Draw("colz");

   c->cd(5);
   //hPhiVsTheta2->Draw("colz");
   effBCxVsEnergy2->Draw("colz");

   c->cd(6);
   //hPhiVsTheta3->Draw("colz");
   //hTrackerMissVsEnergy->Draw("colz");
   effBCyVsBCx->Draw("colz");

   TLegend * leg2 = new TLegend(0.76,0.15,0.999,0.999);
   leg2->SetHeader("");
   //leg2->SetNColumns(2);

   c->cd(7);
   TMultiGraph *mg7 = new TMultiGraph();

   effBCy7->SetLineColor(1);
   effBCy7->Paint();
   if(effBCy7->GetPaintedGraph() ) mg7->Add(effBCy7->GetPaintedGraph(),"PC");
   leg2->AddEntry(effBCy7->GetPaintedGraph(),"N_{#check{C}+T}/N_{#checkC}","lep");

   effBCy9->SetLineColor(9);
   effBCy9->Paint();
   if(effBCy9->GetPaintedGraph() ) mg7->Add(effBCy9->GetPaintedGraph(),"PC");
   leg2->AddEntry(effBCy9->GetPaintedGraph(),"N_{#check{C}TDC10}/N_{0}","lep");

   effBCy42->SetLineColor(42);
   effBCy42->Paint();
   if(effBCy42->GetPaintedGraph() ) mg7->Add(effBCy42->GetPaintedGraph(),"PC");
   leg2->AddEntry(effBCy42->GetPaintedGraph(),"N_{#check{C}ADC}/N_{0}","lep");

   effBCy38->SetLineColor(46);
   effBCy38->Paint();
   if(effBCy38->GetPaintedGraph() ) mg7->Add(effBCy38->GetPaintedGraph(),"PC");
   leg2->AddEntry(effBCy38->GetPaintedGraph(),"N_{#check{C}}/N_{0}","lep");

   effBCy30->SetLineColor(30);
   effBCy30->Paint();
   if(effBCy30->GetPaintedGraph() ) mg7->Add(effBCy30->GetPaintedGraph(),"PC");
   leg2->AddEntry(effBCy30->GetPaintedGraph(),"N_{#check{C}TDC20}/N_{0}","lep");

   mg7->Draw("A");
   mg7->GetXaxis()->SetTitle("y_{bigcal}");
   mg7->GetYaxis()->SetTitle("#epsilon");
   mg7->Draw("A");
   leg2->Draw();

   c->cd(8);
   TMultiGraph *mg8 = new TMultiGraph();

   effBCx7->SetLineColor(1);
   effBCx7->Paint();
   if(effBCx7->GetPaintedGraph() ) mg8->Add(effBCx7->GetPaintedGraph(),"PC");

   effBCx9->SetLineColor(9);
   effBCx9->Paint();
   if(effBCx9->GetPaintedGraph() ) mg8->Add(effBCx9->GetPaintedGraph(),"PC");

   effBCx42->SetLineColor(42);
   effBCx42->Paint();
   if(effBCx42->GetPaintedGraph() ) mg8->Add(effBCx42->GetPaintedGraph(),"PC");

   effBCx38->SetLineColor(46);
   effBCx38->Paint();
   if(effBCx38->GetPaintedGraph() ) mg8->Add(effBCx38->GetPaintedGraph(),"PC");

   effBCx30->SetLineColor(30);
   effBCx30->Paint();
   if(effBCx30->GetPaintedGraph() ) mg8->Add(effBCx30->GetPaintedGraph(),"PC");

   mg8->Draw("A");
   mg8->GetXaxis()->SetTitle("x_{bigcal}");
   mg8->GetYaxis()->SetTitle("#epsilon");
   mg8->Draw("A");

   c->cd(9);
   //effBCyVsEnergy->Draw("colz");
   //hTrackerMissVsPhi->Draw("colz");
   effPhiVsTheta->Draw("colz");

   c->SaveAs(Form("plots/para/track_efficiencies4-%d.png",runGroup));

   t->StartViewer();
//    new TBrowser;
   return(0);

}
