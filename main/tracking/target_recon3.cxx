/** Looking at two cluster events.
 *
 */
Int_t target_recon3(Int_t runNumber = 72938) {

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   if(!events->fTree) return(-1);
   events->fTree->SetBranchStatus("betaDetectorEvent",0);
   events->fTree->SetBranchStatus("fBigcalEvent",0);
   events->fTree->SetBranchStatus("fGasCherenkovEvent",0);
   events->fTree->SetBranchStatus("fLuciteHodoscopeEvent",0);
   events->fTree->SetBranchStatus("fForwardTrackerEvent",0);
   events->fTree->SetBranchStatus("hmsDetectorEvent",0);
   events->fTree->SetBranchStatus("monteCarloEvent",0);

   Int_t nevents = 5000000;

   InSANEReconstructedEvent * fReconEvent = new InSANEReconstructedEvent();

   const char * treeName2 = "trackingPositions";
   TTree * t2 = (TTree*)gROOT->FindObject(treeName2);
   if(!t2){ std::cout << " TREE NOT FOUND : " << treeName2 << "\n"; return(-1);}
   if( t2->BuildIndex("fRunNumber","fEventNumber") < 0 )
      std::cout << "Failed to build branch on " << treeName2 << "\n";
   t2->AddFriend(events->fTree);
   t2->SetBranchAddress("uncorrelatedPositions",&fReconEvent);

   rman->GetCurrentFile()->cd("detectors/tracker");

   Double_t phimin = -5.0;
   Double_t phimax = 5.0;

   TH2F * hbcXvsY = new TH2F("hbcXvsY","Bigcal Y VS X", 60,-60,60,120,-120,120);


   TH1F * hDeltaY      = new TH1F("targetDeltaY","#Delta Y ", 100,phimin,phimax);
   TH1F * hDeltaY_zcut = new TH1F("targetDeltaY_zcut","#Delta Y with z cut", 100,phimin,phimax);
   TH1F * hReconZ      = new TH1F("ReconZ","Reconstructed Z in y-z plane", 100,phimin,phimax);
   TH1F * hDeltaX      = new TH1F("targetDeltaX","#Delta X target", 100,phimin,phimax);
   TH1F * hDeltaX_zcut = new TH1F("targetDeltaX_zcut","#Delta X target with z cut", 100,phimin,phimax);
   TH2F * hYbcVsDeltaX_zcut = new TH2F("YbcVstargetDeltaX_zcut","Bigcal Y VS #Delta X target with z cut", 100,phimin,phimax,120,-120,120);
   TH2F * hYbcVsDeltaY_zcut = new TH2F("YbcVstargetDeltaY_zcut","Bigcal Y VS #Delta Y target with z cut", 100,phimin,phimax,120,-120,120);
   TH1F * hReconZ2     = new TH1F("ReconZ2","ReconstructedZ in x-z plane", 100,phimin,phimax);
   TH1F * hReconZ3      = new TH1F("hReconZ3","Reconstructed Z in y-z plane", 100,phimin,phimax);

   TH1F * hDeltaY_Y1      = new TH1F("targetDeltaY_Y1","#Delta Y ", 100,phimin,phimax);
   TH1F * hDeltaY_Y1_zcut = new TH1F("targetDeltaY_Y1_zcut","#Delta Y target with z cut", 100,phimin,phimax);
   TH1F * hReconZ_Y1      = new TH1F("ReconZ_Y1","Reconstructed Z in y-z plane", 100,phimin,phimax);
   TH1F * hDeltaX_Y1      = new TH1F("targetDeltaX_Y1","#Delta X target", 100,phimin,phimax);
   TH1F * hDeltaX_Y1_zcut = new TH1F("targetDeltaX_Y1_zcut","#Delta X target with z cut", 100,phimin,phimax);
   TH1F * hReconZ2_Y1     = new TH1F("ReconZ2_Y1","ReconstructedZ in x-z plane", 100,phimin,phimax);
   TH2F * hYbcVsDeltaX_Y1_zcut = new TH2F("YbcVstargetDeltaX_Y1_zcut","Bigcal Y VS #Delta X_Y1 target with z cut", 100,phimin,phimax,120,-120,120);
   TH2F * hYbcVsDeltaY_Y1_zcut = new TH2F("YbcVstargetDeltaY_Y1_zcut","Bigcal Y VS #Delta Y_Y1 target with z cut", 100,phimin,phimax,120,-120,120);

   TH1F * hDeltaY_Y2      = new TH1F("targetDeltaY_Y2","#Delta Y", 100,phimin,phimax);
   TH1F * hDeltaY_Y2_zcut = new TH1F("targetDeltaY_Y2_zcut","#Delta Y target with z cut", 100,phimin,phimax);
   TH1F * hReconZ_Y2      = new TH1F("ReconZ_Y2","Reconstructed Z in y-z plane", 100,phimin,phimax);
   TH1F * hDeltaX_Y2      = new TH1F("targetDeltaX_Y2","#Delta X target", 100,phimin,phimax);
   TH1F * hDeltaX_Y2_zcut = new TH1F("targetDeltaX_Y2_zcut","#Delta X target with z cut", 100,phimin,phimax);
   TH1F * hReconZ2_Y2     = new TH1F("ReconZ2_Y2","ReconstructedZ in x-z plane", 100,phimin,phimax);
   TH2F * hYbcVsDeltaX_Y2_zcut = new TH2F("YbcVstargetDeltaX_Y2_zcut","Bigcal Y VS #Delta X_Y2 target with z cut", 100,phimin,phimax,120,-120,120);
   TH2F * hYbcVsDeltaY_Y2_zcut = new TH2F("YbcVstargetDeltaY_Y2_zcut","Bigcal Y VS #Delta Y_Y2 target with z cut", 100,phimin,phimax,120,-120,120);


   TH1F * hDeltaY_L      = new TH1F("targetDeltaY_L","#Delta Y", 100,phimin,phimax);
   TH1F * hDeltaY_L_zcut = new TH1F("targetDeltaY_L_zcut","#Delta Y target with z cut", 100,phimin,phimax);
   TH1F * hReconZ_L      = new TH1F("ReconZ_L","Reconstructed Z in y-z plane", 100,phimin,phimax);
   TH1F * hDeltaX_L      = new TH1F("targetDeltaX_L","#Delta X target", 100,phimin,phimax);
   TH1F * hDeltaX_L_zcut = new TH1F("targetDeltaX_L_zcut","#Delta X target with z cut", 100,phimin,phimax);
   TH1F * hReconZ2_L     = new TH1F("ReconZ2_L","ReconstructedZ in x-z plane", 100,phimin,phimax);

   rman->GetCurrentFile()->cd();

   Double_t RadPerDegree = 0.017453293;

   BIGCALCluster *              cluster = 0;
   ForwardTrackerPositionHit *  tkpos = 0;
   InSANEHitPosition *          hitpos = 0;
   InSANEHitPosition *          targetpos = 0;
   InSANEDISTrajectory  *       atraj = 0;

   TVector3 v1;
   TVector3 N1;
   TVector3 xzcorr;

   for(int i = 0 ; i< t2->GetEntries() && i < nevents ; i++){
      if( i%10000 == 0) std::cout << " Entry " << i << "\n";
      t2->GetEntry(i);

      events->fTree->GetEntryWithIndex(fReconEvent->fRunNumber,fReconEvent->fEventNumber);

      if( events->TRIG->IsBETAEvent() ) {
//         /// Loop over clusters....
//         for(int ic = 0; ic < events->CLUSTER->fClusters->GetEntries() ;ic++){
//            cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[ic];
         for(int j = 0; j < fReconEvent->fTrajectories->GetEntries(); j++ ) {
	    atraj = (InSANEDISTrajectory*)(*(fReconEvent->fTrajectories))[j];
 
            cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[atraj->fClusterNumber];
	     
            /// Check that the cluster channel is not a noisy one!
            if( events->CLUSTER->fNClusters == 1 )
            if( cluster->fYMoment < 2.0 ) // prot
            if( cluster->GetEnergy() < 1200.0 && cluster->GetEnergy() > 1000.0 )
            if( atraj->fLuciteMissDistance < 100.0 )
            if( !(cluster->fIsNoisyChannel) && cluster->fIsGood) 
            if( cluster->fCherenkovBestADCSum > 0.5 && cluster->fCherenkovBestADCSum < 1.3 )
            //if( cluster->fCherenkovBestADCSum > 1.8 && cluster->fCherenkovBestADCSum < 2.8 )
            //if( cluster->fCherenkovBestADCSum > 0.5 && cluster->fCherenkovBestADCSum < 2.8 )
//            if( fReconEvent->fClosestLuciteMissDistance < 50.0)
            {

    	       /// Get The target raster postion
	       targetpos = (InSANEHitPosition *)(*(fReconEvent->fTargetPositions))[0];
	       double ytarg = targetpos->fPosition.Y();
	       double xtarg = targetpos->fPosition.X();


               /// Tracker double hit positions.
               for(int it = 0; it < fReconEvent->fTrackerPositions->GetEntries(); it++) {
                  hitpos = (InSANEHitPosition *)(*(fReconEvent->fTrackerPositions))[it];

                  hbcXvsY->Fill(cluster->fXMoment,cluster->fYMoment);
		  /// Calculate the track's y position in a y-z plane located at x = x_raster 
		  v1 = (hitpos->fPosition - atraj->fPosition0);
                  N1 = v1;
		  N1.SetMag(1);
		  ///  Calculate the y position for the x-z plane
		  double yrecon = (N1.Y()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Y();
		  ///  Calculate the z position for the x-z plane
		  double zrecon = (N1.Z()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Z();

		  /// Calculate the x position for a x-z plane at the y= y_raster
		  double xrecon = (N1.X()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.X();
		  double zrecon2 = (N1.Z()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.Z();


		  hDeltaY->Fill( ytarg - yrecon );
		  if( TMath::Abs(ytarg-yrecon) < 0.75) hReconZ3->Fill(zrecon);
		  if( TMath::Abs(zrecon) < 2.0 ) hDeltaY_zcut->Fill( ytarg -yrecon );
		  hReconZ->Fill(zrecon);
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) hDeltaX->Fill( xtarg -xrecon );
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) if( TMath::Abs(zrecon) < 2.0 ) hDeltaX_zcut->Fill(xtarg -xrecon);
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) if( TMath::Abs(zrecon) < 2.0 ) hYbcVsDeltaX_zcut->Fill( xtarg - xrecon , cluster->fYMoment );
		  if( TMath::Abs(zrecon) < 2.0 ) hYbcVsDeltaY_zcut->Fill( ytarg - yrecon     , cluster->fYMoment );
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) hReconZ2->Fill(zrecon2);

               } // tracker double hit loop

               /// Tracker double hit positions.
               for(int it = 0; it < fReconEvent->fTrackerY1Positions->GetEntries(); it++) {
                  hitpos = (InSANEHitPosition *)(*(fReconEvent->fTrackerY1Positions))[it];

		  /// Calculate the track's y position in a y-z plane located at x = x_raster 
		  v1 = (hitpos->fPosition - atraj->fPosition0);
                  N1 = v1;
		  N1.SetMag(1);
		  double yrecon = (N1.Y()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Y();
		  ///  Calculate the z position for the x-z plane
		  double zrecon = (N1.Z()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Z();

		  /// Calculate the x position for a x-z plane at the y= y_raster
		  double xrecon = (N1.X()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.X();
		  double zrecon2 = (N1.Z()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.Z();
 
		  hDeltaY_Y1->Fill(ytarg -yrecon);
		  if( TMath::Abs(zrecon) < 2.0 ) hDeltaY_Y1_zcut->Fill(ytarg -yrecon);
		  hReconZ_Y1->Fill(zrecon);
		  if( TMath::Abs(cluster->fYMoment) > 50.0 )  hDeltaX_Y1->Fill(xtarg -xrecon);
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) if( TMath::Abs(zrecon) < 2.0 ) hDeltaX_Y1_zcut->Fill(xtarg -xrecon);
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) hReconZ2_Y1->Fill(zrecon2);
		  if( TMath::Abs(zrecon) < 2.0 ) hYbcVsDeltaX_Y1_zcut->Fill( xtarg - xrecon , cluster->fYMoment );
		  if( TMath::Abs(zrecon) < 2.0 ) hYbcVsDeltaY_Y1_zcut->Fill( ytarg - yrecon     , cluster->fYMoment );
               } // tracker Y1 hit loop

               /// Tracker Y2 hit positions.
               for(int it = 0; it < fReconEvent->fTrackerY2Positions->GetEntries(); it++) {
                  hitpos = (InSANEHitPosition *)(*(fReconEvent->fTrackerY2Positions))[it];

		  /// Calculate the track's y position in a y-z plane located at x = x_raster 
		  v1 = (hitpos->fPosition - atraj->fPosition0);
                  N1 = v1;
		  N1.SetMag(1);
		  double yrecon = (N1.Y()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Y();
		  ///  Calculate the z position for the x-z plane
		  double zrecon = (N1.Z()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Z();

		  /// Calculate the x position for a x-z plane at the y= y_raster
		  double xrecon = (N1.X()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.X();
		  double zrecon2 = (N1.Z()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.Z();

		  hDeltaY_Y2->Fill(ytarg -yrecon);
		  if( TMath::Abs(zrecon) < 2.0 ) hDeltaY_Y2_zcut->Fill(ytarg -yrecon);
		  hReconZ_Y2->Fill(zrecon);
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) hDeltaX_Y2->Fill(xtarg -xrecon);
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) if( TMath::Abs(zrecon) < 2.0 ) hDeltaX_Y2_zcut->Fill(xtarg -xrecon);
		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) hReconZ2_Y2->Fill(zrecon2);
		  if( TMath::Abs(zrecon) < 2.0 ) hYbcVsDeltaX_Y2_zcut->Fill( xtarg - xrecon , cluster->fYMoment );
		  if( TMath::Abs(zrecon) < 2.0 ) hYbcVsDeltaY_Y2_zcut->Fill( ytarg - yrecon     , cluster->fYMoment );

               } // tracker Y2 hit loop

	       /// Lucite hit positions.
//                for(int it = 0; it < fReconEvent->fLucitePositions->GetEntries(); it++) {
//                   hitpos = (InSANEHitPosition *)(*(fReconEvent->fLucitePositions))[it];
// 
//                   xzcorr = hitpos->fPositionUncertainty;
// 		  xzcorr.SetY(0);
// 		  hitpos->fPosition = hitpos->fPosition +xzcorr;
// 
// 		  /// Calculate the track's y position in a y-z plane located at x = x_raster 
// 		  v1 = (hitpos->fPosition - atraj->fPosition0);
//                   N1 = v1;
// 		  N1.SetMag(1);
// 		  ///  Calculate the y position for the x-z plane
// 		  double yrecon = (N1.Y()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Y();
// 		  ///  Calculate the z position for the x-z plane
// 		  double zrecon = (N1.Z()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Z();
// 
// 		  /// Calculate the x position for a x-z plane at the y= y_raster
// 		  double xrecon = (N1.X()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.X();
// 		  double zrecon2 = (N1.Z()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.Z();
// 
// 		  hDeltaY_L->Fill(ytarg -yrecon);
// 		  if( TMath::Abs(zrecon) < 2.0 ) hDeltaY_L_zcut->Fill(ytarg -yrecon);
// 		  hReconZ_L->Fill(zrecon);
// 		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) hDeltaX_L->Fill(xtarg -xrecon);
// 		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) if( TMath::Abs(zrecon) < 2.0 ) hDeltaX_L_zcut->Fill(xtarg -xrecon);
// 		  if( TMath::Abs(cluster->fYMoment) > 50.0 ) hReconZ2_L->Fill(zrecon2);
// 
//                } // lucite hit loop


            } // not noisy cluster
         } // trajectory loop
      } // is BETA2 event
   } // event loop


   TCanvas * c = new TCanvas("target_z_recon6","target_z_recon6");
   c->Divide(2,2);

   c->cd(1);

   hDeltaY->Draw();
   hDeltaY_zcut->SetLineColor(kGray);
/*   hDeltaY_zcut->SetFillColor(kBlack+1);*/
/*   hDeltaY_zcut->SetFillStyle(3005);*/
   hDeltaY_zcut->Draw("same");

   hDeltaY_Y1->SetLineColor(2);
   hDeltaY_Y1->Draw("same");
   hDeltaY_Y1_zcut->SetLineColor(kRed-9);
/*   hDeltaY_Y1_zcut->SetFillColor(kRed-9);*/
/*   hDeltaY_Y1_zcut->SetFillStyle(3004);*/
   hDeltaY_Y1_zcut->Draw("same");

   hDeltaY_Y2->SetLineColor(4);
   hDeltaY_Y2->Draw("same");
   hDeltaY_Y2_zcut->SetLineColor(kBlue-9);
/*   hDeltaY_Y2_zcut->SetFillColor(kBlue-9);*/
//    hDeltaY_Y2_zcut->SetFillStyle(3002);
   hDeltaY_Y2_zcut->Draw("same");

//    hDeltaY_L->SetLineColor(3);
//    hDeltaY_L->Draw("same");
//    hDeltaY_L_zcut->SetLineColor(3);
// //    hDeltaY_L_zcut->SetFillColor(3);
// //    hDeltaY_L_zcut->SetFillStyle(3003);
//    hDeltaY_L_zcut->Draw("same");


   c->cd(2);

   hReconZ->Draw();
   hReconZ_Y1->SetLineColor(2);
   hReconZ_Y1->Draw("same");
   hReconZ_Y2->SetLineColor(4);
   hReconZ_Y2->Draw("same");
//    hReconZ_L->SetLineColor(3);
//    hReconZ_L->Draw("same");
/*   hReconZ3->SetLineColor(kRed-9);
   hReconZ3->Draw("same");  */ 

   c->cd(3);

   hDeltaX->Draw();
   hDeltaX_zcut->SetLineColor(kGray);
/*   hDeltaX_zcut->SetFillColor(kBlack+1);*/
/*   hDeltaX_zcut->SetFillStyle(3005);*/
   hDeltaX_zcut->Draw("same");
   
   hDeltaX_Y1->SetLineColor(2);
   hDeltaX_Y1->Draw("same");
   hDeltaX_Y1_zcut->SetLineColor(kRed-9);
/*   hDeltaX_Y1_zcut->SetFillColor(kRed-9);*/
//    hDeltaX_Y1_zcut->SetFillStyle(3004);
   hDeltaX_Y1_zcut->Draw("same");
   
   hDeltaX_Y2->SetLineColor(4);
   hDeltaX_Y2->Draw("same");
   hDeltaX_Y2_zcut->SetLineColor(kBlue-9);
/*   hDeltaX_Y2_zcut->SetFillColor(kBlue-9);*/
//    hDeltaX_Y2_zcut->SetFillStyle(3002);
   hDeltaX_Y2_zcut->Draw("same");

//    hDeltaX_L->SetLineColor(3);
//    hDeltaX_L->Draw("same");
//    hDeltaX_L_zcut->SetLineColor(3);
//    hDeltaX_L_zcut->SetFillColor(3);
//    hDeltaX_L_zcut->SetFillStyle(3003);
//    hDeltaX_L_zcut->Draw("same");

   c->cd(4);

   hReconZ2->Draw();
   hReconZ2_Y1->SetLineColor(2);
   hReconZ2_Y1->Draw("same");
   hReconZ2_Y2->SetLineColor(4);
   hReconZ2_Y2->Draw("same");
//    hReconZ2_L->SetLineColor(3);
//    hReconZ2_L->Draw("same");
/*
   c->cd(3);
   hYbcVsDeltaY_Y1_zcut->Draw("colz");
//    hYbcVsDeltaY_zcut->Draw("colz");

   c->cd(6);
//    hYbcVsDeltaX_zcut->Draw("colz");
   hYbcVsDeltaX_Y1_zcut->Draw("colz");

   c->cd(7);
   hbcXvsY->Draw("colz");

   c->cd(8);
//    hYbcVsDeltaX_zcut->Draw("colz");
   hYbcVsDeltaY_Y2_zcut->Draw("colz");


   c->cd(9);
//    hYbcVsDeltaX_zcut->Draw("colz");
   hYbcVsDeltaX_Y2_zcut->Draw("colz");*/



   c->SaveAs(Form("plots/%d/target_recon3.png",runNumber));

   return(0);

}
