/** Looking at two cluster events.
 *
 */
Int_t target_recon8(Int_t runNumber = 72994) {

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;

   SANEEvents * events = new SANEEvents("betaDetectors1");
   events->SetClusterBranches(events->fTree);
   if(!events->fTree) return(-1);
   events->fTree->SetBranchStatus("betaDetectorEvent",0);
   events->fTree->SetBranchStatus("fBigcalEvent",0);
   events->fTree->SetBranchStatus("fGasCherenkovEvent",0);
   events->fTree->SetBranchStatus("fLuciteHodoscopeEvent",0);
   events->fTree->SetBranchStatus("fForwardTrackerEvent",0);
   events->fTree->SetBranchStatus("hmsDetectorEvent",0);
   events->fTree->SetBranchStatus("monteCarloEvent",0);

   Int_t nevents = 2000000;

   InSANEReconstructedEvent * fReconEvent = new InSANEReconstructedEvent();

   const char * treeName2 = "trackingPositions";
   TTree * t2 = (TTree*)gROOT->FindObject(treeName2);
   if(!t2){ std::cout << " TREE NOT FOUND : " << treeName2 << "\n"; return(-1);}
   if( t2->BuildIndex("fRunNumber","fEventNumber") < 0 )
      std::cout << "Failed to build branch on " << treeName2 << "\n";
   t2->AddFriend(events->fTree);
   t2->SetBranchAddress("uncorrelatedPositions",&fReconEvent);

   rman->GetCurrentFile()->cd("detectors/tracker");

   Double_t phimin = -100.0;
   Double_t phimax = 100.0;


   rman->GetCurrentFile()->cd("detectors/hodoscope");

   TH2F * hbcXvsY = new TH2F("hbcXvsY","Bigcal Y VS X", 60,-60,60,120,-120,120);

   TH2F * hBCXvsZrecon  = new TH2F("BCXvsZrecon","Bigcal X VS Z in y-z plane", 100,phimin,phimax,60,-60,60);
   TH2F * hBCXvsZrecon2 = new TH2F("BCXvsZrecon2","Bigcal X VS Z in x-z plane" , 100,phimin,phimax,60,-60,60);
   TH2F * hBCXvsXrecon  = new TH2F("hBCXvsXrecon","Bigcal X VS X_{xz} in x-z plane", 100,phimin,phimax,60,-60,60);
   TH2F * hBCXvsYrecon = new TH2F("hBCXvsYrecon","Bigcal X VS Y_{yz} in y-z plane" , 100,phimin,phimax,60,-60,60);

   TH2F * hBCYvsZrecon  = new TH2F("BCYvsZrecon","Bigcal Y VS Z in y-z plane", 100,phimin,phimax,60,-120,120);
   TH2F * hBCYvsZrecon2 = new TH2F("BCYvsZrecon2","Bigcal Y VS Z in x-z plane" , 100,phimin,phimax,60,-120,120);
   TH2F * hBCYvsXrecon  = new TH2F("hBCYvsXrecon","Bigcal Y VS X_{xz} in x-z plane", 100,phimin,phimax,60,-120,120);
   TH2F * hBCYvsYrecon = new TH2F("hBCYvsYrecon","Bigcal Y VS Y_{yz} in y-z plane" , 100,phimin,phimax,60,-120,120);

   TH2F * hLucReconYZ   = new TH2F("hLucReconYZ","Lucite reconsted Vertex Z vs Y", 100,phimin,phimax,100,phimin,phimax);
   TH2F * hLucReconXZ   = new TH2F("hLucReconXZ","Lucite reconsted Vertex Z vs X", 100,phimin,phimax,100,phimin,phimax);

   TH2F * hLucReconZZ   = new TH2F("hLucReconZZ","Lucite reconsted Vertex Z vs Z", 100,phimin,phimax,100,phimin,phimax);


   TH1F * hDeltaY_L      = new TH1F("targetDeltaY_L","#Delta Y", 100,phimin,phimax);
   TH1F * hDeltaY_L_zcut = new TH1F("targetDeltaY_L_zcut","#Delta Y target with z cut", 100,phimin,phimax);

   TH1F * hDeltaX_L      = new TH1F("targetDeltaX_L","#Delta X target", 100,phimin,phimax);
   TH1F * hDeltaX_L_zcut = new TH1F("targetDeltaX_L_zcut","#Delta X target with z cut", 100,phimin,phimax);

   TH1F * hDeltaZ_L      = new TH1F("targetDeltaZ_L","#Delta Z = Z_{yz} - Z_{xz}", 100,phimin,phimax);


   TH1F * hReconZ_L      = new TH1F("ReconZ_L","Reconstructed Z in y-z plane", 100,phimin,phimax);
   TH1F * hReconZ2_L     = new TH1F("ReconZ2_L","ReconstructedZ in x-z plane", 100,phimin,phimax);

   TH1F * hReconX_L      = new TH1F("ReconX_L","Reconstructed X in x-z plane", 100,phimin,phimax);
   TH1F * hReconY_L      = new TH1F("ReconY_L","Reconstructed Y in y-z plane", 100,phimin,phimax);



   rman->GetCurrentFile()->cd();

   Double_t RadPerDegree = 0.017453293;

   BIGCALCluster *              cluster = 0;
   ForwardTrackerPositionHit *  tkpos = 0;
   InSANEHitPosition *          hitpos = 0;
   InSANEHitPosition *          targetpos = 0;
   InSANEDISTrajectory  *       atraj = 0;

   TVector3 v1;
   TVector3 N1;
   TVector3 xzcorr;

   for(int i = 0 ; i< t2->GetEntries() && i < nevents ; i++){
      if( i%10000 == 0) std::cout << " Entry " << i << "\n";
      t2->GetEntry(i);

      events->fTree->GetEntryWithIndex(fReconEvent->fRunNumber,fReconEvent->fEventNumber);

      if( events->TRIG->IsBETAEvent() && !events->TRIG->IsPi0Event() ) {
//         /// Loop over clusters....
//         for(int ic = 0; ic < events->CLUSTER->fClusters->GetEntries() ;ic++){
//            cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[ic];
         for(int j = 0; j < fReconEvent->fTrajectories->GetEntries(); j++ ) {
	    atraj = (InSANEDISTrajectory*)(*(fReconEvent->fTrajectories))[j];
 
            cluster = (BIGCALCluster*)(*(events->CLUSTER->fClusters))[atraj->fClusterNumber];
	     
            /// Check that the cluster channel is not a noisy one!
            if( events->CLUSTER->fNClusters == 1 )
            //if( cluster->fYMoment < -20.0 ) //PROT 
            //if( cluster->GetEnergy() < 1200.0 && cluster->GetEnergy() > 800.0 )
            //if( atraj->fLuciteMissDistance < 30.0 )
            if( !(cluster->fIsNoisyChannel) && cluster->fIsGood) 
            //if( cluster->fCherenkovBestADCSum > 0.5 && cluster->fCherenkovBestADCSum < 1.3 )
            //if( cluster->fCherenkovBestADCSum > 1.8 && cluster->fCherenkovBestADCSum < 2.8 )
            if( cluster->fCherenkovBestADCSum > 0.5 && cluster->fCherenkovBestADCSum < 2.8 )
//            if( fReconEvent->fClosestLuciteMissDistance < 50.0)
            {

    	       /// Get The target raster postion
	       targetpos = (InSANEHitPosition *)(*(fReconEvent->fTargetPositions))[0];
	       double ytarg = targetpos->fPosition.Y();
	       double xtarg = targetpos->fPosition.X();


	       /// Lucite hit positions.
                for(int it = 0; it < fReconEvent->fLucitePositions->GetEntries(); it++) {
                   hitpos = (InSANEHitPosition *)(*(fReconEvent->fLucitePositions))[it];
// 
//                   xzcorr = hitpos->fPositionUncertainty;
// 		  xzcorr.SetY(0);
// 		  hitpos->fPosition = hitpos->fPosition +xzcorr;
// 
// 		  /// Calculate the track's y position in a y-z plane located at x = x_raster 
 		  v1 = (hitpos->fPosition - atraj->fPosition0);
                   N1 = v1;
 		  N1.SetMag(1);
 		  ///  Calculate the y position for the x-z plane
 		  double yrecon = (N1.Y()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Y();
 		  ///  Calculate the z position for the x-z plane
 		  double zrecon = (N1.Z()/N1.X())*(xtarg-hitpos->fPosition.X()) +hitpos->fPosition.Z();
 
 		  /// Calculate the x position for a x-z plane at the y= y_raster
 		  double xrecon = (N1.X()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.X();
 		  double zrecon2 = (N1.Z()/N1.Y())*(ytarg-hitpos->fPosition.Y()) +hitpos->fPosition.Z();
 
                  ///  Y
 		  hDeltaY_L->Fill(ytarg -yrecon);
 		  if( TMath::Abs(zrecon) < 2.0 ) hDeltaY_L_zcut->Fill(ytarg -yrecon);
 		  hReconY_L->Fill(yrecon);
 		  /// X
 		  hDeltaX_L->Fill(xtarg -xrecon);
 		  if( TMath::Abs(zrecon) < 2.0 ) hDeltaX_L_zcut->Fill(xtarg -xrecon);
 		  hReconX_L->Fill(xrecon);

 		  /// Z
                  hDeltaZ_L->Fill(zrecon-zrecon2);
 		  hReconZ_L->Fill(zrecon);
 		  hReconZ2_L->Fill(zrecon2);

                  hLucReconYZ->Fill(zrecon,yrecon);
                  hLucReconXZ->Fill(zrecon2,xrecon);

                  hBCXvsZrecon->Fill(zrecon,cluster->fXMoment);
                  hBCXvsZrecon2->Fill(zrecon2,cluster->fXMoment);|
                  hBCXvsXrecon->Fill(xrecon,cluster->fXMoment);
                  hBCXvsYrecon->Fill(yrecon,cluster->fXMoment);

                  hBCYvsZrecon->Fill(zrecon,cluster->fYMoment);
                  hBCYvsZrecon2->Fill(zrecon2,cluster->fYMoment);|
                  hBCYvsXrecon->Fill(xrecon,cluster->fYMoment);
                  hBCYvsYrecon->Fill(yrecon,cluster->fYMoment);

                  hLucReconZZ->Fill(zrecon,zrecon2);
                } // lucite hit loop


            } // not noisy cluster
         } // trajectory loop
      } // is BETA2 event
   } // event loop


   TCanvas * c = new TCanvas("target_z_recon6","target_z_recon6");
   c->Divide(4,4);


   c->cd(6);
   hLucReconYZ->Draw("colz");

   c->cd(1);
   hDeltaY_L->SetLineColor(3);
   hDeltaY_L->Draw();
   hDeltaY_L_zcut->SetLineColor(3);
   //hDeltaY_L_zcut->SetFillColor(3);
   //hDeltaY_L_zcut->SetFillStyle(3003);
   hDeltaY_L_zcut->Draw("same");

   c->cd(5);
   hReconY_L->SetLineColor(3);
   hReconY_L->Draw("hbar2");

   c->cd(2);
   hReconZ_L->SetLineColor(3);
   hReconZ_L->Draw("bar2");


   c->cd(3);
   hReconZ2_L->SetLineColor(3);
   hReconZ2_L->Draw("bar2");

   c->cd(4);

    hDeltaX_L->SetLineColor(3);
    hDeltaX_L->Draw();
    hDeltaX_L_zcut->SetLineColor(3);
//    hDeltaX_L_zcut->SetFillColor(3);
//    hDeltaX_L_zcut->SetFillStyle(3003);
    hDeltaX_L_zcut->Draw("same");


   c->cd(7);
   hLucReconXZ->Draw("colz");

   c->cd(8);
   hReconX_L->SetLineColor(3);
   hReconX_L->Draw("hbar2");

   c->cd(9);
   hBCYvsYrecon->Draw("colz");

   c->cd(10);
   hBCYvsZrecon->Draw("colz");

   c->cd(11);
   hBCYvsZrecon2->Draw("colz");

   c->cd(12);
   hBCYvsXrecon->Draw("colz");

   //c->cd(13);
   //hLucReconZZ->Draw("colz");

   c->cd(13);
   hBCXvsYrecon->Draw("colz");

   c->cd(14);
   hBCXvsZrecon->Draw("colz");

   c->cd(15);
   hBCXvsZrecon2->Draw("colz");

   c->cd(16);
   hBCXvsXrecon->Draw("colz");



   c->SaveAs(Form("plots/%d/target_recon8.png",runNumber));
   c->SaveAs(Form("plots/%d/target_recon8.pdf",runNumber));
   c->SaveAs(Form("plots/%d/target_recon8.ps",runNumber));

   return(0);

}
