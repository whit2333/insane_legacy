Int_t track_efficiencies(Int_t runGroup = 1, bool newmerge = false) {

   if (gROOT->LoadMacro("util/merge_run_list.cxx") != 0) {
      Error(weh, "Failed loading util/merge_run_list.cxx in compiled mode.");
      return -1;
   }

   const char * runlist = "lists/para/test_para.txt";
   const char * outfile = Form("data/Tracks/ParaTracks%d.root",runGroup);;

   if( newmerge )
      if( merge_run_list(runlist,outfile,"Tracks") ) { return(-1);}

   TFile * f = TFile::Open(outfile,"UPDATE");
   f->cd();
   TTree * t = (TTree*)gROOT->FindObject("Tracks");
   if(!t) return(-2);

   TH1F * h1 = 0;
   TH2F * h2 = 0;

   Double_t theta_range[2] = {0.4*180.0/TMath::Pi(),1.0*180.0/TMath::Pi()};
   Double_t phi_range[2] = {-0.6*180.0/TMath::Pi(),1.0*180.0/TMath::Pi()};
   Double_t energy_range[2] = {500,4500};

   TH2F * hPhiVsLucMiss = new TH2F("hPhiVsLucMiss","Phi vs LucMiss;#delta Lucite;#phi",
                                  100,0,50,100,phi_range[0],phi_range[1]);

   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta3 = new TH2F("hPhiVsTheta3","Phi vs Theta with tracker cuts;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   TEfficiency* effPhi3   = new TEfficiency("effPhi3","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta3 = new TEfficiency("effTheta3","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy3 = new TEfficiency("effEnergy3","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi4   = new TEfficiency("effPhi4","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta4 = new TEfficiency("effTheta4","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy4 = new TEfficiency("effEnergy4","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi5   = new TEfficiency("effPhi5","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta5 = new TEfficiency("effTheta5","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy5 = new TEfficiency("effEnergy5","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi6   = new TEfficiency("effPhi6","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta6 = new TEfficiency("effTheta6","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy6 = new TEfficiency("effEnergy6","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi2   = new TEfficiency("effPhi2","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta2 = new TEfficiency("effTheta2","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy2 = new TEfficiency("effEnergy2","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy = new TEfficiency("effEnergy","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   InSANEDISTrajectory * fTrajectory = new InSANEDISTrajectory();
   t->SetBranchAddress("trajectory",&fTrajectory);
   InSANETriggerEvent  * fTriggerEvent = new InSANETriggerEvent();
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   /// Create the Asymmetries for each Q^2 bin.
   Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};

   Int_t fEntries =  t->GetEntries();
   /// Event Loop
   for(Int_t ievent = 0; ievent < fEntries; ievent++) {

      if(ievent%10000 == 0) std::cout << ievent << "/" << fEntries << "\n";
      t->GetEntry(ievent);
      bool passed = false;
      bool passed2 = false;
      bool passed3 = false;
      bool passed4 = false;
      bool passed5 = false;

      hPhiVsTheta1->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());

      /// Standard Cuts.
      if(!fTrajectory->fIsNoisyChannel)
      if(fTrajectory->fIsGood) {
      hPhiVsLucMiss->Fill(fTrajectory->fLuciteMissDistance,fTrajectory->fPhi*180.0/TMath::Pi());

      if(fTrajectory->fNCherenkovElectrons > 0.3 && fTrajectory->fNCherenkovElectrons < 2.5) 
      if(TMath::Abs(fTrajectory->fCherenkovTDC) < 30) {

         passed5 = true;
         
         if( TMath::Abs(fTrajectory->fTrackerMiss.Y()) < 2.2 ) { 
            passed = true;
            passed2 = true;
            hPhiVsTheta3->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());
         } // tracker cut

         if( TMath::Abs(fTrajectory->fLuciteMissDistance) < 25.0 ) { 
            passed3 = true;
            passed4 = true;
            hPhiVsTheta3->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());
         } // tracker cut

         hPhiVsTheta2->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());

         effPhi->Fill(passed,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta->Fill(passed,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy->Fill(passed,fTrajectory->fEnergy);

         effPhi3->Fill(passed3,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta3->Fill(passed3,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy3->Fill(passed3,fTrajectory->fEnergy);

         effPhi6->Fill(passed3 && passed,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta6->Fill(passed3 && passed,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy6->Fill(passed3 && passed,fTrajectory->fEnergy);

      } // end cherenkov cut

      effPhi5->Fill(passed5,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta5->Fill(passed5,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy5->Fill(passed5,fTrajectory->fEnergy);

      effPhi2->Fill(passed2,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta2->Fill(passed2,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy2->Fill(passed2,fTrajectory->fEnergy);

      effPhi4->Fill(passed4,fTrajectory->fPhi*180.0/TMath::Pi());
      effTheta4->Fill(passed4,fTrajectory->fTheta*180.0/TMath::Pi());
      effEnergy4->Fill(passed4,fTrajectory->fEnergy);
      } // end bad channel cuts



   } // end event loop



   TCanvas * c = new TCanvas("track_efficiencies","track_efficiencies");
   c->Divide(3,2);

   c->cd(1);
   TMultiGraph *mg = new TMultiGraph();
   effPhi->Paint();
   if(effPhi->GetPaintedGraph() ) mg->Add(effPhi->GetPaintedGraph(),"P");

   effPhi2->SetLineColor(2);
   effPhi2->Paint();
   if(effPhi2->GetPaintedGraph() ) mg->Add(effPhi2->GetPaintedGraph(),"P");

   effPhi3->SetLineColor(3);
   effPhi3->Paint();
   if(effPhi3->GetPaintedGraph() ) mg->Add(effPhi3->GetPaintedGraph(),"P");

   effPhi4->SetLineColor(4);
   effPhi4->Paint();
   if(effPhi4->GetPaintedGraph() ) mg->Add(effPhi4->GetPaintedGraph(),"P");

   effPhi5->SetLineColor(6);
   effPhi5->Paint();
   if(effPhi5->GetPaintedGraph() ) mg->Add(effPhi5->GetPaintedGraph(),"P");

   effPhi6->SetLineColor(7);
   effPhi6->Paint();
   if(effPhi6->GetPaintedGraph() ) mg->Add(effPhi6->GetPaintedGraph(),"P");

   mg->Draw("A");
   mg->GetXaxis()->SetTitle("#phi");
   mg->GetYaxis()->SetTitle("#epsilon");
   mg->Draw("A");



   c->cd(2);
   TMultiGraph *mg2 = new TMultiGraph();

   effTheta->Paint();
   if(effTheta->GetPaintedGraph() ) mg2->Add(effTheta->GetPaintedGraph(),"P");

   effTheta2->SetLineColor(2);
   effTheta2->Paint();
   if(effTheta2->GetPaintedGraph() ) mg2->Add(effTheta2->GetPaintedGraph(),"P");

   effTheta3->SetLineColor(3);
   effTheta3->Paint();
   if(effTheta3->GetPaintedGraph() ) mg2->Add(effTheta3->GetPaintedGraph(),"P");

   effTheta4->SetLineColor(4);
   effTheta4->Paint();
   if(effTheta4->GetPaintedGraph() ) mg2->Add(effTheta4->GetPaintedGraph(),"P");

   effTheta5->SetLineColor(6);
   effTheta5->Paint();
   if(effTheta5->GetPaintedGraph() ) mg2->Add(effTheta5->GetPaintedGraph(),"P");

   effTheta6->SetLineColor(7);
   effTheta6->Paint();
   if(effTheta6->GetPaintedGraph() ) mg2->Add(effTheta6->GetPaintedGraph(),"P");

   mg2->Draw("A");
   mg2->GetXaxis()->SetTitle("#theta");
   mg2->GetYaxis()->SetTitle("#epsilon");
   mg2->Draw("A");

   c->cd(3);
   TMultiGraph *mg3 = new TMultiGraph();

   effEnergy->Paint();
   if(effEnergy->GetPaintedGraph() ) mg3->Add(effEnergy->GetPaintedGraph(),"P");

   effEnergy2->SetLineColor(2);
   effEnergy2->Paint();
   if(effEnergy2->GetPaintedGraph() ) mg3->Add(effEnergy2->GetPaintedGraph(),"P");

   effEnergy3->SetLineColor(3);
   effEnergy3->Paint();
   if(effEnergy3->GetPaintedGraph() ) mg3->Add(effEnergy3->GetPaintedGraph(),"P");

   effEnergy4->SetLineColor(4);
   effEnergy4->Paint();
   if(effEnergy4->GetPaintedGraph() ) mg3->Add(effEnergy4->GetPaintedGraph(),"P");

   effEnergy5->SetLineColor(6);
   effEnergy5->Paint();
   if(effEnergy5->GetPaintedGraph() ) mg3->Add(effEnergy5->GetPaintedGraph(),"P");

   effEnergy6->SetLineColor(7);
   effEnergy6->Paint();
   if(effEnergy6->GetPaintedGraph() ) mg3->Add(effEnergy6->GetPaintedGraph(),"P");

   mg3->Draw("A");
   mg3->GetXaxis()->SetTitle("E'");
   mg3->GetYaxis()->SetTitle("#epsilon");
   mg3->Draw("A");

   c->cd(4);
   hPhiVsLucMiss->Draw("colz");

   c->cd(5);
   hPhiVsTheta2->Draw("colz");

   c->cd(6);
   hPhiVsTheta3->Draw("colz");

   c->SaveAs(Form("plots/para/track_efficiencies-%d.png",runGroup));

   t->StartViewer();
//    new TBrowser;
   return(0);

}
