Int_t prop_test2(){

   InSANETrackPropagator * tp = new InSANETrackPropagator();
   tp->SetTargetPolAngle(180.0);

   TVector3 p0;
   TVector3 v0;
   TVector3 p1;
   TVector3 v1;

   Double_t theta = 35;
   Double_t p = 1.0;
   for(int i = 0;i<10;i++){
      theta += 2.0;
      Double_t phi = -20;

      for(int j = 0;j<10;j++){
         phi += 3.0;
         p0.SetMagThetaPhi(p,theta*degree,phi*degree);
//          p0 = -1.0*p0;
         v0.SetMagThetaPhi(335.0,theta*degree,phi*degree);
         tp->SetTrackMomentumPosition(p0,v0);
         tp->GetMomentumPositionAtTarget(&p1,&v1);
         p0.Print();
         v0.Print();
         p1.Print();
         v1.Print();
         tp->DrawTrack();

      }
   }
   return(0);
}