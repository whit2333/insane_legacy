Int_t positron_id1(Int_t runNumber = 73006 ) {

   rman->SetRun(runNumber);

   InSANEDISTrajectory  * fTrajectory = new InSANEDISTrajectory();
   InSANETriggerEvent   * fTriggerEvent = new InSANETriggerEvent();

   TTree * t = (TTree*)gROOT->FindObject("Tracks2");
   if(!t) return(-1);
   t->SetBranchAddress("trajectory",&fTrajectory);
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   std::cout << t->GetEntries() << " events \n";
   //t->StartViewer();
   //return(0);

   Double_t zmin = -1;
   Double_t zmax =  1;
   //Double_t zmin2 = -0.001;
   //Double_t zmax2 =  0.0005;
   Double_t zmin2 = -2.0;
   Double_t zmax2 =  2.0;
   Double_t min = -0.30;
   Double_t max =  0.30;


   TH1F * hZa1            = new TH1F("hZa_1","Z_{YZ} a1", 100,zmin,zmax);
   TH1F * hZb1            = new TH1F("hZb_1","Z_{YZ} b1", 100,zmin,zmax);

   TH1F * hZ_yz           = new TH1F("hZ_yz","Z_{YZ}", 100,zmin,zmax);
   TH1F * hZtracker_1     = new TH1F("hZtracker_1","Ztracker_{YZ}", 100,zmin,zmax);
   TH1F * hZtracker_yz    = new TH1F("hZtracker_yz","Ztracker_{YZ}", 100,zmin,zmax);
   TH1F * hZ_xz           = new TH1F("hZ_xZ","Z_{XZ}", 100,zmin,zmax);
   TH1F * hZtracker_xz    = new TH1F("hZtracker_xZ","Ztracker_{XZ}", 100,zmin,zmax);

   TH1F * hY_yz           = new TH1F("hY_yz",          "Y_{YZ}", 100,zmin,zmax);
   TH1F * hYtracker_1     = new TH1F("hYtracker_1",   "Ytracker_{YZ}", 100,zmin,zmax);
   TH1F * hYtracker_yz    = new TH1F("hYtracker_yz",   "Ytracker_{YZ}", 100,zmin,zmax);
   TH1F * hY_xz           = new TH1F("hY_xZ",          "Y_{XZ}", 100,zmin,zmax);
   TH1F * hYtracker_xz    = new TH1F("hYtracker_xZ",   "Ytracker_{XZ}", 100,zmin,zmax);

   TH1F * hdelY_yz        = new TH1F("hdelY_yz",          "#Delta Y_{YZ}", 100,zmin,zmax);
   TH1F * hdelYtracker_1  = new TH1F("hdelYtracker_1",   "#Delta Ytracker_{YZ}1", 100,zmin2,zmax2);
   TH1F * hdelYtracker_2  = new TH1F("hdelYtracker_2",   "#Delta Ytracker_{YZ}2", 100,zmin2,zmax2);
   TH1F * hdelYtracker_3  = new TH1F("hdelYtracker_3",   "#Delta Ytracker_{YZ}3", 100,zmin2,zmax2);
   TH1F * hdelYtracker_4  = new TH1F("hdelYtracker_4",   "#Delta Ytracker_{YZ}4", 100,zmin2,zmax2);
   TH1F * hdelYtracker_yz = new TH1F("hdelYtracker_yz",   "#Delta Ytracker_{YZ}", 100,zmin,zmax);
   TH1F * hdelY_xz        = new TH1F("hdelY_xZ",          "#Delta Y_{XZ}", 100,zmin,zmax);
   TH1F * hdelYtracker_xz = new TH1F("hdelYtracker_xZ",   "#Delta Ytracker_{XZ}", 100,zmin,zmax);

   TH1F * hdelZ_1         = new TH1F("hdelZ_1",   "#Delta Z1", 100,zmin2,zmax2);
   TH1F * hdelZ_2         = new TH1F("hdelZ_2",   "#Delta Z2", 100,zmin2,zmax2);
   TH1F * hdelZ_3         = new TH1F("hdelZ_3",   "#Delta Z3", 100,zmin2,zmax2);
   TH1F * hdelZ_4         = new TH1F("hdelZ_4",   "#Delta Z4", 100,zmin2,zmax2);

   TH1F * hdelYt_1        = new TH1F("hdelYt_1",   "#Delta Yt1", 100,min,max);
   TH1F * hdelYt_2        = new TH1F("hdelYt_2",   "#Delta Yt2", 100,min,max);
   TH1F * hdelYt_3        = new TH1F("hdelYt_3",   "#Delta Yt3", 100,min,max);
   TH1F * hdelYt_4        = new TH1F("hdelYt_4",   "#Delta Yt4", 100,min,max);

   TH1F * hYt_1           = new TH1F("hYt_1",   "Yt1", 100,-2,2);
   TH1F * hYt_2           = new TH1F("hYt_2",   "Yt2", 100,-2,2);
   TH1F * hYt_3           = new TH1F("hYt_3",   "Yt3", 100,-2,2);
   TH1F * hYt_4           = new TH1F("hYt_4",   "Yt4", 100,-2,2);

   TH1F * hX_yz           = new TH1F("hX_yz",          "X_{YZ}", 100,zmin,zmax);
   TH1F * hXtracker_1     = new TH1F("hXtracker_1",   "Xtracker_{YZ}", 100,zmin,zmax);
   TH1F * hXtracker_yz    = new TH1F("hXtracker_yz",   "Xtracker_{YZ}", 100,zmin,zmax);
   TH1F * hX_xz           = new TH1F("hX_xZ",          "X_{XZ}", 100,zmin,zmax);
   TH1F * hXtracker_xz    = new TH1F("hXtracker_xZ",   "Xtracker_{XZ}", 100,zmin,zmax);

   TH2F * hYZ_1           = new TH2F("hYZ_1",          "Yrast-Yrecon1 vs Ytracker",100,min,max,40,-20,20);
   TH2F * hYZ_2           = new TH2F("hYZ_2",          "Yrast-Yrecon1 vs Ytracker",100,min,max,40,-20,20);
   TH2F * hYZ_3           = new TH2F("hYZ_3",          "Yrast-Yrecon1 vs Ytracker",100,min,max,40,-20,20);
   TH2F * hYZ_4           = new TH2F("hYZ_4",          "Yrast-Yrecon1 vs Ytracker",100,min,max,40,-20,20);

   TH2F * hYY_1           = new TH2F("hYY_1",          "Yrast-Yrecon vs Ytracker",100,min,max,40,-20,20);
   TH2F * hYY_2           = new TH2F("hYY_2",          "Yrast-Yrecon vs Ytracker",100,min,max,40,-20,20);
   TH2F * hYY_3           = new TH2F("hYY_3",          "Yrast-Yrecon vs Ytracker",100,min,max,40,-20,20);
   TH2F * hYY_4           = new TH2F("hYY_4",          "Yrast-Yrecon vs Ytracker",100,min,max,40,-20,20);

   TH2F * hYYt_1          = new TH2F("hYYt_1",          "Y vs Y tracker",50,-4,4,40,-20,20);
   TH2F * hYYt_2          = new TH2F("hYYt_2",          "Y vs Y tracker",50,-4,4,40,-20,20);
   TH2F * hYYt_3          = new TH2F("hYYt_3",          "Y vs Y tracker",50,-4,4,40,-20,20);
   TH2F * hYYt_4          = new TH2F("hYYt_4",          "Y vs Y tracker",50,-4,4,40,-20,20);

   Int_t Nevents = t->GetEntries();
   for(int ievent = 0 ; ievent < Nevents; ievent++) {
      if(ievent%10000 == 0) std::cout << ievent << "\n";
      t->GetEntry(ievent);
      if(fTriggerEvent->IsBETAEvent() )
      //if(fTrajectory->fTrackerPosition.Y() > -22 && fTrajectory->fTrackerPosition.Y() < 22 )  
      //if(fTrajectory->fTrackerPosition.Y() > 1 && fTrajectory->fTrackerPosition.Y() < 10 )  
      if(fTrajectory->fReconTarget.Z() > -0.5 && fTrajectory->fReconTarget.Z() < 0.8 )  
      {
      if( fTrajectory->fEnergy < 1200 &&  fTrajectory->fNCherenkovElectrons > 0.3 && fTrajectory->fNCherenkovElectrons < 1.7 ) {

         hYZ_1->Fill(fTrajectory->fReconTarget1.Z(),fTrajectory->fReconTarget2.Y());

         hZ_yz->Fill(fTrajectory->fReconTarget1.Z());
         hZtracker_1->Fill(fTrajectory->fReconTarget.Z());

         hZ_xz->Fill(fTrajectory->fReconTarget2.Z());
         hZtracker_yz->Fill(fTrajectory->fReconTargetY1.Z());
         hZtracker_xz->Fill(fTrajectory->fReconTargetY2.Z());

         hY_yz->Fill(fTrajectory->fReconTarget1.Y());
         hY_xz->Fill(fTrajectory->fReconTarget2.Y());
         hYtracker_1->Fill(fTrajectory->fReconTarget.Y());
         hYtracker_yz->Fill(fTrajectory->fReconTargetY1.Y());
         hYtracker_xz->Fill(fTrajectory->fReconTargetY2.Y());

         hdelY_yz->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y() );
         hdelY_xz->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget2.Y());
         hdelYtracker_yz->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTargetY1.Y());
         hdelYtracker_xz->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTargetY2.Y());

         //hdelYtracker_1->Fill(fTrajectory->fReconTarget.Y() - fTrajectory->fReconTarget1.Y());

         hX_yz->Fill(fTrajectory->fReconTarget1.X());
         hX_xz->Fill(fTrajectory->fReconTarget2.X());
         hXtracker_1->Fill(fTrajectory->fReconTarget.X());
         hXtracker_yz->Fill(fTrajectory->fReconTargetY1.X());
         hXtracker_xz->Fill(fTrajectory->fReconTargetY2.X());
      }

      if( fTrajectory->fEnergy > 1300  && fTrajectory->fEnergy < 1400 &&  
          fTrajectory->fNCherenkovElectrons > 0.5  &&  fTrajectory->fNCherenkovElectrons < 1.8  ) 
      {
         hdelZ_1->Fill(fTrajectory->fReconTarget.Z() - fTrajectory->fReconTarget1.Z());
         //hdelYt_1->Fill(fTrajectory->fTrackerMiss.Y());
         hdelYt_1->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y());
         hYt_1->Fill(fTrajectory->fTrackerMiss.Y());
         hYYt_1->Fill(fTrajectory->fTrackerMiss.Y(),fTrajectory->fTrackerPosition.Y());
         hdelYtracker_1->Fill(fTrajectory->fReconTarget.Y() - fTrajectory->fReconTarget1.Y());
         hYZ_1->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),fTrajectory->fTrackerPosition.Y());
      }
      if( fTrajectory->fEnergy > 1200  && fTrajectory->fEnergy < 1300 &&  
          fTrajectory->fNCherenkovElectrons > 0.5  &&  fTrajectory->fNCherenkovElectrons < 1.8  ) 
          //fTrajectory->fNCherenkovElectrons > 1.8  &&  fTrajectory->fNCherenkovElectrons < 3.0  ) 
          //fTrajectory->fNCherenkovElectrons < 0.3  ) 
      {
         hdelZ_2->Fill(fTrajectory->fReconTarget.Z() - fTrajectory->fReconTarget1.Z());
         //hdelYt_2->Fill(fTrajectory->fTrackerMiss.Y());
         hYt_2->Fill(fTrajectory->fTrackerMiss.Y());
         hYYt_2->Fill(fTrajectory->fTrackerMiss.Y(),fTrajectory->fTrackerPosition.Y());
         hdelYt_2->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y());
         hdelYtracker_2->Fill(fTrajectory->fReconTarget.Y() - fTrajectory->fReconTarget1.Y());
         hYZ_2->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),fTrajectory->fTrackerPosition.Y());
      }
      if( fTrajectory->fEnergy > 1000 && fTrajectory->fEnergy < 1100 &&  
          fTrajectory->fNCherenkovElectrons > 0.5  &&  fTrajectory->fNCherenkovElectrons < 1.8  ) 
      {
         hdelZ_3->Fill(fTrajectory->fReconTarget.Z() - fTrajectory->fReconTarget1.Z());
         //hdelYt_3->Fill(fTrajectory->fTrackerMiss.Y());
         hYt_3->Fill(fTrajectory->fTrackerMiss.Y());
         hYYt_3->Fill(fTrajectory->fTrackerMiss.Y(),fTrajectory->fTrackerPosition.Y());
         hdelYt_3->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y());
         hdelYtracker_3->Fill( fTrajectory->fReconTarget.Y() - fTrajectory->fReconTarget1.Y());
         hYZ_3->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(), fTrajectory->fTrackerPosition.Y());
      }
      if( fTrajectory->fEnergy > 500  && fTrajectory->fEnergy < 900 &&  
          fTrajectory->fNCherenkovElectrons > 0.5  &&  fTrajectory->fNCherenkovElectrons < 1.8  ) 
          //fTrajectory->fNCherenkovElectrons > 1.8  &&  fTrajectory->fNCherenkovElectrons < 3.0  ) 
          //fTrajectory->fNCherenkovElectrons < 0.3  ) 
      {
         hdelZ_4->Fill(fTrajectory->fReconTarget.Z() - fTrajectory->fReconTarget1.Z());
         hdelYt_4->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y());
         hYt_4->Fill(fTrajectory->fTrackerMiss.Y());
         hYYt_4->Fill(fTrajectory->fTrackerMiss.Y(),fTrajectory->fTrackerPosition.Y());
         //hdelYt_4->Fill(fTrajectory->fTrackerMiss.Y());
         hdelYtracker_4->Fill(fTrajectory->fReconTarget.Y() - fTrajectory->fReconTarget1.Y());
         hYZ_4->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),fTrajectory->fTrackerPosition.Y());
      }
      }

   }

   TCanvas * c = new TCanvas("positron_id1","positron_id1");//,800,600);
   c->Divide(2,4);
  

   c->cd(1);
   hZ_yz->SetLineColor(4);
   hZ_yz->Draw();

   hZtracker_1->SetLineColor(kRed);
   hZtracker_1->Draw("same");

   hZ_xz->SetLineColor(2);
   //hZ_xz->Draw("same");
   hZtracker_yz->SetLineColor(kBlue-9);
   //hZtracker_yz->Draw("same");
   hZtracker_xz->SetLineColor(kRed-9);
   //hZtracker_xz->Draw("same");

   TLegend * leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->AddEntry(hZ_yz,"Z straight line ","l");
   leg->AddEntry(hZtracker_1,"Z Propagated","l");
   leg->Draw();

   c->cd(2);
   hY_yz->SetLineColor(4);
   hY_yz->Draw();

   hYtracker_1->SetLineColor(kRed);
   hYtracker_1->Draw("same");

   hY_xz->SetLineColor(2);
   //hY_xz->Draw("same");
   hYtracker_yz->SetLineColor(kBlue-9);
   //hYtracker_yz->Draw("same");
   hYtracker_xz->SetLineColor(kRed-9);
   //hYtracker_xz->Draw("same");

   c->cd(3);
   hdelY_yz->SetLineColor(4);
   //hdelY_yz->Draw();
   hdelY_xz->SetLineColor(2);
   //hdelY_xz->Draw("same");
   hdelYtracker_yz->SetLineColor(kBlue-9);
   //hdelYtracker_yz->Draw("same");

   hdelYtracker_1->SetLineColor(kRed);
   hdelYtracker_2->SetLineColor(kRed-9);
   hdelYtracker_3->SetLineColor(kBlue);
   hdelYtracker_4->SetLineColor(kBlue-9);
   hdelYtracker_3->Draw();
   hdelYtracker_1->Draw("same");
   hdelYtracker_2->Draw("same");
   hdelYtracker_4->Draw("same");
   
   
   //hdelYtracker_xz->SetLineColor(kRed-9);
   //hdelYtracker_xz->Draw("same");

   c->cd(4);
   hdelZ_1->SetLineColor(kRed);
   hdelZ_1->Draw();
   hdelZ_2->SetLineColor(kRed-9);
   hdelZ_2->Draw("same");
   hdelZ_3->SetLineColor(kBlue);
   hdelZ_3->Draw("same");
   hdelZ_4->SetLineColor(kBlue-9);
   hdelZ_4->Draw("same");
   

   TLegend * leg2 = new TLegend(0.1,0.7,0.48,0.9);
   leg2->AddEntry(hdelZ_1,"high E, 1e ","l");
   leg2->AddEntry(hdelZ_2,"high E, 2e ","l");
   leg2->AddEntry(hdelZ_3,"low E, 1e ","l");
   leg2->AddEntry(hdelZ_4,"low E, 2e ","l");
   leg2->Draw();


   c->cd(5);
   hdelYt_1->SetLineColor(kRed);
   hdelYt_2->SetLineColor(kRed-9);
   hdelYt_3->SetLineColor(kBlue);
   hdelYt_4->SetLineColor(kBlue-9);
   hdelYt_3->Draw();
   hdelYt_1->Draw("same");
   hdelYt_2->Draw("same");
   hdelYt_4->Draw("same");

   Int_t lowbin  = hdelYt_1->GetXaxis()->FindBin(-0.29);
   Int_t midbin  = hdelYt_1->GetXaxis()->FindBin(-0.175);
   Int_t highbin = hdelYt_1->GetXaxis()->FindBin(0.2);

   Double_t r1 = hdelYt_1->Integral(lowbin,midbin)/hdelYt_1->Integral(midbin,highbin);
   std::cout << r1 << "\n";
   Double_t r2 = hdelYt_2->Integral(lowbin,midbin)/hdelYt_2->Integral(midbin,highbin);
   std::cout << r2 << "\n";
   Double_t r3 = hdelYt_3->Integral(lowbin,midbin)/hdelYt_3->Integral(midbin,highbin);
   std::cout << r3 << "\n";
   Double_t r4 = hdelYt_4->Integral(lowbin,midbin)/hdelYt_4->Integral(midbin,highbin);
   std::cout << r4 << "\n";

   c->cd(6);
   hYt_1->SetLineColor(kRed);
   hYt_2->SetLineColor(kRed-9);
   hYt_3->SetLineColor(kBlue);
   hYt_4->SetLineColor(kBlue-9);
   hYt_3->Draw();
   hYt_1->Draw("same");
   hYt_2->Draw("same");
   hYt_4->Draw("same");



   c->cd(7)->Divide(2,2);
   c->cd(7)->cd(1);
   hYZ_1->Draw("colz");
   c->cd(7)->cd(2);
   hYZ_2->Draw("colz");
   c->cd(7)->cd(3);
   hYZ_3->Draw("colz");
   c->cd(7)->cd(4);
   hYZ_4->Draw("colz");

   c->cd(8)->Divide(2,2);
   c->cd(8)->cd(1);
   hYYt_1->Draw("colz");
   c->cd(8)->cd(2);
   hYYt_2->Draw("colz");
   c->cd(8)->cd(3);
   hYYt_3->Draw("colz");
   c->cd(8)->cd(4);
   hYYt_4->Draw("colz");

   c->SaveAs(Form("plots/%d/positron_id1.png",runNumber));
   c->SaveAs(Form("plots/%d/positron_id1.pdf",runNumber));

   return(0);
}

