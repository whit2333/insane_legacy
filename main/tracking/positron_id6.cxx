Int_t positron_id6(Int_t runNumber = 73006 , Double_t Energy0 = 800, Bool_t hfile = false) {

   rman->SetRun(runNumber);
   InSANEDISTrajectory  * fTrajectory = new InSANEDISTrajectory();
   InSANETriggerEvent   * fTriggerEvent = new InSANETriggerEvent();
   BIGCALCluster        * cluster = 0;

   TTree * t = (TTree*)gROOT->FindObject("Tracks2");
   if(!t) return(-1);
   t->SetBranchAddress("trajectory",&fTrajectory);
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   std::cout << t->GetEntries() << " events \n";
   //t->StartViewer();
   //return(0);

   Double_t zmin = -1;
   Double_t zmax =  1;
   //Double_t zmin2 = -0.001;
   //Double_t zmax2 =  0.0005;
   Double_t zmin2 = -2.0;
   Double_t zmax2 =  2.0;
   Double_t min = -0.30;
   Double_t max =  0.30;

   /// Binning in energy
   const int  nbins = 10;
   Int_t      binPlotted = 1; 
   Double_t   Elow  = 500;
   Double_t   Ehigh = 1400;
   Double_t   Edelta = (Ehigh-Elow)/float(nbins);

   Double_t   Pt[nbins];
   Double_t   Energy[nbins];
   Double_t   x[nbins];
   Int_t      nBinEvents[nbins];
   Double_t   r[nbins];
   Double_t   r2[nbins];
   Double_t   r3[nbins];
   Double_t   N_3_plus[nbins];
   Double_t   N_3_minus[nbins];
   TH1F * hdelYts[nbins];


   /// Output file
   if(hfile) {
      TString fname = Form("data/positrons%d.root",runNumber);
      TFile * f = gROOT->GetListOfFiles()->FindObject(fname.Data());
      if(!f) return( -3 );
      TString dir = Form("positron-id6-%d",int(Energy0));
      f->mkdir(dir.Data());
      f->cd(dir.Data());
   }
   /// Histograms
   TH1F * hdelYs[nbins];

   TH2F * hTrackeryVsx[nbins];

   TH2F * hTrackeryVsdelY[nbins];
   TH2F * hTrackerxVsdelY[nbins];

   TH2F * hTrackeryVsdelYtracker[nbins];
   TH2F * hTrackerxVsdelYtracker[nbins];

   TH2F * hBigcalYVsdelY[nbins];
   TH2F * hBigcalXVsdelY[nbins];
   TH2F * hBigcalYVsY[nbins];
   TH2F * hBigcalXVsY[nbins];

   TH2F * hTrackeryVsdelY2[nbins];
   TH2F * hTrackeryVsdelY3[nbins];

   for(int i = 0; i<nbins; i++) {
      Pt[i] = 0.0;
      x[i] = 0.0;
      Energy[i] = 0.0;
      nBinEvents[i] = 0;
      hdelYts[i]      = new TH1F(Form("hdelYt_%d",i),Form("#Delta Yt %d", i ), 60,-0.4,0.4);
      hdelYs[i]      = new TH1F(Form("hdelY_%d",i),Form("#Delta Y%d",i), 60,-1.0,1.0);
      hTrackeryVsx[i] = new TH2F(Form("hTrackeryVsx-%d",i),Form("Y_{Tracker} Vs X_{Tracker} ;X;Y")
                                     ,30,25,45,60,-20,20);
      hTrackeryVsdelY[i] = new TH2F(Form("hTrackeryVsdelY-%d"),Form("Y_{Tracker} Vs #Delta_{Y} %d;#Delta_{Y}",
                                     i),60,-0.4,0.4,60,-20,20);
      hTrackerxVsdelY[i] = new TH2F(Form("hTrackerxVsdelY-%d"),Form("X_{Tracker} Vs #Delta_{Y} %d;#Delta_{Y}",
                                    i),60,-0.4,0.4,60,20,40);

      hBigcalYVsdelY[i] = new TH2F(Form("hBigcalYVsdelY-%d"),Form("Y_{Bigcal} Vs #Delta_{Y} %d;#Delta_{Y}",
                                    i),60,-0.4,0.4,60,-120,120);

      hBigcalXVsdelY[i] = new TH2F(Form("hBigcalXVsdelY-%d"),Form("X_{Bigcal} Vs #Delta_{Y} %d;#Delta_{Y}",
                                    i),60,-0.4,0.4,60,170,270);

      hBigcalYVsY[i] = new TH2F(Form("hBigcalYVsY-%d"),Form("Y_{Bigcal} Vs Y %d;Y",
                                    i),60,-0.5,0.5,60,-120,120);

      hBigcalXVsY[i] = new TH2F(Form("hBigcalXVsY-%d"),Form("X_{Bigcal} Vs #Delta_{Y} %d;#Y",
                                    i),60,-0.5,0.5,60,170,270);


      /*
      hTrackeryVsdelYtracker[i] = new TH2F(Form("hTrackeryVsdelYtracker-%d"),Form("Y_{Tracker} Vs #Delta_{YTrac} %d;#Delta_{YTrac}",
                                     i),60,-1.5,1.5,60,-20,20);
      hTrackerxVsdelYtracker[i] = new TH2F(Form("hTrackerxVsdelYtracker-%d"),Form("X_{Tracker} Vs #Delta_{YTrac} %d;#Delta_{YTrac}",
                                    i),60,-1.5,1.5,60,20,45);
      hTrackeryVsdelY2[i] = new TH2F(Form("hTrackeryVsdelY2-%d"),Form("Y_{Tracker} Vs #Delta_{YLine} %d;#Delta_{YLine}", i),60,-1.4,1.4,60,-20,20);
      hTrackeryVsdelY3[i] = new TH2F(Form("hTrackeryVsdelY2-%d"),Form("#Delta_{Y} Vs #Delta_{YLine} %d;#Delta_{YLine}", i),60,-0.4,0.1,60,-1.4,1.4);
      */
      
   }

   Int_t Nevents = t->GetEntries();
   for(int ievent = 0 ; ievent < Nevents; ievent++) {
      if(ievent%10000 == 0) std::cout << ievent << "\n";
      t->GetEntry(ievent);

      /// Selection 
      if(fTriggerEvent->IsBETAEvent() )
         if(fTrajectory->fNTrackerNeighbors == 0 ) 
            if(fTrajectory->fReconTarget.Z() > -5 && fTrajectory->fReconTarget.Z() < 5 )  
            {
               if( TMath::Abs( fTrajectory->fEnergy - Energy0 ) < 50 ) 
               if( fTrajectory->fNCherenkovElectrons > 0.3  &&  fTrajectory->fNCherenkovElectrons < 1.5  ) 
                  //for(int i = 0; i<nbins; i++) {
                  //   if( fTrajectory->fEnergy > Elow + float(i)*Edelta &&
                  //         fTrajectory->fEnergy < Elow + float(i+1)*Edelta ) 
                     { 
        
                        Int_t Nbin = -1; 
                        //if(fTrajectory->fXBigcal_C.Y() >  60 && fTrajectory->fXBigcal_C.Y() <   120 ) Nbin = 0; 
                        //if(fTrajectory->fXBigcal_C.Y() >   0 && fTrajectory->fXBigcal_C.Y() <    60 ) Nbin = 1; 
                        //if(fTrajectory->fXBigcal_C.Y() > -60 && fTrajectory->fXBigcal_C.Y() <     0 ) Nbin = 2; 
                        //if(fTrajectory->fXBigcal_C.Y() > -120 && fTrajectory->fXBigcal_C.Y() < -120 ) Nbin = 3; 
                        if(fTrajectory->fTrackerPosition.Y() > -22 && fTrajectory->fTrackerPosition.Y() < -10 ) Nbin = 3; 
                        if(fTrajectory->fTrackerPosition.Y() > -10 && fTrajectory->fTrackerPosition.Y() <   0 ) Nbin = 2;
                        if(fTrajectory->fTrackerPosition.Y() >   0 && fTrajectory->fTrackerPosition.Y() <  10 ) Nbin = 1; 
                        if(fTrajectory->fTrackerPosition.Y() >  10 && fTrajectory->fTrackerPosition.Y() <  22 ) Nbin = 0; 

                        if( Nbin == -1 ) continue;

                        hdelYts[Nbin]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y());
                        hdelYs[Nbin]->Fill(fTrajectory->fTrackerMiss.Y());

                        hBigcalYVsdelY[Nbin]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fXBigcal_C.Y());

                        hBigcalXVsdelY[Nbin]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fXBigcal_C.X());

                        hBigcalYVsY[Nbin]->Fill(fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fXBigcal_C.Y());

                        hBigcalXVsY[Nbin]->Fill(fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fXBigcal_C.X());

                        hTrackeryVsdelY[Nbin]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fTrackerPosition.Y());

                        hTrackeryVsx[Nbin]->Fill(fTrajectory->fTrackerPosition.X(),
                                                 fTrajectory->fTrackerPosition.Y());

                        hTrackerxVsdelY[Nbin]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 TMath::Sqrt(fTrajectory->fTrackerPosition.X()*fTrajectory->fTrackerPosition.X() + fTrajectory->fTrackerPosition.Y()*fTrajectory->fTrackerPosition.Y()));
/*
                        hTrackeryVsdelYtracker[Nbin]->Fill(fTrajectory->fTrackerMiss.Y(),
                                                 fTrajectory->fTrackerPosition.Y());

                        hTrackerxVsdelYtracker[Nbin]->Fill(fTrajectory->fTrackerMiss.X(),
                                                 fTrajectory->fTrackerPosition.X());

                        hTrackeryVsdelY2[Nbin]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget.Y(),
                                                  fTrajectory->fTrackerPosition.Y());

                        hTrackeryVsdelY3[Nbin]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget.Y());
*/
                        Pt[Nbin] += TMath::Sin(fTrajectory->fTheta)*fTrajectory->fEnergy;
                        Energy[Nbin] += fTrajectory->fEnergy;
                        x[Nbin] += fTrajectory->GetBjorkenX();
                        nBinEvents[Nbin]++;
                     //}
                  }
            }

   }


   //--------------------------------------------------------------
   //t->StartViewer();
   //---------------------------------------------------------------

   TCanvas * c2 = new TCanvas("positron_id6_c2","positron_id6_c2");
   c2->Divide(3,1);
   c2->cd(1)->Divide(1,2);
   c2->cd(2)->Divide(1,2);

   TLegend * leg = new TLegend(0.1,0.7,0.35,0.9);
   leg->SetHeader(Form("E = %d #pm 50 MeV",int(Energy0)));
   THStack * hs = new THStack("hs","#Delta Y - tracker");
   THStack * hs2 = new THStack("hs2","#Delta Y - target");
   
   for(int i = 0; i<nbins;i++){
      //for(int i = nbins-1;i>=0;i--){
      hdelYs[i]->SetLineColor(4000+i+8);
      hdelYs[i]->SetFillColor(4500+i+8);

      hdelYts[i]->SetLineColor(4000+i+8);
      hdelYts[i]->SetFillColor(4500+i+8);

      hTrackeryVsx[i]->SetLineColor(4000+i+8);
      hTrackeryVsx[i]->SetFillColor(4000+i+8);

      leg->AddEntry(hdelYts[i],hdelYts[i]->GetTitle(),"f");

      hs->Add(hdelYs[i]);
      hs2->Add(hdelYts[i]);
   }
   c2->cd(1)->cd(1);
   hs->Draw("nostack");
   c2->cd(2)->cd(1);
   hs2->Draw("nostack");
   leg->Draw();

   c2->cd(1)->cd(2);
   hs->Draw();
   c2->cd(2)->cd(2);
   hs2->Draw();

   TPaveText *pt = new TPaveText(.15,.75,.43,.85,"NDC");
   pt->AddText(Form("E = %d MeV",int(Energy0)));
   pt->Draw();

   c2->cd(3);
   hTrackeryVsx[0]->Draw("box");
   hTrackeryVsx[1]->Draw("box same");
   hTrackeryVsx[2]->Draw("box same");
   hTrackeryVsx[3]->Draw("box same");


   c2->SaveAs(Form("plots/%d/positron_id6_c2_%d.png",runNumber,(int)Energy0));
   c2->SaveAs(Form("plots/%d/positron_id6_c2_%d.pdf",runNumber,(int)Energy0));

   //---------------------------------------------------------------

   gROOT->Reset();
   return(0);
}

/**
 */ 
Int_t positron_id6_loop(Int_t runnumber = 72994){

   TFile * f = new TFile(Form("data/positrons%d.root",runnumber),"RECREATE");
   Int_t n = 20;
   Double_t deltaE = 50.;
   Double_t E0   = 600;
   for(int i = 0; i < n ; i++) {
      Int_t res = positron_id6(runnumber, E0+double(i)*deltaE, true);
      if(res) return(-1);
   }
   return(0);
}

/**
 */ 
Int_t ratios_id6(Int_t runnumber = 72995){

   TFile * f = new TFile(Form("data/positrons%d.root",runnumber),"READ");
   TString fname = Form("data/positrons%d.root",runNumber);
   if(!f) return( -3 );
   
   const Int_t nbins = 20;
   Double_t   Pt[nbins];
   Double_t   Energy[nbins];
   Double_t   x[nbins];
   Int_t      nBinEvents[nbins];
   Double_t   r[nbins];
   Double_t   r2[nbins];
   Double_t   r3[nbins];
   Double_t   N_3_plus[nbins];
   Double_t   N_3_minus[nbins];

   Double_t   deltaE = 50.;
   Double_t   E0     = 600;

   for(int i = 0; i < nbins ; i++) {

      Double_t Energy0 = E0+double(i)*deltaE;
      TString dir      = Form("positron-id6-%d",int(Energy0));
      f->cd(dir.Data());
     
      TH1F * hTarg  = (TH1F*)gROOT->FindObject(Form("hdelYt_%d",i));
      TH1F * hTrack = (TH1F*)gROOT->FindObject(Form("hdelY_%d",i));
   
      Int_t lowbin  = hTarg->GetXaxis()->FindBin(-0.3);
      Int_t midbin  = hTarg->GetXaxis()->FindBin(-0.13);
      Int_t highbin = hTarg->GetXaxis()->FindBin(0.1);

      Int_t lowbin2  = hTrack->GetXaxis()->FindBin(-1.0);
      Int_t midbin2  = hTrack->GetXaxis()->FindBin(0.2);
      Int_t highbin2 = hTrack->GetXaxis()->FindBin(1.0);

      Pt[i]     = Pt[i]/float(nBinEvents[i]);
      x[i]      = x[i]/float(nBinEvents[i]);
      Energy[i] = Energy[i]/float(nBinEvents[i]);
      r[i]      = hdelYts[i]->Integral(lowbin,midbin)/hdelYts[i]->Integral(midbin+1,highbin);
      r2[i]     = 1.0/(hdelYs[i]->Integral(lowbin2,midbin2)/hdelYs[i]->Integral(midbin2+1,highbin2));

   }

   
   /*
   TCanvas * c = new TCanvas("positron_id2","positron_id2",900,800);
   c->Divide(2,3);

   c->cd(1);
    

   /// Do calculations for all the energy bins.
   TF1 * f1 =  new TF1("fit_e_plus_minus","[0]*TMath::Gaus(x,[1],[2])+[3]*TMath::Gaus(x,[4],[5])",-0.4, 0.4);
   TF1 * f2 =  new TF1("fit_e_minus","[0]*TMath::Gaus(x,[1],[2])",-0.4, 0.4);
   TF1 * f3 =  new TF1("fit_e_plus","[0]*TMath::Gaus(x,[1],[2])",-0.4, 0.4);
   
   for(int i = 0; i<nbins; i++) {
      hdelYts[i]->SetLineColor(20+2*i);
      hdelYts[i]->SetFillColor(20+2*i);
      hdelYs[i]->SetLineColor(20+2*i);
      hdelYs[i]->SetFillColor(20+2*i);
      //if(i==0)hdelYts[i]->Draw();
      //else hdelYts[i]->Draw("same");
      //hs->Add(hdelYts[i]);
      //hs2->Add(hdelYs[i]);
      Pt[i]     = Pt[i]/float(nBinEvents[i]);
      x[i]      = x[i]/float(nBinEvents[i]);
      Energy[i] = Energy[i]/float(nBinEvents[i]);
      r[i]      = hdelYts[i]->Integral(lowbin,midbin)/hdelYts[i]->Integral(midbin+1,highbin);
      r2[i]     = 1.0/(hdelYs[i]->Integral(lowbin2,midbin2)/hdelYs[i]->Integral(midbin2+1,highbin2));
   }

   for(int i = 0;i<4;i++){
      c->cd(1+i);
      //if(nbins>i) hTrackeryVsdelYtracker[i]->Draw("colz");
      //if(nbins>i) hTrackeryVsdelY[i]->Draw("colz");
      //if(nbins>i) hTrackerxVsdelY[i]->Draw("colz");
      //if(nbins>i) hBigcalXVsY[i]->Draw("colz");
   }

   c->cd(5);
   TMultiGraph * mg = new TMultiGraph();

   TGraph * gRatio = new TGraph(nbins,Pt,r);
   gRatio->SetMarkerStyle(20);
   gRatio->SetMarkerColor(4);
   gRatio->GetXaxis()->SetTitle("Pt");
   mg->Add(gRatio,"lp");

   TGraph * gRatio_2 = new TGraph(nbins,Pt,r2);
   gRatio_2->SetMarkerStyle(20);
   gRatio_2->SetMarkerColor(2);
   mg->Add(gRatio_2,"lp");

   TGraph * gRatio_3 = new TGraph(nbins,Pt,r3);
   gRatio_3->SetMarkerStyle(21);
   gRatio_3->SetMarkerColor(4);
   mg->Add(gRatio_3,"lp");

   mg->SetMaximum(1.5);
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("Pt");

   c->cd(6);
   TMultiGraph * mg2 = new TMultiGraph();

   TGraph * gRatio2 = new TGraph(nbins,x,r);
   gRatio2->SetMarkerStyle(20);
   gRatio2->SetMarkerColor(4);
   mg2->Add(gRatio2,"lp");

   TGraph * gRatio2_2 = new TGraph(nbins,x,r2);
   gRatio2_2->SetMarkerStyle(20);
   gRatio2_2->SetMarkerColor(2);
   mg2->Add(gRatio2_2,"lp");

   TGraph * gRatio2_3 = new TGraph(nbins,x,r3);
   gRatio2_3->SetMarkerStyle(21);
   gRatio2_3->SetMarkerColor(4);
   mg2->Add(gRatio2_3,"lp");

   mg2->SetMaximum(1.0);
   mg2->Draw("a");
   mg2->GetXaxis()->SetTitle("x");

   c->cd(4);
   TMultiGraph * mg3 = new TMultiGraph();

   TGraph * gRatio3 = new TGraph(nbins,Energy,r);
   gRatio3->SetMarkerStyle(20);
   gRatio3->SetMarkerColor(4);
   mg3->Add(gRatio3,"lp");

   TGraph * gRatio3_2 = new TGraph(nbins,Energy,r2);
   gRatio3_2->SetMarkerStyle(20);
   gRatio3_2->SetMarkerColor(2);
   mg3->Add(gRatio3_2,"lp");

   TGraph * gRatio3_3 = new TGraph(nbins,Energy,r3);
   gRatio3_3->SetMarkerStyle(21);
   gRatio3_3->SetMarkerColor(4);
   mg3->Add(gRatio3_3,"lp");

   //f   mg3->SetMaximum(1.5);
   mg3->Draw("a");
   mg3->GetXaxis()->SetTitle("E");


   ofstream myfile;
   myfile.open(Form("plots/%d/positron_id3_Ratio.dat",runNumber));
   myfile << std::setw(10) << "Ebeam"
      << std::setw(10) << "Eprime"
      << std::setw(10) << "PT"
      << std::setw(10) << "x"
      << std::setw(10) << "R_target"
      << std::setw(10) << "R_tracker" 
      << std::endl;

   Double_t Ebeam = rman->GetCurrentRun()->GetBeamEnergy();

   for(int i = 0; i<nbins; i++) {
      myfile << std::setw(10) << Ebeam
         << std::setw(10) << Energy[i]
         << std::setw(10) << Pt[i]
         << std::setw(10) << x[i]
         << std::setw(10) << r[i]
         << std::setw(10) << r2[i] 
         << std::endl;
   }
   myfile.close();


   c->SaveAs(Form("plots/%d/positron_id6.png",runNumber));
   c->SaveAs(Form("plots/%d/positron_id6.pdf",runNumber));
*/
   return(0);
}

