Int_t positron_id5(Int_t runNumber = 72994 ) {


   rman->SetRun(runNumber);
   InSANEDISTrajectory  * fTrajectory = new InSANEDISTrajectory();
   InSANETriggerEvent   * fTriggerEvent = new InSANETriggerEvent();
   BIGCALCluster        * cluster = 0;

   TTree * t = (TTree*)gROOT->FindObject("Tracks2");
   if(!t) return(-1);
   t->SetBranchAddress("trajectory",&fTrajectory);
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   std::cout << t->GetEntries() << " events \n";
   //t->StartViewer();
   //return(0);

   Double_t zmin = -1;
   Double_t zmax =  1;
   //Double_t zmin2 = -0.001;
   //Double_t zmax2 =  0.0005;
   Double_t zmin2 = -2.0;
   Double_t zmax2 =  2.0;
   Double_t min = -0.30;
   Double_t max =  0.30;

   /// Binning in energy
   const int  nEbins = 5;
   Int_t      EbinPlotted = 1; 
   Double_t   Elow  = 500;
   Double_t   Ehigh = 1800;
   Double_t   Edelta = (Ehigh-Elow)/float(nEbins);

   TH1F * hdelYts[nEbins];

   Double_t   Pt[nEbins];
   Double_t   Energy[nEbins];
   Double_t   x[nEbins];
   Int_t      nBinEvents[nEbins];
   Double_t   r[nEbins];
   Double_t   r2[nEbins];
   Double_t   r3[nEbins];
   Double_t   N_3_plus[nEbins];
   Double_t   N_3_minus[nEbins];


   /// Histograms
   TH1F * hdelYs[nEbins];

   TH2F * hTrackeryVsdelY[nEbins];
   TH2F * hTrackerxVsdelY[nEbins];

   TH2F * hTrackeryVsdelYtracker[nEbins];
   TH2F * hTrackerxVsdelYtracker[nEbins];

   TH2F * hBigcalYVsdelY[nEbins];
   TH2F * hBigcalXVsdelY[nEbins];
   TH2F * hBigcalYVsY[nEbins];
   TH2F * hBigcalXVsY[nEbins];

   TH2F * hTrackeryVsdelY2[nEbins];
   TH2F * hTrackeryVsdelY3[nEbins];

   for(int i = 0; i<nEbins; i++) {
      Pt[i] = 0.0;
      x[i] = 0.0;
      Energy[i] = 0.0;
      nBinEvents[i] = 0;
      hdelYts[i]      = new TH1F(Form("hdelYt_%d",i),Form("#Delta Yt %d", int(Elow[i]+i*Edelta) ), 60,-4.5,4.5);
      hdelYs[i]      = new TH1F(Form("hdelY_%d",i),Form("#Delta Y%d",i), 60,-5.0,5.0);
      hTrackeryVsdelY[i] = new TH2F(Form("hTrackeryVsdelY-%d"),Form("Y_{Tracker} Vs #Delta_{Y} %d;#Delta_{Y}",
                                     int(Elow[i]+i*Edelta)),60,-0.4,0.1,60,-20,20);
      hTrackerxVsdelY[i] = new TH2F(Form("hTrackerxVsdelY-%d"),Form("X_{Tracker} Vs #Delta_{Y} %d;#Delta_{Y}",
                                    int(Elow[i]+i*Edelta)),60,-0.4,0.1,60,20,40);

      hBigcalYVsdelY[i] = new TH2F(Form("hBigcalYVsdelY-%d"),Form("Y_{Bigcal} Vs #Delta_{Y} %d;#Delta_{Y}",
                                    int(Elow[i]+i*Edelta)),60,-0.4,0.1,60,-120,120);

      hBigcalXVsdelY[i] = new TH2F(Form("hBigcalXVsdelY-%d"),Form("X_{Bigcal} Vs #Delta_{Y} %d;#Delta_{Y}",
                                    int(Elow[i]+i*Edelta)),60,-0.4,0.1,60,170,270);

      hBigcalYVsY[i] = new TH2F(Form("hBigcalYVsY-%d"),Form("Y_{Bigcal} Vs Y %d;Y",
                                    int(Elow[i]+i*Edelta)),60,-0.5,0.5,60,-120,120);

      hBigcalXVsY[i] = new TH2F(Form("hBigcalXVsY-%d"),Form("X_{Bigcal} Vs #Delta_{Y} %d;#Y",
                                    int(Elow[i]+i*Edelta)),60,-0.5,0.5,60,170,270);


      hTrackeryVsdelYtracker[i] = new TH2F(Form("hTrackeryVsdelYtracker-%d"),Form("Y_{Tracker} Vs #Delta_{YTrac} %d;#Delta_{YTrac}",
                                     int(Elow[i]+i*Edelta)),60,-1.5,1.5,60,-20,20);
      hTrackerxVsdelYtracker[i] = new TH2F(Form("hTrackerxVsdelYtracker-%d"),Form("X_{Tracker} Vs #Delta_{YTrac} %d;#Delta_{YTrac}",
                                    int(Elow[i]+i*Edelta)),60,-1.5,1.5,60,20,45);
      hTrackeryVsdelY2[i] = new TH2F(Form("hTrackeryVsdelY2-%d"),Form("Y_{Tracker} Vs #Delta_{YLine} %d;#Delta_{YLine}", int(Elow[i]+i*Edelta)),60,-1.4,1.4,60,-20,20);
      hTrackeryVsdelY3[i] = new TH2F(Form("hTrackeryVsdelY2-%d"),Form("#Delta_{Y} Vs #Delta_{YLine} %d;#Delta_{YLine}", int(Elow[i]+i*Edelta)),60,-0.4,0.1,60,-1.4,1.4);
  }

   Int_t Nevents = t->GetEntries();
   for(int ievent = 0 ; ievent < Nevents; ievent++) {
      if(ievent%10000 == 0) std::cout << ievent << "\n";
      t->GetEntry(ievent);

      /// Selection 
      if(fTriggerEvent->IsBETAEvent() )
         //if(fTrajectory->fTrackerPosition.Y() > -22 && fTrajectory->fTrackerPosition.Y() < -10 )  
         //if(fTrajectory->fTrackerPosition.Y() > -10 && fTrajectory->fTrackerPosition.Y() <   0 )  
         //if(fTrajectory->fTrackerPosition.Y() >   0 && fTrajectory->fTrackerPosition.Y() <  10 )  
         //if(fTrajectory->fTrackerPosition.Y() >  10 && fTrajectory->fTrackerPosition.Y() <  22 )  
         //if(fTrajectory->fXBigcal_C.X() >190 && fTrajectory->fXBigcal_C.X() < 220 )  
         //if(fTrajectory->fNTrackerNeighbors == 0 ) 
            //if( fTrajectory->fTrackerMiss.Y() < 0 )  
            if(fTrajectory->fReconTarget.Z() > -3 && fTrajectory->fReconTarget.Z() < 3 )  
            {

               if( fTrajectory->fNCherenkovElectrons > 0.3  &&  fTrajectory->fNCherenkovElectrons < 1.5  ) 
                  for(int i = 0; i<nEbins; i++) {
                     if( fTrajectory->fEnergy > Elow + float(i)*Edelta &&
                           fTrajectory->fEnergy < Elow + float(i+1)*Edelta ) 
                     { 

                        hdelYts[i]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y());
                        hdelYs[i]->Fill(fTrajectory->fTrackerMiss.Y());

                        hBigcalYVsdelY[i]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fXBigcal_C.Y());

                        hBigcalXVsdelY[i]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fXBigcal_C.X());

                        hBigcalYVsY[i]->Fill(fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fXBigcal_C.Y());

                        hBigcalXVsY[i]->Fill(fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fXBigcal_C.X());

                        hTrackeryVsdelY[i]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fTrackerPosition.Y());

                        hTrackerxVsdelY[i]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 TMath::Sqrt(fTrajectory->fTrackerPosition.X()*fTrajectory->fTrackerPosition.X() + fTrajectory->fTrackerPosition.Y()*fTrajectory->fTrackerPosition.Y()));

                        hTrackeryVsdelYtracker[i]->Fill(fTrajectory->fTrackerMiss.Y(),
                                                 fTrajectory->fTrackerPosition.Y());

                        hTrackerxVsdelYtracker[i]->Fill(fTrajectory->fTrackerMiss.X(),
                                                 fTrajectory->fTrackerPosition.X());

                        hTrackeryVsdelY2[i]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget.Y(),
                                                  fTrajectory->fTrackerPosition.Y());

                        hTrackeryVsdelY3[i]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y(),
                                                 fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget.Y());

                        Pt[i] += TMath::Sin(fTrajectory->fTheta)*fTrajectory->fEnergy;
                        Energy[i] += fTrajectory->fEnergy;
                        x[i] += fTrajectory->GetBjorkenX();
                        nBinEvents[i]++;
                     }
                  }
            }

   }


   //--------------------------------------------------------------
   //t->StartViewer();


   TCanvas * c = new TCanvas("positron_id2","positron_id2",900,800);
   c->Divide(2,3);

   c->cd(1);
   /*TLegend * leg2 = new TLegend(0.1,0.7,0.48,0.9);
   leg2->AddEntry(hdelZ_1,"high E, 1e ","l");
   leg2->AddEntry(hdelZ_2,"high E, 2e ","l");
   leg2->AddEntry(hdelZ_3,"low E, 1e ","l");
   leg2->AddEntry(hdelZ_4,"low E, 2e ","l");
   leg2->Draw();
   */
    
   Int_t lowbin  = hdelYts[0]->GetXaxis()->FindBin(-0.3);
   Int_t midbin  = hdelYts[0]->GetXaxis()->FindBin(-0.18);
   Int_t highbin = hdelYts[0]->GetXaxis()->FindBin(0.05);

   Int_t lowbin2  = hdelYs[0]->GetXaxis()->FindBin(-1.0);
   Int_t midbin2  = hdelYs[0]->GetXaxis()->FindBin(0.0);
   Int_t highbin2 = hdelYs[0]->GetXaxis()->FindBin(1.0);



   for(int i = 0;i<6;i++){
      c->cd(1+i);
      //if(nEbins>i) hTrackeryVsdelYtracker[i]->Draw("colz");
      if(nEbins>i) hTrackeryVsdelY[i]->Draw("colz");
      //if(nEbins>i) hTrackerxVsdelY[i]->Draw("colz");
      //if(nEbins>i) hBigcalXVsY[i]->Draw("colz");
   }

   c->SaveAs(Form("plots/%d/positron_id5.png",runNumber));
   c->SaveAs(Form("plots/%d/positron_id5.pdf",runNumber));


   TCanvas * c2 = new TCanvas("positron_id5_c2","positron_id5_c2");
   c2->Divide(2,2);


   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   THStack * hs = new THStack("hs","#Delta Y - tracker");
   THStack * hs2 = new THStack("hs2","#Delta Y - target");
   for(int i = 0; i<nEbins;i++){
   //for(int i = nEbins-1;i>=0;i--){
      hdelYs[i]->SetLineColor(4000+i+8);
      hdelYs[i]->SetFillColor(4500+i+8);

      hdelYts[i]->SetLineColor(4000+i+8);
      hdelYts[i]->SetFillColor(4500+i+8);

      leg->AddEntry(hdelYts[i],hdelYts[i]->GetTitle(),"f");

      hs->Add(hdelYs[i]);
      hs2->Add(hdelYts[i]);
   }
   c2->cd(1);
   hs->Draw("nostack");
   c2->cd(2);
   hs2->Draw("nostack");
   leg->Draw();

   c2->cd(3);
   hs->Draw();
   c2->cd(4);
   hs2->Draw();

   c2->SaveAs(Form("plots/%d/positron_id5_c2.png",runNumber));
   c2->SaveAs(Form("plots/%d/positron_id5_c2.pdf",runNumber));


   return(0);





   /// Do calculations for all the energy bins.
   TF1 * f1 =  new TF1("fit_e_plus_minus","[0]*TMath::Gaus(x,[1],[2])+[3]*TMath::Gaus(x,[4],[5])",-0.4, 0.4);
   TF1 * f2 =  new TF1("fit_e_minus","[0]*TMath::Gaus(x,[1],[2])",-0.4, 0.4);
   TF1 * f3 =  new TF1("fit_e_plus","[0]*TMath::Gaus(x,[1],[2])",-0.4, 0.4);
   
   for(int i = 0; i<nEbins; i++) {
      hdelYts[i]->SetLineColor(20+2*i);
      hdelYts[i]->SetFillColor(20+2*i);
      hdelYs[i]->SetLineColor(20+2*i);
      hdelYs[i]->SetFillColor(20+2*i);
      //if(i==0)hdelYts[i]->Draw();
      //else hdelYts[i]->Draw("same");
      hs->Add(hdelYts[i]);
      hs2->Add(hdelYs[i]);
      Pt[i]     = Pt[i]/float(nBinEvents[i]);
      x[i]      = x[i]/float(nBinEvents[i]);
      Energy[i] = Energy[i]/float(nBinEvents[i]);
      r[i]      = hdelYts[i]->Integral(lowbin,midbin)/hdelYts[i]->Integral(midbin+1,highbin);
      r2[i]     = 1.0/(hdelYs[i]->Integral(lowbin2,midbin2)/hdelYs[i]->Integral(midbin2+1,highbin2));
   }
   //hs->Draw("nostack");
   hs->Draw();
   hs->GetXaxis()->SetTitle("Y_{targ}^{rast} - Y_{targ}^{recon}");

   c->cd(2);
   hTrackeryVsdelY[EbinPlotted]->Draw("colz");
   //hs2->Draw();
   //hs2->GetXaxis()->SetTitle("Y_{tracker}^{det} - Y_{tracker}^{prop}");

   c->cd(3);
   for(int i = 0; i<nEbins; i++) {

      f1->SetParameter(0,   60.0);
      f1->SetParLimits(0,    0.0,1.0e5);
      f1->SetParName(0,"A_1");
      f1->SetParameter(1,   -0.15);
      f1->SetParLimits(1,    -0.2,0.0);
      f1->SetParName(1,"#mu_1");
      f1->SetParameter(2,    0.04);
      f1->SetParLimits(2,    0.0,0.20);
      f1->SetParName(2,"#sigma_1");
      f1->SetParameter(3,   10.0);
      f1->SetParLimits(3,    0.0,1.0e5);
      f1->SetParName(3 ,"A_2");
      f1->SetParameter(4,   -0.2);
      f1->SetParLimits(4,    -0.3,0.0);
      f1->SetParName(4 ,"#mu_2");
      f1->SetParameter(5,    0.01);
      f1->SetParLimits(5,    0.0,0.10);
      f1->SetParName(5 ,"#sigma_2");

      hdelYts[i]->Fit(f1,"E","goff");

      /// Electron Gaussian
      f2->SetLineColor(kRed); 
      f2->SetParameter(0,   f1->GetParameter(0) );
      f2->SetParameter(1,   f1->GetParameter(1) );
      f2->SetParameter(2,   f1->GetParameter(2) );

      f2->SetLineColor(kBlue); 
      f3->SetParameter(0,   f1->GetParameter(3) );
      f3->SetParameter(1,   f1->GetParameter(4) );
      f3->SetParameter(2,   f1->GetParameter(5) );

      Double_t total_plus  = f2->Integral(-0.4,0.4);
      Double_t total_minus = f3->Integral(-0.4,0.4);
      r3[i] = total_minus/total_plus;
      std::cout << "Ratio(Pt=" << Pt[i] << ")" << i << " = " << r3[i] << "\n";
      if(EbinPlotted < nEbins ) {
         hdelYts[EbinPlotted]->Draw();
         f1->DrawCopy("same");
         f2->DrawCopy("same");
         f3->DrawCopy("same");

      }
   }
   c->cd(5);
   TMultiGraph * mg = new TMultiGraph();
   
   TGraph * gRatio = new TGraph(nEbins,Pt,r);
   gRatio->SetMarkerStyle(20);
   gRatio->SetMarkerColor(4);
   gRatio->GetXaxis()->SetTitle("Pt");
   mg->Add(gRatio,"lp");
   
   TGraph * gRatio_2 = new TGraph(nEbins,Pt,r2);
   gRatio_2->SetMarkerStyle(20);
   gRatio_2->SetMarkerColor(2);
   mg->Add(gRatio_2,"lp");
   
   TGraph * gRatio_3 = new TGraph(nEbins,Pt,r3);
   gRatio_3->SetMarkerStyle(21);
   gRatio_3->SetMarkerColor(4);
   mg->Add(gRatio_3,"lp");
   
   mg->SetMaximum(1.5);
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("Pt");

   c->cd(6);
   TMultiGraph * mg2 = new TMultiGraph();
   
   TGraph * gRatio2 = new TGraph(nEbins,x,r);
   gRatio2->SetMarkerStyle(20);
   gRatio2->SetMarkerColor(4);
   mg2->Add(gRatio2,"lp");
   
   TGraph * gRatio2_2 = new TGraph(nEbins,x,r2);
   gRatio2_2->SetMarkerStyle(20);
   gRatio2_2->SetMarkerColor(2);
   mg2->Add(gRatio2_2,"lp");

   TGraph * gRatio2_3 = new TGraph(nEbins,x,r3);
   gRatio2_3->SetMarkerStyle(21);
   gRatio2_3->SetMarkerColor(4);
   mg2->Add(gRatio2_3,"lp");
  
   mg2->SetMaximum(1.0);
   mg2->Draw("a");
   mg2->GetXaxis()->SetTitle("x");

   c->cd(4);
   TMultiGraph * mg3 = new TMultiGraph();

   TGraph * gRatio3 = new TGraph(nEbins,Energy,r);
   gRatio3->SetMarkerStyle(20);
   gRatio3->SetMarkerColor(4);
   mg3->Add(gRatio3,"lp");

   TGraph * gRatio3_2 = new TGraph(nEbins,Energy,r2);
   gRatio3_2->SetMarkerStyle(20);
   gRatio3_2->SetMarkerColor(2);
   mg3->Add(gRatio3_2,"lp");

   TGraph * gRatio3_3 = new TGraph(nEbins,Energy,r3);
   gRatio3_3->SetMarkerStyle(21);
   gRatio3_3->SetMarkerColor(4);
   mg3->Add(gRatio3_3,"lp");

//f   mg3->SetMaximum(1.5);
   mg3->Draw("a");
   mg3->GetXaxis()->SetTitle("E");


   ofstream myfile;
   myfile.open(Form("plots/%d/positron_id3_Ratio.dat",runNumber));
   myfile << std::setw(10) << "Ebeam"
          << std::setw(10) << "Eprime"
          << std::setw(10) << "PT"
          << std::setw(10) << "x"
          << std::setw(10) << "R_target"
          << std::setw(10) << "R_tracker" 
          << std::endl;

   Double_t Ebeam = rman->GetCurrentRun()->GetBeamEnergy();

   for(int i = 0; i<nEbins; i++) {
   myfile << std::setw(10) << Ebeam
          << std::setw(10) << Energy[i]
          << std::setw(10) << Pt[i]
          << std::setw(10) << x[i]
          << std::setw(10) << r[i]
          << std::setw(10) << r2[i] 
          << std::endl;
   }
   myfile.close();

   c->SaveAs(Form("plots/%d/positron_id5.png",runNumber));
   c->SaveAs(Form("plots/%d/positron_id5.pdf",runNumber));

   return(0);
}

