Int_t track_efficiencies5(Int_t runGroup = 1, bool newmerge = false) {

   if (gROOT->LoadMacro("util/merge_run_list.cxx") != 0) {
      Error(weh, "Failed loading util/merge_run_list.cxx in compiled mode.");
      return -1;
   }

   const char * runlist = "lists/para/test_para.txt";
   const char * outfile = Form("data/Tracks/ParaTracks%d.root",runGroup);;

   if( newmerge )
      if( merge_run_list(runlist,outfile,"Tracks") ) { return(-1);}

   TFile * f = TFile::Open(outfile,"UPDATE");
   f->cd();
   TTree * t = (TTree*)gROOT->FindObject("Tracks");
   if(!t) return(-2);

   TH1F * h1 = 0;
   TH2F * h2 = 0;

   Double_t theta_range[2] = {0.4*180.0/TMath::Pi(),1.0*180.0/TMath::Pi()};
   Double_t phi_range[2] = {-0.6*180.0/TMath::Pi(),1.0*180.0/TMath::Pi()};
   Double_t energy_range[2] = {500,4500};

   TH2F * hPhiVsLucMiss = new TH2F("hPhiVsLucMiss","Phi vs LucMiss;#delta Lucite;#phi",
                                  100,0,50,100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTrackerMiss = new TH2F("hPhiVsTrackerMiss","Phi vs LucMiss;#delta Lucite;#phi",
                                  100,-15,15,100,phi_range[0],phi_range[1]);
   TH2F * hTrackerMissVsPhi = new TH2F("hTrackerMissVsPhi","Tracker miss vs #phi;#delta tracker;#phi",
                                  100,phi_range[0],phi_range[1],100,-15,15);
   TH2F * hTrackerMissVsEnergy = new TH2F("hTrackerMissVsEnergy","Tracker miss vs #phi;#delta tracker;#phi",
                                  20,energy_range[0],energy_range[1],100,-5,5);
   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta3 = new TH2F("hPhiVsTheta3","Phi vs Theta with tracker cuts;#theta;#phi",
                                  100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);


   TH1F * hZRecon    = new TH1F("hZRecon","Z reconstructed",100,-10,10);
   TH1F * hZRecon1    = new TH1F("hZRecon1","Z reconstructed",100,-10,10);
   TH1F * hZRecon2    = new TH1F("hZRecon2","Z reconstructed",100,-10,10);

   TH1F * hYRecon    = new TH1F("hYRecon","Z reconstructed",100,-10,10);
   TH1F * hYRecon1    = new TH1F("hYRecon1","Z reconstructed",100,-10,10);
   TH1F * hYRecon2    = new TH1F("hYRecon2","Z reconstructed",100,-10,10);

   TH1F * hCerTDC    = new TH1F("hCerTDC","Cer TDC",100,-50,50);
   TH1F * hCerTDC1    = new TH1F("hCerTDC1","Cer TDC",100,-50,50);
   TH1F * hCerTDC2    = new TH1F("hCerTDC2","Cer TDC",100,-50,50);

   TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy = new TEfficiency("effEnergy","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy = new TEfficiency("effBCy","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx = new TEfficiency("effBCx","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effPhi1   = new TEfficiency("effPhi1","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta1 = new TEfficiency("effTheta1","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy1 = new TEfficiency("effEnergy1","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy1 = new TEfficiency("effBCy1","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx1 = new TEfficiency("effBCx1","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);


   TEfficiency* effPhi2   = new TEfficiency("effPhi2","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta2 = new TEfficiency("effTheta2","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy2 = new TEfficiency("effEnergy2","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy2 = new TEfficiency("effBCy2","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx2 = new TEfficiency("effBCx2","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);


   TEfficiency* effPhi4   = new TEfficiency("effPhi4","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta4 = new TEfficiency("effTheta4","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy4 = new TEfficiency("effEnergy4","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy4 = new TEfficiency("effBCy4","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx4 = new TEfficiency("effBCx4","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effPhi42   = new TEfficiency("effPhi42","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta42 = new TEfficiency("effTheta42","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy42 = new TEfficiency("effEnergy42","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy42 = new TEfficiency("effBCy42","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx42 = new TEfficiency("effBCx42","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effPhi38   = new TEfficiency("effPhi38","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta38 = new TEfficiency("effTheta38","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy38 = new TEfficiency("effEnergy38","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy38 = new TEfficiency("effBCy38","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx38 = new TEfficiency("effBCx38","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effPhi30   = new TEfficiency("effPhi30","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1]);
   TEfficiency* effTheta30 = new TEfficiency("effTheta30","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1]);
   TEfficiency* effEnergy30 = new TEfficiency("effEnergy30","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);
   TEfficiency* effBCy30 = new TEfficiency("effBCy30","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx30 = new TEfficiency("effBCx30","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);

   TEfficiency* effBCyVsEnergy = new TEfficiency("effBCyVsEnergy","Cut Efficiency, Energy Vs Y_{bigcal} ;Y_{bigcal};E",
                                           120,-120,120, 20,500,2500);
   TEfficiency* effBCy7 = new TEfficiency("effBCy","Cut Efficiency vs Y_{bigcal} ;Y_{bigcal};#epsilon",
                                           20,-120,120);
   TEfficiency* effBCx7 = new TEfficiency("effBCx","Cut Efficiency vs X_{bigcal} ;X_{bigcal};#epsilon",
                                           20,160,260);
   TEfficiency* effPhi7   = new TEfficiency("effPhi7","Cut Efficiency vs #phi;#phi;#epsilon",
                                           20,phi_range[0],phi_range[1],20,energy_range[0],energy_range[1]);
   TEfficiency* effTheta7 = new TEfficiency("effTheta7","Cut Efficiency vs #theta;#theta;#epsilon",
                                           20,theta_range[0],theta_range[1],20,energy_range[0],energy_range[1]);
   TEfficiency* effEnergy7 = new TEfficiency("effEnergy7","Cut Efficiency vs E;E;#epsilon",
                                           20,energy_range[0],energy_range[1]);

   TEfficiency* effBCyVsBCx = new TEfficiency("effBCyVsBCx","Cut Efficiency, Y_{bigcal} Vs X_{bigcal} ;X_{bigcal};Y_{bigcal}",
                                           60,160,260, 60,-120,120);
   TEfficiency* effTrackerYVsTrackerX = new TEfficiency("effTrackerYVsTrackerX","Cut Efficiency, Y_{tracker} Vs X_{tracker} ;X_{tracker};Y_{tracker}",
                                           40,24,42,50,-20,20);
   TEfficiency* effPhiVsTheta = new TEfficiency("effPhiVsTheta","Cut Efficiency, #phi Vs #theta ;#theta;#phi",
                                           50,theta_range[0],theta_range[1],50,phi_range[0],phi_range[1]);

   TEfficiency* effBCyVsEnergy2 = new TEfficiency("effBCyVsEnergy2","Cut Efficiency, Energy Vs Y_{bigcal} ;Y_{bigcal};E",
                                           20,500,4000,60,-120,120);

   TEfficiency* effBCxVsEnergy2 = new TEfficiency("effBCxVsEnergy2","Cut Efficiency, Energy Vs X_{bigcal} ;X_{bigcal};E",
                                           20,500,4000,60,160,260);


   InSANEDISTrajectory * fTrajectory = new InSANEDISTrajectory();
   t->SetBranchAddress("trajectory",&fTrajectory);
   InSANETriggerEvent  * fTriggerEvent = new InSANETriggerEvent();
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   /// Create the Asymmetries for each Q^2 bin.
   Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t CerADCCut[3] = {0.5,1.5,2.5};

   Int_t fEntries =  t->GetEntries();
   /// Event Loop
   for(Int_t ievent = 0; ievent < fEntries; ievent++) {

      if(ievent%10000 == 0) std::cout << ievent << "/" << fEntries << "\n";
      t->GetEntry(ievent);

      bool passedZCut = false;
      bool passedZ1Cut = false;
      bool passedZ2Cut = false;
      bool passedStd = false;
      bool passedCerWindow = false;
      bool passedCerTDC = false;
      bool passedOldTrackerCut = false;

      bool passed = false;
      bool passed2 = false;
      bool passed3 = false;
      bool passed4 = false;
      bool passed5 = false;
      bool passed7 = false;
      bool passed8 = false;
      bool passed9 = false;
      bool passed42 = false;
      bool passed30 = false;

      //hPhiVsTheta1->Fill(fTrajectory->fTheta*180.0/TMath::Pi(),fTrajectory->fPhi*180.0/TMath::Pi());
      //hPhiVsLucMiss->Fill(fTrajectory->fLuciteMissDistance,fTrajectory->fPhi*180.0/TMath::Pi());

      /// Standard Cuts.
      if(!fTrajectory->fIsNoisyChannel)  if(fTrajectory->fIsGood) if(fTriggerEvent->IsBETA2Event() ) {

         passedStd = true;

         hPhiVsLucMiss->Fill(fTrajectory->fLuciteMissDistance,fTrajectory->fPhi*180.0/TMath::Pi());
         hPhiVsTrackerMiss->Fill(fTrajectory->fTrackerMiss.Y(),fTrajectory->fPhi*180.0/TMath::Pi());
         hTrackerMissVsPhi->Fill(fTrajectory->fPhi*180.0/TMath::Pi(),fTrajectory->fTrackerMiss.Y());
         hTrackerMissVsEnergy->Fill(fTrajectory->fEnergy,fTrajectory->fTrackerMiss.Y());

         if(fTrajectory->fNCherenkovElectrons > 0.4 && fTrajectory->fNCherenkovElectrons < 1.6){
           passed42 = true;
           passedCerWindow = true;
         }

         if( fTrajectory->fEnergy > 600.0 ) {
           passed1 = true;
         }

         if( TMath::Abs(fTrajectory->fTrackerMiss.Y()) < 2.0 ) passedOldTrackerCut = true;

         if( TMath::Abs(fTrajectory->fReconTargetY1.Z()) < 4.0 ) passedZ1Cut = true;

         if( TMath::Abs(fTrajectory->fReconTargetY2.Z()) < 4.0 ) passedZ2Cut = true;

         if( TMath::Abs(fTrajectory->fReconTarget.Z()) < 4.0 ) passedZCut = true;

         if(TMath::Abs(fTrajectory->fCherenkovTDC) < 20) {
            passed9 = true;
            passedCerTDC = true;
         }

         if( passedCerTDC && passedCerWindow ) {

         effPhi->Fill( passedZCut && passedCerTDC && passedCerWindow  ,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta->Fill( passedZCut && passedCerTDC && passedCerWindow ,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy->Fill( passedZCut && passedCerTDC && passedCerWindow ,fTrajectory->fEnergy);
         effBCy->Fill( passedZCut && passedCerTDC && passedCerWindow ,fTrajectory->fPosition0.Y());
         effBCx->Fill( passedZCut && passedCerTDC && passedCerWindow ,fTrajectory->fPosition0.X());

         effPhi1->Fill( passedZ1Cut && passedCerTDC && passedCerWindow  ,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta1->Fill( passedZ1Cut && passedCerTDC && passedCerWindow ,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy1->Fill( passedZ1Cut && passedCerTDC && passedCerWindow ,fTrajectory->fEnergy);
         effBCy1->Fill( passedZ1Cut && passedCerTDC && passedCerWindow ,fTrajectory->fPosition0.Y());
         effBCx1->Fill( passedZ1Cut && passedCerTDC && passedCerWindow ,fTrajectory->fPosition0.X());

         effPhi2->Fill( passedZ2Cut && passedCerTDC && passedCerWindow  ,fTrajectory->fPhi*180.0/TMath::Pi());
         effTheta2->Fill( passedZ2Cut && passedCerTDC && passedCerWindow ,fTrajectory->fTheta*180.0/TMath::Pi());
         effEnergy2->Fill( passedZ2Cut && passedCerTDC && passedCerWindow ,fTrajectory->fEnergy);
         effBCy2->Fill( passedZ2Cut && passedCerTDC && passedCerWindow ,fTrajectory->fPosition0.Y());
         effBCx2->Fill( passedZ2Cut && passedCerTDC && passedCerWindow ,fTrajectory->fPosition0.X());
         }
         
         hZRecon->Fill(fTrajectory->fReconTarget.Z());
         hZRecon1->Fill(fTrajectory->fReconTargetY1.Z());
         hZRecon2->Fill(fTrajectory->fReconTargetY2.Z());
         
         hYRecon->Fill(fTrajectory->fReconTarget.Y());
         hYRecon1->Fill(fTrajectory->fReconTargetY1.Y());
         hYRecon2->Fill(fTrajectory->fReconTargetY2.Y());

         
         if( passedZCut && passedCerWindow) hCerTDC->Fill(fTrajectory->fCherenkovTDC);
         if( passedZ1Cut && passedCerWindow) hCerTDC1->Fill(fTrajectory->fCherenkovTDC);
         if( passedZ2Cut && passedCerWindow) hCerTDC2->Fill(fTrajectory->fCherenkovTDC);

      } // end bad channel cuts

   } // end event loop



   TCanvas * c = new TCanvas("track_efficiencies5","track_efficiencies5");
   c->Divide(3,3);

   TLegend * leg = new TLegend(0.76,0.15,0.999,0.999);
   leg->SetHeader("Efficiencies");
   //leg->SetNColumns(2);

   c->cd(1);
   TMultiGraph *mg = new TMultiGraph();
   effPhi->Paint();
   if(effPhi->GetPaintedGraph() ) mg->Add(effPhi->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi->GetPaintedGraph(),"N_{#check{C}+Zrecon}/N_{#checkC}","lep");

   effPhi1->SetLineColor(2);
   effPhi1->Paint();
   if(effPhi1->GetPaintedGraph() ) mg->Add(effPhi1->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi1->GetPaintedGraph(),"N_{#check{C}+Zrecon}/N_{#checkC}","lep");

   effPhi2->SetLineColor(4);
   effPhi2->Paint();
   if(effPhi2->GetPaintedGraph() ) mg->Add(effPhi2->GetPaintedGraph(),"P");
   leg->AddEntry(effPhi2->GetPaintedGraph(),"N_{#check{C}+Zrecon}/N_{#checkC}","lep");

   mg->Draw("A");
   mg->GetXaxis()->SetTitle("#phi");
   mg->GetYaxis()->SetTitle("#epsilon");
   mg->Draw("A");


   c->cd(2);
   TMultiGraph *mg2 = new TMultiGraph();

   effTheta->Paint();
   if(effTheta->GetPaintedGraph() ) mg2->Add(effTheta->GetPaintedGraph(),"P");

   effTheta1->SetLineColor(2);
   effTheta1->Paint();
   if(effTheta1->GetPaintedGraph() ) mg2->Add(effTheta1->GetPaintedGraph(),"P");

   effTheta2->SetLineColor(4);
   effTheta2->Paint();
   if(effTheta2->GetPaintedGraph() ) mg2->Add(effTheta2->GetPaintedGraph(),"P");

   mg2->Draw("A");
   mg2->GetXaxis()->SetTitle("#theta");
   mg2->GetYaxis()->SetTitle("#epsilon");
   mg2->Draw("A");

   c->cd(3);
   TMultiGraph *mg3 = new TMultiGraph();

   effEnergy->Paint();
   if(effEnergy->GetPaintedGraph() ) mg3->Add(effEnergy->GetPaintedGraph(),"P");

   effEnergy1->SetLineColor(2);
   effEnergy1->Paint();
   if(effEnergy1->GetPaintedGraph() ) mg3->Add(effEnergy1->GetPaintedGraph(),"P");

   effEnergy2->SetLineColor(4);
   effEnergy2->Paint();
   if(effEnergy2->GetPaintedGraph() ) mg3->Add(effEnergy2->GetPaintedGraph(),"P");

   mg3->Draw("A");
   mg3->GetXaxis()->SetTitle("E'");
   mg3->GetYaxis()->SetTitle("#epsilon");
   mg3->Draw("A");
   leg->Draw();

   c->cd(4);

   TMultiGraph *mg7 = new TMultiGraph();

   effBCy->SetLineColor(1);
   effBCy->Paint();
   if(effBCy->GetPaintedGraph() ) mg7->Add(effBCy->GetPaintedGraph(),"PC");

   effBCy1->SetLineColor(2);
   effBCy1->Paint();
   if(effBCy1->GetPaintedGraph() ) mg7->Add(effBCy1->GetPaintedGraph(),"PC");

   effBCy2->SetLineColor(4);
   effBCy2->Paint();
   if(effBCy2->GetPaintedGraph() ) mg7->Add(effBCy2->GetPaintedGraph(),"PC");

   mg7->Draw("A");
   mg7->GetXaxis()->SetTitle("y_{bigcal}");
   mg7->GetYaxis()->SetTitle("#epsilon");
   mg7->Draw("A");

   c->cd(5);
   TMultiGraph *mg8 = new TMultiGraph();

   effBCx->SetLineColor(1);
   effBCx->Paint();
   if(effBCx->GetPaintedGraph() ) mg8->Add(effBCx->GetPaintedGraph(),"PC");

   effBCx1->SetLineColor(2);
   effBCx1->Paint();
   if(effBCx1->GetPaintedGraph() ) mg8->Add(effBCx1->GetPaintedGraph(),"PC");

   effBCx2->SetLineColor(4);
   effBCx2->Paint();
   if(effBCx2->GetPaintedGraph() ) mg8->Add(effBCx2->GetPaintedGraph(),"PC");

   mg8->Draw("A");
   mg8->GetXaxis()->SetTitle("x_{bigcal}");
   mg8->GetYaxis()->SetTitle("#epsilon");
   mg8->Draw("A");


   c->cd(6);
   hZRecon->Draw();
   hZRecon1->SetLineColor(2);
   hZRecon1->Draw("same");
   hZRecon2->SetLineColor(4);
   hZRecon2->Draw("same");

   c->cd(7);
   hCerTDC->Draw();
   hCerTDC1->SetLineColor(2);
   hCerTDC1->Draw("same");
   hCerTDC2->SetLineColor(4);
   hCerTDC2->Draw("same");

   c->cd(9);
   hYRecon->Draw();
   hYRecon1->SetLineColor(2);
   hYRecon1->Draw("same");
   hYRecon2->SetLineColor(4);
   hYRecon2->Draw("same");


   c->SaveAs(Form("plots/para/track_efficiencies5-%d.png",runGroup));

//   t->StartViewer();
//    new TBrowser;
   return(0);

}
