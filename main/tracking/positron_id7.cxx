Int_t positron_id7(Int_t runNumber = 72999 ) {

   rman->SetRun(runNumber);

   InSANEDISTrajectory  * fTrajectory = new InSANEDISTrajectory();
   InSANETriggerEvent   * fTriggerEvent = new InSANETriggerEvent();
   BIGCALCluster        * cluster = 0;
   TTree * t = (TTree*)gROOT->FindObject("Tracks2");
   if(!t) return(-1);
   t->SetBranchAddress("trajectory",&fTrajectory);
   t->SetBranchAddress("triggerEvent",&fTriggerEvent);

   std::cout << t->GetEntries() << " events \n";

   Double_t zmin = -1;
   Double_t zmax =  1;
   //Double_t zmin2 = -0.001;
   //Double_t zmax2 =  0.0005;
   Double_t zmin2 = -2.0;
   Double_t zmax2 =  2.0;
   Double_t min = -0.30;
   Double_t max =  0.30;


   const int nEbins = 20;
   Double_t Pt[nEbins];
   Double_t Energy[nEbins];
   Double_t x[nEbins];
   Int_t nBinEvents[nEbins];

   Double_t Elow  = 600;
   Double_t Ehigh = 2000;
   Double_t Edelta = (Ehigh-Elow)/float(nEbins);

   TH1F * hdelYts[nEbins];
   TH1F * hdelYs[nEbins];

   for(int i = 0; i<nEbins; i++) {
      Pt[i] = 0.0;
      x[i] = 0.0;
      Energy[i] = 0.0;
      nBinEvents[i] = 0;
      hdelYts[i]      = new TH1F(Form("hdelYt_%d",i),Form("#Delta Yt%d",i), 100,-0.4,0.1);
      hdelYs[i]      = new TH1F(Form("hdelY_%d",i),Form("#Delta Y%d",i), 50,-1.0,1.0);
   }

   Int_t Nevents = t->GetEntries();
   for(int ievent = 0 ; ievent < Nevents; ievent++) {
      if(ievent%10000 == 0) std::cout << ievent << "\n";
      t->GetEntry(ievent);
      if(fTriggerEvent->IsBETAEvent() )
      //if(fTrajectory->fTrackerPosition.Y() > -22 && fTrajectory->fTrackerPosition.Y() < 22 )  
      if(fTrajectory->fTrackerPosition.Y() > -20 && fTrajectory->fTrackerPosition.Y() < -10 )  
      //if(fTrajectory->fTrackerPosition.Y() > -9 && fTrajectory->fTrackerPosition.Y() < -1 )  
      //if(fTrajectory->fTrackerPosition.Y() > 1 && fTrajectory->fTrackerPosition.Y() < 9 )  
      //if(fTrajectory->fTrackerPosition.Y() > 1 && fTrajectory->fTrackerPosition.Y() < 9 )  
      //if(fTrajectory->fReconTarget.Z() > -0.5 && fTrajectory->fReconTarget.Z() < 0.8 )  
      {

      if( fTrajectory->fNCherenkovElectrons > 0.5  &&  fTrajectory->fNCherenkovElectrons < 1.5  )
      //if( fTrajectory->fTrackerMiss.Y() > 0  )   
      for(int i = 0; i<nEbins; i++) {
         if( fTrajectory->fEnergy > Elow + float(i)*Edelta &&
             fTrajectory->fEnergy < Elow + float(i+1)*Edelta ) 
         { 
            hdelYts[i]->Fill(fTrajectory->fTargetPosition.Y() - fTrajectory->fReconTarget1.Y());
            hdelYs[i]->Fill(fTrajectory->fTrackerMiss.Y());
            Pt[i] += TMath::Sin(fTrajectory->fTheta)*fTrajectory->fEnergy;
            Energy[i] += fTrajectory->fEnergy;
            x[i] += fTrajectory->GetBjorkenX();
            nBinEvents[i]++;
         }
      }
      }

   }

   TCanvas * c = new TCanvas("positron_id2","positron_id2",900,800);
   c->Divide(2,3);

   c->cd(1);
   /*TLegend * leg2 = new TLegend(0.1,0.7,0.48,0.9);
   leg2->AddEntry(hdelZ_1,"high E, 1e ","l");
   leg2->AddEntry(hdelZ_2,"high E, 2e ","l");
   leg2->AddEntry(hdelZ_3,"low E, 1e ","l");
   leg2->AddEntry(hdelZ_4,"low E, 2e ","l");
   leg2->Draw();
   */
    
   Int_t lowbin  = hdelYts[0]->GetXaxis()->FindBin(-0.3);
   Int_t midbin  = hdelYts[0]->GetXaxis()->FindBin(-0.13);
   Int_t highbin = hdelYts[0]->GetXaxis()->FindBin(0.1);

   Int_t lowbin2  = hdelYs[0]->GetXaxis()->FindBin(-1.0);
   Int_t midbin2  = hdelYs[0]->GetXaxis()->FindBin(0.20);
   Int_t highbin2 = hdelYs[0]->GetXaxis()->FindBin(1.0);

   Double_t r[nEbins];
   Double_t r2[nEbins];
   c->cd(1);
   THStack * hs = new THStack("hs","test stacked histograms");
   THStack * hs2 = new THStack("hs2","test stacked histograms");
   for(int i = 0; i<nEbins; i++) {
      hdelYts[i]->SetLineColor(20+2*i);
      hdelYts[i]->SetFillColor(20+2*i);
      hdelYs[i]->SetLineColor(20+2*i);
      hdelYs[i]->SetFillColor(20+2*i);
      //if(i==0)hdelYts[i]->Draw();
      //else hdelYts[i]->Draw("same");
      hs->Add(hdelYts[i]);
      hs2->Add(hdelYs[i]);
      Pt[i] = Pt[i]/float(nBinEvents[i]);
      x[i] = x[i]/float(nBinEvents[i]);
      Energy[i] = Energy[i]/float(nBinEvents[i]);
      r[i] = hdelYts[i]->Integral(lowbin,midbin)/hdelYts[i]->Integral(midbin+1,highbin);
      r2[i] = 1.0/(hdelYs[i]->Integral(lowbin2,midbin2)/hdelYs[i]->Integral(midbin2+1,highbin2));
      std::cout << "Ratio(Pt=" << Pt[i] << ")" << i << " = " << r2[i] << "\n";
   }
   //hs->Draw("nostack");
   hs->Draw();
   hs->GetXaxis()->SetTitle("Y_{targ}^{rast} - Y_{targ}^{recon}");

   c->cd(2);
   hs2->Draw();
   hs2->GetXaxis()->SetTitle("Y_{tracker}^{det} - Y_{tracker}^{prop}");

   c->cd(5);
   TMultiGraph * mg = new TMultiGraph();
   TGraph * gRatio = new TGraph(nEbins,Pt,r);
   gRatio->SetMarkerStyle(20);
   gRatio->SetMarkerColor(4);
   gRatio->GetXaxis()->SetTitle("Pt");
   //gRatio->Draw("alp");
   mg->Add(gRatio,"lp");
   TGraph * gRatio_2 = new TGraph(nEbins,Pt,r2);
   gRatio_2->SetMarkerStyle(20);
   gRatio_2->SetMarkerColor(2);
   mg->Add(gRatio_2,"lp");
   //gRatio_2->Draw("lp");
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("Pt");

   c->cd(3);
   TMultiGraph * mg2 = new TMultiGraph();
   TGraph * gRatio2 = new TGraph(nEbins,x,r);
   gRatio2->SetMarkerStyle(20);
   gRatio2->SetMarkerColor(4);
   mg2->Add(gRatio2,"lp");
   TGraph * gRatio2_2 = new TGraph(nEbins,x,r2);
   gRatio2_2->SetMarkerStyle(20);
   gRatio2_2->SetMarkerColor(2);
   gRatio2_2->GetXaxis()->SetTitle("x");
   mg2->Add(gRatio2_2,"lp");
   mg2->Draw("a");
   mg2->GetXaxis()->SetTitle("x");

   c->cd(4);
   TMultiGraph * mg3 = new TMultiGraph();
   TGraph * gRatio3 = new TGraph(nEbins,Energy,r);
   gRatio3->SetMarkerStyle(20);
   gRatio3->SetMarkerColor(4);
   mg3->Add(gRatio3,"lp");
   TGraph * gRatio3_2 = new TGraph(nEbins,Energy,r2);
   gRatio3_2->SetMarkerStyle(20);
   gRatio3_2->SetMarkerColor(2);
   gRatio3_2->GetXaxis()->SetTitle("E");
   mg3->Add(gRatio3_2,"lp");
   mg3->Draw("a");
   mg3->GetXaxis()->SetTitle("E");


   ofstream myfile;
   myfile.open(Form("plots/%d/positron_id2_Ratio.dat",runNumber));
   myfile << std::setw(10) << "Ebeam"
          << std::setw(10) << "Eprime"
          << std::setw(10) << "PT"
          << std::setw(10) << "x"
          << std::setw(10) << "R_target"
          << std::setw(10) << "R_tracker" 
          << std::endl;

   Double_t Ebeam = rman->GetCurrentRun()->GetBeamEnergy();

   for(int i = 0; i<nEbins; i++) {
   myfile << std::setw(10) << Ebeam
          << std::setw(10) << Energy[i]
          << std::setw(10) << Pt[i]
          << std::setw(10) << x[i]
          << std::setw(10) << r[i]
          << std::setw(10) << r2[i] 
          << std::endl;
   }
   myfile.close();

   c->SaveAs(Form("plots/%d/positron_id7.png",runNumber));
   c->SaveAs(Form("plots/%d/positron_id7.pdf",runNumber));

   return(0);
}

