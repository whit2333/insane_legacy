/*! Script used to compare to the results from table III from mo and tsai. 
 */
Int_t sigma_delta_compare(const char * expname  = "mo_and_tsai"){

   /// Ep values from table IV of Mo and Tsai for Es=5GeV
   std::vector<Double_t> Ep_values;
   std::vector<Double_t> sig_values;
   Ep_values.push_back(4.56);
   sig_values.push_back(8.59*1000.0);
   Ep_values.push_back(4.5);
   sig_values.push_back(8.246*1000.0);
   Ep_values.push_back(4.0);
   sig_values.push_back(0.8624*1000.0);
   Ep_values.push_back(2.5);
   sig_values.push_back(0.2229*1000.0);
   Ep_values.push_back(1.0);
   sig_values.push_back(0.1182*1000.0);

   Double_t Ebeam      = 5.0;
   Double_t theta      = 5.0*degree;
   Int_t TargetType    = 0;
   Int_t Z             = 1;
   Int_t A             = 1;
   Double_t phi        = 0*degree;
   Int_t    npar       = 2;
   Double_t Emin       = 0.9;
   Double_t Emax       = 4.6;
   Double_t minTheta   = 10*degree;
   Double_t maxTheta   = 50.0*degree;

   /// DIS Born cross section 
   InSANEPOLRADElasticTailDiffXSec * fDiffXSec = new  InSANEPOLRADElasticTailDiffXSec();
   //InSANEPOLRADInelasticTailDiffXSec * fDiffXSec = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec->SetBeamEnergy(Ebeam);
   fDiffXSec->SetA(A);
   fDiffXSec->SetZ(Z);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->GetPOLRAD()->SetHelicity(0);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace * ps = fDiffXSec->GetPhaseSpace();
   fDiffXSec->GetPhaseSpace()->GetVariableWithName("energy_e")->SetMaximum(20.0);
   TF1 * fSigma = new TF1(Form("fSigma%d",0), fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   fSigma->SetParameter(1,phi);
   fSigma->SetParameter(0,theta);
   fSigma->SetNpx(50);

   /// Create the canvas and plot 
   TCanvas * c = new TCanvas("compare","compare");
   c->SetLogy(true);
   fSigma->Draw();
   fSigma->SetTitle("#frac{d#sigma}{d#Omega dE_{p}}");
   fSigma->GetYaxis()->SetTitle("#frac{d#sigma}{d#Omega dE_{p}} [10^{-31}cm^{2}/sr]");
   fSigma->GetXaxis()->SetTitle("E_{p}");

   TGraph * g1 = new TGraph(sig_values.size(),&Ep_values[0],&sig_values[0]);
   g1->SetMarkerStyle(20);
   g1->SetMarkerColor(kBlack);
   g1->Draw("p,same");

   Double_t * energy_e =  ps->GetVariableWithName("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e =  ps->GetVariableWithName("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e =  ps->GetVariableWithName("phi_e")->GetCurrentValueAddress();
   for(int i = 0; i<Ep_values.size(); i++){
      (*energy_e) = Ep_values[i];
      (*theta_e)  = theta;
      (*phi_e)    = phi;
      fDiffXSec->EvaluateCurrent();
      fDiffXSec->PrintTable();

   }

   //c->SaveAs(Form("results/sigma_delta_compare_%s.png",expname));

   return(0);
}
