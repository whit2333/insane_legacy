Int_t compare_IRT(Double_t theta_target = 0*degree,Int_t aNumber = 9){
    gStyle->SetLineStyleString(14,"20 20");
    gStyle->SetLineStyleString(13,"25 25");
    gStyle->SetLineStyleString(12,"30 30");
    gStyle->SetLineStyleString(11,"40 50");

   using namespace std;
   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctionsFromPDFs * pdfs= dynamic_cast<InSANEStructureFunctionsFromPDFs*>(fman->CreateSFs(6));
   fman->CreatePolSFs(6);
   pdfs->SetUseR(false);
   pdfs->CalculateTMCs(false);

   Double_t beamEnergy = 5.89; //GeV
   Double_t Eprime     = 0.1; 
   Double_t theta      = 40.0*degree;
   Double_t phi        = 0.0*degree;  

   Int_t    npx      = 100;
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 2.3;

   Double_t helicity = -1.0;
   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   TLegend * leg = new TLegend(0.5,0.5,0.87,0.87);
   leg->SetFillColor(0); 
   leg->SetFillStyle(0); 
   //leg->SetHeader("Inelastic radative tails");

   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);

   // ------------------------------------------------
   //  POLRAD
   TString inpath = "polrad/data/stat_xs_polrad_callan-gross.dat"; 
   ifstream infile(inpath);
   Double_t c0,c1,c2;
   vector<Double_t> polradEprime;
   vector<Double_t> polradBorn;
   vector<Double_t> polradIRT;
   Double_t jacob = 1.0;
   if(infile.fail()){
      std::cout << "Cannot open the file: " << inpath << std::endl;
      exit(1);
   }else{
      std::cout << "Opening the file: " << inpath << std::endl;
      while(!infile.eof()){
         infile >> c0 >> c1 >> c2;
         if(infile.eof()) break;
         //jacob = (c0/(2.0*TMath::Pi()*(M_p/GeV)*(beamEnergy-c0)));
         //jacob = 1.0/TMath::Sin(theta);
         polradEprime.push_back(c0);
         polradBorn.push_back(c1*jacob);
         polradIRT.push_back(c2*jacob);
      }
      infile.close();
   }

   TGraph * polradBornGr = new TGraph(polradBorn.size(),&polradEprime[0],&polradBorn[0]);
   polradBornGr->SetMarkerStyle(20);
   polradBornGr->SetMarkerColor(kGreen+1);
   polradBornGr->SetLineColor(kGreen+1);
   polradBornGr->SetLineWidth(2);
   polradBornGr->SetLineStyle(11);

   TGraph * polradIRTGr = new TGraph(polradBorn.size(),&polradEprime[0],&polradIRT[0]);
   polradIRTGr->SetMarkerStyle(20);
   polradIRTGr->SetMarkerColor(kGreen+1);
   polradIRTGr->SetLineColor(kGreen+1);
   polradIRTGr->SetLineWidth(1);

   leg->AddEntry(polradBornGr,"POLRAD2.0 Born ","l");
   leg->AddEntry(polradIRTGr, "POLRAD2.0 IRT ","l");

   // ------------------------------------------------
   // RADCOR FORTRAN
   inpath = "polrad/data/stat_xs_radcor_fortran_int-only.dat"; 
   ifstream infile4(inpath);
   vector<Double_t> radcor2Eprime;
   vector<Double_t> radcor2IRT;
   Double_t c3;
   if(infile4.fail()){
      std::cout << "Cannot open the file: " << inpath << std::endl;
      exit(1);
   }else{
      std::cout << "Opening the file: " << inpath << std::endl;
      while(!infile4.eof()){
         infile4 >> c0 >> c1 >> c2 >> c3;
         Double_t ep = InSANE::Kine::Eprime_W2theta(c2*c2/(1000*1000),theta,c0/1000); 
         if(infile4.eof()) break;
         radcor2Eprime.push_back(ep);
         radcor2IRT.push_back(c3);
      }
      infile4.close();
   }

   TGraph * radcor2IRTGr = new TGraph(radcor2Eprime.size(),&radcor2Eprime[0],&radcor2IRT[0]);
   radcor2IRTGr->SetMarkerStyle(20);
   radcor2IRTGr->SetMarkerColor(kRed);
   radcor2IRTGr->SetLineColor(kRed);
   radcor2IRTGr->SetLineWidth(2);
   radcor2IRTGr->SetLineStyle(3);

   // ------------------------------------------------
   // RADCOR
   inpath = "polrad/data/stat_xs_radcor_int-only.dat"; 
   ifstream infile3(inpath);
   vector<Double_t> radcorEprime;
   vector<Double_t> radcorBorn;
   vector<Double_t> radcorIRT;
   if(infile3.fail()){
      std::cout << "Cannot open the file: " << inpath << std::endl;
      exit(1);
   }else{
      std::cout << "Opening the file: " << inpath << std::endl;
      while(!infile3.eof()){
         infile3 >> c0 >> c1 >> c2;
         if(infile3.eof()) break;
         radcorEprime.push_back(c0);
         radcorBorn.push_back(c1);
         radcorIRT.push_back(c2);
      }
      infile3.close();
   }

   TGraph * radcorBornGr = new TGraph(radcorBorn.size(),&radcorEprime[0],&radcorBorn[0]);
   radcorBornGr->SetMarkerStyle(20);
   radcorBornGr->SetMarkerColor(kRed);
   radcorBornGr->SetLineColor(kRed);
   radcorBornGr->SetLineWidth(2);
   radcorBornGr->SetLineStyle(12);

   TGraph * radcorIRTGr = new TGraph(radcorBorn.size(),&radcorEprime[0],&radcorIRT[0]);
   radcorIRTGr->SetMarkerStyle(20);
   radcorIRTGr->SetMarkerColor(kRed);
   radcorIRTGr->SetLineColor(kRed);
   radcorIRTGr->SetLineWidth(1);

   leg->AddEntry(radcorBornGr,"RADCOR Born ","l");
   leg->AddEntry(radcor2IRTGr,"RADCOR IRT (FORTRAN)","l");
   leg->AddEntry(radcorIRTGr, "RADCOR IRT (C++)","l");

   //-----------------------------------
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec = new  InSANEPOLRADInelasticTailDiffXSec();
   //fDiffXSec->GetPOLRAD()->SetIRTMethod(InSANEPOLRAD::kFull);
   //fDiffXSec->GetPOLRAD()->fIRT_tau_Type   = ROOT::Math::IntegrationOneDim::kADAPTIVE;
   //fDiffXSec->GetPOLRAD()->fIRT_tau_Rule   = 1;
   //fDiffXSec->GetPOLRAD()->fIRT_tau_nCalls = 10000;
   //fDiffXSec->GetPOLRAD()->fIRT_tau_RelErr = 0.00001;
   //fDiffXSec->GetPOLRAD()->fIRT_tau_AbsErr = 0.00001;

   //fDiffXSec->GetPOLRAD()->fIRT_R_Type   = ROOT::Math::IntegrationOneDim::kADAPTIVE;
   //fDiffXSec->GetPOLRAD()->fIRT_R_nLegPoints   = 100;
   ////fDiffXSec->GetPOLRAD()->fIRT_R_Rule   = 5;
   //fDiffXSec->GetPOLRAD()->fIRT_R_RelErr = 0.000001;
   ////fDiffXSec->GetPOLRAD()->fIRT_R_AbsErr = 0.000001;
   ////fDiffXSec->GetPOLRAD()->fIRT_R_nCalls = 100000;

   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->GetPOLRAD()->SetHelicity(helicity);
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec->GetPOLRAD()->SetPolarizationVectors(P_target,helicity);

   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetNpx(60);
   sigmaE->SetLineColor(kBlack);
   sigmaE->SetLineWidth(1);

   //leg->AddEntry(sigmaE,"POLRAD IRT + ","l");
   //sigmaE->SetLineColor(kBlack);

   //-----------------------------------
   // Equiv radiator
   InSANEInelasticRadiativeTail * fDiffXSec2 = new  InSANEInelasticRadiativeTail();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec2->GetBornXSec()->SetBeamEnergy(beamEnergy);
   //fDiffXSec2->SetRegion4(false);
   fDiffXSec2->SetInternalOnly(true);
   fDiffXSec2->GetBornXSec()->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec2->GetBornXSec()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec2->GetBornXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,helicity);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetNpx(npx);
   sigmaE2->SetLineColor(kBlue);
   sigmaE2->SetLineWidth(1);
   //leg->AddEntry(sigmaE2,"Equivalent radiator + ","l");

   //-----------------------------------
   // born polarized + 
   InSANEPOLRADBornDiffXSec * fDiffXSecb1 = new  InSANEPOLRADBornDiffXSec();
   //InSANEPOLRADBornDiffXSec * fDiffXSecb1 = fDiffXSec2->GetBornXSec();//new  InSANEPOLRADBornDiffXSec();
   fDiffXSecb1->SetBeamEnergy(beamEnergy);
   fDiffXSecb1->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSecb1->GetPOLRAD()->SetHelicity(helicity);
   fDiffXSecb1->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSecb1->GetPOLRAD()->SetPolarizationVectors(P_target,helicity);
   fDiffXSecb1->InitializePhaseSpaceVariables();
   fDiffXSecb1->InitializeFinalStateParticles();
   //fDiffXSecb1->Refresh();
   TF1 * sigmab1 = new TF1("sigmab1", fDiffXSecb1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmab1->SetParameter(0,theta);
   sigmab1->SetParameter(1,phi);
   sigmab1->SetNpx(npx);
   sigmab1->SetLineColor(kBlack);
   sigmab1->SetLineStyle(13);
   sigmab1->SetLineWidth(2);
   //leg->AddEntry(sigmab1,"born +","l");

   leg->AddEntry(sigmab1,"InSANE Born","l");
   leg->AddEntry(sigmaE, "InSANE Polarized IRT ","l");
   leg->AddEntry(sigmaE2,"InSANE Equivalent radiator","l");

   // New radiator class
   InSANERadiator<InSANEInclusiveBornDISXSec> * fDiffXSecb2 = new  InSANERadiator<InSANEInclusiveBornDISXSec>();
   //InSANERadiator<InSANEPOLRADBornDiffXSec> * fDiffXSecb2 = new  InSANERadiator<InSANEPOLRADBornDiffXSec>();
   fDiffXSecb2->SetBeamEnergy(beamEnergy);
   fDiffXSecb2->SetTargetNucleus(InSANENucleus::Proton());
   //fDiffXSecb2->GetPOLRAD()->SetHelicity(helicity);
   //fDiffXSecb2->GetPOLRAD()->SetTargetPolarization(1.0);
   //fDiffXSecb2->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSecb2->InitializePhaseSpaceVariables();
   fDiffXSecb2->InitializeFinalStateParticles();
   fDiffXSecb2->SetInternalOnly(true);
   //fDiffXSecb2->SetDeltaM(0.0);
   //fDiffXSecb1->Refresh();
   TF1 * sigmab2 = new TF1("sigmab2", fDiffXSecb2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmab2->SetParameter(0,theta);
   sigmab2->SetParameter(1,phi);
   sigmab2->SetNpx(npx);
   sigmab2->SetLineColor(kBlack);
   sigmab2->SetLineStyle(4);
   sigmab2->SetLineWidth(2);
   //leg->AddEntry(sigmab2,"InSANEInclusiveBornDISXSec","l");

   //-----------------------------------
   F1F209eInclusiveDiffXSec * fDiffXSecF = new  F1F209eInclusiveDiffXSec();
   fDiffXSecF->SetBeamEnergy(beamEnergy);
   fDiffXSecF->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSecF->InitializePhaseSpaceVariables();
   fDiffXSecF->InitializeFinalStateParticles();
   TF1 * sigmaF = new TF1("sigmaF", fDiffXSecF, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaF->SetParameter(0,theta);
   sigmaF->SetParameter(1,phi);
   sigmaF->SetLineColor(4008);
   sigmaF->SetLineWidth(1);
   //leg->AddEntry(sigmaF,"F1F209 born","l");

   //-----------------------
   TCanvas * c = new TCanvas("xsec_test_POLRAD_IRT","xsec_test_POLRAD_IRT");
   //gPad->SetLogy(true);
   TString title =  "Internal Inelastic Radiative Tail";//fDiffXSec->GetPlotTitle();
   TString yAxisTitle = "#frac{d#sigma}{dE'd#Omega} (nb/GeV/sr)";
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   gr = new TGraph(sigmaE->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigmaE2->DrawCopy("same"));
   mg->Add(gr,"l");

   mg->Add(polradIRTGr,"l");

   mg->Add(radcorIRTGr,"l");
   mg->Add(radcor2IRTGr,"l");

   gr = new TGraph(sigmab1->DrawCopy("same"));
   mg->Add(gr,"l");
   //gr = new TGraph(sigmab2->DrawCopy("same"));
   //mg->Add(gr,"l");
   mg->Add(radcorBornGr,"l");
   mg->Add(polradBornGr,"l");

   //gr = new TGraph(sigmaF->DrawCopy("same"));
   //mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   leg->Draw();

   latex->SetTextSize(0.04);
   latex->SetTextAlign(12);  //centered
   latex->SetTextFont(132);
   latex->SetTextAngle(0);
   latex->DrawLatex(0.2,0.35,Form("#theta_{target} = %.0f#circ",theta_target/degree));
   latex->DrawLatex(0.2,0.3,Form("#theta_{e} = %.0f#circ",theta/degree));
   latex->DrawLatex(0.2,0.25,Form("E = %.2f GeV",beamEnergy));

   gPad->Modified();

   c->SaveAs(Form("results/cross_sections/inclusive/compare_IRT_%d.png",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/compare_IRT_%d.pdf",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/compare_IRT_%d.tex",aNumber));

   
   return 0;
}
