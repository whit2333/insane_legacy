#include <cstdlib>
#include <iostream>

Int_t born_test(){

   InSANEPOLRADInternalPolarizedDiffXSec * sig = new InSANEPOLRADInternalPolarizedDiffXSec();
   /// Set k1,k2 etc... (done in constructor right now)
   sig->GetPOLRAD()->SetVerbosity(4);
   sig->InitializePhaseSpaceVariables();
   sig->GetPOLRAD()->Process();
   sig->GetPOLRAD()->SetHelicity(1);

   /* sig->GetPOLRAD()->Print();*/

   Double_t Ebeam = 4.73;
   Double_t Eprime = 0.85;
   Double_t theta = 45.0*degree;
   Double_t nu = Ebeam-Eprime;
   Double_t Mtarg = 0.938;
   Double_t S = 2.0*Ebeam*Mtarg;
   Double_t X = 2.0*Eprime*Mtarg;
   Double_t Q2 = 4.0*Eprime*Ebeam*TMath::Sin(theta/2.0)*TMath::Sin(theta/2.0);
   Double_t x_bj = Q2/(2.*Mtarg*nu);
   /* sig->Print();*/
   // std::cout << "sig_dis_born = " << sig->GetPOLRAD()->sig_dis_born(S,X,Q2) << " units.. \n";

   Double_t arg=0;
   vector<Double_t> xs;
   vector<int> Hel;
   Hel.push_back(-1);
   Hel.push_back(1);

   int N = 2;
   for(int i=0;i<N;i++){
      sig->GetPOLRAD()->SetHelicity(Hel[i]);
      sig->GetPOLRAD()->Process();
      arg = sig->GetPOLRAD()->sig_dis_born(S,X,Q2);
      xs.push_back(arg);
   }

   cout << "=============================== Born DIS XS Test ===============================" << endl;

   cout << "x_bj = " << Form("%.3f",x_bj) << endl;
   for(int i=0;i<N;i++){
      cout << "helicity = " << Hel[i] << "\t" << "born_xs = " << Form("%.4E",xs[i]) << " units" << endl;
   }

   Double_t delta_xs = xs[0] - xs[1];
   Double_t sum_xs = xs[0] + xs[1];
   Double_t avg_xs = 0.5*sum_xs;
   Double_t asym = delta_xs/avg_xs;

   cout << "delta_xs (minus - plus) = " << Form("%.4E",delta_xs) << " units" << endl;
   cout << "average (0.5*sum) = " << Form("%.4E",sum_xs ) << " units" << endl;
   cout << "asymmetry = " << Form("%.4E",asym ) << endl;


   // TF1 * sig_born = new TF1("sig_born", sig, &InSANEPOLRADInternalPolarizedDiffXSec::xDependentXSec,
   // 0.1, 0.9, 2,"InSANEPOLRADInternalPolarizedDiffXSec","xDependentXSec");
   // sig_born->SetNumberFitPoints(4);
   // sig_born->SetParameter(0,4.0/6.0);
   // sig_born->SetParameter(1,0.0);
   // sig_born->Draw();

   return(0);
}
