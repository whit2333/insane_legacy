/*! Script used to compare to the results from table III from mo and tsai. 
 */
Int_t sigma_ert_compare(const char * expname  = "mo_and_tsai"){

   /// Ep and sigma values from table III of Mo and Tsai for Es=5GeV
   vector<Double_t> mEp,rEp;
   vector<Double_t> mXS,rXS;
   vector<Double_t> PDiff; 

   Double_t Ebeam      = 0;
   Int_t FileIndex     = 5; 
   Double_t theta      = 5.0*degree;
   Double_t phi        = 0*degree;
   Int_t    npar       = 2;
   Double_t minTheta   = 10*degree;
   Double_t maxTheta   = 50.0*degree;

   InSANENucleus::NucleusType Target = InSANENucleus::kProton; 

   cout << "Enter beam energy (1, 5 or 20 GeV): "; 
   cin  >> Ebeam; 

   Double_t Emin,Emax; 

   if(Ebeam<4){
	   Emin = 0.200;
	   Emax = 0.990;
   }else if(Ebeam>=5){
	   Emin = 1.000;
	   Emax = 4.900;
   }else if(Ebeam>=20){
	   Emin = 1.500;
	   Emax = 18.400;
   } 

   FileIndex = Int_t(Ebeam); 

   ImportMoTsaiData(FileIndex,mEp,mXS); 
   //ImportRosetailData(FileIndex,rEp,rXS); 

   TGraph *MoTsai   = GetTGraph(mEp,mXS); 
   //TGraph *Rosetail = GetTGraph(rEp,rXS);

   MoTsai->SetMarkerStyle(20);
   MoTsai->SetMarkerColor(kBlack);

   int width = 2;

   //Rosetail->SetLineColor(kBlue);
   //Rosetail->SetLineWidth(width);

   TMultiGraph *G = new TMultiGraph();
   G->Add(MoTsai,"p");
   //G->Add(Rosetail,"c"); 

   /// Elastic tail cross section 
   InSANEPOLRADElasticTailDiffXSec * fDiffXSec = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec->SetTargetType(Target);
   fDiffXSec->SetBeamEnergy(Ebeam);
   fDiffXSec->GetPOLRAD()->SetUnpolarized();
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   InSANEPhaseSpace * ps = fDiffXSec->GetPhaseSpace();
   fDiffXSec->GetPhaseSpace()->GetVariableWithName("energy_e")->SetMaximum(20.0);
   
   TF1 * fSigma = new TF1(Form("fSigma%d",0), fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   fSigma->SetParameter(1,phi);
   fSigma->SetParameter(0,theta);
   fSigma->SetNpx(20);
   fSigma->SetLineColor(kRed);
   fSigma->SetLineWidth(width);

   CalculatePDiff(rEp,rXS,fSigma,PDiff); 
   TGraph *PD = GetTGraph(rEp,PDiff);
   PD->SetLineWidth(width);

   TLegend *L = new TLegend(0.6,0.6,0.8,0.8);
   L->SetFillColor(kWhite);
   L->AddEntry(MoTsai  ,"Mo & Tsai"       ,"p");
   //L->AddEntry(Rosetail,"Rosetail (Exact)","l");
   L->AddEntry(fSigma  ,"POLRAD (Exact)"  ,"l");

   /// Create the canvas and plot 
   TCanvas * c = new TCanvas("compare","compare",1000,800);
   c->Divide(2,1); 

   TString Option     = Form("A");
   TString Title      = Form("Elastic Tail (E_{s} = %.0f GeV, #theta = %.0f#circ)",Ebeam,theta/degree);
   TString xAxisTitle = Form("E_{p} (GeV)");
   TString yAxisTitle = Form("#frac{d^{2}#sigma}{d#OmegadE_{p}} [nb/GeV/sr]");

   c->cd(1);
   gPad->SetLogy(1); 
   //G->Draw(Option);
   //G->SetTitle(Title);
   //G->GetXaxis()->SetTitle(xAxisTitle);
   //G->GetXaxis()->CenterTitle();
   //G->GetYaxis()->SetTitle(yAxisTitle);
   //G->GetYaxis()->CenterTitle();
   fSigma->Draw();
   L->Draw("same"); 
   c->Update();

   c->cd(2); 
   gPad->SetLogy(0); 
   PD->Draw("AL");
   PD->SetTitle("Relative Error of POLRAD Compared to ROSETAIL");
   PD->GetXaxis()->SetTitle(xAxisTitle);
   PD->GetXaxis()->CenterTitle();
   PD->Draw("AL");
   c->Update();
   
   // Print the values
   Double_t * energy_e =  ps->GetVariableWithName("energy_e")->GetCurrentValueAddress();
   Double_t * theta_e  =  ps->GetVariableWithName("theta_e")->GetCurrentValueAddress();
   Double_t * phi_e    =  ps->GetVariableWithName("phi_e")->GetCurrentValueAddress();
   // for(int i = 0; i<mEp.size(); i++){
   //    (*energy_e) = mEp[i];
   //    (*theta_e)  = theta;
   //    (*phi_e)    = phi;
   //    fDiffXSec->EvaluateCurrent();
   //    fDiffXSec->PrintTable();

   // }

   TString fn      = Form("sigma_ert_compare_%s_%d",expname,FileIndex); 
   TString png_dir = Form("./plots/png/");
   TString pdf_dir = Form("./plots/pdf/");
   TString png     = png_dir + fn + Form(".png");
   TString pdf     = pdf_dir + fn + Form(".pdf");
   TString cmd1    = Form("mkdir -p %s",png_dir.Data());
   TString cmd2    = Form("mkdir -p %s",pdf_dir.Data());

   gSystem->Exec(cmd1);
   gSystem->Exec(cmd2);

   c->SaveAs(png);
   c->SaveAs(pdf);

   return(0);
}
//______________________________________________________________________________
void CalculatePDiff(vector<double> x,vector<double> RT,TF1 *f,vector<double> &P){

	double num=0,den=0,arg=0,F=0; 
	const int N = x.size(); 
	for(int i=0;i<N;i++){
		F   = f->Eval(x[i]); 
		num = RT[i] - F;
		den = RT[i]; 
		arg = 100.*TMath::Abs(num/den);  
		P.push_back(arg); 
	}

}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){

	const int N = x.size();
	TGraph *g = new TGraph(N,&x[0],&y[0]);
	return g;

}
//______________________________________________________________________________
void ImportMoTsaiData(int Pass,vector<double> &Ep,vector<double> &Tail){

        double CONV = 1E+3;
	double iEp,iW,iT; 

	TString inpath = Form("./output/MoTsai/elastic/MoTsai_%d-pass.dat",Pass);
	ifstream infile;
	infile.open(inpath); 
	if(infile.fail()){
		cout << "Cannot open the file: " << inpath << endl;
		exit(1);
	}else{
		cout << "Opening the file: " << inpath << endl;
		while(!infile.eof()){
			infile >> iEp >> iW >> iT;
			Ep.push_back(iEp/CONV);
			Tail.push_back(iT); 
		}
		infile.close();
		Ep.pop_back();
		Tail.pop_back();
	}

}
//______________________________________________________________________________
void ImportRosetailData(int Pass,vector<double> &Ep,vector<double> &Tail){

        double CONV = 1E+3;
	double iEs,iEp,iNu,iW,iT; 

	TString inpath = Form("./output/rosetail/elastic/Mo-T_eltail_unpl_%d-pass.out",Pass);
	ifstream infile;
	infile.open(inpath); 
	if(infile.fail()){
		cout << "Cannot open the file: " << inpath << endl;
		exit(1);
	}else{
		cout << "Opening the file: " << inpath << endl;
		while(!infile.eof()){
			infile >> iEs >> iEp >> iNu >> iW >> iT;
			Ep.push_back(iEp/CONV);
			Tail.push_back(iT); 
		}
		infile.close();
		Ep.pop_back();
		Tail.pop_back();
	}


}
