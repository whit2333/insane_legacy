Int_t fill_packing_fractions(Int_t num=1){

//Run# shift raster C_run Kinematics dat/mc Wmin #bins pf dpf

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   TSQLServer * db = dbman->GetMySQLConnection();//TSQLServer::Connect("mysql://quarks.temple.edu","sane","secret");
   const int N_rows = 13;
   double row[N_rows];

   TString query_start = "INSERT INTO SANE.packing_fractions \
			 (run_number,shift,raster,carbon_run,theta_target,beam_energy,p_hms,theta_hms,data_over_mc,W_min,n_bins,packing_fraction,pf_error) \
		         VALUES (";	 
   TString query_values = "";
   TString query_end = " ); ";

   const char * file = Form("packing_fraction/pfs_para.txt");
   std::cout << " FILE NAME: " << file << "\n";
   ifstream myfile (file);
   if (myfile.is_open()) {
     while ( myfile.good() )
     {
	query_values = "";

	for(int i =0; i < N_rows ;i++) {
	   myfile >> row[i];
	   if(i==0)query_values += Form("'%f'",row[i]);
	   else query_values += Form(",'%f'",row[i]);
	   std::cout << i << "=" << row[i] << "\n";
	}
	if(db) {
           TString query = query_start + query_values + query_end ;
	   std::cout << query.Data() << "\n"; 
	   db->Query(query.Data());
	}	
	
        //getline (myfile,line);
        //cout << line << endl;
     
     }
     myfile.close();
   }
   else cout << "Unable to open file"; 

   const char * file2 = Form("packing_fraction/pfs_perp.txt");
   std::cout << " FILE NAME: " << file2 << "\n";
   ifstream myfile (file2);
   if (myfile.is_open()) {
     while ( myfile.good() )
     {
	query_values = "";

	for(int i =0; i < N_rows ;i++) {
	   myfile >> row[i];
	   if(i==0)query_values += Form("'%f'",row[i]);
	   else query_values += Form(",'%f'",row[i]);
	   std::cout << i << "=" << row[i] << "\n";
	}
	if(db) {
           TString query = query_start + query_values + query_end ;
	   std::cout << query.Data() << "\n"; 
	   db->Query(query.Data());
	}	
	
        //getline (myfile,line);
        //cout << line << endl;
     
     }
     myfile.close();
   }
   else cout << "Unable to open file"; 

   return 0;
}
