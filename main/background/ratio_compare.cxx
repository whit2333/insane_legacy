Int_t ratio_compare(){

   gROOT->LoadMacro("background/eplus_eminus_ratio.cxx"); 
   // function of theta,E' (deg,GeV)
   TF2 * R_ratio = GetR_PlusMinus();

   NucDBManager     * dbman = NucDBManager::GetManager();
   NucDBExperiment  * exp   = 0;
   NucDBMeasurement * meas  = 0;

   TList * meas_list = dbman->GetMeasurements("R_eplusOvereminus");
   TMultiGraph  * mg = new TMultiGraph();
   TGraphErrors * gr = 0;
   TLegend * leg     = new TLegend(0.7,0.7,0.9,0.9);

   NucDBBinnedVariable * EbeamBin = new NucDBBinnedVariable("Ebeam","Ebeam");
   EbeamBin->SetBinMinimum(4.0);
   EbeamBin->SetBinMaximum(6.0);
   EbeamBin->SetAverage(5.0);
   EbeamBin->SetMean(5.0);
   meas_list->Print();

   NucDBBinnedVariable * thetaBin = new NucDBBinnedVariable("theta","#theta");
   thetaBin->SetBinMinimum(30.0);
   thetaBin->SetBinMaximum(41.0);

   NucDBDiscreteVariable * A = new NucDBDiscreteVariable("A","A");
   A->SetValue(1);

   NucDBDiscreteVariable * Rl = new NucDBDiscreteVariable("rl","rl");
   Rl->SetValue(0.23);

   for(int i = 0; i<meas_list->GetEntries();i++){
      meas = (NucDBMeasurement*)(meas_list->At(i));
      if(meas){

         TList * plist = meas->ApplyFilterWithBin(EbeamBin);
         if(!plist) continue;
         if(plist->GetEntries() == 0 ) continue;

         plist = meas->ApplyFilterWithBin(thetaBin);
         if(!plist) continue;
         if(plist->GetEntries() == 0 ) continue;

         plist = meas->ApplyFilterWith(A);
         if(!plist) continue;
         if(plist->GetEntries() == 0 ) continue;

         //plist = meas->ApplyFilterWith(Rl);
         //if(!plist) continue;
         //if(plist->GetEntries() == 0 ) continue;

         gr = meas->BuildGraph("E");
         gr->SetMarkerStyle(20+i);
         mg->Add(gr,"ep");
         leg->AddEntry(gr,meas->GetExperimentName(),"p");
      }
   }

   TLegend * leg0 = new TLegend(0.35,0.6,0.9,0.9);
   int Npts     = 100; //hEnergy1->GetXaxis()->GetNbins();
   double Elow  = 0.5;
   double deltaE = 0.02;
   TGraph * g0  = new TGraph(Npts);
   TGraph * g1  = new TGraph(Npts);
   TGraph * g2  = new TGraph(Npts);
   TGraph * g3  = new TGraph(Npts);
   double rl = 0.29;
   for(int i = 0; i<Npts;i++){
      double Eprime = Elow + double(i)*deltaE;
      g0->SetPoint(i,Eprime,R_ratio->Eval(25.0,Eprime)/rl);
      g1->SetPoint(i,Eprime,R_ratio->Eval(30.0,Eprime)/rl);
      g2->SetPoint(i,Eprime,R_ratio->Eval(40.0,Eprime)/rl);
      g3->SetPoint(i,Eprime,R_ratio->Eval(50.0,Eprime)/rl);
   }
   g0->SetLineColor(kGreen-4);
   g0->SetLineWidth(2);
   g1->SetLineColor(3);
   g1->SetLineWidth(2);
   g2->SetLineColor(2);
   g2->SetLineWidth(2);
   g3->SetLineColor(4);
   g3->SetLineWidth(2);
   //leg0->AddEntry(g0,"Fersch #theta=25 deg","l");
   leg0->AddEntry(g1,"Fersch #theta=30 deg","l");
   leg->AddEntry(g2,"Fersch #theta=40 deg","l");
   leg->AddEntry(g3,"Fersch #theta=50 deg","l");
   //leg->Draw();
      
   TCanvas * c = new TCanvas();
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(0.001, 1.1);
   //mg->GetXaxis()->SetLimits(0.001, 2.5);
   mg->GetXaxis()->SetTitle("E");
   gPad->SetLogy(true);
   //g0->Draw("l");
   g1->Draw("l");
   g2->Draw("l");
   g3->Draw("l");
   leg->Draw();
   //leg0->Draw();


   c->SaveAs("results/background/ratio_compare.png");


   return 0;
}
