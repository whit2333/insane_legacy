/*! Subtracts the elastic tail.
 *
 */
Int_t bg_corrections_para47(Int_t paraRunGroup = 8209,Int_t aNumber=8209){

   // old way with thesis of Fersch fit.
   //gROOT->LoadMacro("background/eplus_eminus_ratio.cxx"); 
   // function of theta,E' (deg,GeV)
   //TF2 * R_ratio = GetR_PlusMinus();

   // New fit to simulation
   TFile * fin = new TFile("data/electron_pion_ratio.root","READ");
   TF1 * R_ratio = (TF1*)gROOT->FindObject("RPlusMinus_para47");
   if(!R_ratio) return -22;

   TF1 *r_ratio    = new TF1("rPlusMinus_para47","expo(0)",0.5,3.0);
   r_ratio->SetParameter(0,R_ratio->GetParameter(0));
   r_ratio->SetParameter(1,R_ratio->GetParameter(1));

   TF2 * R_ratio_2D = (TF2*)gROOT->FindObject("fR_2D_para47");
   if(!R_ratio_2D) return -222;


   Double_t theta_target = 180.0*degree;
   Double_t fBeamEnergy  = 4.7;
   TString  filename     = Form("data/binned_asymmetries_para47_%d.root",paraRunGroup);
   TString  filename2    = Form("data/bg_corrected_asymmetries_para47_%d.root",paraRunGroup);

   // --------------------------------------------------
   //
   gSystem->mkdir(Form("results/asymmetries/%d",aNumber),true);

   TFile * f1 = TFile::Open(filename,"UPDATE");
   f1->cd();
   TList * fAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para47-%d",0));
   if(!fAsymmetries) return(-1);

   TList * asym_write = new TList();
   TList * BG_write   = new TList();

   TMultiGraph * mg1 = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();
   TMultiGraph * mg4 = new TMultiGraph();
   TMultiGraph * mg5 = new TMultiGraph();
   TMultiGraph * mg6 = new TMultiGraph();

   TMultiGraph * mgW1 = new TMultiGraph();
   TMultiGraph * mgW2 = new TMultiGraph();
   TMultiGraph * mgW3 = new TMultiGraph();
   TMultiGraph * mgW4 = new TMultiGraph();
   TMultiGraph * mgW5 = new TMultiGraph();
   TMultiGraph * mgW6 = new TMultiGraph();

   std::string table_dir1 = "results/asymmetries/tables/";
   table_dir1            += std::to_string(aNumber);
   table_dir1            += "/bg_sub";
   table_dir1            += "/A180_47_bg_sub/";

   std::string table_dir0 = "results/asymmetries/tables/";
   table_dir0            += std::to_string(aNumber);
   table_dir0            += "/no_corrections";
   table_dir0            += "/A180_47/";

   gSystem->mkdir(table_dir1.c_str(),true);
   gSystem->mkdir(table_dir0.c_str(),true);

   for(int jj = 0;jj<fAsymmetries->GetEntries() ;jj++) {

      // Get the measured asymmetry 
      InSANEAveragedMeasuredAsymmetry * asymAverage = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries->At(jj));
      InSANEMeasuredAsymmetry * asym                  = asymAverage->GetMeasuredAsymmetryResult();

      asym_write->Add(asym);

      asym->ResetMasks();

      std::string   filename0       = Form("A180_47_%d_%d.txt",aNumber,jj);
      std::string   table_filename0 = table_dir0 + filename0;
      std::ofstream outtable0(table_filename0.c_str());

      asym->PrintBinnedTableHead(outtable0);
      asym->PrintBinnedTable(outtable0);


      Int_t   xBinmax = 0;
      Int_t   yBinmax = 0;
      Int_t   zBinmax = 0;
      Int_t   bin     = 0;
      InSANERadiativeCorrections1D * RCs    = 0;

      // -------------------------------------------------
      // Asymmetry vs x
      TH1F *                       fA      = &asym->fAsymmetryVsx;
      InSANEAveragedKinematics1D * avgKine = &asym->fAvgKineVsx;
      TH1F *                       fA_syst = &asym->fSystErrVsx;

      // Add new systematics
      InSANESystematic * BG_systematic_x  = asym->AddSystematic_x(*fA,"C_bg");
      InSANESystematic * Rbg_systematic_x = asym->AddSystematic_x(*fA,"R_bg");
      InSANESystematic * Abg_systematic_x = asym->AddSystematic_x(*fA,"A_bg_pair");
      BG_systematic_x->fBefore  = *fA_syst;
      Rbg_systematic_x->fBefore = *fA_syst;
      Abg_systematic_x->fBefore = *fA_syst;

      TH1F * fR_bg = new TH1F( *(fA) );
      fR_bg->Reset();
      fR_bg->SetNameTitle(Form("Rbg-x-%d",jj),Form("Rbg-x-%d",jj));
      BG_write->Add(fR_bg);

      TH1F * fA_bg = new TH1F( *(fA) );
      fA_bg->Reset();
      fA_bg->SetNameTitle(Form("Abg-x-%d",jj),Form("Abg-x-%d",jj));
      BG_write->Add(fA_bg);

      TH1F * fC_bg = new TH1F( *(fA) );
      fC_bg->Reset();
      fC_bg->SetNameTitle(Form("Cbg-x-%d",jj),Form("Cbg-x-%d",jj));
      BG_write->Add(fC_bg);

      TH1F * ff_bg = new TH1F( *(fA) );
      ff_bg->Reset();
      ff_bg->SetNameTitle(Form("fbg-x-%d",jj),Form("fbg-x-%d",jj));
      BG_write->Add(ff_bg);

      TH1F * fC_Abg = new TH1F( *(fA) );
      fC_Abg->Reset();
      fC_Abg->SetNameTitle(Form("CAbg-x-%d",jj),Form("CAbg-x-%d",jj));
      BG_write->Add(fC_Abg);

      TH1F * fN_pair = new TH1F( *(fA) );
      fN_pair->Reset();
      fN_pair->SetNameTitle(Form("Npair-x-%d",jj),Form("Npair-x-%d",jj));
      BG_write->Add(fN_pair);

      TH1F * fN_dis = new TH1F( *(fA) );
      fN_dis->Reset();
      fN_dis->SetNameTitle(Form("Ndis-x-%d",jj),Form("Ndis-x-%d",jj));
      BG_write->Add(fN_dis);

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over x bins
      //
      for(Int_t i=1; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 


         Double_t A            = fA->GetBinContent(bin);
         Double_t eA           = fA->GetBinError(bin);
         Double_t eASyst       = fA_syst->GetBinContent(bin);
         if(A==0.0) continue;

         // Don't know these too well!!!!!!
         Double_t A_bg         = -0.022;
         Double_t eA_bg        =  0.005;

         Double_t Ep             = avgKine->fE.GetBinContent(bin);
         Double_t theta          = avgKine->fTheta.GetBinContent(bin);


         Double_t r_bg           = r_ratio->Eval(Ep);
         Double_t R_bg           = r_bg/(1.0+r_bg);
         //Double_t R_bg           = R_ratio->Eval(Ep)/2.0;
         //Double_t R_bg           = R_ratio_2D->Eval(Ep,theta/degree)/2.0;
         Double_t eR_bg          = 0.01*R_bg; // Using 1% as error for now
         //Double_t f_bg           = 1.0/(1.0-2.0*R_bg); 
         //Double_t C_Abg          = 2.0*R_bg*A_bg/(1.0-2.0*R_bg); 
         Double_t f_bg           = (1.0+R_bg)/(1.0-R_bg); 
         Double_t C_Abg          = 2.0*R_bg*A_bg/(1.0-R_bg); 
         Double_t C_bg           = 0.0;//
         //if( A != 0.0) C_bg = (1.0 - 2.0*R_bg*A_bg/A)/(1.0 - 2.0*R_bg); // Factor of 2 is for sane since positrons are the "same" as electrons
         if( A != 0.0) {
            C_bg = (f_bg - C_Abg/A); 
         }
         Double_t A_corr         = C_bg*A;
         Double_t eA_corr        = C_bg*eA;

         Double_t N_total        = avgKine->fN.GetBinContent(bin);
         Double_t N_pair         = N_total*(R_bg/(1.0+R_bg));
         Double_t N_dis          = N_total*((1.0-R_bg)/(1.0+R_bg));

         if( TMath::IsNaN(A_corr) ) A_corr = 0;

         fN_pair->SetBinContent(bin,N_pair);
         fN_dis->SetBinContent(bin,N_dis);

         fC_bg->SetBinContent(bin,C_bg);

         ff_bg->SetBinContent(bin,f_bg);
         fC_Abg->SetBinContent(bin,C_Abg);

         fR_bg->SetBinContent(bin,R_bg);

         fA_bg->SetBinContent(bin,A_bg);
         fA_bg->SetBinError(bin,eA_bg);

         fA->SetBinContent(bin,A_corr);
         fA->SetBinError(bin,eA_corr);

         // Systematic Errors
         Double_t eSystBG          = InSANE::Phys::delta_A_pair_corr( A, eASyst, A_bg, eA_bg, R_bg, eR_bg );
         Double_t eSystBG_only     = InSANE::Phys::delta_A_pair_corr( A, 0.0, A_bg, eA_bg, R_bg, eR_bg );
         Double_t eSystBG_R_only   = InSANE::Phys::delta_A_pair_corr( A, 0.0, A_bg, 0.0, R_bg, eR_bg );
         Double_t eSystBG_Abg_only = InSANE::Phys::delta_A_pair_corr( A, 0.0, A_bg, eA_bg, R_bg, eR_bg );

         //   eASyst*eASyst/TMath::Power(1.0-2.0*R_bg,2.0);
         //Double_t num      = 4.0*TMath::Power((1.0-2.0*R_bg)*R_bg*eA_bg,2.0) ;
         //num              += 4.0*TMath::Power((A - A_bg)*eR_bg,2.0);
         //eSystBG          += num/TMath::Power(1.0-2.0*R_bg,4.0);
         fA_syst->SetBinContent( bin, eSystBG );

         BG_systematic_x->fAfter.SetBinContent( bin, eSystBG );
         BG_systematic_x->fValues.SetBinContent(bin, C_bg );
         BG_systematic_x->fUncertainties.SetBinContent(bin, eSystBG_only );
         BG_systematic_x->fRelativeUncertainties.SetBinContent(bin, eSystBG_only/TMath::Abs(A) );

         Rbg_systematic_x->fAfter.SetBinContent( bin, eSystBG );
         Rbg_systematic_x->fValues.SetBinContent(bin, R_bg );
         Rbg_systematic_x->fUncertainties.SetBinContent(bin, eSystBG_R_only );
         Rbg_systematic_x->fRelativeUncertainties.SetBinContent(bin, eSystBG_R_only/TMath::Abs(A) );

         Abg_systematic_x->fAfter.SetBinContent( bin, eSystBG );
         Abg_systematic_x->fValues.SetBinContent(bin, A_bg );
         Abg_systematic_x->fUncertainties.SetBinContent(bin, eSystBG_Abg_only );
         Abg_systematic_x->fRelativeUncertainties.SetBinContent(bin, eSystBG_Abg_only/TMath::Abs(A) );
      }


      TGraph * gr = 0;
      if(jj<4) {
         gr = new TGraph( fR_bg );
         mg1->Add(gr,"l");

         gr = new TGraph( fC_bg );
         mg2->Add(gr,"l");

         gr = new TGraph( fN_dis );
         mg3->Add(gr,"l");
         gr = new TGraph( fN_pair );
         mg3->Add(gr,"l");

         gr = new TGraph( fA_bg );
         mg4->Add(gr,"l");

         gr = new TGraph( ff_bg );
         mg5->Add(gr,"l");

         gr = new TGraph( fC_Abg );
         mg6->Add(gr,"l");

      }


      // -------------------------------------------------
      // Asymmetry vs W
      fA      = &asym->fAsymmetryVsW;
      avgKine = &asym->fAvgKineVsW;
      fA_syst = &asym->fSystErrVsW;

      // Add new systematics
      InSANESystematic * BG_systematic  = asym->AddSystematic_W(*fA,"C_bg");
      InSANESystematic * Rbg_systematic = asym->AddSystematic_W(*fA,"R_bg");
      InSANESystematic * Abg_systematic = asym->AddSystematic_W(*fA,"A_bg_pair");
      BG_systematic->fBefore  = *fA_syst;
      Rbg_systematic->fBefore = *fA_syst;
      Abg_systematic->fBefore = *fA_syst;

      // -------------------------------------------------
      fR_bg = new TH1F( *(fA) );
      fR_bg->Reset();
      fR_bg->SetNameTitle(Form("Rbg-W-%d",jj),Form("Rbg-W-%d",jj));
      BG_write->Add(fR_bg);

      fA_bg = new TH1F( *(fA) );
      fA_bg->Reset();
      fA_bg->SetNameTitle(Form("Abg-W-%d",jj),Form("Abg-W-%d",jj));
      BG_write->Add(fA_bg);

      fC_bg = new TH1F( *(fA) );
      fC_bg->Reset();
      fC_bg->SetNameTitle(Form("Cbg-W-%d",jj),Form("Cbg-W-%d",jj));
      BG_write->Add(fC_bg);

      ff_bg = new TH1F( *(fA) );
      ff_bg->Reset();
      ff_bg->SetNameTitle(Form("fbg-W-%d",jj),Form("fbg-W-%d",jj));
      BG_write->Add(ff_bg);

      fC_Abg = new TH1F( *(fA) );
      fC_Abg->Reset();
      fC_Abg->SetNameTitle(Form("CAbg-W-%d",jj),Form("CAbg-W-%d",jj));
      BG_write->Add(fC_Abg);

      fN_pair = new TH1F( *(fA) );
      fN_pair->Reset();
      fN_pair->SetNameTitle(Form("Npair-W-%d",jj),Form("Npair-W-%d",jj));
      BG_write->Add(fN_pair);

      fN_dis = new TH1F( *(fA) );
      fN_dis->Reset();
      fN_dis->SetNameTitle(Form("Ndis-W-%d",jj),Form("Ndis-W-%d",jj));
      BG_write->Add(fN_dis);

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over W bins
      //
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 


         Double_t A            = fA->GetBinContent(bin);
         Double_t eA           = fA->GetBinError(bin);
         Double_t eASyst       = fA_syst->GetBinContent(bin);
         if(A==0.0) continue;

         // Don't know these too well!!!!!!
         Double_t A_bg         = -0.022;
         Double_t eA_bg        =  0.005;

         Double_t Ep             = avgKine->fE.GetBinContent(bin);
         Double_t theta          = avgKine->fTheta.GetBinContent(bin);

         Double_t r_bg           = r_ratio->Eval(Ep);
         Double_t R_bg           = r_bg/(1.0+r_bg);
         //Double_t R_bg           = R_ratio->Eval(Ep)/2.0;
         //Double_t R_bg           = R_ratio_2D->Eval(Ep,theta/degree)/2.0;
         Double_t eR_bg          = 0.01*R_bg; // Using 1% as error for now
         //Double_t f_bg           = 1.0/(1.0-2.0*R_bg); 
         //Double_t C_Abg          = 2.0*R_bg*A_bg/(1.0-2.0*R_bg); 
         Double_t f_bg           = (1.0+R_bg)/(1.0-R_bg); 
         Double_t C_Abg          = 2.0*R_bg*A_bg/(1.0-R_bg); 
         Double_t C_bg           = 0.0;//
         //if( A != 0.0) C_bg = (1.0 - 2.0*R_bg*A_bg/A)/(1.0 - 2.0*R_bg); // Factor of 2 is for sane since positrons are the "same" as electrons
         if( A != 0.0) {
            C_bg = (f_bg - C_Abg/A); 
         }
         /// \TODO The statment above is not really true because of track reconstruction. Figure out what the equivalency is!
         Double_t A_corr         = C_bg*A;
         Double_t eA_corr        = C_bg*eA;

         Double_t N_total        = avgKine->fN.GetBinContent(bin);
         Double_t N_pair         = N_total*(R_bg/(1.0+R_bg));
         Double_t N_dis          = N_total*((1.0-R_bg)/(1.0+R_bg));

         if( TMath::IsNaN(A_corr) ) A_corr = 0;

         fN_pair->SetBinContent(bin,N_pair);
         fN_dis->SetBinContent( bin,N_dis);

         fC_bg->SetBinContent(bin,C_bg);

         ff_bg->SetBinContent(bin,f_bg);
         fC_Abg->SetBinContent(bin,C_Abg);

         fR_bg->SetBinContent(bin,R_bg);

         fA_bg->SetBinContent(bin,A_bg);
         fA_bg->SetBinError(bin,eA_bg);

         fA->SetBinContent(bin,A_corr);
         fA->SetBinError(bin,  eA_corr);

         // Systematic Errors
         Double_t eSystBG          = InSANE::Phys::delta_A_pair_corr( A, eASyst, A_bg, eA_bg, R_bg, eR_bg );
         Double_t eSystBG_only     = InSANE::Phys::delta_A_pair_corr( A, 0.0, A_bg, eA_bg, R_bg, eR_bg );
         Double_t eSystBG_R_only   = InSANE::Phys::delta_A_pair_corr( A, 0.0, A_bg, 0.0, R_bg, eR_bg );
         Double_t eSystBG_Abg_only = InSANE::Phys::delta_A_pair_corr( A, 0.0, A_bg, eA_bg, R_bg, eR_bg );

         //   eASyst*eASyst/TMath::Power(1.0-2.0*R_bg,2.0);
         //Double_t num      = 4.0*TMath::Power((1.0-2.0*R_bg)*R_bg*eA_bg,2.0) ;
         //num              += 4.0*TMath::Power((A - A_bg)*eR_bg,2.0);
         //eSystBG          += num/TMath::Power(1.0-2.0*R_bg,4.0);
         fA_syst->SetBinContent( bin, eSystBG );

         BG_systematic->fAfter.SetBinContent( bin, eSystBG );
         BG_systematic->fValues.SetBinContent(bin, C_bg );
         BG_systematic->fUncertainties.SetBinContent(bin, eSystBG_only );
         BG_systematic->fRelativeUncertainties.SetBinContent(bin, eSystBG_only/TMath::Abs(A) );

         Rbg_systematic->fAfter.SetBinContent( bin, eSystBG );
         Rbg_systematic->fValues.SetBinContent(bin, R_bg );
         Rbg_systematic->fUncertainties.SetBinContent(bin, eSystBG_R_only );
         Rbg_systematic->fRelativeUncertainties.SetBinContent(bin, eSystBG_R_only/TMath::Abs(A) );

         Abg_systematic->fAfter.SetBinContent( bin, eSystBG );
         Abg_systematic->fValues.SetBinContent(bin, A_bg );
         Abg_systematic->fUncertainties.SetBinContent(bin, eSystBG_Abg_only );
         Abg_systematic->fRelativeUncertainties.SetBinContent(bin, eSystBG_Abg_only/TMath::Abs(A) );
      }

      if(jj<4) {
         gr = new TGraph( fR_bg );
         mgW1->Add(gr,"l");

         gr = new TGraph( fC_bg );
         mgW2->Add(gr,"l");

         gr = new TGraph( fN_dis );
         mgW3->Add(gr,"l");
         gr = new TGraph( fN_pair );
         mgW3->Add(gr,"l");

         gr = new TGraph( fA_bg );
         mgW4->Add(gr,"l");

         gr = new TGraph( ff_bg );
         mgW5->Add(gr,"l");

         gr = new TGraph( fC_Abg );
         mgW6->Add(gr,"l");

      }

      std::string   filename1       = Form("A180_47_bg_sub_%d_%d.txt",aNumber,jj);
      std::string   table_filename1 = table_dir1 + filename1;
      std::ofstream outtable1(table_filename1.c_str());

      asym->CalculateBinMask_W();
      asym->PrintBinnedTableHead(outtable1);
      asym->PrintBinnedTable(outtable1);

   }

   TCanvas * ctemp = 0;
   TString gr_label = "";
   TCanvas * c = new TCanvas("bg_corrections_para47","bg_corrections_para47");
   c->Divide(3,2);

   c->cd(1);
   mg1->SetTitle("R(e+/e-)");
   mg1->Draw("a");
   mg1->GetYaxis()->SetRangeUser(0.0,1.0);
   //ctemp = new TCanvas();
   //gr_label = "R";
   //mg1->Draw("a");
   //ctemp->SaveAs(Form("results/asymmetries/bg_corrections_para47_%s_%d.png",gr_label.Data(),aNumber));
   //delete ctemp;


   c->cd(2);
   mg2->SetTitle("C_{bg}");
   mg2->Draw("a");
   mg2->GetYaxis()->SetRangeUser(1.0,2.0);
   //ctemp = new TCanvas();
   //gr_label = "C";
   //mg2->Draw("a");
   //ctemp->SaveAs(Form("results/asymmetries/bg_corrections_para47_%s_%d.png",gr_label.Data(),aNumber));
   //delete ctemp;

   c->cd(3);
   gPad->SetLogy(true);
   mg3->SetTitle("N_{pair} and N_{dis}");
   mg3->Draw("a");
   //ctemp = new TCanvas();
   //gr_label = "N";
   //mg3->Draw("a");
   //ctemp->SaveAs(Form("results/asymmetries/bg_corrections_para47_%s_%d.png",gr_label.Data(),aNumber));
   //delete ctemp;

   c->cd(4);
   mg4->SetTitle("A_{bg}");
   mg4->Draw("a");
   //ctemp = new TCanvas();
   //gr_label = "A";
   //mg4->Draw("a");
   //ctemp->SaveAs(Form("results/asymmetries/bg_corrections_para47_%s_%d.png",gr_label.Data(),aNumber));
   //delete ctemp;

   c->cd(5);
   mg5->SetTitle("1/f_{bg}");
   mg5->Draw("a");
   mg5->GetYaxis()->SetRangeUser(1.0,2.0);

   c->cd(6);
   mg6->SetTitle("C_{A_{bg}}");
   mg6->Draw("a");
   mg6->GetYaxis()->SetRangeUser(0.0,0.2);

   c->SaveAs(Form("results/asymmetries/%d/bg_corrections_para47_%d.png",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/bg_corrections_para47_%d.pdf",aNumber,aNumber));

   c = new TCanvas("bg_corrections_para47_W","bg_corrections_para47_W");
   c->Divide(3,2);

   c->cd(1);
   mgW1->SetTitle("R(e+/e-)");
   mgW1->Draw("a");
   mgW1->GetYaxis()->SetRangeUser(0.0,1.0);
   mgW1->GetXaxis()->SetLimits(1.0,3.0);


   c->cd(2);
   mgW2->SetTitle("C_{bg}");
   mgW2->Draw("a");
   mgW2->GetYaxis()->SetRangeUser(1.0,2.0);
   mgW2->GetXaxis()->SetLimits(1.0,3.0);
   gPad->Update();

   c->cd(3);
   gPad->SetLogy(true);
   mgW3->SetTitle("N_{pair} and N_{dis}");
   mgW3->Draw("a");
   mgW3->GetXaxis()->SetLimits(1.0,3.0);

   c->cd(4);
   mgW4->SetTitle("A_{bg}");
   mgW4->Draw("a");
   mgW4->GetXaxis()->SetLimits(1.0,3.0);

   c->cd(5);
   mgW5->SetTitle("1/f_{bg}");
   mgW5->Draw("a");
   mgW5->GetYaxis()->SetRangeUser(1.0,2.0);

   c->cd(6);
   mgW6->SetTitle("C_{A_{bg}}");
   mgW6->Draw("a");
   mgW6->GetYaxis()->SetRangeUser(0.0,0.2);


   c->SaveAs(Form("results/asymmetries/%d/bg_corrections_para47_W_%d.png",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/bg_corrections_para47_W_%d.pdf",aNumber,aNumber));

   TFile * f2 = TFile::Open(filename2,"RECREATE");
   f2->WriteObject(asym_write,Form("background-subtracted-asym_para47-%d",0));//,TObject::kSingleKey); 
   f2->WriteObject(BG_write,Form("background-corrections_para47-%d",0));//,TObject::kSingleKey); 
   f2->Close();
 
   return(0);
}
