Int_t MAID_pi0_photons(Double_t th_pi=35.0, Double_t theta_target = 80.0 ){

   gROOT->LoadMacro("background/pi0_photons.cxx"); 

   Double_t beamEnergy                   = 5.9; //GeV
   Double_t theta_pi                     = th_pi*degree;
   Double_t phi_pi                       = 0.0*degree;
   Double_t E_photon_min                 = 0.7;
   Double_t theta_photon_min             = 35*degree; 
   InSANENucleus::NucleusType TargetType = InSANENucleus::kProton; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3
   TVector3 pt(0.0,0.0,1.0);
   pt.SetMagThetaPhi(1.0,theta_target*degree,0.0);

   // For plotting cross sections 
   Double_t Emin     = 0.5;
   Double_t Emax     = 2.0;
   Int_t    npar     = 2;
   Int_t    Npx      = 10;

   TLegend * leg = new TLegend(0.1,0.1,0.3,0.3);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("MAID cross sections");
   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);

   // ---------------------------------------
   // MAID unpolarized
   MAIDInclusivePionDiffXSec *fDiffXSec = new MAIDInclusivePionDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->fXSec0->SetBeamEnergy(beamEnergy);
   fDiffXSec->fXSec0->SetTargetType(TargetType);
   fDiffXSec->fXSec0->SetHelicity(0.0);
   fDiffXSec->fXSec0->SetTargetPolarization(pt);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();
   TF1 * sigma0 = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0->SetParameter(0,theta_pi);
   sigma0->SetParameter(1,phi_pi);
   sigma0->SetNpx(Npx);
   sigma0->SetLineColor(1);
   //leg->AddEntry(sigma0,"unpolarized","l");


   //-------------------------------------------------------

   TString xAxisTitle = "#omega_{#pi} (GeV)"; 

   TCanvas * c       = new TCanvas("MAIDphotons","MAID pi0 photons");
   c->Divide(1,2);
   c->cd(1);
   gPad->SetLogy(true);

   TMultiGraph * mg1 = new TMultiGraph();
   TGraph * gr       = 0;

   TH1F * h0 = (TH1F*) sigma0->DrawCopy("goff")->GetHistogram();
   TH1F * h1 = (TH1F*) h0->Clone("photonAcceptance");
   TH1F * h2 = new TH1F(*h0);
   h2->Reset();

   for(int i=1; i<= h0->GetNbinsX(); i++){
      Double_t Ebin = h0->GetXaxis()->GetBinCenter(i);
      Double_t sig = h0->GetBinContent(i);
      Double_t weight = pi0_photons(theta_pi/degree,Ebin,theta_photon_min/degree,E_photon_min);
      h1->SetBinContent(i,sig*weight);
      h2->SetBinContent(i,weight);
   }


   gr = new TGraph(h0);
   gr->SetLineColor(1);
   mg1->Add(gr,"l");
   leg->AddEntry(gr,"Inclusive pi0 cross section, #sigma_{#pi^{0}}","l");

   gr = new TGraph(h1);
   gr->SetLineColor(2);
   mg1->Add(gr,"l");
   leg->AddEntry(gr,"Inclusive photons from #pi^{0} decay","l");

   mg1->Draw("a");
   mg1->GetXaxis()->SetTitle(xAxisTitle);
   mg1->GetXaxis()->CenterTitle(true);
   gPad->Update();
   leg->Draw();

   latex->DrawLatex(0.35,0.37,Form("E = %.2f GeV",beamEnergy));
   latex->DrawLatex(0.35,0.3,Form("#theta_{T} = %.0f deg",pt.Theta()/degree));
   latex->DrawLatex(0.35,0.23,Form("#theta_{#pi} = %.0f deg",th_pi));
   latex->DrawLatex(0.35,0.16,Form("E_{#gamma}^{min} = %.2f deg",E_photon_min));
   latex->DrawLatex(0.35,0.09,Form("#theta_{#gamma}^{min} = %.0f deg",theta_photon_min/degree));

   c->cd(2);

   gr = new TGraph(h2);
   gr->Draw("al");
   gr->GetXaxis()->SetTitle(xAxisTitle);
   gr->GetXaxis()->CenterTitle(true);
   gPad->Update();


   c->SaveAs(Form("results/background/MAID_pi0_photons_%d_%d_%d.png",int(beamEnergy*10.0),int(theta_target),int(th_pi)));
   c->SaveAs(Form("results/background/MAID_pi0_photons_%d_%d_%d.pdf",int(beamEnergy*10.0),int(theta_target),int(th_pi)));
   c->SaveAs(Form("results/background/MAID_pi0_photons_%d_%d_%d.tex",int(beamEnergy*10.0),int(theta_target),int(th_pi)));

   return 0;
}
