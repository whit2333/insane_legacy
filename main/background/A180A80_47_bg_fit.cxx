Int_t A180A80_47_bg_fit(Int_t paraRunGroup = 23,Int_t perpRunGroup = 23,Int_t aNumber=23){

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   const char * parafile = Form("data/binned_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/binned_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);

   std::cout << "DONE" << std::endl;

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TList * SystHists1 = new TList();
   TList * SystHists2 = new TList();


   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   TF1 * fit1 = new TF1("fit1","[0]+x*[1]",0.2,0.9);
   TF1 * fit2 = new TF1("fit2","[0]+x*[1]",0.2,0.9);
   TF1 * fit10 = new TF1("fit10","[0]",0.2,0.9);
   TF1 * fit20 = new TF1("fit20","[0]",0.2,0.9);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries()&& jj<5;jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();

      TH1F * fApara = paraAsym->fAsymmetryVsx;
      TH1F * fAperp = perpAsym->fAsymmetryVsx;

      InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsx; 

      paraAsym->fSystErrVsx->SetLineColor(fApara->GetLineColor());
      perpAsym->fSystErrVsx->SetLineColor(fApara->GetLineColor());

      SystHists1->Add(paraAsym->fSystErrVsx);
      SystHists2->Add(perpAsym->fSystErrVsx);

      //TF1 * fit1 = new TF1("fit1","[0]+x*[1]",0.25,0.9);
      //TF1 * fit2 = new TF1("fit2","[0]+x*[1]",0.25,0.9);


      fApara->Fit(fit1,"","goff");
      fAperp->Fit(fit2,"","goff");
      fApara->Fit(fit10,"","goff");
      fAperp->Fit(fit20,"","goff");

      TH1 * h1 = fApara; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      TH1 * h2 = fAperp; 
      gr2 = new TGraphErrors(h2);
      for( int j = gr2->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr2->RemovePoint(j);
         }
      }

      if(jj==4) {
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
         gr2->SetMarkerColor(1);
         gr2->SetMarkerStyle(24);
         gr2->SetLineColor(1);
      }
      //gr->SetMarkerStyle(20);
      //gr->SetMarkerColor(fA1->GetMarkerColor());
      //gr->SetLineColor(fA1->GetMarkerColor());
      if(jj<=4) {
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      //gr2->SetMarkerStyle(21);
      //gr2->SetMarkerColor(fA1->GetMarkerColor());
      //gr2->SetLineColor(fA1->GetMarkerColor());
      if(jj<=4) {
         mg2->Add(gr2,"p");
      }

   }

   Double_t Q2 = 4.0;

   // --------------------------------------------------------
   TCanvas * c = new TCanvas("cA180A80_47","A180 A80 47");
   c->Divide(1,2);

   // --------------- A1 ----------------------
   c->cd(1);

   // Load in world data on A1He3 from NucDB  
   gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   //TList * measurementsList = manager->GetMeasurements("A1p");
   //for (int i = 0; i < measurementsList->GetEntries(); i++) {
   //   NucDBMeasurement *A1He3World = (NucDBMeasurement*)measurementsList->At(i);
   //   // Get TGraphErrors object 
   //   TGraphErrors *graph        = A1He3World->BuildGraph("x");
   //   graph->SetMarkerStyle(27);
   //   // Set legend title 
   //   Title = Form("%s",A1He3World->GetExperimentName()); 
   //   leg->AddEntry(graph,Title,"lp");
   //   // Add to TMultiGraph 
   //   MG->Add(graph); 
   //}

   TString Measurement = Form("A_{180}");
   TString GTitle      = Form("Preliminary %s, E=4.7 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   //MG->Add(mg);
   // Draw everything 
   mg->Draw("AP");
   mg->SetTitle(GTitle);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle();
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle();
   mg->GetXaxis()->SetLimits(0.0,1.0); 
   mg->GetYaxis()->SetRangeUser(-0.2,0.8); 
   mg->Draw("AP");

   //for(int i=0;i<SystHists1->GetEntries();i++){
   //   TH1 * hsys2 = (TH1*)SystHists1->At(i);
   //   TGraph * gr_sys = new TGraph(hsys2);
   //   if(i<=4) {
   //      //hsys2->Draw("same");
   //      gr_sys->Draw("l");

   //   }
   //}
   //TLatex l;
   //l.SetTextSize(0.05);
   //l.SetTextAlign(12);  //centered
   //l.SetTextFont(132);
   //l.SetTextAngle(-15);
   //l.DrawLatex(0.05,0.2,"systematic errors #rightarrow");

   leg->Draw();


   fit1->Draw("same");
   fit10->SetLineColor(4);
   fit10->Draw("same");

   //--------------- A2 ---------------------
   c->cd(2);
   //TMultiGraph *MG2 = new TMultiGraph(); 
   //measurementsList = manager->GetMeasurements("A2p");
   //measurementsList->Print();
   //for (int i = 0; i < measurementsList->GetEntries(); i++) {
   //   NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
   //   // Get TGraphErrors object 
   //   TGraphErrors *graph        = A2pMeas->BuildGraph("x");
   //   graph->SetMarkerStyle(27);
   //   // Set legend title 
   //   Title = Form("%s",A2pMeas->GetExperimentName()); 
   //   //leg->AddEntry(graph,Title,"lp");
   //   // Add to TMultiGraph 
   //   MG2->Add(graph,"p"); 
   //}
   //mg2->Add(mg2,"p");
   mg2->Draw("a");
   mg2->SetTitle("Preliminary A_{80}^{p}, E=4.7 GeV");
   mg2->GetXaxis()->SetLimits(0.0,1.0); 
   mg2->GetYaxis()->SetRangeUser(-0.5,0.5); 
   mg2->GetYaxis()->SetTitle("A_{80}^{p}");
   mg2->GetXaxis()->SetTitle("x");
   mg2->GetXaxis()->CenterTitle();
   mg2->GetYaxis()->CenterTitle();
   //for(int i=0;i<SystHists2->GetEntries();i++){
   //   TH1 * hsys2 = (TH1*)SystHists2->At(i);
   //   TGraph * gr_sys = new TGraph(hsys2);
   //   if(i<=4) {
   //      //hsys2->Draw("same");
   //      gr_sys->Draw("l");
   //   }
   //}

   fit2->Draw("same");
   fit20->SetLineColor(4);
   fit20->Draw("same");

   fit1->Write();
   fit2->Write();

   c->Update();

   c->SaveAs(Form("results/asymmetries/A180A80_47_bg_fit_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A180A80_47_bg_fit_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/A180A80_47_bg_fit_%d.svg",aNumber));

   return(0);
}
