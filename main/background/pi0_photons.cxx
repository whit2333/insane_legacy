Double_t pi0_photons(Double_t theta_pi=25.0, Double_t E_pi=2.8, 
                     Double_t theta_min = 30.0, Double_t E_min = 1.00) {

   if (!gROOT->GetClass("TGenPhaseSpace")) gSystem->Load("libPhysics");

   Double_t    m         = M_pion/GeV;
   Double_t    p_pi      = TMath::Sqrt(E_pi*E_pi-m*m);

   TVector3 Pvec_pi(0,0,0); 
   Pvec_pi.SetMagThetaPhi(p_pi,theta_pi*degree,0.0);

   TLorentzVector pi0(Pvec_pi,E_pi);

   //(Momentum, Energy units are Gev/C, GeV)
   Double_t masses[3] = { 0.0, 0.0, 0.0} ;

   TGenPhaseSpace event;
   event.SetDecay(pi0, 2, masses);

   //TH2F *h1 = new TH2F("h1","h1;E1 (GeV);#theta1 (deg)", 100, 0, 3.5, 100, 10,  50);
   TH2F *h2 = new TH2F("h2","h2;#theta2 (deg);E2 (GeV)", 100, 10,  50, 100, 0,  3.5);
   //TH2F *h3 = new TH2F("h3","h3;#theta2 (deg) ;#theta1 (deg)", 100, 10, 50, 100, 10, 50);
   //TH2F *h4 = new TH2F("h4","h4;E1 (GeV); E2 (GeV)", 100, 0,  3.5, 100,  0,  3.5);

   for (Int_t n=0;n<100000;n++) {
      Double_t weight = event.Generate();

      TLorentzVector *pGamma1 = event.GetDecay(0);
      TLorentzVector *pGamma2 = event.GetDecay(1);

      //h1->Fill( pGamma1->E(), pGamma1->Vect().Theta()/degree ,weight);
      h2->Fill( pGamma2->Vect().Theta()/degree ,pGamma2->E(), weight);
      //h3->Fill( pGamma2->Vect().Theta()/degree , pGamma1->Vect().Theta()/degree ,weight);
      //h4->Fill(pGamma1->E(), pGamma2->E(),weight);
   }

   //--------------------------
   Int_t nxBins = 0;
   Int_t nyBins = 0;
   Int_t xBin   = 0;
   Int_t yBin   = 0;
   Int_t zBin   = 0;
   Int_t bin    = 0;

   bin = h2->FindBin(theta_min,E_min);
   h2->GetBinXYZ(bin,xBin,yBin,zBin);
   Double_t Total = h2->Integral();
   Double_t num   = h2->Integral(xBin,nxBins,yBin,nyBins);
   Double_t res   = num/Total;
   std::cout << 100*num/Total << " \% of events in range" << std::endl;
   return res;
}
