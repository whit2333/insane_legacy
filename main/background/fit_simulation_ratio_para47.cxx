Int_t fit_simulation_ratio_para47(Int_t aNum = 10){

   TFile * fout = new TFile("data/electron_pion_ratio.root","UPDATE");

   TH1F * h1 = (TH1F*)gROOT->FindObject("Replus_eminus_para47");
   if(!h1) return -10;

   TF1 *f1_R    = new TF1("RPlusMinus_para47","2.0*expo(0)/(1.0+2.0*expo(0))",0.5,3.0);
   TF1 *f1_r    = new TF1("rPlusMinus_para47","expo(0)",0.5,3.0);

   TCanvas * c1 = new TCanvas();

   h1->Fit(f1_R,"","goff");

   f1_r->SetParameter(0,f1_R->GetParameter(0));
   f1_r->SetParameter(1,f1_R->GetParameter(1));

   h1->SetLineWidth(2);
   h1->Draw("E1");
   h1->GetXaxis()->CenterTitle(true);
   h1->GetXaxis()->SetTitle("E (GeV)");
   h1->GetYaxis()->SetRangeUser(0.0,1.2);
   h1->SetTitle("R(e+/e-)");

   f1_r->SetLineColor(4);
   f1_r->DrawCopy("same");

   h1->Write();
   f1_R->Write();
   f1_r->Write();

   c1->SaveAs(Form("results/background/fit_simulation_ratio_para47_%d.png",aNum));

   return 0;
}
