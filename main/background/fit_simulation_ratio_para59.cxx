Int_t fit_simulation_ratio_para59(Int_t aNum = 10){

   TFile * fout = new TFile("data/electron_pion_ratio.root","UPDATE");
   fout->ls();

   TH1F * h1 = (TH1F*)gROOT->FindObject("Replus_eminus_para59");
   if(!h1) return -10;

   TF1 *f1    = new TF1("RPlusMinus_para59","2.0*expo(0)/(1.0+2.0*expo(0))",0.5,3.0);

   TCanvas * c1 = new TCanvas();

   h1->Fit(f1,"","goff");

   h1->SetLineWidth(2);
   h1->Draw("E1");
   h1->GetXaxis()->CenterTitle(true);
   h1->GetXaxis()->SetTitle("E (GeV)");
   h1->GetYaxis()->SetRangeUser(0.0,1.2);
   h1->SetTitle("R(e+/e-)");

   h1->Write();
   f1->Write();

   c1->SaveAs(Form("results/background/fit_simulation_ratio_para59_%d.png",aNum));

   return 0;
}
