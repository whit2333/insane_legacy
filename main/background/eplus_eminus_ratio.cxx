TF2 * fR_PlusMinus = 0;

TF2 * GetR_PlusMinus(){
   if(fR_PlusMinus) return fR_PlusMinus;
   fR_PlusMinus = new TF2("RPlusMinus","exp([0]+[1]*x+[2]*y+[3]*x*y)",20.0,60.0,0.5,2.0);
   fR_PlusMinus->SetParameter(0,-2.29);
   fR_PlusMinus->SetParameter(1,0.129);
   fR_PlusMinus->SetParameter(2,-0.438);
   fR_PlusMinus->SetParameter(3,-0.0832);
   return fR_PlusMinus;
}

Int_t eplus_eminus_ratio(){

   // From R.Fersch Thesis, Table 6.1
   // Formula uses GeV and degrees
   TF2 * RPlusMinus = new TF2("RPlusMinus","exp([0]+[1]*x+[2]*y+[3]*x*y)",20.0,60.0,0.5,2.0);
   RPlusMinus->SetParameter(0,-2.29);
   RPlusMinus->SetParameter(1,0.129);
   RPlusMinus->SetParameter(2,-0.438);
   RPlusMinus->SetParameter(3,-0.0832);
   Double_t E0 = 5.73;

   TF2 * RPlusMinus2 = new TF2("RPlusMinus2","exp([0]+[1]*x+[2]*y+[3]*x*y)",20.0,60.0,0.5,2.0);
   RPlusMinus2->SetParameter(0,-1.54);
   RPlusMinus2->SetParameter(1,0.0141);
   RPlusMinus2->SetParameter(2,-6.4);
   RPlusMinus2->SetParameter(3,-0.00492);
   Double_t E2 = 1.6;

   TF2 * RPlusMinus11 = new TF2("RPlusMinus11","exp([0]+[1]*x+[2]*y+[3]*x*y)",20.0,60.0,0.5,2.0);
   RPlusMinus11->SetParameter(0,-1.64);
   RPlusMinus11->SetParameter(1,0.0868);
   RPlusMinus11->SetParameter(2,-1.88);
   RPlusMinus11->SetParameter(3,-0.0538);
   Double_t E11 = 4.2;

   TCanvas * c = new TCanvas("eplus_eminus_ratio","eplus_eminus_ratio");
   c->Divide(1,2);

   c->cd(1);
   RPlusMinus->Draw("cont4Z");
   TH2* h2 = (TH2*) RPlusMinus->GetHistogram();


   c->cd(2);
   double theta = 35;
   int Npts = h2->GetYaxis()->GetNbins();

   TGraph * g   = new TGraph(Npts);
   TGraph * g2  = new TGraph(Npts);
   TGraph * g11 = new TGraph(Npts);

   for(int i = 0; i<Npts;i++){
      double Eprime = h2->GetYaxis()->GetBinCenter(i);
      g->SetPoint(i,Eprime,RPlusMinus->Eval(theta,Eprime));
      g2->SetPoint(i,Eprime,RPlusMinus2->Eval(theta,Eprime));
      g11->SetPoint(i,Eprime,RPlusMinus11->Eval(theta,Eprime));
   }

   TMultiGraph * mg = new TMultiGraph();

   g->SetLineColor(1);
   mg->Add(g,"l");

   g11->SetLineColor(2);
   mg->Add(g11,"l");

   g2->SetLineColor(4);
   mg->Add(g2,"l");

   mg->Draw("a");


   c->SaveAs("results/eplus_eminus_ratio.png");

   return 0;
}


