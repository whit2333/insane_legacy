/*! Simple BETA geometry using TGeo Classes.

 */

TGeoVolume *   sane_simple();
TGeoNode *     add_lucite_bar(Int_t barNumber );
void           clear_lucite_bars();
TGeoNode *     add_tracker_bar( Int_t chan );
void           clear_tracker_bars();
//_____________________________________________________________________________

   TGeoManager *geom = 0;
   /// Materials
   TGeoMaterial *matVacuum = 0;
   TGeoMaterial *matTargetVacuum = 0;
   TGeoMaterial *matTrackerFull = 0;
   TGeoMaterial *matTrackerBar = 0;
   TGeoMaterial *matLuciteFull = 0;
   TGeoMaterial *matLuciteBar = 0;
   TGeoMaterial *matBigcalFull = 0;
   TGeoMaterial *matBigcalBlock = 0;
   TGeoMaterial *matAl = 0;
   /// Mediums
   TGeoMedium *Vacuum =  0;
   TGeoMedium *TargetVacuum =  0;
   TGeoMedium *TrackerFullMed =  0;
   TGeoMedium *TrackerBarMed =  0;
   TGeoMedium *LuciteFullMed =  0;
   TGeoMedium *LuciteBarMed =  0;
   TGeoMedium *BigcalFullMed =  0;
   TGeoMedium *BigcalBlockMed =  0;
   /// Volumes
   TGeoVolume * gLuciteVol   = 0;
   TGeoVolume * gTrackerVol  = 0;

//_____________________________________________________________________________
TGeoVolume * sane_simple() {
   // gStyle->SetCanvasPreferGL(true);

   gSystem->Load("libGeom");
   geom = new TGeoManager("simple1", "Simple geometry");
   BIGCALGeometryCalculator * bcGeo = BIGCALGeometryCalculator::GetCalculator();
   
   //--- define some materials
   matVacuum                   = new TGeoMaterial("Vacuum", 0,0,0)                ; matVacuum->SetTransparency(1)        ;
   matTargetVacuum             = new TGeoMaterial("TargetVacuum", 0,0,0)          ; matTargetVacuum->SetTransparency(80) ;
   matTrackerFull              = new TGeoMaterial("TrackerFullMat", 26.98,13,2.7) ; matTrackerFull->SetTransparency(70)  ;
   matTrackerBar               = new TGeoMaterial("TrackerBarMat", 26.98,13,2.7)  ; matTrackerBar->SetTransparency(60)   ;
   matLuciteFull               = new TGeoMaterial("LuciteFullMat", 26.98,13,2.7)  ; matLuciteFull->SetTransparency(60)   ;
   matLuciteBar                = new TGeoMaterial("LuciteBarMat", 26.98,13,2.7)   ; matLuciteBar->SetTransparency(60)    ;
   matBigcalFull               = new TGeoMaterial("BigcalFullMat", 26.98,13,2.7)  ; matBigcalFull->SetTransparency(60)   ;
   matBigcalBlock              = new TGeoMaterial("BigcalBlockMat", 26.98,13,2.7) ;
   matAl                       = new TGeoMaterial("Al", 26.98,13,2.7)             ;
   //--- define some media
   Vacuum         = new TGeoMedium("Vacuum",1, matVacuum);
   TargetVacuum   = new TGeoMedium("Target Vacuum Material",2, matTargetVacuum);
   TrackerFullMed = new TGeoMedium("Tracker Frame Material",2, matTrackerFull);
   TrackerBarMed  = new TGeoMedium("Tracker Bar Material",2, matTrackerBar);
   LuciteFullMed  = new TGeoMedium("Lucite Full Material",2, matLuciteFull);
   LuciteBarMed   = new TGeoMedium("Lucite Bar Material",2, matLuciteBar);
   BigcalFullMed  = new TGeoMedium("Bigcal Full Material",2, matBigcalFull);
   BigcalBlockMed = new TGeoMedium("Bigcal Block Material",2, matBigcalBlock);
 
   //--- define the transformations
   // NOTE: TGeo Angles are in degrees!
   Double_t bigcalDistance           = bcGeo->fNominalRadialDistance +45.0/2.0; // bigcal position plus half its bar length
   Double_t luciteDistance           = 259.0-240.0; //cm
   Double_t betaAngle                = TMath::Pi()*40.0/180.0;
   Double_t betaAngle_deg            = 40.0;
   Double_t worldx                   = 1000.;
   Double_t worldy                   = 1000.;
   Double_t worldz                   = 1000.;
   Double_t trackerX                 = 22.0;
   Double_t trackerY                 = 40.0;

   TGeoTranslation      * bigcalTrans  = new TGeoTranslation(bigcalDistance*TMath::Sin(betaAngle), 0, bigcalDistance*TMath::Cos(betaAngle));
   TGeoTranslation      * luciteTrans  = new TGeoTranslation(luciteDistance*TMath::Sin(betaAngle), 0, luciteDistance*TMath::Cos(betaAngle));

   TGeoRotation        * betaRotation = new TGeoRotation("betaRotation");
   betaRotation->RotateY(betaAngle_deg);

   TGeoRotation      * targetRotation = new TGeoRotation("targetRotation");
   targetRotation->RotateX(-90.0);

   TGeoRotation        * luciteRotation = new TGeoRotation("luciteRotation");
   luciteRotation->RotateY(-50.0);

   TGeoCombiTrans * bigcalPositioning  = new TGeoCombiTrans( *bigcalTrans, *betaRotation);
   TGeoCombiTrans * targetFlip         = new TGeoCombiTrans(0,0,0, targetRotation);
   TGeoCombiTrans * lucitePositioning  = new TGeoCombiTrans( *luciteTrans, (*luciteRotation)*(*targetFlip));

   // ------------------------------------------
   // Make the top container volume
   TGeoVolume *top = geom->MakeBox("TOP", Vacuum, worldx, worldy,worldz);
   geom->SetTopVolume(top);

   // ------------------------------------------
   // Make the static volumes 
   // Target
   TGeoVolume * target =  geom->MakeTube("target", TargetVacuum , 0, 50.0, 50.0/2.0);
   target->SetLineColor(kRed);
   top->AddNode(target,1, targetFlip);

   TGeoVolume * targetcell =  geom->MakeTube("targetcell", TargetVacuum , 0, 1.50, 3.0/2.0);
   targetcell->SetLineColor(kRed+2);
   top->AddNode(targetcell,1);

   // ---------------
   // BigCal
   //TGeoVolume * bigcal =  geom->MakeBox("bigcal", BigcalFullMed , 110.0/2.0,220.0/2.0,40.0/2.0);
   TGeoVolume * bigcal =  geom->MakeBox("bigcal", BigcalFullMed , 122.0/2.0,230.0/2.0,45.0/2.0);
   bigcal->SetLineColor(kBlue-2);
   top->AddNode(bigcal,1, bigcalPositioning);

   // ---------------
   // Tracker
   ForwardTrackerGeometryCalculator * tgeocalc  = ForwardTrackerGeometryCalculator::GetCalculator();
   TVector3 activebboxdim   = tgeocalc->GetActiveBoundingBoxDimensions();
   TVector3 bboxdim         = tgeocalc->GetBoundingBoxDimensions();
   TVector3 pos             = tgeocalc->GetBoundingBoxOrigin();
   Double_t trackerDistance = pos.Z();

   TGeoTranslation * trackerTrans = 
      new TGeoTranslation( trackerDistance*TMath::Sin(betaAngle),
                           0, 
                           trackerDistance*TMath::Cos(betaAngle) );
   TGeoCombiTrans * trackerPositioning = new TGeoCombiTrans( *trackerTrans, *betaRotation);

   // Main tracker volume to place tracker "bars". This is not visible when nodes are added to it?
   gTrackerVol =  geom->MakeBox("tracker", TrackerFullMed ,bboxdim.X()/2.0,bboxdim.Y()/2.0,bboxdim.Z()/2.0 );
   gTrackerVol->SetLineColor(3);
   top->AddNode(gTrackerVol,1, trackerPositioning);
   /// This is a work around to make the volume above visible, ie, make a copy of it.
   TGeoVolume * tracker_vol =  geom->MakeBox("tracker2", TrackerFullMed ,bboxdim.X()/2.0,bboxdim.Y()/2.0,bboxdim.Z()/2.0 );
   tracker_vol->SetLineColor(3);
   top->AddNode(tracker_vol,1, trackerPositioning); 


   TGeoVolume * tracker_active_vol =  geom->MakeBox("tracker_active", TrackerFullMed ,
                                                    activebboxdim.X()/2.0,
                                                    activebboxdim.Y()/2.0,
                                                    activebboxdim.Z()/2.0 );
   tracker_active_vol->SetLineColor(7);
   top->AddNode(tracker_active_vol,1, trackerPositioning); 

   // ---------------
   // Lucite Hodoscope
   gLuciteVol = geom->MakeTubs("lucite", LuciteFullMed ,240.0,243,6.0*28.0/2.0,-11.5,11.5);
   gLuciteVol->SetLineColor(kBlue-2); 
   top->AddNode(gLuciteVol,1, lucitePositioning); 
/*   add_lucite_bar(5 );*/
//    gLuciteVol->SetVisContainers(true);

   //--- close the geometry
/*     geom->CloseGeometry();*/
   
   //--- draw the ROOT box.
   // by default the picture will appear in the standard ROOT TPad.
   //if you have activated the following line in system.rootrc, 
   //it will appear in the GL viewer
   //#Viewer3D.DefaultDrawOption:   ogl
   
/*  geom->SetVisLevel(4);*/
//   top->Draw();
/*  top->Draw("ogle");*/
   return(top);
}   
//_____________________________________________________________________________

TGeoNode * add_lucite_bar( Int_t barNumber ) {
   TGeoVolume * luciteBar = geom->MakeTubs( "luciteBar",
                                         LuciteBarMed,
                                         240.0, 243.0, 6.0/2.0, -11.5, 11.5);
   TGeoTranslation   *  luciteBarTrans = new TGeoTranslation(0,0, -6.0*28.0/2.0 - 6.0/2.0  + (double)barNumber*6.0  );
   if(gLuciteVol){
      gLuciteVol->AddNode(luciteBar,barNumber,luciteBarTrans);
      luciteBar->SetVisContainers(true);
   }
   Int_t nnodes = gLuciteVol->GetNdaughters();
/*   std::cout << " number of nodes " << nnodes << "\n";*/
   return( gLuciteVol->GetNode(nnodes-1) ) ;
}
//_____________________________________________________________________________

void         clear_lucite_bars() {
   Int_t nnodes = gLuciteVol->GetNdaughters();
   for(int i = 0;i<nnodes;i++){
      gLuciteVol->RemoveNode(   gLuciteVol->GetNode(i) );
   }
   //gLuciteVol->AddNode(luciteBar,barNumber,&luciteBarTrans);
}
//_____________________________________________________________________________

void         clear_tracker_bars() {
   Int_t nnodes = gTrackerVol->GetNdaughters();
   for(int i = 0;i<nnodes;i++){
      gTrackerVol->RemoveNode(   gTrackerVol->GetNode(i) );
   }
   //gLuciteVol->AddNode(luciteBar,barNumber,&luciteBarTrans);
}
//_____________________________________________________________________________

TGeoNode * add_tracker_bar( Int_t chan ) {
   ForwardTrackerGeometryCalculator * geocalc  = ForwardTrackerGeometryCalculator::GetCalculator();

   TVector3 bboxdim = geocalc->GetBoundingBoxDimensions(chan);
   TVector3 pos     = geocalc->GetPositioning(chan) - geocalc->GetBoundingBoxOrigin();

   TGeoVolume * trackerBar = geom->MakeBox( "trackerBar",
                                         TrackerBarMed,
                                         bboxdim.X()/2.0,bboxdim.Y()/2.0,bboxdim.Z()/2.0);
   TGeoTranslation   *  trackerBarTrans = new TGeoTranslation( pos.X(),pos.Y(),pos.Z() );
   if(gTrackerVol){
      gTrackerVol->AddNode(trackerBar,chan,trackerBarTrans);
      trackerBar->SetVisContainers(true);
   }
   Int_t nnodes = gTrackerVol->GetNdaughters();
/*   std::cout << " number of nodes " << nnodes << "\n";*/
   return( gTrackerVol->GetNode(nnodes-1) ) ;
}

