// Multi-view (3d, rphi, rhoz) service class using EVE Window Manager.
// Author: Matevz Tadel 2009

#include <TEveManager.h>

#include <TEveViewer.h>
#include <TGLViewer.h>
#include <TGLLightSet.h>
#include <TEveScene.h>

#include <TEveProjectionManager.h>
#include <TEveProjectionAxes.h>

#include <TEveBrowser.h>
#include <TEveWindow.h>

// MultiView
//
// Structure encapsulating standard views: 3D, r-phi and rho-z.
// Includes scenes and projection managers.
//
// Should be used in compiled mode.

struct MultiView
{
   TEveProjectionManager *fLuciteMgr;
   TEveProjectionManager *fRhoZMgr;

   TEveViewer            *f3DView;
   TEveViewer            *fLuciteView;
   TEveViewer            *fRhoZView;

   TEveScene             *fMain3DScene;
   TEveScene             *fLuciteGeomScene;
   TEveScene             *fRhoZGeomScene;
   TEveScene             *fLuciteEventScene;
   TEveScene             *fRhoZEventScene;

   //---------------------------------------------------------------------------

   MultiView()
   {
      // Constructor --- creates required scenes, projection managers
      // and GL viewers.

      // Scenes
      //========

	   

      fLuciteGeomScene  = gEve->SpawnNewScene("Lucite Geometry",
                                            "Scene holding projected geometry for the RPhi view.");
      fRhoZGeomScene  = gEve->SpawnNewScene("RhoZ Geometry",
                                            "Scene holding projected geometry for the RhoZ view.");
      fLuciteEventScene = gEve->SpawnNewScene("Lucite Event Data",
                                            "Scene holding projected event-data for the RPhi view.");
      fRhoZEventScene = gEve->SpawnNewScene("RhoZ Event Data",
                                            "Scene holding projected event-data for the RhoZ view.");


      // Projection managers
      //=====================

      fLuciteMgr = new TEveProjectionManager(TEveProjection::kPT_3D);
      gEve->AddToListTree(fLuciteMgr, kFALSE);
      {
         TEveProjectionAxes* a = new TEveProjectionAxes(fLuciteMgr);
         a->SetMainColor(kWhite);
         a->SetTitle("R-Phi");
         a->SetTitleSize(0.05);
         a->SetTitleFont(102);
         a->SetLabelSize(0.025);
         a->SetLabelFont(102);
         fLuciteGeomScene->AddElement(a);
      }

      fRhoZMgr = new TEveProjectionManager(TEveProjection::kPT_RhoZ);
      gEve->AddToListTree(fRhoZMgr, kFALSE);
      {
         TEveProjectionAxes* a = new TEveProjectionAxes(fRhoZMgr);
         a->SetMainColor(kWhite);
         a->SetTitle("Rho-Z");
         a->SetTitleSize(0.05);
         a->SetTitleFont(102);
         a->SetLabelSize(0.025);
         a->SetLabelFont(102);
         fRhoZGeomScene->AddElement(a);
      }


      // Viewers
      //=========

      TEveWindowSlot *slot = 0;
      TEveWindowPack *pack = 0;

      slot = TEveWindow::CreateWindowInTab(gEve->GetBrowser()->GetTabRight());
      pack = slot->MakePack();
      pack->SetElementName("Multi View");
      pack->SetHorizontal();
      pack->SetShowTitleBar(kFALSE);
      pack->NewSlot()->MakeCurrent();
      f3DView = gEve->SpawnNewViewer("3D View", "");
      f3DView->AddScene(gEve->GetGlobalScene());
      f3DView->AddScene(gEve->GetEventScene());

      pack = pack->NewSlot()->MakePack();
      pack->SetShowTitleBar(kFALSE);
      pack->NewSlot()->MakeCurrent();
      fLuciteView = gEve->SpawnNewViewer("RPhi View", "");
      fLuciteView->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOZ);
      Double_t pos[3]={100.0,0,100.0};
///      fLuciteView->GetGLViewer()->SetOrthoCamera(TGLViewer::kCameraOrthoXOZ.0,10.0,pos,0.017*30.0,0.017*30.0);
//      fLuciteView->GetGLViewer()->GetLightSet()->SetUseSpecular(true);
      fLuciteView->AddScene(fLuciteGeomScene);
      fLuciteView->AddScene(fLuciteEventScene);
      fLuciteView->AddScene(gEve->GetGlobalScene());

      pack->NewSlot()->MakeCurrent();
      fRhoZView = gEve->SpawnNewViewer("RhoZ View", "");
      fRhoZView->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
      fRhoZView->AddScene(fRhoZGeomScene);
      fRhoZView->AddScene(fRhoZEventScene);
   }

   //---------------------------------------------------------------------------

   void SetDepth(Float_t d)
   {
      // Set current depth on all projection managers.

      fLuciteMgr->SetCurrentDepth(d);
      fRhoZMgr->SetCurrentDepth(d);
   }

   //---------------------------------------------------------------------------

/*   void ImportEventMain3D(TEveElement* el)
   { 
     // fRPhiMgr->ImportElements(el, fRPhiEventScene);
   }

   void ImportEventMain3D(TEveElement* el)
   { 
     // fRhoZMgr->ImportElements(el, fRhoZEventScene);
   }
*/
   //
   void ImportGeomRPhi(TEveElement* el)
   { 
      fLuciteMgr->ImportElements(el, fLuciteGeomScene);
   }

   void ImportGeomRhoZ(TEveElement* el)
   { 
      fRhoZMgr->ImportElements(el, fRhoZGeomScene);
   }

   void ImportEventRPhi(TEveElement* el)
   { 
      fLuciteMgr->ImportElements(el, fLuciteEventScene);
   }

   void ImportEventRhoZ(TEveElement* el)
   { 
      fRhoZMgr->ImportElements(el, fRhoZEventScene);
   }

   //---------------------------------------------------------------------------

   void DestroyEventRPhi()
   {
      fLuciteEventScene->DestroyElements();
   }

   void DestroyEventRhoZ()
   {
      fRhoZEventScene->DestroyElements();
   }
};
