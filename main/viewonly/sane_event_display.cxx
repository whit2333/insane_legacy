/*!  BETA Event Display
     Author: Whitney Armstrong (whit@temple.edu)
 */

// Forward Declarations
class SANEMultiView;
class TEveYrProjection;

Int_t        load_lib();
void         init_histograms();
void         init_canvas();
void         init_tracking();
void         init_geometry();
void         make_gui();
void         clear_event();
void         load_event();
void         read_event();
void         save_event_png();
void         process_event_hist();
void         add_cer_marker(Double_t val, Int_t number = 0 , Int_t bins = 1);
void         add_luc_marker(Double_t val, Int_t number = 0 , Int_t bins = 1);
TEveTrack *  make_track(Int_t pdgcode, TVector3 * momentum, TVector3 * vertex);

/// Data
const char*  esd_geom_file_name                       = "SANEGeometry.root";
ANNDisElectronEvent * nnEvent                         = 0;

NNParaElectronCorrectionEnergy * nnParaElectronEnergy = 0;
NNParaElectronCorrectionTheta  * nnParaElectronTheta  = 0;
NNParaElectronCorrectionPhi    * nnParaElectronPhi    = 0;

NNParaPositronCorrectionEnergy * nnParaPositronEnergy = 0;
NNParaPositronCorrectionTheta  * nnParaPositronTheta  = 0;
NNParaPositronCorrectionPhi    * nnParaPositronPhi    = 0;

NNPerpElectronCorrectionEnergy * nnPerpElectronEnergy = 0;
NNPerpElectronCorrectionTheta  * nnPerpElectronTheta  = 0;
NNPerpElectronCorrectionPhi    * nnPerpElectronPhi    = 0;

TFile *beta_file          = 0;
TFile *beta_friends_file  = 0;
TTree *main_tree          = 0;
TTree *detector_tree      = 0;
TTree *reconstructed_tree = 0;
TTree *trajectories_tree  = 0;

InSANETrajectory * aTraj               = 0;
InSANEHitPosition * aHitPos            = 0;
InSANEReconstructedEvent * fReconEvent = new InSANEReconstructedEvent();
TClonesArray * trajectories            = fReconEvent->fTrajectories;//new TClonesArray("InSANETrajectory",1);
TClonesArray * positions               = fReconEvent->fLucitePositions;//new TClonesArray("InSANEHitPosition",1);
TClonesArray * trackerPositions        = fReconEvent->fTrackerPositions;//new TClonesArray("InSANEHitPosition",1);

TGraph * gCerMarker = 0;
TGraph * gLucMarker = 0;

Int_t    fgRunNumber        = 0;
Int_t    fgEventNumber      = 0;
Int_t    fgIsPerp           = false;
Int_t    fgElectronPDGcode  = 11;
Int_t    event_id           = 7; // Current event id.
Int_t    run_id             = 0; // Current event id.

Int_t    fgPictureNumber = 0;

Bool_t   gSkipHists         = false;
TList *  gBigcalHists       = 0;
TList *  gCerHists          = 0;
TList *  gLuciteHists       = 0;
TList *  gTrackerHists      = 0;

TEveElementList *     gHitPoints     = 0;
TEveElementList *     gBoxSets       = 0;
TEveTrackList *       gTrackList     = 0;
TEvePointSet  *       gPointSet      = 0;
TEvePointSet  *       gTrackerPointSet   = 0;
TEvePointSet  *       gTrackerY1PointSet = 0;
TEvePointSet  *       gTrackerY2PointSet = 0;

TEveGeoShape *        gGeomGentle    = 0;
TEveStraightLineSet*  gLineSet       = 0;
TEveBoxSet *          gBoxSet        = 0;
TEveBoxSet *          gCerBoxSet     = 0;
TEveBoxSet *          gLuciteBoxSet  = 0;
TEveElementList *     gLuciteBarSet  = 0;
TEveTrackPropagator*  gPropagator    = 0;
TEveMagField *        gMagField      = 0;
TGFileBrowser *       gHistBrowser   = 0;

TCanvas *             gHistCanvas    = 0;
TCanvas *             gTrackerHistCanvas    = 0;
TCanvas *             gLuciteHistCanvas     = 0;
TCanvas *             gCherenkovHistCanvas  = 0;
TCanvas *             gBigcalHistCanvas     = 0;

BIGCALGeometryCalculator *       gBigcalGeoCalc       = 0;
GasCherenkovGeometryCalculator * gGasCherenkovGeoCalc = 0;

ForwardTrackerDetector *         gTrackerDetector     = 0;

SANEEvents * detector_events          = 0;
Pi0Event *         pi0_event          = 0;
Pi03ClusterEvent * pi0_3cluster_event = 0;
SANEMultiView* gMultiView = 0;

//______________________________________________________________________________
void sane_event_display(Int_t runNumber  = 73040) {

   /// Load libraries
   if (load_lib() != 0) exit - 1;

   /// Set Run, files
   fgRunNumber = runNumber;
   rman->SetRun(fgRunNumber);
   beta_file = rman->GetCurrentFile();
   if (!beta_file) exit - 2;

   /// Detectors
   gTrackerDetector = (ForwardTrackerDetector*) rman->GetCurrentRun()->fDetectors->FindObject("tracker");
   if (gTrackerDetector == 0) std::cout << " NO TRACKER FOUND!!!!!!!!!!!!!!!!!!!!!\n";

   /// Detector tree
   detector_events = new SANEEvents("betaDetectors1");
   if (detector_events->fTree) {
      detector_events->SetClusterBranches(detector_events->fTree);
      detector_tree = detector_events->fTree;
      detector_tree->BuildIndex("fRunNumber", "fEventNumber");
   }
   /// Trajectory tree
   trajectories_tree = (TTree*) beta_file->Get("trackingPositions");
   if (trajectories_tree) {
      trajectories_tree->SetBranchAddress("uncorrelatedPositions", &fReconEvent);
      trajectories_tree->BuildIndex("fRunNumber", "fEventNumber");
   }
   main_tree = trajectories_tree;
   //main_tree = detector_tree;
   if (detector_tree && main_tree != detector_tree) main_tree->AddFriend(detector_tree);
   if (trajectories_tree && main_tree != trajectories_tree) main_tree->AddFriend(trajectories_tree);

   /// EVE
   TEveManager::Create();

   // Standard multi-view
   gMultiView = new SANEMultiView;

   // HTML summary view
   gROOT->LoadMacro("viewonly/alice_esd_html_summary.C");

   fgHtmlSummary       = new HtmlSummary("SANE Event Display Summary Table");
   slot                = TEveWindow::CreateWindowInTab(gEve->GetBrowser()->GetTabRight());
   fgHtml              = new TGHtml(0, 100, 100);
   TEveWindowFrame *wf = slot->MakeFrame(fgHtml);
   fgHtml->MapSubwindows();
   wf->SetElementName("Summary");

   /// Initialize
   init_geometry();
   if (!gSkipHists)init_histograms();
   init_tracking();

   // Build the GUI
   make_gui();

   // Load the first event.
   load_event();

   // Draw
   gEve->Redraw3D(kTRUE); // Reset camera after the first event has been shown.
   //gEve->GetDefaultGLViewer()->SetClearColor(0);

}
//______________________________________________________________________________
void init_geometry() {

   // Geometry Calculators
   gBigcalGeoCalc = BIGCALGeometryCalculator::GetCalculator();
   gGasCherenkovGeoCalc = GasCherenkovGeometryCalculator::GetCalculator();
   // Load Geometry
   Float_t length = 100.0;
   //TFile* geom = TFile::Open(esd_geom_file_name, "CACHEREAD");
   TGeoVolume * thetop = sane_simple();
   thetop->SetAsTopVolume();
   TGeoNode* node1 = gGeoManager->GetTopNode();
   TEveGeoTopNode* its = new TEveGeoTopNode(gGeoManager, node1);
   gEve->AddGlobalElement(its);

   TGeoVolume * trackerVol =  gGeoManager->GetTopVolume()->FindNode("tracker_1")->GetVolume();
   TEveGeoShape * trackerShape = new TEveGeoShape("tracker_shape");
   trackerShape->SetShape(trackerVol->GetShape());

   TEveGeoShapeExtract* gse = new TEveGeoShapeExtract() ;
   gse->SetShape(node1->GetVolume()->GetShape());
   gGeomGentle = TEveGeoShape::ImportShapeExtract(gse, 0);

   //gMultiView->ImportGeomBETASide(trackerShape);

   /// Coordinate Systems
   BigcalCoordinateSystem * bigcalc = new BigcalCoordinateSystem();
   //bigcalc->DrawCoordinateSystem("Human");
   HumanCoordinateSystem * humanCoords = new HumanCoordinateSystem();
   //humanCoords->DrawCoordinateSystem();

}
//______________________________________________________________________________
void init_histograms() {
   // Histogram left hand browser
   gEve->GetBrowser()->StartEmbedding(0);
   gHistBrowser = gEve->GetBrowser()->MakeFileBrowser();
   gEve->GetBrowser()->StopEmbedding("BETA Hists");
   gEve->GetBrowser()->GetTabRight()->SetTab(1);
   TH1F* h;

   gBigcalHists = new TList;
   gBigcalHists->SetName("BigCal");
   gHistBrowser->Add(gBigcalHists);

   gCerHists = new TList;
   gCerHists->SetName("Cherenkov");
   gHistBrowser->Add(gCerHists);

   gLuciteHists = new TList;
   gLuciteHists->SetName("Lucite");
   gHistBrowser->Add(gLuciteHists);

   gTrackerHists = new TList;
   gTrackerHists->SetName("Tracker");
   gHistBrowser->Add(gTrackerHists);

   /// --- Add some macros.
   TMacro* m;
   m = new TMacro;
   m->AddLine("{ gHistCanvas->Clear();"
              "  gHistCanvas->cd();"
              "  gHistCanvas->Update(); }");
   m->SetName("Clear Canvas");
   gHistBrowser->Add(m);
   m = new TMacro;
   m->AddLine("{ gHistCanvas->Clear();"
              "  gHistCanvas->Divide(2,2);"
              "  gHistCanvas->cd(1);"
              "  gHistCanvas->Update(); }");
   m->SetName("Split Canvas");
   gHistBrowser->Add(m);

   /// Create BETA embedded canvas
   gEve->GetBrowser()->StartEmbedding(1);
   gROOT->ProcessLineFast("gHistCanvas = new TCanvas()");
   //gHistCanvas = (TCanvas*) gPad;
   gEve->GetBrowser()->StopEmbedding("BETA Canvas");
   init_canvas();

   /// Create Tracker embedded canvas
   gEve->GetBrowser()->StartEmbedding(1);
   gROOT->ProcessLineFast("gTrackerHistCanvas = new TCanvas()");
   gEve->GetBrowser()->StopEmbedding("Tracker");
   tracker_event_init(detector_events, true, gTrackerHistCanvas);
   gTrackerHists->Add(trackerHistograms);

   gEve->GetBrowser()->StartEmbedding(1); // embed to the right (1)
   gROOT->ProcessLineFast("gLuciteHistCanvas = new TCanvas()");
   gEve->GetBrowser()->StopEmbedding("Lucite");
   lucite_event_init(detector_events, true, gLuciteHistCanvas);
   gLuciteHists->Add(luciteHistograms);

   gEve->GetBrowser()->StartEmbedding(1); // embed to the right (1)
   gROOT->ProcessLineFast("gCherenkovHistCanvas = new TCanvas()");
   gEve->GetBrowser()->StopEmbedding("Cherenkov");
//    lucite_event_init(detector_events,true,gLuciteHistCanvas);
//    gLuciteHists->Add(luciteHistograms);

   gEve->GetBrowser()->StartEmbedding(1); // embed to the right (1)
   gROOT->ProcessLineFast("gBigcalHistCanvas = new TCanvas()");
   gEve->GetBrowser()->StopEmbedding("Bigcal");

//    lucite_event_init(detector_events,true,gLuciteHistCanvas);
//    gLuciteHists->Add(luciteHistograms);

}
//______________________________________________________________________________
void init_tracking() {

   /// Load NNs  Are these needed?!?
   if (fgRunNumber < 72900) fgIsPerp = true;
   else fgIsPerp = false;
   nnEvent = new ANNDisElectronEvent();

   /// Positron NN reconstruction.
   nnParaPositronEnergy = new NNParaPositronCorrectionEnergy();
   nnParaPositronTheta = new NNParaPositronCorrectionTheta();
   nnParaPositronPhi = new NNParaPositronCorrectionPhi();

   nnParaElectronEnergy = new NNParaElectronCorrectionEnergy();
   nnParaElectronTheta = new NNParaElectronCorrectionTheta();
   nnParaElectronPhi = new NNParaElectronCorrectionPhi();

   nnPerpElectronEnergy = new NNPerpElectronCorrectionEnergy();
   nnPerpElectronTheta = new NNPerpElectronCorrectionTheta();
   nnPerpElectronPhi = new NNPerpElectronCorrectionPhi();

   //nnPerpPositronEnergy = new NNPerpElectronCorrectionEnergy();
   //nnPerpPositronTheta = new NNPerpElectronCorrectionTheta();
   //nnPerpPositronPhi = new NNPerpElectronCorrectionPhi();
   /// Create the magnetic field used
//    //gMagField =  new UVAEveMagField();
//    //    TEveTrack * track = make_track(gPropagator, 1);
}
//______________________________________________________________________________
void init_canvas() {

   TTree * t = trajectories_tree; //(TTree*)gROOT->FindObject("betaDetectors2");
   TH1 * h1 = 0;
   TH2 * h2 = 0;

   gHistCanvas->Divide(3, 2);
   if (1) {
      gHistCanvas->cd(1);
      if (t) t->Draw("fTrajectories.fNCherenkovElectrons>>hCerNPE(200,-1.0,4.0)", "", "");
      h1 = (TH1*)gROOT->FindObject("hCerNPE");
      gCerHists->Add(h1);

      gHistCanvas->cd(2);
      if (t) t->Draw("fClosestLuciteMissDistance>>luciteMiss(200,0,200)", "", "");
      h1 = (TH1*)gROOT->FindObject("luciteMiss");
      gLuciteHists->Add(h1);

      gHistCanvas->cd(4);
      if (t) t->Draw("fClosestLuciteMissDistance:fTrajectories.fNCherenkovElectrons>>lucMissVsCer(200,-1.0,4.0,200,0,200)", "", "colz");
      h1 = (TH1*)gROOT->FindObject("lucMissVsCer");
      gLuciteHists->Add(h1);

      gHistCanvas->cd(3);


//        t->Draw("fForwardTrackerEvent.fTrackerPositionHits.fPositionVector.fY:fForwardTrackerEvent.fTrackerPositionHits.fPositionVector.fX>>hTrackerXY1","","colz");
//        h2 = (TH2*)gROOT->FindObject("hTrackerXY1");
//        gTrackerHists->Add(h2);
//
//        gHistCanvas->cd(3);
//        t->Draw("fForwardTrackerEvent.fTrackerPositionHits.fPositionVector2.fY:fForwardTrackerEvent.fTrackerPositionHits.fPositionVector2.fX>>hTrackerXY2","","colz");
//        h2 = (TH2*)gROOT->FindObject("hTrackerXY2");
//        gTrackerHists->Add(h2);
//
//        gHistCanvas->cd(4);
//        t->Draw("fLuciteHodoscopeEvent.fLucitePositionHits.fPositionVector.fY:fLuciteHodoscopeEvent.fLucitePositionHits.fPositionVector.fX>>hLuciteXY","","colz");
//        h2 = (TH2*)gROOT->FindObject("hLuciteXY");
//        gLuciteHists->Add(h2);
//
//
//        gHistCanvas->cd(5);
//        t->Draw("fBigcalClusters.fYMoment:fBigcalClusters.fXMoment>>hBigcalXY","","colz");
//        h2 = (TH2*)gROOT->FindObject("hBigcalXY");
//        gBigcalHists->Add(h2);

   }
}
//______________________________________________________________________________

Int_t load_lib()
{
   const TString weh("load_lib");
   if (gROOT->LoadMacro("viewonly/sane_event_projections.cxx") != 0) {
      Error(weh, "Failed loading MultiView.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("viewonly/sane_simple.C") != 0) {
      Error(weh, "Failed loading sane_simple.C in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("detectors/tracker/tracker_event_display1.cxx") != 0) {
      Error(weh, "Failed loading tracker_event_display1.cxx in compiled mode.");
      return -1;
   }
   if (gROOT->LoadMacro("detectors/hodoscope/lucite_event_display1.cxx") != 0) {
      Error(weh, "Failed loading lucite_event_display1.cxx in compiled mode.");
      return -1;
   }
   return(0);
}
//______________________________________________________________________________

void clear_event() {
   // Clearing the last event
   gEve->GetViewers()->DeleteAnnotations();

   if (!gSkipHists)tracker_event_clear();
   if (!gSkipHists)lucite_event_clear();

   if (gCerBoxSet) {
      gCerBoxSet->DestroyElements();
      gCerBoxSet->Destroy();
      gCerBoxSet = 0;
   }

   if (gLuciteBoxSet) {
      gLuciteBoxSet->DestroyElements();
      gLuciteBoxSet->Destroy();
      gLuciteBoxSet = 0;
   }

   if (gBoxSets) {
      gBoxSets->DestroyElements();
      gBoxSets->Destroy();
      gBoxSets = 0;
      gBoxSet = 0;
   }

   if (gLuciteBarSet) {
      gLuciteBarSet->DestroyElements();
      gLuciteBarSet->Destroy();
      gLuciteBarSet = 0;
   }

   clear_lucite_bars();

   if (gHitPoints) {
      gHitPoints->DestroyElements();
      if (gTrackerPointSet) gTrackerPointSet = 0;
      if (gTrackerY1PointSet) gTrackerY1PointSet = 0;
      if (gTrackerY2PointSet) gTrackerY2PointSet = 0;
      if (gPointSet) gPointSet = 0;
   }
   clear_tracker_bars();


   if (gLineSet) {
      gLineSet->DestroyElements();
      gLineSet->Destroy();
      gLineSet = 0;
   }
   if (gTrackList) {
      gTrackList->DestroyElements();
   }
   /// DONE clearing EVE

}
//______________________________________________________________________________
void load_event() {

   clear_event();

   run_id = fgRunNumber;
   main_tree->GetEntry(event_id);

   Int_t eventnumber = fReconEvent->fEventNumber;
   fgEventNumber     = fReconEvent->fEventNumber;
   fgPictureNumber   = 0;

   printf("Loading entry number %d ", event_id);
   printf("corresponding to event %d of run %d.\n", eventnumber, run_id);

   if (reconstructed_tree) if (reconstructed_tree != main_tree)
         reconstructed_tree->GetEntryWithIndex(run_id, eventnumber);
   if (detector_tree) if (detector_tree != main_tree)
         detector_tree->GetEntryWithIndex(run_id, eventnumber);
   if (trajectories_tree) if (trajectories_tree != main_tree)
         trajectories_tree->GetEntryWithIndex(run_id, eventnumber);

   read_event();

   TEveElement* top = gEve->GetCurrentEvent();
   gMultiView->DestroyEventBETASide();
   gMultiView->ImportEventBETASide(top);

   /*
      gMultiView->DestroyEventRPhi();
      gMultiView->ImportEventRPhi(top);

      gMultiView->DestroyEventRhoZ();
      gMultiView->ImportEventRhoZ(top);
   */
   update_html_summary();
//   if(gPointSet) gPointSet->SetRnrSelf(true);
   gEve->Redraw3D(kFALSE, kTRUE);
   gEve->GetEventScene()->Repaint();
   gEve->GetDefaultGLViewer()->UpdateScene();
}
//______________________________________________________________________________
void  process_event_hist() {

   tracker_event_process(detector_events);
   tracker_event_update();

   lucite_event_process(detector_events);
   lucite_event_update();
}
//______________________________________________________________________________
/** Event navigation buttons.
 *
 */
class EvNavHandler {
public:
   void Fwd() {
      if (event_id < main_tree->GetEntries() - 1) {
         ++event_id;
         load_event();
      } else {
         printf("Already at last event.\n");
      }
   }
   void Bck() {
      if (event_id > 0) {
         --event_id;
         load_event();
      } else {
         printf("Already at first event.\n");
      }
   }

   void SaveImage(){
      //gEve->GetDefaultGLViewer()->UpdateScene();
      gEve->GetDefaultGLViewer()->SavePicture(Form("plots/%d/event_%d_%d.png",fgRunNumber,fgEventNumber,fgPictureNumber));
      fgPictureNumber++;
   }

   void PrintInfo() {
      for (int k = 0; k < detector_events->CLUSTER->fClusters->GetEntries(); k++) {
         /*         aTraj = (InSANETrajectory*)(*trajectories)[k];*/
         /*      aTraj->fPosition0.Print();*/
         BIGCALCluster * clust = (BIGCALCluster*)(*detector_events->CLUSTER->fClusters)[k];
         clust->Dump();
         for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++) printf("block i=%d,j=%d,E=%f \n", i, j, clust->fBlockEnergies[i][j]);
      }
   }
};
//______________________________________________________________________________
void make_gui() {
   // Create minimal GUI for event navigation.
   TEveBrowser* browser = gEve->GetBrowser();
   browser->StartEmbedding(TRootBrowser::kLeft);

   TGMainFrame* frmMain = new TGMainFrame(gClient->GetRoot(), 1000, 600);
   frmMain->SetWindowName("XX GUI");
   frmMain->SetCleanup(kDeepCleanup);

   TGHorizontalFrame* hf = new TGHorizontalFrame(frmMain);
   {
      TString icondir(Form("%s/icons/", gSystem->Getenv("ROOTSYS")));
      TGPictureButton* b = 0;
      EvNavHandler    *fh = new EvNavHandler;

      b = new TGPictureButton(hf, gClient->GetPicture(icondir + "GoBack.gif"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "EvNavHandler", fh, "Bck()");

      b = new TGPictureButton(hf, gClient->GetPicture(icondir + "GoForward.gif"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "EvNavHandler", fh, "Fwd()");

      b = new TGPictureButton(hf, gClient->GetPicture(icondir + "bld_new.xpm"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "EvNavHandler", fh, "PrintInfo()");

      b = new TGPictureButton(hf, gClient->GetPicture(icondir + "save.xpm"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "EvNavHandler", fh, "SaveImage()");
   }
   frmMain->AddFrame(hf);

   frmMain->MapSubwindows();
   frmMain->Resize();
   frmMain->MapWindow();

   browser->StopEmbedding();
   browser->SetTabTitle("Event Control", 0);
}
//______________________________________________________________________________
void read_event() {

   // Does the work of adding all the event information

   InSANERun * saneRun = rman->GetCurrentRun();

   if (gBoxSets == 0) {
      gBoxSets = new TEveElementList("BigcalBoxSets");
      gEve->AddElement(gBoxSets);
   }

   if (gLuciteBarSet == 0) {
      gLuciteBarSet = new TEveElementList("LuciteBarSet");
      gEve->AddElement(gLuciteBarSet);
   }

   if (gLuciteBoxSet == 0) {
      gLuciteBoxSet = new TEveBoxSet("luciteBoxSet");
      gLuciteBoxSet->SetDefDepth(96.0); /// incorrect dimensions
      gLuciteBoxSet->SetDefHeight(6.0);  /// incorrect dimensions
      gLuciteBoxSet->SetDefWidth(3.0);
      gLuciteBoxSet->InitMainTrans(true);
      gLuciteBoxSet->UseSingleColor();
      gLuciteBoxSet->SetMainColor(kYellow + 3);
      gLuciteBoxSet->SetMainTransparency(50.0);
      gLuciteBoxSet->Reset(TEveBoxSet::kBT_FreeBox, kTRUE, 64);
      TEveTrans& tl = gLuciteBoxSet->RefMainTrans();
      tl.SetRotByAngles(0.0, -40.0 * TMath::Pi() / 180.0, 0.0);
//       tl.SetPos( (-3.0/2.0),//*TMath::Sin(40.0*TMath::Pi()/180.0),
//                  (-6.0/2.0),
//                  (-96.0/2.0)    );//*TMath::Cos(40.0*TMath::Pi()/180.0));
      gEve->AddElement(gLuciteBoxSet);
   }


   if (gCerBoxSet == 0) {
      gCerBoxSet = new TEveBoxSet("cerBoxSet");
      gCerBoxSet->SetDefDepth(1.0); /// incorrect dimensions
      gCerBoxSet->SetDefHeight(35.0);  /// incorrect dimensions
      gCerBoxSet->SetDefWidth(35.0);
      gCerBoxSet->UseSingleColor();
      gCerBoxSet->SetMainColor(kRed - 3);
      gCerBoxSet->SetMainTransparency(50.0);
      gCerBoxSet->Reset(TEveBoxSet::kBT_AABoxFixedDim, kTRUE, 64);
      TEveTrans& t1 = gCerBoxSet->RefMainTrans();
      t1.SetRotByAngles(0.0, -40.0 * TMath::Pi() / 180.0, 0.0);
//       t1.SetPos( 190.0*TMath::Sin(50.0*TMath::Pi()/180.0),
//                 0.0 ,
//                 190.0*TMath::Cos(50.0*TMath::Pi()/180.0));
      gEve->AddElement(gCerBoxSet);
   }

   if (gHitPoints == 0) {
      gHitPoints = new TEveElementList("HitPoints");
      gEve->AddElement(gHitPoints);
   }

   if (gLineSet == 0) {
      gLineSet = new TEveStraightLineSet();
      gLineSet->SetMarkerSize(1.0);
      gLineSet->SetMarkerStyle(3);
      gLineSet->SetLineWidth(1);
      gLineSet->SetMarkerColor(3);
      gLineSet->SetLineColor(3);
      gEve->AddElement(gLineSet);
   }

   if (gPointSet == 0) {
      gPointSet = new TEvePointSet();
      gPointSet->SetOwnIds(kTRUE);
      gPointSet->SetMarkerColor(7);
      gPointSet->SetMarkerSize(1.5);
      gPointSet->SetMarkerStyle(4);
      gEve->AddElement(gPointSet);
      gHitPoints->AddElement(gPointSet);
   }

   if (gTrackerPointSet == 0) {
      gTrackerPointSet = new TEvePointSet();
      gTrackerPointSet->SetOwnIds(kTRUE);
      gTrackerPointSet->SetMarkerColor(5);
      gTrackerPointSet->SetMarkerSize(1.0);
      gTrackerPointSet->SetMarkerStyle(20);
      gEve->AddElement(gTrackerPointSet);
      gHitPoints->AddElement(gTrackerPointSet);
   }
   if (gTrackerY1PointSet == 0) {
      gTrackerY1PointSet = new TEvePointSet();
      gTrackerY1PointSet->SetOwnIds(kTRUE);
      gTrackerY1PointSet->SetMarkerColor(2);
      gTrackerY1PointSet->SetMarkerSize(0.8);
      gTrackerY1PointSet->SetMarkerStyle(20);
      gEve->AddElement(gTrackerY1PointSet);
      gHitPoints->AddElement(gTrackerY1PointSet);
   }
   if (gTrackerY2PointSet == 0) {
      gTrackerY2PointSet = new TEvePointSet();
      gTrackerY2PointSet->SetOwnIds(kTRUE);
      gTrackerY2PointSet->SetMarkerColor(3);
      gTrackerY2PointSet->SetMarkerSize(0.8);
      gTrackerY2PointSet->SetMarkerStyle(20);
      gEve->AddElement(gTrackerY2PointSet);
      gHitPoints->AddElement(gTrackerY2PointSet);
   }
   if (gTrackList == 0) {
      gTrackList = new TEveTrackList("Propagated tracks");
      if (gPropagator) delete gPropagator;
      gPropagator = gTrackList->GetPropagator();
      gPropagator->SetMaxR(30000.0);
      gPropagator->SetStepper(TEveTrackPropagator::kRungeKutta);
      if (!gMagField) {
         gMagField = new UVAEveMagField();//new TEveMagFieldConst(0., -10.0, 0.0);
         if (fgIsPerp)((UVAEveMagField*)gMagField)->SetPolarizationAngle(TMath::Pi() * 80.0 / 180.0);
         else ((UVAEveMagField*)gMagField)->SetPolarizationAngle(TMath::Pi());
         //((UVAEveMagField*)gMagField)->SetScaleFactor(-1.0 ); /// Reverse the the field for EVE bug instead of particle.
      }
      gPropagator->SetMagFieldObj(gMagField, false);
//       gPropagator->SetRnrReferences(kTRUE);
//       gPropagator->SetRnrDaughters(kTRUE);
//       gPropagator->SetRnrDecay(kTRUE);
//       gPropagator->SetRnrCluster2Ds(kTRUE);
//       gPropagator->SetFitReferences(kFALSE);
//       gPropagator->RefPMAtt().SetMarkerStyle(4);
      gTrackList->SetMainColor(6);
      gTrackList->SetMarkerColor(kYellow);
      gTrackList->SetMarkerStyle(20);
      gTrackList->SetMarkerSize(0.5);
      gEve->AddElement(gTrackList);
   }

   if (detector_tree) detector_events->TRIG->PrintSummary();

   Double_t electronInputs[9];
   Double_t positronInputs[9];



   /// SHould go in process_event_hist()
   if (!gSkipHists)add_luc_marker(fReconEvent->fClosestLuciteMissDistance);
   if (!gSkipHists)add_cer_marker(-1.0);


   TEveTrack *track = 0;
   Double_t origin[3] = {0.0, 0.0, 0.0};
   Double_t aPoint[3] = {0.0, 0.0, 0.0};
   std::cout << " ____________________________________________________" << "\n";
   std::cout << " Number of trajectores: " << trajectories->GetEntries() << "\n";


   TVector3 targetPos(0,0,0);
   if (fReconEvent->fTargetPositions->GetEntries() > 0) 
      targetPos = ((InSANEHitPosition*)(*(fReconEvent->fTargetPositions))[0])->fPosition;
   targetPos.GetXYZ(origin);

   /// First add straight lines
   for (int k = 0; k < trajectories->GetEntries(); k++) {
      aTraj = (InSANETrajectory*)(*trajectories)[k];
      aTraj->fPosition0.GetXYZ(aPoint);
      if (!gSkipHists) add_cer_marker(aTraj->fNCherenkovElectrons, k, trajectories->GetEntries());
      gLineSet->AddLine(TEveVector(origin), TEveVector(aPoint));
   }
   //   TEveTrackPropagator* trkProp = gTrackList->GetPropagator();
   //   trkProp->SetMagField( 0.1 * esdrun->fMagneticField ); // kGaus to Tesla

   /// Next create a track for each cluster
   std::cout << " Number of Clusters: " << detector_events->CLUSTER->fClusters->GetEntries() << "\n";
   for (int k = 0; k < detector_events->CLUSTER->fClusters->GetEntries(); k++) {
      BIGCALCluster * clust = (BIGCALCluster*)(*detector_events->CLUSTER->fClusters)[k];

      //------------------------------------
      // Make "e-" track for this cluster
      TVector3 momentum(0, 0, 0);
      momentum.SetMagThetaPhi((clust->GetEnergy() + clust->fDeltaE) / 1000.0, clust->GetTheta() + clust->fDeltaTheta, clust->GetPhi() + clust->fDeltaPhi);
      TVector3 vertex(targetPos);/// TODO add raster info
      TEveRecTrackD *rc = new TEveRecTrackD();
      rc->fV.Set(vertex.X(), vertex.Y(), vertex.Z());
      rc->fP.Set(momentum.X(), momentum.Y(), momentum.Z());
      rc->fSign = 1;
      track = new TEveTrack(rc, gPropagator);
      //track  = make_track(fgElectronPDGcode, &momentum, &vertex) ;
      gTrackList->AddElement(track);
      /// Add markers from tracker data
      for (int i = 0; i < trackerPositions->GetEntries(); i++) {
         aHitPos = (InSANEHitPosition*)(*trackerPositions)[i];
         //          track->AddPathMark(TEvePathMarkD(TEvePathMarkD::kReference,
         //                       TEveVectorD(aHitPos->fPosition.X(),aHitPos->fPosition.Y(),aHitPos->fPosition.Z()),
         //                                    TEveVectorD(momentum.X(),momentum.Y() ,momentum.Z()  )) ) ;
      }
      track->SetLineWidth(2);
      track->SetLineColor(2);
      track->MakeTrack();

      //------------------------------------
      // Make "e+" track for this cluster
      TVector3 momentum(0, 0, 0);
      nnEvent->SetEventValues(clust);
      nnEvent->GetCorrectionMLPInputNeuronArray(electronInputs);
      // Determine the positron NN output
      Double_t positron_fDeltaE     = nnParaPositronEnergy->Value(0, electronInputs);
      Double_t positron_fDeltaTheta = nnParaPositronTheta->Value(0, electronInputs);
      Double_t positron_fDeltaPhi   = nnParaPositronPhi->Value(0, electronInputs);

      momentum.SetMagThetaPhi((clust->GetEnergy() + positron_fDeltaE) / 1000.0,
                               clust->GetTheta() + positron_fDeltaTheta,
                               clust->GetPhi()   + positron_fDeltaPhi);
      TVector3 vertex(targetPos);/// TODO add raster info
      TEveRecTrackD *rc = new TEveRecTrackD();
      rc->fV.Set(vertex.X(), vertex.Y(), vertex.Z());
      rc->fP.Set(momentum.X(), momentum.Y(), momentum.Z());
      rc->fSign = -1;
      track = new TEveTrack(rc, gPropagator);
      //track  = make_track(fgElectronPDGcode, &momentum, &vertex) ;
      gTrackList->AddElement(track);
      track->SetLineWidth(2);
      track->SetLineColor(4);
      track->MakeTrack();

      //------------------------------------
      // Loop over blocks in cluster
      for (int ix = 0; ix < 5; ix++) {
         for (int iy = 0; iy < 5; iy++) {
            int ib = clust->fiPeak - 2 + ix;
            int jb = clust->fjPeak - 2 + iy;
            if (ib > 0 && jb > 0) if ((jb < 57 && jb > 32 && ib < 31) || (jb < 33  && ib < 33)) {
               /// Hack that creates a box set for each block....
               gBoxSet = new TEveBoxSet(Form("BigcalBlocks%d%d", ib, jb));
               gBoxSet->Reset(TEveBoxSet::kBT_FreeBox, kTRUE, 64);
               gBoxSet->InitMainTrans(true);
               gBoxSet->UseSingleColor();
               gBoxSet->SetMainColor(kYellow + 2);
               gBoxSet->SetMainTransparency(100.0 - 100.0*TMath::Sqrt(clust->fBlockEnergies[ix][iy] / clust->fBlockEnergies[2][2]));
               TEveTrans& t = gBoxSet->RefMainTrans();
               t.SetRotByAngles(0.0, -40.0 * TMath::Pi() / 180.0, 0.0);
               /*               t.SetPos(360.0*TMath::Sin(40.0*TMath::Pi()/180.0),0.0,360.0*TMath::Cos(40.0*TMath::Pi()/180.0));*/
               gBoxSets->AddElement(gBoxSet);

               Float_t x  = gBigcalGeoCalc->GetBlockXij_BCCoords(ib, jb);
               Float_t yy = gBigcalGeoCalc->GetBlockYij_BCCoords(ib, jb);
               Float_t z  = gBigcalGeoCalc->fNominalRadialDistance + gBigcalGeoCalc->GetBlockLength(clust->fiPeak,clust->fjPeak)/2.0;
               // Box half lengths
               Float_t a = 3.0 / 2.0;
               Float_t b = 3.0 / 2.0;
               Float_t c = 40.0 / 2.0;
               Float_t verts[24] = {
                  x - a , yy - b , z - c ,
                  x - a , yy + b , z - c ,
                  x + a , yy + b , z - c ,
                  x + a , yy - b , z - c ,
                  x - a , yy - b , z + c ,
                  x - a , yy + b , z + c ,
                  x + a , yy + b , z + c ,
                  x + a , yy - b , z + c
               };
               gBoxSet->AddBox(verts);
               gBoxSet->RefitPlex();

               //                gBoxSet->AddBox(0.0,gBigcalGeoCalc->GetBlockYij_BCCoords(ib,jb),gBigcalGeoCalc->GetBlockXij_BCCoords(ib,jb));
               //                gBoxSet->RefitPlex();
            }
         }
      } // End loop over cluster's blocks

      //--------------------------------
      // Cherenkov Mirror Hits
      if (clust->fGoodCherenkovTDCHit) {
         // Crude positioning for display purposes only
         gCerBoxSet->AddBox(gGasCherenkovGeoCalc->GetXPosition(clust->fPrimaryMirror) - 35.0 / 2.0,
               gGasCherenkovGeoCalc->GetYPosition(clust->fPrimaryMirror) - 35.0 / 2.0,
               200.0);
         gCerBoxSet->RefitPlex();
      }
   } // End loop over clusters

   /// Lucite Hodoscope box shaped bars.
   for (int it = 0; it < detector_events->BETA->fLuciteHodoscopeEvent->fLucitePositionHits->GetEntries(); it++) {
      LuciteHodoscopePositionHit * aPosHit = (LuciteHodoscopePositionHit*)(*(detector_events->BETA->fLuciteHodoscopeEvent->fLucitePositionHits))[it];

      Float_t x = 0.0;
      Float_t yy = aPosHit->fPositionVector.Y();
      Float_t z = 250.0;
      // Box half lengths
      Float_t a = 96.0 / 2.0;
      Float_t b = 6.0 / 2.0;
      Float_t c = 3.0 / 2.0;
      Float_t verts[24] = {
         x - a , yy - b , z - c ,
         x - a , yy + b , z - c ,
         x + a , yy + b , z - c ,
         x + a , yy - b , z - c ,
         x - a , yy - b , z + c ,
         x - a , yy + b , z + c ,
         x + a , yy + b , z + c ,
         x + a , yy - b , z + c
      };
//       gLuciteBoxSet->AddBox(verts);
//       gLuciteBoxSet->RefitPlex();

      TGeoNode * gnode = add_lucite_bar(aPosHit->fBarNumber) ;
      TEveGeoNode * aNode = new TEveGeoNode(gnode);
      if (gLuciteBarSet) gLuciteBarSet->AddElement(aNode);

   }

   /*   std::cout << " ____________________________________________________" << "\n";*/
   std::cout << " Number of positions  " << positions->GetEntries() << "\n";
   for (int i = 0; i < positions->GetEntries(); i++) {
      aHitPos = (InSANEHitPosition*)(*positions)[i];
      /*      aHitPos->fPosition.Print();*/
      gPointSet->SetNextPoint(aHitPos->fPosition.X(), aHitPos->fPosition.Y(), aHitPos->fPosition.Z());
      gPointSet->SetPointId(new TNamed(Form("Point %d", i), ""));
   }

   std::cout << " Number of tracker positions  " << trackerPositions->GetEntries() << "\n";
   for (int i = 0; i < trackerPositions->GetEntries(); i++) {
      aHitPos = (InSANEHitPosition*)(*trackerPositions)[i];
      /*      aHitPos->fPosition.Print();*/
      gTrackerPointSet->SetNextPoint(aHitPos->fPosition.X(), aHitPos->fPosition.Y(), aHitPos->fPosition.Z());
      gTrackerPointSet->SetPointId(new TNamed(Form("trackerPoint %d", i), ""));
   }


   /// Tracker Y1 Positions
   TClonesArray * ft1 = detector_events->BETA->fForwardTrackerEvent->fTrackerY1PositionHits;
   for (int i = 0; i < ft1->GetEntries(); i++) {
      ForwardTrackerPositionHit* ftPosHit = (ForwardTrackerPositionHit*)(*ft1)[i];
      TVector3 trackerPos = gTrackerDetector->GetPosition(ftPosHit);
      gTrackerY1PointSet->SetNextPoint(trackerPos.X(), trackerPos.Y(),trackerPos.Z());
      gTrackerY1PointSet->SetPointId(new TNamed(Form("trackerY1Point %d", i), ""));
   }
   /// Tracker Y2 Positions
   TClonesArray * ft2 = detector_events->BETA->fForwardTrackerEvent->fTrackerY2PositionHits;
   for (int i = 0; i < ft2->GetEntries(); i++) {
      ForwardTrackerPositionHit* ftPosHit = (ForwardTrackerPositionHit*)(*ft2)[i];
      TVector3 trackerPos = gTrackerDetector->GetPosition(ftPosHit);
      gTrackerY2PointSet->SetNextPoint(trackerPos.X(), trackerPos.Y(),trackerPos.Z());
      gTrackerY2PointSet->SetPointId(new TNamed(Form("trackerY2Point %d", i), ""));
   }

   /// Tracker scints Positions
   TClonesArray * trkhits = detector_events->BETA->fForwardTrackerEvent->fTrackerHits;
   for (int i = 0; i < trkhits->GetEntries(); i++) {
      ForwardTrackerHit* ftHit = (ForwardTrackerHit*)(*trkhits)[i];

      add_tracker_bar(ftHit->fChannel);

   }

   std::cout << " ____________________________________________________" << "\n";

   gTrackList->MakeTracks();

   if (!gSkipHists) process_event_hist();


}
//______________________________________________________________________________

void add_luc_marker(Double_t val, Int_t number, Int_t bins)
{
   gHistCanvas->cd(2);
   if (!gLucMarker) gLucMarker = new TGraph(bins);
   if (number == 0) {
      delete gLucMarker;
      gLucMarker = new TGraph(bins);
   }
   TH1F * h1 = (TH1F*)gLuciteHists->At(0);
   Double_t yval = 1.0;
   if (h1) yval = h1->GetBinContent(h1->FindBin(val));
   /*   std::cout << " Cer Hist " << h1 << " has y val " << yval << " for a value " << val << "\n";*/
   gLucMarker->SetPoint(number, val, yval);
   gLucMarker->SetMarkerSize(1.3);
   gLucMarker->SetMarkerColor(2001);
   gLucMarker->SetMarkerStyle(21);
   /*   if(h1)h1->Draw();*/
   gLucMarker->Draw("P,same");
   gHistCanvas->Update();
}
//______________________________________________________________________________

void add_cer_marker(Double_t val, Int_t number, Int_t bins)
{
   gHistCanvas->cd(1);
   if (!gCerMarker) gCerMarker = new TGraph(bins);
   if (number == 0) {
      delete gCerMarker;
      gCerMarker = new TGraph(bins);
   }
   TH1F * h1 = (TH1F*)gCerHists->At(0);
   Double_t yval = 1.0;
   if (h1) yval = h1->GetBinContent(h1->FindBin(val));
   std::cout << " Cer Hist " << h1 << " has y val " << yval << " for a value " << val << "\n";
   gCerMarker->SetPoint(number, val, yval);
   gCerMarker->SetMarkerSize(1.3);
   gCerMarker->SetMarkerColor(2000);
   gCerMarker->SetMarkerStyle(21);
   /*   if(h1)h1->Draw();*/
   gCerMarker->Draw("P,same");
   gHistCanvas->Update();
}
//______________________________________________________________________________

TEveTrack* make_track(Int_t pdgcode, TVector3 * momentum, TVector3 * vertex)
{
   TParticle * part = new TParticle();
   part->SetPdgCode(pdgcode);
   Double_t mass = part->GetPDG()->Mass();
   Double_t Energy = TMath::Sqrt(momentum->Mag2() + mass * mass);
   part->SetMomentum(momentum->X(), momentum->Y(), momentum->Z(), Energy);
   part->SetProductionVertex(vertex->X(), vertex->Y(), vertex->Z(), 0);

   TEveTrack* track = new TEveTrack(part, 0, gPropagator);
   track->SetName(Form("Charge %d", pdgcode));
   track->SetLineColor(2);
   track->SetLineWidth(3);
   // daughter 0
//   TEvePathMarkD* pm1 = new TEvePathMarkD(TEvePathMarkD::kDaughter);
//   pm1->fV.Set(1.479084, -4.370661, 3.119761);
//   track->AddPathMark(*pm1);
//   // daughter 1
//   TEvePathMarkD* pm2 = new TEvePathMarkD(TEvePathMarkD::kDaughter);
//   pm2->fV.Set(57.72345, -89.77011, -9.783746);
//   track->AddPathMark(*pm2);

   return track;
}
//______________________________________________________________________________

