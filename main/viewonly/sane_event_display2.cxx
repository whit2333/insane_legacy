/*!  BETA Event Display 
     Author: Whitney Armstrong (whit@temple.edu)

   

 */

/// Forward Declarations
class InSANEReconstructedEvent;
class InSANEHitPositions;
class InSANETrajectory;
class BIGCALCluster;
class BIGCALGeometryCalculator;
class GasCherenkovGeometryCalculator;
class SANEEvents;
class Pi0Event;
class Pi03ClusterEvent;


class NNParaElectronCorrectionEnergy;
class NNParaElectronCorrectionTheta;
class NNParaElectronCorrectionPhi;

class NNPerpElectronCorrectionEnergy;
class NNPerpElectronCorrectionTheta;
class NNPerpElectronCorrectionPhi;

class  ANNDisElectronEvent;

ANNDisElectronEvent * nnEvent =0;

NNParaElectronCorrectionEnergy * nnParaElectronEnergy = 0;
NNParaElectronCorrectionTheta  * nnParaElectronTheta = 0;
NNParaElectronCorrectionPhi    * nnParaElectronPhi = 0;

NNPerpElectronCorrectionEnergy * nnPerpElectronEnergy = 0;
NNPerpElectronCorrectionTheta  * nnPerpElectronTheta = 0;
NNPerpElectronCorrectionPhi    * nnPerpElectronPhi = 0;

Bool_t     sane_loadlib( const char* project);
void       make_gui();
void       load_event();
void       init_canvas();
void       read_event();
void       init_tracking();
TEveTrack* make_track(Int_t pdgcode, TVector3 * momentum, TVector3 * vertex);

const char* esd_geom_file_name = "SANEGeometry.root";

TFile *beta_file          = 0;
TFile *beta_friends_file  = 0;

TTree *main_tree          = 0;
TTree *detector_tree      = 0;
TTree *reconstructed_tree = 0;
TTree *trajectories_tree = 0;

InSANETrajectory * aTraj = 0;
InSANEHitPosition * aHitPos    = 0;
InSANEReconstructedEvent * fReconEvent = new InSANEReconstructedEvent();
TClonesArray * trajectories = fReconEvent->fTrajectories;//new TClonesArray("InSANETrajectory",1);
TClonesArray * positions = fReconEvent->fLucitePositions;//new TClonesArray("InSANEHitPosition",1);
TClonesArray * trackerPositions = fReconEvent->fTrackerPositions;//new TClonesArray("InSANEHitPosition",1);

Int_t fgRunNumber = 73040;
Int_t fgEventNumber =0;

Int_t fgIsPerp = false;
Int_t fgElectronPDGcode = -11; /// because of a bug in the EVE code 

Int_t event_id       = 0; // Current event id.
Int_t run_id       = 0; // Current event id.

TList * gBigcalHists = 0;
TList * gCerHists = 0;
TList * gLuciteHists = 0;
TList * gTrackerHists = 0;

TEveElementList * gHitPoints = 0;
TEveElementList * gBoxSets = 0;

TEveTrackList *gTrackList = 0;
TEvePointSet  *gPointSet  = 0;
TEvePointSet  *gTrackerPointSet = 0;
TEveGeoShape *gGeomGentle = 0;
TEveStraightLineSet* gLineSet = 0;//new TEveStraightLineSet();
TEveBoxSet*   gBoxSet = 0;
TEveBoxSet*   gCerBoxSet = 0;
TEveTrackPropagator* gPropagator = 0;
TEveMagField * gMagField =  0;
TGFileBrowser *gHistBrowser = 0;
TCanvas       *gHistCanvas  = 0;

BIGCALGeometryCalculator * gBigcalGeoCalc = 0; // BIGCALGeometryCalculator::GetCalculator();
GasCherenkovGeometryCalculator * gGasCherenkovGeoCalc = 0;// BIGCALGeometryCalculator::GetCalculator();
SANEEvents * detector_events = 0;

Pi0Event *         pi0_event          = 0;
Pi03ClusterEvent * pi0_3cluster_event = 0;


// Implemented in MultiView.C
class MultiView;
MultiView* gMultiView = 0;

//______________________________________________________________________________
void sane_event_display2()
{

   const TString weh("sane_event_display()");

   if (gROOT->LoadMacro("viewonly/MultiView.C+") != 0)
   {
      Error(weh, "Failed loading MultiView.C in compiled mode.");
      return;
   }
   if (gROOT->LoadMacro("viewonly/sane_simple.C") != 0)
   {
      Error(weh, "Failed loading MultiView.C in compiled mode.");
      return;
   }

   /// hard coding for first draft
   rman->SetRun(fgRunNumber);
   beta_file = rman->fCurrentFile;//TFile::Open(beta_file_name, "CACHEREAD");
   if (!beta_file)
      return;

   trajectories_tree = (TTree*) beta_file->Get("trackingPositions");
   if(trajectories_tree) {
      trajectories_tree->SetBranchAddress("fReconEvent",&fReconEvent);
      trajectories_tree->BuildIndex("fRunNumber","fEventNumber");
/*      if(main_tree)main_tree->AddFriend(detector_tree);*/
   }

   detector_events = new SANEEvents("betaDetectors1");
   if(detector_events->fTree) {
      detector_tree = detector_events->fTree;
      detector_tree->BuildIndex("fRunNumber","fEventNumber");
/*      if(main_tree)main_tree->AddFriend(detector_tree);*/
   }

//    reconstructed_tree = (TTree*) gROOT->FindObject("pi0results");
//    if(reconstructed_tree) {
//       pi0_event = new Pi0Event();
//       reconstructed_tree->SetBranchAddress("pi0reconstruction",&pi0_event);
//       reconstructed_tree->BuildIndex("fRunNumber","fEventNumber");
//    }
   reconstructed_tree = (TTree*) gROOT->FindObject("3clusterPi0results");
   if(reconstructed_tree) {
      pi0_3cluster_event = new Pi03ClusterEvent();
      reconstructed_tree->SetBranchAddress("pi03ClusterEvents",&pi0_3cluster_event);
      reconstructed_tree->BuildIndex("fRunNumber","fEventNumber");
/*      if(main_tree)main_tree->AddFriend(detector_tree);*/
   }
   main_tree = reconstructed_tree;

   if(detector_tree && main_tree != detector_tree) main_tree->AddFriend(detector_tree);
   if(reconstructed_tree && main_tree != reconstructed_tree) main_tree->AddFriend(reconstructed_tree);
   if(trajectories_tree && main_tree != trajectories_tree) main_tree->AddFriend(trajectories_tree);

   if(fgRunNumber < 72900) fgIsPerp = true;
   else fgIsPerp=false;


   nnEvent = new ANNDisElectronEvent();

   nnParaElectronEnergy = new NNParaElectronCorrectionEnergy();
   nnParaElectronTheta = new NNParaElectronCorrectionTheta();
   nnParaElectronPhi = new NNParaElectronCorrectionPhi();

   nnPerpElectronEnergy = new NNPerpElectronCorrectionEnergy();
   nnPerpElectronTheta = new NNPerpElectronCorrectionTheta();
   nnPerpElectronPhi = new NNPerpElectronCorrectionPhi();

/*   TTree * pitree = (TTree*) gROOT->FindObject("pi0results");
   TTree * pi3tree = (TTree*) gROOT->FindObject("3clusterPi0results");*/
//    detector_tree1 = (TTree*)gROOT->FindObject("betaDetectors2");


   TEveManager::Create();

   gBigcalGeoCalc = BIGCALGeometryCalculator::GetCalculator();
   gGasCherenkovGeoCalc = GasCherenkovGeometryCalculator::GetCalculator();

   Float_t length = 100.0;
   TFile* geom = TFile::Open(esd_geom_file_name, "CACHEREAD");
   TGeoVolume * thetop = sane_simple();
   thetop->SetAsTopVolume();
   TGeoNode* node1 = gGeoManager->GetTopNode();
   TEveGeoTopNode* its = new TEveGeoTopNode(gGeoManager, node1);
   gEve->AddGlobalElement(its);
   //gGeomGentle = new TEveGeoTopNode(geomgr, node1);
   //gEve->AddGlobalElement(gGeomGentle);
   TEveGeoShapeExtract* gse = new TEveGeoShapeExtract();
   gse->SetShape(thetop->GetShape());
   gGeomGentle = TEveGeoShape::ImportShapeExtract(gse, 0);
   //gGeomGentle = new TEveGeoShape("SANEGeometry");
   //gGeomGentle->SetShape(humanBETA->GetShape());
   //gEve->AddGlobalElement(gGeomGentle);
   //gGeomGentle = its;


   BigcalCoordinateSystem * bigcalc = new BigcalCoordinateSystem();
   bigcalc->DrawCoordinateSystem("Human");
   HumanCoordinateSystem * humanCoords = new HumanCoordinateSystem();
//    humanCoords->DrawCoordinateSystem();


   rman->fCurrentFile->cd();

   // Standard multi-view
   //=====================
//   gMultiView = new MultiView;
//   gMultiView->ImportGeomRPhi(gGeomGentle);
//   gMultiView->ImportGeomRhoZ(gGeomGentle);


   // HTML summary view
   //===================

   gROOT->LoadMacro("viewonly/alice_esd_html_summary.C");
   fgHtmlSummary = new HtmlSummary("SANE Event Display Summary Table");
   slot = TEveWindow::CreateWindowInTab(gEve->GetBrowser()->GetTabRight());
   fgHtml = new TGHtml(0, 100, 100);
   TEveWindowFrame *wf = slot->MakeFrame(fgHtml);
   fgHtml->MapSubwindows();
   wf->SetElementName("Summary");


   // Histograms
   //=============
    gEve->GetBrowser()->StartEmbedding(0);
    gHistBrowser = gEve->GetBrowser()->MakeFileBrowser();
    gEve->GetBrowser()->StopEmbedding("BETA Hists");gEve->GetBrowser()->GetTabRight()->SetTab(1);
    TH1F* h;

    gBigcalHists = new TList;
    gBigcalHists->SetName("BigCal");
    h = new TH1F("Foo", "Bar", 51, 0, 1);
    for (Int_t i=0; i<500; ++i)
       h->Fill(gRandom->Gaus(.63, .2));
    gBigcalHists->Add(h);
    gHistBrowser->Add(gBigcalHists);

    gCerHists = new TList;
    gCerHists->SetName("Cherenkov");
    h = new TH1F("Foo", "Bar", 51, 0, 1);
    for (Int_t i=0; i<500; ++i)
       h->Fill(gRandom->Gaus(.63, .2));
    gCerHists->Add(h);
    gHistBrowser->Add(gCerHists);

    gLuciteHists = new TList;
    gLuciteHists->SetName("Lucite");
    h = new TH1F("Foo", "Bar", 51, 0, 1);
    for (Int_t i=0; i<500; ++i)
       h->Fill(gRandom->Gaus(.63, .2));
    gLuciteHists->Add(h);
    gHistBrowser->Add(gLuciteHists);

    gTrackerHists = new TList;
    gTrackerHists->SetName("Tracker");
    h = new TH1F("Foo", "Bar", 51, 0, 1);
    for (Int_t i=0; i<500; ++i)
       h->Fill(gRandom->Gaus(.63, .2));
    gTrackerHists->Add(h);
    gHistBrowser->Add(gTrackerHists);

//     TFolder* f = new TFolder("Booboayes", "Statisticos");
//     h = new TH1F("Fooes", "Baros", 51, 0, 1);
//     for (Int_t i=0; i<2000; ++i) {
//        h->Fill(gRandom->Gaus(.7, .1));
//        h->Fill(gRandom->Gaus(.3, .1));
//     }
//     f->Add(h);
//     gHistBrowser->Add(f);
//     h = new TH1F("Fooesoto", "Barosana", 51, 0, 1);
//     for (Int_t i=0; i<4000; ++i) {
//        h->Fill(gRandom->Gaus(.25, .02), 0.04);
//        h->Fill(gRandom->Gaus(.5, .1));
//        h->Fill(gRandom->Gaus(.75, .02), 0.04);
//     }
//     gHistBrowser->Add(h);

   // --- Add some macros.

    TMacro* m;

    m = new TMacro;
    m->AddLine("{ gHistCanvas->Clear();"
               "  gHistCanvas->cd();"
               "  gHistCanvas->Update(); }");
    m->SetName("Clear Canvas");
    gHistBrowser->Add(m);

    m = new TMacro;
    m->AddLine("{ gHistCanvas->Clear();"
               "  gHistCanvas->Divide(2,2);"
               "  gHistCanvas->cd(1);"
               "  gHistCanvas->Update(); }");
    m->SetName("Split Canvas");
    gHistBrowser->Add(m);

    // --- Create an embedded canvas

   gEve->GetBrowser()->StartEmbedding(1);
   gROOT->ProcessLineFast("gHistCanvas = new TCanvas()");
   //gHistCanvas = (TCanvas*) gPad;
   gEve->GetBrowser()->StopEmbedding("BETA Canvas");

   make_gui();

   init_canvas();

   init_tracking();

   load_event();

   gEve->Redraw3D(kTRUE); // Reset camera after the first event has been shown.

}
//______________________________________________________________________________

void init_tracking()
{

   /// Create the magnetic field used
   //gMagField =  new UVAEveMagField();

//    TEveTrack * track = make_track(gPropagator, 1);

}

//______________________________________________________________________________
TEveTrack* make_track(Int_t pdgcode, TVector3 * momentum, TVector3 * vertex)
{

  TParticle * part = new TParticle();
  part->SetPdgCode(pdgcode);
  Double_t mass = part->GetPDG()->Mass();
  Double_t Energy = TMath::Sqrt(momentum->Mag2() + mass*mass);
  part->SetMomentum(momentum->X(),momentum->Y(),momentum->Z(),Energy);
  part->SetProductionVertex(vertex->X(),vertex->Y(),vertex->Z(),0);

  TEveTrack* track = new TEveTrack(part, 0, gPropagator);
  track->SetName(Form("Charge %d", pdgcode));
  track->SetLineColor(2);
  track->SetLineWidth(3);
  // daughter 0
//   TEvePathMarkD* pm1 = new TEvePathMarkD(TEvePathMarkD::kDaughter);
//   pm1->fV.Set(1.479084, -4.370661, 3.119761);
//   track->AddPathMark(*pm1);
//   // daughter 1
//   TEvePathMarkD* pm2 = new TEvePathMarkD(TEvePathMarkD::kDaughter);
//   pm2->fV.Set(57.72345, -89.77011, -9.783746);
//   track->AddPathMark(*pm2);

  return track;
}

//______________________________________________________________________________
void init_canvas(){
    TTree * t = trajectories_tree; //(TTree*)gROOT->FindObject("betaDetectors2");
    TH1 * h1 = 0;
    TH2 * h2 = 0;

    gHistCanvas->Divide(3,2);
    if(0){ 
       gHistCanvas->cd(1);
       if(t) t->Draw("fBigcalClusters.fCherenkovBestNPESum>>hCerNPE(200,2.0,60.0)","","");
       h1 = (TH1*)gROOT->FindObject("hCerNPE");
       gCerHists->Add(h1);

       gHistCanvas->cd(2);
       t->Draw("fForwardTrackerEvent.fTrackerPositionHits.fPositionVector.fY:fForwardTrackerEvent.fTrackerPositionHits.fPositionVector.fX>>hTrackerXY1","","colz");
       h2 = (TH2*)gROOT->FindObject("hTrackerXY1");
       gTrackerHists->Add(h2);

       gHistCanvas->cd(3);
       t->Draw("fForwardTrackerEvent.fTrackerPositionHits.fPositionVector2.fY:fForwardTrackerEvent.fTrackerPositionHits.fPositionVector2.fX>>hTrackerXY2","","colz");
       h2 = (TH2*)gROOT->FindObject("hTrackerXY2");
       gTrackerHists->Add(h2);

       gHistCanvas->cd(4);
       t->Draw("fLuciteHodoscopeEvent.fLucitePositionHits.fPositionVector.fY:fLuciteHodoscopeEvent.fLucitePositionHits.fPositionVector.fX>>hLuciteXY","","colz");
       h2 = (TH2*)gROOT->FindObject("hLuciteXY");
       gLuciteHists->Add(h2);


       gHistCanvas->cd(5);
       t->Draw("fBigcalClusters.fYMoment:fBigcalClusters.fXMoment>>hBigcalXY","","colz");
       h2 = (TH2*)gROOT->FindObject("hBigcalXY");
       gBigcalHists->Add(h2);

    }
}

//______________________________________________________________________________
Bool_t sane_loadlib(const char* project)
{
   return(0);	
}

//______________________________________________________________________________
void load_event()
{
   /// Clearing the last event
   gEve->GetViewers()->DeleteAnnotations();

   if(gCerBoxSet){
      gCerBoxSet->DestroyElements();
      gCerBoxSet->Destroy();
      gCerBoxSet=0;
   }

   if(gBoxSets){
      gBoxSets->DestroyElements();
      gBoxSets->Destroy();
      gBoxSets =0;
      gBoxSet = 0;
   }
   if(gPointSet) {
      std::cout << " gPointSet has " << gPointSet->NumChildren() << " BEFORE.\n";
   }
   if(gHitPoints) {
      std::cout << " gHitPoints has " << gHitPoints->NumChildren() << " children which are about to be deleted...\n";
      gHitPoints->DestroyElements();
      std::cout << " gHitPoints  now has " << gHitPoints->NumChildren() << " children.\n";
      std::cout << " gPointSet = " << gPointSet << "\n";
//      gHitPoints->Destroy();
      if(gPointSet) gPointSet=0;
      if(gTrackerPointSet) gTrackerPointSet=0;

   }
   if(gPointSet) {
      std::cout << " gPointSet has " << gPointSet->NumChildren() << " AFTER\n";
   }
   if(gLineSet) {
      std::cout << " gLineSet has " << gLineSet->NumChildren() << " children which are about to be deleted...\n";
      gLineSet->DestroyElements();
      std::cout << " gLineSet  now has " << gLineSet->NumChildren() << " children.\n";
      gLineSet->Destroy();
      gLineSet=0;
   }
   if(gPointSet)  {
      std::cout << " gPointSet has " << gPointSet->NumChildren() << " children which are about to be deleted...\n";
      gPointSet->DestroyElements();
      std::cout << " gPointSet  now has " << gPointSet->NumChildren() << " children.\n";
      gPointSet->Destroy();
      gPointSet=0;
   }
   if (gTrackList) {
      std::cout << " gTrackList has " << gTrackList->NumChildren() << " children which are about to be deleted...\n";
      gTrackList->DestroyElements();
      std::cout << " gTrackList  now has " << gTrackList->GetPropagator() << " propagator field.\n";
      std::cout << " vs gPropagator  " << gPropagator << " with field " << gTrackList->NumChildren() << " magnetic field.\n";
//       std::cout << " gTrackList  now has " << gTrackList->NumChildren() << " children.\n";
//       gTrackList->Destroy();
/*      gTrackList=0;*/
   }
   /// DONE clearing EVE
   /// Load next event...

   run_id = fgRunNumber;
   main_tree->GetEntry(event_id);

   Int_t eventnumber = pi0_3cluster_event->fEventNumber;

   printf("Loading entry number %d ",event_id);
   printf("corresponding to event %d of run %d.\n", eventnumber,run_id);

   if(reconstructed_tree) if(reconstructed_tree != main_tree)
      reconstructed_tree->GetEntryWithIndex(run_id,eventnumber);
   if(detector_tree) if(detector_tree != main_tree)
      detector_tree->GetEntryWithIndex(run_id,eventnumber);
   if(trajectories_tree) if(trajectories_tree != main_tree)
      trajectories_tree->GetEntryWithIndex(run_id,eventnumber);

   read_event();

   TEveElement* top = gEve->GetCurrentEvent();
/*
   gMultiView->DestroyEventRPhi();
   gMultiView->ImportEventRPhi(top);

   gMultiView->DestroyEventRhoZ();
   gMultiView->ImportEventRhoZ(top);
*/
   update_html_summary();
//   if(gPointSet) gPointSet->SetRnrSelf(true);
   gEve->Redraw3D(kFALSE, kTRUE);
   gEve->GetEventScene()->Repaint();
}

//______________________________________________________________________________
class EvNavHandler
{
public:
   void Fwd()
   {
      if (event_id < main_tree->GetEntries() - 1) {
         ++event_id;
         load_event();
      } else {
         printf("Already at last event.\n");
      }
   }
   void Bck()
   {
      if (event_id > 0) {
         --event_id;
         load_event();
      } else {
         printf("Already at first event.\n");
      }
   }

   void PrintInfo()
   {
      for(int k=0;k<trajectories->GetEntries();k++){
         aTraj = (InSANETrajectory*)(*trajectories)[k];
/*      aTraj->fPosition0.Print();*/
         BIGCALCluster * clust = (BIGCALCluster*)(*fReconEvent->fBigcalClusters)[k];
         clust->Dump();
         for(int i=0;i<5;i++)
            for(int j=0;j<5;j++) printf("block i=%d,j=%d,E=%f \n",i,j,clust->fBlockEnergies[i][j]);
      }
   }
};

//______________________________________________________________________________
void make_gui()
{
   // Create minimal GUI for event navigation.

   TEveBrowser* browser = gEve->GetBrowser();
   browser->StartEmbedding(TRootBrowser::kLeft);

   TGMainFrame* frmMain = new TGMainFrame(gClient->GetRoot(), 1000, 600);
   frmMain->SetWindowName("XX GUI");
   frmMain->SetCleanup(kDeepCleanup);

   TGHorizontalFrame* hf = new TGHorizontalFrame(frmMain);
   {
      
      TString icondir( Form("%s/icons/", gSystem->Getenv("ROOTSYS")) );
      TGPictureButton* b = 0;
      EvNavHandler    *fh = new EvNavHandler;

      b = new TGPictureButton(hf, gClient->GetPicture(icondir+"GoBack.gif"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "EvNavHandler", fh, "Bck()");

      b = new TGPictureButton(hf, gClient->GetPicture(icondir+"GoForward.gif"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "EvNavHandler", fh, "Fwd()");

      b = new TGPictureButton(hf, gClient->GetPicture(icondir+"stop.png"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "EvNavHandler", fh, "PrintInfo()");
   }
   frmMain->AddFrame(hf);

   frmMain->MapSubwindows();
   frmMain->Resize();
   frmMain->MapWindow();

   browser->StopEmbedding();
   browser->SetTabTitle("Event Control", 0);
}

//______________________________________________________________________________
void read_event()
{
   InSANERun * saneRun = rman->GetCurrentRun();

   if(gBoxSets == 0){
      gBoxSets = new TEveElementList("BigcalBoxSets");
      gEve->AddElement(gBoxSets);
   }

   if(gCerBoxSet == 0){
      gCerBoxSet = new TEveBoxSet("cerBoxSet");
      gCerBoxSet->SetDefDepth(1.0); /// incorrect dimensions
      gCerBoxSet->SetDefHeight(35.0);  /// incorrect dimensions
      gCerBoxSet->SetDefWidth(35.0);
      gCerBoxSet->UseSingleColor();
      gCerBoxSet->SetMainColor(kRed+3);
      gCerBoxSet->SetMainTransparency(50.0 );    
      gCerBoxSet->Reset(TEveBoxSet::kBT_AABoxFixedDim, kTRUE, 64);
      TEveTrans& t1 = gCerBoxSet->RefMainTrans();
      t1.SetRotByAngles(0.0,40.0,0.0);
      t1.SetPos( 190.0*TMath::Sin(40.0*TMath::Pi()/180.0), 
                0.0 ,
                190.0*TMath::Cos(40.0*TMath::Pi()/180.0));
      gEve->AddElement(gCerBoxSet);
   }

   if(gHitPoints == 0){
      gHitPoints = new TEveElementList("HitPoints");
      gEve->AddElement(gHitPoints);
   }

   if(gLineSet == 0) {
      gLineSet = new TEveStraightLineSet();
      gLineSet->SetMarkerSize(1.0);
      gLineSet->SetMarkerStyle(3);
      gLineSet->SetLineWidth(1);
      gLineSet->SetMarkerColor(3);
      gLineSet->SetLineColor(3);
      gEve->AddElement(gLineSet);
   }

   if(gPointSet ==0) {
      gPointSet = new TEvePointSet();
      gPointSet->SetOwnIds(kTRUE);
      gPointSet->SetMarkerColor(7);
      gPointSet->SetMarkerSize(1.5);
      gPointSet->SetMarkerStyle(4);
      //gEve->AddElement(gPointSet);
      gHitPoints->AddElement(gPointSet);
   }

   if(gTrackerPointSet ==0) {
      gTrackerPointSet = new TEvePointSet();
      gTrackerPointSet->SetOwnIds(kTRUE);
      gTrackerPointSet->SetMarkerColor(8);
      gTrackerPointSet->SetMarkerSize(0.6);
      gTrackerPointSet->SetMarkerStyle(4);
      //gEve->AddElement(gPointSet);
      gHitPoints->AddElement(gTrackerPointSet);
   }

   if (gTrackList == 0)
   {
      gTrackList = new TEveTrackList("Propagated tracks");
      if(gPropagator) delete gPropagator;
      //gPropagator = new TEveTrackPropagator("TrackPropagator","",gMagField,false);
      gPropagator = gTrackList->GetPropagator();
      //gPropagator->SetFitDaughters(kFALSE);
      gPropagator->SetMaxR(40000.0);
      gPropagator->SetStepper(TEveTrackPropagator::kRungeKutta);
      if(!gMagField){
         gMagField = new UVAEveMagField();//new TEveMagFieldConst(0., -10.0, 0.0); 
         if(fgIsPerp) ((UVAEveMagField*)gMagField)->SetPolarizationAngle(TMath::Pi()*80.0/180.0);
         else ((UVAEveMagField*)gMagField)->SetPolarizationAngle(TMath::Pi());
      }
      gPropagator->SetMagFieldObj(gMagField,false); 
      gTrackList->SetMainColor(6);
      gTrackList->SetMarkerColor(kYellow);
      gTrackList->SetMarkerStyle(20);
      gTrackList->SetMarkerSize(1);
      gEve->AddElement(gTrackList);
   }

   if(detector_tree) detector_events->TRIG->PrintSummary();

   Double_t electronInputs[9];
   Double_t positronInputs[9];

         /// Loop over blocks in cluster
      for(int ix=0;ix<5;ix++){
         for(int iy=0;iy<5;iy++){
            int ib = clust->fiPeak - 2 + ix;
            int jb = clust->fjPeak - 2 + iy;
            if(ib>0 && jb>0) if( (jb<57 && jb > 32 && ib < 31) || ( jb<33  && ib<33 ) ) {
               /// Hack that creates a box set for each block.... 
               gBoxSet = new TEveBoxSet(Form("BigcalBlocks%d%d",ib,jb));
               gBoxSet->SetDefDepth(3.0);
               gBoxSet->SetDefHeight(3.0);
               gBoxSet->SetDefWidth(20.0);
               gBoxSet->UseSingleColor();
               gBoxSet->SetMainColor(kRed+2);
               gBoxSet->SetMainTransparency(100.0 - 100.0*(clust->fBlockEnergies[ix][iy]/clust->fBlockEnergies[2][2]) );    
               gBoxSet->Reset(TEveBoxSet::kBT_AABoxFixedDim, kTRUE, 64);
               TEveTrans& t = gBoxSet->RefMainTrans();
               t.SetRotByAngles(0.0,-40.0,0.0);
               t.SetPos(360.0*TMath::Sin(40.0*TMath::Pi()/180.0),0.0,360.0*TMath::Cos(40.0*TMath::Pi()/180.0));
               gBoxSets->AddElement(gBoxSet);
               gBoxSet->AddBox(0.0,gBigcalGeoCalc->GetBlockYij_BCCoords(ib,jb),gBigcalGeoCalc->GetBlockXij_BCCoords(ib,jb));
               gBoxSet->RefitPlex();
            }
         }
      } // End loop over cluster's blocks


   TEveTrack *track = 0;
   Double_t origin[3]={0.0,0.0,0.0};
   Double_t aPoint[3]={0.0,0.0,0.0};
   std::cout << " ____________________________________________________" << "\n";
   std::cout << " Number of trajectores: " << trajectories->GetEntries() << "\n";
   for(int k=0;k<trajectories->GetEntries();k++){
      aTraj = (InSANETrajectory*)(*trajectories)[k];
/*      aTraj->fPosition0.Print();*/
      aTraj->fPosition0.GetXYZ(aPoint);
      gLineSet->AddLine(TEveVector(origin),TEveVector(aPoint));
      BIGCALCluster * clust = (BIGCALCluster*)(*fReconEvent->fBigcalClusters)[k];

      /// Make track for this cluster
      nnEvent->SetEventValues(clust);
      nnEvent->GetCorrectionMLPInputNeuronArray(electronInputs);
      if(fgIsPerp) {
         clust->fDeltaE     = nnPerpElectronEnergy->Value(0,electronInputs);
         clust->fDeltaTheta = nnPerpElectronTheta->Value(0,electronInputs);
         clust->fDeltaPhi   = nnPerpElectronPhi->Value(0,electronInputs);
         TVector3 momentum(0,0,0);
         momentum.SetMagThetaPhi((clust->GetEnergy()+clust->fDeltaE)/1000.0,clust->GetTheta()+clust->fDeltaTheta,clust->GetPhi()+clust->fDeltaPhi);
         TVector3 vertex(0,0,0);
         track  = make_track(fgElectronPDGcode, &momentum, &vertex) ;
         gTrackList->AddElement(track);
         track->MakeTrack();
      } else {
         clust->fDeltaE     = nnParaElectronEnergy->Value(0,electronInputs);
         clust->fDeltaTheta = nnParaElectronTheta->Value(0,electronInputs);
         clust->fDeltaPhi   = nnParaElectronPhi->Value(0,electronInputs);
         TVector3 momentum(0,0,0);
         momentum.SetMagThetaPhi((clust->GetEnergy()+clust->fDeltaE)/1000.0,clust->GetTheta()+clust->fDeltaTheta,clust->GetPhi()+clust->fDeltaPhi);
         TVector3 vertex(0,0,0);
         track  = make_track(fgElectronPDGcode, &momentum, &vertex) ;
         gTrackList->AddElement(track);
         track->MakeTrack();
      }


      /// Loop over blocks in cluster
      for(int ix=0;ix<5;ix++){
         for(int iy=0;iy<5;iy++){
            int ib = clust->fiPeak - 2 + ix;
            int jb = clust->fjPeak - 2 + iy;
            if(ib>0 && jb>0) if( (jb<57 && jb > 32 && ib < 31) || ( jb<33  && ib<33 ) ) {
               /// Hack that creates a box set for each block.... 
               gBoxSet = new TEveBoxSet(Form("BigcalBlocks%d%d",ib,jb));
               gBoxSet->SetDefDepth(3.0);
               gBoxSet->SetDefHeight(3.0);
               gBoxSet->SetDefWidth(20.0);
               gBoxSet->UseSingleColor();
               gBoxSet->SetMainColor(kRed+2);
               gBoxSet->SetMainTransparency(100.0 - 100.0*(clust->fBlockEnergies[ix][iy]/clust->fBlockEnergies[2][2]) );    
               gBoxSet->Reset(TEveBoxSet::kBT_AABoxFixedDim, kTRUE, 64);
               TEveTrans& t = gBoxSet->RefMainTrans();
               t.SetRotByAngles(0.0,-40.0,0.0);
               t.SetPos(360.0*TMath::Sin(40.0*TMath::Pi()/180.0),0.0,360.0*TMath::Cos(40.0*TMath::Pi()/180.0));
               gBoxSets->AddElement(gBoxSet);
               gBoxSet->AddBox(0.0,gBigcalGeoCalc->GetBlockYij_BCCoords(ib,jb),gBigcalGeoCalc->GetBlockXij_BCCoords(ib,jb));
               gBoxSet->RefitPlex();
            }
         }
      } // End loop over cluster's blocks
      if( clust->fGoodCherenkovTDCHit ){
         gCerBoxSet->AddBox(gGasCherenkovGeoCalc->GetXPosition(clust->fPrimaryMirror),
                            gGasCherenkovGeoCalc->GetYPosition(clust->fPrimaryMirror),
                            0.0 );
         gCerBoxSet->RefitPlex();
      }
   }
//   TEveTrackPropagator* trkProp = gTrackList->GetPropagator();
//   trkProp->SetMagField( 0.1 * esdrun->fMagneticField ); // kGaus to Tesla

    

/*   std::cout << " ____________________________________________________" << "\n";*/
   std::cout << " Number of positions  " << positions->GetEntries() << "\n";
   for(int i=0; i < positions->GetEntries(); i++) {
      aHitPos = (InSANEHitPosition*)(*positions)[i];
/*      aHitPos->fPosition.Print();*/
      gPointSet->SetNextPoint(aHitPos->fPosition.X(),aHitPos->fPosition.Y(),aHitPos->fPosition.Z());
      gPointSet->SetPointId(new TNamed(Form("Point %d", i), ""));
   }
/*   std::cout << " ____________________________________________________" << "\n";*/
   std::cout << " Number of tracker positions  " << trackerPositions->GetEntries() << "\n";
   for(int i=0; i < trackerPositions->GetEntries(); i++) {
      aHitPos = (InSANEHitPosition*)(*trackerPositions)[i];
/*      aHitPos->fPosition.Print();*/
      gTrackerPointSet->SetNextPoint(aHitPos->fPosition.X(),aHitPos->fPosition.Y(),aHitPos->fPosition.Z());
      gTrackerPointSet->SetPointId(new TNamed(Form("trackerPoint %d", i), ""));
   }
   std::cout << " ____________________________________________________" << "\n";


    gTrackList->MakeTracks();
}
