Int_t beamMonitor(Int_t runNumber = 72999) {

   TH2F * h2 = 0;
   TH2F * h2 = 0;
   TGraph * g1 = 0;
   TProfile * px;
   TProfile * py;

   if(!rman->IsRunSet() ) rman->SetRun(runNumber);
   else if(runNumber != rman->fCurrentRunNumber) runNumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   rman->GetScalerFile()->cd();
   
   TTree * t = (TTree*)gROOT->FindObject("Scalers0");

   TCanvas * c = new TCanvas("beamMonotor","Beam Monitor",1200,800);
   c->Divide(1,2);
//    TCanvas * c2 = new TCanvas("beamCurrent","Beam Current",900,240);

   t->Draw("saneScalerEvent.BeamCurrentMonitor1():saneScalerEvent.fTime>>bcm1","","goff");
   t->Draw("saneScalerEvent.BeamCurrentMonitor2():saneScalerEvent.fTime>>bcm2","","goff");
   t->Draw("saneScalerEvent.BeamCurrentAverage():saneScalerEvent.fTime>>bcAvg","","goff");
   t->Draw("saneScalerEvent.BCM1ChargeAsymmetry():saneScalerEvent.fTime>>bcm1QAsym","","goff");
   t->Draw("saneScalerEvent.BCM2ChargeAsymmetry():saneScalerEvent.fTime>>bcm2QAsym","","goff");
   t->Draw("saneScalerEvent.BeamCurrentAverage()*saneScalerEvent.fTime:saneScalerEvent.fTime>>totalQ","","goff");
   t->Draw("saneScalerEvent.BeamCurrentAveragePositiveHelicity()*saneScalerEvent.fDeltaT:saneScalerEvent.fTime>>totalQplus","","goff");
   t->Draw("saneScalerEvent.BeamCurrentAveragePositiveHelicity()*saneScalerEvent.fDeltaT:saneScalerEvent.fTime>>totalQminus","","goff");
   t->Draw("saneScalerEvent.fLiveTime:saneScalerEvent.fTime>>liveTime","","goff");

   c->cd(1);
   h2 = (TH2F*)gROOT->FindObject("bcm1");
   h2->SetTitle("Beam Current Monitor 1");
   px = h2->ProfileX();
   px->GetYaxis()->SetTitle("nA");
   px->GetXaxis()->SetTitle("seconds");
   px->SetTitle("Beam Current Monitor 1");
   px->SetLineColor(32);
   px->SetMarkerColor(4);
   px->SetMarkerStyle(22);
   px->SetMarkerSize(1.1);
   px->DrawClone("lp");
//    g1 = new TGraph(px);
//    g1->Draw("p");

/*   c->cd(2);*/
   h2 = (TH2F*)gROOT->FindObject("bcm2");
   h2->SetTitle("Beam Current Monitor 2");
   px = h2->ProfileX();
   px->GetYaxis()->SetTitle("nA");
   px->GetXaxis()->SetTitle("seconds");
   px->SetTitle("Beam Current Monitor 2");
   px->SetLineColor(38);
   px->SetMarkerColor(2);
   px->SetMarkerStyle(23);
   px->SetMarkerSize(1.1);
   px->DrawClone("lpsame");

   c->cd(2);
   h2 = (TH2F*)gROOT->FindObject("bcAvg");
   h2->SetTitle("Beam Current Average");
   px = h2->ProfileX();
   px->GetYaxis()->SetTitle("nA");
   px->GetXaxis()->SetTitle("seconds");
   px->SetTitle("Beam Current Average");
   px->SetLineColor(39);
   px->SetMarkerColor(9);
   px->SetMarkerStyle(21);
   px->SetMarkerSize(1.1);
   px->Draw("lp");

//    c2->cd(0);
//    px->DrawClone();

   //c->cd(0);
   //c->cd(3);
   //h2 = (TH2F*)gROOT->FindObject("bcm1QAsym");
   //h2->SetTitle("BCM1 Charge Asymmetry");
   //px = h2->ProfileX();
   //px->GetYaxis()->SetTitle("uC");
   //px->GetXaxis()->SetTitle("seconds");
   //px->SetTitle("BCM1 Charge Asymmetry");
   //px->SetLineColor(40);
   //px->SetMarkerColor(8);
   //px->SetMarkerStyle(34);
   //px->SetMarkerSize(1.1);
   //px->DrawClone();

   //c->cd(4);
   //h2 = (TH2F*)gROOT->FindObject("bcm2QAsym");
   //h2->SetTitle("BCM2 Charge Asymmetry");
   //px = h2->ProfileX();
   //px->GetYaxis()->SetTitle("uC");
   //px->GetXaxis()->SetTitle("seconds");
   //px->SetTitle("BCM2 Charge Asymmetry");
   //px->SetLineColor(46);
   //px->SetMarkerColor(9);
   //px->SetMarkerStyle(33);
   //px->SetMarkerSize(1.1);
   //px->DrawClone();


   //c->cd(5);
   //h2 = (TH2F*)gROOT->FindObject("totalQ");
   //h2->SetTitle("Total Charge");
   //px = h2->ProfileX();
   //px->GetYaxis()->SetTitle("uC");
   //px->GetXaxis()->SetTitle("seconds");
   //px->SetTitle("Total Charge");
   //px->SetLineColor(46);
   //px->SetMarkerColor(9);
   //px->SetMarkerStyle(33);
   //px->SetMarkerSize(1.1);
   //px->DrawClone();

   //c->cd(6);
   //h2 = (TH2F*)gROOT->FindObject("liveTime");
   //h2->SetTitle("Live Time");
   //px = h2->ProfileX();
   //px->GetYaxis()->SetTitle("liveTime");
   //px->GetXaxis()->SetTitle("seconds");
   //px->SetTitle("Live Time");
   //px->SetLineColor(46);
   //px->SetMarkerColor(9);
   //px->SetMarkerStyle(33);
   //px->SetMarkerSize(1.1);
   //px->DrawClone();

   c->SaveAs(Form("plots/%d/BeamMonitor.png",runNumber));
   c->SaveAs(Form("plots/%d/BeamMonitor.pdf",runNumber));
//    c2->SaveAs(Form("plots/%d/BeamCurrent.png",runNumber));

//    c->cd(4);
//    h2->Draw("colz");

}
