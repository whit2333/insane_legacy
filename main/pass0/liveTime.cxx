Int_t liveTime(Int_t runNumber = 72494,Int_t aNumber = 21) {

   if(runNumber < 71999) return 0;

   TH2F * h2 = 0;
   TH2F * h3 = 0;
   TGraph * g1 = 0;
   TProfile * px;
   TProfile * py;
   TProfile * py1;
   TProfile * py2;
   TH2F * h_all =0;

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   InSANERun * run = runManager->GetCurrentRun();

   runManager->GetScalerFile()->cd();
   runManager->GetScalerFile()->ls();
   TTree * t = (TTree*)gROOT->FindObject("Scalers0");
   if(!t) {
      t = (TTree*)runManager->GetScalerFile()->FindObject("Scalers0");
   }
   if(!t) {
      runManager->GetScalerFile()->cd();
      runManager->GetScalerFile()->ls();
      t = (TTree*)gROOT->FindObject("Scalers0");
   }
   if(!t) {
      rman->GetCurrentFile()->cd();
      t = (TTree*)gROOT->FindObject("Scalers0");
   }
   if(!t) { std::cout << "COULD NOT FIND TREE Scalers0.\n"; return(-1);}

   TCanvas * c = new TCanvas("liveTimeFitting","Live Time");

   TF1 * LiveTimeFit0 = (TF1*) run->fResults.FindObject("LiveTimeFit0");
   if(LiveTimeFit0) {
      run->fResults.Remove(LiveTimeFit0);
      delete LiveTimeFit0;
      LiveTimeFit0 = 0;
   }
   LiveTimeFit0 = new TF1("LiveTimeFit0","1.0+x*[0]",0,3200);

   TF1 * LiveTimeFit1 = (TF1*) run->fResults.FindObject("LiveTimeFit1");
   if(LiveTimeFit1) {
      run->fResults.Remove(LiveTimeFit1);
      delete LiveTimeFit1;
      LiveTimeFit1 = 0;
   }
   LiveTimeFit1 = new TF1("LiveTimeFit1","1.0+x*[0]",0,3200);

   TF1 * LiveTimeFit2 = (TF1*) run->fResults.FindObject("LiveTimeFit2");
   if(LiveTimeFit2) {
      run->fResults.Remove(LiveTimeFit2);
      delete LiveTimeFit2;
      LiveTimeFit2 = 0;
   }
   LiveTimeFit2 = new TF1("LiveTimeFit2","1.0+x*[0]",0,3200);

   TF1 * LiveTimeFit3 = (TF1*) run->fResults.FindObject("LiveTimeFit3");
   if(LiveTimeFit3) {
      run->fResults.Remove(LiveTimeFit3);
      delete LiveTimeFit3;
      LiveTimeFit3 = 0;
   }
   LiveTimeFit3 = new TF1("LiveTimeFit3","1.0+x*[0]",0,3200);

   LiveTimeFit0->SetLineColor(2001);
   LiveTimeFit2->SetLineColor(2002);
   LiveTimeFit1->SetLineColor(2003);
   LiveTimeFit3->SetLineColor(2000);

   c->Divide(2,3);

   //--------------------------------------------------------------------
   // Helicity independent livetime

   c->cd(1);
   t->Draw("fTSCounter.fTriggerCount/fTSCounter.fScaler:\
         fTSCounter.fTriggerCount>>\
         hLTvsNTrig(150,200,3200, 50,0.75,1.00)","","goff");
   h1 = (TH2F*)gROOT->FindObject("hLTvsNTrig");
   if(h1) {
      h1->Draw("colz");
      h2 = h1;
      py = h1->ProfileX("py0");
      py->GetYaxis()->SetTitle("Live Time ");
      py->GetXaxis()->SetTitle("Trigger Count");
      py->SetTitle("Live Time ");
      py->SetLineColor(kRed);
      py->SetMarkerColor(kRed);
      py->SetMarkerStyle(20);
      py->SetMarkerSize(1.0);
      h1->SetMarkerColor(kRed);
      h1->SetLineColor(kRed);
      py->Approximate();
      c->cd(2); 
      py->Draw();
      py->GetYaxis()->SetRangeUser(0.75,1.0);
      py1 = py;
      TFitResultPtr r = py1->Fit(LiveTimeFit0,"M,E,S","goff");
      LiveTimeFit0->DrawCopy("same");
      // get the maximum location
      int xmax,ymax,zmax;
      int maxbin = h1->GetMaximumBin(xmax,ymax,zmax);
      Double_t maxrate     = h1->ProjectionX()->GetMean();//GetBinCenter(xmax);
      Double_t maxlivetime = h1->ProjectionY()->GetMean();//->GetYaxis()->GetBinCenter(ymax);
      const double * pars = r->GetParams();
      const double * errs = r->GetErrors(); 
      std::ofstream outf(Form("results/livetime/livetime_fit_results1_%d.txt",aNumber),std::ios_base::app);
      outf << runNumber << " " ;
      outf << maxrate << " " ;
      outf << maxlivetime << " " ;
      outf << pars[0] << " "; 
      outf << errs[0] << " "; 
      outf << "\n";
      c->cd(1);
      LiveTimeFit0->DrawCopy("same");
   }

   //--------------------------------------------------------------------
   // Positive Helicity livetime
//         fTSCounter.fNegativeHelicityTriggerCount/(f1MHzClockNegativeHelicityScaler/f1MHzClockScaler2)>>

   c->cd(3);
   t->Draw("fTSCounter.fNegativeHelicityTriggerCount/fTSCounter.fNegativeHelicityScaler :\
         fTSCounter.fNegativeHelicityTriggerCount/(f1MHzClockNegativeHelicityScaler/f1MHzClockScaler)>>\
         hLTPlusVsNTrig(150,200,3200,50,0.75,1.00)",
         "","goff");
   h1 = (TH2F*)gROOT->FindObject("hLTPlusVsNTrig");
   if(h1) {
      h3 = h1;
      h1->Draw("colz");
      py = h1->ProfileX("py1");
      py->GetYaxis()->SetTitle("Negative Live Time ");
      py->GetXaxis()->SetTitle("Negative Trigger Count");
      py->SetTitle("Live Time ");
      py->SetLineColor(kBlue);
      py->SetMarkerColor(kBlue);
      py->SetMarkerStyle(20);
      py->SetMarkerSize(1.0);
      h1->SetMarkerColor(kBlue);
      h1->SetLineColor(kBlue);
      py->Approximate();
      c->cd(4); 
      py->Draw();
      py->GetYaxis()->SetRangeUser(0.75,1.0);
      py2 = py;
      //py2->Fit(LiveTimeFit0,"M,E,W","goff");
      TFitResultPtr r = py2->Fit(LiveTimeFit1,"M,E,S","goff");
      //LiveTimeFit0->DrawCopy("same");
      LiveTimeFit1->DrawCopy("same");
      // get the maximum location
      int xmax,ymax,zmax;
      int maxbin = h1->GetMaximumBin(xmax,ymax,zmax);
      Double_t maxrate     = h1->ProjectionX()->GetMean();//GetBinCenter(xmax);
      Double_t maxlivetime = h1->ProjectionY()->GetMean();//->GetYaxis()->GetBinCenter(ymax);
      const double * pars = r->GetParams();
      const double * errs = r->GetErrors(); 
      //std::ofstream outf("results/livetime/livetime_fit_results2.txt",std::ios_base::app);
      std::ofstream outf(Form("results/livetime/livetime_fit_results2_%d.txt",aNumber),std::ios_base::app);
      outf << runNumber << " " ;
      outf << maxrate << " " ;
      outf << maxlivetime << " " ;
      outf << pars[0] << " "; 
      outf << errs[0] << " "; 
      outf << "\n";
      c->cd(3);
      LiveTimeFit1->DrawCopy("same");

   }
   //LiveTimeFit1->DrawCopy("same");

   //---------------------------------------------------------
   TProfile  * py_all = 0;//new TProfile(*py1);
   c->cd(5);
   if(h2 && h3) {
      h_all = new TH2F(*h2);
      h_all->Add(h3);
      h_all->Draw("colz");
      py_all = h_all->ProfileX("py2");
      py_all->GetYaxis()->SetTitle("Negative Live Time ");
      py_all->GetXaxis()->SetTitle("Negative Trigger Count");
      py_all->SetTitle("Live Time ");
      py_all->SetLineColor(1);
      py_all->SetMarkerColor(1);
      py_all->SetMarkerStyle(20);
      py_all->SetMarkerSize(1.0);
      h_all->SetMarkerColor(1);
      h_all->SetLineColor(1);
      c->cd(6); 
      py_all->Draw();
      py_all->GetYaxis()->SetRangeUser(0.75,1.0);

   }

   //----------------------------------------------------------
   // Fits
   if(py_all){
      TFitResultPtr r = py_all->Fit(LiveTimeFit2,"M,E,S","same");
      // get the maximum location
      int xmax,ymax,zmax;
      int maxbin = h1->GetMaximumBin(xmax,ymax,zmax);
      Double_t maxrate     = h1->ProjectionX()->GetMean();//GetBinCenter(xmax);
      Double_t maxlivetime = h1->ProjectionY()->GetMean();//->GetYaxis()->GetBinCenter(ymax);
      const double * pars = r->GetParams();
      const double * errs = r->GetErrors(); 
      //std::ofstream outf("results/livetime/livetime_fit_results3.txt",std::ios_base::app);
      std::ofstream outf(Form("results/livetime/livetime_fit_results3_%d.txt",aNumber),std::ios_base::app);
      outf << runNumber << " " ;
      outf << maxrate << " " ;
      outf << maxlivetime << " " ;
      outf << pars[0] << " "; 
      outf << errs[0] << " "; 
      outf << "\n";
      run->fLiveTime = maxlivetime;
      c->cd(5);
      LiveTimeFit2->Draw("same");
   }

   c->cd(6);
   LiveTimeFit0->DrawCopy("same");
   LiveTimeFit1->DrawCopy("same");
   LiveTimeFit2->DrawCopy("same");
   //LiveTimeFit2->DrawCopy("same");
   //LiveTimeFit3->DrawCopy("same");

   TLegend * leg6 = new TLegend(0.7,0.7,0.9,0.9);
   leg6->AddEntry(LiveTimeFit0,"Fit to livetime","l");
   leg6->AddEntry(LiveTimeFit1,"Fit to Negative livetime","l");
   leg6->AddEntry(LiveTimeFit2,"Fit to both","l");
   leg6->Draw();

   c->cd(5);
   LiveTimeFit0->DrawCopy("same");
   LiveTimeFit1->DrawCopy("same");
   LiveTimeFit2->DrawCopy("same");
   //LiveTimeFit3->DrawCopy("same");
   //----------------------------------------------------------


   run->fResults.Add(LiveTimeFit0);
   run->fResults.Add(LiveTimeFit1);
   run->fResults.Add(LiveTimeFit2);
   runManager->WriteRun();

   c->Update();
   c->SaveAs(Form("plots/%d/LiveTime.png",runNumber));
   c->SaveAs(Form("plots/%d/LiveTime.pdf",runNumber));
   //c->SaveAs(Form("plots/%d/LiveTime.svg",runNumber));

   return(0);
}

