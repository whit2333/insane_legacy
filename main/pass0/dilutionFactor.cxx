/*! Get/create the dilution factors for a run.
     
   This function builds InSANEDilutionFactors and creates an InSANEDilutionFunction object.

 */  
Int_t dilutionFactor(Int_t run_number=72999){

   Int_t runNumber=run_number;
   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();


   /// Gets all data from a database. 
   TSQLServer * db = dbman->GetMySQLConnection();//TSQLServer::Connect("mysql://quarks.temple.edu","sane","secret");

   /// Q2 bins which each have a "dilution function" (a group of dilution factors which are functions of kinematics)
   double q2_bins[4] = {2.22,3.00,4.066,5.49};

   /// Get the beam energy and target angle 
   TString  sql = Form(" SELECT beam_energy,target_angle FROM SANE.run_info WHERE run_number=%d ", run_number);
   TSQLResult * res = 0;
   TSQLRow *    row = 0;
   double beam_energy = 0.0;
   int target_angle = 0;
   if(db) res = db->Query(sql.Data());
   if(res) {
   //   res->Print();
      if(res->GetRowCount() > 0) {
         row = res->Next();
	 beam_energy  = atof(row->GetField(0))/1000.0; 
	 target_angle = atoi(row->GetField(1)); 
	 std::cout << " Energy = " << atof(row->GetField(0))/1000.0 << "  target_angle=" << target_angle << "\n";
      } 
   }
   if(!res) { std::cout << " run not found \n" ; return(-1);} 

   /// Get the packing fraction 
   int pf_run_number = 0;
/*   sql = Form(" SELECT run_number FROM SANE.packing_fractions WHERE first_run<=%d AND last_run>=%d ;",run_number,run_number);
   if(db) res = db->Query(sql.Data());
   std::cout << sql.Data() << "\n";
   if(res) {
      res->Print();
      if( row = res->Next() ) {
         for(int i = 0;i<res->GetFieldCount(); i++){
	    std::cout << res->GetFieldName(i) << " = " << row->GetField(i) << "\n";
	    pf_run_number = atoi(row->GetField(0));
	 }
      } 
   }
*/

   /// Loop over each Q2 bin and create a "dilution function"
   for(int q=0;q<4;q++) {
      double q2_bin = q2_bins[q];

      /// Get the dilution factors now that we know which beam energy, target angle, and Q2 bin we want. 
      InSANEDilutionFunction * dilution_func = new InSANEDilutionFunction(Form("df-%1.2f",q2_bin),Form("df %1.2f",q2_bin));
      dilution_func->fQ2_bin = q2_bin; 

      InSANEDilutionFactor * df = 0;

      //sql = Form(" SELECT df,df_error FROM SANE.dilution_factors WHERE ABS(beam_energy-%f)<0.2",beam_energy);
      /// Currently only one df for each configuration... ( need more ) 
      sql = Form(" SELECT x_bin,W_bin,df,df_error FROM SANE.dilution_factors WHERE ABS(beam_energy-%f)<0.1 AND target_angle=%d AND q_squared_bin=%f", beam_energy,target_angle,q2_bin);
      //sql = Form(" SELECT x_bin,W_bin,df,df_error FROM SANE.dilution_factors WHERE ABS(beam_energy-%f)<0.2 AND target_angle=%d ", beam_energy,target_angle);
      std::cout << sql.Data() << "\n";

      if(db){
         res = db->Query(sql.Data());
         if(res) {
            dilution_func->SetNBins( res->GetRowCount() ) ;
            //res->Print();
            while( row = res->Next() ) {
            
	       /// Create and set all the values for this kinematic point.
	       df = new InSANEDilutionFactor();
	       df->fx        = atof(row->GetField(0)) ; 
	       df->fW        = atof(row->GetField(1)) ; 
	       df->fDilution = atof(row->GetField(2)) ; 
	       df->fDilution_error = atof(row->GetField(3)) ; 
               dilution_func->Add(df);
         
	       //for(int i = 0;i<res->GetFieldCount(); i++){
	       //   std::cout << res->GetFieldName(i) << " = " << row->GetField(i) << "  ";
	       //}
               //std::cout << "\n __________________________________________________________________________ \n";
            }
	 }    
      }

      /// Fit the x and W distributions 
      dilution_func->FitGraphs();

      /// Add this bin's dilution function to the run       
      SANERunManager::GetRunManager()->GetCurrentRun()->AddDilutionFunction(dilution_func);
      
      std::cout << " df(x=0.37) = " << dilution_func->GetXFitResult(0.37) << " \n";
      std::cout << " df(W=2.15)  = " << dilution_func->GetWFitResult(2.15) << " \n";
   }


/*   TCanvas * c = new TCanvas();
   c->Divide(3,1);
   c->cd(1);
   dilution_func->fGraph->SetMarkerStyle(20);
   dilution_func->fGraph->Draw("P");
   c->cd(2);
   dilution_func->fDfVsW->SetMarkerStyle(21);
   dilution_func->fDfVsW->Draw("AEP");
   c->cd(3);
   dilution_func->fDfVsx->SetMarkerStyle(22);
   dilution_func->fDfVsx->Draw("AEP");

   c->SaveAs(Form("plots/%d/dilutionFactor.png",run_number));
   c->SaveAs(Form("plots/%d/dilutionFactor.png",run_number));
*/

   runManager->WriteRun();
   return(0);
}

