Int_t bigcal_timewalk2(Int_t runNumber = 72995) {

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   runManager->GetCurrentFile()->cd();
   InSANERun * run = runManager->GetCurrentRun();


   SANEEvents * events = new SANEEvents("betaDetectors0");
   if( !(events->fTree) ) return(-1);


   TH2F * bcTimingGroups[56*4]; 
   for(int i = 0; i < 56*4 ; i++ ) {
      bcTimingGroups[i] = new TH2F(Form("bcTimingGroups-%d",i),Form("bcTimingGroups-%d",i),50,0,2000,50,-100,100);
   }

   TH2F * bigcalhists[32*32+30*24]; 
   for(int i = 0; i < 32*32+30*24 ; i++ ) {
      bigcalhists[i] = new TH2F(Form("bigcalhists-%d",i),Form("bigcalhists-%d",i),50,0,1000,50,-100,100);
   }

   TH2F * h2 = new TH2F(Form("bigcalBlockVsGroupEnergy"),Form("bigcalBlockVsGroupEnergy"),100,-1000,5000,100,-1000,5000);


   Int_t fEntries =  events->fTree->GetEntries();

   /// Event Loop
   for(int ievent = 0; ievent < events->fTree->GetEntries() && ievent < 50000  ; ievent++){

      if(ievent%10000 == 0) std::cout << ievent << "/" << fEntries << "\n";
      events->fTree->GetEntry(ievent);

      if(events->TRIG->IsBETA2Event()){

//       for(int j = 0; j < events->BETA->fBigcalEvent->fBigcalTimingGroupHits->GetEntries(); j++){
//          BigcalHit *  bhit = (BigcalHit*)(*(events->BETA->fBigcalEvent->fBigcalTimingGroupHits))[j];
// 
//          bcTimingGroups[bhit->fChannel-1]->Fill(bhit->fADC,bhit->fTDCAlign);
// 
//       } // timing group hits

      for(int i = 0; i < events->BETA->fBigcalEvent->fBigcalHits->GetEntries(); i++){
         BigcalHit *  ahit = (BigcalHit*)(*(events->BETA->fBigcalEvent->fBigcalHits))[i];

         int group = BIGCALGeometryCalculator::GetCalculator()->GetGroupNumber(ahit->fiCell,ahit->fjCell);
         //std::cout << " group = " << group << "\n";
         for(int j = 0; j < events->BETA->fBigcalEvent->fBigcalTimingGroupHits->GetEntries(); j++){
            BigcalHit *  bhit = (BigcalHit*)(*(events->BETA->fBigcalEvent->fBigcalTimingGroupHits))[j];

            //std::cout << "bhit->fChannel+1 = " << bhit->fChannel << "\n";
            if( bhit->fChannel == group ) {
               h2->Fill(ahit->fADC , bhit->fADC);
               //if(TMath::Abs(ahit->fEnergy - bhit->fEnergy) < 800.0 ) bigcalhists[ahit->fChannel]->Fill(ahit->fADC,bhit->fTDCAlign);

            }
         } // timing group hits

      } // bigcal block hits

      } // beta2 event
      
   
   } // event loop


   h2->Draw("colz");

//    TProfile * p = 0;
//    TF1 * bctw = new TF1("bctw","[0]+[1]*x+[2]*x*x",100,800);
// 
//    for(int i = 0; i < 32*32+30*24 ; i++ ) {
//       p = bigcalhists[i]->ProfileX();
//       p->Fit(bctw,"M,R");
//    }

   new TBrowser;


   return(0);
}
