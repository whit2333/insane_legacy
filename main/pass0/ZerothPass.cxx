
/*! \page zerothpass Zeroth Pass documentation page
  The "zeroth pass" consists of execution of just an InSANECorrector concrete class, SANEZerothPassAnalyzer.

  \section zp0 Output 
  Using SANEZerothPassAnalyzer, it opens and clones the
  raw tree "betaDetectors". The new output tree is called
  "betaDetectors0". 

  \section zp1 Description
  The zeroth pass does the following:
    - BETAPedestalCorrection. Subtracts the pedestal values for the run from all ADCs, setting its new value
    - BETATimingCorrection. Produces a new value for TDC hits called fTDCAlign which centers the peak at zero but retains
      the previous value of fTDC.
    - BETAEventCounterCorrection. Counts events and hits
    - SANEScalerCalculation. Used to calculate trigger rates. 

  \section zp4 Bugs
     

  \section zp5 Future Improvements

*/


/**  Zeroth Pass Analysis script
 *
 *  The Steps of a good analyzer pass script
 *  - Name input and output trees
 *  - Define the analyzer for this pass
 *    \note This does not yet create the trees! This happens at initialize
 *  - Create each detector used by the analysis and add it to the analyzer. 
 *    Currently all the detectors are created in InSANEDetectorAnalysis
 *  - Define and setup each "Correction" and "Calculation".
 *  - Set each detector used by the analysis
 *  - Add the corrections and calculations 
 *    \note Order Matters. It is the execution of the list from first to last. 
 *  - Initialze and Process 
 * 
 */
Int_t ZerothPass(Int_t runNumber = 72995) {

  gROOT->SetBatch(kTRUE);

  const char * sourceTreeName = "betaDetectors";
  const char * outputTreeName = "betaDetectors0";

   // First open the run
   if( !(rman->IsRunSet()) ) rman->SetRun(runNumber);
   else runNumber = rman->fRunNumber;
   //rman->CreateNewPass();
   rman->SetVerbosity(3);
      // Delete old trees
   rman->GetScalerFile()->Delete("Scalers0;*");
   rman->GetCurrentFile()->Delete("betaDetectors1;*");
   rman->GetCurrentFile()->Delete("pi0results;*");
   rman->GetCurrentFile()->Delete("cleanedEvents;*");
   rman->GetCurrentFile()->Delete("DISElectrons;*");

   // Create Zeroth Pass analyzer 
   //std::cout << " o Creating SANEZerothPassAnalyzer\n";
   SANEZerothPassAnalyzer * corrector;
   corrector = new SANEZerothPassAnalyzer( outputTreeName , sourceTreeName );

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   Int_t cal_series = bcgeo->GetLatestCalibrationSeries(runNumber);
   bcgeo->SetCalibrationSeries(cal_series);

   corrector->AddScript("pass0/time_walk.cxx");
   //corrector->AddScript("pass0/liveTime.cxx");
   //corrector->AddScript("pass0/dilutionFactor.cxx");
   corrector->AddScript("pass0/raster.cxx");

   // ----------------------------------------------------------------
   // Apparatus : 
   // The detectors are currently created by the class 
   // InSANEDetectorAnalysis -> SANEClusteringAnalysis -> SANEFirstPassAnalyzer
   HallCRasteredBeam * beamline = new HallCRasteredBeam();
   UVAPolarizedAmmoniaTarget * target = new UVAPolarizedAmmoniaTarget("UVA-polarized-ammonia-target",
                                                                      "UVA Polarized Ammonia Target",
                                                                      0.6);

   corrector->AddDetector( corrector->fCherenkovDetector ); 
   corrector->AddDetector( corrector->fBigcalDetector ); 
   corrector->AddDetector( corrector->fHodoscopeDetector ); 
   corrector->AddDetector( corrector->fTrackerDetector ); 

   rman->GetCurrentRun()->fDetectors.Add(corrector->fCherenkovDetector);
   rman->GetCurrentRun()->fDetectors.Add(corrector->fBigcalDetector);
   rman->GetCurrentRun()->fDetectors.Add(corrector->fHodoscopeDetector);
   rman->GetCurrentRun()->fDetectors.Add(corrector->fTrackerDetector);

   rman->GetCurrentRun()->fApparatus.Add(beamline);
   rman->GetCurrentRun()->fApparatus.Add(target);

   /// \todo This could be in the analyzer class?..


   /// Corrections 

   BETAPedestalCorrection * b1;
   b1 = new BETAPedestalCorrection(corrector);
   /*  b1->SetEvent( corrector->GetEvents() );*/
   b1->SetDetectors( corrector );
   corrector->AddCorrection( b1 );

   BETATimingCorrection * b2;
   b2 = new BETATimingCorrection(corrector);
   /*  b2->SetEvent( corrector->GetEvents() );*/
   b2->SetDetectors( corrector );
   corrector->AddCorrection( b2 );

   BETAEventCounterCorrection * b3;
   b3 = new BETAEventCounterCorrection(corrector);
   /*  b3->SetEvent( corrector->GetEvents() );*/
   b3->SetDetectors( corrector );
   corrector->AddCorrection( b3 );

   /// Calculations 

   SANEScalerCalculation * scaler1;
   scaler1 = new SANEScalerCalculation(corrector);
   scaler1->SetDetectors(corrector);
   corrector->AddScalerCalculation( scaler1 );

   /// Beam calculation
   HallCBeamCalculation0 * beam1 = new HallCBeamCalculation0(corrector);
   beam1->SetDetectors(corrector);
   corrector->AddCalculation( beam1 );


   //corrector->Dump();

   corrector->Initialize();

   std::cout << "Done INITIALIZING  \n"; 

   corrector->Process();

   std::cout << " FLUSHING AND WRITING FILE \n"; 
   rman->GetCurrentRun()->Dump();
   rman->GetCurrentFile()->Flush();
   rman->GetCurrentFile()->Write();

   delete corrector;

   //gROOT->SetBatch(kFALSE);
   //gROOT->ProcessLine(".q");
   return(0);
}
