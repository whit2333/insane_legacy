Int_t liveTime2(Int_t runNumber = 72949) {

   TH2F * h2 = 0;
   TH2F * h3 = 0;
   TGraph * g1 = 0;
   TProfile * px;
   TProfile * py;
   TProfile * py1;
   TProfile * py2;
   TH2F * h_all =0;

   SANERunManager * runManager = (SANERunManager*)InSANERunManager::GetRunManager();
   if( !(runManager->IsRunSet()) ) runManager->SetRun(runNumber);
   else runNumber = runManager->fRunNumber;
   InSANERun * run = runManager->GetCurrentRun();

   runManager->GetScalerFile()->cd();
   runManager->GetScalerFile()->ls();
   TTree * t = (TTree*)gROOT->FindObject("Scalers0");
   if(!t) {
      t = (TTree*)runManager->GetScalerFile()->FindObject("Scalers0");
   }
   if(!t) {
      runManager->GetScalerFile()->cd();
      runManager->GetScalerFile()->ls();
      t = (TTree*)gROOT->FindObject("Scalers0");
   }
   if(!t) {
      rman->GetCurrentFile()->cd();
      t = (TTree*)gROOT->FindObject("Scalers0");
   }
   if(!t) { std::cout << "COULD NOT FIND TREE Scalers0.\n"; return(-1);}

   TCanvas * c = new TCanvas("liveTimeFitting","Live Time");

   TF1 * LiveTimeFit = new TF1("LiveTimeFit","[0]+x*[1]",0,2000);
   TF1 * LiveTimeFit1 = new TF1("LiveTimeFit1","1.0+x*[0]",0,2000);
   TF1 * LiveTimeFit2 = new TF1("LiveTimeFit2","[0]+x*[1]+x*x*[2]",0,2000);
   TF1 * LiveTimeFit3 = new TF1("LiveTimeFit3","TMath::Exp(-x*[0])",0,2000);

   LiveTimeFit->SetLineColor(2001);
   LiveTimeFit2->SetLineColor(2002);
   LiveTimeFit1->SetLineColor(2003);
   LiveTimeFit3->SetLineColor(2000);


   c->Divide(2,3);

   //--------------------------------------------------------------------
   // Helicity independent livetime

   c->cd(1);
   t->Draw("saneScalerEvent.fBETA2Counter.fLiveTime:\
            saneScalerEvent.fBETA2Counter.fTriggerCount>>\
            hLTvsNTrig(10,400,750,50,0.80,1.00)",
            "fBETA2Counter.fLiveTime>0.85&&fBCM1>68&&fBCM2>68","goff");
   h1 = (TH2F*)gROOT->FindObject("hLTvsNTrig");
   if(h1) {
      h1->Draw("colz");
      h2 = h1;
      py = h1->ProfileX("py0");
      py->GetYaxis()->SetTitle("Live Time ");
      py->GetXaxis()->SetTitle("Trigger Count");
      py->SetTitle("Live Time ");
      py->SetLineColor(kRed);
      py->SetMarkerColor(kRed);
      py->SetMarkerStyle(20);
      py->SetMarkerSize(1.0);
      h1->SetMarkerColor(kRed);
      h1->SetLineColor(kRed);
      c->cd(2); 
      py->Draw();
      py->GetYaxis()->SetRangeUser(0.8,1.0);
      py1 = py;
   }

   //--------------------------------------------------------------------
   // Positive Helicity livetime

   //saneScalerEvent.fBETA2Counter.fNegativeHelicityLiveTime:\
       saneScalerEvent.fBETA2Counter.fPositiveHelicityTriggerCount>>\
   //t->Draw("fBETA2Counter.fPositiveHelicityTriggerCount/fBETA2Counter.fNegativeHelicityScaler :\
   //         saneScalerEvent.fBETA2Counter.fNegativeHelicityTriggerCount>>\
   //         hLTPlusVsNTrig(80,0,1500,100,0.30,1.00)",
   //         "","goff");*(saneScalerEvent.f1MHzClockPositiveHelicityScaler/saneScalerEvent.f1MHzClockScaler2)
   c->cd(3);
   //t->Draw("fBETA2Counter.fNegativeHelicityTriggerCount/fBETA2Counter.fNegativeHelicityScaler :\
   //         fBETA2Counter.fNegativeHelicityTriggerCount/(f1MHzClockNegativeHelicityScaler/f1MHzClockScaler2)>>\
   //         hLTPlusVsNTrig(10,400,750,50,0.80,1.00)",
   //         "fBETA2Counter.fLiveTime>0.85&&fBCM1>68&&fBCM2>68","goff");
   t->Draw("fBETA2Counter.fNegativeHelicityTriggerCount/fBETA2Counter.fNegativeHelicityScaler :\
            fBETA2Counter.fNegativeHelicityTriggerCount/(f1MHzClockNegativeHelicityScaler/f1MHzClockScaler2)>>\
            hLTPlusVsNTrig(20,400,800,50,0.80,1.00)",
            "TMath::Abs(saneScalerEvent.fEventNumber-100000)<20000 || TMath::Abs(saneScalerEvent.fEventNumber-545000)<20000 \
             || TMath::Abs(saneScalerEvent.fEventNumber-800000)<20000 ","goff");
   h1 = (TH2F*)gROOT->FindObject("hLTPlusVsNTrig");
   if(h1) {
      h3 = h1;
      h1->Draw("colz");
      py = h1->ProfileX("py1");
      py->GetYaxis()->SetTitle("Negative Live Time ");
      py->GetXaxis()->SetTitle("Negative Trigger Count");
      py->SetTitle("Live Time ");
      py->SetLineColor(kBlue);
      py->SetMarkerColor(kBlue);
      py->SetMarkerStyle(20);
      py->SetMarkerSize(1.0);
      h1->SetMarkerColor(kBlue);
      h1->SetLineColor(kBlue);
      c->cd(4); 
      py->Draw();
      py->GetYaxis()->SetRangeUser(0.8,1.0);
      py2 = py;
      py2->Fit(LiveTimeFit1,"M,E,W","goff");
      py2->Fit(LiveTimeFit,"M,E,W","goff");
      LiveTimeFit1->DrawCopy("same");
      LiveTimeFit->DrawCopy("same");
   }

   //---------------------------------------------------------
   TProfile  * py_all = 0;//new TProfile(*py1);
   c->cd(5);
   if(h2 && h3) {
      h_all = new TH2F(*h2);
      h_all->Add(h3);
      h_all->Draw("colz");
      py_all = h_all->ProfileX("py2");
      py_all->GetYaxis()->SetTitle("Negative Live Time ");
      py_all->GetXaxis()->SetTitle("Negative Trigger Count");
      py_all->SetTitle("Live Time ");
      py_all->SetLineColor(1);
      py_all->SetMarkerColor(1);
      py_all->SetMarkerStyle(20);
      py_all->SetMarkerSize(1.0);
      h_all->SetMarkerColor(1);
      h_all->SetLineColor(1);
      c->cd(6); 
      py_all->Draw();
      py_all->GetYaxis()->SetRangeUser(0.8,1.0);

   }

   //----------------------------------------------------------
   // Fits
   if(py_all){
      py_all->Fit(LiveTimeFit,"M,E,W","same");
      py_all->Fit(LiveTimeFit1,"M,E,W","same");
      //py_all->Fit(LiveTimeFit2,"M,E,W","same");
      //py_all->Fit(LiveTimeFit3,"M","same");
   }

   c->cd(6);
   LiveTimeFit->DrawCopy("same");
   LiveTimeFit1->DrawCopy("same");
   //LiveTimeFit2->DrawCopy("same");
   //LiveTimeFit3->DrawCopy("same");

   TLegend * leg6 = new TLegend(0.7,0.7,0.9,0.9);
   leg6->AddEntry(LiveTimeFit1,"constrained with y(0) = 1","l");
   leg6->AddEntry(LiveTimeFit,"2 parameter line","l");
   //leg6->AddEntry(LiveTimeFit2,"3 parameter polynomial","l");
   //leg6->AddEntry(LiveTimeFit3,"1 parameter exponential","l");
   leg6->Draw();

   c->cd(5);
   LiveTimeFit->DrawCopy("same");
   LiveTimeFit1->DrawCopy("same");
   LiveTimeFit2->DrawCopy("same");
   LiveTimeFit3->DrawCopy("same");
   //----------------------------------------------------------


   c->Update();
   c->SaveAs(Form("plots/%d/LiveTime2.png",runNumber));
   c->SaveAs(Form("plots/%d/LiveTime2.pdf",runNumber));
   //c->SaveAs(Form("plots/%d/LiveTime.svg",runNumber));

   return(0);














   h3 = (TH2F*)gROOT->FindObject("liveTimeMinus");
   if(h3) {
      h3->SetTitle("Live Time Minus Vs Trigger Count");
      py = h3->ProfileX();
      py->GetYaxis()->SetTitle("Live Time Minus");
      py->GetXaxis()->SetTitle("Trigger Count Minus");
      py->SetTitle("Live Time Minus");
      py->SetLineColor(2003);
      py->SetMarkerColor(2003);
      py->SetMarkerStyle(20);
      py->SetMarkerSize(1.0);
      h3->SetMarkerColor(4);
      h3->SetLineColor(4);
/*      h3->Draw("box,same");*/
//      py->Draw("same");
   }

   TH2F * h4 = (TH2F*)gROOT->FindObject("liveTime0");
   if(h4) {
      h4->SetTitle("Live Time Minus Vs Trigger Count");
      py = h4->ProfileX();
      py->GetYaxis()->SetTitle("Live Time Minus");
      py->GetXaxis()->SetTitle("Trigger Count Minus");
      py->SetTitle("Live Time Minus");
      py->SetLineColor(kBlue-9);
      py->SetMarkerColor(kBlue-9);
      py->SetMarkerStyle(20);
      py->SetMarkerSize(1.0);
      h4->SetMarkerColor(kBlue-9);
      h4->SetLineColor(kBlue-9);
/*      h3->Draw("box,same");*/
//      py->Draw("same");
   }

   h2 = (TH2F*)gROOT->FindObject("liveTime1");
   if(h2) {
      h2->SetTitle("Live Time Vs Trigger Count");
      px = h2->ProfileX();
/*      std::cout << px << "\n";*/
//      px->BuildOptions(0.5,1.0,"g");
      px->GetYaxis()->SetTitle("Live Time");
      px->GetXaxis()->SetTitle("Trigger Count");
      px->SetTitle("Live Time");
      px->SetLineColor(kGreen-9);
      px->SetMarkerColor(kGreen-9);
      px->SetMarkerStyle(20);
      px->SetMarkerSize(1.0);
      h2->GetXaxis()->SetTitle("N triggers");
      h2->GetYaxis()->SetTitle("Live time");
      h2->Draw("box");
      h3->Draw("box,same");
      //h4->Draw("box,same");
      px->Fit(LiveTimeFit,"M,E","same");
      if(py) py->Draw("same");
      px->Draw("same");
      run->fResults.Add(px);

   }
   TH2F * h1;
   TProfile * p1;
   c->cd(2);
   h1 = (TH2F*) h2->Clone("liveTimePlusMinus");
   if(h1) {
      h1->Add(h3);
      h1->Add(h4);
      h1->SetTitle("Live Time Vs Trigger Count");
      p1 = h1->ProfileX();
/*      std::cout << px << "\n";*/
//      px->BuildOptions(0.5,1.0,"g");
      p1->GetYaxis()->SetTitle("Live Time");
      p1->GetXaxis()->SetTitle("Trigger Count");
      p1->SetTitle("Live Time");
      p1->SetLineColor(2000);
      p1->SetMarkerColor(2000);
      p1->SetMarkerStyle(20);
      p1->SetMarkerSize(1.0);
      h1->Draw("box");
/*      h3->Draw("box,same");*/
      p1->Fit(LiveTimeFit1,"M,E,W","same");
      p1->Fit(LiveTimeFit2,"M,E,W","same");
      LiveTimeFit1->Draw("l,same");
      p1->Draw("same");
//       run->fResults.Add(px);

   }

//   t->Scan("fEventNumber:fBETA2Counter.fScaler:fBETA2Counter.fTriggerCount","","",24,0);

   c->cd(0);

   TLatex * text = new TLatex();
   text->SetNDC();
   text->SetTextFont(62);
   text->SetTextColor(kBlack);
   text->SetTextSize(0.02);
   text->SetTextAlign(12);
   text->DrawLatex(0.45,0.5,Form("%d",(Int_t)runNumber));

   run->fResults.Add(LiveTimeFit);
   run->fResults.Add(LiveTimeFit1);
   run->fResults.Add(LiveTimeFit2);
   runManager->WriteRun();


return(0);

}

