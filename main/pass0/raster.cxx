/** 
 */
Int_t raster(Int_t runnumber=72547) {

   const char * detectorTree = "betaDetectors0";

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) rman->SetRun (runnumber);
   rman->GetCurrentFile()->cd();

   TTree * t = (TTree*) gROOT->FindObject(detectorTree);
   if(!t) return(-1);

   gROOT->cd();

   TCanvas * c = new TCanvas("Raster","raster",500,400);
   c->Divide(2,2);
   TH1F * h1;

   c->cd(1);
   t->Draw("fRasterPosition.fY:fRasterPosition.fX","triggerEvent.IsBETAEvent()","colz");
   c->cd(2);
   t->Draw("fRasterPosition.fY","triggerEvent.IsBETAEvent()","");
   c->cd(3);
   t->Draw("fRasterPosition.fX","triggerEvent.IsBETAEvent()","");
   
   c->cd(4);
   t->Draw("fYRaster:fXRaster","triggerEvent.IsBETAEvent()","colz");

   c->SaveAs(Form("plots/%d/Raster.png",runnumber));
   c->SaveAs(Form("plots/%d/Raster.pdf",runnumber));

   return 0;
}
