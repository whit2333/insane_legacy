Int_t A90_47_W(Int_t paraRunGroup = 23,Int_t perpRunGroup = 23,Int_t aNumber=23){

   // -------------------------------
   // UVA data
   std::string line;
   const char * uvafile = "asym_results/SANEDAT4.csv";
   std::cout << " Using UVa file : " << uvafile << std::endl;
   ifstream infile(uvafile);
   Double_t val;
   std::vector< vector<double> > data;
   data.resize(25);
   std::vector<string> colnames;
   string acolname;
   /// skip the first line.
   std::getline(infile, line);
   std::istringstream iss1(line);
   int ncol = 0;
   while (iss1 >> acolname){
      std::cout << ncol << " " << acolname << "\n";
      ncol++; 
   }
   /// read in the data.
   while( std::getline(infile, line) ) {
      std::istringstream iss(line);
      double n;
      int col = 0;
      while (iss >> n){
         data[col].push_back(n);
         //std::cout << n << " ";
         col++;   
      }
      //std::cout << "\n";
   }
   std::vector<double  > datazero;
   for(int i = 0; i<100;i++){ datazero.push_back(0.0);}
   std::vector< vector<double> > data1;
   data1.resize(25);
   std::vector< vector<double> > data_0;
   std::vector< vector<double> > data_1;
   std::vector< vector<double> > data_2;
   std::vector< vector<double> > data_3;
   data_0.resize(25);
   data_1.resize(25);
   data_2.resize(25);
   data_3.resize(25);
//    Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
//    Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t QsqMin[4] = {1.5,2.5,3.5,4.5};
   Double_t QsqMax[4] = {2.5,3.5,4.5,5.5};
   for(int i =0; i < data[0].size(); i++) {
      for(int j=0;j<25;j++){
         if( data[2][i] < QsqMax[0] && data[2][i] > QsqMin[0] ) {
           data_0[j].push_back(data[j][i]);
         }
         if( data[2][i] < QsqMax[1] && data[2][i] > QsqMin[1] ) {
           data_1[j].push_back(data[j][i]);
         }
         if( data[2][i] < QsqMax[2] && data[2][i] > QsqMin[2] ) {
           data_2[j].push_back(data[j][i]);
         }
         if( data[2][i] < QsqMax[3] && data[2][i] > QsqMin[3] ) {
           data_3[j].push_back(data[j][i]);
         }
      }
   }


   int iycol  = 20;
   int ieycol = 21;

   /// A1
   TMultiGraph *mgUVaA1 = new TMultiGraph();
   TGraphErrors * gA1  = new TGraphErrors(data[0].size(),&data[22][0],&data[iycol][0],&datazero[0]/*&data[1][0]*/,&data[ieycol][0]);
   gA1->SetMarkerStyle(22);
   mgUVaA1->Add(gA1,"p");

   TGraphErrors * gA1_0 = new TGraphErrors( data_0[0].size(),&data_0[22][0],&data_0[iycol][0],&datazero[0]/*&data_0[1][0]*/,&data_0[ieycol][0]);
   gA1_0->SetMarkerStyle(22);
   gA1_0->SetMarkerColor(4004);
   gA1_0->SetLineColor(4004);
   mgUVaA1->Add(gA1_0,"p");

   TGraphErrors * gA1_1 = new TGraphErrors(data_1[0].size(),&data_1[22][0],&data_1[iycol][0],&datazero[0]/*&data_1[1][0]*/,&data_1[ieycol][0]);
   gA1_1->SetMarkerStyle(22);
   gA1_1->SetMarkerColor(4005);
   gA1_1->SetLineColor(4005);
   mgUVaA1->Add(gA1_1,"p");

   TGraphErrors * gA1_2 = new TGraphErrors( data_2[0].size(),&data_2[22][0],&data_2[iycol][0],&datazero[0]/*&data_2[1][0]*/,&data_2[ieycol][0]);
   gA1_2->SetMarkerStyle(22);
   gA1_2->SetMarkerColor(4005);
   gA1_2->SetLineColor(4005);
   mgUVaA1->Add(gA1_2,"p");

   TGraphErrors * gA1_3 = new TGraphErrors(data_3[0].size(),&data_3[22][0],&data_3[iycol][0],&datazero[0]/*&data_3[1][0]*/,&data_3[ieycol][0]);
   gA1_3->SetMarkerStyle(22);
   gA1_3->SetMarkerColor(4006);
   gA1_3->SetLineColor(4005);
   mgUVaA1->Add(gA1_3,"p");

   mgUVaA1->SetTitle("A_{1};x");

   /// A2
   iycol  = 18;
   ieycol = 19;
   TMultiGraph *mgUVaA2 = new TMultiGraph();

   TGraphErrors * gA2 = new TGraphErrors(data[0].size(),&data[22][0],&data[iycol][0],&datazero[0]/*&data[1][0]*/,&data[ieycol][0]);
   gA2->SetMarkerStyle(22);
   mgUVaA2->Add(gA2,"p");

   TGraphErrors * gA2_0 = new TGraphErrors( data_0[0].size(),&data_0[22][0],&data_0[iycol][0],&datazero[0]/*&data_0[1][0]*/,&data_0[ieycol][0]);
   gA2_0->SetMarkerStyle(22);
   gA2_0->SetMarkerColor(4008);
   gA2_0->SetLineColor(4000);
   mgUVaA2->Add(gA2_0,"p");

   TGraphErrors * gA2_1 = new TGraphErrors(data_1[0].size(),&data_1[22][0],&data_1[iycol][0],&datazero[0]/*&data_1[1][0]*/,&data_1[ieycol][0]);
   gA2_1->SetMarkerStyle(22);
   gA2_1->SetMarkerColor(4009);
   gA2_1->SetLineColor(4001);
   mgUVaA2->Add(gA2_1,"p");

   TGraphErrors * gA2_2 = new TGraphErrors( data_2[0].size(),&data_2[22][0],&data_2[iycol][0],&datazero[0]/*&data_2[1][0]*/,&data_2[ieycol][0]);
   gA2_2->SetMarkerStyle(22);
   gA2_2->SetMarkerColor(4010);
   gA2_2->SetLineColor(4002);
   mgUVaA2->Add(gA2_2,"p");

   TGraphErrors * gA2_3 = new TGraphErrors(data_3[0].size(),&data_3[22][0],&data_3[iycol][0],&datazero[0]/*&data_3[1][0]*/,&data_3[ieycol][0]);
   gA2_3->SetMarkerStyle(22);
   gA2_3->SetMarkerColor(4011);
   gA2_3->SetLineColor(4003);
   mgUVaA2->Add(gA2_3,"p");

   mgUVaA2->SetTitle("A_{2};x");

   // ------------------------------------------

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/bg_corrected_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);
   TList * fParaRCs = (TList *) gROOT->FindObject(Form("elastic-RCs_para47-%d",0));
   if(!fParaRCs) return(-1);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);
   TList * fPerpRCs = (TList *) gROOT->FindObject(Form("elastic-RCs_perp47-%d",0));
   if(!fPerpRCs) return(-2);
   //fPerpAsymmetries->Print();

   std::cout << "DONE" << std::endl;
   TFile * fout = new TFile(Form("data/A1A2_47_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fA90Asymmetries = new TList();

   TH1F * fA90 = 0;
   TGraphErrors * gr = 0;

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para47");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      std::cout << jj << std::endl;
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));

      TH1F * fApara = paraAsym->fAsymmetryVsW;
      TH1F * fAperp = perpAsym->fAsymmetryVsW;

      InSANEAveragedKinematics1D * paraKine = paraAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsW; 

      InSANERadiativeCorrections1D * paraRC = (InSANERadiativeCorrections1D*)fParaRCs->FindObject(Form("rc-W-%d",jj));
      InSANERadiativeCorrections1D * perpRC = (InSANERadiativeCorrections1D*)fPerpRCs->FindObject(Form("rc-W-%d",jj));

      TGraph * g1 = 0;
      fA180Calc = paraRC->fAsymBorn;
      g1 = new TGraph(fA180Calc);
      g1->SetLineColor(2);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      mg->Add(g1,"l");

      fA80Calc  = perpRC->fAsymBorn;
      g1 = new TGraph(fA80Calc);
      g1->SetLineColor(2);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      mg2->Add(g1,"l");

      fA90 = new TH1F(*fApara);
      fA90->SetNameTitle(Form("fA90-%d",jj),Form("A90-%d",jj));
      fA90Asymmetries->Add(fA90);

      Int_t   xBinmax = fApara->GetNbinsX();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax; i++){

               bin   = fApara->GetBin(i);

               Double_t W1     = paraKine->fW->GetBinContent(bin);
               Double_t x1     = paraKine->fx->GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi->GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2->GetBinContent(bin);
               Double_t W2     = perpKine->fW->GetBinContent(bin);
               Double_t x2     = perpKine->fx->GetBinContent(bin);
               Double_t phi2   = perpKine->fPhi->GetBinContent(bin);
               Double_t Q22    = perpKine->fQ2->GetBinContent(bin);
               Double_t W      = (W1+W2)/2.0;
               Double_t x      = (x1+x2)/2.0;
               Double_t phi    = (phi1+phi2)/2.0;
               Double_t Q2     = (Q21+Q22)/2.0;

               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               Double_t A180  = fApara->GetBinContent(bin);
               Double_t A80   = fAperp->GetBinContent(bin);

               Double_t eA180  = fApara->GetBinError(bin);
               Double_t eA80   = fAperp->GetBinError(bin);

               Double_t A90    = (A180*TMath::Cos(80.0*degree) + A80)/(TMath::Sin(80*degree)*TMath::Cos(phi2)); 
               Double_t eA90    = TMath::Sqrt(TMath::Power(eA180*TMath::Cos(80.0*degree),2.0) + eA80*eA80)/(TMath::Sin(80*degree)*TMath::Cos(phi2)); 


               fA90->SetBinContent(bin,A90);
               fA90->SetBinError(bin,eA90);

      }

      TH1 * h1 = fApara; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      TH1 * h2 = fAperp; 
      gr2 = new TGraphErrors(h2);
      gr2->SetMarkerStyle(24);
      for( int j = gr2->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr2->RemovePoint(j);
         }
      }
      mg->Add(gr,"p");
      leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");

      mg2->Add(gr2,"p");

      gr = new TGraphErrors(fA90);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }
      gr->SetMarkerStyle(20);
      mg3->Add(gr,"p");

   }

   //fout->WriteObject(fA90Asymmetries,Form("A90-47-%d",0));//,TObject::kSingleKey); 

   // --------------- A1 ----------------------
   c->cd(1);

   mg->Add(mgUVaA1,"p");

   TString Measurement = Form("A_{180}^{p}");
   TString GTitle      = Form("Preliminary %s, E=4.7 GeV",Measurement.Data());
   TString xAxisTitle  = Form("W");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   // Draw everything 
   mg->Draw("AP");
   mg->SetTitle(GTitle);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle();
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle();
   mg->GetXaxis()->SetLimits(1.0,3.0); 
   mg->GetYaxis()->SetRangeUser(0.0,1.0); 
   mg->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=4.7 GeV"); 
   //mg->GetXaxis()->SetTitle("W");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   c->cd(2);
   TMultiGraph *MG2 = new TMultiGraph(); 

   TLegend *leg2 = new TLegend(0.84, 0.15, 0.99, 0.85);
   leg2->AddEntry((TGraphErrors*)mg2->GetListOfGraphs()->At(0),"A80","lp");
   leg2->AddEntry((TGraphErrors*)mg3->GetListOfGraphs()->At(0),"A90","lp");
   leg2->AddEntry((TGraphErrors*)mgUVaA2->GetListOfGraphs()->At(0),"UVa A90","lp");


   MG2->Add(mg2);
   MG2->Add(mg3,"p");
   MG2->Add(mgUVaA2,"p");
   MG2->SetTitle("Preliminary A_{80}, E=4.7 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(1.0,3.0); 
   MG2->GetYaxis()->SetRangeUser(-0.4,0.4); 
   MG2->GetYaxis()->SetTitle("A_{90}^{p}");
   MG2->GetXaxis()->SetTitle("W");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   leg2->Draw();
   c->Update();

   c->SaveAs(Form("results/asymmetries/A90_47_W_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A90_47_W_%d.png",aNumber));

   return(0);
}
