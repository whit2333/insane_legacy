Int_t A1A2_59_noel(Int_t paraRunGroup = 313,Int_t perpRunGroup = 313,Int_t aNumber=313){

   // -------------------------------
   // UVA data
   std::string line;
   const char * uvafile = "asym_results/SANEDAT4.csv";
   std::cout << " Using UVa file : " << uvafile << std::endl;
   ifstream infile(uvafile);
   Double_t val;
   std::vector< vector<double> > data;
   data.resize(25);
   std::vector<string> colnames;
   string acolname;
   /// skip the first line.
   std::getline(infile, line);
   std::istringstream iss1(line);
   int ncol = 0;
   while (iss1 >> acolname){
      std::cout << ncol << " " << acolname << "\n";
      ncol++; 
   }
   /// read in the data.
   while( std::getline(infile, line) ) {
      std::istringstream iss(line);
      double n;
      int col = 0;
      while (iss >> n){
         data[col].push_back(n);
         //std::cout << n << " ";
         col++;   
      }
      //std::cout << "\n";
   }
   std::vector<double  > datazero;
   for(int i = 0; i<100;i++){ datazero.push_back(0.0);}
   std::vector< vector<double> > data1;
   data1.resize(25);
   std::vector< vector<double> > data_0;
   std::vector< vector<double> > data_1;
   std::vector< vector<double> > data_2;
   std::vector< vector<double> > data_3;
   data_0.resize(25);
   data_1.resize(25);
   data_2.resize(25);
   data_3.resize(25);
//    Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
//    Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t QsqMin[4] = {1.5,2.5,3.5,4.5};
   Double_t QsqMax[4] = {2.5,3.5,4.5,5.5};
   for(int i =0; i < data[0].size(); i++) {
      for(int j=0;j<25;j++){
         if( data[2][i] < QsqMax[0] && data[2][i] > QsqMin[0] ) {
           data_0[j].push_back(data[j][i]);
         }
         if( data[2][i] < QsqMax[1] && data[2][i] > QsqMin[1] ) {
           data_1[j].push_back(data[j][i]);
         }
         if( data[2][i] < QsqMax[2] && data[2][i] > QsqMin[2] ) {
           data_2[j].push_back(data[j][i]);
         }
         if( data[2][i] < QsqMax[3] && data[2][i] > QsqMin[3] ) {
           data_3[j].push_back(data[j][i]);
         }
      }
   }



   /// A1
   TMultiGraph *mgUVaA1 = new TMultiGraph();
   TGraphErrors * gA1  = new TGraphErrors(data[0].size(),&data[0][0],&data[6][0],&datazero[0]/*&data[1][0]*/,&data[7][0]);
   gA1->SetMarkerStyle(22);
   mgUVaA1->Add(gA1,"p");

   TGraphErrors * gA1_0 = new TGraphErrors( data_0[0].size(),&data_0[0][0],&data_0[6][0],&datazero[0]/*&data_0[1][0]*/,&data_0[7][0]);
   gA1_0->SetMarkerStyle(22);
   gA1_0->SetMarkerColor(4004+4);
   gA1_0->SetLineColor(4004+4);
   mgUVaA1->Add(gA1_0,"p");

   TGraphErrors * gA1_1 = new TGraphErrors(data_1[0].size(),&data_1[0][0],&data_1[6][0],&datazero[0]/*&data_1[1][0]*/,&data_1[7][0]);
   gA1_1->SetMarkerStyle(22);
   gA1_1->SetMarkerColor(4005+4);
   gA1_1->SetLineColor(4005+4);
   mgUVaA1->Add(gA1_1,"p");

   TGraphErrors * gA1_2 = new TGraphErrors( data_2[0].size(),&data_2[0][0],&data_2[6][0],&datazero[0]/*&data_2[1][0]*/,&data_2[7][0]);
   gA1_2->SetMarkerStyle(22);
   gA1_2->SetMarkerColor(4006+4);
   gA1_2->SetLineColor(4006+4);
   mgUVaA1->Add(gA1_2,"p");

   TGraphErrors * gA1_3 = new TGraphErrors(data_3[0].size(),&data_3[0][0],&data_3[6][0],&datazero[0]/*&data_3[1][0]*/,&data_3[7][0]);
   gA1_3->SetMarkerStyle(22);
   gA1_3->SetMarkerColor(4007+4);
   gA1_3->SetLineColor(4007+4);
   mgUVaA1->Add(gA1_3,"p");

   mgUVaA1->SetTitle("A_{1};x");

   /// A2
   TMultiGraph *mgUVaA2 = new TMultiGraph();

   TGraphErrors * gA2 = new TGraphErrors(data[0].size(),&data[0][0],&data[10][0],&datazero[0]/*&data[1][0]*/,&data[11][0]);
   gA2->SetMarkerStyle(22);
   mgUVaA2->Add(gA2,"p");

   TGraphErrors * gA2_0 = new TGraphErrors( data_0[0].size(),&data_0[0][0],&data_0[10][0],&datazero[0]/*&data_0[1][0]*/,&data_0[11][0]);
   gA2_0->SetMarkerStyle(22);
   gA2_0->SetMarkerColor(4004+4);
   gA2_0->SetLineColor(4004+4);
   mgUVaA2->Add(gA2_0,"p");

   TGraphErrors * gA2_1 = new TGraphErrors(data_1[0].size(),&data_1[0][0],&data_1[10][0],&datazero[0]/*&data_1[1][0]*/,&data_1[11][0]);
   gA2_1->SetMarkerStyle(22);
   gA2_1->SetMarkerColor(4005+4);
   gA2_1->SetLineColor(4005+4);
   mgUVaA2->Add(gA2_1,"p");

   TGraphErrors * gA2_2 = new TGraphErrors( data_2[0].size(),&data_2[0][0],&data_2[10][0],&datazero[0]/*&data_2[1][0]*/,&data_2[11][0]);
   gA2_2->SetMarkerStyle(22);
   gA2_2->SetMarkerColor(4006+4);
   gA2_2->SetLineColor(4006+4);
   mgUVaA2->Add(gA2_2,"p");

   TGraphErrors * gA2_3 = new TGraphErrors(data_3[0].size(),&data_3[0][0],&data_3[10][0],&datazero[0]/*&data_3[1][0]*/,&data_3[11][0]);
   gA2_3->SetMarkerStyle(22);
   gA2_3->SetMarkerColor(4007+4);
   gA2_3->SetLineColor(4007+4);
   mgUVaA2->Add(gA2_3,"p");

   mgUVaA2->SetTitle("A_{2};x");



   //-------------------------------
   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);
   //fPerpAsymmetries->Print();

   std::cout << "DONE" << std::endl;
   TFile * fout = new TFile(Form("data/A1A2_59_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fA1Asymmetries = new TList();
   TList * fA2Asymmetries = new TList();

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 4;jj<fParaAsymmetries->GetEntries() && jj <=4 ;jj++) {

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));

      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));

      TH1F * fApara = paraAsym->fAsymmetryVsx;
      TH1F * fAperp = perpAsym->fAsymmetryVsx;

      InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsx; 

      fA1 = new TH1F(*fApara);
      fA2 = new TH1F(*fAperp);

      fA1->SetNameTitle(Form("fA1-%d",jj),Form("A1-%d",jj));
      fA2->SetNameTitle(Form("fA2-%d",jj),Form("A2-%d",jj));

      fA1Asymmetries->Add(fA1);
      fA2Asymmetries->Add(fA2);

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax; i++){
         for(Int_t j=0; j<= yBinmax; j++){
            for(Int_t k=0; k<= zBinmax; k++){

               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);

               Double_t W1     = paraKine->fW->GetBinContent(bin);
               Double_t x1     = paraKine->fx->GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi->GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2->GetBinContent(bin);
               Double_t W2     = perpKine->fW->GetBinContent(bin);
               Double_t x2     = perpKine->fx->GetBinContent(bin);
               Double_t phi2   = perpKine->fPhi->GetBinContent(bin);
               Double_t Q22    = perpKine->fQ2->GetBinContent(bin);
               Double_t W      = (W1+W2)/2.0;
               Double_t x      = (x1+x2)/2.0;
               Double_t phi    = (phi1+phi2)/2.0;
               Double_t Q2     = (Q21+Q22)/2.0;

               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               Double_t A180  = fApara->GetBinContent(bin);
               Double_t A80   = fAperp->GetBinContent(bin);

               Double_t eA180  = fApara->GetBinError(bin);
               Double_t eA80   = fAperp->GetBinError(bin);

               Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               Double_t R        = SFs->R(x,Q2);
               Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               Double_t cota     = 1.0/TMath::Tan(alpha);
               Double_t csca     = 1.0/TMath::Sin(alpha);

               Double_t c0 = 1.0/(1.0 + eta*xi);
               Double_t c11 = (1.0 + cota*chi)/D ;
               Double_t c12 = (csca*chi)/D ;
               Double_t c21 = (xi - cota*chi/eta)/D ;
               Double_t c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               //A1 = A180*(1/D);
               Double_t A1 = c0*(c11*A180 + c12*A80);
               Double_t A2 = c0*(c21*A180 + c22*A80);

               Double_t eA1 = TMath::Sqrt( TMath::Power(c0*c11*eA180,2.0) + TMath::Power(c0*c12*eA80,2.0));
               Double_t eA2 = TMath::Sqrt( TMath::Power(c0*c21*eA180,2.0) + TMath::Power(c0*c22*eA80,2.0));
               //eA2 = c0*(c21*eA180 + c22*eA80);
               //A1 = c0*(A180/D - eta*A80/d);
               //A2 = c0*(xi*A180/D + A80/d);

               if ( A1 != A1 ) A1 = 0;
               if ( A2 != A2 ) A2 = 0;

               //             std::cout << "theta = " << theta << "\n";
               //             
               fA1->SetBinContent(bin,A1);
               fA2->SetBinContent(bin,A2);
               fA1->SetBinError(bin,eA1);
               fA2->SetBinError(bin,eA2);

            }
         }
      }

      TH1 * h1 = fA1; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         Double_t ye = gr->GetErrorY(j);
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
         if( ye > 1.1 ) {
            gr->RemovePoint(j);
         }
      }

      TH1 * h2 = fA2; 
      gr2 = new TGraphErrors(h2);
      for( int j = gr2->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         Double_t ye = gr2->GetErrorY(j);
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr2->RemovePoint(j);
         }
         if( ye > 1.1 ) {
            gr2->RemovePoint(j);
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj == 4 ) {
         gr->SetLineColor(1);
         gr->SetMarkerColor(1);
      }
      mg->Add(gr,"p");
      leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");

      gr2->SetMarkerStyle(21);
      gr2->SetMarkerSize(0.6);
      gr2->SetMarkerColor(fA1->GetMarkerColor());
      gr2->SetLineColor(fA1->GetMarkerColor());
      mg2->Add(gr2,"p");

   }


   Double_t Q2 = 4.0;

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);

   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  
   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 

   // --------------- A1 ----------------------
   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   TF1 * Model1 = new TF1("Model1",Asym1,&InSANEAsymmetryBase::EvaluateA1p,
         xmin1, xmax1, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model2 = new TF1("Model2",Asym2,&InSANEAsymmetryBase::EvaluateA1p,
         xmin2, xmax2, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model3 = new TF1("Model3",Asym3,&InSANEAsymmetryBase::EvaluateA1p,
         xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model4= new TF1("Model4",Asym4,&InSANEAsymmetryBase::EvaluateA1p,
         xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");

   TString Title;
   Int_t width = 1;

   Model1->SetParameter(0,Q2);
   Model1->SetLineColor(4);
   Model1->SetLineWidth(width);
   Model1->SetLineStyle(1);
   Model1->SetTitle(Title);
   Model1->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model4->SetParameter(0,Q2);
   Model4->SetLineColor(4);
   Model4->SetLineWidth(width);
   Model4->SetLineStyle(2);
   Model4->SetTitle(Title);
   Model4->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model2->SetParameter(0,Q2);
   Model2->SetLineColor(1);
   Model2->SetLineWidth(width);
   Model2->SetLineStyle(1);
   Model2->SetTitle(Title);
   Model2->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model3->SetParameter(0,Q2);
   Model3->SetLineColor(1);
   Model3->SetLineWidth(width);
   Model3->SetLineStyle(2);
   Model3->SetTitle(Title);
   Model3->GetYaxis()->SetRangeUser(ymin,ymax); 

   // Load in world data on A1He3 from NucDB  
   gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   TMultiGraph *MG = new TMultiGraph(); 
   TList * measurementsList =  new TList(); //manager->GetMeasurements("A1p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   // SLAC E143
   exp = manager->GetExperiment("SLAC_E143"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC_E143"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("CLAS-E93009"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1He3World = (NucDBMeasurement*)measurementsList->At(i);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1He3World->BuildGraph("x");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A1He3World->GetExperimentName()); 
      //leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      //MG->Add(graph); 
   }
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("A_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   MG->Add(mgUVaA1,"p");
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(0.0,1.0); 
   MG->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");
   c->SaveAs(Form("results/asymmetries/%d/A1_noel_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A1_noel_%d.png",aNumber,aNumber));

   //--------------- A2 ---------------------
   c = new TCanvas("cA2","A2_para59");
   TMultiGraph *MG2 = new TMultiGraph(); 
   measurementsList = manager->GetMeasurements("A2p");
   //measurementsList->Print();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A2pMeas->BuildGraph("x");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A2pMeas->GetExperimentName()); 
      //leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      //MG2->Add(graph,"p"); 
   }
   MG2->Add(mg2,"p");
   MG2->Add(mgUVaA2,"p");
   MG2->SetTitle("Preliminary A_{2}^{p}, E=5.9 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   MG2->GetYaxis()->SetRangeUser(-1.0,1.0); 
   MG2->GetYaxis()->SetTitle("A_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();

   c->Update();

   c->SaveAs(Form("results/asymmetries/A2_noel_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A2_noel_%d.png",aNumber));

   return(0);
}
