#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

Int_t A180A80_47(Int_t paraRunGroup = 7609,Int_t perpRunGroup = 7609,Int_t aNumber=7609){

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   const char * parafile = Form("data/binned_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->ls();
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/binned_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);

   std::cout << "DONE" << std::endl;

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TList * SystHists1 = new TList();
   TList * SystHists2 = new TList();

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.17, 0.70, 0.40, 0.88);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetMeasuredAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetMeasuredAsymmetryResult();

      TH1F * fApara = &paraAsym->fAsymmetryVsx;
      TH1F * fAperp = &perpAsym->fAsymmetryVsx;

      InSANEAveragedKinematics1D * paraKine = &perpAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsx; 

      paraAsym->fSystErrVsx.SetLineColor(fApara->GetLineColor());
      perpAsym->fSystErrVsx.SetLineColor(fApara->GetLineColor());

      paraAsym->fSystErrVsx.SetFillColor(fApara->GetLineColor());
      perpAsym->fSystErrVsx.SetFillColor(fApara->GetLineColor());

      SystHists1->Add(&paraAsym->fSystErrVsx);
      SystHists2->Add(&perpAsym->fSystErrVsx);

      // ------------------------------------
      gr = new TGraphErrors(fApara);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = paraAsym->fSystErrVsx.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         Double_t eyt_sys = paraAsym->fSystErrVsx.GetBinContent(abin);
         if( yt == 0.0 || yt<0.0 || eyt_sys>1.0 || eyt>1.0 ) {
            gr->RemovePoint(j);
            paraAsym->fSystErrVsx.SetBinContent(abin,0.0);
            paraAsym->fSystErrVsx.SetBinError(abin,0.0);
            continue;
         }
      }
      if(jj==4) {
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
      }
      if(jj<4) {
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("#LTQ^{2}#GT=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }
       

      // ------------------------------------
      gr = new TGraphErrors(fAperp);
      for( int j = gr->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = perpAsym->fSystErrVsx.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         Double_t eyt_sys = perpAsym->fSystErrVsx.GetBinContent(abin);
         if( yt == 0.0 || eyt_sys>1.0 || eyt>1.0 ) {
            gr->RemovePoint(j);
            perpAsym->fSystErrVsx.SetBinContent(abin,0.0);
            perpAsym->fSystErrVsx.SetBinError(abin,0.0);
            continue;
         }
      }

      if(jj==4) {
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
      }
      if(jj<4) {
         mg2->Add(gr,"p");
      }

   }

   // -----------------------------------------------------------------
   // A180

   TCanvas * c = new TCanvas();
   //c->SetFillColor(4);
   TMultiGraph *MG = new TMultiGraph(); 

   MG->Add(mg);
   MG->Draw("AP");
   MG->SetTitle("");//A_{180}^{p},  E=4.7 GeV");
   MG->GetXaxis()->SetTitle("x");
   MG->GetXaxis()->CenterTitle(true);
   MG->GetYaxis()->SetTitle("");
   MG->GetYaxis()->CenterTitle(true);
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.5,1.19); 
   MG->Draw("AP");

   TMultiGraph * mg_syst_err = new TMultiGraph();
   for(int i=0;i<SystHists1->GetEntries()&&i<4;i++){
      TH1 * hsys2 = (TH1*)SystHists1->At(i);
      mg_syst_err->Add(stat_error_graph(hsys2,-0.4),"e3");
   }
   MG->Add(mg_syst_err);

   TLatex l;
   l.SetTextSize(0.05);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(-15);
   //l.DrawLatex(0.05,0.2,"systematic errors #rightarrow");

   leg->Draw();

   c->SaveAs(Form("results/asymmetries/%d/A180A80_47_0_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A180A80_47_0_%d.png",aNumber,aNumber));

   // ------------------------------------------
   // A80
   c = new TCanvas();
   TMultiGraph *MG2 = new TMultiGraph(); 
   MG2->Add(mg2,"p");
   MG2->SetTitle("");//A_{80}^{p},  E=4.7 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   MG2->GetYaxis()->SetRangeUser(-0.79,0.9); 
   MG2->GetYaxis()->SetTitle("");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();

   TMultiGraph * mg2_syst_err = new TMultiGraph();
   for(int i=0;i<SystHists2->GetEntries()&&i<4;i++){
      TH1 * hsys2 = (TH1*)SystHists2->At(i);
      mg2_syst_err->Add(stat_error_graph(hsys2,-0.7),"e3");
   }
   MG2->Add(mg2_syst_err);
   leg->Draw();
   c->Update();

   c->SaveAs(Form("results/asymmetries/%d/A180A80_47_1_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A180A80_47_1_%d.png",aNumber,aNumber));

   return(0);
}
