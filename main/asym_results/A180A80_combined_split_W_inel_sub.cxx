#include "util/syst_error_band.cxx"
#include "asym/sane_data_bins.cxx"
#include "util/combine_results.cxx"

Int_t A180A80_combined_split_W_inel_sub(Int_t paraRunGroup = 8209,Int_t perpRunGroup = 8209,Int_t aNumber=8209){

  TH1::AddDirectory(false);
   //gStyle->SetPadGridX(true);
   //gStyle->SetPadGridY(true);

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   InSANEFunctionManager    * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs  = fman->GetStructureFunctions();

   TFile * f1 = TFile::Open(Form("data/A180A80_59_split_combine_%d.root",paraFileVersion),"READ");
   f1->cd();
   TList * fPara59Asymmetries = (TList *) gROOT->FindObject("A180_59_split_combine_inel_sub");
   if(!fPara59Asymmetries) return(-1);

   TList * fPerp59Asymmetries = (TList *) gROOT->FindObject("A80_59_split_combine_inel_sub");
   if(!fPerp59Asymmetries) return(-2);

   TFile * f2 = TFile::Open(Form("data/A180A80_47_split_combine_%d.root",paraFileVersion),"READ");
   f2->cd();
   TList * fPara47Asymmetries = (TList *) gROOT->FindObject("A180_47_split_combine_inel_sub");
   if(!fPara47Asymmetries) return(-3);

   TList * fPerp47Asymmetries = (TList *) gROOT->FindObject("A80_47_split_combine_inel_sub");
   if(!fPerp47Asymmetries) return(-4);



   TList * A180_combined_list = new TList();
   TList * A80_combined_list  = new TList();
   //A180_combined_list->SetOwner(false);
   //A80_combined_list ->SetOwner(false);
   TMultiGraph * mg  = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg_syst  = new TMultiGraph();
   TMultiGraph * mg2_syst  = new TMultiGraph();

   TMultiGraph * mg_split       = new TMultiGraph();
   TMultiGraph * mg2_split      = new TMultiGraph();
   TMultiGraph * mg_split_syst  = new TMultiGraph();
   TMultiGraph * mg2_split_syst = new TMultiGraph();

   TLegend *leg = new TLegend(0.6, 0.75, 0.85, 0.9);
   //leg->SetBorderSize(0);
   
   std::string table_dir0 = "results/asymmetries/tables/";
   table_dir0            += std::to_string(aNumber);
   table_dir0            += "/combined_split_inel_sub";
   table_dir0            += "/A180A80_combined/";
   gSystem->mkdir(table_dir0.c_str(),true);

   gSystem->mkdir(Form("results/asymmetries/%d",aNumber),true);

   CombineParameters combine_pars{1.8,1.8, 0.4, 0.4, 0.4, 0.4};

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fPara59Asymmetries->GetEntries();jj++) {


      InSANEMeasuredAsymmetry         * para59Asym        = (InSANEMeasuredAsymmetry*)(fPara59Asymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perp59Asym        = (InSANEMeasuredAsymmetry*)(fPerp59Asymmetries->At(jj));
      InSANEMeasuredAsymmetry         * para47Asym        = (InSANEMeasuredAsymmetry*)(fPara47Asymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perp47Asym        = (InSANEMeasuredAsymmetry*)(fPerp47Asymmetries->At(jj));

      InSANEMeasuredAsymmetry         * paraAsym = new InSANEMeasuredAsymmetry( *para59Asym );
      InSANEMeasuredAsymmetry         * perpAsym = new InSANEMeasuredAsymmetry( *perp59Asym );
      A180_combined_list->Add(paraAsym);
      A80_combined_list->Add( perpAsym); 

      double Q2_para = (para59Asym->GetQ2() + para47Asym->GetQ2())/2.0;
      double Q2_perp = (perp59Asym->GetQ2() + perp47Asym->GetQ2())/2.0;

      // --------------------------------------------------------
      // Combine the two beam energies
      // Parallel:
      auto para_res = combine_results( combine_pars,
          &para59Asym->fAsymmetryVsW,  &para47Asym->fAsymmetryVsW, 
          &para59Asym->fSystErrVsW,    &para47Asym->fSystErrVsW,
          &para59Asym->fMask_W,        &para47Asym->fMask_W,
          &para59Asym->fAvgKineVsW,    &para47Asym->fAvgKineVsW,
          &para59Asym->fSystematics_W, &para47Asym->fSystematics_W);

      TH1F * combined_Apara      = std::get<0>(para_res);
      TH1F * combined_Apara_syst = std::get<1>(para_res);
      paraAsym->fAsymmetryVsW    = *combined_Apara     ;
      paraAsym->fSystErrVsW      = *combined_Apara_syst;
      paraAsym->fAvgKineVsW      = std::get<2>(para_res);
      paraAsym->fSystematics_W   = std::get<3>(para_res);

      std::string   filename0       = Form("A180_combined_%d_%d.txt",aNumber,jj);
      std::string   table_filename0 = table_dir0 + filename0;
      std::ofstream outtable0(table_filename0.c_str());
      paraAsym->PrintBinnedTableHead(outtable0);
      paraAsym->PrintBinnedTable(outtable0);

      // Perpendicular:
      auto perp_res = combine_results( combine_pars,
          &perp59Asym->fAsymmetryVsW,  &perp47Asym->fAsymmetryVsW, 
          &perp59Asym->fSystErrVsW,    &perp47Asym->fSystErrVsW,
          &perp59Asym->fMask_W,        &perp47Asym->fMask_W,
          &perp59Asym->fAvgKineVsW,    &perp47Asym->fAvgKineVsW,
          &perp59Asym->fSystematics_W, &perp47Asym->fSystematics_W);

      TH1F * combined_Aperp      = std::get<0>(perp_res);
      TH1F * combined_Aperp_syst = std::get<1>(perp_res);
      perpAsym->fAsymmetryVsW    = *combined_Aperp     ;
      perpAsym->fSystErrVsW      = *combined_Aperp_syst;
      perpAsym->fAvgKineVsW      = std::get<2>(perp_res);
      perpAsym->fSystematics_W   = std::get<3>(perp_res);

      filename0       = Form("A80_combined_%d_%d.txt",aNumber,jj);
      table_filename0 = table_dir0 + filename0;
      std::ofstream outtable1(table_filename0.c_str());
      perpAsym->PrintBinnedTableHead(outtable1);
      perpAsym->PrintBinnedTable(outtable1);


      // ---------------------------------
      // Some graphs
      TGraphErrors * gr_para      = new TGraphErrors( combined_Apara );
      TGraphErrors * gr_para_syst = syst_error_band(combined_Apara_syst, -1.0,0.4);

      // ---------------------------------
      // Clean up graphs 
      // Remove the zeros
      for( int j = gr_para->GetN()-1 ; j>=0; j--) {
         double xt, yt;
         gr_para->GetPoint(j,xt ,yt);
         int    abin     = combined_Apara->FindBin(xt);
         double eyt      = gr_para->GetErrorY(j);
         double eyt_sys  = combined_Apara_syst->GetBinContent(abin);

         double y2 = combined_Aperp->GetBinContent(abin);
         if( (yt == 0.0) || (y2 == 0) ) {
            gr_para->RemovePoint(j);
            gr_para_syst->RemovePoint(j);
            combined_Apara     ->SetBinContent(abin,0.0);
            combined_Apara_syst->SetBinError(  abin,0.0);
            continue;
         }
      }

      TGraphErrors * gr_perp      = new TGraphErrors( combined_Aperp );
      TGraphErrors * gr_perp_syst = syst_error_band(combined_Aperp_syst, -1.0,0.4);
      for( int j = gr_perp->GetN()-1 ; j>=0; j--) {
         double xt, yt;
         gr_perp->GetPoint(j,xt ,yt);
         int    abin    = combined_Aperp->FindBin(xt);
         double eyt     = gr_perp->GetErrorY(j);
         double eyt_sys = combined_Aperp_syst->GetBinContent(abin);

         double y2 = combined_Apara->GetBinContent(abin);
         if( (yt == 0.0) || (y2 == 0) ) {
            gr_perp->RemovePoint(j);
            gr_perp_syst->RemovePoint(j);
            combined_Aperp     ->SetBinContent(abin,0.0);
            combined_Aperp_syst->SetBinError(  abin,0.0);
            continue;
         }
      }

      // ---------------------------------
      // A180
      if(jj==4) {
         gr_para->SetMarkerColor(1);
         gr_para->SetMarkerStyle(24);
         gr_para->SetLineColor(1);

         gr_para_syst->SetLineColor(1);
         gr_para_syst->SetFillColorAlpha(1,0.2);

      } else if( jj >4 && jj < 9 ) {

         gr_para->SetMarkerColor(4008+jj-4);
         gr_para->SetLineColor(  4008+jj-4);
         gr_para->SetMarkerStyle(23);

         mg_split->Add(gr_para,"p");

         gr_para_syst->SetLineColor(  4008+jj-4);
         gr_para_syst->SetFillColorAlpha(  4008+jj-4,0.3);
         mg_split_syst->Add(gr_para_syst,"e3");

      } else if( jj >9 && jj < 14 ) {

         gr_para->SetMarkerColor(4008+jj-9);
         gr_para->SetLineColor(  4008+jj-9);
         gr_para->SetMarkerStyle(22);

         mg_split->Add(gr_para,"p");

         gr_para_syst->SetLineColor(      4008+jj-9);
         gr_para_syst->SetFillColorAlpha( 4008+jj-9,0.3);
         mg_split_syst->Add(gr_para_syst,"e3");
      }
      if(jj<4) {

         gr_para->SetMarkerColor(combined_Apara->GetMarkerColor());
         gr_para->SetLineColor(combined_Apara->GetMarkerColor());

         gr_para_syst->SetLineColor(combined_Apara->GetMarkerColor());
         gr_para_syst->SetFillColorAlpha(combined_Apara->GetMarkerColor(),0.3);

         mg->Add(gr_para,"p0");
         mg_syst->Add(gr_para_syst,"e3");
         leg->AddEntry(gr_para,Form("#LTQ^{2}#GT=%.1f GeV^{2}",Q2_para),"lp");
      }

      // ---------------------------------
      // A80
      if(jj==4) {
         gr_perp->SetMarkerColor(1);
         gr_perp->SetMarkerStyle(24);
         gr_perp->SetLineColor(1);

         gr_perp_syst->SetLineColor(1);
         gr_perp_syst->SetFillColorAlpha(1,0.2);

      } else if( jj >4 && jj < 9 ) {

         gr_perp->SetMarkerColor(4008+jj-4);
         gr_perp->SetLineColor(  4008+jj-4);
         gr_perp->SetMarkerStyle(23);

         mg2_split->Add(gr_perp,"p");

         gr_perp_syst->SetLineColor(  4008+jj-4);
         gr_perp_syst->SetFillColorAlpha(  4008+jj-4,0.3);
         mg2_split_syst->Add(gr_perp_syst,"e3");

      } else if( jj >9 && jj < 14 ) {

         gr_perp->SetMarkerColor(4008+jj-9);
         gr_perp->SetLineColor(  4008+jj-9);
         gr_perp->SetMarkerStyle(22);

         mg2_split->Add(gr_perp,"p");

         gr_perp_syst->SetLineColor(      4008+jj-9);
         gr_perp_syst->SetFillColorAlpha( 4008+jj-9,0.3);
         mg2_split_syst->Add(gr_perp_syst,"e3");
      }
      if(jj<4) {

         gr_perp->SetMarkerColor(combined_Apara->GetMarkerColor());
         gr_perp->SetLineColor(combined_Apara->GetMarkerColor());

         gr_perp_syst->SetLineColor(combined_Apara->GetMarkerColor());
         gr_perp_syst->SetFillColorAlpha(combined_Apara->GetMarkerColor(),0.3);

         mg2->Add(gr_perp,"p0");
         mg2_syst->Add(gr_perp_syst,"e3");
         //leg->AddEntry(gr_perp,Form("#LTQ^{2}#GT=%.1f GeV^{2}",perp59Asym->GetQ2()),"lp");
      }

   }


   // -----------------------------------------------------------------
   // A180

   TCanvas * c = new TCanvas("cA180A80_59","A180 A80 59");
   NucDBManager * manager = NucDBManager::GetManager();
   TMultiGraph *MG = new TMultiGraph(); 
   TString GTitle      = "A_{180} Combined Beam Energies";
   TString xAxisTitle  = "W";
   TString yAxisTitle  = "A_{180}";
   MG->Add(mg_syst);
   MG->Add(mg);
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(1.0,3.2); 
   //MG->GetYaxis()->SetRangeUser(-1.4,1.4); 
   MG->Draw("AP");
   TLatex l;
   //l.SetNDC(true);
   l.SetTextSize(0.04);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(0);
   //l.DrawLatex(3.2,0.1,"#splitline{systematic}{#leftarrow errors}");
   leg->Draw();

   c->SaveAs(Form("results/asymmetries/%d/A180A80_combined_split_W_inel_sub_0_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A180A80_combined_split_W_inel_sub_0_%d.png",aNumber,aNumber));

   // ------------------------------------------
   // A80

   c = new TCanvas();
   TMultiGraph *MG2 = new TMultiGraph(); 

   MG2->Add(mg2_syst);
   MG2->Add(mg2);
   MG2->SetTitle("A_{80}^{p} Combined Beam Energies");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(1.0,3.2); 
   //MG2->GetYaxis()->SetRangeUser(-1.4,1.4); 
   MG2->GetYaxis()->SetTitle("A_{80}^{p}");
   MG2->GetXaxis()->SetTitle("W");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();

   l.SetTextSize(0.04);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(0);
   //l.DrawLatex(3.2,0.1,"#splitline{systematic}{#leftarrow errors}");
   leg->Draw();

   c->SaveAs(Form("results/asymmetries/%d/A180A80_combined_split_W_inel_sub_1_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A180A80_combined_split_W_inel_sub_1_%d.png",aNumber,aNumber));

   TFile * fout = TFile::Open(Form("data/A180A80_combined_split_%d.root",paraFileVersion),"UPDATE");
   fout->WriteObject(A180_combined_list,"A180_combined_split_inel_sub");
   fout->WriteObject(A80_combined_list, "A80_combined_split_inel_sub");
   fout->Close();

   return(0);
}
