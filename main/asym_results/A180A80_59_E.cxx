Int_t A180A80_59_E(Int_t paraRunGroup = 30,Int_t perpRunGroup = 30,Int_t aNumber=30){

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);

   std::cout << "DONE" << std::endl;

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TCanvas * c = new TCanvas("cA180A80_59","A180 A80 59");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();

      InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();

      TH1F * fApara = paraAsym->fAsymmetryVsTheta;
      TH1F * fAperp = perpAsym->fAsymmetryVsTheta;

      InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsTheta;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsTheta; 

      TH1 * h1 = fApara; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      TH1 * h2 = fAperp; 
      gr2 = new TGraphErrors(h2);
      for( int j = gr2->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr2->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr2->RemovePoint(j);
         }
      }

      //gr->SetMarkerStyle(20);
      //gr->SetMarkerColor(fA1->GetMarkerColor());
      //gr->SetLineColor(fA1->GetMarkerColor());
      mg->Add(gr,"p");
      leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");

      //gr2->SetMarkerStyle(21);
      //gr2->SetMarkerColor(fA1->GetMarkerColor());
      //gr2->SetLineColor(fA1->GetMarkerColor());
      mg2->Add(gr2,"p");

   }

   Double_t Q2 = 4.0;

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);

   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  
   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 

   // --------------- A1 ----------------------
   c->cd(1);
   TF1 * Model1 = new TF1("Model1",Asym1,&InSANEAsymmetryBase::EvaluateA1p,
         xmin1, xmax1, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model2 = new TF1("Model2",Asym2,&InSANEAsymmetryBase::EvaluateA1p,
         xmin2, xmax2, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model3 = new TF1("Model3",Asym3,&InSANEAsymmetryBase::EvaluateA1p,
         xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model4= new TF1("Model4",Asym4,&InSANEAsymmetryBase::EvaluateA1p,
         xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");

   TString Title;
   Int_t width = 1;

   Model1->SetParameter(0,Q2);
   Model1->SetLineColor(4);
   Model1->SetLineWidth(width);
   Model1->SetLineStyle(1);
   Model1->SetTitle(Title);
   Model1->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model4->SetParameter(0,Q2);
   Model4->SetLineColor(4);
   Model4->SetLineWidth(width);
   Model4->SetLineStyle(2);
   Model4->SetTitle(Title);
   Model4->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model2->SetParameter(0,Q2);
   Model2->SetLineColor(1);
   Model2->SetLineWidth(width);
   Model2->SetLineStyle(1);
   Model2->SetTitle(Title);
   Model2->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model3->SetParameter(0,Q2);
   Model3->SetLineColor(1);
   Model3->SetLineWidth(width);
   Model3->SetLineStyle(2);
   Model3->SetTitle(Title);
   Model3->GetYaxis()->SetRangeUser(ymin,ymax); 

   // Load in world data on A1He3 from NucDB  
   gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   TMultiGraph *MG = new TMultiGraph(); 
   //TList * measurementsList = manager->GetMeasurements("A1p");
   //for (int i = 0; i < measurementsList->GetEntries(); i++) {
   //   NucDBMeasurement *A1He3World = (NucDBMeasurement*)measurementsList->At(i);
   //   // Get TGraphErrors object 
   //   TGraphErrors *graph        = A1He3World->BuildGraph("x");
   //   graph->SetMarkerStyle(27);
   //   // Set legend title 
   //   Title = Form("%s",A1He3World->GetExperimentName()); 
   //   leg->AddEntry(graph,Title,"lp");
   //   // Add to TMultiGraph 
   //   MG->Add(graph); 
   //}
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("A_{180}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   //MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(0.0,0.8); 
   MG->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   c->cd(2);
   TMultiGraph *MG2 = new TMultiGraph(); 
   //measurementsList = manager->GetMeasurements("A2p");
   //measurementsList->Print();
   //for (int i = 0; i < measurementsList->GetEntries(); i++) {
   //   NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
   //   // Get TGraphErrors object 
   //   TGraphErrors *graph        = A2pMeas->BuildGraph("x");
   //   graph->SetMarkerStyle(27);
   //   // Set legend title 
   //   Title = Form("%s",A2pMeas->GetExperimentName()); 
   //   //leg->AddEntry(graph,Title,"lp");
   //   // Add to TMultiGraph 
   //   MG2->Add(graph,"p"); 
   //}
   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary A_{80}^{p}, E=5.9 GeV");
   MG2->Draw("a");
   //MG2->GetXaxis()->SetLimits(0.0,1.0); 
   MG2->GetYaxis()->SetRangeUser(-0.2,0.2); 
   MG2->GetYaxis()->SetTitle("A_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   c->Update();

   c->SaveAs(Form("results/asymmetries/%d/A180A80_59_E_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A180A80_59_E_%d.png",aNumber,aNumber));

   return(0);
}
