#include "util/syst_error_band.cxx"
#include "asym/sane_data_bins.cxx"
#include "util/combine_results.cxx"

Int_t xg1g2_combined_W(Int_t RunGroup = 8209)
{
  TH1::AddDirectory(false);
  Int_t paraRunGroup = RunGroup;
  Int_t perpRunGroup = RunGroup;
  Int_t aNumber      = RunGroup;

  TList * g1_combined_list = 0;
  TList * g2_combined_list  = 0; 

  TFile * fin = TFile::Open(Form("data/g1g2_combined_%d.root",RunGroup),"UPDATE");
  fin->ls();
  fin->cd();
  g1_combined_list = (TList*)gROOT->FindObject("g1_combined_W");
  if(!g1_combined_list){  return -1; }          
  g2_combined_list  = (TList*)gROOT->FindObject("g2_combined_W");
  if(!g2_combined_list){  return -2; }

  TList        * fAllxg1Asym = new TList();
  TList        * fAllxg2Asym = new TList();
  TMultiGraph * mg1      = new TMultiGraph();
  TMultiGraph * mg1_syst = new TMultiGraph();
  TMultiGraph * mg2      = new TMultiGraph();
  TMultiGraph * mg2_syst = new TMultiGraph();

  TMultiGraph * mg_x2g1     = new TMultiGraph();
  TMultiGraph * mg_x2g2     = new TMultiGraph();

  Double_t alpha = 80.0*TMath::Pi()/180.0;

  std::string table_dir0 = "results/asymmetries/tables/";
  table_dir0            += std::to_string(aNumber);
  table_dir0            += "/combined_inel_sub";
  table_dir0            += "/xg1g2_combined/";
  gSystem->mkdir(table_dir0.c_str(),true);

  InSANEFunctionManager    * fman = InSANEFunctionManager::GetManager();
  InSANEStructureFunctions * SFs  = fman->GetStructureFunctions();

  for(int jj = 0;jj<g1_combined_list->GetEntries();jj++) {

    InSANEMeasuredAsymmetry         * g1Asym        = (InSANEMeasuredAsymmetry*)(g1_combined_list ->At(jj));
    InSANEMeasuredAsymmetry         * g2Asym        = (InSANEMeasuredAsymmetry*)(g2_combined_list  ->At(jj));

    TH1F * fg1 = &g1Asym->fAsymmetryVsW;
    TH1F * fg2 = &g2Asym->fAsymmetryVsW;

    InSANEAveragedKinematics1D * g1Kine = &g1Asym->fAvgKineVsW;
    InSANEAveragedKinematics1D * g2Kine = &g2Asym->fAvgKineVsW; 

    // A1 and A2 
    InSANEMeasuredAsymmetry * fxg1Full = new InSANEMeasuredAsymmetry( *g1Asym );
    InSANEMeasuredAsymmetry * fxg2Full = new InSANEMeasuredAsymmetry( *g2Asym );
    fAllxg1Asym->Add(fxg1Full);
    fAllxg2Asym->Add(fxg2Full);

    TH1F * fxg1 = (TH1F*)fxg1Full->fAsymmetryVsW.Clone();
    TH1F * fxg2 = (TH1F*)fxg2Full->fAsymmetryVsW.Clone();
    fxg1->Reset();
    fxg2->Reset();

    TGraphErrors * gr_x2g1 = new TGraphErrors(fxg1);
    TGraphErrors * gr_x2g2 = new TGraphErrors(fxg1);

    int xBinmax = fxg1->GetNbinsX();
    for(int i_bin=1; i_bin<= xBinmax; i_bin++){

      int gbin   = fg1->GetBin(i_bin);

      Double_t W1     = g1Kine->fW.GetBinContent(gbin);
      Double_t x1     = g1Kine->fx.GetBinContent(gbin);
      Double_t phi1   = g1Kine->fPhi.GetBinContent(gbin);
      Double_t Q21    = g1Kine->fQ2.GetBinContent(gbin);

      Double_t W2     = g2Kine->fW.GetBinContent(gbin);
      Double_t x2     = g2Kine->fx.GetBinContent(gbin);
      Double_t phi2   = g2Kine->fPhi.GetBinContent(gbin);
      Double_t Q22    = g2Kine->fQ2.GetBinContent(gbin);

      Double_t W      = (W1+W2)/2.0;
      Double_t x      = (x1+x2)/2.0;
      Double_t phi    = (phi1+phi2)/2.0;
      Double_t Q2     = (Q21+Q22)/2.0;

      Double_t Ep1   = g1Kine->fE.GetBinContent(gbin);
      Double_t Ep2   = g2Kine->fE.GetBinContent(gbin);
      Double_t theta1   = g1Kine->fTheta.GetBinContent(gbin);
      Double_t theta2   = g2Kine->fTheta.GetBinContent(gbin);

      Double_t D1   = g1Kine->fD.GetBinContent(gbin);
      Double_t D2   = g2Kine->fD.GetBinContent(gbin);
      Double_t D    = (D1+D2)/2.0;

      Double_t Eta1   = g1Kine->fEta.GetBinContent(gbin);
      Double_t Eta2   = g2Kine->fEta.GetBinContent(gbin);
      Double_t eta    = (Eta1+Eta2)/2.0;

      Double_t Xi1   = g1Kine->fXi.GetBinContent(gbin);
      Double_t Xi2   = g2Kine->fXi.GetBinContent(gbin);
      Double_t xi    = (Xi1+Xi2)/2.0;

      Double_t Chi1   = g1Kine->fChi.GetBinContent(gbin);
      Double_t Chi2   = g2Kine->fChi.GetBinContent(gbin);
      Double_t chi    = (Chi1+Chi2)/2.0;

      Double_t F11   = g1Kine->fF1.GetBinContent(gbin);
      Double_t F12   = g2Kine->fF1.GetBinContent(gbin);
      Double_t F1    = (F11+F12)/2.0;

      Double_t R_2      = InSANE::Kine::R1998(x,Q2);
      //Double_t R        = SFs->R(x,Q2);
      Double_t R1   = g1Kine->fR.GetBinContent(gbin);
      Double_t R2   = g2Kine->fR.GetBinContent(gbin);
      Double_t R    = (R1+R2)/2.0;

      //Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
      Double_t Ep    = (Ep1+Ep2)/2.0;
      Double_t theta = (theta1+theta2)/2.0;

      Double_t g1  = fg1->GetBinContent(gbin);
      Double_t g2  = fg2->GetBinContent(gbin);

      Double_t eg1  = fg1->GetBinError(gbin);
      Double_t eg2  = fg2->GetBinError(gbin);

      Double_t eg1Syst  = g1Asym->fSystErrVsW.GetBinContent(gbin);
      Double_t eg2Syst  = g2Asym->fSystErrVsW.GetBinContent(gbin);

      Double_t gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
      Double_t gamma    = TMath::Sqrt(gamma2);
      Double_t cota     = 1.0/TMath::Tan(alpha);
      Double_t csca     = 1.0/TMath::Sin(alpha);

      Double_t xg1 = x*g1;
      Double_t xg2 = x*g2;

      //eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
      Double_t exg1     = x*eg1;
      Double_t exg2     = x*eg2;
      Double_t exg1Syst  = x*eg1Syst;
      Double_t exg2Syst  = x*eg2Syst;

      if(std::isnan(exg1)){
        g1 = 0.0;
        g2 = 0.0;
      }
      if(std::isnan(exg2)){
        g1 = 0.0;
        g2 = 0.0;
      }

      fxg1->SetBinContent(gbin,xg1);
      fxg2->SetBinContent(gbin,xg2);
      fxg1->SetBinError(  gbin,eg1);
      fxg2->SetBinError(  gbin,eg2);

      fxg1Full->fSystErrVsW.SetBinContent(gbin,exg1Syst);
      fxg2Full->fSystErrVsW.SetBinContent(gbin,exg2Syst);

      gr_x2g1->SetPoint(i_bin-1, x, x*xg1);
      gr_x2g2->SetPoint(i_bin-1, x, x*xg2);
      gr_x2g1->SetPointError(i_bin-1, 0, x*exg1);
      gr_x2g2->SetPointError(i_bin-1, 0, x*exg2);

    }

    fxg1Full->fAsymmetryVsW = *fxg1;
    fxg2Full->fAsymmetryVsW = *fxg2;
    fxg1Full->fSystErrVsW.SetFillColor(fxg1->GetLineColor());
    fxg2Full->fSystErrVsW.SetFillColor(fxg1->GetLineColor());
    fxg1Full->fSystErrVsW.SetLineColor(fxg1->GetLineColor());
    fxg2Full->fSystErrVsW.SetLineColor(fxg1->GetLineColor());

    if(jj<4) {
      TGraphErrors * gr1      = new TGraphErrors(&fxg1Full->fAsymmetryVsW);
      TGraphErrors * gr1_syst = syst_error_band( &fxg1Full->fSystErrVsW, -0.5,0.3);
      TGraphErrors * gr2      = new TGraphErrors(&fxg2Full->fAsymmetryVsW);
      TGraphErrors * gr2_syst = syst_error_band( &fxg2Full->fSystErrVsW, -0.5,0.3);
      for( int j_point = gr1->GetN()-1 ; j_point>=0; j_point--) {
        Double_t xt, yt;
        gr1->GetPoint(j_point,xt,yt);
        Double_t xt2, yt2;
        gr2->GetPoint(j_point,xt2,yt2);
        Int_t    abin    = fxg1Full->fSystErrVsW.FindBin(xt);
        Double_t eyt     = gr1->GetErrorY(j_point);
        Double_t eyt_sys = fxg1Full->fSystErrVsW.GetBinContent(abin);
        if( (yt > 2.0) || (yt2 > 2.0) || (yt == 0.0) || (yt2 == 0.0)  ) {
          gr1->RemovePoint(j_point);
          gr2->RemovePoint(j_point);
          gr1_syst->RemovePoint(j_point);
          gr2_syst->RemovePoint(j_point);

          gr_x2g1->RemovePoint(j_point);
          gr_x2g2->RemovePoint(j_point);

          //fg1Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fg1Full->fSystErrVsW.SetBinError(abin,0.0);
          //fg2Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fg2Full->fSystErrVsW.SetBinError(abin,0.0);

          //fg1Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fg1Full->fSystErrVsW.SetBinError(abin,0.0);
          //fg2Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fg2Full->fSystErrVsW.SetBinError(abin,0.0);

          continue;
        }
      }
      mg1->Add(gr1,"ep");
      mg1_syst->Add(gr1_syst,"e3");
      mg2->Add(gr2,"ep");
      mg2_syst->Add(gr2_syst,"e3");

      gr_x2g1->Sort();
      gr_x2g2->Sort();

      mg_x2g1->Add(gr_x2g1,"ep");
      mg_x2g2->Add(gr_x2g2,"ep");
    }


    std::string   filename0       = Form("xg1_combined_%d_%d.txt",aNumber,jj);
    std::string   table_filename0 = table_dir0 + filename0;
    std::ofstream outtable0(table_filename0.c_str());
    fxg1Full->PrintBinnedTableHead(outtable0);
    fxg1Full->PrintBinnedTable(outtable0);

    filename0       = Form("xg2_combined_%d_%d.txt",aNumber,jj);
    table_filename0 = table_dir0 + filename0;
    std::ofstream outtable1(table_filename0.c_str());
    fxg2Full->PrintBinnedTableHead(outtable1);
    fxg2Full->PrintBinnedTable(outtable1);

  }

  // ------------------------------------------
  TCanvas * c = new TCanvas();
  gSystem->mkdir(Form("results/asymmetries/%d",RunGroup),true);

  TMultiGraph * MG_1 = new TMultiGraph();
  MG_1->Add(mg1);
  MG_1->Add(mg1_syst);
  MG_1->Draw("a");
  MG_1->GetXaxis()->SetLimits(1.0,3.2); 
  //MG_1->GetYaxis()->SetRangeUser(-2.0,2.0); 
  MG_1->Draw("a");

  c->SaveAs(Form("results/asymmetries/%d/xg1_combined_W_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/xg1_combined_W_%d.pdf",RunGroup,RunGroup));

  // ------------------------------------------
  c = new TCanvas();

  TMultiGraph * MG_2 = new TMultiGraph();
  MG_2->Add(mg2);
  MG_2->Add(mg2_syst);
  MG_2->Draw("a");
  MG_2->GetXaxis()->SetLimits(1.0,3.2); 
  //MG_2->GetYaxis()->SetRangeUser(-2.0,2.0); 
  MG_2->Draw("a");
  c->SaveAs(Form("results/asymmetries/%d/xg2_combined_W_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/xg2_combined_W_%d.pdf",RunGroup,RunGroup));


  // ------------------------------------------
  c = new TCanvas();

  MG_2 = new TMultiGraph();
  MG_2->Add(mg_x2g1);
  MG_2->Draw("a");
  //MG_2->GetXaxis()->SetLimits(1.0,3.2); 
  //MG_2->GetYaxis()->SetRangeUser(-2.0,2.0); 
  //MG_2->Draw("a");
  c->SaveAs(Form("results/asymmetries/%d/x2g1_combined_x_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/x2g1_combined_x_%d.pdf",RunGroup,RunGroup));

  // ------------------------------------------
  c = new TCanvas();

  MG_2 = new TMultiGraph();
  MG_2->Add(mg_x2g2);
  MG_2->Draw("a");
  //MG_2->GetXaxis()->SetLimits(1.0,3.2); 
  //MG_2->GetYaxis()->SetRangeUser(-2.0,2.0); 
  //MG_2->Draw("a");
  c->SaveAs(Form("results/asymmetries/%d/x2g2_combined_x_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/x2g2_combined_x_%d.pdf",RunGroup,RunGroup));


  TFile * fout = TFile::Open(Form("data/xg1g2_combined_%d.root",RunGroup),"UPDATE");
  fout->WriteObject(fAllxg1Asym,Form("xg1_combined_W"));
  fout->WriteObject(fAllxg2Asym,Form("xg2_combined_W"));
  fout->Close();
  return 0;
}
