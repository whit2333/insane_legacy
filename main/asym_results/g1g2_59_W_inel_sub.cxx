#include "util/syst_error_band.cxx"
#include "asym/sane_data_bins.cxx"
#include "util/combine_results.cxx"

Int_t g1g2_59_W_inel_sub(Int_t RunGroup = 8412)
{
  TH1::AddDirectory(false);
  Int_t paraRunGroup = RunGroup;
  Int_t perpRunGroup = RunGroup;
  Int_t aNumber      = RunGroup;

  TList * A1_59_list = 0;
  TList * A2_59_list  = 0; 

  TFile * fin = TFile::Open(Form("data/A1A2_59_%d.root",RunGroup),"READ");
  fin->ls();
  fin->cd();
  A1_59_list = (TList*)gROOT->FindObject("A1_59_W");
  if(!A1_59_list){  return -1; }          
  A2_59_list  = (TList*)gROOT->FindObject("A2_59_W");
  if(!A2_59_list){  return -2; }

  TList        * fAllg1Asym = new TList();
  TList        * fAllg2Asym = new TList();
  TList        * fAllxg1Asym = new TList();
  TList        * fAllxg2Asym = new TList();
  TMultiGraph * mg1      = new TMultiGraph();
  TMultiGraph * mg1_syst = new TMultiGraph();
  TMultiGraph * mg2      = new TMultiGraph();
  TMultiGraph * mg2_syst = new TMultiGraph();

  TMultiGraph * mxg1      = new TMultiGraph();
  TMultiGraph * mxg1_syst = new TMultiGraph();
  TMultiGraph * mxg2      = new TMultiGraph();
  TMultiGraph * mxg2_syst = new TMultiGraph();

  Double_t alpha = 80.0*TMath::Pi()/180.0;

  std::string table_dir0 = "results/asymmetries/tables/";
  table_dir0            += std::to_string(aNumber);
  table_dir0            += "/inel_sub";
  table_dir0            += "/g1g2/";
  gSystem->mkdir(table_dir0.c_str(),true);

  InSANEFunctionManager    * fman = InSANEFunctionManager::GetManager();
  InSANEStructureFunctions * SFs  = fman->GetStructureFunctions();

  for(int jj = 0;jj<A1_59_list->GetEntries();jj++) {

    InSANEMeasuredAsymmetry         * A1Asym        = (InSANEMeasuredAsymmetry*)(A1_59_list ->At(jj));
    InSANEMeasuredAsymmetry         * A2Asym        = (InSANEMeasuredAsymmetry*)(A2_59_list  ->At(jj));

    TH1F * fA1 = &A1Asym->fAsymmetryVsW;
    TH1F * fA2 = &A2Asym->fAsymmetryVsW;

    TH1F * m1  = &A1Asym->fMask_W;

    InSANEAveragedKinematics1D * A1Kine = &A1Asym->fAvgKineVsW;
    InSANEAveragedKinematics1D * A2Kine = &A2Asym->fAvgKineVsW; 

    // A1 and A2 
    InSANEMeasuredAsymmetry * fg1Full = new InSANEMeasuredAsymmetry( *A1Asym );
    InSANEMeasuredAsymmetry * fg2Full = new InSANEMeasuredAsymmetry( *A2Asym );
    fAllg1Asym->Add(fg1Full);
    fAllg2Asym->Add(fg2Full);

    InSANEMeasuredAsymmetry * fxg1Full = new InSANEMeasuredAsymmetry( *A1Asym );
    InSANEMeasuredAsymmetry * fxg2Full = new InSANEMeasuredAsymmetry( *A2Asym );
    fAllxg1Asym->Add(fxg1Full);
    fAllxg2Asym->Add(fxg2Full);

    TH1F * fg1 = (TH1F*)fg1Full->fAsymmetryVsW.Clone();
    TH1F * fg2 = (TH1F*)fg2Full->fAsymmetryVsW.Clone();
    fg1->Reset();
    fg2->Reset();

    TGraphErrors * gr_xg1 = new TGraphErrors();
    TGraphErrors * gr_xg2 = new TGraphErrors();

    int xBinmax = fg1->GetNbinsX();
    for(int i_bin=1; i_bin<= xBinmax; i_bin++){

      int gbin   = fg1->GetBin(i_bin);

      Double_t W1     = A1Kine->fW.GetBinContent(gbin);
      Double_t x1     = A1Kine->fx.GetBinContent(gbin);
      Double_t phi1   = A1Kine->fPhi.GetBinContent(gbin);
      Double_t Q21    = A1Kine->fQ2.GetBinContent(gbin);

      Double_t W2     = A2Kine->fW.GetBinContent(gbin);
      Double_t x2     = A2Kine->fx.GetBinContent(gbin);
      Double_t phi2   = A2Kine->fPhi.GetBinContent(gbin);
      Double_t Q22    = A2Kine->fQ2.GetBinContent(gbin);

      Double_t W      = (W1+W2)/2.0;
      Double_t x      = (x1+x2)/2.0;
      Double_t phi    = (phi1+phi2)/2.0;
      Double_t Q2     = (Q21+Q22)/2.0;

      Double_t Ep1   = A1Kine->fE.GetBinContent(gbin);
      Double_t Ep2   = A2Kine->fE.GetBinContent(gbin);
      Double_t theta1   = A1Kine->fTheta.GetBinContent(gbin);
      Double_t theta2   = A2Kine->fTheta.GetBinContent(gbin);

      Double_t D1   = A1Kine->fD.GetBinContent(gbin);
      Double_t D2   = A2Kine->fD.GetBinContent(gbin);
      Double_t D    = (D1+D2)/2.0;

      Double_t Eta1   = A1Kine->fEta.GetBinContent(gbin);
      Double_t Eta2   = A2Kine->fEta.GetBinContent(gbin);
      Double_t eta    = (Eta1+Eta2)/2.0;

      Double_t Xi1   = A1Kine->fXi.GetBinContent(gbin);
      Double_t Xi2   = A2Kine->fXi.GetBinContent(gbin);
      Double_t xi    = (Xi1+Xi2)/2.0;

      Double_t Chi1   = A1Kine->fChi.GetBinContent(gbin);
      Double_t Chi2   = A2Kine->fChi.GetBinContent(gbin);
      Double_t chi    = (Chi1+Chi2)/2.0;

      Double_t F11   = A1Kine->fF1.GetBinContent(gbin);
      Double_t F12   = A2Kine->fF1.GetBinContent(gbin);
      Double_t F1    = (F11+F12)/2.0;

      Double_t R_2      = InSANE::Kine::R1998(x,Q2);
      //Double_t R        = SFs->R(x,Q2);
      Double_t R1   = A1Kine->fR.GetBinContent(gbin);
      Double_t R2   = A2Kine->fR.GetBinContent(gbin);
      Double_t R    = (R1+R2)/2.0;

      //Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
      Double_t Ep    = (Ep1+Ep2)/2.0;
      Double_t theta = (theta1+theta2)/2.0;

      Double_t A1  = fA1->GetBinContent(gbin);
      Double_t A2  = fA2->GetBinContent(gbin);

      Double_t eA1  = fA1->GetBinError(gbin);
      Double_t eA2  = fA2->GetBinError(gbin);

      Double_t eA1Syst  = A1Asym->fSystErrVsW.GetBinContent(gbin);
      Double_t eA2Syst  = A2Asym->fSystErrVsW.GetBinContent(gbin);

      Double_t gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
      Double_t gamma    = TMath::Sqrt(gamma2);
      Double_t cota     = 1.0/TMath::Tan(alpha);
      Double_t csca     = 1.0/TMath::Sin(alpha);

      Double_t g1 = (F1/(1.0+gamma2))*(A1 + gamma*A2);
      Double_t g2 = (F1/(1.0+gamma2))*(A2/gamma - A1);

      //eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
      Double_t a1 = (F1/(1.0+gamma2));
      Double_t a2 = (F1/(1.0+gamma2))*gamma;
      Double_t eg1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
      Double_t eg1Syst = TMath::Sqrt( TMath::Power(a1*eA1Syst,2.0) + TMath::Power(a2*eA2Syst,2.0));
      //eg2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
      a1 = -1.0*(F1/(1.0+gamma2));
      a2 = (F1/(1.0+gamma2))/gamma;
      Double_t eg2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
      Double_t eg2Syst = TMath::Sqrt( TMath::Power(a1*eA1Syst,2.0) + TMath::Power(a2*eA2Syst,2.0));

      if(std::isnan(g1)){
        g1 = 0.0;
        g2 = 0.0;
      }
      if(std::isnan(g2)){
        g1 = 0.0;
        g2 = 0.0;
      }

      if(m1->GetBinContent(gbin) < 0.1) {
        g1 = 0.0;
        g2 = 0.0;
        eg1 = 0.0;
        eg2 = 0.0;
        eg1Syst = 0.0;
        eg2Syst = 0.0;
      }

      fg1->SetBinContent(gbin,g1);
      fg2->SetBinContent(gbin,g2);
      fg1->SetBinError(gbin,eg1);
      fg2->SetBinError(gbin,eg2);

      fg1Full->fSystErrVsW.SetBinContent(gbin,eg1Syst);
      fg2Full->fSystErrVsW.SetBinContent(gbin,eg2Syst);

      gr_xg1->SetPoint(     i_bin-1, x, x*g1);
      gr_xg1->SetPointError(i_bin-1, 0, x*eg1);
      gr_xg2->SetPoint(     i_bin-1, x, x*g2);
      gr_xg2->SetPointError(i_bin-1, 0, x*eg2);

    }

    fg1Full->fAsymmetryVsW = *fg1;
    fg2Full->fAsymmetryVsW = *fg2;
    fg1Full->fSystErrVsW.SetFillColor(fg1->GetLineColor());
    fg2Full->fSystErrVsW.SetFillColor(fg1->GetLineColor());
    fg1Full->fSystErrVsW.SetLineColor(fg1->GetLineColor());
    fg2Full->fSystErrVsW.SetLineColor(fg1->GetLineColor());

    gr_xg1->SetFillColor(fg1->GetLineColor());
    gr_xg2->SetFillColor(fg1->GetLineColor());
    gr_xg1->SetLineColor(fg1->GetLineColor());
    gr_xg2->SetLineColor(fg1->GetLineColor());

    if(jj<4) {
      TGraphErrors * gr1      = new TGraphErrors(&fg1Full->fAsymmetryVsW);
      TGraphErrors * gr1_syst = syst_error_band(&fg1Full->fSystErrVsW, -1.0,0.3);
      TGraphErrors * gr2      = new TGraphErrors(&fg2Full->fAsymmetryVsW);
      TGraphErrors * gr2_syst = syst_error_band(&fg2Full->fSystErrVsW, -1.0,0.3);
      for( int j_point = gr1->GetN()-1 ; j_point>=0; j_point--) {
        Double_t xt, yt;
        gr1->GetPoint(j_point,xt,yt);
        Double_t xt2, yt2;
        gr2->GetPoint(j_point,xt2,yt2);
        Int_t    abin    = fg1Full->fSystErrVsW.FindBin(xt);
        Double_t eyt     = gr1->GetErrorY(j_point);
        Double_t eyt_sys = fg1Full->fSystErrVsW.GetBinContent(abin);
        if( (yt > 2.0) || (yt2 > 2.0) || (yt == 0.0) || (yt2 == 0.0)  ) {
          gr1->RemovePoint(j_point);
          gr2->RemovePoint(j_point);
          gr1_syst->RemovePoint(j_point);
          gr2_syst->RemovePoint(j_point);

          //gr_xg1->RemovePoint(j_point);
          //gr_xg2->RemovePoint(j_point);

          //fg1Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fg1Full->fSystErrVsW.SetBinError(abin,0.0);
          //fg2Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fg2Full->fSystErrVsW.SetBinError(abin,0.0);

          //fg1Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fg1Full->fSystErrVsW.SetBinError(abin,0.0);
          //fg2Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fg2Full->fSystErrVsW.SetBinError(abin,0.0);

          continue;
        }
      }
      mg1->Add(gr1,"ep");
      mg1_syst->Add(gr1_syst,"e3");
      mg2->Add(gr2,"ep");
      mg2_syst->Add(gr2_syst,"e3");

      mxg1->Add(gr_xg1,"ep");
      mxg2->Add(gr_xg2,"ep");
    }


    std::string   filename0       = Form("g1_59_%d_%d.txt",aNumber,jj);
    std::string   table_filename0 = table_dir0 + filename0;
    std::ofstream outtable0(table_filename0.c_str());
    fg1Full->PrintBinnedTableHead(outtable0);
    fg1Full->PrintBinnedTable(outtable0);

    filename0       = Form("g2_59_%d_%d.txt",aNumber,jj);
    table_filename0 = table_dir0 + filename0;
    std::ofstream outtable1(table_filename0.c_str());
    fg2Full->PrintBinnedTableHead(outtable1);
    fg2Full->PrintBinnedTable(outtable1);

  }

  // ------------------------------------------
  TCanvas * c = new TCanvas();
  gSystem->mkdir(Form("results/asymmetries/%d",RunGroup),true);

  TMultiGraph * MG_1 = new TMultiGraph();
  MG_1->Add(mg1);
  MG_1->Add(mg1_syst);
  MG_1->Draw("a");
  MG_1->GetXaxis()->SetLimits(1.0,3.2); 
  MG_1->GetYaxis()->SetRangeUser(-2.0,2.0); 
  MG_1->Draw("a");

  c->SaveAs(Form("results/asymmetries/%d/g1_59_W_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/g1_59_W_%d.pdf",RunGroup,RunGroup));

  // ------------------------------------------
  c = new TCanvas();

  TMultiGraph * MG_2 = new TMultiGraph();
  MG_2->Add(mg2);
  MG_2->Add(mg2_syst);
  MG_2->Draw("a");
  MG_2->GetXaxis()->SetLimits(1.0,3.2); 
  MG_2->GetYaxis()->SetRangeUser(-2.0,2.0); 
  MG_2->Draw("a");
  c->SaveAs(Form("results/asymmetries/%d/g2_59_W_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/g2_59_W_%d.pdf",RunGroup,RunGroup));

  // ------------------------------------------
  c = new TCanvas();

  MG = new TMultiGraph();
  MG->Add(mxg1);
  MG->Draw("a");
  //MG->GetXaxis()->SetLimits(1.0,3.2); 
  //MG->GetYaxis()->SetRangeUser(-2.0,2.0); 
  //MG->Draw("a");
  c->SaveAs(Form("results/asymmetries/%d/xg1_59_W_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/xg1_59_W_%d.pdf",RunGroup,RunGroup));

  c = new TCanvas();

  MG = new TMultiGraph();
  MG->Add(mxg2);
  MG->Draw("a");
  //MG->GetXaxis()->SetLimits(1.0,3.2); 
  //MG->GetYaxis()->SetRangeUser(-2.0,2.0); 
  //MG->Draw("a");
  c->SaveAs(Form("results/asymmetries/%d/xg2_59_W_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/xg2_59_W_%d.pdf",RunGroup,RunGroup));



  TFile * fout = TFile::Open(Form("data/g1g2_59_%d.root",RunGroup),"UPDATE");
  fout->WriteObject(fAllg1Asym,Form("g1_59_W"));
  fout->WriteObject(fAllg2Asym,Form("g2_59_W"));
  //fout->Close();
  return 0;
}
