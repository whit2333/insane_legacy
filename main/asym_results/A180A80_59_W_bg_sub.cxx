#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

Int_t A180A80_59_W_bg_sub(Int_t paraRunGroup = 8011,Int_t perpRunGroup = 8011,Int_t aNumber=8011){

   //gStyle->SetPadGridX(true);
   //gStyle->SetPadGridY(true);

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"READ");
   f1->cd();
   //TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_para59-%d",0));
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("background-subtracted-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"READ");
   f2->cd();
   //TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("combined-asym_perp59-%d",0));
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("background-subtracted-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);

   TList * SystHists1 = new TList();
   TList * SystHists2 = new TList();

   TMultiGraph * mg  = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg_syst  = new TMultiGraph();
   TMultiGraph * mg2_syst  = new TMultiGraph();

   TMultiGraph * mg_split       = new TMultiGraph();
   TMultiGraph * mg2_split      = new TMultiGraph();
   TMultiGraph * mg_split_syst  = new TMultiGraph();
   TMultiGraph * mg2_split_syst = new TMultiGraph();

   TLegend *leg = new TLegend(0.7, 0.6, 0.88, 0.85);
   leg->SetBorderSize(0);

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetMeasuredAsymmetryResult();

      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetMeasuredAsymmetryResult();

      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry*)(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry*)(fPerpAsymmetries->At(jj));

      TH1F * fApara = &paraAsym->fAsymmetryVsW;
      TH1F * fAperp = &perpAsym->fAsymmetryVsW;

      InSANEAveragedKinematics1D * paraKine = &perpAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsW; 

      paraAsym->fSystErrVsW.SetLineColor(fApara->GetLineColor());
      perpAsym->fSystErrVsW.SetLineColor(fApara->GetLineColor());
      paraAsym->fSystErrVsW.SetFillColor(fApara->GetLineColor());
      perpAsym->fSystErrVsW.SetFillColor(fApara->GetLineColor());

      SystHists1->Add(&paraAsym->fSystErrVsW);
      SystHists2->Add(&perpAsym->fSystErrVsW);

      // Some graphs
      TGraphErrors * gr_para  = new TGraphErrors( fApara );
      TGraphErrors * gr_perp  = new TGraphErrors( fAperp );
      TGraphErrors * gr2_para = stat_error_graph(&paraAsym->fSystErrVsW, 0.0,0.4);
      TGraphErrors * gr2_perp = stat_error_graph(&perpAsym->fSystErrVsW, 0.0,0.4);
      // ---------------------------------
      // A180
      // Clean up graphs 
      TGraphErrors * gr  = gr_para;
      TGraphErrors * gr2 = gr2_para;
      TH1          * hSyst  = &paraAsym->fSystErrVsW;
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = paraAsym->fSystErrVsW.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         Double_t eyt_sys = paraAsym->fSystErrVsW.GetBinContent(abin);
         if( yt == 0.0 || yt<0.0 || eyt_sys>1.0 || eyt>1.0 ) {
            gr_para->RemovePoint(j);
            gr2_para->RemovePoint(j);
            gr_perp->RemovePoint(j);
            gr2_perp->RemovePoint(j);
            paraAsym->fSystErrVsW.SetBinContent(abin,0.0);
            paraAsym->fSystErrVsW.SetBinError(abin,0.0);
            // Also do perp
            perpAsym->fSystErrVsW.SetBinContent(abin,0.0);
            perpAsym->fSystErrVsW.SetBinError(abin,0.0);
            continue;
         }
      }
      if(jj==4) {
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
         hSyst->SetMarkerColor(1);
         hSyst->SetMarkerStyle(24);
         hSyst->SetLineColor(1);
         hSyst->SetFillColorAlpha(1,0.2);
      } else if( jj >4 && jj < 9 ) {
         gr->SetMarkerColor(4008+jj-4);
         gr->SetLineColor(  4008+jj-4);
         gr->SetMarkerStyle(23);
         mg_split->Add(gr,"p");
         hSyst->SetLineColor(  4008+jj-4);
         hSyst->SetFillColorAlpha(  4008+jj-4,0.3);
         //hSyst->SetMarkerStyle(23);
         //hSyst->SetMarkerColor(4008+jj-4);
         //mg_split_syst->Add(gr2,"l");
         //mg_split_syst->Add(gr2,"e3");
         mg_split_syst->Add(stat_error_graph(hSyst,0.0,0.4),"e3");
      } else if( jj >9 && jj < 14 ) {
         gr->SetMarkerColor(4008+jj-9);
         gr->SetLineColor(  4008+jj-9);
         gr->SetMarkerStyle(22);
         mg_split->Add(gr,"p");
         hSyst->SetLineColor(  4008+jj-9);
         hSyst->SetFillColorAlpha(  4008+jj-9,0.3);
         //gr2->SetMarkerStyle(22);
         //gr2->SetMarkerColor(4008+jj-9);
         //mg2_split_syst->Add(gr2,"l");
         //mg2_split_syst->Add(gr2,"e3");
         mg_split_syst->Add(stat_error_graph(hSyst,0.0,0.3),"e3");
      }
      if(jj<4) {
         mg->Add(gr,"p0");
         mg_syst->Add(stat_error_graph(hSyst,0.0,0.4),"e3");
         leg->AddEntry(gr,Form("#LTQ^{2}#GT=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      // ---------------------------------
      // A80
      gr    =  gr_perp;
      gr2   = gr2_perp;
      hSyst = &perpAsym->fSystErrVsW;
      for( int j = gr->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = perpAsym->fSystErrVsW.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         Double_t eyt_sys = perpAsym->fSystErrVsW.GetBinContent(abin);
         if( yt == 0.0  || eyt_sys>1.0 || eyt>1.0 ) {
            gr->RemovePoint(j);
            gr2->RemovePoint(j);
            perpAsym->fSystErrVsW.SetBinContent(abin,0.0);
            perpAsym->fSystErrVsW.SetBinError(abin,0.0);
            continue;
         }
      }
      if(jj==4) {
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
         hSyst->SetMarkerColor(1);
         hSyst->SetMarkerStyle(24);
         hSyst->SetLineColor(1);
      } else if( jj >4 && jj < 9 ) {
         gr->SetMarkerColor(4008+jj-4);
         gr->SetLineColor(  4008+jj-4);
         gr->SetMarkerStyle(23);
         mg2_split->Add(gr,"p");
         hSyst->SetLineColor(  4008+jj-4);
         hSyst->SetFillColorAlpha(  4008+jj-4,0.5);
         //hSyst->SetMarkerColor(4008+jj-4);
         //hSyst->SetMarkerStyle(23);
         //mg2_split_syst->Add(gr2,"l");
         //mg_split_syst->Add(gr2,"e3");
         mg2_split_syst->Add(stat_error_graph(hSyst,-0.8,0.4),"e3");
      } else if( jj >9 && jj < 14 ) {
         gr->SetMarkerColor(4008+jj-9);
         gr->SetLineColor(  4008+jj-9);
         gr->SetMarkerStyle(22);
         mg2_split->Add(gr,"p");
         hSyst->SetLineColor(  4008+jj-9);
         hSyst->SetFillColorAlpha(  4008+jj-9,0.4);
         //gr2->SetMarkerColor(4008+jj-9);
         //gr2->SetMarkerStyle(22);
         //mg2_split_syst->Add(gr2,"l");
         //mg2_split_syst->Add(gr2,"e3");
         mg2_split_syst->Add(stat_error_graph(hSyst,-0.8,0.4),"e3");
      }
      if(jj<4) {
         mg2->Add(gr,"p");
         //mg2_syst->Add(gr2,"l");
         mg2_syst->Add(stat_error_graph(hSyst,-0.8,0.4),"e3");
      }

   }


   // -----------------------------------------------------------------
   // A180

   TCanvas * c = new TCanvas("cA180A80_59","A180 A80 59");
   NucDBManager * manager = NucDBManager::GetManager();
   TMultiGraph *MG = new TMultiGraph(); 
   TString Measurement = Form("A_{180}");
   TString GTitle      = "E=5.9 GeV";
   TString xAxisTitle  = Form("W");
   TString yAxisTitle  = Form("%s",Measurement.Data());
   MG->Add(mg_syst);
   MG->Add(mg);
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(1.0,3.2); 
   MG->GetYaxis()->SetRangeUser(-0.2,1.2); 
   MG->Draw("AP");
   //for(int i=0;i<SystHists1->GetEntries();i++){
   //   TH1 * hsys2 = (TH1*)SystHists1->At(i);
   //   TGraph * gr_sys = new TGraph(hsys2);
   //   if(i<=4) {
   //      //hsys2->Draw("same");
   //      gr_sys->Draw("l");
   //   }
   //}
   TLatex l;
   //l.SetNDC(true);
   l.SetTextSize(0.04);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(0);
   //l.DrawLatex(3.2,0.1,"#splitline{systematic}{#leftarrow errors}");
   leg->Draw();

   c->SaveAs(Form("results/asymmetries/%d/A180A80_59_W_bg_sub_0_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A180A80_59_W_bg_sub_0_%d.png",aNumber,aNumber));

   // ------------------------------------------
   // A80

   c = new TCanvas();
   TMultiGraph *MG2 = new TMultiGraph(); 

   MG2->Add(mg2_syst);
   MG2->Add(mg2);
   MG2->SetTitle("A_{80}^{p}, E=5.9 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(1.0,3.2); 
   MG2->GetYaxis()->SetRangeUser(-1.0,1.0); 
   MG2->GetYaxis()->SetTitle("A_{80}^{p}");
   MG2->GetXaxis()->SetTitle("W");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   //for(int i=0;i<SystHists2->GetEntries();i++){
   //   TH1 * hsys2 = (TH1*)SystHists2->At(i);
   //   TGraph * gr_sys = new TGraph(hsys2);
   //   if(i<=4) {
   //      //hsys2->Draw("same");
   //      gr_sys->Draw("l");
   //   }
   //}

   l.SetTextSize(0.04);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(0);
   //l.DrawLatex(3.2,0.1,"#splitline{systematic}{#leftarrow errors}");
   leg->Draw();

   c->SaveAs(Form("results/asymmetries/%d/A180A80_59_W_bg_sub_1_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A180A80_59_W_bg_sub_1_%d.png",aNumber,aNumber));

   //------------------------------------------
   c = new TCanvas();
   c->Divide(1,2);

   // --------------
   c->cd(1);
   TMultiGraph *MG3 = new TMultiGraph(); 
   MG3->Add(mg_split_syst);
   MG3->Add(mg_split);
   MG3->Draw("A");
   MG3->SetTitle(GTitle);
   MG3->GetXaxis()->SetTitle(xAxisTitle);
   MG3->GetXaxis()->CenterTitle();
   MG3->GetYaxis()->SetTitle(yAxisTitle);
   MG3->GetYaxis()->CenterTitle();
   MG3->GetXaxis()->SetLimits(1.0,3.2); 
   MG3->GetYaxis()->SetRangeUser(-0.2,1.2); 

   // --------------
   c->cd(2);
   TMultiGraph *MG4 = new TMultiGraph(); 
   MG4->Add(mg2_split_syst);
   MG4->Add(mg2_split);
   MG4->Draw("A");
   MG4->GetXaxis()->SetLimits(1.0,3.2); 
   MG4->GetYaxis()->SetRangeUser(-1.0,1.0); 
   MG4->GetYaxis()->SetTitle("A_{80}^{p}");
   MG4->GetXaxis()->SetTitle("W");
   MG4->GetXaxis()->CenterTitle();
   MG4->GetYaxis()->CenterTitle();

   c->SaveAs(Form("results/asymmetries/%d/A180A80_59_W_bg_sub_split_%d.pdf",aNumber,aNumber));
   c->SaveAs(Form("results/asymmetries/%d/A180A80_59_W_bg_sub_split_%d.png",aNumber,aNumber));

   return(0);
}
