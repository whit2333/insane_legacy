#include "util/syst_error_band.cxx"
#include "asym/sane_data_bins.cxx"
#include "util/combine_results.cxx"

Int_t A1A2_59_W_inel_sub(Int_t RunGroup = 8406)
{
  TH1::AddDirectory(false);
  Int_t paraRunGroup = RunGroup;
  Int_t perpRunGroup = RunGroup;
  Int_t aNumber      = RunGroup;

  const char * para59file = Form("data/bg_corrected_asymmetries_para59_%d.root",paraRunGroup);
  TFile * f1 = TFile::Open(para59file,"READ");
  f1->cd();
  TList * fPara59Asymmetries = (TList *) gROOT->FindObject(Form("inelastic-subtracted-asym_para59-%d",0));
  if(!fPara59Asymmetries) return(-1);

  const char * perp59file = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpRunGroup);
  TFile * f2 = TFile::Open(perp59file,"READ");
  f2->cd();
  TList * fPerp59Asymmetries = (TList *) gROOT->FindObject(Form("inelastic-subtracted-asym_perp59-%d",0));
  if(!fPerp59Asymmetries) return(-2);

  TList * A180_combined_list = fPara59Asymmetries;
  TList * A80_combined_list  = fPerp59Asymmetries; 


  TList        * fAllA1Asym = new TList();
  TList        * fAllA2Asym = new TList();
  TMultiGraph * mg1      = new TMultiGraph();
  TMultiGraph * mg1_syst = new TMultiGraph();
  TMultiGraph * mg2      = new TMultiGraph();
  TMultiGraph * mg2_syst = new TMultiGraph();

  Double_t alpha = 80.0*TMath::Pi()/180.0;

  std::string table_dir0 = "results/asymmetries/tables/";
  table_dir0            += std::to_string(aNumber);
  table_dir0            += "/inel_sub";
  table_dir0            += "/A1A2_59/";
  gSystem->mkdir(table_dir0.c_str(),true);

  InSANEFunctionManager    * fman = InSANEFunctionManager::GetManager();
  InSANEStructureFunctions * SFs  = fman->GetStructureFunctions();

  for(int jj = 0;jj<A180_combined_list->GetEntries();jj++) {

    InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry*)(A180_combined_list ->At(jj));
    InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry*)(A80_combined_list  ->At(jj));

    TH1F * fApara = &paraAsym->fAsymmetryVsW;
    TH1F * fAperp = &perpAsym->fAsymmetryVsW;

    TH1F * m1 = &paraAsym->fMask_W;
    TH1F * m2 = &perpAsym->fMask_W;

    InSANEAveragedKinematics1D * paraKine = &paraAsym->fAvgKineVsW;
    InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsW; 

    // A1 and A2 
    InSANEMeasuredAsymmetry * fA1Full = new InSANEMeasuredAsymmetry( *paraAsym );
    InSANEMeasuredAsymmetry * fA2Full = new InSANEMeasuredAsymmetry( *paraAsym );
    fAllA1Asym->Add(fA1Full);
    fAllA2Asym->Add(fA2Full);

    TH1F * fA1 = (TH1F*)fA1Full->fAsymmetryVsW.Clone();
    TH1F * fA2 = (TH1F*)fA2Full->fAsymmetryVsW.Clone();
    fA1->Reset();
    fA2->Reset();

    int xBinmax = fA1->GetNbinsX();
    for(int i_bin=1; i_bin<= xBinmax; i_bin++){

      int gbin   = fA1->GetBin(i_bin);
      //std::cout << gbin << " " << i_bin <<'\n';

      double mask = m1->GetBinContent(gbin) * m2->GetBinContent(gbin);
      auto  k1 = paraKine->GetSingleBin(gbin);
      auto  k2 = perpKine->GetSingleBin(gbin);
      k1 += k2;
      k1 /= 2.0;

      Double_t W1     = paraKine->fW.GetBinContent(gbin);
      Double_t x1     = paraKine->fx.GetBinContent(gbin);
      Double_t phi1   = paraKine->fPhi.GetBinContent(gbin);
      Double_t Q21    = paraKine->fQ2.GetBinContent(gbin);

      Double_t W2     = perpKine->fW.GetBinContent(gbin);
      Double_t x2     = perpKine->fx.GetBinContent(gbin);
      Double_t phi2   = perpKine->fPhi.GetBinContent(gbin);
      Double_t Q22    = perpKine->fQ2.GetBinContent(gbin);

      Double_t W      = (W1+W2)/2.0;
      Double_t x      = (x1+x2)/2.0;
      Double_t phi    = (phi1+phi2)/2.0;
      Double_t Q2     = (Q21+Q22)/2.0;

      Double_t Ep1   = paraKine->fE.GetBinContent(gbin);
      Double_t Ep2   = perpKine->fE.GetBinContent(gbin);
      Double_t theta1   = paraKine->fTheta.GetBinContent(gbin);
      Double_t theta2   = perpKine->fTheta.GetBinContent(gbin);

      Double_t D1   = paraKine->fD.GetBinContent(gbin);
      Double_t D2   = perpKine->fD.GetBinContent(gbin);
      Double_t D    = (D1+D2)/2.0;

      Double_t Eta1   = paraKine->fEta.GetBinContent(gbin);
      Double_t Eta2   = perpKine->fEta.GetBinContent(gbin);
      Double_t eta    = (Eta1+Eta2)/2.0;

      Double_t Xi1   = paraKine->fXi.GetBinContent(gbin);
      Double_t Xi2   = perpKine->fXi.GetBinContent(gbin);
      Double_t xi    = (Xi1+Xi2)/2.0;

      Double_t Chi1   = paraKine->fChi.GetBinContent(gbin);
      Double_t Chi2   = perpKine->fChi.GetBinContent(gbin);
      Double_t chi    = (Chi1+Chi2)/2.0;

      //Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
      Double_t Ep    = (Ep1+Ep2)/2.0;
      Double_t theta = (theta1+theta2)/2.0;

      Double_t A180  = fApara->GetBinContent(gbin);
      Double_t A80   = fAperp->GetBinContent(gbin);

      Double_t eA180  = fApara->GetBinError(gbin);
      Double_t eA80   = fAperp->GetBinError(gbin);

      Double_t eSystA180  = paraAsym->fSystErrVsW.GetBinContent(gbin);
      Double_t eSystA80   = perpAsym->fSystErrVsW.GetBinContent(gbin);

      Double_t R_2      = InSANE::Kine::R1998(x,Q2);
      //Double_t R        = SFs->R(x,Q2);
      Double_t R1   = paraKine->fR.GetBinContent(gbin);
      Double_t R2   = perpKine->fR.GetBinContent(gbin);
      Double_t R    = (R1+R2)/2.0;

      //Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
      //Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
      //Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
      //Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
      //Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
      //Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
      Double_t cota     = 1.0/TMath::Tan(alpha);
      Double_t csca     = 1.0/TMath::Sin(alpha);

      Double_t c0 = 1.0/(1.0 + eta*xi);
      Double_t c11 = (1.0 + cota*chi)/D ;
      Double_t c12 = (csca*chi)/D ;
      Double_t c21 = (xi - cota*chi/eta)/D ;
      Double_t c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

      //A1 = A180*(1/D);
      Double_t A1 = c0*(c11*A180 + c12*A80);
      Double_t A2 = c0*(c21*A180 + c22*A80);

      Double_t eA1 = TMath::Sqrt( TMath::Power(c0*c11*eA180,2.0) + TMath::Power(c0*c12*eA80,2.0));
      Double_t eA2 = TMath::Sqrt( TMath::Power(c0*c21*eA180,2.0) + TMath::Power(c0*c22*eA80,2.0));
      //eA2 = c0*(c21*eA180 + c22*eA80);
      //A1 = c0*(A180/D - eta*A80/d);
      //A2 = c0*(xi*A180/D + A80/d);
      //Double_t eSystA1 = TMath::Sqrt( TMath::Power(c0*c11*eSystA180,2.0) + TMath::Power(c0*c12*eSystA80,2.0));
      //Double_t eSystA2 = TMath::Sqrt( TMath::Power(c0*c21*eSystA180,2.0) + TMath::Power(c0*c22*eSystA80,2.0));
      Double_t eSystA1 = ( TMath::Abs(c0*c11*eSystA180) + TMath::Abs(c0*c12*eSystA80))/2.0;
      Double_t eSystA2 = ( TMath::Abs(c0*c21*eSystA180) + TMath::Abs(c0*c22*eSystA80))/2.0;

      //if(A1 > 1.0) {
      //  A2 = 0.0;
      //  A1 = 0.0;
      //}
      if(eSystA1 > 0.9) {
        mask=0;
      }
      if(eSystA2 > 0.9) {
        mask=0;
      }
      if(eA1 > 0.75) {
        mask=0;
      }
      if(eA2 > 0.75) {
        mask=0;
      }
      if( std::isnan(A1) ) {
        A1 = 0;
        eA1 = 0;
        eSystA1 = 0;
        mask=0;
      }
      if( std::isnan(A2) ) {
        A2 = 0;
        eA2 = 0;
        eSystA2 = 0;
        mask=0;
      }
      if( mask < 0.1 )  {
        A1=0.0;
        A2=0.0;
        eA1 = 0;
        eSystA1 = 0;
        eA2 = 0;
        eSystA2 = 0;
        k1 *= 0.0;
      }

      //             std::cout << "theta = " << theta << "\n";
      //std::cout << "A1 = " << A1 << "\n";
      //std::cout << "A2 = " << A2 << "\n";
      //             
      fA1->SetBinContent(gbin,A1);
      fA2->SetBinContent(gbin,A2);
      fA1->SetBinError(gbin,eA1);
      fA2->SetBinError(gbin,eA2);

      fA1Full->fSystErrVsW.SetBinContent(gbin,eSystA1);
      fA2Full->fSystErrVsW.SetBinContent(gbin,eSystA2);
      fA1Full->fMask_W.SetBinContent(gbin,mask);
      fA2Full->fMask_W.SetBinContent(gbin,mask);
      fA1Full->fAvgKineVsW.SetSingleBin(gbin,k1);
      fA2Full->fAvgKineVsW.SetSingleBin(gbin,k1);

    }

    fA1Full->fAsymmetryVsW = *fA1;
    fA2Full->fAsymmetryVsW = *fA2;
    fA1Full->fSystErrVsW.SetFillColor(fA1->GetLineColor());
    fA2Full->fSystErrVsW.SetFillColor(fA1->GetLineColor());
    fA1Full->fSystErrVsW.SetLineColor(fA1->GetLineColor());
    fA2Full->fSystErrVsW.SetLineColor(fA1->GetLineColor());

    if(jj<4) {
      TGraphErrors * gr1      = new TGraphErrors(&fA1Full->fAsymmetryVsW);
      TGraphErrors * gr1_syst = syst_error_band(&fA1Full->fSystErrVsW, -1.0,0.3);
      TGraphErrors * gr2      = new TGraphErrors(&fA2Full->fAsymmetryVsW);
      TGraphErrors * gr2_syst = syst_error_band(&fA2Full->fSystErrVsW, -1.0,0.3);
      for( int j_point = gr1->GetN()-1 ; j_point>=0; j_point--) {
        Double_t xt, yt;
        gr1->GetPoint(j_point,xt,yt);
        Int_t    abin    = fA1Full->fSystErrVsW.FindBin(xt);
        Double_t eyt     = gr1->GetErrorY(j_point);
        Double_t eyt_sys = fA1Full->fSystErrVsW.GetBinContent(abin);
        if( yt == 0.0  ) {
          gr1->RemovePoint(j_point);
          gr2->RemovePoint(j_point);
          //fA1Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fA1Full->fSystErrVsW.SetBinError(abin,0.0);
          //fA2Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fA2Full->fSystErrVsW.SetBinError(abin,0.0);

          //fA1Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fA1Full->fSystErrVsW.SetBinError(abin,0.0);
          //fA2Full->fSystErrVsW.SetBinContent(abin,0.0);
          //fA2Full->fSystErrVsW.SetBinError(abin,0.0);

          gr1_syst->RemovePoint(j_point);
          gr2_syst->RemovePoint(j_point);
          continue;
        }
      }
      //gr1_syst->SetFillColor(fA1->GetLineColor());
      //gr2_syst->SetFillColor(fA1->GetLineColor());
      mg1->Add(gr1,"ep");
      mg1_syst->Add(gr1_syst,"e3");
      mg2->Add(gr2,"ep");
      mg2_syst->Add(gr2_syst,"e3");
    }


    std::string   filename0       = Form("A1_59%d_%d.txt",aNumber,jj);
    std::string   table_filename0 = table_dir0 + filename0;
    std::ofstream outtable0(table_filename0.c_str());
    fA1Full->PrintBinnedTableHead(outtable0);
    fA1Full->PrintBinnedTable(outtable0);

    filename0       = Form("A2_59_%d_%d.txt",aNumber,jj);
    table_filename0 = table_dir0 + filename0;
    std::ofstream outtable1(table_filename0.c_str());
    fA2Full->PrintBinnedTableHead(outtable1);
    fA2Full->PrintBinnedTable(outtable1);

  }

  // ------------------------------------------
  TCanvas * c = new TCanvas();
  gSystem->mkdir(Form("results/asymmetries/%d",RunGroup),true);

  TMultiGraph * MG_1 = new TMultiGraph();
  MG_1->Add(mg1);
  MG_1->Add(mg1_syst);
  MG_1->Draw("a");
  MG_1->GetXaxis()->SetLimits(1.0,3.2); 
  MG_1->GetYaxis()->SetRangeUser(-2.0,2.0); 
  MG_1->Draw("a");

  c->SaveAs(Form("results/asymmetries/%d/A1_59_W_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/A1_59_W_%d.pdf",RunGroup,RunGroup));

  // ------------------------------------------
  c = new TCanvas();

  TMultiGraph * MG_2 = new TMultiGraph();
  MG_2->Add(mg2);
  MG_2->Add(mg2_syst);
  MG_2->Draw("a");
  MG_2->GetXaxis()->SetLimits(1.0,3.2); 
  MG_2->GetYaxis()->SetRangeUser(-2.0,2.0); 
  MG_2->Draw("a");
  c->SaveAs(Form("results/asymmetries/%d/A2_59_W_%d.png",RunGroup,RunGroup));
  c->SaveAs(Form("results/asymmetries/%d/A2_59_W_%d.pdf",RunGroup,RunGroup));



  TFile * fout = TFile::Open(Form("data/A1A2_59_%d.root",RunGroup),"UPDATE");
  fout->WriteObject(fAllA1Asym,Form("A1_59_W"));
  fout->WriteObject(fAllA2Asym,Form("A2_59_W"));
  fout->Close();


  return 0;
}
