#ifndef __CINT__
#include "TMath.h"
#include "Math/DistFunc.h"
#include <iostream>
#endif


/**
 *
 */
class TrackerFit {
   protected:
      int fNPeaks;
   public : 
      TrackerFit(){fNPeaks = 5;}
      ~TrackerFit(){}

      double ElectronPeak(double*x,double*p){
         double res = 0;
         double A            = p[0];
         double lambda       = p[1];
         double mu_0         = p[2];
         double sigma_0      = p[3];
         double Sigma        = p[4];
         double Delta        = p[5];
         double y            = x[0];
         for(int i = -15; i< 15;i++){
            res += A*TMath::Gaus(i,mu_0/sigma_0,Sigma/sigma_0,true)*
                     TMath::Gaus(y,mu_0+float(i)*sigma_0,sigma_0,true);
         }
         return(res);
      }

      double PositronPeak(double*x,double*p){
         double res = 0;
         double A            = p[0];
         double lambda       = p[1];
         double mu_0         = p[2];
         double sigma_0      = p[3];
         double Sigma        = p[4];
         double Delta        = p[5];
         double y            = x[0];
         for(int i = -15; i< 15;i++){
            res += A*lambda*TMath::Gaus(i,(mu_0+Delta)/sigma_0,Sigma/sigma_0,true)*
                      TMath::Gaus(y,mu_0+Delta+float(i)*sigma_0,sigma_0,true);
         } 
         return(res);
      }

      double fA,flambda,fSigma,fDelta,fsigma_0,fmu_0,fdelta_0;

      void Print(){
         std::cout << " ---------TrackerFit ----------- " << std::endl;
         std::cout << " fA        = " << fA << std::endl;
         std::cout << " flambda   = " << flambda << std::endl;
         std::cout << " fmu_0     = " << fmu_0 << std::endl;
         std::cout << " fsigam_0  = " << fsigma_0 << std::endl;
         std::cout << " fSigma    = " << fSigma << std::endl;
         std::cout << " fDelta    = " << fDelta << std::endl;
         std::cout << " fdelta_0  = " << fdelta_0 << std::endl;
      }

      void SetParameters(double *p) {
         fA            = p[0];
         flambda       = p[1];
         fmu_0         = p[2];
         fsigma_0      = p[3];
         fSigma        = p[4];
         fDelta        = p[5];
         fdelta_0      = p[6];
      }

      double operator()(double *x, double *p ){
         double res          = 0;
         // Parameters
         double A            = p[0];
         double lambda       = p[1];
         double mu_0         = p[2];
         double sigma_0      = p[3];
         double Sigma        = p[4];
         double Delta        = p[5];
         double delta_0      = p[6];
         double y            = x[0];

         for(int i = -15; i< 15;i++){
//            std::cout << "i = " << i  << std::endl;
            res += A*TMath::Gaus(i+delta_0,(mu_0)/sigma_0,Sigma/sigma_0,true)*
                     TMath::Gaus(y,mu_0+float(i)*sigma_0,sigma_0,true);
            res += A*lambda*TMath::Gaus(i+delta_0,(mu_0+Delta)/sigma_0,Sigma/sigma_0,true)*
                      TMath::Gaus(y,mu_0+Delta+float(i)*sigma_0,sigma_0,true);
         
         } 
//        std::cout << res  << std::endl;
         return(res);
      }
};

