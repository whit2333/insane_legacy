/*! Function returning the weighted mean of a std::vector<Double_t> x values and errors ex.
    Results are returned using the arguments mean  and  error.
 */
Int_t get_weighted_mean(std::vector<Double_t> * x, std::vector<Double_t> * ex, Double_t & mean, Double_t & error) {
   if(x->size() != ex->size()){
      std::cout << "get_weighted_mean() vectors are of different size!" << std::endl;
   }
   std::cout << " size = " << ex->size() << std::endl;
   mean = 0.0;
   error = 0.0;
   Double_t one_over_sig2 = 0.0;
   Double_t x_over_sig2 = 0.0;
   for( int i = 0; i<x->size() && i<ex->size(); i++ ) {
      one_over_sig2 += 1.0/(TMath::Power(ex->at(i),2.0));
      x_over_sig2 += x->at(i)/(TMath::Power(ex->at(i),2.0));
   }

   mean  = x_over_sig2/one_over_sig2;
   error = TMath::Sqrt(1.0 /one_over_sig2);

   return(0);
}

/*!
 */ 
Int_t get_mean_variance(std::vector<Double_t> * x, std::vector<Double_t> * ex, Double_t & mean, Double_t & var) {
   if(x->size() != ex->size()){
      std::cout << "get_weighted_mean() vectors are of different size!" << std::endl;
   }
   mean = 0.0;
   var = 0.0;
   for( int i = 0; i<x->size() && i<ex->size(); i++ ) {
      var += TMath::Power(x->at(i),2.0);
      mean += x->at(i);
   }
   Double_t N = double(x->size()) ;
   mean  = mean/N;
   var   = var/(N);
   var = var - mean*mean;

   return(0);
}

