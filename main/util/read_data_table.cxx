Int_t read_data_table(const char * file, std::vector<std::vector<Double_t>> * data, const Int_t nCols= 12  ) {

   std::ifstream infile(file);
   if ( !infile.is_open() ) {
      return(-1);
   }
   if(!data) return(-2);

   std::vector<Double_t> * cols[nCols];
   Double_t vals[nCols];
   for( int i = 0; i < nCols;i++) {
      cols[i] = new std::vector<Double_t>;
   }

   while ( infile.good() ) {
      for(int i = 0; i< nCols ; i++){
         infile >> vals[i];
         cols[i]->push_back(vals[i]);
      }
   }
   //for(int i = 0; i< nCols ; i++){
   //   cols[i]->pop_back();
   //}
   data->clear();
   for( int i = 0; i < nCols;i++) {
      //cols[i] = new std::vector<Double_t>;
      data->push_back(*(cols[i]));
      //std::cout << i << std::endl;
   }

    
   return(0);
}


