Int_t merge_run_list( const char * ifname,const char * ofname = "data/MergedRunList.root", const char * tname = "Tracks"){

   aman->BuildQueue2(ifname);
   std::cout << "Using run list from file: " << ifname << "\n";

   TChain * ch = aman->BuildChain(tname);

   std::cout << tname << " chain has " << ch->GetEntries() << " entries.\n";

   //TFile* file = TFile::Open(ofname, "RECREATE");
   std::cout << "Merging into file: " << ofname << "\n";
   //ch->Merge(file);
   ch->Merge(ofname);

   gROOT->pwd();
   gROOT->ls();

   return(0);
}
