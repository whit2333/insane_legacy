InSANEVirtualComptonAsymmetries* get_A2_fit(int aNumber = 108) {

  const char * fname   = "/home/whit/work/SANE/input_parameters/A2p_input_pars.txt";

  WA1A2pFit_1 * A2p_fit = new WA1A2pFit_1(fname,fname);

  //TF1 * Model1 = new TF1("Model1",A2p_fit,&InSANE_VCSABase::EvaluateA2p,
  //    0.001, 0.9, 1,"InSANE_VCSABase","EvaluateA2p");
  //Model1->SetParameter(0,3.0);

  //TF1 * Model_W = new TF1("ModelW",A2p_fit,&InSANE_VCSABase::EvaluateA2p_W,
  //    1.0, 20.0, 1,"InSANE_VCSABase","EvaluateA2p_W");
  //Model_W->SetParameter(0,3.0);

  //Model_W->Draw();

  return A2p_fit;
}
