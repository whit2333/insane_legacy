/** Build an error band.
 */
TGraphErrors * syst_error_band(TH1* hist, double y_offset = 0.0, double alpha = 0.5) 
{

  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);
   if(!hist) { 
      std::cout << "stat_error_graph : Bad hist" << std::endl;
   }

   TH1 * hcopy = (TH1*)hist->Clone();
   hcopy->SetFillColor(hist->GetLineColor());
   hcopy->SetLineColor(hist->GetLineColor());
   hcopy->Scale(1.0/2.0);
   //hcopy->Sumw2();
   TGraphErrors * gr = new TGraphErrors(hcopy);
   for( int j = gr->GetN()-1 ;j>=0; j--) {
      Double_t xt, yt;
      Double_t ext, eyt;
      gr->GetPoint(j,xt,yt);
      eyt = gr->GetErrorY(j);
      //if( yt == 0.0 ) {
      //   gr->RemovePoint(j);
      //   continue;
      //}
      //if( eyt >10.0 || TMath::IsNaN(eyt)  ) {
      //   gr->RemovePoint(j);
      //   continue;
      //}
      gr->SetPoint(j,xt,yt+y_offset);
      gr->SetPointError(j,0.0,yt);
   }
   gr->SetFillColorAlpha(hist->GetLineColor(),alpha);
  TH1::AddDirectory(add_status);
   return gr;

}

TGraphErrors * syst_error_band(TGraphErrors * gr_sys, double y_offset = 0.0, double alpha = 0.5) 
{

  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);

   TGraphErrors * gr = new TGraphErrors(*gr_sys);
   for( int j = gr->GetN()-1 ;j>=0; j--) {
      Double_t xt, yt;
      Double_t ext, eyt;
      gr->GetPoint(j,xt,yt);
      eyt = gr->GetErrorY(j);
      //if( yt == 0.0 ) {
      //   gr->RemovePoint(j);
      //   continue;
      //}
      //if( eyt >10.0 || TMath::IsNaN(eyt)  ) {
      //   gr->RemovePoint(j);
      //   continue;
      //}
      gr->SetPoint(j,xt,yt/2.0+y_offset);
      gr->SetPointError(j,0.0,yt/2.0);
   }
   gr->SetFillColorAlpha(gr_sys->GetLineColor(),alpha);
   TH1::AddDirectory(add_status);
   return gr;
}

