#ifndef d2_table_HH
#define d2_table_HH

struct d2_table_entry {
  double Q2_mean           = 0.0;
  double Q2_min            = 0.0;
  double Q2_max            = 0.0;
  double W_mean            = 0.0;
  double W_min             = 0.0;
  double W_max             = 0.0;
  double x_min             = 0.0;
  double x_max             = 0.0;
  int    n_xbins           = 0;
  double d2_WW_measured    = 0.0;
  double d2_WW_stat_unc    = 0.0;
  double d2_WW_syst_unc    = 0.0;
  double d2_CN_measured    = 0.0;
  double d2_CN_stat_unc    = 0.0;
  double d2_CN_syst_unc    = 0.0;
  double d2_Nacht_measured = 0.0;
  double d2_Nacht_stat_unc = 0.0;
  double d2_Nacht_syst_unc = 0.0;
  double d2_OAR_measured   = 0.0;
  double d2_OAR_stat_unc   = 0.0;
  double d2_OAR_syst_unc   = 0.0;
  double d2_CN_low_x          = 0.0;
  double d2_CN_high_x         = 0.0;
  double d2_CN_low_x_unc      = 0.0;
  double d2_CN_high_x_unc     = 0.0;
  double d2_WW_low_x    = 0.0;
  double d2_WW_high_x   = 0.0;
  double d2_WW_low_x_unc = 0.0;
  double d2_WW_high_x_unc= 0.0;
  double d2_Nacht_low_x    = 0.0;
  double d2_Nacht_high_x   = 0.0;
  double d2_Nacht_low_x_unc = 0.0;
  double d2_Nacht_high_x_unc= 0.0;
  double M11               = 0.0;
  double M13               = 0.0;
  double M23               = 0.0;
  double d2_WW_el          = 0.0;
  double d2_WW_el_stat_unc    = 0.0;
  double d2_WW_el_syst_unc    = 0.0;
  double d2_CN_el       = 0.0;
  double d2_CN_el_stat_unc    = 0.0;
  double d2_CN_el_syst_unc    = 0.0;
  double d2_Nacht_el    = 0.0;
  double d2_Nacht_el_stat_unc = 0.0;
  double d2_Nacht_el_syst_unc = 0.0;
  double d2_WW_total    = 0.0;
  double d2_WW_total_stat_unc = 0.0;
  double d2_WW_total_syst_unc = 0.0;
  double d2_CN_total    = 0.0;
  double d2_CN_total_stat_unc = 0.0;
  double d2_CN_total_syst_unc = 0.0;
  double d2_Nacht_total    = 0.0;
  double d2_Nacht_total_stat_unc = 0.0;
  double d2_Nacht_total_syst_unc = 0.0;
  int    number            = 0;
};
//______________________________________________________________________________

TMultiGraph* CreateGraphPoints(const d2_table_entry& p, int col = 2000)
{

  TMultiGraph       * mg         = new TMultiGraph();
  TGraphAsymmErrors * gr_x_range = new TGraphAsymmErrors(1);
  TGraphAsymmErrors * gr_x_range2= new TGraphAsymmErrors(1);
  TGraphErrors      * gr_1       = new TGraphErrors(1);
  TGraphErrors      * gr_syst    = new TGraphErrors(1);
  TGraphErrors      * gr_lowx    = new TGraphErrors(1);
  TGraphErrors      * gr_highx   = new TGraphErrors(1);

  TGraphErrors      * gr_2       = new TGraphErrors(1);
  TGraphErrors      * gr_2syst   = new TGraphErrors(1);

  double xmean  = (p.x_max+p.x_min)/2.0;
  double dx     = (p.x_max-p.x_min)/2.0;
  double scale  = 1.0;
  double exl    = scale*xmean;
  double exh    = scale*(1.0-xmean);
  double exl2   = scale*dx;
  double exh2   = scale*dx;

  gr_1->SetMarkerStyle(20);
  gr_1->SetMarkerColor(col);
  gr_1->SetLineColor(col);
  gr_1   ->SetLineWidth(2);
  gr_syst->SetMarkerColorAlpha(col,0.4);
  gr_syst->SetLineColor(col);
  gr_syst->SetLineWidth(2);

  gr_2->SetMarkerStyle(24);
  gr_2->SetMarkerColor(col);
  gr_2->SetLineColor(col);
  gr_2   ->SetLineWidth(1);
  gr_2syst->SetMarkerColorAlpha(col,0.4);
  gr_2syst->SetLineColor(col);
  gr_2syst->SetLineWidth(2);

  gr_lowx->SetMarkerStyle(21);
  gr_lowx->SetMarkerColor(col);
  gr_lowx->SetLineColor(col);
  gr_lowx->SetLineWidth(2);

  gr_highx->SetMarkerStyle(23);
  gr_highx->SetMarkerColor(col);
  gr_highx->SetLineColor(col);
  gr_highx->SetLineWidth(2);

  gr_1->SetPoint(     0, p.Q2_mean, p.d2_Nacht_measured);
  gr_1->SetPointError(0, 0.0      , p.d2_Nacht_stat_unc);

  gr_2->SetPoint(     0, p.Q2_mean+0.1, p.d2_OAR_measured);
  gr_2->SetPointError(0, 0.0      , p.d2_OAR_stat_unc);

  gr_lowx->SetPoint(     0, p.Q2_mean-0.05, p.d2_CN_low_x);
  gr_lowx->SetPointError(0, 0.0      , p.d2_CN_low_x_unc);

  gr_highx->SetPoint(     0, p.Q2_mean-0.1, p.d2_CN_high_x);
  gr_highx->SetPointError(0, 0.0      , p.d2_CN_high_x_unc);

  gr_syst->SetPoint(0, p.Q2_mean, p.d2_Nacht_measured);
  gr_syst->SetPointError(0, 0.0 , p.d2_Nacht_stat_unc+p.d2_Nacht_syst_unc);

  gr_2syst->SetPoint(0, p.Q2_mean+0.1, p.d2_OAR_measured);
  gr_2syst->SetPointError(0, 0.0 , p.d2_OAR_stat_unc+p.d2_OAR_syst_unc);

  gr_x_range->SetPoint(0, p.Q2_mean, p.d2_Nacht_measured);
  gr_x_range->SetPointError(0, exl, exh, 0.0,0.0);
  gr_x_range2->SetPoint(0, p.Q2_mean, p.d2_Nacht_measured);
  gr_x_range2->SetPointError(0, exl2, exh2, 0.0,0.0);

  gr_x_range2->SetLineColor(2);
  gr_x_range->SetLineColor(1);
  gr_x_range2->SetLineWidth(3);
  gr_x_range->SetLineWidth(5);

  mg->Add(gr_lowx,"ep");
  mg->Add(gr_highx,"ep");
  mg->Add(gr_x_range,"e");
  mg->Add(gr_x_range2,"e");
  mg->Add(gr_1,"ep");
  mg->Add(gr_syst,"[]");
  mg->Add(gr_2,"ep");
  mg->Add(gr_2syst,"[]");

  return mg;
}
//______________________________________________________________________________

#endif

