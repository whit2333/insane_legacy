/*! Use to convert histogram to a graph.
 */
Int_t hist_to_graph(TH1* h, TGraphErrors * g, Bool_t ignore_zeros = true) {

   if(!h) { 
      std::cout << "Error Histogram is null.\n";
      return(-1);
   }
   Int_t   binx,biny,binz;
   Int_t   bin = 0;
   Int_t   nxmax = h->GetNbinsX();
   Double_t x,y,ex,ey;

   std::vector<Double_t> *x_vals  = new std::vector<Double_t>;
   std::vector<Double_t> *y_vals  = new std::vector<Double_t>;
   std::vector<Double_t> *ex_vals = new std::vector<Double_t>;
   std::vector<Double_t> *ey_vals = new std::vector<Double_t>;

   for(Int_t i=1; i<= nxmax; i++){
      bin   = i;//h->GetBin(i,j,k);
      //h->GetBinXYZ(bin,binx,biny,binz);
      x     = h->GetXaxis()->GetBinCenter(bin);
      ex    = 0; 
      y     = h->GetBinContent(bin);
      ey    = h->GetBinError(bin);
      if( y == 0 && ignore_zeros ) continue; 
      else {
         x_vals->push_back(x);
         y_vals->push_back(y);
         ex_vals->push_back(ex);
         ey_vals->push_back(ey);
      }
   }
   g = new TGraphErrors(x_vals->size(),&(*x_vals)[0],&(*y_vals)[0],&(*ex_vals)[0],&(*ey_vals)[0]);
   //g->Draw();
   return(0);
}  


TGraphErrors*  hist_to_graph(const TH1* h,  Bool_t ignore_zeros = true)
{
  TGraphErrors * gr = new TGraphErrors();
  gr->SetMarkerColor(h->GetMarkerColor());
  gr->SetLineColor(  h->GetLineColor());
  gr->SetMarkerStyle(h->GetMarkerStyle());
  gr->SetLineStyle(  h->GetLineStyle());

  Int_t   nxmax = h->GetNbinsX();
  Double_t x,y,ex,ey;

  int ipoints = 0;
  for(int i=1; i<= nxmax; i++){
    x     = h->GetXaxis()->GetBinCenter(i);
    ex    = 0; 
    y     = h->GetBinContent(i);
    ey    = h->GetBinError(i);
    if( y == 0 && ignore_zeros ) {
      continue; 
    } else {
      gr->SetPoint(ipoints, x, y);
      gr->SetPointError(ipoints, ex, ey);
      ipoints++;
    }
  }
  return gr;
}
