Int_t read_data_table_column_to_map(const char * file, std::map<int,double> * data,Int_t aCol, const Int_t nCols= 9  ) {

   std::ifstream infile(file);
   if ( !infile.is_open() ) {
      return(-1);
   }
   if(!data) return(-2);

   std::vector<double> * cols[nCols];
   Double_t vals[nCols];
   for( int i = 0; i < nCols;i++) {
      cols[i] = new std::vector<double>;
   }

   while ( infile.good() ) {
      for(int i = 0; i< nCols ; i++){
         infile >> vals[i];
         if(infile.eof()) break;
         cols[i]->push_back(vals[i]);
         //std::cout << i << " = " << vals[i] << std::endl;
      }
   }
   data->clear();
   for( int i = 0; i < cols[0]->size(); i++){
      int rn = TMath::Floor(cols[0]->at(i));
      double val = cols[aCol]->at(i) ;
      data->insert(std::pair<int,double>(rn,val) );
   }
   //for( int i = 0; i < nCols;i++) {
   //   //cols[i] = new std::vector<Double_t>;
   //   int rn = TMath::Floor(cols[i].at(0));
   //   double val = cols[i]->at(aCol) ;
   //   data->insert(std::pair<int,double>(rn,val) );
   //   //std::cout << i << std::endl;
   //}

    
   return(0);
}


