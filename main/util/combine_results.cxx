struct CombineParameters {
  double y1_max       = 1.0;
  double y2_max       = 1.0;
  double ey1_max      = 1.0;
  double ey2_max      = 1.0;
  double ey1_max_syst = 1.0;
  double ey2_max_syst = 1.0;

  bool is_good_1(double val1, double unc1, double syst_unc1){
    if( val1 == 0.0 ){
      return false;
    }
    if( abs(val1) > y1_max){
     return false;
    }
    if( unc1 > ey1_max ) {
      return false;
    }
    if( syst_unc1 > ey1_max_syst ) {
      return false;
    }
    return true;
  }

  bool is_good_2(double val2, double unc2, double syst_unc2){
    if( val2 == 0.0 ){
      return false;
    }
    if( abs(val2) > y2_max){
     return false;
    }
    if( unc2 > ey2_max ) {
      return false;
    }
    if( syst_unc2 > ey2_max_syst ) {
      return false;
    }
    return true;
  }
};
//______________________________________________________________________________


/** Combine identically binned histogram results
 */
std::tuple<TH1F*, TH1F*,InSANEAveragedKinematics1D,std::vector<InSANESystematic> > combine_results(
    CombineParameters cpars,
    TH1F* h1,              TH1F* h2,
    TH1F* syst1,           TH1F* syst2,
    TH1F* m1,              TH1F* m2,
    InSANEAveragedKinematics1D * k1,
    InSANEAveragedKinematics1D * k2,
    std::vector<InSANESystematic> *s1, 
    std::vector<InSANESystematic> *s2
    )
{

  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);
  int n_bins_x  = h1->GetNbinsX();
  int n_bins_x2 = h2->GetNbinsX();

  if( n_bins_x != n_bins_x2) {
    std::cout << "util/combine_bins() error : histograms have different bins.\n";
    //return nullptr;
  }

  TH1F *                          hRes      = (TH1F*)h1->Clone();
  TH1F *                          hRes_syst = (TH1F*)syst1->Clone();;
  InSANEAveragedKinematics1D      hRes_kine = (*k1);
  std::vector<InSANESystematic>   all_syst  = (*s1);
  hRes->Reset();
  hRes_syst->Reset();
  for(auto& a_sys : all_syst){
    a_sys.Reset();
  }

  for(int i_bin = 1; i_bin <= n_bins_x; i_bin++) {
    int gbin = h1->GetBin(i_bin);

    double val1 = h1->GetBinContent(gbin);
    double val2 = h2->GetBinContent(gbin);
    double unc1 = h1->GetBinError(gbin);
    double unc2 = h2->GetBinError(gbin);
    double syst_unc1 = syst1->GetBinContent(gbin);
    double syst_unc2 = syst2->GetBinContent(gbin);

    double mask1 = m1->GetBinContent(gbin);
    double mask2 = m2->GetBinContent(gbin);

    // take weighted mean
    double combined_num   = 0.0;
    double combined_den   = 0.0;
    double average_syst   = 0.0;
    int    n_comb         = 0;
    InSANESingleBinVariables average_bin_kine;

    // combine
    //
    if( (mask1>0.0) && (cpars.is_good_1(val1,unc1,syst_unc1)) ) {
      combined_num += val1/(unc1*unc1);
      combined_den += 1.0/(unc1*unc1);
      n_comb++;
      average_syst += syst_unc1;
      average_bin_kine += k1->GetSingleBin(gbin);

      int i_sys = 0;
      for(auto& a_sys : all_syst){

        double systemp_1 = (*s1)[i_sys].fBefore               .GetBinContent(gbin); 
        double systemp_2 = (*s1)[i_sys].fAfter                .GetBinContent(gbin);          
        double systemp_3 = (*s1)[i_sys].fValues               .GetBinContent(gbin);
        double systemp_4 = (*s1)[i_sys].fUncertainties        .GetBinContent(gbin);
        double systemp_5 = (*s1)[i_sys].fRelativeUncertainties.GetBinContent(gbin);
        double systemp0_1 = a_sys.fBefore               .GetBinContent(gbin); 
        double systemp0_2 = a_sys.fAfter                .GetBinContent(gbin);          
        double systemp0_3 = a_sys.fValues               .GetBinContent(gbin);
        double systemp0_4 = a_sys.fUncertainties        .GetBinContent(gbin);
        double systemp0_5 = a_sys.fRelativeUncertainties.GetBinContent(gbin);
        if(systemp0_1 != 0.0) {
          std::cout << "systemp_1 not zero! systemp0_1 " << std::endl;
        }
        a_sys.fBefore               .SetBinContent(gbin,systemp0_1+systemp_1); 
        a_sys.fAfter                .SetBinContent(gbin,systemp0_2+systemp_2);          
        a_sys.fValues               .SetBinContent(gbin,systemp0_3+systemp_3);
        a_sys.fUncertainties        .SetBinContent(gbin,systemp0_4+systemp_4);
        a_sys.fRelativeUncertainties.SetBinContent(gbin,systemp0_5+systemp_5);
        i_sys++;
      }
    }

    if( (mask2>0.0) && (cpars.is_good_2(val2,unc2,syst_unc2)) ) {
      combined_num += val2/(unc2*unc2);
      combined_den += 1.0/(unc2*unc2);
      n_comb++;
      average_syst += syst_unc2;
      average_bin_kine += k2->GetSingleBin(gbin);

      int i_sys = 0;
      for(auto& a_sys : all_syst){

        double systemp_1 = (*s2)[i_sys].fBefore               .GetBinContent(gbin); 
        double systemp_2 = (*s2)[i_sys].fAfter                .GetBinContent(gbin);          
        double systemp_3 = (*s2)[i_sys].fValues               .GetBinContent(gbin);
        double systemp_4 = (*s2)[i_sys].fUncertainties        .GetBinContent(gbin);
        double systemp_5 = (*s2)[i_sys].fRelativeUncertainties.GetBinContent(gbin);
        double systemp0_1 = a_sys.fBefore               .GetBinContent(gbin); 
        double systemp0_2 = a_sys.fAfter                .GetBinContent(gbin);          
        double systemp0_3 = a_sys.fValues               .GetBinContent(gbin);
        double systemp0_4 = a_sys.fUncertainties        .GetBinContent(gbin);
        double systemp0_5 = a_sys.fRelativeUncertainties.GetBinContent(gbin);
        a_sys.fBefore               .SetBinContent(gbin,systemp0_1+systemp_1); 
        a_sys.fAfter                .SetBinContent(gbin,systemp0_2+systemp_2);          
        a_sys.fValues               .SetBinContent(gbin,systemp0_3+systemp_3);
        a_sys.fUncertainties        .SetBinContent(gbin,systemp0_4+systemp_4);
        a_sys.fRelativeUncertainties.SetBinContent(gbin,systemp0_5+systemp_5);
        i_sys++;
      }
    }

    double combined_val   = 0.0;
    double combined_unc   = 0.0;

    if(n_comb > 0 ) {
      average_syst      = average_syst/float(n_comb);
      average_bin_kine /= double(n_comb);
      combined_val      = combined_num/combined_den;
      combined_unc      = TMath::Sqrt(1.0/combined_den);

      for(auto& a_sys : all_syst){
        double systemp0_1 = a_sys.fBefore               .GetBinContent(gbin); 
        double systemp0_2 = a_sys.fAfter                .GetBinContent(gbin);          
        double systemp0_3 = a_sys.fValues               .GetBinContent(gbin);
        double systemp0_4 = a_sys.fUncertainties        .GetBinContent(gbin);
        double systemp0_5 = a_sys.fRelativeUncertainties.GetBinContent(gbin);

        a_sys.fBefore               .SetBinContent(gbin,systemp0_1/float(n_comb)); 
        a_sys.fAfter                .SetBinContent(gbin,systemp0_2/float(n_comb));          
        a_sys.fValues               .SetBinContent(gbin,systemp0_3/float(n_comb));
        a_sys.fUncertainties        .SetBinContent(gbin,systemp0_4/float(n_comb));
        a_sys.fRelativeUncertainties.SetBinContent(gbin,systemp0_5/float(n_comb));
      }
    }
    hRes->SetBinContent(gbin, combined_val);
    hRes->SetBinError(  gbin, combined_unc);
    hRes_syst->SetBinContent(gbin, average_syst);
    hRes_kine.SetSingleBin(gbin, average_bin_kine);
  }

  TH1::AddDirectory(add_status);
  return std::make_tuple(hRes,hRes_syst, hRes_kine, all_syst);
}
//______________________________________________________________________________


/** Combine identically binned histogram results
 */
std::pair<TH1F*, TH1F*> combine_results(
    CombineParameters cpars,
    TH1F* h1,              TH1F* h2, 
    TH1F* syst1 = nullptr, TH1F* syst2 = nullptr
    )
{

  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);
  int n_bins_x  = h1->GetNbinsX();
  int n_bins_x2 = h2->GetNbinsX();

  if( n_bins_x != n_bins_x2) {
    std::cout << "util/combine_bins() error : histograms have different bins.\n";
    //return nullptr;
  }
  bool use_syst = false;
  if( syst1 ) {
    if( syst2 ) {
      use_syst = true;
    }
  }

  TH1F * hRes      = (TH1F*)h1->Clone();
  TH1F * hRes_syst = nullptr;

  hRes->Reset();
  if(use_syst){
    hRes_syst = (TH1F*)syst1->Clone();
    hRes_syst->Reset();
  }

  for(int i_bin = 1; i_bin <= n_bins_x; i_bin++) {
    int gbin = h1->GetBin(i_bin);

    double val1 = h1->GetBinContent(gbin);
    double val2 = h2->GetBinContent(gbin);
    double unc1 = h1->GetBinError(gbin);
    double unc2 = h2->GetBinError(gbin);

    double syst_unc1 = 0.0;
    double syst_unc2 = 0.0;

    if( use_syst ){
      syst_unc1 = syst1->GetBinContent(gbin);
      syst_unc2 = syst2->GetBinContent(gbin);
    }
    // take weighted mean
    double combined_num   = 0.0;
    double combined_den   = 0.0;
    double average_syst   = 0.0;
    int    n_comb         = 0;

    //
    // combine
    //
    if( (abs(val1) < cpars.y1_max) && unc1 < cpars.ey1_max ) {
      if( use_syst) {
        if(  syst_unc1 < cpars.ey1_max_syst ) {
          combined_num += val1/(unc1*unc1);
          combined_den += 1.0/(unc1*unc1);
          n_comb++;
          average_syst += syst_unc1;
        }
      } else {
        combined_num += val1/(unc1*unc1);
        combined_den += 1.0/(unc1*unc1);
        n_comb++;
      }
    }

    if( (abs(val2) < cpars.y2_max) && unc2 < cpars.ey2_max ) {
      if( use_syst) { 
        if(  unc2 < cpars.ey2_max_syst ) {
          average_syst += syst_unc2;
          combined_num += val2/(unc2*unc2);
          combined_den += 1.0/(unc2*unc2);
          n_comb++;
        }
      } else {
        combined_num += val2/(unc2*unc2);
        combined_den += 1.0/(unc2*unc2);
        n_comb++;
      }
    }

    double combined_val   = 0.0;
    double combined_unc   = 0.0;
    if(n_comb > 0 ) {
      average_syst = average_syst/float(n_comb);
      combined_val = combined_num/combined_den;
      combined_unc = TMath::Sqrt(1.0/combined_den);
    }
    hRes->SetBinContent(gbin, combined_val);
    hRes->SetBinError(  gbin, combined_unc);
    if(use_syst) {
      hRes_syst->SetBinContent(gbin, average_syst);
    }
  }

  TH1::AddDirectory(add_status);
  return std::make_pair(hRes,hRes_syst);
}
//______________________________________________________________________________


/** Combine identically binned histogram results
 */
std::pair<TH1F*, TH1F*> combine_results(
    TH1F* h1,              TH1F* h2, 
    TH1F* syst1 = nullptr, TH1F* syst2 = nullptr
    )
{

  bool add_status = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);

  int n_bins_x  = h1->GetNbinsX();
  int n_bins_x2 = h2->GetNbinsX();

  if( n_bins_x != n_bins_x2) {
    std::cout << "util/combine_bins() error : histograms have different bins.\n";
    //return nullptr;
  }
  bool use_syst = false;
  if( syst1 ) {
    if( syst2 ) {
      use_syst = true;
    }
  }

  TH1F * hRes      = (TH1F*)h1->Clone();
  TH1F * hRes_syst = nullptr;

  hRes->Reset();
  if(use_syst){
    hRes_syst = (TH1F*)syst1->Clone();
    hRes_syst->Reset();
  }

  for(int i_bin = 1; i_bin <= n_bins_x; i_bin++) {
    int gbin = h1->GetBin(i_bin);

    double val1 = h1->GetBinContent(gbin);
    double val2 = h2->GetBinContent(gbin);
    double unc1 = h1->GetBinError(gbin);
    double unc2 = h2->GetBinError(gbin);

    double syst_unc1 = 0.0;
    double syst_unc2 = 0.0;

    if( use_syst ){
      syst_unc1 = syst1->GetBinContent(gbin);
      syst_unc2 = syst2->GetBinContent(gbin);
    }
    // take weighted mean
    double combined_num   = 0.0;
    double combined_den   = 0.0;
    double average_syst   = 0.0;
    int    n_comb         = 0;

    //
    // combine
    //
    if( (abs(val1) < 1.0) && unc1 < 1.0 ) {
      combined_num += val1/(unc1*unc1);
      combined_den += 1.0/(unc1*unc1);
      n_comb++;
      if( use_syst) { 
        average_syst += syst_unc1;
      }
    }
    if( (abs(val2) < 1.0) && unc2 < 1.0 ) {
      combined_num += val1/(unc2*unc2);
      combined_den += 1.0/(unc2*unc2);
      n_comb++;
      if( use_syst) { 
        average_syst += syst_unc2;
      }
    }

    double combined_val   = 0.0;
    double combined_unc   = 0.0;
    if(n_comb > 0 ) {
      average_syst = average_syst/float(n_comb);
      combined_val = combined_num/combined_den;
      combined_unc = TMath::Sqrt(1.0/combined_den);
    }
    hRes->SetBinContent(gbin, combined_val);
    hRes->SetBinError(  gbin, combined_unc);
    if(use_syst) {
      hRes_syst->SetBinContent(gbin, average_syst);
    }
  }

  TH1::AddDirectory(add_status);
  return std::make_pair(hRes,hRes_syst);
}
//______________________________________________________________________________
