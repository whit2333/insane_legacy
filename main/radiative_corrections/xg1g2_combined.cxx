#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

Int_t xg1g2_combined(Int_t paraFileVersion = 4609,Int_t perpFileVersion = 4609, 
                    Int_t para47Version = 4609, Int_t perp47Version = 4609,Int_t aNumber=4609){
   sane_data_bins();

   // 5.9 GeV asymmetries
   TFile * f = new TFile(Form("data/A1A2_59_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();
   //f->ls();
   TList * fA1Asymmetries59 = (TList*)gROOT->FindObject(Form("xg1-59-x-%d",0));
   if(!fA1Asymmetries59) return(-1);
   TList * fA2Asymmetries59 = (TList*)gROOT->FindObject(Form("xg2-59-x-%d",0));
   if(!fA2Asymmetries59) return(-2);

   // 4.7 GeV asymmetries
   TFile * f47 = new TFile(Form("data/A1A2_47_noinel_%d_%d.root",para47Version,perp47Version),"UPDATE");
   f47->cd();
   TList * fA1Asymmetries47 = (TList*)gROOT->FindObject(Form("xg1-47-x-%d",0));
   if(!fA1Asymmetries47) return(-3);
   TList * fA2Asymmetries47 = (TList*)gROOT->FindObject(Form("xg2-47-x-%d",0));
   if(!fA2Asymmetries47) return(-4);

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("inelastic-subtracted-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-3);

   const char * parafile2 = Form("data/bg_corrected_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f12 = TFile::Open(parafile2,"UPDATE");
   f12->cd();
   TList * fParaAsymmetries2 = (TList *) gROOT->FindObject(Form("inelastic-subtracted-asym_para47-%d",0));
   if(!fParaAsymmetries2) return(-4);
   //fParaAsymmetries->Print();

   //const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   //TFile * f2 = TFile::Open(perpfile,"UPDATE");
   //f2->cd();
   //TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("inelastic-subtracted-asym_perp59-%d",0));
   //if(!fPerpAsymmetries) return(-4);
   ////fPerpAsymmetries->Print();

   TList * fA1combined = new TList();
   TList * fA2combined = new TList();
   TList * fF1combined = new TList();

   TH1F * fA1_59 = 0;
   TH1F * fA2_59 = 0;
   TH1F * fF1_59 = 0;
   TH1F * fA1_47 = 0;
   TH1F * fA2_47 = 0;
   TH1F * fF1_47 = 0;
   TH1F * fA1    = 0;
   TH1F * fA2    = 0;
   TH1F * fF1    = 0;

   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   /// The asymmetry is calculated A = C1(A180*C2+A80*C3)
   TCanvas * c = new TCanvas("A1A2_combined","A1 A2 combined");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);
   TLegend *leg_sane = new TLegend(0.65, 0.65, 0.89, 0.89);

   TGraphErrors *gr_W = 0;
   TList * fAsymmetries_W = new TList();
   TMultiGraph * mg_W = new TMultiGraph();

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries59->GetEntries();jj++) {

      fA1_59 = (TH1F*) fA1Asymmetries59->At(jj);
      fA2_59 = (TH1F*) fA2Asymmetries59->At(jj);
      fA1_47 = (TH1F*) fA1Asymmetries47->At(jj);
      fA2_47 = (TH1F*) fA2Asymmetries47->At(jj);

      fA1 = new TH1F(*fA1_59);
      fA1->Reset();
      fA1combined->Add(fA1);
      fA2 = new TH1F(*fA2_59);
      fA2->Reset();
      fA2combined->Add(fA2);

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * paraAsym2        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries2->At(jj));
      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();
      //InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      InSANEAveragedKinematics1D * paraKine  = &paraAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * paraKine2 = &paraAsym2->fAvgKineVsx;
      //InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsx; 

      Int_t   xBinmax = fA1_59->GetNbinsX();
      Int_t   yBinmax = fA1_59->GetNbinsY();
      Int_t   zBinmax = fA1_59->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      gr_W = new TGraphErrors(xBinmax);
      fAsymmetries_W->Add(gr_W);

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1_59->GetBin(i,j,k);

               Double_t W1     = paraKine->fW.GetBinContent(bin);
               Double_t W2     = paraKine2->fW.GetBinContent(bin);
               Double_t Wavg   = 0;

               Double_t A1_59   = fA1_59->GetBinContent(bin);
               Double_t A2_59   = fA2_59->GetBinContent(bin);
               Double_t eA1_59  = fA1_59->GetBinError(bin);
               Double_t eA2_59  = fA2_59->GetBinError(bin);

               Double_t A1_47   = fA1_47->GetBinContent(bin);
               Double_t A2_47   = fA2_47->GetBinContent(bin);
               Double_t eA1_47  = fA1_47->GetBinError(bin);
               Double_t eA2_47  = fA2_47->GetBinError(bin);

               Double_t A1  = 0.0; 
               Double_t eA1 = 0.0; 
               Double_t A2  = 0.0; 
               Double_t eA2 = 0.0; 


               Wavg = (W1+W2)/2.0;

               if( eA1_47 == 0.0 || TMath::IsNaN(eA1_47) ) {
                  A1  = A1_59;
                  eA1 = eA1_59;
                  Wavg = W1;
               }
               if( eA1_59 == 0.0 || TMath::IsNaN(eA1_59) ) {
                  A1  = A1_47;
                  eA1 = eA1_47;
                  Wavg = W2;
               }
                 
               if( eA1_47 != 0.0 && eA1_59 != 0.0 && !(TMath::IsNaN(eA1_47)) && !(TMath::IsNaN(eA1_59)) ) {

                  A1 = (A1_59/(eA1_59*eA1_59) + A1_47/(eA1_47*eA1_47))/(1.0/(eA1_59*eA1_59) + 1.0/(eA1_47*eA1_47));
                  eA1 = TMath::Sqrt((1.0)/(1.0/(eA1_59*eA1_59) + 1.0/(eA1_47*eA1_47)));
               }


               if( eA2_47 == 0.0 || TMath::IsNaN(eA2_47) ) {
                  A2  = A2_59;
                  eA2 = eA2_59;
                  Wavg = W1;
               }
               if( eA2_59 == 0.0 || TMath::IsNaN(eA2_59) ) {
                  A2  = A2_47;
                  eA2 = eA2_47;
                  Wavg = W2;
               }
               if( eA2_47 != 0.0 && eA2_59 != 0.0 && !(TMath::IsNaN(eA2_47)) && !(TMath::IsNaN(eA2_59)) ) {

                  A2 = (A2_59/(eA2_59*eA2_59) + A2_47/(eA2_47*eA2_47))/(1.0/(eA2_59*eA2_59) + 1.0/(eA2_47*eA2_47));
                  eA2 = TMath::Sqrt((1.0)/(1.0/(eA2_59*eA2_59) + 1.0/(eA2_47*eA2_47)));
               }
               //std::cout << "-----------------------------------" << std::endl;
               //std::cout << F1_47 << " +- " << eF1_47 << std::endl;
               //std::cout << F1_59 << " +- " << eF1_59 << std::endl;
               //std::cout << F1 << " +- " << eF1 << std::endl;

               fA1->SetBinContent(bin,A1);
               fA2->SetBinContent(bin,A2);
               fA1->SetBinError(bin, eA1);
               fA2->SetBinError(bin, eA2);
               //fF1->SetBinContent(bin,F1);
               //fF1->SetBinError(bin, eF1);

               gr_W->SetPoint(i,Wavg,A1);
               gr_W->SetPointError(i,0,eA1);

            }
         }
      }

      // A para converted to vs W
      gr = gr_W;
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(  fA1->GetMarkerColor());
      if(jj%5==4){
         gr->SetMarkerColor(1);
         gr->SetMarkerStyle(24);
         gr->SetLineColor(1);
      }
      if(jj<5){
         mg_W->Add(gr,"p");
         //leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      // -----------------------------------------------
      // xg1 
      TH1 * hrebinned = fA1->Rebin(2,"fx2g1Rebinned");
      if(hrebinned){
         hrebinned->Scale(1.0/2.0);
         gr = new TGraphErrors(hrebinned);
      } else {
         gr = new TGraphErrors(fA1);
      }
      //h1 = fA1; 
      //gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.04 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      gr->SetMarkerSize(1.3);
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<4){
         leg->AddEntry(gr,Form("SANE #LTQ^{2}#GT=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
         leg_sane->AddEntry(gr,Form("SANE #LTQ^{2}#GT=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
         mg->Add(gr,"p");
      }

      // -----------------------------------------------
      // xg2
      hrebinned = fA2->Rebin(2,"fx2g2Rebinned");
      if(hrebinned){
         hrebinned->Scale(1.0/2.0);
         gr = new TGraphErrors(hrebinned);
      } else {
         gr = new TGraphErrors(fA2);
      }
      //TH1 * h1 = fA2; 
      //gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.1 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      gr->SetMarkerSize(1.3);
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<4){
         mg2->Add(gr,"p");
      }



   }

   TFile * fout = new TFile(Form("data/A1A2_combined_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   fout->WriteObject(fA1combined,Form("xg1-combined-x-%d",0));//,TObject::kSingleKey); 
   fout->WriteObject(fA2combined,Form("xg2-combined-x-%d",0));//,TObject::kSingleKey); 
   //fout->WriteObject(fF1combined,Form("F1-combined-x-%d",0));//,TObject::kSingleKey); 

   // --------------------------------------------------------------------------
   // Functions
   // Function to take g2p --> x^2 g2p
   TF2 * xf = new TF2("xf","x*y",0,1,-1,1);
   TF2 * xf2 = new TF2("xf","x*y",0,1,-1,1);
   Double_t Q2 = 4.0;

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);

   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  

   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 
   Double_t yAxisMax = 0.07;
   Double_t yAxisMin =-0.07;

   TF1 * x2g1p = new TF1("x2g1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");

   TF1 * xg2pWW = new TF1("x2g2pWW", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");
   TF1 * xg2pWWA = new TF1("x2g2pWWA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");
   TF1 * xg2pWWB = new TF1("x2g2pWWB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");
   TF1 * xg2pWWC = new TF1("x2g2pWWC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatexg2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2pWW");

   x2g1p->SetParameter(0,Q2);
   xg2pWW->SetParameter(0,Q2);
   xg2pWWA->SetParameter(0,Q2);
   xg2pWWB->SetParameter(0,Q2);
   xg2pWWC->SetParameter(0,Q2);

   xg2pWW->SetLineColor(1);
   xg2pWWA->SetLineColor(3);
   xg2pWWB->SetLineColor(kOrange -1);
   xg2pWWC->SetLineColor(kViolet-2);

   Int_t width = 3;
   //xg2pWW->SetLineWidth(width);
   //xg2pWWA->SetLineWidth(width);
   //xg2pWWB->SetLineWidth(width);
   //xg2pWWC->SetLineWidth(width);

   xg2pWW->SetLineStyle(1);
   xg2pWWA->SetLineStyle(1);
   xg2pWWB->SetLineStyle(1);
   xg2pWWC->SetLineStyle(1);

   xg2pWW->SetMaximum(yAxisMax);
   xg2pWW->SetMinimum(yAxisMin);

   xg2pWW->GetXaxis()->SetTitle("x");
   xg2pWW->GetXaxis()->CenterTitle();

   // -------------------------------------------------------
   // xg1p 

   c->cd(1);
   gPad->SetGridy(true);

   TLegend *leg0 = new TLegend(0.16, 0.65, 0.35, 0.88);

   // Load in world data on A1He3 from NucDB  
   gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 
   Double_t Q2_min = 1.0;
   Double_t Q2_max = 19.0;

   NucDBBinnedVariable * Wbin   = new NucDBBinnedVariable("W","W",20.0,18.0);
   NucDBBinnedVariable * Qsqbin = new NucDBBinnedVariable("Qsquared","Q^{2}");
   Qsqbin->SetBinMinimum(1.5);
   Qsqbin->SetBinMaximum(8.5);
   Qsqbin->SetMean((1.5+8.5)/2.0);
   Qsqbin->SetAverage((1.5+8.5)/2.0);
   Qsqbin->SetTitle(Form("%.1f<Q^{2}<%.1f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));

   leg->SetHeader(Form("World data %s",Qsqbin->GetTitle()));
   leg0->SetHeader(Form("World data %s",Qsqbin->GetTitle()));


   NucDBMeasurement * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   TMultiGraph *MG = new TMultiGraph(); 
   TMultiGraph *MG_data1 = new TMultiGraph(); 
   //TList * measurementsList =  new TList();
   TList * measurementsList = manager->GetMeasurements("g1p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   //// SLAC E143
   //exp = manager->GetExperiment("SLAC E143"); 
   //if(exp) ames = exp->GetMeasurement("g1p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155
   //exp = manager->GetExperiment("SLAC E155"); 
   //if(exp) ames = exp->GetMeasurement("g1p");
   //if(ames) measurementsList->Add(ames);
   ////leg->SetFillColor(kWhite); 

   //// SLAC E155x
   //exp = manager->GetExperiment("SLAC E155x"); 
   //if(exp) ames = exp->GetMeasurement("g1p");
   //if(ames) measurementsList->Add(ames);

   //Int_t markers[] = {22,32,23,33,24,25,26,27};
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement * measg1 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;
      
      plist = measg1->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg1->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      if(!strcmp(measg1->GetExperimentName(),"SANE") ){
         continue;
      }

      if(!strcmp("CLAS",measg1->GetExperimentName())){
         //TList * ls = measg1->MergeDataPoints(2,"x");
         //measg1->AddDataPoints(ls,true);
         //std::cout << "TESTTEST"<< std::endl;
         continue;
      }
      TGraphErrors *graph        = measg1->BuildGraph("x");
      graph->Apply(xf);
      graph->SetMarkerStyle(gNiceMarkerStyle[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      // Set legend title 
      Title = Form("%s",measg1->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      leg0->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph,"p"); 
      MG_data1->Add(graph,"p"); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }
   //TGraphErrors *graph        = measg1_saneQ2->BuildGraph("x");
   //graph->Apply(xf);
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   //leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   //MG->Add(graph,"p");
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("xg_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, Combined Beam Energies",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(0.0,0.1); 

   // Models
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   //TGraph * gr_x2g1p  = new TGraph( x2g1p->DrawCopy("goff")->GetHistogram());
   //gr_x2g1p->Apply(f2);
   //gr_x2g1p->Draw("l");
   //MG->Add(gr_x2g1p,"l");

   MG->Draw("AP");
   leg->Draw();


   // -------------------------------------------------------
   // xg2p 

   c->cd(2);
   TLegend *leg2 = new TLegend(0.84, 0.15, 0.99, 0.85);
   TLegend *leg02 = new TLegend(0.16, 0.65, 0.35, 0.88);
   gPad->SetGridy(true);
   TMultiGraph *MG2 = new TMultiGraph(); 
   TMultiGraph *MG2_data2 = new TMultiGraph(); 

   NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   measurementsList->Clear();
   measurementsList = manager->GetMeasurements("g2p");
   // SLAC E143
   //exp = manager->GetExperiment("SLAC E143"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155
   //exp = manager->GetExperiment("SLAC E155"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155x
   //exp = manager->GetExperiment("SLAC E155x"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *measg2 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;

      plist = measg2->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg2->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      if(!strcmp(measg2->GetExperimentName(),"SANE") ){
         continue;
      }
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg2->BuildGraph("x");
      graph->Apply(xf2);
      //graph->SetMarkerStyle(27);
      graph->SetMarkerStyle(gNiceMarkerStyle[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      // Set legend title 
      Title = Form("%s",measg2->GetExperimentName()); 
      leg2->AddEntry(graph,Title,"lp");
      leg02->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 

      MG2_data2->Add(graph,"p"); 
   }

   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary xg_{2}^{p}, Combined Beam Energies");
   //TString GTitle      = Form("Preliminary %s, Combined Beam Energies",Measurement.Data());
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   MG2->GetYaxis()->SetRangeUser(-0.15,0.15); 
   MG2->GetYaxis()->SetTitle("xg_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();

   // Plot models
   //TGraph * gr_x2g2p = new TGraph( xg2pWW->DrawCopy("goff")->GetHistogram());
   //gr_x2g2p->Apply(f3);
   ////gr_x2g2p->Draw("l");
   //MG2->Add(gr_x2g2p,"l");

   MG2->Draw("a");
   leg2->Draw();
   c->Update();

   c->SaveAs(Form("results/asymmetries/xg1g2_combined_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/xg1g2_combined_%d.png",aNumber));

   // ---------------------------------------
   // Models
   TLegend * legmod = new TLegend(0.37,0.55,0.60,0.88);
   legmod->SetHeader(Form("Models at Q^{2}=%.1f",Q2));
   legmod->SetFillColor(0);
   legmod->SetFillStyle(0);
   legmod->SetBorderSize(0);

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   TList * list_of_models = new TList();
   TList * list_of_models2 = new TList();
   TF1 * aModel = 0;


   // BBS
   fman->CreateSFs(5);
   InSANEStructureFunctions * BBSsf = fman->GetStructureFunctions();
   fman->CreatePolSFs(5);
   InSANEPolarizedStructureFunctions * BBSpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1pbbs", BBSpsf, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BBS","l");
   aModel = new TF1("xg2pbbs", BBSpsf, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models2->Add(aModel);

   // statistical
   fman->CreateSFs(13);
   InSANEStructureFunctions * STATsf = fman->GetStructureFunctions();
   fman->CreatePolSFs(13);
   InSANEPolarizedStructureFunctions * STATpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1p", STATpsf, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"Stat 2015","l");
   aModel = new TF1("xg2p", STATpsf, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models2->Add(aModel);

   // BB 
   fman->CreatePolSFs(3);
   InSANEPolarizedStructureFunctions * BBpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1pBB", BBpsf, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BB","l");
   aModel = new TF1("xg2pBB", BBpsf, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   list_of_models2->Add(aModel);


   // AAC
   fman->CreatePolSFs(2);
   InSANEPolarizedStructureFunctions * AACpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1paac", AACpsf, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"AAC","l");
   aModel = new TF1("xg2paac", AACpsf, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   list_of_models2->Add(aModel);

   // LSS
   fman->CreatePolSFs(1);
   InSANEPolarizedStructureFunctions * LSSpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1plss", LSSpsf, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"LSS2006","l");
   aModel = new TF1("xg2plss", LSSpsf, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models2->Add(aModel);

   // DSSV
   fman->CreatePolSFs(0);
   InSANEPolarizedStructureFunctions * DSSVpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1plssdssv", DSSVpsf, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"DSSV","l");
   aModel = new TF1("xg2plssdssv", DSSVpsf, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models2->Add(aModel);

   TMultiGraph * mg_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      //aModel = (TF1*)list_of_models->At(i);
      //aModel->Draw("same");
      aModel = (TF1*)list_of_models->At(i);
      TGraph * agr = new TGraph(aModel->DrawCopy("goff")->GetHistogram());
      mg_models->Add(agr,"l");
   }

   TMultiGraph * mg_models2 = new TMultiGraph();
   for(int i=0; i<list_of_models2->GetEntries();i++){
      aModel = (TF1*)list_of_models2->At(i);
      TGraph * agr = new TGraph(aModel->DrawCopy("goff")->GetHistogram());
      mg_models2->Add(agr,"l");
   }





   //---------------------------------------------------------------
   // Nicer individual plots

   TCanvas * c2 = new TCanvas("square","square");
   c2->cd(0);

   leg0->SetFillColor(0);
   leg0->SetFillStyle(0);
   leg0->SetBorderSize(0);
   leg_sane->SetFillColor(0);
   leg_sane->SetFillStyle(0);
   leg_sane->SetBorderSize(0);

   MG_data1->Draw("a");
   //MG_data1->SetTitle(GTitle);
   //MG_data1->GetXaxis()->SetTitle(xAxisTitle);
   MG_data1->GetXaxis()->CenterTitle();
   //MG_data1->GetYaxis()->SetTitle(yAxisTitle);
   MG_data1->GetYaxis()->CenterTitle();
   MG_data1->GetXaxis()->SetLimits(0.0,1.0); 
   MG_data1->GetYaxis()->SetRangeUser(-0.02,0.22); 

   leg0->Draw();

   c2->SaveAs(Form("results/asymmetries/xg1_existing_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/xg1_existing_%d.pdf",aNumber));

   // --------------
   // with models
   MG_data1->Add(mg_models);
   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/xg1_existing_models_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/xg1_existing_models_%d.pdf",aNumber));


   // --------------
   // with SANE data
   MG_data1->Add(mg);
   TLatex l;
   l.SetTextSize(0.03);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(20);
   TColor * col = new TColor(6666,0.0,0.0,0.0,"",0.5);
   l.SetTextColor(6666);
   l.SetTextSize(0.2);
   l.SetTextAngle(25);
   //l.DrawLatex(0.1,0.03,"Preliminary");
   leg_sane->Draw();

   c2->Update();

   c2->SaveAs(Form("results/asymmetries/xg1_combined_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/xg1_combined_%d.pdf",aNumber));


   // --------------------------------------
   // xg1 converted to W

   // Q2 bin for filtering data
   NucDBBinnedVariable Q2Filter("Qsquared","Q^{2}");
   Q2Filter.SetBinMinimum(0.0);
   Q2Filter.SetBinMaximum(69.5);
   NucDBBinnedVariable WFilter("W","W");
   WFilter.SetBinMinimum(1.0);
   WFilter.SetBinMaximum(200.0);
   TLegend *leg1 = new TLegend(0.84, 0.15, 0.99, 0.85);

   TCanvas * c1 = new TCanvas();
   TMultiGraph *MG1 = new TMultiGraph(); 
   measurementsList = manager->GetMeasurements("g1p");
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1World = (NucDBMeasurement*)measurementsList->At(i);
      A1World->Multiply("x");
      //A1World->Multiply("x");
      A1World->ApplyFilterWithBin(&Q2Filter);
      A1World->ApplyFilterWithBin(&WFilter);
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1World->BuildGraph("W");
      graph->SetMarkerSize(1.2);
      graph->SetMarkerStyle(27+i);
      //graph->Apply(xf);
      // Set legend title 
      Title = Form("%s",A1World->GetExperimentName()); 
      leg1->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG1->Add(graph); 
   }

   MG1->Add((TGraphErrors*)fAsymmetries_W->At(4),"lp");
   // Draw everything 
   MG1->Draw("AP");
   MG1->SetTitle(GTitle);
   MG1->GetXaxis()->SetTitle("W");
   MG1->GetXaxis()->CenterTitle();
   MG1->GetYaxis()->SetTitle(yAxisTitle);
   MG1->GetYaxis()->CenterTitle();
   MG1->GetXaxis()->SetLimits(1.0,10.0); 
   MG1->GetYaxis()->SetRangeUser(0.0,0.15); 
   MG1->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   leg1->Draw();

   c1->SaveAs(Form("results/asymmetries/g1_vsW_combined_%d.pdf",aNumber));
   c1->SaveAs(Form("results/asymmetries/g1_vsW_combined_%d.png",aNumber));

   // ---------------------------------------------
   //
   leg02->SetFillColor(0);
   leg02->SetFillStyle(0);
   leg02->SetBorderSize(0);
   c2 = new TCanvas();
   TMultiGraph * MG_temp = new TMultiGraph();

   MG_temp->Add(MG2_data2);

   MG_temp->Draw("a");
   MG_temp->GetXaxis()->SetTitle("x");
   MG_temp->GetXaxis()->CenterTitle();
   MG_temp->GetYaxis()->SetTitle("");
   MG_temp->GetYaxis()->CenterTitle();
   MG_temp->GetXaxis()->SetLimits(0.0,1.0); 
   MG_temp->GetYaxis()->SetRangeUser(-0.15,0.25); 

   leg02->Draw();

   c2->SaveAs(Form("results/asymmetries/xg2_existing_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/xg2_existing_%d.pdf",aNumber));

   // ---------------------------------------------
   //
   MG_temp->Add(mg_models2);
   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/xg2_existing_models_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/xg2_existing_models_%d.pdf",aNumber));


   // ---------------------------------------------
   //
   MG_temp->Add(mg2);
   leg_sane->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/xg2_combined_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/xg2_combined_%d.pdf",aNumber));

   return(0);
}
