#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

//int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

Int_t x2g1g2_combined(Int_t paraFileVersion = 6009,Int_t perpFileVersion = 6009, 
                    Int_t para47Version = 6009, Int_t perp47Version = 6009,Int_t aNumber=6009){
   sane_data_bins();

   //TFile * fout = new TFile(Form("data/g1g2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   //fout->WriteObject(fAllg1Asym,Form("g1asym-combined-x-%d",0));//,TObject::kSingleKey); 
   //fout->WriteObject(fAllg2Asym,Form("g2asym-combined-x-%d",0));//,TObject::kSingleKey); 

   // 5.9 GeV asymmetries
   TFile * f = new TFile(Form("data/A1A2_59_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();
   //f->ls();
   TList * fA1Asymmetries59 = (TList*)gROOT->FindObject(Form("x2g1-59-x-%d",0));
   if(!fA1Asymmetries59) return(-1);
   TList * fA2Asymmetries59 = (TList*)gROOT->FindObject(Form("x2g2-59-x-%d",0));
   if(!fA2Asymmetries59) return(-2);
   TList * fF1_SFs59 = (TList*)gROOT->FindObject(Form("d2-59-x-%d",0));
   if(!fF1_SFs59) return(-21);

   // Asymmetries classes
   TList * fA159Asym_list = (TList*)gROOT->FindObject(Form("A1asym-59-x-%d",0));
   if(!fA159Asym_list) return(-1);
   TList * fA259Asym_list = (TList*)gROOT->FindObject(Form("A2asym-59-x-%d",0));
   if(!fA259Asym_list) return(-2);

   // 4.7 GeV asymmetries
   TFile * f47 = new TFile(Form("data/A1A2_47_noinel_%d_%d.root",para47Version,perp47Version),"UPDATE");
   f47->cd();
   TList * fA1Asymmetries47 = (TList*)gROOT->FindObject(Form("x2g1-47-x-%d",0));
   if(!fA1Asymmetries47) return(-3);
   TList * fA2Asymmetries47 = (TList*)gROOT->FindObject(Form("x2g2-47-x-%d",0));
   if(!fA2Asymmetries47) return(-4);
   TList * fF1_SFs47 = (TList*)gROOT->FindObject(Form("d2-47-x-%d",0));
   if(!fF1_SFs47) return(-22);

   // Asymmetries classes
   TList * fA147Asym_list = (TList*)gROOT->FindObject(Form("A1asym-47-x-%d",0));
   if(!fA147Asym_list) return(-8);
   TList * fA247Asym_list = (TList*)gROOT->FindObject(Form("A2asym-47-x-%d",0));
   if(!fA247Asym_list) return(-9);

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("inelastic-subtracted-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-3);
   ////fParaAsymmetries->Print();

   //const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   //TFile * f2 = TFile::Open(perpfile,"UPDATE");
   //f2->cd();
   //TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp59-%d",0));
   //if(!fPerpAsymmetries) return(-4);
   ////fPerpAsymmetries->Print();

   TList * fA1combined = new TList();
   TList * fA2combined = new TList();
   TList * fF1combined = new TList();

   TH1F * fA1_59 = 0;
   TH1F * fA2_59 = 0;
   TH1F * fF1_59 = 0;
   TH1F * fA1_47 = 0;
   TH1F * fA2_47 = 0;
   TH1F * fF1_47 = 0;
   TH1F * fA1    = 0;
   TH1F * fA2    = 0;
   TH1F * fF1    = 0;

   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   /// The asymmetry is calculated A = C1(A180*C2+A80*C3)
   TCanvas * c = new TCanvas("A1A2_combined","A1 A2 combined");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();
   TMultiGraph * mg4 = new TMultiGraph();

   TMultiGraph * mg_x2g1 = new TMultiGraph();
   TMultiGraph * mg_x2g2 = new TMultiGraph();
   TF2 * div2 = new TF2("xf","y/2.0",0,1,-1,1);
   TF2 * div3 = new TF2("xf","y/3.0",0,1,-1,1);

   // Graphs converted to W
   TMultiGraph * mg1_W = new TMultiGraph();
   TMultiGraph * mg2_W = new TMultiGraph();

   // Graphs converted to W
   TMultiGraph * mg1_E = new TMultiGraph();
   TMultiGraph * mg2_E = new TMultiGraph();

   // theta vs E 
   TMultiGraph * mg1_thetaVsE = new TMultiGraph();
   TMultiGraph * mg2_thetaVsE = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);
   TLegend *leg_sane = new TLegend(0.65, 0.65, 0.89, 0.89);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries59->GetEntries();jj++) {

      fA1_59 = (TH1F*) fA1Asymmetries59->At(jj);
      fA2_59 = (TH1F*) fA2Asymmetries59->At(jj);
      fF1_59 = (TH1F*) fF1_SFs59->At(jj);
      fA1_47 = (TH1F*) fA1Asymmetries47->At(jj);
      fA2_47 = (TH1F*) fA2Asymmetries47->At(jj);
      fF1_47 = (TH1F*) fF1_SFs47->At(jj);

      fA1 = new TH1F(*fA1_59);
      fA1->Reset();
      fA1combined->Add(fA1);
      fA2 = new TH1F(*fA2_59);
      fA2->Reset();
      fA2combined->Add(fA2);
      fF1 = new TH1F(*fF1_59);
      fF1->Reset();
      fF1combined->Add(fF1);

      // Graphs vs W
      TGraphErrors * fx2g1_W    = new TGraphErrors(fA1->GetNbinsX());
      TGraphErrors * fx2g2_W    = new TGraphErrors(fA1->GetNbinsX());
      if( jj<5 ) {
         mg1_W->Add(fx2g1_W,"p");
         mg2_W->Add(fx2g2_W,"p");
      }

      // Graphs vs E
      TGraphErrors * fx2g1_E    = new TGraphErrors(fA1->GetNbinsX());
      TGraphErrors * fx2g2_E    = new TGraphErrors(fA1->GetNbinsX());
      if( jj<5 ) {
         mg1_E->Add(fx2g1_E,"p");
         mg2_E->Add(fx2g2_E,"p");
      }

      // theta vs E
      TGraphErrors * ftheta_E    = new TGraphErrors(fA1->GetNbinsX());
      TGraphErrors * ftheta_W    = new TGraphErrors(fA1->GetNbinsX());
      if( jj<5 ) {
         mg1_thetaVsE->Add(ftheta_E,"p");
         mg2_thetaVsE->Add(ftheta_W,"p");
      }

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();
      //InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsx;
      //InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsx; 

      InSANEMeasuredAsymmetry  * fA1Full_59 = (InSANEMeasuredAsymmetry * )(fA159Asym_list->At(jj));
      InSANEMeasuredAsymmetry  * fA1Full_47 = (InSANEMeasuredAsymmetry * )(fA147Asym_list->At(jj));

      // Combine the kinematics 
      InSANEAveragedKinematics1D * kine59 = &fA1Full_59->fAvgKineVsx;
      InSANEAveragedKinematics1D * kine47 = &fA1Full_47->fAvgKineVsx; 
      InSANEAveragedKinematics1D avgKine = (*kine59);
      avgKine                  += (*kine47) ;

      Int_t   xBinmax = fA1_59->GetNbinsX();
      Int_t   yBinmax = fA1_59->GetNbinsY();
      Int_t   zBinmax = fA1_59->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      //std::cout << " Graph " << jj << std::endl;
      // ----------------------------------------------------------
      // Sanitize the y values by removing NaNs
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1_59->GetBin(i,j,k);

               Double_t A1_59   = fA1_59->GetBinContent(bin);
               Double_t A2_59   = fA2_59->GetBinContent(bin);
               Double_t A1_47   = fA1_47->GetBinContent(bin);
               Double_t A2_47   = fA2_47->GetBinContent(bin);

               if( TMath::IsNaN(A1_59)  || std::isinf(A1_59) ) fA1_59->SetBinContent(bin,0.0);
               if( TMath::IsNaN(A2_59)  || std::isinf(A2_59) ) fA2_59->SetBinContent(bin,0.0);
               if( TMath::IsNaN(A1_47)  || std::isinf(A1_47) ) fA1_47->SetBinContent(bin,0.0);
               if( TMath::IsNaN(A2_47)  || std::isinf(A2_47) ) fA2_47->SetBinContent(bin,0.0);
            }
         }
      }
      // Create graph with zeros removed
      TGraph * gr_mean      = 0;
      gr_mean               = new TGraph(fA1_59);
      for( int j = gr_mean->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr_mean->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr_mean->RemovePoint(j);
            continue;
         }
      }
      Double_t A1_59_mean = gr_mean->GetMean(2);
      Double_t A1_59_rms  = gr_mean->GetRMS(2);
      //std::cout << "A1_59_mean = " << A1_59_mean << std::endl;
      //std::cout << "A1_59_rms  = " << A1_59_rms << std::endl;

      gr_mean               = new TGraph(fA2_59);
      for( int j = gr_mean->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr_mean->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr_mean->RemovePoint(j);
            continue;
         }
      }
      Double_t A2_59_mean = gr_mean->GetMean(2);
      Double_t A2_59_rms  = gr_mean->GetRMS(2);
      //std::cout << "A2_59_mean = " << A2_59_mean << std::endl;

      gr_mean               = new TGraph(fA1_47);
      for( int j = gr_mean->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr_mean->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr_mean->RemovePoint(j);
            continue;
         }
      }
      Double_t A1_47_mean = gr_mean->GetMean(2);
      Double_t A1_47_rms  = gr_mean->GetRMS(2);
      //std::cout << "A1_47_mean = " << A1_47_mean << std::endl;

      gr_mean               = new TGraph(fA2_47);
      for( int j = gr_mean->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr_mean->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr_mean->RemovePoint(j);
            continue;
         }
      }
      Double_t A2_47_mean = gr_mean->GetMean(2);
      Double_t A2_47_rms  = gr_mean->GetRMS(2);
      //std::cout << "A2_47_mean = " << A2_47_mean << std::endl;

      Int_t iW = 0;
      // -----------------------------------------------------------
      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1_59->GetBin(i,j,k);

               bool only47  = false;
               bool only59  = false;

               Double_t A1_59   = fA1_59->GetBinContent(bin);
               Double_t A2_59   = fA2_59->GetBinContent(bin);
               Double_t eA1_59  = fA1_59->GetBinError(bin);
               Double_t eA2_59  = fA2_59->GetBinError(bin);

               Double_t x       = avgKine.fx.GetBinContent(bin);
               Double_t W       = avgKine.fW.GetBinContent(bin);
               Double_t Energy  = avgKine.fE.GetBinContent(bin);
               Double_t theta   = avgKine.fTheta.GetBinContent(bin)/degree;

               Double_t A1_47   = fA1_47->GetBinContent(bin);
               Double_t A2_47   = fA2_47->GetBinContent(bin);
               Double_t eA1_47  = fA1_47->GetBinError(bin);
               Double_t eA2_47  = fA2_47->GetBinError(bin);

               Double_t A1  = 0.0; 
               Double_t eA1 = 0.0; 
               Double_t A2  = 0.0; 
               Double_t eA2 = 0.0; 


               Double_t F1_47   = fF1_47->GetBinContent(bin);
               Double_t F1_59   = fF1_59->GetBinContent(bin);
               Double_t eF1_47   = fF1_47->GetBinError(bin);
               Double_t eF1_59   = fF1_59->GetBinError(bin);
               Double_t F1  = 0.0; 
               Double_t eF1  = 0.0; 

               Double_t x2g1ErrorMax = 0.20;
               // Check the two results
               if( eA1_47 == 0.0 || TMath::IsNaN(eA1_47) ) {
                  only59 = true;
               }
               if( eA1_59 == 0.0 || TMath::IsNaN(eA1_59) ) {
                  only47 = true;
               }
               if( eA2_47 == 0.0 || TMath::IsNaN(eA2_47) ) {
                  only59 = true;
               }
               if( eA2_59 == 0.0 || TMath::IsNaN(eA2_59) ) {
                  only47 = true;
               }
               //if( 2.0*x*x*eA1_47 > x2g1ErrorMax  ) {
               //   only59 = true;
               //}
               //if( 2.0*x*x*eA1_59 > x2g1ErrorMax  ) {
               //   only47 = true;
               //}
               //if( 2.0*x*x*eA1_47 > x2g1ErrorMax  ) {
               //   only59 = true;
               //}
               //if( 2.0*x*x*eA1_59 > x2g1ErrorMax  ) {
               //   only47 = true;
               //}
               //if( TMath::Abs(2.0*x*x*(A1_47 - A1_47_mean))  > 1.5*2.0*x*x*A1_47_rms  ) {
               //   only59 = true;
               //}
               //if( TMath::Abs(2.0*x*x*(A1_59 - A1_59_mean))  > 1.5*2.0*x*x*A1_59_rms  ) {
               //   only47 = true;
               //}
               //if( TMath::Abs(3.0*x*x*(A2_47 - A2_47_mean))  > 1.0*3.0*x*x*A2_47_rms  ) {
               //   only59 = true;
               //}
               //if( TMath::Abs(3.0*x*x*(A2_59 - A2_59_mean))  > 1.0*3.0*x*x*A2_59_rms  ) {
               //   only47 = true;
               //}

               if( only59 && !only47 ) {
                  A1  = A1_59;
                  eA1 = eA1_59;
                  A2  = A2_59;
                  eA2 = eA2_59;
               } else if( only47 && !only59 ) {
                  A1  = A1_47;
                  eA1 = eA1_47;
                  A2  = A2_47;
                  eA2 = eA2_47;
               }
                 
               if( (!only47) && (!only59)  ) {
                  A1 = (A1_59/(eA1_59*eA1_59) + A1_47/(eA1_47*eA1_47))/(1.0/(eA1_59*eA1_59) + 1.0/(eA1_47*eA1_47));
                  eA1 = TMath::Sqrt((1.0)/(1.0/(eA1_59*eA1_59) + 1.0/(eA1_47*eA1_47)));

                  A2 = (A2_59/(eA2_59*eA2_59) + A2_47/(eA2_47*eA2_47))/(1.0/(eA2_59*eA2_59) + 1.0/(eA2_47*eA2_47));
                  eA2 = TMath::Sqrt((1.0)/(1.0/(eA2_59*eA2_59) + 1.0/(eA2_47*eA2_47)));
               }

               if( eF1_47 == 0.0 || TMath::IsNaN(eF1_47) ) {
                  F1  = F1_59;
                  eF1 = eF1_59;
               }
               if( eF1_59 == 0.0 || TMath::IsNaN(eF1_59) ) {
                  F1  = F1_47;
                  eF1 = eF1_47;
               }
               if( eF1_47 != 0.0 && eF1_59 != 0.0 && !(TMath::IsNaN(eF1_47)) && !(TMath::IsNaN(eF1_59)) ) {

                  F1 = (F1_59/(eF1_59*eF1_59) + F1_47/(eF1_47*eF1_47))/(1.0/(eF1_59*eF1_59) + 1.0/(eF1_47*eF1_47));
                  eF1 = TMath::Sqrt((1.0)/(1.0/(eF1_59*eF1_59) + 1.0/(eF1_47*eF1_47)));
               }

               //if( eA2_47 == 0.0 || TMath::IsNaN(eA2_47) ) {
               //   A2  = A2_59;
               //   eA2 = eA2_59;
               //}
               //if( eA2_59 == 0.0 || TMath::IsNaN(eA2_59) ) {
               //   A2  = A2_47;
               //   eA2 = eA2_47;
               //}
               //if( eA2_47 != 0.0 && eA2_59 != 0.0 && !(TMath::IsNaN(eA2_47)) && !(TMath::IsNaN(eA2_59)) ) {

               //}
               //std::cout << "-----------------------------------" << std::endl;
               //std::cout << F1_47 << " +- " << eF1_47 << std::endl;
               //std::cout << F1_59 << " +- " << eF1_59 << std::endl;
               //std::cout << F1 << " +- " << eF1 << std::endl;

               fA1->SetBinContent(bin,A1);
               fA2->SetBinContent(bin,A2);
               fA1->SetBinError(bin, eA1);
               fA2->SetBinError(bin, eA2);
               fF1->SetBinContent(bin,F1);
               fF1->SetBinError(bin, eF1);

               fx2g1_W->SetPoint(iW,W,A1);
               fx2g1_W->SetPointError(iW,0.0,eA1);
               fx2g2_W->SetPoint(iW,W,A2);
               fx2g2_W->SetPointError(iW,0.0,eA2);

               fx2g1_E->SetPoint(     iW,Energy,A1);
               fx2g1_E->SetPointError(iW,0.0,eA1);
               fx2g2_E->SetPoint(     iW,Energy,A2);
               fx2g2_E->SetPointError(iW,0.0,eA2);

               ftheta_E->SetPoint(     iW,Energy,theta);
               ftheta_E->SetPointError(iW,0.0,0.0);
               ftheta_W->SetPoint(     iW,W,theta);
               ftheta_W->SetPointError(iW,0.0,0.0);

               iW++;
            }
         }
      }

      // --------------------------------------------------------
      // Style for plots vs W
      fx2g1_W->SetMarkerStyle(20);
      fx2g1_W->SetMarkerSize(0.8);
      fx2g1_W->SetMarkerColor(fA1->GetMarkerColor());
      fx2g1_W->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         fx2g1_W->SetMarkerSize(0.7);
         fx2g1_W->SetMarkerStyle(20);
         fx2g1_W->SetMarkerColor(4);
         fx2g1_W->SetLineColor(4);
      }

      fx2g2_W->SetMarkerStyle(20);
      fx2g2_W->SetMarkerSize(0.8);
      fx2g2_W->SetMarkerColor(fA1->GetMarkerColor());
      fx2g2_W->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         fx2g2_W->SetMarkerSize(0.7);
         fx2g2_W->SetMarkerStyle(20);
         fx2g2_W->SetMarkerColor(4);
         fx2g2_W->SetLineColor(4);
      }

      // --------------------------------------------------------
      // Style for plots vs E
      fx2g1_E->SetMarkerStyle(20);
      fx2g1_E->SetMarkerSize(0.8);
      fx2g1_E->SetMarkerColor(fA1->GetMarkerColor());
      fx2g1_E->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         fx2g1_E->SetMarkerSize(0.7);
         fx2g1_E->SetMarkerStyle(20);
         fx2g1_E->SetMarkerColor(4);
         fx2g1_E->SetLineColor(4);
      }

      fx2g2_E->SetMarkerStyle(20);
      fx2g2_E->SetMarkerSize(0.8);
      fx2g2_E->SetMarkerColor(fA1->GetMarkerColor());
      fx2g2_E->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         fx2g2_E->SetMarkerSize(0.7);
         fx2g2_E->SetMarkerStyle(20);
         fx2g2_E->SetMarkerColor(4);
         fx2g2_E->SetLineColor(4);
      }

      // --------------------------------------------------------
      // Style for plots theta vs E
      ftheta_E->SetMarkerStyle(20);
      ftheta_E->SetMarkerSize(0.8);
      ftheta_E->SetMarkerColor(fA1->GetMarkerColor());
      ftheta_E->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         ftheta_E->SetMarkerSize(0.7);
         ftheta_E->SetMarkerStyle(20);
         ftheta_E->SetMarkerColor(4);
         ftheta_E->SetLineColor(4);
      }

      ftheta_W->SetMarkerStyle(20);
      ftheta_W->SetMarkerSize(0.8);
      ftheta_W->SetMarkerColor(fA1->GetMarkerColor());
      ftheta_W->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         ftheta_W->SetMarkerSize(0.7);
         ftheta_W->SetMarkerStyle(20);
         ftheta_W->SetMarkerColor(4);
         ftheta_W->SetLineColor(4);
      }

      // --------------------------------------------------------
      //
      gr = new TGraphErrors(fA2);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.06 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(20);
         gr->SetMarkerColor(4);
         gr->SetLineColor(4);
      }
      if(jj<4){
      //if(jj==4){
         TGraphErrors * grtemp = new TGraphErrors(*gr);
         grtemp->Apply(div3);
         mg_x2g2->Add(grtemp,"p");

         mg2->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
         leg_sane->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }


      // --------------------------------------------------------
      //
      gr = new TGraphErrors(fA1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.06 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<4){
         TGraphErrors * grtemp = new TGraphErrors(*gr);
         grtemp->Apply(div2);
         mg_x2g1->Add(grtemp,"p");

         mg->Add(gr,"p");
      }


      // --------------------------------------------------------
      //
      gr = new TGraphErrors(fF1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.1 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());

      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
         mg3->Add(gr,"l");
         mg4->Add(gr,"l");
      } else {
         gr->SetMarkerStyle(20);
         if(jj<=4) {
            mg3->Add(gr,"p");
         }
      }

   }

   TFile * fout = new TFile(Form("data/g1g2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   fout->WriteObject(fA1combined,Form("x2g1-combined-x-%d",0));//,TObject::kSingleKey); 
   fout->WriteObject(fA2combined,Form("x2g2-combined-x-%d",0));//,TObject::kSingleKey); 
   //fout->WriteObject(fF1combined,Form("F1-combined-x-%d",0));//,TObject::kSingleKey); 
   fout->Close();

   // --------------------------------------------------------------------------
   // Functions

   // Function to take g2p --> x^2 g2p
   TF2 * xf = new TF2("xf","2.0*x*x*y",0,1,-1,1);
   TF2 * xf2 = new TF2("xf2","3.0*x*x*y",0,1,-1,1);
   TF2 * f2 = new TF2("f2","2.0*y",0,1,-1,1);
   TF2 * f3 = new TF2("f3","3.0*y",0,1,-1,1);

   TF2 * fTimes2 = new TF2("ftimes2","2.0*y",0,1,-1,1);
   TF2 * fTimes3 = new TF2("ftimes3","3.0*y",0,1,-1,1);
   TF2 * x2f = new TF2("x2f","x*x*y",0,1,-1,1);

   Double_t Q2 = 4.0;

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);

   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  

   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 
   Double_t yAxisMax = 0.07;
   Double_t yAxisMin =-0.07;

   TF1 * x2g1p = new TF1("x2g1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");

   TF1 * xg2pWW = new TF1("x2g2pWW", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWA = new TF1("x2g2pWWA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWB = new TF1("x2g2pWWB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWC = new TF1("x2g2pWWC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");

   x2g1p->SetParameter(0,Q2);
   xg2pWW->SetParameter(0,Q2);
   xg2pWWA->SetParameter(0,Q2);
   xg2pWWB->SetParameter(0,Q2);
   xg2pWWC->SetParameter(0,Q2);

   xg2pWW->SetLineColor(1);
   xg2pWWA->SetLineColor(3);
   xg2pWWB->SetLineColor(kOrange -1);
   xg2pWWC->SetLineColor(kViolet-2);

   Int_t width = 3;
   //xg2pWW->SetLineWidth(width);
   //xg2pWWA->SetLineWidth(width);
   //xg2pWWB->SetLineWidth(width);
   //xg2pWWC->SetLineWidth(width);

   xg2pWW->SetLineStyle(1);
   xg2pWWA->SetLineStyle(1);
   xg2pWWB->SetLineStyle(1);
   xg2pWWC->SetLineStyle(1);

   xg2pWW->SetMaximum(yAxisMax);
   xg2pWW->SetMinimum(yAxisMin);

   xg2pWW->GetXaxis()->SetTitle("x");
   xg2pWW->GetXaxis()->CenterTitle();

   // -------------------------------------------------------
   // x2g1p 

   c->cd(1);
   gPad->SetGridy(true);
   TLegend *leg0 = new TLegend(0.16, 0.65, 0.4, 0.88);

   // Load in world data on A1He3 from NucDB  
   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Wbin   = new NucDBBinnedVariable("W","W",20.0,18.0);
   NucDBBinnedVariable * Qsqbin = new NucDBBinnedVariable("Qsquared","Q^{2}");
   Qsqbin->SetBinMinimum(1.5);
   Qsqbin->SetBinMaximum(8.5);
   Qsqbin->SetMean((1.5+8.5)/2.0);
   Qsqbin->SetAverage((1.5+8.5)/2.0);

   NucDBMeasurement * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   TMultiGraph *MG = new TMultiGraph(); 
   TMultiGraph *MG_data1 = new TMultiGraph(); 
   //TList * measurementsList =  new TList(); //manager->GetMeasurements("A1p");
   TList * measurementsList =  manager->GetMeasurements("g1p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   Int_t markers[] = {22,32,23,33,21,25,26,27};
   for (int i = 0; i < measurementsList->GetEntries(); i++) {

      NucDBMeasurement * measg1 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;

      plist = measg1->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg1->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      if(!strcmp(measg1->GetExperimentName(),"SANE") ){
         continue;
      }
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg1->BuildGraph("x");
      graph->Apply(x2f);
      graph->SetMarkerStyle(gNiceMarkerStyle[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      // Set legend title 
      leg->AddEntry(graph,measg1->GetExperimentName(),"lp");
      leg0->AddEntry(graph,measg1->GetExperimentName(),"lp");

      // Add to TMultiGraph 
      MG_data1->Add(graph,"p"); 

      TGraphErrors * graph2 = new TGraphErrors(*graph);
      graph2->Apply(fTimes2);
      MG->Add(graph2,"p"); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }
   //TGraphErrors *graph        = measg1_saneQ2->BuildGraph("x");
   //graph->Apply(xf);
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   //leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   //MG->Add(graph,"p");
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");


   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle("");
   MG->GetXaxis()->SetTitle("x");
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle("2x^{2}g_{1}^{p}");
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.03,0.1); 

   // Models
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   TGraph * gr_x2g1p  = new TGraph( x2g1p->DrawCopy("goff")->GetHistogram());
   gr_x2g1p->Apply(f2);
   //gr_x2g1p->Draw("l");
   MG->Add(gr_x2g1p,"l");

   MG->Draw("AP");
   leg->Draw();


   // -------------------------------------------------------
   // x2g2p 

   c->cd(2);
   TLegend *leg2 = new TLegend(0.84, 0.15, 0.99, 0.85);
   TLegend *leg02 = new TLegend(0.31, 0.65, 0.55, 0.88);
   gPad->SetGridy(true);
   TMultiGraph *MG2 = new TMultiGraph(); 

   NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   measurementsList->Clear();

   TMultiGraph *MG_data2 = new TMultiGraph(); 
   //TList * measurementsList =  new TList(); //manager->GetMeasurements("A1p");
   measurementsList =  manager->GetMeasurements("g2p");

   //// SLAC E143
   //exp = manager->GetExperiment("SLAC E143"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155
   //exp = manager->GetExperiment("SLAC E155"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155x
   //exp = manager->GetExperiment("SLAC E155x"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *measg2 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = measg2->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      if(!strcmp(measg2->GetExperimentName(),"SANE") ){
         continue;
      }
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg2->BuildGraph("x");
      graph->Apply(x2f);
      if(i<3) {
         graph->SetMarkerStyle(markers[0]);
         graph->SetMarkerColor(2);
         graph->SetMarkerSize(1.0);
         graph->SetLineColor(2);
         if(i==0) {
            leg2->AddEntry(graph,    "SLAC","lp");
            leg02->AddEntry(graph,   "SLAC","lp");
            leg_sane->AddEntry(graph,"SLAC","lp");
         }
      } else {
         graph->SetMarkerStyle(markers[i]);
         graph->SetMarkerColor(1);
         graph->SetLineColor(1);
         if(i==3){
            graph->SetMarkerColor(6);
            graph->SetLineColor(6);
         }
         leg2->AddEntry(graph, measg2->GetExperimentName(),"lp");
         leg02->AddEntry(graph,measg2->GetExperimentName(),"lp");
         leg_sane->AddEntry(graph,measg2->GetExperimentName(),"lp");
      }
      // Set legend title 
      // Add to TMultiGraph 
      MG_data2->Add(graph,"p"); 
      TGraphErrors * graph2 = new TGraphErrors(*graph);
      graph2->Apply(fTimes2);
      //MG2->Add(graph,"p"); 
      MG2->Add(graph2,"p"); 
   }

   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary 3x^{2}g_{2}^{p}, Combined Beam Energies");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   MG2->GetYaxis()->SetRangeUser(-0.1,0.08); 
   MG2->GetYaxis()->SetTitle("3x^{2}g_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();

   // Plot models
   TGraph * gr_x2g2p = new TGraph( xg2pWW->DrawCopy("goff")->GetHistogram());
   gr_x2g2p->Apply(f3);
   //gr_x2g2p->Draw("l");
   MG2->Add(gr_x2g2p,"l");

   MG2->Draw("a");
   leg2->Draw();
   c->Update();

   c->SaveAs(Form("results/asymmetries/x2g1g2_combined_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/x2g1g2_combined_%d.png",aNumber));

   // -----------------------------------------------------------------------
   // d2p

   TCanvas * cd2 = new TCanvas();

   gPad->SetGridy(true);
   mg3->Add(gr_x2g1p,"l");
   mg3->Add(gr_x2g2p,"l");
   mg3->Draw("a");

   TH1 * h1_d2p = new TH1F( *((TH1F*)x2g1p->DrawCopy("goff")->GetHistogram()) ) ;
   h1_d2p->Reset();
   TH1 * h1_x2g1p = x2g1p->DrawCopy("goff")->GetHistogram();
   h1_x2g1p->Scale(2.0);
   TH1 * h1_x2g2p = xg2pWW->DrawCopy("goff")->GetHistogram();
   h1_x2g2p->Scale(3.0);

   h1_d2p->Add(h1_x2g1p);
   h1_d2p->Add(h1_x2g2p);

   TGraph * gr_d2p = new TGraph( h1_d2p );
   gr_d2p->SetLineWidth(3);
   mg3->Add(gr_d2p,"l");

   mg3->GetXaxis()->SetLimits(0.0,1.0); 
   mg3->GetYaxis()->SetRangeUser(-0.15,0.15); 
   mg3->Draw("a");
   cd2->Update();
   cd2->SaveAs(Form("results/asymmetries/x2g1g2_combined_d2_%d.png",aNumber));

   //---------------------------------------------------------------
   // Nicer individual plots

   TCanvas * c2 = new TCanvas("square","square");
   c2->cd(0);

   leg0->SetFillColor(0);
   leg0->SetFillStyle(0);
   leg0->SetBorderSize(0);
   leg_sane->SetFillColor(0);
   leg_sane->SetFillStyle(0);
   leg_sane->SetBorderSize(0);

   MG_data1->Draw("a");
   MG_data1->SetTitle("x^{2}g_{1}^{p}");
   MG_data1->GetXaxis()->SetTitle("x");
   MG_data1->GetXaxis()->CenterTitle();
   //MG_data1->GetYaxis()->SetTitle(yAxisTitle);
   MG_data1->GetYaxis()->CenterTitle();
   MG_data1->GetXaxis()->SetLimits(0.0,1.0); 
   MG_data1->GetYaxis()->SetRangeUser(-0.02,0.07); 

   leg0->Draw();

   c2->SaveAs(Form("results/asymmetries/x2g1_existing_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1_existing_%d.pdf",aNumber));

   //Models
   TLegend * legmod = new TLegend(0.16,0.45,0.40,0.65);
   //legmod->SetHeader(Form("Models at Q^{2}=%.1f",Q2));
   legmod->SetFillColor(0);
   legmod->SetFillStyle(0);
   legmod->SetBorderSize(0);

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   TList * list_of_models = new TList();
   TF1 * aModel = 0;


   // BBS
   fman->CreateSFs(5);
   InSANEStructureFunctions * BBSsf = fman->GetStructureFunctions();
   fman->CreatePolSFs(5);
   InSANEPolarizedStructureFunctions * BBSpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1pbbs", BBSpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BBS","l");

   // statistical
   fman->CreateSFs(6);
   InSANEStructureFunctions * STATsf = fman->GetStructureFunctions();
   fman->CreatePolSFs(6);
   InSANEPolarizedStructureFunctions * STATpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1p", STATpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"Statistical","l");

   // BB 
   fman->CreatePolSFs(3);
   InSANEPolarizedStructureFunctions * BBpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1pBB", BBpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BB","l");


   // AAC
   fman->CreatePolSFs(2);
   InSANEPolarizedStructureFunctions * AACpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1paac", AACpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"AAC","l");

   // LSS
   fman->CreatePolSFs(1);
   InSANEPolarizedStructureFunctions * LSSpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1plss", LSSpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"LSS2006","l");

   // DSSV
   fman->CreatePolSFs(0);
   InSANEPolarizedStructureFunctions * DSSVpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1plssdssv", DSSVpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"DSSV","l");

   TMultiGraph * mg_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      aModel->Draw("same");
   }
   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g1_existing_models_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1_existing_models_%d.pdf",aNumber));


   // --------------
   // with SANE data
   MG_data1->Add(mg_x2g1);
   TLatex l;
   l.SetTextSize(0.03);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(20);
   TColor * col = new TColor(6666,0.0,0.0,0.0,"",0.15);
   l.SetTextColor(6666);
   l.SetTextSize(0.2);
   l.SetTextAngle(25);
   //l.DrawLatex(0.1,0.01,"Preliminary");
   leg_sane->Draw();

   c2->Update();

   c2->SaveAs(Form("results/asymmetries/x2g1_combined_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1_combined_%d.pdf",aNumber));



   //---------------------------------------------------------------
   // x2 g2p Nicer individual plots

   c2 = new TCanvas("square","square");
   c2->cd(0);

   //leg02->SetHeader(Form("World data %s",Qsqbin->GetTitle()));
   leg02->SetFillColor(0);
   leg02->SetFillStyle(0);
   leg02->SetBorderSize(0);
   leg_sane->SetFillColor(0);
   leg_sane->SetFillStyle(0);
   leg_sane->SetBorderSize(0);

   MG_data2->Draw("a");
   //MG_data1->SetTitle(GTitle);
   MG_data2->SetTitle("x^{2}g_{2}^{p}");
   MG_data2->GetXaxis()->SetTitle("x");
   MG_data2->GetXaxis()->CenterTitle();
   //MG_data1->GetYaxis()->SetTitle(yAxisTitle);
   MG_data2->GetYaxis()->CenterTitle();
   MG_data2->GetXaxis()->SetLimits(0.0,1.0); 
   MG_data2->GetYaxis()->SetRangeUser(-0.06,0.08); 

   //leg02->Draw();

   c2->SaveAs(Form("results/asymmetries/x2g2_existing_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g2_existing_%d.pdf",aNumber));

   //Models
   legmod = new TLegend(0.51,0.65,0.68,0.88);
   //legmod->SetHeader(Form("Models at Q^{2}=%.1f",Q2));
   legmod->SetFillColor(0);
   legmod->SetFillStyle(0);
   legmod->SetBorderSize(0);

   //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   list_of_models = new TList();
   //TF1 * aModel = 0;


   // BBS
   //fman->CreateSFs(5);
   //InSANEStructureFunctions * BBSsf = fman->GetStructureFunctions();
   //fman->CreatePolSFs(5);
   //InSANEPolarizedStructureFunctions * BBSpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1pbbs", BBSpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BBS","l");

   // statistical
   //fman->CreateSFs(6);
   //InSANEStructureFunctions * STATsf = fman->GetStructureFunctions();
   //fman->CreatePolSFs(6);
   //InSANEPolarizedStructureFunctions * STATpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1p", STATpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"Statistical","l");

   // BB 
   //fman->CreatePolSFs(3);
   //InSANEPolarizedStructureFunctions * BBpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1pBB", BBpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BB","l");


   // AAC
   //fman->CreatePolSFs(2);
   //InSANEPolarizedStructureFunctions * AACpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1paac", AACpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"AAC","l");

   // LSS
   //fman->CreatePolSFs(1);
   //InSANEPolarizedStructureFunctions * LSSpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1plss", LSSpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"LSS2006","l");

   // DSSV
   //fman->CreatePolSFs(0);
   //InSANEPolarizedStructureFunctions * DSSVpsf = fman->GetPolarizedStructureFunctions();
   aModel = new TF1("xg1plssdssv", DSSVpsf, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"DSSV","l");

   mg_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      aModel->Draw("same");
   }
   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g2_existing_models_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g2_existing_models_%d.pdf",aNumber));


   // --------------
   // with SANE data
   MG_data2->Add(mg_x2g2);
   //TLatex l;
   l.SetTextSize(0.03);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(20);
   col = new TColor(6666,0.0,0.0,0.0,"",0.5);
   l.SetTextColor(6666);
   l.SetTextSize(0.2);
   l.SetTextAngle(25);
   //l.DrawLatex(0.1,-0.04,"Preliminary");
   leg_sane->Draw();

   c2->Update();

   c2->SaveAs(Form("results/asymmetries/x2g2_combined_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g2_combined_%d.pdf",aNumber));

   // -------------------------------------------

   TCanvas * c3 = new TCanvas();
   c3->Divide(1,2);

   c3->cd(1);
   mg1_W->Draw("a");
   mg1_W->SetTitle("x^{2}g_{1}^{p}");
   mg1_W->GetXaxis()->SetTitle("W");
   mg1_W->GetXaxis()->CenterTitle();
   mg1_W->GetYaxis()->CenterTitle();
   mg1_W->GetXaxis()->SetLimits(1.0,3.0); 
   mg1_W->GetYaxis()->SetRangeUser(-0.05,0.1); 
   mg1_W->Draw("a");

   c3->cd(2);
   mg2_W->Draw("a");
   mg2_W->SetTitle("x^{2}g_{2}^{p}");
   mg2_W->GetXaxis()->SetTitle("W");
   mg2_W->GetXaxis()->CenterTitle();
   mg2_W->GetYaxis()->CenterTitle();
   mg2_W->GetXaxis()->SetLimits(1.0,3.0); 
   mg2_W->GetYaxis()->SetRangeUser(-0.1,0.08); 
   mg2_W->Draw("a");


   c3->SaveAs(Form("results/asymmetries/x2g2_combined_vsW_%d.png",aNumber));
   c3->SaveAs(Form("results/asymmetries/x2g2_combined_vsW_%d.pdf",aNumber));

   // -------------------------------------------

   TCanvas * c4 = new TCanvas();
   c4->Divide(1,2);

   c4->cd(1);
   mg1_E->Draw("a");
   mg1_E->SetTitle("x^{2}g_{1}^{p}");
   mg1_E->GetXaxis()->SetTitle("E");
   mg1_E->GetXaxis()->CenterTitle();
   mg1_E->GetYaxis()->CenterTitle();
   mg1_E->GetXaxis()->SetLimits(0.7,3.0); 
   mg1_E->GetYaxis()->SetRangeUser(-0.05,0.1); 
   mg1_E->Draw("a");

   c4->cd(2);
   mg2_E->Draw("a");
   mg2_E->SetTitle("x^{2}g_{2}^{p}");
   mg2_E->GetXaxis()->SetTitle("E");
   mg2_E->GetXaxis()->CenterTitle();
   mg2_E->GetYaxis()->CenterTitle();
   mg2_E->GetXaxis()->SetLimits(0.7,3.0); 
   mg2_E->GetYaxis()->SetRangeUser(-0.1,0.08); 
   mg2_E->Draw("a");


   c4->SaveAs(Form("results/asymmetries/x2g2_combined_vsE_%d.png",aNumber));
   c4->SaveAs(Form("results/asymmetries/x2g2_combined_vsE_%d.pdf",aNumber));

   // -------------------------------------------

   TCanvas * c5 = new TCanvas();
   c5->Divide(1,2);

   c5->cd(1);
   mg1_thetaVsE->Draw("a");
   mg1_thetaVsE->SetTitle("x^{2}g_{1}^{p}");
   mg1_thetaVsE->GetXaxis()->SetTitle("E");
   mg1_thetaVsE->GetXaxis()->CenterTitle();
   mg1_thetaVsE->GetYaxis()->CenterTitle();
   mg1_thetaVsE->GetXaxis()->SetLimits(0.7,3.0); 
   mg1_thetaVsE->GetYaxis()->SetRangeUser(20.0,60.0); 
   mg1_thetaVsE->Draw("a");

   c5->cd(2);
   mg2_thetaVsE->Draw("a");
   mg2_thetaVsE->SetTitle("x^{2}g_{2}^{p}");
   mg2_thetaVsE->GetXaxis()->SetTitle("E");
   mg2_thetaVsE->GetXaxis()->CenterTitle();
   mg2_thetaVsE->GetYaxis()->CenterTitle();
   mg2_thetaVsE->GetXaxis()->SetLimits(0.7,3.0); 
   mg2_thetaVsE->GetYaxis()->SetRangeUser(20.0,60.0); 
   mg2_thetaVsE->Draw("a");


   c5->SaveAs(Form("results/asymmetries/x2g2_combined_thetaVsE_%d.png",aNumber));
   c5->SaveAs(Form("results/asymmetries/x2g2_combined_thetaVsE_%d.pdf",aNumber));

   return 0;
}


