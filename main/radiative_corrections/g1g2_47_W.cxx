#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

Int_t g1g2_47_W(Int_t paraFileVersion = 8011,Int_t perpFileVersion = 8011,Int_t aNumber=8011){

   sane_data_bins();

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   TFile * f = new TFile(Form("data/A1A2_47_noel_W_%d_%d.root",paraFileVersion,perpFileVersion),"READ");
   f->cd();

   TList * fA1Asymmetries = (TList*)gROOT->FindObject(Form("A1-47-W-%d",0));
   if(!fA1Asymmetries) return(-1);
   TList * fA2Asymmetries = (TList*)gROOT->FindObject(Form("A2-47-W-%d",0));
   if(!fA2Asymmetries) return(-2);

   const char * parafile = Form("data/bg_corrected_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"READ");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-3);
   //fParaAsymmetries->Print();

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"READ");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-4);
   //fPerpAsymmetries->Print();

   TList * fg1s = new TList();
   TList * fg2s = new TList();

   TList        * fAllg1Asym = new TList();
   TList        * fAllg2Asym = new TList();

   TH1F         * fA1 = 0;
   TH1F         * fA2 = 0;
   TH1F         * fg1 = 0;
   TH1F         * fg2 = 0;
   TGraphErrors * gr  = 0;
   TGraphErrors * gr2 = 0;

   InSANEStructureFunctions * SFs = new F1F209StructureFunctions();

   TCanvas * c = new TCanvas();
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries->GetEntries();jj++) {

      fA1 = (TH1F*) fA1Asymmetries->At(jj);
      fA2 = (TH1F*) fA2Asymmetries->At(jj);

      fg1 = new TH1F(*fA1);
      fg1->Reset();
      fg1s->Add(fg1);
      fg2 = new TH1F(*fA2);
      fg2->Reset();
      fg2s->Add(fg2);

      InSANEMeasuredAsymmetry * paraAsym = (InSANEMeasuredAsymmetry*)(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry * perpAsym = (InSANEMeasuredAsymmetry*)(fPerpAsymmetries->At(jj));

      InSANEMeasuredAsymmetry * fg1Full = new InSANEMeasuredAsymmetry( *paraAsym );
      fAllg1Asym->Add(fg1Full);
      InSANEMeasuredAsymmetry * fg2Full = new InSANEMeasuredAsymmetry( *paraAsym );
      fAllg2Asym->Add(fg2Full);

      InSANEAveragedKinematics1D * paraKine = &paraAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsW; 

      InSANEAveragedKinematics1D avgKine = (*paraKine);
      avgKine                  += (*perpKine) ;
      fg1Full->fAvgKineVsW = avgKine;
      fg2Full->fAvgKineVsW = avgKine;

      //fA1 = fg1Full->fAsymmetryVsx;//new TH1F(*fApara);
      //fA2 = fg2Full->fAsymmetryVsx;//new TH1F(*fAperp);

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin     = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);
               Double_t W1     = paraKine->fW.GetBinContent(bin);
               Double_t x1     = paraKine->fx.GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi.GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2.GetBinContent(bin);
               Double_t W     = W1;//
               Double_t x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t phi   = phi1;//0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
               Double_t Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               Double_t A1   = fA1->GetBinContent(bin);
               Double_t A2   = fA2->GetBinContent(bin);

               Double_t eA1  = fA1->GetBinError(bin);
               Double_t eA2  = fA2->GetBinError(bin);

               Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               Double_t R        = SFs->R(x,Q2);
               Double_t F1       = SFs->F1p(x,Q2);
               Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               Double_t gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
               Double_t gamma    = TMath::Sqrt(gamma2);
               Double_t cota     = 1.0/TMath::Tan(alpha);
               Double_t csca     = 1.0/TMath::Sin(alpha);

               Double_t g1 = (F1/(1.0+gamma2))*(A1 + gamma*A2);
               Double_t g2 = (F1/(1.0+gamma2))*(A2/gamma - A1);

               //eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
               Double_t a1 = (F1/(1.0+gamma2));
               Double_t a2 = (F1/(1.0+gamma2))*gamma;
               Double_t eg1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //eg2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
               a1 = -1.0*(F1/(1.0+gamma2));
               a2 = (F1/(1.0+gamma2))/gamma;
               Double_t eg2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //c0 = 1.0/(1.0 + eta*xi);
               //c11 = (1.0 + cota*chi)/D ;
               //c12 = (csca*chi)/D ;
               //c21 = (xi - cota*chi/eta)/D ;
               //c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               //if( TMath::Abs(A80) > 0.0 ) {
               //   std::cout << "(E0,Ep,theta,phi) = (" << E0 << ","
               //                                        << Ep << ","
               //                                        << theta << ","
               //                                        << phi   << ")" << std::endl;  
               //   std::cout << " delta R      = " << R - R_2 << std::endl;
               //   std::cout << " A180         = " << A180 << std::endl;
               //   std::cout << " c0*c11 - 1/D = " << c0*c11 - 1.0/D << std::endl;
               //   std::cout << " D            = " << D << std::endl;
               //   std::cout << " 1/c11        = " << 1.0/c11 << std::endl;
               //   std::cout << " c0           = " << c0 << std::endl;
               //   std::cout << " c11          = " << c11 << std::endl;
               //   std::cout << " 1/D          = " << 1.0/D << std::endl;
               //   std::cout << " c12          = " << c12 << std::endl;
               //   std::cout << " -eta/d       = " << -eta/d << std::endl;
               //   std::cout << " c21          = " << c21 << std::endl;
               //   std::cout << " xi/D         = " <<  xi/D<< std::endl;
               //   std::cout << " c22          = " << c22 << std::endl;
               //   std::cout << " 1.0/d        = " << 1.0/d << std::endl;
               //   std::cout << " chi          = " << chi << std::endl;
               //   
               //}
               //             
               fg1->SetBinContent(bin,g1);
               fg1->SetBinError(bin, eg1);
               fg2->SetBinContent(bin,g2);
               fg2->SetBinError(bin, eg2);

            }
         }
      }
      // -------------------------------
      // Take care of the asymmetry class
      fg1Full->fAsymmetryVsW = *fg1;
      fg2Full->fAsymmetryVsW = *fg2;

      // -------------------------------
      TH1 * h1 = fg2; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if( jj<4 ) {
         mg2->Add(gr,"p");
      }


      h1 = fg1; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if( jj<4 ) {
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

   }
   // ------------------------------------------
   TFile * fout = new TFile(Form("data/g1g2_47_W_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");

   fout->WriteObject(fAllg1Asym,Form("g1asym-47-W-%d",0));
   fout->WriteObject(fAllg2Asym,Form("g2asym-47-W-%d",0));
   fout->Close();

   Double_t Q2 = 4.0;

   //fout->WriteObject(fA1Asymmetries,Form("A1-para47-%d",paraRunGroup));//,TObject::kSingleKey); 
   //fout->WriteObject(fA2Asymmetries,Form("A2-%d",paraRunGroup));//,TObject::kSingleKey); 


   // --------------- g1 ----------------------
   c->cd(1);
   gPad->SetGridy(true);
   TString Title;
   Int_t width = 1;

   // Load in world data on g1 from NucDB  
   //gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();

   Double_t Q2_min = 1.5;
   Double_t Q2_max = 7.0;
   NucDBBinnedVariable * Qsqbin = 0;
   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(4);
   NucDBMeasurement * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   TMultiGraph *MG = new TMultiGraph(); 
   TList * measurementsList =  manager->GetMeasurements("g1p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   // SLAC E143
   //exp = manager->GetExperiment("SLAC E143"); 
   //if(exp) ames = exp->GetMeasurement("g1p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155
   //exp = manager->GetExperiment("SLAC E155"); 
   //if(exp) ames = exp->GetMeasurement("g1p");
   //if(ames) measurementsList->Add(ames);
   //leg->SetFillColor(kWhite); 

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *measg1 = (NucDBMeasurement*)measurementsList->At(i);
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg1->BuildGraph("W");
      //if( graph->GetN() == 0 ) continue;
      graph->SetMarkerStyle(27);
      // Set legend title 
      leg->AddEntry(graph,measg1->GetExperimentName(),"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }
   //TGraphErrors *graph        = measg1_saneQ2->BuildGraph("W");
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   //leg->AddEntry(graph,Form("SLAC in %s",Qsqbin->GetTitle()),"lp");
   //MG->Add(graph,"p");

   TString Measurement = Form("g_{1}^{p}");
   TString GTitle      = "";//Form("Preliminary %s, E=4.7 GeV",Measurement);
   TString xAxisTitle  = Form("W");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(1.0,4.0); 
   MG->GetYaxis()->SetRangeUser(-0.1,0.4); 
   MG->Draw("AP");
   leg->Draw();


   //--------------- g2 ---------------------
   c->cd(2);
   gPad->SetGridy(true);
   TMultiGraph *MG2 = new TMultiGraph(); 

   NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   measurementsList->Clear();
   //// SLAC E143
   //exp = manager->GetExperiment("SLAC E143"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155
   //exp = manager->GetExperiment("SLAC E155"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   measurementsList =  manager->GetMeasurements("g2p");

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
      //A2pMeas->PrintData();
      // Get TGraphErrors object 
      TGraphErrors *graph        = A2pMeas->BuildGraph("W");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A2pMeas->GetExperimentName()); 
      //leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 
      //measg2_saneQ2->AddDataPoints(A2pMeas->FilterWithBin(Qsqbin));
   }
   //graph        = measg2_saneQ2->BuildGraph("W");
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   ////leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   //MG2->Add(graph,"p");

   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary g_{2}^{p}, E=4.7 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(1.0,4.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   MG2->GetYaxis()->SetRangeUser(-0.3,0.3); 
   MG2->GetYaxis()->SetTitle("g_{2}^{p}");
   MG2->GetXaxis()->SetTitle("W");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   c->Update();

   c->SaveAs(Form("results/asymmetries/g1g2_47_W_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/g1g2_47_W_%d.png",aNumber));

   //fout->WriteObject(fA1Asymmetries,Form("A1-47-%d",0));//,TObject::kSingleKey);
   //fout->WriteObject(fA2Asymmetries,Form("A2-47-%d",0));//,TObject::kSingleKey);

   return(0);
}
