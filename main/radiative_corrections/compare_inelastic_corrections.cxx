Int_t compare_inelastic_corrections(Int_t paraRunGroup = 21, Int_t perpRunGroup = 21, Int_t aNumber=5){

   Double_t theta_target = 80.0*degree;
   Double_t fBeamEnergy = 4.7; 

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();

   fman->CreatePolSFs(6);

   InSANEPolarizedStructureFunctions * polSFs = fman->GetPolarizedStructureFunctions();
   TString psf_label = polSFs->GetLabel(); 
   //InSANEVirtualComptonAsymmetriesModel1 * VCSA = new InSANEVirtualComptonAsymmetriesModel1();
   //InSANEPolSFsFromComptonAsymmetries * polSFsNew = new InSANEPolSFsFromComptonAsymmetries();
   //polSFsNew->SetVirtualComptonAsymmetries(VCSA);
   //fman->SetPolarizedStructureFunctions(polSFsNew);

   gROOT->LoadMacro("radiative_corrections/init_xsections.cxx"); 
   init_xsections(fBeamEnergy,theta_target);

   Double_t x_arg[5], p_arg[5];

   TString  parafile = Form("data/bg_corrected_asymmetries_para47_%d.root",paraRunGroup);
   TFile * f1 = TFile::Open(parafile.Data(),"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);

   TString  perpfile = Form("data/bg_corrected_asymmetries_perp47_%d.root",perpRunGroup);
   TFile * f2 = TFile::Open(perpfile.Data(),"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);


   TLatex * latex = new TLatex();
   latex->SetNDC(true);

   TLegend       * leg     = new TLegend(0.1,0.6,0.5,0.9);
   leg->SetFillStyle(0);
   leg->SetFillColor(0);
   TMultiGraph   * mg      = new TMultiGraph();
   TMultiGraph   * mgBorn  = new TMultiGraph();
   TMultiGraph   * mgDelta = new TMultiGraph();
   TMultiGraph   * mgCorr  = new TMultiGraph();
   TMultiGraph   * mgRad   = new TMultiGraph();
   TGraphErrors  * gr      = 0;
   TH1F          * h1      = 0;

   // Loop over  asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries() && jj<4;jj++) {

      InSANEMeasuredAsymmetry * paraAsym = (InSANEMeasuredAsymmetry*)(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry * perpAsym = (InSANEMeasuredAsymmetry*)(fPerpAsymmetries->At(jj));


      TH1F * fA180_x   = paraAsym->fAsymmetryVsx;
      TH1F * fA80_x    = perpAsym->fAsymmetryVsx;
      TH1F * fA_x      = fA80_x;

      InSANEAveragedKinematics1D * paraKine = paraAsym->fAvgKineVsx; 
      InSANEAveragedKinematics1D * perpKine = paraAsym->fAvgKineVsx; 
      InSANEAveragedKinematics1D * kine     = perpKine; 

      Int_t   xBinmax = fA_x->GetNbinsX();
      Int_t   yBinmax = fA_x->GetNbinsY();
      Int_t   zBinmax = fA_x->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      TH1F * fDeltaA180_x = new TH1F(*fA_x);
      fDeltaA180_x->SetNameTitle(Form("fDeltaA180_x-%d",jj),Form("#DeltaA180_x-%d",jj));
      fDeltaA180_x->Reset();

      TH1F * fA180Raw_x = new TH1F(*fA_x);
      fA180Raw_x->SetNameTitle(Form("fA180Raw_x-%d",jj),Form("A180Raw_x-%d",jj));
      fA180Raw_x->Reset();

      TH1F * fA180Rad_x = new TH1F(*fA_x);
      fA180Rad_x->SetNameTitle(Form("fA180Rad_x-%d",jj),Form("A180Rad_x-%d",jj));
      fA180Rad_x->Reset();

      TH1F * fA180Born_x = new TH1F(*fA_x);
      fA180Born_x->SetNameTitle(Form("fA180Born_x-%d",jj),Form("A180Born_x-%d",jj));
      fA180Born_x->Reset();

      TH1F * fACorrection_x = new TH1F(*fA_x);
      fACorrection_x->SetNameTitle(Form("fACorrection_x-%d",jj),Form("ACorrection_x-%d",jj));
      fACorrection_x->Reset();

      // loop over x bins
      for(Int_t i=0; i< xBinmax; i++){

         bin   = fA_x->GetBin(i);
         std::cout << " -------------------------------------" << std::endl;
         std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t E0    = fBeamEnergy;
         Double_t x     = kine->fx->GetBinContent(bin);
         Double_t W     = kine->fW->GetBinContent(bin);
         Double_t phi   = kine->fPhi->GetBinContent(bin);
         Double_t Q2    = kine->fQ2->GetBinContent(bin);
         Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
         Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
         Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

         x_arg[0]                = x;
         p_arg[0]                = Q2;
         p_arg[1]                = phi;

         Double_t A           = fA_x->GetBinContent(bin);
         Double_t eA          = fA_x->GetBinError(bin);

         Double_t sig_born_in_plus  = fBorn1->xDependentXSec(x_arg,p_arg);
         Double_t sig_born_in_minus = fBorn2->xDependentXSec(x_arg,p_arg);
         Double_t sigma_in_plus  = fIRT1_rt->xDependentXSec(x_arg,p_arg);
         Double_t sigma_in_minus = fIRT2_rt->xDependentXSec(x_arg,p_arg);
         Double_t sigma_in       = (sigma_in_plus + sigma_in_minus)/2.0;

         if( sigma_in == 0  ) sigma_in = 1.0e-8;
         Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in*2.0);
         if( TMath::IsNaN(A_calc) ) A_calc = 0;
         Double_t delta_A        = A - A_calc;

         Double_t A_born         = (sig_born_in_plus - sig_born_in_minus)/(sig_born_in_plus + sig_born_in_minus);
         Double_t A_correction   = A_calc - A_born;

         if( TMath::IsNaN(A) ) A = 0;
         std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         std::cout << " A_raw      = " <<  A << std::endl; 
         std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         std::cout << " delta_A    = " <<  delta_A << std::endl; 
         std::cout << " A_born     = " <<  A_born << std::endl; 
         std::cout << " A_corr     = " <<  A_correction << std::endl; 

         if( TMath::IsNaN(A_born)  ) A_born = 0;
         if( TMath::IsNaN(A_correction)  ) A_correction = 0;
         if( TMath::IsNaN(A_calc)  ) A_calc = 0;
         if( TMath::IsNaN(delta_A)  ) delta_A = 0;
         if( TMath::Infinity == delta_A ) delta_A = 0;

         fDeltaA180_x->SetBinContent(bin,delta_A);
         fDeltaA180_x->SetBinError(bin,eA);

         fACorrection_x->SetBinContent(bin,-1.0*A_correction);

         fA180Raw_x->SetBinContent(bin,A);
         fA180Raw_x->SetBinError(bin,eA);

         fA180Born_x->SetBinContent(bin,A_born);
         //fA180Born_x->SetBinError(bin,eA180);

         fA180Rad_x->SetBinContent(bin,A_calc);
         //fA180Rad_x->SetBinError(bin,eA180);

      }

      // Raw Asymmetry data 
      gr = GetCleanGraph(fA180Raw_x);
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor( fA_x->GetMarkerColor());
      gr->SetMarkerSize(  fA_x->GetMarkerSize()/2.0);
      gr->SetLineColor(   fA_x->GetMarkerColor());
      if(jj==0) leg->AddEntry(gr,"A raw","p");
      mg->Add(gr,"p");

      // Born Asymmetry calculated 
      gr = GetCleanGraph(fA180Born_x);
      gr->SetMarkerStyle(24);
      gr->SetMarkerColor( fA_x->GetMarkerColor());
      gr->SetMarkerSize(  fA_x->GetMarkerSize()/2.0);
      gr->SetLineColor(   fA_x->GetMarkerColor());
      if(jj==0) leg->AddEntry(gr,"Born model","p");
      mgBorn->Add(gr,"p");

      // Raw Asymmetry - Radiated model asymmetry
      gr = GetCleanGraph(fDeltaA180_x);
      gr->SetMarkerStyle(22);
      gr->SetMarkerColor( fA_x->GetMarkerColor());
      gr->SetMarkerSize(  fA_x->GetMarkerSize());
      gr->SetLineColor(   fA_x->GetMarkerColor());
      if(jj==0) leg->AddEntry(gr,"Raw data - Radiated model","p");
      mgDelta->Add(gr,"p");

      // Radiated model - Born model 
      gr = GetCleanGraph(fACorrection_x);
      //gr->SetMarkerStyle(20);
      //gr->SetMarkerColor( fA_x->GetMarkerColor());
      //gr->SetMarkerSize(  0.5);
      //gr->SetLineColor(   fA_x->GetMarkerColor());
      if(jj==0) leg->AddEntry(gr,"Radiated model - Born model","p");
      mgCorr->Add(gr,"l");

      // Radiated model
      gr = GetCleanGraph(fA180Rad_x);
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor( fA_x->GetMarkerColor());
      //gr->SetMarkerSize(  0.5);
      //gr->SetLineStyle(2);
      gr->SetLineColor(   fA_x->GetMarkerColor());
      if(jj==0) leg->AddEntry(gr,"Radiated Model","l");
      mgRad->Add(gr,"l");
   }

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);
   TCanvas * c = new TCanvas();
   c->Divide(2,2);

   c->cd(1);
   mg->SetTitle("Data");
   mg->Add(mgRad);
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle(true);
   mg->SetMaximum(1.1);
   mg->SetMinimum(-0.6);
   //mg->Draw("a");
   gPad->Modified();

   c->cd(2);
   mgDelta->Draw("a");
   mgDelta->SetTitle("Data - Model (Radiated)");
   mgDelta->GetXaxis()->SetTitle("x");
   mgDelta->GetXaxis()->CenterTitle(true);
   mgDelta->SetMaximum(2.1);
   mgDelta->SetMinimum(-1.1);
   gPad->Modified();
   leg->Draw();

   c->cd(3);
   mgBorn->SetTitle("Born model");
   mgBorn->Add(mgRad);
   mgBorn->Draw("a");
   mgBorn->GetXaxis()->SetTitle("x");
   mgBorn->GetXaxis()->CenterTitle(true);
   //mgBorn->SetTitle("Correction to get Born");
   //mgBorn->SetMaximum(1.1);
   //mgBorn->SetMinimum(-1.1);
   gPad->Modified();

   c->cd(4);
   mgCorr->SetTitle("Correction to get Born");
   mgCorr->Draw("a");
   mgCorr->GetXaxis()->SetTitle("x");
   mgCorr->GetXaxis()->CenterTitle(true);
   //mgCorr->SetMaximum(1.1);
   //mgCorr->SetMinimum(-1.1);
   latex->DrawLatex(0.3,0.3,Form("using %s",psf_label.Data()));
   gPad->Modified();

   c->SaveAs(Form("results/RC/compare_inelastic_corrections_%s_%d.png",psf_label.Data(),aNumber));
   c->SaveAs(Form("results/RC/compare_inelastic_corrections_%s_%d.pdf",psf_label.Data(),aNumber));
   c->SaveAs(Form("results/RC/compare_inelastic_corrections_%s_%d.tex",psf_label.Data(),aNumber));

   return(0);
}
