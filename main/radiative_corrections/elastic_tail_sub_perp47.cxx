/*! Subtracts the elastic tail.
 *
 */
Int_t elastic_tail_sub_perp47(Int_t perpRunGroup = 4405,Int_t aNumber=4405){

   Double_t theta_target = 80.0*degree;
   Double_t fBeamEnergy  = 4.7;
   TString  filename     = Form("data/bg_corrected_asymmetries_perp47_%d.root",perpRunGroup);

   // --------------------------------------------------

   TFile * f1 = TFile::Open(filename,"UPDATE");
   f1->cd();
   TList * fAsymmetries = (TList *) gROOT->FindObject(Form("background-subtracted-asym_perp47-%d",0));
   if(!fAsymmetries) return(-1);

   TList * asym_write = new TList();
   TList * RC_write   = new TList();

   for(int jj = 0;jj<fAsymmetries->GetEntries() ;jj++) {

      // Get the measured asymmetry 
      //InSANEAveragedMeasuredAsymmetry * asymAverage = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry * asym                = asymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)(fAsymmetries->At(jj));
      asym_write->Add(asym);

      Int_t   xBinmax = 0;
      Int_t   yBinmax = 0;
      Int_t   zBinmax = 0;
      Int_t   bin     = 0;
      TH1F * fA                             = 0;
      InSANEAveragedKinematics1D * avgKine  = 0;
      InSANERadiativeCorrections1D * RCs    = 0;

      // -------------------------------------------------
      // Asymmetry vs x
      fA      = &asym->fAsymmetryVsx;
      avgKine = &asym->fAvgKineVsx;
      RCs     = new InSANERadiativeCorrections1D(Form("rc-x-%d",jj));
      RC_write->Add(RCs);
      RCs->SetBeamEnergy(fBeamEnergy);
      RCs->SetThetaTarget(theta_target);
      RCs->SetAvgKine(avgKine);
      RCs->InitCrossSections();
      //std::cout << "Calculating RCs vs x for Q2 bin : " << asym->GetQ2() << std::endl;
      RCs->Calculate();

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over x bins
      //
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);

         Double_t sigma_in       = 2.0*RCs->fSigmaIRT->GetBinContent(bin);
         Double_t sigma_in_plus  = RCs->fSigmaIRT_Plus->GetBinContent(bin);
         Double_t sigma_in_minus = RCs->fSigmaIRT_Minus->GetBinContent(bin);
         Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
         Double_t sigma_el_plus  = RCs->fSigmaERT_Plus->GetBinContent(bin);
         Double_t sigma_el_minus = RCs->fSigmaERT_Minus->GetBinContent(bin);
         Double_t sigma_el       = 2.0*RCs->fSigmaERT->GetBinContent(bin);
         Double_t C_el           = (sigma_el_plus - sigma_el_minus)/(sigma_in);
         Double_t f_el           =  sigma_in/(sigma_in + sigma_el);

         // FIX BACKGROUND
         //Double_t Ep             = avgKine->fE->GetBinContent(bin);
         //Double_t f_bg           =  4.122*TMath::Exp(-2.442*Ep);
         //Double_t C_back         = (1.0 /*- f_bg*0.05/A*/)/(1.0 - f_bg);
         Double_t A_corr         = A/f_el - C_el;
         Double_t delta_A        = A - A_calc;

         if( TMath::IsNaN(A_calc) ) A_calc = 0;
         if(sigma_el == 0) continue;
         //std::cout << " Ep         = " <<  Ep   << std::endl;
         //std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         //std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         //std::cout << " sigma_el   = " <<  sigma_el << std::endl;
         //std::cout << " A_el       = " <<  (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus) << std::endl;
         //std::cout << " C_el       = " <<  C_el   << std::endl;
         //std::cout << " f_el       = " <<  f_el   << std::endl;
         //std::cout << " C_back     = " <<  C_back   << std::endl;
         //std::cout << " f_bg       = " <<  f_bg   << std::endl;
         //std::cout << " A_raw      = " <<  A << std::endl; 
         //std::cout << " A_corr     = " <<  A_corr << std::endl; 

         fA->SetBinContent(bin,A_corr);

      }

      // -------------------------------------------------
      // Asymmetry vs W
      fA      = &asym->fAsymmetryVsW;
      avgKine = &asym->fAvgKineVsW;
      RCs     = new InSANERadiativeCorrections1D(Form("rc-W-%d",jj));
      RC_write->Add(RCs);
      RCs->SetBeamEnergy(fBeamEnergy);
      RCs->SetThetaTarget(theta_target);
      RCs->SetAvgKine(avgKine);
      RCs->InitCrossSections();
      //std::cout << "Calculating RCs vs W for Q2 bin : " << asym->GetQ2() << std::endl;
      RCs->Calculate();

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over W bins
      //
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);

         Double_t sigma_in       = 2.0*RCs->fSigmaIRT->GetBinContent(bin);
         Double_t sigma_in_plus  = RCs->fSigmaIRT_Plus->GetBinContent(bin);
         Double_t sigma_in_minus = RCs->fSigmaIRT_Minus->GetBinContent(bin);
         Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
         Double_t sigma_el_plus  = RCs->fSigmaERT_Plus->GetBinContent(bin);
         Double_t sigma_el_minus = RCs->fSigmaERT_Minus->GetBinContent(bin);
         Double_t sigma_el       = 2.0*RCs->fSigmaERT->GetBinContent(bin);
         Double_t C_el           = (sigma_el_plus - sigma_el_minus)/(sigma_in);
         Double_t f_el           =  sigma_in/(sigma_in + sigma_el);

         // FIX BACKGROUND
         Double_t Ep             = avgKine->fE.GetBinContent(bin);
         //Double_t f_bg           =  4.122*TMath::Exp(-2.442*Ep);
         //Double_t C_back         = (1.0 /*- f_bg*0.05/A*/)/(1.0 - f_bg);
         Double_t A_corr         = A/f_el - C_el;
         Double_t delta_A        = A - A_calc;

         if( TMath::IsNaN(A_calc) ) A_calc = 0;
         if(sigma_el == 0) continue;
         //std::cout << " Ep         = " <<  Ep   << std::endl;
         //std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         //std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         //std::cout << " sigma_el   = " <<  sigma_el << std::endl;
         //std::cout << " A_el       = " <<  (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus) << std::endl;
         //std::cout << " C_el       = " <<  C_el   << std::endl;
         //std::cout << " f_el       = " <<  f_el   << std::endl;
         //std::cout << " C_back     = " <<  C_back   << std::endl;
         //std::cout << " f_bg       = " <<  f_bg   << std::endl;
         //std::cout << " A_raw      = " <<  A << std::endl; 
         //std::cout << " A_corr     = " <<  A_corr << std::endl; 

         fA->SetBinContent(bin,A_corr);

      }

   }

   f1->WriteObject(asym_write,Form("elastic-subtracted-asym_perp47-%d",0));//,TObject::kSingleKey); 
   f1->WriteObject(RC_write,Form("elastic-RCs_perp47-%d",0));//,TObject::kSingleKey); 
 
   return(0);
}
