Int_t tables_A1A2_47(Int_t paraRunGroup = 90,Int_t perpRunGroup = 90,Int_t aNumber=90){

   Int_t paraFileVersion          = paraRunGroup;
   Int_t perpFileVersion          = perpRunGroup;

   Double_t E0                    = 5.9;
   Double_t alpha                 = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman   = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   const TString weh("A1A2_47_noel()");
   if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
      Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
      return -1;
   }
   sane_data_bins();
   
   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/bg_corrected_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);

   TFile * fout = new TFile(Form("data/A1A2_47_noel_%d_%d.root",paraFileVersion,perpFileVersion),"READ");
   fout->cd();

   TList        * fA1Asymmetries = (TList*)gROOT->FindObject(Form("A1asym-47-x-%d",0));
   TList        * fA2Asymmetries = (TList*)gROOT->FindObject(Form("A2asym-47-x-%d",0));

   TList        * fF1_SFs        = new TList();
   TList        * fAllA1Asym = new TList();
   TList        * fAllA2Asym = new TList();

   TH1F         * fA1            = 0;
   TH1F         * fA2            = 0;
   TH1F         * fF1            = 0;

   TGraphErrors * gr             = 0;

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para47");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries->GetEntries();jj++) {

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * A1Asym        = (InSANEMeasuredAsymmetry * )(fA1Asymmetries->At(jj));

      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * A2Asym        = (InSANEMeasuredAsymmetry * )(fA2Asymmetries->At(jj));


      std::ofstream outtable1(Form("results/asymmetries/tables/%d/A1_el_sub_47_%d_%d.txt",aNumber,jj,aNumber));
      std::ofstream outtable2(Form("results/asymmetries/tables/%d/A2_el_sub_47_%d_%d.txt",aNumber,jj,aNumber));

      A1Asym->PrintBinnedTableHead(outtable1);
      A1Asym->PrintBinnedTable(outtable1);

      A2Asym->PrintBinnedTableHead(outtable2);
      A2Asym->PrintBinnedTable(outtable2);

      std::ofstream asymtable1(Form("results/asymmetries/tables/%d/A1_el_sub_47_%d_%d.tex",aNumber,jj,aNumber));
      std::ofstream asymtable2(Form("results/asymmetries/tables/%d/A2_el_sub_47_%d_%d.tex",aNumber,jj,aNumber));
      A1Asym->PrintAsymTable(asymtable1);
      A2Asym->PrintAsymTable(asymtable2);

   }

   return(0);
}
