/*! Initializes the cross sections used for radiative corrections scripts.
 */

InSANEInclusiveDiffXSec * fBorn0 = 0;
InSANEInclusiveDiffXSec * fBorn1 = 0;
InSANEInclusiveDiffXSec * fBorn2 = 0;
InSANEInclusiveDiffXSec * fERT0_rt = 0;
InSANEInclusiveDiffXSec * fERT1_rt = 0;
InSANEInclusiveDiffXSec * fERT2_rt = 0;
InSANEInclusiveDiffXSec * fIRT0_rt = 0;
InSANEInclusiveDiffXSec * fIRT1_rt = 0;
InSANEInclusiveDiffXSec * fIRT2_rt = 0;
InSANEInclusiveDiffXSec * fDiffXSec03 = 0;
InSANEInclusiveDiffXSec * fDiffXSec13 = 0;
InSANEInclusiveDiffXSec * fDiffXSec23 = 0;

Int_t init_xsections(Double_t beamEnergy = 5.9, Double_t theta_target = 180*degree){


   //-----------------------

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   //-----------------------
   
   InSANEPhaseSpace * fPolarizedPhaseSpace = new InSANEPhaseSpace();
   InSANEPhaseSpace * fPhaseSpace          = new InSANEPhaseSpace();

   InSANEPhaseSpaceVariable * varEnergy = new InSANEPhaseSpaceVariable("Energy","E#prime"); 
   varEnergy->SetMinimum(0.001);         //GeV
   varEnergy->SetMaximum(beamEnergy); //GeV
   varEnergy->SetVariableUnits("GeV");        //GeV
   fPolarizedPhaseSpace->AddVariable(varEnergy);
   fPhaseSpace->AddVariable(varEnergy);

   InSANEPhaseSpaceVariable *   varTheta = new InSANEPhaseSpaceVariable("theta","#theta");
   varTheta->SetMinimum(2.0*TMath::Pi()/180.0); //
   varTheta->SetMaximum(180.0*TMath::Pi()/180.0); //
   varTheta->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varTheta);
   fPhaseSpace->AddVariable(varTheta);

   InSANEPhaseSpaceVariable *   varPhi = new InSANEPhaseSpaceVariable("phi","#phi");
   varPhi->SetMinimum(-180.0*TMath::Pi()/180.0); //
   varPhi->SetMaximum(180.0*TMath::Pi()/180.0); //
   varPhi->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varPhi);
   fPhaseSpace->AddVariable(varPhi);

   InSANEDiscretePhaseSpaceVariable *   varHelicity = new InSANEDiscretePhaseSpaceVariable("helicity","#lambda");
   varHelicity->SetNumberOfValues(3); // ROOT string latex
   fPolarizedPhaseSpace->AddVariable(varHelicity);

   fPolarizedPhaseSpace->Refresh();

   varEnergy->Print();
   varTheta->Print();
   varPhi->Print();
   varHelicity->Print();

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();

   //-----------------------
   // Born Cross sections
   // born unpolarized 
   InSANEPOLRADBornDiffXSec * fDiffXSec00 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec00->SetBeamEnergy(beamEnergy);
   fDiffXSec00->SetTargetType(InSANENucleus::kProton);
   fDiffXSec00->SetPhaseSpace(fPhaseSpace);
   fDiffXSec00->Refresh();
   fBorn0 = fDiffXSec00;
   // born polarized + 
   InSANEPOLRADBornDiffXSec * fDiffXSec10 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec10->SetBeamEnergy(beamEnergy);
   fDiffXSec10->SetPhaseSpace(fPhaseSpace);
   fDiffXSec10->SetTargetType(InSANENucleus::kProton);
   fDiffXSec10->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec10->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec10->Refresh();
   fBorn1 = fDiffXSec10;
   // born polarized - 
   InSANEPOLRADBornDiffXSec * fDiffXSec20 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec20->SetBeamEnergy(beamEnergy);
   fDiffXSec20->SetPhaseSpace(fPhaseSpace);
   fDiffXSec20->SetTargetType(InSANENucleus::kProton);
   fDiffXSec20->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec20->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec20->Refresh();
   fBorn2 = fDiffXSec20;

   //------------------------
   // Elastic Radiatve Tail 
   // internal and external radiated elastic unpolarized 
   InSANEElasticRadiativeTail * fDiffXSec02 = new  InSANEElasticRadiativeTail();
   fDiffXSec02->SetBeamEnergy(beamEnergy);
   fDiffXSec02->SetPolarizations(0.0,0.0);
   fDiffXSec02->SetTargetType(InSANENucleus::kProton);
   fDiffXSec02->SetPhaseSpace(fPhaseSpace);
   fDiffXSec02->Refresh();
   fERT0_rt = fDiffXSec02;
   // internal and external radiated elastic polarized + 
   InSANEElasticRadiativeTail * fDiffXSec12 = new  InSANEElasticRadiativeTail();
   fDiffXSec12->SetBeamEnergy(beamEnergy);
   fDiffXSec12->SetPolarizations(1.0,1.0);
   fDiffXSec12->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec12->GetPOLRADERT()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec12->SetTargetType(InSANENucleus::kProton);
   fDiffXSec12->SetPhaseSpace(fPhaseSpace);
   fDiffXSec12->Refresh();
   fERT1_rt = fDiffXSec12;
   // internal and external radiated polarized -
   InSANEElasticRadiativeTail * fDiffXSec22 = new InSANEElasticRadiativeTail();
   fDiffXSec22->SetBeamEnergy(beamEnergy);
   fDiffXSec22->SetPolarizations(1.0,1.0);
   fDiffXSec22->GetPOLRADERT()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec22->SetTargetType(InSANENucleus::kProton);
   fDiffXSec22->SetPhaseSpace(fPhaseSpace);
   fDiffXSec22->Refresh();
   fERT2_rt = fDiffXSec22;


   //-------------------------
   //// IRT unpolarized 
   InSANEFullInelasticRadiativeTail * fDiffXSec01 = new  InSANEFullInelasticRadiativeTail();
   fDiffXSec01->SetBeamEnergy(beamEnergy);
   fDiffXSec01->GetInternalXSec()->SetPolarizations(0.0,0.0);
   fDiffXSec01->SetTargetType(InSANENucleus::kProton);
   fDiffXSec01->SetPhaseSpace(fPhaseSpace);
   fDiffXSec01->Refresh();
   fIRT0_rt = fDiffXSec01;
   // internal radiated polarized + 
   InSANEFullInelasticRadiativeTail * fDiffXSec11 = new  InSANEFullInelasticRadiativeTail();
   fDiffXSec11->SetBeamEnergy(beamEnergy);
   fDiffXSec11->GetInternalXSec()->SetPolarizations(1.0,1.0);
   fDiffXSec11->GetInternalXSec()->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec11->SetTargetType(InSANENucleus::kProton);
   fDiffXSec11->SetPhaseSpace(fPhaseSpace);
   fDiffXSec11->Refresh();
   fIRT1_rt = fDiffXSec11;
   // internal radiated polarized -
   InSANEFullInelasticRadiativeTail * fDiffXSec21 = new  InSANEFullInelasticRadiativeTail();
   fDiffXSec21->SetBeamEnergy(beamEnergy);
   fDiffXSec21->GetInternalXSec()->SetPolarizations(1.0,1.0);
   fDiffXSec21->GetInternalXSec()->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec21->SetTargetType(InSANENucleus::kProton);
   fDiffXSec21->SetPhaseSpace(fPhaseSpace);
   fDiffXSec21->Refresh();
   fIRT2_rt = fDiffXSec21;


   return(0);
}

TGraphErrors * GetCleanGraph(TH1F* h1 ){
   TGraphErrors * gr = new TGraphErrors(h1);
   for( int j = gr->GetN()-1;j>=0; j--) {
      Double_t xt, yt;
      gr->GetPoint(j,xt,yt);
      if( yt == 0  ) {
         gr->RemovePoint(j);
      }
   }
   return gr;
}
