/*! Subtracts the elastic tail.
 *
 */
Int_t elastic_tail_sub_para59(Int_t paraRunGroup = 8011,Int_t aNumber=8011){

   Double_t theta_target = 180.0*degree;
   Double_t fBeamEnergy  = 5.9;
   TString  filename     = Form("data/bg_corrected_asymmetries_para59_%d.root",paraRunGroup);

   // --------------------------------------------------

   TFile * f1 = TFile::Open(filename,"UPDATE");
   f1->cd();
   TList * fAsymmetries = (TList *) gROOT->FindObject(Form("background-subtracted-asym_para59-%d",0));
   if(!fAsymmetries) return(-1);

   TList * asym_write = new TList();
   TList * RC_write   = new TList();

   TList * RC0_write   = new TList();
   TList * RC1_write   = new TList();

   for(int jj = 0;jj<fAsymmetries->GetEntries() ;jj++) {

      // Get the measured asymmetry 
      //InSANEAveragedMeasuredAsymmetry * asymAverage = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry * asym                = asymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)(fAsymmetries->At(jj));
      asym_write->Add(asym);

      Int_t   xBinmax = 0;
      Int_t   yBinmax = 0;
      Int_t   zBinmax = 0;
      Int_t   bin     = 0;
      TH1F * fA                             = 0;
      InSANEAveragedKinematics1D    * avgKine  = 0;
      InSANERadiativeCorrections1D  * RCs    = 0;
      SANE_RCs_Model0             * RCs0   = 0;
      SANE_RCs_Model1             * RCs1   = 0;

      // -------------------------------------------------
      // Asymmetry vs x
      fA      = &asym->fAsymmetryVsx;
      avgKine = &asym->fAvgKineVsx;
      RCs     = new InSANERadiativeCorrections1D(Form("rc-x-%d",jj));
      RC_write->Add(RCs);
      RCs->SetBeamEnergy(fBeamEnergy);
      RCs->SetThetaTarget(theta_target);
      RCs->SetAvgKine(avgKine);
      RCs->InitCrossSections();

      RCs0    = new SANE_RCs_Model0(Form("rc0-x-%d",jj));
      RC0_write->Add(RCs0);
      RCs0->SetBeamEnergy(fBeamEnergy);
      RCs0->SetThetaTarget(theta_target);
      RCs0->SetAvgKine(avgKine);
      RCs0->InitCrossSections();

      RCs1    = new SANE_RCs_Model1(Form("rc1-x-%d",jj));
      RC1_write->Add(RCs0);
      RCs1->SetBeamEnergy(fBeamEnergy);
      RCs1->SetThetaTarget(theta_target);
      RCs1->SetAvgKine(avgKine);
      RCs1->InitCrossSections();

      //std::cout << "Calculating RCs vs x for Q2 bin : " << asym->GetQ2() << std::endl;
      RCs->Calculate();
      //RCs0->Calculate();
      //RCs1->Calculate();

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over x bins
      //
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);

         Double_t sigma_in       = 2.0*RCs->fSigmaIRT->GetBinContent(bin);
         Double_t sigma_in_plus  = RCs->fSigmaIRT_Plus->GetBinContent(bin);
         Double_t sigma_in_minus = RCs->fSigmaIRT_Minus->GetBinContent(bin);
         Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
         Double_t sigma_el_plus  = RCs->fSigmaERT_Plus->GetBinContent(bin);
         Double_t sigma_el_minus = RCs->fSigmaERT_Minus->GetBinContent(bin);
         Double_t sigma_el       = 2.0*RCs->fSigmaERT->GetBinContent(bin);
         Double_t C_el           = (sigma_el_plus - sigma_el_minus)/(sigma_in);
         Double_t f_el           =  sigma_in/(sigma_in + sigma_el);

         // FIX BACKGROUND
         //Double_t Ep             = avgKine->fE->GetBinContent(bin);
         //Double_t f_bg           =  4.122*TMath::Exp(-2.442*Ep);
         //Double_t C_back         = (1.0 /*- f_bg*0.05/A*/)/(1.0 - f_bg);
         Double_t A_corr         = (A/f_el - C_el);
         Double_t delta_A        = A - A_calc;

         if( TMath::IsNaN(A_calc) ) A_calc = 0;
         if(sigma_el == 0) continue;
         //std::cout << " Ep         = " <<  Ep   << std::endl;
         //std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         //std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         //std::cout << " sigma_el   = " <<  sigma_el << std::endl;
         //std::cout << " A_el       = " <<  (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus) << std::endl;
         //std::cout << " C_el       = " <<  C_el   << std::endl;
         //std::cout << " f_el       = " <<  f_el   << std::endl;
         //std::cout << " C_back     = " <<  C_back   << std::endl;
         //std::cout << " f_bg       = " <<  f_bg   << std::endl;
         //std::cout << " A_raw      = " <<  A << std::endl; 
         //std::cout << " A_corr     = " <<  A_corr << std::endl; 

         fA->SetBinContent(bin,A_corr);

      }

      // -------------------------------------------------
      // Asymmetry vs W
      fA      = &asym->fAsymmetryVsW;
      avgKine = &asym->fAvgKineVsW;
      RCs     = new InSANERadiativeCorrections1D(Form("rc-W-%d",jj));
      RC_write->Add(RCs);
      RCs->SetBeamEnergy(fBeamEnergy);
      RCs->SetThetaTarget(theta_target);
      RCs->SetAvgKine(avgKine);
      RCs->InitCrossSections();

      RCs0    = new SANE_RCs_Model0(Form("rc0-W-%d",jj));
      RC0_write->Add(RCs0);
      RCs0->SetBeamEnergy(fBeamEnergy);
      RCs0->SetThetaTarget(theta_target);
      RCs0->SetAvgKine(avgKine);
      RCs0->InitCrossSections();

      RCs1    = new SANE_RCs_Model1(Form("rc1-W-%d",jj));
      RC1_write->Add(RCs0);
      RCs1->SetBeamEnergy(fBeamEnergy);
      RCs1->SetThetaTarget(theta_target);
      RCs1->SetAvgKine(avgKine);
      RCs1->InitCrossSections();

      //std::cout << "Calculating RCs vs W for Q2 bin : " << asym->GetQ2() << std::endl;
      RCs->Calculate();
      //RCs0->Calculate();
      //RCs1->Calculate();

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over W bins
      //
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);

         Double_t sigma_in       = RCs->fSigmaIRT->GetBinContent(bin);
         Double_t sigma_in_plus  = RCs->fSigmaIRT_Plus->GetBinContent(bin);
         Double_t sigma_in_minus = RCs->fSigmaIRT_Minus->GetBinContent(bin);
         Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
         Double_t sigma_el_plus  = RCs->fSigmaERT_Plus->GetBinContent(bin);
         Double_t sigma_el_minus = RCs->fSigmaERT_Minus->GetBinContent(bin);
         Double_t sigma_el       = RCs->fSigmaERT->GetBinContent(bin);
         Double_t C_el           = (sigma_el_plus - sigma_el_minus)/(sigma_in);
         Double_t f_el           =  sigma_in/(sigma_in + sigma_el);

         // FIX BACKGROUND
         Double_t Ep             = avgKine->fE.GetBinContent(bin);
         //Double_t f_bg           =  4.122*TMath::Exp(-2.442*Ep);
         //Double_t C_back         = (1.0 /*- f_bg*0.05/A*/)/(1.0 - f_bg);
         Double_t A_corr         = (A/f_el - C_el);
         Double_t delta_A        = A - A_calc;

         if( TMath::IsNaN(A_calc) ) A_calc = 0;
         if(sigma_el == 0) continue;
         //std::cout << " Ep         = " <<  Ep   << std::endl;
         //std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         //std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         //std::cout << " sigma_el   = " <<  sigma_el << std::endl;
         //std::cout << " A_el       = " <<  (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus) << std::endl;
         //std::cout << " C_el       = " <<  C_el   << std::endl;
         //std::cout << " f_el       = " <<  f_el   << std::endl;
         //std::cout << " C_back     = " <<  C_back   << std::endl;
         //std::cout << " f_bg       = " <<  f_bg   << std::endl;
         //std::cout << " A_raw      = " <<  A << std::endl; 
         //std::cout << " A_corr     = " <<  A_corr << std::endl; 

         fA->SetBinContent(bin,A_corr);

      }

   }

   f1->WriteObject(asym_write,Form("elastic-subtracted-asym_para59-%d",0));//,TObject::kSingleKey); 

   f1->WriteObject(RC_write, Form("elastic-RCs_para59-%d",0));//,TObject::kSingleKey); 
   f1->WriteObject(RC0_write,Form("model0-RCs_para59-%d",0));//,TObject::kSingleKey); 
   f1->WriteObject(RC1_write,Form("model1-RCs_para59-%d",0));//,TObject::kSingleKey); 
 
   return(0);
}
