/*! Subtracts the elastic tail.
 *
 */
Int_t correct_IRT_para59(Int_t paraRunGroup = 30,Int_t aNumber=30){

   Double_t theta_target = 180.0*degree;
   Double_t fBeamEnergy  = 5.9;
   TString  filename     = Form("data/bg_corrected_asymmetries_para59_%d.root",paraRunGroup);

   // --------------------------------------------------

   TFile * f1 = TFile::Open(filename,"UPDATE");
   f1->cd();
   TList * fAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para59-%d",0));
   if(!fAsymmetries) return(-1);

   TList * RC_list = (TList *) gROOT->FindObject(Form("elastic-RCs_para59-%d",0));//,TObject::kSingleKey); 

   TList * asym_write = new TList();

   TGraphErrors * gr = 0;
   TMultiGraph * mg  = new TMultiGraph();
   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);

   for(int jj = 0;jj<fAsymmetries->GetEntries() ;jj++) {

      // Get the measured asymmetry 
      //InSANEAveragedMeasuredAsymmetry * asymAverage = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry * asym                = asymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)(fAsymmetries->At(jj));
      asym_write->Add(asym);

      //std::ofstream outtable0(Form("results/asymmetries/tables/%d/A180_59_el_sub_%d_%d.txt",aNumber,aNumber,jj));
      //asym->PrintBinnedTableHead(outtable0);
      //asym->PrintBinnedTable(outtable0);


      Int_t   xBinmax = 0;
      Int_t   yBinmax = 0;
      Int_t   zBinmax = 0;
      Int_t   bin     = 0;
      TH1F * fA                             = 0;
      InSANEAveragedKinematics1D * avgKine  = 0;
      InSANERadiativeCorrections1D * RCs    = 0;

      // -------------------------------------------------
      // Asymmetry vs x
      fA      = asym->fAsymmetryVsx;
      avgKine = asym->fAvgKineVsx;

      RCs     = (InSANERadiativeCorrections1D*)RC_list->FindObject(Form("rc-x-%d",jj));

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over x bins
      //
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);
         Double_t eASyst    = asym->fSystErrVsx->GetBinContent(bin);

         Double_t sigma_in       = 2.0*RCs->fSigmaIRT->GetBinContent(bin);
         Double_t sigma_in_plus  =     RCs->fSigmaIRT_Plus->GetBinContent(bin);
         Double_t sigma_in_minus =     RCs->fSigmaIRT_Minus->GetBinContent(bin);
         Double_t A_in_calc      = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);

         Double_t sigma_b        = 2.0*RCs->fSigmaBorn->GetBinContent(bin);
         Double_t sigma_b_plus   =     RCs->fSigmaBorn_Plus->GetBinContent(bin);
         Double_t sigma_b_minus  =     RCs->fSigmaBorn_Minus->GetBinContent(bin);
         Double_t A_b_calc       = (sigma_b_plus - sigma_b_minus)/(sigma_b_plus + sigma_b_minus);

         Double_t A_correction   = ((A_in_calc - A_b_calc)/A_in_calc)*A;

         // Systematic errors
         //Double_t eSigma_in      = 0.01*sigma_in;
         //Double_t eSigma_el      = 0.01*sigma_in;
         //Double_t eDelta_el      = 0.01*TMath::Abs(sigma_el_plus - sigma_el_minus);

         // FIX BACKGROUND
         //Double_t Ep             = avgKine->fE->GetBinContent(bin);
         //Double_t f_bg           = 4.122*TMath::Exp(-2.442*Ep);
         //Double_t C_back         = (1.0 /*- f_bg*0.05/A*/)/(1.0 - f_bg);
         Double_t A_corr         = A - A_correction;
         Double_t delta_A        = A - A_b_calc;

         //if( TMath::IsNaN(A_in_calc) ) A_in_calc = 0;
         //if(sigma_el == 0) continue;
         //std::cout << " Ep         = " <<  Ep   << std::endl;
         //std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         //std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         //std::cout << " sigma_el   = " <<  sigma_el << std::endl;
         //std::cout << " A_el       = " <<  (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus) << std::endl;
         //std::cout << " C_el       = " <<  C_el   << std::endl;
         //std::cout << " f_el       = " <<  f_el   << std::endl;
         //std::cout << " C_back     = " <<  C_back   << std::endl;
         //std::cout << " f_bg       = " <<  f_bg   << std::endl;
         //std::cout << " A_raw      = " <<  A << std::endl; 
         //std::cout << " A_corr     = " <<  A_corr << std::endl; 

         fA->SetBinContent(bin,A_corr);

         // Systematic error
         //Double_t eSystBG  = eASyst*eASyst*Sigma_tot*Sigma_tot/(sigma_in*sigma_in);
         //Double_t num      = TMath::Power(eSigma_in*(delta_el-A*sigma_el),2.0);
         //num += TMath::Power(eDelta_el*sigma_in,2.0);
         //num += TMath::Power(eSigma_el*A*sigma_in,2.0);
         //eSystBG          += num/TMath::Power(sigma_in,4.0);
         //asym->fSystErrVsx->SetBinContent(bin,TMath::Sqrt(eSystBG));

      }

      gr = new TGraphErrors(fA);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         if( TMath::IsNaN(yt) ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerSize(0.8);
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fA->GetMarkerColor());
      gr->SetLineColor(fA->GetMarkerColor());
      if(jj%5==4){
         gr->SetMarkerStyle(20);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",asym->GetQ2()),"lp");
      } else if( jj<10  ){
         gr->SetMarkerStyle(23);
         //mgSplit->Add(gr,"p");
      } else { 
         gr->SetMarkerStyle(22);
         //mgSplit->Add(gr,"p");
      }

      // -------------------------------------------------
      // Asymmetry vs W
      fA      = asym->fAsymmetryVsW;
      avgKine = asym->fAvgKineVsW;

      RCs     = (InSANERadiativeCorrections1D*)RC_list->FindObject(Form("rc-W-%d",jj));

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over W bins
      //
      /*
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);
         Double_t eASyst    = asym->fSystErrVsx->GetBinContent(bin);

         Double_t sigma_in       = 2.0*RCs->fSigmaIRT->GetBinContent(bin);
         Double_t sigma_in_plus  = RCs->fSigmaIRT_Plus->GetBinContent(bin);
         Double_t sigma_in_minus = RCs->fSigmaIRT_Minus->GetBinContent(bin);
         Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
         Double_t sigma_el_plus  = RCs->fSigmaERT_Plus->GetBinContent(bin);
         Double_t sigma_el_minus = RCs->fSigmaERT_Minus->GetBinContent(bin);
         Double_t sigma_el       = 2.0*RCs->fSigmaERT->GetBinContent(bin);
         Double_t C_el           = (sigma_el_plus - sigma_el_minus)/(sigma_in);
         Double_t f_el           =  sigma_in/(sigma_in + sigma_el);
         Double_t Sigma_tot      = (sigma_in + sigma_el);
         Double_t delta_el       = (sigma_el_plus - sigma_el_minus);

         // Systematic errors
         Double_t eSigma_in      = 0.01*sigma_in;
         Double_t eSigma_el      = 0.01*sigma_in;
         Double_t eDelta_el      = 0.01*(sigma_el_plus - sigma_el_minus);


         // FIX BACKGROUND
         Double_t Ep             = avgKine->fE->GetBinContent(bin);
         //Double_t f_bg           =  4.122*TMath::Exp(-2.442*Ep);
         Double_t A_corr         = A/f_el - C_el;
         Double_t delta_A        = A - A_calc;

         if( TMath::IsNaN(A_calc) ) A_calc = 0;
         if(sigma_el == 0) continue;
         //std::cout << " Ep         = " <<  Ep   << std::endl;
         //std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         //std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         //std::cout << " sigma_el   = " <<  sigma_el << std::endl;
         //std::cout << " A_el       = " <<  (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus) << std::endl;
         //std::cout << " C_el       = " <<  C_el   << std::endl;
         //std::cout << " f_el       = " <<  f_el   << std::endl;
         //std::cout << " C_back     = " <<  C_back   << std::endl;
         //std::cout << " f_bg       = " <<  f_bg   << std::endl;
         //std::cout << " A_raw      = " <<  A << std::endl; 
         //std::cout << " A_corr     = " <<  A_corr << std::endl; 

         fA->SetBinContent(bin,A_corr);

         // Systematic error
         Double_t eSystBG  = eASyst*eASyst*Sigma_tot*Sigma_tot/(sigma_in*sigma_in);
         Double_t num      = TMath::Power(eSigma_in*(delta_el-A*sigma_el),2.0);
         num += TMath::Power(eDelta_el*sigma_in,2.0);
         num += TMath::Power(eSigma_el*A*sigma_in,2.0);
         eSystBG          += num/TMath::Power(sigma_in,4.0);
         asym->fSystErrVsx->SetBinContent(bin,TMath::Sqrt(eSystBG));


      }
      */

   }

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   //c->Divide(2,2);
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.0,1.0); 
   mg->GetYaxis()->SetRangeUser(0.0,0.8); 
   mg->GetYaxis()->SetTitle("A_{180}^{p}");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle();
   mg->GetYaxis()->CenterTitle();
   mg->Draw("a");

   c->SaveAs(Form("results/asymmetries/correct_IRT_para59_%d.png",aNumber));


   f1->WriteObject(asym_write,Form("inelastic-corrected-asym_para59-%d",0));//,TObject::kSingleKey); 
   //f1->WriteObject(RC_write,Form("elastic-RCs_para59-%d",0));//,TObject::kSingleKey); 
 
   return(0);
}
