#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

Int_t x2g1g2_59(Int_t paraFileVersion = 7609,Int_t perpFileVersion = 7609,Int_t aNumber=7609){

   NucDBManager * manager = NucDBManager::GetManager();
   NucDBBinnedVariable * Qsqbin = 0;
   sane_data_bins();

   TFile * f = new TFile(Form("data/A1A2_59_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();

   TList * fA1Asymmetries = (TList*)gROOT->FindObject(Form("A1-59-x-%d",0));
   if(!fA1Asymmetries) return(-1);
   TList * fA2Asymmetries = (TList*)gROOT->FindObject(Form("A2-59-x-%d",0));
   if(!fA2Asymmetries) return(-2);

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("inelastic-subtracted-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * file2 = TFile::Open(perpfile,"UPDATE");
   file2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("inelastic-subtracted-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);

   TList * fg1s = new TList();
   TList * fg2s = new TList();
   TList * fd2s = new TList();
   TList * fWAxes = new TList();
   Double_t Waxis_x1[5] = {0.2  , 0.3   , 0.4 , 0.5 , 0.2};
   Double_t Waxis_x2[5] = {0.35  , 0.5   , 0.8 , 0.9 , 0.97};
   Double_t Waxis_y1[5] = {0.052 , 0.06 , 0.07 , 0.08 , 0.09};

   Double_t E0 = 5.9;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;
   TGraph       * gr1 = 0;
   InSANEStructureFunctions * SFs = new F1F209StructureFunctions();

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mgd2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.89, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries->GetEntries();jj++) {

      TH1F * fA1 = (TH1F*) fA1Asymmetries->At(jj);
      TH1F * fA2 = (TH1F*) fA2Asymmetries->At(jj);

      TH1F * fg1 = new TH1F(*fA1);
      fg1->Reset();
      fg1s->Add(fg1);
      TH1F * fg2 = new TH1F(*fA2);
      fg2->Reset();
      fg2s->Add(fg2);
      TH1F * fd2 = new TH1F(*fA2);
      fd2->Reset();
      fd2s->Add(fd2);

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));

      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));

      Double_t Q2_bin_avg = 1.0;//(paraAsym->GetQ2() + paraAsym->GetQ2())/2.0;

      InSANEAveragedKinematics1D * paraKine = &paraAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsx; 

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);
               Double_t W1     = paraKine->fW.GetBinContent(bin);
               Double_t x1     = paraKine->fx.GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi.GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2.GetBinContent(bin);
               Double_t W     = W1;//
               Double_t x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t phi   = phi1;//0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
               Double_t Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);


               Double_t A1   = fA1->GetBinContent(bin);
               Double_t A2   = fA2->GetBinContent(bin);

               Double_t eA1  = fA1->GetBinError(bin);
               Double_t eA2  = fA2->GetBinError(bin);

               Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               Double_t R        = SFs->R(x,Q2);
               Double_t F1       = SFs->F1p(x,Q2);
               Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               Double_t gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
               Double_t gamma    = TMath::Sqrt(gamma2);
               Double_t cota     = 1.0/TMath::Tan(alpha);
               Double_t csca     = 1.0/TMath::Sin(alpha);

               Double_t g1 = 2.0*x*x*(F1/(1.0+gamma2))*(A1 + gamma*A2);
               Double_t g2 = 3.0*x*x*(F1/(1.0+gamma2))*(A2/gamma - A1);

               Double_t d2p = (g1+g2);

               //eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
               Double_t a1 = 2.0*x*x*(F1/(1.0+gamma2));
               Double_t a2 = 2.0*x*x*(F1/(1.0+gamma2))*gamma;
               Double_t eg1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //eg2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
               a1 = -3.0*x*x*(F1/(1.0+gamma2));
               a2 = 3.0*x*x*(F1/(1.0+gamma2))/gamma;
               Double_t eg2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               Double_t ed2p = 1.0*TMath::Sqrt( eg1*eg1 + eg2*eg2); 
               //c0 = 1.0/(1.0 + eta*xi);
               //c11 = (1.0 + cota*chi)/D ;
               //c12 = (csca*chi)/D ;
               //c21 = (xi - cota*chi/eta)/D ;
               //c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               fg1->SetBinContent(bin,g1);
               fg2->SetBinContent(bin,g2);
               fg1->SetBinError(bin, eg1);
               fg2->SetBinError(bin, eg2);

               fd2->SetBinContent(bin,d2p);
               fd2->SetBinError(bin, ed2p);
            }
         }
      }

      // -------------
      // g2 

      TH1 * h1 = fg2; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         Double_t ey = gr->GetErrorY(j);
         gr->GetPoint(j,xt,yt);
         if( ey > 0.05 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(21);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<=4) {
         mg2->Add(gr,"p");
      }


      // -------------
      // g1
      h1 = fg1; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.05 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<=4) {
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      // create the W axes
      Double_t Waxis_W1 = InSANE::Kine::W_xQsq( Waxis_x2[jj%5], Q2_bin_avg );
      Double_t Waxis_W2 = InSANE::Kine::W_xQsq( Waxis_x1[jj%5], Q2_bin_avg );
      TF1      * WaxisFunc      = new TF1(Form("WaxisFunc%d",jj),Form("InSANE::Kine::xBjorken_WQsq(x,%f)",Q2_bin_avg),Waxis_W1,Waxis_W2);
      TGaxis   * Waxis_1        = new TGaxis(Waxis_x1[jj%5], Waxis_y1[jj%5], Waxis_x2[jj%5], Waxis_y1[jj%5], Form("WaxisFunc%d",jj), 5, "-");
      Waxis_1->SetTitle("");
      Waxis_1->CenterTitle(true);
      Waxis_1->SetLabelOffset(0.01);
      Waxis_1->SetLabelColor(fA1->GetMarkerColor());
      Waxis_1->SetLineColor(fA1->GetMarkerColor());
      fWAxes->Add(Waxis_1);

      // -------------
      // d2
      h1 = fd2; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.3 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<=4) {
         mgd2->Add(gr,"p");
      }


   }

   f->WriteObject(fg1s,Form("x2g1-59-x-%d",0));//,TObject::kSingleKey); 
   f->WriteObject(fg2s,Form("x2g2-59-x-%d",0));//,TObject::kSingleKey); 
   f->WriteObject(fd2s,Form("d2-59-x-%d",0));//,TObject::kSingleKey); 


   // --------------------------------------------------------------------------
   // Functions

   // Function to take g2p --> x^2 g2p
   TF2 * xf = new TF2("xf","2.0*x*x*y",0,1,-1,1);
   TF2 * xf2 = new TF2("xf2","3.0*x*x*y",0,1,-1,1);
   TF2 * f2 = new TF2("f2","2.0*y",0,1,-1,1);
   TF2 * f3 = new TF2("f3","3.0*y",0,1,-1,1);

   Double_t Q2 = 4.0;

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);

   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  

   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 
   Double_t yAxisMax = 0.07;
   Double_t yAxisMin =-0.07;

   TF1 * x2g1p = new TF1("x2g1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");

   TF1 * xg2pWW = new TF1("x2g2pWW", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWA = new TF1("x2g2pWWA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWB = new TF1("x2g2pWWB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWC = new TF1("x2g2pWWC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");

   x2g1p->SetParameter(0,Q2);
   xg2pWW->SetParameter(0,Q2);
   xg2pWWA->SetParameter(0,Q2);
   xg2pWWB->SetParameter(0,Q2);
   xg2pWWC->SetParameter(0,Q2);

   xg2pWW->SetLineColor(1);
   xg2pWWA->SetLineColor(3);
   xg2pWWB->SetLineColor(kOrange -1);
   xg2pWWC->SetLineColor(kViolet-2);

   Int_t width = 3;
   //xg2pWW->SetLineWidth(width);
   //xg2pWWA->SetLineWidth(width);
   //xg2pWWB->SetLineWidth(width);
   //xg2pWWC->SetLineWidth(width);

   xg2pWW->SetLineStyle(1);
   xg2pWWA->SetLineStyle(1);
   xg2pWWB->SetLineStyle(1);
   xg2pWWC->SetLineStyle(1);

   xg2pWW->SetMaximum(yAxisMax);
   xg2pWW->SetMinimum(yAxisMin);

   xg2pWW->GetXaxis()->SetTitle("x");
   xg2pWW->GetXaxis()->CenterTitle();

   // -------------------------------------------------------
   // x2g1p 


   c->cd(1);
   gPad->SetGridy(true);

   // Load in world data on A1He3 from NucDB  

   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(4);
   Qsqbin->SetTitle(Form("%4.2f <Q^{2}<%4.2f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));
   leg->SetHeader(Form("World data %s",Qsqbin->GetTitle()));

   TMultiGraph *MG = new TMultiGraph(); 
   TList * measurementsList =  new TList(); //manager->GetMeasurements("A1p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("g1p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("g1p");
   if(ames) measurementsList->Add(ames);
   //leg->SetFillColor(kWhite); 

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("g1p");
   if(ames) measurementsList->Add(ames);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement * measg1 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = measg1->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg1->BuildGraph("x");
      graph->Apply(xf);
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",measg1->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
   }

   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("2x^{2}g_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.03,0.1); 
   
   // Models
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   TGraph * gr_x2g1p  = new TGraph( x2g1p->DrawCopy("goff")->GetHistogram());
   gr_x2g1p->Apply(f2);
   //gr_x2g1p->Draw("l");
   MG->Add(gr_x2g1p,"l");

   leg->Draw();
   MG->Draw("AP");
   for(int i = 0; i<fWAxes->GetEntries(); i++ ) {
      TGaxis * ax = (TGaxis*)fWAxes->At(i); 
      ax->Draw();
      //ax->Dump();
   }
   c->Update();


   // -------------------------------------------------------
   // x2g2p 

   c->cd(2);
   gPad->SetGridy(true);
   TLegend *leg2 = new TLegend(0.89, 0.15, 0.99, 0.85);
   TMultiGraph *MG2 = new TMultiGraph(); 
   NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   measurementsList->Clear();
   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("g2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("g2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("g2p");
   if(ames) measurementsList->Add(ames);

   //measurementsList = manager->GetMeasurements("g2p");
   //measurementsList->Print();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *measg2 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = measg2->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      //A2pMeas->PrintData();
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg2->BuildGraph("x");
      graph->Apply(xf2);
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",measg2->GetExperimentName()); 
      leg2->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 
   }

   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary 3x^{2}g_{2}^{p}, E=5.9 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   MG2->GetYaxis()->SetRangeUser(-0.2,0.15); 
   MG2->GetYaxis()->SetTitle("3x^{2}g_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();

   // Plot models
   TGraph * gr_x2g2p = new TGraph( xg2pWW->DrawCopy("goff")->GetHistogram());
   gr_x2g2p->Apply(f3);
   //gr_x2g2p->Draw("l");
   MG2->Add(gr_x2g2p,"l");
   //xg2pWWA->Draw("same");
   //xg2pWWB->Draw("same");
   //xg2pWWC->Draw("same");

   MG2->Draw("a");
   leg2->Draw();
   c->Update();

   c->SaveAs(Form("results/asymmetries/x2g1g2_59_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/x2g1g2_59_%d.png",aNumber));

   // -----------------------------------------------------------------------
   // d2p

   TCanvas * c2 = new TCanvas();

   gPad->SetGridy(true);
   mgd2->Add(gr_x2g1p,"l");
   mgd2->Add(gr_x2g2p,"l");
   mgd2->Draw("a");

   TH1F * h1_d2p = new TH1F( *((TH1F*)x2g1p->DrawCopy("goff")->GetHistogram()) ) ;
   h1_d2p->Reset();
   TH1 * h1_x2g1p = x2g1p->DrawCopy("goff")->GetHistogram();
   h1_x2g1p->Scale(2.0);
   TH1 * h1_x2g2p = xg2pWW->DrawCopy("goff")->GetHistogram();
   h1_x2g2p->Scale(3.0);

   h1_d2p->Add(h1_x2g1p);
   h1_d2p->Add(h1_x2g2p);

   TGraph * gr_d2p = new TGraph( h1_d2p );
   mgd2->Add(gr_d2p,"l");

   mgd2->GetXaxis()->SetLimits(0.0,1.0); 
   mgd2->GetYaxis()->SetRangeUser(-0.15,0.15); 
   mgd2->Draw("a");
   //h1_d2p->Draw("same");

   c2->Update();

   c2->SaveAs(Form("results/asymmetries/x2g1g2_59_d2_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1g2_59_d2_%d.png",aNumber));

   return(0);
}
