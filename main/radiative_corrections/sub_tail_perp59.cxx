/*! Subtracts the elastic tail.
 *
 */
Int_t sub_tail_perp59(Int_t perpRunGroup = 8209,Int_t aNumber=8209){

   Double_t theta_target = 180.0*degree;
   Double_t fBeamEnergy  = 5.9;
   TString  filename     = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpRunGroup);

   // --------------------------------------------------

   TFile * f1 = TFile::Open(filename,"UPDATE");
   f1->cd();
   TList * fAsymmetries = (TList *) gROOT->FindObject(Form("background-subtracted-asym_perp59-%d",0));
   if(!fAsymmetries) return(-1);

   TList * RC_list = (TList *) gROOT->FindObject(Form("elastic-RCs_perp59-%d",0));//,TObject::kSingleKey); 
   if(!RC_list) return -2;

   TList * asym_write = new TList();

   std::string table_dir0 = "results/asymmetries/tables/";
   table_dir0            += std::to_string(aNumber);
   table_dir0            += "/el_sub";
   table_dir0            += "/A80_59_el_sub/";
   gSystem->mkdir(table_dir0.c_str(),true);

   for(int jj = 0;jj<fAsymmetries->GetEntries() ;jj++) {

      // Get the measured asymmetry 
      //InSANEAveragedMeasuredAsymmetry * asymAverage = (InSANEAveragedMeasuredAsymmetry*)(fAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry * asym                = asymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)(fAsymmetries->At(jj));
      asym_write->Add(asym);

      Int_t   xBinmax = 0;
      Int_t   yBinmax = 0;
      Int_t   zBinmax = 0;
      Int_t   bin     = 0;
      TH1F * fA                             = 0;
      TH1F * fA_syst                        = 0;
      InSANEAveragedKinematics1D * avgKine  = 0;
      InSANERadiativeCorrections1D * RCs    = 0;

      // -------------------------------------------------
      // Asymmetry vs x
      fA      = &asym->fAsymmetryVsx;
      avgKine = &asym->fAvgKineVsx;
      fA_syst = &asym->fSystErrVsx;

      RCs     = (InSANERadiativeCorrections1D*)RC_list->FindObject(Form("rc-x-%d",jj));

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over x bins
      //
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);
         Double_t eASyst    = asym->fSystErrVsx.GetBinContent(bin);

         Double_t sigma_in       = 2.0*RCs->fSigmaIRT->GetBinContent(bin);
         Double_t sigma_in_plus  = RCs->fSigmaIRT_Plus->GetBinContent(bin);
         Double_t sigma_in_minus = RCs->fSigmaIRT_Minus->GetBinContent(bin);
         if(sigma_in == 0) {
           sigma_in = 0.000001;
         }
         Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
         Double_t sigma_el_plus  = RCs->fSigmaERT_Plus->GetBinContent(bin);
         Double_t sigma_el_minus = RCs->fSigmaERT_Minus->GetBinContent(bin);
         Double_t sigma_el       = 2.0*RCs->fSigmaERT->GetBinContent(bin);
         Double_t C_el           = (sigma_el_plus - sigma_el_minus)/(sigma_in);
         Double_t f_el           =  sigma_in/(sigma_in + sigma_el);
         Double_t Sigma_tot      = (sigma_in + sigma_el);
         Double_t delta_el       = (sigma_el_plus - sigma_el_minus);
         if(f_el == 0) {
           f_el = 0.000001;
         }



         // Systematic errors
         Double_t eSigma_in      = 0.01*sigma_in;
         Double_t eSigma_el      = 0.01*sigma_in;
         Double_t eDelta_el      = 0.01*(sigma_el_plus - sigma_el_minus);


         // FIX BACKGROUND
         //Double_t Ep             = avgKine->fE->GetBinContent(bin);
         //Double_t f_bg           = 4.122*TMath::Exp(-2.442*Ep);
         //Double_t C_back         = (1.0 /*- f_bg*0.05/A*/)/(1.0 - f_bg);
         Double_t A_corr         = A/f_el - C_el;
         Double_t delta_A        = A - A_calc;

         if( TMath::IsNaN(A_calc) ) A_calc = 0;
         if(sigma_el == 0) continue;
         //std::cout << " Ep         = " <<  Ep   << std::endl;
         //std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         //std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         //std::cout << " sigma_el   = " <<  sigma_el << std::endl;
         //std::cout << " A_el       = " <<  (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus) << std::endl;
         //std::cout << " C_el       = " <<  C_el   << std::endl;
         //std::cout << " f_el       = " <<  f_el   << std::endl;
         //std::cout << " C_back     = " <<  C_back   << std::endl;
         //std::cout << " f_bg       = " <<  f_bg   << std::endl;
         //std::cout << " A_raw      = " <<  A << std::endl; 
         //std::cout << " A_corr     = " <<  A_corr << std::endl; 

         fA->SetBinContent(bin,A_corr);

         // Systematic error
         Double_t eSystBG  = eASyst*eASyst*Sigma_tot*Sigma_tot/(sigma_in*sigma_in);
         Double_t num      = TMath::Power(eSigma_in*(delta_el-A*sigma_el),2.0);
         num              += TMath::Power(eDelta_el*sigma_in,2.0);
         num              += TMath::Power(eSigma_el*A*sigma_in,2.0);
         eSystBG          += num/TMath::Power(sigma_in,4.0);
         asym->fSystErrVsx.SetBinContent(bin,TMath::Sqrt(eSystBG));

      }

      // -------------------------------------------------
      // Asymmetry vs W
      fA      = &asym->fAsymmetryVsW;
      avgKine = &asym->fAvgKineVsW;
      fA_syst = &asym->fSystErrVsW;

      // -------------------------------------------------
      // Add new systematics
      InSANESystematic * fel_systematic = asym->AddSystematic_W(*fA,"f_el");
      InSANESystematic * Cel_systematic = asym->AddSystematic_W(*fA,"C_el");
      fel_systematic->fBefore = *fA_syst;
      Cel_systematic->fBefore = *fA_syst;

      RCs     = (InSANERadiativeCorrections1D*)RC_list->FindObject(Form("rc-W-%d",jj));

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over W bins
      //
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " -------------------------------------" << std::endl;
         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);
         Double_t eASyst    = fA_syst->GetBinContent(bin);

         Double_t sigma_in       = 2.0*RCs->fSigmaIRT->GetBinContent(bin);
         Double_t sigma_in_plus  = RCs->fSigmaIRT_Plus->GetBinContent(bin);
         Double_t sigma_in_minus = RCs->fSigmaIRT_Minus->GetBinContent(bin);
         if(sigma_in == 0) {
           sigma_in = 0.000001;
         }
         Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
         Double_t sigma_el_plus  = RCs->fSigmaERT_Plus->GetBinContent(bin);
         Double_t sigma_el_minus = RCs->fSigmaERT_Minus->GetBinContent(bin);
         Double_t sigma_el       = 2.0*RCs->fSigmaERT->GetBinContent(bin);
         Double_t C_el           = (sigma_el_plus - sigma_el_minus)/(sigma_in);
         Double_t f_el           =  sigma_in/(sigma_in + sigma_el);
         Double_t Sigma_tot      = (sigma_in + sigma_el);
         Double_t delta_el       = (sigma_el_plus - sigma_el_minus);
         if(f_el == 0) {
           f_el = 0.000001;
         }

         // Systematic errors
         Double_t eSigma_in      = 0.01*sigma_in;
         Double_t eSigma_el      = 0.01*sigma_in;
         Double_t eDelta_el      = 0.01*(sigma_el_plus - sigma_el_minus);

         using namespace TMath;
         double   delta_f_el2    = (Power(eSigma_in*sigma_el,2.0) + Power(eSigma_el*sigma_in,2.0))/Power(sigma_el + sigma_in,4.0);
         double   delta_C_el2    = (Power(eSigma_in*delta_el,2.0) + Power(eDelta_el*sigma_in,2.0))/Power(sigma_in,4.0);

         double  eSyst_fel_only = Sqrt(A*A*delta_f_el2/(f_el*f_el));
         double  eSyst_Cel_only = Sqrt(delta_C_el2);

         // FIX BACKGROUND
         //Double_t Ep             = avgKine->fE->GetBinContent(bin);
         //Double_t f_bg           =  4.122*TMath::Exp(-2.442*Ep);
         //Double_t C_back         = (1.0 /*- f_bg*0.05/A*/)/(1.0 - f_bg);
         Double_t A_corr         = A/f_el - C_el;
         Double_t delta_A        = A - A_calc;

         if( TMath::IsNaN(A_calc) ) A_calc = 0;
         //if(sigma_el == 0) continue;
         //std::cout << " Ep         = " <<  Ep   << std::endl;
         //std::cout << " sigma_in   = " <<  sigma_in << std::endl;
         //std::cout << " A_in_calc  = " <<  A_calc << std::endl;
         //std::cout << " sigma_el   = " <<  sigma_el << std::endl;
         //std::cout << " A_el       = " <<  (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus) << std::endl;
         //std::cout << " C_el       = " <<  C_el   << std::endl;
         //std::cout << " f_el       = " <<  f_el   << std::endl;
         //std::cout << " C_back     = " <<  C_back   << std::endl;
         //std::cout << " f_bg       = " <<  f_bg   << std::endl;
         //std::cout << " A_raw      = " <<  A << std::endl; 
         //std::cout << " A_corr     = " <<  A_corr << std::endl; 

         fA->SetBinContent(bin,A_corr);
         fA->SetBinError(bin,  eA/f_el);

         // Systematic error
         Double_t eSystBG  = eASyst*eASyst*Sigma_tot*Sigma_tot/(sigma_in*sigma_in);
         Double_t num      = TMath::Power(eSigma_in*(delta_el-A*sigma_el),2.0);
         num              += TMath::Power(eDelta_el*sigma_in,2.0);
         num              += TMath::Power(eSigma_el*A*sigma_in,2.0);
         eSystBG          += num/TMath::Power(sigma_in,4.0);
         asym->fSystErrVsW.SetBinContent(bin,TMath::Sqrt(eSystBG));

         fel_systematic->fAfter.SetBinContent( bin, Sqrt(eSystBG) );
         fel_systematic->fValues.SetBinContent(bin, f_el );
         fel_systematic->fUncertainties.SetBinContent(bin, Sqrt( delta_f_el2 ) );

         Cel_systematic->fAfter.SetBinContent( bin, Sqrt(eSystBG) );
         Cel_systematic->fValues.SetBinContent(bin, C_el );
         Cel_systematic->fUncertainties.SetBinContent(bin, Sqrt( delta_C_el2 ) );
         
         if(A != 0) {
           fel_systematic->fRelativeUncertainties.SetBinContent(bin, eSyst_fel_only/TMath::Abs(A) );
           Cel_systematic->fRelativeUncertainties.SetBinContent(bin, eSyst_Cel_only/TMath::Abs(A) );
         } else {
           fel_systematic->fRelativeUncertainties.SetBinContent(bin, eSyst_fel_only/TMath::Abs(1.0e-12) );
           Cel_systematic->fRelativeUncertainties.SetBinContent(bin, eSyst_Cel_only/TMath::Abs(1.0e-12) );
         }


      }

      std::string   filename0       = Form("A80_59_el_sub_%d_%d.txt",aNumber,jj);
      std::string   table_filename0 = table_dir0 + filename0;
      std::ofstream outtable0(table_filename0.c_str());

      asym->CalculateBinMask_W();
      asym->PrintBinnedTableHead(outtable0);
      asym->PrintBinnedTable(outtable0);


   }

   f1->WriteObject(asym_write,Form("elastic-subtracted-asym_perp59-%d",0));//,TObject::kSingleKey); 
   //f1->WriteObject(RC_write,Form("elastic-RCs_perp59-%d",0));//,TObject::kSingleKey); 
 
   return(0);
}
