Int_t tables_A180A80_59(Int_t paraRunGroup = 23,Int_t perpRunGroup = 23,Int_t aNumber=23){

   Int_t paraFileVersion          = paraRunGroup;
   Int_t perpFileVersion          = perpRunGroup;

   Double_t E0                    = 5.9;
   Double_t alpha                 = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman   = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   const TString weh("A1A2_59_noel()");
   if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
      Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
      return -1;
   }
   sane_data_bins();
   
   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);

   TFile * fout = new TFile(Form("data/A1A2_59_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");

   TList        * fA1Asymmetries = new TList();
   TList        * fA2Asymmetries = new TList();
   TList        * fF1_SFs        = new TList();

   TList        * fAllA1Asym = new TList();
   TList        * fAllA2Asym = new TList();

   TH1F         * fA1            = 0;
   TH1F         * fA2            = 0;
   TH1F         * fF1            = 0;

   TGraphErrors * gr             = 0;

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));

      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));

      InSANEMeasuredAsymmetry * fA1Full = new InSANEMeasuredAsymmetry( *paraAsym );
      fAllA1Asym->Add(fA1Full);
      InSANEMeasuredAsymmetry * fA2Full = new InSANEMeasuredAsymmetry( *paraAsym );
      fAllA2Asym->Add(fA2Full);

      TH1F * fApara = paraAsym->fAsymmetryVsx;
      TH1F * fAperp = perpAsym->fAsymmetryVsx;

      std::ofstream outtable1(Form("results/asymmetries/tables/A180_el_sub_59_%d_%d.txt",jj,aNumber));
      std::ofstream outtable2(Form("results/asymmetries/tables/A80_el_sub_59_%d_%d.txt",jj,aNumber));

      InSANEAveragedKinematics1D * paraKine = paraAsym->fAvgKineVsx;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsx; 
      InSANEAveragedKinematics1D avgKine = (*paraKine);
      avgKine                  += (*perpKine) ;
      (*(fA1Full->fAvgKineVsx)) = avgKine;
      (*(fA2Full->fAvgKineVsx)) = avgKine;

      fA1 = new TH1F(*fApara);
      fA2 = new TH1F(*fAperp);
      fF1 = new TH1F(*fApara);

      fA1->SetNameTitle(Form("fA1-%d",jj),Form("A1-%d",jj));
      fA2->SetNameTitle(Form("fA2-%d",jj),Form("A2-%d",jj));
      fF1->SetNameTitle(Form("fF1-%d",jj),Form("F1-%d",jj));

      fA1Asymmetries->Add(fA1);
      fA2Asymmetries->Add(fA2);
      fF1_SFs->Add(fF1);

      paraAsym->PrintBinnedTableHead(outtable1);
      paraAsym->PrintBinnedTable(outtable1);

      perpAsym->PrintBinnedTableHead(outtable2);
      perpAsym->PrintBinnedTable(outtable2);

   }

   return(0);
}
