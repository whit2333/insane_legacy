/*! Subtracts the inelastic tail.
 */
Int_t sub_inel_tail_para47(Int_t paraRunGroup = 8209,Int_t aNumber=8209){

   Double_t theta_target = 180.0*degree;
   Double_t fBeamEnergy  = 4.7;
   TString  filename     = Form("data/bg_corrected_asymmetries_para47_%d.root",paraRunGroup);

   // --------------------------------------------------

   TFile * f1 = TFile::Open(filename,"UPDATE");
   f1->cd();
   TList * fAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para47-%d",0));
   if(!fAsymmetries) return(-1);

   TList * RC_list = (TList *) gROOT->FindObject(Form("elastic-RCs_para47-%d",0));//,TObject::kSingleKey); 

   TList * asym_write = new TList();

   std::string table_dir0 = "results/asymmetries/tables/";
   table_dir0            += std::to_string(aNumber);
   table_dir0            += "/inel_sub";
   table_dir0            += "/A180_47_inel_sub/";
   gSystem->mkdir(table_dir0.c_str(),true);

   for(int jj = 0;jj<fAsymmetries->GetEntries() ;jj++) {

      // Get the measured asymmetry 
      InSANEMeasuredAsymmetry * asym = (InSANEMeasuredAsymmetry*)(fAsymmetries->At(jj));
      asym_write->Add(asym);

      Int_t   xBinmax = 0;
      Int_t   yBinmax = 0;
      Int_t   zBinmax = 0;
      Int_t   bin     = 0;

      TH1F * fA                             = 0;
      InSANEAveragedKinematics1D * avgKine  = 0;
      InSANERadiativeCorrections1D * RCs    = 0;

      // -------------------------------------------------
      // Asymmetry vs x
      fA      = &asym->fAsymmetryVsx;
      avgKine = &asym->fAvgKineVsx;

      RCs     = (InSANERadiativeCorrections1D*)RC_list->FindObject(Form("rc-x-%d",jj));

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over x bins
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);
         Double_t eASyst    = asym->fSystErrVsx.GetBinContent(bin);

         Double_t Aborn     = RCs->fAsymBorn->GetBinContent(bin);
         Double_t AIRT      = RCs->fAsymIRT->GetBinContent(bin);
         Double_t A_corr    = AIRT - Aborn;

         //if( TMath::IsNaN(A_calc) ) A_calc = 0;

         fA->SetBinContent(bin,A-A_corr);

         // TODO : SYSTEMATIC ERROR
         //// Systematic error
         //Double_t eSystBG  = eASyst*eASyst*Sigma_tot*Sigma_tot/(sigma_in*sigma_in);
         //Double_t num      = TMath::Power(eSigma_in*(delta_el-A*sigma_el),2.0);
         //num += TMath::Power(eDelta_el*sigma_in,2.0);
         //num += TMath::Power(eSigma_el*A*sigma_in,2.0);
         //eSystBG          += num/TMath::Power(sigma_in,4.0);
         //asym->fSystErrVsx->SetBinContent(bin,TMath::Sqrt(eSystBG));

      }

      // -------------------------------------------------
      // Asymmetry vs W
      fA      = &asym->fAsymmetryVsW;
      avgKine = &asym->fAvgKineVsW;

      RCs     = (InSANERadiativeCorrections1D*)RC_list->FindObject(Form("rc-W-%d",jj));

      xBinmax = fA->GetNbinsX();
      yBinmax = fA->GetNbinsY();
      zBinmax = fA->GetNbinsZ();
      bin     = 0;

      // -------------------------------------------------
      // Loop over W bins
      for(Int_t i=0; i<= xBinmax; i++){
         bin   = fA->GetBin(i);

         //std::cout << " Bin " << bin << "/" << xBinmax << std::endl; 

         Double_t A         = fA->GetBinContent(bin);
         Double_t eA        = fA->GetBinError(bin);
         Double_t eASyst    = asym->fSystErrVsW.GetBinContent(bin);

         Double_t Aborn     = RCs->fAsymBorn->GetBinContent(bin);
         Double_t AIRT      = RCs->fAsymIRT->GetBinContent(bin);
         Double_t A_corr    = AIRT - Aborn;

         //if( TMath::IsNaN(A_calc) ) A_calc = 0;

         fA->SetBinContent(bin, A-A_corr);

         // Systematic error
         //Double_t eSystBG  = eASyst*eASyst*Sigma_tot*Sigma_tot/(sigma_in*sigma_in);
         //Double_t num      = TMath::Power(eSigma_in*(delta_el-A*sigma_el),2.0);
         //num += TMath::Power(eDelta_el*sigma_in,2.0);
         //num += TMath::Power(eSigma_el*A*sigma_in,2.0);
         //eSystBG          += num/TMath::Power(sigma_in,4.0);
         //asym->fSystErrVsW->SetBinContent(bin,TMath::Sqrt(eSystBG));

      }

      std::string   filename0       = Form("A180_47_inel_sub_%d_%d.txt",aNumber,jj);
      std::string   table_filename0 = table_dir0 + filename0;
      std::ofstream outtable0(table_filename0.c_str());

      asym->PrintBinnedTableHead(outtable0);
      asym->PrintBinnedTable(outtable0);

   }

   f1->WriteObject(asym_write,Form("inelastic-subtracted-asym_para47-%d",0));//,TObject::kSingleKey); 
 
   return(0);
}
