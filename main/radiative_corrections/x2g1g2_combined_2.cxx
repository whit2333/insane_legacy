#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

//int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

Int_t x2g1g2_combined_2(Int_t paraFileVersion = 3206,Int_t perpFileVersion = 3206, 
                        Int_t para47Version = 3206, Int_t perp47Version = 3206,Int_t aNumber=3206)
{

   // ------------------------
   // Maximum allowed errors for data points in graphs 
   Double_t ey1_max =  0.02;  
   Double_t ey2_max =  0.04;  

   sane_data_bins();


   TFile * f = new TFile(Form("data/g1g2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"READ");
   f->ls();
   TList * fg1Asymmetries = (TList*)gROOT->FindObject(Form("x2g1-combined-x-%d",0));
   TList * fg2Asymmetries = (TList*)gROOT->FindObject(Form("x2g2-combined-x-%d",0));
   if(!fg1Asymmetries) return -23;
   if(!fg2Asymmetries) return -22;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();

   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();
   TMultiGraph * mg4 = new TMultiGraph();

   TMultiGraph * mg_x2g1 = new TMultiGraph();
   TMultiGraph * mg_x2g2 = new TMultiGraph();

   TMultiGraph * mg_syst_err = new TMultiGraph();
   TMultiGraph * mg_syst_err2 = new TMultiGraph();
   TMultiGraph * mg_syst_err3= new TMultiGraph();
   TMultiGraph * mg_syst_err4 = new TMultiGraph();

   TF2 * div2 = new TF2("xf","y/2.0",0,1,-1,1);
   TF2 * div3 = new TF2("xf","y/3.0",0,1,-1,1);

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   TLegend *leg_sane = new TLegend(0.65, 0.65, 0.89, 0.89);
   leg_sane->SetFillColor(0);
   leg_sane->SetFillStyle(0);
   leg_sane->SetBorderSize(0);

   // Function to take g2p --> x^2 g2p
   TF2 * xf = new TF2("xf","2.0*x*x*y",0,1,-1,1);
   TF2 * xf2 = new TF2("xf2","3.0*x*x*y",0,1,-1,1);
   TF2 * f2 = new TF2("f2","2.0*y",0,1,-1,1);
   TF2 * f3 = new TF2("f3","3.0*y",0,1,-1,1);

   TF2 * fTimes2 = new TF2("ftimes2","2.0*y",0,1,-1,1);
   TF2 * fTimes3 = new TF2("ftimes3","3.0*y",0,1,-1,1);
   TF2 * x2f = new TF2("x2f","x*x*y",0,1,-1,1);
   TList        * fAllx2g1Asym = new TList();
   TList        * fAllx2g2Asym = new TList();


   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0; jj<fg1Asymmetries->GetEntries(); jj++) {

      InSANEMeasuredAsymmetry   * fg1_asym  = (InSANEMeasuredAsymmetry*)fg1Asymmetries->At(jj);
      InSANEMeasuredAsymmetry   * fg2_asym  = (InSANEMeasuredAsymmetry*)fg2Asymmetries->At(jj);

      fg1_asym->Dump();
   std::cout << "Derp\n";
      InSANEMeasuredAsymmetry * fx2g1_asym = new InSANEMeasuredAsymmetry( *fg1_asym );
      InSANEMeasuredAsymmetry * fx2g2_asym = new InSANEMeasuredAsymmetry( *fg2_asym );
      fAllx2g1Asym->Add(fx2g1_asym);
      fAllx2g2Asym->Add(fx2g2_asym);

      InSANEAveragedKinematics1D * avg_kine = &(fg1_asym->fAvgKineVsx);

      TH1F * fg1 = &(fg1_asym->fAsymmetryVsx);
      TH1F * fg2 = &(fg2_asym->fAsymmetryVsx);

      //TH1F * fg1 = new TH1F(*fg1_combined);
      //TH1F * fg2 = new TH1F(*fg1_combined);
      //fg1->Reset();
      //fg2->Reset();

      TH1F * fx2g1 = &fx2g1_asym->fAsymmetryVsx;
      TH1F * fx2g2 = &fx2g2_asym->fAsymmetryVsx;

      Int_t   xBinmax = fg1->GetNbinsX();
      Int_t   yBinmax = fg1->GetNbinsY();
      Int_t   zBinmax = fg1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      //std::cout << " Graph " << jj << std::endl;
      // ----------------------------------------------------------
      // Sanitize the y values by removing NaNs
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fg1->GetBin(i,j,k);

               Double_t g1   = fg1->GetBinContent(bin);
               Double_t g2   = fg2->GetBinContent(bin);

               if( TMath::IsNaN(g1)  || std::isinf(g1) ) fg1->SetBinContent(bin,0.0);
               if( TMath::IsNaN(g2)  || std::isinf(g2) ) fg2->SetBinContent(bin,0.0);
            }
         }
      }
      // Create graph with zeros removed
      TGraph * gr_mean      = 0;
      gr_mean               = new TGraph(fg1);
      for( int j = gr_mean->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr_mean->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr_mean->RemovePoint(j);
            continue;
         }
      }
      Double_t g1_59_mean = gr_mean->GetMean(2);
      Double_t g1_59_rms  = gr_mean->GetRMS(2);
      //std::cout << "A1_59_mean = " << A1_59_mean << std::endl;
      //std::cout << "A1_59_rms  = " << A1_59_rms << std::endl;

      gr_mean               = new TGraph(fg2);
      for( int j = gr_mean->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr_mean->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            gr_mean->RemovePoint(j);
            continue;
         }
      }
      Double_t g2_59_mean = gr_mean->GetMean(2);
      Double_t g2_59_rms  = gr_mean->GetRMS(2);
      //std::cout << "A2_59_mean = " << A2_59_mean << std::endl;

      // -----------------------------------------------------------
      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){

               bin   = fg1->GetBin(i,j,k);

               Double_t g1      = fg1->GetBinContent(bin);
               Double_t g2      = fg2->GetBinContent(bin);
               Double_t eg1     = fg1->GetBinError(bin);
               Double_t eg2     = fg2->GetBinError(bin);
               Double_t eg1Syst = fg1_asym->fSystErrVsx.GetBinContent(bin);
               Double_t eg2Syst = fg2_asym->fSystErrVsx.GetBinContent(bin);

               Double_t x       = avg_kine->fx.GetBinContent(bin);

               Double_t x2g1      = x*x*g1;
               Double_t x2g2      = x*x*g2;
               Double_t ex2g1     = x*x*eg1;
               Double_t ex2g2     = x*x*eg2;
               Double_t ex2g1Syst = x*x*eg1;
               Double_t ex2g2Syst = x*x*eg2;

               fx2g1->SetBinContent(bin,x2g1);
               fx2g2->SetBinContent(bin,x2g2);
               fx2g1->SetBinError(bin, ex2g1);
               fx2g2->SetBinError(bin, ex2g2);
               fx2g1_asym->fSystErrVsx.SetBinContent(bin,ex2g1Syst);
               fx2g2_asym->fSystErrVsx.SetBinContent(bin,ex2g2Syst);

            }
         }
      }

      // ----------------------------
      // g1
      TH1 * hrebinned = fx2g1->Rebin(1,"fx2g1Rebinned");
      if(hrebinned){
         hrebinned->Scale(1.0);
         gr = new TGraphErrors(hrebinned);
      } else {
         gr = new TGraphErrors(fx2g1);
      }
      //  syst
      fx2g1_asym->fSystErrVsx.SetFillColor(fg1->GetMarkerColor());
      fx2g1_asym->fSystErrVsx.SetLineColor(fg1->GetMarkerColor());
      TGraphErrors * grSyst = nullptr;
      hrebinned = nullptr;//fx2g1_asym->fSystErrVsx->Rebin(1,"fx2g1RebinnedSyst");
      if(hrebinned){
         hrebinned->Scale(1.0);
         //grSyst = new TGraphErrors(hrebinned);
         grSyst = stat_error_graph(hrebinned,-0.02);
      } else {
         grSyst = stat_error_graph(&fx2g1_asym->fSystErrVsx,-0.02);
      }
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = fx2g1_asym->fSystErrVsx.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         Double_t ey = gr->GetErrorY(j);
         if( ey > ey1_max ) {
            gr->RemovePoint(j);
            grSyst->RemovePoint(j);
            fx2g1_asym->fSystErrVsx.SetBinContent(abin,0.0);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            grSyst->RemovePoint(j);
            fx2g1_asym->fSystErrVsx.SetBinContent(abin,0.0);
            continue;
         }
         if( xt < 0.2 ) {
            gr->RemovePoint(j);
            grSyst->RemovePoint(j);
            fx2g1_asym->fSystErrVsx.SetBinContent(abin,0.0);
            continue;
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(1.3);
      gr->SetMarkerColor(fg1->GetMarkerColor());
      gr->SetLineColor(fg1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         TGraphErrors * grtemp = new TGraphErrors(*gr);
         //grtemp->Apply(div2);
         if(jj!=4 && jj != 0 ){
            mg_x2g1->Add(grtemp,"p");
            gr->Apply(f2);
            mg->Add(gr,"p");
         }
      }
      // Systematic errors
      if(jj<4 && jj!=0 ) {
         //mg_syst_err->Add(grSyst,"e3");
         mg_syst_err->Add(stat_error_graph(&fx2g1_asym->fSystErrVsx,-0.02),"e3");
      }


      // ----------------------------
      // g2
      
      hrebinned = fx2g2->Rebin(1,"fx2g2Rebinned");
      if(hrebinned){
         hrebinned->Scale(1.0);
         gr = new TGraphErrors(hrebinned);
      } else {
         gr = new TGraphErrors(fx2g2);
      }
      //  syst
      fx2g2_asym->fSystErrVsx.SetFillColor(fg2->GetMarkerColor());
      fx2g2_asym->fSystErrVsx.SetLineColor(fg2->GetMarkerColor());
      hrebinned = 0;//fx2g2_asym->fSystErrVsx->Rebin(2,"fx2g2RebinnedSyst");
      if(hrebinned){
         hrebinned->Scale(0.5);
         //grSyst = new TGraphErrors(hrebinned);
         grSyst = stat_error_graph(hrebinned,-0.1);
      } else {
         grSyst = stat_error_graph(&fx2g2_asym->fSystErrVsx,-0.1);
      }
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Int_t    abin    = fx2g2_asym->fSystErrVsx.FindBin(xt);
         Double_t eyt     = gr->GetErrorY(j);
         //Double_t eyt_sys = paraAsym->fSystErrVsx.GetBinContent(abin);
         //jif( yt == 0.0 || yt<0.0 || eyt_sys>1.0 || eyt>1.0 ) {
         //j   gr->RemovePoint(j);
         //j   paraAsym->fSystErrVsx.SetBinContent(abin,0.0);
         //j   paraAsym->fSystErrVsx.SetBinError(abin,0.0);
         //j   continue;
         //j}
         Double_t ey = gr->GetErrorY(j);
         if( ey > ey2_max ) {
            gr->RemovePoint(j);
            grSyst->RemovePoint(j);
            fx2g2_asym->fSystErrVsx.SetBinContent(abin,0.0);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            grSyst->RemovePoint(j);
            fx2g2_asym->fSystErrVsx.SetBinContent(abin,0.0);
            continue;
         }
         if( xt < 0.2 ) {
            gr->RemovePoint(j);
            grSyst->RemovePoint(j);
            fx2g2_asym->fSystErrVsx.SetBinContent(abin,0.0);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(1.3);
      gr->SetMarkerColor(fg1->GetMarkerColor());
      gr->SetLineColor(fg1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         TGraphErrors * grtemp = new TGraphErrors(*gr);
         //grtemp->Apply(div3);
         if(jj!=4 && jj != 0){
            mg_x2g2->Add(grtemp,"p");
            gr->Apply(f3);
            mg2->Add(gr,"p");
            leg->AddEntry(gr,     Form("#LTQ^{2}#GT=%.1f GeV^{2}",fg1_asym->GetQ2()),"lp");
            leg_sane->AddEntry(gr,Form("#LTQ^{2}#GT=%.1f GeV^{2}",fg1_asym->GetQ2()),"lp");
         }
      }
      // Systematic errors
      if(jj<4 && jj!=0 ) {
         mg_syst_err2->Add(stat_error_graph(&fx2g2_asym->fSystErrVsx,-0.1),"e3");
      }


   }

   TFile * fout = new TFile(Form("data/x2g1g2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   fout->WriteObject(fAllx2g1Asym,Form("x2g1-combined-x-%d",0));//,TObject::kSingleKey); 
   fout->WriteObject(fAllx2g2Asym,Form("x2g2-combined-x-%d",0));//,TObject::kSingleKey); 
   //fout->WriteObject(fA1combined,Form("x2g1-combined-x-%d",0));//,TObject::kSingleKey); 
   //fout->WriteObject(fA2combined,Form("x2g2-combined-x-%d",0));//,TObject::kSingleKey); 
   //fout->WriteObject(fF1combined,Form("F1-combined-x-%d",0));//,TObject::kSingleKey); 
   fout->Close();

   // --------------------------------------------------------------------------
   // Functions

   Double_t Q2 = 4.0;


   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);

   Int_t    npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t yAxisMax = 0.07;
   Double_t yAxisMin =-0.07;

   TF1 * x2g1p = new TF1("x2g1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");

   TF1 * xg2pWW = new TF1("x2g2pWW", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");

   x2g1p->SetParameter(0,Q2);
   xg2pWW->SetParameter(0,Q2);

   xg2pWW->SetLineColor(1);
   xg2pWW->SetLineStyle(1);
   //xg2pWW->SetLineWidth(3);

   xg2pWW->SetMaximum(yAxisMax);
   xg2pWW->SetMinimum(yAxisMin);

   xg2pWW->GetXaxis()->SetTitle("x");
   xg2pWW->GetXaxis()->CenterTitle();

   // -------------------------------------------------------
   // x2g1p 
   Int_t width = 1;

   TLegend * leg0 = new TLegend(0.16, 0.60, 0.40, 0.88);
   leg0->SetFillColor(0);
   leg0->SetFillStyle(0);
   leg0->SetBorderSize(0);

   NucDBManager * manager = NucDBManager::GetManager();

   NucDBBinnedVariable * Wbin          = new NucDBBinnedVariable("W","W",20.0,18.0);
   NucDBBinnedVariable * Qsqbin        = new NucDBBinnedVariable("Qsquared","Q^{2}");
   NucDBMeasurement    * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   Qsqbin->SetBinMinimum(1.5);
   Qsqbin->SetBinMaximum(8.5);
   Qsqbin->SetMean((1.5+8.5)/2.0);
   Qsqbin->SetAverage((1.5+8.5)/2.0);
   Qsqbin->SetTitle(Form("%.1f<Q^{2}<%.1f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));

   TMultiGraph * MG         = new TMultiGraph();
   TMultiGraph * MG_data1   = new TMultiGraph();
   TMultiGraph * mg_CLAS_g1 = new TMultiGraph();

   TList            * measurementsList = manager->GetMeasurements("g1p");
   NucDBExperiment  * exp              = 0;
   NucDBMeasurement * ames             = 0;

   Int_t markers[] = {22,32,23,33,21,25,26,27};

   for (int i = 0; i < measurementsList->GetEntries(); i++) {

      NucDBMeasurement * measg1 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;

      plist = measg1->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg1->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      if(!strcmp(measg1->GetExperimentName(),"SANE") ){
         continue;
      }
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg1->BuildGraph("x");
      graph->Apply(x2f);
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);
      graph->SetMarkerSize(1.0);
      // Set legend title 
      Title = Form("%s",measg1->GetExperimentName()); 
      leg->AddEntry(graph,Title,"p");
      leg0->AddEntry(graph,Title,"p");
      if(!strcmp(measg1->GetExperimentName(),"CLAS") ){
         mg_CLAS_g1->Add(graph,"p");
         continue;
      }

      // Add to TMultiGraph 
      MG_data1->Add(graph,"p"); 
      TGraphErrors * graph2 = new TGraphErrors(*graph);
      graph2->Apply(fTimes2);
      MG->Add(graph2,"p"); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }
   //TGraphErrors *graph        = measg1_saneQ2->BuildGraph("x");
   //graph->Apply(xf);
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   //leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   //MG->Add(graph,"p");
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");


   // -------------------------------------------------------
   // x2g2p 

   TLegend *leg2  = new TLegend(0.84, 0.15, 0.99, 0.85);
   leg2->SetFillColor(0);
   leg2->SetFillStyle(0);
   leg2->SetBorderSize(0);
   TLegend *leg02 = new TLegend(0.15, 0.65, 0.45, 0.88);
   leg02->SetFillColor(0);
   leg02->SetFillStyle(0);
   leg02->SetBorderSize(0);
   TMultiGraph *MG2 = new TMultiGraph(); 

   NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   measurementsList->Clear();

   TMultiGraph *MG_data2 = new TMultiGraph(); 
   measurementsList =  manager->GetMeasurements("g2p");

   for (int i = 0; i < measurementsList->GetEntries(); i++) {

      NucDBMeasurement *measg2 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = 0;

      if(!strcmp(measg2->GetExperimentName(),"SANE") ){
         continue;
      }

      plist = measg2->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      plist = measg2->ApplyFilterWithBin(Wbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;

      // Get TGraphErrors object 
      TGraphErrors *graph        = measg2->BuildGraph("x");
      graph->Apply(x2f);
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerColor(1);
      graph->SetLineColor(1);

      leg2->AddEntry( graph,measg2->GetExperimentName(),"p");
      leg02->AddEntry(graph,measg2->GetExperimentName(),"p");

      MG_data2->Add(graph,"p"); 

      TGraphErrors * graph2 = new TGraphErrors(*graph);
      graph2->Apply(fTimes3);
      MG2->Add(graph2,"p"); 

   }

   // -------------------------------------------------------
   // Create graphs for ALL the models 
   TMultiGraph * mg_models = new TMultiGraph();
   std::vector<InSANEPolarizedStructureFunctions*> polSFlist;
   InSANEPolarizedStructureFunctions * polSF = 0;
   for(int i = 0; i< 8; i++ ){
      polSF = fman->CreatePolSFs(i);
      polSFlist.push_back(polSF);
   }
   polSF = fman->CreatePolSFs(12);
   polSFlist.push_back(polSF);

   Double_t model_Q2 = 2.0;
   for(int i = 0; i<polSFlist.size(); i++) {
      InSANEPolarizedStructureFunctions * psf = polSFlist[i];
      TF1 * func2  = psf->GetFunction2();
      func2->SetLineColor(2);
      func2->SetLineWidth(1);
      func2->SetParameter(0,model_Q2);
      TGraph * grfun = new TGraph(func2->DrawCopy("goff")->GetHistogram());
      //grfun->Apply(f3);
      mg_models->Add(grfun,"l");
   }
   model_Q2 = 5.0;
   for(int i = 0; i<polSFlist.size(); i++) {
      InSANEPolarizedStructureFunctions * psf = polSFlist[i];
      TF1 * func2  = psf->GetFunction2();
      func2->SetLineColor(4);
      func2->SetLineWidth(1);
      func2->SetParameter(0,model_Q2);
      TGraph * grfun = new TGraph(func2->DrawCopy("goff")->GetHistogram());
      //grfun->Apply(f3);
      mg_models->Add(grfun,"l");
   }

   // ------------------------------------------------------------
   // g1 and g2 on same canvas 
   TCanvas     * c       = new TCanvas();
   TMultiGraph * mg_temp = new TMultiGraph();

   c->Divide(1,2);
   c->cd(1);
   gPad->SetGridy(true);

   mg_temp->Add(MG);
   mg_temp->Add(mg);

   mg_temp->Draw("AP");
   mg_temp->SetTitle("");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->SetTitle("2x^{2}g_{1}^{p}");
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.03,0.1); 

   TGraph * gr_x2g1p  = new TGraph( x2g1p->DrawCopy("goff")->GetHistogram());
   gr_x2g1p->Apply(f2);
   //gr_x2g1p->Draw("l");
   mg_temp->Add(gr_x2g1p,"l");
   mg_temp->Draw("AP");
   leg->Draw();

   // ---------------------------
   //
   c->cd(2);
   mg_temp = new TMultiGraph();
   gPad->SetGridy(true);

   mg_temp->Add(MG2);
   mg_temp->Add(mg2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("");
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.2,0.15); 
   mg_temp->GetYaxis()->SetTitle("3x^{2}g_{2}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();

   // Plot models
   TGraph * gr_x2g2p = new TGraph( xg2pWW->DrawCopy("goff")->GetHistogram());
   gr_x2g2p->Apply(f3);
   ////gr_x2g2p->Draw("l");
   //MG2->Add(gr_x2g2p,"l");
   //MG2->Add(mg_models);

   mg_temp->Draw("a");
   leg2->Draw();

   c->Update();
   c->SaveAs(Form("results/asymmetries/x2g1g2_combined2_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/x2g1g2_combined2_%d.png",aNumber));

   // -----------------------------------------------------------------------
   // d2p
   TCanvas * cd2 = new TCanvas();

   gPad->SetGridy(true);
   mg3->Add(gr_x2g1p,"l");
   mg3->Add(gr_x2g2p,"l");
   mg3->Draw("a");

   TH1F * h1_d2p = new TH1F( *((TH1F*)x2g1p->DrawCopy("goff")->GetHistogram()) ) ;
   h1_d2p->Reset();
   TH1 * h1_x2g1p = x2g1p->DrawCopy("goff")->GetHistogram();
   h1_x2g1p->Scale(2.0);
   TH1 * h1_x2g2p = xg2pWW->DrawCopy("goff")->GetHistogram();
   h1_x2g2p->Scale(3.0);

   h1_d2p->Add(h1_x2g1p);
   h1_d2p->Add(h1_x2g2p);

   TGraph * gr_d2p = new TGraph( h1_d2p );
   gr_d2p->SetLineWidth(3);
   mg3->Add(gr_d2p,"l");

   mg3->GetXaxis()->SetLimits(0.0,1.0); 
   mg3->GetYaxis()->SetRangeUser(-0.15,0.15); 
   mg3->Draw("a");
   cd2->Update();
   cd2->SaveAs(Form("results/asymmetries/x2g1g2_combined2_d2_%d.png",aNumber));

   // ----------------------------------------------------------------
   // Models
   TLegend * legmod = new TLegend(0.40,0.7,0.60,0.88);
   legmod->SetFillColor(0);
   legmod->SetFillStyle(0);
   legmod->SetBorderSize(0);

   TList * list_of_models = new TList();
   TF1   * aModel         = 0;

   InSANEStructureFunctions          * sfs  = 0;
   InSANEPolarizedStructureFunctions * psfs = 0;

   // BBS
   sfs  = fman->CreateSFs(5);
   psfs = fman->CreatePolSFs(5);
   aModel = new TF1("xg1pbbs", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"BBS","l");

   // statistical
   sfs  = fman->CreateSFs(6);
   psfs = fman->CreatePolSFs(6);
   aModel = new TF1("xg1p", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"Stat 2015","l");

   // BB 
   psfs = fman->CreatePolSFs(3);
   aModel = new TF1("xg1pBB", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BB","l");


   // AAC
   psfs = fman->CreatePolSFs(2);
   aModel = new TF1("xg1paac", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"AAC","l");

   // LSS
   psfs = fman->CreatePolSFs(1);
   aModel = new TF1("xg1plss", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"LSS2006","l");

   // DSSV
   psfs = fman->CreatePolSFs(0);
   aModel = new TF1("xg1plssdssv", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"DSSV","l");

   TMultiGraph * mg_g1_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      TGraph * grmod = new TGraph( aModel->DrawCopy("goff")->GetHistogram());
      mg_g1_models->Add(grmod,"l");
   }

   // --------------------------------------------------
   // g2 Models
   legmod2 = new TLegend(0.51,0.65,0.68,0.88);
   legmod2->SetFillColor(0);
   legmod2->SetFillStyle(0);
   legmod2->SetBorderSize(0);

   //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   list_of_models = new TList();
   //TF1 * aModel = 0;


   // BBS
   psfs = fman->CreatePolSFs(5);
   aModel = new TF1("xg1pbbs", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"BBS","l");

   // statistical
   psfs = fman->CreatePolSFs(13);
   aModel = new TF1("xg1p", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"Stat 2015","l");

   // BB 
   psfs = fman->CreatePolSFs(3);
   aModel = new TF1("xg1pBB", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"BB","l");


   // AAC
   psfs = fman->CreatePolSFs(2);
   aModel = new TF1("xg1paac", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"AAC","l");

   // LSS
   psfs = fman->CreatePolSFs(1);
   aModel = new TF1("xg1plss", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"LSS2006","l");

   // DSSV
   psfs = fman->CreatePolSFs(0);
   aModel = new TF1("xg1plssdssv", psfs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               0.01, 0.98, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod2->AddEntry(aModel,"DSSV","l");

   TMultiGraph * mg_g2_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      TGraph * grmod = new TGraph( aModel->DrawCopy("goff")->GetHistogram());
      mg_g2_models->Add(grmod,"l");
   }




   //---------------------------------------------------------------
   // Nicer individual plots

   // ---------------------------------
   // g1 with data
   TCanvas * c2 = new TCanvas();

   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data1);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{1}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.03,0.08); 

   leg0->Draw();

   c2->SaveAs(Form("results/asymmetries/x2g1_existing_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1_existing_%d.pdf",aNumber));

   // ---------------------------------
   // world data + clas
   mg_temp->Add(mg_CLAS_g1);

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g1_existing_CLAS_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1_existing_CLAS_%d.pdf",aNumber));

   // ---------------------------------
   // world dat + models
   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data1);
   mg_temp->Add(mg_g1_models);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{1}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.03,0.08); 

   legmod->Draw();
   leg0->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g1_existing_models_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1_existing_models_%d.pdf",aNumber));


   // ---------------------------
   // world data + SANE data
   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data1);
   //mg_temp->Add(mg_g1_models);
   mg_temp->Add(mg_x2g1);
   mg_temp->Add(mg_syst_err);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{1}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.03,0.08); 

   leg0->Draw();
   leg_sane->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g1_combined2_sane_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1_combined2_sane_%d.pdf",aNumber));

   // ---------------------------
   // world data + SANE data + models

   mg_temp->Add(mg_g1_models);

   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g1_combined2_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1_combined2_%d.pdf",aNumber));





   // --------------------------------------------------------------
   // g2   Nicer individual plots
   c2 = new TCanvas();

   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{2}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.1,0.15); 

   leg02->Draw();

   c2->SaveAs(Form("results/asymmetries/x2g2_existing_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g2_existing_%d.pdf",aNumber));

   // ------------------------
   //
   mg_temp->Add(mg_g2_models);

   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g2_existing_models2_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g2_existing_models2_%d.pdf",aNumber));

   // -----------------------------
   // with SANE data
   mg_temp = new TMultiGraph();

   mg_temp->Add(MG_data2);
   mg_temp->Add(mg_x2g2);
   mg_temp->Add(mg_syst_err2);

   mg_temp->Draw("a");
   mg_temp->SetTitle("x^{2}g_{2}^{p}");
   mg_temp->GetXaxis()->SetTitle("x");
   mg_temp->GetYaxis()->SetTitle("");
   mg_temp->GetXaxis()->CenterTitle();
   mg_temp->GetYaxis()->CenterTitle();
   mg_temp->GetXaxis()->SetLimits(0.0,1.0); 
   mg_temp->GetYaxis()->SetRangeUser(-0.1,0.15); 

   leg02->Draw();
   leg_sane->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g2_combined2_sane_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g2_combined2_sane_%d.pdf",aNumber));

   // -----------------------------
   // with SANE data

   mg_temp->Add(mg_g2_models);

   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/x2g2_combined2_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g2_combined2_%d.pdf",aNumber));

   return(0);
}
