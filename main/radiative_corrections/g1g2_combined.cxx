#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

//int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

Int_t g1g2_combined(Int_t paraFileVersion = 6009, Int_t perpFileVersion = 6009,
              Int_t para47Version = 6009,   Int_t perp47Version = 6009, Int_t aNumber=6009){

   Double_t alpha = 80.0*TMath::Pi()/180.0;

   // ----------------------------------------------------------------
   //
   TFile * f_in_59     = new TFile(Form("data/g1g2_59_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fg1_59_list = (TList*)gROOT->FindObject(Form("g1asym-59-x-%d",0));
   TList * fg2_59_list = (TList*)gROOT->FindObject(Form("g2asym-59-x-%d",0));
   if(!fg1_59_list) return -11;
   if(!fg2_59_list) return -12;

   TFile * f_in_47     = new TFile(Form("data/g1g2_47_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fg1_47_list = (TList*)gROOT->FindObject(Form("g1asym-47-x-%d",0));
   TList * fg2_47_list = (TList*)gROOT->FindObject(Form("g2asym-47-x-%d",0));
   if(!fg1_47_list) return -13;
   if(!fg2_47_list) return -14;

   TList * fg1s = new TList();
   TList * fg2s = new TList();

   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   InSANEStructureFunctions * SFs = new F1F209StructureFunctions();

   // The asymmetry is calculated A = C1(A180*C2+A80*C3)
   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   TList        * fAllg1Asym = new TList();
   TList        * fAllg2Asym = new TList();

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0; jj<fg1_59_list->GetEntries(); jj++) {

      InSANEMeasuredAsymmetry   * fg1_asym_59  = (InSANEMeasuredAsymmetry*)(fg1_59_list->At(jj));
      InSANEMeasuredAsymmetry   * fg2_asym_59  = (InSANEMeasuredAsymmetry*)(fg2_59_list->At(jj));
      InSANEMeasuredAsymmetry   * fg1_asym_47  = (InSANEMeasuredAsymmetry*)(fg1_47_list->At(jj));
      InSANEMeasuredAsymmetry   * fg2_asym_47  = (InSANEMeasuredAsymmetry*)(fg2_47_list->At(jj));

   //   TH1F * fg1_59 = &fg1_asym_59->fAsymmetryVsx;
   //   TH1F * fg2_59 = &fg2_asym_59->fAsymmetryVsx;
   //   TH1F * fg1_47 = &fg1_asym_47->fAsymmetryVsx;
   //   TH1F * fg2_47 = &fg2_asym_47->fAsymmetryVsx;

   //   InSANEMeasuredAsymmetry * fg1Full = new InSANEMeasuredAsymmetry( *fg1_asym_59 );
   //   InSANEMeasuredAsymmetry * fg2Full = new InSANEMeasuredAsymmetry( *fg2_asym_59 );
   //   fAllg1Asym->Add(fg1Full);
   //   fAllg2Asym->Add(fg2Full);

   //   fg1 = new TH1F(*fg1_59);
   //   fg1->Reset();
   //   fg1s->Add(fg1);
   //   fg2 = new TH1F(*fg2_59);
   //   fg2->Reset();
   //   fg2s->Add(fg2);

   //   InSANEAveragedKinematics1D * Kine59 = &fg1_asym_59->fAvgKineVsx;
   //   InSANEAveragedKinematics1D * Kine47 = &fg1_asym_47->fAvgKineVsx; 

   //   Int_t   xBinmax = fg1->GetNbinsX();
   //   Int_t   yBinmax = fg1->GetNbinsY();
   //   Int_t   zBinmax = fg1->GetNbinsZ();
   //   Int_t   bin = 0;
   //   Int_t   binx,biny,binz;

   //   // ----------------------------------------------------------
   //   // Sanitize the y values by removing NaNs
   //   for(Int_t i=1; i<= xBinmax; i++){
   //      for(Int_t j=1; j<= yBinmax; j++){
   //         for(Int_t k=1; k<= zBinmax; k++){
   //            bin   = fg1_59->GetBin(i,j,k);

   //            Double_t g1_59   = fg1_59->GetBinContent(bin);
   //            Double_t g2_59   = fg2_59->GetBinContent(bin);
   //            Double_t g1_47   = fg1_47->GetBinContent(bin);
   //            Double_t g2_47   = fg2_47->GetBinContent(bin);

   //            if( TMath::IsNaN(g1_59)  || std::isinf(g1_59) ) fg1_59->SetBinContent(bin,0.0);
   //            if( TMath::IsNaN(g2_59)  || std::isinf(g2_59) ) fg2_59->SetBinContent(bin,0.0);
   //            if( TMath::IsNaN(g1_47)  || std::isinf(g1_47) ) fg1_47->SetBinContent(bin,0.0);
   //            if( TMath::IsNaN(g2_47)  || std::isinf(g2_47) ) fg2_47->SetBinContent(bin,0.0);
   //         }
   //      }
   //   }
   //   std::cout <<  "asadf\n";

   //   // now loop over bins to calculate the correct errors
   //   // the reason this error calculation looks complex is because of c2
   //   for(Int_t i=1; i<= xBinmax; i++){
   //      for(Int_t j=1; j<= yBinmax; j++){
   //         for(Int_t k=1; k<= zBinmax; k++){

   //            bin   = fg1->GetBin(i,j,k);
   //            fg1->GetBinXYZ(bin,binx,biny,binz);

   //            //Double_t W1     = Kine59->fW->GetBinContent(bin);
   //            //Double_t x1     = Kine59->fx->GetBinContent(bin);
   //            //Double_t phi1   = Kine59->fPhi->GetBinContent(bin);
   //            //Double_t Q21    = Kine59->fQ2->GetBinContent(bin);
   //            //Double_t W     = W1;//
   //            //Double_t x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
   //            //Double_t phi   = phi1;//0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
   //            //Double_t Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
   //            //Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
   //            //Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
   //            //Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

   //            //Double_t A1   = fA1->GetBinContent(bin);
   //            //Double_t A2   = fA2->GetBinContent(bin);

   //            //Double_t eA1  = fA1->GetBinError(bin);
   //            //Double_t eA2  = fA2->GetBinError(bin);

   //            //Double_t R_2      = InSANE::Kine::R1998(x,Q2);
   //            //Double_t R        = SFs->R(x,Q2);
   //            //Double_t F1       = SFs->F1p(x,Q2);
   //            //Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
   //            //Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
   //            //Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
   //            //Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
   //            //Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
   //            //Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
   //            //Double_t gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
   //            //Double_t gamma    = TMath::Sqrt(gamma2);
   //            //Double_t cota     = 1.0/TMath::Tan(alpha);
   //            //Double_t csca     = 1.0/TMath::Sin(alpha);

   //            //Double_t g1 = (F1/(1.0+gamma2))*(A1 + gamma*A2);
   //            //Double_t g2 = (F1/(1.0+gamma2))*(A2/gamma - A1);

   //            ////eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
   //            //Double_t a1 = (F1/(1.0+gamma2));
   //            //Double_t a2 = (F1/(1.0+gamma2))*gamma;
   //            //Double_t eg1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
   //            ////eg2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
   //            //a1 = -1.0*(F1/(1.0+gamma2));
   //            //a2 = (F1/(1.0+gamma2))/gamma;
   //            //Double_t eg2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));

   //            //fg1->SetBinContent(bin,g1);
   //            //fg2->SetBinContent(bin,g2);
   //            //fg1->SetBinError(bin, eg1);
   //            //fg2->SetBinError(bin, eg2);

   //            bool only47  = false;
   //            bool only59  = false;

   //            Double_t  g1_59  = fg1_59->GetBinContent(bin);
   //            Double_t  g2_59  = fg2_59->GetBinContent(bin);
   //            Double_t eg1_59  = fg1_59->GetBinError(bin);
   //            Double_t eg2_59  = fg2_59->GetBinError(bin);

   //            //Double_t x       = fg1_59->GetBinCenter(bin);

   //            Double_t  g1_47  = fg1_47->GetBinContent(bin);
   //            Double_t  g2_47  = fg2_47->GetBinContent(bin);
   //            Double_t eg1_47  = fg1_47->GetBinError(bin);
   //            Double_t eg2_47  = fg2_47->GetBinError(bin);

   //            Double_t g1      = 0.0;
   //            Double_t eg1     = 0.0;
   //            Double_t eg1Syst = 0.0;
   //            Double_t g2      = 0.0;
   //            Double_t eg2     = 0.0;
   //            Double_t eg2Syst = 0.0;


   //            //Double_t F1_47   = fF1_47->GetBinContent(bin);
   //            //Double_t F1_59   = fF1_59->GetBinContent(bin);
   //            //Double_t eF1_47   = fF1_47->GetBinError(bin);
   //            //Double_t eF1_59   = fF1_59->GetBinError(bin);
   //            //Double_t F1  = 0.0; 
   //            //Double_t eF1  = 0.0; 

   //            Double_t x2g1ErrorMax = 0.025;
   //            // Check the two results
   //            if( eg1_47 == 0.0 || TMath::IsNaN(eg1_47) ) {
   //               only59 = true;
   //            }
   //            if( eg1_59 == 0.0 || TMath::IsNaN(eg1_59) ) {
   //               only47 = true;
   //            }
   //            if( eg2_47 == 0.0 || TMath::IsNaN(eg2_47) ) {
   //               only59 = true;
   //            }
   //            if( eg2_59 == 0.0 || TMath::IsNaN(eg2_59) ) {
   //               only47 = true;
   //            }
   //            //if( 2.0*x*x*eg1_47 > x2g1ErrorMax  ) {
   //            //   only59 = true;
   //            //}
   //            //if( 2.0*x*x*eA1_59 > x2g1ErrorMax  ) {
   //            //   only47 = true;
   //            //}
   //            //if( 3.0*x*x*eA2_47 > 3.0*x2g1ErrorMax  ) {
   //            //   only59 = true;
   //            //}
   //            //if( 3.0*x*x*eA2_59 > 3.0*x2g1ErrorMax  ) {
   //            //   only47 = true;
   //            //}
   //            //if( TMath::Abs(2.0*x*x*(A1_47 - A1_47_mean))  > 1.5*2.0*x*x*A1_47_rms  ) {
   //            //   only59 = true;
   //            //}
   //            //if( TMath::Abs(2.0*x*x*(A1_59 - A1_59_mean))  > 1.5*2.0*x*x*A1_59_rms  ) {
   //            //   only47 = true;
   //            //}
   //            //if( TMath::Abs(3.0*x*x*(A2_47 - A2_47_mean))  > 1.5*3.0*x*x*A2_47_rms  ) {
   //            //   only59 = true;
   //            //}
   //            //if( TMath::Abs(3.0*x*x*(A2_59 - A2_59_mean))  > 1.5*3.0*x*x*A2_59_rms  ) {
   //            //   only47 = true;
   //            //}

   //            if( only59 && !only47 ) {
   //               g1  = g1_59;
   //               eg1 = eg1_59;
   //               g2  = g2_59;
   //               eg2 = eg2_59;
   //            } else if( only47 && !only59 ) {
   //               g1  = g1_47;
   //               eg1 = eg1_47;
   //               g2  = g2_47;
   //               eg2 = eg2_47;
   //            }
   //              
   //            //if( eg1_47 != 0.0 && eg1_59 != 0.0 && !(TMath::IsNaN(eg1_47)) && !(TMath::IsNaN(eg1_59)) ) {
   //            if( (!only47) && (!only59)  ) {
   //               g1 = (g1_59/(eg1_59*eg1_59) + g1_47/(eg1_47*eg1_47))/(1.0/(eg1_59*eg1_59) + 1.0/(eg1_47*eg1_47));
   //               eg1 = TMath::Sqrt((1.0)/(1.0/(eg1_59*eg1_59) + 1.0/(eg1_47*eg1_47)));

   //               g2 = (g2_59/(eg2_59*eg2_59) + g2_47/(eg2_47*eg2_47))/(1.0/(eg2_59*eg2_59) + 1.0/(eg2_47*eg2_47));
   //               eg2 = TMath::Sqrt((1.0)/(1.0/(eg2_59*eg2_59) + 1.0/(eg2_47*eg2_47)));
   //            }

   //            //if( eF1_47 == 0.0 || TMath::IsNaN(eF1_47) ) {
   //            //   F1  = F1_59;
   //            //   eF1 = eF1_59;
   //            //}
   //            //if( eF1_59 == 0.0 || TMath::IsNaN(eF1_59) ) {
   //            //   F1  = F1_47;
   //            //   eF1 = eF1_47;
   //            //}
   //            //if( eF1_47 != 0.0 && eF1_59 != 0.0 && !(TMath::IsNaN(eF1_47)) && !(TMath::IsNaN(eF1_59)) ) {

   //            //   F1 = (F1_59/(eF1_59*eF1_59) + F1_47/(eF1_47*eF1_47))/(1.0/(eF1_59*eF1_59) + 1.0/(eF1_47*eF1_47));
   //            //   eF1 = TMath::Sqrt((1.0)/(1.0/(eF1_59*eF1_59) + 1.0/(eF1_47*eF1_47)));
   //            //}

   //            //if( eA2_47 == 0.0 || TMath::IsNaN(eA2_47) ) {
   //            //   A2  = A2_59;
   //            //   eA2 = eA2_59;
   //            //}
   //            //if( eA2_59 == 0.0 || TMath::IsNaN(eA2_59) ) {
   //            //   A2  = A2_47;
   //            //   eA2 = eA2_47;
   //            //}
   //            //if( eA2_47 != 0.0 && eA2_59 != 0.0 && !(TMath::IsNaN(eA2_47)) && !(TMath::IsNaN(eA2_59)) ) {

   //            //}
   //            //std::cout << "-----------------------------------" << std::endl;
   //            //std::cout << F1_47 << " +- " << eF1_47 << std::endl;
   //            //std::cout << F1_59 << " +- " << eF1_59 << std::endl;
   //            //std::cout << F1 << " +- " << eF1 << std::endl;

   //            fg1->SetBinContent(bin,g1);
   //            fg2->SetBinContent(bin,g2);
   //            fg1->SetBinError(bin, eg1);
   //            fg2->SetBinError(bin, eg2);
   //            //fF1->SetBinContent(bin,F1);
   //            //fF1->SetBinError(bin, eF1);


   //         }
   //      }
   //   }

   //   fg1Full->fAsymmetryVsx = *fg1;
   //   fg2Full->fAsymmetryVsx = *fg2;

   //   //       asy->fAsymmetryVsx->SetTitle("A vs x");
   //   //       asy->fAsymmetryVsx->SetMinimum(-2.0);
   //   //       asy->fAsymmetryVsx->SetMaximum(2.0);
   //   //       if(j==0)asy->fAsymmetryVsx->Draw("E1");
   //   //       else asy->fAsymmetryVsx->Draw("same,E1");
   //   //       if(j<4)leg->AddEntry(asy->fAsymmetryVsx,Form("<Q^{2}>=%1.1f",QsqAvg[j]),"lp");
   //   //TH1D* fA1_y = fA1->ProjectionY(Form("%s_py",fA1->GetName()));
   //   TH1 * h1 = fg2; 
   //   gr = new TGraphErrors(h1);
   //   for( int j = gr->GetN()-1 ; j>=0; j--) {
   //      Double_t xt, yt;
   //      gr->GetPoint(j,xt,yt);
   //      if( yt == 0.0 ) {
   //         gr->RemovePoint(j);
   //      }
   //   }
   //   gr->SetMarkerStyle(21);
   //   gr->SetMarkerSize(0.6);
   //   gr->SetMarkerColor(fg1->GetMarkerColor());
   //   gr->SetLineColor(fg1->GetMarkerColor());
   //   if(jj<=4) {
   //   mg2->Add(gr,"p");
   //   }


   //   h1 = fg1; 
   //   gr = new TGraphErrors(h1);
   //   for( int j = gr->GetN()-1 ; j>=0; j--) {
   //      Double_t xt, yt;
   //      gr->GetPoint(j,xt,yt);
   //      if( yt == 0.0 ) {
   //         gr->RemovePoint(j);
   //      }
   //   }

   //   gr->SetMarkerStyle(20);
   //   gr->SetMarkerSize(0.6);
   //   gr->SetMarkerColor(fg1->GetMarkerColor());
   //   gr->SetLineColor(fg1->GetMarkerColor());
   //   if(jj<=4) {
   //   mg->Add(gr,"p");
   //   leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",fg2_asym_47->GetQ2()),"lp");
   //   }

   //   //if(jj==0) fA1_y->Draw("E1,p");
   //   //else fA1_y->Draw("same,E1,p");

   //   //c->cd(2);
   //   //TH1D* fA2_y = fA2->ProjectionY(Form("%s_py",fA2->GetName()));
   //   //fA2_y->SetMarkerStyle(20);
   //   //fA2_y->SetMarkerColor(1+jj);
   //   //if(jj==0) fA2_y->Draw();
   //   //else fA2_y->Draw("same");

   }

   //TFile * fout = new TFile(Form("data/g1g2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   //fout->WriteObject(fAllg1Asym,Form("g1asym-combined-x-%d",0));//,TObject::kSingleKey); 
   //fout->WriteObject(fAllg2Asym,Form("g2asym-combined-x-%d",0));//,TObject::kSingleKey); 


   //Q2 = 4.0;

   ////fout->WriteObject(fA1Asymmetries,Form("A1-para59-%d",paraRunGroup));//,TObject::kSingleKey); 
   ////fout->WriteObject(fA2Asymmetries,Form("A2-%d",paraRunGroup));//,TObject::kSingleKey); 

   //TString Title;
   //DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   //InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   //DSSVSF->SetPolarizedPDFs(DSSV);

   //AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   //InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   //AACSF->SetPolarizedPDFs(AAC);
   //
   //// Use CTEQ as the unpolarized PDF model 
   //CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   //InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   //CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   //// Use NMC95 as the unpolarized SF model 
   //NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   //F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   //// Asymmetry class: 
   //InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   //Asym1->SetPolarizedSFs(AACSF);
   //Asym1->SetUnpolarizedSFs(F1F209);  
   //// Asymmetry class: Use F1F209 
   //InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   //Asym2->SetPolarizedSFs(DSSVSF);
   //Asym2->SetUnpolarizedSFs(F1F209);  
   //// Asymmetry class: Use CTEQ  
   //InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   //Asym3->SetPolarizedSFs(DSSVSF);
   //Asym3->SetUnpolarizedSFs(CTEQSF);  
   //// Asymmetry class: Use CTEQ  
   //InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   //Asym4->SetPolarizedSFs(AACSF);
   //Asym4->SetUnpolarizedSFs(CTEQSF);  
   //Int_t npar = 1;
   //Double_t xmin = 0.01;
   //Double_t xmax = 1.00;
   //Double_t xmin1 = 0.25;
   //Double_t xmax1 = 0.75;
   //Double_t xmin2 = 0.25;
   //Double_t xmax2 = 0.75;
   //Double_t xmin3 = 0.1;
   //Double_t xmax3 = 0.9;
   //Double_t ymin = -1.0;
   //Double_t ymax =  1.0; 

   //// --------------- g1 ----------------------
   //c->cd(1);
   //gPad->SetGridy(true);
   //Int_t width = 1;

   //// Load in world data on g1 from NucDB  
   //NucDBManager * manager = NucDBManager::GetManager();

   //Double_t Q2_min = 1.5;
   //Double_t Q2_max = 7.0;
   //NucDBBinnedVariable * Qsqbin = 
   //   new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Q2_min,Q2_max));
   //Qsqbin->SetBinMinimum(Q2_min);
   //Qsqbin->SetBinMaximum(Q2_max);
   //NucDBMeasurement * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   //TMultiGraph *MG = new TMultiGraph(); 
   //TList * measurementsList =  new TList(); //manager->GetMeasurements("A1p");
   //NucDBExperiment *  exp = 0;
   //NucDBMeasurement * ames = 0; 

   //// SLAC E143
   //exp = manager->GetExperiment("SLAC_E143"); 
   //if(exp) ames = exp->GetMeasurement("g1p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155
   //exp = manager->GetExperiment("SLAC_E155"); 
   //if(exp) ames = exp->GetMeasurement("g1p");
   //if(ames) measurementsList->Add(ames);
   ////leg->SetFillColor(kWhite); 

   //for (int i = 0; i < measurementsList->GetEntries(); i++) {
   //   NucDBMeasurement *measg1 = (NucDBMeasurement*)measurementsList->At(i);
   //   // Get TGraphErrors object 
   //   TGraphErrors *graph        = measg1->BuildGraph("x");
   //   graph->SetMarkerStyle(27);
   //   // Set legend title 
   //   Title = Form("%s",measg1->GetExperimentName()); 
   //   leg->AddEntry(graph,Title,"lp");
   //   // Add to TMultiGraph 
   //   //MG->Add(graph); 
   //   measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   //}
   //TGraphErrors *graph        = measg1_saneQ2->BuildGraph("x");
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   //leg->AddEntry(graph,Form("SLAC in %s",Qsqbin->GetTitle()),"lp");
   //MG->Add(graph,"p");

   //TString Measurement = Form("g_{1}^{p}");
   //TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   //TString xAxisTitle  = Form("x");
   //TString yAxisTitle  = Form("%s",Measurement.Data());

   //MG->Add(mg);
   //// Draw everything 
   //MG->Draw("AP");
   //MG->SetTitle(GTitle);
   //MG->GetXaxis()->SetTitle(xAxisTitle);
   //MG->GetXaxis()->CenterTitle();
   //MG->GetYaxis()->SetTitle(yAxisTitle);
   //MG->GetYaxis()->CenterTitle();
   //MG->GetXaxis()->SetLimits(0.0,1.0); 
   //MG->GetYaxis()->SetRangeUser(-0.1,0.4); 
   //MG->Draw("AP");
   ////Model1->Draw("same");
   ////Model4->Draw("same");
   ////Model2->Draw("same");
   ////Model3->Draw("same");
   //leg->Draw();

   ////mg->Draw("same");
   ////mg->GetXaxis()->SetLimits(0.0,1.0);
   ////mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   ////mg->SetTitle("A_{1}, E=5.9 GeV"); 
   ////mg->GetXaxis()->SetTitle("x");
   ////mg->GetYaxis()->SetTitle("A_{1}");

   ////--------------- g2 ---------------------
   //c->cd(2);
   //gPad->SetGridy(true);
   //TMultiGraph *MG2 = new TMultiGraph(); 

   //NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   //measurementsList->Clear();
   //// SLAC E143
   //exp = manager->GetExperiment("SLAC_E143"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   //// SLAC E155
   //exp = manager->GetExperiment("SLAC_E155"); 
   //if(exp) ames = exp->GetMeasurement("g2p");
   //if(ames) measurementsList->Add(ames);

   //for (int i = 0; i < measurementsList->GetEntries(); i++) {
   //   NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
   //   //A2pMeas->PrintData();
   //   // Get TGraphErrors object 
   //   TGraphErrors *graph        = A2pMeas->BuildGraph("x");
   //   graph->SetMarkerStyle(27);
   //   // Set legend title 
   //   Title = Form("%s",A2pMeas->GetExperimentName()); 
   //   //leg->AddEntry(graph,Title,"lp");
   //   // Add to TMultiGraph 
   //   //MG2->Add(graph,"p"); 
   //   measg2_saneQ2->AddDataPoints(A2pMeas->FilterWithBin(Qsqbin));
   //}
   //graph        = measg2_saneQ2->BuildGraph("x");
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   ////leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   //MG2->Add(graph,"p");

   //MG2->Add(mg2,"p");
   //MG2->SetTitle("Preliminary g_{2}^{p}, E=5.9 GeV");
   //MG2->Draw("a");
   //MG2->GetXaxis()->SetLimits(0.0,1.0); 
   ////MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   //MG2->GetYaxis()->SetRangeUser(-0.3,0.3); 
   //MG2->GetYaxis()->SetTitle("g_{2}^{p}");
   //MG2->GetXaxis()->SetTitle("x");
   //MG2->GetXaxis()->CenterTitle();
   //MG2->GetYaxis()->CenterTitle();
   //c->Update();

   //c->SaveAs(Form("results/asymmetries/g1g2_combined_%d.pdf",aNumber));
   //c->SaveAs(Form("results/asymmetries/g1g2_combined_%d.png",aNumber));

   ////fout->WriteObject(fA1Asymmetries,Form("A1-59-%d",0));//,TObject::kSingleKey);
   ////fout->WriteObject(fA2Asymmetries,Form("A2-59-%d",0));//,TObject::kSingleKey);

   return(0);
}
