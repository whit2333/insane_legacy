Int_t MAID_pi0_asymmetry(Double_t th_pi=35.0, Double_t theta_target = 180.0, Double_t Ebeam=5.9  ){

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   Double_t beamEnergy                   = Ebeam;//5.9; //GeV
   Double_t theta_pi                     = th_pi*degree;
   Double_t phi_pi                       = 0.0*degree;
   InSANENucleus::NucleusType TargetType = InSANENucleus::kProton; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3
   TVector3 pt(0.0,0.0,1.0);
   pt.SetMagThetaPhi(1.0,theta_target*degree,0.0);

   // For plotting cross sections 
   Double_t Emin     = 0.1;
   Double_t Emax     = 2.0;
   Int_t    npar     = 2;
   Int_t    Npx      = 30;

   TLegend * leg = new TLegend(0.1,0.1,0.3,0.3);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("MAID cross sections");
   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);

   // ---------------------------------------
   // MAID unpolarized
   MAIDInclusivePionDiffXSec *fDiffXSec = new MAIDInclusivePionDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->fXSec0->SetBeamEnergy(beamEnergy);
   fDiffXSec->fXSec0->SetTargetType(TargetType);
   fDiffXSec->fXSec0->SetHelicity(0.0);
   fDiffXSec->fXSec0->SetTargetPolarization(pt);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();
   TF1 * sigma0 = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0->SetParameter(0,theta_pi);
   sigma0->SetParameter(1,phi_pi);
   sigma0->SetNpx(Npx);
   sigma0->SetLineColor(1);
   leg->AddEntry(sigma0,"unpolarized","l");

   // ---------------------------------------
   // MAID polarized
   MAIDInclusivePionDiffXSec *fDiffXSec1 = new MAIDInclusivePionDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetTargetType(TargetType);
   fDiffXSec1->fXSec0->SetBeamEnergy(beamEnergy);
   fDiffXSec1->fXSec0->SetTargetType(TargetType);
   fDiffXSec1->fXSec0->SetHelicity(1.0);
   fDiffXSec1->fXSec0->SetTargetPolarization(pt);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigma1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta_pi);
   sigma1->SetParameter(1,phi_pi);
   sigma1->SetNpx(Npx);
   sigma1->SetLineColor(2);
   leg->AddEntry(sigma1,"+ helicity","l");

   // ---------------------------------------
   // MAID polarized
   MAIDInclusivePionDiffXSec *fDiffXSec2 = new MAIDInclusivePionDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetType(TargetType);
   fDiffXSec2->fXSec0->SetBeamEnergy(beamEnergy);
   fDiffXSec2->fXSec0->SetTargetType(TargetType);
   fDiffXSec2->fXSec0->SetHelicity(-1.0);
   fDiffXSec2->fXSec0->SetTargetPolarization(pt);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigma2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta_pi);
   sigma2->SetParameter(1,phi_pi);
   sigma2->SetNpx(Npx);
   sigma2->SetLineColor(4);
   leg->AddEntry(sigma2,"- helicity","l");

   //-------------------------------------------------------

   TMultiGraph * mg1  = new TMultiGraph();
   TGraph * gr  = 0;

   TCanvas * c        = new TCanvas("MAIDInclusive","MAID Inclusive");
   c->Divide(1,2);

   c->cd(1);
   gPad->SetLogy(true);
   TString title      = fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "#omega_{#pi} (GeV)"; 

   TH1F * h2 = (TH1F*) sigma2->DrawCopy("goff")->GetHistogram();
   TH1F * h0 = (TH1F*) sigma0->DrawCopy("goff")->GetHistogram();
   TH1F * h1 = (TH1F*) sigma1->DrawCopy("goff")->GetHistogram();
   TH1F * hA     = (TH1F*)h0->Clone("hA");
   TH1F * hDenom = (TH1F*)h0->Clone("hDenom");   
   hA->Reset();
   hA->Add(h1,1.0);
   hA->Add(h2,-1.0);
   hDenom->Reset();
   hDenom->Add(h1,1.0);
   hDenom->Add(h2,1.0);
   hA->Divide(hDenom);

   gr = new TGraph(h0);
   mg1->Add(gr,"l");
   gr = new TGraph(h1);
   mg1->Add(gr,"l");
   gr = new TGraph(h2);
   mg1->Add(gr,"l");
   mg1->Draw("A");

   mg1->SetTitle("MAID inclusive #pi^{0} cross section");
   mg1->GetXaxis()->SetTitle(xAxisTitle);
   mg1->GetXaxis()->CenterTitle(true);
   mg1->GetYaxis()->SetTitle(yAxisTitle);
   mg1->GetYaxis()->CenterTitle(true);
   gPad->Update();
   //sigmaE->GetYaxis()->SetRangeUser(0.0,1.1*sigmaE->GetMaximum());
   //sigmaE->SetMinimum(0.0);

   leg->Draw();
   latex->DrawLatex(0.35,0.37,Form("E=%.2f",beamEnergy));
   latex->DrawLatex(0.35,0.3,Form("#theta_{Target}=%.0f",theta_target));
   latex->DrawLatex(0.35,0.23,Form("#theta_{#pi}=%.0f",th_pi));

   c->cd(2);

   gr = new TGraph(hA);
   gr->Draw("al");
   gr->SetTitle("#pi^{0} Asymmetry");
   gr->GetXaxis()->SetTitle(xAxisTitle);
   gr->GetXaxis()->CenterTitle(true);

   c->SaveAs(Form("results/RC/MAID_pi0_asymmetry_%d_%d_%d.png",int(beamEnergy*10.0),int(theta_target),int(th_pi)));
   c->SaveAs(Form("results/RC/MAID_pi0_asymmetry_%d_%d_%d.pdf",int(beamEnergy*10.0),int(theta_target),int(th_pi)));
   c->SaveAs(Form("results/RC/MAID_pi0_asymmetry_%d_%d_%d.tex",int(beamEnergy*10.0),int(theta_target),int(th_pi)));

   return 0;
}
