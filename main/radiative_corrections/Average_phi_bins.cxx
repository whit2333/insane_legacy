Int_t Average_phi_bins(TH2F * h2, TH1F * h1){

   TH1F * h1_x1 = new TH1F(*h1);
   h1_x1->SetNameTitle(Form("h1_x1"),Form("h1_x1"));
   h1_x1->Reset();
   TH1F *   h1_x2 = new TH1F(*h1);
   h1_x2->SetNameTitle(Form("h1_x2"),Form("h1_x2"));
   h1_x2->Reset();
   Int_t   xBinmax = h2->GetNbinsX();
   Int_t   yBinmax = h2->GetNbinsY();
   Int_t   zBinmax = h2->GetNbinsZ();
   Int_t   bin = 0;

   for(Int_t i=0; i<= xBinmax; i++){
      for(Int_t j=0; j<= yBinmax; j++){
         for(Int_t k=0; k<= zBinmax; k++){
            Double_t Ai = h2->GetBinContent(i,j,k);
            Double_t si = h2->GetBinError(i,j,k  );
            //std::cout << "Ai = " << Ai << std::endl;
            if( TMath::IsNaN(Ai) )          Ai = 0;
            if( si==0 || TMath::IsNaN(si) ) si = 0.000001;
            Double_t At = h1_x1->GetBinContent(i);
            Double_t st = h1_x2->GetBinContent(i);
            h1_x1->SetBinContent(i,At + Ai/(si*si) );
            h1_x2->SetBinContent(i,st + 1.0/(si*si) );
         }
      }
   }

   for(int i = 0; i<=h1_x1->GetNbinsX(); i++) {
      Int_t bin = h1_x1->GetBin(i);
      Double_t num   = h1_x1->GetBinContent(bin);
      Double_t denom = h1_x2->GetBinContent(bin);
      if( TMath::IsNaN(num) ) num = 0.0;
      if( TMath::IsNaN(denom) ) denom = 0.00001;
      if( num == TMath::Infinity() ) num = 0.0;
      if( denom == TMath::Infinity() ) denom = 0.000001;
      if( denom==0 ) {
         denom = 0.000001;
         h1->SetBinContent(bin,num/denom );
         h1->SetBinError(bin,0.0);
      } else {
         h1->SetBinContent(bin,num/denom );
         h1->SetBinError(bin,1.0/TMath::Sqrt(denom));
      }
   }

   return(0);
}
