#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

Int_t A1A2_47_W_noel(Int_t paraRunGroup = 7609,Int_t perpRunGroup = 7609,Int_t aNumber=7609){

   sane_data_bins();

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   InSANEStructureFunctions * SFs = fman->GetStructureFunctions();

   //gStyle->SetPadGridX(true);
   //gStyle->SetPadGridY(true);

   const char * parafile = Form("data/bg_corrected_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);

   TFile * fout = new TFile(Form("data/A1A2_47_noel_W_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fA1Asymmetries = new TList();
   TList * fA2Asymmetries = new TList();

   TList        * fAllA1Asym = new TList();
   TList        * fAllA2Asym = new TList();

   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries();jj++) {

      InSANEMeasuredAsymmetry * paraAsym = (InSANEMeasuredAsymmetry*)(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry * perpAsym = (InSANEMeasuredAsymmetry*)(fPerpAsymmetries->At(jj));

      TH1F * fApara = &paraAsym->fAsymmetryVsW;
      TH1F * fAperp = &perpAsym->fAsymmetryVsW;

      InSANEAveragedKinematics1D * paraKine = &paraAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsW; 

      TH1F * fA1 = new TH1F(*fApara);
      TH1F * fA2 = new TH1F(*fAperp);

      InSANEMeasuredAsymmetry * fA1Full = new InSANEMeasuredAsymmetry( *paraAsym );
      InSANEMeasuredAsymmetry * fA2Full = new InSANEMeasuredAsymmetry( *paraAsym );
      fAllA1Asym->Add(fA1Full);
      fAllA2Asym->Add(fA2Full);

      InSANEAveragedKinematics1D avgKine = (*paraKine);
      avgKine                  += (*perpKine) ;
      fA1Full->fAvgKineVsW = avgKine;
      fA2Full->fAvgKineVsW = avgKine;

      fA1->SetNameTitle(Form("fA1_W_%d",jj),Form("A1_%d",jj));
      fA2->SetNameTitle(Form("fA2_W_%d",jj),Form("A2_%d",jj));

      fA1Asymmetries->Add(fA1);
      fA2Asymmetries->Add(fA2);

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax; i++){
         for(Int_t j=0; j<= yBinmax; j++){
            for(Int_t k=0; k<= zBinmax; k++){

               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);

               Double_t W1     = paraKine->fW.GetBinContent(bin);
               Double_t x1     = paraKine->fx.GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi.GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2.GetBinContent(bin);
               Double_t W2     = perpKine->fW.GetBinContent(bin);
               Double_t x2     = perpKine->fx.GetBinContent(bin);
               Double_t phi2   = perpKine->fPhi.GetBinContent(bin);
               Double_t Q22    = perpKine->fQ2.GetBinContent(bin);
               Double_t W      = (W1+W2)/2.0;
               Double_t x      = (x1+x2)/2.0;
               Double_t phi    = (phi1+phi2)/2.0;
               Double_t Q2     = (Q21+Q22)/2.0;

               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               Double_t A180  = fApara->GetBinContent(bin);
               Double_t A80   = fAperp->GetBinContent(bin);

               Double_t eA180  = fApara->GetBinError(bin);
               Double_t eA80   = fAperp->GetBinError(bin);

               Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               Double_t R        = SFs->R(x,Q2);
               Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               Double_t cota     = 1.0/TMath::Tan(alpha);
               Double_t csca     = 1.0/TMath::Sin(alpha);

               Double_t c0 = 1.0/(1.0 + eta*xi);
               Double_t c11 = (1.0 + cota*chi)/D ;
               Double_t c12 = (csca*chi)/D ;
               Double_t c21 = (xi - cota*chi/eta)/D ;
               Double_t c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               //A1 = A180*(1/D);
               Double_t A1 = c0*(c11*A180 + c12*A80);
               Double_t A2 = c0*(c21*A180 + c22*A80);

               Double_t eA1 = TMath::Sqrt( TMath::Power(c0*c11*eA180,2.0) + TMath::Power(c0*c12*eA80,2.0));
               Double_t eA2 = TMath::Sqrt( TMath::Power(c0*c21*eA180,2.0) + TMath::Power(c0*c22*eA80,2.0));
               //eA2 = c0*(c21*eA180 + c22*eA80);
               //A1 = c0*(A180/D - eta*A80/d);
               //A2 = c0*(xi*A180/D + A80/d);

               if ( A1 != A1 ) A1 = 0;
               if ( A2 != A2 ) A2 = 0;

               //             std::cout << "theta = " << theta << "\n";
               //std::cout << "A1 = " << A1 << "\n";
               //std::cout << "A2 = " << A2 << "\n";
               //             
               fA1->SetBinContent(bin,A1);
               fA2->SetBinContent(bin,A2);
               fA1->SetBinError(bin,eA1);
               fA2->SetBinError(bin,eA2);

            }
         }
      }
      fA1Full->fAsymmetryVsW = *fA1;
      fA2Full->fAsymmetryVsW = *fA2;

      TH1 * h1 = fA1; 
      TGraphErrors * gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.8 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerSize(0.8);
      gr->SetMarkerStyle(20);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<4){
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      TH1 * h2 = fA2; 
      gr = new TGraphErrors(h2);
      for( int j = gr->GetN()-1 ;j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.8 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerSize(0.8);
      gr->SetMarkerStyle(21);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<4){
         mg2->Add(gr,"p");
      }

   }

   // -----------------------------------------------------------

   Double_t Q2 = 4.0;
   Double_t W_max = 4.0;

   //fout->WriteObject(fA1Asymmetries,Form("A1-para47-%d",paraRunGroup));//,TObject::kSingleKey); 
   fout->WriteObject(fA1Asymmetries,Form("A1-47-W-%d",0));
   fout->WriteObject(fA2Asymmetries,Form("A2-47-W-%d",0));

   fout->WriteObject(fAllA1Asym,Form("A1asym-47-W-%d",0));
   fout->WriteObject(fAllA2Asym,Form("A2asym-47-W-%d",0));


   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);

   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  
   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para47");
   c->Divide(1,2);

   // --------------- A1 ----------------------
   c->cd(1);
   TF1 * Model1 = new TF1("Model1",Asym1,&InSANEAsymmetryBase::EvaluateA1p,
         xmin1, xmax1, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model2 = new TF1("Model2",Asym2,&InSANEAsymmetryBase::EvaluateA1p,
         xmin2, xmax2, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model3 = new TF1("Model3",Asym3,&InSANEAsymmetryBase::EvaluateA1p,
         xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   TF1 * Model4= new TF1("Model4",Asym4,&InSANEAsymmetryBase::EvaluateA1p,
         xmin3, xmax3, npar,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");

   TF1 * ModelOscar = new TF1("ModelOscar","[0]+[1]/x+[2]*[3]",1.0,9.0);
   ModelOscar->SetParameter(0, -0.1565);
   ModelOscar->SetParameter(1,  1.488);
   ModelOscar->SetParameter(2,  0.0214);
   ModelOscar->SetParameter(3,  4.0);

   Int_t width = 1;

   Model1->SetParameter(0,Q2);
   Model1->SetLineColor(4);
   Model1->SetLineWidth(width);
   Model1->SetLineStyle(1);
   Model1->SetTitle(Title);
   Model1->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model4->SetParameter(0,Q2);
   Model4->SetLineColor(4);
   Model4->SetLineWidth(width);
   Model4->SetLineStyle(2);
   Model4->SetTitle(Title);
   Model4->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model2->SetParameter(0,Q2);
   Model2->SetLineColor(1);
   Model2->SetLineWidth(width);
   Model2->SetLineStyle(1);
   Model2->SetTitle(Title);
   Model2->GetYaxis()->SetRangeUser(ymin,ymax); 

   Model3->SetParameter(0,Q2);
   Model3->SetLineColor(1);
   Model3->SetLineWidth(width);
   Model3->SetLineStyle(2);
   Model3->SetTitle(Title);
   Model3->GetYaxis()->SetRangeUser(ymin,ymax); 

   ModelOscar->SetLineColor(1);
   ModelOscar->SetLineWidth(width);
   ModelOscar->SetLineStyle(2);
   ModelOscar->SetTitle(Title);

   // Load in world data on A1He3 from NucDB  
   gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 

   NucDBBinnedVariable * Qsqbin = 0;
   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(4); //new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Q2_min,Q2_max));
   Qsqbin->SetTitle(Form("%4.2f <Q^{2}<%4.2f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));
   leg->SetHeader(Form("World data %s",Qsqbin->GetTitle()));
   //NucDBMeasurement * measA1_saneQ2 = new NucDBMeasurement("measA1_saneQ2",Form("A_{1}^{p} %s",Qsqbin->GetTitle()));

   ModelOscar->SetParameter(3, Qsqbin->GetMean());

   TMultiGraph *MG = new TMultiGraph(); 

   TList * measurementsList =  new TList(); //manager->GetMeasurements("A1p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 
   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // CLAS-E93009 
   //exp = manager->GetExperiment("CLAS-E93009"); 
   //if(exp) ames = exp->GetMeasurement("A1p");
   //if(ames){
   //   measurementsList->Add(ames);
   //   ames->SetColor(kGreen-2);
   //}

   //measurementsList->Clear();
   //// RSS 
   //exp = manager->GetExperiment("RSS"); 
   //if(exp) ames = exp->GetMeasurement("A1p");
   //if(ames) measurementsList->Add(ames);

   //// CLAS-E93009 
   //exp = manager->GetExperiment("CLAS-E93009"); 
   //if(exp) ames = exp->GetMeasurement("A1p");
   //if(ames){
   ////   measurementsList->Add(ames);
   //   ames->SetColor(kGreen-2);
   //}
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1pMeas = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = A1pMeas->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1pMeas->BuildGraph("W");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A1pMeas->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
   }
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("A_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, E=4.7 GeV",Measurement.Data());
   TString xAxisTitle  = Form("W (GeV)");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(1.0,W_max); 
   MG->GetYaxis()->SetRangeUser(-1.0,1.0); 
   MG->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   ModelOscar->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=4.7 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   c->cd(2);
   TMultiGraph *MG2 = new TMultiGraph(); 
   TLegend *leg2 = new TLegend(0.84, 0.15, 0.99, 0.85);
   //measurementsList = manager->GetMeasurements("A2p");
   measurementsList->Clear();
   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);
   //// 
   //exp = manager->GetExperiment("RSS"); 
   //if(exp) ames = exp->GetMeasurement("A2p");
   //if(ames) measurementsList->Add(ames);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = A2pMeas->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = A2pMeas->BuildGraph("W");
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",A2pMeas->GetExperimentName()); 
      leg2->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 
   }
   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary A_{2}^{p}, E=4.7 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(1.0,W_max); 
   MG2->GetYaxis()->SetRangeUser(-1.0,1.0); 
   MG2->GetYaxis()->SetTitle("A_{2}^{p}");
   MG2->GetXaxis()->SetTitle("W (GeV)");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   leg2->Draw();
   c->Update();

   c->SaveAs(Form("results/asymmetries/A1A2_47_W_noel_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A1A2_47_W_noel_%d.png",aNumber));

   return(0);
}
