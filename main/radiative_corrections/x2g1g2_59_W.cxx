Int_t x2g1g2_59_W(Int_t paraFileVersion = 23,Int_t perpFileVersion = 23,Int_t aNumber=23){

   const TString weh("x2g1g2_59_noel()");
   if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
      Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
      return -1;
   }
   sane_data_bins();

   TFile * f = new TFile(Form("data/A1A2_59_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();

   TList * fA1Asymmetries = (TList*)gROOT->FindObject(Form("A1-59-W-%d",0));
   if(!fA1Asymmetries) return(-1);
   TList * fA2Asymmetries = (TList*)gROOT->FindObject(Form("A2-59-W-%d",0));
   if(!fA2Asymmetries) return(-2);

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp59-%d",0));
   if(!fPerpAsymmetries) return(-2);

   TList * fg1s = new TList();
   TList * fg2s = new TList();
   TList * fd2s = new TList();

   Double_t F1,eta,xi,chi,D,d,epsilon,R,cota,R_2;
   Double_t Q2,W,x,y,phi,E0,Ep,theta,alpha,gamma2,gamma;
   Double_t c0,c11,c12,c21,c22;
   Double_t A180,A80,A1,A2,eA1,eA2;
   Double_t g1,g2,eg1,eg2;
   E0 = 5.9;
   alpha = 80.0*TMath::Pi()/180.0;

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TH1F * fg1 = 0;
   TH1F * fg2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;
   //TH3F * fA1 = 0;
   //TH3F * fA2 = 0;
   InSANEStructureFunctions * SFs = new F1F209StructureFunctions();

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para59");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mgd2 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries->GetEntries();jj++) {

      fA1 = (TH1F*) fA1Asymmetries->At(jj);
      fA2 = (TH1F*) fA2Asymmetries->At(jj);

      fg1 = new TH1F(*fA1);
      fg1->Reset();
      fg1s->Add(fg1);
      fg2 = new TH1F(*fA2);
      fg2->Reset();
      fg2s->Add(fg2);
      fd2 = new TH1F(*fA2);
      fd2->Reset();
      fd2s->Add(fd2);

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));

      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));


      InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsW; 

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);
               Double_t W1     = paraKine->fW->GetBinContent(bin);
               Double_t x1     = paraKine->fx->GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi->GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2->GetBinContent(bin);
               W     = W1;//
               x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
               phi   = phi1;//0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
               Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);


               A1   = fA1->GetBinContent(bin);
               A2   = fA2->GetBinContent(bin);

               eA1  = fA1->GetBinError(bin);
               eA2  = fA2->GetBinError(bin);

               R_2      = InSANE::Kine::R1998(x,Q2);
               R        = SFs->R(x,Q2);
               F1       = SFs->F1p(x,Q2);
               D        = InSANE::Kine::D(E0,Ep,theta,R);
               d        = InSANE::Kine::d(E0,Ep,theta,R);
               eta      = InSANE::Kine::Eta(E0,Ep,theta);
               xi       = InSANE::Kine::Xi(E0,Ep,theta);
               chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
               gamma    = TMath::Sqrt(gamma2);
               cota     = 1.0/TMath::Tan(alpha);
               csca     = 1.0/TMath::Sin(alpha);

               g1 = x*x*(F1/(1.0+gamma2))*(A1 + gamma*A2);
               g2 = 1.5*x*x*(F1/(1.0+gamma2))*(A2/gamma - A1);

               Double_t d2p = 2.0*(g1+g2);

               //eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
               Double_t a1 = x*x*(F1/(1.0+gamma2));
               Double_t a2 = x*x*(F1/(1.0+gamma2))*gamma;
               eg1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //eg2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
               a1 = -1.5*x*x*(F1/(1.0+gamma2));
               a2 = 1.5*x*x*(F1/(1.0+gamma2))/gamma;
               eg2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               Double_t ed2p = 2.0*TMath::Sqrt( eg1*eg1 + eg2*eg2); 
               //c0 = 1.0/(1.0 + eta*xi);
               //c11 = (1.0 + cota*chi)/D ;
               //c12 = (csca*chi)/D ;
               //c21 = (xi - cota*chi/eta)/D ;
               //c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               //if( TMath::Abs(A80) > 0.0 ) {
               //   std::cout << "(E0,Ep,theta,phi) = (" << E0 << ","
               //                                        << Ep << ","
               //                                        << theta << ","
               //                                        << phi   << ")" << std::endl;  
               //   std::cout << " delta R      = " << R - R_2 << std::endl;
               //   std::cout << " A180         = " << A180 << std::endl;
               //   std::cout << " c0*c11 - 1/D = " << c0*c11 - 1.0/D << std::endl;
               //   std::cout << " D            = " << D << std::endl;
               //   std::cout << " 1/c11        = " << 1.0/c11 << std::endl;
               //   std::cout << " c0           = " << c0 << std::endl;
               //   std::cout << " c11          = " << c11 << std::endl;
               //   std::cout << " 1/D          = " << 1.0/D << std::endl;
               //   std::cout << " c12          = " << c12 << std::endl;
               //   std::cout << " -eta/d       = " << -eta/d << std::endl;
               //   std::cout << " c21          = " << c21 << std::endl;
               //   std::cout << " xi/D         = " <<  xi/D<< std::endl;
               //   std::cout << " c22          = " << c22 << std::endl;
               //   std::cout << " 1.0/d        = " << 1.0/d << std::endl;
               //   std::cout << " chi          = " << chi << std::endl;
               //   
               //}
               //             
               fg1->SetBinContent(bin,g1);
               fg2->SetBinContent(bin,g2);
               fg1->SetBinError(bin, eg1);
               fg2->SetBinError(bin, eg2);

               fd2->SetBinContent(bin,d2p);
               fd2->SetBinError(bin, ed2p);
            }
         }
      }

      TH1 * h1 = fg2; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         Double_t ey = gr->GetErrorY(j);
         gr->GetPoint(j,xt,yt);
         if( ey > 0.05 ) {
            gr->RemovePoint(j);
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }
      gr->SetMarkerStyle(21);
      gr->SetMarkerSize(0.6);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      mg2->Add(gr,"p");


      h1 = fg1; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.05 ) {
            gr->RemovePoint(j);
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.6);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      mg->Add(gr,"p");
      leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");

      h1 = fd2; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.3 ) {
            gr->RemovePoint(j);
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
         }
      }

      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.6);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      mgd2->Add(gr,"p");

      //if(jj==0) fA1_y->Draw("E1,p");
      //else fA1_y->Draw("same,E1,p");

      //c->cd(2);
      //TH1D* fA2_y = fA2->ProjectionY(Form("%s_py",fA2->GetName()));
      //fA2_y->SetMarkerStyle(20);
      //fA2_y->SetMarkerColor(1+jj);
      //if(jj==0) fA2_y->Draw();
      //else fA2_y->Draw("same");

   }


   /// Function to take g2p --> x^2 g2p
   TF2 * xf = new TF2("xf","x*x*y",0,1,-1,1);
   TF2 * xf2 = new TF2("xf","1.5*x*x*y",0,1,-1,1);

   Q2 = 4.0;

   //fout->WriteObject(fA1Asymmetries,Form("A1-para59-%d",paraRunGroup));//,TObject::kSingleKey); 
   //fout->WriteObject(fA2Asymmetries,Form("A2-%d",paraRunGroup));//,TObject::kSingleKey); 

   TString Title;
   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   DSSVSF->SetPolarizedPDFs(DSSV);

   AAC08PolarizedPDFs *AAC = new AAC08PolarizedPDFs();
   InSANEPolarizedStructureFunctionsFromPDFs *AACSF = new InSANEPolarizedStructureFunctionsFromPDFs();
   AACSF->SetPolarizedPDFs(AAC);

   // Use CTEQ as the unpolarized PDF model 
   CTEQ6UnpolarizedPDFs *CTEQ = new CTEQ6UnpolarizedPDFs();
   InSANEStructureFunctionsFromPDFs *CTEQSF = new InSANEStructureFunctionsFromPDFs(); 
   CTEQSF->SetUnpolarizedPDFs(CTEQ); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   // Asymmetry class: 
   InSANEAsymmetriesFromStructureFunctions *Asym1 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym1->SetPolarizedSFs(AACSF);
   Asym1->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use F1F209 
   InSANEAsymmetriesFromStructureFunctions *Asym2 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym2->SetPolarizedSFs(DSSVSF);
   Asym2->SetUnpolarizedSFs(F1F209);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym3 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym3->SetPolarizedSFs(DSSVSF);
   Asym3->SetUnpolarizedSFs(CTEQSF);  
   // Asymmetry class: Use CTEQ  
   InSANEAsymmetriesFromStructureFunctions *Asym4 = new InSANEAsymmetriesFromStructureFunctions(); 
   Asym4->SetPolarizedSFs(AACSF);
   Asym4->SetUnpolarizedSFs(CTEQSF);  
   Int_t npar = 1;
   Double_t xmin = 0.01;
   Double_t xmax = 1.00;
   Double_t xmin1 = 0.25;
   Double_t xmax1 = 0.75;
   Double_t xmin2 = 0.25;
   Double_t xmax2 = 0.75;
   Double_t xmin3 = 0.1;
   Double_t xmax3 = 0.9;
   Double_t ymin = -1.0;
   Double_t ymax =  1.0; 

   // --------------- g1 ----------------------
   c->cd(1);
   gPad->SetGridy(true);

   // Load in world data on A1He3 from NucDB  
   gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();
   //leg->SetFillColor(kWhite); 
   Double_t Q2_min = 1.5;
   Double_t Q2_max = 7.0;
   NucDBBinnedVariable * Qsqbin = 0;
   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(4);
   Qsqbin->SetTitle(Form("%4.2f <Q^{2}<%4.2f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));
   leg->SetHeader(Form("World data %s",Qsqbin->GetTitle()));

   NucDBMeasurement * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   TMultiGraph *MG = new TMultiGraph(); 
   TList * measurementsList =  new TList(); //manager->GetMeasurements("A1p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("g1p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("g1p");
   if(ames) measurementsList->Add(ames);
   //leg->SetFillColor(kWhite); 

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("g1p");
   if(ames) measurementsList->Add(ames);

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement * measg1 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = measg1->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg1->BuildGraph("x");
      graph->Apply(xf);
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",measg1->GetExperimentName()); 
      leg->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG->Add(graph); 
      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }
   //TGraphErrors *graph        = measg1_saneQ2->BuildGraph("x");
   //graph->Apply(xf);
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   //leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   //MG->Add(graph,"p");
   //leg->AddEntry(Model1,Form("AAC and F1F209 model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model4,Form("AAC and CTEQ model Q^{2} = %.1f GeV^{2}",Q2) ,"l");
   //leg->AddEntry(Model2,Form("DSSV and F1F209 model Q^{2} = %.1f GeV^{2}",Q2),"l");
   //leg->AddEntry(Model3,Form("DSSV and CTEQ model Q^{2} = %.1f GeV^{2}",Q2)  ,"l");

   TString Measurement = Form("x^{2}g_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, E=5.9 GeV",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(1.0,3.0); 
   MG->GetYaxis()->SetRangeUser(-0.05,0.05); 
   MG->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   TLegend *leg2 = new TLegend(0.84, 0.15, 0.99, 0.85);
   double yAxisMax = 0.07;
   double yAxisMin =-0.07;
   c->cd(2);
   gPad->SetGridy(true);
   TMultiGraph *MG2 = new TMultiGraph(); 
   NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   measurementsList->Clear();
   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("g2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("g2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("g2p");
   if(ames) measurementsList->Add(ames);

   //measurementsList = manager->GetMeasurements("g2p");
   //measurementsList->Print();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *measg2 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = measg2->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      //A2pMeas->PrintData();
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg2->BuildGraph("x");
      graph->Apply(xf2);
      graph->SetMarkerStyle(27);
      // Set legend title 
      Title = Form("%s",measg2->GetExperimentName()); 
      leg2->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 
   }

   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary #frac{3}{2}x^{2}g_{2}^{p}, E=5.9 GeV");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(1.0,3.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   MG2->GetYaxis()->SetRangeUser(-0.05,0.05); 
   MG2->GetYaxis()->SetTitle("#frac{3}{2}x^{2}g_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   leg2->Draw();

   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg2pWW = new TF1("x2g2pWW", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWA = new TF1("x2g2pWWA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWB = new TF1("x2g2pWWB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");
   TF1 * xg2pWWC = new TF1("x2g2pWWC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatex2g2pWW, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2pWW");

   xg2pWW->SetParameter(0,Q2);
   xg2pWWA->SetParameter(0,Q2);
   xg2pWWB->SetParameter(0,Q2);
   xg2pWWC->SetParameter(0,Q2);

   xg2pWW->SetLineColor(1);
   xg2pWWA->SetLineColor(3);
   xg2pWWB->SetLineColor(kOrange -1);
   xg2pWWC->SetLineColor(kViolet-2);

   Int_t width = 3;
   xg2pWW->SetLineWidth(width);
   xg2pWWA->SetLineWidth(width);
   xg2pWWB->SetLineWidth(width);
   xg2pWWC->SetLineWidth(width);

   xg2pWW->SetLineStyle(1);
   xg2pWWA->SetLineStyle(1);
   xg2pWWB->SetLineStyle(1);
   xg2pWWC->SetLineStyle(1);

   xg2pWW->SetMaximum(yAxisMax);
   xg2pWW->SetMinimum(yAxisMin);

   xg2pWW->GetXaxis()->SetTitle("x");
   xg2pWW->GetXaxis()->CenterTitle();
   //xg2pWW->Draw("same");
   //xg2pWWA->Draw("same");
   //xg2pWWB->Draw("same");
   //xg2pWWC->Draw("same");
   c->Update();

   c->SaveAs(Form("results/asymmetries/x2g1g2_59_W_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/x2g1g2_59_W_%d.png",aNumber));

   //fout->WriteObject(fA1Asymmetries,Form("A1-59-%d",0));//,TObject::kSingleKey);
   //fout->WriteObject(fA2Asymmetries,Form("A2-59-%d",0));//,TObject::kSingleKey);

   TCanvas * c2 = new TCanvas();
   gPad->SetGridy(true);
   mgd2->Draw("a");
   mgd2->GetXaxis()->SetLimits(1.0,3.0); 
   mgd2->GetYaxis()->SetRangeUser(-0.15,0.15); 
   c2->Update();

   c2->SaveAs(Form("results/asymmetries/x2g1g2_59_W_d2_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/x2g1g2_59_W_d2_%d.png",aNumber));

   return(0);
}
