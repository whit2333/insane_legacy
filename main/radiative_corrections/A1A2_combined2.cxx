#include "asym/sane_data_bins.cxx"
Int_t A1A2_combined2(Int_t paraFileVersion = 8209,Int_t perpFileVersion = 8209, 
                    Int_t para47Version = 8209, Int_t perp47Version = 8209,Int_t aNumber=8209){

   const char * parfile = "/home/whit/work/WhitFit/stat_test.txt";
   const TString weh("A1A2_combined()");
   //if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
   //   Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
   //   return -1;
   //}
   sane_data_bins();
   // --------------------------------------------------------


   // 5.9 GeV asymmetries
   TFile * f = new TFile(Form("data/A1A2_59_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();
   //f->ls();
   TList * fA1Asymmetries59 = (TList*)gROOT->FindObject(Form("A1asym-59-x-%d",0));
   if(!fA1Asymmetries59) return(-1);
   TList * fA2Asymmetries59 = (TList*)gROOT->FindObject(Form("A2asym-59-x-%d",0));
   if(!fA2Asymmetries59) return(-2);
   TList * fF1_SFs59 = (TList*)gROOT->FindObject(Form("F1-59-x-%d",0));
   if(!fF1_SFs59) return(-21);

   // 4.7 GeV asymmetries
   TFile * f47 = new TFile(Form("data/A1A2_47_noel_%d_%d.root",para47Version,perp47Version),"UPDATE");
   f47->cd();
   TList * fA1Asymmetries47 = (TList*)gROOT->FindObject(Form("A1asym-47-x-%d",0));
   if(!fA1Asymmetries47) return(-3);
   TList * fA2Asymmetries47 = (TList*)gROOT->FindObject(Form("A2asym-47-x-%d",0));
   if(!fA2Asymmetries47) return(-4);
   TList * fF1_SFs47 = (TList*)gROOT->FindObject(Form("F1-47-x-%d",0));
   if(!fF1_SFs47) return(-22);

   const char * parafile = Form("data/bg_corrected_asymmetries_para59_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para59-%d",0));
   if(!fParaAsymmetries) return(-3);
   //fParaAsymmetries->Print();

   //const char * perpfile = Form("data/bg_corrected_asymmetries_perp59_%d.root",perpFileVersion);
   //TFile * f2 = TFile::Open(perpfile,"UPDATE");
   //f2->cd();
   //TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp59-%d",0));
   //if(!fPerpAsymmetries) return(-4);
   ////fPerpAsymmetries->Print();

   TList * fA1combined = new TList();
   TList * fA2combined = new TList();
   TList * fF1combined = new TList();

   TH1F * fA1_59 = 0;
   TH1F * fA2_59 = 0;
   TH1F * fF1_59 = 0;
   TH1F * fA1_47 = 0;
   TH1F * fA2_47 = 0;
   TH1F * fF1_47 = 0;
   TH1F * fA1    = 0;
   TH1F * fA2    = 0;
   TH1F * fF1    = 0;

   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;

   // The asymmetry is calculated A = C1(A180*C2+A80*C3)
   TCanvas * c = new TCanvas("A1A2_combined","A1 A2 combined");
   c->Divide(1,2);
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();

   TLegend *leg  = new TLegend(0.84, 0.15, 0.99, 0.85);
   TLegend *leg0 = new TLegend(0.16, 0.65, 0.35, 0.89);

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fA1Asymmetries59->GetEntries();jj++) {

      fA1_59 = (TH1F*) fA1Asymmetries59->At(jj);
      fA2_59 = (TH1F*) fA2Asymmetries59->At(jj);
      fF1_59 = (TH1F*) fF1_SFs59->At(jj);
      fA1_47 = (TH1F*) fA1Asymmetries47->At(jj);
      fA2_47 = (TH1F*) fA2Asymmetries47->At(jj);
      fF1_47 = (TH1F*) fF1_SFs47->At(jj);

      fA1 = new TH1F(*fA1_59);
      fA1->Reset();
      fA1combined->Add(fA1);
      fA2 = new TH1F(*fA2_59);
      fA2->Reset();
      fA2combined->Add(fA2);
      fF1 = new TH1F(*fF1_59);
      fF1->Reset();
      fF1combined->Add(fF1);

      //InSANEAveragedMeasuredAsymmetry * paraAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * paraAsym        = paraAsymAverage->GetAsymmetryResult();
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      //InSANEAveragedMeasuredAsymmetry * perpAsymAverage = (InSANEAveragedMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEMeasuredAsymmetry         * perpAsym        = perpAsymAverage->GetAsymmetryResult();
      //InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));
      //InSANEAveragedKinematics1D * paraKine = perpAsym->fAvgKineVsx;
      //InSANEAveragedKinematics1D * perpKine = perpAsym->fAvgKineVsx; 

      std::cout << jj << std::endl;

      Int_t   xBinmax = fA1_59->GetNbinsX();
      Int_t   yBinmax = fA1_59->GetNbinsY();
      Int_t   zBinmax = fA1_59->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1_59->GetBin(i,j,k);

               Double_t A1_59   = fA1_59->GetBinContent(bin);
               Double_t A2_59   = fA2_59->GetBinContent(bin);
               Double_t eA1_59  = fA1_59->GetBinError(bin);
               Double_t eA2_59  = fA2_59->GetBinError(bin);

               Double_t A1_47   = fA1_47->GetBinContent(bin);
               Double_t A2_47   = fA2_47->GetBinContent(bin);
               Double_t eA1_47  = fA1_47->GetBinError(bin);
               Double_t eA2_47  = fA2_47->GetBinError(bin);

               Double_t A1  = 0.0; 
               Double_t eA1 = 0.0; 
               Double_t A2  = 0.0; 
               Double_t eA2 = 0.0; 

               Double_t F1_47   = fF1_47->GetBinContent(bin);
               Double_t F1_59   = fF1_59->GetBinContent(bin);
               Double_t F1  = 0.0; 

               if( eA1_47 == 0.0 || TMath::IsNaN(eA1_47) ) {
                  A1  = A1_59;
                  eA1 = eA1_59;
                  F1  = F1_59;
               }
               if( eA1_59 == 0.0 || TMath::IsNaN(eA1_59) ) {
                  A1  = A1_47;
                  eA1 = eA1_47;
                  F1  = F1_47;
               }
                 
               if( eA1_47 != 0.0 && eA1_59 != 0.0 && !(TMath::IsNaN(eA1_47)) && !(TMath::IsNaN(eA1_59)) ) {

                  A1 = (A1_59/(eA1_59*eA1_59) + A1_47/(eA1_47*eA1_47))/(1.0/(eA1_59*eA1_59) + 1.0/(eA1_47*eA1_47));
                  eA1 = TMath::Sqrt((1.0)/(1.0/(eA1_59*eA1_59) + 1.0/(eA1_47*eA1_47)));
                  F1 = (F1_47 + F1_59)/2.0;
               }


               if( eA2_47 == 0.0 || TMath::IsNaN(eA2_47) ) {
                  A2  = A2_59;
                  eA2 = eA2_59;
                  F1  = F1_59;
               }
               if( eA2_59 == 0.0 || TMath::IsNaN(eA2_59) ) {
                  A2  = A2_47;
                  eA2 = eA2_47;
                  F1  = F1_47;
               }
               if( eA2_47 != 0.0 && eA2_59 != 0.0 && !(TMath::IsNaN(eA2_47)) && !(TMath::IsNaN(eA2_59)) ) {

                  A2 = (A2_59/(eA2_59*eA2_59) + A2_47/(eA2_47*eA2_47))/(1.0/(eA2_59*eA2_59) + 1.0/(eA2_47*eA2_47));
                  eA2 = TMath::Sqrt((1.0)/(1.0/(eA2_59*eA2_59) + 1.0/(eA2_47*eA2_47)));
                  F1 = (F1_47 + F1_59)/2.0;
               }
               //std::cout << "-----------------------------------" << std::endl;
               //std::cout << A1_47 << " +- " << eA1_47 << std::endl;
               //std::cout << A1_59 << " +- " << eA1_59 << std::endl;
               //std::cout << A1 << " +- " << eA1 << std::endl;

               fA1->SetBinContent(bin,A1);
               fA2->SetBinContent(bin,A2);
               fA1->SetBinError(bin, eA1);
               fA2->SetBinError(bin, eA2);
               fF1->SetBinContent(bin,F1);

            }
         }
      }
      //       asy->fAsymmetryVsx->SetTitle("A vs x");
      //       asy->fAsymmetryVsx->SetMinimum(-2.0);
      //       asy->fAsymmetryVsx->SetMaximum(2.0);
      //       if(j==0)asy->fAsymmetryVsx->Draw("E1");
      //       else asy->fAsymmetryVsx->Draw("same,E1");
      //       if(j<4)leg->AddEntry(asy->fAsymmetryVsx,Form("<Q^{2}>=%1.1f",QsqAvg[j]),"lp");
      //TH1D* fA1_y = fA1->ProjectionY(Form("%s_py",fA1->GetName()));
      TH1 * h1 = fA2; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 1.0 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<4){
      mg2->Add(gr,"p");
      }


      h1 = fA1; 
      gr = new TGraphErrors(h1);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 1.0 ) {
            gr->RemovePoint(j);
            continue;
         }
         if( jj==4)
         //if( xt > 0.6 ) {
         //   gr->RemovePoint(j);
         //   continue;
         //}
         if( yt == 0.0 || yt < 0.3 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.8);
      gr->SetLineWidth(2);
      gr->SetMarkerColor(fA1->GetMarkerColor());
      gr->SetLineColor(fA1->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(24);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
         mg->Add(gr,"p");
      }
      if(jj<4){
         mg->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"p");
         //leg0->AddEntry(gr,Form("SANE Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"p");
      }

      //if(jj==0) fA1_y->Draw("E1,p");
      //else fA1_y->Draw("same,E1,p");

      //c->cd(2);
      //TH1D* fA2_y = fA2->ProjectionY(Form("%s_py",fA2->GetName()));
      //fA2_y->SetMarkerStyle(20);
      //fA2_y->SetMarkerColor(1+jj);
      //if(jj==0) fA2_y->Draw();
      //else fA2_y->Draw("same");

   }

   TFile * fout = new TFile(Form("data/A1A2_combined_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   fout->WriteObject(fA1combined,Form("A1-combined-x-%d",0));//,TObject::kSingleKey); 
   fout->WriteObject(fA2combined,Form("A2-combined-x-%d",0));//,TObject::kSingleKey); 
   fout->WriteObject(fF1combined,Form("F1-combined-x-%d",0));//,TObject::kSingleKey); 


   // --------------- A1 ----------------------
   c->cd(1);
   gPad->SetGridy(true);
   TString Title;
   Int_t width = 1;

   // Load in world data on g1 from NucDB  
   gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();

   Double_t Q2_min = 1.5;
   Double_t Q2_max = 8.0;
   NucDBBinnedVariable * Qsqbin = 0;
   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(4);
   Qsqbin->SetTitle(Form("%4.1f<Q^{2}<%4.1f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));
   leg->SetHeader(Form("World data %s",Qsqbin->GetTitle()));
   leg0->SetHeader(Form("World Data %s",Qsqbin->GetTitle()));
   //NucDBMeasurement * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   Double_t Q2 = 4.0;//Qsqbin->GetAverage();
   NucDBBinnedVariable *xbin = new NucDBBinnedVariable("x","x");
   xbin->SetBinMinimum(0.0);
   xbin->SetBinMaximum(0.6);
   xbin->SetMean((0.0 + 0.6)/2.0);
   xbin->SetAverage((0.0 + 0.6)/2.0);

   NucDBBinnedVariable *Wbin = new NucDBBinnedVariable("W","W");
   Wbin->SetBinMinimum(2.1);
   Wbin->SetBinMaximum(20.0);
   Wbin->SetMean((2.0 + 20.0)/2.0);
   Wbin->SetAverage((2.0 + 20.0)/2.0);

   TMultiGraph *MG = new TMultiGraph(); 
   TMultiGraph *MGA1_data = new TMultiGraph(); 
   TList * measurementsList =  new TList(); //manager->GetMeasurements("A1p");
   //TList * measurementsList =  manager->GetMeasurements("A1p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);


   // SMC
   exp = manager->GetExperiment("SMC"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // COMPASS
   exp = manager->GetExperiment("COMPASS"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // HERMES
   exp = manager->GetExperiment("HERMES"); 
   if(exp) ames = exp->GetMeasurement("A1p");
   if(ames) measurementsList->Add(ames);

   // CLAS limited to x < 0.6
   //exp = manager->GetExperiment("CLAS-E93009"); 
   //if(exp) ames = exp->GetMeasurement("A1p");
   //if(ames){
   //   ames->SetExperimentName("CLAS-E93009 W>2.1");
   //   ames->ApplyFilterWithBin(Qsqbin);//ApplyFilterWithBin(xbin);
   //   ames->ApplyFilterWithBin(Wbin);
   //   TList * ls = ames->MergeDataPoints(4,"x");
   //   ames->AddDataPoints(ls,true);
   //   measurementsList->Add(ames);
   //}


   std::vector<int> markers = {22,23,25,26,27,28,29,30,32,33,34};
   TMultiGraph * mg_clas = new TMultiGraph();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A1pMeas = (NucDBMeasurement*)measurementsList->At(i);

      TList * plist = A1pMeas->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = A1pMeas->BuildGraph("x");
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerColor(1);
      graph->SetMarkerSize(1.2);
      graph->SetLineColor(1);
      // Set legend title 
      Title = Form("%s",A1pMeas->GetExperimentName()); 
      leg->AddEntry(graph,Title,"p");
      leg0->AddEntry(graph,Title,"p");
      // Add to TMultiGraph 
      MG->Add(graph); 
      if(A1pMeas == ames) mg_clas->Add(graph); 
      else MGA1_data->Add(graph); 

      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }
   //TGraphErrors *graph        = measg1_saneQ2->BuildGraph("x");
   //graph->SetMarkerColor(1);
   //graph->SetMarkerStyle(34);
   //leg->AddEntry(graph,Form("SLAC in %s",Qsqbin.GetTitle()),"lp");
   //MG->Add(graph,"p");

   TString Measurement = Form("A_{1}^{p}");
   TString GTitle      = Form("Preliminary %s, Combined Beam Energies",Measurement.Data());
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(0.0,1.0); 
   MG->Draw("AP");
   //Model1->Draw("same");
   //Model4->Draw("same");
   //Model2->Draw("same");
   //Model3->Draw("same");
   leg->Draw();

   //mg->Draw("same");
   //mg->GetXaxis()->SetLimits(0.0,1.0);
   //mg->GetYaxis()->SetRangeUser(-0.2,1.0);
   //mg->SetTitle("A_{1}, E=5.9 GeV"); 
   //mg->GetXaxis()->SetTitle("x");
   //mg->GetYaxis()->SetTitle("A_{1}");

   //--------------- A2 ---------------------
   c->cd(2);
   TLegend *leg2 = new TLegend(0.84, 0.15, 0.99, 0.85);
   gPad->SetGridy(true);
   TMultiGraph *MG2 = new TMultiGraph(); 

   NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   measurementsList->Clear();
   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);

   // HERMES
   exp = manager->GetExperiment("HERMES"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);


   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = A2pMeas->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = A2pMeas->BuildGraph("x");
      //graph->SetMarkerColor(1);
      //graph->SetLineColor(1);
      graph->SetMarkerStyle(27);
      //graph->SetMarkerSize(1.5);
      // Set legend title 
      Title = Form("%s",A2pMeas->GetExperimentName()); 
      leg2->AddEntry(graph,Title,"lp");
      // Add to TMultiGraph 
      MG2->Add(graph,"p"); 
   }

   MG2->Add(mg2,"p");
   MG2->SetTitle("Preliminary A_{2}^{p}, Combined Beam Energies");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   MG2->GetYaxis()->SetRangeUser(-1.0,1.0); 
   MG2->GetYaxis()->SetTitle("A_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   leg2->Draw();
   c->Update();

   c->SaveAs(Form("results/asymmetries/A1A2_combined_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/A1A2_combined_%d.png",aNumber));
   c->SaveAs(Form("results/asymmetries/A1A2_combined_%d.eps",aNumber));



   // --------------------------------------------------------
   // Nicer looking individiual plots
   leg0->SetFillColor(0);
   leg0->SetFillStyle(0);
   leg0->SetBorderSize(0);
   TCanvas * c2 = new TCanvas("square","square");
   c2->cd(0);

   TMultiGraph * MGA1 = new TMultiGraph();
   MGA1->SetTitle("A_{1}^{p}");

   MGA1->Add(MGA1_data,"P");

   MGA1->Draw("AP");
   MGA1->SetTitle("A_{1}^{p}");
   MGA1->GetXaxis()->SetTitle(xAxisTitle);
   MGA1->GetXaxis()->CenterTitle();
   MGA1->GetYaxis()->SetTitle(yAxisTitle);
   MGA1->GetYaxis()->CenterTitle();
   MGA1->GetXaxis()->SetLimits(0.0,1.0); 
   MGA1->GetYaxis()->SetRangeUser(0.0,1.0); 
   c2->Update();

   //MG->Draw("AP");
   leg0->Draw();
   TLatex l;
   l.SetTextSize(0.03);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(20);
   l.DrawLatex(1.0,0.59,"#leftarrow DSE");
   l.SetTextAngle(-20);
   l.DrawLatex(1.0,5.0/9.0,"#leftarrow SU(6)");
   l.DrawLatex(1.0,0.77,"#leftarrow NJL");
   l.DrawLatex(1.0,1.0,"#leftarrow pQCD");

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/A1_existing_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/A1_existing_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/A1_existing_%d.eps",aNumber));
   //

   MGA1->Add(mg_clas);
   c2->Update();

   c2->SaveAs(Form("results/asymmetries/A1_existing_CLAS_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/A1_existing_CLAS_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/A1_existing_CLAS_%d.eps",aNumber));

   // -----------------------------------------------------------------------------
   // Models
   TLegend * legmod = new TLegend(0.16,0.45,0.30,0.65);
   legmod->SetHeader(Form("Models at Q^{2}=%.1f",Q2));
   legmod->SetFillColor(0);
   legmod->SetFillStyle(0);
   legmod->SetBorderSize(0);

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   TList * list_of_models = new TList();
   TF1 * aModel = 0;

   // BBS
   //fman->CreateSFs(5);
   //InSANEStructureFunctions * BBSsf = fman->GetStructureFunctions();
   //fman->CreatePolSFs(5);
   //InSANEPolarizedStructureFunctions * BBSpsf = fman->GetPolarizedStructureFunctions();
   //InSANEVirtualComptonAsymmetries *Asym3 = new InSANEVirtualComptonAsymmetries(); 
   //Asym3->SetUnpolarizedSFs(BBSsf);
   //Asym3->SetPolarizedSFs(BBSpsf);  
   //aModel = new TF1("Model3",Asym3,&InSANE_VCSABase::EvaluateA1p,
   //      0.01,0.99,1,"InSANE_VCSABase","EvaluateA1p");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineColor(kBlue);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"BBS","l");

   //// Statistical 
   //fman->CreateSFs(6);
   //InSANEStructureFunctions * STATsf = fman->GetStructureFunctions();
   //fman->CreatePolSFs(6);
   //InSANEPolarizedStructureFunctions * STATpsf = fman->GetPolarizedStructureFunctions();
   //InSANEVirtualComptonAsymmetries *Asym4 = new InSANEVirtualComptonAsymmetries(); 
   //Asym4->SetUnpolarizedSFs(STATsf);  
   //Asym4->SetPolarizedSFs(STATpsf);
   //aModel = new TF1("Model4",Asym4,&InSANE_VCSABase::EvaluateA1p,
   //      0.01,0.99,1,"InSANE_VCSABase","EvaluateA1p");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineColor(kRed);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"Statistical","l");

   //// DSSV + F1F209 
   //fman->CreatePolSFs(0);
   //InSANEPolarizedStructureFunctions * DSSVpsf = fman->GetPolarizedStructureFunctions();
   //fman->CreateSFs(0);
   //InSANEStructureFunctions * F1F209sf = fman->GetStructureFunctions();
   ////fman->CreatePolSFs(0);
   ////InSANEPolarizedStructureFunctions * DSSVpsf = fman->GetPolarizedStructureFunctions();
   //InSANEVirtualComptonAsymmetries *Asym2 = new InSANEVirtualComptonAsymmetries(); 
   //Asym2->SetUnpolarizedSFs(F1F209sf);  
   //Asym2->SetPolarizedSFs(DSSVpsf);
   //aModel = new TF1("Model2",Asym2,&InSANE_VCSABase::EvaluateA1p,
   //      0.3, 0.89, 1,"InSANE_VCSABase","EvaluateA1p");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineColor(kGreen);
   ////list_of_models->Add(aModel);
   ////legmod->AddEntry(aModel,"F1F209+DSSV","l");

   //// CTEQ + AAC 
   //fman->CreateSFs(12);
   //InSANEStructureFunctions * CJ12sf = fman->GetStructureFunctions();
   //fman->CreatePolSFs(2);
   //InSANEPolarizedStructureFunctions * AACpsf = fman->GetPolarizedStructureFunctions();
   //InSANEVirtualComptonAsymmetries *Asym4 = new InSANEVirtualComptonAsymmetries(); 
   //Asym4->SetUnpolarizedSFs(CJ12sf);  
   //Asym4->SetPolarizedSFs(AACpsf);
   //aModel = new TF1("Model5",Asym4,&InSANE_VCSABase::EvaluateA1p,
   //      0.01,0.99,1,"InSANE_VCSABase","EvaluateA1p");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineColor(kRed);
   //aModel->SetLineStyle(2);
   ////list_of_models->Add(aModel);
   ////legmod->AddEntry(aModel,"CJ12+AAC","l");

   //// CTEQ + LSS2006 
   //fman->CreateSFs(1);
   //InSANEStructureFunctions * CTEQsf = fman->GetStructureFunctions();
   //fman->CreatePolSFs(1);
   //InSANEPolarizedStructureFunctions * LSSpsf = fman->GetPolarizedStructureFunctions();
   //InSANEVirtualComptonAsymmetries *Asym5 = new InSANEVirtualComptonAsymmetries(); 
   //Asym5->SetUnpolarizedSFs(CTEQsf);  
   //Asym5->SetPolarizedSFs(LSSpsf);
   //aModel = new TF1("Model5",Asym5,&InSANE_VCSABase::EvaluateA1p,
   //      0.01,0.99,1,"InSANE_VCSABase","EvaluateA1p");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineColor(kBlue);
   //aModel->SetLineStyle(2);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"CTEQ+LSS2006","l");

   //// CTEQ + LSS2006 
   //fman->CreatePolSFs(4);
   //InSANEPolarizedStructureFunctions * DNSpsf = fman->GetPolarizedStructureFunctions();
   //InSANEVirtualComptonAsymmetries *Asym6 = new InSANEVirtualComptonAsymmetries(); 
   //Asym6->SetUnpolarizedSFs(CTEQsf);  
   //Asym6->SetPolarizedSFs(DNSpsf);
   //aModel = new TF1("Model6",Asym6,&InSANE_VCSABase::EvaluateA1p,
   //      0.01,0.99,1,"InSANE_VCSABase","EvaluateA1p");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineColor(kGreen);
   //aModel->SetLineStyle(2);
   ////list_of_models->Add(aModel);
   ////legmod->AddEntry(aModel,"CTEQ+DNS2005","l");

   //// DSSV + CTEQ 
   //InSANEVirtualComptonAsymmetries *Asym1 = new InSANEVirtualComptonAsymmetries(); 
   //Asym1->SetUnpolarizedSFs(CTEQsf);  
   //Asym1->SetPolarizedSFs(DSSVpsf);
   //aModel = new TF1("Model1",Asym1,&InSANE_VCSABase::EvaluateA1p,
   //      0.01,0.99, 1,"InSANE_VCSABase","EvaluateA1p");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineStyle(2);
   //aModel->SetLineColor(kMagenta);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"CTEQ+DSSV","l");

   //// WHITFIT
   //gSystem->Load("libWhitFit.so");
   //WFittingStatDistributions4 * PHDs = new WFittingStatDistributions4();
   //PHDs->InitFitParameters(parfile);//"inputs/stat_params4.txt");//,aNumber));
   //PHDs->CalculateConstraints();

   //InSANEUnpolarizedPDFsFromPHDs * wpdfs = new InSANEUnpolarizedPDFsFromPHDs();
   //wpdfs->SetPHDs(PHDs);
   //InSANEPolarizedPDFsFromPHDs * wpolpdfs = new InSANEPolarizedPDFsFromPHDs();
   //wpolpdfs->SetPHDs(PHDs);

   //InSANEStructureFunctionsFromPDFs * wSFs = new InSANEStructureFunctionsFromPDFs();
   //wSFs->SetUnpolarizedPDFs(wpdfs);
   //InSANEPolarizedStructureFunctionsFromPDFs * wPolSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   //wPolSFs->SetPolarizedPDFs(wpolpdfs);

   //InSANEVirtualComptonAsymmetries *AsymW = new InSANEVirtualComptonAsymmetries(); 
   //AsymW->SetUnpolarizedSFs(wSFs);
   //AsymW->SetPolarizedSFs(wPolSFs);  
   //aModel = new TF1("Model81",AsymW,&InSANE_VCSABase::EvaluateA1p,
   //      0.01,0.99, 1,"InSANE_VCSABase","EvaluateA1p");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineStyle(1);
   //aModel->SetLineWidth(3);
   //aModel->SetLineColor(1);
   ////list_of_models->Add(aModel);
   ////legmod->AddEntry(aModel,"Whit Fit","l");


   TMultiGraph * mg_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      aModel->Draw("same");
      //TGraph * grmod = new TGraph(aModel->DrawCopy("goff")->GetHistogram());
      //mg_models->Add(grmod,"l");
      //MGA1->Add(grmod,"l");
      //grmod->Draw("l");
   }
   //MGA1->Add(mg_models);
   //mg_models->Draw("l");
   legmod->Draw();

   c2->Update();

   c2->SaveAs(Form("results/asymmetries/A1_existing_models_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/A1_existing_models_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/A1_existing_models_%d.eps",aNumber));

   //----------------
   gr = (TGraphErrors*)(mg->GetListOfGraphs()->At(4));
   gr->SetMarkerColor(2);
   gr->SetLineColor(2);
   gr->SetMarkerSize(1.4);
   gr->SetMarkerStyle(20);
   MGA1->Add(gr,"P");
   leg0->AddEntry(gr,"SANE x<0.6","p");
   //gr = (TGraphErrors*)(mg->GetListOfGraphs()->At(1));
   //MGA1->Add(gr,"P");
   MGA1->Draw("P");
   TColor * col = new TColor(6666,0.0,0.0,0.0,"",0.5);
   l.SetTextColor(6666);
   l.SetTextSize(0.2);
   l.SetTextAngle(45);
   l.DrawLatex(0.2,0.1,"Preliminary");

   // Resonance locations
   Double_t M_res = 1.232;
   //Double_t M_res = 1.535;
   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(0);
   Double_t x_1 = InSANE::Kine::xBjorken_WQsq(M_res,Qsqbin->GetMinimum());
   //std::cout << "x_1 = " << x_1 << "\n";
   //Double_t x_2 = InSANE::Kine::xBjorken_WQsq(1.535,Qsqbin->GetAverage());
   //Double_t x_3 = InSANE::Kine::xBjorken_WQsq(1.680,Qsqbin->GetAverage());

   TArrow ar1(x_1,0.2,x_1,0.1,0.02,"|>");
   ar1.SetLineColor(4);
   ar1.SetFillColor(4);
   //ar2.SetAngle(40);
   ar1.SetLineWidth(2);
   ar1.Draw();
   l.SetTextAlign(10);  //centered
   l.SetTextSize(0.02);
   l.SetTextFont(132);
   l.SetTextAngle(45);
   l.DrawLatex(x_1,0.2,Form("Q^{2}=%.1f (GeV/c)^{2}",Qsqbin->GetMinimum()));

   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(1);
   Double_t x_2 = InSANE::Kine::xBjorken_WQsq(M_res,Qsqbin->GetMinimum());
   TArrow ar2(x_2,0.2,x_2,0.1,0.02,"|>");
   ar2.SetLineColor(4);
   ar2.SetFillColor(4);
   //ar2.SetAngle(40);
   ar2.SetLineWidth(2);
   ar2.Draw();
   l.SetTextAlign(10);  //centered
   l.SetTextSize(0.02);
   l.SetTextFont(132);
   l.SetTextAngle(45);
   l.DrawLatex(x_2,0.2,Form("Q^{2}=%.1f (GeV/c)^{2}",Qsqbin->GetMinimum()));

   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(2);
   Double_t x_3 = InSANE::Kine::xBjorken_WQsq(M_res,Qsqbin->GetMinimum());
   TArrow ar3(x_3,0.2,x_3,0.1,0.02,"|>");
   ar3.SetLineColor(4);
   ar3.SetFillColor(4);
   //ar2.SetAngle(40);
   ar3.SetLineWidth(2);
   ar3.Draw();
   l.SetTextAlign(10);  //centered
   l.SetTextSize(0.02);
   l.SetTextFont(132);
   l.SetTextAngle(45);
   l.DrawLatex(x_3,0.2,Form("Q^{2}=%.1f (GeV/c)^{2}",Qsqbin->GetMinimum()));

   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(3);
   Double_t x_4 = InSANE::Kine::xBjorken_WQsq(M_res,Qsqbin->GetMinimum());
   TArrow ar4(x_4,0.2,x_4,0.1,0.02,"|>");
   ar4.SetLineColor(4);
   ar4.SetFillColor(4);
   ar4.SetLineWidth(2);
   ar4.Draw();
   l.SetTextAlign(10);  //centered
   l.SetTextSize(0.02);
   l.SetTextFont(132);
   l.SetTextAngle(45);
   l.DrawLatex(x_4,0.2,Form("Q^{2}=%.1f (GeV/c)^{2}",Qsqbin->GetMinimum()));

   leg0->Draw();
   legmod->Draw();

   c2->Update();
   c2->SaveAs(Form("results/asymmetries/A1_combined_%d.pdf",aNumber));
   c2->SaveAs(Form("results/asymmetries/A1_combined_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/A1_combined_%d.eps",aNumber));

   return(0);
}
