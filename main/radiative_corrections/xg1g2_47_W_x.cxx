#include "util/stat_error_graph.cxx"
#include "asym/sane_data_bins.cxx"

Int_t xg1g2_47_W_x(Int_t paraFileVersion = 6009,Int_t perpFileVersion = 6009,Int_t aNumber=6009){

   sane_data_bins();

   TFile * f = new TFile(Form("data/g1g2_47_W_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();
   f->ls();

   TList * fg1Asymmetries = (TList*)gROOT->FindObject(Form("g1asym-47-W-%d",0));
   TList * fg2Asymmetries = (TList*)gROOT->FindObject(Form("g2asym-47-W-%d",0));
   if(!fg1Asymmetries) return(-1);
   if(!fg2Asymmetries) return(-2);

   const char * parafile = Form("data/bg_corrected_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);

   TList * fg1s = new TList();
   TList * fg2s = new TList();
   TList * fd2s = new TList();

   Double_t E0 = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TH1F * fg1 = 0;
   TH1F * fg2 = 0;
   TGraphErrors * fxg1 = 0;
   TGraphErrors * fxg2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;
   InSANEStructureFunctions * SFs = new F1F209StructureFunctions();

   // The asymmetry is calculated A = C1(A180*C2+A80*C3)
   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para47");
   c->Divide(1,2);

   TMultiGraph * mg   = new TMultiGraph();
   TMultiGraph * mg2  = new TMultiGraph();
   TMultiGraph * mgd2 = new TMultiGraph();

   TMultiGraph * mg_split   = new TMultiGraph();
   TMultiGraph * mg2_split  = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   /// Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fg1Asymmetries->GetEntries();jj++) {

      InSANEMeasuredAsymmetry * fg1Full = (InSANEMeasuredAsymmetry*)fg1Asymmetries->At(jj);
      InSANEMeasuredAsymmetry * fg2Full = (InSANEMeasuredAsymmetry*)fg2Asymmetries->At(jj);

      fg1 = (TH1F*) &fg1Full->fAsymmetryVsW;
      fg2 = (TH1F*) &fg2Full->fAsymmetryVsW;

      fxg1 = new TGraphErrors(fg1->GetNbinsX());
      fxg2 = new TGraphErrors(fg2->GetNbinsX());
      fg1s->Add(fxg1);
      fg2s->Add(fxg2);
      //fd2 = new TH1F(*fA2);
      //fd2->Reset();
      //fd2s->Add(fd2);

      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));

      InSANEAveragedKinematics1D * paraKine = &perpAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsW; 

      Int_t   xBinmax = fg1->GetNbinsX();
      Int_t   yBinmax = fg1->GetNbinsY();
      Int_t   zBinmax = fg1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){

               bin   = fg1->GetBin(i,j,k);
               fg1->GetBinXYZ(bin,binx,biny,binz);

               Double_t W1     = paraKine->fW.GetBinContent(bin);
               Double_t x1     = paraKine->fx.GetBinContent(bin);
               Double_t phi1   = paraKine->fPhi.GetBinContent(bin);
               Double_t Q21    = paraKine->fQ2.GetBinContent(bin);
               Double_t W     = W1;//
               Double_t x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t phi   = phi1;//0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
               Double_t Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);


               Double_t g1   = fg1->GetBinContent(bin);
               Double_t g2   = fg2->GetBinContent(bin);

               Double_t eg1  = fg1->GetBinError(bin);
               Double_t eg2  = fg2->GetBinError(bin);

               Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               Double_t R        = SFs->R(x,Q2);
               Double_t F1       = SFs->F1p(x,Q2);
               Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               Double_t gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
               Double_t gamma    = TMath::Sqrt(gamma2);
               Double_t cota     = 1.0/TMath::Tan(alpha);
               Double_t csca     = 1.0/TMath::Sin(alpha);

               Double_t xg1 = x*g1;//(F1/(1.0+gamma2))*(A1 + gamma*A2);
               Double_t xg2 = x*g2;//(F1/(1.0+gamma2))*(A2/gamma - A1);

               Double_t d2p = 2.0*(g1+g2);

               //eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
               //Double_t a1 = x*(F1/(1.0+gamma2));
               //Double_t a2 = x*(F1/(1.0+gamma2))*gamma;
               Double_t exg1 = x*eg1;//TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //eg2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
               //a1 = -x*(F1/(1.0+gamma2));
               //a2 = x*(F1/(1.0+gamma2))/gamma;
               Double_t exg2 = x*eg2;//TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //Double_t ed2p = 2.0*TMath::Sqrt( eg1*eg1 + eg2*eg2); 
               //c0 = 1.0/(1.0 + eta*xi);
               //c11 = (1.0 + cota*chi)/D ;
               //c12 = (csca*chi)/D ;
               //c21 = (xi - cota*chi/eta)/D ;
               //c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               fxg1->SetPoint(i,x,xg1);
               fxg2->SetPoint(i,x,xg2);
               fxg1->SetPointError(i,0.0,exg1);
               fxg2->SetPointError(i,0.0,exg2);



               //fd2->SetBinContent(bin,d2p);
               //fd2->SetBinError(bin, ed2p);
            }
         }
      }

      // -----------------------------------------
      // xg1
      gr =fxg1;
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.9 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.9);
      gr->SetMarkerColor(fg1->GetMarkerColor());
      gr->SetLineColor(fg1->GetMarkerColor());
      //std::cout << fg1->GetMarkerColor() << std::endl;
      if( jj < 4 ) {
         mg->Add(gr,"p");
      } else if( jj > 4 && jj < 9 ) {
         gr->SetMarkerStyle(23);
         gr->SetMarkerColor(4008+jj-5);
         gr->SetLineColor(  4008+jj-5);
         mg_split->Add(gr,"p");
      } else if( jj > 9 && jj < 14 ) {
         gr->SetMarkerColor(4008+jj-10);
         gr->SetLineColor(  4008+jj-10);
         gr->SetMarkerStyle(22);
         mg_split->Add(gr,"p");
      }
      

      // -----------------------------------------
      // xg2
      gr = fxg2;
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( ey > 0.9 ) {
            gr->RemovePoint(j);
            continue;
         }
         else if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(0.9);
      gr->SetMarkerColor(fg1->GetMarkerColor());
      gr->SetLineColor(fg1->GetMarkerColor());
      if( jj < 4 ) {
         mg2->Add(gr,"p");
      } else if( jj > 4 && jj < 9 ) {
         gr->SetMarkerColor(4008+jj-5);
         gr->SetLineColor(  4008+jj-5);
         gr->SetMarkerStyle(23);
         mg2_split->Add(gr,"p");
      } else if( jj > 9 && jj < 14 ) {
         gr->SetMarkerColor(4008+jj-10);
         gr->SetLineColor(  4008+jj-10);
         gr->SetMarkerStyle(22);
         mg2_split->Add(gr,"p");
      }
      //leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");

      //h1 = fd2; 
      //gr = new TGraphErrors(h1);
      //for( int j = gr->GetN()-1 ; j>=0; j--) {
      //   Double_t xt, yt;
      //   gr->GetPoint(j,xt,yt);
      //   if( yt == 0.0 ) {
      //      gr->RemovePoint(j);
      //   }
      //}

      //gr->SetMarkerStyle(20);
      //gr->SetMarkerSize(0.6);
      //gr->SetMarkerColor(fA1->GetMarkerColor());
      //gr->SetLineColor(fA1->GetMarkerColor());
      //mgd2->Add(gr,"p");

      //if(jj==0) fA1_y->Draw("E1,p");
      //else fA1_y->Draw("same,E1,p");

      //c->cd(2);
      //TH1D* fA2_y = fA2->ProjectionY(Form("%s_py",fA2->GetName()));
      //fA2_y->SetMarkerStyle(20);
      //fA2_y->SetMarkerColor(1+jj);
      //if(jj==0) fA2_y->Draw();
      //else fA2_y->Draw("same");

   }

   // ------------------------------------
   // Function to take g2p --> x*g2p
   TF2 * xf = new TF2("xf","x*y",0,1,-1,1);

   // --------------- xg1 ----------------------
   c->cd(1);
   gPad->SetGridy(true);
   NucDBManager * manager = NucDBManager::GetManager();
   NucDBBinnedVariable * Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(4);

   TMultiGraph      * MG               = new TMultiGraph();
   TMultiGraph      * MG_data          = new TMultiGraph();
   TList            * measurementsList = manager->GetMeasurements("g1p");
   NucDBExperiment  * exp              = 0;
   NucDBMeasurement * ames             = 0;

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement * measg1 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = measg1->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg1->BuildGraph("x");
      graph->Apply(xf);
      graph->SetMarkerStyle(27);
      // Set legend title 
      leg->AddEntry(graph,Form("%s",measg1->GetExperimentName()),"lp");
      // Add to TMultiGraph 
      MG_data->Add(graph); 
   }

   TString Measurement = Form("xg_{1}^{p}");
   TString GTitle      = "";
   TString xAxisTitle  = Form("x");
   TString yAxisTitle  = Form("%s",Measurement.Data());

   MG->Add(MG_data,"p");
   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle(GTitle);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(0.0,1.0); 
   MG->GetYaxis()->SetRangeUser(-0.05,0.15); 
   MG->Draw("AP");
   leg->Draw();

   //--------------- xg2 ---------------------
   c->cd(2);
   TLegend *leg2 = new TLegend(0.84, 0.15, 0.99, 0.85);
   gPad->SetGridy(true);
   TMultiGraph      * MG2 = new TMultiGraph(); 
   TMultiGraph      * MG2_data          = new TMultiGraph();
   NucDBMeasurement * measg2_saneQ2 = new NucDBMeasurement("measg2_saneQ2",Form("g_{2}^{p} %s",Qsqbin->GetTitle()));
   measurementsList->Clear();
   measurementsList = manager->GetMeasurements("g2p");
   //measurementsList->Print();

   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement * measg2 = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist = measg2->ApplyFilterWithBin(Qsqbin);
      if(!plist) continue;
      if(plist->GetEntries() == 0 ) continue;
      // Get TGraphErrors object 
      TGraphErrors *graph        = measg2->BuildGraph("x");
      graph->Apply(xf);
      graph->SetMarkerStyle(27);
      // Set legend title 
      leg2->AddEntry(graph,measg2->GetExperimentName(),"lp");
      // Add to TMultiGraph 
      MG2_data->Add(graph,"p"); 
   }

   MG2->Add(MG2_data,"p");
   MG2->Add(mg2,"p");
   MG2->SetTitle("");
   MG2->Draw("a");
   MG2->GetXaxis()->SetLimits(0.0,1.0); 
   //MG2->GetYaxis()->SetRangeUser(-0.35,0.35); 
   MG2->GetYaxis()->SetRangeUser(-0.2,0.2); 
   MG2->GetYaxis()->SetTitle("xg_{2}^{p}");
   MG2->GetXaxis()->SetTitle("x");
   MG2->GetXaxis()->CenterTitle();
   MG2->GetYaxis()->CenterTitle();
   leg2->Draw();

   c->Update();

   c->SaveAs(Form("results/asymmetries/xg1g2_47_W_x_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/xg1g2_47_W_x_%d.png",aNumber));

   // ----------------------------------------
   // split analysis
   c = new TCanvas();
   c->Divide(1,2);

   // xg1
   c->cd(1);
   mg_split->Add(MG_data,"p");
   mg_split->Draw("AP");
   mg_split->SetTitle(GTitle);
   mg_split->GetXaxis()->SetTitle(xAxisTitle);
   mg_split->GetXaxis()->CenterTitle();
   mg_split->GetYaxis()->SetTitle(yAxisTitle);
   mg_split->GetYaxis()->CenterTitle();
   mg_split->GetXaxis()->SetLimits(0.0,1.0); 
   mg_split->GetYaxis()->SetRangeUser(-0.05,0.15); 
   mg_split->Draw("AP");
   leg->Draw();

   // xg2
   c->cd(2);
   mg2_split->Add(MG2_data,"p");
   mg2_split->Draw("AP");
   mg2_split->SetTitle(GTitle);
   mg2_split->GetXaxis()->SetTitle(xAxisTitle);
   mg2_split->GetXaxis()->CenterTitle();
   mg2_split->GetYaxis()->SetTitle(yAxisTitle);
   mg2_split->GetYaxis()->CenterTitle();
   mg2_split->GetXaxis()->SetLimits(0.0,1.0); 
   mg2_split->GetYaxis()->SetRangeUser(-0.2,0.2); 
   mg2_split->Draw("AP");
   leg2->Draw();

   c->SaveAs(Form("results/asymmetries/xg1g2_47_W_x_split_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/xg1g2_47_W_x_split_%d.png",aNumber));

   //TCanvas * c2 = new TCanvas();
   //mgd2->Draw("a");
   //mgd2->GetXaxis()->SetLimits(0.0,1.0); 
   //mgd2->GetYaxis()->SetRangeUser(-0.15,0.15); 
   //c2->Update();

   //c2->SaveAs(Form("results/asymmetries/x2g1g2_47_d2_%d.pdf",aNumber));
   //c2->SaveAs(Form("results/asymmetries/x2g1g2_47_d2_%d.png",aNumber));

   return(0);
}
