Int_t compare_tails_47_W(Int_t paraRunGroup = 23,Int_t perpRunGroup = 23,Int_t aNumber=23){

   // -------------------------------------------------------------
   //
   // -------------------------------------------------------------

   Int_t paraFileVersion = paraRunGroup;
   Int_t perpFileVersion = perpRunGroup;

   Double_t E0    = 4.7;
   Double_t alpha = 80.0*TMath::Pi()/180.0;

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);

   const char * parafile = Form("data/bg_corrected_asymmetries_para47_%d.root",paraFileVersion);
   TFile * f1 = TFile::Open(parafile,"UPDATE");
   f1->cd();
   TList * fParaAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_para47-%d",0));
   if(!fParaAsymmetries) return(-1);
   TList * fParaRCs = (TList *) gROOT->FindObject(Form("elastic-RCs_para47-%d",0));
   if(!fParaRCs) return(-1);

   const char * perpfile = Form("data/bg_corrected_asymmetries_perp47_%d.root",perpFileVersion);
   TFile * f2 = TFile::Open(perpfile,"UPDATE");
   f2->cd();
   TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp47-%d",0));
   if(!fPerpAsymmetries) return(-2);
   TList * fPerpRCs = (TList *) gROOT->FindObject(Form("elastic-RCs_perp47-%d",0));
   if(!fPerpRCs) return(-2);

   TFile * fout = new TFile(Form("data/A1A2_47_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fA90Asymmetries = new TList();

   TH1F * fA90       = 0;
   TH1F * fA80Calc   = 0;
   TH1F * fA180Calc  = 0;
   TGraphErrors * gr = 0;

   TCanvas * c = new TCanvas("cA1A2_4pass","A1_para47");
   c->Divide(2,2);
   TMultiGraph * mg  = new TMultiGraph();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mg3 = new TMultiGraph();
   TMultiGraph * mg4 = new TMultiGraph();
   TMultiGraph * mg5 = new TMultiGraph();
   TMultiGraph * mg6 = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   // Loop over parallel asymmetries Q2 bins
   for(int jj = 0;jj<fParaAsymmetries->GetEntries() ;jj++) {

      //std::cout << jj << std::endl;
      InSANEMeasuredAsymmetry         * paraAsym        = (InSANEMeasuredAsymmetry * )(fParaAsymmetries->At(jj));
      InSANEMeasuredAsymmetry         * perpAsym        = (InSANEMeasuredAsymmetry * )(fPerpAsymmetries->At(jj));

      TH1F * fApara = &paraAsym->fAsymmetryVsW;
      TH1F * fAperp = &perpAsym->fAsymmetryVsW;

      InSANEAveragedKinematics1D * paraKine = &perpAsym->fAvgKineVsW;
      InSANEAveragedKinematics1D * perpKine = &perpAsym->fAvgKineVsW; 

      InSANERadiativeCorrections1D * paraRC = (InSANERadiativeCorrections1D*)fParaRCs->FindObject(Form("rc-W-%d",jj));
      InSANERadiativeCorrections1D * perpRC = (InSANERadiativeCorrections1D*)fPerpRCs->FindObject(Form("rc-W-%d",jj));

      TGraph * g1 = 0;

      // --------------------------------------------------------
      // Born Asymmetries
      fA180Calc = paraRC->fAsymBorn;
      g1 = new TGraph(fA180Calc);
      g1->SetLineColor(4);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg->Add(g1,"l");

      fA80Calc  = perpRC->fAsymBorn;
      g1 = new TGraph(fA80Calc);
      g1->SetLineColor(4);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg2->Add(g1,"l");

      // --------------------------------------------------------
      // Inelastic Asymmetries
      g1 = new TGraph(paraRC->fAsymIRT);
      g1->SetLineColor(1);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg->Add(g1,"l");

      g1 = new TGraph(perpRC->fAsymIRT);
      g1->SetLineColor(1);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg2->Add(g1,"l");

      // --------------------------------------------------------
      // Born Cross Sections
      fA180Calc = paraRC->fSigmaBorn;
      g1 = new TGraph(fA180Calc);
      g1->SetLineColor(4);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      fA80Calc  = perpRC->fSigmaBorn;
      g1 = new TGraph(fA80Calc);
      g1->SetLineColor(4);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");

      // -------------------------------------------
      // Inelastic cross sections
      fA180Calc = paraRC->fSigmaIRT;
      g1 = new TGraph(fA180Calc);
      g1->SetLineColor(1);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      fA80Calc  = perpRC->fSigmaIRT;
      g1 = new TGraph(fA80Calc);
      g1->SetLineWidth(1);
      g1->SetLineColor(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");

      // --------------------------------------------------------
      fA180Calc = paraRC->fSigmaERT;
      g1 = new TGraph(fA180Calc);
      g1->SetLineColor(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      fA80Calc  = perpRC->fSigmaERT;
      g1 = new TGraph(fA80Calc);
      g1->SetLineColor(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");

      // --------------------------------------------------------
      g1 = new TGraph(paraRC->fSigmaBorn_Plus);
      g1->SetLineColor(4);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      g1 = new TGraph(paraRC->fSigmaBorn_Minus);
      g1->SetLineColor(4);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      // --------------------------------------------------------
      g1 = new TGraph(perpRC->fSigmaBorn_Plus);
      g1->SetLineColor(4);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");

      g1 = new TGraph(perpRC->fSigmaBorn_Minus);
      g1->SetLineColor(4);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");

      // --------------------------------------------------------
      g1 = new TGraph(paraRC->fSigmaIRT_Plus);
      g1->SetLineColor(1);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      g1 = new TGraph(paraRC->fSigmaIRT_Minus);
      g1->SetLineColor(1);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      // --------------------------------------------------------
      g1 = new TGraph(perpRC->fSigmaIRT_Plus);
      g1->SetLineColor(1);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");

      g1 = new TGraph(perpRC->fSigmaIRT_Minus);
      g1->SetLineColor(1);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");


      // --------------------------------------------------------
      g1 = new TGraph(paraRC->fSigmaERT_Plus);
      g1->SetLineColor(2);
      g1->SetLineWidth(1);
      g1->SetLineStyle(2);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      g1 = new TGraph(paraRC->fSigmaERT_Minus);
      g1->SetLineColor(2);
      g1->SetLineStyle(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg3->Add(g1,"l");

      // --------------------------------------------------------
      g1 = new TGraph(perpRC->fSigmaERT_Plus);
      g1->SetLineColor(2);
      g1->SetLineWidth(1);
      g1->SetLineStyle(2);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");

      g1 = new TGraph(perpRC->fSigmaERT_Minus);
      g1->SetLineColor(2);
      g1->SetLineWidth(1);
      g1->SetLineStyle(2);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg4->Add(g1,"l");

      // --------------------------------------------------------
      fA180Calc = paraRC->fAsymERT;
      g1 = new TGraph(fA180Calc);
      g1->SetLineColor(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg->Add(g1,"l");

      fA80Calc  = perpRC->fAsymERT;
      g1 = new TGraph(fA80Calc);
      g1->SetLineColor(2);
      g1->SetLineWidth(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg2->Add(g1,"l");

      // --------------------------------------------------------
      TH1F * f_el = 0;

      f_el = new TH1F( *(paraRC->fSigmaIRT) );
      f_el->Reset();
      f_el->Add(paraRC->fSigmaIRT);
      f_el->Add(paraRC->fSigmaERT);
      f_el->Divide(paraRC->fSigmaIRT);
      g1 = new TGraph(f_el);
      g1->SetLineColor(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5) {
         mg5->Add(g1,"l");
      } else if(jj<10) {
         g1->SetLineStyle(2);
         //mg5->Add(g1,"l");
      } else {
         g1->SetLineStyle(3);
         //mg5->Add(g1,"l");
      }



      f_el = new TH1F( *(perpRC->fSigmaIRT) );
      f_el->Reset();
      f_el->Add(perpRC->fSigmaIRT);
      f_el->Add(perpRC->fSigmaERT);
      f_el->Divide(perpRC->fSigmaIRT);
      g1 = new TGraph(f_el);
      g1->SetLineColor(2);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5)mg5->Add(g1,"l");

      // --------------------------------------------------------
      TH1F * C_el = 0;

      C_el = new TH1F( *(paraRC->fDeltaERT) );
      C_el->Reset();
      C_el->Add(paraRC->fDeltaERT);
      C_el->Divide(paraRC->fSigmaIRT);
      g1 = new TGraph(C_el);
      g1->SetLineColor(1);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5){
         mg6->Add(g1,"l");
      } else if(jj<10) {
         g1->SetLineStyle(2);
         //mg6->Add(g1,"l");
      } else {
         g1->SetLineStyle(3);
         //mg6->Add(g1,"l");
      }

      C_el = new TH1F( *(perpRC->fDeltaIRT) );
      C_el->Reset();
      C_el->Add(perpRC->fDeltaERT);
      C_el->Divide(perpRC->fSigmaIRT);
      g1 = new TGraph(C_el);
      g1->SetLineColor(2);
      for( int j = g1->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         g1->GetPoint(j,xt,yt);
         if( yt == 0.0 ) {
            g1->RemovePoint(j);
         }
      }
      if(jj<5){
         mg6->Add(g1,"l");
      } 
      //else if(jj<10) {
      //   g1->SetLineStyle(2);
      //   mg6->Add(g1,"l");
      //} else {
      //   g1->SetLineStyle(3);
      //   mg6->Add(g1,"l");
      //}

   }

   //fout->WriteObject(fA90Asymmetries,Form("A90-47-%d",0));//,TObject::kSingleKey); 

   //------------------------------------------
   //TFile * calcfile = new TFile("results/cross_sections/inclusive/InternalIRT_XSecs_x.root","READ");
   //TMultiGraph * mgCalca180 = new TMultiGraph();
   ////calcfile->ls();
   //calcfile->cd();
   //for(int i=2;i<=5;i++){
   //   TGraph * g = (TGraph*)gROOT->FindObject(Form("asym_x_47_%d_%d",80,i));
   //   if(g) mgCalca180->Add(g,"l");
   //}

   //------------------------------------------
   TLegend *leg2 = new TLegend(0.12, 0.12, 0.35, 0.25);
   leg2->AddEntry((TGraphErrors*)mg3->GetListOfGraphs()->At(0),"Born","l");
   leg2->AddEntry((TGraphErrors*)mg3->GetListOfGraphs()->At(1),"Inelastic","l");
   leg2->AddEntry((TGraphErrors*)mg3->GetListOfGraphs()->At(2),"Elastic Tail","l");

   c->cd(1);

   TString Measurement = "A_{180}";
   TString GTitle      = Measurement + "  E=4.7 GeV";
   TString xAxisTitle  = "W";
   TString yAxisTitle  = Measurement;

   mg->Draw("A");
   mg->SetTitle(GTitle);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle();
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle();
   mg->GetXaxis()->SetLimits(0.55,3.5); 
   mg->GetYaxis()->SetRangeUser(0.0,1.0); 
   mg->Draw("A");
   //leg2->Draw();
   gPad->Update();
   TCanvas * c_1 = new TCanvas();
   mg->Draw("A");
   c_1->SaveAs(Form("results/asymmetries/compare_tails_47_A180_W_%d.pdf",aNumber));

   //------------------------------------------
   c->cd(2);
   gPad->SetLogy(true);
   mg3->SetTitle("#sigma_{180}   E=4.7 GeV");
   mg3->Draw("A");
   mg3->GetXaxis()->SetLimits(0.55,3.5); 
   mg3->GetYaxis()->SetRangeUser(0.0002,30.0); 
   mg3->GetYaxis()->SetTitle("d#sigma_{180}/dEd#Omega  (nb/GeV/sr)");
   mg3->GetXaxis()->SetTitle("W");
   mg3->GetXaxis()->CenterTitle();
   mg3->GetYaxis()->CenterTitle();
   mg3->Draw("A");
   gPad->Update();
   TCanvas * c_2 = new TCanvas();
   gPad->SetLogy(true);
   mg3->Draw("A");
   c_2->SaveAs(Form("results/asymmetries/compare_tails_47_sig180_W_%d.pdf",aNumber));

   //------------------------------------------
   c->cd(3);
   mg2->SetTitle("A_{80}   E=4.7 GeV");
   mg2->Draw("a");
   mg2->GetXaxis()->SetLimits(0.55,3.5); 
   mg2->GetYaxis()->SetRangeUser(-0.4,0.4); 
   mg2->GetYaxis()->SetTitle("A_{80}");
   mg2->GetXaxis()->SetTitle("W");
   mg2->GetXaxis()->CenterTitle();
   mg2->GetYaxis()->CenterTitle();
   mg2->Draw("a");
   //leg2->Draw();
   TCanvas * c_3 = new TCanvas();
   mg3->Draw("A");
   c_3->SaveAs(Form("results/asymmetries/compare_tails_47_A80_W_%d.pdf",aNumber));

   //------------------------------------------
   c->cd(4);
   gPad->SetLogy(true);
   mg4->SetTitle("#sigma_{80}   E=4.7 GeV");
   mg4->Draw("a");
   mg4->GetXaxis()->SetLimits(0.55,3.5); 
   mg4->GetYaxis()->SetRangeUser(0.0002,30.0); 
   mg4->GetYaxis()->SetTitle("d#sigma_{80}/dEd#Omega  (nb/GeV/sr)");
   mg4->GetXaxis()->SetTitle("W");
   mg4->GetXaxis()->CenterTitle();
   mg4->GetYaxis()->CenterTitle();
   mg4->Draw("a");
   TCanvas * c_4 = new TCanvas();
   gPad->SetLogy(true);
   mg4->Draw("A");
   c_4->SaveAs(Form("results/asymmetries/compare_tails_47_sig80_W_%d.pdf",aNumber));


   c->SaveAs(Form("results/asymmetries/compare_tails_47_%d.pdf",aNumber));
   c->SaveAs(Form("results/asymmetries/compare_tails_47_%d.png",aNumber));

   // -----------------------------------------
   TCanvas * c2 = new TCanvas("fandC","f_el and C_el");
   //c2->Divide(1,2);

   //c2->cd(1);
   mg5->SetTitle("1/f_{el}   E=4.7 GeV");
   mg5->Draw("a");
   mg5->GetXaxis()->SetTitle("W (GeV)");
   mg5->GetXaxis()->CenterTitle();
   mg5->GetYaxis()->CenterTitle();
   mg5->GetXaxis()->SetLimits(0.55,3.5); 
   mg5->GetYaxis()->SetRangeUser(1.0,2.0);
   c2->SaveAs(Form("results/asymmetries/compare_tails_f_el_47_W_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/compare_tails_f_el_47_W_%d.pdf",aNumber));

   //c2->cd(2);
   mg6->SetTitle("C_{el}   E=4.7 GeV");
   mg6->Draw("a");
   mg6->GetXaxis()->SetTitle("W (GeV)");
   mg6->GetXaxis()->CenterTitle();
   mg6->GetYaxis()->CenterTitle();
   mg6->GetXaxis()->SetLimits(0.55,3.5); 
   mg6->GetYaxis()->SetRangeUser(-0.35,1.0);

   c2->SaveAs(Form("results/asymmetries/compare_tails_C_el_47_W_%d.png",aNumber));
   c2->SaveAs(Form("results/asymmetries/compare_tails_C_el_47_W_%d.pdf",aNumber));

   return(0);
}
