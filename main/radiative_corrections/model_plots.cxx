Int_t model_plots(TCanvas * c = 0) {

   //gStyle->SetPaperSize(10.,10.);
   if(!c){
      c = new TCanvas();
      c->Divide(1,2,0,0,0);
      c1->cd(1);
      gPad->SetBottomMargin(0.001);
      gPad->DrawFrame(0,0,1,100);
      c1->cd(2);
      gPad->SetTopMargin(0.001);
      gPad->DrawFrame(0,0,1,100);

   }

   Double_t xmin = 0.0;
   Double_t xmax = 1.0;
   Double_t Q2   = 4.0;

   gSystem->Load("libNucDB");
   NucDBManager * manager = NucDBManager::GetManager();
   InSANEFunctionManager * fman  = InSANEFunctionManager::GetManager();
   TLegend * leg  = new TLegend(0.1, 0.6, 0.3, 0.9);
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegend * leg2 = new TLegend(0.1, 0.6, 0.3, 0.9);
   leg2->SetBorderSize(0);
   leg2->SetFillColor(0);
   leg2->SetFillStyle(0);
   //leg->SetHeader("A_{1}^{p} measurments");

   // ---------------------
   // A1p world data
   TList * measurementsList = manager->GetMeasurements("A1p");
   TMultiGraph * mgA1p = new TMultiGraph();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement * g1p = (NucDBMeasurement*)measurementsList->At(i);
      TGraphErrors * graph = g1p->BuildGraph("x");
      //graph->SetLineColor(1);
      //graph->SetMarkerColor(1);
      graph->SetMarkerSize(0.6);
      leg->AddEntry(graph, Form("%s A1p", g1p->GetExperimentName()), "ep");
      mgA1p->Add(graph,"p");
   }

   // ---------------------
   // A1p world data
   measurementsList = manager->GetMeasurements("A2p");
   TMultiGraph * mgA2p = new TMultiGraph();
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement * g1p = (NucDBMeasurement*)measurementsList->At(i);
      TGraphErrors * graph = g1p->BuildGraph("x");
      graph->SetMarkerSize(0.6);
      //graph->SetLineColor(1);
      //graph->SetMarkerColor(1);
      //leg->AddEntry(graph, Form("%s A1p", g1p->GetExperimentName()), "ep");
      mgA2p->Add(graph,"p");
   }

   // ---------------------
   fman->PrintStatus(); 
   // ---------------------
   // Models
   // --------------------
   InSANEAsymmetriesFromStructureFunctions * model1 = new InSANEAsymmetriesFromStructureFunctions();
   TF1 * model1_A1p = new TF1("model1_A1p",model1,&InSANEAsymmetryBase::EvaluateA1p,
         0.2, xmax, 1,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   model1_A1p->SetParameter(0,Q2);
   model1_A1p->SetLineColor(kRed);
   leg2->AddEntry(model1_A1p,model1->GetLabel(),"l");
   TF1 * model1_A2p = new TF1("model1_A2p",model1,&InSANEAsymmetryBase::EvaluateA2p,
         0.2, xmax, 1,"InSANEAsymmetriesFromStructureFunctions","EvaluateA2p");
   model1_A2p->SetParameter(0,Q2);
   model1_A2p->SetLineColor(kRed);

   // --------------------
   fman->CreateSFs(1);
   fman->CreatePolSFs(2);
   InSANEAsymmetriesFromStructureFunctions * model2 = new InSANEAsymmetriesFromStructureFunctions();
   TF1 * model2_A1p = new TF1("model2_A1p",model2,&InSANEAsymmetryBase::EvaluateA1p,
         xmin, xmax, 1,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   model2_A1p->SetParameter(0,Q2);
   model2_A1p->SetLineColor(kGreen+3);
   leg2->AddEntry(model2_A1p,model2->GetLabel(),"l");
   TF1 * model2_A2p = new TF1("model2_A2p",model2,&InSANEAsymmetryBase::EvaluateA2p,
         xmin, xmax, 1,"InSANEAsymmetriesFromStructureFunctions","EvaluateA2p");
   model2_A2p->SetParameter(0,Q2);
   model2_A2p->SetLineColor(kGreen+3);

   // --------------------
   fman->CreateSFs(6);
   fman->CreatePolSFs(6);
   InSANEAsymmetriesFromStructureFunctions * model3 = new InSANEAsymmetriesFromStructureFunctions();
   TF1 * model3_A1p = new TF1("model3_A1p",model3,&InSANEAsymmetryBase::EvaluateA1p,
         xmin, xmax, 1,"InSANEAsymmetriesFromStructureFunctions","EvaluateA1p");
   model3_A1p->SetParameter(0,Q2);
   model3_A1p->SetLineColor(kRed);
   model3_A1p->SetLineStyle(2);
   leg2->AddEntry(model3_A1p,model3->GetLabel(),"l");
   TF1 * model3_A2p = new TF1("model2_A2p",model2,&InSANEAsymmetryBase::EvaluateA2p,
         xmin, xmax, 1,"InSANEAsymmetriesFromStructureFunctions","EvaluateA2p");
   model3_A2p->SetParameter(0,Q2);
   model3_A2p->SetLineColor(kRed);
   model3_A2p->SetLineStyle(2);

   InSANEVirtualComptonAsymmetriesModel1 * model = new InSANEVirtualComptonAsymmetriesModel1();
   TF1 * model0_A1p = model->GetA1pFunction();
   model0_A1p->SetLineColor(kBlue);
   leg2->AddEntry(model0_A1p,"Model 0","l");
   Double_t * p1 = model->GetA1pParameters();
   p1[0] =  1.0;
   p1[1] =  0.0;
   p1[2] = -0.2;
   p1[3] = -0.6;
   p1[4] =  0.7;
   Double_t * p2 = model->GetA2pParameters();
   p2[0] =  0.25;
   p2[1] = -0.1;
   p2[2] =  0.3;
   p2[3] = -0.3;
   p2[4] =  0.9;

   // ---------------------
   c->cd(1);
   mgA1p->Draw("a");
   mgA1p->GetXaxis()->SetRangeUser(0.0,1.0);
   mgA1p->SetMinimum(-0.6);
   mgA1p->SetMaximum(1.8);
   model1_A1p->Draw("same");
   model2_A1p->Draw("same");
   model3_A1p->Draw("same");
   model->GetA1pFunction()->Draw("same");
   leg->Draw();
   gPad->Modified();

   c->cd(2);
   mgA2p->Draw("a");
   mgA2p->GetXaxis()->SetRangeUser(0.0,1.0);
   mgA2p->SetMinimum(-0.6);
   mgA2p->SetMaximum(1.1);
   model1_A2p->Draw("same");
   model2_A2p->Draw("same");
   model3_A2p->Draw("same");
   leg2->Draw();
   gPad->Modified();

   c->SaveAs("results/RC/model_plots.png");
   c->SaveAs("results/RC/model_plots.pdf");
   c->SaveAs("results/RC/model_plots.tex");

   return(0);
}
