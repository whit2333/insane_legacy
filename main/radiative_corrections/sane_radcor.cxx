Int_t sane_radcor(Int_t aNumber = 9) {

   Double_t theta_target = 180*degree;
   InSANERadiativeCorrections1D * RCs  = new InSANERadiativeCorrections1D();
   SANE_RCs_Model0             * RCs0 = new SANE_RCs_Model0();

   TLegend * leg = new TLegend(0.6,0.7,0.88,0.88);
   leg->SetFillColor(0); 
   leg->SetFillStyle(0); 
   leg->SetBorderSize(0); 
   TLegend * leg2 = new TLegend(0.6-0.42,0.7,0.88-0.45,0.88);
   leg2->SetFillColor(0); 
   leg2->SetFillStyle(0); 
   leg2->SetBorderSize(0); 

   RCs->SetBeamEnergy(5.9);
   RCs->SetThetaTarget(theta_target);
   RCs->InitCrossSections();
   RCs0->SetBeamEnergy(5.9);
   RCs0->SetThetaTarget(theta_target);
   RCs0->InitCrossSections();

   Double_t args[] = { 1.0, 40.0*degree,0.0*degree };

   std::cout << " Original : " << std::endl;
   std::cout << "born      : " << RCs->fBorn0->EvaluateXSec(args) << std::endl;;
   std::cout << "born      : " << RCs->fBorn1->EvaluateXSec(args) << std::endl;;
   std::cout << "born      : " << RCs->fBorn2->EvaluateXSec(args) << std::endl;;
   std::cout << "IRT       : " << RCs->fInelasticXSec0_rt->EvaluateXSec(args) << std::endl;;
   std::cout << "IRT       : " << RCs->fInelasticXSec1_rt->EvaluateXSec(args) << std::endl;;
   std::cout << "IRT       : " << RCs->fInelasticXSec2_rt->EvaluateXSec(args) << std::endl;;
   std::cout << " Improved? : " << std::endl;
   std::cout << "born      : " << RCs0->fBorn0->EvaluateXSec(args) << std::endl;;
   std::cout << "born      : " << RCs0->fBorn1->EvaluateXSec(args) << std::endl;;
   std::cout << "born      : " << RCs0->fBorn2->EvaluateXSec(args) << std::endl;;
   std::cout << "IRT       : " << RCs0->fInelasticXSec0_rt->EvaluateXSec(args) << std::endl;;
   std::cout << "IRT       : " << RCs0->fInelasticXSec1_rt->EvaluateXSec(args) << std::endl;;
   std::cout << "IRT       : " << RCs0->fInelasticXSec2_rt->EvaluateXSec(args) << std::endl;;
   //std::cout << "ERT       : " << RCs->fERT0_rt->EvaluateXSec(args) << std::endl;;
   //std::cout << "ERT       : " << RCs->fERT1_rt->EvaluateXSec(args) << std::endl;;
   //std::cout << "ERT       : " << RCs->fERT2_rt->EvaluateXSec(args) << std::endl;;

   //RCs->fBorn0->Print();
   //RCs->fInelasticXSec0_rt->Print();
   //fDiffXSec4->Print();

   //RCs->PrintConfiguration();
   //RCs0->PrintConfiguration();

   //-----------------------------------------------------
   Int_t    npx      = 100;
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 2.3;
   Double_t theta    = 35.0*degree;
   Double_t phi      = 0.0*degree;

   // ------------------------------
   TF1 * sigmaborn0 = new TF1("sigmaborn0", RCs->fBorn0, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaborn0->SetParameter(0,theta);
   sigmaborn0->SetParameter(1,phi);
   sigmaborn0->SetNpx(npx);
   sigmaborn0->SetLineWidth(2);
   sigmaborn0->SetLineColor(1);
   leg->AddEntry(sigmaborn0,"Born","l");
   leg2->AddEntry(sigmaborn0,"Born","l");

   // ------------------------------
   TF1 * sigmaborn1 = new TF1("sigmaborn1", RCs->fBorn1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaborn1->SetParameter(0,theta);
   sigmaborn1->SetParameter(1,phi);
   sigmaborn1->SetNpx(npx);
   sigmaborn1->SetLineWidth(2);
   sigmaborn1->SetLineColor(1);
   sigmaborn1->SetLineStyle(2);

   // ------------------------------
   TF1 * sigmaborn2 = new TF1("sigmaborn2", RCs->fBorn2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaborn2->SetParameter(0,theta);
   sigmaborn2->SetParameter(1,phi);
   sigmaborn2->SetNpx(npx);
   sigmaborn2->SetLineWidth(2);
   sigmaborn2->SetLineColor(1);
   sigmaborn2->SetLineStyle(2);

   // ------------------------------
   TF1 * sigmaborn10 = new TF1("sigmaborn10", RCs0->fBorn0, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaborn10->SetParameter(0,theta);
   sigmaborn10->SetParameter(1,phi);
   sigmaborn10->SetNpx(npx);
   sigmaborn10->SetLineColor(1);
   sigmaborn10->SetLineWidth(2);
   sigmaborn10->SetLineStyle(1);
   // ------------------------------
   TF1 * sigmaborn11 = new TF1("sigmaborn11", RCs0->fBorn1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaborn11->SetParameter(0,theta);
   sigmaborn11->SetParameter(1,phi);
   sigmaborn11->SetNpx(npx);
   sigmaborn11->SetLineColor(1);
   sigmaborn11->SetLineWidth(2);
   sigmaborn11->SetLineStyle(2);
   // ------------------------------
   TF1 * sigmaborn12 = new TF1("sigmaborn12", RCs0->fBorn2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaborn12->SetParameter(0,theta);
   sigmaborn12->SetParameter(1,phi);
   sigmaborn12->SetNpx(npx);
   sigmaborn12->SetLineColor(1);
   sigmaborn12->SetLineWidth(2);
   sigmaborn12->SetLineStyle(2);

   // ------------------------------
   TF1 * sigma0 = new TF1("sigma0", RCs->fInelasticXSec0_rt, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0->SetParameter(0,theta);
   sigma0->SetParameter(1,phi);
   sigma0->SetNpx(npx);
   sigma0->SetLineColor(2);
   sigma0->SetLineWidth(2);
   leg->AddEntry(sigma0,"Equiv.Rad.","l");
   leg2->AddEntry(sigma0,"Equiv.Rad.","l");
   // ------------------------------
   TF1 * sigma1 = new TF1("sigma1", RCs->fInelasticXSec1_rt, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(npx);
   sigma1->SetLineColor(2);
   sigma1->SetLineWidth(2);
   sigma1->SetLineStyle(2);
   // ------------------------------
   TF1 * sigma2 = new TF1("sigma2", RCs->fInelasticXSec2_rt, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(2);
   sigma2->SetLineWidth(2);
   sigma2->SetLineStyle(2);

   // ------------------------------
   TF1 * sigma10 = new TF1("sigma10", RCs0->fInelasticXSec0_rt, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma10->SetParameter(0,theta);
   sigma10->SetParameter(1,phi);
   sigma10->SetNpx(npx);
   sigma10->SetLineColor(4);
   sigma10->SetLineWidth(2);
   leg->AddEntry(sigma10,"POLRAD internal","l");
   leg2->AddEntry(sigma10,"POLRAD internal","l");
   // ------------------------------
   TF1 * sigma11 = new TF1("sigma11", RCs0->fInelasticXSec1_rt, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma11->SetParameter(0,theta);
   sigma11->SetParameter(1,phi);
   sigma11->SetNpx(npx);
   sigma11->SetLineColor(4);
   sigma11->SetLineWidth(2);
   sigma11->SetLineStyle(2);
   // ------------------------------
   TF1 * sigma12 = new TF1("sigma12", RCs0->fInelasticXSec2_rt, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma12->SetParameter(0,theta);
   sigma12->SetParameter(1,phi);
   sigma12->SetNpx(npx);
   sigma12->SetLineColor(4);
   sigma12->SetLineWidth(2);
   sigma12->SetLineStyle(2);

   // ------------------------------
   //
   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   // -------------------------------
   //
   gr = new TGraph(sigmaborn0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   TH1 * hist  = 0;
   gr = new TGraph(hist = sigmaborn1->DrawCopy("goff")->GetHistogram());
   TH1F * hb1 = (TH1F*)hist->Clone();
   mg->Add(gr,"l");

   gr = new TGraph(hist = sigmaborn2->DrawCopy("goff")->GetHistogram());
   TH1F * hb2 = (TH1F*)hist->Clone();
   mg->Add(gr,"l");

   gr = new TGraph(sigmaborn10->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   gr = new TGraph(sigmaborn11->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   gr = new TGraph(sigmaborn12->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   // -------------------------------
   //
   gr = new TGraph(sigma0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   gr = new TGraph(hist = sigma1->DrawCopy("goff")->GetHistogram());
   TH1F * hrc11 = (TH1F*)hist->Clone();
   mg->Add(gr,"l");

   gr = new TGraph(hist = sigma2->DrawCopy("goff")->GetHistogram());
   TH1F * hrc12 = (TH1F*)hist->Clone();
   mg->Add(gr,"l");

   gr = new TGraph(sigma10->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   gr = new TGraph(hist = sigma11->DrawCopy("goff")->GetHistogram());
   TH1F * hrc21 = (TH1F*)hist->Clone();
   mg->Add(gr,"l");

   gr = new TGraph(hist = sigma12->DrawCopy("goff")->GetHistogram());
   TH1F * hrc22 = (TH1F*)hist->Clone();
   mg->Add(gr,"l");

   // ------------------------------
   //
   TCanvas * c = new TCanvas();
   //gPad->SetLogy(true);

   mg->Draw("a");
   mg->GetYaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("d#sigma/dEd#Omega  (nb/GeV/sr)");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetXaxis()->SetTitle("E' (GeV)");
   leg->Draw();

   c->SaveAs(Form("results/radiative_corrections/sane_radcor_0_%d.png",aNumber));
   c->SaveAs(Form("results/radiative_corrections/sane_radcor_0_%d.pdf",aNumber));

   // ------------------------------
   //
   c = new TCanvas();
   TMultiGraph * mg2 = new TMultiGraph();

   // Born asym
   TH1F * bnum = (TH1F*) hb1->Clone("bornNum");
   TH1F * bden = (TH1F*) hb1->Clone("bornDen");
   bnum->Add(hb2,-1.0);
   bden->Add(hb2,1.0);
   bnum->Divide(bden);
   gr = new TGraph(bnum);
   gr->SetLineColor(1);
   gr->SetLineWidth(2);
   gr->SetLineStyle(1);
   mg2->Add(gr,"l");
   TH1F * hB0 = (TH1F*)bnum->Clone();

   // First radiated asym
   bnum = (TH1F*) hrc11->Clone("rc1Num");
   bden = (TH1F*) hrc11->Clone("rc1Den");
   bnum->Add(hrc12,-1.0);
   bden->Add(hrc12,1.0);
   bnum->Divide(bden);
   gr = new TGraph(bnum);
   gr->SetLineColor(2);
   gr->SetLineWidth(2);
   gr->SetLineStyle(1);
   mg2->Add(gr,"l");
   TH1F * hR1 = (TH1F*)bnum->Clone();

   // First radiated asym
   bnum = (TH1F*) hrc21->Clone("rc2Num");
   bden = (TH1F*) hrc21->Clone("rc2Den");
   bnum->Add(hrc22,-1.0);
   bden->Add(hrc22,1.0);
   bnum->Divide(bden);
   gr = new TGraph(bnum);
   gr->SetLineColor(4);
   gr->SetLineWidth(2);
   gr->SetLineStyle(1);
   mg2->Add(gr,"l");
   TH1F * hR2 = (TH1F*)bnum->Clone();

   mg2->Draw("a");
   mg2->GetXaxis()->CenterTitle(true);
   mg2->GetXaxis()->SetTitle("E' (GeV)");

   leg2->Draw();

   c->SaveAs(Form("results/radiative_corrections/sane_radcor_1_%d.png",aNumber));
   c->SaveAs(Form("results/radiative_corrections/sane_radcor_1_%d.pdf",aNumber));

   // ------------------------------
   //
   c = new TCanvas();
   TMultiGraph * mg3 = new TMultiGraph();
   TLegend * leg3 = new TLegend(0.6,0.7-0.42,0.88,0.88-0.45);
   leg3->SetFillColor(0); 
   leg3->SetFillStyle(0); 
   leg3->SetBorderSize(0); 

   hR1->Add(hB0,-1);
   gr = new TGraph(hR1);
   gr->SetLineColor(2);
   gr->SetLineWidth(2);
   gr->SetLineStyle(1);
   mg3->Add(gr,"l");
   leg3->AddEntry(gr,"Equiv. Rad.","l");

   hR2->Add(hB0,-1);
   gr = new TGraph(hR2);
   gr->SetLineColor(4);
   gr->SetLineWidth(2);
   gr->SetLineStyle(1);
   mg3->Add(gr,"l");
   leg3->AddEntry(gr,"POLRAD internal","l");


   mg3->Draw("a");
   mg3->GetXaxis()->CenterTitle(true);
   mg3->GetXaxis()->SetTitle("E' (GeV)");
   mg3->GetYaxis()->SetRangeUser(-0.06,0.03);

   leg3->Draw();

   c->SaveAs(Form("results/radiative_corrections/sane_radcor_2_%d.png",aNumber));
   c->SaveAs(Form("results/radiative_corrections/sane_radcor_2_%d.pdf",aNumber));

   // ------------------------------
   //
   c = new TCanvas();
   TMultiGraph * mg4 = new TMultiGraph();
   TLegend * leg4 = new TLegend(0.6,0.7-0.42,0.88,0.88-0.45);
   leg4->SetFillColor(0); 
   leg4->SetFillStyle(0); 
   leg4->SetBorderSize(0); 

   TH1F * diff = hR1->Clone();
   diff->Add(hR2,-1);
   gr = new TGraph(diff);
   gr->SetLineColor(1);
   gr->SetLineWidth(1);
   gr->SetLineStyle(1);
   mg4->Add(gr,"l");
   leg4->AddEntry(gr,"A^{corr}_{ER} - A^{corr}_{POL} ","l");

   mg4->Draw("a");
   mg4->GetXaxis()->CenterTitle(true);
   mg4->GetXaxis()->SetTitle("E' (GeV)");
   mg4->GetYaxis()->SetRangeUser(-0.06,0.03);

   leg4->Draw();

   c->SaveAs(Form("results/radiative_corrections/sane_radcor_3_%d.png",aNumber));
   c->SaveAs(Form("results/radiative_corrections/sane_radcor_3_%d.pdf",aNumber));


   return 0;
}

