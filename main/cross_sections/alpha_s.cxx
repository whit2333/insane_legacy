Int_t alpha_s(){

   InSANEStrongCouplingConstant*  as = new InSANEStrongCouplingConstant();

   TF1  * fAlphaS    = new TF1("fAlphaS",[=](double* x, double* p){ return (*as)(x[0]*x[0]);}, 1,10,0);

   TCanvas * c = new TCanvas();

   fAlphaS->Draw();

   c->SaveAs("results/cross_sections/alpha_s.png");

   return 0;
}
