class  Igam1 {
   public:
      Igam1(Double_t rl  = 0.01) { fRadLen = rl; fEBeam = 5.9; }
      // use constructor to customize your function object
      Double_t fRadLen;
      Double_t fEBeam;
      double operator() (double *x, double *p) {
         double k  = x[0];
         double rl = p[0];
         double E0 = p[1];
         double kavg = InSANE::Kine::I_gamma_1_k_avg(rl,0.001,E0);
         double EQ = kavg/E0;
         return( InSANE::Kine::I_gamma_1(rl,E0,k)*k);
      }
};
class  Igam2 {
   public:
      Igam2(Double_t rl  = 0.01) { fRadLen = rl; fEBeam = 5.9; }
      // use constructor to customize your function object
      Double_t fRadLen;
      Double_t fEBeam;
      double operator() (double *x, double *p) {
         double k  = x[0];
         double rl = p[0];
         double E0 = p[1];
         double kavg = InSANE::Kine::I_gamma_1_k_avg(rl,0.001,E0);
         double EQ = kavg/E0;
         return( InSANE::Kine::I_gamma_1_approx(rl,E0,k)*k );
      }
};
Int_t brem_spectrum(Int_t aNumber = 0) {

   double Emin = 0.5;
   double Emax = 5.9;

   Igam1 * fobj1 = new Igam1();
   TF1   * f1 = new TF1("f1", fobj1, Emin, Emax, 2, "Igam1");    // create TF1 class.
   f1->SetParameter(0,0.01);
   f1->SetParameter(1,5.9);
   f1->SetLineWidth(1);

   Igam2 * fobj2 = new Igam2();
   TF1   * f2 = new TF1("f2", fobj2, Emin, Emax, 2, "Igam2");    // create TF1 class.
   f2->SetParameter(0,0.01);
   f2->SetParameter(1,5.9);
   f2->SetLineWidth(1);
   f2->SetLineColor(4);

   TCanvas * c = new TCanvas();
   //c->Divide(1,2);

   //c->cd(1);
   f1->DrawCopy()->GetHistogram()->GetYaxis()->SetRangeUser(0.0,0.02);
   f2->DrawCopy("same");

   f1->SetParameter(0,0.015);
   f2->SetParameter(0,0.015);
   //c->cd(1);
   f1->SetLineStyle(2);
   f2->SetLineStyle(2);
   f1->DrawCopy("same");
   f2->DrawCopy("same");

   c->SaveAs("results/cross_sections/brem_spectrum.png");

   return 0;
}
