// Test the F1F209eInclusiveDiffXSec class 

double gCONV = 1E+3;

void SetEp(Double_t EpMin,Double_t EpMax,vector<Double_t> &Ep); 
TGraph *GetTGraph(vector<Double_t>,vector<Double_t>); 

void UnpolXSModels(){

   // Es and Ep (GeV) 
   Double_t Es    = 5.89;  
   Double_t EpMin = 0.60; 
   Double_t EpMax = 2.00; 
   Double_t theta = 45.0*degree; 
   Double_t PHI = 0.0*degree; 

   vector<double> ES,NU,W,XS;

   InSANENucleus::NucleusType Target = InSANENucleus::k3He;

   // Set the phase space 
   // Ep = scattered electron energy 
   InSANEPhaseSpaceVariable *Ep = new InSANEPhaseSpaceVariable("Ep","E_{p}"); 
   Ep->SetVariableUnits("GeV");
   Ep->SetMinimum(EpMin); 
   Ep->SetMaximum(EpMax); 
   Ep->SetParticleIndex(0);
   // ph = horizontal scattering angle   
   InSANEPhaseSpaceVariable *ph = new InSANEPhaseSpaceVariable("ph","#phi"); 
   ph->SetVariableUnits("rad");
   ph->SetMinimum(43.0*degree); 
   ph->SetMaximum(46.0*degree); 
   ph->SetParticleIndex(1);
   // th = vertical scattering angle   
   InSANEPhaseSpaceVariable *th = new InSANEPhaseSpaceVariable("th","#theta"); 
   th->SetVariableUnits("rad");
   th->SetMinimum(-2.5*degree); 
   th->SetMaximum( 2.5*degree); 
   th->SetParticleIndex(2);
   // Add Ep, ph and th to the phase space 
   InSANEPhaseSpace *PS = new InSANEPhaseSpace(); 
   PS->SetDimension(0); 
   PS->AddVariable(Ep);   
   PS->AddVariable(ph);
   PS->AddVariable(th);
   PS->Print();    

   // F1F209  
   F1F209eInclusiveDiffXSec *F1F209uXS = new F1F209eInclusiveDiffXSec(); 
   F1F209uXS->SetPhaseSpace(PS);
   F1F209uXS->SetTargetType(Target); 
   F1F209uXS->SetBeamEnergy(Es); 
   // use the adjusted model that fits the data in the kinematical range of E06-014.
   F1F209uXS->UseModifiedModel('y');   

   // BBS 
   BBSUnpolarizedPDFs *BBSuPDF = new BBSUnpolarizedPDFs(); 
   BBSuPDF->UseQ2Interpolation();
   InSANEStructureFunctionsFromPDFs * BBSuSFs = new InSANEStructureFunctionsFromPDFs();
   BBSuSFs->SetUnpolarizedPDFs(BBSuPDF);

   InSANEInclusiveDiffXSec *BBSuXS = new InSANEInclusiveDiffXSec();
   BBSuXS->SetUnpolarizedStructureFunctions(BBSuSFs); 
   BBSuXS->SetPhaseSpace(PS); 
   BBSuXS->SetBeamEnergy(Es); 
   BBSuXS->SetTargetType(Target); 

   // CTEQ6eInclusiveDiffXSec *CTEQuXS = new CTEQ6eInclusiveDiffXSec();
   // CTEQuXS->SetPhaseSpace(PS); 
   // CTEQuXS->SetA(A); 
   // CTEQuXS->SetZ(Z); 
   // CTEQuXS->SetBeamEnergy(Es); 

   const int N = PS->GetDimension();  

   Double_t par[3]; 
   Double_t farg,carg,carg2,w_arg; 
   vector<Double_t> EPr,f1f2_xs,bbs_xs,bbs_xs2; 

   SetEp(EpMin,EpMax,EPr); 

   const int M = EPr.size();
   for(int i=0;i<M;i++){
      par[0] = EPr[i]; 
      par[1] = theta; 
      par[2] = PHI; 
      farg = F1F209uXS->EvaluateXSec(par);
      carg = BBSuXS->EvaluateXSec(par); 
      carg2 = GetXS_dxdy(BBSuSFs,Es,EPr[i],theta);
      f1f2_xs.push_back(farg);
      bbs_xs.push_back(carg);
      bbs_xs2.push_back(carg2);
      cout << farg << "\t" << carg << "\t" << carg2 << endl;
      w_arg = InSANE::Kine::W_EEprimeTheta(Es,EPr[i],theta); 
      ES.push_back(Es*gCONV); 
      NU.push_back( (Es-EPr[i])*gCONV ); 
      W.push_back(w_arg*gCONV); 
      XS.push_back(carg);
   }

   PrintToFile("bbs_xs-05.dat",ES,NU,W,XS); 

   double EP = 0.637164; 
   // double XS = GetXS_dxdy(BBSuSFs,Es,EP,theta); 
   // cout << "Ep = " << EP << "\t" << XS << endl;

   TGraph *fG  = GetTGraph(EPr,f1f2_xs); 
   TGraph *cG  = GetTGraph(EPr,bbs_xs); 
   TGraph *cG2 = GetTGraph(EPr,bbs_xs2); 

   fG->SetLineColor(kBlack);
   cG->SetLineColor(kRed); 
   cG2->SetLineColor(kBlue); 

   TMultiGraph *MG = new TMultiGraph();
   MG->Add(fG ,"c");
   MG->Add(cG ,"c");
   MG->Add(cG2,"c");

   TLegend *Leg = new TLegend(0.6,0.6,0.8,0.8); 
   Leg->SetFillColor(kWhite); 
   Leg->AddEntry(fG ,"F1F209 (^{3}He, 45#circ)","l");
   Leg->AddEntry(cG ,"BBS    (^{3}He, 45#circ)","l");
   Leg->AddEntry(cG2,"BBS (invariant method, no Callan-Gross) (^{3}He, 45#circ)","l");

   TString Title = Form("Cross Section Models"); 
   TString xAxisTitle = Form("E_{p} (GeV)");
   TString yAxisTitle = Form("#frac{d^{2}#sigma}{d#OmegadE_{p}} (nb/GeV/sr)");

   TCanvas *c1 = new TCanvas("c1","Cross Section Model",700,500);
   c1->SetFillColor(kWhite); 

   c1->cd();
   MG->Draw("A");
   MG->SetTitle(Title);
   MG->GetXaxis()->SetTitle(xAxisTitle);
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle(yAxisTitle);
   MG->GetYaxis()->CenterTitle();
   MG->Draw("A");
   Leg->Draw("same");
   c1->Update();


}
//__________________________________________________________________________________
void PrintToFile(TString outpath,vector<double> Es,vector<double> nu,vector<double> W,vector<double> xs){

   int N = Es.size();
   double ZERO = 0.;

   ofstream outfile; 
   outfile.open(outpath);
   if(outfile.fail()){
      cout << "Cannot open the file: " << outpath << endl;
      exit(1); 
   }else{
      for(int i=0;i<N;i++){
         outfile << Form("%.1f",Es[i]) << "\t" 
                 << Form("%.1f",nu[i]) << "\t" 
                 << Form("%.2f",W[i] ) << "\t"
                 << Form("%.3E",xs[i]) << "\t" 
                 << Form("%.3E",ZERO ) << endl;
      }
      cout << "The data has been written to the file: " << outpath << endl;
   }


}
//__________________________________________________________________________________
double GetXS_dxdy(InSANEStructureFunctionsFromPDFs *SF,double Es,double Ep,double th){

   double alpha    = fine_structure_const; 
   double M        = M_p/GeV; 
   double m        = M_e/GeV; 
   double nu       = Es - Ep; 
   double x        = InSANE::Kine::xBjorken_EEprimeTheta(Es,Ep,th); 
   double y        = nu/Es; 
   double J        = Ep/(M*nu); 
   double Q2       = InSANE::Kine::Qsquared(Es,Ep,th);
   double R        = InSANE::Kine::R1998(x,Q2);
   double gamma2   = (2.*M*x)*(2.*M*x)/Q2;  
   double S        = 2.*M*Es; 
   double X        = (1.-y)*S; 
   double lambda_s = S*S - 4.*m*m*M*M; 
   double sqls     = TMath::Sqrt(lambda_s); 
   double S_x      = S - X; 

   double F1       = SF->F1He3(x,Q2);
   double F2       = 2.*x*F1*(1. + R)/(1. + gamma2);
   // double F2       = SF->F2He3(x,Q2);

   double F1Tilde  = F1; 
   double F2Tilde  = F2*(2.*M*M/Q2)*x; 
 
   double T1       = 4.*pi*alpha*alpha/lambda_s; 
   double T2       = S*S_x/(Q2*Q2); 
   double T3       = (Q2 - 2.*m*m)*F1Tilde; 
   double T4       = (S*X - M*M*Q2)*(1./(2.*M*M))*F2Tilde; 

   double xs_dxdy  = T1*T2*(T3 + T4);
   double xs       = J*xs_dxdy*hbarc2_gev_nb/(2.*pi);    // 2pi takes care of the phi angle  
   return xs;

}
//__________________________________________________________________________________
void SetEp(Double_t EpMin,Double_t EpMax,vector<Double_t> &Ep){

   Double_t delta = EpMin; 
   Double_t step = 0.010; // in GeV 

   while(delta<=EpMax){
      Ep.push_back(delta);
      delta += step; 
   }


}
//__________________________________________________________________________________
TGraph *GetTGraph(vector<Double_t> x,vector<Double_t> y){

   const int N = x.size();
   TGraph *g = new TGraph(N,&x[0],&y[0]); 
   g->SetMarkerStyle(20);
   g->SetLineWidth(2);
   return g; 

}
