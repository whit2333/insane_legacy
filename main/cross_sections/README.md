cross_sections
--------------

The directories "cross_sections/inclusive" and "cross_sections/exclusive" are for cross section example scripts.
Each script should be an example of ONLY ONE cross section. It should run as
script from the "main directory" without the input of any parameters. 
It is meant a simple debugging tool and template for usage. 
There can be multiple scripts for a single cross section class, but not for a simple change in kinematics. 


