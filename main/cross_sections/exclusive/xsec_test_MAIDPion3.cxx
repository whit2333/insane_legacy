Int_t xsec_test_MAIDPion3(){

   Double_t theta_target = 180*degree;
   TVector3 pt(0.0,0.0,1.0);
   pt.SetMagThetaPhi(1.0,theta_target,0.0);

   Double_t beamEnergy = 5.9; //GeV
   Double_t E_pi       = 1.41; 
   Double_t theta      = 5.*degree;
   Double_t theta1     = 30.*degree;
   Double_t theta2     = 40.*degree;
   Double_t phi        = 0.0*degree;  
   Double_t theta_pi   = 35.0*degree;
   Double_t theta_pi0  = 5.0*degree;
   Double_t theta_pi1  = 10.0*degree;
   Double_t theta_pi2  = 15.0*degree;
   Double_t phi_pi     = 180.0*degree;

   InSANENucleus::NucleusType TargetType    = InSANENucleus::kProton; 
   Double_t A          = 1;   
   Double_t Z          = 1;   

   // For plotting cross sections 
   Int_t    npx      = 100;
   Int_t    npar     = 4;
   Double_t Emin     = 1.5;
   Double_t Emax     = 3.0;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;

   //-----------------------
   // MAID xsec 
   TString pion               = "pi0";
   TString nucleon            = "p";
   TString target             = "p";
   MAIDExclusivePionDiffXSec * fDiffXSec = new MAIDExclusivePionDiffXSec(pion,nucleon);
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->SetTargetPolarization(pt);
   fDiffXSec->SetHelicity(0.0);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &MAIDExclusivePionDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"MAIDExclusivePionDiffXSec","EnergyDependentXSec");
   sigmaE->SetNpx(200);
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetParameter(2,theta_pi);
   sigmaE->SetParameter(3,phi_pi);

   //-----------------------
   MAIDExclusivePionDiffXSec * fDiffXSec1 = new MAIDExclusivePionDiffXSec(pion,nucleon);
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetTargetType(TargetType);
   fDiffXSec1->SetHelicity(1.0);
   fDiffXSec1->SetTargetPolarization(pt);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &MAIDExclusivePionDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"MAIDExclusivePionDiffXSec","EnergyDependentXSec");
   sigmaE1->SetNpx(npx);
   sigmaE1->SetParameter(0,theta);
   sigmaE1->SetParameter(1,phi);
   sigmaE1->SetParameter(2,theta_pi);
   sigmaE1->SetParameter(3,phi_pi);
   //-----------------------
   MAIDExclusivePionDiffXSec * fDiffXSec2 = new MAIDExclusivePionDiffXSec(pion,nucleon);
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetType(TargetType);
   fDiffXSec2->SetHelicity(-1.0);
   fDiffXSec2->SetTargetPolarization(pt);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &MAIDExclusivePionDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"MAIDExclusivePionDiffXSec","EnergyDependentXSec");
   sigmaE2->SetNpx(npx);
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetParameter(2,theta_pi);
   sigmaE2->SetParameter(3,phi_pi);
   //-----------------------
   // MAID xsec 
   //MAIDExclusivePionDiffXSec2 * fDiffXSec1 = new MAIDExclusivePionDiffXSec2(pion,nucleon);
   //fDiffXSec1->SetBeamEnergy(beamEnergy);
   //fDiffXSec1->SetA(A);
   //fDiffXSec1->SetZ(Z);
   //fDiffXSec1->SetTargetType(TargetType);
   //fDiffXSec1->InitializePhaseSpaceVariables();
   //fDiffXSec1->InitializeFinalStateParticles();
   //fDiffXSec1->Refresh();
   //TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &MAIDExclusivePionDiffXSec::EnergyDependentXSec, 
   //      Emin, Emax, npar,"MAIDExclusivePionDiffXSec","EnergyDependentXSec");
   //sigmaE1->SetNpx(200);
   //sigmaE1->SetParameter(0,theta);
   //sigmaE1->SetParameter(1,phi);
   //sigmaE1->SetParameter(2,theta_pi);
   //sigmaE1->SetParameter(3,phi_pi);

   //-----------------------
   
   TString title =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 
   TLegend * leg = new TLegend(0.7,0.7,0.85,0.85);
   TLatex  * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);
   //-----------------------
   TCanvas * c =  new TCanvas();
   gPad->SetLogy(true);
   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   sigmaE->SetLineWidth(1);
   sigmaE1->SetLineWidth(1);

   sigmaE->SetLineColor(kBlack);
   gr = new TGraph(sigmaE->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,Form("#theta_{e}=%.0f, #theta_{#pi}=%.0f",
                          sigmaE->GetParameter(0)/degree,sigmaE->GetParameter(2)/degree),"l");

   sigmaE1->SetLineColor(kBlue);
   gr = new TGraph(sigmaE1->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,Form("#theta_{e}=%.0f, #theta_{#pi}=%.0f",
                          sigmaE->GetParameter(0)/degree,sigmaE->GetParameter(2)/degree),"l");

   sigmaE2->SetLineColor(kRed);
   gr = new TGraph(sigmaE2->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");

   mg->Draw("a");
   //leg->Draw();
   latex->SetTextSize(0.025);
   latex->DrawLatex(0.5,0.8,Form("E=%2.1f (GeV)",beamEnergy));
   latex->DrawLatex(0.5,0.77,Form("#phi_{e}=%.0f , #phi_{#pi}=%.0f",phi/degree,phi_pi/degree));

   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);
   gPad->Modified();

   c->SaveAs("results/cross_sections/exclusive/xsec_test_MAIDPion3.png");
   c->SaveAs("results/cross_sections/exclusive/xsec_test_MAIDPion3.pdf");
   c->SaveAs("results/cross_sections/exclusive/xsec_test_MAIDPion3.tex");

   //sigmaE->SetTitle(title);
   //sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   //sigmaE->GetXaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   //sigmaE->GetYaxis()->CenterTitle(true);
   //sigmaE->Draw();

   return 0;
}
