//class  MyFunctionObject0 {
//   public:
//      MyFunctionObject0(InSANEDiffXSec * y = 0) { xsec = y; }
//      InSANEDiffXSec * xsec;
//      double args[6];
//      double operator() (double *x, double *p) {
//         if(!yield) return 0;
//         args[0] = x[0];
//         args[1] = p[0];
//         args[2] = p[1];
//         int i_mat = int(p[2]);
//         return( yield->CalculateTotalRate(i_mat,args,11) );
//      }
//};
//______________________________________________________________________________
Int_t xsec_test_Moller(){

   Double_t beamEnergy = 11.0; //GeV
   Double_t theta      = 0.1*degree;
   Double_t phi        = 0.0*degree;  

   // For plotting cross sections 
   Int_t    npar     = 1;
   Double_t minTheta = 0.001;
   Double_t maxTheta = 2.0*degree;

   InSANEMollerDiffXSec * fDiffXSec = new InSANEMollerDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->UsePhaseSpace(true);
   fDiffXSec->Print();

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEExclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEExclusiveDiffXSec","PolarAngleDependentXSec");
   sigmaE->SetParameter(0,phi);
   sigmaE->SetLineColor(kRed);

   TCanvas * c = new TCanvas();

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   TGraph * gr = 0;
   TMultiGraph * mg = new TMultiGraph();
   
   gr = new TGraph(sigmaE->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"lp");
   leg->AddEntry(gr,"0.5 deg, p","l");

   fDiffXSec->SetTargetNucleus(InSANENucleus::Fe56());
   sigmaE->SetLineColor(kBlue);
   gr = new TGraph(sigmaE->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"lp");
   leg->AddEntry(gr,"0.5 deg, Fe","l");

   mg->Draw("a");
   gPad->SetLogy(true);
   mg->GetXaxis()->SetTitle("#theta");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("[nb/sr]");
   mg->GetYaxis()->CenterTitle(true);
   leg->Draw();

   return 0;
}

