//class  MyFunctionObject0 {
//   public:
//      MyFunctionObject0(InSANEDiffXSec * y = 0) { xsec = y; }
//      InSANEDiffXSec * xsec;
//      double args[6];
//      double operator() (double *x, double *p) {
//         if(!yield) return 0;
//         args[0] = x[0];
//         args[1] = p[0];
//         args[2] = p[1];
//         int i_mat = int(p[2]);
//         return( yield->CalculateTotalRate(i_mat,args,11) );
//      }
//};
//______________________________________________________________________________
Int_t xsec_test_InSANEepElastic(){

   Double_t beamEnergy = 1.0; //GeV
   Double_t Eprime     = 1.41; 
   Double_t theta      = 25.*degree;
   Double_t phi        = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 3.0;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;

   InSANEepElasticDiffXSec * fDiffXSec = new InSANEepElasticDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->UsePhaseSpace(false);

   TString title =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   //TString xAxisTitle = "E' (GeV)"; 
   TString xAxisTitle = "#theta"; 

   //TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEExclusiveDiffXSec::EnergyDependentXSec, 
   //      Emin, Emax, npar,"InSANEExclusiveDiffXSec","EnergyDependentXSec");
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEExclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEExclusiveDiffXSec","PolarAngleDependentXSec");
   sigmaE->SetParameter(0,phi);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetLineColor(kRed);
   sigmaE->Draw();
   sigmaE->SetTitle(title);
   sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   sigmaE->GetXaxis()->CenterTitle(true);
   sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   sigmaE->GetYaxis()->CenterTitle(true);
   sigmaE->Draw();

   return 0;
}
