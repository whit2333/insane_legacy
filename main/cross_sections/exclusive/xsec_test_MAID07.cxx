Int_t xsec_test_MAID07(){

   Double_t beamEnergy = 2.0; //
   Double_t theta      = 50*degree;
   Double_t phi        = 0*degree;
   Double_t theta_pi   = 20*degree;
   Double_t phi_pi     = 100*degree;
   Double_t Emin       = 0.3;
   Double_t Emax       = 1.9;

   MAIDExclusivePionDiffXSec3 * xsec = new MAIDExclusivePionDiffXSec3("n","piminus");
   xsec->SetBeamEnergy(3.0);
   Double_t args[] = {1.0,50.0*degree,0.0*degree,20*degree,100*degree};
   Double_t val = xsec->EvaluateXSec(xsec->GetDependentVariables(args));
   std::cout << "xsec = " << val << std::endl;

   xsec->SetTargetNucleus(InSANENucleus::Proton());
   xsec->InitializePhaseSpaceVariables();
   xsec->InitializeFinalStateParticles();
   xsec->UsePhaseSpace(false);

   TString title      = xsec->GetPlotTitle();
   TString yAxisTitle = xsec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TCanvas * c = new TCanvas();

   TF1 * sigmaE = new TF1("sigma", xsec, &MAIDExclusivePionDiffXSec3::EnergyDependentXSec, 
                           Emin, Emax, 4,"MAIDExclusivePionDiffXSec3","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetParameter(2,theta_pi);
   sigmaE->SetParameter(3,phi_pi);
   sigmaE->SetLineColor(kRed);
   sigmaE->Draw();
   sigmaE->SetTitle(title);
   sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   sigmaE->GetXaxis()->CenterTitle(true);
   sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   sigmaE->GetYaxis()->CenterTitle(true);
   sigmaE->Draw();

   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);

   latex->DrawLatex(0.7,0.8,Form("E=%.2f (GeV)",beamEnergy));
   latex->DrawLatex(0.7,0.7,Form("#theta_{e}=%.1f (deg), #phi_{e}=%.1f (deg)",theta/degree,phi/degree));
   latex->DrawLatex(0.7,0.6,Form("#theta_{#pi}=%.1f (deg), #phi_{#pi}=%.1f (deg)",theta_pi/degree,phi_pi/degree));

   c->SaveAs("results/cross_sections/exclusive/xsec_test_MAID07.png");


   return 0;
}
