Int_t xsec_test_MAIDPion4(){

   Double_t beamEnergy = 4.7; //GeV

   Double_t Eprime     = 3.0; 
   Double_t theta      = 1.0*degree;
   Double_t phi        = 20.0*degree;  

   Double_t theta_pi   = 40.0*degree;
   Double_t phi_pi     = 200.0*degree;

   TVector3 pt(1.0,0.0,0.0);
   InSANENucleus::NucleusType TargetType    = InSANENucleus::kProton; 

   // For plotting cross sections 
   Int_t    npx      = 200;
   Int_t    npar     = 4;
   Double_t minTheta = 0.5*degree;
   Double_t maxTheta = 45.0*degree;

   //-----------------------
   // MAID xsec 
   TString pion               = "pi0";
   TString nucleon            = "p";
   TString target             = "p";
   MAIDExclusivePionDiffXSec * fDiffXSec = new MAIDExclusivePionDiffXSec(pion,nucleon);
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->SetHelicity(0.0);
   fDiffXSec->SetTargetPolarization(pt);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &MAIDExclusivePionDiffXSec::AngleDependentXSec, 
         minTheta, maxTheta, npar,"MAIDExclusivePionDiffXSec","AngleDependentXSec");
   sigmaE->SetNpx(npx);
   sigmaE->SetParameter(0,Eprime);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetParameter(2,theta_pi);
   sigmaE->SetParameter(3,phi_pi);

   //-----------------------
   // MAID xsec 
   MAIDExclusivePionDiffXSec * fDiffXSec1 = new MAIDExclusivePionDiffXSec(pion,nucleon);
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetTargetType(TargetType);
   fDiffXSec1->SetHelicity(1.0);
   fDiffXSec1->SetTargetPolarization(pt);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigma1 = new TF1("sigma1", fDiffXSec1, &MAIDExclusivePionDiffXSec::AngleDependentXSec, 
         minTheta, maxTheta, npar,"MAIDExclusivePionDiffXSec","AngleDependentXSec");
   sigma1->SetNpx(npx);
   sigma1->SetParameter(0,Eprime);
   sigma1->SetParameter(1,phi);
   sigma1->SetParameter(2,theta_pi);
   sigma1->SetParameter(3,phi_pi);

   //-----------------------
   // MAID xsec 
   MAIDExclusivePionDiffXSec * fDiffXSec2 = new MAIDExclusivePionDiffXSec(pion,nucleon);
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetType(TargetType);
   fDiffXSec2->SetHelicity(-1.0);
   fDiffXSec2->SetTargetPolarization(pt);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigma2 = new TF1("sigma2", fDiffXSec2, &MAIDExclusivePionDiffXSec::AngleDependentXSec, 
         minTheta, maxTheta, npar,"MAIDExclusivePionDiffXSec","AngleDependentXSec");
   sigma2->SetNpx(npx);
   sigma2->SetParameter(0,Eprime);
   sigma2->SetParameter(1,phi);
   sigma2->SetParameter(2,theta_pi);
   sigma2->SetParameter(3,phi_pi);


   //-----------------------
   
   TString title      = fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "#theta_{e} (radians)";

   TLegend * leg = new TLegend(0.7,0.7,0.85,0.85);
   TLatex  * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);
   //-----------------------
   TCanvas * c =  new TCanvas();
   gPad->SetLogy(true);
   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   sigmaE->SetLineWidth(1);
   sigma1->SetLineWidth(1);

   sigmaE->SetLineColor(1);
   gr = new TGraph(sigmaE->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   //leg->AddEntry(gr,Form("#theta_{e}=%.0f, #theta_{#pi}=%.0f",
   //                       sigmaE->GetParameter(0)/degree,sigmaE->GetParameter(2)/degree),"l");

   sigma1->SetLineColor(2);
   gr = new TGraph(sigma1->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");

   sigma2->SetLineColor(4);
   gr = new TGraph(sigma2->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");

   mg->Draw("a");
   latex->SetTextSize(0.025);
   latex->DrawLatex(0.5,0.85,Form("E=%2.1f (GeV)",beamEnergy));
   latex->DrawLatex(0.5,0.8,Form("E'=%2.1f (GeV)",Eprime));
   latex->DrawLatex(0.5,0.75,Form("    #theta_{#pi}=%.0f",theta_pi/degree));
   latex->DrawLatex(0.5,0.70,Form("#phi_{e}=%.0f , #phi_{#pi}=%.0f",phi/degree,phi_pi/degree));

   gPad->Modified();
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   c->SaveAs("results/cross_sections/exclusive/xsec_test_MAIDPion4.png");
   c->SaveAs("results/cross_sections/exclusive/xsec_test_MAIDPion4.pdf");
   c->SaveAs("results/cross_sections/exclusive/xsec_test_MAIDPion4.tex");

   return 0;
}
