//class  MyFunctionObject0 {
//   public:
//      MyFunctionObject0(InSANEDiffXSec * y = 0) { xsec = y; }
//      InSANEDiffXSec * xsec;
//      double args[6];
//      double operator() (double *x, double *p) {
//         if(!yield) return 0;
//         args[0] = x[0];
//         args[1] = p[0];
//         args[2] = p[1];
//         int i_mat = int(p[2]);
//         return( yield->CalculateTotalRate(i_mat,args,11) );
//      }
//};
//______________________________________________________________________________
Int_t xsec_test_Elastic_He4(){

   using namespace InSANE::physics;

   Double_t beamEnergy = 1.0; //GeV
   Double_t Eprime     = 1.41; 
   Double_t theta      = 25.*degree;
   Double_t phi        = 0.0*degree;  

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 3.0;
   Double_t minTheta = 1.0*degree;
   Double_t maxTheta = 30.0*degree;

   Helium4ElasticDiffXSec * fDiffXSec = new Helium4ElasticDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetNucleus(InSANENucleus::He4());
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->UsePhaseSpace(false);

   TString title =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   //TString xAxisTitle = "E' (GeV)"; 
   TString xAxisTitle = "#theta"; 

   //auto xsec_f1 = [=](double* xs, double *ps) {
   //   double p1 = xs[0];
   //   return fDiffXSec;
   //};
   //TF1 * n_He4 = new TF1("nHe4", nHe4, 0.0,1.0,0);

   //TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEExclusiveDiffXSec::EnergyDependentXSec, 
   //      Emin, Emax, npar,"InSANEExclusiveDiffXSec","EnergyDependentXSec");
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEExclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEExclusiveDiffXSec","PolarAngleDependentXSec");
   sigmaE->SetParameter(0,phi);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetLineColor(kRed);
   sigmaE->Draw();
   sigmaE->SetTitle(title);
   sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   sigmaE->GetXaxis()->CenterTitle(true);
   sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   sigmaE->GetYaxis()->CenterTitle(true);
   sigmaE->Draw();

   return 0;
}
