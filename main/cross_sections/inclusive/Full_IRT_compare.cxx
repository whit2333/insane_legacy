Int_t Full_IRT_compare(Int_t aNumber = 0) {

   Double_t beamEnergy = 5.89; //GeV
   Double_t Eprime     = 1.1; 
   Double_t theta      = 40.0*degree;
   Double_t phi        = 0.0*degree;  
   Double_t theta_target = 40*degree;

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   Int_t    npx      = 20;
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 2.5;

   //-----------------------------------
   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->SetFillColor(0); 
   leg->SetFillStyle(0); 

   //-----------------------------------
   InSANERadiator<InSANEPOLRADBornDiffXSec> * born0 = new  InSANERadiator<InSANEPOLRADBornDiffXSec>();
   born0->SetBeamEnergy(beamEnergy);
   born0->GetPOLRAD()->SetIRTMethod(InSANEPOLRAD::kAnglePeaking);;
   born0->GetPOLRAD()->SetHelicity(1);
   born0->SetTargetNucleus(InSANENucleus::Proton());
   born0->GetPOLRAD()->SetTargetPolarization(1.0);
   born0->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   born0->InitializePhaseSpaceVariables();
   born0->InitializeFinalStateParticles();
   born0->Refresh();

   TF1 * sigmaE0 = new TF1("sigma0", born0, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE0->SetParameter(0,theta);
   sigmaE0->SetParameter(1,phi);
   sigmaE0->SetNpx(npx);

   //sigmaE->SetLineColor(kBlack);
   //-----------------------------------
   InSANERadiator<InSANEPOLRADInelasticTailDiffXSec> * fDiffXSec = new  InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>();
   fDiffXSec->SetExternalOnly();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->GetPOLRAD()->SetIRTMethod(InSANEPOLRAD::kAnglePeaking);;
   fDiffXSec->GetPOLRAD()->SetHelicity(1);
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetNpx(npx);
   //sigmaE->SetLineColor(kBlack);

   // ------------------------------
   //
   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   gr = new TGraph(sigmaE->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   fDiffXSec->GetPOLRAD()->SetHelicity(-1);
   gr = new TGraph(sigmaE->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");

   // ------------------------------
   //
   TCanvas * c = new TCanvas("xsec_test_POLRAD_IRT","xsec_test_POLRAD_IRT");

   mg->Draw("a");

   c->SaveAs("results/cross_sections/inclusive/Full_IRT_compre.png");
   c->SaveAs("results/cross_sections/inclusive/Full_IRT_compre.pdf");
   c->SaveAs("results/cross_sections/inclusive/Full_IRT_compre.svg");
   

   return 0;
}
