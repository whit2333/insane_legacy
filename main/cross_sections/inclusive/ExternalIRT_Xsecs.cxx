Int_t ExternalIRT_Xsecs(Double_t theta_target = 80*degree){

   Double_t beamEnergy = 4.7; //GeV
   Double_t Eprime     = 0.1; 
   Double_t theta      = 40.0*degree;
   Double_t phi        = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  

   Int_t    npx      = 50;
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;
   Double_t Emax     = 2.5;

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->SetFillColor(0); 
   leg->SetFillStyle(0); 
   leg->SetHeader("Inelastic radative tails");

   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);

   //-----------------------------------
   InSANEFullInelasticRadiativeTail * fDiffXSec2 = new  InSANEFullInelasticRadiativeTail();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec2->GetPOLRAD()->SetBeamEnergy(beamEnergy);
   fDiffXSec2->GetPOLRAD()->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec2->GetPOLRAD()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec2->GetPOLRAD()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec2->GetInternalXSec()->SetBeamEnergy(beamEnergy);
   fDiffXSec2->GetInternalXSec()->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec2->GetInternalXSec()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec2->GetInternalXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetNpx(npx);
   sigmaE2->SetLineColor(kRed);
   leg->AddEntry(sigmaE2,"Equivalent radiator + ","l");
   //-----------------------------------
   InSANEFullInelasticRadiativeTail * fDiffXSec3 = new  InSANEFullInelasticRadiativeTail();
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   fDiffXSec3->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec3->GetPOLRADIRT()->SetBeamEnergy(beamEnergy);
   fDiffXSec3->GetPOLRADIRT()->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec3->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec3->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec3->GetInternalXSec()->SetBeamEnergy(beamEnergy);
   fDiffXSec3->GetInternalXSec()->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec3->GetInternalXSec()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec3->GetInternalXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE3->SetParameter(0,theta);
   sigmaE3->SetParameter(1,phi);
   sigmaE3->SetNpx(npx);
   sigmaE3->SetLineColor(kRed);
   leg->AddEntry(sigmaE3,"Equivalent radiator - ","l");

   //-----------------------------------
   InSANEInelasticRadiativeTail * fDiffXSec4 = new  InSANEInelasticRadiativeTail();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec4->GetPOLRADIRT()->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetRegion4(true);
   fDiffXSec4->GetPOLRADIRT()->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec4->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec4->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   TF1 * sigmaE4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE4->SetParameter(0,theta);
   sigmaE4->SetParameter(1,phi);
   sigmaE4->SetNpx(npx);
   sigmaE4->SetLineColor(kBlue);
   leg->AddEntry(sigmaE4,"Equiv. rad (plus region IV) + ","l");
   //-----------------------------------
   InSANEInelasticRadiativeTail * fDiffXSec5 = new  InSANEInelasticRadiativeTail();
   fDiffXSec5->SetBeamEnergy(beamEnergy);
   fDiffXSec5->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec5->GetPOLRADIRT()->SetBeamEnergy(beamEnergy);
   fDiffXSec5->SetRegion4(true);
   fDiffXSec5->GetPOLRADIRT()->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec5->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec5->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec5->InitializePhaseSpaceVariables();
   fDiffXSec5->InitializeFinalStateParticles();
   TF1 * sigmaE5 = new TF1("sigma5", fDiffXSec5, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE5->SetParameter(0,theta);
   sigmaE5->SetParameter(1,phi);
   sigmaE5->SetNpx(npx);
   sigmaE5->SetLineColor(kBlue);
   leg->AddEntry(sigmaE5,"Equiv. rad (plus region IV) - ","l");


   //-----------------------
   TCanvas * c = new TCanvas("xsec_test_POLRAD_IRT","xsec_test_POLRAD_IRT");
   //gPad->SetLogy(true);
   TString title =  fDiffXSec2->GetPlotTitle();
   TString yAxisTitle = fDiffXSec2->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;
   TGraph * gra1 = 0;
   TGraph * gra2 = 0;
   TGraph * grb1 = 0;
   TGraph * grb2 = 0;
   TGraph * grc1 = 0;
   TGraph * grc2 = 0;

   //gr = new TGraph(sigmaE->DrawCopy("same"));
   //gra1 = gr;
   //mg->Add(gr,"l");
   //gr = new TGraph(sigmaE1->DrawCopy("same"));
   //gra2 = gr;
   //mg->Add(gr,"l");

   gr = new TGraph(sigmaE2->DrawCopy("same"));
   grb1 = gr;
   mg->Add(gr,"l");
   gr = new TGraph(sigmaE3->DrawCopy("same"));
   grb2 = gr;
   mg->Add(gr,"l");

   //std::cout << " with region IV " << std::endl;
   //gr = new TGraph(sigmaE4->DrawCopy("same"));
   //grc1 = gr;
   //mg->Add(gr,"l");
   //std::cout << " with region IV " << std::endl;
   //gr = new TGraph(sigmaE5->DrawCopy("same"));
   //grc2 = gr;
   //mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   leg->Draw();
   latex->DrawLatex(0.2,0.38,Form("#theta_{target}=%.0f",theta_target/degree));
   latex->DrawLatex(0.2,0.3,Form("#theta_{e}=%.0f",theta/degree));
   latex->DrawLatex(0.2,0.22,Form("E=%.2fGeV",beamEnergy));


   gPad->Modified();

   c->SaveAs("results/cross_sections/inclusive/ExternalIRT_Xsecs.png");
   c->SaveAs("results/cross_sections/inclusive/ExternalIRT_Xsecs.pdf");
   c->SaveAs("results/cross_sections/inclusive/ExternalIRT_Xsecs.tex");


   TCanvas * c2 = new TCanvas("IRT","IRT");
   TMultiGraph * mg2 = new TMultiGraph();
   TGraph * gra = new TGraph(grb1->GetN());
   TGraph * grb = new TGraph(grb1->GetN());
   TGraph * grc = new TGraph(grb1->GetN());
   for(int i = 0; i<grb1->GetN(); i++){
      Double_t x1,x2,y1,y2,A;

      //gra1->GetPoint(i,x1,y1);
      //gra2->GetPoint(i,x2,y2);
      //A = (y1-y2)/(y1+y2);
      //if(y1==y2 || TMath::Abs(A)>1.0) A = 0.0;
      //gra->SetPoint(i,x1,A);

      grb1->GetPoint(i,x1,y1);
      grb2->GetPoint(i,x2,y2);
      A = (y1-y2)/(y1+y2);
      if(y1==y2 || TMath::Abs(A)>1.0) A = 0.0;
      grb->SetPoint(i,x1,A);
      
      //grc1->GetPoint(i,x1,y1);
      //grc2->GetPoint(i,x2,y2);
      //A = (y1-y2)/(y1+y2);
      //if(y1==y2 || TMath::Abs(A)>1.0) A = 0.0;
      //grc->SetPoint(i,x1,A);
   }
   //gra->SetLineColor(kBlack);
   grb->SetLineColor(kRed);
   //grc->SetLineColor(kBlue);
   mg2->Add(gra,"l");
   mg2->Add(grc,"l");
   mg2->Add(grb,"l");

   mg2->Draw("a");

   latex->DrawLatex(0.2,0.38,Form("#theta_{target}=%.0f",theta_target/degree));
   latex->DrawLatex(0.2,0.3,Form("#theta_{e}=%.0f",theta/degree));
   latex->DrawLatex(0.2,0.22,Form("E=%.2fGeV",beamEnergy));

   c2->SaveAs("results/cross_sections/inclusive/ExternalIRT_Xsecs_asym.png");
   c2->SaveAs("results/cross_sections/inclusive/ExternalIRT_Xsecs_asym.pdf");
   c2->SaveAs("results/cross_sections/inclusive/ExternalIRT_Xsecs_asym.tex");
   
   return 0;
}
