Int_t xsec_test_MAIDInclusive(){

   Double_t beamEnergy = 3.2; //GeV
   Double_t Eprime     = 1.0; 
   Double_t theta      = 15.0*degree;
   Double_t Q2         = 4.0*beamEnergy*Eprime*TMath::Power(TMath::Sin(theta/2.0),2.0);
   std::cout << "Q2 = " << Q2 << std::endl;
   Double_t phi        = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 1.6;
   Double_t Emax     = 3.2;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;
   Int_t    Npx      = 300;

   TLegend * leg = new TLegend(0.1,0.7,0.3,0.9);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("MAID cross sections");

   // MAID unpolarized
   TString pion               = "pi0";
   TString nucleon            = "p";
   TString target             = "p";
   MAIDInclusiveDiffXSec *fDiffXSec = new MAIDInclusiveDiffXSec(pion,nucleon);
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetA(A);
   fDiffXSec->SetZ(Z);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetNpx(Npx);
   sigmaE->SetLineColor(kRed);
   leg->AddEntry(sigmaE,"unpolarized","l");

   // MAID polarized
   pion               = "pi0";
   nucleon            = "p";
   target             = "p";
   TVector3    pt(0.0,0.0,1.0);
   MAIDInclusiveDiffXSec *fDiffXSec1 = new MAIDInclusiveDiffXSec(pion,nucleon);
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetA(A);
   fDiffXSec1->SetZ(Z);
   fDiffXSec1->SetTargetType(TargetType);
   fDiffXSec1->SetHelicity(1.0);
   fDiffXSec1->SetTargetPolarization(pt);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();

   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE1->SetParameter(0,theta);
   sigmaE1->SetParameter(1,phi);
   sigmaE1->SetNpx(Npx);
   sigmaE1->SetLineColor(kBlue);
   leg->AddEntry(sigmaE1,"+ helicity","l");

   // MAID polarized
   pion               = "pi0";
   nucleon            = "p";
   target             = "p";
   MAIDInclusiveDiffXSec *fDiffXSec2 = new MAIDInclusiveDiffXSec(pion,nucleon);
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetA(A);
   fDiffXSec2->SetZ(Z);
   fDiffXSec2->SetTargetType(TargetType);
   fDiffXSec2->SetHelicity(-1.0);
   fDiffXSec2->SetTargetPolarization(pt);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();

   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetNpx(Npx);
   sigmaE2->SetLineColor(kGreen-8);
   leg->AddEntry(sigmaE2,"- helicity","l");

   //-------------------------------------------------------
   TCanvas * c        = new TCanvas("MAIDInclusive","MAID Inclusive");
   //gPad->SetLogy(true);
   TString title      = fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   sigmaE->Draw();
   sigmaE->SetTitle(title);
   sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   sigmaE->GetXaxis()->CenterTitle(true);
   sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   sigmaE->GetYaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetRangeUser(0.0,1.1*sigmaE->GetMaximum());
   sigmaE->SetMinimum(0.0);
   //sigmaE->Draw();
   c->Update();
   sigmaE1->DrawCopy("same");
   sigmaE2->DrawCopy("same");
   leg->Draw();

   c->SaveAs("results/cross_sections/inclusive/xsec_test_MAIDInxlusive.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_MAIDInxlusive.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_MAIDInxlusive.tex");

   return 0;
}
