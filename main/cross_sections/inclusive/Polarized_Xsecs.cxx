Int_t Polarized_Xsecs(Double_t theta_target = 80*degree){

   Double_t beamEnergy = 5.9; //GeV
   Double_t Eprime     = 1.0; 
   Double_t theta      = 40.*degree;
   Double_t phi        = 0.0*degree;  
   InSANENucleus::NucleusType Target = InSANENucleus::kProton; 
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   
   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 100;
   Double_t Emin     = 0.5;
   Double_t Emax     = 3.9;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;


   TLegend * leg = new TLegend(0.1,0.1,0.3,0.2);
   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);

   // born polarized + 
   InSANEPOLRADBornDiffXSec * fDiffXSec1 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec1->GetPOLRAD()->SetTargetPolarization(-1.0);
   fDiffXSec1->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigma1 = new TF1("sigma", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(npx);
   sigma1->SetLineColor(kRed);

   // born polarized - 
   InSANEPOLRADBornDiffXSec * fDiffXSec2 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec2->GetPOLRAD()->SetTargetPolarization(-1.0);
   fDiffXSec2->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigma2 = new TF1("sigma", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(kBlack);

   // born polarized + 
   PolarizedDISXSecAntiParallelHelicity * fDiffXSec3 = new  PolarizedDISXSecAntiParallelHelicity();
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   fDiffXSec3->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec3->SetTargetPolarizationAngle(theta_target);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   fDiffXSec3->Refresh();
   TF1 * sigma3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma3->SetParameter(0,theta);
   sigma3->SetParameter(1,phi);
   sigma3->SetNpx(npx);
   sigma3->SetLineStyle(2);
   sigma3->SetLineColor(kRed);

   // born polarized - 
   PolarizedDISXSecParallelHelicity * fDiffXSec4 = new  PolarizedDISXSecParallelHelicity();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec4->SetTargetPolarizationAngle(theta_target);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   fDiffXSec4->Refresh();
   TF1 * sigma4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma4->SetParameter(0,theta);
   sigma4->SetParameter(1,phi);
   sigma4->SetNpx(npx);
   sigma4->SetLineStyle(2);
   sigma4->SetLineColor(kBlack);
   // --------------------------------
   
   TCanvas * c = new TCanvas("Polarized_Xsecs","Polarized DIS Xsecs");
   gPad->SetLogy(true);
   TString title =  fDiffXSec1->GetPlotTitle();
   TString yAxisTitle = fDiffXSec1->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();

   gr = new TGraph(sigma1->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"POLRAD +","l");
   gr = new TGraph(sigma2->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"POLRAD -","l");
   gr = new TGraph(sigma3->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"pol DIS +","l");
   gr = new TGraph(sigma4->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"pol DIS -","l");
   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   leg->Draw();
   latex->DrawLatex(0.35,0.35,Form("#theta_{target}=%.0f",theta_target/degree));

   gPad->Modified();
   c->SaveAs("results/cross_sections/inclusive/Polarized_Xsecs.png");
   c->SaveAs("results/cross_sections/inclusive/Polarized_Xsecs.pdf");
   c->SaveAs("results/cross_sections/inclusive/Polarized_Xsecs.tex");
   return 0;
}
