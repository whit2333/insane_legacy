Int_t xsec_test_Radiator2(Double_t theta_target = 180*degree){

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();

   Double_t fBeamEnergy = 6.0;//20.0;//5.9;
   Double_t Emin        = 0.5;//2.0;//0.5;
   Double_t Emax        = 2.5;//16.0;//2.5;
   Double_t theta       = 45.0*degree;//40.0*degree;
   Double_t phi         = 0.0;
   Int_t    npar        = 2;
   Int_t    npx         = 10;
   Int_t    nMC         = 1e3;
   Double_t RadLen[2]; 
   //InSANENucleus Target(6,12);
   InSANENucleus Target = InSANENucleus::Proton();

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   TLegend * leg = new TLegend(0.7,0.6,0.85,0.8);

   //-----------------------------------
   // CTEQ6 
   CTEQ6eInclusiveDiffXSec * fDiffXSec2 = new  CTEQ6eInclusiveDiffXSec();
   fDiffXSec2->SetBeamEnergy(fBeamEnergy);
   fDiffXSec2->SetTargetNucleus(Target);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigma2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(kBlue);
   sigma2->SetLineStyle(2);
   //leg->AddEntry(sigma2,"CTEQ6","l");

   //-----------------------------------
   // Radiated CTEQ6 
   InSANERadiator<CTEQ6eInclusiveDiffXSec> * fDiffXSec1 = new  InSANERadiator<CTEQ6eInclusiveDiffXSec>();
   fDiffXSec1->SetBeamEnergy(fBeamEnergy);
   fDiffXSec1->SetTargetNucleus(Target);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigma1 = new TF1("sigma", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(npx);
   sigma1->SetLineColor(kBlue);
   //leg->AddEntry(sigma1,"Radiated-CTEQ6","l");

   //-----------------------------------
   // default : set to statistical 
   fman->CreateSFs(6);
   fman->CreatePolSFs(6);
   //InSANEInelasticRadiativeTail2 * fDiffXSec3 = new  InSANEInelasticRadiativeTail2();
   InSANECompositeDiffXSec * fDiffXSec3 = new  InSANECompositeDiffXSec();
   fDiffXSec3->SetBeamEnergy(fBeamEnergy);
   fDiffXSec3->SetTargetNucleus(Target);
   //fDiffXSec3->SetRegion4(true);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   fDiffXSec3->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(Emax);
   fDiffXSec3->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(theta-degree);
   TF1 * sigma3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma3->SetParameter(0,theta);
   sigma3->SetParameter(1,phi);
   sigma3->SetNpx(npx);
   sigma3->SetLineColor(1);
   sigma3->SetLineStyle(2);
   leg->AddEntry(sigma3,"default","l");

   //-----------------------------------
   // Radiated default
   InSANERadiator<InSANECompositeDiffXSec> * fDiffXSec4 = new InSANERadiator<InSANECompositeDiffXSec>();
   fDiffXSec4->SetBeamEnergy(fBeamEnergy);
   fDiffXSec4->SetTargetNucleus(Target);
   //fDiffXSec4->SetRegion4(true);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->GetRADCOR()->SetDelta(0.005);
   fDiffXSec4->InitializeFinalStateParticles();
   fDiffXSec4->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(Emax);
   fDiffXSec4->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(theta-degree);
   TF1 * sigma4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma4->SetParameter(0,theta);
   sigma4->SetParameter(1,phi);
   sigma4->SetNpx(npx);
   sigma4->SetLineColor(1);
   leg->AddEntry(sigma4,"Radiated default","l");

   //-----------------------------------
   // Radiated default + Region IV
   InSANERadiator<InSANECompositeDiffXSec> * fDiffXSec7 = new InSANERadiator<InSANECompositeDiffXSec>();
   fDiffXSec7->SetBeamEnergy(fBeamEnergy);
   fDiffXSec7->SetTargetNucleus(Target);
   fDiffXSec7->SetRegion4(true);
   fDiffXSec7->GetRADCOR()->SetDelta(0.01);
   fDiffXSec7->InitializePhaseSpaceVariables();
   fDiffXSec7->InitializeFinalStateParticles();
   fDiffXSec7->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(Emax);
   fDiffXSec7->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(theta-degree);
   TF1 * sigma7 = new TF1("sigma7", fDiffXSec7, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma7->SetParameter(0,theta);
   sigma7->SetParameter(1,phi);
   sigma7->SetNpx(npx);
   sigma7->SetLineColor(4);
   leg->AddEntry(sigma7,"Radiated+IV default","l");

   //-----------------------------------
   // Radiated default Full External 
   InSANERadiator<InSANECompositeDiffXSec> * fDiffXSec8 = new InSANERadiator<InSANECompositeDiffXSec>();
   fDiffXSec8->SetBeamEnergy(fBeamEnergy);
   fDiffXSec8->SetTargetNucleus(Target);
   fDiffXSec8->SetFullExternal(true);
   fDiffXSec8->GetRADCOR()->SetDelta(0.005);
   fDiffXSec8->InitializePhaseSpaceVariables();
   fDiffXSec8->InitializeFinalStateParticles();
   fDiffXSec8->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(Emax);
   fDiffXSec8->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(theta-degree);
   TF1 * sigma8 = new TF1("sigma8", fDiffXSec8, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma8->SetParameter(0,theta);
   sigma8->SetParameter(1,phi);
   sigma8->SetNpx(npx);
   sigma8->SetLineColor(4);
   sigma8->SetLineStyle(2);
   leg->AddEntry(sigma8,"Radiated Full Ext. default","l");

   //-----------------------------------
   // F1F209 
   F1F209eInclusiveDiffXSec * fDiffXSec5 = new  F1F209eInclusiveDiffXSec();
   fDiffXSec5->SetBeamEnergy(fBeamEnergy);
   fDiffXSec5->SetTargetNucleus(Target);
   fDiffXSec5->InitializePhaseSpaceVariables();
   fDiffXSec5->InitializeFinalStateParticles();
   fDiffXSec5->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(Emax);
   fDiffXSec5->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(theta-degree);
   TF1 * sigma5 = new TF1("sigma5", fDiffXSec5, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma5->SetParameter(0,theta);
   sigma5->SetParameter(1,phi);
   sigma5->SetNpx(npx);
   sigma5->SetLineColor(kRed);
   sigma5->SetLineStyle(2);
   leg->AddEntry(sigma5,"F1F209","l");

   //-----------------------------------
   // Radiated F1F209
   InSANERadiator<F1F209eInclusiveDiffXSec> * fDiffXSec6 = new  InSANERadiator<F1F209eInclusiveDiffXSec>();
   fDiffXSec6->SetBeamEnergy(fBeamEnergy);
   fDiffXSec6->SetTargetNucleus(Target);
   //fDiffXSec6->SetRegion4(true);
   fDiffXSec6->InitializePhaseSpaceVariables();
   fDiffXSec6->InitializeFinalStateParticles();
   fDiffXSec6->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(Emax);
   fDiffXSec6->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(theta-degree);
   TF1 * sigma6 = new TF1("sigma6", fDiffXSec6, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma6->SetParameter(0,theta);
   sigma6->SetParameter(1,phi);
   sigma6->SetNpx(npx);
   sigma6->SetLineColor(kRed);
   leg->AddEntry(sigma6,"Radiated-F1F209","l");


   //-----------------------------------
   TCanvas * c = new TCanvas("rc_test1","rc_test1");
   //gPad->SetLogy(true);
   TString title =  "Internal Inelastic Radiative Tail";//fDiffXSec->GetPlotTitle();
   TString yAxisTitle = "#frac{d#sigma}{dE'd#Omega} (nb/GeV/sr)";
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   std::cout << "TEST" << std::endl;
   //gr = new TGraph(sigma1->DrawCopy("same"));
   //mg->Add(gr,"l");
   //gr = new TGraph(sigma2->DrawCopy("same"));
   //mg->Add(gr,"l");
   gr = new TGraph(sigma3->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma4->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma5->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma6->DrawCopy("same"));
   mg->Add(gr,"l");
   std::cout << " region IV " << std::endl;
   gr = new TGraph(sigma7->DrawCopy("same"));
   mg->Add(gr,"l");
   std::cout << " full ext. " << std::endl;
   gr = new TGraph(sigma8->DrawCopy("same"));
   mg->Add(gr,"l");

   //sigma1->Draw();
   //sigma2->Draw("same");
   //sigma3->Draw("same");
   //sigma4->Draw("same");

   mg->Draw("a");
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->Draw();
   //TPaveText * kine = new TPaveText(0.15,0.2,0.3,0.275,"NDC");
   //kine->AddText(Form("E = %1.1f GeV, #theta = %1.1f deg",fBeamEnergy,theta/degree));
   //kine->Draw("");

   TLatex Tl;
   Tl.SetNDC();
   Tl.SetTextSize(0.03);
   Tl.SetTextAlign(21);
   Tl.DrawLatex(0.5,0.8, Form("E = %1.1f GeV, #theta = %1.1f deg",fBeamEnergy,theta/degree));
   //latex->DrawLatex(0.1,0.1,Form("E = %1.1f, #theta = %1.1f",fBeamEnergy,theta/degree));

   c->SaveAs("results/cross_sections/inclusive/xsec_test_Radiator22.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_Radiator22.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_Radiator22.tex");
   return(0);
}
