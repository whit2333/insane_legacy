Int_t sigma_p(){

   NucDBManager * dbman = NucDBManager::GetManager();
   dbman->ListMeasurementsByExperiment("sigma");
   TList * meas_list = dbman->GetMeasurements("sigma");
   if(!meas_list) return -1;
   //meas_list->Print("");

   NucDBExperiment * experiment = dbman->GetExperiment("SLAC-E140");
   if(!experiment)  return -2;
   meas_list->Add(experiment->GetMeasurement("sigma"));

   TMultiGraph * mg = new TMultiGraph();

   NucDBBinnedVariable Ebeam("E","E");
   Ebeam.SetBinValueSize(5.5,1.5);

   NucDBBinnedVariable theta("theta","theta");
   theta.SetBinValueSize(11.2,1.0);

   NucDBBinnedVariable Qsquared("Qsquared","Q^2");
   Qsquared.SetBinValueSize(10.0,20.0);

   TLegend * leg = new TLegend(0.6,0.4,0.8,0.2);

   for (int i = 0; i < meas_list->GetEntries(); i++) {
      NucDBMeasurement *aMeas = (NucDBMeasurement*)meas_list->At(i);
      aMeas->ApplyFilterWithBin(&Qsquared);
      aMeas->ApplyFilterWithBin(&Ebeam);
      aMeas->ApplyFilterWithBin(&theta);
      if(aMeas->GetNDataPoints()<1) continue;
      aMeas->Print("v");
      std::cout << aMeas->GetExperimentName() << std::endl;
      TGraphErrors * gr = aMeas->BuildGraph("W");
      gr->SetMarkerStyle(1);
      mg->Add(gr,"epc");
   }

   InSANEeInclusiveElasticDiffXSec * fDiffXSec = new  InSANEeInclusiveElasticDiffXSec();
   fDiffXSec->SetBeamEnergy(5.5);
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->UsePhaseSpace(false);

   Double_t x[] = {0.938}; // W
   Double_t p[] = {11.2*degree,0.0,0.0}; // W
   Double_t sig_elastic = fDiffXSec->Vs_W(x,p);
   Double_t Wmin = 0.5;
   Double_t Wmax = 3.5;
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEeInclusiveElasticDiffXSec::Vs_W, 
         Wmin, Wmax, 2,"InSANEeInclusiveElasticDiffXSec","Vs_W");
   sigmaE->SetParameter(0,11.2*degree);
   sigmaE->SetParameter(1,0.0);
   sigmaE->SetLineColor(kRed);

   TF1 * f1 = new TF1("elastic","gaus(0)",0.9,1.0);
   f1->SetLineColor(1);
   f1->SetLineWidth(1);
   f1->SetParameter(0,sig_elastic);
   f1->SetParameter(1,0.938);
   f1->SetParameter(2,0.001);

   TCanvas * c  = new TCanvas();
   //gPad->SetLogy(true);
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.8,2.5);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->CenterTitle(true);
   mg->GetXaxis()->SetTitle("W (GeV)");
   mg->GetYaxis()->SetTitle("d#sigma/dEd#Omega");
   f1->Draw("same");
   //sigmaE->Draw("same");

   TLatex latex;
   latex.SetTextFont(gStyle->GetTitleFont("x"));
   latex.SetNDC(true);
   latex.DrawLatex(0.6,0.7,"E=5.5 GeV");
   latex.DrawLatex(0.6,0.8,"#theta=11.2 degrees");

   c->SaveAs("results/cross_sections/inclusive/sigma_p.png");

   return 0;
}
