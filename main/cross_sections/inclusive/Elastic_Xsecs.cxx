Int_t Elastic_Xsecs(Double_t theta_target = 80*degree,Int_t aNumber = 1){

   Double_t beamEnergy = 5.5; //GeV
   Double_t Eprime     = 0.1; 
   Double_t theta      = 40.*degree;
   Double_t phi        = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   
   Double_t Epeak      = beamEnergy/(1.0+beamEnergy*(1.0-TMath::Cos(theta))/(M_p/GeV));
   Double_t Epeak2     = (beamEnergy-1.0)/(1.0+(beamEnergy-1.0)*(1.0-TMath::Cos(theta))/(M_p/GeV));
   Double_t Epeak3     = (beamEnergy-1.75)/(1.0+(beamEnergy-1.75)*(1.0-TMath::Cos(theta))/(M_p/GeV));
   Double_t Epeak4     = (beamEnergy-2.5)/(1.0+(beamEnergy-2.5)*(1.0-TMath::Cos(theta))/(M_p/GeV));
   Int_t    nMC        = 1e3;
   Double_t RadLen[2]; 
   RadLen[0] = 0.025; 
   RadLen[1] = 0.025; 
   InSANENucleus::NucleusType Target = InSANENucleus::kProton; 

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 50;
   Double_t Emin     = 0.5;
   Double_t Emax     = 3.5;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;

   TLegend * leg = new TLegend(0.6,0.17,0.87,0.32);
   leg->SetFillStyle(0);
   leg->SetFillStyle(0);
   //-----------------
   InSANEeInclusiveElasticDiffXSec * fDiffXSec = new  InSANEeInclusiveElasticDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEeInclusiveElasticDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEeInclusiveElasticDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,phi);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetNpx(npx);
   sigmaE->SetLineColor(kRed);
   //leg->AddEntry(sigmaE,"Elastic cross section","l");

   //-----------------
   // Elastic Radiative Tail
   InSANEElasticRadiativeTail * fDiffXSec2 = new  InSANEElasticRadiativeTail();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetPolarizations(0.0,0.0);
   fDiffXSec2->SetTargetNucleus(InSANENucleus::Proton());
   //fDiffXSec2->GetPOLRADERT()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   TF1 * sigma2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(kBlue);

   //-----------------
   InSANEPOLRADElasticDiffXSec * fDiffXSec3 = new  InSANEPOLRADElasticDiffXSec();
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   //fDiffXSec3->GetPOLRAD()->SetPolarizations(-1.0,1.0,0.0);
   fDiffXSec3->GetPOLRAD()->SetPolarizationVectors(P_target,0.0);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec3->InitializeFinalStateParticles();
   TF1 * sigma3 = new TF1("sigma3", fDiffXSec3, &InSANEPOLRADElasticDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEPOLRADElasticDiffXSec","EnergyDependentXSec");
   sigma3->SetParameter(0,phi);
   sigma3->SetParameter(1,phi);
   sigma3->SetNpx(npx);
   sigma3->SetLineColor(kBlue);
   //leg->AddEntry(sigma2,"Elastic cross section","l");

   //-----------------
   InSANEPOLRADElasticDiffXSec * fDiffXSec33 = new  InSANEPOLRADElasticDiffXSec();
   fDiffXSec33->SetBeamEnergy(beamEnergy-1.0);
   //fDiffXSec3->GetPOLRAD()->SetPolarizations(-1.0,1.0,0.0);
   fDiffXSec33->GetPOLRAD()->SetPolarizationVectors(P_target,0.0);
   fDiffXSec33->InitializePhaseSpaceVariables();
   fDiffXSec33->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec33->InitializeFinalStateParticles();
   TF1 * sigma33 = new TF1("sigma33", fDiffXSec33, &InSANEPOLRADElasticDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEPOLRADElasticDiffXSec","EnergyDependentXSec");
   sigma33->SetParameter(0,phi);
   sigma33->SetParameter(1,phi);
   sigma33->SetNpx(npx);
   sigma33->SetLineStyle(2);
   sigma33->SetLineColor(kBlue);

   //-----------------
   // Elastic Radiative Tail
   InSANEElasticRadiativeTail * fDiffXSec22 = new  InSANEElasticRadiativeTail();
   fDiffXSec22->SetBeamEnergy(beamEnergy);
   fDiffXSec22->SetPolarizations(0.0,0.0);
   //fDiffXSec22->GetPOLRADERT()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec22->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec22->InitializePhaseSpaceVariables();
   fDiffXSec22->InitializeFinalStateParticles();
   TF1 * sigma22 = new TF1("sigma22", fDiffXSec22, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma22->SetParameter(0,theta);
   sigma22->SetParameter(1,phi);
   sigma22->SetNpx(npx);
   sigma22->SetLineColor(kBlack);
   //leg->AddEntry(sigma22,"Elastic radiative tail","l");

   

   // ----------------------------------------
   //
   TCanvas * c = new TCanvas("Elastic_Xsecs","Elastic Xsecs");
   gPad->SetLogy(true);
   TString title =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   leg->AddEntry(sigma22,"Elastic radiative tail","l");
   leg->AddEntry(sigma2,"Elastic cross section","l");
   leg->AddEntry(sigma33,"Elastic cross section at lower E","l");

   //gr = new TGraph(sigmaE->DrawCopy("same"));
   //mg->Add(gr,"l");
   //gr = new TGraph(sigma2->DrawCopy("same"));
   //mg->Add(gr,"l");

   sigma22->SetRange(Emin, 2.26);
   gr = new TGraph(sigma22->DrawCopy("same"));
   mg->Add(gr,"l");

   gr = new TGraph(sigma3->DrawCopy("same"));
   mg->Add(gr,"l");

   fDiffXSec33->SetBeamEnergy(beamEnergy-1.0);
   sigma33->SetRange(Emin, Emax);
   gr = new TGraph(sigma33->DrawCopy("same"));
   mg->Add(gr,"l");

   fDiffXSec33->SetBeamEnergy(beamEnergy-1.75);
   sigma33->SetRange(Emin, 2.5);
   gr = new TGraph(sigma33->DrawCopy("same"));
   mg->Add(gr,"l");

   fDiffXSec33->SetBeamEnergy(beamEnergy-2.5);
   sigma33->SetRange(Emin, 2.0);
   gr = new TGraph(sigma33->DrawCopy("same"));
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetYaxis()->SetRangeUser(0.001,10.0);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   //leg->Draw();
   
   gPad->Modified();
   TArrow * arrow  = new TArrow(Epeak, 2.0,Epeak, 0.01  ,0.02,"|>");
   TArrow * arrow2 = new TArrow(Epeak2,2.0,Epeak2,0.039,0.02,"|>");
   TArrow * arrow3 = new TArrow(Epeak3,2.0,Epeak3,0.15,  0.02,"|>");
   TArrow * arrow4 = new TArrow(Epeak4,2.0,Epeak4,0.58 ,0.02,"|>");
   arrow->Draw();
   arrow2->Draw();
   arrow3->Draw();
   arrow4->Draw();

   TLatex * latex = new TLatex();
   latex->SetTextSize(0.04);
   latex->SetTextAlign(12);  //centered
   latex->SetTextFont(132);
   latex->SetTextAngle(0);
   latex->SetNDC();
   latex->DrawLatex(0.5,0.8,"#theta = 40#circ");

   latex->SetNDC(false);
   latex->SetTextAngle(35);
   latex->SetTextSize(0.035);
   latex->DrawLatex(1.42,0.0016,"E = 5.5 GeV");
   latex->SetTextAngle(38);
   latex->DrawLatex(0.9,0.0018,"E = 4.5 GeV");
   latex->SetTextAngle(44);
   latex->DrawLatex(0.74,0.0035,"E = 3.75 GeV");
   latex->SetTextAngle(48);
   latex->DrawLatex(0.58,0.008,"E = 3.0 GeV");

   c->SaveAs(Form("results/cross_sections/inclusive/Elastic_Xsecs_%d.png",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/Elastic_Xsecs_%d.pdf",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/Elastic_Xsecs_%d.tex",aNumber));
   return 0;
}
