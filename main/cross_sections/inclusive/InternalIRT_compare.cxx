Int_t InternalIRT_compare(Double_t theta_target = 180*degree){

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();

   Int_t    aNumber     = 3;
   Double_t beamEnergy  = 4.7;
   Double_t Emin        = 0.5;
   Double_t Emax        = 2.5;
   Double_t theta       = 30.0*degree;
   Double_t phi         = 0.0;
   Int_t    npar        = 2;
   Int_t    npx         = 50;
   Int_t    nMC         = 1e3;
   Double_t RadLen[2]; 
   InSANENucleus Target = InSANENucleus::Proton(); 

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   TLegend * leg = new TLegend(0.7,0.6,0.85,0.8);

   //-----------------------------------
   // born polarized + 
   InSANEPOLRADBornDiffXSec * xsec0 = new  InSANEPOLRADBornDiffXSec();
   xsec0->SetBeamEnergy(beamEnergy);
   xsec0->SetTargetNucleus(Target);
   xsec0->SetHelicity(1);
   xsec0->GetPOLRAD()->SetTargetPolarization(1.0);
   xsec0->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();
   xsec0->Refresh();
   TF1 * sigma0 = new TF1("sigma", xsec0, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0->SetParameter(0,theta);
   sigma0->SetParameter(1,phi);
   sigma0->SetNpx(npx);
   sigma0->SetLineColor(1);
   sigma0->SetLineStyle(2);
   leg->AddEntry(sigma0,"#sigma_{born}^{+}","l");
   // born polarized - 
   InSANEPOLRADBornDiffXSec * xsec1 = new  InSANEPOLRADBornDiffXSec();
   xsec1->SetBeamEnergy(beamEnergy);
   xsec1->SetTargetNucleus(Target);
   xsec1->SetHelicity(-1);
   xsec1->GetPOLRAD()->SetTargetPolarization(-1.0);
   xsec1->GetPOLRAD()->SetPolarizationVectors(P_target,1);
   xsec1->InitializePhaseSpaceVariables();
   xsec1->InitializeFinalStateParticles();
   xsec1->Refresh();
   TF1 * sigma1 = new TF1("sigma1", xsec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(npx);
   sigma1->SetLineColor(1);
   sigma1->SetLineStyle(2);
   leg->AddEntry(sigma1,"#sigma_{born}^{-}","l");

   //-----------------------------------
   // POLRAD IRT
   InSANEPOLRADInelasticTailDiffXSec * xsec2 = new  InSANEPOLRADInelasticTailDiffXSec();
   xsec2->SetBeamEnergy(beamEnergy);
   xsec2->GetPOLRAD()->SetHelicity(1);
   xsec2->SetTargetNucleus(InSANENucleus::Proton());
   xsec2->GetPOLRAD()->SetTargetPolarization(1.0);
   xsec2->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   xsec2->InitializePhaseSpaceVariables();
   xsec2->InitializeFinalStateParticles();
   xsec2->Refresh();
   TF1 * sigma2 = new TF1("sigma2", xsec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(2);
   leg->AddEntry(sigma2,"POLRAD IRT + ","l");
   //-----------------------------------
   InSANEPOLRADInelasticTailDiffXSec * xsec3 = new  InSANEPOLRADInelasticTailDiffXSec();
   xsec3->SetBeamEnergy(beamEnergy);
   xsec3->GetPOLRAD()->SetHelicity(1);
   //xsec3->GetPOLRAD()->SetAnglePeakingApprox(false);
   xsec3->SetTargetNucleus(InSANENucleus::Proton());
   xsec3->GetPOLRAD()->SetTargetPolarization(1.0);
   xsec3->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   xsec3->InitializePhaseSpaceVariables();
   xsec3->InitializeFinalStateParticles();
   xsec3->Refresh();
   TF1 * sigma3 = new TF1("sigma3", xsec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma3->SetParameter(0,theta);
   sigma3->SetParameter(1,phi);
   sigma3->SetNpx(npx);
   sigma3->SetLineColor(2);
   leg->AddEntry(sigma3,"POLRAD IRT -","l");

   //-----------------------------------
   // Equiv. Radiator
   InSANERadiator<InSANEPOLRADBornDiffXSec> * xsec4 = new  InSANERadiator<InSANEPOLRADBornDiffXSec>();
   xsec4->SetInternalOnly(true);
   xsec4->SetBeamEnergy(beamEnergy);
   xsec4->SetTargetNucleus(Target);
   xsec4->SetHelicity(1);
   xsec4->GetPOLRAD()->SetTargetPolarization(1.0);
   xsec4->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   xsec4->InitializePhaseSpaceVariables();
   xsec4->InitializeFinalStateParticles();
   xsec4->Refresh();
   TF1 * sigma4 = new TF1("sigma4", xsec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma4->SetParameter(0,theta);
   sigma4->SetParameter(1,phi);
   sigma4->SetNpx(npx);
   sigma4->SetLineColor(4);
   sigma4->SetLineStyle(2);
   leg->AddEntry(sigma4,"Equiv. Rad","l");
   //  - 
   InSANERadiator<InSANEPOLRADBornDiffXSec> * xsec5 = new  InSANERadiator<InSANEPOLRADBornDiffXSec>();
   xsec5->SetInternalOnly(true);
   xsec5->SetBeamEnergy(beamEnergy);
   xsec5->SetTargetNucleus(Target);
   xsec5->SetHelicity(-1);
   xsec5->GetPOLRAD()->SetTargetPolarization(-1.0);
   xsec5->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   xsec5->InitializePhaseSpaceVariables();
   xsec5->InitializeFinalStateParticles();
   xsec5->Refresh();
   TF1 * sigma5 = new TF1("sigma5", xsec5, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma5->SetParameter(0,theta);
   sigma5->SetParameter(1,phi);
   sigma5->SetNpx(npx);
   sigma5->SetLineColor(4);
   sigma5->SetLineStyle(2);
   leg->AddEntry(sigma5,"Equiv. Rad","l");

   //------------------------
   TCanvas * c = new TCanvas("rc_test1","rc_test1");
   //gPad->SetLogy(true);
   TString title =  "Internal Inelastic Radiative Tail";//fDiffXSec->GetPlotTitle();
   TString yAxisTitle = "#frac{d#sigma}{dE'd#Omega} (nb/GeV/sr)";
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   gr = new TGraph(sigma0->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma1->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma2->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma3->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma4->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma5->DrawCopy("same"));
   mg->Add(gr,"l");

   mg->Draw("a");
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->Draw();
   //TPaveText * kine = new TPaveText(0.15,0.2,0.3,0.275,"NDC");
   //kine->AddText(Form("E = %1.1f GeV, #theta = %1.1f deg",fBeamEnergy,theta/degree));
   //kine->Draw("");

   TLatex Tl;
   Tl.SetNDC();
   Tl.SetTextSize(0.03);
   Tl.SetTextAlign(21);
   Tl.DrawLatex(0.5,0.8, Form("E = %1.1f GeV, #theta = %1.1f deg",beamEnergy,theta/degree));
   //latex->DrawLatex(0.1,0.1,Form("E = %1.1f, #theta = %1.1f",beamEnergy,theta/degree));

   c->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_compare_%d.png",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_compare_%d.pdf",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_compare_%d.tex",aNumber));

   return(0);
}
