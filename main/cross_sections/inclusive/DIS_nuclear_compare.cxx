Int_t DIS_nuclear_compare(){

   Double_t beamEnergy = 3.0;//49; //GeV
   Double_t Eprime     = 1.01; 
   Double_t theta      = 35.0*degree;//12.9.*degree;
   Double_t phi        = 0.0*degree;  
   InSANENucleus targetNucleus(6,12);

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.1;//3.5;
   Double_t Emax     = 2.4;//5.10;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;


   // ---------------------------------------------------
   InSANEStructureFunctions * SFs = InSANEFunctionManager::GetManager()->CreateSFs(11); 
   F1F209StructureFunctions * F1F209SFs = dynamic_cast<F1F209StructureFunctions*>(SFs);
   if( F1F209SFs  ) {
      //F1F209SFs->DoInelasticOnly(true);
   }

   // ------------------
   // 
   InSANEInclusiveBornDISXSec * xsec0 = new InSANEInclusiveBornDISXSec();
   xsec0->SetBeamEnergy(beamEnergy);
   xsec0->UsePhaseSpace(false);
   xsec0->SetTargetNucleus(targetNucleus);
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();
   //xsec0->GetPhaseSpace()->Print();
   xsec0->UsePhaseSpace(false);
   TF1 * sigma0 = new TF1("sigma0", xsec0, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                             Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0->SetParameter(0,theta);
   sigma0->SetParameter(1,phi);
   sigma0->SetLineColor(1);

   // ------------------
   // 
   InSANERadiator<InSANEInclusiveBornDISXSec> * xsec1 = new InSANERadiator<InSANEInclusiveBornDISXSec>();
   //InSANEInclusiveBornDISXSec * xsec1 = new InSANEInclusiveBornDISXSec();
   xsec1->SetBeamEnergy(beamEnergy);
   xsec1->UsePhaseSpace(false);
   xsec1->SetTargetNucleus(targetNucleus);
   xsec1->SetRadiationLength(0.01);
   xsec1->InitializePhaseSpaceVariables();
   xsec1->InitializeFinalStateParticles();
   //xsec1->GetPhaseSpace()->Print();
   xsec1->UsePhaseSpace(false);
   TF1 * sigma1 = new TF1("sigma1", xsec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                             Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetLineColor(2);

   // ------------------
   // 
   InSANEInclusiveBornDISXSec * xsec0_d = new InSANEInclusiveBornDISXSec();
   xsec0_d->SetBeamEnergy(beamEnergy);
   xsec0_d->UsePhaseSpace(false);
   xsec0_d->SetTargetNucleus(InSANENucleus::Deuteron());
   xsec0_d->InitializePhaseSpaceVariables();
   xsec0_d->InitializeFinalStateParticles();
   xsec0_d->UsePhaseSpace(false);
   TF1 * sigma0_d = new TF1("sigma0d", xsec0_d, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                             Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0_d->SetParameter(0,theta);
   sigma0_d->SetParameter(1,phi);
   sigma0_d->SetLineColor(1);

   // ------------------
   // 
   InSANERadiator<InSANEInclusiveBornDISXSec> * xsec1_d = new InSANERadiator<InSANEInclusiveBornDISXSec>();
   xsec1_d->SetBeamEnergy(beamEnergy);
   xsec1_d->UsePhaseSpace(false);
   xsec1_d->SetTargetNucleus(InSANENucleus::Deuteron());
   xsec1_d->SetRadiationLength(0.01);
   xsec1_d->InitializePhaseSpaceVariables();
   xsec1_d->InitializeFinalStateParticles();
   xsec1_d->UsePhaseSpace(false);
   TF1 * sigma1_d = new TF1("sigma1d", xsec1_d, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                             Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1_d->SetParameter(0,theta);
   sigma1_d->SetParameter(1,phi);
   sigma1_d->SetLineColor(4);

   // ------------------------------
   TString title      =  "";//xsec0->GetPlotTitle();
   TString yAxisTitle = "";//xsec0->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   //gPad->SetLogy(true);
   TLegend * leg = new TLegend(0.17,0.17,0.6,0.5);
   leg->SetFillColor(0);
   leg->SetBorderSize(0);

   TLatex  latex;

   c->cd(1);

   gPad->SetLogy(true);
   sigma0->Draw();
   sigma0->SetTitle(title);
   sigma0->GetXaxis()->SetTitle(xAxisTitle);
   sigma0->GetXaxis()->CenterTitle(true);
   sigma0->GetYaxis()->SetTitle(yAxisTitle);
   sigma0->GetYaxis()->CenterTitle(true);
   sigma0->DrawCopy();
   sigma1->DrawCopy("same");
   sigma0_d->DrawCopy("same");
   sigma1_d->DrawCopy("same");
   leg->AddEntry(sigma0,"Born","l");
   leg->AddEntry(sigma1,"^{12}C radiated","l");
   leg->AddEntry(sigma1_d,"^{2}H radiated","l");
   leg->Draw();

   // --------------------

   c->cd(2);
   TH1 * hSig_A  = sigma0->DrawCopy()->GetHistogram();
   TH1 * hSig_d  = sigma0_d->DrawCopy("same")->GetHistogram();
   TH1 * hSig_A2 = sigma1->DrawCopy()->GetHistogram();
   TH1 * hSig_d2 = sigma1_d->DrawCopy("same")->GetHistogram();

   if(!hSig_A) return -33;
   TH1F * hEmc = (TH1F*)hSig_A->Clone("hEmc");
   hEmc->Divide(hSig_d);
   hEmc->Scale(2.0/12.0);

   TH1F * hEmc2 = (TH1F*)hSig_A2->Clone("hEmc");
   hEmc2->Divide(hSig_d2);
   hEmc2->Scale(2.0/12.0);

   hEmc->Draw();
   hEmc->GetYaxis()->SetRangeUser(0.4,1.9);
   hEmc2->Draw("same");
   hEmc->GetXaxis()->SetTitle(xAxisTitle);
   hEmc->GetXaxis()->CenterTitle(true);

   latex.DrawLatex(0.2,1.6,Form("E = %.1f GeV",beamEnergy));
   latex.DrawLatex(0.2,1.4,Form("#theta = %.1f degrees",theta/degree));

   // ---------------------
   c->cd(3);
   Double_t Wmin = 0.7;
   Double_t Wmax = 2.8;
   Double_t Q2 = 1.0;

   TF1 * sigmaW0 = new TF1("sigmaW0", xsec0, &InSANEInclusiveDiffXSec::WDependentXSec, 
                             Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigmaW0->SetParameter(0,Q2);
   sigmaW0->SetParameter(1,phi);
   sigmaW0->SetLineColor(kRed);

   TF1 * sigmaW1 = new TF1("sigmaW1", xsec0_d, &InSANEInclusiveDiffXSec::WDependentXSec, 
                             Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigmaW1->SetParameter(0,Q2);
   sigmaW1->SetParameter(1,phi);
   sigmaW1->SetLineColor(4);

   sigmaW0->Draw();
   sigmaW0->SetTitle(title);
   sigmaW0->GetXaxis()->SetTitle("W");
   sigmaW0->GetXaxis()->CenterTitle(true);
   sigmaW0->GetYaxis()->CenterTitle(true);
   sigmaW0->Draw();
   sigmaW1->Draw("same");

   latex.DrawLatex(2.2,700,Form("Q^{2}=%.1f",Q2));

   c->SaveAs("results/cross_sections/inclusive/DIS_nuclear_compare.png");

}
