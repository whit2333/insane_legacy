Int_t DIS_nuclear_compare2(){

   Double_t beamEnergy = 6.0;//49; //GeV
   Double_t Eprime     = 1.01; 
   Double_t theta      = 35.0*degree;//12.9.*degree;
   Double_t phi        = 0.0*degree;  
   InSANENucleus targetNucleus(6,12);

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.1;
   Double_t Emax     = 3.0;//5.10;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;


   // ---------------------------------------------------
   InSANEStructureFunctions * SFs = InSANEFunctionManager::GetManager()->CreateSFs(11); 
   F1F209StructureFunctions * F1F209SFs = dynamic_cast<F1F209StructureFunctions*>(SFs);
   if( F1F209SFs  ) {
      //std::cout << " Do Inelastic only " << std::endl;
      //F1F209SFs->DoInelasticOnly(true);
   }
   // ---------------------------------------------------

   // ------------------
   // 
   InSANEInclusiveBornDISXSec * xsec0 = new InSANEInclusiveBornDISXSec();
   xsec0->SetBeamEnergy(beamEnergy);
   xsec0->UsePhaseSpace(false);
   xsec0->SetTargetNucleus(targetNucleus);
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();
   //xsec0->GetPhaseSpace()->Print();
   xsec0->UsePhaseSpace(false);
   TF1 * sigma0 = new TF1("sigma0", xsec0, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                             Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0->SetParameter(0,theta);
   sigma0->SetParameter(1,phi);
   sigma0->SetLineColor(1);

   // ------------------
   // 
   InSANERadiator<InSANEInclusiveBornDISXSec> * xsec1 = new InSANERadiator<InSANEInclusiveBornDISXSec>();
   //InSANEInclusiveBornDISXSec * xsec1 = new InSANEInclusiveBornDISXSec();
   xsec1->SetBeamEnergy(beamEnergy);
   xsec1->UsePhaseSpace(false);
   xsec1->SetTargetNucleus(targetNucleus);
   xsec1->SetRadiationLength(0.01);
   xsec1->InitializePhaseSpaceVariables();
   xsec1->InitializeFinalStateParticles();
   //xsec1->GetPhaseSpace()->Print();
   xsec1->UsePhaseSpace(false);
   TF1 * sigma1 = new TF1("sigma1", xsec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                             Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetLineColor(2);

   // ------------------
   // 
   InSANEInclusiveBornDISXSec * xsec0_d = new InSANEInclusiveBornDISXSec();
   xsec0_d->SetBeamEnergy(beamEnergy);
   xsec0_d->UsePhaseSpace(false);
   xsec0_d->SetTargetNucleus(InSANENucleus::Deuteron());
   xsec0_d->InitializePhaseSpaceVariables();
   xsec0_d->InitializeFinalStateParticles();
   xsec0_d->UsePhaseSpace(false);
   TF1 * sigma0_d = new TF1("sigma0d", xsec0_d, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                             Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0_d->SetParameter(0,theta);
   sigma0_d->SetParameter(1,phi);
   sigma0_d->SetLineColor(1);

   // ------------------
   // 
   InSANERadiator<InSANEInclusiveBornDISXSec> * xsec1_d = new InSANERadiator<InSANEInclusiveBornDISXSec>();
   xsec1_d->SetBeamEnergy(beamEnergy);
   xsec1_d->UsePhaseSpace(false);
   xsec1_d->SetTargetNucleus(InSANENucleus::Deuteron());
   xsec1_d->SetRadiationLength(0.01);
   xsec1_d->InitializePhaseSpaceVariables();
   xsec1_d->InitializeFinalStateParticles();
   xsec1_d->UsePhaseSpace(false);
   TF1 * sigma1_d = new TF1("sigma1d", xsec1_d, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                             Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1_d->SetParameter(0,theta);
   sigma1_d->SetParameter(1,phi);
   sigma1_d->SetLineColor(4);

   // ------------------------------
   TString title      =  "";//xsec0->GetPlotTitle();
   TString yAxisTitle = xsec0->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TCanvas * c = new TCanvas();
   //c->Divide(2,2);
   //gPad->SetLogy(true);

   //c->cd(1);
   TLegend * leg = new TLegend(0.16,0.16,0.45,0.45);
   leg->SetFillColor(0);
   leg->SetBorderSize(0);
   TLatex  latex;
   latex.SetTextFont(132);

   TMultiGraph * mg = new TMultiGraph();
   TGraph      * gr = 0;
   gPad->SetLogy(true);

   gr = new TGraph( sigma0_d->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph( sigma1_d->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph( sigma0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph( sigma1->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);
   mg->Draw("a");

   leg->AddEntry(sigma0,"Born","l");
   leg->AddEntry(sigma1,"^{12}C radiated","l");
   leg->AddEntry(sigma1_d,"^{2}H radiated","l");
   leg->Draw();

   latex.DrawLatex(1.5,100,Form("E = %.1f GeV",beamEnergy));
   latex.DrawLatex(1.5,50,Form("#theta = %.1f degrees",theta/degree));

   c->SaveAs("results/cross_sections/inclusive/DIS_nuclear_compare2.png");
   return 0;

}

