Int_t xsec_test_MAIDVirtualComptonAsymmetries(){

   Double_t beamEnergy = 5.9; //GeV
   Double_t Eprime     = 1.0; 
   Double_t theta      = 30.*degree;
   Double_t Q2         = 1.86;//4.0*beamEnergy*Eprime*TMath::Power(TMath::Sin(theta/2.0),2.0);
   Double_t phi        = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t xmin     = 1.0;
   Double_t xmax     = 1.9;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;

   // MAID
   TString pion               = "pi0";
   TString nucleon            = "p";
   TString target             = "p";
   MAIDVirtualComptonAsymmetries *fDiffXSec = new MAIDVirtualComptonAsymmetries(pion,nucleon);
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetA(A);
   fDiffXSec->SetZ(Z);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();

   TString title      =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "x"; 

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::WDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigmaE->SetParameter(0,Q2);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetLineColor(kRed);

   sigmaE->Draw();
   //sigmaE->SetTitle(title);
   //sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   //sigmaE->GetXaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   //sigmaE->GetYaxis()->CenterTitle(true);
   //sigmaE->Draw();

   return 0;
}
