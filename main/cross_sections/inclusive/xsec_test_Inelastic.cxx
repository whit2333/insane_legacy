/*! 
 */ 
Int_t xsec_test_Inelastic(Int_t aNumber = 38 ){

   Double_t beamEnergy = 11.0; //GeV
   Double_t Eprime     = 1.5; 
   Double_t theta      = 30.*degree;
   Double_t phi        = 0.0*degree;  
   Double_t Z          = 1;   
   Double_t Q2         = 4.0*beamEnergy*Eprime*TMath::Power(TMath::Sin(theta/2.0),2.0);

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 50;
   Double_t Emin     = 0.25;
   Double_t Emax     = 2.5;//beamEnergy;
   Double_t minTheta = 4.5*degree;
   Double_t maxTheta = 40.0*degree;
   Double_t Wmin     = 0.5;
   Double_t Wmax     = beamEnergy;
   
   // -----------------------
   // Target
   InSANENucleus targ(1,2); // Z,A
   TVector3 P_target(0,0,0);

   TLegend * leg = new TLegend(0.01,0.01,0.99,0.99);
   leg->SetHeader("E = 2.7 GeV");

   // -----------------------------------
   F1F209eInclusiveDiffXSec * xsec_born = new F1F209eInclusiveDiffXSec();
   xsec_born->SetTargetNucleus(targ);
   xsec_born->SetBeamEnergy(beamEnergy);
   xsec_born->InitializePhaseSpaceVariables();
   xsec_born->InitializeFinalStateParticles();
   xsec_born->UsePhaseSpace(false);

   TF1 * sigmaE_born = new TF1("sigmaEborn", xsec_born, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_born->SetLineColor(2);
   sigmaE_born->SetParameter(0,theta);
   sigmaE_born->SetParameter(1,phi);
   sigmaE_born->SetNpx(npx);

   TF1 * sigma_born = new TF1("sigmaborn", xsec_born, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_born->SetLineColor(2);
   sigma_born->SetParameter(0,Eprime);
   sigma_born->SetParameter(1,phi);
   sigma_born->SetNpx(npx);

   TF1 * sigma2_born = new TF1("sigma2born", xsec_born, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_born->SetLineColor(2);
   sigma2_born->SetParameter(0,Q2);
   sigma2_born->SetParameter(1,phi);
   sigma2_born->SetNpx(npx);

   leg->AddEntry(sigmaE_born,"F1F209 Inelastic (Born)","l");

   //InSANEInclusiveDiffXSec * fDiffXSec = new  InSANEInclusiveDiffXSec();
   //InelasticInclusiveDiffXSec * fDiffXSec = new  InelasticInclusiveDiffXSec();

   // -----------------------------------
   InSANEPOLRADBornDiffXSec * xsec_polrad0 = new  InSANEPOLRADBornDiffXSec();
   xsec_polrad0->SetTargetNucleus(targ);
   xsec_polrad0->GetPOLRAD()->SetTargetNucleus(targ);
   xsec_polrad0->GetPOLRAD()->SetIRTMethod(InSANEPOLRAD::kAnglePeaking);
   xsec_polrad0->SetHelicity(1);
   xsec_polrad0->GetPOLRAD()->SetTargetPolarization(1.0);
   xsec_polrad0->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   xsec_polrad0->SetBeamEnergy(beamEnergy);
   xsec_polrad0->InitializePhaseSpaceVariables();
   xsec_polrad0->InitializeFinalStateParticles();
   xsec_polrad0->UsePhaseSpace(false);

   TF1 * sigmaE_polrad0 = new TF1("sigmaEpolrad0", xsec_polrad0, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_polrad0->SetLineColor(1);
   sigmaE_polrad0->SetParameter(0,theta);
   sigmaE_polrad0->SetParameter(1,phi);
   sigmaE_polrad0->SetNpx(npx);

   TF1 * sigma_polrad0 = new TF1("sigmapolrad0", xsec_polrad0, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_polrad0->SetLineColor(1);
   sigma_polrad0->SetParameter(0,Eprime);
   sigma_polrad0->SetParameter(1,phi);
   sigma_polrad0->SetNpx(npx);

   TF1 * sigma2_polrad0 = new TF1("sigma2polrad0", xsec_polrad0, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_polrad0->SetLineColor(1);
   sigma2_polrad0->SetParameter(0,Q2);
   sigma2_polrad0->SetParameter(1,phi);
   sigma2_polrad0->SetNpx(npx);

   leg->AddEntry(sigmaE_polrad0,"POLRAD (born)","l");

   // -----------------------------------
   InSANEPOLRADInelasticTailDiffXSec * xsec_polrad1 = new  InSANEPOLRADInelasticTailDiffXSec();
   xsec_polrad1->SetTargetNucleus(targ);
   xsec_polrad1->GetPOLRAD()->SetTargetNucleus(targ);
   xsec_polrad1->GetPOLRAD()->SetQRTMethod(InSANEPOLRAD::kAnglePeaking);
   xsec_polrad1->SetBeamEnergy(beamEnergy);
   xsec_polrad1->InitializePhaseSpaceVariables();
   xsec_polrad1->InitializeFinalStateParticles();
   xsec_polrad1->UsePhaseSpace(false);

   TF1 * sigmaE_polrad1 = new TF1("sigmaEpolrad1", xsec_polrad1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_polrad1->SetLineColor(1);
   sigmaE_polrad1->SetLineStyle(2);
   sigmaE_polrad1->SetParameter(0,theta);
   sigmaE_polrad1->SetParameter(1,phi);
   sigmaE_polrad1->SetNpx(npx);

   TF1 * sigma_polrad1 = new TF1("sigmapolrad1", xsec_polrad1, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_polrad1->SetLineColor(1);
   sigma_polrad1->SetLineStyle(2);
   sigma_polrad1->SetParameter(0,Eprime);
   sigma_polrad1->SetParameter(1,phi);
   sigma_polrad1->SetNpx(npx);

   TF1 * sigma2_polrad1 = new TF1("sigma2polrad1", xsec_polrad1, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_polrad1->SetLineColor(1);
   sigma2_polrad1->SetLineStyle(2);
   sigma2_polrad1->SetParameter(0,Q2);
   sigma2_polrad1->SetParameter(1,phi);
   sigma2_polrad1->SetNpx(npx);

   leg->AddEntry(sigmaE_polrad1,"POLRAD IRT - angle peaking","l");

   // -----------------------------------
   //InSANEPOLRADInelasticTailDiffXSec * xsec_polrad2 = new  InSANEPOLRADInelasticTailDiffXSec();
   //xsec_polrad2->SetTargetNucleus(targ);
   //xsec_polrad2->GetPOLRAD()->SetTargetNucleus(targ);
   //xsec_polrad2->GetPOLRAD()->SetQRTMethod(InSANEPOLRAD::kUltraRel);
   //xsec_polrad2->SetBeamEnergy(beamEnergy);
   //xsec_polrad2->InitializePhaseSpaceVariables();
   //xsec_polrad2->InitializeFinalStateParticles();
   //xsec_polrad2->UsePhaseSpace(false);

   //TF1 * sigmaE_polrad2 = new TF1("sigmaEpolrad2", xsec_polrad2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
   //      Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   //sigmaE_polrad2->SetLineColor(1);
   //sigmaE_polrad2->SetLineStyle(2);
   //sigmaE_polrad2->SetParameter(0,theta);
   //sigmaE_polrad2->SetParameter(1,phi);
   //sigmaE_polrad2->SetNpx(npx);

   //TF1 * sigma_polrad2 = new TF1("sigmapolrad2", xsec_polrad2, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
   //      minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   //sigma_polrad2->SetLineColor(1);
   //sigma_polrad2->SetLineStyle(2);
   //sigma_polrad2->SetParameter(0,Eprime);
   //sigma_polrad2->SetParameter(1,phi);
   //sigma_polrad2->SetNpx(npx);

   //TF1 * sigma2_polrad2 = new TF1("sigma2polrad2", xsec_polrad2, &InSANEInclusiveDiffXSec::WDependentXSec, 
   //      Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   //sigma2_polrad2->SetLineColor(1);
   //sigma2_polrad2->SetLineStyle(2);
   //sigma2_polrad2->SetParameter(0,Q2);
   //sigma2_polrad2->SetParameter(1,phi);
   //sigma2_polrad2->SetNpx(npx);

   //leg->AddEntry(sigmaE_polrad2,"POLRAD QRT - (ultra rel. approx.)","l");

   //// -----------------------------------
   //InSANEPOLRADInelasticTailDiffXSec * xsec_polrad3 = new  InSANEPOLRADInelasticTailDiffXSec();
   //xsec_polrad3->SetTargetNucleus(targ);
   //xsec_polrad3->GetPOLRAD()->SetTargetNucleus(targ);
   //xsec_polrad3->GetPOLRAD()->SetQRTMethod(InSANEPOLRAD::kFull);
   //xsec_polrad3->GetPOLRAD()->DoQEFullCalc(true);
   //xsec_polrad3->SetBeamEnergy(beamEnergy);
   //xsec_polrad3->InitializePhaseSpaceVariables();
   //xsec_polrad3->InitializeFinalStateParticles();
   //xsec_polrad3->UsePhaseSpace(false);

   //TF1 * sigmaE_polrad3 = new TF1("sigmaEpolrad3", xsec_polrad3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
   //      Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   //sigmaE_polrad3->SetLineColor(1);
   //sigmaE_polrad3->SetLineStyle(5);
   //sigmaE_polrad3->SetParameter(0,theta);
   //sigmaE_polrad3->SetParameter(1,phi);
   //sigmaE_polrad3->SetNpx(20);

   //TF1 * sigma_polrad3 = new TF1("sigmapolrad3", xsec_polrad3, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
   //      minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   //sigma_polrad3->SetLineColor(1);
   //sigma_polrad3->SetLineStyle(5);
   //sigma_polrad3->SetParameter(0,Eprime);
   //sigma_polrad3->SetParameter(1,phi);
   //sigma_polrad3->SetNpx(20);

   //TF1 * sigma2_polrad3 = new TF1("sigma2polrad3", xsec_polrad3, &InSANEInclusiveDiffXSec::WDependentXSec, 
   //      Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   //sigma2_polrad3->SetLineColor(1);
   //sigma2_polrad3->SetLineStyle(5);
   //sigma2_polrad3->SetParameter(0,Q2);
   //sigma2_polrad3->SetParameter(1,phi);
   //sigma2_polrad3->SetNpx(20);

   //leg->AddEntry(sigmaE_polrad3,"POLRAD QRT - (Full)","l");



   // ------------------------------------------
   ////InSANEInelasticRadiativeTail * xsec_born2 = new  InSANEInelasticRadiativeTail();
   //InelasticInclusiveDiffXSec * xsec_born2 = new  InelasticInclusiveDiffXSec();
   //xsec_born2->SetTargetNucleus(targ);
   //xsec_born2->SetBeamEnergy(beamEnergy);
   //xsec_born2->InitializePhaseSpaceVariables();
   //xsec_born2->InitializeFinalStateParticles();
   //xsec_born2->UsePhaseSpace(false);

   //TF1 * sigmaE_born2 = new TF1("sigmaEborn2", xsec_born2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
   //      Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   //sigmaE_born2->SetLineColor(3);
   //sigmaE_born2->SetParameter(0,theta);
   //sigmaE_born2->SetParameter(1,phi);
   //sigmaE_born2->SetNpx(100);

   //TF1 * sigma_born2 = new TF1("sigmaborn2", xsec_born2, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
   //      minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   //sigma_born2->SetLineColor(3);
   //sigma_born2->SetParameter(0,Eprime);
   //sigma_born2->SetParameter(1,phi);
   //sigma_born2->SetNpx(100);

   //TF1 * sigma2_born2 = new TF1("sigma2born2", xsec_born2, &InSANEInclusiveDiffXSec::WDependentXSec, 
   //      Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   //sigma2_born2->SetLineColor(3);
   //sigma2_born2->SetParameter(0,Q2);
   //sigma2_born2->SetParameter(1,phi);
   //sigma2_born2->SetNpx(100);

   //leg->AddEntry(sigmaE_born2,"InelasticInclusiveDiffXSec (born)","l");

   //// -----------------------------------------------------
   InSANERadiator<F1F209eInclusiveDiffXSec> * xsec_radiator = new  InSANERadiator<F1F209eInclusiveDiffXSec>();
   xsec_radiator->SetDeltaM(0.0);
   xsec_radiator->SetInternalOnly(true);
   xsec_radiator->SetTargetNucleus(targ);
   //xsec_born->GetPOLRAD()->SetTargetNucleus(targ);
   xsec_radiator->SetBeamEnergy(beamEnergy);
   xsec_radiator->InitializePhaseSpaceVariables();
   xsec_radiator->InitializeFinalStateParticles();
   xsec_radiator->UsePhaseSpace(false);

   TF1 * sigmaE_radiator = new TF1("sigmaEradiator", xsec_radiator, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_radiator->SetLineColor(4);
   sigmaE_radiator->SetParameter(0,theta);
   sigmaE_radiator->SetParameter(1,phi);
   sigmaE_radiator->SetNpx(100);

   TF1 * sigma_radiator = new TF1("sigmaradiator", xsec_radiator, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_radiator->SetLineColor(4);
   sigma_radiator->SetParameter(0,Eprime);
   sigma_radiator->SetParameter(1,phi);
   sigma_radiator->SetNpx(100);

   TF1 * sigma2_radiator = new TF1("sigma2radiator", xsec_radiator, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_radiator->SetLineColor(4);
   sigma2_radiator->SetParameter(0,Q2);
   sigma2_radiator->SetParameter(1,phi);
   sigma2_radiator->SetNpx(100);

   leg->AddEntry(sigmaE_radiator,"InSANERadiator<InelasticInclusiveDiffXSec> - (internal)","l");

   // -------------------------------------------------------------------------------

   TCanvas * c = new TCanvas("xsec_test_quasilastic","Inelastic xsec");
   c->Divide(2,2);

   // ----------------------------

   c->cd(1);
   gPad->SetLogy(true);
   TString title      =  xsec_born->GetPlotTitle();
   TString yAxisTitle =  xsec_born->GetLabel();

   //sigma->SetLineColor(kRed);
   sigmaE_born->Draw();
   sigmaE_born->SetTitle(title);
   sigmaE_born->GetXaxis()->SetTitle("E' (GeV)");
   sigmaE_born->GetXaxis()->CenterTitle(true);
   sigmaE_born->GetYaxis()->SetTitle(yAxisTitle);
   sigmaE_born->GetYaxis()->CenterTitle(true);
   sigmaE_born->Draw();
   //sigmaE_born2->DrawCopy("same");
   sigmaE_polrad0->DrawCopy("same");
   //sigmaE_polrad1->DrawCopy("same");
   //sigmaE_polrad2->DrawCopy("same");
   //sigmaE_polrad3->DrawCopy("same");
   //sigmaE_radiator->DrawCopy("same");

   // ----------------------------

   c->cd(2);
   gPad->SetLogy(true);
   sigma2_born->Draw();
   sigma2_born->SetTitle(title);
   sigma2_born->GetXaxis()->SetTitle("W (GeV)");
   sigma2_born->GetXaxis()->CenterTitle(true);
   sigma2_born->GetYaxis()->SetTitle(yAxisTitle);
   sigma2_born->GetYaxis()->CenterTitle(true);
   sigma2_born->Draw();

   //sigma2_born2->Draw("same");
   //sigma2_polrad1->Draw("same");
   //sigma2_polrad2->Draw("same");
   //sigma2_polrad3->Draw("same");
   //sigma2_radiator->Draw("same");

   c->cd(3);
   gPad->SetLogy(true);
   sigma_born->Draw();
   sigma_born->SetTitle(title);
   sigma_born->GetXaxis()->SetTitle("#theta (deg)");
   sigma_born->GetXaxis()->CenterTitle(true);
   sigma_born->GetYaxis()->SetTitle(yAxisTitle);
   sigma_born->GetYaxis()->CenterTitle(true);
   sigma_born->Draw();
   //sigma_born2->Draw("same");
   //sigma_polrad1->Draw("same");
   //sigma_polrad2->Draw("same");
   //sigma_polrad3->Draw("same");
   //sigma_radiator->Draw("same");

   // ----------------------------

   c->cd(4);

   leg->Draw();

   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_Inelastic_%d.png",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_Inelastic_%d.pdf",aNumber));

   return 0;
}

