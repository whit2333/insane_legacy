Int_t PionProduction_electro_compare(Double_t beamEnergy = 5.9, Double_t th_pi = 45.0){
   Int_t pid = 211;

   TString labelNote = "d2n experiment on ^{3}He";
   std::ifstream file("/home/whit/pion_data/pion/Positive/cs_5-pass.dat");
   double x,y,ey;
   std::vector<double> x_energy,y_sigma;
   std::vector<double> ex_energy,ey_sigma;
   while( !(file.eof()) ){
      file >> x >> y >> ey ;
      double p_pi = x/1000.0;
      double e_pi =  TMath::Sqrt(p_pi*p_pi + M_pion*M_pion/(GeV*GeV));
      x_energy.push_back(e_pi); 
      ex_energy.push_back(0.0); 
      // E/p is the jacobian dp/dE
      y_sigma.push_back((e_pi/p_pi)*y); 
      ey_sigma.push_back((e_pi/p_pi)*(ey+0.10*y)); 
      //y_sigma.push_back(TMath::Power(p_pi,3.0)*y); 
   }
   TGraphErrors * dataGr = new TGraphErrors(x_energy.size(),&x_energy[0], &y_sigma[0],&ex_energy[0], &ey_sigma[0]);
   dataGr->SetMarkerStyle(20);
   dataGr->SetMarkerSize(0.5);

   //TString labelNote = "Oconnell data on H";
   //std::ifstream file("/home/whit/work/pion_production/oconnell_data_piminus_5GeV_13deg.png.dat");
   //double x,y,ey;
   //std::vector<double> x_energy,y_sigma;
   //std::vector<double> ex_energy,ey_sigma;
   //while( !(file.eof()) ){
   //   file >> x >> y ;
   //   double p_pi = x;
   //   double e_pi = TMath::Sqrt(p_pi*p_pi + M_pion*M_pion/(GeV*GeV));
   //   x_energy.push_back(e_pi); 
   //   ex_energy.push_back(0.0); 
   //   // E/p is the jacobian dp/dE
   //   y_sigma.push_back((e_pi/p_pi)*y*1000.0); 
   //   ey_sigma.push_back((e_pi/p_pi)*1000.0*(ey+0.15*y)); 
   //   //y_sigma.push_back(TMath::Power(p_pi,3.0)*y); 
   //}
   //TGraphErrors * dataGr = new TGraphErrors(x_energy.size(),&x_energy[0], &y_sigma[0],&ex_energy[0], &ey_sigma[0]);
   //dataGr->SetMarkerStyle(20);

   //Double_t beamEnergy = 1.2;//5.9; //GeV
   Double_t theta_pi   = th_pi*degree;
   Double_t phi_pi     = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   //InSANENucleus targetNucleus = InSANENucleus::Proton();//(1,1);
   InSANENucleus targetNucleus(2,3);
   Double_t wiser_rad_len = 0.046;

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.2;
   Double_t Emax     = beamEnergy-0.1;
   if(Emax>2.0) Emax = 3.0;
   Int_t    Npx      = 20;

   TLatex * latex = new TLatex();
   latex->SetNDC();
   //latex->SetTextAlign(21);
   TLegend * leg = new TLegend(0.6,0.6,0.85,0.85);
   leg->SetFillColor(0); 
   leg->SetBorderSize(0);
   leg->SetFillStyle(0); 
   //leg->SetHeader("MAID cross sections");
   TVector3 pt(0.0,0.0,0.0);

   //-------------------------------------------------------
   // MAID unpolarized
   //MAIDInclusivePi0DiffXSec *fDiffXSec = new MAIDInclusivePi0DiffXSec();
   MAIDInclusivePionDiffXSec *fDiffXSec = new MAIDInclusivePionDiffXSec("n","piminus");
   fDiffXSec->SetBeamEnergy(   beamEnergy);
   fDiffXSec->SetTargetNucleus(targetNucleus);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
  // fDiffXSec->Refresh();

   TF1 * sigmaE = new TF1("sigma0", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta_pi);
   sigmaE->SetParameter(1,phi_pi);
   sigmaE->SetNpx(Npx);
   sigmaE->SetLineColor(8);
   leg->AddEntry(sigmaE,"MAID","l");

   //-------------------------------------------------------
   // OAR Pion 
   ElectroOARPionDiffXSec *fDiffXSec1 = new ElectroOARPionDiffXSec();
   //OARPionElectroDiffXSec *fDiffXSec1 = new OARPionElectroDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetTargetNucleus(targetNucleus);
   fDiffXSec1->UsePhaseSpace(false);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();

   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE1->SetParameter(0,theta_pi);
   sigmaE1->SetParameter(1,phi_pi);
   sigmaE1->SetNpx(Npx);
   sigmaE1->SetLineColor(6);
   leg->AddEntry(sigmaE1,"OAR Pion","l");

   //-------------------------------------------------------
   // EPCV 
   InSANEInclusiveEPCVXSec2 *fDiffXSec2 = new InSANEInclusiveEPCVXSec2();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetNucleus(targetNucleus);
   fDiffXSec2->UsePhaseSpace(false);
   fDiffXSec2->SetProductionParticleType(pid);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta_pi);
   sigmaE2->SetParameter(1,phi_pi);
   sigmaE2->SetNpx(Npx);
   sigmaE2->SetLineColor(kBlack);
   leg->AddEntry(sigmaE2,"EPCV","l");

   //-------------------------------------------------------
   // Wiser 
   InSANEInclusiveWiserXSec *fDiffXSec3 = new InSANEInclusiveWiserXSec();
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   fDiffXSec3->SetTargetNucleus(targetNucleus);
   fDiffXSec3->SetRadiationLength(wiser_rad_len);
   fDiffXSec3->SetProductionParticleType(pid);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   fDiffXSec3->GetPhaseSpace()->GetVariableWithName("energy")->SetMinimum(0.01);
   fDiffXSec3->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(0.0001);
   fDiffXSec3->Refresh();
   TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE3->SetParameter(0,theta_pi);
   sigmaE3->SetParameter(1,phi_pi);
   sigmaE3->SetNpx(Npx);
   sigmaE3->SetLineColor(2);
   leg->AddEntry(sigmaE3,Form("Wiser - %.1f %% ",wiser_rad_len*100.0),"l");

   //-------------------------------------------------------
   // new Wiser
   //WiserInclusiveElectroXSec *fDiffXSec4 = new WiserInclusiveElectroXSec();
   ElectroWiserDiffXSec *fDiffXSec4 = new ElectroWiserDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetTargetNucleus(targetNucleus);
   fDiffXSec4->SetProductionParticleType(pid);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   fDiffXSec4->GetPhaseSpace()->GetVariableWithName("energy")->SetMinimum(0.01);
   fDiffXSec4->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(0.0001);
   fDiffXSec4->Refresh();

   TF1 * sigma4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma4->SetParameter(0,theta_pi);
   sigma4->SetParameter(1,phi_pi);
   sigma4->SetNpx(Npx);
   sigma4->SetLineColor(kBlue);
   sigma4->SetLineWidth(2);
   leg->AddEntry(sigma4,"New ElectroWiser","l");


   //-------------------------------------------------------
   TCanvas * c        = new TCanvas("EPCVInclusive","EPCV Inclusive");
   gPad->SetLogy(true);
   TString title      = "Pion Electroproduction"; 
   TString yAxisTitle = " " ;// fDiffXSec->GetLabel();
   TString xAxisTitle = "E_{#pi} (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;
 
   std::cout << "test" << std::endl;

   // MAID
   gr = new TGraph(sigmaE->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   // OARPion
   gr = new TGraph(sigmaE1->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   // EPC
   gr = new TGraph(sigmaE2->DrawCopy("goff")->GetHistogram());
   //for( int j = gr->GetN()-1 ;j>=0; j--) {
   //   Double_t xt, yt;
   //   gr->GetPoint(j,xt,yt);
   //   Double_t kmin = InSANE::Kine::k_min_photoproduction(xt,theta_pi); 
   //   Double_t Q_equiv = InSANE::Kine::Q_equiv_quanta(kmin,beamEnergy);
   //   Double_t n_gamma = InSANE::Kine::N_gamma(1.0,kmin,beamEnergy);
   //   gr->SetPoint(j,xt,yt*n_gamma);
   //}
   mg->Add(gr,"l");

   // WISER
   gr = new TGraph(sigmaE3->DrawCopy("goff")->GetHistogram());
   //for( int j = gr->GetN()-1 ;j>=0; j--) {
   //   Double_t xt, yt;
   //   gr->GetPoint(j,xt,yt);
   //   Double_t kmin = InSANE::Kine::k_min_photoproduction(xt,theta_pi); 
   //   Double_t Q_equiv = InSANE::Kine::Q_equiv_quanta(kmin,beamEnergy);
   //   Double_t n_gamma = InSANE::Kine::N_gamma(1.0,kmin,beamEnergy);
   //   gr->SetPoint(j,xt,yt*n_gamma);
   //}
   mg->Add(gr,"l");

   // New wiser
   gr = new TGraph(sigma4->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   mg->Add(dataGr,"p");
   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);
   mg->GetYaxis()->SetRangeUser(1.0,5.0e4);

   c->Update();
   gPad->Modified();

   latex->DrawLatex(0.3,0.3,Form("#theta_{#pi}=%.0f",theta_pi/degree));
   latex->DrawLatex(0.3,0.22,Form("E=%.2fGeV",beamEnergy));
   latex->DrawLatex(0.3,0.15,labelNote.Data());
   leg->Draw();

   c->SaveAs(Form("results/cross_sections/inclusive/PionProduction_electro_compare_%d_%d_%d_2.png",int(beamEnergy*10.0),int(th_pi),pid));
   c->SaveAs(Form("results/cross_sections/inclusive/PionProduction_electro_compare_%d_%d_%d_2.pdf",int(beamEnergy*10.0),int(th_pi),pid));
   c->SaveAs(Form("results/cross_sections/inclusive/PionProduction_electro_compare_%d_%d_%d_2.tex",int(beamEnergy*10.0),int(th_pi),pid));

   return 0;
}
