/*! 
 */ 
Int_t xsec_test_F1F209_QE(Int_t aNumber = 43 ){

   Double_t beamEnergy = 2.8; //GeV
   Double_t Eprime     = 1.5; 
   Double_t theta      = 30.*degree;
   Double_t phi        = 0.0*degree;  
   Double_t Z          = 1;   
   Double_t Q2         = 4.0*beamEnergy*Eprime*TMath::Power(TMath::Sin(theta/2.0),2.0);

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 200;
   Double_t Emin     = 0.25;
   Double_t Emax     = 2.8;//beamEnergy;
   Double_t minTheta = 20*degree;
   Double_t maxTheta = 40*degree;
   Double_t Wmin     = 0.5;
   Double_t Wmax     = beamEnergy;
   InSANENucleus targ(1,2);
   InSANENucleus targ2(6,12);

   // -----------------------------------------------------------------------------------
   F1F209QuasiElasticDiffXSec * xsec_born = new F1F209QuasiElasticDiffXSec();
   xsec_born->SetTargetNucleus(targ);
   xsec_born->SetBeamEnergy(beamEnergy);
   xsec_born->InitializePhaseSpaceVariables();
   xsec_born->InitializeFinalStateParticles();
   xsec_born->UsePhaseSpace(false);

   TF1 * sigmaE_born = new TF1("sigmaEborn", xsec_born, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_born->SetLineColor(2);
   sigmaE_born->SetParameter(0,theta);
   sigmaE_born->SetParameter(1,phi);
   sigmaE_born->SetNpx(npx);

   // -----------------------------------------------------------------------------------
   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   fman->CreateSFs(9);
   InSANEInclusiveBornDISXSec * xsec_born2 = new InSANEInclusiveBornDISXSec();
   xsec_born2->SetTargetNucleus(targ);
   xsec_born2->SetBeamEnergy(beamEnergy);
   xsec_born2->InitializePhaseSpaceVariables();
   xsec_born2->InitializeFinalStateParticles();
   xsec_born2->UsePhaseSpace(false);

   TF1 * sigmaE_born2 = new TF1("sigmaEborn2", xsec_born2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_born2->SetLineColor(2);
   sigmaE_born2->SetParameter(0,theta);
   sigmaE_born2->SetParameter(1,phi);
   sigmaE_born2->SetNpx(npx);


   //InSANEInclusiveDiffXSec * fDiffXSec = new  InSANEInclusiveDiffXSec();
   //QuasiElasticInclusiveDiffXSec * fDiffXSec = new  QuasiElasticInclusiveDiffXSec();
   // -----------------------------------------------------------------------------------
   F1F209eInclusiveDiffXSec * xsec_radiator = new  F1F209eInclusiveDiffXSec();
   xsec_radiator->SetTargetNucleus(targ);
   xsec_radiator->SetBeamEnergy(beamEnergy);
   xsec_radiator->InitializePhaseSpaceVariables();
   xsec_radiator->InitializeFinalStateParticles();
   xsec_radiator->UsePhaseSpace(false);

   TF1 * sigmaE_radiator = new TF1("sigmaEradiator", xsec_radiator, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_radiator->SetLineColor(4);
   sigmaE_radiator->SetLineStyle(2);
   sigmaE_radiator->SetParameter(0,theta);
   sigmaE_radiator->SetParameter(1,phi);
   sigmaE_radiator->SetNpx(npx);

   TF1 * sigma_radiator = new TF1("sigmaradiator", xsec_radiator, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_radiator->SetLineColor(4);
   sigma_radiator->SetLineStyle(2);
   sigma_radiator->SetParameter(0,Eprime);
   sigma_radiator->SetParameter(1,phi);
   sigma_radiator->SetNpx(100);

   TF1 * sigma2_radiator = new TF1("sigma2radiator", xsec_radiator, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_radiator->SetLineColor(4);
   sigma2_radiator->SetLineStyle(2);
   sigma2_radiator->SetParameter(0,Q2);
   sigma2_radiator->SetParameter(1,phi);
   sigma2_radiator->SetNpx(100);

   // -----------------------------------------------------------------------------------
   InSANERadiator<F1F209QuasiElasticDiffXSec> * xsec_radiator2 = new  InSANERadiator<F1F209QuasiElasticDiffXSec>();
   xsec_radiator2->SetDeltaM(0.0);
   xsec_radiator2->SetInternalOnly(true);
   xsec_radiator2->SetTargetNucleus(targ);
   xsec_radiator2->SetBeamEnergy(beamEnergy);
   xsec_radiator2->InitializePhaseSpaceVariables();
   xsec_radiator2->InitializeFinalStateParticles();
   xsec_radiator2->UsePhaseSpace(false);

   TF1 * sigmaE_radiator2 = new TF1("sigmaEradiator2", xsec_radiator2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_radiator2->SetLineColor(2);
   sigmaE_radiator2->SetLineStyle(2);
   sigmaE_radiator2->SetParameter(0,theta);
   sigmaE_radiator2->SetParameter(1,phi);
   sigmaE_radiator2->SetNpx(npx);

   TF1 * sigma_radiator2 = new TF1("sigmaradiator2", xsec_radiator2, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_radiator2->SetLineColor(2);
   sigma_radiator2->SetLineStyle(2);
   sigma_radiator2->SetParameter(0,Eprime);
   sigma_radiator2->SetParameter(1,phi);
   sigma_radiator2->SetNpx(npx);

   TF1 * sigma2_radiator2 = new TF1("sigma2radiator2", xsec_radiator2, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_radiator2->SetLineColor(2);
   sigma2_radiator2->SetLineStyle(2);
   sigma2_radiator2->SetParameter(0,Q2);
   sigma2_radiator2->SetParameter(1,phi);
   sigma2_radiator2->SetNpx(npx);

   // -------------------------------------------------------------------------------

   TLegend * leg = new TLegend(0.7,0.7,0.8,0.87);
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   //leg->SetHeader("E = 2.7 GeV");

   //leg->AddEntry(sigmaE_radiator2,"InSANERadiator<F1F209QuasiElasticDiffXSec> - (internal)","l");
   //leg->AddEntry(sigmaE_born2,"QuasiElasticInclusiveDiffXSec (born)","l");
   //leg->AddEntry(sigmaE_radiator, "InSANERadiator<QuasiElasticInclusiveDiffXSec> - (internal)","l");
   //leg->AddEntry(sigmaE_polrad1,"POLRAD QRT - angle peaking","l");
   //leg->AddEntry(sigmaE_polrad2,"POLRAD QRT - (ultra rel. approx.)","l");
   //leg->AddEntry(sigmaE_polrad3,"POLRAD QRT - (Full)","l");

   // ----------------------------

   TCanvas * c = new TCanvas("xsec_test_quasilastic","quasielastic xsec");
   //c->Divide(2,2);
   TGraph * gr = 0;

   // ----------------------------

   c->cd(1);
   TMultiGraph * mg1 = new TMultiGraph();
   //gPad->SetLogy(true);
   TString title      =  xsec_born->GetPlotTitle();
   TString yAxisTitle =  xsec_born->GetLabel();
   
   sigmaE_born->SetLineColor(2);
   sigmaE_radiator->SetLineColor(2);
   gr = new TGraph(sigmaE_born->DrawCopy("goff")->GetHistogram());;
   leg->AddEntry(gr, "^{2}H","l");
   mg1->Add(gr,"l");

   gr = new TGraph(sigmaE_born2->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");

   gr = new TGraph(sigmaE_radiator->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");
   //gr = new TGraph(sigmaE_radiator2->DrawCopy("goff")->GetHistogram());;
   //mg1->Add(gr,"l");

   xsec_born->SetTargetNucleus(targ2);
   xsec_radiator->SetTargetNucleus(targ2);
   sigmaE_born->SetLineColor(4);
   sigmaE_radiator->SetLineColor(4);

   gr = new TGraph(sigmaE_born->DrawCopy("goff")->GetHistogram());;
   leg->AddEntry(gr, "^{12}C","l");
   mg1->Add(gr,"l");
   //gr = new TGraph(sigmaE_born2->DrawCopy("goff")->GetHistogram());;
   //mg1->Add(gr,"l");
   //gr = new TGraph(sigmaE_polrad1->DrawCopy("goff")->GetHistogram());;
   //mg1->Add(gr,"l");
   //gr = new TGraph(sigmaE_polrad2->DrawCopy("goff")->GetHistogram());;
   //mg1->Add(gr,"l");
   //gr = new TGraph(sigmaE_polrad3->DrawCopy("goff")->GetHistogram());;
   //mg1->Add(gr,"l");
   gr = new TGraph(sigmaE_radiator->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");
   //gr = new TGraph(sigmaE_radiator2->DrawCopy("goff")->GetHistogram());;

   mg1->Draw("a");
   mg1->SetTitle("");
   mg1->GetXaxis()->SetTitle("E' (GeV)");
   mg1->GetXaxis()->CenterTitle(true);
   mg1->GetYaxis()->SetTitle(yAxisTitle);
   mg1->GetYaxis()->CenterTitle(true);
   //mg1->GetXaxis()->SetLimits(0.2,1.7);
   //mg1->GetYaxis()->SetRangeUser(5,890);


   // ----------------------------

   //c->cd(2);
   //TMultiGraph * mg2 = new TMultiGraph();
   //gPad->SetLogy(true);

   //gr = new TGraph(sigma2_born->DrawCopy("goff")->GetHistogram());;
   //mg2->Add(gr,"l");
   //gr = new TGraph(sigma2_born2->DrawCopy("goff")->GetHistogram());;
   //mg2->Add(gr,"l");
   //gr = new TGraph(sigma2_polrad1->DrawCopy("goff")->GetHistogram());;
   //mg2->Add(gr,"l");
   //gr = new TGraph(sigma2_polrad2->DrawCopy("goff")->GetHistogram());;
   //mg2->Add(gr,"l");
   ////gr = new TGraph(sigma2_polrad3->DrawCopy("goff")->GetHistogram());;
   ////mg2->Add(gr,"l");
   //gr = new TGraph(sigma2_radiator->DrawCopy("goff")->GetHistogram());;
   //mg2->Add(gr,"l");
   //gr = new TGraph(sigma2_radiator2->DrawCopy("goff")->GetHistogram());;
   //mg2->Add(gr,"l");

   //mg2->Draw("a");
   //mg2->SetTitle("");
   //mg2->GetXaxis()->SetTitle("W (GeV)");
   //mg2->GetXaxis()->CenterTitle(true);
   //mg2->GetYaxis()->SetTitle(yAxisTitle);
   //mg2->GetYaxis()->CenterTitle(true);

   // ----------------------------

   //c->cd(3);
   //TMultiGraph * mg3 = new TMultiGraph();
   ////gPad->SetLogy(true);

   //gr = new TGraph(sigma_born->DrawCopy("goff")->GetHistogram());;
   //mg3->Add(gr,"l");
   //gr = new TGraph(sigma_born2->DrawCopy("goff")->GetHistogram());;
   //mg3->Add(gr,"l");
   //gr = new TGraph(sigma_polrad1->DrawCopy("goff")->GetHistogram());;
   //mg3->Add(gr,"l");
   //gr = new TGraph(sigma_polrad2->DrawCopy("goff")->GetHistogram());;
   //mg3->Add(gr,"l");
   ////gr = new TGraph(sigma_polrad3->DrawCopy("goff")->GetHistogram());;
   ////mg3->Add(gr,"l");
   //gr = new TGraph(sigma_radiator->DrawCopy("goff")->GetHistogram());;
   //mg3->Add(gr,"l");
   //gr = new TGraph(sigma_radiator2->DrawCopy("goff")->GetHistogram());;
   //mg3->Add(gr,"l");

   //mg3->Draw("a");
   //mg3->SetTitle("");
   //mg3->GetXaxis()->SetTitle("#theta (rad)");
   //mg3->GetXaxis()->CenterTitle(true);
   //mg3->GetYaxis()->SetTitle(yAxisTitle);
   //mg3->GetYaxis()->CenterTitle(true);


   //// ----------------------------

   //c->cd(4);

   leg->Draw();

   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_F1F209_QE_%d.png",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_F1F209_QE_%d.pdf",aNumber));

   return 0;
}

