Int_t xsec_test_POLRAD_IRT(Double_t theta_target = 0*degree){

   Double_t beamEnergy = 5.49; //GeV
   Double_t Eprime     = 0.1; 
   Double_t theta      = 13.0*degree;
   Double_t phi        = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 10;
   Double_t Emin     = 3.5;
   Double_t Emax     = 5.1;

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("Elastic radiative tail cross sections");

   //-----------------------------------
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetType(InSANENucleus::kProton);
   fDiffXSec->GetPOLRAD()->SetHelicity(0);
   fDiffXSec->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec->GetPOLRAD()->SetPolarizationVectors(P_target,0.0);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetNpx(npx);
   leg->AddEntry(sigmaE,"unpolarized","l");

   //-----------------------------------
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec1 = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetTargetType(InSANENucleus::kProton);
   fDiffXSec1->GetPOLRAD()->SetHelicity(0);
   fDiffXSec1->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec1->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE1->SetParameter(0,theta);
   sigmaE1->SetParameter(1,phi);
   sigmaE1->SetNpx(npx);
   sigmaE1->SetLineColor(kRed);
   leg->AddEntry(sigmaE1,"+polarized","l");

   //-----------------------------------
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec2 = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetType(InSANENucleus::kProton);
   fDiffXSec2->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec2->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec2->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetNpx(npx);
   sigmaE2->SetLineColor(kBlue);
   leg->AddEntry(sigmaE2,"-polarized","l");

   //-----------------------------------
   // born polarized + 
   InSANEPOLRADBornDiffXSec * fDiffXSecb1 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSecb1->SetBeamEnergy(beamEnergy);
   fDiffXSecb1->SetTargetType(InSANENucleus::kProton);
   fDiffXSecb1->GetPOLRAD()->SetHelicity(1);
   fDiffXSecb1->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSecb1->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSecb1->InitializePhaseSpaceVariables();
   fDiffXSecb1->InitializeFinalStateParticles();
   fDiffXSecb1->Refresh();
   TF1 * sigmab1 = new TF1("sigmab1", fDiffXSecb1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmab1->SetParameter(0,theta);
   sigmab1->SetParameter(1,phi);
   sigmab1->SetNpx(npx);
   sigmab1->SetLineColor(kRed);
   sigmab1->SetLineStyle(2);
   leg->AddEntry(sigmab1,"born +","l");

   // born polarized - 
   InSANEPOLRADBornDiffXSec * fDiffXSecb2 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSecb2->SetBeamEnergy(beamEnergy);
   fDiffXSecb2->SetTargetType(InSANENucleus::kProton);
   fDiffXSecb2->GetPOLRAD()->SetHelicity(-1);
   fDiffXSecb2->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSecb2->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSecb2->InitializePhaseSpaceVariables();
   fDiffXSecb2->InitializeFinalStateParticles();
   fDiffXSecb2->Refresh();
   TF1 * sigmab2 = new TF1("sigmab2", fDiffXSecb2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmab2->SetParameter(0,theta);
   sigmab2->SetParameter(1,phi);
   sigmab2->SetNpx(npx);
   sigmab2->SetLineColor(kBlue);
   sigmab2->SetLineStyle(2);
   leg->AddEntry(sigmab2,"born - ","l");

   //-----------------------
   TCanvas * c = new TCanvas("xsec_test_POLRAD_IRT","xsec_test_POLRAD_IRT");
   //gPad->SetLogy(true);
   TString title =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;
   //gr = new TGraph(sigmaE->DrawCopy("same")->GetHistogram());;
   //mg->Add(gr,"l");
   gr = new TGraph(sigmaE1->DrawCopy("same")->GetHistogram());;
   mg->Add(gr,"l");
   gr = new TGraph(sigmaE2->DrawCopy("same")->GetHistogram());;
   mg->Add(gr,"l");
   gr = new TGraph(sigmab1->DrawCopy("same")->GetHistogram());;
   mg->Add(gr,"l");
   gr = new TGraph(sigmab2->DrawCopy("same")->GetHistogram());;
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   gPad->Modified();
   leg->Draw();

   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRAD_IRT_1.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRAD_IRT_1.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRAD_IRT_1.tex");
   return 0;
}
