/// Compute the cross section using the MAID model for a 3He target 

#include "./src/Graph.C"

/// Polarizations of the neutron and proton in 3He 
Double_t Pn = 0.879;
Double_t Pp = -0.021;
/// including off-shell effects 
Double_t PnTilde  = Pn + 0.056;
Double_t PpTilde  = 2.*Pp - 0.014; 

Int_t test(){

   //Double_t Es         = 3.381; //GeV
   Double_t Es         = 2.581; //GeV
   Double_t theta      = 15.5*degree;

   TString TargPolType = Form("para");

   // E94-010 data 
   TGraph *gE94010 = GetE94010Data(Es,TargPolType); 
   SetGraphParameters(gE94010,20,kRed); 

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    Npx      = 200;
   // Double_t Emin     = 1.6;
   // Double_t Emax     = 2.85;
   Double_t Emin     = 1.6;
   Double_t Emax     = 2.85;
   Double_t EpStep   = (Emax-Emin)/( (double)Npx ); 

   Double_t Wmin     = 1.100; 
   Double_t Wmax     = 2.200; 
   Double_t WStep    = (Wmax-Wmin)/( (double)Npx ); 

   // target information 
   InSANENucleus::NucleusType He3 = InSANENucleus::k3He; 
   InSANENucleus *Target = new InSANENucleus(); 
   Target->SetType(He3); 

   Double_t A = Target->GetA(); 
   Double_t Z = Target->GetZ(); 

   TLegend * leg = new TLegend(0.1,0.7,0.3,0.9);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("MAID cross sections");

   TString pi0     = "pi0";
   TString piPlus  = "piplus"; 
   TString piMinus = "piminus"; 
   TString neutron = "n"; 
   TString proton  = "p";

   InSANENucleus::NucleusType Proton  = InSANENucleus::kProton; 
   InSANENucleus::NucleusType Neutron = InSANENucleus::kNeutron; 

   /// unpolarized 
   // neutron target 
   MAIDInclusiveDiffXSec *u_pi0_n = new MAIDInclusiveDiffXSec(pi0,neutron);
   u_pi0_n->SetBeamEnergy(Es);
   u_pi0_n->SetTargetType(Neutron);
   u_pi0_n->InitializePhaseSpaceVariables();
   u_pi0_n->InitializeFinalStateParticles();
   u_pi0_n->Refresh();

   MAIDInclusiveDiffXSec *u_piMinus_p = new MAIDInclusiveDiffXSec(piMinus,proton);
   u_piMinus_p->SetBeamEnergy(Es);
   u_piMinus_p->SetTargetType(Neutron);
   u_piMinus_p->InitializePhaseSpaceVariables();
   u_piMinus_p->InitializeFinalStateParticles();
   u_piMinus_p->Refresh();

   // proton target 
   MAIDInclusiveDiffXSec *u_pi0_p     = new MAIDInclusiveDiffXSec(pi0,proton);
   u_pi0_p->SetBeamEnergy(Es);
   u_pi0_p->SetTargetType(Proton);
   u_pi0_p->InitializePhaseSpaceVariables();
   u_pi0_p->InitializeFinalStateParticles();
   u_pi0_p->Refresh();

   MAIDInclusiveDiffXSec *u_piPlus_n  = new MAIDInclusiveDiffXSec(piPlus,neutron);
   u_piPlus_n->SetBeamEnergy(Es);
   u_piPlus_n->SetTargetType(Proton);
   u_piPlus_n->InitializePhaseSpaceVariables();
   u_piPlus_n->InitializeFinalStateParticles();
   u_piPlus_n->Refresh();

   /// polarized
   Int_t MinusHel = -1; 
   Int_t PlusHel  =  1; 
   TVector3    pt(0.0,0.0,1.0);  // target polarized along the z-axis 

   /// minus helicity 
   // neutron target 
   MAIDInclusiveDiffXSec *p_pi0_n_mh = new MAIDInclusiveDiffXSec(pi0,neutron);
   p_pi0_n_mh->SetBeamEnergy(Es);
   p_pi0_n_mh->SetTargetType(Neutron);
   p_pi0_n_mh->SetHelicity(MinusHel);
   p_pi0_n_mh->SetTargetPolarization(pt);
   p_pi0_n_mh->InitializePhaseSpaceVariables();
   p_pi0_n_mh->InitializeFinalStateParticles();
   p_pi0_n_mh->Refresh();

   MAIDInclusiveDiffXSec *p_piMinus_p_mh = new MAIDInclusiveDiffXSec(piMinus,proton);
   p_piMinus_p_mh->SetBeamEnergy(Es);
   p_piMinus_p_mh->SetTargetType(Neutron);
   p_piMinus_p_mh->SetHelicity(MinusHel);
   p_piMinus_p_mh->SetTargetPolarization(pt);
   p_piMinus_p_mh->InitializePhaseSpaceVariables();
   p_piMinus_p_mh->InitializeFinalStateParticles();
   p_piMinus_p_mh->Refresh();

   // proton target 
   MAIDInclusiveDiffXSec *p_pi0_p_mh     = new MAIDInclusiveDiffXSec(pi0,proton);
   p_pi0_p_mh->SetBeamEnergy(Es);
   p_pi0_p_mh->SetTargetType(Proton);
   p_pi0_p_mh->SetHelicity(MinusHel); 
   p_pi0_p_mh->SetTargetPolarization(pt); 
   p_pi0_p_mh->InitializePhaseSpaceVariables();
   p_pi0_p_mh->InitializeFinalStateParticles();
   p_pi0_p_mh->Refresh();

   MAIDInclusiveDiffXSec *p_piPlus_n_mh  = new MAIDInclusiveDiffXSec(piPlus,neutron);
   p_piPlus_n_mh->SetBeamEnergy(Es);
   p_piPlus_n_mh->SetTargetType(Proton);
   p_piPlus_n_mh->SetHelicity(MinusHel);
   p_piPlus_n_mh->SetTargetPolarization(pt); 
   p_piPlus_n_mh->InitializePhaseSpaceVariables();
   p_piPlus_n_mh->InitializeFinalStateParticles();
   p_piPlus_n_mh->Refresh();

   /// plus helicity 
   // neutron target 
   MAIDInclusiveDiffXSec *p_pi0_n_ph = new MAIDInclusiveDiffXSec(pi0,neutron);
   p_pi0_n_ph->SetBeamEnergy(Es);
   p_pi0_n_ph->SetTargetType(Neutron);
   p_pi0_n_ph->SetHelicity(PlusHel);
   p_pi0_n_ph->SetTargetPolarization(pt);
   p_pi0_n_ph->InitializePhaseSpaceVariables();
   p_pi0_n_ph->InitializeFinalStateParticles();
   p_pi0_n_ph->Refresh();

   MAIDInclusiveDiffXSec *p_piMinus_p_ph = new MAIDInclusiveDiffXSec(piMinus,proton);
   p_piMinus_p_ph->SetBeamEnergy(Es);
   p_piMinus_p_ph->SetTargetType(Neutron);
   p_piMinus_p_ph->SetHelicity(PlusHel);
   p_piMinus_p_ph->SetTargetPolarization(pt);
   p_piMinus_p_ph->InitializePhaseSpaceVariables();
   p_piMinus_p_ph->InitializeFinalStateParticles();
   p_piMinus_p_ph->Refresh();

   // proton target 
   MAIDInclusiveDiffXSec *p_pi0_p_ph    = new MAIDInclusiveDiffXSec(pi0,proton);
   p_pi0_p_ph->SetBeamEnergy(Es);
   p_pi0_p_ph->SetTargetType(Proton);
   p_pi0_p_ph->SetHelicity(PlusHel); 
   p_pi0_p_ph->SetTargetPolarization(pt); 
   p_pi0_p_ph->InitializePhaseSpaceVariables();
   p_pi0_p_ph->InitializeFinalStateParticles();
   p_pi0_p_ph->Refresh();

   MAIDInclusiveDiffXSec *p_piPlus_n_ph  = new MAIDInclusiveDiffXSec(piPlus,neutron);
   p_piPlus_n_ph->SetBeamEnergy(Es);
   p_piPlus_n_ph->SetTargetType(Proton);
   p_piPlus_n_ph->SetHelicity(PlusHel);
   p_piPlus_n_ph->SetTargetPolarization(pt); 
   p_piPlus_n_ph->InitializePhaseSpaceVariables();
   p_piPlus_n_ph->InitializeFinalStateParticles();
   p_piPlus_n_ph->Refresh();

   // u_pi0_n->DebugMode(true); 

   // NOW, let's compute cross sections... 
   double iEp,iW,iNu,iQ2; 
   double arg_u,arg_ph,arg_mh,arg_diff,arg_hsum;
   vector<double> X,Ep,W,Nu,xs_u,xs_ph,xs_mh,xs_diff,xs_hsum;  
   for(int i=0;i<Npx;i++){
        // iEp      = Emin + ( (double)i )*EpStep; 
        // iW       = InSANE::Kine::W_EEprimeTheta(Es,iEp,theta); 
        iW       = Wmin + ( (double)i )*WStep;  
        iEp      = InSANE::Kine::Eprime_W2theta(iW*iW,theta,Es);
        iNu      = Es - iEp; 
        iQ2      = InSANE::Kine::Qsquared(Es,iEp,theta); 
        arg_u    = CalculateXSec(0,Z,A,iEp,theta,u_pi0_n,u_piMinus_p,u_pi0_p,u_piPlus_n);  
        arg_ph   = CalculateXSec(1,Z,A,iEp,theta,p_pi0_n_ph,p_piMinus_p_ph,p_pi0_p_ph,p_piPlus_n_ph);  
        arg_mh   = CalculateXSec(1,Z,A,iEp,theta,p_pi0_n_mh,p_piMinus_p_mh,p_pi0_p_mh,p_piPlus_n_mh);  
        arg_diff = arg_mh - arg_ph; 
        arg_hsum = 0.5*(arg_mh + arg_ph); 
        // cout << "i = "           << Form("%.d",i        ) << "\t" 
        //      << "Ep = "          << Form("%.3f",iEp     ) << "\t" 
        //      << "W = "           << Form("%.3f",iW      ) << "\t" 
        //      << "Q2 = "          << Form("%.3f",iQ2     ) << "\t" 
        //      << "xs (h = 0) = "  << Form("%.3E",arg_hsum) << "\t" 
        //      << "xs (h = -1) = " << Form("%.3E",arg_mh  ) << "\t" 
        //      << "xs (h = +1) = " << Form("%.3E",arg_ph  ) << endl; 
        Ep.push_back(iEp); 
        W.push_back(iW); 
        X.push_back(iW); 
        xs_u.push_back(arg_u); 
        xs_ph.push_back(arg_ph); 
        xs_mh.push_back(arg_mh); 
        xs_diff.push_back(arg_diff); 
        xs_hsum.push_back(arg_hsum); 
   }   

   TGraph *gXS_u = GetTGraph(X,xs_u);
   SetGraphParameters(gXS_u,20,kBlack,2);  

   TGraph *gXS_ph = GetTGraph(X,xs_ph);
   SetGraphParameters(gXS_ph,20,kBlue,2);  

   TGraph *gXS_mh = GetTGraph(X,xs_mh);
   SetGraphParameters(gXS_mh,20,kRed,2);  

   TGraph *gXS_diff = GetTGraph(X,xs_diff);
   SetGraphParameters(gXS_diff,20,kBlack,2);  

   TGraph *gXS_hsum = GetTGraph(X,xs_hsum);
   SetGraphParameters(gXS_hsum,20,kGreen+1,2);  

   TMultiGraph *MG = new TMultiGraph(); 
   MG->Add(gXS_u   ,"L"); 
   MG->Add(gXS_ph  ,"L"); 
   MG->Add(gXS_mh  ,"L"); 
   MG->Add(gXS_hsum,"L"); 

   TMultiGraph *MGDiff = new TMultiGraph(); 
   MGDiff->Add(gXS_diff,"L"); 
   MGDiff->Add(gE94010 ,"p"); 

   TLegend *LG = new TLegend(0.6,0.6,0.8,0.8); 
   LG->SetFillColor(kWhite); 
   LG->AddEntry(gXS_u ,"Unpolarized","l"); 
   LG->AddEntry(gXS_ph,"h = +1"     ,"l"); 
   LG->AddEntry(gXS_mh,"h = -1"     ,"l"); 
   LG->AddEntry(gXS_hsum,"(#sigma^{#downarrow#Uparrow} + #sigma^{#uparrow#Uparrow})/2","l"); 

   TLegend *LDiff = new TLegend(0.6,0.6,0.8,0.8); 
   LDiff->SetFillColor(kWhite); 
   LDiff->AddEntry(gXS_diff,"MAID"       ,"l"); 
   LDiff->AddEntry(gE94010 ,"JLab E94010","p"); 

   TString Title          = Form("^{3}He MAID Cross Section (E_{s} = %.2f GeV, #theta = %.1f#circ)",Es,theta/degree);
   TString TitleDiff      = Form("#Delta#sigma_{#parallel} = #sigma^{#downarrow#Uparrow} - #sigma^{#uparrow#Uparrow}");
   TString xAxisTitle     = Form("W (GeV)");
   TString yAxisTitle     = Form("#frac{d^{2}#sigma}{dE_{p}d#Omega} (nb/GeV/sr)");
   TString yAxisTitleDiff = Form("#Delta#sigma (nb/GeV/sr)");

   TCanvas *c1 = new TCanvas("c1","MAID 3He test",1200,800);
   c1->SetFillColor(kWhite);
   c1->Divide(2,1);  

   c1->cd(1); 
   MG->Draw("A");
   MG->SetTitle(Title); 
   MG->GetXaxis()->SetTitle(xAxisTitle); 
   MG->GetXaxis()->CenterTitle(); 
   MG->GetYaxis()->SetTitle(yAxisTitle); 
   MG->GetYaxis()->CenterTitle(); 
   MG->Draw("A");
   LG->Draw("same");
   c1->Update();

   c1->cd(2); 
   MGDiff->Draw("A");
   MGDiff->SetTitle(TitleDiff);
   MGDiff->GetXaxis()->SetTitle(xAxisTitle); 
   MGDiff->GetXaxis()->CenterTitle(); 
   MGDiff->GetYaxis()->SetTitle(yAxisTitleDiff); 
   MGDiff->GetYaxis()->CenterTitle(); 
   MGDiff->Draw("A");
   LDiff->Draw("same");
   c1->Update();

   return 0;
}
//______________________________________________________________________________
double CalculateXSec(int pol,double Z,double A,double Ep,double th,
                     MAIDInclusiveDiffXSec *pi0_n,MAIDInclusiveDiffXSec *piMinus_p,
                     MAIDInclusiveDiffXSec *pi0_p,MAIDInclusiveDiffXSec *piPlus_n){

   Double_t phi           = 0.; 
   Double_t par[3]        = {Ep,th,phi}; 
   Double_t arg_pi0_n     = pi0_n->EvaluateXSec(par); 
   Double_t arg_piMinus_p = piMinus_p->EvaluateXSec(par); 
   Double_t arg_pi0_p     = pi0_p->EvaluateXSec(par); 
   Double_t arg_piPlus_n  = piPlus_n->EvaluateXSec(par);
   Double_t xs            = 0.;  
   if(pol==0){
      xs = Z*(arg_pi0_p + arg_piPlus_n) + (A-Z)*(arg_pi0_n + arg_piMinus_p); 
   }else if(pol==1){
      xs = PpTilde*(arg_pi0_p + arg_piPlus_n) + PnTilde*(arg_pi0_n + arg_piMinus_p); 
   }
   return xs; 

}
//______________________________________________________________________________
TGraph *GetE94010Data(Double_t Es,TString TargPolType){

   double CONV = 1E+3; 
   vector<double> W,xs; 
   TString inpath = Form("./input/E94010_delta-sig-%s_Es-%.0f_th-15.dat",TargPolType.Data(),Es*CONV);
   ImportData(inpath,W,xs);

   TGraph *g = GetTGraph(W,xs); 
   return g; 

}
//______________________________________________________________________________
void ImportData(TString inpath,vector<double> &x,vector<double> &y){

   double CONV = 1E+3; 
   double ix,iy; 

   ifstream infile; 
   infile.open(inpath); 
   if(infile.fail()){
      cout << "Cannot open the file: " << inpath << endl;
      exit(1); 
   }else{
      while(!infile.eof()){
         infile >> ix >> iy;
         x.push_back(ix/CONV); 
         y.push_back(iy*CONV); 
      }
      infile.close(); 
      x.pop_back(); 
      y.pop_back(); 
   }



}

