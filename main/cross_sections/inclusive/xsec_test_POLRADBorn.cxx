Int_t xsec_test_POLRADBorn(){

   Double_t beamEnergy = 5.9; //GeV
   Double_t Eprime     = 1.0; 
   Double_t theta      = 40.*degree;
   Double_t phi        = 0.0*degree;  
   InSANENucleus::NucleusType Target = InSANENucleus::kProton; 
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 200;
   Double_t Emin     = 0.5;
   Double_t Emax     = 2.5;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;

   // born polarized + 
   InSANEPOLRADBornDiffXSec * fDiffXSec1 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetA(1);
   fDiffXSec1->SetZ(1);
   fDiffXSec1->GetPOLRAD()->SetHelicity(1);
   fDiffXSec1->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigma1 = new TF1("sigma", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(npx);
   sigma1->SetLineColor(kRed);

   // born polarized - 
   InSANEPOLRADBornDiffXSec * fDiffXSec2 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetA(1);
   fDiffXSec2->SetZ(1);
   fDiffXSec2->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec2->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigma2 = new TF1("sigma", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(kBlue);

   // --------------------------------
   
   TCanvas * c = new TCanvas("Polarized_Xsecs","Polarized DIS Xsecs");
   gPad->SetLogy(true);
   TString title =  fDiffXSec1->GetPlotTitle();
   TString yAxisTitle = fDiffXSec1->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();

   gr = new TGraph(sigma1->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(sigma2->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   gPad->Modified();

   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRADBorn.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRADBorn.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRADBorn.tex");
   return 0;
}
