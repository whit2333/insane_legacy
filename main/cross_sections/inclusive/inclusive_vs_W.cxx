Int_t inclusive_vs_W(){

   Double_t beamEnergy         = 5.5; //GeV
   Double_t phi                = 0.0*degree;
   Double_t Q2                 = 1.0;
   InSANENucleus targetNucleus = InSANENucleus::Proton();

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Wmin     = 0.5;//3.5;
   Double_t Wmax     = 3.0;//5.10;
   Int_t    npx      = 1000;


   // ------------------
   // 
   InSANEInclusiveBornDISXSec * xsec0 = new InSANEInclusiveBornDISXSec();
   xsec0->SetBeamEnergy(beamEnergy);
   xsec0->SetTargetNucleus(targetNucleus);
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();
   xsec0->UsePhaseSpace(false);
   xsec0->GetPhaseSpace()->Print();

   TF1 * sigma0 = new TF1("sigma0", xsec0, &InSANEInclusiveDiffXSec::WDependentXSec, 
                             Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma0->SetParameter(0,Q2);
   sigma0->SetParameter(1,phi);
   sigma0->SetLineColor(1);
   sigma0->SetNpx(npx);

   // ------------------
   // 
   InSANEeInclusiveElasticDiffXSec * fDiffXSec = new  InSANEeInclusiveElasticDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetNucleus(targetNucleus);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->UsePhaseSpace(false);

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigmaE->SetParameter(0,Q2);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetLineColor(kRed);
   sigmaE->SetNpx(npx);

   //leg->AddEntry(sigmaE,"Elastic cross section","l");
   fDiffXSec->GetFormFactors()->Dump();

   // --------------------

   TCanvas * c = new TCanvas();
   c->SetLogy(true);

   //TLegend * leg = new TLegend(0.17,0.17,0.6,0.5);
   //leg->SetFillColor(0);
   //leg->SetBorderSize(0);
   //TLatex  latex;

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   gr = new TGraph( sigma0->DrawCopy("goff")->GetHistogram() );
   mg->Add(gr,"l");

   gr = new TGraph( sigmaE->DrawCopy("goff")->GetHistogram() );
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->GetXaxis()->SetTitle("W (GeV)");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("");
   mg->GetYaxis()->CenterTitle(true);
   mg->Draw("a");

   // --------------------
   //latex.DrawLatex(2.2,700,Form("Q^{2}=%.1f",Q2));

   c->SaveAs("results/cross_sections/inclusive/inclusive_vs_W.png");

   return 0;
}
