Int_t electronPion_compare() {

   Double_t Emin   = 0.5;
   Double_t Emax   = 2.0;
   Double_t theta  = 35.0*degree;
   Double_t phi    =  0.0*degree;
   Int_t    npar   = 2;
   Int_t    Npx    = 30;
   Double_t pf     = 0.6; 
   Double_t Ebeam  = 5.9;
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget("UVaTarget","UVa Ammonia target",pf);
   InSANEFunctionManager::GetManager()->CreateSFs(1); // 1=CTEQ

   Int_t iTargMat = 0;
   InSANETargetMaterial * mat = targ->GetMaterial(iTargMat);
   InSANENucleus targNucleus(int(mat->fZ),int(mat->fA));

   InSANERadiator<InSANEInclusiveBornDISXSec> * xsec = new InSANERadiator<InSANEInclusiveBornDISXSec>();
   xsec->SetRadiationLength(mat->GetNumberOfRadiationLengths());
   //xsec->SetInternalOnly(true);// external is taken care of by GEANT4
   //InSANEInclusiveDISXSec * xsec = new InSANEInclusiveDISXSec();
   //xsec->SetTargetThickness(mat->GetNumberOfRadiationLengths());
   //xsec->Dump();
   //std::cout << "X/X0 = " << mat->GetNumberOfRadiationLengths() << std::endl;
   xsec->SetTargetMaterial(*mat);
   xsec->SetTargetMaterialIndex(iTargMat);
   xsec->SetBeamEnergy(Ebeam);
   xsec->SetTargetNucleus(targNucleus);
   xsec->InitializePhaseSpaceVariables();
   xsec->InitializeFinalStateParticles();
   xsec->GetPhaseSpace()->GetVariableWithName("energy")->SetMinimum(0.5);
   xsec->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(4.5);
   xsec->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(25.0*degree);
   //samp = new InSANEPhaseSpaceSampler(xsec);
   //samp->SetFoamCells(100);
   //samp->SetWeight(weight);
   //AddSampler(samp);

   PhotoOARPionDiffXSec * xsec1 = new PhotoOARPionDiffXSec();
   //xsec->Dump();
   xsec1->SetTargetMaterial(*mat);
   xsec1->SetTargetMaterialIndex(iTargMat);
   xsec1->SetBeamEnergy(Ebeam);
   xsec1->SetTargetNucleus(targNucleus);
   xsec1->InitializePhaseSpaceVariables();
   xsec1->InitializeFinalStateParticles();
   //samp = new InSANEPhaseSpaceSampler(xsec1);
   //samp->SetFoamCells(100);
   //samp->SetWeight(weight);
   //AddSampler(samp);

   ElectroOARPionDiffXSec *xsec2 = new ElectroOARPionDiffXSec();
   //OARPionElectroDiffXSec *xsec2 = new OARPionElectroDiffXSec();
   //xsec->Dump();
   xsec2->SetTargetMaterial(*mat);
   xsec2->SetTargetMaterialIndex(iTargMat);
   xsec2->SetBeamEnergy(Ebeam);
   xsec2->SetTargetNucleus(targNucleus);
   xsec2->InitializePhaseSpaceVariables();
   xsec2->InitializeFinalStateParticles();
   //samp = new InSANEPhaseSpaceSampler(xsec2);
   //samp->SetFoamCells(100);
   //samp->SetWeight(weight);
   //AddSampler(samp);

   // --------------------------------------------------------------------

   TF1 * sigma0 = new TF1("sigma0", xsec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                        Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma0->SetParameter(0,theta);
   sigma0->SetParameter(1,phi);
   sigma0->SetNpx(Npx);

   TF1 * sigma1 = new TF1("sigma1", xsec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                        Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(Npx);

   TF1 * sigma2 = new TF1("sigma2", xsec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
                        Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(Npx);

   // --------------------------------------------------------------------

   TCanvas * c        = new TCanvas("electronPion","electronPion");
   TLegend * leg      = new TLegend(0.7,0.7,0.9,0.9);
   gPad->SetLogy(true);
   TString title      = "Pion Electroproduction"; 
   TString yAxisTitle = " " ;// fDiffXSec->GetLabel();
   TString xAxisTitle = "E_{#pi} (GeV)"; 

   TMultiGraph  * mg = new TMultiGraph();
   TGraph       * gr = 0;

   sigma0->SetLineColor(1);
   gr = new TGraph(sigma0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"electron","l");

   sigma1->SetLineColor(2);
   gr = new TGraph(sigma1->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"photo pion","l");

   sigma2->SetLineColor(4);
   gr = new TGraph(sigma2->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"electro pion","l");

   mg->Draw("a");
   leg->Draw();

   c->SaveAs(Form("results/cross_sections/inclusive/electroPion_compare_%d.png",iTargMat));

   return 0;
}
