Int_t PionProduction_photo_compare(Double_t beamEnergy = 4.5, Double_t th_pi = 40.0){

   TString labelNote = "Alankian, et al, on ^{12}C";
   //TString labelNote = "Alankian, et al, on ^{12}C";

   Int_t pid = -211;
   //std::ifstream file("/home/whit/pion_data/pion/Positive/cs_5-pass.dat");
   //double x,y,ey;
   //std::vector<double> x_energy,y_sigma;
   //std::vector<double> ex_energy,ey_sigma;
   //while( !(file.eof()) ){
   //   file >> x >> y >> ey ;
   //   double p_pi = x/1000.0;
   //   double e_pi =  TMath::Sqrt(p_pi*p_pi + M_pion*M_pion/(GeV*GeV));
   //   x_energy.push_back(e_pi); 
   //   ex_energy.push_back(0.0); 
   //   // E/p is the jacobian dp/dE
   //   y_sigma.push_back((e_pi/p_pi)*y); 
   //   ey_sigma.push_back((e_pi/p_pi)*(ey+0.10*y)); 
   //   //y_sigma.push_back(TMath::Power(p_pi,3.0)*y); 
   //}
   //TGraphErrors * dataGr = new TGraphErrors(x_energy.size(),&x_energy[0], &y_sigma[0],&ex_energy[0], &ey_sigma[0]);
   //dataGr->SetMarkerStyle(20);
   //dataGr->SetMarkerSize(0.5);

   std::ifstream file("/home/whit/pion_data_alanakian/alanakian_piminus_40deg.dat");
   double x,y;
   std::vector<double> x_energy,y_sigma;
   while( !(file.eof()) ){
      file >> x >> y ;
      double e_tot = M_pion/GeV + x;
      double p_pi = TMath::Sqrt(e_tot*e_tot - M_pion*M_pion/(GeV*GeV));
      x_energy.push_back(e_tot); 
      // E/p is the jacobian dp/dE
      y_sigma.push_back((p_pi)*y); 
      //y_sigma.push_back(12.0*y); 
      //y_sigma.push_back(y/e_tot/e_tot/e_tot); 
      //y_sigma.push_back(TMath::Power(p_pi,3.0)*y); 
   }
   TGraph * dataGr = new TGraph(x_energy.size(),&x_energy[0], &y_sigma[0]);
   dataGr->SetMarkerStyle(20);


   // ---------------------------------------------

   //Double_t beamEnergy = 1.2;//5.9; //GeV
   Double_t theta_pi      = th_pi*degree;
   Double_t phi_pi        = 0.0*degree;

   // Target 
   InSANENucleus targetNucleus(6,12);
   Double_t wiser_rad_len = 0.05;

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.2;
   Double_t Emax     = beamEnergy-0.1;
   if(Emax>2.0) Emax = 1.9;
   Int_t    Npx      = 50;

   TLatex * latex = new TLatex();
   latex->SetNDC();
   //latex->SetTextAlign(21);
   TLegend * leg = new TLegend(0.1,0.1,0.3,0.3);
   leg->SetFillColor(0); 
   leg->SetBorderSize(0);
   leg->SetFillStyle(0); 
   //leg->SetHeader("MAID cross sections");
   TVector3 pt(0.0,0.0,0.0);

   //-------------------------------------------------------
   // MAID unpolarized
   //MAIDInclusivePi0DiffXSec *fDiffXSec = new MAIDInclusivePi0DiffXSec();
   //MAIDInclusivePionDiffXSec *fDiffXSec = new MAIDInclusivePionDiffXSec("n","piplus");
   //fDiffXSec->SetBeamEnergy(beamEnergy);
   //fDiffXSec->SetTargetNucleus(targetNucleus);
   ////fDiffXSec->fXSec0->SetBeamEnergy(beamEnergy);
   ////fDiffXSec->fXSec0->SetHelicity(0.0);
   ////fDiffXSec->fXSec0->SetTargetPolarization(pt);
   //std::cout << " init ps " << std::endl;
   //fDiffXSec->InitializePhaseSpaceVariables();
   //fDiffXSec->InitializeFinalStateParticles();
   //fDiffXSec->Refresh();

   //TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
   //      Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   //sigmaE->SetParameter(0,theta_pi);
   //sigmaE->SetParameter(1,phi_pi);
   //sigmaE->SetNpx(Npx);
   //sigmaE->SetLineColor(kRed);
   //leg->AddEntry(sigmaE,"MAID","l");

   //-------------------------------------------------------
   // OAR Pion 
   OARPionDiffXSec *fDiffXSec0 = new OARPionDiffXSec();
   fDiffXSec0->SetBeamEnergy(beamEnergy);
   fDiffXSec0->SetTargetNucleus(targetNucleus);
   fDiffXSec0->UsePhaseSpace(false);
   fDiffXSec0->InitializePhaseSpaceVariables();
   fDiffXSec0->InitializeFinalStateParticles();
   fDiffXSec0->Refresh();

   TF1 * sigmaE0 = new TF1("sigma0", fDiffXSec0, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE0->SetParameter(0,theta_pi);
   sigmaE0->SetParameter(1,phi_pi);
   sigmaE0->SetNpx(Npx);
   sigmaE0->SetLineColor(8);
   leg->AddEntry(sigmaE0,"OAR Pion","l");

   // OAR Pion 
   //OARPionDiffXSec *fDiffXSec1 = new OARPionDiffXSec();
   PhotoOARPionDiffXSec *fDiffXSec1 = new PhotoOARPionDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetRadiationLength(0.01);
   fDiffXSec1->SetTargetNucleus(targetNucleus);
   fDiffXSec1->UsePhaseSpace(false);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();

   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE1->SetParameter(0,theta_pi);
   sigmaE1->SetParameter(1,phi_pi);
   sigmaE1->SetNpx(Npx);
   sigmaE1->SetLineColor(6);
   leg->AddEntry(sigmaE1,"OAR Pion","l");

   //-------------------------------------------------------
   // EPCV 
   InSANEInclusiveEPCVXSec *fDiffXSec2 = new InSANEInclusiveEPCVXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetNucleus(targetNucleus);
   fDiffXSec2->SetProductionParticleType(pid);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta_pi);
   sigmaE2->SetParameter(1,phi_pi);
   sigmaE2->SetNpx(Npx);
   sigmaE2->SetLineColor(kBlack);
   //leg->AddEntry(sigmaE2,"EPCV","l");

   //-------------------------------------------------------
   // Wiser 
   InSANEInclusiveWiserXSec *fDiffXSec3 = new InSANEInclusiveWiserXSec();
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   fDiffXSec3->SetTargetNucleus(targetNucleus);
   fDiffXSec3->SetRadiationLength(wiser_rad_len);
   fDiffXSec3->SetProductionParticleType(pid);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   fDiffXSec3->GetPhaseSpace()->GetVariableWithName("energy")->SetMinimum(0.01);
   fDiffXSec3->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(0.0001);
   fDiffXSec3->Refresh();
   TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE3->SetParameter(0,theta_pi);
   sigmaE3->SetParameter(1,phi_pi);
   sigmaE3->SetNpx(Npx);
   sigmaE3->SetLineColor(kBlue);
   leg->AddEntry(sigmaE3,Form("Wiser - %.2f %% ",wiser_rad_len*100.0),"l");

   //-------------------------------------------------------
   // new Wiser
   //WiserInclusivePhotoXSec *fDiffXSec4 = new WiserInclusivePhotoXSec();
   PhotoWiserDiffXSec *fDiffXSec4 = new PhotoWiserDiffXSec();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetTargetNucleus(targetNucleus);
   fDiffXSec4->SetRadiationLength(wiser_rad_len);
   fDiffXSec4->SetProductionParticleType(pid);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   fDiffXSec4->GetPhaseSpace()->GetVariableWithName("energy")->SetMinimum(0.01);
   fDiffXSec4->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(0.0001);
   fDiffXSec4->Refresh();

   TF1 * sigma4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma4->SetParameter(0,theta_pi);
   sigma4->SetParameter(1,phi_pi);
   sigma4->SetNpx(Npx);
   sigma4->SetLineColor(kGreen);
   sigma4->SetLineWidth(1);
   leg->AddEntry(sigma4,"New PhotoWiser","l");

   TCanvas * c        = new TCanvas("EPCVInclusive","EPCV Inclusive");
   gPad->SetLogy(true);
   TString title      = "Pion Photoproduction"; 
   TString yAxisTitle = " " ;// fDiffXSec->GetLabel();
   TString xAxisTitle = "E_{#pi} (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;
   mg->Add(dataGr,"p");
 
   std::cout << "test" << std::endl;
   // MAID
   //gr = new TGraph(sigmaE->DrawCopy("goff")->GetHistogram());
   //mg->Add(gr,"l");

   // OARPion
   TH1 * hist = (TH1*)(sigmaE0->DrawCopy("goff")->GetHistogram());
   hist->Scale(12.0);
   gr = new TGraph(hist);
   mg->Add(gr,"l");
   gr = new TGraph(sigmaE1->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   // EPC
   gr = new TGraph(sigmaE2->DrawCopy("goff")->GetHistogram());
   //for( int j = gr->GetN()-1 ;j>=0; j--) {
   //   Double_t xt, yt;
   //   gr->GetPoint(j,xt,yt);
   //   Double_t kmin = InSANE::Kine::k_min_photoproduction(xt,theta_pi); 
   //   //Double_t Q_equiv = InSANE::Kine::Q_equiv_quanta(kmin,beamEnergy);
   //   Double_t n_gamma = InSANE::Kine::N_gamma(1.0,kmin,beamEnergy);
   //   gr->SetPoint(j,xt,yt*n_gamma);
   //}
   mg->Add(gr,"l");

   // WISER
   gr = new TGraph(sigmaE3->DrawCopy("goff")->GetHistogram());
   //for( int j = gr->GetN()-1 ;j>=0; j--) {
   //   Double_t xt, yt;
   //   gr->GetPoint(j,xt,yt);
   //   Double_t kmin = InSANE::Kine::k_min_photoproduction(xt,theta_pi); 
   //   Double_t Q_equiv = InSANE::Kine::Q_equiv_quanta(kmin,beamEnergy);
   //   Double_t n_gamma = InSANE::Kine::N_gamma(1.0,kmin,beamEnergy);
   //   gr->SetPoint(j,xt,yt/Q_equiv);
   //}
   mg->Add(gr,"l");

   // New wiser
   gr = new TGraph(sigma4->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");


   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);
   mg->GetYaxis()->SetRangeUser(0.1,5.0e4);


   gPad->Modified();

   latex->DrawLatex(0.3,0.3,Form("#theta_{#pi}=%.0f",theta_pi/degree));
   latex->DrawLatex(0.3,0.25,Form("E=%.2fGeV",beamEnergy));
   //latex->DrawLatex(0.3,0.20,Form("(Z,A) = (%d,%d)",targetNucleus.GetZ(),targetNucleus.GetA()));
   latex->DrawLatex(0.3,0.20,labelNote.Data());
   leg->Draw();

   c->Update();
   c->SaveAs(Form("results/cross_sections/inclusive/PionProduction_photo_compare_%d_%d_%d.png",int(beamEnergy*10.0),int(th_pi),pid));
   c->SaveAs(Form("results/cross_sections/inclusive/PionProduction_photo_compare_%d_%d_%d.pdf",int(beamEnergy*10.0),int(th_pi),pid));
   c->SaveAs(Form("results/cross_sections/inclusive/PionProduction_photo_compare_%d_%d_%d.tex",int(beamEnergy*10.0),int(th_pi),pid));

   return 0;
}
