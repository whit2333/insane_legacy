Int_t DIS_vs_W(){

   Double_t beamEnergy = 49; //GeV
   Double_t Eprime     = 1.01; 
   Double_t theta      = 35.0*degree;//12.9.*degree;
   Double_t phi        = 0.0*degree;  
   Double_t Q2         = 2.0; 


   InSANENucleus targetNucleus = InSANENucleus::Proton();

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Wmin     = 0.5;//3.5;
   Double_t Wmax     = 4.0;//5.10;


   // ------------------
   // 
   InSANEInclusiveBornDISXSec * xsec0 = new InSANEInclusiveBornDISXSec();
   xsec0->SetBeamEnergy(beamEnergy);
   xsec0->SetTargetNucleus(targetNucleus);
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();
   xsec0->UsePhaseSpace(false);
   xsec0->GetPhaseSpace()->Print();

   TF1 * sigma0 = new TF1("sigma0", xsec0, &InSANEInclusiveDiffXSec::WDependentXSec, 
                             Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma0->SetParameter(0,Q2);
   sigma0->SetParameter(1,phi);
   sigma0->SetLineColor(1);

   // --------------------

   TCanvas * c = new TCanvas();

   //TLegend * leg = new TLegend(0.17,0.17,0.6,0.5);
   //leg->SetFillColor(0);
   //leg->SetBorderSize(0);
   //TLatex  latex;

   sigma0->Draw();
   //sigma0->SetTitle(title);
   sigma0->GetXaxis()->SetTitle("W (GeV)");
   sigma0->GetXaxis()->CenterTitle(true);
   sigma0->GetYaxis()->SetTitle("");
   sigma0->GetYaxis()->CenterTitle(true);
   sigma0->DrawCopy();
   //leg->AddEntry(sigma0,"Born","l");
   //leg->AddEntry(sigma1,"^{12}C radiated","l");
   //leg->AddEntry(sigma1_d,"^{2}H radiated","l");
   //leg->Draw();

   // --------------------

   //latex.DrawLatex(2.2,700,Form("Q^{2}=%.1f",Q2));

   c->SaveAs("results/cross_sections/inclusive/DIS_vs_W.png");

   return 0;
}
