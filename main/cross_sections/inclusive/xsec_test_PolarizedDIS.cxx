Int_t xsec_test_PolarizedDIS(){

   Double_t beamEnergy = 5.9; //GeV
   Double_t Eprime     = 1.0; 
   Double_t theta      = 40.*degree;
   Double_t phi        = 0.0*degree;  
   InSANENucleus::NucleusType Target = InSANENucleus::kProton; 
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   
   Double_t theta_target = 180*degree;

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 200;
   Double_t Emin     = 0.5;
   Double_t Emax     = 3.9;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;


   // born polarized + 
   PolarizedDISXSecAntiParallelHelicity * fDiffXSec1 = new  PolarizedDISXSecAntiParallelHelicity();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetA(1);
   fDiffXSec1->SetZ(1);
   fDiffXSec1->SetTargetPolarizationAngle(theta_target);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigma1 = new TF1("sigma", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(npx);
   sigma1->SetLineColor(kRed);

   // born polarized - 
   PolarizedDISXSecParallelHelicity * fDiffXSec2 = new  PolarizedDISXSecParallelHelicity();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetA(1);
   fDiffXSec2->SetZ(1);
   fDiffXSec2->SetTargetPolarizationAngle(theta_target);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigma2 = new TF1("sigma", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(kBlack);

   // --------------------------------
   
   TCanvas * c = new TCanvas("Polarized_Xsecs","Polarized DIS Xsecs");
   gPad->SetLogy(true);
   TString title =  fDiffXSec1->GetPlotTitle();
   TString yAxisTitle = fDiffXSec1->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();

   gr = new TGraph(sigma1->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   gr = new TGraph(sigma2->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");
   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   gPad->Modified();

   c->SaveAs("results/cross_sections/inclusive/xsec_test_PolarizedDIS.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_PolarizedDIS.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_PolarizedDIS.tex");
   return 0;
}
