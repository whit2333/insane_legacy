/*! Inclusive elastic scattering cross section.
 *  There are two independent variables for the inclusive cross section.
 *  In addition to phi, the other is either theta or eprime. 
 *  The class InSANEeInclusiveElasticDiffXSec uses theta and phi, however,
 *  plotting as a function of Eprime and theta is possible.
 */ 
Int_t xsec_test_Elastic(){

   Double_t beamEnergy = 5.9; //GeV
   //Double_t Eprime     = 0.1; 
   //Double_t theta      = 10.*degree;
   Double_t phi        = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   

   // For plotting cross sections 
   Int_t    npar     = 1;
   Double_t Emin     = 0.1;
   Double_t Emax     = 5.89;
   Double_t minTheta = 5.0*degree;
   Double_t maxTheta = 40.0*degree;

   InSANEeInclusiveElasticDiffXSec * fDiffXSec = new  InSANEeInclusiveElasticDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   //fDiffXSec->SetA(A);
   //fDiffXSec->SetZ(Z);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   //TF1 * sigma = new TF1("sigma", fDiffXSec, &InSANEeInclusiveElasticDiffXSec::PolarAngleDependentXSec, 
   //      Emin, Emax, npar,"InSANEeInclusiveElasticDiffXSec","PolarAngleDependentXSec");
   TF1 * sigma = new TF1("sigma", fDiffXSec, &InSANEeInclusiveElasticDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEeInclusiveElasticDiffXSec","PolarAngleDependentXSec");
   sigma->SetParameter(0,phi);
   sigma->SetNpx(10);

   //----------------
   TCanvas * c = new TCanvas("xsec_test_Elastic","Elastic xsec");
   gPad->SetLogy(true);
   TString title =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "#theta (rad)"; 

   //sigma->SetLineColor(kRed);
   sigma->Draw();
   sigma->SetTitle(title);
   sigma->GetXaxis()->SetTitle(xAxisTitle);
   sigma->GetXaxis()->CenterTitle(true);
   sigma->GetYaxis()->SetTitle(yAxisTitle);
   sigma->GetYaxis()->CenterTitle(true);
   sigma->Draw();

   return 0;
}
