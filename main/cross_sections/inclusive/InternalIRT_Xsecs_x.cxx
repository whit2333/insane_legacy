Int_t InternalIRT_Xsecs_x(Double_t theta_target = 80*degree){

   gStyle->SetPadGridX(true);
   gStyle->SetPadGridY(true);
   //InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   //fman->CreateSFs(6);
   //fman->CreatePolSFs(6);

   Double_t beamEnergy =  5.89; //GeV
   Double_t Eprime     =  1.1; 
   Double_t theta      = 40.0*degree;
   Double_t Q2         =  5.5;//4.1;//3.0;//2.2;
   Double_t phi        =  0.0*degree;  
   Int_t TargetType    =  0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  

   Int_t    npx      = 10;
   Int_t    npar     = 2;
   Double_t xmin     = 0.5;//0.40;//0.30;//0.24;
   Double_t xmax     = 0.9;//0.75;//0.48;//0.31;

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->SetFillColor(0); 
   leg->SetFillStyle(0); 
   leg->SetHeader("Inelastic radative tails");

   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);

   //-----------------------------------
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->GetPOLRAD()->SetHelicity(0);
   fDiffXSec->SetTargetType(InSANENucleus::kProton);
   fDiffXSec->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);

   fDiffXSec->GetPOLRAD()->fIRT_tau_RelErr = 0.0001;
   fDiffXSec->GetPOLRAD()->fIRT_tau_AbsErr = 0.01;
   fDiffXSec->GetPOLRAD()->fIRT_tau_nCalls = 10000;

   fDiffXSec->GetPOLRAD()->fIRT_R_RelErr = 0.00001;
   fDiffXSec->GetPOLRAD()->fIRT_R_AbsErr = 0.00001;
   fDiffXSec->GetPOLRAD()->fIRT_R_nCalls = 1000;

   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::xDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","xDependentXSec");
   sigmaE->SetParameter(0,Q2);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetNpx(npx);
   leg->AddEntry(sigmaE,"POLRAD IRT + ","l");
   //sigmaE->SetLineColor(kBlack);


   //-----------------------------------
   InSANEPOLRADInelasticTailDiffXSec * fDiffXSec1 = new  InSANEPOLRADInelasticTailDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->GetPOLRAD()->SetHelicity(0);
   fDiffXSec1->SetTargetType(InSANENucleus::kProton);
   fDiffXSec1->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec1->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);

   fDiffXSec1->GetPOLRAD()->fIRT_tau_RelErr = 0.0001;
   fDiffXSec1->GetPOLRAD()->fIRT_tau_AbsErr = 0.01;
   fDiffXSec1->GetPOLRAD()->fIRT_tau_nCalls = 10000;

   fDiffXSec1->GetPOLRAD()->fIRT_R_RelErr = 0.00001;
   fDiffXSec1->GetPOLRAD()->fIRT_R_AbsErr = 0.00001;
   fDiffXSec1->GetPOLRAD()->fIRT_R_nCalls = 1000;

   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::xDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","xDependentXSec");
   sigmaE1->SetParameter(0,Q2);
   sigmaE1->SetParameter(1,phi);
   sigmaE1->SetNpx(npx);
   leg->AddEntry(sigmaE1,"POLRAD IRT -","l");
   //sigmaE->SetLineColor(kBlack);

   //-----------------------------------
   // Equiv Radiator
   InSANEInelasticRadiativeTail * fDiffXSec2 = new  InSANEInelasticRadiativeTail();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetType(InSANENucleus::kProton);
   fDiffXSec2->GetPOLRADIRT()->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetRegion4(false);
   fDiffXSec2->GetPOLRADIRT()->SetTargetType(InSANENucleus::kProton);
   fDiffXSec2->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec2->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::xDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","xDependentXSec");
   sigmaE2->SetParameter(0,Q2);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetNpx(npx);
   sigmaE2->SetLineColor(kRed);
   leg->AddEntry(sigmaE2,"Equivalent radiator + ","l");
   //-----------------------------------
   // Equiv Radiator
   InSANEInelasticRadiativeTail * fDiffXSec3 = new  InSANEInelasticRadiativeTail();
   fDiffXSec3->SetBeamEnergy(beamEnergy);
   fDiffXSec3->SetTargetType(InSANENucleus::kProton);
   fDiffXSec3->GetPOLRADIRT()->SetBeamEnergy(beamEnergy);
   fDiffXSec3->SetRegion4(false);
   fDiffXSec3->GetPOLRADIRT()->SetTargetType(InSANENucleus::kProton);
   fDiffXSec3->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec3->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   TF1 * sigmaE3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::xDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","xDependentXSec");
   sigmaE3->SetParameter(0,Q2);
   sigmaE3->SetParameter(1,phi);
   sigmaE3->SetNpx(npx);
   sigmaE3->SetLineColor(kRed);
   leg->AddEntry(sigmaE3,"Equivalent radiator - ","l");
   //-----------------------------------
   // Equiv Radiator
   InSANEInelasticRadiativeTail * fDiffXSec4 = new  InSANEInelasticRadiativeTail();
   fDiffXSec4->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetTargetType(InSANENucleus::kProton);
   fDiffXSec4->GetPOLRADIRT()->SetBeamEnergy(beamEnergy);
   fDiffXSec4->SetRegion4(true);
   fDiffXSec4->GetPOLRADIRT()->SetTargetType(InSANENucleus::kProton);
   fDiffXSec4->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec4->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   TF1 * sigmaE4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::xDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","xDependentXSec");
   sigmaE4->SetParameter(0,Q2);
   sigmaE4->SetParameter(1,phi);
   sigmaE4->SetNpx(npx);
   sigmaE4->SetLineColor(kBlue);
   leg->AddEntry(sigmaE4,"Equiv. rad (plus region IV) + ","l");
   //-----------------------------------
   // Equiv Radiator
   InSANEInelasticRadiativeTail * fDiffXSec5 = new  InSANEInelasticRadiativeTail();
   fDiffXSec5->SetBeamEnergy(beamEnergy);
   fDiffXSec5->SetTargetType(InSANENucleus::kProton);
   fDiffXSec5->GetPOLRADIRT()->SetBeamEnergy(beamEnergy);
   fDiffXSec5->SetRegion4(true);
   fDiffXSec5->GetPOLRADIRT()->SetTargetType(InSANENucleus::kProton);
   fDiffXSec5->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec5->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec5->InitializePhaseSpaceVariables();
   fDiffXSec5->InitializeFinalStateParticles();
   TF1 * sigmaE5 = new TF1("sigma5", fDiffXSec5, &InSANEInclusiveDiffXSec::xDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","xDependentXSec");
   sigmaE5->SetParameter(0,Q2);
   sigmaE5->SetParameter(1,phi);
   sigmaE5->SetNpx(npx);
   sigmaE5->SetLineColor(kBlue);
   leg->AddEntry(sigmaE5,"Equiv. rad (plus region IV) - ","l");

   //-----------------------------------
   // born polarized + 
   InSANEPOLRADBornDiffXSec * fDiffXSecb1 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSecb1->SetBeamEnergy(beamEnergy);
   fDiffXSecb1->SetTargetType(InSANENucleus::kProton);
   fDiffXSecb1->GetPOLRAD()->SetHelicity(1);
   fDiffXSecb1->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSecb1->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSecb1->InitializePhaseSpaceVariables();
   fDiffXSecb1->InitializeFinalStateParticles();
   fDiffXSecb1->Refresh();
   TF1 * sigmab1 = new TF1("sigmab1", fDiffXSecb1, &InSANEInclusiveDiffXSec::xDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","xDependentXSec");
   sigmab1->SetParameter(0,Q2);
   sigmab1->SetParameter(1,phi);
   sigmab1->SetNpx(npx);
   sigmab1->SetLineColor(kBlack);
   sigmab1->SetLineStyle(2);
   leg->AddEntry(sigmab1,"born +","l");

   // born polarized - 
   InSANEPOLRADBornDiffXSec * fDiffXSecb2 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSecb2->SetBeamEnergy(beamEnergy);
   fDiffXSecb2->SetTargetType(InSANENucleus::kProton);
   fDiffXSecb2->GetPOLRAD()->SetHelicity(-1);
   fDiffXSecb2->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSecb2->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSecb2->InitializePhaseSpaceVariables();
   fDiffXSecb2->InitializeFinalStateParticles();
   fDiffXSecb2->Refresh();
   TF1 * sigmab2 = new TF1("sigmab2", fDiffXSecb2, &InSANEInclusiveDiffXSec::xDependentXSec, 
         xmin, xmax, npar,"InSANEInclusiveDiffXSec","xDependentXSec");
   sigmab2->SetParameter(0,Q2);
   sigmab2->SetParameter(1,phi);
   sigmab2->SetNpx(npx);
   sigmab2->SetLineColor(kBlack);
   sigmab2->SetLineStyle(2);
   leg->AddEntry(sigmab2,"born - ","l");

   //-----------------------
   TCanvas * c = new TCanvas("xsec_test_POLRAD_IRT","xsec_test_POLRAD_IRT");
   //gPad->SetLogy(true);
   TString title =  "Internal Inelastic Radiative Tail";//fDiffXSec->GetPlotTitle();
   TString yAxisTitle = "#frac{d#sigma}{dE'd#Omega} (nb/GeV/sr)";
   TString xAxisTitle = "x"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;
   TGraph * gra1 = 0;
   TGraph * gra2 = 0;
   TGraph * grb1 = 0;
   TGraph * grb2 = 0;
   TGraph * grc1 = 0;
   TGraph * grc2 = 0;

   //gr = new TGraph(sigmaE->DrawCopy("same"));
   //gra1 = gr;
   //mg->Add(gr,"l");
   //gr = new TGraph(sigmaE1->DrawCopy("same"));
   //gra2 = gr;
   //mg->Add(gr,"l");

   std::cout << " without region IV " << std::endl;
   gr = new TGraph(sigmaE2->DrawCopy("same"));
   grb1 = gr;
   mg->Add(gr,"l");
   std::cout << " without region IV " << std::endl;
   gr = new TGraph(sigmaE3->DrawCopy("same"));
   grb2 = gr;
   mg->Add(gr,"l");

   gr = new TGraph(sigmab1->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigmab2->DrawCopy("same"));
   mg->Add(gr,"l");

   //std::cout << " with region IV " << std::endl;
   //gr = new TGraph(sigmaE4->DrawCopy("same"));
   //grc1 = gr;
   //mg->Add(gr,"l");
   //std::cout << " with region IV " << std::endl;
   //gr = new TGraph(sigmaE5->DrawCopy("same"));
   //grc2 = gr;
   //mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   leg->Draw();
   latex->DrawLatex(0.2,0.38,Form("#theta_{target}=%.0f",theta_target/degree));
   latex->DrawLatex(0.2,0.3,Form("Q^{2}=%.0f",Q2));
   latex->DrawLatex(0.2,0.22,Form("E=%.2fGeV",beamEnergy));

   gPad->Modified();

   c->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_Xsecs_x_59_%d-%d-%d.png",int(theta_target/degree),int(beamEnergy),int(Q2)));
   c->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_Xsecs_x_59_%d-%d-%d.pdf",int(theta_target/degree),int(beamEnergy),int(Q2)));
   c->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_Xsecs_x_59_%d-%d-%d.tex",int(theta_target/degree),int(beamEnergy),int(Q2)));

   TCanvas * c2 = new TCanvas("IRT","IRT");
   TMultiGraph * mg2 = new TMultiGraph();
   TGraph * gra = new TGraph(grb1->GetN());
   TGraph * grb = new TGraph(grb1->GetN());
   TGraph * grc = new TGraph(grb1->GetN());
   for(int i = 0; i<grb1->GetN(); i++){
      Double_t x1,x2,y1,y2,A;

      //gra1->GetPoint(i,x1,y1);
      //gra2->GetPoint(i,x2,y2);
      //A = (y1-y2)/(y1+y2);
      //if(y1==y2 || TMath::Abs(A)>1.0) A = 0.0;
      //gra->SetPoint(i,x1,A);

      grb1->GetPoint(i,x1,y1);
      grb2->GetPoint(i,x2,y2);
      A = (y1-y2)/(y1+y2);
      if(y1==y2 || TMath::Abs(A)>1.0) A = 0.0;
      grb->SetPoint(i,x1,A);
      
      //grc1->GetPoint(i,x1,y1);
      //grc2->GetPoint(i,x2,y2);
      //A = (y1-y2)/(y1+y2);
      //if(y1==y2 || TMath::Abs(A)>1.0) A = 0.0;
      //grc->SetPoint(i,x1,A);
   }
   gra->SetLineColor(kBlack);
   grb->SetLineColor(kRed);
   grc->SetLineColor(kBlue);
   mg2->Add(gra,"l");
   mg2->Add(grc,"l");
   mg2->Add(grb,"l");

   mg2->Draw("a");
   mg2->GetXaxis()->SetTitle(xAxisTitle);
   mg2->GetXaxis()->CenterTitle(true);
   mg2->GetXaxis()->SetLimits(0.0,1.0);
   mg2->GetYaxis()->SetRangeUser(-0.5,0.5);

   latex->DrawLatex(0.2,0.38,Form("#theta_{target}=%.0f",theta_target/degree));
   latex->DrawLatex(0.2,0.3,Form("#theta_{e}=%.0f",theta/degree));
   latex->DrawLatex(0.2,0.22,Form("E=%.2fGeV",beamEnergy));

   c2->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_Xsecs_asym_x_59_%d-%d-%d.png",int(theta_target/degree),int(beamEnergy),int(Q2)));
   c2->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_Xsecs_asym_x_59_%d-%d-%d.pdf",int(theta_target/degree),int(beamEnergy),int(Q2)));
   c2->SaveAs(Form("results/cross_sections/inclusive/InternalIRT_Xsecs_asym_x_59_%d-%d-%d.tex",int(theta_target/degree),int(beamEnergy),int(Q2)));
   
   return 0;
   TFile * outfile = new TFile("results/cross_sections/inclusive/InternalIRT_XSecs_x.root","UPDATE");
   outfile->cd();
   grb->Write(Form("asym_x_59_%d_%d",int(theta_target/degree),int(Q2)),TObject::kSingleKey);
   outfile->Flush();

   return 0;
}
