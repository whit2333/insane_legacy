Int_t xsec_test_Radiator(Double_t theta_target = 180*degree){

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();

   Double_t fBeamEnergy = 5.9;
   Double_t Emin        = 0.5;
   Double_t Emax        = 2.5;
   Double_t theta       = 30.0*degree;
   Double_t phi         = 0.0;
   Int_t    npar        = 2;
   Int_t    npx         = 50;
   Int_t    nMC         = 1e3;
   Double_t RadLen[2]; 
   InSANENucleus Target = InSANENucleus::Proton(); 

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   TLegend * leg = new TLegend(0.7,0.6,0.85,0.8);

   //-----------------------------------
   // CTEQ6 
   CTEQ6eInclusiveDiffXSec * fDiffXSec2 = new  CTEQ6eInclusiveDiffXSec();
   fDiffXSec2->SetBeamEnergy(fBeamEnergy);
   fDiffXSec2->SetTargetNucleus(Target);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigma2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(kBlue);
   sigma2->SetLineStyle(2);
   leg->AddEntry(sigma2,"CTEQ6","l");

   //-----------------------------------
   // Radiated CTEQ6 
   InSANERadiator<CTEQ6eInclusiveDiffXSec> * fDiffXSec1 = new  InSANERadiator<CTEQ6eInclusiveDiffXSec>();
   fDiffXSec1->SetBeamEnergy(fBeamEnergy);
   fDiffXSec1->SetTargetNucleus(Target);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigma1 = new TF1("sigma", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(npx);
   sigma1->SetLineColor(kBlue);
   leg->AddEntry(sigma1,"Radiated-CTEQ6","l");

   //-----------------------------------
   // default : set to statistical 
   fman->CreateSFs(6);
   fman->CreatePolSFs(6);
   //InSANEInelasticRadiativeTail2 * fDiffXSec3 = new  InSANEInelasticRadiativeTail2();
   InSANEInclusiveDiffXSec * fDiffXSec3 = new  InSANEInclusiveDiffXSec();
   fDiffXSec3->SetBeamEnergy(fBeamEnergy);
   fDiffXSec3->SetTargetNucleus(Target);
   //fDiffXSec3->SetRegion4(true);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   TF1 * sigma3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma3->SetParameter(0,theta);
   sigma3->SetParameter(1,phi);
   sigma3->SetNpx(npx);
   sigma3->SetLineColor(1);
   sigma3->SetLineStyle(2);
   leg->AddEntry(sigma3,"non-template","l");

   //-----------------------------------
   // Radiated default
   //InSANERadiator<F1F209eInclusiveDiffXSec> * fDiffXSec4 = new  InSANERadiator<F1F209eInclusiveDiffXSec>();
   InSANERadiator<InSANEInclusiveDiffXSec> * fDiffXSec4 = new  InSANERadiator<InSANEInclusiveDiffXSec>();
   fDiffXSec4->SetBeamEnergy(fBeamEnergy);
   fDiffXSec4->SetTargetNucleus(Target);
   //fDiffXSec4->SetRegion4(true);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   TF1 * sigma4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma4->SetParameter(0,theta);
   sigma4->SetParameter(1,phi);
   sigma4->SetNpx(npx);
   sigma4->SetLineColor(1);
   leg->AddEntry(sigma4,"Radiated-Inclusive","l");

   //-----------------------------------
   // F1F209 
   F1F209eInclusiveDiffXSec * fDiffXSec5 = new  F1F209eInclusiveDiffXSec();
   fDiffXSec5->SetBeamEnergy(fBeamEnergy);
   fDiffXSec5->SetTargetNucleus(Target);
   fDiffXSec5->InitializePhaseSpaceVariables();
   fDiffXSec5->InitializeFinalStateParticles();
   TF1 * sigma5 = new TF1("sigma5", fDiffXSec5, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma5->SetParameter(0,theta);
   sigma5->SetParameter(1,phi);
   sigma5->SetNpx(npx);
   sigma5->SetLineColor(kRed);
   sigma5->SetLineStyle(2);
   leg->AddEntry(sigma5,"non-template","l");

   //-----------------------------------
   // Radiated F1F209
   InSANERadiator<F1F209eInclusiveDiffXSec> * fDiffXSec6 = new  InSANERadiator<F1F209eInclusiveDiffXSec>();
   fDiffXSec6->SetBeamEnergy(fBeamEnergy);
   fDiffXSec6->SetTargetNucleus(Target);
   //fDiffXSec6->SetRegion4(true);
   fDiffXSec6->InitializePhaseSpaceVariables();
   fDiffXSec6->InitializeFinalStateParticles();
   TF1 * sigma6 = new TF1("sigma6", fDiffXSec6, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma6->SetParameter(0,theta);
   sigma6->SetParameter(1,phi);
   sigma6->SetNpx(npx);
   sigma6->SetLineColor(kRed);
   leg->AddEntry(sigma6,"Radiated-Inclusive","l");


   //-----------------------------------
   TCanvas * c = new TCanvas("rc_test1","rc_test1");
   gPad->SetLogy(true);
   TString title =  "Internal Inelastic Radiative Tail";//fDiffXSec->GetPlotTitle();
   TString yAxisTitle = "#frac{d#sigma}{dE'd#Omega} (nb/GeV/sr)";
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   gr = new TGraph(sigma1->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma2->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma3->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma4->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma5->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma6->DrawCopy("same"));
   mg->Add(gr,"l");

   //sigma1->Draw();
   //sigma2->Draw("same");
   //sigma3->Draw("same");
   //sigma4->Draw("same");

   mg->Draw("a");
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->Draw();
   //TPaveText * kine = new TPaveText(0.15,0.2,0.3,0.275,"NDC");
   //kine->AddText(Form("E = %1.1f GeV, #theta = %1.1f deg",fBeamEnergy,theta/degree));
   //kine->Draw("");

   TLatex Tl;
   Tl.SetNDC();
   Tl.SetTextSize(0.03);
   Tl.SetTextAlign(21);
   Tl.DrawLatex(0.5,0.8, Form("E = %1.1f GeV, #theta = %1.1f deg",fBeamEnergy,theta/degree));
   //latex->DrawLatex(0.1,0.1,Form("E = %1.1f, #theta = %1.1f",fBeamEnergy,theta/degree));

   c->SaveAs("results/cross_sections/inclusive/xsec_test_Radiator.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_Radiator.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_Radiator.tex");
   return(0);
}
