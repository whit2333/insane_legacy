Int_t xsec_test_WiserPhotoProduction(){

   Double_t beamEnergy = 4.7; //GeV
   Double_t theta_pi   = 45.0*degree;
   Double_t phi_pi     = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.2;
   Double_t Emax     = 2.5;
   Int_t    Npx      = 200;

   TLegend * leg = new TLegend(0.1,0.7,0.3,0.9);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("MAID cross sections");

   //-------------------------------------------------------
   TString pion               = "pi0";
   TString nucleon            = "p";
   TString target             = "p";
   WiserInclusivePhotoXSec *fDiffXSec = new WiserInclusivePhotoXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->SetProductionParticleType(-211);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta_pi);
   sigmaE->SetParameter(1,phi_pi);
   sigmaE->SetNpx(Npx);
   sigmaE->SetLineColor(kRed);
   leg->AddEntry(sigmaE,"unpolarized","l");

   //-------------------------------------------------------

   TCanvas * c        = new TCanvas("WISERInclusive","WISER Inclusive");
   gPad->SetLogy(true);
   TString title      = fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

 
   sigmaE->Draw();
   //sigmaE->SetTitle(title);
   //sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   //sigmaE->GetXaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   //sigmaE->GetYaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetRangeUser(0.0,1.1*sigmaE->GetMaximum());
   //F
   //sigmaE->SetMinimum(0.0);
   //sigmaE->Draw();
   //c->Update();
   //sigmaE1->DrawCopy("same");
   //sigmaE2->DrawCopy("same");
   //leg->Draw();

   c->SaveAs("results/cross_sections/inclusive/xsec_test_WiserPhotoProduction.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_WiserPhotoProduction.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_WiserPhotoProduction.tex");

   return 0;
}
