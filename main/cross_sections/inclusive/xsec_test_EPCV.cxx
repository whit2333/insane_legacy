Int_t xsec_test_EPCV(){

   Double_t beamEnergy = 5.9; //GeV
   Double_t theta_pi   = 5.0*degree;
   Double_t phi_pi     = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  
   Double_t A          = 1;   
   Double_t Z          = 1;   

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 1.0;
   Double_t Emax     = 2.5;
   Int_t    Npx      = 200;

   TLegend * leg = new TLegend(0.1,0.7,0.3,0.9);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("MAID cross sections");

   //-------------------------------------------------------
   // MAID unpolarized
   TString pion               = "pi0";
   TString nucleon            = "p";
   TString target             = "p";
   InSANEInclusiveEPCVXSec *fDiffXSec = new InSANEInclusiveEPCVXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetA(A);
   fDiffXSec->SetZ(Z);
   fDiffXSec->SetTargetType(TargetType);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta_pi);
   sigmaE->SetParameter(1,phi_pi);
   sigmaE->SetNpx(Npx);
   sigmaE->SetLineColor(kRed);
   leg->AddEntry(sigmaE,"unpolarized","l");

   //-------------------------------------------------------

   TCanvas * c        = new TCanvas("EPCVInclusive","EPCV Inclusive");
   gPad->SetLogy(true);
   TString title      = fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

 
   sigmaE->Draw();
   //sigmaE->SetTitle(title);
   //sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   //sigmaE->GetXaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   //sigmaE->GetYaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetRangeUser(0.0,1.1*sigmaE->GetMaximum());
   //F
   //sigmaE->SetMinimum(0.0);
   //sigmaE->Draw();
   //c->Update();
   //sigmaE1->DrawCopy("same");
   //sigmaE2->DrawCopy("same");
   //leg->Draw();

   c->SaveAs("results/cross_sections/inclusive/xsec_test_EPCV.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_EPCV.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_EPCV.tex");

   return 0;
}
