Int_t xsec_test_F1F209(){

   Double_t beamEnergy = 11.0;//49; //GeV
   Double_t Eprime     = 1.41; 
   Double_t theta      = 25.0*degree;//12.9.*degree;
   Double_t phi        = 0.0*degree;  
   InSANENucleus  targetNucleus = InSANENucleus::Proton();//(6,12);

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.5;//3.5;
   Double_t Emax     = 10.0;//5.10;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;

   F1F209eInclusiveDiffXSec * fDiffXSec = new  F1F209eInclusiveDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetNucleus(targetNucleus);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->GetPhaseSpace()->Print();
   fDiffXSec->UsePhaseSpace(false);

   TString title =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetLineColor(kRed);

   TCanvas * c = new TCanvas();
   //gPad->SetLogy(true);

   sigmaE->Draw();
   sigmaE->SetTitle(title);
   sigmaE->GetXaxis()->SetTitle(xAxisTitle);
   sigmaE->GetXaxis()->CenterTitle(true);
   sigmaE->GetYaxis()->SetTitle(yAxisTitle);
   sigmaE->GetYaxis()->CenterTitle(true);
   sigmaE->Draw();

   c->SaveAs("results/cross_sections/inclusive/xsec_test_F1F209.png");

   return 0;
}
