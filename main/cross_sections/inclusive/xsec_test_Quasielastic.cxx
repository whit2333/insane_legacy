/*! 
 */ 
Int_t xsec_test_Quasielastic(Int_t aNumber = 43 ){

   Double_t beamEnergy = 4.7; //GeV
   Double_t Eprime     = 1.5; 
   Double_t theta      = 40.*degree;
   Double_t phi        = 0.0*degree;  
   Double_t Z          = 1;   
   Double_t Q2         = 4.0*beamEnergy*Eprime*TMath::Power(TMath::Sin(theta/2.0),2.0);

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 100;
   Double_t Emin     = 0.25;
   Double_t Emax     = 3.0;//beamEnergy;
   Double_t minTheta = 20*degree;
   Double_t maxTheta = 40*degree;
   Double_t Wmin     = 0.5;
   Double_t Wmax     = beamEnergy;
   InSANENucleus targ(7,14);

   // -----------------------------------------------------------------------------------
   F1F209QuasiElasticDiffXSec * xsec_born = new F1F209QuasiElasticDiffXSec();
   xsec_born->SetTargetNucleus(targ);
   xsec_born->SetBeamEnergy(beamEnergy);
   xsec_born->InitializePhaseSpaceVariables();
   xsec_born->InitializeFinalStateParticles();
   xsec_born->UsePhaseSpace(false);

   TF1 * sigmaE_born = new TF1("sigmaEborn", xsec_born, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_born->SetLineColor(2);
   sigmaE_born->SetParameter(0,theta);
   sigmaE_born->SetParameter(1,phi);
   sigmaE_born->SetNpx(npx);

   TF1 * sigma_born = new TF1("sigmaborn", xsec_born, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_born->SetLineColor(2);
   sigma_born->SetParameter(0,Eprime);
   sigma_born->SetParameter(1,phi);
   sigma_born->SetNpx(npx);

   TF1 * sigma2_born = new TF1("sigma2born", xsec_born, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_born->SetLineColor(2);
   sigma2_born->SetParameter(0,Q2);
   sigma2_born->SetParameter(1,phi);
   sigma2_born->SetNpx(npx);

   //InSANEInclusiveDiffXSec * fDiffXSec = new  InSANEInclusiveDiffXSec();
   //QuasiElasticInclusiveDiffXSec * fDiffXSec = new  QuasiElasticInclusiveDiffXSec();

   // -----------------------------------------------------------------------------------
   InSANEPOLRADQuasiElasticTailDiffXSec * xsec_polrad1 = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   xsec_polrad1->SetTargetNucleus(targ);
   xsec_polrad1->GetPOLRAD()->SetTargetNucleus(targ);
   xsec_polrad1->GetPOLRAD()->SetQRTMethod(InSANEPOLRAD::kAnglePeaking);
   xsec_polrad1->SetBeamEnergy(beamEnergy);
   xsec_polrad1->InitializePhaseSpaceVariables();
   xsec_polrad1->InitializeFinalStateParticles();
   xsec_polrad1->UsePhaseSpace(false);

   TF1 * sigmaE_polrad1 = new TF1("sigmaEpolrad1", xsec_polrad1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_polrad1->SetLineColor(1);
   sigmaE_polrad1->SetParameter(0,theta);
   sigmaE_polrad1->SetParameter(1,phi);
   sigmaE_polrad1->SetNpx(npx);

   TF1 * sigma_polrad1 = new TF1("sigmapolrad1", xsec_polrad1, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_polrad1->SetLineColor(1);
   sigma_polrad1->SetParameter(0,Eprime);
   sigma_polrad1->SetParameter(1,phi);
   sigma_polrad1->SetNpx(npx);

   TF1 * sigma2_polrad1 = new TF1("sigma2polrad1", xsec_polrad1, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_polrad1->SetLineColor(1);
   sigma2_polrad1->SetParameter(0,Q2);
   sigma2_polrad1->SetParameter(1,phi);
   sigma2_polrad1->SetNpx(npx);

   // -----------------------------------------------------------------------------------
   InSANEPOLRADQuasiElasticTailDiffXSec * xsec_polrad2 = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   xsec_polrad2->SetTargetNucleus(targ);
   xsec_polrad2->GetPOLRAD()->SetTargetNucleus(targ);
   xsec_polrad2->GetPOLRAD()->SetQRTMethod(InSANEPOLRAD::kUltraRel);
   xsec_polrad2->SetBeamEnergy(beamEnergy);
   xsec_polrad2->InitializePhaseSpaceVariables();
   xsec_polrad2->InitializeFinalStateParticles();
   xsec_polrad2->UsePhaseSpace(false);

   TF1 * sigmaE_polrad2 = new TF1("sigmaEpolrad2", xsec_polrad2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_polrad2->SetLineColor(1);
   sigmaE_polrad2->SetLineStyle(2);
   sigmaE_polrad2->SetParameter(0,theta);
   sigmaE_polrad2->SetParameter(1,phi);
   sigmaE_polrad2->SetNpx(npx);

   TF1 * sigma_polrad2 = new TF1("sigmapolrad2", xsec_polrad2, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_polrad2->SetLineColor(1);
   sigma_polrad2->SetLineStyle(2);
   sigma_polrad2->SetParameter(0,Eprime);
   sigma_polrad2->SetParameter(1,phi);
   sigma_polrad2->SetNpx(npx);

   TF1 * sigma2_polrad2 = new TF1("sigma2polrad2", xsec_polrad2, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_polrad2->SetLineColor(1);
   sigma2_polrad2->SetLineStyle(2);
   sigma2_polrad2->SetParameter(0,Q2);
   sigma2_polrad2->SetParameter(1,phi);
   sigma2_polrad2->SetNpx(npx);


   // -----------------------------------------------------------------------------------
   InSANEPOLRADQuasiElasticTailDiffXSec * xsec_polrad3 = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   xsec_polrad3->SetTargetNucleus(targ);
   xsec_polrad3->GetPOLRAD()->SetTargetNucleus(targ);
   xsec_polrad3->GetPOLRAD()->SetQRTMethod(InSANEPOLRAD::kFull);
   xsec_polrad3->SetBeamEnergy(beamEnergy);
   xsec_polrad3->InitializePhaseSpaceVariables();
   xsec_polrad3->InitializeFinalStateParticles();
   xsec_polrad3->UsePhaseSpace(false);

   TF1 * sigmaE_polrad3 = new TF1("sigmaEpolrad3", xsec_polrad3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_polrad3->SetLineColor(1);
   sigmaE_polrad3->SetLineStyle(5);
   sigmaE_polrad3->SetParameter(0,theta);
   sigmaE_polrad3->SetParameter(1,phi);
   sigmaE_polrad3->SetNpx(20);

   TF1 * sigma_polrad3 = new TF1("sigmapolrad3", xsec_polrad3, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_polrad3->SetLineColor(1);
   sigma_polrad3->SetLineStyle(5);
   sigma_polrad3->SetParameter(0,Eprime);
   sigma_polrad3->SetParameter(1,phi);
   sigma_polrad3->SetNpx(20);

   TF1 * sigma2_polrad3 = new TF1("sigma2polrad3", xsec_polrad3, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_polrad3->SetLineColor(1);
   sigma2_polrad3->SetLineStyle(5);
   sigma2_polrad3->SetParameter(0,Q2);
   sigma2_polrad3->SetParameter(1,phi);
   sigma2_polrad3->SetNpx(20);

   // -----------------------------------------------------------------------------------
   QuasiElasticInclusiveDiffXSec * xsec_born2 = new  QuasiElasticInclusiveDiffXSec();
   xsec_born2->SetTargetNucleus(targ);
   xsec_born2->SetBeamEnergy(beamEnergy);
   xsec_born2->InitializePhaseSpaceVariables();
   xsec_born2->InitializeFinalStateParticles();
   xsec_born2->UsePhaseSpace(false);

   TF1 * sigmaE_born2 = new TF1("sigmaEborn2", xsec_born2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_born2->SetLineColor(4);
   sigmaE_born2->SetParameter(0,theta);
   sigmaE_born2->SetParameter(1,phi);
   sigmaE_born2->SetNpx(100);

   TF1 * sigma_born2 = new TF1("sigmaborn2", xsec_born2, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_born2->SetLineColor(4);
   sigma_born2->SetParameter(0,Eprime);
   sigma_born2->SetParameter(1,phi);
   sigma_born2->SetNpx(100);

   TF1 * sigma2_born2 = new TF1("sigma2born2", xsec_born2, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_born2->SetLineColor(4);
   sigma2_born2->SetParameter(0,Q2);
   sigma2_born2->SetParameter(1,phi);
   sigma2_born2->SetNpx(100);

   // -----------------------------------------------------------------------------------
   InSANERadiator<QuasiElasticInclusiveDiffXSec> * xsec_radiator = new  InSANERadiator<QuasiElasticInclusiveDiffXSec>();
   xsec_radiator->SetDeltaM(0.0);
   xsec_radiator->SetInternalOnly(true);
   xsec_radiator->SetTargetNucleus(targ);
   xsec_radiator->SetBeamEnergy(beamEnergy);
   xsec_radiator->InitializePhaseSpaceVariables();
   xsec_radiator->InitializeFinalStateParticles();
   xsec_radiator->UsePhaseSpace(false);

   TF1 * sigmaE_radiator = new TF1("sigmaEradiator", xsec_radiator, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_radiator->SetLineColor(4);
   sigmaE_radiator->SetLineStyle(2);
   sigmaE_radiator->SetParameter(0,theta);
   sigmaE_radiator->SetParameter(1,phi);
   sigmaE_radiator->SetNpx(100);

   TF1 * sigma_radiator = new TF1("sigmaradiator", xsec_radiator, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_radiator->SetLineColor(4);
   sigma_radiator->SetLineStyle(2);
   sigma_radiator->SetParameter(0,Eprime);
   sigma_radiator->SetParameter(1,phi);
   sigma_radiator->SetNpx(100);

   TF1 * sigma2_radiator = new TF1("sigma2radiator", xsec_radiator, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_radiator->SetLineColor(4);
   sigma2_radiator->SetLineStyle(2);
   sigma2_radiator->SetParameter(0,Q2);
   sigma2_radiator->SetParameter(1,phi);
   sigma2_radiator->SetNpx(100);

   // -----------------------------------------------------------------------------------
   InSANERadiator<F1F209QuasiElasticDiffXSec> * xsec_radiator2 = new  InSANERadiator<F1F209QuasiElasticDiffXSec>();
   xsec_radiator2->SetDeltaM(0.0);
   xsec_radiator2->SetInternalOnly(true);
   xsec_radiator2->SetTargetNucleus(targ);
   xsec_radiator2->SetBeamEnergy(beamEnergy);
   xsec_radiator2->InitializePhaseSpaceVariables();
   xsec_radiator2->InitializeFinalStateParticles();
   xsec_radiator2->UsePhaseSpace(false);

   TF1 * sigmaE_radiator2 = new TF1("sigmaEradiator2", xsec_radiator2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_radiator2->SetLineColor(2);
   sigmaE_radiator2->SetLineStyle(2);
   sigmaE_radiator2->SetParameter(0,theta);
   sigmaE_radiator2->SetParameter(1,phi);
   sigmaE_radiator2->SetNpx(npx);

   TF1 * sigma_radiator2 = new TF1("sigmaradiator2", xsec_radiator2, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_radiator2->SetLineColor(2);
   sigma_radiator2->SetLineStyle(2);
   sigma_radiator2->SetParameter(0,Eprime);
   sigma_radiator2->SetParameter(1,phi);
   sigma_radiator2->SetNpx(npx);

   TF1 * sigma2_radiator2 = new TF1("sigma2radiator2", xsec_radiator2, &InSANEInclusiveDiffXSec::WDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma2_radiator2->SetLineColor(2);
   sigma2_radiator2->SetLineStyle(2);
   sigma2_radiator2->SetParameter(0,Q2);
   sigma2_radiator2->SetParameter(1,phi);
   sigma2_radiator2->SetNpx(npx);

   // -------------------------------------------------------------------------------

   TLegend * leg = new TLegend(0.01,0.01,0.99,0.99);
   leg->SetHeader("E = 2.7 GeV");

   leg->AddEntry(sigmaE_born, "F1F209 Quasielastic (Born)","l");
   leg->AddEntry(sigmaE_radiator2,"InSANERadiator<F1F209QuasiElasticDiffXSec> - (internal)","l");
   leg->AddEntry(sigmaE_born2,"QuasiElasticInclusiveDiffXSec (born)","l");
   leg->AddEntry(sigmaE_radiator, "InSANERadiator<QuasiElasticInclusiveDiffXSec> - (internal)","l");
   leg->AddEntry(sigmaE_polrad1,"POLRAD QRT - angle peaking","l");
   leg->AddEntry(sigmaE_polrad2,"POLRAD QRT - (ultra rel. approx.)","l");
   leg->AddEntry(sigmaE_polrad3,"POLRAD QRT - (Full)","l");

   // ----------------------------

   TCanvas * c = new TCanvas("xsec_test_quasilastic","quasielastic xsec");
   c->Divide(2,2);
   TGraph * gr = 0;

   // ----------------------------

   c->cd(1);
   TMultiGraph * mg1 = new TMultiGraph();
   gPad->SetLogy(true);
   TString title      =  xsec_born->GetPlotTitle();
   TString yAxisTitle =  xsec_born->GetLabel();

   gr = new TGraph(sigmaE_born->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");
   gr = new TGraph(sigmaE_born2->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");
   gr = new TGraph(sigmaE_polrad1->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");
   gr = new TGraph(sigmaE_polrad2->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");
   //gr = new TGraph(sigmaE_polrad3->DrawCopy("goff")->GetHistogram());;
   //mg1->Add(gr,"l");
   gr = new TGraph(sigmaE_radiator->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");
   gr = new TGraph(sigmaE_radiator2->DrawCopy("goff")->GetHistogram());;
   mg1->Add(gr,"l");

   mg1->Draw("a");
   mg1->SetTitle("");
   mg1->GetXaxis()->SetTitle("E' (GeV)");
   mg1->GetXaxis()->CenterTitle(true);
   mg1->GetYaxis()->SetTitle(yAxisTitle);
   mg1->GetYaxis()->CenterTitle(true);


   // ----------------------------

   c->cd(2);
   TMultiGraph * mg2 = new TMultiGraph();
   gPad->SetLogy(true);

   gr = new TGraph(sigma2_born->DrawCopy("goff")->GetHistogram());;
   mg2->Add(gr,"l");
   gr = new TGraph(sigma2_born2->DrawCopy("goff")->GetHistogram());;
   mg2->Add(gr,"l");
   gr = new TGraph(sigma2_polrad1->DrawCopy("goff")->GetHistogram());;
   mg2->Add(gr,"l");
   gr = new TGraph(sigma2_polrad2->DrawCopy("goff")->GetHistogram());;
   mg2->Add(gr,"l");
   //gr = new TGraph(sigma2_polrad3->DrawCopy("goff")->GetHistogram());;
   //mg2->Add(gr,"l");
   gr = new TGraph(sigma2_radiator->DrawCopy("goff")->GetHistogram());;
   mg2->Add(gr,"l");
   gr = new TGraph(sigma2_radiator2->DrawCopy("goff")->GetHistogram());;
   mg2->Add(gr,"l");

   mg2->Draw("a");
   mg2->SetTitle("");
   mg2->GetXaxis()->SetTitle("W (GeV)");
   mg2->GetXaxis()->CenterTitle(true);
   mg2->GetYaxis()->SetTitle(yAxisTitle);
   mg2->GetYaxis()->CenterTitle(true);

   // ----------------------------

   c->cd(3);
   TMultiGraph * mg3 = new TMultiGraph();
   //gPad->SetLogy(true);

   gr = new TGraph(sigma_born->DrawCopy("goff")->GetHistogram());;
   mg3->Add(gr,"l");
   gr = new TGraph(sigma_born2->DrawCopy("goff")->GetHistogram());;
   mg3->Add(gr,"l");
   gr = new TGraph(sigma_polrad1->DrawCopy("goff")->GetHistogram());;
   mg3->Add(gr,"l");
   gr = new TGraph(sigma_polrad2->DrawCopy("goff")->GetHistogram());;
   mg3->Add(gr,"l");
   //gr = new TGraph(sigma_polrad3->DrawCopy("goff")->GetHistogram());;
   //mg3->Add(gr,"l");
   gr = new TGraph(sigma_radiator->DrawCopy("goff")->GetHistogram());;
   mg3->Add(gr,"l");
   gr = new TGraph(sigma_radiator2->DrawCopy("goff")->GetHistogram());;
   mg3->Add(gr,"l");

   mg3->Draw("a");
   mg3->SetTitle("");
   mg3->GetXaxis()->SetTitle("#theta (rad)");
   mg3->GetXaxis()->CenterTitle(true);
   mg3->GetYaxis()->SetTitle(yAxisTitle);
   mg3->GetYaxis()->CenterTitle(true);


   // ----------------------------

   c->cd(4);

   leg->Draw();

   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_Quasielastic_%d.png",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_Quasielastic_%d.pdf",aNumber));

   return 0;
}

