Int_t MAIDInclusive_compare(){

   InSANENucleus::NucleusType TargetType    = InSANENucleus::kProton;
   Int_t Z             = 1;
   Int_t A             = 1;
   Double_t beamEnergy = 3.1;
   Double_t Eprime     = 1.41;
   Double_t theta      = 12.0*degree;
   Double_t phi        = 0*degree;

   Int_t    npar       = 2;
   Double_t Emin       = 0.3;
   Double_t Emax       = 5.1;
   Double_t minTheta   = 15.0*degree;
   Double_t maxTheta   = 55.0*degree;
   Int_t    npx        = 250;

   TLegend * leg = new TLegend(0.1,0.7,0.3,0.85);
   TMultiGraph * mg = new TMultiGraph();

   //------------------
   InSANEStructureFunctionsFromPDFs * SFs = new InSANEStructureFunctionsFromPDFs();
   SFs->SetUnpolarizedPDFs(new BBSUnpolarizedPDFs());
   //SFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());

   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs(new BBSPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new DSSVPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new BBPolarizedPDFs());
   //pSFs->SetPolarizedPDFs(new DNS2005PolarizedPDFs());

   InSANEFunctionManager * fm = InSANEFunctionManager::GetInstance();
   //fm->SetPolarizedStructureFunctions(pSFs);
   //fm->SetStructureFunctions(SFs);


   //------------------
   // NucDB data from E94110
   NucDBManager * ndb = NucDBManager::GetManager();
   NucDBExperiment * e94110 = ndb->GetExperiment("JLAB-E94110"); if(!e94110) return( -1);
   NucDBMeasurement * sigma = e94110->GetMeasurement("sigma");   if(!sigma) return(-2);
   std::vector<Double_t> beam_energies;
   sigma->GetUniqueBinnedVariableValues("E",&beam_energies);

   NucDBExperiment * e99118 = ndb->GetExperiment("JLAB-E99118"); if(!e99118) return( -3);
   NucDBMeasurement * sigma_2 = e99118->GetMeasurement("sigma"); if(!sigma_2) return(-4);
   sigma_2->Print();

   NucDBExperiment * e00002 = ndb->GetExperiment("JLAB-E00002"); if(!e00002) return( -3);
   NucDBMeasurement * sigma_3 = e00002->GetMeasurement("sigma"); if(!sigma_3) return(-4);

   // Create a bins to filter the data
   NucDBBinnedVariable * EbeamBin = new NucDBBinnedVariable("E", "E 5.3-5.9 GeV");
   EbeamBin->SetBinMinimum(2.0);
   EbeamBin->SetBinMaximum(3.4);

   NucDBBinnedVariable * ThetaBin = new NucDBBinnedVariable("theta", "0-20 deg");
   ThetaBin->SetBinMinimum(5.0);
   ThetaBin->SetBinMaximum(15.0);

   //--------------------------------
   // Create a new measurement
   // E94110
   NucDBMeasurement * sigma01 = sigma->CreateMeasurementFilteredWithBin(ThetaBin);
   NucDBMeasurement * sigma1 = new NucDBMeasurement("sigma1", Form("#sigma %s", EbeamBin->GetTitle()));
   sigma1->AddDataPoints(sigma01->FilterWithBin(EbeamBin) );
   Double_t beamEnergy1 = sigma1->GetBinnedVariableMean("E");
   Double_t theta1      = sigma1->GetBinnedVariableMean("theta");
   std::cout << " beamEnergy1 = " <<  beamEnergy1 << "\n";
   std::cout << " theta1      = " <<  theta1 << "\n";
   TGraphErrors * gr1 = sigma1->BuildGraph("Eprime");
   gr1->SetLineColor(4009); 
   gr1->SetMarkerColor(4009);
   mg->Add(gr1,"ep");
   leg->AddEntry(gr1,"E94110","ep");
   std::vector<Double_t> beam_energies1;
   std::vector<Double_t> theta_settings1;
   sigma1->GetUniqueBinnedVariableValues("theta",&theta_settings1);
   sigma1->GetUniqueBinnedVariableValues("E",&beam_energies1);

   //NucDBMeasurement * sigma02 = sigma_2->CreateMeasurementFilteredWithBin(ThetaBin);
   //NucDBMeasurement * sigma2 = new NucDBMeasurement("sigma2", Form("#sigma %s", EbeamBin->GetTitle()));
   //sigma2->AddDataPoints(sigma02->FilterWithBin(EbeamBin) );
   //Double_t beamEnergy2 = sigma2->GetBinnedVariableMean("E");
   //Double_t theta2      = sigma2->GetBinnedVariableMean("theta");
   //std::cout << " beamEnergy2 = " <<  beamEnergy2 << "\n";
   //std::cout << " theta2      = " <<  theta2 << "\n";
   //TGraphErrors * gr2 = sigma2->BuildGraph("Eprime");
   //gr2->SetLineColor(4008); 
   //gr2->SetMarkerColor(4008);
   //mg->Add(gr2,"ep");
   //leg->AddEntry(gr2,"E99118","ep");
   //std::vector<Double_t> beam_energies2;
   //std::vector<Double_t> theta_settings2;
   //sigma2->GetUniqueBinnedVariableValues("theta",&theta_settings2);
   //sigma2->GetUniqueBinnedVariableValues("E",&beam_energies2);
   
   NucDBMeasurement * sigma03 = sigma_3->CreateMeasurementFilteredWithBin(ThetaBin);
   NucDBMeasurement * sigma3 = sigma03->CreateMeasurementFilteredWithBin(EbeamBin);
   Double_t beamEnergy3 = sigma3->GetBinnedVariableMean("E");
   Double_t theta3      = sigma3->GetBinnedVariableMean("theta");
   std::cout << " beamEnergy3 = " <<  beamEnergy3 << "\n";
   std::cout << " theta3      = " <<  theta3 << "\n";
   TGraphErrors * gr3 = sigma3->BuildGraph("Eprime");
   gr3->SetLineColor(4010); 
   gr3->SetMarkerColor(4010);
   mg->Add(gr3,"ep");
   leg->AddEntry(gr3,"E00002","ep");
   std::vector<Double_t> beam_energies3;
   std::vector<Double_t> theta_settings3;
   sigma3->GetUniqueBinnedVariableValues("theta",&theta_settings3);
   sigma3->GetUniqueBinnedVariableValues("E",&beam_energies3);


   beamEnergy = (beamEnergy1 + beamEnergy3)/2.0;
   theta      = (theta1      + theta3)*degree/2.0;

   // ------------------------------
   // Cross section models
   // DIS Born cross section 
   //F1F209eInclusiveDiffXSec * fDiffXSec1 = new  F1F209eInclusiveDiffXSec();
   InSANEPOLRADBornDiffXSec * fDiffXSec1 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetA(A);
   fDiffXSec1->SetZ(Z);
   fDiffXSec1->SetTargetType(TargetType);
   fDiffXSec1->GetPOLRAD()->SetHelicity(0);
   fDiffXSec1->GetPOLRAD()->SetTargetType(TargetType);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE1->SetParameter(0,theta);
   sigmaE1->SetParameter(1,phi);
   sigmaE1->SetLineColor(kBlack);
   sigmaE1->SetNpx(npx);
   leg->AddEntry(sigmaE1,"F1F209","l");

   // ------------------------------
   // MAID
   TString target             = "p";
   MAIDInclusiveDiffXSec *fDiffXSec2 = new MAIDInclusiveDiffXSec(target);
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetA(A);
   fDiffXSec2->SetZ(Z);
   fDiffXSec2->SetTargetType(TargetType);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetLineColor(kBlue);
   sigmaE2->SetNpx(npx);
   leg->AddEntry(sigmaE2,"MAID","l");

   // ----------------------------------------
   TCanvas * c = new TCanvas("compare","compare");
   //c->SetLogy(true);
   mg->Draw("a");
   mg->GetHistogram()->SetMaximum(1.4*mg->GetHistogram()->GetMaximum());

   gPad->Modified();

   sigmaE1->DrawCopy("same");
   sigmaE2->DrawCopy("same");

   leg->Draw();

   c->SaveAs("results/cross_sections/inclusive/MAIDInclusive_compare.png");
   c->SaveAs("results/cross_sections/inclusive/MAIDInclusive_compare.tex");
   c->SaveAs("results/cross_sections/inclusive/MAIDInclusive_compare.pdf");

   return(0);
}
