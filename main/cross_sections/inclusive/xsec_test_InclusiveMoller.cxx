Int_t xsec_test_InclusiveMoller(){

   Double_t beamEnergy = 11.0; //GeV
   Double_t theta      = 0.1*degree;
   Double_t phi        = 0.0*degree;  

   Int_t    npar     = 1;
   Double_t minTheta = 0.1;
   Double_t maxTheta = 45.0;

   InSANEInclusiveMollerDiffXSec * xsec0 = new InSANEInclusiveMollerDiffXSec();
   xsec0->SetBeamEnergy(beamEnergy);
   xsec0->SetTargetNucleus(InSANENucleus::Proton());
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();
   xsec0->UsePhaseSpace(false);

   TF1 * func0 = new TF1("sigma",
         [=](double* th, double *ps) {
         double vars[2]= {th[0]*degree, ps[0]};
         return( xsec0->EvaluateXSec( xsec0->GetDependentVariables(vars) ) );
               }, 
         minTheta, maxTheta, npar);
   func0->SetParameter(0,phi);
   func0->SetLineColor(kRed);
   func0->SetNpx(200);

   InSANEExternalRadiator<InSANEInclusiveMollerDiffXSec> * xsec1 = new InSANEExternalRadiator<InSANEInclusiveMollerDiffXSec>();
   xsec1->SetBeamEnergy(beamEnergy);
   xsec1->SetTargetNucleus(InSANENucleus::Proton());
   xsec1->InitializePhaseSpaceVariables();
   xsec1->InitializeFinalStateParticles();
   xsec1->UsePhaseSpace(false);
   //xsec0->Print();

   TF1 * func1 = new TF1("func1", 
         [=](double* th, double *ps) {
         double vars[2]= {th[0]*degree, ps[0]};
         return( xsec1->EvaluateXSec( xsec1->GetDependentVariables(vars) ) );
         }, 
         minTheta, maxTheta, npar);
   func1->SetParameter(0,phi);
   func1->SetLineColor(kBlue);
   func1->SetNpx(200);

   TCanvas * c = new TCanvas();

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   TGraph * gr = 0;
   TMultiGraph * mg = new TMultiGraph();
   
   gr = new TGraph(func0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"lp");
   leg->AddEntry(gr,"Born, p","l");

   gr = new TGraph(func1->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"lp");
   leg->AddEntry(gr,"external radiated, p","l");

   //xsec0->SetTargetNucleus(InSANENucleus::Fe56());
   //func0->SetLineColor(kBlue);
   //gr = new TGraph(func0->DrawCopy("goff")->GetHistogram());
   //mg->Add(gr,"lp");
   //leg->AddEntry(gr,"0.5 deg, Fe","l");

   xsec0->SetBeamEnergy(8.0);
   func0->SetLineColor(kBlack);
   gr = new TGraph(func0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"lp");

   xsec0->SetBeamEnergy(6.0);
   func0->SetLineColor(kBlack);
   gr = new TGraph(func0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"lp");

   xsec0->SetBeamEnergy(4.0);
   func0->SetLineColor(kBlack);
   gr = new TGraph(func0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"lp");

   xsec0->SetBeamEnergy(2.0);
   func0->SetLineColor(kBlack);
   gr = new TGraph(func0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"lp");

   mg->Draw("a");
   gPad->SetLogy(true);
   mg->GetXaxis()->SetTitle("#theta");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("[nb/sr]");
   mg->GetYaxis()->CenterTitle(true);
   leg->Draw();

   return 0;
}

