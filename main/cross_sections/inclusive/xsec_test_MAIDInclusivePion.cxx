Int_t xsec_test_MAIDInclusivePion(Int_t aNumber = 2){

   Double_t beamEnergy = 5.9; //GeV
   Double_t theta_pi   = 10.0*degree;
   Double_t phi_pi     = 80.0*degree;  
   InSANENucleus targetNucleus = InSANENucleus::Proton();

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.1;
   Double_t Emax     = 2.0;
   Int_t    Npx      = 20;

   //-------------------------------------------------------
   // MAID unpolarized
   TString pion               = "pi0";
   TString nucleon            = "p";
   TString target             = "p";

   MAIDInclusivePionDiffXSec *fDiffXSec = new MAIDInclusivePionDiffXSec(nucleon);
   fDiffXSec->SetBeamEnergy(    beamEnergy    );
   fDiffXSec->SetTargetNucleus( targetNucleus );
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->UsePhaseSpace(false);
   fDiffXSec->Refresh();

   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta_pi);
   sigmaE->SetParameter(1,phi_pi);
   sigmaE->SetNpx(Npx);

   TF1 * sigma2 = new TF1("sigma2", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,30*degree);
   sigma2->SetParameter(1,phi_pi);
   sigma2->SetNpx(Npx);

   //-------------------------------------------------------

   TCanvas * c        = new TCanvas("MAIDInclusive","MAID Inclusive");
   gPad->SetLogy(true);
   TString title      = fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph  * mg  = new TMultiGraph();
   TGraph * gr  = 0;

   TLegend      * leg = new TLegend(0.7,0.7,0.8,0.8);
   leg->SetFillColor(kWhite); 
   //leg->SetHeader("MAID cross sections");

   TLatex * latex = new TLatex();
   latex->SetNDC();
   latex->SetTextAlign(21);

   sigmaE->SetParameter(0,theta_pi);
   gr = new TGraph( sigmaE->DrawCopy("goff")->GetHistogram()) ;
   gr->SetLineColor(1);
   leg->AddEntry(gr,Form("#theta_{#pi}=%d",int(theta_pi/degree)),"l");
   mg->Add(gr,"L");

   sigma2->SetParameter(0,20.0*degree);
   gr = new TGraph( sigma2->DrawCopy("goff")->GetHistogram()) ;
   gr->SetLineColor(4);
   leg->AddEntry(gr,Form("#theta_{#pi}=%d",15),"l");
   mg->Add(gr,"l");

   sigma2->SetParameter(0,30.0*degree);
   gr = new TGraph( sigma2->DrawCopy("goff")->GetHistogram()) ;
   gr->SetLineColor(2);
   leg->AddEntry(gr,Form("#theta_{#pi}=%d",30),"l");
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);

   leg->Draw();

   //c->Update();

   //leg->Draw();
   //latex->DrawLatex(0.2,0.38,Form("#theta_{target}=%.0f",theta_target/degree));
   //latex->DrawLatex(0.2,0.3,Form("#theta_{#pi}=%.0f",theta_pi/degree));
   latex->DrawLatex(0.5,0.22,Form("E=%.2fGeV",beamEnergy));

   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_MAIDInclusivePion_%d.png",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_MAIDInclusivePion_%d.pdf",aNumber));
   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_MAIDInclusivePion_%d.tex",aNumber));

   return 0;
}
