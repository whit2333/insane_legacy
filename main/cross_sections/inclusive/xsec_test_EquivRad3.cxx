Int_t xsec_test_EquivRad3(Double_t theta_target = 180*degree){

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();

   Double_t fBeamEnergy = 4.7;
   Double_t Emin        = 0.5;
   Double_t Emax        = 2.5;
   Double_t theta       = 30.0*degree;
   Double_t phi         = 0.0;
   Int_t    npar        = 2;
   Int_t    npx         = 50;
   Int_t    nMC         = 1e3;
   Double_t RadLen[2]; 
   InSANENucleus Target = InSANENucleus::Proton(); 

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,theta_target,0.0);

   TLegend * leg = new TLegend(0.7,0.6,0.85,0.8);

   // born polarized + 
   InSANEPOLRADBornDiffXSec * fDiffXSec1 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec1->SetBeamEnergy(fBeamEnergy);
   fDiffXSec1->SetTargetNucleus(Target);
   fDiffXSec1->GetPOLRAD()->SetHelicity(1);
   fDiffXSec1->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec1->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   fDiffXSec1->Refresh();
   TF1 * sigma1 = new TF1("sigma", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,theta);
   sigma1->SetParameter(1,phi);
   sigma1->SetNpx(npx);
   sigma1->SetLineColor(kRed);
   sigma1->SetLineStyle(2);
   leg->AddEntry(sigma1,"#sigma_{born}^{+}","l");

   // born polarized - 
   InSANEPOLRADBornDiffXSec * fDiffXSec2 = new  InSANEPOLRADBornDiffXSec();
   fDiffXSec2->SetBeamEnergy(fBeamEnergy);
   fDiffXSec2->SetTargetNucleus(Target);
   fDiffXSec2->GetPOLRAD()->SetHelicity(1);
   fDiffXSec2->GetPOLRAD()->SetTargetPolarization(-1.0);
   fDiffXSec2->GetPOLRAD()->SetPolarizationVectors(P_target,1);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigma2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma2->SetParameter(0,theta);
   sigma2->SetParameter(1,phi);
   sigma2->SetNpx(npx);
   sigma2->SetLineColor(kBlue);
   sigma2->SetLineStyle(2);
   leg->AddEntry(sigma2,"#sigma_{born}^{-}","l");

   //-----------------------------------
   InSANEInelasticRadiativeTail2 * fDiffXSec3 = new  InSANEInelasticRadiativeTail2();
   fDiffXSec3->SetBeamEnergy(fBeamEnergy);
   fDiffXSec3->SetTargetNucleus(Target);
   //fDiffXSec3->GetPOLRADIRT()->SetBeamEnergy(fBeamEnergy);
   //fDiffXSec3->SetRegion4(true);
   //fDiffXSec3->GetPOLRADIRT()->SetTargetType(InSANENucleus::kProton);
   //fDiffXSec3->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(1.0);
   //fDiffXSec3->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec3->InitializePhaseSpaceVariables();
   fDiffXSec3->InitializeFinalStateParticles();
   TF1 * sigma3 = new TF1("sigma3", fDiffXSec3, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma3->SetParameter(0,theta);
   sigma3->SetParameter(1,phi);
   sigma3->SetNpx(npx);
   sigma3->SetLineColor(kRed);
   leg->AddEntry(sigma3,"non-template","l");

   //-----------------------------------
   //InSANEInelasticRadiativeTail2 * fDiffXSec4 = new  InSANEInelasticRadiativeTail2();
   InSANERadiator<F1F209eInclusiveDiffXSec> * fDiffXSec4 = new  InSANERadiator<F1F209eInclusiveDiffXSec>();
   //InSANERadiator<InSANEInclusiveDiffXSec> * fDiffXSec4 = new  InSANERadiator<InSANEInclusiveDiffXSec>();
   fDiffXSec4->SetBeamEnergy(fBeamEnergy);
   fDiffXSec4->SetTargetNucleus(Target);
   //fDiffXSec4->GetPOLRADIRT()->SetBeamEnergy(fBeamEnergy);
   //fDiffXSec4->SetRegion4(true);
   //fDiffXSec4->GetPOLRADIRT()->SetTargetType(InSANENucleus::kProton);
   //fDiffXSec4->GetPOLRADIRT()->GetPOLRAD()->SetTargetPolarization(-1.0);
   //fDiffXSec4->GetPOLRADIRT()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec4->InitializePhaseSpaceVariables();
   fDiffXSec4->InitializeFinalStateParticles();
   TF1 * sigma4 = new TF1("sigma4", fDiffXSec4, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma4->SetParameter(0,theta);
   sigma4->SetParameter(1,phi);
   sigma4->SetNpx(npx);
   sigma4->SetLineColor(kBlue);
   leg->AddEntry(sigma4,"template","l");
   fDiffXSec4->Print();


   //------------------------
   TCanvas * c = new TCanvas("rc_test1","rc_test1");
   //gPad->SetLogy(true);
   TString title =  "Internal Inelastic Radiative Tail";//fDiffXSec->GetPlotTitle();
   TString yAxisTitle = "#frac{d#sigma}{dE'd#Omega} (nb/GeV/sr)";
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   gr = new TGraph(sigma1->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma2->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma3->DrawCopy("same"));
   mg->Add(gr,"l");
   gr = new TGraph(sigma4->DrawCopy("same"));
   mg->Add(gr,"l");

   //sigma1->Draw();
   //sigma2->Draw("same");
   //sigma3->Draw("same");
   //sigma4->Draw("same");

   mg->Draw("a");
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->Draw();
   //TPaveText * kine = new TPaveText(0.15,0.2,0.3,0.275,"NDC");
   //kine->AddText(Form("E = %1.1f GeV, #theta = %1.1f deg",fBeamEnergy,theta/degree));
   //kine->Draw("");

   TLatex Tl;
   Tl.SetNDC();
   Tl.SetTextSize(0.03);
   Tl.SetTextAlign(21);
   Tl.DrawLatex(0.5,0.8, Form("E = %1.1f GeV, #theta = %1.1f deg",fBeamEnergy,theta/degree));
   //latex->DrawLatex(0.1,0.1,Form("E = %1.1f, #theta = %1.1f",fBeamEnergy,theta/degree));

   c->SaveAs("results/cross_sections/inclusive/xsec_test_EquivRad3.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_EquivRad3.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_EquivRad3.tex");
   return(0);
}
