Int_t DIS_vs_Q2_nu(){

   Double_t beamEnergy = 20; //GeV
   Double_t Eprime     = 1.01; 
   Double_t theta      = 35.0*degree;//12.9.*degree;
   Double_t phi        = 0.0*degree;  
   Double_t Q2         = 2.0; 


   InSANENucleus targetNucleus = InSANENucleus::Proton();

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Wmin     = 0.5;//3.5;
   Double_t Wmax     = 4.0;//5.10;


   // ------------------
   // 
   InSANEInclusiveBornDISXSec * xsec0 = new InSANEInclusiveBornDISXSec();
   xsec0->SetBeamEnergy(beamEnergy);
   xsec0->SetTargetNucleus(targetNucleus);
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();
   xsec0->UsePhaseSpace(false);
   xsec0->GetPhaseSpace()->Print();

   TF1 * sigma0 = new TF1("sigma0", xsec0, &InSANEInclusiveDiffXSec::WDependentXSec, 
                             Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma0->SetParameter(0,Q2);
   sigma0->SetParameter(1,phi);
   sigma0->SetLineColor(1);

   // --------------------

   TCanvas * c = new TCanvas();
   TGraph * gr = 0;
   TMultiGraph * mg = new TMultiGraph();

   Double_t Q2min = 0.25;
   Double_t Q2max = 10.0;
   Double_t delta_Q2 = 0.25;

   Double_t   Q20 = Q2min;
   while(Q20 <= Q2max ) {


      Double_t   vars[3];
      Double_t   numin    = 0.1;
      Double_t   numax    = 5.050;
      Int_t      Nnu      = 100;
      Double_t   delta_nu = (numax-numin)/double(Nnu);

      std::ofstream fout(Form("results/cross_sections/inclusive/tables/DIS_vs_nu_Q2_%d.txt",int(Q20*100)));
      gr = new TGraph(Nnu);
         fout  << "Qsq" << " " << "nu" << " " << "sigma" << " " << "sigma2" << std::endl;

      for(int i = 0;i< Nnu;i++) {

         double nu = numin + double(i)*delta_nu;
         double ep = beamEnergy - nu;
         double y  = nu/beamEnergy;
         double xb = InSANE::Kine::xBjorken(Q20,nu);
         double th = InSANE::Kine::Theta_xQ2y(xb,Q20,y); 
         double mott = InSANE::Kine::Sig_Mott(beamEnergy, th)*1000.0;
         vars[0] = ep;
         vars[1] = th;
         vars[2] = 0.0;
         double res = xsec0->EvaluateXSec(vars);
         //std::cout << "res: " << res << ", Mott: " << mott << std::endl;
         gr->SetPoint(i,nu,res);
         if(res <= 0.0) res = 0.0;
         fout  << Q20 << " " << nu << " " << res << " " << Q20*Q20*res/100.0/mott << std::endl;
         //fout  << Q20 << " " << nu << " " << res << " " << res << std::endl;

      }
      mg->Add(gr,"l");

      Q20 += delta_Q2;

   }

   mg->Draw("a");
   //TLegend * leg = new TLegend(0.17,0.17,0.6,0.5);
   //leg->SetFillColor(0);
   //leg->SetBorderSize(0);
   //TLatex  latex;

   //sigma0->Draw();
   ////sigma0->SetTitle(title);
   //sigma0->GetXaxis()->SetTitle("W (GeV)");
   //sigma0->GetXaxis()->CenterTitle(true);
   //sigma0->GetYaxis()->SetTitle("");
   //sigma0->GetYaxis()->CenterTitle(true);
   //sigma0->DrawCopy();
   ////leg->AddEntry(sigma0,"Born","l");
   ////leg->AddEntry(sigma1,"^{12}C radiated","l");
   ////leg->AddEntry(sigma1_d,"^{2}H radiated","l");
   ////leg->Draw();

   // --------------------

   //latex.DrawLatex(2.2,700,Form("Q^{2}=%.1f",Q2));

   c->SaveAs("results/cross_sections/inclusive/DIS_vs_Q2_nu.png");

   return 0;
}
