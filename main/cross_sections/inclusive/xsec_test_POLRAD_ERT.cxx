Int_t xsec_test_POLRAD_ERT(){

   Double_t beamEnergy = 5.9; //GeV
   Double_t Eprime     = 0.1; 
   Double_t theta      = 40.0*degree;
   Double_t phi        = 0.0*degree;  
   Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 10;
   Double_t Emin     = 0.1;
   Double_t Emax     = 2.5;
   Double_t minTheta = 15.0*degree;
   Double_t maxTheta = 55.0*degree;

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("Elastic radiative tail cross sections");

   //-----------------------------------
   InSANEPOLRADElasticTailDiffXSec * fDiffXSec = new  InSANEPOLRADElasticTailDiffXSec();
   //InSANEeInclusiveElasticDiffXSec * fDiffXSec = new  InSANEeInclusiveElasticDiffXSec();
   fDiffXSec->SetBeamEnergy(beamEnergy);
   fDiffXSec->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec->GetPOLRAD()->SetHelicity(0);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->Refresh();
   TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE->SetParameter(0,theta);
   sigmaE->SetParameter(1,phi);
   sigmaE->SetNpx(npx);
   leg->AddEntry(sigmaE,"unpolarized","l");
   //sigmaE->SetLineColor(kBlack);

   //-----------------------------------
   InSANEPOLRADElasticTailDiffXSec * fDiffXSec1 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec1->GetPOLRAD()->SetHelicity(1);
   fDiffXSec1->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec1->InitializePhaseSpaceVariables();
   fDiffXSec1->InitializeFinalStateParticles();
   TF1 * sigmaE1 = new TF1("sigma1", fDiffXSec1, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE1->SetParameter(0,theta);
   sigmaE1->SetParameter(1,phi);
   sigmaE1->SetNpx(npx);
   sigmaE1->SetLineColor(kRed);
   leg->AddEntry(sigmaE1,"+polarized","l");

   //-----------------------------------
   InSANEPOLRADElasticTailDiffXSec * fDiffXSec2 = new  InSANEPOLRADElasticTailDiffXSec();
   fDiffXSec2->SetBeamEnergy(beamEnergy);
   fDiffXSec2->SetTargetNucleus(InSANENucleus::Proton());
   fDiffXSec2->GetPOLRAD()->SetHelicity(-1);
   fDiffXSec2->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec2->InitializePhaseSpaceVariables();
   fDiffXSec2->InitializeFinalStateParticles();
   fDiffXSec2->Refresh();
   TF1 * sigmaE2 = new TF1("sigma2", fDiffXSec2, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE2->SetParameter(0,theta);
   sigmaE2->SetParameter(1,phi);
   sigmaE2->SetNpx(npx);
   sigmaE2->SetLineColor(kBlue);
   leg->AddEntry(sigmaE2,"-polarized","l");

   //-----------------------
   TCanvas * c = new TCanvas("xsec_test_POLRAD_ERT","xsec_test_POLRAD_ERT");
   gPad->SetLogy(true);
   TString title =  fDiffXSec->GetPlotTitle();
   TString yAxisTitle = fDiffXSec->GetLabel();
   TString xAxisTitle = "E' (GeV)"; 

   TMultiGraph * mg = new TMultiGraph();
   TGraph      * gr = 0;

   gr = new TGraph(sigmaE->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");

   gr = new TGraph(sigmaE1->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");

   gr = new TGraph(sigmaE2->DrawCopy()->GetHistogram());
   mg->Add(gr,"l");

   mg->Draw("a");
   gPad->Modified();
   mg->SetTitle(title);
   mg->GetXaxis()->SetTitle(xAxisTitle);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle(yAxisTitle);
   mg->GetYaxis()->CenterTitle(true);
   //sigmaE->Draw();
   //sigmaE1->Draw("same");
   //sigmaE2->Draw("same");
   leg->Draw();

   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRAD_ERT.png");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRAD_ERT.pdf");
   c->SaveAs("results/cross_sections/inclusive/xsec_test_POLRAD_ERT.tex");
   return 0;
}
