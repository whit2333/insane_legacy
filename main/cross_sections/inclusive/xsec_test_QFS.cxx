/*! Inclusive elastic scattering cross section.
 *  There are two independent variables for the inclusive cross section.
 *  In addition to phi, the other is either theta or eprime. 
 *  The class InSANEeInclusiveElasticDiffXSec uses theta and phi, however,
 *  plotting as a function of Eprime and theta is possible.
 */ 
Int_t xsec_test_QFS(Int_t aNumber = 16 ){

   Double_t beamEnergy = 0.560; //GeV
   Double_t Eprime     = 0.5; 
   Double_t theta      = 90.*degree;
   Double_t phi        = 0.0*degree;  
   Double_t Z          = 1;   

   // For plotting cross sections 
   Int_t    npar     = 2;
   Int_t    npx      = 40;
   Double_t Emin     = 0.135;
   Double_t Emax     = 0.515;//beamEnergy;
   Double_t minTheta = 20*degree;
   Double_t maxTheta = 90.0*degree;
   Double_t Wmin     = 0.1;
   Double_t Wmax     = beamEnergy;
   InSANENucleus targ = InSANENucleus::Fe56();//(26,56);

   //F1F209QuasiElasticDiffXSec * xsec_born = new F1F209QuasiElasticDiffXSec();
   //InSANEPOLRADQuasiElasticTailDiffXSec * xsec_born = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   QFSInclusiveDiffXSec * xsec_born = new  QFSInclusiveDiffXSec();
   xsec_born->SetTargetNucleus(targ);
   xsec_born->fConfiguration->fPF   = 250;
   xsec_born->fConfiguration->fEPS  = 10;
   xsec_born->fConfiguration->fEPSD = 15;
   xsec_born->SetBeamEnergy(beamEnergy);
   xsec_born->InitializePhaseSpaceVariables();
   xsec_born->InitializeFinalStateParticles();
   xsec_born->UsePhaseSpace(false);

   TF1 * sigmaE_born = new TF1("sigmaE_born", xsec_born, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigmaE_born->SetLineColor(2);
   sigmaE_born->SetParameter(0,theta);
   sigmaE_born->SetParameter(1,phi);
   sigmaE_born->SetNpx(100);

   TF1 * sigma_born = new TF1("sigma_born", xsec_born, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
         minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   sigma_born->SetLineColor(2);
   sigma_born->SetParameter(0,Eprime);
   sigma_born->SetParameter(1,phi);
   sigma_born->SetNpx(100);

   TF1 * sigma2_born = new TF1("sigma2_born", xsec_born, &InSANEInclusiveDiffXSec::PhotonEnergyDependentXSec, 
         Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","PhotonEnergyDependentXSec");
   sigma2_born->SetLineColor(2);
   sigma2_born->SetParameter(0,theta);
   sigma2_born->SetParameter(1,phi);
   sigma2_born->SetNpx(100);

   //InSANEInclusiveDiffXSec * fDiffXSec = new  InSANEInclusiveDiffXSec();
   //QuasiElasticInclusiveDiffXSec * fDiffXSec = new  QuasiElasticInclusiveDiffXSec();

   //InSANEPOLRADQuasiElasticTailDiffXSec * fDiffXSec = new  InSANEPOLRADQuasiElasticTailDiffXSec();
   //fDiffXSec->SetTargetNucleus(targ);
   //fDiffXSec->GetPOLRAD()->SetTargetNucleus(targ);
   //fDiffXSec->GetPOLRAD()->DoQEFullCalc(true);
   //fDiffXSec->SetBeamEnergy(beamEnergy);
   //fDiffXSec->InitializePhaseSpaceVariables();
   //fDiffXSec->InitializeFinalStateParticles();
   //fDiffXSec->UsePhaseSpace(false);

   //TF1 * sigmaE = new TF1("sigmaE", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
   //      Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   //sigmaE->SetLineColor(1);
   //sigmaE->SetParameter(0,theta);
   //sigmaE->SetParameter(1,phi);
   //sigmaE->SetNpx(npx);

   //TF1 * sigma = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec, 
   //      minTheta, maxTheta, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec");
   //sigma->SetLineColor(1);
   //sigma->SetParameter(0,Eprime);
   //sigma->SetParameter(1,phi);
   //sigma->SetNpx(npx);

   //TF1 * sigma2 = new TF1("sigma2", fDiffXSec, &InSANEInclusiveDiffXSec::WDependentXSec, 
   //      Wmin, Wmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   //sigma2->SetLineColor(1);
   //sigma2->SetParameter(0,theta);
   //sigma2->SetParameter(1,phi);
   //sigma2->SetNpx(npx);

   //----------------
   TCanvas * c = new TCanvas("xsec_test_quasilastic","quasielastic xsec");
   c->Divide(2,2);

   c->cd(1);
   gPad->SetLogy(true);
   TString title =  "QFS";
   TString yAxisTitle = "sigama";
   TString xAxisTitle = "#theta (rad)"; 

   //sigma->SetLineColor(kRed);
   sigmaE_born->Draw();
   sigmaE_born->SetTitle(title);
   sigmaE_born->GetXaxis()->SetTitle("E' (GeV)");
   sigmaE_born->GetXaxis()->CenterTitle(true);
   sigmaE_born->GetYaxis()->SetTitle(yAxisTitle);
   sigmaE_born->GetYaxis()->CenterTitle(true);
   sigmaE_born->Draw();
   //sigmaE->Draw("same");

   c->cd(2);
   gPad->SetLogy(false);
   sigma2_born->Draw();
   sigma2_born->SetTitle(title);
   sigma2_born->GetXaxis()->SetTitle("#nu (GeV)");
   sigma2_born->GetXaxis()->CenterTitle(true);
   sigma2_born->GetYaxis()->SetTitle(yAxisTitle);
   sigma2_born->GetYaxis()->CenterTitle(true);
   sigma2_born->Draw();
   //sigma2->Draw("same");

   c->cd(3);
   gPad->SetLogy(true);
   sigma_born->Draw();
   sigma_born->SetTitle(title);
   sigma_born->GetXaxis()->SetTitle("#theta (deg)");
   sigma_born->GetXaxis()->CenterTitle(true);
   sigma_born->GetYaxis()->SetTitle(yAxisTitle);
   sigma_born->GetYaxis()->CenterTitle(true);
   sigma_born->Draw();
   //sigma->Draw("same");

   c->SaveAs(Form("results/cross_sections/inclusive/xsec_test_QFS_%d.png",aNumber));

   return 0;
}

