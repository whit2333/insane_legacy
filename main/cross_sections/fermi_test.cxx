class  fermiMomentumDist {
   public:
      // use constructor to customize your function object
      double operator() (double *x, double *p) {
         // function implementation using class data members
         double k = x[0];
         double P = k*197.0;
         int    A = p[0];
         double res = 0;
         //std::cout << " A=" << A << std::endl;
         fermi3_(&P,&A,&res);
         //res *= 1.0e6;
         return(P*P*res*197.0);
      }
};

class  fermiMomentumDist2 {
   public:
      InSANE::physics::FermiMomentumDist fDist;
      // use constructor to customize your function object
      double operator() (double *x, double *p) {
         // function implementation using class data members
         double k = x[0];
         double P = k;
         int    A = p[0];
         double res = 0;
         //std::cout << " A=" << A << std::endl;
         res = fDist.Evaluate(P,A);//fermi3_(&P,&A,&res);
         //fermi3_(&P,&A,&res);
         //res *= 1.0e9;
         //return(res);
         return(res*(0.197*0.197*0.197));
      }
};

Int_t fermi_test(Int_t aNumber = 2){

   fermiMomentumDist * fobj = new fermiMomentumDist();       // create the function object
   TF1 * f = new TF1("f",fobj,0.0,5.0,1,"fermiMomentumDist");    // create TF1 class.
   f->SetNpx(100);

   fermiMomentumDist2 * fobj2 = new fermiMomentumDist2();
   TF1 * f2 = new TF1("f2",fobj2,0.0,1.50,1,"fermiMomentumDist2");
   f2->SetNpx(100);

   // --------------------------------

   TCanvas * c = new TCanvas();
   gPad->SetLogy(true);
   TLegend * leg = new TLegend(0.7,0.7,0.89,0.89);
   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   // ------------------
   f2->SetParameter(0,4);
   f2->SetLineColor(1);
   gr = new TGraph( f2->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"^{4}He","l");


   // ------------------
   f2->SetParameter(0,2);
   f2->SetLineColor(2);
   gr = new TGraph( f2->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"^{2}H","l");

   // ------------------
   f2->SetParameter(0,56);
   f2->SetLineColor(4);
   gr = new TGraph( f2->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"^{56}Fe","l");

   mg->Draw("a");
   mg->GetYaxis()->SetTitle("n(k)/A [fm^{3}]");
   mg->GetXaxis()->SetTitle("k [GeV]");
   leg->Draw();

   f2->SetParameter(0,16);
   std::cout << "integral (56) : " <<  f2->Integral(0.0,5.0) << std::endl;

   c->SaveAs(Form("results/cross_sections/fermi_test_%d.png",aNumber));

   return(0);
}
