class  BremSpectrum {
   // Bremsstrahlung spectrum as a function of k/E_beam
   public:
      BremSpectrum(InSANETarget * t = 0) { target = t; }
      InSANETarget * target;
      double args[6];
      double operator() (double *x, double *p) {
         if(!target) return 0;
         int i_mat = int(p[0]);
         if( i_mat >= target->GetNMaterials() ) return 0.0;
         double E0    = p[1];
         double k_gamma = x[0]*E0;

         InSANETargetMaterial * mat = target->GetMaterial(i_mat);
         return( k_gamma*mat->GetBremRadiator()->I_gamma(i_mat, k_gamma, E0 ) );
      }
};

Int_t target_brems_spectra(Double_t beamCurrent = 100.0){

   Double_t beamEnergy = 5.9; //GeV
   Double_t Eprime     = 1.0;
   Double_t theta_pi   = 40.0*degree;
   Double_t phi_pi     = 0.0*degree;  

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.2;
   Double_t Emax     = 2.5;
   Int_t    Npx      = 200;

   TLegend * leg = new TLegend(0.1,0.7,0.3,0.9);
   leg->SetFillColor(kWhite); 
   leg->SetHeader("MAID cross sections");

   //-------------------------------------------------------
   //InSANESimpleTargetWithWindows * target = new InSANESimpleTargetWithWindows();
   UVAPolarizedAmmoniaTarget * target = new UVAPolarizedAmmoniaTarget();
   //
   target->Print();

   BremSpectrum * spec = new BremSpectrum(target);
   TF1 * fbrem = new TF1("fbrem", spec, 0.2, 0.9, 2, "BremSpectrum");
   fbrem->SetParameter(0,0);
   fbrem->SetParameter(1,beamEnergy);

   //-------------------------------------------------------

   TCanvas * c        = new TCanvas("WISERInclusive","WISER Inclusive");
   //gPad->SetLogy(true);

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   fbrem->SetParameter(0,0);
   fbrem->SetLineColor(1);
   gr = new TGraph(fbrem->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   fbrem->SetParameter(0,1);
   fbrem->SetLineColor(2);
   gr = new TGraph(fbrem->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");

   //for(int imat = 2 ; imat < target->GetNMaterials(); imat++) {
   //   fbrem->SetParameter(0,imat);
   //   fbrem->SetLineColor(40+imat);
   //   gr = new TGraph(fbrem->DrawCopy("goff")->GetHistogram());
   //   mg->Add(gr,"l");
   //}

   mg->Draw("a");
   mg->SetTitle("rates");
   mg->GetXaxis()->SetTitle("k/E ");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("I_{#gamma}");
   mg->GetYaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetRangeUser(0.0,1.1*sigmaE->GetMaximum());

   c->SaveAs("results/cross_sections/rates/target_brems_spectra.png");
   c->SaveAs("results/cross_sections/rates/target_brems_spectra.pdf");
   //c->SaveAs("results/cross_sections/rates/simple_target_rates.tex");

    target->GetMaterial(0)->GetBremRadiator()->Print();

   return 0;
}
