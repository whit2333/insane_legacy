class  MyFunctionObject2 {
   public:
      MyFunctionObject2(InSANEYield * y = 0) { yield = y; }
      // use constructor to customize your function object
      InSANEYield * yield;
      double args[6];
      double operator() (double *x, double *p) {
         if(!yield) return 0;
         args[0] = x[0];
         args[1] = p[0];
         args[2] = p[1];
         int i_mat = int(p[2]);
         return( yield->CalculateRate(i_mat,args) );
      }
};
class  MyFunctionObject {
   public:
      MyFunctionObject(InSANEYield * y = 0) { yield = y; }
      // use constructor to customize your function object
      InSANEYield * yield;
      double args[6];
      double operator() (double *x, double *p) {
         if(!yield) return 0;
         args[0] = x[0];
         args[1] = p[0];
         args[2] = p[1];
         return( yield->CalculateRate(args) );
      }
};

Int_t simple_target_rates(Double_t beamCurrent = 100.0){

   Double_t beamEnergy = 4.9; //GeV
   Double_t Eprime     = 1.0;
   Double_t theta_pi   = 40.0*degree;
   Double_t phi_pi     = 0.0*degree;  

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.2;
   Double_t Emax     = 4.5;
   Int_t    Npx      = 200;

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->SetFillColor(0); 
   leg->SetFillStyle(0); 
   leg->SetBorderSize(0);

   //-------------------------------------------------------
   //InSANESimpleTargetWithWindows * target = new InSANESimpleTargetWithWindows();
   UVAPolarizedAmmoniaTarget * target = new UVAPolarizedAmmoniaTarget();
   //
   target->Print();

   // -------------------------------------
   // DIS (Born) Electron rate
   PhotoWiserDiffXSec *fDiffXSec0 = new PhotoWiserDiffXSec();
   //OARPionPhotoDiffXSec *fDiffXSec0 = new OARPionPhotoDiffXSec();
   //InSANEInclusiveBornDISXSec *fDiffXSec0 = new InSANEInclusiveBornDISXSec();
   //ElectroWiserDiffXSec *fDiffXSec0 = new ElectroWiserDiffXSec();
   fDiffXSec0->SetBeamEnergy(beamEnergy);
   fDiffXSec0->UsePhaseSpace(false);
   fDiffXSec0->SetProductionParticleType(111);
   InSANEYield * yield0 = new InSANEYield(target,100.0e-9);
   yield0->SetCrossSection(fDiffXSec0);
   yield0->CalculateLuminosity();

   MyFunctionObject * fobj0 = new MyFunctionObject(yield0);
   TF1 * f_rate0 = new TF1("f", fobj0, 0.5, 2.5, 2, "MyFunctionObject");    // create TF1 class.
   f_rate0->SetParameter(0,theta_pi);
   f_rate0->SetParameter(1,phi_pi);
   f_rate0->SetLineWidth(3);

   MyFunctionObject2 * fobj0_0 = new MyFunctionObject2(yield0);
   TF1 * f_rate0_0 = new TF1("f20", fobj0_0, 0.5, 2.5, 3, "MyFunctionObject2");    // create TF1 class.
   f_rate0_0->SetParameter(0,theta_pi);
   f_rate0_0->SetParameter(1,phi_pi);
   f_rate0_0->SetLineWidth(3);

   // -------------------------------------
   //WiserInclusivePhotoXSec2 *fDiffXSec1 = new WiserInclusivePhotoXSec2();
   //ElectroWiserDiffXSec *fDiffXSec1 = new ElectroWiserDiffXSec();
   PhotoWiserDiffXSec2 *fDiffXSec1 = new PhotoWiserDiffXSec2();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->UsePhaseSpace(false);
   fDiffXSec1->SetProductionParticleType(111);
   InSANEYield * yield1 = new InSANEYield(target,100.0e-9);
   yield1->SetCrossSection(fDiffXSec1);
   yield1->CalculateLuminosity();

   yield0->Print();
   yield1->Print();

   MyFunctionObject * fobj1 = new MyFunctionObject(yield1);
   TF1 * f_rate1 = new TF1("fff", fobj1, 0.5, 2.5, 2, "MyFunctionObject");    // create TF1 class.
   f_rate1->SetParameter(0,theta_pi);
   f_rate1->SetParameter(1,phi_pi);
   f_rate1->SetLineWidth(3);

   MyFunctionObject2 * fobj1_0 = new MyFunctionObject2(yield1);
   TF1 * f_rate1_0 = new TF1("f222", fobj1_0, 0.5, 2.5, 3, "MyFunctionObject2");    // create TF1 class.
   f_rate1_0->SetParameter(0,theta_pi);
   f_rate1_0->SetParameter(1,phi_pi);
   f_rate1_0->SetLineWidth(3);

   //-------------------------------------------------------

   TCanvas * c        = new TCanvas("WISERInclusive","WISER Inclusive");
   gPad->SetLogy(true);

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   // ----------------------
   // old wiser photoproduction
   f_rate0->SetLineColor(4);
   gr = new TGraph(f_rate0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"PhotoWiser v1","l");

   f_rate0_0->SetLineColor(4);
   f_rate0_0->SetLineStyle(2);
   // each material alone
   for(int imat = 0 ; imat < target->GetNMaterials() && imat<1; imat++) {
      f_rate0_0->SetParameter(2,imat);
      f_rate0_0->SetLineColor(50+imat);
      gr = new TGraph(f_rate0_0->DrawCopy("goff")->GetHistogram());
      mg->Add(gr,"l");
   }

   // ----------------------
   // new wiser photoproduction
   f_rate1->SetLineColor(2);
   f_rate1->SetLineStyle(0);
   gr = new TGraph(f_rate1->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"PhotoWiser v2","l");

   f_rate1_0->SetLineColor(3);
   f_rate1_0->SetLineStyle(1);
   // each material alone
   for(int imat = 0 ; imat < target->GetNMaterials() ; imat++) {
      f_rate1_0->SetParameter(2,imat);
      f_rate1_0->SetLineColor(20+imat);
      gr = new TGraph(f_rate1_0->DrawCopy("goff")->GetHistogram());
      mg->Add(gr,"l");
   }


   mg->Draw("a");
   mg->SetTitle("rates");
   mg->GetXaxis()->SetTitle("E' [GeV]");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("Rate [1/s/GeV/sr]");
   mg->GetYaxis()->CenterTitle(true);
   //sigmaE->GetYaxis()->SetRangeUser(0.0,1.1*sigmaE->GetMaximum());

   leg->Draw();

   c->SaveAs("results/cross_sections/rates/simple_target_rates.png");
   c->SaveAs("results/cross_sections/rates/simple_target_rates.pdf");
   //c->SaveAs("results/cross_sections/rates/simple_target_rates.tex");


   return 0;
}
