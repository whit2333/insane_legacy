
int MAIDTest(){


  Double_t Ebeam = 5.0; 
  Double_t EpMin = 3.0; 
  Double_t EpMax = 4.7; 
  Double_t theta = 5*degree; 
  Double_t phi   = 0*degree; 

  TString pion    = "piplus"; 
  TString nucleon = "n";
  TString target  = "p";  
  
  MAIDInclusiveDiffXSec *mXS = new MAIDInclusiveDiffXSec(pion,nucleon);
  // mXS->DebugMode(true); 
  mXS->SetBeamEnergy(Ebeam); 
  mXS->InitializePhaseSpaceVariables(); 
  mXS->InitializeFinalStateParticles(); 

  Int_t NPTS = 100; 
  Double_t step = (EpMax-EpMin)/( Double_t(NPTS) ); 

  Double_t ixs,iEp,par[3]; 
  vector<Double_t> vEp,vxs; 

  for(int i=0;i<NPTS;i++){
     iEp    = EpMin + ( Double_t(i) )*step; 
     par[0] = iEp; 
     par[1] = theta; 
     par[2] = phi;
     ixs    = mXS->EvaluateXSec(par); 
     vEp.push_back(iEp); 
     vxs.push_back(ixs);  
  }

  TGraph *g = GetTGraph(vEp,vxs); 
  SetGraphParameters(g,20,kBlack,2); 

  TCanvas *c1 = new TCanvas("c1","MAID Cross Section",800,600);
  c1->SetFillColor(kWhite); 
  c1->cd(); 

  TString Title      = Form("MAID Cross Section for e(%s,e'%s)%s (E = %.2f GeV, #theta = %.2f#circ)",target.Data(),nucleon.Data(),pion.Data(),Ebeam,theta/degree);
  TString xAxisTitle = Form("E' (GeV)");
  TString yAxisTitle = Form("#frac{d^{2}#sigma}{d#OmegadE'} (nb/GeV/sr)");

  g->Draw("AC");
  g->SetTitle(Title); 
  g->GetXaxis()->SetTitle(xAxisTitle); 
  g->GetXaxis()->CenterTitle(); 
  g->GetYaxis()->SetTitle(yAxisTitle); 
  g->GetYaxis()->CenterTitle(); 
  g->Draw("AC");
  c1->Update(); 

  return 0;

}
//______________________________________________________________________________
TGraph *GetTGraph(vector<double> x,vector<double> y){
   const int N = x.size();
   TGraph *g = new TGraph(N,&x[0],&y[0]);
   return g;
}
//______________________________________________________________________________
void SetGraphParameters(TGraph *g,int MarkerStyle,int Color,int width = 1){
   g->SetMarkerStyle(MarkerStyle);
   g->SetMarkerColor(Color);
   g->SetLineColor(Color); 
   g->SetLineWidth(width); 
}
