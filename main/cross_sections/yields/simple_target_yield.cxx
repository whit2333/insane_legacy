class  MyFunctionObject4 {
   public:
      MyFunctionObject4(InSANEYield * y = 0) { yield = y; }
      // use constructor to customize your function object
      InSANEYield * yield;
      double args[6];
      double operator() (double *x, double *p) {
         if(!yield) return 0;
         args[0] = x[0];
         args[1] = p[0];
         args[2] = p[1];
         int i_mat = int(p[2]);
         return( yield->CalculateTotalRate(i_mat,args,11) );
      }
};
class  MyFunctionObject3 {
   public:
      MyFunctionObject3(InSANEYield * y = 0) { yield = y; }
      // use constructor to customize your function object
      InSANEYield * yield;
      double args[6];
      double operator() (double *x, double *p) {
         if(!yield) return 0;
         args[0] = x[0];
         args[1] = p[0];
         args[2] = p[1];
         return( yield->CalculateTotalRate(args,11) );
      }
};
class  MyFunctionObject2 {
   public:
      MyFunctionObject2(InSANEYield * y = 0) { yield = y; }
      // use constructor to customize your function object
      InSANEYield * yield;
      double args[6];
      double operator() (double *x, double *p) {
         if(!yield) return 0;
         args[0] = x[0];
         args[1] = p[0];
         args[2] = p[1];
         int i_mat = int(p[2]);
         return( yield->CalculateRate(i_mat,args) );
      }
};
class  MyFunctionObject {
   public:
      MyFunctionObject(InSANEYield * y = 0) { yield = y; }
      // use constructor to customize your function object
      InSANEYield * yield;
      double args[6];
      double operator() (double *x, double *p) {
         if(!yield) return 0;
         args[0] = x[0];
         args[1] = p[0];
         args[2] = p[1];
         return( yield->CalculateRate(args) );
      }
};

Int_t simple_target_yield(Int_t aNumber = 20, Double_t th_deg   = 40.0, Double_t beamCurrent = 100.0){

   Double_t beamEnergy = 5.9; //GeV
   Double_t Eprime     = 1.0;
   Double_t theta_pi   = th_deg*degree;
   Double_t phi_pi     = 0.0*degree;  

   // For plotting cross sections 
   Int_t    npar     = 2;
   Double_t Emin     = 0.2;
   Double_t Emax     = 4.5;
   Int_t    Npx      = 200;

   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   leg->SetFillColor(0); 
   leg->SetFillStyle(0); 
   leg->SetBorderSize(0);

   //-------------------------------------------------------
   //InSANESimpleTargetWithWindows * target = new InSANESimpleTargetWithWindows();
   UVAPolarizedAmmoniaTarget * target = new UVAPolarizedAmmoniaTarget();
   target->Print();

   // -------------------------------------
   // DIS (Born) Electron rate
   //PhotoWiserDiffXSec *fDiffXSec0 = new PhotoWiserDiffXSec();
   //OARPionPhotoDiffXSec *fDiffXSec0 = new OARPionPhotoDiffXSec();
   InSANEInclusiveBornDISXSec *fDiffXSec0 = new InSANEInclusiveBornDISXSec();
   //InSANERadiator<InSANEInclusiveBornDISXSec> *fDiffXSec0 = new InSANERadiator<InSANEInclusiveBornDISXSec>();
   //ElectroWiserDiffXSec *fDiffXSec0 = new ElectroWiserDiffXSec();
   fDiffXSec0->SetBeamEnergy(beamEnergy);
   fDiffXSec0->UsePhaseSpace(false);
   //fDiffXSec0->SetProductionParticleType(111);

   InSANEYield * yield0 = new InSANEYield(target,100.0e-9);
   yield0->SetCrossSection(fDiffXSec0);
   yield0->CalculateLuminosity();

   MyFunctionObject * fobj0 = new MyFunctionObject(yield0);
   TF1 * f_rate0 = new TF1("f", fobj0, Emin, Emax, 2, "MyFunctionObject");    // create TF1 class.
   f_rate0->SetParameter(0,theta_pi);
   f_rate0->SetParameter(1,phi_pi);
   f_rate0->SetLineWidth(3);

   MyFunctionObject2 * fobj0_0 = new MyFunctionObject2(yield0);
   TF1 * f_rate0_0 = new TF1("f20", fobj0_0, Emin, Emax, 3, "MyFunctionObject2");    // create TF1 class.
   f_rate0_0->SetParameter(0,theta_pi);
   f_rate0_0->SetParameter(1,phi_pi);
   f_rate0_0->SetLineWidth(3);

   // -------------------------------------
   //WiserInclusivePhotoXSec2 *fDiffXSec1 = new WiserInclusivePhotoXSec2();
   //ElectroWiserDiffXSec *fDiffXSec1 = new ElectroWiserDiffXSec();
   //PhotoWiserDiffXSec2 *fDiffXSec1 = new PhotoWiserDiffXSec2();
   InclusivePhotoProductionXSec *fDiffXSec1 = new InclusivePhotoProductionXSec();
   fDiffXSec1->SetBeamEnergy(beamEnergy);
   fDiffXSec1->UsePhaseSpace(false);
   fDiffXSec1->SetProductionParticleType(111);



   InSANEYield * yield1 = new InSANEYield(target,100.0e-9);
   yield1->SetCrossSection(fDiffXSec1);
   yield1->CalculateLuminosity();

   yield0->Print();
   yield1->Print();

   MyFunctionObject * fobj1 = new MyFunctionObject(yield1);
   TF1 * f_rate1 = new TF1("fff", fobj1, Emin, Emax, 2, "MyFunctionObject");    // create TF1 class.
   f_rate1->SetParameter(0,theta_pi);
   f_rate1->SetParameter(1,phi_pi);
   f_rate1->SetLineWidth(3);

   MyFunctionObject2 * fobj1_0 = new MyFunctionObject2(yield1);
   TF1 * f_rate1_0 = new TF1("f222", fobj1_0, Emin, Emax, 3, "MyFunctionObject2");    // create TF1 class.
   f_rate1_0->SetParameter(0,theta_pi);
   f_rate1_0->SetParameter(1,phi_pi);
   f_rate1_0->SetLineWidth(3);


   // tails
   // Add elastic radiative tail for proton 
   InSANEElasticRadiativeTail * xsec_tail = new  InSANEElasticRadiativeTail();
   xsec_tail->SetPolarizations(0.0,0.0);
   //xsec_tail->SetTargetMaterial(*mat);
   //xsec_tail->SetTargetMaterialIndex(i);
   xsec_tail->SetBeamEnergy(beamEnergy);
   //xsec_tail->SetTargetNucleus(InSANENucleus::Proton());
   //xsec_tail->SetTargetNucleus(*targ);
   xsec_tail->UsePhaseSpace(false);

   InSANERadiator<QuasiElasticInclusiveDiffXSec> * QE_xsec = new InSANERadiator<QuasiElasticInclusiveDiffXSec>();
   QE_xsec->SetBeamEnergy(beamEnergy);
   QE_xsec->UsePhaseSpace(false);



   // -----------------------------------------
   // Combined yield
   InSANEYield * yield3 = new InSANEYield(target,100.0e-9);
   yield3->AddCrossSection(fDiffXSec1);
   yield3->AddCrossSection(xsec_tail);
   yield3->AddCrossSection(QE_xsec);
   yield3->AddCrossSection(fDiffXSec0);
   yield3->CalculateLuminosity();

   MyFunctionObject3 * fobj3 = new MyFunctionObject3(yield3);
   TF1 * f_rate3 = new TF1("fff3", fobj3, Emin, Emax, 2, "MyFunctionObject3");    // create TF1 class.
   f_rate3->SetParameter(0,theta_pi);
   f_rate3->SetParameter(1,phi_pi);
   f_rate3->SetLineWidth(3);

   MyFunctionObject4 * fobj3_0 = new MyFunctionObject4(yield3);
   TF1 * f_rate3_0 = new TF1("f343", fobj3_0, Emin, Emax, 3, "MyFunctionObject4");    // create TF1 class.
   f_rate3_0->SetParameter(0,theta_pi);
   f_rate3_0->SetParameter(1,phi_pi);
   f_rate3_0->SetLineWidth(3);

   //-------------------------------------------------------
   TCanvas * c        = new TCanvas("WISERInclusive","WISER Inclusive");
   gPad->SetLogy(true);


   TMultiGraph * mg   = new TMultiGraph();
   TGraph      * gr   = 0;
   TLegend     * leg0 = new TLegend(0.7,0.6,0.85,1.0);
   TLegend     * leg1 = new TLegend(0.85,0.6,1.0,1.0);

   // ----------------------
   // DIS Electron
   f_rate0->SetLineColor(4);
   gr = new TGraph(f_rate0->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg0->AddEntry(gr,"DIS tot.","l");

   f_rate0_0->SetLineColor(4);
   f_rate0_0->SetLineStyle(2);

   for(int imat = 0 ; imat < target->GetNMaterials() ; imat++) {
      f_rate0_0->SetParameter(2,imat);
      f_rate0_0->SetLineColor(40+imat*2);
      gr = new TGraph(f_rate0_0->DrawCopy("goff")->GetHistogram());
      mg->Add(gr,"l");
      leg0->AddEntry(gr,target->GetMaterial(imat)->GetName(),"l");
   }

   // ----------------------
   // photoproduction
   f_rate1->SetLineColor(2);
   f_rate1->SetLineStyle(0);
   f_rate1->SetLineWidth(3);
   gr = new TGraph(f_rate1->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg1->AddEntry(gr,"Photo tot.","l");

   f_rate1_0->SetLineColor(3);
   f_rate1_0->SetLineStyle(1);
   f_rate1_0->SetLineWidth(3);
   // each material alone
   for(int imat = 0 ; imat < target->GetNMaterials(); imat++) {
      f_rate1_0->SetParameter(2,imat);
      f_rate1_0->SetLineColor(40+imat*2);
      gr = new TGraph(f_rate1_0->DrawCopy("goff")->GetHistogram());
      mg->Add(gr,"l");
      leg1->AddEntry(gr,target->GetMaterial(imat)->GetName(),"l");
   }

   // ----------------------
   // combined
   f_rate3->SetLineColor(1);
   f_rate3->SetLineStyle(5);
   f_rate3->SetLineWidth(3);
   gr = new TGraph(f_rate3->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   //leg1->AddEntry(gr,"Photo tot.","l");

   f_rate3_0->SetLineColor(3);
   f_rate3_0->SetLineStyle(5);
   // each material alone
   for(int imat = 0 ; imat < target->GetNMaterials(); imat++) {
      f_rate3_0->SetParameter(2,imat);
      f_rate3_0->SetLineColor(40+imat*2);
      gr = new TGraph(f_rate3_0->DrawCopy("goff")->GetHistogram());
      //mg->Add(gr,"l");
      //leg1->AddEntry(gr,target->GetMaterial(imat)->GetName(),"l");
   }

   //-----------------

   c->Clear();

   mg->Draw("a");
   mg->SetTitle("");
   mg->GetXaxis()->SetTitle("E' [GeV]");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("Rate [1/s/GeV/sr]");
   mg->GetYaxis()->CenterTitle(true);
   mg->GetYaxis()->SetRangeUser(0.0001,mg->GetYaxis()->GetXmax());
   //mg->GetYaxis()->SetRangeUser(1.0e4,mg->GetYaxis()->GetXmax());

   leg->Draw();
   leg0->Draw();
   leg1->Draw();

   TLatex latex;
   latex.SetNDC(true);
   latex.SetTextSize(0.8*latex.GetTextSize());
   latex.DrawLatex(0.2,0.95,Form("E = %.1f GeV, #theta_{#pi} = %.0f^{#circ}",beamEnergy,theta_pi/degree));


   c->SaveAs(Form("results/cross_sections/yields/simple_target_yields_%d.png",aNumber));
   c->SaveAs(Form("results/cross_sections/yields/simple_target_yields_%d.pdf",aNumber));
   //c->SaveAs("results/cross_sections/rates/simple_target_rates.tex");


   return 0;
}
