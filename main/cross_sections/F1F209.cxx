// Test the F1F209eInclusiveDiffXSec class 

{

        // Es and Ep (GeV) 
	Double_t Es    = 5.89;  
	Double_t EpMin = 0.50; 
	Double_t EpMax = 2.00; 
        // 3He target 
        Double_t A = 3.0; 
        Double_t Z = 2.0; 

	// Set the phase space 
        // Ep = scattered electron energy 
	InSANEPhaseSpaceVariable *Ep = new InSANEPhaseSpaceVariable("Ep","E_{p}"); 
	Ep->SetVariableUnits("GeV");
	Ep->SetVariableMinima(EpMin); 
	Ep->SetVariableMaxima(EpMax); 
	Ep->SetParticleIndex(0);
        // ph = horizontal scattering angle   
	InSANEPhaseSpaceVariable *ph = new InSANEPhaseSpaceVariable("ph","#phi"); 
	ph->SetVariableUnits("rad");
	ph->SetVariableMinima(43.0*degree); 
	ph->SetVariableMaxima(46.0*degree); 
	ph->SetParticleIndex(1);
        // th = vertical scattering angle   
	InSANEPhaseSpaceVariable *th = new InSANEPhaseSpaceVariable("th","#theta"); 
	th->SetVariableUnits("rad");
	th->SetVariableMinima(-2.5*degree); 
	th->SetVariableMaxima( 2.5*degree); 
	th->SetParticleIndex(2);
        // Add Ep, ph and th to the phase space 
        InSANEPhaseSpace *PS = new InSANEPhaseSpace(); 
        PS->SetDimension(0); 
        PS->AddVariable(Ep);   
        PS->AddVariable(ph);
        PS->AddVariable(th);
        PS->Print();    
	// Choose cross section model and set the phase space 
	F1F209eInclusiveDiffXSec *XS = new F1F209eInclusiveDiffXSec(); 
        XS->SetPhaseSpace(PS); 
        XS->SetA(A); 
        XS->SetZ(Z); 
	XS->SetBeamEnergy(Es); 
	// use the adjusted model that fits the data in the kinematical range of E06-014.
        XS->UseModifiedModel('y');   

        const int N = PS->GetDimension();  

	Int_t npar   = 2; 
	Double_t ph0 = 45.0*degree; 
	Double_t th0 = 0.0*degree; 
	Double_t xmin = EpMin;
	Double_t xmax = EpMax; 
	Double_t ymin = 0;
	Double_t ymax = 10; 

        // X = (Ep,ph,th) 
	Double_t X[N] = {EpMin,ph0,th0};

        TF1 *Model = new TF1("F1F209XS",XS,&InSANEInclusiveDiffXSec::EnergyDependentXSec,
                             xmin,xmax,npar,"F1F209eInclusiveDiffXSec","EnergyDependentXSec");
	Model->SetParameter(0,ph0);
	Model->SetParameter(1,th0);
	Model->GetYaxis()->SetRangeUser(ymin,ymax);
	Model->SetLineWidth(2);

        TCanvas *c1 = new TCanvas("c1","Cross Section Model",700,500);
	c1->SetFillColor(kWhite); 

	c1->cd();
	Model->Draw();
	c1->Update();

        // Double_t xsec = XS->EvaluateXSec(X); 

        // std::cout << Form("%.2f",E   )        << " GeV"       << "\t" 
        //           << Form("%.2f",X[0])        << " GeV"       << "\t" 
        //           << Form("%.2f",X[1]/degree) << " deg"       << "\t" 
        //           << Form("%.3E",xsec)        << " nb/GeV/sr" << std::endl;


}
