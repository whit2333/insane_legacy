/*! Standard cuts. 
 *  The main cuts are:
 *   - Cherenkov TDC
 *   - Cherenkov ADC
 *   - Single Lucite TDC 
 *
 */
Int_t std_cuts_para47_by_row( Int_t runnumber   = 72999,
                              Int_t fileVersion = 3,
                              Double_t E_min    = 800.0,
                              Int_t row         = 37      )
{

   if( gROOT->LoadMacro("asym/init_asymmetries.cxx") )  {
      Error(weh, "Failed loading asym/init_asymmetries.cxx in compiled mode.");
      return 1;
   }

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   Int_t  NEventsMax = 6e6;

   InSANERun        * run = rman->GetCurrentRun();
   InSANERunSummary  rSum =  run->GetRunSummary();

   InSANEFunctionManager * fman  = InSANEFunctionManager::GetManager();
   fman->SetBeamEnergy(4.7);
   //fman->CreateSFs(6);
   //fman->CreatePolSFs(6);

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();

   BIGCALGeometryCalculator * bcgeo = BIGCALGeometryCalculator::GetCalculator();
   //bcgeo->LoadBadBlocks(Form("detectors/bigcal/bad_blocks/bigcal_noisy_blocks%d.txt",runnumber));

   // ----------------------------------------------------------
   // Trees
   TTree * t = (TTree*) gROOT->FindObject("Tracks"); 
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);

   const char * trackingTree = "trackingPositions";
   TTree * t2 = (TTree*)gROOT->FindObject(trackingTree);
   if(!t2) {
      std::cout << "No Tree Found: " << trackingTree << "\n";
      return(-11);
   }
   t2->BuildIndex("fRunNumber","fEventNumber");
   t->AddFriend(t2);

   // ----------------------------------------------------------
   // Event classes
   InSANEReconstructedEvent * fRecon = new InSANEReconstructedEvent();
   t2->SetBranchAddress("uncorrelatedPositions",&fRecon);

   InSANEHitPosition * lucHit = 0;
   BIGCALCluster     * cluster = 0;
   bool goodLucite            = false;
   int  goodClusterN          = 0;

   Int_t ev      = 0;
   Int_t runnum  = 0;
   Int_t stdcut  = 0;
   TH1F * h1     = 0;
   TH2F * h2     = 0;

   //TTree * selectt = new TTree("cuts","The cuts");
   //selectt->Branch("ev",&ev,"ev/I");
   //selectt->Branch("runnum",&runnum,"runnum/I");
   //selectt->Branch("stdcut",&stdcut,"stdcut/I");
   bool passed = false;

   // --------------------------------------------------------------------
   // Histograms
   TH1::AddDirectory(kTRUE); 
   TFile * hist_file = new TFile(Form("data/cuts/std_cuts_para47_row_%d.root",row),"UPDATE");

   Double_t theta_range[2] = {0.4,1.0};
   Double_t phi_range[2] = {-0.6,1.0};
   Double_t energy_range[2] = {500,4500};

   hist_file->cd();
   //      
   
   TH1F * hCerTDC01   = new TH1F("hCutsCerTDC01","Cuts Cer TDC 01",200,-500,500);
   TH1F * hCerTDC02   = new TH1F("hCutsCerTDC01","Cuts Cer TDC 02",200,-500,500);
   TH1F * hCerTDC11   = new TH1F("hCutsCerTDC11","Cuts Cer TDC 11",200,-500,500);
   TH1F * hCerTDC12   = new TH1F("hCutsCerTDC11","Cuts Cer TDC 12",200,-500,500);
   TH1F * hCerTDC21   = new TH1F("hCutsCerTDC21","Cuts Cer TDC 21",200,-500,500);
   TH1F * hCerTDC22   = new TH1F("hCutsCerTDC21","Cuts Cer TDC 22",200,-500,500);

   TH1F * hCerADC01   = new TH1F("hCutsCerADC01","Cuts Cer ADC 01",200,0,5);
   TH1F * hCerADC02   = new TH1F("hCutsCerADC01","Cuts Cer ADC 02",200,0,5);
   TH1F * hCerADC11   = new TH1F("hCutsCerADC11","Cuts Cer ADC 11",200,0,5);
   TH1F * hCerADC12   = new TH1F("hCutsCerADC11","Cuts Cer ADC 12",200,0,5);
   TH1F * hCerADC21   = new TH1F("hCutsCerADC21","Cuts Cer ADC 21",200,0,5);
   TH1F * hCerADC22   = new TH1F("hCutsCerADC21","Cuts Cer ADC 22",200,0,5);

   TH1F * hCerTDC0    = new TH1F("hCutsCerTDC0", "Cuts Cer TDC 0", 200,-500,500);
   TH1F * hCerTDC1    = new TH1F("hCutsCerTDC1", "Cuts Cer TDC 1", 200,-500,500);
   TH1F * hCerTDC2    = new TH1F("hCutsCerTDC2", "Cuts Cer TDC 2", 200,-500,500);
   TH1F * hCerTDC3    = new TH1F("hCutsCerTDC3", "Cuts Cer TDC 3", 200,-500,500);
   TH1F * hCerTDC4    = new TH1F("hCutsCerTDC4", "Cuts Cer TDC 4", 200,-500,500);
   TH1F * hCerTDC5    = new TH1F("hCutsCerTDC5", "Cuts Cer TDC 5", 200,-500,500);
   TH1F * hCerTDC6    = new TH1F("hCutsCerTDC6", "Cuts Cer TDC 6", 200,-500,500);
   TH1F * hCerTDC7    = new TH1F("hCutsCerTDC7", "Cuts Cer TDC 7", 200,-500,500);
   TH1F * hCerTDC8    = new TH1F("hCutsCerTDC8", "Cuts Cer TDC 8", 200,-500,500);

   TH1F * hCerADC0    = new TH1F("hCutsCerADC0", "Cuts Cer ADC 0", 100,0,5);
   TH1F * hCerADC1    = new TH1F("hCutsCerADC1", "Cuts Cer ADC 1", 100,0,5);
   TH1F * hCerADC2    = new TH1F("hCutsCerADC2", "Cuts Cer ADC 2", 100,0,5);
   TH1F * hCerADC3    = new TH1F("hCutsCerADC3", "Cuts Cer ADC 3", 100,0,5);
   TH1F * hCerADC4    = new TH1F("hCutsCerADC4", "Cuts Cer ADC 4", 100,0,5);
   TH1F * hCerADC5    = new TH1F("hCutsCerADC5", "Cuts Cer ADC 5", 100,0,5);
   TH1F * hCerADC6    = new TH1F("hCutsCerADC6", "Cuts Cer ADC 6", 100,0,5);
   TH1F * hCerADC7    = new TH1F("hCutsCerADC7", "Cuts Cer ADC 7", 100,0,5);
   TH1F * hCerADC8    = new TH1F("hCutsCerADC8", "Cuts Cer ADC 8", 100,0,5);

   TH1F * hEnergy0    = new TH1F("hCutsEnergy0","Cuts Energy 0",100,0,3000);
   TH1F * hEnergy1    = new TH1F("hCutsEnergy1","Cuts Energy 1",100,0,3000);
   TH1F * hEnergy2    = new TH1F("hCutsEnergy2","Cuts Energy 2",100,0,3000);
   TH1F * hEnergy3    = new TH1F("hCutsEnergy3","Cuts Energy 3",100,0,3000);
   TH1F * hEnergy4    = new TH1F("hCutsEnergy4","Cuts Energy 4",100,0,3000);
   TH1F * hEnergy5    = new TH1F("hCutsEnergy5","Cuts Energy 5",100,0,3000);
   TH1F * hEnergy6    = new TH1F("hCutsEnergy6","Cuts Energy 6",100,0,3000);
   TH1F * hEnergy7    = new TH1F("hCutsEnergy7","Cuts Energy 7",100,0,3000);
   TH1F * hEnergy8    = new TH1F("hCutsEnergy8","Cuts Energy 8",100,0,3000);

   TH1F * hLucTDC0    = new TH1F("hCutsLucTDC0", "Cuts Luc TDC 0", 200,-500,500);
   TH1F * hLucTDC1    = new TH1F("hCutsLucTDC1", "Cuts Luc TDC 1", 200,-500,500);
   TH1F * hLucTDC2    = new TH1F("hCutsLucTDC2", "Cuts Luc TDC 2", 200,-500,500);
   TH1F * hLucTDC3    = new TH1F("hCutsLucTDC3", "Cuts Luc TDC 3", 200,-500,500);
   TH1F * hLucTDC4    = new TH1F("hCutsLucTDC4", "Cuts Luc TDC 4", 200,-500,500);
   TH1F * hLucTDC5    = new TH1F("hCutsLucTDC5", "Cuts Luc TDC 5", 200,-500,500);
   TH1F * hLucTDC6    = new TH1F("hCutsLucTDC6", "Cuts Luc TDC 6", 200,-500,500);
   TH1F * hLucTDC7    = new TH1F("hCutsLucTDC7", "Cuts Luc TDC 7", 200,-500,500);
   TH1F * hLucTDC8    = new TH1F("hCutsLucTDC8", "Cuts Luc TDC 8", 200,-500,500);

   TH2F * hPhiVsTheta1 = new TH2F("hPhiVsTheta1","Phi vs Theta;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);
   TH2F * hPhiVsTheta2 = new TH2F("hPhiVsTheta2","Phi vs Theta with cuts;#theta;#phi",
         100,theta_range[0],theta_range[1],100,phi_range[0],phi_range[1]);

   //TEfficiency* effPhi   = new TEfficiency("effPhi","Cut Efficiency vs #phi;#phi;#epsilon",
   //      20,phi_range[0],phi_range[1]);
   //TEfficiency* effTheta = new TEfficiency("effTheta","Cut Efficiency vs #theta;#theta;#epsilon",
   //      20,theta_range[0],theta_range[1]);
   //TEfficiency* effEnergy = new TEfficiency("effEnergy","Cut Efficiency vs E;E;#epsilon",
   //      20,energy_range[0],energy_range[1]);

   // ------------------------------------------------
   // Create the target and dilution from target
   Double_t pf                      = run->fPackingFraction;
   if(pf>1.0) pf = pf/100.0;
   InSANEDilutionFromTarget * df    = new InSANEDilutionFromTarget();
   UVAPolarizedAmmoniaTarget * targ = new UVAPolarizedAmmoniaTarget("UVaTarget","UVa Pol. Target",pf);
   targ->SetPackingFraction(pf);
   df->SetTarget(targ);

   // ------------------------------------------------
   // Create the asymmetries to be calculated.
   //TList *  fAsymmetries = init_asymmetries(dis,rSum,df);
   TList *  fAsymmetries = init_asymmetries(dis,rSum,0);
   Double_t QsqMin[4] = {1.89,2.55,3.45,4.67};
   Double_t QsqMax[4] = {2.55,3.45,4.67,6.31};
   Double_t QsqAvg[4];
   for(int i=0;i<4;i++){
      QsqAvg[i] = (QsqMax[i]+QsqMin[i])/2.0;
   }
   std::vector<int> groups;

   InSANEDatabaseManager * dbman = InSANEDatabaseManager::GetManager();
   dbman->CloseConnections();

   hist_file->cd();
   // -------------------------------------------------------------
   // Event Loop
   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0; iEvent < nEvents && iEvent<NEventsMax; iEvent++){

      if(iEvent%1000 == 0) {
         std::cout << "\r" << iEvent << "     " ;
         std::cout.flush();
      }

      t->GetEntry(iEvent);
      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;
      Int_t bytes = t2->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      if(bytes <= 0) continue;

      dis->fRunNumber         = traj->fRunNumber;
      runnum                  = traj->fRunNumber;
      fDisEvent->fEventNumber = traj->fEventNumber;
      ev                      = traj->fEventNumber;
      stdcut                  = 0;
      dis->fHelicity          = traj->fHelicity;
      dis->fx                 = traj->GetBjorkenX();
      dis->fW                 = TMath::Sqrt(traj->GetInvariantMassSquared())/1000.0;
      dis->fQ2                = traj->GetQSquared()/1000000.0;
      dis->fTheta             = traj->fTheta;
      dis->fPhi               = traj->fPhi;
      dis->fEnergy            = traj->fEnergy/1000.0;
      dis->fBeamEnergy        = traj->fBeamEnergy/1000.0;
      dis->fGroup             = traj->fSubDetector;
      //dis->fIsGood            = false;
      //dis->fIsGood            = traj->fIsGood;
      //std::cout << "E= " << dis->fEnergy << std::endl;
      //std::cout << "phi = " << dis->fPhi/degree << std::endl;
      //std::cout << "theta = " << dis->fTheta/degree << std::endl;
      Int_t ibl = bcgeo->GetBlockNumber(traj->fCluster.fiPeak,traj->fCluster.fjPeak);
      groups.clear();
      if(traj->fCluster.fjPeak >32 ) {
         int gvert = 15+2+ (traj->fCluster.fjPeak-33)/12;
         groups.push_back(gvert);
      } else {
         int gvert = 15+ (traj->fCluster.fjPeak-1)/16;
         groups.push_back(gvert);
      }

      if( (traj->fCluster.fjPeak > row) ) {

      // -----------------------------
      // Cut level 0
      hCerTDC0->Fill(traj->fCherenkovTDC);
      hCerTDC01->Fill(traj->fCherenkovTDC);
      hCerADC0->Fill(traj->fNCherenkovElectrons);
      hCerADC01->Fill(traj->fCluster.fCherenkovBestADCSum);
      hEnergy0->Fill(traj->fEnergy);
      hLucTDC0->Fill(traj->fLuciteMissDistance);

      if( trig->IsBETA2Event() )   
      if( (TMath::Abs(traj->fCluster.fXMoment) < 55 ) && (TMath::Abs(traj->fCluster.fYMoment) < 100 ) ) {
      // -----------------------------
      // Cut level 1

      hCerTDC1->Fill(traj->fCherenkovTDC);
      hCerADC1->Fill(traj->fNCherenkovElectrons);
      hEnergy1->Fill(traj->fEnergy);
      hLucTDC1->Fill(traj->fLuciteMissDistance);

      //if( dis->fW > 1.0 )
      //if( !traj->fIsNoisyChannel)
      //if( traj->fIsGood)
      //if( (TMath::Abs(traj->fCluster.fClusterTime1) < 5 ) || (TMath::Abs(traj->fCluster.fClusterTime2) < 5 ) ) 
      
      if( traj->fNCherenkovElectrons > 0.4 && traj->fNCherenkovElectrons < 1.7 ) {
      // --------------------------
      // Cut level 2
      hCerTDC2->Fill(traj->fCherenkovTDC);
      hCerADC2->Fill(traj->fNCherenkovElectrons);
      hEnergy2->Fill(traj->fEnergy);
      hLucTDC2->Fill(traj->fLuciteMissDistance);


      if( traj->fEnergy > E_min ) {
      // --------------------------
      // Cut level 3
      hCerTDC3->Fill(traj->fCherenkovTDC);
      hCerADC3->Fill(traj->fNCherenkovElectrons);
      hEnergy3->Fill(traj->fEnergy);
      hLucTDC3->Fill(traj->fLuciteMissDistance);

      if( !(bcgeo->IsBadBlock(ibl)) ) {
      // --------------------------
      // Cut level 4
      hCerTDC4->Fill(traj->fCherenkovTDC);
      hCerADC4->Fill(traj->fNCherenkovElectrons);
      hEnergy4->Fill(traj->fEnergy);
      hLucTDC4->Fill(traj->fLuciteMissDistance);

      if(TMath::Abs(traj->fCherenkovTDC) < 25) {
      // --------------------------
      // Cut level 5
      hCerTDC5->Fill(traj->fCherenkovTDC);
      hCerADC5->Fill(traj->fNCherenkovElectrons);
      hEnergy5->Fill(traj->fEnergy);
      hLucTDC5->Fill(traj->fLuciteMissDistance);

      // A quick lucite calculation
      goodLucite = false;
      if( fRecon->fNTrajectories > 0 ){ 
         // Determine if there is a good lucite hit
         for(int i = 0; i< fRecon->fNLucitePositions; i++){
            lucHit = (InSANEHitPosition*)(*(fRecon->fLucitePositions))[i];
     
            if( TMath::Abs(lucHit->fPositionUncertainty.Y() ) < 10 ) {
               if( TMath::Abs(lucHit->fTiming ) < 250 ) {
                  goodLucite = true;
                  goodClusterN = lucHit->fClusterNumber; 
                  break;
               }
            }
         }
      }

      if(goodLucite){
      // --------------------------
      // Cut level 6
      hCerTDC6->Fill(traj->fCherenkovTDC);
      hCerADC6->Fill(traj->fNCherenkovElectrons);
      hEnergy6->Fill(traj->fEnergy);
      hLucTDC6->Fill(traj->fLuciteMissDistance);

         passed = true;
         hPhiVsTheta2->Fill(dis->fTheta,dis->fPhi);
         stdcut = 1;
         //for(int j = 0; j<fAsymmetries->GetEntries();j++) {
         //   ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent();
         //   ((InSANEMeasuredAsymmetry*)(fAsymmetries->At(j)))->CountEvent(groups);
         //}
        
      } // cut level 6
      } // cut level 5
      } // cut level 4
      } // cut level 3
      } // cut level 2
      } // cut level 1
      } // cut level 0
   }

   hist_file->Write();

   //std::cout << " Calculating Asymmetries... \n";
   //for(int j = 0;j<fAsymmetries->GetEntries();j++) {
   //   SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
   //   Double_t x_mean = asy->fNPlusVsx->GetMean();
   //   asy->SetDilution(df->GetDilution(x_mean,asy->GetQ2()) );
   //   asy->SetRunNumber(runnumber);
   //   asy->SetRunSummary(&rSum);
   //   asy->CalculateAsymmetries();
   //}

   //for(int j = 0;j<fAsymmetries->GetEntries();j++) {
   //   SANEMeasuredAsymmetry * asy = ((SANEMeasuredAsymmetry*)(fAsymmetries->At(j)));
   //   std::ofstream fileout(Form("plots/%d/binned_asym1-%d.dat",runnumber,j),std::ios_base::trunc );
   //   asy->PrintBinnedTableHead(fileout) ;
   //   asy->PrintBinnedTable(fileout) ;
   //   asy->Print();
   //   asy->PrintBinnedTableHead(std::cout) ;
   //   asy->PrintBinnedTable(std::cout) ;
   //}


   //TFile * f = new TFile(Form("data/asym-%d/binned_asymmetries_para47_%d.root",fileVersion,runnumber),"UPDATE");
   //f->cd();
   //f->WriteObject(fAsymmetries,Form("binned-asym-%d",runnumber));//,TObject::kSingleKey); 

   //delete events;
   delete traj;
   delete trig;
   delete dis;

   return(0);
}
