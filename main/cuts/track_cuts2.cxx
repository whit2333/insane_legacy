Int_t track_cuts2(Int_t runnumber = 72893 ) {

   if(!rman->IsRunSet() ) rman->SetRun(runnumber);
   else if(runnumber != rman->fCurrentRunNumber) runnumber = rman->fCurrentRunNumber;
   rman->GetCurrentFile()->cd();

   InSANERun * run = rman->GetCurrentRun();
   InSANERunSummary rSum =  run->GetRunSummary();

   InSANEDISTrajectory * traj = new InSANEDISTrajectory();
   InSANETriggerEvent  * trig = new InSANETriggerEvent();
   InSANEDISEvent      * dis  = new InSANEDISEvent();
   InSANEReconstructedEvent * recon = new InSANEReconstructedEvent();
   TH1F * h1 = 0;

   TTree * t = (TTree*) gROOT->FindObject("Tracks");
   if(!t) return -1;
   t->SetBranchAddress("trajectory",&traj);
   t->SetBranchAddress("triggerEvent",&trig);
   TTree * t2 = (TTree*) gROOT->FindObject("trackingPositions");
   if(!t2) return -2;
   t2->SetBranchAddress("uncorrelatedPositions",&recon);
   t2->BuildIndex("uncorrelatedPositions.fRunNumber","uncorrelatedPositions.fEventNumber");
   t->AddFriend(t2);
   std::cout << " done adding friend"<< std::endl;;
   //t->StartViewer();
   //return 0;
   //SANEEvents * events = new SANEEvents("betaDetectors1");
   //events->fTree->BuildIndex("fRunNumber","fEventNumber");

   TCanvas * c = new TCanvas("track_cuts1","track_cuts1");
   c->Divide(2,3);

   TCut tdccut   = "((TMath::Abs(fCluster.fClusterTime1) < 50) || ( TMath::Abs(fCluster.fClusterTime2) < 50) )";
   TCut certdc   = "TMath::Abs(fCherenkovTDC)<50";
   TCut luccut   = "trackingPositions.uncorrelatedPositions.fClosestLuciteMissDistance > 0 && trackingPositions.uncorrelatedPositions.fClosestLuciteMissDistance < 200"; 
   TCut cluster1 = "fNClusters == 1";
   TCut cut0     = "triggerEvent.IsBETA2Event()";
   TCut cut1     = "fIsGood && triggerEvent.IsBETA2Event()";
   TCut cut2     = cut1 && tdccut && certdc;
   TCut cut2_2   = cut1 && tdccut && certdc && luccut;
   TCut cut3_1  = "fNCherenkovElectrons < 1.5 && fIsGood && triggerEvent.IsBETA2Event() ";
   TCut cut3    = cut3_1 && tdccut && certdc;
   TCut cut4_1  = "fMomentum.fE > 900 && fNCherenkovElectrons < 1.5 && fIsGood && triggerEvent.IsBETA2Event()  ";
   TCut cut4    = cut4_1 && tdccut;
   TCut cut5_1  = "fMomentum.fE > 1300 && fNCherenkovElectrons < 1.5 && fIsGood && triggerEvent.IsBETA2Event() ";
   TCut cut5    = cut5_1 && tdccut;
   TCut cut6_1  = "fNCherenkovElectrons > 0.3 && fNCherenkovElectrons < 1.5 && fIsGood && triggerEvent.IsBETA2Event() ";
   TCut cut6    = cut6_1 && tdccut;
   TCut cut7_1  = "fNCherenkovElectrons > 2.0 && fNCherenkovElectrons < 3.0 && fIsGood && triggerEvent.IsBETA2Event() ";
   TCut cut7    = cut7_1 && tdccut;
   TCut cut8    = "TMath::Abs(fPhi) < 0.4 && fIsGood && triggerEvent.IsBETA2Event() && TMath::Abs(fCherenkovTDC)<500";

   TCut cutRCS  = "fPosition0.fY > 20 && triggerEvent.IsBETA2Event()";
   TCut cutPROT = "fPosition0.fY < 10 && triggerEvent.IsBETA2Event()";

   TCut cutRCS0  = cutRCS   + cut0 ;
   TCut cutPROT0 = cutPROT  + cut0 ;
   TCut cutRCS1  = cutRCS   + cut1 ;
   TCut cutPROT1 = cutPROT  + cut1 ;
   TCut cutRCS2  = cutRCS   + cut2 ;
   TCut cutPROT2 = cutPROT  + cut2 ;
   TCut cutRCS3  = cutRCS   + cut3 ;
   TCut cutPROT3 = cutPROT  + cut3 ;
   TCut cutRCS4  = cutRCS   + cut4 ;
   TCut cutPROT4 = cutPROT  + cut4 ;

   //------------------------------------------------------------
   c->cd(1)->Divide(3,1);

   c->cd(1)->cd(1); 
   t->Draw("fPosition0.fY:fPosition0.fX",cut0 ,"colz");
   h1= (TH1F*)gROOT->FindObject("htemp");
   if(h1) {
      h1->SetTitle("cut0");
   }

   c->cd(1)->cd(2); 
   t->Draw("fPosition0.fY:fPosition0.fX",cut1 ,"colz");

   c->cd(1)->cd(3); 
   t->Draw("fPosition0.fY:fPosition0.fX",cut2 ,"colz");
   //return(0);
   //------------------------------------------------------------
   c->cd(2)->Divide(3,1);

   c->cd(2)->cd(1); 
   t->Draw("fPosition0.fY:fPosition0.fX",cut3 ,"colz");

   c->cd(2)->cd(2); 
   t->Draw("fPosition0.fY:fPosition0.fX",cut4 ,"colz");

   c->cd(2)->cd(3); 
   t->Draw("fPosition0.fY:fPosition0.fX",cut5 ,"colz");

   //------------------------------------------------------------
   c->cd(3);
   //gPad->SetLogy(true);
   TLegend * leg3 = new TLegend(0.6,0.6,0.9,0.9);
   t->Draw("fNCherenkovElectrons>>hcer(100,0,4)",cut1 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcer");
   if(h1){
      h1->SetTitle("Cherenkov ADC");
      h1->GetXaxis()->SetTitle("N electrons");
      h1->Draw();
      leg3->AddEntry(h1,"BETA2 + good block","l");
   }

   t->Draw("fNCherenkovElectrons>>hcer1(100,0,4)",cut2 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcer1");
   if(h1){
      h1->SetFillStyle(3002);
      h1->SetFillColor(1);
      h1->Draw("same");
      leg3->AddEntry(h1,"+ Cherenkov TDC","lf");
   }

   t->Draw("fNCherenkovElectrons>>hcer22(100,0,4)",cut2_2 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcer22");
   if(h1){
      h1->SetFillStyle(3004);
      h1->SetFillColor(3);
      h1->SetLineColor(3);
      h1->Draw("same");
      leg3->AddEntry(h1,"+ Lucite","lf");
   }

   t->Draw("fNCherenkovElectrons>>hcerRCS1(100,0,4)",cutRCS2 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcerRCS1");
   if(h1){
      //h1->SetFillStyle(3002);
      //h1->SetFillColor(1);
      h1->SetLineColor(kBlue);
      h1->Draw("same");
      leg3->AddEntry(h1," (RCS) + Cherenkov TDC","lf");
   }

   t->Draw("fNCherenkovElectrons>>hcerPROT1(100,0,4)",cutPROT2 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcerPROT1");
   if(h1){
      //h1->SetFillStyle(3002);
      //h1->SetFillColor(1);
      h1->SetLineColor(kRed);
      h1->Draw("same");
      leg3->AddEntry(h1," (PROT) + Cherenkov TDC","lf");
   }
   leg3->Draw();

   //------------------------------------------------------------
   c->cd(4);

   TPad*    upperPad = new TPad("upperPad", "upperPad", 
         .005, .305, .995, .995);
   TPad*    lowerPad = new TPad("lowerPad", "lowerPad", 
         .005, .005, .995, .3);
   upperPad->Draw();                             
   lowerPad->Draw(); 

   upperPad->cd() ;
   //gPad->SetLogy(true);
   TLegend * leg4 = new TLegend(0.6,0.6,0.9,0.9);
   leg4->SetHeader("Cuts");

   t->Draw("fMomentum.fE>>hEnergy(100,0,2500)",cut0 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hEnergy");
   if(h1){
      h1->SetTitle("Energy");
      h1->GetXaxis()->SetTitle("MeV");
      h1->Draw();
      leg4->AddEntry(h1,"BETA2 trigger","l");
   }

   t->Draw("fMomentum.fE>>hEnergy1(100,0,2500)",cut1 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hEnergy1");
   if(h1){
      h1->SetFillStyle(3004);
      h1->SetFillColor(1);
      h1->Draw("same");
      leg4->AddEntry(h1,"+ good block","lf");
   }

   t->Draw("fMomentum.fE>>hEnergy2(100,0,2500)",cut2 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hEnergy2");
   if(h1){
      h1->SetFillStyle(3005);
      h1->SetFillColor(2);
      h1->Draw("same");
      leg4->AddEntry(h1,"+ Cherenkov TDC","lf");
   }

   t->Draw("fMomentum.fE>>hEnergy3(100,0,2500)",cut3 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hEnergy3");
   if(h1){
      h1->SetFillStyle(3002);
      h1->SetFillColor(4);
      h1->Draw("same");
      leg4->AddEntry(h1,"+ Cherenkov ADC","lf");
   }
   leg4->Draw();

   //-----------------------------------------------------
   lowerPad->cd() ;
   gPad->SetLogy(true);

   TH1F * h2 = 0;
   TH1F * h3 = 0;
   t->Draw("fCluster.fClusterTime1>>hBCTime1(200,-100,100)",cut1 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hBCTime1");
   if(h1){
      //h1->SetFillStyle(3002);
      //h1->SetFillColor(1);
      h1->Draw();
      //leg4->AddEntry(h1,"+ Cherenkov ADC","lf");
   }
   t->Draw("fCluster.fClusterTime2>>hBCTime2(200,-100,100)",cut1 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hBCTime2");
   if(h1){
      h1->SetLineColor(4);
      //h1->SetFillStyle(3002);
      //h1->SetFillColor(1);
      h1->Draw("same");
      //leg4->AddEntry(h1,"+ Cherenkov ADC","lf");
   }

   //h2 = (TH1F*)gROOT->FindObject("hEnergy2");
   //h1 = (TH1F*)gROOT->FindObject("hEnergy3");
   //if( h1 && h2) {
   //   h3 = new TH1F(*h2);
   //   h3->Add(h1,-1.0);
   //   h3->Draw();
   //}

   //-----------------------------------------------------

   c->cd(5);
   gPad->SetLogy(true);
   TLegend * leg5 = new TLegend(0.6,0.6,0.9,0.9);

   t->Draw("fCherenkovTDC>>hcerTDC(100,-1000,1000)",cut1 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcerTDC");
   if(h1){
      h1->SetTitle("Cherenkov TDC");
      h1->GetXaxis()->SetTitle("TDC Channels");
      h1->Draw();
      leg5->AddEntry(h1,"BETA2 + good block","l");
   }
   
   t->Draw("fCherenkovTDC>>hcerTDCRCS1(100,-1000,1000)",cutRCS1 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcerTDCRCS1");
   if(h1){
      h1->SetTitle("Cherenkov TDC");
      h1->GetXaxis()->SetTitle("TDC Channels");
      h1->SetLineColor(kRed);
      h1->Draw("same");
      leg5->AddEntry(h1,"(RCS) BETA2 + good block","l");
   }

   t->Draw("fCherenkovTDC>>hcerTDCPROT1(100,-500,500)",cutPROT1 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcerTDCPROT1");
   if(h1){
      h1->SetTitle("Cherenkov TDC");
      h1->GetXaxis()->SetTitle("TDC Channels");
      h1->SetLineColor(kBlue);
      h1->Draw("same");
      leg5->AddEntry(h1,"(PROT) BETA2 + good block","l");
   }

   t->Draw("fCherenkovTDC>>hcerTDC6(100,-1000,1000)",cut6 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcerTDC6");
   if(h1){
      h1->SetTitle("Cherenkov TDC");
      h1->GetXaxis()->SetTitle("TDC Channels");
      h1->SetFillStyle(3002);
      h1->SetFillColor(1);
      h1->Draw("same");
      leg5->AddEntry(h1," + CER 1 Track Window ADC","lf");
   }
   
   t->Draw("fCherenkovTDC>>hcerTDC7(100,-1000,1000)",cut7 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hcerTDC7");
   if(h1){
      h1->SetTitle("Cherenkov TDC");
      h1->GetXaxis()->SetTitle("TDC Channels");
      h1->SetFillStyle(3002);
      h1->SetFillColor(kRed);
      h1->SetLineColor(kRed);
      h1->Draw("same");
      leg5->AddEntry(h1," + CER 2 Track Window ADC","lf");
   }

   leg5->Draw();
   
   //-----------------------------------------------------
   c->cd(6);
   gPad->SetLogy(true);
   TLegend * leg6 = new TLegend(0.6,0.6,0.9,0.9);

   t->Draw("fMomentum.fE>>hEnergyPROT2(100,0,2500)",cutPROT2 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hEnergyPROT2");
   if(h1){
      h1->SetTitle("Energy");
      h1->GetXaxis()->SetTitle("MeV");
      h1->SetLineColor(kRed);
      h1->Draw();
      leg6->AddEntry(h1,"(PROT)","l");
   }

   t->Draw("fMomentum.fE>>hEnergyRCS2(100,0,2500)",cutRCS2 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hEnergyRCS2");
   if(h1){
      h1->SetTitle("Energy");
      h1->GetXaxis()->SetTitle("MeV");
      h1->SetLineColor(kBlue);
      h1->Draw("same");
      leg6->AddEntry(h1,"(RCS)","l");
   }

   t->Draw("fMomentum.fE>>hEnergyPROT3(100,0,2500)",cutPROT3 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hEnergyPROT3");
   if(h1){
      h1->SetTitle("Energy");
      h1->GetXaxis()->SetTitle("MeV");
      h1->SetLineColor(kRed-9);
      h1->Draw("same");
      leg6->AddEntry(h1,"(PROT) + CER ADC Window","l");
   }

   t->Draw("fMomentum.fE>>hEnergyRCS3(100,0,2500)",cutRCS3 ,"goff");
   h1 = (TH1F*)gROOT->FindObject("hEnergyRCS3");
   if(h1){
      h1->SetTitle("Energy");
      h1->GetXaxis()->SetTitle("MeV");
      h1->SetLineColor(kBlue-9);
      h1->Draw("same");
      leg6->AddEntry(h1,"(RCS) + CER ADC Window","l");
   }

   leg6->Draw();
   //-----------------------------------------------------

   //t->StartViewer();

   c->SaveAs(Form("plots/%d/track_cuts1.png",runnumber));
   c->SaveAs(Form("plots/%d/track_cuts1.pdf",runnumber));

   return(0);









   Int_t nEvents = t->GetEntries();
   for(Int_t iEvent = 0;iEvent < nEvents ; iEvent++){

      t->GetEntry(iEvent);

      //events->fTree->GetEntryWithIndex(trig->fRunNumber,trig->fEventNumber);
      //std::cout << "Event:       " << trig->fEventNumber << std::endl;
      //std::cout << "  beamEvent: " << events->BEAM->fEventNumber << std::endl;
      if( TMath::Abs( traj->fPhi ) < 0.4 )
         if( trig->IsBETA2Event() )
            if( TMath::Abs(traj->fCherenkovTDC) < 8 )
               if( traj->fIsGood )
                  //if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 2.0 )
               {
                  dis->fHelicity = traj->fHelicity;
                  dis->fx        = traj->GetBjorkenX();
                  dis->fW        = TMath::Sqrt(traj->GetInvariantMassSquared()/1.0e6);
                  dis->fQ2       = traj->GetQSquared()/1.0e6;

                  //             if(dis->fW > 1.0 ) {
                  //                if( traj->fEnergy > 900 ){
                  //                   asym1->CountEvent();
                  //                   if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
                  //                      asym3->CountEvent();
                  //                }
                  //                if( traj->fEnergy > 1300 ) {
                  //                   asym2->CountEvent();
                  //                   if( traj->fNCherenkovElectrons > 0.5 && traj->fNCherenkovElectrons < 1.5 )
                  //                      asym4->CountEvent();
                  //                }
               }

   }


   return(0);
}
