#include "InSANE/Math.h"
using namespace nucdb;

void d2_merge_test() {

   Manager * dbman = Manager::GetManager(1);
   Experiment * testexp = dbman->GetExperiment("testSANE");

   auto meas = testexp->GetMeasurement("lowQ2 d2p integrand");
   meas->SortBy("x");

   //meas->Print("data");

   TCanvas*     c  = new TCanvas();
   TMultiGraph* mg = new TMultiGraph();

   auto gr1 = meas->BuildGraph("x");
   //mg->Add(gr1,"ep");

   auto gr = meas->BuildSystematicErrorBand("x",-0.01);
   //mg->Add(gr,"e3");

   BinnedVariable W_bin1("W","W");
   BinnedVariable Q2_bin1("Qsquared","Q^{2}",2.5,1.0);
   BinnedVariable x_bin1("x","x");

   x_bin1.SetLimits(0.10,0.95);
   W_bin1.SetLimits(1.6,5.0);

   double Q2_cutoff = 3.5;

   // Low Q2 bin
   Q2_bin1.SetLimits(2.0,Q2_cutoff);
   auto meas2 = meas->NewMeasurementWithFilter(&Q2_bin1)
                    ->NewMeasurementWithFilter(&W_bin1)
                    ->NewMeasurementWithFilter(&x_bin1);
   meas2->MergeDataPoints(4,"x",true);
   auto gr2 = meas2->BuildGraph("x");
   gr2->SetLineColor(2);
   gr2->SetMarkerColor(2);
   mg->Add(gr2,"ep");
   auto gr2_stat = new TGraph(gr2->GetN(), gr2->GetX(), gr2->GetEY());
   auto gr2_syst = meas2->BuildSystematicErrorBand("x",-0.1);
   auto gr2_syst2 = meas2->BuildSystematicErrorBand("x",0.0);
   gr2_syst->SetLineColor(2);
   gr2_syst->SetMarkerColor(2);
   gr2_syst->SetFillColor(2);
   mg->Add(gr2_syst,"e3");
   auto Npoints = gr2->GetN();
   double x0,y0; 
   double x1,y1; 
   gr2->GetPoint(0,x0,y0);
   gr2->GetPoint(Npoints-2,x1,y1);
   std::cout << "Integral("<< x0 << ", " << x1 << ") : "
     << insane::integrate::simple([&](double xx){ return gr2->Eval(xx); }, x0,x1) << " +- " 
     << TMath::Sqrt(insane::math::simple_quad_sum([&](double xx){ return gr2_stat->Eval(xx)*gr2_stat->Eval(xx); }, x0, x1, int(gr2_syst2->GetN()-2)) ) << " +- "
     << insane::integrate::simple([&](double xx){ return gr2_syst2->Eval(xx); }, x0,x1)/(gr2_syst2->GetN()-2) << std::endl;
   std::cout << "<Q2> = " << meas2->GetBinnedVariableMean("Qsquared") << std::endl;
   std::cout << "Q2_max = " << meas2->GetBinnedVariableMax("Qsquared") << std::endl;

   // High Q2 bin
   Q2_bin1.SetLimits(Q2_cutoff,6.5);
   auto meas3 = meas->NewMeasurementWithFilter(&Q2_bin1)
                    ->NewMeasurementWithFilter(&W_bin1)
                    ->NewMeasurementWithFilter(&x_bin1);
   meas3->MergeDataPoints(3,"x",true);
   auto gr3 = meas3->BuildGraph("x");
   gr3->SetLineColor(4);
   gr3->SetMarkerColor(4);
   mg->Add(gr3,"ep");
   auto gr3_stat = new TGraph(gr3->GetN(), gr3->GetX(), gr3->GetEY());
   auto gr3_syst = meas3->BuildSystematicErrorBand("x",-0.1);
   auto gr3_syst2 = meas3->BuildSystematicErrorBand("x",0.0);
   gr3_syst->SetLineColor(4);
   gr3_syst->SetMarkerColor(4);
   gr3_syst->SetFillColor(4);
   mg->Add(gr3_syst,"e3");
   Npoints = gr3->GetN();
   gr3->GetPoint(0,x0,y0);
   gr3->GetPoint(Npoints-1,x1,y1);
   std::cout << "Integral("<< x0 << ", " << x1 << ") : " 
     << insane::integrate::simple([&](double xx){ return gr3->Eval(xx); }, x0,x1) << " +- "
     << TMath::Sqrt(insane::math::simple_quad_sum([&](double xx){ return gr3_stat->Eval(xx)*gr3_stat->Eval(xx); }, x0,x1,(gr3_syst2->GetN()-1))) << " +- "
     << insane::integrate::simple([&](double xx){ return gr3_syst2->Eval(xx); }, x0,x1)/(gr3_syst2->GetN()-1) << std::endl;
   std::cout << "<Q2> = " << meas3->GetBinnedVariableMean("Qsquared") << std::endl;
   std::cout << "Q2_max = " << meas3->GetBinnedVariableMax("Qsquared") << std::endl;

   mg->Draw("a");

   c->SaveAs("results/d2_merge_test.pdf");


   meas3->SetNameTitle("meas3","meas3");
   meas2->SetNameTitle("meas2","meas2");
   testexp->AddMeasurement(meas2);
   testexp->AddMeasurement(meas3);
   dbman->SaveExperiment(testexp);


}
