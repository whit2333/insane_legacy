Int_t d2p_vs_cut(){

   ifstream ifile("d2p_vs_cut.txt");

   std::vector<double> num;
   std::vector<double> zero;
   std::vector<double> d2_exp;
   std::vector<double> ed2_exp;
   std::vector<double> d2_exp2;
   std::vector<double> ed2_exp2;
   std::vector<double> d2_lowx;
   std::vector<double> d2_nach;
   std::vector<double> ed2_nach;

   double col[10];
   while(ifile.good()) {
      // 8 columns
      for(int i=0;i<8;i++) {
         ifile >> col[i];
      }
      if(ifile.eof()) break;

      num.push_back(col[0]);
      d2_exp.push_back(col[1]);
      ed2_exp.push_back(col[2]);
      d2_exp2.push_back(col[3]);
      ed2_exp2.push_back(col[4]);
      d2_lowx.push_back(col[5]);
      d2_nach.push_back(col[6]);
      ed2_nach.push_back(col[7]);
      zero.push_back(0.0);
   }

   TCanvas * c = new TCanvas();
   TLegend * leg = new TLegend(0.6,0.4,0.9,0.6);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   //leg->SetBorderSize(0);

   TGraphErrors * gr = 0;

   TMultiGraph * mg = new TMultiGraph();

   gr = new TGraphErrors(num.size(),&num[0],&d2_exp[0],&zero[0],&ed2_exp[0]);
   gr->SetMarkerStyle(20);
   mg->Add(gr,"p");
   leg->AddEntry(gr,"exp","p");

   gr = new TGraphErrors(num.size(),&num[0],&d2_nach[0],&zero[0],&ed2_nach[0]);
   gr->SetMarkerStyle(20);
   gr->SetMarkerColor(4);
   gr->SetLineColor(4);
   mg->Add(gr,"p");
   leg->AddEntry(gr,"Nachtmann","p");

   gr = new TGraphErrors(num.size(),&num[0],&d2_exp2[0],&zero[0],&ed2_exp2[0]);
   gr->SetMarkerStyle(20);
   gr->SetMarkerColor(2);
   gr->SetLineColor(2);
   mg->Add(gr,"p");
   leg->AddEntry(gr,"model extraction","p");

   gr = new TGraphErrors(num.size(),&num[0],&d2_lowx[0],&zero[0],&zero[0]);
   gr->SetMarkerStyle(21);
   gr->SetMarkerColor(6);
   gr->SetLineColor(6);
   mg->Add(gr,"p");
   leg->AddEntry(gr,"low x (0.01-0.35)","p");


   mg->Draw("a");
   leg->Draw();

   c->SaveAs("results/d2p_vs_cut_2.png");

   return 0;

}
