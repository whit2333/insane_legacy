#include "asym/sane_data_bins.cxx"
//int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

Int_t d2_measure_3( Int_t aNumber = 8609 ) {

   Int_t paraFileVersion = aNumber;
   Int_t perpFileVersion = aNumber;
   Int_t para47Version   = aNumber;
   Int_t perp47Version   = aNumber;

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 
   InSANEFormFactors     * ffs  = fman->CreateFFs(4);

   sane_data_bins();

   // ----------------------------------------------------------------
   //
   //TFile * fcombined = new TFile(Form("data/g1g2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   //TList * fg1_comb_list = (TList*)gROOT->FindObject(Form("g1asym-combined-W-%d",0));
   //TList * fg2_comb_list = (TList*)gROOT->FindObject(Form("g2asym-combined-W-%d",0));
   TFile * fcombined = new TFile(Form("data/g1g2_combined_%d.root",paraFileVersion),"UPDATE");
   TList * fg1_comb_list = (TList*)gROOT->FindObject("g1_combined_W");
   TList * fg2_comb_list = (TList*)gROOT->FindObject("g2_combined_W");
   if(!fg1_comb_list) {
      std::cout << " -11 " << std::endl;
      return -11;
   }
   if(!fg2_comb_list){
      std::cout << " -12 " << std::endl;
      return -12;
   }

   // ----------------------------------------------------------------
   // Model used to calculate  missing regions
   InSANEPolarizedStructureFunctions * model_SFs = 0;
   TString    sfName    = "stat2015";
   model_SFs            = fman->CreatePolSFs(13);

   std::vector<Double_t>   bin_d2p_value    ;
   std::vector<Double_t>   bin_wd2p_exp     ;
   std::vector<Double_t>   bin_d2p_exp      ;
   std::vector<Double_t>   bin_err_d2p_exp  ;
   std::vector<Double_t>   bin_syserr_d2p_exp  ;
   std::vector<Double_t>   bin_d2p_exp2     ;
   std::vector<Double_t>   bin_wd2p_exp2    ;
   std::vector<Double_t>   bin_err_d2p_exp2 ;
   std::vector<Double_t>   bin_syserr_d2p_exp2 ;
   std::vector<Double_t>   bin_d2p_nach     ;
   std::vector<Double_t>   bin_err_d2p_nach ;
   std::vector<Double_t>   bin_syserr_d2p_nach ;
   std::vector<Int_t>      bin_iXbins;
   std::vector<Double_t>   bin_Q2 ;
   std::vector<Double_t>   bin_Q2min;
   std::vector<Double_t>   bin_Q2max;
   std::vector<Int_t>      bin_iQ2 ;
   std::vector<Double_t>   bin_mean_Q2 ;

   std::vector<Double_t> xLimits_0;
   std::vector<Double_t> xLimits_1;

   // ------------------------------------------
   // Loop over Q2 bins
   for(int jj = 0;((jj<fg1_comb_list->GetEntries())&&(jj<15));jj++) {

      InSANEMeasuredAsymmetry         * fg1_asym        = (InSANEMeasuredAsymmetry * )(fg1_comb_list->At(jj));
      InSANEMeasuredAsymmetry         * fg2_asym        = (InSANEMeasuredAsymmetry * )(fg2_comb_list->At(jj));

      // kine is the same for g1 and g2
      InSANEAveragedKinematics1D      * kine            = &fg1_asym->fAvgKineVsW;

      TH1F * fg1   = &fg1_asym->fAsymmetryVsW;
      TH1F * fg2   = &fg2_asym->fAsymmetryVsW;

      //fg1_asym->MaskIntersection(fg1_asym->fMask_W,fg2_asym->fMask_W);
      TH1F * mask  = &fg1_asym->fMask_W;

      int lo_W_bin = mask->FindFirstBinAbove(0.1);
      if( lo_W_bin == -1 ) {
        std::cout << "Could not find bin above 0.1\n";
      }
      int hi_W_bin = mask->FindLastBinAbove(0.1);
      if( hi_W_bin == -1 ) {
        std::cout << "Could not find bin above 0.1\n";
      }
      //double W1 = mask->GetBinLowEdge(lo_W_bin);
      //double W2 = mask->GetBinLowEdge(hi_W_bin+1);
      double Q2_1 = kine->fQ2.GetBinContent(lo_W_bin);
      double Q2_2 = kine->fQ2.GetBinContent(hi_W_bin);
      double W1   = kine->fW.GetBinContent(lo_W_bin);
      double W2   = kine->fW.GetBinContent(hi_W_bin);
      double x1   = InSANE::Kine::xBjorken_WQ2(W1,Q2_1);
      double x2   = InSANE::Kine::xBjorken_WQ2(W2,Q2_2);
      std::cout << " ===== " << jj << "=====\n";
      std::cout << lo_W_bin << "  W1: " << W1 << "  Q2_1: " << Q2_1 << "  x1: " << x1 << '\n';
      std::cout << hi_W_bin << "  W2: " << W2 << "  Q2_2: " << Q2_2 << "  x2: " << x2 << '\n';
      // 
      if(x2<x1) {
        xLimits_0.push_back(x2);
        xLimits_1.push_back(x1);
      } else {
        xLimits_0.push_back(x1);
        xLimits_1.push_back(x2);
      }

      bin_Q2.push_back(   fg1_asym->GetQ2());
      bin_Q2min.push_back(fg1_asym->fQ2Min);
      bin_Q2max.push_back(fg1_asym->fQ2Max);

      Double_t d2p_value       = 0.0;
      Double_t wd2p_exp        = 0.0;
      Double_t d2p_exp         = 0.0;
      Double_t err_d2p_exp     = 0.0;
      Double_t syserr_d2p_exp  = 0.0;
      Double_t d2p_exp2        = 0.0;
      Double_t wd2p_exp2       = 0.0;
      Double_t err_d2p_exp2    = 0.0;
      Double_t syserr_d2p_exp2 = 0.0;
      Double_t d2p_nach        = 0.0;
      Double_t err_d2p_nach    = 0.0;
      Double_t syserr_d2p_nach = 0.0;
      Int_t    iXbins          = 0;

      Int_t   xBinmax = fg1->GetNbinsX();
      Int_t   yBinmax = fg1->GetNbinsY();
      Int_t   zBinmax = fg1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      Int_t iQ2 = 0;
      Int_t mean_Q2 = 0.0;

      // ----------------------------------------------------------
      // 
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fg1->GetBin(i,j,k);

               Double_t g1      = fg1->GetBinContent(bin);
               Double_t g2      = fg2->GetBinContent(bin);
               Double_t eg1     = fg1->GetBinError(bin);
               Double_t eg2     = fg2->GetBinError(bin);
               Double_t eg1Syst = fg1_asym->fSystErrVsW.GetBinError(bin);
               Double_t eg2Syst = fg2_asym->fSystErrVsW.GetBinError(bin);

               if(  TMath::IsNaN(eg1Syst) || TMath::IsNaN(eg2Syst) ) {
                  eg1Syst  = 0.0;
                  eg2Syst  = 0.0;
               }

               Double_t W_low = kine->fW.GetBinLowEdge(bin);
               Double_t dW    = kine->fW.GetBinWidth(bin);
               Double_t W_hi  = W_low+dW;

               Double_t W     = kine->fW.GetBinContent(bin);
               Double_t x     = kine->fx.GetBinContent(bin);
               Double_t Q2    = kine->fQ2.GetBinContent(bin);
               Double_t M     = (M_p/GeV);
               Double_t xi    = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));

               Double_t x_hi  = InSANE::Kine::xBjorken_WQ2(W_low,Q2);
               Double_t x_low = InSANE::Kine::xBjorken_WQ2(W_low+dW,Q2);
               Double_t dx = x_hi - x_low;

               // -------------------------------------
               // Nachtmann moments
               //Double_t dx    = kine->fx.GetBinWidth(bin);
               // for n = 3
               Double_t g1_coeff_nach       = (xi*xi/x)*(xi*xi/x)*(x/xi);
               Double_t g2_coeff_nach       = (xi*xi/x)*(xi*xi/x)*((3.0*x*x)/(2.0*xi*xi) - (3.0*M*M*x*x)/(4.0*Q2));
               Double_t d2p_nach_int        = (g1_coeff_nach*g1 + g2_coeff_nach*g2);
               Double_t err_d2p_nach_int    = (g1_coeff_nach*eg1 + g2_coeff_nach*eg2);
               Double_t syserr_d2p_nach_int = (g1_coeff_nach*eg1Syst + g2_coeff_nach*eg2Syst)/2.0;

               Double_t x2g1     = x*x*g1;
               Double_t x2g2     = x*x*g2;
               Double_t ex2g1    = x*x*eg1;
               Double_t ex2g2    = x*x*eg2;
               Double_t ex2g1Syst = x*x*eg1Syst;
               Double_t ex2g2Syst = x*x*eg2Syst;

               Double_t d2_int_exp     = (2.0*x2g1 + 3.0*x2g2);
               Double_t ed2_int_exp    = TMath::Sqrt(TMath::Power(2.0*ex2g1,2.0) + TMath::Power(3.0*ex2g2,2.0));
               Double_t sysed2_int_exp = TMath::Sqrt(TMath::Power(2.0*ex2g1Syst,2.0) + TMath::Power(3.0*ex2g2Syst,2.0));

               Double_t g2WW_model      = model_SFs->x2g2pWW(x,Q2);
               Double_t g1_model        = model_SFs->g1p_Twist2(x,Q2);
               Double_t g1_twist3_model = model_SFs->g1p_Twist3(x,Q2);
               Double_t eg2WW_model     = ex2g1/(x*x);
               Double_t eg1_model       = ex2g1/(x*x);

               Double_t x2g2bar         = (x2g2 - x*x*(g2WW_model))*3.0;
               Double_t x2g1bar         =  x2g1 - x*x*(g1_model+g1_twist3_model);

               Double_t ex2g2bar        = TMath::Sqrt(ex2g2*ex2g2 )*3.0;
               Double_t sysex2g2bar     = TMath::Sqrt(ex2g2Syst*ex2g2Syst + TMath::Power(x*x*eg2WW_model,2.0))*3.0;

               Double_t xg2bar          = 0;
               Double_t exg2bar         = 0;
               Double_t ex2g1bar        = 0.0;

               if(  TMath::IsNaN(x2g1) || TMath::IsNaN(x2g2)  ||  TMath::IsNaN(ex2g1) || TMath::IsNaN(ex2g2) ) {
                  x2g1bar  = 0.0;
                  x2g2bar  = 0.0;
                  xg2bar   = 0.0;
                  ex2g1bar = 0.0;
                  ex2g2bar = 0.0;
                  exg2bar  = 0.0;
               }

               if(jj<15) {

                  if(x > xLimits_0[jj] && x < xLimits_1[jj] ) {

                     if( (!TMath::IsNaN(d2_int_exp))  ){

                        if(  ( ed2_int_exp > 2.5)  ){
                           std::cout << "error: W " << x << "  "  << ex2g2bar << std::endl;
                           std::cout << " low : " << xLimits_0[jj] << " high : " << xLimits_1[jj] << std::endl;
                        } else {

                           //if(ed2_int_exp == 0.0 ) ed2_int_exp = 9999.0;
                           //std::cout << Q2 << std::endl;

                           d2p_exp          += (d2_int_exp*dx);
                           err_d2p_exp      += TMath::Power(ed2_int_exp*dx,2.0);
                           syserr_d2p_exp   += TMath::Power(sysed2_int_exp*dx,2.0);

                           d2p_exp2         += (x2g2bar*dx);
                           err_d2p_exp2     += TMath::Power(ex2g2bar*dx,2.0);
                           syserr_d2p_exp2  += TMath::Power(sysex2g2bar*dx,2.0);

                           mean_Q2 += Q2;
                           iQ2++;

                           d2p_nach        += (d2p_nach_int*dx);
                           err_d2p_nach    += TMath::Power(err_d2p_nach_int*dx,2.0);
                           syserr_d2p_nach += TMath::Power(syserr_d2p_nach_int*dx,2.0);

                           iXbins++;
                           //std::cout << "error: x " << x << "  "  << ex2g2bar << std::endl;
                        }
                     }
                  }
               }

            } // bin loop
         }
      }

      // -------------------------------

      mean_Q2 = mean_Q2/double(iQ2);
      bin_mean_Q2.push_back(mean_Q2);

      d2p_nach        = 2.0*d2p_nach;
      err_d2p_nach    = 4.0*err_d2p_nach;
      syserr_d2p_nach = 4.0*syserr_d2p_nach;

      bin_d2p_value.push_back    ( d2p_value    );
      bin_wd2p_exp.push_back     ( wd2p_exp     );
      bin_d2p_exp.push_back      ( d2p_exp      );
      bin_err_d2p_exp.push_back  ( err_d2p_exp  );
      bin_syserr_d2p_exp.push_back  ( syserr_d2p_exp  );
      bin_d2p_exp2.push_back     ( d2p_exp2     );
      bin_wd2p_exp2.push_back    ( wd2p_exp2    );
      bin_err_d2p_exp2.push_back ( err_d2p_exp2 );
      bin_syserr_d2p_exp2.push_back ( syserr_d2p_exp2 );
      bin_d2p_nach.push_back     ( d2p_nach     );
      bin_err_d2p_nach.push_back ( err_d2p_nach );
      bin_syserr_d2p_nach.push_back ( syserr_d2p_nach );
      bin_iXbins.push_back       ( iXbins);

   }


   // -----------------------------------------------------------------------
   //
   std::ofstream ofile("d2/d2p_values.txt", std::ofstream::app);
   ofile <<      "\t" << "i";
   ofile      << "\t" << "Q2";
   ofile      << "\t" << "x_low";
   ofile      << "\t" << "x_high";
   ofile      << "\t" << "d2p_exp " << "\t" << "stat"<< "\t" << "syst";
   ofile      << "\t" << "d2p_exp2" << "\t" << "stat"<< "\t" << "syst";
   ofile      << "\t" << "d2p_lowx" ;
   ofile      << "\t" << "d2p_highx" ;
   ofile      << "\t" << "d2p_nach"  << "\t" << "stat"<< "\t" << "syst";
   ofile << std::endl;

   // -----------------------------------------------------------------------
   //
   NucDBManager * dbman = NucDBManager::GetManager(1);
   NucDBExperiment * exp = dbman->GetExperiment("SANE");
   if(!exp) exp = new NucDBExperiment("SANE","SANE");

   // ---------------------------------------
   NucDBMeasurement * d2pWW_meas = exp->GetMeasurement("d2pWW measured");
   if( !d2pWW_meas ) {
      d2pWW_meas = new NucDBMeasurement("d2pWW measured","d2pWW measured");
      exp->AddMeasurement(d2pWW_meas);
   }
   d2pWW_meas->ClearDataPoints();
   d2pWW_meas->SetExperimentName("Using g1 model");

   // ---------------------------------------
   NucDBMeasurement * d2p_meas = exp->GetMeasurement("d2p measured");
   if( !d2p_meas ) {
      d2p_meas = new NucDBMeasurement("d2p measured","d2p measured");
      exp->AddMeasurement(d2p_meas);
   }
   d2p_meas->ClearDataPoints();
   d2p_meas->SetExperimentName("#bar{d}_{2} BETA");

   // ---------------------------------------
   NucDBMeasurement * d2pbar_nachmom = exp->GetMeasurement("d2pbar Nachtmann");
   if( !d2pbar_nachmom ) {
      d2pbar_nachmom = new NucDBMeasurement("d2pbar Nachtmann","d2pbar Nachtmann");
      exp->AddMeasurement(d2pbar_nachmom);
   }
   d2pbar_nachmom->ClearDataPoints();
   d2pbar_nachmom->SetExperimentName("BETA inelastic Nachtmann");

   // ---------------------------------------
   NucDBMeasurement * d2p_nachmom = exp->GetMeasurement("d2p Nachtmann");
   if( !d2p_nachmom ) {
      d2p_nachmom = new NucDBMeasurement("d2p Nachtmann","d2p Nachtmann");
      exp->AddMeasurement(d2p_nachmom);
   }
   d2p_nachmom->ClearDataPoints();
   d2p_nachmom->SetExperimentName("BETA Nachtmann");

   // ---------------------------------------
   NucDBMeasurement * d2p_lowx = exp->GetMeasurement("d2p lowx");
   if( !d2p_lowx ) {
      d2p_lowx = new NucDBMeasurement("d2p lowx","d2p lowx");
      exp->AddMeasurement(d2p_lowx);
   }
   d2p_lowx->ClearDataPoints();
   d2p_lowx->SetExperimentName("BETA low x");

   // ---------------------------------------
   NucDBMeasurement * d2p_highx = exp->GetMeasurement("d2p highx");
   if( !d2p_highx ) {
      d2p_highx = new NucDBMeasurement("d2p highx","d2p highx");
      exp->AddMeasurement(d2p_highx);
   }
   d2p_highx->ClearDataPoints();
   d2p_highx->SetExperimentName("BETA high x");

   // ---------------------------------------
   NucDBMeasurement * d2p_bar = exp->GetMeasurement("d2pbar");
   if( !d2p_bar ) {
      d2p_bar = new NucDBMeasurement("d2pbar","d2pbar");
      exp->AddMeasurement(d2p_bar);
   }
   d2p_bar->ClearDataPoints();
   d2p_bar->SetExperimentName("d_{2} BETA");

   // ---------------------------------------
   NucDBMeasurement * d2p_total = exp->GetMeasurement("d2p inelastic");
   if( !d2p_total ) {
      d2p_total = new NucDBMeasurement("d2p inelastic","d2p inelastic");
      exp->AddMeasurement(d2p_total);
   }
   d2p_total->ClearDataPoints();
   d2p_total->SetExperimentName("BETA inelastic CN");

   // ---------------------------------------
   NucDBMeasurement * d2p_all = exp->GetMeasurement("d2p all");
   if( !d2p_all ) {
      d2p_all = new NucDBMeasurement("d2p all","d2p all");
      exp->AddMeasurement(d2p_all);
   }
   d2p_all->ClearDataPoints();
   d2p_all->SetExperimentName("d_{2} BETA");


   NucDBDataPoint         datapoint; 
   NucDBBinnedVariable  * Q2Bin = new NucDBBinnedVariable("Qsquared","Q^{2}");
   NucDBBinnedVariable  * xBin  = new NucDBBinnedVariable("x","");
   datapoint.AddBinVariable(Q2Bin);
   datapoint.AddBinVariable(xBin);

   for(int i=0; i < bin_d2p_value.size() && i<fSANENBins; i++) {

      if( bin_iXbins[i] == 0 ) bin_iXbins[i] = 1;

      Double_t d2p_lowx_model  = model_SFs->d2p_tilde(bin_Q2[i],0.01,xLimits_0[i]);
      Double_t d2p_highx_model = model_SFs->d2p_tilde(bin_Q2[i],xLimits_1[i],0.99);
      Double_t M23_lowx_model  = model_SFs->M2n_p(3,bin_Q2[i],0.01,xLimits_0[i]);
      Double_t M23_highx_model = model_SFs->M2n_p(3,bin_Q2[i],xLimits_1[i],0.99);
      Double_t d2p_tilde_lowx_model    = M23_lowx_model *2.0;
      Double_t d2p_tilde_highx_model   = M23_highx_model*2.0;
      Double_t d2p_tilde_elastic_model =  ffs->d2p_el_Nachtmann(bin_Q2[i])*2.0;

      bin_err_d2p_exp[i]     = TMath::Sqrt(bin_err_d2p_exp[i]  );
      bin_err_d2p_exp2[i]    = TMath::Sqrt(bin_err_d2p_exp2[i] );
      bin_err_d2p_nach[i]    = TMath::Sqrt(bin_err_d2p_nach[i] );
      bin_syserr_d2p_exp[i]  = TMath::Sqrt(bin_syserr_d2p_exp[i]  /double(bin_iXbins[i]));
      bin_syserr_d2p_exp2[i] = TMath::Sqrt(bin_syserr_d2p_exp2[i] /double(bin_iXbins[i]));
      bin_syserr_d2p_nach[i] = TMath::Sqrt(bin_syserr_d2p_nach[i] /double(bin_iXbins[i]));

      // Sane measured
      xBin->SetLimits(  xLimits_0[i], xLimits_1[i] );
      Q2Bin->SetLimits( bin_Q2min[i], bin_Q2max[i] );
      Q2Bin->SetMean(   bin_Q2[i]);

      NucDBErrorBar err0(  bin_err_d2p_exp[i] );
      NucDBErrorBar err1( bin_err_d2p_exp2[i] );
      NucDBErrorBar err2( bin_err_d2p_nach[i] );

      NucDBErrorBar syserr0(  bin_syserr_d2p_exp[i] );
      NucDBErrorBar syserr1( bin_syserr_d2p_exp2[i] );
      NucDBErrorBar syserr2( bin_syserr_d2p_nach[i] );

      datapoint.SetStatError(err0);
      datapoint.SetSystError(syserr0);
      datapoint.SetValue(bin_d2p_exp[i]);
      datapoint.CalculateTotalError();
      d2p_meas->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(           bin_Q2[i]+0.03  );
      datapoint.SetStatError(   err1            );
      datapoint.SetSystError(   syserr1         );
      datapoint.SetValue(       bin_d2p_exp2[i] );
      datapoint.CalculateTotalError();
      d2pWW_meas->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean         ( bin_Q2[i]+0.06                                    ) ;
      datapoint.SetStatError ( err2                                              ) ;
      datapoint.SetSystError ( syserr2                                           ) ;
      datapoint.SetValue     ( bin_d2p_nach[i]+ d2p_lowx_model + d2p_highx_model ) ;
      datapoint.CalculateTotalError();
      d2pbar_nachmom->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(bin_Q2[i]+0.06);
      datapoint.SetStatError(err2);
      datapoint.SetSystError(syserr2);
      datapoint.SetValue(bin_d2p_nach[i] + d2p_lowx_model + d2p_highx_model + d2p_tilde_elastic_model );
      datapoint.CalculateTotalError();
      d2p_nachmom->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(bin_Q2[i]+0.09);
      datapoint.SetStatError(err1); // find model error on low/high x
      datapoint.SetSystError(syserr1); // find model error on low/high x
      datapoint.SetValue(bin_d2p_exp2[i] + d2p_lowx_model + d2p_lowx_model );
      datapoint.CalculateTotalError();
      d2p_bar->AddDataPoint( new NucDBDataPoint(datapoint) );
      d2p_total->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(bin_Q2[i]-0.03);
      datapoint.SetStatError(err1); // find model error on low/high x
      datapoint.SetSystError(syserr1); // find model error on low/high x
      datapoint.SetValue(bin_d2p_exp2[i] + d2p_lowx_model + d2p_lowx_model + ffs->d2p_el(bin_Q2[i]));
      datapoint.CalculateTotalError();
      d2p_all->AddDataPoint( new NucDBDataPoint(datapoint) );


      //std::cout << "d2p (model)       = " << bin_d2p_value[i] << std::endl;
      //std::cout << "d2p (exp)         = " << bin_d2p_exp[i] << " += " <<  TMath::Sqrt(err_d2p_exp[i])/double(1.0) << std::endl;
      //std::cout << "d2p (exp g2 only) = " << bin_d2p_exp2[i] << " += " << (bin_err_d2p_exp2[i])/double(bin_iXbins[i]) << std::endl;
      //std::cout << "d2p (0.01-0.35)   = " << d2p_lowx << std::endl;
      //std::cout << "d2p (nach)        = " << bin_d2p_nach[i] << " += " <<  (bin_err_d2p_nach[i])/double(bin_iXbins[i]) << std::endl;

      ofile << i << "\t" << bin_Q2[i];
      ofile      << "\t" << xLimits_0[i];
      ofile      << "\t" << xLimits_1[i];
      ofile      << "\t" << bin_d2p_exp[i]  << "\t" << (bin_err_d2p_exp[i]) << "\t" << (bin_syserr_d2p_exp[i])  ;
      ofile      << "\t" << bin_d2p_exp2[i] << "\t" << (bin_err_d2p_exp2[i])<< "\t" << (bin_syserr_d2p_exp2[i]) ;
      ofile      << "\t" << d2p_lowx_model ;
      ofile      << "\t" << d2p_highx_model ;
      ofile      << "\t" << bin_d2p_nach[i]  << "\t" << (bin_err_d2p_nach[i]) << "\t" << (bin_syserr_d2p_nach[i]);
      ofile      << std::endl;
   }

   NucDBErrorBar syserr0( 0.0 );
   datapoint.SetSystError(syserr0);

   Double_t  d2p_kang_lowx_model  = model_SFs->d2p_tilde(1.86,0.01,0.47);
   Double_t d2p_kang_highx_model  = model_SFs->d2p_tilde(1.86,0.87,0.99);
   std::cout << " d2p_kang_lowx  "  << d2p_kang_lowx_model << std::endl;
   std::cout << " d2p_kang_highx " << d2p_kang_highx_model << std::endl;

   // --------------------------------------------------
   // Hoyoung's HMS data
   // -0.0087  +-0.0014  (0.47 < 0.87) , Q2=1.86
   // d2p_kang_lowx  0.00787601
   // d2p_kang_highx -0.000561791

   // --------------
   NucDBMeasurement * d2p_kang_highx = exp->GetMeasurement("d2p kang");
   if( !d2p_kang_highx ) {
      d2p_kang_highx = new NucDBMeasurement("d2p kang","d2p Kang");
      exp->AddMeasurement(d2p_kang_highx);
   }
   d2p_kang_highx->ClearDataPoints();
   d2p_kang_highx->SetExperimentName("SANE HMS");
   xBin->SetLimits(0.87,0.90);
   NucDBErrorBar err(0.00001);
   datapoint.SetStatError(err);
   datapoint.SetValue(d2p_kang_highx_model);
   datapoint.CalculateTotalError();
   d2p_kang_highx->AddDataPoint( new NucDBDataPoint(datapoint) );

   // --------------
   NucDBMeasurement * d2p_kang_lowx = exp->GetMeasurement("d2p lowx kang");
   if( !d2p_kang_lowx ) {
      d2p_kang_lowx = new NucDBMeasurement("d2p lowx kang","d2p lowx kang");
      exp->AddMeasurement(d2p_kang_lowx);
   }
   d2p_kang_lowx->ClearDataPoints();
   d2p_kang_lowx->SetExperimentName("SANE HMS");
   xBin->SetLimits(0.01,0.47);
   Q2Bin->SetValueSize(1.86,0.1);
   err = NucDBErrorBar(0.00001);
   datapoint.SetStatError(err);
   datapoint.SetValue(d2p_kang_lowx_model);
   datapoint.CalculateTotalError();
   d2p_kang_lowx->AddDataPoint( new NucDBDataPoint(datapoint) );

   // --------------
   NucDBMeasurement * d2p_kang = exp->GetMeasurement("d2p kang");
   if( !d2p_kang ) {
      d2p_kang = new NucDBMeasurement("d2p kang","d2p Kang");
      exp->AddMeasurement(d2p_kang);
   }
   d2p_kang->ClearDataPoints();
   d2p_kang->SetExperimentName("#bar{d}_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   err = NucDBErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087);
   datapoint.CalculateTotalError();
   d2p_kang->AddDataPoint( new NucDBDataPoint(datapoint) );

   NucDBMeasurement * d2p_bar_kang = exp->GetMeasurement("d2pbar kang");
   if( !d2p_bar_kang ) {
      d2p_bar_kang = new NucDBMeasurement("d2pbar kang","d2pbar kang");
      exp->AddMeasurement(d2p_bar_kang);
   }
   d2p_bar_kang->ClearDataPoints();
   d2p_bar_kang->SetExperimentName("d_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   err = NucDBErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087+d2p_kang_lowx_model + d2p_kang_highx_model);
   datapoint.CalculateTotalError();
   d2p_bar_kang->AddDataPoint( new NucDBDataPoint(datapoint) );

   NucDBMeasurement * d2p_all_kang = exp->GetMeasurement("d2p all kang");
   if( !d2p_all_kang ) {
      d2p_all_kang = new NucDBMeasurement("d2p all kang","d2p all kang");
      exp->AddMeasurement(d2p_all_kang);
   }
   d2p_all_kang->ClearDataPoints();
   d2p_all_kang->SetExperimentName("d_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   err = NucDBErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087+d2p_kang_lowx_model + d2p_kang_highx_model + ffs->d2p_el_Nachtmann(1.86) );
   datapoint.CalculateTotalError();
   d2p_all_kang->AddDataPoint( new NucDBDataPoint(datapoint) );


   exp->Print();
   dbman->SaveExperiment(exp);

   return 0;
}
