int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

Int_t d2_extraction1(Int_t paraFileVersion = 2405,Int_t perpFileVersion = 2405, 
                    Int_t para47Version = 2405, Int_t perp47Version = 2405,Int_t aNumber=2405){
   const TString weh("d2_extraction()");
   if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
      Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
      return -1;
   }
   sane_data_bins();

   // Combined x2g1 and x2g2 
   TFile * f = new TFile(Form("data/A1A2_combined_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();
   //f->ls();
   TList * fx2g1Combined_list = (TList*)gROOT->FindObject(Form("x2g1-combined-x-%d",0));
   if(!fx2g1Combined_list) return(-1);
   TList * fx2g2Combined_list = (TList*)gROOT->FindObject(Form("x2g2-combined-x-%d",0));
   if(!fx2g2Combined_list) return(-2);

   // Get A1,A2, and F1 combined to use the kinematics ( need to check values of A1 and A2 ....)
   TFile * fileA1A2 = new TFile(Form("data/A1A2_combined_noel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   fileA1A2->cd();
   TList * fA1Combined_list = (TList *) gROOT->FindObject(Form("A1asym-combined-x-%d",0));
   if(!fA1Combined_list) return(-5);
   TList * fA2Combined_list = (TList *) gROOT->FindObject(Form("A2asym-combined-x-%d",0));
   if(!fA2Combined_list) return(-6);
   TList * fF1Combined_list = (TList *) gROOT->FindObject(Form("F1-combined-x-%d",0));
   if(!fF1Combined_list) return(-7);
   // probably dont need F1 but it is just a list of histograms (not asymmetry classes)

   //const char * perpfile = Form("data/binned_asymmetries_perp59_%d.root",perpFileVersion);
   //TFile * f2 = TFile::Open(perpfile,"UPDATE");
   //f2->cd();
   //TList * fPerpAsymmetries = (TList *) gROOT->FindObject(Form("elastic-subtracted-asym_perp59-%d",0));
   //if(!fPerpAsymmetries) return(-4);
   ////fPerpAsymmetries->Print();


   // ----------------------------------------------------------------
   TString sfName = "jam";
   InSANEPolarizedStructureFunctionsFromPDFs * bbSFs = 0;
   //bbSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   //bbSFs->SetPolarizedPDFs(new DNS2005PolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new StatisticalPolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new BBPolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new BBSPolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new AAC08PolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new DSSVPolarizedPDFs());

   Double_t Q20 = 1.0;
   JAMPolarizedPDFs * jamPDFs = new JAMPolarizedPDFs();
   bbSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   bbSFs->SetPolarizedPDFs(jamPDFs);
   //jamPDFs->MakeGrid();
   //jamPDFs->EvolveInit(Q20);
   //jamPDFs->Evolve();

   //Double_t Q20 = 4.0;
   //WPolarizedEvolution<MHKPolarizedPDFs> * MHKPDFs = new WPolarizedEvolution<MHKPolarizedPDFs>();
   //bbSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   //bbSFs->SetPolarizedPDFs(MHKPDFs);
   //MHKPDFs->MakeGrid();
   //MHKPDFs->EvolveInit(Q20);
   //MHKPDFs->Evolve();

   // ------------------------------
   // Functions
   TF1 * xg1p_0 = new TF1("xg1p0", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
                                 0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   xg1p_0->SetParameter(0,5.0);
   xg1p_0->SetLineColor(1);

   TF1 * xg1p_1 = new TF1("xg1p1", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p_Twist234, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p_Twist234");
   xg1p_1->SetParameter(0,1.0);
   xg1p_1->SetLineColor(1);

   TF1 * xg1p_2 = new TF1("xg1p2", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p_Twist2, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p_Twist2");
   xg1p_2->SetParameter(0,1.0);
   xg1p_2->SetLineColor(1);

   TF1 * xg1p_3 = new TF1("xg1p3", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p_Twist3, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p_Twist3");
   xg1p_3->SetParameter(0,1.0);
   xg1p_3->SetLineColor(1);

   TF1 * xg2p_3 = new TF1("xg2p3", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g2pbar, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g2pbar");
   xg2p_3->SetParameter(0,1.0);
   xg2p_3->SetLineColor(1);

   TF1 * xg1p_4 = new TF1("xg1p4", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p_Twist4, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p_Twist4");
   xg1p_4->SetParameter(0,1.0);
   xg1p_4->SetLineColor(1);


   //--------------------------------------
   TList * fd2_list      = new TList();
   TList * fx2g2bar_list = new TList();
   TH1F  * fx2g2bar      = 0;
   TList * fxg2bar_list = new TList();
   TH1F  * fxg2bar      = 0;
   TList * fx2g1bar_list = new TList();
   TH1F  * fx2g1bar      = 0;
   TList * fg2bar_list   = new TList();
   TH1F  * fg2bar        = 0;

   TMultiGraph * mg_x2g2bar = new TMultiGraph();
   TMultiGraph * mg_x2g1bar = new TMultiGraph();
   TMultiGraph * mg_xg2bar  = new TMultiGraph();

   TLegend *leg = new TLegend(0.17, 0.6, 0.35, 0.88);

   Double_t d2p_value = 0.0;
   /// Loop over Q2 bins
   for(int jj = 0;jj<fA1Combined_list->GetEntries();jj++) {

      InSANEMeasuredAsymmetry         * fA1_asym        = (InSANEMeasuredAsymmetry * )(fA1Combined_list->At(jj));
      InSANEMeasuredAsymmetry         * fA2_asym        = (InSANEMeasuredAsymmetry * )(fA2Combined_list->At(jj));
      InSANEAveragedKinematics1D * kine = fA1_asym->fAvgKineVsx;
      // kine is the same for A1 and A2

      TH1F * fx2g1 = (TH1F*) fx2g1Combined_list->At(jj);
      TH1F * fx2g2 = (TH1F*) fx2g2Combined_list->At(jj);
      TH1F * fF1   = (TH1F*) fF1Combined_list->At(jj);

      // Create resulting histograms
      fx2g2bar = new TH1F(*fx2g1);
      fx2g2bar->Reset();
      fx2g2bar_list->Add(fx2g2bar);
      fxg2bar = new TH1F(*fx2g1);
      fxg2bar->Reset();
      fxg2bar_list->Add(fx2g2bar);
      fx2g1bar = new TH1F(*fx2g1);
      fx2g1bar->Reset();
      fx2g1bar_list->Add(fx2g2bar);
      fg2bar = new TH1F(*fx2g1);
      fg2bar->Reset();
      fg2bar_list->Add(fg2bar);

      Int_t   xBinmax = fx2g1->GetNbinsX();
      Int_t   yBinmax = fx2g1->GetNbinsY();
      Int_t   zBinmax = fx2g1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      //std::cout << " Graph " << jj << std::endl;
      // ----------------------------------------------------------
      // Sanitize the y values by removing NaNs
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fx2g1->GetBin(i,j,k);

               Double_t x2g1  = fx2g1->GetBinContent(bin)/2.0;
               Double_t x2g2  = fx2g2->GetBinContent(bin)/3.0;
               Double_t ex2g1 = fx2g1->GetBinError(bin)/2.0;
               Double_t ex2g2 = fx2g2->GetBinError(bin)/3.0;

               Double_t x     = kine->fx->GetBinContent(bin);
               Double_t Q2    = kine->fQ2->GetBinContent(bin);


               Double_t g2WW_model = bbSFs->x2g2pWW(x,Q2);
               Double_t g1_model   = bbSFs->g1p_Twist2(x,Q2);
               Double_t g1_twist3_model   = bbSFs->g1p_Twist3(x,Q2);
               Double_t g2_twist3_model   = bbSFs->g2p_Twist3(x,Q2);
               Double_t eg2WW_model = ex2g1/(x*x);
               Double_t eg1_model   = ex2g1/(x*x);

               //Double_t x2g2bar    = g2_twist3_model*x*x;//(x2g2 - x*x*(g2WW_model))*3.0;
               Double_t x2g2bar    = (x2g2 - x*x*(g2WW_model))*3.0;
               Double_t x2g1bar    = x2g1 - x*x*(g1_model+g1_twist3_model);

               Double_t ex2g2bar    = ex2g2 + x*x*eg2WW_model;
               Double_t ex2g1bar    = ex2g1 + x*x*eg1_model;

               //std::cout << "error: x " << x << "  "  << ex2g2bar << std::endl;

               if(  TMath::IsNaN(x2g1) || TMath::IsNaN(x2g2)  ||  TMath::IsNaN(ex2g1) || TMath::IsNaN(ex2g2) ) {
                  x2g1bar  = 0.0;
                  x2g2bar  = 0.0;
                  xg2bar   = 0.0;
                  ex2g1bar = 0.0;
                  ex2g2bar = 0.0;
                  exg2bar  = 0.0;
               }

               fx2g2bar->SetBinContent( bin, x2g2bar);
               fx2g2bar->SetBinError(   bin, ex2g2bar);

               fx2g1bar->SetBinContent( bin, x2g1bar);
               fx2g1bar->SetBinError(   bin, ex2g1bar);

               fxg2bar->SetBinContent(  bin, x2g2bar/x);
               fxg2bar->SetBinError(    bin, ex2g2bar/x);

            }
         }
      }

      // -------------------------------
      gr = new TGraphErrors(fx2g2bar);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(1.0);
      //gr->SetMarkerColor(fx2g2->GetMarkerColor());
      //gr->SetLineColor(  fx2g2->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(20);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         mg_x2g2bar->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",fA1_asym->GetQ2()),"lp");
         //leg_sane->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      if(jj==4){

         Int_t nint = 20;
         Double_t xmin = 0.35;
         Double_t xmax = 0.8;
         Double_t intstep = (xmax-xmin)/double(nint);
         for(int i = 0; i<nint; i++) {
            double xval = xmin + i*intstep;
            d2p_value += intstep*gr->Eval(xval);
         } 
      }

      // -------------------------------
      gr = new TGraphErrors(fx2g1bar);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(1.0);
      //gr->SetMarkerColor(fx2g2->GetMarkerColor());
      //gr->SetLineColor(  fx2g2->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(20);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         mg_x2g1bar->Add(gr,"p");
         //leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",fA1_asym->GetQ2()),"lp");
         //leg_sane->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      //// -------------------------------

   }


         std::cout << "d2p = " << d2p_value << std::endl;
   // -----------------------------------------------------------------------
   // d2p

   TCanvas * c = new TCanvas("d2_extraction1","d2_extraction1");
   //c->Divide(1,2);

   //c->cd(1);
   //gPad->SetGridy(true);

   mg_x2g2bar->Draw("a");
   mg_x2g2bar->SetTitle("x^{2} #bar{g}_{2} ");
   mg_x2g2bar->GetXaxis()->SetLimits(0.0,1.0); 
   mg_x2g2bar->GetXaxis()->SetTitle("x"); 
   mg_x2g2bar->GetYaxis()->SetRangeUser(-0.1,0.1); 
   mg_x2g2bar->Draw("a");

   xg2p_3->DrawCopy("same");

   c->SaveAs(Form("results/d2_extract/d2_extraction1_0_%d_%s.png",aNumber,sfName.Data()));
   c->SaveAs(Form("results/d2_extract/d2_extraction1_0_%d_%s.pdf",aNumber,sfName.Data()));
   c->SaveAs(Form("results/d2_extract/d2_extraction1_0_%d_%s.svg",aNumber,sfName.Data()));

   //c->cd(2);
   //gPad->SetGridy(true);

   mg_x2g1bar->Draw("a");
   mg_x2g1bar->SetTitle("x^{2} #bar{g}_{1} = exp - twist2 - twist3 = twist4 ");
   mg_x2g1bar->GetXaxis()->SetTitle("x"); 
   mg_x2g1bar->GetXaxis()->SetLimits(0.0,1.0); 
   mg_x2g1bar->GetYaxis()->SetRangeUser(-0.1,0.1); 
   mg_x2g1bar->Draw("a");

   xg1p_4->DrawCopy("same");

   leg->Draw();

   c->SaveAs(Form("results/d2_extract/d2_extraction1_1_%d_%s.png",aNumber,sfName.Data()));
   c->SaveAs(Form("results/d2_extract/d2_extraction1_1_%d_%s.pdf",aNumber,sfName.Data()));
   c->SaveAs(Form("results/d2_extract/d2_extraction1_1_%d_%s.svg",aNumber,sfName.Data()));

   return 0;
}
