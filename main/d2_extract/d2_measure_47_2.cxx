#include "asym/sane_data_bins.cxx"
//int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

#include "util/d2_table.h"
//#include "util/get_A2_fit.cxx"

using namespace nucdb;

Int_t d2_measure_47_2( Int_t aNumber = 8605 ) {

  using namespace TMath;
  Int_t paraFileVersion = aNumber;
  Int_t perpFileVersion = aNumber;
  Int_t para47Version   = aNumber;
  Int_t perp47Version   = aNumber;

  // ----------------------------------------------------------------
  // Model used to calculate  missing regions
  InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 
  InSANEFormFactors     * ffs  = fman->CreateFFs(4);
  TString    sfName    = "stat2015";
  InSANEPolarizedStructureFunctions * model_SFs            = fman->CreatePolSFs(6);
  InSANEPolarizedStructureFunctions * model_SFs2           = fman->CreatePolSFs(3);
  auto best_sfs  = fman->CreateSFs(0);
  auto best_ssfs = fman->CreatePolSFs(6);
  //auto A2_fit      = get_A2_fit();

  sane_data_bins();

  // ----------------------------------------------------------------
  //
  TFile * f47 = new TFile(Form("data/g1g2_47_%d.root",aNumber),"READ");
  TList * fg1_comb_list = (TList*)gROOT->FindObject("g1_47_W");
  TList * fg2_comb_list = (TList*)gROOT->FindObject("g2_47_W");
  if(!fg1_comb_list) {std::cout << "-11" << std::endl; return -11;}
  if(!fg2_comb_list){ std::cout << "-12" << std::endl; return -12;}

  TFile * fA47 = new TFile(Form("data/A1A2_47_%d.root",aNumber),"READ");
  TList * fA1_comb_list = (TList*)gROOT->FindObject("A1_47_W");
  TList * fA2_comb_list = (TList*)gROOT->FindObject("A2_47_W");
   if(!fA1_comb_list) {std::cout << "-13" << std::endl; return -13;}
   if(!fA2_comb_list){ std::cout << "-14" << std::endl; return -14;}

   std::vector<Double_t> xLimits_0;
   std::vector<Double_t> xLimits_1;

   std::vector<d2_table_entry> d2_table;

   std::vector<TGraphErrors*>  d2_integrands;
   std::vector<TGraphErrors*>  d2_integrands_syst;

   Manager * dbman = Manager::GetManager(1);
   Experiment * testexp = dbman->GetExperiment("SANE-moments");
   if(!testexp) testexp = new Experiment("SANE-moments","SANE-moments");
   testexp->GetMeasurements()->Clear();
   dbman->SaveExperiment(testexp);

   auto d2_Q2_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"d2p integrand","d2p integrand");

   auto d2_OAR_Q2_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"d2p OAR integrand","d2p OAR integrand");
   auto gT_OAR_Q2_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"gT OAR integrand","gT OAR integrand");
   auto g1_OAR_Q2_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"g1 OAR integrand","g1 OAR integrand");

   auto CN_10_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"CN_10 integrand","CN_10 integrand");
   auto CN_11_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"CN_11 integrand","CN_11 integrand");
   auto CN_12_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"CN_12 integrand","CN_12 integrand");

   auto CN_20_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"CN_20 integrand","CN_20 integrand");
   auto CN_21_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"CN_21 integrand","CN_21 integrand");
   auto CN_22_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"CN_22 integrand","CN_22 integrand");

   auto M_11_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"M_11 integrand","M_11 integrand");
   auto M_13_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"M_13 integrand","M_13 integrand");
   auto M_23_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"M_23 integrand","M_23 integrand");
   auto M_15_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"M_15 integrand","M_15 integrand");
   auto M_25_integrand_m = nucdb::GetOrCreateMeasurement(testexp,"M_25 integrand","M_25 integrand");

   DataPoint         datapoint; 
   BinnedVariable  * Q2Bin = new BinnedVariable("Qsquared","Q^{2}");
   BinnedVariable  * xBin  = new BinnedVariable("x","");
   BinnedVariable  * eBeam  = new BinnedVariable("Ebeam","E_{0}");
   BinnedVariable  * WBin  = new BinnedVariable("W","W");
   eBeam->SetMean(   4.7);
   eBeam->SetLimits(  4.6,4.8 );
   datapoint.AddBinVariable(Q2Bin);
   datapoint.AddBinVariable(WBin);
   datapoint.AddBinVariable(xBin);
   datapoint.AddBinVariable(eBeam);

   auto M_1n_g1_coeff = [](int n, double x, double Q2){
     double M  = 0.938;
     double xi = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));
     double y2 = ((M*M)/Q2);
     double c0 = TMath::Power(xi,double(n+1))/(x*x);
     double c1 = (x/xi) - (double(n*n)/double((n+2)*(n+2)))*xi*x*y2;
     return c0*c1;
   };
   auto M_1n_g2_coeff = [](int n, double x, double Q2){
     double M  = 0.938;
     double xi = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));
     double y2 = ((M*M)/Q2);
     double c0 = TMath::Power(xi,double(n+1))/(x*x);
     double c1 = -1.0*y2*x*x*(double(4*n)/double(n+2));
     return c0*c1;
   };

   auto M_2n_g1_coeff = [](int n, double x, double Q2){
     double M  = 0.938;
     double xi = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));
     double c0 = TMath::Power(xi,double(n+1))/(x*x);
     double c1 = (x/xi);
     return c0*c1;
   };
   auto M_2n_g2_coeff = [](int n, double x, double Q2){
     double M  = 0.938;
     double xi = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));
     double y2 = ((M*M)/Q2);
     double c0 = TMath::Power(xi,double(n+1))/(x*x);
     double c1 = (double(n)/double(n-1))*(x*x/(xi*xi))-(double(n)/double(n+1))*x*x*y2;
     return c0*c1;
   };
   // ------------------------------------------
   // Loop over Q2 bins
   for(int jj = 0;((jj<fg1_comb_list->GetEntries())&&(jj<5));jj++) {

      InSANEMeasuredAsymmetry         * fg1_asym        = (InSANEMeasuredAsymmetry * )(fg1_comb_list->At(jj));
      InSANEMeasuredAsymmetry         * fg2_asym        = (InSANEMeasuredAsymmetry * )(fg2_comb_list->At(jj));

      InSANEMeasuredAsymmetry         * fA1_asym        = (InSANEMeasuredAsymmetry * )(fA1_comb_list->At(jj));
      InSANEMeasuredAsymmetry         * fA2_asym        = (InSANEMeasuredAsymmetry * )(fA2_comb_list->At(jj));

      // kine is the same for g1 and g2
      InSANEAveragedKinematics1D      * kine            = &fg1_asym->fAvgKineVsW;

      TH1F * fg1   = &fg1_asym->fAsymmetryVsW;
      TH1F * fg2   = &fg2_asym->fAsymmetryVsW;
      TH1F * fA1   = &fA1_asym->fAsymmetryVsW;
      TH1F * fA2   = &fA2_asym->fAsymmetryVsW;

      //fg1_asym->MaskIntersection(fg1_asym->fMask_W,fg2_asym->fMask_W);
      TH1F * mask  = &fA1_asym->fMask_W;

      int lo_W_bin = mask->FindFirstBinAbove(0.1);
      if( lo_W_bin == -1 ) {
        std::cout << "Could not find bin above 0.1\n";
      }
      int hi_W_bin = mask->FindLastBinAbove(0.1);
      if( hi_W_bin == -1 ) {
        std::cout << "Could not find bin above 0.1\n";
      }

      d2_table_entry d2_point; 
      double W1 = mask->GetBinLowEdge(lo_W_bin);
      double W2 = mask->GetBinLowEdge(hi_W_bin+1);
      double Q2_1 = kine->fQ2.GetBinContent(lo_W_bin);
      double Q2_2 = kine->fQ2.GetBinContent(hi_W_bin);
      //double W1   = kine->fW.GetBinContent(lo_W_bin);
      //double W2   = kine->fW.GetBinContent(hi_W_bin);
      double x1   = InSANE::Kine::xBjorken_WQ2(W1,Q2_1);
      double x2   = InSANE::Kine::xBjorken_WQ2(W2,Q2_2);
      std::cout << " ===== " << jj << "=====\n";
      std::cout << lo_W_bin << "  W1: " << W1 << "  Q2_1: " << Q2_1 << "  x1: " << x1 << '\n';
      std::cout << hi_W_bin << "  W2: " << W2 << "  Q2_2: " << Q2_2 << "  x2: " << x2 << '\n';
      // 
      if(x2<x1) {
        xLimits_0.push_back(x2);
        xLimits_1.push_back(x1);
      } else {
        xLimits_0.push_back(x1);
        xLimits_1.push_back(x2);
      }
      d2_point.Q2_mean = fg1_asym->GetQ2();
      d2_point.Q2_min  = Q2_2;
      d2_point.Q2_max  = Q2_1;
      d2_point.W_mean  = (W2+W1)/2.0;
      d2_point.W_min   = W1;
      d2_point.W_max   = W2;
      d2_point.x_min   = x2;
      d2_point.x_max   = x1;

      TGraphErrors * gr_d2_nacht_integrand      = new TGraphErrors(1);
      TGraphErrors * gr_d2_nacht_integrand_syst = new TGraphErrors(1);
      d2_integrands.push_back(gr_d2_nacht_integrand);
      d2_integrands_syst.push_back(gr_d2_nacht_integrand_syst);

      TGraphErrors * gr_d2_OAR_integrand      = new TGraphErrors(1);
      TGraphErrors * gr_d2_OAR_integrand_syst = new TGraphErrors(1);
      d2_integrands.push_back(gr_d2_OAR_integrand);
      d2_integrands_syst.push_back(gr_d2_OAR_integrand_syst);

      Double_t d2p_value       = 0.0;
      Double_t wd2p_exp        = 0.0;
      Double_t d2p_exp         = 0.0;
      Double_t err_d2p_exp     = 0.0;
      Double_t syserr_d2p_exp  = 0.0;
      Double_t d2p_exp2        = 0.0;
      Double_t wd2p_exp2       = 0.0;
      Double_t err_d2p_exp2    = 0.0;
      Double_t syserr_d2p_exp2 = 0.0;
      Double_t d2p_nach        = 0.0;
      Double_t err_d2p_nach    = 0.0;
      Double_t syserr_d2p_nach = 0.0;
      Double_t d2p_OAR         = 0.0;
      Double_t err_d2p_OAR     = 0.0;
      Double_t syserr_d2p_OAR  = 0.0;
      Int_t    iXbins          = 0;

      Int_t   xBinmax = fg1->GetNbinsX();
      Int_t   yBinmax = fg1->GetNbinsY();
      Int_t   zBinmax = fg1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      Int_t iQ2 = 0;
      double mean_Q2 = 0.0;

      // ----------------------------------------------------------
      // 
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fg1->GetBin(i,j,k);

               Double_t g1      = fg1->GetBinContent(bin);
               Double_t g2      = fg2->GetBinContent(bin);
               Double_t eg1     = fg1->GetBinError(bin);
               Double_t eg2     = fg2->GetBinError(bin);
               Double_t eg1Syst = fg1_asym->fSystErrVsW.GetBinError(bin);
               Double_t eg2Syst = fg2_asym->fSystErrVsW.GetBinError(bin);

               Double_t A2      = fA2->GetBinContent(bin);
               Double_t eA2     = fA2->GetBinError(bin);
               Double_t eA2Syst = fA2_asym->fSystErrVsW.GetBinError(bin);

               if( TMath::IsNaN(eg1Syst) || TMath::IsNaN(eg2Syst) ) {
                  eg1Syst  = 0.0;
                  eg2Syst  = 0.0;
               }

               Double_t W_low = kine->fW.GetBinLowEdge(bin);
               Double_t dW    = kine->fW.GetBinWidth(bin);
               Double_t W_hi  = W_low+dW;

               Double_t W     = kine->fW.GetBinContent(bin);
               Double_t x     = kine->fx.GetBinContent(bin);
               Double_t Q2    = kine->fQ2.GetBinContent(bin);
               Double_t M     = (M_p/GeV);
               Double_t xi    = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));

               Double_t x_hi  = InSANE::Kine::xBjorken_WQ2(W_low,Q2);
               Double_t x_low = InSANE::Kine::xBjorken_WQ2(W_low+dW,Q2);
               Double_t dx = x_hi - x_low;

               WBin->SetMeanLimits(W,W_low,W_hi);
               xBin->SetMeanLimits(x,x_low,x_hi);
               Q2Bin->SetMeanLimits(Q2,Q2,Q2);

               // -------------------------------------
               // d2 Nachtmann moment (Oscar's method)
               // d2 = \int xi^2[ (2*xi/x - 3(1-xi^2 M^2/Q^2))g1(x) + 3(1-xi^2 M^2/Q^2)(F1/gamma)A2(x)]
               Double_t coeff_OAR    = xi*xi;
               Double_t g1_coeff_OAR = 2.0*xi/x-3.0*(1.0-xi*xi*M*M/(2.0*Q2));
               Double_t gT_coeff_OAR = 3.0*(1.0-xi*xi*M*M/(2.0*Q2));
               Double_t gamma        = TMath::Sqrt((2.0*x*M)*(2.0*x*M)/Q2);
               Double_t F1           = best_sfs->F1p(x,Q2);
               Double_t gT_OAR       = (F1/gamma)*A2;

               //Double_t gT_OAR       = (F1/gamma)*A2_fit->A2p(x,Q2);
               Double_t egT_OAR      = (F1/gamma)*eA2;
               Double_t egT_OARSyst  = (F1/gamma)*eA2Syst;

               Double_t g1_emp       = best_ssfs->g1p_TMC(x,Q2);
               Double_t eg1_emp      = 0.05*TMath::Abs(g1_emp); // 5 percent

               Double_t d2p_OAR_int      = coeff_OAR*(g1_coeff_OAR*g1_emp    + gT_coeff_OAR*gT_OAR);
               Double_t ed2p_OAR_int     = Power( coeff_OAR*(g1_coeff_OAR*0.0),2.0)       + Power(coeff_OAR*(gT_coeff_OAR*egT_OAR),2.0);
               Double_t ed2p_OAR_intSyst = coeff_OAR*(g1_coeff_OAR*eg1_emp   + gT_coeff_OAR*egT_OARSyst);

               Double_t gT_OAR_int      = coeff_OAR*( gT_coeff_OAR*gT_OAR);
               Double_t egT_OAR_int     = Power(coeff_OAR*(g1_coeff_OAR*0.0),2.0)       + Power(coeff_OAR*(gT_coeff_OAR*egT_OAR),2.0);
               Double_t egT_OAR_intSyst = coeff_OAR*(gT_coeff_OAR*egT_OARSyst);

               Double_t g1_OAR_int      = coeff_OAR*( g1_coeff_OAR*g1_emp);
               Double_t eg1_OAR_int     = 0.0000001;
               Double_t eg1_OAR_intSyst = coeff_OAR*(g1_coeff_OAR*eg1_emp   );

               // -------------------------------------
               // Nachtmann moments
               //Double_t dx    = kine->fx.GetBinWidth(bin);
               // for n = 3
               Double_t g1_coeff_nach       = (xi*xi/x)*(xi*xi/x)*(x/xi);
               Double_t g2_coeff_nach       = (xi*xi/x)*(xi*xi/x)*((3.0*x*x)/(2.0*xi*xi) - (3.0*M*M*x*x)/(4.0*Q2));
               Double_t d2p_nach_int        = (g1_coeff_nach*g1 + g2_coeff_nach*g2);
               Double_t err_d2p_nach_int    = Power(g1_coeff_nach*eg1,2.0) + Power(g2_coeff_nach*eg2,2.0);
               Double_t syserr_d2p_nach_int = Power(g1_coeff_nach*eg1Syst,2.0) + Power(g2_coeff_nach*eg2Syst,2.0);

               Double_t x2g1     = x*x*g1;
               Double_t x2g2     = x*x*g2;
               Double_t ex2g1    = x*x*eg1;
               Double_t ex2g2    = x*x*eg2;
               Double_t ex2g1Syst = x*x*eg1Syst;
               Double_t ex2g2Syst = x*x*eg2Syst;

               // --------------------------------------
               // d2 CN Moment
               Double_t d2_int_exp     = (2.0*x2g1 + 3.0*x2g2);
               Double_t ed2_int_exp    = TMath::Power(2.0*ex2g1,2.0) + TMath::Power(3.0*ex2g2,2.0);
               Double_t sysed2_int_exp = TMath::Sqrt(TMath::Power(2.0*ex2g1Syst,2.0) + TMath::Power(3.0*ex2g2Syst,2.0));

               Double_t g2WW_model      = model_SFs->x2g2pWW(x,Q2);
               Double_t g1_model        = model_SFs->g1p_Twist2(x,Q2);
               Double_t g1_twist3_model = model_SFs->g1p_Twist3(x,Q2);
               Double_t eg2WW_model     = ex2g1/(x*x);
               Double_t eg1_model       = ex2g1/(x*x);

               Double_t x2g2bar         = (x2g2 - x*x*(g2WW_model))*3.0;
               Double_t x2g1bar         =  x2g1 - x*x*(g1_model+g1_twist3_model);

               Double_t ex2g2bar        = TMath::Sqrt(ex2g2*ex2g2 )*3.0;
               Double_t sysex2g2bar     = TMath::Sqrt(ex2g2Syst*ex2g2Syst + TMath::Power(x*x*eg2WW_model,2.0))*3.0;

               Double_t xg2bar          = 0;
               Double_t exg2bar         = 0;
               Double_t ex2g1bar        = 0.0;

               if(  TMath::IsNaN(x2g1) || TMath::IsNaN(x2g2)  ||  TMath::IsNaN(ex2g1) || TMath::IsNaN(ex2g2) ) {
                  x2g1bar  = 0.0;
                  x2g2bar  = 0.0;
                  xg2bar   = 0.0;
                  ex2g1bar = 0.0;
                  ex2g2bar = 0.0;
                  exg2bar  = 0.0;
               }

               if(jj<4) {

                  if(x > xLimits_0[jj] && x < xLimits_1[jj] ) {

                     if( (!TMath::IsNaN(d2_int_exp))  ){

               std::cout << " g1 syst = " << g1_coeff_nach*eg1Syst << std::endl;
               std::cout << " g2 syst = " << g2_coeff_nach*eg2Syst << std::endl;

                        if(  ( ed2_int_exp > 2.5)  ){
                           std::cout << "error: W " << x << "  "  << ex2g2bar << std::endl;
                           std::cout << " low : " << xLimits_0[jj] << " high : " << xLimits_1[jj] << std::endl;
                        } else {

                           //if(ed2_int_exp == 0.0 ) ed2_int_exp = 9999.0;
                           //std::cout << Q2 << std::endl;

                           d2p_exp          += d2_int_exp*dx;
                           err_d2p_exp      += ed2_int_exp*dx*dx;
                           syserr_d2p_exp   += sysed2_int_exp*dx;

                           d2p_exp2         += x2g2bar*dx;
                           err_d2p_exp2     += ex2g2bar*dx*dx;
                           syserr_d2p_exp2  += sysex2g2bar*dx;

                           d2p_nach        += (d2p_nach_int*dx);
                           err_d2p_nach    += err_d2p_nach_int*dx*dx;
                           syserr_d2p_nach += syserr_d2p_nach_int*dx*dx;

                           d2p_OAR        += d2p_OAR_int*dx;
                           err_d2p_OAR    += ed2p_OAR_int*dx*dx;
                           syserr_d2p_OAR += ed2p_OAR_intSyst*dx;

                           mean_Q2 += Q2;
                           iQ2++;


                           gr_d2_nacht_integrand->SetPoint(          iXbins, x,   2.0*d2p_nach_int);
                           gr_d2_nacht_integrand->SetPointError(     iXbins, 0.0, 2.0*Sqrt(err_d2p_nach_int));
                           gr_d2_nacht_integrand_syst->SetPoint(     iXbins, x,   -0.5+syserr_d2p_nach_int);
                           gr_d2_nacht_integrand_syst->SetPointError(iXbins, 0.0, syserr_d2p_nach_int);

                           gr_d2_OAR_integrand->SetPoint(          iXbins, x,   2.0*d2p_OAR_int);
                           gr_d2_OAR_integrand->SetPointError(     iXbins, 0.0, 2.0*Sqrt(ed2p_OAR_int));
                           gr_d2_OAR_integrand_syst->SetPoint(     iXbins, x,   -0.5+ed2p_OAR_intSyst);
                           gr_d2_OAR_integrand_syst->SetPointError(iXbins, 0.0, ed2p_OAR_intSyst);


                           // ------------------------------
                           // nucdb moment integrands
                           
                           datapoint.SetStatError( 2.0*Sqrt(err_d2p_nach_int)     );
                           datapoint.SetSystError( syserr_d2p_nach_int            );
                           datapoint.SetValue(     2.0*d2p_nach_int );
                           d2_Q2_integrand_m->AddDataPoint( new DataPoint(datapoint) );

                           // OAR Nachtmann moments
                           datapoint.SetValStatSyst( d2p_OAR_int     , TMath::Sqrt(ed2p_OAR_int)    , ed2p_OAR_intSyst );
                           d2_OAR_Q2_integrand_m->AddDataPoint( new DataPoint(datapoint) );

                           datapoint.SetValStatSyst( gT_OAR_int     , TMath::Sqrt(egT_OAR_int)    , egT_OAR_intSyst );
                           gT_OAR_Q2_integrand_m->AddDataPoint( new DataPoint(datapoint) );

                           datapoint.SetValStatSyst( g1_OAR_int     , TMath::Sqrt(eg1_OAR_int)    , eg1_OAR_intSyst );
                           g1_OAR_Q2_integrand_m->AddDataPoint( new DataPoint(datapoint) );

                           // CN Moments
                           //
                           datapoint.SetValStatSyst( g1, eg1, eg1Syst );
                           CN_10_integrand_m->AddDataPoint( new DataPoint(datapoint) );
                           datapoint.SetValStatSyst( x*g1, x*eg1, x*eg1Syst );
                           CN_11_integrand_m->AddDataPoint( new DataPoint(datapoint) );
                           datapoint.SetValStatSyst( x*x*g1, x*x*eg1, x*x*eg1Syst );
                           CN_12_integrand_m->AddDataPoint( new DataPoint(datapoint) );

                           datapoint.SetValStatSyst( g2, eg2, eg2Syst );
                           CN_20_integrand_m->AddDataPoint( new DataPoint(datapoint) );
                           datapoint.SetValStatSyst( x*g2, x*eg2, x*eg2Syst );
                           CN_21_integrand_m->AddDataPoint( new DataPoint(datapoint) );
                           datapoint.SetValStatSyst( x*x*g2, x*x*eg2, x*x*eg2Syst );
                           CN_22_integrand_m->AddDataPoint( new DataPoint(datapoint) );



                           // Standard Nachtmann moments
                           
                           double g1coef = M_1n_g1_coeff(1,x,Q2);
                           double g2coef = M_1n_g2_coeff(1,x,Q2);
                           double MOM    = g1coef*g1 + g2coef*g2;
                           double eMOM   = TMath::Sqrt(TMath::Power(g1coef*eg1,2.0) + TMath::Power(g2coef*eg2,2.0));
                           double eMOMSy = TMath::Sqrt(TMath::Power(g1coef*eg1Syst,2.0) + TMath::Power(g2coef*eg1Syst,2.0));
                           datapoint.SetValStatSyst( MOM, eMOM, eMOMSy );
                           M_11_integrand_m->AddDataPoint( new DataPoint(datapoint) );
                           
                           g1coef = M_1n_g1_coeff(3,x,Q2);
                           g2coef = M_1n_g2_coeff(3,x,Q2);
                           MOM    = g1coef*g1 + g2coef*g2;
                           eMOM   = TMath::Sqrt(TMath::Power(g1coef*eg1,2.0) + TMath::Power(g2coef*eg2,2.0));
                           eMOMSy = TMath::Sqrt(TMath::Power(g1coef*eg1Syst,2.0) + TMath::Power(g2coef*eg1Syst,2.0));
                           datapoint.SetValStatSyst( MOM, eMOM, eMOMSy );
                           M_13_integrand_m->AddDataPoint( new DataPoint(datapoint) );

                           g1coef = M_2n_g1_coeff(3,x,Q2);
                           g2coef = M_2n_g2_coeff(3,x,Q2);
                           MOM    = g1coef*g1 + g2coef*g2;
                           eMOM   = TMath::Sqrt(TMath::Power(g1coef*eg1,2.0) + TMath::Power(g2coef*eg2,2.0));
                           eMOMSy = TMath::Sqrt(TMath::Power(g1coef*eg1Syst,2.0) + TMath::Power(g2coef*eg1Syst,2.0));
                           datapoint.SetValStatSyst( MOM, eMOM, eMOMSy );
                           M_23_integrand_m->AddDataPoint( new DataPoint(datapoint) );

                           g1coef = M_1n_g1_coeff(5,x,Q2);
                           g2coef = M_1n_g2_coeff(5,x,Q2);
                           MOM    = g1coef*g1 + g2coef*g2;
                           eMOM   = TMath::Sqrt(TMath::Power(g1coef*eg1,2.0) + TMath::Power(g2coef*eg2,2.0));
                           eMOMSy = TMath::Sqrt(TMath::Power(g1coef*eg1Syst,2.0) + TMath::Power(g2coef*eg1Syst,2.0));
                           datapoint.SetValStatSyst( MOM, eMOM, eMOMSy );
                           M_15_integrand_m->AddDataPoint( new DataPoint(datapoint) );

                           g1coef = M_2n_g1_coeff(5,x,Q2);
                           g2coef = M_2n_g2_coeff(5,x,Q2);
                           MOM    = g1coef*g1 + g2coef*g2;
                           eMOM   = TMath::Sqrt(TMath::Power(g1coef*eg1,2.0) + TMath::Power(g2coef*eg2,2.0));
                           eMOMSy = TMath::Sqrt(TMath::Power(g1coef*eg1Syst,2.0) + TMath::Power(g2coef*eg1Syst,2.0));
                           datapoint.SetValStatSyst( MOM, eMOM, eMOMSy );
                           M_25_integrand_m->AddDataPoint( new DataPoint(datapoint) );


                           iXbins++;
                           //std::cout << "error: x " << x << "  "  << ex2g2bar << std::endl;
                        }
                     }
                  }
               }

            } // bin loop
         }
      }

      // -------------------------------

      mean_Q2 = mean_Q2/double(iQ2);
      //bin_mean_Q2.push_back(mean_Q2);

      d2p_nach        = 2.0*d2p_nach;
      err_d2p_nach    = 4.0*err_d2p_nach;
      syserr_d2p_nach = 2.0*syserr_d2p_nach;

      d2p_OAR        = 2.0*d2p_OAR;
      err_d2p_OAR    = 4.0*err_d2p_OAR;
      syserr_d2p_OAR = 2.0*syserr_d2p_OAR;

      d2_point.d2_CN_measured = d2p_exp;
      d2_point.d2_CN_stat_unc = TMath::Sqrt(err_d2p_exp);
      d2_point.d2_CN_syst_unc = TMath::Sqrt(syserr_d2p_exp)/double(iXbins);

      d2_point.d2_WW_measured = d2p_exp2;
      d2_point.d2_WW_stat_unc = TMath::Sqrt(err_d2p_exp2);
      d2_point.d2_WW_syst_unc = TMath::Sqrt(syserr_d2p_exp2)/double(iXbins);

      d2_point.d2_Nacht_measured = d2p_nach;
      d2_point.d2_Nacht_stat_unc = TMath::Sqrt(err_d2p_nach);
      d2_point.d2_Nacht_syst_unc = TMath::Sqrt(syserr_d2p_nach)/TMath::Sqrt(double(iXbins-1));

      d2_point.d2_OAR_measured = d2p_OAR;
      d2_point.d2_OAR_stat_unc = TMath::Sqrt(err_d2p_OAR);
      d2_point.d2_OAR_syst_unc = syserr_d2p_OAR/double(iXbins);

      //bin_d2p_value.push_back    ( d2p_value    );
      //bin_wd2p_exp.push_back     ( wd2p_exp     );
      //bin_d2p_exp.push_back      ( d2p_exp      );
      //bin_err_d2p_exp.push_back  ( err_d2p_exp  );
      //bin_syserr_d2p_exp.push_back  ( syserr_d2p_exp  );
      //bin_d2p_exp2.push_back     ( d2p_exp2     );
      //bin_wd2p_exp2.push_back    ( wd2p_exp2    );
      //bin_err_d2p_exp2.push_back ( err_d2p_exp2 );
      //bin_syserr_d2p_exp2.push_back ( syserr_d2p_exp2 );

      //bin_d2p_nach.push_back     ( d2p_nach     );
      //bin_err_d2p_nach.push_back ( err_d2p_nach );
      //bin_syserr_d2p_nach.push_back ( syserr_d2p_nach );

      //bin_d2p_OAR.push_back     ( d2p_OAR     );
      //bin_err_d2p_OAR.push_back ( err_d2p_OAR );
      //bin_syserr_d2p_OAR.push_back ( syserr_d2p_OAR );

      //bin_iXbins.push_back       ( iXbins);
      d2_point.n_xbins = iXbins;

      d2_table.push_back(d2_point);

   }

   dbman->SaveExperiment(testexp);


   //// -----------------------------------------------------------------------
   ////
   //std::ofstream ofile("results/d2/d2p_values.txt", std::ofstream::app);
   //ofile <<      "\t" << "i";
   //ofile      << "\t" << "Q2";
   //ofile      << "\t" << "x_low";
   //ofile      << "\t" << "x_high";
   //ofile      << "\t" << "d2p_exp " << "\t" << "stat"<< "\t" << "syst";
   //ofile      << "\t" << "d2p_exp2" << "\t" << "stat"<< "\t" << "syst";
   //ofile      << "\t" << "d2p_lowx" ;
   //ofile      << "\t" << "d2p_highx" ;
   //ofile      << "\t" << "d2p_nach"  << "\t" << "stat"<< "\t" << "syst";
   //ofile << std::endl;

   // -----------------------------------------------------------------------
   //
   Experiment * exp = dbman->GetExperiment("SANE");
   if(!exp) exp = new Experiment("SANE","SANE");
   std::vector<Measurement*> d2_measurments;

   // ---------------------------------------
   auto d2pWW_meas = nucdb::GetOrCreateMeasurement(exp,"d2pWW measured","d2pWW measured");
   d2pWW_meas->SetExperimentName("Using g1 model");
   d2_measurments.push_back(d2pWW_meas);

   // ---------------------------------------
   auto d2p_meas = nucdb::GetOrCreateMeasurement(exp,"d2p measured","d2p measured");
   d2p_meas->SetExperimentName("#bar{d}_{2} BETA");
   d2_measurments.push_back(d2p_meas);

   auto d2p_lowx = nucdb::GetOrCreateMeasurement(exp,"d2p lowx","d2p lowx");
   d2p_lowx->SetExperimentName("BETA low x");
   d2_measurments.push_back(d2p_lowx);

   auto d2p_highx = nucdb::GetOrCreateMeasurement(exp,"d2p highx","d2p highx");
   d2p_highx->SetExperimentName("BETA high x");
   d2_measurments.push_back(d2p_highx);

   auto d2p_bar = nucdb::GetOrCreateMeasurement(exp, "d2pbar","d2pbar");
   d2p_bar->SetExperimentName("d_{2} BETA");
   d2_measurments.push_back(d2p_bar);

   auto d2p_CN = nucdb::GetOrCreateMeasurement(exp, "d2p","d2p");
   d2p_CN->SetExperimentName("d_{2} BETA");
   d2_measurments.push_back(d2p_CN);

   auto d2p_all = nucdb::GetOrCreateMeasurement(exp, "d2p all","d2p all");
   d2p_all->SetExperimentName("d_{2} BETA");
   d2_measurments.push_back(d2p_all);

   // ---------------------------------------
   auto d2p_meas_nachmom = nucdb::GetOrCreateMeasurement(exp,"d2p Nachtmann measured","d2p Nachtmann measured");
   d2p_meas_nachmom->SetExperimentName("#bar{d}_{2} Nachtmann BETA");
   d2_measurments.push_back(d2p_meas_nachmom);

   auto d2p_nachmom = nucdb::GetOrCreateMeasurement(exp,"d2p Nachtmann","d2p Nachtmann");
   d2p_nachmom->SetExperimentName("BETA inelastic Nachtmann");
   d2_measurments.push_back(d2p_nachmom);

   auto d2p_nachmom_all = nucdb::GetOrCreateMeasurement(exp,"d2p Nachtmann all","d2p Nachtmann all");
   d2p_nachmom_all->SetExperimentName("BETA Nachtmann all parts");
   d2_measurments.push_back(d2p_nachmom_all);

   auto d2p_nachmom_lowx = nucdb::GetOrCreateMeasurement(exp,"d2p Nachtmann lowx","d2p Nachtmann lowx");
   d2p_nachmom_lowx->SetExperimentName("BETA low x");
   d2_measurments.push_back(d2p_nachmom_lowx);

   auto d2p_nachmom_highx = nucdb::GetOrCreateMeasurement(exp,"d2p Nachtmann highx","d2p Nachtmann highx");
   d2p_nachmom_highx->SetExperimentName("BETA high x");
   d2_measurments.push_back(d2p_nachmom_highx);

   auto d2p_nachmom_el = nucdb::GetOrCreateMeasurement(exp,"d2p Nachtmann elastic","d2p Nachtmann elastic");
   d2p_nachmom_el->SetExperimentName("BETA elastic");
   d2_measurments.push_back(d2p_nachmom_el);


   // Clear all existing data points
   for(auto m : d2_measurments){
     m->ClearDataPoints();
   }

   // ---------------------------------------


   for(int i=0; i < d2_table.size() && i<fSANENBins; i++) {

      if( d2_table[i].n_xbins == 0 ) d2_table[i].n_xbins = 1;

      double Q2_val = d2_table[i].Q2_mean;

      d2_table[i].d2_CN_el           = ffs->d2p_el(Q2_val);
      d2_table[i].d2_CN_low_x        = model_SFs->d2p_tilde( Q2_val, 0.01        , xLimits_0[i]);
      d2_table[i].d2_CN_high_x       = model_SFs->d2p_tilde( Q2_val, xLimits_1[i], 0.99);

      d2_table[i].d2_WW_el           = ffs->d2p_el(Q2_val);
      d2_table[i].d2_WW_low_x        = model_SFs->d2p_tilde( Q2_val, 0.01        , xLimits_0[i]);
      d2_table[i].d2_WW_high_x       = model_SFs->d2p_tilde( Q2_val, xLimits_1[i], 0.99);

      d2_table[i].d2_Nacht_el        = 2.0*ffs->d2p_el_Nachtmann(Q2_val);;

      double xl_model0    =  2.0*model_SFs->M2n_p(3, Q2_val, 0.01        , xLimits_0[i]);
      double xh_model0    =  2.0*model_SFs->M2n_p(3, Q2_val, xLimits_1[i], 0.99);

      d2_table[i].d2_Nacht_low_x     = xl_model0;
      d2_table[i].d2_Nacht_high_x    = xh_model0;

      double xl_model2    = 2.0*model_SFs2->M2n_p(3, Q2_val, 0.01        , xLimits_0[i]);
      double xh_model2    = 2.0*model_SFs2->M2n_p(3, Q2_val, xLimits_1[i], 0.99);

      d2_table[i].d2_Nacht_low_x_unc     = TMath::Abs(xl_model0-xl_model2);
      d2_table[i].d2_Nacht_high_x_unc    = TMath::Abs(xh_model0-xh_model2);

      double d2_CN_inelastic = d2_table[i].d2_CN_measured + d2_table[i].d2_CN_low_x + d2_table[i].d2_CN_high_x;
      d2_table[i].d2_CN_total = d2_CN_inelastic + d2_table[i].d2_CN_el;

      double d2_WW_inelastic = d2_table[i].d2_WW_measured + d2_table[i].d2_WW_low_x + d2_table[i].d2_WW_high_x;
      d2_table[i].d2_WW_total = d2_WW_inelastic + d2_table[i].d2_WW_el;

      double d2_Nacht_inelastic = d2_table[i].d2_Nacht_measured + d2_table[i].d2_Nacht_low_x + d2_table[i].d2_Nacht_high_x;
      d2_table[i].d2_Nacht_total = d2_Nacht_inelastic + d2_table[i].d2_Nacht_el;

      //bin_err_d2p_exp[i]     = TMath::Sqrt(bin_err_d2p_exp[i]  );
      //bin_err_d2p_exp2[i]    = TMath::Sqrt(bin_err_d2p_exp2[i] );
      //bin_err_d2p_nach[i]    = TMath::Sqrt(bin_err_d2p_nach[i] );
      //bin_syserr_d2p_exp[i]  = TMath::Sqrt(bin_syserr_d2p_exp[i]  /double(bin_iXbins[i]));
      //bin_syserr_d2p_exp2[i] = TMath::Sqrt(bin_syserr_d2p_exp2[i] /double(bin_iXbins[i]));
      //bin_syserr_d2p_nach[i] = TMath::Sqrt(bin_syserr_d2p_nach[i] /double(bin_iXbins[i]));

      // Sane measured
      xBin->SetLimits(  d2_table[i].x_min,  d2_table[i].x_max );
      Q2Bin->SetMean(   d2_table[i].Q2_mean );
      Q2Bin->SetLimits( d2_table[i].Q2_min, d2_table[i].Q2_max  );

      //ErrorBar d2_CN_staterr(    d2_table[i].d2_CN_stat_unc );
      //ErrorBar d2_Nacht_staterr( d2_table[i].d2_Nacht_stat_unc );
      //ErrorBar d2_OAR_staterr(   d2_table[i].d2_OAR_stat_unc );

      //ErrorBar d2_CN_systerr(    d2_table[i].d2_CN_syst_unc );
      //ErrorBar d2_Nacht_systerr( d2_table[i].d2_Nacht_syst_unc );
      //ErrorBar d2_OAR_systerr(   d2_table[i].d2_OAR_syst_unc );

      // FIXME: add horizontal offset function to  to avoid adding to Q2
      // CN moments
      
      datapoint.SetStatError( d2_table[i].d2_CN_stat_unc );
      datapoint.SetSystError( d2_table[i].d2_CN_syst_unc );
      datapoint.SetValue(     d2_table[i].d2_CN_measured );
      d2p_meas->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetValue(     d2_CN_inelastic );
      d2p_CN->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( d2_table[i].d2_WW_stat_unc );
      datapoint.SetSystError( d2_table[i].d2_WW_syst_unc );
      datapoint.SetValue(     d2_table[i].d2_WW_measured );
      d2pWW_meas->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetValue(     d2_WW_inelastic);
      d2p_bar  ->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( d2_table[i].d2_CN_stat_unc );
      datapoint.SetSystError( d2_table[i].d2_CN_syst_unc );
      datapoint.SetValue(     d2_table[i].d2_CN_total );
      d2p_all->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( 0                       );
      datapoint.SetSystError( 0.001                   );
      datapoint.SetValue(     d2_table[i].d2_CN_low_x );
      d2p_lowx->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( 0                       );
      datapoint.SetSystError( 0.001                   );
      datapoint.SetValue(     d2_table[i].d2_CN_high_x );
      d2p_highx->AddDataPoint( new DataPoint(datapoint) );

      // ---------------------------------------------------
      // Nachtmann moments
      double d2_Nacht_total_sys_unc = TMath::Sqrt(
        TMath::Power(d2_table[i].d2_Nacht_low_x_unc ,2.0) +
        TMath::Power(d2_table[i].d2_Nacht_high_x_unc,2.0) +
        TMath::Power(d2_table[i].d2_Nacht_syst_unc  ,2.0) );
      
      datapoint.SetStatError( d2_table[i].d2_Nacht_stat_unc );
      datapoint.SetSystError( d2_Nacht_total_sys_unc );
      datapoint.SetValue    ( d2_Nacht_inelastic) ;
      d2p_nachmom->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( d2_table[i].d2_Nacht_stat_unc );
      datapoint.SetSystError( d2_table[i].d2_Nacht_syst_unc );
      datapoint.SetValue    ( d2_table[i].d2_Nacht_measured );
      d2p_meas_nachmom->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( d2_table[i].d2_Nacht_stat_unc );
      datapoint.SetSystError( d2_Nacht_total_sys_unc );
      datapoint.SetValue(     d2_table[i].d2_Nacht_total);
      d2p_nachmom_all->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( 0                       );
      datapoint.SetSystError( d2_table[i].d2_Nacht_low_x_unc );
      datapoint.SetValue(     d2_table[i].d2_Nacht_low_x );
      d2p_nachmom_lowx->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( 0                       );
      datapoint.SetSystError( d2_table[i].d2_Nacht_high_x_unc );
      datapoint.SetValue(     d2_table[i].d2_Nacht_high_x );
      d2p_nachmom_highx->AddDataPoint( new DataPoint(datapoint) );

      datapoint.SetStatError( 0                       );
      datapoint.SetSystError( 0.0001 );
      datapoint.SetValue(     d2_table[i].d2_Nacht_el );
      d2p_nachmom_el->AddDataPoint( new DataPoint(datapoint) );

      //d2p_lowx->Print("data");
      //d2p_highx->Print("data");

      //std::cout << "d2p (model)       = " << bin_d2p_value[i] << std::endl;
      //std::cout << "d2p (exp)         = " << bin_d2p_exp[i] << " += " <<  TMath::Sqrt(err_d2p_exp[i])/double(1.0) << std::endl;
      //std::cout << "d2p (exp g2 only) = " << bin_d2p_exp2[i] << " += " << (bin_err_d2p_exp2[i])/double(bin_iXbins[i]) << std::endl;
      //std::cout << "d2p (0.01-0.35)   = " << d2p_lowx << std::endl;
      //std::cout << "d2p (nach)        = " << bin_d2p_nach[i] << " += " <<  (bin_err_d2p_nach[i])/double(bin_iXbins[i]) << std::endl;

     // ofile << i << "\t" << bin_Q2[i];
     // ofile      << "\t" << xLimits_0[i];
     // ofile      << "\t" << xLimits_1[i];
     // ofile      << "\t" << bin_d2p_exp[i]  << "\t" << (bin_err_d2p_exp[i]) << "\t" << (bin_syserr_d2p_exp[i])  ;
     // ofile      << "\t" << bin_d2p_exp2[i] << "\t" << (bin_err_d2p_exp2[i])<< "\t" << (bin_syserr_d2p_exp2[i]) ;
     // ofile      << "\t" << d2p_lowx_model ;
     // ofile      << "\t" << d2p_highx_model ;
     // ofile      << "\t" << bin_d2p_nach[i]  << "\t" << (bin_err_d2p_nach[i]) << "\t" << (bin_syserr_d2p_nach[i]);
     // ofile      << std::endl;
   }

   ErrorBar syserr0( 0.0 );
   datapoint.SetSystError(syserr0);

   Double_t  d2p_kang_lowx_model   = model_SFs->d2p_tilde(1.86,0.01,0.47);
   Double_t  d2p_kang_highx_model  = model_SFs->d2p_tilde(1.86,0.87,0.99);
   std::cout << " d2p_kang_lowx  " << d2p_kang_lowx_model  << std::endl;
   std::cout << " d2p_kang_highx " << d2p_kang_highx_model << std::endl;


   // -----------------------------------------------------------------------
   //
   TCanvas * c = new TCanvas();
   TGraphErrors  * gr_nacht = new TGraphErrors();
   TMultiGraph * mg = new TMultiGraph();
   for(unsigned int ig = 0; ig<8; ig++) {
     d2_integrands[ig]->SetLineColor(2000+ig/2);
     d2_integrands[ig]->SetMarkerColor(2000+ig/2);
     d2_integrands[ig]->SetMarkerStyle(20);
     d2_integrands[ig]->SetMarkerStyle(20+4*(ig%2));
     d2_integrands[ig]->SetLineWidth(3);

     d2_integrands_syst[ig]->SetLineColor(2000+ig/2);
     d2_integrands_syst[ig]->SetMarkerColor(2000+ig/2);
     d2_integrands_syst[ig]->SetMarkerStyle(20);
     d2_integrands_syst[ig]->SetLineWidth(3);
     d2_integrands_syst[ig]->SetFillColorAlpha(2000+ig/2,0.3);
     d2_integrands_syst[ig]->SetFillStyle(1001);//3005+2*(ig%2));
     
     mg->Add(d2_integrands[ig],"ep");
     mg->Add(d2_integrands_syst[ig],"e3");

   }
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.0,1.0);
   mg->GetYaxis()->SetRangeUser(-0.5,0.5);
   mg->Draw("a");
   c->SaveAs(Form("results/d2_extract/d2_measure_47_%d_0.png",aNumber));
   c->SaveAs(Form("results/d2_extract/d2_measure_47_%d_0.pdf",aNumber));

   //TFile fout("d2_table_out.root","UPDATE");

   // -------------------
   c = new TCanvas();
   TMultiGraph * mg2 = new TMultiGraph();
   TMultiGraph * mgtemp = 0;
   TLegend * leg = new TLegend(0.7,0.7,0.9,0.9);
   for(unsigned int ig = 0; ig<4; ig++) {
     auto p =  d2_table[ig];
     mg2->Add( mgtemp = CreateGraphPoints(p, 2000+ig) );
     if(ig==0) {
       leg->AddEntry(mgtemp->GetListOfGraphs()->At(4),"measured","lp");
       leg->AddEntry(mgtemp->GetListOfGraphs()->At(0),"low x","lp");
       leg->AddEntry(mgtemp->GetListOfGraphs()->At(1),"high x","lp");
       leg->AddEntry(mgtemp->GetListOfGraphs()->At(6),"OAR","lp");
     }
   }

   auto pdfs = new JAM15PolarizedPDFs();
   auto sfs  = best_ssfs;//new InSANEPolarizedStructureFunctionsFromPDFs(pdfs);

   TGraph * gr_elastic = new TGraph();
   TGraph * gr_jam     = new TGraph();
   for(int i= 0; i<60;i++) {
     double q2 = 0.5+0.1*double(i);
     gr_elastic->SetPoint(i,q2, 2.0*ffs->d2p_el_Nachtmann(q2));
     gr_jam    ->SetPoint(i,q2, 2.0*sfs->M2n_TMC_p(3,q2,0.1,0.8));
   }

   gr_elastic->SetLineColor(1);
   gr_elastic->SetLineWidth(2);
   mg2->Add(gr_elastic,"l");

   gr_jam->SetLineColor(2);
   gr_jam->SetLineWidth(2);
   gr_jam->SetLineStyle(2);
   mg2->Add(gr_jam,"l");

   mg2->Draw("a");
   mg2->GetYaxis()->SetRangeUser(-0.04,0.04);
   mg2->GetXaxis()->SetLimits(0.5,6.0);
   leg->Draw();
   c->SaveAs(Form("results/d2_extract/d2_measure_47_%d_1.png",aNumber));
   c->SaveAs(Form("results/d2_extract/d2_measure_47_%d_1.pdf",aNumber));



   // --------------------------------------------------
   // Hoyoung's HMS data
   // -0.0087  +-0.0014  (0.47 < 0.87) , Q2=1.86
   // d2p_kang_lowx  0.00787601
   // d2p_kang_highx -0.000561791

   // --------------
   Measurement * d2p_kang_highx = exp->GetMeasurement("d2p kang");
   if( !d2p_kang_highx ) {
      d2p_kang_highx = new Measurement("d2p kang","d2p Kang");
      exp->AddMeasurement(d2p_kang_highx);
   }
   d2p_kang_highx->ClearDataPoints();
   d2p_kang_highx->SetExperimentName("SANE HMS");
   xBin->SetLimits(0.87,0.90);
   ErrorBar err(0.00001);
   datapoint.SetStatError(err);
   datapoint.SetValue(d2p_kang_highx_model);
   datapoint.CalculateTotalError();
   d2p_kang_highx->AddDataPoint( new DataPoint(datapoint) );

   // --------------
   Measurement * d2p_kang_lowx = exp->GetMeasurement("d2p lowx kang");
   if( !d2p_kang_lowx ) {
      d2p_kang_lowx = new Measurement("d2p lowx kang","d2p lowx kang");
      exp->AddMeasurement(d2p_kang_lowx);
   }
   d2p_kang_lowx->ClearDataPoints();
   d2p_kang_lowx->SetExperimentName("SANE HMS");
   xBin->SetLimits(0.01,0.47);
   Q2Bin->SetValueSize(1.86,0.1);
   err = ErrorBar(0.00001);
   datapoint.SetStatError(err);
   datapoint.SetValue(d2p_kang_lowx_model);
   datapoint.CalculateTotalError();
   d2p_kang_lowx->AddDataPoint( new DataPoint(datapoint) );

   // --------------
   Measurement * d2p_kang = exp->GetMeasurement("d2p kang");
   if( !d2p_kang ) {
      d2p_kang = new Measurement("d2p kang","d2p Kang");
      exp->AddMeasurement(d2p_kang);
   }
   d2p_kang->ClearDataPoints();
   d2p_kang->SetExperimentName("#bar{d}_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   err = ErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087);
   datapoint.CalculateTotalError();
   d2p_kang->AddDataPoint( new DataPoint(datapoint) );

   Measurement * d2p_bar_kang = exp->GetMeasurement("d2pbar kang");
   if( !d2p_bar_kang ) {
      d2p_bar_kang = new Measurement("d2pbar kang","d2pbar kang");
      exp->AddMeasurement(d2p_bar_kang);
   }
   d2p_bar_kang->ClearDataPoints();
   d2p_bar_kang->SetExperimentName("d_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   err = ErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087+d2p_kang_lowx_model + d2p_kang_highx_model);
   datapoint.CalculateTotalError();
   d2p_bar_kang->AddDataPoint( new DataPoint(datapoint) );

   Measurement * d2p_all_kang = exp->GetMeasurement("d2p all kang");
   if( !d2p_all_kang ) {
      d2p_all_kang = new Measurement("d2p all kang","d2p all kang");
      exp->AddMeasurement(d2p_all_kang);
   }
   d2p_all_kang->ClearDataPoints();
   d2p_all_kang->SetExperimentName("d_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   err = ErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087+d2p_kang_lowx_model + d2p_kang_highx_model + ffs->d2p_el_Nachtmann(1.86) );
   datapoint.CalculateTotalError();
   d2p_all_kang->AddDataPoint( new DataPoint(datapoint) );


   exp->Print();
   dbman->SaveExperiment(exp);

   return 0;
}
