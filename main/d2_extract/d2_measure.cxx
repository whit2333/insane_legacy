#include "asym/sane_data_bins.cxx"

int isinf(double x) { return !TMath::IsNaN(x) && TMath::IsNaN(x - x); }

Int_t d2_measure(Int_t paraFileVersion = 7609,Int_t perpFileVersion = 7609, 
                    Int_t para47Version = 7609, Int_t perp47Version = 7609,Int_t aNumber=7609){

   const TString weh("d2_measure()");
   //if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
   //   Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
   //   return -1;
   //}
   sane_data_bins();

   // Combined x2g1 and x2g2 
   TFile * f = new TFile(Form("data/A1A2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f->cd();
   //f->ls();
   TList * fx2g1Combined_list = (TList*)gROOT->FindObject(Form("x2g1-combined-x-%d",0));
   if(!fx2g1Combined_list) return(-1);
   TList * fx2g2Combined_list = (TList*)gROOT->FindObject(Form("x2g2-combined-x-%d",0));
   if(!fx2g2Combined_list) return(-2);

   // Get A1,A2, and F1 combined to use the kinematics ( need to check values of A1 and A2 ....)
   TFile * fileA1A2 = new TFile(Form("data/A1A2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   fileA1A2->cd();
   TList * fA1Combined_list = (TList *) gROOT->FindObject(Form("A1asym-combined-x-%d",0));
   if(!fA1Combined_list) return(-5);
   TList * fA2Combined_list = (TList *) gROOT->FindObject(Form("A2asym-combined-x-%d",0));
   if(!fA2Combined_list) return(-6);
   TList * fF1Combined_list = (TList *) gROOT->FindObject(Form("F1-combined-x-%d",0));
   if(!fF1Combined_list) return(-7);


   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 
   InSANEFormFactors                 * ffs  = fman->CreateFFs(4);
   // ----------------------------------------------------------------
   //
   TString sfName = "bb";
   InSANEPolarizedStructureFunctionsFromPDFs * bbSFs = 0;
   bbSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   //bbSFs->SetPolarizedPDFs(new DNS2005PolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new StatisticalPolarizedPDFs());
   bbSFs->SetPolarizedPDFs(new BBPolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new BBSPolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new AAC08PolarizedPDFs());
   //bbSFs->SetPolarizedPDFs(new DSSVPolarizedPDFs());

   Double_t Q20 = 1.0;
   ////WPolarizedEvolution<JAMPolarizedPDFs> * jamPDFs = new WPolarizedEvolution<JAMPolarizedPDFs>();
   //JAMPolarizedPDFs * jamPDFs = new JAMPolarizedPDFs();
   //bbSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   //bbSFs->SetPolarizedPDFs(jamPDFs);
   //jamPDFs->MakeGrid();
   //jamPDFs->EvolveInit(Q20);
   //jamPDFs->Evolve();

   //Double_t Q20 = 4.0;
   //WPolarizedEvolution<MHKPolarizedPDFs> * MHKPDFs = new WPolarizedEvolution<MHKPolarizedPDFs>();
   //bbSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   //bbSFs->SetPolarizedPDFs(MHKPDFs);
   //MHKPDFs->MakeGrid();
   //MHKPDFs->EvolveInit(Q20);
   //MHKPDFs->Evolve();

   // ------------------------------
   // Functions
   TF1 * xg1p_0 = new TF1("xg1p0", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p, 
                                 0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p");
   xg1p_0->SetParameter(0,5.0);
   xg1p_0->SetLineColor(1);

   TF1 * xg1p_1 = new TF1("xg1p1", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p_Twist234, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p_Twist234");
   xg1p_1->SetParameter(0,1.0);
   xg1p_1->SetLineColor(1);

   TF1 * xg1p_2 = new TF1("xg1p2", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p_Twist2, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p_Twist2");
   xg1p_2->SetParameter(0,1.0);
   xg1p_2->SetLineColor(1);

   TF1 * xg1p_3 = new TF1("xg1p3", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p_Twist3, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p_Twist3");
   xg1p_3->SetParameter(0,1.0);
   xg1p_3->SetLineColor(1);

   TF1 * xg1p_4 = new TF1("xg1p4", bbSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g1p_Twist4, 
                                  0, 1, 1,"InSANEPolarizedStructureFunctions","Evaluatex2g1p_Twist4");
   xg1p_4->SetParameter(0,1.0);
   xg1p_4->SetLineColor(1);


   //--------------------------------------
   TList * fd2_list      = new TList();
   TList * fx2g2bar_list = new TList();
   TH1F  * fx2g2bar      = 0;
   TList * fxg2bar_list = new TList();
   TH1F  * fxg2bar      = 0;
   TList * fx2g1bar_list = new TList();
   TH1F  * fx2g1bar      = 0;
   TList * fg2bar_list   = new TList();
   TH1F  * fg2bar        = 0;
   TList * fnacht_list   = new TList();
   TH1F  * fnacht        = 0;

   TMultiGraph * mg_x2g2bar = new TMultiGraph();
   TMultiGraph * mg_x2g1bar = new TMultiGraph();
   TMultiGraph * mg_xg2bar  = new TMultiGraph();
   TMultiGraph * mg_nacht  = new TMultiGraph();

   TLegend *leg = new TLegend(0.84, 0.15, 0.99, 0.85);

   std::vector<Double_t> bin_d2p_value    ;
   std::vector<Double_t> bin_wd2p_exp     ;
   std::vector<Double_t> bin_d2p_exp      ;
   std::vector<Double_t> bin_err_d2p_exp  ;
   std::vector<Double_t> bin_syserr_d2p_exp  ;
   std::vector<Double_t> bin_d2p_exp2     ;
   std::vector<Double_t> bin_wd2p_exp2    ;
   std::vector<Double_t> bin_err_d2p_exp2 ;
   std::vector<Double_t> bin_syserr_d2p_exp2 ;
   std::vector<Double_t> bin_d2p_nach     ;
   std::vector<Double_t> bin_err_d2p_nach ;
   std::vector<Double_t> bin_syserr_d2p_nach ;
   std::vector<Int_t>    bin_iXbins;
   std::vector<Double_t> bin_Q2 ;
   std::vector<Double_t> bin_Q2min;
   std::vector<Double_t> bin_Q2max;
   std::vector<Int_t>    bin_iQ2 ;
   std::vector<Double_t> bin_mean_Q2 ;

   Double_t xLimits_0[] = {0.15, 0.25, 0.4, 0.6, 0.15,
                           0.15, 0.25, 0.4, 0.6, 0.15,
                           0.15, 0.25, 0.4, 0.6, 0.15};
   Double_t xLimits_1[] = {0.32, 0.55, 0.9, 0.9, 0.9,
                           0.32, 0.55, 0.9, 0.9, 0.9,
                           0.32, 0.55, 0.9, 0.9, 0.9};

   // Loop over Q2 bins
   for(int jj = 0;jj<fA1Combined_list->GetEntries();jj++) {

      InSANEMeasuredAsymmetry         * fA1_asym        = (InSANEMeasuredAsymmetry * )(fA1Combined_list->At(jj));
      InSANEMeasuredAsymmetry         * fA2_asym        = (InSANEMeasuredAsymmetry * )(fA2Combined_list->At(jj));
      InSANEAveragedKinematics1D * kine = &(fA1_asym->fAvgKineVsx);
      // kine is the same for A1 and A2

      TH1F * fx2g1 = (TH1F*) fx2g1Combined_list->At(jj);
      TH1F * fx2g2 = (TH1F*) fx2g2Combined_list->At(jj);
      TH1F * fF1   = (TH1F*) fF1Combined_list->At(jj);

      bin_Q2.push_back(fA1_asym->GetQ2());
      bin_Q2min.push_back(fA1_asym->fQ2Min);
      bin_Q2max.push_back(fA1_asym->fQ2Max);

      Double_t d2p_value    = 0.0;
      Double_t wd2p_exp     = 0.0;
      Double_t d2p_exp      = 0.0;
      Double_t err_d2p_exp  = 0.0;
      Double_t syserr_d2p_exp  = 0.0;
      Double_t d2p_exp2     = 0.0;
      Double_t wd2p_exp2    = 0.0;
      Double_t err_d2p_exp2 = 0.0;
      Double_t syserr_d2p_exp2 = 0.0;
      Double_t d2p_nach     = 0.0;
      Double_t err_d2p_nach = 0.0;
      Double_t syserr_d2p_nach = 0.0;
      Int_t    iXbins = 0;

      // Create resulting histograms
      fx2g2bar = new TH1F(*fx2g1);
      fx2g2bar->Reset();
      fx2g2bar_list->Add(fx2g2bar);
      fxg2bar = new TH1F(*fx2g1);
      fxg2bar->Reset();
      fxg2bar_list->Add(fx2g2bar);
      fx2g1bar = new TH1F(*fx2g1);
      fx2g1bar->Reset();
      fx2g1bar_list->Add(fx2g2bar);
      fg2bar = new TH1F(*fx2g1);
      fg2bar->Reset();
      fg2bar_list->Add(fg2bar);

      fnacht = new TH1F(*fx2g1);
      fnacht->Reset();
      fnacht_list->Add(fnacht);

      Int_t   xBinmax = fx2g1->GetNbinsX();
      Int_t   yBinmax = fx2g1->GetNbinsY();
      Int_t   zBinmax = fx2g1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      Int_t iQ2 = 0;
      Int_t mean_Q2 = 0.0;

      //std::cout << " Graph " << jj << std::endl;
      // ----------------------------------------------------------
      // Sanitize the y values by removing NaNs
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fx2g1->GetBin(i,j,k);

               Double_t x2g1  = fx2g1->GetBinContent(bin)/2.0;
               Double_t x2g2  = fx2g2->GetBinContent(bin)/3.0;
               Double_t ex2g1 = fx2g1->GetBinError(bin)/2.0;
               Double_t ex2g2 = fx2g2->GetBinError(bin)/3.0;

               Double_t x     = kine->fx.GetBinContent(bin);
               Double_t Q2    = kine->fQ2.GetBinContent(bin);
               Double_t M     = (M_p/GeV);
               Double_t xi    = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));


               Double_t g1  = x2g1/(x*x);
               Double_t g2  = x2g2/(x*x);
               Double_t eg1  = ex2g1/(x*x);
               Double_t eg2  = ex2g2/(x*x);

               // Nachtmann moments
               Double_t dx    = kine->fx.GetBinWidth(bin);
               // for n = 3
               Double_t g1_coeff_nach = (xi*xi/x)*(xi*xi/x)*(x/xi);
               Double_t g2_coeff_nach = (xi*xi/x)*(xi*xi/x)*((3.0*x*x)/(2.0*xi*xi) - (3.0*M*M*x*x)/(4.0*Q2));
               Double_t d2p_nach_int  = (g1_coeff_nach*g1 + g2_coeff_nach*g2);
               Double_t err_d2p_nach_int  = (g1_coeff_nach*eg1 + g2_coeff_nach*eg2);

   //double xi = InSANE::Kine::xi_Nachtmann(x,Q2);
   //double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
   //double c0 = TMath::Power(xi,double(n))/(x*x);
   //double c1 = x/xi;
   //double c2 = (x/xi)*(x/xi)*double(n)/double(n-1) - y2*x*x*double(n)/double(n+1);
   //return( c0*(c1*this->g1p(x,Q2) + c2*this->g2p(x,Q2)) );

               //std::cout << "dx = " << dx << std::endl;

               Double_t d2_int_exp = (2.0*x2g1 + 3.0*x2g2);
               Double_t ed2_int_exp = TMath::Sqrt(TMath::Power(2.0*ex2g1,2.0) + TMath::Power(3.0*ex2g2,2.0));


               Double_t g2WW_model      = bbSFs->x2g2pWW(x,Q2);
               Double_t g1_model        = bbSFs->g1p_Twist2(x,Q2);
               Double_t g1_twist3_model = bbSFs->g1p_Twist3(x,Q2);
               Double_t eg2WW_model     = ex2g1/(x*x);
               Double_t eg1_model       = ex2g1/(x*x);

               Double_t x2g2bar         = (x2g2 - x*x*(g2WW_model))*3.0;
               Double_t x2g1bar         =  x2g1 - x*x*(g1_model+g1_twist3_model);

               Double_t ex2g2bar        = TMath::Sqrt(ex2g2*ex2g2 + TMath::Power(x*x*eg2WW_model,2.0))*3.0;
               //Double_t ex2g2bar        = TMath::Sqrt(ex2g2*ex2g2 + TMath::Power(x*x*eg2WW_model,2.0))*3.0;

               Double_t xg2bar          = 0;
               Double_t exg2bar         = 0;
               Double_t ex2g1bar = 0.0;

               // Systematic errors 


               //std::cout << "error: x " << x << "  "  << ex2g2bar << std::endl;

               if(  TMath::IsNaN(x2g1) || TMath::IsNaN(x2g2)  ||  TMath::IsNaN(ex2g1) || TMath::IsNaN(ex2g2) ) {
                  x2g1bar  = 0.0;
                  x2g2bar  = 0.0;
                  xg2bar   = 0.0;
                  ex2g1bar = 0.0;
                  ex2g2bar = 0.0;
                  exg2bar  = 0.0;
               }

               if(jj<15)
                  if(x > xLimits_0[jj] && x < xLimits_1[jj] ) {
                  if(  ( ed2_int_exp < 0.9)  ){
                     //if(ed2_int_exp == 0.0 ) ed2_int_exp = 9999.0;

                     std::cout << Q2 << std::endl;

                     d2p_exp      += (d2_int_exp*dx);
                     //err_d2p_exp  += 1.0/(TMath::Power(ed2_int_exp*dx,2.0));
                     err_d2p_exp  += TMath::Power(ed2_int_exp*dx,2.0);
                     //err_d2p_exp  += (ed2_int_exp*dx);

                     d2p_exp2     += (x2g2bar*dx);
                     err_d2p_exp2  += TMath::Power(ex2g2bar*dx,2.0);

                     mean_Q2 += Q2;
                     iQ2++;

                     d2p_nach += (d2p_nach_int*dx);
                     err_d2p_nach += TMath::Power(err_d2p_nach_int*dx,2.0);

                     iXbins++;
                     //std::cout << "error: x " << x << "  "  << ex2g2bar << std::endl;
                  }
               }

               fx2g2bar->SetBinContent( bin, d2_int_exp);
               fx2g2bar->SetBinError(   bin, ed2_int_exp);

               fx2g1bar->SetBinContent( bin, x2g1bar);
               fx2g1bar->SetBinError(   bin, ex2g1bar);

               fxg2bar->SetBinContent(  bin, x2g2bar/x);
               fxg2bar->SetBinError(    bin, ex2g2bar/x);

               fnacht->SetBinContent(  bin, d2p_nach_int);
               fnacht->SetBinError(    bin, err_d2p_nach_int);

            }
         }
      }

      mean_Q2 = mean_Q2/double(iQ2);
      bin_mean_Q2.push_back(mean_Q2);

      d2p_nach     = 2.0*d2p_nach;
      err_d2p_nach = 2.0*err_d2p_nach;

      bin_d2p_value.push_back    ( d2p_value    );
      bin_wd2p_exp.push_back     ( wd2p_exp     );
      bin_d2p_exp.push_back      ( d2p_exp      );
      bin_err_d2p_exp.push_back  ( err_d2p_exp  );
      bin_d2p_exp2.push_back     ( d2p_exp2     );
      bin_wd2p_exp2.push_back    ( wd2p_exp2    );
      bin_err_d2p_exp2.push_back ( err_d2p_exp2 );
      bin_d2p_nach.push_back     ( d2p_nach     );
      bin_err_d2p_nach.push_back ( err_d2p_nach );
      bin_iXbins.push_back       ( iXbins);

      // -------------------------------
      gr = new TGraphErrors(fx2g2bar);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(1.0);
      //gr->SetMarkerColor(fx2g2->GetMarkerColor());
      //gr->SetLineColor(  fx2g2->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(20);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         mg_x2g2bar->Add(gr,"p");
         leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",fA1_asym->GetQ2()),"lp");
         //leg_sane->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      if(jj==4){

         Int_t nint = 20;
         Double_t xmin = 0.35;
         Double_t xmax = 0.9;
         Double_t intstep = (xmax-xmin)/double(nint);
         for(int i = 0; i<nint; i++) {
            double xval = xmin + i*intstep;
            d2p_value += intstep*gr->Eval(xval);
         } 
      }

      // -------------------------------
      gr = new TGraphErrors(fx2g1bar);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(1.0);
      //gr->SetMarkerColor(fx2g2->GetMarkerColor());
      //gr->SetLineColor(  fx2g2->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(20);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         mg_x2g1bar->Add(gr,"p");
         //leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",fA1_asym->GetQ2()),"lp");
         //leg_sane->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      // -------------------------------
      gr = new TGraphErrors(fnacht);
      for( int j = gr->GetN()-1 ; j>=0; j--) {
         Double_t xt, yt;
         gr->GetPoint(j,xt,yt);
         Double_t ey = gr->GetErrorY(j);
         if( yt == 0.0 ) {
            gr->RemovePoint(j);
            continue;
         }
      }
      gr->SetMarkerStyle(20);
      gr->SetMarkerSize(1.0);
      //gr->SetMarkerColor(fx2g2->GetMarkerColor());
      //gr->SetLineColor(  fx2g2->GetMarkerColor());
      if(jj==4){
         gr->SetMarkerStyle(20);
         gr->SetMarkerColor(1);
         gr->SetLineColor(1);
      }
      if(jj<5){
         mg_nacht->Add(gr,"p");
         //leg->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",fA1_asym->GetQ2()),"lp");
         //leg_sane->AddEntry(gr,Form("SANE, Q^{2}=%.1f GeV^{2}",paraAsym->GetQ2()),"lp");
      }

      // -------------------------------
   }


   // -----------------------------------------------------------------------
   //
   std::ofstream ofile("d2p_values.txt", std::ofstream::app);
   ofile      << "\t" << "Q2";
   ofile      << "\t" << "x_low";
   ofile      << "\t" << "x_high";
   ofile      << "\t" << "d2p_exp " << "\t" << "err";
   ofile      << "\t" << "d2p_exp2" << "\t" << "err";
   ofile      << "\t" << "d2p_lowx" ;
   ofile      << "\t" << "d2p_highx" ;
   ofile      << "\t" << "d2p_nach"  << "\t" << "err";
   ofile << std::endl;

   // -----------------------------------------------------------------------
   //
   NucDBManager * dbman = NucDBManager::GetManager(1);
   NucDBExperiment * exp = dbman->GetExperiment("SANE");
   if(!exp) exp = new NucDBExperiment("SANE","SANE");

   // ---------------------------------------
   NucDBMeasurement * d2pWW_meas = exp->GetMeasurement("d2pWW measured");
   if( !d2pWW_meas ) {
      d2pWW_meas = new NucDBMeasurement("d2pWW measured","d2pWW measured");
      exp->AddMeasurement(d2pWW_meas);
   }
   d2pWW_meas->ClearDataPoints();
   d2pWW_meas->SetExperimentName("Using g1 model");

   // ---------------------------------------
   NucDBMeasurement * d2p_meas = exp->GetMeasurement("d2p measured");
   if( !d2p_meas ) {
      d2p_meas = new NucDBMeasurement("d2p measured","d2p measured");
      exp->AddMeasurement(d2p_meas);
   }
   d2p_meas->ClearDataPoints();
   d2p_meas->SetExperimentName("#bar{d}_{2} BETA");

   // ---------------------------------------
   NucDBMeasurement * d2pbar_nachmom = exp->GetMeasurement("d2pbar Nachtmann");
   if( !d2pbar_nachmom ) {
      d2pbar_nachmom = new NucDBMeasurement("d2pbar Nachtmann","d2pbar Nachtmann");
      exp->AddMeasurement(d2pbar_nachmom);
   }
   d2pbar_nachmom->ClearDataPoints();
   d2pbar_nachmom->SetExperimentName("BETA inelastic Nachtmann");

   // ---------------------------------------
   NucDBMeasurement * d2p_nachmom = exp->GetMeasurement("d2p Nachtmann");
   if( !d2p_nachmom ) {
      d2p_nachmom = new NucDBMeasurement("d2p Nachtmann","d2p Nachtmann");
      exp->AddMeasurement(d2p_nachmom);
   }
   d2p_nachmom->ClearDataPoints();
   d2p_nachmom->SetExperimentName("BETA Nachtmann");

   // ---------------------------------------
   NucDBMeasurement * d2p_lowx = exp->GetMeasurement("d2p lowx");
   if( !d2p_lowx ) {
      d2p_lowx = new NucDBMeasurement("d2p lowx","d2p lowx");
      exp->AddMeasurement(d2p_lowx);
   }
   d2p_lowx->ClearDataPoints();
   d2p_lowx->SetExperimentName("BETA low x");

   // ---------------------------------------
   NucDBMeasurement * d2p_highx = exp->GetMeasurement("d2p highx");
   if( !d2p_highx ) {
      d2p_highx = new NucDBMeasurement("d2p highx","d2p highx");
      exp->AddMeasurement(d2p_highx);
   }
   d2p_highx->ClearDataPoints();
   d2p_highx->SetExperimentName("BETA high x");

   // ---------------------------------------
   NucDBMeasurement * d2p_bar = exp->GetMeasurement("d2pbar");
   if( !d2p_bar ) {
      d2p_bar = new NucDBMeasurement("d2pbar","d2pbar");
      exp->AddMeasurement(d2p_bar);
   }
   d2p_bar->ClearDataPoints();
   d2p_bar->SetExperimentName("d_{2} BETA");

   // ---------------------------------------
   NucDBMeasurement * d2p_total = exp->GetMeasurement("d2p inelastic");
   if( !d2p_total ) {
      d2p_total = new NucDBMeasurement("d2p inelastic","d2p inelastic");
      exp->AddMeasurement(d2p_total);
   }
   d2p_total->ClearDataPoints();
   d2p_total->SetExperimentName("BETA inelastic CN");

   // ---------------------------------------
   NucDBMeasurement * d2p_all = exp->GetMeasurement("d2p all");
   if( !d2p_all ) {
      d2p_all = new NucDBMeasurement("d2p all","d2p all");
      exp->AddMeasurement(d2p_all);
   }
   d2p_all->ClearDataPoints();
   d2p_all->SetExperimentName("d_{2} BETA");


   NucDBDataPoint         datapoint; 
   NucDBBinnedVariable  * Q2Bin = new NucDBBinnedVariable("Qsquared","Q^{2}");
   NucDBBinnedVariable  * xBin  = new NucDBBinnedVariable("x","");
   datapoint.AddBinVariable(Q2Bin);
   datapoint.AddBinVariable(xBin);

   for(int i=0; i < bin_d2p_value.size() && i<fSANENBins; i++) {

      if( bin_iXbins[i] == 0 ) bin_iXbins[i] = 1;

      Double_t d2p_lowx_model  = bbSFs->d2p_tilde(bin_Q2[i],0.01,xLimits_0[i]);
      Double_t d2p_highx_model = bbSFs->d2p_tilde(bin_Q2[i],xLimits_1[i],0.99);
      Double_t M23_lowx_model  = bbSFs->M2n_p(3,bin_Q2[i],0.01,xLimits_0[i]);
      Double_t M23_highx_model = bbSFs->M2n_p(3,bin_Q2[i],xLimits_1[i],0.99);
      Double_t d2p_tilde_lowx_model    = M23_lowx_model *2.0;
      Double_t d2p_tilde_highx_model   = M23_highx_model*2.0;
      Double_t d2p_tilde_elastic_model =  ffs->d2p_el_Nachtmann(bin_Q2[i])*2.0;

      bin_err_d2p_exp[i]  = TMath::Sqrt(bin_err_d2p_exp[i]  );
      bin_err_d2p_exp2[i] = TMath::Sqrt(bin_err_d2p_exp2[i] );
      bin_err_d2p_nach[i] = TMath::Sqrt(bin_err_d2p_nach[i] );

      // Sane measured
      xBin->SetLimits(xLimits_0[i],xLimits_1[i]);
      Q2Bin->SetLimits(bin_Q2min[i],bin_Q2max[i]);
      Q2Bin->SetMean(bin_Q2[i]);

      NucDBErrorBar err0(  bin_err_d2p_exp[i] );
      NucDBErrorBar err1( bin_err_d2p_exp2[i] );
      NucDBErrorBar err2( bin_err_d2p_nach[i] );

      datapoint.SetStatError(err0);
      datapoint.SetValue(bin_d2p_exp[i]);
      datapoint.CalculateTotalError();
      d2p_meas->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(bin_Q2[i]+0.03);
      datapoint.SetStatError(err1);
      datapoint.SetValue(bin_d2p_exp2[i]);
      datapoint.CalculateTotalError();
      d2pWW_meas->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(bin_Q2[i]+0.06);
      datapoint.SetStatError(err2);
      datapoint.SetValue(bin_d2p_nach[i]+ d2p_lowx_model + d2p_highx_model);
      datapoint.CalculateTotalError();
      d2pbar_nachmom->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(bin_Q2[i]+0.06);
      datapoint.SetStatError(err2);
      datapoint.SetValue(bin_d2p_nach[i] + d2p_lowx_model + d2p_highx_model + d2p_tilde_elastic_model );
      datapoint.CalculateTotalError();
      d2p_nachmom->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(bin_Q2[i]+0.09);
      datapoint.SetStatError(err1); // find model error on low/high x
      datapoint.SetValue(bin_d2p_exp2[i] + d2p_lowx_model + d2p_lowx_model );
      datapoint.CalculateTotalError();
      d2p_bar->AddDataPoint( new NucDBDataPoint(datapoint) );
      d2p_total->AddDataPoint( new NucDBDataPoint(datapoint) );

      Q2Bin->SetMean(bin_Q2[i]-0.03);
      datapoint.SetStatError(err1); // find model error on low/high x
      datapoint.SetValue(bin_d2p_exp2[i] + d2p_lowx_model + d2p_lowx_model + ffs->d2p_el(bin_Q2[i]));
      datapoint.CalculateTotalError();
      d2p_all->AddDataPoint( new NucDBDataPoint(datapoint) );


      //std::cout << "d2p (model)       = " << bin_d2p_value[i] << std::endl;
      //std::cout << "d2p (exp)         = " << bin_d2p_exp[i] << " += " <<  TMath::Sqrt(err_d2p_exp[i])/double(1.0) << std::endl;
      //std::cout << "d2p (exp g2 only) = " << bin_d2p_exp2[i] << " += " << (bin_err_d2p_exp2[i])/double(bin_iXbins[i]) << std::endl;
      //std::cout << "d2p (0.01-0.35)   = " << d2p_lowx << std::endl;
      //std::cout << "d2p (nach)        = " << bin_d2p_nach[i] << " += " <<  (bin_err_d2p_nach[i])/double(bin_iXbins[i]) << std::endl;

      ofile << i << "\t" << bin_Q2[i];
      ofile      << "\t" << xLimits_0[i];
      ofile      << "\t" << xLimits_1[i];
      ofile      << "\t" << bin_d2p_exp[i]  << "\t" << (bin_err_d2p_exp[i])/double(bin_iXbins[i]);
      ofile      << "\t" << bin_d2p_exp2[i] << "\t" << (bin_err_d2p_exp2[i])/double(bin_iXbins[i]);
      ofile      << "\t" << d2p_lowx_model ;
      ofile      << "\t" << d2p_highx_model ;
      ofile <<  "\t" << bin_d2p_nach[i]  << "\t" << (bin_err_d2p_nach[i])/double(bin_iXbins[i]);
      ofile << std::endl;
   }


   Double_t  d2p_kang_lowx_model  = bbSFs->d2p_tilde(1.86,0.01,0.47);
   Double_t d2p_kang_highx_model = bbSFs->d2p_tilde(1.86,0.87,0.99);
   std::cout << " d2p_kang_lowx  "  << d2p_kang_lowx_model << std::endl;
   std::cout << " d2p_kang_highx " << d2p_kang_highx_model << std::endl;

   // --------------------------------------------------
   // Hoyoung's HMS data
   // -0.0087  +-0.0014  (0.47 < 0.87) , Q2=1.86
   // d2p_kang_lowx  0.00787601
   // d2p_kang_highx -0.000561791

   // --------------
   NucDBMeasurement * d2p_kang_highx = exp->GetMeasurement("d2p kang");
   if( !d2p_kang_highx ) {
      d2p_kang_highx = new NucDBMeasurement("d2p kang","d2p Kang");
      exp->AddMeasurement(d2p_kang_highx);
   }
   d2p_kang_highx->ClearDataPoints();
   d2p_kang_highx->SetExperimentName("SANE HMS");
   xBin->SetLimits(0.87,0.90);
   NucDBErrorBar err(0.00001);
   datapoint.SetStatError(err);
   datapoint.SetValue(d2p_kang_highx_model);
   datapoint.CalculateTotalError();
   d2p_kang_highx->AddDataPoint( new NucDBDataPoint(datapoint) );

   // --------------
   NucDBMeasurement * d2p_kang_lowx = exp->GetMeasurement("d2p lowx kang");
   if( !d2p_kang_lowx ) {
      d2p_kang_lowx = new NucDBMeasurement("d2p lowx kang","d2p lowx kang");
      exp->AddMeasurement(d2p_kang_lowx);
   }
   d2p_kang_lowx->ClearDataPoints();
   d2p_kang_lowx->SetExperimentName("SANE HMS");
   xBin->SetLimits(0.01,0.47);
   Q2Bin->SetValueSize(1.86,0.1);
   err = NucDBErrorBar(0.00001);
   datapoint.SetStatError(err);
   datapoint.SetValue(d2p_kang_lowx_model);
   datapoint.CalculateTotalError();
   d2p_kang_lowx->AddDataPoint( new NucDBDataPoint(datapoint) );

   // --------------
   NucDBMeasurement * d2p_kang = exp->GetMeasurement("d2p kang");
   if( !d2p_kang ) {
      d2p_kang = new NucDBMeasurement("d2p kang","d2p Kang");
      exp->AddMeasurement(d2p_kang);
   }
   d2p_kang->ClearDataPoints();
   d2p_kang->SetExperimentName("#bar{d}_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   //NucDBErrorBar err();
   err = NucDBErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087);
   datapoint.CalculateTotalError();
   d2p_kang->AddDataPoint( new NucDBDataPoint(datapoint) );

   NucDBMeasurement * d2p_bar_kang = exp->GetMeasurement("d2pbar kang");
   if( !d2p_bar_kang ) {
      d2p_bar_kang = new NucDBMeasurement("d2pbar kang","d2pbar kang");
      exp->AddMeasurement(d2p_bar_kang);
   }
   d2p_bar_kang->ClearDataPoints();
   d2p_bar_kang->SetExperimentName("d_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   //NucDBErrorBar err(0.0014);
   err = NucDBErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087+d2p_kang_lowx_model + d2p_kang_highx_model);
   datapoint.CalculateTotalError();
   d2p_bar_kang->AddDataPoint( new NucDBDataPoint(datapoint) );

   NucDBMeasurement * d2p_all_kang = exp->GetMeasurement("d2p all kang");
   if( !d2p_all_kang ) {
      d2p_all_kang = new NucDBMeasurement("d2p all kang","d2p all kang");
      exp->AddMeasurement(d2p_all_kang);
   }
   d2p_all_kang->ClearDataPoints();
   d2p_all_kang->SetExperimentName("d_{2} HMS");
   xBin->SetLimits(0.47,0.87);
   Q2Bin->SetValueSize(1.86,0.1);
   //NucDBErrorBar err(0.0014);
   err = NucDBErrorBar(0.0014);
   datapoint.SetStatError(err);
   datapoint.SetValue(-0.0087+d2p_kang_lowx_model + d2p_kang_highx_model + ffs->d2p_el_Nachtmann(1.86) );
   datapoint.CalculateTotalError();
   d2p_all_kang->AddDataPoint( new NucDBDataPoint(datapoint) );


   exp->Print();
   dbman->SaveExperiment(exp);


   // -----------------------------------------------------------------------
   // d2p

   TCanvas * c = new TCanvas("d2_measure","d2_measure");

   //c->cd(1);
   //gPad->SetGridy(true);

   mg_x2g2bar->Draw("a");
   mg_x2g2bar->SetTitle("x^{2} (2g_{1}+3g_{2}) ");
   mg_x2g2bar->GetXaxis()->SetLimits(0.0,1.0); 
   mg_x2g2bar->GetXaxis()->SetTitle("x"); 
   mg_x2g2bar->GetYaxis()->SetRangeUser(-0.1,0.1); 
   mg_x2g2bar->Draw("a");

   c->SaveAs(Form("results/d2_extract/d2_measure_0_%d_%s.png",aNumber,sfName.Data()));
   c->SaveAs(Form("results/d2_extract/d2_measure_0_%d_%s.pdf",aNumber,sfName.Data()));

   //c->cd(2);
   //gPad->SetGridy(true);

   mg_nacht->Draw("a");
   mg_nacht->SetTitle(" M_{2}^{3} integrand ");
   mg_nacht->GetXaxis()->SetTitle("x"); 
   mg_nacht->GetXaxis()->SetLimits(0.0,1.0); 
   mg_nacht->GetYaxis()->SetRangeUser(-0.1,0.1); 
   mg_nacht->Draw("a");

   //xg1p_4->DrawCopy("same");

   leg->Draw();

   c->SaveAs(Form("results/d2_extract/d2_measure_1_%d_%s.png",aNumber,sfName.Data()));
   c->SaveAs(Form("results/d2_extract/d2_measure_1_%d_%s.pdf",aNumber,sfName.Data()));

   return 0;
}
