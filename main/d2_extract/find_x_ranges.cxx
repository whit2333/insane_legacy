#include "asym/sane_data_bins.cxx"

Int_t find_x_ranges(Int_t aNumber = 8412) {

   Int_t paraFileVersion = aNumber;
   Int_t perpFileVersion = aNumber;
   Int_t para47Version   = aNumber;
   Int_t perp47Version   = aNumber;

   sane_data_bins();

   TFile * fcombined = new TFile(Form("data/A1A2_combined_%d.root",paraFileVersion),"UPDATE");
   //TFile * fcombined = new TFile(Form("data/g1g2_combined_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   TList * fg1_comb_list = (TList*)gROOT->FindObject("A1_combined_W");
   TList * fg2_comb_list = (TList*)gROOT->FindObject("A2_combined_W");
   if(!fg1_comb_list) return -11;
   if(!fg2_comb_list) return -12;

   //TFile * f_in_59 = new TFile(Form("data/A1A2_59_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   //TList * fg1_59_list = (TList*)gROOT->FindObject("A1_combined_W");
   //TList * fg2_59_list = (TList*)gROOT->FindObject("A2_combined_W");
   //if(!fg1_59_list) return -11;
   //if(!fg2_59_list) return -12;

   //TFile * f_in_47 = new TFile(Form("data/A1A2_47_noinel_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   //TList * fg1_47_list = (TList*)gROOT->FindObject(Form("A1asym-47-x-%d",0));
   //TList * fg2_47_list = (TList*)gROOT->FindObject(Form("A1asym-47-x-%d",0));
   //if(!fg1_47_list) return -13;
   //if(!fg2_47_list) return -14;
   //----------------------------------------------------

   TH1F * fA1 = 0;
   TH1F * fA2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;
  
   double error_thresh = 0.3;

   std::vector<double> x_min;
   std::vector<double> x_max;
   std::vector<double> bin_x_min;
   std::vector<double> bin_x_max;

   for(int jj = 0;jj<fg1_comb_list->GetEntries() ;jj++) {

      InSANEMeasuredAsymmetry         * g1Asym        = (InSANEMeasuredAsymmetry * )(fg1_comb_list->At(jj));
      InSANEMeasuredAsymmetry         * g2Asym        = (InSANEMeasuredAsymmetry * )(fg1_comb_list->At(jj));

      TH1F * fApara = &g1Asym->fAsymmetryVsW;
      TH1F * fAperp = &g2Asym->fAsymmetryVsW;

      InSANEAveragedKinematics1D * g1Kine = &g1Asym->fAvgKineVsW;
      InSANEAveragedKinematics1D * g2Kine = &g2Asym->fAvgKineVsW; 

      Int_t   xBinmax = fApara->GetNbinsX();
      Int_t   yBinmax = fApara->GetNbinsY();
      Int_t   zBinmax = fApara->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;

      bool found_min = false;
      bool found_max = false;
      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=0; i<= xBinmax+1; i++){
         for(Int_t j=0; j<= yBinmax+1; j++){
            for(Int_t k=0; k<= zBinmax+1; k++){

               bin   = fApara->GetBin(i,j,k);
               fApara->GetBinXYZ(bin,binx,biny,binz);

               //Double_t W     = g1Kine->fW->GetBinContent(bin);
               Double_t W     = fApara->GetBinCenter(bin);//paraKine->fx->GetBinContent(bin);
               Double_t W_low = fApara->GetBinLowEdge(bin);//paraKine->fx->GetBinContent(bin);
               Double_t W_high = W_low + fApara->GetBinWidth(bin);//paraKine->fx->GetBinContent(bin);
               //Double_t phi   = paraKine->fPhi->GetBinContent(bin);
               //Double_t Q2    = paraKine->fQ2->GetBinContent(bin);
               Double_t Q2    = g1Kine->fQ2.GetBinContent(bin);
               Double_t x     = g1Kine->fx.GetBinContent(bin);
               //Double_t theta = paraKine->fTheta->GetBinContent(bin);

               Double_t x_hi  = InSANE::Kine::xBjorken_WQ2(W_low,Q2);
               Double_t x_low = InSANE::Kine::xBjorken_WQ2(W_high,Q2);
               Double_t dx    = x_hi - x_low;

               Double_t A180  = fApara->GetBinContent(bin);
               Double_t A80   = fAperp->GetBinContent(bin);

               Double_t eA180  = fApara->GetBinError(bin);
               Double_t eA80   = fAperp->GetBinError(bin);

               if( TMath::IsNaN(eA180) ) eA180 = 10000;
               if( TMath::IsNaN(eA80) ) eA80 = 10000;
               if( eA180 == 0.0 ) eA180 = 10000;
               if( eA80  == 0.0 ) eA80 = 10000;
               if( A180 == 0.0 ) eA180 = 10000;
               if( A80  == 0.0 ) eA80 = 10000;

               if( eA180 > error_thresh || eA80 > error_thresh) {
                  // bad bin

                  // if we have a min already
                  if(found_max) {
                     if(!found_min) {
                        std::cout << jj << " : " << "xmin("<<bin<<") = " << x_low << ", A180 = " << A180 << " +- "  << eA180 << std::endl;
                        found_min = true;
                        x_min.push_back(x_hi);
                        bin_x_min.push_back(bin);
                     }
                  }
               } else {
                  if(!found_max) {
                     std::cout << jj << " : " << "xmax("<<bin<<") = " << x_hi << ", A180 = " << A180 << " +- "  << eA180 << std::endl;
                     found_max = true;
                     x_max.push_back(x_low);
                     bin_x_max.push_back(bin);
                  }
               }

            }
         }
      }

      if(!found_min){
         x_min.push_back(0.25);
         bin_x_min.push_back(bin);
      }
      if(!found_max){
         x_max.push_back(0.85);
         bin_x_max.push_back(bin);
      }
   }

   // ------------------------------------
   TFile * fout = new TFile("d2/bin_limits.root","UPDATE");

   TList * fBinLimits = new TList();
   fBinLimits->SetName("bin limits");

   TList * fBinMaxs = new TList();
   fBinMaxs->SetName("x bin minima");
   TList * fBinMins = new TList();
   fBinMins->SetName("x bin maxima");

   fBinLimits->Add(fBinMins);
   fBinLimits->Add(fBinMaxs);


   for(int i = 0; i < x_max.size(); i++ ) {

      TParameter<double> * pmin =  new TParameter<double>(Form("%d xmin",i),x_min[i]);
      fBinMins->Add(pmin);
      TParameter<double> * pmax =  new TParameter<double>(Form("%d xmax",i),x_max[i]);
      fBinMaxs->Add(pmax);

   }

   fout->WriteObject(fBinLimits,"bin limits");

   fout->Close();
   return 0;
}

