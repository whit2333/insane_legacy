class  d2p_elastic_integral {
   public:
      InSANEFormFactors                 * ffs;
      double operator() (double *x, double *p) { return( ffs->d2p_el(x[0])); }
};

class  d2n_elastic_integral {
   public:
      InSANEFormFactors                 * ffs;
      double operator() (double *x, double *p) { return( ffs->d2n_el(x[0])); }
};

class  d2p_I {
   public:
      InSANEPolarizedStructureFunctions * psfs;
      double operator() (double *x, double *p) { return( ffs->d2p_el(x[0]) + psfs->d2p_tilde(x[0],p[0],p[1])); }
};

class  d2n_I {
   public:
      InSANEPolarizedStructureFunctions * psfs;
      double operator() (double *x, double *p) { return( ffs->d2n_el(x[0]) + psfs->d2n_tilde(x[0],p[0],p[1])); }
};

 
Int_t d2p_existing(Int_t aNumber = 2405) {

   NucDBManager * dbman = NucDBManager::GetManager();

   d2p_elastic_integral  * fobj  = new d2p_elastic_integral();
   d2n_elastic_integral * fobj2  = new d2n_elastic_integral();
   d2p_I * fd2pI  = new d2p_I();
   d2n_I * fd2nI  = new d2n_I();

   TF1                 * d2p_el = new TF1("d2p_el",fobj,0.1,10,0,"d2p_elastic_integral");
   TF1                 * d2n_el = new TF1("d2n_el",fobj2,0.1,10,0,"d2n_elastic_integral");
   TF1                 * d2p_total = new TF1("d2p_int",fd2pI,0.1,10,2,"d2p_I");
   TF1                 * d2n_total = new TF1("d2n_int",fd2nI,0.1,10,2,"d2n_I");
   d2p_total->SetParameters(0.01,0.9);
   d2n_total->SetParameters(0.01,0.9);

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 
   psfs = fman->CreatePolSFs(6);
   ffs  = fman->CreateFFs(1);

   TCanvas * c = new TCanvas();
   TGraphErrors * gr = 0;

   d2p_total->SetLineColor(1);
   d2p_total->SetLineStyle(2);

   d2n_total->SetLineColor(4);
   d2n_total->SetLineStyle(2);

   d2p_el->SetLineColor(1);
   d2n_el->SetLineColor(4);

   // -------------------------------------------------
   //
   TLegend * leg = new TLegend(0.65,0.65,0.88,0.88);
   leg->SetFillStyle(0);
   leg->SetBorderSize(0);

   TLegend * leg2 = new TLegend(0.45,0.65,0.65,0.88);
   leg2->SetFillStyle(0);
   leg2->SetBorderSize(0);

   // ---------------------------------------------
   TList       * calc_meas = dbman->GetMeasurementCalculations("d2p");
   NucDBMeasurement * lattice_meas = NucDB::FindExperiment("Gockeler",calc_meas);
   lattice_meas->Print("data");
   gr = lattice_meas->BuildGraph("Qsquared");
   gr->SetMarkerStyle(24);
   gr->SetLineWidth(2);
   TMultiGraph * mg_calc   = new TMultiGraph();//NucDB::CreateMultiGraph(calc_meas,"Qsquared");
   mg_calc->Add(gr,"p");
   //NucDB::FillLegend(leg,calc_meas,mg_calc);
   leg->AddEntry(gr,lattice_meas->GetExperimentName(),"lp");

   TList       * meas      = dbman->GetMeasurements("d2p");
   TMultiGraph * mg_meas   = NucDB::CreateMultiGraph(meas,"Qsquared");
   NucDB::FillLegend(leg,meas,mg_meas);

   TMultiGraph * mg = new TMultiGraph();
   NucDBMeasurement * osipenko = NucDB::FindExperiment("Osipenko",calc_meas);
   if( osipenko ) {
      //osipenko->Print("data");
      TGraph * gr_sys = osipenko->BuildGraph("Qsquared",true);
      gr_sys->SetFillColorAlpha(kGreen,0.1);
      //mg->Add(gr_sys,"e3");
      //TLegendEntry * leg_entry = (TLegendEntry*) leg->GetListOfPrimitives()->At(0);
      //leg_entry->SetFillColorAlpha(kGreen,0.1);
      //leg_entry->SetOption("lpf");
      //leg_entry->SetObject(gr_sys);
   }

   mg->Add(mg_calc);
   mg->Add(mg_meas);

   // --------------------------------
   // Calculated curves
   TMultiGraph * mg_curves = new TMultiGraph();

   gr1 = new TGraph(d2p_el->DrawCopy("goff")->GetHistogram());
   mg_curves->Add(gr1,"l");
   leg->AddEntry(gr1,"elastic","l");

   TGraph * gr1 = new TGraph(d2p_total->DrawCopy("goff")->GetHistogram());
   mg_curves->Add(gr1,"l");
   leg->AddEntry(gr1,"inelastic","l");

   // --------------------------------
   TCanvas * c = new TCanvas();

   mg->Add(mg_curves);
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.1,11.0);
   mg->GetYaxis()->SetRangeUser(-0.03,0.05);
   mg->GetXaxis()->CenterTitle(true);
   mg->GetXaxis()->SetTitle("Q^{2} (GeV^{2})");

   leg->Draw();

   c->SaveAs(Form("results/sane_results/d2p_existing_0_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/d2p_existing_0_%d.pdf",aNumber));

   //NucDBExperiment * exp = dbman->GetExperiment("SANE");
   //NucDBMeasurement * d2p_sane = exp->GetMeasurement("d2p");
   //if(!d2p_sane) { 
   //   d2p_sane = new NucDBMeasurement("d2p","d2p");
   //}

   // -------------------------------
   c = new TCanvas();
   TMultiGraph * mg2 = new TMultiGraph();//dbman->GetMultiGraph("d2p","Qsquared");

   // BETA 
   TList       * sane_meas = 0;
   TMultiGraph * mg_sane   = 0;

   sane_meas = dbman->GetMeasurements("d2p measured");
   if( sane_meas ) {
      mg_sane   = NucDB::CreateMultiGraph(sane_meas,"Qsquared");
      //NucDB::FillLegend(leg2,sane_meas,mg_sane);
      //mg->Add(mg_sane);
   }
   sane_meas = dbman->GetMeasurements("d2pWW measured");
   if( sane_meas ) {
      mg_sane   = NucDB::CreateMultiGraph(sane_meas,"Qsquared");
      //NucDB::FillLegend(leg2,sane_meas,mg_sane);
      //mg->Add(mg_sane);
   }
   sane_meas = dbman->GetMeasurements("d2p Nachtmann");
   if( sane_meas ) {
      mg_sane   = NucDB::CreateMultiGraph(sane_meas,"Qsquared");
      //NucDB::FillLegend(leg2,sane_meas,mg_sane);
      //mg->Add(mg_sane);
   }
   sane_meas = dbman->GetMeasurements("d2pbar");
   if( sane_meas ) {
      mg_sane   = NucDB::CreateMultiGraph(sane_meas,"Qsquared");
      //NucDB::FillLegend(leg2,sane_meas,mg_sane);
      //mg->Add(mg_sane);
   }
   sane_meas = dbman->GetMeasurements("d2p total");
   if( sane_meas ) {
      mg_sane   = NucDB::CreateMultiGraph(sane_meas,"Qsquared");
      NucDB::FillLegend(leg2,sane_meas,mg_sane);
      mg->Add(mg_sane);
      gr = (TGraphErrors*) mg_sane->GetListOfGraphs()->At(0);
      gr->SetMarkerStyle(21);

   }

   // HMS 
   TList       * hms_meas = 0;
   TMultiGraph * mg_hms   = 0;

   hms_meas      = dbman->GetMeasurements("d2pbar kang");
   if(hms_meas) {
      mg_hms      = NucDB::CreateMultiGraph(hms_meas,"Qsquared");
      NucDB::FillLegend(leg2,hms_meas,mg_hms);
      mg->Add(mg_hms);
   }

   hms_meas      = dbman->GetMeasurements("d2p kang");
   if(hms_meas) {
      mg_hms      = NucDB::CreateMultiGraph(hms_meas,"Qsquared");
      //NucDB::FillLegend(leg2,hms_meas,mg_hms);
      //mg->Add(mg_hms);
   }

   //------------------------
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.1,11.0);
   mg->GetYaxis()->SetRangeUser(-0.03,0.05);
   mg->GetXaxis()->SetTitle("Q^{2} (GeV^{2})");

   leg->Draw();
   leg2->Draw();

   //d2p_total->Draw("same");
   ////d2n_total->Draw("same");
   //d2p_el->Draw("same");
   ////d2n_el->Draw("same");

   c->Update();
   c->SaveAs(Form("results/sane_results/d2p_existing_1_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/d2p_existing_1_%d.pdf",aNumber));

   return 0;
}
