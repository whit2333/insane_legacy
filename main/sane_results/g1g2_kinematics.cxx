Int_t g1g2_kinematics(Double_t Qsq = 15.0,Double_t QsqBinWidth=7.0,Int_t aNumber = 2008){

   gSystem->mkdir(Form("results/sane_results/%d",aNumber));
   Int_t nMerge = 0;
   TCanvas * c = new TCanvas("xg1p","xg1p");

   // Create Legend
   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   NucDBExperiment * exp = manager->GetExperiment("SANE");
   if(!exp) return -3;

   NucDBMeasurement * meas = exp->GetMeasurement("g1p");
   meas->Print("data");

   TGraph * gr = meas->BuildKinematicGraph("x","Qsquared");
   gr->SetFillColorAlpha(kRed,0.5);
   gr->Draw("ap2e");

   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_xQ2.svg",aNumber));
   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_xQ2.png",aNumber));
   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_xQ2.pdf",aNumber));

   gr = meas->BuildKinematicGraph("W","Qsquared");
   gr->SetFillColorAlpha(kRed,0.5);
   gr->Draw("ap2e");

   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_WQ2.png",aNumber));
   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_WQ2.svg",aNumber));
   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_WQ2.pdf",aNumber));

   gr = meas->BuildKinematicGraph("x","W");
   gr->SetFillColorAlpha(kRed,0.5);
   gr->Draw("ap2e");

   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_xW.png",aNumber));
   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_xW.svg",aNumber));
   c->SaveAs(Form("results/sane_results/%d/g1p_kinematics_xW.pdf",aNumber));


   return 0;
}
