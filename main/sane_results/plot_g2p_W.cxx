/** Plots models and data for g2p at Qsq. */
Int_t plot_g2p_W( Int_t aNumber = 2008, Double_t Qsq = 1.30,Double_t QsqBinWidth=0.1){

   TCanvas * c = new TCanvas("xg2p","xg2p",1200,800);

   Int_t nMerge = 1;

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 

   // Create Legend
   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   InSANEPolarizedStructureFunctionsFromPDFs *pSFsD  = new InSANEPolarizedStructureFunctionsFromPDFs(); 
   pSFsD->SetPolarizedPDFs( new StatisticalPolarizedPDFs); 

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg2p = new TF1("xg2p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   TF1 * xg2pA = new TF1("xg2pA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   TF1 * xg2pB = new TF1("xg2pB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   TF1 * xg2pC = new TF1("xg2pC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   TF1 * xg2pD = new TF1("xg2pC", pSFsD, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");

   xg2p->SetParameter(0,Qsq);
   xg2pA->SetParameter(0,Qsq);
   xg2pB->SetParameter(0,Qsq);
   xg2pC->SetParameter(0,Qsq);
   xg2pD->SetParameter(0,Qsq);

   xg2p->SetLineColor(1);
   xg2pA->SetLineColor(3);
   xg2pB->SetLineColor(kOrange-1);
   xg2pC->SetLineColor(kCyan+1);
   xg2pD->SetLineColor(kMagenta);

   Int_t width = 3;
   xg2p->SetLineWidth(width);
   xg2pA->SetLineWidth(width);
   xg2pB->SetLineWidth(width);
   xg2pC->SetLineWidth(width);
   xg2pD->SetLineWidth(width);

   xg2p->SetLineStyle(1);
   xg2pA->SetLineStyle(1);
   xg2pB->SetLineStyle(1);
   xg2pC->SetLineStyle(1);
   xg2pD->SetLineStyle(1);

   xg2pA->SetMaximum(0.1);
   xg2pA->SetMinimum(-0.03);

   xg2pA->Draw();

   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   // Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   // Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g2pQsq1 = new NucDBMeasurement("g2pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   NucDBMeasurement * g2pSANE = new NucDBMeasurement("g2pSANE","g_{1}^{p} SANE");

   TF2 * xf = new TF2("xf","x*x*y",0,1,-1,1);
   TList * listOfMeas = manager->GetMeasurements("g2p");
   TMultiGraph * mg = new TMultiGraph();
   TMultiGraph * mg_world_data = new TMultiGraph();

   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g2pMeas = (NucDBMeasurement*)listOfMeas->At(i);

       if( !strcmp(g2pMeas->GetExperimentName(),"SANE"))  {
          g2pSANE->AddDataPoints(g2pMeas->GetDataPoints());
          continue;
       } else {

       TList * points = g2pMeas->FilterWithBin(Qsqbin);
       g2pQsq1->AddDataPoints(points);

       TGraphErrors * graph = g2pMeas->BuildGraph("W");
       //graph->Apply(xf);
       graph->SetMarkerStyle(20+i);
       graph->SetMarkerSize(1.6);
       graph->SetMarkerColor(kBlue-2-i);
       graph->SetLineColor(kBlue-2-i);
       graph->SetLineWidth(1);

       mg->Add(graph,"ep");
       mg_world_data->Add(graph,"ep");

       leg->AddEntry(graph,Form("%s %s",g2pMeas->GetExperimentName(),g2pMeas->GetTitle() ),"lp");
       }

   }

   TGraphErrors * gr = g2pQsq1->BuildGraph("W");
   //gr->Apply(xf);
   gr->SetMarkerColor(3);
   gr->SetLineColor(3);
   mg->Add(gr,"ep");
   leg->AddEntry(gr,Form("%s %s","All g2p data",g2pQsq1->GetTitle() ),"ep");

   std::vector<double> sane_Q2_values;
   g2pSANE->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   std::cout << sane_Q2_values.size() << std::endl;
   TList * sane_g2p_meas = new TList();
   for(int i = 0; i<sane_Q2_values.size(); i++) {
      NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);
      NucDBMeasurement * g2pSANE_i = new NucDBMeasurement(Form("g2pSANE%d",i),"g_{1}^{p} SANE");
      g2pSANE_i->AddDataPoints(g2pSANE->FilterWithBin(avar));
      sane_g2p_meas->Add(g2pSANE_i);
      std::cout << i << std::endl;
   }
   std::cout << sane_g2p_meas->GetEntries() << " Q2 bins" << std::endl;

   NucDBMeasurement * aMeas = 0;
   
   // Q2 bins
   for( int i = 0; i<4; i++ ){

      TMultiGraph* mgtemp = new TMultiGraph();
      mgtemp->Add(mg_world_data);

      aMeas = (NucDBMeasurement*)sane_g2p_meas->At(i);
      aMeas->MergeDataPoints(nMerge,"x",true);
      gr = aMeas->BuildGraph("W");
      //gr->Apply(xf);
      gr->SetMarkerColor(kRed+i);
      gr->SetLineColor(kRed+i);
      gr->SetLineWidth(2);
      gr->SetMarkerStyle(gNiceMarkerStyle[i]);
      mg->Add(gr,"ep");
      mgtemp->Add(gr,"ep");

      aMeas->SortBy("W");
      gr = aMeas->BuildSystematicErrorBand("W",-0.2);
      gr->SetMarkerColor(kRed+i);
      gr->SetFillColorAlpha(kRed+i,0.4);
      gr->SetLineWidth(2);
      gr->SetMarkerStyle(gNiceMarkerStyle[i]);
      //mg->Add(gr,"e3");
      //mgtemp->Add(gr,"e3");

      // --------------------------
      //c->Clear();
      gPad->SetGridy(true);
      mgtemp->Draw("a");
      mgtemp->GetYaxis()->SetRangeUser(-0.5,0.7);
      mgtemp->GetXaxis()->SetLimits(1.0,3.0);
      //A1pSANE->Print("data");
      leg->Draw();
      t->DrawLatex(0.16,0.95,Form("g_{2}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)aMeas->GetBinnedVariableMean("Qsquared")));
      c->SaveAs(Form("results/sane_results/%d/plot_g2p_W_%d_%d.png",aNumber,nMerge,i));
   }

   //g2pSANE->SortBy("Qsquared");
   //g2pSANE->SortBy("x");
   g2pSANE->Print();

   //TMultiGraph * mg2 = g2pSANE->BuildGraphUnique("x","Qsquared");
   //for(int i = 0; i<mg2->GetListOfGraphs()->GetEntries(); i++) {
   //   TGraphErrors * gr = (TGraphErrors*)mg2->GetListOfGraphs()->At(i);
   //   //gr->Apply(xf);
   //}
   //mg->Add(mg2);

   gPad->SetGridy(true);
   g2pSANE->PrintBreakDown("Qsquared");
   // --------------------------
   // 
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.5,0.7);
   mg->GetXaxis()->SetLimits(1.0,3.0);
   //g2pSANE->Print("data");

   //xg2pB->Draw("same");
   //xg2pC->Draw("same");
   //xg2pD->Draw("same");

   //xg2p->SetParameter(0,3.0);
   //xg2p->Draw("same");


   // Complete the Legend 
   //leg->AddEntry(xg2p,"xg2p BB ","l");
   //leg->AddEntry(xg2pA,"xg2p DNS2005 ","l");
   //leg->AddEntry(xg2pB,"xg2p AAC ","l");
   //leg->AddEntry(xg2pC,"xg2p GS ","l");
   //leg->AddEntry(xg2pD,"xg2p Stat ","l");
   leg->Draw();

   t->DrawLatex(0.16,0.95,Form("g_{2}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));


   c->SaveAs(Form("results/sane_results/%d/plot_g2p_W_%d.png",aNumber,nMerge));
   c->SaveAs(Form("results/sane_results/%d/plot_g2p_W_%d.pdf",aNumber,nMerge));

   return(0);
}
