Int_t A2p_vs_W(Int_t aNumber  = 1){

   Int_t nMerge = 1;

   NucDBManager * manager = NucDBManager::GetManager();
   if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
      Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
      return -1;
   }
   sane_data_bins();
   // --------------------------------------------------------

   NucDBBinnedVariable * Qsqbin2 = new NucDBBinnedVariable("Qsquared","Q2",40.0,37.0);
   NucDBBinnedVariable * Wbin2   = new NucDBBinnedVariable("W","Wbin",40.0,36.0);

   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);
   leg->SetFillStyle(0);

   TLegend     * leg2  = new TLegend(0.18,0.65,0.45,0.87);
   leg2->SetBorderSize(0);
   leg2->SetFillStyle(0);

   TLegend     * leg0  = new TLegend(0.25+0.4,0.18,0.5+0.4,0.45);
   leg0->SetBorderSize(0);
   leg0->SetFillStyle(0);

   TLegend     * leg4  = new TLegend(0.25+0.4,0.18,0.5+0.4,0.45);
   leg4->SetBorderSize(0);
   leg4->SetFillStyle(0);

   TMultiGraph * mg    = new TMultiGraph();
   TMultiGraph * mg_2  = new TMultiGraph();
   TMultiGraph * mg_3  = new TMultiGraph();

   // -----------------------------------------------------------
   // SANE data
   NucDBMeasurement * A2pSANE = manager->GetExperiment("SANE")->GetMeasurement("MergedA2p");

   std::vector<double> sane_Q2_values;
   A2pSANE->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   //std::cout << sane_Q2_values.size() << std::endl;
   TList * sane_A2p_meas = new TList();
   for(int i = 0; i<sane_Q2_values.size(); i++) {
      NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);
      NucDBMeasurement * A2pSANE_i = new NucDBMeasurement(Form("A2pSANE%d",i),"g_{1}^{p} SANE");
      A2pSANE_i->AddDataPoints(A2pSANE->FilterWithBin(avar));
      sane_A2p_meas->Add(A2pSANE_i);
      std::cout << i << std::endl;
   }
   std::cout << sane_A2p_meas->GetEntries() << " Q2 bins" << std::endl;
   
   Int_t nMergeMax = 2;
   for(int i = 0; i< 4;i++) {
      // Q2 bin 0
      NucDBMeasurement * aMeas =  (NucDBMeasurement*)sane_A2p_meas->At(i);
      //aMeas->MergeNeighboringDataPoints(nMergeMax,"W",0.1,"Qsquared",0.5,true);
      TGraph * gr = aMeas->BuildGraph("W");
      //gr->Apply(xf);
      gr->SetMarkerColor(kRed+i);
      gr->SetLineColor(kRed+i);
      gr->SetLineWidth(2);
      gr->SetMarkerStyle(gNiceMarkerStyle[i]);
      mg->Add(gr,"ep");
      mg_3->Add(gr,"ep");
   }

   TMultiGraph * mg2 = new TMultiGraph();
   gr =  A2pSANE->BuildGraph("W");
   gr->SetLineColor(2);
   gr->SetMarkerColor(2);
   gr->SetMarkerStyle(20);
   mg2->Add(gr,"ep");

   // ---------------------------------------
   // Now get the world data
   Double_t Q2_min = 1.0;
   Double_t Q2_max = 8.0;
   NucDBBinnedVariable * Qsqbin = 0;
   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(4);
   Qsqbin->SetTitle(Form("%4.1f<Q^{2}<%4.1f",Qsqbin->GetBinMinimum(),Qsqbin->GetBinMaximum()));

   //leg->SetHeader(Form("World data %s",Qsqbin->GetTitle()));
   //leg0->SetHeader(Form("World Data %s",Qsqbin->GetTitle()));
   //NucDBMeasurement * measg1_saneQ2 = new NucDBMeasurement("measg1_saneQ2",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   Double_t Q2 = 4.0;//Qsqbin->GetAverage();
   NucDBBinnedVariable *xbin = new NucDBBinnedVariable("x","x");
   xbin->SetBinMinimum(0.0);
   xbin->SetBinMaximum(0.6);
   xbin->SetMean((0.0 + 0.6)/2.0);
   xbin->SetAverage((0.0 + 0.6)/2.0);

   NucDBBinnedVariable *Wbin = new NucDBBinnedVariable("W","W");
   Wbin->SetBinMinimum(1.4);
   Wbin->SetBinMaximum(20.0);

   TList * measurementsList =  new TList(); //manager->GetMeasurements("A2p");
   //TList * measurementsList =  manager->GetMeasurements("A2p");
   NucDBExperiment *  exp = 0;
   NucDBMeasurement * ames = 0; 

   // SLAC E143
   exp = manager->GetExperiment("SLAC E143"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155
   exp = manager->GetExperiment("SLAC E155"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);

   // SLAC E155x
   exp = manager->GetExperiment("SLAC E155x"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);


   // SMC
   exp = manager->GetExperiment("SMC"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);


   // CLAS limited to x < 0.6
   NucDBMeasurement * clas_meas = 0;
   exp = manager->GetExperiment("CLAS-E93009"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames){
      ames->SetExperimentName("CLAS-E93009");
      ames->ApplyFilterWithBin(Qsqbin);//ApplyFilterWithBin(xbin);
      ames->ApplyFilterWithBin(Wbin);
      //std::vector<std::string> clas_filter;
      //clas_filter.push_back
      TList * ls = ames->MergeNeighboringDataPoints(4,"W",0.1);
      //ames->Print("data");
      ames->AddDataPoints(ls,true);
      measurementsList->Add(ames);
      clas_meas = ames;
   }
   // HERMES
   exp = manager->GetExperiment("HERMES"); 
   if(exp) ames = exp->GetMeasurement("A2p");
   if(ames) measurementsList->Add(ames);


   TMultiGraph * mg_clas   = new TMultiGraph();
   TMultiGraph * MG        = new TMultiGraph();
   TMultiGraph * MGA1_data = new TMultiGraph();

   Int_t markers[] = {22,32,23,33,21,35,36,37};
   for (int i = 0; i < measurementsList->GetEntries(); i++) {
      NucDBMeasurement *A2pMeas = (NucDBMeasurement*)measurementsList->At(i);
      TList * plist =0;
      //if(A2pMeas == ames) { 

      //} else {
         plist = A2pMeas->ApplyFilterWithBin(Qsqbin);
         if(!plist) continue;
         if(plist->GetEntries() == 0 ) continue;
      //}
         plist = A2pMeas->ApplyFilterWithBin(Wbin);
         if(!plist) continue;
         if(plist->GetEntries() == 0 ) continue;

      // Get TGraphErrors object 
      TGraphErrors *graph        = A2pMeas->BuildGraph("W");
      graph->SetMarkerStyle(markers[i]);
      graph->SetMarkerColor(1);
      graph->SetMarkerSize(1.2);
      graph->SetLineColor(1);
      // Set legend title 
      leg->AddEntry(graph,A2pMeas->GetExperimentName(),"p");
      leg0->AddEntry(graph,A2pMeas->GetExperimentName(),"p");
      leg4->AddEntry(graph,A2pMeas->GetExperimentName(),"p");
      // Add to TMultiGraph 
      MG->Add(graph); 
      if(A2pMeas == clas_meas){
         mg_clas->Add(graph,"ep"); 
      } else {
         MGA1_data->Add(graph,"ep"); 
      }

      //measg1_saneQ2->AddDataPoints(measg1->FilterWithBin(Qsqbin));
   }

   // ----------------------------------------------------
   TCanvas * c = new TCanvas();

   MG->Add(mg);
   // Draw everything 
   MG->Draw("AP");
   MG->SetTitle("");
   MG->GetXaxis()->SetTitle("W");
   MG->GetXaxis()->CenterTitle();
   MG->GetYaxis()->SetTitle("");
   MG->GetYaxis()->CenterTitle();
   MG->GetXaxis()->SetLimits(1.0,3.40); 
   MG->GetYaxis()->SetRangeUser(-0.7,0.8); 

   leg->Draw();


   // --------------------------------------------------------
   // Nicer looking individiual plots

   Double_t model_min = 1.5;
   Double_t model_max = 10.0;

   TMultiGraph * MGA1 = new TMultiGraph();

   MGA1->Add(MGA1_data,"P");

   MGA1->Draw("A");
   MGA1->SetTitle("A_{2}^{p}");
   MGA1->GetXaxis()->SetTitle("W");
   MGA1->GetXaxis()->CenterTitle();
   MGA1->GetYaxis()->SetTitle("");
   MGA1->GetYaxis()->CenterTitle();
   MGA1->GetXaxis()->SetLimits(1.0,3.4); 
   MGA1->GetYaxis()->SetRangeUser(-0.7,0.8); 
   leg0->Draw();

   // x -> Model predictions
   TLatex l;
   l.SetTextSize(0.03);
   l.SetTextAlign(12);  //centered
   l.SetTextFont(132);
   l.SetTextAngle(20);
   //l.DrawLatex(1.0,0.59,"#leftarrow DSE");
   //l.SetTextAngle(-20);
   //l.DrawLatex(1.0,5.0/9.0,"#leftarrow SU(6)");
   //l.DrawLatex(1.0,0.77,"#leftarrow NJL");
   //l.DrawLatex(1.0,1.0,"#leftarrow pQCD");

   c->Update();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_0.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_0.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_0.svg",aNumber));

   // Add the CLAS data too
   MGA1->Add(mg_clas);

   c->Update();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_1.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_1.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_1.svg",aNumber));

   // -----------------------------------------------------------------------------
   // Models
   TLegend * legmod = new TLegend(0.17,0.60,0.40,0.88);
   legmod->SetHeader(Form("Models at Q^{2}=%.1f",Q2));
   legmod->SetFillColor(0);
   legmod->SetFillStyle(0);
   legmod->SetBorderSize(0);

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager();
   TList * list_of_models = new TList();
   TF1 * aModel = 0;

   // BBS
   fman->CreateSFs(5);
   InSANEStructureFunctions * BBSsf = fman->GetStructureFunctions();
   fman->CreatePolSFs(5);
   InSANEPolarizedStructureFunctions * BBSpsf = fman->GetPolarizedStructureFunctions();
   InSANEVirtualComptonAsymmetries *Asym3 = new InSANEVirtualComptonAsymmetries(); 
   Asym3->SetUnpolarizedSFs(BBSsf);
   Asym3->SetPolarizedSFs(BBSpsf);  
   aModel = new TF1("Model3",Asym3,&InSANE_VCSABase::EvaluateA2p_W,
         model_min,model_max,1,"InSANE_VCSABase","EvaluateA2p_W");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue+1);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"BBS","l");

   // LCWF 
   fman->CreateSFs(7);
   InSANEStructureFunctions * LCWFsf = fman->GetStructureFunctions();
   fman->CreatePolSFs(7);
   InSANEPolarizedStructureFunctions * LCWFpsf = fman->GetPolarizedStructureFunctions();
   InSANEVirtualComptonAsymmetries *Asym7 = new InSANEVirtualComptonAsymmetries(); 
   Asym7->SetUnpolarizedSFs(LCWFsf);
   Asym7->SetPolarizedSFs(LCWFpsf);  
   aModel = new TF1("Model7",Asym7,&InSANE_VCSABase::EvaluateA2p_W,
         model_min,model_max,1,"InSANE_VCSABase","EvaluateA2p_W");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"LCWF","l");

   // Statistical 
   fman->CreateSFs(6);
   InSANEStructureFunctions * STATsf = fman->GetStructureFunctions();
   fman->CreatePolSFs(6);
   InSANEPolarizedStructureFunctions * STATpsf = fman->GetPolarizedStructureFunctions();
   InSANEVirtualComptonAsymmetries *Asym4 = new InSANEVirtualComptonAsymmetries(); 
   Asym4->SetUnpolarizedSFs(STATsf);  
   Asym4->SetPolarizedSFs(STATpsf);
   aModel = new TF1("Model4",Asym4,&InSANE_VCSABase::EvaluateA2p_W,
         model_min,model_max,1,"InSANE_VCSABase","EvaluateA2p_W");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"Statistical","l");

   TMultiGraph * mg_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      aModel->Draw("same");
      //TGraph * grmod = new TGraph(aModel->DrawCopy("goff")->GetHistogram());
      //mg_models->Add(grmod,"l");
      //MGA1->Add(grmod,"l");
      //grmod->Draw("l");
   }
   //MGA1->Add(mg_models);
   //mg_models->Draw("l");
   legmod->Draw();

   c->Update();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_2.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_2.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_2.svg",aNumber));

   // ------------------------------------------------------
   // WHITFIT
   //WFittingStatDistributions4 * PHDs = new WFittingStatDistributions4();
   //PHDs->InitFitParameters(parfile);//"inputs/stat_params4.txt");//,aNumber));
   //PHDs->CalculateConstraints();

   ////InSANEUnpolarizedPDFsFromPHDs * wpdfs = new InSANEUnpolarizedPDFsFromPHDs();
   ////wpdfs->SetPHDs(PHDs);
   ////InSANEPolarizedPDFsFromPHDs * wpolpdfs = new InSANEPolarizedPDFsFromPHDs();
   ////wpolpdfs->SetPHDs(PHDs);

   //StatisticalUnpolarizedPDFs * wpdfs = new StatisticalUnpolarizedPDFs();
   //StatisticalPolarizedPDFs * wpolpdfs = new StatisticalPolarizedPDFs();

   //InSANEStructureFunctionsFromPDFs * wSFs = new InSANEStructureFunctionsFromPDFs();
   //wSFs->SetUnpolarizedPDFs(wpdfs);
   //InSANEPolarizedStructureFunctionsFromPDFs * wPolSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   //wPolSFs->SetPolarizedPDFs(wpolpdfs);

   //InSANEVirtualComptonAsymmetries *AsymW = new InSANEVirtualComptonAsymmetries(); 
   //AsymW->SetUnpolarizedSFs(wSFs);
   //AsymW->SetPolarizedSFs(wPolSFs);  
   //aModel = new TF1("Model81",AsymW,&InSANE_VCSABase::EvaluateA2p_W,
   //      0.01,0.99, 1,"InSANE_VCSABase","EvaluateA2p_W");
   //aModel->SetParameter(0,Q2);
   //aModel->SetLineStyle(1);
   //aModel->SetLineWidth(3);
   //aModel->SetLineColor(1);
   ////list_of_models->Add(aModel);
   ////legmod->AddEntry(aModel,"Whit Fit","l");

   TMultiGraph * mg_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      aModel->Draw("same");
      //TGraph * grmod = new TGraph(aModel->DrawCopy("goff")->GetHistogram());
      //mg_models->Add(grmod,"l");
      //MGA1->Add(grmod,"l");
      //grmod->Draw("l");
   }
   //MGA1->Add(mg_models);
   //mg_models->Draw("l");
   legmod->Draw();

   c->Update();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_3.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_3.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_3.svg",aNumber));

   // -------------------------------------------------------------
   // Add more models

   // DSSV + F1F209 
   fman->CreatePolSFs(0);
   InSANEPolarizedStructureFunctions * DSSVpsf = fman->GetPolarizedStructureFunctions();
   fman->CreateSFs(0);
   InSANEStructureFunctions * F1F209sf = fman->GetStructureFunctions();
   //fman->CreatePolSFs(0);
   //InSANEPolarizedStructureFunctions * DSSVpsf = fman->GetPolarizedStructureFunctions();
   InSANEVirtualComptonAsymmetries *Asym2 = new InSANEVirtualComptonAsymmetries(); 
   Asym2->SetUnpolarizedSFs(F1F209sf);  
   Asym2->SetPolarizedSFs(DSSVpsf);
   aModel = new TF1("Model2",Asym2,&InSANE_VCSABase::EvaluateA2p_W,
         model_min,model_max, 1,"InSANE_VCSABase","EvaluateA2p_W");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   //list_of_models->Add(aModel);
   //legmod->AddEntry(aModel,"F1F209+DSSV","l");

   // CTEQ + AAC 
   fman->CreateSFs(12);
   InSANEStructureFunctions * CJ12sf = fman->GetStructureFunctions();
   fman->CreatePolSFs(2);
   InSANEPolarizedStructureFunctions * AACpsf = fman->GetPolarizedStructureFunctions();
   InSANEVirtualComptonAsymmetries *Asym4 = new InSANEVirtualComptonAsymmetries(); 
   Asym4->SetUnpolarizedSFs(CJ12sf);  
   Asym4->SetPolarizedSFs(AACpsf);
   aModel = new TF1("Model5",Asym4,&InSANE_VCSABase::EvaluateA2p_W,
         model_min,model_max,1,"InSANE_VCSABase","EvaluateA2p_W");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kRed);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"CJ12+AAC","l");

   // CTEQ + LSS2006 
   fman->CreateSFs(1);
   InSANEStructureFunctions * CTEQsf = fman->GetStructureFunctions();
   fman->CreatePolSFs(1);
   InSANEPolarizedStructureFunctions * LSSpsf = fman->GetPolarizedStructureFunctions();
   InSANEVirtualComptonAsymmetries *Asym5 = new InSANEVirtualComptonAsymmetries(); 
   Asym5->SetUnpolarizedSFs(CTEQsf);  
   Asym5->SetPolarizedSFs(LSSpsf);
   aModel = new TF1("Model5",Asym5,&InSANE_VCSABase::EvaluateA2p_W,
         model_min,model_max,1,"InSANE_VCSABase","EvaluateA2p_W");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kBlue);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"CTEQ+LSS2006","l");

   // CTEQ + LSS2006 
   fman->CreatePolSFs(4);
   InSANEPolarizedStructureFunctions * DNSpsf = fman->GetPolarizedStructureFunctions();
   InSANEVirtualComptonAsymmetries *Asym6 = new InSANEVirtualComptonAsymmetries(); 
   Asym6->SetUnpolarizedSFs(CTEQsf);  
   Asym6->SetPolarizedSFs(DNSpsf);
   aModel = new TF1("Model6",Asym6,&InSANE_VCSABase::EvaluateA2p_W,
         model_min,model_max,1,"InSANE_VCSABase","EvaluateA2p_W");
   aModel->SetParameter(0,Q2);
   aModel->SetLineColor(kGreen);
   aModel->SetLineStyle(2);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"CTEQ+DNS2005","l");
   // DSSV + CTEQ 
   InSANEVirtualComptonAsymmetries *Asym1 = new InSANEVirtualComptonAsymmetries(); 
   Asym1->SetUnpolarizedSFs(CTEQsf);  
   Asym1->SetPolarizedSFs(DSSVpsf);
   aModel = new TF1("Model1",Asym1,&InSANE_VCSABase::EvaluateA2p_W,
         model_min,model_max, 1,"InSANE_VCSABase","EvaluateA2p_W");
   aModel->SetParameter(0,Q2);
   aModel->SetLineStyle(2);
   aModel->SetLineColor(kMagenta);
   list_of_models->Add(aModel);
   legmod->AddEntry(aModel,"CTEQ+DSSV","l");

   TMultiGraph * mg_models = new TMultiGraph();
   for(int i=0; i<list_of_models->GetEntries();i++){
      aModel = (TF1*)list_of_models->At(i);
      aModel->Draw("same");
      //TGraph * grmod = new TGraph(aModel->DrawCopy("goff")->GetHistogram());
      //mg_models->Add(grmod,"l");
      //MGA1->Add(grmod,"l");
      //grmod->Draw("l");
   }
   //MGA1->Add(mg_models);
   //mg_models->Draw("l");
   legmod->Draw();

   c->Update();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_4.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_4.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_4.svg",aNumber));

   //-------------------------------------------------------
   // Add SANE data

   //gr = (TGraphErrors*)(mg->GetListOfGraphs()->At(4));
   //gr->SetMarkerColor(2);
   //gr->SetLineColor(2);
   //gr->SetMarkerSize(1.4);
   //gr->SetMarkerStyle(20);
   gr = (TGraphErrors*)(mg->GetListOfGraphs()->At(0));
   MGA1->Add(mg2,"P");
   leg0->AddEntry(gr,"SANE","p");
   leg4->AddEntry(gr,"SANE","p");
   //gr = (TGraphErrors*)(mg->GetListOfGraphs()->At(1));
   //MGA1->Add(gr,"P");
   MGA1->Draw("P");
   //TColor * col = new TColor(6666,0.0,0.0,0.0,"",0.1);
   l.SetTextColor(1);
   l.SetTextSize(0.2);
   l.SetTextAngle(45);
   //l.DrawLatex(0.2,0.1,"Preliminary");

   leg0->Draw();
   legmod->Draw();

   c->Update();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_5.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_5.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_5.svg",aNumber));


   //-------------------------------------------------------
   // Resonance locations
   Double_t M_res = 1.232;
   //Double_t M_res = 1.535;
   Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(0);
   Double_t x_1 = InSANE::Kine::xBjorken_WQsq(M_res,Qsqbin->GetMinimum());
   //std::cout << "x_1 = " << x_1 << "\n";
   //Double_t x_2 = InSANE::Kine::xBjorken_WQsq(1.535,Qsqbin->GetAverage());
   //Double_t x_3 = InSANE::Kine::xBjorken_WQsq(1.680,Qsqbin->GetAverage());

   //TArrow ar1(x_1,0.2,x_1,0.1,0.02,"|>");
   //ar1.SetLineColor(4);
   //ar1.SetFillColor(4);
   ////ar2.SetAngle(40);
   //ar1.SetLineWidth(2);
   //ar1.Draw();
   //l.SetTextAlign(10);  //centered
   //l.SetTextSize(0.02);
   //l.SetTextFont(132);
   //l.SetTextAngle(45);
   //l.DrawLatex(x_1,0.2,Form("Q^{2}=%.1f (GeV/c)^{2}",Qsqbin->GetMinimum()));

   //Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(1);
   //Double_t x_2 = InSANE::Kine::xBjorken_WQsq(M_res,Qsqbin->GetMinimum());
   //TArrow ar2(x_2,0.2,x_2,0.1,0.02,"|>");
   //ar2.SetLineColor(4);
   //ar2.SetFillColor(4);
   ////ar2.SetAngle(40);
   //ar2.SetLineWidth(2);
   //ar2.Draw();
   //l.SetTextAlign(10);  //centered
   //l.SetTextSize(0.02);
   //l.SetTextFont(132);
   //l.SetTextAngle(45);
   //l.DrawLatex(x_2,0.2,Form("Q^{2}=%.1f (GeV/c)^{2}",Qsqbin->GetMinimum()));

   //Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(2);
   //Double_t x_3 = InSANE::Kine::xBjorken_WQsq(M_res,Qsqbin->GetMinimum());
   //TArrow ar3(x_3,0.2,x_3,0.1,0.02,"|>");
   //ar3.SetLineColor(4);
   //ar3.SetFillColor(4);
   ////ar2.SetAngle(40);
   //ar3.SetLineWidth(2);
   //ar3.Draw();
   //l.SetTextAlign(10);  //centered
   //l.SetTextSize(0.02);
   //l.SetTextFont(132);
   //l.SetTextAngle(45);
   //l.DrawLatex(x_3,0.2,Form("Q^{2}=%.1f (GeV/c)^{2}",Qsqbin->GetMinimum()));

   //Qsqbin = (NucDBBinnedVariable*)fSANEQ2Bins->At(3);
   //Double_t x_4 = InSANE::Kine::xBjorken_WQsq(M_res,Qsqbin->GetMinimum());
   //TArrow ar4(x_4,0.2,x_4,0.1,0.02,"|>");
   //ar4.SetLineColor(4);
   //ar4.SetFillColor(4);
   //ar4.SetLineWidth(2);
   //ar4.Draw();
   //l.SetTextAlign(10);  //centered
   //l.SetTextSize(0.02);
   //l.SetTextFont(132);
   //l.SetTextAngle(45);
   //l.DrawLatex(x_4,0.2,Form("Q^{2}=%.1f (GeV/c)^{2}",Qsqbin->GetMinimum()));

   //leg->Draw();
   leg0->Draw();
   legmod->Draw();

   c->Update();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_6.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_6.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_6.svg",aNumber));

   //------------------------------
   c = new TCanvas();
   TMultiGraph * mg_data_sane = new TMultiGraph();

   mg_data_sane->Add(mg2);
   mg_data_sane->Add(MGA1_data);
   mg_data_sane->Draw("a");

   mg_data_sane->SetTitle("A_{2}^{p}");
   mg_data_sane->GetXaxis()->SetTitle("W (GeV)");
   mg_data_sane->GetXaxis()->CenterTitle();
   mg_data_sane->GetYaxis()->SetTitle("");
   mg_data_sane->GetYaxis()->CenterTitle();
   mg_data_sane->GetXaxis()->SetLimits(1.0,3.4); 
   mg_data_sane->GetYaxis()->SetRangeUser(-0.7,0.8); 
   leg4->Draw();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_7.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_7.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_7.svg",aNumber));

   //------------------------------
   c = new TCanvas();

   mg_data_sane->Add(mg_clas);
   mg_data_sane->Add(mg2);
   mg_data_sane->Draw("a");

   mg_data_sane->SetTitle("A_{2}^{p}");
   mg_data_sane->GetXaxis()->SetTitle("W (GeV)");
   mg_data_sane->GetXaxis()->CenterTitle();
   mg_data_sane->GetYaxis()->SetTitle("");
   mg_data_sane->GetYaxis()->CenterTitle();
   mg_data_sane->GetXaxis()->SetLimits(1.0,3.4); 
   mg_data_sane->GetYaxis()->SetRangeUser(-0.7,0.8); 
   leg4->Draw();
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_8.pdf",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_8.png",aNumber));
   c->SaveAs(Form("results/sane_results/A2p_vs_W_%d_8.svg",aNumber));

   return 0;
}
