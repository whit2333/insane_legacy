/** Plots models and data for g2p at Qsq. */
Int_t plot_x2g2p(Double_t Qsq = 2.0,Double_t QsqBinWidth=1.0, Int_t aNumber = 2405){

   Int_t    nMergeW  = 1;
   Double_t dWMerge  = 0.07;
   Int_t    nMergex  = 1;
   Double_t dxMerge  = 0.005;

   TCanvas * c = new TCanvas("x2g2p","x2g2p",1200,800);
   // Create Legend
   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);

   Int_t    npar  = 1;
   Int_t    width = 3;
   Double_t xmin  = 0.01;
   Double_t xmax  = 1.0;
   TF1 * xg2p = new TF1("xg2p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2p");

   xg2p->SetParameter(0,Qsq);
   xg2p->SetLineColor(1);
   xg2p->SetLineWidth(width);
   xg2p->SetLineStyle(1);

   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   // Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   // Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g2pQsq1 = new NucDBMeasurement("g2pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   NucDBMeasurement * g2pSANE = new NucDBMeasurement("g2pSANE","g_{1}^{p} SANE");

   TF2 * xf = new TF2("xf","x*x*y",0,1,-1,1);
   TList * listOfMeas = manager->GetMeasurements("g2p");
   TMultiGraph * mg = new TMultiGraph();

   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g2pMeas = (NucDBMeasurement*)listOfMeas->At(i);

       if( !strcmp(g2pMeas->GetExperimentName(),"SANE"))  {
          g2pSANE->AddDataPoints(g2pMeas->GetDataPoints());
       } else {

       TList * points = g2pMeas->FilterWithBin(Qsqbin);
       g2pQsq1->AddDataPoints(points);

       TGraphErrors * graph = g2pMeas->BuildGraph("x");
       graph->Apply(xf);
       graph->SetMarkerStyle(20+i);
       graph->SetMarkerSize(1.6);
       graph->SetMarkerColor(kBlue-2-i);
       graph->SetLineColor(kBlue-2-i);
       graph->SetLineWidth(1);

       mg->Add(graph,"ep");

       leg->AddEntry(graph,Form("%s %s",g2pMeas->GetExperimentName(),g2pMeas->GetTitle() ),"lp");
       }

   }

   // Existing data that falls into Q2 bin
   TGraphErrors * gr = g2pQsq1->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(3);
   gr->SetLineColor(3);
   mg->Add(gr,"ep");
   leg->AddEntry(gr,Form("%s %s","All g2p data",g2pQsq1->GetTitle() ),"ep");


   std::vector<double> sane_Q2_values;
   g2pSANE->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   std::cout << sane_Q2_values.size() << std::endl;

   TList * g2p_Q2_filtered = g2pSANE->CreateMeasurementsWithUniqueBins("Qsquared");
   TList * sane_g2p_meas =  g2p_Q2_filtered;//new TList();

   //for(int i = 0; i<sane_Q2_values.size(); i++) {
   //   NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);
   //   NucDBMeasurement * g2pSANE_i = new NucDBMeasurement(Form("g2pSANE%d",i),"g_{1}^{p} SANE");
   //   g2pSANE_i->AddDataPoints(g2pSANE->FilterWithBin(avar));
   //   sane_g2p_meas->Add(g2pSANE_i);
   //   std::cout << i << std::endl;
   //}
   std::cout << sane_g2p_meas->GetEntries() << " Q2 bins" << std::endl;

   NucDBMeasurement * aMeas = 0;
   for( int i = 0; i < sane_g2p_meas->GetEntries();i++ ){
      // Q2 bin 0
      aMeas = (NucDBMeasurement*)sane_g2p_meas->At(i);
      aMeas->MergeNeighboringDataPoints(nMergex,"x",dxMerge,true);
      aMeas->MergeNeighboringDataPoints(nMergeW,"W",dWMerge,true);
      gr = aMeas->BuildGraph("x");
      gr->Apply(xf);
      gr->SetMarkerColor(kRed-2+i*2);
      gr->SetLineColor(  kRed-2+i*2);
      //gr->SetMarkerColor(gNiceColors[i]);
      //gr->SetLineColor(  gNiceColors[i]);
      gr->SetLineWidth(2);
      gr->SetMarkerStyle(gNiceMarkerStyle[i]);
      mg->Add(gr,"ep");
   }

   //g2pSANE->SortBy("Qsquared");
   //g2pSANE->SortBy("x");
   g2pSANE->Print();

   //TMultiGraph * mg2 = g2pSANE->BuildGraphUnique("x","Qsquared");
   //for(int i = 0; i<mg2->GetListOfGraphs()->GetEntries(); i++) {
   //   TGraphErrors * gr = (TGraphErrors*)mg2->GetListOfGraphs()->At(i);
   //   //gr->Apply(xf);
   //}
   //mg->Add(mg2);

   gPad->SetGridy(true);
   g2pSANE->PrintBreakDown("Qsquared");
   // --------------------------
   // 
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.10,0.06);
   mg->GetXaxis()->SetLimits(0.01,0.99);
   //g2pSANE->Print("data");

   //xg2pB->Draw("same");
   //xg2pC->Draw("same");
   //xg2pD->Draw("same");

   xg2p->SetParameter(0,3.0);
   xg2p->Draw("same");


   // Complete the Legend 
   //leg->AddEntry(xg2p,"xg2p BB ","l");
   //leg->AddEntry(xg2pA,"xg2p DNS2005 ","l");
   //leg->AddEntry(xg2pB,"xg2p AAC ","l");
   //leg->AddEntry(xg2pC,"xg2p GS ","l");
   //leg->AddEntry(xg2pD,"xg2p Stat ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   //t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.16,0.95,Form("x^{2}g_{2}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));

   c->SaveAs(Form("results/sane_results/%d/plot_x2g2p_%d_%d.png",aNumber,nMergex,nMergeW));
   c->SaveAs(Form("results/sane_results/%d/plot_x2g2p_%d_%d.pdf",aNumber,nMergex,nMergeW));

   return(0);
}
