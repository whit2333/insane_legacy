/** Plots models and data for g2p at Qsq. */
Int_t plot_xg2p(Double_t Qsq = 5.0,Double_t QsqBinWidth=3.0, Int_t aNumber = 2008){

   Int_t nMerge = 1;

   TCanvas * c = new TCanvas("xg2p","xg2p",1200,800);

   // Create Legend
   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg2p = new TF1("xg2p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatex2g2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatex2g2p");

   xg2p->SetParameter(0,Qsq);
   xg2p->SetLineColor(1);
   Int_t width = 3;
   xg2p->SetLineWidth(width);
   xg2p->SetLineStyle(1);

   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   // Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   // Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g2pQsq1 = new NucDBMeasurement("g2pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   NucDBMeasurement * g2pSANE = new NucDBMeasurement("g2pSANE","g_{1}^{p} SANE");

   TF2 * xf = new TF2("xf","x*x*y",0,1,-1,1);
   TList * listOfMeas = manager->GetMeasurements("g2p");
   TMultiGraph * mg = new TMultiGraph();

   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g2pMeas = (NucDBMeasurement*)listOfMeas->At(i);

       if( !strcmp(g2pMeas->GetExperimentName(),"SANE"))  {
          g2pSANE->AddDataPoints(g2pMeas->GetDataPoints());
       } else {

       TList * points = g2pMeas->FilterWithBin(Qsqbin);
       g2pQsq1->AddDataPoints(points);

       TGraphErrors * graph = g2pMeas->BuildGraph("x");
       graph->Apply(xf);
       graph->SetMarkerStyle(20+i);
       graph->SetMarkerSize(1.6);
       graph->SetMarkerColor(kBlue-2-i);
       graph->SetLineColor(kBlue-2-i);
       graph->SetLineWidth(1);

       mg->Add(graph,"ep");

       leg->AddEntry(graph,Form("%s %s",g2pMeas->GetExperimentName(),g2pMeas->GetTitle() ),"lp");
       }

   }

   TGraphErrors * gr = g2pQsq1->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(3);
   gr->SetLineColor(3);
   mg->Add(gr,"ep");
   leg->AddEntry(gr,Form("%s %s","All g2p data",g2pQsq1->GetTitle() ),"ep");

   std::vector<double> sane_Q2_values;
   g2pSANE->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   std::cout << sane_Q2_values.size() << std::endl;
   TList * sane_g2p_meas = new TList();
   for(int i = 0; i<sane_Q2_values.size(); i++) {
      NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);
      NucDBMeasurement * g2pSANE_i = new NucDBMeasurement(Form("g2pSANE%d",i),"g_{1}^{p} SANE");
      g2pSANE_i->AddDataPoints(g2pSANE->FilterWithBin(avar));
      sane_g2p_meas->Add(g2pSANE_i);
      std::cout << i << std::endl;
   }
   std::cout << sane_g2p_meas->GetEntries() << " Q2 bins" << std::endl;

   NucDBMeasurement * aMeas = 0;
   
   // Q2 bin 0
   aMeas = (NucDBMeasurement*)sane_g2p_meas->At(0);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(2);
   gr->SetLineColor(2);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[0]);
   mg->Add(gr,"ep");

   // Q2 bin 1
   aMeas = (NucDBMeasurement*)sane_g2p_meas->At(1);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(kRed+1);
   gr->SetLineColor(kRed+1);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[1]);
   mg->Add(gr,"ep");
      
   // Q2 bin 2
   aMeas = (NucDBMeasurement*)sane_g2p_meas->At(2);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(kRed+2);
   gr->SetLineColor(kRed+2);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[2]);
   mg->Add(gr,"ep");
      
   // Q2 bin 3
   aMeas = (NucDBMeasurement*)sane_g2p_meas->At(3);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(kRed+3);
   gr->SetLineColor(kRed+3);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[3]);
   mg->Add(gr,"ep");
      
   //g2pSANE->MergeDataPoints(4,"x",true);
   //gr = g2pSANE->BuildGraph("x");
   //gr->Apply(xf);
   //gr->SetMarkerColor(2);
   //gr->SetLineColor(2);
   //mg->Add(gr,"ep");

   //g2pSANE->SortBy("Qsquared");
   //g2pSANE->SortBy("x");
   g2pSANE->Print();

   //TMultiGraph * mg2 = g2pSANE->BuildGraphUnique("x","Qsquared");
   //for(int i = 0; i<mg2->GetListOfGraphs()->GetEntries(); i++) {
   //   TGraphErrors * gr = (TGraphErrors*)mg2->GetListOfGraphs()->At(i);
   //   //gr->Apply(xf);
   //}
   //mg->Add(mg2);

   gPad->SetGridy(true);
   g2pSANE->PrintBreakDown("Qsquared");
   // --------------------------
   // 
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.10,0.06);
   mg->GetXaxis()->SetLimits(0.01,0.99);
   //g2pSANE->Print("data");

   //xg2pB->Draw("same");
   //xg2pC->Draw("same");
   //xg2pD->Draw("same");

   xg2p->SetParameter(0,3.0);
   xg2p->Draw("same");


   // Complete the Legend 
   //leg->AddEntry(xg2p,"xg2p BB ","l");
   //leg->AddEntry(xg2pA,"xg2p DNS2005 ","l");
   //leg->AddEntry(xg2pB,"xg2p AAC ","l");
   //leg->AddEntry(xg2pC,"xg2p GS ","l");
   //leg->AddEntry(xg2pD,"xg2p Stat ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.16,0.95,Form("x^{2}g_{2}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));

   c->SaveAs(Form("results/sane_results/%d/plot_xg2p_%d.png",aNumber,nMerge));
   c->SaveAs(Form("results/sane_results/%d/plot_xg2p_%d.pdf",aNumber,nMerge));

   return(0);
}
