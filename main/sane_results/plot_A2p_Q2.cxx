/** Plots models and data for A2p at Qsq. */
Int_t plot_A2p_Q2(Double_t xBinValue = 0.5,Double_t xBinWidth=0.3, Int_t aNumber = 2408){

   TCanvas * c = new TCanvas("xA2p","xA2p",1200,800);

   Int_t nMerge = 1;

   // Create Legend
   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);


   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   // Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * xbin = 
      new NucDBBinnedVariable("x",Form("%4.2f <x<%4.2f",xBinValue-xBinWidth,xBinValue+xBinWidth),xBinValue,xBinWidth);

   Double_t WBinValue = 2.5;
   Double_t WBinWidth = 0.5;
   NucDBBinnedVariable * Wbin = 
      new NucDBBinnedVariable("x",Form("%4.2f <W<%4.2f",WBinValue-WBinWidth,WBinValue+WBinWidth),WBinValue,WBinWidth);

   // Create a new measurement which has all data points within the bin 
   NucDBMeasurement * A2pQsq1 = new NucDBMeasurement("A2pQsq1",Form("g_{1}^{p} %s",Wbin->GetTitle()));
   NucDBMeasurement * A2pSANE = new NucDBMeasurement("A2pSANE","g_{1}^{p} SANE");

   TF2 * xf = new TF2("xf","x*x*y",0,1,-1,1);
   TList * listOfMeas = manager->GetMeasurements("A2p");
   TMultiGraph * mg = new TMultiGraph();

   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * A2pMeas = (NucDBMeasurement*)listOfMeas->At(i);

       if( !strcmp(A2pMeas->GetExperimentName(),"SANE"))  {
          A2pSANE->AddDataPoints(A2pMeas->GetDataPoints());
          continue;
       } else {

       if( !strcmp(A2pMeas->GetExperimentName(),"CLAS"))  {
          continue;
       }
       if( !strcmp(A2pMeas->GetExperimentName(),"CLAS-E93009"))  {
          continue;
       }

       TList * points = A2pMeas->FilterWithBin(Wbin);
       A2pQsq1->AddDataPoints(points);

       TGraphErrors * graph = A2pMeas->BuildGraph("Qsquared");
       //graph->Apply(xf);
       graph->SetMarkerStyle(20+i);
       graph->SetMarkerSize(1.6);
       graph->SetMarkerColor(kBlue-2-i);
       graph->SetLineColor(kBlue-2-i);
       graph->SetLineWidth(1);

       mg->Add(graph,"ep");

       leg->AddEntry(graph,Form("%s %s",A2pMeas->GetExperimentName(),A2pMeas->GetTitle() ),"lp");
       }

   }

   TGraphErrors * gr = A2pQsq1->BuildGraph("Qsquared");
   //gr->Apply(xf);
   gr->SetMarkerColor(3);
   gr->SetLineColor(3);
   mg->Add(gr,"ep");
   leg->AddEntry(gr,Form("%s %s","All A2p data",A2pQsq1->GetTitle() ),"ep");

   std::vector<double> sane_Q2_values;
   A2pSANE->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   std::cout << sane_Q2_values.size() << std::endl;
   TList * sane_A2p_meas = new TList();
   for(int i = 0; i<sane_Q2_values.size(); i++) {
      NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);
      NucDBMeasurement * A2pSANE_i = new NucDBMeasurement(Form("A2pSANE%d",i),"g_{1}^{p} SANE");
      A2pSANE_i->AddDataPoints(A2pSANE->FilterWithBin(avar));
      sane_A2p_meas->Add(A2pSANE_i);
      std::cout << i << std::endl;
   }
   std::cout << sane_A2p_meas->GetEntries() << " Q2 bins" << std::endl;

   NucDBMeasurement * aMeas = 0;
   
   // Q2 bin 0
   aMeas = (NucDBMeasurement*)sane_A2p_meas->At(0);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("Qsquared");
   //gr->Apply(xf);
   gr->SetMarkerColor(2);
   gr->SetLineColor(2);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[0]);
   mg->Add(gr,"ep");

   // Q2 bin 1
   aMeas = (NucDBMeasurement*)sane_A2p_meas->At(1);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("Qsquared");
   //gr->Apply(xf);
   gr->SetMarkerColor(kRed+1);
   gr->SetLineColor(kRed+1);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[1]);
   mg->Add(gr,"ep");
      
   // Q2 bin 2
   aMeas = (NucDBMeasurement*)sane_A2p_meas->At(2);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("Qsquared");
   //gr->Apply(xf);
   gr->SetMarkerColor(kRed+2);
   gr->SetLineColor(kRed+2);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[0]);
   mg->Add(gr,"ep");
      
   // Q2 bin 3
   aMeas = (NucDBMeasurement*)sane_A2p_meas->At(3);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("Qsquared");
   //gr->Apply(xf);
   gr->SetMarkerColor(kRed+3);
   gr->SetLineColor(kRed+3);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[1]);
   mg->Add(gr,"ep");
      
   //A2pSANE->MergeDataPoints(4,"x",true);
   //gr = A2pSANE->BuildGraph("x");
   //gr->Apply(xf);
   //gr->SetMarkerColor(2);
   //gr->SetLineColor(2);
   //mg->Add(gr,"ep");

   //A2pSANE->SortBy("Qsquared");
   //A2pSANE->SortBy("x");
   A2pSANE->Print();

   //TMultiGraph * mg2 = A2pSANE->BuildGraphUnique("x","Qsquared");
   //for(int i = 0; i<mg2->GetListOfGraphs()->GetEntries(); i++) {
   //   TGraphErrors * gr = (TGraphErrors*)mg2->GetListOfGraphs()->At(i);
   //   //gr->Apply(xf);
   //}
   //mg->Add(mg2);

   gPad->SetGridy(true);
   A2pSANE->PrintBreakDown("Qsquared");
   // --------------------------
   // 
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-1.0,1.5);
   mg->GetXaxis()->SetLimits(1.0,8.5);
   //A2pSANE->Print("data");

   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.16,0.95,"A_{1}^{p}(x,Q^{2})");


   c->SaveAs(Form("results/sane_results/%d/plot_A2p_Q2_%d.png",aNumber,nMerge));
   c->SaveAs(Form("results/sane_results/%d/plot_A2p_Q2_%d.pdf",aNumber,nMerge));

   return 0;
}
