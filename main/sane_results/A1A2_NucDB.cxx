#include "asym/sane_data_bins.cxx"

using namespace nucdb;
Int_t A1A2_NucDB(Int_t aNumber=8609){

  Int_t FileVersion = aNumber;

   Double_t  E0              = 5.9;
   bool      clear_sane_data = true;
   Double_t  max_error       = 0.9;

   Manager        * manager = Manager::GetManager(1);//("SANE_results.root",1);
   Measurement    * A1_meas = 0;
   Measurement    * A2_meas = 0;
   BinnedVariable * Qsqbin  = 0;
   Experiment     * exp     = manager->GetExperiment("SANE");
   if(!exp) {
      exp = new Experiment("SANE","SANE");
   }
   
   // Get the measurements
   A1_meas = exp->GetMeasurement("A1p");
   A2_meas = exp->GetMeasurement("A2p");

   if(!A1_meas) {
      A1_meas = new Measurement("A1p","A_{1}^{p}");
      exp->AddMeasurement(A1_meas);
   }
   if(!A2_meas) {
      A2_meas = new Measurement("A2p","A_{2}^{p}");
      exp->AddMeasurement(A2_meas);
   }
   if( clear_sane_data ) {
      A1_meas->ClearDataPoints();
      A2_meas->ClearDataPoints();
   }

   //manager->SaveExperiment(exp);
   //return 0;


   //const TString weh("x2A1A2_59_noel()");
   //if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
   //   Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
   //   return -1;
   //}
   sane_data_bins();

   TFile * f1 = new TFile(Form("data/A1A2_59_%d.root",FileVersion),"UPDATE");
   f1->cd();
   f1->ls();

   TList * fA1Asymmetries = (TList*)gROOT->FindObject("A1_59_W");
   TList * fA2Asymmetries = (TList*)gROOT->FindObject("A2_59_W");
   if(!fA1Asymmetries) return(-1);
   if(!fA2Asymmetries) return(-2);

   TList * fA1s = new TList();
   TList * fA2s = new TList();
   TList * fd2s = new TList();
   TList * fWAxes = new TList();

   TH1F  * fA1 = 0;
   TH1F  * fA2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;
   TGraph       * gr1 = 0;


   // ----------------------------------------
   // Variables
   BinnedVariable    x_bin("x","x");
   BinnedVariable    Q2_bin("Qsquared","Q^{2}");
   BinnedVariable    Ebeam_bin("Ebeam","E_{beam}",E0,0.0001);
   BinnedVariable    W_bin("W","W");
   //BinnedVariable    Pt("Pt","p_{T}");
   //DiscreteVariable  A("A","A");
   //DiscreteVariable  Z("Z","Z");

   // ----------------------------------------
   // This is the point we manipulate locally
   // When adding datapoint to the measurment be sure to use the 
   // point's copy constructor.
   DataPoint  point;
   point.AddBinVariable(&Ebeam_bin);
   point.AddBinVariable(&Q2_bin);
   point.AddBinVariable(&x_bin);
   point.AddBinVariable(&W_bin);

   ErrorBar err1(0.005);
   ErrorBar err2(0.005);


   // ----------------------------------------
   // Loop over Q2 bins
   for(int jj = 0;jj<fA1Asymmetries->GetEntries()&&jj<4;jj++) {

      InSANEMeasuredAsymmetry * A1Asym = (InSANEMeasuredAsymmetry*)fA1Asymmetries->At(jj);
      InSANEMeasuredAsymmetry * A2Asym = (InSANEMeasuredAsymmetry*)fA2Asymmetries->At(jj);

      fA1 = &A1Asym->fAsymmetryVsW;
      fA2 = &A2Asym->fAsymmetryVsW;

      Double_t Q2_bin_avg = (A1Asym->fQ2Min + A1Asym->fQ2Max)/2.0;

      // Are the the same?
      InSANEAveragedKinematics1D * measKine1 = &A1Asym->fAvgKineVsW;
      InSANEAveragedKinematics1D * measKine2 = &A2Asym->fAvgKineVsW; 

      //measKine1->Print();

      Int_t   xBinmax = fA1->GetNbinsX();
      Int_t   yBinmax = fA1->GetNbinsY();
      Int_t   zBinmax = fA1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fA1->GetBin(i,j,k);
               fA1->GetBinXYZ(bin,binx,biny,binz);

               Double_t W1     = measKine1->fW.GetBinContent(bin);
               Double_t x1     = measKine1->fx.GetBinContent(bin);
               Double_t phi1   = measKine1->fPhi.GetBinContent(bin);
               Double_t Q21    = measKine1->fQ2.GetBinContent(bin);
               Double_t W     = W1;//
               Double_t x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t phi   = phi1;//0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
               Double_t Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               //Double_t x_width = measKine1->fx->GetBinWidth(bin);
               //Double_t x_low   = measKine1->fx->GetBinLowEdge(bin);
               //Double_t x_high  = x_low+x_width;

               Double_t W_width = measKine1->fW.GetBinWidth(bin);
               Double_t W_low   = measKine1->fW.GetBinLowEdge(bin);
               Double_t W_high  = W_low+W_width;

               Double_t x_low   = InSANE::Kine::xBjorken_WQ2(W_high,Q2);
               Double_t x_high  = InSANE::Kine::xBjorken_WQ2(W_low,Q2);

               x_bin.SetMeanLimits(x1,x_low,x_high);
               Q2_bin.SetMeanLimits(Q21,A1Asym->fQ2Min,A1Asym->fQ2Max);
               W_bin.SetMeanLimits(W,W_low,W_high);


               Double_t A1   = fA1->GetBinContent(bin);
               Double_t A2   = fA2->GetBinContent(bin);

               Double_t eA1  = fA1->GetBinError(bin);
               Double_t eA2  = fA2->GetBinError(bin);

               Double_t esysA1  = A1Asym->fSystErrVsW.GetBinContent(bin);
               Double_t esysA2  = A2Asym->fSystErrVsW.GetBinContent(bin);

               if( eA1 > max_error || eA2 > max_error ) continue;
               if( A1 == 0.0 || A2 == 0.0 ) continue;
               if( TMath::IsNaN(A1) || TMath::IsNaN(A2) ) continue;

               // Add point to A1 measurement
               point.SetValue(A1);
               err1 = ErrorBar(eA1);
               err2 = ErrorBar(esysA1);
               point.SetStatError(err1); 
               point.SetSystError(err2); 
               point.CalculateTotalError();
               A1_meas->AddDataPoint(new DataPoint(point));

               point.Print();

               // Add point to A2 measurement
               point.SetValue(A2);
               err1 = ErrorBar(eA2);
               err2 = ErrorBar(esysA2);
               point.SetStatError(err1); 
               point.SetSystError(err2); 
               point.CalculateTotalError();
               A2_meas->AddDataPoint(new DataPoint(point));

               //Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               //Double_t R        = SFs->R(x,Q2);
               //Double_t F1       = SFs->F1p(x,Q2);
               //Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               //Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               //Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               //Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               //Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               //Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               //Double_t gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
               //Double_t gamma    = TMath::Sqrt(gamma2);
               //Double_t cota     = 1.0/TMath::Tan(alpha);
               //Double_t csca     = 1.0/TMath::Sin(alpha);

               //Double_t A1 = 2.0*x*x*(F1/(1.0+gamma2))*(A1 + gamma*A2);
               //Double_t A2 = 3.0*x*x*(F1/(1.0+gamma2))*(A2/gamma - A1);

               //Double_t d2p = (A1+A2);

               ////eA1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
               //Double_t a1 = 2.0*x*x*(F1/(1.0+gamma2));
               //Double_t a2 = 2.0*x*x*(F1/(1.0+gamma2))*gamma;
               //eA1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               ////eA2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
               //a1 = -3.0*x*x*(F1/(1.0+gamma2));
               //a2 = 3.0*x*x*(F1/(1.0+gamma2))/gamma;
               //eA2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //Double_t ed2p = 1.0*TMath::Sqrt( eA1*eA1 + eA2*eA2); 
               ////c0 = 1.0/(1.0 + eta*xi);
               ////c11 = (1.0 + cota*chi)/D ;
               ////c12 = (csca*chi)/D ;
               ////c21 = (xi - cota*chi/eta)/D ;
               ////c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               ////if( TMath::Abs(A80) > 0.0 ) {
               ////   std::cout << "(E0,Ep,theta,phi) = (" << E0 << ","
               ////                                        << Ep << ","
               ////                                        << theta << ","
               ////                                        << phi   << ")" << std::endl;  
               ////   std::cout << " delta R      = " << R - R_2 << std::endl;
               ////   std::cout << " A180         = " << A180 << std::endl;
               ////   std::cout << " c0*c11 - 1/D = " << c0*c11 - 1.0/D << std::endl;
               ////   std::cout << " D            = " << D << std::endl;
               ////   std::cout << " 1/c11        = " << 1.0/c11 << std::endl;
               ////   std::cout << " c0           = " << c0 << std::endl;
               ////   std::cout << " c11          = " << c11 << std::endl;
               ////   std::cout << " 1/D          = " << 1.0/D << std::endl;
               ////   std::cout << " c12          = " << c12 << std::endl;
               ////   std::cout << " -eta/d       = " << -eta/d << std::endl;
               ////   std::cout << " c21          = " << c21 << std::endl;
               ////   std::cout << " xi/D         = " <<  xi/D<< std::endl;
               ////   std::cout << " c22          = " << c22 << std::endl;
               ////   std::cout << " 1.0/d        = " << 1.0/d << std::endl;
               ////   std::cout << " chi          = " << chi << std::endl;
               ////   
               ////}
               ////             
               //fA1->SetBinContent(bin,A1);
               //fA2->SetBinContent(bin,A2);
               //fA1->SetBinError(bin, eA1);
               //fA2->SetBinError(bin, eA2);

               //fd2->SetBinContent(bin,d2p);
               //fd2->SetBinError(bin, ed2p);
            }
         }
      }

      //A1_meas->Print("data");
   }


   // --------------------------------------------------------------------------
   //
   A1_meas->PrintTable(std::cout);
   A2_meas->PrintTable(std::cout);


   // --------------------------------------------------------------------------
   // Functions

   exp->Print();
   manager->SaveExperiment(exp);

   return(0);
}
