Int_t clear_NucDB(Int_t paraFileVersion = 8411,Int_t perpFileVersion = 8411,Int_t aNumber=8411){

   gSystem->mkdir(Form("results/sane_results/%d",aNumber));

   bool      clear_sane_data = true;

   NucDBManager        * manager = NucDBManager::GetManager(1);
   NucDBMeasurement    * g1_meas = 0;
   NucDBMeasurement    * g2_meas = 0;
   NucDBMeasurement    * A1_meas = 0;
   NucDBMeasurement    * A2_meas = 0;
   NucDBBinnedVariable * Qsqbin  = 0;
   NucDBExperiment     * exp     = manager->GetExperiment("SANE");
   if(!exp) {
      exp = new NucDBExperiment("SANE","SANE");
   }
   
   // Get the measurements
   g1_meas = exp->GetMeasurement("g1p");
   g2_meas = exp->GetMeasurement("g2p");

   if(!g1_meas) {
      g1_meas = new NucDBMeasurement("g1p","g_{1}^{p}");
      exp->AddMeasurement(g1_meas);
   }
   if(!g2_meas) {
      g2_meas = new NucDBMeasurement("g2p","g_{2}^{p}");
      exp->AddMeasurement(g2_meas);
   }
   if( clear_sane_data ) {
      g1_meas->ClearDataPoints();
      g2_meas->ClearDataPoints();
   }

   A1_meas = exp->GetMeasurement("A1p");
   A2_meas = exp->GetMeasurement("A2p");
   if(!A1_meas) {
      A1_meas = new NucDBMeasurement("A1p","A_{1}^{p}");
      exp->AddMeasurement(A1_meas);
   }
   if(!A2_meas) {
      A2_meas = new NucDBMeasurement("A2p","A_{2}^{p}");
      exp->AddMeasurement(A2_meas);
   }
   if( clear_sane_data ) {
      A1_meas->ClearDataPoints();
      A2_meas->ClearDataPoints();
   }

   exp->GetMeasurements()->Clear();
   manager->SaveExperiment(exp);
   return 0;
}
