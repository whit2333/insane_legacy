Int_t merge_bins_g1p( Int_t   aNumber = 1 )
{

   NucDBManager     * dbman        = NucDBManager::GetManager(1);

   // -------------------------------------------------------------------------------
   NucDBBinnedVariable * Q2bin_sane_0 = new NucDBBinnedVariable("Qsquared","Q2");
   NucDBBinnedVariable * Q2bin_sane_1 = new NucDBBinnedVariable("Qsquared","Q2");
   NucDBBinnedVariable * Wbin_sane_0 = new NucDBBinnedVariable("W","W");
   NucDBBinnedVariable * Wbin_sane_1 = new NucDBBinnedVariable("W","W");
   NucDBBinnedVariable * Wbin_sane_2 = new NucDBBinnedVariable("W","W");
   NucDBBinnedVariable * Wbin_sane_3 = new NucDBBinnedVariable("W","W");

   Q2bin_sane_0->SetLimits(5.0,8.0);
   Q2bin_sane_1->SetLimits(3.7,4.2);
   Wbin_sane_0->SetLimits(1.2,1.77);
   Wbin_sane_1->SetLimits(1.84,2.0);

   NucDBExperiment * exp = dbman->GetExperiment("SANE");
   exp->Print();
   NucDBMeasurement * saneMeas = exp->GetMeasurement("g1p");
   NucDBMeasurement * g1p_merged = exp->GetMeasurement("Mergedg1p");
   if(!g1p_merged) {
      g1p_merged = new NucDBMeasurement("Mergedg1p","g_{1}^{p}");
   }
   g1p_merged->ClearDataPoints();

   TGraph           * saneKine  = 0;
   TGraph           * saneKine2  = 0;
   TMultiGraph      * mg_0 = new TMultiGraph();
   TMultiGraph      * mg_1 = new TMultiGraph();
   TMultiGraph      * mg_2 = new TMultiGraph();
   TMultiGraph      * mg_3 = new TMultiGraph();
   TMultiGraph      * mg_4 = new TMultiGraph();
   TMultiGraph      * mg_5 = new TMultiGraph();
   TMultiGraph      * mg_6 = new TMultiGraph();
   TMultiGraph      * mg_7 = new TMultiGraph();
   TMultiGraph      * mg_8 = new TMultiGraph();
   TMultiGraph      * mg_9 = new TMultiGraph();
   if(saneMeas) {

      TGraphErrors * gr3 = saneMeas->BuildGraph("W");
      gr3->SetLineColor(1);
      gr3->SetMarkerColor(1);
      gr3->SetMarkerStyle(20);
      mg_4->Add(gr3,"ep");

      gr3 = saneMeas->BuildGraph("x");
      gr3->SetLineColor(1);
      gr3->SetMarkerColor(1);
      gr3->SetMarkerStyle(20);
      mg_7->Add(gr3,"ep");

      saneKine = saneMeas->BuildKinematicGraph("W","Qsquared");
      saneKine->SetLineColor(1);
      saneKine->SetMarkerColor(1);
      saneKine->SetMarkerStyle(20);
      mg_5->Add(saneKine,"p");

      saneKine = saneMeas->BuildKinematicGraph("x","Qsquared");
      saneKine->SetLineColor(1);
      saneKine->SetMarkerColor(1);
      saneKine->SetMarkerStyle(20);
      mg_8->Add(saneKine,"p");

      // ------------------------------------------------------------
      // Merge sane bins

      g1p_merged->AddDataPoints(saneMeas->GetDataPoints());

      //g1p_merged->SortBy("W");

      std::vector<std::string> sort_vars;
      sort_vars.push_back("W");
      //sort_vars.push_back("Qsquared");
      sort_vars.push_back("Ebeam");
      g1p_merged->SortBy(sort_vars);

      std::vector<int> N_merge;
      N_merge.push_back(5);
      N_merge.push_back(4);
      N_merge.push_back(4);
      N_merge.push_back(4);
      N_merge.push_back(4);
      N_merge.push_back(4);
      N_merge.push_back(4);
      N_merge.push_back(4);
      N_merge.push_back(4);
      N_merge.push_back(4);
      N_merge.push_back(3);
      N_merge.push_back(3);
      N_merge.push_back(2);
      N_merge.push_back(2);

      N_merge.push_back(4);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);
      N_merge.push_back(2);

      g1p_merged->MergeDataPoints(N_merge,true);

      sort_vars.clear();
      //sort_vars.push_back("x");
      //sort_vars.push_back("Qsquared");
      //g1p_merged->MergeNeighboringDataPoints(2,"W",0.05,"Qsquared",0.6,true);

      //sort_vars.push_back("x");
      //saneMeas->SortBy(sort_vars);


      // Save 
      NucDBExperiment  * exp = dbman->GetExperiment("SANE");

      //exp->Print();
      g1p_merged->SetName("Mergedg1p");
      exp->AddMeasurement(g1p_merged);
      dbman->SaveExperiment(exp);

      // ------------------------------------------------------------
      //
      TGraphErrors * gr4 = g1p_merged->BuildGraph("W");
      gr4->SetLineColor(2);
      gr4->SetMarkerColor(2);
      gr4->SetMarkerStyle(23);
      mg_4->Add(gr4,"ep");

      gr4 = g1p_merged->BuildGraph("x");
      gr4->SetLineColor(2);
      gr4->SetMarkerColor(2);
      gr4->SetMarkerStyle(23);
      mg_7->Add(gr4,"ep");

      // Clean looking merged plot
      gr4 = g1p_merged->BuildGraph("W");
      gr4->SetLineColor(1);
      gr4->SetMarkerColor(1);
      gr4->SetMarkerStyle(20);
      mg_6->Add(gr4,"ep");

      gr4 = g1p_merged->BuildGraph("x");
      gr4->SetLineColor(2);
      gr4->SetMarkerColor(2);
      gr4->SetMarkerStyle(20);
      mg_9->Add(gr4,"ep");

      saneKine2 = g1p_merged->BuildKinematicGraph("W","Qsquared");
      saneKine2->SetLineColor(2);
      saneKine2->SetMarkerColor(2);
      saneKine2->SetMarkerStyle(23);
      mg_5->Add(saneKine2,"p");

      saneKine2 = g1p_merged->BuildKinematicGraph("x","Qsquared");
      saneKine2->SetLineColor(4);
      saneKine2->SetMarkerColor(4);
      saneKine2->SetMarkerStyle(23);
      mg_8->Add(saneKine2,"p");

   }

   //saneMeas->SortBy("W");
   //saneMeas->SortBy("x","Qsquared");
   //g1p_merged->Print("data");

   // ---------------------------
   std::cout << "derp" << std::endl;


   // ---------------------------
   TCanvas * c = new TCanvas();
   c->Divide(1,2);

   c->cd(1);
   mg_5->Draw("a");
   mg_5->GetXaxis()->SetLimits(1.0,3.2);

   c->cd(2);
   mg_4->Draw("a");
   mg_4->GetXaxis()->SetLimits(1.0,3.2);

   c->SaveAs(Form("results/sane_results/merge_bins_g1p_0_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/merge_bins_g1p_0_%d.pdf",aNumber));


   // ---------------------------
   c = new TCanvas();
   c->Divide(1,2);

   c->cd(1);
   mg_8->Draw("a");
   mg_8->GetXaxis()->SetLimits(0.0,1.0);

   c->cd(2);
   //mg_7->Add(mg_7);
   mg_7->Draw("a");
   mg_7->GetXaxis()->SetLimits(0.0,1.0);
   mg_7->GetYaxis()->SetRangeUser(0.0,1.0);

   c->SaveAs(Form("results/sane_results/merge_bins_g1p_1_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/merge_bins_g1p_1_%d.pdf",aNumber));


   // ---------------------------
   c = new TCanvas();
   c->Divide(1,2);

   c->cd(1);
   mg_5->Draw("a");
   mg_5->GetXaxis()->SetLimits(1.0,3.5);

   c->cd(2);
   mg_3->Add(mg_6);
   mg_3->Draw("a");
   mg_3->GetXaxis()->SetLimits(1.0,3.5);
   mg_3->GetYaxis()->SetRangeUser(0.0,1.5);
   mg_3->GetXaxis()->SetTitle("W");
   mg_3->GetXaxis()->CenterTitle(true);

   c->SaveAs(Form("results/sane_results/merge_bins_g1p_2_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/merge_bins_g1p_2_%d.pdf",aNumber));

   return 0;

   // ----------------------------
   c = new TCanvas();
   c->Divide(1,2);

   c->cd(1);
   mg_8->Draw("a");
   mg_8->GetXaxis()->SetLimits(0.0,1.0);


   c->cd(2);
   mg_33->Add(mg_9);
   mg_33->Draw("a");
   mg_33->GetXaxis()->SetLimits(0.0,1.0);
   mg_33->GetYaxis()->SetRangeUser(0.0,1.0);


   c->SaveAs(Form("results/sane_results/merge_bins_g1p_3_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/merge_bins_g1p_3_%d.pdf",aNumber));


   // ----------------------------
   c = new TCanvas();
   gPad->SetLogx(false); 
   mg_33->Draw("a");
   mg_33->GetXaxis()->SetLimits(0.0,1.0);
   mg_33->GetYaxis()->SetRangeUser(0.0,1.0);

   c->SaveAs(Form("results/sane_results/merge_bins_g1p_4_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/merge_bins_g1p_4_%d.pdf",aNumber));

   return 0;
}

