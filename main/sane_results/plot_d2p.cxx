/** Plots models and data for g2p at Qsq. */
Int_t plot_d2p(Double_t Qsq = 15.0,Double_t QsqBinWidth=7.0){

   TCanvas * c = new TCanvas("xg2p","xg2p");

   // Create Legend
   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   InSANEPolarizedStructureFunctionsFromPDFs *pSFsD  = new InSANEPolarizedStructureFunctionsFromPDFs(); 
   pSFsD->SetPolarizedPDFs( new StatisticalPolarizedPDFs); 

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg2p = new TF1("xg2p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   TF1 * xg2pA = new TF1("xg2pA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   TF1 * xg2pB = new TF1("xg2pB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   TF1 * xg2pC = new TF1("xg2pC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   TF1 * xg2pD = new TF1("xg2pC", pSFsD, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");

   xg2p->SetParameter(0,Qsq);
   xg2pA->SetParameter(0,Qsq);
   xg2pB->SetParameter(0,Qsq);
   xg2pC->SetParameter(0,Qsq);
   xg2pD->SetParameter(0,Qsq);

   xg2p->SetLineColor(1);
   xg2pA->SetLineColor(3);
   xg2pB->SetLineColor(kOrange-1);
   xg2pC->SetLineColor(kCyan+1);
   xg2pD->SetLineColor(kMagenta);

   Int_t width = 3;
   xg2p->SetLineWidth(width);
   xg2pA->SetLineWidth(width);
   xg2pB->SetLineWidth(width);
   xg2pC->SetLineWidth(width);
   xg2pD->SetLineWidth(width);

   xg2p->SetLineStyle(1);
   xg2pA->SetLineStyle(1);
   xg2pB->SetLineStyle(1);
   xg2pC->SetLineStyle(1);
   xg2pD->SetLineStyle(1);

   xg2pA->SetMaximum(0.1);
   xg2pA->SetMinimum(-0.03);

   xg2pA->Draw();

   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   // Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   // Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g2pQsq1 = new NucDBMeasurement("g2pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));

   TF2 * xf = new TF2("xf","x*x*y",0,1,-1,1);
   //TList * listOfMeas = manager->GetMeasurements("g2p");
   TMultiGraph * mg = new TMultiGraph();

   NucDBExperiment * exp = manager->GetExperiment("SANE");

   NucDBMeasurement * g1pSANE = exp->GetMeasurement("g1p");
   NucDBMeasurement * g2pSANE = exp->GetMeasurement("g2p");

   g1pSANE->PrintBreakDown("Qsquared");
   g2pSANE->PrintBreakDown("Qsquared");

   std::vector<double> sane_Q2_values;
   g2pSANE->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   std::cout << sane_Q2_values.size() << std::endl;

   TList * sane_g1p_meas = new TList();
   TList * sane_g2p_meas = new TList();

   for(int i = 0; i<sane_Q2_values.size(); i++) {
      NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);
      NucDBMeasurement * g1pSANE_i = new NucDBMeasurement(Form("g1pSANE%d",i),"g_{1}^{p} SANE");
      NucDBMeasurement * g2pSANE_i = new NucDBMeasurement(Form("g2pSANE%d",i),"g_{2}^{p} SANE");
      g1pSANE_i->AddDataPoints(g1pSANE->FilterWithBin(avar));
      g2pSANE_i->AddDataPoints(g2pSANE->FilterWithBin(avar));
      sane_g1p_meas->Add(g1pSANE_i);
      sane_g2p_meas->Add(g2pSANE_i);
      std::cout << i << std::endl;
   }
   std::cout << sane_g2p_meas->GetEntries() << " Q2 bins" << std::endl;

   Int_t nMerge = 1;

   // Create d2p measurement
   TMultiGraph * mg = new TMultiGraph();
   TList * sane_d2p_meas = new TList();

   for(int i = 0; i<sane_Q2_values.size(); i++) {

      NucDBMeasurement * d2pSANE_i = new NucDBMeasurement(Form("d2pSANE%d",i),"d_{2}^{p} SANE");
      sane_d2p_meas->Add(d2pSANE_i);
      NucDBMeasurement * g1pSANE_i = (NucDBMeasurement*)sane_g1p_meas->At(i);
      NucDBMeasurement * g2pSANE_i = (NucDBMeasurement*)sane_g2p_meas->At(i);

      for(int j = 0; j<g1pSANE_i->GetNDataPoints(); j++) {
         NucDBDataPoint * g1p_point = (NucDBDataPoint*)g1pSANE_i->GetDataPoints()->At(j);
         NucDBDataPoint * g2p_point = (NucDBDataPoint*)g2pSANE_i->GetDataPoints()->At(j);
         double x   = g1p_point->GetBinVariable("x")->GetMean();
         double Q2  = g1p_point->GetBinVariable("Qsquared")->GetMean();
         double M   = M_p/GeV;
         double g1  = g1p_point->GetValue();
         double eg1 = g1p_point->GetError();
         double g2  = g2p_point->GetValue();
         double eg2 = g2p_point->GetError();
         double d2  = x*x*(2.0*g1 + 3.0*g2);
         double ed2  = x*x*(2.0*eg1+3.0*eg2);

         double xi    = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));
         double g1_coeff_nach = (xi*xi/x)*(xi*xi/x)*(x/xi);
         double g2_coeff_nach = (xi*xi/x)*(xi*xi/x)*((3.0*x*x)/(2.0*xi*xi) - (3.0*M*M*x*x)/(4.0*Q2));
         double d2p_nach_int      = (g1_coeff_nach*g1 + g2_coeff_nach*g2);
         double err_d2p_nach_int  = (g1_coeff_nach*eg1 + g2_coeff_nach*eg2);

         NucDBDataPoint d2p_point((*g2p_point));
         d2p_point.SetValue(d2);
         d2p_point.SetStatError(ed2);
         d2pSANE_i->AddDataPoint(new NucDBDataPoint(d2p_point));
      }
      d2pSANE_i->MergeDataPoints(nMerge,"x",true);
      TGraphErrors * gr = d2pSANE_i->BuildGraph("x");
      gROOT->GetColor(40+i)->SetAlpha(0.5);
      gr->SetFillColor(40+i);
      gr->SetMarkerColor(kRed+i);
      gr->SetLineColor(kRed+i);
      gr->SetLineWidth(1);
      gr->SetMarkerStyle(gNiceMarkerStyle[i]);
      mg->Add(gr,"e3pc");
      mg->Add(gr,"ep");
   }

   gPad->SetGridy(true);

   // --------------------------
   // 
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.25,0.25);
   mg->GetXaxis()->SetLimits(0.01,0.99);
   //g2pSANE->Print("data");

   //xg2pB->Draw("same");
   //xg2pC->Draw("same");
   //xg2pD->Draw("same");
   //xg2p->Draw("same");


   // Complete the Legend 
   //leg->AddEntry(xg2p,"xg2p BB ","l");
   //leg->AddEntry(xg2pA,"xg2p DNS2005 ","l");
   //leg->AddEntry(xg2pB,"xg2p AAC ","l");
   //leg->AddEntry(xg2pC,"xg2p GS ","l");
   //leg->AddEntry(xg2pD,"xg2p Stat ","l");
   //leg->Draw();

   //TLatex * t = new TLatex();
   //t->SetNDC();
   //t->SetTextFont(62);
   //t->SetTextColor(kBlack);
   //t->SetTextSize(0.04);
   //t->SetTextAlign(12); 
   //t->DrawLatex(0.16,0.95,Form("xg_{2}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));


   c->SaveAs(Form("results/sane_results/plot_d2p_%d.png",nMerge));
   c->SaveAs(Form("results/sane_results/plot_d2p_%d.pdf",nMerge));

   return(0);
}
