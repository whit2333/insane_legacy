#include "asym/sane_data_bins.cxx"

using namespace nucdb;
Int_t g1g2_NucDB(Int_t paraFileVersion = 8609,Int_t perpFileVersion = 8609,Int_t aNumber=8609){

   gSystem->mkdir(Form("results/sane_results/%d",aNumber));

   Double_t  E0              = 5.9;
   bool      clear_sane_data = true;
   Double_t  max_error       = 0.5;

   Manager        * manager = new Manager(1);//"SANE_results.root",1);
   Measurement    * g1_meas = 0;
   Measurement    * g2_meas = 0;
   BinnedVariable * Qsqbin  = 0;
   Experiment     * exp     = manager->GetExperiment("SANE");
   if(!exp) {
      exp = new Experiment("SANE","SANE");
   }
   
   // Get the measurements
   g1_meas = exp->GetMeasurement("g1p");
   g2_meas = exp->GetMeasurement("g2p");

   if(!g1_meas) {
      g1_meas = new Measurement("g1p","g_{1}^{p}");
      exp->AddMeasurement(g1_meas);
   }
   if(!g2_meas) {
      g2_meas = new Measurement("g2p","g_{2}^{p}");
      exp->AddMeasurement(g2_meas);
   }
   if( clear_sane_data ) {
      g1_meas->ClearDataPoints();
      g2_meas->ClearDataPoints();
   }

   //manager->SaveExperiment(exp);
   //return 0;


   //const TString weh("x2g1g2_59_noel()");
   //if( gROOT->LoadMacro("asym/sane_data_bins.cxx") )  {
   //   Error(weh, "Failed loading asym/sane_data_bins.cxx in compiled mode.");
   //   return -1;
   //}
   sane_data_bins();

   TFile * f1 = new TFile(Form("data/g1g2_59_W_%d_%d.root",paraFileVersion,perpFileVersion),"UPDATE");
   f1->cd();

   TList * fg1Asymmetries = (TList*)gROOT->FindObject(Form("g1asym-59-W-%d",0));
   TList * fg2Asymmetries = (TList*)gROOT->FindObject(Form("g2asym-59-W-%d",0));
   if(!fg1Asymmetries) return(-1);
   if(!fg2Asymmetries) return(-2);

   TList * fg1s = new TList();
   TList * fg2s = new TList();
   TList * fd2s = new TList();
   TList * fWAxes = new TList();

   TH1F  * fg1 = 0;
   TH1F  * fg2 = 0;
   TGraphErrors * gr = 0;
   TGraphErrors * gr2 = 0;
   TGraph       * gr1 = 0;


   // ----------------------------------------
   // Variables
   BinnedVariable    x_bin("x","x");
   BinnedVariable    Q2_bin("Qsquared","Q^{2}");
   BinnedVariable    Ebeam_bin("Ebeam","E_{beam}",E0,0.0001);
   BinnedVariable    W_bin("W","W");
   //BinnedVariable    Pt("Pt","p_{T}");
   //DiscreteVariable  A("A","A");
   //DiscreteVariable  Z("Z","Z");

   // ----------------------------------------
   // This is the point we manipulate locally
   // When adding datapoint to the measurment be sure to use the 
   // point's copy constructor.
   DataPoint  point;
   point.AddBinVariable(&Ebeam_bin);
   point.AddBinVariable(&Q2_bin);
   point.AddBinVariable(&x_bin);
   point.AddBinVariable(&W_bin);

   ErrorBar err1(0.005);
   ErrorBar err2(0.005);


   // ----------------------------------------
   // Loop over Q2 bins
   for(int jj = 0;jj<fg1Asymmetries->GetEntries()&&jj<4;jj++) {

      InSANEMeasuredAsymmetry * g1Asym = (InSANEMeasuredAsymmetry*)fg1Asymmetries->At(jj);
      InSANEMeasuredAsymmetry * g2Asym = (InSANEMeasuredAsymmetry*)fg2Asymmetries->At(jj);

      fg1 = &g1Asym->fAsymmetryVsW;
      fg2 = &g2Asym->fAsymmetryVsW;

      Double_t Q2_bin_avg = (g1Asym->fQ2Min + g1Asym->fQ2Max)/2.0;

      // Are the the same?
      InSANEAveragedKinematics1D * measKine1 = &g1Asym->fAvgKineVsW;
      InSANEAveragedKinematics1D * measKine2 = &g2Asym->fAvgKineVsW; 

      //measKine1->Print();

      Int_t   xBinmax = fg1->GetNbinsX();
      Int_t   yBinmax = fg1->GetNbinsY();
      Int_t   zBinmax = fg1->GetNbinsZ();
      Int_t   bin = 0;
      Int_t   binx,biny,binz;
      Double_t bot, error, a, b, da, db;

      // now loop over bins to calculate the correct errors
      // the reason this error calculation looks complex is because of c2
      for(Int_t i=1; i<= xBinmax; i++){
         for(Int_t j=1; j<= yBinmax; j++){
            for(Int_t k=1; k<= zBinmax; k++){
               bin   = fg1->GetBin(i,j,k);
               fg1->GetBinXYZ(bin,binx,biny,binz);

               Double_t W1     = measKine1->fW.GetBinContent(bin);
               Double_t x1     = measKine1->fx.GetBinContent(bin);
               Double_t phi1   = measKine1->fPhi.GetBinContent(bin);
               Double_t Q21    = measKine1->fQ2.GetBinContent(bin);
               Double_t W     = W1;//
               Double_t x     = x1;//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t phi   = phi1;//0.0;//TMath::Pi();//fA1->GetZaxis()->GetBinCenter(binz);
               Double_t Q2    = Q21;//paraAsym->GetQ2();//InSANE::Kine::Q2_xW(x,W);//fA1->GetXaxis()->GetBinCenter(binx);
               Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
               Double_t Ep    = InSANE::Kine::Eprime_xQ2y(x,Q2,y);
               Double_t theta = InSANE::Kine::Theta_xQ2y(x,Q2,y);

               //Double_t x_width = measKine1->fx->GetBinWidth(bin);
               //Double_t x_low   = measKine1->fx->GetBinLowEdge(bin);
               //Double_t x_high  = x_low+x_width;

               Double_t W_width = measKine1->fW.GetBinWidth(bin);
               Double_t W_low   = measKine1->fW.GetBinLowEdge(bin);
               Double_t W_high  = W_low+W_width;

               Double_t x_low   = InSANE::Kine::xBjorken_WQ2(W_high,g1Asym->fQ2Min);
               Double_t x_high  = InSANE::Kine::xBjorken_WQ2(W_low ,g1Asym->fQ2Max);

               x_bin.SetMeanLimits(  x1  , x_low          , x_high);
               Q2_bin.SetMeanLimits( Q21 , g1Asym->fQ2Min , g1Asym->fQ2Max);
               W_bin.SetMeanLimits(  W   , W_low          , W_high);


               Double_t g1   = fg1->GetBinContent(bin);
               Double_t g2   = fg2->GetBinContent(bin);

               Double_t eg1  = fg1->GetBinError(bin);
               Double_t eg2  = fg2->GetBinError(bin);

               Double_t esysg1  = g1Asym->fSystErrVsW.GetBinContent(bin);
               Double_t esysg2  = g2Asym->fSystErrVsW.GetBinContent(bin);

               if( eg1 > max_error || eg2 > max_error ) continue;
               if( g1 == 0.0 || g2 == 0.0 ) continue;
               if( TMath::IsNaN(g1) || TMath::IsNaN(g2) ) continue;

               // Add point to g1 measurement
               point.SetValue(g1);
               err1 = ErrorBar(eg1);
               err2 = ErrorBar(esysg1);
               point.SetStatError(err1); 
               point.SetSystError(err2); 
               point.CalculateTotalError();
               g1_meas->AddDataPoint(new DataPoint(point));

               point.Print();

               // Add point to g2 measurement
               point.SetValue(g2);
               err1 = ErrorBar(eg2);
               err2 = ErrorBar(esysg2);
               point.SetStatError(err1); 
               point.SetSystError(err2); 
               point.CalculateTotalError();
               g2_meas->AddDataPoint(new DataPoint(point));

               //Double_t R_2      = InSANE::Kine::R1998(x,Q2);
               //Double_t R        = SFs->R(x,Q2);
               //Double_t F1       = SFs->F1p(x,Q2);
               //Double_t D        = InSANE::Kine::D(E0,Ep,theta,R);
               //Double_t d        = InSANE::Kine::d(E0,Ep,theta,R);
               //Double_t eta      = InSANE::Kine::Eta(E0,Ep,theta);
               //Double_t xi       = InSANE::Kine::Xi(E0,Ep,theta);
               //Double_t chi      = InSANE::Kine::Chi(E0,Ep,theta,phi);
               //Double_t epsilon  = InSANE::Kine::epsilon(E0,Ep,theta);
               //Double_t gamma2   = 4.0*x*x*(M_p/GeV)*(M_p/GeV)/Q2;
               //Double_t gamma    = TMath::Sqrt(gamma2);
               //Double_t cota     = 1.0/TMath::Tan(alpha);
               //Double_t csca     = 1.0/TMath::Sin(alpha);

               //Double_t g1 = 2.0*x*x*(F1/(1.0+gamma2))*(A1 + gamma*A2);
               //Double_t g2 = 3.0*x*x*(F1/(1.0+gamma2))*(A2/gamma - A1);

               //Double_t d2p = (g1+g2);

               ////eg1 = (F1/(1.0+gamma2))*(eA1 + gamma*eA2);
               //Double_t a1 = 2.0*x*x*(F1/(1.0+gamma2));
               //Double_t a2 = 2.0*x*x*(F1/(1.0+gamma2))*gamma;
               //eg1 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               ////eg2 = (F1/(1.0+gamma2))*(eA2/gamma - eA1);
               //a1 = -3.0*x*x*(F1/(1.0+gamma2));
               //a2 = 3.0*x*x*(F1/(1.0+gamma2))/gamma;
               //eg2 = TMath::Sqrt( TMath::Power(a1*eA1,2.0) + TMath::Power(a2*eA2,2.0));
               //Double_t ed2p = 1.0*TMath::Sqrt( eg1*eg1 + eg2*eg2); 
               ////c0 = 1.0/(1.0 + eta*xi);
               ////c11 = (1.0 + cota*chi)/D ;
               ////c12 = (csca*chi)/D ;
               ////c21 = (xi - cota*chi/eta)/D ;
               ////c22 = (xi - cota*chi/eta)/(D*( TMath::Cos(alpha) - TMath::Sin(alpha)*TMath::Cos(phi)*TMath::Cos(phi)*chi));

               ////if( TMath::Abs(A80) > 0.0 ) {
               ////   std::cout << "(E0,Ep,theta,phi) = (" << E0 << ","
               ////                                        << Ep << ","
               ////                                        << theta << ","
               ////                                        << phi   << ")" << std::endl;  
               ////   std::cout << " delta R      = " << R - R_2 << std::endl;
               ////   std::cout << " A180         = " << A180 << std::endl;
               ////   std::cout << " c0*c11 - 1/D = " << c0*c11 - 1.0/D << std::endl;
               ////   std::cout << " D            = " << D << std::endl;
               ////   std::cout << " 1/c11        = " << 1.0/c11 << std::endl;
               ////   std::cout << " c0           = " << c0 << std::endl;
               ////   std::cout << " c11          = " << c11 << std::endl;
               ////   std::cout << " 1/D          = " << 1.0/D << std::endl;
               ////   std::cout << " c12          = " << c12 << std::endl;
               ////   std::cout << " -eta/d       = " << -eta/d << std::endl;
               ////   std::cout << " c21          = " << c21 << std::endl;
               ////   std::cout << " xi/D         = " <<  xi/D<< std::endl;
               ////   std::cout << " c22          = " << c22 << std::endl;
               ////   std::cout << " 1.0/d        = " << 1.0/d << std::endl;
               ////   std::cout << " chi          = " << chi << std::endl;
               ////   
               ////}
               ////             
               //fg1->SetBinContent(bin,g1);
               //fg2->SetBinContent(bin,g2);
               //fg1->SetBinError(bin, eg1);
               //fg2->SetBinError(bin, eg2);

               //fd2->SetBinContent(bin,d2p);
               //fd2->SetBinError(bin, ed2p);
            }
         }
      }

      //g1_meas->Print("data");
   }


   // --------------------------------------------------------------------------
   //
   g1_meas->PrintTable(std::cout);
   g2_meas->PrintTable(std::cout);


   // --------------------------------------------------------------------------
   // Functions

   exp->Print();
   manager->SaveExperiment(exp);

   return(0);
}
