Int_t plot_xg1p(Double_t Qsq = 15.0,Double_t QsqBinWidth=7.0, Int_t aNumber = 2008){

   Int_t nMerge = 1;

   TCanvas * c = new TCanvas("xg1p","xg1p",1200,800);

   // Create Legend
   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // First Plot all the Polarized Structure Function Models! 
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsA = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsA->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsB = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsB->SetPolarizedPDFs( new AAC08PolarizedPDFs);
   InSANEPolarizedStructureFunctionsFromPDFs * pSFsC = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFsC->SetPolarizedPDFs( new GSPolarizedPDFs);

   InSANEPolarizedStructureFunctionsFromPDFs *pSFsD  = new InSANEPolarizedStructureFunctionsFromPDFs(); 
   pSFsD->SetPolarizedPDFs( new StatisticalPolarizedPDFs); 

   Int_t npar=1;
   Double_t xmin=0.01;
   Double_t xmax=1.0;
   TF1 * xg1p = new TF1("xg1p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   TF1 * xg1pA = new TF1("xg1pA", pSFsA, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   TF1 * xg1pB = new TF1("xg1pB", pSFsB, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   TF1 * xg1pC = new TF1("xg1pC", pSFsC, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");
   TF1 * xg1pD = new TF1("xg1pC", pSFsD, &InSANEPolarizedStructureFunctions::Evaluatexg1p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg1p");

   xg1p->SetParameter(0,Qsq);
   xg1pA->SetParameter(0,Qsq);
   xg1pB->SetParameter(0,Qsq);
   xg1pC->SetParameter(0,Qsq);
   xg1pD->SetParameter(0,Qsq);

   xg1p->SetLineColor(1);
   xg1pA->SetLineColor(3);
   xg1pB->SetLineColor(kOrange-1);
   xg1pC->SetLineColor(kCyan+1);
   xg1pD->SetLineColor(kMagenta);

   Int_t width = 3;
   xg1p->SetLineWidth(width);
   xg1pA->SetLineWidth(width);
   xg1pB->SetLineWidth(width);
   xg1pC->SetLineWidth(width);
   xg1pD->SetLineWidth(width);

   xg1p->SetLineStyle(1);
   xg1pA->SetLineStyle(1);
   xg1pB->SetLineStyle(1);
   xg1pC->SetLineStyle(1);
   xg1pD->SetLineStyle(1);

   xg1pA->SetMaximum(0.1);
   xg1pA->SetMinimum(-0.03);

   xg1pA->Draw();

   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   // Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = 
      new NucDBBinnedVariable("Qsquared",Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   // Create a new measurement which has all data points within the bin 
   NucDBMeasurement * g1pQsq1 = new NucDBMeasurement("g1pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   NucDBMeasurement * g1pSANE = new NucDBMeasurement("g1pSANE","g_{1}^{p} SANE");

   TF2 * xf = new TF2("xf","x*x*y",0,1,-1,1);
   TList * listOfMeas = manager->GetMeasurements("g1p");
   TMultiGraph * mg = new TMultiGraph();

   for(int i =0; i<listOfMeas->GetEntries();i++) {

       NucDBMeasurement * g1pMeas = (NucDBMeasurement*)listOfMeas->At(i);

       if( !strcmp(g1pMeas->GetExperimentName(),"SANE"))  {
          g1pSANE->AddDataPoints(g1pMeas->GetDataPoints());
       } else {

       TList * points = g1pMeas->FilterWithBin(Qsqbin);
       g1pQsq1->AddDataPoints(points);

       TGraphErrors * graph = g1pMeas->BuildGraph("x");
       graph->Apply(xf);
       graph->SetMarkerStyle(20+i);
       graph->SetMarkerSize(1.6);
       graph->SetMarkerColor(kBlue-2-i);
       graph->SetLineColor(kBlue-2-i);
       graph->SetLineWidth(1);

       mg->Add(graph,"ep");

       leg->AddEntry(graph,Form("%s %s",g1pMeas->GetExperimentName(),g1pMeas->GetTitle() ),"lp");
       }

   }

   TGraphErrors * gr = g1pQsq1->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(3);
   gr->SetLineColor(3);
   mg->Add(gr,"ep");
   leg->AddEntry(gr,Form("%s %s","All g1p data",g1pQsq1->GetTitle() ),"ep");

   std::vector<double> sane_Q2_values;
   g1pSANE->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   std::cout << sane_Q2_values.size() << std::endl;
   TList * sane_g1p_meas = new TList();
   for(int i = 0; i<sane_Q2_values.size(); i++) {
      NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);
      NucDBMeasurement * g1pSANE_i = new NucDBMeasurement(Form("g1pSANE%d",i),"g_{1}^{p} SANE");
      g1pSANE_i->AddDataPoints(g1pSANE->FilterWithBin(avar));
      sane_g1p_meas->Add(g1pSANE_i);
      std::cout << i << std::endl;
   }
   std::cout << sane_g1p_meas->GetEntries() << " Q2 bins" << std::endl;

   NucDBMeasurement * aMeas = 0;
   
   // Q2 bin 0
   aMeas = (NucDBMeasurement*)sane_g1p_meas->At(0);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(2);
   gr->SetLineColor(2);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[0]);
   mg->Add(gr,"ep");

   // Q2 bin 1
   aMeas = (NucDBMeasurement*)sane_g1p_meas->At(1);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(kRed+1);
   gr->SetLineColor(kRed+1);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[1]);
   mg->Add(gr,"ep");
      
   // Q2 bin 2
   aMeas = (NucDBMeasurement*)sane_g1p_meas->At(2);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(kRed+2);
   gr->SetLineColor(kRed+2);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[2]);
   mg->Add(gr,"ep");
      
   // Q2 bin 3
   aMeas = (NucDBMeasurement*)sane_g1p_meas->At(3);
   aMeas->MergeDataPoints(nMerge,"x",true);
   gr = aMeas->BuildGraph("x");
   gr->Apply(xf);
   gr->SetMarkerColor(kRed+3);
   gr->SetLineColor(kRed+3);
   gr->SetLineWidth(2);
   gr->SetMarkerStyle(gNiceMarkerStyle[3]);
   mg->Add(gr,"ep");
      
   //g1pSANE->MergeDataPoints(4,"x",true);
   //gr = g1pSANE->BuildGraph("x");
   //gr->Apply(xf);
   //gr->SetMarkerColor(2);
   //gr->SetLineColor(2);
   //mg->Add(gr,"ep");

   //g1pSANE->SortBy("Qsquared");
   //g1pSANE->SortBy("x");
   g1pSANE->Print();

   //TMultiGraph * mg2 = g1pSANE->BuildGraphUnique("x","Qsquared");
   //for(int i = 0; i<mg2->GetListOfGraphs()->GetEntries(); i++) {
   //   TGraphErrors * gr = (TGraphErrors*)mg2->GetListOfGraphs()->At(i);
   //   //gr->Apply(xf);
   //}
   //mg->Add(mg2);

   gPad->SetGridy(true);
   g1pSANE->PrintBreakDown("Qsquared");
   // --------------------------
   // 
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.05,0.05);
   mg->GetXaxis()->SetLimits(0.01,0.99);
   //g1pSANE->Print("data");

   //xg1pB->Draw("same");
   //xg1pC->Draw("same");
   //xg1pD->Draw("same");

   //xg1p->SetParameter(0,3.0);
   //xg1p->Draw("same");


   // Complete the Legend 
   //leg->AddEntry(xg1p,"xg1p BB ","l");
   //leg->AddEntry(xg1pA,"xg1p DNS2005 ","l");
   //leg->AddEntry(xg1pB,"xg1p AAC ","l");
   //leg->AddEntry(xg1pC,"xg1p GS ","l");
   //leg->AddEntry(xg1pD,"xg1p Stat ","l");
   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.16,0.95,Form("x^{2}g_{2}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));


   c->SaveAs(Form("results/sane_results/%d/plot_xg1p_%d.png",aNumber,nMerge));
   c->SaveAs(Form("results/sane_results/%d/plot_xg1p_%d.pdf",aNumber,nMerge));

   return 0;
}

