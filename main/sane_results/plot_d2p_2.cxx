/** Plots models and data for g2p at Qsq. */
Int_t plot_d2p_2(Double_t Qsq = 15.0,Double_t QsqBinWidth=7.0, Int_t aNumber = 2008){

   gSystem->mkdir(Form("results/sane_results/%d",aNumber));

   Int_t  nMerge0   = 1;
   double dxMerge0  = 0.025;
   Int_t  nMerge    = 1;
   double dWMerge   = 0.05;

   TCanvas * c = new TCanvas("xg2p","xg2p",1600,800);

   // Create Legend
   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetNColumns(2);
   leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));

   // Model 
   Int_t npar    = 1;
   Int_t width   = 3;
   Double_t xmin = 0.0025;
   Double_t xmax = 1.0;
   InSANEPolarizedStructureFunctionsFromPDFs * pSFs = new InSANEPolarizedStructureFunctionsFromPDFs();
   pSFs->SetPolarizedPDFs( new BBPolarizedPDFs);
   TF1 * xg2p = new TF1("xg2p", pSFs, &InSANEPolarizedStructureFunctions::Evaluatexg2p, 
               xmin, xmax, npar,"InSANEPolarizedStructureFunctions","Evaluatexg2p");
   xg2p->SetParameter(0,Qsq);
   xg2p->SetLineColor(1);
   xg2p->SetLineWidth(width);
   xg2p->SetLineStyle(1);
   //xg2pA->Draw();

   // Next,  Plot all the DATA
   NucDBManager * manager = NucDBManager::GetManager();

   // Create a new measurement which has all data points within the bin 
   NucDBBinnedVariable * Ebeam_bin47 = new NucDBBinnedVariable("Ebeam","Ebeam",4.7,0.1);
   NucDBBinnedVariable * Ebeam_bin59 = new NucDBBinnedVariable("Ebeam","Ebeam",5.9,0.1);

   TMultiGraph * mg = new TMultiGraph();

   NucDBExperiment * exp = manager->GetExperiment("SANE");

   NucDBMeasurement * g1pSANE = exp->GetMeasurement("g1p");
   NucDBMeasurement * g2pSANE = exp->GetMeasurement("g2p");

   //g1pSANE->Print("data");

   g1pSANE->PrintBreakDown("Qsquared");

   NucDBMeasurement * g1pSANE_47 =  g1pSANE->CreateMeasurementFilteredWithBin(Ebeam_bin47);
   NucDBMeasurement * g2pSANE_47 =  g2pSANE->CreateMeasurementFilteredWithBin(Ebeam_bin47);
   NucDBMeasurement * g1pSANE_59 =  g1pSANE->CreateMeasurementFilteredWithBin(Ebeam_bin59);
   NucDBMeasurement * g2pSANE_59 =  g2pSANE->CreateMeasurementFilteredWithBin(Ebeam_bin59);

   std::vector<double> sane_Q2_values;
   g1pSANE_59->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   std::cout << sane_Q2_values.size() << std::endl;

   TList * sane_g1p_meas = new TList();
   TList * sane_g2p_meas = new TList();


   // Now organize by Q2
   for(int i = 0; i<sane_Q2_values.size(); i++) {

      NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);

      // combined energies but separated Q2 measurements:
      NucDBMeasurement * g1pSANE_i = new NucDBMeasurement(Form("g1pSANE%d",i),"g_{1}^{p} SANE");
      NucDBMeasurement * g2pSANE_i = new NucDBMeasurement(Form("g2pSANE%d",i),"g_{2}^{p} SANE");

      NucDBMeasurement * g1pSANE_47_merged = new NucDBMeasurement("g1p","g_{1}^{p}");
      g1pSANE_47_merged->AddDataPoints(g1pSANE_47->FilterWithBin(avar));

      NucDBMeasurement * g1pSANE_59_merged = new NucDBMeasurement("g1p","g_{1}^{p}");
      g1pSANE_59_merged->AddDataPoints(g1pSANE_59->FilterWithBin(avar));

      NucDBMeasurement * g2pSANE_47_merged = new NucDBMeasurement("g2p","g_{1}^{p}");
      g2pSANE_47_merged->AddDataPoints(g2pSANE_47->FilterWithBin(avar));

      NucDBMeasurement * g2pSANE_59_merged = new NucDBMeasurement("g2p","g_{1}^{p}");
      g2pSANE_59_merged->AddDataPoints(g2pSANE_59->FilterWithBin(avar));

      g1pSANE_47_merged->MergeDataPoints(nMerge0,"x",true);
      g2pSANE_47_merged->MergeDataPoints(nMerge0,"x",true);
      g1pSANE_59_merged->MergeDataPoints(nMerge0,"x",true);
      g2pSANE_59_merged->MergeDataPoints(nMerge0,"x",true);


      g1pSANE_i->AddDataPoints(g1pSANE_47_merged->GetDataPoints());
      g1pSANE_i->AddDataPoints(g1pSANE_59_merged->GetDataPoints());

      g2pSANE_i->AddDataPoints(g2pSANE_47_merged->GetDataPoints());
      g2pSANE_i->AddDataPoints(g2pSANE_59_merged->GetDataPoints());

      sane_g1p_meas->Add(g1pSANE_i);
      sane_g2p_meas->Add(g2pSANE_i);

      std::cout << i << std::endl;
      g1pSANE_i->PrintBreakDown("Qsquared");
      g2pSANE_i->PrintBreakDown("Qsquared");
   }
   std::cout << sane_g2p_meas->GetEntries() << " Q2 bins" << std::endl;



   // Create d2p measurement
   TMultiGraph * mg = new TMultiGraph();
   TList * sane_d2p_meas = new TList();

   for(int i = 0; i<sane_Q2_values.size(); i++) {

      NucDBMeasurement * d2pSANE_i = new NucDBMeasurement(Form("d2pSANE%d",i),"d_{2}^{p} SANE");
      sane_d2p_meas->Add(d2pSANE_i);
      NucDBMeasurement * g1pSANE_i = (NucDBMeasurement*)sane_g1p_meas->At(i);
      NucDBMeasurement * g2pSANE_i = (NucDBMeasurement*)sane_g2p_meas->At(i);

      for(int j = 0; j<g1pSANE_i->GetNDataPoints(); j++) {
         NucDBDataPoint * g1p_point = (NucDBDataPoint*)g1pSANE_i->GetDataPoints()->At(j);
         NucDBDataPoint * g2p_point = (NucDBDataPoint*)g2pSANE_i->GetDataPoints()->At(j);
         double x   = g1p_point->GetBinVariable("x")->GetMean();
         double Q2  = g1p_point->GetBinVariable("Qsquared")->GetMean();
         double M   = M_p/GeV;
         double g1  = g1p_point->GetValue();
         double eg1 = g1p_point->GetError();
         double g2  = g2p_point->GetValue();
         double eg2 = g2p_point->GetError();
         double d2  = x*x*(2.0*g1 + 3.0*g2);
         double ed2  = x*x*(2.0*eg1+3.0*eg2);

         double xi    = 2.0*x/(1.0+TMath::Sqrt(1.0 + (2.0*x*M)*(2.0*x*M)/Q2));
         double g1_coeff_nach = (xi*xi/x)*(xi*xi/x)*(x/xi);
         double g2_coeff_nach = (xi*xi/x)*(xi*xi/x)*((3.0*x*x)/(2.0*xi*xi) - (3.0*M*M*x*x)/(4.0*Q2));
         double d2p_nach_int      = (g1_coeff_nach*g1 + g2_coeff_nach*g2);
         double err_d2p_nach_int  = (g1_coeff_nach*eg1 + g2_coeff_nach*eg2);

         NucDBDataPoint d2p_point((*g2p_point));
         d2p_point.SetValue(d2);
         d2p_point.SetStatError(ed2);
         d2pSANE_i->AddDataPoint(new NucDBDataPoint(d2p_point));
      }

      d2pSANE_i->MergeNeighboringDataPoints(nMerge,"W",dWMerge,true);
      d2pSANE_i->SortBy("x");

      TGraphErrors * gr = d2pSANE_i->BuildGraph("x");
      gROOT->GetColor(40+i)->SetAlpha(0.5);
      gr->SetFillColor(40+i);
      gr->SetMarkerColor(kRed+i);
      gr->SetLineColor(kRed+i);
      gr->SetLineWidth(1);
      gr->SetMarkerStyle(gNiceMarkerStyle[i]);
      mg->Add(gr,"e3p");
      mg->Add(gr,"ep");

      ofstream d2pout(Form("results/sane_results/%d/d2p_integrand_%d_%d_%d.txt",aNumber,i,nMerge0,nMerge));
      d2pSANE_i->PrintTable(d2pout);
   }

   gPad->SetGridy(true);


   // --------------------------
   // 
   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-0.25,0.25);
   mg->GetXaxis()->SetLimits(0.01,0.99);
   //g2pSANE->Print("data");

   c->SaveAs(Form("results/sane_results/%d/plot_d2p_2_%d_%d.png",aNumber,nMerge0,nMerge));
   c->SaveAs(Form("results/sane_results/%d/plot_d2p_2_%d_%d.pdf",aNumber,nMerge0,nMerge));

   return(0);
}
