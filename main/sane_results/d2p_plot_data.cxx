InSANEPolarizedStructureFunctions * psfs;
InSANEFormFactors                 * ffs;

class  d2p_elastic_integral {
   public:
      double operator() (double *x, double *p) { return( ffs->d2p_el(x[0])); }
};

class  d2n_elastic_integral {
   public:
      double operator() (double *x, double *p) { return( ffs->d2n_el(x[0])); }
};

class  d2p_I {
   public:
      double operator() (double *x, double *p) { return( /*ffs->d2p_el(x[0]) +*/ psfs->d2p_tilde(x[0],p[0],p[1])); }
};

class  d2n_I {
   public:
      double operator() (double *x, double *p) { return( ffs->d2n_el(x[0]) + psfs->d2n_tilde(x[0],p[0],p[1])); }
};



Int_t d2p_plot_data(Int_t aNumber = 0) {

   NucDBManager * dbman = NucDBManager::GetManager();

   d2p_elastic_integral  * fobj  = new d2p_elastic_integral();
   d2n_elastic_integral * fobj2  = new d2n_elastic_integral();
   d2p_I * fd2pI  = new d2p_I();
   d2n_I * fd2nI  = new d2n_I();

   TF1                 * d2p_el = new TF1("d2p_el",fobj,0.1,10,0,"d2p_elastic_integral");
   TF1                 * d2n_el = new TF1("d2n_el",fobj2,0.1,10,0,"d2n_elastic_integral");
   TF1                 * d2p_total = new TF1("d2p_int",fd2pI,0.1,10,2,"d2p_I");
   TF1                 * d2n_total = new TF1("d2n_int",fd2nI,0.1,10,2,"d2n_I");
   d2p_total->SetParameters(0.01,0.9);
   d2n_total->SetParameters(0.01,0.9);

   InSANEFunctionManager * fman = InSANEFunctionManager::GetManager(); 
   psfs = fman->CreatePolSFs(6);
   ffs  = fman->CreateFFs(0);

   TCanvas * c = new TCanvas();

   d2p_total->SetLineColor(1);
   d2p_total->SetLineStyle(2);

   d2n_total->SetLineColor(4);
   d2n_total->SetLineStyle(2);

   d2p_el->SetLineColor(1);
   d2n_el->SetLineColor(4);

   
   //TList * measurements = dbman->GetMeasurements("d2p");
   //measurements->Print("data");

   //std::cout << " ------------------------------------------- " << std::endl;
   //std::cout << " Papers : " << std::endl;
   //dbman->ListPapers();
   //dbman->ListCalculations();
   //dbman->ListExperiments();

   //TList * papers = dbman->GetPapers();
   //papers->Print("data");
   //TList * measurements = dbman->GetMeasurements("d2p");
   //measurements->Print("data");

   TList * calc_meas = dbman->GetMeasurementCalculations("d2p");
   //TList * calc_meas = new TList();//dbman->GetMeasurementCalculations("d2p");
   TList * meas      = dbman->GetMeasurements("d2p");
   calc_meas->AddAll(meas);

   //calc_meas->Print();
   TMultiGraph * mg_calc  = NucDB::CreateMultiGraph(calc_meas,"Qsquared");

   TMultiGraph * mg = new TMultiGraph();//dbman->GetMultiGraph("d2p","Qsquared");

   TList * sane_meas = dbman->GetMeasurementCalculations("d2p measured");

   // --------------------------------
   TCanvas * c = new TCanvas();

   mg->Add(mg_calc);
   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.1,11.0);
   mg->GetYaxis()->SetRangeUser(-0.03,0.03);

   d2p_total->Draw("same");
   d2n_total->Draw("same");
   d2p_el->Draw("same");
   d2n_el->Draw("same");

   c->SaveAs(Form("results/sane_results/d2p_existing_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/d2p_existing_%d.pdf",aNumber));


   NucDBExperiment * exp = dbman->GetExperiment("SANE");
   NucDBMeasurement * d2p_sane = exp->GetMeasurement("d2p");
   if(!d2p_sane) { 
      d2p_sane = new NucDBMeasurement("d2p","d2p");
   }

   // -------------------------------
   c = new TCanvas();


   TMultiGraph * mg2 = new TMultiGraph();//dbman->GetMultiGraph("d2p","Qsquared");
   TGraphErrors * gr = new TGraphErrors(3);
   //1   2.875  0.25   0.55    -0.0071622    4.61092e-07  -0.0142458   0.000626205  0.00289202   0.00379031    -0.00643789   0.000623639
   //2   4.125  0.4    0.9     0.00216378    1.07462e-06  -0.0066521   0.000969621  0.00660476   -0.000152062  0.00104377    0.000913869
   //3   6.125  0.6    0.9     -0.00313995   4.78759e-07  -0.00295964  0.000747053  0.0121508    -8.65039e-05  -0.00281223   0.000729593
   //4   4.25   0.15   0.9     -0.00299381   1.86055e-06  -0.0103362   0.00108446   0.000997577  -0.000146157  -0.0031866    0.00104313
   gr->SetPoint( 1-1,   2.875,-0.0142458  +0.00289202   + 0.00379031  );
   gr->SetPoint( 2-1,   4.125,-0.0066521  +0.00660476   -0.000152062);
   gr->SetPoint( 3-1,   6.125,-0.00295964 +0.0121508    -8.65039e-05);
   gr->SetPointError( 1-1,   0,0.00062620 );
   gr->SetPointError( 2-1,   0,0.000969621 );
   gr->SetPointError( 3-1,   0,0.000747053 );
   gr->SetMarkerStyle(20);
   gr->SetLineColor(2);
   gr->SetMarkerColor(2);
   mg->Add(gr,"ep");

   gr = new TGraphErrors(3);
   gr->SetPoint( 1-1      , 2.875 , -0.0142458 );
   gr->SetPoint( 2-1      , 4.125 , -0.0066521 );
   gr->SetPoint( 3-1      , 6.125 , -0.00295964);
   gr->SetPointError( 1-1 , 0     , 0.000626205 );
   gr->SetPointError( 2-1 , 0     , 0.000969621 );
   gr->SetPointError( 3-1 , 0     , 0.000747053 );
   gr->SetMarkerStyle(21);
   gr->SetLineColor(2);
   gr->SetMarkerColor(2);
   mg->Add(gr,"ep");

   gr = new TGraphErrors(1);
   gr->SetPoint( 4-1,   4.25 ,-0.0103362 + 0.000997577  -0.000146157);
   gr->SetPointError( 4-1,   0,0.00108446  );
   gr->SetMarkerStyle(20);
   gr->SetLineColor(4);
   gr->SetMarkerColor(4);
   mg->Add(gr,"ep");

   gr = new TGraphErrors(1);
   gr->SetPoint( 0,   4.25 ,-0.0103362);
   gr->SetPointError( 0,   0,0.00108446  );
   gr->SetMarkerStyle(21);
   gr->SetLineColor(4);
   gr->SetMarkerColor(4);
   mg->Add(gr,"ep");

   // -----------with inelastic tail sub
   //44  Q2     x_low  x_high  d2p_exp       err          d2p_exp2     err          d2p_lowx     d2p_highx     d2p_nach     err
   //0   1.625  0.15   0.35    0             0            0            0            0.0010052    0.00652178    0            0
   //1   2.875  0.25   0.55    -0.00822132   4.61092e-07  -0.0259073   0.000626205  0.00289202   0.00379031    -0.0075887   0.000623639
   //2   4.125  0.4    0.9     -0.000940484  1.07462e-06  -0.0229004   0.000969621  0.00660476   -0.000152062  -0.00189684  0.000913869
   //3   6.125  0.6    0.9     -0.0044673    4.78759e-07  -0.00792845  0.000747053  0.0121508    -8.65039e-05  -0.00402997  0.000729593
   //4   4.25   0.15   0.9     -0.00624105   1.86055e-06  -0.0298908   0.00108446   0.000997577  -0.000146157  -0.00631435  0.00104313


   gr = new TGraphErrors(3);
   gr->SetPoint( 1-1,        2.875,  -0.0259073   +  0.00289202  + 0.00379031      );
   gr->SetPoint( 2-1,        4.125,  -0.0229004   +  0.00660476   -0.000152062  );
   gr->SetPoint( 3-1,        6.125,  -0.00792845  +  0.0121508    -8.65039e-05  );
   gr->SetPointError( 1-1,   0, 0.000626205 );
   gr->SetPointError( 2-1,   0, 0.000969621 );
   gr->SetPointError( 3-1,   0, 0.000747053 );
   gr->SetMarkerStyle(22);
   gr->SetLineColor(8);
   gr->SetMarkerColor(8);
   mg->Add(gr,"ep");

   gr = new TGraphErrors(1);
   gr->SetPoint( 0,   4.25 , -0.0103362  +    0.000997577  -0.000146157);
   gr->SetPointError( 0,   0, 0.00108446  );
   gr->SetMarkerStyle(22);
   gr->SetLineColor(8);
   gr->SetMarkerColor(8);
   mg->Add(gr,"ep");


   // -------------------------------------------
   // Hoyoung's data point 
   // -0.0087  +-0.0014  (0.47 < 0.87) , Q2=1.86
   // d2p_kang_lowx  0.00787601
   // d2p_kang_highx -0.000561791
   gr = new TGraphErrors(1);
   gr->SetPoint( 0,   1.86 ,-0.0087 + 0.00787 - 0.00056);
   gr->SetPointError( 0,   0,0.0014  );
   gr->SetMarkerStyle(23);
   gr->SetLineColor(6);
   gr->SetMarkerColor(6);
   mg->Add(gr,"ep");


   mg->Draw("a");
   mg->GetXaxis()->SetLimits(0.1,11.0);
   mg->GetYaxis()->SetRangeUser(-0.03,0.03);

   d2p_total->Draw("same");
   //d2n_total->Draw("same");
   d2p_el->Draw("same");
   //d2n_el->Draw("same");

   c->Update();
   c->SaveAs(Form("results/sane_results/d2p_existing_1_%d.png",aNumber));
   c->SaveAs(Form("results/sane_results/d2p_existing_1_%d.pdf",aNumber));

   return 0;
}
