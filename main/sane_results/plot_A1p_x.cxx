Int_t plot_A1p_x(Double_t Qsq = 1.30,Double_t QsqBinWidth=0.1, Int_t aNumber = 7610){

   Int_t nMerge = 1;

   NucDBManager * manager = NucDBManager::GetManager(1);

   // Create a Bin in Q^2 for plotting a range of data on top  of all points
   NucDBBinnedVariable * Qsqbin = new NucDBBinnedVariable(
         "Qsquared",
         Form("%4.2f <Q^{2}<%4.2f",Qsq-QsqBinWidth,Qsq+QsqBinWidth));
   Qsqbin->SetBinMinimum(Qsq-QsqBinWidth);
   Qsqbin->SetBinMaximum(Qsq+QsqBinWidth);

   NucDBBinnedVariable * Qsqbin2 = new NucDBBinnedVariable("Qsquared","Q2",40.0,37.0);
   NucDBBinnedVariable * Wbin2   = new NucDBBinnedVariable("W","Wbin",40.0,36.0);

   // Create a new measurement which has all data points within the bin 
   NucDBMeasurement * A1pQsq1 = new NucDBMeasurement("A1pQsq1",Form("g_{1}^{p} %s",Qsqbin->GetTitle()));
   NucDBMeasurement * A1pSANE = new NucDBMeasurement("A1pSANE","g_{1}^{p} SANE");

   TLegend * leg = new TLegend(0.7,0.7,0.975,0.975);
   leg->SetFillStyle(0);
   leg->SetNColumns(2);
   //leg->SetHeader(Form("Q^{2} = %d GeV^{2}",(Int_t)Qsq));
   TMultiGraph * mg         = new TMultiGraph();

   TLegend     * leg2  = new TLegend(0.18,0.65,0.45,0.87);
   leg2->SetBorderSize(0);
   leg2->SetFillStyle(0);
   TMultiGraph * mg_2  = new TMultiGraph();
   TMultiGraph * mg_3  = new TMultiGraph();

   TList       * listOfMeas = manager->GetMeasurements("A1p");

   for(int i =0; i<listOfMeas->GetEntries();i++) {

      NucDBMeasurement * A1pMeas = (NucDBMeasurement*)listOfMeas->At(i);

      if( !strcmp(A1pMeas->GetExperimentName(),"SANE"))  {
         A1pSANE->AddDataPoints(A1pMeas->GetDataPoints());
         continue;
      } 

      if( !strcmp(A1pMeas->GetExperimentName(),"CLAS-E93009"))  {
         continue;
      }

      TList * points = A1pMeas->FilterWithBin(Qsqbin);
      A1pQsq1->AddDataPoints(points);

      // -----------------------------------------
      TGraphErrors * graph = A1pMeas->BuildGraph("x");
      graph->SetMarkerStyle(20+i);
      graph->SetMarkerSize(1.6);
      graph->SetMarkerColor(kBlue-2-i);
      graph->SetLineColor(kBlue-2-i);
      graph->SetLineWidth(1);

      mg->Add(graph,"ep");
      leg->AddEntry(graph,Form("%s %s",A1pMeas->GetExperimentName(),A1pMeas->GetTitle() ),"lp");

      // -----------------------------------------
      A1pMeas->ApplyFilterWithBin(Qsqbin2);
      A1pMeas->ApplyFilterWithBin(Wbin2);
      if(A1pMeas->GetNDataPoints() <= 0 ) {
         continue;
      }
      TGraphErrors * gr = A1pMeas->BuildGraph("x");
      gr->SetMarkerStyle(20+i);
      gr->SetMarkerSize(1.0);
      gr->SetMarkerColor(kBlue-2-i);
      gr->SetLineColor(kBlue-2-i);
      gr->SetLineWidth(1);
      mg_2->Add(gr,"ep");
      leg2->AddEntry(graph,Form("%s",A1pMeas->GetExperimentName()),"lp");
   }

   TGraphErrors * gr = A1pQsq1->BuildGraph("x");
   gr->SetMarkerColor(3);
   gr->SetLineColor(3);
   mg->Add(gr,"ep");
   leg->AddEntry(gr,Form("%s %s","All A1p data",A1pQsq1->GetTitle() ),"ep");

   std::vector<double> sane_Q2_values;
   A1pSANE->GetUniqueBinnedVariableValues("Qsquared",sane_Q2_values);
   std::cout << sane_Q2_values.size() << std::endl;
   TList * sane_A1p_meas = new TList();
   for(int i = 0; i<sane_Q2_values.size(); i++) {
      NucDBBinnedVariable * avar = new NucDBBinnedVariable("Qsquared","Q2",sane_Q2_values[i],0.001);
      NucDBMeasurement * A1pSANE_i = new NucDBMeasurement(Form("A1pSANE%d",i),"g_{1}^{p} SANE");
      A1pSANE_i->AddDataPoints(A1pSANE->FilterWithBin(avar));
      sane_A1p_meas->Add(A1pSANE_i);
      std::cout << i << std::endl;
   }
   std::cout << sane_A1p_meas->GetEntries() << " Q2 bins" << std::endl;


   
   // Merge SANE bins
   Int_t   nMergeMax = 5;
   TList * sane_merged_list = new TList();
   NucDBMeasurement * aMeas = 0;
   for(int i = 0; i<4 ; i++ ) {
      // Q2 bin 0
      aMeas = (NucDBMeasurement*)sane_A1p_meas->At(i);
      aMeas->MergeNeighboringDataPoints(nMergeMax,"W",0.1,"Qsquared",0.5,true);
      sane_merged_list->Add(aMeas);
      gr = aMeas->BuildGraph("x");
      //gr->Apply(xf);
      gr->SetMarkerColor(2);
      gr->SetLineColor(2);
      gr->SetLineWidth(2);
      gr->SetMarkerStyle(gNiceMarkerStyle[0]);
      mg->Add(gr,"ep");
      mg_3->Add(gr,"ep");
   }
      
   // Save 
   //NucDBExperiment  * exp = manager->GetExperiment("SANE");
   //NucDBMeasurement * sane_merged = NucDB::Merge(sane_merged_list, "A1p_merged");
   //exp->AddMeasurement(sane_merged);
   //manager->SaveExperiment(exp);

   //A1pSANE->SortBy("Qsquared");
   //A1pSANE->SortBy("x");

   //A1pSANE->Print();
   //A1pSANE->PrintBreakDown("Qsquared");

   // ------------------------------------------------------------------
   // Models

   DSSVPolarizedPDFs *DSSV = new DSSVPolarizedPDFs(); 
   InSANEPolarizedStructureFunctionsFromPDFs *DSSVSF = new InSANEPolarizedStructureFunctionsFromPDFs(); 
   DSSVSF->SetPolarizedPDFs(DSSV); 

   // Use NMC95 as the unpolarized SF model 
   NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions(); 
   F1F209StructureFunctions *F1F209 = new F1F209StructureFunctions(); 

   InSANEVirtualComptonAsymmetries *Asym1 = new InSANEVirtualComptonAsymmetries(); 
   //Asym1->SetPolarizedSFs(DSSVSF);
   //Asym1->SetUnpolarizedSFs(F1F209);  

   TF1 * Model1 = new TF1("Model1",Asym1,&InSANE_VCSABase::EvaluateA1p,
         0.0001, 0.75, 2,"InSANE_VCSABase","EvaluateA1p");

   Model1->SetParameter(0,20.0);
   Model1->SetLineColor(1);
   Model1->SetLineWidth(2);
   Model1->SetLineStyle(1);


   // -------------------------------------------------
   //
   TCanvas * c = new TCanvas("xA1p","xA1p",1200,800);
   gPad->SetGridy(true);

   mg->Draw("a");
   mg->GetYaxis()->SetRangeUser(-1.0,1.5);
   mg->GetXaxis()->SetLimits(0.0,1.0);
   leg->Draw();
   Model1->Draw("same");

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(132);
   t->SetTextColor(kBlack);
   t->SetTextSize(0.04);
   t->SetTextAlign(12); 
   t->DrawLatex(0.16,0.95,Form("A_{1}^{p}(x,Q^{2}=%d GeV^{2})",(Int_t)Qsq));

   c->SaveAs(Form("results/sane_results/%d/plot_A1p_x_%d.png",aNumber,nMerge));
   c->SaveAs(Form("results/sane_results/%d/plot_A1p_x_%d.pdf",aNumber,nMerge));


   // ------------------------------------------------
   //
   c = new TCanvas();

   mg_2->Draw("a");
   mg_2->GetYaxis()->SetRangeUser(0.0,1.0);
   mg_2->GetXaxis()->SetLimits(0.0,1.0);
   mg_2->GetXaxis()->CenterTitle(true);
   mg_2->GetXaxis()->SetTitle("x");
   mg_2->GetYaxis()->CenterTitle(true);
   mg_2->GetYaxis()->SetTitle("A_{1}^{p}");
   leg2->Draw();
   Model1->Draw("same");

   c->SaveAs(Form("results/sane_results/%d/plot_A1p_x_%d_1.png",aNumber,nMerge));
   c->SaveAs(Form("results/sane_results/%d/plot_A1p_x_%d_1.pdf",aNumber,nMerge));


   // ------------------------------------------------
   //
   c = new TCanvas();

   mg_2->Add(mg_3);
   mg_2->Draw("a");
   mg_2->GetYaxis()->SetRangeUser(0.0,1.0);
   mg_2->GetXaxis()->SetLimits(0.0,1.0);
   mg_2->GetXaxis()->CenterTitle(true);
   mg_2->GetXaxis()->SetTitle("x");
   mg_2->GetYaxis()->CenterTitle(true);
   mg_2->GetYaxis()->SetTitle("A_{1}^{p}");
   leg2->Draw();
   Model1->Draw("same");

   c->SaveAs(Form("results/sane_results/%d/plot_A1p_x_%d_2.png",aNumber,nMerge));
   c->SaveAs(Form("results/sane_results/%d/plot_A1p_x_%d_2.pdf",aNumber,nMerge));


   return 0;
}
