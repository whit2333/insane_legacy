InSANE
======
Written by Whitney Armstrong (whit@temple.edu)

Introduction
------------
InSANE is a set of C++ shared libraries which are usefull for doing data analysis and performing calculations in nuclear/particle physics.
The libraries easily link into the ROOT framework and stand alone programs. 


# Installation

## Prerequisites

Collaborators please see below about building on the farm.

### [ROOT](http://root.cern.ch "ROOT")
Make sure to build root with python enabled:

    cd $ROOTSYS
    ./configure --enable-python
    make

### [CLHEP](http://proj-clhep.web.cern.ch/proj-clhep/ "CLHEP - A Class Library for High Energy Physics ") - A Class Library for High Energy Physics.

This library provides InSANE mainly the system of units (which are the same as GEANT4) and physical constants.

#### TODO: Use new version of CLHEP or remove this dependency


### [CMake](http://www.cmake.org)

CMake build system. 

### [LHAPDF](http://lhapdf.hepforge.org "LHAPDF") - The Les Houches Accord PDF Interface

Note this has not been tested with version 6!
During the [install](http://lhapdf.hepforge.org/install "LHAPDF/install") it is recommended that you configure with the low memory option.

    ./configure --prefix=/path/to/directory --enable-low-memory

This will later be optional...

### MySQL

Database used for experimental analysis side.

### Building on the farm

A version of LHAPDF is installed into my home directory along with my latest build of InSANE so you can just that build if you would like.
This should get you most of the way there:

    export PATH=$PATH:/home/whit/bin
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/whit/lib

On the farm you can find various builds under 

    /site/12gev_phys




## Downloading InSANE

    git clone ssh://git@github.com/whit2333/InSANE.git

## Configuring your environment.

You should set the following environment variables in your .bashrc or equivalent shell commands.

    export InSANEDIR=/path/to/InSANE             # the location of the source NOT the build directory
    export InSANE_PDF_GRID_DIR=$InSANEDIR/main/grid
    export PYTHONPATH=$InSANEDIR/lib:$PYTHONPATH:$InSANEDIR/python



## Building

Since cmake is used as the build system you should make a directory outside of the InSANE directory
Once the environment is configured you can build. Here is an example which installs to the user home directory.
It will copy all the libraries to $HOME/lib, headers to $HOME/bin, and binaries to $HOME/bin

    mkdir insane_build
    git clone ssh://git@github.com/whit2333/InSANE.git
    cd insane_build
    cmake ../InSANE/. -DCMAKE_INSTALL_PREFIX=$HOME
    make [-jN]
    make install

If you choose to install into a non-standard location you will likely need to add this directory to the path and ld linking path. For our example above it would be

    export PATH=$PATH:$HOME/bin
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/lib

If you make changes to the source code and want to recompile 

    cd insane_build
    make [-jN]   # this will recompile only the files that changed and their dependencies
    make install # if all goes well


## Using with root
Add to your .rootlogon.C

     gSystem->Load("libFoam");
     gSystem->Load("libTreeViewer");
     gSystem->Load("libEG");
     /// InSANE Libraries
     gSystem->Load("libInSANE.so");

## Further Documentation

Futher documentation can be found [here](http://quarks.temple.edu/~whit/code/insane/html "InSANE doc"). 

Also check out the ["official" website](http://whit2333.github.com/InSANE "Officaial InSANE home page") for updates.







