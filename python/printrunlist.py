#!/usr/bin/env python

from hallctools import *
import getopt
import sys
import os
from ROOT import InSANERunManager,InSANEAnalysisManager,SANEAnalysisManager
#print sys.argv

def printrunlist() :
    InSANERunManager.GetRunManager().SetVerbosity(0)
    aman = SANEAnalysisManager.GetAnalysisManager()
    options, remainder = getopt.getopt(sys.argv[1:], 'f:',
      ['file='])
    input_filename=''
    #print 'OPTIONS   :', options
    for opt, arg in options:
        if opt in ('-f', '--file'):
            input_filename = arg
    aman.BuildQueue2(input_filename)
    aman.PrintQueue();


# if not imported execute program 
if __name__ == "__main__":
    printrunlist()
