#!/usr/bin/env python

import sys
import time
import datetime
import math
import subprocess

""" @package hallctools HallCTools
Python tools for HallC experiments.

More..
"""


class EDT(datetime.tzinfo):
    """Concrete imp of tzinfo for a datetime timezone  EDT
    """
    def utcoffset(self,dt):
        return timedelta(hours=4,minutes=0) 
    def tzname(self,dt):
        return "EDT"
    def dst(self,dt):
        return timedelta(0)

#def convertToMysqlDatetime(pyDatetime)
    #startdate = pyDatetime.strftime('%Y-%m-%d %H:%M:%S')
    #starttime = epicsdata[1][0].strftime('%H:%M:%S')

def convertFromUTC(utc_number):
    """
    Converts a UTC time ig 97832598.345 to a 
    datetime object. (note i have no idea how to do this in python...) 
    using the perl :
      #!/usr/bin/perl
      print scalar gmtime((shift)), "\n"
    """
    p = subprocess.Popen(["./convert_from_UTC",utc_number],stdout=subprocess.PIPE)
    out, err = p.communicate()
    return out

def get_epics_datetime(epicsDateTimeLine):
    """ Function that converts the EPICS date and time to a python datetime
    """
    epicsDatetimeFMT = '%a %b %d %H:%M:%S %Z %Y '
    dt = datetime.datetime.now()
    if epicsDateTimeLine == "":
        print "null timestamp provided!"
    else :
        dt = dt.strptime(epicsDateTimeLine,epicsDatetimeFMT)
        return(dt)

def GetBeamSourceQE(run=0):
    """ Gets the quantum efficiency for a run

    Uses database table polsource_QE
    """
    
    pass

def GetBeamEnergy():
    """Documentation for a function.

    More details.
    """
    pass

def GetBeamPolarization():
    """Documentation for a function.

    More details.
    """
    pass

def GetTargetSettings():
    """Documentation for a function.

    More details.
    """
    pass


def GetHMSSettings():
    """Documentation for a function.

    More details.
    """
    pass


def GetBetaSettings():
    """ This function should ...

    More details.
    """
    pass


