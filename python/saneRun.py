#!/usr/bin/env python

from hallctools import *
import getopt
import sys
import os
import MySQLdb
import string

#from ROOT import InSANERun,InSANERunManager
#print sys.argv 

class ExpRun :
    def __init__(self) :
        self.conn               = MySQLdb.connect(read_default_file = "~/.my.cnf",read_default_group = "client")
        self.cursor             = self.conn.cursor()
        self.simulationRunTable = " BETAG4_run_info "
        self.runTable           = " run_info "
        self.columns            = ['run_number','target_type','number_of_events','quality','replay_datetime','analysis_pass','beam_pol','target_stop_pol']
        self.SingleRunPrintCols = ['run_number','target_type','target_cup','number_of_events','quality','replay_datetime','analysis_pass','beam_pol','target_stop_pol']
    
    def IsSimulationRun(self,runNumber) :
        sqlcmd="SELECT Count(Run_number) FROM " + str(BETAG4_run_info) 
        sqlcmd += " WHERE Run_number=" + str(runNumber)
        self.cursor.execute (sqlcmd)
        row = self.fetchone ()
        if row == None:
            return False 
        else : 
            return True
        
    def PrintRun(self,runNumber) :
        sqlcmd  = "SELECT "
        sqlcmd += ",".join(self.SingleRunPrintCols)
        sqlcmd += " FROM " + str(self.runTable)
        sqlcmd += " WHERE run_number=" + str(runNumber) 
        sqlcmd += ";"
        #print sqlcmd
        tempcursor = self.conn.cursor()
        tempcursor.execute (sqlcmd)
        while (1):
            row = tempcursor.fetchone ()
            if row == None:
                break
            print row
            #n=0
            #if row[2] == None:
            #    n=0
            #else :
            #    n=float(row[1])/1000000.0
            #if str(row[7]) == 'None':
            #    pt=0.0
            #else :
            #    pt=float(row[7])
            #line = " " + str(row[0]) + " Pb:" + str("%+.2f" % float(row[6]))
            #line += " Pt:" + str("%+.2f" % float(pt)) + " @ " +str(row[3]) + "   " + str(n) + " M events. " + str(row[2]) + " at pass " + str(row[4]) 
        #print line
	    result=1

    def SetRunQuality(self,runNumber,quality) :
        sqlcmd="UPDATE " + str(self.runTable)
        sqlcmd += " SET quality=" + quality 
        sqlcmd += " WHERE Run_number=" + str(runNumber) 
        self.cursor.execute (sqlcmd)

    def ListRunsByNumberOfEvents(self,numberToList=0) :
        sqlcmd="SELECT Run_number, number_of_events FROM " + str(self.runTable)
        #sqlcmd += " WHERE Run_number=" + str(runNumber) 
        sqlcmd += " ORDER BY number_of_events ASC"
        self.cursor.execute (sqlcmd)
        while (1):
            row = self.cursor.fetchone ()
            if row == None:
                break
            if not numberToList == 0 :
                print row[0]
            else : 
                self.PrintRun(row[0])
            #n=0
            #if row[1] == None:
                #n=0
            #else :
                #n=float(row[1])/1000000.0
            #line = " run " + str(row[0]) + " has " + str(n) + " M events."
	    #print line
	    #result=1

    def ListRunsByReplayDate(self,numberToList=0) :
        sqlcmd  = "SELECT "
        sqlcmd += ",".join(self.columns)
        sqlcmd += " FROM " + str(self.runTable)
        #sqlcmd += " WHERE Run_number=" + str(runNumber) 
        if numberToList == 0 :
            sqlcmd += " ORDER BY replay_datetime ASC"
        else :
            sqlcmd += " ORDER BY replay_datetime DESC LIMIT " + str(numberToList)
        self.cursor.execute (sqlcmd)
        count=0
        while (1):
            count+=1
            row = self.cursor.fetchone ()
            if row == None:
                break
            lrow = list(row)
            #print lrow
            line = ""
            for elem in lrow : 
                line += string.rjust(str(elem), 10)
                #print elem
                #if isfloat(elem) :
                #    line += str(float(elem))
                #elif isint(elem) :
                #    line += str(int(elem))
                #else :
                #line += str(elem)
                line += " " 
            print line 
                    





            #if not numberToList == 0 :
            #    if count > numberToList :
            #        break
            #    print row[0]
            #else : 
            #    self.PrintRun(row[0])

    def ListSimulationRunsByNumberOfEvents(self,numberToList=0) :
        sqlcmd="SELECT Run_number, number_of_events FROM " + str(self.simulationRunTable)
        #sqlcmd += " WHERE Run_number=" + str(runNumber) 
        sqlcmd += " ORDER BY number_of_events ASC"
        self.cursor.execute (sqlcmd)
        while (1):
            row = self.cursor.fetchone ()
            if row == None:
                break
            n=0
            if row[1] == None:
                n=0
            else :
                n=float(row[1])/1000000.0
            line = " run " + str(row[0]) + " has " + str(n) + " M events."
	    print line
	    result=1

def isfloat(x):
    if not x.isnumeric() :
        return False
    try:
        a = float(x)
    except ValueError:
        return False
    except TypeError:
        return False
    else:
        return True

def isint(x):
    if not x.isnumeric():
        return False
    try:
        a = float(x)
        b = int(a)
    except ValueError:
        return False
    except TypeError:
        return False
    else:
        return a == b


def listbetag4run():
    conn   = MySQLdb.connect(read_default_file = "~/.my.cnf",read_default_group = "client")
    cursor = conn.cursor()
    result = 0
    sqlcmd="SELECT Run_number,number_of_events FROM BETAG4_run_info ORDER BY start_date ASC"
    #sqlcmd = sqlcmd + "Run_number=" + str(runnumber)
    cursor.execute (sqlcmd)
    while (1):
        row = cursor.fetchone ()
        if row == None:
           break
        line = " Simulation run " + str(row[0]) + " has " + str(row[1]) + " events."
	print line
	result=1
    cursor.close()
    conn.close()
    return result

def betag4run(runnumber):
    conn   = MySQLdb.connect(read_default_file = "~/.my.cnf",read_default_group = "client")
    cursor = conn.cursor()
    result = 0
    sqlcmd="SELECT Run_number,number_of_events FROM BETAG4_run_info WHERE "
    sqlcmd = sqlcmd + "Run_number=" + str(runnumber)
    cursor.execute (sqlcmd)
    while (1):
        row = cursor.fetchone ()
        if row == None:
           break
        line = " Simulation run " + str(row[0]) + " has " + str(row[1]) + " events."
	print line
	result=1
    cursor.close()
    conn.close()
    return result

def insanerun(runnumber):
    conn   = MySQLdb.connect(read_default_file = "~/.my.cnf",read_default_group = "client")
    cursor = conn.cursor()
    sqlcmd="SELECT Run_number,number_of_events FROM run_info WHERE "
    sqlcmd = sqlcmd + "Run_number=" + str(runnumber)
    cursor.execute (sqlcmd)
    while (1):
        row = cursor.fetchone ()
        if row == None:
           break
        line = " Run " + str(row[0]) + " has " + str(row[1]) + " events."
	print line
	result=1
    cursor.close()
    conn.close()
    return result

def usage() : 
    print " Print information about runs "
    print " -h,--help    This information"
    print " -l,--list    List the runs "
    print " -d,--date    Arrange by replay date"
    print " -r #,--run=#     List information about a specific run number."
    

def saneRun() :
    ''' Executed if module is not imported '''
    run             = ExpRun()
    version         = '1.0'
    verbose         = False
    output_filename = 'default.out'
    isSimRun        = False
    isInsaneRun     = False
    listSimRuns     = False
    listRuns        = False
    addComment      = False
    addQuality      = False
    comment         = ''
    quarlity        = ''
    runNumber       = 0
    numberToList    = 10 
    orderbydate     = False

    options, remainder = getopt.getopt(sys.argv[1:], 'hldsvVN:n:q:c:r:o:',
      ['output=', 'verbose','version','run=','list=','comment=','help','num='])
    for opt, arg in options:

        if opt in ('-o', '--output'):
            output_filename = arg

        elif opt in ('-v', '--verbose'):
            verbose = True

        elif opt in ('-r', '--run'):
            runNumber=int(arg)

        elif opt in ('-s', '--sim'):
            isSimRun= True

        elif opt in ('-n', '--num'):
            numberToList=int(arg)

        elif opt in ('-d', '--sim'):
            orderbydate= True

        elif opt in ('-l', '--list'):
            listSimRuns= True
            listRuns= True

        elif opt in ('-c', '--comment'):
            addComment= True
            comment=str(arg)

        elif opt in ('-q', '--quality'):
            addQuality=True
            quality=str(arg)

        elif opt in ('-h', '--help'):
            usage()

        elif opt in ('-V', '--version'):
            print version

        else :
            usage()
    
    if not runNumber == 0:
        if addQuality :
            run.SetRunQuality(runNumber,quality)
        
    #if isSimRun :
        #if betag4run(runNumber) :
	    #if insanerun(runNumber) :
	        #print " Simulation has been replayed. " 
    #if listSimRuns :
        #listbetag4run()
    if not runNumber == 0 :
        run.PrintRun(runNumber)
    else :
        if listRuns :
            if not orderbydate :
                run.ListRunsByNumberOfEvents(numberToList)
            else :
	        run.ListRunsByReplayDate(numberToList)


# if not imported execute program 
if __name__ == "__main__":
    import sys
    saneRun()

