#!/usr/bin/env python

""" @package InSANEpython
Python tools for HallC experiments.

More..
"""

from hallctools import *
import MySQLdb
#import sqlite3
import os

class InSANEHallCExtractor:
    """ Base class for an extractor of values from text files produced by the Hall C analyzer

        For example the epics log produced  in replay/scalers/epics#run#.txt is used for 
        InSANEEpicsExtractor.
        Also A database is filled 
    """
    def __init__(self):
        self.valueNames = []
        self.values = []
        self.valueTitles = []
        self.searchFile = 0
        self.filename = 0
        self.runNumber = 0

        self.mysqlconn = MySQLdb.connect (read_default_file='~/.my.cnf' )

        self.dbfile = os.environ['InSANE_DB_PATH']
        #print self.dbfile
        #self.conn = sqlite3.connect(self.dbfile+'/SANE.db')
        #self.cursor = self.conn.cursor()
        self.columns = 0
        self.valueAverages = []

    def InitializeQuantities(self):
        """  """
        self.values =  [ [] for i in range(len(self.valueNames)) ]
        if len(self.values) != len(self.valueNames) :
            print " Length of quantities does not match lenth of quantityNames "

    def AddQuantity(self,name,title):
        self.valueNames.append(name)
        self.valueTitles.append(title)

    def CalculateAverageValues(self):
        """ """
        pass

    def MySQLUpdate(self,columns):
        """ Updates the MySQL tables.
        """ 
        sqlcmd="INSERT INTO run_info SET "
        for n, v in columns.iteritems():
           sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ON DUPLICATE KEY UPDATE "
        for n, v in columns.iteritems():
            sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ; "
        #print " MySQL: " 
        #print sqlcmd
        self.mysqlconn.query(sqlcmd)

    def SQLiteUpdate(self,columns):
        """ """ 
        sqlcmd="INSERT OR IGNORE INTO run_info ( "
        for n, v in columns.iteritems():
           sqlcmd = sqlcmd + n +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ) VALUES ( "
        for n, v in columns.iteritems():
           sqlcmd = sqlcmd + str(v) + ", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " )"
        sqlcmd = sqlcmd + ";"
        #print "sqlite3: " 
        #print sqlcmd
        #self.cursor.execute (sqlcmd)
        sqlcmd = "UPDATE run_info SET "
        for n, v in columns.iteritems():
            sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " WHERE Run_number=" + str(columns['Run_number']) + " ;"
        #print "sqlite3: " 
        #print sqlcmd
        #self.cursor.execute(sqlcmd)
        #print "sqlite3: commit " 
        #self.conn.commit()
#_________________________________________________________________________#

class InSANEEpicsExtractor(InSANEHallCExtractor):
    """Extracts Epics information from Hall C analyzer epics output
       text file.

    The file is assumed to have the following structure:
    """
    def __init__(self,num):
        InSANEHallCExtractor.__init__(self)
        self.valueNames = [" epics event # ", "epicsdatetime", "hcptPT_Encoder","hcptPolarization","ibcm1","ibcm2","HC:Bigcalth1",
             "HC:Bigcalth2","HC:Bigcalth3","HC:Bigcalth4","HC:SCerth1","HC:SCerth2","HALLC:p","EHCSR_XIPEAK","EHCSR_YIPEAK",
             "MBSY3C_energy"]
        self.averagedValueNames = ["Target pol","BCM1","BCM2"]
        self.valueTitles = ["EventNumber","datetime","Target Encoder","Target pol","BCM1","BCM2",
                            "Bigcal Threshold","Bigcal Threshold2","Bigcal Threshold3","Bigcal Threshold4",
                            "Cherenkov Threshold", "Cherenkov Threshold2","Beamenergy", "xpeak","ypeak"]
        self.runNumber = num
        self.filename = "scalers/epics"+str(self.runNumber)+".txt"
        self.searchFile = open(self.filename,"r")
        self.searchFile2 = open(self.filename, "r") # used to read one line ahead
        self.nextline = self.searchFile2.readline() # keep it one line ahead of searchfile
        self.secondsIntoRun = []
        self.startdate = 0
        self.starttime = 0
        self.enddate   = 0
        self.endtime   = 0
        self.columns   = 0
        self.beamEnergy =0

    def ExtractValues(self):
        for line in self.searchFile:
            self.nextline = self.searchFile2.readline()
            for j, quantity in enumerate(self.valueNames):
                if quantity in line:
                    #print line
                    if j==0: # special sequence for initial epics event
                        self.values[j].append( float(line.split()[3])) # here is the format assumption
                        self.nextline.lstrip()
                        #print self.nextline
                        eventtime = get_epics_datetime(self.nextline.lstrip())
                        #print eventtime
                        self.values[1].append( eventtime)
                        self.secondsIntoRun.append( (eventtime-self.values[1][0]).seconds )
                        #print str((eventtime-self.values[1][0]).seconds) + "seconds"
               # return the file position to its original location
                    else: 
                        #print line
                        if line.split()[1] == '-+' :
                            self.values[j].append(0)
                        else:
                            self.values[j].append( float(line.split()[1]))
        self.startdate = self.values[1][0].strftime('%Y-%m-%d')
        self.starttime = self.values[1][0].strftime('%H:%M:%S')
        self.enddate   = self.values[1][len(self.values[1])-1].strftime('%Y-%m-%d')
        self.endtime   = self.values[1][len(self.values[1])-1].strftime('%H:%M:%S')
        #print "Start Date and time are " + self.startdate + " " + self.starttime
        #print "End Date and time are " + self.enddate + " " + self.endtime

    def CalculateAverageValues(self):
        for j, quantity in enumerate(self.valueTitles):
            for avgName in self.averagedValueNames:
               if avgName == quantity:
                   self.valueAverages.append( sum(self.values[j])/float(len(self.values[j]) ) )
                   #print self.valueAverages[len(self.valueAverages)-1]

    def FillDatabase(self):
        columns = {
           "Run_number": self.runNumber,
           "start_date": "'"+self.startdate+"'" ,
           "start_time": "'"+self.starttime+"'",
           "start_datetime": "'"+self.startdate+" "+self.starttime+"'",
           "end_date": "'"+self.enddate+"'",
           "end_time": "'"+self.endtime+"'",
           "end_datetime": "'"+self.enddate+" "+self.endtime+"'",
           #"replay_datetime":"NOW()",
           #"replay_time":"current_time()",
           #"replay_date":"current_date()",
           #"replay_timestamp":"NOW()"}
           "replay_datetime":"datetime()",
           "replay_time":"time()",
           "replay_date":"date()",
           "replay_timestamp":"strftime('%s','now')"}           #"beam_energy": self.beamEnergy,
           #"beam_current": beamCurrentAvg,
           #"halfwave_plate": int(beamValues[8][0]),
           #"target_field": epicsdata[3][0] }
        sqlcmd="INSERT INTO run_info SET "
        for n, v in columns.iteritems():
           sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ON DUPLICATE KEY UPDATE "
        for n, v in columns.iteritems():
            sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ; "
        #print sqlcmd
        #self.SQLiteUpdate(columns)
        # MySQL :
        columns = {
           "Run_number": self.runNumber,
           "start_date": "'"+self.startdate+"'" ,
           "start_time": "'"+self.starttime+"'",
           "start_datetime": "'"+self.startdate+" "+self.starttime+"'",
           "end_date": "'"+self.enddate+"'",
           "end_time": "'"+self.endtime+"'",
           "end_datetime": "'"+self.enddate+" "+self.endtime+"'",
           "replay_datetime":"NOW()",
           "replay_time":"current_time()",
           "replay_date":"current_date()",
           "replay_timestamp":"NOW()"}
        sqlcmd="INSERT INTO run_info SET "
        for n, v in columns.iteritems():
           sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ON DUPLICATE KEY UPDATE "
        for n, v in columns.iteritems():
            sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ; "
        self.MySQLUpdate(columns)
        #self.cursor.execute (sqlcmd)

#_________________________________________________________________________#

class InSANEEpicsBeamExtractor(InSANEEpicsExtractor):
    """Extracts BEAM Epics information from Hall C analyzer epics output
       text file.

    The file is assumed to have the following structure:
    """

    def __init__(self,num):
        """The constructor."""
        InSANEEpicsExtractor.__init__(self,num)
        self.valueNames = ["MBSY3C_energy","EHCSR_XIPEAK","EHCSR_YIPEAK","ibcm1","ibcm2",
              "EHCFR_INITSPPAT","EHCFR_SPWidth","EHCFR_SPIpeakraw.VAL",
              "IGL1I00DI24_24M","IGL1I00OD16_16","IPM3H00A.XRAW","IPM3H00A.YRAW",
              "IPM3H00B.XRAW","IPM3H00B.YRAW","WienAngle"]
        self.averagedValueNames = ["Beam Energy","Slow Raster X Magnet Current (RMS)",
             "Slow Raster Y Magnet Current (RMS)","BCM 1","BCM 2",
             "BPM1x","BPM1y","BPM2x","BPM2y"]
        self.valueTitles = ["Beam Energy","Slow Raster X Magnet Current (RMS)","Slow Raster Y Magnet Current (RMS)","BCM 1","BCM 2",
              "Raster pattern (0=Lissajous, 1=Spiral?)","Fast Raster Spiral Radius","Fast Raster Magnet setting?",
              "1/2 Wave Plate retracted","1/2 Wave Plate Retracted Control","BPM1x","BPM1y","BPM2x","BPM2y"]

    def ExtractValues(self):
        """Documentation for a method."""
        for line in self.searchFile:
            self.nextline = self.searchFile2.readline()
            for j, quantity in enumerate(self.valueNames):
                if quantity in line:
                    #print line
                    if line.split()[1] == "-+" :
                        self.values[j].append( float(0))
                    else :
                        self.values[j].append( float(line.split()[1] ))

    def FillDatabase(self):
        self.beamEnergy = self.valueAverages[0]
        columns = {
           "Run_number": self.runNumber,
           "beam_energy": self.beamEnergy,
           "beam_current": (float(self.valueAverages[3]) + float(self.valueAverages[4]))/2.0 ,
           "halfwave_plate": int(self.values[8][0]),
           "wien_angle":    float(self.values[14][0]),
           "beam_pass":    4 }
        sqlcmd="INSERT INTO run_info SET "
        for n, v in columns.iteritems():
           sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ON DUPLICATE KEY UPDATE "
        for n, v in columns.iteritems():
            sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ; "
        #print sqlcmd
        #self.SQLiteUpdate(columns)
        self.MySQLUpdate(columns)
        #self.cursor.execute (sqlcmd)

#_________________________________________________________________________#

class InSANEEpicsHMSExtractor(InSANEEpicsExtractor):
    """Extracts HMS Epics information from Hall C analyzer epics output
       text file.

    The file is assumed to have the following structure:
    """

    def __init__(self,num):
        """The constructor."""
        InSANEEpicsExtractor.__init__(self,num)
        self.valueNames = ["ecHMS_Angle","ecP_HMS"]
        self.averagedValueNames = ["HMS Angle","HMS Momentum"]
        self.valueTitles = ["HMS Angle","HMS Momentum"]

    def ExtractValues(self):
        """Documentation for a method."""
        for line in self.searchFile:
            self.nextline = self.searchFile2.readline()
            for j, quantity in enumerate(self.valueNames):
                if quantity in line:
                    #print line
                    if line.split()[1] != "-+":
                        self.values[j].append( float(line.split()[1]))



    def FillDatabase(self):
        self.beamEnergy = self.valueAverages[0]
        columns = {
           "Run_number": self.runNumber,
           "hms_momentum": float(self.valueAverages[1]),
           "hms_angle": float(self.valueAverages[0])}
        sqlcmd="INSERT INTO run_info SET "
        for n, v in columns.iteritems():
           sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ON DUPLICATE KEY UPDATE "
        for n, v in columns.iteritems():
            sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ; "
        #print sqlcmd
        #self.SQLiteUpdate(columns)
        #self.cursor.execute (sqlcmd)
        self.MySQLUpdate(columns)

#_________________________________________________________________________#

class InSANEEpicsTargetExtractor(InSANEEpicsExtractor):
    """Extracts TARGET Epics information from Hall C analyzer epics output
       text file.

    The file is assumed to have the following structure:
    """

    def __init__(self,num):
        """The constructor."""
        InSANEEpicsExtractor.__init__(self,num)
        self.valueNames = ["hcptPolarization",
	       "hcptPT_Encoder",
	       "hcptuWave_Freq",
	       "hcptSpinSpecies",
	       "hcptPT_Position",
	       "hcptMagField",
	       "hcptMagnet_Current",
	       "hcptNMR_Area",
	       "hcptVPT_3He",
	       "hcptVPT_4He"]
        self.averagedValueNames = ["Polarization","Microwave Frequency",
                                   "Magnetic Field", "magnet current",
                                   "NMR Area", "Vapor Pressure 3He", "Vapor Pressure 4HE"]
        self.valueTitles = ["Polarization", "Encoder","Microwave Frequency",
                                   "spin species","Position","Magnetic Field", "magnet current",
                                   "NMR Area", "Vapor Pressure 3He", "Vapor Pressure 4HE"]

    def ExtractValues(self):
        """Documentation for a method."""
        for line in self.searchFile:
            self.nextline = self.searchFile2.readline()
            for j, quantity in enumerate(self.valueNames):
                if quantity in line:
                    #print line
                    if line.split()[1] == '-+' :
                        self.values[j].append(0)
                    else:
                        self.values[j].append( float(line.split()[1]))
                    #self.values[j].append( float(line.split()[1]))

    def FillDatabase(self):
        self.beamEnergy = self.valueAverages[0]
        columns = {
           "Run_number": self.runNumber,
           "target_start_pol": self.values[0][0],
           "target_stop_pol":  self.values[0][len(self.values[0])-1] ,
           "target_current":   self.valueAverages[3]  }
        sqlcmd="INSERT INTO run_info SET "
        for n, v in columns.iteritems():
           sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ON DUPLICATE KEY UPDATE "
        for n, v in columns.iteritems():
            sqlcmd = sqlcmd + n + "=" + str(v) +", "
        sqlcmd = sqlcmd.strip(", ")
        sqlcmd = sqlcmd + " ; "
        #self.SQLiteUpdate(columns)
        #print sqlcmd
        #self.cursor.execute (sqlcmd)
        self.MySQLUpdate(columns)

