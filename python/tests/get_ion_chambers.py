#!/usr/bin/python
#usage: get_epics_quantity [run_number]

import numpy as np
import pylab as plt
import matplotlib.mlab as mlab
import sys
import time
import datetime
import MySQLdb
import math

conn = MySQLdb.connect (host = "localhost",
                        user = "sane",
                        passwd = "secret",
                        db = "SANE")
cursor = conn.cursor ()

class EDT(datetime.tzinfo):
    """Concrete imp of tzinfo for a datetime timezone

    EDT
    """
    def utcoffset(self,dt):
        return timedelta(hours=4,minutes=0) 
    def tzname(self,dt):
        return "EDT"
    def dst(self,dt):
        return timedelta(0)

def get_epics_datetime(epicsDateTimeLine):
    """ Converts the EPICS date and time to a datetime

    """
    epicsDatetimeFMT = '%a %b %d %H:%M:%S %Z %Y '
    dt = datetime.datetime.now()
    if epicsDateTimeLine == "":
        print "null timestamp provided!"
    else :
        dt = dt.strptime(epicsDateTimeLine,epicsDatetimeFMT)
        return(dt )




# tab at end is there to remove finding random printing of variables
# since the value is separetd by a tab
quantities = [" epics event # ", "epicsdatetime", "hcptPT_Encoder","hcptPolarization","ibcm1","ibcm2","HC:Bigcalth1",
             "HC:Bigcalth2","HC:Bigcalth3","HC:Bigcalth4","HC:SCerth1","HC:SCerth2","HALLC:p","EHCSR_XIPEAK","EHCSR_YIPEAK",
             "MBSY3C_energy"]
epicsdata =  [ [] for i in range(len(quantities)) ]

ionChamberNames = ["SLD3H01GDOSC","SLD3H02GDOSC",
                   "SLD3H03GDOSC","SLD3H04GDOSC",
                   "SLD3H05GDOSC","SLD3H06GDOSC",
                   "SLD3H07GDOSC","SLD3H08GDOSC"]
ionChamberValues =  [ [] for i in range(len(ionChamberNames)) ]

secondsIntoRun = []

runnumber = sys.argv[1]
filename = "scalers/epics"+str(runnumber)+".txt"
results = []

searchfile = open(filename, "r")
searchfile2 = open(filename, "r")
nextline = searchfile2.readline() # keep it one line ahead of searchfile

for line in searchfile:  
    nextline = searchfile2.readline()
    for j, quantity in enumerate(quantities):
        if quantity in line:
            print line
            if j==0: 
                epicsdata[j].append( float(line.split()[3]))
                nextline.lstrip()
                print nextline
                eventtime = get_epics_datetime(nextline.lstrip())
                print eventtime
                epicsdata[1].append( eventtime)
                secondsIntoRun.append( (eventtime-epicsdata[1][0]).seconds )
		print str((eventtime-epicsdata[1][0]).seconds) + "seconds"
               # return the file position to its original location
            else: epicsdata[j].append( float(line.split()[1]))
    for j, quantity in enumerate(ionChamberNames):
        if quantity in line:
            print line
            ionChamberValues[j].append( float(line.split()[1]))


searchfile.close()
searchfile2.close()


box = dict(facecolor='yellow', pad=5, alpha=0.2)
fig = plt.figure()
fig.subplots_adjust(left=0.2, wspace=0.6)
labelx = -0.3  # axes coords

ax1 = fig.add_subplot(211)
ax1.set_title(' ion chambers ')
ax1.plot(secondsIntoRun,ionChamberValues[0],'ro',
         secondsIntoRun,ionChamberValues[1],'go',
         secondsIntoRun,ionChamberValues[2],'bo',
         secondsIntoRun,ionChamberValues[3],'mo',
         secondsIntoRun,ionChamberValues[4],'r+',
         secondsIntoRun,ionChamberValues[5],'g+',
         secondsIntoRun,ionChamberValues[6],'b+',
         secondsIntoRun,ionChamberValues[7],'m+')

ax2 = fig.add_subplot(212)
ax2.set_title(' beam current')
ax2.plot(secondsIntoRun,epicsdata[4],'ro',
         secondsIntoRun,epicsdata[5],'m+')

plt.show()

