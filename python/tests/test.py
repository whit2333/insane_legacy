#!/usr/bin/python
import os
import sys
from InSANEHallCExtractor import *

epics = InSANEEpicsExtractor(72995)
epics.InitializeQuantities()
epics.ExtractValues()
epics.CalculateAverageValues()
epics.FillDatabase()
del epics

epicsbeam = InSANEEpicsTargetExtractor(72995)
epicsbeam.InitializeQuantities()
epicsbeam.ExtractValues()
epicsbeam.CalculateAverageValues()
epicsbeam.FillDatabase()
del epicsbeam
