#!/usr/bin/python
import sys
import os
from subprocess import call
    #*  0: No run / Junk run
    #* 1: Good Ammonia Run
    #* 2: Ugly Ammonia Run
    #* 4: Good Carbon Run with Helium
    #* 5: Good Carbon Run without Helium
    #* 6: Ugly Carbon Run 
pydir = "../python/"
runlistfile = open("run_list.txt", "r")
for line in runlistfile:
    res = line.split()
    if int(res[1]) == 1:
        print "Run " + res[0] + " is Good Ammonia"
        os.system(pydir + "get_hallc_epics.py "+res[0] )
        os.system(pydir + "get_target_epics.py "+res[0] )
    elif int(res[1]) == 4:
        print "Run " + res[0] + " is Good Carbon with Helium"
        os.system(pydir + "get_hallc_epics.py "+res[0] )
        os.system(pydir + "get_target_epics.py "+res[0] )
    elif int(res[1]) == 5:
        print "Run " + res[0] + " is Good Carbon without Helium"
        os.system(pydir + "get_hallc_epics.py "+res[0] )
        os.system(pydir + "get_target_epics.py "+res[0] )
    else 
        print "Run " + res[0] + " is other"
        os.system(pydir + "get_hallc_epics.py "+res[0] )
        os.system(pydir + "get_target_epics.py "+res[0] )
