#!/usr/bin/python

print "Hello, Python!";
import sys
print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)
file_name = sys.argv[1]
out_filename = "out.txt"
try:
  # open file stream
  file = open(out_filename, "w")
except IOError:
  print "There was an error writing to",out_filename 
  sys.exit()

file_finish = "finished"
print "Enter '", file_finish,
print "' When finished"
file_text = raw_input("Enter text: ")
while file_text != file_finish:
  file_text = raw_input("Enter text: ")
  if file_text == file_finish:
    # close the file
    file.close
    break
  file.write(file_text)
  file.write("\n")
file.close()
file_name = raw_input("Enter filename: ")
if len(file_name) == 0:
  print "Next time please enter something"
  sys.exit()
try:
  file = open(file_name, "r")
except IOError:
  print "There was an error reading file"
  sys.exit()
file_text = file.read()
file.close()
print file_text
raw_input("\n\nPress the enter key to exit.")
