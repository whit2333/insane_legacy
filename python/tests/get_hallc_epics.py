#!/usr/bin/python
#usage: get_hallc_epics [run_number]

import numpy as np
import pylab as plt
import matplotlib.mlab as mlab
import sys
import time
import datetime
import MySQLdb
import math
import hallctools as hc

conn = MySQLdb.connect (host = "localhost",
                        user = "sane",
                        passwd = "secret",
                        db = "SANE")
cursor = conn.cursor()

# tab at end is there to remove finding random printing of variables
# since the value is separetd by a tab
epics = [" epics event # ", 
              "epicsdatetime", 
	      "HALLC:p",
	      "MBSY3C_energy"]
epicsValues =  [ [] for i in range(len(epics)) ]

thresholds = ["HC:Bigcalth1",
	      "HC:Bigcalth2",
	      "HC:Bigcalth3",
	      "HC:Bigcalth4",
	      "HC:SCerth1",
	      "HC:SCerth2"]
thresholdValues =  [ [] for i in range(len(thresholds)) ]

hmsNames = ["ecHMS_Angle",
              "ecP_HMS"]
hmsValues =  [ [] for i in range(len(hmsNames)) ]

beamNames =  ["MBSY3C_energy",
	      "ibcm1",
	      "ibcm2",
              "IGL1I00OD16_16"]
beamTitles = ["beam_energy",
              "bcm1",
              "bcm2",
              "halfwave_plate"]
beamValues =  [ [] for i in range(len(beamNames)) ]

targetNames = ["hcptPolarization",
	       "hcptPT_Encoder",
	       "hcptuWave_Freq",
	       "hcptSpinSpecies",
	       "hcptPT_Position",
	       "hcptMagField",
	       "hcptMagnet_Current",
	       "hcptNMR_Area",
	       "hcptVPT_3He",
	       "hcptVPT_4He"]
targetValues =  [ [] for i in range(len(targetNames)) ]

bpmNames = ["IPM3C01","IPM3C01.XPOS","IPM3C01.YPOS",
             "IPM3C02","IPM3C02.XPOS","IPM3C02.YPOS",
             "IPM3C03","IPM3C03.XPOS","IPM3C03.YPOS",
             "IPM3C04","IPM3C04.XPOS","IPM3C04.YPOS",
             "IPM3C05","IPM3C05.XPOS","IPM3C05.YPOS",
             "IPM3C06","IPM3C06.XPOS","IPM3C06.YPOS",
             "IPM3C07","IPM3C07.XPOS","IPM3C07.YPOS",
             "IPM3C08","IPM3C08.XPOS","IPM3C08.YPOS",
             "IPM3C09","IPM3C09.XPOS","IPM3C09.YPOS",
             "IPM3C10","IPM3C10.XPOS","IPM3C10.YPOS",
             "IPM3C11","IPM3C11.XPOS","IPM3C11.YPOS",
             "IPM3C12","IPM3C12.XPOS","IPM3C12.YPOS",
             "IPM3C13","IPM3C13.XPOS","IPM3C13.YPOS",
             "IPM3C14","IPM3C14.XPOS","IPM3C14.YPOS",
             "IPM3C15","IPM3C15.XPOS","IPM3C15.YPOS",
             "IPM3C16","IPM3C16.XPOS","IPM3C16.YPOS",
             "IPM3C17","IPM3C17.XPOS","IPM3C17.YPOS",
             "IPM3C18","IPM3C18.XPOS","IPM3C18.YPOS"
             "IPM3C19","IPM3C19.XPOS","IPM3C19.YPOS",
             "IPM3C20","IPM3C20.XPOS","IPM3C20.YPOS",
             "IPM3C03","IPM3C03.XPOS","IPM3C03.YPOS",
             "IPM3C04","IPM3C04.XPOS","IPM3C04.YPOS",
             "IPM3C05","IPM3C05.XPOS","IPM3C05.YPOS",
             "IPM3C06","IPM3C06.XPOS","IPM3C06.YPOS"]
bpmValues =  [ [] for i in range(len(bpmNames)) ]

secondsIntoRun = []

runnumber = sys.argv[1]
filename = "scalers/epics"+str(runnumber)+".txt"
results = []

searchfile = open(filename, "r")
searchfile2 = open(filename, "r")
nextline = searchfile2.readline() # keep it one line ahead of searchfile

for line in searchfile:  
    nextline = searchfile2.readline()
    for j, quantity in enumerate(epics):
        if quantity in line:
#            print line
            if j==0: 
                epicsValues[j].append( float(line.split()[3]))
                nextline.lstrip()
#                print nextline
                eventtime = hc.get_epics_datetime(nextline.lstrip())
#                print eventtime
                epicsValues[1].append( eventtime)
                secondsIntoRun.append( (eventtime-epicsValues[1][0]).seconds )
#		print str((eventtime-epicsValues[1][0]).seconds) + "seconds"
               # return the file position to its original location
            else: epicsValues[j].append( float(line.split()[1]))

    for j, quantity in enumerate(thresholds):
        if quantity in line:
            thresholdValues[j].append( float(line.split()[1]))

    for j, quantity in enumerate(beamNames):
        if quantity in line:
            beamValues[j].append( float(line.split()[1]))

    for j, quantity in enumerate(targetNames):
        if quantity in line:
            targetValues[j].append( float(line.split()[1]))

    for j, quantity in enumerate(hmsNames):
        if quantity in line:
            hmsValues[j].append( float(line.split()[1]))

searchfile.close()
searchfile2.close()

startdate = epicsValues[1][0].strftime('%Y-%m-%d')
starttime = epicsValues[1][0].strftime('%H:%M:%S')
enddate   = epicsValues[1][len(epicsValues[1])-1].strftime('%Y-%m-%d')
endtime   = epicsValues[1][len(epicsValues[1])-1].strftime('%H:%M:%S')

beamCurrentAvg = (sum(beamValues[1]) + sum(beamValues[2]))/(float(len(beamValues[1])) + float(len(beamValues[2])))
beamEnergy = beamValues[0][0]
beamEnergyAvg = sum(beamValues[0])/(float(len(beamValues[0])))

targetPolarizationAvg = sum(targetValues[0])/(float(len(targetValues[0])))

targetAngle=80
if runnumber > 72900 : targetAngle=180


print "Start Date and time are " + startdate + " " + starttime
print "End Date and time are " + enddate + " " + endtime
print " "
print "Average beam current is " + str(beamCurrentAvg)
print "Beam Energy is " + str(beamEnergy)
print "Average Beam Energy is " + str(beamEnergyAvg)
print "Average target polarization is " + str(targetPolarizationAvg)


columns = {"Run_number": runnumber,
           "start_date": "'"+startdate+"'" ,
	   "start_time": "'"+starttime+"'",
	   "end_date": "'"+enddate+"'",
	   "end_time": "'"+endtime+"'",
	   "beam_energy":beamEnergy,
	   "beam_current": beamCurrentAvg,
	   "halfwave_plate": int(beamValues[3][0]),
	   "target_field": targetValues[5][0],
           "target_angle": targetAngle,
	   "target_current": targetValues[6][0],
	   "target_start_pol": targetValues[0][0],
           "target_stop_pol": targetValues[0][len(targetValues[0])-1],
           "hms_angle": hmsValues[0][0],
           "hms_momentum": hmsValues[1][0]}


sqlcmd="INSERT INTO run_info SET "
for n, v in columns.iteritems():
    sqlcmd = sqlcmd + n + "=" + str(v) +", "
sqlcmd = sqlcmd.strip(", ")
sqlcmd = sqlcmd + " ON DUPLICATE KEY UPDATE "
for n, v in columns.iteritems():
    sqlcmd = sqlcmd + n + "=" + str(v) +", "
sqlcmd = sqlcmd.strip(", ")
sqlcmd = sqlcmd + " ; "
print sqlcmd

cursor.execute (sqlcmd)
#row = cursor.fetchone ()
#print "server version:", row[0]
cursor.close ()
conn.close ()

box = dict(facecolor='yellow', pad=5, alpha=0.2)
fig = plt.figure()
fig.subplots_adjust(left=0.2, wspace=0.6)
labelx = -0.3  # axes coords

#ax1 = fig.add_subplot(221)
#ax1.set_title('Bigcal Thresholds')
#ax1.plot(secondsIntoRun,epicsdata[6],'bo',
         #secondsIntoRun,epicsdata[7],'ro',
	 #secondsIntoRun,epicsdata[8],'go',
	 #secondsIntoRun,epicsdata[9],'mo')
#ax1 = fig.add_subplot(221)
#ax1.set_title('Raster magnet current')
#ax1.plot(epicsdata[0],epicsdata[13],'go')

#ax3 = fig.add_subplot(223)
#ax3.set_title('Raster magnet current')
#ax3.plot(epicsdata[0],epicsdata[14],'mo')

#ax2 = fig.add_subplot(222)
#ax2.set_title('bcm vs event#')
#ax2.plot(epicsdata[0],epicsdata[4],'bo',epicsdata[0],epicsdata[5],'ro')
#ax2.set_ylabel('bcm', bbox=box)
##ax2.yaxis.set_label_coords(labelx, 0.5)
##ax2.set_ylim(0, 2000)

#ax4 = fig.add_subplot(224)
#ax4.plot(epicsdata[0],secondsIntoRun,'bo')
#ax4.set_ylabel('seconds into run', bbox=box)
##ax4.yaxis.set_label_coords(labelx, 0.5)


#box = dict(facecolor='yellow', pad=5, alpha=0.2)
#fig2 = plt.figure()
#fig2.subplots_adjust(left=0.2, wspace=0.6)

#ax1 = fig2.add_subplot(221)
#ax1.set_title('BPM')
#ax1.plot(bpmValues[1],bpmValues[2],'ro',
         #bpmValues[4],bpmValues[5],'go',
         #bpmValues[7],bpmValues[8],'bo',
         #bpmValues[10],bpmValues[11],'mo')
         ##bpmValues[13],bpmValues[14],'y+',
         ##bpmValues[16],bpmValues[17],'g+',
         ##bpmValues[19],bpmValues[20],'b+',
         ##bpmValues[22],bpmValues[23],'r+',
         ##bpmValues[25],bpmValues[26],'c+',
         ##bpmValues[28],bpmValues[29],'y+',
         ##bpmValues[31],bpmValues[32],'g+',
         ##bpmValues[34],bpmValues[35],'b+',
         ##bpmValues[37],bpmValues[38],'r+',
         ##bpmValues[40],bpmValues[41],'c+',
         ##bpmValues[43],bpmValues[44],'y+')
#ax6 = fig2.add_subplot(222)
#ax6.set_title('BPM')
#ax6.plot(bpmValues[0],'go',
         #bpmValues[3],'bo',
         #bpmValues[6],'ro',
         #bpmValues[9],'co',
         #bpmValues[12],'y+',
         #bpmValues[15],'g+',
         #bpmValues[18],'b+',
         #bpmValues[21],'r+',
         #bpmValues[24],'c+',
         #bpmValues[27],'y+',
         #bpmValues[30],'g+',
	 #bpmValues[33],'b+',
	 #bpmValues[36],'r+',
	 #bpmValues[39],'c+',
	 #bpmValues[42],'y+')
#plt.show()

