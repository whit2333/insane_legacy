#!/usr/bin/python

import numpy as np
import pylab as plt
import matplotlib.mlab as mlab
import sys
import time
import datetime
import MySQLdb
import math

conn = MySQLdb.connect (host = "localhost",
                        user = "sane",
                        passwd = "secret",
                        db = "SANE")
cursor = conn.cursor ()

class EDT(datetime.tzinfo):
    """Concrete imp of tzinfo for a datetime timezone

    EDT
    """
    def utcoffset(self,dt):
        return timedelta(hours=4,minutes=0) 
    def tzname(self,dt):
        return "EDT"
    def dst(self,dt):
        return timedelta(0)

def get_epics_datetime(epicsDateTimeLine):
    """ Converts the EPICS date and time to a datetime

    """
    epicsDatetimeFMT = '%a %b %d %H:%M:%S %Z %Y '
    dt = datetime.datetime.now()
    if epicsDateTimeLine == "":
        print "null timestamp provided!"
    else :
        dt = dt.strptime(epicsDateTimeLine,epicsDatetimeFMT)
        return(dt )




# tab at end is there to remove finding random printing of variables
# since the value is separetd by a tab
quantities = [" epics event # ", "epicsdatetime", "hcptPT_Encoder","hcptPolarization","ibcm1","ibcm2","MBSY3C_energy"]
epicsdata =  [ [] for i in range(len(quantities)) ]

beamNames = ["IPM3H00A.XALP","IPM3H00A.YALP",
             "IPM3H00B.XALP","IPM3H00B.YALP",
             "IPM3H00A.XRAW","IPM3H00A.YRAW",
             "IPM3H00B.XRAW","IPM3H00B.YRAW"]
beamValues =  [ [] for i in range(len(beamNames)) ]

secondsIntoRun = []

crossHairRuns = [72544,72563,72613,72653,72655,72656] #,72912
someruns = [72544,72563,72609,72610,72613,72620,72653,72655,72656] #,72912

beamSettings = ["IPM3H00A.XRAW","IPM3H00A.YRAW",
             "IPM3H00B.XRAW","IPM3H00B.YRAW",
             "hcptPT_Encoder","hcptPT_Position"]
beamSettingValues =  [ [] for i in range(len(beamSettings)) ]


runs = someruns

for runi,runnumber in enumerate(runs):
    beamValues =  [ [] for i in range(len(beamSettings)) ]
    epicsdata =  [ [] for i in range(len(quantities)) ]

    filename = "scalers/epics"+str(runnumber)+".txt"
    results = []

    searchfile = open(filename, "r")
    searchfile2 = open(filename, "r")
    nextline = searchfile2.readline() # keep it one line ahead of searchfile

    for line in searchfile: 
        nextline = searchfile2.readline()
        for j, quantity in enumerate(quantities):
            if quantity in line:
                print line
                if j==0: 
                    epicsdata[j].append( float(line.split()[3]))
                    nextline.lstrip()
                    print nextline
                    eventtime = get_epics_datetime(nextline.lstrip())
                    print eventtime
                    epicsdata[1].append( eventtime)
                    secondsIntoRun.append( (eventtime-epicsdata[1][0]).seconds )
    		    print str((eventtime-epicsdata[1][0]).seconds) + "seconds"
                   # return the file position to its original location
                else: epicsdata[j].append( float(line.split()[1]))
        for j, quantity in enumerate(beamSettings):
            if quantity in line:
                print line
                beamValues[j].append( float(line.split()[1]))
    print filename
    for j, beamset in enumerate(beamSettings):
        beamSettingValues[j].append(beamValues[j][0])
    searchfile.close()
    searchfile2.close()

print beamSettingValues


#fig = plt.figure()

#fig.subplots_adjust( left=0.1,wspace=0.20)

#ax1 = fig.add_subplot(321)
##ax1.set_title(' pumping microwave frequency ')
#p1 = ax1.plot(runs,beamSettingValues[0],'ro')
#ax1.set_ylabel(beamNames[0])
#ax1.set_xlabel("run number")

#plt.title('Beamline EPICS')

##grid = plt.AxesGrid(fig, 221, # similar to subplot(131)
                    ##nrows_ncols = (2, 2),
                    ##axes_pad = 0.05,
                    ##label_mode = "1"
                    ###)

#ax2 = fig.add_subplot(322)
##ax2.set_title(' target position encoder ')
#p2 = ax2.plot(runs,beamSettingValues[1],'ro')
#ax2.set_ylabel(beamSettings[1])
#ax2.set_xlabel("run number")

#ax3 = fig.add_subplot(323)
##ax3.set_title(' target position integer ')
#p3 = ax3.plot(runs,beamSettingValues[2],'ro')
#ax3.set_ylabel(beamSettings[2])
#ax3.set_xlabel("run number")

#ax4 = fig.add_subplot(324)
##ax3.set_title(' target position integer ')
#p4 = ax4.plot(runs,beamSettingValues[3],'ro')
#ax4.set_ylabel(beamSettings[3])
#ax4.set_xlabel("run number")

#ax5 = fig.add_subplot(325)
##ax3.set_title(' target position integer ')
#p5 = ax5.plot(runs,beamSettingValues[4],'go',
              #runs,beamSettingValues[5],'r+')
#ax5.set_ylabel(beamSettings[4])
#ax5.set_xlabel("run number")

#plt.show()

