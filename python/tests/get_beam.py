#!/usr/bin/python
#usage: get_epics_quantity [run_number]

import numpy as np
import pylab as plt
import matplotlib.mlab as mlab
import sys
import time
import datetime
import MySQLdb
import math

conn = MySQLdb.connect (host = "localhost",
                        user = "sane",
                        passwd = "secret",
                        db = "SANE")
cursor = conn.cursor ()

class EDT(datetime.tzinfo):
    """Concrete imp of tzinfo for a datetime timezone

    EDT
    """
    def utcoffset(self,dt):
        return timedelta(hours=4,minutes=0) 
    def tzname(self,dt):
        return "EDT"
    def dst(self,dt):
        return timedelta(0)

def get_epics_datetime(epicsDateTimeLine):
    """ Converts the EPICS date and time to a datetime

    """
    epicsDatetimeFMT = '%a %b %d %H:%M:%S %Z %Y '
    dt = datetime.datetime.now()
    if epicsDateTimeLine == "":
        print "null timestamp provided!"
    else :
        dt = dt.strptime(epicsDateTimeLine,epicsDatetimeFMT)
        return(dt )




# tab at end is there to remove finding random printing of variables
# since the value is separetd by a tab
quantities = [" epics event # ", "epicsdatetime", "hcptPT_Encoder","hcptPolarization","ibcm1","ibcm2","MBSY3C_energy"]
epicsdata =  [ [] for i in range(len(quantities)) ]

beamNames = ["IPM3H00A.XRAW","IPM3H00A.YRAW",
             "IPM3H00B.XRAW","IPM3H00B.YRAW",
             "IPM3H00A.XRAW","IPM3H00A.YRAW",
             "IPM3H00B.XRAW","IPM3H00B.YRAW"]
beamValues =  [ [] for i in range(len(beamNames)) ]

secondsIntoRun = []

runnumber = sys.argv[1]
filename = "scalers/epics"+str(runnumber)+".txt"
results = []

searchfile = open(filename, "r")
searchfile2 = open(filename, "r")
nextline = searchfile2.readline() # keep it one line ahead of searchfile

for line in searchfile:  
    nextline = searchfile2.readline()
    for j, quantity in enumerate(quantities):
        if quantity in line:
            print line
            if j==0: 
                epicsdata[j].append( float(line.split()[3]))
                nextline.lstrip()
                print nextline
                eventtime = get_epics_datetime(nextline.lstrip())
                print eventtime
                epicsdata[1].append( eventtime)
                secondsIntoRun.append( (eventtime-epicsdata[1][0]).seconds )
		print str((eventtime-epicsdata[1][0]).seconds) + "seconds"
               # return the file position to its original location
            else: epicsdata[j].append( float(line.split()[1]))
    for j, quantity in enumerate(beamNames):
        if quantity in line:
            print line
            beamValues[j].append( float(line.split()[1]))


searchfile.close()
searchfile2.close()


fig = plt.figure()

fig.subplots_adjust( wspace=1.0)
labelx = -0.3  # axes coords

ax1 = fig.add_subplot(221)
#ax1.set_title(' pumping microwave frequency ')
p1 = ax1.plot(beamValues[0],beamValues[1],'ro')
ax1.set_ylabel(beamNames[0])
ax1.set_xlabel(beamNames[1])

plt.title('Beamline EPICS')

#grid = plt.AxesGrid(fig, 221, # similar to subplot(131)
                    #nrows_ncols = (2, 2),
                    #axes_pad = 0.05,
                    #label_mode = "1"
                    #)

ax2 = fig.add_subplot(222)
#ax2.set_title(' target position encoder ')
p2 = ax2.plot(beamValues[2],beamValues[3],'bo')
ax2.set_ylabel(beamNames[2])
ax2.set_xlabel(beamNames[3])

ax3 = fig.add_subplot(223)
#ax3.set_title(' target position integer ')
p3 = ax3.plot(beamValues[4],beamValues[5],'go')
ax3.set_ylabel(beamNames[4])
ax3.set_xlabel(beamNames[5])

ax4 = fig.add_subplot(224)
#ax3.set_title(' target position integer ')
p4 = ax4.plot(beamValues[6],beamValues[7],'mo')
ax4.set_ylabel(beamNames[6])
ax4.set_xlabel(beamNames[7])

plt.show()

