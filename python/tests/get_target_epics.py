#!/usr/bin/python
#usage: get_epics_quantity [run_number]
import numpy as np
import pylab as plt
import matplotlib.mlab as mlab
import sys
import time
import datetime
import MySQLdb
import math
import hallctools as hc

conn = MySQLdb.connect (host = "localhost",
                        user = "sane",
                        passwd = "secret",
                        db = "SANE")
cursor = conn.cursor ()


# tab at end is there to remove finding random printing of variables
# since the value is separetd by a tab
quantities = [" epics event # ", 
              "epicsdatetime", 
	      "hcptPT_Encoder",
	      "hcptPolarization",
	      "ibcm1",
	      "ibcm2","HC:Bigcalth1",
	      "HC:Bigcalth2","HC:Bigcalth3",
	      "HC:Bigcalth4",
	      "HC:SCerth1",
	      "HC:SCerth2","HALLC:p","EHCSR_XIPEAK","EHCSR_YIPEAK",
             "MBSY3C_energy","hcptSpinSpecies"]
epicsdata =  [ [] for i in range(len(quantities)) ]

targetNames = ["hcptuWave_Freq","hcptPT_Encoder","hcptPT_Position"]
targetValues =  [ [] for i in range(len(targetNames)) ]

secondsIntoRun = []

runnumber = sys.argv[1]
filename = "scalers/epics"+str(runnumber)+".txt"
results = []

searchfile = open(filename, "r")
searchfile2 = open(filename, "r")
nextline = searchfile2.readline() # keep it one line ahead of searchfile

for line in searchfile:  
    nextline = searchfile2.readline()
    for j, quantity in enumerate(quantities):
        if quantity in line:
            print line
            if j==0: 
                epicsdata[j].append( float(line.split()[3]))
                nextline.lstrip()
                print nextline
                eventtime = hc.get_epics_datetime(nextline.lstrip())
                print eventtime
                epicsdata[1].append( eventtime)
                secondsIntoRun.append( (eventtime-epicsdata[1][0]).seconds )
		print str((eventtime-epicsdata[1][0]).seconds) + "seconds"
               # return the file position to its original location
            else: epicsdata[j].append( float(line.split()[1]))
    for j, quantity in enumerate(targetNames):
        if quantity in line:
            print line
            targetValues[j].append( float(line.split()[1]))


searchfile.close()
searchfile2.close()

columns = {"Run_number": runnumber,
           "target_start_pol": epicsdata[3][0],
           "target_stop_pol": epicsdata[3][len(epicsdata[3])-1]}

sqlcmd="INSERT INTO run_info SET "

for n, v in columns.iteritems():
    sqlcmd = sqlcmd + n + "=" + str(v) +", "
sqlcmd = sqlcmd.strip(", ")
sqlcmd = sqlcmd + " ON DUPLICATE KEY UPDATE "
for n, v in columns.iteritems():
    sqlcmd = sqlcmd + n + "=" + str(v) +", "
sqlcmd = sqlcmd.strip(", ")
sqlcmd = sqlcmd + " ; "
print sqlcmd

cursor.execute (sqlcmd)
#row = cursor.fetchone ()
#print "server version:", row[0]
cursor.close ()
conn.close ()


#fig = plt.figure()

#fig.subplots_adjust( wspace=1.0)
#labelx = -0.3  # axes coords

#ax1 = fig.add_subplot(311)
##ax1.set_title(' pumping microwave frequency ')
#p1 = ax1.plot(secondsIntoRun,targetValues[0],'rp')
#ax1.set_ylabel('pumping microwave frequency')
#ax1.set_xlabel('time seconds')

#plt.title('Target EPICS')


#ax2 = fig.add_subplot(312)
##ax2.set_title(' target position encoder ')
#p2 = ax2.plot(secondsIntoRun,targetValues[1],'mo')
#ax2.set_ylabel(' target position encoder')
#ax2.set_xlabel('time seconds')

#ax3 = fig.add_subplot(313)
##ax3.set_title(' target position integer ')
#p3 = ax3.plot(secondsIntoRun,targetValues[2],'go')
#ax3.set_ylabel('target position integer')
#ax3.set_xlabel('time seconds')

##ax2 = fig.add_subplot(122)
##ax2.set_title(' beam current')
##ax2.plot(secondsIntoRun,epicsdata[4],'ro',
         ##secondsIntoRun,epicsdata[5],'m+')

#plt.show()

