InSANE Cross Section IDs
========================

Basic Cross Section
-------------------

All cross sections have base class `InSANEDiffXSec`.

### Numbering convention

`XXXcYYpNN`

| index  | meaning             |
| ------ | ------------------- |
| `XXX`  | Type (eg inclusive) |
| `c`    | composite type      |
| `YY`   | variable meaning    |
| `p`    | particle 0=deafult  |
| `NN`   | arbitrary number    |


### Inclusive

| class                                  | ID        | comment
| -------------------------------------- | --------- | -------------------
| `InSANEInclusiveDiffXSec`              | 100000000 |
| `InSANEFlatInclusiveDiffXSec`          | 100000001 |
| `InSANEInclusiveMottXSec`              | 100000002 |
| `InSANEInclusiveBornDISXSec`           | 100000010 |
| `CTEQ6eInclusiveDiffXSec`              | 100000011 |
| `F1F209eInclusiveDiffXSec`             | 100000012 |
| -------------------------------------- | --------- | -------------------
| `InSANECompositeDiffXSec`              | 100100000 |
| `InSANEInclusiveDISXSec`               | 100100001 |
| -------------------------------------- | --------- | -------------------
| `InSANEPhotonDiffXSec`                 | 100001001 | Mono-energetic photon XS
| `InSANEInclusiveWiserXSec`             | 100001002 | Mono-energetic Wiser fit
| -------------------------------------- | --------- | -------------------
| `InSANEInclusivePhotoProductionXSec`   | 100002001 |
| `InclusivePhotoProductionXSec`         | 100102001 |
| -------------------------------------- | --------- | -------------------
| `InSANEInclusiveElectroProductionXSec` | 100003001 |
| `InclusiveElectroProductionXSec`       | 100103001 |
| -------------------------------------- | --------- | -------------------
| `WiserInclusivePhotoXSec`              | 100002002 |
| `WiserInclusivePhotoXSec2`             | 100002003 |
| `PhotoWiserDiffXSec`                   | 100102002 |
| `PhotoWiserDiffXSec2`                  | 100102003 |
| -------------------------------------- | --------- | -------------------
| `WiserInclusiveElectroXSec`            | 100003002 |
| `ElectroWiserDiffXSec`                 | 100103002 |
| -------------------------------------- | --------- | -------------------
| `OARPionDiffXSec`                      | 100001012 | Oscar's Mono-energetic fit
| `OARInclusivePhotoXSec`                | 100002012 |
| `PhotoOARPionDiffXSec`                 | 100102012 |
| `OARPionElectroDiffXSec`               | 100003012 |
| `ElectroOARPionDiffXSec`               | 100103012 |
| -------------------------------------- | --------- | -------------------
| `InSANEInclusiveEPCVXSec`              | 100004001 |
| `InSANEInclusiveEPCVXSec2`             | 100004002 |
| -------------------------------------- | --------- | -------------------
| `InSANEeInclusiveElasticDiffXSec`      | 100004011 |
| `InSANEpInclusiveElasticDiffXSec`      | 100004012 |
| `RCeInclusiveElasticDiffXSec`          | 100014012 |
| -------------------------------------- | --------- | -------------------
| `InSANEInclusiveMollerDiffXSec`        | 100004021 |
| -------------------------------------- | --------- | -------------------
| `MAIDInclusiveDiffXSec`                | 100104031 |
| `MAIDInclusivePionDiffXSec`            | 100104032 |
| `MAIDInclusiveElectronDiffXSec`        | 100104033 |
| `MAIDNucleusInclusiveDiffXSec`         | 100104034 |
| `MAIDPolarizedTargetDiffXSec`          | 100104035 |
| -------------------------------------- | --------- | -------------------
| `InSANEPOLRADBornDiffXSec`              | 100010001 |
| `InSANEPOLRADRadiatedDiffXSec`          | 100010002 |
| `InSANEPOLRADElasticTailDiffXSec`       | 100010002 |
| `InSANEPOLRADInelasticTailDiffXSec`     | 100010003 |
| `InSANEPOLRADInternalPolarizedDiffXSec` | 100010004 |
| `InSANEPOLRADQuasiElasticTailDiffXSec`  | 100010005 |
| --------------------------------------- | --------- | -------------------
| `InSANERadiativeTail`                   | 100010011 |
| `InSANEInelasticRadiativeTail`          | 100010012 |
| `InSANEInelasticRadiativeTail2`         | 100010013 |
| `InSANEElasticRadiativeTail`            | 100010014 |
| --------------------------------------- | --------- | -------------------
| `InSANERADCORInternalUnpolarizedDiffXSec` | 100010021 |
| `InSANERADCORRadiatedDiffXSec`            | 100010022 |
| `InSANERADCORRadiatedUnpolarizedDiffXSec` | 100010023 |
| --------------------------------------- | --------- | -------------------
| `InSANEPolarizedCrossSectionDifference` | 100010031 |
| --------------------------------------- | --------- | -------------------
| `InclusiveHadronProductionXSec<T>`      | T+10000000 |
| `InSANERadiator<T>`                     | T+20000000 |




InSANEPOLRADBornDiffXSec
InSANEPOLRADUltraRelInelasticTailDiffXSec
InSANEPOLRADElasticTailDiffXSec
InSANEPOLRADInelasticTailDiffXSec
InSANEPOLRADInternalPolarizedDiffXSec
InSANEPOLRADElasticDiffXSec
InSANEPOLRADQuasiElasticTailDiffXSec


class InSANERadiatorBase<InSANEInclusiveDiffXSec>+;
class InSANERadiatorBase<F1F209eInclusiveDiffXSec>+;
class InSANERadiator<InSANEInclusiveDiffXSec>+;
class InSANERadiator<InSANEInclusiveBornDISXSec>+;
class InSANERadiator<InSANECompositeDiffXSec>+;
class InSANERadiator<F1F209eInclusiveDiffXSec>+;
class InSANERadiator<F1F209QuasiElasticDiffXSec>+;
class InSANERadiator<CTEQ6eInclusiveDiffXSec>+;
class InSANERadiator<InSANEPOLRADBornDiffXSec>+;
class InSANERadiator<InSANEPOLRADInelasticTailDiffXSec>+;
class InSANERadiator<F1F209QuasiElasticDiffXSec>+;
class InSANERadiator<QFSInclusiveDiffXSec>+;
class InSANEExternalRadiator<InSANEInclusiveMollerDiffXSec>+;


### Exclusive

#### Numbering convention

`XXXcYYpNN`

| index  | meaning             |
| ------ | ------------------- |
| `XXX`  | Type (eg inclusive) |
| `c`    | composite type      |
| `YY`   | variable meaning    |
| `p`    | particle 0=deafult  |
| `NN`   | arbitrary number    |

| class                                  | ID        | comment
| -------------------------------------- | --------- | -------------------
| `InSANEExclusiveDiffXSec`              | 200000000 |
| `InSANEFlatExclusiveDiffXSec`          | 200000001 |
| `InSANEExclusiveMottXSec`              | 200000002 |
| `InSANEepElasticDiffXSec`              | 200000010 |
| -------------------------------------- | --------- | -------------------
| `InSANEMollerDiffXSec`                 | 200001001 |
| -------------------------------------- | --------- | -------------------
| `DVCSDiffXSec`                         | 201000000 |
| `IncoherentDVCSXSec`                   | 201100000 |

MAIDExclusivePionDiffXSec                fID = 1020;
InSANEPOLRADElasticTailDiffXSec

src/physics/include/DVCSDiffXSec.h
MAIDExclusivePionDiffXSec2.h
MAIDExclusivePionDiffXSec3.h
MAIDExclusivePionDiffXSec.h

### Semi-Inclusive

### Semi-Exclusive

PolarizedDISXSec.h
class InSANEPolarizedDiffXSec : public InSANEInclusiveDiffXSec {
class PolarizedDISXSec : public InSANEInclusiveDiffXSec {
class PolarizedDISXSecParallelHelicity : public InSANEInclusiveDiffXSec {
class PolarizedDISXSecAntiParallelHelicity : public InSANEInclusiveDiffXSec {

QFSInclusiveDiffXSec.h
class QFSInclusiveDiffXSec : public InSANEInclusiveDiffXSec {

