/*! \mainpage InSANE

   A collection C++ libraries for physics analysis of the SANE experiment.
   Created and maintained  Whitney Armstrong.

   \section contents Table of Contents

   This manual is divided in the following sections:

 - \subpage intro
   - \ref about
   - \ref download
   - \subpage install
   - \subpage firststeps

 - \subpage extendingInSANE 
   - \subpage newClass
   - \subpage fortran2C
   - \subpage linkingwithpython

 - InSANE Structure
   - \subpage insanebasics 
   - \subpage detectorImp 
     - \ref Apparatus
       - \ref Detectors
         - \ref Cherenkov
         - \ref hodoscope
         - \ref BIGCAL
         - \ref tracker
       - \ref BETA
       - \ref beam
   - \subpage scalers
     - \ref scalerdata
     - \ref scaleranalysis
     - \ref scalerFilters
   - \subpage Runinfo
   - \subpage treesandbranches
   - Manager Classes

 - \subpage simulation

 - Analysis Organization
   - \subpage eventselect Cuts
   - \subpage analysisPasses

 - Analysis Passes 
   - \subpage zerothpass "Zeroth Pass" Analysis
   - \subpage firstpass "First Pass" Analysis
   - \subpage secondpass "Second Pass" Analysis
   - \subpage pi0pass

   <h2>Authors, Contributors, and License</h2>

   <b>Author</b><br>
   Created and maintained by Whitney Armstrong (whit@temple.edu).

   <pre>Note this is was created as part of my thesis, so when its done
   feel free to cite it! 
   </pre>

   <b>Contributors</b><br>
   - David Flay 
   - Matthew Posik
   - You? C'mon now y'all.

   <b>Contributors</b><br>

   <b>License</b><br>
   LGPL 
<p style="width:600px;"> 
   <b>In it's current state </b> InSANE does not have complete references. 
   Hopefully that will be my in thesis. <b>Also</b> many exitisting codes, 
   mostly older standlone fortran codes, are used. <b> If you find your code </b> (which you hold copy(ing) rights), 
   and <b>you do not want it used here</b>
   please kindly let me know and it we be removed from the project. 
   But its mostly old fotran codes that have no copyright information. 
   </p>

 
 
 */

/*!  \page  analysisPasses Basic Analysis Passes
   \section passOverview Overview of Analysis Passes
   - Passes -2 and -1 are used to import and get the data ready to be analyzed.
   - Pass 0 is for pedestal and timing peak corrections, along with some premliminary analysis...  
   - Pass 1 is for rough TDC cuts, clustering, junk event removal etc...
   - Pass 2 creates trajectories, executes tight cuts on detectors, ...
   - Pass 3 ....


  There is a script called "sanePass" with the following syntax
  sanePass [-d] -r RUNNUMBER -p STARTPass [-P ENDPass] [-n NEvents]


   \section passNegative2 Pass -2 
   This uses the Hall C Analyzer to extract the raw data from the CODA files. It converts
   the HBOOK ntuples to root files using h2root. The output directory for the root files is "data/h2rootfiles".

   \section passNegative1 Pass -1 
   This pass begins to use the InSANE shared libraries. It creates the detector and scaler TTrees using concrete
  implementations of TSelector. See \ref AnalyzerToInSANE and \ref AnalyzerScalersToInSANE .
 
   \section pass0 Pass 0

  \section pass1 Pass 1
   

  \section pass2 Pass 2 

   
 */ 

/*!
  \page extendingInSANE Using and Extending InSANE 
   - \ref newClass
   - \ref fortran2C
   - \ref linkingwithpython
*/

/*! 
    \page linkingwithpython Using Python and PyROOT with InSANE
    \section pyROOTlink Linking InSANE Libraries with pyROOTlink
    First you need <a href="http://www.python.org/">python</a> 
    and <a href="http://root.cern.ch/drupal/content/pyroot">PyROOT</a>. 
    Also recommended is the python module 
    <a href="http://packages.python.org/rootplot/index.html"> rootplot </a>.
    
    The following example shows one way to use ROOT in python:
    \code
    from ROOT import gROOT,TCanvas
    c=TCanvas() 
    \endcode
    See <a href="http://wlav.web.cern.ch/wlav/pyroot/using.html"> PyROOT Manual</a> for 
    other ways and more details on using PyROOT. 

    \section pyinsanelink Using InSANE in python
    To use InSANE in python you import classes from the module ROOT (like the example above)
    but first you need to load the InSANE libraries and their dependencies into ROOT. 
    This similarly done in your \ref rootlogonC . 
    
    Create the file .rootlogon.py which is executed when you import ROOT. 
    The following example shows how to load and use InSANE from your .rootlogon.py script.
    
    \subsection rootlogonpy Example .rootlogon.py loading InSANE:
    \include simple_rootlogon.py
    
    \subsection example2py Example python script using InSANE:
    \include example2.py
     \image html bigcal_calibration.png "Plot of bigcal calibration coefficients produced by example2.py"
 */


//______________________________________________________________________________
/*! \page treesandbranches Tree and Branch Naming
   \section Trees TTree Names
   <a href="http://root.cern.ch/root/html/TTree.html">TTrees </a>(everyone is 
   <a href="http://root.cern.ch/root/html/TTree.html#TTree:AddFriend">Friends</a> 
   with everyone when it doesn't slow down too much)
   The <a href="http://root.cern.ch/root/html/TTree.html#TTree:BuildIndex">indices are built</a> with (run number, event number) 
   
   The following is a list of trees for SANE analysis (but not complete )
   <ul> 
   <li><em>betaDetectors</em> -> Detector data in a very raw form (uncorrected)
   <li><em>betaDetectors0</em> -> Detector data after a <em>zeroth pass</em> (corrected)
   <li><em>betaDetectors1</em> -> Detector data after a <em>first pass</em> (selection)
   <li><em>Clusters</em> -> Clustering results
   <li><em>pi0results</em> -> Pi0 reconstruction

   </ul>
 
   \section Branches TBranch Names
   <b>betaDetectors</b> Branches:
    - <em>betaDetectorEvent</em> -> Branch for BETAEvent. Do not set manually use helper class SANEEvents
    - <em>fBigcalEvent</em> -> Branch for BigcalEvent. Do not set manually use helper class SANEEvents
    - <em>fGasCherenkovEvent</em> -> Branch for GasCherenkovEvent. Do not set manually use helper class SANEEvents
    - <em>fLuciteHodoscopeEvent</em> -> Branch for LuciteHodoscopeEvent. Do not set manually use helper class SANEEvents
    - <em>fForwardTracker</em> -> Branch for ForwardTracker. Do not set manually use helper class SANEEvents

 */
/*!
   \page newClass Adding a New Class to Shared libraries
   Adding a new class is straightforward following the procedures highlighted 
   in the ROOT documentation. I list the steps for sanity's sake:
   <ol> 
   <li> Make source and header 
   <li> Add ClassDef(Class,#) to header just before the end of the class declaration's "};" 
        It does not need a semicolon after it and should always be the last thing.
   <li> Add ClassImp(Class), with no trailing semicolon at the top of the source. Between the
   includes and member function definitions.
   <li> Add appropriate line to (module)_LinkDef.h file. (eg. #pragma link C++ class BigcalEvent+;)
   <li> Create Dictionary header and source definitions with rootcint ( rootcint -f $(srcdir)/BETAEventDict.cxx -c $(CXXFLAGS) -p $^ ) This makes BETAEventDict.cxx and BETAEventDict.h. Or just find the makefiles in build and add a lib/NEWCLASSTHATNEEDSDICT.o to list of object files
   <li> Compile this to get the object file 
   <li> Assuming you have all objects make the shared libraries : g++ $(CXXFLAGS) -shared  -Wl,-soname,libWHATEVER.so.1.1 -o allTheObjectFiles.o) 
   <li> All goes well you have a shared library libWHATEVER.so.1.1
   </ol>
 */

//______________________________________________________________________________
/*!
   \page Analysis2 page about nothing

     a page about nothing
 */

//______________________________________________________________________________
/*!
   \page Analysis General Analysis Flow
   
   The data splits up into three distinct groups. 
   <ol>
   <li> Raw hardware data
   <li> Measured (or reconstructed) quantities 
   <li> Calculated physics quantities
   </ol>
   Going from Raw to Measured quantities is the most difficult task and can be done in many ways. 
   The physics quantities (e.g. Qsq,x,W etc..) are calculated as a function of the measured 
   quantities and should be independent of how they were reconstructed.
   
   Therefore, for measured quantities, we have the InSANEDetectorEvent class to store their data
   and a separate class, InSANEPhysics, for calculating the physics quantities. 
   
   \section Events Event structures
    (see Events modules)
   
   \section TClonesArray Creating and looping through the TClonesArray
   The various detector hit classes (see Hits module) are stored in a TClonesArray which is then written to the TTree/File. 
   <a href="http://root.cern.ch/root/htmldoc/TClonesArray.html">TClonesArray</a> acts like an array but is dynamic in size and quite fast. Here is some example code to create and fill a TClonesArray:
   \code 
   fgForwardTrackerHits = new TClonesArray("ForwardTrackerHit", 12);
   for(i=0;i<y2t_hit;i++) 
   {
     ftEvent->fNumberOfHits++;
     aFThit = new((*trackerHits)[i]) fgForwardTrackerHits();
     aFThit->fScintLayer = 2; 
   }
   \endcode
   Here is some example code to loop over a TClonesArray:
   \code 
     TClonesArray   clusters = 0; 
     for(Int_t i = 0;i<clusters->GetEntries();i++) 
     {
       aCluster = (BIGCALCluster *)(*clusters)[i];
       aCluster->DoStuff();
     }
   \endcode

*/

/*!
    \page scalers Scalers 
    \section scalerdata Scaler Data Source
    
    \section scaleranalysis Analysis and Syncronization of Scalers
    \section scalerFilters Temporal Scaler Filters
 */


//______________________________________________________________________________
/*! @defgroup asymmetries Asymmetries
    Asymmetries
 */

//______________________________________________________________________________
/*! @defgroup physics Physics
    Physics based classes
 */

//______________________________________________________________________________
/*! @defgroup xsections Cross sections
    Cross section specific code
    
    \image  html polXsecs.png "Example from scripts/polXsecs.cxx showing random sampling of 200k events by helicity" 
    
    \image  html unpolXsecs.png "Example from scripts/unpolXsecs.cxx" 

    @ingroup physics
 */

  /*! @defgroup inclusiveXSec Inclusive cross sections
      Inclusive cross sections
      @ingroup xsections
   */
  /*! @defgroup semiinclusiveXSec Semi-inclusive cross sections
      Semi-inclusive cross sections
      @ingroup xsections
   */
  /*! @defgroup exclusiveXSec Exclusive cross sections
      Exclusive cross sections
      @ingroup xsections
   */
  
  /* defgroup QFS QFS
      Quasi-free Scattering code 
      
      ingroup xsections
   */
  /* defgroup wiser WISER
      Inclusive particle production code from 1977
      ingroup inclusiveXSec
   */
  /* defgroup EPCV EPCV
      Inclusive particle production code from 1987
      ingroup inclusiveXSec
   */


//______________________________________________________________________________
/*! @defgroup structurefunctions Structure Functions
     Unpolarized and Polarized structure functions for nucleons and nuclei.

    \image html F2p_compare.png "Comparing F1F2 and LHAPDF code to data " width=10cm

    \image html g1p_compare.png "Comparing g1p code to data " width=10cm

    \image html polStructureFuncs.png "Polarized Structure Functions using ..." width=10cm

    \image html unpolStructureFuncs_LHAPDF.png "Structure Functions from LHAPDF" width=10cm

    \image html unpolStructureFuncs_F1F209.png "Structure Functions from F1F209" width=10cm

    @ingroup physics
 */

//______________________________________________________________________________
/*! @defgroup formfactors Form Factors
    Form factors for nucleons and nuclei
     
    @ingroup physics
 */

//______________________________________________________________________________
/*! @defgroup partondistributions Parton Distribution Functions
     Unpolarized and Polarized parton distribution functions
     
     \image html pol_pdfs.png "Polarized PDFs" width=10cm
     
    @ingroup physics
 */
   /*! @defgroup updfs Unpolarized PDFs
        Unpolarized  parton distribution functions
        
        \image html pol_pdfs.png "Polarized PDFs" width=10cm
        
       @ingroup partondistributions
    */
   /*! @defgroup ppdfs Polarized PDFs
        Unpolarized  parton distribution functions
        
        \image html pol_pdfs.png "Polarized PDFs" width=10cm
        
       @ingroup partondistributions
    */
   /** @name Unpolarized 
    *  @{ */
   /*! defgroup cteq6 CTEQ6 - Unpolarized PDFs
       CTEQ6  PDFS implemented by linking with original CTEQ Fortran code. 
       ingroup partondistributions
    */
   /*! defgroup lhapdf LHAPDF - Unpolarized PDFs
       A simple interface to LHAPDF 
       ingroup partondistributions
    */
   //@}
   /** @name Polarized 
    *  @{ */
   /*! defgroup AAC AAC Polarized PDFs
       Uses the Fortran subroutine AAC08PDF 
       ingroup partondistributions
    */
   /*! defgroup BB BB Polarized PDFs
       Polarized parton distributions by Bluemlein and Boettcher
       ingroup partondistributions
    */
   /*! defgroup DNS2005 DNS2005 Polarized PDFs
       Polarized parton distributions by D. de Florian G.A. Navarro and R. Sassot.
       ingroup partondistributions
    */
   /*! defgroup LSS2006 LSS2006 Polarized PDFs
       Polarized parton distributions by E. LEADER, A.V. SIDOROV AND D.B. STAMENOV  
       ingroup partondistributions
    */
   /*! defgroup GS1996 GS1996 Polarized PDFs
       Polarized parton distributions by T. Gehrmann and W.J. Stirling 
       ingroup partondistributions
    */
   //@}

//______________________________________________________________________________
/*! @defgroup fragfuncs Fragmentation Functions
     Quark fragmentation functions
     
    @ingroup physics
 */

//______________________________________________________________________________
/*! @defgroup tmds TMDs
     Transverse momentum distributions
     
    @ingroup physics
 */

//______________________________________________________________________________
/*! @defgroup Analysis Analysis
    This represents a generic analysis procedure
 */

/*! @defgroup emfields Electromagnetic Fields
    Electric and Magnetic fields
*/
/*! @defgroup Calculations Corrections and Calculations
    @ingroup Analysis
    Calculations are attached to a concrete implementation of InSANEAnalyzer and are used for clustering and
    creating new trees and branches in subsequent analysis levels. 
 */
/*! @defgroup Analyzers Analyzers
    @ingroup Analysis
    "Analyzers" are used to collect the detectors, corrections, calculations, clusterings, etc...
    and process an them all in an orderly way. 
 */

/*! @defgroup Analyses Analyses
    @ingroup Analysis
    Analyses ...
 */

/*! @defgroup EventGen Event Generation  

 */

/*! @defgroup Management Management 
   
 */

/*! @defgroup AnalysisManager  Analysis Manager
    @ingroup Management
 */

/*! @defgroup RunManager  Run Manager
    @ingroup Analysis
    @ingroup Management
 */


/*! @defgroup RunReport  Run Report
    @ingroup RunManager
    @ingroup Management
 */

/*! @defgroup RunTiming  Run Timing
    @ingroup RunManager
 */

/*! @defgroup Runs Runs
    @ingroup RunManager
    Run information
 */

/*! @defgroup Events Events
    Events are interesting indeed!
 */
/*! @defgroup Hits Hits
    @ingroup Events
    "Hit" classes are made to fill arrays with a generic data structure and provided some member functions
 */

/*! @defgroup Apparatus Apparatus
   Particle detectors have data associated with them for each event: ADCs TDCs etc...

   The important detector lingo to focus on here would be "Events" and "Hits". 
   That is an event contains all the data for said event. This may be a single value or
   a large array of numbers. To generalize a large array we use ROOT's TClonesArray. 
   We fill this array not with numbers but "objects" and in particluar a "Hit" object. 
   Thus a need for the classes Events and Hits 
   
 */
/*! @defgroup Detectors Detectors
    Detectors
    @ingroup Apparatus 
 */
/*! @defgroup detComponents Detector components
    Detector and Apparatus components 
    @ingroup Detectors 
 */
/*! @defgroup beam Hall C Beam
    BETA related code
    @ingroup Detectors 
 */
/*! @defgroup BETA BETA
    BETA related code
    @ingroup Detectors 
 */
/*! @defgroup BIGCAL BigCal
    BigCal related code
    @ingroup Detectors 
 */
/*! @defgroup Cherenkov Gas Cherenkov
    Gas Cherenkov related code
    @ingroup Detectors 
 */
/*! @defgroup tracker Forward Tracker
    Forward Tracker related code
    @ingroup Detectors 
 */
/*! @defgroup hodoscope Lucite Hodoscope
    Lucite Hodoscope related code
    @ingroup Detectors 
 */

/*! @defgroup Geometry Geometry
    Geometry related classes
 */

/*! @defgroup Coordinates Coordinate systems
    @ingroup Geometry
 */

/*! @defgroup Clustering Clustering
    General clustering. Mostly specific to calorimeter clustering... but not limited to
 */

/*! @defgroup Clusters Cluster Objects
    @ingroup Clustering
    Cluster classes used in various algorithms
 */

/*! @defgroup ClusteringAlgorithms Clustering Algorithms
    @ingroup Clustering
    These algorithms for clustering fill the InSANEClusterEvent's TClonesArray with the corresponding clusters.
    These clusters are the 
    concrete implementation of InSANECluster which can be specific to the algorithm.
 */



/*! @defgroup Scalers Scalers for all
    The HallC scalers are filled every 30 seconds and every 2 mins
 */

/*! @defgroup Simulation Simulation specific 
    more to come.
 */


/*! @defgroup Cuts Event Selection 
    @ingroup Analysis
    "Hit" classes are made to fill arrays with a generic data structure and provided some member functions


 */

/*! @defgroup Reconstruction Event Reconstruction
    Event reconstruction uses neural networks, assumed event types and decays, 
    and determines the physics quantities from the detected. 
 */
/*! @defgroup pi0 Pi0 Reconstruction
    @ingroup Reconstruction
    \f$ \pi^{0} \f$ Reconstruction 
 */

/*! @defgroup NeuralNetworks Neural Networks
    @ingroup Reconstruction
    Artificial Neural Networks used in event reconstruction
 */



/*! @defgroup SANE SANE
    This module contains all the SANE specific concrete classes and information
 */
// A simple program that computes the square root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "InSANEConfig.h"

int main (int argc, char *argv[])
{
   if (argc < 2)
   {
      fprintf(stdout,"%s Version %d.%d\n",
            argv[0],
            InSANE_VERSION_MAJOR,
            InSANE_VERSION_MINOR);
      fprintf(stdout,"Usage: %s number\n",argv[0]);
      return 1;
   }
   double inputValue = atof(argv[1]);
   double outputValue = sqrt(inputValue);
   fprintf(stdout,"The square root of %g is %g\n",
         inputValue, outputValue);
   return 0;
}

