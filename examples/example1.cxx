Int_t example1(Int_t runNumber = 1) {
  const char * sourceTreeName = "betaDetectors0";
  const char * outputTreeName = "betaDetectors1";
  rman->SetRun(runNumber); // Assumes rman object defined in current scope (eg rootlogon.cxx)

  SANEFirstPassAnalyzer * analyzer = 0;
  analyzer = new SANEFirstPassAnalyzer(outputTreeName ,sourceTreeName);

  analyzer->AddDetector( analyzer->fCherenkovDetector ); 
  analyzer->AddDetector( analyzer->fBigcalDetector ); 
  analyzer->AddDetector( analyzer->fHodoscopeDetector ); 
  analyzer->AddDetector( analyzer->fTrackerDetector ); 

  //___________CORRECTIONS and CALCULATIONS_________//
  BETATimeWalkCorrection* timewalkcorr = new BETATimeWalkCorrection(analyzer);
  timewalkcorr->SetDetectors(analyzer);

  analyzer->AddCorrection(timewalkcorr);

  GasCherenkovCalculation1 * g1;
  g1 = new GasCherenkovCalculation1(analyzer);
  g1->SetDetectors(analyzer);

  LuciteHodoscopeCalculation1 * l1;
  l1 = new LuciteHodoscopeCalculation1(analyzer);
  l1->SetDetectors(analyzer);

  BigcalCalculation1 * bc1;
  bc1 = new BigcalCalculation1(analyzer);
  bc1->SetDetectors(analyzer);

  /// Perform clustering
  LocalMaxClusterProcessor * clusterProcessor = 
     new LocalMaxClusterProcessor();
  BigcalClusteringCalculation1 * b1 = new BigcalClusteringCalculation1(analyzer);
  b1->SetClusterProcessor(clusterProcessor);
  b1->SetDetectors(analyzer);

  analyzer->AddCalculation(g1);
  analyzer->AddCalculation(l1);
  analyzer->AddCalculation(bc1);
  analyzer->AddCalculation(b1);

  analyzer->Initialize();

  analyzer->Process();

  if(gROOT->IsBatch())   gROOT->ProcessLine(".q");
  else return(0);
}
