import os
from ROOT import TCanvas,BigcalDetector

c = TCanvas()
bc = BigcalDetector()
h = bc.GetCalibrationCoefficientHist(1)
c.SaveAs('bigcal_calibration.png')
