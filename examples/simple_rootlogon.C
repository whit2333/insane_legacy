{
  gStyle->Reset();

  /// InSANE Libraries
  gSystem->Load("libFoam");
  gSystem->Load("libTreeViewer");
  gSystem->AddIncludePath("-I$InSANEDIR/include");
  gSystem->AddIncludePath("-I$InSANEDIR/main"); 
  gSystem->Load("libInSANE.so");
  gROOT->SetMacroPath(Form("%s:%s/main/scripts",gROOT->GetMacroPath(),gSystem->Getenv("InSANEDIR")));

  /// Frequently used pointers
  SANERunManager      * rman = SANERunManager::GetRunManager();
  SANEAnalysisManager * aman = SANEAnalysisManager::GetAnalysisManager();

  /// load some useful scripts
  gROOT->LoadMacro(Form("%s/src/set_plot_style.cxx",gSystem->Getenv("HOME") ) );



}
